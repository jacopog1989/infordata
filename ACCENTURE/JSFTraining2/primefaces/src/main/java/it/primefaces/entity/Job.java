package it.primefaces.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="jobs")
public class Job {
	
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="ID", nullable=false)
	private Long id;
	
	@Column(name="description", nullable=false)
	private String description;
	
	
	@Column(name="id_customer", nullable=false)
	@ManyToOne
	@JoinColumn(name ="id")
	private PoolCustomer idCustomer;


	public Job() {
	
	}


	public Job(Long id, String description, PoolCustomer idCustomer) {

		this.id = id;
		this.description = description;
		this.idCustomer = idCustomer;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public PoolCustomer getIdCustomer() {
		return idCustomer;
	}


	public void setIdCustomer(PoolCustomer idCustomer) {
		this.idCustomer = idCustomer;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Job other = (Job) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return "Job [id=" + id + ", description=" + description + ", idCustomer=" + idCustomer + "]";
	}
	
	
	
	
	
	
	
	
	
	

}
