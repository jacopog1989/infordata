package it.primefaces.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="pool_customer")
public class PoolCustomer {
	
	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="ID", nullable=false)
	private Long id;
	
	@Column(name="first_name", nullable=false)
	private String firstName;
	
	@Column(name="last_name", nullable=false)
	private String lastName;
	
	@Column(name="enter_date", nullable=false)
	private Date enterDate;
	
	@Column(name="email", nullable=false)
	private String email;
	
	@Column(name="phone", nullable=false)
	private String phone;

	@OneToMany(mappedBy="idCustomer")
	private List<Job> jobList;
	
	public PoolCustomer() {
		
	}

	public PoolCustomer(Long id, String firstName, String lastName, Date enterDate, String email, String phone, List<Job> jobList) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.enterDate = enterDate;
		this.email = email;
		this.phone = phone;
		this.jobList=jobList;
	}

	public List<Job> getJobList() {
		return jobList;
	}

	public void setJobList(List<Job> jobList) {
		this.jobList = jobList;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getEnterDate() {
		return enterDate;
	}

	public void setEnterDate(Date enterDate) {
		this.enterDate = enterDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	
	
	
	

}
