
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoEstrazione_type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoEstrazione_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NONE"/>
 *     &lt;enumeration value="DATA"/>
 *     &lt;enumeration value="METADATA"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoEstrazione_type")
@XmlEnum
public enum TipoEstrazioneType {

    NONE,
    DATA,
    METADATA;

    public String value() {
        return name();
    }

    public static TipoEstrazioneType fromValue(String v) {
        return valueOf(v);
    }

}
