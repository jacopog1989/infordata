
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Identificativi dei Fascicoli Raccolta Provvisoria
 * 
 * <p>Java class for risposta_getElencoFascicoliRaccoltaProvvisoriaAttoDecreto_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="risposta_getElencoFascicoliRaccoltaProvvisoriaAttoDecreto_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ElencoFacicoliRaccoltaProvvisoria" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}identificativiFascicoloRaccoltaProvvisoria_type" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_getElencoFascicoliRaccoltaProvvisoriaAttoDecreto_type", propOrder = {
    "elencoFacicoliRaccoltaProvvisoria"
})
public class RispostaGetElencoFascicoliRaccoltaProvvisoriaAttoDecretoType {

    @XmlElement(name = "ElencoFacicoliRaccoltaProvvisoria")
    protected List<IdentificativiFascicoloRaccoltaProvvisoriaType> elencoFacicoliRaccoltaProvvisoria;

    /**
     * Gets the value of the elencoFacicoliRaccoltaProvvisoria property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elencoFacicoliRaccoltaProvvisoria property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getElencoFacicoliRaccoltaProvvisoria().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IdentificativiFascicoloRaccoltaProvvisoriaType }
     * 
     * 
     */
    public List<IdentificativiFascicoloRaccoltaProvvisoriaType> getElencoFacicoliRaccoltaProvvisoria() {
        if (elencoFacicoliRaccoltaProvvisoria == null) {
            elencoFacicoliRaccoltaProvvisoria = new ArrayList<IdentificativiFascicoloRaccoltaProvvisoriaType>();
        }
        return this.elencoFacicoliRaccoltaProvvisoria;
    }

}
