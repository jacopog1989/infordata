
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiede l'eliminazione di un documento Allegato Decreto IGB
 * 
 * <p>Java class for richiesta_removeDocumentoFascicoloAllegatoDecretoIGB_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_removeDocumentoFascicoloAllegatoDecretoIGB_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloAttoDecreto" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="IdDocumentoFascicoloAllegatoDecretoIGB" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_removeDocumentoFascicoloAllegatoDecretoIGB_type", propOrder = {
    "idFascicoloAttoDecreto",
    "idDocumentoFascicoloAllegatoDecretoIGB"
})
public class RichiestaRemoveDocumentoFascicoloAllegatoDecretoIGBType {

    @XmlElement(name = "IdFascicoloAttoDecreto", required = true)
    protected String idFascicoloAttoDecreto;
    @XmlElement(name = "IdDocumentoFascicoloAllegatoDecretoIGB", required = true)
    protected String idDocumentoFascicoloAllegatoDecretoIGB;

    /**
     * Gets the value of the idFascicoloAttoDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloAttoDecreto() {
        return idFascicoloAttoDecreto;
    }

    /**
     * Sets the value of the idFascicoloAttoDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloAttoDecreto(String value) {
        this.idFascicoloAttoDecreto = value;
    }

    /**
     * Gets the value of the idDocumentoFascicoloAllegatoDecretoIGB property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoFascicoloAllegatoDecretoIGB() {
        return idDocumentoFascicoloAllegatoDecretoIGB;
    }

    /**
     * Sets the value of the idDocumentoFascicoloAllegatoDecretoIGB property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoFascicoloAllegatoDecretoIGB(String value) {
        this.idDocumentoFascicoloAllegatoDecretoIGB = value;
    }

}
