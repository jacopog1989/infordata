
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Metadati del Fascicolo Decreto SIPATR
 * 
 * <p>Java class for risposta_getFascicoloSIPATRAttoDecreto_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="risposta_getFascicoloSIPATRAttoDecreto_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}BaseServiceResponseType">
 *       &lt;sequence>
 *         &lt;element name="DettaglioFascicolo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}fascicoloSIPATR_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_getFascicoloSIPATRAttoDecreto_type", propOrder = {
    "dettaglioFascicolo"
})
public class RispostaGetFascicoloSIPATRAttoDecretoType
    extends BaseServiceResponseType
{

    @XmlElement(name = "DettaglioFascicolo")
    protected FascicoloSIPATRType dettaglioFascicolo;

    /**
     * Gets the value of the dettaglioFascicolo property.
     * 
     * @return
     *     possible object is
     *     {@link FascicoloSIPATRType }
     *     
     */
    public FascicoloSIPATRType getDettaglioFascicolo() {
        return dettaglioFascicolo;
    }

    /**
     * Sets the value of the dettaglioFascicolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link FascicoloSIPATRType }
     *     
     */
    public void setDettaglioFascicolo(FascicoloSIPATRType value) {
        this.dettaglioFascicolo = value;
    }

}
