
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoMessaggioAD_type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoMessaggioAD_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="RGS-CDC 5xx"/>
 *     &lt;enumeration value="CDC-RGS 6xx"/>
 *     &lt;enumeration value="CONFERMA_RICEZIONE"/>
 *     &lt;enumeration value="NOTIFICA_ECCEZIONE"/>
 *     &lt;enumeration value="AGGIORNAMENTO_CONFERMA"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoMessaggioAD_type")
@XmlEnum
public enum TipoMessaggioADType {

    @XmlEnumValue("RGS-CDC 5xx")
    RGS_CDC_5_XX("RGS-CDC 5xx"),
    @XmlEnumValue("CDC-RGS 6xx")
    CDC_RGS_6_XX("CDC-RGS 6xx"),
    CONFERMA_RICEZIONE("CONFERMA_RICEZIONE"),
    NOTIFICA_ECCEZIONE("NOTIFICA_ECCEZIONE"),
    AGGIORNAMENTO_CONFERMA("AGGIORNAMENTO_CONFERMA");
    private final String value;

    TipoMessaggioADType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TipoMessaggioADType fromValue(String v) {
        for (TipoMessaggioADType c: TipoMessaggioADType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
