
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlType;


/**
 * Contenuto in byte del file
 * 
 * <p>Java class for documentoContent_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="documentoContent_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}documentoFile_type">
 *       &lt;sequence>
 *         &lt;element name="Content" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *         &lt;element name="PreviewPng" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "documentoContent_type", propOrder = {
    "content",
    "previewPng"
})
public class DocumentoContentType
    extends DocumentoFileType
{

    @XmlElement(name = "Content", required = true)
    @XmlMimeType("application/octet-stream")
    protected DataHandler content;
    @XmlElement(name = "PreviewPng")
    protected byte[] previewPng;

    /**
     * Gets the value of the content property.
     * 
     * @return
     *     possible object is
     *     {@link DataHandler }
     *     
     */
    public DataHandler getContent() {
        return content;
    }

    /**
     * Sets the value of the content property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataHandler }
     *     
     */
    public void setContent(DataHandler value) {
        this.content = value;
    }

    /**
     * Gets the value of the previewPng property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getPreviewPng() {
        return previewPng;
    }

    /**
     * Sets the value of the previewPng property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setPreviewPng(byte[] value) {
        this.previewPng = value;
    }

}
