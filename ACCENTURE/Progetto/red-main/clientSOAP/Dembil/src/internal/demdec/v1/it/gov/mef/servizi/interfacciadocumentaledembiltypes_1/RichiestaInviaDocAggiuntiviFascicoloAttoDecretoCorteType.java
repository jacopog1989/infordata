
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiesta invio del fascicolo AttoDecreto a Corte 
 * 
 * <p>Java class for richiesta_inviaDocAggiuntiviFascicoloAttoDecretoCorte_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_inviaDocAggiuntiviFascicoloAttoDecretoCorte_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloAttoDecreto" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="Applicazione" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="Flusso_AD" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}flussoAD_type"/>
 *         &lt;element name="IdDocumentoAggiuntivo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_inviaDocAggiuntiviFascicoloAttoDecretoCorte_type", propOrder = {
    "idFascicoloAttoDecreto",
    "applicazione",
    "flussoAD",
    "idDocumentoAggiuntivo"
})
public class RichiestaInviaDocAggiuntiviFascicoloAttoDecretoCorteType {

    @XmlElement(name = "IdFascicoloAttoDecreto", required = true)
    protected String idFascicoloAttoDecreto;
    @XmlElement(name = "Applicazione", required = true)
    protected Object applicazione;
    @XmlElement(name = "Flusso_AD", required = true)
    protected FlussoADType flussoAD;
    @XmlElement(name = "IdDocumentoAggiuntivo", required = true)
    protected List<String> idDocumentoAggiuntivo;

    /**
     * Gets the value of the idFascicoloAttoDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloAttoDecreto() {
        return idFascicoloAttoDecreto;
    }

    /**
     * Sets the value of the idFascicoloAttoDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloAttoDecreto(String value) {
        this.idFascicoloAttoDecreto = value;
    }

    /**
     * Gets the value of the applicazione property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getApplicazione() {
        return applicazione;
    }

    /**
     * Sets the value of the applicazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setApplicazione(Object value) {
        this.applicazione = value;
    }

    /**
     * Gets the value of the flussoAD property.
     * 
     * @return
     *     possible object is
     *     {@link FlussoADType }
     *     
     */
    public FlussoADType getFlussoAD() {
        return flussoAD;
    }

    /**
     * Sets the value of the flussoAD property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlussoADType }
     *     
     */
    public void setFlussoAD(FlussoADType value) {
        this.flussoAD = value;
    }

    /**
     * Gets the value of the idDocumentoAggiuntivo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the idDocumentoAggiuntivo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdDocumentoAggiuntivo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getIdDocumentoAggiuntivo() {
        if (idDocumentoAggiuntivo == null) {
            idDocumentoAggiuntivo = new ArrayList<String>();
        }
        return this.idDocumentoAggiuntivo;
    }

}
