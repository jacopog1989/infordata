
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Dettaglio del fascicolo Atto Decreto
 * 
 * <p>Java class for fascicoloAttoDecreto_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="fascicoloAttoDecreto_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloAttoDecreto" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="IdFascicoloAttoDecretoParent" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid" minOccurs="0"/>
 *         &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DatiFascicolo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}fascicoloMetadataAttoDecreto_type" minOccurs="0"/>
 *         &lt;element name="Documenti" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}documentoFascicoloAttoDecreto_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DocumentazioneAggiuntiva" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}documentoFascicolo_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="FascicoliSIPATR" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}fascicoloSIPATR_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="AllegatiDecretoIGB" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}documentoFascicoloAllegatoDecretoIGB_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="StatoFascicoloDocumentale" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}statoFascicoloDocumentale_type"/>
 *         &lt;element name="DataCreazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="DataAggiornamentoAttoDecreto" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DataAggiornamentoSIPATR" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DataAggiornamentoFascicoloIGB" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DataChiusura" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DaInviare" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Attivo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SistemaProduttore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NumeroAllegati" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}numeroAllegati_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fascicoloAttoDecreto_type", propOrder = {
    "idFascicoloAttoDecreto",
    "idFascicoloAttoDecretoParent",
    "descrizione",
    "datiFascicolo",
    "documenti",
    "documentazioneAggiuntiva",
    "fascicoliSIPATR",
    "allegatiDecretoIGB",
    "statoFascicoloDocumentale",
    "dataCreazione",
    "dataAggiornamentoAttoDecreto",
    "dataAggiornamentoSIPATR",
    "dataAggiornamentoFascicoloIGB",
    "dataChiusura",
    "daInviare",
    "attivo",
    "sistemaProduttore",
    "numeroAllegati"
})
public class FascicoloAttoDecretoType {

    @XmlElement(name = "IdFascicoloAttoDecreto", required = true)
    protected String idFascicoloAttoDecreto;
    @XmlElement(name = "IdFascicoloAttoDecretoParent")
    protected String idFascicoloAttoDecretoParent;
    @XmlElement(name = "Descrizione", required = true)
    protected String descrizione;
    @XmlElement(name = "DatiFascicolo")
    protected FascicoloMetadataAttoDecretoType datiFascicolo;
    @XmlElement(name = "Documenti")
    protected List<DocumentoFascicoloAttoDecretoType> documenti;
    @XmlElement(name = "DocumentazioneAggiuntiva")
    protected List<DocumentoFascicoloType> documentazioneAggiuntiva;
    @XmlElement(name = "FascicoliSIPATR")
    protected List<FascicoloSIPATRType> fascicoliSIPATR;
    @XmlElement(name = "AllegatiDecretoIGB")
    protected List<DocumentoFascicoloAllegatoDecretoIGBType> allegatiDecretoIGB;
    @XmlElement(name = "StatoFascicoloDocumentale", required = true)
    protected StatoFascicoloDocumentaleType statoFascicoloDocumentale;
    @XmlElement(name = "DataCreazione", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataCreazione;
    @XmlElement(name = "DataAggiornamentoAttoDecreto", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataAggiornamentoAttoDecreto;
    @XmlElement(name = "DataAggiornamentoSIPATR")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataAggiornamentoSIPATR;
    @XmlElement(name = "DataAggiornamentoFascicoloIGB")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataAggiornamentoFascicoloIGB;
    @XmlElement(name = "DataChiusura")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataChiusura;
    @XmlElement(name = "DaInviare", defaultValue = "true")
    protected boolean daInviare;
    @XmlElement(name = "Attivo", defaultValue = "true")
    protected boolean attivo;
    @XmlElement(name = "SistemaProduttore", required = true)
    protected String sistemaProduttore;
    @XmlElement(name = "NumeroAllegati")
    protected NumeroAllegatiType numeroAllegati;

    /**
     * Gets the value of the idFascicoloAttoDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloAttoDecreto() {
        return idFascicoloAttoDecreto;
    }

    /**
     * Sets the value of the idFascicoloAttoDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloAttoDecreto(String value) {
        this.idFascicoloAttoDecreto = value;
    }

    /**
     * Gets the value of the idFascicoloAttoDecretoParent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloAttoDecretoParent() {
        return idFascicoloAttoDecretoParent;
    }

    /**
     * Sets the value of the idFascicoloAttoDecretoParent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloAttoDecretoParent(String value) {
        this.idFascicoloAttoDecretoParent = value;
    }

    /**
     * Gets the value of the descrizione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Sets the value of the descrizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Gets the value of the datiFascicolo property.
     * 
     * @return
     *     possible object is
     *     {@link FascicoloMetadataAttoDecretoType }
     *     
     */
    public FascicoloMetadataAttoDecretoType getDatiFascicolo() {
        return datiFascicolo;
    }

    /**
     * Sets the value of the datiFascicolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link FascicoloMetadataAttoDecretoType }
     *     
     */
    public void setDatiFascicolo(FascicoloMetadataAttoDecretoType value) {
        this.datiFascicolo = value;
    }

    /**
     * Gets the value of the documenti property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documenti property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocumenti().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentoFascicoloAttoDecretoType }
     * 
     * 
     */
    public List<DocumentoFascicoloAttoDecretoType> getDocumenti() {
        if (documenti == null) {
            documenti = new ArrayList<DocumentoFascicoloAttoDecretoType>();
        }
        return this.documenti;
    }

    /**
     * Gets the value of the documentazioneAggiuntiva property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documentazioneAggiuntiva property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocumentazioneAggiuntiva().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentoFascicoloType }
     * 
     * 
     */
    public List<DocumentoFascicoloType> getDocumentazioneAggiuntiva() {
        if (documentazioneAggiuntiva == null) {
            documentazioneAggiuntiva = new ArrayList<DocumentoFascicoloType>();
        }
        return this.documentazioneAggiuntiva;
    }

    /**
     * Gets the value of the fascicoliSIPATR property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fascicoliSIPATR property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFascicoliSIPATR().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FascicoloSIPATRType }
     * 
     * 
     */
    public List<FascicoloSIPATRType> getFascicoliSIPATR() {
        if (fascicoliSIPATR == null) {
            fascicoliSIPATR = new ArrayList<FascicoloSIPATRType>();
        }
        return this.fascicoliSIPATR;
    }

    /**
     * Gets the value of the allegatiDecretoIGB property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allegatiDecretoIGB property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllegatiDecretoIGB().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentoFascicoloAllegatoDecretoIGBType }
     * 
     * 
     */
    public List<DocumentoFascicoloAllegatoDecretoIGBType> getAllegatiDecretoIGB() {
        if (allegatiDecretoIGB == null) {
            allegatiDecretoIGB = new ArrayList<DocumentoFascicoloAllegatoDecretoIGBType>();
        }
        return this.allegatiDecretoIGB;
    }

    /**
     * Gets the value of the statoFascicoloDocumentale property.
     * 
     * @return
     *     possible object is
     *     {@link StatoFascicoloDocumentaleType }
     *     
     */
    public StatoFascicoloDocumentaleType getStatoFascicoloDocumentale() {
        return statoFascicoloDocumentale;
    }

    /**
     * Sets the value of the statoFascicoloDocumentale property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatoFascicoloDocumentaleType }
     *     
     */
    public void setStatoFascicoloDocumentale(StatoFascicoloDocumentaleType value) {
        this.statoFascicoloDocumentale = value;
    }

    /**
     * Gets the value of the dataCreazione property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataCreazione() {
        return dataCreazione;
    }

    /**
     * Sets the value of the dataCreazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataCreazione(XMLGregorianCalendar value) {
        this.dataCreazione = value;
    }

    /**
     * Gets the value of the dataAggiornamentoAttoDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAggiornamentoAttoDecreto() {
        return dataAggiornamentoAttoDecreto;
    }

    /**
     * Sets the value of the dataAggiornamentoAttoDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAggiornamentoAttoDecreto(XMLGregorianCalendar value) {
        this.dataAggiornamentoAttoDecreto = value;
    }

    /**
     * Gets the value of the dataAggiornamentoSIPATR property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAggiornamentoSIPATR() {
        return dataAggiornamentoSIPATR;
    }

    /**
     * Sets the value of the dataAggiornamentoSIPATR property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAggiornamentoSIPATR(XMLGregorianCalendar value) {
        this.dataAggiornamentoSIPATR = value;
    }

    /**
     * Gets the value of the dataAggiornamentoFascicoloIGB property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAggiornamentoFascicoloIGB() {
        return dataAggiornamentoFascicoloIGB;
    }

    /**
     * Sets the value of the dataAggiornamentoFascicoloIGB property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAggiornamentoFascicoloIGB(XMLGregorianCalendar value) {
        this.dataAggiornamentoFascicoloIGB = value;
    }

    /**
     * Gets the value of the dataChiusura property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataChiusura() {
        return dataChiusura;
    }

    /**
     * Sets the value of the dataChiusura property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataChiusura(XMLGregorianCalendar value) {
        this.dataChiusura = value;
    }

    /**
     * Gets the value of the daInviare property.
     * 
     */
    public boolean isDaInviare() {
        return daInviare;
    }

    /**
     * Sets the value of the daInviare property.
     * 
     */
    public void setDaInviare(boolean value) {
        this.daInviare = value;
    }

    /**
     * Gets the value of the attivo property.
     * 
     */
    public boolean isAttivo() {
        return attivo;
    }

    /**
     * Sets the value of the attivo property.
     * 
     */
    public void setAttivo(boolean value) {
        this.attivo = value;
    }

    /**
     * Gets the value of the sistemaProduttore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaProduttore() {
        return sistemaProduttore;
    }

    /**
     * Sets the value of the sistemaProduttore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaProduttore(String value) {
        this.sistemaProduttore = value;
    }

    /**
     * Gets the value of the numeroAllegati property.
     * 
     * @return
     *     possible object is
     *     {@link NumeroAllegatiType }
     *     
     */
    public NumeroAllegatiType getNumeroAllegati() {
        return numeroAllegati;
    }

    /**
     * Sets the value of the numeroAllegati property.
     * 
     * @param value
     *     allowed object is
     *     {@link NumeroAllegatiType }
     *     
     */
    public void setNumeroAllegati(NumeroAllegatiType value) {
        this.numeroAllegati = value;
    }

}
