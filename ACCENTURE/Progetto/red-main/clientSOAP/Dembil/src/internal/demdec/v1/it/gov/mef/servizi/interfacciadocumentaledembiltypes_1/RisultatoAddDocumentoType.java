
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Tipo generico per la risposta Add Documento
 * 
 * <p>Java class for risultatoAddDocumento_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="risultatoAddDocumento_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DatiDocumento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}documentoFile_type"/>
 *         &lt;element name="DocumentiTrattati" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}documentoResultType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risultatoAddDocumento_type", propOrder = {
    "datiDocumento",
    "documentiTrattati"
})
public class RisultatoAddDocumentoType {

    @XmlElement(name = "DatiDocumento", required = true)
    protected DocumentoFileType datiDocumento;
    @XmlElement(name = "DocumentiTrattati")
    protected List<DocumentoResultType> documentiTrattati;

    /**
     * Gets the value of the datiDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoFileType }
     *     
     */
    public DocumentoFileType getDatiDocumento() {
        return datiDocumento;
    }

    /**
     * Sets the value of the datiDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoFileType }
     *     
     */
    public void setDatiDocumento(DocumentoFileType value) {
        this.datiDocumento = value;
    }

    /**
     * Gets the value of the documentiTrattati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documentiTrattati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocumentiTrattati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentoResultType }
     * 
     * 
     */
    public List<DocumentoResultType> getDocumentiTrattati() {
        if (documentiTrattati == null) {
            documentiTrattati = new ArrayList<DocumentoResultType>();
        }
        return this.documentiTrattati;
    }

}
