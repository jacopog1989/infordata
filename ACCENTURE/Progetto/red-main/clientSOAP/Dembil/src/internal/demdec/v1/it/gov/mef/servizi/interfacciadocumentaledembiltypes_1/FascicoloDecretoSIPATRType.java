
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Dettaglio del fascicolo decreto SIPATR
 * 
 * <p>Java class for fascicoloDecretoSIPATR_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="fascicoloDecretoSIPATR_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloDecretoSIPATR" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DatiFascicolo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}fascicoloMetadataDecretoSIPATR_type" minOccurs="0"/>
 *         &lt;element name="DocumentazioneAggiuntiva" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}documentoFascicolo_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="FascicoliSIPATR" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}fascicoloSIPATR_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="StatoFascicoloDocumentale" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}statoFascicoloDocumentale_type"/>
 *         &lt;element name="DataCreazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="DataAggiornamento" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DataChiusura" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="NumeroAllegati" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}numeroAllegati_type" minOccurs="0"/>
 *         &lt;element name="DaInviare" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Attivo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SistemaProduttore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fascicoloDecretoSIPATR_type", propOrder = {
    "idFascicoloDecretoSIPATR",
    "descrizione",
    "datiFascicolo",
    "documentazioneAggiuntiva",
    "fascicoliSIPATR",
    "statoFascicoloDocumentale",
    "dataCreazione",
    "dataAggiornamento",
    "dataChiusura",
    "numeroAllegati",
    "daInviare",
    "attivo",
    "sistemaProduttore"
})
public class FascicoloDecretoSIPATRType {

    @XmlElement(name = "IdFascicoloDecretoSIPATR", required = true)
    protected String idFascicoloDecretoSIPATR;
    @XmlElement(name = "Descrizione", required = true)
    protected String descrizione;
    @XmlElement(name = "DatiFascicolo")
    protected FascicoloMetadataDecretoSIPATRType datiFascicolo;
    @XmlElement(name = "DocumentazioneAggiuntiva")
    protected List<DocumentoFascicoloType> documentazioneAggiuntiva;
    @XmlElement(name = "FascicoliSIPATR")
    protected List<FascicoloSIPATRType> fascicoliSIPATR;
    @XmlElement(name = "StatoFascicoloDocumentale", required = true)
    protected StatoFascicoloDocumentaleType statoFascicoloDocumentale;
    @XmlElement(name = "DataCreazione", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataCreazione;
    @XmlElement(name = "DataAggiornamento", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataAggiornamento;
    @XmlElement(name = "DataChiusura")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataChiusura;
    @XmlElement(name = "NumeroAllegati")
    protected NumeroAllegatiType numeroAllegati;
    @XmlElement(name = "DaInviare", defaultValue = "true")
    protected boolean daInviare;
    @XmlElement(name = "Attivo", defaultValue = "true")
    protected boolean attivo;
    @XmlElement(name = "SistemaProduttore", required = true)
    protected String sistemaProduttore;

    /**
     * Gets the value of the idFascicoloDecretoSIPATR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloDecretoSIPATR() {
        return idFascicoloDecretoSIPATR;
    }

    /**
     * Sets the value of the idFascicoloDecretoSIPATR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloDecretoSIPATR(String value) {
        this.idFascicoloDecretoSIPATR = value;
    }

    /**
     * Gets the value of the descrizione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Sets the value of the descrizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Gets the value of the datiFascicolo property.
     * 
     * @return
     *     possible object is
     *     {@link FascicoloMetadataDecretoSIPATRType }
     *     
     */
    public FascicoloMetadataDecretoSIPATRType getDatiFascicolo() {
        return datiFascicolo;
    }

    /**
     * Sets the value of the datiFascicolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link FascicoloMetadataDecretoSIPATRType }
     *     
     */
    public void setDatiFascicolo(FascicoloMetadataDecretoSIPATRType value) {
        this.datiFascicolo = value;
    }

    /**
     * Gets the value of the documentazioneAggiuntiva property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documentazioneAggiuntiva property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocumentazioneAggiuntiva().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentoFascicoloType }
     * 
     * 
     */
    public List<DocumentoFascicoloType> getDocumentazioneAggiuntiva() {
        if (documentazioneAggiuntiva == null) {
            documentazioneAggiuntiva = new ArrayList<DocumentoFascicoloType>();
        }
        return this.documentazioneAggiuntiva;
    }

    /**
     * Gets the value of the fascicoliSIPATR property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fascicoliSIPATR property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFascicoliSIPATR().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FascicoloSIPATRType }
     * 
     * 
     */
    public List<FascicoloSIPATRType> getFascicoliSIPATR() {
        if (fascicoliSIPATR == null) {
            fascicoliSIPATR = new ArrayList<FascicoloSIPATRType>();
        }
        return this.fascicoliSIPATR;
    }

    /**
     * Gets the value of the statoFascicoloDocumentale property.
     * 
     * @return
     *     possible object is
     *     {@link StatoFascicoloDocumentaleType }
     *     
     */
    public StatoFascicoloDocumentaleType getStatoFascicoloDocumentale() {
        return statoFascicoloDocumentale;
    }

    /**
     * Sets the value of the statoFascicoloDocumentale property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatoFascicoloDocumentaleType }
     *     
     */
    public void setStatoFascicoloDocumentale(StatoFascicoloDocumentaleType value) {
        this.statoFascicoloDocumentale = value;
    }

    /**
     * Gets the value of the dataCreazione property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataCreazione() {
        return dataCreazione;
    }

    /**
     * Sets the value of the dataCreazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataCreazione(XMLGregorianCalendar value) {
        this.dataCreazione = value;
    }

    /**
     * Gets the value of the dataAggiornamento property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAggiornamento() {
        return dataAggiornamento;
    }

    /**
     * Sets the value of the dataAggiornamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAggiornamento(XMLGregorianCalendar value) {
        this.dataAggiornamento = value;
    }

    /**
     * Gets the value of the dataChiusura property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataChiusura() {
        return dataChiusura;
    }

    /**
     * Sets the value of the dataChiusura property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataChiusura(XMLGregorianCalendar value) {
        this.dataChiusura = value;
    }

    /**
     * Gets the value of the numeroAllegati property.
     * 
     * @return
     *     possible object is
     *     {@link NumeroAllegatiType }
     *     
     */
    public NumeroAllegatiType getNumeroAllegati() {
        return numeroAllegati;
    }

    /**
     * Sets the value of the numeroAllegati property.
     * 
     * @param value
     *     allowed object is
     *     {@link NumeroAllegatiType }
     *     
     */
    public void setNumeroAllegati(NumeroAllegatiType value) {
        this.numeroAllegati = value;
    }

    /**
     * Gets the value of the daInviare property.
     * 
     */
    public boolean isDaInviare() {
        return daInviare;
    }

    /**
     * Sets the value of the daInviare property.
     * 
     */
    public void setDaInviare(boolean value) {
        this.daInviare = value;
    }

    /**
     * Gets the value of the attivo property.
     * 
     */
    public boolean isAttivo() {
        return attivo;
    }

    /**
     * Sets the value of the attivo property.
     * 
     */
    public void setAttivo(boolean value) {
        this.attivo = value;
    }

    /**
     * Gets the value of the sistemaProduttore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaProduttore() {
        return sistemaProduttore;
    }

    /**
     * Sets the value of the sistemaProduttore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaProduttore(String value) {
        this.sistemaProduttore = value;
    }

}
