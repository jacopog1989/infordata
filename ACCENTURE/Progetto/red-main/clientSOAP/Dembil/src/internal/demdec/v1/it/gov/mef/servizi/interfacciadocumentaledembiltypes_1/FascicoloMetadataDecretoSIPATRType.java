
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Metadati del Fascicolo Decreto SIPATR
 * 
 * <p>Java class for fascicoloMetadataDecretoSIPATR_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="fascicoloMetadataDecretoSIPATR_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdentificativoSIPATR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdFascicoloAttoDecreto" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid" minOccurs="0"/>
 *         &lt;element name="Anno" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="NumeroDecreto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Titolo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="Metadata" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="VersioneMetadata" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fascicoloMetadataDecretoSIPATR_type", propOrder = {
    "identificativoSIPATR",
    "idFascicoloAttoDecreto",
    "anno",
    "numeroDecreto",
    "titolo",
    "metadata",
    "versioneMetadata"
})
public class FascicoloMetadataDecretoSIPATRType {

    @XmlElement(name = "IdentificativoSIPATR", required = true)
    protected String identificativoSIPATR;
    @XmlElement(name = "IdFascicoloAttoDecreto")
    protected String idFascicoloAttoDecreto;
    @XmlElement(name = "Anno")
    protected int anno;
    @XmlElement(name = "NumeroDecreto")
    protected int numeroDecreto;
    @XmlElement(name = "Titolo", required = true)
    protected CodiceDescrizioneType titolo;
    @XmlElement(name = "Metadata")
    protected byte[] metadata;
    @XmlElement(name = "VersioneMetadata")
    protected String versioneMetadata;

    /**
     * Gets the value of the identificativoSIPATR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificativoSIPATR() {
        return identificativoSIPATR;
    }

    /**
     * Sets the value of the identificativoSIPATR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificativoSIPATR(String value) {
        this.identificativoSIPATR = value;
    }

    /**
     * Gets the value of the idFascicoloAttoDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloAttoDecreto() {
        return idFascicoloAttoDecreto;
    }

    /**
     * Sets the value of the idFascicoloAttoDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloAttoDecreto(String value) {
        this.idFascicoloAttoDecreto = value;
    }

    /**
     * Gets the value of the anno property.
     * 
     */
    public int getAnno() {
        return anno;
    }

    /**
     * Sets the value of the anno property.
     * 
     */
    public void setAnno(int value) {
        this.anno = value;
    }

    /**
     * Gets the value of the numeroDecreto property.
     * 
     */
    public int getNumeroDecreto() {
        return numeroDecreto;
    }

    /**
     * Sets the value of the numeroDecreto property.
     * 
     */
    public void setNumeroDecreto(int value) {
        this.numeroDecreto = value;
    }

    /**
     * Gets the value of the titolo property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getTitolo() {
        return titolo;
    }

    /**
     * Sets the value of the titolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setTitolo(CodiceDescrizioneType value) {
        this.titolo = value;
    }

    /**
     * Gets the value of the metadata property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getMetadata() {
        return metadata;
    }

    /**
     * Sets the value of the metadata property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setMetadata(byte[] value) {
        this.metadata = value;
    }

    /**
     * Gets the value of the versioneMetadata property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersioneMetadata() {
        return versioneMetadata;
    }

    /**
     * Sets the value of the versioneMetadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersioneMetadata(String value) {
        this.versioneMetadata = value;
    }

}
