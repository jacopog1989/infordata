
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiesta di aggiunta del documento al fascicolo Atto Decreto
 * 
 * <p>Java class for richiesta_addDocumentoFascicoloAttoDecretoCorte_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_addDocumentoFascicoloAttoDecretoCorte_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloAttoDecreto" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="IdDocumento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="TipoDocumento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoDocumento_type"/>
 *         &lt;element name="DocumentoContent" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}documentoContent_type"/>
 *         &lt;element name="DatiDocumento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}documentoMetadataAttoDecreto_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_addDocumentoFascicoloAttoDecretoCorte_type", propOrder = {
    "idFascicoloAttoDecreto",
    "idDocumento",
    "tipoDocumento",
    "documentoContent",
    "datiDocumento"
})
public class RichiestaAddDocumentoFascicoloAttoDecretoCorteType {

    @XmlElement(name = "IdFascicoloAttoDecreto", required = true)
    protected String idFascicoloAttoDecreto;
    @XmlElement(name = "IdDocumento", required = true, nillable = true)
    protected String idDocumento;
    @XmlElement(name = "TipoDocumento", required = true)
    protected TipoDocumentoType tipoDocumento;
    @XmlElement(name = "DocumentoContent", required = true)
    protected DocumentoContentType documentoContent;
    @XmlElement(name = "DatiDocumento", required = true)
    protected DocumentoMetadataAttoDecretoType datiDocumento;

    /**
     * Gets the value of the idFascicoloAttoDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloAttoDecreto() {
        return idFascicoloAttoDecreto;
    }

    /**
     * Sets the value of the idFascicoloAttoDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloAttoDecreto(String value) {
        this.idFascicoloAttoDecreto = value;
    }

    /**
     * Gets the value of the idDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumento() {
        return idDocumento;
    }

    /**
     * Sets the value of the idDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumento(String value) {
        this.idDocumento = value;
    }

    /**
     * Gets the value of the tipoDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link TipoDocumentoType }
     *     
     */
    public TipoDocumentoType getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Sets the value of the tipoDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDocumentoType }
     *     
     */
    public void setTipoDocumento(TipoDocumentoType value) {
        this.tipoDocumento = value;
    }

    /**
     * Gets the value of the documentoContent property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoContentType }
     *     
     */
    public DocumentoContentType getDocumentoContent() {
        return documentoContent;
    }

    /**
     * Sets the value of the documentoContent property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoContentType }
     *     
     */
    public void setDocumentoContent(DocumentoContentType value) {
        this.documentoContent = value;
    }

    /**
     * Gets the value of the datiDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoMetadataAttoDecretoType }
     *     
     */
    public DocumentoMetadataAttoDecretoType getDatiDocumento() {
        return datiDocumento;
    }

    /**
     * Sets the value of the datiDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoMetadataAttoDecretoType }
     *     
     */
    public void setDatiDocumento(DocumentoMetadataAttoDecretoType value) {
        this.datiDocumento = value;
    }

}
