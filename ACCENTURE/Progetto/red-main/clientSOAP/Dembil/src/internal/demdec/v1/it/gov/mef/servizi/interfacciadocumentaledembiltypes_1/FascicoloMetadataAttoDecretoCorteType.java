
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Metadati del Fascicolo Atto Decreto
 * 
 * <p>Java class for fascicoloMetadataAttoDecretoCorte_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="fascicoloMetadataAttoDecretoCorte_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdentificativoAttoDecreto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Titolo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="Amministrazione" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="Anno" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TipologiaDecreto" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="TipoDecreto" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="NumeroDecreto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="UfficioCreatore" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="StatoFascicoloAttoDecreto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StatoAcquisizione" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}statoAcquisizione_type"/>
 *         &lt;element name="DataAcquisizione" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fascicoloMetadataAttoDecretoCorte_type", propOrder = {
    "identificativoAttoDecreto",
    "titolo",
    "amministrazione",
    "anno",
    "tipologiaDecreto",
    "tipoDecreto",
    "numeroDecreto",
    "ufficioCreatore",
    "statoFascicoloAttoDecreto",
    "statoAcquisizione",
    "dataAcquisizione"
})
public class FascicoloMetadataAttoDecretoCorteType {

    @XmlElement(name = "IdentificativoAttoDecreto", required = true)
    protected String identificativoAttoDecreto;
    @XmlElement(name = "Titolo", required = true)
    protected CodiceDescrizioneType titolo;
    @XmlElement(name = "Amministrazione", required = true)
    protected CodiceDescrizioneType amministrazione;
    @XmlElement(name = "Anno")
    protected int anno;
    @XmlElement(name = "TipologiaDecreto", required = true)
    protected CodiceDescrizioneType tipologiaDecreto;
    @XmlElement(name = "TipoDecreto", required = true)
    protected CodiceDescrizioneType tipoDecreto;
    @XmlElement(name = "NumeroDecreto")
    protected int numeroDecreto;
    @XmlElement(name = "UfficioCreatore", required = true)
    protected CodiceDescrizioneType ufficioCreatore;
    @XmlElement(name = "StatoFascicoloAttoDecreto", required = true)
    protected String statoFascicoloAttoDecreto;
    @XmlElement(name = "StatoAcquisizione", required = true)
    protected StatoAcquisizioneType statoAcquisizione;
    @XmlElement(name = "DataAcquisizione")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataAcquisizione;

    /**
     * Gets the value of the identificativoAttoDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificativoAttoDecreto() {
        return identificativoAttoDecreto;
    }

    /**
     * Sets the value of the identificativoAttoDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificativoAttoDecreto(String value) {
        this.identificativoAttoDecreto = value;
    }

    /**
     * Gets the value of the titolo property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getTitolo() {
        return titolo;
    }

    /**
     * Sets the value of the titolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setTitolo(CodiceDescrizioneType value) {
        this.titolo = value;
    }

    /**
     * Gets the value of the amministrazione property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getAmministrazione() {
        return amministrazione;
    }

    /**
     * Sets the value of the amministrazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setAmministrazione(CodiceDescrizioneType value) {
        this.amministrazione = value;
    }

    /**
     * Gets the value of the anno property.
     * 
     */
    public int getAnno() {
        return anno;
    }

    /**
     * Sets the value of the anno property.
     * 
     */
    public void setAnno(int value) {
        this.anno = value;
    }

    /**
     * Gets the value of the tipologiaDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getTipologiaDecreto() {
        return tipologiaDecreto;
    }

    /**
     * Sets the value of the tipologiaDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setTipologiaDecreto(CodiceDescrizioneType value) {
        this.tipologiaDecreto = value;
    }

    /**
     * Gets the value of the tipoDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getTipoDecreto() {
        return tipoDecreto;
    }

    /**
     * Sets the value of the tipoDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setTipoDecreto(CodiceDescrizioneType value) {
        this.tipoDecreto = value;
    }

    /**
     * Gets the value of the numeroDecreto property.
     * 
     */
    public int getNumeroDecreto() {
        return numeroDecreto;
    }

    /**
     * Sets the value of the numeroDecreto property.
     * 
     */
    public void setNumeroDecreto(int value) {
        this.numeroDecreto = value;
    }

    /**
     * Gets the value of the ufficioCreatore property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getUfficioCreatore() {
        return ufficioCreatore;
    }

    /**
     * Sets the value of the ufficioCreatore property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setUfficioCreatore(CodiceDescrizioneType value) {
        this.ufficioCreatore = value;
    }

    /**
     * Gets the value of the statoFascicoloAttoDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatoFascicoloAttoDecreto() {
        return statoFascicoloAttoDecreto;
    }

    /**
     * Sets the value of the statoFascicoloAttoDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatoFascicoloAttoDecreto(String value) {
        this.statoFascicoloAttoDecreto = value;
    }

    /**
     * Gets the value of the statoAcquisizione property.
     * 
     * @return
     *     possible object is
     *     {@link StatoAcquisizioneType }
     *     
     */
    public StatoAcquisizioneType getStatoAcquisizione() {
        return statoAcquisizione;
    }

    /**
     * Sets the value of the statoAcquisizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatoAcquisizioneType }
     *     
     */
    public void setStatoAcquisizione(StatoAcquisizioneType value) {
        this.statoAcquisizione = value;
    }

    /**
     * Gets the value of the dataAcquisizione property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAcquisizione() {
        return dataAcquisizione;
    }

    /**
     * Sets the value of the dataAcquisizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAcquisizione(XMLGregorianCalendar value) {
        this.dataAcquisizione = value;
    }

}
