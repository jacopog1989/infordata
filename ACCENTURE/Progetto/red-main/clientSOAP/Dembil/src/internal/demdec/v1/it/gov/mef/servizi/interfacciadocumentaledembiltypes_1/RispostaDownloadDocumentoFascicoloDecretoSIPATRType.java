
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Content del Documento del Fascicolo Decreto SIPATR
 * 
 * <p>Java class for risposta_downloadDocumentoFascicoloDecretoSIPATR_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="risposta_downloadDocumentoFascicoloDecretoSIPATR_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}BaseServiceResponseType">
 *       &lt;sequence>
 *         &lt;element name="DocumentoContent" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}documentoContent_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_downloadDocumentoFascicoloDecretoSIPATR_type", propOrder = {
    "documentoContent"
})
public class RispostaDownloadDocumentoFascicoloDecretoSIPATRType
    extends BaseServiceResponseType
{

    @XmlElement(name = "DocumentoContent")
    protected DocumentoContentType documentoContent;

    /**
     * Gets the value of the documentoContent property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoContentType }
     *     
     */
    public DocumentoContentType getDocumentoContent() {
        return documentoContent;
    }

    /**
     * Sets the value of the documentoContent property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoContentType }
     *     
     */
    public void setDocumentoContent(DocumentoContentType value) {
        this.documentoContent = value;
    }

}
