
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Cambio stato fascicolo SIPATR
 * 
 * <p>Java class for richiesta_changeStatoFascicoloSIPATR_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_changeStatoFascicoloSIPATR_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloSIPATR" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid" maxOccurs="unbounded"/>
 *         &lt;element name="StatoFascicoloDocumentale" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}statoFascicoloDocumentale_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_changeStatoFascicoloSIPATR_type", propOrder = {
    "idFascicoloSIPATR",
    "statoFascicoloDocumentale"
})
public class RichiestaChangeStatoFascicoloSIPATRType {

    @XmlElement(name = "IdFascicoloSIPATR", required = true)
    protected List<String> idFascicoloSIPATR;
    @XmlElement(name = "StatoFascicoloDocumentale", required = true)
    protected StatoFascicoloDocumentaleType statoFascicoloDocumentale;

    /**
     * Gets the value of the idFascicoloSIPATR property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the idFascicoloSIPATR property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdFascicoloSIPATR().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getIdFascicoloSIPATR() {
        if (idFascicoloSIPATR == null) {
            idFascicoloSIPATR = new ArrayList<String>();
        }
        return this.idFascicoloSIPATR;
    }

    /**
     * Gets the value of the statoFascicoloDocumentale property.
     * 
     * @return
     *     possible object is
     *     {@link StatoFascicoloDocumentaleType }
     *     
     */
    public StatoFascicoloDocumentaleType getStatoFascicoloDocumentale() {
        return statoFascicoloDocumentale;
    }

    /**
     * Sets the value of the statoFascicoloDocumentale property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatoFascicoloDocumentaleType }
     *     
     */
    public void setStatoFascicoloDocumentale(StatoFascicoloDocumentaleType value) {
        this.statoFascicoloDocumentale = value;
    }

}
