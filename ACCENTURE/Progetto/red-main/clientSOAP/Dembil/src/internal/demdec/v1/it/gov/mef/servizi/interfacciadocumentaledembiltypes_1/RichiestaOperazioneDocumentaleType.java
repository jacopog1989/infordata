
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Elenco di Tipologia di Documenti Inseribili nel Fasciolo
 * 
 * <p>Java class for richiestaOperazioneDocumentale_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiestaOperazioneDocumentale_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoOperazione" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoOperazione_type"/>
 *         &lt;element name="ParametriOperazione" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Chiave" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Valore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="IdDocumentoOperazione" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiestaOperazioneDocumentale_type", propOrder = {
    "tipoOperazione",
    "parametriOperazione",
    "idDocumentoOperazione"
})
public class RichiestaOperazioneDocumentaleType {

    @XmlElement(name = "TipoOperazione", required = true)
    protected TipoOperazioneType tipoOperazione;
    @XmlElement(name = "ParametriOperazione")
    protected List<RichiestaOperazioneDocumentaleType.ParametriOperazione> parametriOperazione;
    @XmlElement(name = "IdDocumentoOperazione")
    protected String idDocumentoOperazione;

    /**
     * Gets the value of the tipoOperazione property.
     * 
     * @return
     *     possible object is
     *     {@link TipoOperazioneType }
     *     
     */
    public TipoOperazioneType getTipoOperazione() {
        return tipoOperazione;
    }

    /**
     * Sets the value of the tipoOperazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoOperazioneType }
     *     
     */
    public void setTipoOperazione(TipoOperazioneType value) {
        this.tipoOperazione = value;
    }

    /**
     * Gets the value of the parametriOperazione property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the parametriOperazione property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParametriOperazione().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RichiestaOperazioneDocumentaleType.ParametriOperazione }
     * 
     * 
     */
    public List<RichiestaOperazioneDocumentaleType.ParametriOperazione> getParametriOperazione() {
        if (parametriOperazione == null) {
            parametriOperazione = new ArrayList<RichiestaOperazioneDocumentaleType.ParametriOperazione>();
        }
        return this.parametriOperazione;
    }

    /**
     * Gets the value of the idDocumentoOperazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoOperazione() {
        return idDocumentoOperazione;
    }

    /**
     * Sets the value of the idDocumentoOperazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoOperazione(String value) {
        this.idDocumentoOperazione = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Chiave" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Valore" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "chiave",
        "valore"
    })
    public static class ParametriOperazione {

        @XmlElement(name = "Chiave", required = true)
        protected String chiave;
        @XmlElement(name = "Valore", required = true)
        protected String valore;

        /**
         * Gets the value of the chiave property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChiave() {
            return chiave;
        }

        /**
         * Sets the value of the chiave property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChiave(String value) {
            this.chiave = value;
        }

        /**
         * Gets the value of the valore property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValore() {
            return valore;
        }

        /**
         * Sets the value of the valore property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValore(String value) {
            this.valore = value;
        }

    }

}
