
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiesta monitoraggi dei documenti
 * 
 * <p>Java class for richiesta_getMonitoraggioDocumenti_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_getMonitoraggioDocumenti_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoMonitoraggio" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoMonitoraggio_type"/>
 *         &lt;element name="IdFascicolo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid" minOccurs="0"/>
 *         &lt;element name="TipoFascicolo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoFascicolo_type"/>
 *         &lt;element name="TipoDocumento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoDocumento_type" minOccurs="0"/>
 *         &lt;element name="DataDocumento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}dataRange_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_getMonitoraggioDocumenti_type", propOrder = {
    "tipoMonitoraggio",
    "idFascicolo",
    "tipoFascicolo",
    "tipoDocumento",
    "dataDocumento"
})
public class RichiestaGetMonitoraggioDocumentiType {

    @XmlElement(name = "TipoMonitoraggio", required = true)
    protected TipoMonitoraggioType tipoMonitoraggio;
    @XmlElement(name = "IdFascicolo")
    protected String idFascicolo;
    @XmlElement(name = "TipoFascicolo", required = true)
    protected TipoFascicoloType tipoFascicolo;
    @XmlElement(name = "TipoDocumento")
    protected TipoDocumentoType tipoDocumento;
    @XmlElement(name = "DataDocumento")
    protected DataRangeType dataDocumento;

    /**
     * Gets the value of the tipoMonitoraggio property.
     * 
     * @return
     *     possible object is
     *     {@link TipoMonitoraggioType }
     *     
     */
    public TipoMonitoraggioType getTipoMonitoraggio() {
        return tipoMonitoraggio;
    }

    /**
     * Sets the value of the tipoMonitoraggio property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoMonitoraggioType }
     *     
     */
    public void setTipoMonitoraggio(TipoMonitoraggioType value) {
        this.tipoMonitoraggio = value;
    }

    /**
     * Gets the value of the idFascicolo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicolo() {
        return idFascicolo;
    }

    /**
     * Sets the value of the idFascicolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicolo(String value) {
        this.idFascicolo = value;
    }

    /**
     * Gets the value of the tipoFascicolo property.
     * 
     * @return
     *     possible object is
     *     {@link TipoFascicoloType }
     *     
     */
    public TipoFascicoloType getTipoFascicolo() {
        return tipoFascicolo;
    }

    /**
     * Sets the value of the tipoFascicolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoFascicoloType }
     *     
     */
    public void setTipoFascicolo(TipoFascicoloType value) {
        this.tipoFascicolo = value;
    }

    /**
     * Gets the value of the tipoDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link TipoDocumentoType }
     *     
     */
    public TipoDocumentoType getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Sets the value of the tipoDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDocumentoType }
     *     
     */
    public void setTipoDocumento(TipoDocumentoType value) {
        this.tipoDocumento = value;
    }

    /**
     * Gets the value of the dataDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link DataRangeType }
     *     
     */
    public DataRangeType getDataDocumento() {
        return dataDocumento;
    }

    /**
     * Sets the value of the dataDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataRangeType }
     *     
     */
    public void setDataDocumento(DataRangeType value) {
        this.dataDocumento = value;
    }

}
