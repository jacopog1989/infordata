
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiesta metadati dati del Fascicolo Decreto SIPATR
 * 
 * <p>Java class for richiesta_getFascicoloSIPATRAttoDecreto_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_getFascicoloSIPATRAttoDecreto_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloAttoDecreto" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="IdFascicoloSIPATR" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="TipoEstrazioneFascicolo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoEstrazione_type"/>
 *         &lt;element name="DocumentCriteria" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}criteriaDocumentiSIPATR_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_getFascicoloSIPATRAttoDecreto_type", propOrder = {
    "idFascicoloAttoDecreto",
    "idFascicoloSIPATR",
    "tipoEstrazioneFascicolo",
    "documentCriteria"
})
public class RichiestaGetFascicoloSIPATRAttoDecretoType {

    @XmlElement(name = "IdFascicoloAttoDecreto", required = true)
    protected String idFascicoloAttoDecreto;
    @XmlElement(name = "IdFascicoloSIPATR", required = true)
    protected String idFascicoloSIPATR;
    @XmlElement(name = "TipoEstrazioneFascicolo", required = true, defaultValue = "DATA")
    protected TipoEstrazioneType tipoEstrazioneFascicolo;
    @XmlElement(name = "DocumentCriteria")
    protected CriteriaDocumentiSIPATRType documentCriteria;

    /**
     * Gets the value of the idFascicoloAttoDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloAttoDecreto() {
        return idFascicoloAttoDecreto;
    }

    /**
     * Sets the value of the idFascicoloAttoDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloAttoDecreto(String value) {
        this.idFascicoloAttoDecreto = value;
    }

    /**
     * Gets the value of the idFascicoloSIPATR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloSIPATR() {
        return idFascicoloSIPATR;
    }

    /**
     * Sets the value of the idFascicoloSIPATR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloSIPATR(String value) {
        this.idFascicoloSIPATR = value;
    }

    /**
     * Gets the value of the tipoEstrazioneFascicolo property.
     * 
     * @return
     *     possible object is
     *     {@link TipoEstrazioneType }
     *     
     */
    public TipoEstrazioneType getTipoEstrazioneFascicolo() {
        return tipoEstrazioneFascicolo;
    }

    /**
     * Sets the value of the tipoEstrazioneFascicolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEstrazioneType }
     *     
     */
    public void setTipoEstrazioneFascicolo(TipoEstrazioneType value) {
        this.tipoEstrazioneFascicolo = value;
    }

    /**
     * Gets the value of the documentCriteria property.
     * 
     * @return
     *     possible object is
     *     {@link CriteriaDocumentiSIPATRType }
     *     
     */
    public CriteriaDocumentiSIPATRType getDocumentCriteria() {
        return documentCriteria;
    }

    /**
     * Sets the value of the documentCriteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link CriteriaDocumentiSIPATRType }
     *     
     */
    public void setDocumentCriteria(CriteriaDocumentiSIPATRType value) {
        this.documentCriteria = value;
    }

}
