
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Metadati del Fascicolo Raccolta Provvisoria
 * 
 * <p>Java class for fascicoloMetadataRaccoltaProvvisoria_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="fascicoloMetadataRaccoltaProvvisoria_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdentificativoRaccoltaProvvisoria" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Ragioneria" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="Amministrazione" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="UfficioCreatoreRED" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="TipoFlusso" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoFlusso_type"/>
 *         &lt;element name="Protocollo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}protocollo_type" maxOccurs="unbounded"/>
 *         &lt;element name="UtenteCreatore" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="StatoFascicoloRaccoltaProvvisoria" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Metadata" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="VersioneMetadata" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fascicoloMetadataRaccoltaProvvisoria_type", propOrder = {
    "identificativoRaccoltaProvvisoria",
    "ragioneria",
    "amministrazione",
    "ufficioCreatoreRED",
    "tipoFlusso",
    "protocollo",
    "utenteCreatore",
    "statoFascicoloRaccoltaProvvisoria",
    "metadata",
    "versioneMetadata"
})
public class FascicoloMetadataRaccoltaProvvisoriaType {

    @XmlElement(name = "IdentificativoRaccoltaProvvisoria", required = true)
    protected String identificativoRaccoltaProvvisoria;
    @XmlElement(name = "Ragioneria", required = true)
    protected CodiceDescrizioneType ragioneria;
    @XmlElement(name = "Amministrazione", required = true)
    protected CodiceDescrizioneType amministrazione;
    @XmlElement(name = "UfficioCreatoreRED", required = true)
    protected CodiceDescrizioneType ufficioCreatoreRED;
    @XmlElement(name = "TipoFlusso", required = true)
    protected TipoFlussoType tipoFlusso;
    @XmlElement(name = "Protocollo", required = true)
    protected List<ProtocolloType> protocollo;
    @XmlElement(name = "UtenteCreatore", required = true)
    protected CodiceDescrizioneType utenteCreatore;
    @XmlElement(name = "StatoFascicoloRaccoltaProvvisoria", required = true)
    protected String statoFascicoloRaccoltaProvvisoria;
    @XmlElement(name = "Metadata")
    protected byte[] metadata;
    @XmlElement(name = "VersioneMetadata")
    protected String versioneMetadata;

    /**
     * Gets the value of the identificativoRaccoltaProvvisoria property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificativoRaccoltaProvvisoria() {
        return identificativoRaccoltaProvvisoria;
    }

    /**
     * Sets the value of the identificativoRaccoltaProvvisoria property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificativoRaccoltaProvvisoria(String value) {
        this.identificativoRaccoltaProvvisoria = value;
    }

    /**
     * Gets the value of the ragioneria property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getRagioneria() {
        return ragioneria;
    }

    /**
     * Sets the value of the ragioneria property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setRagioneria(CodiceDescrizioneType value) {
        this.ragioneria = value;
    }

    /**
     * Gets the value of the amministrazione property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getAmministrazione() {
        return amministrazione;
    }

    /**
     * Sets the value of the amministrazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setAmministrazione(CodiceDescrizioneType value) {
        this.amministrazione = value;
    }

    /**
     * Gets the value of the ufficioCreatoreRED property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getUfficioCreatoreRED() {
        return ufficioCreatoreRED;
    }

    /**
     * Sets the value of the ufficioCreatoreRED property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setUfficioCreatoreRED(CodiceDescrizioneType value) {
        this.ufficioCreatoreRED = value;
    }

    /**
     * Gets the value of the tipoFlusso property.
     * 
     * @return
     *     possible object is
     *     {@link TipoFlussoType }
     *     
     */
    public TipoFlussoType getTipoFlusso() {
        return tipoFlusso;
    }

    /**
     * Sets the value of the tipoFlusso property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoFlussoType }
     *     
     */
    public void setTipoFlusso(TipoFlussoType value) {
        this.tipoFlusso = value;
    }

    /**
     * Gets the value of the protocollo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the protocollo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProtocollo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtocolloType }
     * 
     * 
     */
    public List<ProtocolloType> getProtocollo() {
        if (protocollo == null) {
            protocollo = new ArrayList<ProtocolloType>();
        }
        return this.protocollo;
    }

    /**
     * Gets the value of the utenteCreatore property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getUtenteCreatore() {
        return utenteCreatore;
    }

    /**
     * Sets the value of the utenteCreatore property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setUtenteCreatore(CodiceDescrizioneType value) {
        this.utenteCreatore = value;
    }

    /**
     * Gets the value of the statoFascicoloRaccoltaProvvisoria property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatoFascicoloRaccoltaProvvisoria() {
        return statoFascicoloRaccoltaProvvisoria;
    }

    /**
     * Sets the value of the statoFascicoloRaccoltaProvvisoria property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatoFascicoloRaccoltaProvvisoria(String value) {
        this.statoFascicoloRaccoltaProvvisoria = value;
    }

    /**
     * Gets the value of the metadata property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getMetadata() {
        return metadata;
    }

    /**
     * Sets the value of the metadata property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setMetadata(byte[] value) {
        this.metadata = value;
    }

    /**
     * Gets the value of the versioneMetadata property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersioneMetadata() {
        return versioneMetadata;
    }

    /**
     * Sets the value of the versioneMetadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersioneMetadata(String value) {
        this.versioneMetadata = value;
    }

}
