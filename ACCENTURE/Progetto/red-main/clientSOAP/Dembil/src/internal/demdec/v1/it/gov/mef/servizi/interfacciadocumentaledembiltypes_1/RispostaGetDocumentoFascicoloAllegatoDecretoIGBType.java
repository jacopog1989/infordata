
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Dati del Documento del sotto fascicolo AllegatoDecretoIGB
 * 
 * <p>Java class for risposta_getDocumentoFascicoloAllegatoDecretoIGB_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="risposta_getDocumentoFascicoloAllegatoDecretoIGB_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}BaseServiceResponseType">
 *       &lt;sequence>
 *         &lt;element name="DettaglioDocumento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}documentoFascicoloAllegatoDecretoIGB_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_getDocumentoFascicoloAllegatoDecretoIGB_type", propOrder = {
    "dettaglioDocumento"
})
public class RispostaGetDocumentoFascicoloAllegatoDecretoIGBType
    extends BaseServiceResponseType
{

    @XmlElement(name = "DettaglioDocumento")
    protected DocumentoFascicoloAllegatoDecretoIGBType dettaglioDocumento;

    /**
     * Gets the value of the dettaglioDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoFascicoloAllegatoDecretoIGBType }
     *     
     */
    public DocumentoFascicoloAllegatoDecretoIGBType getDettaglioDocumento() {
        return dettaglioDocumento;
    }

    /**
     * Sets the value of the dettaglioDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoFascicoloAllegatoDecretoIGBType }
     *     
     */
    public void setDettaglioDocumento(DocumentoFascicoloAllegatoDecretoIGBType value) {
        this.dettaglioDocumento = value;
    }

}
