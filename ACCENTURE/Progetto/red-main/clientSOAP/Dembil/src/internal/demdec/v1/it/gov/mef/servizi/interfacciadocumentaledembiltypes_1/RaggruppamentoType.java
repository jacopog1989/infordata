
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Permette il sort dei documenti nel fascicolo per Gruppo e ordinamento
 * 
 * <p>Java class for raggruppamentoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="raggruppamentoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CodiceGruppo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="PosizioneGruppo" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="PosizioneDocumento" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="PosizioneDocumentoPadre" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "raggruppamentoType", propOrder = {
    "codiceGruppo",
    "posizioneGruppo",
    "posizioneDocumento",
    "posizioneDocumentoPadre"
})
public class RaggruppamentoType {

    @XmlElement(name = "CodiceGruppo", required = true)
    protected CodiceDescrizioneType codiceGruppo;
    @XmlElement(name = "PosizioneGruppo")
    protected short posizioneGruppo;
    @XmlElement(name = "PosizioneDocumento")
    protected short posizioneDocumento;
    @XmlElement(name = "PosizioneDocumentoPadre")
    protected Short posizioneDocumentoPadre;

    /**
     * Gets the value of the codiceGruppo property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getCodiceGruppo() {
        return codiceGruppo;
    }

    /**
     * Sets the value of the codiceGruppo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setCodiceGruppo(CodiceDescrizioneType value) {
        this.codiceGruppo = value;
    }

    /**
     * Gets the value of the posizioneGruppo property.
     * 
     */
    public short getPosizioneGruppo() {
        return posizioneGruppo;
    }

    /**
     * Sets the value of the posizioneGruppo property.
     * 
     */
    public void setPosizioneGruppo(short value) {
        this.posizioneGruppo = value;
    }

    /**
     * Gets the value of the posizioneDocumento property.
     * 
     */
    public short getPosizioneDocumento() {
        return posizioneDocumento;
    }

    /**
     * Sets the value of the posizioneDocumento property.
     * 
     */
    public void setPosizioneDocumento(short value) {
        this.posizioneDocumento = value;
    }

    /**
     * Gets the value of the posizioneDocumentoPadre property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getPosizioneDocumentoPadre() {
        return posizioneDocumentoPadre;
    }

    /**
     * Sets the value of the posizioneDocumentoPadre property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setPosizioneDocumentoPadre(Short value) {
        this.posizioneDocumentoPadre = value;
    }

}
