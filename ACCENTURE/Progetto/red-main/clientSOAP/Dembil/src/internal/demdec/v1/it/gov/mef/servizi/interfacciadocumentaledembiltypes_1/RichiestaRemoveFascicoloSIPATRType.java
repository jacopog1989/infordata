
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiede l'eliminazione del Fascicolo SIPATR
 * 
 * <p>Java class for richiesta_removeFascicoloSIPATR_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_removeFascicoloSIPATR_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloSIPATR" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_removeFascicoloSIPATR_type", propOrder = {
    "idFascicoloSIPATR"
})
public class RichiestaRemoveFascicoloSIPATRType {

    @XmlElement(name = "IdFascicoloSIPATR", required = true)
    protected String idFascicoloSIPATR;

    /**
     * Gets the value of the idFascicoloSIPATR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloSIPATR() {
        return idFascicoloSIPATR;
    }

    /**
     * Sets the value of the idFascicoloSIPATR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloSIPATR(String value) {
        this.idFascicoloSIPATR = value;
    }

}
