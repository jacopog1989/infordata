
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for validazioneFirma_type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="validazioneFirma_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NON_VALIDATA"/>
 *     &lt;enumeration value="VALIDATA_CON_SUCCESSO"/>
 *     &lt;enumeration value="VALIDATA_CON_ERRORE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "validazioneFirma_type")
@XmlEnum
public enum ValidazioneFirmaType {

    NON_VALIDATA,
    VALIDATA_CON_SUCCESSO,
    VALIDATA_CON_ERRORE;

    public String value() {
        return name();
    }

    public static ValidazioneFirmaType fromValue(String v) {
        return valueOf(v);
    }

}
