
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import internal.demdec.v1.it.gov.mef.servizi.common.headeraccessoapplicativo.AccessoApplicativo;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaAddCollegamentoFascicoloSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaAddDocumentoAggiuntivoFascicoloDecretoSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaAddDocumentoFascicoloSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaChangeStatoFascicoloDecretoSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaChangeStatoFascicoloSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaCreateFascicoloDecretoSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaCreateFascicoloSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaDownloadDocumentoFascicoloSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaGetDocumentoFascicoloSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaGetElencoDocumentiFascicoloType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaGetFascicoloDecretoSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaGetFascicoloSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaGetMonitoraggioDocumentiType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaGetRisultatoOperazioneDocumentoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaModifyMetadataDocumentoFascicoloSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaModifyMetadataFascicoloDecretoSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaModifyMetadataFascicoloSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaRemoveCollegamentoFascicoloSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaRemoveDocumentoAggiuntivoFascicoloDecretoSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaRemoveDocumentoFascicoloSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaRemoveFascicoloDecretoSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaRemoveFascicoloSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaAddCollegamentoFascicoloSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaAddDocumentoAggiuntivoFascicoloDecretoSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaAddDocumentoFascicoloSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaChangeStatoFascicoloDecretoSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaChangeStatoFascicoloSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaCreateFascicoloDecretoSIPATR;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaCreateFascicoloSIPATR;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaDownloadDocumentoFascicoloSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetDocumentoFascicoloSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetElencoDocumentiFascicoloType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetFascicoloDecretoSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetFascicoloSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetMonitoraggioDocumentiType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetRisultatoOperazioneDocumento;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaModifyMetadataDocumentoFascicoloSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaModifyMetadataFascicoloDecretoSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaModifyMetadataFascicoloSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaRemoveCollegamentoFascicoloSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaRemoveDocumentoAggiuntivoFascicoloSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaRemoveDocumentoFascicoloSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaRemoveFascicoloDecretoSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaRemoveFascicoloSIPATRType;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebService(name = "InterfacciaSIPATRDEMBIL", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBIL")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.ObjectFactory.class,
    internal.demdec.v1.it.gov.mef.servizi.common.headeraccessoapplicativo.ObjectFactory.class,
    internal.demdec.v1.it.gov.mef.servizi.common.headerfault.ObjectFactory.class,
    org.xmlsoap.schemas.soap.envelope.ObjectFactory.class
})
public interface InterfacciaSIPATRDEMBIL {


    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaCreateFascicoloDecretoSIPATR
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:createFascicoloDecretoSIPATR")
    @WebResult(name = "risposta_createFascicoloDecretoSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaCreateFascicoloDecretoSIPATR createFascicoloDecretoSIPATR(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_createFascicoloDecretoSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaCreateFascicoloDecretoSIPATRType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetFascicoloDecretoSIPATRType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:getFascicoloDecretoSIPATR")
    @WebResult(name = "risposta_getFascicoloDecretoSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaGetFascicoloDecretoSIPATRType getFascicoloDecretoSIPATR(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_getFascicoloDecretoSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaGetFascicoloDecretoSIPATRType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaAddDocumentoAggiuntivoFascicoloDecretoSIPATRType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:addDocumentoAggiuntivoFascicoloDecretoSIPATR")
    @WebResult(name = "risposta_addDocumentoAggiuntivoFascicoloDecretoSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaAddDocumentoAggiuntivoFascicoloDecretoSIPATRType addDocumentoAggiuntivoFascicoloDecretoSIPATR(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_addDocumentoAggiuntivoFascicoloDecretoSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaAddDocumentoAggiuntivoFascicoloDecretoSIPATRType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:getDocumentoAggiuntivoFascicoloDecretoSIPATR")
    @WebResult(name = "risposta_getDocumentoAggiuntivoFascicoloDecretoSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType getDocumentoAggiuntivoFascicoloDecretoSIPATR(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_getDocumentoAggiuntivoFascicoloDecretoSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaDownloadDocumentoFascicoloSIPATRType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:downloadDocumentoAggiuntivoFascicoloDecretoSIPATR")
    @WebResult(name = "risposta_downloadDocumentoAggiuntivoFascicoloDecretoSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaDownloadDocumentoFascicoloSIPATRType downloadDocumentoAggiuntivoFascicoloDecretoSIPATR(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_downloadDocumentoAggiuntivoFascicoloDecretoSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaDownloadDocumentoFascicoloSIPATRType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaRemoveDocumentoAggiuntivoFascicoloSIPATRType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:removeDocumentoAggiuntivoFascicoloDecretoSIPATR")
    @WebResult(name = "risposta_removeDocumentoAggiuntivoFascicoloDecretoSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaRemoveDocumentoAggiuntivoFascicoloSIPATRType removeDocumentoAggiuntivoFascicoloDecretoSIPATR(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_removeDocumentoAggiuntivoFascicoloDecretoSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaRemoveDocumentoAggiuntivoFascicoloDecretoSIPATRType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaChangeStatoFascicoloDecretoSIPATRType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:changeStatoFascicoloDecretoSIPATR")
    @WebResult(name = "risposta_changeStatoFascicoloDecretoSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaChangeStatoFascicoloDecretoSIPATRType changeStatoFascicoloDecretoSIPATR(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_changeStatoFascicoloDecretoSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaChangeStatoFascicoloDecretoSIPATRType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaModifyMetadataFascicoloDecretoSIPATRType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:modifyMetadataFascicoloDecretoSIPATR")
    @WebResult(name = "risposta_modifyMetadataFascicoloDecretoSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaModifyMetadataFascicoloDecretoSIPATRType modifyMetadataFascicoloDecretoSIPATR(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_modifyMetadataFascicoloDecretoSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaModifyMetadataFascicoloDecretoSIPATRType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaRemoveFascicoloDecretoSIPATRType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:removeFascicoloDecretoSIPATR")
    @WebResult(name = "risposta_removeFascicoloDecretoSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaRemoveFascicoloDecretoSIPATRType removeFascicoloDecretoSIPATR(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_removeFascicoloDecretoSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaRemoveFascicoloDecretoSIPATRType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetElencoDocumentiFascicoloType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:getElencoDocumentiDecretoSIPATR")
    @WebResult(name = "risposta_getElencoDocumentiFascicolo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaGetElencoDocumentiFascicoloType getElencoDocumentiDecretoSIPATR(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_getElencoDocumentiFascicolo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaGetElencoDocumentiFascicoloType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetMonitoraggioDocumentiType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:monitoraggioFascicoloDecretoSIPATR")
    @WebResult(name = "risposta_monitoraggioDocumenti", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaGetMonitoraggioDocumentiType monitoraggioFascicoloDecretoSIPATR(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_monitoraggioDocumenti", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaGetMonitoraggioDocumentiType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaCreateFascicoloSIPATR
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:createFascicoloSIPATR")
    @WebResult(name = "risposta_createFascicoloSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaCreateFascicoloSIPATR createFascicoloSIPATR(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_createFascicoloSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaCreateFascicoloSIPATRType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetFascicoloSIPATRType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:getFascicoloSIPATR")
    @WebResult(name = "risposta_getFascicoloSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaGetFascicoloSIPATRType getFascicoloSIPATR(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_getFascicoloSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaGetFascicoloSIPATRType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaChangeStatoFascicoloSIPATRType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:changeStatoFascicoloSIPATR")
    @WebResult(name = "risposta_changeStatoFascicoloSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaChangeStatoFascicoloSIPATRType changeStatoFascicoloSIPATR(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_changeStatoFascicoloSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaChangeStatoFascicoloSIPATRType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaModifyMetadataFascicoloSIPATRType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:modifyMetadataFascicoloSIPATR")
    @WebResult(name = "risposta_modifyMetadataFascicoloSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaModifyMetadataFascicoloSIPATRType modifyMetadataFascicoloSIPATR(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_modifyMetadataFascicoloSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaModifyMetadataFascicoloSIPATRType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaRemoveFascicoloSIPATRType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:removeFascicoloSIPATR")
    @WebResult(name = "risposta_removeFascicoloSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaRemoveFascicoloSIPATRType removeFascicoloSIPATR(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_removeFascicoloSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaRemoveFascicoloSIPATRType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaAddDocumentoFascicoloSIPATRType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:addDocumentoFascicoloSIPATR")
    @WebResult(name = "risposta_addDocumentoFascicoloSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaAddDocumentoFascicoloSIPATRType addDocumentoFascicoloSIPATR(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_addDocumentoFascicoloSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaAddDocumentoFascicoloSIPATRType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetDocumentoFascicoloSIPATRType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:getDocumentoFascicoloSIPATR")
    @WebResult(name = "risposta_getDocumentoFascicoloSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaGetDocumentoFascicoloSIPATRType getDocumentoFascicoloSIPATR(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_getDocumentoFascicoloSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaGetDocumentoFascicoloSIPATRType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaModifyMetadataDocumentoFascicoloSIPATRType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:modifyMetadataDocumentoFascicoloSIPATR")
    @WebResult(name = "risposta_modifyMetadataDocumentoFascicoloSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaModifyMetadataDocumentoFascicoloSIPATRType modifyMetadataDocumentoFascicoloSIPATR(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_modifyMetadataDocumentoFascicoloSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaModifyMetadataDocumentoFascicoloSIPATRType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaDownloadDocumentoFascicoloSIPATRType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:downloadDocumentoFascicoloSIPATR")
    @WebResult(name = "risposta_downloadDocumentoFascicoloSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaDownloadDocumentoFascicoloSIPATRType downloadDocumentoFascicoloSIPATR(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_downloadDocumentoFascicoloSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaDownloadDocumentoFascicoloSIPATRType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaRemoveDocumentoFascicoloSIPATRType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:removeDocumentoFascicoloSIPATR")
    @WebResult(name = "risposta_removeDocumentoFascicoloSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaRemoveDocumentoFascicoloSIPATRType removeDocumentoFascicoloSIPATR(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_removeDocumentoFascicoloSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaRemoveDocumentoFascicoloSIPATRType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaAddCollegamentoFascicoloSIPATRType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:addCollegamentoFascicoloSIPATR")
    @WebResult(name = "risposta_addCollegamentoFascicoloSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaAddCollegamentoFascicoloSIPATRType addCollegamentoFascicoloSIPATR(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_addCollegamentoFascicoloSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaAddCollegamentoFascicoloSIPATRType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaRemoveCollegamentoFascicoloSIPATRType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:removeCollegamentoFascicoloSIPATR")
    @WebResult(name = "risposta_removeCollegamentoFascicoloSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaRemoveCollegamentoFascicoloSIPATRType removeCollegamentoFascicoloSIPATR(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_removeCollegamentoFascicoloSIPATR", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaRemoveCollegamentoFascicoloSIPATRType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetRisultatoOperazioneDocumento
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:getRisultatoOperazioneDocumento")
    @WebResult(name = "risposta_getRisultatoOperazioneDocumento", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaGetRisultatoOperazioneDocumento getRisultatoOperazioneDocumento(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_getRisultatoOperazioneDocumento", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaGetRisultatoOperazioneDocumentoType parameters)
        throws GenericFault, SecurityFault
    ;

}
