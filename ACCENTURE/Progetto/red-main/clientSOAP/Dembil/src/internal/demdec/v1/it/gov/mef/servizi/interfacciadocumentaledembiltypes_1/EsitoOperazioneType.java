
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for esitoOperazione_type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="esitoOperazione_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="INSERITA"/>
 *     &lt;enumeration value="LAVORAZIONE"/>
 *     &lt;enumeration value="COMPLETA"/>
 *     &lt;enumeration value="ERRORE"/>
 *     &lt;enumeration value="NONCONVERTIBILE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "esitoOperazione_type")
@XmlEnum
public enum EsitoOperazioneType {

    INSERITA,
    LAVORAZIONE,
    COMPLETA,
    ERRORE,
    NONCONVERTIBILE;

    public String value() {
        return name();
    }

    public static EsitoOperazioneType fromValue(String v) {
        return valueOf(v);
    }

}
