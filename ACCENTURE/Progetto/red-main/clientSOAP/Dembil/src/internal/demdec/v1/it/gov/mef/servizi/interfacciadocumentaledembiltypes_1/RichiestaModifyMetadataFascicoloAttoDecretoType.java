
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiede l'aggiornamento dei metadati del Fascicolo Atto Decreto
 * 
 * <p>Java class for richiesta_modifyMetadataFascicoloAttoDecreto_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_modifyMetadataFascicoloAttoDecreto_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloAttoDecreto" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FascicoloMetadata" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}fascicoloMetadataAttoDecreto_type"/>
 *         &lt;element name="DaInviare" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Condivisibile" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Attivo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_modifyMetadataFascicoloAttoDecreto_type", propOrder = {
    "idFascicoloAttoDecreto",
    "descrizione",
    "fascicoloMetadata",
    "daInviare",
    "condivisibile",
    "attivo"
})
public class RichiestaModifyMetadataFascicoloAttoDecretoType {

    @XmlElement(name = "IdFascicoloAttoDecreto", required = true)
    protected String idFascicoloAttoDecreto;
    @XmlElement(name = "Descrizione")
    protected String descrizione;
    @XmlElement(name = "FascicoloMetadata", required = true)
    protected FascicoloMetadataAttoDecretoType fascicoloMetadata;
    @XmlElement(name = "DaInviare", defaultValue = "true")
    protected boolean daInviare;
    @XmlElement(name = "Condivisibile", defaultValue = "true")
    protected boolean condivisibile;
    @XmlElement(name = "Attivo", defaultValue = "true")
    protected boolean attivo;

    /**
     * Gets the value of the idFascicoloAttoDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloAttoDecreto() {
        return idFascicoloAttoDecreto;
    }

    /**
     * Sets the value of the idFascicoloAttoDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloAttoDecreto(String value) {
        this.idFascicoloAttoDecreto = value;
    }

    /**
     * Gets the value of the descrizione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Sets the value of the descrizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Gets the value of the fascicoloMetadata property.
     * 
     * @return
     *     possible object is
     *     {@link FascicoloMetadataAttoDecretoType }
     *     
     */
    public FascicoloMetadataAttoDecretoType getFascicoloMetadata() {
        return fascicoloMetadata;
    }

    /**
     * Sets the value of the fascicoloMetadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link FascicoloMetadataAttoDecretoType }
     *     
     */
    public void setFascicoloMetadata(FascicoloMetadataAttoDecretoType value) {
        this.fascicoloMetadata = value;
    }

    /**
     * Gets the value of the daInviare property.
     * 
     */
    public boolean isDaInviare() {
        return daInviare;
    }

    /**
     * Sets the value of the daInviare property.
     * 
     */
    public void setDaInviare(boolean value) {
        this.daInviare = value;
    }

    /**
     * Gets the value of the condivisibile property.
     * 
     */
    public boolean isCondivisibile() {
        return condivisibile;
    }

    /**
     * Sets the value of the condivisibile property.
     * 
     */
    public void setCondivisibile(boolean value) {
        this.condivisibile = value;
    }

    /**
     * Gets the value of the attivo property.
     * 
     */
    public boolean isAttivo() {
        return attivo;
    }

    /**
     * Sets the value of the attivo property.
     * 
     */
    public void setAttivo(boolean value) {
        this.attivo = value;
    }

}
