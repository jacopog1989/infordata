
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoElenco_type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoElenco_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ALL"/>
 *     &lt;enumeration value="SOURCE"/>
 *     &lt;enumeration value="VISIBLE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoElenco_type")
@XmlEnum
public enum TipoElencoType {

    ALL,
    SOURCE,
    VISIBLE;

    public String value() {
        return name();
    }

    public static TipoElencoType fromValue(String v) {
        return valueOf(v);
    }

}
