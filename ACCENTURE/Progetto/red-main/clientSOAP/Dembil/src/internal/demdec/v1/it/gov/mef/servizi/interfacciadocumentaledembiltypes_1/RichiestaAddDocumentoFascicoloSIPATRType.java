
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiesta di aggiunta del documento al fascicolo SIPATR
 * 
 * <p>Java class for richiesta_addDocumentoFascicoloSIPATR_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_addDocumentoFascicoloSIPATR_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloSIPATR" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="IdDocumento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="TipoDocumento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoDocumento_type"/>
 *         &lt;element name="DocumentoContent" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}documentoContent_type"/>
 *         &lt;element name="DatiDocumento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}documentoMetadataSIPATR_type"/>
 *         &lt;element name="DaInviare" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Condivisibile" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Attivo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Operazione" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}richiestaOperazioneDocumentale_type" minOccurs="0"/>
 *         &lt;element name="Raggruppamento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}raggruppamentoType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_addDocumentoFascicoloSIPATR_type", propOrder = {
    "idFascicoloSIPATR",
    "idDocumento",
    "tipoDocumento",
    "documentoContent",
    "datiDocumento",
    "daInviare",
    "condivisibile",
    "attivo",
    "operazione",
    "raggruppamento"
})
public class RichiestaAddDocumentoFascicoloSIPATRType {

    @XmlElement(name = "IdFascicoloSIPATR", required = true)
    protected String idFascicoloSIPATR;
    @XmlElement(name = "IdDocumento", required = true, nillable = true)
    protected String idDocumento;
    @XmlElement(name = "TipoDocumento", required = true)
    protected TipoDocumentoType tipoDocumento;
    @XmlElement(name = "DocumentoContent", required = true)
    protected DocumentoContentType documentoContent;
    @XmlElement(name = "DatiDocumento", required = true)
    protected DocumentoMetadataSIPATRType datiDocumento;
    @XmlElement(name = "DaInviare", defaultValue = "true")
    protected boolean daInviare;
    @XmlElement(name = "Condivisibile", defaultValue = "true")
    protected boolean condivisibile;
    @XmlElement(name = "Attivo", defaultValue = "true")
    protected boolean attivo;
    @XmlElement(name = "Operazione")
    protected RichiestaOperazioneDocumentaleType operazione;
    @XmlElement(name = "Raggruppamento")
    protected RaggruppamentoType raggruppamento;

    /**
     * Gets the value of the idFascicoloSIPATR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloSIPATR() {
        return idFascicoloSIPATR;
    }

    /**
     * Sets the value of the idFascicoloSIPATR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloSIPATR(String value) {
        this.idFascicoloSIPATR = value;
    }

    /**
     * Gets the value of the idDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumento() {
        return idDocumento;
    }

    /**
     * Sets the value of the idDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumento(String value) {
        this.idDocumento = value;
    }

    /**
     * Gets the value of the tipoDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link TipoDocumentoType }
     *     
     */
    public TipoDocumentoType getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Sets the value of the tipoDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDocumentoType }
     *     
     */
    public void setTipoDocumento(TipoDocumentoType value) {
        this.tipoDocumento = value;
    }

    /**
     * Gets the value of the documentoContent property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoContentType }
     *     
     */
    public DocumentoContentType getDocumentoContent() {
        return documentoContent;
    }

    /**
     * Sets the value of the documentoContent property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoContentType }
     *     
     */
    public void setDocumentoContent(DocumentoContentType value) {
        this.documentoContent = value;
    }

    /**
     * Gets the value of the datiDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoMetadataSIPATRType }
     *     
     */
    public DocumentoMetadataSIPATRType getDatiDocumento() {
        return datiDocumento;
    }

    /**
     * Sets the value of the datiDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoMetadataSIPATRType }
     *     
     */
    public void setDatiDocumento(DocumentoMetadataSIPATRType value) {
        this.datiDocumento = value;
    }

    /**
     * Gets the value of the daInviare property.
     * 
     */
    public boolean isDaInviare() {
        return daInviare;
    }

    /**
     * Sets the value of the daInviare property.
     * 
     */
    public void setDaInviare(boolean value) {
        this.daInviare = value;
    }

    /**
     * Gets the value of the condivisibile property.
     * 
     */
    public boolean isCondivisibile() {
        return condivisibile;
    }

    /**
     * Sets the value of the condivisibile property.
     * 
     */
    public void setCondivisibile(boolean value) {
        this.condivisibile = value;
    }

    /**
     * Gets the value of the attivo property.
     * 
     */
    public boolean isAttivo() {
        return attivo;
    }

    /**
     * Sets the value of the attivo property.
     * 
     */
    public void setAttivo(boolean value) {
        this.attivo = value;
    }

    /**
     * Gets the value of the operazione property.
     * 
     * @return
     *     possible object is
     *     {@link RichiestaOperazioneDocumentaleType }
     *     
     */
    public RichiestaOperazioneDocumentaleType getOperazione() {
        return operazione;
    }

    /**
     * Sets the value of the operazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link RichiestaOperazioneDocumentaleType }
     *     
     */
    public void setOperazione(RichiestaOperazioneDocumentaleType value) {
        this.operazione = value;
    }

    /**
     * Gets the value of the raggruppamento property.
     * 
     * @return
     *     possible object is
     *     {@link RaggruppamentoType }
     *     
     */
    public RaggruppamentoType getRaggruppamento() {
        return raggruppamento;
    }

    /**
     * Sets the value of the raggruppamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link RaggruppamentoType }
     *     
     */
    public void setRaggruppamento(RaggruppamentoType value) {
        this.raggruppamento = value;
    }

}
