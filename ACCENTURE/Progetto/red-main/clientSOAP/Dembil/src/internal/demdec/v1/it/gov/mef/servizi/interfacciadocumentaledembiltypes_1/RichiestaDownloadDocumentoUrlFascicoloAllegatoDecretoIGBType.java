
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiede la url AXWAY per il Download Documento
 * 
 * <p>Java class for richiesta_downloadDocumentoUrlFascicoloAllegatoDecretoIGB_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_downloadDocumentoUrlFascicoloAllegatoDecretoIGB_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloAllegatoDecretoIGB" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="IdDocumento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_downloadDocumentoUrlFascicoloAllegatoDecretoIGB_type", propOrder = {
    "idFascicoloAllegatoDecretoIGB",
    "idDocumento"
})
public class RichiestaDownloadDocumentoUrlFascicoloAllegatoDecretoIGBType {

    @XmlElement(name = "IdFascicoloAllegatoDecretoIGB", required = true)
    protected String idFascicoloAllegatoDecretoIGB;
    @XmlElement(name = "IdDocumento", required = true)
    protected String idDocumento;

    /**
     * Gets the value of the idFascicoloAllegatoDecretoIGB property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloAllegatoDecretoIGB() {
        return idFascicoloAllegatoDecretoIGB;
    }

    /**
     * Sets the value of the idFascicoloAllegatoDecretoIGB property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloAllegatoDecretoIGB(String value) {
        this.idFascicoloAllegatoDecretoIGB = value;
    }

    /**
     * Gets the value of the idDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumento() {
        return idDocumento;
    }

    /**
     * Sets the value of the idDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumento(String value) {
        this.idDocumento = value;
    }

}
