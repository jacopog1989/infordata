
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;


/**
 * Tipo generico 
 * 
 * <p>Java class for risultatoOperazioneDocumento_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="risultatoOperazioneDocumento_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="IdDocumento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="Operazione" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}operazioneDocumentale_type">
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Ordinamento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risultatoOperazioneDocumento_type", propOrder = {
    "idDocumentoAndOperazioneAndOrdinamento"
})
public class RisultatoOperazioneDocumentoType {

    @XmlElements({
        @XmlElement(name = "IdDocumento", required = true, type = String.class),
        @XmlElement(name = "Operazione", required = true, type = RisultatoOperazioneDocumentoType.Operazione.class),
        @XmlElement(name = "Ordinamento", required = true, type = Integer.class)
    })
    protected List<Object> idDocumentoAndOperazioneAndOrdinamento;

    /**
     * Gets the value of the idDocumentoAndOperazioneAndOrdinamento property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the idDocumentoAndOperazioneAndOrdinamento property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdDocumentoAndOperazioneAndOrdinamento().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * {@link RisultatoOperazioneDocumentoType.Operazione }
     * {@link Integer }
     * 
     * 
     */
    public List<Object> getIdDocumentoAndOperazioneAndOrdinamento() {
        if (idDocumentoAndOperazioneAndOrdinamento == null) {
            idDocumentoAndOperazioneAndOrdinamento = new ArrayList<Object>();
        }
        return this.idDocumentoAndOperazioneAndOrdinamento;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}operazioneDocumentale_type">
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Operazione
        extends OperazioneDocumentaleType
    {


    }

}
