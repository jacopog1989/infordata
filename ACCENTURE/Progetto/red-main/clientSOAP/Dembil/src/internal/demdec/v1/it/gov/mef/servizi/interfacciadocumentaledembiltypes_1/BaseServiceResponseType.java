
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Base response type to be used across all operations 
 * 
 * <p>Java class for BaseServiceResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BaseServiceResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Esito" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}esito_type"/>
 *         &lt;element name="ErrorList" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}ServiceErrorType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseServiceResponseType", propOrder = {
    "esito",
    "errorList"
})
@XmlSeeAlso({
    RispostaDownloadDocumentoUrlFascicoloAllegatoDecretoIGBType.class,
    RispostaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType.class,
    RispostaRemoveDocumentoAggiuntivoFascicoloAttoDecretoType.class,
    RispostaRemoveFascicoloAttoDecretoType.class,
    RispostaGetDocumentoFascicoloRaccoltaProvvisoriaType.class,
    RispostaChangeStatoFascicoloSIPATRType.class,
    RispostaGetFascicoloSIPATRAttoDecretoType.class,
    RispostaDownloadDocumentoAggiuntivoFascicoloAttoDecretoType.class,
    RispostaAcquisizioneFascicoloAttoDecretoType.class,
    RispostaGetFascicoloSIPATRType.class,
    RispostaModifyMetadataFascicoloRaccoltaProvvisoriaType.class,
    RispostaAddDocumentoAggiuntivoFascicoloDecretoSIPATRType.class,
    RispostaConvertiDocumentoType.class,
    RispostaModifyMetadataFascicoloAttoDecretoType.class,
    RispostaGetMonitoraggioDocumentiType.class,
    RispostaModifyMetadataFascicoloDecretoSIPATRType.class,
    RispostaRemoveDocumentoFascicoloRaccoltaProvvisoriaType.class,
    RispostaDownloadDocumentoFascicoloRaccoltaProvvisoriaType.class,
    RispostaGetDocumentoFascicoloSIPATRAttoDecretoType.class,
    RispostaDownloadDocumentoUrlFascicoloRaccoltaProvvisoriaType.class,
    RispostaAddDocumentoFascicoloAllegatoDecretoIGBType.class,
    RispostaDownloadDocumentoFascicoloSIPATRType.class,
    RispostaAddDocumentoFascicoloSIPATRType.class,
    RispostaGetDocumentoAggiuntivoFascicoloAttoDecretoType.class,
    RispostaCopyFascicoloAttoDecretoType.class,
    RispostaDownloadDocumentoUrlFascicoloSIPATRType.class,
    RispostaGetElencoDocumentiFascicoloType.class,
    RispostaAddDocumentoFascicoloAttoDecretoCorteType.class,
    RispostaMergeFascicoloAttoDecretoType.class,
    RispostaRemoveFascicoloDecretoSIPATRType.class,
    RispostaRemoveDocumentoFascicoloSIPATRType.class,
    RispostaRemoveDocumentoFascicoloAttoDecretoCorteType.class,
    RispostaAddDocumentoAggiuntivoFascicoloAttoDecretoType.class,
    RispostaGetFascicoloAttoDecretoCorteType.class,
    RispostaCreateFascicoloRaccoltaProvvisoriaType.class,
    RispostaChangeStatoFascicoloAttoDecretoType.class,
    RispostaRemoveFascicoloSIPATRType.class,
    RispostaGetDocumentoFascicoloSIPATRType.class,
    RispostaInviaFascicoloAttoDecretoCorteType.class,
    RispostaUpdateContentDocumentoFascicoloAttoDecretoType.class,
    RispostaModifyMetadataFascicoloSIPATRType.class,
    RispostaModifyMetadataDocumentoFascicoloSIPATRType.class,
    RispostaDownloadDocumentoFascicoloDecretoSIPATRType.class,
    RispostaArchiveFascicoloAttoDecretoType.class,
    RispostaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType.class,
    RispostaAddCollegamentoFascicoloSIPATRType.class,
    RispostaModifyMetadatiDocumentoFascicoloAttoDecretoType.class,
    RispostaGetElencoDocumentiFascicoloAllegatoDecretoIGBType.class,
    RispostaDownloadDocumentoFascicoloAllegatoDecretoIGBType.class,
    RispostaGetFascicoloDecretoSIPATRType.class,
    RispostaChangeStatoFascicoloDecretoSIPATRType.class,
    RispostaGetDocumentoFascicoloAttoDecretoType.class,
    RispostaChangeStatoFascicoloRaccoltaProvvisoriaType.class,
    RispostaDownloadDocumentoFascicoloAttoDecretoCorteType.class,
    RispostaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGBType.class,
    RispostaRemoveDocumentoFascicoloAllegatoDecretoIGBType.class,
    RispostaSearchFascicoliRaccoltaProvvisoriaType.class,
    RispostaRemoveDocumentoAggiuntivoFascicoloSIPATRType.class,
    RispostaRemoveCollegamentoFascicoloSIPATRType.class,
    RispostaRemoveDocumentoFascicoloAttoDecretoType.class,
    RispostaGetDocumentoFascicoloAllegatoDecretoIGBType.class,
    RispostaAddDocumentoFascicoloRaccoltaProvvisoriaType.class,
    RispostaUpdateMetadatiDocumentoAggiuntivoFascicoloAttoDecretoType.class,
    RispostaInviaDocAggiuntiviFascicoloAttoDecretoCorteType.class,
    RispostaDownloadDocumentoAggiuntivoFascicoloDecretoSIPATRType.class,
    RispostaEliminaFascicoloAttoDecretoType.class,
    RispostaGetRisultatoOperazioneDocumentoType.class,
    RispostaGetFascicoloAttoDecretoType.class,
    RispostaCreateFascicoloAttoDecretoType.class,
    RispostaCreateFascicoloSIPATRType.class,
    RispostaDownloadDocumentoFascicoloAttoDecretoType.class,
    RispostaCreateFascicoloDecretoSIPATRType.class,
    RispostaAddDocumentoFascicoloAttoDecretoType.class,
    RispostaDownloadDocumentoUrlFascicoloAttoDecretoType.class,
    RispostaGetFascicoloRaccoltaProvvisoriaType.class
})
public abstract class BaseServiceResponseType {

    @XmlElement(name = "Esito", required = true)
    protected EsitoType esito;
    @XmlElement(name = "ErrorList")
    protected List<ServiceErrorType> errorList;

    /**
     * Gets the value of the esito property.
     * 
     * @return
     *     possible object is
     *     {@link EsitoType }
     *     
     */
    public EsitoType getEsito() {
        return esito;
    }

    /**
     * Sets the value of the esito property.
     * 
     * @param value
     *     allowed object is
     *     {@link EsitoType }
     *     
     */
    public void setEsito(EsitoType value) {
        this.esito = value;
    }

    /**
     * Gets the value of the errorList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceErrorType }
     * 
     * 
     */
    public List<ServiceErrorType> getErrorList() {
        if (errorList == null) {
            errorList = new ArrayList<ServiceErrorType>();
        }
        return this.errorList;
    }

}
