
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Esito creazione del Fascicolo Atto Decreto
 * 
 * <p>Java class for risposta_createFascicoloAttoDecreto_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="risposta_createFascicoloAttoDecreto_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}BaseServiceResponseType">
 *       &lt;sequence>
 *         &lt;element name="DettaglioFascicolo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}fascicoloAttoDecreto_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_createFascicoloAttoDecreto_type", propOrder = {
    "dettaglioFascicolo"
})
@XmlSeeAlso({
    RispostaCreateFascicoloAttoDecreto.class
})
public class RispostaCreateFascicoloAttoDecretoType
    extends BaseServiceResponseType
{

    @XmlElement(name = "DettaglioFascicolo")
    protected FascicoloAttoDecretoType dettaglioFascicolo;

    /**
     * Gets the value of the dettaglioFascicolo property.
     * 
     * @return
     *     possible object is
     *     {@link FascicoloAttoDecretoType }
     *     
     */
    public FascicoloAttoDecretoType getDettaglioFascicolo() {
        return dettaglioFascicolo;
    }

    /**
     * Sets the value of the dettaglioFascicolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link FascicoloAttoDecretoType }
     *     
     */
    public void setDettaglioFascicolo(FascicoloAttoDecretoType value) {
        this.dettaglioFascicolo = value;
    }

}
