
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoOperazione_type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoOperazione_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CONVERSIONE"/>
 *     &lt;enumeration value="FIRMA_AUTOMATICA"/>
 *     &lt;enumeration value="FIRMA_IMMAGINE"/>
 *     &lt;enumeration value="TIMBRO"/>
 *     &lt;enumeration value="PLACEHOLDER"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoOperazione_type")
@XmlEnum
public enum TipoOperazioneType {

    CONVERSIONE,
    FIRMA_AUTOMATICA,
    FIRMA_IMMAGINE,
    TIMBRO,
    PLACEHOLDER;

    public String value() {
        return name();
    }

    public static TipoOperazioneType fromValue(String v) {
        return valueOf(v);
    }

}
