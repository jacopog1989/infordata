
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiesta Documenti del sotto Fascicolo del Decreto IGB
 * 
 * <p>Java class for richiesta_getElencoDocumentiFascicoloAllegatoDecretoIGB_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_getElencoDocumentiFascicoloAllegatoDecretoIGB_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloAttoDecreto" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="TipoEstrazioneDocumento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoEstrazione_type"/>
 *         &lt;element name="TipoElenco" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoElenco_type"/>
 *         &lt;element name="ShowPreview" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_getElencoDocumentiFascicoloAllegatoDecretoIGB_type", propOrder = {
    "idFascicoloAttoDecreto",
    "tipoEstrazioneDocumento",
    "tipoElenco",
    "showPreview"
})
public class RichiestaGetElencoDocumentiFascicoloAllegatoDecretoIGBType {

    @XmlElement(name = "IdFascicoloAttoDecreto", required = true)
    protected String idFascicoloAttoDecreto;
    @XmlElement(name = "TipoEstrazioneDocumento", required = true, defaultValue = "NONE")
    protected TipoEstrazioneType tipoEstrazioneDocumento;
    @XmlElement(name = "TipoElenco", required = true, defaultValue = "SOURCE")
    protected TipoElencoType tipoElenco;
    @XmlElement(name = "ShowPreview", defaultValue = "false")
    protected Boolean showPreview;

    /**
     * Gets the value of the idFascicoloAttoDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloAttoDecreto() {
        return idFascicoloAttoDecreto;
    }

    /**
     * Sets the value of the idFascicoloAttoDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloAttoDecreto(String value) {
        this.idFascicoloAttoDecreto = value;
    }

    /**
     * Gets the value of the tipoEstrazioneDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link TipoEstrazioneType }
     *     
     */
    public TipoEstrazioneType getTipoEstrazioneDocumento() {
        return tipoEstrazioneDocumento;
    }

    /**
     * Sets the value of the tipoEstrazioneDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEstrazioneType }
     *     
     */
    public void setTipoEstrazioneDocumento(TipoEstrazioneType value) {
        this.tipoEstrazioneDocumento = value;
    }

    /**
     * Gets the value of the tipoElenco property.
     * 
     * @return
     *     possible object is
     *     {@link TipoElencoType }
     *     
     */
    public TipoElencoType getTipoElenco() {
        return tipoElenco;
    }

    /**
     * Sets the value of the tipoElenco property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoElencoType }
     *     
     */
    public void setTipoElenco(TipoElencoType value) {
        this.tipoElenco = value;
    }

    /**
     * Gets the value of the showPreview property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShowPreview() {
        return showPreview;
    }

    /**
     * Sets the value of the showPreview property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShowPreview(Boolean value) {
        this.showPreview = value;
    }

}
