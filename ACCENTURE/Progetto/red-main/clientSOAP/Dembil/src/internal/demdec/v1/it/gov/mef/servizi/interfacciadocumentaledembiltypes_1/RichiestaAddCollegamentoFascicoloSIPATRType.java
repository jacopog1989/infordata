
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Richiesta collegamento del Fascicolo SIPATR al Fascicolo Atto Decreto
 * 
 * <p>Java class for richiesta_addCollegamentoFascicoloSIPATR_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_addCollegamentoFascicoloSIPATR_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloDecretoSIPATR" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="AssociazioneFascicolo" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="IdFascicoloSIPATR" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *                   &lt;element name="DataChiusuraAmministrativa" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_addCollegamentoFascicoloSIPATR_type", propOrder = {
    "idFascicoloDecretoSIPATR",
    "associazioneFascicolo"
})
public class RichiestaAddCollegamentoFascicoloSIPATRType {

    @XmlElement(name = "IdFascicoloDecretoSIPATR", required = true, nillable = true)
    protected String idFascicoloDecretoSIPATR;
    @XmlElement(name = "AssociazioneFascicolo", required = true)
    protected List<RichiestaAddCollegamentoFascicoloSIPATRType.AssociazioneFascicolo> associazioneFascicolo;

    /**
     * Gets the value of the idFascicoloDecretoSIPATR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloDecretoSIPATR() {
        return idFascicoloDecretoSIPATR;
    }

    /**
     * Sets the value of the idFascicoloDecretoSIPATR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloDecretoSIPATR(String value) {
        this.idFascicoloDecretoSIPATR = value;
    }

    /**
     * Gets the value of the associazioneFascicolo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the associazioneFascicolo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssociazioneFascicolo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RichiestaAddCollegamentoFascicoloSIPATRType.AssociazioneFascicolo }
     * 
     * 
     */
    public List<RichiestaAddCollegamentoFascicoloSIPATRType.AssociazioneFascicolo> getAssociazioneFascicolo() {
        if (associazioneFascicolo == null) {
            associazioneFascicolo = new ArrayList<RichiestaAddCollegamentoFascicoloSIPATRType.AssociazioneFascicolo>();
        }
        return this.associazioneFascicolo;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="IdFascicoloSIPATR" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
     *         &lt;element name="DataChiusuraAmministrativa" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idFascicoloSIPATR",
        "dataChiusuraAmministrativa"
    })
    public static class AssociazioneFascicolo {

        @XmlElement(name = "IdFascicoloSIPATR", required = true, nillable = true)
        protected String idFascicoloSIPATR;
        @XmlElement(name = "DataChiusuraAmministrativa")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar dataChiusuraAmministrativa;

        /**
         * Gets the value of the idFascicoloSIPATR property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdFascicoloSIPATR() {
            return idFascicoloSIPATR;
        }

        /**
         * Sets the value of the idFascicoloSIPATR property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdFascicoloSIPATR(String value) {
            this.idFascicoloSIPATR = value;
        }

        /**
         * Gets the value of the dataChiusuraAmministrativa property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDataChiusuraAmministrativa() {
            return dataChiusuraAmministrativa;
        }

        /**
         * Sets the value of the dataChiusuraAmministrativa property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDataChiusuraAmministrativa(XMLGregorianCalendar value) {
            this.dataChiusuraAmministrativa = value;
        }

    }

}
