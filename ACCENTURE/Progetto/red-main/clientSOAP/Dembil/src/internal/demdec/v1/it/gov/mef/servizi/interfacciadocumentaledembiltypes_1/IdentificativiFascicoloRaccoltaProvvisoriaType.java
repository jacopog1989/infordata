
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Identificativi del Fascicolo Raccolta Provvisoria
 * 
 * <p>Java class for identificativiFascicoloRaccoltaProvvisoria_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="identificativiFascicoloRaccoltaProvvisoria_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloRaccoltaProvvisoria" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="IdFascicoloRaccoltaProvvisoriaRED" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "identificativiFascicoloRaccoltaProvvisoria_type", propOrder = {
    "idFascicoloRaccoltaProvvisoria",
    "idFascicoloRaccoltaProvvisoriaRED"
})
public class IdentificativiFascicoloRaccoltaProvvisoriaType {

    @XmlElement(name = "IdFascicoloRaccoltaProvvisoria", required = true)
    protected String idFascicoloRaccoltaProvvisoria;
    @XmlElement(name = "IdFascicoloRaccoltaProvvisoriaRED", required = true)
    protected String idFascicoloRaccoltaProvvisoriaRED;

    /**
     * Gets the value of the idFascicoloRaccoltaProvvisoria property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloRaccoltaProvvisoria() {
        return idFascicoloRaccoltaProvvisoria;
    }

    /**
     * Sets the value of the idFascicoloRaccoltaProvvisoria property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloRaccoltaProvvisoria(String value) {
        this.idFascicoloRaccoltaProvvisoria = value;
    }

    /**
     * Gets the value of the idFascicoloRaccoltaProvvisoriaRED property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloRaccoltaProvvisoriaRED() {
        return idFascicoloRaccoltaProvvisoriaRED;
    }

    /**
     * Sets the value of the idFascicoloRaccoltaProvvisoriaRED property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloRaccoltaProvvisoriaRED(String value) {
        this.idFascicoloRaccoltaProvvisoriaRED = value;
    }

}
