
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Restituisce il content del documento del Fascicolo Raccolta Provvisoria
 * 
 * <p>Java class for risposta_getRisultatoOperazioneDocumento_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="risposta_getRisultatoOperazioneDocumento_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}BaseServiceResponseType">
 *       &lt;sequence>
 *         &lt;element name="Operazioni" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}risultatoOperazioneDocumento_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_getRisultatoOperazioneDocumento_type", propOrder = {
    "operazioni"
})
@XmlSeeAlso({
    RispostaGetRisultatoOperazioneDocumento.class
})
public class RispostaGetRisultatoOperazioneDocumentoType
    extends BaseServiceResponseType
{

    @XmlElement(name = "Operazioni")
    protected RisultatoOperazioneDocumentoType operazioni;

    /**
     * Gets the value of the operazioni property.
     * 
     * @return
     *     possible object is
     *     {@link RisultatoOperazioneDocumentoType }
     *     
     */
    public RisultatoOperazioneDocumentoType getOperazioni() {
        return operazioni;
    }

    /**
     * Sets the value of the operazioni property.
     * 
     * @param value
     *     allowed object is
     *     {@link RisultatoOperazioneDocumentoType }
     *     
     */
    public void setOperazioni(RisultatoOperazioneDocumentoType value) {
        this.operazioni = value;
    }

}
