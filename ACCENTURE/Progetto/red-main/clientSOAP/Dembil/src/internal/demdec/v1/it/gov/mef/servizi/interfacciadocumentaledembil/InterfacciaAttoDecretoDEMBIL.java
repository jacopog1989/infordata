
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import internal.demdec.v1.it.gov.mef.servizi.common.headeraccessoapplicativo.AccessoApplicativo;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaAddDocumentoAggiuntivoFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaAddDocumentoFascicoloAllegatoDecretoIGBType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaAddDocumentoFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaArchiveFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaChangeStatoFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaChangeStatoFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaCopyFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaCreateFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaDownloadDocumentoAggiuntivoFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaDownloadDocumentoFascicoloAllegatoDecretoIGBType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaDownloadDocumentoFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaDownloadDocumentoFascicoloDecretoSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaDownloadDocumentoFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaGetDocumentoAggiuntivoFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaGetDocumentoFascicoloAllegatoDecretoIGBType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaGetDocumentoFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaGetDocumentoFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaGetDocumentoFascicoloSIPATRAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaGetElencoDocumentiFascicoloType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaGetFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaGetFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaGetFascicoloSIPATRAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaGetMonitoraggioDocumentiType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaGetRisultatoOperazioneDocumentoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaMergeFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaModifyMetadataFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGBType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaModifyMetadatiDocumentoFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaRemoveDocumentoAggiuntivoFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaRemoveDocumentoFascicoloAllegatoDecretoIGBType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaRemoveDocumentoFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaRemoveFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaSearchFascicoliRaccolteProvvisorieType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaUpdateContentDocumentoFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaAddDocumentoAggiuntivoFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaAddDocumentoFascicoloAllegatoDecretoIGBType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaAddDocumentoFascicoloAttoDecreto;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaArchiveFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaChangeStatoFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaChangeStatoFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaCopyFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaCreateFascicoloAttoDecreto;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaDownloadDocumentoAggiuntivoFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaDownloadDocumentoFascicoloAllegatoDecretoIGBType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaDownloadDocumentoFascicoloAttoDecreto;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaDownloadDocumentoFascicoloDecretoSIPATRType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaDownloadDocumentoFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetDocumentoAggiuntivoFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetDocumentoFascicoloAllegatoDecretoIGBType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetDocumentoFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetDocumentoFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetDocumentoFascicoloSIPATRAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetElencoDocumentiFascicoloType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetFascicoloAttoDecreto;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetFascicoloRaccoltaProvvisoria;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetFascicoloSIPATRAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetMonitoraggioDocumentiType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetRisultatoOperazioneDocumento;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaMergeFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaModifyMetadataFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGBType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaModifyMetadatiDocumentoFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaRemoveDocumentoAggiuntivoFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaRemoveDocumentoFascicoloAllegatoDecretoIGBType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaRemoveDocumentoFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaRemoveFascicoloAttoDecretoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaSearchFascicoliRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaUpdateContentDocumentoFascicoloAttoDecretoType;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebService(name = "InterfacciaAttoDecretoDEMBIL", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBIL")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.ObjectFactory.class,
    internal.demdec.v1.it.gov.mef.servizi.common.headeraccessoapplicativo.ObjectFactory.class,
    internal.demdec.v1.it.gov.mef.servizi.common.headerfault.ObjectFactory.class,
    org.xmlsoap.schemas.soap.envelope.ObjectFactory.class
})
public interface InterfacciaAttoDecretoDEMBIL {


    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaCreateFascicoloAttoDecreto
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:createFascicoloAttoDecreto")
    @WebResult(name = "risposta_createFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaCreateFascicoloAttoDecreto createFascicoloAttoDecreto(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_createFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaCreateFascicoloAttoDecretoType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaCopyFascicoloAttoDecretoType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:copyFascicoloAttoDecreto")
    @WebResult(name = "risposta_copyFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaCopyFascicoloAttoDecretoType copyFascicoloAttoDecreto(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_copyFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaCopyFascicoloAttoDecretoType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaArchiveFascicoloAttoDecretoType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:archiveFascicoloAttoDecreto")
    @WebResult(name = "risposta_archiveFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaArchiveFascicoloAttoDecretoType archiveFascicoloAttoDecreto(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_archiveFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaArchiveFascicoloAttoDecretoType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaMergeFascicoloAttoDecretoType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:mergeFascicoloAttoDecreto")
    @WebResult(name = "risposta_mergeFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaMergeFascicoloAttoDecretoType mergeFascicoloAttoDecreto(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_mergeFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaMergeFascicoloAttoDecretoType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetFascicoloAttoDecreto
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:getFascicoloAttoDecreto")
    @WebResult(name = "risposta_getFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaGetFascicoloAttoDecreto getFascicoloAttoDecreto(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_getFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaGetFascicoloAttoDecretoType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetElencoDocumentiFascicoloType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:getElencoDocumentiFascicoloAttoDecreto")
    @WebResult(name = "risposta_getElencoDocumentiFascicolo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaGetElencoDocumentiFascicoloType getElencoDocumentiFascicoloAttoDecreto(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_getElencoDocumentiFascicolo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaGetElencoDocumentiFascicoloType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetMonitoraggioDocumentiType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:monitoraggioFascicoloAttoDecreto")
    @WebResult(name = "risposta_monitoraggioDocumenti", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaGetMonitoraggioDocumentiType monitoraggioFascicoloAttoDecreto(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_monitoraggioDocumenti", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaGetMonitoraggioDocumentiType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaModifyMetadataFascicoloAttoDecretoType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:getFascicoloAttoDecreto")
    @WebResult(name = "risposta_modifyMetadatiFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaModifyMetadataFascicoloAttoDecretoType modifyMetadatiFascicoloAttoDecreto(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_modifyMetadatiFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaModifyMetadataFascicoloAttoDecretoType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaChangeStatoFascicoloAttoDecretoType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:changeStatoFascicoloAttoDecreto")
    @WebResult(name = "risposta_changeStatoFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaChangeStatoFascicoloAttoDecretoType changeStatoFascicoloAttoDecreto(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_changeStatoFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaChangeStatoFascicoloAttoDecretoType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaRemoveFascicoloAttoDecretoType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:removeFascicoloAttoDecreto")
    @WebResult(name = "risposta_removeFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaRemoveFascicoloAttoDecretoType removeFascicoloAttoDecreto(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_removeFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaRemoveFascicoloAttoDecretoType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaAddDocumentoFascicoloAttoDecreto
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:addDocumentoFascicoloAttoDecreto")
    @WebResult(name = "risposta_addDocumentoFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaAddDocumentoFascicoloAttoDecreto addDocumentoFascicoloAttoDecreto(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_addDocumentoFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaAddDocumentoFascicoloAttoDecretoType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaAddDocumentoAggiuntivoFascicoloAttoDecretoType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:addDocumentoAggiuntivoFascicoloAttoDecreto")
    @WebResult(name = "risposta_addDocumentoAggiuntivoFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaAddDocumentoAggiuntivoFascicoloAttoDecretoType addDocumentoAggiuntivoFascicoloAttoDecreto(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_addDocumentoAggiuntivoFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaAddDocumentoAggiuntivoFascicoloAttoDecretoType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetDocumentoAggiuntivoFascicoloAttoDecretoType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:getDocumentoAggiuntivoFascicoloAttoDecreto")
    @WebResult(name = "risposta_getDocumentoAggiuntivoFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaGetDocumentoAggiuntivoFascicoloAttoDecretoType getDocumentoAggiuntivoFascicoloAttoDecreto(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_getDocumentoAggiuntivoFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaGetDocumentoAggiuntivoFascicoloAttoDecretoType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaDownloadDocumentoAggiuntivoFascicoloAttoDecretoType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:downloadDocumentoAggiuntivoFascicoloAttoDecreto")
    @WebResult(name = "risposta_downloadDocumentoAggiuntivoFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaDownloadDocumentoAggiuntivoFascicoloAttoDecretoType downloadDocumentoAggiuntivoFascicoloAttoDecreto(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_downloadDocumentoAggiuntivoFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaDownloadDocumentoAggiuntivoFascicoloAttoDecretoType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaRemoveDocumentoAggiuntivoFascicoloAttoDecretoType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:removeDocumentoAggiuntivoFascicoloAttoDecreto")
    @WebResult(name = "risposta_removeDocumentoAggiuntivoFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaRemoveDocumentoAggiuntivoFascicoloAttoDecretoType removeDocumentoAggiuntivoFascicoloAttoDecreto(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_removeDocumentoAggiuntivoFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaRemoveDocumentoAggiuntivoFascicoloAttoDecretoType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaUpdateContentDocumentoFascicoloAttoDecretoType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:updateContentDocumentoFascicoloAttoDecreto")
    @WebResult(name = "risposta_updateContentDocumentoFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaUpdateContentDocumentoFascicoloAttoDecretoType updateContentDocumentoFascicoloAttoDecreto(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_updateContentDocumentoFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaUpdateContentDocumentoFascicoloAttoDecretoType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetDocumentoFascicoloAttoDecretoType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:getDocumentoFascicoloAttoDecreto")
    @WebResult(name = "risposta_getDocumentoFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaGetDocumentoFascicoloAttoDecretoType getDocumentoFascicoloAttoDecreto(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_getDocumentoFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaGetDocumentoFascicoloAttoDecretoType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaDownloadDocumentoFascicoloAttoDecreto
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:downloadDocumentoFascicoloAttoDecreto")
    @WebResult(name = "risposta_downloadDocumentoFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaDownloadDocumentoFascicoloAttoDecreto downloadDocumentoFascicoloAttoDecreto(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_downloadDocumentoFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaDownloadDocumentoFascicoloAttoDecretoType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaModifyMetadatiDocumentoFascicoloAttoDecretoType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:modifyMetadatiDocumentoFascicoloAttoDecreto")
    @WebResult(name = "risposta_modifyMetadatiDocumentoFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaModifyMetadatiDocumentoFascicoloAttoDecretoType modifyMetadatiDocumentoFascicoloAttoDecreto(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_modifyMetadatiDocumentoFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaModifyMetadatiDocumentoFascicoloAttoDecretoType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaRemoveDocumentoFascicoloAttoDecretoType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:removeDocumentoFascicoloAttoDecreto")
    @WebResult(name = "risposta_removeDocumentoFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaRemoveDocumentoFascicoloAttoDecretoType removeDocumentoFascicoloAttoDecreto(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_removeDocumentoFascicoloAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaRemoveDocumentoFascicoloAttoDecretoType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaAddDocumentoFascicoloAllegatoDecretoIGBType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:addDocumentoFascicoloAllegatoDecretoIGB")
    @WebResult(name = "risposta_addDocumentoFascicoloAllegatoDecretoIGB", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaAddDocumentoFascicoloAllegatoDecretoIGBType addDocumentoFascicoloAllegatoDecretoIGB(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_addDocumentoFascicoloAllegatoDecretoIGB", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaAddDocumentoFascicoloAllegatoDecretoIGBType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetDocumentoFascicoloAllegatoDecretoIGBType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:getDocumentoFascicoloAllegatoDecretoIGB")
    @WebResult(name = "risposta_getDocumentoFascicoloAllegatoDecretoIGB", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaGetDocumentoFascicoloAllegatoDecretoIGBType getDocumentoFascicoloAllegatoDecretoIGB(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_getDocumentoFascicoloAllegatoDecretoIGB", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaGetDocumentoFascicoloAllegatoDecretoIGBType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaDownloadDocumentoFascicoloAllegatoDecretoIGBType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:downloadDocumentoAllegatoDecretoIGB")
    @WebResult(name = "risposta_downloadDocumentoFascicoloAllegatoDecretoIGB", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaDownloadDocumentoFascicoloAllegatoDecretoIGBType downloadDocumentoFascicoloAllegatoDecretoIGB(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_downloadDocumentoFascicoloAllegatoDecretoIGB", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaDownloadDocumentoFascicoloAllegatoDecretoIGBType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGBType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:modifyMetadatiAllegatoDecretoIGB")
    @WebResult(name = "risposta_modifyMetadatiDocumentoFascicoloAllegatoDecretoIGB", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGBType modifyMetadatiDocumentoFascicoloAllegatoDecretoIGB(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_modifyMetadatiDocumentoFascicoloAllegatoDecretoIGB", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGBType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaRemoveDocumentoFascicoloAllegatoDecretoIGBType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:removeAllegatoDecretoIGB")
    @WebResult(name = "risposta_removeDocumentoFascicoloAllegatoDecretoIGB", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaRemoveDocumentoFascicoloAllegatoDecretoIGBType removeDocumentoAllegatoDecretoIGB(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_removeDocumentoFascicoloAllegatoDecretoIGB", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaRemoveDocumentoFascicoloAllegatoDecretoIGBType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetFascicoloRaccoltaProvvisoria
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:getFascicoloRaccoltaProvvisoria")
    @WebResult(name = "risposta_getFascicoloRaccoltaProvvisoria", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaGetFascicoloRaccoltaProvvisoria getFascicoloRaccoltaProvvisoria(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_getFascicoloRaccoltaProvvisoria", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaGetFascicoloRaccoltaProvvisoriaType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaChangeStatoFascicoloRaccoltaProvvisoriaType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:changeStatoFascicoloRaccoltaProvvisoria")
    @WebResult(name = "risposta_changeStatoFascicoloRaccoltaProvvisoria", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaChangeStatoFascicoloRaccoltaProvvisoriaType changeStatoFascicoloRaccoltaProvvisoria(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_changeStatoFascicoloRaccoltaProvvisoria", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaChangeStatoFascicoloRaccoltaProvvisoriaType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaSearchFascicoliRaccoltaProvvisoriaType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:searchFascicoliRaccolteProvvisorie")
    @WebResult(name = "risposta_searchFascicoliRaccolteProvvisorie", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaSearchFascicoliRaccoltaProvvisoriaType searchFascicoliRaccolteProvvisorie(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_searchFascicoliRaccolteProvvisorie", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaSearchFascicoliRaccolteProvvisorieType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetDocumentoFascicoloRaccoltaProvvisoriaType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:getDocumentoFascicoloRaccoltaProvvisoria")
    @WebResult(name = "risposta_getDocumentoFascicoloRaccoltaProvvisoria", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaGetDocumentoFascicoloRaccoltaProvvisoriaType getDocumentoFascicoloRaccoltaProvvisoria(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_getDocumentoFascicoloRaccoltaProvvisoria", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaGetDocumentoFascicoloRaccoltaProvvisoriaType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaDownloadDocumentoFascicoloRaccoltaProvvisoriaType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:downloadDocumentoFascicoloRaccoltaProvvisoria")
    @WebResult(name = "risposta_downloadDocumentoFascicoloRaccoltaProvvisoria", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaDownloadDocumentoFascicoloRaccoltaProvvisoriaType downloadDocumentoFascicoloRaccoltaProvvisoria(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_downloadDocumentoFascicoloRaccoltaProvvisoria", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaDownloadDocumentoFascicoloRaccoltaProvvisoriaType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetFascicoloSIPATRAttoDecretoType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:getFascicoloSIPATRAttoDecreto")
    @WebResult(name = "risposta_getFascicoloSIPATRAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaGetFascicoloSIPATRAttoDecretoType getFascicoloSIPATRAttoDecreto(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_getFascicoloSIPATRAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaGetFascicoloSIPATRAttoDecretoType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetDocumentoFascicoloSIPATRAttoDecretoType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:getDocumentoFascicoloSIPATRAttoDecreto")
    @WebResult(name = "risposta_getDocumentoFascicoloSIPATRAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaGetDocumentoFascicoloSIPATRAttoDecretoType getDocumentoFascicoloSIPATRAttoDecreto(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_getDocumentoFascicoloSIPATRAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaGetDocumentoFascicoloSIPATRAttoDecretoType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaDownloadDocumentoFascicoloDecretoSIPATRType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:downloadDocumentoFascicoloSIPATRAttoDecreto")
    @WebResult(name = "risposta_downloadDocumentoFascicoloSIPATRAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaDownloadDocumentoFascicoloDecretoSIPATRType downloadDocumentoFascicoloSIPATRAttoDecreto(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_downloadDocumentoFascicoloSIPATRAttoDecreto", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaDownloadDocumentoFascicoloDecretoSIPATRType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetRisultatoOperazioneDocumento
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:getRisultatoOperazioneDocumento")
    @WebResult(name = "risposta_getRisultatoOperazioneDocumento", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaGetRisultatoOperazioneDocumento getRisultatoOperazioneDocumento(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_getRisultatoOperazioneDocumento", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaGetRisultatoOperazioneDocumentoType parameters)
        throws GenericFault, SecurityFault
    ;

}
