
package internal.demdec.v1.it.gov.mef.servizi.common.headerfault;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderFault}BaseServiceFaultType">
 *       &lt;sequence>
 *         &lt;element name="APPLICAZIONE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SERVICE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OPERATION" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "applicazione",
    "service",
    "operation"
})
@XmlRootElement(name = "SecurityFault")
public class SecurityFault
    extends BaseServiceFaultType
{

    @XmlElement(name = "APPLICAZIONE", required = true)
    protected String applicazione;
    @XmlElement(name = "SERVICE", required = true)
    protected String service;
    @XmlElement(name = "OPERATION", required = true)
    protected String operation;

    /**
     * Gets the value of the applicazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAPPLICAZIONE() {
        return applicazione;
    }

    /**
     * Sets the value of the applicazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAPPLICAZIONE(String value) {
        this.applicazione = value;
    }

    /**
     * Gets the value of the service property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSERVICE() {
        return service;
    }

    /**
     * Sets the value of the service property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSERVICE(String value) {
        this.service = value;
    }

    /**
     * Gets the value of the operation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOPERATION() {
        return operation;
    }

    /**
     * Sets the value of the operation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOPERATION(String value) {
        this.operation = value;
    }

}
