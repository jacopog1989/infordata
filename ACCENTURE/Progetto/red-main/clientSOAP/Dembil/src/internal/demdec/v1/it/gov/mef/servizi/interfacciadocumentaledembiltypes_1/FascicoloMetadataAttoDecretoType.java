
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Metadati del Fascicolo Atto Decreto
 * 
 * <p>Java class for fascicoloMetadataAttoDecreto_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="fascicoloMetadataAttoDecreto_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdentificativoAttoDecreto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Titolo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="Ragioneria" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="Amministrazione" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="Anno" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TipologiaDecreto" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="TipoDecreto" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="NumeroDecreto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="UfficioCapofila" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="UfficioCreatore" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="UtenteCreatore" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type" minOccurs="0"/>
 *         &lt;element name="StatoFascicoloAttoDecreto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DataInvioCorte" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="StatoAcquisizioneCorte" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}statoAcquisizione_type" minOccurs="0"/>
 *         &lt;element name="DataAcquisizioneCorte" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="IdFascicoloDecretoSIPATR" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid" minOccurs="0"/>
 *         &lt;element name="NumeroDecretoSIPATR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VersioneMetadata" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Metadata" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fascicoloMetadataAttoDecreto_type", propOrder = {
    "identificativoAttoDecreto",
    "titolo",
    "ragioneria",
    "amministrazione",
    "anno",
    "tipologiaDecreto",
    "tipoDecreto",
    "numeroDecreto",
    "ufficioCapofila",
    "ufficioCreatore",
    "utenteCreatore",
    "statoFascicoloAttoDecreto",
    "dataInvioCorte",
    "statoAcquisizioneCorte",
    "dataAcquisizioneCorte",
    "idFascicoloDecretoSIPATR",
    "numeroDecretoSIPATR",
    "versioneMetadata",
    "metadata"
})
public class FascicoloMetadataAttoDecretoType {

    @XmlElement(name = "IdentificativoAttoDecreto", required = true)
    protected String identificativoAttoDecreto;
    @XmlElement(name = "Titolo", required = true)
    protected CodiceDescrizioneType titolo;
    @XmlElement(name = "Ragioneria", required = true)
    protected CodiceDescrizioneType ragioneria;
    @XmlElement(name = "Amministrazione", required = true)
    protected CodiceDescrizioneType amministrazione;
    @XmlElement(name = "Anno")
    protected int anno;
    @XmlElement(name = "TipologiaDecreto", required = true)
    protected CodiceDescrizioneType tipologiaDecreto;
    @XmlElement(name = "TipoDecreto", required = true)
    protected CodiceDescrizioneType tipoDecreto;
    @XmlElement(name = "NumeroDecreto")
    protected int numeroDecreto;
    @XmlElement(name = "UfficioCapofila", required = true)
    protected CodiceDescrizioneType ufficioCapofila;
    @XmlElement(name = "UfficioCreatore", required = true)
    protected CodiceDescrizioneType ufficioCreatore;
    @XmlElement(name = "UtenteCreatore")
    protected CodiceDescrizioneType utenteCreatore;
    @XmlElement(name = "StatoFascicoloAttoDecreto", required = true)
    protected String statoFascicoloAttoDecreto;
    @XmlElement(name = "DataInvioCorte")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataInvioCorte;
    @XmlElement(name = "StatoAcquisizioneCorte")
    protected StatoAcquisizioneType statoAcquisizioneCorte;
    @XmlElement(name = "DataAcquisizioneCorte")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataAcquisizioneCorte;
    @XmlElement(name = "IdFascicoloDecretoSIPATR")
    protected String idFascicoloDecretoSIPATR;
    @XmlElement(name = "NumeroDecretoSIPATR")
    protected String numeroDecretoSIPATR;
    @XmlElement(name = "VersioneMetadata")
    protected String versioneMetadata;
    @XmlElement(name = "Metadata")
    protected byte[] metadata;

    /**
     * Gets the value of the identificativoAttoDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificativoAttoDecreto() {
        return identificativoAttoDecreto;
    }

    /**
     * Sets the value of the identificativoAttoDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificativoAttoDecreto(String value) {
        this.identificativoAttoDecreto = value;
    }

    /**
     * Gets the value of the titolo property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getTitolo() {
        return titolo;
    }

    /**
     * Sets the value of the titolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setTitolo(CodiceDescrizioneType value) {
        this.titolo = value;
    }

    /**
     * Gets the value of the ragioneria property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getRagioneria() {
        return ragioneria;
    }

    /**
     * Sets the value of the ragioneria property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setRagioneria(CodiceDescrizioneType value) {
        this.ragioneria = value;
    }

    /**
     * Gets the value of the amministrazione property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getAmministrazione() {
        return amministrazione;
    }

    /**
     * Sets the value of the amministrazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setAmministrazione(CodiceDescrizioneType value) {
        this.amministrazione = value;
    }

    /**
     * Gets the value of the anno property.
     * 
     */
    public int getAnno() {
        return anno;
    }

    /**
     * Sets the value of the anno property.
     * 
     */
    public void setAnno(int value) {
        this.anno = value;
    }

    /**
     * Gets the value of the tipologiaDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getTipologiaDecreto() {
        return tipologiaDecreto;
    }

    /**
     * Sets the value of the tipologiaDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setTipologiaDecreto(CodiceDescrizioneType value) {
        this.tipologiaDecreto = value;
    }

    /**
     * Gets the value of the tipoDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getTipoDecreto() {
        return tipoDecreto;
    }

    /**
     * Sets the value of the tipoDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setTipoDecreto(CodiceDescrizioneType value) {
        this.tipoDecreto = value;
    }

    /**
     * Gets the value of the numeroDecreto property.
     * 
     */
    public int getNumeroDecreto() {
        return numeroDecreto;
    }

    /**
     * Sets the value of the numeroDecreto property.
     * 
     */
    public void setNumeroDecreto(int value) {
        this.numeroDecreto = value;
    }

    /**
     * Gets the value of the ufficioCapofila property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getUfficioCapofila() {
        return ufficioCapofila;
    }

    /**
     * Sets the value of the ufficioCapofila property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setUfficioCapofila(CodiceDescrizioneType value) {
        this.ufficioCapofila = value;
    }

    /**
     * Gets the value of the ufficioCreatore property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getUfficioCreatore() {
        return ufficioCreatore;
    }

    /**
     * Sets the value of the ufficioCreatore property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setUfficioCreatore(CodiceDescrizioneType value) {
        this.ufficioCreatore = value;
    }

    /**
     * Gets the value of the utenteCreatore property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getUtenteCreatore() {
        return utenteCreatore;
    }

    /**
     * Sets the value of the utenteCreatore property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setUtenteCreatore(CodiceDescrizioneType value) {
        this.utenteCreatore = value;
    }

    /**
     * Gets the value of the statoFascicoloAttoDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatoFascicoloAttoDecreto() {
        return statoFascicoloAttoDecreto;
    }

    /**
     * Sets the value of the statoFascicoloAttoDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatoFascicoloAttoDecreto(String value) {
        this.statoFascicoloAttoDecreto = value;
    }

    /**
     * Gets the value of the dataInvioCorte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataInvioCorte() {
        return dataInvioCorte;
    }

    /**
     * Sets the value of the dataInvioCorte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataInvioCorte(XMLGregorianCalendar value) {
        this.dataInvioCorte = value;
    }

    /**
     * Gets the value of the statoAcquisizioneCorte property.
     * 
     * @return
     *     possible object is
     *     {@link StatoAcquisizioneType }
     *     
     */
    public StatoAcquisizioneType getStatoAcquisizioneCorte() {
        return statoAcquisizioneCorte;
    }

    /**
     * Sets the value of the statoAcquisizioneCorte property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatoAcquisizioneType }
     *     
     */
    public void setStatoAcquisizioneCorte(StatoAcquisizioneType value) {
        this.statoAcquisizioneCorte = value;
    }

    /**
     * Gets the value of the dataAcquisizioneCorte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAcquisizioneCorte() {
        return dataAcquisizioneCorte;
    }

    /**
     * Sets the value of the dataAcquisizioneCorte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAcquisizioneCorte(XMLGregorianCalendar value) {
        this.dataAcquisizioneCorte = value;
    }

    /**
     * Gets the value of the idFascicoloDecretoSIPATR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloDecretoSIPATR() {
        return idFascicoloDecretoSIPATR;
    }

    /**
     * Sets the value of the idFascicoloDecretoSIPATR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloDecretoSIPATR(String value) {
        this.idFascicoloDecretoSIPATR = value;
    }

    /**
     * Gets the value of the numeroDecretoSIPATR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroDecretoSIPATR() {
        return numeroDecretoSIPATR;
    }

    /**
     * Sets the value of the numeroDecretoSIPATR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroDecretoSIPATR(String value) {
        this.numeroDecretoSIPATR = value;
    }

    /**
     * Gets the value of the versioneMetadata property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersioneMetadata() {
        return versioneMetadata;
    }

    /**
     * Sets the value of the versioneMetadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersioneMetadata(String value) {
        this.versioneMetadata = value;
    }

    /**
     * Gets the value of the metadata property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getMetadata() {
        return metadata;
    }

    /**
     * Sets the value of the metadata property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setMetadata(byte[] value) {
        this.metadata = value;
    }

}
