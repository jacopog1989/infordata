
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Criteri di filtro per l'estrazione dei documenti dei Documenti Raccolta Provvisoria
 * 
 * <p>Java class for criteriaDocumentiRaccoltaProvvisoria_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="criteriaDocumentiRaccoltaProvvisoria_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoEstrazioneElenco" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoEstrazione_type"/>
 *         &lt;element name="TipoElenco" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoElenco_type"/>
 *         &lt;element name="TipoDocumento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoDocumento_type" minOccurs="0"/>
 *         &lt;element name="DataCreazione" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}dataRange_type" minOccurs="0"/>
 *         &lt;element name="DataDocumento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}dataRange_type" minOccurs="0"/>
 *         &lt;element name="DataChiusura" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}dataRange_type" minOccurs="0"/>
 *         &lt;element name="DataAnnullamento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}dataRange_type" minOccurs="0"/>
 *         &lt;element name="DataRiapertura" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}dataRange_type" minOccurs="0"/>
 *         &lt;element name="Protocollo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}protocollo_type" minOccurs="0"/>
 *         &lt;element name="Note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShowPreview" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "criteriaDocumentiRaccoltaProvvisoria_type", propOrder = {
    "tipoEstrazioneElenco",
    "tipoElenco",
    "tipoDocumento",
    "dataCreazione",
    "dataDocumento",
    "dataChiusura",
    "dataAnnullamento",
    "dataRiapertura",
    "protocollo",
    "note",
    "showPreview"
})
public class CriteriaDocumentiRaccoltaProvvisoriaType {

    @XmlElement(name = "TipoEstrazioneElenco", required = true, defaultValue = "NONE")
    protected TipoEstrazioneType tipoEstrazioneElenco;
    @XmlElement(name = "TipoElenco", required = true, defaultValue = "SOURCE")
    protected TipoElencoType tipoElenco;
    @XmlElement(name = "TipoDocumento")
    protected TipoDocumentoType tipoDocumento;
    @XmlElement(name = "DataCreazione")
    protected DataRangeType dataCreazione;
    @XmlElement(name = "DataDocumento")
    protected DataRangeType dataDocumento;
    @XmlElement(name = "DataChiusura")
    protected DataRangeType dataChiusura;
    @XmlElement(name = "DataAnnullamento")
    protected DataRangeType dataAnnullamento;
    @XmlElement(name = "DataRiapertura")
    protected DataRangeType dataRiapertura;
    @XmlElement(name = "Protocollo")
    protected ProtocolloType protocollo;
    @XmlElement(name = "Note")
    protected String note;
    @XmlElement(name = "ShowPreview", defaultValue = "false")
    protected Boolean showPreview;

    /**
     * Gets the value of the tipoEstrazioneElenco property.
     * 
     * @return
     *     possible object is
     *     {@link TipoEstrazioneType }
     *     
     */
    public TipoEstrazioneType getTipoEstrazioneElenco() {
        return tipoEstrazioneElenco;
    }

    /**
     * Sets the value of the tipoEstrazioneElenco property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEstrazioneType }
     *     
     */
    public void setTipoEstrazioneElenco(TipoEstrazioneType value) {
        this.tipoEstrazioneElenco = value;
    }

    /**
     * Gets the value of the tipoElenco property.
     * 
     * @return
     *     possible object is
     *     {@link TipoElencoType }
     *     
     */
    public TipoElencoType getTipoElenco() {
        return tipoElenco;
    }

    /**
     * Sets the value of the tipoElenco property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoElencoType }
     *     
     */
    public void setTipoElenco(TipoElencoType value) {
        this.tipoElenco = value;
    }

    /**
     * Gets the value of the tipoDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link TipoDocumentoType }
     *     
     */
    public TipoDocumentoType getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Sets the value of the tipoDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDocumentoType }
     *     
     */
    public void setTipoDocumento(TipoDocumentoType value) {
        this.tipoDocumento = value;
    }

    /**
     * Gets the value of the dataCreazione property.
     * 
     * @return
     *     possible object is
     *     {@link DataRangeType }
     *     
     */
    public DataRangeType getDataCreazione() {
        return dataCreazione;
    }

    /**
     * Sets the value of the dataCreazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataRangeType }
     *     
     */
    public void setDataCreazione(DataRangeType value) {
        this.dataCreazione = value;
    }

    /**
     * Gets the value of the dataDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link DataRangeType }
     *     
     */
    public DataRangeType getDataDocumento() {
        return dataDocumento;
    }

    /**
     * Sets the value of the dataDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataRangeType }
     *     
     */
    public void setDataDocumento(DataRangeType value) {
        this.dataDocumento = value;
    }

    /**
     * Gets the value of the dataChiusura property.
     * 
     * @return
     *     possible object is
     *     {@link DataRangeType }
     *     
     */
    public DataRangeType getDataChiusura() {
        return dataChiusura;
    }

    /**
     * Sets the value of the dataChiusura property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataRangeType }
     *     
     */
    public void setDataChiusura(DataRangeType value) {
        this.dataChiusura = value;
    }

    /**
     * Gets the value of the dataAnnullamento property.
     * 
     * @return
     *     possible object is
     *     {@link DataRangeType }
     *     
     */
    public DataRangeType getDataAnnullamento() {
        return dataAnnullamento;
    }

    /**
     * Sets the value of the dataAnnullamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataRangeType }
     *     
     */
    public void setDataAnnullamento(DataRangeType value) {
        this.dataAnnullamento = value;
    }

    /**
     * Gets the value of the dataRiapertura property.
     * 
     * @return
     *     possible object is
     *     {@link DataRangeType }
     *     
     */
    public DataRangeType getDataRiapertura() {
        return dataRiapertura;
    }

    /**
     * Sets the value of the dataRiapertura property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataRangeType }
     *     
     */
    public void setDataRiapertura(DataRangeType value) {
        this.dataRiapertura = value;
    }

    /**
     * Gets the value of the protocollo property.
     * 
     * @return
     *     possible object is
     *     {@link ProtocolloType }
     *     
     */
    public ProtocolloType getProtocollo() {
        return protocollo;
    }

    /**
     * Sets the value of the protocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtocolloType }
     *     
     */
    public void setProtocollo(ProtocolloType value) {
        this.protocollo = value;
    }

    /**
     * Gets the value of the note property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNote() {
        return note;
    }

    /**
     * Sets the value of the note property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNote(String value) {
        this.note = value;
    }

    /**
     * Gets the value of the showPreview property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isShowPreview() {
        return showPreview;
    }

    /**
     * Sets the value of the showPreview property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setShowPreview(Boolean value) {
        this.showPreview = value;
    }

}
