
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiede l'aggiornamento del content del documento AttoDecreto al fascicolo
 * 
 * <p>Java class for richiesta_updateContentDocumentoFascicoloAttoDecreto_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_updateContentDocumentoFascicoloAttoDecreto_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloAttoDecreto" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="IdDocumentoAttoDecreto" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="DocumentoContent" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}documentoContent_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_updateContentDocumentoFascicoloAttoDecreto_type", propOrder = {
    "idFascicoloAttoDecreto",
    "idDocumentoAttoDecreto",
    "documentoContent"
})
public class RichiestaUpdateContentDocumentoFascicoloAttoDecretoType {

    @XmlElement(name = "IdFascicoloAttoDecreto", required = true)
    protected String idFascicoloAttoDecreto;
    @XmlElement(name = "IdDocumentoAttoDecreto", required = true, nillable = true)
    protected String idDocumentoAttoDecreto;
    @XmlElement(name = "DocumentoContent", required = true)
    protected DocumentoContentType documentoContent;

    /**
     * Gets the value of the idFascicoloAttoDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloAttoDecreto() {
        return idFascicoloAttoDecreto;
    }

    /**
     * Sets the value of the idFascicoloAttoDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloAttoDecreto(String value) {
        this.idFascicoloAttoDecreto = value;
    }

    /**
     * Gets the value of the idDocumentoAttoDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoAttoDecreto() {
        return idDocumentoAttoDecreto;
    }

    /**
     * Sets the value of the idDocumentoAttoDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoAttoDecreto(String value) {
        this.idDocumentoAttoDecreto = value;
    }

    /**
     * Gets the value of the documentoContent property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoContentType }
     *     
     */
    public DocumentoContentType getDocumentoContent() {
        return documentoContent;
    }

    /**
     * Sets the value of the documentoContent property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoContentType }
     *     
     */
    public void setDocumentoContent(DocumentoContentType value) {
        this.documentoContent = value;
    }

}
