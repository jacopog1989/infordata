
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RichiestaRemoveDocumentoFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_removeDocumentoFascicoloSIPATR");
    private final static QName _RichiestaDownloadDocumentoFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_downloadDocumentoFascicoloSIPATR");
    private final static QName _RispostaDownloadDocumentoFascicoloRaccoltaProvvisoria_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_downloadDocumentoFascicoloRaccoltaProvvisoria");
    private final static QName _RispostaRemoveDocumentoFascicoloRaccoltaProvvisoria_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_removeDocumentoFascicoloRaccoltaProvvisoria");
    private final static QName _RichiestaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoria_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_modifyMetadatiDocumentoFascicoloRaccoltaProvvisoria");
    private final static QName _RichiestaGetRisultatoOperazioneDocumento_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_getRisultatoOperazioneDocumento");
    private final static QName _RispostaMonitoraggioDocumenti_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_monitoraggioDocumenti");
    private final static QName _RispostaModifyMetadatiFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_modifyMetadatiFascicoloAttoDecreto");
    private final static QName _RispostaConvertiDocumento_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_convertiDocumento");
    private final static QName _RispostaAddDocumentoAggiuntivoFascicoloDecretoSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_addDocumentoAggiuntivoFascicoloDecretoSIPATR");
    private final static QName _RispostaModifyMetadatiFascicoloRaccoltaProvvisoria_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_modifyMetadatiFascicoloRaccoltaProvvisoria");
    private final static QName _RichiestaCopyFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_copyFascicoloAttoDecreto");
    private final static QName _RichiestaGetDocumentiFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_getDocumentiFascicoloAttoDecreto");
    private final static QName _RispostaModifyMetadataFascicoloDecretoSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_modifyMetadataFascicoloDecretoSIPATR");
    private final static QName _RispostaGetFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_getFascicoloSIPATR");
    private final static QName _RispostaAcquisizioneFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_acquisizioneFascicoloAttoDecreto");
    private final static QName _RichiestaGetFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_getFascicoloAttoDecreto");
    private final static QName _RichiestaChangeStatoFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_changeStatoFascicoloSIPATR");
    private final static QName _RichiestaDownloadDocumentoUrlFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_downloadDocumentoUrlFascicoloSIPATR");
    private final static QName _RispostaGetFascicoloSIPATRAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_getFascicoloSIPATRAttoDecreto");
    private final static QName _RispostaDownloadDocumentoAggiuntivoFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_downloadDocumentoAggiuntivoFascicoloAttoDecreto");
    private final static QName _RichiestaInviaDocAggiuntiviFascicoloAttoDecretoCorte_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_inviaDocAggiuntiviFascicoloAttoDecretoCorte");
    private final static QName _RichiestaSearchFascicoliRaccolteProvvisorie_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_searchFascicoliRaccolteProvvisorie");
    private final static QName _RichiestaAddDocumentoFascicoloAttoDecretoCorte_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_addDocumentoFascicoloAttoDecretoCorte");
    private final static QName _RispostaChangeStatoFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_changeStatoFascicoloSIPATR");
    private final static QName _RichiestaUpdateMetadatiDocumentoAggiuntivoFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_updateMetadatiDocumentoAggiuntivoFascicoloAttoDecreto");
    private final static QName _RispostaRemoveFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_removeFascicoloAttoDecreto");
    private final static QName _RichiestaGetFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_getFascicoloSIPATR");
    private final static QName _RichiestaRemoveFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_removeFascicoloSIPATR");
    private final static QName _RichiestaInviaFascicoloAttoDecretoCorte_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_inviaFascicoloAttoDecretoCorte");
    private final static QName _RichiestaModifyMetadataDocumentoFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_modifyMetadataDocumentoFascicoloSIPATR");
    private final static QName _RispostaGetDocumentoFascicoloRaccoltaProvvisoria_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_getDocumentoFascicoloRaccoltaProvvisoria");
    private final static QName _RichiestaRemoveDocumentoFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_removeDocumentoFascicoloAttoDecreto");
    private final static QName _RichiestaCreateFascicoloDecretoSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_createFascicoloDecretoSIPATR");
    private final static QName _RichiestaRemoveFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_removeFascicoloAttoDecreto");
    private final static QName _RispostaRemoveDocumentoAggiuntivoFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_removeDocumentoAggiuntivoFascicoloAttoDecreto");
    private final static QName _RichiestaModifyMetadataFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_modifyMetadataFascicoloSIPATR");
    private final static QName _RichiestaGetElencoDocumentiFascicolo_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_getElencoDocumentiFascicolo");
    private final static QName _RispostaGetDocumentoAggiuntivoFascicoloDecretoSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_getDocumentoAggiuntivoFascicoloDecretoSIPATR");
    private final static QName _RichiestaAddCollegamentoFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_addCollegamentoFascicoloSIPATR");
    private final static QName _RichiestaDownloadDocumentoUrlFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_downloadDocumentoUrlFascicoloAttoDecreto");
    private final static QName _RispostaDownloadDocumentoUrlFascicoloAllegatoDecretoIGB_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_downloadDocumentoUrlFascicoloAllegatoDecretoIGB");
    private final static QName _RichiestaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGB_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_modifyMetadatiDocumentoFascicoloAllegatoDecretoIGB");
    private final static QName _RichiestaChangeStatoFascicoloDecretoSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_changeStatoFascicoloDecretoSIPATR");
    private final static QName _RichiestaGetElencoDocumentiFascicoloAllegatoDecretoIGB_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_getElencoDocumentiFascicoloAllegatoDecretoIGB");
    private final static QName _RispostaGetFascicoloAttoDecretoCorte_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_getFascicoloAttoDecretoCorte");
    private final static QName _RichiestaGetFascicoloSIPATRAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_getFascicoloSIPATRAttoDecreto");
    private final static QName _RichiestaAddDocumentoFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_addDocumentoFascicoloAttoDecreto");
    private final static QName _RispostaRemoveFascicoloDecretoSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_removeFascicoloDecretoSIPATR");
    private final static QName _RispostaRemoveDocumentoFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_removeDocumentoFascicoloSIPATR");
    private final static QName _RispostaAddDocumentoAggiuntivoFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_addDocumentoAggiuntivoFascicoloAttoDecreto");
    private final static QName _RispostaRemoveDocumentoFascicoloAttoDecretoCorte_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_removeDocumentoFascicoloAttoDecretoCorte");
    private final static QName _RichiestaDownloadDocumentoFascicoloAttoDecretoCorte_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_downloadDocumentoFascicoloAttoDecretoCorte");
    private final static QName _RichiestaModifyMetadataFascicoloDecretoSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_modifyMetadataFascicoloDecretoSIPATR");
    private final static QName _RispostaGetElencoDocumentiFascicolo_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_getElencoDocumentiFascicolo");
    private final static QName _RispostaDownloadDocumentoFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_downloadDocumentoFascicoloSIPATR");
    private final static QName _RichiestaRemoveCollegamentoFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_removeCollegamentoFascicoloSIPATR");
    private final static QName _RichiestaRemoveDocumentoFascicoloAllegatoDecretoIGB_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_removeDocumentoFascicoloAllegatoDecretoIGB");
    private final static QName _RispostaMergeFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_mergeFascicoloAttoDecreto");
    private final static QName _RispostaAddDocumentoFascicoloAttoDecretoCorte_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_addDocumentoFascicoloAttoDecretoCorte");
    private final static QName _RispostaDownloadDocumentoUrlFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_downloadDocumentoUrlFascicoloSIPATR");
    private final static QName _RispostaGetDocumentoAggiuntivoFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_getDocumentoAggiuntivoFascicoloAttoDecreto");
    private final static QName _RispostaCopyFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_copyFascicoloAttoDecreto");
    private final static QName _RichiestaRemoveDocumentoAggiuntivoFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_removeDocumentoAggiuntivoFascicoloAttoDecreto");
    private final static QName _RichiestaGetDocumentoFascicoloSIPATRAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_getDocumentoFascicoloSIPATRAttoDecreto");
    private final static QName _RispostaDownloadDocumentoAggiuntivoFascicoloDecretoSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_downloadDocumentoAggiuntivoFascicoloDecretoSIPATR");
    private final static QName _RispostaAddDocumentoFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_addDocumentoFascicoloSIPATR");
    private final static QName _RispostaAddDocumentoFascicoloAllegatoDecretoIGB_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_addDocumentoFascicoloAllegatoDecretoIGB");
    private final static QName _RispostaDownloadDocumentoUrlFascicoloRaccoltaProvvisoria_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_downloadDocumentoUrlFascicoloRaccoltaProvvisoria");
    private final static QName _RispostaGetDocumentoFascicoloSIPATRAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_getDocumentoFascicoloSIPATRAttoDecreto");
    private final static QName _RichiestaGetDocumentoFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_getDocumentoFascicoloSIPATR");
    private final static QName _RichiestaGetDocumentoAggiuntivoFascicoloDecretoSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_getDocumentoAggiuntivoFascicoloDecretoSIPATR");
    private final static QName _RichiestaChangeStatoFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_changeStatoFascicoloAttoDecreto");
    private final static QName _RichiestaGetDocumentoAggiuntivoFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_getDocumentoAggiuntivoFascicoloAttoDecreto");
    private final static QName _RichiestaCreateFascicoloRaccoltaProvvisoria_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_createFascicoloRaccoltaProvvisoria");
    private final static QName _RichiestaGetDocumentoFascicoloAllegatoDecretoIGB_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_getDocumentoFascicoloAllegatoDecretoIGB");
    private final static QName _RispostaArchiveFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_archiveFascicoloAttoDecreto");
    private final static QName _RispostaInviaFascicoloAttoDecretoCorte_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_inviaFascicoloAttoDecretoCorte");
    private final static QName _RispostaModifyMetadataFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_modifyMetadataFascicoloSIPATR");
    private final static QName _RichiestaDownloadDocumentoFascicoloAllegatoDecretoIGB_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_downloadDocumentoFascicoloAllegatoDecretoIGB");
    private final static QName _RispostaUpdateContentDocumentoFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_updateContentDocumentoFascicoloAttoDecreto");
    private final static QName _RichiestaDownloadDocumentoAggiuntivoFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_downloadDocumentoAggiuntivoFascicoloAttoDecreto");
    private final static QName _RichiestaDownloadDocumentoUrlFascicoloAllegatoDecretoIGB_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_downloadDocumentoUrlFascicoloAllegatoDecretoIGB");
    private final static QName _RichiestaGetDocumentoFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_getDocumentoFascicoloAttoDecreto");
    private final static QName _RichiestaModifyMetadatiDocumentoFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_modifyMetadatiDocumentoFascicoloAttoDecreto");
    private final static QName _RispostaDownloadDocumentoFascicoloSIPATRAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_downloadDocumentoFascicoloSIPATRAttoDecreto");
    private final static QName _RispostaModifyMetadataDocumentoFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_modifyMetadataDocumentoFascicoloSIPATR");
    private final static QName _RichiestaCreateFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_createFascicoloAttoDecreto");
    private final static QName _RichiestaArchiveFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_archiveFascicoloAttoDecreto");
    private final static QName _RispostaUpdateMetadatiDocumentoAggiuntivoFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_updateMetadatiDocumentoAggiuntivoFascicoloAttoDecreto");
    private final static QName _RispostaGetElencoFascicoliRaccoltaProvvisoriaAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_getElencoFascicoliRaccoltaProvvisoriaAttoDecreto");
    private final static QName _RichiestaAddDocumentoFascicoloAllegatoDecretoIGB_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_addDocumentoFascicoloAllegatoDecretoIGB");
    private final static QName _RichiestaDownloadDocumentoFascicoloRaccoltaProvvisoria_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_downloadDocumentoFascicoloRaccoltaProvvisoria");
    private final static QName _RispostaInviaDocAggiuntiviFascicoloAttoDecretoCorte_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_inviaDocAggiuntiviFascicoloAttoDecretoCorte");
    private final static QName _RispostaRemoveFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_removeFascicoloSIPATR");
    private final static QName _RispostaGetDocumentoFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_getDocumentoFascicoloSIPATR");
    private final static QName _RichiestaMergeFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_mergeFascicoloAttoDecreto");
    private final static QName _RichiestaGetFascicoloAttoDecretoCorte_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_getFascicoloAttoDecretoCorte");
    private final static QName _RichiestaDownloadDocumentoFascicoloSIPATRAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_downloadDocumentoFascicoloSIPATRAttoDecreto");
    private final static QName _RichiestaMonitoraggioDocumenti_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_monitoraggioDocumenti");
    private final static QName _RichiestaAddDocumentoAggiuntivoFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_addDocumentoAggiuntivoFascicoloAttoDecreto");
    private final static QName _RichiestaUpdateContentDocumentoFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_updateContentDocumentoFascicoloAttoDecreto");
    private final static QName _RichiestaConvertiDocumento_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_convertiDocumento");
    private final static QName _RispostaChangeStatoFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_changeStatoFascicoloAttoDecreto");
    private final static QName _RichiestaCreateFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_createFascicoloSIPATR");
    private final static QName _RichiestaDownloadDocumentoAggiuntivoFascicoloDecretoSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_downloadDocumentoAggiuntivoFascicoloDecretoSIPATR");
    private final static QName _RichiestaChangeStatoFascicoloRaccoltaProvvisoria_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_changeStatoFascicoloRaccoltaProvvisoria");
    private final static QName _RichiestaRemoveDocumentoFascicoloRaccoltaProvvisoria_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_removeDocumentoFascicoloRaccoltaProvvisoria");
    private final static QName _RispostaCreateFascicoloRaccoltaProvvisoria_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_createFascicoloRaccoltaProvvisoria");
    private final static QName _RichiestaDownloadDocumentoFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_downloadDocumentoFascicoloAttoDecreto");
    private final static QName _RichiestaModifyMetadatiFascicoloRaccoltaProvvisoria_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_modifyMetadatiFascicoloRaccoltaProvvisoria");
    private final static QName _RispostaRemoveCollegamentoFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_removeCollegamentoFascicoloSIPATR");
    private final static QName _RispostaRemoveDocumentoAggiuntivoFascicoloDecretoSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_removeDocumentoAggiuntivoFascicoloDecretoSIPATR");
    private final static QName _RispostaAddDocumentoFascicoloRaccoltaProvvisoria_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_addDocumentoFascicoloRaccoltaProvvisoria");
    private final static QName _RispostaGetDocumentoFascicoloAllegatoDecretoIGB_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_getDocumentoFascicoloAllegatoDecretoIGB");
    private final static QName _RispostaRemoveDocumentoFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_removeDocumentoFascicoloAttoDecreto");
    private final static QName _RichiestaGetFascicoloDecretoSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_getFascicoloDecretoSIPATR");
    private final static QName _RichiestaRemoveDocumentoFascicoloAttoDecretoCorte_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_removeDocumentoFascicoloAttoDecretoCorte");
    private final static QName _RispostaRemoveDocumentoFascicoloAllegatoDecretoIGB_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_removeDocumentoFascicoloAllegatoDecretoIGB");
    private final static QName _RichiestaGetElencoFascicoliRaccoltaProvvisoriaAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_getElencoFascicoliRaccoltaProvvisoriaAttoDecreto");
    private final static QName _RispostaSearchFascicoliRaccolteProvvisorie_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_searchFascicoliRaccolteProvvisorie");
    private final static QName _RichiestaRemoveFascicoloDecretoSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_removeFascicoloDecretoSIPATR");
    private final static QName _RispostaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGB_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_modifyMetadatiDocumentoFascicoloAllegatoDecretoIGB");
    private final static QName _RispostaChangeStatoFascicoloRaccoltaProvvisoria_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_changeStatoFascicoloRaccoltaProvvisoria");
    private final static QName _RispostaGetDocumentoFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_getDocumentoFascicoloAttoDecreto");
    private final static QName _RispostaChangeStatoFascicoloDecretoSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_changeStatoFascicoloDecretoSIPATR");
    private final static QName _RichiestaRemoveDocumentoAggiuntivoFascicoloDecretoSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_removeDocumentoAggiuntivoFascicoloDecretoSIPATR");
    private final static QName _RichiestaDownloadDocumentoUrlFascicoloRaccoltaProvvisoria_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_downloadDocumentoUrlFascicoloRaccoltaProvvisoria");
    private final static QName _RispostaDownloadDocumentoFascicoloAttoDecretoCorte_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_downloadDocumentoFascicoloAttoDecretoCorte");
    private final static QName _RispostaDownloadDocumentoFascicoloAllegatoDecretoIGB_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_downloadDocumentoFascicoloAllegatoDecretoIGB");
    private final static QName _RispostaGetElencoDocumentiFascicoloAllegatoDecretoIGB_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_getElencoDocumentiFascicoloAllegatoDecretoIGB");
    private final static QName _RichiestaAddDocumentoFascicoloRaccoltaProvvisoria_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_addDocumentoFascicoloRaccoltaProvvisoria");
    private final static QName _RispostaModifyMetadatiDocumentoFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_modifyMetadatiDocumentoFascicoloAttoDecreto");
    private final static QName _RichiestaGetDocumentoFascicoloRaccoltaProvvisoria_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_getDocumentoFascicoloRaccoltaProvvisoria");
    private final static QName _RispostaGetFascicoloDecretoSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_getFascicoloDecretoSIPATR");
    private final static QName _RichiestaAcquisizioneFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_acquisizioneFascicoloAttoDecreto");
    private final static QName _RichiestaGetFascicoloRaccoltaProvvisoria_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_getFascicoloRaccoltaProvvisoria");
    private final static QName _RichiestaModifyMetadatiFascicoloAttoDecreto_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_modifyMetadatiFascicoloAttoDecreto");
    private final static QName _RichiestaAddDocumentoAggiuntivoFascicoloDecretoSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_addDocumentoAggiuntivoFascicoloDecretoSIPATR");
    private final static QName _RichiestaAddDocumentoFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "richiesta_addDocumentoFascicoloSIPATR");
    private final static QName _RispostaAddCollegamentoFascicoloSIPATR_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_addCollegamentoFascicoloSIPATR");
    private final static QName _RispostaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoria_QNAME = new QName("http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", "risposta_modifyMetadatiDocumentoFascicoloRaccoltaProvvisoria");
    private final static QName _DocumentoFascicoloDecretoSIPATRTypeTicketArchiviazione_QNAME = new QName("", "TicketArchiviazione");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RichiestaOperazioneDocumentaleType }
     * 
     */
    public RichiestaOperazioneDocumentaleType createRichiestaOperazioneDocumentaleType() {
        return new RichiestaOperazioneDocumentaleType();
    }

    /**
     * Create an instance of {@link RisultatoOperazioneDocumentoType }
     * 
     */
    public RisultatoOperazioneDocumentoType createRisultatoOperazioneDocumentoType() {
        return new RisultatoOperazioneDocumentoType();
    }

    /**
     * Create an instance of {@link RichiestaAddCollegamentoFascicoloSIPATRType }
     * 
     */
    public RichiestaAddCollegamentoFascicoloSIPATRType createRichiestaAddCollegamentoFascicoloSIPATRType() {
        return new RichiestaAddCollegamentoFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RichiestaGetElencoDocumentiFascicoloType }
     * 
     */
    public RichiestaGetElencoDocumentiFascicoloType createRichiestaGetElencoDocumentiFascicoloType() {
        return new RichiestaGetElencoDocumentiFascicoloType();
    }

    /**
     * Create an instance of {@link RispostaDownloadDocumentoUrlFascicoloAllegatoDecretoIGBType }
     * 
     */
    public RispostaDownloadDocumentoUrlFascicoloAllegatoDecretoIGBType createRispostaDownloadDocumentoUrlFascicoloAllegatoDecretoIGBType() {
        return new RispostaDownloadDocumentoUrlFascicoloAllegatoDecretoIGBType();
    }

    /**
     * Create an instance of {@link RichiestaDownloadDocumentoUrlFascicoloAttoDecretoType }
     * 
     */
    public RichiestaDownloadDocumentoUrlFascicoloAttoDecretoType createRichiestaDownloadDocumentoUrlFascicoloAttoDecretoType() {
        return new RichiestaDownloadDocumentoUrlFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RispostaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType }
     * 
     */
    public RispostaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType createRispostaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType() {
        return new RispostaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType();
    }

    /**
     * Create an instance of {@link RichiestaRemoveFascicoloAttoDecretoType }
     * 
     */
    public RichiestaRemoveFascicoloAttoDecretoType createRichiestaRemoveFascicoloAttoDecretoType() {
        return new RichiestaRemoveFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RichiestaCreateFascicoloDecretoSIPATRType }
     * 
     */
    public RichiestaCreateFascicoloDecretoSIPATRType createRichiestaCreateFascicoloDecretoSIPATRType() {
        return new RichiestaCreateFascicoloDecretoSIPATRType();
    }

    /**
     * Create an instance of {@link RichiestaModifyMetadataFascicoloSIPATRType }
     * 
     */
    public RichiestaModifyMetadataFascicoloSIPATRType createRichiestaModifyMetadataFascicoloSIPATRType() {
        return new RichiestaModifyMetadataFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RispostaRemoveDocumentoAggiuntivoFascicoloAttoDecretoType }
     * 
     */
    public RispostaRemoveDocumentoAggiuntivoFascicoloAttoDecretoType createRispostaRemoveDocumentoAggiuntivoFascicoloAttoDecretoType() {
        return new RispostaRemoveDocumentoAggiuntivoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RichiestaModifyMetadataDocumentoFascicoloSIPATRType }
     * 
     */
    public RichiestaModifyMetadataDocumentoFascicoloSIPATRType createRichiestaModifyMetadataDocumentoFascicoloSIPATRType() {
        return new RichiestaModifyMetadataDocumentoFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RichiestaInviaFascicoloAttoDecretoCorteType }
     * 
     */
    public RichiestaInviaFascicoloAttoDecretoCorteType createRichiestaInviaFascicoloAttoDecretoCorteType() {
        return new RichiestaInviaFascicoloAttoDecretoCorteType();
    }

    /**
     * Create an instance of {@link RichiestaRemoveFascicoloSIPATRType }
     * 
     */
    public RichiestaRemoveFascicoloSIPATRType createRichiestaRemoveFascicoloSIPATRType() {
        return new RichiestaRemoveFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RispostaRemoveFascicoloAttoDecretoType }
     * 
     */
    public RispostaRemoveFascicoloAttoDecretoType createRispostaRemoveFascicoloAttoDecretoType() {
        return new RispostaRemoveFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RichiestaGetFascicoloSIPATRType }
     * 
     */
    public RichiestaGetFascicoloSIPATRType createRichiestaGetFascicoloSIPATRType() {
        return new RichiestaGetFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RichiestaRemoveDocumentoFascicoloAttoDecretoType }
     * 
     */
    public RichiestaRemoveDocumentoFascicoloAttoDecretoType createRichiestaRemoveDocumentoFascicoloAttoDecretoType() {
        return new RichiestaRemoveDocumentoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RispostaGetDocumentoFascicoloRaccoltaProvvisoriaType }
     * 
     */
    public RispostaGetDocumentoFascicoloRaccoltaProvvisoriaType createRispostaGetDocumentoFascicoloRaccoltaProvvisoriaType() {
        return new RispostaGetDocumentoFascicoloRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link RichiestaUpdateMetadatiDocumentoAggiuntivoFascicoloAttoDecretoType }
     * 
     */
    public RichiestaUpdateMetadatiDocumentoAggiuntivoFascicoloAttoDecretoType createRichiestaUpdateMetadatiDocumentoAggiuntivoFascicoloAttoDecretoType() {
        return new RichiestaUpdateMetadatiDocumentoAggiuntivoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RichiestaAddDocumentoFascicoloAttoDecretoCorteType }
     * 
     */
    public RichiestaAddDocumentoFascicoloAttoDecretoCorteType createRichiestaAddDocumentoFascicoloAttoDecretoCorteType() {
        return new RichiestaAddDocumentoFascicoloAttoDecretoCorteType();
    }

    /**
     * Create an instance of {@link RispostaChangeStatoFascicoloSIPATRType }
     * 
     */
    public RispostaChangeStatoFascicoloSIPATRType createRispostaChangeStatoFascicoloSIPATRType() {
        return new RispostaChangeStatoFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RichiestaInviaDocAggiuntiviFascicoloAttoDecretoCorteType }
     * 
     */
    public RichiestaInviaDocAggiuntiviFascicoloAttoDecretoCorteType createRichiestaInviaDocAggiuntiviFascicoloAttoDecretoCorteType() {
        return new RichiestaInviaDocAggiuntiviFascicoloAttoDecretoCorteType();
    }

    /**
     * Create an instance of {@link RichiestaSearchFascicoliRaccolteProvvisorieType }
     * 
     */
    public RichiestaSearchFascicoliRaccolteProvvisorieType createRichiestaSearchFascicoliRaccolteProvvisorieType() {
        return new RichiestaSearchFascicoliRaccolteProvvisorieType();
    }

    /**
     * Create an instance of {@link RispostaGetFascicoloSIPATRAttoDecretoType }
     * 
     */
    public RispostaGetFascicoloSIPATRAttoDecretoType createRispostaGetFascicoloSIPATRAttoDecretoType() {
        return new RispostaGetFascicoloSIPATRAttoDecretoType();
    }

    /**
     * Create an instance of {@link RispostaDownloadDocumentoAggiuntivoFascicoloAttoDecretoType }
     * 
     */
    public RispostaDownloadDocumentoAggiuntivoFascicoloAttoDecretoType createRispostaDownloadDocumentoAggiuntivoFascicoloAttoDecretoType() {
        return new RispostaDownloadDocumentoAggiuntivoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RichiestaGetFascicoloAttoDecretoType }
     * 
     */
    public RichiestaGetFascicoloAttoDecretoType createRichiestaGetFascicoloAttoDecretoType() {
        return new RichiestaGetFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RispostaAcquisizioneFascicoloAttoDecretoType }
     * 
     */
    public RispostaAcquisizioneFascicoloAttoDecretoType createRispostaAcquisizioneFascicoloAttoDecretoType() {
        return new RispostaAcquisizioneFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RispostaGetFascicoloSIPATRType }
     * 
     */
    public RispostaGetFascicoloSIPATRType createRispostaGetFascicoloSIPATRType() {
        return new RispostaGetFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RichiestaDownloadDocumentoUrlFascicoloSIPATRType }
     * 
     */
    public RichiestaDownloadDocumentoUrlFascicoloSIPATRType createRichiestaDownloadDocumentoUrlFascicoloSIPATRType() {
        return new RichiestaDownloadDocumentoUrlFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RichiestaChangeStatoFascicoloSIPATRType }
     * 
     */
    public RichiestaChangeStatoFascicoloSIPATRType createRichiestaChangeStatoFascicoloSIPATRType() {
        return new RichiestaChangeStatoFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RispostaModifyMetadataFascicoloRaccoltaProvvisoriaType }
     * 
     */
    public RispostaModifyMetadataFascicoloRaccoltaProvvisoriaType createRispostaModifyMetadataFascicoloRaccoltaProvvisoriaType() {
        return new RispostaModifyMetadataFascicoloRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link RispostaAddDocumentoAggiuntivoFascicoloDecretoSIPATRType }
     * 
     */
    public RispostaAddDocumentoAggiuntivoFascicoloDecretoSIPATRType createRispostaAddDocumentoAggiuntivoFascicoloDecretoSIPATRType() {
        return new RispostaAddDocumentoAggiuntivoFascicoloDecretoSIPATRType();
    }

    /**
     * Create an instance of {@link RispostaConvertiDocumentoType }
     * 
     */
    public RispostaConvertiDocumentoType createRispostaConvertiDocumentoType() {
        return new RispostaConvertiDocumentoType();
    }

    /**
     * Create an instance of {@link RispostaModifyMetadataFascicoloAttoDecretoType }
     * 
     */
    public RispostaModifyMetadataFascicoloAttoDecretoType createRispostaModifyMetadataFascicoloAttoDecretoType() {
        return new RispostaModifyMetadataFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RispostaGetMonitoraggioDocumentiType }
     * 
     */
    public RispostaGetMonitoraggioDocumentiType createRispostaGetMonitoraggioDocumentiType() {
        return new RispostaGetMonitoraggioDocumentiType();
    }

    /**
     * Create an instance of {@link RichiestaGetRisultatoOperazioneDocumentoType }
     * 
     */
    public RichiestaGetRisultatoOperazioneDocumentoType createRichiestaGetRisultatoOperazioneDocumentoType() {
        return new RichiestaGetRisultatoOperazioneDocumentoType();
    }

    /**
     * Create an instance of {@link RispostaModifyMetadataFascicoloDecretoSIPATRType }
     * 
     */
    public RispostaModifyMetadataFascicoloDecretoSIPATRType createRispostaModifyMetadataFascicoloDecretoSIPATRType() {
        return new RispostaModifyMetadataFascicoloDecretoSIPATRType();
    }

    /**
     * Create an instance of {@link RichiestaCopyFascicoloAttoDecretoType }
     * 
     */
    public RichiestaCopyFascicoloAttoDecretoType createRichiestaCopyFascicoloAttoDecretoType() {
        return new RichiestaCopyFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RispostaRemoveDocumentoFascicoloRaccoltaProvvisoriaType }
     * 
     */
    public RispostaRemoveDocumentoFascicoloRaccoltaProvvisoriaType createRispostaRemoveDocumentoFascicoloRaccoltaProvvisoriaType() {
        return new RispostaRemoveDocumentoFascicoloRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link RispostaDownloadDocumentoFascicoloRaccoltaProvvisoriaType }
     * 
     */
    public RispostaDownloadDocumentoFascicoloRaccoltaProvvisoriaType createRispostaDownloadDocumentoFascicoloRaccoltaProvvisoriaType() {
        return new RispostaDownloadDocumentoFascicoloRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link RichiestaDownloadDocumentoFascicoloSIPATRType }
     * 
     */
    public RichiestaDownloadDocumentoFascicoloSIPATRType createRichiestaDownloadDocumentoFascicoloSIPATRType() {
        return new RichiestaDownloadDocumentoFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RichiestaRemoveDocumentoFascicoloSIPATRType }
     * 
     */
    public RichiestaRemoveDocumentoFascicoloSIPATRType createRichiestaRemoveDocumentoFascicoloSIPATRType() {
        return new RichiestaRemoveDocumentoFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RichiestaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType }
     * 
     */
    public RichiestaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType createRichiestaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType() {
        return new RichiestaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link RichiestaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType }
     * 
     */
    public RichiestaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType createRichiestaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType() {
        return new RichiestaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType();
    }

    /**
     * Create an instance of {@link RispostaGetFascicoloRaccoltaProvvisoria }
     * 
     */
    public RispostaGetFascicoloRaccoltaProvvisoria createRispostaGetFascicoloRaccoltaProvvisoria() {
        return new RispostaGetFascicoloRaccoltaProvvisoria();
    }

    /**
     * Create an instance of {@link RispostaGetFascicoloRaccoltaProvvisoriaType }
     * 
     */
    public RispostaGetFascicoloRaccoltaProvvisoriaType createRispostaGetFascicoloRaccoltaProvvisoriaType() {
        return new RispostaGetFascicoloRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link ServiceErrorType }
     * 
     */
    public ServiceErrorType createServiceErrorType() {
        return new ServiceErrorType();
    }

    /**
     * Create an instance of {@link FascicoloRaccoltaProvvisoriaType }
     * 
     */
    public FascicoloRaccoltaProvvisoriaType createFascicoloRaccoltaProvvisoriaType() {
        return new FascicoloRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link RichiestaGetDocumentoFascicoloSIPATRType }
     * 
     */
    public RichiestaGetDocumentoFascicoloSIPATRType createRichiestaGetDocumentoFascicoloSIPATRType() {
        return new RichiestaGetDocumentoFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RispostaDownloadDocumentoUrlFascicoloAttoDecreto }
     * 
     */
    public RispostaDownloadDocumentoUrlFascicoloAttoDecreto createRispostaDownloadDocumentoUrlFascicoloAttoDecreto() {
        return new RispostaDownloadDocumentoUrlFascicoloAttoDecreto();
    }

    /**
     * Create an instance of {@link RispostaDownloadDocumentoUrlFascicoloAttoDecretoType }
     * 
     */
    public RispostaDownloadDocumentoUrlFascicoloAttoDecretoType createRispostaDownloadDocumentoUrlFascicoloAttoDecretoType() {
        return new RispostaDownloadDocumentoUrlFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RispostaAddDocumentoFascicoloAttoDecreto }
     * 
     */
    public RispostaAddDocumentoFascicoloAttoDecreto createRispostaAddDocumentoFascicoloAttoDecreto() {
        return new RispostaAddDocumentoFascicoloAttoDecreto();
    }

    /**
     * Create an instance of {@link RispostaAddDocumentoFascicoloAttoDecretoType }
     * 
     */
    public RispostaAddDocumentoFascicoloAttoDecretoType createRispostaAddDocumentoFascicoloAttoDecretoType() {
        return new RispostaAddDocumentoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RisultatoAddDocumentoType }
     * 
     */
    public RisultatoAddDocumentoType createRisultatoAddDocumentoType() {
        return new RisultatoAddDocumentoType();
    }

    /**
     * Create an instance of {@link RispostaGetDocumentoFascicoloSIPATRAttoDecretoType }
     * 
     */
    public RispostaGetDocumentoFascicoloSIPATRAttoDecretoType createRispostaGetDocumentoFascicoloSIPATRAttoDecretoType() {
        return new RispostaGetDocumentoFascicoloSIPATRAttoDecretoType();
    }

    /**
     * Create an instance of {@link RispostaDownloadDocumentoUrlFascicoloRaccoltaProvvisoriaType }
     * 
     */
    public RispostaDownloadDocumentoUrlFascicoloRaccoltaProvvisoriaType createRispostaDownloadDocumentoUrlFascicoloRaccoltaProvvisoriaType() {
        return new RispostaDownloadDocumentoUrlFascicoloRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link RispostaAddDocumentoFascicoloAllegatoDecretoIGBType }
     * 
     */
    public RispostaAddDocumentoFascicoloAllegatoDecretoIGBType createRispostaAddDocumentoFascicoloAllegatoDecretoIGBType() {
        return new RispostaAddDocumentoFascicoloAllegatoDecretoIGBType();
    }

    /**
     * Create an instance of {@link RispostaDownloadDocumentoFascicoloSIPATRType }
     * 
     */
    public RispostaDownloadDocumentoFascicoloSIPATRType createRispostaDownloadDocumentoFascicoloSIPATRType() {
        return new RispostaDownloadDocumentoFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RispostaAddDocumentoFascicoloSIPATRType }
     * 
     */
    public RispostaAddDocumentoFascicoloSIPATRType createRispostaAddDocumentoFascicoloSIPATRType() {
        return new RispostaAddDocumentoFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RispostaGetDocumentoAggiuntivoFascicoloAttoDecretoType }
     * 
     */
    public RispostaGetDocumentoAggiuntivoFascicoloAttoDecretoType createRispostaGetDocumentoAggiuntivoFascicoloAttoDecretoType() {
        return new RispostaGetDocumentoAggiuntivoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RispostaCopyFascicoloAttoDecretoType }
     * 
     */
    public RispostaCopyFascicoloAttoDecretoType createRispostaCopyFascicoloAttoDecretoType() {
        return new RispostaCopyFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RispostaDownloadDocumentoUrlFascicoloSIPATRType }
     * 
     */
    public RispostaDownloadDocumentoUrlFascicoloSIPATRType createRispostaDownloadDocumentoUrlFascicoloSIPATRType() {
        return new RispostaDownloadDocumentoUrlFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RichiestaGetDocumentoFascicoloSIPATRAttoDecretoType }
     * 
     */
    public RichiestaGetDocumentoFascicoloSIPATRAttoDecretoType createRichiestaGetDocumentoFascicoloSIPATRAttoDecretoType() {
        return new RichiestaGetDocumentoFascicoloSIPATRAttoDecretoType();
    }

    /**
     * Create an instance of {@link RichiestaRemoveDocumentoAggiuntivoFascicoloAttoDecretoType }
     * 
     */
    public RichiestaRemoveDocumentoAggiuntivoFascicoloAttoDecretoType createRichiestaRemoveDocumentoAggiuntivoFascicoloAttoDecretoType() {
        return new RichiestaRemoveDocumentoAggiuntivoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RispostaGetElencoDocumentiFascicoloType }
     * 
     */
    public RispostaGetElencoDocumentiFascicoloType createRispostaGetElencoDocumentiFascicoloType() {
        return new RispostaGetElencoDocumentiFascicoloType();
    }

    /**
     * Create an instance of {@link RispostaAddDocumentoFascicoloAttoDecretoCorteType }
     * 
     */
    public RispostaAddDocumentoFascicoloAttoDecretoCorteType createRispostaAddDocumentoFascicoloAttoDecretoCorteType() {
        return new RispostaAddDocumentoFascicoloAttoDecretoCorteType();
    }

    /**
     * Create an instance of {@link RispostaMergeFascicoloAttoDecretoType }
     * 
     */
    public RispostaMergeFascicoloAttoDecretoType createRispostaMergeFascicoloAttoDecretoType() {
        return new RispostaMergeFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RichiestaRemoveCollegamentoFascicoloSIPATRType }
     * 
     */
    public RichiestaRemoveCollegamentoFascicoloSIPATRType createRichiestaRemoveCollegamentoFascicoloSIPATRType() {
        return new RichiestaRemoveCollegamentoFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RichiestaRemoveDocumentoFascicoloAllegatoDecretoIGBType }
     * 
     */
    public RichiestaRemoveDocumentoFascicoloAllegatoDecretoIGBType createRichiestaRemoveDocumentoFascicoloAllegatoDecretoIGBType() {
        return new RichiestaRemoveDocumentoFascicoloAllegatoDecretoIGBType();
    }

    /**
     * Create an instance of {@link RispostaRemoveFascicoloDecretoSIPATRType }
     * 
     */
    public RispostaRemoveFascicoloDecretoSIPATRType createRispostaRemoveFascicoloDecretoSIPATRType() {
        return new RispostaRemoveFascicoloDecretoSIPATRType();
    }

    /**
     * Create an instance of {@link RispostaRemoveDocumentoFascicoloSIPATRType }
     * 
     */
    public RispostaRemoveDocumentoFascicoloSIPATRType createRispostaRemoveDocumentoFascicoloSIPATRType() {
        return new RispostaRemoveDocumentoFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RichiestaAddDocumentoFascicoloAttoDecretoType }
     * 
     */
    public RichiestaAddDocumentoFascicoloAttoDecretoType createRichiestaAddDocumentoFascicoloAttoDecretoType() {
        return new RichiestaAddDocumentoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RichiestaDownloadDocumentoFascicoloAttoDecretoCorteType }
     * 
     */
    public RichiestaDownloadDocumentoFascicoloAttoDecretoCorteType createRichiestaDownloadDocumentoFascicoloAttoDecretoCorteType() {
        return new RichiestaDownloadDocumentoFascicoloAttoDecretoCorteType();
    }

    /**
     * Create an instance of {@link RichiestaModifyMetadataFascicoloDecretoSIPATRType }
     * 
     */
    public RichiestaModifyMetadataFascicoloDecretoSIPATRType createRichiestaModifyMetadataFascicoloDecretoSIPATRType() {
        return new RichiestaModifyMetadataFascicoloDecretoSIPATRType();
    }

    /**
     * Create an instance of {@link RispostaRemoveDocumentoFascicoloAttoDecretoCorteType }
     * 
     */
    public RispostaRemoveDocumentoFascicoloAttoDecretoCorteType createRispostaRemoveDocumentoFascicoloAttoDecretoCorteType() {
        return new RispostaRemoveDocumentoFascicoloAttoDecretoCorteType();
    }

    /**
     * Create an instance of {@link RispostaAddDocumentoAggiuntivoFascicoloAttoDecretoType }
     * 
     */
    public RispostaAddDocumentoAggiuntivoFascicoloAttoDecretoType createRispostaAddDocumentoAggiuntivoFascicoloAttoDecretoType() {
        return new RispostaAddDocumentoAggiuntivoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RichiestaGetElencoDocumentiFascicoloAllegatoDecretoIGBType }
     * 
     */
    public RichiestaGetElencoDocumentiFascicoloAllegatoDecretoIGBType createRichiestaGetElencoDocumentiFascicoloAllegatoDecretoIGBType() {
        return new RichiestaGetElencoDocumentiFascicoloAllegatoDecretoIGBType();
    }

    /**
     * Create an instance of {@link RispostaCreateFascicoloDecretoSIPATR }
     * 
     */
    public RispostaCreateFascicoloDecretoSIPATR createRispostaCreateFascicoloDecretoSIPATR() {
        return new RispostaCreateFascicoloDecretoSIPATR();
    }

    /**
     * Create an instance of {@link RispostaCreateFascicoloDecretoSIPATRType }
     * 
     */
    public RispostaCreateFascicoloDecretoSIPATRType createRispostaCreateFascicoloDecretoSIPATRType() {
        return new RispostaCreateFascicoloDecretoSIPATRType();
    }

    /**
     * Create an instance of {@link FascicoloDecretoSIPATRType }
     * 
     */
    public FascicoloDecretoSIPATRType createFascicoloDecretoSIPATRType() {
        return new FascicoloDecretoSIPATRType();
    }

    /**
     * Create an instance of {@link RispostaGetFascicoloAttoDecretoCorteType }
     * 
     */
    public RispostaGetFascicoloAttoDecretoCorteType createRispostaGetFascicoloAttoDecretoCorteType() {
        return new RispostaGetFascicoloAttoDecretoCorteType();
    }

    /**
     * Create an instance of {@link RichiestaGetFascicoloSIPATRAttoDecretoType }
     * 
     */
    public RichiestaGetFascicoloSIPATRAttoDecretoType createRichiestaGetFascicoloSIPATRAttoDecretoType() {
        return new RichiestaGetFascicoloSIPATRAttoDecretoType();
    }

    /**
     * Create an instance of {@link RichiestaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGBType }
     * 
     */
    public RichiestaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGBType createRichiestaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGBType() {
        return new RichiestaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGBType();
    }

    /**
     * Create an instance of {@link RichiestaChangeStatoFascicoloDecretoSIPATRType }
     * 
     */
    public RichiestaChangeStatoFascicoloDecretoSIPATRType createRichiestaChangeStatoFascicoloDecretoSIPATRType() {
        return new RichiestaChangeStatoFascicoloDecretoSIPATRType();
    }

    /**
     * Create an instance of {@link RichiestaChangeStatoFascicoloRaccoltaProvvisoriaType }
     * 
     */
    public RichiestaChangeStatoFascicoloRaccoltaProvvisoriaType createRichiestaChangeStatoFascicoloRaccoltaProvvisoriaType() {
        return new RichiestaChangeStatoFascicoloRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link RichiestaRemoveDocumentoFascicoloRaccoltaProvvisoriaType }
     * 
     */
    public RichiestaRemoveDocumentoFascicoloRaccoltaProvvisoriaType createRichiestaRemoveDocumentoFascicoloRaccoltaProvvisoriaType() {
        return new RichiestaRemoveDocumentoFascicoloRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link RispostaCreateFascicoloRaccoltaProvvisoriaType }
     * 
     */
    public RispostaCreateFascicoloRaccoltaProvvisoriaType createRispostaCreateFascicoloRaccoltaProvvisoriaType() {
        return new RispostaCreateFascicoloRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link RispostaDownloadDocumentoFascicoloAttoDecreto }
     * 
     */
    public RispostaDownloadDocumentoFascicoloAttoDecreto createRispostaDownloadDocumentoFascicoloAttoDecreto() {
        return new RispostaDownloadDocumentoFascicoloAttoDecreto();
    }

    /**
     * Create an instance of {@link RispostaDownloadDocumentoFascicoloAttoDecretoType }
     * 
     */
    public RispostaDownloadDocumentoFascicoloAttoDecretoType createRispostaDownloadDocumentoFascicoloAttoDecretoType() {
        return new RispostaDownloadDocumentoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link DocumentoContentType }
     * 
     */
    public DocumentoContentType createDocumentoContentType() {
        return new DocumentoContentType();
    }

    /**
     * Create an instance of {@link RichiestaDownloadDocumentoFascicoloAttoDecretoType }
     * 
     */
    public RichiestaDownloadDocumentoFascicoloAttoDecretoType createRichiestaDownloadDocumentoFascicoloAttoDecretoType() {
        return new RichiestaDownloadDocumentoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RichiestaConvertiDocumentoType }
     * 
     */
    public RichiestaConvertiDocumentoType createRichiestaConvertiDocumentoType() {
        return new RichiestaConvertiDocumentoType();
    }

    /**
     * Create an instance of {@link RichiestaAddDocumentoAggiuntivoFascicoloAttoDecretoType }
     * 
     */
    public RichiestaAddDocumentoAggiuntivoFascicoloAttoDecretoType createRichiestaAddDocumentoAggiuntivoFascicoloAttoDecretoType() {
        return new RichiestaAddDocumentoAggiuntivoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RichiestaUpdateContentDocumentoFascicoloAttoDecretoType }
     * 
     */
    public RichiestaUpdateContentDocumentoFascicoloAttoDecretoType createRichiestaUpdateContentDocumentoFascicoloAttoDecretoType() {
        return new RichiestaUpdateContentDocumentoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RichiestaCreateFascicoloSIPATRType }
     * 
     */
    public RichiestaCreateFascicoloSIPATRType createRichiestaCreateFascicoloSIPATRType() {
        return new RichiestaCreateFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RispostaChangeStatoFascicoloAttoDecretoType }
     * 
     */
    public RispostaChangeStatoFascicoloAttoDecretoType createRispostaChangeStatoFascicoloAttoDecretoType() {
        return new RispostaChangeStatoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RispostaRemoveFascicoloSIPATRType }
     * 
     */
    public RispostaRemoveFascicoloSIPATRType createRispostaRemoveFascicoloSIPATRType() {
        return new RispostaRemoveFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RispostaGetDocumentoFascicoloSIPATRType }
     * 
     */
    public RispostaGetDocumentoFascicoloSIPATRType createRispostaGetDocumentoFascicoloSIPATRType() {
        return new RispostaGetDocumentoFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RispostaInviaFascicoloAttoDecretoCorteType }
     * 
     */
    public RispostaInviaFascicoloAttoDecretoCorteType createRispostaInviaFascicoloAttoDecretoCorteType() {
        return new RispostaInviaFascicoloAttoDecretoCorteType();
    }

    /**
     * Create an instance of {@link RichiestaGetMonitoraggioDocumentiType }
     * 
     */
    public RichiestaGetMonitoraggioDocumentiType createRichiestaGetMonitoraggioDocumentiType() {
        return new RichiestaGetMonitoraggioDocumentiType();
    }

    /**
     * Create an instance of {@link RichiestaGetFascicoloAttoDecretoCorteType }
     * 
     */
    public RichiestaGetFascicoloAttoDecretoCorteType createRichiestaGetFascicoloAttoDecretoCorteType() {
        return new RichiestaGetFascicoloAttoDecretoCorteType();
    }

    /**
     * Create an instance of {@link RichiestaDownloadDocumentoFascicoloDecretoSIPATRType }
     * 
     */
    public RichiestaDownloadDocumentoFascicoloDecretoSIPATRType createRichiestaDownloadDocumentoFascicoloDecretoSIPATRType() {
        return new RichiestaDownloadDocumentoFascicoloDecretoSIPATRType();
    }

    /**
     * Create an instance of {@link RichiestaMergeFascicoloAttoDecretoType }
     * 
     */
    public RichiestaMergeFascicoloAttoDecretoType createRichiestaMergeFascicoloAttoDecretoType() {
        return new RichiestaMergeFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RichiestaDownloadDocumentoFascicoloRaccoltaProvvisoriaType }
     * 
     */
    public RichiestaDownloadDocumentoFascicoloRaccoltaProvvisoriaType createRichiestaDownloadDocumentoFascicoloRaccoltaProvvisoriaType() {
        return new RichiestaDownloadDocumentoFascicoloRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link RichiestaAddDocumentoFascicoloAllegatoDecretoIGBType }
     * 
     */
    public RichiestaAddDocumentoFascicoloAllegatoDecretoIGBType createRichiestaAddDocumentoFascicoloAllegatoDecretoIGBType() {
        return new RichiestaAddDocumentoFascicoloAllegatoDecretoIGBType();
    }

    /**
     * Create an instance of {@link RispostaCreateFascicoloSIPATR }
     * 
     */
    public RispostaCreateFascicoloSIPATR createRispostaCreateFascicoloSIPATR() {
        return new RispostaCreateFascicoloSIPATR();
    }

    /**
     * Create an instance of {@link RispostaCreateFascicoloSIPATRType }
     * 
     */
    public RispostaCreateFascicoloSIPATRType createRispostaCreateFascicoloSIPATRType() {
        return new RispostaCreateFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link FascicoloSIPATRType }
     * 
     */
    public FascicoloSIPATRType createFascicoloSIPATRType() {
        return new FascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RichiestaArchiveFascicoloAttoDecretoType }
     * 
     */
    public RichiestaArchiveFascicoloAttoDecretoType createRichiestaArchiveFascicoloAttoDecretoType() {
        return new RichiestaArchiveFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RichiestaCreateFascicoloAttoDecretoType }
     * 
     */
    public RichiestaCreateFascicoloAttoDecretoType createRichiestaCreateFascicoloAttoDecretoType() {
        return new RichiestaCreateFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RispostaUpdateContentDocumentoFascicoloAttoDecretoType }
     * 
     */
    public RispostaUpdateContentDocumentoFascicoloAttoDecretoType createRispostaUpdateContentDocumentoFascicoloAttoDecretoType() {
        return new RispostaUpdateContentDocumentoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RispostaGetElencoFascicoliRaccoltaProvvisoriaAttoDecretoType }
     * 
     */
    public RispostaGetElencoFascicoliRaccoltaProvvisoriaAttoDecretoType createRispostaGetElencoFascicoliRaccoltaProvvisoriaAttoDecretoType() {
        return new RispostaGetElencoFascicoliRaccoltaProvvisoriaAttoDecretoType();
    }

    /**
     * Create an instance of {@link RichiestaDownloadDocumentoFascicoloAllegatoDecretoIGBType }
     * 
     */
    public RichiestaDownloadDocumentoFascicoloAllegatoDecretoIGBType createRichiestaDownloadDocumentoFascicoloAllegatoDecretoIGBType() {
        return new RichiestaDownloadDocumentoFascicoloAllegatoDecretoIGBType();
    }

    /**
     * Create an instance of {@link RispostaModifyMetadataFascicoloSIPATRType }
     * 
     */
    public RispostaModifyMetadataFascicoloSIPATRType createRispostaModifyMetadataFascicoloSIPATRType() {
        return new RispostaModifyMetadataFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RispostaModifyMetadataDocumentoFascicoloSIPATRType }
     * 
     */
    public RispostaModifyMetadataDocumentoFascicoloSIPATRType createRispostaModifyMetadataDocumentoFascicoloSIPATRType() {
        return new RispostaModifyMetadataDocumentoFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RispostaDownloadDocumentoFascicoloDecretoSIPATRType }
     * 
     */
    public RispostaDownloadDocumentoFascicoloDecretoSIPATRType createRispostaDownloadDocumentoFascicoloDecretoSIPATRType() {
        return new RispostaDownloadDocumentoFascicoloDecretoSIPATRType();
    }

    /**
     * Create an instance of {@link RichiestaModifyMetadatiDocumentoFascicoloAttoDecretoType }
     * 
     */
    public RichiestaModifyMetadatiDocumentoFascicoloAttoDecretoType createRichiestaModifyMetadatiDocumentoFascicoloAttoDecretoType() {
        return new RichiestaModifyMetadatiDocumentoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RichiestaGetDocumentoFascicoloAttoDecretoType }
     * 
     */
    public RichiestaGetDocumentoFascicoloAttoDecretoType createRichiestaGetDocumentoFascicoloAttoDecretoType() {
        return new RichiestaGetDocumentoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RichiestaDownloadDocumentoUrlFascicoloAllegatoDecretoIGBType }
     * 
     */
    public RichiestaDownloadDocumentoUrlFascicoloAllegatoDecretoIGBType createRichiestaDownloadDocumentoUrlFascicoloAllegatoDecretoIGBType() {
        return new RichiestaDownloadDocumentoUrlFascicoloAllegatoDecretoIGBType();
    }

    /**
     * Create an instance of {@link RichiestaDownloadDocumentoAggiuntivoFascicoloAttoDecretoType }
     * 
     */
    public RichiestaDownloadDocumentoAggiuntivoFascicoloAttoDecretoType createRichiestaDownloadDocumentoAggiuntivoFascicoloAttoDecretoType() {
        return new RichiestaDownloadDocumentoAggiuntivoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RichiestaGetDocumentoAggiuntivoFascicoloAttoDecretoType }
     * 
     */
    public RichiestaGetDocumentoAggiuntivoFascicoloAttoDecretoType createRichiestaGetDocumentoAggiuntivoFascicoloAttoDecretoType() {
        return new RichiestaGetDocumentoAggiuntivoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RichiestaChangeStatoFascicoloAttoDecretoType }
     * 
     */
    public RichiestaChangeStatoFascicoloAttoDecretoType createRichiestaChangeStatoFascicoloAttoDecretoType() {
        return new RichiestaChangeStatoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RispostaCreateFascicoloAttoDecreto }
     * 
     */
    public RispostaCreateFascicoloAttoDecreto createRispostaCreateFascicoloAttoDecreto() {
        return new RispostaCreateFascicoloAttoDecreto();
    }

    /**
     * Create an instance of {@link RispostaCreateFascicoloAttoDecretoType }
     * 
     */
    public RispostaCreateFascicoloAttoDecretoType createRispostaCreateFascicoloAttoDecretoType() {
        return new RispostaCreateFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link FascicoloAttoDecretoType }
     * 
     */
    public FascicoloAttoDecretoType createFascicoloAttoDecretoType() {
        return new FascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RispostaArchiveFascicoloAttoDecretoType }
     * 
     */
    public RispostaArchiveFascicoloAttoDecretoType createRispostaArchiveFascicoloAttoDecretoType() {
        return new RispostaArchiveFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RichiestaGetDocumentoFascicoloAllegatoDecretoIGBType }
     * 
     */
    public RichiestaGetDocumentoFascicoloAllegatoDecretoIGBType createRichiestaGetDocumentoFascicoloAllegatoDecretoIGBType() {
        return new RichiestaGetDocumentoFascicoloAllegatoDecretoIGBType();
    }

    /**
     * Create an instance of {@link RichiestaCreateFascicoloRaccoltaProvvisoriaType }
     * 
     */
    public RichiestaCreateFascicoloRaccoltaProvvisoriaType createRichiestaCreateFascicoloRaccoltaProvvisoriaType() {
        return new RichiestaCreateFascicoloRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link RispostaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType }
     * 
     */
    public RispostaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType createRispostaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType() {
        return new RispostaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link RispostaAddCollegamentoFascicoloSIPATRType }
     * 
     */
    public RispostaAddCollegamentoFascicoloSIPATRType createRispostaAddCollegamentoFascicoloSIPATRType() {
        return new RispostaAddCollegamentoFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RichiestaModifyMetadataFascicoloAttoDecretoType }
     * 
     */
    public RichiestaModifyMetadataFascicoloAttoDecretoType createRichiestaModifyMetadataFascicoloAttoDecretoType() {
        return new RichiestaModifyMetadataFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RichiestaAddDocumentoFascicoloSIPATRType }
     * 
     */
    public RichiestaAddDocumentoFascicoloSIPATRType createRichiestaAddDocumentoFascicoloSIPATRType() {
        return new RichiestaAddDocumentoFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RichiestaAddDocumentoAggiuntivoFascicoloDecretoSIPATRType }
     * 
     */
    public RichiestaAddDocumentoAggiuntivoFascicoloDecretoSIPATRType createRichiestaAddDocumentoAggiuntivoFascicoloDecretoSIPATRType() {
        return new RichiestaAddDocumentoAggiuntivoFascicoloDecretoSIPATRType();
    }

    /**
     * Create an instance of {@link RispostaGetDocumentiFascicoloAttoDecreto }
     * 
     */
    public RispostaGetDocumentiFascicoloAttoDecreto createRispostaGetDocumentiFascicoloAttoDecreto() {
        return new RispostaGetDocumentiFascicoloAttoDecreto();
    }

    /**
     * Create an instance of {@link RispostaGetFascicoloAttoDecretoType }
     * 
     */
    public RispostaGetFascicoloAttoDecretoType createRispostaGetFascicoloAttoDecretoType() {
        return new RispostaGetFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RispostaModifyMetadatiDocumentoFascicoloAttoDecretoType }
     * 
     */
    public RispostaModifyMetadatiDocumentoFascicoloAttoDecretoType createRispostaModifyMetadatiDocumentoFascicoloAttoDecretoType() {
        return new RispostaModifyMetadatiDocumentoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RichiestaAddDocumentoFascicoloRaccoltaProvvisoriaType }
     * 
     */
    public RichiestaAddDocumentoFascicoloRaccoltaProvvisoriaType createRichiestaAddDocumentoFascicoloRaccoltaProvvisoriaType() {
        return new RichiestaAddDocumentoFascicoloRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link RispostaGetElencoDocumentiFascicoloAllegatoDecretoIGBType }
     * 
     */
    public RispostaGetElencoDocumentiFascicoloAllegatoDecretoIGBType createRispostaGetElencoDocumentiFascicoloAllegatoDecretoIGBType() {
        return new RispostaGetElencoDocumentiFascicoloAllegatoDecretoIGBType();
    }

    /**
     * Create an instance of {@link RispostaDownloadDocumentoFascicoloAllegatoDecretoIGBType }
     * 
     */
    public RispostaDownloadDocumentoFascicoloAllegatoDecretoIGBType createRispostaDownloadDocumentoFascicoloAllegatoDecretoIGBType() {
        return new RispostaDownloadDocumentoFascicoloAllegatoDecretoIGBType();
    }

    /**
     * Create an instance of {@link RichiestaGetFascicoloRaccoltaProvvisoriaType }
     * 
     */
    public RichiestaGetFascicoloRaccoltaProvvisoriaType createRichiestaGetFascicoloRaccoltaProvvisoriaType() {
        return new RichiestaGetFascicoloRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link RichiestaAcquisizioneFascicoloAttoDecretoType }
     * 
     */
    public RichiestaAcquisizioneFascicoloAttoDecretoType createRichiestaAcquisizioneFascicoloAttoDecretoType() {
        return new RichiestaAcquisizioneFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RispostaGetFascicoloDecretoSIPATRType }
     * 
     */
    public RispostaGetFascicoloDecretoSIPATRType createRispostaGetFascicoloDecretoSIPATRType() {
        return new RispostaGetFascicoloDecretoSIPATRType();
    }

    /**
     * Create an instance of {@link RichiestaGetDocumentoFascicoloRaccoltaProvvisoriaType }
     * 
     */
    public RichiestaGetDocumentoFascicoloRaccoltaProvvisoriaType createRichiestaGetDocumentoFascicoloRaccoltaProvvisoriaType() {
        return new RichiestaGetDocumentoFascicoloRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link RichiestaRemoveDocumentoAggiuntivoFascicoloDecretoSIPATRType }
     * 
     */
    public RichiestaRemoveDocumentoAggiuntivoFascicoloDecretoSIPATRType createRichiestaRemoveDocumentoAggiuntivoFascicoloDecretoSIPATRType() {
        return new RichiestaRemoveDocumentoAggiuntivoFascicoloDecretoSIPATRType();
    }

    /**
     * Create an instance of {@link RispostaChangeStatoFascicoloDecretoSIPATRType }
     * 
     */
    public RispostaChangeStatoFascicoloDecretoSIPATRType createRispostaChangeStatoFascicoloDecretoSIPATRType() {
        return new RispostaChangeStatoFascicoloDecretoSIPATRType();
    }

    /**
     * Create an instance of {@link RispostaGetDocumentoFascicoloAttoDecretoType }
     * 
     */
    public RispostaGetDocumentoFascicoloAttoDecretoType createRispostaGetDocumentoFascicoloAttoDecretoType() {
        return new RispostaGetDocumentoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RispostaChangeStatoFascicoloRaccoltaProvvisoriaType }
     * 
     */
    public RispostaChangeStatoFascicoloRaccoltaProvvisoriaType createRispostaChangeStatoFascicoloRaccoltaProvvisoriaType() {
        return new RispostaChangeStatoFascicoloRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link RispostaDownloadDocumentoFascicoloAttoDecretoCorteType }
     * 
     */
    public RispostaDownloadDocumentoFascicoloAttoDecretoCorteType createRispostaDownloadDocumentoFascicoloAttoDecretoCorteType() {
        return new RispostaDownloadDocumentoFascicoloAttoDecretoCorteType();
    }

    /**
     * Create an instance of {@link RichiestaDownloadDocumentoUrlFascicoloRaccoltaProvvisoriaType }
     * 
     */
    public RichiestaDownloadDocumentoUrlFascicoloRaccoltaProvvisoriaType createRichiestaDownloadDocumentoUrlFascicoloRaccoltaProvvisoriaType() {
        return new RichiestaDownloadDocumentoUrlFascicoloRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link RispostaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGBType }
     * 
     */
    public RispostaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGBType createRispostaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGBType() {
        return new RispostaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGBType();
    }

    /**
     * Create an instance of {@link RichiestaRemoveFascicoloDecretoSIPATRType }
     * 
     */
    public RichiestaRemoveFascicoloDecretoSIPATRType createRichiestaRemoveFascicoloDecretoSIPATRType() {
        return new RichiestaRemoveFascicoloDecretoSIPATRType();
    }

    /**
     * Create an instance of {@link RispostaRemoveDocumentoFascicoloAllegatoDecretoIGBType }
     * 
     */
    public RispostaRemoveDocumentoFascicoloAllegatoDecretoIGBType createRispostaRemoveDocumentoFascicoloAllegatoDecretoIGBType() {
        return new RispostaRemoveDocumentoFascicoloAllegatoDecretoIGBType();
    }

    /**
     * Create an instance of {@link RispostaSearchFascicoliRaccoltaProvvisoriaType }
     * 
     */
    public RispostaSearchFascicoliRaccoltaProvvisoriaType createRispostaSearchFascicoliRaccoltaProvvisoriaType() {
        return new RispostaSearchFascicoliRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link RichiestaGetElencoFascicoliRaccoltaProvvisoriaAttoDecretoType }
     * 
     */
    public RichiestaGetElencoFascicoliRaccoltaProvvisoriaAttoDecretoType createRichiestaGetElencoFascicoliRaccoltaProvvisoriaAttoDecretoType() {
        return new RichiestaGetElencoFascicoliRaccoltaProvvisoriaAttoDecretoType();
    }

    /**
     * Create an instance of {@link RispostaGetFascicoloAttoDecreto }
     * 
     */
    public RispostaGetFascicoloAttoDecreto createRispostaGetFascicoloAttoDecreto() {
        return new RispostaGetFascicoloAttoDecreto();
    }

    /**
     * Create an instance of {@link RichiestaGetFascicoloDecretoSIPATRType }
     * 
     */
    public RichiestaGetFascicoloDecretoSIPATRType createRichiestaGetFascicoloDecretoSIPATRType() {
        return new RichiestaGetFascicoloDecretoSIPATRType();
    }

    /**
     * Create an instance of {@link RispostaGetRisultatoOperazioneDocumento }
     * 
     */
    public RispostaGetRisultatoOperazioneDocumento createRispostaGetRisultatoOperazioneDocumento() {
        return new RispostaGetRisultatoOperazioneDocumento();
    }

    /**
     * Create an instance of {@link RispostaGetRisultatoOperazioneDocumentoType }
     * 
     */
    public RispostaGetRisultatoOperazioneDocumentoType createRispostaGetRisultatoOperazioneDocumentoType() {
        return new RispostaGetRisultatoOperazioneDocumentoType();
    }

    /**
     * Create an instance of {@link RichiestaRemoveDocumentoFascicoloAttoDecretoCorteType }
     * 
     */
    public RichiestaRemoveDocumentoFascicoloAttoDecretoCorteType createRichiestaRemoveDocumentoFascicoloAttoDecretoCorteType() {
        return new RichiestaRemoveDocumentoFascicoloAttoDecretoCorteType();
    }

    /**
     * Create an instance of {@link RispostaRemoveDocumentoAggiuntivoFascicoloSIPATRType }
     * 
     */
    public RispostaRemoveDocumentoAggiuntivoFascicoloSIPATRType createRispostaRemoveDocumentoAggiuntivoFascicoloSIPATRType() {
        return new RispostaRemoveDocumentoAggiuntivoFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RispostaRemoveCollegamentoFascicoloSIPATRType }
     * 
     */
    public RispostaRemoveCollegamentoFascicoloSIPATRType createRispostaRemoveCollegamentoFascicoloSIPATRType() {
        return new RispostaRemoveCollegamentoFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link RichiestaModifyMetadataFascicoloRaccoltaProvvisoriaType }
     * 
     */
    public RichiestaModifyMetadataFascicoloRaccoltaProvvisoriaType createRichiestaModifyMetadataFascicoloRaccoltaProvvisoriaType() {
        return new RichiestaModifyMetadataFascicoloRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link RispostaRemoveDocumentoFascicoloAttoDecretoType }
     * 
     */
    public RispostaRemoveDocumentoFascicoloAttoDecretoType createRispostaRemoveDocumentoFascicoloAttoDecretoType() {
        return new RispostaRemoveDocumentoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link RispostaGetDocumentoFascicoloAllegatoDecretoIGBType }
     * 
     */
    public RispostaGetDocumentoFascicoloAllegatoDecretoIGBType createRispostaGetDocumentoFascicoloAllegatoDecretoIGBType() {
        return new RispostaGetDocumentoFascicoloAllegatoDecretoIGBType();
    }

    /**
     * Create an instance of {@link RispostaAddDocumentoFascicoloRaccoltaProvvisoriaType }
     * 
     */
    public RispostaAddDocumentoFascicoloRaccoltaProvvisoriaType createRispostaAddDocumentoFascicoloRaccoltaProvvisoriaType() {
        return new RispostaAddDocumentoFascicoloRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link RispostaUpdateMetadatiDocumentoAggiuntivoFascicoloAttoDecretoType }
     * 
     */
    public RispostaUpdateMetadatiDocumentoAggiuntivoFascicoloAttoDecretoType createRispostaUpdateMetadatiDocumentoAggiuntivoFascicoloAttoDecretoType() {
        return new RispostaUpdateMetadatiDocumentoAggiuntivoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link DocumentoFascicoloSIPATRType }
     * 
     */
    public DocumentoFascicoloSIPATRType createDocumentoFascicoloSIPATRType() {
        return new DocumentoFascicoloSIPATRType();
    }

    /**
     * Create an instance of {@link ProtocolloSearchType }
     * 
     */
    public ProtocolloSearchType createProtocolloSearchType() {
        return new ProtocolloSearchType();
    }

    /**
     * Create an instance of {@link FascicoloMetadataRaccoltaProvvisoriaType }
     * 
     */
    public FascicoloMetadataRaccoltaProvvisoriaType createFascicoloMetadataRaccoltaProvvisoriaType() {
        return new FascicoloMetadataRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link DocumentoMetadataDecretoSIPATRType }
     * 
     */
    public DocumentoMetadataDecretoSIPATRType createDocumentoMetadataDecretoSIPATRType() {
        return new DocumentoMetadataDecretoSIPATRType();
    }

    /**
     * Create an instance of {@link DataRangeType }
     * 
     */
    public DataRangeType createDataRangeType() {
        return new DataRangeType();
    }

    /**
     * Create an instance of {@link FascicoloMetadataAttoDecretoType }
     * 
     */
    public FascicoloMetadataAttoDecretoType createFascicoloMetadataAttoDecretoType() {
        return new FascicoloMetadataAttoDecretoType();
    }

    /**
     * Create an instance of {@link DocumentoFascicoloType }
     * 
     */
    public DocumentoFascicoloType createDocumentoFascicoloType() {
        return new DocumentoFascicoloType();
    }

    /**
     * Create an instance of {@link CriteriaDocumentiAttoDecretoType }
     * 
     */
    public CriteriaDocumentiAttoDecretoType createCriteriaDocumentiAttoDecretoType() {
        return new CriteriaDocumentiAttoDecretoType();
    }

    /**
     * Create an instance of {@link CriteriaDocumentiSIPATRType }
     * 
     */
    public CriteriaDocumentiSIPATRType createCriteriaDocumentiSIPATRType() {
        return new CriteriaDocumentiSIPATRType();
    }

    /**
     * Create an instance of {@link DocumentoMetadataDocAggiuntivaType }
     * 
     */
    public DocumentoMetadataDocAggiuntivaType createDocumentoMetadataDocAggiuntivaType() {
        return new DocumentoMetadataDocAggiuntivaType();
    }

    /**
     * Create an instance of {@link RispostaInviaDocAggiuntiviFascicoloAttoDecretoCorteType }
     * 
     */
    public RispostaInviaDocAggiuntiviFascicoloAttoDecretoCorteType createRispostaInviaDocAggiuntiviFascicoloAttoDecretoCorteType() {
        return new RispostaInviaDocAggiuntiviFascicoloAttoDecretoCorteType();
    }

    /**
     * Create an instance of {@link NumeroAllegatiType }
     * 
     */
    public NumeroAllegatiType createNumeroAllegatiType() {
        return new NumeroAllegatiType();
    }

    /**
     * Create an instance of {@link CriteriaFascicoliSIPATRType }
     * 
     */
    public CriteriaFascicoliSIPATRType createCriteriaFascicoliSIPATRType() {
        return new CriteriaFascicoliSIPATRType();
    }

    /**
     * Create an instance of {@link DocumentoFascicoloAllegatoDecretoIGBType }
     * 
     */
    public DocumentoFascicoloAllegatoDecretoIGBType createDocumentoFascicoloAllegatoDecretoIGBType() {
        return new DocumentoFascicoloAllegatoDecretoIGBType();
    }

    /**
     * Create an instance of {@link FascicoloMetadataDecretoSIPATRType }
     * 
     */
    public FascicoloMetadataDecretoSIPATRType createFascicoloMetadataDecretoSIPATRType() {
        return new FascicoloMetadataDecretoSIPATRType();
    }

    /**
     * Create an instance of {@link CodiceDescrizioneType }
     * 
     */
    public CodiceDescrizioneType createCodiceDescrizioneType() {
        return new CodiceDescrizioneType();
    }

    /**
     * Create an instance of {@link DocumentoFileType }
     * 
     */
    public DocumentoFileType createDocumentoFileType() {
        return new DocumentoFileType();
    }

    /**
     * Create an instance of {@link DocumentoFascicoloRaccoltaProvvisoriaType }
     * 
     */
    public DocumentoFascicoloRaccoltaProvvisoriaType createDocumentoFascicoloRaccoltaProvvisoriaType() {
        return new DocumentoFascicoloRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link DocumentoMetadataSIPATRType }
     * 
     */
    public DocumentoMetadataSIPATRType createDocumentoMetadataSIPATRType() {
        return new DocumentoMetadataSIPATRType();
    }

    /**
     * Create an instance of {@link FlussoADType }
     * 
     */
    public FlussoADType createFlussoADType() {
        return new FlussoADType();
    }

    /**
     * Create an instance of {@link CriteriaDocumentiFascicoloIGBType }
     * 
     */
    public CriteriaDocumentiFascicoloIGBType createCriteriaDocumentiFascicoloIGBType() {
        return new CriteriaDocumentiFascicoloIGBType();
    }

    /**
     * Create an instance of {@link RispostaDownloadDocumentoAggiuntivoFascicoloDecretoSIPATRType }
     * 
     */
    public RispostaDownloadDocumentoAggiuntivoFascicoloDecretoSIPATRType createRispostaDownloadDocumentoAggiuntivoFascicoloDecretoSIPATRType() {
        return new RispostaDownloadDocumentoAggiuntivoFascicoloDecretoSIPATRType();
    }

    /**
     * Create an instance of {@link ConversioneContentType }
     * 
     */
    public ConversioneContentType createConversioneContentType() {
        return new ConversioneContentType();
    }

    /**
     * Create an instance of {@link FascicoloAttoDecretoCorteType }
     * 
     */
    public FascicoloAttoDecretoCorteType createFascicoloAttoDecretoCorteType() {
        return new FascicoloAttoDecretoCorteType();
    }

    /**
     * Create an instance of {@link IdentificativiFascicoloRaccoltaProvvisoriaType }
     * 
     */
    public IdentificativiFascicoloRaccoltaProvvisoriaType createIdentificativiFascicoloRaccoltaProvvisoriaType() {
        return new IdentificativiFascicoloRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link TipoFascicoloType }
     * 
     */
    public TipoFascicoloType createTipoFascicoloType() {
        return new TipoFascicoloType();
    }

    /**
     * Create an instance of {@link RaggruppamentoType }
     * 
     */
    public RaggruppamentoType createRaggruppamentoType() {
        return new RaggruppamentoType();
    }

    /**
     * Create an instance of {@link RispostaEliminaFascicoloAttoDecretoType }
     * 
     */
    public RispostaEliminaFascicoloAttoDecretoType createRispostaEliminaFascicoloAttoDecretoType() {
        return new RispostaEliminaFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link DocumentoMetadataRaccoltaProvvisoriaType }
     * 
     */
    public DocumentoMetadataRaccoltaProvvisoriaType createDocumentoMetadataRaccoltaProvvisoriaType() {
        return new DocumentoMetadataRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link DocumentoFascicoloDecretoSIPATRType }
     * 
     */
    public DocumentoFascicoloDecretoSIPATRType createDocumentoFascicoloDecretoSIPATRType() {
        return new DocumentoFascicoloDecretoSIPATRType();
    }

    /**
     * Create an instance of {@link RichiestaEliminaFascicoloAttoDecretoType }
     * 
     */
    public RichiestaEliminaFascicoloAttoDecretoType createRichiestaEliminaFascicoloAttoDecretoType() {
        return new RichiestaEliminaFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link ProtocolloType }
     * 
     */
    public ProtocolloType createProtocolloType() {
        return new ProtocolloType();
    }

    /**
     * Create an instance of {@link DatiFascicoloType }
     * 
     */
    public DatiFascicoloType createDatiFascicoloType() {
        return new DatiFascicoloType();
    }

    /**
     * Create an instance of {@link DocumentoMetadataAllegatoDecretoIGBType }
     * 
     */
    public DocumentoMetadataAllegatoDecretoIGBType createDocumentoMetadataAllegatoDecretoIGBType() {
        return new DocumentoMetadataAllegatoDecretoIGBType();
    }

    /**
     * Create an instance of {@link DocumentoFascicoloAttoDecretoType }
     * 
     */
    public DocumentoFascicoloAttoDecretoType createDocumentoFascicoloAttoDecretoType() {
        return new DocumentoFascicoloAttoDecretoType();
    }

    /**
     * Create an instance of {@link CriteriaDocumentiRaccoltaProvvisoriaType }
     * 
     */
    public CriteriaDocumentiRaccoltaProvvisoriaType createCriteriaDocumentiRaccoltaProvvisoriaType() {
        return new CriteriaDocumentiRaccoltaProvvisoriaType();
    }

    /**
     * Create an instance of {@link DocumentoResultType }
     * 
     */
    public DocumentoResultType createDocumentoResultType() {
        return new DocumentoResultType();
    }

    /**
     * Create an instance of {@link RichiestaDownloadDocumentoAggiuntivoFascicoloDecretoSIPATRType }
     * 
     */
    public RichiestaDownloadDocumentoAggiuntivoFascicoloDecretoSIPATRType createRichiestaDownloadDocumentoAggiuntivoFascicoloDecretoSIPATRType() {
        return new RichiestaDownloadDocumentoAggiuntivoFascicoloDecretoSIPATRType();
    }

    /**
     * Create an instance of {@link FascicoloMetadataSIPATRType }
     * 
     */
    public FascicoloMetadataSIPATRType createFascicoloMetadataSIPATRType() {
        return new FascicoloMetadataSIPATRType();
    }

    /**
     * Create an instance of {@link DocumentoMetadataAttoDecretoType }
     * 
     */
    public DocumentoMetadataAttoDecretoType createDocumentoMetadataAttoDecretoType() {
        return new DocumentoMetadataAttoDecretoType();
    }

    /**
     * Create an instance of {@link DocumentoFascicoloAttoDecretoCorteType }
     * 
     */
    public DocumentoFascicoloAttoDecretoCorteType createDocumentoFascicoloAttoDecretoCorteType() {
        return new DocumentoFascicoloAttoDecretoCorteType();
    }

    /**
     * Create an instance of {@link FascicoloMetadataAttoDecretoCorteType }
     * 
     */
    public FascicoloMetadataAttoDecretoCorteType createFascicoloMetadataAttoDecretoCorteType() {
        return new FascicoloMetadataAttoDecretoCorteType();
    }

    /**
     * Create an instance of {@link CriteriaFascicoliRaccolteProvvisorieType }
     * 
     */
    public CriteriaFascicoliRaccolteProvvisorieType createCriteriaFascicoliRaccolteProvvisorieType() {
        return new CriteriaFascicoliRaccolteProvvisorieType();
    }

    /**
     * Create an instance of {@link OperazioneDocumentaleType }
     * 
     */
    public OperazioneDocumentaleType createOperazioneDocumentaleType() {
        return new OperazioneDocumentaleType();
    }

    /**
     * Create an instance of {@link TipoDocumentoType }
     * 
     */
    public TipoDocumentoType createTipoDocumentoType() {
        return new TipoDocumentoType();
    }

    /**
     * Create an instance of {@link RichiestaOperazioneDocumentaleType.ParametriOperazione }
     * 
     */
    public RichiestaOperazioneDocumentaleType.ParametriOperazione createRichiestaOperazioneDocumentaleTypeParametriOperazione() {
        return new RichiestaOperazioneDocumentaleType.ParametriOperazione();
    }

    /**
     * Create an instance of {@link RisultatoOperazioneDocumentoType.Operazione }
     * 
     */
    public RisultatoOperazioneDocumentoType.Operazione createRisultatoOperazioneDocumentoTypeOperazione() {
        return new RisultatoOperazioneDocumentoType.Operazione();
    }

    /**
     * Create an instance of {@link RichiestaAddCollegamentoFascicoloSIPATRType.AssociazioneFascicolo }
     * 
     */
    public RichiestaAddCollegamentoFascicoloSIPATRType.AssociazioneFascicolo createRichiestaAddCollegamentoFascicoloSIPATRTypeAssociazioneFascicolo() {
        return new RichiestaAddCollegamentoFascicoloSIPATRType.AssociazioneFascicolo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRemoveDocumentoFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_removeDocumentoFascicoloSIPATR")
    public JAXBElement<RichiestaRemoveDocumentoFascicoloSIPATRType> createRichiestaRemoveDocumentoFascicoloSIPATR(RichiestaRemoveDocumentoFascicoloSIPATRType value) {
        return new JAXBElement<RichiestaRemoveDocumentoFascicoloSIPATRType>(_RichiestaRemoveDocumentoFascicoloSIPATR_QNAME, RichiestaRemoveDocumentoFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaDownloadDocumentoFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_downloadDocumentoFascicoloSIPATR")
    public JAXBElement<RichiestaDownloadDocumentoFascicoloSIPATRType> createRichiestaDownloadDocumentoFascicoloSIPATR(RichiestaDownloadDocumentoFascicoloSIPATRType value) {
        return new JAXBElement<RichiestaDownloadDocumentoFascicoloSIPATRType>(_RichiestaDownloadDocumentoFascicoloSIPATR_QNAME, RichiestaDownloadDocumentoFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaDownloadDocumentoFascicoloRaccoltaProvvisoriaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_downloadDocumentoFascicoloRaccoltaProvvisoria")
    public JAXBElement<RispostaDownloadDocumentoFascicoloRaccoltaProvvisoriaType> createRispostaDownloadDocumentoFascicoloRaccoltaProvvisoria(RispostaDownloadDocumentoFascicoloRaccoltaProvvisoriaType value) {
        return new JAXBElement<RispostaDownloadDocumentoFascicoloRaccoltaProvvisoriaType>(_RispostaDownloadDocumentoFascicoloRaccoltaProvvisoria_QNAME, RispostaDownloadDocumentoFascicoloRaccoltaProvvisoriaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRemoveDocumentoFascicoloRaccoltaProvvisoriaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_removeDocumentoFascicoloRaccoltaProvvisoria")
    public JAXBElement<RispostaRemoveDocumentoFascicoloRaccoltaProvvisoriaType> createRispostaRemoveDocumentoFascicoloRaccoltaProvvisoria(RispostaRemoveDocumentoFascicoloRaccoltaProvvisoriaType value) {
        return new JAXBElement<RispostaRemoveDocumentoFascicoloRaccoltaProvvisoriaType>(_RispostaRemoveDocumentoFascicoloRaccoltaProvvisoria_QNAME, RispostaRemoveDocumentoFascicoloRaccoltaProvvisoriaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_modifyMetadatiDocumentoFascicoloRaccoltaProvvisoria")
    public JAXBElement<RichiestaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType> createRichiestaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoria(RichiestaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType value) {
        return new JAXBElement<RichiestaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType>(_RichiestaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoria_QNAME, RichiestaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetRisultatoOperazioneDocumentoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_getRisultatoOperazioneDocumento")
    public JAXBElement<RichiestaGetRisultatoOperazioneDocumentoType> createRichiestaGetRisultatoOperazioneDocumento(RichiestaGetRisultatoOperazioneDocumentoType value) {
        return new JAXBElement<RichiestaGetRisultatoOperazioneDocumentoType>(_RichiestaGetRisultatoOperazioneDocumento_QNAME, RichiestaGetRisultatoOperazioneDocumentoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaGetMonitoraggioDocumentiType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_monitoraggioDocumenti")
    public JAXBElement<RispostaGetMonitoraggioDocumentiType> createRispostaMonitoraggioDocumenti(RispostaGetMonitoraggioDocumentiType value) {
        return new JAXBElement<RispostaGetMonitoraggioDocumentiType>(_RispostaMonitoraggioDocumenti_QNAME, RispostaGetMonitoraggioDocumentiType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaModifyMetadataFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_modifyMetadatiFascicoloAttoDecreto")
    public JAXBElement<RispostaModifyMetadataFascicoloAttoDecretoType> createRispostaModifyMetadatiFascicoloAttoDecreto(RispostaModifyMetadataFascicoloAttoDecretoType value) {
        return new JAXBElement<RispostaModifyMetadataFascicoloAttoDecretoType>(_RispostaModifyMetadatiFascicoloAttoDecreto_QNAME, RispostaModifyMetadataFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaConvertiDocumentoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_convertiDocumento")
    public JAXBElement<RispostaConvertiDocumentoType> createRispostaConvertiDocumento(RispostaConvertiDocumentoType value) {
        return new JAXBElement<RispostaConvertiDocumentoType>(_RispostaConvertiDocumento_QNAME, RispostaConvertiDocumentoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaAddDocumentoAggiuntivoFascicoloDecretoSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_addDocumentoAggiuntivoFascicoloDecretoSIPATR")
    public JAXBElement<RispostaAddDocumentoAggiuntivoFascicoloDecretoSIPATRType> createRispostaAddDocumentoAggiuntivoFascicoloDecretoSIPATR(RispostaAddDocumentoAggiuntivoFascicoloDecretoSIPATRType value) {
        return new JAXBElement<RispostaAddDocumentoAggiuntivoFascicoloDecretoSIPATRType>(_RispostaAddDocumentoAggiuntivoFascicoloDecretoSIPATR_QNAME, RispostaAddDocumentoAggiuntivoFascicoloDecretoSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaModifyMetadataFascicoloRaccoltaProvvisoriaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_modifyMetadatiFascicoloRaccoltaProvvisoria")
    public JAXBElement<RispostaModifyMetadataFascicoloRaccoltaProvvisoriaType> createRispostaModifyMetadatiFascicoloRaccoltaProvvisoria(RispostaModifyMetadataFascicoloRaccoltaProvvisoriaType value) {
        return new JAXBElement<RispostaModifyMetadataFascicoloRaccoltaProvvisoriaType>(_RispostaModifyMetadatiFascicoloRaccoltaProvvisoria_QNAME, RispostaModifyMetadataFascicoloRaccoltaProvvisoriaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaCopyFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_copyFascicoloAttoDecreto")
    public JAXBElement<RichiestaCopyFascicoloAttoDecretoType> createRichiestaCopyFascicoloAttoDecreto(RichiestaCopyFascicoloAttoDecretoType value) {
        return new JAXBElement<RichiestaCopyFascicoloAttoDecretoType>(_RichiestaCopyFascicoloAttoDecreto_QNAME, RichiestaCopyFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_getDocumentiFascicoloAttoDecreto")
    public JAXBElement<RichiestaGetFascicoloAttoDecretoType> createRichiestaGetDocumentiFascicoloAttoDecreto(RichiestaGetFascicoloAttoDecretoType value) {
        return new JAXBElement<RichiestaGetFascicoloAttoDecretoType>(_RichiestaGetDocumentiFascicoloAttoDecreto_QNAME, RichiestaGetFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaModifyMetadataFascicoloDecretoSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_modifyMetadataFascicoloDecretoSIPATR")
    public JAXBElement<RispostaModifyMetadataFascicoloDecretoSIPATRType> createRispostaModifyMetadataFascicoloDecretoSIPATR(RispostaModifyMetadataFascicoloDecretoSIPATRType value) {
        return new JAXBElement<RispostaModifyMetadataFascicoloDecretoSIPATRType>(_RispostaModifyMetadataFascicoloDecretoSIPATR_QNAME, RispostaModifyMetadataFascicoloDecretoSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaGetFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_getFascicoloSIPATR")
    public JAXBElement<RispostaGetFascicoloSIPATRType> createRispostaGetFascicoloSIPATR(RispostaGetFascicoloSIPATRType value) {
        return new JAXBElement<RispostaGetFascicoloSIPATRType>(_RispostaGetFascicoloSIPATR_QNAME, RispostaGetFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaAcquisizioneFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_acquisizioneFascicoloAttoDecreto")
    public JAXBElement<RispostaAcquisizioneFascicoloAttoDecretoType> createRispostaAcquisizioneFascicoloAttoDecreto(RispostaAcquisizioneFascicoloAttoDecretoType value) {
        return new JAXBElement<RispostaAcquisizioneFascicoloAttoDecretoType>(_RispostaAcquisizioneFascicoloAttoDecreto_QNAME, RispostaAcquisizioneFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_getFascicoloAttoDecreto")
    public JAXBElement<RichiestaGetFascicoloAttoDecretoType> createRichiestaGetFascicoloAttoDecreto(RichiestaGetFascicoloAttoDecretoType value) {
        return new JAXBElement<RichiestaGetFascicoloAttoDecretoType>(_RichiestaGetFascicoloAttoDecreto_QNAME, RichiestaGetFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaChangeStatoFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_changeStatoFascicoloSIPATR")
    public JAXBElement<RichiestaChangeStatoFascicoloSIPATRType> createRichiestaChangeStatoFascicoloSIPATR(RichiestaChangeStatoFascicoloSIPATRType value) {
        return new JAXBElement<RichiestaChangeStatoFascicoloSIPATRType>(_RichiestaChangeStatoFascicoloSIPATR_QNAME, RichiestaChangeStatoFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaDownloadDocumentoUrlFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_downloadDocumentoUrlFascicoloSIPATR")
    public JAXBElement<RichiestaDownloadDocumentoUrlFascicoloSIPATRType> createRichiestaDownloadDocumentoUrlFascicoloSIPATR(RichiestaDownloadDocumentoUrlFascicoloSIPATRType value) {
        return new JAXBElement<RichiestaDownloadDocumentoUrlFascicoloSIPATRType>(_RichiestaDownloadDocumentoUrlFascicoloSIPATR_QNAME, RichiestaDownloadDocumentoUrlFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaGetFascicoloSIPATRAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_getFascicoloSIPATRAttoDecreto")
    public JAXBElement<RispostaGetFascicoloSIPATRAttoDecretoType> createRispostaGetFascicoloSIPATRAttoDecreto(RispostaGetFascicoloSIPATRAttoDecretoType value) {
        return new JAXBElement<RispostaGetFascicoloSIPATRAttoDecretoType>(_RispostaGetFascicoloSIPATRAttoDecreto_QNAME, RispostaGetFascicoloSIPATRAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaDownloadDocumentoAggiuntivoFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_downloadDocumentoAggiuntivoFascicoloAttoDecreto")
    public JAXBElement<RispostaDownloadDocumentoAggiuntivoFascicoloAttoDecretoType> createRispostaDownloadDocumentoAggiuntivoFascicoloAttoDecreto(RispostaDownloadDocumentoAggiuntivoFascicoloAttoDecretoType value) {
        return new JAXBElement<RispostaDownloadDocumentoAggiuntivoFascicoloAttoDecretoType>(_RispostaDownloadDocumentoAggiuntivoFascicoloAttoDecreto_QNAME, RispostaDownloadDocumentoAggiuntivoFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaInviaDocAggiuntiviFascicoloAttoDecretoCorteType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_inviaDocAggiuntiviFascicoloAttoDecretoCorte")
    public JAXBElement<RichiestaInviaDocAggiuntiviFascicoloAttoDecretoCorteType> createRichiestaInviaDocAggiuntiviFascicoloAttoDecretoCorte(RichiestaInviaDocAggiuntiviFascicoloAttoDecretoCorteType value) {
        return new JAXBElement<RichiestaInviaDocAggiuntiviFascicoloAttoDecretoCorteType>(_RichiestaInviaDocAggiuntiviFascicoloAttoDecretoCorte_QNAME, RichiestaInviaDocAggiuntiviFascicoloAttoDecretoCorteType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaSearchFascicoliRaccolteProvvisorieType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_searchFascicoliRaccolteProvvisorie")
    public JAXBElement<RichiestaSearchFascicoliRaccolteProvvisorieType> createRichiestaSearchFascicoliRaccolteProvvisorie(RichiestaSearchFascicoliRaccolteProvvisorieType value) {
        return new JAXBElement<RichiestaSearchFascicoliRaccolteProvvisorieType>(_RichiestaSearchFascicoliRaccolteProvvisorie_QNAME, RichiestaSearchFascicoliRaccolteProvvisorieType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAddDocumentoFascicoloAttoDecretoCorteType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_addDocumentoFascicoloAttoDecretoCorte")
    public JAXBElement<RichiestaAddDocumentoFascicoloAttoDecretoCorteType> createRichiestaAddDocumentoFascicoloAttoDecretoCorte(RichiestaAddDocumentoFascicoloAttoDecretoCorteType value) {
        return new JAXBElement<RichiestaAddDocumentoFascicoloAttoDecretoCorteType>(_RichiestaAddDocumentoFascicoloAttoDecretoCorte_QNAME, RichiestaAddDocumentoFascicoloAttoDecretoCorteType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaChangeStatoFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_changeStatoFascicoloSIPATR")
    public JAXBElement<RispostaChangeStatoFascicoloSIPATRType> createRispostaChangeStatoFascicoloSIPATR(RispostaChangeStatoFascicoloSIPATRType value) {
        return new JAXBElement<RispostaChangeStatoFascicoloSIPATRType>(_RispostaChangeStatoFascicoloSIPATR_QNAME, RispostaChangeStatoFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaUpdateMetadatiDocumentoAggiuntivoFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_updateMetadatiDocumentoAggiuntivoFascicoloAttoDecreto")
    public JAXBElement<RichiestaUpdateMetadatiDocumentoAggiuntivoFascicoloAttoDecretoType> createRichiestaUpdateMetadatiDocumentoAggiuntivoFascicoloAttoDecreto(RichiestaUpdateMetadatiDocumentoAggiuntivoFascicoloAttoDecretoType value) {
        return new JAXBElement<RichiestaUpdateMetadatiDocumentoAggiuntivoFascicoloAttoDecretoType>(_RichiestaUpdateMetadatiDocumentoAggiuntivoFascicoloAttoDecreto_QNAME, RichiestaUpdateMetadatiDocumentoAggiuntivoFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRemoveFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_removeFascicoloAttoDecreto")
    public JAXBElement<RispostaRemoveFascicoloAttoDecretoType> createRispostaRemoveFascicoloAttoDecreto(RispostaRemoveFascicoloAttoDecretoType value) {
        return new JAXBElement<RispostaRemoveFascicoloAttoDecretoType>(_RispostaRemoveFascicoloAttoDecreto_QNAME, RispostaRemoveFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_getFascicoloSIPATR")
    public JAXBElement<RichiestaGetFascicoloSIPATRType> createRichiestaGetFascicoloSIPATR(RichiestaGetFascicoloSIPATRType value) {
        return new JAXBElement<RichiestaGetFascicoloSIPATRType>(_RichiestaGetFascicoloSIPATR_QNAME, RichiestaGetFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRemoveFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_removeFascicoloSIPATR")
    public JAXBElement<RichiestaRemoveFascicoloSIPATRType> createRichiestaRemoveFascicoloSIPATR(RichiestaRemoveFascicoloSIPATRType value) {
        return new JAXBElement<RichiestaRemoveFascicoloSIPATRType>(_RichiestaRemoveFascicoloSIPATR_QNAME, RichiestaRemoveFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaInviaFascicoloAttoDecretoCorteType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_inviaFascicoloAttoDecretoCorte")
    public JAXBElement<RichiestaInviaFascicoloAttoDecretoCorteType> createRichiestaInviaFascicoloAttoDecretoCorte(RichiestaInviaFascicoloAttoDecretoCorteType value) {
        return new JAXBElement<RichiestaInviaFascicoloAttoDecretoCorteType>(_RichiestaInviaFascicoloAttoDecretoCorte_QNAME, RichiestaInviaFascicoloAttoDecretoCorteType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaModifyMetadataDocumentoFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_modifyMetadataDocumentoFascicoloSIPATR")
    public JAXBElement<RichiestaModifyMetadataDocumentoFascicoloSIPATRType> createRichiestaModifyMetadataDocumentoFascicoloSIPATR(RichiestaModifyMetadataDocumentoFascicoloSIPATRType value) {
        return new JAXBElement<RichiestaModifyMetadataDocumentoFascicoloSIPATRType>(_RichiestaModifyMetadataDocumentoFascicoloSIPATR_QNAME, RichiestaModifyMetadataDocumentoFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaGetDocumentoFascicoloRaccoltaProvvisoriaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_getDocumentoFascicoloRaccoltaProvvisoria")
    public JAXBElement<RispostaGetDocumentoFascicoloRaccoltaProvvisoriaType> createRispostaGetDocumentoFascicoloRaccoltaProvvisoria(RispostaGetDocumentoFascicoloRaccoltaProvvisoriaType value) {
        return new JAXBElement<RispostaGetDocumentoFascicoloRaccoltaProvvisoriaType>(_RispostaGetDocumentoFascicoloRaccoltaProvvisoria_QNAME, RispostaGetDocumentoFascicoloRaccoltaProvvisoriaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRemoveDocumentoFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_removeDocumentoFascicoloAttoDecreto")
    public JAXBElement<RichiestaRemoveDocumentoFascicoloAttoDecretoType> createRichiestaRemoveDocumentoFascicoloAttoDecreto(RichiestaRemoveDocumentoFascicoloAttoDecretoType value) {
        return new JAXBElement<RichiestaRemoveDocumentoFascicoloAttoDecretoType>(_RichiestaRemoveDocumentoFascicoloAttoDecreto_QNAME, RichiestaRemoveDocumentoFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaCreateFascicoloDecretoSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_createFascicoloDecretoSIPATR")
    public JAXBElement<RichiestaCreateFascicoloDecretoSIPATRType> createRichiestaCreateFascicoloDecretoSIPATR(RichiestaCreateFascicoloDecretoSIPATRType value) {
        return new JAXBElement<RichiestaCreateFascicoloDecretoSIPATRType>(_RichiestaCreateFascicoloDecretoSIPATR_QNAME, RichiestaCreateFascicoloDecretoSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRemoveFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_removeFascicoloAttoDecreto")
    public JAXBElement<RichiestaRemoveFascicoloAttoDecretoType> createRichiestaRemoveFascicoloAttoDecreto(RichiestaRemoveFascicoloAttoDecretoType value) {
        return new JAXBElement<RichiestaRemoveFascicoloAttoDecretoType>(_RichiestaRemoveFascicoloAttoDecreto_QNAME, RichiestaRemoveFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRemoveDocumentoAggiuntivoFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_removeDocumentoAggiuntivoFascicoloAttoDecreto")
    public JAXBElement<RispostaRemoveDocumentoAggiuntivoFascicoloAttoDecretoType> createRispostaRemoveDocumentoAggiuntivoFascicoloAttoDecreto(RispostaRemoveDocumentoAggiuntivoFascicoloAttoDecretoType value) {
        return new JAXBElement<RispostaRemoveDocumentoAggiuntivoFascicoloAttoDecretoType>(_RispostaRemoveDocumentoAggiuntivoFascicoloAttoDecreto_QNAME, RispostaRemoveDocumentoAggiuntivoFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaModifyMetadataFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_modifyMetadataFascicoloSIPATR")
    public JAXBElement<RichiestaModifyMetadataFascicoloSIPATRType> createRichiestaModifyMetadataFascicoloSIPATR(RichiestaModifyMetadataFascicoloSIPATRType value) {
        return new JAXBElement<RichiestaModifyMetadataFascicoloSIPATRType>(_RichiestaModifyMetadataFascicoloSIPATR_QNAME, RichiestaModifyMetadataFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetElencoDocumentiFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_getElencoDocumentiFascicolo")
    public JAXBElement<RichiestaGetElencoDocumentiFascicoloType> createRichiestaGetElencoDocumentiFascicolo(RichiestaGetElencoDocumentiFascicoloType value) {
        return new JAXBElement<RichiestaGetElencoDocumentiFascicoloType>(_RichiestaGetElencoDocumentiFascicolo_QNAME, RichiestaGetElencoDocumentiFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_getDocumentoAggiuntivoFascicoloDecretoSIPATR")
    public JAXBElement<RispostaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType> createRispostaGetDocumentoAggiuntivoFascicoloDecretoSIPATR(RispostaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType value) {
        return new JAXBElement<RispostaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType>(_RispostaGetDocumentoAggiuntivoFascicoloDecretoSIPATR_QNAME, RispostaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAddCollegamentoFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_addCollegamentoFascicoloSIPATR")
    public JAXBElement<RichiestaAddCollegamentoFascicoloSIPATRType> createRichiestaAddCollegamentoFascicoloSIPATR(RichiestaAddCollegamentoFascicoloSIPATRType value) {
        return new JAXBElement<RichiestaAddCollegamentoFascicoloSIPATRType>(_RichiestaAddCollegamentoFascicoloSIPATR_QNAME, RichiestaAddCollegamentoFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaDownloadDocumentoUrlFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_downloadDocumentoUrlFascicoloAttoDecreto")
    public JAXBElement<RichiestaDownloadDocumentoUrlFascicoloAttoDecretoType> createRichiestaDownloadDocumentoUrlFascicoloAttoDecreto(RichiestaDownloadDocumentoUrlFascicoloAttoDecretoType value) {
        return new JAXBElement<RichiestaDownloadDocumentoUrlFascicoloAttoDecretoType>(_RichiestaDownloadDocumentoUrlFascicoloAttoDecreto_QNAME, RichiestaDownloadDocumentoUrlFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaDownloadDocumentoUrlFascicoloAllegatoDecretoIGBType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_downloadDocumentoUrlFascicoloAllegatoDecretoIGB")
    public JAXBElement<RispostaDownloadDocumentoUrlFascicoloAllegatoDecretoIGBType> createRispostaDownloadDocumentoUrlFascicoloAllegatoDecretoIGB(RispostaDownloadDocumentoUrlFascicoloAllegatoDecretoIGBType value) {
        return new JAXBElement<RispostaDownloadDocumentoUrlFascicoloAllegatoDecretoIGBType>(_RispostaDownloadDocumentoUrlFascicoloAllegatoDecretoIGB_QNAME, RispostaDownloadDocumentoUrlFascicoloAllegatoDecretoIGBType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGBType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_modifyMetadatiDocumentoFascicoloAllegatoDecretoIGB")
    public JAXBElement<RichiestaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGBType> createRichiestaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGB(RichiestaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGBType value) {
        return new JAXBElement<RichiestaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGBType>(_RichiestaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGB_QNAME, RichiestaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGBType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaChangeStatoFascicoloDecretoSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_changeStatoFascicoloDecretoSIPATR")
    public JAXBElement<RichiestaChangeStatoFascicoloDecretoSIPATRType> createRichiestaChangeStatoFascicoloDecretoSIPATR(RichiestaChangeStatoFascicoloDecretoSIPATRType value) {
        return new JAXBElement<RichiestaChangeStatoFascicoloDecretoSIPATRType>(_RichiestaChangeStatoFascicoloDecretoSIPATR_QNAME, RichiestaChangeStatoFascicoloDecretoSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetElencoDocumentiFascicoloAllegatoDecretoIGBType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_getElencoDocumentiFascicoloAllegatoDecretoIGB")
    public JAXBElement<RichiestaGetElencoDocumentiFascicoloAllegatoDecretoIGBType> createRichiestaGetElencoDocumentiFascicoloAllegatoDecretoIGB(RichiestaGetElencoDocumentiFascicoloAllegatoDecretoIGBType value) {
        return new JAXBElement<RichiestaGetElencoDocumentiFascicoloAllegatoDecretoIGBType>(_RichiestaGetElencoDocumentiFascicoloAllegatoDecretoIGB_QNAME, RichiestaGetElencoDocumentiFascicoloAllegatoDecretoIGBType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaGetFascicoloAttoDecretoCorteType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_getFascicoloAttoDecretoCorte")
    public JAXBElement<RispostaGetFascicoloAttoDecretoCorteType> createRispostaGetFascicoloAttoDecretoCorte(RispostaGetFascicoloAttoDecretoCorteType value) {
        return new JAXBElement<RispostaGetFascicoloAttoDecretoCorteType>(_RispostaGetFascicoloAttoDecretoCorte_QNAME, RispostaGetFascicoloAttoDecretoCorteType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetFascicoloSIPATRAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_getFascicoloSIPATRAttoDecreto")
    public JAXBElement<RichiestaGetFascicoloSIPATRAttoDecretoType> createRichiestaGetFascicoloSIPATRAttoDecreto(RichiestaGetFascicoloSIPATRAttoDecretoType value) {
        return new JAXBElement<RichiestaGetFascicoloSIPATRAttoDecretoType>(_RichiestaGetFascicoloSIPATRAttoDecreto_QNAME, RichiestaGetFascicoloSIPATRAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAddDocumentoFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_addDocumentoFascicoloAttoDecreto")
    public JAXBElement<RichiestaAddDocumentoFascicoloAttoDecretoType> createRichiestaAddDocumentoFascicoloAttoDecreto(RichiestaAddDocumentoFascicoloAttoDecretoType value) {
        return new JAXBElement<RichiestaAddDocumentoFascicoloAttoDecretoType>(_RichiestaAddDocumentoFascicoloAttoDecreto_QNAME, RichiestaAddDocumentoFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRemoveFascicoloDecretoSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_removeFascicoloDecretoSIPATR")
    public JAXBElement<RispostaRemoveFascicoloDecretoSIPATRType> createRispostaRemoveFascicoloDecretoSIPATR(RispostaRemoveFascicoloDecretoSIPATRType value) {
        return new JAXBElement<RispostaRemoveFascicoloDecretoSIPATRType>(_RispostaRemoveFascicoloDecretoSIPATR_QNAME, RispostaRemoveFascicoloDecretoSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRemoveDocumentoFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_removeDocumentoFascicoloSIPATR")
    public JAXBElement<RispostaRemoveDocumentoFascicoloSIPATRType> createRispostaRemoveDocumentoFascicoloSIPATR(RispostaRemoveDocumentoFascicoloSIPATRType value) {
        return new JAXBElement<RispostaRemoveDocumentoFascicoloSIPATRType>(_RispostaRemoveDocumentoFascicoloSIPATR_QNAME, RispostaRemoveDocumentoFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaAddDocumentoAggiuntivoFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_addDocumentoAggiuntivoFascicoloAttoDecreto")
    public JAXBElement<RispostaAddDocumentoAggiuntivoFascicoloAttoDecretoType> createRispostaAddDocumentoAggiuntivoFascicoloAttoDecreto(RispostaAddDocumentoAggiuntivoFascicoloAttoDecretoType value) {
        return new JAXBElement<RispostaAddDocumentoAggiuntivoFascicoloAttoDecretoType>(_RispostaAddDocumentoAggiuntivoFascicoloAttoDecreto_QNAME, RispostaAddDocumentoAggiuntivoFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRemoveDocumentoFascicoloAttoDecretoCorteType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_removeDocumentoFascicoloAttoDecretoCorte")
    public JAXBElement<RispostaRemoveDocumentoFascicoloAttoDecretoCorteType> createRispostaRemoveDocumentoFascicoloAttoDecretoCorte(RispostaRemoveDocumentoFascicoloAttoDecretoCorteType value) {
        return new JAXBElement<RispostaRemoveDocumentoFascicoloAttoDecretoCorteType>(_RispostaRemoveDocumentoFascicoloAttoDecretoCorte_QNAME, RispostaRemoveDocumentoFascicoloAttoDecretoCorteType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaDownloadDocumentoFascicoloAttoDecretoCorteType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_downloadDocumentoFascicoloAttoDecretoCorte")
    public JAXBElement<RichiestaDownloadDocumentoFascicoloAttoDecretoCorteType> createRichiestaDownloadDocumentoFascicoloAttoDecretoCorte(RichiestaDownloadDocumentoFascicoloAttoDecretoCorteType value) {
        return new JAXBElement<RichiestaDownloadDocumentoFascicoloAttoDecretoCorteType>(_RichiestaDownloadDocumentoFascicoloAttoDecretoCorte_QNAME, RichiestaDownloadDocumentoFascicoloAttoDecretoCorteType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaModifyMetadataFascicoloDecretoSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_modifyMetadataFascicoloDecretoSIPATR")
    public JAXBElement<RichiestaModifyMetadataFascicoloDecretoSIPATRType> createRichiestaModifyMetadataFascicoloDecretoSIPATR(RichiestaModifyMetadataFascicoloDecretoSIPATRType value) {
        return new JAXBElement<RichiestaModifyMetadataFascicoloDecretoSIPATRType>(_RichiestaModifyMetadataFascicoloDecretoSIPATR_QNAME, RichiestaModifyMetadataFascicoloDecretoSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaGetElencoDocumentiFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_getElencoDocumentiFascicolo")
    public JAXBElement<RispostaGetElencoDocumentiFascicoloType> createRispostaGetElencoDocumentiFascicolo(RispostaGetElencoDocumentiFascicoloType value) {
        return new JAXBElement<RispostaGetElencoDocumentiFascicoloType>(_RispostaGetElencoDocumentiFascicolo_QNAME, RispostaGetElencoDocumentiFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaDownloadDocumentoFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_downloadDocumentoFascicoloSIPATR")
    public JAXBElement<RispostaDownloadDocumentoFascicoloSIPATRType> createRispostaDownloadDocumentoFascicoloSIPATR(RispostaDownloadDocumentoFascicoloSIPATRType value) {
        return new JAXBElement<RispostaDownloadDocumentoFascicoloSIPATRType>(_RispostaDownloadDocumentoFascicoloSIPATR_QNAME, RispostaDownloadDocumentoFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRemoveCollegamentoFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_removeCollegamentoFascicoloSIPATR")
    public JAXBElement<RichiestaRemoveCollegamentoFascicoloSIPATRType> createRichiestaRemoveCollegamentoFascicoloSIPATR(RichiestaRemoveCollegamentoFascicoloSIPATRType value) {
        return new JAXBElement<RichiestaRemoveCollegamentoFascicoloSIPATRType>(_RichiestaRemoveCollegamentoFascicoloSIPATR_QNAME, RichiestaRemoveCollegamentoFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRemoveDocumentoFascicoloAllegatoDecretoIGBType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_removeDocumentoFascicoloAllegatoDecretoIGB")
    public JAXBElement<RichiestaRemoveDocumentoFascicoloAllegatoDecretoIGBType> createRichiestaRemoveDocumentoFascicoloAllegatoDecretoIGB(RichiestaRemoveDocumentoFascicoloAllegatoDecretoIGBType value) {
        return new JAXBElement<RichiestaRemoveDocumentoFascicoloAllegatoDecretoIGBType>(_RichiestaRemoveDocumentoFascicoloAllegatoDecretoIGB_QNAME, RichiestaRemoveDocumentoFascicoloAllegatoDecretoIGBType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaMergeFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_mergeFascicoloAttoDecreto")
    public JAXBElement<RispostaMergeFascicoloAttoDecretoType> createRispostaMergeFascicoloAttoDecreto(RispostaMergeFascicoloAttoDecretoType value) {
        return new JAXBElement<RispostaMergeFascicoloAttoDecretoType>(_RispostaMergeFascicoloAttoDecreto_QNAME, RispostaMergeFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaAddDocumentoFascicoloAttoDecretoCorteType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_addDocumentoFascicoloAttoDecretoCorte")
    public JAXBElement<RispostaAddDocumentoFascicoloAttoDecretoCorteType> createRispostaAddDocumentoFascicoloAttoDecretoCorte(RispostaAddDocumentoFascicoloAttoDecretoCorteType value) {
        return new JAXBElement<RispostaAddDocumentoFascicoloAttoDecretoCorteType>(_RispostaAddDocumentoFascicoloAttoDecretoCorte_QNAME, RispostaAddDocumentoFascicoloAttoDecretoCorteType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaDownloadDocumentoUrlFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_downloadDocumentoUrlFascicoloSIPATR")
    public JAXBElement<RispostaDownloadDocumentoUrlFascicoloSIPATRType> createRispostaDownloadDocumentoUrlFascicoloSIPATR(RispostaDownloadDocumentoUrlFascicoloSIPATRType value) {
        return new JAXBElement<RispostaDownloadDocumentoUrlFascicoloSIPATRType>(_RispostaDownloadDocumentoUrlFascicoloSIPATR_QNAME, RispostaDownloadDocumentoUrlFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaGetDocumentoAggiuntivoFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_getDocumentoAggiuntivoFascicoloAttoDecreto")
    public JAXBElement<RispostaGetDocumentoAggiuntivoFascicoloAttoDecretoType> createRispostaGetDocumentoAggiuntivoFascicoloAttoDecreto(RispostaGetDocumentoAggiuntivoFascicoloAttoDecretoType value) {
        return new JAXBElement<RispostaGetDocumentoAggiuntivoFascicoloAttoDecretoType>(_RispostaGetDocumentoAggiuntivoFascicoloAttoDecreto_QNAME, RispostaGetDocumentoAggiuntivoFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaCopyFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_copyFascicoloAttoDecreto")
    public JAXBElement<RispostaCopyFascicoloAttoDecretoType> createRispostaCopyFascicoloAttoDecreto(RispostaCopyFascicoloAttoDecretoType value) {
        return new JAXBElement<RispostaCopyFascicoloAttoDecretoType>(_RispostaCopyFascicoloAttoDecreto_QNAME, RispostaCopyFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRemoveDocumentoAggiuntivoFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_removeDocumentoAggiuntivoFascicoloAttoDecreto")
    public JAXBElement<RichiestaRemoveDocumentoAggiuntivoFascicoloAttoDecretoType> createRichiestaRemoveDocumentoAggiuntivoFascicoloAttoDecreto(RichiestaRemoveDocumentoAggiuntivoFascicoloAttoDecretoType value) {
        return new JAXBElement<RichiestaRemoveDocumentoAggiuntivoFascicoloAttoDecretoType>(_RichiestaRemoveDocumentoAggiuntivoFascicoloAttoDecreto_QNAME, RichiestaRemoveDocumentoAggiuntivoFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetDocumentoFascicoloSIPATRAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_getDocumentoFascicoloSIPATRAttoDecreto")
    public JAXBElement<RichiestaGetDocumentoFascicoloSIPATRAttoDecretoType> createRichiestaGetDocumentoFascicoloSIPATRAttoDecreto(RichiestaGetDocumentoFascicoloSIPATRAttoDecretoType value) {
        return new JAXBElement<RichiestaGetDocumentoFascicoloSIPATRAttoDecretoType>(_RichiestaGetDocumentoFascicoloSIPATRAttoDecreto_QNAME, RichiestaGetDocumentoFascicoloSIPATRAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaDownloadDocumentoFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_downloadDocumentoAggiuntivoFascicoloDecretoSIPATR")
    public JAXBElement<RispostaDownloadDocumentoFascicoloSIPATRType> createRispostaDownloadDocumentoAggiuntivoFascicoloDecretoSIPATR(RispostaDownloadDocumentoFascicoloSIPATRType value) {
        return new JAXBElement<RispostaDownloadDocumentoFascicoloSIPATRType>(_RispostaDownloadDocumentoAggiuntivoFascicoloDecretoSIPATR_QNAME, RispostaDownloadDocumentoFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaAddDocumentoFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_addDocumentoFascicoloSIPATR")
    public JAXBElement<RispostaAddDocumentoFascicoloSIPATRType> createRispostaAddDocumentoFascicoloSIPATR(RispostaAddDocumentoFascicoloSIPATRType value) {
        return new JAXBElement<RispostaAddDocumentoFascicoloSIPATRType>(_RispostaAddDocumentoFascicoloSIPATR_QNAME, RispostaAddDocumentoFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaAddDocumentoFascicoloAllegatoDecretoIGBType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_addDocumentoFascicoloAllegatoDecretoIGB")
    public JAXBElement<RispostaAddDocumentoFascicoloAllegatoDecretoIGBType> createRispostaAddDocumentoFascicoloAllegatoDecretoIGB(RispostaAddDocumentoFascicoloAllegatoDecretoIGBType value) {
        return new JAXBElement<RispostaAddDocumentoFascicoloAllegatoDecretoIGBType>(_RispostaAddDocumentoFascicoloAllegatoDecretoIGB_QNAME, RispostaAddDocumentoFascicoloAllegatoDecretoIGBType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaDownloadDocumentoUrlFascicoloRaccoltaProvvisoriaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_downloadDocumentoUrlFascicoloRaccoltaProvvisoria")
    public JAXBElement<RispostaDownloadDocumentoUrlFascicoloRaccoltaProvvisoriaType> createRispostaDownloadDocumentoUrlFascicoloRaccoltaProvvisoria(RispostaDownloadDocumentoUrlFascicoloRaccoltaProvvisoriaType value) {
        return new JAXBElement<RispostaDownloadDocumentoUrlFascicoloRaccoltaProvvisoriaType>(_RispostaDownloadDocumentoUrlFascicoloRaccoltaProvvisoria_QNAME, RispostaDownloadDocumentoUrlFascicoloRaccoltaProvvisoriaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaGetDocumentoFascicoloSIPATRAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_getDocumentoFascicoloSIPATRAttoDecreto")
    public JAXBElement<RispostaGetDocumentoFascicoloSIPATRAttoDecretoType> createRispostaGetDocumentoFascicoloSIPATRAttoDecreto(RispostaGetDocumentoFascicoloSIPATRAttoDecretoType value) {
        return new JAXBElement<RispostaGetDocumentoFascicoloSIPATRAttoDecretoType>(_RispostaGetDocumentoFascicoloSIPATRAttoDecreto_QNAME, RispostaGetDocumentoFascicoloSIPATRAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetDocumentoFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_getDocumentoFascicoloSIPATR")
    public JAXBElement<RichiestaGetDocumentoFascicoloSIPATRType> createRichiestaGetDocumentoFascicoloSIPATR(RichiestaGetDocumentoFascicoloSIPATRType value) {
        return new JAXBElement<RichiestaGetDocumentoFascicoloSIPATRType>(_RichiestaGetDocumentoFascicoloSIPATR_QNAME, RichiestaGetDocumentoFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_getDocumentoAggiuntivoFascicoloDecretoSIPATR")
    public JAXBElement<RichiestaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType> createRichiestaGetDocumentoAggiuntivoFascicoloDecretoSIPATR(RichiestaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType value) {
        return new JAXBElement<RichiestaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType>(_RichiestaGetDocumentoAggiuntivoFascicoloDecretoSIPATR_QNAME, RichiestaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaChangeStatoFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_changeStatoFascicoloAttoDecreto")
    public JAXBElement<RichiestaChangeStatoFascicoloAttoDecretoType> createRichiestaChangeStatoFascicoloAttoDecreto(RichiestaChangeStatoFascicoloAttoDecretoType value) {
        return new JAXBElement<RichiestaChangeStatoFascicoloAttoDecretoType>(_RichiestaChangeStatoFascicoloAttoDecreto_QNAME, RichiestaChangeStatoFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetDocumentoAggiuntivoFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_getDocumentoAggiuntivoFascicoloAttoDecreto")
    public JAXBElement<RichiestaGetDocumentoAggiuntivoFascicoloAttoDecretoType> createRichiestaGetDocumentoAggiuntivoFascicoloAttoDecreto(RichiestaGetDocumentoAggiuntivoFascicoloAttoDecretoType value) {
        return new JAXBElement<RichiestaGetDocumentoAggiuntivoFascicoloAttoDecretoType>(_RichiestaGetDocumentoAggiuntivoFascicoloAttoDecreto_QNAME, RichiestaGetDocumentoAggiuntivoFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaCreateFascicoloRaccoltaProvvisoriaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_createFascicoloRaccoltaProvvisoria")
    public JAXBElement<RichiestaCreateFascicoloRaccoltaProvvisoriaType> createRichiestaCreateFascicoloRaccoltaProvvisoria(RichiestaCreateFascicoloRaccoltaProvvisoriaType value) {
        return new JAXBElement<RichiestaCreateFascicoloRaccoltaProvvisoriaType>(_RichiestaCreateFascicoloRaccoltaProvvisoria_QNAME, RichiestaCreateFascicoloRaccoltaProvvisoriaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetDocumentoFascicoloAllegatoDecretoIGBType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_getDocumentoFascicoloAllegatoDecretoIGB")
    public JAXBElement<RichiestaGetDocumentoFascicoloAllegatoDecretoIGBType> createRichiestaGetDocumentoFascicoloAllegatoDecretoIGB(RichiestaGetDocumentoFascicoloAllegatoDecretoIGBType value) {
        return new JAXBElement<RichiestaGetDocumentoFascicoloAllegatoDecretoIGBType>(_RichiestaGetDocumentoFascicoloAllegatoDecretoIGB_QNAME, RichiestaGetDocumentoFascicoloAllegatoDecretoIGBType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaArchiveFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_archiveFascicoloAttoDecreto")
    public JAXBElement<RispostaArchiveFascicoloAttoDecretoType> createRispostaArchiveFascicoloAttoDecreto(RispostaArchiveFascicoloAttoDecretoType value) {
        return new JAXBElement<RispostaArchiveFascicoloAttoDecretoType>(_RispostaArchiveFascicoloAttoDecreto_QNAME, RispostaArchiveFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaInviaFascicoloAttoDecretoCorteType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_inviaFascicoloAttoDecretoCorte")
    public JAXBElement<RispostaInviaFascicoloAttoDecretoCorteType> createRispostaInviaFascicoloAttoDecretoCorte(RispostaInviaFascicoloAttoDecretoCorteType value) {
        return new JAXBElement<RispostaInviaFascicoloAttoDecretoCorteType>(_RispostaInviaFascicoloAttoDecretoCorte_QNAME, RispostaInviaFascicoloAttoDecretoCorteType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaModifyMetadataFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_modifyMetadataFascicoloSIPATR")
    public JAXBElement<RispostaModifyMetadataFascicoloSIPATRType> createRispostaModifyMetadataFascicoloSIPATR(RispostaModifyMetadataFascicoloSIPATRType value) {
        return new JAXBElement<RispostaModifyMetadataFascicoloSIPATRType>(_RispostaModifyMetadataFascicoloSIPATR_QNAME, RispostaModifyMetadataFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaDownloadDocumentoFascicoloAllegatoDecretoIGBType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_downloadDocumentoFascicoloAllegatoDecretoIGB")
    public JAXBElement<RichiestaDownloadDocumentoFascicoloAllegatoDecretoIGBType> createRichiestaDownloadDocumentoFascicoloAllegatoDecretoIGB(RichiestaDownloadDocumentoFascicoloAllegatoDecretoIGBType value) {
        return new JAXBElement<RichiestaDownloadDocumentoFascicoloAllegatoDecretoIGBType>(_RichiestaDownloadDocumentoFascicoloAllegatoDecretoIGB_QNAME, RichiestaDownloadDocumentoFascicoloAllegatoDecretoIGBType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaUpdateContentDocumentoFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_updateContentDocumentoFascicoloAttoDecreto")
    public JAXBElement<RispostaUpdateContentDocumentoFascicoloAttoDecretoType> createRispostaUpdateContentDocumentoFascicoloAttoDecreto(RispostaUpdateContentDocumentoFascicoloAttoDecretoType value) {
        return new JAXBElement<RispostaUpdateContentDocumentoFascicoloAttoDecretoType>(_RispostaUpdateContentDocumentoFascicoloAttoDecreto_QNAME, RispostaUpdateContentDocumentoFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaDownloadDocumentoAggiuntivoFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_downloadDocumentoAggiuntivoFascicoloAttoDecreto")
    public JAXBElement<RichiestaDownloadDocumentoAggiuntivoFascicoloAttoDecretoType> createRichiestaDownloadDocumentoAggiuntivoFascicoloAttoDecreto(RichiestaDownloadDocumentoAggiuntivoFascicoloAttoDecretoType value) {
        return new JAXBElement<RichiestaDownloadDocumentoAggiuntivoFascicoloAttoDecretoType>(_RichiestaDownloadDocumentoAggiuntivoFascicoloAttoDecreto_QNAME, RichiestaDownloadDocumentoAggiuntivoFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaDownloadDocumentoUrlFascicoloAllegatoDecretoIGBType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_downloadDocumentoUrlFascicoloAllegatoDecretoIGB")
    public JAXBElement<RichiestaDownloadDocumentoUrlFascicoloAllegatoDecretoIGBType> createRichiestaDownloadDocumentoUrlFascicoloAllegatoDecretoIGB(RichiestaDownloadDocumentoUrlFascicoloAllegatoDecretoIGBType value) {
        return new JAXBElement<RichiestaDownloadDocumentoUrlFascicoloAllegatoDecretoIGBType>(_RichiestaDownloadDocumentoUrlFascicoloAllegatoDecretoIGB_QNAME, RichiestaDownloadDocumentoUrlFascicoloAllegatoDecretoIGBType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetDocumentoFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_getDocumentoFascicoloAttoDecreto")
    public JAXBElement<RichiestaGetDocumentoFascicoloAttoDecretoType> createRichiestaGetDocumentoFascicoloAttoDecreto(RichiestaGetDocumentoFascicoloAttoDecretoType value) {
        return new JAXBElement<RichiestaGetDocumentoFascicoloAttoDecretoType>(_RichiestaGetDocumentoFascicoloAttoDecreto_QNAME, RichiestaGetDocumentoFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaModifyMetadatiDocumentoFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_modifyMetadatiDocumentoFascicoloAttoDecreto")
    public JAXBElement<RichiestaModifyMetadatiDocumentoFascicoloAttoDecretoType> createRichiestaModifyMetadatiDocumentoFascicoloAttoDecreto(RichiestaModifyMetadatiDocumentoFascicoloAttoDecretoType value) {
        return new JAXBElement<RichiestaModifyMetadatiDocumentoFascicoloAttoDecretoType>(_RichiestaModifyMetadatiDocumentoFascicoloAttoDecreto_QNAME, RichiestaModifyMetadatiDocumentoFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaDownloadDocumentoFascicoloDecretoSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_downloadDocumentoFascicoloSIPATRAttoDecreto")
    public JAXBElement<RispostaDownloadDocumentoFascicoloDecretoSIPATRType> createRispostaDownloadDocumentoFascicoloSIPATRAttoDecreto(RispostaDownloadDocumentoFascicoloDecretoSIPATRType value) {
        return new JAXBElement<RispostaDownloadDocumentoFascicoloDecretoSIPATRType>(_RispostaDownloadDocumentoFascicoloSIPATRAttoDecreto_QNAME, RispostaDownloadDocumentoFascicoloDecretoSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaModifyMetadataDocumentoFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_modifyMetadataDocumentoFascicoloSIPATR")
    public JAXBElement<RispostaModifyMetadataDocumentoFascicoloSIPATRType> createRispostaModifyMetadataDocumentoFascicoloSIPATR(RispostaModifyMetadataDocumentoFascicoloSIPATRType value) {
        return new JAXBElement<RispostaModifyMetadataDocumentoFascicoloSIPATRType>(_RispostaModifyMetadataDocumentoFascicoloSIPATR_QNAME, RispostaModifyMetadataDocumentoFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaCreateFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_createFascicoloAttoDecreto")
    public JAXBElement<RichiestaCreateFascicoloAttoDecretoType> createRichiestaCreateFascicoloAttoDecreto(RichiestaCreateFascicoloAttoDecretoType value) {
        return new JAXBElement<RichiestaCreateFascicoloAttoDecretoType>(_RichiestaCreateFascicoloAttoDecreto_QNAME, RichiestaCreateFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaArchiveFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_archiveFascicoloAttoDecreto")
    public JAXBElement<RichiestaArchiveFascicoloAttoDecretoType> createRichiestaArchiveFascicoloAttoDecreto(RichiestaArchiveFascicoloAttoDecretoType value) {
        return new JAXBElement<RichiestaArchiveFascicoloAttoDecretoType>(_RichiestaArchiveFascicoloAttoDecreto_QNAME, RichiestaArchiveFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaUpdateContentDocumentoFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_updateMetadatiDocumentoAggiuntivoFascicoloAttoDecreto")
    public JAXBElement<RispostaUpdateContentDocumentoFascicoloAttoDecretoType> createRispostaUpdateMetadatiDocumentoAggiuntivoFascicoloAttoDecreto(RispostaUpdateContentDocumentoFascicoloAttoDecretoType value) {
        return new JAXBElement<RispostaUpdateContentDocumentoFascicoloAttoDecretoType>(_RispostaUpdateMetadatiDocumentoAggiuntivoFascicoloAttoDecreto_QNAME, RispostaUpdateContentDocumentoFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaGetElencoFascicoliRaccoltaProvvisoriaAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_getElencoFascicoliRaccoltaProvvisoriaAttoDecreto")
    public JAXBElement<RispostaGetElencoFascicoliRaccoltaProvvisoriaAttoDecretoType> createRispostaGetElencoFascicoliRaccoltaProvvisoriaAttoDecreto(RispostaGetElencoFascicoliRaccoltaProvvisoriaAttoDecretoType value) {
        return new JAXBElement<RispostaGetElencoFascicoliRaccoltaProvvisoriaAttoDecretoType>(_RispostaGetElencoFascicoliRaccoltaProvvisoriaAttoDecreto_QNAME, RispostaGetElencoFascicoliRaccoltaProvvisoriaAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAddDocumentoFascicoloAllegatoDecretoIGBType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_addDocumentoFascicoloAllegatoDecretoIGB")
    public JAXBElement<RichiestaAddDocumentoFascicoloAllegatoDecretoIGBType> createRichiestaAddDocumentoFascicoloAllegatoDecretoIGB(RichiestaAddDocumentoFascicoloAllegatoDecretoIGBType value) {
        return new JAXBElement<RichiestaAddDocumentoFascicoloAllegatoDecretoIGBType>(_RichiestaAddDocumentoFascicoloAllegatoDecretoIGB_QNAME, RichiestaAddDocumentoFascicoloAllegatoDecretoIGBType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaDownloadDocumentoFascicoloRaccoltaProvvisoriaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_downloadDocumentoFascicoloRaccoltaProvvisoria")
    public JAXBElement<RichiestaDownloadDocumentoFascicoloRaccoltaProvvisoriaType> createRichiestaDownloadDocumentoFascicoloRaccoltaProvvisoria(RichiestaDownloadDocumentoFascicoloRaccoltaProvvisoriaType value) {
        return new JAXBElement<RichiestaDownloadDocumentoFascicoloRaccoltaProvvisoriaType>(_RichiestaDownloadDocumentoFascicoloRaccoltaProvvisoria_QNAME, RichiestaDownloadDocumentoFascicoloRaccoltaProvvisoriaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaInviaFascicoloAttoDecretoCorteType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_inviaDocAggiuntiviFascicoloAttoDecretoCorte")
    public JAXBElement<RispostaInviaFascicoloAttoDecretoCorteType> createRispostaInviaDocAggiuntiviFascicoloAttoDecretoCorte(RispostaInviaFascicoloAttoDecretoCorteType value) {
        return new JAXBElement<RispostaInviaFascicoloAttoDecretoCorteType>(_RispostaInviaDocAggiuntiviFascicoloAttoDecretoCorte_QNAME, RispostaInviaFascicoloAttoDecretoCorteType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRemoveFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_removeFascicoloSIPATR")
    public JAXBElement<RispostaRemoveFascicoloSIPATRType> createRispostaRemoveFascicoloSIPATR(RispostaRemoveFascicoloSIPATRType value) {
        return new JAXBElement<RispostaRemoveFascicoloSIPATRType>(_RispostaRemoveFascicoloSIPATR_QNAME, RispostaRemoveFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaGetDocumentoFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_getDocumentoFascicoloSIPATR")
    public JAXBElement<RispostaGetDocumentoFascicoloSIPATRType> createRispostaGetDocumentoFascicoloSIPATR(RispostaGetDocumentoFascicoloSIPATRType value) {
        return new JAXBElement<RispostaGetDocumentoFascicoloSIPATRType>(_RispostaGetDocumentoFascicoloSIPATR_QNAME, RispostaGetDocumentoFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaMergeFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_mergeFascicoloAttoDecreto")
    public JAXBElement<RichiestaMergeFascicoloAttoDecretoType> createRichiestaMergeFascicoloAttoDecreto(RichiestaMergeFascicoloAttoDecretoType value) {
        return new JAXBElement<RichiestaMergeFascicoloAttoDecretoType>(_RichiestaMergeFascicoloAttoDecreto_QNAME, RichiestaMergeFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetFascicoloAttoDecretoCorteType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_getFascicoloAttoDecretoCorte")
    public JAXBElement<RichiestaGetFascicoloAttoDecretoCorteType> createRichiestaGetFascicoloAttoDecretoCorte(RichiestaGetFascicoloAttoDecretoCorteType value) {
        return new JAXBElement<RichiestaGetFascicoloAttoDecretoCorteType>(_RichiestaGetFascicoloAttoDecretoCorte_QNAME, RichiestaGetFascicoloAttoDecretoCorteType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaDownloadDocumentoFascicoloDecretoSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_downloadDocumentoFascicoloSIPATRAttoDecreto")
    public JAXBElement<RichiestaDownloadDocumentoFascicoloDecretoSIPATRType> createRichiestaDownloadDocumentoFascicoloSIPATRAttoDecreto(RichiestaDownloadDocumentoFascicoloDecretoSIPATRType value) {
        return new JAXBElement<RichiestaDownloadDocumentoFascicoloDecretoSIPATRType>(_RichiestaDownloadDocumentoFascicoloSIPATRAttoDecreto_QNAME, RichiestaDownloadDocumentoFascicoloDecretoSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetMonitoraggioDocumentiType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_monitoraggioDocumenti")
    public JAXBElement<RichiestaGetMonitoraggioDocumentiType> createRichiestaMonitoraggioDocumenti(RichiestaGetMonitoraggioDocumentiType value) {
        return new JAXBElement<RichiestaGetMonitoraggioDocumentiType>(_RichiestaMonitoraggioDocumenti_QNAME, RichiestaGetMonitoraggioDocumentiType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAddDocumentoAggiuntivoFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_addDocumentoAggiuntivoFascicoloAttoDecreto")
    public JAXBElement<RichiestaAddDocumentoAggiuntivoFascicoloAttoDecretoType> createRichiestaAddDocumentoAggiuntivoFascicoloAttoDecreto(RichiestaAddDocumentoAggiuntivoFascicoloAttoDecretoType value) {
        return new JAXBElement<RichiestaAddDocumentoAggiuntivoFascicoloAttoDecretoType>(_RichiestaAddDocumentoAggiuntivoFascicoloAttoDecreto_QNAME, RichiestaAddDocumentoAggiuntivoFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaUpdateContentDocumentoFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_updateContentDocumentoFascicoloAttoDecreto")
    public JAXBElement<RichiestaUpdateContentDocumentoFascicoloAttoDecretoType> createRichiestaUpdateContentDocumentoFascicoloAttoDecreto(RichiestaUpdateContentDocumentoFascicoloAttoDecretoType value) {
        return new JAXBElement<RichiestaUpdateContentDocumentoFascicoloAttoDecretoType>(_RichiestaUpdateContentDocumentoFascicoloAttoDecreto_QNAME, RichiestaUpdateContentDocumentoFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaConvertiDocumentoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_convertiDocumento")
    public JAXBElement<RichiestaConvertiDocumentoType> createRichiestaConvertiDocumento(RichiestaConvertiDocumentoType value) {
        return new JAXBElement<RichiestaConvertiDocumentoType>(_RichiestaConvertiDocumento_QNAME, RichiestaConvertiDocumentoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaChangeStatoFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_changeStatoFascicoloAttoDecreto")
    public JAXBElement<RispostaChangeStatoFascicoloAttoDecretoType> createRispostaChangeStatoFascicoloAttoDecreto(RispostaChangeStatoFascicoloAttoDecretoType value) {
        return new JAXBElement<RispostaChangeStatoFascicoloAttoDecretoType>(_RispostaChangeStatoFascicoloAttoDecreto_QNAME, RispostaChangeStatoFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaCreateFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_createFascicoloSIPATR")
    public JAXBElement<RichiestaCreateFascicoloSIPATRType> createRichiestaCreateFascicoloSIPATR(RichiestaCreateFascicoloSIPATRType value) {
        return new JAXBElement<RichiestaCreateFascicoloSIPATRType>(_RichiestaCreateFascicoloSIPATR_QNAME, RichiestaCreateFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaDownloadDocumentoFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_downloadDocumentoAggiuntivoFascicoloDecretoSIPATR")
    public JAXBElement<RichiestaDownloadDocumentoFascicoloSIPATRType> createRichiestaDownloadDocumentoAggiuntivoFascicoloDecretoSIPATR(RichiestaDownloadDocumentoFascicoloSIPATRType value) {
        return new JAXBElement<RichiestaDownloadDocumentoFascicoloSIPATRType>(_RichiestaDownloadDocumentoAggiuntivoFascicoloDecretoSIPATR_QNAME, RichiestaDownloadDocumentoFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaChangeStatoFascicoloRaccoltaProvvisoriaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_changeStatoFascicoloRaccoltaProvvisoria")
    public JAXBElement<RichiestaChangeStatoFascicoloRaccoltaProvvisoriaType> createRichiestaChangeStatoFascicoloRaccoltaProvvisoria(RichiestaChangeStatoFascicoloRaccoltaProvvisoriaType value) {
        return new JAXBElement<RichiestaChangeStatoFascicoloRaccoltaProvvisoriaType>(_RichiestaChangeStatoFascicoloRaccoltaProvvisoria_QNAME, RichiestaChangeStatoFascicoloRaccoltaProvvisoriaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRemoveDocumentoFascicoloRaccoltaProvvisoriaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_removeDocumentoFascicoloRaccoltaProvvisoria")
    public JAXBElement<RichiestaRemoveDocumentoFascicoloRaccoltaProvvisoriaType> createRichiestaRemoveDocumentoFascicoloRaccoltaProvvisoria(RichiestaRemoveDocumentoFascicoloRaccoltaProvvisoriaType value) {
        return new JAXBElement<RichiestaRemoveDocumentoFascicoloRaccoltaProvvisoriaType>(_RichiestaRemoveDocumentoFascicoloRaccoltaProvvisoria_QNAME, RichiestaRemoveDocumentoFascicoloRaccoltaProvvisoriaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaCreateFascicoloRaccoltaProvvisoriaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_createFascicoloRaccoltaProvvisoria")
    public JAXBElement<RispostaCreateFascicoloRaccoltaProvvisoriaType> createRispostaCreateFascicoloRaccoltaProvvisoria(RispostaCreateFascicoloRaccoltaProvvisoriaType value) {
        return new JAXBElement<RispostaCreateFascicoloRaccoltaProvvisoriaType>(_RispostaCreateFascicoloRaccoltaProvvisoria_QNAME, RispostaCreateFascicoloRaccoltaProvvisoriaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaDownloadDocumentoFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_downloadDocumentoFascicoloAttoDecreto")
    public JAXBElement<RichiestaDownloadDocumentoFascicoloAttoDecretoType> createRichiestaDownloadDocumentoFascicoloAttoDecreto(RichiestaDownloadDocumentoFascicoloAttoDecretoType value) {
        return new JAXBElement<RichiestaDownloadDocumentoFascicoloAttoDecretoType>(_RichiestaDownloadDocumentoFascicoloAttoDecreto_QNAME, RichiestaDownloadDocumentoFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaModifyMetadataFascicoloRaccoltaProvvisoriaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_modifyMetadatiFascicoloRaccoltaProvvisoria")
    public JAXBElement<RichiestaModifyMetadataFascicoloRaccoltaProvvisoriaType> createRichiestaModifyMetadatiFascicoloRaccoltaProvvisoria(RichiestaModifyMetadataFascicoloRaccoltaProvvisoriaType value) {
        return new JAXBElement<RichiestaModifyMetadataFascicoloRaccoltaProvvisoriaType>(_RichiestaModifyMetadatiFascicoloRaccoltaProvvisoria_QNAME, RichiestaModifyMetadataFascicoloRaccoltaProvvisoriaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRemoveCollegamentoFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_removeCollegamentoFascicoloSIPATR")
    public JAXBElement<RispostaRemoveCollegamentoFascicoloSIPATRType> createRispostaRemoveCollegamentoFascicoloSIPATR(RispostaRemoveCollegamentoFascicoloSIPATRType value) {
        return new JAXBElement<RispostaRemoveCollegamentoFascicoloSIPATRType>(_RispostaRemoveCollegamentoFascicoloSIPATR_QNAME, RispostaRemoveCollegamentoFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRemoveDocumentoAggiuntivoFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_removeDocumentoAggiuntivoFascicoloDecretoSIPATR")
    public JAXBElement<RispostaRemoveDocumentoAggiuntivoFascicoloSIPATRType> createRispostaRemoveDocumentoAggiuntivoFascicoloDecretoSIPATR(RispostaRemoveDocumentoAggiuntivoFascicoloSIPATRType value) {
        return new JAXBElement<RispostaRemoveDocumentoAggiuntivoFascicoloSIPATRType>(_RispostaRemoveDocumentoAggiuntivoFascicoloDecretoSIPATR_QNAME, RispostaRemoveDocumentoAggiuntivoFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaAddDocumentoFascicoloRaccoltaProvvisoriaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_addDocumentoFascicoloRaccoltaProvvisoria")
    public JAXBElement<RispostaAddDocumentoFascicoloRaccoltaProvvisoriaType> createRispostaAddDocumentoFascicoloRaccoltaProvvisoria(RispostaAddDocumentoFascicoloRaccoltaProvvisoriaType value) {
        return new JAXBElement<RispostaAddDocumentoFascicoloRaccoltaProvvisoriaType>(_RispostaAddDocumentoFascicoloRaccoltaProvvisoria_QNAME, RispostaAddDocumentoFascicoloRaccoltaProvvisoriaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaGetDocumentoFascicoloAllegatoDecretoIGBType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_getDocumentoFascicoloAllegatoDecretoIGB")
    public JAXBElement<RispostaGetDocumentoFascicoloAllegatoDecretoIGBType> createRispostaGetDocumentoFascicoloAllegatoDecretoIGB(RispostaGetDocumentoFascicoloAllegatoDecretoIGBType value) {
        return new JAXBElement<RispostaGetDocumentoFascicoloAllegatoDecretoIGBType>(_RispostaGetDocumentoFascicoloAllegatoDecretoIGB_QNAME, RispostaGetDocumentoFascicoloAllegatoDecretoIGBType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRemoveDocumentoFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_removeDocumentoFascicoloAttoDecreto")
    public JAXBElement<RispostaRemoveDocumentoFascicoloAttoDecretoType> createRispostaRemoveDocumentoFascicoloAttoDecreto(RispostaRemoveDocumentoFascicoloAttoDecretoType value) {
        return new JAXBElement<RispostaRemoveDocumentoFascicoloAttoDecretoType>(_RispostaRemoveDocumentoFascicoloAttoDecreto_QNAME, RispostaRemoveDocumentoFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetFascicoloDecretoSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_getFascicoloDecretoSIPATR")
    public JAXBElement<RichiestaGetFascicoloDecretoSIPATRType> createRichiestaGetFascicoloDecretoSIPATR(RichiestaGetFascicoloDecretoSIPATRType value) {
        return new JAXBElement<RichiestaGetFascicoloDecretoSIPATRType>(_RichiestaGetFascicoloDecretoSIPATR_QNAME, RichiestaGetFascicoloDecretoSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRemoveDocumentoFascicoloAttoDecretoCorteType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_removeDocumentoFascicoloAttoDecretoCorte")
    public JAXBElement<RichiestaRemoveDocumentoFascicoloAttoDecretoCorteType> createRichiestaRemoveDocumentoFascicoloAttoDecretoCorte(RichiestaRemoveDocumentoFascicoloAttoDecretoCorteType value) {
        return new JAXBElement<RichiestaRemoveDocumentoFascicoloAttoDecretoCorteType>(_RichiestaRemoveDocumentoFascicoloAttoDecretoCorte_QNAME, RichiestaRemoveDocumentoFascicoloAttoDecretoCorteType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRemoveDocumentoFascicoloAllegatoDecretoIGBType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_removeDocumentoFascicoloAllegatoDecretoIGB")
    public JAXBElement<RispostaRemoveDocumentoFascicoloAllegatoDecretoIGBType> createRispostaRemoveDocumentoFascicoloAllegatoDecretoIGB(RispostaRemoveDocumentoFascicoloAllegatoDecretoIGBType value) {
        return new JAXBElement<RispostaRemoveDocumentoFascicoloAllegatoDecretoIGBType>(_RispostaRemoveDocumentoFascicoloAllegatoDecretoIGB_QNAME, RispostaRemoveDocumentoFascicoloAllegatoDecretoIGBType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetElencoFascicoliRaccoltaProvvisoriaAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_getElencoFascicoliRaccoltaProvvisoriaAttoDecreto")
    public JAXBElement<RichiestaGetElencoFascicoliRaccoltaProvvisoriaAttoDecretoType> createRichiestaGetElencoFascicoliRaccoltaProvvisoriaAttoDecreto(RichiestaGetElencoFascicoliRaccoltaProvvisoriaAttoDecretoType value) {
        return new JAXBElement<RichiestaGetElencoFascicoliRaccoltaProvvisoriaAttoDecretoType>(_RichiestaGetElencoFascicoliRaccoltaProvvisoriaAttoDecreto_QNAME, RichiestaGetElencoFascicoliRaccoltaProvvisoriaAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaSearchFascicoliRaccoltaProvvisoriaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_searchFascicoliRaccolteProvvisorie")
    public JAXBElement<RispostaSearchFascicoliRaccoltaProvvisoriaType> createRispostaSearchFascicoliRaccolteProvvisorie(RispostaSearchFascicoliRaccoltaProvvisoriaType value) {
        return new JAXBElement<RispostaSearchFascicoliRaccoltaProvvisoriaType>(_RispostaSearchFascicoliRaccolteProvvisorie_QNAME, RispostaSearchFascicoliRaccoltaProvvisoriaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRemoveFascicoloDecretoSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_removeFascicoloDecretoSIPATR")
    public JAXBElement<RichiestaRemoveFascicoloDecretoSIPATRType> createRichiestaRemoveFascicoloDecretoSIPATR(RichiestaRemoveFascicoloDecretoSIPATRType value) {
        return new JAXBElement<RichiestaRemoveFascicoloDecretoSIPATRType>(_RichiestaRemoveFascicoloDecretoSIPATR_QNAME, RichiestaRemoveFascicoloDecretoSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGBType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_modifyMetadatiDocumentoFascicoloAllegatoDecretoIGB")
    public JAXBElement<RispostaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGBType> createRispostaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGB(RispostaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGBType value) {
        return new JAXBElement<RispostaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGBType>(_RispostaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGB_QNAME, RispostaModifyMetadatiDocumentoFascicoloAllegatoDecretoIGBType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaChangeStatoFascicoloRaccoltaProvvisoriaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_changeStatoFascicoloRaccoltaProvvisoria")
    public JAXBElement<RispostaChangeStatoFascicoloRaccoltaProvvisoriaType> createRispostaChangeStatoFascicoloRaccoltaProvvisoria(RispostaChangeStatoFascicoloRaccoltaProvvisoriaType value) {
        return new JAXBElement<RispostaChangeStatoFascicoloRaccoltaProvvisoriaType>(_RispostaChangeStatoFascicoloRaccoltaProvvisoria_QNAME, RispostaChangeStatoFascicoloRaccoltaProvvisoriaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaGetDocumentoFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_getDocumentoFascicoloAttoDecreto")
    public JAXBElement<RispostaGetDocumentoFascicoloAttoDecretoType> createRispostaGetDocumentoFascicoloAttoDecreto(RispostaGetDocumentoFascicoloAttoDecretoType value) {
        return new JAXBElement<RispostaGetDocumentoFascicoloAttoDecretoType>(_RispostaGetDocumentoFascicoloAttoDecreto_QNAME, RispostaGetDocumentoFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaChangeStatoFascicoloDecretoSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_changeStatoFascicoloDecretoSIPATR")
    public JAXBElement<RispostaChangeStatoFascicoloDecretoSIPATRType> createRispostaChangeStatoFascicoloDecretoSIPATR(RispostaChangeStatoFascicoloDecretoSIPATRType value) {
        return new JAXBElement<RispostaChangeStatoFascicoloDecretoSIPATRType>(_RispostaChangeStatoFascicoloDecretoSIPATR_QNAME, RispostaChangeStatoFascicoloDecretoSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRemoveDocumentoAggiuntivoFascicoloDecretoSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_removeDocumentoAggiuntivoFascicoloDecretoSIPATR")
    public JAXBElement<RichiestaRemoveDocumentoAggiuntivoFascicoloDecretoSIPATRType> createRichiestaRemoveDocumentoAggiuntivoFascicoloDecretoSIPATR(RichiestaRemoveDocumentoAggiuntivoFascicoloDecretoSIPATRType value) {
        return new JAXBElement<RichiestaRemoveDocumentoAggiuntivoFascicoloDecretoSIPATRType>(_RichiestaRemoveDocumentoAggiuntivoFascicoloDecretoSIPATR_QNAME, RichiestaRemoveDocumentoAggiuntivoFascicoloDecretoSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaDownloadDocumentoUrlFascicoloRaccoltaProvvisoriaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_downloadDocumentoUrlFascicoloRaccoltaProvvisoria")
    public JAXBElement<RichiestaDownloadDocumentoUrlFascicoloRaccoltaProvvisoriaType> createRichiestaDownloadDocumentoUrlFascicoloRaccoltaProvvisoria(RichiestaDownloadDocumentoUrlFascicoloRaccoltaProvvisoriaType value) {
        return new JAXBElement<RichiestaDownloadDocumentoUrlFascicoloRaccoltaProvvisoriaType>(_RichiestaDownloadDocumentoUrlFascicoloRaccoltaProvvisoria_QNAME, RichiestaDownloadDocumentoUrlFascicoloRaccoltaProvvisoriaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaDownloadDocumentoFascicoloAttoDecretoCorteType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_downloadDocumentoFascicoloAttoDecretoCorte")
    public JAXBElement<RispostaDownloadDocumentoFascicoloAttoDecretoCorteType> createRispostaDownloadDocumentoFascicoloAttoDecretoCorte(RispostaDownloadDocumentoFascicoloAttoDecretoCorteType value) {
        return new JAXBElement<RispostaDownloadDocumentoFascicoloAttoDecretoCorteType>(_RispostaDownloadDocumentoFascicoloAttoDecretoCorte_QNAME, RispostaDownloadDocumentoFascicoloAttoDecretoCorteType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaDownloadDocumentoFascicoloAllegatoDecretoIGBType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_downloadDocumentoFascicoloAllegatoDecretoIGB")
    public JAXBElement<RispostaDownloadDocumentoFascicoloAllegatoDecretoIGBType> createRispostaDownloadDocumentoFascicoloAllegatoDecretoIGB(RispostaDownloadDocumentoFascicoloAllegatoDecretoIGBType value) {
        return new JAXBElement<RispostaDownloadDocumentoFascicoloAllegatoDecretoIGBType>(_RispostaDownloadDocumentoFascicoloAllegatoDecretoIGB_QNAME, RispostaDownloadDocumentoFascicoloAllegatoDecretoIGBType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaGetElencoDocumentiFascicoloAllegatoDecretoIGBType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_getElencoDocumentiFascicoloAllegatoDecretoIGB")
    public JAXBElement<RispostaGetElencoDocumentiFascicoloAllegatoDecretoIGBType> createRispostaGetElencoDocumentiFascicoloAllegatoDecretoIGB(RispostaGetElencoDocumentiFascicoloAllegatoDecretoIGBType value) {
        return new JAXBElement<RispostaGetElencoDocumentiFascicoloAllegatoDecretoIGBType>(_RispostaGetElencoDocumentiFascicoloAllegatoDecretoIGB_QNAME, RispostaGetElencoDocumentiFascicoloAllegatoDecretoIGBType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAddDocumentoFascicoloRaccoltaProvvisoriaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_addDocumentoFascicoloRaccoltaProvvisoria")
    public JAXBElement<RichiestaAddDocumentoFascicoloRaccoltaProvvisoriaType> createRichiestaAddDocumentoFascicoloRaccoltaProvvisoria(RichiestaAddDocumentoFascicoloRaccoltaProvvisoriaType value) {
        return new JAXBElement<RichiestaAddDocumentoFascicoloRaccoltaProvvisoriaType>(_RichiestaAddDocumentoFascicoloRaccoltaProvvisoria_QNAME, RichiestaAddDocumentoFascicoloRaccoltaProvvisoriaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaModifyMetadatiDocumentoFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_modifyMetadatiDocumentoFascicoloAttoDecreto")
    public JAXBElement<RispostaModifyMetadatiDocumentoFascicoloAttoDecretoType> createRispostaModifyMetadatiDocumentoFascicoloAttoDecreto(RispostaModifyMetadatiDocumentoFascicoloAttoDecretoType value) {
        return new JAXBElement<RispostaModifyMetadatiDocumentoFascicoloAttoDecretoType>(_RispostaModifyMetadatiDocumentoFascicoloAttoDecreto_QNAME, RispostaModifyMetadatiDocumentoFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetDocumentoFascicoloRaccoltaProvvisoriaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_getDocumentoFascicoloRaccoltaProvvisoria")
    public JAXBElement<RichiestaGetDocumentoFascicoloRaccoltaProvvisoriaType> createRichiestaGetDocumentoFascicoloRaccoltaProvvisoria(RichiestaGetDocumentoFascicoloRaccoltaProvvisoriaType value) {
        return new JAXBElement<RichiestaGetDocumentoFascicoloRaccoltaProvvisoriaType>(_RichiestaGetDocumentoFascicoloRaccoltaProvvisoria_QNAME, RichiestaGetDocumentoFascicoloRaccoltaProvvisoriaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaGetFascicoloDecretoSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_getFascicoloDecretoSIPATR")
    public JAXBElement<RispostaGetFascicoloDecretoSIPATRType> createRispostaGetFascicoloDecretoSIPATR(RispostaGetFascicoloDecretoSIPATRType value) {
        return new JAXBElement<RispostaGetFascicoloDecretoSIPATRType>(_RispostaGetFascicoloDecretoSIPATR_QNAME, RispostaGetFascicoloDecretoSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAcquisizioneFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_acquisizioneFascicoloAttoDecreto")
    public JAXBElement<RichiestaAcquisizioneFascicoloAttoDecretoType> createRichiestaAcquisizioneFascicoloAttoDecreto(RichiestaAcquisizioneFascicoloAttoDecretoType value) {
        return new JAXBElement<RichiestaAcquisizioneFascicoloAttoDecretoType>(_RichiestaAcquisizioneFascicoloAttoDecreto_QNAME, RichiestaAcquisizioneFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetFascicoloRaccoltaProvvisoriaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_getFascicoloRaccoltaProvvisoria")
    public JAXBElement<RichiestaGetFascicoloRaccoltaProvvisoriaType> createRichiestaGetFascicoloRaccoltaProvvisoria(RichiestaGetFascicoloRaccoltaProvvisoriaType value) {
        return new JAXBElement<RichiestaGetFascicoloRaccoltaProvvisoriaType>(_RichiestaGetFascicoloRaccoltaProvvisoria_QNAME, RichiestaGetFascicoloRaccoltaProvvisoriaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaModifyMetadataFascicoloAttoDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_modifyMetadatiFascicoloAttoDecreto")
    public JAXBElement<RichiestaModifyMetadataFascicoloAttoDecretoType> createRichiestaModifyMetadatiFascicoloAttoDecreto(RichiestaModifyMetadataFascicoloAttoDecretoType value) {
        return new JAXBElement<RichiestaModifyMetadataFascicoloAttoDecretoType>(_RichiestaModifyMetadatiFascicoloAttoDecreto_QNAME, RichiestaModifyMetadataFascicoloAttoDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAddDocumentoAggiuntivoFascicoloDecretoSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_addDocumentoAggiuntivoFascicoloDecretoSIPATR")
    public JAXBElement<RichiestaAddDocumentoAggiuntivoFascicoloDecretoSIPATRType> createRichiestaAddDocumentoAggiuntivoFascicoloDecretoSIPATR(RichiestaAddDocumentoAggiuntivoFascicoloDecretoSIPATRType value) {
        return new JAXBElement<RichiestaAddDocumentoAggiuntivoFascicoloDecretoSIPATRType>(_RichiestaAddDocumentoAggiuntivoFascicoloDecretoSIPATR_QNAME, RichiestaAddDocumentoAggiuntivoFascicoloDecretoSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAddDocumentoFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "richiesta_addDocumentoFascicoloSIPATR")
    public JAXBElement<RichiestaAddDocumentoFascicoloSIPATRType> createRichiestaAddDocumentoFascicoloSIPATR(RichiestaAddDocumentoFascicoloSIPATRType value) {
        return new JAXBElement<RichiestaAddDocumentoFascicoloSIPATRType>(_RichiestaAddDocumentoFascicoloSIPATR_QNAME, RichiestaAddDocumentoFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaAddCollegamentoFascicoloSIPATRType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_addCollegamentoFascicoloSIPATR")
    public JAXBElement<RispostaAddCollegamentoFascicoloSIPATRType> createRispostaAddCollegamentoFascicoloSIPATR(RispostaAddCollegamentoFascicoloSIPATRType value) {
        return new JAXBElement<RispostaAddCollegamentoFascicoloSIPATRType>(_RispostaAddCollegamentoFascicoloSIPATR_QNAME, RispostaAddCollegamentoFascicoloSIPATRType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", name = "risposta_modifyMetadatiDocumentoFascicoloRaccoltaProvvisoria")
    public JAXBElement<RispostaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType> createRispostaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoria(RispostaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType value) {
        return new JAXBElement<RispostaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType>(_RispostaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoria_QNAME, RispostaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TicketArchiviazione", scope = DocumentoFascicoloDecretoSIPATRType.class)
    public JAXBElement<String> createDocumentoFascicoloDecretoSIPATRTypeTicketArchiviazione(String value) {
        return new JAXBElement<String>(_DocumentoFascicoloDecretoSIPATRTypeTicketArchiviazione_QNAME, String.class, DocumentoFascicoloDecretoSIPATRType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TicketArchiviazione", scope = DocumentoFascicoloAttoDecretoType.class)
    public JAXBElement<String> createDocumentoFascicoloAttoDecretoTypeTicketArchiviazione(String value) {
        return new JAXBElement<String>(_DocumentoFascicoloDecretoSIPATRTypeTicketArchiviazione_QNAME, String.class, DocumentoFascicoloAttoDecretoType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TicketArchiviazione", scope = DocumentoFascicoloAllegatoDecretoIGBType.class)
    public JAXBElement<String> createDocumentoFascicoloAllegatoDecretoIGBTypeTicketArchiviazione(String value) {
        return new JAXBElement<String>(_DocumentoFascicoloDecretoSIPATRTypeTicketArchiviazione_QNAME, String.class, DocumentoFascicoloAllegatoDecretoIGBType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TicketArchiviazione", scope = DocumentoFascicoloRaccoltaProvvisoriaType.class)
    public JAXBElement<String> createDocumentoFascicoloRaccoltaProvvisoriaTypeTicketArchiviazione(String value) {
        return new JAXBElement<String>(_DocumentoFascicoloDecretoSIPATRTypeTicketArchiviazione_QNAME, String.class, DocumentoFascicoloRaccoltaProvvisoriaType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TicketArchiviazione", scope = DocumentoFascicoloSIPATRType.class)
    public JAXBElement<String> createDocumentoFascicoloSIPATRTypeTicketArchiviazione(String value) {
        return new JAXBElement<String>(_DocumentoFascicoloDecretoSIPATRTypeTicketArchiviazione_QNAME, String.class, DocumentoFascicoloSIPATRType.class, value);
    }

}
