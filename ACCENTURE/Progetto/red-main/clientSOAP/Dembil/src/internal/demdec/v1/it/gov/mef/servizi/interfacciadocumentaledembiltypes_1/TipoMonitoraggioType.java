
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoMonitoraggio_type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoMonitoraggio_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ALL"/>
 *     &lt;enumeration value="IN_LAVORAZIONE"/>
 *     &lt;enumeration value="IN_ERRORE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoMonitoraggio_type")
@XmlEnum
public enum TipoMonitoraggioType {

    ALL,
    IN_LAVORAZIONE,
    IN_ERRORE;

    public String value() {
        return name();
    }

    public static TipoMonitoraggioType fromValue(String v) {
        return valueOf(v);
    }

}
