
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Richiede l'aggiornamento del content del documento AttoDecreto al fascicolo
 * 
 * <p>Java class for richiesta_updateMetadatiDocumentoAggiuntivoFascicoloAttoDecreto_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_updateMetadatiDocumentoAggiuntivoFascicoloAttoDecreto_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloAttoDecreto" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="IdDocumentoAttoDecreto" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="Protocollo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}protocollo_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DataInvioCorte" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="FLUSSO_AD" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}flussoAD_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_updateMetadatiDocumentoAggiuntivoFascicoloAttoDecreto_type", propOrder = {
    "idFascicoloAttoDecreto",
    "idDocumentoAttoDecreto",
    "protocollo",
    "dataInvioCorte",
    "flussoad"
})
public class RichiestaUpdateMetadatiDocumentoAggiuntivoFascicoloAttoDecretoType {

    @XmlElement(name = "IdFascicoloAttoDecreto", required = true)
    protected String idFascicoloAttoDecreto;
    @XmlElement(name = "IdDocumentoAttoDecreto", required = true, nillable = true)
    protected String idDocumentoAttoDecreto;
    @XmlElement(name = "Protocollo")
    protected List<ProtocolloType> protocollo;
    @XmlElement(name = "DataInvioCorte", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataInvioCorte;
    @XmlElement(name = "FLUSSO_AD", required = true)
    protected FlussoADType flussoad;

    /**
     * Gets the value of the idFascicoloAttoDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloAttoDecreto() {
        return idFascicoloAttoDecreto;
    }

    /**
     * Sets the value of the idFascicoloAttoDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloAttoDecreto(String value) {
        this.idFascicoloAttoDecreto = value;
    }

    /**
     * Gets the value of the idDocumentoAttoDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoAttoDecreto() {
        return idDocumentoAttoDecreto;
    }

    /**
     * Sets the value of the idDocumentoAttoDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoAttoDecreto(String value) {
        this.idDocumentoAttoDecreto = value;
    }

    /**
     * Gets the value of the protocollo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the protocollo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProtocollo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtocolloType }
     * 
     * 
     */
    public List<ProtocolloType> getProtocollo() {
        if (protocollo == null) {
            protocollo = new ArrayList<ProtocolloType>();
        }
        return this.protocollo;
    }

    /**
     * Gets the value of the dataInvioCorte property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataInvioCorte() {
        return dataInvioCorte;
    }

    /**
     * Sets the value of the dataInvioCorte property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataInvioCorte(XMLGregorianCalendar value) {
        this.dataInvioCorte = value;
    }

    /**
     * Gets the value of the flussoad property.
     * 
     * @return
     *     possible object is
     *     {@link FlussoADType }
     *     
     */
    public FlussoADType getFLUSSOAD() {
        return flussoad;
    }

    /**
     * Sets the value of the flussoad property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlussoADType }
     *     
     */
    public void setFLUSSOAD(FlussoADType value) {
        this.flussoad = value;
    }

}
