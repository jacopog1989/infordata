
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiesta metadati del Fascicolo Decreto SIPATR
 * 
 * <p>Java class for richiesta_getFascicoloDecretoSIPATR_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_getFascicoloDecretoSIPATR_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloDecretoSIPATR" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="TipoEstrazioneFascicolo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoEstrazione_type"/>
 *         &lt;element name="FascicoliSIPATRCriteria" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}criteriaFascicoliSIPATR_type" minOccurs="0"/>
 *         &lt;element name="TipoElenco" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoElenco_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_getFascicoloDecretoSIPATR_type", propOrder = {
    "idFascicoloDecretoSIPATR",
    "tipoEstrazioneFascicolo",
    "fascicoliSIPATRCriteria",
    "tipoElenco"
})
public class RichiestaGetFascicoloDecretoSIPATRType {

    @XmlElement(name = "IdFascicoloDecretoSIPATR", required = true)
    protected String idFascicoloDecretoSIPATR;
    @XmlElement(name = "TipoEstrazioneFascicolo", required = true, defaultValue = "DATA")
    protected TipoEstrazioneType tipoEstrazioneFascicolo;
    @XmlElement(name = "FascicoliSIPATRCriteria")
    protected CriteriaFascicoliSIPATRType fascicoliSIPATRCriteria;
    @XmlElement(name = "TipoElenco", required = true, defaultValue = "SOURCE")
    protected TipoElencoType tipoElenco;

    /**
     * Gets the value of the idFascicoloDecretoSIPATR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloDecretoSIPATR() {
        return idFascicoloDecretoSIPATR;
    }

    /**
     * Sets the value of the idFascicoloDecretoSIPATR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloDecretoSIPATR(String value) {
        this.idFascicoloDecretoSIPATR = value;
    }

    /**
     * Gets the value of the tipoEstrazioneFascicolo property.
     * 
     * @return
     *     possible object is
     *     {@link TipoEstrazioneType }
     *     
     */
    public TipoEstrazioneType getTipoEstrazioneFascicolo() {
        return tipoEstrazioneFascicolo;
    }

    /**
     * Sets the value of the tipoEstrazioneFascicolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEstrazioneType }
     *     
     */
    public void setTipoEstrazioneFascicolo(TipoEstrazioneType value) {
        this.tipoEstrazioneFascicolo = value;
    }

    /**
     * Gets the value of the fascicoliSIPATRCriteria property.
     * 
     * @return
     *     possible object is
     *     {@link CriteriaFascicoliSIPATRType }
     *     
     */
    public CriteriaFascicoliSIPATRType getFascicoliSIPATRCriteria() {
        return fascicoliSIPATRCriteria;
    }

    /**
     * Sets the value of the fascicoliSIPATRCriteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link CriteriaFascicoliSIPATRType }
     *     
     */
    public void setFascicoliSIPATRCriteria(CriteriaFascicoliSIPATRType value) {
        this.fascicoliSIPATRCriteria = value;
    }

    /**
     * Gets the value of the tipoElenco property.
     * 
     * @return
     *     possible object is
     *     {@link TipoElencoType }
     *     
     */
    public TipoElencoType getTipoElenco() {
        return tipoElenco;
    }

    /**
     * Sets the value of the tipoElenco property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoElencoType }
     *     
     */
    public void setTipoElenco(TipoElencoType value) {
        this.tipoElenco = value;
    }

}
