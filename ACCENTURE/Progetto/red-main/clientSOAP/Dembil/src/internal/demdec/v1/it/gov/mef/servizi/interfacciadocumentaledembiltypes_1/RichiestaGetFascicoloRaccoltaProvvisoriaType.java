
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiesta metadati del Fascicolo Raccolta Provvisoria
 * 
 * <p>Java class for richiesta_getFascicoloRaccoltaProvvisoria_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_getFascicoloRaccoltaProvvisoria_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloRaccoltaProvvisoria" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="TipoEstrazioneFascicolo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoEstrazione_type"/>
 *         &lt;element name="DocumentCriteria" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}criteriaDocumentiRaccoltaProvvisoria_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_getFascicoloRaccoltaProvvisoria_type", propOrder = {
    "idFascicoloRaccoltaProvvisoria",
    "tipoEstrazioneFascicolo",
    "documentCriteria"
})
public class RichiestaGetFascicoloRaccoltaProvvisoriaType {

    @XmlElement(name = "IdFascicoloRaccoltaProvvisoria", required = true)
    protected String idFascicoloRaccoltaProvvisoria;
    @XmlElement(name = "TipoEstrazioneFascicolo", required = true, defaultValue = "DATA")
    protected TipoEstrazioneType tipoEstrazioneFascicolo;
    @XmlElement(name = "DocumentCriteria")
    protected CriteriaDocumentiRaccoltaProvvisoriaType documentCriteria;

    /**
     * Gets the value of the idFascicoloRaccoltaProvvisoria property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloRaccoltaProvvisoria() {
        return idFascicoloRaccoltaProvvisoria;
    }

    /**
     * Sets the value of the idFascicoloRaccoltaProvvisoria property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloRaccoltaProvvisoria(String value) {
        this.idFascicoloRaccoltaProvvisoria = value;
    }

    /**
     * Gets the value of the tipoEstrazioneFascicolo property.
     * 
     * @return
     *     possible object is
     *     {@link TipoEstrazioneType }
     *     
     */
    public TipoEstrazioneType getTipoEstrazioneFascicolo() {
        return tipoEstrazioneFascicolo;
    }

    /**
     * Sets the value of the tipoEstrazioneFascicolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEstrazioneType }
     *     
     */
    public void setTipoEstrazioneFascicolo(TipoEstrazioneType value) {
        this.tipoEstrazioneFascicolo = value;
    }

    /**
     * Gets the value of the documentCriteria property.
     * 
     * @return
     *     possible object is
     *     {@link CriteriaDocumentiRaccoltaProvvisoriaType }
     *     
     */
    public CriteriaDocumentiRaccoltaProvvisoriaType getDocumentCriteria() {
        return documentCriteria;
    }

    /**
     * Sets the value of the documentCriteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link CriteriaDocumentiRaccoltaProvvisoriaType }
     *     
     */
    public void setDocumentCriteria(CriteriaDocumentiRaccoltaProvvisoriaType value) {
        this.documentCriteria = value;
    }

}
