
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoFlusso_type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoFlusso_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MANUALE"/>
 *     &lt;enumeration value="AUTOMATICO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoFlusso_type")
@XmlEnum
public enum TipoFlussoType {

    MANUALE,
    AUTOMATICO;

    public String value() {
        return name();
    }

    public static TipoFlussoType fromValue(String v) {
        return valueOf(v);
    }

}
