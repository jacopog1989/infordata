
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiede l'aggiornamento dei metadati di un documento del Fascicolo SIPATR
 * 
 * <p>Java class for richiesta_modifyMetadataDocumentoFascicoloSIPATR_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_modifyMetadataDocumentoFascicoloSIPATR_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloSIPATR" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="IdDocumento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="DocumentoMetadata" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}documentoMetadataSIPATR_type" minOccurs="0"/>
 *         &lt;element name="DaInviare" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Condivisibile" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Attivo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_modifyMetadataDocumentoFascicoloSIPATR_type", propOrder = {
    "idFascicoloSIPATR",
    "idDocumento",
    "documentoMetadata",
    "daInviare",
    "condivisibile",
    "attivo"
})
public class RichiestaModifyMetadataDocumentoFascicoloSIPATRType {

    @XmlElement(name = "IdFascicoloSIPATR", required = true)
    protected String idFascicoloSIPATR;
    @XmlElement(name = "IdDocumento", required = true)
    protected String idDocumento;
    @XmlElement(name = "DocumentoMetadata")
    protected DocumentoMetadataSIPATRType documentoMetadata;
    @XmlElement(name = "DaInviare", defaultValue = "true")
    protected boolean daInviare;
    @XmlElement(name = "Condivisibile", defaultValue = "true")
    protected boolean condivisibile;
    @XmlElement(name = "Attivo", defaultValue = "true")
    protected boolean attivo;

    /**
     * Gets the value of the idFascicoloSIPATR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloSIPATR() {
        return idFascicoloSIPATR;
    }

    /**
     * Sets the value of the idFascicoloSIPATR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloSIPATR(String value) {
        this.idFascicoloSIPATR = value;
    }

    /**
     * Gets the value of the idDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumento() {
        return idDocumento;
    }

    /**
     * Sets the value of the idDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumento(String value) {
        this.idDocumento = value;
    }

    /**
     * Gets the value of the documentoMetadata property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoMetadataSIPATRType }
     *     
     */
    public DocumentoMetadataSIPATRType getDocumentoMetadata() {
        return documentoMetadata;
    }

    /**
     * Sets the value of the documentoMetadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoMetadataSIPATRType }
     *     
     */
    public void setDocumentoMetadata(DocumentoMetadataSIPATRType value) {
        this.documentoMetadata = value;
    }

    /**
     * Gets the value of the daInviare property.
     * 
     */
    public boolean isDaInviare() {
        return daInviare;
    }

    /**
     * Sets the value of the daInviare property.
     * 
     */
    public void setDaInviare(boolean value) {
        this.daInviare = value;
    }

    /**
     * Gets the value of the condivisibile property.
     * 
     */
    public boolean isCondivisibile() {
        return condivisibile;
    }

    /**
     * Sets the value of the condivisibile property.
     * 
     */
    public void setCondivisibile(boolean value) {
        this.condivisibile = value;
    }

    /**
     * Gets the value of the attivo property.
     * 
     */
    public boolean isAttivo() {
        return attivo;
    }

    /**
     * Sets the value of the attivo property.
     * 
     */
    public void setAttivo(boolean value) {
        this.attivo = value;
    }

}
