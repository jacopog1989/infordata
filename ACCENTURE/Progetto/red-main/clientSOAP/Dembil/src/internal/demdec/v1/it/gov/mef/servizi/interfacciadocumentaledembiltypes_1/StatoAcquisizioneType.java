
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for statoAcquisizione_type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="statoAcquisizione_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Da Acquisire"/>
 *     &lt;enumeration value="Acquisito"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "statoAcquisizione_type")
@XmlEnum
public enum StatoAcquisizioneType {

    @XmlEnumValue("Da Acquisire")
    DA_ACQUISIRE("Da Acquisire"),
    @XmlEnumValue("Acquisito")
    ACQUISITO("Acquisito");
    private final String value;

    StatoAcquisizioneType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static StatoAcquisizioneType fromValue(String v) {
        for (StatoAcquisizioneType c: StatoAcquisizioneType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
