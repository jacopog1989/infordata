
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for statoFascicoloDocumentale_type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="statoFascicoloDocumentale_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="INSERITO"/>
 *     &lt;enumeration value="APERTO"/>
 *     &lt;enumeration value="CHIUSO"/>
 *     &lt;enumeration value="IN_ELABORAZIONE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "statoFascicoloDocumentale_type")
@XmlEnum
public enum StatoFascicoloDocumentaleType {

    INSERITO,
    APERTO,
    CHIUSO,
    IN_ELABORAZIONE;

    public String value() {
        return name();
    }

    public static StatoFascicoloDocumentaleType fromValue(String v) {
        return valueOf(v);
    }

}
