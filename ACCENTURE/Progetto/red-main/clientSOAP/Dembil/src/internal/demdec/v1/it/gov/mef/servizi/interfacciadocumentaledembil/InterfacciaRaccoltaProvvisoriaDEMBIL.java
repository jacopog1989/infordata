
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import internal.demdec.v1.it.gov.mef.servizi.common.headeraccessoapplicativo.AccessoApplicativo;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaAddDocumentoFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaChangeStatoFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaCreateFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaDownloadDocumentoFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaGetFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaModifyMetadataFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaRemoveDocumentoFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaAddDocumentoFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaChangeStatoFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaCreateFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaDownloadDocumentoFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetFascicoloRaccoltaProvvisoria;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaModifyMetadataFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaRemoveDocumentoFascicoloRaccoltaProvvisoriaType;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebService(name = "InterfacciaRaccoltaProvvisoriaDEMBIL", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBIL")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.ObjectFactory.class,
    internal.demdec.v1.it.gov.mef.servizi.common.headeraccessoapplicativo.ObjectFactory.class,
    internal.demdec.v1.it.gov.mef.servizi.common.headerfault.ObjectFactory.class,
    org.xmlsoap.schemas.soap.envelope.ObjectFactory.class
})
public interface InterfacciaRaccoltaProvvisoriaDEMBIL {


    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaCreateFascicoloRaccoltaProvvisoriaType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:createFascicoloRaccoltaProvvisoria")
    @WebResult(name = "risposta_createFascicoloRaccoltaProvvisoria", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaCreateFascicoloRaccoltaProvvisoriaType createFascicoloRaccoltaProvvisoria(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_createFascicoloRaccoltaProvvisoria", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaCreateFascicoloRaccoltaProvvisoriaType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetFascicoloRaccoltaProvvisoria
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:getFascicoloRaccoltaProvvisoria")
    @WebResult(name = "risposta_getFascicoloRaccoltaProvvisoria", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaGetFascicoloRaccoltaProvvisoria getFascicoloRaccoltaProvvisoria(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_getFascicoloRaccoltaProvvisoria", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaGetFascicoloRaccoltaProvvisoriaType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaChangeStatoFascicoloRaccoltaProvvisoriaType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:changeStatoFascicoloRaccoltaProvvisoria")
    @WebResult(name = "risposta_changeStatoFascicoloRaccoltaProvvisoria", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaChangeStatoFascicoloRaccoltaProvvisoriaType changeStatoFascicoloRaccoltaProvvisoria(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_changeStatoFascicoloRaccoltaProvvisoria", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaChangeStatoFascicoloRaccoltaProvvisoriaType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaModifyMetadataFascicoloRaccoltaProvvisoriaType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:modifyMetadatiFascicoloRaccoltaProvvisoria")
    @WebResult(name = "risposta_modifyMetadatiFascicoloRaccoltaProvvisoria", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaModifyMetadataFascicoloRaccoltaProvvisoriaType modifyMetadatiFascicoloRaccoltaProvvisoria(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_modifyMetadatiFascicoloRaccoltaProvvisoria", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaModifyMetadataFascicoloRaccoltaProvvisoriaType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaAddDocumentoFascicoloRaccoltaProvvisoriaType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:addDocumentoFascicoloRaccoltaProvvisoria")
    @WebResult(name = "risposta_addDocumentoFascicoloRaccoltaProvvisoria", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaAddDocumentoFascicoloRaccoltaProvvisoriaType addDocumentoFascicoloRaccoltaProvvisoria(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_addDocumentoFascicoloRaccoltaProvvisoria", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaAddDocumentoFascicoloRaccoltaProvvisoriaType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaRemoveDocumentoFascicoloRaccoltaProvvisoriaType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:removeDocumentoFascicoloRaccoltaProvvisoria")
    @WebResult(name = "risposta_removeDocumentoFascicoloRaccoltaProvvisoria", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaRemoveDocumentoFascicoloRaccoltaProvvisoriaType removeDocumentoFascicoloRaccoltaProvvisoria(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_removeDocumentoFascicoloRaccoltaProvvisoria", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaRemoveDocumentoFascicoloRaccoltaProvvisoriaType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:modifyMetadatiDocumentoFascicoloRaccoltaProvvisoria")
    @WebResult(name = "risposta_modifyMetadatiDocumentoFascicoloRaccoltaProvvisoria", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType modifyMetadatiDocumentoFascicoloRaccoltaProvvisoria(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_modifyMetadatiDocumentoFascicoloRaccoltaProvvisoria", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaDownloadDocumentoFascicoloRaccoltaProvvisoriaType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:downloadDocumentoFascicoloRaccoltaProvvisoria")
    @WebResult(name = "risposta_downloadDocumentoFascicoloRaccoltaProvvisoria", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaDownloadDocumentoFascicoloRaccoltaProvvisoriaType downloadDocumentoFascicoloRaccoltaProvvisoria(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_downloadDocumentoFascicoloRaccoltaProvvisoria", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaDownloadDocumentoFascicoloRaccoltaProvvisoriaType parameters)
        throws GenericFault, SecurityFault
    ;

}
