
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Elenco Fascicoli Raccolta Provvisoria
 * 
 * <p>Java class for risposta_searchFascicoliRaccoltaProvvisoria_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="risposta_searchFascicoliRaccoltaProvvisoria_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}BaseServiceResponseType">
 *       &lt;sequence>
 *         &lt;element name="Fascicoli" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}fascicoloRaccoltaProvvisoria_type" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_searchFascicoliRaccoltaProvvisoria_type", propOrder = {
    "fascicoli"
})
public class RispostaSearchFascicoliRaccoltaProvvisoriaType
    extends BaseServiceResponseType
{

    @XmlElement(name = "Fascicoli")
    protected List<FascicoloRaccoltaProvvisoriaType> fascicoli;

    /**
     * Gets the value of the fascicoli property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fascicoli property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFascicoli().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FascicoloRaccoltaProvvisoriaType }
     * 
     * 
     */
    public List<FascicoloRaccoltaProvvisoriaType> getFascicoli() {
        if (fascicoli == null) {
            fascicoli = new ArrayList<FascicoloRaccoltaProvvisoriaType>();
        }
        return this.fascicoli;
    }

}
