
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Criteri di filtro per l'estrazione dei fascicoli Raccolta provvisoria
 * 
 * <p>Java class for criteriaFascicoliRaccolteProvvisorie_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="criteriaFascicoliRaccolteProvvisorie_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StatoFascicoloRaccoltaProvvisoria" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}statoFascicoloDocumentale_type" minOccurs="0"/>
 *         &lt;element name="Ragioneria" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type" minOccurs="0"/>
 *         &lt;element name="Amministrazione" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type" minOccurs="0"/>
 *         &lt;element name="UfficioCreatoreRED" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type" minOccurs="0"/>
 *         &lt;element name="TipoFlusso" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoFlusso_type" minOccurs="0"/>
 *         &lt;element name="Protocollo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}protocollo_type" minOccurs="0"/>
 *         &lt;element name="UtenteCreatore" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type" minOccurs="0"/>
 *         &lt;element name="DataCreazioneFascicolo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}dataRange_type" minOccurs="0"/>
 *         &lt;element name="DataAggiornamento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}dataRange_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "criteriaFascicoliRaccolteProvvisorie_type", propOrder = {
    "descrizione",
    "statoFascicoloRaccoltaProvvisoria",
    "ragioneria",
    "amministrazione",
    "ufficioCreatoreRED",
    "tipoFlusso",
    "protocollo",
    "utenteCreatore",
    "dataCreazioneFascicolo",
    "dataAggiornamento"
})
public class CriteriaFascicoliRaccolteProvvisorieType {

    @XmlElement(name = "Descrizione")
    protected String descrizione;
    @XmlElement(name = "StatoFascicoloRaccoltaProvvisoria")
    protected StatoFascicoloDocumentaleType statoFascicoloRaccoltaProvvisoria;
    @XmlElement(name = "Ragioneria")
    protected CodiceDescrizioneType ragioneria;
    @XmlElement(name = "Amministrazione")
    protected CodiceDescrizioneType amministrazione;
    @XmlElement(name = "UfficioCreatoreRED")
    protected CodiceDescrizioneType ufficioCreatoreRED;
    @XmlElement(name = "TipoFlusso")
    protected TipoFlussoType tipoFlusso;
    @XmlElement(name = "Protocollo")
    protected ProtocolloType protocollo;
    @XmlElement(name = "UtenteCreatore")
    protected CodiceDescrizioneType utenteCreatore;
    @XmlElement(name = "DataCreazioneFascicolo")
    protected DataRangeType dataCreazioneFascicolo;
    @XmlElement(name = "DataAggiornamento")
    protected DataRangeType dataAggiornamento;

    /**
     * Gets the value of the descrizione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Sets the value of the descrizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Gets the value of the statoFascicoloRaccoltaProvvisoria property.
     * 
     * @return
     *     possible object is
     *     {@link StatoFascicoloDocumentaleType }
     *     
     */
    public StatoFascicoloDocumentaleType getStatoFascicoloRaccoltaProvvisoria() {
        return statoFascicoloRaccoltaProvvisoria;
    }

    /**
     * Sets the value of the statoFascicoloRaccoltaProvvisoria property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatoFascicoloDocumentaleType }
     *     
     */
    public void setStatoFascicoloRaccoltaProvvisoria(StatoFascicoloDocumentaleType value) {
        this.statoFascicoloRaccoltaProvvisoria = value;
    }

    /**
     * Gets the value of the ragioneria property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getRagioneria() {
        return ragioneria;
    }

    /**
     * Sets the value of the ragioneria property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setRagioneria(CodiceDescrizioneType value) {
        this.ragioneria = value;
    }

    /**
     * Gets the value of the amministrazione property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getAmministrazione() {
        return amministrazione;
    }

    /**
     * Sets the value of the amministrazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setAmministrazione(CodiceDescrizioneType value) {
        this.amministrazione = value;
    }

    /**
     * Gets the value of the ufficioCreatoreRED property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getUfficioCreatoreRED() {
        return ufficioCreatoreRED;
    }

    /**
     * Sets the value of the ufficioCreatoreRED property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setUfficioCreatoreRED(CodiceDescrizioneType value) {
        this.ufficioCreatoreRED = value;
    }

    /**
     * Gets the value of the tipoFlusso property.
     * 
     * @return
     *     possible object is
     *     {@link TipoFlussoType }
     *     
     */
    public TipoFlussoType getTipoFlusso() {
        return tipoFlusso;
    }

    /**
     * Sets the value of the tipoFlusso property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoFlussoType }
     *     
     */
    public void setTipoFlusso(TipoFlussoType value) {
        this.tipoFlusso = value;
    }

    /**
     * Gets the value of the protocollo property.
     * 
     * @return
     *     possible object is
     *     {@link ProtocolloType }
     *     
     */
    public ProtocolloType getProtocollo() {
        return protocollo;
    }

    /**
     * Sets the value of the protocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtocolloType }
     *     
     */
    public void setProtocollo(ProtocolloType value) {
        this.protocollo = value;
    }

    /**
     * Gets the value of the utenteCreatore property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getUtenteCreatore() {
        return utenteCreatore;
    }

    /**
     * Sets the value of the utenteCreatore property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setUtenteCreatore(CodiceDescrizioneType value) {
        this.utenteCreatore = value;
    }

    /**
     * Gets the value of the dataCreazioneFascicolo property.
     * 
     * @return
     *     possible object is
     *     {@link DataRangeType }
     *     
     */
    public DataRangeType getDataCreazioneFascicolo() {
        return dataCreazioneFascicolo;
    }

    /**
     * Sets the value of the dataCreazioneFascicolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataRangeType }
     *     
     */
    public void setDataCreazioneFascicolo(DataRangeType value) {
        this.dataCreazioneFascicolo = value;
    }

    /**
     * Gets the value of the dataAggiornamento property.
     * 
     * @return
     *     possible object is
     *     {@link DataRangeType }
     *     
     */
    public DataRangeType getDataAggiornamento() {
        return dataAggiornamento;
    }

    /**
     * Sets the value of the dataAggiornamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataRangeType }
     *     
     */
    public void setDataAggiornamento(DataRangeType value) {
        this.dataAggiornamento = value;
    }

}
