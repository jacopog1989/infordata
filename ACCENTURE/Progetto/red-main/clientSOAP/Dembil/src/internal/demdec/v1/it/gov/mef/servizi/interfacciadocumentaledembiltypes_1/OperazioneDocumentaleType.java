
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Elenco di Tipologia di Documenti Inseribili nel Fasciolo
 * 
 * <p>Java class for operazioneDocumentale_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="operazioneDocumentale_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoOperazione" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoOperazione_type"/>
 *         &lt;element name="EsitoOperazione" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}esitoOperazione_type"/>
 *         &lt;element name="DocumentoOperazione" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}documentoFile_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "operazioneDocumentale_type", propOrder = {
    "tipoOperazione",
    "esitoOperazione",
    "documentoOperazione"
})
@XmlSeeAlso({
    internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RisultatoOperazioneDocumentoType.Operazione.class
})
public class OperazioneDocumentaleType {

    @XmlElement(name = "TipoOperazione", required = true)
    protected TipoOperazioneType tipoOperazione;
    @XmlElement(name = "EsitoOperazione", required = true)
    protected EsitoOperazioneType esitoOperazione;
    @XmlElement(name = "DocumentoOperazione")
    protected DocumentoFileType documentoOperazione;

    /**
     * Gets the value of the tipoOperazione property.
     * 
     * @return
     *     possible object is
     *     {@link TipoOperazioneType }
     *     
     */
    public TipoOperazioneType getTipoOperazione() {
        return tipoOperazione;
    }

    /**
     * Sets the value of the tipoOperazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoOperazioneType }
     *     
     */
    public void setTipoOperazione(TipoOperazioneType value) {
        this.tipoOperazione = value;
    }

    /**
     * Gets the value of the esitoOperazione property.
     * 
     * @return
     *     possible object is
     *     {@link EsitoOperazioneType }
     *     
     */
    public EsitoOperazioneType getEsitoOperazione() {
        return esitoOperazione;
    }

    /**
     * Sets the value of the esitoOperazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link EsitoOperazioneType }
     *     
     */
    public void setEsitoOperazione(EsitoOperazioneType value) {
        this.esitoOperazione = value;
    }

    /**
     * Gets the value of the documentoOperazione property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoFileType }
     *     
     */
    public DocumentoFileType getDocumentoOperazione() {
        return documentoOperazione;
    }

    /**
     * Sets the value of the documentoOperazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoFileType }
     *     
     */
    public void setDocumentoOperazione(DocumentoFileType value) {
        this.documentoOperazione = value;
    }

}
