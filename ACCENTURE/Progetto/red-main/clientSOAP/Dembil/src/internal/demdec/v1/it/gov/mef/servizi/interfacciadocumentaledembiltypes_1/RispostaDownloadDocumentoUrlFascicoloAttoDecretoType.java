
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Url AXWAY per il Download Documento
 * 
 * <p>Java class for risposta_downloadDocumentoUrlFascicoloAttoDecreto_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="risposta_downloadDocumentoUrlFascicoloAttoDecreto_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}BaseServiceResponseType">
 *       &lt;sequence>
 *         &lt;element name="DocumentoUrl" type="{http://www.w3.org/2001/XMLSchema}anyURI" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_downloadDocumentoUrlFascicoloAttoDecreto_type", propOrder = {
    "documentoUrl"
})
@XmlSeeAlso({
    RispostaDownloadDocumentoUrlFascicoloAttoDecreto.class
})
public class RispostaDownloadDocumentoUrlFascicoloAttoDecretoType
    extends BaseServiceResponseType
{

    @XmlElement(name = "DocumentoUrl")
    @XmlSchemaType(name = "anyURI")
    protected String documentoUrl;

    /**
     * Gets the value of the documentoUrl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentoUrl() {
        return documentoUrl;
    }

    /**
     * Sets the value of the documentoUrl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentoUrl(String value) {
        this.documentoUrl = value;
    }

}
