
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiede l'aggiornamento dei metadati del Fascicolo Decreto SIPATR
 * 
 * <p>Java class for richiesta_modifyMetadataFascicoloDecretoSIPATR_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_modifyMetadataFascicoloDecretoSIPATR_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloDecretoSIPATR" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="FascicoloMetadata" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}fascicoloMetadataDecretoSIPATR_type"/>
 *         &lt;element name="DaInviare" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Condivisibile" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Attivo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_modifyMetadataFascicoloDecretoSIPATR_type", propOrder = {
    "idFascicoloDecretoSIPATR",
    "fascicoloMetadata",
    "daInviare",
    "condivisibile",
    "attivo"
})
public class RichiestaModifyMetadataFascicoloDecretoSIPATRType {

    @XmlElement(name = "IdFascicoloDecretoSIPATR", required = true)
    protected String idFascicoloDecretoSIPATR;
    @XmlElement(name = "FascicoloMetadata", required = true)
    protected FascicoloMetadataDecretoSIPATRType fascicoloMetadata;
    @XmlElement(name = "DaInviare", defaultValue = "true")
    protected boolean daInviare;
    @XmlElement(name = "Condivisibile", defaultValue = "true")
    protected boolean condivisibile;
    @XmlElement(name = "Attivo", defaultValue = "true")
    protected boolean attivo;

    /**
     * Gets the value of the idFascicoloDecretoSIPATR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloDecretoSIPATR() {
        return idFascicoloDecretoSIPATR;
    }

    /**
     * Sets the value of the idFascicoloDecretoSIPATR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloDecretoSIPATR(String value) {
        this.idFascicoloDecretoSIPATR = value;
    }

    /**
     * Gets the value of the fascicoloMetadata property.
     * 
     * @return
     *     possible object is
     *     {@link FascicoloMetadataDecretoSIPATRType }
     *     
     */
    public FascicoloMetadataDecretoSIPATRType getFascicoloMetadata() {
        return fascicoloMetadata;
    }

    /**
     * Sets the value of the fascicoloMetadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link FascicoloMetadataDecretoSIPATRType }
     *     
     */
    public void setFascicoloMetadata(FascicoloMetadataDecretoSIPATRType value) {
        this.fascicoloMetadata = value;
    }

    /**
     * Gets the value of the daInviare property.
     * 
     */
    public boolean isDaInviare() {
        return daInviare;
    }

    /**
     * Sets the value of the daInviare property.
     * 
     */
    public void setDaInviare(boolean value) {
        this.daInviare = value;
    }

    /**
     * Gets the value of the condivisibile property.
     * 
     */
    public boolean isCondivisibile() {
        return condivisibile;
    }

    /**
     * Sets the value of the condivisibile property.
     * 
     */
    public void setCondivisibile(boolean value) {
        this.condivisibile = value;
    }

    /**
     * Gets the value of the attivo property.
     * 
     */
    public boolean isAttivo() {
        return attivo;
    }

    /**
     * Sets the value of the attivo property.
     * 
     */
    public void setAttivo(boolean value) {
        this.attivo = value;
    }

}
