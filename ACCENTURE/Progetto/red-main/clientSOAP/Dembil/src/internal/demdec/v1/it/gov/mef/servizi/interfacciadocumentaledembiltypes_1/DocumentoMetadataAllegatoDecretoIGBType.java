
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Metadati dei documenti del Allegato decreto IGB
 * 
 * <p>Java class for documentoMetadataAllegatoDecretoIGB_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="documentoMetadataAllegatoDecretoIGB_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloRaccoltaProvvisoria" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="IdDocumentoRaccoltaProvvisoria" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="IdentificativoRaccoltaProvvisoria" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TipoDocumentoRED" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoDocumento_type"/>
 *         &lt;element name="Ragioneria" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="Amministrazione" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="UfficioCreatoreRED" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="TipoFlusso" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoFlusso_type"/>
 *         &lt;element name="UtenteCreatore" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="DataCreazione" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="DataDocumento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="DataChiusura" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="DataAnnullamento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="Note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Protocollo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}protocollo_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="VersioneMetadata" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Metadata" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="FLUSSO_AD" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}flussoAD_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "documentoMetadataAllegatoDecretoIGB_type", propOrder = {
    "idFascicoloRaccoltaProvvisoria",
    "idDocumentoRaccoltaProvvisoria",
    "identificativoRaccoltaProvvisoria",
    "tipoDocumentoRED",
    "ragioneria",
    "amministrazione",
    "ufficioCreatoreRED",
    "tipoFlusso",
    "utenteCreatore",
    "dataCreazione",
    "dataDocumento",
    "dataChiusura",
    "dataAnnullamento",
    "note",
    "protocollo",
    "versioneMetadata",
    "metadata",
    "flussoad"
})
public class DocumentoMetadataAllegatoDecretoIGBType {

    @XmlElement(name = "IdFascicoloRaccoltaProvvisoria", required = true)
    protected String idFascicoloRaccoltaProvvisoria;
    @XmlElement(name = "IdDocumentoRaccoltaProvvisoria", required = true)
    protected String idDocumentoRaccoltaProvvisoria;
    @XmlElement(name = "IdentificativoRaccoltaProvvisoria", required = true)
    protected String identificativoRaccoltaProvvisoria;
    @XmlElement(name = "TipoDocumentoRED", required = true)
    protected TipoDocumentoType tipoDocumentoRED;
    @XmlElement(name = "Ragioneria", required = true)
    protected CodiceDescrizioneType ragioneria;
    @XmlElement(name = "Amministrazione", required = true)
    protected CodiceDescrizioneType amministrazione;
    @XmlElement(name = "UfficioCreatoreRED", required = true)
    protected CodiceDescrizioneType ufficioCreatoreRED;
    @XmlElement(name = "TipoFlusso", required = true)
    protected TipoFlussoType tipoFlusso;
    @XmlElement(name = "UtenteCreatore", required = true)
    protected CodiceDescrizioneType utenteCreatore;
    @XmlElement(name = "DataCreazione")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataCreazione;
    @XmlElement(name = "DataDocumento")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataDocumento;
    @XmlElement(name = "DataChiusura")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataChiusura;
    @XmlElement(name = "DataAnnullamento")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataAnnullamento;
    @XmlElement(name = "Note")
    protected String note;
    @XmlElement(name = "Protocollo")
    protected List<ProtocolloType> protocollo;
    @XmlElement(name = "VersioneMetadata")
    protected String versioneMetadata;
    @XmlElement(name = "Metadata")
    protected byte[] metadata;
    @XmlElement(name = "FLUSSO_AD")
    protected FlussoADType flussoad;

    /**
     * Gets the value of the idFascicoloRaccoltaProvvisoria property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloRaccoltaProvvisoria() {
        return idFascicoloRaccoltaProvvisoria;
    }

    /**
     * Sets the value of the idFascicoloRaccoltaProvvisoria property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloRaccoltaProvvisoria(String value) {
        this.idFascicoloRaccoltaProvvisoria = value;
    }

    /**
     * Gets the value of the idDocumentoRaccoltaProvvisoria property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoRaccoltaProvvisoria() {
        return idDocumentoRaccoltaProvvisoria;
    }

    /**
     * Sets the value of the idDocumentoRaccoltaProvvisoria property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoRaccoltaProvvisoria(String value) {
        this.idDocumentoRaccoltaProvvisoria = value;
    }

    /**
     * Gets the value of the identificativoRaccoltaProvvisoria property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificativoRaccoltaProvvisoria() {
        return identificativoRaccoltaProvvisoria;
    }

    /**
     * Sets the value of the identificativoRaccoltaProvvisoria property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificativoRaccoltaProvvisoria(String value) {
        this.identificativoRaccoltaProvvisoria = value;
    }

    /**
     * Gets the value of the tipoDocumentoRED property.
     * 
     * @return
     *     possible object is
     *     {@link TipoDocumentoType }
     *     
     */
    public TipoDocumentoType getTipoDocumentoRED() {
        return tipoDocumentoRED;
    }

    /**
     * Sets the value of the tipoDocumentoRED property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDocumentoType }
     *     
     */
    public void setTipoDocumentoRED(TipoDocumentoType value) {
        this.tipoDocumentoRED = value;
    }

    /**
     * Gets the value of the ragioneria property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getRagioneria() {
        return ragioneria;
    }

    /**
     * Sets the value of the ragioneria property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setRagioneria(CodiceDescrizioneType value) {
        this.ragioneria = value;
    }

    /**
     * Gets the value of the amministrazione property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getAmministrazione() {
        return amministrazione;
    }

    /**
     * Sets the value of the amministrazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setAmministrazione(CodiceDescrizioneType value) {
        this.amministrazione = value;
    }

    /**
     * Gets the value of the ufficioCreatoreRED property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getUfficioCreatoreRED() {
        return ufficioCreatoreRED;
    }

    /**
     * Sets the value of the ufficioCreatoreRED property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setUfficioCreatoreRED(CodiceDescrizioneType value) {
        this.ufficioCreatoreRED = value;
    }

    /**
     * Gets the value of the tipoFlusso property.
     * 
     * @return
     *     possible object is
     *     {@link TipoFlussoType }
     *     
     */
    public TipoFlussoType getTipoFlusso() {
        return tipoFlusso;
    }

    /**
     * Sets the value of the tipoFlusso property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoFlussoType }
     *     
     */
    public void setTipoFlusso(TipoFlussoType value) {
        this.tipoFlusso = value;
    }

    /**
     * Gets the value of the utenteCreatore property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getUtenteCreatore() {
        return utenteCreatore;
    }

    /**
     * Sets the value of the utenteCreatore property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setUtenteCreatore(CodiceDescrizioneType value) {
        this.utenteCreatore = value;
    }

    /**
     * Gets the value of the dataCreazione property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataCreazione() {
        return dataCreazione;
    }

    /**
     * Sets the value of the dataCreazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataCreazione(XMLGregorianCalendar value) {
        this.dataCreazione = value;
    }

    /**
     * Gets the value of the dataDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataDocumento() {
        return dataDocumento;
    }

    /**
     * Sets the value of the dataDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataDocumento(XMLGregorianCalendar value) {
        this.dataDocumento = value;
    }

    /**
     * Gets the value of the dataChiusura property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataChiusura() {
        return dataChiusura;
    }

    /**
     * Sets the value of the dataChiusura property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataChiusura(XMLGregorianCalendar value) {
        this.dataChiusura = value;
    }

    /**
     * Gets the value of the dataAnnullamento property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAnnullamento() {
        return dataAnnullamento;
    }

    /**
     * Sets the value of the dataAnnullamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAnnullamento(XMLGregorianCalendar value) {
        this.dataAnnullamento = value;
    }

    /**
     * Gets the value of the note property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNote() {
        return note;
    }

    /**
     * Sets the value of the note property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNote(String value) {
        this.note = value;
    }

    /**
     * Gets the value of the protocollo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the protocollo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProtocollo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtocolloType }
     * 
     * 
     */
    public List<ProtocolloType> getProtocollo() {
        if (protocollo == null) {
            protocollo = new ArrayList<ProtocolloType>();
        }
        return this.protocollo;
    }

    /**
     * Gets the value of the versioneMetadata property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersioneMetadata() {
        return versioneMetadata;
    }

    /**
     * Sets the value of the versioneMetadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersioneMetadata(String value) {
        this.versioneMetadata = value;
    }

    /**
     * Gets the value of the metadata property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getMetadata() {
        return metadata;
    }

    /**
     * Sets the value of the metadata property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setMetadata(byte[] value) {
        this.metadata = value;
    }

    /**
     * Gets the value of the flussoad property.
     * 
     * @return
     *     possible object is
     *     {@link FlussoADType }
     *     
     */
    public FlussoADType getFLUSSOAD() {
        return flussoad;
    }

    /**
     * Sets the value of the flussoad property.
     * 
     * @param value
     *     allowed object is
     *     {@link FlussoADType }
     *     
     */
    public void setFLUSSOAD(FlussoADType value) {
        this.flussoad = value;
    }

}
