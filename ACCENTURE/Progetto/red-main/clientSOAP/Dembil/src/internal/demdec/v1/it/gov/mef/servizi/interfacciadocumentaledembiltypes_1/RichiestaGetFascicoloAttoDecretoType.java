
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiesta metadati del Fascicolo AttoDecreto
 * 
 * <p>Java class for richiesta_getFascicoloAttoDecreto_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_getFascicoloAttoDecreto_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloAttoDecreto" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="TipoEstrazioneFascicolo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoEstrazione_type"/>
 *         &lt;element name="AttoDecretoDocumentCriteria" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}criteriaDocumentiAttoDecreto_type" minOccurs="0"/>
 *         &lt;element name="DecretoIGBDocumentCriteria" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}criteriaDocumentiFascicoloIGB_type" minOccurs="0"/>
 *         &lt;element name="FascicoliSIPATRCriteria" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}criteriaFascicoliSIPATR_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_getFascicoloAttoDecreto_type", propOrder = {
    "idFascicoloAttoDecreto",
    "tipoEstrazioneFascicolo",
    "attoDecretoDocumentCriteria",
    "decretoIGBDocumentCriteria",
    "fascicoliSIPATRCriteria"
})
public class RichiestaGetFascicoloAttoDecretoType {

    @XmlElement(name = "IdFascicoloAttoDecreto", required = true)
    protected String idFascicoloAttoDecreto;
    @XmlElement(name = "TipoEstrazioneFascicolo", required = true, defaultValue = "DATA")
    protected TipoEstrazioneType tipoEstrazioneFascicolo;
    @XmlElement(name = "AttoDecretoDocumentCriteria")
    protected CriteriaDocumentiAttoDecretoType attoDecretoDocumentCriteria;
    @XmlElement(name = "DecretoIGBDocumentCriteria")
    protected CriteriaDocumentiFascicoloIGBType decretoIGBDocumentCriteria;
    @XmlElement(name = "FascicoliSIPATRCriteria")
    protected CriteriaFascicoliSIPATRType fascicoliSIPATRCriteria;

    /**
     * Gets the value of the idFascicoloAttoDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloAttoDecreto() {
        return idFascicoloAttoDecreto;
    }

    /**
     * Sets the value of the idFascicoloAttoDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloAttoDecreto(String value) {
        this.idFascicoloAttoDecreto = value;
    }

    /**
     * Gets the value of the tipoEstrazioneFascicolo property.
     * 
     * @return
     *     possible object is
     *     {@link TipoEstrazioneType }
     *     
     */
    public TipoEstrazioneType getTipoEstrazioneFascicolo() {
        return tipoEstrazioneFascicolo;
    }

    /**
     * Sets the value of the tipoEstrazioneFascicolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEstrazioneType }
     *     
     */
    public void setTipoEstrazioneFascicolo(TipoEstrazioneType value) {
        this.tipoEstrazioneFascicolo = value;
    }

    /**
     * Gets the value of the attoDecretoDocumentCriteria property.
     * 
     * @return
     *     possible object is
     *     {@link CriteriaDocumentiAttoDecretoType }
     *     
     */
    public CriteriaDocumentiAttoDecretoType getAttoDecretoDocumentCriteria() {
        return attoDecretoDocumentCriteria;
    }

    /**
     * Sets the value of the attoDecretoDocumentCriteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link CriteriaDocumentiAttoDecretoType }
     *     
     */
    public void setAttoDecretoDocumentCriteria(CriteriaDocumentiAttoDecretoType value) {
        this.attoDecretoDocumentCriteria = value;
    }

    /**
     * Gets the value of the decretoIGBDocumentCriteria property.
     * 
     * @return
     *     possible object is
     *     {@link CriteriaDocumentiFascicoloIGBType }
     *     
     */
    public CriteriaDocumentiFascicoloIGBType getDecretoIGBDocumentCriteria() {
        return decretoIGBDocumentCriteria;
    }

    /**
     * Sets the value of the decretoIGBDocumentCriteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link CriteriaDocumentiFascicoloIGBType }
     *     
     */
    public void setDecretoIGBDocumentCriteria(CriteriaDocumentiFascicoloIGBType value) {
        this.decretoIGBDocumentCriteria = value;
    }

    /**
     * Gets the value of the fascicoliSIPATRCriteria property.
     * 
     * @return
     *     possible object is
     *     {@link CriteriaFascicoliSIPATRType }
     *     
     */
    public CriteriaFascicoliSIPATRType getFascicoliSIPATRCriteria() {
        return fascicoliSIPATRCriteria;
    }

    /**
     * Sets the value of the fascicoliSIPATRCriteria property.
     * 
     * @param value
     *     allowed object is
     *     {@link CriteriaFascicoliSIPATRType }
     *     
     */
    public void setFascicoliSIPATRCriteria(CriteriaFascicoliSIPATRType value) {
        this.fascicoliSIPATRCriteria = value;
    }

}
