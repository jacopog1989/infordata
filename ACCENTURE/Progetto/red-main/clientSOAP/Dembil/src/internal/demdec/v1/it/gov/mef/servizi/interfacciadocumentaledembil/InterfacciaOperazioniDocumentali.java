
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import internal.demdec.v1.it.gov.mef.servizi.common.headeraccessoapplicativo.AccessoApplicativo;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RichiestaConvertiDocumentoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaConvertiDocumentoType;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.4-b01
 * Generated source version: 2.2
 * 
 */
@WebService(name = "InterfacciaOperazioniDocumentali", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBIL")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.ObjectFactory.class,
    internal.demdec.v1.it.gov.mef.servizi.common.headeraccessoapplicativo.ObjectFactory.class,
    internal.demdec.v1.it.gov.mef.servizi.common.headerfault.ObjectFactory.class,
    org.xmlsoap.schemas.soap.envelope.ObjectFactory.class
})
public interface InterfacciaOperazioniDocumentali {


    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaConvertiDocumentoType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:convertiDocumento")
    @WebResult(name = "risposta_convertiDocumento", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
    public RispostaConvertiDocumentoType convertiDocumento(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_convertiDocumento", targetNamespace = "http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1", partName = "parameters")
        RichiestaConvertiDocumentoType parameters)
        throws GenericFault, SecurityFault
    ;

}
