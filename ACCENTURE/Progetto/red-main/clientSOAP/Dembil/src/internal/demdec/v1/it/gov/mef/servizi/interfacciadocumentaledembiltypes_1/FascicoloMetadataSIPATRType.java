
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Metadati del Fascicolo SIPATR
 * 
 * <p>Java class for fascicoloMetadataSIPATR_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="fascicoloMetadataSIPATR_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdentificativoSIPATR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Amministrazione" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="Ragioneria" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="Titolo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}codiceDescrizione_type"/>
 *         &lt;element name="StatoFascicoloSIPATR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DataChiusuraAmministrativa" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="Metadata" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="VersioneMetadata" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fascicoloMetadataSIPATR_type", propOrder = {
    "identificativoSIPATR",
    "amministrazione",
    "ragioneria",
    "titolo",
    "statoFascicoloSIPATR",
    "dataChiusuraAmministrativa",
    "metadata",
    "versioneMetadata"
})
public class FascicoloMetadataSIPATRType {

    @XmlElement(name = "IdentificativoSIPATR", required = true)
    protected String identificativoSIPATR;
    @XmlElement(name = "Amministrazione", required = true)
    protected CodiceDescrizioneType amministrazione;
    @XmlElement(name = "Ragioneria", required = true)
    protected CodiceDescrizioneType ragioneria;
    @XmlElement(name = "Titolo", required = true)
    protected CodiceDescrizioneType titolo;
    @XmlElement(name = "StatoFascicoloSIPATR", required = true)
    protected String statoFascicoloSIPATR;
    @XmlElement(name = "DataChiusuraAmministrativa")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataChiusuraAmministrativa;
    @XmlElement(name = "Metadata")
    protected byte[] metadata;
    @XmlElement(name = "VersioneMetadata")
    protected String versioneMetadata;

    /**
     * Gets the value of the identificativoSIPATR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificativoSIPATR() {
        return identificativoSIPATR;
    }

    /**
     * Sets the value of the identificativoSIPATR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificativoSIPATR(String value) {
        this.identificativoSIPATR = value;
    }

    /**
     * Gets the value of the amministrazione property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getAmministrazione() {
        return amministrazione;
    }

    /**
     * Sets the value of the amministrazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setAmministrazione(CodiceDescrizioneType value) {
        this.amministrazione = value;
    }

    /**
     * Gets the value of the ragioneria property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getRagioneria() {
        return ragioneria;
    }

    /**
     * Sets the value of the ragioneria property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setRagioneria(CodiceDescrizioneType value) {
        this.ragioneria = value;
    }

    /**
     * Gets the value of the titolo property.
     * 
     * @return
     *     possible object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public CodiceDescrizioneType getTitolo() {
        return titolo;
    }

    /**
     * Sets the value of the titolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CodiceDescrizioneType }
     *     
     */
    public void setTitolo(CodiceDescrizioneType value) {
        this.titolo = value;
    }

    /**
     * Gets the value of the statoFascicoloSIPATR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatoFascicoloSIPATR() {
        return statoFascicoloSIPATR;
    }

    /**
     * Sets the value of the statoFascicoloSIPATR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatoFascicoloSIPATR(String value) {
        this.statoFascicoloSIPATR = value;
    }

    /**
     * Gets the value of the dataChiusuraAmministrativa property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataChiusuraAmministrativa() {
        return dataChiusuraAmministrativa;
    }

    /**
     * Sets the value of the dataChiusuraAmministrativa property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataChiusuraAmministrativa(XMLGregorianCalendar value) {
        this.dataChiusuraAmministrativa = value;
    }

    /**
     * Gets the value of the metadata property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getMetadata() {
        return metadata;
    }

    /**
     * Sets the value of the metadata property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setMetadata(byte[] value) {
        this.metadata = value;
    }

    /**
     * Gets the value of the versioneMetadata property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersioneMetadata() {
        return versioneMetadata;
    }

    /**
     * Sets the value of the versioneMetadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersioneMetadata(String value) {
        this.versioneMetadata = value;
    }

}
