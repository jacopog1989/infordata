
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Dati del Documento del Fascicolo Atto Decreto
 * 
 * <p>Java class for risposta_getDocumentoAggiuntivoFascicoloDecretoSIPATR_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="risposta_getDocumentoAggiuntivoFascicoloDecretoSIPATR_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}BaseServiceResponseType">
 *       &lt;sequence>
 *         &lt;element name="DettaglioDocumento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}documentoFascicoloAttoDecreto_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_getDocumentoAggiuntivoFascicoloDecretoSIPATR_type", propOrder = {
    "dettaglioDocumento"
})
public class RispostaGetDocumentoAggiuntivoFascicoloDecretoSIPATRType
    extends BaseServiceResponseType
{

    @XmlElement(name = "DettaglioDocumento")
    protected DocumentoFascicoloAttoDecretoType dettaglioDocumento;

    /**
     * Gets the value of the dettaglioDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoFascicoloAttoDecretoType }
     *     
     */
    public DocumentoFascicoloAttoDecretoType getDettaglioDocumento() {
        return dettaglioDocumento;
    }

    /**
     * Sets the value of the dettaglioDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoFascicoloAttoDecretoType }
     *     
     */
    public void setDettaglioDocumento(DocumentoFascicoloAttoDecretoType value) {
        this.dettaglioDocumento = value;
    }

}
