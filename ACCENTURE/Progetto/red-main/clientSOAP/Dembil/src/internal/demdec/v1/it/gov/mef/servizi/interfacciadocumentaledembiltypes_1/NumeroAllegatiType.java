
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Contatore Allegati
 * 
 * <p>Java class for numeroAllegati_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="numeroAllegati_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="numeroAllegatiTotali" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="numeroAllegatiFirmati" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="numeroAllegatiDainviare" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="numeroAllegatiDaCompletare" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="numeroAllegatiInErrore" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="numeroAllegatiConFirmaInvalida" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="numeroAllegatiFascicolo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="numeroAllegatiSottoFascicoli" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "numeroAllegati_type", propOrder = {
    "numeroAllegatiTotali",
    "numeroAllegatiFirmati",
    "numeroAllegatiDainviare",
    "numeroAllegatiDaCompletare",
    "numeroAllegatiInErrore",
    "numeroAllegatiConFirmaInvalida",
    "numeroAllegatiFascicolo",
    "numeroAllegatiSottoFascicoli"
})
public class NumeroAllegatiType {

    @XmlElement(defaultValue = "0")
    protected Integer numeroAllegatiTotali;
    @XmlElement(defaultValue = "0")
    protected Integer numeroAllegatiFirmati;
    @XmlElement(defaultValue = "0")
    protected Integer numeroAllegatiDainviare;
    @XmlElement(defaultValue = "0")
    protected Integer numeroAllegatiDaCompletare;
    @XmlElement(defaultValue = "0")
    protected Integer numeroAllegatiInErrore;
    @XmlElement(defaultValue = "0")
    protected Integer numeroAllegatiConFirmaInvalida;
    @XmlElement(defaultValue = "0")
    protected Integer numeroAllegatiFascicolo;
    @XmlElement(defaultValue = "0")
    protected Integer numeroAllegatiSottoFascicoli;

    /**
     * Gets the value of the numeroAllegatiTotali property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumeroAllegatiTotali() {
        return numeroAllegatiTotali;
    }

    /**
     * Sets the value of the numeroAllegatiTotali property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumeroAllegatiTotali(Integer value) {
        this.numeroAllegatiTotali = value;
    }

    /**
     * Gets the value of the numeroAllegatiFirmati property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumeroAllegatiFirmati() {
        return numeroAllegatiFirmati;
    }

    /**
     * Sets the value of the numeroAllegatiFirmati property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumeroAllegatiFirmati(Integer value) {
        this.numeroAllegatiFirmati = value;
    }

    /**
     * Gets the value of the numeroAllegatiDainviare property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumeroAllegatiDainviare() {
        return numeroAllegatiDainviare;
    }

    /**
     * Sets the value of the numeroAllegatiDainviare property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumeroAllegatiDainviare(Integer value) {
        this.numeroAllegatiDainviare = value;
    }

    /**
     * Gets the value of the numeroAllegatiDaCompletare property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumeroAllegatiDaCompletare() {
        return numeroAllegatiDaCompletare;
    }

    /**
     * Sets the value of the numeroAllegatiDaCompletare property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumeroAllegatiDaCompletare(Integer value) {
        this.numeroAllegatiDaCompletare = value;
    }

    /**
     * Gets the value of the numeroAllegatiInErrore property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumeroAllegatiInErrore() {
        return numeroAllegatiInErrore;
    }

    /**
     * Sets the value of the numeroAllegatiInErrore property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumeroAllegatiInErrore(Integer value) {
        this.numeroAllegatiInErrore = value;
    }

    /**
     * Gets the value of the numeroAllegatiConFirmaInvalida property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumeroAllegatiConFirmaInvalida() {
        return numeroAllegatiConFirmaInvalida;
    }

    /**
     * Sets the value of the numeroAllegatiConFirmaInvalida property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumeroAllegatiConFirmaInvalida(Integer value) {
        this.numeroAllegatiConFirmaInvalida = value;
    }

    /**
     * Gets the value of the numeroAllegatiFascicolo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumeroAllegatiFascicolo() {
        return numeroAllegatiFascicolo;
    }

    /**
     * Sets the value of the numeroAllegatiFascicolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumeroAllegatiFascicolo(Integer value) {
        this.numeroAllegatiFascicolo = value;
    }

    /**
     * Gets the value of the numeroAllegatiSottoFascicoli property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumeroAllegatiSottoFascicoli() {
        return numeroAllegatiSottoFascicoli;
    }

    /**
     * Sets the value of the numeroAllegatiSottoFascicoli property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumeroAllegatiSottoFascicoli(Integer value) {
        this.numeroAllegatiSottoFascicoli = value;
    }

}
