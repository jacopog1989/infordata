
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tipoEstrazioneDocumento_type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoEstrazioneDocumento_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ALL"/>
 *     &lt;enumeration value="FIRMATI"/>
 *     &lt;enumeration value="DA_INVIARE"/>
 *     &lt;enumeration value="IN_ERRORE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoEstrazioneDocumento_type")
@XmlEnum
public enum TipoEstrazioneDocumentoType {

    ALL,
    FIRMATI,
    DA_INVIARE,
    IN_ERRORE;

    public String value() {
        return name();
    }

    public static TipoEstrazioneDocumentoType fromValue(String v) {
        return valueOf(v);
    }

}
