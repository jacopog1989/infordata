
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Esito aggiunta del documento al Fascicolo Atto Decreto
 * 
 * <p>Java class for risposta_addDocumentoFascicoloAttoDecretoCorte_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="risposta_addDocumentoFascicoloAttoDecretoCorte_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}BaseServiceResponseType">
 *       &lt;sequence>
 *         &lt;element name="Dati" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}documentoFile_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_addDocumentoFascicoloAttoDecretoCorte_type", propOrder = {
    "dati"
})
public class RispostaAddDocumentoFascicoloAttoDecretoCorteType
    extends BaseServiceResponseType
{

    @XmlElement(name = "Dati")
    protected DocumentoFileType dati;

    /**
     * Gets the value of the dati property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoFileType }
     *     
     */
    public DocumentoFileType getDati() {
        return dati;
    }

    /**
     * Sets the value of the dati property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoFileType }
     *     
     */
    public void setDati(DocumentoFileType value) {
        this.dati = value;
    }

}
