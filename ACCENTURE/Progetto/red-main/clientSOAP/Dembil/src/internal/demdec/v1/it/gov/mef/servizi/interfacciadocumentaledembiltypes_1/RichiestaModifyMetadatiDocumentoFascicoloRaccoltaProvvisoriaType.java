
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiede l'aggiornamento dei metadati di un documento del Fascicolo Raccolta Provvisoria
 * 
 * <p>Java class for richiesta_modifyMetadatiDocumentoFascicoloRaccoltaProvvisoria_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_modifyMetadatiDocumentoFascicoloRaccoltaProvvisoria_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloRaccoltaProvvisoria" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="IdDocumento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DocumentoMetadata" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}documentoMetadataRaccoltaProvvisoria_type" minOccurs="0"/>
 *         &lt;element name="Raggruppamento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}raggruppamentoType" minOccurs="0"/>
 *         &lt;element name="DaInviare" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Condivisibile" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Attivo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_modifyMetadatiDocumentoFascicoloRaccoltaProvvisoria_type", propOrder = {
    "idFascicoloRaccoltaProvvisoria",
    "idDocumento",
    "descrizione",
    "documentoMetadata",
    "raggruppamento",
    "daInviare",
    "condivisibile",
    "attivo"
})
public class RichiestaModifyMetadatiDocumentoFascicoloRaccoltaProvvisoriaType {

    @XmlElement(name = "IdFascicoloRaccoltaProvvisoria", required = true)
    protected String idFascicoloRaccoltaProvvisoria;
    @XmlElement(name = "IdDocumento", required = true)
    protected String idDocumento;
    @XmlElement(name = "Descrizione")
    protected String descrizione;
    @XmlElement(name = "DocumentoMetadata")
    protected DocumentoMetadataRaccoltaProvvisoriaType documentoMetadata;
    @XmlElement(name = "Raggruppamento")
    protected RaggruppamentoType raggruppamento;
    @XmlElement(name = "DaInviare", defaultValue = "true")
    protected boolean daInviare;
    @XmlElement(name = "Condivisibile", defaultValue = "true")
    protected boolean condivisibile;
    @XmlElement(name = "Attivo", defaultValue = "true")
    protected boolean attivo;

    /**
     * Gets the value of the idFascicoloRaccoltaProvvisoria property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloRaccoltaProvvisoria() {
        return idFascicoloRaccoltaProvvisoria;
    }

    /**
     * Sets the value of the idFascicoloRaccoltaProvvisoria property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloRaccoltaProvvisoria(String value) {
        this.idFascicoloRaccoltaProvvisoria = value;
    }

    /**
     * Gets the value of the idDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumento() {
        return idDocumento;
    }

    /**
     * Sets the value of the idDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumento(String value) {
        this.idDocumento = value;
    }

    /**
     * Gets the value of the descrizione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Sets the value of the descrizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Gets the value of the documentoMetadata property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoMetadataRaccoltaProvvisoriaType }
     *     
     */
    public DocumentoMetadataRaccoltaProvvisoriaType getDocumentoMetadata() {
        return documentoMetadata;
    }

    /**
     * Sets the value of the documentoMetadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoMetadataRaccoltaProvvisoriaType }
     *     
     */
    public void setDocumentoMetadata(DocumentoMetadataRaccoltaProvvisoriaType value) {
        this.documentoMetadata = value;
    }

    /**
     * Gets the value of the raggruppamento property.
     * 
     * @return
     *     possible object is
     *     {@link RaggruppamentoType }
     *     
     */
    public RaggruppamentoType getRaggruppamento() {
        return raggruppamento;
    }

    /**
     * Sets the value of the raggruppamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link RaggruppamentoType }
     *     
     */
    public void setRaggruppamento(RaggruppamentoType value) {
        this.raggruppamento = value;
    }

    /**
     * Gets the value of the daInviare property.
     * 
     */
    public boolean isDaInviare() {
        return daInviare;
    }

    /**
     * Sets the value of the daInviare property.
     * 
     */
    public void setDaInviare(boolean value) {
        this.daInviare = value;
    }

    /**
     * Gets the value of the condivisibile property.
     * 
     */
    public boolean isCondivisibile() {
        return condivisibile;
    }

    /**
     * Sets the value of the condivisibile property.
     * 
     */
    public void setCondivisibile(boolean value) {
        this.condivisibile = value;
    }

    /**
     * Gets the value of the attivo property.
     * 
     */
    public boolean isAttivo() {
        return attivo;
    }

    /**
     * Sets the value of the attivo property.
     * 
     */
    public void setAttivo(boolean value) {
        this.attivo = value;
    }

}
