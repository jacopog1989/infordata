
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiede l'aggiornamento dei metadati del Fascicolo Raccolta Provvisoria
 * 
 * <p>Java class for richiesta_modifyMetadataFascicoloRaccoltaProvvisoria_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_modifyMetadataFascicoloRaccoltaProvvisoria_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloRaccoltaProvvisoria" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="FascicoloMetadata" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}fascicoloMetadataRaccoltaProvvisoria_type"/>
 *         &lt;element name="DaInviare" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Condivisibile" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Attivo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_modifyMetadataFascicoloRaccoltaProvvisoria_type", propOrder = {
    "idFascicoloRaccoltaProvvisoria",
    "fascicoloMetadata",
    "daInviare",
    "condivisibile",
    "attivo"
})
public class RichiestaModifyMetadataFascicoloRaccoltaProvvisoriaType {

    @XmlElement(name = "IdFascicoloRaccoltaProvvisoria", required = true)
    protected String idFascicoloRaccoltaProvvisoria;
    @XmlElement(name = "FascicoloMetadata", required = true)
    protected FascicoloMetadataRaccoltaProvvisoriaType fascicoloMetadata;
    @XmlElement(name = "DaInviare", defaultValue = "true")
    protected boolean daInviare;
    @XmlElement(name = "Condivisibile", defaultValue = "true")
    protected boolean condivisibile;
    @XmlElement(name = "Attivo", defaultValue = "true")
    protected boolean attivo;

    /**
     * Gets the value of the idFascicoloRaccoltaProvvisoria property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloRaccoltaProvvisoria() {
        return idFascicoloRaccoltaProvvisoria;
    }

    /**
     * Sets the value of the idFascicoloRaccoltaProvvisoria property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloRaccoltaProvvisoria(String value) {
        this.idFascicoloRaccoltaProvvisoria = value;
    }

    /**
     * Gets the value of the fascicoloMetadata property.
     * 
     * @return
     *     possible object is
     *     {@link FascicoloMetadataRaccoltaProvvisoriaType }
     *     
     */
    public FascicoloMetadataRaccoltaProvvisoriaType getFascicoloMetadata() {
        return fascicoloMetadata;
    }

    /**
     * Sets the value of the fascicoloMetadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link FascicoloMetadataRaccoltaProvvisoriaType }
     *     
     */
    public void setFascicoloMetadata(FascicoloMetadataRaccoltaProvvisoriaType value) {
        this.fascicoloMetadata = value;
    }

    /**
     * Gets the value of the daInviare property.
     * 
     */
    public boolean isDaInviare() {
        return daInviare;
    }

    /**
     * Sets the value of the daInviare property.
     * 
     */
    public void setDaInviare(boolean value) {
        this.daInviare = value;
    }

    /**
     * Gets the value of the condivisibile property.
     * 
     */
    public boolean isCondivisibile() {
        return condivisibile;
    }

    /**
     * Sets the value of the condivisibile property.
     * 
     */
    public void setCondivisibile(boolean value) {
        this.condivisibile = value;
    }

    /**
     * Gets the value of the attivo property.
     * 
     */
    public boolean isAttivo() {
        return attivo;
    }

    /**
     * Sets the value of the attivo property.
     * 
     */
    public void setAttivo(boolean value) {
        this.attivo = value;
    }

}
