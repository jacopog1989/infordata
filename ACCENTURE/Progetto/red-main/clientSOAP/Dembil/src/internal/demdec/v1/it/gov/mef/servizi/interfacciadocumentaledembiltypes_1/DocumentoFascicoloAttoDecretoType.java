
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Tipologia dei documenti presenti nel fascicolo Atto Decreto
 * 
 * <p>Java class for documentoFascicoloAttoDecreto_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="documentoFascicoloAttoDecreto_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdDocumento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="TipoDocumento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoDocumento_type"/>
 *         &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DataCreazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="MetadatiDocumento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}documentoMetadataAttoDecreto_type" minOccurs="0"/>
 *         &lt;element name="FileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Hash" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *         &lt;element name="MimeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Length" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="TipoCompressione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Firmato" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="ValidazioneFirma" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}validazioneFirma_type" minOccurs="0"/>
 *         &lt;element name="Condivisibile" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="DaInviare" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Attivo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SistemaProduttore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsDocumentoOriginale" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsDocumentoTrattato" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="DocumentoOrigine" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}documentoFile_type" minOccurs="0"/>
 *         &lt;element name="Operazione" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}operazioneDocumentale_type" minOccurs="0"/>
 *         &lt;element name="TicketArchiviazione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PreviewPng" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="Raggruppamento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}raggruppamentoType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "documentoFascicoloAttoDecreto_type", propOrder = {
    "idDocumento",
    "tipoDocumento",
    "descrizione",
    "dataCreazione",
    "metadatiDocumento",
    "fileName",
    "hash",
    "mimeType",
    "length",
    "tipoCompressione",
    "firmato",
    "validazioneFirma",
    "condivisibile",
    "daInviare",
    "attivo",
    "sistemaProduttore",
    "isDocumentoOriginale",
    "isDocumentoTrattato",
    "documentoOrigine",
    "operazione",
    "ticketArchiviazione",
    "previewPng",
    "raggruppamento"
})
public class DocumentoFascicoloAttoDecretoType {

    @XmlElement(name = "IdDocumento", required = true)
    protected String idDocumento;
    @XmlElement(name = "TipoDocumento", required = true)
    protected TipoDocumentoType tipoDocumento;
    @XmlElement(name = "Descrizione", required = true)
    protected String descrizione;
    @XmlElement(name = "DataCreazione", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataCreazione;
    @XmlElement(name = "MetadatiDocumento")
    protected DocumentoMetadataAttoDecretoType metadatiDocumento;
    @XmlElement(name = "FileName", required = true, nillable = true)
    protected String fileName;
    @XmlElement(name = "Hash", required = true, nillable = true)
    protected byte[] hash;
    @XmlElement(name = "MimeType", required = true)
    protected String mimeType;
    @XmlElement(name = "Length")
    protected long length;
    @XmlElement(name = "TipoCompressione")
    protected String tipoCompressione;
    @XmlElement(name = "Firmato")
    protected boolean firmato;
    @XmlElement(name = "ValidazioneFirma")
    protected ValidazioneFirmaType validazioneFirma;
    @XmlElement(name = "Condivisibile", defaultValue = "true")
    protected boolean condivisibile;
    @XmlElement(name = "DaInviare", defaultValue = "true")
    protected boolean daInviare;
    @XmlElement(name = "Attivo", defaultValue = "true")
    protected boolean attivo;
    @XmlElement(name = "SistemaProduttore", required = true)
    protected String sistemaProduttore;
    @XmlElement(name = "IsDocumentoOriginale", defaultValue = "true")
    protected boolean isDocumentoOriginale;
    @XmlElement(name = "IsDocumentoTrattato", defaultValue = "true")
    protected boolean isDocumentoTrattato;
    @XmlElement(name = "DocumentoOrigine")
    protected DocumentoFileType documentoOrigine;
    @XmlElement(name = "Operazione")
    protected OperazioneDocumentaleType operazione;
    @XmlElementRef(name = "TicketArchiviazione", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ticketArchiviazione;
    @XmlElement(name = "PreviewPng")
    protected byte[] previewPng;
    @XmlElement(name = "Raggruppamento")
    protected RaggruppamentoType raggruppamento;

    /**
     * Gets the value of the idDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumento() {
        return idDocumento;
    }

    /**
     * Sets the value of the idDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumento(String value) {
        this.idDocumento = value;
    }

    /**
     * Gets the value of the tipoDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link TipoDocumentoType }
     *     
     */
    public TipoDocumentoType getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Sets the value of the tipoDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDocumentoType }
     *     
     */
    public void setTipoDocumento(TipoDocumentoType value) {
        this.tipoDocumento = value;
    }

    /**
     * Gets the value of the descrizione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Sets the value of the descrizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Gets the value of the dataCreazione property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataCreazione() {
        return dataCreazione;
    }

    /**
     * Sets the value of the dataCreazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataCreazione(XMLGregorianCalendar value) {
        this.dataCreazione = value;
    }

    /**
     * Gets the value of the metadatiDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoMetadataAttoDecretoType }
     *     
     */
    public DocumentoMetadataAttoDecretoType getMetadatiDocumento() {
        return metadatiDocumento;
    }

    /**
     * Sets the value of the metadatiDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoMetadataAttoDecretoType }
     *     
     */
    public void setMetadatiDocumento(DocumentoMetadataAttoDecretoType value) {
        this.metadatiDocumento = value;
    }

    /**
     * Gets the value of the fileName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Sets the value of the fileName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileName(String value) {
        this.fileName = value;
    }

    /**
     * Gets the value of the hash property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getHash() {
        return hash;
    }

    /**
     * Sets the value of the hash property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setHash(byte[] value) {
        this.hash = value;
    }

    /**
     * Gets the value of the mimeType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMimeType() {
        return mimeType;
    }

    /**
     * Sets the value of the mimeType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMimeType(String value) {
        this.mimeType = value;
    }

    /**
     * Gets the value of the length property.
     * 
     */
    public long getLength() {
        return length;
    }

    /**
     * Sets the value of the length property.
     * 
     */
    public void setLength(long value) {
        this.length = value;
    }

    /**
     * Gets the value of the tipoCompressione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCompressione() {
        return tipoCompressione;
    }

    /**
     * Sets the value of the tipoCompressione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCompressione(String value) {
        this.tipoCompressione = value;
    }

    /**
     * Gets the value of the firmato property.
     * 
     */
    public boolean isFirmato() {
        return firmato;
    }

    /**
     * Sets the value of the firmato property.
     * 
     */
    public void setFirmato(boolean value) {
        this.firmato = value;
    }

    /**
     * Gets the value of the validazioneFirma property.
     * 
     * @return
     *     possible object is
     *     {@link ValidazioneFirmaType }
     *     
     */
    public ValidazioneFirmaType getValidazioneFirma() {
        return validazioneFirma;
    }

    /**
     * Sets the value of the validazioneFirma property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidazioneFirmaType }
     *     
     */
    public void setValidazioneFirma(ValidazioneFirmaType value) {
        this.validazioneFirma = value;
    }

    /**
     * Gets the value of the condivisibile property.
     * 
     */
    public boolean isCondivisibile() {
        return condivisibile;
    }

    /**
     * Sets the value of the condivisibile property.
     * 
     */
    public void setCondivisibile(boolean value) {
        this.condivisibile = value;
    }

    /**
     * Gets the value of the daInviare property.
     * 
     */
    public boolean isDaInviare() {
        return daInviare;
    }

    /**
     * Sets the value of the daInviare property.
     * 
     */
    public void setDaInviare(boolean value) {
        this.daInviare = value;
    }

    /**
     * Gets the value of the attivo property.
     * 
     */
    public boolean isAttivo() {
        return attivo;
    }

    /**
     * Sets the value of the attivo property.
     * 
     */
    public void setAttivo(boolean value) {
        this.attivo = value;
    }

    /**
     * Gets the value of the sistemaProduttore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaProduttore() {
        return sistemaProduttore;
    }

    /**
     * Sets the value of the sistemaProduttore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaProduttore(String value) {
        this.sistemaProduttore = value;
    }

    /**
     * Gets the value of the isDocumentoOriginale property.
     * 
     */
    public boolean isIsDocumentoOriginale() {
        return isDocumentoOriginale;
    }

    /**
     * Sets the value of the isDocumentoOriginale property.
     * 
     */
    public void setIsDocumentoOriginale(boolean value) {
        this.isDocumentoOriginale = value;
    }

    /**
     * Gets the value of the isDocumentoTrattato property.
     * 
     */
    public boolean isIsDocumentoTrattato() {
        return isDocumentoTrattato;
    }

    /**
     * Sets the value of the isDocumentoTrattato property.
     * 
     */
    public void setIsDocumentoTrattato(boolean value) {
        this.isDocumentoTrattato = value;
    }

    /**
     * Gets the value of the documentoOrigine property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoFileType }
     *     
     */
    public DocumentoFileType getDocumentoOrigine() {
        return documentoOrigine;
    }

    /**
     * Sets the value of the documentoOrigine property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoFileType }
     *     
     */
    public void setDocumentoOrigine(DocumentoFileType value) {
        this.documentoOrigine = value;
    }

    /**
     * Gets the value of the operazione property.
     * 
     * @return
     *     possible object is
     *     {@link OperazioneDocumentaleType }
     *     
     */
    public OperazioneDocumentaleType getOperazione() {
        return operazione;
    }

    /**
     * Sets the value of the operazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link OperazioneDocumentaleType }
     *     
     */
    public void setOperazione(OperazioneDocumentaleType value) {
        this.operazione = value;
    }

    /**
     * Gets the value of the ticketArchiviazione property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTicketArchiviazione() {
        return ticketArchiviazione;
    }

    /**
     * Sets the value of the ticketArchiviazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTicketArchiviazione(JAXBElement<String> value) {
        this.ticketArchiviazione = value;
    }

    /**
     * Gets the value of the previewPng property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getPreviewPng() {
        return previewPng;
    }

    /**
     * Sets the value of the previewPng property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setPreviewPng(byte[] value) {
        this.previewPng = value;
    }

    /**
     * Gets the value of the raggruppamento property.
     * 
     * @return
     *     possible object is
     *     {@link RaggruppamentoType }
     *     
     */
    public RaggruppamentoType getRaggruppamento() {
        return raggruppamento;
    }

    /**
     * Sets the value of the raggruppamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link RaggruppamentoType }
     *     
     */
    public void setRaggruppamento(RaggruppamentoType value) {
        this.raggruppamento = value;
    }

}
