
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiesta documenti del Fascicolo
 * 
 * <p>Java class for richiesta_getElencoDocumentiFascicolo_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_getElencoDocumentiFascicolo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicolo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="TipoFascicolo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoFascicolo_type"/>
 *         &lt;element name="TipoEstrazioneDocumenti" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoEstrazioneDocumento_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_getElencoDocumentiFascicolo_type", propOrder = {
    "idFascicolo",
    "tipoFascicolo",
    "tipoEstrazioneDocumenti"
})
public class RichiestaGetElencoDocumentiFascicoloType {

    @XmlElement(name = "IdFascicolo", required = true)
    protected String idFascicolo;
    @XmlElement(name = "TipoFascicolo", required = true)
    protected TipoFascicoloType tipoFascicolo;
    @XmlElement(name = "TipoEstrazioneDocumenti", required = true)
    protected TipoEstrazioneDocumentoType tipoEstrazioneDocumenti;

    /**
     * Gets the value of the idFascicolo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicolo() {
        return idFascicolo;
    }

    /**
     * Sets the value of the idFascicolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicolo(String value) {
        this.idFascicolo = value;
    }

    /**
     * Gets the value of the tipoFascicolo property.
     * 
     * @return
     *     possible object is
     *     {@link TipoFascicoloType }
     *     
     */
    public TipoFascicoloType getTipoFascicolo() {
        return tipoFascicolo;
    }

    /**
     * Sets the value of the tipoFascicolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoFascicoloType }
     *     
     */
    public void setTipoFascicolo(TipoFascicoloType value) {
        this.tipoFascicolo = value;
    }

    /**
     * Gets the value of the tipoEstrazioneDocumenti property.
     * 
     * @return
     *     possible object is
     *     {@link TipoEstrazioneDocumentoType }
     *     
     */
    public TipoEstrazioneDocumentoType getTipoEstrazioneDocumenti() {
        return tipoEstrazioneDocumenti;
    }

    /**
     * Sets the value of the tipoEstrazioneDocumenti property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoEstrazioneDocumentoType }
     *     
     */
    public void setTipoEstrazioneDocumenti(TipoEstrazioneDocumentoType value) {
        this.tipoEstrazioneDocumenti = value;
    }

}
