
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiesta Conversione documento sincrona
 * 
 * <p>Java class for richiesta_convertiDocumento_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_convertiDocumento_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoConversione" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoConversione_type"/>
 *         &lt;element name="DatiDocumento" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}conversioneContent_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_convertiDocumento_type", propOrder = {
    "tipoConversione",
    "datiDocumento"
})
public class RichiestaConvertiDocumentoType {

    @XmlElement(name = "TipoConversione", required = true)
    protected TipoConversioneType tipoConversione;
    @XmlElement(name = "DatiDocumento", required = true)
    protected ConversioneContentType datiDocumento;

    /**
     * Gets the value of the tipoConversione property.
     * 
     * @return
     *     possible object is
     *     {@link TipoConversioneType }
     *     
     */
    public TipoConversioneType getTipoConversione() {
        return tipoConversione;
    }

    /**
     * Sets the value of the tipoConversione property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoConversioneType }
     *     
     */
    public void setTipoConversione(TipoConversioneType value) {
        this.tipoConversione = value;
    }

    /**
     * Gets the value of the datiDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link ConversioneContentType }
     *     
     */
    public ConversioneContentType getDatiDocumento() {
        return datiDocumento;
    }

    /**
     * Sets the value of the datiDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConversioneContentType }
     *     
     */
    public void setDatiDocumento(ConversioneContentType value) {
        this.datiDocumento = value;
    }

}
