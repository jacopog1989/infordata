
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * dati del flusso AD con CDC
 * 
 * <p>Java class for flussoAD_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="flussoAD_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DataMessaggio" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="IdentificativoMessaggio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdentificatoreProcesso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="tipoMessaggio" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}tipoMessaggioAD_type"/>
 *         &lt;element name="Identificatore_PEC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "flussoAD_type", propOrder = {
    "dataMessaggio",
    "identificativoMessaggio",
    "identificatoreProcesso",
    "tipoMessaggio",
    "identificatorePEC"
})
public class FlussoADType {

    @XmlElement(name = "DataMessaggio", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataMessaggio;
    @XmlElement(name = "IdentificativoMessaggio", required = true)
    protected String identificativoMessaggio;
    @XmlElement(name = "IdentificatoreProcesso", required = true)
    protected String identificatoreProcesso;
    @XmlElement(required = true)
    protected TipoMessaggioADType tipoMessaggio;
    @XmlElement(name = "Identificatore_PEC")
    protected String identificatorePEC;

    /**
     * Gets the value of the dataMessaggio property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataMessaggio() {
        return dataMessaggio;
    }

    /**
     * Sets the value of the dataMessaggio property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataMessaggio(XMLGregorianCalendar value) {
        this.dataMessaggio = value;
    }

    /**
     * Gets the value of the identificativoMessaggio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificativoMessaggio() {
        return identificativoMessaggio;
    }

    /**
     * Sets the value of the identificativoMessaggio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificativoMessaggio(String value) {
        this.identificativoMessaggio = value;
    }

    /**
     * Gets the value of the identificatoreProcesso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificatoreProcesso() {
        return identificatoreProcesso;
    }

    /**
     * Sets the value of the identificatoreProcesso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificatoreProcesso(String value) {
        this.identificatoreProcesso = value;
    }

    /**
     * Gets the value of the tipoMessaggio property.
     * 
     * @return
     *     possible object is
     *     {@link TipoMessaggioADType }
     *     
     */
    public TipoMessaggioADType getTipoMessaggio() {
        return tipoMessaggio;
    }

    /**
     * Sets the value of the tipoMessaggio property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoMessaggioADType }
     *     
     */
    public void setTipoMessaggio(TipoMessaggioADType value) {
        this.tipoMessaggio = value;
    }

    /**
     * Gets the value of the identificatorePEC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificatorePEC() {
        return identificatorePEC;
    }

    /**
     * Sets the value of the identificatorePEC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificatorePEC(String value) {
        this.identificatorePEC = value;
    }

}
