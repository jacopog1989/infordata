
package internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiesta creazione del fascicolo AttoDecreto
 * 
 * <p>Java class for richiesta_copyFascicoloAttoDecreto_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_copyFascicoloAttoDecreto_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicoloAttoDecreto" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid" minOccurs="0"/>
 *         &lt;element name="IdFascicoloAttoDecretoOrigine" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}Guid"/>
 *         &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DatiFascicolo" type="{http://mef.gov.it.v1.demdec.internal/servizi/InterfacciaDocumentaleDEMBILTypes_1}fascicoloMetadataAttoDecreto_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_copyFascicoloAttoDecreto_type", propOrder = {
    "idFascicoloAttoDecreto",
    "idFascicoloAttoDecretoOrigine",
    "descrizione",
    "datiFascicolo"
})
public class RichiestaCopyFascicoloAttoDecretoType {

    @XmlElement(name = "IdFascicoloAttoDecreto")
    protected String idFascicoloAttoDecreto;
    @XmlElement(name = "IdFascicoloAttoDecretoOrigine", required = true)
    protected String idFascicoloAttoDecretoOrigine;
    @XmlElement(name = "Descrizione")
    protected String descrizione;
    @XmlElement(name = "DatiFascicolo")
    protected FascicoloMetadataAttoDecretoType datiFascicolo;

    /**
     * Gets the value of the idFascicoloAttoDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloAttoDecreto() {
        return idFascicoloAttoDecreto;
    }

    /**
     * Sets the value of the idFascicoloAttoDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloAttoDecreto(String value) {
        this.idFascicoloAttoDecreto = value;
    }

    /**
     * Gets the value of the idFascicoloAttoDecretoOrigine property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloAttoDecretoOrigine() {
        return idFascicoloAttoDecretoOrigine;
    }

    /**
     * Sets the value of the idFascicoloAttoDecretoOrigine property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloAttoDecretoOrigine(String value) {
        this.idFascicoloAttoDecretoOrigine = value;
    }

    /**
     * Gets the value of the descrizione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Sets the value of the descrizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Gets the value of the datiFascicolo property.
     * 
     * @return
     *     possible object is
     *     {@link FascicoloMetadataAttoDecretoType }
     *     
     */
    public FascicoloMetadataAttoDecretoType getDatiFascicolo() {
        return datiFascicolo;
    }

    /**
     * Sets the value of the datiFascicolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link FascicoloMetadataAttoDecretoType }
     *     
     */
    public void setDatiFascicolo(FascicoloMetadataAttoDecretoType value) {
        this.datiFascicolo = value;
    }

}
