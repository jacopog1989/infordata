
package com.siebel.xml.mef.sbl_rds_create_from_ws.dettagliords.io.data;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.siebel.xml.mef.sbl_rds_create_from_ws.dettagliords.io.data package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DETTAGLIORDS_QNAME = new QName("http://www.siebel.com/xml/MEF SBL_RDS_CREATE_FROM_WS DettaglioRDS IO/Data", "DETTAGLIO_RDS");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.siebel.xml.mef.sbl_rds_create_from_ws.dettagliords.io.data
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DETTAGLIORDSData }
     * 
     */
    public DETTAGLIORDSData createDETTAGLIORDSData() {
        return new DETTAGLIORDSData();
    }

    /**
     * Create an instance of {@link DETTAGLIOALLEGATOData }
     * 
     */
    public DETTAGLIOALLEGATOData createDETTAGLIOALLEGATOData() {
        return new DETTAGLIOALLEGATOData();
    }

    /**
     * Create an instance of {@link ALLEGATIData }
     * 
     */
    public ALLEGATIData createALLEGATIData() {
        return new ALLEGATIData();
    }

    /**
     * Create an instance of {@link DETTAGLIORDSTopElmtData }
     * 
     */
    public DETTAGLIORDSTopElmtData createDETTAGLIORDSTopElmtData() {
        return new DETTAGLIORDSTopElmtData();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DETTAGLIORDSData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.siebel.com/xml/MEF SBL_RDS_CREATE_FROM_WS DettaglioRDS IO/Data", name = "DETTAGLIO_RDS")
    public JAXBElement<DETTAGLIORDSData> createDETTAGLIORDS(DETTAGLIORDSData value) {
        return new JAXBElement<DETTAGLIORDSData>(_DETTAGLIORDS_QNAME, DETTAGLIORDSData.class, null, value);
    }

}
