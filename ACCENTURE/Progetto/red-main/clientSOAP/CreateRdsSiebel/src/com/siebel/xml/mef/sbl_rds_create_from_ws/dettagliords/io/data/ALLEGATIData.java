
package com.siebel.xml.mef.sbl_rds_create_from_ws.dettagliords.io.data;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per ALLEGATIData complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="ALLEGATIData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DETTAGLIO_ALLEGATO" type="{http://www.siebel.com/xml/MEF SBL_RDS_CREATE_FROM_WS DettaglioRDS IO/Data}DETTAGLIO_ALLEGATOData" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ALLEGATIData", propOrder = {
    "dettaglioallegato"
})
public class ALLEGATIData {

    @XmlElement(name = "DETTAGLIO_ALLEGATO")
    protected List<DETTAGLIOALLEGATOData> dettaglioallegato;

    /**
     * Gets the value of the dettaglioallegato property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dettaglioallegato property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDETTAGLIOALLEGATO().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DETTAGLIOALLEGATOData }
     * 
     * 
     */
    public List<DETTAGLIOALLEGATOData> getDETTAGLIOALLEGATO() {
        if (dettaglioallegato == null) {
            dettaglioallegato = new ArrayList<DETTAGLIOALLEGATOData>();
        }
        return this.dettaglioallegato;
    }

}
