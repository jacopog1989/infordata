
package com.siebel.xml.mef.sbl_rds_create_from_ws.dettagliords.io.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per DETTAGLIO_RDSData complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="DETTAGLIO_RDSData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SISTEMA_ESTERNO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ID_SISTEMA_ESTERNO_1" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ID_SISTEMA_ESTERNO_2" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ID_CONTATTO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="EMAIL_SISTEMA_ESTERNO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TITOLO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DESCRIZIONE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ID_GRUPPO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ALLEGATI" type="{http://www.siebel.com/xml/MEF SBL_RDS_CREATE_FROM_WS DettaglioRDS IO/Data}ALLEGATIData" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DETTAGLIO_RDSData", propOrder = {
    "tid",
    "sistemaesterno",
    "idsistemaesterno1",
    "idsistemaesterno2",
    "idcontatto",
    "emailsistemaesterno",
    "titolo",
    "descrizione",
    "idgruppo",
    "allegati"
})
public class DETTAGLIORDSData {

    @XmlElement(name = "TID", required = true)
    protected String tid;
    @XmlElement(name = "SISTEMA_ESTERNO", required = true)
    protected String sistemaesterno;
    @XmlElement(name = "ID_SISTEMA_ESTERNO_1")
    protected int idsistemaesterno1;
    @XmlElement(name = "ID_SISTEMA_ESTERNO_2")
    protected int idsistemaesterno2;
    @XmlElement(name = "ID_CONTATTO", required = true)
    protected String idcontatto;
    @XmlElement(name = "EMAIL_SISTEMA_ESTERNO")
    protected String emailsistemaesterno;
    @XmlElement(name = "TITOLO", required = true)
    protected String titolo;
    @XmlElement(name = "DESCRIZIONE", required = true)
    protected String descrizione;
    @XmlElement(name = "ID_GRUPPO", required = true)
    protected String idgruppo;
    @XmlElement(name = "ALLEGATI")
    protected ALLEGATIData allegati;

    /**
     * Recupera il valore della proprietà tid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTID() {
        return tid;
    }

    /**
     * Imposta il valore della proprietà tid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTID(String value) {
        this.tid = value;
    }

    /**
     * Recupera il valore della proprietà sistemaesterno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSISTEMAESTERNO() {
        return sistemaesterno;
    }

    /**
     * Imposta il valore della proprietà sistemaesterno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSISTEMAESTERNO(String value) {
        this.sistemaesterno = value;
    }

    /**
     * Recupera il valore della proprietà idsistemaesterno1.
     * 
     */
    public int getIDSISTEMAESTERNO1() {
        return idsistemaesterno1;
    }

    /**
     * Imposta il valore della proprietà idsistemaesterno1.
     * 
     */
    public void setIDSISTEMAESTERNO1(int value) {
        this.idsistemaesterno1 = value;
    }

    /**
     * Recupera il valore della proprietà idsistemaesterno2.
     * 
     */
    public int getIDSISTEMAESTERNO2() {
        return idsistemaesterno2;
    }

    /**
     * Imposta il valore della proprietà idsistemaesterno2.
     * 
     */
    public void setIDSISTEMAESTERNO2(int value) {
        this.idsistemaesterno2 = value;
    }

    /**
     * Recupera il valore della proprietà idcontatto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDCONTATTO() {
        return idcontatto;
    }

    /**
     * Imposta il valore della proprietà idcontatto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDCONTATTO(String value) {
        this.idcontatto = value;
    }

    /**
     * Recupera il valore della proprietà emailsistemaesterno.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMAILSISTEMAESTERNO() {
        return emailsistemaesterno;
    }

    /**
     * Imposta il valore della proprietà emailsistemaesterno.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMAILSISTEMAESTERNO(String value) {
        this.emailsistemaesterno = value;
    }

    /**
     * Recupera il valore della proprietà titolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTITOLO() {
        return titolo;
    }

    /**
     * Imposta il valore della proprietà titolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTITOLO(String value) {
        this.titolo = value;
    }

    /**
     * Recupera il valore della proprietà descrizione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESCRIZIONE() {
        return descrizione;
    }

    /**
     * Imposta il valore della proprietà descrizione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESCRIZIONE(String value) {
        this.descrizione = value;
    }

    /**
     * Recupera il valore della proprietà idgruppo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDGRUPPO() {
        return idgruppo;
    }

    /**
     * Imposta il valore della proprietà idgruppo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDGRUPPO(String value) {
        this.idgruppo = value;
    }

    /**
     * Recupera il valore della proprietà allegati.
     * 
     * @return
     *     possible object is
     *     {@link ALLEGATIData }
     *     
     */
    public ALLEGATIData getALLEGATI() {
        return allegati;
    }

    /**
     * Imposta il valore della proprietà allegati.
     * 
     * @param value
     *     allowed object is
     *     {@link ALLEGATIData }
     *     
     */
    public void setALLEGATI(ALLEGATIData value) {
        this.allegati = value;
    }

}
