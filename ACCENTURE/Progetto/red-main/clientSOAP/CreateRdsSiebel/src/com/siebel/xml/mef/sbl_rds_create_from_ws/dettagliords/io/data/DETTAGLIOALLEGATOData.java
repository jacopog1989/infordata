
package com.siebel.xml.mef.sbl_rds_create_from_ws.dettagliords.io.data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per DETTAGLIO_ALLEGATOData complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="DETTAGLIO_ALLEGATOData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ALLEGATO_ATTACHMENTNAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ALLEGATO_ATTACHMENTEXT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ALLEGATO_ATTACHMENTDATA" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *         &lt;element name="ALLEGATO_ATTACHMENTORIGSIZE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DETTAGLIO_ALLEGATOData", propOrder = {
    "allegatoattachmentname",
    "allegatoattachmentext",
    "allegatoattachmentdata",
    "allegatoattachmentorigsize"
})
public class DETTAGLIOALLEGATOData {

    @XmlElement(name = "ALLEGATO_ATTACHMENTNAME", required = true)
    protected String allegatoattachmentname;
    @XmlElement(name = "ALLEGATO_ATTACHMENTEXT", required = true)
    protected String allegatoattachmentext;
    @XmlElement(name = "ALLEGATO_ATTACHMENTDATA", required = true)
    protected byte[] allegatoattachmentdata;
    @XmlElement(name = "ALLEGATO_ATTACHMENTORIGSIZE")
    protected String allegatoattachmentorigsize;

    /**
     * Recupera il valore della proprietà allegatoattachmentname.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getALLEGATOATTACHMENTNAME() {
        return allegatoattachmentname;
    }

    /**
     * Imposta il valore della proprietà allegatoattachmentname.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setALLEGATOATTACHMENTNAME(String value) {
        this.allegatoattachmentname = value;
    }

    /**
     * Recupera il valore della proprietà allegatoattachmentext.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getALLEGATOATTACHMENTEXT() {
        return allegatoattachmentext;
    }

    /**
     * Imposta il valore della proprietà allegatoattachmentext.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setALLEGATOATTACHMENTEXT(String value) {
        this.allegatoattachmentext = value;
    }

    /**
     * Recupera il valore della proprietà allegatoattachmentdata.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getALLEGATOATTACHMENTDATA() {
        return allegatoattachmentdata;
    }

    /**
     * Imposta il valore della proprietà allegatoattachmentdata.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setALLEGATOATTACHMENTDATA(byte[] value) {
        this.allegatoattachmentdata = value;
    }

    /**
     * Recupera il valore della proprietà allegatoattachmentorigsize.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getALLEGATOATTACHMENTORIGSIZE() {
        return allegatoattachmentorigsize;
    }

    /**
     * Imposta il valore della proprietà allegatoattachmentorigsize.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setALLEGATOATTACHMENTORIGSIZE(String value) {
        this.allegatoattachmentorigsize = value;
    }

}
