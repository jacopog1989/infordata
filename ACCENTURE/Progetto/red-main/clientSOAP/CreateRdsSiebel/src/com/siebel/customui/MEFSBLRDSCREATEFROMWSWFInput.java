
package com.siebel.customui;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.siebel.xml.mef.sbl_rds_create_from_ws.dettagliords.io.data.DETTAGLIORDSData;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.siebel.com/xml/MEF SBL_RDS_CREATE_FROM_WS DettaglioRDS IO/Data}DETTAGLIO_RDS"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dettagliords"
})
@XmlRootElement(name = "MEF_SBL_RDS_CREATE_FROM_WS_WF_Input")
public class MEFSBLRDSCREATEFROMWSWFInput {

    @XmlElement(name = "DETTAGLIO_RDS", namespace = "http://www.siebel.com/xml/MEF SBL_RDS_CREATE_FROM_WS DettaglioRDS IO/Data", required = true)
    protected DETTAGLIORDSData dettagliords;

    /**
     * Recupera il valore della proprietà dettagliords.
     * 
     * @return
     *     possible object is
     *     {@link DETTAGLIORDSData }
     *     
     */
    public DETTAGLIORDSData getDETTAGLIORDS() {
        return dettagliords;
    }

    /**
     * Imposta il valore della proprietà dettagliords.
     * 
     * @param value
     *     allowed object is
     *     {@link DETTAGLIORDSData }
     *     
     */
    public void setDETTAGLIORDS(DETTAGLIORDSData value) {
        this.dettagliords = value;
    }

}
