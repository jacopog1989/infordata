
package com.siebel.customui;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CODICE_ESITO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DESCRIZIONE_ESITO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DT_APERTURA_RDS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ID_SIEBEL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ID_SISTEMA_ESTERNO_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ID_SISTEMA_ESTERNO_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "codiceesito",
    "descrizioneesito",
    "dtaperturards",
    "idsiebel",
    "idsistemaesterno1",
    "idsistemaesterno2",
    "tid"
})
@XmlRootElement(name = "MEF_SBL_RDS_CREATE_FROM_WS_WF_Output")
public class MEFSBLRDSCREATEFROMWSWFOutput {

    @XmlElement(name = "CODICE_ESITO", required = true)
    protected String codiceesito;
    @XmlElement(name = "DESCRIZIONE_ESITO", required = true)
    protected String descrizioneesito;
    @XmlElement(name = "DT_APERTURA_RDS", required = true)
    protected String dtaperturards;
    @XmlElement(name = "ID_SIEBEL", required = true)
    protected String idsiebel;
    @XmlElement(name = "ID_SISTEMA_ESTERNO_1", required = true)
    protected String idsistemaesterno1;
    @XmlElement(name = "ID_SISTEMA_ESTERNO_2", required = true)
    protected String idsistemaesterno2;
    @XmlElement(name = "TID", required = true)
    protected String tid;

    /**
     * Recupera il valore della proprietà codiceesito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODICEESITO() {
        return codiceesito;
    }

    /**
     * Imposta il valore della proprietà codiceesito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODICEESITO(String value) {
        this.codiceesito = value;
    }

    /**
     * Recupera il valore della proprietà descrizioneesito.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESCRIZIONEESITO() {
        return descrizioneesito;
    }

    /**
     * Imposta il valore della proprietà descrizioneesito.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESCRIZIONEESITO(String value) {
        this.descrizioneesito = value;
    }

    /**
     * Recupera il valore della proprietà dtaperturards.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDTAPERTURARDS() {
        return dtaperturards;
    }

    /**
     * Imposta il valore della proprietà dtaperturards.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDTAPERTURARDS(String value) {
        this.dtaperturards = value;
    }

    /**
     * Recupera il valore della proprietà idsiebel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDSIEBEL() {
        return idsiebel;
    }

    /**
     * Imposta il valore della proprietà idsiebel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDSIEBEL(String value) {
        this.idsiebel = value;
    }

    /**
     * Recupera il valore della proprietà idsistemaesterno1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDSISTEMAESTERNO1() {
        return idsistemaesterno1;
    }

    /**
     * Imposta il valore della proprietà idsistemaesterno1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDSISTEMAESTERNO1(String value) {
        this.idsistemaesterno1 = value;
    }

    /**
     * Recupera il valore della proprietà idsistemaesterno2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDSISTEMAESTERNO2() {
        return idsistemaesterno2;
    }

    /**
     * Imposta il valore della proprietà idsistemaesterno2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDSISTEMAESTERNO2(String value) {
        this.idsistemaesterno2 = value;
    }

    /**
     * Recupera il valore della proprietà tid.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTID() {
        return tid;
    }

    /**
     * Imposta il valore della proprietà tid.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTID(String value) {
        this.tid = value;
    }

}
