
package it.sogei.gestionedocumentale.documentale.ejbprotocollo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the it.sogei.gestionedocumentale.documentale.ejbprotocollo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TRASA0042FU001GetVoidXMLDocEsteso_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_getVoidXMLDocEsteso");
    private final static QName _ServiceException_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "ServiceException");
    private final static QName _TRASA0042FU001GetFascicoliFromSegnatura_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_getFascicoliFromSegnatura");
    private final static QName _TRASA0042FU001GetVoidXMLDocEstesoResponse_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_getVoidXMLDocEstesoResponse");
    private final static QName _TRASA0042FU001SetDatiAggiuntiviResponse_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_setDatiAggiuntiviResponse");
    private final static QName _TRASA0042FU001InsertDocumentoByProtocolloResponse_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_insertDocumentoByProtocolloResponse");
    private final static QName _TRASA0042FU001DesettaFlagInPratica_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_desettaFlagInPratica");
    private final static QName _MetadatoBean_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "metadatoBean");
    private final static QName _TRASA0042FU001Ricerca_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_ricerca");
    private final static QName _TRASA0042FU001GetProtocolloById_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_getProtocolloById");
    private final static QName _RicercaRegistroBeanWeb_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "ricercaRegistroBeanWeb");
    private final static QName _LoginException_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "LoginException");
    private final static QName _TRASA0042FU001RicercaProtocolloConParametri_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_ricercaProtocolloConParametri");
    private final static QName _TRASA0042FU001Protocolla_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_protocolla");
    private final static QName _TRASA0042FU001ProtocollaResponse_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_protocollaResponse");
    private final static QName _TRASA0042FU001GetProtoCollegatiFromSegnatura_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_getProtoCollegatiFromSegnatura");
    private final static QName _TRASA0042FU001GetProtocolloByIdResponse_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_getProtocolloByIdResponse");
    private final static QName _TRASA0042FU001ProtocollaDocEsteso_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_protocollaDocEsteso");
    private final static QName _TRASA0042FU001SettaFlagInPraticaResponse_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_settaFlagInPraticaResponse");
    private final static QName _TRASA0042FU001RicercaProtocolloConParametriResponse_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_ricercaProtocolloConParametriResponse");
    private final static QName _TRASA0042FU001ProtocollaB2B_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_protocollaB2B");
    private final static QName _TRASA0042FU001InsertDocumentoByProtocollo_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_insertDocumentoByProtocollo");
    private final static QName _TRASA0042FU001SettaFlagInPratica_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_settaFlagInPratica");
    private final static QName _TRASA0042FU001DesettaFlagInPraticaResponse_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_desettaFlagInPraticaResponse");
    private final static QName _TRASA0042FU001ProtocollaB2BResponse_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_protocollaB2BResponse");
    private final static QName _TRASA0042FU001GetFascicoliFromSegnaturaResponse_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_getFascicoliFromSegnaturaResponse");
    private final static QName _TRASA0042FU001GetProtoCollegatiFromSegnaturaResponse_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_getProtoCollegatiFromSegnaturaResponse");
    private final static QName _TRASA0042FU001RicercaResponse_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_ricercaResponse");
    private final static QName _ProtocollazioneBean_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "protocollazioneBean");
    private final static QName _TRASA0042FU001CountRicercaProtocolloConParametriResponse_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_countRicercaProtocolloConParametriResponse");
    private final static QName _TRASA0042FU001CountRicercaProtocolloConParametri_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_countRicercaProtocolloConParametri");
    private final static QName _TRASA0042FU001AggiornaProtocollo_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_aggiornaProtocollo");
    private final static QName _TRASA0042FU001UpdateMetadatiProtocollo_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_updateMetadatiProtocollo");
    private final static QName _TRASA0042FU001AnnullaProtocollazioneResponse_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_annullaProtocollazioneResponse");
    private final static QName _TRASA0042FU001SetDatiAggiuntivi_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_setDatiAggiuntivi");
    private final static QName _TRASA0042FU001AnnullaProtocollazione_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_annullaProtocollazione");
    private final static QName _TRASA0042FU001ProtocollaDocEstesoResponse_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_protocollaDocEstesoResponse");
    private final static QName _TRASA0042FU001UpdateMetadatiProtocolloResponse_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_updateMetadatiProtocolloResponse");
    private final static QName _TRASA0042FU001GetProtocollo_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_getProtocollo");
    private final static QName _TRASA0042FU001GetProtocolloResponse_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_getProtocolloResponse");
    private final static QName _TRASA0042FU001AggiornaProtocolloResponse_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "TRA-SA-0042_FU_001_aggiornaProtocolloResponse");
    private final static QName _RicercaVuotaException_QNAME = new QName("http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", "RicercaVuotaException");
    private final static QName _TRASA0042FU001ProtocollaDocEstesoArg7_QNAME = new QName("", "arg7");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: it.sogei.gestionedocumentale.documentale.ejbprotocollo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RisultatiRicercaBean }
     * 
     */
    public RisultatiRicercaBean createRisultatiRicercaBean() {
        return new RisultatiRicercaBean();
    }

    /**
     * Create an instance of {@link RisultatiRicercaBean.HashUtentiAttivita }
     * 
     */
    public RisultatiRicercaBean.HashUtentiAttivita createRisultatiRicercaBeanHashUtentiAttivita() {
        return new RisultatiRicercaBean.HashUtentiAttivita();
    }

    /**
     * Create an instance of {@link RisultatiRicercaBean.HashUfficiAttivita }
     * 
     */
    public RisultatiRicercaBean.HashUfficiAttivita createRisultatiRicercaBeanHashUfficiAttivita() {
        return new RisultatiRicercaBean.HashUfficiAttivita();
    }

    /**
     * Create an instance of {@link SpeedActionForm }
     * 
     */
    public SpeedActionForm createSpeedActionForm() {
        return new SpeedActionForm();
    }

    /**
     * Create an instance of {@link SpeedActionForm.WarningMessages }
     * 
     */
    public SpeedActionForm.WarningMessages createSpeedActionFormWarningMessages() {
        return new SpeedActionForm.WarningMessages();
    }

    /**
     * Create an instance of {@link SpeedActionForm.ErrorMessages }
     * 
     */
    public SpeedActionForm.ErrorMessages createSpeedActionFormErrorMessages() {
        return new SpeedActionForm.ErrorMessages();
    }

    /**
     * Create an instance of {@link SpeedProtocolloBean }
     * 
     */
    public SpeedProtocolloBean createSpeedProtocolloBean() {
        return new SpeedProtocolloBean();
    }

    /**
     * Create an instance of {@link SpeedProtocolloBean.ValuesAttrRegAux }
     * 
     */
    public SpeedProtocolloBean.ValuesAttrRegAux createSpeedProtocolloBeanValuesAttrRegAux() {
        return new SpeedProtocolloBean.ValuesAttrRegAux();
    }

    /**
     * Create an instance of {@link SpeedProtocolloBean.ValuesAttrInvioCDC }
     * 
     */
    public SpeedProtocolloBean.ValuesAttrInvioCDC createSpeedProtocolloBeanValuesAttrInvioCDC() {
        return new SpeedProtocolloBean.ValuesAttrInvioCDC();
    }

    /**
     * Create an instance of {@link SpeedProtocolloBean.ValuesAttrExt }
     * 
     */
    public SpeedProtocolloBean.ValuesAttrExt createSpeedProtocolloBeanValuesAttrExt() {
        return new SpeedProtocolloBean.ValuesAttrExt();
    }

    /**
     * Create an instance of {@link TRASA0042FU001GetProtocolloResponse }
     * 
     */
    public TRASA0042FU001GetProtocolloResponse createTRASA0042FU001GetProtocolloResponse() {
        return new TRASA0042FU001GetProtocolloResponse();
    }

    /**
     * Create an instance of {@link TRASA0042FU001GetProtocollo }
     * 
     */
    public TRASA0042FU001GetProtocollo createTRASA0042FU001GetProtocollo() {
        return new TRASA0042FU001GetProtocollo();
    }

    /**
     * Create an instance of {@link TRASA0042FU001UpdateMetadatiProtocolloResponse }
     * 
     */
    public TRASA0042FU001UpdateMetadatiProtocolloResponse createTRASA0042FU001UpdateMetadatiProtocolloResponse() {
        return new TRASA0042FU001UpdateMetadatiProtocolloResponse();
    }

    /**
     * Create an instance of {@link TRASA0042FU001AnnullaProtocollazione }
     * 
     */
    public TRASA0042FU001AnnullaProtocollazione createTRASA0042FU001AnnullaProtocollazione() {
        return new TRASA0042FU001AnnullaProtocollazione();
    }

    /**
     * Create an instance of {@link TRASA0042FU001ProtocollaDocEstesoResponse }
     * 
     */
    public TRASA0042FU001ProtocollaDocEstesoResponse createTRASA0042FU001ProtocollaDocEstesoResponse() {
        return new TRASA0042FU001ProtocollaDocEstesoResponse();
    }

    /**
     * Create an instance of {@link RicercaVuotaException }
     * 
     */
    public RicercaVuotaException createRicercaVuotaException() {
        return new RicercaVuotaException();
    }

    /**
     * Create an instance of {@link TRASA0042FU001AggiornaProtocolloResponse }
     * 
     */
    public TRASA0042FU001AggiornaProtocolloResponse createTRASA0042FU001AggiornaProtocolloResponse() {
        return new TRASA0042FU001AggiornaProtocolloResponse();
    }

    /**
     * Create an instance of {@link ProtocollazioneBean }
     * 
     */
    public ProtocollazioneBean createProtocollazioneBean() {
        return new ProtocollazioneBean();
    }

    /**
     * Create an instance of {@link TRASA0042FU001GetFascicoliFromSegnaturaResponse }
     * 
     */
    public TRASA0042FU001GetFascicoliFromSegnaturaResponse createTRASA0042FU001GetFascicoliFromSegnaturaResponse() {
        return new TRASA0042FU001GetFascicoliFromSegnaturaResponse();
    }

    /**
     * Create an instance of {@link TRASA0042FU001GetProtoCollegatiFromSegnaturaResponse }
     * 
     */
    public TRASA0042FU001GetProtoCollegatiFromSegnaturaResponse createTRASA0042FU001GetProtoCollegatiFromSegnaturaResponse() {
        return new TRASA0042FU001GetProtoCollegatiFromSegnaturaResponse();
    }

    /**
     * Create an instance of {@link TRASA0042FU001RicercaResponse }
     * 
     */
    public TRASA0042FU001RicercaResponse createTRASA0042FU001RicercaResponse() {
        return new TRASA0042FU001RicercaResponse();
    }

    /**
     * Create an instance of {@link TRASA0042FU001DesettaFlagInPraticaResponse }
     * 
     */
    public TRASA0042FU001DesettaFlagInPraticaResponse createTRASA0042FU001DesettaFlagInPraticaResponse() {
        return new TRASA0042FU001DesettaFlagInPraticaResponse();
    }

    /**
     * Create an instance of {@link TRASA0042FU001ProtocollaB2BResponse }
     * 
     */
    public TRASA0042FU001ProtocollaB2BResponse createTRASA0042FU001ProtocollaB2BResponse() {
        return new TRASA0042FU001ProtocollaB2BResponse();
    }

    /**
     * Create an instance of {@link TRASA0042FU001SettaFlagInPratica }
     * 
     */
    public TRASA0042FU001SettaFlagInPratica createTRASA0042FU001SettaFlagInPratica() {
        return new TRASA0042FU001SettaFlagInPratica();
    }

    /**
     * Create an instance of {@link TRASA0042FU001SetDatiAggiuntivi }
     * 
     */
    public TRASA0042FU001SetDatiAggiuntivi createTRASA0042FU001SetDatiAggiuntivi() {
        return new TRASA0042FU001SetDatiAggiuntivi();
    }

    /**
     * Create an instance of {@link TRASA0042FU001AnnullaProtocollazioneResponse }
     * 
     */
    public TRASA0042FU001AnnullaProtocollazioneResponse createTRASA0042FU001AnnullaProtocollazioneResponse() {
        return new TRASA0042FU001AnnullaProtocollazioneResponse();
    }

    /**
     * Create an instance of {@link TRASA0042FU001AggiornaProtocollo }
     * 
     */
    public TRASA0042FU001AggiornaProtocollo createTRASA0042FU001AggiornaProtocollo() {
        return new TRASA0042FU001AggiornaProtocollo();
    }

    /**
     * Create an instance of {@link TRASA0042FU001UpdateMetadatiProtocollo }
     * 
     */
    public TRASA0042FU001UpdateMetadatiProtocollo createTRASA0042FU001UpdateMetadatiProtocollo() {
        return new TRASA0042FU001UpdateMetadatiProtocollo();
    }

    /**
     * Create an instance of {@link TRASA0042FU001CountRicercaProtocolloConParametri }
     * 
     */
    public TRASA0042FU001CountRicercaProtocolloConParametri createTRASA0042FU001CountRicercaProtocolloConParametri() {
        return new TRASA0042FU001CountRicercaProtocolloConParametri();
    }

    /**
     * Create an instance of {@link TRASA0042FU001CountRicercaProtocolloConParametriResponse }
     * 
     */
    public TRASA0042FU001CountRicercaProtocolloConParametriResponse createTRASA0042FU001CountRicercaProtocolloConParametriResponse() {
        return new TRASA0042FU001CountRicercaProtocolloConParametriResponse();
    }

    /**
     * Create an instance of {@link TRASA0042FU001SettaFlagInPraticaResponse }
     * 
     */
    public TRASA0042FU001SettaFlagInPraticaResponse createTRASA0042FU001SettaFlagInPraticaResponse() {
        return new TRASA0042FU001SettaFlagInPraticaResponse();
    }

    /**
     * Create an instance of {@link TRASA0042FU001GetProtocolloByIdResponse }
     * 
     */
    public TRASA0042FU001GetProtocolloByIdResponse createTRASA0042FU001GetProtocolloByIdResponse() {
        return new TRASA0042FU001GetProtocolloByIdResponse();
    }

    /**
     * Create an instance of {@link TRASA0042FU001ProtocollaDocEsteso }
     * 
     */
    public TRASA0042FU001ProtocollaDocEsteso createTRASA0042FU001ProtocollaDocEsteso() {
        return new TRASA0042FU001ProtocollaDocEsteso();
    }

    /**
     * Create an instance of {@link TRASA0042FU001GetProtoCollegatiFromSegnatura }
     * 
     */
    public TRASA0042FU001GetProtoCollegatiFromSegnatura createTRASA0042FU001GetProtoCollegatiFromSegnatura() {
        return new TRASA0042FU001GetProtoCollegatiFromSegnatura();
    }

    /**
     * Create an instance of {@link TRASA0042FU001Protocolla }
     * 
     */
    public TRASA0042FU001Protocolla createTRASA0042FU001Protocolla() {
        return new TRASA0042FU001Protocolla();
    }

    /**
     * Create an instance of {@link TRASA0042FU001ProtocollaResponse }
     * 
     */
    public TRASA0042FU001ProtocollaResponse createTRASA0042FU001ProtocollaResponse() {
        return new TRASA0042FU001ProtocollaResponse();
    }

    /**
     * Create an instance of {@link TRASA0042FU001RicercaProtocolloConParametri }
     * 
     */
    public TRASA0042FU001RicercaProtocolloConParametri createTRASA0042FU001RicercaProtocolloConParametri() {
        return new TRASA0042FU001RicercaProtocolloConParametri();
    }

    /**
     * Create an instance of {@link TRASA0042FU001InsertDocumentoByProtocollo }
     * 
     */
    public TRASA0042FU001InsertDocumentoByProtocollo createTRASA0042FU001InsertDocumentoByProtocollo() {
        return new TRASA0042FU001InsertDocumentoByProtocollo();
    }

    /**
     * Create an instance of {@link TRASA0042FU001ProtocollaB2B }
     * 
     */
    public TRASA0042FU001ProtocollaB2B createTRASA0042FU001ProtocollaB2B() {
        return new TRASA0042FU001ProtocollaB2B();
    }

    /**
     * Create an instance of {@link TRASA0042FU001RicercaProtocolloConParametriResponse }
     * 
     */
    public TRASA0042FU001RicercaProtocolloConParametriResponse createTRASA0042FU001RicercaProtocolloConParametriResponse() {
        return new TRASA0042FU001RicercaProtocolloConParametriResponse();
    }

    /**
     * Create an instance of {@link TRASA0042FU001InsertDocumentoByProtocolloResponse }
     * 
     */
    public TRASA0042FU001InsertDocumentoByProtocolloResponse createTRASA0042FU001InsertDocumentoByProtocolloResponse() {
        return new TRASA0042FU001InsertDocumentoByProtocolloResponse();
    }

    /**
     * Create an instance of {@link TRASA0042FU001GetVoidXMLDocEstesoResponse }
     * 
     */
    public TRASA0042FU001GetVoidXMLDocEstesoResponse createTRASA0042FU001GetVoidXMLDocEstesoResponse() {
        return new TRASA0042FU001GetVoidXMLDocEstesoResponse();
    }

    /**
     * Create an instance of {@link TRASA0042FU001SetDatiAggiuntiviResponse }
     * 
     */
    public TRASA0042FU001SetDatiAggiuntiviResponse createTRASA0042FU001SetDatiAggiuntiviResponse() {
        return new TRASA0042FU001SetDatiAggiuntiviResponse();
    }

    /**
     * Create an instance of {@link TRASA0042FU001GetFascicoliFromSegnatura }
     * 
     */
    public TRASA0042FU001GetFascicoliFromSegnatura createTRASA0042FU001GetFascicoliFromSegnatura() {
        return new TRASA0042FU001GetFascicoliFromSegnatura();
    }

    /**
     * Create an instance of {@link ServiceException }
     * 
     */
    public ServiceException createServiceException() {
        return new ServiceException();
    }

    /**
     * Create an instance of {@link TRASA0042FU001GetVoidXMLDocEsteso }
     * 
     */
    public TRASA0042FU001GetVoidXMLDocEsteso createTRASA0042FU001GetVoidXMLDocEsteso() {
        return new TRASA0042FU001GetVoidXMLDocEsteso();
    }

    /**
     * Create an instance of {@link LoginException }
     * 
     */
    public LoginException createLoginException() {
        return new LoginException();
    }

    /**
     * Create an instance of {@link RicercaRegistroBeanWeb }
     * 
     */
    public RicercaRegistroBeanWeb createRicercaRegistroBeanWeb() {
        return new RicercaRegistroBeanWeb();
    }

    /**
     * Create an instance of {@link TRASA0042FU001GetProtocolloById }
     * 
     */
    public TRASA0042FU001GetProtocolloById createTRASA0042FU001GetProtocolloById() {
        return new TRASA0042FU001GetProtocolloById();
    }

    /**
     * Create an instance of {@link TRASA0042FU001Ricerca }
     * 
     */
    public TRASA0042FU001Ricerca createTRASA0042FU001Ricerca() {
        return new TRASA0042FU001Ricerca();
    }

    /**
     * Create an instance of {@link TRASA0042FU001DesettaFlagInPratica }
     * 
     */
    public TRASA0042FU001DesettaFlagInPratica createTRASA0042FU001DesettaFlagInPratica() {
        return new TRASA0042FU001DesettaFlagInPratica();
    }

    /**
     * Create an instance of {@link FascicoloFromSegnaturaOut }
     * 
     */
    public FascicoloFromSegnaturaOut createFascicoloFromSegnaturaOut() {
        return new FascicoloFromSegnaturaOut();
    }

    /**
     * Create an instance of {@link ProtocollaType }
     * 
     */
    public ProtocollaType createProtocollaType() {
        return new ProtocollaType();
    }

    /**
     * Create an instance of {@link PaginazioneBean }
     * 
     */
    public PaginazioneBean createPaginazioneBean() {
        return new PaginazioneBean();
    }

    /**
     * Create an instance of {@link RispostaUpdateDocumentiBean }
     * 
     */
    public RispostaUpdateDocumentiBean createRispostaUpdateDocumentiBean() {
        return new RispostaUpdateDocumentiBean();
    }

    /**
     * Create an instance of {@link RequestProtocollazioneType }
     * 
     */
    public RequestProtocollazioneType createRequestProtocollazioneType() {
        return new RequestProtocollazioneType();
    }

    /**
     * Create an instance of {@link RichiestaUpdateDocumentiBean }
     * 
     */
    public RichiestaUpdateDocumentiBean createRichiestaUpdateDocumentiBean() {
        return new RichiestaUpdateDocumentiBean();
    }

    /**
     * Create an instance of {@link CollegatoFromSegnaturaIn }
     * 
     */
    public CollegatoFromSegnaturaIn createCollegatoFromSegnaturaIn() {
        return new CollegatoFromSegnaturaIn();
    }

    /**
     * Create an instance of {@link CollegatoFromSegnaturaOut }
     * 
     */
    public CollegatoFromSegnaturaOut createCollegatoFromSegnaturaOut() {
        return new CollegatoFromSegnaturaOut();
    }

    /**
     * Create an instance of {@link ResponseProtocollazioneType }
     * 
     */
    public ResponseProtocollazioneType createResponseProtocollazioneType() {
        return new ResponseProtocollazioneType();
    }

    /**
     * Create an instance of {@link SpeedMittenteDestinatario }
     * 
     */
    public SpeedMittenteDestinatario createSpeedMittenteDestinatario() {
        return new SpeedMittenteDestinatario();
    }

    /**
     * Create an instance of {@link SpeedBaseManager }
     * 
     */
    public SpeedBaseManager createSpeedBaseManager() {
        return new SpeedBaseManager();
    }

    /**
     * Create an instance of {@link AuthBean }
     * 
     */
    public AuthBean createAuthBean() {
        return new AuthBean();
    }

    /**
     * Create an instance of {@link HashMapBean }
     * 
     */
    public HashMapBean createHashMapBean() {
        return new HashMapBean();
    }

    /**
     * Create an instance of {@link SegnaturaBean }
     * 
     */
    public SegnaturaBean createSegnaturaBean() {
        return new SegnaturaBean();
    }

    /**
     * Create an instance of {@link AllegatoBean }
     * 
     */
    public AllegatoBean createAllegatoBean() {
        return new AllegatoBean();
    }

    /**
     * Create an instance of {@link FascicoloFromSegnaturaIn }
     * 
     */
    public FascicoloFromSegnaturaIn createFascicoloFromSegnaturaIn() {
        return new FascicoloFromSegnaturaIn();
    }

    /**
     * Create an instance of {@link AllegatoType }
     * 
     */
    public AllegatoType createAllegatoType() {
        return new AllegatoType();
    }

    /**
     * Create an instance of {@link SpeedMezzoSpedizione }
     * 
     */
    public SpeedMezzoSpedizione createSpeedMezzoSpedizione() {
        return new SpeedMezzoSpedizione();
    }

    /**
     * Create an instance of {@link SoggettoInteressatoType }
     * 
     */
    public SoggettoInteressatoType createSoggettoInteressatoType() {
        return new SoggettoInteressatoType();
    }

    /**
     * Create an instance of {@link SpeedTipologieDocumento }
     * 
     */
    public SpeedTipologieDocumento createSpeedTipologieDocumento() {
        return new SpeedTipologieDocumento();
    }

    /**
     * Create an instance of {@link MittenteBean }
     * 
     */
    public MittenteBean createMittenteBean() {
        return new MittenteBean();
    }

    /**
     * Create an instance of {@link DatiAggiuntiviBean }
     * 
     */
    public DatiAggiuntiviBean createDatiAggiuntiviBean() {
        return new DatiAggiuntiviBean();
    }

    /**
     * Create an instance of {@link SpeedProtocolloMessageMananger }
     * 
     */
    public SpeedProtocolloMessageMananger createSpeedProtocolloMessageMananger() {
        return new SpeedProtocolloMessageMananger();
    }

    /**
     * Create an instance of {@link MittenteDestinatarioType }
     * 
     */
    public MittenteDestinatarioType createMittenteDestinatarioType() {
        return new MittenteDestinatarioType();
    }

    /**
     * Create an instance of {@link AllegatiListType }
     * 
     */
    public AllegatiListType createAllegatiListType() {
        return new AllegatiListType();
    }

    /**
     * Create an instance of {@link RicercaRegistroBean }
     * 
     */
    public RicercaRegistroBean createRicercaRegistroBean() {
        return new RicercaRegistroBean();
    }

    /**
     * Create an instance of {@link SoggettiInteressatiListType }
     * 
     */
    public SoggettiInteressatiListType createSoggettiInteressatiListType() {
        return new SoggettiInteressatiListType();
    }

    /**
     * Create an instance of {@link MetadatiAggiuntiviBean }
     * 
     */
    public MetadatiAggiuntiviBean createMetadatiAggiuntiviBean() {
        return new MetadatiAggiuntiviBean();
    }

    /**
     * Create an instance of {@link SpeedCasellaEmail }
     * 
     */
    public SpeedCasellaEmail createSpeedCasellaEmail() {
        return new SpeedCasellaEmail();
    }

    /**
     * Create an instance of {@link AuthenticationType }
     * 
     */
    public AuthenticationType createAuthenticationType() {
        return new AuthenticationType();
    }

    /**
     * Create an instance of {@link MetadatoBean }
     * 
     */
    public MetadatoBean createMetadatoBean() {
        return new MetadatoBean();
    }

    /**
     * Create an instance of {@link RisultatiRicercaBean.HashUtentiAttivita.Entry }
     * 
     */
    public RisultatiRicercaBean.HashUtentiAttivita.Entry createRisultatiRicercaBeanHashUtentiAttivitaEntry() {
        return new RisultatiRicercaBean.HashUtentiAttivita.Entry();
    }

    /**
     * Create an instance of {@link RisultatiRicercaBean.HashUfficiAttivita.Entry }
     * 
     */
    public RisultatiRicercaBean.HashUfficiAttivita.Entry createRisultatiRicercaBeanHashUfficiAttivitaEntry() {
        return new RisultatiRicercaBean.HashUfficiAttivita.Entry();
    }

    /**
     * Create an instance of {@link SpeedActionForm.WarningMessages.Entry }
     * 
     */
    public SpeedActionForm.WarningMessages.Entry createSpeedActionFormWarningMessagesEntry() {
        return new SpeedActionForm.WarningMessages.Entry();
    }

    /**
     * Create an instance of {@link SpeedActionForm.ErrorMessages.Entry }
     * 
     */
    public SpeedActionForm.ErrorMessages.Entry createSpeedActionFormErrorMessagesEntry() {
        return new SpeedActionForm.ErrorMessages.Entry();
    }

    /**
     * Create an instance of {@link SpeedProtocolloBean.ValuesAttrRegAux.Entry }
     * 
     */
    public SpeedProtocolloBean.ValuesAttrRegAux.Entry createSpeedProtocolloBeanValuesAttrRegAuxEntry() {
        return new SpeedProtocolloBean.ValuesAttrRegAux.Entry();
    }

    /**
     * Create an instance of {@link SpeedProtocolloBean.ValuesAttrInvioCDC.Entry }
     * 
     */
    public SpeedProtocolloBean.ValuesAttrInvioCDC.Entry createSpeedProtocolloBeanValuesAttrInvioCDCEntry() {
        return new SpeedProtocolloBean.ValuesAttrInvioCDC.Entry();
    }

    /**
     * Create an instance of {@link SpeedProtocolloBean.ValuesAttrExt.Entry }
     * 
     */
    public SpeedProtocolloBean.ValuesAttrExt.Entry createSpeedProtocolloBeanValuesAttrExtEntry() {
        return new SpeedProtocolloBean.ValuesAttrExt.Entry();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001GetVoidXMLDocEsteso }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_getVoidXMLDocEsteso")
    public JAXBElement<TRASA0042FU001GetVoidXMLDocEsteso> createTRASA0042FU001GetVoidXMLDocEsteso(TRASA0042FU001GetVoidXMLDocEsteso value) {
        return new JAXBElement<TRASA0042FU001GetVoidXMLDocEsteso>(_TRASA0042FU001GetVoidXMLDocEsteso_QNAME, TRASA0042FU001GetVoidXMLDocEsteso.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "ServiceException")
    public JAXBElement<ServiceException> createServiceException(ServiceException value) {
        return new JAXBElement<ServiceException>(_ServiceException_QNAME, ServiceException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001GetFascicoliFromSegnatura }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_getFascicoliFromSegnatura")
    public JAXBElement<TRASA0042FU001GetFascicoliFromSegnatura> createTRASA0042FU001GetFascicoliFromSegnatura(TRASA0042FU001GetFascicoliFromSegnatura value) {
        return new JAXBElement<TRASA0042FU001GetFascicoliFromSegnatura>(_TRASA0042FU001GetFascicoliFromSegnatura_QNAME, TRASA0042FU001GetFascicoliFromSegnatura.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001GetVoidXMLDocEstesoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_getVoidXMLDocEstesoResponse")
    public JAXBElement<TRASA0042FU001GetVoidXMLDocEstesoResponse> createTRASA0042FU001GetVoidXMLDocEstesoResponse(TRASA0042FU001GetVoidXMLDocEstesoResponse value) {
        return new JAXBElement<TRASA0042FU001GetVoidXMLDocEstesoResponse>(_TRASA0042FU001GetVoidXMLDocEstesoResponse_QNAME, TRASA0042FU001GetVoidXMLDocEstesoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001SetDatiAggiuntiviResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_setDatiAggiuntiviResponse")
    public JAXBElement<TRASA0042FU001SetDatiAggiuntiviResponse> createTRASA0042FU001SetDatiAggiuntiviResponse(TRASA0042FU001SetDatiAggiuntiviResponse value) {
        return new JAXBElement<TRASA0042FU001SetDatiAggiuntiviResponse>(_TRASA0042FU001SetDatiAggiuntiviResponse_QNAME, TRASA0042FU001SetDatiAggiuntiviResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001InsertDocumentoByProtocolloResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_insertDocumentoByProtocolloResponse")
    public JAXBElement<TRASA0042FU001InsertDocumentoByProtocolloResponse> createTRASA0042FU001InsertDocumentoByProtocolloResponse(TRASA0042FU001InsertDocumentoByProtocolloResponse value) {
        return new JAXBElement<TRASA0042FU001InsertDocumentoByProtocolloResponse>(_TRASA0042FU001InsertDocumentoByProtocolloResponse_QNAME, TRASA0042FU001InsertDocumentoByProtocolloResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001DesettaFlagInPratica }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_desettaFlagInPratica")
    public JAXBElement<TRASA0042FU001DesettaFlagInPratica> createTRASA0042FU001DesettaFlagInPratica(TRASA0042FU001DesettaFlagInPratica value) {
        return new JAXBElement<TRASA0042FU001DesettaFlagInPratica>(_TRASA0042FU001DesettaFlagInPratica_QNAME, TRASA0042FU001DesettaFlagInPratica.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "metadatoBean")
    public JAXBElement<Object> createMetadatoBean(Object value) {
        return new JAXBElement<Object>(_MetadatoBean_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001Ricerca }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_ricerca")
    public JAXBElement<TRASA0042FU001Ricerca> createTRASA0042FU001Ricerca(TRASA0042FU001Ricerca value) {
        return new JAXBElement<TRASA0042FU001Ricerca>(_TRASA0042FU001Ricerca_QNAME, TRASA0042FU001Ricerca.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001GetProtocolloById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_getProtocolloById")
    public JAXBElement<TRASA0042FU001GetProtocolloById> createTRASA0042FU001GetProtocolloById(TRASA0042FU001GetProtocolloById value) {
        return new JAXBElement<TRASA0042FU001GetProtocolloById>(_TRASA0042FU001GetProtocolloById_QNAME, TRASA0042FU001GetProtocolloById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RicercaRegistroBeanWeb }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "ricercaRegistroBeanWeb")
    public JAXBElement<RicercaRegistroBeanWeb> createRicercaRegistroBeanWeb(RicercaRegistroBeanWeb value) {
        return new JAXBElement<RicercaRegistroBeanWeb>(_RicercaRegistroBeanWeb_QNAME, RicercaRegistroBeanWeb.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoginException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "LoginException")
    public JAXBElement<LoginException> createLoginException(LoginException value) {
        return new JAXBElement<LoginException>(_LoginException_QNAME, LoginException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001RicercaProtocolloConParametri }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_ricercaProtocolloConParametri")
    public JAXBElement<TRASA0042FU001RicercaProtocolloConParametri> createTRASA0042FU001RicercaProtocolloConParametri(TRASA0042FU001RicercaProtocolloConParametri value) {
        return new JAXBElement<TRASA0042FU001RicercaProtocolloConParametri>(_TRASA0042FU001RicercaProtocolloConParametri_QNAME, TRASA0042FU001RicercaProtocolloConParametri.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001Protocolla }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_protocolla")
    public JAXBElement<TRASA0042FU001Protocolla> createTRASA0042FU001Protocolla(TRASA0042FU001Protocolla value) {
        return new JAXBElement<TRASA0042FU001Protocolla>(_TRASA0042FU001Protocolla_QNAME, TRASA0042FU001Protocolla.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001ProtocollaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_protocollaResponse")
    public JAXBElement<TRASA0042FU001ProtocollaResponse> createTRASA0042FU001ProtocollaResponse(TRASA0042FU001ProtocollaResponse value) {
        return new JAXBElement<TRASA0042FU001ProtocollaResponse>(_TRASA0042FU001ProtocollaResponse_QNAME, TRASA0042FU001ProtocollaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001GetProtoCollegatiFromSegnatura }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_getProtoCollegatiFromSegnatura")
    public JAXBElement<TRASA0042FU001GetProtoCollegatiFromSegnatura> createTRASA0042FU001GetProtoCollegatiFromSegnatura(TRASA0042FU001GetProtoCollegatiFromSegnatura value) {
        return new JAXBElement<TRASA0042FU001GetProtoCollegatiFromSegnatura>(_TRASA0042FU001GetProtoCollegatiFromSegnatura_QNAME, TRASA0042FU001GetProtoCollegatiFromSegnatura.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001GetProtocolloByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_getProtocolloByIdResponse")
    public JAXBElement<TRASA0042FU001GetProtocolloByIdResponse> createTRASA0042FU001GetProtocolloByIdResponse(TRASA0042FU001GetProtocolloByIdResponse value) {
        return new JAXBElement<TRASA0042FU001GetProtocolloByIdResponse>(_TRASA0042FU001GetProtocolloByIdResponse_QNAME, TRASA0042FU001GetProtocolloByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001ProtocollaDocEsteso }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_protocollaDocEsteso")
    public JAXBElement<TRASA0042FU001ProtocollaDocEsteso> createTRASA0042FU001ProtocollaDocEsteso(TRASA0042FU001ProtocollaDocEsteso value) {
        return new JAXBElement<TRASA0042FU001ProtocollaDocEsteso>(_TRASA0042FU001ProtocollaDocEsteso_QNAME, TRASA0042FU001ProtocollaDocEsteso.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001SettaFlagInPraticaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_settaFlagInPraticaResponse")
    public JAXBElement<TRASA0042FU001SettaFlagInPraticaResponse> createTRASA0042FU001SettaFlagInPraticaResponse(TRASA0042FU001SettaFlagInPraticaResponse value) {
        return new JAXBElement<TRASA0042FU001SettaFlagInPraticaResponse>(_TRASA0042FU001SettaFlagInPraticaResponse_QNAME, TRASA0042FU001SettaFlagInPraticaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001RicercaProtocolloConParametriResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_ricercaProtocolloConParametriResponse")
    public JAXBElement<TRASA0042FU001RicercaProtocolloConParametriResponse> createTRASA0042FU001RicercaProtocolloConParametriResponse(TRASA0042FU001RicercaProtocolloConParametriResponse value) {
        return new JAXBElement<TRASA0042FU001RicercaProtocolloConParametriResponse>(_TRASA0042FU001RicercaProtocolloConParametriResponse_QNAME, TRASA0042FU001RicercaProtocolloConParametriResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001ProtocollaB2B }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_protocollaB2B")
    public JAXBElement<TRASA0042FU001ProtocollaB2B> createTRASA0042FU001ProtocollaB2B(TRASA0042FU001ProtocollaB2B value) {
        return new JAXBElement<TRASA0042FU001ProtocollaB2B>(_TRASA0042FU001ProtocollaB2B_QNAME, TRASA0042FU001ProtocollaB2B.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001InsertDocumentoByProtocollo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_insertDocumentoByProtocollo")
    public JAXBElement<TRASA0042FU001InsertDocumentoByProtocollo> createTRASA0042FU001InsertDocumentoByProtocollo(TRASA0042FU001InsertDocumentoByProtocollo value) {
        return new JAXBElement<TRASA0042FU001InsertDocumentoByProtocollo>(_TRASA0042FU001InsertDocumentoByProtocollo_QNAME, TRASA0042FU001InsertDocumentoByProtocollo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001SettaFlagInPratica }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_settaFlagInPratica")
    public JAXBElement<TRASA0042FU001SettaFlagInPratica> createTRASA0042FU001SettaFlagInPratica(TRASA0042FU001SettaFlagInPratica value) {
        return new JAXBElement<TRASA0042FU001SettaFlagInPratica>(_TRASA0042FU001SettaFlagInPratica_QNAME, TRASA0042FU001SettaFlagInPratica.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001DesettaFlagInPraticaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_desettaFlagInPraticaResponse")
    public JAXBElement<TRASA0042FU001DesettaFlagInPraticaResponse> createTRASA0042FU001DesettaFlagInPraticaResponse(TRASA0042FU001DesettaFlagInPraticaResponse value) {
        return new JAXBElement<TRASA0042FU001DesettaFlagInPraticaResponse>(_TRASA0042FU001DesettaFlagInPraticaResponse_QNAME, TRASA0042FU001DesettaFlagInPraticaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001ProtocollaB2BResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_protocollaB2BResponse")
    public JAXBElement<TRASA0042FU001ProtocollaB2BResponse> createTRASA0042FU001ProtocollaB2BResponse(TRASA0042FU001ProtocollaB2BResponse value) {
        return new JAXBElement<TRASA0042FU001ProtocollaB2BResponse>(_TRASA0042FU001ProtocollaB2BResponse_QNAME, TRASA0042FU001ProtocollaB2BResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001GetFascicoliFromSegnaturaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_getFascicoliFromSegnaturaResponse")
    public JAXBElement<TRASA0042FU001GetFascicoliFromSegnaturaResponse> createTRASA0042FU001GetFascicoliFromSegnaturaResponse(TRASA0042FU001GetFascicoliFromSegnaturaResponse value) {
        return new JAXBElement<TRASA0042FU001GetFascicoliFromSegnaturaResponse>(_TRASA0042FU001GetFascicoliFromSegnaturaResponse_QNAME, TRASA0042FU001GetFascicoliFromSegnaturaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001GetProtoCollegatiFromSegnaturaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_getProtoCollegatiFromSegnaturaResponse")
    public JAXBElement<TRASA0042FU001GetProtoCollegatiFromSegnaturaResponse> createTRASA0042FU001GetProtoCollegatiFromSegnaturaResponse(TRASA0042FU001GetProtoCollegatiFromSegnaturaResponse value) {
        return new JAXBElement<TRASA0042FU001GetProtoCollegatiFromSegnaturaResponse>(_TRASA0042FU001GetProtoCollegatiFromSegnaturaResponse_QNAME, TRASA0042FU001GetProtoCollegatiFromSegnaturaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001RicercaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_ricercaResponse")
    public JAXBElement<TRASA0042FU001RicercaResponse> createTRASA0042FU001RicercaResponse(TRASA0042FU001RicercaResponse value) {
        return new JAXBElement<TRASA0042FU001RicercaResponse>(_TRASA0042FU001RicercaResponse_QNAME, TRASA0042FU001RicercaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProtocollazioneBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "protocollazioneBean")
    public JAXBElement<ProtocollazioneBean> createProtocollazioneBean(ProtocollazioneBean value) {
        return new JAXBElement<ProtocollazioneBean>(_ProtocollazioneBean_QNAME, ProtocollazioneBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001CountRicercaProtocolloConParametriResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_countRicercaProtocolloConParametriResponse")
    public JAXBElement<TRASA0042FU001CountRicercaProtocolloConParametriResponse> createTRASA0042FU001CountRicercaProtocolloConParametriResponse(TRASA0042FU001CountRicercaProtocolloConParametriResponse value) {
        return new JAXBElement<TRASA0042FU001CountRicercaProtocolloConParametriResponse>(_TRASA0042FU001CountRicercaProtocolloConParametriResponse_QNAME, TRASA0042FU001CountRicercaProtocolloConParametriResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001CountRicercaProtocolloConParametri }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_countRicercaProtocolloConParametri")
    public JAXBElement<TRASA0042FU001CountRicercaProtocolloConParametri> createTRASA0042FU001CountRicercaProtocolloConParametri(TRASA0042FU001CountRicercaProtocolloConParametri value) {
        return new JAXBElement<TRASA0042FU001CountRicercaProtocolloConParametri>(_TRASA0042FU001CountRicercaProtocolloConParametri_QNAME, TRASA0042FU001CountRicercaProtocolloConParametri.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001AggiornaProtocollo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_aggiornaProtocollo")
    public JAXBElement<TRASA0042FU001AggiornaProtocollo> createTRASA0042FU001AggiornaProtocollo(TRASA0042FU001AggiornaProtocollo value) {
        return new JAXBElement<TRASA0042FU001AggiornaProtocollo>(_TRASA0042FU001AggiornaProtocollo_QNAME, TRASA0042FU001AggiornaProtocollo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001UpdateMetadatiProtocollo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_updateMetadatiProtocollo")
    public JAXBElement<TRASA0042FU001UpdateMetadatiProtocollo> createTRASA0042FU001UpdateMetadatiProtocollo(TRASA0042FU001UpdateMetadatiProtocollo value) {
        return new JAXBElement<TRASA0042FU001UpdateMetadatiProtocollo>(_TRASA0042FU001UpdateMetadatiProtocollo_QNAME, TRASA0042FU001UpdateMetadatiProtocollo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001AnnullaProtocollazioneResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_annullaProtocollazioneResponse")
    public JAXBElement<TRASA0042FU001AnnullaProtocollazioneResponse> createTRASA0042FU001AnnullaProtocollazioneResponse(TRASA0042FU001AnnullaProtocollazioneResponse value) {
        return new JAXBElement<TRASA0042FU001AnnullaProtocollazioneResponse>(_TRASA0042FU001AnnullaProtocollazioneResponse_QNAME, TRASA0042FU001AnnullaProtocollazioneResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001SetDatiAggiuntivi }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_setDatiAggiuntivi")
    public JAXBElement<TRASA0042FU001SetDatiAggiuntivi> createTRASA0042FU001SetDatiAggiuntivi(TRASA0042FU001SetDatiAggiuntivi value) {
        return new JAXBElement<TRASA0042FU001SetDatiAggiuntivi>(_TRASA0042FU001SetDatiAggiuntivi_QNAME, TRASA0042FU001SetDatiAggiuntivi.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001AnnullaProtocollazione }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_annullaProtocollazione")
    public JAXBElement<TRASA0042FU001AnnullaProtocollazione> createTRASA0042FU001AnnullaProtocollazione(TRASA0042FU001AnnullaProtocollazione value) {
        return new JAXBElement<TRASA0042FU001AnnullaProtocollazione>(_TRASA0042FU001AnnullaProtocollazione_QNAME, TRASA0042FU001AnnullaProtocollazione.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001ProtocollaDocEstesoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_protocollaDocEstesoResponse")
    public JAXBElement<TRASA0042FU001ProtocollaDocEstesoResponse> createTRASA0042FU001ProtocollaDocEstesoResponse(TRASA0042FU001ProtocollaDocEstesoResponse value) {
        return new JAXBElement<TRASA0042FU001ProtocollaDocEstesoResponse>(_TRASA0042FU001ProtocollaDocEstesoResponse_QNAME, TRASA0042FU001ProtocollaDocEstesoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001UpdateMetadatiProtocolloResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_updateMetadatiProtocolloResponse")
    public JAXBElement<TRASA0042FU001UpdateMetadatiProtocolloResponse> createTRASA0042FU001UpdateMetadatiProtocolloResponse(TRASA0042FU001UpdateMetadatiProtocolloResponse value) {
        return new JAXBElement<TRASA0042FU001UpdateMetadatiProtocolloResponse>(_TRASA0042FU001UpdateMetadatiProtocolloResponse_QNAME, TRASA0042FU001UpdateMetadatiProtocolloResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001GetProtocollo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_getProtocollo")
    public JAXBElement<TRASA0042FU001GetProtocollo> createTRASA0042FU001GetProtocollo(TRASA0042FU001GetProtocollo value) {
        return new JAXBElement<TRASA0042FU001GetProtocollo>(_TRASA0042FU001GetProtocollo_QNAME, TRASA0042FU001GetProtocollo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001GetProtocolloResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_getProtocolloResponse")
    public JAXBElement<TRASA0042FU001GetProtocolloResponse> createTRASA0042FU001GetProtocolloResponse(TRASA0042FU001GetProtocolloResponse value) {
        return new JAXBElement<TRASA0042FU001GetProtocolloResponse>(_TRASA0042FU001GetProtocolloResponse_QNAME, TRASA0042FU001GetProtocolloResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU001AggiornaProtocolloResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "TRA-SA-0042_FU_001_aggiornaProtocolloResponse")
    public JAXBElement<TRASA0042FU001AggiornaProtocolloResponse> createTRASA0042FU001AggiornaProtocolloResponse(TRASA0042FU001AggiornaProtocolloResponse value) {
        return new JAXBElement<TRASA0042FU001AggiornaProtocolloResponse>(_TRASA0042FU001AggiornaProtocolloResponse_QNAME, TRASA0042FU001AggiornaProtocolloResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RicercaVuotaException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://gestionedocumentale.sogei.it/documentale/ejbprotocollo", name = "RicercaVuotaException")
    public JAXBElement<RicercaVuotaException> createRicercaVuotaException(RicercaVuotaException value) {
        return new JAXBElement<RicercaVuotaException>(_RicercaVuotaException_QNAME, RicercaVuotaException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link byte[]}{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "arg7", scope = TRASA0042FU001ProtocollaDocEsteso.class)
    public JAXBElement<byte[]> createTRASA0042FU001ProtocollaDocEstesoArg7(byte[] value) {
        return new JAXBElement<byte[]>(_TRASA0042FU001ProtocollaDocEstesoArg7_QNAME, byte[].class, TRASA0042FU001ProtocollaDocEsteso.class, ((byte[]) value));
    }

}
