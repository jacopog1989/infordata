
package it.sogei.servizi.fascicoli.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the it.sogei.servizi.fascicoli.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://ws.fascicoli.servizi.sogei.it/", "Exception");
    private final static QName _TRASA0042FU006CountRicercaFascicoli_QNAME = new QName("http://ws.fascicoli.servizi.sogei.it/", "TRA-SA-0042_FU_006_countRicercaFascicoli");
    private final static QName _TRASA0042FU006GetFascicoloByIdResponse_QNAME = new QName("http://ws.fascicoli.servizi.sogei.it/", "TRA-SA-0042_FU_006_getFascicoloByIdResponse");
    private final static QName _TRASA0042FU006CountRicercaFascicoliResponse_QNAME = new QName("http://ws.fascicoli.servizi.sogei.it/", "TRA-SA-0042_FU_006_countRicercaFascicoliResponse");
    private final static QName _TRASA0042FU006GetFascicoloById_QNAME = new QName("http://ws.fascicoli.servizi.sogei.it/", "TRA-SA-0042_FU_006_getFascicoloById");
    private final static QName _TRASA0042FU006RicercaFascicoliResponse_QNAME = new QName("http://ws.fascicoli.servizi.sogei.it/", "TRA-SA-0042_FU_006_ricercaFascicoliResponse");
    private final static QName _TRASA0042FU006RicercaFascicoli_QNAME = new QName("http://ws.fascicoli.servizi.sogei.it/", "TRA-SA-0042_FU_006_ricercaFascicoli");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: it.sogei.servizi.fascicoli.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TRASA0042FU006RicercaFascicoliResponse }
     * 
     */
    public TRASA0042FU006RicercaFascicoliResponse createTRASA0042FU006RicercaFascicoliResponse() {
        return new TRASA0042FU006RicercaFascicoliResponse();
    }

    /**
     * Create an instance of {@link TRASA0042FU006RicercaFascicoli }
     * 
     */
    public TRASA0042FU006RicercaFascicoli createTRASA0042FU006RicercaFascicoli() {
        return new TRASA0042FU006RicercaFascicoli();
    }

    /**
     * Create an instance of {@link TRASA0042FU006GetFascicoloByIdResponse }
     * 
     */
    public TRASA0042FU006GetFascicoloByIdResponse createTRASA0042FU006GetFascicoloByIdResponse() {
        return new TRASA0042FU006GetFascicoloByIdResponse();
    }

    /**
     * Create an instance of {@link TRASA0042FU006CountRicercaFascicoliResponse }
     * 
     */
    public TRASA0042FU006CountRicercaFascicoliResponse createTRASA0042FU006CountRicercaFascicoliResponse() {
        return new TRASA0042FU006CountRicercaFascicoliResponse();
    }

    /**
     * Create an instance of {@link TRASA0042FU006GetFascicoloById }
     * 
     */
    public TRASA0042FU006GetFascicoloById createTRASA0042FU006GetFascicoloById() {
        return new TRASA0042FU006GetFascicoloById();
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link TRASA0042FU006CountRicercaFascicoli }
     * 
     */
    public TRASA0042FU006CountRicercaFascicoli createTRASA0042FU006CountRicercaFascicoli() {
        return new TRASA0042FU006CountRicercaFascicoli();
    }

    /**
     * Create an instance of {@link GetFascicolo }
     * 
     */
    public GetFascicolo createGetFascicolo() {
        return new GetFascicolo();
    }

    /**
     * Create an instance of {@link Fascicolo }
     * 
     */
    public Fascicolo createFascicolo() {
        return new Fascicolo();
    }

    /**
     * Create an instance of {@link AuthenticationBean }
     * 
     */
    public AuthenticationBean createAuthenticationBean() {
        return new AuthenticationBean();
    }

    /**
     * Create an instance of {@link Documento }
     * 
     */
    public Documento createDocumento() {
        return new Documento();
    }

    /**
     * Create an instance of {@link BeanMetadato }
     * 
     */
    public BeanMetadato createBeanMetadato() {
        return new BeanMetadato();
    }

    /**
     * Create an instance of {@link Folder }
     * 
     */
    public Folder createFolder() {
        return new Folder();
    }

    /**
     * Create an instance of {@link RicercaFascicoli }
     * 
     */
    public RicercaFascicoli createRicercaFascicoli() {
        return new RicercaFascicoli();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fascicoli.servizi.sogei.it/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU006CountRicercaFascicoli }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fascicoli.servizi.sogei.it/", name = "TRA-SA-0042_FU_006_countRicercaFascicoli")
    public JAXBElement<TRASA0042FU006CountRicercaFascicoli> createTRASA0042FU006CountRicercaFascicoli(TRASA0042FU006CountRicercaFascicoli value) {
        return new JAXBElement<TRASA0042FU006CountRicercaFascicoli>(_TRASA0042FU006CountRicercaFascicoli_QNAME, TRASA0042FU006CountRicercaFascicoli.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU006GetFascicoloByIdResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fascicoli.servizi.sogei.it/", name = "TRA-SA-0042_FU_006_getFascicoloByIdResponse")
    public JAXBElement<TRASA0042FU006GetFascicoloByIdResponse> createTRASA0042FU006GetFascicoloByIdResponse(TRASA0042FU006GetFascicoloByIdResponse value) {
        return new JAXBElement<TRASA0042FU006GetFascicoloByIdResponse>(_TRASA0042FU006GetFascicoloByIdResponse_QNAME, TRASA0042FU006GetFascicoloByIdResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU006CountRicercaFascicoliResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fascicoli.servizi.sogei.it/", name = "TRA-SA-0042_FU_006_countRicercaFascicoliResponse")
    public JAXBElement<TRASA0042FU006CountRicercaFascicoliResponse> createTRASA0042FU006CountRicercaFascicoliResponse(TRASA0042FU006CountRicercaFascicoliResponse value) {
        return new JAXBElement<TRASA0042FU006CountRicercaFascicoliResponse>(_TRASA0042FU006CountRicercaFascicoliResponse_QNAME, TRASA0042FU006CountRicercaFascicoliResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU006GetFascicoloById }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fascicoli.servizi.sogei.it/", name = "TRA-SA-0042_FU_006_getFascicoloById")
    public JAXBElement<TRASA0042FU006GetFascicoloById> createTRASA0042FU006GetFascicoloById(TRASA0042FU006GetFascicoloById value) {
        return new JAXBElement<TRASA0042FU006GetFascicoloById>(_TRASA0042FU006GetFascicoloById_QNAME, TRASA0042FU006GetFascicoloById.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU006RicercaFascicoliResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fascicoli.servizi.sogei.it/", name = "TRA-SA-0042_FU_006_ricercaFascicoliResponse")
    public JAXBElement<TRASA0042FU006RicercaFascicoliResponse> createTRASA0042FU006RicercaFascicoliResponse(TRASA0042FU006RicercaFascicoliResponse value) {
        return new JAXBElement<TRASA0042FU006RicercaFascicoliResponse>(_TRASA0042FU006RicercaFascicoliResponse_QNAME, TRASA0042FU006RicercaFascicoliResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TRASA0042FU006RicercaFascicoli }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fascicoli.servizi.sogei.it/", name = "TRA-SA-0042_FU_006_ricercaFascicoli")
    public JAXBElement<TRASA0042FU006RicercaFascicoli> createTRASA0042FU006RicercaFascicoli(TRASA0042FU006RicercaFascicoli value) {
        return new JAXBElement<TRASA0042FU006RicercaFascicoli>(_TRASA0042FU006RicercaFascicoli_QNAME, TRASA0042FU006RicercaFascicoli.class, null, value);
    }

}
