
package it.sogei.gestionedocumentale.documentale.ejbprotocollo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TRA-SA-0042_FU_001_protocollaB2B complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TRA-SA-0042_FU_001_protocollaB2B">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arg0" type="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}requestProtocollazioneType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TRA-SA-0042_FU_001_protocollaB2B", propOrder = {
    "arg0"
})
public class TRASA0042FU001ProtocollaB2B {

    protected RequestProtocollazioneType arg0;

    /**
     * Gets the value of the arg0 property.
     * 
     * @return
     *     possible object is
     *     {@link RequestProtocollazioneType }
     *     
     */
    public RequestProtocollazioneType getArg0() {
        return arg0;
    }

    /**
     * Sets the value of the arg0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestProtocollazioneType }
     *     
     */
    public void setArg0(RequestProtocollazioneType value) {
        this.arg0 = value;
    }

}
