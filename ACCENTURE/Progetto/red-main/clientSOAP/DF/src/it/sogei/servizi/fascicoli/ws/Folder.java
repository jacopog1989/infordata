
package it.sogei.servizi.fascicoli.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for folder complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="folder">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dataApertura" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="idFolderId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="listaCartelle" type="{http://ws.fascicoli.servizi.sogei.it/}folder" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="listaDocumenti" type="{http://ws.fascicoli.servizi.sogei.it/}documento" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="nomeCartella" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="iChronicleId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "folder", propOrder = {
    "dataApertura",
    "idFolderId",
    "listaCartelle",
    "listaDocumenti",
    "nomeCartella",
    "iChronicleId"
})
public class Folder {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataApertura;
    protected String idFolderId;
    @XmlElement(nillable = true)
    protected List<Folder> listaCartelle;
    @XmlElement(nillable = true)
    protected List<Documento> listaDocumenti;
    protected String nomeCartella;
    protected String iChronicleId;

    /**
     * Gets the value of the dataApertura property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataApertura() {
        return dataApertura;
    }

    /**
     * Sets the value of the dataApertura property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataApertura(XMLGregorianCalendar value) {
        this.dataApertura = value;
    }

    /**
     * Gets the value of the idFolderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFolderId() {
        return idFolderId;
    }

    /**
     * Sets the value of the idFolderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFolderId(String value) {
        this.idFolderId = value;
    }

    /**
     * Gets the value of the listaCartelle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaCartelle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaCartelle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Folder }
     * 
     * 
     */
    public List<Folder> getListaCartelle() {
        if (listaCartelle == null) {
            listaCartelle = new ArrayList<Folder>();
        }
        return this.listaCartelle;
    }

    /**
     * Gets the value of the listaDocumenti property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaDocumenti property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaDocumenti().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Documento }
     * 
     * 
     */
    public List<Documento> getListaDocumenti() {
        if (listaDocumenti == null) {
            listaDocumenti = new ArrayList<Documento>();
        }
        return this.listaDocumenti;
    }

    /**
     * Gets the value of the nomeCartella property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeCartella() {
        return nomeCartella;
    }

    /**
     * Sets the value of the nomeCartella property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeCartella(String value) {
        this.nomeCartella = value;
    }

    /**
     * Gets the value of the iChronicleId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIChronicleId() {
        return iChronicleId;
    }

    /**
     * Sets the value of the iChronicleId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIChronicleId(String value) {
        this.iChronicleId = value;
    }

}
