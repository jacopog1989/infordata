
package it.sogei.gestionedocumentale.documentale.ejbprotocollo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for risultatiRicercaBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="risultatiRicercaBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accessoFascicoli" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="aclList" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="apriPopup" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="authDatiSensibili" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BRicercaStorico" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="campoTitolario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cnRetCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiceAoo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiceEnte" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiceRegistro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="countRicercaRegistro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="creaAttivita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataAcquisizioneDoc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataProtocollo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataRicezione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descFileName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descOggetto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descProtocollatore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descRegistro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTipoDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descUfficio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descrizione_Aoo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="destinatario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="docIsLimited" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="emAoo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="emAutenticato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="existAttivita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fkAttEstesi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagAnnullato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagChiuso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagDatiSensibili" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagInPratica" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="flagModalita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagRiservato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagStato" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="flagStatoContenuto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hasPermessoProtoCongruente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hashContenuto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="hashUfficiAttivita">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="entry" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                             &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="hashUtentiAttivita">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="entry" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                             &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="idAutorizzazione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idCasella" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idDocConservato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idProtocollo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idUfficiAttivita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idUfficio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idUtenteModifica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idUtentiAttivita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="identificativoControllo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idsDocDaAssegnare" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="idsDocDaCollegare" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="imgAttivita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="isCriptato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="isLimited" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="isVisible" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="linkAttivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="listaAllegati" type="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}allegatoBean" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="listaMittenti" type="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}mittenteBean" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="mittenete" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mitteneteDestinatario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modalitaIU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numAllegati" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="originalDocId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="permessoCasella" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="priorita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="protocollatore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="registro" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="secureIdProtocollo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sequLongId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sequLongIdConservato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="statoAutorizzazioneDatiSensibili" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoRicevuta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ufficioIdOwner" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="ufficioProtocollo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risultatiRicercaBean", propOrder = {
    "accessoFascicoli",
    "aclList",
    "apriPopup",
    "authDatiSensibili",
    "bRicercaStorico",
    "campoTitolario",
    "cnRetCode",
    "codiceAoo",
    "codiceEnte",
    "codiceRegistro",
    "countRicercaRegistro",
    "creaAttivita",
    "dataAcquisizioneDoc",
    "dataProtocollo",
    "dataRicezione",
    "descFileName",
    "descOggetto",
    "descProtocollatore",
    "descRegistro",
    "descTipoDocumento",
    "descUfficio",
    "descrizioneAoo",
    "destinatario",
    "docIsLimited",
    "emAoo",
    "emAutenticato",
    "existAttivita",
    "fkAttEstesi",
    "flagAnnullato",
    "flagChiuso",
    "flagDatiSensibili",
    "flagInPratica",
    "flagModalita",
    "flagRiservato",
    "flagStato",
    "flagStatoContenuto",
    "hasPermessoProtoCongruente",
    "hashContenuto",
    "hashUfficiAttivita",
    "hashUtentiAttivita",
    "idAutorizzazione",
    "idCasella",
    "idDocConservato",
    "idEmail",
    "idProtocollo",
    "idUfficiAttivita",
    "idUfficio",
    "idUtenteModifica",
    "idUtentiAttivita",
    "identificativoControllo",
    "idsDocDaAssegnare",
    "idsDocDaCollegare",
    "imgAttivita",
    "isCriptato",
    "isLimited",
    "isVisible",
    "linkAttivo",
    "listaAllegati",
    "listaMittenti",
    "mittenete",
    "mitteneteDestinatario",
    "modalitaIU",
    "numAllegati",
    "originalDocId",
    "permessoCasella",
    "priorita",
    "protocollatore",
    "registro",
    "secureIdProtocollo",
    "sequLongId",
    "sequLongIdConservato",
    "statoAutorizzazioneDatiSensibili",
    "tipoDocumento",
    "tipoRicevuta",
    "ufficioIdOwner",
    "ufficioProtocollo"
})
public class RisultatiRicercaBean {

    protected String accessoFascicoli;
    protected String aclList;
    protected String apriPopup;
    protected String authDatiSensibili;
    @XmlElement(name = "BRicercaStorico")
    protected int bRicercaStorico;
    protected String campoTitolario;
    protected String cnRetCode;
    protected String codiceAoo;
    protected String codiceEnte;
    protected String codiceRegistro;
    protected String countRicercaRegistro;
    protected String creaAttivita;
    protected String dataAcquisizioneDoc;
    protected String dataProtocollo;
    protected String dataRicezione;
    protected String descFileName;
    protected String descOggetto;
    protected String descProtocollatore;
    protected String descRegistro;
    protected String descTipoDocumento;
    protected String descUfficio;
    @XmlElement(name = "descrizione_Aoo")
    protected String descrizioneAoo;
    protected String destinatario;
    protected String docIsLimited;
    protected String emAoo;
    protected String emAutenticato;
    protected String existAttivita;
    protected String fkAttEstesi;
    protected String flagAnnullato;
    protected String flagChiuso;
    protected String flagDatiSensibili;
    protected Integer flagInPratica;
    protected String flagModalita;
    protected String flagRiservato;
    protected int flagStato;
    protected String flagStatoContenuto;
    protected String hasPermessoProtoCongruente;
    protected String hashContenuto;
    @XmlElement(required = true)
    protected RisultatiRicercaBean.HashUfficiAttivita hashUfficiAttivita;
    @XmlElement(required = true)
    protected RisultatiRicercaBean.HashUtentiAttivita hashUtentiAttivita;
    protected String idAutorizzazione;
    protected String idCasella;
    protected String idDocConservato;
    protected String idEmail;
    protected String idProtocollo;
    protected String idUfficiAttivita;
    protected String idUfficio;
    protected String idUtenteModifica;
    protected String idUtentiAttivita;
    protected String identificativoControllo;
    @XmlElement(nillable = true)
    protected List<String> idsDocDaAssegnare;
    @XmlElement(nillable = true)
    protected List<String> idsDocDaCollegare;
    protected String imgAttivita;
    protected String isCriptato;
    protected String isLimited;
    protected String isVisible;
    protected String linkAttivo;
    @XmlElement(nillable = true)
    protected List<AllegatoBean> listaAllegati;
    @XmlElement(nillable = true)
    protected List<MittenteBean> listaMittenti;
    protected String mittenete;
    protected String mitteneteDestinatario;
    protected String modalitaIU;
    protected String numAllegati;
    protected String originalDocId;
    protected boolean permessoCasella;
    protected String priorita;
    protected String protocollatore;
    protected Long registro;
    protected String secureIdProtocollo;
    protected String sequLongId;
    protected String sequLongIdConservato;
    protected String statoAutorizzazioneDatiSensibili;
    protected String tipoDocumento;
    protected String tipoRicevuta;
    protected Long ufficioIdOwner;
    protected String ufficioProtocollo;

    /**
     * Gets the value of the accessoFascicoli property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccessoFascicoli() {
        return accessoFascicoli;
    }

    /**
     * Sets the value of the accessoFascicoli property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccessoFascicoli(String value) {
        this.accessoFascicoli = value;
    }

    /**
     * Gets the value of the aclList property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAclList() {
        return aclList;
    }

    /**
     * Sets the value of the aclList property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAclList(String value) {
        this.aclList = value;
    }

    /**
     * Gets the value of the apriPopup property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApriPopup() {
        return apriPopup;
    }

    /**
     * Sets the value of the apriPopup property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApriPopup(String value) {
        this.apriPopup = value;
    }

    /**
     * Gets the value of the authDatiSensibili property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthDatiSensibili() {
        return authDatiSensibili;
    }

    /**
     * Sets the value of the authDatiSensibili property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthDatiSensibili(String value) {
        this.authDatiSensibili = value;
    }

    /**
     * Gets the value of the bRicercaStorico property.
     * 
     */
    public int getBRicercaStorico() {
        return bRicercaStorico;
    }

    /**
     * Sets the value of the bRicercaStorico property.
     * 
     */
    public void setBRicercaStorico(int value) {
        this.bRicercaStorico = value;
    }

    /**
     * Gets the value of the campoTitolario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCampoTitolario() {
        return campoTitolario;
    }

    /**
     * Sets the value of the campoTitolario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCampoTitolario(String value) {
        this.campoTitolario = value;
    }

    /**
     * Gets the value of the cnRetCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCnRetCode() {
        return cnRetCode;
    }

    /**
     * Sets the value of the cnRetCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCnRetCode(String value) {
        this.cnRetCode = value;
    }

    /**
     * Gets the value of the codiceAoo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceAoo() {
        return codiceAoo;
    }

    /**
     * Sets the value of the codiceAoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceAoo(String value) {
        this.codiceAoo = value;
    }

    /**
     * Gets the value of the codiceEnte property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceEnte() {
        return codiceEnte;
    }

    /**
     * Sets the value of the codiceEnte property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceEnte(String value) {
        this.codiceEnte = value;
    }

    /**
     * Gets the value of the codiceRegistro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceRegistro() {
        return codiceRegistro;
    }

    /**
     * Sets the value of the codiceRegistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceRegistro(String value) {
        this.codiceRegistro = value;
    }

    /**
     * Gets the value of the countRicercaRegistro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountRicercaRegistro() {
        return countRicercaRegistro;
    }

    /**
     * Sets the value of the countRicercaRegistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountRicercaRegistro(String value) {
        this.countRicercaRegistro = value;
    }

    /**
     * Gets the value of the creaAttivita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreaAttivita() {
        return creaAttivita;
    }

    /**
     * Sets the value of the creaAttivita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreaAttivita(String value) {
        this.creaAttivita = value;
    }

    /**
     * Gets the value of the dataAcquisizioneDoc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataAcquisizioneDoc() {
        return dataAcquisizioneDoc;
    }

    /**
     * Sets the value of the dataAcquisizioneDoc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataAcquisizioneDoc(String value) {
        this.dataAcquisizioneDoc = value;
    }

    /**
     * Gets the value of the dataProtocollo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataProtocollo() {
        return dataProtocollo;
    }

    /**
     * Sets the value of the dataProtocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataProtocollo(String value) {
        this.dataProtocollo = value;
    }

    /**
     * Gets the value of the dataRicezione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataRicezione() {
        return dataRicezione;
    }

    /**
     * Sets the value of the dataRicezione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataRicezione(String value) {
        this.dataRicezione = value;
    }

    /**
     * Gets the value of the descFileName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescFileName() {
        return descFileName;
    }

    /**
     * Sets the value of the descFileName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescFileName(String value) {
        this.descFileName = value;
    }

    /**
     * Gets the value of the descOggetto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescOggetto() {
        return descOggetto;
    }

    /**
     * Sets the value of the descOggetto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescOggetto(String value) {
        this.descOggetto = value;
    }

    /**
     * Gets the value of the descProtocollatore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescProtocollatore() {
        return descProtocollatore;
    }

    /**
     * Sets the value of the descProtocollatore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescProtocollatore(String value) {
        this.descProtocollatore = value;
    }

    /**
     * Gets the value of the descRegistro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescRegistro() {
        return descRegistro;
    }

    /**
     * Sets the value of the descRegistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescRegistro(String value) {
        this.descRegistro = value;
    }

    /**
     * Gets the value of the descTipoDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTipoDocumento() {
        return descTipoDocumento;
    }

    /**
     * Sets the value of the descTipoDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTipoDocumento(String value) {
        this.descTipoDocumento = value;
    }

    /**
     * Gets the value of the descUfficio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescUfficio() {
        return descUfficio;
    }

    /**
     * Sets the value of the descUfficio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescUfficio(String value) {
        this.descUfficio = value;
    }

    /**
     * Gets the value of the descrizioneAoo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizioneAoo() {
        return descrizioneAoo;
    }

    /**
     * Sets the value of the descrizioneAoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizioneAoo(String value) {
        this.descrizioneAoo = value;
    }

    /**
     * Gets the value of the destinatario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinatario() {
        return destinatario;
    }

    /**
     * Sets the value of the destinatario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinatario(String value) {
        this.destinatario = value;
    }

    /**
     * Gets the value of the docIsLimited property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocIsLimited() {
        return docIsLimited;
    }

    /**
     * Sets the value of the docIsLimited property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocIsLimited(String value) {
        this.docIsLimited = value;
    }

    /**
     * Gets the value of the emAoo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmAoo() {
        return emAoo;
    }

    /**
     * Sets the value of the emAoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmAoo(String value) {
        this.emAoo = value;
    }

    /**
     * Gets the value of the emAutenticato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmAutenticato() {
        return emAutenticato;
    }

    /**
     * Sets the value of the emAutenticato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmAutenticato(String value) {
        this.emAutenticato = value;
    }

    /**
     * Gets the value of the existAttivita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExistAttivita() {
        return existAttivita;
    }

    /**
     * Sets the value of the existAttivita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExistAttivita(String value) {
        this.existAttivita = value;
    }

    /**
     * Gets the value of the fkAttEstesi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFkAttEstesi() {
        return fkAttEstesi;
    }

    /**
     * Sets the value of the fkAttEstesi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFkAttEstesi(String value) {
        this.fkAttEstesi = value;
    }

    /**
     * Gets the value of the flagAnnullato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagAnnullato() {
        return flagAnnullato;
    }

    /**
     * Sets the value of the flagAnnullato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagAnnullato(String value) {
        this.flagAnnullato = value;
    }

    /**
     * Gets the value of the flagChiuso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagChiuso() {
        return flagChiuso;
    }

    /**
     * Sets the value of the flagChiuso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagChiuso(String value) {
        this.flagChiuso = value;
    }

    /**
     * Gets the value of the flagDatiSensibili property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagDatiSensibili() {
        return flagDatiSensibili;
    }

    /**
     * Sets the value of the flagDatiSensibili property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagDatiSensibili(String value) {
        this.flagDatiSensibili = value;
    }

    /**
     * Gets the value of the flagInPratica property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFlagInPratica() {
        return flagInPratica;
    }

    /**
     * Sets the value of the flagInPratica property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFlagInPratica(Integer value) {
        this.flagInPratica = value;
    }

    /**
     * Gets the value of the flagModalita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagModalita() {
        return flagModalita;
    }

    /**
     * Sets the value of the flagModalita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagModalita(String value) {
        this.flagModalita = value;
    }

    /**
     * Gets the value of the flagRiservato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagRiservato() {
        return flagRiservato;
    }

    /**
     * Sets the value of the flagRiservato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagRiservato(String value) {
        this.flagRiservato = value;
    }

    /**
     * Gets the value of the flagStato property.
     * 
     */
    public int getFlagStato() {
        return flagStato;
    }

    /**
     * Sets the value of the flagStato property.
     * 
     */
    public void setFlagStato(int value) {
        this.flagStato = value;
    }

    /**
     * Gets the value of the flagStatoContenuto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagStatoContenuto() {
        return flagStatoContenuto;
    }

    /**
     * Sets the value of the flagStatoContenuto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagStatoContenuto(String value) {
        this.flagStatoContenuto = value;
    }

    /**
     * Gets the value of the hasPermessoProtoCongruente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHasPermessoProtoCongruente() {
        return hasPermessoProtoCongruente;
    }

    /**
     * Sets the value of the hasPermessoProtoCongruente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHasPermessoProtoCongruente(String value) {
        this.hasPermessoProtoCongruente = value;
    }

    /**
     * Gets the value of the hashContenuto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHashContenuto() {
        return hashContenuto;
    }

    /**
     * Sets the value of the hashContenuto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHashContenuto(String value) {
        this.hashContenuto = value;
    }

    /**
     * Gets the value of the hashUfficiAttivita property.
     * 
     * @return
     *     possible object is
     *     {@link RisultatiRicercaBean.HashUfficiAttivita }
     *     
     */
    public RisultatiRicercaBean.HashUfficiAttivita getHashUfficiAttivita() {
        return hashUfficiAttivita;
    }

    /**
     * Sets the value of the hashUfficiAttivita property.
     * 
     * @param value
     *     allowed object is
     *     {@link RisultatiRicercaBean.HashUfficiAttivita }
     *     
     */
    public void setHashUfficiAttivita(RisultatiRicercaBean.HashUfficiAttivita value) {
        this.hashUfficiAttivita = value;
    }

    /**
     * Gets the value of the hashUtentiAttivita property.
     * 
     * @return
     *     possible object is
     *     {@link RisultatiRicercaBean.HashUtentiAttivita }
     *     
     */
    public RisultatiRicercaBean.HashUtentiAttivita getHashUtentiAttivita() {
        return hashUtentiAttivita;
    }

    /**
     * Sets the value of the hashUtentiAttivita property.
     * 
     * @param value
     *     allowed object is
     *     {@link RisultatiRicercaBean.HashUtentiAttivita }
     *     
     */
    public void setHashUtentiAttivita(RisultatiRicercaBean.HashUtentiAttivita value) {
        this.hashUtentiAttivita = value;
    }

    /**
     * Gets the value of the idAutorizzazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdAutorizzazione() {
        return idAutorizzazione;
    }

    /**
     * Sets the value of the idAutorizzazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdAutorizzazione(String value) {
        this.idAutorizzazione = value;
    }

    /**
     * Gets the value of the idCasella property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdCasella() {
        return idCasella;
    }

    /**
     * Sets the value of the idCasella property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdCasella(String value) {
        this.idCasella = value;
    }

    /**
     * Gets the value of the idDocConservato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocConservato() {
        return idDocConservato;
    }

    /**
     * Sets the value of the idDocConservato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocConservato(String value) {
        this.idDocConservato = value;
    }

    /**
     * Gets the value of the idEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdEmail() {
        return idEmail;
    }

    /**
     * Sets the value of the idEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdEmail(String value) {
        this.idEmail = value;
    }

    /**
     * Gets the value of the idProtocollo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdProtocollo() {
        return idProtocollo;
    }

    /**
     * Sets the value of the idProtocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdProtocollo(String value) {
        this.idProtocollo = value;
    }

    /**
     * Gets the value of the idUfficiAttivita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdUfficiAttivita() {
        return idUfficiAttivita;
    }

    /**
     * Sets the value of the idUfficiAttivita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdUfficiAttivita(String value) {
        this.idUfficiAttivita = value;
    }

    /**
     * Gets the value of the idUfficio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdUfficio() {
        return idUfficio;
    }

    /**
     * Sets the value of the idUfficio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdUfficio(String value) {
        this.idUfficio = value;
    }

    /**
     * Gets the value of the idUtenteModifica property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdUtenteModifica() {
        return idUtenteModifica;
    }

    /**
     * Sets the value of the idUtenteModifica property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdUtenteModifica(String value) {
        this.idUtenteModifica = value;
    }

    /**
     * Gets the value of the idUtentiAttivita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdUtentiAttivita() {
        return idUtentiAttivita;
    }

    /**
     * Sets the value of the idUtentiAttivita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdUtentiAttivita(String value) {
        this.idUtentiAttivita = value;
    }

    /**
     * Gets the value of the identificativoControllo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificativoControllo() {
        return identificativoControllo;
    }

    /**
     * Sets the value of the identificativoControllo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificativoControllo(String value) {
        this.identificativoControllo = value;
    }

    /**
     * Gets the value of the idsDocDaAssegnare property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the idsDocDaAssegnare property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdsDocDaAssegnare().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getIdsDocDaAssegnare() {
        if (idsDocDaAssegnare == null) {
            idsDocDaAssegnare = new ArrayList<String>();
        }
        return this.idsDocDaAssegnare;
    }

    /**
     * Gets the value of the idsDocDaCollegare property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the idsDocDaCollegare property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdsDocDaCollegare().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getIdsDocDaCollegare() {
        if (idsDocDaCollegare == null) {
            idsDocDaCollegare = new ArrayList<String>();
        }
        return this.idsDocDaCollegare;
    }

    /**
     * Gets the value of the imgAttivita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImgAttivita() {
        return imgAttivita;
    }

    /**
     * Sets the value of the imgAttivita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImgAttivita(String value) {
        this.imgAttivita = value;
    }

    /**
     * Gets the value of the isCriptato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCriptato() {
        return isCriptato;
    }

    /**
     * Sets the value of the isCriptato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCriptato(String value) {
        this.isCriptato = value;
    }

    /**
     * Gets the value of the isLimited property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsLimited() {
        return isLimited;
    }

    /**
     * Sets the value of the isLimited property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsLimited(String value) {
        this.isLimited = value;
    }

    /**
     * Gets the value of the isVisible property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsVisible() {
        return isVisible;
    }

    /**
     * Sets the value of the isVisible property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsVisible(String value) {
        this.isVisible = value;
    }

    /**
     * Gets the value of the linkAttivo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLinkAttivo() {
        return linkAttivo;
    }

    /**
     * Sets the value of the linkAttivo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLinkAttivo(String value) {
        this.linkAttivo = value;
    }

    /**
     * Gets the value of the listaAllegati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaAllegati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaAllegati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AllegatoBean }
     * 
     * 
     */
    public List<AllegatoBean> getListaAllegati() {
        if (listaAllegati == null) {
            listaAllegati = new ArrayList<AllegatoBean>();
        }
        return this.listaAllegati;
    }

    /**
     * Gets the value of the listaMittenti property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaMittenti property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaMittenti().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MittenteBean }
     * 
     * 
     */
    public List<MittenteBean> getListaMittenti() {
        if (listaMittenti == null) {
            listaMittenti = new ArrayList<MittenteBean>();
        }
        return this.listaMittenti;
    }

    /**
     * Gets the value of the mittenete property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMittenete() {
        return mittenete;
    }

    /**
     * Sets the value of the mittenete property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMittenete(String value) {
        this.mittenete = value;
    }

    /**
     * Gets the value of the mitteneteDestinatario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMitteneteDestinatario() {
        return mitteneteDestinatario;
    }

    /**
     * Sets the value of the mitteneteDestinatario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMitteneteDestinatario(String value) {
        this.mitteneteDestinatario = value;
    }

    /**
     * Gets the value of the modalitaIU property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModalitaIU() {
        return modalitaIU;
    }

    /**
     * Sets the value of the modalitaIU property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModalitaIU(String value) {
        this.modalitaIU = value;
    }

    /**
     * Gets the value of the numAllegati property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumAllegati() {
        return numAllegati;
    }

    /**
     * Sets the value of the numAllegati property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumAllegati(String value) {
        this.numAllegati = value;
    }

    /**
     * Gets the value of the originalDocId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalDocId() {
        return originalDocId;
    }

    /**
     * Sets the value of the originalDocId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalDocId(String value) {
        this.originalDocId = value;
    }

    /**
     * Gets the value of the permessoCasella property.
     * 
     */
    public boolean isPermessoCasella() {
        return permessoCasella;
    }

    /**
     * Sets the value of the permessoCasella property.
     * 
     */
    public void setPermessoCasella(boolean value) {
        this.permessoCasella = value;
    }

    /**
     * Gets the value of the priorita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriorita() {
        return priorita;
    }

    /**
     * Sets the value of the priorita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriorita(String value) {
        this.priorita = value;
    }

    /**
     * Gets the value of the protocollatore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProtocollatore() {
        return protocollatore;
    }

    /**
     * Sets the value of the protocollatore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProtocollatore(String value) {
        this.protocollatore = value;
    }

    /**
     * Gets the value of the registro property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRegistro() {
        return registro;
    }

    /**
     * Sets the value of the registro property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRegistro(Long value) {
        this.registro = value;
    }

    /**
     * Gets the value of the secureIdProtocollo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecureIdProtocollo() {
        return secureIdProtocollo;
    }

    /**
     * Sets the value of the secureIdProtocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecureIdProtocollo(String value) {
        this.secureIdProtocollo = value;
    }

    /**
     * Gets the value of the sequLongId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequLongId() {
        return sequLongId;
    }

    /**
     * Sets the value of the sequLongId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequLongId(String value) {
        this.sequLongId = value;
    }

    /**
     * Gets the value of the sequLongIdConservato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequLongIdConservato() {
        return sequLongIdConservato;
    }

    /**
     * Sets the value of the sequLongIdConservato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequLongIdConservato(String value) {
        this.sequLongIdConservato = value;
    }

    /**
     * Gets the value of the statoAutorizzazioneDatiSensibili property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatoAutorizzazioneDatiSensibili() {
        return statoAutorizzazioneDatiSensibili;
    }

    /**
     * Sets the value of the statoAutorizzazioneDatiSensibili property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatoAutorizzazioneDatiSensibili(String value) {
        this.statoAutorizzazioneDatiSensibili = value;
    }

    /**
     * Gets the value of the tipoDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Sets the value of the tipoDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDocumento(String value) {
        this.tipoDocumento = value;
    }

    /**
     * Gets the value of the tipoRicevuta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoRicevuta() {
        return tipoRicevuta;
    }

    /**
     * Sets the value of the tipoRicevuta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoRicevuta(String value) {
        this.tipoRicevuta = value;
    }

    /**
     * Gets the value of the ufficioIdOwner property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getUfficioIdOwner() {
        return ufficioIdOwner;
    }

    /**
     * Sets the value of the ufficioIdOwner property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setUfficioIdOwner(Long value) {
        this.ufficioIdOwner = value;
    }

    /**
     * Gets the value of the ufficioProtocollo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUfficioProtocollo() {
        return ufficioProtocollo;
    }

    /**
     * Sets the value of the ufficioProtocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUfficioProtocollo(String value) {
        this.ufficioProtocollo = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="entry" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                   &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "entry"
    })
    public static class HashUfficiAttivita {

        protected List<RisultatiRicercaBean.HashUfficiAttivita.Entry> entry;

        /**
         * Gets the value of the entry property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the entry property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEntry().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RisultatiRicercaBean.HashUfficiAttivita.Entry }
         * 
         * 
         */
        public List<RisultatiRicercaBean.HashUfficiAttivita.Entry> getEntry() {
            if (entry == null) {
                entry = new ArrayList<RisultatiRicercaBean.HashUfficiAttivita.Entry>();
            }
            return this.entry;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "key",
            "value"
        })
        public static class Entry {

            protected Object key;
            protected Object value;

            /**
             * Gets the value of the key property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getKey() {
                return key;
            }

            /**
             * Sets the value of the key property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setKey(Object value) {
                this.key = value;
            }

            /**
             * Gets the value of the value property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setValue(Object value) {
                this.value = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="entry" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                   &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "entry"
    })
    public static class HashUtentiAttivita {

        protected List<RisultatiRicercaBean.HashUtentiAttivita.Entry> entry;

        /**
         * Gets the value of the entry property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the entry property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEntry().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RisultatiRicercaBean.HashUtentiAttivita.Entry }
         * 
         * 
         */
        public List<RisultatiRicercaBean.HashUtentiAttivita.Entry> getEntry() {
            if (entry == null) {
                entry = new ArrayList<RisultatiRicercaBean.HashUtentiAttivita.Entry>();
            }
            return this.entry;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "key",
            "value"
        })
        public static class Entry {

            protected Object key;
            protected Object value;

            /**
             * Gets the value of the key property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getKey() {
                return key;
            }

            /**
             * Sets the value of the key property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setKey(Object value) {
                this.key = value;
            }

            /**
             * Gets the value of the value property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setValue(Object value) {
                this.value = value;
            }

        }

    }

}
