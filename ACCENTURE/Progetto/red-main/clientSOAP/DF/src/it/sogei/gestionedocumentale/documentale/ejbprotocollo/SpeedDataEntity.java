
package it.sogei.gestionedocumentale.documentale.ejbprotocollo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for speedDataEntity complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="speedDataEntity">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dataAggiornamento" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataCreazione" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="modalitaEsecuzione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="utenteAggiornamento" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="utenteCreazione" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "speedDataEntity", propOrder = {
    "dataAggiornamento",
    "dataCreazione",
    "id",
    "modalitaEsecuzione",
    "utenteAggiornamento",
    "utenteCreazione"
})
@XmlSeeAlso({
    SpeedMezzoSpedizione.class,
    SpeedTipologieDocumento.class,
    SpeedCasellaEmail.class
})
public abstract class SpeedDataEntity {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataAggiornamento;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataCreazione;
    protected Long id;
    protected String modalitaEsecuzione;
    protected Long utenteAggiornamento;
    protected Long utenteCreazione;

    /**
     * Gets the value of the dataAggiornamento property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAggiornamento() {
        return dataAggiornamento;
    }

    /**
     * Sets the value of the dataAggiornamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAggiornamento(XMLGregorianCalendar value) {
        this.dataAggiornamento = value;
    }

    /**
     * Gets the value of the dataCreazione property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataCreazione() {
        return dataCreazione;
    }

    /**
     * Sets the value of the dataCreazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataCreazione(XMLGregorianCalendar value) {
        this.dataCreazione = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setId(Long value) {
        this.id = value;
    }

    /**
     * Gets the value of the modalitaEsecuzione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModalitaEsecuzione() {
        return modalitaEsecuzione;
    }

    /**
     * Sets the value of the modalitaEsecuzione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModalitaEsecuzione(String value) {
        this.modalitaEsecuzione = value;
    }

    /**
     * Gets the value of the utenteAggiornamento property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getUtenteAggiornamento() {
        return utenteAggiornamento;
    }

    /**
     * Sets the value of the utenteAggiornamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setUtenteAggiornamento(Long value) {
        this.utenteAggiornamento = value;
    }

    /**
     * Gets the value of the utenteCreazione property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getUtenteCreazione() {
        return utenteCreazione;
    }

    /**
     * Sets the value of the utenteCreazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setUtenteCreazione(Long value) {
        this.utenteCreazione = value;
    }

}
