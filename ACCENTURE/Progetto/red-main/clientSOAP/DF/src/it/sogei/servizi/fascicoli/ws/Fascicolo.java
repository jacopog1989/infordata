
package it.sogei.servizi.fascicoli.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for fascicolo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="fascicolo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="businessOwner" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="classeDocumentale" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="classificazione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cod_aoo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cod_titolario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cod_uff_destinatario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cod_uff_proprietario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataApertura" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataChiusura" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataScadenza" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="descr_aoo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descr_titolario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descr_uff_proprietario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descrizione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="destinatario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="folder_signature" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idFolderId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idFolderNSD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idFolderParent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="listaCartelle" type="{http://ws.fascicoli.servizi.sogei.it/}folder" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="listaDocumenti" type="{http://ws.fascicoli.servizi.sogei.it/}documento" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="listaMetadati" type="{http://ws.fascicoli.servizi.sogei.it/}beanMetadato" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="motivoChiusura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeCartella" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numCartelle" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numDocumenti" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numeroFascicolo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="owner" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proprietario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proprietario_cf" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="responsabile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="stato" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="tipologia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="visibile" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="iChronicleId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fascicolo", propOrder = {
    "businessOwner",
    "classeDocumentale",
    "classificazione",
    "codAoo",
    "codTitolario",
    "codUffDestinatario",
    "codUffProprietario",
    "dataApertura",
    "dataChiusura",
    "dataScadenza",
    "descrAoo",
    "descrTitolario",
    "descrUffProprietario",
    "descrizione",
    "destinatario",
    "folderSignature",
    "id",
    "idFolderId",
    "idFolderNSD",
    "idFolderParent",
    "listaCartelle",
    "listaDocumenti",
    "listaMetadati",
    "motivoChiusura",
    "nomeCartella",
    "note",
    "numCartelle",
    "numDocumenti",
    "numeroFascicolo",
    "owner",
    "proprietario",
    "proprietarioCf",
    "responsabile",
    "stato",
    "tipologia",
    "visibile",
    "iChronicleId"
})
public class Fascicolo {

    protected String businessOwner;
    protected String classeDocumentale;
    protected String classificazione;
    @XmlElement(name = "cod_aoo")
    protected String codAoo;
    @XmlElement(name = "cod_titolario")
    protected String codTitolario;
    @XmlElement(name = "cod_uff_destinatario")
    protected String codUffDestinatario;
    @XmlElement(name = "cod_uff_proprietario")
    protected String codUffProprietario;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataApertura;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataChiusura;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataScadenza;
    @XmlElement(name = "descr_aoo")
    protected String descrAoo;
    @XmlElement(name = "descr_titolario")
    protected String descrTitolario;
    @XmlElement(name = "descr_uff_proprietario")
    protected String descrUffProprietario;
    protected String descrizione;
    protected String destinatario;
    @XmlElement(name = "folder_signature")
    protected String folderSignature;
    protected String id;
    protected String idFolderId;
    protected String idFolderNSD;
    protected String idFolderParent;
    @XmlElement(nillable = true)
    protected List<Folder> listaCartelle;
    @XmlElement(nillable = true)
    protected List<Documento> listaDocumenti;
    @XmlElement(nillable = true)
    protected List<BeanMetadato> listaMetadati;
    protected String motivoChiusura;
    protected String nomeCartella;
    protected String note;
    protected int numCartelle;
    protected int numDocumenti;
    protected Integer numeroFascicolo;
    protected String owner;
    protected String proprietario;
    @XmlElement(name = "proprietario_cf")
    protected String proprietarioCf;
    protected String responsabile;
    protected Integer stato;
    protected String tipologia;
    protected boolean visibile;
    protected String iChronicleId;

    /**
     * Gets the value of the businessOwner property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessOwner() {
        return businessOwner;
    }

    /**
     * Sets the value of the businessOwner property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessOwner(String value) {
        this.businessOwner = value;
    }

    /**
     * Gets the value of the classeDocumentale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClasseDocumentale() {
        return classeDocumentale;
    }

    /**
     * Sets the value of the classeDocumentale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClasseDocumentale(String value) {
        this.classeDocumentale = value;
    }

    /**
     * Gets the value of the classificazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassificazione() {
        return classificazione;
    }

    /**
     * Sets the value of the classificazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassificazione(String value) {
        this.classificazione = value;
    }

    /**
     * Gets the value of the codAoo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodAoo() {
        return codAoo;
    }

    /**
     * Sets the value of the codAoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodAoo(String value) {
        this.codAoo = value;
    }

    /**
     * Gets the value of the codTitolario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodTitolario() {
        return codTitolario;
    }

    /**
     * Sets the value of the codTitolario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodTitolario(String value) {
        this.codTitolario = value;
    }

    /**
     * Gets the value of the codUffDestinatario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodUffDestinatario() {
        return codUffDestinatario;
    }

    /**
     * Sets the value of the codUffDestinatario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodUffDestinatario(String value) {
        this.codUffDestinatario = value;
    }

    /**
     * Gets the value of the codUffProprietario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodUffProprietario() {
        return codUffProprietario;
    }

    /**
     * Sets the value of the codUffProprietario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodUffProprietario(String value) {
        this.codUffProprietario = value;
    }

    /**
     * Gets the value of the dataApertura property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataApertura() {
        return dataApertura;
    }

    /**
     * Sets the value of the dataApertura property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataApertura(XMLGregorianCalendar value) {
        this.dataApertura = value;
    }

    /**
     * Gets the value of the dataChiusura property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataChiusura() {
        return dataChiusura;
    }

    /**
     * Sets the value of the dataChiusura property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataChiusura(XMLGregorianCalendar value) {
        this.dataChiusura = value;
    }

    /**
     * Gets the value of the dataScadenza property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataScadenza() {
        return dataScadenza;
    }

    /**
     * Sets the value of the dataScadenza property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataScadenza(XMLGregorianCalendar value) {
        this.dataScadenza = value;
    }

    /**
     * Gets the value of the descrAoo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrAoo() {
        return descrAoo;
    }

    /**
     * Sets the value of the descrAoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrAoo(String value) {
        this.descrAoo = value;
    }

    /**
     * Gets the value of the descrTitolario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrTitolario() {
        return descrTitolario;
    }

    /**
     * Sets the value of the descrTitolario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrTitolario(String value) {
        this.descrTitolario = value;
    }

    /**
     * Gets the value of the descrUffProprietario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrUffProprietario() {
        return descrUffProprietario;
    }

    /**
     * Sets the value of the descrUffProprietario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrUffProprietario(String value) {
        this.descrUffProprietario = value;
    }

    /**
     * Gets the value of the descrizione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Sets the value of the descrizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Gets the value of the destinatario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDestinatario() {
        return destinatario;
    }

    /**
     * Sets the value of the destinatario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDestinatario(String value) {
        this.destinatario = value;
    }

    /**
     * Gets the value of the folderSignature property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFolderSignature() {
        return folderSignature;
    }

    /**
     * Sets the value of the folderSignature property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFolderSignature(String value) {
        this.folderSignature = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setId(String value) {
        this.id = value;
    }

    /**
     * Gets the value of the idFolderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFolderId() {
        return idFolderId;
    }

    /**
     * Sets the value of the idFolderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFolderId(String value) {
        this.idFolderId = value;
    }

    /**
     * Gets the value of the idFolderNSD property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFolderNSD() {
        return idFolderNSD;
    }

    /**
     * Sets the value of the idFolderNSD property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFolderNSD(String value) {
        this.idFolderNSD = value;
    }

    /**
     * Gets the value of the idFolderParent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFolderParent() {
        return idFolderParent;
    }

    /**
     * Sets the value of the idFolderParent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFolderParent(String value) {
        this.idFolderParent = value;
    }

    /**
     * Gets the value of the listaCartelle property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaCartelle property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaCartelle().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Folder }
     * 
     * 
     */
    public List<Folder> getListaCartelle() {
        if (listaCartelle == null) {
            listaCartelle = new ArrayList<Folder>();
        }
        return this.listaCartelle;
    }

    /**
     * Gets the value of the listaDocumenti property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaDocumenti property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaDocumenti().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Documento }
     * 
     * 
     */
    public List<Documento> getListaDocumenti() {
        if (listaDocumenti == null) {
            listaDocumenti = new ArrayList<Documento>();
        }
        return this.listaDocumenti;
    }

    /**
     * Gets the value of the listaMetadati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaMetadati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaMetadati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BeanMetadato }
     * 
     * 
     */
    public List<BeanMetadato> getListaMetadati() {
        if (listaMetadati == null) {
            listaMetadati = new ArrayList<BeanMetadato>();
        }
        return this.listaMetadati;
    }

    /**
     * Gets the value of the motivoChiusura property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMotivoChiusura() {
        return motivoChiusura;
    }

    /**
     * Sets the value of the motivoChiusura property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMotivoChiusura(String value) {
        this.motivoChiusura = value;
    }

    /**
     * Gets the value of the nomeCartella property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeCartella() {
        return nomeCartella;
    }

    /**
     * Sets the value of the nomeCartella property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeCartella(String value) {
        this.nomeCartella = value;
    }

    /**
     * Gets the value of the note property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNote() {
        return note;
    }

    /**
     * Sets the value of the note property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNote(String value) {
        this.note = value;
    }

    /**
     * Gets the value of the numCartelle property.
     * 
     */
    public int getNumCartelle() {
        return numCartelle;
    }

    /**
     * Sets the value of the numCartelle property.
     * 
     */
    public void setNumCartelle(int value) {
        this.numCartelle = value;
    }

    /**
     * Gets the value of the numDocumenti property.
     * 
     */
    public int getNumDocumenti() {
        return numDocumenti;
    }

    /**
     * Sets the value of the numDocumenti property.
     * 
     */
    public void setNumDocumenti(int value) {
        this.numDocumenti = value;
    }

    /**
     * Gets the value of the numeroFascicolo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumeroFascicolo() {
        return numeroFascicolo;
    }

    /**
     * Sets the value of the numeroFascicolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumeroFascicolo(Integer value) {
        this.numeroFascicolo = value;
    }

    /**
     * Gets the value of the owner property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOwner() {
        return owner;
    }

    /**
     * Sets the value of the owner property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOwner(String value) {
        this.owner = value;
    }

    /**
     * Gets the value of the proprietario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProprietario() {
        return proprietario;
    }

    /**
     * Sets the value of the proprietario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProprietario(String value) {
        this.proprietario = value;
    }

    /**
     * Gets the value of the proprietarioCf property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProprietarioCf() {
        return proprietarioCf;
    }

    /**
     * Sets the value of the proprietarioCf property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProprietarioCf(String value) {
        this.proprietarioCf = value;
    }

    /**
     * Gets the value of the responsabile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponsabile() {
        return responsabile;
    }

    /**
     * Sets the value of the responsabile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponsabile(String value) {
        this.responsabile = value;
    }

    /**
     * Gets the value of the stato property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getStato() {
        return stato;
    }

    /**
     * Sets the value of the stato property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setStato(Integer value) {
        this.stato = value;
    }

    /**
     * Gets the value of the tipologia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipologia() {
        return tipologia;
    }

    /**
     * Sets the value of the tipologia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipologia(String value) {
        this.tipologia = value;
    }

    /**
     * Gets the value of the visibile property.
     * 
     */
    public boolean isVisibile() {
        return visibile;
    }

    /**
     * Sets the value of the visibile property.
     * 
     */
    public void setVisibile(boolean value) {
        this.visibile = value;
    }

    /**
     * Gets the value of the iChronicleId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIChronicleId() {
        return iChronicleId;
    }

    /**
     * Sets the value of the iChronicleId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIChronicleId(String value) {
        this.iChronicleId = value;
    }

}
