
package it.sogei.gestionedocumentale.documentale.ejbprotocollo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for soggettiInteressatiListType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="soggettiInteressatiListType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="soggInteressato" type="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}soggettoInteressatoType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "soggettiInteressatiListType", propOrder = {
    "soggInteressato"
})
public class SoggettiInteressatiListType {

    @XmlElement(nillable = true)
    protected List<SoggettoInteressatoType> soggInteressato;

    /**
     * Gets the value of the soggInteressato property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the soggInteressato property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSoggInteressato().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SoggettoInteressatoType }
     * 
     * 
     */
    public List<SoggettoInteressatoType> getSoggInteressato() {
        if (soggInteressato == null) {
            soggInteressato = new ArrayList<SoggettoInteressatoType>();
        }
        return this.soggInteressato;
    }

}
