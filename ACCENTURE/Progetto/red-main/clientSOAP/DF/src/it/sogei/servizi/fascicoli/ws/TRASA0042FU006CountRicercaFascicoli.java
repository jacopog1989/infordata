
package it.sogei.servizi.fascicoli.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TRA-SA-0042_FU_006_countRicercaFascicoli complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TRA-SA-0042_FU_006_countRicercaFascicoli">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arg0" type="{http://ws.fascicoli.servizi.sogei.it/}authenticationBean" minOccurs="0"/>
 *         &lt;element name="arg1" type="{http://ws.fascicoli.servizi.sogei.it/}ricercaFascicoli" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TRA-SA-0042_FU_006_countRicercaFascicoli", propOrder = {
    "arg0",
    "arg1"
})
public class TRASA0042FU006CountRicercaFascicoli {

    protected AuthenticationBean arg0;
    protected RicercaFascicoli arg1;

    /**
     * Gets the value of the arg0 property.
     * 
     * @return
     *     possible object is
     *     {@link AuthenticationBean }
     *     
     */
    public AuthenticationBean getArg0() {
        return arg0;
    }

    /**
     * Sets the value of the arg0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthenticationBean }
     *     
     */
    public void setArg0(AuthenticationBean value) {
        this.arg0 = value;
    }

    /**
     * Gets the value of the arg1 property.
     * 
     * @return
     *     possible object is
     *     {@link RicercaFascicoli }
     *     
     */
    public RicercaFascicoli getArg1() {
        return arg1;
    }

    /**
     * Sets the value of the arg1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link RicercaFascicoli }
     *     
     */
    public void setArg1(RicercaFascicoli value) {
        this.arg1 = value;
    }

}
