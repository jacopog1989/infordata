
package it.sogei.gestionedocumentale.documentale.ejbprotocollo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for richiestaUpdateDocumentiBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiestaUpdateDocumentiBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="anno" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codAoo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codRegistro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="metadati" type="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}metadatoBean" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="numeroProto" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="tipologiaDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiestaUpdateDocumentiBean", propOrder = {
    "anno",
    "codAoo",
    "codRegistro",
    "metadati",
    "numeroProto",
    "tipologiaDocumento"
})
public class RichiestaUpdateDocumentiBean {

    protected int anno;
    protected String codAoo;
    protected String codRegistro;
    @XmlElement(nillable = true)
    protected List<MetadatoBean> metadati;
    protected Long numeroProto;
    protected String tipologiaDocumento;

    /**
     * Gets the value of the anno property.
     * 
     */
    public int getAnno() {
        return anno;
    }

    /**
     * Sets the value of the anno property.
     * 
     */
    public void setAnno(int value) {
        this.anno = value;
    }

    /**
     * Gets the value of the codAoo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodAoo() {
        return codAoo;
    }

    /**
     * Sets the value of the codAoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodAoo(String value) {
        this.codAoo = value;
    }

    /**
     * Gets the value of the codRegistro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodRegistro() {
        return codRegistro;
    }

    /**
     * Sets the value of the codRegistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodRegistro(String value) {
        this.codRegistro = value;
    }

    /**
     * Gets the value of the metadati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the metadati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMetadati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MetadatoBean }
     * 
     * 
     */
    public List<MetadatoBean> getMetadati() {
        if (metadati == null) {
            metadati = new ArrayList<MetadatoBean>();
        }
        return this.metadati;
    }

    /**
     * Gets the value of the numeroProto property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumeroProto() {
        return numeroProto;
    }

    /**
     * Sets the value of the numeroProto property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumeroProto(Long value) {
        this.numeroProto = value;
    }

    /**
     * Gets the value of the tipologiaDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipologiaDocumento() {
        return tipologiaDocumento;
    }

    /**
     * Sets the value of the tipologiaDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipologiaDocumento(String value) {
        this.tipologiaDocumento = value;
    }

}
