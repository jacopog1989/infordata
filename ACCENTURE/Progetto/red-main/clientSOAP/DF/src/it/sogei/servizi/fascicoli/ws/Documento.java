
package it.sogei.servizi.fascicoli.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for documento complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="documento">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="classeDocumentale" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descrizione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idFolderId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="listaMetadati" type="{http://ws.fascicoli.servizi.sogei.it/}beanMetadato" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="nome" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="protocollo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="segnatura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="iChronicleId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "documento", propOrder = {
    "classeDocumentale",
    "descrizione",
    "idFolderId",
    "listaMetadati",
    "nome",
    "protocollo",
    "segnatura",
    "iChronicleId"
})
public class Documento {

    protected String classeDocumentale;
    protected String descrizione;
    protected String idFolderId;
    @XmlElement(nillable = true)
    protected List<BeanMetadato> listaMetadati;
    protected String nome;
    protected boolean protocollo;
    protected String segnatura;
    protected String iChronicleId;

    /**
     * Gets the value of the classeDocumentale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClasseDocumentale() {
        return classeDocumentale;
    }

    /**
     * Sets the value of the classeDocumentale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClasseDocumentale(String value) {
        this.classeDocumentale = value;
    }

    /**
     * Gets the value of the descrizione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Sets the value of the descrizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Gets the value of the idFolderId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFolderId() {
        return idFolderId;
    }

    /**
     * Sets the value of the idFolderId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFolderId(String value) {
        this.idFolderId = value;
    }

    /**
     * Gets the value of the listaMetadati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaMetadati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaMetadati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BeanMetadato }
     * 
     * 
     */
    public List<BeanMetadato> getListaMetadati() {
        if (listaMetadati == null) {
            listaMetadati = new ArrayList<BeanMetadato>();
        }
        return this.listaMetadati;
    }

    /**
     * Gets the value of the nome property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNome() {
        return nome;
    }

    /**
     * Sets the value of the nome property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNome(String value) {
        this.nome = value;
    }

    /**
     * Gets the value of the protocollo property.
     * 
     */
    public boolean isProtocollo() {
        return protocollo;
    }

    /**
     * Sets the value of the protocollo property.
     * 
     */
    public void setProtocollo(boolean value) {
        this.protocollo = value;
    }

    /**
     * Gets the value of the segnatura property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSegnatura() {
        return segnatura;
    }

    /**
     * Sets the value of the segnatura property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSegnatura(String value) {
        this.segnatura = value;
    }

    /**
     * Gets the value of the iChronicleId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIChronicleId() {
        return iChronicleId;
    }

    /**
     * Sets the value of the iChronicleId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIChronicleId(String value) {
        this.iChronicleId = value;
    }

}
