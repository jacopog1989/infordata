
package it.sogei.gestionedocumentale.documentale.ejbprotocollo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for protocollaType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="protocollaType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codiceAoo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataProtocollazione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="docId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fk_flusso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagPubblicazione" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="listaAllegati" type="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}allegatiListType" minOccurs="0"/>
 *         &lt;element name="listaSoggettiInteressati" type="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}soggettiInteressatiListType" minOccurs="0"/>
 *         &lt;element name="mittDest" type="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}mittenteDestinatarioType" minOccurs="0"/>
 *         &lt;element name="modalita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeRegistro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroProtocollo" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="oggetto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="protocolloCollegato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoRepository" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="ufficioProtocollatore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uri" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "protocollaType", propOrder = {
    "codiceAoo",
    "dataProtocollazione",
    "docId",
    "ente",
    "fkFlusso",
    "flagPubblicazione",
    "listaAllegati",
    "listaSoggettiInteressati",
    "mittDest",
    "modalita",
    "nomeRegistro",
    "numeroProtocollo",
    "oggetto",
    "protocolloCollegato",
    "tipoRepository",
    "ufficioProtocollatore",
    "uri"
})
public class ProtocollaType {

    protected String codiceAoo;
    protected String dataProtocollazione;
    protected String docId;
    protected String ente;
    @XmlElement(name = "fk_flusso")
    protected String fkFlusso;
    protected int flagPubblicazione;
    protected AllegatiListType listaAllegati;
    protected SoggettiInteressatiListType listaSoggettiInteressati;
    protected MittenteDestinatarioType mittDest;
    protected String modalita;
    protected String nomeRegistro;
    protected Long numeroProtocollo;
    protected String oggetto;
    protected String protocolloCollegato;
    protected short tipoRepository;
    protected String ufficioProtocollatore;
    protected String uri;

    /**
     * Gets the value of the codiceAoo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceAoo() {
        return codiceAoo;
    }

    /**
     * Sets the value of the codiceAoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceAoo(String value) {
        this.codiceAoo = value;
    }

    /**
     * Gets the value of the dataProtocollazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataProtocollazione() {
        return dataProtocollazione;
    }

    /**
     * Sets the value of the dataProtocollazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataProtocollazione(String value) {
        this.dataProtocollazione = value;
    }

    /**
     * Gets the value of the docId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocId() {
        return docId;
    }

    /**
     * Sets the value of the docId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocId(String value) {
        this.docId = value;
    }

    /**
     * Gets the value of the ente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnte() {
        return ente;
    }

    /**
     * Sets the value of the ente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnte(String value) {
        this.ente = value;
    }

    /**
     * Gets the value of the fkFlusso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFkFlusso() {
        return fkFlusso;
    }

    /**
     * Sets the value of the fkFlusso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFkFlusso(String value) {
        this.fkFlusso = value;
    }

    /**
     * Gets the value of the flagPubblicazione property.
     * 
     */
    public int getFlagPubblicazione() {
        return flagPubblicazione;
    }

    /**
     * Sets the value of the flagPubblicazione property.
     * 
     */
    public void setFlagPubblicazione(int value) {
        this.flagPubblicazione = value;
    }

    /**
     * Gets the value of the listaAllegati property.
     * 
     * @return
     *     possible object is
     *     {@link AllegatiListType }
     *     
     */
    public AllegatiListType getListaAllegati() {
        return listaAllegati;
    }

    /**
     * Sets the value of the listaAllegati property.
     * 
     * @param value
     *     allowed object is
     *     {@link AllegatiListType }
     *     
     */
    public void setListaAllegati(AllegatiListType value) {
        this.listaAllegati = value;
    }

    /**
     * Gets the value of the listaSoggettiInteressati property.
     * 
     * @return
     *     possible object is
     *     {@link SoggettiInteressatiListType }
     *     
     */
    public SoggettiInteressatiListType getListaSoggettiInteressati() {
        return listaSoggettiInteressati;
    }

    /**
     * Sets the value of the listaSoggettiInteressati property.
     * 
     * @param value
     *     allowed object is
     *     {@link SoggettiInteressatiListType }
     *     
     */
    public void setListaSoggettiInteressati(SoggettiInteressatiListType value) {
        this.listaSoggettiInteressati = value;
    }

    /**
     * Gets the value of the mittDest property.
     * 
     * @return
     *     possible object is
     *     {@link MittenteDestinatarioType }
     *     
     */
    public MittenteDestinatarioType getMittDest() {
        return mittDest;
    }

    /**
     * Sets the value of the mittDest property.
     * 
     * @param value
     *     allowed object is
     *     {@link MittenteDestinatarioType }
     *     
     */
    public void setMittDest(MittenteDestinatarioType value) {
        this.mittDest = value;
    }

    /**
     * Gets the value of the modalita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModalita() {
        return modalita;
    }

    /**
     * Sets the value of the modalita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModalita(String value) {
        this.modalita = value;
    }

    /**
     * Gets the value of the nomeRegistro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeRegistro() {
        return nomeRegistro;
    }

    /**
     * Sets the value of the nomeRegistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeRegistro(String value) {
        this.nomeRegistro = value;
    }

    /**
     * Gets the value of the numeroProtocollo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumeroProtocollo() {
        return numeroProtocollo;
    }

    /**
     * Sets the value of the numeroProtocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumeroProtocollo(Long value) {
        this.numeroProtocollo = value;
    }

    /**
     * Gets the value of the oggetto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggetto() {
        return oggetto;
    }

    /**
     * Sets the value of the oggetto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggetto(String value) {
        this.oggetto = value;
    }

    /**
     * Gets the value of the protocolloCollegato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProtocolloCollegato() {
        return protocolloCollegato;
    }

    /**
     * Sets the value of the protocolloCollegato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProtocolloCollegato(String value) {
        this.protocolloCollegato = value;
    }

    /**
     * Gets the value of the tipoRepository property.
     * 
     */
    public short getTipoRepository() {
        return tipoRepository;
    }

    /**
     * Sets the value of the tipoRepository property.
     * 
     */
    public void setTipoRepository(short value) {
        this.tipoRepository = value;
    }

    /**
     * Gets the value of the ufficioProtocollatore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUfficioProtocollatore() {
        return ufficioProtocollatore;
    }

    /**
     * Sets the value of the ufficioProtocollatore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUfficioProtocollatore(String value) {
        this.ufficioProtocollatore = value;
    }

    /**
     * Gets the value of the uri property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUri() {
        return uri;
    }

    /**
     * Sets the value of the uri property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUri(String value) {
        this.uri = value;
    }

}
