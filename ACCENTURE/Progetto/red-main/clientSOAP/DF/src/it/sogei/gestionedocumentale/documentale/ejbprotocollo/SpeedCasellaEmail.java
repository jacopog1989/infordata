
package it.sogei.gestionedocumentale.documentale.ejbprotocollo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for speedCasellaEmail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="speedCasellaEmail">
 *   &lt;complexContent>
 *     &lt;extension base="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}speedDataEntity">
 *       &lt;sequence>
 *         &lt;element name="cambiaPassword" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codiceAoo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="connIngressoSicura" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="connUscitaAutenticata" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="connUscitaSicura" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="dataUltimaLetturaPosta" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="descAoo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descTipoCasella" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descUfficioProtocolAutomat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descrizione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descrizioneCasella" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagAbilita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagAbilitaDemone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagRubricaAooVisibile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="folderIngresso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idAoo" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idTipoCasella" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idUfficioProtocolAutomat" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="indirizzonot" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="intervallo" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="messaggioErroreUltimaLetturaPosta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modalita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="portaIngresso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="portaUscita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="protocol" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="providerCert" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="server" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serverUscita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoCasella" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="titolarePEC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ufficiale" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="utente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="utenzaWebService" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "speedCasellaEmail", propOrder = {
    "cambiaPassword",
    "codiceAoo",
    "connIngressoSicura",
    "connUscitaAutenticata",
    "connUscitaSicura",
    "dataUltimaLetturaPosta",
    "descAoo",
    "descTipoCasella",
    "descUfficioProtocolAutomat",
    "descrizione",
    "descrizioneCasella",
    "flagAbilita",
    "flagAbilitaDemone",
    "flagRubricaAooVisibile",
    "folderIngresso",
    "idAoo",
    "idTipoCasella",
    "idUfficioProtocolAutomat",
    "indirizzo",
    "indirizzonot",
    "intervallo",
    "messaggioErroreUltimaLetturaPosta",
    "modalita",
    "password",
    "portaIngresso",
    "portaUscita",
    "protocol",
    "providerCert",
    "server",
    "serverUscita",
    "tipoCasella",
    "titolarePEC",
    "ufficiale",
    "utente",
    "utenzaWebService"
})
public class SpeedCasellaEmail
    extends SpeedDataEntity
{

    protected Integer cambiaPassword;
    protected String codiceAoo;
    protected Integer connIngressoSicura;
    protected Integer connUscitaAutenticata;
    protected Integer connUscitaSicura;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataUltimaLetturaPosta;
    protected String descAoo;
    protected String descTipoCasella;
    protected String descUfficioProtocolAutomat;
    protected String descrizione;
    protected String descrizioneCasella;
    protected String flagAbilita;
    protected String flagAbilitaDemone;
    protected String flagRubricaAooVisibile;
    protected String folderIngresso;
    protected Long idAoo;
    protected String idTipoCasella;
    protected Long idUfficioProtocolAutomat;
    protected String indirizzo;
    protected String indirizzonot;
    protected Long intervallo;
    protected String messaggioErroreUltimaLetturaPosta;
    protected String modalita;
    protected String password;
    protected String portaIngresso;
    protected String portaUscita;
    protected String protocol;
    protected String providerCert;
    protected String server;
    protected String serverUscita;
    protected String tipoCasella;
    protected String titolarePEC;
    protected Integer ufficiale;
    protected String utente;
    protected String utenzaWebService;

    /**
     * Gets the value of the cambiaPassword property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCambiaPassword() {
        return cambiaPassword;
    }

    /**
     * Sets the value of the cambiaPassword property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCambiaPassword(Integer value) {
        this.cambiaPassword = value;
    }

    /**
     * Gets the value of the codiceAoo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceAoo() {
        return codiceAoo;
    }

    /**
     * Sets the value of the codiceAoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceAoo(String value) {
        this.codiceAoo = value;
    }

    /**
     * Gets the value of the connIngressoSicura property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getConnIngressoSicura() {
        return connIngressoSicura;
    }

    /**
     * Sets the value of the connIngressoSicura property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setConnIngressoSicura(Integer value) {
        this.connIngressoSicura = value;
    }

    /**
     * Gets the value of the connUscitaAutenticata property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getConnUscitaAutenticata() {
        return connUscitaAutenticata;
    }

    /**
     * Sets the value of the connUscitaAutenticata property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setConnUscitaAutenticata(Integer value) {
        this.connUscitaAutenticata = value;
    }

    /**
     * Gets the value of the connUscitaSicura property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getConnUscitaSicura() {
        return connUscitaSicura;
    }

    /**
     * Sets the value of the connUscitaSicura property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setConnUscitaSicura(Integer value) {
        this.connUscitaSicura = value;
    }

    /**
     * Gets the value of the dataUltimaLetturaPosta property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataUltimaLetturaPosta() {
        return dataUltimaLetturaPosta;
    }

    /**
     * Sets the value of the dataUltimaLetturaPosta property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataUltimaLetturaPosta(XMLGregorianCalendar value) {
        this.dataUltimaLetturaPosta = value;
    }

    /**
     * Gets the value of the descAoo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescAoo() {
        return descAoo;
    }

    /**
     * Sets the value of the descAoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescAoo(String value) {
        this.descAoo = value;
    }

    /**
     * Gets the value of the descTipoCasella property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTipoCasella() {
        return descTipoCasella;
    }

    /**
     * Sets the value of the descTipoCasella property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTipoCasella(String value) {
        this.descTipoCasella = value;
    }

    /**
     * Gets the value of the descUfficioProtocolAutomat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescUfficioProtocolAutomat() {
        return descUfficioProtocolAutomat;
    }

    /**
     * Sets the value of the descUfficioProtocolAutomat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescUfficioProtocolAutomat(String value) {
        this.descUfficioProtocolAutomat = value;
    }

    /**
     * Gets the value of the descrizione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Sets the value of the descrizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Gets the value of the descrizioneCasella property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizioneCasella() {
        return descrizioneCasella;
    }

    /**
     * Sets the value of the descrizioneCasella property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizioneCasella(String value) {
        this.descrizioneCasella = value;
    }

    /**
     * Gets the value of the flagAbilita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagAbilita() {
        return flagAbilita;
    }

    /**
     * Sets the value of the flagAbilita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagAbilita(String value) {
        this.flagAbilita = value;
    }

    /**
     * Gets the value of the flagAbilitaDemone property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagAbilitaDemone() {
        return flagAbilitaDemone;
    }

    /**
     * Sets the value of the flagAbilitaDemone property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagAbilitaDemone(String value) {
        this.flagAbilitaDemone = value;
    }

    /**
     * Gets the value of the flagRubricaAooVisibile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagRubricaAooVisibile() {
        return flagRubricaAooVisibile;
    }

    /**
     * Sets the value of the flagRubricaAooVisibile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagRubricaAooVisibile(String value) {
        this.flagRubricaAooVisibile = value;
    }

    /**
     * Gets the value of the folderIngresso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFolderIngresso() {
        return folderIngresso;
    }

    /**
     * Sets the value of the folderIngresso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFolderIngresso(String value) {
        this.folderIngresso = value;
    }

    /**
     * Gets the value of the idAoo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdAoo() {
        return idAoo;
    }

    /**
     * Sets the value of the idAoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdAoo(Long value) {
        this.idAoo = value;
    }

    /**
     * Gets the value of the idTipoCasella property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdTipoCasella() {
        return idTipoCasella;
    }

    /**
     * Sets the value of the idTipoCasella property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdTipoCasella(String value) {
        this.idTipoCasella = value;
    }

    /**
     * Gets the value of the idUfficioProtocolAutomat property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdUfficioProtocolAutomat() {
        return idUfficioProtocolAutomat;
    }

    /**
     * Sets the value of the idUfficioProtocolAutomat property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdUfficioProtocolAutomat(Long value) {
        this.idUfficioProtocolAutomat = value;
    }

    /**
     * Gets the value of the indirizzo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndirizzo() {
        return indirizzo;
    }

    /**
     * Sets the value of the indirizzo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndirizzo(String value) {
        this.indirizzo = value;
    }

    /**
     * Gets the value of the indirizzonot property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndirizzonot() {
        return indirizzonot;
    }

    /**
     * Sets the value of the indirizzonot property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndirizzonot(String value) {
        this.indirizzonot = value;
    }

    /**
     * Gets the value of the intervallo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIntervallo() {
        return intervallo;
    }

    /**
     * Sets the value of the intervallo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIntervallo(Long value) {
        this.intervallo = value;
    }

    /**
     * Gets the value of the messaggioErroreUltimaLetturaPosta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessaggioErroreUltimaLetturaPosta() {
        return messaggioErroreUltimaLetturaPosta;
    }

    /**
     * Sets the value of the messaggioErroreUltimaLetturaPosta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessaggioErroreUltimaLetturaPosta(String value) {
        this.messaggioErroreUltimaLetturaPosta = value;
    }

    /**
     * Gets the value of the modalita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModalita() {
        return modalita;
    }

    /**
     * Sets the value of the modalita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModalita(String value) {
        this.modalita = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Gets the value of the portaIngresso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPortaIngresso() {
        return portaIngresso;
    }

    /**
     * Sets the value of the portaIngresso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPortaIngresso(String value) {
        this.portaIngresso = value;
    }

    /**
     * Gets the value of the portaUscita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPortaUscita() {
        return portaUscita;
    }

    /**
     * Sets the value of the portaUscita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPortaUscita(String value) {
        this.portaUscita = value;
    }

    /**
     * Gets the value of the protocol property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProtocol() {
        return protocol;
    }

    /**
     * Sets the value of the protocol property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProtocol(String value) {
        this.protocol = value;
    }

    /**
     * Gets the value of the providerCert property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProviderCert() {
        return providerCert;
    }

    /**
     * Sets the value of the providerCert property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProviderCert(String value) {
        this.providerCert = value;
    }

    /**
     * Gets the value of the server property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServer() {
        return server;
    }

    /**
     * Sets the value of the server property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServer(String value) {
        this.server = value;
    }

    /**
     * Gets the value of the serverUscita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServerUscita() {
        return serverUscita;
    }

    /**
     * Sets the value of the serverUscita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServerUscita(String value) {
        this.serverUscita = value;
    }

    /**
     * Gets the value of the tipoCasella property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCasella() {
        return tipoCasella;
    }

    /**
     * Sets the value of the tipoCasella property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCasella(String value) {
        this.tipoCasella = value;
    }

    /**
     * Gets the value of the titolarePEC property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitolarePEC() {
        return titolarePEC;
    }

    /**
     * Sets the value of the titolarePEC property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitolarePEC(String value) {
        this.titolarePEC = value;
    }

    /**
     * Gets the value of the ufficiale property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getUfficiale() {
        return ufficiale;
    }

    /**
     * Sets the value of the ufficiale property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setUfficiale(Integer value) {
        this.ufficiale = value;
    }

    /**
     * Gets the value of the utente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUtente() {
        return utente;
    }

    /**
     * Sets the value of the utente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUtente(String value) {
        this.utente = value;
    }

    /**
     * Gets the value of the utenzaWebService property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUtenzaWebService() {
        return utenzaWebService;
    }

    /**
     * Sets the value of the utenzaWebService property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUtenzaWebService(String value) {
        this.utenzaWebService = value;
    }

}
