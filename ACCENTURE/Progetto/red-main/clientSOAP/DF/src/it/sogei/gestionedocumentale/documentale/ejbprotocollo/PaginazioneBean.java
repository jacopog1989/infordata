
package it.sogei.gestionedocumentale.documentale.ejbprotocollo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for paginazioneBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="paginazioneBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AIndex" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="campoSort" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contRicercaRegistro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contRicercaRegistroView" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="currPage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="daIndex" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="existPagSuccessiva" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="infoPrecentiSoloDocVisivili" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="lastRowNum" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="maxNumIteratore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mostraSoloDocVisibili" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numMaxRigheDalDb" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numMaxRighePerPagina" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="orderType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prevPage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="resultVett" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="typeSort" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "paginazioneBean", propOrder = {
    "aIndex",
    "campoSort",
    "contRicercaRegistro",
    "contRicercaRegistroView",
    "currPage",
    "daIndex",
    "existPagSuccessiva",
    "infoPrecentiSoloDocVisivili",
    "lastRowNum",
    "maxNumIteratore",
    "mostraSoloDocVisibili",
    "numMaxRigheDalDb",
    "numMaxRighePerPagina",
    "orderType",
    "prevPage",
    "resultVett",
    "typeSort"
})
public class PaginazioneBean {

    @XmlElement(name = "AIndex")
    protected String aIndex;
    protected String campoSort;
    protected String contRicercaRegistro;
    protected String contRicercaRegistroView;
    protected String currPage;
    protected String daIndex;
    protected String existPagSuccessiva;
    @XmlElement(nillable = true)
    protected List<Object> infoPrecentiSoloDocVisivili;
    protected String lastRowNum;
    protected String maxNumIteratore;
    protected String mostraSoloDocVisibili;
    protected String numMaxRigheDalDb;
    protected String numMaxRighePerPagina;
    protected String orderType;
    protected String prevPage;
    @XmlElement(nillable = true)
    protected List<Object> resultVett;
    protected String typeSort;

    /**
     * Gets the value of the aIndex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAIndex() {
        return aIndex;
    }

    /**
     * Sets the value of the aIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAIndex(String value) {
        this.aIndex = value;
    }

    /**
     * Gets the value of the campoSort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCampoSort() {
        return campoSort;
    }

    /**
     * Sets the value of the campoSort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCampoSort(String value) {
        this.campoSort = value;
    }

    /**
     * Gets the value of the contRicercaRegistro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContRicercaRegistro() {
        return contRicercaRegistro;
    }

    /**
     * Sets the value of the contRicercaRegistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContRicercaRegistro(String value) {
        this.contRicercaRegistro = value;
    }

    /**
     * Gets the value of the contRicercaRegistroView property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContRicercaRegistroView() {
        return contRicercaRegistroView;
    }

    /**
     * Sets the value of the contRicercaRegistroView property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContRicercaRegistroView(String value) {
        this.contRicercaRegistroView = value;
    }

    /**
     * Gets the value of the currPage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrPage() {
        return currPage;
    }

    /**
     * Sets the value of the currPage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrPage(String value) {
        this.currPage = value;
    }

    /**
     * Gets the value of the daIndex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDaIndex() {
        return daIndex;
    }

    /**
     * Sets the value of the daIndex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDaIndex(String value) {
        this.daIndex = value;
    }

    /**
     * Gets the value of the existPagSuccessiva property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExistPagSuccessiva() {
        return existPagSuccessiva;
    }

    /**
     * Sets the value of the existPagSuccessiva property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExistPagSuccessiva(String value) {
        this.existPagSuccessiva = value;
    }

    /**
     * Gets the value of the infoPrecentiSoloDocVisivili property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the infoPrecentiSoloDocVisivili property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInfoPrecentiSoloDocVisivili().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getInfoPrecentiSoloDocVisivili() {
        if (infoPrecentiSoloDocVisivili == null) {
            infoPrecentiSoloDocVisivili = new ArrayList<Object>();
        }
        return this.infoPrecentiSoloDocVisivili;
    }

    /**
     * Gets the value of the lastRowNum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastRowNum() {
        return lastRowNum;
    }

    /**
     * Sets the value of the lastRowNum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastRowNum(String value) {
        this.lastRowNum = value;
    }

    /**
     * Gets the value of the maxNumIteratore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxNumIteratore() {
        return maxNumIteratore;
    }

    /**
     * Sets the value of the maxNumIteratore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxNumIteratore(String value) {
        this.maxNumIteratore = value;
    }

    /**
     * Gets the value of the mostraSoloDocVisibili property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMostraSoloDocVisibili() {
        return mostraSoloDocVisibili;
    }

    /**
     * Sets the value of the mostraSoloDocVisibili property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMostraSoloDocVisibili(String value) {
        this.mostraSoloDocVisibili = value;
    }

    /**
     * Gets the value of the numMaxRigheDalDb property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumMaxRigheDalDb() {
        return numMaxRigheDalDb;
    }

    /**
     * Sets the value of the numMaxRigheDalDb property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumMaxRigheDalDb(String value) {
        this.numMaxRigheDalDb = value;
    }

    /**
     * Gets the value of the numMaxRighePerPagina property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumMaxRighePerPagina() {
        return numMaxRighePerPagina;
    }

    /**
     * Sets the value of the numMaxRighePerPagina property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumMaxRighePerPagina(String value) {
        this.numMaxRighePerPagina = value;
    }

    /**
     * Gets the value of the orderType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderType() {
        return orderType;
    }

    /**
     * Sets the value of the orderType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderType(String value) {
        this.orderType = value;
    }

    /**
     * Gets the value of the prevPage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrevPage() {
        return prevPage;
    }

    /**
     * Sets the value of the prevPage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrevPage(String value) {
        this.prevPage = value;
    }

    /**
     * Gets the value of the resultVett property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the resultVett property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getResultVett().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getResultVett() {
        if (resultVett == null) {
            resultVett = new ArrayList<Object>();
        }
        return this.resultVett;
    }

    /**
     * Gets the value of the typeSort property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeSort() {
        return typeSort;
    }

    /**
     * Sets the value of the typeSort property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeSort(String value) {
        this.typeSort = value;
    }

}
