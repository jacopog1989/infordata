
package it.sogei.gestionedocumentale.documentale.ejbprotocollo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for speedProtocolloMessageMananger complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="speedProtocolloMessageMananger">
 *   &lt;complexContent>
 *     &lt;extension base="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}speedBaseManager">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "speedProtocolloMessageMananger")
public class SpeedProtocolloMessageMananger
    extends SpeedBaseManager
{


}
