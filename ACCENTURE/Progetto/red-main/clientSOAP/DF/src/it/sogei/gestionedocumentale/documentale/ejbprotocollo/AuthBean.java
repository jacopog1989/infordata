
package it.sogei.gestionedocumentale.documentale.ejbprotocollo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for authBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="authBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="utente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nomeEnte" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="idFlusso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="utenteOperazione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "authBean", propOrder = {
    "utente",
    "nomeEnte",
    "idFlusso",
    "utenteOperazione"
})
public class AuthBean {

    @XmlElement(required = true)
    protected String utente;
    @XmlElement(required = true)
    protected String nomeEnte;
    @XmlElement(required = true)
    protected String idFlusso;
    protected String utenteOperazione;

    /**
     * Gets the value of the utente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUtente() {
        return utente;
    }

    /**
     * Sets the value of the utente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUtente(String value) {
        this.utente = value;
    }

    /**
     * Gets the value of the nomeEnte property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeEnte() {
        return nomeEnte;
    }

    /**
     * Sets the value of the nomeEnte property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeEnte(String value) {
        this.nomeEnte = value;
    }

    /**
     * Gets the value of the idFlusso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFlusso() {
        return idFlusso;
    }

    /**
     * Sets the value of the idFlusso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFlusso(String value) {
        this.idFlusso = value;
    }

    /**
     * Gets the value of the utenteOperazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUtenteOperazione() {
        return utenteOperazione;
    }

    /**
     * Sets the value of the utenteOperazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUtenteOperazione(String value) {
        this.utenteOperazione = value;
    }

}
