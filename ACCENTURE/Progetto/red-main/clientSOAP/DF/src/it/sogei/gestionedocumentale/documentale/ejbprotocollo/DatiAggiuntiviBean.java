
package it.sogei.gestionedocumentale.documentale.ejbprotocollo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for datiAggiuntiviBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="datiAggiuntiviBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="annoProto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="codiceAoo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiceRegisto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="listaMetadatiAggiuntivi" type="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}metadatiAggiuntiviBean" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="mittenteDestinatario" type="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}mittenteBean" minOccurs="0"/>
 *         &lt;element name="modalita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroProto" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "datiAggiuntiviBean", propOrder = {
    "annoProto",
    "codiceAoo",
    "codiceRegisto",
    "listaMetadatiAggiuntivi",
    "mittenteDestinatario",
    "modalita",
    "numeroProto"
})
public class DatiAggiuntiviBean {

    protected int annoProto;
    protected String codiceAoo;
    protected String codiceRegisto;
    @XmlElement(nillable = true)
    protected List<MetadatiAggiuntiviBean> listaMetadatiAggiuntivi;
    protected MittenteBean mittenteDestinatario;
    protected String modalita;
    protected Long numeroProto;

    /**
     * Gets the value of the annoProto property.
     * 
     */
    public int getAnnoProto() {
        return annoProto;
    }

    /**
     * Sets the value of the annoProto property.
     * 
     */
    public void setAnnoProto(int value) {
        this.annoProto = value;
    }

    /**
     * Gets the value of the codiceAoo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceAoo() {
        return codiceAoo;
    }

    /**
     * Sets the value of the codiceAoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceAoo(String value) {
        this.codiceAoo = value;
    }

    /**
     * Gets the value of the codiceRegisto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceRegisto() {
        return codiceRegisto;
    }

    /**
     * Sets the value of the codiceRegisto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceRegisto(String value) {
        this.codiceRegisto = value;
    }

    /**
     * Gets the value of the listaMetadatiAggiuntivi property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaMetadatiAggiuntivi property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaMetadatiAggiuntivi().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MetadatiAggiuntiviBean }
     * 
     * 
     */
    public List<MetadatiAggiuntiviBean> getListaMetadatiAggiuntivi() {
        if (listaMetadatiAggiuntivi == null) {
            listaMetadatiAggiuntivi = new ArrayList<MetadatiAggiuntiviBean>();
        }
        return this.listaMetadatiAggiuntivi;
    }

    /**
     * Gets the value of the mittenteDestinatario property.
     * 
     * @return
     *     possible object is
     *     {@link MittenteBean }
     *     
     */
    public MittenteBean getMittenteDestinatario() {
        return mittenteDestinatario;
    }

    /**
     * Sets the value of the mittenteDestinatario property.
     * 
     * @param value
     *     allowed object is
     *     {@link MittenteBean }
     *     
     */
    public void setMittenteDestinatario(MittenteBean value) {
        this.mittenteDestinatario = value;
    }

    /**
     * Gets the value of the modalita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModalita() {
        return modalita;
    }

    /**
     * Sets the value of the modalita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModalita(String value) {
        this.modalita = value;
    }

    /**
     * Gets the value of the numeroProto property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumeroProto() {
        return numeroProto;
    }

    /**
     * Sets the value of the numeroProto property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumeroProto(Long value) {
        this.numeroProto = value;
    }

}
