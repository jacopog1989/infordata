
package it.sogei.servizi.fascicoli.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ricercaFascicoli complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ricercaFascicoli">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="annoProtocollo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="codiceAoo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiceRegistro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiceTitolario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataAperturaAl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataAperturaDal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataChiusuraAl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataChiusuraDal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataScadenzaAl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataScadenzaDal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descrTitolario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descrizione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dimensionePagina" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="listaStati" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="nomeCartella" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroFascicolo" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="numeroFascicoloNsd" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="numeroProtocollo" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="order" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pagina" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="responsabile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipologia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ufficioProprietario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ricercaFascicoli", propOrder = {
    "annoProtocollo",
    "codiceAoo",
    "codiceRegistro",
    "codiceTitolario",
    "dataAperturaAl",
    "dataAperturaDal",
    "dataChiusuraAl",
    "dataChiusuraDal",
    "dataScadenzaAl",
    "dataScadenzaDal",
    "descrTitolario",
    "descrizione",
    "dimensionePagina",
    "listaStati",
    "nomeCartella",
    "note",
    "numeroFascicolo",
    "numeroFascicoloNsd",
    "numeroProtocollo",
    "order",
    "pagina",
    "responsabile",
    "tipologia",
    "ufficioProprietario"
})
public class RicercaFascicoli {

    protected Integer annoProtocollo;
    protected String codiceAoo;
    protected String codiceRegistro;
    protected String codiceTitolario;
    protected String dataAperturaAl;
    protected String dataAperturaDal;
    protected String dataChiusuraAl;
    protected String dataChiusuraDal;
    protected String dataScadenzaAl;
    protected String dataScadenzaDal;
    protected String descrTitolario;
    protected String descrizione;
    protected int dimensionePagina;
    @XmlElement(nillable = true)
    protected List<String> listaStati;
    protected String nomeCartella;
    protected String note;
    protected Integer numeroFascicolo;
    protected Integer numeroFascicoloNsd;
    protected Long numeroProtocollo;
    protected String order;
    protected int pagina;
    protected String responsabile;
    protected String tipologia;
    protected String ufficioProprietario;

    /**
     * Gets the value of the annoProtocollo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAnnoProtocollo() {
        return annoProtocollo;
    }

    /**
     * Sets the value of the annoProtocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAnnoProtocollo(Integer value) {
        this.annoProtocollo = value;
    }

    /**
     * Gets the value of the codiceAoo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceAoo() {
        return codiceAoo;
    }

    /**
     * Sets the value of the codiceAoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceAoo(String value) {
        this.codiceAoo = value;
    }

    /**
     * Gets the value of the codiceRegistro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceRegistro() {
        return codiceRegistro;
    }

    /**
     * Sets the value of the codiceRegistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceRegistro(String value) {
        this.codiceRegistro = value;
    }

    /**
     * Gets the value of the codiceTitolario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceTitolario() {
        return codiceTitolario;
    }

    /**
     * Sets the value of the codiceTitolario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceTitolario(String value) {
        this.codiceTitolario = value;
    }

    /**
     * Gets the value of the dataAperturaAl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataAperturaAl() {
        return dataAperturaAl;
    }

    /**
     * Sets the value of the dataAperturaAl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataAperturaAl(String value) {
        this.dataAperturaAl = value;
    }

    /**
     * Gets the value of the dataAperturaDal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataAperturaDal() {
        return dataAperturaDal;
    }

    /**
     * Sets the value of the dataAperturaDal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataAperturaDal(String value) {
        this.dataAperturaDal = value;
    }

    /**
     * Gets the value of the dataChiusuraAl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataChiusuraAl() {
        return dataChiusuraAl;
    }

    /**
     * Sets the value of the dataChiusuraAl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataChiusuraAl(String value) {
        this.dataChiusuraAl = value;
    }

    /**
     * Gets the value of the dataChiusuraDal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataChiusuraDal() {
        return dataChiusuraDal;
    }

    /**
     * Sets the value of the dataChiusuraDal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataChiusuraDal(String value) {
        this.dataChiusuraDal = value;
    }

    /**
     * Gets the value of the dataScadenzaAl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataScadenzaAl() {
        return dataScadenzaAl;
    }

    /**
     * Sets the value of the dataScadenzaAl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataScadenzaAl(String value) {
        this.dataScadenzaAl = value;
    }

    /**
     * Gets the value of the dataScadenzaDal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataScadenzaDal() {
        return dataScadenzaDal;
    }

    /**
     * Sets the value of the dataScadenzaDal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataScadenzaDal(String value) {
        this.dataScadenzaDal = value;
    }

    /**
     * Gets the value of the descrTitolario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrTitolario() {
        return descrTitolario;
    }

    /**
     * Sets the value of the descrTitolario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrTitolario(String value) {
        this.descrTitolario = value;
    }

    /**
     * Gets the value of the descrizione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Sets the value of the descrizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Gets the value of the dimensionePagina property.
     * 
     */
    public int getDimensionePagina() {
        return dimensionePagina;
    }

    /**
     * Sets the value of the dimensionePagina property.
     * 
     */
    public void setDimensionePagina(int value) {
        this.dimensionePagina = value;
    }

    /**
     * Gets the value of the listaStati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaStati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaStati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getListaStati() {
        if (listaStati == null) {
            listaStati = new ArrayList<String>();
        }
        return this.listaStati;
    } 
    
    public void setListaStati(List<String> listaStati) {
       this.listaStati = listaStati;
     } 

    /**
     * Gets the value of the nomeCartella property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeCartella() {
        return nomeCartella;
    }

    /**
     * Sets the value of the nomeCartella property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeCartella(String value) {
        this.nomeCartella = value;
    }

    /**
     * Gets the value of the note property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNote() {
        return note;
    }

    /**
     * Sets the value of the note property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNote(String value) {
        this.note = value;
    }

    /**
     * Gets the value of the numeroFascicolo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumeroFascicolo() {
        return numeroFascicolo;
    }

    /**
     * Sets the value of the numeroFascicolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumeroFascicolo(Integer value) {
        this.numeroFascicolo = value;
    }

    /**
     * Gets the value of the numeroFascicoloNsd property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumeroFascicoloNsd() {
        return numeroFascicoloNsd;
    }

    /**
     * Sets the value of the numeroFascicoloNsd property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumeroFascicoloNsd(Integer value) {
        this.numeroFascicoloNsd = value;
    }

    /**
     * Gets the value of the numeroProtocollo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumeroProtocollo() {
        return numeroProtocollo;
    }

    /**
     * Sets the value of the numeroProtocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumeroProtocollo(Long value) {
        this.numeroProtocollo = value;
    }

    /**
     * Gets the value of the order property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrder() {
        return order;
    }

    /**
     * Sets the value of the order property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrder(String value) {
        this.order = value;
    }

    /**
     * Gets the value of the pagina property.
     * 
     */
    public int getPagina() {
        return pagina;
    }

    /**
     * Sets the value of the pagina property.
     * 
     */
    public void setPagina(int value) {
        this.pagina = value;
    }

    /**
     * Gets the value of the responsabile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponsabile() {
        return responsabile;
    }

    /**
     * Sets the value of the responsabile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponsabile(String value) {
        this.responsabile = value;
    }

    /**
     * Gets the value of the tipologia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipologia() {
        return tipologia;
    }

    /**
     * Sets the value of the tipologia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipologia(String value) {
        this.tipologia = value;
    }

    /**
     * Gets the value of the ufficioProprietario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUfficioProprietario() {
        return ufficioProprietario;
    }

    /**
     * Sets the value of the ufficioProprietario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUfficioProprietario(String value) {
        this.ufficioProprietario = value;
    }

}
