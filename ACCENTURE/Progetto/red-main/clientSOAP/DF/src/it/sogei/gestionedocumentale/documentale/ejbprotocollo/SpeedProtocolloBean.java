
package it.sogei.gestionedocumentale.documentale.ejbprotocollo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for speedProtocolloBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="speedProtocolloBean">
 *   &lt;complexContent>
 *     &lt;extension base="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}speedActionForm">
 *       &lt;sequence>
 *         &lt;element name="allegati" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="annoDocCollegato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="annoRiferimentoRegistro" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="annullato" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="archiviazioneSostitutiva" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="assegnatarioWF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="attributi" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="checkInComment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="checkedAllegati" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chiuso" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="civilia" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="classificaElements" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="classificazioneSegnatura" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="classifiche" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="cleanErrorMsg" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="codiceURP" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiceUfficio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="collegatiElements" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="collegatiStoriciElements" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="collocazione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="completo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="conferma" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="conservaDati" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="contentDocWorkFlow" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataProtEsterno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataProtocollo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataRegistrazione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataRicezioneSpedizione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataScarico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descGruppoIndirizzo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="divVisibility" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="docCheCollegano" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="docCheColleganoStorico" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="docIncompleto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="docProtId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="docProtIdValore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="docSize" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="docSupporto" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="documentoUnico" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="dummyDocService" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="emergenza" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="erroreAttributi" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="extension" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="firmatario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fkStatoPubblicazione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagAllegatiChecked" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="flagConoscenza" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagInPratica" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="flagWorkFlow" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="folderAllegati" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="forceTipoGenerico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="iconaFileOriginale" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idAllegati" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="idAllegatiWorkFlow" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idAoo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idAssegnazione" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idCasellaEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idDocCollegati" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idDocumentale" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idDocumentoDaPratica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idDocumentoOriginale" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idFolderPratica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idLivelloRiservatezza" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idPratica" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idTaskWorkFlow" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idTipologiaDocumento" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="idUfficio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="indirizzo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ingresso" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="iniCaselle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="iniFlagConoscenza" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="iniMezzoSpedizioni" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="iniTmpCaselle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="iniTmpFlagConoscenza" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="iniTmpMezzoSpedizioni" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="interesseMase" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="is3BisEnabled" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="isTrasmissione" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="isTrasmissioneCDC" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="listaRicevute" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="messageManager" type="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}speedProtocolloMessageMananger" minOccurs="0"/>
 *         &lt;element name="mezzoSpedizione" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="mezzoSpedizioni" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mittDest" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="mittdestPG" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="modalita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nameListMittDest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nameListMittDestAnnullati" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="noAnnullamentoMittDest" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="noMittDest" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="nomeCartellaWF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeFile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeFileContenuto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="notaAnnullamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroAllegati" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numeroProtEsterno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nuovoContenuto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nuovoContenutoOriginale" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="oggetto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="originalDocId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pathDocWorkFlow" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="percorsoGuidato" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="permessoModifica" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="permessoRiservato" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="presenzaCecPac" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="presenzaDatiSensibili" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="presenzaMase" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="presoInCaricoDaMase" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="priorita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="protocollatore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="provvedimentoAnnullamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pubblicato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="registroId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="registroName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="riferimentoAreaDiScambio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="riservato" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="searchCmd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secureId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secureIdDocumentoDaPratica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secureIdFolder" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secureIdPratica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="secureRegistroId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sequenzaProtocollo" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="smistamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="soggInteressati" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="stato" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="statoArchiviazione" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="statoContenuto" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="strCaselle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoOriginale" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoRicerca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoRubrica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipologia" type="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}speedTipologieDocumento" minOccurs="0"/>
 *         &lt;element name="tmpAnnullato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tmpCaselle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tmpCompleto" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="tmpFlagConoscenza" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tmpMezzoSpedizioni" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tmpMittDest" type="{http://www.w3.org/2001/XMLSchema}anyType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="tornaAllaMailCmd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="typeDocumentWF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="typeEstensioneDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uffAssegnatarioWF" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ufficioProtocollatore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ultimoProgressivo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="urlImgI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="urlImgU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uscita" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="useMascheraCumulativa" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="userIdProtocollatore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valuesAttrExt">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="entry" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                             &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="valuesAttrInvioCDC">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="entry" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                             &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="valuesAttrRegAux">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="entry" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                             &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "speedProtocolloBean", propOrder = {
    "allegati",
    "annoDocCollegato",
    "annoRiferimentoRegistro",
    "annullato",
    "archiviazioneSostitutiva",
    "assegnatarioWF",
    "attributi",
    "checkInComment",
    "checkedAllegati",
    "chiuso",
    "civilia",
    "classificaElements",
    "classificazioneSegnatura",
    "classifiche",
    "cleanErrorMsg",
    "codiceURP",
    "codiceUfficio",
    "collegatiElements",
    "collegatiStoriciElements",
    "collocazione",
    "completo",
    "conferma",
    "conservaDati",
    "contentDocWorkFlow",
    "dataProtEsterno",
    "dataProtocollo",
    "dataRegistrazione",
    "dataRicezioneSpedizione",
    "dataScarico",
    "descGruppoIndirizzo",
    "divVisibility",
    "docCheCollegano",
    "docCheColleganoStorico",
    "docIncompleto",
    "docProtId",
    "docProtIdValore",
    "docSize",
    "docSupporto",
    "documentoUnico",
    "dummyDocService",
    "emergenza",
    "erroreAttributi",
    "extension",
    "firmatario",
    "fkStatoPubblicazione",
    "flagAllegatiChecked",
    "flagConoscenza",
    "flagInPratica",
    "flagWorkFlow",
    "folderAllegati",
    "forceTipoGenerico",
    "iconaFileOriginale",
    "idAllegati",
    "idAllegatiWorkFlow",
    "idAoo",
    "idAssegnazione",
    "idCasellaEmail",
    "idDocCollegati",
    "idDocumentale",
    "idDocumentoDaPratica",
    "idDocumentoOriginale",
    "idEmail",
    "idFolderPratica",
    "idLivelloRiservatezza",
    "idPratica",
    "idR",
    "idTaskWorkFlow",
    "idTipologiaDocumento",
    "idUfficio",
    "indirizzo",
    "ingresso",
    "iniCaselle",
    "iniFlagConoscenza",
    "iniMezzoSpedizioni",
    "iniTmpCaselle",
    "iniTmpFlagConoscenza",
    "iniTmpMezzoSpedizioni",
    "interesseMase",
    "is3BisEnabled",
    "isTrasmissione",
    "isTrasmissioneCDC",
    "listaRicevute",
    "messageManager",
    "mezzoSpedizione",
    "mezzoSpedizioni",
    "mittDest",
    "mittdestPG",
    "modalita",
    "nameListMittDest",
    "nameListMittDestAnnullati",
    "noAnnullamentoMittDest",
    "noMittDest",
    "nomeCartellaWF",
    "nomeFile",
    "nomeFileContenuto",
    "notaAnnullamento",
    "note",
    "numeroAllegati",
    "numeroProtEsterno",
    "nuovoContenuto",
    "nuovoContenutoOriginale",
    "oggetto",
    "originalDocId",
    "pathDocWorkFlow",
    "percorsoGuidato",
    "permessoModifica",
    "permessoRiservato",
    "presenzaCecPac",
    "presenzaDatiSensibili",
    "presenzaMase",
    "presoInCaricoDaMase",
    "priorita",
    "protocollatore",
    "provvedimentoAnnullamento",
    "pubblicato",
    "registroId",
    "registroName",
    "riferimentoAreaDiScambio",
    "riservato",
    "searchCmd",
    "secureId",
    "secureIdDocumentoDaPratica",
    "secureIdFolder",
    "secureIdPratica",
    "secureRegistroId",
    "sequenzaProtocollo",
    "smistamento",
    "soggInteressati",
    "stato",
    "statoArchiviazione",
    "statoContenuto",
    "strCaselle",
    "tipoOriginale",
    "tipoRicerca",
    "tipoRubrica",
    "tipologia",
    "tmpAnnullato",
    "tmpCaselle",
    "tmpCompleto",
    "tmpFlagConoscenza",
    "tmpMezzoSpedizioni",
    "tmpMittDest",
    "tornaAllaMailCmd",
    "typeDocumentWF",
    "typeEstensioneDocumento",
    "uffAssegnatarioWF",
    "ufficioProtocollatore",
    "ultimoProgressivo",
    "urlImgI",
    "urlImgU",
    "uscita",
    "useMascheraCumulativa",
    "userIdProtocollatore",
    "valuesAttrExt",
    "valuesAttrInvioCDC",
    "valuesAttrRegAux"
})
public class SpeedProtocolloBean
    extends SpeedActionForm
{

    @XmlElement(nillable = true)
    protected List<Object> allegati;
    protected String annoDocCollegato;
    protected Integer annoRiferimentoRegistro;
    protected boolean annullato;
    protected boolean archiviazioneSostitutiva;
    protected String assegnatarioWF;
    protected boolean attributi;
    protected String checkInComment;
    protected String checkedAllegati;
    protected boolean chiuso;
    protected boolean civilia;
    @XmlElement(nillable = true)
    protected List<Object> classificaElements;
    protected String classificazioneSegnatura;
    @XmlElement(nillable = true)
    protected List<String> classifiche;
    protected boolean cleanErrorMsg;
    protected String codiceURP;
    protected String codiceUfficio;
    @XmlElement(nillable = true)
    protected List<Object> collegatiElements;
    @XmlElement(nillable = true)
    protected List<Object> collegatiStoriciElements;
    protected String collocazione;
    protected boolean completo;
    protected boolean conferma;
    protected boolean conservaDati;
    protected String contentDocWorkFlow;
    protected String dataProtEsterno;
    protected String dataProtocollo;
    protected String dataRegistrazione;
    protected String dataRicezioneSpedizione;
    protected String dataScarico;
    protected String descGruppoIndirizzo;
    protected boolean divVisibility;
    @XmlElement(nillable = true)
    protected List<Object> docCheCollegano;
    @XmlElement(nillable = true)
    protected List<Object> docCheColleganoStorico;
    protected String docIncompleto;
    protected Long docProtId;
    protected String docProtIdValore;
    protected Long docSize;
    @XmlElement(nillable = true)
    protected List<String> docSupporto;
    protected Integer documentoUnico;
    protected boolean dummyDocService;
    protected String emergenza;
    protected boolean erroreAttributi;
    protected String extension;
    protected String firmatario;
    protected String fkStatoPubblicazione;
    protected boolean flagAllegatiChecked;
    protected String flagConoscenza;
    protected long flagInPratica;
    protected String flagWorkFlow;
    @XmlElement(nillable = true)
    protected List<String> folderAllegati;
    protected String forceTipoGenerico;
    protected String iconaFileOriginale;
    @XmlElement(nillable = true)
    protected List<String> idAllegati;
    protected String idAllegatiWorkFlow;
    protected String idAoo;
    protected Long idAssegnazione;
    protected String idCasellaEmail;
    protected String idDocCollegati;
    protected String idDocumentale;
    protected String idDocumentoDaPratica;
    protected String idDocumentoOriginale;
    protected String idEmail;
    protected String idFolderPratica;
    protected Long idLivelloRiservatezza;
    protected Long idPratica;
    protected String idR;
    protected String idTaskWorkFlow;
    protected Long idTipologiaDocumento;
    protected String idUfficio;
    protected String indirizzo;
    protected boolean ingresso;
    protected String iniCaselle;
    protected String iniFlagConoscenza;
    protected String iniMezzoSpedizioni;
    protected String iniTmpCaselle;
    protected String iniTmpFlagConoscenza;
    protected String iniTmpMezzoSpedizioni;
    protected boolean interesseMase;
    protected boolean is3BisEnabled;
    protected boolean isTrasmissione;
    protected boolean isTrasmissioneCDC;
    @XmlElement(nillable = true)
    protected List<Object> listaRicevute;
    protected SpeedProtocolloMessageMananger messageManager;
    protected Long mezzoSpedizione;
    protected String mezzoSpedizioni;
    @XmlElement(nillable = true)
    protected List<Object> mittDest;
    @XmlElement(nillable = true)
    protected List<Object> mittdestPG;
    protected String modalita;
    protected String nameListMittDest;
    protected String nameListMittDestAnnullati;
    protected int noAnnullamentoMittDest;
    protected int noMittDest;
    protected String nomeCartellaWF;
    protected String nomeFile;
    protected String nomeFileContenuto;
    protected String notaAnnullamento;
    protected String note;
    protected int numeroAllegati;
    protected String numeroProtEsterno;
    protected String nuovoContenuto;
    protected String nuovoContenutoOriginale;
    protected String oggetto;
    protected String originalDocId;
    protected String pathDocWorkFlow;
    protected boolean percorsoGuidato;
    protected boolean permessoModifica;
    protected boolean permessoRiservato;
    protected boolean presenzaCecPac;
    protected boolean presenzaDatiSensibili;
    protected boolean presenzaMase;
    protected boolean presoInCaricoDaMase;
    protected String priorita;
    protected String protocollatore;
    protected String provvedimentoAnnullamento;
    protected String pubblicato;
    protected Long registroId;
    protected String registroName;
    protected String riferimentoAreaDiScambio;
    protected boolean riservato;
    protected String searchCmd;
    protected String secureId;
    protected String secureIdDocumentoDaPratica;
    protected String secureIdFolder;
    protected String secureIdPratica;
    protected String secureRegistroId;
    protected Long sequenzaProtocollo;
    protected String smistamento;
    @XmlElement(nillable = true)
    protected List<Object> soggInteressati;
    protected Integer stato;
    protected Integer statoArchiviazione;
    protected Integer statoContenuto;
    protected String strCaselle;
    protected String tipoOriginale;
    protected String tipoRicerca;
    protected String tipoRubrica;
    protected SpeedTipologieDocumento tipologia;
    protected String tmpAnnullato;
    protected String tmpCaselle;
    protected boolean tmpCompleto;
    protected String tmpFlagConoscenza;
    protected String tmpMezzoSpedizioni;
    @XmlElement(nillable = true)
    protected List<Object> tmpMittDest;
    protected String tornaAllaMailCmd;
    protected String typeDocumentWF;
    protected String typeEstensioneDocumento;
    protected String uffAssegnatarioWF;
    protected String ufficioProtocollatore;
    protected String ultimoProgressivo;
    protected String urlImgI;
    protected String urlImgU;
    protected boolean uscita;
    protected boolean useMascheraCumulativa;
    protected String userIdProtocollatore;
    @XmlElement(required = true)
    protected SpeedProtocolloBean.ValuesAttrExt valuesAttrExt;
    @XmlElement(required = true)
    protected SpeedProtocolloBean.ValuesAttrInvioCDC valuesAttrInvioCDC;
    @XmlElement(required = true)
    protected SpeedProtocolloBean.ValuesAttrRegAux valuesAttrRegAux;

    /**
     * Gets the value of the allegati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allegati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllegati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getAllegati() {
        if (allegati == null) {
            allegati = new ArrayList<Object>();
        }
        return this.allegati;
    }

    /**
     * Gets the value of the annoDocCollegato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnoDocCollegato() {
        return annoDocCollegato;
    }

    /**
     * Sets the value of the annoDocCollegato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnoDocCollegato(String value) {
        this.annoDocCollegato = value;
    }

    /**
     * Gets the value of the annoRiferimentoRegistro property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAnnoRiferimentoRegistro() {
        return annoRiferimentoRegistro;
    }

    /**
     * Sets the value of the annoRiferimentoRegistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAnnoRiferimentoRegistro(Integer value) {
        this.annoRiferimentoRegistro = value;
    }

    /**
     * Gets the value of the annullato property.
     * 
     */
    public boolean isAnnullato() {
        return annullato;
    }

    /**
     * Sets the value of the annullato property.
     * 
     */
    public void setAnnullato(boolean value) {
        this.annullato = value;
    }

    /**
     * Gets the value of the archiviazioneSostitutiva property.
     * 
     */
    public boolean isArchiviazioneSostitutiva() {
        return archiviazioneSostitutiva;
    }

    /**
     * Sets the value of the archiviazioneSostitutiva property.
     * 
     */
    public void setArchiviazioneSostitutiva(boolean value) {
        this.archiviazioneSostitutiva = value;
    }

    /**
     * Gets the value of the assegnatarioWF property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssegnatarioWF() {
        return assegnatarioWF;
    }

    /**
     * Sets the value of the assegnatarioWF property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssegnatarioWF(String value) {
        this.assegnatarioWF = value;
    }

    /**
     * Gets the value of the attributi property.
     * 
     */
    public boolean isAttributi() {
        return attributi;
    }

    /**
     * Sets the value of the attributi property.
     * 
     */
    public void setAttributi(boolean value) {
        this.attributi = value;
    }

    /**
     * Gets the value of the checkInComment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckInComment() {
        return checkInComment;
    }

    /**
     * Sets the value of the checkInComment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckInComment(String value) {
        this.checkInComment = value;
    }

    /**
     * Gets the value of the checkedAllegati property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckedAllegati() {
        return checkedAllegati;
    }

    /**
     * Sets the value of the checkedAllegati property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckedAllegati(String value) {
        this.checkedAllegati = value;
    }

    /**
     * Gets the value of the chiuso property.
     * 
     */
    public boolean isChiuso() {
        return chiuso;
    }

    /**
     * Sets the value of the chiuso property.
     * 
     */
    public void setChiuso(boolean value) {
        this.chiuso = value;
    }

    /**
     * Gets the value of the civilia property.
     * 
     */
    public boolean isCivilia() {
        return civilia;
    }

    /**
     * Sets the value of the civilia property.
     * 
     */
    public void setCivilia(boolean value) {
        this.civilia = value;
    }

    /**
     * Gets the value of the classificaElements property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the classificaElements property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClassificaElements().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getClassificaElements() {
        if (classificaElements == null) {
            classificaElements = new ArrayList<Object>();
        }
        return this.classificaElements;
    }

    /**
     * Gets the value of the classificazioneSegnatura property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassificazioneSegnatura() {
        return classificazioneSegnatura;
    }

    /**
     * Sets the value of the classificazioneSegnatura property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassificazioneSegnatura(String value) {
        this.classificazioneSegnatura = value;
    }

    /**
     * Gets the value of the classifiche property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the classifiche property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getClassifiche().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getClassifiche() {
        if (classifiche == null) {
            classifiche = new ArrayList<String>();
        }
        return this.classifiche;
    }

    /**
     * Gets the value of the cleanErrorMsg property.
     * 
     */
    public boolean isCleanErrorMsg() {
        return cleanErrorMsg;
    }

    /**
     * Sets the value of the cleanErrorMsg property.
     * 
     */
    public void setCleanErrorMsg(boolean value) {
        this.cleanErrorMsg = value;
    }

    /**
     * Gets the value of the codiceURP property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceURP() {
        return codiceURP;
    }

    /**
     * Sets the value of the codiceURP property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceURP(String value) {
        this.codiceURP = value;
    }

    /**
     * Gets the value of the codiceUfficio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceUfficio() {
        return codiceUfficio;
    }

    /**
     * Sets the value of the codiceUfficio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceUfficio(String value) {
        this.codiceUfficio = value;
    }

    /**
     * Gets the value of the collegatiElements property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the collegatiElements property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCollegatiElements().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getCollegatiElements() {
        if (collegatiElements == null) {
            collegatiElements = new ArrayList<Object>();
        }
        return this.collegatiElements;
    }

    /**
     * Gets the value of the collegatiStoriciElements property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the collegatiStoriciElements property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCollegatiStoriciElements().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getCollegatiStoriciElements() {
        if (collegatiStoriciElements == null) {
            collegatiStoriciElements = new ArrayList<Object>();
        }
        return this.collegatiStoriciElements;
    }

    /**
     * Gets the value of the collocazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCollocazione() {
        return collocazione;
    }

    /**
     * Sets the value of the collocazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollocazione(String value) {
        this.collocazione = value;
    }

    /**
     * Gets the value of the completo property.
     * 
     */
    public boolean isCompleto() {
        return completo;
    }

    /**
     * Sets the value of the completo property.
     * 
     */
    public void setCompleto(boolean value) {
        this.completo = value;
    }

    /**
     * Gets the value of the conferma property.
     * 
     */
    public boolean isConferma() {
        return conferma;
    }

    /**
     * Sets the value of the conferma property.
     * 
     */
    public void setConferma(boolean value) {
        this.conferma = value;
    }

    /**
     * Gets the value of the conservaDati property.
     * 
     */
    public boolean isConservaDati() {
        return conservaDati;
    }

    /**
     * Sets the value of the conservaDati property.
     * 
     */
    public void setConservaDati(boolean value) {
        this.conservaDati = value;
    }

    /**
     * Gets the value of the contentDocWorkFlow property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContentDocWorkFlow() {
        return contentDocWorkFlow;
    }

    /**
     * Sets the value of the contentDocWorkFlow property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContentDocWorkFlow(String value) {
        this.contentDocWorkFlow = value;
    }

    /**
     * Gets the value of the dataProtEsterno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataProtEsterno() {
        return dataProtEsterno;
    }

    /**
     * Sets the value of the dataProtEsterno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataProtEsterno(String value) {
        this.dataProtEsterno = value;
    }

    /**
     * Gets the value of the dataProtocollo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataProtocollo() {
        return dataProtocollo;
    }

    /**
     * Sets the value of the dataProtocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataProtocollo(String value) {
        this.dataProtocollo = value;
    }

    /**
     * Gets the value of the dataRegistrazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataRegistrazione() {
        return dataRegistrazione;
    }

    /**
     * Sets the value of the dataRegistrazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataRegistrazione(String value) {
        this.dataRegistrazione = value;
    }

    /**
     * Gets the value of the dataRicezioneSpedizione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataRicezioneSpedizione() {
        return dataRicezioneSpedizione;
    }

    /**
     * Sets the value of the dataRicezioneSpedizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataRicezioneSpedizione(String value) {
        this.dataRicezioneSpedizione = value;
    }

    /**
     * Gets the value of the dataScarico property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataScarico() {
        return dataScarico;
    }

    /**
     * Sets the value of the dataScarico property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataScarico(String value) {
        this.dataScarico = value;
    }

    /**
     * Gets the value of the descGruppoIndirizzo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescGruppoIndirizzo() {
        return descGruppoIndirizzo;
    }

    /**
     * Sets the value of the descGruppoIndirizzo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescGruppoIndirizzo(String value) {
        this.descGruppoIndirizzo = value;
    }

    /**
     * Gets the value of the divVisibility property.
     * 
     */
    public boolean isDivVisibility() {
        return divVisibility;
    }

    /**
     * Sets the value of the divVisibility property.
     * 
     */
    public void setDivVisibility(boolean value) {
        this.divVisibility = value;
    }

    /**
     * Gets the value of the docCheCollegano property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the docCheCollegano property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocCheCollegano().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getDocCheCollegano() {
        if (docCheCollegano == null) {
            docCheCollegano = new ArrayList<Object>();
        }
        return this.docCheCollegano;
    }

    /**
     * Gets the value of the docCheColleganoStorico property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the docCheColleganoStorico property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocCheColleganoStorico().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getDocCheColleganoStorico() {
        if (docCheColleganoStorico == null) {
            docCheColleganoStorico = new ArrayList<Object>();
        }
        return this.docCheColleganoStorico;
    }

    /**
     * Gets the value of the docIncompleto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocIncompleto() {
        return docIncompleto;
    }

    /**
     * Sets the value of the docIncompleto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocIncompleto(String value) {
        this.docIncompleto = value;
    }

    /**
     * Gets the value of the docProtId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDocProtId() {
        return docProtId;
    }

    /**
     * Sets the value of the docProtId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDocProtId(Long value) {
        this.docProtId = value;
    }

    /**
     * Gets the value of the docProtIdValore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocProtIdValore() {
        return docProtIdValore;
    }

    /**
     * Sets the value of the docProtIdValore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocProtIdValore(String value) {
        this.docProtIdValore = value;
    }

    /**
     * Gets the value of the docSize property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getDocSize() {
        return docSize;
    }

    /**
     * Sets the value of the docSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setDocSize(Long value) {
        this.docSize = value;
    }

    /**
     * Gets the value of the docSupporto property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the docSupporto property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocSupporto().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getDocSupporto() {
        if (docSupporto == null) {
            docSupporto = new ArrayList<String>();
        }
        return this.docSupporto;
    }

    /**
     * Gets the value of the documentoUnico property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDocumentoUnico() {
        return documentoUnico;
    }

    /**
     * Sets the value of the documentoUnico property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDocumentoUnico(Integer value) {
        this.documentoUnico = value;
    }

    /**
     * Gets the value of the dummyDocService property.
     * 
     */
    public boolean isDummyDocService() {
        return dummyDocService;
    }

    /**
     * Sets the value of the dummyDocService property.
     * 
     */
    public void setDummyDocService(boolean value) {
        this.dummyDocService = value;
    }

    /**
     * Gets the value of the emergenza property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmergenza() {
        return emergenza;
    }

    /**
     * Sets the value of the emergenza property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmergenza(String value) {
        this.emergenza = value;
    }

    /**
     * Gets the value of the erroreAttributi property.
     * 
     */
    public boolean isErroreAttributi() {
        return erroreAttributi;
    }

    /**
     * Sets the value of the erroreAttributi property.
     * 
     */
    public void setErroreAttributi(boolean value) {
        this.erroreAttributi = value;
    }

    /**
     * Gets the value of the extension property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExtension() {
        return extension;
    }

    /**
     * Sets the value of the extension property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExtension(String value) {
        this.extension = value;
    }

    /**
     * Gets the value of the firmatario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirmatario() {
        return firmatario;
    }

    /**
     * Sets the value of the firmatario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirmatario(String value) {
        this.firmatario = value;
    }

    /**
     * Gets the value of the fkStatoPubblicazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFkStatoPubblicazione() {
        return fkStatoPubblicazione;
    }

    /**
     * Sets the value of the fkStatoPubblicazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFkStatoPubblicazione(String value) {
        this.fkStatoPubblicazione = value;
    }

    /**
     * Gets the value of the flagAllegatiChecked property.
     * 
     */
    public boolean isFlagAllegatiChecked() {
        return flagAllegatiChecked;
    }

    /**
     * Sets the value of the flagAllegatiChecked property.
     * 
     */
    public void setFlagAllegatiChecked(boolean value) {
        this.flagAllegatiChecked = value;
    }

    /**
     * Gets the value of the flagConoscenza property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagConoscenza() {
        return flagConoscenza;
    }

    /**
     * Sets the value of the flagConoscenza property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagConoscenza(String value) {
        this.flagConoscenza = value;
    }

    /**
     * Gets the value of the flagInPratica property.
     * 
     */
    public long getFlagInPratica() {
        return flagInPratica;
    }

    /**
     * Sets the value of the flagInPratica property.
     * 
     */
    public void setFlagInPratica(long value) {
        this.flagInPratica = value;
    }

    /**
     * Gets the value of the flagWorkFlow property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagWorkFlow() {
        return flagWorkFlow;
    }

    /**
     * Sets the value of the flagWorkFlow property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagWorkFlow(String value) {
        this.flagWorkFlow = value;
    }

    /**
     * Gets the value of the folderAllegati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the folderAllegati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFolderAllegati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getFolderAllegati() {
        if (folderAllegati == null) {
            folderAllegati = new ArrayList<String>();
        }
        return this.folderAllegati;
    }

    /**
     * Gets the value of the forceTipoGenerico property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForceTipoGenerico() {
        return forceTipoGenerico;
    }

    /**
     * Sets the value of the forceTipoGenerico property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForceTipoGenerico(String value) {
        this.forceTipoGenerico = value;
    }

    /**
     * Gets the value of the iconaFileOriginale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIconaFileOriginale() {
        return iconaFileOriginale;
    }

    /**
     * Sets the value of the iconaFileOriginale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIconaFileOriginale(String value) {
        this.iconaFileOriginale = value;
    }

    /**
     * Gets the value of the idAllegati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the idAllegati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdAllegati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getIdAllegati() {
        if (idAllegati == null) {
            idAllegati = new ArrayList<String>();
        }
        return this.idAllegati;
    }

    /**
     * Gets the value of the idAllegatiWorkFlow property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdAllegatiWorkFlow() {
        return idAllegatiWorkFlow;
    }

    /**
     * Sets the value of the idAllegatiWorkFlow property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdAllegatiWorkFlow(String value) {
        this.idAllegatiWorkFlow = value;
    }

    /**
     * Gets the value of the idAoo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdAoo() {
        return idAoo;
    }

    /**
     * Sets the value of the idAoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdAoo(String value) {
        this.idAoo = value;
    }

    /**
     * Gets the value of the idAssegnazione property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdAssegnazione() {
        return idAssegnazione;
    }

    /**
     * Sets the value of the idAssegnazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdAssegnazione(Long value) {
        this.idAssegnazione = value;
    }

    /**
     * Gets the value of the idCasellaEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdCasellaEmail() {
        return idCasellaEmail;
    }

    /**
     * Sets the value of the idCasellaEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdCasellaEmail(String value) {
        this.idCasellaEmail = value;
    }

    /**
     * Gets the value of the idDocCollegati property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocCollegati() {
        return idDocCollegati;
    }

    /**
     * Sets the value of the idDocCollegati property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocCollegati(String value) {
        this.idDocCollegati = value;
    }

    /**
     * Gets the value of the idDocumentale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentale() {
        return idDocumentale;
    }

    /**
     * Sets the value of the idDocumentale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentale(String value) {
        this.idDocumentale = value;
    }

    /**
     * Gets the value of the idDocumentoDaPratica property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoDaPratica() {
        return idDocumentoDaPratica;
    }

    /**
     * Sets the value of the idDocumentoDaPratica property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoDaPratica(String value) {
        this.idDocumentoDaPratica = value;
    }

    /**
     * Gets the value of the idDocumentoOriginale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoOriginale() {
        return idDocumentoOriginale;
    }

    /**
     * Sets the value of the idDocumentoOriginale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoOriginale(String value) {
        this.idDocumentoOriginale = value;
    }

    /**
     * Gets the value of the idEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdEmail() {
        return idEmail;
    }

    /**
     * Sets the value of the idEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdEmail(String value) {
        this.idEmail = value;
    }

    /**
     * Gets the value of the idFolderPratica property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFolderPratica() {
        return idFolderPratica;
    }

    /**
     * Sets the value of the idFolderPratica property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFolderPratica(String value) {
        this.idFolderPratica = value;
    }

    /**
     * Gets the value of the idLivelloRiservatezza property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdLivelloRiservatezza() {
        return idLivelloRiservatezza;
    }

    /**
     * Sets the value of the idLivelloRiservatezza property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdLivelloRiservatezza(Long value) {
        this.idLivelloRiservatezza = value;
    }

    /**
     * Gets the value of the idPratica property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdPratica() {
        return idPratica;
    }

    /**
     * Sets the value of the idPratica property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdPratica(Long value) {
        this.idPratica = value;
    }

    /**
     * Gets the value of the idR property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdR() {
        return idR;
    }

    /**
     * Sets the value of the idR property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdR(String value) {
        this.idR = value;
    }

    /**
     * Gets the value of the idTaskWorkFlow property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdTaskWorkFlow() {
        return idTaskWorkFlow;
    }

    /**
     * Sets the value of the idTaskWorkFlow property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdTaskWorkFlow(String value) {
        this.idTaskWorkFlow = value;
    }

    /**
     * Gets the value of the idTipologiaDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIdTipologiaDocumento() {
        return idTipologiaDocumento;
    }

    /**
     * Sets the value of the idTipologiaDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIdTipologiaDocumento(Long value) {
        this.idTipologiaDocumento = value;
    }

    /**
     * Gets the value of the idUfficio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdUfficio() {
        return idUfficio;
    }

    /**
     * Sets the value of the idUfficio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdUfficio(String value) {
        this.idUfficio = value;
    }

    /**
     * Gets the value of the indirizzo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndirizzo() {
        return indirizzo;
    }

    /**
     * Sets the value of the indirizzo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndirizzo(String value) {
        this.indirizzo = value;
    }

    /**
     * Gets the value of the ingresso property.
     * 
     */
    public boolean isIngresso() {
        return ingresso;
    }

    /**
     * Sets the value of the ingresso property.
     * 
     */
    public void setIngresso(boolean value) {
        this.ingresso = value;
    }

    /**
     * Gets the value of the iniCaselle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIniCaselle() {
        return iniCaselle;
    }

    /**
     * Sets the value of the iniCaselle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIniCaselle(String value) {
        this.iniCaselle = value;
    }

    /**
     * Gets the value of the iniFlagConoscenza property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIniFlagConoscenza() {
        return iniFlagConoscenza;
    }

    /**
     * Sets the value of the iniFlagConoscenza property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIniFlagConoscenza(String value) {
        this.iniFlagConoscenza = value;
    }

    /**
     * Gets the value of the iniMezzoSpedizioni property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIniMezzoSpedizioni() {
        return iniMezzoSpedizioni;
    }

    /**
     * Sets the value of the iniMezzoSpedizioni property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIniMezzoSpedizioni(String value) {
        this.iniMezzoSpedizioni = value;
    }

    /**
     * Gets the value of the iniTmpCaselle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIniTmpCaselle() {
        return iniTmpCaselle;
    }

    /**
     * Sets the value of the iniTmpCaselle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIniTmpCaselle(String value) {
        this.iniTmpCaselle = value;
    }

    /**
     * Gets the value of the iniTmpFlagConoscenza property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIniTmpFlagConoscenza() {
        return iniTmpFlagConoscenza;
    }

    /**
     * Sets the value of the iniTmpFlagConoscenza property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIniTmpFlagConoscenza(String value) {
        this.iniTmpFlagConoscenza = value;
    }

    /**
     * Gets the value of the iniTmpMezzoSpedizioni property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIniTmpMezzoSpedizioni() {
        return iniTmpMezzoSpedizioni;
    }

    /**
     * Sets the value of the iniTmpMezzoSpedizioni property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIniTmpMezzoSpedizioni(String value) {
        this.iniTmpMezzoSpedizioni = value;
    }

    /**
     * Gets the value of the interesseMase property.
     * 
     */
    public boolean isInteresseMase() {
        return interesseMase;
    }

    /**
     * Sets the value of the interesseMase property.
     * 
     */
    public void setInteresseMase(boolean value) {
        this.interesseMase = value;
    }

    /**
     * Gets the value of the is3BisEnabled property.
     * 
     */
    public boolean isIs3BisEnabled() {
        return is3BisEnabled;
    }

    /**
     * Sets the value of the is3BisEnabled property.
     * 
     */
    public void setIs3BisEnabled(boolean value) {
        this.is3BisEnabled = value;
    }

    /**
     * Gets the value of the isTrasmissione property.
     * 
     */
    public boolean isIsTrasmissione() {
        return isTrasmissione;
    }

    /**
     * Sets the value of the isTrasmissione property.
     * 
     */
    public void setIsTrasmissione(boolean value) {
        this.isTrasmissione = value;
    }

    /**
     * Gets the value of the isTrasmissioneCDC property.
     * 
     */
    public boolean isIsTrasmissioneCDC() {
        return isTrasmissioneCDC;
    }

    /**
     * Sets the value of the isTrasmissioneCDC property.
     * 
     */
    public void setIsTrasmissioneCDC(boolean value) {
        this.isTrasmissioneCDC = value;
    }

    /**
     * Gets the value of the listaRicevute property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaRicevute property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaRicevute().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getListaRicevute() {
        if (listaRicevute == null) {
            listaRicevute = new ArrayList<Object>();
        }
        return this.listaRicevute;
    }

    /**
     * Gets the value of the messageManager property.
     * 
     * @return
     *     possible object is
     *     {@link SpeedProtocolloMessageMananger }
     *     
     */
    public SpeedProtocolloMessageMananger getMessageManager() {
        return messageManager;
    }

    /**
     * Sets the value of the messageManager property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpeedProtocolloMessageMananger }
     *     
     */
    public void setMessageManager(SpeedProtocolloMessageMananger value) {
        this.messageManager = value;
    }

    /**
     * Gets the value of the mezzoSpedizione property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getMezzoSpedizione() {
        return mezzoSpedizione;
    }

    /**
     * Sets the value of the mezzoSpedizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setMezzoSpedizione(Long value) {
        this.mezzoSpedizione = value;
    }

    /**
     * Gets the value of the mezzoSpedizioni property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMezzoSpedizioni() {
        return mezzoSpedizioni;
    }

    /**
     * Sets the value of the mezzoSpedizioni property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMezzoSpedizioni(String value) {
        this.mezzoSpedizioni = value;
    }

    /**
     * Gets the value of the mittDest property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mittDest property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMittDest().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getMittDest() {
        if (mittDest == null) {
            mittDest = new ArrayList<Object>();
        }
        return this.mittDest;
    }

    /**
     * Gets the value of the mittdestPG property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mittdestPG property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMittdestPG().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getMittdestPG() {
        if (mittdestPG == null) {
            mittdestPG = new ArrayList<Object>();
        }
        return this.mittdestPG;
    }

    /**
     * Gets the value of the modalita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModalita() {
        return modalita;
    }

    /**
     * Sets the value of the modalita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModalita(String value) {
        this.modalita = value;
    }

    /**
     * Gets the value of the nameListMittDest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameListMittDest() {
        return nameListMittDest;
    }

    /**
     * Sets the value of the nameListMittDest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameListMittDest(String value) {
        this.nameListMittDest = value;
    }

    /**
     * Gets the value of the nameListMittDestAnnullati property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNameListMittDestAnnullati() {
        return nameListMittDestAnnullati;
    }

    /**
     * Sets the value of the nameListMittDestAnnullati property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNameListMittDestAnnullati(String value) {
        this.nameListMittDestAnnullati = value;
    }

    /**
     * Gets the value of the noAnnullamentoMittDest property.
     * 
     */
    public int getNoAnnullamentoMittDest() {
        return noAnnullamentoMittDest;
    }

    /**
     * Sets the value of the noAnnullamentoMittDest property.
     * 
     */
    public void setNoAnnullamentoMittDest(int value) {
        this.noAnnullamentoMittDest = value;
    }

    /**
     * Gets the value of the noMittDest property.
     * 
     */
    public int getNoMittDest() {
        return noMittDest;
    }

    /**
     * Sets the value of the noMittDest property.
     * 
     */
    public void setNoMittDest(int value) {
        this.noMittDest = value;
    }

    /**
     * Gets the value of the nomeCartellaWF property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeCartellaWF() {
        return nomeCartellaWF;
    }

    /**
     * Sets the value of the nomeCartellaWF property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeCartellaWF(String value) {
        this.nomeCartellaWF = value;
    }

    /**
     * Gets the value of the nomeFile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeFile() {
        return nomeFile;
    }

    /**
     * Sets the value of the nomeFile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeFile(String value) {
        this.nomeFile = value;
    }

    /**
     * Gets the value of the nomeFileContenuto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeFileContenuto() {
        return nomeFileContenuto;
    }

    /**
     * Sets the value of the nomeFileContenuto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeFileContenuto(String value) {
        this.nomeFileContenuto = value;
    }

    /**
     * Gets the value of the notaAnnullamento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNotaAnnullamento() {
        return notaAnnullamento;
    }

    /**
     * Sets the value of the notaAnnullamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNotaAnnullamento(String value) {
        this.notaAnnullamento = value;
    }

    /**
     * Gets the value of the note property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNote() {
        return note;
    }

    /**
     * Sets the value of the note property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNote(String value) {
        this.note = value;
    }

    /**
     * Gets the value of the numeroAllegati property.
     * 
     */
    public int getNumeroAllegati() {
        return numeroAllegati;
    }

    /**
     * Sets the value of the numeroAllegati property.
     * 
     */
    public void setNumeroAllegati(int value) {
        this.numeroAllegati = value;
    }

    /**
     * Gets the value of the numeroProtEsterno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroProtEsterno() {
        return numeroProtEsterno;
    }

    /**
     * Sets the value of the numeroProtEsterno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroProtEsterno(String value) {
        this.numeroProtEsterno = value;
    }

    /**
     * Gets the value of the nuovoContenuto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNuovoContenuto() {
        return nuovoContenuto;
    }

    /**
     * Sets the value of the nuovoContenuto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNuovoContenuto(String value) {
        this.nuovoContenuto = value;
    }

    /**
     * Gets the value of the nuovoContenutoOriginale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNuovoContenutoOriginale() {
        return nuovoContenutoOriginale;
    }

    /**
     * Sets the value of the nuovoContenutoOriginale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNuovoContenutoOriginale(String value) {
        this.nuovoContenutoOriginale = value;
    }

    /**
     * Gets the value of the oggetto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggetto() {
        return oggetto;
    }

    /**
     * Sets the value of the oggetto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggetto(String value) {
        this.oggetto = value;
    }

    /**
     * Gets the value of the originalDocId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginalDocId() {
        return originalDocId;
    }

    /**
     * Sets the value of the originalDocId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginalDocId(String value) {
        this.originalDocId = value;
    }

    /**
     * Gets the value of the pathDocWorkFlow property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPathDocWorkFlow() {
        return pathDocWorkFlow;
    }

    /**
     * Sets the value of the pathDocWorkFlow property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPathDocWorkFlow(String value) {
        this.pathDocWorkFlow = value;
    }

    /**
     * Gets the value of the percorsoGuidato property.
     * 
     */
    public boolean isPercorsoGuidato() {
        return percorsoGuidato;
    }

    /**
     * Sets the value of the percorsoGuidato property.
     * 
     */
    public void setPercorsoGuidato(boolean value) {
        this.percorsoGuidato = value;
    }

    /**
     * Gets the value of the permessoModifica property.
     * 
     */
    public boolean isPermessoModifica() {
        return permessoModifica;
    }

    /**
     * Sets the value of the permessoModifica property.
     * 
     */
    public void setPermessoModifica(boolean value) {
        this.permessoModifica = value;
    }

    /**
     * Gets the value of the permessoRiservato property.
     * 
     */
    public boolean isPermessoRiservato() {
        return permessoRiservato;
    }

    /**
     * Sets the value of the permessoRiservato property.
     * 
     */
    public void setPermessoRiservato(boolean value) {
        this.permessoRiservato = value;
    }

    /**
     * Gets the value of the presenzaCecPac property.
     * 
     */
    public boolean isPresenzaCecPac() {
        return presenzaCecPac;
    }

    /**
     * Sets the value of the presenzaCecPac property.
     * 
     */
    public void setPresenzaCecPac(boolean value) {
        this.presenzaCecPac = value;
    }

    /**
     * Gets the value of the presenzaDatiSensibili property.
     * 
     */
    public boolean isPresenzaDatiSensibili() {
        return presenzaDatiSensibili;
    }

    /**
     * Sets the value of the presenzaDatiSensibili property.
     * 
     */
    public void setPresenzaDatiSensibili(boolean value) {
        this.presenzaDatiSensibili = value;
    }

    /**
     * Gets the value of the presenzaMase property.
     * 
     */
    public boolean isPresenzaMase() {
        return presenzaMase;
    }

    /**
     * Sets the value of the presenzaMase property.
     * 
     */
    public void setPresenzaMase(boolean value) {
        this.presenzaMase = value;
    }

    /**
     * Gets the value of the presoInCaricoDaMase property.
     * 
     */
    public boolean isPresoInCaricoDaMase() {
        return presoInCaricoDaMase;
    }

    /**
     * Sets the value of the presoInCaricoDaMase property.
     * 
     */
    public void setPresoInCaricoDaMase(boolean value) {
        this.presoInCaricoDaMase = value;
    }

    /**
     * Gets the value of the priorita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriorita() {
        return priorita;
    }

    /**
     * Sets the value of the priorita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriorita(String value) {
        this.priorita = value;
    }

    /**
     * Gets the value of the protocollatore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProtocollatore() {
        return protocollatore;
    }

    /**
     * Sets the value of the protocollatore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProtocollatore(String value) {
        this.protocollatore = value;
    }

    /**
     * Gets the value of the provvedimentoAnnullamento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvvedimentoAnnullamento() {
        return provvedimentoAnnullamento;
    }

    /**
     * Sets the value of the provvedimentoAnnullamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvvedimentoAnnullamento(String value) {
        this.provvedimentoAnnullamento = value;
    }

    /**
     * Gets the value of the pubblicato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPubblicato() {
        return pubblicato;
    }

    /**
     * Sets the value of the pubblicato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPubblicato(String value) {
        this.pubblicato = value;
    }

    /**
     * Gets the value of the registroId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getRegistroId() {
        return registroId;
    }

    /**
     * Sets the value of the registroId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setRegistroId(Long value) {
        this.registroId = value;
    }

    /**
     * Gets the value of the registroName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistroName() {
        return registroName;
    }

    /**
     * Sets the value of the registroName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistroName(String value) {
        this.registroName = value;
    }

    /**
     * Gets the value of the riferimentoAreaDiScambio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiferimentoAreaDiScambio() {
        return riferimentoAreaDiScambio;
    }

    /**
     * Sets the value of the riferimentoAreaDiScambio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiferimentoAreaDiScambio(String value) {
        this.riferimentoAreaDiScambio = value;
    }

    /**
     * Gets the value of the riservato property.
     * 
     */
    public boolean isRiservato() {
        return riservato;
    }

    /**
     * Sets the value of the riservato property.
     * 
     */
    public void setRiservato(boolean value) {
        this.riservato = value;
    }

    /**
     * Gets the value of the searchCmd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchCmd() {
        return searchCmd;
    }

    /**
     * Sets the value of the searchCmd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchCmd(String value) {
        this.searchCmd = value;
    }

    /**
     * Gets the value of the secureId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecureId() {
        return secureId;
    }

    /**
     * Sets the value of the secureId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecureId(String value) {
        this.secureId = value;
    }

    /**
     * Gets the value of the secureIdDocumentoDaPratica property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecureIdDocumentoDaPratica() {
        return secureIdDocumentoDaPratica;
    }

    /**
     * Sets the value of the secureIdDocumentoDaPratica property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecureIdDocumentoDaPratica(String value) {
        this.secureIdDocumentoDaPratica = value;
    }

    /**
     * Gets the value of the secureIdFolder property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecureIdFolder() {
        return secureIdFolder;
    }

    /**
     * Sets the value of the secureIdFolder property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecureIdFolder(String value) {
        this.secureIdFolder = value;
    }

    /**
     * Gets the value of the secureIdPratica property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecureIdPratica() {
        return secureIdPratica;
    }

    /**
     * Sets the value of the secureIdPratica property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecureIdPratica(String value) {
        this.secureIdPratica = value;
    }

    /**
     * Gets the value of the secureRegistroId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSecureRegistroId() {
        return secureRegistroId;
    }

    /**
     * Sets the value of the secureRegistroId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSecureRegistroId(String value) {
        this.secureRegistroId = value;
    }

    /**
     * Gets the value of the sequenzaProtocollo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getSequenzaProtocollo() {
        return sequenzaProtocollo;
    }

    /**
     * Sets the value of the sequenzaProtocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setSequenzaProtocollo(Long value) {
        this.sequenzaProtocollo = value;
    }

    /**
     * Gets the value of the smistamento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSmistamento() {
        return smistamento;
    }

    /**
     * Sets the value of the smistamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSmistamento(String value) {
        this.smistamento = value;
    }

    /**
     * Gets the value of the soggInteressati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the soggInteressati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSoggInteressati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getSoggInteressati() {
        if (soggInteressati == null) {
            soggInteressati = new ArrayList<Object>();
        }
        return this.soggInteressati;
    }

    /**
     * Gets the value of the stato property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getStato() {
        return stato;
    }

    /**
     * Sets the value of the stato property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setStato(Integer value) {
        this.stato = value;
    }

    /**
     * Gets the value of the statoArchiviazione property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getStatoArchiviazione() {
        return statoArchiviazione;
    }

    /**
     * Sets the value of the statoArchiviazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setStatoArchiviazione(Integer value) {
        this.statoArchiviazione = value;
    }

    /**
     * Gets the value of the statoContenuto property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getStatoContenuto() {
        return statoContenuto;
    }

    /**
     * Sets the value of the statoContenuto property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setStatoContenuto(Integer value) {
        this.statoContenuto = value;
    }

    /**
     * Gets the value of the strCaselle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrCaselle() {
        return strCaselle;
    }

    /**
     * Sets the value of the strCaselle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrCaselle(String value) {
        this.strCaselle = value;
    }

    /**
     * Gets the value of the tipoOriginale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoOriginale() {
        return tipoOriginale;
    }

    /**
     * Sets the value of the tipoOriginale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoOriginale(String value) {
        this.tipoOriginale = value;
    }

    /**
     * Gets the value of the tipoRicerca property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoRicerca() {
        return tipoRicerca;
    }

    /**
     * Sets the value of the tipoRicerca property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoRicerca(String value) {
        this.tipoRicerca = value;
    }

    /**
     * Gets the value of the tipoRubrica property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoRubrica() {
        return tipoRubrica;
    }

    /**
     * Sets the value of the tipoRubrica property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoRubrica(String value) {
        this.tipoRubrica = value;
    }

    /**
     * Gets the value of the tipologia property.
     * 
     * @return
     *     possible object is
     *     {@link SpeedTipologieDocumento }
     *     
     */
    public SpeedTipologieDocumento getTipologia() {
        return tipologia;
    }

    /**
     * Sets the value of the tipologia property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpeedTipologieDocumento }
     *     
     */
    public void setTipologia(SpeedTipologieDocumento value) {
        this.tipologia = value;
    }

    /**
     * Gets the value of the tmpAnnullato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTmpAnnullato() {
        return tmpAnnullato;
    }

    /**
     * Sets the value of the tmpAnnullato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTmpAnnullato(String value) {
        this.tmpAnnullato = value;
    }

    /**
     * Gets the value of the tmpCaselle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTmpCaselle() {
        return tmpCaselle;
    }

    /**
     * Sets the value of the tmpCaselle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTmpCaselle(String value) {
        this.tmpCaselle = value;
    }

    /**
     * Gets the value of the tmpCompleto property.
     * 
     */
    public boolean isTmpCompleto() {
        return tmpCompleto;
    }

    /**
     * Sets the value of the tmpCompleto property.
     * 
     */
    public void setTmpCompleto(boolean value) {
        this.tmpCompleto = value;
    }

    /**
     * Gets the value of the tmpFlagConoscenza property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTmpFlagConoscenza() {
        return tmpFlagConoscenza;
    }

    /**
     * Sets the value of the tmpFlagConoscenza property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTmpFlagConoscenza(String value) {
        this.tmpFlagConoscenza = value;
    }

    /**
     * Gets the value of the tmpMezzoSpedizioni property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTmpMezzoSpedizioni() {
        return tmpMezzoSpedizioni;
    }

    /**
     * Sets the value of the tmpMezzoSpedizioni property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTmpMezzoSpedizioni(String value) {
        this.tmpMezzoSpedizioni = value;
    }

    /**
     * Gets the value of the tmpMittDest property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tmpMittDest property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTmpMittDest().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Object }
     * 
     * 
     */
    public List<Object> getTmpMittDest() {
        if (tmpMittDest == null) {
            tmpMittDest = new ArrayList<Object>();
        }
        return this.tmpMittDest;
    }

    /**
     * Gets the value of the tornaAllaMailCmd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTornaAllaMailCmd() {
        return tornaAllaMailCmd;
    }

    /**
     * Sets the value of the tornaAllaMailCmd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTornaAllaMailCmd(String value) {
        this.tornaAllaMailCmd = value;
    }

    /**
     * Gets the value of the typeDocumentWF property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeDocumentWF() {
        return typeDocumentWF;
    }

    /**
     * Sets the value of the typeDocumentWF property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeDocumentWF(String value) {
        this.typeDocumentWF = value;
    }

    /**
     * Gets the value of the typeEstensioneDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeEstensioneDocumento() {
        return typeEstensioneDocumento;
    }

    /**
     * Sets the value of the typeEstensioneDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeEstensioneDocumento(String value) {
        this.typeEstensioneDocumento = value;
    }

    /**
     * Gets the value of the uffAssegnatarioWF property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUffAssegnatarioWF() {
        return uffAssegnatarioWF;
    }

    /**
     * Sets the value of the uffAssegnatarioWF property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUffAssegnatarioWF(String value) {
        this.uffAssegnatarioWF = value;
    }

    /**
     * Gets the value of the ufficioProtocollatore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUfficioProtocollatore() {
        return ufficioProtocollatore;
    }

    /**
     * Sets the value of the ufficioProtocollatore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUfficioProtocollatore(String value) {
        this.ufficioProtocollatore = value;
    }

    /**
     * Gets the value of the ultimoProgressivo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUltimoProgressivo() {
        return ultimoProgressivo;
    }

    /**
     * Sets the value of the ultimoProgressivo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUltimoProgressivo(String value) {
        this.ultimoProgressivo = value;
    }

    /**
     * Gets the value of the urlImgI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrlImgI() {
        return urlImgI;
    }

    /**
     * Sets the value of the urlImgI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrlImgI(String value) {
        this.urlImgI = value;
    }

    /**
     * Gets the value of the urlImgU property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrlImgU() {
        return urlImgU;
    }

    /**
     * Sets the value of the urlImgU property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrlImgU(String value) {
        this.urlImgU = value;
    }

    /**
     * Gets the value of the uscita property.
     * 
     */
    public boolean isUscita() {
        return uscita;
    }

    /**
     * Sets the value of the uscita property.
     * 
     */
    public void setUscita(boolean value) {
        this.uscita = value;
    }

    /**
     * Gets the value of the useMascheraCumulativa property.
     * 
     */
    public boolean isUseMascheraCumulativa() {
        return useMascheraCumulativa;
    }

    /**
     * Sets the value of the useMascheraCumulativa property.
     * 
     */
    public void setUseMascheraCumulativa(boolean value) {
        this.useMascheraCumulativa = value;
    }

    /**
     * Gets the value of the userIdProtocollatore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserIdProtocollatore() {
        return userIdProtocollatore;
    }

    /**
     * Sets the value of the userIdProtocollatore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserIdProtocollatore(String value) {
        this.userIdProtocollatore = value;
    }

    /**
     * Gets the value of the valuesAttrExt property.
     * 
     * @return
     *     possible object is
     *     {@link SpeedProtocolloBean.ValuesAttrExt }
     *     
     */
    public SpeedProtocolloBean.ValuesAttrExt getValuesAttrExt() {
        return valuesAttrExt;
    }

    /**
     * Sets the value of the valuesAttrExt property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpeedProtocolloBean.ValuesAttrExt }
     *     
     */
    public void setValuesAttrExt(SpeedProtocolloBean.ValuesAttrExt value) {
        this.valuesAttrExt = value;
    }

    /**
     * Gets the value of the valuesAttrInvioCDC property.
     * 
     * @return
     *     possible object is
     *     {@link SpeedProtocolloBean.ValuesAttrInvioCDC }
     *     
     */
    public SpeedProtocolloBean.ValuesAttrInvioCDC getValuesAttrInvioCDC() {
        return valuesAttrInvioCDC;
    }

    /**
     * Sets the value of the valuesAttrInvioCDC property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpeedProtocolloBean.ValuesAttrInvioCDC }
     *     
     */
    public void setValuesAttrInvioCDC(SpeedProtocolloBean.ValuesAttrInvioCDC value) {
        this.valuesAttrInvioCDC = value;
    }

    /**
     * Gets the value of the valuesAttrRegAux property.
     * 
     * @return
     *     possible object is
     *     {@link SpeedProtocolloBean.ValuesAttrRegAux }
     *     
     */
    public SpeedProtocolloBean.ValuesAttrRegAux getValuesAttrRegAux() {
        return valuesAttrRegAux;
    }

    /**
     * Sets the value of the valuesAttrRegAux property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpeedProtocolloBean.ValuesAttrRegAux }
     *     
     */
    public void setValuesAttrRegAux(SpeedProtocolloBean.ValuesAttrRegAux value) {
        this.valuesAttrRegAux = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="entry" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                   &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "entry"
    })
    public static class ValuesAttrExt {

        protected List<SpeedProtocolloBean.ValuesAttrExt.Entry> entry;

        /**
         * Gets the value of the entry property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the entry property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEntry().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SpeedProtocolloBean.ValuesAttrExt.Entry }
         * 
         * 
         */
        public List<SpeedProtocolloBean.ValuesAttrExt.Entry> getEntry() {
            if (entry == null) {
                entry = new ArrayList<SpeedProtocolloBean.ValuesAttrExt.Entry>();
            }
            return this.entry;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "key",
            "value"
        })
        public static class Entry {

            protected Object key;
            protected Object value;

            /**
             * Gets the value of the key property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getKey() {
                return key;
            }

            /**
             * Sets the value of the key property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setKey(Object value) {
                this.key = value;
            }

            /**
             * Gets the value of the value property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setValue(Object value) {
                this.value = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="entry" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                   &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "entry"
    })
    public static class ValuesAttrInvioCDC {

        protected List<SpeedProtocolloBean.ValuesAttrInvioCDC.Entry> entry;

        /**
         * Gets the value of the entry property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the entry property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEntry().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SpeedProtocolloBean.ValuesAttrInvioCDC.Entry }
         * 
         * 
         */
        public List<SpeedProtocolloBean.ValuesAttrInvioCDC.Entry> getEntry() {
            if (entry == null) {
                entry = new ArrayList<SpeedProtocolloBean.ValuesAttrInvioCDC.Entry>();
            }
            return this.entry;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "key",
            "value"
        })
        public static class Entry {

            protected Object key;
            protected Object value;

            /**
             * Gets the value of the key property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getKey() {
                return key;
            }

            /**
             * Sets the value of the key property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setKey(Object value) {
                this.key = value;
            }

            /**
             * Gets the value of the value property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setValue(Object value) {
                this.value = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="entry" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                   &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "entry"
    })
    public static class ValuesAttrRegAux {

        protected List<SpeedProtocolloBean.ValuesAttrRegAux.Entry> entry;

        /**
         * Gets the value of the entry property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the entry property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEntry().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link SpeedProtocolloBean.ValuesAttrRegAux.Entry }
         * 
         * 
         */
        public List<SpeedProtocolloBean.ValuesAttrRegAux.Entry> getEntry() {
            if (entry == null) {
                entry = new ArrayList<SpeedProtocolloBean.ValuesAttrRegAux.Entry>();
            }
            return this.entry;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "key",
            "value"
        })
        public static class Entry {

            protected Object key;
            protected Object value;

            /**
             * Gets the value of the key property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getKey() {
                return key;
            }

            /**
             * Sets the value of the key property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setKey(Object value) {
                this.key = value;
            }

            /**
             * Gets the value of the value property.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getValue() {
                return value;
            }

            /**
             * Sets the value of the value property.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setValue(Object value) {
                this.value = value;
            }

        }

    }

}
