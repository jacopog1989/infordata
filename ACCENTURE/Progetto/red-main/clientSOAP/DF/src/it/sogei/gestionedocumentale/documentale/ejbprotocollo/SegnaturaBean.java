
package it.sogei.gestionedocumentale.documentale.ejbprotocollo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for segnaturaBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="segnaturaBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="casella" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiceClassificazione" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="dataDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataProtocolloCollegato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dati_sensibili" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="descCodiceRegistro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="listaMittDest" type="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}speedMittenteDestinatario" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="mittentiDest" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="modalita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroProtocolloCollegato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="oggetto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="protocolloEsterno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="protocolloMittente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="registro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="registroProtocolloCollegato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="riservato" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="speedCasellaEmail" type="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}speedCasellaEmail" minOccurs="0"/>
 *         &lt;element name="speedMezzoSpedizione" type="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}speedMezzoSpedizione" minOccurs="0"/>
 *         &lt;element name="tipoDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipologia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "segnaturaBean", propOrder = {
    "casella",
    "codiceClassificazione",
    "dataDocumento",
    "dataProtocolloCollegato",
    "datiSensibili",
    "descCodiceRegistro",
    "listaMittDest",
    "mittentiDest",
    "modalita",
    "note",
    "numeroProtocolloCollegato",
    "oggetto",
    "protocolloEsterno",
    "protocolloMittente",
    "registro",
    "registroProtocolloCollegato",
    "riservato",
    "speedCasellaEmail",
    "speedMezzoSpedizione",
    "tipoDocumento",
    "tipologia"
})
public class SegnaturaBean {

    protected String casella;
    @XmlElement(nillable = true)
    protected List<String> codiceClassificazione;
    protected String dataDocumento;
    protected String dataProtocolloCollegato;
    @XmlElement(name = "dati_sensibili")
    protected boolean datiSensibili;
    protected String descCodiceRegistro;
    @XmlElement(nillable = true)
    protected List<SpeedMittenteDestinatario> listaMittDest;
    @XmlElement(nillable = true)
    protected List<String> mittentiDest;
    protected String modalita;
    protected String note;
    protected String numeroProtocolloCollegato;
    protected String oggetto;
    protected String protocolloEsterno;
    protected String protocolloMittente;
    protected String registro;
    protected String registroProtocolloCollegato;
    protected boolean riservato;
    protected SpeedCasellaEmail speedCasellaEmail;
    protected SpeedMezzoSpedizione speedMezzoSpedizione;
    protected String tipoDocumento;
    protected String tipologia;

    /**
     * Gets the value of the casella property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCasella() {
        return casella;
    }

    /**
     * Sets the value of the casella property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCasella(String value) {
        this.casella = value;
    }

    /**
     * Gets the value of the codiceClassificazione property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the codiceClassificazione property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCodiceClassificazione().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getCodiceClassificazione() {
        if (codiceClassificazione == null) {
            codiceClassificazione = new ArrayList<String>();
        }
        return this.codiceClassificazione;
    }

    /**
     * Gets the value of the dataDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataDocumento() {
        return dataDocumento;
    }

    /**
     * Sets the value of the dataDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataDocumento(String value) {
        this.dataDocumento = value;
    }

    /**
     * Gets the value of the dataProtocolloCollegato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataProtocolloCollegato() {
        return dataProtocolloCollegato;
    }

    /**
     * Sets the value of the dataProtocolloCollegato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataProtocolloCollegato(String value) {
        this.dataProtocolloCollegato = value;
    }

    /**
     * Gets the value of the datiSensibili property.
     * 
     */
    public boolean isDatiSensibili() {
        return datiSensibili;
    }

    /**
     * Sets the value of the datiSensibili property.
     * 
     */
    public void setDatiSensibili(boolean value) {
        this.datiSensibili = value;
    }

    /**
     * Gets the value of the descCodiceRegistro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescCodiceRegistro() {
        return descCodiceRegistro;
    }

    /**
     * Sets the value of the descCodiceRegistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescCodiceRegistro(String value) {
        this.descCodiceRegistro = value;
    }

    /**
     * Gets the value of the listaMittDest property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaMittDest property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaMittDest().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SpeedMittenteDestinatario }
     * 
     * 
     */
    public List<SpeedMittenteDestinatario> getListaMittDest() {
        if (listaMittDest == null) {
            listaMittDest = new ArrayList<SpeedMittenteDestinatario>();
        }
        return this.listaMittDest;
    }

    /**
     * Gets the value of the mittentiDest property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mittentiDest property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMittentiDest().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getMittentiDest() {
        if (mittentiDest == null) {
            mittentiDest = new ArrayList<String>();
        }
        return this.mittentiDest;
    }

    /**
     * Gets the value of the modalita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModalita() {
        return modalita;
    }

    /**
     * Sets the value of the modalita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModalita(String value) {
        this.modalita = value;
    }

    /**
     * Gets the value of the note property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNote() {
        return note;
    }

    /**
     * Sets the value of the note property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNote(String value) {
        this.note = value;
    }

    /**
     * Gets the value of the numeroProtocolloCollegato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroProtocolloCollegato() {
        return numeroProtocolloCollegato;
    }

    /**
     * Sets the value of the numeroProtocolloCollegato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroProtocolloCollegato(String value) {
        this.numeroProtocolloCollegato = value;
    }

    /**
     * Gets the value of the oggetto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggetto() {
        return oggetto;
    }

    /**
     * Sets the value of the oggetto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggetto(String value) {
        this.oggetto = value;
    }

    /**
     * Gets the value of the protocolloEsterno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProtocolloEsterno() {
        return protocolloEsterno;
    }

    /**
     * Sets the value of the protocolloEsterno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProtocolloEsterno(String value) {
        this.protocolloEsterno = value;
    }

    /**
     * Gets the value of the protocolloMittente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProtocolloMittente() {
        return protocolloMittente;
    }

    /**
     * Sets the value of the protocolloMittente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProtocolloMittente(String value) {
        this.protocolloMittente = value;
    }

    /**
     * Gets the value of the registro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistro() {
        return registro;
    }

    /**
     * Sets the value of the registro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistro(String value) {
        this.registro = value;
    }

    /**
     * Gets the value of the registroProtocolloCollegato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistroProtocolloCollegato() {
        return registroProtocolloCollegato;
    }

    /**
     * Sets the value of the registroProtocolloCollegato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistroProtocolloCollegato(String value) {
        this.registroProtocolloCollegato = value;
    }

    /**
     * Gets the value of the riservato property.
     * 
     */
    public boolean isRiservato() {
        return riservato;
    }

    /**
     * Sets the value of the riservato property.
     * 
     */
    public void setRiservato(boolean value) {
        this.riservato = value;
    }

    /**
     * Gets the value of the speedCasellaEmail property.
     * 
     * @return
     *     possible object is
     *     {@link SpeedCasellaEmail }
     *     
     */
    public SpeedCasellaEmail getSpeedCasellaEmail() {
        return speedCasellaEmail;
    }

    /**
     * Sets the value of the speedCasellaEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpeedCasellaEmail }
     *     
     */
    public void setSpeedCasellaEmail(SpeedCasellaEmail value) {
        this.speedCasellaEmail = value;
    }

    /**
     * Gets the value of the speedMezzoSpedizione property.
     * 
     * @return
     *     possible object is
     *     {@link SpeedMezzoSpedizione }
     *     
     */
    public SpeedMezzoSpedizione getSpeedMezzoSpedizione() {
        return speedMezzoSpedizione;
    }

    /**
     * Sets the value of the speedMezzoSpedizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpeedMezzoSpedizione }
     *     
     */
    public void setSpeedMezzoSpedizione(SpeedMezzoSpedizione value) {
        this.speedMezzoSpedizione = value;
    }

    /**
     * Gets the value of the tipoDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Sets the value of the tipoDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDocumento(String value) {
        this.tipoDocumento = value;
    }

    /**
     * Gets the value of the tipologia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipologia() {
        return tipologia;
    }

    /**
     * Sets the value of the tipologia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipologia(String value) {
        this.tipologia = value;
    }

}
