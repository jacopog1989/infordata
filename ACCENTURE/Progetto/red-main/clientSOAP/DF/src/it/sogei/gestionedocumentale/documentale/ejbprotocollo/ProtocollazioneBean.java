
package it.sogei.gestionedocumentale.documentale.ejbprotocollo;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for protocollazioneBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="protocollazioneBean">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="allegati" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="allegatiId" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="codiceAoo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiceUfficioProtocollazione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataProtocollazione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataProtocolloCollegato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="docId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="file" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="fk_flusso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagPubblicazione" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="mittente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mittenteEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modalita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeAllegato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeFile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeRegistro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeProtocollo" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="numerator" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="numeroLista" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numeroProtocolloCollegato" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="oggetto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="parametriExtra" type="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}hashMapBean" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="riferimentoAreaDiScambio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="riferimentoAreaScambioAllegati" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="segnatura" type="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}segnaturaBean" minOccurs="0"/>
 *         &lt;element name="serviceName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="soggettoInteressato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="soggettoInteressatoEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipoDocumentoTelematico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ufficioTerritorialeTelematico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="uri" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "protocollazioneBean", propOrder = {
    "allegati",
    "allegatiId",
    "codiceAoo",
    "codiceUfficioProtocollazione",
    "dataProtocollazione",
    "dataProtocolloCollegato",
    "docId",
    "ente",
    "file",
    "fkFlusso",
    "flagPubblicazione",
    "mittente",
    "mittenteEmail",
    "modalita",
    "nomeAllegato",
    "nomeFile",
    "nomeRegistro",
    "numeProtocollo",
    "numerator",
    "numeroLista",
    "numeroProtocolloCollegato",
    "oggetto",
    "parametriExtra",
    "password",
    "riferimentoAreaDiScambio",
    "riferimentoAreaScambioAllegati",
    "segnatura",
    "serviceName",
    "soggettoInteressato",
    "soggettoInteressatoEmail",
    "tipoDocumentoTelematico",
    "ufficioTerritorialeTelematico",
    "uri",
    "userId"
})
public class ProtocollazioneBean {

    protected byte[] allegati;
    @XmlElement(nillable = true)
    protected List<String> allegatiId;
    protected String codiceAoo;
    protected String codiceUfficioProtocollazione;
    protected String dataProtocollazione;
    protected String dataProtocolloCollegato;
    protected String docId;
    protected String ente;
    protected byte[] file;
    @XmlElement(name = "fk_flusso")
    protected String fkFlusso;
    protected int flagPubblicazione;
    protected String mittente;
    protected String mittenteEmail;
    protected String modalita;
    protected String nomeAllegato;
    protected String nomeFile;
    protected String nomeRegistro;
    protected int numeProtocollo;
    protected long numerator;
    protected String numeroLista;
    protected long numeroProtocolloCollegato;
    protected String oggetto;
    @XmlElement(nillable = true)
    protected List<HashMapBean> parametriExtra;
    protected String password;
    protected String riferimentoAreaDiScambio;
    protected String riferimentoAreaScambioAllegati;
    protected SegnaturaBean segnatura;
    protected String serviceName;
    protected String soggettoInteressato;
    protected String soggettoInteressatoEmail;
    protected String tipoDocumentoTelematico;
    protected String ufficioTerritorialeTelematico;
    protected String uri;
    protected String userId;

    /**
     * Gets the value of the allegati property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getAllegati() {
        return allegati;
    }

    /**
     * Sets the value of the allegati property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setAllegati(byte[] value) {
        this.allegati = value;
    }

    /**
     * Gets the value of the allegatiId property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allegatiId property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllegatiId().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getAllegatiId() {
        if (allegatiId == null) {
            allegatiId = new ArrayList<String>();
        }
        return this.allegatiId;
    }

    /**
     * Gets the value of the codiceAoo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceAoo() {
        return codiceAoo;
    }

    /**
     * Sets the value of the codiceAoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceAoo(String value) {
        this.codiceAoo = value;
    }

    /**
     * Gets the value of the codiceUfficioProtocollazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceUfficioProtocollazione() {
        return codiceUfficioProtocollazione;
    }

    /**
     * Sets the value of the codiceUfficioProtocollazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceUfficioProtocollazione(String value) {
        this.codiceUfficioProtocollazione = value;
    }

    /**
     * Gets the value of the dataProtocollazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataProtocollazione() {
        return dataProtocollazione;
    }

    /**
     * Sets the value of the dataProtocollazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataProtocollazione(String value) {
        this.dataProtocollazione = value;
    }

    /**
     * Gets the value of the dataProtocolloCollegato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataProtocolloCollegato() {
        return dataProtocolloCollegato;
    }

    /**
     * Sets the value of the dataProtocolloCollegato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataProtocolloCollegato(String value) {
        this.dataProtocolloCollegato = value;
    }

    /**
     * Gets the value of the docId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocId() {
        return docId;
    }

    /**
     * Sets the value of the docId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocId(String value) {
        this.docId = value;
    }

    /**
     * Gets the value of the ente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnte() {
        return ente;
    }

    /**
     * Sets the value of the ente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnte(String value) {
        this.ente = value;
    }

    /**
     * Gets the value of the file property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getFile() {
        return file;
    }

    /**
     * Sets the value of the file property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setFile(byte[] value) {
        this.file = value;
    }

    /**
     * Gets the value of the fkFlusso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFkFlusso() {
        return fkFlusso;
    }

    /**
     * Sets the value of the fkFlusso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFkFlusso(String value) {
        this.fkFlusso = value;
    }

    /**
     * Gets the value of the flagPubblicazione property.
     * 
     */
    public int getFlagPubblicazione() {
        return flagPubblicazione;
    }

    /**
     * Sets the value of the flagPubblicazione property.
     * 
     */
    public void setFlagPubblicazione(int value) {
        this.flagPubblicazione = value;
    }

    /**
     * Gets the value of the mittente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMittente() {
        return mittente;
    }

    /**
     * Sets the value of the mittente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMittente(String value) {
        this.mittente = value;
    }

    /**
     * Gets the value of the mittenteEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMittenteEmail() {
        return mittenteEmail;
    }

    /**
     * Sets the value of the mittenteEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMittenteEmail(String value) {
        this.mittenteEmail = value;
    }

    /**
     * Gets the value of the modalita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModalita() {
        return modalita;
    }

    /**
     * Sets the value of the modalita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModalita(String value) {
        this.modalita = value;
    }

    /**
     * Gets the value of the nomeAllegato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeAllegato() {
        return nomeAllegato;
    }

    /**
     * Sets the value of the nomeAllegato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeAllegato(String value) {
        this.nomeAllegato = value;
    }

    /**
     * Gets the value of the nomeFile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeFile() {
        return nomeFile;
    }

    /**
     * Sets the value of the nomeFile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeFile(String value) {
        this.nomeFile = value;
    }

    /**
     * Gets the value of the nomeRegistro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeRegistro() {
        return nomeRegistro;
    }

    /**
     * Sets the value of the nomeRegistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeRegistro(String value) {
        this.nomeRegistro = value;
    }

    /**
     * Gets the value of the numeProtocollo property.
     * 
     */
    public int getNumeProtocollo() {
        return numeProtocollo;
    }

    /**
     * Sets the value of the numeProtocollo property.
     * 
     */
    public void setNumeProtocollo(int value) {
        this.numeProtocollo = value;
    }

    /**
     * Gets the value of the numerator property.
     * 
     */
    public long getNumerator() {
        return numerator;
    }

    /**
     * Sets the value of the numerator property.
     * 
     */
    public void setNumerator(long value) {
        this.numerator = value;
    }

    /**
     * Gets the value of the numeroLista property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroLista() {
        return numeroLista;
    }

    /**
     * Sets the value of the numeroLista property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroLista(String value) {
        this.numeroLista = value;
    }

    /**
     * Gets the value of the numeroProtocolloCollegato property.
     * 
     */
    public long getNumeroProtocolloCollegato() {
        return numeroProtocolloCollegato;
    }

    /**
     * Sets the value of the numeroProtocolloCollegato property.
     * 
     */
    public void setNumeroProtocolloCollegato(long value) {
        this.numeroProtocolloCollegato = value;
    }

    /**
     * Gets the value of the oggetto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggetto() {
        return oggetto;
    }

    /**
     * Sets the value of the oggetto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggetto(String value) {
        this.oggetto = value;
    }

    /**
     * Gets the value of the parametriExtra property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the parametriExtra property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParametriExtra().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HashMapBean }
     * 
     * 
     */
    public List<HashMapBean> getParametriExtra() {
        if (parametriExtra == null) {
            parametriExtra = new ArrayList<HashMapBean>();
        }
        return this.parametriExtra;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Gets the value of the riferimentoAreaDiScambio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiferimentoAreaDiScambio() {
        return riferimentoAreaDiScambio;
    }

    /**
     * Sets the value of the riferimentoAreaDiScambio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiferimentoAreaDiScambio(String value) {
        this.riferimentoAreaDiScambio = value;
    }

    /**
     * Gets the value of the riferimentoAreaScambioAllegati property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiferimentoAreaScambioAllegati() {
        return riferimentoAreaScambioAllegati;
    }

    /**
     * Sets the value of the riferimentoAreaScambioAllegati property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiferimentoAreaScambioAllegati(String value) {
        this.riferimentoAreaScambioAllegati = value;
    }

    /**
     * Gets the value of the segnatura property.
     * 
     * @return
     *     possible object is
     *     {@link SegnaturaBean }
     *     
     */
    public SegnaturaBean getSegnatura() {
        return segnatura;
    }

    /**
     * Sets the value of the segnatura property.
     * 
     * @param value
     *     allowed object is
     *     {@link SegnaturaBean }
     *     
     */
    public void setSegnatura(SegnaturaBean value) {
        this.segnatura = value;
    }

    /**
     * Gets the value of the serviceName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceName() {
        return serviceName;
    }

    /**
     * Sets the value of the serviceName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceName(String value) {
        this.serviceName = value;
    }

    /**
     * Gets the value of the soggettoInteressato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoggettoInteressato() {
        return soggettoInteressato;
    }

    /**
     * Sets the value of the soggettoInteressato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoggettoInteressato(String value) {
        this.soggettoInteressato = value;
    }

    /**
     * Gets the value of the soggettoInteressatoEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoggettoInteressatoEmail() {
        return soggettoInteressatoEmail;
    }

    /**
     * Sets the value of the soggettoInteressatoEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoggettoInteressatoEmail(String value) {
        this.soggettoInteressatoEmail = value;
    }

    /**
     * Gets the value of the tipoDocumentoTelematico property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDocumentoTelematico() {
        return tipoDocumentoTelematico;
    }

    /**
     * Sets the value of the tipoDocumentoTelematico property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDocumentoTelematico(String value) {
        this.tipoDocumentoTelematico = value;
    }

    /**
     * Gets the value of the ufficioTerritorialeTelematico property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUfficioTerritorialeTelematico() {
        return ufficioTerritorialeTelematico;
    }

    /**
     * Sets the value of the ufficioTerritorialeTelematico property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUfficioTerritorialeTelematico(String value) {
        this.ufficioTerritorialeTelematico = value;
    }

    /**
     * Gets the value of the uri property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUri() {
        return uri;
    }

    /**
     * Sets the value of the uri property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUri(String value) {
        this.uri = value;
    }

    /**
     * Gets the value of the userId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Sets the value of the userId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

}
