
package it.sogei.gestionedocumentale.documentale.ejbprotocollo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ricercaRegistroBean complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ricercaRegistroBean">
 *   &lt;complexContent>
 *     &lt;extension base="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}ricercaRegistroBeanWeb">
 *       &lt;sequence>
 *         &lt;element name="paginazione" type="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}paginazioneBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ricercaRegistroBean", propOrder = {
    "paginazione"
})
public class RicercaRegistroBean
    extends RicercaRegistroBeanWeb
{

    protected PaginazioneBean paginazione;

    /**
     * Gets the value of the paginazione property.
     * 
     * @return
     *     possible object is
     *     {@link PaginazioneBean }
     *     
     */
    public PaginazioneBean getPaginazione() {
        return paginazione;
    }

    /**
     * Sets the value of the paginazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link PaginazioneBean }
     *     
     */
    public void setPaginazione(PaginazioneBean value) {
        this.paginazione = value;
    }

}
