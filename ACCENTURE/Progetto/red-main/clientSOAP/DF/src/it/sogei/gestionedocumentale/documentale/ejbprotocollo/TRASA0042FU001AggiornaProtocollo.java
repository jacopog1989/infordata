
package it.sogei.gestionedocumentale.documentale.ejbprotocollo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TRA-SA-0042_FU_001_aggiornaProtocollo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TRA-SA-0042_FU_001_aggiornaProtocollo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="arg0" type="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}speedProtocolloBean" minOccurs="0"/>
 *         &lt;element name="arg1" type="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}authBean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TRA-SA-0042_FU_001_aggiornaProtocollo", propOrder = {
    "arg0",
    "arg1"
})
public class TRASA0042FU001AggiornaProtocollo {

    protected SpeedProtocolloBean arg0;
    protected AuthBean arg1;

    /**
     * Gets the value of the arg0 property.
     * 
     * @return
     *     possible object is
     *     {@link SpeedProtocolloBean }
     *     
     */
    public SpeedProtocolloBean getArg0() {
        return arg0;
    }

    /**
     * Sets the value of the arg0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link SpeedProtocolloBean }
     *     
     */
    public void setArg0(SpeedProtocolloBean value) {
        this.arg0 = value;
    }

    /**
     * Gets the value of the arg1 property.
     * 
     * @return
     *     possible object is
     *     {@link AuthBean }
     *     
     */
    public AuthBean getArg1() {
        return arg1;
    }

    /**
     * Sets the value of the arg1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthBean }
     *     
     */
    public void setArg1(AuthBean value) {
        this.arg1 = value;
    }

}
