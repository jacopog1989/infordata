
package it.sogei.gestionedocumentale.documentale.ejbprotocollo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for fascicoloFromSegnaturaOut complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="fascicoloFromSegnaturaOut">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="folderSignature" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeCartella" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="iChronicleId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fascicoloFromSegnaturaOut", propOrder = {
    "folderSignature",
    "nomeCartella",
    "iChronicleId"
})
public class FascicoloFromSegnaturaOut {

    protected String folderSignature;
    protected String nomeCartella;
    protected String iChronicleId;

    /**
     * Gets the value of the folderSignature property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFolderSignature() {
        return folderSignature;
    }

    /**
     * Sets the value of the folderSignature property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFolderSignature(String value) {
        this.folderSignature = value;
    }

    /**
     * Gets the value of the nomeCartella property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeCartella() {
        return nomeCartella;
    }

    /**
     * Sets the value of the nomeCartella property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeCartella(String value) {
        this.nomeCartella = value;
    }

    /**
     * Gets the value of the iChronicleId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIChronicleId() {
        return iChronicleId;
    }

    /**
     * Sets the value of the iChronicleId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIChronicleId(String value) {
        this.iChronicleId = value;
    }

}
