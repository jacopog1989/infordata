
package it.sogei.gestionedocumentale.documentale.ejbprotocollo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for speedMezzoSpedizione complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="speedMezzoSpedizione">
 *   &lt;complexContent>
 *     &lt;extension base="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}speedDataEntity">
 *       &lt;sequence>
 *         &lt;element name="codiMezzo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataAnnullamento" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="descrizione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="lAoo" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "speedMezzoSpedizione", propOrder = {
    "codiMezzo",
    "dataAnnullamento",
    "descrizione",
    "lAoo"
})
public class SpeedMezzoSpedizione
    extends SpeedDataEntity
{

    protected String codiMezzo;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataAnnullamento;
    protected String descrizione;
    protected Long lAoo;

    /**
     * Gets the value of the codiMezzo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiMezzo() {
        return codiMezzo;
    }

    /**
     * Sets the value of the codiMezzo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiMezzo(String value) {
        this.codiMezzo = value;
    }

    /**
     * Gets the value of the dataAnnullamento property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAnnullamento() {
        return dataAnnullamento;
    }

    /**
     * Sets the value of the dataAnnullamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAnnullamento(XMLGregorianCalendar value) {
        this.dataAnnullamento = value;
    }

    /**
     * Gets the value of the descrizione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Sets the value of the descrizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Gets the value of the lAoo property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getLAoo() {
        return lAoo;
    }

    /**
     * Sets the value of the lAoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setLAoo(Long value) {
        this.lAoo = value;
    }

}
