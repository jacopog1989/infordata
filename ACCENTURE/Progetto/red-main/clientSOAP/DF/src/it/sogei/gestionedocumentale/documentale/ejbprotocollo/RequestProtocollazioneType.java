
package it.sogei.gestionedocumentale.documentale.ejbprotocollo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for requestProtocollazioneType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="requestProtocollazioneType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="autenticazione" type="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}authenticationType" minOccurs="0"/>
 *         &lt;element name="protocolla" type="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}protocollaType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "requestProtocollazioneType", propOrder = {
    "autenticazione",
    "protocolla"
})
public class RequestProtocollazioneType {

    protected AuthenticationType autenticazione;
    protected ProtocollaType protocolla;

    /**
     * Gets the value of the autenticazione property.
     * 
     * @return
     *     possible object is
     *     {@link AuthenticationType }
     *     
     */
    public AuthenticationType getAutenticazione() {
        return autenticazione;
    }

    /**
     * Sets the value of the autenticazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthenticationType }
     *     
     */
    public void setAutenticazione(AuthenticationType value) {
        this.autenticazione = value;
    }

    /**
     * Gets the value of the protocolla property.
     * 
     * @return
     *     possible object is
     *     {@link ProtocollaType }
     *     
     */
    public ProtocollaType getProtocolla() {
        return protocolla;
    }

    /**
     * Sets the value of the protocolla property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtocollaType }
     *     
     */
    public void setProtocolla(ProtocollaType value) {
        this.protocolla = value;
    }

}
