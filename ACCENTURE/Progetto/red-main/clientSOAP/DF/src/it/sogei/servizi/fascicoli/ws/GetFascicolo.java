
package it.sogei.servizi.fascicoli.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getFascicolo complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="getFascicolo">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="iChronicleId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getFascicolo", propOrder = {
    "iChronicleId"
})
public class GetFascicolo {

    protected String iChronicleId;

    /**
     * Gets the value of the iChronicleId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIChronicleId() {
        return iChronicleId;
    }

    /**
     * Sets the value of the iChronicleId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIChronicleId(String value) {
        this.iChronicleId = value;
    }

}
