
package it.sogei.gestionedocumentale.documentale.ejbprotocollo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for speedBaseManager complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="speedBaseManager">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dataAggiornamento" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="iteratorGlobalCount" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "speedBaseManager", propOrder = {
    "dataAggiornamento",
    "iteratorGlobalCount"
})
@XmlSeeAlso({
    SpeedProtocolloMessageMananger.class
})
public class SpeedBaseManager {

    protected long dataAggiornamento;
    protected Long iteratorGlobalCount;

    /**
     * Gets the value of the dataAggiornamento property.
     * 
     */
    public long getDataAggiornamento() {
        return dataAggiornamento;
    }

    /**
     * Sets the value of the dataAggiornamento property.
     * 
     */
    public void setDataAggiornamento(long value) {
        this.dataAggiornamento = value;
    }

    /**
     * Gets the value of the iteratorGlobalCount property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIteratorGlobalCount() {
        return iteratorGlobalCount;
    }

    /**
     * Sets the value of the iteratorGlobalCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIteratorGlobalCount(Long value) {
        this.iteratorGlobalCount = value;
    }

}
