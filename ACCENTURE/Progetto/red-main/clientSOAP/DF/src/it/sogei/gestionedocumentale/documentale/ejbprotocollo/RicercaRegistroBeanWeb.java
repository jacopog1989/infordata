
package it.sogei.gestionedocumentale.documentale.ejbprotocollo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ricercaRegistroBeanWeb complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ricercaRegistroBeanWeb">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="oggettoRicerca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sequ_long_id_criptato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sequ_long_id" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="idRegistro" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="codRegistro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codAoo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numProtocollo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modalita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="progressivoDal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="progressivoAl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="progressivoRegDal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="progressivoRegAl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataProtocollazioneDa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataProtocollazioneA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="selTipologia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="oggetto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="emergCHK" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="classCHK" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="titolariHtml" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="daCollegati" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="daFascicoli" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idPratica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idFolder" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="daRidefTipiDoc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="firmatoCHK" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="conservatiCHK" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cartellaAttuale" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="presenzaCecPac" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mittDestPanelOpen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="classificazionePanelOpen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="protocollatorePanelOpen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="approvDocumentiOpen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="statoDocumentoPanelOpen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="invioDocumentiPanelOpen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="annoProtocollazione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="emNumeroRegistro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="emProtocollo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="emDataProtocolloDa" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="emDataProtocolloA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="emUtenteProtocollo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="emIdUtenteProtocollo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="emProgressivoImport" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="emModalita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="emDescrizioneRegistro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="identificativoControllo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="priorita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prioritaCHK" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chkStorAoo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chkStorUff" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeUfficioStor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idUfficioStor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="statoBozza" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descUfficioMittente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiceUfficio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="protocollatore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idProtocollatore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="assCHK" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="firmatario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nomeUfficio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ufficio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="docRiservato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datiSensibili" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="imgAssociata" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inFascicolo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="docAnnullati" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="attCHK1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="telCHK1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="property" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="docAnnullato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inPratica" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="interopNonSpediti" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="scaricatiCHK" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataScaricoDal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataScaricoAl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="chiusiCHK" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="protWorkflowCHK" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="assCHK1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numProtEsterno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataDocumentoDal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataDocumentoAl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mittente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codFiscaleMittente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="contrib" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codFiscaleContrib" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataRicezioneSpedizioneDal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataRicezioneSpedizioneAl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inviatiExtra1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="statoInvio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mezzoSpedizione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idAoo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numProtEmergenza" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="speditiCHK" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="collocazione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="descRegistro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tooltip" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="campoTitolario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inputParameters" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idUtente" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="modificare" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idRicerca" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idUfficio" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="openPanels" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idCasellaEmailIngresso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idCasellaEmailUscita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagCasellaUscita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagCasellaIngresso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inviatiExtraUscita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mezzoSpedizioneUscita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="statoInvioUscita" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idCasellaEmailIngressoEntrambi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="idCasellaEmailUscitaEntrambi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mezzoSpedizioneIngressoEntrambi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mezzoSpedizioneUscitaEntrambi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="statoInvioUscitaEntrambi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inviatiExtra1Entrambi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inviatiExtraUscitaEntrambi" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ricercaNearLine" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ricercaStorico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cmd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ricercaRegistroBeanWeb", propOrder = {
    "oggettoRicerca",
    "sequLongIdCriptato",
    "sequLongId",
    "idRegistro",
    "codRegistro",
    "codAoo",
    "numProtocollo",
    "modalita",
    "progressivoDal",
    "progressivoAl",
    "progressivoRegDal",
    "progressivoRegAl",
    "dataProtocollazioneDa",
    "dataProtocollazioneA",
    "selTipologia",
    "oggetto",
    "note",
    "emergCHK",
    "classCHK",
    "titolariHtml",
    "daCollegati",
    "daFascicoli",
    "idPratica",
    "idFolder",
    "daRidefTipiDoc",
    "firmatoCHK",
    "conservatiCHK",
    "cartellaAttuale",
    "presenzaCecPac",
    "mittDestPanelOpen",
    "classificazionePanelOpen",
    "protocollatorePanelOpen",
    "approvDocumentiOpen",
    "statoDocumentoPanelOpen",
    "invioDocumentiPanelOpen",
    "annoProtocollazione",
    "emNumeroRegistro",
    "emProtocollo",
    "emDataProtocolloDa",
    "emDataProtocolloA",
    "emUtenteProtocollo",
    "emIdUtenteProtocollo",
    "emProgressivoImport",
    "emModalita",
    "emDescrizioneRegistro",
    "identificativoControllo",
    "priorita",
    "prioritaCHK",
    "chkStorAoo",
    "chkStorUff",
    "nomeUfficioStor",
    "idUfficioStor",
    "statoBozza",
    "descUfficioMittente",
    "codiceUfficio",
    "protocollatore",
    "idProtocollatore",
    "assCHK",
    "firmatario",
    "nomeUfficio",
    "ufficio",
    "docRiservato",
    "datiSensibili",
    "imgAssociata",
    "inFascicolo",
    "docAnnullati",
    "attCHK1",
    "telCHK1",
    "property",
    "docAnnullato",
    "inPratica",
    "interopNonSpediti",
    "scaricatiCHK",
    "dataScaricoDal",
    "dataScaricoAl",
    "chiusiCHK",
    "protWorkflowCHK",
    "assCHK1",
    "numProtEsterno",
    "dataDocumentoDal",
    "dataDocumentoAl",
    "mittente",
    "codFiscaleMittente",
    "contrib",
    "codFiscaleContrib",
    "dataRicezioneSpedizioneDal",
    "dataRicezioneSpedizioneAl",
    "inviatiExtra1",
    "statoInvio",
    "mezzoSpedizione",
    "idAoo",
    "numProtEmergenza",
    "speditiCHK",
    "collocazione",
    "descRegistro",
    "tooltip",
    "campoTitolario",
    "inputParameters",
    "idUtente",
    "modificare",
    "idRicerca",
    "idUfficio",
    "openPanels",
    "idCasellaEmailIngresso",
    "idCasellaEmailUscita",
    "flagCasellaUscita",
    "flagCasellaIngresso",
    "inviatiExtraUscita",
    "mezzoSpedizioneUscita",
    "statoInvioUscita",
    "idCasellaEmailIngressoEntrambi",
    "idCasellaEmailUscitaEntrambi",
    "mezzoSpedizioneIngressoEntrambi",
    "mezzoSpedizioneUscitaEntrambi",
    "statoInvioUscitaEntrambi",
    "inviatiExtra1Entrambi",
    "inviatiExtraUscitaEntrambi",
    "ricercaNearLine",
    "ricercaStorico",
    "cmd"
})
@XmlSeeAlso({
    RicercaRegistroBean.class
})
public class RicercaRegistroBeanWeb {

    protected String oggettoRicerca;
    @XmlElement(name = "sequ_long_id_criptato")
    protected String sequLongIdCriptato;
    @XmlElement(name = "sequ_long_id")
    protected long sequLongId;
    @XmlElement(required = true)
    protected String idRegistro;
    protected String codRegistro;
    protected String codAoo;
    protected String numProtocollo;
    protected String modalita;
    protected String progressivoDal;
    protected String progressivoAl;
    protected String progressivoRegDal;
    protected String progressivoRegAl;
    protected String dataProtocollazioneDa;
    protected String dataProtocollazioneA;
    protected String selTipologia;
    protected String oggetto;
    protected String note;
    protected String emergCHK;
    protected String classCHK;
    protected String titolariHtml;
    protected String daCollegati;
    protected String daFascicoli;
    protected String idPratica;
    protected String idFolder;
    protected String daRidefTipiDoc;
    protected String firmatoCHK;
    protected String conservatiCHK;
    protected String cartellaAttuale;
    protected String presenzaCecPac;
    protected String mittDestPanelOpen;
    protected String classificazionePanelOpen;
    protected String protocollatorePanelOpen;
    protected String approvDocumentiOpen;
    protected String statoDocumentoPanelOpen;
    protected String invioDocumentiPanelOpen;
    protected String annoProtocollazione;
    protected String emNumeroRegistro;
    protected String emProtocollo;
    protected String emDataProtocolloDa;
    protected String emDataProtocolloA;
    protected String emUtenteProtocollo;
    protected String emIdUtenteProtocollo;
    protected String emProgressivoImport;
    protected String emModalita;
    protected String emDescrizioneRegistro;
    protected String identificativoControllo;
    protected String priorita;
    protected String prioritaCHK;
    protected String chkStorAoo;
    protected String chkStorUff;
    protected String nomeUfficioStor;
    protected String idUfficioStor;
    protected String statoBozza;
    protected String descUfficioMittente;
    protected String codiceUfficio;
    protected String protocollatore;
    protected String idProtocollatore;
    protected String assCHK;
    protected String firmatario;
    protected String nomeUfficio;
    protected String ufficio;
    protected String docRiservato;
    protected String datiSensibili;
    protected String imgAssociata;
    protected String inFascicolo;
    protected String docAnnullati;
    protected String attCHK1;
    protected String telCHK1;
    protected String property;
    protected String docAnnullato;
    protected String inPratica;
    protected String interopNonSpediti;
    protected String scaricatiCHK;
    protected String dataScaricoDal;
    protected String dataScaricoAl;
    protected String chiusiCHK;
    protected String protWorkflowCHK;
    protected String assCHK1;
    protected String numProtEsterno;
    protected String dataDocumentoDal;
    protected String dataDocumentoAl;
    protected String mittente;
    protected String codFiscaleMittente;
    protected String contrib;
    protected String codFiscaleContrib;
    protected String dataRicezioneSpedizioneDal;
    protected String dataRicezioneSpedizioneAl;
    protected String inviatiExtra1;
    protected String statoInvio;
    protected String mezzoSpedizione;
    @XmlElement(required = true)
    protected String idAoo;
    protected String numProtEmergenza;
    protected String speditiCHK;
    protected String collocazione;
    protected String descRegistro;
    protected String tooltip;
    protected String campoTitolario;
    protected String inputParameters;
    protected long idUtente;
    protected String modificare;
    protected String idRicerca;
    protected long idUfficio;
    protected String openPanels;
    protected String idCasellaEmailIngresso;
    protected String idCasellaEmailUscita;
    protected String flagCasellaUscita;
    protected String flagCasellaIngresso;
    protected String inviatiExtraUscita;
    protected String mezzoSpedizioneUscita;
    protected String statoInvioUscita;
    protected String idCasellaEmailIngressoEntrambi;
    protected String idCasellaEmailUscitaEntrambi;
    protected String mezzoSpedizioneIngressoEntrambi;
    protected String mezzoSpedizioneUscitaEntrambi;
    protected String statoInvioUscitaEntrambi;
    protected String inviatiExtra1Entrambi;
    protected String inviatiExtraUscitaEntrambi;
    protected String ricercaNearLine;
    protected String ricercaStorico;
    protected String cmd;

    /**
     * Gets the value of the oggettoRicerca property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggettoRicerca() {
        return oggettoRicerca;
    }

    /**
     * Sets the value of the oggettoRicerca property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggettoRicerca(String value) {
        this.oggettoRicerca = value;
    }

    /**
     * Gets the value of the sequLongIdCriptato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequLongIdCriptato() {
        return sequLongIdCriptato;
    }

    /**
     * Sets the value of the sequLongIdCriptato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequLongIdCriptato(String value) {
        this.sequLongIdCriptato = value;
    }

    /**
     * Gets the value of the sequLongId property.
     * 
     */
    public long getSequLongId() {
        return sequLongId;
    }

    /**
     * Sets the value of the sequLongId property.
     * 
     */
    public void setSequLongId(long value) {
        this.sequLongId = value;
    }

    /**
     * Gets the value of the idRegistro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRegistro() {
        return idRegistro;
    }

    /**
     * Sets the value of the idRegistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRegistro(String value) {
        this.idRegistro = value;
    }

    /**
     * Gets the value of the codRegistro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodRegistro() {
        return codRegistro;
    }

    /**
     * Sets the value of the codRegistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodRegistro(String value) {
        this.codRegistro = value;
    }

    /**
     * Gets the value of the codAoo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodAoo() {
        return codAoo;
    }

    /**
     * Sets the value of the codAoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodAoo(String value) {
        this.codAoo = value;
    }

    /**
     * Gets the value of the numProtocollo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumProtocollo() {
        return numProtocollo;
    }

    /**
     * Sets the value of the numProtocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumProtocollo(String value) {
        this.numProtocollo = value;
    }

    /**
     * Gets the value of the modalita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModalita() {
        return modalita;
    }

    /**
     * Sets the value of the modalita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModalita(String value) {
        this.modalita = value;
    }

    /**
     * Gets the value of the progressivoDal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgressivoDal() {
        return progressivoDal;
    }

    /**
     * Sets the value of the progressivoDal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgressivoDal(String value) {
        this.progressivoDal = value;
    }

    /**
     * Gets the value of the progressivoAl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgressivoAl() {
        return progressivoAl;
    }

    /**
     * Sets the value of the progressivoAl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgressivoAl(String value) {
        this.progressivoAl = value;
    }

    /**
     * Gets the value of the progressivoRegDal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgressivoRegDal() {
        return progressivoRegDal;
    }

    /**
     * Sets the value of the progressivoRegDal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgressivoRegDal(String value) {
        this.progressivoRegDal = value;
    }

    /**
     * Gets the value of the progressivoRegAl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProgressivoRegAl() {
        return progressivoRegAl;
    }

    /**
     * Sets the value of the progressivoRegAl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProgressivoRegAl(String value) {
        this.progressivoRegAl = value;
    }

    /**
     * Gets the value of the dataProtocollazioneDa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataProtocollazioneDa() {
        return dataProtocollazioneDa;
    }

    /**
     * Sets the value of the dataProtocollazioneDa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataProtocollazioneDa(String value) {
        this.dataProtocollazioneDa = value;
    }

    /**
     * Gets the value of the dataProtocollazioneA property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataProtocollazioneA() {
        return dataProtocollazioneA;
    }

    /**
     * Sets the value of the dataProtocollazioneA property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataProtocollazioneA(String value) {
        this.dataProtocollazioneA = value;
    }

    /**
     * Gets the value of the selTipologia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSelTipologia() {
        return selTipologia;
    }

    /**
     * Sets the value of the selTipologia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSelTipologia(String value) {
        this.selTipologia = value;
    }

    /**
     * Gets the value of the oggetto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggetto() {
        return oggetto;
    }

    /**
     * Sets the value of the oggetto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggetto(String value) {
        this.oggetto = value;
    }

    /**
     * Gets the value of the note property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNote() {
        return note;
    }

    /**
     * Sets the value of the note property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNote(String value) {
        this.note = value;
    }

    /**
     * Gets the value of the emergCHK property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmergCHK() {
        return emergCHK;
    }

    /**
     * Sets the value of the emergCHK property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmergCHK(String value) {
        this.emergCHK = value;
    }

    /**
     * Gets the value of the classCHK property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassCHK() {
        return classCHK;
    }

    /**
     * Sets the value of the classCHK property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassCHK(String value) {
        this.classCHK = value;
    }

    /**
     * Gets the value of the titolariHtml property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitolariHtml() {
        return titolariHtml;
    }

    /**
     * Sets the value of the titolariHtml property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitolariHtml(String value) {
        this.titolariHtml = value;
    }

    /**
     * Gets the value of the daCollegati property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDaCollegati() {
        return daCollegati;
    }

    /**
     * Sets the value of the daCollegati property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDaCollegati(String value) {
        this.daCollegati = value;
    }

    /**
     * Gets the value of the daFascicoli property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDaFascicoli() {
        return daFascicoli;
    }

    /**
     * Sets the value of the daFascicoli property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDaFascicoli(String value) {
        this.daFascicoli = value;
    }

    /**
     * Gets the value of the idPratica property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdPratica() {
        return idPratica;
    }

    /**
     * Sets the value of the idPratica property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdPratica(String value) {
        this.idPratica = value;
    }

    /**
     * Gets the value of the idFolder property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFolder() {
        return idFolder;
    }

    /**
     * Sets the value of the idFolder property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFolder(String value) {
        this.idFolder = value;
    }

    /**
     * Gets the value of the daRidefTipiDoc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDaRidefTipiDoc() {
        return daRidefTipiDoc;
    }

    /**
     * Sets the value of the daRidefTipiDoc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDaRidefTipiDoc(String value) {
        this.daRidefTipiDoc = value;
    }

    /**
     * Gets the value of the firmatoCHK property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirmatoCHK() {
        return firmatoCHK;
    }

    /**
     * Sets the value of the firmatoCHK property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirmatoCHK(String value) {
        this.firmatoCHK = value;
    }

    /**
     * Gets the value of the conservatiCHK property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConservatiCHK() {
        return conservatiCHK;
    }

    /**
     * Sets the value of the conservatiCHK property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConservatiCHK(String value) {
        this.conservatiCHK = value;
    }

    /**
     * Gets the value of the cartellaAttuale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCartellaAttuale() {
        return cartellaAttuale;
    }

    /**
     * Sets the value of the cartellaAttuale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCartellaAttuale(String value) {
        this.cartellaAttuale = value;
    }

    /**
     * Gets the value of the presenzaCecPac property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPresenzaCecPac() {
        return presenzaCecPac;
    }

    /**
     * Sets the value of the presenzaCecPac property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPresenzaCecPac(String value) {
        this.presenzaCecPac = value;
    }

    /**
     * Gets the value of the mittDestPanelOpen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMittDestPanelOpen() {
        return mittDestPanelOpen;
    }

    /**
     * Sets the value of the mittDestPanelOpen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMittDestPanelOpen(String value) {
        this.mittDestPanelOpen = value;
    }

    /**
     * Gets the value of the classificazionePanelOpen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassificazionePanelOpen() {
        return classificazionePanelOpen;
    }

    /**
     * Sets the value of the classificazionePanelOpen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassificazionePanelOpen(String value) {
        this.classificazionePanelOpen = value;
    }

    /**
     * Gets the value of the protocollatorePanelOpen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProtocollatorePanelOpen() {
        return protocollatorePanelOpen;
    }

    /**
     * Sets the value of the protocollatorePanelOpen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProtocollatorePanelOpen(String value) {
        this.protocollatorePanelOpen = value;
    }

    /**
     * Gets the value of the approvDocumentiOpen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApprovDocumentiOpen() {
        return approvDocumentiOpen;
    }

    /**
     * Sets the value of the approvDocumentiOpen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApprovDocumentiOpen(String value) {
        this.approvDocumentiOpen = value;
    }

    /**
     * Gets the value of the statoDocumentoPanelOpen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatoDocumentoPanelOpen() {
        return statoDocumentoPanelOpen;
    }

    /**
     * Sets the value of the statoDocumentoPanelOpen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatoDocumentoPanelOpen(String value) {
        this.statoDocumentoPanelOpen = value;
    }

    /**
     * Gets the value of the invioDocumentiPanelOpen property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvioDocumentiPanelOpen() {
        return invioDocumentiPanelOpen;
    }

    /**
     * Sets the value of the invioDocumentiPanelOpen property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvioDocumentiPanelOpen(String value) {
        this.invioDocumentiPanelOpen = value;
    }

    /**
     * Gets the value of the annoProtocollazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnoProtocollazione() {
        return annoProtocollazione;
    }

    /**
     * Sets the value of the annoProtocollazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnoProtocollazione(String value) {
        this.annoProtocollazione = value;
    }

    /**
     * Gets the value of the emNumeroRegistro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmNumeroRegistro() {
        return emNumeroRegistro;
    }

    /**
     * Sets the value of the emNumeroRegistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmNumeroRegistro(String value) {
        this.emNumeroRegistro = value;
    }

    /**
     * Gets the value of the emProtocollo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmProtocollo() {
        return emProtocollo;
    }

    /**
     * Sets the value of the emProtocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmProtocollo(String value) {
        this.emProtocollo = value;
    }

    /**
     * Gets the value of the emDataProtocolloDa property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmDataProtocolloDa() {
        return emDataProtocolloDa;
    }

    /**
     * Sets the value of the emDataProtocolloDa property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmDataProtocolloDa(String value) {
        this.emDataProtocolloDa = value;
    }

    /**
     * Gets the value of the emDataProtocolloA property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmDataProtocolloA() {
        return emDataProtocolloA;
    }

    /**
     * Sets the value of the emDataProtocolloA property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmDataProtocolloA(String value) {
        this.emDataProtocolloA = value;
    }

    /**
     * Gets the value of the emUtenteProtocollo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmUtenteProtocollo() {
        return emUtenteProtocollo;
    }

    /**
     * Sets the value of the emUtenteProtocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmUtenteProtocollo(String value) {
        this.emUtenteProtocollo = value;
    }

    /**
     * Gets the value of the emIdUtenteProtocollo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmIdUtenteProtocollo() {
        return emIdUtenteProtocollo;
    }

    /**
     * Sets the value of the emIdUtenteProtocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmIdUtenteProtocollo(String value) {
        this.emIdUtenteProtocollo = value;
    }

    /**
     * Gets the value of the emProgressivoImport property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmProgressivoImport() {
        return emProgressivoImport;
    }

    /**
     * Sets the value of the emProgressivoImport property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmProgressivoImport(String value) {
        this.emProgressivoImport = value;
    }

    /**
     * Gets the value of the emModalita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmModalita() {
        return emModalita;
    }

    /**
     * Sets the value of the emModalita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmModalita(String value) {
        this.emModalita = value;
    }

    /**
     * Gets the value of the emDescrizioneRegistro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmDescrizioneRegistro() {
        return emDescrizioneRegistro;
    }

    /**
     * Sets the value of the emDescrizioneRegistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmDescrizioneRegistro(String value) {
        this.emDescrizioneRegistro = value;
    }

    /**
     * Gets the value of the identificativoControllo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificativoControllo() {
        return identificativoControllo;
    }

    /**
     * Sets the value of the identificativoControllo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificativoControllo(String value) {
        this.identificativoControllo = value;
    }

    /**
     * Gets the value of the priorita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPriorita() {
        return priorita;
    }

    /**
     * Sets the value of the priorita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPriorita(String value) {
        this.priorita = value;
    }

    /**
     * Gets the value of the prioritaCHK property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrioritaCHK() {
        return prioritaCHK;
    }

    /**
     * Sets the value of the prioritaCHK property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrioritaCHK(String value) {
        this.prioritaCHK = value;
    }

    /**
     * Gets the value of the chkStorAoo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChkStorAoo() {
        return chkStorAoo;
    }

    /**
     * Sets the value of the chkStorAoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChkStorAoo(String value) {
        this.chkStorAoo = value;
    }

    /**
     * Gets the value of the chkStorUff property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChkStorUff() {
        return chkStorUff;
    }

    /**
     * Sets the value of the chkStorUff property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChkStorUff(String value) {
        this.chkStorUff = value;
    }

    /**
     * Gets the value of the nomeUfficioStor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeUfficioStor() {
        return nomeUfficioStor;
    }

    /**
     * Sets the value of the nomeUfficioStor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeUfficioStor(String value) {
        this.nomeUfficioStor = value;
    }

    /**
     * Gets the value of the idUfficioStor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdUfficioStor() {
        return idUfficioStor;
    }

    /**
     * Sets the value of the idUfficioStor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdUfficioStor(String value) {
        this.idUfficioStor = value;
    }

    /**
     * Gets the value of the statoBozza property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatoBozza() {
        return statoBozza;
    }

    /**
     * Sets the value of the statoBozza property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatoBozza(String value) {
        this.statoBozza = value;
    }

    /**
     * Gets the value of the descUfficioMittente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescUfficioMittente() {
        return descUfficioMittente;
    }

    /**
     * Sets the value of the descUfficioMittente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescUfficioMittente(String value) {
        this.descUfficioMittente = value;
    }

    /**
     * Gets the value of the codiceUfficio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceUfficio() {
        return codiceUfficio;
    }

    /**
     * Sets the value of the codiceUfficio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceUfficio(String value) {
        this.codiceUfficio = value;
    }

    /**
     * Gets the value of the protocollatore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProtocollatore() {
        return protocollatore;
    }

    /**
     * Sets the value of the protocollatore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProtocollatore(String value) {
        this.protocollatore = value;
    }

    /**
     * Gets the value of the idProtocollatore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdProtocollatore() {
        return idProtocollatore;
    }

    /**
     * Sets the value of the idProtocollatore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdProtocollatore(String value) {
        this.idProtocollatore = value;
    }

    /**
     * Gets the value of the assCHK property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssCHK() {
        return assCHK;
    }

    /**
     * Sets the value of the assCHK property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssCHK(String value) {
        this.assCHK = value;
    }

    /**
     * Gets the value of the firmatario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirmatario() {
        return firmatario;
    }

    /**
     * Sets the value of the firmatario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirmatario(String value) {
        this.firmatario = value;
    }

    /**
     * Gets the value of the nomeUfficio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeUfficio() {
        return nomeUfficio;
    }

    /**
     * Sets the value of the nomeUfficio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeUfficio(String value) {
        this.nomeUfficio = value;
    }

    /**
     * Gets the value of the ufficio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUfficio() {
        return ufficio;
    }

    /**
     * Sets the value of the ufficio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUfficio(String value) {
        this.ufficio = value;
    }

    /**
     * Gets the value of the docRiservato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocRiservato() {
        return docRiservato;
    }

    /**
     * Sets the value of the docRiservato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocRiservato(String value) {
        this.docRiservato = value;
    }

    /**
     * Gets the value of the datiSensibili property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDatiSensibili() {
        return datiSensibili;
    }

    /**
     * Sets the value of the datiSensibili property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDatiSensibili(String value) {
        this.datiSensibili = value;
    }

    /**
     * Gets the value of the imgAssociata property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getImgAssociata() {
        return imgAssociata;
    }

    /**
     * Sets the value of the imgAssociata property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setImgAssociata(String value) {
        this.imgAssociata = value;
    }

    /**
     * Gets the value of the inFascicolo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInFascicolo() {
        return inFascicolo;
    }

    /**
     * Sets the value of the inFascicolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInFascicolo(String value) {
        this.inFascicolo = value;
    }

    /**
     * Gets the value of the docAnnullati property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocAnnullati() {
        return docAnnullati;
    }

    /**
     * Sets the value of the docAnnullati property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocAnnullati(String value) {
        this.docAnnullati = value;
    }

    /**
     * Gets the value of the attCHK1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttCHK1() {
        return attCHK1;
    }

    /**
     * Sets the value of the attCHK1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttCHK1(String value) {
        this.attCHK1 = value;
    }

    /**
     * Gets the value of the telCHK1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTelCHK1() {
        return telCHK1;
    }

    /**
     * Sets the value of the telCHK1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTelCHK1(String value) {
        this.telCHK1 = value;
    }

    /**
     * Gets the value of the property property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProperty() {
        return property;
    }

    /**
     * Sets the value of the property property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProperty(String value) {
        this.property = value;
    }

    /**
     * Gets the value of the docAnnullato property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocAnnullato() {
        return docAnnullato;
    }

    /**
     * Sets the value of the docAnnullato property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocAnnullato(String value) {
        this.docAnnullato = value;
    }

    /**
     * Gets the value of the inPratica property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInPratica() {
        return inPratica;
    }

    /**
     * Sets the value of the inPratica property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInPratica(String value) {
        this.inPratica = value;
    }

    /**
     * Gets the value of the interopNonSpediti property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInteropNonSpediti() {
        return interopNonSpediti;
    }

    /**
     * Sets the value of the interopNonSpediti property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInteropNonSpediti(String value) {
        this.interopNonSpediti = value;
    }

    /**
     * Gets the value of the scaricatiCHK property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScaricatiCHK() {
        return scaricatiCHK;
    }

    /**
     * Sets the value of the scaricatiCHK property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScaricatiCHK(String value) {
        this.scaricatiCHK = value;
    }

    /**
     * Gets the value of the dataScaricoDal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataScaricoDal() {
        return dataScaricoDal;
    }

    /**
     * Sets the value of the dataScaricoDal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataScaricoDal(String value) {
        this.dataScaricoDal = value;
    }

    /**
     * Gets the value of the dataScaricoAl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataScaricoAl() {
        return dataScaricoAl;
    }

    /**
     * Sets the value of the dataScaricoAl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataScaricoAl(String value) {
        this.dataScaricoAl = value;
    }

    /**
     * Gets the value of the chiusiCHK property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiusiCHK() {
        return chiusiCHK;
    }

    /**
     * Sets the value of the chiusiCHK property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiusiCHK(String value) {
        this.chiusiCHK = value;
    }

    /**
     * Gets the value of the protWorkflowCHK property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProtWorkflowCHK() {
        return protWorkflowCHK;
    }

    /**
     * Sets the value of the protWorkflowCHK property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProtWorkflowCHK(String value) {
        this.protWorkflowCHK = value;
    }

    /**
     * Gets the value of the assCHK1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAssCHK1() {
        return assCHK1;
    }

    /**
     * Sets the value of the assCHK1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAssCHK1(String value) {
        this.assCHK1 = value;
    }

    /**
     * Gets the value of the numProtEsterno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumProtEsterno() {
        return numProtEsterno;
    }

    /**
     * Sets the value of the numProtEsterno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumProtEsterno(String value) {
        this.numProtEsterno = value;
    }

    /**
     * Gets the value of the dataDocumentoDal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataDocumentoDal() {
        return dataDocumentoDal;
    }

    /**
     * Sets the value of the dataDocumentoDal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataDocumentoDal(String value) {
        this.dataDocumentoDal = value;
    }

    /**
     * Gets the value of the dataDocumentoAl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataDocumentoAl() {
        return dataDocumentoAl;
    }

    /**
     * Sets the value of the dataDocumentoAl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataDocumentoAl(String value) {
        this.dataDocumentoAl = value;
    }

    /**
     * Gets the value of the mittente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMittente() {
        return mittente;
    }

    /**
     * Sets the value of the mittente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMittente(String value) {
        this.mittente = value;
    }

    /**
     * Gets the value of the codFiscaleMittente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodFiscaleMittente() {
        return codFiscaleMittente;
    }

    /**
     * Sets the value of the codFiscaleMittente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodFiscaleMittente(String value) {
        this.codFiscaleMittente = value;
    }

    /**
     * Gets the value of the contrib property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContrib() {
        return contrib;
    }

    /**
     * Sets the value of the contrib property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContrib(String value) {
        this.contrib = value;
    }

    /**
     * Gets the value of the codFiscaleContrib property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodFiscaleContrib() {
        return codFiscaleContrib;
    }

    /**
     * Sets the value of the codFiscaleContrib property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodFiscaleContrib(String value) {
        this.codFiscaleContrib = value;
    }

    /**
     * Gets the value of the dataRicezioneSpedizioneDal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataRicezioneSpedizioneDal() {
        return dataRicezioneSpedizioneDal;
    }

    /**
     * Sets the value of the dataRicezioneSpedizioneDal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataRicezioneSpedizioneDal(String value) {
        this.dataRicezioneSpedizioneDal = value;
    }

    /**
     * Gets the value of the dataRicezioneSpedizioneAl property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDataRicezioneSpedizioneAl() {
        return dataRicezioneSpedizioneAl;
    }

    /**
     * Sets the value of the dataRicezioneSpedizioneAl property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDataRicezioneSpedizioneAl(String value) {
        this.dataRicezioneSpedizioneAl = value;
    }

    /**
     * Gets the value of the inviatiExtra1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInviatiExtra1() {
        return inviatiExtra1;
    }

    /**
     * Sets the value of the inviatiExtra1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInviatiExtra1(String value) {
        this.inviatiExtra1 = value;
    }

    /**
     * Gets the value of the statoInvio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatoInvio() {
        return statoInvio;
    }

    /**
     * Sets the value of the statoInvio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatoInvio(String value) {
        this.statoInvio = value;
    }

    /**
     * Gets the value of the mezzoSpedizione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMezzoSpedizione() {
        return mezzoSpedizione;
    }

    /**
     * Sets the value of the mezzoSpedizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMezzoSpedizione(String value) {
        this.mezzoSpedizione = value;
    }

    /**
     * Gets the value of the idAoo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdAoo() {
        return idAoo;
    }

    /**
     * Sets the value of the idAoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdAoo(String value) {
        this.idAoo = value;
    }

    /**
     * Gets the value of the numProtEmergenza property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumProtEmergenza() {
        return numProtEmergenza;
    }

    /**
     * Sets the value of the numProtEmergenza property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumProtEmergenza(String value) {
        this.numProtEmergenza = value;
    }

    /**
     * Gets the value of the speditiCHK property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpeditiCHK() {
        return speditiCHK;
    }

    /**
     * Sets the value of the speditiCHK property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpeditiCHK(String value) {
        this.speditiCHK = value;
    }

    /**
     * Gets the value of the collocazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCollocazione() {
        return collocazione;
    }

    /**
     * Sets the value of the collocazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollocazione(String value) {
        this.collocazione = value;
    }

    /**
     * Gets the value of the descRegistro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescRegistro() {
        return descRegistro;
    }

    /**
     * Sets the value of the descRegistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescRegistro(String value) {
        this.descRegistro = value;
    }

    /**
     * Gets the value of the tooltip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTooltip() {
        return tooltip;
    }

    /**
     * Sets the value of the tooltip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTooltip(String value) {
        this.tooltip = value;
    }

    /**
     * Gets the value of the campoTitolario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCampoTitolario() {
        return campoTitolario;
    }

    /**
     * Sets the value of the campoTitolario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCampoTitolario(String value) {
        this.campoTitolario = value;
    }

    /**
     * Gets the value of the inputParameters property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInputParameters() {
        return inputParameters;
    }

    /**
     * Sets the value of the inputParameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInputParameters(String value) {
        this.inputParameters = value;
    }

    /**
     * Gets the value of the idUtente property.
     * 
     */
    public long getIdUtente() {
        return idUtente;
    }

    /**
     * Sets the value of the idUtente property.
     * 
     */
    public void setIdUtente(long value) {
        this.idUtente = value;
    }

    /**
     * Gets the value of the modificare property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModificare() {
        return modificare;
    }

    /**
     * Sets the value of the modificare property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModificare(String value) {
        this.modificare = value;
    }

    /**
     * Gets the value of the idRicerca property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRicerca() {
        return idRicerca;
    }

    /**
     * Sets the value of the idRicerca property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRicerca(String value) {
        this.idRicerca = value;
    }

    /**
     * Gets the value of the idUfficio property.
     * 
     */
    public long getIdUfficio() {
        return idUfficio;
    }

    /**
     * Sets the value of the idUfficio property.
     * 
     */
    public void setIdUfficio(long value) {
        this.idUfficio = value;
    }

    /**
     * Gets the value of the openPanels property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOpenPanels() {
        return openPanels;
    }

    /**
     * Sets the value of the openPanels property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOpenPanels(String value) {
        this.openPanels = value;
    }

    /**
     * Gets the value of the idCasellaEmailIngresso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdCasellaEmailIngresso() {
        return idCasellaEmailIngresso;
    }

    /**
     * Sets the value of the idCasellaEmailIngresso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdCasellaEmailIngresso(String value) {
        this.idCasellaEmailIngresso = value;
    }

    /**
     * Gets the value of the idCasellaEmailUscita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdCasellaEmailUscita() {
        return idCasellaEmailUscita;
    }

    /**
     * Sets the value of the idCasellaEmailUscita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdCasellaEmailUscita(String value) {
        this.idCasellaEmailUscita = value;
    }

    /**
     * Gets the value of the flagCasellaUscita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagCasellaUscita() {
        return flagCasellaUscita;
    }

    /**
     * Sets the value of the flagCasellaUscita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagCasellaUscita(String value) {
        this.flagCasellaUscita = value;
    }

    /**
     * Gets the value of the flagCasellaIngresso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlagCasellaIngresso() {
        return flagCasellaIngresso;
    }

    /**
     * Sets the value of the flagCasellaIngresso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlagCasellaIngresso(String value) {
        this.flagCasellaIngresso = value;
    }

    /**
     * Gets the value of the inviatiExtraUscita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInviatiExtraUscita() {
        return inviatiExtraUscita;
    }

    /**
     * Sets the value of the inviatiExtraUscita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInviatiExtraUscita(String value) {
        this.inviatiExtraUscita = value;
    }

    /**
     * Gets the value of the mezzoSpedizioneUscita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMezzoSpedizioneUscita() {
        return mezzoSpedizioneUscita;
    }

    /**
     * Sets the value of the mezzoSpedizioneUscita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMezzoSpedizioneUscita(String value) {
        this.mezzoSpedizioneUscita = value;
    }

    /**
     * Gets the value of the statoInvioUscita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatoInvioUscita() {
        return statoInvioUscita;
    }

    /**
     * Sets the value of the statoInvioUscita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatoInvioUscita(String value) {
        this.statoInvioUscita = value;
    }

    /**
     * Gets the value of the idCasellaEmailIngressoEntrambi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdCasellaEmailIngressoEntrambi() {
        return idCasellaEmailIngressoEntrambi;
    }

    /**
     * Sets the value of the idCasellaEmailIngressoEntrambi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdCasellaEmailIngressoEntrambi(String value) {
        this.idCasellaEmailIngressoEntrambi = value;
    }

    /**
     * Gets the value of the idCasellaEmailUscitaEntrambi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdCasellaEmailUscitaEntrambi() {
        return idCasellaEmailUscitaEntrambi;
    }

    /**
     * Sets the value of the idCasellaEmailUscitaEntrambi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdCasellaEmailUscitaEntrambi(String value) {
        this.idCasellaEmailUscitaEntrambi = value;
    }

    /**
     * Gets the value of the mezzoSpedizioneIngressoEntrambi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMezzoSpedizioneIngressoEntrambi() {
        return mezzoSpedizioneIngressoEntrambi;
    }

    /**
     * Sets the value of the mezzoSpedizioneIngressoEntrambi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMezzoSpedizioneIngressoEntrambi(String value) {
        this.mezzoSpedizioneIngressoEntrambi = value;
    }

    /**
     * Gets the value of the mezzoSpedizioneUscitaEntrambi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMezzoSpedizioneUscitaEntrambi() {
        return mezzoSpedizioneUscitaEntrambi;
    }

    /**
     * Sets the value of the mezzoSpedizioneUscitaEntrambi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMezzoSpedizioneUscitaEntrambi(String value) {
        this.mezzoSpedizioneUscitaEntrambi = value;
    }

    /**
     * Gets the value of the statoInvioUscitaEntrambi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatoInvioUscitaEntrambi() {
        return statoInvioUscitaEntrambi;
    }

    /**
     * Sets the value of the statoInvioUscitaEntrambi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatoInvioUscitaEntrambi(String value) {
        this.statoInvioUscitaEntrambi = value;
    }

    /**
     * Gets the value of the inviatiExtra1Entrambi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInviatiExtra1Entrambi() {
        return inviatiExtra1Entrambi;
    }

    /**
     * Sets the value of the inviatiExtra1Entrambi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInviatiExtra1Entrambi(String value) {
        this.inviatiExtra1Entrambi = value;
    }

    /**
     * Gets the value of the inviatiExtraUscitaEntrambi property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInviatiExtraUscitaEntrambi() {
        return inviatiExtraUscitaEntrambi;
    }

    /**
     * Sets the value of the inviatiExtraUscitaEntrambi property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInviatiExtraUscitaEntrambi(String value) {
        this.inviatiExtraUscitaEntrambi = value;
    }

    /**
     * Gets the value of the ricercaNearLine property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRicercaNearLine() {
        return ricercaNearLine;
    }

    /**
     * Sets the value of the ricercaNearLine property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRicercaNearLine(String value) {
        this.ricercaNearLine = value;
    }

    /**
     * Gets the value of the ricercaStorico property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRicercaStorico() {
        return ricercaStorico;
    }

    /**
     * Sets the value of the ricercaStorico property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRicercaStorico(String value) {
        this.ricercaStorico = value;
    }

    /**
     * Gets the value of the cmd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCmd() {
        return cmd;
    }

    /**
     * Sets the value of the cmd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCmd(String value) {
        this.cmd = value;
    }

}
