
package it.sogei.servizi.fascicoli.ws;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "FascicoliWS", targetNamespace = "http://ws.fascicoli.servizi.sogei.it/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface FascicoliWS {


    /**
     * 
     * @param arg1
     * @param arg0
     * @return
     *     returns java.util.List<it.sogei.servizi.fascicoli.ws.Fascicolo>
     * @throws Exception_Exception
     */
    @WebMethod(operationName = "TRA-SA-0042_FU_006_ricercaFascicoli", action = "TRA-SA-0042_FU_006_ricercaFascicoli")
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "TRA-SA-0042_FU_006_ricercaFascicoli", targetNamespace = "http://ws.fascicoli.servizi.sogei.it/", className = "it.sogei.servizi.fascicoli.ws.TRASA0042FU006RicercaFascicoli")
    @ResponseWrapper(localName = "TRA-SA-0042_FU_006_ricercaFascicoliResponse", targetNamespace = "http://ws.fascicoli.servizi.sogei.it/", className = "it.sogei.servizi.fascicoli.ws.TRASA0042FU006RicercaFascicoliResponse")
    @Action(input = "TRA-SA-0042_FU_006_ricercaFascicoli", output = "http://ws.fascicoli.servizi.sogei.it/FascicoliWS/TRA-SA-0042_FU_006_ricercaFascicoliResponse", fault = {
        @FaultAction(className = Exception_Exception.class, value = "http://ws.fascicoli.servizi.sogei.it/FascicoliWS/TRA-SA-0042_FU_006_ricercaFascicoli/Fault/Exception")
    })
    public List<Fascicolo> traSA0042FU006RicercaFascicoli(
        @WebParam(name = "arg0", targetNamespace = "")
        AuthenticationBean arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        RicercaFascicoli arg1)
        throws Exception_Exception
    ;

    /**
     * 
     * @param arg1
     * @param arg0
     * @return
     *     returns int
     * @throws Exception_Exception
     */
    @WebMethod(operationName = "TRA-SA-0042_FU_006_countRicercaFascicoli", action = "TRA-SA-0042_FU_006_countRicercaFascicoli")
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "TRA-SA-0042_FU_006_countRicercaFascicoli", targetNamespace = "http://ws.fascicoli.servizi.sogei.it/", className = "it.sogei.servizi.fascicoli.ws.TRASA0042FU006CountRicercaFascicoli")
    @ResponseWrapper(localName = "TRA-SA-0042_FU_006_countRicercaFascicoliResponse", targetNamespace = "http://ws.fascicoli.servizi.sogei.it/", className = "it.sogei.servizi.fascicoli.ws.TRASA0042FU006CountRicercaFascicoliResponse")
    @Action(input = "TRA-SA-0042_FU_006_countRicercaFascicoli", output = "http://ws.fascicoli.servizi.sogei.it/FascicoliWS/TRA-SA-0042_FU_006_countRicercaFascicoliResponse", fault = {
        @FaultAction(className = Exception_Exception.class, value = "http://ws.fascicoli.servizi.sogei.it/FascicoliWS/TRA-SA-0042_FU_006_countRicercaFascicoli/Fault/Exception")
    })
    public int traSA0042FU006CountRicercaFascicoli(
        @WebParam(name = "arg0", targetNamespace = "")
        AuthenticationBean arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        RicercaFascicoli arg1)
        throws Exception_Exception
    ;

    /**
     * 
     * @param arg1
     * @param arg0
     * @return
     *     returns it.sogei.servizi.fascicoli.ws.Fascicolo
     * @throws Exception_Exception
     */
    @WebMethod(operationName = "TRA-SA-0042_FU_006_getFascicoloById", action = "TRA-SA-0042_FU_006_getFascicoloById")
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "TRA-SA-0042_FU_006_getFascicoloById", targetNamespace = "http://ws.fascicoli.servizi.sogei.it/", className = "it.sogei.servizi.fascicoli.ws.TRASA0042FU006GetFascicoloById")
    @ResponseWrapper(localName = "TRA-SA-0042_FU_006_getFascicoloByIdResponse", targetNamespace = "http://ws.fascicoli.servizi.sogei.it/", className = "it.sogei.servizi.fascicoli.ws.TRASA0042FU006GetFascicoloByIdResponse")
    @Action(input = "TRA-SA-0042_FU_006_getFascicoloById", output = "http://ws.fascicoli.servizi.sogei.it/FascicoliWS/TRA-SA-0042_FU_006_getFascicoloByIdResponse", fault = {
        @FaultAction(className = Exception_Exception.class, value = "http://ws.fascicoli.servizi.sogei.it/FascicoliWS/TRA-SA-0042_FU_006_getFascicoloById/Fault/Exception")
    })
    public Fascicolo traSA0042FU006GetFascicoloById(
        @WebParam(name = "arg0", targetNamespace = "")
        AuthenticationBean arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        GetFascicolo arg1)
        throws Exception_Exception
    ;

}
