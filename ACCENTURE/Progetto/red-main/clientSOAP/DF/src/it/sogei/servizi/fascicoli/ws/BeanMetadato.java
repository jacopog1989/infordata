
package it.sogei.servizi.fascicoli.ws;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for beanMetadato complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="beanMetadato">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="descrizione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dimensione" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="label" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="listaValori" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="multivalore" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="nomeFisico" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="obbligatorio" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="tipoValore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="valoriAmmessi" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "beanMetadato", propOrder = {
    "descrizione",
    "dimensione",
    "id",
    "label",
    "listaValori",
    "multivalore",
    "nomeFisico",
    "obbligatorio",
    "tipoValore",
    "valore",
    "valoriAmmessi"
})
public class BeanMetadato {

    protected String descrizione;
    protected int dimensione;
    protected Long id;
    protected String label;
    @XmlElement(nillable = true)
    protected List<String> listaValori;
    protected boolean multivalore;
    protected String nomeFisico;
    protected boolean obbligatorio;
    protected String tipoValore;
    protected String valore;
    @XmlElement(nillable = true)
    protected List<String> valoriAmmessi;

    /**
     * Gets the value of the descrizione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Sets the value of the descrizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Gets the value of the dimensione property.
     * 
     */
    public int getDimensione() {
        return dimensione;
    }

    /**
     * Sets the value of the dimensione property.
     * 
     */
    public void setDimensione(int value) {
        this.dimensione = value;
    }

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setId(Long value) {
        this.id = value;
    }

    /**
     * Gets the value of the label property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets the value of the label property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the listaValori property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the listaValori property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getListaValori().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getListaValori() {
        if (listaValori == null) {
            listaValori = new ArrayList<String>();
        }
        return this.listaValori;
    }

    /**
     * Gets the value of the multivalore property.
     * 
     */
    public boolean isMultivalore() {
        return multivalore;
    }

    /**
     * Sets the value of the multivalore property.
     * 
     */
    public void setMultivalore(boolean value) {
        this.multivalore = value;
    }

    /**
     * Gets the value of the nomeFisico property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeFisico() {
        return nomeFisico;
    }

    /**
     * Sets the value of the nomeFisico property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeFisico(String value) {
        this.nomeFisico = value;
    }

    /**
     * Gets the value of the obbligatorio property.
     * 
     */
    public boolean isObbligatorio() {
        return obbligatorio;
    }

    /**
     * Sets the value of the obbligatorio property.
     * 
     */
    public void setObbligatorio(boolean value) {
        this.obbligatorio = value;
    }

    /**
     * Gets the value of the tipoValore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoValore() {
        return tipoValore;
    }

    /**
     * Sets the value of the tipoValore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoValore(String value) {
        this.tipoValore = value;
    }

    /**
     * Gets the value of the valore property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValore() {
        return valore;
    }

    /**
     * Sets the value of the valore property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValore(String value) {
        this.valore = value;
    }

    /**
     * Gets the value of the valoriAmmessi property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the valoriAmmessi property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValoriAmmessi().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getValoriAmmessi() {
        if (valoriAmmessi == null) {
            valoriAmmessi = new ArrayList<String>();
        }
        return this.valoriAmmessi;
    }

}
