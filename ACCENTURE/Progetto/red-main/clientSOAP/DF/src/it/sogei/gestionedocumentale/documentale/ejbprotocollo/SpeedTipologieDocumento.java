
package it.sogei.gestionedocumentale.documentale.ejbprotocollo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for speedTipologieDocumento complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="speedTipologieDocumento">
 *   &lt;complexContent>
 *     &lt;extension base="{http://gestionedocumentale.sogei.it/documentale/ejbprotocollo}speedDataEntity">
 *       &lt;sequence>
 *         &lt;element name="aooId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="archivioDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="attiva" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="classeDocumentale" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="codiceAoo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dataAttivazione" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="dataDisattivazione" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="descTipologia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="errorMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="flagStato" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="giorniRinvio" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="indicazioneDocUnico" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="livelloClassificazione" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="mascheraInserimento" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="nascosto" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="nomeTipologia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="numFamiglia" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="numVersione" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="obblTrasmissione" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="tempoVita" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="xmlCodi" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="xmlFile" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "speedTipologieDocumento", propOrder = {
    "aooId",
    "archivioDocumento",
    "attiva",
    "classeDocumentale",
    "codiceAoo",
    "dataAttivazione",
    "dataDisattivazione",
    "descTipologia",
    "errorMessage",
    "flagStato",
    "giorniRinvio",
    "indicazioneDocUnico",
    "livelloClassificazione",
    "mascheraInserimento",
    "nascosto",
    "nomeTipologia",
    "numFamiglia",
    "numVersione",
    "obblTrasmissione",
    "tempoVita",
    "xmlCodi",
    "xmlFile"
})
public class SpeedTipologieDocumento
    extends SpeedDataEntity
{

    protected Long aooId;
    protected String archivioDocumento;
    protected Integer attiva;
    protected String classeDocumentale;
    protected String codiceAoo;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataAttivazione;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataDisattivazione;
    protected String descTipologia;
    protected String errorMessage;
    protected Integer flagStato;
    protected Integer giorniRinvio;
    protected Integer indicazioneDocUnico;
    protected Integer livelloClassificazione;
    protected Integer mascheraInserimento;
    protected Integer nascosto;
    protected String nomeTipologia;
    protected Long numFamiglia;
    protected Long numVersione;
    protected Integer obblTrasmissione;
    protected Integer tempoVita;
    protected byte[] xmlCodi;
    protected byte[] xmlFile;

    /**
     * Gets the value of the aooId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getAooId() {
        return aooId;
    }

    /**
     * Sets the value of the aooId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setAooId(Long value) {
        this.aooId = value;
    }

    /**
     * Gets the value of the archivioDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArchivioDocumento() {
        return archivioDocumento;
    }

    /**
     * Sets the value of the archivioDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArchivioDocumento(String value) {
        this.archivioDocumento = value;
    }

    /**
     * Gets the value of the attiva property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAttiva() {
        return attiva;
    }

    /**
     * Sets the value of the attiva property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAttiva(Integer value) {
        this.attiva = value;
    }

    /**
     * Gets the value of the classeDocumentale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClasseDocumentale() {
        return classeDocumentale;
    }

    /**
     * Sets the value of the classeDocumentale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClasseDocumentale(String value) {
        this.classeDocumentale = value;
    }

    /**
     * Gets the value of the codiceAoo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceAoo() {
        return codiceAoo;
    }

    /**
     * Sets the value of the codiceAoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceAoo(String value) {
        this.codiceAoo = value;
    }

    /**
     * Gets the value of the dataAttivazione property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAttivazione() {
        return dataAttivazione;
    }

    /**
     * Sets the value of the dataAttivazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAttivazione(XMLGregorianCalendar value) {
        this.dataAttivazione = value;
    }

    /**
     * Gets the value of the dataDisattivazione property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataDisattivazione() {
        return dataDisattivazione;
    }

    /**
     * Sets the value of the dataDisattivazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataDisattivazione(XMLGregorianCalendar value) {
        this.dataDisattivazione = value;
    }

    /**
     * Gets the value of the descTipologia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescTipologia() {
        return descTipologia;
    }

    /**
     * Sets the value of the descTipologia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescTipologia(String value) {
        this.descTipologia = value;
    }

    /**
     * Gets the value of the errorMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sets the value of the errorMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorMessage(String value) {
        this.errorMessage = value;
    }

    /**
     * Gets the value of the flagStato property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFlagStato() {
        return flagStato;
    }

    /**
     * Sets the value of the flagStato property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFlagStato(Integer value) {
        this.flagStato = value;
    }

    /**
     * Gets the value of the giorniRinvio property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getGiorniRinvio() {
        return giorniRinvio;
    }

    /**
     * Sets the value of the giorniRinvio property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setGiorniRinvio(Integer value) {
        this.giorniRinvio = value;
    }

    /**
     * Gets the value of the indicazioneDocUnico property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getIndicazioneDocUnico() {
        return indicazioneDocUnico;
    }

    /**
     * Sets the value of the indicazioneDocUnico property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setIndicazioneDocUnico(Integer value) {
        this.indicazioneDocUnico = value;
    }

    /**
     * Gets the value of the livelloClassificazione property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getLivelloClassificazione() {
        return livelloClassificazione;
    }

    /**
     * Sets the value of the livelloClassificazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setLivelloClassificazione(Integer value) {
        this.livelloClassificazione = value;
    }

    /**
     * Gets the value of the mascheraInserimento property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getMascheraInserimento() {
        return mascheraInserimento;
    }

    /**
     * Sets the value of the mascheraInserimento property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setMascheraInserimento(Integer value) {
        this.mascheraInserimento = value;
    }

    /**
     * Gets the value of the nascosto property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNascosto() {
        return nascosto;
    }

    /**
     * Sets the value of the nascosto property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNascosto(Integer value) {
        this.nascosto = value;
    }

    /**
     * Gets the value of the nomeTipologia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomeTipologia() {
        return nomeTipologia;
    }

    /**
     * Sets the value of the nomeTipologia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomeTipologia(String value) {
        this.nomeTipologia = value;
    }

    /**
     * Gets the value of the numFamiglia property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumFamiglia() {
        return numFamiglia;
    }

    /**
     * Sets the value of the numFamiglia property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumFamiglia(Long value) {
        this.numFamiglia = value;
    }

    /**
     * Gets the value of the numVersione property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNumVersione() {
        return numVersione;
    }

    /**
     * Sets the value of the numVersione property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNumVersione(Long value) {
        this.numVersione = value;
    }

    /**
     * Gets the value of the obblTrasmissione property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getObblTrasmissione() {
        return obblTrasmissione;
    }

    /**
     * Sets the value of the obblTrasmissione property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setObblTrasmissione(Integer value) {
        this.obblTrasmissione = value;
    }

    /**
     * Gets the value of the tempoVita property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getTempoVita() {
        return tempoVita;
    }

    /**
     * Sets the value of the tempoVita property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setTempoVita(Integer value) {
        this.tempoVita = value;
    }

    /**
     * Gets the value of the xmlCodi property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getXmlCodi() {
        return xmlCodi;
    }

    /**
     * Sets the value of the xmlCodi property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setXmlCodi(byte[] value) {
        this.xmlCodi = value;
    }

    /**
     * Gets the value of the xmlFile property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getXmlFile() {
        return xmlFile;
    }

    /**
     * Sets the value of the xmlFile property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setXmlFile(byte[] value) {
        this.xmlFile = value;
    }

}
