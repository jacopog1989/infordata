package it.ibm.red.business.df.exceptions;

public class BusinessDelegateRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private String message;

	public BusinessDelegateRuntimeException(Exception exception) {
		super(exception);
	}

	public BusinessDelegateRuntimeException(String msg) {
		super(msg);
	}

	public BusinessDelegateRuntimeException(String msg, Exception e) {
		super(e);
		this.message = msg;
	}

	public String getMessage() {
		if (this.message != null) {
			return this.message;
		}
		return super.getMessage();
	}
}