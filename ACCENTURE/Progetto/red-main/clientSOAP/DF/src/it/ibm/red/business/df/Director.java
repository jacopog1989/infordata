package it.ibm.red.business.df;
   
 

import java.util.ArrayList;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
 
import it.sogei.gestionedocumentale.documentale.ejbprotocollo.PaginazioneBean;
import it.sogei.gestionedocumentale.documentale.ejbprotocollo.RicercaRegistroBean;
import it.sogei.servizi.fascicoli.ws.GetFascicolo;
import it.sogei.servizi.fascicoli.ws.RicercaFascicoli;  
 

public class Director {
 
	private static it.sogei.servizi.fascicoli.ws.ObjectFactory ofTypesFasc;
	private static it.sogei.gestionedocumentale.documentale.ejbprotocollo.ObjectFactory ofTypesProt;
	
	private static final String orderForProtocollo = "idProtocollo";
	
	static {
		ofTypesFasc = new it.sogei.servizi.fascicoli.ws.ObjectFactory();
		ofTypesProt = new it.sogei.gestionedocumentale.documentale.ejbprotocollo.ObjectFactory();
	}
	
	public static RicercaFascicoli constructRicercaFascicoliInput(String codiceAoo,String nome,String descrizione,
			String responsabile,String stato, String codiceTitolario,Integer numeroFascicolo,String dataAperturaDa,
			String dataAperturaA,String dataChiusuraDa,String dataChiusuraA,String dataScadenzaDa,String dataScadenzaA,
			Integer numProtocollo,Integer annoProtocollo,String tipoRegistro,String uffProprietario,
			Integer dimensionePagina,Integer pagina) throws DatatypeConfigurationException {
		 
		RicercaFascicoli fascicoliWS = ofTypesFasc.createRicercaFascicoli();
		 
		if(codiceAoo!=null && !(codiceAoo.equals(""))) {
			fascicoliWS.setCodiceAoo(codiceAoo);
		} 
		 
		if(nome!=null && !(nome.equals(""))){
			fascicoliWS.setNomeCartella(nome);
		}
		
		if(descrizione!=null && !(descrizione.equals(""))) {
			fascicoliWS.setDescrizione(descrizione);
		} 
		 
		if(responsabile!=null && !(responsabile.equals(""))) {
			fascicoliWS.setResponsabile(responsabile);
		}
		 
		if(stato!=null && !(stato.equals(""))){
			List<String> listaStati = new ArrayList<>();
			listaStati.add(stato);
			fascicoliWS.setListaStati(listaStati);
		}
		
		if(codiceTitolario!=null && !(codiceTitolario.equals(""))) {
			fascicoliWS.setCodiceTitolario(codiceTitolario);
		} 
		
		if(numeroFascicolo!=null && !(numeroFascicolo.equals(""))) {
			fascicoliWS.setNumeroFascicolo(numeroFascicolo);
		} 
		
		if(dataAperturaDa!=null && !(dataAperturaDa.equals(""))) {
			fascicoliWS.setDataAperturaDal(dataAperturaDa);
		} 
		if(dataAperturaA!=null && !(dataAperturaA.equals(""))) {
			fascicoliWS.setDataAperturaAl(dataAperturaA);
		} 
		 
		if(dataChiusuraDa!=null && !(dataChiusuraDa.equals(""))) {
			fascicoliWS.setDataChiusuraDal(dataChiusuraDa);
		} 
		if(dataChiusuraA!=null && !(dataChiusuraA.equals(""))) {
			fascicoliWS.setDataChiusuraAl(dataChiusuraA);
		}  
		
		if(dataScadenzaDa!=null && !(dataScadenzaDa.equals(""))) {
			fascicoliWS.setDataScadenzaDal(dataScadenzaDa);
		}  
		if(dataScadenzaA!=null && !(dataScadenzaA.equals(""))) {
			fascicoliWS.setDataScadenzaAl(dataScadenzaA);
		}
		
		if(numProtocollo!=null) {
			fascicoliWS.setNumeroProtocollo(numProtocollo.longValue());
		}
		
		if(annoProtocollo!=null) {
			fascicoliWS.setAnnoProtocollo(annoProtocollo);
		} 
		
		if(tipoRegistro!=null && !(tipoRegistro.equals(""))) {
			fascicoliWS.setCodiceRegistro(tipoRegistro);
		} 
		
		if(uffProprietario!=null && !(uffProprietario.equals(""))) {
			 fascicoliWS.setUfficioProprietario(uffProprietario);
		}
		
		if(dimensionePagina!=null) {
			fascicoliWS.setDimensionePagina(dimensionePagina);
		}
		 
		if(pagina!=null) {
			fascicoliWS.setPagina(pagina);
		}
	 
		return fascicoliWS;
	}
	
	public static GetFascicolo constructRicercaFascicoliByIdInput(String idRegistro){
		GetFascicolo fascicoliWS = ofTypesFasc.createGetFascicolo();
		 
		//idFascicolo
		if(idRegistro!=null) {
			fascicoliWS.setIChronicleId(idRegistro);
		} 
		return fascicoliWS;
	}
   
	
	public static RicercaRegistroBean constructRicercaProtocolliInput(String oggetto,String idRegistro,String modalita,
			String numProtocolloDa,String numProtocolloA,String annoProtocollazione,String dataProtocollazioneDa, String dataProtocollazioneA,
			String tipologiaDocumento, String mittDest,String nProtMittente,String cFpIvaMittDest,
			String idAoo,String lastRowNum,String maxNumIteratore){
		 
		RicercaRegistroBean registroWS = ofTypesProt.createRicercaRegistroBean();
		  
	
		if(oggetto!=null && (!oggetto.equals(""))) {
			registroWS.setOggetto(oggetto);
		}
		
		if(idRegistro!=null && (!idRegistro.equals(""))) {
			registroWS.setIdRegistro(idRegistro);
		}
		
		if(modalita!=null && (!modalita.equals(""))) {
			registroWS.setModalita(modalita);
		}
		
		if(numProtocolloDa!=null && (!numProtocolloDa.equals(""))) {
			registroWS.setProgressivoDal(numProtocolloDa);
		}
		
		if(numProtocolloA!=null && (!numProtocolloA.equals(""))) {
			registroWS.setProgressivoAl(numProtocolloA);
		}
		
		if(annoProtocollazione!=null && (!annoProtocollazione.equals(""))) {
			registroWS.setAnnoProtocollazione(annoProtocollazione);
		}
		 
		if(dataProtocollazioneDa!=null && (!dataProtocollazioneDa.equals(""))) {
			registroWS.setDataProtocollazioneDa(dataProtocollazioneDa);
		}
		
		if(dataProtocollazioneA!=null && (!dataProtocollazioneA.equals(""))) {
			registroWS.setDataProtocollazioneA(dataProtocollazioneA);
		}
		  
		if(tipologiaDocumento!=null && (!tipologiaDocumento.equals(""))) {
			registroWS.setSelTipologia(tipologiaDocumento);
		} 
		
		if(mittDest!=null && (!mittDest.equals(""))) {
			registroWS.setMittente(mittDest);
		}
		
		if(nProtMittente!=null && (!nProtMittente.equals(""))) {
			registroWS.setNumProtEsterno(nProtMittente);
		}
		
		if(cFpIvaMittDest!=null && (!cFpIvaMittDest.equals(""))) {
			registroWS.setCodFiscaleMittente(cFpIvaMittDest);
		}
		
		if(idAoo!=null && (!idAoo.equals(""))) {
			registroWS.setIdAoo(idAoo);
		}
		
		
		if(lastRowNum!=null && maxNumIteratore!=null) {
			PaginazioneBean pb = new PaginazioneBean();
			pb.setLastRowNum(lastRowNum);
			pb.setMaxNumIteratore(maxNumIteratore);
			pb.setCampoSort(orderForProtocollo);
			registroWS.setPaginazione(pb);
		}		
		 
		return registroWS;
	}
	
	public static RicercaRegistroBean constructRicercaProtocolliInputById (String idRegistro,String codAoo,String idProtocollo,String annoProtocollazione,String modalita) throws DatatypeConfigurationException {
		RicercaRegistroBean registroWS = ofTypesProt.createRicercaRegistroBean();
		
		if(idRegistro!=null && (!idRegistro.equals(""))) {
			registroWS.setIdRegistro(idRegistro);
		}
		
		if(codAoo!=null && (!codAoo.equals(""))) {
			registroWS.setIdAoo(codAoo);
		}
		
		if(idProtocollo!=null && (!idProtocollo.equals(""))) {
			registroWS.setProgressivoDal(idProtocollo);
			registroWS.setProgressivoAl(idProtocollo);
		}
		
		if(annoProtocollazione!=null && (!annoProtocollazione.equals(""))) {
			registroWS.setAnnoProtocollazione(annoProtocollazione);
		}
		
		if(modalita != null && (!modalita.equals(""))) {
			registroWS.setModalita(modalita);
		}
		return registroWS;
	}
}

