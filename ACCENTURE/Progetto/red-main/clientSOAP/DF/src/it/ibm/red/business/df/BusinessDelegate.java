package it.ibm.red.business.df;

import java.net.URL;
import java.util.List;
 

import it.entrate.protocollo.ejb.DocumentiEjb;
import it.entrate.protocollo.ejb.DocumentiEjbService;
import it.entrate.protocollo.ejb.DocumentoBean;
import it.entrate.protocollo.ejb.ReperimentoDocumentoEnum;
import it.ibm.red.business.df.exceptions.BusinessDelegateRuntimeException;
import it.sogei.gestionedocumentale.documentale.ejbprotocollo.AuthBean;
import it.sogei.gestionedocumentale.documentale.ejbprotocollo.ProtocolloEjb;
import it.sogei.gestionedocumentale.documentale.ejbprotocollo.ProtocolloEjbService;
import it.sogei.gestionedocumentale.documentale.ejbprotocollo.RicercaRegistroBean;
import it.sogei.gestionedocumentale.documentale.ejbprotocollo.RisultatiRicercaBean;
import it.sogei.servizi.fascicoli.ws.AuthenticationBean;
import it.sogei.servizi.fascicoli.ws.FascicoliWS;
import it.sogei.servizi.fascicoli.ws.FascicoliWSService;
import it.sogei.servizi.fascicoli.ws.GetFascicolo;
import it.sogei.servizi.fascicoli.ws.RicercaFascicoli;  


public class BusinessDelegate {

	private static FascicoliWS  proxyDFFascicoli;

	private static ProtocolloEjb proxyDFProtocolli; 

	private static DocumentiEjb proxyDFDocumenti; 
	//Handler per intercettare le chiamate effettuate verso NSD
	//	static class NSDWSClientHandler implements SOAPHandler<SOAPMessageContext> {
	//
	//	//	private REDLogger logger = REDLogger.getLogger(NSDWSClientHandler.class);
	//		private PrintStream out = System.out;
	//
	//		    public Set<QName> getHeaders() {
	//		        return null;
	//		    }
	//
	//		    public boolean handleMessage(SOAPMessageContext smc) {
	//		        logToSystemOut(smc);
	//		        return true;
	//		    }
	//
	//		    public boolean handleFault(SOAPMessageContext smc) {
	//		        logToSystemOut(smc);
	//		        return true;
	//		    }
	//
	//		    // nothing to clean up
	//		    public void close(MessageContext messageContext) {
	//		    }
	//
	//		    /*
	//		     * Check the MESSAGE_OUTBOUND_PROPERTY in the context
	//		     * to see if this is an outgoing or incoming message.
	//		     * Write a brief message to the print stream and
	//		     * output the message. The writeTo() method can throw
	//		     * SOAPException or IOException
	//		     */
	//		    private void logToSystemOut(SOAPMessageContext smc) {
	//		        Boolean outboundProperty = (Boolean)
	//		            smc.get (MessageContext.MESSAGE_OUTBOUND_PROPERTY);
	//
	//		        if (outboundProperty.booleanValue()) {
	//		            out.println("\nOutbound message:");
	//		        } else {
	//		            out.println("\nInbound message:");
	//		        }
	//
	//		        SOAPMessage message = smc.getMessage();
	//		        try {
	//		            message.writeTo(out);
	//		            out.println("");   // just to add a newline
	//		        } catch (Exception e) {
	//		            out.println("Exception in handler: " + e);
	//		        }
	//		    }
	//
	//	}



	public static void init(String endPointProtocollo, String endPointFascicolo, String endPointDocumento) throws Exception {
		try { 	
			//			ProtocolloEjbService protocolliWsService = new ProtocolloEjbService(new URL(endPointProtocollo));
			//			proxyDFProtocolli = protocolliWsService.getProtocolloEjbPort(); 
			//			BindingProvider bpProtocollo = (BindingProvider) proxyDFProtocolli;
			//			bpProtocollo.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPointProtocollo);

			//Serve per intercettare le chiamate SOAP effettuate verso NPS
			//			BindingProvider bp = (BindingProvider) proxyDFProtocolli;
			//			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPointProtocollo);
			//			List<Handler> handlerList = bp.getBinding().getHandlerChain();
			//		    handlerList.add(new NSDWSClientHandler());
			//		    bp.getBinding().setHandlerChain(handlerList);

			//Interfaccia Protocolli Nsd
			ProtocolloEjbService protocolliWsService = new ProtocolloEjbService(new URL(endPointProtocollo));
			proxyDFProtocolli = protocolliWsService.getProtocolloEjbPort(); 
			//Interfaccia Fascicoli Nsd
			FascicoliWSService fascicoliWsService = new FascicoliWSService(new URL(endPointFascicolo));
			proxyDFFascicoli = fascicoliWsService.getFascicoliWSPort();
			//Interfaccia Documenti Nsd
			DocumentiEjbService documentiWsService = new DocumentiEjbService(new URL(endPointDocumento));
			proxyDFDocumenti = documentiWsService.getDocumentiEjbPort();
		} catch (Exception e) {
			throw new BusinessDelegateRuntimeException("Errore durante la creazione degli stub per la chiamata ai servizi di NSD", e);
		}

	}


	public static class Fascicolo {

		public static List<it.sogei.servizi.fascicoli.ws.Fascicolo> ricercaFascicoli(String codFiscale,String nomeEnte,String idFlusso,
				String codiceAoo,String nome,String descrizione,
				String responsabile,String stato, String codiceTitolario,Integer numeroFascicolo,String dataAperturaDa,
				String dataAperturaA,String dataChiusuraDa,String dataChiusuraA,String dataScadenzaDa,String dataScadenzaA,
				Integer numProtocollo,Integer annoProtocollo,String tipoRegistro,String uffProprietario,
				Integer dimensionePagina,Integer pagina) {
			try {
				AuthenticationBean authBean = setAuthBeanFasc(codFiscale,nomeEnte,idFlusso);

				RicercaFascicoli input = Director.constructRicercaFascicoliInput(codiceAoo,nome,descrizione,
						responsabile,stato, codiceTitolario,numeroFascicolo,dataAperturaDa,
						dataAperturaA,dataChiusuraDa, dataChiusuraA,dataScadenzaDa,dataScadenzaA,
						numProtocollo,annoProtocollo,tipoRegistro,uffProprietario,
						dimensionePagina,pagina); 

				return BusinessDelegate.proxyDFFascicoli.traSA0042FU006RicercaFascicoli(authBean, input);

			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di DF", e);
			}
		}

		public static int ricercaFascicoliCount(String codFiscale,String nomeEnte,String idFlusso,
				String codiceAoo,String nome,String descrizione,
				String responsabile,String stato, String codiceTitolario,Integer numeroFascicolo,String dataAperturaDa,
				String dataAperturaA,String dataChiusuraDa,String dataChiusuraA,String dataScadenzaDa,String dataScadenzaA,
				Integer numProtocollo,Integer annoProtocollo,String tipoRegistro,String uffProprietario,
				Integer dimensionePagina,Integer pagina) {

			try {
				AuthenticationBean authBean = setAuthBeanFasc(codFiscale, nomeEnte, idFlusso);

				RicercaFascicoli input = Director.constructRicercaFascicoliInput(codiceAoo,nome,descrizione,
						responsabile,stato, codiceTitolario,numeroFascicolo,dataAperturaDa,
						dataAperturaA,dataChiusuraDa,dataChiusuraA,dataScadenzaDa,dataScadenzaA,
						numProtocollo,annoProtocollo,tipoRegistro,uffProprietario,
						dimensionePagina,pagina);

				return BusinessDelegate.proxyDFFascicoli.traSA0042FU006CountRicercaFascicoli(authBean, input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di DF", e);
			}

		}



		public static it.sogei.servizi.fascicoli.ws.Fascicolo getFascicoloById(String codFiscale,String nomeEnte,String idFlusso,String idFascicolo) {
			try {
				AuthenticationBean authBean = setAuthBeanFasc(codFiscale,nomeEnte,idFlusso);

				GetFascicolo input = Director.constructRicercaFascicoliByIdInput(idFascicolo);
				return BusinessDelegate.proxyDFFascicoli.traSA0042FU006GetFascicoloById(authBean, input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di DF", e);
			}
		} 

		private static AuthenticationBean setAuthBeanFasc(String codFiscale, String nomeEnte, String idFlusso) {
			AuthenticationBean authBean = new AuthenticationBean();
			if(codFiscale!=null) {
				authBean.setUtente(codFiscale);
			}
			if(nomeEnte!=null) {
				authBean.setNomeEnte(nomeEnte);
			}
			if(idFlusso!=null) {
				authBean.setIdFlusso(idFlusso);
			}
			return authBean;
		}

	}


	public static class Protocollo{
		public static List<RisultatiRicercaBean> ricercaProtocolli(String codFiscale,String nomeEnte,String idFlusso,
				String oggetto,String idRegistro,String modalita,String numProtocolloDa,String numProtocolloA,
				String annoProtocollazione,String dataProtocollazioneDa, String dataProtocollazioneA,String tipologiaDocumento, 
				String mittDest,String nProtMittente,String cFpIvaMittDest,String idAoo,String lastRowNum,String maxNumIteratore) {
			try {
				//Setto Auth Bean per la ricerca
				AuthBean authBean = setAuthBeanProt(codFiscale,nomeEnte,idFlusso);

				RicercaRegistroBean input = Director.constructRicercaProtocolliInput(oggetto,idRegistro,modalita,
						numProtocolloDa,numProtocolloA,annoProtocollazione,dataProtocollazioneDa,dataProtocollazioneA,
						tipologiaDocumento,mittDest,nProtMittente,cFpIvaMittDest,idAoo,lastRowNum,maxNumIteratore);

				return BusinessDelegate.proxyDFProtocolli.traSA0042FU001RicercaProtocolloConParametri(input, authBean);

			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di DF", e);
			}
		}

		//Metodo count per la paginazione
		public static int ricercaProtocolliCount(String codFiscale,String nomeEnte,String idFlusso,
				String oggetto,String idRegistro,String modalita,String numProtocolloDa,String numProtocolloA,
				String annoProtocollazione,String dataProtocollazioneDa, String dataProtocollazioneA,String tipologiaDocumento, 
				String mittDest,String nProtMittente,String cFpIvaMittDest,String idAoo,String lastRowNum,String maxNumIteratore) {
			try {
				AuthBean authBean = setAuthBeanProt(codFiscale,nomeEnte,idFlusso);

				RicercaRegistroBean input = Director.constructRicercaProtocolliInput(oggetto,idRegistro,modalita,numProtocolloDa,numProtocolloA,
						annoProtocollazione,dataProtocollazioneDa,dataProtocollazioneA,tipologiaDocumento,
						mittDest,nProtMittente,cFpIvaMittDest,idAoo,lastRowNum,maxNumIteratore); 

				return BusinessDelegate.proxyDFProtocolli.traSA0042FU001CountRicercaProtocolloConParametri(input, authBean);

			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di DF", e);
			}
		}

		public static List<RisultatiRicercaBean> ricercaProtocolliById(String codFiscale,String nomeEnte,String idFlusso,
				String idRegistro,String codAoo,String idProtocollo,String annoProtocollazione,String modalita) {
			try {
				//Setto Auth Bean per la ricerca
				AuthBean authBean = setAuthBeanProt(codFiscale, nomeEnte, idFlusso);
				RicercaRegistroBean input = Director.constructRicercaProtocolliInputById(idRegistro,codAoo,idProtocollo,annoProtocollazione,modalita);
				return BusinessDelegate.proxyDFProtocolli.traSA0042FU001RicercaProtocolloConParametri(input,authBean);

			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di DF", e);
			}
		} 


		public static DocumentoBean getDocumentoConContent(String codFiscale,String nomeEnte,String idFlusso,String idDocumento) { 
			try {
				it.entrate.protocollo.ejb.AuthBean authBeanDoc = setAuthBeanDoc(codFiscale,nomeEnte,idFlusso);
				return BusinessDelegate.proxyDFDocumenti.getDocumento(idDocumento, ReperimentoDocumentoEnum.CON_CONTENUTO, null, authBeanDoc);
			} catch (Exception e) {
				if(e.getMessage().equalsIgnoreCase("Errore nell'accesso al Documentale")) {
					throw new BusinessDelegateRuntimeException("Errore nell'accesso al Documentale", e);
				}
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di DF", e);
			}  
		}

		private static AuthBean setAuthBeanProt(String codFiscale,String nomeEnte,String idFlusso) {
			AuthBean authBean = new AuthBean(); 
			if(codFiscale!=null) {
				authBean.setUtente(codFiscale);
			}

			if(nomeEnte != null) {
				authBean.setNomeEnte(nomeEnte); 
			}

			if(idFlusso != null) {
				authBean.setIdFlusso(idFlusso);
			}
			return authBean;
		}


		private static it.entrate.protocollo.ejb.AuthBean setAuthBeanDoc(String codFiscale,String nomeEnte,String idFlusso){
			it.entrate.protocollo.ejb.AuthBean authBean = new it.entrate.protocollo.ejb.AuthBean(); 
			if(codFiscale!=null) {
				authBean.setUtente(codFiscale);
			}

			if(nomeEnte != null) {
				authBean.setNomeEnte(nomeEnte); 
			}

			if(idFlusso != null) {
				authBean.setIdFlusso(idFlusso);
			}
			return authBean;
		}


	}
}
