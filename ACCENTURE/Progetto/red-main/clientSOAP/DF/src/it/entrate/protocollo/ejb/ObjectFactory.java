
package it.entrate.protocollo.ejb;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the it.entrate.protocollo.ejb package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RecuperaVersioniDocumentoResponse_QNAME = new QName("http://ejb.protocollo.entrate.it/", "recuperaVersioniDocumentoResponse");
    private final static QName _BloccaSbloccaDocumento_QNAME = new QName("http://ejb.protocollo.entrate.it/", "bloccaSbloccaDocumento");
    private final static QName _EstraiDocumento_QNAME = new QName("http://ejb.protocollo.entrate.it/", "estraiDocumento");
    private final static QName _RicercaDocumentiResponse_QNAME = new QName("http://ejb.protocollo.entrate.it/", "ricercaDocumentiResponse");
    private final static QName _UpdateMetadatiDocumentoResponse_QNAME = new QName("http://ejb.protocollo.entrate.it/", "updateMetadatiDocumentoResponse");
    private final static QName _ArchivaDocumentoResponse_QNAME = new QName("http://ejb.protocollo.entrate.it/", "archivaDocumentoResponse");
    private final static QName _RicercaDocumenti_QNAME = new QName("http://ejb.protocollo.entrate.it/", "ricercaDocumenti");
    private final static QName _UpdateDocumentoResponse_QNAME = new QName("http://ejb.protocollo.entrate.it/", "updateDocumentoResponse");
    private final static QName _RicercaVuotaException_QNAME = new QName("http://ejb.protocollo.entrate.it/", "RicercaVuotaException");
    private final static QName _EstraiDocumentoResponse_QNAME = new QName("http://ejb.protocollo.entrate.it/", "estraiDocumentoResponse");
    private final static QName _ServiceException_QNAME = new QName("http://ejb.protocollo.entrate.it/", "ServiceException");
    private final static QName _GetDocumento_QNAME = new QName("http://ejb.protocollo.entrate.it/", "getDocumento");
    private final static QName _UpdateDocumento_QNAME = new QName("http://ejb.protocollo.entrate.it/", "updateDocumento");
    private final static QName _ArchivaDocumento_QNAME = new QName("http://ejb.protocollo.entrate.it/", "archivaDocumento");
    private final static QName _GetDocumentoResponse_QNAME = new QName("http://ejb.protocollo.entrate.it/", "getDocumentoResponse");
    private final static QName _BloccaSbloccaDocumentoResponse_QNAME = new QName("http://ejb.protocollo.entrate.it/", "bloccaSbloccaDocumentoResponse");
    private final static QName _MetadatoBean_QNAME = new QName("http://ejb.protocollo.entrate.it/", "metadatoBean");
    private final static QName _AnnullaEstrazioneDocumentoResponse_QNAME = new QName("http://ejb.protocollo.entrate.it/", "annullaEstrazioneDocumentoResponse");
    private final static QName _UpdateMetadatiDocumento_QNAME = new QName("http://ejb.protocollo.entrate.it/", "updateMetadatiDocumento");
    private final static QName _AnnullaEstrazioneDocumento_QNAME = new QName("http://ejb.protocollo.entrate.it/", "annullaEstrazioneDocumento");
    private final static QName _LoginException_QNAME = new QName("http://ejb.protocollo.entrate.it/", "LoginException");
    private final static QName _RecuperaVersioniDocumento_QNAME = new QName("http://ejb.protocollo.entrate.it/", "recuperaVersioniDocumento");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: it.entrate.protocollo.ejb
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link UpdateMetadatiDocumentoResponse }
     * 
     */
    public UpdateMetadatiDocumentoResponse createUpdateMetadatiDocumentoResponse() {
        return new UpdateMetadatiDocumentoResponse();
    }

    /**
     * Create an instance of {@link RicercaDocumentiResponse }
     * 
     */
    public RicercaDocumentiResponse createRicercaDocumentiResponse() {
        return new RicercaDocumentiResponse();
    }

    /**
     * Create an instance of {@link BloccaSbloccaDocumento }
     * 
     */
    public BloccaSbloccaDocumento createBloccaSbloccaDocumento() {
        return new BloccaSbloccaDocumento();
    }

    /**
     * Create an instance of {@link EstraiDocumento }
     * 
     */
    public EstraiDocumento createEstraiDocumento() {
        return new EstraiDocumento();
    }

    /**
     * Create an instance of {@link RecuperaVersioniDocumentoResponse }
     * 
     */
    public RecuperaVersioniDocumentoResponse createRecuperaVersioniDocumentoResponse() {
        return new RecuperaVersioniDocumentoResponse();
    }

    /**
     * Create an instance of {@link EstraiDocumentoResponse }
     * 
     */
    public EstraiDocumentoResponse createEstraiDocumentoResponse() {
        return new EstraiDocumentoResponse();
    }

    /**
     * Create an instance of {@link RicercaVuotaException }
     * 
     */
    public RicercaVuotaException createRicercaVuotaException() {
        return new RicercaVuotaException();
    }

    /**
     * Create an instance of {@link UpdateDocumentoResponse }
     * 
     */
    public UpdateDocumentoResponse createUpdateDocumentoResponse() {
        return new UpdateDocumentoResponse();
    }

    /**
     * Create an instance of {@link RicercaDocumenti }
     * 
     */
    public RicercaDocumenti createRicercaDocumenti() {
        return new RicercaDocumenti();
    }

    /**
     * Create an instance of {@link ArchivaDocumentoResponse }
     * 
     */
    public ArchivaDocumentoResponse createArchivaDocumentoResponse() {
        return new ArchivaDocumentoResponse();
    }

    /**
     * Create an instance of {@link GetDocumentoResponse }
     * 
     */
    public GetDocumentoResponse createGetDocumentoResponse() {
        return new GetDocumentoResponse();
    }

    /**
     * Create an instance of {@link ArchivaDocumento }
     * 
     */
    public ArchivaDocumento createArchivaDocumento() {
        return new ArchivaDocumento();
    }

    /**
     * Create an instance of {@link UpdateDocumento }
     * 
     */
    public UpdateDocumento createUpdateDocumento() {
        return new UpdateDocumento();
    }

    /**
     * Create an instance of {@link ServiceException }
     * 
     */
    public ServiceException createServiceException() {
        return new ServiceException();
    }

    /**
     * Create an instance of {@link GetDocumento }
     * 
     */
    public GetDocumento createGetDocumento() {
        return new GetDocumento();
    }

    /**
     * Create an instance of {@link LoginException }
     * 
     */
    public LoginException createLoginException() {
        return new LoginException();
    }

    /**
     * Create an instance of {@link RecuperaVersioniDocumento }
     * 
     */
    public RecuperaVersioniDocumento createRecuperaVersioniDocumento() {
        return new RecuperaVersioniDocumento();
    }

    /**
     * Create an instance of {@link AnnullaEstrazioneDocumento }
     * 
     */
    public AnnullaEstrazioneDocumento createAnnullaEstrazioneDocumento() {
        return new AnnullaEstrazioneDocumento();
    }

    /**
     * Create an instance of {@link AnnullaEstrazioneDocumentoResponse }
     * 
     */
    public AnnullaEstrazioneDocumentoResponse createAnnullaEstrazioneDocumentoResponse() {
        return new AnnullaEstrazioneDocumentoResponse();
    }

    /**
     * Create an instance of {@link UpdateMetadatiDocumento }
     * 
     */
    public UpdateMetadatiDocumento createUpdateMetadatiDocumento() {
        return new UpdateMetadatiDocumento();
    }

    /**
     * Create an instance of {@link BloccaSbloccaDocumentoResponse }
     * 
     */
    public BloccaSbloccaDocumentoResponse createBloccaSbloccaDocumentoResponse() {
        return new BloccaSbloccaDocumentoResponse();
    }

    /**
     * Create an instance of {@link MetadatoBean }
     * 
     */
    public MetadatoBean createMetadatoBean() {
        return new MetadatoBean();
    }

    /**
     * Create an instance of {@link DocumentoBean }
     * 
     */
    public DocumentoBean createDocumentoBean() {
        return new DocumentoBean();
    }

    /**
     * Create an instance of {@link VersioniDocumentoBean }
     * 
     */
    public VersioniDocumentoBean createVersioniDocumentoBean() {
        return new VersioniDocumentoBean();
    }

    /**
     * Create an instance of {@link AuthBean }
     * 
     */
    public AuthBean createAuthBean() {
        return new AuthBean();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecuperaVersioniDocumentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.protocollo.entrate.it/", name = "recuperaVersioniDocumentoResponse")
    public JAXBElement<RecuperaVersioniDocumentoResponse> createRecuperaVersioniDocumentoResponse(RecuperaVersioniDocumentoResponse value) {
        return new JAXBElement<RecuperaVersioniDocumentoResponse>(_RecuperaVersioniDocumentoResponse_QNAME, RecuperaVersioniDocumentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BloccaSbloccaDocumento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.protocollo.entrate.it/", name = "bloccaSbloccaDocumento")
    public JAXBElement<BloccaSbloccaDocumento> createBloccaSbloccaDocumento(BloccaSbloccaDocumento value) {
        return new JAXBElement<BloccaSbloccaDocumento>(_BloccaSbloccaDocumento_QNAME, BloccaSbloccaDocumento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EstraiDocumento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.protocollo.entrate.it/", name = "estraiDocumento")
    public JAXBElement<EstraiDocumento> createEstraiDocumento(EstraiDocumento value) {
        return new JAXBElement<EstraiDocumento>(_EstraiDocumento_QNAME, EstraiDocumento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RicercaDocumentiResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.protocollo.entrate.it/", name = "ricercaDocumentiResponse")
    public JAXBElement<RicercaDocumentiResponse> createRicercaDocumentiResponse(RicercaDocumentiResponse value) {
        return new JAXBElement<RicercaDocumentiResponse>(_RicercaDocumentiResponse_QNAME, RicercaDocumentiResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateMetadatiDocumentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.protocollo.entrate.it/", name = "updateMetadatiDocumentoResponse")
    public JAXBElement<UpdateMetadatiDocumentoResponse> createUpdateMetadatiDocumentoResponse(UpdateMetadatiDocumentoResponse value) {
        return new JAXBElement<UpdateMetadatiDocumentoResponse>(_UpdateMetadatiDocumentoResponse_QNAME, UpdateMetadatiDocumentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArchivaDocumentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.protocollo.entrate.it/", name = "archivaDocumentoResponse")
    public JAXBElement<ArchivaDocumentoResponse> createArchivaDocumentoResponse(ArchivaDocumentoResponse value) {
        return new JAXBElement<ArchivaDocumentoResponse>(_ArchivaDocumentoResponse_QNAME, ArchivaDocumentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RicercaDocumenti }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.protocollo.entrate.it/", name = "ricercaDocumenti")
    public JAXBElement<RicercaDocumenti> createRicercaDocumenti(RicercaDocumenti value) {
        return new JAXBElement<RicercaDocumenti>(_RicercaDocumenti_QNAME, RicercaDocumenti.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateDocumentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.protocollo.entrate.it/", name = "updateDocumentoResponse")
    public JAXBElement<UpdateDocumentoResponse> createUpdateDocumentoResponse(UpdateDocumentoResponse value) {
        return new JAXBElement<UpdateDocumentoResponse>(_UpdateDocumentoResponse_QNAME, UpdateDocumentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RicercaVuotaException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.protocollo.entrate.it/", name = "RicercaVuotaException")
    public JAXBElement<RicercaVuotaException> createRicercaVuotaException(RicercaVuotaException value) {
        return new JAXBElement<RicercaVuotaException>(_RicercaVuotaException_QNAME, RicercaVuotaException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EstraiDocumentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.protocollo.entrate.it/", name = "estraiDocumentoResponse")
    public JAXBElement<EstraiDocumentoResponse> createEstraiDocumentoResponse(EstraiDocumentoResponse value) {
        return new JAXBElement<EstraiDocumentoResponse>(_EstraiDocumentoResponse_QNAME, EstraiDocumentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.protocollo.entrate.it/", name = "ServiceException")
    public JAXBElement<ServiceException> createServiceException(ServiceException value) {
        return new JAXBElement<ServiceException>(_ServiceException_QNAME, ServiceException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDocumento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.protocollo.entrate.it/", name = "getDocumento")
    public JAXBElement<GetDocumento> createGetDocumento(GetDocumento value) {
        return new JAXBElement<GetDocumento>(_GetDocumento_QNAME, GetDocumento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateDocumento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.protocollo.entrate.it/", name = "updateDocumento")
    public JAXBElement<UpdateDocumento> createUpdateDocumento(UpdateDocumento value) {
        return new JAXBElement<UpdateDocumento>(_UpdateDocumento_QNAME, UpdateDocumento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArchivaDocumento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.protocollo.entrate.it/", name = "archivaDocumento")
    public JAXBElement<ArchivaDocumento> createArchivaDocumento(ArchivaDocumento value) {
        return new JAXBElement<ArchivaDocumento>(_ArchivaDocumento_QNAME, ArchivaDocumento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDocumentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.protocollo.entrate.it/", name = "getDocumentoResponse")
    public JAXBElement<GetDocumentoResponse> createGetDocumentoResponse(GetDocumentoResponse value) {
        return new JAXBElement<GetDocumentoResponse>(_GetDocumentoResponse_QNAME, GetDocumentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BloccaSbloccaDocumentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.protocollo.entrate.it/", name = "bloccaSbloccaDocumentoResponse")
    public JAXBElement<BloccaSbloccaDocumentoResponse> createBloccaSbloccaDocumentoResponse(BloccaSbloccaDocumentoResponse value) {
        return new JAXBElement<BloccaSbloccaDocumentoResponse>(_BloccaSbloccaDocumentoResponse_QNAME, BloccaSbloccaDocumentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatoBean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.protocollo.entrate.it/", name = "metadatoBean")
    public JAXBElement<MetadatoBean> createMetadatoBean(MetadatoBean value) {
        return new JAXBElement<MetadatoBean>(_MetadatoBean_QNAME, MetadatoBean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnnullaEstrazioneDocumentoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.protocollo.entrate.it/", name = "annullaEstrazioneDocumentoResponse")
    public JAXBElement<AnnullaEstrazioneDocumentoResponse> createAnnullaEstrazioneDocumentoResponse(AnnullaEstrazioneDocumentoResponse value) {
        return new JAXBElement<AnnullaEstrazioneDocumentoResponse>(_AnnullaEstrazioneDocumentoResponse_QNAME, AnnullaEstrazioneDocumentoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateMetadatiDocumento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.protocollo.entrate.it/", name = "updateMetadatiDocumento")
    public JAXBElement<UpdateMetadatiDocumento> createUpdateMetadatiDocumento(UpdateMetadatiDocumento value) {
        return new JAXBElement<UpdateMetadatiDocumento>(_UpdateMetadatiDocumento_QNAME, UpdateMetadatiDocumento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnnullaEstrazioneDocumento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.protocollo.entrate.it/", name = "annullaEstrazioneDocumento")
    public JAXBElement<AnnullaEstrazioneDocumento> createAnnullaEstrazioneDocumento(AnnullaEstrazioneDocumento value) {
        return new JAXBElement<AnnullaEstrazioneDocumento>(_AnnullaEstrazioneDocumento_QNAME, AnnullaEstrazioneDocumento.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoginException }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.protocollo.entrate.it/", name = "LoginException")
    public JAXBElement<LoginException> createLoginException(LoginException value) {
        return new JAXBElement<LoginException>(_LoginException_QNAME, LoginException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RecuperaVersioniDocumento }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ejb.protocollo.entrate.it/", name = "recuperaVersioniDocumento")
    public JAXBElement<RecuperaVersioniDocumento> createRecuperaVersioniDocumento(RecuperaVersioniDocumento value) {
        return new JAXBElement<RecuperaVersioniDocumento>(_RecuperaVersioniDocumento_QNAME, RecuperaVersioniDocumento.class, null, value);
    }

}
