
package it.entrate.protocollo.ejb;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for reperimentoDocumentoEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="reperimentoDocumentoEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SOLO_INFO"/>
 *     &lt;enumeration value="CON_CONTENUTO"/>
 *     &lt;enumeration value="SU_AREA_DI_SCAMBIO"/>
 *     &lt;enumeration value="TMP_SU_DISCO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "reperimentoDocumentoEnum")
@XmlEnum
public enum ReperimentoDocumentoEnum {

    SOLO_INFO,
    CON_CONTENUTO,
    SU_AREA_DI_SCAMBIO,
    TMP_SU_DISCO;

    public String value() {
        return name();
    }

    public static ReperimentoDocumentoEnum fromValue(String v) {
        return valueOf(v);
    }

}
