
package fepa.services.v3;

import javax.xml.ws.WebFault;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebFault(name = "GenericFault", targetNamespace = "urn:fepa:header:v3")
public class GenericFault
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private fepa.header.v3.GenericFault faultInfo;

    /**
     * 
     * @param faultInfo
     * @param message
     */
    public GenericFault(String message, fepa.header.v3.GenericFault faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param faultInfo
     * @param cause
     * @param message
     */
    public GenericFault(String message, fepa.header.v3.GenericFault faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: fepa.header.v3.GenericFault
     */
    public fepa.header.v3.GenericFault getFaultInfo() {
        return faultInfo;
    }

}
