
package fepa.services.v3;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import fepa.header.v3.AccessoApplicativo;
import fepa.types.v3.RichiestaConvertDocumentoType;
import fepa.types.v3.RichiestaDownloadFileContentType;
import fepa.types.v3.RichiestaUploadFileContentType;
import fepa.types.v3.RispostaConvertDocumentoType;
import fepa.types.v3.RispostaDownloadFileContentType;
import fepa.types.v3.RispostaUploadFileContentType;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "InterfacciaOperazioniDocumentali", targetNamespace = "urn:fepa:services:v3")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    fepa.types.v3.ObjectFactory.class,
    fepa.header.v3.ObjectFactory.class,
    fepa.messages.v3.ObjectFactory.class,
    org.xmlsoap.schemas.soap.envelope.ObjectFactory.class
})
public interface InterfacciaOperazioniDocumentali {


    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns fepa.types.v3.RispostaConvertDocumentoType
     * @throws GenericFault
     * @throws SecurityFault
     */
    @WebMethod(action = "urn:convertDocumento")
    @WebResult(name = "risposta_convertDocumento", targetNamespace = "urn:fepa:messages:v3", partName = "parameters")
    public RispostaConvertDocumentoType convertDocumento(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "urn:fepa:header:v3", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_convertDocumento", targetNamespace = "urn:fepa:messages:v3", partName = "parameters")
        RichiestaConvertDocumentoType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns fepa.types.v3.RispostaUploadFileContentType
     * @throws GenericFault
     * @throws SecurityFault
     */
    @WebMethod(action = "urn:uploadFileContent")
    @WebResult(name = "risposta_uploadFileContent", targetNamespace = "urn:fepa:messages:v3", partName = "parameters")
    public RispostaUploadFileContentType uploadFileContent(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "urn:fepa:header:v3", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_uploadFileContent", targetNamespace = "urn:fepa:messages:v3", partName = "parameters")
        RichiestaUploadFileContentType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns fepa.types.v3.RispostaDownloadFileContentType
     * @throws GenericFault
     * @throws SecurityFault
     */
    @WebMethod(action = "urn:downloadFileContent")
    @WebResult(name = "risposta_downloadFileContent", targetNamespace = "urn:fepa:messages:v3", partName = "parameters")
    public RispostaDownloadFileContentType downloadFileContent(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "urn:fepa:header:v3", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_downloadFileContent", targetNamespace = "urn:fepa:messages:v3", partName = "parameters")
        RichiestaDownloadFileContentType parameters)
        throws GenericFault, SecurityFault
    ;

}
