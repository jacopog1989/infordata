
package fepa.types.v3;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per tipoDettaglioEstrazioneFascicolo_type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoDettaglioEstrazioneFascicolo_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DATI_MINIMI"/>
 *     &lt;enumeration value="METADATI_FASCICOLO"/>
 *     &lt;enumeration value="DOCUMENTI"/>
 *     &lt;enumeration value="DOCUMENTI_PREVIEW"/>
 *     &lt;enumeration value="DOCUMENTI_OPERAZIONI"/>
 *     &lt;enumeration value="ELENCO_FASCICOLICONTENUTI"/>
 *     &lt;enumeration value="ELENCO_FASCICOLICONTENUTI_METADATI"/>
 *     &lt;enumeration value="ASSOCIAZIONI"/>
 *     &lt;enumeration value="METADATI_DOCUMENTO"/>
 *     &lt;enumeration value="PREVIEW_DOCUMENTO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoDettaglioEstrazioneFascicolo_type")
@XmlEnum
public enum TipoDettaglioEstrazioneFascicoloType {

    DATI_MINIMI,
    METADATI_FASCICOLO,
    DOCUMENTI,
    DOCUMENTI_PREVIEW,
    DOCUMENTI_OPERAZIONI,
    ELENCO_FASCICOLICONTENUTI,
    ELENCO_FASCICOLICONTENUTI_METADATI,
    ASSOCIAZIONI,
    METADATI_DOCUMENTO,
    PREVIEW_DOCUMENTO;

    public String value() {
        return name();
    }

    public static TipoDettaglioEstrazioneFascicoloType fromValue(String v) {
        return valueOf(v);
    }

}
