
package fepa.types.v3;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;


/**
 * Modifica dei metadati del fascicolo, e modifica del trattamento dei documenti per i sotto fascicoli
 * 
 * <p>Classe Java per richiesta_updateFascicolo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_updateFascicolo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicolo" type="{urn:fepa:types:v3}Guid"/>
 *         &lt;element name="TipoFascicolo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FascicoloMetadata" type="{urn:fepa:types:v3}fascicoloMetadata_type" minOccurs="0"/>
 *         &lt;element name="DaInviare" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Condivisibile" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Attivo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="TrattamentoDocumentiFascicoliContenuti" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence maxOccurs="unbounded">
 *                   &lt;element name="IdFascicoloContenuto" type="{urn:fepa:types:v3}Guid"/>
 *                   &lt;element name="TipoFascicolo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="DaInviare" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                   &lt;element name="Condivisibile" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                   &lt;element name="Attivo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                   &lt;element name="DocumentiFascicolo" type="{urn:fepa:types:v3}Guid" maxOccurs="unbounded"/>
 *                   &lt;element name="TipoTrattamento">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="INCLUDI"/>
 *                         &lt;enumeration value="ESCLUDI"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_updateFascicolo_type", propOrder = {
    "idFascicolo",
    "tipoFascicolo",
    "fascicoloMetadata",
    "daInviare",
    "condivisibile",
    "attivo",
    "trattamentoDocumentiFascicoliContenuti"
})
public class RichiestaUpdateFascicoloType
    implements Serializable
{

    @XmlElement(name = "IdFascicolo", required = true)
    protected String idFascicolo;
    @XmlElement(name = "TipoFascicolo", required = true)
    protected String tipoFascicolo;
    @XmlElement(name = "FascicoloMetadata")
    protected FascicoloMetadataType fascicoloMetadata;
    @XmlElement(name = "DaInviare", defaultValue = "true")
    protected Boolean daInviare;
    @XmlElement(name = "Condivisibile", defaultValue = "true")
    protected Boolean condivisibile;
    @XmlElement(name = "Attivo", defaultValue = "true")
    protected Boolean attivo;
    @XmlElement(name = "TrattamentoDocumentiFascicoliContenuti")
    protected RichiestaUpdateFascicoloType.TrattamentoDocumentiFascicoliContenuti trattamentoDocumentiFascicoliContenuti;

    /**
     * Recupera il valore della proprietà idFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicolo() {
        return idFascicolo;
    }

    /**
     * Imposta il valore della proprietà idFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicolo(String value) {
        this.idFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà tipoFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoFascicolo() {
        return tipoFascicolo;
    }

    /**
     * Imposta il valore della proprietà tipoFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoFascicolo(String value) {
        this.tipoFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà fascicoloMetadata.
     * 
     * @return
     *     possible object is
     *     {@link FascicoloMetadataType }
     *     
     */
    public FascicoloMetadataType getFascicoloMetadata() {
        return fascicoloMetadata;
    }

    /**
     * Imposta il valore della proprietà fascicoloMetadata.
     * 
     * @param value
     *     allowed object is
     *     {@link FascicoloMetadataType }
     *     
     */
    public void setFascicoloMetadata(FascicoloMetadataType value) {
        this.fascicoloMetadata = value;
    }

    /**
     * Recupera il valore della proprietà daInviare.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDaInviare() {
        return daInviare;
    }

    /**
     * Imposta il valore della proprietà daInviare.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDaInviare(Boolean value) {
        this.daInviare = value;
    }

    /**
     * Recupera il valore della proprietà condivisibile.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCondivisibile() {
        return condivisibile;
    }

    /**
     * Imposta il valore della proprietà condivisibile.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCondivisibile(Boolean value) {
        this.condivisibile = value;
    }

    /**
     * Recupera il valore della proprietà attivo.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAttivo() {
        return attivo;
    }

    /**
     * Imposta il valore della proprietà attivo.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAttivo(Boolean value) {
        this.attivo = value;
    }

    /**
     * Recupera il valore della proprietà trattamentoDocumentiFascicoliContenuti.
     * 
     * @return
     *     possible object is
     *     {@link RichiestaUpdateFascicoloType.TrattamentoDocumentiFascicoliContenuti }
     *     
     */
    public RichiestaUpdateFascicoloType.TrattamentoDocumentiFascicoliContenuti getTrattamentoDocumentiFascicoliContenuti() {
        return trattamentoDocumentiFascicoliContenuti;
    }

    /**
     * Imposta il valore della proprietà trattamentoDocumentiFascicoliContenuti.
     * 
     * @param value
     *     allowed object is
     *     {@link RichiestaUpdateFascicoloType.TrattamentoDocumentiFascicoliContenuti }
     *     
     */
    public void setTrattamentoDocumentiFascicoliContenuti(RichiestaUpdateFascicoloType.TrattamentoDocumentiFascicoliContenuti value) {
        this.trattamentoDocumentiFascicoliContenuti = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence maxOccurs="unbounded">
     *         &lt;element name="IdFascicoloContenuto" type="{urn:fepa:types:v3}Guid"/>
     *         &lt;element name="TipoFascicolo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="DaInviare" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *         &lt;element name="Condivisibile" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *         &lt;element name="Attivo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *         &lt;element name="DocumentiFascicolo" type="{urn:fepa:types:v3}Guid" maxOccurs="unbounded"/>
     *         &lt;element name="TipoTrattamento">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="INCLUDI"/>
     *               &lt;enumeration value="ESCLUDI"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idFascicoloContenutoAndTipoFascicoloAndDaInviare"
    })
    public static class TrattamentoDocumentiFascicoliContenuti
        implements Serializable
    {

        @XmlElementRefs({
            @XmlElementRef(name = "DaInviare", type = JAXBElement.class),
            @XmlElementRef(name = "IdFascicoloContenuto", type = JAXBElement.class),
            @XmlElementRef(name = "TipoFascicolo", type = JAXBElement.class),
            @XmlElementRef(name = "Condivisibile", type = JAXBElement.class),
            @XmlElementRef(name = "DocumentiFascicolo", type = JAXBElement.class),
            @XmlElementRef(name = "Attivo", type = JAXBElement.class),
            @XmlElementRef(name = "TipoTrattamento", type = JAXBElement.class)
        })
        protected List<JAXBElement<? extends Serializable>> idFascicoloContenutoAndTipoFascicoloAndDaInviare;

        /**
         * Gets the value of the idFascicoloContenutoAndTipoFascicoloAndDaInviare property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the idFascicoloContenutoAndTipoFascicoloAndDaInviare property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getIdFascicoloContenutoAndTipoFascicoloAndDaInviare().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * {@link JAXBElement }{@code <}{@link Boolean }{@code >}
         * {@link JAXBElement }{@code <}{@link String }{@code >}
         * 
         * 
         */
        public List<JAXBElement<? extends Serializable>> getIdFascicoloContenutoAndTipoFascicoloAndDaInviare() {
            if (idFascicoloContenutoAndTipoFascicoloAndDaInviare == null) {
                idFascicoloContenutoAndTipoFascicoloAndDaInviare = new ArrayList<JAXBElement<? extends Serializable>>();
            }
            return this.idFascicoloContenutoAndTipoFascicoloAndDaInviare;
        }

    }

}
