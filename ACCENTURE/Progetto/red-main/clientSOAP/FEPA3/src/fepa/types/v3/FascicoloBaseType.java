
package fepa.types.v3;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Dettaglio del fascicolo
 * 
 * <p>Classe Java per fascicoloBase_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="fascicoloBase_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicolo" type="{urn:fepa:types:v3}Guid"/>
 *         &lt;element name="TipoFascicolo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DescrizioneTipoFascicolo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IdFascicoloRiferimento" type="{urn:fepa:types:v3}Guid" minOccurs="0"/>
 *         &lt;element name="TipoRelazione" type="{urn:fepa:types:v3}relazioneFascicolo_type" minOccurs="0"/>
 *         &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DatiFascicolo" type="{urn:fepa:types:v3}fascicoloMetadata_type" minOccurs="0"/>
 *         &lt;element name="Documenti" type="{urn:fepa:types:v3}documentoFascicolo_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="StatoFascicoloDocumentale" type="{urn:fepa:types:v3}statoFascicoloDocumentale_type"/>
 *         &lt;element name="statoEsternoFascicolo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataCreazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="DataAggiornamento" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DaInviare" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Attivo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Storicizzato" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SistemaProduttore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fascicoloBase_type", propOrder = {
    "idFascicolo",
    "tipoFascicolo",
    "descrizioneTipoFascicolo",
    "idFascicoloRiferimento",
    "tipoRelazione",
    "descrizione",
    "datiFascicolo",
    "documenti",
    "statoFascicoloDocumentale",
    "statoEsternoFascicolo",
    "dataCreazione",
    "dataAggiornamento",
    "daInviare",
    "attivo",
    "storicizzato",
    "sistemaProduttore"
})
@XmlSeeAlso({
    FascicoloType.class
})
public class FascicoloBaseType
    implements Serializable
{

    @XmlElement(name = "IdFascicolo", required = true)
    protected String idFascicolo;
    @XmlElement(name = "TipoFascicolo", required = true)
    protected String tipoFascicolo;
    @XmlElement(name = "DescrizioneTipoFascicolo")
    protected String descrizioneTipoFascicolo;
    @XmlElement(name = "IdFascicoloRiferimento")
    protected String idFascicoloRiferimento;
    @XmlElement(name = "TipoRelazione")
    @XmlSchemaType(name = "string")
    protected RelazioneFascicoloType tipoRelazione;
    @XmlElement(name = "Descrizione", required = true)
    protected String descrizione;
    @XmlElement(name = "DatiFascicolo")
    protected FascicoloMetadataType datiFascicolo;
    @XmlElement(name = "Documenti")
    protected List<DocumentoFascicoloType> documenti;
    @XmlElement(name = "StatoFascicoloDocumentale", required = true)
    @XmlSchemaType(name = "string")
    protected StatoFascicoloDocumentaleType statoFascicoloDocumentale;
    protected String statoEsternoFascicolo;
    @XmlElement(name = "DataCreazione", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataCreazione;
    @XmlElement(name = "DataAggiornamento", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataAggiornamento;
    @XmlElement(name = "DaInviare", defaultValue = "true")
    protected boolean daInviare;
    @XmlElement(name = "Attivo", defaultValue = "true")
    protected boolean attivo;
    @XmlElement(name = "Storicizzato", defaultValue = "true")
    protected boolean storicizzato;
    @XmlElement(name = "SistemaProduttore", required = true)
    protected String sistemaProduttore;

    /**
     * Recupera il valore della proprietà idFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicolo() {
        return idFascicolo;
    }

    /**
     * Imposta il valore della proprietà idFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicolo(String value) {
        this.idFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà tipoFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoFascicolo() {
        return tipoFascicolo;
    }

    /**
     * Imposta il valore della proprietà tipoFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoFascicolo(String value) {
        this.tipoFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà descrizioneTipoFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizioneTipoFascicolo() {
        return descrizioneTipoFascicolo;
    }

    /**
     * Imposta il valore della proprietà descrizioneTipoFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizioneTipoFascicolo(String value) {
        this.descrizioneTipoFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà idFascicoloRiferimento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloRiferimento() {
        return idFascicoloRiferimento;
    }

    /**
     * Imposta il valore della proprietà idFascicoloRiferimento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloRiferimento(String value) {
        this.idFascicoloRiferimento = value;
    }

    /**
     * Recupera il valore della proprietà tipoRelazione.
     * 
     * @return
     *     possible object is
     *     {@link RelazioneFascicoloType }
     *     
     */
    public RelazioneFascicoloType getTipoRelazione() {
        return tipoRelazione;
    }

    /**
     * Imposta il valore della proprietà tipoRelazione.
     * 
     * @param value
     *     allowed object is
     *     {@link RelazioneFascicoloType }
     *     
     */
    public void setTipoRelazione(RelazioneFascicoloType value) {
        this.tipoRelazione = value;
    }

    /**
     * Recupera il valore della proprietà descrizione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Imposta il valore della proprietà descrizione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Recupera il valore della proprietà datiFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link FascicoloMetadataType }
     *     
     */
    public FascicoloMetadataType getDatiFascicolo() {
        return datiFascicolo;
    }

    /**
     * Imposta il valore della proprietà datiFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link FascicoloMetadataType }
     *     
     */
    public void setDatiFascicolo(FascicoloMetadataType value) {
        this.datiFascicolo = value;
    }

    /**
     * Gets the value of the documenti property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documenti property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocumenti().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentoFascicoloType }
     * 
     * 
     */
    public List<DocumentoFascicoloType> getDocumenti() {
        if (documenti == null) {
            documenti = new ArrayList<DocumentoFascicoloType>();
        }
        return this.documenti;
    }

    /**
     * Recupera il valore della proprietà statoFascicoloDocumentale.
     * 
     * @return
     *     possible object is
     *     {@link StatoFascicoloDocumentaleType }
     *     
     */
    public StatoFascicoloDocumentaleType getStatoFascicoloDocumentale() {
        return statoFascicoloDocumentale;
    }

    /**
     * Imposta il valore della proprietà statoFascicoloDocumentale.
     * 
     * @param value
     *     allowed object is
     *     {@link StatoFascicoloDocumentaleType }
     *     
     */
    public void setStatoFascicoloDocumentale(StatoFascicoloDocumentaleType value) {
        this.statoFascicoloDocumentale = value;
    }

    /**
     * Recupera il valore della proprietà statoEsternoFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatoEsternoFascicolo() {
        return statoEsternoFascicolo;
    }

    /**
     * Imposta il valore della proprietà statoEsternoFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatoEsternoFascicolo(String value) {
        this.statoEsternoFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà dataCreazione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataCreazione() {
        return dataCreazione;
    }

    /**
     * Imposta il valore della proprietà dataCreazione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataCreazione(XMLGregorianCalendar value) {
        this.dataCreazione = value;
    }

    /**
     * Recupera il valore della proprietà dataAggiornamento.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAggiornamento() {
        return dataAggiornamento;
    }

    /**
     * Imposta il valore della proprietà dataAggiornamento.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAggiornamento(XMLGregorianCalendar value) {
        this.dataAggiornamento = value;
    }

    /**
     * Recupera il valore della proprietà daInviare.
     * 
     */
    public boolean isDaInviare() {
        return daInviare;
    }

    /**
     * Imposta il valore della proprietà daInviare.
     * 
     */
    public void setDaInviare(boolean value) {
        this.daInviare = value;
    }

    /**
     * Recupera il valore della proprietà attivo.
     * 
     */
    public boolean isAttivo() {
        return attivo;
    }

    /**
     * Imposta il valore della proprietà attivo.
     * 
     */
    public void setAttivo(boolean value) {
        this.attivo = value;
    }

    /**
     * Recupera il valore della proprietà storicizzato.
     * 
     */
    public boolean isStoricizzato() {
        return storicizzato;
    }

    /**
     * Imposta il valore della proprietà storicizzato.
     * 
     */
    public void setStoricizzato(boolean value) {
        this.storicizzato = value;
    }

    /**
     * Recupera il valore della proprietà sistemaProduttore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaProduttore() {
        return sistemaProduttore;
    }

    /**
     * Imposta il valore della proprietà sistemaProduttore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaProduttore(String value) {
        this.sistemaProduttore = value;
    }

}
