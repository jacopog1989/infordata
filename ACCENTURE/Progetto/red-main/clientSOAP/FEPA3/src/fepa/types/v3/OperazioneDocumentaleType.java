
package fepa.types.v3;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Dettaglio dell'operazione documentale
 * 
 * <p>Classe Java per operazioneDocumentale_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="operazioneDocumentale_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoOperazione">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="CONVERSIONE"/>
 *               &lt;enumeration value="FIRMA"/>
 *               &lt;enumeration value="TIMBRO"/>
 *               &lt;enumeration value="PLACEHOLDER"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="EsitoOperazione">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="INSERITA"/>
 *               &lt;enumeration value="LAVORAZIONE"/>
 *               &lt;enumeration value="COMPLETA"/>
 *               &lt;enumeration value="ERRORE"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DocumentoOperazione" type="{urn:fepa:types:v3}documentoFile_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "operazioneDocumentale_type", propOrder = {
    "tipoOperazione",
    "esitoOperazione",
    "documentoOperazione"
})
@XmlSeeAlso({
    fepa.types.v3.RisultatoOperazioneDocumentoType.Operazione.class
})
public class OperazioneDocumentaleType implements Serializable
{

    @XmlElement(name = "TipoOperazione", required = true)
    protected String tipoOperazione;
    @XmlElement(name = "EsitoOperazione", required = true)
    protected String esitoOperazione;
    @XmlElement(name = "DocumentoOperazione")
    protected DocumentoFileType documentoOperazione;

    /**
     * Recupera il valore della proprietà tipoOperazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoOperazione() {
        return tipoOperazione;
    }

    /**
     * Imposta il valore della proprietà tipoOperazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoOperazione(String value) {
        this.tipoOperazione = value;
    }

    /**
     * Recupera il valore della proprietà esitoOperazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsitoOperazione() {
        return esitoOperazione;
    }

    /**
     * Imposta il valore della proprietà esitoOperazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsitoOperazione(String value) {
        this.esitoOperazione = value;
    }

    /**
     * Recupera il valore della proprietà documentoOperazione.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoFileType }
     *     
     */
    public DocumentoFileType getDocumentoOperazione() {
        return documentoOperazione;
    }

    /**
     * Imposta il valore della proprietà documentoOperazione.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoFileType }
     *     
     */
    public void setDocumentoOperazione(DocumentoFileType value) {
        this.documentoOperazione = value;
    }

}
