
package fepa.types.v3;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the fepa.types.v3 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RichiestaSearchMetadatiFascicoloTypeMatadatiValue_QNAME = new QName("", "Value");
    private final static QName _RichiestaSearchMetadatiFascicoloTypeMatadatiKey_QNAME = new QName("", "Key");
    private final static QName _RichiestaUpdateFascicoloTypeTrattamentoDocumentiFascicoliContenutiTipoTrattamento_QNAME = new QName("", "TipoTrattamento");
    private final static QName _RichiestaUpdateFascicoloTypeTrattamentoDocumentiFascicoliContenutiIdFascicoloContenuto_QNAME = new QName("", "IdFascicoloContenuto");
    private final static QName _RichiestaUpdateFascicoloTypeTrattamentoDocumentiFascicoliContenutiAttivo_QNAME = new QName("", "Attivo");
    private final static QName _RichiestaUpdateFascicoloTypeTrattamentoDocumentiFascicoliContenutiDaInviare_QNAME = new QName("", "DaInviare");
    private final static QName _RichiestaUpdateFascicoloTypeTrattamentoDocumentiFascicoliContenutiCondivisibile_QNAME = new QName("", "Condivisibile");
    private final static QName _RichiestaUpdateFascicoloTypeTrattamentoDocumentiFascicoliContenutiTipoFascicolo_QNAME = new QName("", "TipoFascicolo");
    private final static QName _RichiestaUpdateFascicoloTypeTrattamentoDocumentiFascicoliContenutiDocumentiFascicolo_QNAME = new QName("", "DocumentiFascicolo");
    private final static QName _DocumentoFascicoloTypeTicketArchiviazione_QNAME = new QName("", "TicketArchiviazione");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: fepa.types.v3
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RichiestaAddDocumentoFascicoloType }
     * 
     */
    public RichiestaAddDocumentoFascicoloType createRichiestaAddDocumentoFascicoloType() {
        return new RichiestaAddDocumentoFascicoloType();
    }

    /**
     * Create an instance of {@link RichiestaUpdateFascicoloType }
     * 
     */
    public RichiestaUpdateFascicoloType createRichiestaUpdateFascicoloType() {
        return new RichiestaUpdateFascicoloType();
    }

    /**
     * Create an instance of {@link RichiestaSearchDocumentiFascicoloType }
     * 
     */
    public RichiestaSearchDocumentiFascicoloType createRichiestaSearchDocumentiFascicoloType() {
        return new RichiestaSearchDocumentiFascicoloType();
    }

    /**
     * Create an instance of {@link RichiestaSearchDocumentiFascicoloType.Metadata }
     * 
     */
    public RichiestaSearchDocumentiFascicoloType.Metadata createRichiestaSearchDocumentiFascicoloTypeMetadata() {
        return new RichiestaSearchDocumentiFascicoloType.Metadata();
    }

    /**
     * Create an instance of {@link RichiestaSearchMetadatiFascicoloType }
     * 
     */
    public RichiestaSearchMetadatiFascicoloType createRichiestaSearchMetadatiFascicoloType() {
        return new RichiestaSearchMetadatiFascicoloType();
    }

    /**
     * Create an instance of {@link RisultatoOperazioneDocumentoType }
     * 
     */
    public RisultatoOperazioneDocumentoType createRisultatoOperazioneDocumentoType() {
        return new RisultatoOperazioneDocumentoType();
    }

    /**
     * Create an instance of {@link RichiestaAssociateFascicoloType }
     * 
     */
    public RichiestaAssociateFascicoloType createRichiestaAssociateFascicoloType() {
        return new RichiestaAssociateFascicoloType();
    }

    /**
     * Create an instance of {@link RichiestaDisAssociateFascicoloType }
     * 
     */
    public RichiestaDisAssociateFascicoloType createRichiestaDisAssociateFascicoloType() {
        return new RichiestaDisAssociateFascicoloType();
    }

    /**
     * Create an instance of {@link RichiestaOperazioneDocumentaleType }
     * 
     */
    public RichiestaOperazioneDocumentaleType createRichiestaOperazioneDocumentaleType() {
        return new RichiestaOperazioneDocumentaleType();
    }

    /**
     * Create an instance of {@link RispostaUpdateDocumentoFascicoloType }
     * 
     */
    public RispostaUpdateDocumentoFascicoloType createRispostaUpdateDocumentoFascicoloType() {
        return new RispostaUpdateDocumentoFascicoloType();
    }

    /**
     * Create an instance of {@link RispostaDownloadDocumentoUrlFascicoloType }
     * 
     */
    public RispostaDownloadDocumentoUrlFascicoloType createRispostaDownloadDocumentoUrlFascicoloType() {
        return new RispostaDownloadDocumentoUrlFascicoloType();
    }

    /**
     * Create an instance of {@link RispostaUploadFileContentType }
     * 
     */
    public RispostaUploadFileContentType createRispostaUploadFileContentType() {
        return new RispostaUploadFileContentType();
    }

    /**
     * Create an instance of {@link RichiestaLockSubFasciclesType }
     * 
     */
    public RichiestaLockSubFasciclesType createRichiestaLockSubFasciclesType() {
        return new RichiestaLockSubFasciclesType();
    }

    /**
     * Create an instance of {@link RispostaGetLinkFascicoloType }
     * 
     */
    public RispostaGetLinkFascicoloType createRispostaGetLinkFascicoloType() {
        return new RispostaGetLinkFascicoloType();
    }

    /**
     * Create an instance of {@link RispostaDownloadFileContentType }
     * 
     */
    public RispostaDownloadFileContentType createRispostaDownloadFileContentType() {
        return new RispostaDownloadFileContentType();
    }

    /**
     * Create an instance of {@link OperazioneDocumentaleType }
     * 
     */
    public OperazioneDocumentaleType createOperazioneDocumentaleType() {
        return new OperazioneDocumentaleType();
    }

    /**
     * Create an instance of {@link DataRangeType }
     * 
     */
    public DataRangeType createDataRangeType() {
        return new DataRangeType();
    }

    /**
     * Create an instance of {@link ProtocolloType }
     * 
     */
    public ProtocolloType createProtocolloType() {
        return new ProtocolloType();
    }

    /**
     * Create an instance of {@link CriteriaDocumentiType }
     * 
     */
    public CriteriaDocumentiType createCriteriaDocumentiType() {
        return new CriteriaDocumentiType();
    }

    /**
     * Create an instance of {@link RichiestaGetFascicoloType }
     * 
     */
    public RichiestaGetFascicoloType createRichiestaGetFascicoloType() {
        return new RichiestaGetFascicoloType();
    }

    /**
     * Create an instance of {@link RichiestaCreateFascicoloType }
     * 
     */
    public RichiestaCreateFascicoloType createRichiestaCreateFascicoloType() {
        return new RichiestaCreateFascicoloType();
    }

    /**
     * Create an instance of {@link RispostaAddDocumentoFascicoloType }
     * 
     */
    public RispostaAddDocumentoFascicoloType createRispostaAddDocumentoFascicoloType() {
        return new RispostaAddDocumentoFascicoloType();
    }

    /**
     * Create an instance of {@link DocumentoFascicoloType }
     * 
     */
    public DocumentoFascicoloType createDocumentoFascicoloType() {
        return new DocumentoFascicoloType();
    }

    /**
     * Create an instance of {@link RispostaAssociaProtocolloType }
     * 
     */
    public RispostaAssociaProtocolloType createRispostaAssociaProtocolloType() {
        return new RispostaAssociaProtocolloType();
    }

    /**
     * Create an instance of {@link RichiestaAssociaProtocolloType }
     * 
     */
    public RichiestaAssociaProtocolloType createRichiestaAssociaProtocolloType() {
        return new RichiestaAssociaProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaSearchDocumentiFascicoloType }
     * 
     */
    public RispostaSearchDocumentiFascicoloType createRispostaSearchDocumentiFascicoloType() {
        return new RispostaSearchDocumentiFascicoloType();
    }

    /**
     * Create an instance of {@link RichiestaEliminaFascicoloType }
     * 
     */
    public RichiestaEliminaFascicoloType createRichiestaEliminaFascicoloType() {
        return new RichiestaEliminaFascicoloType();
    }

    /**
     * Create an instance of {@link RichiestaUploadFileContentType }
     * 
     */
    public RichiestaUploadFileContentType createRichiestaUploadFileContentType() {
        return new RichiestaUploadFileContentType();
    }

    /**
     * Create an instance of {@link RispostaConvertDocumentoType }
     * 
     */
    public RispostaConvertDocumentoType createRispostaConvertDocumentoType() {
        return new RispostaConvertDocumentoType();
    }

    /**
     * Create an instance of {@link RichiestaSendFascicoloType }
     * 
     */
    public RichiestaSendFascicoloType createRichiestaSendFascicoloType() {
        return new RichiestaSendFascicoloType();
    }

    /**
     * Create an instance of {@link FascicoloType }
     * 
     */
    public FascicoloType createFascicoloType() {
        return new FascicoloType();
    }

    /**
     * Create an instance of {@link RispostaLockSubFasciclesType }
     * 
     */
    public RispostaLockSubFasciclesType createRispostaLockSubFasciclesType() {
        return new RispostaLockSubFasciclesType();
    }

    /**
     * Create an instance of {@link RispostaRemoveFascicoloType }
     * 
     */
    public RispostaRemoveFascicoloType createRispostaRemoveFascicoloType() {
        return new RispostaRemoveFascicoloType();
    }

    /**
     * Create an instance of {@link ConversioneContentType }
     * 
     */
    public ConversioneContentType createConversioneContentType() {
        return new ConversioneContentType();
    }

    /**
     * Create an instance of {@link RichiestaRemoveDocumentoFascicoloType }
     * 
     */
    public RichiestaRemoveDocumentoFascicoloType createRichiestaRemoveDocumentoFascicoloType() {
        return new RichiestaRemoveDocumentoFascicoloType();
    }

    /**
     * Create an instance of {@link DocumentoMetadataType }
     * 
     */
    public DocumentoMetadataType createDocumentoMetadataType() {
        return new DocumentoMetadataType();
    }

    /**
     * Create an instance of {@link DocumentoContentType }
     * 
     */
    public DocumentoContentType createDocumentoContentType() {
        return new DocumentoContentType();
    }

    /**
     * Create an instance of {@link RichiestaDownloadDocumentoFascicoloType }
     * 
     */
    public RichiestaDownloadDocumentoFascicoloType createRichiestaDownloadDocumentoFascicoloType() {
        return new RichiestaDownloadDocumentoFascicoloType();
    }

    /**
     * Create an instance of {@link RichiestaDownloadDocumentoUrlFascicoloType }
     * 
     */
    public RichiestaDownloadDocumentoUrlFascicoloType createRichiestaDownloadDocumentoUrlFascicoloType() {
        return new RichiestaDownloadDocumentoUrlFascicoloType();
    }

    /**
     * Create an instance of {@link RichiestaConvertDocumentoType }
     * 
     */
    public RichiestaConvertDocumentoType createRichiestaConvertDocumentoType() {
        return new RichiestaConvertDocumentoType();
    }

    /**
     * Create an instance of {@link RispostaSearchMetadatiFascicoloType }
     * 
     */
    public RispostaSearchMetadatiFascicoloType createRispostaSearchMetadatiFascicoloType() {
        return new RispostaSearchMetadatiFascicoloType();
    }

    /**
     * Create an instance of {@link RispostaAssociateFascicoloType }
     * 
     */
    public RispostaAssociateFascicoloType createRispostaAssociateFascicoloType() {
        return new RispostaAssociateFascicoloType();
    }

    /**
     * Create an instance of {@link RispostaUpdateFascicoloType }
     * 
     */
    public RispostaUpdateFascicoloType createRispostaUpdateFascicoloType() {
        return new RispostaUpdateFascicoloType();
    }

    /**
     * Create an instance of {@link RispostaRemoveDocumentoFascicoloType }
     * 
     */
    public RispostaRemoveDocumentoFascicoloType createRispostaRemoveDocumentoFascicoloType() {
        return new RispostaRemoveDocumentoFascicoloType();
    }

    /**
     * Create an instance of {@link CriteriaFascicoliContenutiType }
     * 
     */
    public CriteriaFascicoliContenutiType createCriteriaFascicoliContenutiType() {
        return new CriteriaFascicoliContenutiType();
    }

    /**
     * Create an instance of {@link DocumentoSearchType }
     * 
     */
    public DocumentoSearchType createDocumentoSearchType() {
        return new DocumentoSearchType();
    }

    /**
     * Create an instance of {@link RichiestaGetLinkFascicoloType }
     * 
     */
    public RichiestaGetLinkFascicoloType createRichiestaGetLinkFascicoloType() {
        return new RichiestaGetLinkFascicoloType();
    }

    /**
     * Create an instance of {@link RichiestaGetDocumentoFascicoloType }
     * 
     */
    public RichiestaGetDocumentoFascicoloType createRichiestaGetDocumentoFascicoloType() {
        return new RichiestaGetDocumentoFascicoloType();
    }

    /**
     * Create an instance of {@link RichiestaArchiveFascicoloType }
     * 
     */
    public RichiestaArchiveFascicoloType createRichiestaArchiveFascicoloType() {
        return new RichiestaArchiveFascicoloType();
    }

    /**
     * Create an instance of {@link CodiceDescrizioneType }
     * 
     */
    public CodiceDescrizioneType createCodiceDescrizioneType() {
        return new CodiceDescrizioneType();
    }

    /**
     * Create an instance of {@link TipoDocumentoType }
     * 
     */
    public TipoDocumentoType createTipoDocumentoType() {
        return new TipoDocumentoType();
    }

    /**
     * Create an instance of {@link RispostaDownloadDocumentoFascicoloType }
     * 
     */
    public RispostaDownloadDocumentoFascicoloType createRispostaDownloadDocumentoFascicoloType() {
        return new RispostaDownloadDocumentoFascicoloType();
    }

    /**
     * Create an instance of {@link RispostaGetDocumentoFascicoloType }
     * 
     */
    public RispostaGetDocumentoFascicoloType createRispostaGetDocumentoFascicoloType() {
        return new RispostaGetDocumentoFascicoloType();
    }

    /**
     * Create an instance of {@link RichiestaDownloadFileContentType }
     * 
     */
    public RichiestaDownloadFileContentType createRichiestaDownloadFileContentType() {
        return new RichiestaDownloadFileContentType();
    }

    /**
     * Create an instance of {@link RispostaCreateFascicoloType }
     * 
     */
    public RispostaCreateFascicoloType createRispostaCreateFascicoloType() {
        return new RispostaCreateFascicoloType();
    }

    /**
     * Create an instance of {@link RichiestaDownloadDocumentoFascicoloByTypeDocType }
     * 
     */
    public RichiestaDownloadDocumentoFascicoloByTypeDocType createRichiestaDownloadDocumentoFascicoloByTypeDocType() {
        return new RichiestaDownloadDocumentoFascicoloByTypeDocType();
    }

    /**
     * Create an instance of {@link RispostaGetRisultatoOperazioneDocumentoType }
     * 
     */
    public RispostaGetRisultatoOperazioneDocumentoType createRispostaGetRisultatoOperazioneDocumentoType() {
        return new RispostaGetRisultatoOperazioneDocumentoType();
    }

    /**
     * Create an instance of {@link RichiestaChangeStatoFascicoloType }
     * 
     */
    public RichiestaChangeStatoFascicoloType createRichiestaChangeStatoFascicoloType() {
        return new RichiestaChangeStatoFascicoloType();
    }

    /**
     * Create an instance of {@link RispostaDownloadDocumentoFascicoloByTypeDocType }
     * 
     */
    public RispostaDownloadDocumentoFascicoloByTypeDocType createRispostaDownloadDocumentoFascicoloByTypeDocType() {
        return new RispostaDownloadDocumentoFascicoloByTypeDocType();
    }

    /**
     * Create an instance of {@link RispostaGetFascicoloType }
     * 
     */
    public RispostaGetFascicoloType createRispostaGetFascicoloType() {
        return new RispostaGetFascicoloType();
    }

    /**
     * Create an instance of {@link RispostaArchiveFascicoloType }
     * 
     */
    public RispostaArchiveFascicoloType createRispostaArchiveFascicoloType() {
        return new RispostaArchiveFascicoloType();
    }

    /**
     * Create an instance of {@link FascicoloResponseType }
     * 
     */
    public FascicoloResponseType createFascicoloResponseType() {
        return new FascicoloResponseType();
    }

    /**
     * Create an instance of {@link RispostaEliminaFascicoloType }
     * 
     */
    public RispostaEliminaFascicoloType createRispostaEliminaFascicoloType() {
        return new RispostaEliminaFascicoloType();
    }

    /**
     * Create an instance of {@link FascicoloBaseType }
     * 
     */
    public FascicoloBaseType createFascicoloBaseType() {
        return new FascicoloBaseType();
    }

    /**
     * Create an instance of {@link RichiestaGetRisultatoOperazioneDocumentoType }
     * 
     */
    public RichiestaGetRisultatoOperazioneDocumentoType createRichiestaGetRisultatoOperazioneDocumentoType() {
        return new RichiestaGetRisultatoOperazioneDocumentoType();
    }

    /**
     * Create an instance of {@link RichiestaRemoveFascicoloType }
     * 
     */
    public RichiestaRemoveFascicoloType createRichiestaRemoveFascicoloType() {
        return new RichiestaRemoveFascicoloType();
    }

    /**
     * Create an instance of {@link IdentificativoAssociazioneApplicazioneType }
     * 
     */
    public IdentificativoAssociazioneApplicazioneType createIdentificativoAssociazioneApplicazioneType() {
        return new IdentificativoAssociazioneApplicazioneType();
    }

    /**
     * Create an instance of {@link RichiestaUpdateDocumentoFascicoloType }
     * 
     */
    public RichiestaUpdateDocumentoFascicoloType createRichiestaUpdateDocumentoFascicoloType() {
        return new RichiestaUpdateDocumentoFascicoloType();
    }

    /**
     * Create an instance of {@link RispostaSendFascicoloType }
     * 
     */
    public RispostaSendFascicoloType createRispostaSendFascicoloType() {
        return new RispostaSendFascicoloType();
    }

    /**
     * Create an instance of {@link DocumentoFileType }
     * 
     */
    public DocumentoFileType createDocumentoFileType() {
        return new DocumentoFileType();
    }

    /**
     * Create an instance of {@link RispostaDisAssociateFascicoloType }
     * 
     */
    public RispostaDisAssociateFascicoloType createRispostaDisAssociateFascicoloType() {
        return new RispostaDisAssociateFascicoloType();
    }

    /**
     * Create an instance of {@link FascicoloMetadataType }
     * 
     */
    public FascicoloMetadataType createFascicoloMetadataType() {
        return new FascicoloMetadataType();
    }

    /**
     * Create an instance of {@link RispostaChangeStatoFascicoloType }
     * 
     */
    public RispostaChangeStatoFascicoloType createRispostaChangeStatoFascicoloType() {
        return new RispostaChangeStatoFascicoloType();
    }

    /**
     * Create an instance of {@link RichiestaAddDocumentoFascicoloType.Documento }
     * 
     */
    public RichiestaAddDocumentoFascicoloType.Documento createRichiestaAddDocumentoFascicoloTypeDocumento() {
        return new RichiestaAddDocumentoFascicoloType.Documento();
    }

    /**
     * Create an instance of {@link RichiestaAddDocumentoFascicoloType.DocumentoEsistente }
     * 
     */
    public RichiestaAddDocumentoFascicoloType.DocumentoEsistente createRichiestaAddDocumentoFascicoloTypeDocumentoEsistente() {
        return new RichiestaAddDocumentoFascicoloType.DocumentoEsistente();
    }

    /**
     * Create an instance of {@link RichiestaUpdateFascicoloType.TrattamentoDocumentiFascicoliContenuti }
     * 
     */
    public RichiestaUpdateFascicoloType.TrattamentoDocumentiFascicoliContenuti createRichiestaUpdateFascicoloTypeTrattamentoDocumentiFascicoliContenuti() {
        return new RichiestaUpdateFascicoloType.TrattamentoDocumentiFascicoliContenuti();
    }

    /**
     * Create an instance of {@link RichiestaSearchDocumentiFascicoloType.Metadata.Matadati }
     * 
     */
    public RichiestaSearchDocumentiFascicoloType.Metadata.Matadati createRichiestaSearchDocumentiFascicoloTypeMetadataMatadati() {
        return new RichiestaSearchDocumentiFascicoloType.Metadata.Matadati();
    }

    /**
     * Create an instance of {@link RichiestaSearchMetadatiFascicoloType.Matadati }
     * 
     */
    public RichiestaSearchMetadatiFascicoloType.Matadati createRichiestaSearchMetadatiFascicoloTypeMatadati() {
        return new RichiestaSearchMetadatiFascicoloType.Matadati();
    }

    /**
     * Create an instance of {@link RisultatoOperazioneDocumentoType.Operazione }
     * 
     */
    public RisultatoOperazioneDocumentoType.Operazione createRisultatoOperazioneDocumentoTypeOperazione() {
        return new RisultatoOperazioneDocumentoType.Operazione();
    }

    /**
     * Create an instance of {@link RichiestaAssociateFascicoloType.Associazioni }
     * 
     */
    public RichiestaAssociateFascicoloType.Associazioni createRichiestaAssociateFascicoloTypeAssociazioni() {
        return new RichiestaAssociateFascicoloType.Associazioni();
    }

    /**
     * Create an instance of {@link RichiestaDisAssociateFascicoloType.Associazioni }
     * 
     */
    public RichiestaDisAssociateFascicoloType.Associazioni createRichiestaDisAssociateFascicoloTypeAssociazioni() {
        return new RichiestaDisAssociateFascicoloType.Associazioni();
    }

    /**
     * Create an instance of {@link RichiestaOperazioneDocumentaleType.ParametriOperazione }
     * 
     */
    public RichiestaOperazioneDocumentaleType.ParametriOperazione createRichiestaOperazioneDocumentaleTypeParametriOperazione() {
        return new RichiestaOperazioneDocumentaleType.ParametriOperazione();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Value", scope = RichiestaSearchMetadatiFascicoloType.Matadati.class)
    public JAXBElement<Object> createRichiestaSearchMetadatiFascicoloTypeMatadatiValue(Object value) {
        return new JAXBElement<Object>(_RichiestaSearchMetadatiFascicoloTypeMatadatiValue_QNAME, Object.class, RichiestaSearchMetadatiFascicoloType.Matadati.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Key", scope = RichiestaSearchMetadatiFascicoloType.Matadati.class)
    public JAXBElement<Object> createRichiestaSearchMetadatiFascicoloTypeMatadatiKey(Object value) {
        return new JAXBElement<Object>(_RichiestaSearchMetadatiFascicoloTypeMatadatiKey_QNAME, Object.class, RichiestaSearchMetadatiFascicoloType.Matadati.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TipoTrattamento", scope = RichiestaUpdateFascicoloType.TrattamentoDocumentiFascicoliContenuti.class, defaultValue = "INCLUDI")
    public JAXBElement<String> createRichiestaUpdateFascicoloTypeTrattamentoDocumentiFascicoliContenutiTipoTrattamento(String value) {
        return new JAXBElement<String>(_RichiestaUpdateFascicoloTypeTrattamentoDocumentiFascicoliContenutiTipoTrattamento_QNAME, String.class, RichiestaUpdateFascicoloType.TrattamentoDocumentiFascicoliContenuti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IdFascicoloContenuto", scope = RichiestaUpdateFascicoloType.TrattamentoDocumentiFascicoliContenuti.class)
    public JAXBElement<String> createRichiestaUpdateFascicoloTypeTrattamentoDocumentiFascicoliContenutiIdFascicoloContenuto(String value) {
        return new JAXBElement<String>(_RichiestaUpdateFascicoloTypeTrattamentoDocumentiFascicoliContenutiIdFascicoloContenuto_QNAME, String.class, RichiestaUpdateFascicoloType.TrattamentoDocumentiFascicoliContenuti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Attivo", scope = RichiestaUpdateFascicoloType.TrattamentoDocumentiFascicoliContenuti.class, defaultValue = "true")
    public JAXBElement<Boolean> createRichiestaUpdateFascicoloTypeTrattamentoDocumentiFascicoliContenutiAttivo(Boolean value) {
        return new JAXBElement<Boolean>(_RichiestaUpdateFascicoloTypeTrattamentoDocumentiFascicoliContenutiAttivo_QNAME, Boolean.class, RichiestaUpdateFascicoloType.TrattamentoDocumentiFascicoliContenuti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DaInviare", scope = RichiestaUpdateFascicoloType.TrattamentoDocumentiFascicoliContenuti.class, defaultValue = "true")
    public JAXBElement<Boolean> createRichiestaUpdateFascicoloTypeTrattamentoDocumentiFascicoliContenutiDaInviare(Boolean value) {
        return new JAXBElement<Boolean>(_RichiestaUpdateFascicoloTypeTrattamentoDocumentiFascicoliContenutiDaInviare_QNAME, Boolean.class, RichiestaUpdateFascicoloType.TrattamentoDocumentiFascicoliContenuti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Condivisibile", scope = RichiestaUpdateFascicoloType.TrattamentoDocumentiFascicoliContenuti.class, defaultValue = "true")
    public JAXBElement<Boolean> createRichiestaUpdateFascicoloTypeTrattamentoDocumentiFascicoliContenutiCondivisibile(Boolean value) {
        return new JAXBElement<Boolean>(_RichiestaUpdateFascicoloTypeTrattamentoDocumentiFascicoliContenutiCondivisibile_QNAME, Boolean.class, RichiestaUpdateFascicoloType.TrattamentoDocumentiFascicoliContenuti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TipoFascicolo", scope = RichiestaUpdateFascicoloType.TrattamentoDocumentiFascicoliContenuti.class)
    public JAXBElement<String> createRichiestaUpdateFascicoloTypeTrattamentoDocumentiFascicoliContenutiTipoFascicolo(String value) {
        return new JAXBElement<String>(_RichiestaUpdateFascicoloTypeTrattamentoDocumentiFascicoliContenutiTipoFascicolo_QNAME, String.class, RichiestaUpdateFascicoloType.TrattamentoDocumentiFascicoliContenuti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DocumentiFascicolo", scope = RichiestaUpdateFascicoloType.TrattamentoDocumentiFascicoliContenuti.class)
    public JAXBElement<String> createRichiestaUpdateFascicoloTypeTrattamentoDocumentiFascicoliContenutiDocumentiFascicolo(String value) {
        return new JAXBElement<String>(_RichiestaUpdateFascicoloTypeTrattamentoDocumentiFascicoliContenutiDocumentiFascicolo_QNAME, String.class, RichiestaUpdateFascicoloType.TrattamentoDocumentiFascicoliContenuti.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Value", scope = RichiestaSearchDocumentiFascicoloType.Metadata.Matadati.class)
    public JAXBElement<Object> createRichiestaSearchDocumentiFascicoloTypeMetadataMatadatiValue(Object value) {
        return new JAXBElement<Object>(_RichiestaSearchMetadatiFascicoloTypeMatadatiValue_QNAME, Object.class, RichiestaSearchDocumentiFascicoloType.Metadata.Matadati.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Key", scope = RichiestaSearchDocumentiFascicoloType.Metadata.Matadati.class)
    public JAXBElement<Object> createRichiestaSearchDocumentiFascicoloTypeMetadataMatadatiKey(Object value) {
        return new JAXBElement<Object>(_RichiestaSearchMetadatiFascicoloTypeMatadatiKey_QNAME, Object.class, RichiestaSearchDocumentiFascicoloType.Metadata.Matadati.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TicketArchiviazione", scope = DocumentoFascicoloType.class)
    public JAXBElement<String> createDocumentoFascicoloTypeTicketArchiviazione(String value) {
        return new JAXBElement<String>(_DocumentoFascicoloTypeTicketArchiviazione_QNAME, String.class, DocumentoFascicoloType.class, value);
    }

}
