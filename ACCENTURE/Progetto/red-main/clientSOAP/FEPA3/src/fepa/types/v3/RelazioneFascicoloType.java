
package fepa.types.v3;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per relazioneFascicolo_type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="relazioneFascicolo_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SOTTOFASCICOLO"/>
 *     &lt;enumeration value="FASCICOLOCOLLEGATO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "relazioneFascicolo_type")
@XmlEnum
public enum RelazioneFascicoloType {


    /**
     * Fascicolo con sincronizzazione dei dati pubblici biunivoca
     * 
     */
    SOTTOFASCICOLO,

    /**
     * copia i documenti CONDIVISI dalla sorgente alla destinazione non sincronizzando ulteriori aggiunte ma visualizzando sempre la versione corrente
     * 
     */
    FASCICOLOCOLLEGATO;

    public String value() {
        return name();
    }

    public static RelazioneFascicoloType fromValue(String v) {
        return valueOf(v);
    }

}
