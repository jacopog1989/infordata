
package fepa.types.v3;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * criteri di filtro per l'estrazione dei fascicoli SIPATR da un decreto
 * 
 * <p>Classe Java per criteriaFascicoliContenuti_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="criteriaFascicoliContenuti_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoFascicolo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoRelazione" type="{urn:fepa:types:v3}relazioneFascicolo_type" minOccurs="0"/>
 *         &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataCreazione" type="{urn:fepa:types:v3}dataRangeType" minOccurs="0"/>
 *         &lt;element name="DataAggiornamento" type="{urn:fepa:types:v3}dataRangeType" minOccurs="0"/>
 *         &lt;element name="StatoFascicoloDocumentale" type="{urn:fepa:types:v3}statoFascicoloDocumentale_type" minOccurs="0"/>
 *         &lt;element name="SistemaProduttore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Applicazione" type="{urn:fepa:types:v3}IdentificativoApplicazioneType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "criteriaFascicoliContenuti_type", propOrder = {
    "tipoFascicolo",
    "tipoRelazione",
    "descrizione",
    "dataCreazione",
    "dataAggiornamento",
    "statoFascicoloDocumentale",
    "sistemaProduttore",
    "applicazione"
})
public class CriteriaFascicoliContenutiType
    implements Serializable
{

    @XmlElement(name = "TipoFascicolo")
    protected String tipoFascicolo;
    @XmlElement(name = "TipoRelazione")
    @XmlSchemaType(name = "string")
    protected RelazioneFascicoloType tipoRelazione;
    @XmlElement(name = "Descrizione")
    protected String descrizione;
    @XmlElement(name = "DataCreazione")
    protected DataRangeType dataCreazione;
    @XmlElement(name = "DataAggiornamento")
    protected DataRangeType dataAggiornamento;
    @XmlElement(name = "StatoFascicoloDocumentale")
    @XmlSchemaType(name = "string")
    protected StatoFascicoloDocumentaleType statoFascicoloDocumentale;
    @XmlElement(name = "SistemaProduttore")
    protected String sistemaProduttore;
    @XmlElement(name = "Applicazione")
    protected String applicazione;

    /**
     * Recupera il valore della proprietà tipoFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoFascicolo() {
        return tipoFascicolo;
    }

    /**
     * Imposta il valore della proprietà tipoFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoFascicolo(String value) {
        this.tipoFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà tipoRelazione.
     * 
     * @return
     *     possible object is
     *     {@link RelazioneFascicoloType }
     *     
     */
    public RelazioneFascicoloType getTipoRelazione() {
        return tipoRelazione;
    }

    /**
     * Imposta il valore della proprietà tipoRelazione.
     * 
     * @param value
     *     allowed object is
     *     {@link RelazioneFascicoloType }
     *     
     */
    public void setTipoRelazione(RelazioneFascicoloType value) {
        this.tipoRelazione = value;
    }

    /**
     * Recupera il valore della proprietà descrizione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Imposta il valore della proprietà descrizione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Recupera il valore della proprietà dataCreazione.
     * 
     * @return
     *     possible object is
     *     {@link DataRangeType }
     *     
     */
    public DataRangeType getDataCreazione() {
        return dataCreazione;
    }

    /**
     * Imposta il valore della proprietà dataCreazione.
     * 
     * @param value
     *     allowed object is
     *     {@link DataRangeType }
     *     
     */
    public void setDataCreazione(DataRangeType value) {
        this.dataCreazione = value;
    }

    /**
     * Recupera il valore della proprietà dataAggiornamento.
     * 
     * @return
     *     possible object is
     *     {@link DataRangeType }
     *     
     */
    public DataRangeType getDataAggiornamento() {
        return dataAggiornamento;
    }

    /**
     * Imposta il valore della proprietà dataAggiornamento.
     * 
     * @param value
     *     allowed object is
     *     {@link DataRangeType }
     *     
     */
    public void setDataAggiornamento(DataRangeType value) {
        this.dataAggiornamento = value;
    }

    /**
     * Recupera il valore della proprietà statoFascicoloDocumentale.
     * 
     * @return
     *     possible object is
     *     {@link StatoFascicoloDocumentaleType }
     *     
     */
    public StatoFascicoloDocumentaleType getStatoFascicoloDocumentale() {
        return statoFascicoloDocumentale;
    }

    /**
     * Imposta il valore della proprietà statoFascicoloDocumentale.
     * 
     * @param value
     *     allowed object is
     *     {@link StatoFascicoloDocumentaleType }
     *     
     */
    public void setStatoFascicoloDocumentale(StatoFascicoloDocumentaleType value) {
        this.statoFascicoloDocumentale = value;
    }

    /**
     * Recupera il valore della proprietà sistemaProduttore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaProduttore() {
        return sistemaProduttore;
    }

    /**
     * Imposta il valore della proprietà sistemaProduttore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaProduttore(String value) {
        this.sistemaProduttore = value;
    }

    /**
     * Recupera il valore della proprietà applicazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicazione() {
        return applicazione;
    }

    /**
     * Imposta il valore della proprietà applicazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicazione(String value) {
        this.applicazione = value;
    }

}
