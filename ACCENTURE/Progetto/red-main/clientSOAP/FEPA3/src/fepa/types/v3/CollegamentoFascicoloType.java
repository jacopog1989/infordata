
package fepa.types.v3;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per collegamentoFascicolo_type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="collegamentoFascicolo_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Sincronizzato"/>
 *     &lt;enumeration value="Contenuto"/>
 *     &lt;enumeration value="VersioneContenuto"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "collegamentoFascicolo_type")
@XmlEnum
public enum CollegamentoFascicoloType {


    /**
     * Fascicolo con sincronizzazione dei dati pubblici biunivoca
     * 
     */
    @XmlEnumValue("Sincronizzato")
    SINCRONIZZATO("Sincronizzato"),

    /**
     * copia i documenti CONDIVISI dalla sorgente alla destinazione non sincronizzando ulteriori aggiunte ma visualizzando sempre la versione corrente
     * 
     */
    @XmlEnumValue("Contenuto")
    CONTENUTO("Contenuto"),

    /**
     * copia la versione corrente i documenti CONDIVISI dalla sorgente alla destinazione non sincronizzando ulteriori aggiunte e visualizzando la versione al momento della copia
     * 
     */
    @XmlEnumValue("VersioneContenuto")
    VERSIONE_CONTENUTO("VersioneContenuto");
    private final String value;

    CollegamentoFascicoloType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CollegamentoFascicoloType fromValue(String v) {
        for (CollegamentoFascicoloType c: CollegamentoFascicoloType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
