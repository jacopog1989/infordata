
package fepa.types.v3;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Elenco di Tipologia di Documenti Inseribili nel Fasciolo
 * 
 * <p>Classe Java per richiestaOperazioneDocumentale_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiestaOperazioneDocumentale_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoOperazione" type="{urn:fepa:types:v3}tipoOperazione_type"/>
 *         &lt;element name="ParametriOperazione" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Chiave" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Valore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="IdDocumentoOperazione" type="{urn:fepa:types:v3}Guid" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiestaOperazioneDocumentale_type", propOrder = {
    "tipoOperazione",
    "parametriOperazione",
    "idDocumentoOperazione"
})
public class RichiestaOperazioneDocumentaleType
    implements Serializable
{

    @XmlElement(name = "TipoOperazione", required = true)
    @XmlSchemaType(name = "string")
    protected TipoOperazioneType tipoOperazione;
    @XmlElement(name = "ParametriOperazione")
    protected List<RichiestaOperazioneDocumentaleType.ParametriOperazione> parametriOperazione;
    @XmlElement(name = "IdDocumentoOperazione")
    protected String idDocumentoOperazione;

    /**
     * Recupera il valore della proprietà tipoOperazione.
     * 
     * @return
     *     possible object is
     *     {@link TipoOperazioneType }
     *     
     */
    public TipoOperazioneType getTipoOperazione() {
        return tipoOperazione;
    }

    /**
     * Imposta il valore della proprietà tipoOperazione.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoOperazioneType }
     *     
     */
    public void setTipoOperazione(TipoOperazioneType value) {
        this.tipoOperazione = value;
    }

    /**
     * Gets the value of the parametriOperazione property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the parametriOperazione property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParametriOperazione().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RichiestaOperazioneDocumentaleType.ParametriOperazione }
     * 
     * 
     */
    public List<RichiestaOperazioneDocumentaleType.ParametriOperazione> getParametriOperazione() {
        if (parametriOperazione == null) {
            parametriOperazione = new ArrayList<RichiestaOperazioneDocumentaleType.ParametriOperazione>();
        }
        return this.parametriOperazione;
    }

    /**
     * Recupera il valore della proprietà idDocumentoOperazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoOperazione() {
        return idDocumentoOperazione;
    }

    /**
     * Imposta il valore della proprietà idDocumentoOperazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoOperazione(String value) {
        this.idDocumentoOperazione = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Chiave" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Valore" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "chiave",
        "valore"
    })
    public static class ParametriOperazione
        implements Serializable
    {

        @XmlElement(name = "Chiave", required = true)
        protected String chiave;
        @XmlElement(name = "Valore", required = true)
        protected String valore;

        /**
         * Recupera il valore della proprietà chiave.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getChiave() {
            return chiave;
        }

        /**
         * Imposta il valore della proprietà chiave.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setChiave(String value) {
            this.chiave = value;
        }

        /**
         * Recupera il valore della proprietà valore.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getValore() {
            return valore;
        }

        /**
         * Imposta il valore della proprietà valore.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setValore(String value) {
            this.valore = value;
        }

    }

}
