
package fepa.types.v3;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * criteri di filtro per l'estrazione dei documenti del ATTo decreto
 * 
 * <p>Classe Java per criteriaDocumenti_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="criteriaDocumenti_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoDocumento" type="{urn:fepa:types:v3}tipoDocumento_type" minOccurs="0"/>
 *         &lt;element name="MimeType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataCreazione" type="{urn:fepa:types:v3}dataRangeType" minOccurs="0"/>
 *         &lt;element name="DataDocumento" type="{urn:fepa:types:v3}dataRangeType" minOccurs="0"/>
 *         &lt;element name="ProtocolloMittente" type="{urn:fepa:types:v3}protocollo_type" minOccurs="0"/>
 *         &lt;element name="Protocollo" type="{urn:fepa:types:v3}protocollo_type" minOccurs="0"/>
 *         &lt;element name="EsitoOperazione" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="INSERITA"/>
 *               &lt;enumeration value="LAVORAZIONE"/>
 *               &lt;enumeration value="COMPLETA"/>
 *               &lt;enumeration value="ERRORE"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "criteriaDocumenti_type", propOrder = {
    "tipoDocumento",
    "mimeType",
    "descrizione",
    "dataCreazione",
    "dataDocumento",
    "protocolloMittente",
    "protocollo",
    "esitoOperazione"
})
public class CriteriaDocumentiType
    implements Serializable
{

    @XmlElement(name = "TipoDocumento")
    protected TipoDocumentoType tipoDocumento;
    @XmlElement(name = "MimeType")
    protected String mimeType;
    @XmlElement(name = "Descrizione")
    protected String descrizione;
    @XmlElement(name = "DataCreazione")
    protected DataRangeType dataCreazione;
    @XmlElement(name = "DataDocumento")
    protected DataRangeType dataDocumento;
    @XmlElement(name = "ProtocolloMittente")
    protected ProtocolloType protocolloMittente;
    @XmlElement(name = "Protocollo")
    protected ProtocolloType protocollo;
    @XmlElement(name = "EsitoOperazione")
    protected String esitoOperazione;

    /**
     * Recupera il valore della proprietà tipoDocumento.
     * 
     * @return
     *     possible object is
     *     {@link TipoDocumentoType }
     *     
     */
    public TipoDocumentoType getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Imposta il valore della proprietà tipoDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDocumentoType }
     *     
     */
    public void setTipoDocumento(TipoDocumentoType value) {
        this.tipoDocumento = value;
    }

    /**
     * Recupera il valore della proprietà mimeType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMimeType() {
        return mimeType;
    }

    /**
     * Imposta il valore della proprietà mimeType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMimeType(String value) {
        this.mimeType = value;
    }

    /**
     * Recupera il valore della proprietà descrizione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Imposta il valore della proprietà descrizione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Recupera il valore della proprietà dataCreazione.
     * 
     * @return
     *     possible object is
     *     {@link DataRangeType }
     *     
     */
    public DataRangeType getDataCreazione() {
        return dataCreazione;
    }

    /**
     * Imposta il valore della proprietà dataCreazione.
     * 
     * @param value
     *     allowed object is
     *     {@link DataRangeType }
     *     
     */
    public void setDataCreazione(DataRangeType value) {
        this.dataCreazione = value;
    }

    /**
     * Recupera il valore della proprietà dataDocumento.
     * 
     * @return
     *     possible object is
     *     {@link DataRangeType }
     *     
     */
    public DataRangeType getDataDocumento() {
        return dataDocumento;
    }

    /**
     * Imposta il valore della proprietà dataDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link DataRangeType }
     *     
     */
    public void setDataDocumento(DataRangeType value) {
        this.dataDocumento = value;
    }

    /**
     * Recupera il valore della proprietà protocolloMittente.
     * 
     * @return
     *     possible object is
     *     {@link ProtocolloType }
     *     
     */
    public ProtocolloType getProtocolloMittente() {
        return protocolloMittente;
    }

    /**
     * Imposta il valore della proprietà protocolloMittente.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtocolloType }
     *     
     */
    public void setProtocolloMittente(ProtocolloType value) {
        this.protocolloMittente = value;
    }

    /**
     * Recupera il valore della proprietà protocollo.
     * 
     * @return
     *     possible object is
     *     {@link ProtocolloType }
     *     
     */
    public ProtocolloType getProtocollo() {
        return protocollo;
    }

    /**
     * Imposta il valore della proprietà protocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtocolloType }
     *     
     */
    public void setProtocollo(ProtocolloType value) {
        this.protocollo = value;
    }

    /**
     * Recupera il valore della proprietà esitoOperazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsitoOperazione() {
        return esitoOperazione;
    }

    /**
     * Imposta il valore della proprietà esitoOperazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsitoOperazione(String value) {
        this.esitoOperazione = value;
    }

}
