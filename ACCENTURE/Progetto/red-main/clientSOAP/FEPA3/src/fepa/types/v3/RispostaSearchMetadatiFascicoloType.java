
package fepa.types.v3;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import fepa.header.v3.BaseServiceResponseType;


/**
 * Esito ricerca
 * 
 * <p>Classe Java per risposta_searchMetadatiFascicolo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_searchMetadatiFascicolo_type">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:fepa:header:v3}BaseServiceResponseType">
 *       &lt;sequence>
 *         &lt;element name="DettaglioFascicolo" type="{urn:fepa:types:v3}fascicoloResponse_type" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_searchMetadatiFascicolo_type", propOrder = {
    "dettaglioFascicolo"
})
public class RispostaSearchMetadatiFascicoloType
    extends BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "DettaglioFascicolo")
    protected List<FascicoloResponseType> dettaglioFascicolo;

    /**
     * Gets the value of the dettaglioFascicolo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dettaglioFascicolo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDettaglioFascicolo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FascicoloResponseType }
     * 
     * 
     */
    public List<FascicoloResponseType> getDettaglioFascicolo() {
        if (dettaglioFascicolo == null) {
            dettaglioFascicolo = new ArrayList<FascicoloResponseType>();
        }
        return this.dettaglioFascicolo;
    }

}
