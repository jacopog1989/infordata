
package fepa.types.v3;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Dettaglio del fascicolo
 * 
 * <p>Classe Java per fascicoloResponse_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="fascicoloResponse_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicolo" type="{urn:fepa:types:v3}Guid"/>
 *         &lt;element name="TipoFascicolo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdFascicoloRiferimento" type="{urn:fepa:types:v3}Guid" minOccurs="0"/>
 *         &lt;element name="TipoRelazione" type="{urn:fepa:types:v3}relazioneFascicolo_type" minOccurs="0"/>
 *         &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StatoFascicoloDocumentale" type="{urn:fepa:types:v3}statoFascicoloDocumentale_type"/>
 *         &lt;element name="DataCreazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="DataAggiornamento" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DaInviare" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Storicizzato" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Attivo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SistemaProduttore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fascicoloResponse_type", propOrder = {
    "idFascicolo",
    "tipoFascicolo",
    "idFascicoloRiferimento",
    "tipoRelazione",
    "descrizione",
    "statoFascicoloDocumentale",
    "dataCreazione",
    "dataAggiornamento",
    "daInviare",
    "storicizzato",
    "attivo",
    "sistemaProduttore"
})
public class FascicoloResponseType
    implements Serializable
{

    @XmlElement(name = "IdFascicolo", required = true)
    protected String idFascicolo;
    @XmlElement(name = "TipoFascicolo", required = true)
    protected String tipoFascicolo;
    @XmlElement(name = "IdFascicoloRiferimento")
    protected String idFascicoloRiferimento;
    @XmlElement(name = "TipoRelazione")
    @XmlSchemaType(name = "string")
    protected RelazioneFascicoloType tipoRelazione;
    @XmlElement(name = "Descrizione", required = true)
    protected String descrizione;
    @XmlElement(name = "StatoFascicoloDocumentale", required = true)
    @XmlSchemaType(name = "string")
    protected StatoFascicoloDocumentaleType statoFascicoloDocumentale;
    @XmlElement(name = "DataCreazione", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataCreazione;
    @XmlElement(name = "DataAggiornamento", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataAggiornamento;
    @XmlElement(name = "DaInviare", defaultValue = "true")
    protected boolean daInviare;
    @XmlElement(name = "Storicizzato", defaultValue = "true")
    protected boolean storicizzato;
    @XmlElement(name = "Attivo", defaultValue = "true")
    protected boolean attivo;
    @XmlElement(name = "SistemaProduttore", required = true)
    protected String sistemaProduttore;

    /**
     * Recupera il valore della proprietà idFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicolo() {
        return idFascicolo;
    }

    /**
     * Imposta il valore della proprietà idFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicolo(String value) {
        this.idFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà tipoFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoFascicolo() {
        return tipoFascicolo;
    }

    /**
     * Imposta il valore della proprietà tipoFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoFascicolo(String value) {
        this.tipoFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà idFascicoloRiferimento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloRiferimento() {
        return idFascicoloRiferimento;
    }

    /**
     * Imposta il valore della proprietà idFascicoloRiferimento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloRiferimento(String value) {
        this.idFascicoloRiferimento = value;
    }

    /**
     * Recupera il valore della proprietà tipoRelazione.
     * 
     * @return
     *     possible object is
     *     {@link RelazioneFascicoloType }
     *     
     */
    public RelazioneFascicoloType getTipoRelazione() {
        return tipoRelazione;
    }

    /**
     * Imposta il valore della proprietà tipoRelazione.
     * 
     * @param value
     *     allowed object is
     *     {@link RelazioneFascicoloType }
     *     
     */
    public void setTipoRelazione(RelazioneFascicoloType value) {
        this.tipoRelazione = value;
    }

    /**
     * Recupera il valore della proprietà descrizione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Imposta il valore della proprietà descrizione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Recupera il valore della proprietà statoFascicoloDocumentale.
     * 
     * @return
     *     possible object is
     *     {@link StatoFascicoloDocumentaleType }
     *     
     */
    public StatoFascicoloDocumentaleType getStatoFascicoloDocumentale() {
        return statoFascicoloDocumentale;
    }

    /**
     * Imposta il valore della proprietà statoFascicoloDocumentale.
     * 
     * @param value
     *     allowed object is
     *     {@link StatoFascicoloDocumentaleType }
     *     
     */
    public void setStatoFascicoloDocumentale(StatoFascicoloDocumentaleType value) {
        this.statoFascicoloDocumentale = value;
    }

    /**
     * Recupera il valore della proprietà dataCreazione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataCreazione() {
        return dataCreazione;
    }

    /**
     * Imposta il valore della proprietà dataCreazione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataCreazione(XMLGregorianCalendar value) {
        this.dataCreazione = value;
    }

    /**
     * Recupera il valore della proprietà dataAggiornamento.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAggiornamento() {
        return dataAggiornamento;
    }

    /**
     * Imposta il valore della proprietà dataAggiornamento.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAggiornamento(XMLGregorianCalendar value) {
        this.dataAggiornamento = value;
    }

    /**
     * Recupera il valore della proprietà daInviare.
     * 
     */
    public boolean isDaInviare() {
        return daInviare;
    }

    /**
     * Imposta il valore della proprietà daInviare.
     * 
     */
    public void setDaInviare(boolean value) {
        this.daInviare = value;
    }

    /**
     * Recupera il valore della proprietà storicizzato.
     * 
     */
    public boolean isStoricizzato() {
        return storicizzato;
    }

    /**
     * Imposta il valore della proprietà storicizzato.
     * 
     */
    public void setStoricizzato(boolean value) {
        this.storicizzato = value;
    }

    /**
     * Recupera il valore della proprietà attivo.
     * 
     */
    public boolean isAttivo() {
        return attivo;
    }

    /**
     * Imposta il valore della proprietà attivo.
     * 
     */
    public void setAttivo(boolean value) {
        this.attivo = value;
    }

    /**
     * Recupera il valore della proprietà sistemaProduttore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaProduttore() {
        return sistemaProduttore;
    }

    /**
     * Imposta il valore della proprietà sistemaProduttore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaProduttore(String value) {
        this.sistemaProduttore = value;
    }

}
