
package fepa.types.v3;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiestacollegamento fascicoli
 * 
 * <p>Classe Java per richiesta_associateFascicolo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_associateFascicolo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicolo" type="{urn:fepa:types:v3}Guid"/>
 *         &lt;element name="TipoFascicolo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Associazioni" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="IdFascicoloDaAssociare" type="{urn:fepa:types:v3}Guid"/>
 *                   &lt;element name="TipoFascicoloDaAssociare" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="TipoRelazione" type="{urn:fepa:types:v3}relazioneFascicolo_type"/>
 *                   &lt;element name="Condivisibile" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_associateFascicolo_type", propOrder = {
    "idFascicolo",
    "tipoFascicolo",
    "associazioni"
})
public class RichiestaAssociateFascicoloType
    implements Serializable
{

    @XmlElement(name = "IdFascicolo", required = true)
    protected String idFascicolo;
    @XmlElement(name = "TipoFascicolo", required = true)
    protected String tipoFascicolo;
    @XmlElement(name = "Associazioni", required = true)
    protected List<RichiestaAssociateFascicoloType.Associazioni> associazioni;

    /**
     * Recupera il valore della proprietà idFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicolo() {
        return idFascicolo;
    }

    /**
     * Imposta il valore della proprietà idFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicolo(String value) {
        this.idFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà tipoFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoFascicolo() {
        return tipoFascicolo;
    }

    /**
     * Imposta il valore della proprietà tipoFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoFascicolo(String value) {
        this.tipoFascicolo = value;
    }

    /**
     * Gets the value of the associazioni property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the associazioni property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssociazioni().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RichiestaAssociateFascicoloType.Associazioni }
     * 
     * 
     */
    public List<RichiestaAssociateFascicoloType.Associazioni> getAssociazioni() {
        if (associazioni == null) {
            associazioni = new ArrayList<RichiestaAssociateFascicoloType.Associazioni>();
        }
        return this.associazioni;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="IdFascicoloDaAssociare" type="{urn:fepa:types:v3}Guid"/>
     *         &lt;element name="TipoFascicoloDaAssociare" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="TipoRelazione" type="{urn:fepa:types:v3}relazioneFascicolo_type"/>
     *         &lt;element name="Condivisibile" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idFascicoloDaAssociare",
        "tipoFascicoloDaAssociare",
        "tipoRelazione",
        "condivisibile"
    })
    public static class Associazioni
        implements Serializable
    {

        @XmlElement(name = "IdFascicoloDaAssociare", required = true)
        protected String idFascicoloDaAssociare;
        @XmlElement(name = "TipoFascicoloDaAssociare", required = true)
        protected String tipoFascicoloDaAssociare;
        @XmlElement(name = "TipoRelazione", required = true)
        @XmlSchemaType(name = "string")
        protected RelazioneFascicoloType tipoRelazione;
        @XmlElement(name = "Condivisibile", defaultValue = "true")
        protected boolean condivisibile;

        /**
         * Recupera il valore della proprietà idFascicoloDaAssociare.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdFascicoloDaAssociare() {
            return idFascicoloDaAssociare;
        }

        /**
         * Imposta il valore della proprietà idFascicoloDaAssociare.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdFascicoloDaAssociare(String value) {
            this.idFascicoloDaAssociare = value;
        }

        /**
         * Recupera il valore della proprietà tipoFascicoloDaAssociare.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTipoFascicoloDaAssociare() {
            return tipoFascicoloDaAssociare;
        }

        /**
         * Imposta il valore della proprietà tipoFascicoloDaAssociare.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTipoFascicoloDaAssociare(String value) {
            this.tipoFascicoloDaAssociare = value;
        }

        /**
         * Recupera il valore della proprietà tipoRelazione.
         * 
         * @return
         *     possible object is
         *     {@link RelazioneFascicoloType }
         *     
         */
        public RelazioneFascicoloType getTipoRelazione() {
            return tipoRelazione;
        }

        /**
         * Imposta il valore della proprietà tipoRelazione.
         * 
         * @param value
         *     allowed object is
         *     {@link RelazioneFascicoloType }
         *     
         */
        public void setTipoRelazione(RelazioneFascicoloType value) {
            this.tipoRelazione = value;
        }

        /**
         * Recupera il valore della proprietà condivisibile.
         * 
         */
        public boolean isCondivisibile() {
            return condivisibile;
        }

        /**
         * Imposta il valore della proprietà condivisibile.
         * 
         */
        public void setCondivisibile(boolean value) {
            this.condivisibile = value;
        }

    }

}
