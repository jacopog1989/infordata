
package fepa.types.v3;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import fepa.header.v3.BaseServiceResponseType;


/**
 * Content del Documento del Fascicolo
 * 
 * <p>Classe Java per risposta_downloadDocumentoFascicoloByTypeDoc_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_downloadDocumentoFascicoloByTypeDoc_type">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:fepa:header:v3}BaseServiceResponseType">
 *       &lt;sequence>
 *         &lt;element name="DocumentoContent" type="{urn:fepa:types:v3}documentoContent_type" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_downloadDocumentoFascicoloByTypeDoc_type", propOrder = {
    "documentoContent"
})
public class RispostaDownloadDocumentoFascicoloByTypeDocType
    extends BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "DocumentoContent")
    protected List<DocumentoContentType> documentoContent;

    /**
     * Gets the value of the documentoContent property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documentoContent property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocumentoContent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentoContentType }
     * 
     * 
     */
    public List<DocumentoContentType> getDocumentoContent() {
        if (documentoContent == null) {
            documentoContent = new ArrayList<DocumentoContentType>();
        }
        return this.documentoContent;
    }

}
