
package fepa.types.v3;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per tipoConversione_type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoConversione_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PDF"/>
 *     &lt;enumeration value="PDF1A"/>
 *     &lt;enumeration value="PDF1B"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoConversione_type")
@XmlEnum
public enum TipoConversioneType {

    PDF("PDF"),
    @XmlEnumValue("PDF1A")
    PDF_1_A("PDF1A"),
    @XmlEnumValue("PDF1B")
    PDF_1_B("PDF1B");
    private final String value;

    TipoConversioneType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TipoConversioneType fromValue(String v) {
        for (TipoConversioneType c: TipoConversioneType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
