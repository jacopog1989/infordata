
package fepa.types.v3;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiesta Conversione documento sincrona
 * 
 * <p>Classe Java per richiesta_convertDocumento_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_convertDocumento_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoConversione" type="{urn:fepa:types:v3}tipoConversione_type"/>
 *         &lt;element name="DatiDocumento" type="{urn:fepa:types:v3}conversioneContent_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_convertDocumento_type", propOrder = {
    "tipoConversione",
    "datiDocumento"
})
public class RichiestaConvertDocumentoType
    implements Serializable
{

    @XmlElement(name = "TipoConversione", required = true)
    @XmlSchemaType(name = "string")
    protected TipoConversioneType tipoConversione;
    @XmlElement(name = "DatiDocumento", required = true)
    protected ConversioneContentType datiDocumento;

    /**
     * Recupera il valore della proprietà tipoConversione.
     * 
     * @return
     *     possible object is
     *     {@link TipoConversioneType }
     *     
     */
    public TipoConversioneType getTipoConversione() {
        return tipoConversione;
    }

    /**
     * Imposta il valore della proprietà tipoConversione.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoConversioneType }
     *     
     */
    public void setTipoConversione(TipoConversioneType value) {
        this.tipoConversione = value;
    }

    /**
     * Recupera il valore della proprietà datiDocumento.
     * 
     * @return
     *     possible object is
     *     {@link ConversioneContentType }
     *     
     */
    public ConversioneContentType getDatiDocumento() {
        return datiDocumento;
    }

    /**
     * Imposta il valore della proprietà datiDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link ConversioneContentType }
     *     
     */
    public void setDatiDocumento(ConversioneContentType value) {
        this.datiDocumento = value;
    }

}
