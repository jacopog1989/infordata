
package fepa.types.v3;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Tipologia dei documenti presenti nel fascicolo
 * 
 * <p>Classe Java per documentoSearch_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="documentoSearch_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicolo" type="{urn:fepa:types:v3}Guid"/>
 *         &lt;element name="TipoFascicolo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DescrizioneFascicolo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdDocumento" type="{urn:fepa:types:v3}Guid"/>
 *         &lt;element name="TipoDocumento" type="{urn:fepa:types:v3}tipoDocumento_type"/>
 *         &lt;element name="DescrizioneDocumento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DataCreazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="FileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Hash" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *         &lt;element name="MimeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Length" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="Firmato" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SistemaProduttore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PreviewPng" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "documentoSearch_type", propOrder = {
    "idFascicolo",
    "tipoFascicolo",
    "descrizioneFascicolo",
    "idDocumento",
    "tipoDocumento",
    "descrizioneDocumento",
    "dataCreazione",
    "fileName",
    "hash",
    "mimeType",
    "length",
    "firmato",
    "sistemaProduttore",
    "previewPng"
})
public class DocumentoSearchType
    implements Serializable
{

    @XmlElement(name = "IdFascicolo", required = true)
    protected String idFascicolo;
    @XmlElement(name = "TipoFascicolo", required = true)
    protected String tipoFascicolo;
    @XmlElement(name = "DescrizioneFascicolo", required = true)
    protected String descrizioneFascicolo;
    @XmlElement(name = "IdDocumento", required = true)
    protected String idDocumento;
    @XmlElement(name = "TipoDocumento", required = true)
    protected TipoDocumentoType tipoDocumento;
    @XmlElement(name = "DescrizioneDocumento", required = true)
    protected String descrizioneDocumento;
    @XmlElement(name = "DataCreazione", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataCreazione;
    @XmlElement(name = "FileName", required = true, nillable = true)
    protected String fileName;
    @XmlElement(name = "Hash", required = true, nillable = true)
    protected byte[] hash;
    @XmlElement(name = "MimeType", required = true)
    protected String mimeType;
    @XmlElement(name = "Length")
    protected long length;
    @XmlElement(name = "Firmato")
    protected boolean firmato;
    @XmlElement(name = "SistemaProduttore", required = true)
    protected String sistemaProduttore;
    @XmlElement(name = "PreviewPng")
    protected byte[] previewPng;

    /**
     * Recupera il valore della proprietà idFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicolo() {
        return idFascicolo;
    }

    /**
     * Imposta il valore della proprietà idFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicolo(String value) {
        this.idFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà tipoFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoFascicolo() {
        return tipoFascicolo;
    }

    /**
     * Imposta il valore della proprietà tipoFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoFascicolo(String value) {
        this.tipoFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà descrizioneFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizioneFascicolo() {
        return descrizioneFascicolo;
    }

    /**
     * Imposta il valore della proprietà descrizioneFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizioneFascicolo(String value) {
        this.descrizioneFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà idDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumento() {
        return idDocumento;
    }

    /**
     * Imposta il valore della proprietà idDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumento(String value) {
        this.idDocumento = value;
    }

    /**
     * Recupera il valore della proprietà tipoDocumento.
     * 
     * @return
     *     possible object is
     *     {@link TipoDocumentoType }
     *     
     */
    public TipoDocumentoType getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Imposta il valore della proprietà tipoDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDocumentoType }
     *     
     */
    public void setTipoDocumento(TipoDocumentoType value) {
        this.tipoDocumento = value;
    }

    /**
     * Recupera il valore della proprietà descrizioneDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizioneDocumento() {
        return descrizioneDocumento;
    }

    /**
     * Imposta il valore della proprietà descrizioneDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizioneDocumento(String value) {
        this.descrizioneDocumento = value;
    }

    /**
     * Recupera il valore della proprietà dataCreazione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataCreazione() {
        return dataCreazione;
    }

    /**
     * Imposta il valore della proprietà dataCreazione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataCreazione(XMLGregorianCalendar value) {
        this.dataCreazione = value;
    }

    /**
     * Recupera il valore della proprietà fileName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Imposta il valore della proprietà fileName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileName(String value) {
        this.fileName = value;
    }

    /**
     * Recupera il valore della proprietà hash.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getHash() {
        return hash;
    }

    /**
     * Imposta il valore della proprietà hash.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setHash(byte[] value) {
        this.hash = value;
    }

    /**
     * Recupera il valore della proprietà mimeType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMimeType() {
        return mimeType;
    }

    /**
     * Imposta il valore della proprietà mimeType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMimeType(String value) {
        this.mimeType = value;
    }

    /**
     * Recupera il valore della proprietà length.
     * 
     */
    public long getLength() {
        return length;
    }

    /**
     * Imposta il valore della proprietà length.
     * 
     */
    public void setLength(long value) {
        this.length = value;
    }

    /**
     * Recupera il valore della proprietà firmato.
     * 
     */
    public boolean isFirmato() {
        return firmato;
    }

    /**
     * Imposta il valore della proprietà firmato.
     * 
     */
    public void setFirmato(boolean value) {
        this.firmato = value;
    }

    /**
     * Recupera il valore della proprietà sistemaProduttore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaProduttore() {
        return sistemaProduttore;
    }

    /**
     * Imposta il valore della proprietà sistemaProduttore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaProduttore(String value) {
        this.sistemaProduttore = value;
    }

    /**
     * Recupera il valore della proprietà previewPng.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getPreviewPng() {
        return previewPng;
    }

    /**
     * Imposta il valore della proprietà previewPng.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setPreviewPng(byte[] value) {
        this.previewPng = value;
    }

}
