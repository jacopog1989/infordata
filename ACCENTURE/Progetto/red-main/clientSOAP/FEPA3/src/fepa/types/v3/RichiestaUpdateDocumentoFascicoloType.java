
package fepa.types.v3;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Cambio di Stato del documento
 * 
 * <p>Classe Java per richiesta_updateDocumentoFascicolo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_updateDocumentoFascicolo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicolo" type="{urn:fepa:types:v3}Guid"/>
 *         &lt;element name="TipoFascicolo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdDocumento" type="{urn:fepa:types:v3}Guid"/>
 *         &lt;element name="OrgIdProvenienza" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DocumentoMetadata" type="{urn:fepa:types:v3}documentoMetadata_type" minOccurs="0"/>
 *         &lt;element name="DaInviare" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Condivisibile" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Attivo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_updateDocumentoFascicolo_type", propOrder = {
    "idFascicolo",
    "tipoFascicolo",
    "idDocumento",
    "orgIdProvenienza",
    "documentoMetadata",
    "daInviare",
    "condivisibile",
    "attivo"
})
public class RichiestaUpdateDocumentoFascicoloType
    implements Serializable
{

    @XmlElement(name = "IdFascicolo", required = true)
    protected String idFascicolo;
    @XmlElement(name = "TipoFascicolo", required = true)
    protected String tipoFascicolo;
    @XmlElement(name = "IdDocumento", required = true)
    protected String idDocumento;
    @XmlElement(name = "OrgIdProvenienza")
    protected String orgIdProvenienza;
    @XmlElement(name = "DocumentoMetadata")
    protected DocumentoMetadataType documentoMetadata;
    @XmlElement(name = "DaInviare", defaultValue = "true")
    protected Boolean daInviare;
    @XmlElement(name = "Condivisibile", defaultValue = "true")
    protected Boolean condivisibile;
    @XmlElement(name = "Attivo", defaultValue = "true")
    protected Boolean attivo;

    /**
     * Recupera il valore della proprietà idFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicolo() {
        return idFascicolo;
    }

    /**
     * Imposta il valore della proprietà idFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicolo(String value) {
        this.idFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà tipoFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoFascicolo() {
        return tipoFascicolo;
    }

    /**
     * Imposta il valore della proprietà tipoFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoFascicolo(String value) {
        this.tipoFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà idDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumento() {
        return idDocumento;
    }

    /**
     * Imposta il valore della proprietà idDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumento(String value) {
        this.idDocumento = value;
    }

    /**
     * Recupera il valore della proprietà orgIdProvenienza.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrgIdProvenienza() {
        return orgIdProvenienza;
    }

    /**
     * Imposta il valore della proprietà orgIdProvenienza.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrgIdProvenienza(String value) {
        this.orgIdProvenienza = value;
    }

    /**
     * Recupera il valore della proprietà documentoMetadata.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoMetadataType }
     *     
     */
    public DocumentoMetadataType getDocumentoMetadata() {
        return documentoMetadata;
    }

    /**
     * Imposta il valore della proprietà documentoMetadata.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoMetadataType }
     *     
     */
    public void setDocumentoMetadata(DocumentoMetadataType value) {
        this.documentoMetadata = value;
    }

    /**
     * Recupera il valore della proprietà daInviare.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDaInviare() {
        return daInviare;
    }

    /**
     * Imposta il valore della proprietà daInviare.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDaInviare(Boolean value) {
        this.daInviare = value;
    }

    /**
     * Recupera il valore della proprietà condivisibile.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isCondivisibile() {
        return condivisibile;
    }

    /**
     * Imposta il valore della proprietà condivisibile.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setCondivisibile(Boolean value) {
        this.condivisibile = value;
    }

    /**
     * Recupera il valore della proprietà attivo.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isAttivo() {
        return attivo;
    }

    /**
     * Imposta il valore della proprietà attivo.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setAttivo(Boolean value) {
        this.attivo = value;
    }

}
