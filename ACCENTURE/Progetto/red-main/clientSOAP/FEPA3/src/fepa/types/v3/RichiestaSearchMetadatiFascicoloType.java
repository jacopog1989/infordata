
package fepa.types.v3;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Ricerca i metadati di una tipologia di fascicolo
 * 
 * <p>Classe Java per richiesta_searchMetadatiFascicolo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_searchMetadatiFascicolo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoFascicolo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DataCreazione" type="{urn:fepa:types:v3}dataRangeType" minOccurs="0"/>
 *         &lt;element name="DataAggiornamento" type="{urn:fepa:types:v3}dataRangeType" minOccurs="0"/>
 *         &lt;element name="StatoFascicoloDocumentale" type="{urn:fepa:types:v3}statoFascicoloDocumentale_type" minOccurs="0"/>
 *         &lt;element name="Matadati">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence maxOccurs="unbounded">
 *                   &lt;element name="Key" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="VersioneMetadata" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Attivo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SistemaProduttore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_searchMetadatiFascicolo_type", propOrder = {
    "tipoFascicolo",
    "descrizione",
    "dataCreazione",
    "dataAggiornamento",
    "statoFascicoloDocumentale",
    "matadati",
    "versioneMetadata",
    "attivo",
    "sistemaProduttore"
})
public class RichiestaSearchMetadatiFascicoloType
    implements Serializable
{

    @XmlElement(name = "TipoFascicolo", required = true)
    protected String tipoFascicolo;
    @XmlElement(name = "Descrizione", required = true)
    protected String descrizione;
    @XmlElement(name = "DataCreazione")
    protected DataRangeType dataCreazione;
    @XmlElement(name = "DataAggiornamento")
    protected DataRangeType dataAggiornamento;
    @XmlElement(name = "StatoFascicoloDocumentale")
    @XmlSchemaType(name = "string")
    protected StatoFascicoloDocumentaleType statoFascicoloDocumentale;
    @XmlElement(name = "Matadati", required = true)
    protected RichiestaSearchMetadatiFascicoloType.Matadati matadati;
    @XmlElement(name = "VersioneMetadata", required = true)
    protected String versioneMetadata;
    @XmlElement(name = "Attivo")
    protected boolean attivo;
    @XmlElement(name = "SistemaProduttore", required = true)
    protected String sistemaProduttore;

    /**
     * Recupera il valore della proprietà tipoFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoFascicolo() {
        return tipoFascicolo;
    }

    /**
     * Imposta il valore della proprietà tipoFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoFascicolo(String value) {
        this.tipoFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà descrizione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Imposta il valore della proprietà descrizione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Recupera il valore della proprietà dataCreazione.
     * 
     * @return
     *     possible object is
     *     {@link DataRangeType }
     *     
     */
    public DataRangeType getDataCreazione() {
        return dataCreazione;
    }

    /**
     * Imposta il valore della proprietà dataCreazione.
     * 
     * @param value
     *     allowed object is
     *     {@link DataRangeType }
     *     
     */
    public void setDataCreazione(DataRangeType value) {
        this.dataCreazione = value;
    }

    /**
     * Recupera il valore della proprietà dataAggiornamento.
     * 
     * @return
     *     possible object is
     *     {@link DataRangeType }
     *     
     */
    public DataRangeType getDataAggiornamento() {
        return dataAggiornamento;
    }

    /**
     * Imposta il valore della proprietà dataAggiornamento.
     * 
     * @param value
     *     allowed object is
     *     {@link DataRangeType }
     *     
     */
    public void setDataAggiornamento(DataRangeType value) {
        this.dataAggiornamento = value;
    }

    /**
     * Recupera il valore della proprietà statoFascicoloDocumentale.
     * 
     * @return
     *     possible object is
     *     {@link StatoFascicoloDocumentaleType }
     *     
     */
    public StatoFascicoloDocumentaleType getStatoFascicoloDocumentale() {
        return statoFascicoloDocumentale;
    }

    /**
     * Imposta il valore della proprietà statoFascicoloDocumentale.
     * 
     * @param value
     *     allowed object is
     *     {@link StatoFascicoloDocumentaleType }
     *     
     */
    public void setStatoFascicoloDocumentale(StatoFascicoloDocumentaleType value) {
        this.statoFascicoloDocumentale = value;
    }

    /**
     * Recupera il valore della proprietà matadati.
     * 
     * @return
     *     possible object is
     *     {@link RichiestaSearchMetadatiFascicoloType.Matadati }
     *     
     */
    public RichiestaSearchMetadatiFascicoloType.Matadati getMatadati() {
        return matadati;
    }

    /**
     * Imposta il valore della proprietà matadati.
     * 
     * @param value
     *     allowed object is
     *     {@link RichiestaSearchMetadatiFascicoloType.Matadati }
     *     
     */
    public void setMatadati(RichiestaSearchMetadatiFascicoloType.Matadati value) {
        this.matadati = value;
    }

    /**
     * Recupera il valore della proprietà versioneMetadata.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersioneMetadata() {
        return versioneMetadata;
    }

    /**
     * Imposta il valore della proprietà versioneMetadata.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersioneMetadata(String value) {
        this.versioneMetadata = value;
    }

    /**
     * Recupera il valore della proprietà attivo.
     * 
     */
    public boolean isAttivo() {
        return attivo;
    }

    /**
     * Imposta il valore della proprietà attivo.
     * 
     */
    public void setAttivo(boolean value) {
        this.attivo = value;
    }

    /**
     * Recupera il valore della proprietà sistemaProduttore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaProduttore() {
        return sistemaProduttore;
    }

    /**
     * Imposta il valore della proprietà sistemaProduttore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaProduttore(String value) {
        this.sistemaProduttore = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence maxOccurs="unbounded">
     *         &lt;element name="Key" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "keyAndValue"
    })
    public static class Matadati
        implements Serializable
    {

        @XmlElementRefs({
            @XmlElementRef(name = "Key", type = JAXBElement.class),
            @XmlElementRef(name = "Value", type = JAXBElement.class)
        })
        protected List<JAXBElement<Object>> keyAndValue;

        /**
         * Gets the value of the keyAndValue property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the keyAndValue property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getKeyAndValue().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link JAXBElement }{@code <}{@link Object }{@code >}
         * {@link JAXBElement }{@code <}{@link Object }{@code >}
         * 
         * 
         */
        public List<JAXBElement<Object>> getKeyAndValue() {
            if (keyAndValue == null) {
                keyAndValue = new ArrayList<JAXBElement<Object>>();
            }
            return this.keyAndValue;
        }

    }

}
