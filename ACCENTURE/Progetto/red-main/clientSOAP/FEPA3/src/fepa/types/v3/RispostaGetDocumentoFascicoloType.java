
package fepa.types.v3;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import fepa.header.v3.BaseServiceResponseType;


/**
 * Dati del Documento di un fascicolo
 * 
 * <p>Classe Java per risposta_getDocumentoFascicolo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_getDocumentoFascicolo_type">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:fepa:header:v3}BaseServiceResponseType">
 *       &lt;sequence>
 *         &lt;element name="DettaglioDocumento" type="{urn:fepa:types:v3}documentoFascicolo_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_getDocumentoFascicolo_type", propOrder = {
    "dettaglioDocumento"
})
public class RispostaGetDocumentoFascicoloType
    extends BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "DettaglioDocumento")
    protected DocumentoFascicoloType dettaglioDocumento;

    /**
     * Recupera il valore della proprietà dettaglioDocumento.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoFascicoloType }
     *     
     */
    public DocumentoFascicoloType getDettaglioDocumento() {
        return dettaglioDocumento;
    }

    /**
     * Imposta il valore della proprietà dettaglioDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoFascicoloType }
     *     
     */
    public void setDettaglioDocumento(DocumentoFascicoloType value) {
        this.dettaglioDocumento = value;
    }

}
