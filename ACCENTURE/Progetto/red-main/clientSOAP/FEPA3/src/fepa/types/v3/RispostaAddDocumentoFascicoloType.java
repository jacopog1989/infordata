
package fepa.types.v3;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import fepa.header.v3.BaseServiceResponseType;
import fepa.messages.v3.RispostaAddDocumentoFascicolo;


/**
 * Aggiunta documento al fascicolo, il tipo del documento aggiunto è O002 Documento generico
 * 
 * <p>Classe Java per risposta_addDocumentoFascicolo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_addDocumentoFascicolo_type">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:fepa:header:v3}BaseServiceResponseType">
 *       &lt;sequence>
 *         &lt;element name="DatiDocumento" type="{urn:fepa:types:v3}documentoFile_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_addDocumentoFascicolo_type", propOrder = {
    "datiDocumento"
})
@XmlSeeAlso({
    RispostaAddDocumentoFascicolo.class
})
public class RispostaAddDocumentoFascicoloType
    extends BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "DatiDocumento")
    protected DocumentoFileType datiDocumento;

    /**
     * Recupera il valore della proprietà datiDocumento.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoFileType }
     *     
     */
    public DocumentoFileType getDatiDocumento() {
        return datiDocumento;
    }

    /**
     * Imposta il valore della proprietà datiDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoFileType }
     *     
     */
    public void setDatiDocumento(DocumentoFileType value) {
        this.datiDocumento = value;
    }

}
