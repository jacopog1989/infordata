
package fepa.types.v3;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Identificativo di dell'associazione di una UO per applicazione
 * 
 * <p>Classe Java per IdentificativoAssociazioneApplicazioneType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="IdentificativoAssociazioneApplicazioneType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Applicazione" type="{urn:fepa:types:v3}IdentificativoApplicazioneType"/>
 *         &lt;element name="TipoCollegamento" type="{urn:fepa:types:v3}collegamentoFascicolo_type"/>
 *         &lt;element name="Condivisibile" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentificativoAssociazioneApplicazioneType", propOrder = {
    "applicazione",
    "tipoCollegamento",
    "condivisibile"
})
public class IdentificativoAssociazioneApplicazioneType
    implements Serializable
{

    @XmlElement(name = "Applicazione", required = true)
    protected String applicazione;
    @XmlElement(name = "TipoCollegamento", required = true, defaultValue = "Sincronizzato")
    @XmlSchemaType(name = "string")
    protected CollegamentoFascicoloType tipoCollegamento;
    @XmlElement(name = "Condivisibile", defaultValue = "true")
    protected boolean condivisibile;

    /**
     * Recupera il valore della proprietà applicazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicazione() {
        return applicazione;
    }

    /**
     * Imposta il valore della proprietà applicazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicazione(String value) {
        this.applicazione = value;
    }

    /**
     * Recupera il valore della proprietà tipoCollegamento.
     * 
     * @return
     *     possible object is
     *     {@link CollegamentoFascicoloType }
     *     
     */
    public CollegamentoFascicoloType getTipoCollegamento() {
        return tipoCollegamento;
    }

    /**
     * Imposta il valore della proprietà tipoCollegamento.
     * 
     * @param value
     *     allowed object is
     *     {@link CollegamentoFascicoloType }
     *     
     */
    public void setTipoCollegamento(CollegamentoFascicoloType value) {
        this.tipoCollegamento = value;
    }

    /**
     * Recupera il valore della proprietà condivisibile.
     * 
     */
    public boolean isCondivisibile() {
        return condivisibile;
    }

    /**
     * Imposta il valore della proprietà condivisibile.
     * 
     */
    public void setCondivisibile(boolean value) {
        this.condivisibile = value;
    }

}
