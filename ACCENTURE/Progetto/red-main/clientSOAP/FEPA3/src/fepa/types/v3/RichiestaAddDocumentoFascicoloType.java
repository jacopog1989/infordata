
package fepa.types.v3;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Aggiunta documento al fascicolo, il tipo del documento aggiunto è O002 Documento generico
 * 
 * <p>Classe Java per richiesta_addDocumentoFascicolo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_addDocumentoFascicolo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicolo" type="{urn:fepa:types:v3}Guid"/>
 *         &lt;element name="TipoFascicolo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;choice>
 *           &lt;element name="Documento">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="IdDocumento" type="{urn:fepa:types:v3}Guid"/>
 *                     &lt;element name="DocumentoContent" type="{urn:fepa:types:v3}documentoContent_type"/>
 *                     &lt;element name="Operazione" type="{urn:fepa:types:v3}richiestaOperazioneDocumentale_type" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="DocumentoEsistente">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="IdDocumento" type="{urn:fepa:types:v3}Guid"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *         &lt;element name="TipoDocumento" type="{urn:fepa:types:v3}tipoDocumento_type"/>
 *         &lt;element name="DatiDocumento" type="{urn:fepa:types:v3}documentoMetadata_type"/>
 *         &lt;element name="DaInviare" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Condivisibile" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Attivo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_addDocumentoFascicolo_type", propOrder = {
    "idFascicolo",
    "tipoFascicolo",
    "documento",
    "documentoEsistente",
    "tipoDocumento",
    "datiDocumento",
    "daInviare",
    "condivisibile",
    "attivo"
})
public class RichiestaAddDocumentoFascicoloType
    implements Serializable
{

    @XmlElement(name = "IdFascicolo", required = true)
    protected String idFascicolo;
    @XmlElement(name = "TipoFascicolo", required = true)
    protected String tipoFascicolo;
    @XmlElement(name = "Documento")
    protected RichiestaAddDocumentoFascicoloType.Documento documento;
    @XmlElement(name = "DocumentoEsistente")
    protected RichiestaAddDocumentoFascicoloType.DocumentoEsistente documentoEsistente;
    @XmlElement(name = "TipoDocumento", required = true)
    protected TipoDocumentoType tipoDocumento;
    @XmlElement(name = "DatiDocumento", required = true)
    protected DocumentoMetadataType datiDocumento;
    @XmlElement(name = "DaInviare", defaultValue = "true")
    protected boolean daInviare;
    @XmlElement(name = "Condivisibile", defaultValue = "true")
    protected boolean condivisibile;
    @XmlElement(name = "Attivo", defaultValue = "true")
    protected boolean attivo;

    /**
     * Recupera il valore della proprietà idFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicolo() {
        return idFascicolo;
    }

    /**
     * Imposta il valore della proprietà idFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicolo(String value) {
        this.idFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà tipoFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoFascicolo() {
        return tipoFascicolo;
    }

    /**
     * Imposta il valore della proprietà tipoFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoFascicolo(String value) {
        this.tipoFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà documento.
     * 
     * @return
     *     possible object is
     *     {@link RichiestaAddDocumentoFascicoloType.Documento }
     *     
     */
    public RichiestaAddDocumentoFascicoloType.Documento getDocumento() {
        return documento;
    }

    /**
     * Imposta il valore della proprietà documento.
     * 
     * @param value
     *     allowed object is
     *     {@link RichiestaAddDocumentoFascicoloType.Documento }
     *     
     */
    public void setDocumento(RichiestaAddDocumentoFascicoloType.Documento value) {
        this.documento = value;
    }

    /**
     * Recupera il valore della proprietà documentoEsistente.
     * 
     * @return
     *     possible object is
     *     {@link RichiestaAddDocumentoFascicoloType.DocumentoEsistente }
     *     
     */
    public RichiestaAddDocumentoFascicoloType.DocumentoEsistente getDocumentoEsistente() {
        return documentoEsistente;
    }

    /**
     * Imposta il valore della proprietà documentoEsistente.
     * 
     * @param value
     *     allowed object is
     *     {@link RichiestaAddDocumentoFascicoloType.DocumentoEsistente }
     *     
     */
    public void setDocumentoEsistente(RichiestaAddDocumentoFascicoloType.DocumentoEsistente value) {
        this.documentoEsistente = value;
    }

    /**
     * Recupera il valore della proprietà tipoDocumento.
     * 
     * @return
     *     possible object is
     *     {@link TipoDocumentoType }
     *     
     */
    public TipoDocumentoType getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Imposta il valore della proprietà tipoDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDocumentoType }
     *     
     */
    public void setTipoDocumento(TipoDocumentoType value) {
        this.tipoDocumento = value;
    }

    /**
     * Recupera il valore della proprietà datiDocumento.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoMetadataType }
     *     
     */
    public DocumentoMetadataType getDatiDocumento() {
        return datiDocumento;
    }

    /**
     * Imposta il valore della proprietà datiDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoMetadataType }
     *     
     */
    public void setDatiDocumento(DocumentoMetadataType value) {
        this.datiDocumento = value;
    }

    /**
     * Recupera il valore della proprietà daInviare.
     * 
     */
    public boolean isDaInviare() {
        return daInviare;
    }

    /**
     * Imposta il valore della proprietà daInviare.
     * 
     */
    public void setDaInviare(boolean value) {
        this.daInviare = value;
    }

    /**
     * Recupera il valore della proprietà condivisibile.
     * 
     */
    public boolean isCondivisibile() {
        return condivisibile;
    }

    /**
     * Imposta il valore della proprietà condivisibile.
     * 
     */
    public void setCondivisibile(boolean value) {
        this.condivisibile = value;
    }

    /**
     * Recupera il valore della proprietà attivo.
     * 
     */
    public boolean isAttivo() {
        return attivo;
    }

    /**
     * Imposta il valore della proprietà attivo.
     * 
     */
    public void setAttivo(boolean value) {
        this.attivo = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="IdDocumento" type="{urn:fepa:types:v3}Guid"/>
     *         &lt;element name="DocumentoContent" type="{urn:fepa:types:v3}documentoContent_type"/>
     *         &lt;element name="Operazione" type="{urn:fepa:types:v3}richiestaOperazioneDocumentale_type" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idDocumento",
        "documentoContent",
        "operazione"
    })
    public static class Documento
        implements Serializable
    {

        @XmlElement(name = "IdDocumento", required = true, nillable = true)
        protected String idDocumento;
        @XmlElement(name = "DocumentoContent", required = true)
        protected DocumentoContentType documentoContent;
        @XmlElement(name = "Operazione")
        protected RichiestaOperazioneDocumentaleType operazione;

        /**
         * Recupera il valore della proprietà idDocumento.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocumento() {
            return idDocumento;
        }

        /**
         * Imposta il valore della proprietà idDocumento.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocumento(String value) {
            this.idDocumento = value;
        }

        /**
         * Recupera il valore della proprietà documentoContent.
         * 
         * @return
         *     possible object is
         *     {@link DocumentoContentType }
         *     
         */
        public DocumentoContentType getDocumentoContent() {
            return documentoContent;
        }

        /**
         * Imposta il valore della proprietà documentoContent.
         * 
         * @param value
         *     allowed object is
         *     {@link DocumentoContentType }
         *     
         */
        public void setDocumentoContent(DocumentoContentType value) {
            this.documentoContent = value;
        }

        /**
         * Recupera il valore della proprietà operazione.
         * 
         * @return
         *     possible object is
         *     {@link RichiestaOperazioneDocumentaleType }
         *     
         */
        public RichiestaOperazioneDocumentaleType getOperazione() {
            return operazione;
        }

        /**
         * Imposta il valore della proprietà operazione.
         * 
         * @param value
         *     allowed object is
         *     {@link RichiestaOperazioneDocumentaleType }
         *     
         */
        public void setOperazione(RichiestaOperazioneDocumentaleType value) {
            this.operazione = value;
        }

    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="IdDocumento" type="{urn:fepa:types:v3}Guid"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idDocumento"
    })
    public static class DocumentoEsistente
        implements Serializable
    {

        @XmlElement(name = "IdDocumento", required = true)
        protected String idDocumento;

        /**
         * Recupera il valore della proprietà idDocumento.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocumento() {
            return idDocumento;
        }

        /**
         * Imposta il valore della proprietà idDocumento.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocumento(String value) {
            this.idDocumento = value;
        }

    }

}
