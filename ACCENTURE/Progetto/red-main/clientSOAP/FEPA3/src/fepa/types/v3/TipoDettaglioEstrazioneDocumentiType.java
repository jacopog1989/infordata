
package fepa.types.v3;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per tipoDettaglioEstrazioneDocumenti_type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="tipoDettaglioEstrazioneDocumenti_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DATI_MINIMI"/>
 *     &lt;enumeration value="DOCUMENTI"/>
 *     &lt;enumeration value="DOCUMENTI_PREVIEW"/>
 *     &lt;enumeration value="DOCUMENTI_OPERAZIONI"/>
 *     &lt;enumeration value="METADATI_DOCUMENTO"/>
 *     &lt;enumeration value="PREVIEW_DOCUMENTO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "tipoDettaglioEstrazioneDocumenti_type")
@XmlEnum
public enum TipoDettaglioEstrazioneDocumentiType {

    DATI_MINIMI,
    DOCUMENTI,
    DOCUMENTI_PREVIEW,
    DOCUMENTI_OPERAZIONI,
    METADATI_DOCUMENTO,
    PREVIEW_DOCUMENTO;

    public String value() {
        return name();
    }

    public static TipoDettaglioEstrazioneDocumentiType fromValue(String v) {
        return valueOf(v);
    }

}
