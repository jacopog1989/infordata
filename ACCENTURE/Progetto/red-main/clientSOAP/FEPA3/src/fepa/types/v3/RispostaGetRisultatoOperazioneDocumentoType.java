
package fepa.types.v3;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import fepa.header.v3.BaseServiceResponseType;


/**
 * Restituisce il content del documento del Fascicolo Raccolta Provvisoria
 * 
 * <p>Classe Java per risposta_getRisultatoOperazioneDocumento_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_getRisultatoOperazioneDocumento_type">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:fepa:header:v3}BaseServiceResponseType">
 *       &lt;sequence>
 *         &lt;element name="Operazioni" type="{urn:fepa:types:v3}risultatoOperazioneDocumento_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_getRisultatoOperazioneDocumento_type", propOrder = {
    "operazioni"
})
public class RispostaGetRisultatoOperazioneDocumentoType
    extends BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "Operazioni")
    protected RisultatoOperazioneDocumentoType operazioni;

    /**
     * Recupera il valore della proprietà operazioni.
     * 
     * @return
     *     possible object is
     *     {@link RisultatoOperazioneDocumentoType }
     *     
     */
    public RisultatoOperazioneDocumentoType getOperazioni() {
        return operazioni;
    }

    /**
     * Imposta il valore della proprietà operazioni.
     * 
     * @param value
     *     allowed object is
     *     {@link RisultatoOperazioneDocumentoType }
     *     
     */
    public void setOperazioni(RisultatoOperazioneDocumentoType value) {
        this.operazioni = value;
    }

}
