
package fepa.types.v3;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per statoFascicoloDocumentale_type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="statoFascicoloDocumentale_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="APERTO"/>
 *     &lt;enumeration value="CHIUSO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "statoFascicoloDocumentale_type")
@XmlEnum
public enum StatoFascicoloDocumentaleType {

    APERTO,
    CHIUSO;

    public String value() {
        return name();
    }

    public static StatoFascicoloDocumentaleType fromValue(String v) {
        return valueOf(v);
    }

}
