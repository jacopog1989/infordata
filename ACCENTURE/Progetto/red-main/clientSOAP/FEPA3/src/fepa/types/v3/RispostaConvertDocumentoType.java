
package fepa.types.v3;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import fepa.header.v3.BaseServiceResponseType;


/**
 * Content del Documento Allegato Decreto IGB
 * 
 * <p>Classe Java per risposta_convertDocumento_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_convertDocumento_type">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:fepa:header:v3}BaseServiceResponseType">
 *       &lt;sequence>
 *         &lt;element name="Document" type="{urn:fepa:types:v3}conversioneContent_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_convertDocumento_type", propOrder = {
    "document"
})
public class RispostaConvertDocumentoType
    extends BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "Document")
    protected ConversioneContentType document;

    /**
     * Recupera il valore della proprietà document.
     * 
     * @return
     *     possible object is
     *     {@link ConversioneContentType }
     *     
     */
    public ConversioneContentType getDocument() {
        return document;
    }

    /**
     * Imposta il valore della proprietà document.
     * 
     * @param value
     *     allowed object is
     *     {@link ConversioneContentType }
     *     
     */
    public void setDocument(ConversioneContentType value) {
        this.document = value;
    }

}
