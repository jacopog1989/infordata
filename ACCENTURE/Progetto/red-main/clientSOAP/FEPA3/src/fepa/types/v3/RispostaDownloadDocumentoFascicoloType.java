
package fepa.types.v3;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import fepa.header.v3.BaseServiceResponseType;
import fepa.messages.v3.RispostaDownloadDocumentoFascicolo;


/**
 * Content del Documento del Fascicolo
 * 
 * <p>Classe Java per risposta_downloadDocumentoFascicolo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_downloadDocumentoFascicolo_type">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:fepa:header:v3}BaseServiceResponseType">
 *       &lt;sequence>
 *         &lt;element name="DocumentoContent" type="{urn:fepa:types:v3}documentoContent_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_downloadDocumentoFascicolo_type", propOrder = {
    "documentoContent"
})
@XmlSeeAlso({
    RispostaDownloadDocumentoFascicolo.class
})
public class RispostaDownloadDocumentoFascicoloType
    extends BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "DocumentoContent")
    protected DocumentoContentType documentoContent;

    /**
     * Recupera il valore della proprietà documentoContent.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoContentType }
     *     
     */
    public DocumentoContentType getDocumentoContent() {
        return documentoContent;
    }

    /**
     * Imposta il valore della proprietà documentoContent.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoContentType }
     *     
     */
    public void setDocumentoContent(DocumentoContentType value) {
        this.documentoContent = value;
    }

}
