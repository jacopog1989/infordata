
package fepa.types.v3;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Tipologia dei documenti presenti nel fascicolo
 * 
 * <p>Classe Java per documentoFascicolo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="documentoFascicolo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdDocumento" type="{urn:fepa:types:v3}Guid"/>
 *         &lt;element name="TipoDocumento" type="{urn:fepa:types:v3}tipoDocumento_type"/>
 *         &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DataCreazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="MetadatiDocumento" type="{urn:fepa:types:v3}documentoMetadata_type" minOccurs="0"/>
 *         &lt;element name="FileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Hash" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *         &lt;element name="MimeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Length" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="TipoCompressione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Firmato" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Condivisibile" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Attivo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SistemaProduttore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsDocumentoOriginale" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsDocumentoTrattato" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="DocumentoOrigine" type="{urn:fepa:types:v3}documentoFile_type" minOccurs="0"/>
 *         &lt;element name="Operazione" type="{urn:fepa:types:v3}operazioneDocumentale_type" minOccurs="0"/>
 *         &lt;element name="TicketArchiviazione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PreviewPng" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "documentoFascicolo_type", propOrder = {
    "idDocumento",
    "tipoDocumento",
    "descrizione",
    "dataCreazione",
    "metadatiDocumento",
    "fileName",
    "hash",
    "mimeType",
    "length",
    "tipoCompressione",
    "firmato",
    "condivisibile",
    "attivo",
    "note",
    "sistemaProduttore",
    "isDocumentoOriginale",
    "isDocumentoTrattato",
    "documentoOrigine",
    "operazione",
    "ticketArchiviazione",
    "previewPng"
})
public class DocumentoFascicoloType
    implements Serializable
{

    @XmlElement(name = "IdDocumento", required = true)
    protected String idDocumento;
    @XmlElement(name = "TipoDocumento", required = true)
    protected TipoDocumentoType tipoDocumento;
    @XmlElement(name = "Descrizione", required = true)
    protected String descrizione;
    @XmlElement(name = "DataCreazione", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataCreazione;
    @XmlElement(name = "MetadatiDocumento")
    protected DocumentoMetadataType metadatiDocumento;
    @XmlElement(name = "FileName", required = true, nillable = true)
    protected String fileName;
    @XmlElement(name = "Hash", required = true, nillable = true)
    protected byte[] hash;
    @XmlElement(name = "MimeType", required = true)
    protected String mimeType;
    @XmlElement(name = "Length")
    protected long length;
    @XmlElement(name = "TipoCompressione")
    protected String tipoCompressione;
    @XmlElement(name = "Firmato")
    protected boolean firmato;
    @XmlElement(name = "Condivisibile", defaultValue = "true")
    protected boolean condivisibile;
    @XmlElement(name = "Attivo", defaultValue = "true")
    protected boolean attivo;
    @XmlElement(name = "Note")
    protected String note;
    @XmlElement(name = "SistemaProduttore", required = true)
    protected String sistemaProduttore;
    @XmlElement(name = "IsDocumentoOriginale", defaultValue = "true")
    protected boolean isDocumentoOriginale;
    @XmlElement(name = "IsDocumentoTrattato", defaultValue = "true")
    protected boolean isDocumentoTrattato;
    @XmlElement(name = "DocumentoOrigine")
    protected DocumentoFileType documentoOrigine;
    @XmlElement(name = "Operazione")
    protected OperazioneDocumentaleType operazione;
    @XmlElementRef(name = "TicketArchiviazione", type = JAXBElement.class, required = false)
    protected JAXBElement<String> ticketArchiviazione;
    @XmlElement(name = "PreviewPng")
    protected byte[] previewPng;

    /**
     * Recupera il valore della proprietà idDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumento() {
        return idDocumento;
    }

    /**
     * Imposta il valore della proprietà idDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumento(String value) {
        this.idDocumento = value;
    }

    /**
     * Recupera il valore della proprietà tipoDocumento.
     * 
     * @return
     *     possible object is
     *     {@link TipoDocumentoType }
     *     
     */
    public TipoDocumentoType getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Imposta il valore della proprietà tipoDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDocumentoType }
     *     
     */
    public void setTipoDocumento(TipoDocumentoType value) {
        this.tipoDocumento = value;
    }

    /**
     * Recupera il valore della proprietà descrizione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Imposta il valore della proprietà descrizione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Recupera il valore della proprietà dataCreazione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataCreazione() {
        return dataCreazione;
    }

    /**
     * Imposta il valore della proprietà dataCreazione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataCreazione(XMLGregorianCalendar value) {
        this.dataCreazione = value;
    }

    /**
     * Recupera il valore della proprietà metadatiDocumento.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoMetadataType }
     *     
     */
    public DocumentoMetadataType getMetadatiDocumento() {
        return metadatiDocumento;
    }

    /**
     * Imposta il valore della proprietà metadatiDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoMetadataType }
     *     
     */
    public void setMetadatiDocumento(DocumentoMetadataType value) {
        this.metadatiDocumento = value;
    }

    /**
     * Recupera il valore della proprietà fileName.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Imposta il valore della proprietà fileName.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileName(String value) {
        this.fileName = value;
    }

    /**
     * Recupera il valore della proprietà hash.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getHash() {
        return hash;
    }

    /**
     * Imposta il valore della proprietà hash.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setHash(byte[] value) {
        this.hash = value;
    }

    /**
     * Recupera il valore della proprietà mimeType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMimeType() {
        return mimeType;
    }

    /**
     * Imposta il valore della proprietà mimeType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMimeType(String value) {
        this.mimeType = value;
    }

    /**
     * Recupera il valore della proprietà length.
     * 
     */
    public long getLength() {
        return length;
    }

    /**
     * Imposta il valore della proprietà length.
     * 
     */
    public void setLength(long value) {
        this.length = value;
    }

    /**
     * Recupera il valore della proprietà tipoCompressione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCompressione() {
        return tipoCompressione;
    }

    /**
     * Imposta il valore della proprietà tipoCompressione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCompressione(String value) {
        this.tipoCompressione = value;
    }

    /**
     * Recupera il valore della proprietà firmato.
     * 
     */
    public boolean isFirmato() {
        return firmato;
    }

    /**
     * Imposta il valore della proprietà firmato.
     * 
     */
    public void setFirmato(boolean value) {
        this.firmato = value;
    }

    /**
     * Recupera il valore della proprietà condivisibile.
     * 
     */
    public boolean isCondivisibile() {
        return condivisibile;
    }

    /**
     * Imposta il valore della proprietà condivisibile.
     * 
     */
    public void setCondivisibile(boolean value) {
        this.condivisibile = value;
    }

    /**
     * Recupera il valore della proprietà attivo.
     * 
     */
    public boolean isAttivo() {
        return attivo;
    }

    /**
     * Imposta il valore della proprietà attivo.
     * 
     */
    public void setAttivo(boolean value) {
        this.attivo = value;
    }

    /**
     * Recupera il valore della proprietà note.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNote() {
        return note;
    }

    /**
     * Imposta il valore della proprietà note.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNote(String value) {
        this.note = value;
    }

    /**
     * Recupera il valore della proprietà sistemaProduttore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaProduttore() {
        return sistemaProduttore;
    }

    /**
     * Imposta il valore della proprietà sistemaProduttore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaProduttore(String value) {
        this.sistemaProduttore = value;
    }

    /**
     * Recupera il valore della proprietà isDocumentoOriginale.
     * 
     */
    public boolean isIsDocumentoOriginale() {
        return isDocumentoOriginale;
    }

    /**
     * Imposta il valore della proprietà isDocumentoOriginale.
     * 
     */
    public void setIsDocumentoOriginale(boolean value) {
        this.isDocumentoOriginale = value;
    }

    /**
     * Recupera il valore della proprietà isDocumentoTrattato.
     * 
     */
    public boolean isIsDocumentoTrattato() {
        return isDocumentoTrattato;
    }

    /**
     * Imposta il valore della proprietà isDocumentoTrattato.
     * 
     */
    public void setIsDocumentoTrattato(boolean value) {
        this.isDocumentoTrattato = value;
    }

    /**
     * Recupera il valore della proprietà documentoOrigine.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoFileType }
     *     
     */
    public DocumentoFileType getDocumentoOrigine() {
        return documentoOrigine;
    }

    /**
     * Imposta il valore della proprietà documentoOrigine.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoFileType }
     *     
     */
    public void setDocumentoOrigine(DocumentoFileType value) {
        this.documentoOrigine = value;
    }

    /**
     * Recupera il valore della proprietà operazione.
     * 
     * @return
     *     possible object is
     *     {@link OperazioneDocumentaleType }
     *     
     */
    public OperazioneDocumentaleType getOperazione() {
        return operazione;
    }

    /**
     * Imposta il valore della proprietà operazione.
     * 
     * @param value
     *     allowed object is
     *     {@link OperazioneDocumentaleType }
     *     
     */
    public void setOperazione(OperazioneDocumentaleType value) {
        this.operazione = value;
    }

    /**
     * Recupera il valore della proprietà ticketArchiviazione.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getTicketArchiviazione() {
        return ticketArchiviazione;
    }

    /**
     * Imposta il valore della proprietà ticketArchiviazione.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setTicketArchiviazione(JAXBElement<String> value) {
        this.ticketArchiviazione = value;
    }

    /**
     * Recupera il valore della proprietà previewPng.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getPreviewPng() {
        return previewPng;
    }

    /**
     * Imposta il valore della proprietà previewPng.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setPreviewPng(byte[] value) {
        this.previewPng = value;
    }

}
