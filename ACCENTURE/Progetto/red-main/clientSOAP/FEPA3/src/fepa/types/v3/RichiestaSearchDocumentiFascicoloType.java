
package fepa.types.v3;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlElementRefs;
import javax.xml.bind.annotation.XmlType;


/**
 * Ricerca nei  documenti dei fascicoli
 * 
 * <p>Classe Java per richiesta_searchDocumentiFascicolo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_searchDocumentiFascicolo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoFascicolo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdFascicolo" type="{urn:fepa:types:v3}Guid" minOccurs="0"/>
 *         &lt;element name="DataCreazione" type="{urn:fepa:types:v3}dataRangeType" minOccurs="0"/>
 *         &lt;element name="FullTextSearch" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DocumentCriteria" type="{urn:fepa:types:v3}criteriaDocumenti_type" minOccurs="0"/>
 *         &lt;element name="Metadata" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Matadati">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence maxOccurs="unbounded">
 *                             &lt;element name="Key" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="VersioneMetadata" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="NumeroElementi" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_searchDocumentiFascicolo_type", propOrder = {
    "tipoFascicolo",
    "descrizione",
    "idFascicolo",
    "dataCreazione",
    "fullTextSearch",
    "documentCriteria",
    "metadata",
    "numeroElementi"
})
public class RichiestaSearchDocumentiFascicoloType
    implements Serializable
{

    @XmlElement(name = "TipoFascicolo", required = true)
    protected String tipoFascicolo;
    @XmlElement(name = "Descrizione", required = true)
    protected String descrizione;
    @XmlElement(name = "IdFascicolo")
    protected String idFascicolo;
    @XmlElement(name = "DataCreazione")
    protected DataRangeType dataCreazione;
    @XmlElement(name = "FullTextSearch")
    protected String fullTextSearch;
    @XmlElement(name = "DocumentCriteria")
    protected CriteriaDocumentiType documentCriteria;
    @XmlElement(name = "Metadata")
    protected RichiestaSearchDocumentiFascicoloType.Metadata metadata;
    @XmlElement(name = "NumeroElementi")
    protected int numeroElementi;

    /**
     * Recupera il valore della proprietà tipoFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoFascicolo() {
        return tipoFascicolo;
    }

    /**
     * Imposta il valore della proprietà tipoFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoFascicolo(String value) {
        this.tipoFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà descrizione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Imposta il valore della proprietà descrizione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Recupera il valore della proprietà idFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicolo() {
        return idFascicolo;
    }

    /**
     * Imposta il valore della proprietà idFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicolo(String value) {
        this.idFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà dataCreazione.
     * 
     * @return
     *     possible object is
     *     {@link DataRangeType }
     *     
     */
    public DataRangeType getDataCreazione() {
        return dataCreazione;
    }

    /**
     * Imposta il valore della proprietà dataCreazione.
     * 
     * @param value
     *     allowed object is
     *     {@link DataRangeType }
     *     
     */
    public void setDataCreazione(DataRangeType value) {
        this.dataCreazione = value;
    }

    /**
     * Recupera il valore della proprietà fullTextSearch.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFullTextSearch() {
        return fullTextSearch;
    }

    /**
     * Imposta il valore della proprietà fullTextSearch.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFullTextSearch(String value) {
        this.fullTextSearch = value;
    }

    /**
     * Recupera il valore della proprietà documentCriteria.
     * 
     * @return
     *     possible object is
     *     {@link CriteriaDocumentiType }
     *     
     */
    public CriteriaDocumentiType getDocumentCriteria() {
        return documentCriteria;
    }

    /**
     * Imposta il valore della proprietà documentCriteria.
     * 
     * @param value
     *     allowed object is
     *     {@link CriteriaDocumentiType }
     *     
     */
    public void setDocumentCriteria(CriteriaDocumentiType value) {
        this.documentCriteria = value;
    }

    /**
     * Recupera il valore della proprietà metadata.
     * 
     * @return
     *     possible object is
     *     {@link RichiestaSearchDocumentiFascicoloType.Metadata }
     *     
     */
    public RichiestaSearchDocumentiFascicoloType.Metadata getMetadata() {
        return metadata;
    }

    /**
     * Imposta il valore della proprietà metadata.
     * 
     * @param value
     *     allowed object is
     *     {@link RichiestaSearchDocumentiFascicoloType.Metadata }
     *     
     */
    public void setMetadata(RichiestaSearchDocumentiFascicoloType.Metadata value) {
        this.metadata = value;
    }

    /**
     * Recupera il valore della proprietà numeroElementi.
     * 
     */
    public int getNumeroElementi() {
        return numeroElementi;
    }

    /**
     * Imposta il valore della proprietà numeroElementi.
     * 
     */
    public void setNumeroElementi(int value) {
        this.numeroElementi = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Matadati">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence maxOccurs="unbounded">
     *                   &lt;element name="Key" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="VersioneMetadata" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "matadati",
        "versioneMetadata"
    })
    public static class Metadata
        implements Serializable
    {

        @XmlElement(name = "Matadati", required = true)
        protected RichiestaSearchDocumentiFascicoloType.Metadata.Matadati matadati;
        @XmlElement(name = "VersioneMetadata", required = true)
        protected String versioneMetadata;

        /**
         * Recupera il valore della proprietà matadati.
         * 
         * @return
         *     possible object is
         *     {@link RichiestaSearchDocumentiFascicoloType.Metadata.Matadati }
         *     
         */
        public RichiestaSearchDocumentiFascicoloType.Metadata.Matadati getMatadati() {
            return matadati;
        }

        /**
         * Imposta il valore della proprietà matadati.
         * 
         * @param value
         *     allowed object is
         *     {@link RichiestaSearchDocumentiFascicoloType.Metadata.Matadati }
         *     
         */
        public void setMatadati(RichiestaSearchDocumentiFascicoloType.Metadata.Matadati value) {
            this.matadati = value;
        }

        /**
         * Recupera il valore della proprietà versioneMetadata.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVersioneMetadata() {
            return versioneMetadata;
        }

        /**
         * Imposta il valore della proprietà versioneMetadata.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVersioneMetadata(String value) {
            this.versioneMetadata = value;
        }


        /**
         * <p>Classe Java per anonymous complex type.
         * 
         * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence maxOccurs="unbounded">
         *         &lt;element name="Key" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="Value" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "keyAndValue"
        })
        public static class Matadati
            implements Serializable
        {

            @XmlElementRefs({
                @XmlElementRef(name = "Key", type = JAXBElement.class),
                @XmlElementRef(name = "Value", type = JAXBElement.class)
            })
            protected List<JAXBElement<Object>> keyAndValue;

            /**
             * Gets the value of the keyAndValue property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the keyAndValue property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getKeyAndValue().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link JAXBElement }{@code <}{@link Object }{@code >}
             * {@link JAXBElement }{@code <}{@link Object }{@code >}
             * 
             * 
             */
            public List<JAXBElement<Object>> getKeyAndValue() {
                if (keyAndValue == null) {
                    keyAndValue = new ArrayList<JAXBElement<Object>>();
                }
                return this.keyAndValue;
            }

        }

    }

}
