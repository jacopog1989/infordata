
package fepa.types.v3;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import fepa.header.v3.BaseServiceResponseType;


/**
 * Esito Associazione
 * 
 * <p>Classe Java per risposta_associateFascicolo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_associateFascicolo_type">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:fepa:header:v3}BaseServiceResponseType">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_associateFascicolo_type")
public class RispostaAssociateFascicoloType
    extends BaseServiceResponseType
    implements Serializable
{


}
