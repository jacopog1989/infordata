
package fepa.types.v3;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import fepa.header.v3.BaseServiceResponseType;
import fepa.messages.v3.RispostaGetFascicolo;


/**
 * Dati del fascicolo 
 * 
 * <p>Classe Java per risposta_getFascicolo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_getFascicolo_type">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:fepa:header:v3}BaseServiceResponseType">
 *       &lt;sequence>
 *         &lt;element name="DettaglioFascicolo" type="{urn:fepa:types:v3}fascicolo_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_getFascicolo_type", propOrder = {
    "dettaglioFascicolo"
})
@XmlSeeAlso({
    RispostaGetFascicolo.class
})
public class RispostaGetFascicoloType
    extends BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "DettaglioFascicolo")
    protected FascicoloType dettaglioFascicolo;

    /**
     * Recupera il valore della proprietà dettaglioFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link FascicoloType }
     *     
     */
    public FascicoloType getDettaglioFascicolo() {
        return dettaglioFascicolo;
    }

    /**
     * Imposta il valore della proprietà dettaglioFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link FascicoloType }
     *     
     */
    public void setDettaglioFascicolo(FascicoloType value) {
        this.dettaglioFascicolo = value;
    }

}
