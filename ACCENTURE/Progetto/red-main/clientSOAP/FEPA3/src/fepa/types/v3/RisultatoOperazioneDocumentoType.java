
package fepa.types.v3;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlType;


/**
 * Tipo generico 
 * 
 * <p>Classe Java per risultatoOperazioneDocumento_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risultatoOperazioneDocumento_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence maxOccurs="unbounded">
 *         &lt;element name="IdDocumento" type="{urn:fepa:types:v3}Guid"/>
 *         &lt;element name="Operazione" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{urn:fepa:types:v3}operazioneDocumentale_type">
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Ordinamento" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risultatoOperazioneDocumento_type", propOrder = {
    "idDocumentoAndOperazioneAndOrdinamento"
})
public class RisultatoOperazioneDocumentoType
    implements Serializable
{

    @XmlElements({
        @XmlElement(name = "IdDocumento", required = true, type = String.class),
        @XmlElement(name = "Operazione", required = true, type = RisultatoOperazioneDocumentoType.Operazione.class),
        @XmlElement(name = "Ordinamento", required = true, type = Integer.class)
    })
    protected List<Serializable> idDocumentoAndOperazioneAndOrdinamento;

    /**
     * Gets the value of the idDocumentoAndOperazioneAndOrdinamento property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the idDocumentoAndOperazioneAndOrdinamento property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdDocumentoAndOperazioneAndOrdinamento().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * {@link RisultatoOperazioneDocumentoType.Operazione }
     * {@link Integer }
     * 
     * 
     */
    public List<Serializable> getIdDocumentoAndOperazioneAndOrdinamento() {
        if (idDocumentoAndOperazioneAndOrdinamento == null) {
            idDocumentoAndOperazioneAndOrdinamento = new ArrayList<Serializable>();
        }
        return this.idDocumentoAndOperazioneAndOrdinamento;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{urn:fepa:types:v3}operazioneDocumentale_type">
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Operazione
        extends OperazioneDocumentaleType
        implements Serializable
    {


    }

}
