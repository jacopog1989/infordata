
package fepa.types.v3;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * url AXWAY per il Download Documento
 * 
 * <p>Classe Java per richiesta_downloadDocumentoUrlFascicolo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_downloadDocumentoUrlFascicolo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicolo" type="{urn:fepa:types:v3}Guid"/>
 *         &lt;element name="TipoFascicolo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdParentFascicolo" type="{urn:fepa:types:v3}Guid" minOccurs="0"/>
 *         &lt;element name="IdDocumento" type="{urn:fepa:types:v3}Guid"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_downloadDocumentoUrlFascicolo_type", propOrder = {
    "idFascicolo",
    "tipoFascicolo",
    "idParentFascicolo",
    "idDocumento"
})
public class RichiestaDownloadDocumentoUrlFascicoloType
    implements Serializable
{

    @XmlElement(name = "IdFascicolo", required = true)
    protected String idFascicolo;
    @XmlElement(name = "TipoFascicolo", required = true)
    protected String tipoFascicolo;
    @XmlElement(name = "IdParentFascicolo")
    protected String idParentFascicolo;
    @XmlElement(name = "IdDocumento", required = true)
    protected String idDocumento;

    /**
     * Recupera il valore della proprietà idFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicolo() {
        return idFascicolo;
    }

    /**
     * Imposta il valore della proprietà idFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicolo(String value) {
        this.idFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà tipoFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoFascicolo() {
        return tipoFascicolo;
    }

    /**
     * Imposta il valore della proprietà tipoFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoFascicolo(String value) {
        this.tipoFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà idParentFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdParentFascicolo() {
        return idParentFascicolo;
    }

    /**
     * Imposta il valore della proprietà idParentFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdParentFascicolo(String value) {
        this.idParentFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà idDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumento() {
        return idDocumento;
    }

    /**
     * Imposta il valore della proprietà idDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumento(String value) {
        this.idDocumento = value;
    }

}
