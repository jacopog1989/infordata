
package fepa.types.v3;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import fepa.header.v3.BaseServiceResponseType;


/**
 * Esito ricerca
 * 
 * <p>Classe Java per risposta_searchDocumentiFascicolo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_searchDocumentiFascicolo_type">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:fepa:header:v3}BaseServiceResponseType">
 *       &lt;sequence>
 *         &lt;element name="DettaglioDocumentiFascicolo" type="{urn:fepa:types:v3}documentoSearch_type" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_searchDocumentiFascicolo_type", propOrder = {
    "dettaglioDocumentiFascicolo"
})
public class RispostaSearchDocumentiFascicoloType
    extends BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "DettaglioDocumentiFascicolo")
    protected List<DocumentoSearchType> dettaglioDocumentiFascicolo;

    /**
     * Gets the value of the dettaglioDocumentiFascicolo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dettaglioDocumentiFascicolo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDettaglioDocumentiFascicolo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentoSearchType }
     * 
     * 
     */
    public List<DocumentoSearchType> getDettaglioDocumentiFascicolo() {
        if (dettaglioDocumentiFascicolo == null) {
            dettaglioDocumentiFascicolo = new ArrayList<DocumentoSearchType>();
        }
        return this.dettaglioDocumentiFascicolo;
    }

}
