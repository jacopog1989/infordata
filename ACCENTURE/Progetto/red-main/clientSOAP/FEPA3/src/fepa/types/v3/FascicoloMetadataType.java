
package fepa.types.v3;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Metadati del Fascicolo
 * 
 * <p>Classe Java per fascicoloMetadata_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="fascicoloMetadata_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChiaveEsterna" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Metadata" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="VersioneMetadata" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fascicoloMetadata_type", propOrder = {
    "chiaveEsterna",
    "metadata",
    "versioneMetadata"
})
public class FascicoloMetadataType
    implements Serializable
{

    @XmlElement(name = "ChiaveEsterna")
    protected String chiaveEsterna;
    @XmlElement(name = "Metadata")
    protected byte[] metadata;
    @XmlElement(name = "VersioneMetadata")
    protected String versioneMetadata;

    /**
     * Recupera il valore della proprietà chiaveEsterna.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveEsterna() {
        return chiaveEsterna;
    }

    /**
     * Imposta il valore della proprietà chiaveEsterna.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveEsterna(String value) {
        this.chiaveEsterna = value;
    }

    /**
     * Recupera il valore della proprietà metadata.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getMetadata() {
        return metadata;
    }

    /**
     * Imposta il valore della proprietà metadata.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setMetadata(byte[] value) {
        this.metadata = value;
    }

    /**
     * Recupera il valore della proprietà versioneMetadata.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersioneMetadata() {
        return versioneMetadata;
    }

    /**
     * Imposta il valore della proprietà versioneMetadata.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersioneMetadata(String value) {
        this.versioneMetadata = value;
    }

}
