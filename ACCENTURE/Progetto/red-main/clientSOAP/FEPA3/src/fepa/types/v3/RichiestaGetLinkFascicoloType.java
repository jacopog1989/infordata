
package fepa.types.v3;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiesta navigazione fascicolo
 * 
 * <p>Classe Java per richiesta_getLinkFascicolo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_getLinkFascicolo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicolo" type="{urn:fepa:types:v3}Guid"/>
 *         &lt;element name="TipoFascicolo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TipoRelazione">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="PADRI"/>
 *               &lt;enumeration value="FIGLI"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TipoFascicoloCollegato" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_getLinkFascicolo_type", propOrder = {
    "idFascicolo",
    "tipoFascicolo",
    "tipoRelazione",
    "tipoFascicoloCollegato"
})
public class RichiestaGetLinkFascicoloType
    implements Serializable
{

    @XmlElement(name = "IdFascicolo", required = true)
    protected String idFascicolo;
    @XmlElement(name = "TipoFascicolo", required = true)
    protected String tipoFascicolo;
    @XmlElement(name = "TipoRelazione", required = true)
    protected String tipoRelazione;
    @XmlElement(name = "TipoFascicoloCollegato")
    protected List<String> tipoFascicoloCollegato;

    /**
     * Recupera il valore della proprietà idFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicolo() {
        return idFascicolo;
    }

    /**
     * Imposta il valore della proprietà idFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicolo(String value) {
        this.idFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà tipoFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoFascicolo() {
        return tipoFascicolo;
    }

    /**
     * Imposta il valore della proprietà tipoFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoFascicolo(String value) {
        this.tipoFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà tipoRelazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoRelazione() {
        return tipoRelazione;
    }

    /**
     * Imposta il valore della proprietà tipoRelazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoRelazione(String value) {
        this.tipoRelazione = value;
    }

    /**
     * Gets the value of the tipoFascicoloCollegato property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tipoFascicoloCollegato property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTipoFascicoloCollegato().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getTipoFascicoloCollegato() {
        if (tipoFascicoloCollegato == null) {
            tipoFascicoloCollegato = new ArrayList<String>();
        }
        return this.tipoFascicoloCollegato;
    }

}
