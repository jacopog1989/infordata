
package fepa.types.v3;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Metadati dei documenti del ATTO DECRETO
 * 
 * <p>Classe Java per documentoMetadata_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="documentoMetadata_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DataDocumento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="DataChiusura" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="DataAnnullamento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="ProtocolloMittente" type="{urn:fepa:types:v3}protocollo_type" minOccurs="0"/>
 *         &lt;element name="Protocollo" type="{urn:fepa:types:v3}protocollo_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Metadata" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="VersioneMetadata" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "documentoMetadata_type", propOrder = {
    "dataDocumento",
    "dataChiusura",
    "dataAnnullamento",
    "protocolloMittente",
    "protocollo",
    "note",
    "metadata",
    "versioneMetadata"
})
public class DocumentoMetadataType
    implements Serializable
{

    @XmlElement(name = "DataDocumento")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataDocumento;
    @XmlElement(name = "DataChiusura")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataChiusura;
    @XmlElement(name = "DataAnnullamento")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataAnnullamento;
    @XmlElement(name = "ProtocolloMittente")
    protected ProtocolloType protocolloMittente;
    @XmlElement(name = "Protocollo")
    protected List<ProtocolloType> protocollo;
    @XmlElement(name = "Note")
    protected String note;
    @XmlElement(name = "Metadata")
    protected byte[] metadata;
    @XmlElement(name = "VersioneMetadata")
    protected String versioneMetadata;

    /**
     * Recupera il valore della proprietà dataDocumento.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataDocumento() {
        return dataDocumento;
    }

    /**
     * Imposta il valore della proprietà dataDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataDocumento(XMLGregorianCalendar value) {
        this.dataDocumento = value;
    }

    /**
     * Recupera il valore della proprietà dataChiusura.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataChiusura() {
        return dataChiusura;
    }

    /**
     * Imposta il valore della proprietà dataChiusura.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataChiusura(XMLGregorianCalendar value) {
        this.dataChiusura = value;
    }

    /**
     * Recupera il valore della proprietà dataAnnullamento.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAnnullamento() {
        return dataAnnullamento;
    }

    /**
     * Imposta il valore della proprietà dataAnnullamento.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAnnullamento(XMLGregorianCalendar value) {
        this.dataAnnullamento = value;
    }

    /**
     * Recupera il valore della proprietà protocolloMittente.
     * 
     * @return
     *     possible object is
     *     {@link ProtocolloType }
     *     
     */
    public ProtocolloType getProtocolloMittente() {
        return protocolloMittente;
    }

    /**
     * Imposta il valore della proprietà protocolloMittente.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtocolloType }
     *     
     */
    public void setProtocolloMittente(ProtocolloType value) {
        this.protocolloMittente = value;
    }

    /**
     * Gets the value of the protocollo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the protocollo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProtocollo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtocolloType }
     * 
     * 
     */
    public List<ProtocolloType> getProtocollo() {
        if (protocollo == null) {
            protocollo = new ArrayList<ProtocolloType>();
        }
        return this.protocollo;
    }

    /**
     * Recupera il valore della proprietà note.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNote() {
        return note;
    }

    /**
     * Imposta il valore della proprietà note.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNote(String value) {
        this.note = value;
    }

    /**
     * Recupera il valore della proprietà metadata.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getMetadata() {
        return metadata;
    }

    /**
     * Imposta il valore della proprietà metadata.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setMetadata(byte[] value) {
        this.metadata = value;
    }

    /**
     * Recupera il valore della proprietà versioneMetadata.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersioneMetadata() {
        return versioneMetadata;
    }

    /**
     * Imposta il valore della proprietà versioneMetadata.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersioneMetadata(String value) {
        this.versioneMetadata = value;
    }

}
