
package fepa.types.v3;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import fepa.header.v3.BaseServiceResponseType;
import fepa.messages.v3.RispostaDownloadDocumentoUrlFascicolo;


/**
 * url AXWAY per il Download Documento
 * 
 * <p>Classe Java per risposta_downloadDocumentoUrlFascicolo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_downloadDocumentoUrlFascicolo_type">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:fepa:header:v3}BaseServiceResponseType">
 *       &lt;sequence>
 *         &lt;element name="DocumentoUrl" type="{http://www.w3.org/2001/XMLSchema}anyURI" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_downloadDocumentoUrlFascicolo_type", propOrder = {
    "documentoUrl"
})
@XmlSeeAlso({
    RispostaDownloadDocumentoUrlFascicolo.class
})
public class RispostaDownloadDocumentoUrlFascicoloType
    extends BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "DocumentoUrl")
    @XmlSchemaType(name = "anyURI")
    protected String documentoUrl;

    /**
     * Recupera il valore della proprietà documentoUrl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentoUrl() {
        return documentoUrl;
    }

    /**
     * Imposta il valore della proprietà documentoUrl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentoUrl(String value) {
        this.documentoUrl = value;
    }

}
