
package fepa.types.v3;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Dettaglio del fascicolo
 * 
 * <p>Classe Java per fascicolo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="fascicolo_type">
 *   &lt;complexContent>
 *     &lt;extension base="{urn:fepa:types:v3}fascicoloBase_type">
 *       &lt;sequence>
 *         &lt;element name="FascicoliContenuti" type="{urn:fepa:types:v3}fascicoloBase_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Associazioni" type="{urn:fepa:types:v3}IdentificativoAssociazioneApplicazioneType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fascicolo_type", propOrder = {
    "fascicoliContenuti",
    "associazioni"
})
public class FascicoloType
    extends FascicoloBaseType
    implements Serializable
{

    @XmlElement(name = "FascicoliContenuti")
    protected List<FascicoloBaseType> fascicoliContenuti;
    @XmlElement(name = "Associazioni")
    protected List<IdentificativoAssociazioneApplicazioneType> associazioni;

    /**
     * Gets the value of the fascicoliContenuti property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fascicoliContenuti property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFascicoliContenuti().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FascicoloBaseType }
     * 
     * 
     */
    public List<FascicoloBaseType> getFascicoliContenuti() {
        if (fascicoliContenuti == null) {
            fascicoliContenuti = new ArrayList<FascicoloBaseType>();
        }
        return this.fascicoliContenuti;
    }

    /**
     * Gets the value of the associazioni property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the associazioni property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssociazioni().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IdentificativoAssociazioneApplicazioneType }
     * 
     * 
     */
    public List<IdentificativoAssociazioneApplicazioneType> getAssociazioni() {
        if (associazioni == null) {
            associazioni = new ArrayList<IdentificativoAssociazioneApplicazioneType>();
        }
        return this.associazioni;
    }

}
