
package fepa.types.v3;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Richiesta dati del Documento del fascicolo 
 * 
 * <p>Classe Java per richiesta_getDocumentoFascicolo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_getDocumentoFascicolo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicolo" type="{urn:fepa:types:v3}Guid"/>
 *         &lt;element name="TipoFascicolo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdFascicoloRiferimento" type="{urn:fepa:types:v3}Guid" minOccurs="0"/>
 *         &lt;element name="IdDocumento" type="{urn:fepa:types:v3}Guid"/>
 *         &lt;element name="TipoEstrazione" type="{urn:fepa:types:v3}tipoDettaglioEstrazioneDocumenti_type" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_getDocumentoFascicolo_type", propOrder = {
    "idFascicolo",
    "tipoFascicolo",
    "idFascicoloRiferimento",
    "idDocumento",
    "tipoEstrazione"
})
public class RichiestaGetDocumentoFascicoloType
    implements Serializable
{

    @XmlElement(name = "IdFascicolo", required = true)
    protected String idFascicolo;
    @XmlElement(name = "TipoFascicolo", required = true)
    protected String tipoFascicolo;
    @XmlElement(name = "IdFascicoloRiferimento")
    protected String idFascicoloRiferimento;
    @XmlElement(name = "IdDocumento", required = true)
    protected String idDocumento;
    @XmlElement(name = "TipoEstrazione", required = true, defaultValue = "DATI_MINIMI")
    @XmlSchemaType(name = "string")
    protected List<TipoDettaglioEstrazioneDocumentiType> tipoEstrazione;

    /**
     * Recupera il valore della proprietà idFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicolo() {
        return idFascicolo;
    }

    /**
     * Imposta il valore della proprietà idFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicolo(String value) {
        this.idFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà tipoFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoFascicolo() {
        return tipoFascicolo;
    }

    /**
     * Imposta il valore della proprietà tipoFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoFascicolo(String value) {
        this.tipoFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà idFascicoloRiferimento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicoloRiferimento() {
        return idFascicoloRiferimento;
    }

    /**
     * Imposta il valore della proprietà idFascicoloRiferimento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicoloRiferimento(String value) {
        this.idFascicoloRiferimento = value;
    }

    /**
     * Recupera il valore della proprietà idDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumento() {
        return idDocumento;
    }

    /**
     * Imposta il valore della proprietà idDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumento(String value) {
        this.idDocumento = value;
    }

    /**
     * Gets the value of the tipoEstrazione property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tipoEstrazione property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTipoEstrazione().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TipoDettaglioEstrazioneDocumentiType }
     * 
     * 
     */
    public List<TipoDettaglioEstrazioneDocumentiType> getTipoEstrazione() {
        if (tipoEstrazione == null) {
            tipoEstrazione = new ArrayList<TipoDettaglioEstrazioneDocumentiType>();
        }
        return this.tipoEstrazione;
    }

}
