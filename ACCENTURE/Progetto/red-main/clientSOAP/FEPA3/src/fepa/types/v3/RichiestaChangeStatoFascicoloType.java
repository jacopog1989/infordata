
package fepa.types.v3;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Cambio StatoFascicolo
 * 
 * <p>Classe Java per richiesta_changeStatoFascicolo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_changeStatoFascicolo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicolo" type="{urn:fepa:types:v3}Guid"/>
 *         &lt;element name="TipoFascicolo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StatoFascicoloDocumentale" type="{urn:fepa:types:v3}statoFascicoloDocumentale_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_changeStatoFascicolo_type", propOrder = {
    "idFascicolo",
    "tipoFascicolo",
    "statoFascicoloDocumentale"
})
public class RichiestaChangeStatoFascicoloType
    implements Serializable
{

    @XmlElement(name = "IdFascicolo", required = true)
    protected String idFascicolo;
    @XmlElement(name = "TipoFascicolo", required = true)
    protected String tipoFascicolo;
    @XmlElement(name = "StatoFascicoloDocumentale", required = true)
    @XmlSchemaType(name = "string")
    protected StatoFascicoloDocumentaleType statoFascicoloDocumentale;

    /**
     * Recupera il valore della proprietà idFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicolo() {
        return idFascicolo;
    }

    /**
     * Imposta il valore della proprietà idFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicolo(String value) {
        this.idFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà tipoFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoFascicolo() {
        return tipoFascicolo;
    }

    /**
     * Imposta il valore della proprietà tipoFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoFascicolo(String value) {
        this.tipoFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà statoFascicoloDocumentale.
     * 
     * @return
     *     possible object is
     *     {@link StatoFascicoloDocumentaleType }
     *     
     */
    public StatoFascicoloDocumentaleType getStatoFascicoloDocumentale() {
        return statoFascicoloDocumentale;
    }

    /**
     * Imposta il valore della proprietà statoFascicoloDocumentale.
     * 
     * @param value
     *     allowed object is
     *     {@link StatoFascicoloDocumentaleType }
     *     
     */
    public void setStatoFascicoloDocumentale(StatoFascicoloDocumentaleType value) {
        this.statoFascicoloDocumentale = value;
    }

}
