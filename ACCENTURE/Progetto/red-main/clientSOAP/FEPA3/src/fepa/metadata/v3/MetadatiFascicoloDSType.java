//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Metadati Fascicolo del decreto assegnazione fondi
 * 
 * <p>Classe Java per MetadatiFascicoloDSType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiFascicoloDSType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DECRETO" type="{urn:fepa:metadata:v3}IdentificativoDecretoType"/>
 *         &lt;element name="CODICERAGIONERIA">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="RAGIONERIA">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CAPITOLO" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiFascicoloDSType", propOrder = {
    "decreto",
    "codiceragioneria",
    "ragioneria",
    "capitolo"
})
public class MetadatiFascicoloDSType {

    @XmlElement(name = "DECRETO", required = true)
    protected IdentificativoDecretoType decreto;
    @XmlElement(name = "CODICERAGIONERIA", required = true)
    protected String codiceragioneria;
    @XmlElement(name = "RAGIONERIA", required = true)
    protected String ragioneria;
    @XmlElement(name = "CAPITOLO")
    protected Integer capitolo;

    /**
     * Recupera il valore della propriet� decreto.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoDecretoType }
     *     
     */
    public IdentificativoDecretoType getDECRETO() {
        return decreto;
    }

    /**
     * Imposta il valore della propriet� decreto.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoDecretoType }
     *     
     */
    public void setDECRETO(IdentificativoDecretoType value) {
        this.decreto = value;
    }

    /**
     * Recupera il valore della propriet� codiceragioneria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODICERAGIONERIA() {
        return codiceragioneria;
    }

    /**
     * Imposta il valore della propriet� codiceragioneria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODICERAGIONERIA(String value) {
        this.codiceragioneria = value;
    }

    /**
     * Recupera il valore della propriet� ragioneria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRAGIONERIA() {
        return ragioneria;
    }

    /**
     * Imposta il valore della propriet� ragioneria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRAGIONERIA(String value) {
        this.ragioneria = value;
    }

    /**
     * Recupera il valore della propriet� capitolo.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCAPITOLO() {
        return capitolo;
    }

    /**
     * Imposta il valore della propriet� capitolo.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCAPITOLO(Integer value) {
        this.capitolo = value;
    }

}
