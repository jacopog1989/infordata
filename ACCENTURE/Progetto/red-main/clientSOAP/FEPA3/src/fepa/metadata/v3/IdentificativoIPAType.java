//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Identificativo dell'ufficio nell'IPA
 * 
 * <p>Classe Java per IdentificativoIPAType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="IdentificativoIPAType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CODICEPA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CODICEAOO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CODICEUFFICIO" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="6"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentificativoIPAType", propOrder = {
    "codicepa",
    "codiceaoo",
    "codiceufficio"
})
public class IdentificativoIPAType {

    @XmlElement(name = "CODICEPA", required = true)
    protected String codicepa;
    @XmlElement(name = "CODICEAOO")
    protected String codiceaoo;
    @XmlElement(name = "CODICEUFFICIO")
    protected String codiceufficio;

    /**
     * Recupera il valore della propriet� codicepa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODICEPA() {
        return codicepa;
    }

    /**
     * Imposta il valore della propriet� codicepa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODICEPA(String value) {
        this.codicepa = value;
    }

    /**
     * Recupera il valore della propriet� codiceaoo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODICEAOO() {
        return codiceaoo;
    }

    /**
     * Imposta il valore della propriet� codiceaoo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODICEAOO(String value) {
        this.codiceaoo = value;
    }

    /**
     * Recupera il valore della propriet� codiceufficio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODICEUFFICIO() {
        return codiceufficio;
    }

    /**
     * Imposta il valore della propriet� codiceufficio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODICEUFFICIO(String value) {
        this.codiceufficio = value;
    }

}
