//
// Questo file Ŕ stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrÓ persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Metadati Fascicolo dell'impegni Pluriennali ad esigibilitÓ
 * 
 * <p>Classe Java per MetadatiFascicoloIPEType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiFascicoloIPEType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IPE" type="{urn:fepa:metadata:v3}IdentificativoIPEType"/>
 *         &lt;element name="PCO" type="{urn:fepa:metadata:v3}IdentificativoPCOType"/>
 *         &lt;element name="BENEFICIARIO">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="60"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OGGETTOSPESA">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiFascicoloIPEType", propOrder = {
    "ipe",
    "pco",
    "beneficiario",
    "oggettospesa"
})
public class MetadatiFascicoloIPEType {

    @XmlElement(name = "IPE", required = true)
    protected IdentificativoIPEType ipe;
    @XmlElement(name = "PCO", required = true)
    protected IdentificativoPCOType pco;
    @XmlElement(name = "BENEFICIARIO", required = true)
    protected String beneficiario;
    @XmlElement(name = "OGGETTOSPESA", required = true)
    protected String oggettospesa;

    /**
     * Recupera il valore della proprietÓ ipe.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoIPEType }
     *     
     */
    public IdentificativoIPEType getIPE() {
        return ipe;
    }

    /**
     * Imposta il valore della proprietÓ ipe.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoIPEType }
     *     
     */
    public void setIPE(IdentificativoIPEType value) {
        this.ipe = value;
    }

    /**
     * Recupera il valore della proprietÓ pco.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoPCOType }
     *     
     */
    public IdentificativoPCOType getPCO() {
        return pco;
    }

    /**
     * Imposta il valore della proprietÓ pco.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoPCOType }
     *     
     */
    public void setPCO(IdentificativoPCOType value) {
        this.pco = value;
    }

    /**
     * Recupera il valore della proprietÓ beneficiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBENEFICIARIO() {
        return beneficiario;
    }

    /**
     * Imposta il valore della proprietÓ beneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBENEFICIARIO(String value) {
        this.beneficiario = value;
    }

    /**
     * Recupera il valore della proprietÓ oggettospesa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOGGETTOSPESA() {
        return oggettospesa;
    }

    /**
     * Imposta il valore della proprietÓ oggettospesa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOGGETTOSPESA(String value) {
        this.oggettospesa = value;
    }

}
