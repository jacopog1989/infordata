//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Tipo documento allegato alla fattura
 * 
 * <p>Classe Java per MetadatiDocumentoAllegatoFatturaType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiDocumentoAllegatoFatturaType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDENTIFICATIVOSDI" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="IDENTIFICATIVOSIFE" type="{urn:fepa:metadata:v3}IdentificativoDocumentoContabileType"/>
 *         &lt;element name="NUMEROFATTURA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DATAFATTURA" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="IDENTIFICATIVOLOTTO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiDocumentoAllegatoFatturaType", propOrder = {
    "identificativosdi",
    "identificativosife",
    "numerofattura",
    "datafattura",
    "identificativolotto"
})
public class MetadatiDocumentoAllegatoFatturaType {

    @XmlElement(name = "IDENTIFICATIVOSDI")
    protected long identificativosdi;
    @XmlElement(name = "IDENTIFICATIVOSIFE", required = true)
    protected String identificativosife;
    @XmlElement(name = "NUMEROFATTURA", required = true)
    protected String numerofattura;
    @XmlElement(name = "DATAFATTURA", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datafattura;
    @XmlElement(name = "IDENTIFICATIVOLOTTO")
    protected int identificativolotto;

    /**
     * Recupera il valore della propriet� identificativosdi.
     * 
     */
    public long getIDENTIFICATIVOSDI() {
        return identificativosdi;
    }

    /**
     * Imposta il valore della propriet� identificativosdi.
     * 
     */
    public void setIDENTIFICATIVOSDI(long value) {
        this.identificativosdi = value;
    }

    /**
     * Recupera il valore della propriet� identificativosife.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDENTIFICATIVOSIFE() {
        return identificativosife;
    }

    /**
     * Imposta il valore della propriet� identificativosife.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDENTIFICATIVOSIFE(String value) {
        this.identificativosife = value;
    }

    /**
     * Recupera il valore della propriet� numerofattura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNUMEROFATTURA() {
        return numerofattura;
    }

    /**
     * Imposta il valore della propriet� numerofattura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNUMEROFATTURA(String value) {
        this.numerofattura = value;
    }

    /**
     * Recupera il valore della propriet� datafattura.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATAFATTURA() {
        return datafattura;
    }

    /**
     * Imposta il valore della propriet� datafattura.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATAFATTURA(XMLGregorianCalendar value) {
        this.datafattura = value;
    }

    /**
     * Recupera il valore della propriet� identificativolotto.
     * 
     */
    public int getIDENTIFICATIVOLOTTO() {
        return identificativolotto;
    }

    /**
     * Imposta il valore della propriet� identificativolotto.
     * 
     */
    public void setIDENTIFICATIVOLOTTO(int value) {
        this.identificativolotto = value;
    }

}
