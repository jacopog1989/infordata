//
// Questo file Ŕ stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrÓ persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Fascicolo contenente il contratto
 * 
 * <p>Classe Java per MetadatiFascicoloContrattoType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiFascicoloContrattoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDCONTRATTO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ANNO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CODICECONTRATTO">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="15"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TIPOCONTRATTO">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DESRIZIONETIPOCONTRATTO">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="UFFICIOIPA" type="{urn:fepa:metadata:v3}IdentificativoIPAType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiFascicoloContrattoType", propOrder = {
    "idcontratto",
    "anno",
    "codicecontratto",
    "tipocontratto",
    "desrizionetipocontratto",
    "ufficioipa"
})
public class MetadatiFascicoloContrattoType {

    @XmlElement(name = "IDCONTRATTO")
    protected int idcontratto;
    @XmlElement(name = "ANNO")
    protected int anno;
    @XmlElement(name = "CODICECONTRATTO", required = true)
    protected String codicecontratto;
    @XmlElement(name = "TIPOCONTRATTO", required = true)
    protected String tipocontratto;
    @XmlElement(name = "DESRIZIONETIPOCONTRATTO", required = true)
    protected String desrizionetipocontratto;
    @XmlElement(name = "UFFICIOIPA", required = true)
    protected IdentificativoIPAType ufficioipa;

    /**
     * Recupera il valore della proprietÓ idcontratto.
     * 
     */
    public int getIDCONTRATTO() {
        return idcontratto;
    }

    /**
     * Imposta il valore della proprietÓ idcontratto.
     * 
     */
    public void setIDCONTRATTO(int value) {
        this.idcontratto = value;
    }

    /**
     * Recupera il valore della proprietÓ anno.
     * 
     */
    public int getANNO() {
        return anno;
    }

    /**
     * Imposta il valore della proprietÓ anno.
     * 
     */
    public void setANNO(int value) {
        this.anno = value;
    }

    /**
     * Recupera il valore della proprietÓ codicecontratto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODICECONTRATTO() {
        return codicecontratto;
    }

    /**
     * Imposta il valore della proprietÓ codicecontratto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODICECONTRATTO(String value) {
        this.codicecontratto = value;
    }

    /**
     * Recupera il valore della proprietÓ tipocontratto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTIPOCONTRATTO() {
        return tipocontratto;
    }

    /**
     * Imposta il valore della proprietÓ tipocontratto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTIPOCONTRATTO(String value) {
        this.tipocontratto = value;
    }

    /**
     * Recupera il valore della proprietÓ desrizionetipocontratto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESRIZIONETIPOCONTRATTO() {
        return desrizionetipocontratto;
    }

    /**
     * Imposta il valore della proprietÓ desrizionetipocontratto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESRIZIONETIPOCONTRATTO(String value) {
        this.desrizionetipocontratto = value;
    }

    /**
     * Recupera il valore della proprietÓ ufficioipa.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoIPAType }
     *     
     */
    public IdentificativoIPAType getUFFICIOIPA() {
        return ufficioipa;
    }

    /**
     * Imposta il valore della proprietÓ ufficioipa.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoIPAType }
     *     
     */
    public void setUFFICIOIPA(IdentificativoIPAType value) {
        this.ufficioipa = value;
    }

}
