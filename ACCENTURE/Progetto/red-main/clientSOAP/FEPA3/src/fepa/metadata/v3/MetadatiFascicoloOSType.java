//
// Questo file Ŕ stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrÓ persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Fascicolo contenente l'Ordine 
 * 			
 * 
 * <p>Classe Java per MetadatiFascicoloOSType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiFascicoloOSType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ANNO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CODIAMMINISTRAZIONE">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AMMINISTRAZIONE">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CODICERAGIONERIA">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="RAGIONERIA">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="IDENTIFICATIVOSIFE" type="{urn:fepa:metadata:v3}IdentificativoDocumentoContabileType"/>
 *         &lt;element name="CAPITOLO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NUMEROOA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NUMEROTITOLO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OGGETTOSPESA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BENEFICIARIO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CODICEFISCALE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PARTITAIVA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DATAEMISSIONE" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="CHIAVEFD" type="{urn:fepa:metadata:v3}chiaveFD_type"/>
 *         &lt;element name="DESCRIZIONEFUNZIONARIODELEGATO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CODIAMMINISTRAZIONEOPERANTE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AMMINISTRAZIONEOPERANTE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiFascicoloOSType", propOrder = {
    "anno",
    "codiamministrazione",
    "amministrazione",
    "codiceragioneria",
    "ragioneria",
    "identificativosife",
    "capitolo",
    "numerooa",
    "numerotitolo",
    "oggettospesa",
    "beneficiario",
    "codicefiscale",
    "partitaiva",
    "dataemissione",
    "chiavefd",
    "descrizionefunzionariodelegato",
    "codiamministrazioneoperante",
    "amministrazioneoperante"
})
public class MetadatiFascicoloOSType {

    @XmlElement(name = "ANNO")
    protected int anno;
    @XmlElement(name = "CODIAMMINISTRAZIONE", required = true)
    protected String codiamministrazione;
    @XmlElement(name = "AMMINISTRAZIONE", required = true)
    protected String amministrazione;
    @XmlElement(name = "CODICERAGIONERIA", required = true)
    protected String codiceragioneria;
    @XmlElement(name = "RAGIONERIA", required = true)
    protected String ragioneria;
    @XmlElement(name = "IDENTIFICATIVOSIFE", required = true)
    protected String identificativosife;
    @XmlElement(name = "CAPITOLO", required = true)
    protected String capitolo;
    @XmlElement(name = "NUMEROOA", required = true)
    protected String numerooa;
    @XmlElement(name = "NUMEROTITOLO", required = true)
    protected String numerotitolo;
    @XmlElement(name = "OGGETTOSPESA", required = true)
    protected String oggettospesa;
    @XmlElement(name = "BENEFICIARIO", required = true)
    protected String beneficiario;
    @XmlElement(name = "CODICEFISCALE", required = true)
    protected String codicefiscale;
    @XmlElement(name = "PARTITAIVA", required = true)
    protected String partitaiva;
    @XmlElement(name = "DATAEMISSIONE", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataemissione;
    @XmlElement(name = "CHIAVEFD", required = true)
    protected ChiaveFDType chiavefd;
    @XmlElement(name = "DESCRIZIONEFUNZIONARIODELEGATO", required = true)
    protected String descrizionefunzionariodelegato;
    @XmlElement(name = "CODIAMMINISTRAZIONEOPERANTE", required = true)
    protected String codiamministrazioneoperante;
    @XmlElement(name = "AMMINISTRAZIONEOPERANTE", required = true)
    protected String amministrazioneoperante;

    /**
     * Recupera il valore della proprietÓ anno.
     * 
     */
    public int getANNO() {
        return anno;
    }

    /**
     * Imposta il valore della proprietÓ anno.
     * 
     */
    public void setANNO(int value) {
        this.anno = value;
    }

    /**
     * Recupera il valore della proprietÓ codiamministrazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODIAMMINISTRAZIONE() {
        return codiamministrazione;
    }

    /**
     * Imposta il valore della proprietÓ codiamministrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODIAMMINISTRAZIONE(String value) {
        this.codiamministrazione = value;
    }

    /**
     * Recupera il valore della proprietÓ amministrazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAMMINISTRAZIONE() {
        return amministrazione;
    }

    /**
     * Imposta il valore della proprietÓ amministrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAMMINISTRAZIONE(String value) {
        this.amministrazione = value;
    }

    /**
     * Recupera il valore della proprietÓ codiceragioneria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODICERAGIONERIA() {
        return codiceragioneria;
    }

    /**
     * Imposta il valore della proprietÓ codiceragioneria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODICERAGIONERIA(String value) {
        this.codiceragioneria = value;
    }

    /**
     * Recupera il valore della proprietÓ ragioneria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRAGIONERIA() {
        return ragioneria;
    }

    /**
     * Imposta il valore della proprietÓ ragioneria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRAGIONERIA(String value) {
        this.ragioneria = value;
    }

    /**
     * Recupera il valore della proprietÓ identificativosife.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDENTIFICATIVOSIFE() {
        return identificativosife;
    }

    /**
     * Imposta il valore della proprietÓ identificativosife.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDENTIFICATIVOSIFE(String value) {
        this.identificativosife = value;
    }

    /**
     * Recupera il valore della proprietÓ capitolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCAPITOLO() {
        return capitolo;
    }

    /**
     * Imposta il valore della proprietÓ capitolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCAPITOLO(String value) {
        this.capitolo = value;
    }

    /**
     * Recupera il valore della proprietÓ numerooa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNUMEROOA() {
        return numerooa;
    }

    /**
     * Imposta il valore della proprietÓ numerooa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNUMEROOA(String value) {
        this.numerooa = value;
    }

    /**
     * Recupera il valore della proprietÓ numerotitolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNUMEROTITOLO() {
        return numerotitolo;
    }

    /**
     * Imposta il valore della proprietÓ numerotitolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNUMEROTITOLO(String value) {
        this.numerotitolo = value;
    }

    /**
     * Recupera il valore della proprietÓ oggettospesa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOGGETTOSPESA() {
        return oggettospesa;
    }

    /**
     * Imposta il valore della proprietÓ oggettospesa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOGGETTOSPESA(String value) {
        this.oggettospesa = value;
    }

    /**
     * Recupera il valore della proprietÓ beneficiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBENEFICIARIO() {
        return beneficiario;
    }

    /**
     * Imposta il valore della proprietÓ beneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBENEFICIARIO(String value) {
        this.beneficiario = value;
    }

    /**
     * Recupera il valore della proprietÓ codicefiscale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODICEFISCALE() {
        return codicefiscale;
    }

    /**
     * Imposta il valore della proprietÓ codicefiscale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODICEFISCALE(String value) {
        this.codicefiscale = value;
    }

    /**
     * Recupera il valore della proprietÓ partitaiva.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPARTITAIVA() {
        return partitaiva;
    }

    /**
     * Imposta il valore della proprietÓ partitaiva.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPARTITAIVA(String value) {
        this.partitaiva = value;
    }

    /**
     * Recupera il valore della proprietÓ dataemissione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATAEMISSIONE() {
        return dataemissione;
    }

    /**
     * Imposta il valore della proprietÓ dataemissione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATAEMISSIONE(XMLGregorianCalendar value) {
        this.dataemissione = value;
    }

    /**
     * Recupera il valore della proprietÓ chiavefd.
     * 
     * @return
     *     possible object is
     *     {@link ChiaveFDType }
     *     
     */
    public ChiaveFDType getCHIAVEFD() {
        return chiavefd;
    }

    /**
     * Imposta il valore della proprietÓ chiavefd.
     * 
     * @param value
     *     allowed object is
     *     {@link ChiaveFDType }
     *     
     */
    public void setCHIAVEFD(ChiaveFDType value) {
        this.chiavefd = value;
    }

    /**
     * Recupera il valore della proprietÓ descrizionefunzionariodelegato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESCRIZIONEFUNZIONARIODELEGATO() {
        return descrizionefunzionariodelegato;
    }

    /**
     * Imposta il valore della proprietÓ descrizionefunzionariodelegato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESCRIZIONEFUNZIONARIODELEGATO(String value) {
        this.descrizionefunzionariodelegato = value;
    }

    /**
     * Recupera il valore della proprietÓ codiamministrazioneoperante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODIAMMINISTRAZIONEOPERANTE() {
        return codiamministrazioneoperante;
    }

    /**
     * Imposta il valore della proprietÓ codiamministrazioneoperante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODIAMMINISTRAZIONEOPERANTE(String value) {
        this.codiamministrazioneoperante = value;
    }

    /**
     * Recupera il valore della proprietÓ amministrazioneoperante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAMMINISTRAZIONEOPERANTE() {
        return amministrazioneoperante;
    }

    /**
     * Imposta il valore della proprietÓ amministrazioneoperante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAMMINISTRAZIONEOPERANTE(String value) {
        this.amministrazioneoperante = value;
    }

}
