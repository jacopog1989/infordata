//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per statoDocumentoContabile_type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="statoDocumentoContabile_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ACQUISITO"/>
 *     &lt;enumeration value="ACCETTATO"/>
 *     &lt;enumeration value="TRASFERITO_AD_ALTRA_AMM"/>
 *     &lt;enumeration value="RICEVUTO_DA_TRAFERIMENTO_DA_ALTRA_AMM"/>
 *     &lt;enumeration value="RIFIUTATO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "statoDocumentoContabile_type")
@XmlEnum
public enum StatoDocumentoContabileType {

    ACQUISITO,
    ACCETTATO,
    TRASFERITO_AD_ALTRA_AMM,
    RICEVUTO_DA_TRAFERIMENTO_DA_ALTRA_AMM,
    RIFIUTATO;

    public String value() {
        return name();
    }

    public static StatoDocumentoContabileType fromValue(String v) {
        return valueOf(v);
    }

}
