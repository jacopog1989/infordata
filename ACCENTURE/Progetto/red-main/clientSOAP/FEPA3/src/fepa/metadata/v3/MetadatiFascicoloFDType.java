//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Metadati Fascicolo Cronoprogramma Funzionario Delegato
 * 
 * <p>Classe Java per MetadatiFascicoloFDType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiFascicoloFDType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FUNZIONARIODELEGATO" type="{urn:fepa:metadata:v3}IdentificativoFDtype"/>
 *         &lt;element name="PIANOCONTABILE" type="{urn:fepa:metadata:v3}IdentificativoPCOType"/>
 *         &lt;element name="NUMEROPROGRAMMASPESA" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CAUSALE">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiFascicoloFDType", propOrder = {
    "funzionariodelegato",
    "pianocontabile",
    "numeroprogrammaspesa",
    "causale"
})
public class MetadatiFascicoloFDType {

    @XmlElement(name = "FUNZIONARIODELEGATO", required = true)
    protected IdentificativoFDtype funzionariodelegato;
    @XmlElement(name = "PIANOCONTABILE", required = true)
    protected IdentificativoPCOType pianocontabile;
    @XmlElement(name = "NUMEROPROGRAMMASPESA")
    protected int numeroprogrammaspesa;
    @XmlElement(name = "CAUSALE", required = true)
    protected String causale;

    /**
     * Recupera il valore della propriet� funzionariodelegato.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoFDtype }
     *     
     */
    public IdentificativoFDtype getFUNZIONARIODELEGATO() {
        return funzionariodelegato;
    }

    /**
     * Imposta il valore della propriet� funzionariodelegato.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoFDtype }
     *     
     */
    public void setFUNZIONARIODELEGATO(IdentificativoFDtype value) {
        this.funzionariodelegato = value;
    }

    /**
     * Recupera il valore della propriet� pianocontabile.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoPCOType }
     *     
     */
    public IdentificativoPCOType getPIANOCONTABILE() {
        return pianocontabile;
    }

    /**
     * Imposta il valore della propriet� pianocontabile.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoPCOType }
     *     
     */
    public void setPIANOCONTABILE(IdentificativoPCOType value) {
        this.pianocontabile = value;
    }

    /**
     * Recupera il valore della propriet� numeroprogrammaspesa.
     * 
     */
    public int getNUMEROPROGRAMMASPESA() {
        return numeroprogrammaspesa;
    }

    /**
     * Imposta il valore della propriet� numeroprogrammaspesa.
     * 
     */
    public void setNUMEROPROGRAMMASPESA(int value) {
        this.numeroprogrammaspesa = value;
    }

    /**
     * Recupera il valore della propriet� causale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCAUSALE() {
        return causale;
    }

    /**
     * Imposta il valore della propriet� causale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCAUSALE(String value) {
        this.causale = value;
    }

}
