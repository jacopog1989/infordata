//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Tipo documento Notifica della decorrenza dei termini
 * 			
 * 
 * <p>Classe Java per MetadatiDocumentoNotificaDecorrenzaTerminiType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiDocumentoNotificaDecorrenzaTerminiType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDENTIFICATIVOSDI" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiDocumentoNotificaDecorrenzaTerminiType", propOrder = {
    "identificativosdi"
})
public class MetadatiDocumentoNotificaDecorrenzaTerminiType {

    @XmlElement(name = "IDENTIFICATIVOSDI")
    protected long identificativosdi;

    /**
     * Recupera il valore della propriet� identificativosdi.
     * 
     */
    public long getIDENTIFICATIVOSDI() {
        return identificativosdi;
    }

    /**
     * Imposta il valore della propriet� identificativosdi.
     * 
     */
    public void setIDENTIFICATIVOSDI(long value) {
        this.identificativosdi = value;
    }

}
