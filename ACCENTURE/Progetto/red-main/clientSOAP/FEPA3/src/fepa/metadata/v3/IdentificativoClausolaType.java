//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Identificativo delle clausole
 * 
 * <p>Classe Java per IdentificativoClausolaType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="IdentificativoClausolaType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DECRETO" type="{urn:fepa:metadata:v3}IdentificativoDecretoType"/>
 *         &lt;element name="NUMEROCLAUSOLA" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentificativoClausolaType", propOrder = {
    "decreto",
    "numeroclausola"
})
public class IdentificativoClausolaType {

    @XmlElement(name = "DECRETO", required = true)
    protected IdentificativoDecretoType decreto;
    @XmlElement(name = "NUMEROCLAUSOLA")
    protected int numeroclausola;

    /**
     * Recupera il valore della propriet� decreto.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoDecretoType }
     *     
     */
    public IdentificativoDecretoType getDECRETO() {
        return decreto;
    }

    /**
     * Imposta il valore della propriet� decreto.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoDecretoType }
     *     
     */
    public void setDECRETO(IdentificativoDecretoType value) {
        this.decreto = value;
    }

    /**
     * Recupera il valore della propriet� numeroclausola.
     * 
     */
    public int getNUMEROCLAUSOLA() {
        return numeroclausola;
    }

    /**
     * Imposta il valore della propriet� numeroclausola.
     * 
     */
    public void setNUMEROCLAUSOLA(int value) {
        this.numeroclausola = value;
    }

}
