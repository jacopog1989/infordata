//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per chiaveTitoloCS_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="chiaveTitoloCS_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ANNOEMISSIONE" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PROGRESSIVO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TESORERIA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CONTOTESORERIA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "chiaveTitoloCS_type", propOrder = {
    "annoemissione",
    "progressivo",
    "tesoreria",
    "contotesoreria"
})
public class ChiaveTitoloCSType {

    @XmlElement(name = "ANNOEMISSIONE")
    protected int annoemissione;
    @XmlElement(name = "PROGRESSIVO", required = true)
    protected String progressivo;
    @XmlElement(name = "TESORERIA", required = true)
    protected String tesoreria;
    @XmlElement(name = "CONTOTESORERIA", required = true)
    protected String contotesoreria;

    /**
     * Recupera il valore della propriet� annoemissione.
     * 
     */
    public int getANNOEMISSIONE() {
        return annoemissione;
    }

    /**
     * Imposta il valore della propriet� annoemissione.
     * 
     */
    public void setANNOEMISSIONE(int value) {
        this.annoemissione = value;
    }

    /**
     * Recupera il valore della propriet� progressivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPROGRESSIVO() {
        return progressivo;
    }

    /**
     * Imposta il valore della propriet� progressivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPROGRESSIVO(String value) {
        this.progressivo = value;
    }

    /**
     * Recupera il valore della propriet� tesoreria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTESORERIA() {
        return tesoreria;
    }

    /**
     * Imposta il valore della propriet� tesoreria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTESORERIA(String value) {
        this.tesoreria = value;
    }

    /**
     * Recupera il valore della propriet� contotesoreria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCONTOTESORERIA() {
        return contotesoreria;
    }

    /**
     * Imposta il valore della propriet� contotesoreria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCONTOTESORERIA(String value) {
        this.contotesoreria = value;
    }

}
