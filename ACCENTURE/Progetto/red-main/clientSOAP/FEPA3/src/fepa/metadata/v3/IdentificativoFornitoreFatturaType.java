//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Fornitore della Fattura Elettronica
 * 
 * <p>Classe Java per IdentificativoFornitoreFatturaType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="IdentificativoFornitoreFatturaType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CODICEFISCALE">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="16"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DENOMINAZIONE">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="IDFISCALEPAESEFORNITORE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="IDFISCALECODICEFORNITORE" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="28"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentificativoFornitoreFatturaType", propOrder = {
    "codicefiscale",
    "denominazione",
    "idfiscalepaesefornitore",
    "idfiscalecodicefornitore"
})
public class IdentificativoFornitoreFatturaType {

    @XmlElement(name = "CODICEFISCALE", required = true)
    protected String codicefiscale;
    @XmlElement(name = "DENOMINAZIONE", required = true)
    protected String denominazione;
    @XmlElement(name = "IDFISCALEPAESEFORNITORE")
    protected String idfiscalepaesefornitore;
    @XmlElement(name = "IDFISCALECODICEFORNITORE")
    protected String idfiscalecodicefornitore;

    /**
     * Recupera il valore della propriet� codicefiscale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODICEFISCALE() {
        return codicefiscale;
    }

    /**
     * Imposta il valore della propriet� codicefiscale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODICEFISCALE(String value) {
        this.codicefiscale = value;
    }

    /**
     * Recupera il valore della propriet� denominazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDENOMINAZIONE() {
        return denominazione;
    }

    /**
     * Imposta il valore della propriet� denominazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDENOMINAZIONE(String value) {
        this.denominazione = value;
    }

    /**
     * Recupera il valore della propriet� idfiscalepaesefornitore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDFISCALEPAESEFORNITORE() {
        return idfiscalepaesefornitore;
    }

    /**
     * Imposta il valore della propriet� idfiscalepaesefornitore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDFISCALEPAESEFORNITORE(String value) {
        this.idfiscalepaesefornitore = value;
    }

    /**
     * Recupera il valore della propriet� idfiscalecodicefornitore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDFISCALECODICEFORNITORE() {
        return idfiscalecodicefornitore;
    }

    /**
     * Imposta il valore della propriet� idfiscalecodicefornitore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDFISCALECODICEFORNITORE(String value) {
        this.idfiscalecodicefornitore = value;
    }

}
