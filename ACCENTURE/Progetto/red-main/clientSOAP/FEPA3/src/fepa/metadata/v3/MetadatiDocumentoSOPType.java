//
// Questo file Ŕ stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrÓ persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Tipo documento Ordine di Pagare
 * 
 * <p>Classe Java per MetadatiDocumentoSOPType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiDocumentoSOPType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ANNO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CODIAMMINISTRAZIONE">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AMMINISTRAZIONE">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CODICERAGIONERIA">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="RAGIONERIA">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="IDENTIFICATIVOSIFE" type="{urn:fepa:metadata:v3}IdentificativoDocumentoContabileType"/>
 *         &lt;element name="CAPITOLO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PIANOGESTIONE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NUMEROTITOLO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="OGGETTOSPESA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BENEFICIARIO" type="{urn:fepa:metadata:v3}IdentificativoBeneficiarioSOPType" minOccurs="0"/>
 *         &lt;element name="TIPOTITOLO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DATAEMISSIONE" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="AOO_REGISTRAZIONE_RGS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NUME_REGISTRAZIONE_RGS" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DT_REGISTRAZIONE_RGS" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiDocumentoSOPType", propOrder = {
    "anno",
    "codiamministrazione",
    "amministrazione",
    "codiceragioneria",
    "ragioneria",
    "identificativosife",
    "capitolo",
    "pianogestione",
    "numerotitolo",
    "oggettospesa",
    "beneficiario",
    "tipotitolo",
    "dataemissione",
    "aooregistrazionergs",
    "numeregistrazionergs",
    "dtregistrazionergs"
})
public class MetadatiDocumentoSOPType {

    @XmlElement(name = "ANNO")
    protected int anno;
    @XmlElement(name = "CODIAMMINISTRAZIONE", required = true)
    protected String codiamministrazione;
    @XmlElement(name = "AMMINISTRAZIONE", required = true)
    protected String amministrazione;
    @XmlElement(name = "CODICERAGIONERIA", required = true)
    protected String codiceragioneria;
    @XmlElement(name = "RAGIONERIA", required = true)
    protected String ragioneria;
    @XmlElement(name = "IDENTIFICATIVOSIFE", required = true)
    protected String identificativosife;
    @XmlElement(name = "CAPITOLO")
    protected String capitolo;
    @XmlElement(name = "PIANOGESTIONE")
    protected String pianogestione;
    @XmlElement(name = "NUMEROTITOLO")
    protected String numerotitolo;
    @XmlElement(name = "OGGETTOSPESA")
    protected String oggettospesa;
    @XmlElement(name = "BENEFICIARIO")
    protected IdentificativoBeneficiarioSOPType beneficiario;
    @XmlElement(name = "TIPOTITOLO")
    protected String tipotitolo;
    @XmlElement(name = "DATAEMISSIONE")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataemissione;
    @XmlElement(name = "AOO_REGISTRAZIONE_RGS")
    protected String aooregistrazionergs;
    @XmlElement(name = "NUME_REGISTRAZIONE_RGS")
    protected String numeregistrazionergs;
    @XmlElement(name = "DT_REGISTRAZIONE_RGS")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dtregistrazionergs;

    /**
     * Recupera il valore della proprietÓ anno.
     * 
     */
    public int getANNO() {
        return anno;
    }

    /**
     * Imposta il valore della proprietÓ anno.
     * 
     */
    public void setANNO(int value) {
        this.anno = value;
    }

    /**
     * Recupera il valore della proprietÓ codiamministrazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODIAMMINISTRAZIONE() {
        return codiamministrazione;
    }

    /**
     * Imposta il valore della proprietÓ codiamministrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODIAMMINISTRAZIONE(String value) {
        this.codiamministrazione = value;
    }

    /**
     * Recupera il valore della proprietÓ amministrazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAMMINISTRAZIONE() {
        return amministrazione;
    }

    /**
     * Imposta il valore della proprietÓ amministrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAMMINISTRAZIONE(String value) {
        this.amministrazione = value;
    }

    /**
     * Recupera il valore della proprietÓ codiceragioneria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODICERAGIONERIA() {
        return codiceragioneria;
    }

    /**
     * Imposta il valore della proprietÓ codiceragioneria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODICERAGIONERIA(String value) {
        this.codiceragioneria = value;
    }

    /**
     * Recupera il valore della proprietÓ ragioneria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRAGIONERIA() {
        return ragioneria;
    }

    /**
     * Imposta il valore della proprietÓ ragioneria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRAGIONERIA(String value) {
        this.ragioneria = value;
    }

    /**
     * Recupera il valore della proprietÓ identificativosife.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDENTIFICATIVOSIFE() {
        return identificativosife;
    }

    /**
     * Imposta il valore della proprietÓ identificativosife.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDENTIFICATIVOSIFE(String value) {
        this.identificativosife = value;
    }

    /**
     * Recupera il valore della proprietÓ capitolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCAPITOLO() {
        return capitolo;
    }

    /**
     * Imposta il valore della proprietÓ capitolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCAPITOLO(String value) {
        this.capitolo = value;
    }

    /**
     * Recupera il valore della proprietÓ pianogestione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPIANOGESTIONE() {
        return pianogestione;
    }

    /**
     * Imposta il valore della proprietÓ pianogestione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPIANOGESTIONE(String value) {
        this.pianogestione = value;
    }

    /**
     * Recupera il valore della proprietÓ numerotitolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNUMEROTITOLO() {
        return numerotitolo;
    }

    /**
     * Imposta il valore della proprietÓ numerotitolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNUMEROTITOLO(String value) {
        this.numerotitolo = value;
    }

    /**
     * Recupera il valore della proprietÓ oggettospesa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOGGETTOSPESA() {
        return oggettospesa;
    }

    /**
     * Imposta il valore della proprietÓ oggettospesa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOGGETTOSPESA(String value) {
        this.oggettospesa = value;
    }

    /**
     * Recupera il valore della proprietÓ beneficiario.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoBeneficiarioSOPType }
     *     
     */
    public IdentificativoBeneficiarioSOPType getBENEFICIARIO() {
        return beneficiario;
    }

    /**
     * Imposta il valore della proprietÓ beneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoBeneficiarioSOPType }
     *     
     */
    public void setBENEFICIARIO(IdentificativoBeneficiarioSOPType value) {
        this.beneficiario = value;
    }

    /**
     * Recupera il valore della proprietÓ tipotitolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTIPOTITOLO() {
        return tipotitolo;
    }

    /**
     * Imposta il valore della proprietÓ tipotitolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTIPOTITOLO(String value) {
        this.tipotitolo = value;
    }

    /**
     * Recupera il valore della proprietÓ dataemissione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATAEMISSIONE() {
        return dataemissione;
    }

    /**
     * Imposta il valore della proprietÓ dataemissione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATAEMISSIONE(XMLGregorianCalendar value) {
        this.dataemissione = value;
    }

    /**
     * Recupera il valore della proprietÓ aooregistrazionergs.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAOOREGISTRAZIONERGS() {
        return aooregistrazionergs;
    }

    /**
     * Imposta il valore della proprietÓ aooregistrazionergs.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAOOREGISTRAZIONERGS(String value) {
        this.aooregistrazionergs = value;
    }

    /**
     * Recupera il valore della proprietÓ numeregistrazionergs.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNUMEREGISTRAZIONERGS() {
        return numeregistrazionergs;
    }

    /**
     * Imposta il valore della proprietÓ numeregistrazionergs.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNUMEREGISTRAZIONERGS(String value) {
        this.numeregistrazionergs = value;
    }

    /**
     * Recupera il valore della proprietÓ dtregistrazionergs.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDTREGISTRAZIONERGS() {
        return dtregistrazionergs;
    }

    /**
     * Imposta il valore della proprietÓ dtregistrazionergs.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDTREGISTRAZIONERGS(XMLGregorianCalendar value) {
        this.dtregistrazionergs = value;
    }

}
