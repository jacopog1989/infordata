//
// Questo file Ŕ stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrÓ persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Tipo per il Fascicolo della Fattura
 * 
 * <p>Classe Java per MetadatiFascicoloFatturaType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiFascicoloFatturaType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDENTIFICATIVOSDI" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="IDENTIFICATIVOSIFE" type="{urn:fepa:metadata:v3}IdentificativoDocumentoContabileType"/>
 *         &lt;element name="IDENTIFICATIVOUFFICIOFE" type="{urn:fepa:metadata:v3}IdentificativoIPAType"/>
 *         &lt;element name="IDENTIFICATIVOFORNITORE" type="{urn:fepa:metadata:v3}IdentificativoFornitoreFatturaType" minOccurs="0"/>
 *         &lt;element name="NUMEROFATTURA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DATAFATTURA" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="IDENTIFICATIVOLOTTO" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="DATAPRESAINCARICO" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="STATOFASCICOLO" type="{urn:fepa:metadata:v3}statoDocumentoContabile_type" minOccurs="0"/>
 *         &lt;element name="REGISTRAZIONE" type="{urn:fepa:metadata:v3}registroPROTOCOLLO_type" minOccurs="0"/>
 *         &lt;element name="FORMATOTRASMISSIONE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TIPODOCUMENTOCONTABILE" type="{urn:fepa:metadata:v3}tipoDocumentoContabile_type" minOccurs="0"/>
 *         &lt;element name="DECORRENZATERMINI" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiFascicoloFatturaType", propOrder = {
    "identificativosdi",
    "identificativosife",
    "identificativoufficiofe",
    "identificativofornitore",
    "numerofattura",
    "datafattura",
    "identificativolotto",
    "datapresaincarico",
    "statofascicolo",
    "registrazione",
    "formatotrasmissione",
    "tipodocumentocontabile",
    "decorrenzatermini"
})
public class MetadatiFascicoloFatturaType {

    @XmlElement(name = "IDENTIFICATIVOSDI")
    protected long identificativosdi;
    @XmlElement(name = "IDENTIFICATIVOSIFE", required = true)
    protected String identificativosife;
    @XmlElement(name = "IDENTIFICATIVOUFFICIOFE", required = true)
    protected IdentificativoIPAType identificativoufficiofe;
    @XmlElement(name = "IDENTIFICATIVOFORNITORE")
    protected IdentificativoFornitoreFatturaType identificativofornitore;
    @XmlElement(name = "NUMEROFATTURA")
    protected String numerofattura;
    @XmlElement(name = "DATAFATTURA")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datafattura;
    @XmlElement(name = "IDENTIFICATIVOLOTTO")
    protected Long identificativolotto;
    @XmlElement(name = "DATAPRESAINCARICO")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar datapresaincarico;
    @XmlElement(name = "STATOFASCICOLO")
    @XmlSchemaType(name = "string")
    protected StatoDocumentoContabileType statofascicolo;
    @XmlElement(name = "REGISTRAZIONE")
    protected RegistroPROTOCOLLOType registrazione;
    @XmlElement(name = "FORMATOTRASMISSIONE")
    protected String formatotrasmissione;
    @XmlElement(name = "TIPODOCUMENTOCONTABILE")
    @XmlSchemaType(name = "string")
    protected TipoDocumentoContabileType tipodocumentocontabile;
    @XmlElement(name = "DECORRENZATERMINI")
    protected boolean decorrenzatermini;

    /**
     * Recupera il valore della proprietÓ identificativosdi.
     * 
     */
    public long getIDENTIFICATIVOSDI() {
        return identificativosdi;
    }

    /**
     * Imposta il valore della proprietÓ identificativosdi.
     * 
     */
    public void setIDENTIFICATIVOSDI(long value) {
        this.identificativosdi = value;
    }

    /**
     * Recupera il valore della proprietÓ identificativosife.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDENTIFICATIVOSIFE() {
        return identificativosife;
    }

    /**
     * Imposta il valore della proprietÓ identificativosife.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDENTIFICATIVOSIFE(String value) {
        this.identificativosife = value;
    }

    /**
     * Recupera il valore della proprietÓ identificativoufficiofe.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoIPAType }
     *     
     */
    public IdentificativoIPAType getIDENTIFICATIVOUFFICIOFE() {
        return identificativoufficiofe;
    }

    /**
     * Imposta il valore della proprietÓ identificativoufficiofe.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoIPAType }
     *     
     */
    public void setIDENTIFICATIVOUFFICIOFE(IdentificativoIPAType value) {
        this.identificativoufficiofe = value;
    }

    /**
     * Recupera il valore della proprietÓ identificativofornitore.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoFornitoreFatturaType }
     *     
     */
    public IdentificativoFornitoreFatturaType getIDENTIFICATIVOFORNITORE() {
        return identificativofornitore;
    }

    /**
     * Imposta il valore della proprietÓ identificativofornitore.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoFornitoreFatturaType }
     *     
     */
    public void setIDENTIFICATIVOFORNITORE(IdentificativoFornitoreFatturaType value) {
        this.identificativofornitore = value;
    }

    /**
     * Recupera il valore della proprietÓ numerofattura.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNUMEROFATTURA() {
        return numerofattura;
    }

    /**
     * Imposta il valore della proprietÓ numerofattura.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNUMEROFATTURA(String value) {
        this.numerofattura = value;
    }

    /**
     * Recupera il valore della proprietÓ datafattura.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATAFATTURA() {
        return datafattura;
    }

    /**
     * Imposta il valore della proprietÓ datafattura.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATAFATTURA(XMLGregorianCalendar value) {
        this.datafattura = value;
    }

    /**
     * Recupera il valore della proprietÓ identificativolotto.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getIDENTIFICATIVOLOTTO() {
        return identificativolotto;
    }

    /**
     * Imposta il valore della proprietÓ identificativolotto.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setIDENTIFICATIVOLOTTO(Long value) {
        this.identificativolotto = value;
    }

    /**
     * Recupera il valore della proprietÓ datapresaincarico.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATAPRESAINCARICO() {
        return datapresaincarico;
    }

    /**
     * Imposta il valore della proprietÓ datapresaincarico.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATAPRESAINCARICO(XMLGregorianCalendar value) {
        this.datapresaincarico = value;
    }

    /**
     * Recupera il valore della proprietÓ statofascicolo.
     * 
     * @return
     *     possible object is
     *     {@link StatoDocumentoContabileType }
     *     
     */
    public StatoDocumentoContabileType getSTATOFASCICOLO() {
        return statofascicolo;
    }

    /**
     * Imposta il valore della proprietÓ statofascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link StatoDocumentoContabileType }
     *     
     */
    public void setSTATOFASCICOLO(StatoDocumentoContabileType value) {
        this.statofascicolo = value;
    }

    /**
     * Recupera il valore della proprietÓ registrazione.
     * 
     * @return
     *     possible object is
     *     {@link RegistroPROTOCOLLOType }
     *     
     */
    public RegistroPROTOCOLLOType getREGISTRAZIONE() {
        return registrazione;
    }

    /**
     * Imposta il valore della proprietÓ registrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistroPROTOCOLLOType }
     *     
     */
    public void setREGISTRAZIONE(RegistroPROTOCOLLOType value) {
        this.registrazione = value;
    }

    /**
     * Recupera il valore della proprietÓ formatotrasmissione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFORMATOTRASMISSIONE() {
        return formatotrasmissione;
    }

    /**
     * Imposta il valore della proprietÓ formatotrasmissione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFORMATOTRASMISSIONE(String value) {
        this.formatotrasmissione = value;
    }

    /**
     * Recupera il valore della proprietÓ tipodocumentocontabile.
     * 
     * @return
     *     possible object is
     *     {@link TipoDocumentoContabileType }
     *     
     */
    public TipoDocumentoContabileType getTIPODOCUMENTOCONTABILE() {
        return tipodocumentocontabile;
    }

    /**
     * Imposta il valore della proprietÓ tipodocumentocontabile.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDocumentoContabileType }
     *     
     */
    public void setTIPODOCUMENTOCONTABILE(TipoDocumentoContabileType value) {
        this.tipodocumentocontabile = value;
    }

    /**
     * Recupera il valore della proprietÓ decorrenzatermini.
     * 
     */
    public boolean isDECORRENZATERMINI() {
        return decorrenzatermini;
    }

    /**
     * Imposta il valore della proprietÓ decorrenzatermini.
     * 
     */
    public void setDECORRENZATERMINI(boolean value) {
        this.decorrenzatermini = value;
    }

}
