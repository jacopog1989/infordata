//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Tipo documento Generico
 * 
 * <p>Classe Java per MetadatiDocumentoGenericoType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiDocumentoGenericoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDDOCUMENTO" type="{urn:fepa:metadata:v3}Guid"/>
 *         &lt;element name="METADATIASSOCIATI" type="{urn:fepa:metadata:v3}metaDatiAssociatiType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiDocumentoGenericoType", propOrder = {
    "iddocumento",
    "metadatiassociati"
})
public class MetadatiDocumentoGenericoType {

    @XmlElement(name = "IDDOCUMENTO", required = true)
    protected String iddocumento;
    @XmlElement(name = "METADATIASSOCIATI")
    protected MetaDatiAssociatiType metadatiassociati;

    /**
     * Recupera il valore della propriet� iddocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDDOCUMENTO() {
        return iddocumento;
    }

    /**
     * Imposta il valore della propriet� iddocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDDOCUMENTO(String value) {
        this.iddocumento = value;
    }

    /**
     * Recupera il valore della propriet� metadatiassociati.
     * 
     * @return
     *     possible object is
     *     {@link MetaDatiAssociatiType }
     *     
     */
    public MetaDatiAssociatiType getMETADATIASSOCIATI() {
        return metadatiassociati;
    }

    /**
     * Imposta il valore della propriet� metadatiassociati.
     * 
     * @param value
     *     allowed object is
     *     {@link MetaDatiAssociatiType }
     *     
     */
    public void setMETADATIASSOCIATI(MetaDatiAssociatiType value) {
        this.metadatiassociati = value;
    }

}
