//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Identificativo Decreto
 * 
 * <p>Classe Java per IdentificativoDecretoType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="IdentificativoDecretoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ANNO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CODIAMMINISTRAZIONE">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="UFFICIO1">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="UFFICIO2">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NUMERODECRETO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentificativoDecretoType", propOrder = {
    "anno",
    "codiamministrazione",
    "ufficio1",
    "ufficio2",
    "numerodecreto"
})
public class IdentificativoDecretoType {

    @XmlElement(name = "ANNO")
    protected int anno;
    @XmlElement(name = "CODIAMMINISTRAZIONE", required = true)
    protected String codiamministrazione;
    @XmlElement(name = "UFFICIO1", required = true)
    protected String ufficio1;
    @XmlElement(name = "UFFICIO2", required = true)
    protected String ufficio2;
    @XmlElement(name = "NUMERODECRETO")
    protected int numerodecreto;

    /**
     * Recupera il valore della propriet� anno.
     * 
     */
    public int getANNO() {
        return anno;
    }

    /**
     * Imposta il valore della propriet� anno.
     * 
     */
    public void setANNO(int value) {
        this.anno = value;
    }

    /**
     * Recupera il valore della propriet� codiamministrazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODIAMMINISTRAZIONE() {
        return codiamministrazione;
    }

    /**
     * Imposta il valore della propriet� codiamministrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODIAMMINISTRAZIONE(String value) {
        this.codiamministrazione = value;
    }

    /**
     * Recupera il valore della propriet� ufficio1.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUFFICIO1() {
        return ufficio1;
    }

    /**
     * Imposta il valore della propriet� ufficio1.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUFFICIO1(String value) {
        this.ufficio1 = value;
    }

    /**
     * Recupera il valore della propriet� ufficio2.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUFFICIO2() {
        return ufficio2;
    }

    /**
     * Imposta il valore della propriet� ufficio2.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUFFICIO2(String value) {
        this.ufficio2 = value;
    }

    /**
     * Recupera il valore della propriet� numerodecreto.
     * 
     */
    public int getNUMERODECRETO() {
        return numerodecreto;
    }

    /**
     * Imposta il valore della propriet� numerodecreto.
     * 
     */
    public void setNUMERODECRETO(int value) {
        this.numerodecreto = value;
    }

}
