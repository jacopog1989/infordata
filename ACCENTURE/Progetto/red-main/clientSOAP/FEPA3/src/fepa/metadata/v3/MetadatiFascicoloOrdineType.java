//
// Questo file Ŕ stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrÓ persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Fascicolo contenente l'ordine
 * 
 * <p>Classe Java per MetadatiFascicoloOrdineType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiFascicoloOrdineType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDCONTRATTO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IDORDINE" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ANNO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="NUMEROORDINE">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="20"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TIPOORDINE">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="DESRIZIONETIPOORDINE">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="UFFICIOIPA" type="{urn:fepa:metadata:v3}IdentificativoIPAType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiFascicoloOrdineType", propOrder = {
    "idcontratto",
    "idordine",
    "anno",
    "numeroordine",
    "tipoordine",
    "desrizionetipoordine",
    "ufficioipa"
})
public class MetadatiFascicoloOrdineType {

    @XmlElement(name = "IDCONTRATTO")
    protected int idcontratto;
    @XmlElement(name = "IDORDINE")
    protected int idordine;
    @XmlElement(name = "ANNO")
    protected int anno;
    @XmlElement(name = "NUMEROORDINE", required = true)
    protected String numeroordine;
    @XmlElement(name = "TIPOORDINE", required = true)
    protected String tipoordine;
    @XmlElement(name = "DESRIZIONETIPOORDINE", required = true)
    protected String desrizionetipoordine;
    @XmlElement(name = "UFFICIOIPA", required = true)
    protected IdentificativoIPAType ufficioipa;

    /**
     * Recupera il valore della proprietÓ idcontratto.
     * 
     */
    public int getIDCONTRATTO() {
        return idcontratto;
    }

    /**
     * Imposta il valore della proprietÓ idcontratto.
     * 
     */
    public void setIDCONTRATTO(int value) {
        this.idcontratto = value;
    }

    /**
     * Recupera il valore della proprietÓ idordine.
     * 
     */
    public int getIDORDINE() {
        return idordine;
    }

    /**
     * Imposta il valore della proprietÓ idordine.
     * 
     */
    public void setIDORDINE(int value) {
        this.idordine = value;
    }

    /**
     * Recupera il valore della proprietÓ anno.
     * 
     */
    public int getANNO() {
        return anno;
    }

    /**
     * Imposta il valore della proprietÓ anno.
     * 
     */
    public void setANNO(int value) {
        this.anno = value;
    }

    /**
     * Recupera il valore della proprietÓ numeroordine.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNUMEROORDINE() {
        return numeroordine;
    }

    /**
     * Imposta il valore della proprietÓ numeroordine.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNUMEROORDINE(String value) {
        this.numeroordine = value;
    }

    /**
     * Recupera il valore della proprietÓ tipoordine.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTIPOORDINE() {
        return tipoordine;
    }

    /**
     * Imposta il valore della proprietÓ tipoordine.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTIPOORDINE(String value) {
        this.tipoordine = value;
    }

    /**
     * Recupera il valore della proprietÓ desrizionetipoordine.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESRIZIONETIPOORDINE() {
        return desrizionetipoordine;
    }

    /**
     * Imposta il valore della proprietÓ desrizionetipoordine.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESRIZIONETIPOORDINE(String value) {
        this.desrizionetipoordine = value;
    }

    /**
     * Recupera il valore della proprietÓ ufficioipa.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoIPAType }
     *     
     */
    public IdentificativoIPAType getUFFICIOIPA() {
        return ufficioipa;
    }

    /**
     * Imposta il valore della proprietÓ ufficioipa.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoIPAType }
     *     
     */
    public void setUFFICIOIPA(IdentificativoIPAType value) {
        this.ufficioipa = value;
    }

}
