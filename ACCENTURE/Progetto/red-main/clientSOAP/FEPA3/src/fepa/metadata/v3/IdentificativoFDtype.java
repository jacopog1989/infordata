//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Identificativo del FunzionarioDelegato
 * 
 * <p>Classe Java per IdentificativoFDtype complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="IdentificativoFDtype">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AMMINISTRAZIONE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="QUALIFICA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ZONA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PROGRESSIVO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "IdentificativoFDtype", propOrder = {
    "amministrazione",
    "qualifica",
    "zona",
    "progressivo"
})
public class IdentificativoFDtype {

    @XmlElement(name = "AMMINISTRAZIONE", required = true)
    protected String amministrazione;
    @XmlElement(name = "QUALIFICA", required = true)
    protected String qualifica;
    @XmlElement(name = "ZONA", required = true)
    protected String zona;
    @XmlElement(name = "PROGRESSIVO", required = true)
    protected String progressivo;

    /**
     * Recupera il valore della propriet� amministrazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAMMINISTRAZIONE() {
        return amministrazione;
    }

    /**
     * Imposta il valore della propriet� amministrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAMMINISTRAZIONE(String value) {
        this.amministrazione = value;
    }

    /**
     * Recupera il valore della propriet� qualifica.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQUALIFICA() {
        return qualifica;
    }

    /**
     * Imposta il valore della propriet� qualifica.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQUALIFICA(String value) {
        this.qualifica = value;
    }

    /**
     * Recupera il valore della propriet� zona.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZONA() {
        return zona;
    }

    /**
     * Imposta il valore della propriet� zona.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZONA(String value) {
        this.zona = value;
    }

    /**
     * Recupera il valore della propriet� progressivo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPROGRESSIVO() {
        return progressivo;
    }

    /**
     * Imposta il valore della propriet� progressivo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPROGRESSIVO(String value) {
        this.progressivo = value;
    }

}
