//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Metadati Fascicolo dell'impegno
 * 
 * <p>Classe Java per MetadatiFascicoloImpegnoType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiFascicoloImpegnoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IMPEGNO" type="{urn:fepa:metadata:v3}IdentificativoClausolaType"/>
 *         &lt;element name="BENEFICIARIO" type="{urn:fepa:metadata:v3}IdentificativoBeneficiarioImpegnoType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiFascicoloImpegnoType", propOrder = {
    "impegno",
    "beneficiario"
})
public class MetadatiFascicoloImpegnoType {

    @XmlElement(name = "IMPEGNO", required = true)
    protected IdentificativoClausolaType impegno;
    @XmlElement(name = "BENEFICIARIO", required = true)
    protected IdentificativoBeneficiarioImpegnoType beneficiario;

    /**
     * Recupera il valore della propriet� impegno.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoClausolaType }
     *     
     */
    public IdentificativoClausolaType getIMPEGNO() {
        return impegno;
    }

    /**
     * Imposta il valore della propriet� impegno.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoClausolaType }
     *     
     */
    public void setIMPEGNO(IdentificativoClausolaType value) {
        this.impegno = value;
    }

    /**
     * Recupera il valore della propriet� beneficiario.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoBeneficiarioImpegnoType }
     *     
     */
    public IdentificativoBeneficiarioImpegnoType getBENEFICIARIO() {
        return beneficiario;
    }

    /**
     * Imposta il valore della propriet� beneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoBeneficiarioImpegnoType }
     *     
     */
    public void setBENEFICIARIO(IdentificativoBeneficiarioImpegnoType value) {
        this.beneficiario = value;
    }

}
