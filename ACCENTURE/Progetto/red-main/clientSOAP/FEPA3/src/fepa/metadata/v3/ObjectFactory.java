//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the fepa.metadata.v3 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MetadatiFascicoloOP_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloOP");
    private final static QName _MetadatiFascicoloAttoAnnullamento_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloAttoAnnullamento");
    private final static QName _MetadatiDocumentoLotto_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiDocumentoLotto");
    private final static QName _MetadatiDocumentoOP_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiDocumentoOP");
    private final static QName _MetadatiFascicoloOS_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloOS");
    private final static QName _MetadatiDocumentoServiziResi_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiDocumentoServiziResi");
    private final static QName _MetadatiFascicoloCE_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloCE");
    private final static QName _MetadatiFascicoloODP_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloODP");
    private final static QName _MetadatiDocumentoNotificaDecorrenzaTermini_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiDocumentoNotificaDecorrenzaTermini");
    private final static QName _MetadatiFascicoloFV_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloFV");
    private final static QName _MetadatiFascicoloDS_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloDS");
    private final static QName _MetadatiFascicoloSOP_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloSOP");
    private final static QName _MetadatiFascicoloDecretoSE_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloDecretoSE");
    private final static QName _MetadatiFascicoloOIL_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloOIL");
    private final static QName _MetadatiFascicoloReversale_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloReversale");
    private final static QName _MetadatiFascicoloOA_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloOA");
    private final static QName _MetadatiDocumentoSOP_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiDocumentoSOP");
    private final static QName _MetadatiFascicoloIPE_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloIPE");
    private final static QName _MetadatiFascicoloFD_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloFD");
    private final static QName _MetadatiFascicoloPN_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloPN");
    private final static QName _MetadatiFascicoloDecreto_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloDecreto");
    private final static QName _MetadatiFascicoloFB_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloFB");
    private final static QName _MetadatiFascicoloRendiconto_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloRendiconto");
    private final static QName _MetadatiDocumentoDD_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiDocumentoDD");
    private final static QName _MetadatiFascicoloDecretoRidVar_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloDecretoRidVar");
    private final static QName _MetadatiFascicoloMandato_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloMandato");
    private final static QName _MetadatiDocumentoFattura_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiDocumentoFattura");
    private final static QName _MetadatiDocumentoAllegatoFattura_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiDocumentoAllegatoFattura");
    private final static QName _MetadatiFascicoloImpegno_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloImpegno");
    private final static QName _MetadatiFascicoloCS_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloCS");
    private final static QName _MetadatiDocumentoGenerico_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiDocumentoGenerico");
    private final static QName _MetadatiFascicoloRichiestaAnnullamento_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloRichiestaAnnullamento");
    private final static QName _MetadatiFascicoloFattura_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloFattura");
    private final static QName _MetadatiFascicoloClausolaVariazione_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloClausolaVariazione");
    private final static QName _MetadatiFascicoloOrdine_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloOrdine");
    private final static QName _MetadatiFascicoloOSOld_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloOS_old");
    private final static QName _MetadatiFascicoloContratto_QNAME = new QName("urn:fepa:metadata:v3", "MetadatiFascicoloContratto");
    private final static QName _MetaDatiAssociatiTypeValore_QNAME = new QName("", "Valore");
    private final static QName _MetaDatiAssociatiTypeCodice_QNAME = new QName("", "Codice");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: fepa.metadata.v3
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MetadatiFascicoloOILType }
     * 
     */
    public MetadatiFascicoloOILType createMetadatiFascicoloOILType() {
        return new MetadatiFascicoloOILType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloSOPType }
     * 
     */
    public MetadatiFascicoloSOPType createMetadatiFascicoloSOPType() {
        return new MetadatiFascicoloSOPType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloDecretoSEType }
     * 
     */
    public MetadatiFascicoloDecretoSEType createMetadatiFascicoloDecretoSEType() {
        return new MetadatiFascicoloDecretoSEType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloDSType }
     * 
     */
    public MetadatiFascicoloDSType createMetadatiFascicoloDSType() {
        return new MetadatiFascicoloDSType();
    }

    /**
     * Create an instance of {@link MetadatiDocumentoNotificaDecorrenzaTerminiType }
     * 
     */
    public MetadatiDocumentoNotificaDecorrenzaTerminiType createMetadatiDocumentoNotificaDecorrenzaTerminiType() {
        return new MetadatiDocumentoNotificaDecorrenzaTerminiType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloFVType }
     * 
     */
    public MetadatiFascicoloFVType createMetadatiFascicoloFVType() {
        return new MetadatiFascicoloFVType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloOAType }
     * 
     */
    public MetadatiFascicoloOAType createMetadatiFascicoloOAType() {
        return new MetadatiFascicoloOAType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloReversaleType }
     * 
     */
    public MetadatiFascicoloReversaleType createMetadatiFascicoloReversaleType() {
        return new MetadatiFascicoloReversaleType();
    }

    /**
     * Create an instance of {@link MetadatiDocumentoLottoType }
     * 
     */
    public MetadatiDocumentoLottoType createMetadatiDocumentoLottoType() {
        return new MetadatiDocumentoLottoType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloAttoAnnullamentoType }
     * 
     */
    public MetadatiFascicoloAttoAnnullamentoType createMetadatiFascicoloAttoAnnullamentoType() {
        return new MetadatiFascicoloAttoAnnullamentoType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloOPType }
     * 
     */
    public MetadatiFascicoloOPType createMetadatiFascicoloOPType() {
        return new MetadatiFascicoloOPType();
    }

    /**
     * Create an instance of {@link MetadatiDocumentoServiziResiType }
     * 
     */
    public MetadatiDocumentoServiziResiType createMetadatiDocumentoServiziResiType() {
        return new MetadatiDocumentoServiziResiType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloCEType }
     * 
     */
    public MetadatiFascicoloCEType createMetadatiFascicoloCEType() {
        return new MetadatiFascicoloCEType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloODPType }
     * 
     */
    public MetadatiFascicoloODPType createMetadatiFascicoloODPType() {
        return new MetadatiFascicoloODPType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloOSType }
     * 
     */
    public MetadatiFascicoloOSType createMetadatiFascicoloOSType() {
        return new MetadatiFascicoloOSType();
    }

    /**
     * Create an instance of {@link MetadatiDocumentoOPType }
     * 
     */
    public MetadatiDocumentoOPType createMetadatiDocumentoOPType() {
        return new MetadatiDocumentoOPType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloRichiestaAnnullamentoType }
     * 
     */
    public MetadatiFascicoloRichiestaAnnullamentoType createMetadatiFascicoloRichiestaAnnullamentoType() {
        return new MetadatiFascicoloRichiestaAnnullamentoType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloFatturaType }
     * 
     */
    public MetadatiFascicoloFatturaType createMetadatiFascicoloFatturaType() {
        return new MetadatiFascicoloFatturaType();
    }

    /**
     * Create an instance of {@link MetadatiDocumentoGenericoType }
     * 
     */
    public MetadatiDocumentoGenericoType createMetadatiDocumentoGenericoType() {
        return new MetadatiDocumentoGenericoType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloImpegnoType }
     * 
     */
    public MetadatiFascicoloImpegnoType createMetadatiFascicoloImpegnoType() {
        return new MetadatiFascicoloImpegnoType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloCSType }
     * 
     */
    public MetadatiFascicoloCSType createMetadatiFascicoloCSType() {
        return new MetadatiFascicoloCSType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloOrdineType }
     * 
     */
    public MetadatiFascicoloOrdineType createMetadatiFascicoloOrdineType() {
        return new MetadatiFascicoloOrdineType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloOSOldType }
     * 
     */
    public MetadatiFascicoloOSOldType createMetadatiFascicoloOSOldType() {
        return new MetadatiFascicoloOSOldType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloContrattoType }
     * 
     */
    public MetadatiFascicoloContrattoType createMetadatiFascicoloContrattoType() {
        return new MetadatiFascicoloContrattoType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloClausolaVariazioneType }
     * 
     */
    public MetadatiFascicoloClausolaVariazioneType createMetadatiFascicoloClausolaVariazioneType() {
        return new MetadatiFascicoloClausolaVariazioneType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloRendicontoType }
     * 
     */
    public MetadatiFascicoloRendicontoType createMetadatiFascicoloRendicontoType() {
        return new MetadatiFascicoloRendicontoType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloDecretoType }
     * 
     */
    public MetadatiFascicoloDecretoType createMetadatiFascicoloDecretoType() {
        return new MetadatiFascicoloDecretoType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloFBType }
     * 
     */
    public MetadatiFascicoloFBType createMetadatiFascicoloFBType() {
        return new MetadatiFascicoloFBType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloIPEType }
     * 
     */
    public MetadatiFascicoloIPEType createMetadatiFascicoloIPEType() {
        return new MetadatiFascicoloIPEType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloFDType }
     * 
     */
    public MetadatiFascicoloFDType createMetadatiFascicoloFDType() {
        return new MetadatiFascicoloFDType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloPNType }
     * 
     */
    public MetadatiFascicoloPNType createMetadatiFascicoloPNType() {
        return new MetadatiFascicoloPNType();
    }

    /**
     * Create an instance of {@link MetadatiDocumentoSOPType }
     * 
     */
    public MetadatiDocumentoSOPType createMetadatiDocumentoSOPType() {
        return new MetadatiDocumentoSOPType();
    }

    /**
     * Create an instance of {@link MetadatiDocumentoAllegatoFatturaType }
     * 
     */
    public MetadatiDocumentoAllegatoFatturaType createMetadatiDocumentoAllegatoFatturaType() {
        return new MetadatiDocumentoAllegatoFatturaType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloDecretoRidVarType }
     * 
     */
    public MetadatiFascicoloDecretoRidVarType createMetadatiFascicoloDecretoRidVarType() {
        return new MetadatiFascicoloDecretoRidVarType();
    }

    /**
     * Create an instance of {@link MetadatiFascicoloMandatoType }
     * 
     */
    public MetadatiFascicoloMandatoType createMetadatiFascicoloMandatoType() {
        return new MetadatiFascicoloMandatoType();
    }

    /**
     * Create an instance of {@link MetadatiDocumentoFatturaType }
     * 
     */
    public MetadatiDocumentoFatturaType createMetadatiDocumentoFatturaType() {
        return new MetadatiDocumentoFatturaType();
    }

    /**
     * Create an instance of {@link MetadatiDocumentoDDType }
     * 
     */
    public MetadatiDocumentoDDType createMetadatiDocumentoDDType() {
        return new MetadatiDocumentoDDType();
    }

    /**
     * Create an instance of {@link IdentificativoIPEType }
     * 
     */
    public IdentificativoIPEType createIdentificativoIPEType() {
        return new IdentificativoIPEType();
    }

    /**
     * Create an instance of {@link ChiaveFDType }
     * 
     */
    public ChiaveFDType createChiaveFDType() {
        return new ChiaveFDType();
    }

    /**
     * Create an instance of {@link IdentificativoClausolaType }
     * 
     */
    public IdentificativoClausolaType createIdentificativoClausolaType() {
        return new IdentificativoClausolaType();
    }

    /**
     * Create an instance of {@link ChiaveTitoloCSType }
     * 
     */
    public ChiaveTitoloCSType createChiaveTitoloCSType() {
        return new ChiaveTitoloCSType();
    }

    /**
     * Create an instance of {@link IdentificativoBeneficiarioCSType }
     * 
     */
    public IdentificativoBeneficiarioCSType createIdentificativoBeneficiarioCSType() {
        return new IdentificativoBeneficiarioCSType();
    }

    /**
     * Create an instance of {@link IdentificativoPCOType }
     * 
     */
    public IdentificativoPCOType createIdentificativoPCOType() {
        return new IdentificativoPCOType();
    }

    /**
     * Create an instance of {@link RegistroPROTOCOLLOType }
     * 
     */
    public RegistroPROTOCOLLOType createRegistroPROTOCOLLOType() {
        return new RegistroPROTOCOLLOType();
    }

    /**
     * Create an instance of {@link MetaDatiAssociatiType }
     * 
     */
    public MetaDatiAssociatiType createMetaDatiAssociatiType() {
        return new MetaDatiAssociatiType();
    }

    /**
     * Create an instance of {@link ENTEType }
     * 
     */
    public ENTEType createENTEType() {
        return new ENTEType();
    }

    /**
     * Create an instance of {@link IdentificativoFornitoreFatturaType }
     * 
     */
    public IdentificativoFornitoreFatturaType createIdentificativoFornitoreFatturaType() {
        return new IdentificativoFornitoreFatturaType();
    }

    /**
     * Create an instance of {@link IdentificativoBeneficiarioSOPType }
     * 
     */
    public IdentificativoBeneficiarioSOPType createIdentificativoBeneficiarioSOPType() {
        return new IdentificativoBeneficiarioSOPType();
    }

    /**
     * Create an instance of {@link IdentificativoBeneficiarioImpegnoType }
     * 
     */
    public IdentificativoBeneficiarioImpegnoType createIdentificativoBeneficiarioImpegnoType() {
        return new IdentificativoBeneficiarioImpegnoType();
    }

    /**
     * Create an instance of {@link IdentificativoFDtype }
     * 
     */
    public IdentificativoFDtype createIdentificativoFDtype() {
        return new IdentificativoFDtype();
    }

    /**
     * Create an instance of {@link IdentificativoIPAType }
     * 
     */
    public IdentificativoIPAType createIdentificativoIPAType() {
        return new IdentificativoIPAType();
    }

    /**
     * Create an instance of {@link IdentificativoDecretoType }
     * 
     */
    public IdentificativoDecretoType createIdentificativoDecretoType() {
        return new IdentificativoDecretoType();
    }

    /**
     * Create an instance of {@link IdentificativoPNIType }
     * 
     */
    public IdentificativoPNIType createIdentificativoPNIType() {
        return new IdentificativoPNIType();
    }

    /**
     * Create an instance of {@link IdentificativoBeneficiarioOPType }
     * 
     */
    public IdentificativoBeneficiarioOPType createIdentificativoBeneficiarioOPType() {
        return new IdentificativoBeneficiarioOPType();
    }

    /**
     * Create an instance of {@link ChiaveFunzionarioDelegatoType }
     * 
     */
    public ChiaveFunzionarioDelegatoType createChiaveFunzionarioDelegatoType() {
        return new ChiaveFunzionarioDelegatoType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloOPType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloOP")
    public JAXBElement<MetadatiFascicoloOPType> createMetadatiFascicoloOP(MetadatiFascicoloOPType value) {
        return new JAXBElement<MetadatiFascicoloOPType>(_MetadatiFascicoloOP_QNAME, MetadatiFascicoloOPType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloAttoAnnullamentoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloAttoAnnullamento")
    public JAXBElement<MetadatiFascicoloAttoAnnullamentoType> createMetadatiFascicoloAttoAnnullamento(MetadatiFascicoloAttoAnnullamentoType value) {
        return new JAXBElement<MetadatiFascicoloAttoAnnullamentoType>(_MetadatiFascicoloAttoAnnullamento_QNAME, MetadatiFascicoloAttoAnnullamentoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiDocumentoLottoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiDocumentoLotto")
    public JAXBElement<MetadatiDocumentoLottoType> createMetadatiDocumentoLotto(MetadatiDocumentoLottoType value) {
        return new JAXBElement<MetadatiDocumentoLottoType>(_MetadatiDocumentoLotto_QNAME, MetadatiDocumentoLottoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiDocumentoOPType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiDocumentoOP")
    public JAXBElement<MetadatiDocumentoOPType> createMetadatiDocumentoOP(MetadatiDocumentoOPType value) {
        return new JAXBElement<MetadatiDocumentoOPType>(_MetadatiDocumentoOP_QNAME, MetadatiDocumentoOPType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloOSType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloOS")
    public JAXBElement<MetadatiFascicoloOSType> createMetadatiFascicoloOS(MetadatiFascicoloOSType value) {
        return new JAXBElement<MetadatiFascicoloOSType>(_MetadatiFascicoloOS_QNAME, MetadatiFascicoloOSType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiDocumentoServiziResiType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiDocumentoServiziResi")
    public JAXBElement<MetadatiDocumentoServiziResiType> createMetadatiDocumentoServiziResi(MetadatiDocumentoServiziResiType value) {
        return new JAXBElement<MetadatiDocumentoServiziResiType>(_MetadatiDocumentoServiziResi_QNAME, MetadatiDocumentoServiziResiType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloCEType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloCE")
    public JAXBElement<MetadatiFascicoloCEType> createMetadatiFascicoloCE(MetadatiFascicoloCEType value) {
        return new JAXBElement<MetadatiFascicoloCEType>(_MetadatiFascicoloCE_QNAME, MetadatiFascicoloCEType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloODPType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloODP")
    public JAXBElement<MetadatiFascicoloODPType> createMetadatiFascicoloODP(MetadatiFascicoloODPType value) {
        return new JAXBElement<MetadatiFascicoloODPType>(_MetadatiFascicoloODP_QNAME, MetadatiFascicoloODPType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiDocumentoNotificaDecorrenzaTerminiType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiDocumentoNotificaDecorrenzaTermini")
    public JAXBElement<MetadatiDocumentoNotificaDecorrenzaTerminiType> createMetadatiDocumentoNotificaDecorrenzaTermini(MetadatiDocumentoNotificaDecorrenzaTerminiType value) {
        return new JAXBElement<MetadatiDocumentoNotificaDecorrenzaTerminiType>(_MetadatiDocumentoNotificaDecorrenzaTermini_QNAME, MetadatiDocumentoNotificaDecorrenzaTerminiType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloFVType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloFV")
    public JAXBElement<MetadatiFascicoloFVType> createMetadatiFascicoloFV(MetadatiFascicoloFVType value) {
        return new JAXBElement<MetadatiFascicoloFVType>(_MetadatiFascicoloFV_QNAME, MetadatiFascicoloFVType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloDSType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloDS")
    public JAXBElement<MetadatiFascicoloDSType> createMetadatiFascicoloDS(MetadatiFascicoloDSType value) {
        return new JAXBElement<MetadatiFascicoloDSType>(_MetadatiFascicoloDS_QNAME, MetadatiFascicoloDSType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloSOPType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloSOP")
    public JAXBElement<MetadatiFascicoloSOPType> createMetadatiFascicoloSOP(MetadatiFascicoloSOPType value) {
        return new JAXBElement<MetadatiFascicoloSOPType>(_MetadatiFascicoloSOP_QNAME, MetadatiFascicoloSOPType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloDecretoSEType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloDecretoSE")
    public JAXBElement<MetadatiFascicoloDecretoSEType> createMetadatiFascicoloDecretoSE(MetadatiFascicoloDecretoSEType value) {
        return new JAXBElement<MetadatiFascicoloDecretoSEType>(_MetadatiFascicoloDecretoSE_QNAME, MetadatiFascicoloDecretoSEType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloOILType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloOIL")
    public JAXBElement<MetadatiFascicoloOILType> createMetadatiFascicoloOIL(MetadatiFascicoloOILType value) {
        return new JAXBElement<MetadatiFascicoloOILType>(_MetadatiFascicoloOIL_QNAME, MetadatiFascicoloOILType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloReversaleType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloReversale")
    public JAXBElement<MetadatiFascicoloReversaleType> createMetadatiFascicoloReversale(MetadatiFascicoloReversaleType value) {
        return new JAXBElement<MetadatiFascicoloReversaleType>(_MetadatiFascicoloReversale_QNAME, MetadatiFascicoloReversaleType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloOAType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloOA")
    public JAXBElement<MetadatiFascicoloOAType> createMetadatiFascicoloOA(MetadatiFascicoloOAType value) {
        return new JAXBElement<MetadatiFascicoloOAType>(_MetadatiFascicoloOA_QNAME, MetadatiFascicoloOAType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiDocumentoSOPType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiDocumentoSOP")
    public JAXBElement<MetadatiDocumentoSOPType> createMetadatiDocumentoSOP(MetadatiDocumentoSOPType value) {
        return new JAXBElement<MetadatiDocumentoSOPType>(_MetadatiDocumentoSOP_QNAME, MetadatiDocumentoSOPType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloIPEType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloIPE")
    public JAXBElement<MetadatiFascicoloIPEType> createMetadatiFascicoloIPE(MetadatiFascicoloIPEType value) {
        return new JAXBElement<MetadatiFascicoloIPEType>(_MetadatiFascicoloIPE_QNAME, MetadatiFascicoloIPEType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloFDType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloFD")
    public JAXBElement<MetadatiFascicoloFDType> createMetadatiFascicoloFD(MetadatiFascicoloFDType value) {
        return new JAXBElement<MetadatiFascicoloFDType>(_MetadatiFascicoloFD_QNAME, MetadatiFascicoloFDType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloPNType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloPN")
    public JAXBElement<MetadatiFascicoloPNType> createMetadatiFascicoloPN(MetadatiFascicoloPNType value) {
        return new JAXBElement<MetadatiFascicoloPNType>(_MetadatiFascicoloPN_QNAME, MetadatiFascicoloPNType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloDecretoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloDecreto")
    public JAXBElement<MetadatiFascicoloDecretoType> createMetadatiFascicoloDecreto(MetadatiFascicoloDecretoType value) {
        return new JAXBElement<MetadatiFascicoloDecretoType>(_MetadatiFascicoloDecreto_QNAME, MetadatiFascicoloDecretoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloFBType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloFB")
    public JAXBElement<MetadatiFascicoloFBType> createMetadatiFascicoloFB(MetadatiFascicoloFBType value) {
        return new JAXBElement<MetadatiFascicoloFBType>(_MetadatiFascicoloFB_QNAME, MetadatiFascicoloFBType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloRendicontoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloRendiconto")
    public JAXBElement<MetadatiFascicoloRendicontoType> createMetadatiFascicoloRendiconto(MetadatiFascicoloRendicontoType value) {
        return new JAXBElement<MetadatiFascicoloRendicontoType>(_MetadatiFascicoloRendiconto_QNAME, MetadatiFascicoloRendicontoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiDocumentoDDType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiDocumentoDD")
    public JAXBElement<MetadatiDocumentoDDType> createMetadatiDocumentoDD(MetadatiDocumentoDDType value) {
        return new JAXBElement<MetadatiDocumentoDDType>(_MetadatiDocumentoDD_QNAME, MetadatiDocumentoDDType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloDecretoRidVarType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloDecretoRidVar")
    public JAXBElement<MetadatiFascicoloDecretoRidVarType> createMetadatiFascicoloDecretoRidVar(MetadatiFascicoloDecretoRidVarType value) {
        return new JAXBElement<MetadatiFascicoloDecretoRidVarType>(_MetadatiFascicoloDecretoRidVar_QNAME, MetadatiFascicoloDecretoRidVarType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloMandatoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloMandato")
    public JAXBElement<MetadatiFascicoloMandatoType> createMetadatiFascicoloMandato(MetadatiFascicoloMandatoType value) {
        return new JAXBElement<MetadatiFascicoloMandatoType>(_MetadatiFascicoloMandato_QNAME, MetadatiFascicoloMandatoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiDocumentoFatturaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiDocumentoFattura")
    public JAXBElement<MetadatiDocumentoFatturaType> createMetadatiDocumentoFattura(MetadatiDocumentoFatturaType value) {
        return new JAXBElement<MetadatiDocumentoFatturaType>(_MetadatiDocumentoFattura_QNAME, MetadatiDocumentoFatturaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiDocumentoAllegatoFatturaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiDocumentoAllegatoFattura")
    public JAXBElement<MetadatiDocumentoAllegatoFatturaType> createMetadatiDocumentoAllegatoFattura(MetadatiDocumentoAllegatoFatturaType value) {
        return new JAXBElement<MetadatiDocumentoAllegatoFatturaType>(_MetadatiDocumentoAllegatoFattura_QNAME, MetadatiDocumentoAllegatoFatturaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloImpegnoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloImpegno")
    public JAXBElement<MetadatiFascicoloImpegnoType> createMetadatiFascicoloImpegno(MetadatiFascicoloImpegnoType value) {
        return new JAXBElement<MetadatiFascicoloImpegnoType>(_MetadatiFascicoloImpegno_QNAME, MetadatiFascicoloImpegnoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloCSType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloCS")
    public JAXBElement<MetadatiFascicoloCSType> createMetadatiFascicoloCS(MetadatiFascicoloCSType value) {
        return new JAXBElement<MetadatiFascicoloCSType>(_MetadatiFascicoloCS_QNAME, MetadatiFascicoloCSType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiDocumentoGenericoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiDocumentoGenerico")
    public JAXBElement<MetadatiDocumentoGenericoType> createMetadatiDocumentoGenerico(MetadatiDocumentoGenericoType value) {
        return new JAXBElement<MetadatiDocumentoGenericoType>(_MetadatiDocumentoGenerico_QNAME, MetadatiDocumentoGenericoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloRichiestaAnnullamentoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloRichiestaAnnullamento")
    public JAXBElement<MetadatiFascicoloRichiestaAnnullamentoType> createMetadatiFascicoloRichiestaAnnullamento(MetadatiFascicoloRichiestaAnnullamentoType value) {
        return new JAXBElement<MetadatiFascicoloRichiestaAnnullamentoType>(_MetadatiFascicoloRichiestaAnnullamento_QNAME, MetadatiFascicoloRichiestaAnnullamentoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloFatturaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloFattura")
    public JAXBElement<MetadatiFascicoloFatturaType> createMetadatiFascicoloFattura(MetadatiFascicoloFatturaType value) {
        return new JAXBElement<MetadatiFascicoloFatturaType>(_MetadatiFascicoloFattura_QNAME, MetadatiFascicoloFatturaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloClausolaVariazioneType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloClausolaVariazione")
    public JAXBElement<MetadatiFascicoloClausolaVariazioneType> createMetadatiFascicoloClausolaVariazione(MetadatiFascicoloClausolaVariazioneType value) {
        return new JAXBElement<MetadatiFascicoloClausolaVariazioneType>(_MetadatiFascicoloClausolaVariazione_QNAME, MetadatiFascicoloClausolaVariazioneType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloOrdineType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloOrdine")
    public JAXBElement<MetadatiFascicoloOrdineType> createMetadatiFascicoloOrdine(MetadatiFascicoloOrdineType value) {
        return new JAXBElement<MetadatiFascicoloOrdineType>(_MetadatiFascicoloOrdine_QNAME, MetadatiFascicoloOrdineType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloOSOldType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloOS_old")
    public JAXBElement<MetadatiFascicoloOSOldType> createMetadatiFascicoloOSOld(MetadatiFascicoloOSOldType value) {
        return new JAXBElement<MetadatiFascicoloOSOldType>(_MetadatiFascicoloOSOld_QNAME, MetadatiFascicoloOSOldType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MetadatiFascicoloContrattoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:metadata:v3", name = "MetadatiFascicoloContratto")
    public JAXBElement<MetadatiFascicoloContrattoType> createMetadatiFascicoloContratto(MetadatiFascicoloContrattoType value) {
        return new JAXBElement<MetadatiFascicoloContrattoType>(_MetadatiFascicoloContratto_QNAME, MetadatiFascicoloContrattoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Valore", scope = MetaDatiAssociatiType.class)
    public JAXBElement<String> createMetaDatiAssociatiTypeValore(String value) {
        return new JAXBElement<String>(_MetaDatiAssociatiTypeValore_QNAME, String.class, MetaDatiAssociatiType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Codice", scope = MetaDatiAssociatiType.class)
    public JAXBElement<String> createMetaDatiAssociatiTypeCodice(String value) {
        return new JAXBElement<String>(_MetaDatiAssociatiTypeCodice_QNAME, String.class, MetaDatiAssociatiType.class, value);
    }

}
