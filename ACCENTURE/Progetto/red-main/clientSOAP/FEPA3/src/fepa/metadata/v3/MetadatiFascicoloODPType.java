//
// Questo file Ŕ stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrÓ persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Fascicolo contenente l'Ordine 
 * 			
 * 
 * <p>Classe Java per MetadatiFascicoloODPType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiFascicoloODPType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ANNO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="NUMEROTITOLO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CHIAVEFD" type="{urn:fepa:metadata:v3}chiaveFD_type"/>
 *         &lt;element name="IDENTIFICATIVOSIFE" type="{urn:fepa:metadata:v3}IdentificativoDocumentoContabileType"/>
 *         &lt;element name="DESCRIZIONEFUNZIONARIODELEGATO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CODICEUFFICIOSUBFD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OGGETTOSPESA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BENIFICIARIO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CODICEFISCALE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PARTITAIVA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DATAEMISSIONE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AMMINISTRAZIONEOPERANTE" type="{urn:fepa:metadata:v3}ENTE_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiFascicoloODPType", propOrder = {
    "anno",
    "numerotitolo",
    "chiavefd",
    "identificativosife",
    "descrizionefunzionariodelegato",
    "codiceufficiosubfd",
    "oggettospesa",
    "benificiario",
    "codicefiscale",
    "partitaiva",
    "dataemissione",
    "amministrazioneoperante"
})
public class MetadatiFascicoloODPType {

    @XmlElement(name = "ANNO")
    protected int anno;
    @XmlElement(name = "NUMEROTITOLO", required = true)
    protected String numerotitolo;
    @XmlElement(name = "CHIAVEFD", required = true)
    protected ChiaveFDType chiavefd;
    @XmlElement(name = "IDENTIFICATIVOSIFE", required = true)
    protected String identificativosife;
    @XmlElement(name = "DESCRIZIONEFUNZIONARIODELEGATO", required = true)
    protected String descrizionefunzionariodelegato;
    @XmlElement(name = "CODICEUFFICIOSUBFD", required = true)
    protected String codiceufficiosubfd;
    @XmlElement(name = "OGGETTOSPESA", required = true)
    protected String oggettospesa;
    @XmlElement(name = "BENIFICIARIO", required = true)
    protected String benificiario;
    @XmlElement(name = "CODICEFISCALE")
    protected String codicefiscale;
    @XmlElement(name = "PARTITAIVA")
    protected String partitaiva;
    @XmlElement(name = "DATAEMISSIONE", required = true)
    protected String dataemissione;
    @XmlElement(name = "AMMINISTRAZIONEOPERANTE", required = true)
    protected ENTEType amministrazioneoperante;

    /**
     * Recupera il valore della proprietÓ anno.
     * 
     */
    public int getANNO() {
        return anno;
    }

    /**
     * Imposta il valore della proprietÓ anno.
     * 
     */
    public void setANNO(int value) {
        this.anno = value;
    }

    /**
     * Recupera il valore della proprietÓ numerotitolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNUMEROTITOLO() {
        return numerotitolo;
    }

    /**
     * Imposta il valore della proprietÓ numerotitolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNUMEROTITOLO(String value) {
        this.numerotitolo = value;
    }

    /**
     * Recupera il valore della proprietÓ chiavefd.
     * 
     * @return
     *     possible object is
     *     {@link ChiaveFDType }
     *     
     */
    public ChiaveFDType getCHIAVEFD() {
        return chiavefd;
    }

    /**
     * Imposta il valore della proprietÓ chiavefd.
     * 
     * @param value
     *     allowed object is
     *     {@link ChiaveFDType }
     *     
     */
    public void setCHIAVEFD(ChiaveFDType value) {
        this.chiavefd = value;
    }

    /**
     * Recupera il valore della proprietÓ identificativosife.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDENTIFICATIVOSIFE() {
        return identificativosife;
    }

    /**
     * Imposta il valore della proprietÓ identificativosife.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDENTIFICATIVOSIFE(String value) {
        this.identificativosife = value;
    }

    /**
     * Recupera il valore della proprietÓ descrizionefunzionariodelegato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESCRIZIONEFUNZIONARIODELEGATO() {
        return descrizionefunzionariodelegato;
    }

    /**
     * Imposta il valore della proprietÓ descrizionefunzionariodelegato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESCRIZIONEFUNZIONARIODELEGATO(String value) {
        this.descrizionefunzionariodelegato = value;
    }

    /**
     * Recupera il valore della proprietÓ codiceufficiosubfd.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODICEUFFICIOSUBFD() {
        return codiceufficiosubfd;
    }

    /**
     * Imposta il valore della proprietÓ codiceufficiosubfd.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODICEUFFICIOSUBFD(String value) {
        this.codiceufficiosubfd = value;
    }

    /**
     * Recupera il valore della proprietÓ oggettospesa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOGGETTOSPESA() {
        return oggettospesa;
    }

    /**
     * Imposta il valore della proprietÓ oggettospesa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOGGETTOSPESA(String value) {
        this.oggettospesa = value;
    }

    /**
     * Recupera il valore della proprietÓ benificiario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBENIFICIARIO() {
        return benificiario;
    }

    /**
     * Imposta il valore della proprietÓ benificiario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBENIFICIARIO(String value) {
        this.benificiario = value;
    }

    /**
     * Recupera il valore della proprietÓ codicefiscale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODICEFISCALE() {
        return codicefiscale;
    }

    /**
     * Imposta il valore della proprietÓ codicefiscale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODICEFISCALE(String value) {
        this.codicefiscale = value;
    }

    /**
     * Recupera il valore della proprietÓ partitaiva.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPARTITAIVA() {
        return partitaiva;
    }

    /**
     * Imposta il valore della proprietÓ partitaiva.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPARTITAIVA(String value) {
        this.partitaiva = value;
    }

    /**
     * Recupera il valore della proprietÓ dataemissione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDATAEMISSIONE() {
        return dataemissione;
    }

    /**
     * Imposta il valore della proprietÓ dataemissione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDATAEMISSIONE(String value) {
        this.dataemissione = value;
    }

    /**
     * Recupera il valore della proprietÓ amministrazioneoperante.
     * 
     * @return
     *     possible object is
     *     {@link ENTEType }
     *     
     */
    public ENTEType getAMMINISTRAZIONEOPERANTE() {
        return amministrazioneoperante;
    }

    /**
     * Imposta il valore della proprietÓ amministrazioneoperante.
     * 
     * @param value
     *     allowed object is
     *     {@link ENTEType }
     *     
     */
    public void setAMMINISTRAZIONEOPERANTE(ENTEType value) {
        this.amministrazioneoperante = value;
    }

}
