//
// Questo file Ŕ stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrÓ persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Fascicolo contenente la richiesta di annullamento
 * 			
 * 
 * <p>Classe Java per MetadatiFascicoloRichiestaAnnullamentoType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiFascicoloRichiestaAnnullamentoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ANNO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CODIAMMINISTRAZIONE">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AMMINISTRAZIONE">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CODICERAGIONERIA">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="RAGIONERIA">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NUMERO_ANNULLAMENTO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ANNO_OA" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CAPITOLO_OA" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CODIAMMINISTRAZIONE_OA">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CODICERAGIONERIA_OA">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NUMERO_OA" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="NUMERO_DEF_RGS_OA" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CAUSALE">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="60"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TIPO_OA" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DATAEMISSIONE_OA" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="DATAEMISSIONE_RICH_ANN" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiFascicoloRichiestaAnnullamentoType", propOrder = {
    "anno",
    "codiamministrazione",
    "amministrazione",
    "codiceragioneria",
    "ragioneria",
    "numeroannullamento",
    "annooa",
    "capitolooa",
    "codiamministrazioneoa",
    "codiceragioneriaoa",
    "numerooa",
    "numerodefrgsoa",
    "causale",
    "tipooa",
    "dataemissioneoa",
    "dataemissionerichann"
})
public class MetadatiFascicoloRichiestaAnnullamentoType {

    @XmlElement(name = "ANNO")
    protected int anno;
    @XmlElement(name = "CODIAMMINISTRAZIONE", required = true)
    protected String codiamministrazione;
    @XmlElement(name = "AMMINISTRAZIONE", required = true)
    protected String amministrazione;
    @XmlElement(name = "CODICERAGIONERIA", required = true)
    protected String codiceragioneria;
    @XmlElement(name = "RAGIONERIA", required = true)
    protected String ragioneria;
    @XmlElement(name = "NUMERO_ANNULLAMENTO")
    protected int numeroannullamento;
    @XmlElement(name = "ANNO_OA")
    protected int annooa;
    @XmlElement(name = "CAPITOLO_OA")
    protected int capitolooa;
    @XmlElement(name = "CODIAMMINISTRAZIONE_OA", required = true)
    protected String codiamministrazioneoa;
    @XmlElement(name = "CODICERAGIONERIA_OA", required = true)
    protected String codiceragioneriaoa;
    @XmlElement(name = "NUMERO_OA")
    protected int numerooa;
    @XmlElement(name = "NUMERO_DEF_RGS_OA")
    protected int numerodefrgsoa;
    @XmlElement(name = "CAUSALE", required = true)
    protected String causale;
    @XmlElement(name = "TIPO_OA")
    protected int tipooa;
    @XmlElement(name = "DATAEMISSIONE_OA", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataemissioneoa;
    @XmlElement(name = "DATAEMISSIONE_RICH_ANN", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataemissionerichann;

    /**
     * Recupera il valore della proprietÓ anno.
     * 
     */
    public int getANNO() {
        return anno;
    }

    /**
     * Imposta il valore della proprietÓ anno.
     * 
     */
    public void setANNO(int value) {
        this.anno = value;
    }

    /**
     * Recupera il valore della proprietÓ codiamministrazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODIAMMINISTRAZIONE() {
        return codiamministrazione;
    }

    /**
     * Imposta il valore della proprietÓ codiamministrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODIAMMINISTRAZIONE(String value) {
        this.codiamministrazione = value;
    }

    /**
     * Recupera il valore della proprietÓ amministrazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAMMINISTRAZIONE() {
        return amministrazione;
    }

    /**
     * Imposta il valore della proprietÓ amministrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAMMINISTRAZIONE(String value) {
        this.amministrazione = value;
    }

    /**
     * Recupera il valore della proprietÓ codiceragioneria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODICERAGIONERIA() {
        return codiceragioneria;
    }

    /**
     * Imposta il valore della proprietÓ codiceragioneria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODICERAGIONERIA(String value) {
        this.codiceragioneria = value;
    }

    /**
     * Recupera il valore della proprietÓ ragioneria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRAGIONERIA() {
        return ragioneria;
    }

    /**
     * Imposta il valore della proprietÓ ragioneria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRAGIONERIA(String value) {
        this.ragioneria = value;
    }

    /**
     * Recupera il valore della proprietÓ numeroannullamento.
     * 
     */
    public int getNUMEROANNULLAMENTO() {
        return numeroannullamento;
    }

    /**
     * Imposta il valore della proprietÓ numeroannullamento.
     * 
     */
    public void setNUMEROANNULLAMENTO(int value) {
        this.numeroannullamento = value;
    }

    /**
     * Recupera il valore della proprietÓ annooa.
     * 
     */
    public int getANNOOA() {
        return annooa;
    }

    /**
     * Imposta il valore della proprietÓ annooa.
     * 
     */
    public void setANNOOA(int value) {
        this.annooa = value;
    }

    /**
     * Recupera il valore della proprietÓ capitolooa.
     * 
     */
    public int getCAPITOLOOA() {
        return capitolooa;
    }

    /**
     * Imposta il valore della proprietÓ capitolooa.
     * 
     */
    public void setCAPITOLOOA(int value) {
        this.capitolooa = value;
    }

    /**
     * Recupera il valore della proprietÓ codiamministrazioneoa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODIAMMINISTRAZIONEOA() {
        return codiamministrazioneoa;
    }

    /**
     * Imposta il valore della proprietÓ codiamministrazioneoa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODIAMMINISTRAZIONEOA(String value) {
        this.codiamministrazioneoa = value;
    }

    /**
     * Recupera il valore della proprietÓ codiceragioneriaoa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODICERAGIONERIAOA() {
        return codiceragioneriaoa;
    }

    /**
     * Imposta il valore della proprietÓ codiceragioneriaoa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODICERAGIONERIAOA(String value) {
        this.codiceragioneriaoa = value;
    }

    /**
     * Recupera il valore della proprietÓ numerooa.
     * 
     */
    public int getNUMEROOA() {
        return numerooa;
    }

    /**
     * Imposta il valore della proprietÓ numerooa.
     * 
     */
    public void setNUMEROOA(int value) {
        this.numerooa = value;
    }

    /**
     * Recupera il valore della proprietÓ numerodefrgsoa.
     * 
     */
    public int getNUMERODEFRGSOA() {
        return numerodefrgsoa;
    }

    /**
     * Imposta il valore della proprietÓ numerodefrgsoa.
     * 
     */
    public void setNUMERODEFRGSOA(int value) {
        this.numerodefrgsoa = value;
    }

    /**
     * Recupera il valore della proprietÓ causale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCAUSALE() {
        return causale;
    }

    /**
     * Imposta il valore della proprietÓ causale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCAUSALE(String value) {
        this.causale = value;
    }

    /**
     * Recupera il valore della proprietÓ tipooa.
     * 
     */
    public int getTIPOOA() {
        return tipooa;
    }

    /**
     * Imposta il valore della proprietÓ tipooa.
     * 
     */
    public void setTIPOOA(int value) {
        this.tipooa = value;
    }

    /**
     * Recupera il valore della proprietÓ dataemissioneoa.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATAEMISSIONEOA() {
        return dataemissioneoa;
    }

    /**
     * Imposta il valore della proprietÓ dataemissioneoa.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATAEMISSIONEOA(XMLGregorianCalendar value) {
        this.dataemissioneoa = value;
    }

    /**
     * Recupera il valore della proprietÓ dataemissionerichann.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATAEMISSIONERICHANN() {
        return dataemissionerichann;
    }

    /**
     * Imposta il valore della proprietÓ dataemissionerichann.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATAEMISSIONERICHANN(XMLGregorianCalendar value) {
        this.dataemissionerichann = value;
    }

}
