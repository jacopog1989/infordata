//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Tipo documento Decreto di Liquidazione
 * 
 * <p>Classe Java per MetadatiDocumentoDDType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiDocumentoDDType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FIRMATARIO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NUMERODECRETO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DATADECRETO" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiDocumentoDDType", propOrder = {
    "firmatario",
    "numerodecreto",
    "datadecreto"
})
@XmlRootElement
public class MetadatiDocumentoDDType {

    @XmlElement(name = "FIRMATARIO", required = true)
    protected String firmatario;
    @XmlElement(name = "NUMERODECRETO")
    protected int numerodecreto;
    @XmlElement(name = "DATADECRETO", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datadecreto;

    /**
     * Recupera il valore della propriet� firmatario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFIRMATARIO() {
        return firmatario;
    }

    /**
     * Imposta il valore della propriet� firmatario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFIRMATARIO(String value) {
        this.firmatario = value;
    }

    /**
     * Recupera il valore della propriet� numerodecreto.
     * 
     */
    public int getNUMERODECRETO() {
        return numerodecreto;
    }

    /**
     * Imposta il valore della propriet� numerodecreto.
     * 
     */
    public void setNUMERODECRETO(int value) {
        this.numerodecreto = value;
    }

    /**
     * Recupera il valore della propriet� datadecreto.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATADECRETO() {
        return datadecreto;
    }

    /**
     * Imposta il valore della propriet� datadecreto.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATADECRETO(XMLGregorianCalendar value) {
        this.datadecreto = value;
    }

}
