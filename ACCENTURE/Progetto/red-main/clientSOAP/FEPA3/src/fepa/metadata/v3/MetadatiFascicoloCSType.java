//
// Questo file Ŕ stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrÓ persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Fascicolo contenente CS
 * 			
 * 
 * <p>Classe Java per MetadatiFascicoloCSType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiFascicoloCSType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDENTIFICATIVOSIFE" type="{urn:fepa:metadata:v3}IdentificativoDocumentoContabileType"/>
 *         &lt;element name="CHIAVETITOLOCS" type="{urn:fepa:metadata:v3}chiaveTitoloCS_type"/>
 *         &lt;element name="OGGETTOSPESA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DATAEMISSIONE" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="CHIAVEFD" type="{urn:fepa:metadata:v3}chiaveFD_type"/>
 *         &lt;element name="DESCRIZIONEFD" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AMMINISTRAZIONEOPERANTE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DESCRIZIONEAMMINISTRAZIONEOPERANTE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BENEFICIARIO" type="{urn:fepa:metadata:v3}IdentificativoBeneficiarioCSType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiFascicoloCSType", propOrder = {
    "identificativosife",
    "chiavetitolocs",
    "oggettospesa",
    "dataemissione",
    "chiavefd",
    "descrizionefd",
    "amministrazioneoperante",
    "descrizioneamministrazioneoperante",
    "beneficiario"
})
public class MetadatiFascicoloCSType {

    @XmlElement(name = "IDENTIFICATIVOSIFE", required = true)
    protected String identificativosife;
    @XmlElement(name = "CHIAVETITOLOCS", required = true)
    protected ChiaveTitoloCSType chiavetitolocs;
    @XmlElement(name = "OGGETTOSPESA", required = true)
    protected String oggettospesa;
    @XmlElement(name = "DATAEMISSIONE", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataemissione;
    @XmlElement(name = "CHIAVEFD", required = true)
    protected ChiaveFDType chiavefd;
    @XmlElement(name = "DESCRIZIONEFD", required = true)
    protected String descrizionefd;
    @XmlElement(name = "AMMINISTRAZIONEOPERANTE", required = true)
    protected String amministrazioneoperante;
    @XmlElement(name = "DESCRIZIONEAMMINISTRAZIONEOPERANTE", required = true)
    protected String descrizioneamministrazioneoperante;
    @XmlElement(name = "BENEFICIARIO")
    protected IdentificativoBeneficiarioCSType beneficiario;

    /**
     * Recupera il valore della proprietÓ identificativosife.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDENTIFICATIVOSIFE() {
        return identificativosife;
    }

    /**
     * Imposta il valore della proprietÓ identificativosife.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDENTIFICATIVOSIFE(String value) {
        this.identificativosife = value;
    }

    /**
     * Recupera il valore della proprietÓ chiavetitolocs.
     * 
     * @return
     *     possible object is
     *     {@link ChiaveTitoloCSType }
     *     
     */
    public ChiaveTitoloCSType getCHIAVETITOLOCS() {
        return chiavetitolocs;
    }

    /**
     * Imposta il valore della proprietÓ chiavetitolocs.
     * 
     * @param value
     *     allowed object is
     *     {@link ChiaveTitoloCSType }
     *     
     */
    public void setCHIAVETITOLOCS(ChiaveTitoloCSType value) {
        this.chiavetitolocs = value;
    }

    /**
     * Recupera il valore della proprietÓ oggettospesa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOGGETTOSPESA() {
        return oggettospesa;
    }

    /**
     * Imposta il valore della proprietÓ oggettospesa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOGGETTOSPESA(String value) {
        this.oggettospesa = value;
    }

    /**
     * Recupera il valore della proprietÓ dataemissione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATAEMISSIONE() {
        return dataemissione;
    }

    /**
     * Imposta il valore della proprietÓ dataemissione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATAEMISSIONE(XMLGregorianCalendar value) {
        this.dataemissione = value;
    }

    /**
     * Recupera il valore della proprietÓ chiavefd.
     * 
     * @return
     *     possible object is
     *     {@link ChiaveFDType }
     *     
     */
    public ChiaveFDType getCHIAVEFD() {
        return chiavefd;
    }

    /**
     * Imposta il valore della proprietÓ chiavefd.
     * 
     * @param value
     *     allowed object is
     *     {@link ChiaveFDType }
     *     
     */
    public void setCHIAVEFD(ChiaveFDType value) {
        this.chiavefd = value;
    }

    /**
     * Recupera il valore della proprietÓ descrizionefd.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESCRIZIONEFD() {
        return descrizionefd;
    }

    /**
     * Imposta il valore della proprietÓ descrizionefd.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESCRIZIONEFD(String value) {
        this.descrizionefd = value;
    }

    /**
     * Recupera il valore della proprietÓ amministrazioneoperante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAMMINISTRAZIONEOPERANTE() {
        return amministrazioneoperante;
    }

    /**
     * Imposta il valore della proprietÓ amministrazioneoperante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAMMINISTRAZIONEOPERANTE(String value) {
        this.amministrazioneoperante = value;
    }

    /**
     * Recupera il valore della proprietÓ descrizioneamministrazioneoperante.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESCRIZIONEAMMINISTRAZIONEOPERANTE() {
        return descrizioneamministrazioneoperante;
    }

    /**
     * Imposta il valore della proprietÓ descrizioneamministrazioneoperante.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESCRIZIONEAMMINISTRAZIONEOPERANTE(String value) {
        this.descrizioneamministrazioneoperante = value;
    }

    /**
     * Recupera il valore della proprietÓ beneficiario.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoBeneficiarioCSType }
     *     
     */
    public IdentificativoBeneficiarioCSType getBENEFICIARIO() {
        return beneficiario;
    }

    /**
     * Imposta il valore della proprietÓ beneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoBeneficiarioCSType }
     *     
     */
    public void setBENEFICIARIO(IdentificativoBeneficiarioCSType value) {
        this.beneficiario = value;
    }

}
