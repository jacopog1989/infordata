//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Fascicolo contenente CS
 * 			
 * 
 * <p>Classe Java per MetadatiFascicoloFBType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiFascicoloFBType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDENTIFICATIVOBILANCIOENTI" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ANNO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="FASEBILANCIO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ENTE" type="{urn:fepa:metadata:v3}ENTE_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiFascicoloFBType", propOrder = {
    "identificativobilancioenti",
    "anno",
    "fasebilancio",
    "ente"
})
public class MetadatiFascicoloFBType {

    @XmlElement(name = "IDENTIFICATIVOBILANCIOENTI", required = true)
    protected String identificativobilancioenti;
    @XmlElement(name = "ANNO")
    protected int anno;
    @XmlElement(name = "FASEBILANCIO", required = true)
    protected String fasebilancio;
    @XmlElement(name = "ENTE", required = true)
    protected ENTEType ente;

    /**
     * Recupera il valore della propriet� identificativobilancioenti.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDENTIFICATIVOBILANCIOENTI() {
        return identificativobilancioenti;
    }

    /**
     * Imposta il valore della propriet� identificativobilancioenti.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDENTIFICATIVOBILANCIOENTI(String value) {
        this.identificativobilancioenti = value;
    }

    /**
     * Recupera il valore della propriet� anno.
     * 
     */
    public int getANNO() {
        return anno;
    }

    /**
     * Imposta il valore della propriet� anno.
     * 
     */
    public void setANNO(int value) {
        this.anno = value;
    }

    /**
     * Recupera il valore della propriet� fasebilancio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFASEBILANCIO() {
        return fasebilancio;
    }

    /**
     * Imposta il valore della propriet� fasebilancio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFASEBILANCIO(String value) {
        this.fasebilancio = value;
    }

    /**
     * Recupera il valore della propriet� ente.
     * 
     * @return
     *     possible object is
     *     {@link ENTEType }
     *     
     */
    public ENTEType getENTE() {
        return ente;
    }

    /**
     * Imposta il valore della propriet� ente.
     * 
     * @param value
     *     allowed object is
     *     {@link ENTEType }
     *     
     */
    public void setENTE(ENTEType value) {
        this.ente = value;
    }

}
