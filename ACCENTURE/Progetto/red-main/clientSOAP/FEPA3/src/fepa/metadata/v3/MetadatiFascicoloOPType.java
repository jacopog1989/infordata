//
// Questo file Ŕ stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrÓ persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Fascicolo contenente l'Ordine di Pagare
 * 			
 * 
 * <p>Classe Java per MetadatiFascicoloOPType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiFascicoloOPType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ANNO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CODIAMMINISTRAZIONE">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AMMINISTRAZIONE">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CODICERAGIONERIA">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="RAGIONERIA">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="IDENTIFICATIVOSIFE" type="{urn:fepa:metadata:v3}IdentificativoDocumentoContabileType"/>
 *         &lt;element name="CAPITOLO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PIANOGESTIONE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="NUMEROTITOLO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OGGETTOSPESA" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="BENEFICIARIO" type="{urn:fepa:metadata:v3}IdentificativoBeneficiarioOPType"/>
 *         &lt;element name="TIPOTITOLO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DATAEMISSIONE" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="UFFICIOMITTENTE" type="{urn:fepa:metadata:v3}IdentificativoIPAType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiFascicoloOPType", propOrder = {
    "anno",
    "codiamministrazione",
    "amministrazione",
    "codiceragioneria",
    "ragioneria",
    "identificativosife",
    "capitolo",
    "pianogestione",
    "numerotitolo",
    "oggettospesa",
    "beneficiario",
    "tipotitolo",
    "dataemissione",
    "ufficiomittente"
})
public class MetadatiFascicoloOPType {

    @XmlElement(name = "ANNO")
    protected int anno;
    @XmlElement(name = "CODIAMMINISTRAZIONE", required = true)
    protected String codiamministrazione;
    @XmlElement(name = "AMMINISTRAZIONE", required = true)
    protected String amministrazione;
    @XmlElement(name = "CODICERAGIONERIA", required = true)
    protected String codiceragioneria;
    @XmlElement(name = "RAGIONERIA", required = true)
    protected String ragioneria;
    @XmlElement(name = "IDENTIFICATIVOSIFE", required = true)
    protected String identificativosife;
    @XmlElement(name = "CAPITOLO", required = true)
    protected String capitolo;
    @XmlElement(name = "PIANOGESTIONE", required = true)
    protected String pianogestione;
    @XmlElement(name = "NUMEROTITOLO", required = true)
    protected String numerotitolo;
    @XmlElement(name = "OGGETTOSPESA", required = true)
    protected String oggettospesa;
    @XmlElement(name = "BENEFICIARIO", required = true)
    protected IdentificativoBeneficiarioOPType beneficiario;
    @XmlElement(name = "TIPOTITOLO", required = true)
    protected String tipotitolo;
    @XmlElement(name = "DATAEMISSIONE", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataemissione;
    @XmlElement(name = "UFFICIOMITTENTE")
    protected List<IdentificativoIPAType> ufficiomittente;

    /**
     * Recupera il valore della proprietÓ anno.
     * 
     */
    public int getANNO() {
        return anno;
    }

    /**
     * Imposta il valore della proprietÓ anno.
     * 
     */
    public void setANNO(int value) {
        this.anno = value;
    }

    /**
     * Recupera il valore della proprietÓ codiamministrazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODIAMMINISTRAZIONE() {
        return codiamministrazione;
    }

    /**
     * Imposta il valore della proprietÓ codiamministrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODIAMMINISTRAZIONE(String value) {
        this.codiamministrazione = value;
    }

    /**
     * Recupera il valore della proprietÓ amministrazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAMMINISTRAZIONE() {
        return amministrazione;
    }

    /**
     * Imposta il valore della proprietÓ amministrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAMMINISTRAZIONE(String value) {
        this.amministrazione = value;
    }

    /**
     * Recupera il valore della proprietÓ codiceragioneria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODICERAGIONERIA() {
        return codiceragioneria;
    }

    /**
     * Imposta il valore della proprietÓ codiceragioneria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODICERAGIONERIA(String value) {
        this.codiceragioneria = value;
    }

    /**
     * Recupera il valore della proprietÓ ragioneria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRAGIONERIA() {
        return ragioneria;
    }

    /**
     * Imposta il valore della proprietÓ ragioneria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRAGIONERIA(String value) {
        this.ragioneria = value;
    }

    /**
     * Recupera il valore della proprietÓ identificativosife.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDENTIFICATIVOSIFE() {
        return identificativosife;
    }

    /**
     * Imposta il valore della proprietÓ identificativosife.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDENTIFICATIVOSIFE(String value) {
        this.identificativosife = value;
    }

    /**
     * Recupera il valore della proprietÓ capitolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCAPITOLO() {
        return capitolo;
    }

    /**
     * Imposta il valore della proprietÓ capitolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCAPITOLO(String value) {
        this.capitolo = value;
    }

    /**
     * Recupera il valore della proprietÓ pianogestione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPIANOGESTIONE() {
        return pianogestione;
    }

    /**
     * Imposta il valore della proprietÓ pianogestione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPIANOGESTIONE(String value) {
        this.pianogestione = value;
    }

    /**
     * Recupera il valore della proprietÓ numerotitolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNUMEROTITOLO() {
        return numerotitolo;
    }

    /**
     * Imposta il valore della proprietÓ numerotitolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNUMEROTITOLO(String value) {
        this.numerotitolo = value;
    }

    /**
     * Recupera il valore della proprietÓ oggettospesa.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOGGETTOSPESA() {
        return oggettospesa;
    }

    /**
     * Imposta il valore della proprietÓ oggettospesa.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOGGETTOSPESA(String value) {
        this.oggettospesa = value;
    }

    /**
     * Recupera il valore della proprietÓ beneficiario.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoBeneficiarioOPType }
     *     
     */
    public IdentificativoBeneficiarioOPType getBENEFICIARIO() {
        return beneficiario;
    }

    /**
     * Imposta il valore della proprietÓ beneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoBeneficiarioOPType }
     *     
     */
    public void setBENEFICIARIO(IdentificativoBeneficiarioOPType value) {
        this.beneficiario = value;
    }

    /**
     * Recupera il valore della proprietÓ tipotitolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTIPOTITOLO() {
        return tipotitolo;
    }

    /**
     * Imposta il valore della proprietÓ tipotitolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTIPOTITOLO(String value) {
        this.tipotitolo = value;
    }

    /**
     * Recupera il valore della proprietÓ dataemissione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATAEMISSIONE() {
        return dataemissione;
    }

    /**
     * Imposta il valore della proprietÓ dataemissione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATAEMISSIONE(XMLGregorianCalendar value) {
        this.dataemissione = value;
    }

    /**
     * Gets the value of the ufficiomittente property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ufficiomittente property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUFFICIOMITTENTE().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IdentificativoIPAType }
     * 
     * 
     */
    public List<IdentificativoIPAType> getUFFICIOMITTENTE() {
        if (ufficiomittente == null) {
            ufficiomittente = new ArrayList<IdentificativoIPAType>();
        }
        return this.ufficiomittente;
    }

}
