//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Metadati Fascicolo del Decreto per Sicoge enti
 * 
 * <p>Classe Java per MetadatiFascicoloDecretoSEType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiFascicoloDecretoSEType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CODIAMMINISTRAZIONE">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ANNO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CDR">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CDS">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NUMERODECRETO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiFascicoloDecretoSEType", propOrder = {
    "codiamministrazione",
    "anno",
    "cdr",
    "cds",
    "numerodecreto"
})
public class MetadatiFascicoloDecretoSEType {

    @XmlElement(name = "CODIAMMINISTRAZIONE", required = true)
    protected String codiamministrazione;
    @XmlElement(name = "ANNO")
    protected int anno;
    @XmlElement(name = "CDR", required = true)
    protected String cdr;
    @XmlElement(name = "CDS", required = true)
    protected String cds;
    @XmlElement(name = "NUMERODECRETO")
    protected int numerodecreto;

    /**
     * Recupera il valore della propriet� codiamministrazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODIAMMINISTRAZIONE() {
        return codiamministrazione;
    }

    /**
     * Imposta il valore della propriet� codiamministrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODIAMMINISTRAZIONE(String value) {
        this.codiamministrazione = value;
    }

    /**
     * Recupera il valore della propriet� anno.
     * 
     */
    public int getANNO() {
        return anno;
    }

    /**
     * Imposta il valore della propriet� anno.
     * 
     */
    public void setANNO(int value) {
        this.anno = value;
    }

    /**
     * Recupera il valore della propriet� cdr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCDR() {
        return cdr;
    }

    /**
     * Imposta il valore della propriet� cdr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCDR(String value) {
        this.cdr = value;
    }

    /**
     * Recupera il valore della propriet� cds.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCDS() {
        return cds;
    }

    /**
     * Imposta il valore della propriet� cds.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCDS(String value) {
        this.cds = value;
    }

    /**
     * Recupera il valore della propriet� numerodecreto.
     * 
     */
    public int getNUMERODECRETO() {
        return numerodecreto;
    }

    /**
     * Imposta il valore della propriet� numerodecreto.
     * 
     */
    public void setNUMERODECRETO(int value) {
        this.numerodecreto = value;
    }

}
