//
// Questo file Ŕ stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrÓ persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Fascicolo contenente CS
 * 			
 * 
 * <p>Classe Java per MetadatiFascicoloRendicontoType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiFascicoloRendicontoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDENTIFICATIVOFASCICOLORENDICONTO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ANNOESERCIZIO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CAPITOLO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AMMINISTRAZIONE" type="{urn:fepa:metadata:v3}ENTE_type"/>
 *         &lt;element name="CHIAVEFUNZIONARIODELEGATO" type="{urn:fepa:metadata:v3}ChiaveFunzionarioDelegato_Type"/>
 *         &lt;element name="DESCRIZIONEFUNZIONARIODELEGATO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TIPORENDICONTO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AMMINISTRAZIONEOPERANTE" type="{urn:fepa:metadata:v3}ENTE_type"/>
 *         &lt;element name="RAGIONERIARISCONTRANTE" type="{urn:fepa:metadata:v3}ENTE_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiFascicoloRendicontoType", propOrder = {
    "identificativofascicolorendiconto",
    "annoesercizio",
    "capitolo",
    "amministrazione",
    "chiavefunzionariodelegato",
    "descrizionefunzionariodelegato",
    "tiporendiconto",
    "amministrazioneoperante",
    "ragioneriariscontrante"
})
public class MetadatiFascicoloRendicontoType {

    @XmlElement(name = "IDENTIFICATIVOFASCICOLORENDICONTO", required = true)
    protected String identificativofascicolorendiconto;
    @XmlElement(name = "ANNOESERCIZIO")
    protected int annoesercizio;
    @XmlElement(name = "CAPITOLO")
    protected int capitolo;
    @XmlElement(name = "AMMINISTRAZIONE", required = true)
    protected ENTEType amministrazione;
    @XmlElement(name = "CHIAVEFUNZIONARIODELEGATO", required = true)
    protected ChiaveFunzionarioDelegatoType chiavefunzionariodelegato;
    @XmlElement(name = "DESCRIZIONEFUNZIONARIODELEGATO", required = true)
    protected String descrizionefunzionariodelegato;
    @XmlElement(name = "TIPORENDICONTO", required = true)
    protected String tiporendiconto;
    @XmlElement(name = "AMMINISTRAZIONEOPERANTE", required = true)
    protected ENTEType amministrazioneoperante;
    @XmlElement(name = "RAGIONERIARISCONTRANTE", required = true)
    protected ENTEType ragioneriariscontrante;

    /**
     * Recupera il valore della proprietÓ identificativofascicolorendiconto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDENTIFICATIVOFASCICOLORENDICONTO() {
        return identificativofascicolorendiconto;
    }

    /**
     * Imposta il valore della proprietÓ identificativofascicolorendiconto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDENTIFICATIVOFASCICOLORENDICONTO(String value) {
        this.identificativofascicolorendiconto = value;
    }

    /**
     * Recupera il valore della proprietÓ annoesercizio.
     * 
     */
    public int getANNOESERCIZIO() {
        return annoesercizio;
    }

    /**
     * Imposta il valore della proprietÓ annoesercizio.
     * 
     */
    public void setANNOESERCIZIO(int value) {
        this.annoesercizio = value;
    }

    /**
     * Recupera il valore della proprietÓ capitolo.
     * 
     */
    public int getCAPITOLO() {
        return capitolo;
    }

    /**
     * Imposta il valore della proprietÓ capitolo.
     * 
     */
    public void setCAPITOLO(int value) {
        this.capitolo = value;
    }

    /**
     * Recupera il valore della proprietÓ amministrazione.
     * 
     * @return
     *     possible object is
     *     {@link ENTEType }
     *     
     */
    public ENTEType getAMMINISTRAZIONE() {
        return amministrazione;
    }

    /**
     * Imposta il valore della proprietÓ amministrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link ENTEType }
     *     
     */
    public void setAMMINISTRAZIONE(ENTEType value) {
        this.amministrazione = value;
    }

    /**
     * Recupera il valore della proprietÓ chiavefunzionariodelegato.
     * 
     * @return
     *     possible object is
     *     {@link ChiaveFunzionarioDelegatoType }
     *     
     */
    public ChiaveFunzionarioDelegatoType getCHIAVEFUNZIONARIODELEGATO() {
        return chiavefunzionariodelegato;
    }

    /**
     * Imposta il valore della proprietÓ chiavefunzionariodelegato.
     * 
     * @param value
     *     allowed object is
     *     {@link ChiaveFunzionarioDelegatoType }
     *     
     */
    public void setCHIAVEFUNZIONARIODELEGATO(ChiaveFunzionarioDelegatoType value) {
        this.chiavefunzionariodelegato = value;
    }

    /**
     * Recupera il valore della proprietÓ descrizionefunzionariodelegato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESCRIZIONEFUNZIONARIODELEGATO() {
        return descrizionefunzionariodelegato;
    }

    /**
     * Imposta il valore della proprietÓ descrizionefunzionariodelegato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESCRIZIONEFUNZIONARIODELEGATO(String value) {
        this.descrizionefunzionariodelegato = value;
    }

    /**
     * Recupera il valore della proprietÓ tiporendiconto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTIPORENDICONTO() {
        return tiporendiconto;
    }

    /**
     * Imposta il valore della proprietÓ tiporendiconto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTIPORENDICONTO(String value) {
        this.tiporendiconto = value;
    }

    /**
     * Recupera il valore della proprietÓ amministrazioneoperante.
     * 
     * @return
     *     possible object is
     *     {@link ENTEType }
     *     
     */
    public ENTEType getAMMINISTRAZIONEOPERANTE() {
        return amministrazioneoperante;
    }

    /**
     * Imposta il valore della proprietÓ amministrazioneoperante.
     * 
     * @param value
     *     allowed object is
     *     {@link ENTEType }
     *     
     */
    public void setAMMINISTRAZIONEOPERANTE(ENTEType value) {
        this.amministrazioneoperante = value;
    }

    /**
     * Recupera il valore della proprietÓ ragioneriariscontrante.
     * 
     * @return
     *     possible object is
     *     {@link ENTEType }
     *     
     */
    public ENTEType getRAGIONERIARISCONTRANTE() {
        return ragioneriariscontrante;
    }

    /**
     * Imposta il valore della proprietÓ ragioneriariscontrante.
     * 
     * @param value
     *     allowed object is
     *     {@link ENTEType }
     *     
     */
    public void setRAGIONERIARISCONTRANTE(ENTEType value) {
        this.ragioneriariscontrante = value;
    }

}
