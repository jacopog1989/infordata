//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Metadati Fascicolo Cassa Economale
 * 
 * <p>Classe Java per MetadatiFascicoloCEType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiFascicoloCEType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CODIAMMINISTRAZIONE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ANNOFINANZIARIO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CDR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CodiceEconomo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiFascicoloCEType", propOrder = {
    "codiamministrazione",
    "annofinanziario",
    "cdr",
    "codiceEconomo"
})
public class MetadatiFascicoloCEType {

    @XmlElement(name = "CODIAMMINISTRAZIONE", required = true)
    protected String codiamministrazione;
    @XmlElement(name = "ANNOFINANZIARIO")
    protected int annofinanziario;
    @XmlElement(name = "CDR", required = true)
    protected String cdr;
    @XmlElement(name = "CodiceEconomo", required = true)
    protected String codiceEconomo;

    /**
     * Recupera il valore della propriet� codiamministrazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODIAMMINISTRAZIONE() {
        return codiamministrazione;
    }

    /**
     * Imposta il valore della propriet� codiamministrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODIAMMINISTRAZIONE(String value) {
        this.codiamministrazione = value;
    }

    /**
     * Recupera il valore della propriet� annofinanziario.
     * 
     */
    public int getANNOFINANZIARIO() {
        return annofinanziario;
    }

    /**
     * Imposta il valore della propriet� annofinanziario.
     * 
     */
    public void setANNOFINANZIARIO(int value) {
        this.annofinanziario = value;
    }

    /**
     * Recupera il valore della propriet� cdr.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCDR() {
        return cdr;
    }

    /**
     * Imposta il valore della propriet� cdr.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCDR(String value) {
        this.cdr = value;
    }

    /**
     * Recupera il valore della propriet� codiceEconomo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceEconomo() {
        return codiceEconomo;
    }

    /**
     * Imposta il valore della propriet� codiceEconomo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceEconomo(String value) {
        this.codiceEconomo = value;
    }

}
