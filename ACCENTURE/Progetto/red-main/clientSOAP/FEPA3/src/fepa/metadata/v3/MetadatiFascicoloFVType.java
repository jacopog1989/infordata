//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Fascicolo contenente CS
 * 			
 * 
 * <p>Classe Java per MetadatiFascicoloFVType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiFascicoloFVType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDENTIFICATIVOVERBALEREVISIONE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DATAVERBALE" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="NUMEROVERBALE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ENTE" type="{urn:fepa:metadata:v3}ENTE_type"/>
 *         &lt;element name="REVISORE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiFascicoloFVType", propOrder = {
    "identificativoverbalerevisione",
    "dataverbale",
    "numeroverbale",
    "ente",
    "revisore"
})
public class MetadatiFascicoloFVType {

    @XmlElement(name = "IDENTIFICATIVOVERBALEREVISIONE", required = true)
    protected String identificativoverbalerevisione;
    @XmlElement(name = "DATAVERBALE", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataverbale;
    @XmlElement(name = "NUMEROVERBALE", required = true)
    protected String numeroverbale;
    @XmlElement(name = "ENTE", required = true)
    protected ENTEType ente;
    @XmlElement(name = "REVISORE", required = true)
    protected String revisore;

    /**
     * Recupera il valore della propriet� identificativoverbalerevisione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDENTIFICATIVOVERBALEREVISIONE() {
        return identificativoverbalerevisione;
    }

    /**
     * Imposta il valore della propriet� identificativoverbalerevisione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDENTIFICATIVOVERBALEREVISIONE(String value) {
        this.identificativoverbalerevisione = value;
    }

    /**
     * Recupera il valore della propriet� dataverbale.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATAVERBALE() {
        return dataverbale;
    }

    /**
     * Imposta il valore della propriet� dataverbale.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATAVERBALE(XMLGregorianCalendar value) {
        this.dataverbale = value;
    }

    /**
     * Recupera il valore della propriet� numeroverbale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNUMEROVERBALE() {
        return numeroverbale;
    }

    /**
     * Imposta il valore della propriet� numeroverbale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNUMEROVERBALE(String value) {
        this.numeroverbale = value;
    }

    /**
     * Recupera il valore della propriet� ente.
     * 
     * @return
     *     possible object is
     *     {@link ENTEType }
     *     
     */
    public ENTEType getENTE() {
        return ente;
    }

    /**
     * Imposta il valore della propriet� ente.
     * 
     * @param value
     *     allowed object is
     *     {@link ENTEType }
     *     
     */
    public void setENTE(ENTEType value) {
        this.ente = value;
    }

    /**
     * Recupera il valore della propriet� revisore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREVISORE() {
        return revisore;
    }

    /**
     * Imposta il valore della propriet� revisore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREVISORE(String value) {
        this.revisore = value;
    }

}
