//
// Questo file Ŕ stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrÓ persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Fascicolo contenente l'Ordine di Accreditamento
 * 			
 * 
 * <p>Classe Java per MetadatiFascicoloOAType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiFascicoloOAType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ANNO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CAPITOLO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CODIAMMINISTRAZIONE">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="AMMINISTRAZIONE">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="CODICERAGIONERIA">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="4"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="RAGIONERIA">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="255"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="NUMERO_OA" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="AMMINISTRAZIONE_FD">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="3"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="QUALIFICA_FD" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="ZONA_FD" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="PROGRESSIVO_FD" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="CAUSALE">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="60"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TIPO_OA" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DATAEMISSIONE" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiFascicoloOAType", propOrder = {
    "anno",
    "capitolo",
    "codiamministrazione",
    "amministrazione",
    "codiceragioneria",
    "ragioneria",
    "numerooa",
    "amministrazionefd",
    "qualificafd",
    "zonafd",
    "progressivofd",
    "causale",
    "tipooa",
    "dataemissione"
})
public class MetadatiFascicoloOAType {

    @XmlElement(name = "ANNO")
    protected int anno;
    @XmlElement(name = "CAPITOLO")
    protected int capitolo;
    @XmlElement(name = "CODIAMMINISTRAZIONE", required = true)
    protected String codiamministrazione;
    @XmlElement(name = "AMMINISTRAZIONE", required = true)
    protected String amministrazione;
    @XmlElement(name = "CODICERAGIONERIA", required = true)
    protected String codiceragioneria;
    @XmlElement(name = "RAGIONERIA", required = true)
    protected String ragioneria;
    @XmlElement(name = "NUMERO_OA")
    protected int numerooa;
    @XmlElement(name = "AMMINISTRAZIONE_FD", required = true)
    protected String amministrazionefd;
    @XmlElement(name = "QUALIFICA_FD")
    protected int qualificafd;
    @XmlElement(name = "ZONA_FD")
    protected int zonafd;
    @XmlElement(name = "PROGRESSIVO_FD")
    protected int progressivofd;
    @XmlElement(name = "CAUSALE", required = true)
    protected String causale;
    @XmlElement(name = "TIPO_OA")
    protected int tipooa;
    @XmlElement(name = "DATAEMISSIONE", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataemissione;

    /**
     * Recupera il valore della proprietÓ anno.
     * 
     */
    public int getANNO() {
        return anno;
    }

    /**
     * Imposta il valore della proprietÓ anno.
     * 
     */
    public void setANNO(int value) {
        this.anno = value;
    }

    /**
     * Recupera il valore della proprietÓ capitolo.
     * 
     */
    public int getCAPITOLO() {
        return capitolo;
    }

    /**
     * Imposta il valore della proprietÓ capitolo.
     * 
     */
    public void setCAPITOLO(int value) {
        this.capitolo = value;
    }

    /**
     * Recupera il valore della proprietÓ codiamministrazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODIAMMINISTRAZIONE() {
        return codiamministrazione;
    }

    /**
     * Imposta il valore della proprietÓ codiamministrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODIAMMINISTRAZIONE(String value) {
        this.codiamministrazione = value;
    }

    /**
     * Recupera il valore della proprietÓ amministrazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAMMINISTRAZIONE() {
        return amministrazione;
    }

    /**
     * Imposta il valore della proprietÓ amministrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAMMINISTRAZIONE(String value) {
        this.amministrazione = value;
    }

    /**
     * Recupera il valore della proprietÓ codiceragioneria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODICERAGIONERIA() {
        return codiceragioneria;
    }

    /**
     * Imposta il valore della proprietÓ codiceragioneria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODICERAGIONERIA(String value) {
        this.codiceragioneria = value;
    }

    /**
     * Recupera il valore della proprietÓ ragioneria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRAGIONERIA() {
        return ragioneria;
    }

    /**
     * Imposta il valore della proprietÓ ragioneria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRAGIONERIA(String value) {
        this.ragioneria = value;
    }

    /**
     * Recupera il valore della proprietÓ numerooa.
     * 
     */
    public int getNUMEROOA() {
        return numerooa;
    }

    /**
     * Imposta il valore della proprietÓ numerooa.
     * 
     */
    public void setNUMEROOA(int value) {
        this.numerooa = value;
    }

    /**
     * Recupera il valore della proprietÓ amministrazionefd.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAMMINISTRAZIONEFD() {
        return amministrazionefd;
    }

    /**
     * Imposta il valore della proprietÓ amministrazionefd.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAMMINISTRAZIONEFD(String value) {
        this.amministrazionefd = value;
    }

    /**
     * Recupera il valore della proprietÓ qualificafd.
     * 
     */
    public int getQUALIFICAFD() {
        return qualificafd;
    }

    /**
     * Imposta il valore della proprietÓ qualificafd.
     * 
     */
    public void setQUALIFICAFD(int value) {
        this.qualificafd = value;
    }

    /**
     * Recupera il valore della proprietÓ zonafd.
     * 
     */
    public int getZONAFD() {
        return zonafd;
    }

    /**
     * Imposta il valore della proprietÓ zonafd.
     * 
     */
    public void setZONAFD(int value) {
        this.zonafd = value;
    }

    /**
     * Recupera il valore della proprietÓ progressivofd.
     * 
     */
    public int getPROGRESSIVOFD() {
        return progressivofd;
    }

    /**
     * Imposta il valore della proprietÓ progressivofd.
     * 
     */
    public void setPROGRESSIVOFD(int value) {
        this.progressivofd = value;
    }

    /**
     * Recupera il valore della proprietÓ causale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCAUSALE() {
        return causale;
    }

    /**
     * Imposta il valore della proprietÓ causale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCAUSALE(String value) {
        this.causale = value;
    }

    /**
     * Recupera il valore della proprietÓ tipooa.
     * 
     */
    public int getTIPOOA() {
        return tipooa;
    }

    /**
     * Imposta il valore della proprietÓ tipooa.
     * 
     */
    public void setTIPOOA(int value) {
        this.tipooa = value;
    }

    /**
     * Recupera il valore della proprietÓ dataemissione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATAEMISSIONE() {
        return dataemissione;
    }

    /**
     * Imposta il valore della proprietÓ dataemissione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATAEMISSIONE(XMLGregorianCalendar value) {
        this.dataemissione = value;
    }

}
