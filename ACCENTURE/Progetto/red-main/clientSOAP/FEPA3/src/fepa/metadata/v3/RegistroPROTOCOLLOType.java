//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Protocollo del SICOGE
 * 
 * <p>Classe Java per registroPROTOCOLLO_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="registroPROTOCOLLO_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DATAREGISTRAZIONE" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="NUMEROPROTOCOLLO" type="{urn:fepa:metadata:v3}numeroProtocollo_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "registroPROTOCOLLO_type", propOrder = {
    "dataregistrazione",
    "numeroprotocollo"
})
public class RegistroPROTOCOLLOType {

    @XmlElement(name = "DATAREGISTRAZIONE", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataregistrazione;
    @XmlElement(name = "NUMEROPROTOCOLLO", required = true)
    protected BigInteger numeroprotocollo;

    /**
     * Recupera il valore della propriet� dataregistrazione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATAREGISTRAZIONE() {
        return dataregistrazione;
    }

    /**
     * Imposta il valore della propriet� dataregistrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATAREGISTRAZIONE(XMLGregorianCalendar value) {
        this.dataregistrazione = value;
    }

    /**
     * Recupera il valore della propriet� numeroprotocollo.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNUMEROPROTOCOLLO() {
        return numeroprotocollo;
    }

    /**
     * Imposta il valore della propriet� numeroprotocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNUMEROPROTOCOLLO(BigInteger value) {
        this.numeroprotocollo = value;
    }

}
