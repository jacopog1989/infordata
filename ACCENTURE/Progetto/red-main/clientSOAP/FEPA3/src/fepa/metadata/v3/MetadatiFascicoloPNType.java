//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Metadati Fascicolo del Cronoprogramma Amm.ne Centrale
 * 
 * <p>Classe Java per MetadatiFascicoloPNType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiFascicoloPNType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PN" type="{urn:fepa:metadata:v3}IdentificativoPNIType"/>
 *         &lt;element name="PCO" type="{urn:fepa:metadata:v3}IdentificativoPCOType"/>
 *         &lt;element name="CAUSALE">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="100"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiFascicoloPNType", propOrder = {
    "pn",
    "pco",
    "causale"
})
public class MetadatiFascicoloPNType {

    @XmlElement(name = "PN", required = true)
    protected IdentificativoPNIType pn;
    @XmlElement(name = "PCO", required = true)
    protected IdentificativoPCOType pco;
    @XmlElement(name = "CAUSALE", required = true)
    protected String causale;

    /**
     * Recupera il valore della propriet� pn.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoPNIType }
     *     
     */
    public IdentificativoPNIType getPN() {
        return pn;
    }

    /**
     * Imposta il valore della propriet� pn.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoPNIType }
     *     
     */
    public void setPN(IdentificativoPNIType value) {
        this.pn = value;
    }

    /**
     * Recupera il valore della propriet� pco.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoPCOType }
     *     
     */
    public IdentificativoPCOType getPCO() {
        return pco;
    }

    /**
     * Imposta il valore della propriet� pco.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoPCOType }
     *     
     */
    public void setPCO(IdentificativoPCOType value) {
        this.pco = value;
    }

    /**
     * Recupera il valore della propriet� causale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCAUSALE() {
        return causale;
    }

    /**
     * Imposta il valore della propriet� causale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCAUSALE(String value) {
        this.causale = value;
    }

}
