//
// Questo file Ŕ stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andrÓ persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Tipo documento Lotto
 * 
 * <p>Classe Java per MetadatiDocumentoLottoType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiDocumentoLottoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IDENTIFICATIVOLOTTO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="IDENTIFICATIVOSIFE" type="{urn:fepa:metadata:v3}IdentificativoDocumentoContabileType"/>
 *         &lt;element name="IDENTIFICATIVOFORNITORE" type="{urn:fepa:metadata:v3}IdentificativoFornitoreFatturaType"/>
 *         &lt;element name="IDENTIFICATIVOUFFICIOFE" type="{urn:fepa:metadata:v3}IdentificativoIPAType"/>
 *         &lt;element name="DATAPRESAINCARICO" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="TICKET_ARCHIVIAZIONE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiDocumentoLottoType", propOrder = {
    "identificativolotto",
    "identificativosife",
    "identificativofornitore",
    "identificativoufficiofe",
    "datapresaincarico",
    "ticketarchiviazione"
})
public class MetadatiDocumentoLottoType {

    @XmlElement(name = "IDENTIFICATIVOLOTTO")
    protected int identificativolotto;
    @XmlElement(name = "IDENTIFICATIVOSIFE", required = true)
    protected String identificativosife;
    @XmlElement(name = "IDENTIFICATIVOFORNITORE", required = true)
    protected IdentificativoFornitoreFatturaType identificativofornitore;
    @XmlElement(name = "IDENTIFICATIVOUFFICIOFE", required = true)
    protected IdentificativoIPAType identificativoufficiofe;
    @XmlElement(name = "DATAPRESAINCARICO")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datapresaincarico;
    @XmlElement(name = "TICKET_ARCHIVIAZIONE")
    protected String ticketarchiviazione;

    /**
     * Recupera il valore della proprietÓ identificativolotto.
     * 
     */
    public int getIDENTIFICATIVOLOTTO() {
        return identificativolotto;
    }

    /**
     * Imposta il valore della proprietÓ identificativolotto.
     * 
     */
    public void setIDENTIFICATIVOLOTTO(int value) {
        this.identificativolotto = value;
    }

    /**
     * Recupera il valore della proprietÓ identificativosife.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDENTIFICATIVOSIFE() {
        return identificativosife;
    }

    /**
     * Imposta il valore della proprietÓ identificativosife.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDENTIFICATIVOSIFE(String value) {
        this.identificativosife = value;
    }

    /**
     * Recupera il valore della proprietÓ identificativofornitore.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoFornitoreFatturaType }
     *     
     */
    public IdentificativoFornitoreFatturaType getIDENTIFICATIVOFORNITORE() {
        return identificativofornitore;
    }

    /**
     * Imposta il valore della proprietÓ identificativofornitore.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoFornitoreFatturaType }
     *     
     */
    public void setIDENTIFICATIVOFORNITORE(IdentificativoFornitoreFatturaType value) {
        this.identificativofornitore = value;
    }

    /**
     * Recupera il valore della proprietÓ identificativoufficiofe.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoIPAType }
     *     
     */
    public IdentificativoIPAType getIDENTIFICATIVOUFFICIOFE() {
        return identificativoufficiofe;
    }

    /**
     * Imposta il valore della proprietÓ identificativoufficiofe.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoIPAType }
     *     
     */
    public void setIDENTIFICATIVOUFFICIOFE(IdentificativoIPAType value) {
        this.identificativoufficiofe = value;
    }

    /**
     * Recupera il valore della proprietÓ datapresaincarico.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATAPRESAINCARICO() {
        return datapresaincarico;
    }

    /**
     * Imposta il valore della proprietÓ datapresaincarico.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATAPRESAINCARICO(XMLGregorianCalendar value) {
        this.datapresaincarico = value;
    }

    /**
     * Recupera il valore della proprietÓ ticketarchiviazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTICKETARCHIVIAZIONE() {
        return ticketarchiviazione;
    }

    /**
     * Imposta il valore della proprietÓ ticketarchiviazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTICKETARCHIVIAZIONE(String value) {
        this.ticketarchiviazione = value;
    }

}
