//
// Questo file � stato generato dall'architettura JavaTM per XML Binding (JAXB) Reference Implementation, v2.2.8-b130911.1802 
// Vedere <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Qualsiasi modifica a questo file andr� persa durante la ricompilazione dello schema di origine. 
// Generato il: 2020.08.04 alle 04:27:46 PM CEST 
//


package fepa.metadata.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Metadati Fascicolo Reversale
 * 
 * <p>Classe Java per MetadatiFascicoloReversaleType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="MetadatiFascicoloReversaleType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DATA" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="NUMERO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="BENEFICIARIO" type="{urn:fepa:metadata:v3}IdentificativoBeneficiarioImpegnoType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetadatiFascicoloReversaleType", propOrder = {
    "data",
    "numero",
    "beneficiario"
})
public class MetadatiFascicoloReversaleType {

    @XmlElement(name = "DATA", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar data;
    @XmlElement(name = "NUMERO")
    protected int numero;
    @XmlElement(name = "BENEFICIARIO", required = true)
    protected IdentificativoBeneficiarioImpegnoType beneficiario;

    /**
     * Recupera il valore della propriet� data.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATA() {
        return data;
    }

    /**
     * Imposta il valore della propriet� data.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATA(XMLGregorianCalendar value) {
        this.data = value;
    }

    /**
     * Recupera il valore della propriet� numero.
     * 
     */
    public int getNUMERO() {
        return numero;
    }

    /**
     * Imposta il valore della propriet� numero.
     * 
     */
    public void setNUMERO(int value) {
        this.numero = value;
    }

    /**
     * Recupera il valore della propriet� beneficiario.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoBeneficiarioImpegnoType }
     *     
     */
    public IdentificativoBeneficiarioImpegnoType getBENEFICIARIO() {
        return beneficiario;
    }

    /**
     * Imposta il valore della propriet� beneficiario.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoBeneficiarioImpegnoType }
     *     
     */
    public void setBENEFICIARIO(IdentificativoBeneficiarioImpegnoType value) {
        this.beneficiario = value;
    }

}
