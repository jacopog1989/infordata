
package fepa.messages.v3;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import fepa.types.v3.RispostaDownloadDocumentoUrlFascicoloType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;extension base="{urn:fepa:types:v3}risposta_downloadDocumentoUrlFascicolo_type">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "risposta_downloadDocumentoUrlFascicolo")
public class RispostaDownloadDocumentoUrlFascicolo
    extends RispostaDownloadDocumentoUrlFascicoloType
    implements Serializable
{


}
