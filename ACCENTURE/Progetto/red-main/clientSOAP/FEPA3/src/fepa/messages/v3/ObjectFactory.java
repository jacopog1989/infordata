
package fepa.messages.v3;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import fepa.types.v3.RichiestaAddDocumentoFascicoloType;
import fepa.types.v3.RichiestaArchiveFascicoloType;
import fepa.types.v3.RichiestaAssociaProtocolloType;
import fepa.types.v3.RichiestaAssociateFascicoloType;
import fepa.types.v3.RichiestaChangeStatoFascicoloType;
import fepa.types.v3.RichiestaConvertDocumentoType;
import fepa.types.v3.RichiestaCreateFascicoloType;
import fepa.types.v3.RichiestaDisAssociateFascicoloType;
import fepa.types.v3.RichiestaDownloadDocumentoFascicoloByTypeDocType;
import fepa.types.v3.RichiestaDownloadDocumentoFascicoloType;
import fepa.types.v3.RichiestaDownloadDocumentoUrlFascicoloType;
import fepa.types.v3.RichiestaDownloadFileContentType;
import fepa.types.v3.RichiestaGetDocumentoFascicoloType;
import fepa.types.v3.RichiestaGetFascicoloType;
import fepa.types.v3.RichiestaGetLinkFascicoloType;
import fepa.types.v3.RichiestaLockSubFasciclesType;
import fepa.types.v3.RichiestaRemoveDocumentoFascicoloType;
import fepa.types.v3.RichiestaRemoveFascicoloType;
import fepa.types.v3.RichiestaSearchDocumentiFascicoloType;
import fepa.types.v3.RichiestaSearchMetadatiFascicoloType;
import fepa.types.v3.RichiestaSendFascicoloType;
import fepa.types.v3.RichiestaUpdateDocumentoFascicoloType;
import fepa.types.v3.RichiestaUpdateFascicoloType;
import fepa.types.v3.RichiestaUploadFileContentType;
import fepa.types.v3.RispostaArchiveFascicoloType;
import fepa.types.v3.RispostaAssociaProtocolloType;
import fepa.types.v3.RispostaAssociateFascicoloType;
import fepa.types.v3.RispostaChangeStatoFascicoloType;
import fepa.types.v3.RispostaConvertDocumentoType;
import fepa.types.v3.RispostaDisAssociateFascicoloType;
import fepa.types.v3.RispostaDownloadDocumentoFascicoloByTypeDocType;
import fepa.types.v3.RispostaDownloadFileContentType;
import fepa.types.v3.RispostaGetDocumentoFascicoloType;
import fepa.types.v3.RispostaGetLinkFascicoloType;
import fepa.types.v3.RispostaLockSubFasciclesType;
import fepa.types.v3.RispostaRemoveDocumentoFascicoloType;
import fepa.types.v3.RispostaRemoveFascicoloType;
import fepa.types.v3.RispostaSearchDocumentiFascicoloType;
import fepa.types.v3.RispostaSearchMetadatiFascicoloType;
import fepa.types.v3.RispostaSendFascicoloType;
import fepa.types.v3.RispostaUpdateDocumentoFascicoloType;
import fepa.types.v3.RispostaUpdateFascicoloType;
import fepa.types.v3.RispostaUploadFileContentType;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the fepa.messages.v3 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RispostaRemoveDocumentoFascicolo_QNAME = new QName("urn:fepa:messages:v3", "risposta_removeDocumentoFascicolo");
    private final static QName _RispostaArchiveFascicolo_QNAME = new QName("urn:fepa:messages:v3", "risposta_archiveFascicolo");
    private final static QName _RichiestaUpdateFascicolo_QNAME = new QName("urn:fepa:messages:v3", "richiesta_updateFascicolo");
    private final static QName _RichiestaUploadFileContent_QNAME = new QName("urn:fepa:messages:v3", "richiesta_uploadFileContent");
    private final static QName _RichiestaGetDocumentoFascicolo_QNAME = new QName("urn:fepa:messages:v3", "richiesta_getDocumentoFascicolo");
    private final static QName _RichiestaChangeStatoFascicolo_QNAME = new QName("urn:fepa:messages:v3", "richiesta_changeStatoFascicolo");
    private final static QName _RichiestaRemoveDocumentoFascicolo_QNAME = new QName("urn:fepa:messages:v3", "richiesta_removeDocumentoFascicolo");
    private final static QName _RispostaDownloadDocumentoFascicoloByTypeDoc_QNAME = new QName("urn:fepa:messages:v3", "risposta_downloadDocumentoFascicoloByTypeDoc");
    private final static QName _RispostaChangeStatoFascicolo_QNAME = new QName("urn:fepa:messages:v3", "risposta_changeStatoFascicolo");
    private final static QName _RichiestaAddDocumentoFascicolo_QNAME = new QName("urn:fepa:messages:v3", "richiesta_addDocumentoFascicolo");
    private final static QName _RispostaGetDocumentoFascicolo_QNAME = new QName("urn:fepa:messages:v3", "risposta_getDocumentoFascicolo");
    private final static QName _RichiestaLockSubFascicles_QNAME = new QName("urn:fepa:messages:v3", "richiesta_lockSubFascicles");
    private final static QName _RispostaGetLinkFascicolo_QNAME = new QName("urn:fepa:messages:v3", "risposta_getLinkFascicolo");
    private final static QName _RichiestaDownloadDocumentoFascicolo_QNAME = new QName("urn:fepa:messages:v3", "richiesta_downloadDocumentoFascicolo");
    private final static QName _RispostaUploadFileContent_QNAME = new QName("urn:fepa:messages:v3", "risposta_uploadFileContent");
    private final static QName _RichiestaSearchMetadatoFascicolo_QNAME = new QName("urn:fepa:messages:v3", "richiesta_searchMetadatoFascicolo");
    private final static QName _RichiestaAssociaProtocollo_QNAME = new QName("urn:fepa:messages:v3", "richiesta_associaProtocollo");
    private final static QName _RispostaDownloadFileContent_QNAME = new QName("urn:fepa:messages:v3", "risposta_downloadFileContent");
    private final static QName _RispostaAssociaProtocollo_QNAME = new QName("urn:fepa:messages:v3", "risposta_associaProtocollo");
    private final static QName _RispostaRemoveFascicolo_QNAME = new QName("urn:fepa:messages:v3", "risposta_removeFascicolo");
    private final static QName _RispostaDisAssociateFascicolo_QNAME = new QName("urn:fepa:messages:v3", "risposta_disAssociateFascicolo");
    private final static QName _RispostaSendFascicolo_QNAME = new QName("urn:fepa:messages:v3", "risposta_sendFascicolo");
    private final static QName _RichiestaCreateFascicolo_QNAME = new QName("urn:fepa:messages:v3", "richiesta_createFascicolo");
    private final static QName _RichiestaSendFascicolo_QNAME = new QName("urn:fepa:messages:v3", "richiesta_sendFascicolo");
    private final static QName _RichiestaGetFascicolo_QNAME = new QName("urn:fepa:messages:v3", "richiesta_getFascicolo");
    private final static QName _RichiestaDownloadDocumentoFascicoloByTypeDoc_QNAME = new QName("urn:fepa:messages:v3", "richiesta_downloadDocumentoFascicoloByTypeDoc");
    private final static QName _RichiestaDownloadDocumentoUrlFascicolo_QNAME = new QName("urn:fepa:messages:v3", "richiesta_downloadDocumentoUrlFascicolo");
    private final static QName _RichiestaAssociateFascicolo_QNAME = new QName("urn:fepa:messages:v3", "richiesta_associateFascicolo");
    private final static QName _RichiestaSearchDocumentiFascicolo_QNAME = new QName("urn:fepa:messages:v3", "richiesta_searchDocumentiFascicolo");
    private final static QName _RichiestaGetLinkFascicolo_QNAME = new QName("urn:fepa:messages:v3", "richiesta_getLinkFascicolo");
    private final static QName _RispostaConvertDocumento_QNAME = new QName("urn:fepa:messages:v3", "risposta_convertDocumento");
    private final static QName _RispostaSearchMetadatoFascicolo_QNAME = new QName("urn:fepa:messages:v3", "risposta_searchMetadatoFascicolo");
    private final static QName _RispostaSearchDocumentiFascicolo_QNAME = new QName("urn:fepa:messages:v3", "risposta_searchDocumentiFascicolo");
    private final static QName _RichiestaConvertDocumento_QNAME = new QName("urn:fepa:messages:v3", "richiesta_convertDocumento");
    private final static QName _RispostaLockSubFascicles_QNAME = new QName("urn:fepa:messages:v3", "risposta_lockSubFascicles");
    private final static QName _RichiestaUpdateDocumentoFascicolo_QNAME = new QName("urn:fepa:messages:v3", "richiesta_updateDocumentoFascicolo");
    private final static QName _RispostaAssociateFascicolo_QNAME = new QName("urn:fepa:messages:v3", "risposta_associateFascicolo");
    private final static QName _RispostaUpdateDocumentoFascicolo_QNAME = new QName("urn:fepa:messages:v3", "risposta_updateDocumentoFascicolo");
    private final static QName _RispostaUpdateFascicolo_QNAME = new QName("urn:fepa:messages:v3", "risposta_updateFascicolo");
    private final static QName _RichiestaDisAssociateFascicolo_QNAME = new QName("urn:fepa:messages:v3", "richiesta_disAssociateFascicolo");
    private final static QName _RichiestaDownloadFileContent_QNAME = new QName("urn:fepa:messages:v3", "richiesta_downloadFileContent");
    private final static QName _RichiestaArchiveFascicolo_QNAME = new QName("urn:fepa:messages:v3", "richiesta_archiveFascicolo");
    private final static QName _RichiestaRemoveFascicolo_QNAME = new QName("urn:fepa:messages:v3", "richiesta_removeFascicolo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: fepa.messages.v3
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RispostaCreateFascicolo }
     * 
     */
    public RispostaCreateFascicolo createRispostaCreateFascicolo() {
        return new RispostaCreateFascicolo();
    }

    /**
     * Create an instance of {@link RispostaGetFascicolo }
     * 
     */
    public RispostaGetFascicolo createRispostaGetFascicolo() {
        return new RispostaGetFascicolo();
    }

    /**
     * Create an instance of {@link RispostaAddDocumentoFascicolo }
     * 
     */
    public RispostaAddDocumentoFascicolo createRispostaAddDocumentoFascicolo() {
        return new RispostaAddDocumentoFascicolo();
    }

    /**
     * Create an instance of {@link RispostaDownloadDocumentoFascicolo }
     * 
     */
    public RispostaDownloadDocumentoFascicolo createRispostaDownloadDocumentoFascicolo() {
        return new RispostaDownloadDocumentoFascicolo();
    }

    /**
     * Create an instance of {@link RispostaDownloadDocumentoUrlFascicolo }
     * 
     */
    public RispostaDownloadDocumentoUrlFascicolo createRispostaDownloadDocumentoUrlFascicolo() {
        return new RispostaDownloadDocumentoUrlFascicolo();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRemoveDocumentoFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "risposta_removeDocumentoFascicolo")
    public JAXBElement<RispostaRemoveDocumentoFascicoloType> createRispostaRemoveDocumentoFascicolo(RispostaRemoveDocumentoFascicoloType value) {
        return new JAXBElement<RispostaRemoveDocumentoFascicoloType>(_RispostaRemoveDocumentoFascicolo_QNAME, RispostaRemoveDocumentoFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaArchiveFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "risposta_archiveFascicolo")
    public JAXBElement<RispostaArchiveFascicoloType> createRispostaArchiveFascicolo(RispostaArchiveFascicoloType value) {
        return new JAXBElement<RispostaArchiveFascicoloType>(_RispostaArchiveFascicolo_QNAME, RispostaArchiveFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaUpdateFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "richiesta_updateFascicolo")
    public JAXBElement<RichiestaUpdateFascicoloType> createRichiestaUpdateFascicolo(RichiestaUpdateFascicoloType value) {
        return new JAXBElement<RichiestaUpdateFascicoloType>(_RichiestaUpdateFascicolo_QNAME, RichiestaUpdateFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaUploadFileContentType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "richiesta_uploadFileContent")
    public JAXBElement<RichiestaUploadFileContentType> createRichiestaUploadFileContent(RichiestaUploadFileContentType value) {
        return new JAXBElement<RichiestaUploadFileContentType>(_RichiestaUploadFileContent_QNAME, RichiestaUploadFileContentType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetDocumentoFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "richiesta_getDocumentoFascicolo")
    public JAXBElement<RichiestaGetDocumentoFascicoloType> createRichiestaGetDocumentoFascicolo(RichiestaGetDocumentoFascicoloType value) {
        return new JAXBElement<RichiestaGetDocumentoFascicoloType>(_RichiestaGetDocumentoFascicolo_QNAME, RichiestaGetDocumentoFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaChangeStatoFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "richiesta_changeStatoFascicolo")
    public JAXBElement<RichiestaChangeStatoFascicoloType> createRichiestaChangeStatoFascicolo(RichiestaChangeStatoFascicoloType value) {
        return new JAXBElement<RichiestaChangeStatoFascicoloType>(_RichiestaChangeStatoFascicolo_QNAME, RichiestaChangeStatoFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRemoveDocumentoFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "richiesta_removeDocumentoFascicolo")
    public JAXBElement<RichiestaRemoveDocumentoFascicoloType> createRichiestaRemoveDocumentoFascicolo(RichiestaRemoveDocumentoFascicoloType value) {
        return new JAXBElement<RichiestaRemoveDocumentoFascicoloType>(_RichiestaRemoveDocumentoFascicolo_QNAME, RichiestaRemoveDocumentoFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaDownloadDocumentoFascicoloByTypeDocType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "risposta_downloadDocumentoFascicoloByTypeDoc")
    public JAXBElement<RispostaDownloadDocumentoFascicoloByTypeDocType> createRispostaDownloadDocumentoFascicoloByTypeDoc(RispostaDownloadDocumentoFascicoloByTypeDocType value) {
        return new JAXBElement<RispostaDownloadDocumentoFascicoloByTypeDocType>(_RispostaDownloadDocumentoFascicoloByTypeDoc_QNAME, RispostaDownloadDocumentoFascicoloByTypeDocType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaChangeStatoFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "risposta_changeStatoFascicolo")
    public JAXBElement<RispostaChangeStatoFascicoloType> createRispostaChangeStatoFascicolo(RispostaChangeStatoFascicoloType value) {
        return new JAXBElement<RispostaChangeStatoFascicoloType>(_RispostaChangeStatoFascicolo_QNAME, RispostaChangeStatoFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAddDocumentoFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "richiesta_addDocumentoFascicolo")
    public JAXBElement<RichiestaAddDocumentoFascicoloType> createRichiestaAddDocumentoFascicolo(RichiestaAddDocumentoFascicoloType value) {
        return new JAXBElement<RichiestaAddDocumentoFascicoloType>(_RichiestaAddDocumentoFascicolo_QNAME, RichiestaAddDocumentoFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaGetDocumentoFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "risposta_getDocumentoFascicolo")
    public JAXBElement<RispostaGetDocumentoFascicoloType> createRispostaGetDocumentoFascicolo(RispostaGetDocumentoFascicoloType value) {
        return new JAXBElement<RispostaGetDocumentoFascicoloType>(_RispostaGetDocumentoFascicolo_QNAME, RispostaGetDocumentoFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaLockSubFasciclesType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "richiesta_lockSubFascicles")
    public JAXBElement<RichiestaLockSubFasciclesType> createRichiestaLockSubFascicles(RichiestaLockSubFasciclesType value) {
        return new JAXBElement<RichiestaLockSubFasciclesType>(_RichiestaLockSubFascicles_QNAME, RichiestaLockSubFasciclesType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaGetLinkFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "risposta_getLinkFascicolo")
    public JAXBElement<RispostaGetLinkFascicoloType> createRispostaGetLinkFascicolo(RispostaGetLinkFascicoloType value) {
        return new JAXBElement<RispostaGetLinkFascicoloType>(_RispostaGetLinkFascicolo_QNAME, RispostaGetLinkFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaDownloadDocumentoFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "richiesta_downloadDocumentoFascicolo")
    public JAXBElement<RichiestaDownloadDocumentoFascicoloType> createRichiestaDownloadDocumentoFascicolo(RichiestaDownloadDocumentoFascicoloType value) {
        return new JAXBElement<RichiestaDownloadDocumentoFascicoloType>(_RichiestaDownloadDocumentoFascicolo_QNAME, RichiestaDownloadDocumentoFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaUploadFileContentType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "risposta_uploadFileContent")
    public JAXBElement<RispostaUploadFileContentType> createRispostaUploadFileContent(RispostaUploadFileContentType value) {
        return new JAXBElement<RispostaUploadFileContentType>(_RispostaUploadFileContent_QNAME, RispostaUploadFileContentType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaSearchMetadatiFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "richiesta_searchMetadatoFascicolo")
    public JAXBElement<RichiestaSearchMetadatiFascicoloType> createRichiestaSearchMetadatoFascicolo(RichiestaSearchMetadatiFascicoloType value) {
        return new JAXBElement<RichiestaSearchMetadatiFascicoloType>(_RichiestaSearchMetadatoFascicolo_QNAME, RichiestaSearchMetadatiFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAssociaProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "richiesta_associaProtocollo")
    public JAXBElement<RichiestaAssociaProtocolloType> createRichiestaAssociaProtocollo(RichiestaAssociaProtocolloType value) {
        return new JAXBElement<RichiestaAssociaProtocolloType>(_RichiestaAssociaProtocollo_QNAME, RichiestaAssociaProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaDownloadFileContentType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "risposta_downloadFileContent")
    public JAXBElement<RispostaDownloadFileContentType> createRispostaDownloadFileContent(RispostaDownloadFileContentType value) {
        return new JAXBElement<RispostaDownloadFileContentType>(_RispostaDownloadFileContent_QNAME, RispostaDownloadFileContentType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaAssociaProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "risposta_associaProtocollo")
    public JAXBElement<RispostaAssociaProtocolloType> createRispostaAssociaProtocollo(RispostaAssociaProtocolloType value) {
        return new JAXBElement<RispostaAssociaProtocolloType>(_RispostaAssociaProtocollo_QNAME, RispostaAssociaProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRemoveFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "risposta_removeFascicolo")
    public JAXBElement<RispostaRemoveFascicoloType> createRispostaRemoveFascicolo(RispostaRemoveFascicoloType value) {
        return new JAXBElement<RispostaRemoveFascicoloType>(_RispostaRemoveFascicolo_QNAME, RispostaRemoveFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaDisAssociateFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "risposta_disAssociateFascicolo")
    public JAXBElement<RispostaDisAssociateFascicoloType> createRispostaDisAssociateFascicolo(RispostaDisAssociateFascicoloType value) {
        return new JAXBElement<RispostaDisAssociateFascicoloType>(_RispostaDisAssociateFascicolo_QNAME, RispostaDisAssociateFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaSendFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "risposta_sendFascicolo")
    public JAXBElement<RispostaSendFascicoloType> createRispostaSendFascicolo(RispostaSendFascicoloType value) {
        return new JAXBElement<RispostaSendFascicoloType>(_RispostaSendFascicolo_QNAME, RispostaSendFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaCreateFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "richiesta_createFascicolo")
    public JAXBElement<RichiestaCreateFascicoloType> createRichiestaCreateFascicolo(RichiestaCreateFascicoloType value) {
        return new JAXBElement<RichiestaCreateFascicoloType>(_RichiestaCreateFascicolo_QNAME, RichiestaCreateFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaSendFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "richiesta_sendFascicolo")
    public JAXBElement<RichiestaSendFascicoloType> createRichiestaSendFascicolo(RichiestaSendFascicoloType value) {
        return new JAXBElement<RichiestaSendFascicoloType>(_RichiestaSendFascicolo_QNAME, RichiestaSendFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "richiesta_getFascicolo")
    public JAXBElement<RichiestaGetFascicoloType> createRichiestaGetFascicolo(RichiestaGetFascicoloType value) {
        return new JAXBElement<RichiestaGetFascicoloType>(_RichiestaGetFascicolo_QNAME, RichiestaGetFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaDownloadDocumentoFascicoloByTypeDocType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "richiesta_downloadDocumentoFascicoloByTypeDoc")
    public JAXBElement<RichiestaDownloadDocumentoFascicoloByTypeDocType> createRichiestaDownloadDocumentoFascicoloByTypeDoc(RichiestaDownloadDocumentoFascicoloByTypeDocType value) {
        return new JAXBElement<RichiestaDownloadDocumentoFascicoloByTypeDocType>(_RichiestaDownloadDocumentoFascicoloByTypeDoc_QNAME, RichiestaDownloadDocumentoFascicoloByTypeDocType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaDownloadDocumentoUrlFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "richiesta_downloadDocumentoUrlFascicolo")
    public JAXBElement<RichiestaDownloadDocumentoUrlFascicoloType> createRichiestaDownloadDocumentoUrlFascicolo(RichiestaDownloadDocumentoUrlFascicoloType value) {
        return new JAXBElement<RichiestaDownloadDocumentoUrlFascicoloType>(_RichiestaDownloadDocumentoUrlFascicolo_QNAME, RichiestaDownloadDocumentoUrlFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAssociateFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "richiesta_associateFascicolo")
    public JAXBElement<RichiestaAssociateFascicoloType> createRichiestaAssociateFascicolo(RichiestaAssociateFascicoloType value) {
        return new JAXBElement<RichiestaAssociateFascicoloType>(_RichiestaAssociateFascicolo_QNAME, RichiestaAssociateFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaSearchDocumentiFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "richiesta_searchDocumentiFascicolo")
    public JAXBElement<RichiestaSearchDocumentiFascicoloType> createRichiestaSearchDocumentiFascicolo(RichiestaSearchDocumentiFascicoloType value) {
        return new JAXBElement<RichiestaSearchDocumentiFascicoloType>(_RichiestaSearchDocumentiFascicolo_QNAME, RichiestaSearchDocumentiFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetLinkFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "richiesta_getLinkFascicolo")
    public JAXBElement<RichiestaGetLinkFascicoloType> createRichiestaGetLinkFascicolo(RichiestaGetLinkFascicoloType value) {
        return new JAXBElement<RichiestaGetLinkFascicoloType>(_RichiestaGetLinkFascicolo_QNAME, RichiestaGetLinkFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaConvertDocumentoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "risposta_convertDocumento")
    public JAXBElement<RispostaConvertDocumentoType> createRispostaConvertDocumento(RispostaConvertDocumentoType value) {
        return new JAXBElement<RispostaConvertDocumentoType>(_RispostaConvertDocumento_QNAME, RispostaConvertDocumentoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaSearchMetadatiFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "risposta_searchMetadatoFascicolo")
    public JAXBElement<RispostaSearchMetadatiFascicoloType> createRispostaSearchMetadatoFascicolo(RispostaSearchMetadatiFascicoloType value) {
        return new JAXBElement<RispostaSearchMetadatiFascicoloType>(_RispostaSearchMetadatoFascicolo_QNAME, RispostaSearchMetadatiFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaSearchDocumentiFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "risposta_searchDocumentiFascicolo")
    public JAXBElement<RispostaSearchDocumentiFascicoloType> createRispostaSearchDocumentiFascicolo(RispostaSearchDocumentiFascicoloType value) {
        return new JAXBElement<RispostaSearchDocumentiFascicoloType>(_RispostaSearchDocumentiFascicolo_QNAME, RispostaSearchDocumentiFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaConvertDocumentoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "richiesta_convertDocumento")
    public JAXBElement<RichiestaConvertDocumentoType> createRichiestaConvertDocumento(RichiestaConvertDocumentoType value) {
        return new JAXBElement<RichiestaConvertDocumentoType>(_RichiestaConvertDocumento_QNAME, RichiestaConvertDocumentoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaLockSubFasciclesType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "risposta_lockSubFascicles")
    public JAXBElement<RispostaLockSubFasciclesType> createRispostaLockSubFascicles(RispostaLockSubFasciclesType value) {
        return new JAXBElement<RispostaLockSubFasciclesType>(_RispostaLockSubFascicles_QNAME, RispostaLockSubFasciclesType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaUpdateDocumentoFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "richiesta_updateDocumentoFascicolo")
    public JAXBElement<RichiestaUpdateDocumentoFascicoloType> createRichiestaUpdateDocumentoFascicolo(RichiestaUpdateDocumentoFascicoloType value) {
        return new JAXBElement<RichiestaUpdateDocumentoFascicoloType>(_RichiestaUpdateDocumentoFascicolo_QNAME, RichiestaUpdateDocumentoFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaAssociateFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "risposta_associateFascicolo")
    public JAXBElement<RispostaAssociateFascicoloType> createRispostaAssociateFascicolo(RispostaAssociateFascicoloType value) {
        return new JAXBElement<RispostaAssociateFascicoloType>(_RispostaAssociateFascicolo_QNAME, RispostaAssociateFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaUpdateDocumentoFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "risposta_updateDocumentoFascicolo")
    public JAXBElement<RispostaUpdateDocumentoFascicoloType> createRispostaUpdateDocumentoFascicolo(RispostaUpdateDocumentoFascicoloType value) {
        return new JAXBElement<RispostaUpdateDocumentoFascicoloType>(_RispostaUpdateDocumentoFascicolo_QNAME, RispostaUpdateDocumentoFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaUpdateFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "risposta_updateFascicolo")
    public JAXBElement<RispostaUpdateFascicoloType> createRispostaUpdateFascicolo(RispostaUpdateFascicoloType value) {
        return new JAXBElement<RispostaUpdateFascicoloType>(_RispostaUpdateFascicolo_QNAME, RispostaUpdateFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaDisAssociateFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "richiesta_disAssociateFascicolo")
    public JAXBElement<RichiestaDisAssociateFascicoloType> createRichiestaDisAssociateFascicolo(RichiestaDisAssociateFascicoloType value) {
        return new JAXBElement<RichiestaDisAssociateFascicoloType>(_RichiestaDisAssociateFascicolo_QNAME, RichiestaDisAssociateFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaDownloadFileContentType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "richiesta_downloadFileContent")
    public JAXBElement<RichiestaDownloadFileContentType> createRichiestaDownloadFileContent(RichiestaDownloadFileContentType value) {
        return new JAXBElement<RichiestaDownloadFileContentType>(_RichiestaDownloadFileContent_QNAME, RichiestaDownloadFileContentType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaArchiveFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "richiesta_archiveFascicolo")
    public JAXBElement<RichiestaArchiveFascicoloType> createRichiestaArchiveFascicolo(RichiestaArchiveFascicoloType value) {
        return new JAXBElement<RichiestaArchiveFascicoloType>(_RichiestaArchiveFascicolo_QNAME, RichiestaArchiveFascicoloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRemoveFascicoloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:fepa:messages:v3", name = "richiesta_removeFascicolo")
    public JAXBElement<RichiestaRemoveFascicoloType> createRichiestaRemoveFascicolo(RichiestaRemoveFascicoloType value) {
        return new JAXBElement<RichiestaRemoveFascicoloType>(_RichiestaRemoveFascicolo_QNAME, RichiestaRemoveFascicoloType.class, null, value);
    }

}
