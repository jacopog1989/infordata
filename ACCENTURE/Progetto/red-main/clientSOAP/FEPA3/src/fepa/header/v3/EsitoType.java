
package fepa.header.v3;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per esito_type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="esito_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="OK"/>
 *     &lt;enumeration value="ERRORE_DATI_NON_PRESENTI"/>
 *     &lt;enumeration value="ERRORE_NON_AUTORIZZATO"/>
 *     &lt;enumeration value="ERRORE_DATI_NON_DI_COMPETENZA"/>
 *     &lt;enumeration value="ERRORE_ELABORAZIONE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "esito_type")
@XmlEnum
public enum EsitoType {

    OK,
    ERRORE_DATI_NON_PRESENTI,
    ERRORE_NON_AUTORIZZATO,
    ERRORE_DATI_NON_DI_COMPETENZA,
    ERRORE_ELABORAZIONE;

    public String value() {
        return name();
    }

    public static EsitoType fromValue(String v) {
        return valueOf(v);
    }

}
