
package fepa.header.v3;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import fepa.types.v3.RispostaAddDocumentoFascicoloType;
import fepa.types.v3.RispostaArchiveFascicoloType;
import fepa.types.v3.RispostaAssociaProtocolloType;
import fepa.types.v3.RispostaAssociateFascicoloType;
import fepa.types.v3.RispostaChangeStatoFascicoloType;
import fepa.types.v3.RispostaConvertDocumentoType;
import fepa.types.v3.RispostaCreateFascicoloType;
import fepa.types.v3.RispostaDisAssociateFascicoloType;
import fepa.types.v3.RispostaDownloadDocumentoFascicoloByTypeDocType;
import fepa.types.v3.RispostaDownloadDocumentoFascicoloType;
import fepa.types.v3.RispostaDownloadDocumentoUrlFascicoloType;
import fepa.types.v3.RispostaDownloadFileContentType;
import fepa.types.v3.RispostaEliminaFascicoloType;
import fepa.types.v3.RispostaGetDocumentoFascicoloType;
import fepa.types.v3.RispostaGetFascicoloType;
import fepa.types.v3.RispostaGetLinkFascicoloType;
import fepa.types.v3.RispostaGetRisultatoOperazioneDocumentoType;
import fepa.types.v3.RispostaLockSubFasciclesType;
import fepa.types.v3.RispostaRemoveDocumentoFascicoloType;
import fepa.types.v3.RispostaRemoveFascicoloType;
import fepa.types.v3.RispostaSearchDocumentiFascicoloType;
import fepa.types.v3.RispostaSearchMetadatiFascicoloType;
import fepa.types.v3.RispostaSendFascicoloType;
import fepa.types.v3.RispostaUpdateDocumentoFascicoloType;
import fepa.types.v3.RispostaUpdateFascicoloType;
import fepa.types.v3.RispostaUploadFileContentType;


/**
 *  Base response type to be used across all operations 
 * 
 * <p>Classe Java per BaseServiceResponseType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="BaseServiceResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Esito" type="{urn:fepa:header:v3}esito_type"/>
 *         &lt;element name="ErrorList" type="{urn:fepa:header:v3}ServiceErrorType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseServiceResponseType", propOrder = {
    "esito",
    "errorList"
})
@XmlSeeAlso({
    RispostaUpdateDocumentoFascicoloType.class,
    RispostaUploadFileContentType.class,
    RispostaGetLinkFascicoloType.class,
    RispostaDownloadFileContentType.class,
    RispostaAssociaProtocolloType.class,
    RispostaSearchDocumentiFascicoloType.class,
    RispostaConvertDocumentoType.class,
    RispostaLockSubFasciclesType.class,
    RispostaRemoveFascicoloType.class,
    RispostaSearchMetadatiFascicoloType.class,
    RispostaAssociateFascicoloType.class,
    RispostaUpdateFascicoloType.class,
    RispostaRemoveDocumentoFascicoloType.class,
    RispostaGetDocumentoFascicoloType.class,
    RispostaGetRisultatoOperazioneDocumentoType.class,
    RispostaDownloadDocumentoFascicoloByTypeDocType.class,
    RispostaArchiveFascicoloType.class,
    RispostaEliminaFascicoloType.class,
    RispostaSendFascicoloType.class,
    RispostaDisAssociateFascicoloType.class,
    RispostaChangeStatoFascicoloType.class,
    RispostaDownloadDocumentoUrlFascicoloType.class,
    RispostaDownloadDocumentoFascicoloType.class,
    RispostaAddDocumentoFascicoloType.class,
    RispostaGetFascicoloType.class,
    RispostaCreateFascicoloType.class
})
public abstract class BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "Esito", required = true)
    @XmlSchemaType(name = "string")
    protected EsitoType esito;
    @XmlElement(name = "ErrorList")
    protected List<ServiceErrorType> errorList;

    /**
     * Recupera il valore della proprietà esito.
     * 
     * @return
     *     possible object is
     *     {@link EsitoType }
     *     
     */
    public EsitoType getEsito() {
        return esito;
    }

    /**
     * Imposta il valore della proprietà esito.
     * 
     * @param value
     *     allowed object is
     *     {@link EsitoType }
     *     
     */
    public void setEsito(EsitoType value) {
        this.esito = value;
    }

    /**
     * Gets the value of the errorList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the errorList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErrorList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ServiceErrorType }
     * 
     * 
     */
    public List<ServiceErrorType> getErrorList() {
        if (errorList == null) {
            errorList = new ArrayList<ServiceErrorType>();
        }
        return this.errorList;
    }

}
