
package fepa.header.v3;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;extension base="{urn:fepa:header:v3}BaseServiceFaultType">
 *       &lt;sequence>
 *         &lt;element name="FAULTCODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "faultcode"
})
@XmlRootElement(name = "GenericFault")
public class GenericFault
    extends BaseServiceFaultType
    implements Serializable
{

    @XmlElement(name = "FAULTCODE", required = true)
    protected String faultcode;

    /**
     * Recupera il valore della proprietà faultcode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFAULTCODE() {
        return faultcode;
    }

    /**
     * Imposta il valore della proprietà faultcode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFAULTCODE(String value) {
        this.faultcode = value;
    }

}
