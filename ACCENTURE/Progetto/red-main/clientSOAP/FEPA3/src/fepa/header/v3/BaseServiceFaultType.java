
package fepa.header.v3;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 *  Base fault to be used across a series of faults 
 * 
 * <p>Classe Java per BaseServiceFaultType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="BaseServiceFaultType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ErrorDetails" type="{urn:fepa:header:v3}ServiceErrorType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BaseServiceFaultType", propOrder = {
    "errorDetails"
})
@XmlSeeAlso({
    SecurityFault.class,
    GenericFault.class
})
public class BaseServiceFaultType
    implements Serializable
{

    @XmlElement(name = "ErrorDetails", required = true)
    protected ServiceErrorType errorDetails;

    /**
     * Recupera il valore della proprietà errorDetails.
     * 
     * @return
     *     possible object is
     *     {@link ServiceErrorType }
     *     
     */
    public ServiceErrorType getErrorDetails() {
        return errorDetails;
    }

    /**
     * Imposta il valore della proprietà errorDetails.
     * 
     * @param value
     *     allowed object is
     *     {@link ServiceErrorType }
     *     
     */
    public void setErrorDetails(ServiceErrorType value) {
        this.errorDetails = value;
    }

}
