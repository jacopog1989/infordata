
package org.datacontract.schemas._2004._07.prevalidatoreesternoasincrono_application_dto;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.datacontract.schemas._2004._07.prevalidatoreesternoasincrono_application_dto package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ArrayOfEsitoRedOPDTO_QNAME = new QName("http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.RedOP", "ArrayOfEsitoRedOPDTO");
    private final static QName _ArrayOfRedNodoDecretoLiquidazioneDTO_QNAME = new QName("http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.RedOP", "ArrayOfRedNodoDecretoLiquidazioneDTO");
    private final static QName _EsitoRedOPDTO_QNAME = new QName("http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.RedOP", "EsitoRedOPDTO");
    private final static QName _EnumClienteDTO_QNAME = new QName("http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.Enums", "EnumClienteDTO");
    private final static QName _RedNodoDecretoLiquidazioneDTO_QNAME = new QName("http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.RedOP", "RedNodoDecretoLiquidazioneDTO");
    private final static QName _EnumRedStatoDTO_QNAME = new QName("http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.Enums", "EnumRedStatoDTO");
    private final static QName _RedNodoDecretoLiquidazioneDTONumeroDecreto_QNAME = new QName("http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.RedOP", "NumeroDecreto");
    private final static QName _EsitoRedOPDTOCodiceRagioneria_QNAME = new QName("http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.RedOP", "CodiceRagioneria");
    private final static QName _EsitoRedOPDTOCodiceAmministrazione_QNAME = new QName("http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.RedOP", "CodiceAmministrazione");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.datacontract.schemas._2004._07.prevalidatoreesternoasincrono_application_dto
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ArrayOfEsitoRedOPDTO }
     * 
     */
    public ArrayOfEsitoRedOPDTO createArrayOfEsitoRedOPDTO() {
        return new ArrayOfEsitoRedOPDTO();
    }

    /**
     * Create an instance of {@link ArrayOfRedNodoDecretoLiquidazioneDTO }
     * 
     */
    public ArrayOfRedNodoDecretoLiquidazioneDTO createArrayOfRedNodoDecretoLiquidazioneDTO() {
        return new ArrayOfRedNodoDecretoLiquidazioneDTO();
    }

    /**
     * Create an instance of {@link RedNodoDecretoLiquidazioneDTO }
     * 
     */
    public RedNodoDecretoLiquidazioneDTO createRedNodoDecretoLiquidazioneDTO() {
        return new RedNodoDecretoLiquidazioneDTO();
    }

    /**
     * Create an instance of {@link EsitoRedOPDTO }
     * 
     */
    public EsitoRedOPDTO createEsitoRedOPDTO() {
        return new EsitoRedOPDTO();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfEsitoRedOPDTO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.RedOP", name = "ArrayOfEsitoRedOPDTO")
    public JAXBElement<ArrayOfEsitoRedOPDTO> createArrayOfEsitoRedOPDTO(ArrayOfEsitoRedOPDTO value) {
        return new JAXBElement<ArrayOfEsitoRedOPDTO>(_ArrayOfEsitoRedOPDTO_QNAME, ArrayOfEsitoRedOPDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRedNodoDecretoLiquidazioneDTO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.RedOP", name = "ArrayOfRedNodoDecretoLiquidazioneDTO")
    public JAXBElement<ArrayOfRedNodoDecretoLiquidazioneDTO> createArrayOfRedNodoDecretoLiquidazioneDTO(ArrayOfRedNodoDecretoLiquidazioneDTO value) {
        return new JAXBElement<ArrayOfRedNodoDecretoLiquidazioneDTO>(_ArrayOfRedNodoDecretoLiquidazioneDTO_QNAME, ArrayOfRedNodoDecretoLiquidazioneDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EsitoRedOPDTO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.RedOP", name = "EsitoRedOPDTO")
    public JAXBElement<EsitoRedOPDTO> createEsitoRedOPDTO(EsitoRedOPDTO value) {
        return new JAXBElement<EsitoRedOPDTO>(_EsitoRedOPDTO_QNAME, EsitoRedOPDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnumClienteDTO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.Enums", name = "EnumClienteDTO")
    public JAXBElement<EnumClienteDTO> createEnumClienteDTO(EnumClienteDTO value) {
        return new JAXBElement<EnumClienteDTO>(_EnumClienteDTO_QNAME, EnumClienteDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RedNodoDecretoLiquidazioneDTO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.RedOP", name = "RedNodoDecretoLiquidazioneDTO")
    public JAXBElement<RedNodoDecretoLiquidazioneDTO> createRedNodoDecretoLiquidazioneDTO(RedNodoDecretoLiquidazioneDTO value) {
        return new JAXBElement<RedNodoDecretoLiquidazioneDTO>(_RedNodoDecretoLiquidazioneDTO_QNAME, RedNodoDecretoLiquidazioneDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnumRedStatoDTO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.Enums", name = "EnumRedStatoDTO")
    public JAXBElement<EnumRedStatoDTO> createEnumRedStatoDTO(EnumRedStatoDTO value) {
        return new JAXBElement<EnumRedStatoDTO>(_EnumRedStatoDTO_QNAME, EnumRedStatoDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.RedOP", name = "NumeroDecreto", scope = RedNodoDecretoLiquidazioneDTO.class)
    public JAXBElement<String> createRedNodoDecretoLiquidazioneDTONumeroDecreto(String value) {
        return new JAXBElement<String>(_RedNodoDecretoLiquidazioneDTONumeroDecreto_QNAME, String.class, RedNodoDecretoLiquidazioneDTO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.RedOP", name = "CodiceRagioneria", scope = EsitoRedOPDTO.class)
    public JAXBElement<String> createEsitoRedOPDTOCodiceRagioneria(String value) {
        return new JAXBElement<String>(_EsitoRedOPDTOCodiceRagioneria_QNAME, String.class, EsitoRedOPDTO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.RedOP", name = "CodiceAmministrazione", scope = EsitoRedOPDTO.class)
    public JAXBElement<String> createEsitoRedOPDTOCodiceAmministrazione(String value) {
        return new JAXBElement<String>(_EsitoRedOPDTOCodiceAmministrazione_QNAME, String.class, EsitoRedOPDTO.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.RedOP", name = "NumeroDecreto", scope = EsitoRedOPDTO.class)
    public JAXBElement<String> createEsitoRedOPDTONumeroDecreto(String value) {
        return new JAXBElement<String>(_RedNodoDecretoLiquidazioneDTONumeroDecreto_QNAME, String.class, EsitoRedOPDTO.class, value);
    }

}
