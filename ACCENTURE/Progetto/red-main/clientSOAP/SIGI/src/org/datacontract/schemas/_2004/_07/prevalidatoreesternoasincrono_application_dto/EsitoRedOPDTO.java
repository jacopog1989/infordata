
package org.datacontract.schemas._2004._07.prevalidatoreesternoasincrono_application_dto;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EsitoRedOPDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EsitoRedOPDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AnnoEseOP" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="CodiceAmministrazione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodiceGestionale" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="CodiceRagioneria" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumeroCapitolo" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="NumeroDecreto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NumeroOP" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="PianoGestione" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="StatoOP" type="{http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.Enums}EnumRedStatoDTO" minOccurs="0"/>
 *         &lt;element name="TipoOP" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EsitoRedOPDTO", propOrder = {
    "annoEseOP",
    "codiceAmministrazione",
    "codiceGestionale",
    "codiceRagioneria",
    "numeroCapitolo",
    "numeroDecreto",
    "numeroOP",
    "pianoGestione",
    "statoOP",
    "tipoOP"
})
public class EsitoRedOPDTO {

    @XmlElement(name = "AnnoEseOP")
    protected Short annoEseOP;
    @XmlElementRef(name = "CodiceAmministrazione", namespace = "http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.RedOP", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codiceAmministrazione;
    @XmlElement(name = "CodiceGestionale")
    protected Short codiceGestionale;
    @XmlElementRef(name = "CodiceRagioneria", namespace = "http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.RedOP", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codiceRagioneria;
    @XmlElement(name = "NumeroCapitolo")
    protected Short numeroCapitolo;
    @XmlElementRef(name = "NumeroDecreto", namespace = "http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.RedOP", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numeroDecreto;
    @XmlElement(name = "NumeroOP")
    protected Integer numeroOP;
    @XmlElement(name = "PianoGestione")
    protected Short pianoGestione;
    @XmlElement(name = "StatoOP")
    @XmlSchemaType(name = "string")
    protected EnumRedStatoDTO statoOP;
    @XmlElement(name = "TipoOP")
    protected Short tipoOP;

    /**
     * Gets the value of the annoEseOP property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getAnnoEseOP() {
        return annoEseOP;
    }

    /**
     * Sets the value of the annoEseOP property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setAnnoEseOP(Short value) {
        this.annoEseOP = value;
    }

    /**
     * Gets the value of the codiceAmministrazione property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodiceAmministrazione() {
        return codiceAmministrazione;
    }

    /**
     * Sets the value of the codiceAmministrazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodiceAmministrazione(JAXBElement<String> value) {
        this.codiceAmministrazione = value;
    }

    /**
     * Gets the value of the codiceGestionale property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getCodiceGestionale() {
        return codiceGestionale;
    }

    /**
     * Sets the value of the codiceGestionale property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setCodiceGestionale(Short value) {
        this.codiceGestionale = value;
    }

    /**
     * Gets the value of the codiceRagioneria property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodiceRagioneria() {
        return codiceRagioneria;
    }

    /**
     * Sets the value of the codiceRagioneria property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodiceRagioneria(JAXBElement<String> value) {
        this.codiceRagioneria = value;
    }

    /**
     * Gets the value of the numeroCapitolo property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getNumeroCapitolo() {
        return numeroCapitolo;
    }

    /**
     * Sets the value of the numeroCapitolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setNumeroCapitolo(Short value) {
        this.numeroCapitolo = value;
    }

    /**
     * Gets the value of the numeroDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumeroDecreto() {
        return numeroDecreto;
    }

    /**
     * Sets the value of the numeroDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumeroDecreto(JAXBElement<String> value) {
        this.numeroDecreto = value;
    }

    /**
     * Gets the value of the numeroOP property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumeroOP() {
        return numeroOP;
    }

    /**
     * Sets the value of the numeroOP property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumeroOP(Integer value) {
        this.numeroOP = value;
    }

    /**
     * Gets the value of the pianoGestione property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getPianoGestione() {
        return pianoGestione;
    }

    /**
     * Sets the value of the pianoGestione property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setPianoGestione(Short value) {
        this.pianoGestione = value;
    }

    /**
     * Gets the value of the statoOP property.
     * 
     * @return
     *     possible object is
     *     {@link EnumRedStatoDTO }
     *     
     */
    public EnumRedStatoDTO getStatoOP() {
        return statoOP;
    }

    /**
     * Sets the value of the statoOP property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumRedStatoDTO }
     *     
     */
    public void setStatoOP(EnumRedStatoDTO value) {
        this.statoOP = value;
    }

    /**
     * Gets the value of the tipoOP property.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getTipoOP() {
        return tipoOP;
    }

    /**
     * Sets the value of the tipoOP property.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setTipoOP(Short value) {
        this.tipoOP = value;
    }

}
