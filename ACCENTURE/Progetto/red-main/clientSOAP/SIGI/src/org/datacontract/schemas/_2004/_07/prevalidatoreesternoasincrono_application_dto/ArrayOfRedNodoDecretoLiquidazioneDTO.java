
package org.datacontract.schemas._2004._07.prevalidatoreesternoasincrono_application_dto;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfRedNodoDecretoLiquidazioneDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfRedNodoDecretoLiquidazioneDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RedNodoDecretoLiquidazioneDTO" type="{http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.RedOP}RedNodoDecretoLiquidazioneDTO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfRedNodoDecretoLiquidazioneDTO", propOrder = {
    "redNodoDecretoLiquidazioneDTO"
})
public class ArrayOfRedNodoDecretoLiquidazioneDTO {

    @XmlElement(name = "RedNodoDecretoLiquidazioneDTO", nillable = true)
    protected List<RedNodoDecretoLiquidazioneDTO> redNodoDecretoLiquidazioneDTO;

    /**
     * Gets the value of the redNodoDecretoLiquidazioneDTO property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the redNodoDecretoLiquidazioneDTO property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRedNodoDecretoLiquidazioneDTO().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RedNodoDecretoLiquidazioneDTO }
     * 
     * 
     */
    public List<RedNodoDecretoLiquidazioneDTO> getRedNodoDecretoLiquidazioneDTO() {
        if (redNodoDecretoLiquidazioneDTO == null) {
            redNodoDecretoLiquidazioneDTO = new ArrayList<RedNodoDecretoLiquidazioneDTO>();
        }
        return this.redNodoDecretoLiquidazioneDTO;
    }

}
