
package org.datacontract.schemas._2004._07.prevalidatoreesternoasincrono_application_dto;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EnumRedStatoDTO.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EnumRedStatoDTO">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Nuovo"/>
 *     &lt;enumeration value="Inviato"/>
 *     &lt;enumeration value="Annullato"/>
 *     &lt;enumeration value="OPAssente"/>
 *     &lt;enumeration value="DLAssente"/>
 *     &lt;enumeration value="Errore"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EnumRedStatoDTO", namespace = "http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.Enums")
@XmlEnum
public enum EnumRedStatoDTO {

    @XmlEnumValue("Nuovo")
    NUOVO("Nuovo"),
    @XmlEnumValue("Inviato")
    INVIATO("Inviato"),
    @XmlEnumValue("Annullato")
    ANNULLATO("Annullato"),
    @XmlEnumValue("OPAssente")
    OP_ASSENTE("OPAssente"),
    @XmlEnumValue("DLAssente")
    DL_ASSENTE("DLAssente"),
    @XmlEnumValue("Errore")
    ERRORE("Errore");
    private final String value;

    EnumRedStatoDTO(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EnumRedStatoDTO fromValue(String v) {
        for (EnumRedStatoDTO c: EnumRedStatoDTO.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
