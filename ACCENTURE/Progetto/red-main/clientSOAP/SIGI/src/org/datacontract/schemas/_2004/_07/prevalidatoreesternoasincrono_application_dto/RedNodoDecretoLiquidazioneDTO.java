
package org.datacontract.schemas._2004._07.prevalidatoreesternoasincrono_application_dto;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RedNodoDecretoLiquidazioneDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RedNodoDecretoLiquidazioneDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Cliente" type="{http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.Enums}EnumClienteDTO" minOccurs="0"/>
 *         &lt;element name="NumeroDecreto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RedNodoDecretoLiquidazioneDTO", propOrder = {
    "cliente",
    "numeroDecreto"
})
public class RedNodoDecretoLiquidazioneDTO {

    @XmlElement(name = "Cliente")
    @XmlSchemaType(name = "string")
    protected EnumClienteDTO cliente;
    @XmlElementRef(name = "NumeroDecreto", namespace = "http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.RedOP", type = JAXBElement.class, required = false)
    protected JAXBElement<String> numeroDecreto;

    /**
     * Gets the value of the cliente property.
     * 
     * @return
     *     possible object is
     *     {@link EnumClienteDTO }
     *     
     */
    public EnumClienteDTO getCliente() {
        return cliente;
    }

    /**
     * Sets the value of the cliente property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumClienteDTO }
     *     
     */
    public void setCliente(EnumClienteDTO value) {
        this.cliente = value;
    }

    /**
     * Gets the value of the numeroDecreto property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getNumeroDecreto() {
        return numeroDecreto;
    }

    /**
     * Sets the value of the numeroDecreto property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setNumeroDecreto(JAXBElement<String> value) {
        this.numeroDecreto = value;
    }

}
