
package org.datacontract.schemas._2004._07.prevalidatoreesternoasincrono_application_dto;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EnumClienteDTO.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EnumClienteDTO">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="RGS"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EnumClienteDTO", namespace = "http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.Enums")
@XmlEnum
public enum EnumClienteDTO {

    RGS;

    public String value() {
        return name();
    }

    public static EnumClienteDTO fromValue(String v) {
        return valueOf(v);
    }

}
