
package org.tempuri;

import java.net.URL;

import javax.jws.HandlerChain;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;

@WebServiceClient(name = "RedService", targetNamespace = "http://tempuri.org/", wsdlLocation = "META-INF/wsdl/RedService_1.wsdl")
@HandlerChain(file="handler-chain.xml")
public class RedService
    extends Service
{

    private final static URL REDSERVICE_WSDL_LOCATION;
    private final static WebServiceException REDSERVICE_EXCEPTION;
    private final static QName REDSERVICE_QNAME = new QName("http://tempuri.org/", "RedService");

    static {
            REDSERVICE_WSDL_LOCATION = org.tempuri.RedService.class.getClassLoader().getResource("/META-INF/wsdl/RedService_1.wsdl");
        WebServiceException e = null;
        if (REDSERVICE_WSDL_LOCATION == null) {
            e = new WebServiceException("Cannot find 'META-INF/wsdl/RedService_1.wsdl' wsdl. Place the resource correctly in the classpath.");
        }
        REDSERVICE_EXCEPTION = e;
    }

    public RedService() {
        super(__getWsdlLocation(), REDSERVICE_QNAME);
    }

    public RedService(WebServiceFeature... features) {
        super(__getWsdlLocation(), REDSERVICE_QNAME, features);
    }

    public RedService(URL wsdlLocation) {
        super(wsdlLocation, REDSERVICE_QNAME);
    }

    public RedService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, REDSERVICE_QNAME, features);
    }

    public RedService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public RedService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns IRedService
     */
    @WebEndpoint(name = "WSHttpBinding_IRedService")
    public IRedService getWSHttpBindingIRedService() {
        return super.getPort(new QName("http://tempuri.org/", "WSHttpBinding_IRedService"), IRedService.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns IRedService
     */
    @WebEndpoint(name = "WSHttpBinding_IRedService")
    public IRedService getWSHttpBindingIRedService(WebServiceFeature... features) {
        return super.getPort(new QName("http://tempuri.org/", "WSHttpBinding_IRedService"), IRedService.class, features);
    }

    private static URL __getWsdlLocation() {
        if (REDSERVICE_EXCEPTION!= null) {
            throw REDSERVICE_EXCEPTION;
        }
        return REDSERVICE_WSDL_LOCATION;
    }

}
