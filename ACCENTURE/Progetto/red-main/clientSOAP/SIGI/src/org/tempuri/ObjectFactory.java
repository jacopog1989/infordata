
package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import org.datacontract.schemas._2004._07.prevalidatoreesternoasincrono_application_dto.ArrayOfEsitoRedOPDTO;
import org.datacontract.schemas._2004._07.prevalidatoreesternoasincrono_application_dto.ArrayOfRedNodoDecretoLiquidazioneDTO;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetOrdinePagareByDecretoResponseGetOrdinePagareByDecretoResult_QNAME = new QName("http://tempuri.org/", "GetOrdinePagareByDecretoResult");
    private final static QName _GetOrdinePagareByDecretoDtoNodoRed_QNAME = new QName("http://tempuri.org/", "dtoNodoRed");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetOrdinePagareByDecretoResponse }
     * 
     */
    public GetOrdinePagareByDecretoResponse createGetOrdinePagareByDecretoResponse() {
        return new GetOrdinePagareByDecretoResponse();
    }

    /**
     * Create an instance of {@link GetOrdinePagareByDecreto }
     * 
     */
    public GetOrdinePagareByDecreto createGetOrdinePagareByDecreto() {
        return new GetOrdinePagareByDecreto();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfEsitoRedOPDTO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "GetOrdinePagareByDecretoResult", scope = GetOrdinePagareByDecretoResponse.class)
    public JAXBElement<ArrayOfEsitoRedOPDTO> createGetOrdinePagareByDecretoResponseGetOrdinePagareByDecretoResult(ArrayOfEsitoRedOPDTO value) {
        return new JAXBElement<ArrayOfEsitoRedOPDTO>(_GetOrdinePagareByDecretoResponseGetOrdinePagareByDecretoResult_QNAME, ArrayOfEsitoRedOPDTO.class, GetOrdinePagareByDecretoResponse.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ArrayOfRedNodoDecretoLiquidazioneDTO }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://tempuri.org/", name = "dtoNodoRed", scope = GetOrdinePagareByDecreto.class)
    public JAXBElement<ArrayOfRedNodoDecretoLiquidazioneDTO> createGetOrdinePagareByDecretoDtoNodoRed(ArrayOfRedNodoDecretoLiquidazioneDTO value) {
        return new JAXBElement<ArrayOfRedNodoDecretoLiquidazioneDTO>(_GetOrdinePagareByDecretoDtoNodoRed_QNAME, ArrayOfRedNodoDecretoLiquidazioneDTO.class, GetOrdinePagareByDecreto.class, value);
    }

}
