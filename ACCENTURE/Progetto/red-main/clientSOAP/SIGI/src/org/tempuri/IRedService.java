
package org.tempuri;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;
import org.datacontract.schemas._2004._07.prevalidatoreesternoasincrono_application_dto.ArrayOfEsitoRedOPDTO;
import org.datacontract.schemas._2004._07.prevalidatoreesternoasincrono_application_dto.ArrayOfRedNodoDecretoLiquidazioneDTO;

@WebService(name = "IRedService", targetNamespace = "http://tempuri.org/")
@XmlSeeAlso({
    com.microsoft.schemas._2003._10.serialization.ObjectFactory.class,
    org.datacontract.schemas._2004._07.prevalidatoreesternoasincrono_application_dto.ObjectFactory.class,
    org.tempuri.ObjectFactory.class
})
public interface IRedService {


    /**
     * 
     * @param dtoNodoRed
     * @return
     *     returns org.datacontract.schemas._2004._07.prevalidatoreesternoasincrono_application_dto.ArrayOfEsitoRedOPDTO
     */
    @WebMethod(operationName = "GetOrdinePagareByDecreto", action = "http://tempuri.org/IRedService/GetOrdinePagareByDecreto")
    @WebResult(name = "GetOrdinePagareByDecretoResult", targetNamespace = "http://tempuri.org/")
    @RequestWrapper(localName = "GetOrdinePagareByDecreto", targetNamespace = "http://tempuri.org/", className = "org.tempuri.GetOrdinePagareByDecreto")
    @ResponseWrapper(localName = "GetOrdinePagareByDecretoResponse", targetNamespace = "http://tempuri.org/", className = "org.tempuri.GetOrdinePagareByDecretoResponse")
    public ArrayOfEsitoRedOPDTO getOrdinePagareByDecreto(
        @WebParam(name = "dtoNodoRed", targetNamespace = "http://tempuri.org/")
        ArrayOfRedNodoDecretoLiquidazioneDTO dtoNodoRed);

}
