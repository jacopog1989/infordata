
package org.tempuri;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.datacontract.schemas._2004._07.prevalidatoreesternoasincrono_application_dto.ArrayOfRedNodoDecretoLiquidazioneDTO;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dtoNodoRed" type="{http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.RedOP}ArrayOfRedNodoDecretoLiquidazioneDTO" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dtoNodoRed"
})
@XmlRootElement(name = "GetOrdinePagareByDecreto")
public class GetOrdinePagareByDecreto {

    @XmlElementRef(name = "dtoNodoRed", namespace = "http://tempuri.org/", type = JAXBElement.class, required = false)
    protected JAXBElement<ArrayOfRedNodoDecretoLiquidazioneDTO> dtoNodoRed;

    /**
     * Gets the value of the dtoNodoRed property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfRedNodoDecretoLiquidazioneDTO }{@code >}
     *     
     */
    public JAXBElement<ArrayOfRedNodoDecretoLiquidazioneDTO> getDtoNodoRed() {
        return dtoNodoRed;
    }

    /**
     * Sets the value of the dtoNodoRed property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ArrayOfRedNodoDecretoLiquidazioneDTO }{@code >}
     *     
     */
    public void setDtoNodoRed(JAXBElement<ArrayOfRedNodoDecretoLiquidazioneDTO> value) {
        this.dtoNodoRed = value;
    }

}
