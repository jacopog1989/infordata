package it.ibm.sigi;

import org.datacontract.schemas._2004._07.prevalidatoreesternoasincrono_application_dto.EnumRedStatoDTO;

public enum StatoOPEnum {
    
	NUOVO("Nuovo"),
    INVIATO("Inviato"),
    ANNULLATO("Annullato"),
    OP_ASSENTE("OPAssente"),
    DL_ASSENTE("DLAssente"),
    ERRORE("Errore");
	
    private final String value;

    StatoOPEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static StatoOPEnum fromValue(String v) {
    	StatoOPEnum out = null;
    	if (v!=null) {
            for (StatoOPEnum c: StatoOPEnum.values()) {
                if (c.value.equals(v)) {
                    out = c;
                }
            }
    	}
    	return out;
    }
    
    public static StatoOPEnum fromValue(EnumRedStatoDTO v) {
    	StatoOPEnum out = null;
    	if (v != null) {
        	out = StatoOPEnum.fromValue(v.value());
    	}
    	return out;
    }
    
}
