package it.ibm.sigi;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.datacontract.schemas._2004._07.prevalidatoreesternoasincrono_application_dto.ArrayOfEsitoRedOPDTO;
import org.datacontract.schemas._2004._07.prevalidatoreesternoasincrono_application_dto.EsitoRedOPDTO;

public class OrdinePagamentoDTO {
	
	private Integer annoEsercizio;
    private String codiceAmministrazione;
    private Integer codiceGestionale;
    private String codiceRagioneria;
    private Integer numeroCapitolo;
    private String numeroDecreto;
    private Integer numeroOP;
    private Integer pianoGestione;
    private Integer tipoOP;
    private StatoOPEnum statoOP;
	
    static public List<OrdinePagamentoDTO> transform(ArrayOfEsitoRedOPDTO result) {
    	List<OrdinePagamentoDTO> out = new ArrayList<OrdinePagamentoDTO>();
    	if (result.getEsitoRedOPDTO() != null) {
        	for (EsitoRedOPDTO e:result.getEsitoRedOPDTO()) {
        		out.add(new OrdinePagamentoDTO(e));
        	}
    	}
    	return out;
    }
    
    public OrdinePagamentoDTO(EsitoRedOPDTO esito) {
		super();
		this.annoEsercizio = esito.getAnnoEseOP().intValue();
		this.codiceAmministrazione = getStringFromElement(esito.getCodiceAmministrazione());
		this.codiceGestionale = fromShortToInteger(esito.getCodiceGestionale());
		this.codiceRagioneria = getStringFromElement(esito.getCodiceRagioneria());
		this.numeroCapitolo = fromShortToInteger(esito.getNumeroCapitolo());
		this.numeroDecreto = getStringFromElement(esito.getNumeroDecreto());
		this.numeroOP = esito.getNumeroOP();
		this.pianoGestione = fromShortToInteger(esito.getPianoGestione());
		this.tipoOP = fromShortToInteger(esito.getTipoOP());
		this.statoOP = StatoOPEnum.fromValue(esito.getStatoOP());
	}

    private static Integer fromShortToInteger(Short s) {
    	Integer out = null;
    	if (s!=null) {
    		out = s.intValue();
    	}
    	return out;
    }

    private static String getStringFromElement(JAXBElement<String> element) {
    	String output = null;
    	if (element!=null) {
    		output = element.getValue();
    	}
    	return output;
    }
    
    public Integer getAnnoEsercizio() {
		return annoEsercizio;
	}
	public String getCodiceAmministrazione() {
		return codiceAmministrazione;
	}
	public Integer getCodiceGestionale() {
		return codiceGestionale;
	}
	public String getCodiceRagioneria() {
		return codiceRagioneria;
	}
	public Integer getNumeroCapitolo() {
		return numeroCapitolo;
	}
	public String getNumeroDecreto() {
		return numeroDecreto;
	}
	public Integer getNumeroOP() {
		return numeroOP;
	}
	public Integer getPianoGestione() {
		return pianoGestione;
	}
	public Integer getTipoOP() {
		return tipoOP;
	}
	public StatoOPEnum getStatoOP() {
		return statoOP;
	}

    
}
