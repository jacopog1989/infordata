package it.ibm.sigi;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.datacontract.schemas._2004._07.prevalidatoreesternoasincrono_application_dto.ArrayOfEsitoRedOPDTO;
import org.datacontract.schemas._2004._07.prevalidatoreesternoasincrono_application_dto.ArrayOfRedNodoDecretoLiquidazioneDTO;
import org.datacontract.schemas._2004._07.prevalidatoreesternoasincrono_application_dto.EnumClienteDTO;
import org.datacontract.schemas._2004._07.prevalidatoreesternoasincrono_application_dto.RedNodoDecretoLiquidazioneDTO;
import org.tempuri.IRedService;
import org.tempuri.RedService;

public class BusinessDelegate {

	public static List<OrdinePagamentoDTO> getOrdinePagareByDecreto(String urlRedService, String decreto) {
		List<OrdinePagamentoDTO> out = null;
		try {
			RedService d = new RedService(new URL(urlRedService));
			IRedService srv = d.getWSHttpBindingIRedService();

			ArrayOfRedNodoDecretoLiquidazioneDTO dtoNodoRed = new ArrayOfRedNodoDecretoLiquidazioneDTO();
			RedNodoDecretoLiquidazioneDTO e = new RedNodoDecretoLiquidazioneDTO();
			e.setCliente(EnumClienteDTO.RGS);
			JAXBElement<String> numDecreto = new JAXBElement<String>(new QName("http://schemas.datacontract.org/2004/07/PrevalidatoreEsternoAsincrono.Application.Dto.RedOP", "NumeroDecreto"), String.class, decreto);
			e.setNumeroDecreto(numDecreto);
			dtoNodoRed.getRedNodoDecretoLiquidazioneDTO().add(e);

			ArrayOfEsitoRedOPDTO esito = srv.getOrdinePagareByDecreto(dtoNodoRed);

			out = OrdinePagamentoDTO.transform(esito);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
		return out;
	}
	
	public static void main(String[] args) {
		List<OrdinePagamentoDTO> result = BusinessDelegate.getOrdinePagareByDecreto("http://governance2.tesoro.it/RedOPService/RedService.svc?wsdl", "2019-20191");
		System.out.println(result.size());
	}
}
