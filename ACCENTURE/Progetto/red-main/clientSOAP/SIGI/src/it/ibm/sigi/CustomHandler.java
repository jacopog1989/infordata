package it.ibm.sigi;

import java.util.HashSet;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

public class CustomHandler implements SOAPHandler<SOAPMessageContext> {

	@Override
	public boolean handleMessage(SOAPMessageContext context) {
		
		Boolean outboundProperty = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
		if (outboundProperty!=null && outboundProperty) {
			try {
				SOAPMessage message = context.getMessage();
				SOAPEnvelope envelope = message.getSOAPPart().getEnvelope();
				
				SOAPHeader header = envelope.getHeader();
				if (header == null) {
					header = envelope.addHeader();
				}

				SOAPElement elementAction = header.addChildElement(new QName("http://www.w3.org/2005/08/addressing", "Action", "wsa"));
				elementAction.setTextContent("http://tempuri.org/IRedService/GetOrdinePagareByDecreto");
//				elementAction.addAttribute(envelope.createName("soap:mustUnderstand") , "1");
				
				SOAPElement elementTo = header.addChildElement(new QName("http://www.w3.org/2005/08/addressing", "To", "wsa"));
				elementTo.setTextContent("http://collaudo2.governance.tesoro.it/RedOPService/RedService.svc/RedService");
//				elementTo.addAttribute(envelope.createName("soap:mustUnderstand") , "1");
				
			} catch (SOAPException e) {
				e.printStackTrace();
			}
			
		}
		return true;
	}

	@Override
	public boolean handleFault(SOAPMessageContext context) {
		return true;
	}

	@Override
	public void close(MessageContext context) {
	}

	@Override
	public Set<QName> getHeaders() {
        QName actionHeader = new QName("http://www.w3.org/2005/08/addressing", "Action", "wsa"); 
        HashSet<QName> headers = new HashSet<QName>(); 
        headers.add(actionHeader);         
        return headers; 
	}

}
