package it.ibm.red.business.nps.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class ProtocolloNpsDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String idDocumento;
	
	private String oggetto;
	
	private String mittente;
	
	private String destinatario;
	
	private int numeroProtocollo;
	
	private String idProtocollo;
	
	private String annoProtocollo;
	
	private Date dataProtocollo;
	
	private String[] assegnatari;
	
	private String numeroProtocolloAnnoProtocollo;
	
	private String descrizioneTitolario;
	
	private String descrizioneTipologiaDocumento;
	
	private ArrayList<AllegatoNpsDTO> allegatiList;
	
	private ArrayList<AllaccioDTO> allacciList;
	
	private ArrayList<StoricoDTO> storicoList;
	
	private ArrayList<AssegnatariDTO> assegnatariList;
	
	private String tipoProtocollo;
	
	private String destinatariString;
	
	private String mittenteString;
	
	private boolean entrataProtocollo;
	
	private DocumentoNpsDTO documentoPrincipale;
	
	private String statoProtocollo;
	
	private String utenteAnnullatore;
	
	private String motivoAnnullamento;
	
	private String provvedimentoAnnullamento;
	
	private Date dataAnnullamento;
	

	public String getIdDocumento() {
		return this.idDocumento;
	}

	public void setIdDocumento(String idDocumento) {
		this.idDocumento = idDocumento;
	}

	public String getOggetto() {
		return this.oggetto;
	}

	public void setOggetto(String oggetto) {
		this.oggetto = oggetto;
	}

	public String getMittente() {
		return this.mittente;
	}

	public String getDestinatario() {
		return this.destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public void setMittente(String mittente) {
		this.mittente = mittente;
	}

	public String[] getAssegnatari() {
		return this.assegnatari;
	}

	public void setAssegnatari(String[] assegnatari) {
		this.assegnatari = assegnatari;
	}

	public int getNumeroProtocollo() {
		return this.numeroProtocollo;
	}

	public void setNumeroProtocollo(int numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}

	public String getAnnoProtocollo() {
		return this.annoProtocollo;
	}

	public void setAnnoProtocollo(String annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}

	public Date getDataProtocollo() {
		return this.dataProtocollo;
	}

	public void setDataProtocollo(Date dataProtocollo) {
		this.dataProtocollo = dataProtocollo;
	}

	public String getIdProtocollo() {
		return this.idProtocollo;
	}

	public void setIdProtocollo(String idProtocollo) {
		this.idProtocollo = idProtocollo;
	}

	public String getNumeroProtocolloAnnoProtocollo() {
		return this.numeroProtocolloAnnoProtocollo;
	}

	public void setNumeroProtocolloAnnoProtocollo(String numeroProtocolloAnnoProtocollo) {
		this.numeroProtocolloAnnoProtocollo = numeroProtocolloAnnoProtocollo;
	}

	public ArrayList<AllegatoNpsDTO> getAllegatiList() {
		return allegatiList;
	}

	public void setAllegatiList(ArrayList<AllegatoNpsDTO> allegatiList) {
		this.allegatiList = allegatiList;
	}

	public ArrayList<AllaccioDTO> getAllacciList() {
		return this.allacciList;
	}

	public void setAllacciList(ArrayList<AllaccioDTO> allacciList) {
		this.allacciList = allacciList;
	}

	public ArrayList<StoricoDTO> getStoricoList() {
		return this.storicoList;
	}

	public void setStoricoList(ArrayList<StoricoDTO> storicoList) {
		this.storicoList = storicoList;
	}

	public String getDestinatariString() {
		return this.destinatariString;
	}

	public void setDestinatariString(String destinatariString) {
		this.destinatariString = destinatariString;
	}

	public String getMittenteString() {
		return this.mittenteString;
	}

	public void setMittenteString(String mittenteString) {
		this.mittenteString = mittenteString;
	}

	public String getDescrizioneTitolario() {
		return this.descrizioneTitolario;
	}

	public void setDescrizioneTitolario(String descrizioneTitolario) {
		this.descrizioneTitolario = descrizioneTitolario;
	}

	public String getDescrizioneTipologiaDocumento() {
		return this.descrizioneTipologiaDocumento;
	}

	public void setDescrizioneTipologiaDocumento(String descrizioneTipologiaDocumento) {
		this.descrizioneTipologiaDocumento = descrizioneTipologiaDocumento;
	}

	public ArrayList<AssegnatariDTO> getAssegnatariList() {
		return this.assegnatariList;
	}

	public void setAssegnatariList(ArrayList<AssegnatariDTO> assegnatariList) {
		this.assegnatariList = assegnatariList;
	}

	public String getTipoProtocollo() {
		return this.tipoProtocollo;
	}

	public void setTipoProtocollo(String tipoProtocollo) {
		this.tipoProtocollo = tipoProtocollo;
	}

	public boolean isEntrataProtocollo() {
		return this.entrataProtocollo;
	}

	public void setEntrataProtocollo(boolean entrataProtocollo) {
		this.entrataProtocollo = entrataProtocollo;
	}

	public void setStatoProtocollo(String statoProtocollo) {
		this.statoProtocollo = statoProtocollo;
	}

	public String getStatoProtocollo() {
		return this.statoProtocollo;
	}

	public DocumentoNpsDTO getDocumentoPrincipale() {
		return documentoPrincipale;
	}

	public void setDocumentoPrincipale(DocumentoNpsDTO documentoPrincipale) {
		this.documentoPrincipale = documentoPrincipale;
	}

	public String getUtenteAnnullatore() {
		return utenteAnnullatore;
	}

	public void setUtenteAnnullatore(String utenteAnnullatore) {
		this.utenteAnnullatore = utenteAnnullatore;
	}

	public String getMotivoAnnullamento() {
		return motivoAnnullamento;
	}

	public void setMotivoAnnullamento(String motivoAnnullamento) {
		this.motivoAnnullamento = motivoAnnullamento;
	}

	public String getProvvedimentoAnnullamento() {
		return provvedimentoAnnullamento;
	}

	public void setProvvedimentoAnnullamento(String provvedimentoAnnullamento) {
		this.provvedimentoAnnullamento = provvedimentoAnnullamento;
	}

	public Date getDataAnnullamento() {
		return dataAnnullamento;
	}

	public void setDataAnnullamento(Date dataAnnullamento) {
		this.dataAnnullamento = dataAnnullamento;
	}
}