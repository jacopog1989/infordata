package it.ibm.red.business.nps.builder;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;

import org.apache.commons.io.IOUtils;

import it.ibm.red.business.nps.exceptions.BusinessDelegateRuntimeException;
import npsmessages.v1.it.gov.mef.IdentificativoDestinatarioType;
import npsmessages.v1.it.gov.mef.IdentificativoDocumentoRequestType;
import npsmessages.v1.it.gov.mef.IdentificativoProtocolloRequestType;
import npsmessages.v1.it.gov.mef.RichiestaAggiungiAllaccioProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaAggiungiAssegnatarioProtocolloEntrataType;
import npsmessages.v1.it.gov.mef.RichiestaAggiungiDestinatarioProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaAggiungiDocumentiProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaAnnullamentoProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaCambiaStatoProtocolloEntrataType;
import npsmessages.v1.it.gov.mef.RichiestaCambiaStatoProtocolloUscitaType;
import npsmessages.v1.it.gov.mef.RichiestaCollegaRegistrazioneAusiliariaProtocolloIngressoType;
import npsmessages.v1.it.gov.mef.RichiestaCollegaRegistrazioneAusiliariaProtocolloUscitaType;
import npsmessages.v1.it.gov.mef.RichiestaCreateProtocolloEntrataType;
import npsmessages.v1.it.gov.mef.RichiestaCreateProtocolloUscitaType;
import npsmessages.v1.it.gov.mef.RichiestaCreateRegistrazioneAusiliariaType;
import npsmessages.v1.it.gov.mef.RichiestaGetProtocolloEntrataType;
import npsmessages.v1.it.gov.mef.RichiestaGetProtocolloUscita;
import npsmessages.v1.it.gov.mef.RichiestaInviaAttoType;
import npsmessages.v1.it.gov.mef.RichiestaInviaMessaggioPostaType;
import npsmessages.v1.it.gov.mef.RichiestaModificaSpedizioneDestinatarioProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaReportProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaReportRegistroGiornalieroType;
import npsmessages.v1.it.gov.mef.RichiestaRicercaProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaRicercaRegistrazioneAusiliariaType;
import npsmessages.v1.it.gov.mef.RichiestaRimuoviAllaccioProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaRimuoviDestinatarioProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaRimuoviDocumentoProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaSpedisciProtocolloUscitaType;
import npsmessages.v1.it.gov.mef.RichiestaUpdateDatiProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaUploadDocumentoType;
import npstypes.v1.it.gov.mef.AllChiaveEsternaType;
import npstypes.v1.it.gov.mef.AllCodiceDescrizioneType;
import npstypes.v1.it.gov.mef.AllDataRangeType;
import npstypes.v1.it.gov.mef.AllNumeroRangeType;
import npstypes.v1.it.gov.mef.AllTipoRegistroAusiliarioType;
import npstypes.v1.it.gov.mef.DocRichiestaOperazioneDocumentaleType;
import npstypes.v1.it.gov.mef.DocTipoOperazioneType;
import npstypes.v1.it.gov.mef.DocValidazioneFirmaType;
import npstypes.v1.it.gov.mef.EmailIndirizzoEmailType;
import npstypes.v1.it.gov.mef.OrgAmministrazioneType;
import npstypes.v1.it.gov.mef.OrgAooType;
import npstypes.v1.it.gov.mef.OrgCasellaEmailType;
import npstypes.v1.it.gov.mef.OrgOrganigrammaType; 
import npstypes.v1.it.gov.mef.ProtAllaccioProtocolloType;
import npstypes.v1.it.gov.mef.ProtAnnullamentoType;
import npstypes.v1.it.gov.mef.ProtAssegnatarioType;
import npstypes.v1.it.gov.mef.ProtCriteriaProtocolliType;
import npstypes.v1.it.gov.mef.ProtDestinatarioType;
import npstypes.v1.it.gov.mef.ProtDocumentoMetadataProtocollatoType;
import npstypes.v1.it.gov.mef.ProtMetadatiType;
import npstypes.v1.it.gov.mef.ProtMittenteType;
import npstypes.v1.it.gov.mef.ProtRegistroProtocolloType;
import npstypes.v1.it.gov.mef.ProtTipoDettaglioEstrazioneType;
import npstypes.v1.it.gov.mef.ProtTipoProtocolloType;
import npstypes.v1.it.gov.mef.ProtTipologiaDocumentoType;
import npstypes.v1.it.gov.mef.RegAttributiEstesiType;
import npstypes.v1.it.gov.mef.RegCriteriaRegistrazioneAusiliariaType;
import npstypes.v1.it.gov.mef.RegIdentificatoreRegistrazioneAusiliariaType;
import npstypes.v1.it.gov.mef.RegRegistroType;
import npstypes.v1.it.gov.mef.RegTipologiaDocumentoType;
import npstypes.v1.it.gov.mef.WkfDestinatarioFlussoType;
import npstypesestesi.v1.it.gov.mef.TIPOLOGIA;

public class Director {

	private static npstypes.v1.it.gov.mef.ObjectFactory ofTypes;
	private static npsmessages.v1.it.gov.mef.ObjectFactory ofMessages;

	static {
		ofTypes = new npstypes.v1.it.gov.mef.ObjectFactory();
		ofMessages = new npsmessages.v1.it.gov.mef.ObjectFactory();
	}

	public static RichiestaCambiaStatoProtocolloEntrataType constructCambiaStatoProtocolloEntrataInput(OrgOrganigrammaType operatore, 
			String idProtocollo, String statoProtocollo, Date dataOperazione) throws DatatypeConfigurationException {
		RichiestaCambiaStatoProtocolloEntrataType richiestaType = ofMessages.createRichiestaCambiaStatoProtocolloEntrataType();

		richiestaType.setOperatore(operatore);

		IdentificativoProtocolloRequestType identificativo = ofMessages.createIdentificativoProtocolloRequestType();
		identificativo.setIdProtocollo(idProtocollo);

		richiestaType.setProtocollo(identificativo);

		richiestaType.setStato(statoProtocollo);
		
		richiestaType.setDataOperazione(Builder.buildXmlGregorianCalendarFromDate(dataOperazione));
				
		return richiestaType;
	}
	
	public static RichiestaCambiaStatoProtocolloUscitaType constructCambiaStatoProtocolloUscitaInput(OrgOrganigrammaType operatore, 
			String idProtocollo, String statoProtocollo, Date dataOperazione) throws DatatypeConfigurationException {
		RichiestaCambiaStatoProtocolloUscitaType richiestaType = ofMessages.createRichiestaCambiaStatoProtocolloUscitaType();

		richiestaType.setOperatore(operatore);

		IdentificativoProtocolloRequestType identificativo = ofMessages.createIdentificativoProtocolloRequestType();
		identificativo.setIdProtocollo(idProtocollo);

		richiestaType.setProtocollo(identificativo);

		richiestaType.setStato(statoProtocollo);
		
		richiestaType.setDataOperazione(Builder.buildXmlGregorianCalendarFromDate(dataOperazione));
		
		return richiestaType;
	}

	public static RichiestaReportProtocolloType constructReportProtocolloInput(String idProtocollo, String tipoReport) {
		RichiestaReportProtocolloType output = ofMessages.createRichiestaReportProtocolloType();
		output.setProtocollo(Builder.buildIdentificativoProtocollo(idProtocollo));
		output.setTipoReport(tipoReport);
		return output;
	}

	public static RichiestaAggiungiDocumentiProtocolloType constructAggiungiDocumentiProtocolloInput(Boolean isCompleto, String idProtocollo, 
			OrgOrganigrammaType operatore, RichiestaAggiungiDocumentiProtocolloType.Documenti[] documentiArray) {
		
		RichiestaAggiungiDocumentiProtocolloType outputType = ofMessages.createRichiestaAggiungiDocumentiProtocolloType();
		outputType.getDocumenti().addAll(Arrays.asList(documentiArray));
		outputType.setIsCompleto(isCompleto.booleanValue());
		outputType.setProtocollo(Builder.buildIdentificativoProtocollo(idProtocollo));
		outputType.setOperatore(operatore);
		return outputType;
	}

	public static RichiestaAggiungiAssegnatarioProtocolloEntrataType constructAggiungiAssegnatarioProtocolloEntrataInput(
			String idProtocollo, String acl, Date dataAssegnazione, String perCompetenza, OrgOrganigrammaType assegnante, 
			OrgOrganigrammaType assegnatario) throws DatatypeConfigurationException {
		RichiestaAggiungiAssegnatarioProtocolloEntrataType output = ofMessages.createRichiestaAggiungiAssegnatarioProtocolloEntrataType();
		
		if (acl != null) {
			output.setAcl(acl);
		}
		
		ProtAssegnatarioType ass = ofTypes.createProtAssegnatarioType();
		ass.setAssegnante(assegnante);
		ass.setAssegnatario(assegnatario);
		if (dataAssegnazione != null) {
			ass.setDataAssegnazione(Builder.buildXmlGregorianCalendarFromDate(dataAssegnazione));
		}
		ass.setPerCompetenza(perCompetenza);
		output.setDatiAssegnatario(ass);
		output.setOperatore(assegnante);
		output.setProtocollo(Builder.buildIdentificativoProtocollo(idProtocollo));
		return output;
	}

	public static RichiestaAnnullamentoProtocolloType constructAnnullamentoProtocolloInput(String idProtocollo,
			String motivoAnnullamento, Date dataAnnullamento, String provvAnnullamento,
			OrgOrganigrammaType annullatore, Date dataOperazione) throws DatatypeConfigurationException {
		RichiestaAnnullamentoProtocolloType output = ofMessages.createRichiestaAnnullamentoProtocolloType();
		ProtAnnullamentoType datiAnnullamento = ofTypes.createProtAnnullamentoType();
		datiAnnullamento.setAnnullatore(annullatore);
		datiAnnullamento.setDataAnnullamento(Builder.buildXmlGregorianCalendarFromDate(dataAnnullamento));
		datiAnnullamento.setMotivoAnnullamento(motivoAnnullamento);
		datiAnnullamento.setProvvAnnullamento(provvAnnullamento);
		output.setDatiAnnullamento(datiAnnullamento);
		output.setIdentificativoProtocollo(Builder.buildIdentificativoProtocollo(idProtocollo));
		output.setDataOperazione(Builder.buildXmlGregorianCalendarFromDate(dataOperazione));
		return output;
	}

	public static RichiestaRimuoviAllaccioProtocolloType constructRimuoviAllaccioProtocolloInput(
			OrgOrganigrammaType operatore, String idProtocolloAllacciante, String idProtocolloAllacciato,
			Date dataRegistrazione, String numeroRegistrazione, String codiceRegistro, String denominazioneRegistro) throws DatatypeConfigurationException {
		RichiestaRimuoviAllaccioProtocolloType output = ofMessages.createRichiestaRimuoviAllaccioProtocolloType();
		output.setOperatore(operatore);
		output.setIdentificativoProtocollo(Builder.buildIdentificativoProtocollo(idProtocolloAllacciante));

		ProtRegistroProtocolloType registro = Builder.buildRegistroProtocollo(operatore, codiceRegistro, denominazioneRegistro);

		IdentificativoProtocolloRequestType allaccio = ofMessages.createIdentificativoProtocolloRequestType();
		allaccio.setDataRegistrazione(Builder.buildXmlGregorianCalendarFromDate(dataRegistrazione));
		allaccio.setIdProtocollo(idProtocolloAllacciato);
		allaccio.setNumeroRegistrazione(Integer.parseInt(numeroRegistrazione));
		allaccio.setRegistro(registro);
		output.setAllaccio(allaccio);
		return output;
	}

	public static RichiestaRimuoviDestinatarioProtocolloType constructRimuoviDestinatarioProtocolloInput(
			String idOperatore, String cognome, String codiceAmministrazione, String denominazioneAmministrazione,
			String denominazioneAOO, String codiceAOO, String denominazioneUO, String codiceUO, String nome,
			String idProtocollo, String idDestinatario, String idUO, String codiceFiscale) {
		
		@SuppressWarnings("unused")
		IdentificativoDestinatarioType destinatario = Builder.buildDestinatario(idDestinatario);

		RichiestaRimuoviDestinatarioProtocolloType output = ofMessages.createRichiestaRimuoviDestinatarioProtocolloType();

		OrgOrganigrammaType operatore = Builder.buildOrganigrammaPf(idOperatore, cognome, nome, codiceFiscale, codiceAmministrazione,
				denominazioneAmministrazione, codiceAOO, denominazioneAOO, codiceUO, denominazioneUO, idUO);
		output.setOperatore(operatore);
		output.setProtocollo(Builder.buildIdentificativoProtocollo(idProtocollo));
		return output;
	}

	public static RichiestaAggiungiAllaccioProtocolloType constructAggiungiAllaccioProtocolloInput(
			OrgOrganigrammaType operatore, String idProtocollo,
			RichiestaAggiungiAllaccioProtocolloType.Allaccio[] allacci) {
		RichiestaAggiungiAllaccioProtocolloType allaccioType = null;
		if (allacci != null) {
			allaccioType  = ofMessages.createRichiestaAggiungiAllaccioProtocolloType();
			allaccioType.getAllaccio().addAll(Arrays.asList(allacci));
			allaccioType.setOperatore(operatore);
			allaccioType.setIdentificativoProtocollo(Builder.buildIdentificativoProtocollo(idProtocollo));
		}
		return allaccioType;
	}

	public static RichiestaGetProtocolloEntrataType constructGetProtocolloEntrataInput(String idProtocollo,
			ProtTipoDettaglioEstrazioneType[] dettagli) {
		RichiestaGetProtocolloEntrataType richiestaType = ofMessages.createRichiestaGetProtocolloEntrataType();
		richiestaType.setIdProtocollo(idProtocollo);
		richiestaType.getTipoDettaglioElenco().addAll(Arrays.asList(dettagli));
		return richiestaType;
	}

	public static RichiestaGetProtocolloEntrataType constructGetProtocolloEntrataInput(Date dataRegistrazione,
			String idProtocollo, int numeroRegistrazione, String codiceRegistro, String codiceAOO,
			String codiceAmministrazione, String denominazioneRegistro, String denominazioneAmministrazione,
			String denominazioneAOO, ProtTipoDettaglioEstrazioneType[] dettagli) throws DatatypeConfigurationException {
		RichiestaGetProtocolloEntrataType output = ofMessages.createRichiestaGetProtocolloEntrataType();
		ProtRegistroProtocolloType registro = Builder.buildRegistroProtocollo(codiceRegistro, denominazioneRegistro,
				denominazioneAmministrazione, codiceAmministrazione, codiceAOO, denominazioneAOO);
		output.setDataRegistrazione(Builder.buildXmlGregorianCalendarFromDate(dataRegistrazione));
		output.setNumeroRegistrazione(numeroRegistrazione);
		output.setRegistro(registro);
		output.setIdProtocollo(idProtocollo);
		output.getTipoDettaglioElenco().addAll(Arrays.asList(dettagli));
		return output;
	}

	public static RichiestaGetProtocolloUscita constructGetProtocolloUscitaInput(String idProtocollo, ProtTipoDettaglioEstrazioneType[] dettagli) {
		RichiestaGetProtocolloUscita output = ofMessages.createRichiestaGetProtocolloUscita();
		output.setIdProtocollo(idProtocollo);
		output.getTipoDettaglioElenco().addAll(Arrays.asList(dettagli));
		return output;
	}
	
	public static RichiestaGetProtocolloUscita constructGetProtocolloUscitaInput(Date dataRegistrazione,
			String idProtocollo, int numeroRegistrazione, String codiceRegistro, String codiceAOO,
			String codiceAmministrazione, String denominazioneRegistro, String denominazioneAmministrazione,
			String denominazioneAOO, ProtTipoDettaglioEstrazioneType[] dettagli) throws DatatypeConfigurationException {
		RichiestaGetProtocolloUscita output = ofMessages.createRichiestaGetProtocolloUscita();
		ProtRegistroProtocolloType registro = Builder.buildRegistroProtocollo(codiceRegistro, denominazioneRegistro,
				denominazioneAmministrazione, codiceAmministrazione, codiceAOO, denominazioneAOO);
		output.setDataRegistrazione(Builder.buildXmlGregorianCalendarFromDate(dataRegistrazione));
		output.setNumeroRegistrazione(numeroRegistrazione);
		output.setRegistro(registro);
		output.setIdProtocollo(idProtocollo);
		output.getTipoDettaglioElenco().addAll(Arrays.asList(dettagli));
		return output;
	}

	public static RichiestaRimuoviDocumentoProtocolloType constructRimuoviDocumentoProtocolloInput(
			String idOperatore, String cognome, String codiceAmministrazione, String denominazioneAmministrazione,
			String denominazioneAOO, String codiceAOO, String denominazioneUO, String codiceUO, String nome,
			String idProtocollo, String idDocumento, String idUO, String codiceFiscale) {
		RichiestaRimuoviDocumentoProtocolloType outputType = ofMessages.createRichiestaRimuoviDocumentoProtocolloType();
		IdentificativoDocumentoRequestType documentov = Builder.buildIdDocumento(idDocumento);
		outputType.setDocumento(documentov);
		OrgOrganigrammaType operatore = Builder.buildOrganigrammaPf(idOperatore, cognome, nome, codiceFiscale, codiceAmministrazione,
				denominazioneAmministrazione, codiceAOO, denominazioneAOO, codiceUO, denominazioneUO, idUO);
		outputType.setOperatore(operatore);
		outputType.setProtocollo(Builder.buildIdentificativoProtocollo(idProtocollo));
		return outputType;
	}

	public static RichiestaRicercaProtocolloType constructRicercaProtocolloInput(Short numeroRisultati, String acl,
			Short anno, Boolean isAnnullato, Boolean isCompleto, Boolean isRiservato, String oggetto,
			String sistemaProduttore, String stato, ProtTipoProtocolloType tipoProtocollo, String statoSpedizione,
			String descrizioneTipologiaDocumentale, String codiceTipologiaDocumentale, String descrizioneVoceTitolario,
			String codiceVoceTitolario, Integer numeroRegistrazioneA, Integer numeroRegistrazioneDa, String codiceAOO,
			String codiceRegistro, Date dataAnnullamentoA, Date dataAnnullamentoDa, Date dataRegistrazioneDa,
			String codiceAmministrazione, String idUtente, String denominazioneAmministrazione, String denominazioneUO,
			String nome, String denominazioneAOO, String cognome, String codiceUO, String denominazioneRegistro,
			Date dataRegistrazioneA, String codiceAmministrazioneAssegnante, String nomeAssegnante,
			String idOperatoreAssegnante, String cognomeAssegnante, String codiceAOOAssegnante,
			String denominazioneAOOAssegnante, String denominazioneAmministrazioneAssegnante, String codiceUOAssegnante,
			String denominazioneUOAssegnante, String cognomeAssegnatario, String idOperatoreAssegnatario,
			String nomeAssegnatario, String codiceAmministrazioneAssegnatario, String denominazioneAOOAssegnatario,
			String codiceUOAssegnatario, String codiceAOOAssegnatario, String denominazioneUOAssegnatario,
			String denominazioneAmministrazioneAssegnatario, String codiceMezzoRicezione,
			Integer numeroRegistrazioneMittenteA, Date dataRicezioneA, String descrizioneMezzoRicezione,
			Date dataRicezioneDa, Integer numeroRegistrazioneMittenteDa, Date dataProtocolloMittenteA,
			Date dataProtocolloMittenteDa, Date dataSpedizioneDa, Date dataSpedizioneA, String destinatarioSpedizione,
			String displayNameEmailSpedizione, String descrizioneMezzoSpedizioneCartaceo,
			String codiceMezzoSpedizioneCartaceo, String emailSpedizione, String mittente,
			ProtTipoDettaglioEstrazioneType[] tipoDettaglioElencoEnum, String idUO, String codiceFiscale) throws DatatypeConfigurationException {
		
		ProtTipologiaDocumentoType tipologiaDocumentale = ofTypes.createProtTipologiaDocumentoType();
		if (codiceTipologiaDocumentale != null && !codiceTipologiaDocumentale.equals("")) {
			tipologiaDocumentale.setCodice(codiceTipologiaDocumentale);
		}
		
		if (descrizioneTipologiaDocumentale != null && !descrizioneTipologiaDocumentale.equals("")) {
			JAXBElement<String> value = ofTypes.createProtTipologiaDocumentoTypeDescrizione(descrizioneTipologiaDocumentale);
			tipologiaDocumentale.setDescrizione(value);
		}
		
		AllNumeroRangeType numeroRegistrazione = ofTypes.createAllNumeroRangeType();
		if (numeroRegistrazioneDa != null && numeroRegistrazioneDa > 0) {
			numeroRegistrazione.setNumeroDa(numeroRegistrazioneDa);
		}
		if (numeroRegistrazioneA != null && numeroRegistrazioneA > 0) {
			numeroRegistrazione.setNumeroA(numeroRegistrazioneA);
		}

		@SuppressWarnings("unused")
		OrgOrganigrammaType protocollatore = Builder.buildOrganigrammaPf(idUtente, cognome, nome, codiceFiscale, codiceAmministrazione,
				denominazioneAmministrazione, codiceAOO, denominazioneAOO, codiceUO, denominazioneUO, idUO);

		ProtRegistroProtocolloType registro = Builder.buildRegistroProtocollo(codiceRegistro, denominazioneRegistro,
				denominazioneAmministrazione, codiceAmministrazione, codiceAOO, denominazioneAOO);

		AllDataRangeType dataRegistrazione = ofTypes.createAllDataRangeType();
		dataRegistrazione.setData(Builder.buildXmlGregorianCalendarFromDate(dataRegistrazioneDa));
		dataRegistrazione.setDataA(Builder.buildXmlGregorianCalendarFromDate(dataRegistrazioneA));
		
		ProtCriteriaProtocolliType.DatiSpedizione datSpedizione = ofTypes.createProtCriteriaProtocolliTypeDatiSpedizione();
		if ((destinatarioSpedizione != null) && (!destinatarioSpedizione.equals(""))) {
			datSpedizione.setDestinatario(destinatarioSpedizione);
		}
		ProtCriteriaProtocolliType.DatiRicezione datRicezione = ofTypes.createProtCriteriaProtocolliTypeDatiRicezione();
		if ((mittente != null) && (!mittente.equals(""))) {
			datRicezione.setMittente(mittente);
		}
		RichiestaRicercaProtocolloType.DatiRicerca datiRicerca = ofMessages.createRichiestaRicercaProtocolloTypeDatiRicerca();
		if (acl != null) {
			datiRicerca.setAcl(acl);
		}
		if (tipologiaDocumentale!=null && (tipologiaDocumentale.getCodice() != null || tipologiaDocumentale.getDescrizione() != null)) {
			JAXBElement<ProtTipologiaDocumentoType> value = ofTypes.createProtCriteriaProtocolliTypeTipologiaDocumentale(tipologiaDocumentale);
			datiRicerca.setTipologiaDocumentale(value);
		}
		if (datSpedizione.getDestinatario() != null) {
			datiRicerca.setDatiSpedizione(datSpedizione);
		}
		if (datRicezione.getMittente() != null) {
			datiRicerca.setDatiRicezione(datRicezione);
		}
		if ((anno != null) && (anno.shortValue() != 0)) {
			datiRicerca.setAnno(anno.shortValue());
		}
		if ((dataRegistrazione.getData() != null) || (dataRegistrazione.getDataA() != null)) {
			datiRicerca.setDataRegistrazione(dataRegistrazione);
		}
		if (numeroRegistrazione.getNumeroA() != null && ((numeroRegistrazione.getNumeroA() > 0) || (numeroRegistrazione.getNumeroDa() > 0))) {
			datiRicerca.setNumeroRegistrazione(numeroRegistrazione);
		}
		if ((oggetto != null) && (!oggetto.equals(""))) {
			datiRicerca.setOggetto(oggetto);
		}
		datiRicerca.setRegistro(registro);
		if ((stato != null) && (!stato.equals(""))) {
			datiRicerca.setStato(stato);
		}
		if (tipoProtocollo != null) {
			datiRicerca.setTipoProtocollo(tipoProtocollo);
		}
		datiRicerca.setIsAnnullato(isAnnullato);
		datiRicerca.getTipoDettaglioElenco().addAll(Arrays.asList(tipoDettaglioElencoEnum));
		
		RichiestaRicercaProtocolloType output = ofMessages.createRichiestaRicercaProtocolloType();
		output.setDatiRicerca(datiRicerca);
		output.setNumeroRisultati(numeroRisultati.shortValue());
		return output;
	}

	public static RichiestaCreateProtocolloEntrataType constructCreateProtocolloEntrataPFInput(
			Date dataRegistrazione, Integer numeroRegistrazione, String codiceRegistro,
			String denominazioneAmministrazione, String denominazioneRegistro, String codiceAmministrazione,
			String denominazioneAOO, String codiceAOO, ProtTipoProtocolloType tipoProtocollo,
			ProtAllaccioProtocolloType[] cAllacciati, ProtAssegnatarioType[] cAssegnatari,
			ProtDestinatarioType[] cDestinatari, String acl, OrgOrganigrammaType protocollatoreOrg, Boolean isRiservato,
			String oggetto, String stato, String idProtocollo, String codiceVoceTitolario, String descrizioneVoceTitolario, String descrizioneTipologia,
			String codiceTipologia, ProtMittenteType mittente, ProtMetadatiType metadatiAssociati, String idDocumentoMessaggioPostaNps) throws DatatypeConfigurationException {
		return constructCreateProtocolloEntrataInput(dataRegistrazione, numeroRegistrazione, codiceRegistro,
				denominazioneAmministrazione, denominazioneRegistro, codiceAmministrazione, denominazioneAOO, codiceAOO,
				tipoProtocollo, cAllacciati, cAssegnatari, cDestinatari, acl, protocollatoreOrg, isRiservato, oggetto,
				stato, idProtocollo, codiceVoceTitolario, descrizioneVoceTitolario, descrizioneTipologia,
				codiceTipologia, mittente, metadatiAssociati, idDocumentoMessaggioPostaNps);
	}

	private static RichiestaCreateProtocolloEntrataType constructCreateProtocolloEntrataInput(
			Date dataRegistrazione, Integer numeroRegistrazione, String codiceRegistro,
			String denominazioneAmministrazione, String denominazioneRegistro, String codiceAmministrazione,
			String denominazioneAOO, String codiceAOO, ProtTipoProtocolloType tipoProtocollo,
			ProtAllaccioProtocolloType[] cAllacciati, ProtAssegnatarioType[] cAssegnatari,
			ProtDestinatarioType[] cDestinatari, String acl, OrgOrganigrammaType protocollatoreOrg, Boolean isRiservato,
			String oggetto, String stato, String idProtocollo, String codiceVoceTitolario, String descrizioneVoceTitolario, 
			String descrizioneTipologia, String codiceTipologia, ProtMittenteType mittente, ProtMetadatiType metadatiAssociati,
			String idDocumentoMessaggioPostaNps) throws DatatypeConfigurationException {
		RichiestaCreateProtocolloEntrataType output = ofMessages.createRichiestaCreateProtocolloEntrataType();
		
		output.setAcl(acl);
		if ((cAllacciati != null) && (cAllacciati.length > 0)) {
			output.getAllacciati().addAll(Arrays.asList(cAllacciati));
		}
		if ((cAssegnatari != null) && (cAssegnatari.length > 0)) {
			output.getAssegnatari().addAll(Arrays.asList(cAssegnatari));
		}
		if ((cDestinatari != null) && (cDestinatari.length > 0)) {
			output.getDestinatari().addAll(Arrays.asList(cDestinatari));
		}
		output.setProtocollatore(protocollatoreOrg);
		output.setIdentificatoreProtocollo(Builder.buildIdentificatoreProtocollo(dataRegistrazione, numeroRegistrazione,
				codiceRegistro, denominazioneAmministrazione, denominazioneRegistro, codiceAmministrazione,
				denominazioneAOO, codiceAOO, tipoProtocollo));
		output.setIdProtocollo(null);
		output.setIsRiservato(isRiservato.booleanValue());
		output.setMittente(mittente);
		output.setOggetto(oggetto);
		output.setStato(stato);
		output.setTipologiaDocumento(
				Builder.buildTipologiaDocumento(descrizioneTipologia, codiceTipologia, metadatiAssociati));

		AllCodiceDescrizioneType allCodiceDescrizioneType = ofTypes.createAllCodiceDescrizioneType();
		allCodiceDescrizioneType.setCodice(codiceVoceTitolario);
		allCodiceDescrizioneType.setDescrizione(descrizioneVoceTitolario);
		output.setVoceTitolario(allCodiceDescrizioneType);
		output.setIsCompleto(false);
		
		// Interoperabilità
		if (idDocumentoMessaggioPostaNps != null && !"".equals(idDocumentoMessaggioPostaNps)) {
			output.setIdMessaggioPostaInteroperabile(idDocumentoMessaggioPostaNps);
		}
		
		return output;
	}

	public static RichiestaCreateProtocolloUscitaType constructCreateProtocolloUscitaPFInput(Date dataRegistrazione,
			Integer numeroRegistrazione, String codiceRegistro, String denominazioneAmministrazione,
			String denominazioneRegistro, String codiceAmministrazione, String denominazioneAOO, String codiceAOO,
			ProtTipoProtocolloType tipoProtocollo, ProtAllaccioProtocolloType[] cAllacciati,
			ProtDestinatarioType[] cDestinatari, String idProtocollo, Boolean isRiservato,
			String stato, String oggetto, String acl,
			OrgOrganigrammaType protocollatoreOrg, String codiceVoceTitolario, String descrizioneVoceTitolario,
			String descrizioneTipologia, String codiceTipologia, boolean risposta, String oggettoRisposta,
			String dataRegistrazioneRisposta, ProtTipoProtocolloType tipoProtocolloRisposta,
			String idProtocolloRisposta, String numeroRegistrazioneRisposta, ProtMittenteType mittente,
			ProtMetadatiType metadatiAssociati) throws DatatypeConfigurationException {
		return constructCreateProtocolloUscitaInput(dataRegistrazione, numeroRegistrazione, codiceRegistro,
				denominazioneAmministrazione, denominazioneRegistro, codiceAmministrazione, denominazioneAOO, codiceAOO,
				tipoProtocollo, cAllacciati, cDestinatari, idProtocollo, isRiservato, stato, oggetto, acl,
				protocollatoreOrg, codiceVoceTitolario, descrizioneVoceTitolario, descrizioneTipologia, codiceTipologia,
				risposta, oggettoRisposta, dataRegistrazioneRisposta, tipoProtocolloRisposta, idProtocolloRisposta,
				numeroRegistrazioneRisposta, mittente, metadatiAssociati);
	}

	public static RichiestaCreateProtocolloUscitaType constructCreateProtocolloUscitaPGInput(Date dataRegistrazione,
			Integer numeroRegistrazione, String codiceRegistro, String denominazioneAmministrazione,
			String denominazioneRegistro, String codiceAmministrazione, String denominazioneAOO, String codiceAOO,
			ProtTipoProtocolloType tipoProtocollo, ProtAllaccioProtocolloType[] cAllacciati,
			ProtDestinatarioType[] cDestinatari, String idProtocollo, Boolean isRiservato,
			String stato, String oggetto, String acl,
			OrgOrganigrammaType protocollatoreOrg, String codiceVoceTitolario, String descrizioneVoceTitolario,
			String descrizioneTipologia, String codiceTipologia, boolean risposta, String oggettoRisposta,
			String dataRegistrazioneRisposta, ProtTipoProtocolloType tipoProtocolloRisposta,
			String idProtocolloRisposta, String numeroRegistrazioneRisposta, String value, String note,
			String denominazionePG, String tipo, String mezzoRicezioneMittente,
			String descrizioneMezzoRicezioneMittente, ProtMetadatiType metadatiAssociati) throws DatatypeConfigurationException {
		
		ProtMittenteType mittente = Builder.buildMittentePG(mezzoRicezioneMittente, descrizioneMezzoRicezioneMittente,
				tipo, value, denominazionePG);

		return constructCreateProtocolloUscitaInput(dataRegistrazione, numeroRegistrazione, codiceRegistro,
				denominazioneAmministrazione, denominazioneRegistro, codiceAmministrazione, denominazioneAOO, codiceAOO,
				tipoProtocollo, cAllacciati, cDestinatari, idProtocollo, isRiservato, stato, oggetto, acl,
				protocollatoreOrg, codiceVoceTitolario, descrizioneVoceTitolario, descrizioneTipologia, codiceTipologia,
				risposta, oggettoRisposta, dataRegistrazioneRisposta, tipoProtocolloRisposta, idProtocolloRisposta,
				numeroRegistrazioneRisposta, mittente, metadatiAssociati);
	}

	private static RichiestaCreateProtocolloUscitaType constructCreateProtocolloUscitaInput(Date dataRegistrazione,
			Integer numeroRegistrazione, String codiceRegistro, String denominazioneAmministrazione,
			String denominazioneRegistro, String codiceAmministrazione, String denominazioneAOO, String codiceAOO,
			ProtTipoProtocolloType tipoProtocollo, ProtAllaccioProtocolloType[] cAllacciati,
			ProtDestinatarioType[] cDestinatari, String idProtocollo, Boolean isRiservato,
			String stato, String oggetto, String acl,
			OrgOrganigrammaType protocollatoreOrg, String codiceVoceTitolario, String descrizioneVoceTitolario,
			String descrizioneTipologia, String codiceTipologia, boolean risposta, String oggettoRisposta,
			String dataRegistrazioneRisposta, ProtTipoProtocolloType tipoProtocolloRisposta,
			String idProtocolloRisposta, String numeroRegistrazioneRisposta, ProtMittenteType mittente,
			ProtMetadatiType metadatiAssociati) throws DatatypeConfigurationException {
		
		RichiestaCreateProtocolloUscitaType output = ofMessages.createRichiestaCreateProtocolloUscitaType();
		output.setAcl(acl);
		if (cAllacciati != null) {
			output.getAllacciati().addAll(Arrays.asList(cAllacciati));
		}
		if (cDestinatari != null) {
			output.getDestinatari().addAll(Arrays.asList(cDestinatari));
		}
		output.setIdentificatoreProtocollo(Builder.buildIdentificatoreProtocollo(dataRegistrazione, numeroRegistrazione,
				codiceRegistro, denominazioneAmministrazione, denominazioneRegistro, codiceAmministrazione,
				denominazioneAOO, codiceAOO, tipoProtocollo));
		output.setIdProtocollo(idProtocollo);
		output.setIsRiservato(isRiservato.booleanValue());
		output.setMittente(mittente);
		output.setOggetto(oggetto);
		output.setProtocollatore(protocollatoreOrg);
		if ((numeroRegistrazioneRisposta != null) && (dataRegistrazioneRisposta != null)) {
			output.setRisposta(Builder.buildRisposta(tipoProtocolloRisposta, risposta, oggettoRisposta,
					numeroRegistrazioneRisposta, idProtocolloRisposta, dataRegistrazioneRisposta));
		}
		output.setStato(stato);
		output.setTipologiaDocumento(
				Builder.buildTipologiaDocumento(descrizioneTipologia, codiceTipologia, metadatiAssociati));
		if ((codiceVoceTitolario != null) && (descrizioneVoceTitolario != null)) {
			AllCodiceDescrizioneType allCodiceDescrizioneType = ofTypes.createAllCodiceDescrizioneType();
			allCodiceDescrizioneType.setCodice(codiceVoceTitolario);
			allCodiceDescrizioneType.setDescrizione(descrizioneVoceTitolario);
			output.setVoceTitolario(allCodiceDescrizioneType);
		}
		return output;
	}

	public static RichiestaUpdateDatiProtocolloType constructUpdateDatiProtocolloInput(
			OrgOrganigrammaType operatore, String idProtocollo, String oggetto, String descrizioneTipologia, String indiceClassificazione,
			boolean isCompleto, ProtMetadatiType metadatiAssociati, String acl, Boolean isRiservato, Date dataOperazione) throws DatatypeConfigurationException {
		
		RichiestaUpdateDatiProtocolloType outputType = ofMessages.createRichiestaUpdateDatiProtocolloType();

		RichiestaUpdateDatiProtocolloType.DatiModifica datiModifica = ofMessages.createRichiestaUpdateDatiProtocolloTypeDatiModifica();
		if (oggetto != null) {
			datiModifica.setOggetto(oggetto);
		}
		if (descrizioneTipologia != null) {
			datiModifica.setTipologiaDocumento(
					Builder.buildTipologiaDocumento(descrizioneTipologia, descrizioneTipologia, metadatiAssociati));
		}
		if (indiceClassificazione != null) {
			String[] parts = indiceClassificazione.split("-");
			String codIndiceClass = parts[0];
			String indiceClassificazioneDesc = indiceClassificazione;
			AllCodiceDescrizioneType allCodiceDescrizioneType = ofTypes.createAllCodiceDescrizioneType();
			allCodiceDescrizioneType.setCodice(codIndiceClass);
			allCodiceDescrizioneType.setDescrizione(indiceClassificazioneDesc);
			datiModifica.setVoceTitolario(allCodiceDescrizioneType);
		}
		
		if (isCompleto) {
			datiModifica.setIsCompleto(true);
		}
		
		if (isRiservato != null) {
			datiModifica.setIsRiservato(isRiservato);
		}
		
		if (acl != null && !(acl.equals(""))) {
			datiModifica.setAcl(acl);
		}
		outputType.setDatiModifica(datiModifica);
		outputType.setIdentificativoProtocollo(Builder.buildIdentificativoProtocollo(idProtocollo));
		outputType.setOperatore(operatore);
		outputType.setDataOperazione(Builder.buildXmlGregorianCalendarFromDate(dataOperazione));
		
		return outputType;
	}

	public static RichiestaModificaSpedizioneDestinatarioProtocolloType constructModificaDestinatarioProtocolloInput(
			OrgOrganigrammaType operatore, String idProtocollo, String statoSpedizione,	Date dataSpedizione, String idDestinatario, 
			String newEmailDestinatario) throws DatatypeConfigurationException {
		
		RichiestaModificaSpedizioneDestinatarioProtocolloType output = ofMessages.createRichiestaModificaSpedizioneDestinatarioProtocolloType();
		RichiestaModificaSpedizioneDestinatarioProtocolloType.DatiDestinatario datiDestinatario = ofMessages.createRichiestaModificaSpedizioneDestinatarioProtocolloTypeDatiDestinatario();
		datiDestinatario.setIndirizzoTelematico(Builder.buildIndirizzoTelematico(null, newEmailDestinatario));
		datiDestinatario.setDataSpedizione(Builder.buildXmlGregorianCalendarFromDate(dataSpedizione));
		datiDestinatario.setDestinatario(Builder.buildDestinatario(idDestinatario));
		datiDestinatario.setStatoSpedizione(statoSpedizione);
		output.getDatiDestinatario().add(datiDestinatario);

		output.setOperatore(operatore);
		output.setProtocollo(Builder.buildIdentificativoProtocollo(idProtocollo));
		return output;
	
	}

	public static RichiestaAggiungiDestinatarioProtocolloType constructAggiungiDestinatarioProtocolloInput(
			OrgOrganigrammaType operatore, String idProtocollo, ProtDestinatarioType[] destinatari) {
		
		RichiestaAggiungiDestinatarioProtocolloType output = ofMessages.createRichiestaAggiungiDestinatarioProtocolloType();
		
		if ((destinatari != null) && (destinatari.length > 0)) {
			output.getDestinatari().addAll(Arrays.asList(destinatari));

			output.setOperatore(operatore);
			output.setProtocollo(Builder.buildIdentificativoProtocollo(idProtocollo));
		} else {
			throw new BusinessDelegateRuntimeException("Fornire almeno un destinatario!");
		}
		return output;
	}

	public static RichiestaUploadDocumentoType constructUploadDocumentoInput(boolean attivo, String chiaveEsterna,
			boolean condivisibile, InputStream inputStream, String descrizione, String fileName, Boolean firmato,
			byte[] hash, String idDocumento, Long length, String MIMEType, byte[] previewPng, String tipoCompressione,
			DocValidazioneFirmaType validazioneFirma, Date dataAnnullamento, Date dataChiusura, Date dataDocumento,
			String note, DocTipoOperazioneType tipoOperazione, String idDocumentoOperazione,
			HashMap<String, String> parametriOperazione) throws IOException, DatatypeConfigurationException {
		
		RichiestaUploadDocumentoType output = ofMessages.createRichiestaUploadDocumentoType();
		output.setAttivo(attivo);

		AllChiaveEsternaType chiaveEsternaType = ofTypes.createAllChiaveEsternaType();
		chiaveEsternaType.setValue(chiaveEsterna);

		output.setChiaveEsterna(chiaveEsternaType);
		output.setCondivisibile(condivisibile);
		output.setContent(IOUtils.toByteArray(inputStream));
		output.setDescrizione(descrizione);
		output.setFileName(fileName);
		output.setFirmato(firmato.booleanValue());
		output.setHash(hash);
		output.setIdDocumento(idDocumento);
		output.setMIMEType(MIMEType);

		ProtDocumentoMetadataProtocollatoType datiDocumento = ofTypes.createProtDocumentoMetadataProtocollatoType();
		
		datiDocumento.setDataAnnullamento(Builder.buildXmlGregorianCalendarFromDate(dataAnnullamento));
		datiDocumento.setDataChiusura(Builder.buildXmlGregorianCalendarFromDate(dataChiusura));
		datiDocumento.setDataDocumento(Builder.buildXmlGregorianCalendarFromDate(dataDocumento));
		
		datiDocumento.setNote(note);

		DocRichiestaOperazioneDocumentaleType operazione = ofTypes.createDocRichiestaOperazioneDocumentaleType();
		operazione.setIdDocumentoOperazione(idDocumentoOperazione);
		if (parametriOperazione != null) {
			operazione.getParametriOperazione();
		}
		
		return output;
	}

	public static RichiestaReportRegistroGiornalieroType constructReportRegistroGiornalieroInput(Date dataDa,
			Date dataA, OrgOrganigrammaType operatore, String codiceRegistro, String denominazioneRegistro) throws DatatypeConfigurationException {
		
		RichiestaReportRegistroGiornalieroType registroType = ofMessages.createRichiestaReportRegistroGiornalieroType();
		AllDataRangeType dataRange = ofTypes.createAllDataRangeType();
		dataRange.setData(Builder.buildXmlGregorianCalendarFromDate(dataDa));
		dataRange.setDataA(Builder.buildXmlGregorianCalendarFromDate(dataA));
		
		if (dataRange.getData() != null) {
			registroType.setDataRegistrazione(dataRange);
		}
		
		registroType.setRegistro(Builder.buildRegistroProtocollo(operatore, codiceRegistro, denominazioneRegistro));
		
		return registroType;
	}

	public static RichiestaSpedisciProtocolloUscitaType constructSpedisciProtocolloUscita(
			OrgOrganigrammaType operatoreOrg, String idProtocollo, String dataSpedizione, 
			String oggettoMessaggio, String corpoMessaggio) throws DatatypeConfigurationException {
		
		RichiestaSpedisciProtocolloUscitaType richiestaType = ofMessages.createRichiestaSpedisciProtocolloUscitaType();
		richiestaType.setDataSpedizione(Builder.buildXmlGregorianCalendarFromStringDate(dataSpedizione));
		richiestaType.setProtocollo(Builder.buildIdentificativoProtocollo(idProtocollo));
		richiestaType.setOperatore(operatoreOrg);
		
		if(oggettoMessaggio != null) {
			richiestaType.setMessaggio(oggettoMessaggio);
		}
		
		if(corpoMessaggio != null) {
			richiestaType.setCorpoMessaggio(corpoMessaggio);
		}
		
		return richiestaType;
	
	}
	
	public static RichiestaReportProtocolloType constructRichiestaReportProtocolloInput(String idProtocollo, String tipoReport) {
		RichiestaReportProtocolloType richiestaType = ofMessages.createRichiestaReportProtocolloType();
		
		if (idProtocollo != null && !idProtocollo.equals("")) {
			IdentificativoProtocolloRequestType idProtocolloRequest = ofMessages.createIdentificativoProtocolloRequestType();
			idProtocolloRequest.setIdProtocollo(idProtocollo);
			richiestaType.setProtocollo(idProtocolloRequest);
		}
		
		if (tipoReport != null && !tipoReport.equals("")) {
			richiestaType.setTipoReport(tipoReport);
		}
		
		return richiestaType;
	}
	
	public static RichiestaRicercaRegistrazioneAusiliariaType constructRichiestaRicercaRegistrazioneAusiliaria(short anno, String codiceRegistro, String descrizioneRegistro,
			AllTipoRegistroAusiliarioType tipoRegistro, Integer numeroRegistrazioneDa, Integer numeroRegistrazioneA, Date dataRegistrazioneDa, Date dataRegistrazioneA, 
			Date dataAnnullamentoDa, Date dataAnnullamentoA, String codiceTipologiaDocumento, boolean annullato, String acl, String applicazione, String idUtente,
			String nome, String cognome, String codiceAmministrazione, String denominazioneAmministrazione, String denominazioneUO, String codiceUO, String denominazioneAOO, 
			String codiceAOO, short numeroRisultati, String idUO, String oggetto) throws DatatypeConfigurationException {
		RichiestaRicercaRegistrazioneAusiliariaType richiestaType = ofMessages.createRichiestaRicercaRegistrazioneAusiliariaType();
		
		// Criteri di ricerca -> START
		RegCriteriaRegistrazioneAusiliariaType criteriaRicercaType = ofTypes.createRegCriteriaRegistrazioneAusiliariaType();
		
		RegRegistroType registro = Builder.buildRegRegistroType(codiceAmministrazione, denominazioneAmministrazione, codiceAOO, denominazioneAOO,
				codiceRegistro, descrizioneRegistro, tipoRegistro);
		criteriaRicercaType.setRegistro(registro);
		
		criteriaRicercaType.setAnno(anno);
		
		if(oggetto != null && !oggetto.equals("")) {
			criteriaRicercaType.setOggetto(oggetto);
		}
		
		// AOO
		OrgAmministrazioneType amministrazione = ofTypes.createOrgAmministrazioneType();
		amministrazione.setCodiceAmministrazione(codiceAmministrazione);
		amministrazione.setDenominazione(denominazioneAmministrazione);
		
		RegTipologiaDocumentoType tipologiaDocumentale = Builder.buildRegTipologiaDocumento(codiceTipologiaDocumento, codiceTipologiaDocumento);
		criteriaRicercaType.setTipologiaDocumentale(tipologiaDocumentale);
		
		if ((numeroRegistrazioneDa != null && numeroRegistrazioneDa > 0) ||
				(numeroRegistrazioneA != null && numeroRegistrazioneA > 0)) {
			AllNumeroRangeType numeroRegistrazione = ofTypes.createAllNumeroRangeType();
			if (numeroRegistrazioneDa != null && numeroRegistrazioneDa > 0) {
				numeroRegistrazione.setNumeroDa(numeroRegistrazioneDa);
			}
			if (numeroRegistrazioneA != null && numeroRegistrazioneA > 0) {
				numeroRegistrazione.setNumeroA(numeroRegistrazioneA);
			}
			criteriaRicercaType.setNumeroRegistrazione(numeroRegistrazione);
		}

		if(dataRegistrazioneDa != null || dataRegistrazioneA != null) {
			AllDataRangeType dataRegistrazione = ofTypes.createAllDataRangeType();
			dataRegistrazione.setData(Builder.buildXmlGregorianCalendarFromDate(dataRegistrazioneDa));
			dataRegistrazione.setDataA(Builder.buildXmlGregorianCalendarFromDate(dataRegistrazioneA));
			criteriaRicercaType.setDataRegistrazione(dataRegistrazione);
		}
		
		if(dataAnnullamentoDa != null || dataAnnullamentoA != null) {
			AllDataRangeType dataAnnullamento = ofTypes.createAllDataRangeType();
			dataAnnullamento.setData(Builder.buildXmlGregorianCalendarFromDate(dataAnnullamentoDa));
			dataAnnullamento.setDataA(Builder.buildXmlGregorianCalendarFromDate(dataAnnullamentoA));
			criteriaRicercaType.setDataAnnullamento(dataAnnullamento);
		}
		
		criteriaRicercaType.setIsAnnullato(annullato);
		
		criteriaRicercaType.setApplicazione(applicazione);
		
		criteriaRicercaType.setAcl(acl);
		// Criteri di ricerca -> END

		richiestaType.setDatiRicerca(criteriaRicercaType);
		richiestaType.setNumeroRisultati(numeroRisultati);
		
		return richiestaType;
	}

	public static RichiestaCreateRegistrazioneAusiliariaType constructRichiestaCreateRegistrazioneAusiliariaType(String acl, String oggetto,
			String idUtente, String nome, String cognome, String codiceAmministrazione, String denominazioneAmministrazione, String denominazioneUO, 
			String codiceUO, String denominazioneAOO, String codiceAOO, String codiceRegistro, String descrizioneRegistro, AllTipoRegistroAusiliarioType tipoRegistro,
			String codiceTipologiaDocumento, String descrizioneTipologiaDocumento, String idProtocolloIngresso, String idUO, String codiceFiscale) throws DatatypeConfigurationException {
		RichiestaCreateRegistrazioneAusiliariaType richiestaType = ofMessages.createRichiestaCreateRegistrazioneAusiliariaType();
		
		richiestaType.setAcl(acl);
		richiestaType.setOggetto(oggetto);
		
		// Utente che effettua l'operazione
		OrgOrganigrammaType operatore = Builder.buildOrganigrammaPf(idUtente, cognome, nome, codiceFiscale, codiceAmministrazione,
				denominazioneAmministrazione, codiceAOO, denominazioneAOO, codiceUO, denominazioneUO, idUO);
		richiestaType.setOperatore(operatore);
		
		RegRegistroType registro = Builder.buildRegRegistroType(codiceAmministrazione, denominazioneAmministrazione, codiceAOO, denominazioneAOO,
				codiceRegistro, descrizioneRegistro, tipoRegistro);
		richiestaType.setRegistro(registro);
		
		RegAttributiEstesiType tipoDocumento = ofTypes.createRegAttributiEstesiType();
		tipoDocumento.setTipologia(Builder.buildRegTipologiaDocumento(codiceTipologiaDocumento, descrizioneTipologiaDocumento));
		richiestaType.setTipoDocumento(tipoDocumento );
		
		
		npstypes.v1.it.gov.mef.IdentificativoProtocolloRequestType protocolloIngresso = ofTypes.createIdentificativoProtocolloRequestType();
		protocolloIngresso.setIdProtocollo(idProtocolloIngresso);
		richiestaType.setProtocolloAssociato(protocolloIngresso );
		
		return richiestaType;
	}
	
	
	public static RichiestaCollegaRegistrazioneAusiliariaProtocolloIngressoType constructRichiestaCollegaRegistrazioneAusiliariaProtocolloIngressoType(String idUtente, String nome, String cognome, String codiceAmministrazione, String denominazioneAmministrazione, String denominazioneUO, 
			String codiceUO, String denominazioneAOO, String codiceAOO, String idRegistrazioneAusiliaria, String idProtocolloIngresso, String idUO, String codiceFiscale) throws DatatypeConfigurationException {
		RichiestaCollegaRegistrazioneAusiliariaProtocolloIngressoType richiestaType = ofMessages.createRichiestaCollegaRegistrazioneAusiliariaProtocolloIngressoType();
		
		RegIdentificatoreRegistrazioneAusiliariaType idRegAux = ofTypes.createRegIdentificatoreRegistrazioneAusiliariaType();
		idRegAux.setIdRegistrazioneAusiliaria(idRegistrazioneAusiliaria);
		richiestaType.setIdentificativoRegistrazioneAusiliaria(idRegAux);
		
		// Utente che effettua l'operazione
		OrgOrganigrammaType operatore = Builder.buildOrganigrammaPf(idUtente, cognome, nome, codiceFiscale, codiceAmministrazione,
				denominazioneAmministrazione, codiceAOO, denominazioneAOO, codiceUO, denominazioneUO, idUO);
		richiestaType.setOperatore(operatore);
		
		npstypes.v1.it.gov.mef.IdentificativoProtocolloRequestType protocolloIngresso = ofTypes.createIdentificativoProtocolloRequestType();
		protocolloIngresso.setIdProtocollo(idProtocolloIngresso);
		richiestaType.setProtocolloIngresso(protocolloIngresso);
		
		return richiestaType;
	}
	
	public static RichiestaCollegaRegistrazioneAusiliariaProtocolloUscitaType constructRichiestaCollegaRegistrazioneAusiliariaProtocolloUscitaType(String idUtente, String nome, String cognome, String codiceAmministrazione, String denominazioneAmministrazione, String denominazioneUO, 
			String codiceUO, String denominazioneAOO, String codiceAOO, String idRegistrazioneAusiliaria, String idProtocolloUscita, String idUO, String codiceFiscale) throws DatatypeConfigurationException {
		RichiestaCollegaRegistrazioneAusiliariaProtocolloUscitaType richiestaType = ofMessages.createRichiestaCollegaRegistrazioneAusiliariaProtocolloUscitaType();
		
		RegIdentificatoreRegistrazioneAusiliariaType idRegAux = ofTypes.createRegIdentificatoreRegistrazioneAusiliariaType();
		idRegAux.setIdRegistrazioneAusiliaria(idRegistrazioneAusiliaria);
		richiestaType.setIdentificativoRegistrazioneAusiliaria(idRegAux);
		
		// Utente che effettua l'operazione
		OrgOrganigrammaType operatore = Builder.buildOrganigrammaPf(idUtente, cognome, nome, codiceFiscale, codiceAmministrazione,
				denominazioneAmministrazione, codiceAOO, denominazioneAOO, codiceUO, denominazioneUO, idUO);
		richiestaType.setOperatore(operatore);
		
		npstypes.v1.it.gov.mef.IdentificativoProtocolloRequestType protocolloIngresso = ofTypes.createIdentificativoProtocolloRequestType();
		protocolloIngresso.setIdProtocollo(idProtocolloUscita);
		richiestaType.setProtocolloUscita(protocolloIngresso);
		
		return richiestaType;
	}

	public static RichiestaInviaAttoType constructRichiestaInviaAttoType(String acl, WkfDestinatarioFlussoType[] cDestinatari,
			String codiceFlusso, String identificatoreProcesso, String identificativoMessaggio, String idUtente, String nome, String cognome, String codiceAmministrazione, 
			String denominazioneAmministrazione, String denominazioneUO, String codiceUO, String denominazioneAOO, String codiceAOO, String oggetto, TIPOLOGIA metadatiEstesi,
			String guidDocPrincipale, String[] guidAllegati, String codiceVoceTitolario, String descrizioneVoceTitolario, String descrizioneTipologia, String codiceTipologia, 
			ProtMetadatiType metadatiAssociati, ProtAllaccioProtocolloType[] cAllacciati, String idUO, String messageId, String codiceFiscale,
			String oggettoMessaggio, String corpoMessaggio) {
		
		RichiestaInviaAttoType output = ofMessages.createRichiestaInviaAttoType();
		
		output.setAcl(acl);
		
		if (cDestinatari != null) {
			output.getDestinatari().addAll(Arrays.asList(cDestinatari));
		}
		
		output.setCodiceFlusso(codiceFlusso);
		output.setIdentificatoreProcesso(identificatoreProcesso);
		
		output.setChiaveDocumento(guidDocPrincipale);
		if(guidAllegati != null) {
			output.getChiaveAllegati().addAll(Arrays.asList(guidAllegati));
		}
		
		output.setMittente(Builder.buildOrganigrammaPf(idUtente, cognome, nome, codiceFiscale, codiceAmministrazione, denominazioneAmministrazione, codiceAOO, 
				denominazioneAOO, codiceUO, denominazioneUO, idUO));
		
		output.setIdMessaggio(messageId);
		output.setOggetto(oggetto);
		
		output.getContestoProcedurale().add(Builder.buildContestoProcedurale(identificativoMessaggio, codiceFlusso, 
			codiceAmministrazione, codiceAOO, metadatiEstesi));
		
		output.setVoceTitolario(Builder.buildCodiceDescrizione(codiceVoceTitolario, descrizioneVoceTitolario));
		
		output.setTipologiaDocumento(Builder.buildTipologiaDocumento(descrizioneTipologia, codiceTipologia, metadatiAssociati));
		
		if (cAllacciati != null) {
			output.getAllacciati().addAll(Arrays.asList(cAllacciati));
		}
		
		
		if (oggettoMessaggio != null) {
			output.setMessaggio(oggettoMessaggio);
		}
		
		if (corpoMessaggio != null) {
			output.setCorpoMessaggio(corpoMessaggio);
		}
		
		return output;
	}
	 
	public static RichiestaInviaMessaggioPostaType constructRichiestaInviaMessaggioPostaType(String idMessaggio,
			String codAoo,String indirizzoMailMittente,String isPec,String ufficiale,
			String messaggio,String corpoMessaggio,List<String> indirizzoMailDest,
			List<String> allegatiGuid,Boolean notificaSpedizione) {

		RichiestaInviaMessaggioPostaType output = ofMessages.createRichiestaInviaMessaggioPostaType();

		output.setIdMessaggio(idMessaggio);  
		OrgAooType orgAoo = ofTypes.createOrgAooType();
		orgAoo.setCodiceAOO(codAoo);
		EmailIndirizzoEmailType indirizzoEmailMittente = ofTypes.createEmailIndirizzoEmailType();
		indirizzoEmailMittente.setEmail(indirizzoMailMittente);
		
		OrgCasellaEmailType orgCasellaEmail = ofTypes.createOrgCasellaEmailType();
		orgCasellaEmail.setAoo(orgAoo);
		orgCasellaEmail.setIndirizzoEmail(indirizzoEmailMittente);
		orgCasellaEmail.setIsPEC(isPec);
		
		
		orgCasellaEmail.setIsPEC(isPec);
		orgCasellaEmail.setUfficiale(ufficiale);
		output.setCasellaEmail(orgCasellaEmail);
		
		output.setMessaggio(messaggio);
		output.setCorpoMessaggio(corpoMessaggio);
		
		
		if(indirizzoMailDest!=null && indirizzoMailDest.size()>0){
			for(String destinatarioMail : indirizzoMailDest) {
				EmailIndirizzoEmailType indirizzoEmailDestinatario = ofTypes.createEmailIndirizzoEmailType();
				indirizzoEmailDestinatario.setEmail(destinatarioMail);
				output.getDestinatarioEmail().add(indirizzoEmailDestinatario);
			}
		}
		if(allegatiGuid!=null && allegatiGuid.size()>0) {
			for(String allegatoGuid : allegatiGuid) {
				output.getAllegati().add(allegatoGuid);
			}
		}
		  
		output.setNotificaSpedizione(notificaSpedizione);
		return output;
	} 
}
