package it.ibm.red.business.nps.dto;

public class EnteDTO {
	private String codiceAmministrazioneEnte;
	private String codiceAOOEnte;
	private String codiceUOEnte;
	private String denominazioneUOEnte;

	public String getCodiceAmministrazioneEnte() {
		return this.codiceAmministrazioneEnte;
	}

	public void setCodiceAmministrazioneEnte(String codiceAmministrazioneEnte) {
		this.codiceAmministrazioneEnte = codiceAmministrazioneEnte;
	}

	public String getCodiceAOOEnte() {
		return this.codiceAOOEnte;
	}

	public void setCodiceAOOEnte(String codiceAOOEnte) {
		this.codiceAOOEnte = codiceAOOEnte;
	}

	public String getCodiceUOEnte() {
		return this.codiceUOEnte;
	}

	public void setCodiceUOEnte(String codiceUOEnte) {
		this.codiceUOEnte = codiceUOEnte;
	}

	public String getDenominazioneUOEnte() {
		return this.denominazioneUOEnte;
	}

	public void setDenominazioneUOEnte(String denominazioneUOEnte) {
		this.denominazioneUOEnte = denominazioneUOEnte;
	}
}
