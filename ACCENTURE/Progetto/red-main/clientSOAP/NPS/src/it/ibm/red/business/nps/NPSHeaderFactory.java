package it.ibm.red.business.nps;

import java.io.Serializable;

import it.gov.mef.servizi.common.headeraccessoapplicativo.AccessoApplicativo;

public class NPSHeaderFactory implements Serializable {
	private static final long serialVersionUID = 8005323678119919808L;

	private String applicazione;
	private String utente;
	private String password;
	private String servizio;
	private String client;
	private String actor;

	private it.gov.mef.servizi.common.headeraccessoapplicativo.ObjectFactory of;

	public NPSHeaderFactory(String utente, String client, String password, String servizio, String actor,
			String applicazione) {
		this.utente = utente;
		this.client = client;
		this.password = password;
		this.servizio = servizio;
		this.actor = actor;
		this.applicazione = applicazione;

		this.of = new it.gov.mef.servizi.common.headeraccessoapplicativo.ObjectFactory();
	}

	public AccessoApplicativo createHeader(String messageId) {
		return createHeader(messageId, false);
	}

	public AccessoApplicativo createHeader() {
		return createHeader(null);
	}

	public AccessoApplicativo createHeaderSuperUser(String messageId) {
		return createHeader(messageId, true);
	}

	private AccessoApplicativo createHeader(String messageId, boolean superUser) {
		AccessoApplicativo headerAccesso = of.createAccessoApplicativo();

		headerAccesso.setPassword(this.password);
		headerAccesso.setUtente(this.utente);
		headerAccesso.setServizio(this.servizio);

		headerAccesso.setActor(this.actor);
		headerAccesso.setClient(this.client);
		headerAccesso.setMessageId(messageId);
		headerAccesso.setMustUnderstand(true);
		headerAccesso.setApplicazione(this.applicazione);
		return headerAccesso;
	}
}