package it.ibm.red.business.nps.enums;

public enum StatoProtocolloEntrataEnum {
	DA_ASSEGNARE("Da assegnare"), SOSPESO("Sospeso"), IN_LAVORAZIONE("In Lavorazione"), RESPINTO(
			"Respinto"), ALLA_FIRMA("Alla Firma"), AGLI_ATTI("Agli atti"), RISPOSTA("Risposta");

	private String descrizione;

	public String getDescrizione() {
		return this.descrizione;
	}

	private StatoProtocolloEntrataEnum(String inDescrizione) {
		this.descrizione = inDescrizione;
	}
}
