package it.ibm.red.business.nps;

import java.lang.reflect.Method;

import it.ibm.red.business.nps.exceptions.NPSAsynchCallerRuntimeException;
import it.ibm.red.business.nps.finalizer.FinalizerCommand;

public class NPSAsynchCaller {
	
	private static FinalizerCommand createFinalizer(String cls) {
		FinalizerCommand output = null;
		try {
			Object obj = Class.forName(cls).newInstance();
			if ((obj != null) && ((obj instanceof FinalizerCommand))) {
				output = (FinalizerCommand) obj;
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
			throw new NPSAsynchCallerRuntimeException(e);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw new NPSAsynchCallerRuntimeException(e);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			throw new NPSAsynchCallerRuntimeException(e);
		}
		return output;
	}

	public static Long sendRequest(Long idUtente, Class<? extends Object> cls, String method, Object input,
			Class<? extends FinalizerCommand> finalizerCls) {
		return null;
	}

	@SuppressWarnings("unused")
	public static void consume(Integer idAoo, Long partitionId) {
		String cls = null;
		String method = null;
		Object input = null;
		String finalizerFQN = null;
		String messageID = null;
		Object soapOutput = execute(idAoo, cls, method, messageID, input);
		if ((finalizerFQN != null) && (finalizerFQN.length() > 0)) {
			FinalizerCommand finalizer = createFinalizer(finalizerFQN);
			finalizer.finalize(soapOutput);
		}
	}

	@SuppressWarnings("rawtypes")
	public static Object execute(Integer idAoo, String clsName, String mtdName, String messageID, Object input) {
		Class[] types = new Class[3];
		types[0] = Integer.class;
		types[1] = String.class;
		types[2] = input.getClass();
		try {
			Method method = Class.forName(clsName).getMethod(mtdName, types);
			return method.invoke(null, idAoo, messageID ,input);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}
}
