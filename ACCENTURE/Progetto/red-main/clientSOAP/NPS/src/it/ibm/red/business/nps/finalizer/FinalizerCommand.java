package it.ibm.red.business.nps.finalizer;

public abstract interface FinalizerCommand
{
  public abstract void finalize(Object paramObject);
}
