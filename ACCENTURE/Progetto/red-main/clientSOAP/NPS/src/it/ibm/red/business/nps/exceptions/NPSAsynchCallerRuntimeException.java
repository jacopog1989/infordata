package it.ibm.red.business.nps.exceptions;

public class NPSAsynchCallerRuntimeException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public NPSAsynchCallerRuntimeException(Exception exception) {
		super(exception);
	}

	public NPSAsynchCallerRuntimeException(String msg) {
		super(msg);
	}
}
