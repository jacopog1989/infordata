package it.ibm.red.business.nps.dto;

public class OperatoreDTO {
	private String cognome;
	private String idOperatore;
	private String nome;
	private String idUO;
	private String codiceUO;
	private String denominazioneUO;
	private String denominazioneAmministrazione;
	private String codiceAmministrazione;
	private String denominazioneAOO;
	private String codiceAOO;
	private String codiceFiscale;

	public OperatoreDTO(String cognome, String idOperatore, String nome, String codiceFiscale, String idUO, String codiceUO, String denominazioneUO,
			String denominazioneAmministrazione, String codiceAmministrazione, String denominazioneAOO,
			String codiceAOO) {
		this.cognome = cognome;
		this.idOperatore = idOperatore;
		this.nome = nome;
		this.idUO = idUO;
		this.codiceUO = codiceUO;
		this.denominazioneUO = denominazioneUO;
		this.denominazioneAmministrazione = denominazioneAmministrazione;
		this.codiceAmministrazione = codiceAmministrazione;
		this.denominazioneAOO = denominazioneAOO;
		this.codiceAOO = codiceAOO;
		this.codiceFiscale = codiceFiscale;
	}

	public String getCognome() {
		return this.cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getIdOperatore() {
		return this.idOperatore;
	}

	public void setIdOperatore(String idOperatore) {
		this.idOperatore = idOperatore;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getIdUO() {
		return idUO;
	}

	public void setIdUO(String idUO) {
		this.idUO = idUO;
	}

	public String getCodiceUO() {
		return this.codiceUO;
	}

	public void setCodiceUO(String codiceUO) {
		this.codiceUO = codiceUO;
	}

	public String getDenominazioneUO() {
		return this.denominazioneUO;
	}

	public void setDenominazioneUO(String denominazioneUO) {
		this.denominazioneUO = denominazioneUO;
	}

	public String getDenominazioneAmministrazione() {
		return this.denominazioneAmministrazione;
	}

	public void setDenominazioneAmministrazione(String denominazioneAmministrazione) {
		this.denominazioneAmministrazione = denominazioneAmministrazione;
	}

	public String getCodiceAmministrazione() {
		return this.codiceAmministrazione;
	}

	public void setCodiceAmministrazione(String codiceAmministrazione) {
		this.codiceAmministrazione = codiceAmministrazione;
	}

	public String getDenominazioneAOO() {
		return this.denominazioneAOO;
	}

	public void setDenominazioneAOO(String denominazioneAOO) {
		this.denominazioneAOO = denominazioneAOO;
	}

	public String getCodiceAOO() {
		return this.codiceAOO;
	}

	public void setCodiceAOO(String codiceAOO) {
		this.codiceAOO = codiceAOO;
	}
	
	public String getCodiceFiscale() {
		return codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}
}
