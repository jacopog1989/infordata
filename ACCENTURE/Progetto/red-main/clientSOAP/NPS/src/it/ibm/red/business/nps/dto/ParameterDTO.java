package it.ibm.red.business.nps.dto;

import it.ibm.red.business.nps.exceptions.NPSAsynchCallerRuntimeException;

public class ParameterDTO {
	private Object valore;
	  private Class cls;
	  
	  public Object getValore()
	  {
	    return this.valore;
	  }
	  
	  public Class getCls()
	  {
	    return this.cls;
	  }
	  
	  public ParameterDTO(Class inCls, Object inValore)
	  {
	    this.valore = inValore;
	    this.cls = inCls;
	  }
	  
	  public ParameterDTO(String fqnTipo, String valoreTxt)
	  {
	    this.cls = getClassFromString(fqnTipo);
	  }
	  
	  private static Class getClassFromString(String fqn)
	  {
	    try
	    {
	      return Class.forName(fqn);
	    }
	    catch (ClassNotFoundException e)
	    {
	      e.printStackTrace();
	      throw new NPSAsynchCallerRuntimeException(e);
	    }
	  }
}
