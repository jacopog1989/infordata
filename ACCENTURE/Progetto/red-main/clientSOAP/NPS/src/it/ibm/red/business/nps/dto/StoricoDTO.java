package it.ibm.red.business.nps.dto;

import java.util.Date;

import npstypes.v1.it.gov.mef.OrgOrganigrammaType;

public class StoricoDTO {
	
	private OrgOrganigrammaType operatore;
	
	private String descrizione;
	
	private String tipoOperazione;
	
	private Date dataOperazione;
	
	
	public OrgOrganigrammaType getOperatore() {
		return this.operatore;
	}

	public void setOperatore(OrgOrganigrammaType operatore) {
		this.operatore = operatore;
	}

	public String getDescrizione() {
		return this.descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getTipoOperazione() {
		return this.tipoOperazione;
	}

	public void setTipoOperazione(String tipoOperazione) {
		this.tipoOperazione = tipoOperazione;
	}

	public Date getDataOperazione() {
		return this.dataOperazione;
	}

	public void setDataOperazione(Date dataOperazione) {
		this.dataOperazione = dataOperazione;
	}
}