package it.ibm.red.business.nps.dto;

public class AssegnanteDTO {
	
	private Boolean isUtente;
	
	private String cognome;
	
	private String nome;
	
	private String codiceFiscale;
	
	private String descrizioneNodo;

	private String codiceAoo;
	

	/**
	 * @return the isUtente
	 */
	public Boolean getIsUtente() {
		return isUtente;
	}

	/**
	 * @param isUtente the isUtente to set
	 */
	public void setIsUtente(Boolean isUtente) {
		this.isUtente = isUtente;
	}

	/**
	 * @return the cognome
	 */
	public String getCognome() {
		return cognome;
	}

	/**
	 * @param cognome the cognome to set
	 */
	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the codiceFiscale
	 */
	public String getCodiceFiscale() {
		return codiceFiscale;
	}

	/**
	 * @param codiceFiscale the codiceFiscale to set
	 */
	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}

	/**
	 * @return the descrizioneNodo
	 */
	public String getDescrizioneNodo() {
		return descrizioneNodo;
	}

	/**
	 * @param descrizioneNodo the descrizioneNodo to set
	 */
	public void setDescrizioneNodo(String descrizioneNodo) {
		this.descrizioneNodo = descrizioneNodo;
	}

	/**
	 * @return the codiceAoo
	 */
	public String getCodiceAoo() {
		return codiceAoo;
	}

	/**
	 * @param codiceAoo the codiceAoo to set
	 */
	public void setCodiceAoo(String codiceAoo) {
		this.codiceAoo = codiceAoo;
	}
	
}
