package it.ibm.red.business.nps.dto;

import java.io.Serializable;

public class NPSConfigurationDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private int idAoo;
	private String codiceAoo;
	private String denominazioneAOO;
	private String codiceAmministrazione;
	private String denominazioneAmministrazione;
	private String registroUfficiale;
	private String denominazioneRegistro;

	public int getIdAoo() {
		return this.idAoo;
	}

	public void setIdAoo(int idAoo) {
		this.idAoo = idAoo;
	}

	public String getCodiceAoo() {
		return this.codiceAoo;
	}

	public void setCodiceAoo(String codiceAoo) {
		this.codiceAoo = codiceAoo;
	}

	public String getDenominazioneAOO() {
		return this.denominazioneAOO;
	}

	public void setDenominazioneAOO(String denominazioneAOO) {
		this.denominazioneAOO = denominazioneAOO;
	}

	public String getCodiceAmministrazione() {
		return this.codiceAmministrazione;
	}

	public void setCodiceAmministrazione(String codiceAmministrazione) {
		this.codiceAmministrazione = codiceAmministrazione;
	}

	public String getDenominazioneAmministrazione() {
		return this.denominazioneAmministrazione;
	}

	public void setDenominazioneAmministrazione(String denominazioneAmministrazione) {
		this.denominazioneAmministrazione = denominazioneAmministrazione;
	}

	public String getRegistroUfficiale() {
		return this.registroUfficiale;
	}

	public void setRegistroUfficiale(String registroUfficiale) {
		this.registroUfficiale = registroUfficiale;
	}

	public String getDenominazioneRegistro() {
		return this.denominazioneRegistro;
	}

	public void setDenominazioneRegistro(String denominazioneRegistro) {
		this.denominazioneRegistro = denominazioneRegistro;
	}
}