package it.ibm.red.business.nps.dto;

import java.io.InputStream;
import java.util.Date;

public class DocumentoNpsDTO {
	
	private String guid;
	
	private String nomeFile;
	
	private String contentType;
	
	private InputStream inputStream;
	
	private String descrizione;
	
	private String oggetto;
	
	private Date dataCreazione;
	
	private String guidLastVersion;
	
	private byte[] hash;

	public String getGuid() {
		return this.guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getNomeFile() {
		return this.nomeFile;
	}

	public void setNomeFile(String nomeFile) {
		this.nomeFile = nomeFile;
	}

	public String getContentType() {
		return this.contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public InputStream getInputStream() {
		return this.inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public String getDescrizione() {
		return this.descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getOggetto() {
		return oggetto;
	}

	public void setOggetto(String oggetto) {
		this.oggetto = oggetto;
	}

	public Date getDataCreazione() {
		return dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}
	
	public String getGuidLastVersion() {
		return guidLastVersion;
	}

	public void setGuidLastVersion(String guidLastVersion) {
		this.guidLastVersion = guidLastVersion;
	}
	
	public byte[] getHash() {
		return hash;
	}

	public void setHash(byte[] hash) {
		this.hash = hash;
	}
	
}