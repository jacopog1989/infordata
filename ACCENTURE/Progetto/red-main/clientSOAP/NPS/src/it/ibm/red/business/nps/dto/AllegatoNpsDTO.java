package it.ibm.red.business.nps.dto;

import java.io.InputStream;
import java.util.Date;


public class AllegatoNpsDTO {
	
	private String guid;
	
	private String nomeFile;
	
	private String oggetto;
	
	private Date dateCreated;
	
	private String guidLastVersion;
	
	private String contentType;
	
	private InputStream inputStream;	

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getNomeFile() {
		return nomeFile;
	}

	public void setNomeFile(String nomeFile) {
		this.nomeFile = nomeFile;
	}

	public String getOggetto() {
		return oggetto;
	}

	public void setOggetto(String oggetto) {
		this.oggetto = oggetto;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public String getGuidLastVersion() {
		return guidLastVersion;
	}

	public void setGuidLastVersion(String guidLastVersion) {
		this.guidLastVersion = guidLastVersion;
	}
	
	public String getContentType() {
		return this.contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public InputStream getInputStream() {
		return this.inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}
	
}