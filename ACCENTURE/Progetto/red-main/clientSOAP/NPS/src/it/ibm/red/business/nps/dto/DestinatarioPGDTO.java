package it.ibm.red.business.nps.dto;

public class DestinatarioPGDTO extends DestinatarioDTO {
	private String denominazionePG;

	public String getDenominazionePG() {
		return this.denominazionePG;
	}

	public void setDenominazionePG(String denominazionePG) {
		this.denominazionePG = denominazionePG;
	}
}
