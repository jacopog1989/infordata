package it.ibm.red.business.nps.dto;

public class DestinatarioPFDTO extends DestinatarioDTO {
	private String nomePF;
	private String cognomePF;
	private String codiceFiscalePF;

	public String getNomePF() {
		return this.nomePF;
	}

	public void setNomePF(String nomePF) {
		this.nomePF = nomePF;
	}

	public String getCognomePF() {
		return this.cognomePF;
	}

	public void setCognomePF(String cognomePF) {
		this.cognomePF = cognomePF;
	}

	public String getCodiceFiscalePF() {
		return this.codiceFiscalePF;
	}

	public void setCodiceFiscalePF(String codiceFiscalePF) {
		this.codiceFiscalePF = codiceFiscalePF;
	}
}
