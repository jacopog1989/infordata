package it.ibm.red.business.nps.builder;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import it.gov.digitpa.protocollo.CodiceAOO;
import it.gov.digitpa.protocollo.CodiceAmministrazione;
import it.gov.digitpa.protocollo.ContestoProcedurale;
import it.gov.digitpa.protocollo.Identificativo;
import it.gov.digitpa.protocollo.TipoContestoProcedurale;
import it.ibm.red.business.nps.dto.DocumentoNpsDTO;
import npsmessages.v1.it.gov.mef.IdentificativoDestinatarioType;
import npsmessages.v1.it.gov.mef.IdentificativoDocumentoRequestType;
import npsmessages.v1.it.gov.mef.IdentificativoProtocolloRequestType;
import npsmessages.v1.it.gov.mef.RichiestaAggiungiAllaccioProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaAggiungiDocumentiProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaDownloadDocumentoType;
import npstypes.v1.it.gov.mef.AllChiaveEsternaType;
import npstypes.v1.it.gov.mef.AllCodiceDescrizioneType;
import npstypes.v1.it.gov.mef.AllTipoRegistroAusiliarioType;
import npstypes.v1.it.gov.mef.AnaEnteEsternoType;
import npstypes.v1.it.gov.mef.AnaIndirizzoTelematicoType;
import npstypes.v1.it.gov.mef.AnaPersonaFisicaType;
import npstypes.v1.it.gov.mef.AnaPersonaGiuridicaType;
import npstypes.v1.it.gov.mef.DocTipoOperazioneType;
import npstypes.v1.it.gov.mef.EmailIndirizzoEmailType;
import npstypes.v1.it.gov.mef.OrgAmministrazioneType;
import npstypes.v1.it.gov.mef.OrgAooType;
import npstypes.v1.it.gov.mef.OrgCasellaEmailType;
import npstypes.v1.it.gov.mef.OrgOrganigrammaAooType;
import npstypes.v1.it.gov.mef.OrgOrganigrammaType;
import npstypes.v1.it.gov.mef.OrgOrganigrammaUnitaOrganizzativaType;
import npstypes.v1.it.gov.mef.OrgUnitaOrganizzativaType;
import npstypes.v1.it.gov.mef.ProtAllaccioProtocolloType;
import npstypes.v1.it.gov.mef.ProtAssegnatarioType;
import npstypes.v1.it.gov.mef.ProtAttributiEstesiType;
import npstypes.v1.it.gov.mef.ProtDestinatarioType;
import npstypes.v1.it.gov.mef.ProtIdentificatoreProtocolloType;
import npstypes.v1.it.gov.mef.ProtMetadatiType;
import npstypes.v1.it.gov.mef.ProtMetadatoAssociatoType;
import npstypes.v1.it.gov.mef.ProtMittenteType;
import npstypes.v1.it.gov.mef.ProtRegistroProtocolloType;
import npstypes.v1.it.gov.mef.ProtRicezioneType;
import npstypes.v1.it.gov.mef.ProtSpedizioneType;
import npstypes.v1.it.gov.mef.ProtSpedizioneType.MezzoSpedizione.Elettronico;
import npstypes.v1.it.gov.mef.ProtTipoProtocolloType;
import npstypes.v1.it.gov.mef.ProtTipologiaDocumentoType;
import npstypes.v1.it.gov.mef.RegRegistroType;
import npstypes.v1.it.gov.mef.RegTipologiaDocumentoType;
import npstypes.v1.it.gov.mef.WkfDatiContestoProceduraleType;
import npstypes.v1.it.gov.mef.WkfDestinatarioFlussoType;
import npstypesestesi.v1.it.gov.mef.MetaDatoAssociato;
import npstypesestesi.v1.it.gov.mef.TIPOLOGIA;

public class Builder {

	public static final String SI = "si";
	public static final String NO = "no";
	
	public static final String DA_SPEDIRE = "DA_SPEDIRE";
    public static final String SPEDITO = "SPEDITO";
    public static final String ERRORE = "ERRORE";
    public static final String RICEVUTA_ACCETTAZIONE = "RICEVUTA_ACCETTAZIONE";
    public static final String RICEVUTA_CONSEGNA = "RICEVUTA_CONSEGNA";

    public static final String AGLI_ATTI = "AGLI_ATTI";
    public static final String CHIUSO = "CHIUSO";
    
    public static final String URI = "uri";
    public static final String SMTP = "smtp";
    public static final String NMTOKEN = "NMTOKEN";
    
    public static final String DA_ASSEGNARE = "DA_ASSEGNARE";
    public static final String IN_LAVORAZIONE = "IN_LAVORAZIONE";
    public static final String INVIATO = "INVIATO";
    
    public static final String REGISTRO_UFFICIALE = "REGISTRO UFFICIALE";
        
	private static npstypes.v1.it.gov.mef.ObjectFactory ofTypes;
	private static npsmessages.v1.it.gov.mef.ObjectFactory ofMessages;
	private static npstypesestesi.v1.it.gov.mef.ObjectFactory ofTypesAttrExt;

	static {
		ofTypes = new npstypes.v1.it.gov.mef.ObjectFactory();
		ofMessages = new npsmessages.v1.it.gov.mef.ObjectFactory();
		ofTypesAttrExt = new npstypesestesi.v1.it.gov.mef.ObjectFactory();
	}

	public static OrgOrganigrammaType buildOrganigrammaPf(String idUtente, String cognome, String nome, String codiceFiscale,
			String codiceAmministrazione, String denominazioneAmministrazione, String codiceAOO,
			String denominazioneAOO, String codiceUO, String denominazioneUO, String idUO) {

		OrgAmministrazioneType amministrazione = ofTypes.createOrgAmministrazioneType();
		amministrazione.setCodiceAmministrazione(codiceAmministrazione);
		amministrazione.setDenominazione(denominazioneAmministrazione);

		OrgOrganigrammaAooType aoo = ofTypes.createOrgOrganigrammaAooType();
		aoo.setAmministrazione(amministrazione);
		aoo.setCodiceAOO(codiceAOO);
		if (denominazioneAOO != null) {
			aoo.setDenominazione(denominazioneAOO);
		}

		OrgOrganigrammaUnitaOrganizzativaType unitaOrganizzativa = ofTypes.createOrgOrganigrammaUnitaOrganizzativaType();
		unitaOrganizzativa.setAoo(aoo);
		unitaOrganizzativa.setDenominazione(denominazioneUO);
		unitaOrganizzativa.setCodiceUO(codiceUO);
		AllChiaveEsternaType chiaveEsternaUO = ofTypes.createAllChiaveEsternaType();
		chiaveEsternaUO.setValue(idUO);
		unitaOrganizzativa.setChiaveEsterna(chiaveEsternaUO);

		AnaPersonaFisicaType utente = ofTypes.createAnaPersonaFisicaType();
		utente.setCognome(cognome);
		utente.setNome(nome);
		if(codiceFiscale!=null) {
			utente.setCodiceFiscale(codiceFiscale);
		}
		AllChiaveEsternaType chiaveEsternaUtente = ofTypes.createAllChiaveEsternaType();
		chiaveEsternaUtente.setValue(idUtente);
		utente.setChiaveEsterna(chiaveEsternaUtente);

		AllChiaveEsternaType chiaveEsternaOperatore = ofTypes.createAllChiaveEsternaType();
		chiaveEsternaOperatore.setValue(idUO + "." + idUtente);
		OrgOrganigrammaType output = ofTypes.createOrgOrganigrammaType();
		output.setUnitaOrganizzativa(unitaOrganizzativa);
		output.setUtente(utente);
		output.setChiaveEsterna(chiaveEsternaOperatore);
		return output;
	}

	public static OrgOrganigrammaType buildOrganigrammaEnte(String codiceAmministrazione,
			String denominazioneAmministrazione, String codiceAOO, String denominazioneAOO, String codiceUO,
			String denominazioneUO, String idUO) {
		OrgAmministrazioneType amministrazione = ofTypes.createOrgAmministrazioneType();
		amministrazione.setCodiceAmministrazione(codiceAmministrazione);
		amministrazione.setDenominazione(denominazioneAmministrazione);

		OrgOrganigrammaAooType aoo = ofTypes.createOrgOrganigrammaAooType();
		aoo.setAmministrazione(amministrazione);
		aoo.setCodiceAOO(codiceAOO);
		if (denominazioneAOO != null) {
			aoo.setDenominazione(denominazioneAOO);
		}
		OrgOrganigrammaUnitaOrganizzativaType unitaOrganizzativa = ofTypes
				.createOrgOrganigrammaUnitaOrganizzativaType();
		unitaOrganizzativa.setAoo(aoo);
		unitaOrganizzativa.setDenominazione(denominazioneUO);
		unitaOrganizzativa.setCodiceUO(codiceUO);
		AllChiaveEsternaType chiaveEsternaUO = ofTypes.createAllChiaveEsternaType();
		chiaveEsternaUO.setValue(idUO);
		unitaOrganizzativa.setChiaveEsterna(chiaveEsternaUO);

		AllChiaveEsternaType chiaveEsternaOperatore = ofTypes.createAllChiaveEsternaType();
		chiaveEsternaOperatore.setValue(idUO);
		OrgOrganigrammaType output = ofTypes.createOrgOrganigrammaType();
		output.setUnitaOrganizzativa(unitaOrganizzativa);
		output.setChiaveEsterna(chiaveEsternaOperatore);
		return output;
	}

	public static IdentificativoProtocolloRequestType buildIdentificativoProtocollo(String id) {
		IdentificativoProtocolloRequestType identificativoProtocolloRequestType = ofMessages
				.createIdentificativoProtocolloRequestType();
		identificativoProtocolloRequestType.setIdProtocollo(id);
		return identificativoProtocolloRequestType;
	}

	public static AllCodiceDescrizioneType buildCodiceDescrizione(String codice, String denominazione) {
		AllCodiceDescrizioneType cod = ofTypes.createAllCodiceDescrizioneType();
		cod.setCodice(codice);
		cod.setDescrizione(denominazione);
		return cod;
	}

	public static ProtRegistroProtocolloType buildRegistroProtocollo(OrgOrganigrammaType operatore,
			String codiceRegistro, String denominazioneRegistro) {
		ProtRegistroProtocolloType registro = ofTypes.createProtRegistroProtocolloType();
		registro.setAoo(operatore.getUnitaOrganizzativa().getAoo());
		AllCodiceDescrizioneType codice = buildCodiceDescrizione(codiceRegistro, denominazioneRegistro);
		registro.setCodice(codice);
		registro.setUfficiale(SI);
		return registro;
	}

	public static ProtRegistroProtocolloType buildRegistroProtocollo(String codiceRegistro,
			String denominazioneRegistro, String denominazioneAmministrazione, String codiceAmministrazione,
			String codiceAOO, String denominazioneAOO) {
		ProtRegistroProtocolloType registro = ofTypes.createProtRegistroProtocolloType();
		OrgAmministrazioneType amministrazione = ofTypes.createOrgAmministrazioneType();
		amministrazione.setCodiceAmministrazione(codiceAmministrazione);
		amministrazione.setDenominazione(denominazioneAmministrazione);
		OrgOrganigrammaAooType aoo = ofTypes.createOrgOrganigrammaAooType();
		aoo.setAmministrazione(amministrazione);
		aoo.setCodiceAOO(codiceAOO);
		aoo.setDenominazione(denominazioneAOO);
		registro.setAoo(aoo);
		AllCodiceDescrizioneType codice = buildCodiceDescrizione(codiceRegistro, denominazioneRegistro);
		registro.setCodice(codice);
		registro.setUfficiale(SI); 
		
		if (!REGISTRO_UFFICIALE.equals(codiceRegistro)) {
			registro.setUfficiale(NO); 
		}
		
		return registro;
	}

	public static IdentificativoDestinatarioType buildDestinatario(String idDestinatario) {
		AllChiaveEsternaType chiaveEsterna = ofTypes.createAllChiaveEsternaType();
		chiaveEsterna.setValue(idDestinatario);
		IdentificativoDestinatarioType destinatario = ofMessages.createIdentificativoDestinatarioType();
		destinatario.setChiaveEsterna(chiaveEsterna);
		return destinatario;
	}

	public static IdentificativoDocumentoRequestType buildIdDocumento(String idDocumento) {
		IdentificativoDocumentoRequestType documentov = ofMessages.createIdentificativoDocumentoRequestType();
		documentov.setIdDocumento(idDocumento);
		return documentov;
	}

	public static OrgCasellaEmailType buildPostaElettronica(String ufficialePostaElettronica,
			String codiceAOOPostaElettronica, AllChiaveEsternaType chiaveEsternaAOOPostaElettronica,
			String denominazioneAOOPostaElettronica, String displayNamePostaElettronica, String isPECPostaElettronica,
			String codiceUOPostaElettronica, String emailPostaElettronica, String denominazioneUOPostaElettronica,
			AllChiaveEsternaType chiaveEsternaUOPostaElettronica) {
		OrgCasellaEmailType postaElettronica = ofTypes.createOrgCasellaEmailType();
		postaElettronica.setUfficiale(ufficialePostaElettronica);
		OrgAooType orgAooType = ofTypes.createOrgAooType();
		orgAooType.setCodiceAOO(codiceAOOPostaElettronica);
		orgAooType.setDenominazione(denominazioneAOOPostaElettronica);
		orgAooType.setChiaveEsterna(chiaveEsternaAOOPostaElettronica);
		postaElettronica.setAoo(orgAooType);
		EmailIndirizzoEmailType emailIndirizzoEmailType = ofTypes.createEmailIndirizzoEmailType();
		emailIndirizzoEmailType.setDisplayName(displayNamePostaElettronica);
		emailIndirizzoEmailType.setEmail(emailPostaElettronica);
		postaElettronica.setIndirizzoEmail(emailIndirizzoEmailType);
		postaElettronica.setIsPEC(isPECPostaElettronica);
		OrgUnitaOrganizzativaType orgUnitaOrganizzativaType = ofTypes.createOrgUnitaOrganizzativaType();
		orgUnitaOrganizzativaType.setChiaveEsterna(chiaveEsternaUOPostaElettronica);
		orgUnitaOrganizzativaType.setCodiceUO(codiceUOPostaElettronica);
		orgUnitaOrganizzativaType.setDenominazione(denominazioneUOPostaElettronica);
		postaElettronica.setUnitaOrganizzativa(orgUnitaOrganizzativaType);
		return postaElettronica;
	}

	public static ProtAttributiEstesiType buildTipologiaDocumento(String descrizioneTipologia, String codiceTipologia,
			ProtMetadatiType metadatiAssociati) {
		ProtAttributiEstesiType output = ofTypes.createProtAttributiEstesiType();
		if (output.getMetadatiAssociati() == null) {
			output.setMetadatiAssociati(ofTypes.createProtAttributiEstesiTypeMetadatiAssociati(metadatiAssociati));
		} else {
			output.getMetadatiAssociati().setValue(metadatiAssociati);
		}
		
		ProtTipologiaDocumentoType protTipologiaDocumentoTypenew = ofTypes.createProtTipologiaDocumentoType();
		protTipologiaDocumentoTypenew.setCodice(codiceTipologia);
		if (protTipologiaDocumentoTypenew.getDescrizione() == null) {
			protTipologiaDocumentoTypenew.setDescrizione(ofTypes.createProtTipologiaDocumentoTypeDescrizione(descrizioneTipologia));
		} else {
			protTipologiaDocumentoTypenew.getDescrizione().setValue(descrizioneTipologia);
		}
		output.setTipologia(protTipologiaDocumentoTypenew);
		
		return output;
	}

	public static ProtMittenteType buildMittenteEnte(String tipo, String value, String codiceAmministrazioneEnte,
			String codiceAOOEnte, String codiceUOEnte, String denominazioneUOEnte) {
		ProtMittenteType output = ofTypes.createProtMittenteType();
		output.setEnte(buildEnte(codiceAmministrazioneEnte, codiceAOOEnte, codiceUOEnte, denominazioneUOEnte));
		if ((value != null) && (tipo != null)) {
			output.setIndirizzoTelematico(buildIndirizzoTelematico(tipo, value));
		}
		return output;
	}

	public static ProtMittenteType buildMittenteEntrataPF(String codiceRicezione, String descrizioneRicezione,
			String tipo, String indirizzoTelematico, String nomePF, String cognomePF, String codiceFiscalePF)
			throws DatatypeConfigurationException {
		ProtMittenteType output = ofTypes.createProtMittenteType();
		output.setIndirizzoTelematico(buildIndirizzoTelematico(tipo, indirizzoTelematico));
		AnaPersonaFisicaType pf = buildPF(nomePF, cognomePF, codiceFiscalePF, indirizzoTelematico);
		ProtRicezioneType ricezione = ofTypes.createProtRicezioneType();
		AllCodiceDescrizioneType codiceDesc = ofTypes.createAllCodiceDescrizioneType();
		codiceDesc.setCodice(codiceRicezione);
		codiceDesc.setDescrizione(descrizioneRicezione);
		ricezione.setMezzoRicezione(codiceDesc);
		ricezione.setDataRicezione(buildXmlGregorianCalendarFromDate(new Date()));

		output.setRicezione(ricezione);
		output.setPersonaFisica(pf);
		return output;
	}

	public static ProtMittenteType buildMittenteUscitaPF(String tipo, String indirizzoTelematico, String nomePF,
			String cognomePF, String codiceFiscalePF, String codiceAmministrazioneEnte, String codiceAOOEnte,
			String codiceUOEnte, String denominazioneUOEnte) {
		ProtMittenteType output = ofTypes.createProtMittenteType();
		
		if ((tipo != null) && (indirizzoTelematico != null)) {
			output.setIndirizzoTelematico(buildIndirizzoTelematico(tipo, indirizzoTelematico));
		}
		
		AnaEnteEsternoType ente = buildEnte(codiceAmministrazioneEnte, codiceAOOEnte, codiceUOEnte, denominazioneUOEnte);
		
		if ((nomePF != null) && (cognomePF != null)) {
			AnaPersonaFisicaType pf = buildPF(nomePF, cognomePF, codiceFiscalePF, indirizzoTelematico);
			ente.setPersona(pf);
		}
		output.setEnte(ente);
		
		return output;
	}

	public static ProtMittenteType buildMittentePG(String codiceRicezione, String descrizioneRicezione, String tipo,
			String value, String denominazionePG) throws DatatypeConfigurationException {
		ProtMittenteType output = ofTypes.createProtMittenteType();
		output.setIndirizzoTelematico(buildIndirizzoTelematico(tipo, value));
		output.setPersonaGiuridica(buildPG(denominazionePG));

		ProtRicezioneType ricezione = ofTypes.createProtRicezioneType();
		AllCodiceDescrizioneType codiceDesc = ofTypes.createAllCodiceDescrizioneType();
		codiceDesc.setCodice(codiceRicezione);
		codiceDesc.setDescrizione(descrizioneRicezione);
		ricezione.setMezzoRicezione(codiceDesc);
		ricezione.setDataRicezione(buildXmlGregorianCalendarFromDate(new Date()));

		output.setRicezione(ricezione);
		return output;
	}

	public static ProtIdentificatoreProtocolloType buildIdentificatoreProtocollo(Date dataRegistrazione,
			Integer numeroRegistrazione, String codiceRegistro, String denominazioneAmministrazione,
			String denominazioneRegistro, String codiceAmministrazione, String denominazioneAOO, String codiceAOO,
			ProtTipoProtocolloType tipoProtocollo) throws DatatypeConfigurationException {
		ProtIdentificatoreProtocolloType output = ofTypes.createProtIdentificatoreProtocolloType();
		output.setDataRegistrazione(buildXmlGregorianCalendarFromDate(dataRegistrazione));
		output.setNumeroRegistrazione(1);
		output.setRegistro(buildRegistroProtocollo(codiceRegistro, denominazioneRegistro, denominazioneAmministrazione,
				codiceAmministrazione, codiceAOO, denominazioneAOO));
		output.setTipoProtocollo(tipoProtocollo);
		return output;
	}

	public static RichiestaAggiungiAllaccioProtocolloType.Allaccio buildAllaccio(boolean isRisposta, String stato,
			String idProtocollo) {
		RichiestaAggiungiAllaccioProtocolloType.Allaccio out = ofMessages
				.createRichiestaAggiungiAllaccioProtocolloTypeAllaccio();
		out.setIsRisposta(isRisposta);
		if (stato != null) {
			out.setStato(stato);
		}
		IdentificativoProtocolloRequestType protocollo = buildIdentificativoProtocollo(idProtocollo);
		out.setProtocollo(protocollo);
		return out;
	}

	public static ProtAllaccioProtocolloType buildRisposta(ProtTipoProtocolloType tipoProtocollo, boolean risposta,
			String oggetto, String numeroRegistrazione, String idProtocollo, String dataRegistrazione)
			throws DatatypeConfigurationException {
		ProtAllaccioProtocolloType output = ofTypes.createProtAllaccioProtocolloType();
		output.setDataRegistrazione(buildXmlGregorianCalendarFromStringDate(dataRegistrazione));
		if (idProtocollo != null) {
			output.setIdProtocollo(ofTypes.createProtAllaccioProtocolloTypeIdProtocollo(idProtocollo));
		}
		if (numeroRegistrazione != null) {
			output.setNumeroRegistrazione(Integer.parseInt(numeroRegistrazione));
		}
		output.setOggetto(oggetto);
		output.setRisposta(risposta);
		output.setTipoProtocollo(tipoProtocollo);
		return output;
	}
	
	public static ProtAllaccioProtocolloType buildRisposta(String numeroRegistrazione)
			throws DatatypeConfigurationException {
		ProtAllaccioProtocolloType output = ofTypes.createProtAllaccioProtocolloType();
		if (numeroRegistrazione != null) {
			output.setNumeroRegistrazione(Integer.parseInt(numeroRegistrazione));
		}
		return output;
	}
	
	public static ProtAllaccioProtocolloType buildRisposta(ProtTipoProtocolloType tipoProtocollo, boolean risposta,
			String oggetto, String numeroRegistrazione, String idProtocollo, Date dataRegistrazione)
			throws DatatypeConfigurationException {
		ProtAllaccioProtocolloType output = ofTypes.createProtAllaccioProtocolloType();
		output.setDataRegistrazione(buildXmlGregorianCalendarFromDate(dataRegistrazione));
		if (idProtocollo != null) {
			output.setIdProtocollo(ofTypes.createProtAllaccioProtocolloTypeIdProtocollo(idProtocollo));
		}
		if (numeroRegistrazione != null) {
			output.setNumeroRegistrazione(Integer.parseInt(numeroRegistrazione));
		}
		output.setOggetto(oggetto);
		output.setRisposta(risposta);
		output.setTipoProtocollo(tipoProtocollo);
		return output;
	}
	
	private static WkfDestinatarioFlussoType buildDestinatarioFlussoAUT(String perConoscenza, String tipo, String indirizzoTelematico) {
		WkfDestinatarioFlussoType output = ofTypes.createWkfDestinatarioFlussoType();
		if ((tipo != null) && (indirizzoTelematico != null)) {
			output.setIndirizzoTelematico(buildIndirizzoTelematico(tipo, indirizzoTelematico));
		}
		output.setPerConoscenza(perConoscenza);
		return output;
	}
	
	private static ProtDestinatarioType buildDestinatario(String perConoscenza, String tipo, String indirizzoTelematico) {
		ProtDestinatarioType dest = ofTypes.createProtDestinatarioType();
		if ((tipo != null) && (indirizzoTelematico != null)) {
			dest.setIndirizzoTelematico(buildIndirizzoTelematico(tipo, indirizzoTelematico));
		}
		dest.setPerConoscenza(perConoscenza);
		return dest;
	}

	public static AnaIndirizzoTelematicoType buildIndirizzoTelematico(String tipo, String value) {
		AnaIndirizzoTelematicoType indirizzoTelematico = ofTypes.createAnaIndirizzoTelematicoType();
		indirizzoTelematico.setValue(value);
		indirizzoTelematico.setTipo(tipo);
		return indirizzoTelematico;
	}

	public static ProtDestinatarioType buildDestinatarioEnte(String codiceAmministrazioneEnte, String codiceAOOEnte,
			String codiceUOEnte, String denominazioneUOEnte) {
		ProtDestinatarioType output = ofTypes.createProtDestinatarioType();
		AnaEnteEsternoType ente = buildEnte(codiceAmministrazioneEnte, codiceAOOEnte, codiceUOEnte, denominazioneUOEnte);
		output.setEnte(ente);
		return output;
	}
	
	public static WkfDestinatarioFlussoType buildDestinatarioEnteFlussoAUT(String codiceAmministrazioneEnte, String codiceAOOEnte,
			String codiceUOEnte, String denominazioneUOEnte) {
		WkfDestinatarioFlussoType output = ofTypes.createWkfDestinatarioFlussoType();
		AnaEnteEsternoType ente = buildEnte(codiceAmministrazioneEnte, codiceAOOEnte, codiceUOEnte, denominazioneUOEnte);
		output.setEnte(ente);
		return output;
	}

	private static AnaEnteEsternoType buildEnte(String codiceAmministrazioneEnte, String codiceAOOEnte,
			String codiceUOEnte, String denominazioneUOEnte) {
		AnaEnteEsternoType ente = ofTypes.createAnaEnteEsternoType();
		ente.setCodiceAmministrazione(codiceAmministrazioneEnte);
		ente.setCodiceAOO(codiceAOOEnte);
		ente.setCodiceUO(codiceUOEnte);
		ente.setDenominazioneUO(denominazioneUOEnte);
		return ente;
	}
	
	public static WkfDestinatarioFlussoType buildDestinatarioFlussoAUTPF(String perConoscenza, String nomePF, String cognomePF,
			String codiceFiscalePF, String tipo, String indirizzoEmail) {
		WkfDestinatarioFlussoType output = buildDestinatarioFlussoAUT(perConoscenza, tipo, indirizzoEmail);
		output.setPersonaFisica(buildPF(nomePF, cognomePF, codiceFiscalePF, indirizzoEmail));
		return output;
	}

	public static ProtDestinatarioType buildDestinatarioPF(String perConoscenza, String nomePF, String cognomePF,
			String codiceFiscalePF, String tipo, String indirizzoEmail) {
		ProtDestinatarioType output = buildDestinatario(perConoscenza, tipo, indirizzoEmail);
		output.setPersonaFisica(buildPF(nomePF, cognomePF, codiceFiscalePF, indirizzoEmail));
		return output;
	}

	private static AnaPersonaFisicaType buildPF(String nomePF, String cognomePF, String codiceFiscalePF, String indirizzoTelematico) {
		AnaPersonaFisicaType output = ofTypes.createAnaPersonaFisicaType();
		
		if (codiceFiscalePF != null) {
			output.setCodiceFiscale(codiceFiscalePF);
		}
		
		output.setCognome(cognomePF);
		output.setNome(nomePF);
		
		AllChiaveEsternaType chiaveEsterna = ofTypes.createAllChiaveEsternaType();
		chiaveEsterna.setValue(nomePF + "_" + cognomePF);
		output.setChiaveEsterna(chiaveEsterna);
		
		return output;
	}
	
	public static WkfDestinatarioFlussoType buildDestinatarioFlussoAUTPG(String perConoscenza, String denominazionePG, String tipo,
			String indirizzoTelematico) {
		WkfDestinatarioFlussoType output = buildDestinatarioFlussoAUT(perConoscenza, tipo, indirizzoTelematico);
		output.setPersonaGiuridica(buildPG(denominazionePG));
		return output;
	}

	public static ProtDestinatarioType buildDestinatarioPG(String perConoscenza, String denominazionePG, String tipo,
			String indirizzoTelematico) {
		ProtDestinatarioType output = buildDestinatario(perConoscenza, tipo, indirizzoTelematico);
		output.setPersonaGiuridica(buildPG(denominazionePG));
		return output;
	}

	private static AnaPersonaGiuridicaType buildPG(String denominazionePG) {
		AnaPersonaGiuridicaType anaPersonaGiuridicaType = ofTypes.createAnaPersonaGiuridicaType();
		anaPersonaGiuridicaType.setDenominazione(denominazionePG);
		
		AllChiaveEsternaType chiaveEsterna = ofTypes.createAllChiaveEsternaType();
		chiaveEsterna.setValue(denominazionePG);
		anaPersonaGiuridicaType.setChiaveEsterna(chiaveEsterna);
		
		return anaPersonaGiuridicaType;
	}
	
	public static WkfDestinatarioFlussoType buildDestinatarioEnteFlussoAUT(String codiceAmministrazione, String codiceAOO, String codiceUfficio, String descrizioneUfficio, 
			String nome, String cognome, String codiceFiscale) {
		WkfDestinatarioFlussoType output = buildDestinatarioEnteFlussoAUT(codiceAmministrazione, codiceAOO, codiceUfficio, descrizioneUfficio);
		AnaEnteEsternoType anaEnteEsternoType = output.getEnte();
		
		if(nome!=null && !nome.equals("")) {
			anaEnteEsternoType.setPersona(buildPF(nome, cognome, codiceFiscale, null));
			AllChiaveEsternaType chiaveEsternaUtente = ofTypes.createAllChiaveEsternaType();
			chiaveEsternaUtente.setValue(nome + "_" + cognome);
			anaEnteEsternoType.setChiaveEsterna(chiaveEsternaUtente);
		} else {
			AllChiaveEsternaType chiaveEsternaUfficio = ofTypes.createAllChiaveEsternaType();
			chiaveEsternaUfficio.setValue(codiceAOO + "_" + codiceUfficio);
			anaEnteEsternoType.setChiaveEsterna(chiaveEsternaUfficio);
		}
		
		return output;
	}
	
	public static ProtDestinatarioType buildDestinatarioEnte(String codiceAmministrazione, String codiceAOO, String codiceUfficio, String descrizioneUfficio, 
			String nome, String cognome, String codiceFiscale) {
		ProtDestinatarioType output = buildDestinatarioEnte(codiceAmministrazione, codiceAOO, codiceUfficio, descrizioneUfficio);
		AnaEnteEsternoType anaEnteEsternoType = output.getEnte();
		
		if(nome!=null && !nome.equals("")) {
			anaEnteEsternoType.setPersona(buildPF(nome, cognome, codiceFiscale, null));
			AllChiaveEsternaType chiaveEsternaUtente = ofTypes.createAllChiaveEsternaType();
			chiaveEsternaUtente.setValue(nome + "_" + cognome);
			anaEnteEsternoType.setChiaveEsterna(chiaveEsternaUtente);
		} else {
			AllChiaveEsternaType chiaveEsternaUfficio = ofTypes.createAllChiaveEsternaType();
			chiaveEsternaUfficio.setValue(codiceAOO + "_" + codiceUfficio);
			anaEnteEsternoType.setChiaveEsterna(chiaveEsternaUfficio);
		}
		
		return output;
	}
	
	

	public static DocumentoNpsDTO buildDocumentoToUploadToNps(String guidDoc, String contentType, String nomeFile,
			String descrizione, byte[] content, byte[] hash) throws IOException {
		DocumentoNpsDTO documentoNps = new DocumentoNpsDTO();
		documentoNps.setGuid(guidDoc);
		documentoNps.setContentType(contentType);
		documentoNps.setNomeFile(nomeFile);
		if (descrizione != null) {
			documentoNps.setDescrizione(descrizione);
		} else {
			documentoNps.setDescrizione(nomeFile);
		}
		if (content != null) {
			documentoNps.setInputStream(new ByteArrayInputStream(content));
		}
		if (hash != null) {
			documentoNps.setHash(hash);
		}
		return documentoNps;
	}

	public static RichiestaAggiungiDocumentiProtocolloType.Documenti buildDocumentoToAddToNps(String guid,
			String nomeDocumento, String descrizione, boolean isPrincipale, boolean daInviare,
			String idDocumentoOperazione, DocTipoOperazioneType tipoOperazioneEnum) {
		RichiestaAggiungiDocumentiProtocolloType.Documenti documento = ofMessages
				.createRichiestaAggiungiDocumentiProtocolloTypeDocumenti();
		documento.setIdDocumento(guid.toString().replace("{", "").replace("}", "").trim());
		documento.setIsPrincipale(isPrincipale);
		documento.setDaInviare(daInviare);
		if (descrizione != null) {
			documento.setDescrizione(descrizione);
		} else {
			documento.setDescrizione(nomeDocumento);
		}
		if ((tipoOperazioneEnum != null) && (idDocumentoOperazione != null)) {
			documento.setTipoOperazione(tipoOperazioneEnum);
			documento.setIdDocumentoOperazione(idDocumentoOperazione.replace("{", "").replace("}", "").trim());
		}
		return documento;
	}

	public static ProtSpedizioneType buildSpedizioneElettronico(String stato, Date dataSpedizione, String email, boolean isPEC, String codiceAoo, String denominazioneAoo)
			throws DatatypeConfigurationException {
		ProtSpedizioneType protSpedizioneType = ofTypes.createProtSpedizioneType();
		protSpedizioneType.setDataSpedizione(buildXmlGregorianCalendarFromDate(dataSpedizione));
		protSpedizioneType.setStatoSpedizione(stato);
		ProtSpedizioneType.MezzoSpedizione mezzoSpedizione = ofTypes.createProtSpedizioneTypeMezzoSpedizione();
		Elettronico elettronico = ofTypes.createProtSpedizioneTypeMezzoSpedizioneElettronico();
		OrgCasellaEmailType casella = ofTypes.createOrgCasellaEmailType();
		EmailIndirizzoEmailType indirizzoEmail = ofTypes.createEmailIndirizzoEmailType();
		indirizzoEmail.setDisplayName(email);
		indirizzoEmail.setEmail(email);
		OrgAooType aoo = ofTypes.createOrgAooType();
		aoo.setCodiceAOO(codiceAoo);
		aoo.setDenominazione(denominazioneAoo);
		casella.setIndirizzoEmail(indirizzoEmail);
		casella.setIsPEC(isPEC ? SI : NO);
		casella.setAoo(aoo);
		elettronico.setPostaElettronica(casella);
		mezzoSpedizione.setElettronico(elettronico);
		protSpedizioneType.setMezzoSpedizione(mezzoSpedizione);
		return protSpedizioneType;
	}
	
	public static ProtSpedizioneType buildSpedizioneCartaceo(String stato, Date dataSpedizione)
			throws DatatypeConfigurationException {
		ProtSpedizioneType protSpedizioneType = ofTypes.createProtSpedizioneType();
		protSpedizioneType.setDataSpedizione(buildXmlGregorianCalendarFromDate(dataSpedizione));
		protSpedizioneType.setStatoSpedizione(stato);
		ProtSpedizioneType.MezzoSpedizione mezzoSpedizione = ofTypes.createProtSpedizioneTypeMezzoSpedizione();
		AllCodiceDescrizioneType cartaceo = ofTypes.createAllCodiceDescrizioneType();
		cartaceo.setCodice("INVIO CARTACEO");
		cartaceo.setDescrizione("INVIO CARTACEO");
		mezzoSpedizione.setCartaceo(cartaceo);
		protSpedizioneType.setMezzoSpedizione(mezzoSpedizione);
		return protSpedizioneType;
	}

	public static XMLGregorianCalendar buildXmlGregorianCalendarFromDate(Date date)
			throws DatatypeConfigurationException {
		if (date != null) {
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(date);
			return DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
		}
		return null;
	}

	public static XMLGregorianCalendar buildXmlGregorianCalendarFromStringDate(String date)
			throws DatatypeConfigurationException {
		return buildXmlGregorianCalendarFromDate(parseDate(date));
	}

	private static Date parseDate(String s) {
		Date d = null;
		if (s == null) {
			return d;
		}
		String pattern = "";
		if (s.indexOf(':') >= 0) {
			if (s.length() > 20) {
				pattern = "yyyy-MM-dd HH:mm:ss.SSS";
			} else if (s.lastIndexOf(':') != s.indexOf(':')) {
				pattern = "dd/MM/yyyy HH:mm:ss";
			} else {
				pattern = "dd/MM/yyyy HH:mm";
			}
		} else {
			pattern = "dd/MM/yyyy";
		}
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		if ((s != null) && (s.length() > 0)) {
			try {
				d = sdf.parse(s);
			} catch (ParseException e) {
			}
		}
		return d;
	}
	
	public static RichiestaDownloadDocumentoType buildRichiestaDownloadDocument(String guid) {
		RichiestaDownloadDocumentoType output = ofMessages.createRichiestaDownloadDocumentoType();
		output.setIdDocumento(guid);
		return output;
	}
	
	public static ProtAssegnatarioType buildProtAssegnatarioType(OrgOrganigrammaType assegnatarioOrg, OrgOrganigrammaType protocollatoreOrg, 
			Date currentDate, boolean isDestinatarioCompetenza) throws DatatypeConfigurationException {
		
		ProtAssegnatarioType assegnazioneNps = ofTypes.createProtAssegnatarioType(); 
		assegnazioneNps.setAssegnatario(assegnatarioOrg);
		assegnazioneNps.setAssegnante(protocollatoreOrg);
		if (isDestinatarioCompetenza) {
			assegnazioneNps.setPerCompetenza(SI);
		} else {
			assegnazioneNps.setPerCompetenza(NO);
		}
		
		assegnazioneNps.setDataAssegnazione(buildXmlGregorianCalendarFromDate(currentDate));
		
		return assegnazioneNps;
	
	}
	
	public static RegTipologiaDocumentoType buildRegTipologiaDocumento(String codice, String descrizione) {
		RegTipologiaDocumentoType tipologiaDocumentale = ofTypes.createRegTipologiaDocumentoType();
		
		if (codice != null && !"".equals(codice)) {
			tipologiaDocumentale.setCodice(codice);
		}
		
		if (descrizione != null && !"".equals(descrizione)) {
			JAXBElement<String> descrizioneType = ofTypes.createRegTipologiaDocumentoTypeDescrizione(descrizione);
			tipologiaDocumentale.setDescrizione(descrizioneType);
		}
		
		return tipologiaDocumentale;
	}
	
	public static RegRegistroType buildRegRegistroType(String codiceAmministrazione, String denominazioneAmministrazione, String codiceAOO, String denominazioneAOO,
			String codiceRegistro, String descrizioneRegistro, AllTipoRegistroAusiliarioType tipoRegistro) {
		RegRegistroType registro = ofTypes.createRegRegistroType();
		OrgOrganigrammaAooType aoo = ofTypes.createOrgOrganigrammaAooType();
		
		OrgAmministrazioneType amministrazione = ofTypes.createOrgAmministrazioneType();
		amministrazione.setCodiceAmministrazione(codiceAmministrazione);
		amministrazione.setDenominazione(denominazioneAmministrazione);
		
		aoo.setAmministrazione(amministrazione);
		aoo.setCodiceAOO(codiceAOO);
		if (denominazioneAOO != null) {
			aoo.setDenominazione(denominazioneAOO);
		}
		registro.setAoo(aoo);
		
		AllCodiceDescrizioneType infoRegistro = ofTypes.createAllCodiceDescrizioneType();
		infoRegistro.setCodice(codiceRegistro);
		infoRegistro.setDescrizione(descrizioneRegistro);
		registro.setCodice(infoRegistro);
		
		registro.setTipoRegistro(tipoRegistro);
		
		return registro;
	}

	public static WkfDatiContestoProceduraleType buildContestoProcedurale(String identificativoMessaggioString, String codiceFlusso, 
			String codiceAmministrazioneString, String codiceAOOString, TIPOLOGIA metadatiEstesi) {
		WkfDatiContestoProceduraleType datiContestoProcedurale = ofTypes.createWkfDatiContestoProceduraleType();
		
		datiContestoProcedurale.setIdentificativoMessaggio(identificativoMessaggioString);
		
		ContestoProcedurale contestoProcedurale = new ContestoProcedurale();
		
		CodiceAmministrazione codiceAmministrazione = new CodiceAmministrazione();
		codiceAmministrazione.setContent(codiceAmministrazioneString);
		contestoProcedurale.setCodiceAmministrazione(codiceAmministrazione);
		
		CodiceAOO codiceAOO = new CodiceAOO();
		codiceAOO.setContent(codiceAOOString);
		contestoProcedurale.setCodiceAOO(codiceAOO);
		
		Identificativo identificativoMessaggio = new Identificativo();
		identificativoMessaggio.setContent(identificativoMessaggioString);
		contestoProcedurale.setIdentificativo(identificativoMessaggio);
				
		TipoContestoProcedurale tipoContestoProcedurale = new TipoContestoProcedurale();
		tipoContestoProcedurale.setContent(codiceFlusso);
		contestoProcedurale.setTipoContestoProcedurale(tipoContestoProcedurale);
		
		datiContestoProcedurale.setContestoProcedurale(contestoProcedurale );
		
		datiContestoProcedurale.setDatiEstesi(metadatiEstesi);
		
		return datiContestoProcedurale;
	}

	public static TIPOLOGIA createMetadatiEstesiFlusso(String tipologiaDocumento, List<MetaDatoAssociato> metadati) {
		TIPOLOGIA attrExt = ofTypesAttrExt.createTIPOLOGIA();
		attrExt.setNOME(tipologiaDocumento);
		attrExt.getMetadatoAssociato().addAll(metadati);
		return attrExt;
	}

	public static MetaDatoAssociato createMetadato(String codice, String valore) {
		MetaDatoAssociato metadato = ofTypesAttrExt.createMetaDatoAssociato();
		metadato.setCodice(codice);
		metadato.setValore(valore);
		return metadato;
	}
	
	public static ProtMetadatiType createProtMetadatiType(List<ProtMetadatoAssociatoType> metadatiAssociati) {
		ProtMetadatiType output = ofTypes.createProtMetadatiType();
		output.getMetadatoAssociato().addAll(metadatiAssociati);
		return output;
	}
	
	public static ProtMetadatoAssociatoType createProtMetadato(String codice, String valore) {
		ProtMetadatoAssociatoType metadato = ofTypes.createProtMetadatoAssociatoType();
		metadato.setCodice(codice);
		metadato.setValore(valore);
		return metadato;
	}
	
}
