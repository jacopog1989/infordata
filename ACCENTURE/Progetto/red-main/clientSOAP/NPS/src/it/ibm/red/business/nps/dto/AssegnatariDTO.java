package it.ibm.red.business.nps.dto;

import java.util.Date;

public class AssegnatariDTO {
	
	private AssegnanteDTO assegnante;
	
	private AssegnatarioDTO assegnatario;
	
	private String competenza;
	
	private Date dataAssegnazione;

	/**
	 * @return the assegnante
	 */
	public AssegnanteDTO getAssegnante() {
		return assegnante;
	}

	/**
	 * @param assegnante the assegnante to set
	 */
	public void setAssegnante(AssegnanteDTO assegnante) {
		this.assegnante = assegnante;
	}

	/**
	 * @return the assegnatario
	 */
	public AssegnatarioDTO getAssegnatario() {
		return assegnatario;
	}

	/**
	 * @param assegnatario the assegnatario to set
	 */
	public void setAssegnatario(AssegnatarioDTO assegnatario) {
		this.assegnatario = assegnatario;
	}

	/**
	 * @return the competenza
	 */
	public String getCompetenza() {
		return competenza;
	}

	/**
	 * @param competenza the competenza to set
	 */
	public void setCompetenza(String competenza) {
		this.competenza = competenza;
	}

	/**
	 * @return the dataAssegnazione
	 */
	public Date getDataAssegnazione() {
		return dataAssegnazione;
	}

	/**
	 * @param dataAssegnazione the dataAssegnazione to set
	 */
	public void setDataAssegnazione(Date dataAssegnazione) {
		this.dataAssegnazione = dataAssegnazione;
	}
	
}
