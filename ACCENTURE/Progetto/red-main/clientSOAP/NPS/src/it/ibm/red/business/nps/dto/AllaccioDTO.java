package it.ibm.red.business.nps.dto;

import java.util.Date;

public class AllaccioDTO {
	
	private Boolean risposta;
	
	private String stato;
	
	private String idProtocollo;
	
	private String oggetto;
	
	private Date dataRegistrazione;
	
	private int numeroProtocollo;
	
	private String annoProtocollo;
	

	public Boolean getRisposta() {
		return this.risposta;
	}

	public void setRisposta(Boolean risposta) {
		this.risposta = risposta;
	}

	public String getStato() {
		return this.stato;
	}

	public void setStato(String stato) {
		this.stato = stato;
	}

	public String getIdProtocollo() {
		return this.idProtocollo;
	}

	public void setIdProtocollo(String idProtocollo) {
		this.idProtocollo = idProtocollo;
	}

	public String getOggetto() {
		return this.oggetto;
	}

	public void setOggetto(String oggetto) {
		this.oggetto = oggetto;
	}

	public Date getDataRegistrazione() {
		return this.dataRegistrazione;
	}

	public void setDataRegistrazione(Date dataRegistrazione) {
		this.dataRegistrazione = dataRegistrazione;
	}

	public int getNumeroProtocollo() {
		return this.numeroProtocollo;
	}

	public void setNumeroProtocollo(int numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}

	public String getAnnoProtocollo() {
		return this.annoProtocollo;
	}

	public void setAnnoProtocollo(String annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}
}
