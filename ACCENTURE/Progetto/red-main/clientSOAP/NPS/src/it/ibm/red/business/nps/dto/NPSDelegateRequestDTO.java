package it.ibm.red.business.nps.dto;

import java.util.Date;

public class NPSDelegateRequestDTO {
	private String cls;
	private String method;
	private byte[] parameters;
	private Date dataRichiesta;
	private Long idUtente;
	private String finalizerFQN;

	public String getMethod() {
		return this.method;
	}

	public String getFinalizerFQN() {
		return this.finalizerFQN;
	}

	public void setFinalizerFQN(String finalizerFQN) {
		this.finalizerFQN = finalizerFQN;
	}

	public String getCls() {
		return this.cls;
	}

	public void setCls(String cls) {
		this.cls = cls;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public byte[] getParameters() {
		return this.parameters;
	}

	public void setParameters(byte[] parameters) {
		this.parameters = parameters;
	}

	public Date getDataRichiesta() {
		return this.dataRichiesta;
	}

	public void setDataRichiesta(Date dataRichiesta) {
		this.dataRichiesta = dataRichiesta;
	}

	public Long getIdUtente() {
		return this.idUtente;
	}

	public void setIdUtente(Long idUtente) {
		this.idUtente = idUtente;
	}
}
