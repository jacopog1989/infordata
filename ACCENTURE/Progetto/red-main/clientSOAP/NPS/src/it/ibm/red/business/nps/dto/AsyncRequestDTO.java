package it.ibm.red.business.nps.dto;

import java.util.Date;

public class AsyncRequestDTO<T> {
	private int id_nar;
	private T input;
	private T xml;
	private String fqn_input;
	private Date dataCreazione;
	private Date dataLavorazione;
	private int priority;
	private int partition;
	private String methodName;
	private String className;
	private String fqn_finalizer;
	private int stato;
	private int id_nar_execute_required;
	private boolean esitoOperazione;
	private String exception;
	private String numeroAnnoProtocollo;
	private String numeroAnnoProtocolloEmergenza;
	private String classNameInput;
	private String idDocumento;

	public String getFqn_input() {
		return this.fqn_input;
	}

	public void setFqn_input(String fqn_input) {
		this.fqn_input = fqn_input;
	}

	public Date getDataCreazione() {
		return this.dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	public Date getDataLavorazione() {
		return this.dataLavorazione;
	}

	public void setDataLavorazione(Date dataLavorazione) {
		this.dataLavorazione = dataLavorazione;
	}

	public int getPriority() {
		return this.priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public int getPartition() {
		return this.partition;
	}

	public void setPartition(int partition) {
		this.partition = partition;
	}

	public String getMethodName() {
		return this.methodName;
	}

	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}

	public String getClassName() {
		return this.className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getFqn_finalizer() {
		return this.fqn_finalizer;
	}

	public void setFqn_finalizer(String fqn_finalizer) {
		this.fqn_finalizer = fqn_finalizer;
	}

	public int getStato() {
		return this.stato;
	}

	public void setStato(int stato) {
		this.stato = stato;
	}

	public int getId_nar() {
		return this.id_nar;
	}

	public void setId_nar(int id_nar) {
		this.id_nar = id_nar;
	}

	public T getInput() {
		return (T) this.input;
	}

	public void setInput(T input) {
		this.input = input;
	}

	public T getXml() {
		return (T) this.xml;
	}

	public void setXml(T xml) {
		this.xml = xml;
	}

	public int getId_nar_execute_required() {
		return this.id_nar_execute_required;
	}

	public void setId_nar_execute_required(int id_nar_execute_required) {
		this.id_nar_execute_required = id_nar_execute_required;
	}

	public boolean isEsitoOperazione() {
		return this.esitoOperazione;
	}

	public void setEsitoOperazione(boolean esitoOperazione) {
		this.esitoOperazione = esitoOperazione;
	}

	public String getException() {
		return this.exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public String getNumeroAnnoProtocollo() {
		return this.numeroAnnoProtocollo;
	}

	public void setNumeroAnnoProtocollo(String numeroAnnoProtocollo) {
		this.numeroAnnoProtocollo = numeroAnnoProtocollo;
	}

	public String getNumeroAnnoProtocolloEmergenza() {
		return numeroAnnoProtocolloEmergenza;
	}

	public void setNumeroAnnoProtocolloEmergenza(String numeroAnnoProtocolloEmergenza) {
		this.numeroAnnoProtocolloEmergenza = numeroAnnoProtocolloEmergenza;
	}

	public String getClassNameInput() {
		return classNameInput;
	}

	public void setClassNameInput(String classNameInput) {
		this.classNameInput = classNameInput;
	}

	public String getIdDocumento() {
		return idDocumento;
	}

	public void setIdDocumento(String idDocumento) {
		this.idDocumento = idDocumento;
	}
	
}
