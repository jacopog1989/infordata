package it.ibm.red.business.nps.exceptions;

public class ProtocolloGialloException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	
	private String message;

	public ProtocolloGialloException(Exception exception) {
		super(exception);
	}

	public ProtocolloGialloException(String msg) {
		super(msg);
	}

	public ProtocolloGialloException(String msg, Exception e) {
		super(e);
		this.message = msg;
	}

	public String getMessage() {
		if (this.message != null) {
			return this.message;
		}
		return super.getMessage();
	}
}