package it.ibm.red.business.nps.enums;

public enum PriorityEnum {
	ALTA(Integer.valueOf(1)), MEDIA(Integer.valueOf(2)), BASSA(Integer.valueOf(3));

	private Integer value;

	public Integer getValue() {
		return this.value;
	}

	private PriorityEnum(Integer inValue) {
		this.value = inValue;
	}
}
