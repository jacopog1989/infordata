package it.ibm.red.business.nps;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.soap.SOAPBinding;

import it.ibm.red.business.nps.builder.Builder;
import it.ibm.red.business.nps.builder.Director;
import it.ibm.red.business.nps.dto.DocumentoNpsDTO;
import it.ibm.red.business.nps.dto.OperatoreDTO;
import it.ibm.red.business.nps.exceptions.BusinessDelegateRuntimeException;
import it.ibm.red.business.nps.exceptions.ProtocolloGialloException;
import nps.v1.it.gov.mef.servizi.interfaccianps.InterfacciaDocumentale;
import nps.v1.it.gov.mef.servizi.interfaccianps.InterfacciaDocumentale_Service;
import nps.v1.it.gov.mef.servizi.interfaccianps.InterfacciaProtocollo;
import nps.v1.it.gov.mef.servizi.interfaccianps.InterfacciaProtocollo_Service;
import nps.v1.it.gov.mef.servizi.interfaccianps.InterfacciaRegistrazioneAusiliaria;
import nps.v1.it.gov.mef.servizi.interfaccianps.InterfacciaRegistrazioneAusiliaria_Service;
import nps.v2.it.gov.mef.servizi.interfaccianps.InterfacciaFlussoAUT;
import nps.v2.it.gov.mef.servizi.interfaccianps.InterfacciaFlussoAUT_Service;
import nps.v2.it.gov.mef.servizi.interfaccianps.InterfacciaPosta;
import nps.v2.it.gov.mef.servizi.interfaccianps.InterfacciaPosta_Service;
import npsmessages.v1.it.gov.mef.RichiestaAggiungiAllaccioProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaAggiungiAssegnatarioProtocolloEntrataType;
import npsmessages.v1.it.gov.mef.RichiestaAggiungiDestinatarioProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaAggiungiDocumentiProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaAnnullamentoProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaCambiaStatoProtocolloEntrataType;
import npsmessages.v1.it.gov.mef.RichiestaCambiaStatoProtocolloUscitaType;
import npsmessages.v1.it.gov.mef.RichiestaCollegaRegistrazioneAusiliariaProtocolloIngressoType;
import npsmessages.v1.it.gov.mef.RichiestaCollegaRegistrazioneAusiliariaProtocolloUscitaType;
import npsmessages.v1.it.gov.mef.RichiestaCreateProtocolloEntrataType;
import npsmessages.v1.it.gov.mef.RichiestaCreateProtocolloUscitaType;
import npsmessages.v1.it.gov.mef.RichiestaCreateRegistrazioneAusiliariaType;
import npsmessages.v1.it.gov.mef.RichiestaDownloadDocumentoType;
import npsmessages.v1.it.gov.mef.RichiestaGetProtocolloEntrataType;
import npsmessages.v1.it.gov.mef.RichiestaGetProtocolloUscita;
import npsmessages.v1.it.gov.mef.RichiestaInviaAttoType;
import npsmessages.v1.it.gov.mef.RichiestaInviaMessaggioPostaType;
import npsmessages.v1.it.gov.mef.RichiestaModificaSpedizioneDestinatarioProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaReportProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaReportRegistroGiornalieroType;
import npsmessages.v1.it.gov.mef.RichiestaRicercaProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaRicercaRegistrazioneAusiliariaType;
import npsmessages.v1.it.gov.mef.RichiestaRimuoviAllaccioProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaRimuoviDestinatarioProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaRimuoviDocumentoProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaSpedisciProtocolloUscitaType;
import npsmessages.v1.it.gov.mef.RichiestaUpdateDatiProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaUploadDocumentoType;
import npsmessages.v1.it.gov.mef.RispostaAggiungiAllaccioProtocolloType;
import npsmessages.v1.it.gov.mef.RispostaAggiungiAssegnatarioProtocolloEntrataType;
import npsmessages.v1.it.gov.mef.RispostaAggiungiDestinatarioProtocolloType;
import npsmessages.v1.it.gov.mef.RispostaAggiungiDocumentiProtocolloType;
import npsmessages.v1.it.gov.mef.RispostaAnnullamentoProtocolloType;
import npsmessages.v1.it.gov.mef.RispostaCambiaStatoProtocolloEntrataType;
import npsmessages.v1.it.gov.mef.RispostaCambiaStatoProtocolloUscitaType;
import npsmessages.v1.it.gov.mef.RispostaCollegaRegistrazioneAusiliariaProtocolloIngressoType;
import npsmessages.v1.it.gov.mef.RispostaCollegaRegistrazioneAusiliariaProtocolloUscitaType;
import npsmessages.v1.it.gov.mef.RispostaCreateProtocolloEntrataType;
import npsmessages.v1.it.gov.mef.RispostaCreateProtocolloUscitaType;
import npsmessages.v1.it.gov.mef.RispostaCreateRegistrazioneAusiliariaType;
import npsmessages.v1.it.gov.mef.RispostaDownloadDocumentoType;
import npsmessages.v1.it.gov.mef.RispostaGetProtocolloEntrataType;
import npsmessages.v1.it.gov.mef.RispostaGetProtocolloUscitaType;
import npsmessages.v1.it.gov.mef.RispostaInviaAttoType;
import npsmessages.v1.it.gov.mef.RispostaInviaMessaggioPostaType;
import npsmessages.v1.it.gov.mef.RispostaModificaSpedizioneDestinatarioProtocolloType;
import npsmessages.v1.it.gov.mef.RispostaReportProtocolloType;
import npsmessages.v1.it.gov.mef.RispostaReportRegistroGiornalieroType;
import npsmessages.v1.it.gov.mef.RispostaRicercaProtocolloType;
import npsmessages.v1.it.gov.mef.RispostaRicercaRegistrazioneAusiliariaType;
import npsmessages.v1.it.gov.mef.RispostaRimuoviAllaccioProtocolloType;
import npsmessages.v1.it.gov.mef.RispostaRimuoviDestinatarioProtocolloType;
import npsmessages.v1.it.gov.mef.RispostaRimuoviDocumentoProtocolloType;
import npsmessages.v1.it.gov.mef.RispostaSpedisciProtocolloUscitaType;
import npsmessages.v1.it.gov.mef.RispostaUpdateDatiProtocolloType;
import npsmessages.v1.it.gov.mef.RispostaUploadDocumentoType;
import npstypes.v1.it.gov.mef.AllTipoRegistroAusiliarioType;
import npstypes.v1.it.gov.mef.DocDocumentoContentType;
import npstypes.v1.it.gov.mef.DocTipoOperazioneType;
import npstypes.v1.it.gov.mef.DocValidazioneFirmaType;
import npstypes.v1.it.gov.mef.OrgOrganigrammaType;
import npstypes.v1.it.gov.mef.ProtAllaccioProtocolloType;
import npstypes.v1.it.gov.mef.ProtAssegnatarioType;
import npstypes.v1.it.gov.mef.ProtDestinatarioType;
import npstypes.v1.it.gov.mef.ProtMetadatiType;
import npstypes.v1.it.gov.mef.ProtMittenteType;
import npstypes.v1.it.gov.mef.ProtTipoDettaglioEstrazioneType;
import npstypes.v1.it.gov.mef.ProtTipoProtocolloType;
import npstypes.v1.it.gov.mef.WkfDestinatarioFlussoType;
import npstypesestesi.v1.it.gov.mef.TIPOLOGIA;

public class BusinessDelegate {

	private static Map<String,NPSHeaderFactory> headersFactoryAooMap;
	
	private static InterfacciaProtocollo proxyNPSProtocollo;
	
	private static InterfacciaDocumentale proxyNPSDocumentale;
	
	private static InterfacciaRegistrazioneAusiliaria proxyNPSRegistrazioneAusiliaria;
	
	private static InterfacciaFlussoAUT proxyNPSFlussoAUT;
	
	private static InterfacciaPosta proxyNPSPosta;
	

	//Handler per intercettare le chiamate effettuate verso NPS
//	static class NPSWSClientHandler implements SOAPHandler<SOAPMessageContext> {
// 
//		private PrintStream out = System.out;
//
//		    public Set<QName> getHeaders() {
//		        return null;
//		    }
//
//		    public boolean handleMessage(SOAPMessageContext smc) {
//		        logToSystemOut(smc);
//		        return true;
//		    }
//
//		    public boolean handleFault(SOAPMessageContext smc) {
//		        logToSystemOut(smc);
//		        return true;
//		    }
//
////		    // nothing to clean up
//		    public void close(MessageContext messageContext) {
//		    }
////
////		    /*
////		     * Check the MESSAGE_OUTBOUND_PROPERTY in the context
////		     * to see if this is an outgoing or incoming message.
////		     * Write a brief message to the print stream and
////		     * output the message. The writeTo() method can throw
////		     * SOAPException or IOException
////		     */
//		    private void logToSystemOut(SOAPMessageContext smc) {
//		        Boolean outboundProperty = (Boolean)
//		            smc.get (MessageContext.MESSAGE_OUTBOUND_PROPERTY);
//
//		        if (outboundProperty.booleanValue()) {
//		            out.println("\nOutbound message:");
//		        } else {
//		            out.println("\nInbound message:");
//		        }
//
//		        SOAPMessage message = smc.getMessage();
//		        try {
//		            message.writeTo(out);
//		            out.println("");   // just to add a newline
//		        } catch (Exception e) {
//		            out.println("Exception in handler: " + e);
//		        }
//		    }
//
//	}

	public static void init(String endPointProtocollo, String endPointDocumentale, String endPointRegistrazioneAusiliaria, 
			String endPointFlussoAUT, String endPointPosta, Map<String,NPSHeaderFactory> headersAooMap) {

		try {
			proxyNPSProtocollo = (new InterfacciaProtocollo_Service(new URL(endPointProtocollo))).getInterfacciaProtocolloSOAPPort();
//			Serve per intercettare le chiamate SOAP effettuate verso NPS
//			BindingProvider bp = (BindingProvider) proxyNPSProtocollo;
//			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endPointProtocollo);
//			List<Handler> handlerList = bp.getBinding().getHandlerChain();
//		    handlerList.add(new NPSWSClientHandler());
//		    bp.getBinding().setHandlerChain(handlerList);
		    
			proxyNPSDocumentale = (new InterfacciaDocumentale_Service(new URL(endPointDocumentale))).getInterfacciaDocumentaleSOAPPort();
			BindingProvider bp = (BindingProvider) proxyNPSDocumentale;
			SOAPBinding binding = (SOAPBinding) bp.getBinding();
			binding.setMTOMEnabled(true);
			
			proxyNPSRegistrazioneAusiliaria = 
					(new InterfacciaRegistrazioneAusiliaria_Service(new URL(endPointRegistrazioneAusiliaria))).getInterfacciaRegistrazioneAusiliariaSOAPPort();
			
			proxyNPSFlussoAUT = 
					(new InterfacciaFlussoAUT_Service(new URL(endPointFlussoAUT))).getInterfacciaFlussoAUTSOAPPort();
			
			proxyNPSPosta = 
					(new InterfacciaPosta_Service(new URL(endPointPosta))).getInterfacciaPostaSOAPPort();
			
			 
 			headersFactoryAooMap = headersAooMap;
			
		} catch (MalformedURLException e) {
			throw new BusinessDelegateRuntimeException("Errore durante la creazione degli stub per la chiamata ai servizi NPS", e);
		}

	}

	public static class Destinatario {
		public static RispostaAggiungiDestinatarioProtocolloType aggiungiDestinatario(Integer idAoo, OrgOrganigrammaType operatore, String idProtocollo, ProtDestinatarioType[] destinatari) {
			try {
				String messageId = UUID.randomUUID().toString();
				
				RichiestaAggiungiDestinatarioProtocolloType input = Director.constructAggiungiDestinatarioProtocolloInput(operatore, idProtocollo, destinatari);
				
				return BusinessDelegate.proxyNPSProtocollo.aggiungiDestinatarioProtocollo(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaAggiungiDestinatarioProtocolloType aggiungiDestinatario(Integer idAoo, String messageId,
				RichiestaAggiungiDestinatarioProtocolloType input) {
			try {
				return BusinessDelegate.proxyNPSProtocollo.aggiungiDestinatarioProtocollo(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaRimuoviDestinatarioProtocolloType rimuovi(Integer idAoo, OperatoreDTO operatore, String idProtocollo, String idDestinatario) {
			try {
				String messageId = UUID.randomUUID().toString();
				RichiestaRimuoviDestinatarioProtocolloType input = Director.constructRimuoviDestinatarioProtocolloInput(
						operatore.getIdOperatore(), operatore.getCognome(), operatore.getCodiceAmministrazione(),
						operatore.getDenominazioneAmministrazione(), operatore.getDenominazioneAOO(),
						operatore.getCodiceAOO(), operatore.getDenominazioneUO(), operatore.getCodiceUO(),
						operatore.getNome(), idProtocollo, idDestinatario, operatore.getIdUO(), operatore.getCodiceFiscale());
				return BusinessDelegate.proxyNPSProtocollo.rimuoviDestinatarioProtocollo(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaRimuoviDestinatarioProtocolloType rimuovi(Integer idAoo, String messageId, RichiestaRimuoviDestinatarioProtocolloType input) {
			try {
				return BusinessDelegate.proxyNPSProtocollo.rimuoviDestinatarioProtocollo(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaModificaSpedizioneDestinatarioProtocolloType modifica(Integer idAoo, OrgOrganigrammaType operatoreOrg,
				String idProtocollo, String statoSpedizione, Date dataSpedizione, String idDestinatario, String newEmailDestinatario) {
			try {
				String messageId = UUID.randomUUID().toString();
				
				RichiestaModificaSpedizioneDestinatarioProtocolloType input = Director.constructModificaDestinatarioProtocolloInput(operatoreOrg, idProtocollo, statoSpedizione, 
						dataSpedizione, idDestinatario, newEmailDestinatario);
				
				return BusinessDelegate.proxyNPSProtocollo.modificaSpedizioneDestinatarioProtocollo(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaModificaSpedizioneDestinatarioProtocolloType modifica(Integer idAoo, String messageId, RichiestaModificaSpedizioneDestinatarioProtocolloType input) {
			try {
				return BusinessDelegate.proxyNPSProtocollo.modificaSpedizioneDestinatarioProtocollo(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}
	}

	public static class Protocollo {
		public static RispostaSpedisciProtocolloUscitaType spedisciProtocolloUscita(Integer idAoo, String messageId, RichiestaSpedisciProtocolloUscitaType input) {
			try {
				return BusinessDelegate.proxyNPSProtocollo.spedisciProtocolloUscita(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}
		
		public static RispostaSpedisciProtocolloUscitaType spedisciProtocolloUscita(Integer idAoo, OrgOrganigrammaType operatore, String idProtocollo, String dataSpedizione, String oggettoMessaggio, String corpoMessaggio) {
			try {
				String messageId = UUID.randomUUID().toString();
				
				RichiestaSpedisciProtocolloUscitaType input = Director.constructSpedisciProtocolloUscita(operatore, idProtocollo, dataSpedizione, oggettoMessaggio, corpoMessaggio);
				
				return BusinessDelegate.proxyNPSProtocollo.spedisciProtocolloUscita(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaCambiaStatoProtocolloEntrataType cambiaStatoProtEntrata(Integer idAoo, OrgOrganigrammaType operatore, String idProtocollo, String stato) {
			try {
				String messageId = UUID.randomUUID().toString();
				RichiestaCambiaStatoProtocolloEntrataType input = Director.constructCambiaStatoProtocolloEntrataInput(operatore, idProtocollo, stato, new Date());
				return BusinessDelegate.proxyNPSProtocollo.cambiaStatoProtocolloEntrata(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaCambiaStatoProtocolloEntrataType cambiaStatoProtEntrata(Integer idAoo, String messageId, RichiestaCambiaStatoProtocolloEntrataType input) {
			try {
				return BusinessDelegate.proxyNPSProtocollo.cambiaStatoProtocolloEntrata(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}
		
		public static RispostaCambiaStatoProtocolloUscitaType cambiaStatoProtUscita(Integer idAoo, OrgOrganigrammaType operatore, String idProtocollo, String stato) {
			try {
				String messageId = UUID.randomUUID().toString();
				
				RichiestaCambiaStatoProtocolloUscitaType input = Director.constructCambiaStatoProtocolloUscitaInput(operatore, idProtocollo, stato, new Date());
				
				return BusinessDelegate.proxyNPSProtocollo.cambiaStatoProtocolloUscita(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaCambiaStatoProtocolloUscitaType cambiaStatoProtUscita(Integer idAoo, String messageId, RichiestaCambiaStatoProtocolloUscitaType input) {
			try {
				return BusinessDelegate.proxyNPSProtocollo.cambiaStatoProtocolloUscita(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaAnnullamentoProtocolloType annullamento(Integer idAoo, String idProtocollo, String motivoAnnullamento, Date dataAnnullamento, 
				String provvAnnullamento, OrgOrganigrammaType operatore) {
			try {
				String messageId = UUID.randomUUID().toString();
				
				RichiestaAnnullamentoProtocolloType input = Director.constructAnnullamentoProtocolloInput(idProtocollo, motivoAnnullamento, dataAnnullamento, provvAnnullamento, operatore, new Date());
				
				return BusinessDelegate.proxyNPSProtocollo.annullamentoProtocollo(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeaderSuperUser(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaAnnullamentoProtocolloType annullamento(Integer idAoo, String messageId, RichiestaAnnullamentoProtocolloType input) {
			try {
				return BusinessDelegate.proxyNPSProtocollo.annullamentoProtocollo(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeaderSuperUser(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaRicercaProtocolloType ricerca(Integer idAoo, Short numeroRisultati, String acl, Short anno,
				Boolean isAnnullato, Boolean isCompleto, Boolean isRiservato, String oggetto, String sistemaProduttore,
				String stato, ProtTipoProtocolloType tipoProtocollo, String statoSpedizione,
				String descrizioneTipologiaDocumentale, String codiceTipologiaDocumentale,
				String descrizioneVoceTitolario, String codiceVoceTitolario, Integer numeroRegistrazioneA,
				Integer numeroRegistrazioneDa, String codiceRegistro, Date dataAnnullamentoA, Date dataAnnullamentoDa,
				Date dataRegistrazioneDa, String codiceAmministrazione, String idUtente,
				String denominazioneAmministrazione, String nome, String cognome, String denominazioneRegistro,
				Date dataRegistrazioneA, String codiceAmministrazioneAssegnante, String nomeAssegnante,
				String idOperatoreAssegnante, String cognomeAssegnante, String codiceAOOAssegnante,
				String denominazioneAOOAssegnante, String denominazioneAmministrazioneAssegnante,
				String codiceUOAssegnante, String denominazioneUOAssegnante, String cognomeAssegnatario,
				String idOperatoreAssegnatario, String nomeAssegnatario, String codiceAmministrazioneAssegnatario,
				String denominazioneAOOAssegnatario, String codiceUOAssegnatario, String codiceAOOAssegnatario,
				String denominazioneUOAssegnatario, String denominazioneAmministrazioneAssegnatario,
				String codiceMezzoRicezione, Integer numeroRegistrazioneMittenteA, Date dataRicezioneA,
				String descrizioneMezzoRicezione, Date dataRicezioneDa, Integer numeroRegistrazioneMittenteDa,
				Date dataProtocolloMittenteA, Date dataProtocolloMittenteDa, Date dataSpedizioneDa,
				String codiceAOOPostaElettronica, String denominazioneUOPostaElettronica,
				String denominazioneAOOPostaElettronica, Date dataSpedizioneA, String destinatarioSpedizione,
				String displayNameEmailSpedizione, String codiceUOPostaElettronica,
				String descrizioneMezzoSpedizioneCartaceo, String codiceMezzoSpedizioneCartaceo, String emailSpedizione,
				String mittente, ProtTipoDettaglioEstrazioneType[] tipoDettaglioElencoEnum, String idUO, String codiceFiscale,
				boolean ricercaConAdmin) {
			try {
				String messageId = UUID.randomUUID().toString();
				
				RichiestaRicercaProtocolloType input = Director.constructRicercaProtocolloInput(numeroRisultati,
						acl, anno, isAnnullato, isCompleto, isRiservato, oggetto, sistemaProduttore, stato,
						tipoProtocollo, statoSpedizione, descrizioneTipologiaDocumentale, codiceTipologiaDocumentale,
						descrizioneVoceTitolario, codiceVoceTitolario, numeroRegistrazioneA, numeroRegistrazioneDa,
						codiceAOOPostaElettronica, codiceRegistro, dataAnnullamentoA, dataAnnullamentoDa,
						dataRegistrazioneDa, codiceAmministrazione, idUtente, denominazioneAmministrazione,
						denominazioneUOPostaElettronica, nome, denominazioneAOOPostaElettronica, cognome,
						codiceUOPostaElettronica, denominazioneRegistro, dataRegistrazioneA,
						codiceAmministrazioneAssegnante, nomeAssegnante, idOperatoreAssegnante, cognomeAssegnante,
						codiceAOOAssegnante, denominazioneAOOAssegnante, denominazioneAmministrazioneAssegnante,
						codiceUOAssegnante, denominazioneUOAssegnante, cognomeAssegnatario, idOperatoreAssegnatario,
						nomeAssegnatario, codiceAmministrazioneAssegnatario, denominazioneAOOAssegnatario,
						codiceUOAssegnatario, codiceAOOAssegnatario, denominazioneUOAssegnatario,
						denominazioneAmministrazioneAssegnatario, codiceMezzoRicezione, numeroRegistrazioneMittenteA,
						dataRicezioneA, descrizioneMezzoRicezione, dataRicezioneDa, numeroRegistrazioneMittenteDa,
						dataProtocolloMittenteA, dataProtocolloMittenteDa, dataSpedizioneDa, dataSpedizioneA,
						destinatarioSpedizione, displayNameEmailSpedizione, descrizioneMezzoSpedizioneCartaceo,
						codiceMezzoSpedizioneCartaceo, emailSpedizione, mittente, tipoDettaglioElencoEnum, idUO,
						codiceFiscale);
				
				String headerKeyMap = ricercaConAdmin ? idAoo+"_Admin" : ""+idAoo;
				
				return BusinessDelegate.proxyNPSProtocollo.ricercaProtocollo(BusinessDelegate.headersFactoryAooMap.get(headerKeyMap).createHeader(messageId), input);
				  
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaRicercaProtocolloType ricerca(Integer idAoo, String messageId, RichiestaRicercaProtocolloType input) {
			try {
				return BusinessDelegate.proxyNPSProtocollo.ricercaProtocollo(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaCreateProtocolloEntrataType createEntrataPF(Integer idAoo, Date dataRegistrazione,
				Integer numeroRegistrazione, String codiceRegistro, String denominazioneAmministrazione,
				String denominazioneRegistro, String denominazioneAOO, ProtTipoProtocolloType tipoProtocollo,
				ProtAllaccioProtocolloType[] cAllacciati, ProtAssegnatarioType[] cAssegnatari,
				ProtDestinatarioType[] cDestinatari, String acl, OrgOrganigrammaType protocollatoreOrg,
				Boolean isRiservato, String oggetto, String stato, String idProtocollo, String codiceVoceTitolario, String descrizioneVoceTitolario,
				String descrizioneTipologia, String codiceTipologia, String codiceAmministrazione, String note,	String tipo, String codiceAOO, 
				String value, ProtMittenteType mittente, ProtMetadatiType metadatiAssociati, String idDocumentoMessaggioPostaNps) {
			try {
				String messageId = UUID.randomUUID().toString();
				
				RichiestaCreateProtocolloEntrataType input = Director.constructCreateProtocolloEntrataPFInput(
						dataRegistrazione, numeroRegistrazione, codiceRegistro, denominazioneAmministrazione,
						denominazioneRegistro, codiceAmministrazione, denominazioneAOO, codiceAOO, tipoProtocollo,
						cAllacciati, cAssegnatari, cDestinatari, acl, protocollatoreOrg, isRiservato, oggetto, stato,
						idProtocollo, codiceVoceTitolario, descrizioneVoceTitolario, descrizioneTipologia,
						codiceTipologia, mittente, metadatiAssociati, idDocumentoMessaggioPostaNps);
				
				return BusinessDelegate.proxyNPSProtocollo.createProtocolloEntrata(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaCreateProtocolloEntrataType createEntrataPF(String messageId, RichiestaCreateProtocolloEntrataType input) {
			return null;
		}

		public static RispostaCreateProtocolloUscitaType createUscitaPF(Integer idAoo, Date dataRegistrazione,
				Integer numeroRegistrazione, String codiceRegistro, String denominazioneAmministrazione,
				String denominazioneRegistro, String codiceAmministrazione, String denominazioneAOO, String codiceAoo,
				ProtTipoProtocolloType tipoProtocollo, ProtAllaccioProtocolloType[] cAllacciati,
				ProtDestinatarioType[] cDestinatari, String idProtocollo, Boolean isRiservato,
				String stato, String oggetto, String acl,
				OrgOrganigrammaType protocollatoreOrg, String codiceVoceTitolario, String descrizioneVoceTitolario,
				String descrizioneTipologia, String codiceTipologia, boolean risposta, String oggettoRisposta,
				String dataRegistrazioneRisposta, ProtTipoProtocolloType tipoProtocolloRisposta,
				String idProtocolloRisposta, String numeroRegistrazioneRisposta, ProtMittenteType mittente,
				ProtMetadatiType metadatiAssociati) {
			
			String messageId = "";
			RichiestaCreateProtocolloUscitaType input = null;
			try {
				messageId = UUID.randomUUID().toString();
				
				input = Director.constructCreateProtocolloUscitaPFInput(
						dataRegistrazione, numeroRegistrazione, codiceRegistro, denominazioneAmministrazione,
						denominazioneRegistro, codiceAmministrazione, denominazioneAOO, codiceAoo, tipoProtocollo,
						cAllacciati, cDestinatari, idProtocollo, isRiservato, stato, oggetto, acl, protocollatoreOrg,
						codiceVoceTitolario, descrizioneVoceTitolario, descrizioneTipologia, codiceTipologia, risposta,
						oggettoRisposta, dataRegistrazioneRisposta, tipoProtocolloRisposta, idProtocolloRisposta,
						numeroRegistrazioneRisposta, mittente, metadatiAssociati);
				 
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
			
			try { 
				return BusinessDelegate.proxyNPSProtocollo.createProtocolloUscita(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new ProtocolloGialloException("##NPSFAULT## - Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaCreateProtocolloUscitaType createUscitaPF(Integer idAoo, String messageId,
				RichiestaCreateProtocolloUscitaType input) {
			try {
				return BusinessDelegate.proxyNPSProtocollo.createProtocolloUscita(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaGetProtocolloUscitaType getUscita(Integer idAoo, String idProtocollo, ProtTipoDettaglioEstrazioneType[] dettagli) {
			try {
				String messageId = UUID.randomUUID().toString();
				
				RichiestaGetProtocolloUscita input = Director.constructGetProtocolloUscitaInput(idProtocollo, dettagli);
				
				return BusinessDelegate.proxyNPSProtocollo.getProtocolloUscita(BusinessDelegate.headersFactoryAooMap.get(idAoo+"_Admin").createHeader(messageId), input);
				 
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}
		
		public static RispostaGetProtocolloUscitaType getUscita(Integer idAoo, Date dataRegistrazione, String idProtocollo,
				int numeroRegistrazione, String codiceRegistro, String codiceAOO, String codiceAmministrazione,
				String denominazioneRegistro, String denominazioneAmministrazione, String denominazioneAOO,
				ProtTipoDettaglioEstrazioneType[] dettagli) {
			try {
				String messageId = UUID.randomUUID().toString();
				
				RichiestaGetProtocolloUscita input = Director.constructGetProtocolloUscitaInput(
						dataRegistrazione, idProtocollo, numeroRegistrazione, codiceRegistro, codiceAOO,
						codiceAmministrazione, denominazioneRegistro, denominazioneAmministrazione, denominazioneAOO,
						dettagli);
				
				return BusinessDelegate.proxyNPSProtocollo.getProtocolloUscita(BusinessDelegate.headersFactoryAooMap.get(idAoo+"_Admin").createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaGetProtocolloUscitaType getUscita(Integer idAoo, String messageId, RichiestaGetProtocolloUscita input) {
			try {
				return BusinessDelegate.proxyNPSProtocollo.getProtocolloUscita(BusinessDelegate.headersFactoryAooMap.get(idAoo+"_Admin").createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaGetProtocolloEntrataType getEntrata(Integer idAoo, String idProtocollo,
				ProtTipoDettaglioEstrazioneType[] dettagli) {
			try {
				String messageId = UUID.randomUUID().toString();
				
				RichiestaGetProtocolloEntrataType input = Director.constructGetProtocolloEntrataInput(idProtocollo, dettagli);
				
				return BusinessDelegate.proxyNPSProtocollo.getProtocolloEntrata(BusinessDelegate.headersFactoryAooMap.get(idAoo+"_Admin").createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaGetProtocolloEntrataType getEntrata(Integer idAoo, Date dataRegistrazione, String idProtocollo,
				int numeroRegistrazione, String codiceRegistro, String codiceAOO, String codiceAmministrazione,
				String denominazioneRegistro, String denominazioneAmministrazione, String denominazioneAOO,
				ProtTipoDettaglioEstrazioneType[] dettagli) {
			try {
				String messageId = UUID.randomUUID().toString();
				
				RichiestaGetProtocolloEntrataType input = Director.constructGetProtocolloEntrataInput(
						dataRegistrazione, idProtocollo, numeroRegistrazione, codiceRegistro, codiceAOO,
						codiceAmministrazione, denominazioneRegistro, denominazioneAmministrazione, denominazioneAOO,
						dettagli);
				
				return BusinessDelegate.proxyNPSProtocollo.getProtocolloEntrata(BusinessDelegate.headersFactoryAooMap.get(idAoo+"_Admin").createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaGetProtocolloEntrataType getEntrata(Integer idAoo, String messageId, RichiestaGetProtocolloEntrataType input) {
			try {
				return BusinessDelegate.proxyNPSProtocollo.getProtocolloEntrata(BusinessDelegate.headersFactoryAooMap.get(idAoo+"_Admin").createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		/**
		 * @param acl se è null non bisogna settare nulla
		 * */
		public static RispostaUpdateDatiProtocolloType updateDatiProtocollo(Integer idAoo, OrgOrganigrammaType operatore, String idProtocollo, String oggetto, 
				String descrizioneTipologia, String indiceClassificazione, boolean isCompleto, String acl, Boolean isRiservato, ProtMetadatiType metadatiAssociati) {
			try {
				String messageId = UUID.randomUUID().toString();
				RichiestaUpdateDatiProtocolloType richiestaUpdate = Director.constructUpdateDatiProtocolloInput(operatore, idProtocollo, oggetto, descrizioneTipologia, indiceClassificazione, isCompleto, metadatiAssociati, acl, isRiservato, new Date());
				
				String headerKeyMap = isRiservato != null ? idAoo+"_Admin" : ""+idAoo;
				
				return BusinessDelegate.proxyNPSProtocollo.updateDatiProtocollo(BusinessDelegate.headersFactoryAooMap.get(headerKeyMap).createHeader(messageId), richiestaUpdate);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}
		
		public static RispostaUpdateDatiProtocolloType updateDatiProtocollo(Integer idAoo, String messageId, RichiestaUpdateDatiProtocolloType input) {
			try {
				
				String headerKeyMap = input.getDatiModifica() != null && input.getDatiModifica().isIsRiservato() != null ? idAoo+"_Admin" : ""+idAoo;
				
				return BusinessDelegate.proxyNPSProtocollo.updateDatiProtocollo(BusinessDelegate.headersFactoryAooMap.get(headerKeyMap).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}
		
		public static RispostaUpdateDatiProtocolloType updateProtocolloCompleto(Integer idAoo, String messageId, RichiestaUpdateDatiProtocolloType input) {
			try {
				return BusinessDelegate.proxyNPSProtocollo.updateDatiProtocollo(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}
		
		public static RispostaReportProtocolloType stampaEtichette(Integer idAoo, String idProtocollo, String tipoReport) {
			try {
				String messageId = UUID.randomUUID().toString();
				RichiestaReportProtocolloType richiestaReportProtocollo = Director.constructRichiestaReportProtocolloInput(idProtocollo, tipoReport);
				return BusinessDelegate.proxyNPSProtocollo.getReportProtocollo(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), richiestaReportProtocollo);
			} catch(Exception ex) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", ex);
			}
		}
	}

	public static class Assegnatario {
		public static RispostaAggiungiAssegnatarioProtocolloEntrataType aggiungiAssegnatario(Integer idAoo, String idProtocollo,
				String acl, Date dataAssegnazione, String perCompetenza,
				OrgOrganigrammaType assegnante, OrgOrganigrammaType assegnatario) {
			try {
				String messageId = UUID.randomUUID().toString();
				
				RichiestaAggiungiAssegnatarioProtocolloEntrataType input = Director.constructAggiungiAssegnatarioProtocolloEntrataInput(idProtocollo, acl, dataAssegnazione,
								perCompetenza, assegnante, assegnatario);
				
				return BusinessDelegate.proxyNPSProtocollo.aggiungiAssegnatarioProtocolloEntrata(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
				
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaAggiungiAssegnatarioProtocolloEntrataType aggiungiAssegnatario(Integer idAoo, String messageId,
				RichiestaAggiungiAssegnatarioProtocolloEntrataType input) {
			try {
				return BusinessDelegate.proxyNPSProtocollo.aggiungiAssegnatarioProtocolloEntrata(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}
	}

	public static class Documento {
		public static RispostaAggiungiDocumentiProtocolloType aggiungi(Integer idAoo, String idProtocollo,
				OrgOrganigrammaType operatore, RichiestaAggiungiDocumentiProtocolloType.Documenti[] documentiArray) {
			try {
				String messageId = UUID.randomUUID().toString();
				
				RichiestaAggiungiDocumentiProtocolloType input = Director.constructAggiungiDocumentiProtocolloInput(
						Boolean.valueOf(true), idProtocollo, operatore, documentiArray);
				
				return BusinessDelegate.proxyNPSProtocollo.aggiungiDocumentiProtocollo(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaAggiungiDocumentiProtocolloType aggiungi(Integer idAoo, String messageId,
				RichiestaAggiungiDocumentiProtocolloType input) {
			try {
				return BusinessDelegate.proxyNPSProtocollo.aggiungiDocumentiProtocollo(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaRimuoviDocumentoProtocolloType rimuovi(Integer idAoo, OperatoreDTO operatore, String idProtocollo, String idDocumento) {
			try {
				String messageId = UUID.randomUUID().toString();
				
				RichiestaRimuoviDocumentoProtocolloType input = Director.constructRimuoviDocumentoProtocolloInput(
						operatore.getIdOperatore(), operatore.getCognome(), operatore.getCodiceAmministrazione(),
						operatore.getDenominazioneAmministrazione(), operatore.getDenominazioneAOO(),
						operatore.getCodiceAOO(), operatore.getDenominazioneUO(), operatore.getCodiceUO(),
						operatore.getNome(), idProtocollo, idDocumento, operatore.getIdUO(), operatore.getCodiceFiscale());
				
				return BusinessDelegate.proxyNPSProtocollo.rimuoviDocumentoProtocollo(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaRimuoviDocumentoProtocolloType rimuovi(Integer idAoo, String messageId, RichiestaRimuoviDocumentoProtocolloType input) {
			try {
				return BusinessDelegate.proxyNPSProtocollo.rimuoviDocumentoProtocollo(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaUploadDocumentoType upload(Integer idAoo, Boolean firmato, String fileName,
				DocTipoOperazioneType tipoOperazione, boolean attivo, String MIMEType, String descrizione,
				InputStream content, byte[] hash, String idDocumentoOperazione, Date dataAnnullamento,
				String tipoCompressione, Date dataDocumento, byte[] previewPng, Date dataChiusura, String note,
				boolean condivisibile, String chiaveEsterna, String idDocumento,
				DocValidazioneFirmaType validazioneFirma, Long length,
				HashMap<String, String> parametriOperazione) {
			String messageId = "";
			RichiestaUploadDocumentoType input = null;
			try {
				messageId = UUID.randomUUID().toString();
				
				input = Director.constructUploadDocumentoInput(attivo, chiaveEsterna,
						condivisibile, content, descrizione, fileName, firmato, hash, idDocumento, length, MIMEType,
						previewPng, tipoCompressione, validazioneFirma, dataAnnullamento, dataChiusura, dataDocumento,
						note, tipoOperazione, idDocumentoOperazione, parametriOperazione);
				 
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
			
			try {
				return BusinessDelegate.proxyNPSDocumentale.uploadDocumento(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new ProtocolloGialloException("##NPSFAULT## - Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaUploadDocumentoType upload(Integer idAoo, String messageId, RichiestaUploadDocumentoType input) {
			try {
				return BusinessDelegate.proxyNPSDocumentale.uploadDocumento(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static DocumentoNpsDTO downloadDocumento(Integer idAoo, String guid,boolean ricercaConAcl) {
			DocumentoNpsDTO documentoNpsDTO = null;
			try {
				String messageId = UUID.randomUUID().toString();
				
				RichiestaDownloadDocumentoType richiesta = Builder.buildRichiestaDownloadDocument(guid);
				
				RispostaDownloadDocumentoType risposta = null;
				try {
					risposta = BusinessDelegate.proxyNPSDocumentale.downloadDocumento(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), richiesta);
				} catch (Exception e) {
					throw new ProtocolloGialloException("##NPSFAULT## - Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
				}
				 
				if ((risposta != null) && (risposta.getDatiDocumento() != null)) {
					DocDocumentoContentType datiDocumento = risposta.getDatiDocumento();
					documentoNpsDTO = new DocumentoNpsDTO();
					documentoNpsDTO.setGuid(datiDocumento.getIdDocumento());
					documentoNpsDTO.setInputStream(new ByteArrayInputStream(datiDocumento.getContent()));
					documentoNpsDTO.setNomeFile(datiDocumento.getFileName());
					documentoNpsDTO.setContentType(datiDocumento.getMIMEType());
					documentoNpsDTO.setDescrizione(datiDocumento.getDescrizione());
				}
			} catch(ProtocolloGialloException e) {
				throw new ProtocolloGialloException("##NPSFAULT## - Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
			return documentoNpsDTO;
		}
	}

	public static class Report {
		public static RispostaReportRegistroGiornalieroType registroGiornaliero(Integer idAoo, Date dataDa, Date dataA,
				OrgOrganigrammaType operatore, String codiceRegistro, String denominazioneRegistro) {
			try {
				String messageId = UUID.randomUUID().toString();
				
				RichiestaReportRegistroGiornalieroType input = Director.constructReportRegistroGiornalieroInput(dataDa, dataA, operatore, codiceRegistro, denominazioneRegistro);
				
				return BusinessDelegate.proxyNPSProtocollo.getReportRegistroGiornaliero(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}
	}

	public static class Allaccio {
		public static RispostaAggiungiAllaccioProtocolloType aggiungiAllacci(Integer idAoo, OrgOrganigrammaType operatore,
				String idProtocollo, RichiestaAggiungiAllaccioProtocolloType.Allaccio[] allacci) {
			try {
				String messageId = UUID.randomUUID().toString();
				
				RichiestaAggiungiAllaccioProtocolloType input = Director.constructAggiungiAllaccioProtocolloInput(operatore, idProtocollo, allacci);
				
				return BusinessDelegate.proxyNPSProtocollo.aggiungiAllaccioProtocollo(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
				
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaAggiungiAllaccioProtocolloType aggiungiAllacci(Integer idAoo, String messageId, RichiestaAggiungiAllaccioProtocolloType input) {
			try {
				return BusinessDelegate.proxyNPSProtocollo.aggiungiAllaccioProtocollo(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaRimuoviAllaccioProtocolloType rimuovi(Integer idAoo, OrgOrganigrammaType operatore,
				String idProtocolloAllacciante, String codiceRegistro, String denominazioneRegistro,
				Date dataRegistrazione, String idProtocolloAllacciato, String numeroRegistrazione) {
			try {
				String messageId = UUID.randomUUID().toString();
				
				RichiestaRimuoviAllaccioProtocolloType input = Director.constructRimuoviAllaccioProtocolloInput(
						operatore, idProtocolloAllacciante, idProtocolloAllacciato, dataRegistrazione,
						numeroRegistrazione, codiceRegistro, denominazioneRegistro);
				
				return BusinessDelegate.proxyNPSProtocollo.rimuoviAllaccioProtocollo(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

		public static RispostaRimuoviAllaccioProtocolloType rimuovi(Integer idAoo, String messageId, RichiestaRimuoviAllaccioProtocolloType input) {
			try {
				return BusinessDelegate.proxyNPSProtocollo.rimuoviAllaccioProtocollo(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}
	}
	
	public static class RegistrazioneAusiliaria {
		public static RispostaRicercaRegistrazioneAusiliariaType ricercaRegistrazioneAusiliaria(Integer idAoo, short anno, String codiceRegistro, String descrizioneRegistro, AllTipoRegistroAusiliarioType tipoRegistro,
				Integer numeroRegistrazioneDa, Integer numeroRegistrazioneA, Date dataRegistrazioneDa, Date dataRegistrazioneA, Date dataAnnullamentoDa, Date dataAnnullamentoA, 
				String codiceTipologiaDocumento, boolean annullato, String acl, String applicazione, String idUtente, String nome, String cognome, String codiceAmministrazione, 
				String denominazioneAmministrazione, String denominazioneUO, String codiceUO, String denominazioneAOO, String codiceAOO, short numeroRisultati, String idUO, String oggetto) {
			try {
				String messageId = UUID.randomUUID().toString();
				
				RichiestaRicercaRegistrazioneAusiliariaType input = Director.constructRichiestaRicercaRegistrazioneAusiliaria(anno, codiceRegistro, descrizioneRegistro,
						tipoRegistro, numeroRegistrazioneDa, numeroRegistrazioneA, dataRegistrazioneDa, dataRegistrazioneA, dataAnnullamentoDa, dataAnnullamentoA, 
						codiceTipologiaDocumento, annullato, acl, applicazione, idUtente, nome, cognome, codiceAmministrazione, 
						denominazioneAmministrazione, denominazioneUO, codiceUO, denominazioneAOO, codiceAOO, numeroRisultati, idUO, oggetto);
				
				return BusinessDelegate.proxyNPSRegistrazioneAusiliaria.ricercaRegistrazioneAusiliaria(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
				
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}
		
		public static RispostaCreateRegistrazioneAusiliariaType createRegistrazioneAusiliaria(Integer idAoo, String acl, String oggetto, 
				String idUtente, String nome, String cognome, String codiceAmministrazione, String denominazioneAmministrazione, String denominazioneUO, String codiceUO, 
				String denominazioneAOO, String codiceAOO, String codiceRegistro, String descrizioneRegistro, AllTipoRegistroAusiliarioType tipoRegistro,
				String codiceTipologiaDocumento, String descrizioneTipologiaDocumento, String idProtocolloIngresso, String idUO, String codiceFiscale) {
			
			String messageId = "";
			RichiestaCreateRegistrazioneAusiliariaType input = null;
			try {
				messageId = UUID.randomUUID().toString();
				
				input = Director.constructRichiestaCreateRegistrazioneAusiliariaType(acl, oggetto,
						idUtente, nome, cognome, codiceAmministrazione, denominazioneAmministrazione, denominazioneUO, codiceUO, denominazioneAOO, 
						codiceAOO, codiceRegistro, descrizioneRegistro, tipoRegistro, codiceTipologiaDocumento, descrizioneTipologiaDocumento,
						idProtocolloIngresso, idUO, codiceFiscale);
				 
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
			
			try {
				return BusinessDelegate.proxyNPSRegistrazioneAusiliaria.createRegistrazioneAusiliaria(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			}catch (Exception e) {
				throw new ProtocolloGialloException("##NPSFAULT## - Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}
		
		public static RispostaCollegaRegistrazioneAusiliariaProtocolloIngressoType collegaRegistrazioneAusiliariaProtocolloIngresso(Integer idAoo, String messageId, RichiestaCollegaRegistrazioneAusiliariaProtocolloIngressoType input) {
			try {
				
				return BusinessDelegate.proxyNPSRegistrazioneAusiliaria.collegaRegistrazioneAusiliariaProtocolloIngresso(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
				
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}
		
		public static RispostaCollegaRegistrazioneAusiliariaProtocolloUscitaType collegaRegistrazioneAusiliariaProtocolloUscita(Integer idAoo, String messageId, RichiestaCollegaRegistrazioneAusiliariaProtocolloUscitaType input) {
			try {
				
				return BusinessDelegate.proxyNPSRegistrazioneAusiliaria.collegaRegistrazioneAusiliariaProtocolloUscita(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
				
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}
	}
	
	public static class FlussoAUT {
		
		public static RispostaInviaAttoType inviaAtto(Integer idAoo, String acl, WkfDestinatarioFlussoType[] cDestinatari,
				String codiceFlusso, String identificatoreProcesso, String identificativoMessaggio, String idUtente, String nome, String cognome, 
				String codiceAmministrazione, String denominazioneAmministrazione, String denominazioneUO, String codiceUO, String denominazioneAOO, 
				String codiceAOO, String oggetto, TIPOLOGIA metadatiEstesi, String guidDocPrincipale, String[] guidAllegati, String codiceVoceTitolario, 
				String descrizioneVoceTitolario, String descrizioneTipologia, String codiceTipologia, ProtMetadatiType metadatiAssociati, 
				ProtAllaccioProtocolloType[] cAllacciati, String idUO, String codiceFiscale, String oggettoMessaggio, String corpoMessaggio) {
			String messageId = "";
			RichiestaInviaAttoType input = null;
			try {
				messageId = UUID.randomUUID().toString();
				
				input = Director.constructRichiestaInviaAttoType(acl, cDestinatari, codiceFlusso, identificatoreProcesso, 
						identificativoMessaggio, idUtente, nome, cognome, codiceAmministrazione, denominazioneAmministrazione, denominazioneUO, codiceUO, 
						denominazioneAOO, codiceAOO, oggetto, metadatiEstesi, guidDocPrincipale, guidAllegati, codiceVoceTitolario, descrizioneVoceTitolario, 
						descrizioneTipologia, codiceTipologia, metadatiAssociati, cAllacciati, idUO, messageId, codiceFiscale, oggettoMessaggio, corpoMessaggio);
				 
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
			
			try {
				return BusinessDelegate.proxyNPSFlussoAUT.inviaAtto(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
			} catch (Exception e) {
				throw new ProtocolloGialloException("##NPSFAULT## - Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}
		
	}
	
	public static class Posta {

		public static RispostaInviaMessaggioPostaType inviaMessaggioPosta(Integer idAoo, String codAoo,
				String indirizzoMailMittente,String isPec,String ufficiale,String messaggio,
				String corpoMessaggio,List<String> indirizzoMailDest,List<String> allegatiGuid,
				Boolean notificaSpedizione) {
			try {
				String messageId = UUID.randomUUID().toString();

				RichiestaInviaMessaggioPostaType input = Director.constructRichiestaInviaMessaggioPostaType(messageId,
						codAoo,indirizzoMailMittente,isPec,ufficiale,messaggio,corpoMessaggio,indirizzoMailDest,allegatiGuid,notificaSpedizione);

				return BusinessDelegate.proxyNPSPosta.inviaMessaggioPosta(BusinessDelegate.headersFactoryAooMap.get(""+idAoo).createHeader(messageId), input);
				
			} catch (Exception e) {
				throw new BusinessDelegateRuntimeException("Errore durante la chiamata al metodo del BusinessDelegate di NPS", e);
			}
		}

	}
}
