
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce il tipo di dato del riusltato del Documento Trattato
 * 
 * <p>Classe Java per doc_documentoResult_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="doc_documentoResult_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}doc_documentoFile_type">
 *       &lt;sequence>
 *         &lt;element name="IdDocumentoOrigine" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="IsDocumentoOriginale" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsDocumentoTrattato" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "doc_documentoResult_type", propOrder = {
    "idDocumentoOrigine",
    "isDocumentoOriginale",
    "isDocumentoTrattato"
})
public class DocDocumentoResultType
    extends DocDocumentoFileType
    implements Serializable
{

    @XmlElement(name = "IdDocumentoOrigine", required = true, nillable = true)
    protected String idDocumentoOrigine;
    @XmlElement(name = "IsDocumentoOriginale", defaultValue = "true")
    protected boolean isDocumentoOriginale;
    @XmlElement(name = "IsDocumentoTrattato", defaultValue = "true")
    protected boolean isDocumentoTrattato;

    /**
     * Recupera il valore della proprietà idDocumentoOrigine.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoOrigine() {
        return idDocumentoOrigine;
    }

    /**
     * Imposta il valore della proprietà idDocumentoOrigine.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoOrigine(String value) {
        this.idDocumentoOrigine = value;
    }

    /**
     * Recupera il valore della proprietà isDocumentoOriginale.
     * 
     */
    public boolean isIsDocumentoOriginale() {
        return isDocumentoOriginale;
    }

    /**
     * Imposta il valore della proprietà isDocumentoOriginale.
     * 
     */
    public void setIsDocumentoOriginale(boolean value) {
        this.isDocumentoOriginale = value;
    }

    /**
     * Recupera il valore della proprietà isDocumentoTrattato.
     * 
     */
    public boolean isIsDocumentoTrattato() {
        return isDocumentoTrattato;
    }

    /**
     * Imposta il valore della proprietà isDocumentoTrattato.
     * 
     */
    public void setIsDocumentoTrattato(boolean value) {
        this.isDocumentoTrattato = value;
    }

}
