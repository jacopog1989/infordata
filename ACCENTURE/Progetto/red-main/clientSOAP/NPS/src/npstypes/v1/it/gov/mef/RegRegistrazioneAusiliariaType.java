
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Definisce il tipo di dato relativo alla registrazione ausiliaria
 * 
 * <p>Classe Java per reg_registrazioneAusiliaria_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="reg_registrazioneAusiliaria_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdRegistrazioneAusiliaria" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="Registro" type="{http://mef.gov.it.v1.npsTypes}reg_registro_type"/>
 *         &lt;element name="DataRegistrazione" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="NumeroRegistrazione" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="Oggetto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TipoDocumento" type="{http://mef.gov.it.v1.npsTypes}reg_attributiEstesi_type" minOccurs="0"/>
 *         &lt;element name="ProtocolloAssociato" type="{http://mef.gov.it.v1.npsTypes}prot_identificatoreProtocollo_type" minOccurs="0"/>
 *         &lt;element name="ProtocolloRisposta" type="{http://mef.gov.it.v1.npsTypes}prot_identificatoreProtocollo_type" minOccurs="0"/>
 *         &lt;element name="DocumentoPrincipale" type="{http://mef.gov.it.v1.npsTypes}reg_documento_type" minOccurs="0"/>
 *         &lt;element name="Operatore" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type"/>
 *         &lt;element name="IsAnnullato" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="MotivoAnnullamento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataAnnullamento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="Acl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reg_registrazioneAusiliaria_type", propOrder = {
    "idRegistrazioneAusiliaria",
    "registro",
    "dataRegistrazione",
    "numeroRegistrazione",
    "oggetto",
    "tipoDocumento",
    "protocolloAssociato",
    "protocolloRisposta",
    "documentoPrincipale",
    "operatore",
    "isAnnullato",
    "motivoAnnullamento",
    "dataAnnullamento",
    "acl"
})
public class RegRegistrazioneAusiliariaType
    implements Serializable
{

    @XmlElement(name = "IdRegistrazioneAusiliaria", required = true, nillable = true)
    protected String idRegistrazioneAusiliaria;
    @XmlElement(name = "Registro", required = true)
    protected RegRegistroType registro;
    @XmlElementRef(name = "DataRegistrazione", type = JAXBElement.class, required = false)
    protected JAXBElement<XMLGregorianCalendar> dataRegistrazione;
    @XmlElement(name = "NumeroRegistrazione")
    protected Integer numeroRegistrazione;
    @XmlElement(name = "Oggetto", required = true)
    protected String oggetto;
    @XmlElement(name = "TipoDocumento")
    protected RegAttributiEstesiType tipoDocumento;
    @XmlElement(name = "ProtocolloAssociato")
    protected ProtIdentificatoreProtocolloType protocolloAssociato;
    @XmlElement(name = "ProtocolloRisposta")
    protected ProtIdentificatoreProtocolloType protocolloRisposta;
    @XmlElement(name = "DocumentoPrincipale")
    protected RegDocumentoType documentoPrincipale;
    @XmlElement(name = "Operatore", required = true)
    protected OrgOrganigrammaType operatore;
    @XmlElement(name = "IsAnnullato")
    protected Boolean isAnnullato;
    @XmlElement(name = "MotivoAnnullamento")
    protected String motivoAnnullamento;
    @XmlElement(name = "DataAnnullamento")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataAnnullamento;
    @XmlElement(name = "Acl")
    protected String acl;

    /**
     * Recupera il valore della proprietà idRegistrazioneAusiliaria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRegistrazioneAusiliaria() {
        return idRegistrazioneAusiliaria;
    }

    /**
     * Imposta il valore della proprietà idRegistrazioneAusiliaria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRegistrazioneAusiliaria(String value) {
        this.idRegistrazioneAusiliaria = value;
    }

    /**
     * Recupera il valore della proprietà registro.
     * 
     * @return
     *     possible object is
     *     {@link RegRegistroType }
     *     
     */
    public RegRegistroType getRegistro() {
        return registro;
    }

    /**
     * Imposta il valore della proprietà registro.
     * 
     * @param value
     *     allowed object is
     *     {@link RegRegistroType }
     *     
     */
    public void setRegistro(RegRegistroType value) {
        this.registro = value;
    }

    /**
     * Recupera il valore della proprietà dataRegistrazione.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public JAXBElement<XMLGregorianCalendar> getDataRegistrazione() {
        return dataRegistrazione;
    }

    /**
     * Imposta il valore della proprietà dataRegistrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}
     *     
     */
    public void setDataRegistrazione(JAXBElement<XMLGregorianCalendar> value) {
        this.dataRegistrazione = value;
    }

    /**
     * Recupera il valore della proprietà numeroRegistrazione.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumeroRegistrazione() {
        return numeroRegistrazione;
    }

    /**
     * Imposta il valore della proprietà numeroRegistrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumeroRegistrazione(Integer value) {
        this.numeroRegistrazione = value;
    }

    /**
     * Recupera il valore della proprietà oggetto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggetto() {
        return oggetto;
    }

    /**
     * Imposta il valore della proprietà oggetto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggetto(String value) {
        this.oggetto = value;
    }

    /**
     * Recupera il valore della proprietà tipoDocumento.
     * 
     * @return
     *     possible object is
     *     {@link RegAttributiEstesiType }
     *     
     */
    public RegAttributiEstesiType getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Imposta il valore della proprietà tipoDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link RegAttributiEstesiType }
     *     
     */
    public void setTipoDocumento(RegAttributiEstesiType value) {
        this.tipoDocumento = value;
    }

    /**
     * Recupera il valore della proprietà protocolloAssociato.
     * 
     * @return
     *     possible object is
     *     {@link ProtIdentificatoreProtocolloType }
     *     
     */
    public ProtIdentificatoreProtocolloType getProtocolloAssociato() {
        return protocolloAssociato;
    }

    /**
     * Imposta il valore della proprietà protocolloAssociato.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtIdentificatoreProtocolloType }
     *     
     */
    public void setProtocolloAssociato(ProtIdentificatoreProtocolloType value) {
        this.protocolloAssociato = value;
    }

    /**
     * Recupera il valore della proprietà protocolloRisposta.
     * 
     * @return
     *     possible object is
     *     {@link ProtIdentificatoreProtocolloType }
     *     
     */
    public ProtIdentificatoreProtocolloType getProtocolloRisposta() {
        return protocolloRisposta;
    }

    /**
     * Imposta il valore della proprietà protocolloRisposta.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtIdentificatoreProtocolloType }
     *     
     */
    public void setProtocolloRisposta(ProtIdentificatoreProtocolloType value) {
        this.protocolloRisposta = value;
    }

    /**
     * Recupera il valore della proprietà documentoPrincipale.
     * 
     * @return
     *     possible object is
     *     {@link RegDocumentoType }
     *     
     */
    public RegDocumentoType getDocumentoPrincipale() {
        return documentoPrincipale;
    }

    /**
     * Imposta il valore della proprietà documentoPrincipale.
     * 
     * @param value
     *     allowed object is
     *     {@link RegDocumentoType }
     *     
     */
    public void setDocumentoPrincipale(RegDocumentoType value) {
        this.documentoPrincipale = value;
    }

    /**
     * Recupera il valore della proprietà operatore.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getOperatore() {
        return operatore;
    }

    /**
     * Imposta il valore della proprietà operatore.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setOperatore(OrgOrganigrammaType value) {
        this.operatore = value;
    }

    /**
     * Recupera il valore della proprietà isAnnullato.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsAnnullato() {
        return isAnnullato;
    }

    /**
     * Imposta il valore della proprietà isAnnullato.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsAnnullato(Boolean value) {
        this.isAnnullato = value;
    }

    /**
     * Recupera il valore della proprietà motivoAnnullamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMotivoAnnullamento() {
        return motivoAnnullamento;
    }

    /**
     * Imposta il valore della proprietà motivoAnnullamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMotivoAnnullamento(String value) {
        this.motivoAnnullamento = value;
    }

    /**
     * Recupera il valore della proprietà dataAnnullamento.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAnnullamento() {
        return dataAnnullamento;
    }

    /**
     * Imposta il valore della proprietà dataAnnullamento.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAnnullamento(XMLGregorianCalendar value) {
        this.dataAnnullamento = value;
    }

    /**
     * Recupera il valore della proprietà acl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcl() {
        return acl;
    }

    /**
     * Imposta il valore della proprietà acl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcl(String value) {
        this.acl = value;
    }

}
