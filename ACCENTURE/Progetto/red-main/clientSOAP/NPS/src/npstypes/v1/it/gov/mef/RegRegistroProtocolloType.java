
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Definisce il tipo di dato relativo al registro di protocollo informatico
 * 
 * <p>Classe Java per reg_registroProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="reg_registroProtocollo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Aoo" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_aoo_type"/>
 *         &lt;element name="Codice" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Anno" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DataApertura" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="DataChiusura" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="IsApertoEntrata" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsApertoUscita" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsAutomatico" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Ufficiale">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="si"/>
 *             &lt;enumeration value="no"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reg_registroProtocollo_type", propOrder = {
    "aoo",
    "codice",
    "descrizione",
    "anno",
    "dataApertura",
    "dataChiusura",
    "isApertoEntrata",
    "isApertoUscita",
    "isAutomatico"
})
public class RegRegistroProtocolloType
    implements Serializable
{

    @XmlElement(name = "Aoo", required = true)
    protected OrgOrganigrammaAooType aoo;
    @XmlElement(name = "Codice", required = true)
    protected String codice;
    @XmlElement(name = "Descrizione", required = true)
    protected String descrizione;
    @XmlElement(name = "Anno")
    protected int anno;
    @XmlElement(name = "DataApertura", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataApertura;
    @XmlElement(name = "DataChiusura")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataChiusura;
    @XmlElement(name = "IsApertoEntrata")
    protected boolean isApertoEntrata;
    @XmlElement(name = "IsApertoUscita")
    protected boolean isApertoUscita;
    @XmlElement(name = "IsAutomatico")
    protected boolean isAutomatico;
    @XmlAttribute(name = "Ufficiale")
    protected String ufficiale;

    /**
     * Recupera il valore della proprietà aoo.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaAooType }
     *     
     */
    public OrgOrganigrammaAooType getAoo() {
        return aoo;
    }

    /**
     * Imposta il valore della proprietà aoo.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaAooType }
     *     
     */
    public void setAoo(OrgOrganigrammaAooType value) {
        this.aoo = value;
    }

    /**
     * Recupera il valore della proprietà codice.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodice() {
        return codice;
    }

    /**
     * Imposta il valore della proprietà codice.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodice(String value) {
        this.codice = value;
    }

    /**
     * Recupera il valore della proprietà descrizione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Imposta il valore della proprietà descrizione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Recupera il valore della proprietà anno.
     * 
     */
    public int getAnno() {
        return anno;
    }

    /**
     * Imposta il valore della proprietà anno.
     * 
     */
    public void setAnno(int value) {
        this.anno = value;
    }

    /**
     * Recupera il valore della proprietà dataApertura.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataApertura() {
        return dataApertura;
    }

    /**
     * Imposta il valore della proprietà dataApertura.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataApertura(XMLGregorianCalendar value) {
        this.dataApertura = value;
    }

    /**
     * Recupera il valore della proprietà dataChiusura.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataChiusura() {
        return dataChiusura;
    }

    /**
     * Imposta il valore della proprietà dataChiusura.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataChiusura(XMLGregorianCalendar value) {
        this.dataChiusura = value;
    }

    /**
     * Recupera il valore della proprietà isApertoEntrata.
     * 
     */
    public boolean isIsApertoEntrata() {
        return isApertoEntrata;
    }

    /**
     * Imposta il valore della proprietà isApertoEntrata.
     * 
     */
    public void setIsApertoEntrata(boolean value) {
        this.isApertoEntrata = value;
    }

    /**
     * Recupera il valore della proprietà isApertoUscita.
     * 
     */
    public boolean isIsApertoUscita() {
        return isApertoUscita;
    }

    /**
     * Imposta il valore della proprietà isApertoUscita.
     * 
     */
    public void setIsApertoUscita(boolean value) {
        this.isApertoUscita = value;
    }

    /**
     * Recupera il valore della proprietà isAutomatico.
     * 
     */
    public boolean isIsAutomatico() {
        return isAutomatico;
    }

    /**
     * Imposta il valore della proprietà isAutomatico.
     * 
     */
    public void setIsAutomatico(boolean value) {
        this.isAutomatico = value;
    }

    /**
     * Recupera il valore della proprietà ufficiale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUfficiale() {
        return ufficiale;
    }

    /**
     * Imposta il valore della proprietà ufficiale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUfficiale(String value) {
        this.ufficiale = value;
    }

}
