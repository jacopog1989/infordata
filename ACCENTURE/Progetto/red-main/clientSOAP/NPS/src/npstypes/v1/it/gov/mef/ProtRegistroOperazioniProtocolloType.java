
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Definisce il tipo di dato relativo allo storico delle operazioni effettuate sul protocollo
 * 
 * <p>Classe Java per prot_registroOperazioniProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_registroOperazioniProtocollo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DataOperazioneProtocollo" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="TipoOperazioneProtocollo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Operatore" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type"/>
 *         &lt;choice>
 *           &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *           &lt;element name="DettaglioOperazioni" type="{http://mef.gov.it.v1.npsTypes}prot_dettaglioOperazioniProtocollo_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_registroOperazioniProtocollo_type", propOrder = {
    "dataOperazioneProtocollo",
    "tipoOperazioneProtocollo",
    "operatore",
    "descrizione",
    "dettaglioOperazioni"
})
public class ProtRegistroOperazioniProtocolloType
    implements Serializable
{

    @XmlElement(name = "DataOperazioneProtocollo", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataOperazioneProtocollo;
    @XmlElement(name = "TipoOperazioneProtocollo", required = true)
    protected String tipoOperazioneProtocollo;
    @XmlElement(name = "Operatore", required = true)
    protected OrgOrganigrammaType operatore;
    @XmlElement(name = "Descrizione")
    protected String descrizione;
    @XmlElement(name = "DettaglioOperazioni")
    protected List<ProtDettaglioOperazioniProtocolloType> dettaglioOperazioni;

    /**
     * Recupera il valore della proprietà dataOperazioneProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataOperazioneProtocollo() {
        return dataOperazioneProtocollo;
    }

    /**
     * Imposta il valore della proprietà dataOperazioneProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataOperazioneProtocollo(XMLGregorianCalendar value) {
        this.dataOperazioneProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà tipoOperazioneProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoOperazioneProtocollo() {
        return tipoOperazioneProtocollo;
    }

    /**
     * Imposta il valore della proprietà tipoOperazioneProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoOperazioneProtocollo(String value) {
        this.tipoOperazioneProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà operatore.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getOperatore() {
        return operatore;
    }

    /**
     * Imposta il valore della proprietà operatore.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setOperatore(OrgOrganigrammaType value) {
        this.operatore = value;
    }

    /**
     * Recupera il valore della proprietà descrizione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Imposta il valore della proprietà descrizione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Gets the value of the dettaglioOperazioni property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dettaglioOperazioni property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDettaglioOperazioni().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtDettaglioOperazioniProtocolloType }
     * 
     * 
     */
    public List<ProtDettaglioOperazioniProtocolloType> getDettaglioOperazioni() {
        if (dettaglioOperazioni == null) {
            dettaglioOperazioni = new ArrayList<ProtDettaglioOperazioniProtocolloType>();
        }
        return this.dettaglioOperazioni;
    }

}
