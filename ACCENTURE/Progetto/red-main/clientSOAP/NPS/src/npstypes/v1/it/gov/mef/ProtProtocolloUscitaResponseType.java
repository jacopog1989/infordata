
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce il tipo di dato retituito relativamente alla protoocollazione in uscita 
 * 
 * <p>Classe Java per prot_protocolloUscitaResponseType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_protocolloUscitaResponseType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}prot_protocolloBaseResponse_type">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_protocolloUscitaResponseType")
public class ProtProtocolloUscitaResponseType
    extends ProtProtocolloBaseResponseType
    implements Serializable
{


}
