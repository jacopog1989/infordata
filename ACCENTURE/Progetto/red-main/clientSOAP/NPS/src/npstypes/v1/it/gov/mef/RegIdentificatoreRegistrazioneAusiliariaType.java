
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import npsmessages.v1.it.gov.mef.RichiestaGetRegistrazioneAusiliariaType;


/**
 * Definisce il tipo di dato Identificatore della Registrazione Ausiliaria
 * 
 * <p>Classe Java per reg_identificatoreRegistrazioneAusiliaria_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="reg_identificatoreRegistrazioneAusiliaria_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="IdRegistrazioneAusiliaria" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;sequence>
 *           &lt;element name="Registro" type="{http://mef.gov.it.v1.npsTypes}reg_registro_type"/>
 *           &lt;element name="DataRegistrazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *           &lt;element name="NumeroRegistrazione" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;/sequence>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reg_identificatoreRegistrazioneAusiliaria_type", propOrder = {
    "idRegistrazioneAusiliaria",
    "registro",
    "dataRegistrazione",
    "numeroRegistrazione"
})
@XmlSeeAlso({
    RichiestaGetRegistrazioneAusiliariaType.class
})
public class RegIdentificatoreRegistrazioneAusiliariaType
    implements Serializable
{

    @XmlElement(name = "IdRegistrazioneAusiliaria")
    protected String idRegistrazioneAusiliaria;
    @XmlElement(name = "Registro")
    protected RegRegistroType registro;
    @XmlElement(name = "DataRegistrazione")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataRegistrazione;
    @XmlElement(name = "NumeroRegistrazione")
    protected Integer numeroRegistrazione;

    /**
     * Recupera il valore della proprietà idRegistrazioneAusiliaria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRegistrazioneAusiliaria() {
        return idRegistrazioneAusiliaria;
    }

    /**
     * Imposta il valore della proprietà idRegistrazioneAusiliaria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRegistrazioneAusiliaria(String value) {
        this.idRegistrazioneAusiliaria = value;
    }

    /**
     * Recupera il valore della proprietà registro.
     * 
     * @return
     *     possible object is
     *     {@link RegRegistroType }
     *     
     */
    public RegRegistroType getRegistro() {
        return registro;
    }

    /**
     * Imposta il valore della proprietà registro.
     * 
     * @param value
     *     allowed object is
     *     {@link RegRegistroType }
     *     
     */
    public void setRegistro(RegRegistroType value) {
        this.registro = value;
    }

    /**
     * Recupera il valore della proprietà dataRegistrazione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataRegistrazione() {
        return dataRegistrazione;
    }

    /**
     * Imposta il valore della proprietà dataRegistrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataRegistrazione(XMLGregorianCalendar value) {
        this.dataRegistrazione = value;
    }

    /**
     * Recupera il valore della proprietà numeroRegistrazione.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumeroRegistrazione() {
        return numeroRegistrazione;
    }

    /**
     * Imposta il valore della proprietà numeroRegistrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumeroRegistrazione(Integer value) {
        this.numeroRegistrazione = value;
    }

}
