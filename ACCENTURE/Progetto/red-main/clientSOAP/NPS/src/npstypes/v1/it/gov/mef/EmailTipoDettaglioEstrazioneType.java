
package npstypes.v1.it.gov.mef;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per email_tipoDettaglioEstrazione_type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="email_tipoDettaglioEstrazione_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DATI_MINIMI"/>
 *     &lt;enumeration value="ALLEGATI"/>
 *     &lt;enumeration value="MESSAGGIO_POSTA"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "email_tipoDettaglioEstrazione_type")
@XmlEnum
public enum EmailTipoDettaglioEstrazioneType {

    DATI_MINIMI,
    ALLEGATI,
    MESSAGGIO_POSTA;

    public String value() {
        return name();
    }

    public static EmailTipoDettaglioEstrazioneType fromValue(String v) {
        return valueOf(v);
    }

}
