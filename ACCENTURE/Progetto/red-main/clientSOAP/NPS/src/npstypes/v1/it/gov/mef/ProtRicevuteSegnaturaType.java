
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Definisce il tipo di dato relativo alle recevute della segnatura di prpotocollo 
 * 
 * <p>Classe Java per prot_ricevuteSegnatura_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_ricevuteSegnatura_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DataRicezione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="TipoMessaggio">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="CONFERMA_RICEZIONE"/>
 *               &lt;enumeration value="NOTIFICA_ECCEZIONE"/>
 *               &lt;enumeration value="AGGIORNAMENTO_CONFERMA"/>
 *               &lt;enumeration value="ANNULLAMENTO_PROTOCOLLAZIONE"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="TestoMessaggio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ContentMessaggio" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_ricevuteSegnatura_type", propOrder = {
    "dataRicezione",
    "tipoMessaggio",
    "testoMessaggio",
    "contentMessaggio"
})
public class ProtRicevuteSegnaturaType
    implements Serializable
{

    @XmlElement(name = "DataRicezione", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataRicezione;
    @XmlElement(name = "TipoMessaggio", required = true)
    protected String tipoMessaggio;
    @XmlElement(name = "TestoMessaggio", required = true)
    protected String testoMessaggio;
    @XmlElement(name = "ContentMessaggio", required = true)
    protected byte[] contentMessaggio;

    /**
     * Recupera il valore della proprietà dataRicezione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataRicezione() {
        return dataRicezione;
    }

    /**
     * Imposta il valore della proprietà dataRicezione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataRicezione(XMLGregorianCalendar value) {
        this.dataRicezione = value;
    }

    /**
     * Recupera il valore della proprietà tipoMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoMessaggio() {
        return tipoMessaggio;
    }

    /**
     * Imposta il valore della proprietà tipoMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoMessaggio(String value) {
        this.tipoMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà testoMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTestoMessaggio() {
        return testoMessaggio;
    }

    /**
     * Imposta il valore della proprietà testoMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTestoMessaggio(String value) {
        this.testoMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà contentMessaggio.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getContentMessaggio() {
        return contentMessaggio;
    }

    /**
     * Imposta il valore della proprietà contentMessaggio.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setContentMessaggio(byte[] value) {
        this.contentMessaggio = value;
    }

}
