
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce il tipo di dato del protocollo da resituire al chiamante
 * 
 * <p>Classe Java per prot_protocolloBaseResponse_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_protocolloBaseResponse_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}prot_protocolloBase_type">
 *       &lt;sequence>
 *         &lt;element name="Allaccianti" type="{http://mef.gov.it.v1.npsTypes}prot_allaccioProtocollo_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="DocumentoPrincipale" type="{http://mef.gov.it.v1.npsTypes}prot_documentoProtocollato_type" minOccurs="0"/>
 *         &lt;element name="Allegati" type="{http://mef.gov.it.v1.npsTypes}prot_documentoProtocollato_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="IsAnnullato" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsMozione" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Stato">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="DA_ASSEGNARE"/>
 *               &lt;enumeration value="IN_LAVORAZIONE"/>
 *               &lt;enumeration value="AGLI_ATTI"/>
 *               &lt;enumeration value="CHIUSO"/>
 *               &lt;enumeration value="DA_SPEDIRE"/>
 *               &lt;enumeration value="INVIATO"/>
 *               &lt;enumeration value="SPEDITO"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SistemaProduttore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_protocolloBaseResponse_type", propOrder = {
    "allaccianti",
    "documentoPrincipale",
    "allegati",
    "isAnnullato",
    "isMozione",
    "stato",
    "sistemaProduttore"
})
@XmlSeeAlso({
    ProtProtocolloUscitaResponseType.class,
    ProtProtocolloEntrataResponseType.class
})
public class ProtProtocolloBaseResponseType
    extends ProtProtocolloBaseType
    implements Serializable
{

    @XmlElement(name = "Allaccianti")
    protected List<ProtAllaccioProtocolloType> allaccianti;
    @XmlElement(name = "DocumentoPrincipale")
    protected ProtDocumentoProtocollatoType documentoPrincipale;
    @XmlElement(name = "Allegati")
    protected List<ProtDocumentoProtocollatoType> allegati;
    @XmlElement(name = "IsAnnullato", defaultValue = "false")
    protected boolean isAnnullato;
    @XmlElement(name = "IsMozione", defaultValue = "false")
    protected boolean isMozione;
    @XmlElement(name = "Stato", required = true)
    protected String stato;
    @XmlElement(name = "SistemaProduttore", required = true)
    protected String sistemaProduttore;

    /**
     * Gets the value of the allaccianti property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allaccianti property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllaccianti().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtAllaccioProtocolloType }
     * 
     * 
     */
    public List<ProtAllaccioProtocolloType> getAllaccianti() {
        if (allaccianti == null) {
            allaccianti = new ArrayList<ProtAllaccioProtocolloType>();
        }
        return this.allaccianti;
    }

    /**
     * Recupera il valore della proprietà documentoPrincipale.
     * 
     * @return
     *     possible object is
     *     {@link ProtDocumentoProtocollatoType }
     *     
     */
    public ProtDocumentoProtocollatoType getDocumentoPrincipale() {
        return documentoPrincipale;
    }

    /**
     * Imposta il valore della proprietà documentoPrincipale.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtDocumentoProtocollatoType }
     *     
     */
    public void setDocumentoPrincipale(ProtDocumentoProtocollatoType value) {
        this.documentoPrincipale = value;
    }

    /**
     * Gets the value of the allegati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allegati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllegati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtDocumentoProtocollatoType }
     * 
     * 
     */
    public List<ProtDocumentoProtocollatoType> getAllegati() {
        if (allegati == null) {
            allegati = new ArrayList<ProtDocumentoProtocollatoType>();
        }
        return this.allegati;
    }

    /**
     * Recupera il valore della proprietà isAnnullato.
     * 
     */
    public boolean isIsAnnullato() {
        return isAnnullato;
    }

    /**
     * Imposta il valore della proprietà isAnnullato.
     * 
     */
    public void setIsAnnullato(boolean value) {
        this.isAnnullato = value;
    }

    /**
     * Recupera il valore della proprietà isMozione.
     * 
     */
    public boolean isIsMozione() {
        return isMozione;
    }

    /**
     * Imposta il valore della proprietà isMozione.
     * 
     */
    public void setIsMozione(boolean value) {
        this.isMozione = value;
    }

    /**
     * Recupera il valore della proprietà stato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStato() {
        return stato;
    }

    /**
     * Imposta il valore della proprietà stato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStato(String value) {
        this.stato = value;
    }

    /**
     * Recupera il valore della proprietà sistemaProduttore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaProduttore() {
        return sistemaProduttore;
    }

    /**
     * Imposta il valore della proprietà sistemaProduttore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaProduttore(String value) {
        this.sistemaProduttore = value;
    }

}
