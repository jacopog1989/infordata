
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Definisce il tipo di dato Assegnatario del protocollo
 * 
 * <p>Classe Java per prot_assegnatario_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_assegnatario_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DataAssegnazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="Assegnatario" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type"/>
 *         &lt;element name="Assegnante" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type"/>
 *       &lt;/sequence>
 *       &lt;attribute name="PerCompetenza">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="si"/>
 *             &lt;enumeration value="no"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_assegnatario_type", propOrder = {
    "dataAssegnazione",
    "assegnatario",
    "assegnante"
})
public class ProtAssegnatarioType
    implements Serializable
{

    @XmlElement(name = "DataAssegnazione", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataAssegnazione;
    @XmlElement(name = "Assegnatario", required = true)
    protected OrgOrganigrammaType assegnatario;
    @XmlElement(name = "Assegnante", required = true)
    protected OrgOrganigrammaType assegnante;
    @XmlAttribute(name = "PerCompetenza")
    protected String perCompetenza;

    /**
     * Recupera il valore della proprietà dataAssegnazione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAssegnazione() {
        return dataAssegnazione;
    }

    /**
     * Imposta il valore della proprietà dataAssegnazione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAssegnazione(XMLGregorianCalendar value) {
        this.dataAssegnazione = value;
    }

    /**
     * Recupera il valore della proprietà assegnatario.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getAssegnatario() {
        return assegnatario;
    }

    /**
     * Imposta il valore della proprietà assegnatario.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setAssegnatario(OrgOrganigrammaType value) {
        this.assegnatario = value;
    }

    /**
     * Recupera il valore della proprietà assegnante.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getAssegnante() {
        return assegnante;
    }

    /**
     * Imposta il valore della proprietà assegnante.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setAssegnante(OrgOrganigrammaType value) {
        this.assegnante = value;
    }

    /**
     * Recupera il valore della proprietà perCompetenza.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerCompetenza() {
        return perCompetenza;
    }

    /**
     * Imposta il valore della proprietà perCompetenza.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerCompetenza(String value) {
        this.perCompetenza = value;
    }

}
