
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Criteri di filtro per l'estrazione dei registri provvisori
 * 
 * <p>Classe Java per reg_criteriaRegistrazioneAusiliaria_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="reg_criteriaRegistrazioneAusiliaria_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Anno" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;element name="Registro" type="{http://mef.gov.it.v1.npsTypes}reg_registro_type"/>
 *         &lt;element name="Applicazione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataRegistrazione" type="{http://mef.gov.it.v1.npsTypes}all_dataRange_type" minOccurs="0"/>
 *         &lt;element name="NumeroRegistrazione" type="{http://mef.gov.it.v1.npsTypes}all_numeroRange_type" minOccurs="0"/>
 *         &lt;element name="IsAnnullato" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DataAnnullamento" type="{http://mef.gov.it.v1.npsTypes}all_dataRange_type" minOccurs="0"/>
 *         &lt;element name="Oggetto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipologiaDocumentale" type="{http://mef.gov.it.v1.npsTypes}reg_tipologiaDocumento_type" minOccurs="0"/>
 *         &lt;element name="Operatore" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type" minOccurs="0"/>
 *         &lt;element name="Acl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SistemaProduttore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reg_criteriaRegistrazioneAusiliaria_type", propOrder = {
    "anno",
    "registro",
    "applicazione",
    "dataRegistrazione",
    "numeroRegistrazione",
    "isAnnullato",
    "dataAnnullamento",
    "oggetto",
    "tipologiaDocumentale",
    "operatore",
    "acl",
    "sistemaProduttore"
})
public class RegCriteriaRegistrazioneAusiliariaType
    implements Serializable
{

    @XmlElement(name = "Anno")
    protected short anno;
    @XmlElement(name = "Registro", required = true)
    protected RegRegistroType registro;
    @XmlElement(name = "Applicazione")
    protected String applicazione;
    @XmlElement(name = "DataRegistrazione")
    protected AllDataRangeType dataRegistrazione;
    @XmlElement(name = "NumeroRegistrazione")
    protected AllNumeroRangeType numeroRegistrazione;
    @XmlElement(name = "IsAnnullato", defaultValue = "false")
    protected Boolean isAnnullato;
    @XmlElement(name = "DataAnnullamento")
    protected AllDataRangeType dataAnnullamento;
    @XmlElement(name = "Oggetto")
    protected String oggetto;
    @XmlElement(name = "TipologiaDocumentale")
    protected RegTipologiaDocumentoType tipologiaDocumentale;
    @XmlElement(name = "Operatore")
    protected OrgOrganigrammaType operatore;
    @XmlElement(name = "Acl")
    protected String acl;
    @XmlElement(name = "SistemaProduttore")
    protected String sistemaProduttore;

    /**
     * Recupera il valore della proprietà anno.
     * 
     */
    public short getAnno() {
        return anno;
    }

    /**
     * Imposta il valore della proprietà anno.
     * 
     */
    public void setAnno(short value) {
        this.anno = value;
    }

    /**
     * Recupera il valore della proprietà registro.
     * 
     * @return
     *     possible object is
     *     {@link RegRegistroType }
     *     
     */
    public RegRegistroType getRegistro() {
        return registro;
    }

    /**
     * Imposta il valore della proprietà registro.
     * 
     * @param value
     *     allowed object is
     *     {@link RegRegistroType }
     *     
     */
    public void setRegistro(RegRegistroType value) {
        this.registro = value;
    }

    /**
     * Recupera il valore della proprietà applicazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicazione() {
        return applicazione;
    }

    /**
     * Imposta il valore della proprietà applicazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicazione(String value) {
        this.applicazione = value;
    }

    /**
     * Recupera il valore della proprietà dataRegistrazione.
     * 
     * @return
     *     possible object is
     *     {@link AllDataRangeType }
     *     
     */
    public AllDataRangeType getDataRegistrazione() {
        return dataRegistrazione;
    }

    /**
     * Imposta il valore della proprietà dataRegistrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link AllDataRangeType }
     *     
     */
    public void setDataRegistrazione(AllDataRangeType value) {
        this.dataRegistrazione = value;
    }

    /**
     * Recupera il valore della proprietà numeroRegistrazione.
     * 
     * @return
     *     possible object is
     *     {@link AllNumeroRangeType }
     *     
     */
    public AllNumeroRangeType getNumeroRegistrazione() {
        return numeroRegistrazione;
    }

    /**
     * Imposta il valore della proprietà numeroRegistrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link AllNumeroRangeType }
     *     
     */
    public void setNumeroRegistrazione(AllNumeroRangeType value) {
        this.numeroRegistrazione = value;
    }

    /**
     * Recupera il valore della proprietà isAnnullato.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsAnnullato() {
        return isAnnullato;
    }

    /**
     * Imposta il valore della proprietà isAnnullato.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsAnnullato(Boolean value) {
        this.isAnnullato = value;
    }

    /**
     * Recupera il valore della proprietà dataAnnullamento.
     * 
     * @return
     *     possible object is
     *     {@link AllDataRangeType }
     *     
     */
    public AllDataRangeType getDataAnnullamento() {
        return dataAnnullamento;
    }

    /**
     * Imposta il valore della proprietà dataAnnullamento.
     * 
     * @param value
     *     allowed object is
     *     {@link AllDataRangeType }
     *     
     */
    public void setDataAnnullamento(AllDataRangeType value) {
        this.dataAnnullamento = value;
    }

    /**
     * Recupera il valore della proprietà oggetto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggetto() {
        return oggetto;
    }

    /**
     * Imposta il valore della proprietà oggetto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggetto(String value) {
        this.oggetto = value;
    }

    /**
     * Recupera il valore della proprietà tipologiaDocumentale.
     * 
     * @return
     *     possible object is
     *     {@link RegTipologiaDocumentoType }
     *     
     */
    public RegTipologiaDocumentoType getTipologiaDocumentale() {
        return tipologiaDocumentale;
    }

    /**
     * Imposta il valore della proprietà tipologiaDocumentale.
     * 
     * @param value
     *     allowed object is
     *     {@link RegTipologiaDocumentoType }
     *     
     */
    public void setTipologiaDocumentale(RegTipologiaDocumentoType value) {
        this.tipologiaDocumentale = value;
    }

    /**
     * Recupera il valore della proprietà operatore.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getOperatore() {
        return operatore;
    }

    /**
     * Imposta il valore della proprietà operatore.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setOperatore(OrgOrganigrammaType value) {
        this.operatore = value;
    }

    /**
     * Recupera il valore della proprietà acl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcl() {
        return acl;
    }

    /**
     * Imposta il valore della proprietà acl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcl(String value) {
        this.acl = value;
    }

    /**
     * Recupera il valore della proprietà sistemaProduttore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaProduttore() {
        return sistemaProduttore;
    }

    /**
     * Imposta il valore della proprietà sistemaProduttore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaProduttore(String value) {
        this.sistemaProduttore = value;
    }

}
