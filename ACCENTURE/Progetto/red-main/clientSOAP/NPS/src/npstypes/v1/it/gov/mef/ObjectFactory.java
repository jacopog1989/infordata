
package npstypes.v1.it.gov.mef;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the npstypes.v1.it.gov.mef package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ProtTipologiaDocumentoTypeDescrizione_QNAME = new QName("", "Descrizione");
    private final static QName _ProtAttributiEstesiTypeMetadatiAssociati_QNAME = new QName("", "metadatiAssociati");
    private final static QName _ProtAllaccioProtocolloTypeIdProtocollo_QNAME = new QName("", "IdProtocollo");
    
    private final static QName _ProtDocumentoProtocollatoTypeArchiviazioneSostitutiva_QNAME = new QName("", "ArchiviazioneSostitutiva");
    private final static QName _ProtCriteriaProtocolliTypeTipologiaDocumentale_QNAME = new QName("", "TipologiaDocumentale");
    
    private final static QName _RegRegistrazioneAusiliariaTypeDataRegistrazione_QNAME = new QName("", "DataRegistrazione");
    private final static QName _RegTipologiaDocumentoTypeDescrizione_QNAME = new QName("", "Descrizione");
    private final static QName _RegAttributiEstesiTypeMetadatiAssociati_QNAME = new QName("", "metadatiAssociati");
    private final static QName _RegRegistrazioneAusiliariaMiniTypeCodiceTipoDocumento_QNAME = new QName("", "CodiceTipoDocumento");
    
    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: npstypes.v1.it.gov.mef
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ProtSpedizioneType }
     * 
     */
    public ProtSpedizioneType createProtSpedizioneType() {
        return new ProtSpedizioneType();
    }

    /**
     * Create an instance of {@link ProtSpedizioneType.MezzoSpedizione }
     * 
     */
    public ProtSpedizioneType.MezzoSpedizione createProtSpedizioneTypeMezzoSpedizione() {
        return new ProtSpedizioneType.MezzoSpedizione();
    }

    /**
     * Create an instance of {@link EmailMessageType }
     * 
     */
    public EmailMessageType createEmailMessageType() {
        return new EmailMessageType();
    }

    /**
     * Create an instance of {@link ProtCorrispondenteType }
     * 
     */
    public ProtCorrispondenteType createProtCorrispondenteType() {
        return new ProtCorrispondenteType();
    }

    /**
     * Create an instance of {@link ProtDestinatarioType }
     * 
     */
    public ProtDestinatarioType createProtDestinatarioType() {
        return new ProtDestinatarioType();
    }

    /**
     * Create an instance of {@link OrgOrganigrammaType }
     * 
     */
    public OrgOrganigrammaType createOrgOrganigrammaType() {
        return new OrgOrganigrammaType();
    }

    /**
     * Create an instance of {@link OrgCasellaEmailType }
     * 
     */
    public OrgCasellaEmailType createOrgCasellaEmailType() {
        return new OrgCasellaEmailType();
    }

    /**
     * Create an instance of {@link ProtTipologiaDocumentoType }
     * 
     */
    public ProtTipologiaDocumentoType createProtTipologiaDocumentoType() {
        return new ProtTipologiaDocumentoType();
    }

    /**
     * Create an instance of {@link OrgOrganigrammaAooType }
     * 
     */
    public OrgOrganigrammaAooType createOrgOrganigrammaAooType() {
        return new OrgOrganigrammaAooType();
    }

    /**
     * Create an instance of {@link WkfDatiContestoProceduraleType }
     * 
     */
    public WkfDatiContestoProceduraleType createWkfDatiContestoProceduraleType() {
        return new WkfDatiContestoProceduraleType();
    }

    /**
     * Create an instance of {@link WkfDatiPostaType }
     * 
     */
    public WkfDatiPostaType createWkfDatiPostaType() {
        return new WkfDatiPostaType();
    }

    /**
     * Create an instance of {@link AnaToponimoType }
     * 
     */
    public AnaToponimoType createAnaToponimoType() {
        return new AnaToponimoType();
    }

    /**
     * Create an instance of {@link EmailAllegatiMessaggioType }
     * 
     */
    public EmailAllegatiMessaggioType createEmailAllegatiMessaggioType() {
        return new EmailAllegatiMessaggioType();
    }

    /**
     * Create an instance of {@link AnaIndirizzoType }
     * 
     */
    public AnaIndirizzoType createAnaIndirizzoType() {
        return new AnaIndirizzoType();
    }

    /**
     * Create an instance of {@link ProtRicevutePecType }
     * 
     */
    public ProtRicevutePecType createProtRicevutePecType() {
        return new ProtRicevutePecType();
    }

    /**
     * Create an instance of {@link ProtAllaccioProtocolloType }
     * 
     */
    public ProtAllaccioProtocolloType createProtAllaccioProtocolloType() {
        return new ProtAllaccioProtocolloType();
    }

    /**
     * Create an instance of {@link WkfDatiFlussoType }
     * 
     */
    public WkfDatiFlussoType createWkfDatiFlussoType() {
        return new WkfDatiFlussoType();
    }

    /**
     * Create an instance of {@link WkfDatiMiminiType }
     * 
     */
    public WkfDatiMiminiType createWkfDatiMiminiType() {
        return new WkfDatiMiminiType();
    }

    /**
     * Create an instance of {@link ProtMetadatiType }
     * 
     */
    public ProtMetadatiType createProtMetadatiType() {
        return new ProtMetadatiType();
    }

    /**
     * Create an instance of {@link DocDocumentoFileType }
     * 
     */
    public DocDocumentoFileType createDocDocumentoFileType() {
        return new DocDocumentoFileType();
    }

    /**
     * Create an instance of {@link OrgOrganigrammaUnitaOrganizzativaType }
     * 
     */
    public OrgOrganigrammaUnitaOrganizzativaType createOrgOrganigrammaUnitaOrganizzativaType() {
        return new OrgOrganigrammaUnitaOrganizzativaType();
    }

    /**
     * Create an instance of {@link ProtRegistroProtocolloType }
     * 
     */
    public ProtRegistroProtocolloType createProtRegistroProtocolloType() {
        return new ProtRegistroProtocolloType();
    }

    /**
     * Create an instance of {@link AnaEnteEsternoType }
     * 
     */
    public AnaEnteEsternoType createAnaEnteEsternoType() {
        return new AnaEnteEsternoType();
    }

    /**
     * Create an instance of {@link OrgUnitaOrganizzativaType }
     * 
     */
    public OrgUnitaOrganizzativaType createOrgUnitaOrganizzativaType() {
        return new OrgUnitaOrganizzativaType();
    }

    /**
     * Create an instance of {@link WkfDatiOperazioneFlussoType }
     * 
     */
    public WkfDatiOperazioneFlussoType createWkfDatiOperazioneFlussoType() {
        return new WkfDatiOperazioneFlussoType();
    }

    /**
     * Create an instance of {@link ProtIdentificatoreProtocolloType }
     * 
     */
    public ProtIdentificatoreProtocolloType createProtIdentificatoreProtocolloType() {
        return new ProtIdentificatoreProtocolloType();
    }

    /**
     * Create an instance of {@link AnaIndirizzoPostaleType }
     * 
     */
    public AnaIndirizzoPostaleType createAnaIndirizzoPostaleType() {
        return new AnaIndirizzoPostaleType();
    }

    /**
     * Create an instance of {@link AllChiaveEsternaType }
     * 
     */
    public AllChiaveEsternaType createAllChiaveEsternaType() {
        return new AllChiaveEsternaType();
    }

    /**
     * Create an instance of {@link ProtMetadatoAssociatoType }
     * 
     */
    public ProtMetadatoAssociatoType createProtMetadatoAssociatoType() {
        return new ProtMetadatoAssociatoType();
    }

    /**
     * Create an instance of {@link AllNumeroRangeType }
     * 
     */
    public AllNumeroRangeType createAllNumeroRangeType() {
        return new AllNumeroRangeType();
    }

    /**
     * Create an instance of {@link WkfDatiFlussoEntrataType }
     * 
     */
    public WkfDatiFlussoEntrataType createWkfDatiFlussoEntrataType() {
        return new WkfDatiFlussoEntrataType();
    }

    /**
     * Create an instance of {@link AllDataRangeType }
     * 
     */
    public AllDataRangeType createAllDataRangeType() {
        return new AllDataRangeType();
    }

    /**
     * Create an instance of {@link WkfDatiSegnaturaType }
     * 
     */
    public WkfDatiSegnaturaType createWkfDatiSegnaturaType() {
        return new WkfDatiSegnaturaType();
    }

    /**
     * Create an instance of {@link AnaPersonaFisicaType }
     * 
     */
    public AnaPersonaFisicaType createAnaPersonaFisicaType() {
        return new AnaPersonaFisicaType();
    }

    /**
     * Create an instance of {@link WkfDatiTipoFlussoType }
     * 
     */
    public WkfDatiTipoFlussoType createWkfDatiTipoFlussoType() {
        return new WkfDatiTipoFlussoType();
    }

    /**
     * Create an instance of {@link WkfDatiErroreFlussoType }
     * 
     */
    public WkfDatiErroreFlussoType createWkfDatiErroreFlussoType() {
        return new WkfDatiErroreFlussoType();
    }

    /**
     * Create an instance of {@link OrgAooType }
     * 
     */
    public OrgAooType createOrgAooType() {
        return new OrgAooType();
    }

    /**
     * Create an instance of {@link AnaPersonaGiuridicaType }
     * 
     */
    public AnaPersonaGiuridicaType createAnaPersonaGiuridicaType() {
        return new AnaPersonaGiuridicaType();
    }

    /**
     * Create an instance of {@link ProtRicevuteSegnaturaType }
     * 
     */
    public ProtRicevuteSegnaturaType createProtRicevuteSegnaturaType() {
        return new ProtRicevuteSegnaturaType();
    }

    /**
     * Create an instance of {@link OrgAmministrazioneType }
     * 
     */
    public OrgAmministrazioneType createOrgAmministrazioneType() {
        return new OrgAmministrazioneType();
    }

    /**
     * Create an instance of {@link EmailIndirizzoEmailType }
     * 
     */
    public EmailIndirizzoEmailType createEmailIndirizzoEmailType() {
        return new EmailIndirizzoEmailType();
    }

    /**
     * Create an instance of {@link ProtAnnotazioneType }
     * 
     */
    public ProtAnnotazioneType createProtAnnotazioneType() {
        return new ProtAnnotazioneType();
    }

    /**
     * Create an instance of {@link ProtProtocolloDatiMinimiType }
     * 
     */
    public ProtProtocolloDatiMinimiType createProtProtocolloDatiMinimiType() {
        return new ProtProtocolloDatiMinimiType();
    }

    /**
     * Create an instance of {@link WkfDestinatarioFlussoType }
     * 
     */
    public WkfDestinatarioFlussoType createWkfDestinatarioFlussoType() {
        return new WkfDestinatarioFlussoType();
    }

    /**
     * Create an instance of {@link WkfDatiFlussoInteroperabilitaType }
     * 
     */
    public WkfDatiFlussoInteroperabilitaType createWkfDatiFlussoInteroperabilitaType() {
        return new WkfDatiFlussoInteroperabilitaType();
    }

    /**
     * Create an instance of {@link AnaIndirizzoTelematicoType }
     * 
     */
    public AnaIndirizzoTelematicoType createAnaIndirizzoTelematicoType() {
        return new AnaIndirizzoTelematicoType();
    }

    /**
     * Create an instance of {@link WkfDatiFlussoNotificaType }
     * 
     */
    public WkfDatiFlussoNotificaType createWkfDatiFlussoNotificaType() {
        return new WkfDatiFlussoNotificaType();
    }

    /**
     * Create an instance of {@link WkfDatiValidazioneFlussoType }
     * 
     */
    public WkfDatiValidazioneFlussoType createWkfDatiValidazioneFlussoType() {
        return new WkfDatiValidazioneFlussoType();
    }

    /**
     * Create an instance of {@link AllCodiceDescrizioneType }
     * 
     */
    public AllCodiceDescrizioneType createAllCodiceDescrizioneType() {
        return new AllCodiceDescrizioneType();
    }

    /**
     * Create an instance of {@link WkfAcquisisciDocumentoType }
     * 
     */
    public WkfAcquisisciDocumentoType createWkfAcquisisciDocumentoType() {
        return new WkfAcquisisciDocumentoType();
    }

    /**
     * Create an instance of {@link ProtAttributiEstesiType }
     * 
     */
    public ProtAttributiEstesiType createProtAttributiEstesiType() {
        return new ProtAttributiEstesiType();
    }

    /**
     * Create an instance of {@link WkfIdentificatoreProtcolloType }
     * 
     */
    public WkfIdentificatoreProtcolloType createWkfIdentificatoreProtcolloType() {
        return new WkfIdentificatoreProtcolloType();
    }

    /**
     * Create an instance of {@link ProtSpedizioneType.MezzoSpedizione.Elettronico }
     * 
     */
    public ProtSpedizioneType.MezzoSpedizione.Elettronico createProtSpedizioneTypeMezzoSpedizioneElettronico() {
        return new ProtSpedizioneType.MezzoSpedizione.Elettronico();
    }

    /**
     * Create an instance of {@link EmailMessageType.AllegatiMessaggio }
     * 
     */
    public EmailMessageType.AllegatiMessaggio createEmailMessageTypeAllegatiMessaggio() {
        return new EmailMessageType.AllegatiMessaggio();
    }

    /**
     * Create an instance of {@link ProtCorrispondenteType.DatiAnnullamento }
     * 
     */
    public ProtCorrispondenteType.DatiAnnullamento createProtCorrispondenteTypeDatiAnnullamento() {
        return new ProtCorrispondenteType.DatiAnnullamento();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Descrizione", scope = ProtTipologiaDocumentoType.class)
    public JAXBElement<String> createProtTipologiaDocumentoTypeDescrizione(String value) {
        return new JAXBElement<String>(_ProtTipologiaDocumentoTypeDescrizione_QNAME, String.class, ProtTipologiaDocumentoType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProtMetadatiType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "metadatiAssociati", scope = ProtAttributiEstesiType.class)
    public JAXBElement<ProtMetadatiType> createProtAttributiEstesiTypeMetadatiAssociati(ProtMetadatiType value) {
        return new JAXBElement<ProtMetadatiType>(_ProtAttributiEstesiTypeMetadatiAssociati_QNAME, ProtMetadatiType.class, ProtAttributiEstesiType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "IdProtocollo", scope = ProtAllaccioProtocolloType.class)
    public JAXBElement<String> createProtAllaccioProtocolloTypeIdProtocollo(String value) {
        return new JAXBElement<String>(_ProtAllaccioProtocolloTypeIdProtocollo_QNAME, String.class, ProtAllaccioProtocolloType.class, value);
    }
    
    /**
     * Create an instance of {@link ProtProtocolloBaseType }
     * 
     */
    public ProtProtocolloBaseType createProtProtocolloBaseType() {
        return new ProtProtocolloBaseType();
    }

    /**
     * Create an instance of {@link ProtProtocolloBaseType.SegnaturaEstesa }
     * 
     */
    public ProtProtocolloBaseType.SegnaturaEstesa createProtProtocolloBaseTypeSegnaturaEstesa() {
        return new ProtProtocolloBaseType.SegnaturaEstesa();
    }

    /**
     * Create an instance of {@link ProtCriteriaProtocolliType }
     * 
     */
    public ProtCriteriaProtocolliType createProtCriteriaProtocolliType() {
        return new ProtCriteriaProtocolliType();
    }

    /**
     * Create an instance of {@link ProtCriteriaProtocolliType.DatiSpedizione }
     * 
     */
    public ProtCriteriaProtocolliType.DatiSpedizione createProtCriteriaProtocolliTypeDatiSpedizione() {
        return new ProtCriteriaProtocolliType.DatiSpedizione();
    }

    /**
     * Create an instance of {@link ProtCriteriaProtocolliType.DatiSpedizione.MezzoSpedizione }
     * 
     */
    public ProtCriteriaProtocolliType.DatiSpedizione.MezzoSpedizione createProtCriteriaProtocolliTypeDatiSpedizioneMezzoSpedizione() {
        return new ProtCriteriaProtocolliType.DatiSpedizione.MezzoSpedizione();
    }

    /**
     * Create an instance of {@link DocRichiestaOperazioneDocumentaleType }
     * 
     */
    public DocRichiestaOperazioneDocumentaleType createDocRichiestaOperazioneDocumentaleType() {
        return new DocRichiestaOperazioneDocumentaleType();
    }

    /**
     * Create an instance of {@link ProtProtocolloGenericoResponseType }
     * 
     */
    public ProtProtocolloGenericoResponseType createProtProtocolloGenericoResponseType() {
        return new ProtProtocolloGenericoResponseType();
    }

    /**
     * Create an instance of {@link ProtProtocolloGenericoResponseType.SegnaturaEstesa }
     * 
     */
    public ProtProtocolloGenericoResponseType.SegnaturaEstesa createProtProtocolloGenericoResponseTypeSegnaturaEstesa() {
        return new ProtProtocolloGenericoResponseType.SegnaturaEstesa();
    }

    /**
     * Create an instance of {@link RegRegistroProtocolloType }
     * 
     */
    public RegRegistroProtocolloType createRegRegistroProtocolloType() {
        return new RegRegistroProtocolloType();
    }

    /**
     * Create an instance of {@link AllTipoDocumentoType }
     * 
     */
    public AllTipoDocumentoType createAllTipoDocumentoType() {
        return new AllTipoDocumentoType();
    }

    /**
     * Create an instance of {@link ProtProtocolloBaseResponseType }
     * 
     */
    public ProtProtocolloBaseResponseType createProtProtocolloBaseResponseType() {
        return new ProtProtocolloBaseResponseType();
    }

    /**
     * Create an instance of {@link DocOperazioneDocumentaleType }
     * 
     */
    public DocOperazioneDocumentaleType createDocOperazioneDocumentaleType() {
        return new DocOperazioneDocumentaleType();
    }

    /**
     * Create an instance of {@link ProtDocumentoDaProtocollareType }
     * 
     */
    public ProtDocumentoDaProtocollareType createProtDocumentoDaProtocollareType() {
        return new ProtDocumentoDaProtocollareType();
    }

    /**
     * Create an instance of {@link ProtDocumentoMetadataProtocollatoType }
     * 
     */
    public ProtDocumentoMetadataProtocollatoType createProtDocumentoMetadataProtocollatoType() {
        return new ProtDocumentoMetadataProtocollatoType();
    }

    /**
     * Create an instance of {@link ProtDocumentoProtocollatoType }
     * 
     */
    public ProtDocumentoProtocollatoType createProtDocumentoProtocollatoType() {
        return new ProtDocumentoProtocollatoType();
    }

    /**
     * Create an instance of {@link DocDocumentoType }
     * 
     */
    public DocDocumentoType createDocDocumentoType() {
        return new DocDocumentoType();
    }

    /**
     * Create an instance of {@link ProtAnnullamentoType }
     * 
     */
    public ProtAnnullamentoType createProtAnnullamentoType() {
        return new ProtAnnullamentoType();
    }

    /**
     * Create an instance of {@link ProtRicezioneType }
     * 
     */
    public ProtRicezioneType createProtRicezioneType() {
        return new ProtRicezioneType();
    }

    /**
     * Create an instance of {@link DocConversioneContentType }
     * 
     */
    public DocConversioneContentType createDocConversioneContentType() {
        return new DocConversioneContentType();
    }

    /**
     * Create an instance of {@link ProtDettaglioOperazioniProtocolloType }
     * 
     */
    public ProtDettaglioOperazioniProtocolloType createProtDettaglioOperazioniProtocolloType() {
        return new ProtDettaglioOperazioniProtocolloType();
    }

    /**
     * Create an instance of {@link ProtProtocolloUscitaType }
     * 
     */
    public ProtProtocolloUscitaType createProtProtocolloUscitaType() {
        return new ProtProtocolloUscitaType();
    }

    /**
     * Create an instance of {@link ProtProtocolloEntrataType }
     * 
     */
    public ProtProtocolloEntrataType createProtProtocolloEntrataType() {
        return new ProtProtocolloEntrataType();
    }

    /**
     * Create an instance of {@link ProtRisultatoRicercaProtocolloType }
     * 
     */
    public ProtRisultatoRicercaProtocolloType createProtRisultatoRicercaProtocolloType() {
        return new ProtRisultatoRicercaProtocolloType();
    }

    /**
     * Create an instance of {@link ProtProtocolloBaseRequestType }
     * 
     */
    public ProtProtocolloBaseRequestType createProtProtocolloBaseRequestType() {
        return new ProtProtocolloBaseRequestType();
    }

    /**
     * Create an instance of {@link ProtProtocolloUscitaResponseType }
     * 
     */
    public ProtProtocolloUscitaResponseType createProtProtocolloUscitaResponseType() {
        return new ProtProtocolloUscitaResponseType();
    }

    /**
     * Create an instance of {@link ProtMittenteType }
     * 
     */
    public ProtMittenteType createProtMittenteType() {
        return new ProtMittenteType();
    }

    /**
     * Create an instance of {@link RegRegistroAusiliarioType }
     * 
     */
    public RegRegistroAusiliarioType createRegRegistroAusiliarioType() {
        return new RegRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link ProtProtocolloEntrataResponseType }
     * 
     */
    public ProtProtocolloEntrataResponseType createProtProtocolloEntrataResponseType() {
        return new ProtProtocolloEntrataResponseType();
    }

    /**
     * Create an instance of {@link EmailCodaOutType }
     * 
     */
    public EmailCodaOutType createEmailCodaOutType() {
        return new EmailCodaOutType();
    }

    /**
     * Create an instance of {@link DocDocumentoContentType }
     * 
     */
    public DocDocumentoContentType createDocDocumentoContentType() {
        return new DocDocumentoContentType();
    }

    /**
     * Create an instance of {@link ProtRegistroOperazioniProtocolloType }
     * 
     */
    public ProtRegistroOperazioniProtocolloType createProtRegistroOperazioniProtocolloType() {
        return new ProtRegistroOperazioniProtocolloType();
    }

    /**
     * Create an instance of {@link DocDocumentoResultType }
     * 
     */
    public DocDocumentoResultType createDocDocumentoResultType() {
        return new DocDocumentoResultType();
    }

    /**
     * Create an instance of {@link EmailCodaInType }
     * 
     */
    public EmailCodaInType createEmailCodaInType() {
        return new EmailCodaInType();
    }

    /**
     * Create an instance of {@link ProtArchiviazioneSostitutivaType }
     * 
     */
    public ProtArchiviazioneSostitutivaType createProtArchiviazioneSostitutivaType() {
        return new ProtArchiviazioneSostitutivaType();
    }

    /**
     * Create an instance of {@link ProtAssegnatarioType }
     * 
     */
    public ProtAssegnatarioType createProtAssegnatarioType() {
        return new ProtAssegnatarioType();
    }

    /**
     * Create an instance of {@link ProtProtocolloBaseType.RegistroEmergenza }
     * 
     */
    public ProtProtocolloBaseType.RegistroEmergenza createProtProtocolloBaseTypeRegistroEmergenza() {
        return new ProtProtocolloBaseType.RegistroEmergenza();
    }

    /**
     * Create an instance of {@link ProtProtocolloBaseType.SegnaturaEstesa.Intestazione }
     * 
     */
    public ProtProtocolloBaseType.SegnaturaEstesa.Intestazione createProtProtocolloBaseTypeSegnaturaEstesaIntestazione() {
        return new ProtProtocolloBaseType.SegnaturaEstesa.Intestazione();
    }

    /**
     * Create an instance of {@link ProtCriteriaProtocolliType.DatiRicezione }
     * 
     */
    public ProtCriteriaProtocolliType.DatiRicezione createProtCriteriaProtocolliTypeDatiRicezione() {
        return new ProtCriteriaProtocolliType.DatiRicezione();
    }

    /**
     * Create an instance of {@link ProtCriteriaProtocolliType.DatiAssegnazione }
     * 
     */
    public ProtCriteriaProtocolliType.DatiAssegnazione createProtCriteriaProtocolliTypeDatiAssegnazione() {
        return new ProtCriteriaProtocolliType.DatiAssegnazione();
    }

    /**
     * Create an instance of {@link ProtCriteriaProtocolliType.DatiAssegnazioneApplicazione }
     * 
     */
    public ProtCriteriaProtocolliType.DatiAssegnazioneApplicazione createProtCriteriaProtocolliTypeDatiAssegnazioneApplicazione() {
        return new ProtCriteriaProtocolliType.DatiAssegnazioneApplicazione();
    }

    /**
     * Create an instance of {@link ProtCriteriaProtocolliType.RegistroEmergenza }
     * 
     */
    public ProtCriteriaProtocolliType.RegistroEmergenza createProtCriteriaProtocolliTypeRegistroEmergenza() {
        return new ProtCriteriaProtocolliType.RegistroEmergenza();
    }

    /**
     * Create an instance of {@link ProtCriteriaProtocolliType.DatiSpedizione.MezzoSpedizione.Elettronico }
     * 
     */
    public ProtCriteriaProtocolliType.DatiSpedizione.MezzoSpedizione.Elettronico createProtCriteriaProtocolliTypeDatiSpedizioneMezzoSpedizioneElettronico() {
        return new ProtCriteriaProtocolliType.DatiSpedizione.MezzoSpedizione.Elettronico();
    }

    /**
     * Create an instance of {@link DocRichiestaOperazioneDocumentaleType.ParametriOperazione }
     * 
     */
    public DocRichiestaOperazioneDocumentaleType.ParametriOperazione createDocRichiestaOperazioneDocumentaleTypeParametriOperazione() {
        return new DocRichiestaOperazioneDocumentaleType.ParametriOperazione();
    }

    /**
     * Create an instance of {@link ProtProtocolloGenericoResponseType.SegnaturaEstesa.Intestazione }
     * 
     */
    public ProtProtocolloGenericoResponseType.SegnaturaEstesa.Intestazione createProtProtocolloGenericoResponseTypeSegnaturaEstesaIntestazione() {
        return new ProtProtocolloGenericoResponseType.SegnaturaEstesa.Intestazione();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProtArchiviazioneSostitutivaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArchiviazioneSostitutiva", scope = ProtDocumentoProtocollatoType.class)
    public JAXBElement<ProtArchiviazioneSostitutivaType> createProtDocumentoProtocollatoTypeArchiviazioneSostitutiva(ProtArchiviazioneSostitutivaType value) {
        return new JAXBElement<ProtArchiviazioneSostitutivaType>(_ProtDocumentoProtocollatoTypeArchiviazioneSostitutiva_QNAME, ProtArchiviazioneSostitutivaType.class, ProtDocumentoProtocollatoType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProtArchiviazioneSostitutivaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "ArchiviazioneSostitutiva", scope = ProtProtocolloBaseType.class)
    public JAXBElement<ProtArchiviazioneSostitutivaType> createProtProtocolloBaseTypeArchiviazioneSostitutiva(ProtArchiviazioneSostitutivaType value) {
        return new JAXBElement<ProtArchiviazioneSostitutivaType>(_ProtDocumentoProtocollatoTypeArchiviazioneSostitutiva_QNAME, ProtArchiviazioneSostitutivaType.class, ProtProtocolloBaseType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProtTipologiaDocumentoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "TipologiaDocumentale", scope = ProtCriteriaProtocolliType.class)
    public JAXBElement<ProtTipologiaDocumentoType> createProtCriteriaProtocolliTypeTipologiaDocumentale(ProtTipologiaDocumentoType value) {
        return new JAXBElement<ProtTipologiaDocumentoType>(_ProtCriteriaProtocolliTypeTipologiaDocumentale_QNAME, ProtTipologiaDocumentoType.class, ProtCriteriaProtocolliType.class, value);
    }
    
    /**
     * Create an instance of {@link RegAttributiEstesiType }
     * 
     */
    public RegAttributiEstesiType createRegAttributiEstesiType() {
        return new RegAttributiEstesiType();
    }

    /**
     * Create an instance of {@link RegDocumentoType }
     * 
     */
    public RegDocumentoType createRegDocumentoType() {
        return new RegDocumentoType();
    }

    /**
     * Create an instance of {@link RegMetadatiType }
     * 
     */
    public RegMetadatiType createRegMetadatiType() {
        return new RegMetadatiType();
    }

    /**
     * Create an instance of {@link RegCriteriaRegistrazioneAusiliariaType }
     * 
     */
    public RegCriteriaRegistrazioneAusiliariaType createRegCriteriaRegistrazioneAusiliariaType() {
        return new RegCriteriaRegistrazioneAusiliariaType();
    }

    /**
     * Create an instance of {@link RegRegistroAusiliarioMiniType }
     * 
     */
    public RegRegistroAusiliarioMiniType createRegRegistroAusiliarioMiniType() {
        return new RegRegistroAusiliarioMiniType();
    }

    /**
     * Create an instance of {@link RegRegistroType }
     * 
     */
    public RegRegistroType createRegRegistroType() {
        return new RegRegistroType();
    }

    /**
     * Create an instance of {@link RegRegistrazioneAusiliariaType }
     * 
     */
    public RegRegistrazioneAusiliariaType createRegRegistrazioneAusiliariaType() {
        return new RegRegistrazioneAusiliariaType();
    }

    /**
     * Create an instance of {@link RegIdentificatoreRegistroAusiliarioType }
     * 
     */
    public RegIdentificatoreRegistroAusiliarioType createRegIdentificatoreRegistroAusiliarioType() {
        return new RegIdentificatoreRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link RegTipologiaDocumentoType }
     * 
     */
    public RegTipologiaDocumentoType createRegTipologiaDocumentoType() {
        return new RegTipologiaDocumentoType();
    }

    /**
     * Create an instance of {@link RegMetadatoAssociatoType }
     * 
     */
    public RegMetadatoAssociatoType createRegMetadatoAssociatoType() {
        return new RegMetadatoAssociatoType();
    }

    /**
     * Create an instance of {@link RegIdentificatoreRegistrazioneAusiliariaType }
     * 
     */
    public RegIdentificatoreRegistrazioneAusiliariaType createRegIdentificatoreRegistrazioneAusiliariaType() {
        return new RegIdentificatoreRegistrazioneAusiliariaType();
    }

    /**
     * Create an instance of {@link RegRegistrazioneAusiliariaMiniType }
     * 
     */
    public RegRegistrazioneAusiliariaMiniType createRegRegistrazioneAusiliariaMiniType() {
        return new RegRegistrazioneAusiliariaMiniType();
    }

    /**
     * Create an instance of {@link RegCriteriaRegistroAusiliarioType }
     * 
     */
    public RegCriteriaRegistroAusiliarioType createRegCriteriaRegistroAusiliarioType() {
        return new RegCriteriaRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link IdentificativoDocumentoRequestType }
     * 
     */
    public IdentificativoDocumentoRequestType createIdentificativoDocumentoRequestType() {
        return new IdentificativoDocumentoRequestType();
    }

    /**
     * Create an instance of {@link IdentificativoProtocolloRequestType }
     * 
     */
    public IdentificativoProtocolloRequestType createIdentificativoProtocolloRequestType() {
        return new IdentificativoProtocolloRequestType();
    }
    
    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DataRegistrazione", scope = RegRegistrazioneAusiliariaType.class)
    public JAXBElement<XMLGregorianCalendar> createRegRegistrazioneAusiliariaTypeDataRegistrazione(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RegRegistrazioneAusiliariaTypeDataRegistrazione_QNAME, XMLGregorianCalendar.class, RegRegistrazioneAusiliariaType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "Descrizione", scope = RegTipologiaDocumentoType.class)
    public JAXBElement<String> createRegTipologiaDocumentoTypeDescrizione(String value) {
        return new JAXBElement<String>(_RegTipologiaDocumentoTypeDescrizione_QNAME, String.class, RegTipologiaDocumentoType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegMetadatiType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "metadatiAssociati", scope = RegAttributiEstesiType.class)
    public JAXBElement<RegMetadatiType> createRegAttributiEstesiTypeMetadatiAssociati(RegMetadatiType value) {
        return new JAXBElement<RegMetadatiType>(_RegAttributiEstesiTypeMetadatiAssociati_QNAME, RegMetadatiType.class, RegAttributiEstesiType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "CodiceTipoDocumento", scope = RegRegistrazioneAusiliariaMiniType.class)
    public JAXBElement<String> createRegRegistrazioneAusiliariaMiniTypeCodiceTipoDocumento(String value) {
        return new JAXBElement<String>(_RegRegistrazioneAusiliariaMiniTypeCodiceTipoDocumento_QNAME, String.class, RegRegistrazioneAusiliariaMiniType.class, value);
    }

}
