
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce il tipo di dato del registro di protocollo
 * 
 * <p>Classe Java per prot_registroProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_registroProtocollo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Aoo" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_aoo_type"/>
 *         &lt;element name="Codice" type="{http://mef.gov.it.v1.npsTypes}all_codiceDescrizione_type"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Ufficiale">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="si"/>
 *             &lt;enumeration value="no"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_registroProtocollo_type", propOrder = {
    "aoo",
    "codice"
})
public class ProtRegistroProtocolloType
    implements Serializable
{

    @XmlElement(name = "Aoo", required = true)
    protected OrgOrganigrammaAooType aoo;
    @XmlElement(name = "Codice", required = true)
    protected AllCodiceDescrizioneType codice;
    @XmlAttribute(name = "Ufficiale")
    protected String ufficiale;

    /**
     * Recupera il valore della proprietà aoo.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaAooType }
     *     
     */
    public OrgOrganigrammaAooType getAoo() {
        return aoo;
    }

    /**
     * Imposta il valore della proprietà aoo.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaAooType }
     *     
     */
    public void setAoo(OrgOrganigrammaAooType value) {
        this.aoo = value;
    }

    /**
     * Recupera il valore della proprietà codice.
     * 
     * @return
     *     possible object is
     *     {@link AllCodiceDescrizioneType }
     *     
     */
    public AllCodiceDescrizioneType getCodice() {
        return codice;
    }

    /**
     * Imposta il valore della proprietà codice.
     * 
     * @param value
     *     allowed object is
     *     {@link AllCodiceDescrizioneType }
     *     
     */
    public void setCodice(AllCodiceDescrizioneType value) {
        this.codice = value;
    }

    /**
     * Recupera il valore della proprietà ufficiale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUfficiale() {
        return ufficiale;
    }

    /**
     * Imposta il valore della proprietà ufficiale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUfficiale(String value) {
        this.ufficiale = value;
    }

}
