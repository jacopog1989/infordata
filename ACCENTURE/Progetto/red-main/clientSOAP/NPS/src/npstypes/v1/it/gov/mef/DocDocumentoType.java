
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import npsmessages.v1.it.gov.mef.RichiestaUploadDocumentoType;


/**
 * Definisce il tipo di dato Documento
 * 
 * <p>Classe Java per doc_documento_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="doc_documento_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}doc_documentoContent_type">
 *       &lt;sequence>
 *         &lt;element name="DatiDocumento" type="{http://mef.gov.it.v1.npsTypes}prot_documentoMetadataProtocollato_type" minOccurs="0"/>
 *         &lt;element name="Condivisibile" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Attivo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Operazione" type="{http://mef.gov.it.v1.npsTypes}doc_richiestaOperazioneDocumentale_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "doc_documento_type", propOrder = {
    "datiDocumento",
    "condivisibile",
    "attivo",
    "operazione"
})
@XmlSeeAlso({
    RichiestaUploadDocumentoType.class
})
public class DocDocumentoType
    extends DocDocumentoContentType
    implements Serializable
{

    @XmlElement(name = "DatiDocumento")
    protected ProtDocumentoMetadataProtocollatoType datiDocumento;
    @XmlElement(name = "Condivisibile", defaultValue = "true")
    protected boolean condivisibile;
    @XmlElement(name = "Attivo", defaultValue = "true")
    protected boolean attivo;
    @XmlElement(name = "Operazione")
    protected DocRichiestaOperazioneDocumentaleType operazione;

    /**
     * Recupera il valore della proprietà datiDocumento.
     * 
     * @return
     *     possible object is
     *     {@link ProtDocumentoMetadataProtocollatoType }
     *     
     */
    public ProtDocumentoMetadataProtocollatoType getDatiDocumento() {
        return datiDocumento;
    }

    /**
     * Imposta il valore della proprietà datiDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtDocumentoMetadataProtocollatoType }
     *     
     */
    public void setDatiDocumento(ProtDocumentoMetadataProtocollatoType value) {
        this.datiDocumento = value;
    }

    /**
     * Recupera il valore della proprietà condivisibile.
     * 
     */
    public boolean isCondivisibile() {
        return condivisibile;
    }

    /**
     * Imposta il valore della proprietà condivisibile.
     * 
     */
    public void setCondivisibile(boolean value) {
        this.condivisibile = value;
    }

    /**
     * Recupera il valore della proprietà attivo.
     * 
     */
    public boolean isAttivo() {
        return attivo;
    }

    /**
     * Imposta il valore della proprietà attivo.
     * 
     */
    public void setAttivo(boolean value) {
        this.attivo = value;
    }

    /**
     * Recupera il valore della proprietà operazione.
     * 
     * @return
     *     possible object is
     *     {@link DocRichiestaOperazioneDocumentaleType }
     *     
     */
    public DocRichiestaOperazioneDocumentaleType getOperazione() {
        return operazione;
    }

    /**
     * Imposta il valore della proprietà operazione.
     * 
     * @param value
     *     allowed object is
     *     {@link DocRichiestaOperazioneDocumentaleType }
     *     
     */
    public void setOperazione(DocRichiestaOperazioneDocumentaleType value) {
        this.operazione = value;
    }

}
