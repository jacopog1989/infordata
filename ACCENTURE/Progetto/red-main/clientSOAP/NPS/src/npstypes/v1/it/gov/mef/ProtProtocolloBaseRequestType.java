
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce i dati di protocollo base da utilizzare come richiesta
 * 
 * <p>Classe Java per prot_protocolloBaseRequest_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_protocolloBaseRequest_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}prot_protocolloBase_type">
 *       &lt;sequence>
 *         &lt;element name="DocumentoPrincipale" type="{http://mef.gov.it.v1.npsTypes}prot_documentoDaProtocollare_type" minOccurs="0"/>
 *         &lt;element name="Allegati" type="{http://mef.gov.it.v1.npsTypes}prot_documentoDaProtocollare_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Stato">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="DA_ASSEGNARE"/>
 *               &lt;enumeration value="IN_LAVORAZIONE"/>
 *               &lt;enumeration value="AGLI_ATTI"/>
 *               &lt;enumeration value="CHIUSO"/>
 *               &lt;enumeration value="DA_SPEDIRE"/>
 *               &lt;enumeration value="INVIATO"/>
 *               &lt;enumeration value="SPEDITO"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_protocolloBaseRequest_type", propOrder = {
    "documentoPrincipale",
    "allegati",
    "stato"
})
@XmlSeeAlso({
    ProtProtocolloEntrataType.class,
    ProtProtocolloUscitaType.class
})
public class ProtProtocolloBaseRequestType
    extends ProtProtocolloBaseType
    implements Serializable
{

    @XmlElement(name = "DocumentoPrincipale")
    protected ProtDocumentoDaProtocollareType documentoPrincipale;
    @XmlElement(name = "Allegati")
    protected List<ProtDocumentoDaProtocollareType> allegati;
    @XmlElement(name = "Stato", required = true)
    protected String stato;

    /**
     * Recupera il valore della proprietà documentoPrincipale.
     * 
     * @return
     *     possible object is
     *     {@link ProtDocumentoDaProtocollareType }
     *     
     */
    public ProtDocumentoDaProtocollareType getDocumentoPrincipale() {
        return documentoPrincipale;
    }

    /**
     * Imposta il valore della proprietà documentoPrincipale.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtDocumentoDaProtocollareType }
     *     
     */
    public void setDocumentoPrincipale(ProtDocumentoDaProtocollareType value) {
        this.documentoPrincipale = value;
    }

    /**
     * Gets the value of the allegati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allegati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllegati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtDocumentoDaProtocollareType }
     * 
     * 
     */
    public List<ProtDocumentoDaProtocollareType> getAllegati() {
        if (allegati == null) {
            allegati = new ArrayList<ProtDocumentoDaProtocollareType>();
        }
        return this.allegati;
    }

    /**
     * Recupera il valore della proprietà stato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStato() {
        return stato;
    }

    /**
     * Imposta il valore della proprietà stato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStato(String value) {
        this.stato = value;
    }

}
