
package npstypes.v1.it.gov.mef;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per all_tipoElenco_type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="all_tipoElenco_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ALL"/>
 *     &lt;enumeration value="SOURCE"/>
 *     &lt;enumeration value="VISIBLE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "all_tipoElenco_type")
@XmlEnum
public enum AllTipoElencoType {

    ALL,
    SOURCE,
    VISIBLE;

    public String value() {
        return name();
    }

    public static AllTipoElencoType fromValue(String v) {
        return valueOf(v);
    }

}
