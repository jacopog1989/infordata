
package npstypes.v1.it.gov.mef;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per all_tipoNumerazioneRegistroAusiliario_type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="all_tipoNumerazioneRegistroAusiliario_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PROGRESSIVA"/>
 *     &lt;enumeration value="ANNUALE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "all_tipoNumerazioneRegistroAusiliario_type")
@XmlEnum
public enum AllTipoNumerazioneRegistroAusiliarioType {

    PROGRESSIVA,
    ANNUALE;

    public String value() {
        return name();
    }

    public static AllTipoNumerazioneRegistroAusiliarioType fromValue(String v) {
        return valueOf(v);
    }

}
