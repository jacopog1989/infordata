
package npstypes.v1.it.gov.mef;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per email_statoSpedizione_type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="email_statoSpedizione_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DA_SPEDIRE"/>
 *     &lt;enumeration value="INVIATO"/>
 *     &lt;enumeration value="SPEDITO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "email_statoSpedizione_type")
@XmlEnum
public enum EmailStatoSpedizioneType {

    DA_SPEDIRE,
    INVIATO,
    SPEDITO;

    public String value() {
        return name();
    }

    public static EmailStatoSpedizioneType fromValue(String v) {
        return valueOf(v);
    }

}
