
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Dati del messaggio di posta
 * 
 * <p>Classe Java per email_datiPostaUscita_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="email_datiPostaUscita_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdMessaggio" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="IdDocumento" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="DataAccodamento" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="Messaggio" type="{http://mef.gov.it.v1.npsTypes}email_message_type" minOccurs="0"/>
 *         &lt;element name="ChiaveFascicolo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Protocollo" type="{http://mef.gov.it.v1.npsTypes}prot_identificatoreProtocollo_type" minOccurs="0"/>
 *         &lt;element name="StatoSpedizione" type="{http://mef.gov.it.v1.npsTypes}email_statoSpedizione_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "email_datiPostaUscita_type", propOrder = {
    "idMessaggio",
    "idDocumento",
    "dataAccodamento",
    "messaggio",
    "chiaveFascicolo",
    "protocollo",
    "statoSpedizione"
})
public class EmailDatiPostaUscitaType
    implements Serializable
{

    @XmlElement(name = "IdMessaggio", required = true)
    protected String idMessaggio;
    @XmlElement(name = "IdDocumento", required = true)
    protected String idDocumento;
    @XmlElement(name = "DataAccodamento", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataAccodamento;
    @XmlElement(name = "Messaggio")
    protected EmailMessageType messaggio;
    @XmlElement(name = "ChiaveFascicolo")
    protected String chiaveFascicolo;
    @XmlElement(name = "Protocollo")
    protected ProtIdentificatoreProtocolloType protocollo;
    @XmlElement(name = "StatoSpedizione", required = true)
    @XmlSchemaType(name = "string")
    protected EmailStatoSpedizioneType statoSpedizione;

    /**
     * Recupera il valore della proprietà idMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdMessaggio() {
        return idMessaggio;
    }

    /**
     * Imposta il valore della proprietà idMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdMessaggio(String value) {
        this.idMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà idDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumento() {
        return idDocumento;
    }

    /**
     * Imposta il valore della proprietà idDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumento(String value) {
        this.idDocumento = value;
    }

    /**
     * Recupera il valore della proprietà dataAccodamento.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAccodamento() {
        return dataAccodamento;
    }

    /**
     * Imposta il valore della proprietà dataAccodamento.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAccodamento(XMLGregorianCalendar value) {
        this.dataAccodamento = value;
    }

    /**
     * Recupera il valore della proprietà messaggio.
     * 
     * @return
     *     possible object is
     *     {@link EmailMessageType }
     *     
     */
    public EmailMessageType getMessaggio() {
        return messaggio;
    }

    /**
     * Imposta il valore della proprietà messaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailMessageType }
     *     
     */
    public void setMessaggio(EmailMessageType value) {
        this.messaggio = value;
    }

    /**
     * Recupera il valore della proprietà chiaveFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveFascicolo() {
        return chiaveFascicolo;
    }

    /**
     * Imposta il valore della proprietà chiaveFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveFascicolo(String value) {
        this.chiaveFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà protocollo.
     * 
     * @return
     *     possible object is
     *     {@link ProtIdentificatoreProtocolloType }
     *     
     */
    public ProtIdentificatoreProtocolloType getProtocollo() {
        return protocollo;
    }

    /**
     * Imposta il valore della proprietà protocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtIdentificatoreProtocolloType }
     *     
     */
    public void setProtocollo(ProtIdentificatoreProtocolloType value) {
        this.protocollo = value;
    }

    /**
     * Recupera il valore della proprietà statoSpedizione.
     * 
     * @return
     *     possible object is
     *     {@link EmailStatoSpedizioneType }
     *     
     */
    public EmailStatoSpedizioneType getStatoSpedizione() {
        return statoSpedizione;
    }

    /**
     * Imposta il valore della proprietà statoSpedizione.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailStatoSpedizioneType }
     *     
     */
    public void setStatoSpedizione(EmailStatoSpedizioneType value) {
        this.statoSpedizione = value;
    }

}
