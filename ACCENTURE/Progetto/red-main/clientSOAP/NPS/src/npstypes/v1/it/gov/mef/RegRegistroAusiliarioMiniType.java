
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Definisce il tipo di dato relativo al registro ausiliario di NPS
 * 
 * <p>Classe Java per reg_registroAusiliarioMini_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="reg_registroAusiliarioMini_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdRegistroAusiliario" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="CodiceAmministrazione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CodiceAOO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CodiceRegistro" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TipoRegistro" type="{http://mef.gov.it.v1.npsTypes}all_tipoRegistroAusiliario_type"/>
 *         &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Anno" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Numero" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DataRegistro" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="Stato" type="{http://mef.gov.it.v1.npsTypes}all_statoRegistroAusiliario_type"/>
 *         &lt;element name="IsRegistroDocumentale" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsLocked" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SistemaProduttore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reg_registroAusiliarioMini_type", propOrder = {
    "idRegistroAusiliario",
    "codiceAmministrazione",
    "codiceAOO",
    "codiceRegistro",
    "tipoRegistro",
    "descrizione",
    "anno",
    "numero",
    "dataRegistro",
    "stato",
    "isRegistroDocumentale",
    "isLocked",
    "sistemaProduttore"
})
public class RegRegistroAusiliarioMiniType
    implements Serializable
{

    @XmlElement(name = "IdRegistroAusiliario", required = true, nillable = true)
    protected String idRegistroAusiliario;
    @XmlElement(name = "CodiceAmministrazione", required = true)
    protected String codiceAmministrazione;
    @XmlElement(name = "CodiceAOO", required = true)
    protected String codiceAOO;
    @XmlElement(name = "CodiceRegistro", required = true)
    protected String codiceRegistro;
    @XmlElement(name = "TipoRegistro", required = true)
    @XmlSchemaType(name = "string")
    protected AllTipoRegistroAusiliarioType tipoRegistro;
    @XmlElement(name = "Descrizione", required = true)
    protected String descrizione;
    @XmlElement(name = "Anno")
    protected int anno;
    @XmlElement(name = "Numero")
    protected int numero;
    @XmlElement(name = "DataRegistro", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataRegistro;
    @XmlElement(name = "Stato", required = true)
    @XmlSchemaType(name = "string")
    protected AllStatoRegistroAusiliarioType stato;
    @XmlElement(name = "IsRegistroDocumentale")
    protected boolean isRegistroDocumentale;
    @XmlElement(name = "IsLocked")
    protected boolean isLocked;
    @XmlElement(name = "SistemaProduttore", required = true)
    protected String sistemaProduttore;

    /**
     * Recupera il valore della proprietà idRegistroAusiliario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRegistroAusiliario() {
        return idRegistroAusiliario;
    }

    /**
     * Imposta il valore della proprietà idRegistroAusiliario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRegistroAusiliario(String value) {
        this.idRegistroAusiliario = value;
    }

    /**
     * Recupera il valore della proprietà codiceAmministrazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceAmministrazione() {
        return codiceAmministrazione;
    }

    /**
     * Imposta il valore della proprietà codiceAmministrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceAmministrazione(String value) {
        this.codiceAmministrazione = value;
    }

    /**
     * Recupera il valore della proprietà codiceAOO.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceAOO() {
        return codiceAOO;
    }

    /**
     * Imposta il valore della proprietà codiceAOO.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceAOO(String value) {
        this.codiceAOO = value;
    }

    /**
     * Recupera il valore della proprietà codiceRegistro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceRegistro() {
        return codiceRegistro;
    }

    /**
     * Imposta il valore della proprietà codiceRegistro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceRegistro(String value) {
        this.codiceRegistro = value;
    }

    /**
     * Recupera il valore della proprietà tipoRegistro.
     * 
     * @return
     *     possible object is
     *     {@link AllTipoRegistroAusiliarioType }
     *     
     */
    public AllTipoRegistroAusiliarioType getTipoRegistro() {
        return tipoRegistro;
    }

    /**
     * Imposta il valore della proprietà tipoRegistro.
     * 
     * @param value
     *     allowed object is
     *     {@link AllTipoRegistroAusiliarioType }
     *     
     */
    public void setTipoRegistro(AllTipoRegistroAusiliarioType value) {
        this.tipoRegistro = value;
    }

    /**
     * Recupera il valore della proprietà descrizione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Imposta il valore della proprietà descrizione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Recupera il valore della proprietà anno.
     * 
     */
    public int getAnno() {
        return anno;
    }

    /**
     * Imposta il valore della proprietà anno.
     * 
     */
    public void setAnno(int value) {
        this.anno = value;
    }

    /**
     * Recupera il valore della proprietà numero.
     * 
     */
    public int getNumero() {
        return numero;
    }

    /**
     * Imposta il valore della proprietà numero.
     * 
     */
    public void setNumero(int value) {
        this.numero = value;
    }

    /**
     * Recupera il valore della proprietà dataRegistro.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataRegistro() {
        return dataRegistro;
    }

    /**
     * Imposta il valore della proprietà dataRegistro.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataRegistro(XMLGregorianCalendar value) {
        this.dataRegistro = value;
    }

    /**
     * Recupera il valore della proprietà stato.
     * 
     * @return
     *     possible object is
     *     {@link AllStatoRegistroAusiliarioType }
     *     
     */
    public AllStatoRegistroAusiliarioType getStato() {
        return stato;
    }

    /**
     * Imposta il valore della proprietà stato.
     * 
     * @param value
     *     allowed object is
     *     {@link AllStatoRegistroAusiliarioType }
     *     
     */
    public void setStato(AllStatoRegistroAusiliarioType value) {
        this.stato = value;
    }

    /**
     * Recupera il valore della proprietà isRegistroDocumentale.
     * 
     */
    public boolean isIsRegistroDocumentale() {
        return isRegistroDocumentale;
    }

    /**
     * Imposta il valore della proprietà isRegistroDocumentale.
     * 
     */
    public void setIsRegistroDocumentale(boolean value) {
        this.isRegistroDocumentale = value;
    }

    /**
     * Recupera il valore della proprietà isLocked.
     * 
     */
    public boolean isIsLocked() {
        return isLocked;
    }

    /**
     * Imposta il valore della proprietà isLocked.
     * 
     */
    public void setIsLocked(boolean value) {
        this.isLocked = value;
    }

    /**
     * Recupera il valore della proprietà sistemaProduttore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaProduttore() {
        return sistemaProduttore;
    }

    /**
     * Imposta il valore della proprietà sistemaProduttore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaProduttore(String value) {
        this.sistemaProduttore = value;
    }

}
