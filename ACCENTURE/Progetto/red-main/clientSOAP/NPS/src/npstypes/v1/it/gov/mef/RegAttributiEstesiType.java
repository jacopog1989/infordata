
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce l'insieme della tipologia documentale, dei suoi attributi e dei valori ad essi associati
 * 
 * <p>Classe Java per reg_attributiEstesi_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="reg_attributiEstesi_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tipologia" type="{http://mef.gov.it.v1.npsTypes}reg_tipologiaDocumento_type"/>
 *         &lt;element name="metadatiAssociati" type="{http://mef.gov.it.v1.npsTypes}reg_metadati_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reg_attributiEstesi_type", propOrder = {
    "tipologia",
    "metadatiAssociati"
})
public class RegAttributiEstesiType
    implements Serializable
{

    @XmlElement(required = true, nillable = true)
    protected RegTipologiaDocumentoType tipologia;
    @XmlElementRef(name = "metadatiAssociati", type = JAXBElement.class, required = false)
    protected JAXBElement<RegMetadatiType> metadatiAssociati;

    /**
     * Recupera il valore della proprietà tipologia.
     * 
     * @return
     *     possible object is
     *     {@link RegTipologiaDocumentoType }
     *     
     */
    public RegTipologiaDocumentoType getTipologia() {
        return tipologia;
    }

    /**
     * Imposta il valore della proprietà tipologia.
     * 
     * @param value
     *     allowed object is
     *     {@link RegTipologiaDocumentoType }
     *     
     */
    public void setTipologia(RegTipologiaDocumentoType value) {
        this.tipologia = value;
    }

    /**
     * Recupera il valore della proprietà metadatiAssociati.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link RegMetadatiType }{@code >}
     *     
     */
    public JAXBElement<RegMetadatiType> getMetadatiAssociati() {
        return metadatiAssociati;
    }

    /**
     * Imposta il valore della proprietà metadatiAssociati.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link RegMetadatiType }{@code >}
     *     
     */
    public void setMetadatiAssociati(JAXBElement<RegMetadatiType> value) {
        this.metadatiAssociati = value;
    }

}
