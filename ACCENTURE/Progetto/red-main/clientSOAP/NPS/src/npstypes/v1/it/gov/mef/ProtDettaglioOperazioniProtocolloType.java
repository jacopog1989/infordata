
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce il tipo di dato Dettaglio Operazione
 * 
 * <p>Classe Java per prot_dettaglioOperazioniProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_dettaglioOperazioniProtocollo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoDettaglioOperazione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ValoreOriginale" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ValoreModificato" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_dettaglioOperazioniProtocollo_type", propOrder = {
    "tipoDettaglioOperazione",
    "valoreOriginale",
    "valoreModificato"
})
public class ProtDettaglioOperazioniProtocolloType
    implements Serializable
{

    @XmlElement(name = "TipoDettaglioOperazione", required = true)
    protected String tipoDettaglioOperazione;
    @XmlElement(name = "ValoreOriginale", required = true)
    protected String valoreOriginale;
    @XmlElement(name = "ValoreModificato", required = true)
    protected String valoreModificato;

    /**
     * Recupera il valore della proprietà tipoDettaglioOperazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDettaglioOperazione() {
        return tipoDettaglioOperazione;
    }

    /**
     * Imposta il valore della proprietà tipoDettaglioOperazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDettaglioOperazione(String value) {
        this.tipoDettaglioOperazione = value;
    }

    /**
     * Recupera il valore della proprietà valoreOriginale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValoreOriginale() {
        return valoreOriginale;
    }

    /**
     * Imposta il valore della proprietà valoreOriginale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValoreOriginale(String value) {
        this.valoreOriginale = value;
    }

    /**
     * Recupera il valore della proprietà valoreModificato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValoreModificato() {
        return valoreModificato;
    }

    /**
     * Imposta il valore della proprietà valoreModificato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValoreModificato(String value) {
        this.valoreModificato = value;
    }

}
