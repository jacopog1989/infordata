
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Dati del messaggio di posta
 * 
 * <p>Classe Java per wkf_datiPosta_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_datiPosta_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdMessaggio" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="messaggioRicevuto" type="{http://mef.gov.it.v1.npsTypes}email_message_type"/>
 *         &lt;element name="ChiaveFascicolo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChiaveDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChiaveDocumentoPEC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChiaveDocumentoNotificaPEC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DatiSegnatura" type="{http://mef.gov.it.v1.npsTypes}wkf_datiSegnatura_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_datiPosta_type", propOrder = {
    "idMessaggio",
    "messaggioRicevuto",
    "chiaveFascicolo",
    "chiaveDocumento",
    "chiaveDocumentoPEC",
    "chiaveDocumentoNotificaPEC",
    "datiSegnatura"
})
public class WkfDatiPostaType
    implements Serializable
{

    @XmlElement(name = "IdMessaggio", required = true)
    protected String idMessaggio;
    @XmlElement(required = true)
    protected EmailMessageType messaggioRicevuto;
    @XmlElement(name = "ChiaveFascicolo")
    protected String chiaveFascicolo;
    @XmlElement(name = "ChiaveDocumento")
    protected String chiaveDocumento;
    @XmlElement(name = "ChiaveDocumentoPEC")
    protected String chiaveDocumentoPEC;
    @XmlElement(name = "ChiaveDocumentoNotificaPEC")
    protected String chiaveDocumentoNotificaPEC;
    @XmlElement(name = "DatiSegnatura")
    protected WkfDatiSegnaturaType datiSegnatura;

    /**
     * Recupera il valore della proprietà idMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdMessaggio() {
        return idMessaggio;
    }

    /**
     * Imposta il valore della proprietà idMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdMessaggio(String value) {
        this.idMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà messaggioRicevuto.
     * 
     * @return
     *     possible object is
     *     {@link EmailMessageType }
     *     
     */
    public EmailMessageType getMessaggioRicevuto() {
        return messaggioRicevuto;
    }

    /**
     * Imposta il valore della proprietà messaggioRicevuto.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailMessageType }
     *     
     */
    public void setMessaggioRicevuto(EmailMessageType value) {
        this.messaggioRicevuto = value;
    }

    /**
     * Recupera il valore della proprietà chiaveFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveFascicolo() {
        return chiaveFascicolo;
    }

    /**
     * Imposta il valore della proprietà chiaveFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveFascicolo(String value) {
        this.chiaveFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà chiaveDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveDocumento() {
        return chiaveDocumento;
    }

    /**
     * Imposta il valore della proprietà chiaveDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveDocumento(String value) {
        this.chiaveDocumento = value;
    }

    /**
     * Recupera il valore della proprietà chiaveDocumentoPEC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveDocumentoPEC() {
        return chiaveDocumentoPEC;
    }

    /**
     * Imposta il valore della proprietà chiaveDocumentoPEC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveDocumentoPEC(String value) {
        this.chiaveDocumentoPEC = value;
    }

    /**
     * Recupera il valore della proprietà chiaveDocumentoNotificaPEC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveDocumentoNotificaPEC() {
        return chiaveDocumentoNotificaPEC;
    }

    /**
     * Imposta il valore della proprietà chiaveDocumentoNotificaPEC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveDocumentoNotificaPEC(String value) {
        this.chiaveDocumentoNotificaPEC = value;
    }

    /**
     * Recupera il valore della proprietà datiSegnatura.
     * 
     * @return
     *     possible object is
     *     {@link WkfDatiSegnaturaType }
     *     
     */
    public WkfDatiSegnaturaType getDatiSegnatura() {
        return datiSegnatura;
    }

    /**
     * Imposta il valore della proprietà datiSegnatura.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfDatiSegnaturaType }
     *     
     */
    public void setDatiSegnatura(WkfDatiSegnaturaType value) {
        this.datiSegnatura = value;
    }

}
