
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import it.gov.digitpa.protocollo.Classifica;
import it.gov.digitpa.protocollo.InterventoOperatore;
import it.gov.digitpa.protocollo.Note;
import it.gov.digitpa.protocollo.PiuInfo;
import it.gov.digitpa.protocollo.PrimaRegistrazione;
import it.gov.digitpa.protocollo.Riferimenti;
import it.gov.digitpa.protocollo.RiferimentiTelematici;
import it.gov.digitpa.protocollo.RiferimentoDocumentiCartacei;


/**
 * Definisce il tipo di dato del protocollo da resituire al chiamante
 * 
 * <p>Classe Java per prot_protocolloGenericoResponse_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_protocolloGenericoResponse_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdProtocollo" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="IdentificatoreProtocollo" type="{http://mef.gov.it.v1.npsTypes}prot_identificatoreProtocollo_type"/>
 *         &lt;element name="Oggetto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Protocollatore" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type"/>
 *         &lt;element name="Mittente" type="{http://mef.gov.it.v1.npsTypes}prot_mittente_type"/>
 *         &lt;element name="TipologiaDocumento" type="{http://mef.gov.it.v1.npsTypes}prot_attributiEstesi_type"/>
 *         &lt;element name="DocumentoPrincipale" type="{http://mef.gov.it.v1.npsTypes}prot_documentoProtocollato_type" minOccurs="0"/>
 *         &lt;element name="Allegati" type="{http://mef.gov.it.v1.npsTypes}prot_documentoProtocollato_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="VoceTitolario" type="{http://mef.gov.it.v1.npsTypes}all_codiceDescrizione_type" minOccurs="0"/>
 *         &lt;element name="DatiAnnullamento" type="{http://mef.gov.it.v1.npsTypes}prot_annullamento_type" minOccurs="0"/>
 *         &lt;element name="Assegnatari" type="{http://mef.gov.it.v1.npsTypes}prot_assegnatario_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Destinatari" type="{http://mef.gov.it.v1.npsTypes}prot_destinatario_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Allacciati" type="{http://mef.gov.it.v1.npsTypes}prot_allaccioProtocollo_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="IsRiservato" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SegnaturaEstesa" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Intestazione" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}PrimaRegistrazione" minOccurs="0"/>
 *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}InterventoOperatore" minOccurs="0"/>
 *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentoDocumentiCartacei" minOccurs="0"/>
 *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentiTelematici" minOccurs="0"/>
 *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Classifica" minOccurs="0"/>
 *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Note" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Riferimenti" minOccurs="0"/>
 *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}PiuInfo" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Acl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SistemaAusiliario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsAnnullato" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsMozione" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="Stato">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="DA_ASSEGNARE"/>
 *               &lt;enumeration value="IN_LAVORAZIONE"/>
 *               &lt;enumeration value="AGLI_ATTI"/>
 *               &lt;enumeration value="CHIUSO"/>
 *               &lt;enumeration value="DA_SPEDIRE"/>
 *               &lt;enumeration value="INVIATO"/>
 *               &lt;enumeration value="SPEDITO"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="SistemaProduttore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_protocolloGenericoResponse_type", propOrder = {
    "idProtocollo",
    "identificatoreProtocollo",
    "oggetto",
    "protocollatore",
    "mittente",
    "tipologiaDocumento",
    "documentoPrincipale",
    "allegati",
    "voceTitolario",
    "datiAnnullamento",
    "assegnatari",
    "destinatari",
    "allacciati",
    "isRiservato",
    "segnaturaEstesa",
    "acl",
    "sistemaAusiliario",
    "isAnnullato",
    "isMozione",
    "stato",
    "sistemaProduttore"
})
public class ProtProtocolloGenericoResponseType
    implements Serializable
{

    @XmlElement(name = "IdProtocollo", required = true, nillable = true)
    protected String idProtocollo;
    @XmlElement(name = "IdentificatoreProtocollo", required = true)
    protected ProtIdentificatoreProtocolloType identificatoreProtocollo;
    @XmlElement(name = "Oggetto", required = true)
    protected String oggetto;
    @XmlElement(name = "Protocollatore", required = true)
    protected OrgOrganigrammaType protocollatore;
    @XmlElement(name = "Mittente", required = true)
    protected ProtMittenteType mittente;
    @XmlElement(name = "TipologiaDocumento", required = true)
    protected ProtAttributiEstesiType tipologiaDocumento;
    @XmlElement(name = "DocumentoPrincipale")
    protected ProtDocumentoProtocollatoType documentoPrincipale;
    @XmlElement(name = "Allegati")
    protected List<ProtDocumentoProtocollatoType> allegati;
    @XmlElement(name = "VoceTitolario")
    protected AllCodiceDescrizioneType voceTitolario;
    @XmlElement(name = "DatiAnnullamento")
    protected ProtAnnullamentoType datiAnnullamento;
    @XmlElement(name = "Assegnatari")
    protected List<ProtAssegnatarioType> assegnatari;
    @XmlElement(name = "Destinatari")
    protected List<ProtDestinatarioType> destinatari;
    @XmlElement(name = "Allacciati")
    protected List<ProtAllaccioProtocolloType> allacciati;
    @XmlElement(name = "IsRiservato", defaultValue = "false")
    protected Boolean isRiservato;
    @XmlElement(name = "SegnaturaEstesa")
    protected ProtProtocolloGenericoResponseType.SegnaturaEstesa segnaturaEstesa;
    @XmlElement(name = "Acl")
    protected String acl;
    @XmlElement(name = "SistemaAusiliario")
    protected String sistemaAusiliario;
    @XmlElement(name = "IsAnnullato", defaultValue = "false")
    protected boolean isAnnullato;
    @XmlElement(name = "IsMozione", defaultValue = "false")
    protected boolean isMozione;
    @XmlElement(name = "Stato", required = true)
    protected String stato;
    @XmlElement(name = "SistemaProduttore", required = true)
    protected String sistemaProduttore;

    /**
     * Recupera il valore della proprietà idProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdProtocollo() {
        return idProtocollo;
    }

    /**
     * Imposta il valore della proprietà idProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdProtocollo(String value) {
        this.idProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà identificatoreProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link ProtIdentificatoreProtocolloType }
     *     
     */
    public ProtIdentificatoreProtocolloType getIdentificatoreProtocollo() {
        return identificatoreProtocollo;
    }

    /**
     * Imposta il valore della proprietà identificatoreProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtIdentificatoreProtocolloType }
     *     
     */
    public void setIdentificatoreProtocollo(ProtIdentificatoreProtocolloType value) {
        this.identificatoreProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà oggetto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggetto() {
        return oggetto;
    }

    /**
     * Imposta il valore della proprietà oggetto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggetto(String value) {
        this.oggetto = value;
    }

    /**
     * Recupera il valore della proprietà protocollatore.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getProtocollatore() {
        return protocollatore;
    }

    /**
     * Imposta il valore della proprietà protocollatore.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setProtocollatore(OrgOrganigrammaType value) {
        this.protocollatore = value;
    }

    /**
     * Recupera il valore della proprietà mittente.
     * 
     * @return
     *     possible object is
     *     {@link ProtMittenteType }
     *     
     */
    public ProtMittenteType getMittente() {
        return mittente;
    }

    /**
     * Imposta il valore della proprietà mittente.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtMittenteType }
     *     
     */
    public void setMittente(ProtMittenteType value) {
        this.mittente = value;
    }

    /**
     * Recupera il valore della proprietà tipologiaDocumento.
     * 
     * @return
     *     possible object is
     *     {@link ProtAttributiEstesiType }
     *     
     */
    public ProtAttributiEstesiType getTipologiaDocumento() {
        return tipologiaDocumento;
    }

    /**
     * Imposta il valore della proprietà tipologiaDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtAttributiEstesiType }
     *     
     */
    public void setTipologiaDocumento(ProtAttributiEstesiType value) {
        this.tipologiaDocumento = value;
    }

    /**
     * Recupera il valore della proprietà documentoPrincipale.
     * 
     * @return
     *     possible object is
     *     {@link ProtDocumentoProtocollatoType }
     *     
     */
    public ProtDocumentoProtocollatoType getDocumentoPrincipale() {
        return documentoPrincipale;
    }

    /**
     * Imposta il valore della proprietà documentoPrincipale.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtDocumentoProtocollatoType }
     *     
     */
    public void setDocumentoPrincipale(ProtDocumentoProtocollatoType value) {
        this.documentoPrincipale = value;
    }

    /**
     * Gets the value of the allegati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allegati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllegati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtDocumentoProtocollatoType }
     * 
     * 
     */
    public List<ProtDocumentoProtocollatoType> getAllegati() {
        if (allegati == null) {
            allegati = new ArrayList<ProtDocumentoProtocollatoType>();
        }
        return this.allegati;
    }

    /**
     * Recupera il valore della proprietà voceTitolario.
     * 
     * @return
     *     possible object is
     *     {@link AllCodiceDescrizioneType }
     *     
     */
    public AllCodiceDescrizioneType getVoceTitolario() {
        return voceTitolario;
    }

    /**
     * Imposta il valore della proprietà voceTitolario.
     * 
     * @param value
     *     allowed object is
     *     {@link AllCodiceDescrizioneType }
     *     
     */
    public void setVoceTitolario(AllCodiceDescrizioneType value) {
        this.voceTitolario = value;
    }

    /**
     * Recupera il valore della proprietà datiAnnullamento.
     * 
     * @return
     *     possible object is
     *     {@link ProtAnnullamentoType }
     *     
     */
    public ProtAnnullamentoType getDatiAnnullamento() {
        return datiAnnullamento;
    }

    /**
     * Imposta il valore della proprietà datiAnnullamento.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtAnnullamentoType }
     *     
     */
    public void setDatiAnnullamento(ProtAnnullamentoType value) {
        this.datiAnnullamento = value;
    }

    /**
     * Gets the value of the assegnatari property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the assegnatari property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssegnatari().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtAssegnatarioType }
     * 
     * 
     */
    public List<ProtAssegnatarioType> getAssegnatari() {
        if (assegnatari == null) {
            assegnatari = new ArrayList<ProtAssegnatarioType>();
        }
        return this.assegnatari;
    }

    /**
     * Gets the value of the destinatari property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the destinatari property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDestinatari().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtDestinatarioType }
     * 
     * 
     */
    public List<ProtDestinatarioType> getDestinatari() {
        if (destinatari == null) {
            destinatari = new ArrayList<ProtDestinatarioType>();
        }
        return this.destinatari;
    }

    /**
     * Gets the value of the allacciati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allacciati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllacciati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtAllaccioProtocolloType }
     * 
     * 
     */
    public List<ProtAllaccioProtocolloType> getAllacciati() {
        if (allacciati == null) {
            allacciati = new ArrayList<ProtAllaccioProtocolloType>();
        }
        return this.allacciati;
    }

    /**
     * Recupera il valore della proprietà isRiservato.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsRiservato() {
        return isRiservato;
    }

    /**
     * Imposta il valore della proprietà isRiservato.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsRiservato(Boolean value) {
        this.isRiservato = value;
    }

    /**
     * Recupera il valore della proprietà segnaturaEstesa.
     * 
     * @return
     *     possible object is
     *     {@link ProtProtocolloGenericoResponseType.SegnaturaEstesa }
     *     
     */
    public ProtProtocolloGenericoResponseType.SegnaturaEstesa getSegnaturaEstesa() {
        return segnaturaEstesa;
    }

    /**
     * Imposta il valore della proprietà segnaturaEstesa.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtProtocolloGenericoResponseType.SegnaturaEstesa }
     *     
     */
    public void setSegnaturaEstesa(ProtProtocolloGenericoResponseType.SegnaturaEstesa value) {
        this.segnaturaEstesa = value;
    }

    /**
     * Recupera il valore della proprietà acl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcl() {
        return acl;
    }

    /**
     * Imposta il valore della proprietà acl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcl(String value) {
        this.acl = value;
    }

    /**
     * Recupera il valore della proprietà sistemaAusiliario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaAusiliario() {
        return sistemaAusiliario;
    }

    /**
     * Imposta il valore della proprietà sistemaAusiliario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaAusiliario(String value) {
        this.sistemaAusiliario = value;
    }

    /**
     * Recupera il valore della proprietà isAnnullato.
     * 
     */
    public boolean isIsAnnullato() {
        return isAnnullato;
    }

    /**
     * Imposta il valore della proprietà isAnnullato.
     * 
     */
    public void setIsAnnullato(boolean value) {
        this.isAnnullato = value;
    }

    /**
     * Recupera il valore della proprietà isMozione.
     * 
     */
    public boolean isIsMozione() {
        return isMozione;
    }

    /**
     * Imposta il valore della proprietà isMozione.
     * 
     */
    public void setIsMozione(boolean value) {
        this.isMozione = value;
    }

    /**
     * Recupera il valore della proprietà stato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStato() {
        return stato;
    }

    /**
     * Imposta il valore della proprietà stato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStato(String value) {
        this.stato = value;
    }

    /**
     * Recupera il valore della proprietà sistemaProduttore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaProduttore() {
        return sistemaProduttore;
    }

    /**
     * Imposta il valore della proprietà sistemaProduttore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaProduttore(String value) {
        this.sistemaProduttore = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Intestazione" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}PrimaRegistrazione" minOccurs="0"/>
     *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}InterventoOperatore" minOccurs="0"/>
     *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentoDocumentiCartacei" minOccurs="0"/>
     *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentiTelematici" minOccurs="0"/>
     *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Classifica" minOccurs="0"/>
     *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Note" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Riferimenti" minOccurs="0"/>
     *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}PiuInfo" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "intestazione",
        "riferimenti",
        "piuInfo"
    })
    public static class SegnaturaEstesa
        implements Serializable
    {

        @XmlElement(name = "Intestazione")
        protected ProtProtocolloGenericoResponseType.SegnaturaEstesa.Intestazione intestazione;
        @XmlElement(name = "Riferimenti", namespace = "http://www.digitPa.gov.it/protocollo/")
        protected Riferimenti riferimenti;
        @XmlElement(name = "PiuInfo", namespace = "http://www.digitPa.gov.it/protocollo/")
        protected PiuInfo piuInfo;

        /**
         * Recupera il valore della proprietà intestazione.
         * 
         * @return
         *     possible object is
         *     {@link ProtProtocolloGenericoResponseType.SegnaturaEstesa.Intestazione }
         *     
         */
        public ProtProtocolloGenericoResponseType.SegnaturaEstesa.Intestazione getIntestazione() {
            return intestazione;
        }

        /**
         * Imposta il valore della proprietà intestazione.
         * 
         * @param value
         *     allowed object is
         *     {@link ProtProtocolloGenericoResponseType.SegnaturaEstesa.Intestazione }
         *     
         */
        public void setIntestazione(ProtProtocolloGenericoResponseType.SegnaturaEstesa.Intestazione value) {
            this.intestazione = value;
        }

        /**
         * Recupera il valore della proprietà riferimenti.
         * 
         * @return
         *     possible object is
         *     {@link Riferimenti }
         *     
         */
        public Riferimenti getRiferimenti() {
            return riferimenti;
        }

        /**
         * Imposta il valore della proprietà riferimenti.
         * 
         * @param value
         *     allowed object is
         *     {@link Riferimenti }
         *     
         */
        public void setRiferimenti(Riferimenti value) {
            this.riferimenti = value;
        }

        /**
         * Recupera il valore della proprietà piuInfo.
         * 
         * @return
         *     possible object is
         *     {@link PiuInfo }
         *     
         */
        public PiuInfo getPiuInfo() {
            return piuInfo;
        }

        /**
         * Imposta il valore della proprietà piuInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link PiuInfo }
         *     
         */
        public void setPiuInfo(PiuInfo value) {
            this.piuInfo = value;
        }


        /**
         * <p>Classe Java per anonymous complex type.
         * 
         * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}PrimaRegistrazione" minOccurs="0"/>
         *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}InterventoOperatore" minOccurs="0"/>
         *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentoDocumentiCartacei" minOccurs="0"/>
         *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentiTelematici" minOccurs="0"/>
         *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Classifica" minOccurs="0"/>
         *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Note" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "primaRegistrazione",
            "interventoOperatore",
            "riferimentoDocumentiCartacei",
            "riferimentiTelematici",
            "classifica",
            "note"
        })
        public static class Intestazione
            implements Serializable
        {

            @XmlElement(name = "PrimaRegistrazione", namespace = "http://www.digitPa.gov.it/protocollo/")
            protected PrimaRegistrazione primaRegistrazione;
            @XmlElement(name = "InterventoOperatore", namespace = "http://www.digitPa.gov.it/protocollo/")
            protected InterventoOperatore interventoOperatore;
            @XmlElement(name = "RiferimentoDocumentiCartacei", namespace = "http://www.digitPa.gov.it/protocollo/")
            protected RiferimentoDocumentiCartacei riferimentoDocumentiCartacei;
            @XmlElement(name = "RiferimentiTelematici", namespace = "http://www.digitPa.gov.it/protocollo/")
            protected RiferimentiTelematici riferimentiTelematici;
            @XmlElement(name = "Classifica", namespace = "http://www.digitPa.gov.it/protocollo/")
            protected Classifica classifica;
            @XmlElement(name = "Note", namespace = "http://www.digitPa.gov.it/protocollo/")
            protected Note note;

            /**
             * Recupera il valore della proprietà primaRegistrazione.
             * 
             * @return
             *     possible object is
             *     {@link PrimaRegistrazione }
             *     
             */
            public PrimaRegistrazione getPrimaRegistrazione() {
                return primaRegistrazione;
            }

            /**
             * Imposta il valore della proprietà primaRegistrazione.
             * 
             * @param value
             *     allowed object is
             *     {@link PrimaRegistrazione }
             *     
             */
            public void setPrimaRegistrazione(PrimaRegistrazione value) {
                this.primaRegistrazione = value;
            }

            /**
             * Recupera il valore della proprietà interventoOperatore.
             * 
             * @return
             *     possible object is
             *     {@link InterventoOperatore }
             *     
             */
            public InterventoOperatore getInterventoOperatore() {
                return interventoOperatore;
            }

            /**
             * Imposta il valore della proprietà interventoOperatore.
             * 
             * @param value
             *     allowed object is
             *     {@link InterventoOperatore }
             *     
             */
            public void setInterventoOperatore(InterventoOperatore value) {
                this.interventoOperatore = value;
            }

            /**
             * Recupera il valore della proprietà riferimentoDocumentiCartacei.
             * 
             * @return
             *     possible object is
             *     {@link RiferimentoDocumentiCartacei }
             *     
             */
            public RiferimentoDocumentiCartacei getRiferimentoDocumentiCartacei() {
                return riferimentoDocumentiCartacei;
            }

            /**
             * Imposta il valore della proprietà riferimentoDocumentiCartacei.
             * 
             * @param value
             *     allowed object is
             *     {@link RiferimentoDocumentiCartacei }
             *     
             */
            public void setRiferimentoDocumentiCartacei(RiferimentoDocumentiCartacei value) {
                this.riferimentoDocumentiCartacei = value;
            }

            /**
             * Recupera il valore della proprietà riferimentiTelematici.
             * 
             * @return
             *     possible object is
             *     {@link RiferimentiTelematici }
             *     
             */
            public RiferimentiTelematici getRiferimentiTelematici() {
                return riferimentiTelematici;
            }

            /**
             * Imposta il valore della proprietà riferimentiTelematici.
             * 
             * @param value
             *     allowed object is
             *     {@link RiferimentiTelematici }
             *     
             */
            public void setRiferimentiTelematici(RiferimentiTelematici value) {
                this.riferimentiTelematici = value;
            }

            /**
             * Recupera il valore della proprietà classifica.
             * 
             * @return
             *     possible object is
             *     {@link Classifica }
             *     
             */
            public Classifica getClassifica() {
                return classifica;
            }

            /**
             * Imposta il valore della proprietà classifica.
             * 
             * @param value
             *     allowed object is
             *     {@link Classifica }
             *     
             */
            public void setClassifica(Classifica value) {
                this.classifica = value;
            }

            /**
             * Recupera il valore della proprietà note.
             * 
             * @return
             *     possible object is
             *     {@link Note }
             *     
             */
            public Note getNote() {
                return note;
            }

            /**
             * Imposta il valore della proprietà note.
             * 
             * @param value
             *     allowed object is
             *     {@link Note }
             *     
             */
            public void setNote(Note value) {
                this.note = value;
            }

        }

    }

}
