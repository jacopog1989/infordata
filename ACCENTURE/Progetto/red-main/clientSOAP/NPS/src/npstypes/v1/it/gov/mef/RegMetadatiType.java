
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce l'elenco degli attributi estesi di una tipologia documentale e dei valori ad essi associati
 * 
 * <p>Classe Java per reg_metadati_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="reg_metadati_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MetadatoAssociato" type="{http://mef.gov.it.v1.npsTypes}reg_metadatoAssociato_type" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reg_metadati_type", propOrder = {
    "metadatoAssociato"
})
public class RegMetadatiType
    implements Serializable
{

    @XmlElement(name = "MetadatoAssociato")
    protected List<RegMetadatoAssociatoType> metadatoAssociato;

    /**
     * Gets the value of the metadatoAssociato property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the metadatoAssociato property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMetadatoAssociato().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegMetadatoAssociatoType }
     * 
     * 
     */
    public List<RegMetadatoAssociatoType> getMetadatoAssociato() {
        if (metadatoAssociato == null) {
            metadatoAssociato = new ArrayList<RegMetadatoAssociatoType>();
        }
        return this.metadatoAssociato;
    }

}
