
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce il tipo di dato Documento da Protocollare
 * 
 * <p>Classe Java per prot_documentoDaProtocollare_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_documentoDaProtocollare_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdDocumento" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="Atmos" type="{http://mef.gov.it.v1.npsTypes}AtmosType" minOccurs="0"/>
 *         &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Attivo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsDaInviare" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsDaTimbrare" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_documentoDaProtocollare_type", propOrder = {
    "idDocumento",
    "atmos",
    "descrizione",
    "attivo",
    "isDaInviare",
    "isDaTimbrare"
})
public class ProtDocumentoDaProtocollareType
    implements Serializable
{

    @XmlElement(name = "IdDocumento", required = true)
    protected String idDocumento;
    @XmlElement(name = "Atmos")
    protected AtmosType atmos;
    @XmlElement(name = "Descrizione", required = true)
    protected String descrizione;
    @XmlElement(name = "Attivo", defaultValue = "true")
    protected boolean attivo;
    @XmlElement(name = "IsDaInviare", defaultValue = "true")
    protected boolean isDaInviare;
    @XmlElement(name = "IsDaTimbrare", defaultValue = "false")
    protected Boolean isDaTimbrare;

    /**
     * Recupera il valore della proprietà idDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumento() {
        return idDocumento;
    }

    /**
     * Imposta il valore della proprietà idDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumento(String value) {
        this.idDocumento = value;
    }

    /**
     * Recupera il valore della proprietà atmos.
     * 
     * @return
     *     possible object is
     *     {@link AtmosType }
     *     
     */
    public AtmosType getAtmos() {
        return atmos;
    }

    /**
     * Imposta il valore della proprietà atmos.
     * 
     * @param value
     *     allowed object is
     *     {@link AtmosType }
     *     
     */
    public void setAtmos(AtmosType value) {
        this.atmos = value;
    }

    /**
     * Recupera il valore della proprietà descrizione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Imposta il valore della proprietà descrizione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Recupera il valore della proprietà attivo.
     * 
     */
    public boolean isAttivo() {
        return attivo;
    }

    /**
     * Imposta il valore della proprietà attivo.
     * 
     */
    public void setAttivo(boolean value) {
        this.attivo = value;
    }

    /**
     * Recupera il valore della proprietà isDaInviare.
     * 
     */
    public boolean isIsDaInviare() {
        return isDaInviare;
    }

    /**
     * Imposta il valore della proprietà isDaInviare.
     * 
     */
    public void setIsDaInviare(boolean value) {
        this.isDaInviare = value;
    }

    /**
     * Recupera il valore della proprietà isDaTimbrare.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsDaTimbrare() {
        return isDaTimbrare;
    }

    /**
     * Imposta il valore della proprietà isDaTimbrare.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsDaTimbrare(Boolean value) {
        this.isDaTimbrare = value;
    }

}
