
package npstypes.v1.it.gov.mef;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per all_tipoRegistroAusiliario_type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="all_tipoRegistroAusiliario_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="GENERICO"/>
 *     &lt;enumeration value="VISTO"/>
 *     &lt;enumeration value="OSSERVAZIONE"/>
 *     &lt;enumeration value="RICHIESTA_INTEGRAZIONE"/>
 *     &lt;enumeration value="RESTITUZIONE"/>
 *     &lt;enumeration value="APPLICAZIONE"/>
 *     &lt;enumeration value="SEGNALAZIONE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "all_tipoRegistroAusiliario_type")
@XmlEnum
public enum AllTipoRegistroAusiliarioType {

    GENERICO,
    VISTO,
    OSSERVAZIONE,
    RICHIESTA_INTEGRAZIONE,
    RESTITUZIONE,
    APPLICAZIONE,
    SEGNALAZIONE;

    public String value() {
        return name();
    }

    public static AllTipoRegistroAusiliarioType fromValue(String v) {
        return valueOf(v);
    }

}
