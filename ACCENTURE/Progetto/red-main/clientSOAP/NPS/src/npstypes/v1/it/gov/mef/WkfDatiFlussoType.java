
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import npsmessages.v1.it.gov.mef.RichiestaInviaAttoType;
import npsmessages.v1.it.gov.mef.RichiestaInviaComunicazioneGenericaType;
import npsmessages.v1.it.gov.mef.RichiestaInviaDocumentazioneGenericaType;
import npsmessages.v1.it.gov.mef.RichiestaInviaDocumentazioneIntegrativaType;
import npsmessages.v1.it.gov.mef.RichiestaRitiroAutotutelaType;


/**
 * Dati del flusso
 * 
 * <p>Classe Java per wkf_datiFlusso_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_datiFlusso_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdMessaggio" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="Oggetto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CodiceFlusso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdentificatoreProcesso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChiaveFascicolo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChiaveDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChiaveAllegati" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Mittente" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type" minOccurs="0"/>
 *         &lt;element name="Destinatari" type="{http://mef.gov.it.v1.npsTypes}wkf_destinatarioFlusso_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Acl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipologiaDocumento" type="{http://mef.gov.it.v1.npsTypes}prot_attributiEstesi_type" minOccurs="0"/>
 *         &lt;element name="VoceTitolario" type="{http://mef.gov.it.v1.npsTypes}all_codiceDescrizione_type" minOccurs="0"/>
 *         &lt;element name="Allacciati" type="{http://mef.gov.it.v1.npsTypes}prot_allaccioProtocollo_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Messaggio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CorpoMessaggio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ContestoProcedurale" type="{http://mef.gov.it.v1.npsTypes}wkf_datiContestoProcedurale_type" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_datiFlusso_type", propOrder = {
    "idMessaggio",
    "oggetto",
    "codiceFlusso",
    "identificatoreProcesso",
    "chiaveFascicolo",
    "chiaveDocumento",
    "chiaveAllegati",
    "mittente",
    "destinatari",
    "acl",
    "tipologiaDocumento",
    "voceTitolario",
    "allacciati",
    "messaggio",
    "corpoMessaggio",
    "contestoProcedurale"
})
@XmlSeeAlso({
    WkfDatiFlussoEntrataType.class,
    RichiestaInviaAttoType.class,
    RichiestaRitiroAutotutelaType.class,
    RichiestaInviaDocumentazioneGenericaType.class,
    RichiestaInviaComunicazioneGenericaType.class,
    RichiestaInviaDocumentazioneIntegrativaType.class
})
public class WkfDatiFlussoType
    implements Serializable
{

    @XmlElement(name = "IdMessaggio", required = true)
    protected String idMessaggio;
    @XmlElement(name = "Oggetto", required = true)
    protected String oggetto;
    @XmlElement(name = "CodiceFlusso", required = true)
    protected String codiceFlusso;
    @XmlElement(name = "IdentificatoreProcesso", required = true)
    protected String identificatoreProcesso;
    @XmlElement(name = "ChiaveFascicolo")
    protected String chiaveFascicolo;
    @XmlElement(name = "ChiaveDocumento")
    protected String chiaveDocumento;
    @XmlElement(name = "ChiaveAllegati")
    protected List<String> chiaveAllegati;
    @XmlElement(name = "Mittente")
    protected OrgOrganigrammaType mittente;
    @XmlElement(name = "Destinatari")
    protected List<WkfDestinatarioFlussoType> destinatari;
    @XmlElement(name = "Acl")
    protected String acl;
    @XmlElement(name = "TipologiaDocumento")
    protected ProtAttributiEstesiType tipologiaDocumento;
    @XmlElement(name = "VoceTitolario")
    protected AllCodiceDescrizioneType voceTitolario;
    @XmlElement(name = "Allacciati")
    protected List<ProtAllaccioProtocolloType> allacciati;
    @XmlElement(name = "Messaggio")
    protected String messaggio;
    @XmlElement(name = "CorpoMessaggio")
    protected String corpoMessaggio;
    @XmlElement(name = "ContestoProcedurale", required = true)
    protected List<WkfDatiContestoProceduraleType> contestoProcedurale;

    /**
     * Recupera il valore della proprietà idMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdMessaggio() {
        return idMessaggio;
    }

    /**
     * Imposta il valore della proprietà idMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdMessaggio(String value) {
        this.idMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà oggetto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggetto() {
        return oggetto;
    }

    /**
     * Imposta il valore della proprietà oggetto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggetto(String value) {
        this.oggetto = value;
    }

    /**
     * Recupera il valore della proprietà codiceFlusso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceFlusso() {
        return codiceFlusso;
    }

    /**
     * Imposta il valore della proprietà codiceFlusso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceFlusso(String value) {
        this.codiceFlusso = value;
    }

    /**
     * Recupera il valore della proprietà identificatoreProcesso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificatoreProcesso() {
        return identificatoreProcesso;
    }

    /**
     * Imposta il valore della proprietà identificatoreProcesso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificatoreProcesso(String value) {
        this.identificatoreProcesso = value;
    }

    /**
     * Recupera il valore della proprietà chiaveFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveFascicolo() {
        return chiaveFascicolo;
    }

    /**
     * Imposta il valore della proprietà chiaveFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveFascicolo(String value) {
        this.chiaveFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà chiaveDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveDocumento() {
        return chiaveDocumento;
    }

    /**
     * Imposta il valore della proprietà chiaveDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveDocumento(String value) {
        this.chiaveDocumento = value;
    }

    /**
     * Gets the value of the chiaveAllegati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the chiaveAllegati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChiaveAllegati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getChiaveAllegati() {
        if (chiaveAllegati == null) {
            chiaveAllegati = new ArrayList<String>();
        }
        return this.chiaveAllegati;
    }

    /**
     * Recupera il valore della proprietà mittente.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getMittente() {
        return mittente;
    }

    /**
     * Imposta il valore della proprietà mittente.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setMittente(OrgOrganigrammaType value) {
        this.mittente = value;
    }

    /**
     * Gets the value of the destinatari property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the destinatari property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDestinatari().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WkfDestinatarioFlussoType }
     * 
     * 
     */
    public List<WkfDestinatarioFlussoType> getDestinatari() {
        if (destinatari == null) {
            destinatari = new ArrayList<WkfDestinatarioFlussoType>();
        }
        return this.destinatari;
    }

    /**
     * Recupera il valore della proprietà acl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcl() {
        return acl;
    }

    /**
     * Imposta il valore della proprietà acl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcl(String value) {
        this.acl = value;
    }

    /**
     * Recupera il valore della proprietà tipologiaDocumento.
     * 
     * @return
     *     possible object is
     *     {@link ProtAttributiEstesiType }
     *     
     */
    public ProtAttributiEstesiType getTipologiaDocumento() {
        return tipologiaDocumento;
    }

    /**
     * Imposta il valore della proprietà tipologiaDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtAttributiEstesiType }
     *     
     */
    public void setTipologiaDocumento(ProtAttributiEstesiType value) {
        this.tipologiaDocumento = value;
    }

    /**
     * Recupera il valore della proprietà voceTitolario.
     * 
     * @return
     *     possible object is
     *     {@link AllCodiceDescrizioneType }
     *     
     */
    public AllCodiceDescrizioneType getVoceTitolario() {
        return voceTitolario;
    }

    /**
     * Imposta il valore della proprietà voceTitolario.
     * 
     * @param value
     *     allowed object is
     *     {@link AllCodiceDescrizioneType }
     *     
     */
    public void setVoceTitolario(AllCodiceDescrizioneType value) {
        this.voceTitolario = value;
    }

    /**
     * Gets the value of the allacciati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allacciati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllacciati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtAllaccioProtocolloType }
     * 
     * 
     */
    public List<ProtAllaccioProtocolloType> getAllacciati() {
        if (allacciati == null) {
            allacciati = new ArrayList<ProtAllaccioProtocolloType>();
        }
        return this.allacciati;
    }

    /**
     * Recupera il valore della proprietà messaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessaggio() {
        return messaggio;
    }

    /**
     * Imposta il valore della proprietà messaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessaggio(String value) {
        this.messaggio = value;
    }

    /**
     * Recupera il valore della proprietà corpoMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorpoMessaggio() {
        return corpoMessaggio;
    }

    /**
     * Imposta il valore della proprietà corpoMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorpoMessaggio(String value) {
        this.corpoMessaggio = value;
    }

    /**
     * Gets the value of the contestoProcedurale property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contestoProcedurale property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContestoProcedurale().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WkfDatiContestoProceduraleType }
     * 
     * 
     */
    public List<WkfDatiContestoProceduraleType> getContestoProcedurale() {
        if (contestoProcedurale == null) {
            contestoProcedurale = new ArrayList<WkfDatiContestoProceduraleType>();
        }
        return this.contestoProcedurale;
    }

}
