
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Definisce il tipo di dato Identificatore del Protocollo
 * 
 * <p>Classe Java per prot_identificatoreProtocolloCollegato_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_identificatoreProtocolloCollegato_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdProtocollo" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="DataRegistrazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="NumeroRegistrazione" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TipoProtocollo" type="{http://mef.gov.it.v1.npsTypes}prot_tipoProtocollo_type"/>
 *         &lt;element name="Stato">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="DA_ASSEGNARE"/>
 *               &lt;enumeration value="IN_LAVORAZIONE"/>
 *               &lt;enumeration value="AGLI_ATTI"/>
 *               &lt;enumeration value="CHIUSO"/>
 *               &lt;enumeration value="DA_SPEDIRE"/>
 *               &lt;enumeration value="INVIATO"/>
 *               &lt;enumeration value="SPEDITO"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Allacciante" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="IdProtocollo" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *                   &lt;element name="NumeroRegistrazione" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="DataRegistrazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                   &lt;element name="TipoProtocollo" type="{http://mef.gov.it.v1.npsTypes}prot_tipoProtocollo_type"/>
 *                   &lt;element name="Stato">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="DA_ASSEGNARE"/>
 *                         &lt;enumeration value="IN_LAVORAZIONE"/>
 *                         &lt;enumeration value="AGLI_ATTI"/>
 *                         &lt;enumeration value="CHIUSO"/>
 *                         &lt;enumeration value="DA_SPEDIRE"/>
 *                         &lt;enumeration value="INVIATO"/>
 *                         &lt;enumeration value="SPEDITO"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_identificatoreProtocolloCollegato_type", propOrder = {
    "idProtocollo",
    "dataRegistrazione",
    "numeroRegistrazione",
    "tipoProtocollo",
    "stato",
    "allacciante"
})
public class ProtIdentificatoreProtocolloCollegatoType
    implements Serializable
{

    @XmlElement(name = "IdProtocollo", required = true)
    protected String idProtocollo;
    @XmlElement(name = "DataRegistrazione", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataRegistrazione;
    @XmlElement(name = "NumeroRegistrazione")
    protected int numeroRegistrazione;
    @XmlElement(name = "TipoProtocollo", required = true)
    @XmlSchemaType(name = "string")
    protected ProtTipoProtocolloType tipoProtocollo;
    @XmlElement(name = "Stato", required = true)
    protected String stato;
    @XmlElement(name = "Allacciante")
    protected List<ProtIdentificatoreProtocolloCollegatoType.Allacciante> allacciante;

    /**
     * Recupera il valore della proprietà idProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdProtocollo() {
        return idProtocollo;
    }

    /**
     * Imposta il valore della proprietà idProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdProtocollo(String value) {
        this.idProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà dataRegistrazione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataRegistrazione() {
        return dataRegistrazione;
    }

    /**
     * Imposta il valore della proprietà dataRegistrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataRegistrazione(XMLGregorianCalendar value) {
        this.dataRegistrazione = value;
    }

    /**
     * Recupera il valore della proprietà numeroRegistrazione.
     * 
     */
    public int getNumeroRegistrazione() {
        return numeroRegistrazione;
    }

    /**
     * Imposta il valore della proprietà numeroRegistrazione.
     * 
     */
    public void setNumeroRegistrazione(int value) {
        this.numeroRegistrazione = value;
    }

    /**
     * Recupera il valore della proprietà tipoProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link ProtTipoProtocolloType }
     *     
     */
    public ProtTipoProtocolloType getTipoProtocollo() {
        return tipoProtocollo;
    }

    /**
     * Imposta il valore della proprietà tipoProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtTipoProtocolloType }
     *     
     */
    public void setTipoProtocollo(ProtTipoProtocolloType value) {
        this.tipoProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà stato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStato() {
        return stato;
    }

    /**
     * Imposta il valore della proprietà stato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStato(String value) {
        this.stato = value;
    }

    /**
     * Gets the value of the allacciante property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allacciante property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllacciante().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtIdentificatoreProtocolloCollegatoType.Allacciante }
     * 
     * 
     */
    public List<ProtIdentificatoreProtocolloCollegatoType.Allacciante> getAllacciante() {
        if (allacciante == null) {
            allacciante = new ArrayList<ProtIdentificatoreProtocolloCollegatoType.Allacciante>();
        }
        return this.allacciante;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="IdProtocollo" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
     *         &lt;element name="NumeroRegistrazione" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="DataRegistrazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *         &lt;element name="TipoProtocollo" type="{http://mef.gov.it.v1.npsTypes}prot_tipoProtocollo_type"/>
     *         &lt;element name="Stato">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="DA_ASSEGNARE"/>
     *               &lt;enumeration value="IN_LAVORAZIONE"/>
     *               &lt;enumeration value="AGLI_ATTI"/>
     *               &lt;enumeration value="CHIUSO"/>
     *               &lt;enumeration value="DA_SPEDIRE"/>
     *               &lt;enumeration value="INVIATO"/>
     *               &lt;enumeration value="SPEDITO"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idProtocollo",
        "numeroRegistrazione",
        "dataRegistrazione",
        "tipoProtocollo",
        "stato"
    })
    public static class Allacciante
        implements Serializable
    {

        @XmlElement(name = "IdProtocollo", required = true)
        protected String idProtocollo;
        @XmlElement(name = "NumeroRegistrazione")
        protected int numeroRegistrazione;
        @XmlElement(name = "DataRegistrazione", required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar dataRegistrazione;
        @XmlElement(name = "TipoProtocollo", required = true)
        @XmlSchemaType(name = "string")
        protected ProtTipoProtocolloType tipoProtocollo;
        @XmlElement(name = "Stato", required = true)
        protected String stato;

        /**
         * Recupera il valore della proprietà idProtocollo.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdProtocollo() {
            return idProtocollo;
        }

        /**
         * Imposta il valore della proprietà idProtocollo.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdProtocollo(String value) {
            this.idProtocollo = value;
        }

        /**
         * Recupera il valore della proprietà numeroRegistrazione.
         * 
         */
        public int getNumeroRegistrazione() {
            return numeroRegistrazione;
        }

        /**
         * Imposta il valore della proprietà numeroRegistrazione.
         * 
         */
        public void setNumeroRegistrazione(int value) {
            this.numeroRegistrazione = value;
        }

        /**
         * Recupera il valore della proprietà dataRegistrazione.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDataRegistrazione() {
            return dataRegistrazione;
        }

        /**
         * Imposta il valore della proprietà dataRegistrazione.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDataRegistrazione(XMLGregorianCalendar value) {
            this.dataRegistrazione = value;
        }

        /**
         * Recupera il valore della proprietà tipoProtocollo.
         * 
         * @return
         *     possible object is
         *     {@link ProtTipoProtocolloType }
         *     
         */
        public ProtTipoProtocolloType getTipoProtocollo() {
            return tipoProtocollo;
        }

        /**
         * Imposta il valore della proprietà tipoProtocollo.
         * 
         * @param value
         *     allowed object is
         *     {@link ProtTipoProtocolloType }
         *     
         */
        public void setTipoProtocollo(ProtTipoProtocolloType value) {
            this.tipoProtocollo = value;
        }

        /**
         * Recupera il valore della proprietà stato.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStato() {
            return stato;
        }

        /**
         * Imposta il valore della proprietà stato.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStato(String value) {
            this.stato = value;
        }

    }

}
