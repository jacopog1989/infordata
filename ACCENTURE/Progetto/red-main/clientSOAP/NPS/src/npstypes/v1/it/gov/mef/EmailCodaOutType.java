
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Coda di aleborazione Email in uscita
 * 
 * <p>Classe Java per email_coda_out_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="email_coda_out_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdCodaOut" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="TipoCoda" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DataAccodamento" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="DataSpedizione" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="MessaggioEmail" type="{http://mef.gov.it.v1.npsTypes}email_message_type"/>
 *         &lt;element name="TipoTrattamento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DataTrattamento" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="TipoFlusso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Priorita" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="RiferimentoProtocollo" type="{http://mef.gov.it.v1.npsTypes}prot_protocolloDatiMinimi_type" minOccurs="0"/>
 *         &lt;element name="SistemaProduttore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "email_coda_out_type", propOrder = {
    "idCodaOut",
    "tipoCoda",
    "dataAccodamento",
    "dataSpedizione",
    "messaggioEmail",
    "tipoTrattamento",
    "dataTrattamento",
    "tipoFlusso",
    "priorita",
    "riferimentoProtocollo",
    "sistemaProduttore"
})
public class EmailCodaOutType
    implements Serializable
{

    @XmlElement(name = "IdCodaOut", required = true)
    protected String idCodaOut;
    @XmlElement(name = "TipoCoda", required = true)
    protected String tipoCoda;
    @XmlElement(name = "DataAccodamento", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataAccodamento;
    @XmlElement(name = "DataSpedizione", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataSpedizione;
    @XmlElement(name = "MessaggioEmail", required = true)
    protected EmailMessageType messaggioEmail;
    @XmlElement(name = "TipoTrattamento", required = true)
    protected String tipoTrattamento;
    @XmlElement(name = "DataTrattamento", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataTrattamento;
    @XmlElement(name = "TipoFlusso", required = true)
    protected String tipoFlusso;
    @XmlElement(name = "Priorita")
    protected int priorita;
    @XmlElement(name = "RiferimentoProtocollo")
    protected ProtProtocolloDatiMinimiType riferimentoProtocollo;
    @XmlElement(name = "SistemaProduttore", required = true)
    protected String sistemaProduttore;

    /**
     * Recupera il valore della proprietà idCodaOut.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdCodaOut() {
        return idCodaOut;
    }

    /**
     * Imposta il valore della proprietà idCodaOut.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdCodaOut(String value) {
        this.idCodaOut = value;
    }

    /**
     * Recupera il valore della proprietà tipoCoda.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoCoda() {
        return tipoCoda;
    }

    /**
     * Imposta il valore della proprietà tipoCoda.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoCoda(String value) {
        this.tipoCoda = value;
    }

    /**
     * Recupera il valore della proprietà dataAccodamento.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAccodamento() {
        return dataAccodamento;
    }

    /**
     * Imposta il valore della proprietà dataAccodamento.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAccodamento(XMLGregorianCalendar value) {
        this.dataAccodamento = value;
    }

    /**
     * Recupera il valore della proprietà dataSpedizione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataSpedizione() {
        return dataSpedizione;
    }

    /**
     * Imposta il valore della proprietà dataSpedizione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataSpedizione(XMLGregorianCalendar value) {
        this.dataSpedizione = value;
    }

    /**
     * Recupera il valore della proprietà messaggioEmail.
     * 
     * @return
     *     possible object is
     *     {@link EmailMessageType }
     *     
     */
    public EmailMessageType getMessaggioEmail() {
        return messaggioEmail;
    }

    /**
     * Imposta il valore della proprietà messaggioEmail.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailMessageType }
     *     
     */
    public void setMessaggioEmail(EmailMessageType value) {
        this.messaggioEmail = value;
    }

    /**
     * Recupera il valore della proprietà tipoTrattamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoTrattamento() {
        return tipoTrattamento;
    }

    /**
     * Imposta il valore della proprietà tipoTrattamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoTrattamento(String value) {
        this.tipoTrattamento = value;
    }

    /**
     * Recupera il valore della proprietà dataTrattamento.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataTrattamento() {
        return dataTrattamento;
    }

    /**
     * Imposta il valore della proprietà dataTrattamento.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataTrattamento(XMLGregorianCalendar value) {
        this.dataTrattamento = value;
    }

    /**
     * Recupera il valore della proprietà tipoFlusso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoFlusso() {
        return tipoFlusso;
    }

    /**
     * Imposta il valore della proprietà tipoFlusso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoFlusso(String value) {
        this.tipoFlusso = value;
    }

    /**
     * Recupera il valore della proprietà priorita.
     * 
     */
    public int getPriorita() {
        return priorita;
    }

    /**
     * Imposta il valore della proprietà priorita.
     * 
     */
    public void setPriorita(int value) {
        this.priorita = value;
    }

    /**
     * Recupera il valore della proprietà riferimentoProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link ProtProtocolloDatiMinimiType }
     *     
     */
    public ProtProtocolloDatiMinimiType getRiferimentoProtocollo() {
        return riferimentoProtocollo;
    }

    /**
     * Imposta il valore della proprietà riferimentoProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtProtocolloDatiMinimiType }
     *     
     */
    public void setRiferimentoProtocollo(ProtProtocolloDatiMinimiType value) {
        this.riferimentoProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà sistemaProduttore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaProduttore() {
        return sistemaProduttore;
    }

    /**
     * Imposta il valore della proprietà sistemaProduttore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaProduttore(String value) {
        this.sistemaProduttore = value;
    }

}
