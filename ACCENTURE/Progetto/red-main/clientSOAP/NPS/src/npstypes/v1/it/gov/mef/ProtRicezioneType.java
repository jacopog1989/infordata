
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Definisce i tipo di dato relativio alla ricezione del protocollo
 * 
 * <p>Classe Java per prot_ricezione_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_ricezione_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="MezzoRicezione" type="{http://mef.gov.it.v1.npsTypes}all_codiceDescrizione_type"/>
 *         &lt;element name="DataRicezione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="DataProtocolloMittente" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="NumeroProtocolloMittente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_ricezione_type", propOrder = {
    "mezzoRicezione",
    "dataRicezione",
    "dataProtocolloMittente",
    "numeroProtocolloMittente"
})
public class ProtRicezioneType
    implements Serializable
{

    @XmlElement(name = "MezzoRicezione", required = true)
    protected AllCodiceDescrizioneType mezzoRicezione;
    @XmlElement(name = "DataRicezione", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataRicezione;
    @XmlElement(name = "DataProtocolloMittente")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataProtocolloMittente;
    @XmlElement(name = "NumeroProtocolloMittente")
    protected String numeroProtocolloMittente;

    /**
     * Recupera il valore della proprietà mezzoRicezione.
     * 
     * @return
     *     possible object is
     *     {@link AllCodiceDescrizioneType }
     *     
     */
    public AllCodiceDescrizioneType getMezzoRicezione() {
        return mezzoRicezione;
    }

    /**
     * Imposta il valore della proprietà mezzoRicezione.
     * 
     * @param value
     *     allowed object is
     *     {@link AllCodiceDescrizioneType }
     *     
     */
    public void setMezzoRicezione(AllCodiceDescrizioneType value) {
        this.mezzoRicezione = value;
    }

    /**
     * Recupera il valore della proprietà dataRicezione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataRicezione() {
        return dataRicezione;
    }

    /**
     * Imposta il valore della proprietà dataRicezione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataRicezione(XMLGregorianCalendar value) {
        this.dataRicezione = value;
    }

    /**
     * Recupera il valore della proprietà dataProtocolloMittente.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataProtocolloMittente() {
        return dataProtocolloMittente;
    }

    /**
     * Imposta il valore della proprietà dataProtocolloMittente.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataProtocolloMittente(XMLGregorianCalendar value) {
        this.dataProtocolloMittente = value;
    }

    /**
     * Recupera il valore della proprietà numeroProtocolloMittente.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroProtocolloMittente() {
        return numeroProtocolloMittente;
    }

    /**
     * Imposta il valore della proprietà numeroProtocolloMittente.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroProtocolloMittente(String value) {
        this.numeroProtocolloMittente = value;
    }

}
