
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce il tipo di dato Unità Organizzativa dell'Organigramma
 * 
 * <p>Classe Java per org_organigramma_unitaOrganizzativa_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="org_organigramma_unitaOrganizzativa_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Aoo" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_aoo_type"/>
 *         &lt;element name="Denominazione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CodiceUO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChiaveEsterna" type="{http://mef.gov.it.v1.npsTypes}all_chiaveEsterna_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "org_organigramma_unitaOrganizzativa_type", propOrder = {
    "aoo",
    "denominazione",
    "codiceUO",
    "chiaveEsterna"
})
public class OrgOrganigrammaUnitaOrganizzativaType
    implements Serializable
{

    @XmlElement(name = "Aoo", required = true)
    protected OrgOrganigrammaAooType aoo;
    @XmlElement(name = "Denominazione", required = true)
    protected String denominazione;
    @XmlElement(name = "CodiceUO")
    protected String codiceUO;
    @XmlElement(name = "ChiaveEsterna")
    protected AllChiaveEsternaType chiaveEsterna;

    /**
     * Recupera il valore della proprietà aoo.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaAooType }
     *     
     */
    public OrgOrganigrammaAooType getAoo() {
        return aoo;
    }

    /**
     * Imposta il valore della proprietà aoo.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaAooType }
     *     
     */
    public void setAoo(OrgOrganigrammaAooType value) {
        this.aoo = value;
    }

    /**
     * Recupera il valore della proprietà denominazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenominazione() {
        return denominazione;
    }

    /**
     * Imposta il valore della proprietà denominazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenominazione(String value) {
        this.denominazione = value;
    }

    /**
     * Recupera il valore della proprietà codiceUO.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceUO() {
        return codiceUO;
    }

    /**
     * Imposta il valore della proprietà codiceUO.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceUO(String value) {
        this.codiceUO = value;
    }

    /**
     * Recupera il valore della proprietà chiaveEsterna.
     * 
     * @return
     *     possible object is
     *     {@link AllChiaveEsternaType }
     *     
     */
    public AllChiaveEsternaType getChiaveEsterna() {
        return chiaveEsterna;
    }

    /**
     * Imposta il valore della proprietà chiaveEsterna.
     * 
     * @param value
     *     allowed object is
     *     {@link AllChiaveEsternaType }
     *     
     */
    public void setChiaveEsterna(AllChiaveEsternaType value) {
        this.chiaveEsterna = value;
    }

}
