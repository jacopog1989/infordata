
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import npsmessages.v1.it.gov.mef.RichiestaCreateProtocolloUscitaType;


/**
 * Definisce il tipo di dato per la protocollazione in uscita
 * 
 * <p>Classe Java per prot_protocolloUscita_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_protocolloUscita_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}prot_protocolloBaseRequest_type">
 *       &lt;sequence>
 *         &lt;element name="Risposta" type="{http://mef.gov.it.v1.npsTypes}prot_allaccioProtocollo_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_protocolloUscita_type", propOrder = {
    "risposta"
})
@XmlSeeAlso({
    RichiestaCreateProtocolloUscitaType.class
})
public class ProtProtocolloUscitaType
    extends ProtProtocolloBaseRequestType
    implements Serializable
{

    @XmlElement(name = "Risposta")
    protected ProtAllaccioProtocolloType risposta;

    /**
     * Recupera il valore della proprietà risposta.
     * 
     * @return
     *     possible object is
     *     {@link ProtAllaccioProtocolloType }
     *     
     */
    public ProtAllaccioProtocolloType getRisposta() {
        return risposta;
    }

    /**
     * Imposta il valore della proprietà risposta.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtAllaccioProtocolloType }
     *     
     */
    public void setRisposta(ProtAllaccioProtocolloType value) {
        this.risposta = value;
    }

}
