
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce il tipo di dato relativo all'allegato di posta elettronica
 * 
 * <p>Classe Java per email_allegatiMessaggio_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="email_allegatiMessaggio_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}doc_documentoFile_type">
 *       &lt;sequence>
 *         &lt;element name="TipoAllegato">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="ALLEGATO_GENERICO"/>
 *               &lt;enumeration value="DOCUMENTO_PRINCIPALE"/>
 *               &lt;enumeration value="POSTACERT"/>
 *               &lt;enumeration value="DATICERT"/>
 *               &lt;enumeration value="INTEROPERABILITA"/>
 *               &lt;enumeration value="DATI_FLUSSO"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "email_allegatiMessaggio_type", propOrder = {
    "tipoAllegato"
})
@XmlSeeAlso({
    npstypes.v1.it.gov.mef.EmailMessageType.AllegatiMessaggio.class
})
public class EmailAllegatiMessaggioType
    extends DocDocumentoFileType
    implements Serializable
{

    @XmlElement(name = "TipoAllegato", required = true)
    protected String tipoAllegato;

    /**
     * Recupera il valore della proprietà tipoAllegato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoAllegato() {
        return tipoAllegato;
    }

    /**
     * Imposta il valore della proprietà tipoAllegato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoAllegato(String value) {
        this.tipoAllegato = value;
    }

}
