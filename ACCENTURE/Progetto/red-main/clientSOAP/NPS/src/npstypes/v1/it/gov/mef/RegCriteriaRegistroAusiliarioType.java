
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Criteri di filtro per l'estrazione dei registri provvisori
 * 
 * <p>Classe Java per reg_criteriaRegistroAusiliario_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="reg_criteriaRegistroAusiliario_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Aoo" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_aoo_type"/>
 *         &lt;element name="TipoRegistro" type="{http://mef.gov.it.v1.npsTypes}all_tipoRegistroAusiliario_type" minOccurs="0"/>
 *         &lt;element name="Stato" type="{http://mef.gov.it.v1.npsTypes}all_statoRegistroAusiliario_type" minOccurs="0"/>
 *         &lt;element name="IsAssociatoRegistroUfficiale" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsLocked" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsAutomatico" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SistemaProduttore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Applicazione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reg_criteriaRegistroAusiliario_type", propOrder = {
    "aoo",
    "tipoRegistro",
    "stato",
    "isAssociatoRegistroUfficiale",
    "isLocked",
    "isAutomatico",
    "sistemaProduttore",
    "applicazione"
})
public class RegCriteriaRegistroAusiliarioType
    implements Serializable
{

    @XmlElement(name = "Aoo", required = true)
    protected OrgOrganigrammaAooType aoo;
    @XmlElement(name = "TipoRegistro")
    @XmlSchemaType(name = "string")
    protected AllTipoRegistroAusiliarioType tipoRegistro;
    @XmlElement(name = "Stato")
    @XmlSchemaType(name = "string")
    protected AllStatoRegistroAusiliarioType stato;
    @XmlElement(name = "IsAssociatoRegistroUfficiale")
    protected Boolean isAssociatoRegistroUfficiale;
    @XmlElement(name = "IsLocked")
    protected Boolean isLocked;
    @XmlElement(name = "IsAutomatico")
    protected Boolean isAutomatico;
    @XmlElement(name = "SistemaProduttore")
    protected String sistemaProduttore;
    @XmlElement(name = "Applicazione")
    protected String applicazione;

    /**
     * Recupera il valore della proprietà aoo.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaAooType }
     *     
     */
    public OrgOrganigrammaAooType getAoo() {
        return aoo;
    }

    /**
     * Imposta il valore della proprietà aoo.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaAooType }
     *     
     */
    public void setAoo(OrgOrganigrammaAooType value) {
        this.aoo = value;
    }

    /**
     * Recupera il valore della proprietà tipoRegistro.
     * 
     * @return
     *     possible object is
     *     {@link AllTipoRegistroAusiliarioType }
     *     
     */
    public AllTipoRegistroAusiliarioType getTipoRegistro() {
        return tipoRegistro;
    }

    /**
     * Imposta il valore della proprietà tipoRegistro.
     * 
     * @param value
     *     allowed object is
     *     {@link AllTipoRegistroAusiliarioType }
     *     
     */
    public void setTipoRegistro(AllTipoRegistroAusiliarioType value) {
        this.tipoRegistro = value;
    }

    /**
     * Recupera il valore della proprietà stato.
     * 
     * @return
     *     possible object is
     *     {@link AllStatoRegistroAusiliarioType }
     *     
     */
    public AllStatoRegistroAusiliarioType getStato() {
        return stato;
    }

    /**
     * Imposta il valore della proprietà stato.
     * 
     * @param value
     *     allowed object is
     *     {@link AllStatoRegistroAusiliarioType }
     *     
     */
    public void setStato(AllStatoRegistroAusiliarioType value) {
        this.stato = value;
    }

    /**
     * Recupera il valore della proprietà isAssociatoRegistroUfficiale.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsAssociatoRegistroUfficiale() {
        return isAssociatoRegistroUfficiale;
    }

    /**
     * Imposta il valore della proprietà isAssociatoRegistroUfficiale.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsAssociatoRegistroUfficiale(Boolean value) {
        this.isAssociatoRegistroUfficiale = value;
    }

    /**
     * Recupera il valore della proprietà isLocked.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsLocked() {
        return isLocked;
    }

    /**
     * Imposta il valore della proprietà isLocked.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsLocked(Boolean value) {
        this.isLocked = value;
    }

    /**
     * Recupera il valore della proprietà isAutomatico.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsAutomatico() {
        return isAutomatico;
    }

    /**
     * Imposta il valore della proprietà isAutomatico.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsAutomatico(Boolean value) {
        this.isAutomatico = value;
    }

    /**
     * Recupera il valore della proprietà sistemaProduttore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaProduttore() {
        return sistemaProduttore;
    }

    /**
     * Imposta il valore della proprietà sistemaProduttore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaProduttore(String value) {
        this.sistemaProduttore = value;
    }

    /**
     * Recupera il valore della proprietà applicazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicazione() {
        return applicazione;
    }

    /**
     * Imposta il valore della proprietà applicazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicazione(String value) {
        this.applicazione = value;
    }

}
