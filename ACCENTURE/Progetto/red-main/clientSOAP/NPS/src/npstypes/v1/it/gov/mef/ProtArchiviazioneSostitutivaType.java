
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Tipo di dato che definisce i dati per l'archiviazione sostitutiva
 * 
 * <p>Classe Java per prot_archiviazioneSostitutiva_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_archiviazioneSostitutiva_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Ticket" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SistemaDiRiferimento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Flusso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_archiviazioneSostitutiva_type", propOrder = {
    "ticket",
    "sistemaDiRiferimento",
    "flusso"
})
public class ProtArchiviazioneSostitutivaType
    implements Serializable
{

    @XmlElement(name = "Ticket", required = true)
    protected String ticket;
    @XmlElement(name = "SistemaDiRiferimento", required = true)
    protected String sistemaDiRiferimento;
    @XmlElement(name = "Flusso")
    protected String flusso;

    /**
     * Recupera il valore della proprietà ticket.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTicket() {
        return ticket;
    }

    /**
     * Imposta il valore della proprietà ticket.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTicket(String value) {
        this.ticket = value;
    }

    /**
     * Recupera il valore della proprietà sistemaDiRiferimento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaDiRiferimento() {
        return sistemaDiRiferimento;
    }

    /**
     * Imposta il valore della proprietà sistemaDiRiferimento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaDiRiferimento(String value) {
        this.sistemaDiRiferimento = value;
    }

    /**
     * Recupera il valore della proprietà flusso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFlusso() {
        return flusso;
    }

    /**
     * Imposta il valore della proprietà flusso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFlusso(String value) {
        this.flusso = value;
    }

}
