
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import npsmessages.v1.it.gov.mef.RichiestaCreateProtocolloEntrataType;


/**
 * Definisce il tipo di dato per la protocollazione in entrata
 * 
 * <p>Classe Java per prot_protocolloEntrata_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_protocolloEntrata_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}prot_protocolloBaseRequest_type">
 *       &lt;sequence>
 *         &lt;element name="Assegnatari" type="{http://mef.gov.it.v1.npsTypes}prot_assegnatario_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="IdMessaggioPostaInteroperabile" type="{http://mef.gov.it.v1.npsTypes}Guid" minOccurs="0"/>
 *         &lt;element name="SpedisciRicevutaHtml" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_protocolloEntrata_type", propOrder = {
    "assegnatari",
    "idMessaggioPostaInteroperabile",
    "spedisciRicevutaHtml"
})
@XmlSeeAlso({
    RichiestaCreateProtocolloEntrataType.class
})
public class ProtProtocolloEntrataType
    extends ProtProtocolloBaseRequestType
    implements Serializable
{

    @XmlElement(name = "Assegnatari")
    protected List<ProtAssegnatarioType> assegnatari;
    @XmlElement(name = "IdMessaggioPostaInteroperabile")
    protected String idMessaggioPostaInteroperabile;
    @XmlElement(name = "SpedisciRicevutaHtml")
    protected Boolean spedisciRicevutaHtml;

    /**
     * Gets the value of the assegnatari property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the assegnatari property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssegnatari().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtAssegnatarioType }
     * 
     * 
     */
    public List<ProtAssegnatarioType> getAssegnatari() {
        if (assegnatari == null) {
            assegnatari = new ArrayList<ProtAssegnatarioType>();
        }
        return this.assegnatari;
    }

    /**
     * Recupera il valore della proprietà idMessaggioPostaInteroperabile.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdMessaggioPostaInteroperabile() {
        return idMessaggioPostaInteroperabile;
    }

    /**
     * Imposta il valore della proprietà idMessaggioPostaInteroperabile.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdMessaggioPostaInteroperabile(String value) {
        this.idMessaggioPostaInteroperabile = value;
    }

    /**
     * Recupera il valore della proprietà spedisciRicevutaHtml.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSpedisciRicevutaHtml() {
        return spedisciRicevutaHtml;
    }

    /**
     * Imposta il valore della proprietà spedisciRicevutaHtml.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSpedisciRicevutaHtml(Boolean value) {
        this.spedisciRicevutaHtml = value;
    }

}
