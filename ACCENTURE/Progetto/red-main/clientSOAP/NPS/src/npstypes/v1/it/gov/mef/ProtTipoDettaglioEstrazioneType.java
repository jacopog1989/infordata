
package npstypes.v1.it.gov.mef;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per prot_tipoDettaglioEstrazione_type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="prot_tipoDettaglioEstrazione_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DATI_MINIMI"/>
 *     &lt;enumeration value="DATI_MINIMI_ESTESI"/>
 *     &lt;enumeration value="ALLACCIO"/>
 *     &lt;enumeration value="ALLEGATI"/>
 *     &lt;enumeration value="ALLEGATI_PREVIEW"/>
 *     &lt;enumeration value="ALLEGATI_OPERAZIONI"/>
 *     &lt;enumeration value="ALLEGATI_ATMOS"/>
 *     &lt;enumeration value="ASSEGNATARI"/>
 *     &lt;enumeration value="DESTINATARI_RICEVUTE_PEC"/>
 *     &lt;enumeration value="DESTINATARI_RICEVUTE_INTEROPERABILITA"/>
 *     &lt;enumeration value="SEGNATURA"/>
 *     &lt;enumeration value="STORICO_OPERAZIONI"/>
 *     &lt;enumeration value="STORICO_OPERAZIONI_REGISTRO"/>
 *     &lt;enumeration value="ANNOTAZIONI"/>
 *     &lt;enumeration value="RICEVUTE_INTEROPERABILITA"/>
 *     &lt;enumeration value="ANNULLAMENTI_PARZIALI"/>
 *     &lt;enumeration value="METADATI_TIPOLOGIA_DOCUMENTALE"/>
 *     &lt;enumeration value="ACL_ESTESE"/>
 *     &lt;enumeration value="ACL_ESTESE_AUSILIARIO"/>
 *     &lt;enumeration value="ACL_ESTESE_PRODUTTORE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "prot_tipoDettaglioEstrazione_type")
@XmlEnum
public enum ProtTipoDettaglioEstrazioneType {

    DATI_MINIMI,
    DATI_MINIMI_ESTESI,
    ALLACCIO,
    ALLEGATI,
    ALLEGATI_PREVIEW,
    ALLEGATI_OPERAZIONI,
    ALLEGATI_ATMOS,
    ASSEGNATARI,
    DESTINATARI_RICEVUTE_PEC,
    DESTINATARI_RICEVUTE_INTEROPERABILITA,
    SEGNATURA,
    STORICO_OPERAZIONI,
    STORICO_OPERAZIONI_REGISTRO,
    ANNOTAZIONI,
    RICEVUTE_INTEROPERABILITA,
    ANNULLAMENTI_PARZIALI,
    METADATI_TIPOLOGIA_DOCUMENTALE,
    ACL_ESTESE,
    ACL_ESTESE_AUSILIARIO,
    ACL_ESTESE_PRODUTTORE;

    public String value() {
        return name();
    }

    public static ProtTipoDettaglioEstrazioneType fromValue(String v) {
        return valueOf(v);
    }

}
