
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce il tipo di dato del registro di protocollo
 * 
 * <p>Classe Java per reg_registro_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="reg_registro_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Aoo" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_aoo_type"/>
 *         &lt;element name="Codice" type="{http://mef.gov.it.v1.npsTypes}all_codiceDescrizione_type"/>
 *         &lt;element name="TipoRegistro" type="{http://mef.gov.it.v1.npsTypes}all_tipoRegistroAusiliario_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reg_registro_type", propOrder = {
    "aoo",
    "codice",
    "tipoRegistro"
})
public class RegRegistroType
    implements Serializable
{

    @XmlElement(name = "Aoo", required = true)
    protected OrgOrganigrammaAooType aoo;
    @XmlElement(name = "Codice", required = true)
    protected AllCodiceDescrizioneType codice;
    @XmlElement(name = "TipoRegistro", required = true)
    @XmlSchemaType(name = "string")
    protected AllTipoRegistroAusiliarioType tipoRegistro;

    /**
     * Recupera il valore della proprietà aoo.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaAooType }
     *     
     */
    public OrgOrganigrammaAooType getAoo() {
        return aoo;
    }

    /**
     * Imposta il valore della proprietà aoo.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaAooType }
     *     
     */
    public void setAoo(OrgOrganigrammaAooType value) {
        this.aoo = value;
    }

    /**
     * Recupera il valore della proprietà codice.
     * 
     * @return
     *     possible object is
     *     {@link AllCodiceDescrizioneType }
     *     
     */
    public AllCodiceDescrizioneType getCodice() {
        return codice;
    }

    /**
     * Imposta il valore della proprietà codice.
     * 
     * @param value
     *     allowed object is
     *     {@link AllCodiceDescrizioneType }
     *     
     */
    public void setCodice(AllCodiceDescrizioneType value) {
        this.codice = value;
    }

    /**
     * Recupera il valore della proprietà tipoRegistro.
     * 
     * @return
     *     possible object is
     *     {@link AllTipoRegistroAusiliarioType }
     *     
     */
    public AllTipoRegistroAusiliarioType getTipoRegistro() {
        return tipoRegistro;
    }

    /**
     * Imposta il valore della proprietà tipoRegistro.
     * 
     * @param value
     *     allowed object is
     *     {@link AllTipoRegistroAusiliarioType }
     *     
     */
    public void setTipoRegistro(AllTipoRegistroAusiliarioType value) {
        this.tipoRegistro = value;
    }

}
