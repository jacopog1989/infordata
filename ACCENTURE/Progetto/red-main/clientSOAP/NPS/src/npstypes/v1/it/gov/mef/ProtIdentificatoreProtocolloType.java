
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Definisce il tipo di dato Identificatore del Protocollo
 * 
 * <p>Classe Java per prot_identificatoreProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_identificatoreProtocollo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Registro" type="{http://mef.gov.it.v1.npsTypes}prot_registroProtocollo_type"/>
 *         &lt;element name="DataRegistrazione" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="NumeroRegistrazione" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TipoProtocollo" type="{http://mef.gov.it.v1.npsTypes}prot_tipoProtocollo_type"/>
 *         &lt;element name="Destinatario" type="{http://mef.gov.it.v1.npsTypes}email_indirizzoEmail_type" minOccurs="0"/>
 *         &lt;element name="IdProtocollo" type="{http://mef.gov.it.v1.npsTypes}Guid" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_identificatoreProtocollo_type", propOrder = {
    "registro",
    "dataRegistrazione",
    "numeroRegistrazione",
    "tipoProtocollo",
    "destinatario",
    "idProtocollo"
})
public class ProtIdentificatoreProtocolloType
    implements Serializable
{

    @XmlElement(name = "Registro", required = true)
    protected ProtRegistroProtocolloType registro;
    @XmlElement(name = "DataRegistrazione")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataRegistrazione;
    @XmlElement(name = "NumeroRegistrazione")
    protected Integer numeroRegistrazione;
    @XmlElement(name = "TipoProtocollo", required = true)
    @XmlSchemaType(name = "string")
    protected ProtTipoProtocolloType tipoProtocollo;
    @XmlElement(name = "Destinatario")
    protected EmailIndirizzoEmailType destinatario;
    @XmlElement(name = "IdProtocollo")
    protected String idProtocollo;

    /**
     * Recupera il valore della proprietà registro.
     * 
     * @return
     *     possible object is
     *     {@link ProtRegistroProtocolloType }
     *     
     */
    public ProtRegistroProtocolloType getRegistro() {
        return registro;
    }

    /**
     * Imposta il valore della proprietà registro.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtRegistroProtocolloType }
     *     
     */
    public void setRegistro(ProtRegistroProtocolloType value) {
        this.registro = value;
    }

    /**
     * Recupera il valore della proprietà dataRegistrazione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataRegistrazione() {
        return dataRegistrazione;
    }

    /**
     * Imposta il valore della proprietà dataRegistrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataRegistrazione(XMLGregorianCalendar value) {
        this.dataRegistrazione = value;
    }

    /**
     * Recupera il valore della proprietà numeroRegistrazione.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumeroRegistrazione() {
        return numeroRegistrazione;
    }

    /**
     * Imposta il valore della proprietà numeroRegistrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumeroRegistrazione(Integer value) {
        this.numeroRegistrazione = value;
    }

    /**
     * Recupera il valore della proprietà tipoProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link ProtTipoProtocolloType }
     *     
     */
    public ProtTipoProtocolloType getTipoProtocollo() {
        return tipoProtocollo;
    }

    /**
     * Imposta il valore della proprietà tipoProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtTipoProtocolloType }
     *     
     */
    public void setTipoProtocollo(ProtTipoProtocolloType value) {
        this.tipoProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà destinatario.
     * 
     * @return
     *     possible object is
     *     {@link EmailIndirizzoEmailType }
     *     
     */
    public EmailIndirizzoEmailType getDestinatario() {
        return destinatario;
    }

    /**
     * Imposta il valore della proprietà destinatario.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailIndirizzoEmailType }
     *     
     */
    public void setDestinatario(EmailIndirizzoEmailType value) {
        this.destinatario = value;
    }

    /**
     * Recupera il valore della proprietà idProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdProtocollo() {
        return idProtocollo;
    }

    /**
     * Imposta il valore della proprietà idProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdProtocollo(String value) {
        this.idProtocollo = value;
    }

}
