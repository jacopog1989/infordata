
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Definisce il tipo di dato del Documento Protocollato
 * 
 * <p>Classe Java per prot_documentoProtocollato_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_documentoProtocollato_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}doc_documentoFile_type">
 *       &lt;sequence>
 *         &lt;element name="DataCreazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="MetadatiDocumento" type="{http://mef.gov.it.v1.npsTypes}prot_documentoMetadataProtocollato_type" minOccurs="0"/>
 *         &lt;element name="Attivo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SistemaProduttore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IsDocumentoOriginale" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsDocumentoTrattato" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="DocumentoOrigine" type="{http://mef.gov.it.v1.npsTypes}doc_documentoFile_type" minOccurs="0"/>
 *         &lt;element name="Operazione" type="{http://mef.gov.it.v1.npsTypes}doc_operazioneDocumentale_type" minOccurs="0"/>
 *         &lt;element name="ArchiviazioneSostitutiva" type="{http://mef.gov.it.v1.npsTypes}prot_archiviazioneSostitutiva_type" minOccurs="0"/>
 *         &lt;element name="PreviewPng" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_documentoProtocollato_type", propOrder = {
    "dataCreazione",
    "metadatiDocumento",
    "attivo",
    "sistemaProduttore",
    "isDocumentoOriginale",
    "isDocumentoTrattato",
    "documentoOrigine",
    "operazione",
    "archiviazioneSostitutiva",
    "previewPng"
})
public class ProtDocumentoProtocollatoType
    extends DocDocumentoFileType
    implements Serializable
{

    @XmlElement(name = "DataCreazione", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataCreazione;
    @XmlElement(name = "MetadatiDocumento")
    protected ProtDocumentoMetadataProtocollatoType metadatiDocumento;
    @XmlElement(name = "Attivo", defaultValue = "true")
    protected boolean attivo;
    @XmlElement(name = "SistemaProduttore", required = true)
    protected String sistemaProduttore;
    @XmlElement(name = "IsDocumentoOriginale", defaultValue = "true")
    protected boolean isDocumentoOriginale;
    @XmlElement(name = "IsDocumentoTrattato", defaultValue = "true")
    protected boolean isDocumentoTrattato;
    @XmlElement(name = "DocumentoOrigine")
    protected DocDocumentoFileType documentoOrigine;
    @XmlElement(name = "Operazione")
    protected DocOperazioneDocumentaleType operazione;
    @XmlElementRef(name = "ArchiviazioneSostitutiva", type = JAXBElement.class, required = false)
    protected JAXBElement<ProtArchiviazioneSostitutivaType> archiviazioneSostitutiva;
    @XmlElement(name = "PreviewPng")
    protected byte[] previewPng;

    /**
     * Recupera il valore della proprietà dataCreazione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataCreazione() {
        return dataCreazione;
    }

    /**
     * Imposta il valore della proprietà dataCreazione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataCreazione(XMLGregorianCalendar value) {
        this.dataCreazione = value;
    }

    /**
     * Recupera il valore della proprietà metadatiDocumento.
     * 
     * @return
     *     possible object is
     *     {@link ProtDocumentoMetadataProtocollatoType }
     *     
     */
    public ProtDocumentoMetadataProtocollatoType getMetadatiDocumento() {
        return metadatiDocumento;
    }

    /**
     * Imposta il valore della proprietà metadatiDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtDocumentoMetadataProtocollatoType }
     *     
     */
    public void setMetadatiDocumento(ProtDocumentoMetadataProtocollatoType value) {
        this.metadatiDocumento = value;
    }

    /**
     * Recupera il valore della proprietà attivo.
     * 
     */
    public boolean isAttivo() {
        return attivo;
    }

    /**
     * Imposta il valore della proprietà attivo.
     * 
     */
    public void setAttivo(boolean value) {
        this.attivo = value;
    }

    /**
     * Recupera il valore della proprietà sistemaProduttore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaProduttore() {
        return sistemaProduttore;
    }

    /**
     * Imposta il valore della proprietà sistemaProduttore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaProduttore(String value) {
        this.sistemaProduttore = value;
    }

    /**
     * Recupera il valore della proprietà isDocumentoOriginale.
     * 
     */
    public boolean isIsDocumentoOriginale() {
        return isDocumentoOriginale;
    }

    /**
     * Imposta il valore della proprietà isDocumentoOriginale.
     * 
     */
    public void setIsDocumentoOriginale(boolean value) {
        this.isDocumentoOriginale = value;
    }

    /**
     * Recupera il valore della proprietà isDocumentoTrattato.
     * 
     */
    public boolean isIsDocumentoTrattato() {
        return isDocumentoTrattato;
    }

    /**
     * Imposta il valore della proprietà isDocumentoTrattato.
     * 
     */
    public void setIsDocumentoTrattato(boolean value) {
        this.isDocumentoTrattato = value;
    }

    /**
     * Recupera il valore della proprietà documentoOrigine.
     * 
     * @return
     *     possible object is
     *     {@link DocDocumentoFileType }
     *     
     */
    public DocDocumentoFileType getDocumentoOrigine() {
        return documentoOrigine;
    }

    /**
     * Imposta il valore della proprietà documentoOrigine.
     * 
     * @param value
     *     allowed object is
     *     {@link DocDocumentoFileType }
     *     
     */
    public void setDocumentoOrigine(DocDocumentoFileType value) {
        this.documentoOrigine = value;
    }

    /**
     * Recupera il valore della proprietà operazione.
     * 
     * @return
     *     possible object is
     *     {@link DocOperazioneDocumentaleType }
     *     
     */
    public DocOperazioneDocumentaleType getOperazione() {
        return operazione;
    }

    /**
     * Imposta il valore della proprietà operazione.
     * 
     * @param value
     *     allowed object is
     *     {@link DocOperazioneDocumentaleType }
     *     
     */
    public void setOperazione(DocOperazioneDocumentaleType value) {
        this.operazione = value;
    }

    /**
     * Recupera il valore della proprietà archiviazioneSostitutiva.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ProtArchiviazioneSostitutivaType }{@code >}
     *     
     */
    public JAXBElement<ProtArchiviazioneSostitutivaType> getArchiviazioneSostitutiva() {
        return archiviazioneSostitutiva;
    }

    /**
     * Imposta il valore della proprietà archiviazioneSostitutiva.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ProtArchiviazioneSostitutivaType }{@code >}
     *     
     */
    public void setArchiviazioneSostitutiva(JAXBElement<ProtArchiviazioneSostitutivaType> value) {
        this.archiviazioneSostitutiva = value;
    }

    /**
     * Recupera il valore della proprietà previewPng.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getPreviewPng() {
        return previewPng;
    }

    /**
     * Imposta il valore della proprietà previewPng.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setPreviewPng(byte[] value) {
        this.previewPng = value;
    }

}
