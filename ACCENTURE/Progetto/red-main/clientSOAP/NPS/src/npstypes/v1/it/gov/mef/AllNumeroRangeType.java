
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce il tipo Intervallo numerico
 * 
 * <p>Classe Java per all_numeroRange_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="all_numeroRange_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NumeroDa" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="NumeroA" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "all_numeroRange_type", propOrder = {
    "numeroDa",
    "numeroA"
})
public class AllNumeroRangeType
    implements Serializable
{

    @XmlElement(name = "NumeroDa")
    protected int numeroDa;
    @XmlElement(name = "NumeroA")
    protected Integer numeroA;

    /**
     * Recupera il valore della proprietà numeroDa.
     * 
     */
    public int getNumeroDa() {
        return numeroDa;
    }

    /**
     * Imposta il valore della proprietà numeroDa.
     * 
     */
    public void setNumeroDa(int value) {
        this.numeroDa = value;
    }

    /**
     * Recupera il valore della proprietà numeroA.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumeroA() {
        return numeroA;
    }

    /**
     * Imposta il valore della proprietà numeroA.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumeroA(Integer value) {
        this.numeroA = value;
    }

}
