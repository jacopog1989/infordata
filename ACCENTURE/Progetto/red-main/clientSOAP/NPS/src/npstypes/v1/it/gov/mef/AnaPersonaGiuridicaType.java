
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce i metadati relativi ad una persona giuridica
 * 
 * <p>Classe Java per ana_personaGiuridica_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="ana_personaGiuridica_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Denominazione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PartitaIVA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Referente" type="{http://mef.gov.it.v1.npsTypes}ana_personaFisica_type" minOccurs="0"/>
 *         &lt;element name="ChiaveEsterna" type="{http://mef.gov.it.v1.npsTypes}all_chiaveEsterna_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ana_personaGiuridica_type", propOrder = {
    "denominazione",
    "partitaIVA",
    "referente",
    "chiaveEsterna"
})
public class AnaPersonaGiuridicaType
    implements Serializable
{

    @XmlElement(name = "Denominazione", required = true)
    protected String denominazione;
    @XmlElement(name = "PartitaIVA")
    protected String partitaIVA;
    @XmlElement(name = "Referente")
    protected AnaPersonaFisicaType referente;
    @XmlElement(name = "ChiaveEsterna")
    protected AllChiaveEsternaType chiaveEsterna;

    /**
     * Recupera il valore della proprietà denominazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDenominazione() {
        return denominazione;
    }

    /**
     * Imposta il valore della proprietà denominazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDenominazione(String value) {
        this.denominazione = value;
    }

    /**
     * Recupera il valore della proprietà partitaIVA.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartitaIVA() {
        return partitaIVA;
    }

    /**
     * Imposta il valore della proprietà partitaIVA.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartitaIVA(String value) {
        this.partitaIVA = value;
    }

    /**
     * Recupera il valore della proprietà referente.
     * 
     * @return
     *     possible object is
     *     {@link AnaPersonaFisicaType }
     *     
     */
    public AnaPersonaFisicaType getReferente() {
        return referente;
    }

    /**
     * Imposta il valore della proprietà referente.
     * 
     * @param value
     *     allowed object is
     *     {@link AnaPersonaFisicaType }
     *     
     */
    public void setReferente(AnaPersonaFisicaType value) {
        this.referente = value;
    }

    /**
     * Recupera il valore della proprietà chiaveEsterna.
     * 
     * @return
     *     possible object is
     *     {@link AllChiaveEsternaType }
     *     
     */
    public AllChiaveEsternaType getChiaveEsterna() {
        return chiaveEsterna;
    }

    /**
     * Imposta il valore della proprietà chiaveEsterna.
     * 
     * @param value
     *     allowed object is
     *     {@link AllChiaveEsternaType }
     *     
     */
    public void setChiaveEsterna(AllChiaveEsternaType value) {
        this.chiaveEsterna = value;
    }

}
