
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce la validazione della segnatura
 * 
 * <p>Classe Java per prot_validazioneSegnatura_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_validazioneSegnatura_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EsitoValidazione">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="SEGNATURA_VALIDA"/>
 *               &lt;enumeration value="SEGNATURA_ERRATA"/>
 *               &lt;enumeration value="SEGNATURA_NON_CONFORME"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="ErroriRiscontrati" type="{http://mef.gov.it.v1.npsTypes}prot_erroreValidazioneSegnatura_type" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_validazioneSegnatura_type", propOrder = {
    "esitoValidazione",
    "erroriRiscontrati"
})
public class ProtValidazioneSegnaturaType
    implements Serializable
{

    @XmlElement(name = "EsitoValidazione", required = true)
    protected String esitoValidazione;
    @XmlElement(name = "ErroriRiscontrati")
    protected List<ProtErroreValidazioneSegnaturaType> erroriRiscontrati;

    /**
     * Recupera il valore della proprietà esitoValidazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEsitoValidazione() {
        return esitoValidazione;
    }

    /**
     * Imposta il valore della proprietà esitoValidazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEsitoValidazione(String value) {
        this.esitoValidazione = value;
    }

    /**
     * Gets the value of the erroriRiscontrati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the erroriRiscontrati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getErroriRiscontrati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtErroreValidazioneSegnaturaType }
     * 
     * 
     */
    public List<ProtErroreValidazioneSegnaturaType> getErroriRiscontrati() {
        if (erroriRiscontrati == null) {
            erroriRiscontrati = new ArrayList<ProtErroreValidazioneSegnaturaType>();
        }
        return this.erroriRiscontrati;
    }

}
