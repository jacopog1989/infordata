
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Criteri di filtro per l'estrazione dei documenti dei Documenti Raccolta Provvisoria
 * 
 * <p>Classe Java per prot_criteriaProtocolli_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_criteriaProtocolli_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Registro" type="{http://mef.gov.it.v1.npsTypes}prot_registroProtocollo_type"/>
 *         &lt;element name="Anno" type="{http://www.w3.org/2001/XMLSchema}short" minOccurs="0"/>
 *         &lt;element name="DataRegistrazione" type="{http://mef.gov.it.v1.npsTypes}all_dataRange_type" minOccurs="0"/>
 *         &lt;element name="NumeroRegistrazione" type="{http://mef.gov.it.v1.npsTypes}all_numeroRange_type" minOccurs="0"/>
 *         &lt;element name="DataAnnullamento" type="{http://mef.gov.it.v1.npsTypes}all_dataRange_type" minOccurs="0"/>
 *         &lt;element name="TipoProtocollo" type="{http://mef.gov.it.v1.npsTypes}prot_tipoProtocollo_type" minOccurs="0"/>
 *         &lt;element name="Oggetto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipologiaDocumentale" type="{http://mef.gov.it.v1.npsTypes}prot_tipologiaDocumento_type" minOccurs="0"/>
 *         &lt;element name="Protocollatore" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type" minOccurs="0"/>
 *         &lt;element name="Acl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DatiRicezione" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Mittente" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="DataRicezione" type="{http://mef.gov.it.v1.npsTypes}all_dataRange_type" minOccurs="0"/>
 *                   &lt;element name="NumeroRegistrazioneMittente" type="{http://mef.gov.it.v1.npsTypes}all_numeroRange_type" minOccurs="0"/>
 *                   &lt;element name="DataProtocolloMittente" type="{http://mef.gov.it.v1.npsTypes}all_dataRange_type" minOccurs="0"/>
 *                   &lt;element name="MezzoRicezione" type="{http://mef.gov.it.v1.npsTypes}all_codiceDescrizione_type" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DatiSpedizione" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Destinatario" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="DataSpedizione" type="{http://mef.gov.it.v1.npsTypes}all_dataRange_type" minOccurs="0"/>
 *                   &lt;element name="StatoSpedizione" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="DA_SPEDIRE"/>
 *                         &lt;enumeration value="IN_SPEDIZIONE"/>
 *                         &lt;enumeration value="SPEDITO"/>
 *                         &lt;enumeration value="ERRORE"/>
 *                         &lt;enumeration value="RICEVUTA_ACCETTAZIONE"/>
 *                         &lt;enumeration value="RICEVUTA_CONSEGNA"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="IndirizzoEmail" type="{http://mef.gov.it.v1.npsTypes}email_indirizzoEmail_type" minOccurs="0"/>
 *                   &lt;element name="MezzoSpedizione" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;choice>
 *                             &lt;element name="Elettronico">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;choice>
 *                                       &lt;element name="CooperazioneApplicativa" type="{http://www.w3.org/2001/XMLSchema}anyURI"/>
 *                                       &lt;element name="PostaElettronica" type="{http://mef.gov.it.v1.npsTypes}org_casellaEmail_type"/>
 *                                     &lt;/choice>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="Cartaceo" type="{http://mef.gov.it.v1.npsTypes}all_codiceDescrizione_type"/>
 *                           &lt;/choice>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Stato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsRiservato" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsCompleto" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsAnnullato" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="DatiAssegnazione" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Assegnatario" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type" minOccurs="0"/>
 *                   &lt;element name="Assegnante" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="DatiAssegnazioneApplicazione" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Assegnatario" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type" minOccurs="0"/>
 *                   &lt;element name="Assegnante" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type" minOccurs="0"/>
 *                   &lt;element name="Applicazione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Stato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="RegistroEmergenza" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="DataRegistrazione" type="{http://mef.gov.it.v1.npsTypes}all_dataRange_type"/>
 *                   &lt;element name="NumeroRegistrazione" type="{http://mef.gov.it.v1.npsTypes}all_numeroRange_type"/>
 *                   &lt;element name="TipoProtocollo" type="{http://mef.gov.it.v1.npsTypes}prot_tipoProtocollo_type" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="VoceTitolario" type="{http://mef.gov.it.v1.npsTypes}all_codiceDescrizione_type" minOccurs="0"/>
 *         &lt;element name="SistemaProduttore" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SistemaAusiliario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoDettaglioElenco" type="{http://mef.gov.it.v1.npsTypes}prot_tipoDettaglioEstrazione_type" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_criteriaProtocolli_type", propOrder = {
    "registro",
    "anno",
    "dataRegistrazione",
    "numeroRegistrazione",
    "dataAnnullamento",
    "tipoProtocollo",
    "oggetto",
    "tipologiaDocumentale",
    "protocollatore",
    "acl",
    "datiRicezione",
    "datiSpedizione",
    "stato",
    "isRiservato",
    "isCompleto",
    "isAnnullato",
    "datiAssegnazione",
    "datiAssegnazioneApplicazione",
    "registroEmergenza",
    "voceTitolario",
    "sistemaProduttore",
    "sistemaAusiliario",
    "tipoDettaglioElenco"
})
@XmlSeeAlso({
    npsmessages.v1.it.gov.mef.RichiestaRicercaProtocolloType.DatiRicerca.class
})
public class ProtCriteriaProtocolliType
    implements Serializable
{

    @XmlElement(name = "Registro", required = true)
    protected ProtRegistroProtocolloType registro;
    @XmlElement(name = "Anno")
    protected Short anno;
    @XmlElement(name = "DataRegistrazione")
    protected AllDataRangeType dataRegistrazione;
    @XmlElement(name = "NumeroRegistrazione")
    protected AllNumeroRangeType numeroRegistrazione;
    @XmlElement(name = "DataAnnullamento")
    protected AllDataRangeType dataAnnullamento;
    @XmlElement(name = "TipoProtocollo")
    @XmlSchemaType(name = "string")
    protected ProtTipoProtocolloType tipoProtocollo;
    @XmlElement(name = "Oggetto")
    protected String oggetto;
    @XmlElementRef(name = "TipologiaDocumentale", type = JAXBElement.class, required = false)
    protected JAXBElement<ProtTipologiaDocumentoType> tipologiaDocumentale;
    @XmlElement(name = "Protocollatore")
    protected OrgOrganigrammaType protocollatore;
    @XmlElement(name = "Acl")
    protected String acl;
    @XmlElement(name = "DatiRicezione")
    protected ProtCriteriaProtocolliType.DatiRicezione datiRicezione;
    @XmlElement(name = "DatiSpedizione")
    protected ProtCriteriaProtocolliType.DatiSpedizione datiSpedizione;
    @XmlElement(name = "Stato")
    protected String stato;
    @XmlElement(name = "IsRiservato", defaultValue = "false")
    protected Boolean isRiservato;
    @XmlElement(name = "IsCompleto", defaultValue = "false")
    protected Boolean isCompleto;
    @XmlElement(name = "IsAnnullato", defaultValue = "false")
    protected Boolean isAnnullato;
    @XmlElement(name = "DatiAssegnazione")
    protected ProtCriteriaProtocolliType.DatiAssegnazione datiAssegnazione;
    @XmlElement(name = "DatiAssegnazioneApplicazione")
    protected ProtCriteriaProtocolliType.DatiAssegnazioneApplicazione datiAssegnazioneApplicazione;
    @XmlElement(name = "RegistroEmergenza")
    protected ProtCriteriaProtocolliType.RegistroEmergenza registroEmergenza;
    @XmlElement(name = "VoceTitolario")
    protected AllCodiceDescrizioneType voceTitolario;
    @XmlElement(name = "SistemaProduttore")
    protected String sistemaProduttore;
    @XmlElement(name = "SistemaAusiliario")
    protected String sistemaAusiliario;
    @XmlElement(name = "TipoDettaglioElenco", required = true)
    @XmlSchemaType(name = "string")
    protected List<ProtTipoDettaglioEstrazioneType> tipoDettaglioElenco;

    /**
     * Recupera il valore della proprietà registro.
     * 
     * @return
     *     possible object is
     *     {@link ProtRegistroProtocolloType }
     *     
     */
    public ProtRegistroProtocolloType getRegistro() {
        return registro;
    }

    /**
     * Imposta il valore della proprietà registro.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtRegistroProtocolloType }
     *     
     */
    public void setRegistro(ProtRegistroProtocolloType value) {
        this.registro = value;
    }

    /**
     * Recupera il valore della proprietà anno.
     * 
     * @return
     *     possible object is
     *     {@link Short }
     *     
     */
    public Short getAnno() {
        return anno;
    }

    /**
     * Imposta il valore della proprietà anno.
     * 
     * @param value
     *     allowed object is
     *     {@link Short }
     *     
     */
    public void setAnno(Short value) {
        this.anno = value;
    }

    /**
     * Recupera il valore della proprietà dataRegistrazione.
     * 
     * @return
     *     possible object is
     *     {@link AllDataRangeType }
     *     
     */
    public AllDataRangeType getDataRegistrazione() {
        return dataRegistrazione;
    }

    /**
     * Imposta il valore della proprietà dataRegistrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link AllDataRangeType }
     *     
     */
    public void setDataRegistrazione(AllDataRangeType value) {
        this.dataRegistrazione = value;
    }

    /**
     * Recupera il valore della proprietà numeroRegistrazione.
     * 
     * @return
     *     possible object is
     *     {@link AllNumeroRangeType }
     *     
     */
    public AllNumeroRangeType getNumeroRegistrazione() {
        return numeroRegistrazione;
    }

    /**
     * Imposta il valore della proprietà numeroRegistrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link AllNumeroRangeType }
     *     
     */
    public void setNumeroRegistrazione(AllNumeroRangeType value) {
        this.numeroRegistrazione = value;
    }

    /**
     * Recupera il valore della proprietà dataAnnullamento.
     * 
     * @return
     *     possible object is
     *     {@link AllDataRangeType }
     *     
     */
    public AllDataRangeType getDataAnnullamento() {
        return dataAnnullamento;
    }

    /**
     * Imposta il valore della proprietà dataAnnullamento.
     * 
     * @param value
     *     allowed object is
     *     {@link AllDataRangeType }
     *     
     */
    public void setDataAnnullamento(AllDataRangeType value) {
        this.dataAnnullamento = value;
    }

    /**
     * Recupera il valore della proprietà tipoProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link ProtTipoProtocolloType }
     *     
     */
    public ProtTipoProtocolloType getTipoProtocollo() {
        return tipoProtocollo;
    }

    /**
     * Imposta il valore della proprietà tipoProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtTipoProtocolloType }
     *     
     */
    public void setTipoProtocollo(ProtTipoProtocolloType value) {
        this.tipoProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà oggetto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggetto() {
        return oggetto;
    }

    /**
     * Imposta il valore della proprietà oggetto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggetto(String value) {
        this.oggetto = value;
    }

    /**
     * Recupera il valore della proprietà tipologiaDocumentale.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ProtTipologiaDocumentoType }{@code >}
     *     
     */
    public JAXBElement<ProtTipologiaDocumentoType> getTipologiaDocumentale() {
        return tipologiaDocumentale;
    }

    /**
     * Imposta il valore della proprietà tipologiaDocumentale.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ProtTipologiaDocumentoType }{@code >}
     *     
     */
    public void setTipologiaDocumentale(JAXBElement<ProtTipologiaDocumentoType> value) {
        this.tipologiaDocumentale = value;
    }

    /**
     * Recupera il valore della proprietà protocollatore.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getProtocollatore() {
        return protocollatore;
    }

    /**
     * Imposta il valore della proprietà protocollatore.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setProtocollatore(OrgOrganigrammaType value) {
        this.protocollatore = value;
    }

    /**
     * Recupera il valore della proprietà acl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcl() {
        return acl;
    }

    /**
     * Imposta il valore della proprietà acl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcl(String value) {
        this.acl = value;
    }

    /**
     * Recupera il valore della proprietà datiRicezione.
     * 
     * @return
     *     possible object is
     *     {@link ProtCriteriaProtocolliType.DatiRicezione }
     *     
     */
    public ProtCriteriaProtocolliType.DatiRicezione getDatiRicezione() {
        return datiRicezione;
    }

    /**
     * Imposta il valore della proprietà datiRicezione.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtCriteriaProtocolliType.DatiRicezione }
     *     
     */
    public void setDatiRicezione(ProtCriteriaProtocolliType.DatiRicezione value) {
        this.datiRicezione = value;
    }

    /**
     * Recupera il valore della proprietà datiSpedizione.
     * 
     * @return
     *     possible object is
     *     {@link ProtCriteriaProtocolliType.DatiSpedizione }
     *     
     */
    public ProtCriteriaProtocolliType.DatiSpedizione getDatiSpedizione() {
        return datiSpedizione;
    }

    /**
     * Imposta il valore della proprietà datiSpedizione.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtCriteriaProtocolliType.DatiSpedizione }
     *     
     */
    public void setDatiSpedizione(ProtCriteriaProtocolliType.DatiSpedizione value) {
        this.datiSpedizione = value;
    }

    /**
     * Recupera il valore della proprietà stato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStato() {
        return stato;
    }

    /**
     * Imposta il valore della proprietà stato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStato(String value) {
        this.stato = value;
    }

    /**
     * Recupera il valore della proprietà isRiservato.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsRiservato() {
        return isRiservato;
    }

    /**
     * Imposta il valore della proprietà isRiservato.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsRiservato(Boolean value) {
        this.isRiservato = value;
    }

    /**
     * Recupera il valore della proprietà isCompleto.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsCompleto() {
        return isCompleto;
    }

    /**
     * Imposta il valore della proprietà isCompleto.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsCompleto(Boolean value) {
        this.isCompleto = value;
    }

    /**
     * Recupera il valore della proprietà isAnnullato.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsAnnullato() {
        return isAnnullato;
    }

    /**
     * Imposta il valore della proprietà isAnnullato.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsAnnullato(Boolean value) {
        this.isAnnullato = value;
    }

    /**
     * Recupera il valore della proprietà datiAssegnazione.
     * 
     * @return
     *     possible object is
     *     {@link ProtCriteriaProtocolliType.DatiAssegnazione }
     *     
     */
    public ProtCriteriaProtocolliType.DatiAssegnazione getDatiAssegnazione() {
        return datiAssegnazione;
    }

    /**
     * Imposta il valore della proprietà datiAssegnazione.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtCriteriaProtocolliType.DatiAssegnazione }
     *     
     */
    public void setDatiAssegnazione(ProtCriteriaProtocolliType.DatiAssegnazione value) {
        this.datiAssegnazione = value;
    }

    /**
     * Recupera il valore della proprietà datiAssegnazioneApplicazione.
     * 
     * @return
     *     possible object is
     *     {@link ProtCriteriaProtocolliType.DatiAssegnazioneApplicazione }
     *     
     */
    public ProtCriteriaProtocolliType.DatiAssegnazioneApplicazione getDatiAssegnazioneApplicazione() {
        return datiAssegnazioneApplicazione;
    }

    /**
     * Imposta il valore della proprietà datiAssegnazioneApplicazione.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtCriteriaProtocolliType.DatiAssegnazioneApplicazione }
     *     
     */
    public void setDatiAssegnazioneApplicazione(ProtCriteriaProtocolliType.DatiAssegnazioneApplicazione value) {
        this.datiAssegnazioneApplicazione = value;
    }

    /**
     * Recupera il valore della proprietà registroEmergenza.
     * 
     * @return
     *     possible object is
     *     {@link ProtCriteriaProtocolliType.RegistroEmergenza }
     *     
     */
    public ProtCriteriaProtocolliType.RegistroEmergenza getRegistroEmergenza() {
        return registroEmergenza;
    }

    /**
     * Imposta il valore della proprietà registroEmergenza.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtCriteriaProtocolliType.RegistroEmergenza }
     *     
     */
    public void setRegistroEmergenza(ProtCriteriaProtocolliType.RegistroEmergenza value) {
        this.registroEmergenza = value;
    }

    /**
     * Recupera il valore della proprietà voceTitolario.
     * 
     * @return
     *     possible object is
     *     {@link AllCodiceDescrizioneType }
     *     
     */
    public AllCodiceDescrizioneType getVoceTitolario() {
        return voceTitolario;
    }

    /**
     * Imposta il valore della proprietà voceTitolario.
     * 
     * @param value
     *     allowed object is
     *     {@link AllCodiceDescrizioneType }
     *     
     */
    public void setVoceTitolario(AllCodiceDescrizioneType value) {
        this.voceTitolario = value;
    }

    /**
     * Recupera il valore della proprietà sistemaProduttore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaProduttore() {
        return sistemaProduttore;
    }

    /**
     * Imposta il valore della proprietà sistemaProduttore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaProduttore(String value) {
        this.sistemaProduttore = value;
    }

    /**
     * Recupera il valore della proprietà sistemaAusiliario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaAusiliario() {
        return sistemaAusiliario;
    }

    /**
     * Imposta il valore della proprietà sistemaAusiliario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaAusiliario(String value) {
        this.sistemaAusiliario = value;
    }

    /**
     * Gets the value of the tipoDettaglioElenco property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tipoDettaglioElenco property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTipoDettaglioElenco().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtTipoDettaglioEstrazioneType }
     * 
     * 
     */
    public List<ProtTipoDettaglioEstrazioneType> getTipoDettaglioElenco() {
        if (tipoDettaglioElenco == null) {
            tipoDettaglioElenco = new ArrayList<ProtTipoDettaglioEstrazioneType>();
        }
        return this.tipoDettaglioElenco;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Assegnatario" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type" minOccurs="0"/>
     *         &lt;element name="Assegnante" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "assegnatario",
        "assegnante"
    })
    public static class DatiAssegnazione
        implements Serializable
    {

        @XmlElement(name = "Assegnatario")
        protected OrgOrganigrammaType assegnatario;
        @XmlElement(name = "Assegnante")
        protected OrgOrganigrammaType assegnante;

        /**
         * Recupera il valore della proprietà assegnatario.
         * 
         * @return
         *     possible object is
         *     {@link OrgOrganigrammaType }
         *     
         */
        public OrgOrganigrammaType getAssegnatario() {
            return assegnatario;
        }

        /**
         * Imposta il valore della proprietà assegnatario.
         * 
         * @param value
         *     allowed object is
         *     {@link OrgOrganigrammaType }
         *     
         */
        public void setAssegnatario(OrgOrganigrammaType value) {
            this.assegnatario = value;
        }

        /**
         * Recupera il valore della proprietà assegnante.
         * 
         * @return
         *     possible object is
         *     {@link OrgOrganigrammaType }
         *     
         */
        public OrgOrganigrammaType getAssegnante() {
            return assegnante;
        }

        /**
         * Imposta il valore della proprietà assegnante.
         * 
         * @param value
         *     allowed object is
         *     {@link OrgOrganigrammaType }
         *     
         */
        public void setAssegnante(OrgOrganigrammaType value) {
            this.assegnante = value;
        }

    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Assegnatario" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type" minOccurs="0"/>
     *         &lt;element name="Assegnante" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type" minOccurs="0"/>
     *         &lt;element name="Applicazione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Stato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "assegnatario",
        "assegnante",
        "applicazione",
        "stato"
    })
    public static class DatiAssegnazioneApplicazione
        implements Serializable
    {

        @XmlElement(name = "Assegnatario")
        protected OrgOrganigrammaType assegnatario;
        @XmlElement(name = "Assegnante")
        protected OrgOrganigrammaType assegnante;
        @XmlElement(name = "Applicazione")
        protected String applicazione;
        @XmlElement(name = "Stato")
        protected String stato;

        /**
         * Recupera il valore della proprietà assegnatario.
         * 
         * @return
         *     possible object is
         *     {@link OrgOrganigrammaType }
         *     
         */
        public OrgOrganigrammaType getAssegnatario() {
            return assegnatario;
        }

        /**
         * Imposta il valore della proprietà assegnatario.
         * 
         * @param value
         *     allowed object is
         *     {@link OrgOrganigrammaType }
         *     
         */
        public void setAssegnatario(OrgOrganigrammaType value) {
            this.assegnatario = value;
        }

        /**
         * Recupera il valore della proprietà assegnante.
         * 
         * @return
         *     possible object is
         *     {@link OrgOrganigrammaType }
         *     
         */
        public OrgOrganigrammaType getAssegnante() {
            return assegnante;
        }

        /**
         * Imposta il valore della proprietà assegnante.
         * 
         * @param value
         *     allowed object is
         *     {@link OrgOrganigrammaType }
         *     
         */
        public void setAssegnante(OrgOrganigrammaType value) {
            this.assegnante = value;
        }

        /**
         * Recupera il valore della proprietà applicazione.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getApplicazione() {
            return applicazione;
        }

        /**
         * Imposta il valore della proprietà applicazione.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setApplicazione(String value) {
            this.applicazione = value;
        }

        /**
         * Recupera il valore della proprietà stato.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStato() {
            return stato;
        }

        /**
         * Imposta il valore della proprietà stato.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStato(String value) {
            this.stato = value;
        }

    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Mittente" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="DataRicezione" type="{http://mef.gov.it.v1.npsTypes}all_dataRange_type" minOccurs="0"/>
     *         &lt;element name="NumeroRegistrazioneMittente" type="{http://mef.gov.it.v1.npsTypes}all_numeroRange_type" minOccurs="0"/>
     *         &lt;element name="DataProtocolloMittente" type="{http://mef.gov.it.v1.npsTypes}all_dataRange_type" minOccurs="0"/>
     *         &lt;element name="MezzoRicezione" type="{http://mef.gov.it.v1.npsTypes}all_codiceDescrizione_type" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mittente",
        "dataRicezione",
        "numeroRegistrazioneMittente",
        "dataProtocolloMittente",
        "mezzoRicezione"
    })
    public static class DatiRicezione
        implements Serializable
    {

        @XmlElement(name = "Mittente", required = true)
        protected String mittente;
        @XmlElement(name = "DataRicezione")
        protected AllDataRangeType dataRicezione;
        @XmlElement(name = "NumeroRegistrazioneMittente")
        protected AllNumeroRangeType numeroRegistrazioneMittente;
        @XmlElement(name = "DataProtocolloMittente")
        protected AllDataRangeType dataProtocolloMittente;
        @XmlElement(name = "MezzoRicezione")
        protected AllCodiceDescrizioneType mezzoRicezione;

        /**
         * Recupera il valore della proprietà mittente.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMittente() {
            return mittente;
        }

        /**
         * Imposta il valore della proprietà mittente.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMittente(String value) {
            this.mittente = value;
        }

        /**
         * Recupera il valore della proprietà dataRicezione.
         * 
         * @return
         *     possible object is
         *     {@link AllDataRangeType }
         *     
         */
        public AllDataRangeType getDataRicezione() {
            return dataRicezione;
        }

        /**
         * Imposta il valore della proprietà dataRicezione.
         * 
         * @param value
         *     allowed object is
         *     {@link AllDataRangeType }
         *     
         */
        public void setDataRicezione(AllDataRangeType value) {
            this.dataRicezione = value;
        }

        /**
         * Recupera il valore della proprietà numeroRegistrazioneMittente.
         * 
         * @return
         *     possible object is
         *     {@link AllNumeroRangeType }
         *     
         */
        public AllNumeroRangeType getNumeroRegistrazioneMittente() {
            return numeroRegistrazioneMittente;
        }

        /**
         * Imposta il valore della proprietà numeroRegistrazioneMittente.
         * 
         * @param value
         *     allowed object is
         *     {@link AllNumeroRangeType }
         *     
         */
        public void setNumeroRegistrazioneMittente(AllNumeroRangeType value) {
            this.numeroRegistrazioneMittente = value;
        }

        /**
         * Recupera il valore della proprietà dataProtocolloMittente.
         * 
         * @return
         *     possible object is
         *     {@link AllDataRangeType }
         *     
         */
        public AllDataRangeType getDataProtocolloMittente() {
            return dataProtocolloMittente;
        }

        /**
         * Imposta il valore della proprietà dataProtocolloMittente.
         * 
         * @param value
         *     allowed object is
         *     {@link AllDataRangeType }
         *     
         */
        public void setDataProtocolloMittente(AllDataRangeType value) {
            this.dataProtocolloMittente = value;
        }

        /**
         * Recupera il valore della proprietà mezzoRicezione.
         * 
         * @return
         *     possible object is
         *     {@link AllCodiceDescrizioneType }
         *     
         */
        public AllCodiceDescrizioneType getMezzoRicezione() {
            return mezzoRicezione;
        }

        /**
         * Imposta il valore della proprietà mezzoRicezione.
         * 
         * @param value
         *     allowed object is
         *     {@link AllCodiceDescrizioneType }
         *     
         */
        public void setMezzoRicezione(AllCodiceDescrizioneType value) {
            this.mezzoRicezione = value;
        }

    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Destinatario" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="DataSpedizione" type="{http://mef.gov.it.v1.npsTypes}all_dataRange_type" minOccurs="0"/>
     *         &lt;element name="StatoSpedizione" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="DA_SPEDIRE"/>
     *               &lt;enumeration value="IN_SPEDIZIONE"/>
     *               &lt;enumeration value="SPEDITO"/>
     *               &lt;enumeration value="ERRORE"/>
     *               &lt;enumeration value="RICEVUTA_ACCETTAZIONE"/>
     *               &lt;enumeration value="RICEVUTA_CONSEGNA"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="IndirizzoEmail" type="{http://mef.gov.it.v1.npsTypes}email_indirizzoEmail_type" minOccurs="0"/>
     *         &lt;element name="MezzoSpedizione" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;choice>
     *                   &lt;element name="Elettronico">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;choice>
     *                             &lt;element name="CooperazioneApplicativa" type="{http://www.w3.org/2001/XMLSchema}anyURI"/>
     *                             &lt;element name="PostaElettronica" type="{http://mef.gov.it.v1.npsTypes}org_casellaEmail_type"/>
     *                           &lt;/choice>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="Cartaceo" type="{http://mef.gov.it.v1.npsTypes}all_codiceDescrizione_type"/>
     *                 &lt;/choice>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "destinatario",
        "dataSpedizione",
        "statoSpedizione",
        "indirizzoEmail",
        "mezzoSpedizione"
    })
    public static class DatiSpedizione
        implements Serializable
    {

        @XmlElement(name = "Destinatario", required = true)
        protected String destinatario;
        @XmlElement(name = "DataSpedizione")
        protected AllDataRangeType dataSpedizione;
        @XmlElement(name = "StatoSpedizione")
        protected String statoSpedizione;
        @XmlElement(name = "IndirizzoEmail")
        protected EmailIndirizzoEmailType indirizzoEmail;
        @XmlElement(name = "MezzoSpedizione")
        protected ProtCriteriaProtocolliType.DatiSpedizione.MezzoSpedizione mezzoSpedizione;

        /**
         * Recupera il valore della proprietà destinatario.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDestinatario() {
            return destinatario;
        }

        /**
         * Imposta il valore della proprietà destinatario.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDestinatario(String value) {
            this.destinatario = value;
        }

        /**
         * Recupera il valore della proprietà dataSpedizione.
         * 
         * @return
         *     possible object is
         *     {@link AllDataRangeType }
         *     
         */
        public AllDataRangeType getDataSpedizione() {
            return dataSpedizione;
        }

        /**
         * Imposta il valore della proprietà dataSpedizione.
         * 
         * @param value
         *     allowed object is
         *     {@link AllDataRangeType }
         *     
         */
        public void setDataSpedizione(AllDataRangeType value) {
            this.dataSpedizione = value;
        }

        /**
         * Recupera il valore della proprietà statoSpedizione.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatoSpedizione() {
            return statoSpedizione;
        }

        /**
         * Imposta il valore della proprietà statoSpedizione.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatoSpedizione(String value) {
            this.statoSpedizione = value;
        }

        /**
         * Recupera il valore della proprietà indirizzoEmail.
         * 
         * @return
         *     possible object is
         *     {@link EmailIndirizzoEmailType }
         *     
         */
        public EmailIndirizzoEmailType getIndirizzoEmail() {
            return indirizzoEmail;
        }

        /**
         * Imposta il valore della proprietà indirizzoEmail.
         * 
         * @param value
         *     allowed object is
         *     {@link EmailIndirizzoEmailType }
         *     
         */
        public void setIndirizzoEmail(EmailIndirizzoEmailType value) {
            this.indirizzoEmail = value;
        }

        /**
         * Recupera il valore della proprietà mezzoSpedizione.
         * 
         * @return
         *     possible object is
         *     {@link ProtCriteriaProtocolliType.DatiSpedizione.MezzoSpedizione }
         *     
         */
        public ProtCriteriaProtocolliType.DatiSpedizione.MezzoSpedizione getMezzoSpedizione() {
            return mezzoSpedizione;
        }

        /**
         * Imposta il valore della proprietà mezzoSpedizione.
         * 
         * @param value
         *     allowed object is
         *     {@link ProtCriteriaProtocolliType.DatiSpedizione.MezzoSpedizione }
         *     
         */
        public void setMezzoSpedizione(ProtCriteriaProtocolliType.DatiSpedizione.MezzoSpedizione value) {
            this.mezzoSpedizione = value;
        }


        /**
         * <p>Classe Java per anonymous complex type.
         * 
         * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;choice>
         *         &lt;element name="Elettronico">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;choice>
         *                   &lt;element name="CooperazioneApplicativa" type="{http://www.w3.org/2001/XMLSchema}anyURI"/>
         *                   &lt;element name="PostaElettronica" type="{http://mef.gov.it.v1.npsTypes}org_casellaEmail_type"/>
         *                 &lt;/choice>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="Cartaceo" type="{http://mef.gov.it.v1.npsTypes}all_codiceDescrizione_type"/>
         *       &lt;/choice>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "elettronico",
            "cartaceo"
        })
        public static class MezzoSpedizione
            implements Serializable
        {

            @XmlElement(name = "Elettronico")
            protected ProtCriteriaProtocolliType.DatiSpedizione.MezzoSpedizione.Elettronico elettronico;
            @XmlElement(name = "Cartaceo")
            protected AllCodiceDescrizioneType cartaceo;

            /**
             * Recupera il valore della proprietà elettronico.
             * 
             * @return
             *     possible object is
             *     {@link ProtCriteriaProtocolliType.DatiSpedizione.MezzoSpedizione.Elettronico }
             *     
             */
            public ProtCriteriaProtocolliType.DatiSpedizione.MezzoSpedizione.Elettronico getElettronico() {
                return elettronico;
            }

            /**
             * Imposta il valore della proprietà elettronico.
             * 
             * @param value
             *     allowed object is
             *     {@link ProtCriteriaProtocolliType.DatiSpedizione.MezzoSpedizione.Elettronico }
             *     
             */
            public void setElettronico(ProtCriteriaProtocolliType.DatiSpedizione.MezzoSpedizione.Elettronico value) {
                this.elettronico = value;
            }

            /**
             * Recupera il valore della proprietà cartaceo.
             * 
             * @return
             *     possible object is
             *     {@link AllCodiceDescrizioneType }
             *     
             */
            public AllCodiceDescrizioneType getCartaceo() {
                return cartaceo;
            }

            /**
             * Imposta il valore della proprietà cartaceo.
             * 
             * @param value
             *     allowed object is
             *     {@link AllCodiceDescrizioneType }
             *     
             */
            public void setCartaceo(AllCodiceDescrizioneType value) {
                this.cartaceo = value;
            }


            /**
             * <p>Classe Java per anonymous complex type.
             * 
             * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;choice>
             *         &lt;element name="CooperazioneApplicativa" type="{http://www.w3.org/2001/XMLSchema}anyURI"/>
             *         &lt;element name="PostaElettronica" type="{http://mef.gov.it.v1.npsTypes}org_casellaEmail_type"/>
             *       &lt;/choice>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "cooperazioneApplicativa",
                "postaElettronica"
            })
            public static class Elettronico
                implements Serializable
            {

                @XmlElement(name = "CooperazioneApplicativa")
                @XmlSchemaType(name = "anyURI")
                protected String cooperazioneApplicativa;
                @XmlElement(name = "PostaElettronica")
                protected OrgCasellaEmailType postaElettronica;

                /**
                 * Recupera il valore della proprietà cooperazioneApplicativa.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getCooperazioneApplicativa() {
                    return cooperazioneApplicativa;
                }

                /**
                 * Imposta il valore della proprietà cooperazioneApplicativa.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setCooperazioneApplicativa(String value) {
                    this.cooperazioneApplicativa = value;
                }

                /**
                 * Recupera il valore della proprietà postaElettronica.
                 * 
                 * @return
                 *     possible object is
                 *     {@link OrgCasellaEmailType }
                 *     
                 */
                public OrgCasellaEmailType getPostaElettronica() {
                    return postaElettronica;
                }

                /**
                 * Imposta il valore della proprietà postaElettronica.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link OrgCasellaEmailType }
                 *     
                 */
                public void setPostaElettronica(OrgCasellaEmailType value) {
                    this.postaElettronica = value;
                }

            }

        }

    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DataRegistrazione" type="{http://mef.gov.it.v1.npsTypes}all_dataRange_type"/>
     *         &lt;element name="NumeroRegistrazione" type="{http://mef.gov.it.v1.npsTypes}all_numeroRange_type"/>
     *         &lt;element name="TipoProtocollo" type="{http://mef.gov.it.v1.npsTypes}prot_tipoProtocollo_type" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dataRegistrazione",
        "numeroRegistrazione",
        "tipoProtocollo"
    })
    public static class RegistroEmergenza
        implements Serializable
    {

        @XmlElement(name = "DataRegistrazione", required = true)
        protected AllDataRangeType dataRegistrazione;
        @XmlElement(name = "NumeroRegistrazione", required = true)
        protected AllNumeroRangeType numeroRegistrazione;
        @XmlElement(name = "TipoProtocollo")
        @XmlSchemaType(name = "string")
        protected ProtTipoProtocolloType tipoProtocollo;

        /**
         * Recupera il valore della proprietà dataRegistrazione.
         * 
         * @return
         *     possible object is
         *     {@link AllDataRangeType }
         *     
         */
        public AllDataRangeType getDataRegistrazione() {
            return dataRegistrazione;
        }

        /**
         * Imposta il valore della proprietà dataRegistrazione.
         * 
         * @param value
         *     allowed object is
         *     {@link AllDataRangeType }
         *     
         */
        public void setDataRegistrazione(AllDataRangeType value) {
            this.dataRegistrazione = value;
        }

        /**
         * Recupera il valore della proprietà numeroRegistrazione.
         * 
         * @return
         *     possible object is
         *     {@link AllNumeroRangeType }
         *     
         */
        public AllNumeroRangeType getNumeroRegistrazione() {
            return numeroRegistrazione;
        }

        /**
         * Imposta il valore della proprietà numeroRegistrazione.
         * 
         * @param value
         *     allowed object is
         *     {@link AllNumeroRangeType }
         *     
         */
        public void setNumeroRegistrazione(AllNumeroRangeType value) {
            this.numeroRegistrazione = value;
        }

        /**
         * Recupera il valore della proprietà tipoProtocollo.
         * 
         * @return
         *     possible object is
         *     {@link ProtTipoProtocolloType }
         *     
         */
        public ProtTipoProtocolloType getTipoProtocollo() {
            return tipoProtocollo;
        }

        /**
         * Imposta il valore della proprietà tipoProtocollo.
         * 
         * @param value
         *     allowed object is
         *     {@link ProtTipoProtocolloType }
         *     
         */
        public void setTipoProtocollo(ProtTipoProtocolloType value) {
            this.tipoProtocollo = value;
        }

    }

}
