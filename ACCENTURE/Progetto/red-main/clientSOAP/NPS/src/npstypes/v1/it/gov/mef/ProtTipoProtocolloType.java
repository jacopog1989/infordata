
package npstypes.v1.it.gov.mef;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per prot_tipoProtocollo_type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="prot_tipoProtocollo_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ENTRATA"/>
 *     &lt;enumeration value="USCITA"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "prot_tipoProtocollo_type")
@XmlEnum
public enum ProtTipoProtocolloType {

    ENTRATA,
    USCITA;

    public String value() {
        return name();
    }

    public static ProtTipoProtocolloType fromValue(String v) {
        return valueOf(v);
    }

}
