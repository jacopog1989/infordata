
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import npsmessages.v1.it.gov.mef.RichiestaCreateRegistroAusiliarioType;


/**
 * Definisce il tipo di dato relativo al registro ausiliario di NPS
 * 
 * <p>Classe Java per reg_registroAusiliario_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="reg_registroAusiliario_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdRegistroAusiliario" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="Amministrazione" type="{http://mef.gov.it.v1.npsTypes}org_amministrazione_type"/>
 *         &lt;element name="Aoo" type="{http://mef.gov.it.v1.npsTypes}org_aoo_type"/>
 *         &lt;element name="CodiceRegistro" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TipoNumerazione" type="{http://mef.gov.it.v1.npsTypes}all_tipoNumerazioneRegistroAusiliario_type"/>
 *         &lt;element name="TipoRegistro" type="{http://mef.gov.it.v1.npsTypes}all_tipoRegistroAusiliario_type"/>
 *         &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Anno" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Numero" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DataRegistro" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="Stato" type="{http://mef.gov.it.v1.npsTypes}all_statoRegistroAusiliario_type"/>
 *         &lt;element name="IsAssociatoRegistroUfficiale" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsLocked" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsRegistroDocumentale" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsAutomatico" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsAttivo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="SistemaProduttore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ApplicazioniAssociate" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TipologieDocumentaliAssociate" type="{http://mef.gov.it.v1.npsTypes}all_tipoDocumento_type" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reg_registroAusiliario_type", propOrder = {
    "idRegistroAusiliario",
    "amministrazione",
    "aoo",
    "codiceRegistro",
    "tipoNumerazione",
    "tipoRegistro",
    "descrizione",
    "anno",
    "numero",
    "dataRegistro",
    "stato",
    "isAssociatoRegistroUfficiale",
    "isLocked",
    "isRegistroDocumentale",
    "isAutomatico",
    "isAttivo",
    "sistemaProduttore",
    "applicazioniAssociate",
    "tipologieDocumentaliAssociate"
})
@XmlSeeAlso({
    RichiestaCreateRegistroAusiliarioType.class
})
public class RegRegistroAusiliarioType
    implements Serializable
{

    @XmlElement(name = "IdRegistroAusiliario", required = true, nillable = true)
    protected String idRegistroAusiliario;
    @XmlElement(name = "Amministrazione", required = true)
    protected OrgAmministrazioneType amministrazione;
    @XmlElement(name = "Aoo", required = true)
    protected OrgAooType aoo;
    @XmlElement(name = "CodiceRegistro", required = true)
    protected String codiceRegistro;
    @XmlElement(name = "TipoNumerazione", required = true)
    @XmlSchemaType(name = "string")
    protected AllTipoNumerazioneRegistroAusiliarioType tipoNumerazione;
    @XmlElement(name = "TipoRegistro", required = true)
    @XmlSchemaType(name = "string")
    protected AllTipoRegistroAusiliarioType tipoRegistro;
    @XmlElement(name = "Descrizione", required = true)
    protected String descrizione;
    @XmlElement(name = "Anno")
    protected int anno;
    @XmlElement(name = "Numero")
    protected Integer numero;
    @XmlElement(name = "DataRegistro", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataRegistro;
    @XmlElement(name = "Stato", required = true)
    @XmlSchemaType(name = "string")
    protected AllStatoRegistroAusiliarioType stato;
    @XmlElement(name = "IsAssociatoRegistroUfficiale")
    protected boolean isAssociatoRegistroUfficiale;
    @XmlElement(name = "IsLocked")
    protected boolean isLocked;
    @XmlElement(name = "IsRegistroDocumentale")
    protected boolean isRegistroDocumentale;
    @XmlElement(name = "IsAutomatico")
    protected boolean isAutomatico;
    @XmlElement(name = "IsAttivo")
    protected boolean isAttivo;
    @XmlElement(name = "SistemaProduttore", required = true)
    protected String sistemaProduttore;
    @XmlElement(name = "ApplicazioniAssociate")
    protected List<String> applicazioniAssociate;
    @XmlElement(name = "TipologieDocumentaliAssociate")
    protected List<AllTipoDocumentoType> tipologieDocumentaliAssociate;

    /**
     * Recupera il valore della proprietà idRegistroAusiliario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRegistroAusiliario() {
        return idRegistroAusiliario;
    }

    /**
     * Imposta il valore della proprietà idRegistroAusiliario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRegistroAusiliario(String value) {
        this.idRegistroAusiliario = value;
    }

    /**
     * Recupera il valore della proprietà amministrazione.
     * 
     * @return
     *     possible object is
     *     {@link OrgAmministrazioneType }
     *     
     */
    public OrgAmministrazioneType getAmministrazione() {
        return amministrazione;
    }

    /**
     * Imposta il valore della proprietà amministrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgAmministrazioneType }
     *     
     */
    public void setAmministrazione(OrgAmministrazioneType value) {
        this.amministrazione = value;
    }

    /**
     * Recupera il valore della proprietà aoo.
     * 
     * @return
     *     possible object is
     *     {@link OrgAooType }
     *     
     */
    public OrgAooType getAoo() {
        return aoo;
    }

    /**
     * Imposta il valore della proprietà aoo.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgAooType }
     *     
     */
    public void setAoo(OrgAooType value) {
        this.aoo = value;
    }

    /**
     * Recupera il valore della proprietà codiceRegistro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceRegistro() {
        return codiceRegistro;
    }

    /**
     * Imposta il valore della proprietà codiceRegistro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceRegistro(String value) {
        this.codiceRegistro = value;
    }

    /**
     * Recupera il valore della proprietà tipoNumerazione.
     * 
     * @return
     *     possible object is
     *     {@link AllTipoNumerazioneRegistroAusiliarioType }
     *     
     */
    public AllTipoNumerazioneRegistroAusiliarioType getTipoNumerazione() {
        return tipoNumerazione;
    }

    /**
     * Imposta il valore della proprietà tipoNumerazione.
     * 
     * @param value
     *     allowed object is
     *     {@link AllTipoNumerazioneRegistroAusiliarioType }
     *     
     */
    public void setTipoNumerazione(AllTipoNumerazioneRegistroAusiliarioType value) {
        this.tipoNumerazione = value;
    }

    /**
     * Recupera il valore della proprietà tipoRegistro.
     * 
     * @return
     *     possible object is
     *     {@link AllTipoRegistroAusiliarioType }
     *     
     */
    public AllTipoRegistroAusiliarioType getTipoRegistro() {
        return tipoRegistro;
    }

    /**
     * Imposta il valore della proprietà tipoRegistro.
     * 
     * @param value
     *     allowed object is
     *     {@link AllTipoRegistroAusiliarioType }
     *     
     */
    public void setTipoRegistro(AllTipoRegistroAusiliarioType value) {
        this.tipoRegistro = value;
    }

    /**
     * Recupera il valore della proprietà descrizione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Imposta il valore della proprietà descrizione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Recupera il valore della proprietà anno.
     * 
     */
    public int getAnno() {
        return anno;
    }

    /**
     * Imposta il valore della proprietà anno.
     * 
     */
    public void setAnno(int value) {
        this.anno = value;
    }

    /**
     * Recupera il valore della proprietà numero.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumero() {
        return numero;
    }

    /**
     * Imposta il valore della proprietà numero.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumero(Integer value) {
        this.numero = value;
    }

    /**
     * Recupera il valore della proprietà dataRegistro.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataRegistro() {
        return dataRegistro;
    }

    /**
     * Imposta il valore della proprietà dataRegistro.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataRegistro(XMLGregorianCalendar value) {
        this.dataRegistro = value;
    }

    /**
     * Recupera il valore della proprietà stato.
     * 
     * @return
     *     possible object is
     *     {@link AllStatoRegistroAusiliarioType }
     *     
     */
    public AllStatoRegistroAusiliarioType getStato() {
        return stato;
    }

    /**
     * Imposta il valore della proprietà stato.
     * 
     * @param value
     *     allowed object is
     *     {@link AllStatoRegistroAusiliarioType }
     *     
     */
    public void setStato(AllStatoRegistroAusiliarioType value) {
        this.stato = value;
    }

    /**
     * Recupera il valore della proprietà isAssociatoRegistroUfficiale.
     * 
     */
    public boolean isIsAssociatoRegistroUfficiale() {
        return isAssociatoRegistroUfficiale;
    }

    /**
     * Imposta il valore della proprietà isAssociatoRegistroUfficiale.
     * 
     */
    public void setIsAssociatoRegistroUfficiale(boolean value) {
        this.isAssociatoRegistroUfficiale = value;
    }

    /**
     * Recupera il valore della proprietà isLocked.
     * 
     */
    public boolean isIsLocked() {
        return isLocked;
    }

    /**
     * Imposta il valore della proprietà isLocked.
     * 
     */
    public void setIsLocked(boolean value) {
        this.isLocked = value;
    }

    /**
     * Recupera il valore della proprietà isRegistroDocumentale.
     * 
     */
    public boolean isIsRegistroDocumentale() {
        return isRegistroDocumentale;
    }

    /**
     * Imposta il valore della proprietà isRegistroDocumentale.
     * 
     */
    public void setIsRegistroDocumentale(boolean value) {
        this.isRegistroDocumentale = value;
    }

    /**
     * Recupera il valore della proprietà isAutomatico.
     * 
     */
    public boolean isIsAutomatico() {
        return isAutomatico;
    }

    /**
     * Imposta il valore della proprietà isAutomatico.
     * 
     */
    public void setIsAutomatico(boolean value) {
        this.isAutomatico = value;
    }

    /**
     * Recupera il valore della proprietà isAttivo.
     * 
     */
    public boolean isIsAttivo() {
        return isAttivo;
    }

    /**
     * Imposta il valore della proprietà isAttivo.
     * 
     */
    public void setIsAttivo(boolean value) {
        this.isAttivo = value;
    }

    /**
     * Recupera il valore della proprietà sistemaProduttore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaProduttore() {
        return sistemaProduttore;
    }

    /**
     * Imposta il valore della proprietà sistemaProduttore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaProduttore(String value) {
        this.sistemaProduttore = value;
    }

    /**
     * Gets the value of the applicazioniAssociate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the applicazioniAssociate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getApplicazioniAssociate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getApplicazioniAssociate() {
        if (applicazioniAssociate == null) {
            applicazioniAssociate = new ArrayList<String>();
        }
        return this.applicazioniAssociate;
    }

    /**
     * Gets the value of the tipologieDocumentaliAssociate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tipologieDocumentaliAssociate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTipologieDocumentaliAssociate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AllTipoDocumentoType }
     * 
     * 
     */
    public List<AllTipoDocumentoType> getTipologieDocumentaliAssociate() {
        if (tipologieDocumentaliAssociate == null) {
            tipologieDocumentaliAssociate = new ArrayList<AllTipoDocumentoType>();
        }
        return this.tipologieDocumentaliAssociate;
    }

}
