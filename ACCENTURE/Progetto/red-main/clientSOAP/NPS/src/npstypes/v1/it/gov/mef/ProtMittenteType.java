
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce i tipo di dato del Mittente del protocollo
 * 
 * <p>Classe Java per prot_mittente_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_mittente_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}prot_corrispondente_type">
 *       &lt;sequence>
 *         &lt;element name="Ricezione" type="{http://mef.gov.it.v1.npsTypes}prot_ricezione_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_mittente_type", propOrder = {
    "ricezione"
})
public class ProtMittenteType
    extends ProtCorrispondenteType
    implements Serializable
{

    @XmlElement(name = "Ricezione")
    protected ProtRicezioneType ricezione;

    /**
     * Recupera il valore della proprietà ricezione.
     * 
     * @return
     *     possible object is
     *     {@link ProtRicezioneType }
     *     
     */
    public ProtRicezioneType getRicezione() {
        return ricezione;
    }

    /**
     * Imposta il valore della proprietà ricezione.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtRicezioneType }
     *     
     */
    public void setRicezione(ProtRicezioneType value) {
        this.ricezione = value;
    }

}
