
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * Definisce i metadati del singolo destinatario da utilizzare nei flussi
 * 
 * <p>Classe Java per wkf_destinatarioFlusso_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_destinatarioFlusso_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="PersonaFisica" type="{http://mef.gov.it.v1.npsTypes}ana_personaFisica_type"/>
 *           &lt;element name="PersonaGiuridica" type="{http://mef.gov.it.v1.npsTypes}ana_personaGiuridica_type"/>
 *           &lt;element name="Ente" type="{http://mef.gov.it.v1.npsTypes}ana_enteEsterno_type"/>
 *         &lt;/choice>
 *         &lt;element name="IndirizzoTelematico" type="{http://mef.gov.it.v1.npsTypes}ana_indirizzoTelematico_type"/>
 *       &lt;/sequence>
 *       &lt;attribute name="PerConoscenza" default="no">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *             &lt;enumeration value="si"/>
 *             &lt;enumeration value="no"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_destinatarioFlusso_type", propOrder = {
    "personaFisica",
    "personaGiuridica",
    "ente",
    "indirizzoTelematico"
})
public class WkfDestinatarioFlussoType
    implements Serializable
{

    @XmlElement(name = "PersonaFisica")
    protected AnaPersonaFisicaType personaFisica;
    @XmlElement(name = "PersonaGiuridica")
    protected AnaPersonaGiuridicaType personaGiuridica;
    @XmlElement(name = "Ente")
    protected AnaEnteEsternoType ente;
    @XmlElement(name = "IndirizzoTelematico", required = true)
    protected AnaIndirizzoTelematicoType indirizzoTelematico;
    @XmlAttribute(name = "PerConoscenza")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String perConoscenza;

    /**
     * Recupera il valore della proprietà personaFisica.
     * 
     * @return
     *     possible object is
     *     {@link AnaPersonaFisicaType }
     *     
     */
    public AnaPersonaFisicaType getPersonaFisica() {
        return personaFisica;
    }

    /**
     * Imposta il valore della proprietà personaFisica.
     * 
     * @param value
     *     allowed object is
     *     {@link AnaPersonaFisicaType }
     *     
     */
    public void setPersonaFisica(AnaPersonaFisicaType value) {
        this.personaFisica = value;
    }

    /**
     * Recupera il valore della proprietà personaGiuridica.
     * 
     * @return
     *     possible object is
     *     {@link AnaPersonaGiuridicaType }
     *     
     */
    public AnaPersonaGiuridicaType getPersonaGiuridica() {
        return personaGiuridica;
    }

    /**
     * Imposta il valore della proprietà personaGiuridica.
     * 
     * @param value
     *     allowed object is
     *     {@link AnaPersonaGiuridicaType }
     *     
     */
    public void setPersonaGiuridica(AnaPersonaGiuridicaType value) {
        this.personaGiuridica = value;
    }

    /**
     * Recupera il valore della proprietà ente.
     * 
     * @return
     *     possible object is
     *     {@link AnaEnteEsternoType }
     *     
     */
    public AnaEnteEsternoType getEnte() {
        return ente;
    }

    /**
     * Imposta il valore della proprietà ente.
     * 
     * @param value
     *     allowed object is
     *     {@link AnaEnteEsternoType }
     *     
     */
    public void setEnte(AnaEnteEsternoType value) {
        this.ente = value;
    }

    /**
     * Recupera il valore della proprietà indirizzoTelematico.
     * 
     * @return
     *     possible object is
     *     {@link AnaIndirizzoTelematicoType }
     *     
     */
    public AnaIndirizzoTelematicoType getIndirizzoTelematico() {
        return indirizzoTelematico;
    }

    /**
     * Imposta il valore della proprietà indirizzoTelematico.
     * 
     * @param value
     *     allowed object is
     *     {@link AnaIndirizzoTelematicoType }
     *     
     */
    public void setIndirizzoTelematico(AnaIndirizzoTelematicoType value) {
        this.indirizzoTelematico = value;
    }

    /**
     * Recupera il valore della proprietà perConoscenza.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerConoscenza() {
        if (perConoscenza == null) {
            return "no";
        } else {
            return perConoscenza;
        }
    }

    /**
     * Imposta il valore della proprietà perConoscenza.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerConoscenza(String value) {
        this.perConoscenza = value;
    }

}
