
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce i dati di protocollo in ingresso da utilizzare nelle risposte
 * 
 * <p>Classe Java per prot_protocolloEntrataResponse_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_protocolloEntrataResponse_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}prot_protocolloBaseResponse_type">
 *       &lt;sequence>
 *         &lt;element name="Assegnatari" type="{http://mef.gov.it.v1.npsTypes}prot_assegnatario_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="RicevuteInteroperabilita" type="{http://mef.gov.it.v1.npsTypes}prot_ricevuteSegnatura_type" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_protocolloEntrataResponse_type", propOrder = {
    "assegnatari",
    "ricevuteInteroperabilita"
})
public class ProtProtocolloEntrataResponseType
    extends ProtProtocolloBaseResponseType
    implements Serializable
{

    @XmlElement(name = "Assegnatari")
    protected List<ProtAssegnatarioType> assegnatari;
    @XmlElement(name = "RicevuteInteroperabilita")
    protected List<ProtRicevuteSegnaturaType> ricevuteInteroperabilita;

    /**
     * Gets the value of the assegnatari property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the assegnatari property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssegnatari().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtAssegnatarioType }
     * 
     * 
     */
    public List<ProtAssegnatarioType> getAssegnatari() {
        if (assegnatari == null) {
            assegnatari = new ArrayList<ProtAssegnatarioType>();
        }
        return this.assegnatari;
    }

    /**
     * Gets the value of the ricevuteInteroperabilita property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ricevuteInteroperabilita property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRicevuteInteroperabilita().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtRicevuteSegnaturaType }
     * 
     * 
     */
    public List<ProtRicevuteSegnaturaType> getRicevuteInteroperabilita() {
        if (ricevuteInteroperabilita == null) {
            ricevuteInteroperabilita = new ArrayList<ProtRicevuteSegnaturaType>();
        }
        return this.ricevuteInteroperabilita;
    }

}
