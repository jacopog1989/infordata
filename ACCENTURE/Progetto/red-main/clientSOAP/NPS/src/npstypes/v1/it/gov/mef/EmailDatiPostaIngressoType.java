
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import it.gov.digitpa.protocollo.Segnatura;
import postacerttypes.v1.it.gov.mef.PostacertType;


/**
 * Dati del messaggio di posta
 * 
 * <p>Classe Java per email_datiPostaIngresso_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="email_datiPostaIngresso_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdMessaggio" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="IdDocumento" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="Messaggio" type="{http://mef.gov.it.v1.npsTypes}email_message_type"/>
 *         &lt;element name="DatiCert" type="{http://mef.gov.it.v1.postacertTypes/}postacertType" minOccurs="0"/>
 *         &lt;element name="ChiaveFascicolo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DatiSegnatura" type="{http://www.digitPa.gov.it/protocollo/}Segnatura" minOccurs="0"/>
 *         &lt;element name="ValidazioneMessaggio" type="{http://mef.gov.it.v1.npsTypes}prot_validazioneMessaggio_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "email_datiPostaIngresso_type", propOrder = {
    "idMessaggio",
    "idDocumento",
    "messaggio",
    "datiCert",
    "chiaveFascicolo",
    "datiSegnatura",
    "validazioneMessaggio"
})
public class EmailDatiPostaIngressoType
    implements Serializable
{

    @XmlElement(name = "IdMessaggio", required = true)
    protected String idMessaggio;
    @XmlElement(name = "IdDocumento", required = true)
    protected String idDocumento;
    @XmlElement(name = "Messaggio", required = true)
    protected EmailMessageType messaggio;
    @XmlElement(name = "DatiCert")
    protected PostacertType datiCert;
    @XmlElement(name = "ChiaveFascicolo")
    protected String chiaveFascicolo;
    @XmlElement(name = "DatiSegnatura")
    protected Segnatura datiSegnatura;
    @XmlElement(name = "ValidazioneMessaggio")
    protected ProtValidazioneMessaggioType validazioneMessaggio;

    /**
     * Recupera il valore della proprietà idMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdMessaggio() {
        return idMessaggio;
    }

    /**
     * Imposta il valore della proprietà idMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdMessaggio(String value) {
        this.idMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà idDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumento() {
        return idDocumento;
    }

    /**
     * Imposta il valore della proprietà idDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumento(String value) {
        this.idDocumento = value;
    }

    /**
     * Recupera il valore della proprietà messaggio.
     * 
     * @return
     *     possible object is
     *     {@link EmailMessageType }
     *     
     */
    public EmailMessageType getMessaggio() {
        return messaggio;
    }

    /**
     * Imposta il valore della proprietà messaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailMessageType }
     *     
     */
    public void setMessaggio(EmailMessageType value) {
        this.messaggio = value;
    }

    /**
     * Recupera il valore della proprietà datiCert.
     * 
     * @return
     *     possible object is
     *     {@link PostacertType }
     *     
     */
    public PostacertType getDatiCert() {
        return datiCert;
    }

    /**
     * Imposta il valore della proprietà datiCert.
     * 
     * @param value
     *     allowed object is
     *     {@link PostacertType }
     *     
     */
    public void setDatiCert(PostacertType value) {
        this.datiCert = value;
    }

    /**
     * Recupera il valore della proprietà chiaveFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveFascicolo() {
        return chiaveFascicolo;
    }

    /**
     * Imposta il valore della proprietà chiaveFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveFascicolo(String value) {
        this.chiaveFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà datiSegnatura.
     * 
     * @return
     *     possible object is
     *     {@link Segnatura }
     *     
     */
    public Segnatura getDatiSegnatura() {
        return datiSegnatura;
    }

    /**
     * Imposta il valore della proprietà datiSegnatura.
     * 
     * @param value
     *     allowed object is
     *     {@link Segnatura }
     *     
     */
    public void setDatiSegnatura(Segnatura value) {
        this.datiSegnatura = value;
    }

    /**
     * Recupera il valore della proprietà validazioneMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link ProtValidazioneMessaggioType }
     *     
     */
    public ProtValidazioneMessaggioType getValidazioneMessaggio() {
        return validazioneMessaggio;
    }

    /**
     * Imposta il valore della proprietà validazioneMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtValidazioneMessaggioType }
     *     
     */
    public void setValidazioneMessaggio(ProtValidazioneMessaggioType value) {
        this.validazioneMessaggio = value;
    }

}
