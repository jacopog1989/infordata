
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Definisce il tipo di dato relativo alla registrazione ausiliaria
 * 
 * <p>Classe Java per reg_registrazioneAusiliariaMini_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="reg_registrazioneAusiliariaMini_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdRegistrazioneAusiliaria" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="CodiceAmministrazione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CodiceAOO" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CodiceRegistro" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="TipoRegistro" type="{http://mef.gov.it.v1.npsTypes}all_tipoRegistroAusiliario_type"/>
 *         &lt;element name="DataRegistrazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="NumeroRegistrazione" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Oggetto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CodiceTipoDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="IsAnnullato" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reg_registrazioneAusiliariaMini_type", propOrder = {
    "idRegistrazioneAusiliaria",
    "codiceAmministrazione",
    "codiceAOO",
    "codiceRegistro",
    "tipoRegistro",
    "dataRegistrazione",
    "numeroRegistrazione",
    "oggetto",
    "codiceTipoDocumento",
    "isAnnullato"
})
public class RegRegistrazioneAusiliariaMiniType
    implements Serializable
{

    @XmlElement(name = "IdRegistrazioneAusiliaria", required = true, nillable = true)
    protected String idRegistrazioneAusiliaria;
    @XmlElement(name = "CodiceAmministrazione", required = true)
    protected String codiceAmministrazione;
    @XmlElement(name = "CodiceAOO", required = true)
    protected String codiceAOO;
    @XmlElement(name = "CodiceRegistro", required = true)
    protected String codiceRegistro;
    @XmlElement(name = "TipoRegistro", required = true)
    @XmlSchemaType(name = "string")
    protected AllTipoRegistroAusiliarioType tipoRegistro;
    @XmlElement(name = "DataRegistrazione", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataRegistrazione;
    @XmlElement(name = "NumeroRegistrazione")
    protected int numeroRegistrazione;
    @XmlElement(name = "Oggetto", required = true)
    protected String oggetto;
    @XmlElementRef(name = "CodiceTipoDocumento", type = JAXBElement.class, required = false)
    protected JAXBElement<String> codiceTipoDocumento;
    @XmlElement(name = "IsAnnullato")
    protected boolean isAnnullato;

    /**
     * Recupera il valore della proprietà idRegistrazioneAusiliaria.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRegistrazioneAusiliaria() {
        return idRegistrazioneAusiliaria;
    }

    /**
     * Imposta il valore della proprietà idRegistrazioneAusiliaria.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRegistrazioneAusiliaria(String value) {
        this.idRegistrazioneAusiliaria = value;
    }

    /**
     * Recupera il valore della proprietà codiceAmministrazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceAmministrazione() {
        return codiceAmministrazione;
    }

    /**
     * Imposta il valore della proprietà codiceAmministrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceAmministrazione(String value) {
        this.codiceAmministrazione = value;
    }

    /**
     * Recupera il valore della proprietà codiceAOO.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceAOO() {
        return codiceAOO;
    }

    /**
     * Imposta il valore della proprietà codiceAOO.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceAOO(String value) {
        this.codiceAOO = value;
    }

    /**
     * Recupera il valore della proprietà codiceRegistro.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceRegistro() {
        return codiceRegistro;
    }

    /**
     * Imposta il valore della proprietà codiceRegistro.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceRegistro(String value) {
        this.codiceRegistro = value;
    }

    /**
     * Recupera il valore della proprietà tipoRegistro.
     * 
     * @return
     *     possible object is
     *     {@link AllTipoRegistroAusiliarioType }
     *     
     */
    public AllTipoRegistroAusiliarioType getTipoRegistro() {
        return tipoRegistro;
    }

    /**
     * Imposta il valore della proprietà tipoRegistro.
     * 
     * @param value
     *     allowed object is
     *     {@link AllTipoRegistroAusiliarioType }
     *     
     */
    public void setTipoRegistro(AllTipoRegistroAusiliarioType value) {
        this.tipoRegistro = value;
    }

    /**
     * Recupera il valore della proprietà dataRegistrazione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataRegistrazione() {
        return dataRegistrazione;
    }

    /**
     * Imposta il valore della proprietà dataRegistrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataRegistrazione(XMLGregorianCalendar value) {
        this.dataRegistrazione = value;
    }

    /**
     * Recupera il valore della proprietà numeroRegistrazione.
     * 
     */
    public int getNumeroRegistrazione() {
        return numeroRegistrazione;
    }

    /**
     * Imposta il valore della proprietà numeroRegistrazione.
     * 
     */
    public void setNumeroRegistrazione(int value) {
        this.numeroRegistrazione = value;
    }

    /**
     * Recupera il valore della proprietà oggetto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggetto() {
        return oggetto;
    }

    /**
     * Imposta il valore della proprietà oggetto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggetto(String value) {
        this.oggetto = value;
    }

    /**
     * Recupera il valore della proprietà codiceTipoDocumento.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getCodiceTipoDocumento() {
        return codiceTipoDocumento;
    }

    /**
     * Imposta il valore della proprietà codiceTipoDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setCodiceTipoDocumento(JAXBElement<String> value) {
        this.codiceTipoDocumento = value;
    }

    /**
     * Recupera il valore della proprietà isAnnullato.
     * 
     */
    public boolean isIsAnnullato() {
        return isAnnullato;
    }

    /**
     * Imposta il valore della proprietà isAnnullato.
     * 
     */
    public void setIsAnnullato(boolean value) {
        this.isAnnullato = value;
    }

}
