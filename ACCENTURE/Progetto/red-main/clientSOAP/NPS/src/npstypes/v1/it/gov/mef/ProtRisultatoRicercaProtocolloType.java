
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce il tipo di dato relativo alla restituzione dell'elenco dei protocolli ricercati  
 * 
 * <p>Classe Java per prot_risultatoRicercaProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_risultatoRicercaProtocollo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="Entrata" type="{http://mef.gov.it.v1.npsTypes}prot_protocolloEntrataResponse_type"/>
 *         &lt;element name="Uscita" type="{http://mef.gov.it.v1.npsTypes}prot_protocolloUscitaResponseType"/>
 *         &lt;element name="Base" type="{http://mef.gov.it.v1.npsTypes}prot_protocolloDatiMinimi_type"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_risultatoRicercaProtocollo_type", propOrder = {
    "entrata",
    "uscita",
    "base"
})
public class ProtRisultatoRicercaProtocolloType
    implements Serializable
{

    @XmlElement(name = "Entrata")
    protected ProtProtocolloEntrataResponseType entrata;
    @XmlElement(name = "Uscita")
    protected ProtProtocolloUscitaResponseType uscita;
    @XmlElement(name = "Base")
    protected ProtProtocolloDatiMinimiType base;

    /**
     * Recupera il valore della proprietà entrata.
     * 
     * @return
     *     possible object is
     *     {@link ProtProtocolloEntrataResponseType }
     *     
     */
    public ProtProtocolloEntrataResponseType getEntrata() {
        return entrata;
    }

    /**
     * Imposta il valore della proprietà entrata.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtProtocolloEntrataResponseType }
     *     
     */
    public void setEntrata(ProtProtocolloEntrataResponseType value) {
        this.entrata = value;
    }

    /**
     * Recupera il valore della proprietà uscita.
     * 
     * @return
     *     possible object is
     *     {@link ProtProtocolloUscitaResponseType }
     *     
     */
    public ProtProtocolloUscitaResponseType getUscita() {
        return uscita;
    }

    /**
     * Imposta il valore della proprietà uscita.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtProtocolloUscitaResponseType }
     *     
     */
    public void setUscita(ProtProtocolloUscitaResponseType value) {
        this.uscita = value;
    }

    /**
     * Recupera il valore della proprietà base.
     * 
     * @return
     *     possible object is
     *     {@link ProtProtocolloDatiMinimiType }
     *     
     */
    public ProtProtocolloDatiMinimiType getBase() {
        return base;
    }

    /**
     * Imposta il valore della proprietà base.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtProtocolloDatiMinimiType }
     *     
     */
    public void setBase(ProtProtocolloDatiMinimiType value) {
        this.base = value;
    }

}
