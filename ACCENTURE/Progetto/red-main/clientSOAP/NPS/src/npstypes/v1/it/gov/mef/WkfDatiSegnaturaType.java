
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import it.gov.digitpa.protocollo.ContestoProcedurale;
import it.gov.digitpa.protocollo.Identificatore;


/**
 * Dati della segnatura
 * 
 * <p>Classe Java per wkf_datiSegnatura_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_datiSegnatura_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Identificatore" type="{http://www.digitPa.gov.it/protocollo/}Identificatore"/>
 *         &lt;element name="ContestoProcedurale" type="{http://www.digitPa.gov.it/protocollo/}ContestoProcedurale" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_datiSegnatura_type", propOrder = {
    "identificatore",
    "contestoProcedurale"
})
public class WkfDatiSegnaturaType
    implements Serializable
{

    @XmlElement(name = "Identificatore", required = true)
    protected Identificatore identificatore;
    @XmlElement(name = "ContestoProcedurale")
    protected List<ContestoProcedurale> contestoProcedurale;

    /**
     * Recupera il valore della proprietà identificatore.
     * 
     * @return
     *     possible object is
     *     {@link Identificatore }
     *     
     */
    public Identificatore getIdentificatore() {
        return identificatore;
    }

    /**
     * Imposta il valore della proprietà identificatore.
     * 
     * @param value
     *     allowed object is
     *     {@link Identificatore }
     *     
     */
    public void setIdentificatore(Identificatore value) {
        this.identificatore = value;
    }

    /**
     * Gets the value of the contestoProcedurale property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the contestoProcedurale property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getContestoProcedurale().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ContestoProcedurale }
     * 
     * 
     */
    public List<ContestoProcedurale> getContestoProcedurale() {
        if (contestoProcedurale == null) {
            contestoProcedurale = new ArrayList<ContestoProcedurale>();
        }
        return this.contestoProcedurale;
    }

}
