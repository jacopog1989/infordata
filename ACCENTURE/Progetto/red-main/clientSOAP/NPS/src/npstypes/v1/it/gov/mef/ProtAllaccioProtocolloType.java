
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Definisce il tio di dato del Protocollo da Allacciare.
 * 
 * <p>Classe Java per prot_allaccioProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_allaccioProtocollo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdProtocollo" type="{http://mef.gov.it.v1.npsTypes}Guid" minOccurs="0"/>
 *         &lt;element name="DataRegistrazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="NumeroRegistrazione" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="TipoProtocollo" type="{http://mef.gov.it.v1.npsTypes}prot_tipoProtocollo_type" minOccurs="0"/>
 *         &lt;element name="Oggetto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Stato" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="Risposta" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_allaccioProtocollo_type", propOrder = {
    "idProtocollo",
    "dataRegistrazione",
    "numeroRegistrazione",
    "tipoProtocollo",
    "oggetto",
    "stato"
})
public class ProtAllaccioProtocolloType
    implements Serializable
{

    @XmlElementRef(name = "IdProtocollo", type = JAXBElement.class, required = false)
    protected JAXBElement<String> idProtocollo;
    @XmlElement(name = "DataRegistrazione", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataRegistrazione;
    @XmlElement(name = "NumeroRegistrazione")
    protected int numeroRegistrazione;
    @XmlElement(name = "TipoProtocollo")
    @XmlSchemaType(name = "string")
    protected ProtTipoProtocolloType tipoProtocollo;
    @XmlElement(name = "Oggetto")
    protected String oggetto;
    @XmlElement(name = "Stato")
    protected String stato;
    @XmlAttribute(name = "Risposta")
    protected Boolean risposta;

    /**
     * Recupera il valore della proprietà idProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public JAXBElement<String> getIdProtocollo() {
        return idProtocollo;
    }

    /**
     * Imposta il valore della proprietà idProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link String }{@code >}
     *     
     */
    public void setIdProtocollo(JAXBElement<String> value) {
        this.idProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà dataRegistrazione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataRegistrazione() {
        return dataRegistrazione;
    }

    /**
     * Imposta il valore della proprietà dataRegistrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataRegistrazione(XMLGregorianCalendar value) {
        this.dataRegistrazione = value;
    }

    /**
     * Recupera il valore della proprietà numeroRegistrazione.
     * 
     */
    public int getNumeroRegistrazione() {
        return numeroRegistrazione;
    }

    /**
     * Imposta il valore della proprietà numeroRegistrazione.
     * 
     */
    public void setNumeroRegistrazione(int value) {
        this.numeroRegistrazione = value;
    }

    /**
     * Recupera il valore della proprietà tipoProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link ProtTipoProtocolloType }
     *     
     */
    public ProtTipoProtocolloType getTipoProtocollo() {
        return tipoProtocollo;
    }

    /**
     * Imposta il valore della proprietà tipoProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtTipoProtocolloType }
     *     
     */
    public void setTipoProtocollo(ProtTipoProtocolloType value) {
        this.tipoProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà oggetto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggetto() {
        return oggetto;
    }

    /**
     * Imposta il valore della proprietà oggetto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggetto(String value) {
        this.oggetto = value;
    }

    /**
     * Recupera il valore della proprietà stato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStato() {
        return stato;
    }

    /**
     * Imposta il valore della proprietà stato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStato(String value) {
        this.stato = value;
    }

    /**
     * Recupera il valore della proprietà risposta.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isRisposta() {
        return risposta;
    }

    /**
     * Imposta il valore della proprietà risposta.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setRisposta(Boolean value) {
        this.risposta = value;
    }

}
