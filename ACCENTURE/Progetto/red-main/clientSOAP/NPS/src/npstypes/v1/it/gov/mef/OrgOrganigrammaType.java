
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce il tipo di dato Organigramma 
 * 
 * <p>Classe Java per org_organigramma_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="org_organigramma_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UnitaOrganizzativa" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_unitaOrganizzativa_type"/>
 *         &lt;element name="Utente" type="{http://mef.gov.it.v1.npsTypes}ana_personaFisica_type" minOccurs="0"/>
 *         &lt;element name="IndirizzoPostale" type="{http://mef.gov.it.v1.npsTypes}ana_indirizzoPostale_type" minOccurs="0"/>
 *         &lt;element name="IndirizzoTelematico" type="{http://mef.gov.it.v1.npsTypes}ana_indirizzoTelematico_type" minOccurs="0"/>
 *         &lt;element name="AclItem" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChiaveEsterna" type="{http://mef.gov.it.v1.npsTypes}all_chiaveEsterna_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "org_organigramma_type", propOrder = {
    "unitaOrganizzativa",
    "utente",
    "indirizzoPostale",
    "indirizzoTelematico",
    "aclItem",
    "chiaveEsterna"
})
public class OrgOrganigrammaType
    implements Serializable
{

    @XmlElement(name = "UnitaOrganizzativa", required = true)
    protected OrgOrganigrammaUnitaOrganizzativaType unitaOrganizzativa;
    @XmlElement(name = "Utente")
    protected AnaPersonaFisicaType utente;
    @XmlElement(name = "IndirizzoPostale")
    protected AnaIndirizzoPostaleType indirizzoPostale;
    @XmlElement(name = "IndirizzoTelematico")
    protected AnaIndirizzoTelematicoType indirizzoTelematico;
    @XmlElement(name = "AclItem")
    protected String aclItem;
    @XmlElement(name = "ChiaveEsterna")
    protected AllChiaveEsternaType chiaveEsterna;

    /**
     * Recupera il valore della proprietà unitaOrganizzativa.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaUnitaOrganizzativaType }
     *     
     */
    public OrgOrganigrammaUnitaOrganizzativaType getUnitaOrganizzativa() {
        return unitaOrganizzativa;
    }

    /**
     * Imposta il valore della proprietà unitaOrganizzativa.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaUnitaOrganizzativaType }
     *     
     */
    public void setUnitaOrganizzativa(OrgOrganigrammaUnitaOrganizzativaType value) {
        this.unitaOrganizzativa = value;
    }

    /**
     * Recupera il valore della proprietà utente.
     * 
     * @return
     *     possible object is
     *     {@link AnaPersonaFisicaType }
     *     
     */
    public AnaPersonaFisicaType getUtente() {
        return utente;
    }

    /**
     * Imposta il valore della proprietà utente.
     * 
     * @param value
     *     allowed object is
     *     {@link AnaPersonaFisicaType }
     *     
     */
    public void setUtente(AnaPersonaFisicaType value) {
        this.utente = value;
    }

    /**
     * Recupera il valore della proprietà indirizzoPostale.
     * 
     * @return
     *     possible object is
     *     {@link AnaIndirizzoPostaleType }
     *     
     */
    public AnaIndirizzoPostaleType getIndirizzoPostale() {
        return indirizzoPostale;
    }

    /**
     * Imposta il valore della proprietà indirizzoPostale.
     * 
     * @param value
     *     allowed object is
     *     {@link AnaIndirizzoPostaleType }
     *     
     */
    public void setIndirizzoPostale(AnaIndirizzoPostaleType value) {
        this.indirizzoPostale = value;
    }

    /**
     * Recupera il valore della proprietà indirizzoTelematico.
     * 
     * @return
     *     possible object is
     *     {@link AnaIndirizzoTelematicoType }
     *     
     */
    public AnaIndirizzoTelematicoType getIndirizzoTelematico() {
        return indirizzoTelematico;
    }

    /**
     * Imposta il valore della proprietà indirizzoTelematico.
     * 
     * @param value
     *     allowed object is
     *     {@link AnaIndirizzoTelematicoType }
     *     
     */
    public void setIndirizzoTelematico(AnaIndirizzoTelematicoType value) {
        this.indirizzoTelematico = value;
    }

    /**
     * Recupera il valore della proprietà aclItem.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAclItem() {
        return aclItem;
    }

    /**
     * Imposta il valore della proprietà aclItem.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAclItem(String value) {
        this.aclItem = value;
    }

    /**
     * Recupera il valore della proprietà chiaveEsterna.
     * 
     * @return
     *     possible object is
     *     {@link AllChiaveEsternaType }
     *     
     */
    public AllChiaveEsternaType getChiaveEsterna() {
        return chiaveEsterna;
    }

    /**
     * Imposta il valore della proprietà chiaveEsterna.
     * 
     * @param value
     *     allowed object is
     *     {@link AllChiaveEsternaType }
     *     
     */
    public void setChiaveEsterna(AllChiaveEsternaType value) {
        this.chiaveEsterna = value;
    }

}
