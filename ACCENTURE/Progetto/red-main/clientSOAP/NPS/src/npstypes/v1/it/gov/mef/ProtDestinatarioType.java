
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * Definisce i metadati del singolo destinatario
 * 
 * <p>Classe Java per prot_destinatario_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_destinatario_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}prot_corrispondente_type">
 *       &lt;sequence>
 *         &lt;element name="Spedizione" type="{http://mef.gov.it.v1.npsTypes}prot_spedizione_type" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="PerConoscenza" default="no">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}NMTOKEN">
 *             &lt;enumeration value="si"/>
 *             &lt;enumeration value="no"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_destinatario_type", propOrder = {
    "spedizione"
})
public class ProtDestinatarioType
    extends ProtCorrispondenteType
    implements Serializable
{

    @XmlElement(name = "Spedizione")
    protected ProtSpedizioneType spedizione;
    @XmlAttribute(name = "PerConoscenza")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String perConoscenza;

    /**
     * Recupera il valore della proprietà spedizione.
     * 
     * @return
     *     possible object is
     *     {@link ProtSpedizioneType }
     *     
     */
    public ProtSpedizioneType getSpedizione() {
        return spedizione;
    }

    /**
     * Imposta il valore della proprietà spedizione.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtSpedizioneType }
     *     
     */
    public void setSpedizione(ProtSpedizioneType value) {
        this.spedizione = value;
    }

    /**
     * Recupera il valore della proprietà perConoscenza.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPerConoscenza() {
        if (perConoscenza == null) {
            return "no";
        } else {
            return perConoscenza;
        }
    }

    /**
     * Imposta il valore della proprietà perConoscenza.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPerConoscenza(String value) {
        this.perConoscenza = value;
    }

}
