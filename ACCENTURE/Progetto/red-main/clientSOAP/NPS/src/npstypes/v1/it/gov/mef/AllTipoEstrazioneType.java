
package npstypes.v1.it.gov.mef;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per all_tipoEstrazione_type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="all_tipoEstrazione_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NONE"/>
 *     &lt;enumeration value="DATA"/>
 *     &lt;enumeration value="METADATA"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "all_tipoEstrazione_type")
@XmlEnum
public enum AllTipoEstrazioneType {

    NONE,
    DATA,
    METADATA;

    public String value() {
        return name();
    }

    public static AllTipoEstrazioneType fromValue(String v) {
        return valueOf(v);
    }

}
