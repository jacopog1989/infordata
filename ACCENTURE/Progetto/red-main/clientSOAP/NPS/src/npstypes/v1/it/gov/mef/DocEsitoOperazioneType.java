
package npstypes.v1.it.gov.mef;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per doc_esitoOperazione_type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="doc_esitoOperazione_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="INSERITA"/>
 *     &lt;enumeration value="IN_LAVORAZIONE"/>
 *     &lt;enumeration value="COMPLETA"/>
 *     &lt;enumeration value="ERRORE"/>
 *     &lt;enumeration value="NON_CONVERTIBILE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "doc_esitoOperazione_type")
@XmlEnum
public enum DocEsitoOperazioneType {

    INSERITA,
    IN_LAVORAZIONE,
    COMPLETA,
    ERRORE,
    NON_CONVERTIBILE;

    public String value() {
        return name();
    }

    public static DocEsitoOperazioneType fromValue(String v) {
        return valueOf(v);
    }

}
