
package npstypes.v1.it.gov.mef;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per doc_tipoConversione_type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="doc_tipoConversione_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PDF"/>
 *     &lt;enumeration value="PDF1A"/>
 *     &lt;enumeration value="PDF1B"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "doc_tipoConversione_type")
@XmlEnum
public enum DocTipoConversioneType {

    PDF("PDF"),
    @XmlEnumValue("PDF1A")
    PDF_1_A("PDF1A"),
    @XmlEnumValue("PDF1B")
    PDF_1_B("PDF1B");
    private final String value;

    DocTipoConversioneType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DocTipoConversioneType fromValue(String v) {
        for (DocTipoConversioneType c: DocTipoConversioneType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
