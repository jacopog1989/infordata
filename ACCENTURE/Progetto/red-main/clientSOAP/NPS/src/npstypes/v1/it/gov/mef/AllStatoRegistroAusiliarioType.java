
package npstypes.v1.it.gov.mef;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per all_statoRegistroAusiliario_type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="all_statoRegistroAusiliario_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="APERTO"/>
 *     &lt;enumeration value="CHIUSO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "all_statoRegistroAusiliario_type")
@XmlEnum
public enum AllStatoRegistroAusiliarioType {

    APERTO,
    CHIUSO;

    public String value() {
        return name();
    }

    public static AllStatoRegistroAusiliarioType fromValue(String v) {
        return valueOf(v);
    }

}
