
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce il tipo di dato OperazioneDocumentale che è un 
 * 
 * <p>Classe Java per doc_operazioneDocumentale_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="doc_operazioneDocumentale_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoOperazione" type="{http://mef.gov.it.v1.npsTypes}doc_tipoOperazione_type"/>
 *         &lt;element name="EsitoOperazione" type="{http://mef.gov.it.v1.npsTypes}doc_esitoOperazione_type"/>
 *         &lt;element name="DocumentoOperazione" type="{http://mef.gov.it.v1.npsTypes}doc_documentoFile_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "doc_operazioneDocumentale_type", propOrder = {
    "tipoOperazione",
    "esitoOperazione",
    "documentoOperazione"
})
public class DocOperazioneDocumentaleType
    implements Serializable
{

    @XmlElement(name = "TipoOperazione", required = true)
    @XmlSchemaType(name = "string")
    protected DocTipoOperazioneType tipoOperazione;
    @XmlElement(name = "EsitoOperazione", required = true)
    @XmlSchemaType(name = "string")
    protected DocEsitoOperazioneType esitoOperazione;
    @XmlElement(name = "DocumentoOperazione")
    protected DocDocumentoFileType documentoOperazione;

    /**
     * Recupera il valore della proprietà tipoOperazione.
     * 
     * @return
     *     possible object is
     *     {@link DocTipoOperazioneType }
     *     
     */
    public DocTipoOperazioneType getTipoOperazione() {
        return tipoOperazione;
    }

    /**
     * Imposta il valore della proprietà tipoOperazione.
     * 
     * @param value
     *     allowed object is
     *     {@link DocTipoOperazioneType }
     *     
     */
    public void setTipoOperazione(DocTipoOperazioneType value) {
        this.tipoOperazione = value;
    }

    /**
     * Recupera il valore della proprietà esitoOperazione.
     * 
     * @return
     *     possible object is
     *     {@link DocEsitoOperazioneType }
     *     
     */
    public DocEsitoOperazioneType getEsitoOperazione() {
        return esitoOperazione;
    }

    /**
     * Imposta il valore della proprietà esitoOperazione.
     * 
     * @param value
     *     allowed object is
     *     {@link DocEsitoOperazioneType }
     *     
     */
    public void setEsitoOperazione(DocEsitoOperazioneType value) {
        this.esitoOperazione = value;
    }

    /**
     * Recupera il valore della proprietà documentoOperazione.
     * 
     * @return
     *     possible object is
     *     {@link DocDocumentoFileType }
     *     
     */
    public DocDocumentoFileType getDocumentoOperazione() {
        return documentoOperazione;
    }

    /**
     * Imposta il valore della proprietà documentoOperazione.
     * 
     * @param value
     *     allowed object is
     *     {@link DocDocumentoFileType }
     *     
     */
    public void setDocumentoOperazione(DocDocumentoFileType value) {
        this.documentoOperazione = value;
    }

}
