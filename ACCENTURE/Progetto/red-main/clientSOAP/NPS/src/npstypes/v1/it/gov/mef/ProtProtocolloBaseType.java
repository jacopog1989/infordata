
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import it.gov.digitpa.protocollo.Classifica;
import it.gov.digitpa.protocollo.InterventoOperatore;
import it.gov.digitpa.protocollo.Note;
import it.gov.digitpa.protocollo.PiuInfo;
import it.gov.digitpa.protocollo.PrimaRegistrazione;
import it.gov.digitpa.protocollo.Riferimenti;
import it.gov.digitpa.protocollo.RiferimentiTelematici;
import it.gov.digitpa.protocollo.RiferimentoDocumentiCartacei;


/**
 * Definisce l'insieme dei dati di protocollo
 * 
 * <p>Classe Java per prot_protocolloBase_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="prot_protocolloBase_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdProtocollo" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="IdentificatoreProtocollo" type="{http://mef.gov.it.v1.npsTypes}prot_identificatoreProtocollo_type"/>
 *         &lt;element name="Oggetto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Protocollatore" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type"/>
 *         &lt;element name="Mittente" type="{http://mef.gov.it.v1.npsTypes}prot_mittente_type"/>
 *         &lt;element name="Destinatari" type="{http://mef.gov.it.v1.npsTypes}prot_destinatario_type" maxOccurs="unbounded"/>
 *         &lt;element name="DatiAnnullamento" type="{http://mef.gov.it.v1.npsTypes}prot_annullamento_type" minOccurs="0"/>
 *         &lt;element name="Allacciati" type="{http://mef.gov.it.v1.npsTypes}prot_allaccioProtocollo_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="TipologiaDocumento" type="{http://mef.gov.it.v1.npsTypes}prot_attributiEstesi_type"/>
 *         &lt;element name="NumeroAllegati" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DataModifica" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="IsRiservato" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsCompleto" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="SegnaturaEstesa" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Intestazione" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}PrimaRegistrazione" minOccurs="0"/>
 *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}InterventoOperatore" minOccurs="0"/>
 *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentoDocumentiCartacei" minOccurs="0"/>
 *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentiTelematici" minOccurs="0"/>
 *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Classifica" minOccurs="0"/>
 *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Note" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Riferimenti" minOccurs="0"/>
 *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}PiuInfo" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Annotazioni" type="{http://mef.gov.it.v1.npsTypes}prot_annotazione_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="StoricoOperazioni" type="{http://mef.gov.it.v1.npsTypes}prot_registroOperazioniProtocollo_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Acl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="RegistroEmergenza" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="DataRegistrazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                   &lt;element name="NumeroRegistrazione" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="Note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="VoceTitolario" type="{http://mef.gov.it.v1.npsTypes}all_codiceDescrizione_type" minOccurs="0"/>
 *         &lt;element name="SistemaAusiliario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ArchiviazioneSostitutiva" type="{http://mef.gov.it.v1.npsTypes}prot_archiviazioneSostitutiva_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prot_protocolloBase_type", propOrder = {
    "idProtocollo",
    "identificatoreProtocollo",
    "oggetto",
    "protocollatore",
    "mittente",
    "destinatari",
    "datiAnnullamento",
    "allacciati",
    "tipologiaDocumento",
    "numeroAllegati",
    "dataModifica",
    "isRiservato",
    "isCompleto",
    "segnaturaEstesa",
    "annotazioni",
    "storicoOperazioni",
    "acl",
    "registroEmergenza",
    "voceTitolario",
    "sistemaAusiliario",
    "archiviazioneSostitutiva"
})
@XmlSeeAlso({
    ProtProtocolloBaseResponseType.class,
    ProtProtocolloBaseRequestType.class
})
public class ProtProtocolloBaseType
    implements Serializable
{

    @XmlElement(name = "IdProtocollo", required = true, nillable = true)
    protected String idProtocollo;
    @XmlElement(name = "IdentificatoreProtocollo", required = true)
    protected ProtIdentificatoreProtocolloType identificatoreProtocollo;
    @XmlElement(name = "Oggetto", required = true)
    protected String oggetto;
    @XmlElement(name = "Protocollatore", required = true)
    protected OrgOrganigrammaType protocollatore;
    @XmlElement(name = "Mittente", required = true)
    protected ProtMittenteType mittente;
    @XmlElement(name = "Destinatari", required = true)
    protected List<ProtDestinatarioType> destinatari;
    @XmlElement(name = "DatiAnnullamento")
    protected ProtAnnullamentoType datiAnnullamento;
    @XmlElement(name = "Allacciati")
    protected List<ProtAllaccioProtocolloType> allacciati;
    @XmlElement(name = "TipologiaDocumento", required = true)
    protected ProtAttributiEstesiType tipologiaDocumento;
    @XmlElement(name = "NumeroAllegati")
    protected Integer numeroAllegati;
    @XmlElement(name = "DataModifica")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataModifica;
    @XmlElement(name = "IsRiservato", defaultValue = "false")
    protected Boolean isRiservato;
    @XmlElement(name = "IsCompleto", defaultValue = "false")
    protected Boolean isCompleto;
    @XmlElement(name = "SegnaturaEstesa")
    protected ProtProtocolloBaseType.SegnaturaEstesa segnaturaEstesa;
    @XmlElement(name = "Annotazioni")
    protected List<ProtAnnotazioneType> annotazioni;
    @XmlElement(name = "StoricoOperazioni")
    protected List<ProtRegistroOperazioniProtocolloType> storicoOperazioni;
    @XmlElement(name = "Acl")
    protected String acl;
    @XmlElement(name = "RegistroEmergenza")
    protected ProtProtocolloBaseType.RegistroEmergenza registroEmergenza;
    @XmlElement(name = "VoceTitolario")
    protected AllCodiceDescrizioneType voceTitolario;
    @XmlElement(name = "SistemaAusiliario")
    protected String sistemaAusiliario;
    @XmlElementRef(name = "ArchiviazioneSostitutiva", type = JAXBElement.class, required = false)
    protected JAXBElement<ProtArchiviazioneSostitutivaType> archiviazioneSostitutiva;

    /**
     * Recupera il valore della proprietà idProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdProtocollo() {
        return idProtocollo;
    }

    /**
     * Imposta il valore della proprietà idProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdProtocollo(String value) {
        this.idProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà identificatoreProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link ProtIdentificatoreProtocolloType }
     *     
     */
    public ProtIdentificatoreProtocolloType getIdentificatoreProtocollo() {
        return identificatoreProtocollo;
    }

    /**
     * Imposta il valore della proprietà identificatoreProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtIdentificatoreProtocolloType }
     *     
     */
    public void setIdentificatoreProtocollo(ProtIdentificatoreProtocolloType value) {
        this.identificatoreProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà oggetto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggetto() {
        return oggetto;
    }

    /**
     * Imposta il valore della proprietà oggetto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggetto(String value) {
        this.oggetto = value;
    }

    /**
     * Recupera il valore della proprietà protocollatore.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getProtocollatore() {
        return protocollatore;
    }

    /**
     * Imposta il valore della proprietà protocollatore.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setProtocollatore(OrgOrganigrammaType value) {
        this.protocollatore = value;
    }

    /**
     * Recupera il valore della proprietà mittente.
     * 
     * @return
     *     possible object is
     *     {@link ProtMittenteType }
     *     
     */
    public ProtMittenteType getMittente() {
        return mittente;
    }

    /**
     * Imposta il valore della proprietà mittente.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtMittenteType }
     *     
     */
    public void setMittente(ProtMittenteType value) {
        this.mittente = value;
    }

    /**
     * Gets the value of the destinatari property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the destinatari property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDestinatari().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtDestinatarioType }
     * 
     * 
     */
    public List<ProtDestinatarioType> getDestinatari() {
        if (destinatari == null) {
            destinatari = new ArrayList<ProtDestinatarioType>();
        }
        return this.destinatari;
    }

    /**
     * Recupera il valore della proprietà datiAnnullamento.
     * 
     * @return
     *     possible object is
     *     {@link ProtAnnullamentoType }
     *     
     */
    public ProtAnnullamentoType getDatiAnnullamento() {
        return datiAnnullamento;
    }

    /**
     * Imposta il valore della proprietà datiAnnullamento.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtAnnullamentoType }
     *     
     */
    public void setDatiAnnullamento(ProtAnnullamentoType value) {
        this.datiAnnullamento = value;
    }

    /**
     * Gets the value of the allacciati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allacciati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllacciati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtAllaccioProtocolloType }
     * 
     * 
     */
    public List<ProtAllaccioProtocolloType> getAllacciati() {
        if (allacciati == null) {
            allacciati = new ArrayList<ProtAllaccioProtocolloType>();
        }
        return this.allacciati;
    }

    /**
     * Recupera il valore della proprietà tipologiaDocumento.
     * 
     * @return
     *     possible object is
     *     {@link ProtAttributiEstesiType }
     *     
     */
    public ProtAttributiEstesiType getTipologiaDocumento() {
        return tipologiaDocumento;
    }

    /**
     * Imposta il valore della proprietà tipologiaDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtAttributiEstesiType }
     *     
     */
    public void setTipologiaDocumento(ProtAttributiEstesiType value) {
        this.tipologiaDocumento = value;
    }

    /**
     * Recupera il valore della proprietà numeroAllegati.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumeroAllegati() {
        return numeroAllegati;
    }

    /**
     * Imposta il valore della proprietà numeroAllegati.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumeroAllegati(Integer value) {
        this.numeroAllegati = value;
    }

    /**
     * Recupera il valore della proprietà dataModifica.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataModifica() {
        return dataModifica;
    }

    /**
     * Imposta il valore della proprietà dataModifica.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataModifica(XMLGregorianCalendar value) {
        this.dataModifica = value;
    }

    /**
     * Recupera il valore della proprietà isRiservato.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsRiservato() {
        return isRiservato;
    }

    /**
     * Imposta il valore della proprietà isRiservato.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsRiservato(Boolean value) {
        this.isRiservato = value;
    }

    /**
     * Recupera il valore della proprietà isCompleto.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsCompleto() {
        return isCompleto;
    }

    /**
     * Imposta il valore della proprietà isCompleto.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsCompleto(Boolean value) {
        this.isCompleto = value;
    }

    /**
     * Recupera il valore della proprietà segnaturaEstesa.
     * 
     * @return
     *     possible object is
     *     {@link ProtProtocolloBaseType.SegnaturaEstesa }
     *     
     */
    public ProtProtocolloBaseType.SegnaturaEstesa getSegnaturaEstesa() {
        return segnaturaEstesa;
    }

    /**
     * Imposta il valore della proprietà segnaturaEstesa.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtProtocolloBaseType.SegnaturaEstesa }
     *     
     */
    public void setSegnaturaEstesa(ProtProtocolloBaseType.SegnaturaEstesa value) {
        this.segnaturaEstesa = value;
    }

    /**
     * Gets the value of the annotazioni property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the annotazioni property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAnnotazioni().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtAnnotazioneType }
     * 
     * 
     */
    public List<ProtAnnotazioneType> getAnnotazioni() {
        if (annotazioni == null) {
            annotazioni = new ArrayList<ProtAnnotazioneType>();
        }
        return this.annotazioni;
    }

    /**
     * Gets the value of the storicoOperazioni property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the storicoOperazioni property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStoricoOperazioni().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtRegistroOperazioniProtocolloType }
     * 
     * 
     */
    public List<ProtRegistroOperazioniProtocolloType> getStoricoOperazioni() {
        if (storicoOperazioni == null) {
            storicoOperazioni = new ArrayList<ProtRegistroOperazioniProtocolloType>();
        }
        return this.storicoOperazioni;
    }

    /**
     * Recupera il valore della proprietà acl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcl() {
        return acl;
    }

    /**
     * Imposta il valore della proprietà acl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcl(String value) {
        this.acl = value;
    }

    /**
     * Recupera il valore della proprietà registroEmergenza.
     * 
     * @return
     *     possible object is
     *     {@link ProtProtocolloBaseType.RegistroEmergenza }
     *     
     */
    public ProtProtocolloBaseType.RegistroEmergenza getRegistroEmergenza() {
        return registroEmergenza;
    }

    /**
     * Imposta il valore della proprietà registroEmergenza.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtProtocolloBaseType.RegistroEmergenza }
     *     
     */
    public void setRegistroEmergenza(ProtProtocolloBaseType.RegistroEmergenza value) {
        this.registroEmergenza = value;
    }

    /**
     * Recupera il valore della proprietà voceTitolario.
     * 
     * @return
     *     possible object is
     *     {@link AllCodiceDescrizioneType }
     *     
     */
    public AllCodiceDescrizioneType getVoceTitolario() {
        return voceTitolario;
    }

    /**
     * Imposta il valore della proprietà voceTitolario.
     * 
     * @param value
     *     allowed object is
     *     {@link AllCodiceDescrizioneType }
     *     
     */
    public void setVoceTitolario(AllCodiceDescrizioneType value) {
        this.voceTitolario = value;
    }

    /**
     * Recupera il valore della proprietà sistemaAusiliario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaAusiliario() {
        return sistemaAusiliario;
    }

    /**
     * Imposta il valore della proprietà sistemaAusiliario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaAusiliario(String value) {
        this.sistemaAusiliario = value;
    }

    /**
     * Recupera il valore della proprietà archiviazioneSostitutiva.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link ProtArchiviazioneSostitutivaType }{@code >}
     *     
     */
    public JAXBElement<ProtArchiviazioneSostitutivaType> getArchiviazioneSostitutiva() {
        return archiviazioneSostitutiva;
    }

    /**
     * Imposta il valore della proprietà archiviazioneSostitutiva.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link ProtArchiviazioneSostitutivaType }{@code >}
     *     
     */
    public void setArchiviazioneSostitutiva(JAXBElement<ProtArchiviazioneSostitutivaType> value) {
        this.archiviazioneSostitutiva = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DataRegistrazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *         &lt;element name="NumeroRegistrazione" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="Note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dataRegistrazione",
        "numeroRegistrazione",
        "note"
    })
    public static class RegistroEmergenza
        implements Serializable
    {

        @XmlElement(name = "DataRegistrazione", required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar dataRegistrazione;
        @XmlElement(name = "NumeroRegistrazione")
        protected int numeroRegistrazione;
        @XmlElement(name = "Note")
        protected String note;

        /**
         * Recupera il valore della proprietà dataRegistrazione.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDataRegistrazione() {
            return dataRegistrazione;
        }

        /**
         * Imposta il valore della proprietà dataRegistrazione.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDataRegistrazione(XMLGregorianCalendar value) {
            this.dataRegistrazione = value;
        }

        /**
         * Recupera il valore della proprietà numeroRegistrazione.
         * 
         */
        public int getNumeroRegistrazione() {
            return numeroRegistrazione;
        }

        /**
         * Imposta il valore della proprietà numeroRegistrazione.
         * 
         */
        public void setNumeroRegistrazione(int value) {
            this.numeroRegistrazione = value;
        }

        /**
         * Recupera il valore della proprietà note.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNote() {
            return note;
        }

        /**
         * Imposta il valore della proprietà note.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNote(String value) {
            this.note = value;
        }

    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Intestazione" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}PrimaRegistrazione" minOccurs="0"/>
     *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}InterventoOperatore" minOccurs="0"/>
     *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentoDocumentiCartacei" minOccurs="0"/>
     *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentiTelematici" minOccurs="0"/>
     *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Classifica" minOccurs="0"/>
     *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Note" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Riferimenti" minOccurs="0"/>
     *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}PiuInfo" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "intestazione",
        "riferimenti",
        "piuInfo"
    })
    public static class SegnaturaEstesa
        implements Serializable
    {

        @XmlElement(name = "Intestazione")
        protected ProtProtocolloBaseType.SegnaturaEstesa.Intestazione intestazione;
        @XmlElement(name = "Riferimenti", namespace = "http://www.digitPa.gov.it/protocollo/")
        protected Riferimenti riferimenti;
        @XmlElement(name = "PiuInfo", namespace = "http://www.digitPa.gov.it/protocollo/")
        protected PiuInfo piuInfo;

        /**
         * Recupera il valore della proprietà intestazione.
         * 
         * @return
         *     possible object is
         *     {@link ProtProtocolloBaseType.SegnaturaEstesa.Intestazione }
         *     
         */
        public ProtProtocolloBaseType.SegnaturaEstesa.Intestazione getIntestazione() {
            return intestazione;
        }

        /**
         * Imposta il valore della proprietà intestazione.
         * 
         * @param value
         *     allowed object is
         *     {@link ProtProtocolloBaseType.SegnaturaEstesa.Intestazione }
         *     
         */
        public void setIntestazione(ProtProtocolloBaseType.SegnaturaEstesa.Intestazione value) {
            this.intestazione = value;
        }

        /**
         * Recupera il valore della proprietà riferimenti.
         * 
         * @return
         *     possible object is
         *     {@link Riferimenti }
         *     
         */
        public Riferimenti getRiferimenti() {
            return riferimenti;
        }

        /**
         * Imposta il valore della proprietà riferimenti.
         * 
         * @param value
         *     allowed object is
         *     {@link Riferimenti }
         *     
         */
        public void setRiferimenti(Riferimenti value) {
            this.riferimenti = value;
        }

        /**
         * Recupera il valore della proprietà piuInfo.
         * 
         * @return
         *     possible object is
         *     {@link PiuInfo }
         *     
         */
        public PiuInfo getPiuInfo() {
            return piuInfo;
        }

        /**
         * Imposta il valore della proprietà piuInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link PiuInfo }
         *     
         */
        public void setPiuInfo(PiuInfo value) {
            this.piuInfo = value;
        }


        /**
         * <p>Classe Java per anonymous complex type.
         * 
         * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}PrimaRegistrazione" minOccurs="0"/>
         *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}InterventoOperatore" minOccurs="0"/>
         *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentoDocumentiCartacei" minOccurs="0"/>
         *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentiTelematici" minOccurs="0"/>
         *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Classifica" minOccurs="0"/>
         *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Note" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "primaRegistrazione",
            "interventoOperatore",
            "riferimentoDocumentiCartacei",
            "riferimentiTelematici",
            "classifica",
            "note"
        })
        public static class Intestazione
            implements Serializable
        {

            @XmlElement(name = "PrimaRegistrazione", namespace = "http://www.digitPa.gov.it/protocollo/")
            protected PrimaRegistrazione primaRegistrazione;
            @XmlElement(name = "InterventoOperatore", namespace = "http://www.digitPa.gov.it/protocollo/")
            protected InterventoOperatore interventoOperatore;
            @XmlElement(name = "RiferimentoDocumentiCartacei", namespace = "http://www.digitPa.gov.it/protocollo/")
            protected RiferimentoDocumentiCartacei riferimentoDocumentiCartacei;
            @XmlElement(name = "RiferimentiTelematici", namespace = "http://www.digitPa.gov.it/protocollo/")
            protected RiferimentiTelematici riferimentiTelematici;
            @XmlElement(name = "Classifica", namespace = "http://www.digitPa.gov.it/protocollo/")
            protected Classifica classifica;
            @XmlElement(name = "Note", namespace = "http://www.digitPa.gov.it/protocollo/")
            protected Note note;

            /**
             * Recupera il valore della proprietà primaRegistrazione.
             * 
             * @return
             *     possible object is
             *     {@link PrimaRegistrazione }
             *     
             */
            public PrimaRegistrazione getPrimaRegistrazione() {
                return primaRegistrazione;
            }

            /**
             * Imposta il valore della proprietà primaRegistrazione.
             * 
             * @param value
             *     allowed object is
             *     {@link PrimaRegistrazione }
             *     
             */
            public void setPrimaRegistrazione(PrimaRegistrazione value) {
                this.primaRegistrazione = value;
            }

            /**
             * Recupera il valore della proprietà interventoOperatore.
             * 
             * @return
             *     possible object is
             *     {@link InterventoOperatore }
             *     
             */
            public InterventoOperatore getInterventoOperatore() {
                return interventoOperatore;
            }

            /**
             * Imposta il valore della proprietà interventoOperatore.
             * 
             * @param value
             *     allowed object is
             *     {@link InterventoOperatore }
             *     
             */
            public void setInterventoOperatore(InterventoOperatore value) {
                this.interventoOperatore = value;
            }

            /**
             * Recupera il valore della proprietà riferimentoDocumentiCartacei.
             * 
             * @return
             *     possible object is
             *     {@link RiferimentoDocumentiCartacei }
             *     
             */
            public RiferimentoDocumentiCartacei getRiferimentoDocumentiCartacei() {
                return riferimentoDocumentiCartacei;
            }

            /**
             * Imposta il valore della proprietà riferimentoDocumentiCartacei.
             * 
             * @param value
             *     allowed object is
             *     {@link RiferimentoDocumentiCartacei }
             *     
             */
            public void setRiferimentoDocumentiCartacei(RiferimentoDocumentiCartacei value) {
                this.riferimentoDocumentiCartacei = value;
            }

            /**
             * Recupera il valore della proprietà riferimentiTelematici.
             * 
             * @return
             *     possible object is
             *     {@link RiferimentiTelematici }
             *     
             */
            public RiferimentiTelematici getRiferimentiTelematici() {
                return riferimentiTelematici;
            }

            /**
             * Imposta il valore della proprietà riferimentiTelematici.
             * 
             * @param value
             *     allowed object is
             *     {@link RiferimentiTelematici }
             *     
             */
            public void setRiferimentiTelematici(RiferimentiTelematici value) {
                this.riferimentiTelematici = value;
            }

            /**
             * Recupera il valore della proprietà classifica.
             * 
             * @return
             *     possible object is
             *     {@link Classifica }
             *     
             */
            public Classifica getClassifica() {
                return classifica;
            }

            /**
             * Imposta il valore della proprietà classifica.
             * 
             * @param value
             *     allowed object is
             *     {@link Classifica }
             *     
             */
            public void setClassifica(Classifica value) {
                this.classifica = value;
            }

            /**
             * Recupera il valore della proprietà note.
             * 
             * @return
             *     possible object is
             *     {@link Note }
             *     
             */
            public Note getNote() {
                return note;
            }

            /**
             * Imposta il valore della proprietà note.
             * 
             * @param value
             *     allowed object is
             *     {@link Note }
             *     
             */
            public void setNote(Note value) {
                this.note = value;
            }

        }

    }

}
