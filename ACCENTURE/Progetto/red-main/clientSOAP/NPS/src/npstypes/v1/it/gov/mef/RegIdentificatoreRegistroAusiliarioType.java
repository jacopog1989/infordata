
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Definisce il tipo di dato Identificatore della Registrazione Ausiliaria
 * 
 * <p>Classe Java per reg_identificatoreRegistroAusiliario_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="reg_identificatoreRegistroAusiliario_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="IdRegistroAusiliario" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="Registro" type="{http://mef.gov.it.v1.npsTypes}reg_registro_type"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reg_identificatoreRegistroAusiliario_type", propOrder = {
    "idRegistroAusiliario",
    "registro"
})
public class RegIdentificatoreRegistroAusiliarioType
    implements Serializable
{

    @XmlElement(name = "IdRegistroAusiliario")
    protected String idRegistroAusiliario;
    @XmlElement(name = "Registro")
    protected RegRegistroType registro;

    /**
     * Recupera il valore della proprietà idRegistroAusiliario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdRegistroAusiliario() {
        return idRegistroAusiliario;
    }

    /**
     * Imposta il valore della proprietà idRegistroAusiliario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdRegistroAusiliario(String value) {
        this.idRegistroAusiliario = value;
    }

    /**
     * Recupera il valore della proprietà registro.
     * 
     * @return
     *     possible object is
     *     {@link RegRegistroType }
     *     
     */
    public RegRegistroType getRegistro() {
        return registro;
    }

    /**
     * Imposta il valore della proprietà registro.
     * 
     * @param value
     *     allowed object is
     *     {@link RegRegistroType }
     *     
     */
    public void setRegistro(RegRegistroType value) {
        this.registro = value;
    }

}
