
package npstypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Dati del flusso delle notifiche PEC
 * 
 * <p>Classe Java per wkf_datiFlussoNotifica_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="wkf_datiFlussoNotifica_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdMessaggio" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="Oggetto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CodiceFlusso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdentificatoreProcesso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdentificativoMessaggio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChiaveFascicolo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChiaveDocumentoPEC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChiaveDocumentoNotificaPEC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="messaggioRicevuto" type="{http://mef.gov.it.v1.npsTypes}email_message_type" minOccurs="0"/>
 *         &lt;element name="TipoNotificaPEC" type="{http://mef.gov.it.v1.npsTypes}prot_ricevutePec_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "wkf_datiFlussoNotifica_type", propOrder = {
    "idMessaggio",
    "oggetto",
    "codiceFlusso",
    "identificatoreProcesso",
    "identificativoMessaggio",
    "chiaveFascicolo",
    "chiaveDocumentoPEC",
    "chiaveDocumentoNotificaPEC",
    "messaggioRicevuto",
    "tipoNotificaPEC"
})
public class WkfDatiFlussoNotificaType
    implements Serializable
{

    @XmlElement(name = "IdMessaggio", required = true)
    protected String idMessaggio;
    @XmlElement(name = "Oggetto", required = true)
    protected String oggetto;
    @XmlElement(name = "CodiceFlusso", required = true)
    protected String codiceFlusso;
    @XmlElement(name = "IdentificatoreProcesso", required = true)
    protected String identificatoreProcesso;
    @XmlElement(name = "IdentificativoMessaggio", required = true)
    protected String identificativoMessaggio;
    @XmlElement(name = "ChiaveFascicolo")
    protected String chiaveFascicolo;
    @XmlElement(name = "ChiaveDocumentoPEC")
    protected String chiaveDocumentoPEC;
    @XmlElement(name = "ChiaveDocumentoNotificaPEC")
    protected String chiaveDocumentoNotificaPEC;
    protected EmailMessageType messaggioRicevuto;
    @XmlElement(name = "TipoNotificaPEC", required = true)
    protected ProtRicevutePecType tipoNotificaPEC;

    /**
     * Recupera il valore della proprietà idMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdMessaggio() {
        return idMessaggio;
    }

    /**
     * Imposta il valore della proprietà idMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdMessaggio(String value) {
        this.idMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà oggetto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggetto() {
        return oggetto;
    }

    /**
     * Imposta il valore della proprietà oggetto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggetto(String value) {
        this.oggetto = value;
    }

    /**
     * Recupera il valore della proprietà codiceFlusso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceFlusso() {
        return codiceFlusso;
    }

    /**
     * Imposta il valore della proprietà codiceFlusso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceFlusso(String value) {
        this.codiceFlusso = value;
    }

    /**
     * Recupera il valore della proprietà identificatoreProcesso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificatoreProcesso() {
        return identificatoreProcesso;
    }

    /**
     * Imposta il valore della proprietà identificatoreProcesso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificatoreProcesso(String value) {
        this.identificatoreProcesso = value;
    }

    /**
     * Recupera il valore della proprietà identificativoMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificativoMessaggio() {
        return identificativoMessaggio;
    }

    /**
     * Imposta il valore della proprietà identificativoMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificativoMessaggio(String value) {
        this.identificativoMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà chiaveFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveFascicolo() {
        return chiaveFascicolo;
    }

    /**
     * Imposta il valore della proprietà chiaveFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveFascicolo(String value) {
        this.chiaveFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà chiaveDocumentoPEC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveDocumentoPEC() {
        return chiaveDocumentoPEC;
    }

    /**
     * Imposta il valore della proprietà chiaveDocumentoPEC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveDocumentoPEC(String value) {
        this.chiaveDocumentoPEC = value;
    }

    /**
     * Recupera il valore della proprietà chiaveDocumentoNotificaPEC.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveDocumentoNotificaPEC() {
        return chiaveDocumentoNotificaPEC;
    }

    /**
     * Imposta il valore della proprietà chiaveDocumentoNotificaPEC.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveDocumentoNotificaPEC(String value) {
        this.chiaveDocumentoNotificaPEC = value;
    }

    /**
     * Recupera il valore della proprietà messaggioRicevuto.
     * 
     * @return
     *     possible object is
     *     {@link EmailMessageType }
     *     
     */
    public EmailMessageType getMessaggioRicevuto() {
        return messaggioRicevuto;
    }

    /**
     * Imposta il valore della proprietà messaggioRicevuto.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailMessageType }
     *     
     */
    public void setMessaggioRicevuto(EmailMessageType value) {
        this.messaggioRicevuto = value;
    }

    /**
     * Recupera il valore della proprietà tipoNotificaPEC.
     * 
     * @return
     *     possible object is
     *     {@link ProtRicevutePecType }
     *     
     */
    public ProtRicevutePecType getTipoNotificaPEC() {
        return tipoNotificaPEC;
    }

    /**
     * Imposta il valore della proprietà tipoNotificaPEC.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtRicevutePecType }
     *     
     */
    public void setTipoNotificaPEC(ProtRicevutePecType value) {
        this.tipoNotificaPEC = value;
    }

}
