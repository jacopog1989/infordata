
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.RegRegistrazioneAusiliariaType;


/**
 * Tipo di dato restituito a seguito di modifica di un registro ausiliario
 * 
 * <p>Java class for risposta_rimuoviApplicazioneRegistrazioneAusiliaria_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="risposta_rimuoviApplicazioneRegistrazioneAusiliaria_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsMessages}baseServiceResponse_type">
 *       &lt;sequence>
 *         &lt;element name="DatiRegistro" type="{http://mef.gov.it.v1.npsTypes}reg_registrazioneAusiliaria_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_rimuoviApplicazioneRegistrazioneAusiliaria_type", propOrder = {
    "datiRegistro"
})
public class RispostaRimuoviApplicazioneRegistrazioneAusiliariaType
    extends BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "DatiRegistro")
    protected RegRegistrazioneAusiliariaType datiRegistro;

    /**
     * Gets the value of the datiRegistro property.
     * 
     * @return
     *     possible object is
     *     {@link RegRegistrazioneAusiliariaType }
     *     
     */
    public RegRegistrazioneAusiliariaType getDatiRegistro() {
        return datiRegistro;
    }

    /**
     * Sets the value of the datiRegistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegRegistrazioneAusiliariaType }
     *     
     */
    public void setDatiRegistro(RegRegistrazioneAusiliariaType value) {
        this.datiRegistro = value;
    }

}
