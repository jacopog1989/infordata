
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.OrgOrganigrammaType;


/**
 * Tipo di dato utilizzato nella richiesta di rimozione di una annotazione da un protocollo esistente
 * 
 * <p>Classe Java per richiesta_rimuoviAnnotazioneProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_rimuoviAnnotazioneProtocollo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdentificativoProtocollo" type="{http://mef.gov.it.v1.npsMessages}identificativoProtocolloRequest_type"/>
 *         &lt;element name="Annotazione" type="{http://mef.gov.it.v1.npsMessages}identificativoAnnotazione_type"/>
 *         &lt;element name="Operatore" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_rimuoviAnnotazioneProtocollo_type", propOrder = {
    "identificativoProtocollo",
    "annotazione",
    "operatore"
})
public class RichiestaRimuoviAnnotazioneProtocolloType
    implements Serializable
{

    @XmlElement(name = "IdentificativoProtocollo", required = true)
    protected IdentificativoProtocolloRequestType identificativoProtocollo;
    @XmlElement(name = "Annotazione", required = true)
    protected IdentificativoAnnotazioneType annotazione;
    @XmlElement(name = "Operatore", required = true)
    protected OrgOrganigrammaType operatore;

    /**
     * Recupera il valore della proprietà identificativoProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoProtocolloRequestType }
     *     
     */
    public IdentificativoProtocolloRequestType getIdentificativoProtocollo() {
        return identificativoProtocollo;
    }

    /**
     * Imposta il valore della proprietà identificativoProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoProtocolloRequestType }
     *     
     */
    public void setIdentificativoProtocollo(IdentificativoProtocolloRequestType value) {
        this.identificativoProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà annotazione.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoAnnotazioneType }
     *     
     */
    public IdentificativoAnnotazioneType getAnnotazione() {
        return annotazione;
    }

    /**
     * Imposta il valore della proprietà annotazione.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoAnnotazioneType }
     *     
     */
    public void setAnnotazione(IdentificativoAnnotazioneType value) {
        this.annotazione = value;
    }

    /**
     * Recupera il valore della proprietà operatore.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getOperatore() {
        return operatore;
    }

    /**
     * Imposta il valore della proprietà operatore.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setOperatore(OrgOrganigrammaType value) {
        this.operatore = value;
    }

}
