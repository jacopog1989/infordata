
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.AtmosType;
import npstypes.v1.it.gov.mef.DocDocumentoType;
import npstypes.v1.it.gov.mef.OrgOrganigrammaType;


/**
 * Tipo di dato utilizzato nella richiesta di aggiungere un documento ad un protocollo esistente.
 * 
 * <p>Classe Java per richiesta_aggiungiDocumentoProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_aggiungiDocumentoProtocollo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Protocollo" type="{http://mef.gov.it.v1.npsMessages}identificativoProtocolloRequest_type"/>
 *         &lt;element name="IsCompleto" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element name="DocumentoEsistente">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="IdDocumento" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *                     &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="Atmos" type="{http://mef.gov.it.v1.npsTypes}AtmosType" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="Documento" type="{http://mef.gov.it.v1.npsTypes}doc_documento_type"/>
 *         &lt;/choice>
 *         &lt;element name="DaInviare" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="TipoTrattamento">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="ORIGINALE"/>
 *               &lt;enumeration value="RISULTATO_OPERAZIONE"/>
 *               &lt;enumeration value="ORIGINALE_E_RISULTATO_OPERAZIONE"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Operatore" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type"/>
 *       &lt;/sequence>
 *       &lt;attribute name="IsPrincipale">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *             &lt;enumeration value="si"/>
 *             &lt;enumeration value="no"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/attribute>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_aggiungiDocumentoProtocollo_type", propOrder = {
    "protocollo",
    "isCompleto",
    "documentoEsistente",
    "documento",
    "daInviare",
    "tipoTrattamento",
    "operatore"
})
public class RichiestaAggiungiDocumentoProtocolloType
    implements Serializable
{

    @XmlElement(name = "Protocollo", required = true)
    protected IdentificativoProtocolloRequestType protocollo;
    @XmlElement(name = "IsCompleto", defaultValue = "true")
    protected Boolean isCompleto;
    @XmlElement(name = "DocumentoEsistente")
    protected RichiestaAggiungiDocumentoProtocolloType.DocumentoEsistente documentoEsistente;
    @XmlElement(name = "Documento")
    protected DocDocumentoType documento;
    @XmlElement(name = "DaInviare", defaultValue = "true")
    protected Boolean daInviare;
    @XmlElement(name = "TipoTrattamento", required = true)
    protected String tipoTrattamento;
    @XmlElement(name = "Operatore", required = true)
    protected OrgOrganigrammaType operatore;
    @XmlAttribute(name = "IsPrincipale")
    protected String isPrincipale;

    /**
     * Recupera il valore della proprietà protocollo.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoProtocolloRequestType }
     *     
     */
    public IdentificativoProtocolloRequestType getProtocollo() {
        return protocollo;
    }

    /**
     * Imposta il valore della proprietà protocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoProtocolloRequestType }
     *     
     */
    public void setProtocollo(IdentificativoProtocolloRequestType value) {
        this.protocollo = value;
    }

    /**
     * Recupera il valore della proprietà isCompleto.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsCompleto() {
        return isCompleto;
    }

    /**
     * Imposta il valore della proprietà isCompleto.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsCompleto(Boolean value) {
        this.isCompleto = value;
    }

    /**
     * Recupera il valore della proprietà documentoEsistente.
     * 
     * @return
     *     possible object is
     *     {@link RichiestaAggiungiDocumentoProtocolloType.DocumentoEsistente }
     *     
     */
    public RichiestaAggiungiDocumentoProtocolloType.DocumentoEsistente getDocumentoEsistente() {
        return documentoEsistente;
    }

    /**
     * Imposta il valore della proprietà documentoEsistente.
     * 
     * @param value
     *     allowed object is
     *     {@link RichiestaAggiungiDocumentoProtocolloType.DocumentoEsistente }
     *     
     */
    public void setDocumentoEsistente(RichiestaAggiungiDocumentoProtocolloType.DocumentoEsistente value) {
        this.documentoEsistente = value;
    }

    /**
     * Recupera il valore della proprietà documento.
     * 
     * @return
     *     possible object is
     *     {@link DocDocumentoType }
     *     
     */
    public DocDocumentoType getDocumento() {
        return documento;
    }

    /**
     * Imposta il valore della proprietà documento.
     * 
     * @param value
     *     allowed object is
     *     {@link DocDocumentoType }
     *     
     */
    public void setDocumento(DocDocumentoType value) {
        this.documento = value;
    }

    /**
     * Recupera il valore della proprietà daInviare.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isDaInviare() {
        return daInviare;
    }

    /**
     * Imposta il valore della proprietà daInviare.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setDaInviare(Boolean value) {
        this.daInviare = value;
    }

    /**
     * Recupera il valore della proprietà tipoTrattamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoTrattamento() {
        return tipoTrattamento;
    }

    /**
     * Imposta il valore della proprietà tipoTrattamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoTrattamento(String value) {
        this.tipoTrattamento = value;
    }

    /**
     * Recupera il valore della proprietà operatore.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getOperatore() {
        return operatore;
    }

    /**
     * Imposta il valore della proprietà operatore.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setOperatore(OrgOrganigrammaType value) {
        this.operatore = value;
    }

    /**
     * Recupera il valore della proprietà isPrincipale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsPrincipale() {
        return isPrincipale;
    }

    /**
     * Imposta il valore della proprietà isPrincipale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsPrincipale(String value) {
        this.isPrincipale = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="IdDocumento" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
     *         &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Atmos" type="{http://mef.gov.it.v1.npsTypes}AtmosType" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idDocumento",
        "descrizione",
        "atmos"
    })
    public static class DocumentoEsistente
        implements Serializable
    {

        @XmlElement(name = "IdDocumento", required = true, nillable = true)
        protected String idDocumento;
        @XmlElement(name = "Descrizione", required = true)
        protected String descrizione;
        @XmlElement(name = "Atmos")
        protected AtmosType atmos;

        /**
         * Recupera il valore della proprietà idDocumento.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocumento() {
            return idDocumento;
        }

        /**
         * Imposta il valore della proprietà idDocumento.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocumento(String value) {
            this.idDocumento = value;
        }

        /**
         * Recupera il valore della proprietà descrizione.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescrizione() {
            return descrizione;
        }

        /**
         * Imposta il valore della proprietà descrizione.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescrizione(String value) {
            this.descrizione = value;
        }

        /**
         * Recupera il valore della proprietà atmos.
         * 
         * @return
         *     possible object is
         *     {@link AtmosType }
         *     
         */
        public AtmosType getAtmos() {
            return atmos;
        }

        /**
         * Imposta il valore della proprietà atmos.
         * 
         * @param value
         *     allowed object is
         *     {@link AtmosType }
         *     
         */
        public void setAtmos(AtmosType value) {
            this.atmos = value;
        }

    }

}
