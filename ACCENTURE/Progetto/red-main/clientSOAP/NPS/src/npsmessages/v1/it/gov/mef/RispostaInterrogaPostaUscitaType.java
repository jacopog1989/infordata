
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.EmailDatiPostaUscitaType;


/**
 * Tipo di dato restituito a seguito della richiesta di interrogazione delle email
 * 
 * <p>Classe Java per risposta_interrogaPostaUscita_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_interrogaPostaUscita_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsMessages}baseServiceResponse_type">
 *       &lt;sequence>
 *         &lt;element name="MessaggioPosta" type="{http://mef.gov.it.v1.npsTypes}email_datiPostaUscita_type" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_interrogaPostaUscita_type", propOrder = {
    "messaggioPosta"
})
@XmlSeeAlso({
    RispostaInterrogaPostaUscita.class
})
public class RispostaInterrogaPostaUscitaType
    extends BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "MessaggioPosta")
    protected List<EmailDatiPostaUscitaType> messaggioPosta;

    /**
     * Gets the value of the messaggioPosta property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the messaggioPosta property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMessaggioPosta().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EmailDatiPostaUscitaType }
     * 
     * 
     */
    public List<EmailDatiPostaUscitaType> getMessaggioPosta() {
        if (messaggioPosta == null) {
            messaggioPosta = new ArrayList<EmailDatiPostaUscitaType>();
        }
        return this.messaggioPosta;
    }

}
