
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.ProtProtocolloUscitaType;


/**
 * Tipo di dato utilizzato nella richiesta di protocollazione in uscita
 * 
 * <p>Classe Java per richiesta_createProtocolloUscita_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_createProtocolloUscita_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}prot_protocolloUscita_type">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_createProtocolloUscita_type")
public class RichiestaCreateProtocolloUscitaType
    extends ProtProtocolloUscitaType
    implements Serializable
{


}
