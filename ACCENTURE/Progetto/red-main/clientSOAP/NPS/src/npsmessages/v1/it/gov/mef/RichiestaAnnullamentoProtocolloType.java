
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import npstypes.v1.it.gov.mef.ProtAnnullamentoType;


/**
 * Tipo di dato utilizzato nella rihiesta di annullamento di un protocollo.
 * 
 * <p>Classe Java per richiesta_annullamentoProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_annullamentoProtocollo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdentificativoProtocollo" type="{http://mef.gov.it.v1.npsMessages}identificativoProtocolloRequest_type"/>
 *         &lt;element name="DataOperazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="DatiAnnullamento" type="{http://mef.gov.it.v1.npsTypes}prot_annullamento_type"/>
 *         &lt;element name="IdMessaggioPostaInteroperabile" type="{http://mef.gov.it.v1.npsTypes}Guid" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_annullamentoProtocollo_type", propOrder = {
    "identificativoProtocollo",
    "dataOperazione",
    "datiAnnullamento",
    "idMessaggioPostaInteroperabile"
})
public class RichiestaAnnullamentoProtocolloType
    implements Serializable
{

    @XmlElement(name = "IdentificativoProtocollo", required = true)
    protected IdentificativoProtocolloRequestType identificativoProtocollo;
    @XmlElement(name = "DataOperazione", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataOperazione;
    @XmlElement(name = "DatiAnnullamento", required = true)
    protected ProtAnnullamentoType datiAnnullamento;
    @XmlElement(name = "IdMessaggioPostaInteroperabile")
    protected String idMessaggioPostaInteroperabile;

    /**
     * Recupera il valore della proprietà identificativoProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoProtocolloRequestType }
     *     
     */
    public IdentificativoProtocolloRequestType getIdentificativoProtocollo() {
        return identificativoProtocollo;
    }

    /**
     * Imposta il valore della proprietà identificativoProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoProtocolloRequestType }
     *     
     */
    public void setIdentificativoProtocollo(IdentificativoProtocolloRequestType value) {
        this.identificativoProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà dataOperazione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataOperazione() {
        return dataOperazione;
    }

    /**
     * Imposta il valore della proprietà dataOperazione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataOperazione(XMLGregorianCalendar value) {
        this.dataOperazione = value;
    }

    /**
     * Recupera il valore della proprietà datiAnnullamento.
     * 
     * @return
     *     possible object is
     *     {@link ProtAnnullamentoType }
     *     
     */
    public ProtAnnullamentoType getDatiAnnullamento() {
        return datiAnnullamento;
    }

    /**
     * Imposta il valore della proprietà datiAnnullamento.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtAnnullamentoType }
     *     
     */
    public void setDatiAnnullamento(ProtAnnullamentoType value) {
        this.datiAnnullamento = value;
    }

    /**
     * Recupera il valore della proprietà idMessaggioPostaInteroperabile.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdMessaggioPostaInteroperabile() {
        return idMessaggioPostaInteroperabile;
    }

    /**
     * Imposta il valore della proprietà idMessaggioPostaInteroperabile.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdMessaggioPostaInteroperabile(String value) {
        this.idMessaggioPostaInteroperabile = value;
    }

}
