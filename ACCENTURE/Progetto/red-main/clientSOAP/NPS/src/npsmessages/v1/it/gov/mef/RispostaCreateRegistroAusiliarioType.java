
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.RegRegistroAusiliarioMiniType;


/**
 * Tipo di dato restituito a seguito di creazione di un registro ausiliario
 * 
 * <p>Classe Java per risposta_createRegistroAusiliario_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_createRegistroAusiliario_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsMessages}baseServiceResponse_type">
 *       &lt;sequence>
 *         &lt;element name="DatiRegistro" type="{http://mef.gov.it.v1.npsTypes}reg_registroAusiliarioMini_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_createRegistroAusiliario_type", propOrder = {
    "datiRegistro"
})
public class RispostaCreateRegistroAusiliarioType
    extends BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "DatiRegistro")
    protected RegRegistroAusiliarioMiniType datiRegistro;

    /**
     * Recupera il valore della proprietà datiRegistro.
     * 
     * @return
     *     possible object is
     *     {@link RegRegistroAusiliarioMiniType }
     *     
     */
    public RegRegistroAusiliarioMiniType getDatiRegistro() {
        return datiRegistro;
    }

    /**
     * Imposta il valore della proprietà datiRegistro.
     * 
     * @param value
     *     allowed object is
     *     {@link RegRegistroAusiliarioMiniType }
     *     
     */
    public void setDatiRegistro(RegRegistroAusiliarioMiniType value) {
        this.datiRegistro = value;
    }

}
