
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.WkfDatiFlussoType;


/**
 * Tipo di dato per richiedere l'invio della comunicazione generica
 * 
 * <p>Classe Java per richiesta_inviaComunicazioneGenerica_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_inviaComunicazioneGenerica_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}wkf_datiFlusso_type">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_inviaComunicazioneGenerica_type")
public class RichiestaInviaComunicazioneGenericaType
    extends WkfDatiFlussoType
    implements Serializable
{


}
