
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.ProtCriteriaProtocolliType;


/**
 * Definisce i dati e il numero di protocolli da resituire nella richiesta di ricerca di protocolli
 * 
 * <p>Classe Java per richiesta_ricercaProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_ricercaProtocollo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DatiRicerca">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{http://mef.gov.it.v1.npsTypes}prot_criteriaProtocolli_type">
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;sequence>
 *           &lt;element name="NumeroRisultati" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_ricercaProtocollo_type", propOrder = {
    "datiRicerca",
    "numeroRisultati"
})
public class RichiestaRicercaProtocolloType
    implements Serializable
{

    @XmlElement(name = "DatiRicerca", required = true)
    protected RichiestaRicercaProtocolloType.DatiRicerca datiRicerca;
    @XmlElement(name = "NumeroRisultati", defaultValue = "500")
    protected short numeroRisultati;

    /**
     * Recupera il valore della proprietà datiRicerca.
     * 
     * @return
     *     possible object is
     *     {@link RichiestaRicercaProtocolloType.DatiRicerca }
     *     
     */
    public RichiestaRicercaProtocolloType.DatiRicerca getDatiRicerca() {
        return datiRicerca;
    }

    /**
     * Imposta il valore della proprietà datiRicerca.
     * 
     * @param value
     *     allowed object is
     *     {@link RichiestaRicercaProtocolloType.DatiRicerca }
     *     
     */
    public void setDatiRicerca(RichiestaRicercaProtocolloType.DatiRicerca value) {
        this.datiRicerca = value;
    }

    /**
     * Recupera il valore della proprietà numeroRisultati.
     * 
     */
    public short getNumeroRisultati() {
        return numeroRisultati;
    }

    /**
     * Imposta il valore della proprietà numeroRisultati.
     * 
     */
    public void setNumeroRisultati(short value) {
        this.numeroRisultati = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}prot_criteriaProtocolli_type">
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class DatiRicerca
        extends ProtCriteriaProtocolliType
        implements Serializable
    {


    }

}
