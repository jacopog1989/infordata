
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import npstypes.v1.it.gov.mef.AnaIndirizzoTelematicoType;
import npstypes.v1.it.gov.mef.OrgOrganigrammaType;


/**
 * Tipo di dato utilizzato nella richiesta di modifica di un destinatario di un protocollo
 * 
 * <p>Classe Java per richiesta_modificaSpedizioneDestinatarioProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_modificaSpedizioneDestinatarioProtocollo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Protocollo" type="{http://mef.gov.it.v1.npsMessages}identificativoProtocolloRequest_type"/>
 *         &lt;element name="DatiDestinatario" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Destinatario" type="{http://mef.gov.it.v1.npsMessages}identificativoDestinatario_type"/>
 *                   &lt;element name="IndirizzoTelematico" type="{http://mef.gov.it.v1.npsTypes}ana_indirizzoTelematico_type" minOccurs="0"/>
 *                   &lt;element name="DataSpedizione" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="StatoSpedizione">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="DA_SPEDIRE"/>
 *                         &lt;enumeration value="IN_SPEDIZIONE"/>
 *                         &lt;enumeration value="SPEDITO"/>
 *                         &lt;enumeration value="ERRORE"/>
 *                         &lt;enumeration value="RICEVUTA_ACCETTAZIONE"/>
 *                         &lt;enumeration value="RICEVUTA_CONSEGNA"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Operatore" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_modificaSpedizioneDestinatarioProtocollo_type", propOrder = {
    "protocollo",
    "datiDestinatario",
    "operatore"
})
public class RichiestaModificaSpedizioneDestinatarioProtocolloType
    implements Serializable
{

    @XmlElement(name = "Protocollo", required = true)
    protected IdentificativoProtocolloRequestType protocollo;
    @XmlElement(name = "DatiDestinatario", required = true)
    protected List<RichiestaModificaSpedizioneDestinatarioProtocolloType.DatiDestinatario> datiDestinatario;
    @XmlElement(name = "Operatore", required = true)
    protected OrgOrganigrammaType operatore;

    /**
     * Recupera il valore della proprietà protocollo.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoProtocolloRequestType }
     *     
     */
    public IdentificativoProtocolloRequestType getProtocollo() {
        return protocollo;
    }

    /**
     * Imposta il valore della proprietà protocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoProtocolloRequestType }
     *     
     */
    public void setProtocollo(IdentificativoProtocolloRequestType value) {
        this.protocollo = value;
    }

    /**
     * Gets the value of the datiDestinatario property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the datiDestinatario property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDatiDestinatario().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RichiestaModificaSpedizioneDestinatarioProtocolloType.DatiDestinatario }
     * 
     * 
     */
    public List<RichiestaModificaSpedizioneDestinatarioProtocolloType.DatiDestinatario> getDatiDestinatario() {
        if (datiDestinatario == null) {
            datiDestinatario = new ArrayList<RichiestaModificaSpedizioneDestinatarioProtocolloType.DatiDestinatario>();
        }
        return this.datiDestinatario;
    }

    /**
     * Recupera il valore della proprietà operatore.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getOperatore() {
        return operatore;
    }

    /**
     * Imposta il valore della proprietà operatore.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setOperatore(OrgOrganigrammaType value) {
        this.operatore = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Destinatario" type="{http://mef.gov.it.v1.npsMessages}identificativoDestinatario_type"/>
     *         &lt;element name="IndirizzoTelematico" type="{http://mef.gov.it.v1.npsTypes}ana_indirizzoTelematico_type" minOccurs="0"/>
     *         &lt;element name="DataSpedizione" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="StatoSpedizione">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="DA_SPEDIRE"/>
     *               &lt;enumeration value="IN_SPEDIZIONE"/>
     *               &lt;enumeration value="SPEDITO"/>
     *               &lt;enumeration value="ERRORE"/>
     *               &lt;enumeration value="RICEVUTA_ACCETTAZIONE"/>
     *               &lt;enumeration value="RICEVUTA_CONSEGNA"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "destinatario",
        "indirizzoTelematico",
        "dataSpedizione",
        "statoSpedizione"
    })
    public static class DatiDestinatario
        implements Serializable
    {

        @XmlElement(name = "Destinatario", required = true)
        protected IdentificativoDestinatarioType destinatario;
        @XmlElement(name = "IndirizzoTelematico")
        protected AnaIndirizzoTelematicoType indirizzoTelematico;
        @XmlElement(name = "DataSpedizione")
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar dataSpedizione;
        @XmlElement(name = "StatoSpedizione", required = true)
        protected String statoSpedizione;

        /**
         * Recupera il valore della proprietà destinatario.
         * 
         * @return
         *     possible object is
         *     {@link IdentificativoDestinatarioType }
         *     
         */
        public IdentificativoDestinatarioType getDestinatario() {
            return destinatario;
        }

        /**
         * Imposta il valore della proprietà destinatario.
         * 
         * @param value
         *     allowed object is
         *     {@link IdentificativoDestinatarioType }
         *     
         */
        public void setDestinatario(IdentificativoDestinatarioType value) {
            this.destinatario = value;
        }

        /**
         * Recupera il valore della proprietà indirizzoTelematico.
         * 
         * @return
         *     possible object is
         *     {@link AnaIndirizzoTelematicoType }
         *     
         */
        public AnaIndirizzoTelematicoType getIndirizzoTelematico() {
            return indirizzoTelematico;
        }

        /**
         * Imposta il valore della proprietà indirizzoTelematico.
         * 
         * @param value
         *     allowed object is
         *     {@link AnaIndirizzoTelematicoType }
         *     
         */
        public void setIndirizzoTelematico(AnaIndirizzoTelematicoType value) {
            this.indirizzoTelematico = value;
        }

        /**
         * Recupera il valore della proprietà dataSpedizione.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDataSpedizione() {
            return dataSpedizione;
        }

        /**
         * Imposta il valore della proprietà dataSpedizione.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDataSpedizione(XMLGregorianCalendar value) {
            this.dataSpedizione = value;
        }

        /**
         * Recupera il valore della proprietà statoSpedizione.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStatoSpedizione() {
            return statoSpedizione;
        }

        /**
         * Imposta il valore della proprietà statoSpedizione.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStatoSpedizione(String value) {
            this.statoSpedizione = value;
        }

    }

}
