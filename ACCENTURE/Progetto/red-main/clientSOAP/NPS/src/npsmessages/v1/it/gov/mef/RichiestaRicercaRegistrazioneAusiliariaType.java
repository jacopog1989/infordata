
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.RegCriteriaRegistrazioneAusiliariaType;


/**
 * Definisce i dati e il numero di registri ausiliari da resituire nella richiesta di ricerca 
 * 
 * <p>Classe Java per richiesta_ricercaRegistrazioneAusiliaria_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_ricercaRegistrazioneAusiliaria_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DatiRicerca" type="{http://mef.gov.it.v1.npsTypes}reg_criteriaRegistrazioneAusiliaria_type"/>
 *         &lt;sequence>
 *           &lt;element name="NumeroRisultati" type="{http://www.w3.org/2001/XMLSchema}short"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_ricercaRegistrazioneAusiliaria_type", propOrder = {
    "datiRicerca",
    "numeroRisultati"
})
public class RichiestaRicercaRegistrazioneAusiliariaType
    implements Serializable
{

    @XmlElement(name = "DatiRicerca", required = true)
    protected RegCriteriaRegistrazioneAusiliariaType datiRicerca;
    @XmlElement(name = "NumeroRisultati", defaultValue = "500")
    protected short numeroRisultati;

    /**
     * Recupera il valore della proprietà datiRicerca.
     * 
     * @return
     *     possible object is
     *     {@link RegCriteriaRegistrazioneAusiliariaType }
     *     
     */
    public RegCriteriaRegistrazioneAusiliariaType getDatiRicerca() {
        return datiRicerca;
    }

    /**
     * Imposta il valore della proprietà datiRicerca.
     * 
     * @param value
     *     allowed object is
     *     {@link RegCriteriaRegistrazioneAusiliariaType }
     *     
     */
    public void setDatiRicerca(RegCriteriaRegistrazioneAusiliariaType value) {
        this.datiRicerca = value;
    }

    /**
     * Recupera il valore della proprietà numeroRisultati.
     * 
     */
    public short getNumeroRisultati() {
        return numeroRisultati;
    }

    /**
     * Imposta il valore della proprietà numeroRisultati.
     * 
     */
    public void setNumeroRisultati(short value) {
        this.numeroRisultati = value;
    }

}
