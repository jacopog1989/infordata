
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.DocConversioneContentType;
import npstypes.v1.it.gov.mef.DocTipoConversioneType;


/**
 * Richiesta Conversione documento sincrona
 * 
 * <p>Classe Java per richiesta_convertiDocumento_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_convertiDocumento_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TipoConversione" type="{http://mef.gov.it.v1.npsTypes}doc_tipoConversione_type"/>
 *         &lt;element name="DatiDocumento" type="{http://mef.gov.it.v1.npsTypes}doc_conversioneContent_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_convertiDocumento_type", propOrder = {
    "tipoConversione",
    "datiDocumento"
})
public class RichiestaConvertiDocumentoType
    implements Serializable
{

    @XmlElement(name = "TipoConversione", required = true)
    @XmlSchemaType(name = "string")
    protected DocTipoConversioneType tipoConversione;
    @XmlElement(name = "DatiDocumento", required = true)
    protected DocConversioneContentType datiDocumento;

    /**
     * Recupera il valore della proprietà tipoConversione.
     * 
     * @return
     *     possible object is
     *     {@link DocTipoConversioneType }
     *     
     */
    public DocTipoConversioneType getTipoConversione() {
        return tipoConversione;
    }

    /**
     * Imposta il valore della proprietà tipoConversione.
     * 
     * @param value
     *     allowed object is
     *     {@link DocTipoConversioneType }
     *     
     */
    public void setTipoConversione(DocTipoConversioneType value) {
        this.tipoConversione = value;
    }

    /**
     * Recupera il valore della proprietà datiDocumento.
     * 
     * @return
     *     possible object is
     *     {@link DocConversioneContentType }
     *     
     */
    public DocConversioneContentType getDatiDocumento() {
        return datiDocumento;
    }

    /**
     * Imposta il valore della proprietà datiDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link DocConversioneContentType }
     *     
     */
    public void setDatiDocumento(DocConversioneContentType value) {
        this.datiDocumento = value;
    }

}
