
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.ProtProtocolloGenericoResponseType;


/**
 * Tipo di dato utilizzato nella restituzione dei del protocollo generico (usato dai servizi UCB)
 * 
 * <p>Classe Java per risposta_getProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_getProtocollo_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsMessages}baseServiceResponse_type">
 *       &lt;sequence>
 *         &lt;element name="DatiProtocollo" type="{http://mef.gov.it.v1.npsTypes}prot_protocolloGenericoResponse_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_getProtocollo_type", propOrder = {
    "datiProtocollo"
})
public class RispostaGetProtocolloType
    extends BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "DatiProtocollo")
    protected ProtProtocolloGenericoResponseType datiProtocollo;

    /**
     * Recupera il valore della proprietà datiProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link ProtProtocolloGenericoResponseType }
     *     
     */
    public ProtProtocolloGenericoResponseType getDatiProtocollo() {
        return datiProtocollo;
    }

    /**
     * Imposta il valore della proprietà datiProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtProtocolloGenericoResponseType }
     *     
     */
    public void setDatiProtocollo(ProtProtocolloGenericoResponseType value) {
        this.datiProtocollo = value;
    }

}
