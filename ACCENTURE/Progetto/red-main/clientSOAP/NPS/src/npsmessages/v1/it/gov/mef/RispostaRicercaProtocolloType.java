
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.ProtRisultatoRicercaProtocolloType;


/**
 * Tipo utilizzato per la risposta della ricerca dei protocolli
 * 
 * <p>Classe Java per risposta_ricercaProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_ricercaProtocollo_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsMessages}baseServiceResponse_type">
 *       &lt;sequence>
 *         &lt;element name="DatiRicerca" type="{http://mef.gov.it.v1.npsTypes}prot_risultatoRicercaProtocollo_type" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_ricercaProtocollo_type", propOrder = {
    "datiRicerca"
})
public class RispostaRicercaProtocolloType
    extends BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "DatiRicerca")
    protected List<ProtRisultatoRicercaProtocolloType> datiRicerca;

    /**
     * Gets the value of the datiRicerca property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the datiRicerca property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDatiRicerca().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtRisultatoRicercaProtocolloType }
     * 
     * 
     */
    public List<ProtRisultatoRicercaProtocolloType> getDatiRicerca() {
        if (datiRicerca == null) {
            datiRicerca = new ArrayList<ProtRisultatoRicercaProtocolloType>();
        }
        return this.datiRicerca;
    }

}
