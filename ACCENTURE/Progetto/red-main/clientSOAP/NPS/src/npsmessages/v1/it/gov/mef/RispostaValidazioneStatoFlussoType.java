
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.AllCodiceDescrizioneType;


/**
 * Tipo di dato restituito a seguito della richiesta per la validazione del flusso
 * 
 * <p>Classe Java per risposta_validazioneStato_Flusso_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_validazioneStato_Flusso_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsMessages}baseServiceResponse_type">
 *       &lt;sequence>
 *         &lt;element name="Errore" type="{http://mef.gov.it.v1.npsTypes}all_codiceDescrizione_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_validazioneStato_Flusso_type", propOrder = {
    "errore"
})
@XmlSeeAlso({
    RispostaValidazioneStatoFlusso.class
})
public class RispostaValidazioneStatoFlussoType
    extends BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "Errore")
    protected AllCodiceDescrizioneType errore;

    /**
     * Recupera il valore della proprietà errore.
     * 
     * @return
     *     possible object is
     *     {@link AllCodiceDescrizioneType }
     *     
     */
    public AllCodiceDescrizioneType getErrore() {
        return errore;
    }

    /**
     * Imposta il valore della proprietà errore.
     * 
     * @param value
     *     allowed object is
     *     {@link AllCodiceDescrizioneType }
     *     
     */
    public void setErrore(AllCodiceDescrizioneType value) {
        this.errore = value;
    }

}
