
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.AllChiaveEsternaType;
import npstypes.v1.it.gov.mef.OrgOrganigrammaType;


/**
 * identificativo dell'assegnatario per le request
 * 
 * <p>Classe Java per identificativoAssegnatario_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="identificativoAssegnatario_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="Assegnatario" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type"/>
 *         &lt;element name="ChiaveEsterna" type="{http://mef.gov.it.v1.npsTypes}all_chiaveEsterna_type"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "identificativoAssegnatario_type", propOrder = {
    "assegnatario",
    "chiaveEsterna"
})
public class IdentificativoAssegnatarioType
    implements Serializable
{

    @XmlElement(name = "Assegnatario")
    protected OrgOrganigrammaType assegnatario;
    @XmlElement(name = "ChiaveEsterna")
    protected AllChiaveEsternaType chiaveEsterna;

    /**
     * Recupera il valore della proprietà assegnatario.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getAssegnatario() {
        return assegnatario;
    }

    /**
     * Imposta il valore della proprietà assegnatario.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setAssegnatario(OrgOrganigrammaType value) {
        this.assegnatario = value;
    }

    /**
     * Recupera il valore della proprietà chiaveEsterna.
     * 
     * @return
     *     possible object is
     *     {@link AllChiaveEsternaType }
     *     
     */
    public AllChiaveEsternaType getChiaveEsterna() {
        return chiaveEsterna;
    }

    /**
     * Imposta il valore della proprietà chiaveEsterna.
     * 
     * @param value
     *     allowed object is
     *     {@link AllChiaveEsternaType }
     *     
     */
    public void setChiaveEsterna(AllChiaveEsternaType value) {
        this.chiaveEsterna = value;
    }

}
