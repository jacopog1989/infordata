
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.WkfAcquisisciDocumentoType;


/**
 * Tipo di dato per richiedere l'acquisizione di uno o più documenti dal documentale esterno
 * 
 * <p>Classe Java per richiesta_acquisisciDocumento_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_acquisisciDocumento_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdMessaggio" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="Elenco" type="{http://mef.gov.it.v1.npsTypes}wkf_acquisisciDocumentoType" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_acquisisciDocumento_type", propOrder = {
    "idMessaggio",
    "elenco"
})
public class RichiestaAcquisisciDocumentoType
    implements Serializable
{

    @XmlElement(name = "IdMessaggio", required = true)
    protected String idMessaggio;
    @XmlElement(name = "Elenco", required = true)
    protected List<WkfAcquisisciDocumentoType> elenco;

    /**
     * Recupera il valore della proprietà idMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdMessaggio() {
        return idMessaggio;
    }

    /**
     * Imposta il valore della proprietà idMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdMessaggio(String value) {
        this.idMessaggio = value;
    }

    /**
     * Gets the value of the elenco property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elenco property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getElenco().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WkfAcquisisciDocumentoType }
     * 
     * 
     */
    public List<WkfAcquisisciDocumentoType> getElenco() {
        if (elenco == null) {
            elenco = new ArrayList<WkfAcquisisciDocumentoType>();
        }
        return this.elenco;
    }

}
