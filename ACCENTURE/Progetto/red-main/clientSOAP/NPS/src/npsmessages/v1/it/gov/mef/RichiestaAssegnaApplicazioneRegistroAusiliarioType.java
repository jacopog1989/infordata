
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.OrgOrganigrammaType;
import npstypes.v1.it.gov.mef.RegIdentificatoreRegistroAusiliarioType;


/**
 * Tipo di dato per richiedere l'assegnazione ad una o più applicazioni del registro
 * 
 * <p>Classe Java per richiesta_assegnaApplicazioneRegistroAusiliario_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_assegnaApplicazioneRegistroAusiliario_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Registro" type="{http://mef.gov.it.v1.npsTypes}reg_identificatoreRegistroAusiliario_type"/>
 *         &lt;element name="ApplicazioniAssociate" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
 *         &lt;element name="Operatore" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_assegnaApplicazioneRegistroAusiliario_type", propOrder = {
    "registro",
    "applicazioniAssociate",
    "operatore"
})
public class RichiestaAssegnaApplicazioneRegistroAusiliarioType
    implements Serializable
{

    @XmlElement(name = "Registro", required = true)
    protected RegIdentificatoreRegistroAusiliarioType registro;
    @XmlElement(name = "ApplicazioniAssociate", required = true)
    protected List<String> applicazioniAssociate;
    @XmlElement(name = "Operatore", required = true)
    protected OrgOrganigrammaType operatore;

    /**
     * Recupera il valore della proprietà registro.
     * 
     * @return
     *     possible object is
     *     {@link RegIdentificatoreRegistroAusiliarioType }
     *     
     */
    public RegIdentificatoreRegistroAusiliarioType getRegistro() {
        return registro;
    }

    /**
     * Imposta il valore della proprietà registro.
     * 
     * @param value
     *     allowed object is
     *     {@link RegIdentificatoreRegistroAusiliarioType }
     *     
     */
    public void setRegistro(RegIdentificatoreRegistroAusiliarioType value) {
        this.registro = value;
    }

    /**
     * Gets the value of the applicazioniAssociate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the applicazioniAssociate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getApplicazioniAssociate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getApplicazioniAssociate() {
        if (applicazioniAssociate == null) {
            applicazioniAssociate = new ArrayList<String>();
        }
        return this.applicazioniAssociate;
    }

    /**
     * Recupera il valore della proprietà operatore.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getOperatore() {
        return operatore;
    }

    /**
     * Imposta il valore della proprietà operatore.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setOperatore(OrgOrganigrammaType value) {
        this.operatore = value;
    }

}
