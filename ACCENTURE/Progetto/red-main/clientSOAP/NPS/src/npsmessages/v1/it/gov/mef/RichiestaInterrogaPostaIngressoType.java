
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.AllDataRangeType;
import npstypes.v1.it.gov.mef.EmailIndirizzoEmailType;
import npstypes.v1.it.gov.mef.EmailTipoDettaglioEstrazioneType;
import npstypes.v1.it.gov.mef.OrgCasellaEmailType;
import npstypes.v1.it.gov.mef.ProtIdentificatoreProtocolloType;


/**
 * Criteri di filtro per l'estrazione dei messaggi ricevuti in ingresso
 * 
 * <p>Classe Java per richiesta_interrogaPostaIngresso_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_interrogaPostaIngresso_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CasellaEmail" type="{http://mef.gov.it.v1.npsTypes}org_casellaEmail_type"/>
 *         &lt;element name="DataRicezioneMessaggio" type="{http://mef.gov.it.v1.npsTypes}all_dataRange_type"/>
 *         &lt;element name="DataMessaggio" type="{http://mef.gov.it.v1.npsTypes}all_dataRange_type" minOccurs="0"/>
 *         &lt;element name="TipoMessaggio">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="GENERICO_EMAIL"/>
 *               &lt;enumeration value="GENERICO_FLUSSO"/>
 *               &lt;enumeration value="SEGNATURA"/>
 *               &lt;enumeration value="SEGNATURA_CONFERMA_RICEZIONE"/>
 *               &lt;enumeration value="SEGNATURA_NOTIFICA_ECCEZIONE"/>
 *               &lt;enumeration value="SEGNATURA_AGGIORNAMENTO_CONFERMA"/>
 *               &lt;enumeration value="SEGNATURA_ANNULLAMENTO_PROTOCOLLAZIONE"/>
 *               &lt;enumeration value="PEC_RICEVUTA"/>
 *               &lt;enumeration value="PEC_ACCETTAZIONE"/>
 *               &lt;enumeration value="PEC_NON_ACCETTAZIONE"/>
 *               &lt;enumeration value="PEC_PRESA_IN_CARICO"/>
 *               &lt;enumeration value="PEC_AVVENUTA_CONSEGNA"/>
 *               &lt;enumeration value="PEC_POSTA_CERTIFICATA"/>
 *               &lt;enumeration value="PEC_ERRORE_CONSEGNA"/>
 *               &lt;enumeration value="PEC_PREAVVISO_ERRORE_CONSEGNA"/>
 *               &lt;enumeration value="PEC_ANOMALIA_MESSAGGIO"/>
 *               &lt;enumeration value="PEC_RILEVAZIONE_VIRUS"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="OggettoMessaggio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CorpoMessaggio" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="Mittente" type="{http://mef.gov.it.v1.npsTypes}email_indirizzoEmail_type" minOccurs="0"/>
 *         &lt;element name="SistemaProduttore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdMessaggio" type="{http://mef.gov.it.v1.npsTypes}Guid" minOccurs="0"/>
 *         &lt;element name="CodiMessaggio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodiceFlusso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Protocollo" type="{http://mef.gov.it.v1.npsTypes}prot_identificatoreProtocollo_type" minOccurs="0"/>
 *         &lt;element name="TipoDettaglioElenco" type="{http://mef.gov.it.v1.npsTypes}email_tipoDettaglioEstrazione_type" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_interrogaPostaIngresso_type", propOrder = {
    "casellaEmail",
    "dataRicezioneMessaggio",
    "dataMessaggio",
    "tipoMessaggio",
    "oggettoMessaggio",
    "corpoMessaggio",
    "mittente",
    "sistemaProduttore",
    "idMessaggio",
    "codiMessaggio",
    "codiceFlusso",
    "protocollo",
    "tipoDettaglioElenco"
})
public class RichiestaInterrogaPostaIngressoType
    implements Serializable
{

    @XmlElement(name = "CasellaEmail", required = true)
    protected OrgCasellaEmailType casellaEmail;
    @XmlElement(name = "DataRicezioneMessaggio", required = true)
    protected AllDataRangeType dataRicezioneMessaggio;
    @XmlElement(name = "DataMessaggio")
    protected AllDataRangeType dataMessaggio;
    @XmlElement(name = "TipoMessaggio", required = true)
    protected String tipoMessaggio;
    @XmlElement(name = "OggettoMessaggio")
    protected String oggettoMessaggio;
    @XmlElement(name = "CorpoMessaggio")
    protected Object corpoMessaggio;
    @XmlElement(name = "Mittente")
    protected EmailIndirizzoEmailType mittente;
    @XmlElement(name = "SistemaProduttore", required = true)
    protected String sistemaProduttore;
    @XmlElement(name = "IdMessaggio")
    protected String idMessaggio;
    @XmlElement(name = "CodiMessaggio")
    protected String codiMessaggio;
    @XmlElement(name = "CodiceFlusso")
    protected String codiceFlusso;
    @XmlElement(name = "Protocollo")
    protected ProtIdentificatoreProtocolloType protocollo;
    @XmlElement(name = "TipoDettaglioElenco", required = true)
    @XmlSchemaType(name = "string")
    protected List<EmailTipoDettaglioEstrazioneType> tipoDettaglioElenco;

    /**
     * Recupera il valore della proprietà casellaEmail.
     * 
     * @return
     *     possible object is
     *     {@link OrgCasellaEmailType }
     *     
     */
    public OrgCasellaEmailType getCasellaEmail() {
        return casellaEmail;
    }

    /**
     * Imposta il valore della proprietà casellaEmail.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgCasellaEmailType }
     *     
     */
    public void setCasellaEmail(OrgCasellaEmailType value) {
        this.casellaEmail = value;
    }

    /**
     * Recupera il valore della proprietà dataRicezioneMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link AllDataRangeType }
     *     
     */
    public AllDataRangeType getDataRicezioneMessaggio() {
        return dataRicezioneMessaggio;
    }

    /**
     * Imposta il valore della proprietà dataRicezioneMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link AllDataRangeType }
     *     
     */
    public void setDataRicezioneMessaggio(AllDataRangeType value) {
        this.dataRicezioneMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà dataMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link AllDataRangeType }
     *     
     */
    public AllDataRangeType getDataMessaggio() {
        return dataMessaggio;
    }

    /**
     * Imposta il valore della proprietà dataMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link AllDataRangeType }
     *     
     */
    public void setDataMessaggio(AllDataRangeType value) {
        this.dataMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà tipoMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoMessaggio() {
        return tipoMessaggio;
    }

    /**
     * Imposta il valore della proprietà tipoMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoMessaggio(String value) {
        this.tipoMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà oggettoMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggettoMessaggio() {
        return oggettoMessaggio;
    }

    /**
     * Imposta il valore della proprietà oggettoMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggettoMessaggio(String value) {
        this.oggettoMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà corpoMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getCorpoMessaggio() {
        return corpoMessaggio;
    }

    /**
     * Imposta il valore della proprietà corpoMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setCorpoMessaggio(Object value) {
        this.corpoMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà mittente.
     * 
     * @return
     *     possible object is
     *     {@link EmailIndirizzoEmailType }
     *     
     */
    public EmailIndirizzoEmailType getMittente() {
        return mittente;
    }

    /**
     * Imposta il valore della proprietà mittente.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailIndirizzoEmailType }
     *     
     */
    public void setMittente(EmailIndirizzoEmailType value) {
        this.mittente = value;
    }

    /**
     * Recupera il valore della proprietà sistemaProduttore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaProduttore() {
        return sistemaProduttore;
    }

    /**
     * Imposta il valore della proprietà sistemaProduttore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaProduttore(String value) {
        this.sistemaProduttore = value;
    }

    /**
     * Recupera il valore della proprietà idMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdMessaggio() {
        return idMessaggio;
    }

    /**
     * Imposta il valore della proprietà idMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdMessaggio(String value) {
        this.idMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà codiMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiMessaggio() {
        return codiMessaggio;
    }

    /**
     * Imposta il valore della proprietà codiMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiMessaggio(String value) {
        this.codiMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà codiceFlusso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceFlusso() {
        return codiceFlusso;
    }

    /**
     * Imposta il valore della proprietà codiceFlusso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceFlusso(String value) {
        this.codiceFlusso = value;
    }

    /**
     * Recupera il valore della proprietà protocollo.
     * 
     * @return
     *     possible object is
     *     {@link ProtIdentificatoreProtocolloType }
     *     
     */
    public ProtIdentificatoreProtocolloType getProtocollo() {
        return protocollo;
    }

    /**
     * Imposta il valore della proprietà protocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtIdentificatoreProtocolloType }
     *     
     */
    public void setProtocollo(ProtIdentificatoreProtocolloType value) {
        this.protocollo = value;
    }

    /**
     * Gets the value of the tipoDettaglioElenco property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tipoDettaglioElenco property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTipoDettaglioElenco().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EmailTipoDettaglioEstrazioneType }
     * 
     * 
     */
    public List<EmailTipoDettaglioEstrazioneType> getTipoDettaglioElenco() {
        if (tipoDettaglioElenco == null) {
            tipoDettaglioElenco = new ArrayList<EmailTipoDettaglioEstrazioneType>();
        }
        return this.tipoDettaglioElenco;
    }

}
