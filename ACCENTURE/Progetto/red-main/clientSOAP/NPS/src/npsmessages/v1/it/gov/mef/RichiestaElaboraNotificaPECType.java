
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.WkfDatiFlussoNotificaType;


/**
 * Tipo di dato per richiedere la notifica PEC ricevuta
 * 
 * <p>Java class for richiesta_elaboraNotifica_PEC_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_elaboraNotifica_PEC_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}wkf_datiFlussoNotifica_type">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_elaboraNotifica_PEC_type")
public class RichiestaElaboraNotificaPECType
    extends WkfDatiFlussoNotificaType
    implements Serializable
{


}
