
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.WkfDatiMiminiType;


/**
 * Tipo di dato restituito a seguito di un invio della documentazione generica
 * 
 * <p>Classe Java per risposta_inviaDocumentazioneGenerica_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_inviaDocumentazioneGenerica_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsMessages}baseServiceResponse_type">
 *       &lt;sequence>
 *         &lt;element name="DatiFlusso" type="{http://mef.gov.it.v1.npsTypes}wkf_datiMimini_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_inviaDocumentazioneGenerica_type", propOrder = {
    "datiFlusso"
})
public class RispostaInviaDocumentazioneGenericaType
    extends BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "DatiFlusso")
    protected WkfDatiMiminiType datiFlusso;

    /**
     * Recupera il valore della proprietà datiFlusso.
     * 
     * @return
     *     possible object is
     *     {@link WkfDatiMiminiType }
     *     
     */
    public WkfDatiMiminiType getDatiFlusso() {
        return datiFlusso;
    }

    /**
     * Imposta il valore della proprietà datiFlusso.
     * 
     * @param value
     *     allowed object is
     *     {@link WkfDatiMiminiType }
     *     
     */
    public void setDatiFlusso(WkfDatiMiminiType value) {
        this.datiFlusso = value;
    }

}
