
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.EmailIndirizzoEmailType;


/**
 * Tipo di dato per richiedere l'invio delle peo per conoscenza
 * 
 * <p>Java class for richiesta_inviaPeoConoscenza_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_inviaPeoConoscenza_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdMessaggio" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="CodiceFlusso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Messaggio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CorpoMessaggio" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *         &lt;element name="DestinatarioEmail" type="{http://mef.gov.it.v1.npsTypes}email_indirizzoEmail_type" maxOccurs="unbounded"/>
 *         &lt;element name="Allegati" type="{http://mef.gov.it.v1.npsTypes}Guid" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_inviaPeoConoscenza_type", propOrder = {
    "idMessaggio",
    "codiceFlusso",
    "messaggio",
    "corpoMessaggio",
    "destinatarioEmail",
    "allegati"
})
public class RichiestaInviaPeoConoscenzaType
    implements Serializable
{

    @XmlElement(name = "IdMessaggio", required = true)
    protected String idMessaggio;
    @XmlElement(name = "CodiceFlusso", required = true)
    protected String codiceFlusso;
    @XmlElement(name = "Messaggio", required = true)
    protected String messaggio;
    @XmlElement(name = "CorpoMessaggio", required = true)
    protected Object corpoMessaggio;
    @XmlElement(name = "DestinatarioEmail", required = true)
    protected List<EmailIndirizzoEmailType> destinatarioEmail;
    @XmlElement(name = "Allegati")
    protected List<String> allegati;

    /**
     * Gets the value of the idMessaggio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdMessaggio() {
        return idMessaggio;
    }

    /**
     * Sets the value of the idMessaggio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdMessaggio(String value) {
        this.idMessaggio = value;
    }

    /**
     * Gets the value of the codiceFlusso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceFlusso() {
        return codiceFlusso;
    }

    /**
     * Sets the value of the codiceFlusso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceFlusso(String value) {
        this.codiceFlusso = value;
    }

    /**
     * Gets the value of the messaggio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessaggio() {
        return messaggio;
    }

    /**
     * Sets the value of the messaggio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessaggio(String value) {
        this.messaggio = value;
    }

    /**
     * Gets the value of the corpoMessaggio property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getCorpoMessaggio() {
        return corpoMessaggio;
    }

    /**
     * Sets the value of the corpoMessaggio property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setCorpoMessaggio(Object value) {
        this.corpoMessaggio = value;
    }

    /**
     * Gets the value of the destinatarioEmail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the destinatarioEmail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDestinatarioEmail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EmailIndirizzoEmailType }
     * 
     * 
     */
    public List<EmailIndirizzoEmailType> getDestinatarioEmail() {
        if (destinatarioEmail == null) {
            destinatarioEmail = new ArrayList<EmailIndirizzoEmailType>();
        }
        return this.destinatarioEmail;
    }

    /**
     * Gets the value of the allegati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allegati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllegati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getAllegati() {
        if (allegati == null) {
            allegati = new ArrayList<String>();
        }
        return this.allegati;
    }

}
