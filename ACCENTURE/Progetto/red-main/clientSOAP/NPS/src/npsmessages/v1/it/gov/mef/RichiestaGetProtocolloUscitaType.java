
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.ProtTipoDettaglioEstrazioneType;


/**
 * Tipo di dato utilizzato per la richiesta di un protocollo in uscita
 * 
 * <p>Classe Java per richiesta_getProtocolloUscita_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_getProtocolloUscita_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsMessages}identificativoProtocolloRequest_type">
 *       &lt;sequence>
 *         &lt;element name="TipoDettaglioElenco" type="{http://mef.gov.it.v1.npsTypes}prot_tipoDettaglioEstrazione_type" maxOccurs="unbounded"/>
 *         &lt;element name="Acl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_getProtocolloUscita_type", propOrder = {
    "tipoDettaglioElenco",
    "acl"
})
@XmlSeeAlso({
    RichiestaGetProtocolloUscita.class
})
public class RichiestaGetProtocolloUscitaType
    extends IdentificativoProtocolloRequestType
    implements Serializable
{

    @XmlElement(name = "TipoDettaglioElenco", required = true)
    @XmlSchemaType(name = "string")
    protected List<ProtTipoDettaglioEstrazioneType> tipoDettaglioElenco;
    @XmlElement(name = "Acl")
    protected String acl;
    
    /**
     * Gets the value of the tipoDettaglioElenco property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tipoDettaglioElenco property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTipoDettaglioElenco().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtTipoDettaglioEstrazioneType }
     * 
     * 
     */
    public List<ProtTipoDettaglioEstrazioneType> getTipoDettaglioElenco() {
        if (tipoDettaglioElenco == null) {
            tipoDettaglioElenco = new ArrayList<ProtTipoDettaglioEstrazioneType>();
        }
        return this.tipoDettaglioElenco;
    }
    
    /**
     * Recupera il valore della proprietà acl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcl() {
        return acl;
    }

    /**
     * Imposta il valore della proprietà acl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcl(String value) {
        this.acl = value;
    }

}
