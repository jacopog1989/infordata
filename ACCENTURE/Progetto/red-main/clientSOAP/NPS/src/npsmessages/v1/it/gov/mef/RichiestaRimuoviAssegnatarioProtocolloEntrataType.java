
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.OrgOrganigrammaType;


/**
 * Tipo di dato utilizzato nella richiesta di rimozione di un assegnatario da un protocollo esistente
 * 
 * <p>Classe Java per richiesta_rimuoviAssegnatarioProtocolloEntrata_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_rimuoviAssegnatarioProtocolloEntrata_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Protocollo" type="{http://mef.gov.it.v1.npsMessages}identificativoProtocolloRequest_type"/>
 *         &lt;element name="DatiAssegnatario" type="{http://mef.gov.it.v1.npsMessages}identificativoAssegnatario_type"/>
 *         &lt;element name="Operatore" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type"/>
 *         &lt;element name="Acl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Applicazione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_rimuoviAssegnatarioProtocolloEntrata_type", propOrder = {
    "protocollo",
    "datiAssegnatario",
    "operatore",
    "acl",
    "applicazione"
})
public class RichiestaRimuoviAssegnatarioProtocolloEntrataType
    implements Serializable
{

    @XmlElement(name = "Protocollo", required = true)
    protected IdentificativoProtocolloRequestType protocollo;
    @XmlElement(name = "DatiAssegnatario", required = true)
    protected IdentificativoAssegnatarioType datiAssegnatario;
    @XmlElement(name = "Operatore", required = true)
    protected OrgOrganigrammaType operatore;
    @XmlElement(name = "Acl")
    protected String acl;
    @XmlElement(name = "Applicazione", required = true)
    protected String applicazione;

    /**
     * Recupera il valore della proprietà protocollo.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoProtocolloRequestType }
     *     
     */
    public IdentificativoProtocolloRequestType getProtocollo() {
        return protocollo;
    }

    /**
     * Imposta il valore della proprietà protocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoProtocolloRequestType }
     *     
     */
    public void setProtocollo(IdentificativoProtocolloRequestType value) {
        this.protocollo = value;
    }

    /**
     * Recupera il valore della proprietà datiAssegnatario.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoAssegnatarioType }
     *     
     */
    public IdentificativoAssegnatarioType getDatiAssegnatario() {
        return datiAssegnatario;
    }

    /**
     * Imposta il valore della proprietà datiAssegnatario.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoAssegnatarioType }
     *     
     */
    public void setDatiAssegnatario(IdentificativoAssegnatarioType value) {
        this.datiAssegnatario = value;
    }

    /**
     * Recupera il valore della proprietà operatore.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getOperatore() {
        return operatore;
    }

    /**
     * Imposta il valore della proprietà operatore.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setOperatore(OrgOrganigrammaType value) {
        this.operatore = value;
    }

    /**
     * Recupera il valore della proprietà acl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcl() {
        return acl;
    }

    /**
     * Imposta il valore della proprietà acl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcl(String value) {
        this.acl = value;
    }

    /**
     * Recupera il valore della proprietà applicazione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicazione() {
        return applicazione;
    }

    /**
     * Imposta il valore della proprietà applicazione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicazione(String value) {
        this.applicazione = value;
    }

}
