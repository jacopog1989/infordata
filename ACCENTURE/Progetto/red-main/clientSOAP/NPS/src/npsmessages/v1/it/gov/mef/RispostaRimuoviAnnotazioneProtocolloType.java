
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Tipo di dato utilizzato nella risposta di rimozione di una annotazione da un protocollo
 * 
 * <p>Classe Java per risposta_rimuoviAnnotazioneProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_rimuoviAnnotazioneProtocollo_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsMessages}risposta_updateDatiProtocollo_type">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_rimuoviAnnotazioneProtocollo_type")
public class RispostaRimuoviAnnotazioneProtocolloType
    extends RispostaUpdateDatiProtocolloType
    implements Serializable
{


}
