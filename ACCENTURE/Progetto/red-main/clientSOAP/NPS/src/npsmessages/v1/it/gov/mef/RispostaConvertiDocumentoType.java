
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.DocConversioneContentType;


/**
 * Content del Documento Allegato Decreto IGB
 * 
 * <p>Classe Java per risposta_convertiDocumento_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_convertiDocumento_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsMessages}baseServiceResponse_type">
 *       &lt;sequence>
 *         &lt;element name="Document" type="{http://mef.gov.it.v1.npsTypes}doc_conversioneContent_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_convertiDocumento_type", propOrder = {
    "document"
})
public class RispostaConvertiDocumentoType
    extends BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "Document")
    protected DocConversioneContentType document;

    /**
     * Recupera il valore della proprietà document.
     * 
     * @return
     *     possible object is
     *     {@link DocConversioneContentType }
     *     
     */
    public DocConversioneContentType getDocument() {
        return document;
    }

    /**
     * Imposta il valore della proprietà document.
     * 
     * @param value
     *     allowed object is
     *     {@link DocConversioneContentType }
     *     
     */
    public void setDocument(DocConversioneContentType value) {
        this.document = value;
    }

}
