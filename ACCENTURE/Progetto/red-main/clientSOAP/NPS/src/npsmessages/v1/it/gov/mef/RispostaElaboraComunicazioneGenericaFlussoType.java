
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Tipo di dato restituito a seguito della richiesta l'elaborazione della comunicazione generica
 * 
 * <p>Java class for risposta_elaboraComunicazioneGenerica_Flusso_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="risposta_elaboraComunicazioneGenerica_Flusso_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsMessages}baseServiceResponse_type">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_elaboraComunicazioneGenerica_Flusso_type")
public class RispostaElaboraComunicazioneGenericaFlussoType
    extends BaseServiceResponseType
    implements Serializable
{


}
