
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Tipo di dato utilizzato nella risposta di timbro di un documento
 * 
 * <p>Classe Java per risposta_timbroDocumento_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_timbroDocumento_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsMessages}baseServiceResponse_type">
 *       &lt;sequence>
 *         &lt;element name="DatiDocumento" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="FileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="MIMEType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Hash" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *                   &lt;element name="Content" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_timbroDocumento_type", propOrder = {
    "datiDocumento"
})
public class RispostaTimbroDocumentoType
    extends BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "DatiDocumento")
    protected RispostaTimbroDocumentoType.DatiDocumento datiDocumento;

    /**
     * Recupera il valore della proprietà datiDocumento.
     * 
     * @return
     *     possible object is
     *     {@link RispostaTimbroDocumentoType.DatiDocumento }
     *     
     */
    public RispostaTimbroDocumentoType.DatiDocumento getDatiDocumento() {
        return datiDocumento;
    }

    /**
     * Imposta il valore della proprietà datiDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link RispostaTimbroDocumentoType.DatiDocumento }
     *     
     */
    public void setDatiDocumento(RispostaTimbroDocumentoType.DatiDocumento value) {
        this.datiDocumento = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="FileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="MIMEType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Hash" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
     *         &lt;element name="Content" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fileName",
        "mimeType",
        "hash",
        "content"
    })
    public static class DatiDocumento
        implements Serializable
    {

        @XmlElement(name = "FileName", required = true, nillable = true)
        protected String fileName;
        @XmlElement(name = "MIMEType", required = true)
        protected String mimeType;
        @XmlElement(name = "Hash", required = true, nillable = true)
        protected byte[] hash;
        @XmlElement(name = "Content", required = true)
        protected byte[] content;

        /**
         * Recupera il valore della proprietà fileName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFileName() {
            return fileName;
        }

        /**
         * Imposta il valore della proprietà fileName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFileName(String value) {
            this.fileName = value;
        }

        /**
         * Recupera il valore della proprietà mimeType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMIMEType() {
            return mimeType;
        }

        /**
         * Imposta il valore della proprietà mimeType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMIMEType(String value) {
            this.mimeType = value;
        }

        /**
         * Recupera il valore della proprietà hash.
         * 
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getHash() {
            return hash;
        }

        /**
         * Imposta il valore della proprietà hash.
         * 
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setHash(byte[] value) {
            this.hash = value;
        }

        /**
         * Recupera il valore della proprietà content.
         * 
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getContent() {
            return content;
        }

        /**
         * Imposta il valore della proprietà content.
         * 
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setContent(byte[] value) {
            this.content = value;
        }

    }

}
