
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Tipo di dato restituito a seguito della richiesta l'invio delle peo per conoscenza
 * 
 * <p>Java class for risposta_inviaPeoConoscenza_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="risposta_inviaPeoConoscenza_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsMessages}baseServiceResponse_type">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_inviaPeoConoscenza_type")
@XmlSeeAlso({
    RispostaInviaPeoConoscenza.class
})
public class RispostaInviaPeoConoscenzaType
    extends BaseServiceResponseType
    implements Serializable
{


}
