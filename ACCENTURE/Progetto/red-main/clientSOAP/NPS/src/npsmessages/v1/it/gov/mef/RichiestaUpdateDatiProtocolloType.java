
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import it.gov.digitpa.protocollo.Classifica;
import it.gov.digitpa.protocollo.InterventoOperatore;
import it.gov.digitpa.protocollo.Note;
import it.gov.digitpa.protocollo.PiuInfo;
import it.gov.digitpa.protocollo.PrimaRegistrazione;
import it.gov.digitpa.protocollo.Riferimenti;
import it.gov.digitpa.protocollo.RiferimentiTelematici;
import it.gov.digitpa.protocollo.RiferimentoDocumentiCartacei;
import npstypes.v1.it.gov.mef.AllCodiceDescrizioneType;
import npstypes.v1.it.gov.mef.OrgOrganigrammaType;
import npstypes.v1.it.gov.mef.ProtAttributiEstesiType;
import npstypes.v1.it.gov.mef.ProtMittenteType;


/**
 * Tipo di dato utilizzato nella richiesta di aggiornamento di un protocollo
 * 
 * <p>Classe Java per richiesta_updateDatiProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_updateDatiProtocollo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdentificativoProtocollo" type="{http://mef.gov.it.v1.npsMessages}identificativoProtocolloRequest_type"/>
 *         &lt;element name="DataOperazione" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="DatiModifica">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Mittente" type="{http://mef.gov.it.v1.npsTypes}prot_mittente_type" minOccurs="0"/>
 *                   &lt;element name="Oggetto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="TipologiaDocumento" type="{http://mef.gov.it.v1.npsTypes}prot_attributiEstesi_type" minOccurs="0"/>
 *                   &lt;element name="Acl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="IsRiservato" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                   &lt;element name="IsCompleto" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                   &lt;element name="SegnaturaEstesa" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="Intestazione" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element ref="{http://www.digitPa.gov.it/protocollo/}PrimaRegistrazione" minOccurs="0"/>
 *                                       &lt;element ref="{http://www.digitPa.gov.it/protocollo/}InterventoOperatore" minOccurs="0"/>
 *                                       &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentoDocumentiCartacei" minOccurs="0"/>
 *                                       &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentiTelematici" minOccurs="0"/>
 *                                       &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Classifica" minOccurs="0"/>
 *                                       &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Note" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Riferimenti" minOccurs="0"/>
 *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}PiuInfo" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="VoceTitolario" type="{http://mef.gov.it.v1.npsTypes}all_codiceDescrizione_type" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Operatore" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_updateDatiProtocollo_type", propOrder = {
    "identificativoProtocollo",
    "dataOperazione",
    "datiModifica",
    "operatore"
})
public class RichiestaUpdateDatiProtocolloType
    implements Serializable
{

    @XmlElement(name = "IdentificativoProtocollo", required = true)
    protected IdentificativoProtocolloRequestType identificativoProtocollo;
    @XmlElement(name = "DataOperazione", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataOperazione;
    @XmlElement(name = "DatiModifica", required = true)
    protected RichiestaUpdateDatiProtocolloType.DatiModifica datiModifica;
    @XmlElement(name = "Operatore")
    protected OrgOrganigrammaType operatore;

    /**
     * Recupera il valore della proprietà identificativoProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoProtocolloRequestType }
     *     
     */
    public IdentificativoProtocolloRequestType getIdentificativoProtocollo() {
        return identificativoProtocollo;
    }

    /**
     * Imposta il valore della proprietà identificativoProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoProtocolloRequestType }
     *     
     */
    public void setIdentificativoProtocollo(IdentificativoProtocolloRequestType value) {
        this.identificativoProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà dataOperazione.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataOperazione() {
        return dataOperazione;
    }

    /**
     * Imposta il valore della proprietà dataOperazione.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataOperazione(XMLGregorianCalendar value) {
        this.dataOperazione = value;
    }

    /**
     * Recupera il valore della proprietà datiModifica.
     * 
     * @return
     *     possible object is
     *     {@link RichiestaUpdateDatiProtocolloType.DatiModifica }
     *     
     */
    public RichiestaUpdateDatiProtocolloType.DatiModifica getDatiModifica() {
        return datiModifica;
    }

    /**
     * Imposta il valore della proprietà datiModifica.
     * 
     * @param value
     *     allowed object is
     *     {@link RichiestaUpdateDatiProtocolloType.DatiModifica }
     *     
     */
    public void setDatiModifica(RichiestaUpdateDatiProtocolloType.DatiModifica value) {
        this.datiModifica = value;
    }

    /**
     * Recupera il valore della proprietà operatore.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getOperatore() {
        return operatore;
    }

    /**
     * Imposta il valore della proprietà operatore.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setOperatore(OrgOrganigrammaType value) {
        this.operatore = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Mittente" type="{http://mef.gov.it.v1.npsTypes}prot_mittente_type" minOccurs="0"/>
     *         &lt;element name="Oggetto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="TipologiaDocumento" type="{http://mef.gov.it.v1.npsTypes}prot_attributiEstesi_type" minOccurs="0"/>
     *         &lt;element name="Acl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="IsRiservato" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *         &lt;element name="IsCompleto" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *         &lt;element name="SegnaturaEstesa" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Intestazione" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}PrimaRegistrazione" minOccurs="0"/>
     *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}InterventoOperatore" minOccurs="0"/>
     *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentoDocumentiCartacei" minOccurs="0"/>
     *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentiTelematici" minOccurs="0"/>
     *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Classifica" minOccurs="0"/>
     *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Note" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Riferimenti" minOccurs="0"/>
     *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}PiuInfo" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="VoceTitolario" type="{http://mef.gov.it.v1.npsTypes}all_codiceDescrizione_type" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mittente",
        "oggetto",
        "tipologiaDocumento",
        "acl",
        "isRiservato",
        "isCompleto",
        "segnaturaEstesa",
        "voceTitolario"
    })
    public static class DatiModifica
        implements Serializable
    {

        @XmlElement(name = "Mittente")
        protected ProtMittenteType mittente;
        @XmlElement(name = "Oggetto")
        protected String oggetto;
        @XmlElement(name = "TipologiaDocumento")
        protected ProtAttributiEstesiType tipologiaDocumento;
        @XmlElement(name = "Acl")
        protected String acl;
        @XmlElement(name = "IsRiservato", defaultValue = "false")
        protected Boolean isRiservato;
        @XmlElement(name = "IsCompleto", defaultValue = "false")
        protected Boolean isCompleto;
        @XmlElement(name = "SegnaturaEstesa")
        protected RichiestaUpdateDatiProtocolloType.DatiModifica.SegnaturaEstesa segnaturaEstesa;
        @XmlElement(name = "VoceTitolario")
        protected AllCodiceDescrizioneType voceTitolario;

        /**
         * Recupera il valore della proprietà mittente.
         * 
         * @return
         *     possible object is
         *     {@link ProtMittenteType }
         *     
         */
        public ProtMittenteType getMittente() {
            return mittente;
        }

        /**
         * Imposta il valore della proprietà mittente.
         * 
         * @param value
         *     allowed object is
         *     {@link ProtMittenteType }
         *     
         */
        public void setMittente(ProtMittenteType value) {
            this.mittente = value;
        }

        /**
         * Recupera il valore della proprietà oggetto.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOggetto() {
            return oggetto;
        }

        /**
         * Imposta il valore della proprietà oggetto.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOggetto(String value) {
            this.oggetto = value;
        }

        /**
         * Recupera il valore della proprietà tipologiaDocumento.
         * 
         * @return
         *     possible object is
         *     {@link ProtAttributiEstesiType }
         *     
         */
        public ProtAttributiEstesiType getTipologiaDocumento() {
            return tipologiaDocumento;
        }

        /**
         * Imposta il valore della proprietà tipologiaDocumento.
         * 
         * @param value
         *     allowed object is
         *     {@link ProtAttributiEstesiType }
         *     
         */
        public void setTipologiaDocumento(ProtAttributiEstesiType value) {
            this.tipologiaDocumento = value;
        }

        /**
         * Recupera il valore della proprietà acl.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAcl() {
            return acl;
        }

        /**
         * Imposta il valore della proprietà acl.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAcl(String value) {
            this.acl = value;
        }

        /**
         * Recupera il valore della proprietà isRiservato.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isIsRiservato() {
            return isRiservato;
        }

        /**
         * Imposta il valore della proprietà isRiservato.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setIsRiservato(Boolean value) {
            this.isRiservato = value;
        }

        /**
         * Recupera il valore della proprietà isCompleto.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isIsCompleto() {
            return isCompleto;
        }

        /**
         * Imposta il valore della proprietà isCompleto.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setIsCompleto(Boolean value) {
            this.isCompleto = value;
        }

        /**
         * Recupera il valore della proprietà segnaturaEstesa.
         * 
         * @return
         *     possible object is
         *     {@link RichiestaUpdateDatiProtocolloType.DatiModifica.SegnaturaEstesa }
         *     
         */
        public RichiestaUpdateDatiProtocolloType.DatiModifica.SegnaturaEstesa getSegnaturaEstesa() {
            return segnaturaEstesa;
        }

        /**
         * Imposta il valore della proprietà segnaturaEstesa.
         * 
         * @param value
         *     allowed object is
         *     {@link RichiestaUpdateDatiProtocolloType.DatiModifica.SegnaturaEstesa }
         *     
         */
        public void setSegnaturaEstesa(RichiestaUpdateDatiProtocolloType.DatiModifica.SegnaturaEstesa value) {
            this.segnaturaEstesa = value;
        }

        /**
         * Recupera il valore della proprietà voceTitolario.
         * 
         * @return
         *     possible object is
         *     {@link AllCodiceDescrizioneType }
         *     
         */
        public AllCodiceDescrizioneType getVoceTitolario() {
            return voceTitolario;
        }

        /**
         * Imposta il valore della proprietà voceTitolario.
         * 
         * @param value
         *     allowed object is
         *     {@link AllCodiceDescrizioneType }
         *     
         */
        public void setVoceTitolario(AllCodiceDescrizioneType value) {
            this.voceTitolario = value;
        }


        /**
         * <p>Classe Java per anonymous complex type.
         * 
         * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Intestazione" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}PrimaRegistrazione" minOccurs="0"/>
         *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}InterventoOperatore" minOccurs="0"/>
         *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentoDocumentiCartacei" minOccurs="0"/>
         *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentiTelematici" minOccurs="0"/>
         *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Classifica" minOccurs="0"/>
         *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Note" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Riferimenti" minOccurs="0"/>
         *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}PiuInfo" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "intestazione",
            "riferimenti",
            "piuInfo"
        })
        public static class SegnaturaEstesa
            implements Serializable
        {

            @XmlElement(name = "Intestazione")
            protected RichiestaUpdateDatiProtocolloType.DatiModifica.SegnaturaEstesa.Intestazione intestazione;
            @XmlElement(name = "Riferimenti", namespace = "http://www.digitPa.gov.it/protocollo/")
            protected Riferimenti riferimenti;
            @XmlElement(name = "PiuInfo", namespace = "http://www.digitPa.gov.it/protocollo/")
            protected PiuInfo piuInfo;

            /**
             * Recupera il valore della proprietà intestazione.
             * 
             * @return
             *     possible object is
             *     {@link RichiestaUpdateDatiProtocolloType.DatiModifica.SegnaturaEstesa.Intestazione }
             *     
             */
            public RichiestaUpdateDatiProtocolloType.DatiModifica.SegnaturaEstesa.Intestazione getIntestazione() {
                return intestazione;
            }

            /**
             * Imposta il valore della proprietà intestazione.
             * 
             * @param value
             *     allowed object is
             *     {@link RichiestaUpdateDatiProtocolloType.DatiModifica.SegnaturaEstesa.Intestazione }
             *     
             */
            public void setIntestazione(RichiestaUpdateDatiProtocolloType.DatiModifica.SegnaturaEstesa.Intestazione value) {
                this.intestazione = value;
            }

            /**
             * Recupera il valore della proprietà riferimenti.
             * 
             * @return
             *     possible object is
             *     {@link Riferimenti }
             *     
             */
            public Riferimenti getRiferimenti() {
                return riferimenti;
            }

            /**
             * Imposta il valore della proprietà riferimenti.
             * 
             * @param value
             *     allowed object is
             *     {@link Riferimenti }
             *     
             */
            public void setRiferimenti(Riferimenti value) {
                this.riferimenti = value;
            }

            /**
             * Recupera il valore della proprietà piuInfo.
             * 
             * @return
             *     possible object is
             *     {@link PiuInfo }
             *     
             */
            public PiuInfo getPiuInfo() {
                return piuInfo;
            }

            /**
             * Imposta il valore della proprietà piuInfo.
             * 
             * @param value
             *     allowed object is
             *     {@link PiuInfo }
             *     
             */
            public void setPiuInfo(PiuInfo value) {
                this.piuInfo = value;
            }


            /**
             * <p>Classe Java per anonymous complex type.
             * 
             * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}PrimaRegistrazione" minOccurs="0"/>
             *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}InterventoOperatore" minOccurs="0"/>
             *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentoDocumentiCartacei" minOccurs="0"/>
             *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentiTelematici" minOccurs="0"/>
             *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Classifica" minOccurs="0"/>
             *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Note" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "primaRegistrazione",
                "interventoOperatore",
                "riferimentoDocumentiCartacei",
                "riferimentiTelematici",
                "classifica",
                "note"
            })
            public static class Intestazione
                implements Serializable
            {

                @XmlElement(name = "PrimaRegistrazione", namespace = "http://www.digitPa.gov.it/protocollo/")
                protected PrimaRegistrazione primaRegistrazione;
                @XmlElement(name = "InterventoOperatore", namespace = "http://www.digitPa.gov.it/protocollo/")
                protected InterventoOperatore interventoOperatore;
                @XmlElement(name = "RiferimentoDocumentiCartacei", namespace = "http://www.digitPa.gov.it/protocollo/")
                protected RiferimentoDocumentiCartacei riferimentoDocumentiCartacei;
                @XmlElement(name = "RiferimentiTelematici", namespace = "http://www.digitPa.gov.it/protocollo/")
                protected RiferimentiTelematici riferimentiTelematici;
                @XmlElement(name = "Classifica", namespace = "http://www.digitPa.gov.it/protocollo/")
                protected Classifica classifica;
                @XmlElement(name = "Note", namespace = "http://www.digitPa.gov.it/protocollo/")
                protected Note note;

                /**
                 * Recupera il valore della proprietà primaRegistrazione.
                 * 
                 * @return
                 *     possible object is
                 *     {@link PrimaRegistrazione }
                 *     
                 */
                public PrimaRegistrazione getPrimaRegistrazione() {
                    return primaRegistrazione;
                }

                /**
                 * Imposta il valore della proprietà primaRegistrazione.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link PrimaRegistrazione }
                 *     
                 */
                public void setPrimaRegistrazione(PrimaRegistrazione value) {
                    this.primaRegistrazione = value;
                }

                /**
                 * Recupera il valore della proprietà interventoOperatore.
                 * 
                 * @return
                 *     possible object is
                 *     {@link InterventoOperatore }
                 *     
                 */
                public InterventoOperatore getInterventoOperatore() {
                    return interventoOperatore;
                }

                /**
                 * Imposta il valore della proprietà interventoOperatore.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link InterventoOperatore }
                 *     
                 */
                public void setInterventoOperatore(InterventoOperatore value) {
                    this.interventoOperatore = value;
                }

                /**
                 * Recupera il valore della proprietà riferimentoDocumentiCartacei.
                 * 
                 * @return
                 *     possible object is
                 *     {@link RiferimentoDocumentiCartacei }
                 *     
                 */
                public RiferimentoDocumentiCartacei getRiferimentoDocumentiCartacei() {
                    return riferimentoDocumentiCartacei;
                }

                /**
                 * Imposta il valore della proprietà riferimentoDocumentiCartacei.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link RiferimentoDocumentiCartacei }
                 *     
                 */
                public void setRiferimentoDocumentiCartacei(RiferimentoDocumentiCartacei value) {
                    this.riferimentoDocumentiCartacei = value;
                }

                /**
                 * Recupera il valore della proprietà riferimentiTelematici.
                 * 
                 * @return
                 *     possible object is
                 *     {@link RiferimentiTelematici }
                 *     
                 */
                public RiferimentiTelematici getRiferimentiTelematici() {
                    return riferimentiTelematici;
                }

                /**
                 * Imposta il valore della proprietà riferimentiTelematici.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link RiferimentiTelematici }
                 *     
                 */
                public void setRiferimentiTelematici(RiferimentiTelematici value) {
                    this.riferimentiTelematici = value;
                }

                /**
                 * Recupera il valore della proprietà classifica.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Classifica }
                 *     
                 */
                public Classifica getClassifica() {
                    return classifica;
                }

                /**
                 * Imposta il valore della proprietà classifica.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Classifica }
                 *     
                 */
                public void setClassifica(Classifica value) {
                    this.classifica = value;
                }

                /**
                 * Recupera il valore della proprietà note.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Note }
                 *     
                 */
                public Note getNote() {
                    return note;
                }

                /**
                 * Imposta il valore della proprietà note.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Note }
                 *     
                 */
                public void setNote(Note value) {
                    this.note = value;
                }

            }

        }

    }

}
