
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.ProtDocumentoDaProtocollareType;


/**
 * identificativo del documento da utilizzare nelle varie richieste
 * 
 * <p>Classe Java per identificativoDocumentiRequest_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="identificativoDocumentiRequest_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DocumentoPrincipale" type="{http://mef.gov.it.v1.npsTypes}prot_documentoDaProtocollare_type"/>
 *         &lt;element name="Allegati" type="{http://mef.gov.it.v1.npsTypes}prot_documentoDaProtocollare_type" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "identificativoDocumentiRequest_type", propOrder = {
    "documentoPrincipale",
    "allegati"
})
public class IdentificativoDocumentiRequestType
    implements Serializable
{

    @XmlElement(name = "DocumentoPrincipale", required = true)
    protected ProtDocumentoDaProtocollareType documentoPrincipale;
    @XmlElement(name = "Allegati")
    protected List<ProtDocumentoDaProtocollareType> allegati;

    /**
     * Recupera il valore della proprietà documentoPrincipale.
     * 
     * @return
     *     possible object is
     *     {@link ProtDocumentoDaProtocollareType }
     *     
     */
    public ProtDocumentoDaProtocollareType getDocumentoPrincipale() {
        return documentoPrincipale;
    }

    /**
     * Imposta il valore della proprietà documentoPrincipale.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtDocumentoDaProtocollareType }
     *     
     */
    public void setDocumentoPrincipale(ProtDocumentoDaProtocollareType value) {
        this.documentoPrincipale = value;
    }

    /**
     * Gets the value of the allegati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allegati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllegati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtDocumentoDaProtocollareType }
     * 
     * 
     */
    public List<ProtDocumentoDaProtocollareType> getAllegati() {
        if (allegati == null) {
            allegati = new ArrayList<ProtDocumentoDaProtocollareType>();
        }
        return this.allegati;
    }

}
