
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import npstypes.v1.it.gov.mef.AllStatoRegistroAusiliarioType;
import npstypes.v1.it.gov.mef.AllTipoNumerazioneRegistroAusiliarioType;
import npstypes.v1.it.gov.mef.RegIdentificatoreRegistroAusiliarioType;


/**
 * Tipo di dato per richiedere la modifica di un registro ausiliario - solo admin
 * 
 * <p>Classe Java per richiesta_updateRegistroAusiliario_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_updateRegistroAusiliario_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdentificazioneRegistro" type="{http://mef.gov.it.v1.npsTypes}reg_identificatoreRegistroAusiliario_type"/>
 *         &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoNumerazione" type="{http://mef.gov.it.v1.npsTypes}all_tipoNumerazioneRegistroAusiliario_type" minOccurs="0"/>
 *         &lt;element name="DataRegistro" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="Stato" type="{http://mef.gov.it.v1.npsTypes}all_statoRegistroAusiliario_type" minOccurs="0"/>
 *         &lt;element name="IsAssociatoRegistroUfficiale" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsRegistroDocumentale" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsLocked" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsAutomatico" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="IsAttivo" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_updateRegistroAusiliario_type", propOrder = {
    "identificazioneRegistro",
    "descrizione",
    "tipoNumerazione",
    "dataRegistro",
    "stato",
    "isAssociatoRegistroUfficiale",
    "isRegistroDocumentale",
    "isLocked",
    "isAutomatico",
    "isAttivo"
})
public class RichiestaUpdateRegistroAusiliarioType
    implements Serializable
{

    @XmlElement(name = "IdentificazioneRegistro", required = true)
    protected RegIdentificatoreRegistroAusiliarioType identificazioneRegistro;
    @XmlElement(name = "Descrizione")
    protected String descrizione;
    @XmlElement(name = "TipoNumerazione")
    @XmlSchemaType(name = "string")
    protected AllTipoNumerazioneRegistroAusiliarioType tipoNumerazione;
    @XmlElement(name = "DataRegistro")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataRegistro;
    @XmlElement(name = "Stato")
    @XmlSchemaType(name = "string")
    protected AllStatoRegistroAusiliarioType stato;
    @XmlElement(name = "IsAssociatoRegistroUfficiale")
    protected Boolean isAssociatoRegistroUfficiale;
    @XmlElement(name = "IsRegistroDocumentale")
    protected Boolean isRegistroDocumentale;
    @XmlElement(name = "IsLocked")
    protected Boolean isLocked;
    @XmlElement(name = "IsAutomatico")
    protected Boolean isAutomatico;
    @XmlElement(name = "IsAttivo")
    protected Boolean isAttivo;

    /**
     * Recupera il valore della proprietà identificazioneRegistro.
     * 
     * @return
     *     possible object is
     *     {@link RegIdentificatoreRegistroAusiliarioType }
     *     
     */
    public RegIdentificatoreRegistroAusiliarioType getIdentificazioneRegistro() {
        return identificazioneRegistro;
    }

    /**
     * Imposta il valore della proprietà identificazioneRegistro.
     * 
     * @param value
     *     allowed object is
     *     {@link RegIdentificatoreRegistroAusiliarioType }
     *     
     */
    public void setIdentificazioneRegistro(RegIdentificatoreRegistroAusiliarioType value) {
        this.identificazioneRegistro = value;
    }

    /**
     * Recupera il valore della proprietà descrizione.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescrizione() {
        return descrizione;
    }

    /**
     * Imposta il valore della proprietà descrizione.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescrizione(String value) {
        this.descrizione = value;
    }

    /**
     * Recupera il valore della proprietà tipoNumerazione.
     * 
     * @return
     *     possible object is
     *     {@link AllTipoNumerazioneRegistroAusiliarioType }
     *     
     */
    public AllTipoNumerazioneRegistroAusiliarioType getTipoNumerazione() {
        return tipoNumerazione;
    }

    /**
     * Imposta il valore della proprietà tipoNumerazione.
     * 
     * @param value
     *     allowed object is
     *     {@link AllTipoNumerazioneRegistroAusiliarioType }
     *     
     */
    public void setTipoNumerazione(AllTipoNumerazioneRegistroAusiliarioType value) {
        this.tipoNumerazione = value;
    }

    /**
     * Recupera il valore della proprietà dataRegistro.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataRegistro() {
        return dataRegistro;
    }

    /**
     * Imposta il valore della proprietà dataRegistro.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataRegistro(XMLGregorianCalendar value) {
        this.dataRegistro = value;
    }

    /**
     * Recupera il valore della proprietà stato.
     * 
     * @return
     *     possible object is
     *     {@link AllStatoRegistroAusiliarioType }
     *     
     */
    public AllStatoRegistroAusiliarioType getStato() {
        return stato;
    }

    /**
     * Imposta il valore della proprietà stato.
     * 
     * @param value
     *     allowed object is
     *     {@link AllStatoRegistroAusiliarioType }
     *     
     */
    public void setStato(AllStatoRegistroAusiliarioType value) {
        this.stato = value;
    }

    /**
     * Recupera il valore della proprietà isAssociatoRegistroUfficiale.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsAssociatoRegistroUfficiale() {
        return isAssociatoRegistroUfficiale;
    }

    /**
     * Imposta il valore della proprietà isAssociatoRegistroUfficiale.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsAssociatoRegistroUfficiale(Boolean value) {
        this.isAssociatoRegistroUfficiale = value;
    }

    /**
     * Recupera il valore della proprietà isRegistroDocumentale.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsRegistroDocumentale() {
        return isRegistroDocumentale;
    }

    /**
     * Imposta il valore della proprietà isRegistroDocumentale.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsRegistroDocumentale(Boolean value) {
        this.isRegistroDocumentale = value;
    }

    /**
     * Recupera il valore della proprietà isLocked.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsLocked() {
        return isLocked;
    }

    /**
     * Imposta il valore della proprietà isLocked.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsLocked(Boolean value) {
        this.isLocked = value;
    }

    /**
     * Recupera il valore della proprietà isAutomatico.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsAutomatico() {
        return isAutomatico;
    }

    /**
     * Imposta il valore della proprietà isAutomatico.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsAutomatico(Boolean value) {
        this.isAutomatico = value;
    }

    /**
     * Recupera il valore della proprietà isAttivo.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsAttivo() {
        return isAttivo;
    }

    /**
     * Imposta il valore della proprietà isAttivo.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsAttivo(Boolean value) {
        this.isAttivo = value;
    }

}
