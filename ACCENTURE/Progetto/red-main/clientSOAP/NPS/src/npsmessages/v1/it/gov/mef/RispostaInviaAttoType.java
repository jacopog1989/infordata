
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.ProtProtocolloDatiMinimiType;


/**
 * Tipo di dato restituito a seguito di un invio dell'atto
 * 
 * <p>Classe Java per risposta_inviaAtto_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_inviaAtto_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsMessages}baseServiceResponse_type">
 *       &lt;sequence>
 *         &lt;element name="DatiProtocollo" type="{http://mef.gov.it.v1.npsTypes}prot_protocolloDatiMinimi_type" minOccurs="0"/>
 *         &lt;element name="ChiaveDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_inviaAtto_type", propOrder = {
    "datiProtocollo",
    "chiaveDocumento"
})
public class RispostaInviaAttoType
    extends BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "DatiProtocollo")
    protected ProtProtocolloDatiMinimiType datiProtocollo;
    @XmlElement(name = "ChiaveDocumento")
    protected String chiaveDocumento;

    /**
     * Recupera il valore della proprietà datiProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link ProtProtocolloDatiMinimiType }
     *     
     */
    public ProtProtocolloDatiMinimiType getDatiProtocollo() {
        return datiProtocollo;
    }

    /**
     * Imposta il valore della proprietà datiProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtProtocolloDatiMinimiType }
     *     
     */
    public void setDatiProtocollo(ProtProtocolloDatiMinimiType value) {
        this.datiProtocollo = value;
    }

    /**
     * Recupera il valore della proprietà chiaveDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveDocumento() {
        return chiaveDocumento;
    }

    /**
     * Imposta il valore della proprietà chiaveDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveDocumento(String value) {
        this.chiaveDocumento = value;
    }

}
