
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.OrgOrganigrammaType;
import npstypes.v1.it.gov.mef.RegAttributiEstesiType;
import npstypes.v1.it.gov.mef.RegIdentificatoreRegistrazioneAusiliariaType;


/**
 * Tipo di dato per richiedere la modifica di un registro ausiliario
 * 
 * <p>Classe Java per richiesta_updateRegistrazioneAusiliaria_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_updateRegistrazioneAusiliaria_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdentificativoRegistrazioneAusiliaria" type="{http://mef.gov.it.v1.npsTypes}reg_identificatoreRegistrazioneAusiliaria_type"/>
 *         &lt;element name="Oggetto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoDocumento" type="{http://mef.gov.it.v1.npsTypes}reg_attributiEstesi_type" minOccurs="0"/>
 *         &lt;element name="Acl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Operatore" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_updateRegistrazioneAusiliaria_type", propOrder = {
    "identificativoRegistrazioneAusiliaria",
    "oggetto",
    "tipoDocumento",
    "acl",
    "operatore"
})
public class RichiestaUpdateRegistrazioneAusiliariaType
    implements Serializable
{

    @XmlElement(name = "IdentificativoRegistrazioneAusiliaria", required = true, nillable = true)
    protected RegIdentificatoreRegistrazioneAusiliariaType identificativoRegistrazioneAusiliaria;
    @XmlElement(name = "Oggetto")
    protected String oggetto;
    @XmlElement(name = "TipoDocumento")
    protected RegAttributiEstesiType tipoDocumento;
    @XmlElement(name = "Acl")
    protected String acl;
    @XmlElement(name = "Operatore", required = true)
    protected OrgOrganigrammaType operatore;

    /**
     * Recupera il valore della proprietà identificativoRegistrazioneAusiliaria.
     * 
     * @return
     *     possible object is
     *     {@link RegIdentificatoreRegistrazioneAusiliariaType }
     *     
     */
    public RegIdentificatoreRegistrazioneAusiliariaType getIdentificativoRegistrazioneAusiliaria() {
        return identificativoRegistrazioneAusiliaria;
    }

    /**
     * Imposta il valore della proprietà identificativoRegistrazioneAusiliaria.
     * 
     * @param value
     *     allowed object is
     *     {@link RegIdentificatoreRegistrazioneAusiliariaType }
     *     
     */
    public void setIdentificativoRegistrazioneAusiliaria(RegIdentificatoreRegistrazioneAusiliariaType value) {
        this.identificativoRegistrazioneAusiliaria = value;
    }

    /**
     * Recupera il valore della proprietà oggetto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggetto() {
        return oggetto;
    }

    /**
     * Imposta il valore della proprietà oggetto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggetto(String value) {
        this.oggetto = value;
    }

    /**
     * Recupera il valore della proprietà tipoDocumento.
     * 
     * @return
     *     possible object is
     *     {@link RegAttributiEstesiType }
     *     
     */
    public RegAttributiEstesiType getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Imposta il valore della proprietà tipoDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link RegAttributiEstesiType }
     *     
     */
    public void setTipoDocumento(RegAttributiEstesiType value) {
        this.tipoDocumento = value;
    }

    /**
     * Recupera il valore della proprietà acl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcl() {
        return acl;
    }

    /**
     * Imposta il valore della proprietà acl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcl(String value) {
        this.acl = value;
    }

    /**
     * Recupera il valore della proprietà operatore.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getOperatore() {
        return operatore;
    }

    /**
     * Imposta il valore della proprietà operatore.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setOperatore(OrgOrganigrammaType value) {
        this.operatore = value;
    }

}
