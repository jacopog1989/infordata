
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.IdentificativoProtocolloRequestType;
import npstypes.v1.it.gov.mef.OrgOrganigrammaType;
import npstypes.v1.it.gov.mef.RegIdentificatoreRegistrazioneAusiliariaType;


/**
 * Tipo di dato per richiedere la modifica di un registro ausiliario
 * 
 * <p>Classe Java per richiesta_collegaRegistrazioneAusiliariaProtocolloIngresso_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_collegaRegistrazioneAusiliariaProtocolloIngresso_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdentificativoRegistrazioneAusiliaria" type="{http://mef.gov.it.v1.npsTypes}reg_identificatoreRegistrazioneAusiliaria_type"/>
 *         &lt;element name="ProtocolloIngresso" type="{http://mef.gov.it.v1.npsTypes}identificativoProtocolloRequest_type"/>
 *         &lt;element name="Operatore" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_collegaRegistrazioneAusiliariaProtocolloIngresso_type", propOrder = {
    "identificativoRegistrazioneAusiliaria",
    "protocolloIngresso",
    "operatore"
})
public class RichiestaCollegaRegistrazioneAusiliariaProtocolloIngressoType
    implements Serializable
{

    @XmlElement(name = "IdentificativoRegistrazioneAusiliaria", required = true, nillable = true)
    protected RegIdentificatoreRegistrazioneAusiliariaType identificativoRegistrazioneAusiliaria;
    @XmlElement(name = "ProtocolloIngresso", required = true)
    protected IdentificativoProtocolloRequestType protocolloIngresso;
    @XmlElement(name = "Operatore", required = true)
    protected OrgOrganigrammaType operatore;

    /**
     * Recupera il valore della proprietà identificativoRegistrazioneAusiliaria.
     * 
     * @return
     *     possible object is
     *     {@link RegIdentificatoreRegistrazioneAusiliariaType }
     *     
     */
    public RegIdentificatoreRegistrazioneAusiliariaType getIdentificativoRegistrazioneAusiliaria() {
        return identificativoRegistrazioneAusiliaria;
    }

    /**
     * Imposta il valore della proprietà identificativoRegistrazioneAusiliaria.
     * 
     * @param value
     *     allowed object is
     *     {@link RegIdentificatoreRegistrazioneAusiliariaType }
     *     
     */
    public void setIdentificativoRegistrazioneAusiliaria(RegIdentificatoreRegistrazioneAusiliariaType value) {
        this.identificativoRegistrazioneAusiliaria = value;
    }

    /**
     * Recupera il valore della proprietà protocolloIngresso.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoProtocolloRequestType }
     *     
     */
    public IdentificativoProtocolloRequestType getProtocolloIngresso() {
        return protocolloIngresso;
    }

    /**
     * Imposta il valore della proprietà protocolloIngresso.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoProtocolloRequestType }
     *     
     */
    public void setProtocolloIngresso(IdentificativoProtocolloRequestType value) {
        this.protocolloIngresso = value;
    }

    /**
     * Recupera il valore della proprietà operatore.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getOperatore() {
        return operatore;
    }

    /**
     * Imposta il valore della proprietà operatore.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setOperatore(OrgOrganigrammaType value) {
        this.operatore = value;
    }

}
