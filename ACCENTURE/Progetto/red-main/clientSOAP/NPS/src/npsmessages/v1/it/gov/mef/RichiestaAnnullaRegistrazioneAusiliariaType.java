
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import npstypes.v1.it.gov.mef.OrgOrganigrammaType;
import npstypes.v1.it.gov.mef.RegIdentificatoreRegistrazioneAusiliariaType;


/**
 * Tipo di dato per richiedere l'annullamento di una registrazione ausiliaria
 * 
 * <p>Classe Java per richiesta_annullaRegistrazioneAusiliaria_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_annullaRegistrazioneAusiliaria_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdentificativoRegistrazioneAusiliaria" type="{http://mef.gov.it.v1.npsTypes}reg_identificatoreRegistrazioneAusiliaria_type"/>
 *         &lt;element name="MotivoAnnullamento" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DataAnnullamento" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="Operatore" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_annullaRegistrazioneAusiliaria_type", propOrder = {
    "identificativoRegistrazioneAusiliaria",
    "motivoAnnullamento",
    "dataAnnullamento",
    "operatore"
})
@XmlSeeAlso({
    RichiestaAnnullaRegistrazioneAusiliaria.class
})
public class RichiestaAnnullaRegistrazioneAusiliariaType
    implements Serializable
{

    @XmlElement(name = "IdentificativoRegistrazioneAusiliaria", required = true, nillable = true)
    protected RegIdentificatoreRegistrazioneAusiliariaType identificativoRegistrazioneAusiliaria;
    @XmlElement(name = "MotivoAnnullamento", required = true)
    protected String motivoAnnullamento;
    @XmlElement(name = "DataAnnullamento")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataAnnullamento;
    @XmlElement(name = "Operatore", required = true)
    protected OrgOrganigrammaType operatore;

    /**
     * Recupera il valore della proprietà identificativoRegistrazioneAusiliaria.
     * 
     * @return
     *     possible object is
     *     {@link RegIdentificatoreRegistrazioneAusiliariaType }
     *     
     */
    public RegIdentificatoreRegistrazioneAusiliariaType getIdentificativoRegistrazioneAusiliaria() {
        return identificativoRegistrazioneAusiliaria;
    }

    /**
     * Imposta il valore della proprietà identificativoRegistrazioneAusiliaria.
     * 
     * @param value
     *     allowed object is
     *     {@link RegIdentificatoreRegistrazioneAusiliariaType }
     *     
     */
    public void setIdentificativoRegistrazioneAusiliaria(RegIdentificatoreRegistrazioneAusiliariaType value) {
        this.identificativoRegistrazioneAusiliaria = value;
    }

    /**
     * Recupera il valore della proprietà motivoAnnullamento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMotivoAnnullamento() {
        return motivoAnnullamento;
    }

    /**
     * Imposta il valore della proprietà motivoAnnullamento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMotivoAnnullamento(String value) {
        this.motivoAnnullamento = value;
    }

    /**
     * Recupera il valore della proprietà dataAnnullamento.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataAnnullamento() {
        return dataAnnullamento;
    }

    /**
     * Imposta il valore della proprietà dataAnnullamento.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataAnnullamento(XMLGregorianCalendar value) {
        this.dataAnnullamento = value;
    }

    /**
     * Recupera il valore della proprietà operatore.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getOperatore() {
        return operatore;
    }

    /**
     * Imposta il valore della proprietà operatore.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setOperatore(OrgOrganigrammaType value) {
        this.operatore = value;
    }

}
