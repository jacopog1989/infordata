
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.WkfDatiErroreFlussoType;


/**
 * Tipo di dato per richiedere l'elaborazione dell'errore per il  messaggio del flusso
 * 
 * <p>Java class for richiesta_elaboraErrore_Flusso_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_elaboraErrore_Flusso_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}wkf_datiErroreFlusso_type">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_elaboraErrore_Flusso_type")
public class RichiestaElaboraErroreFlussoType
    extends WkfDatiErroreFlussoType
    implements Serializable
{


}
