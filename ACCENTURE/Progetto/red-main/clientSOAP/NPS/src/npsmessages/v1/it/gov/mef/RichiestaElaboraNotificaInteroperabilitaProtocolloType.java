
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.WkfDatiFlussoInteroperabilitaType;


/**
 * Tipo di dato per richiedere la notifica interoperabile ricevuta
 * 
 * <p>Java class for richiesta_elaboraNotificaInteroperabilita_Protocollo_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_elaboraNotificaInteroperabilita_Protocollo_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}wkf_datiFlussoInteroperabilita_type">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_elaboraNotificaInteroperabilita_Protocollo_type")
public class RichiestaElaboraNotificaInteroperabilitaProtocolloType
    extends WkfDatiFlussoInteroperabilitaType
    implements Serializable
{


}
