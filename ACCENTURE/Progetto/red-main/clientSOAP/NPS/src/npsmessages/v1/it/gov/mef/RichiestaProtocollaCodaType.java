
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import it.gov.digitpa.protocollo.Classifica;
import it.gov.digitpa.protocollo.InterventoOperatore;
import it.gov.digitpa.protocollo.Note;
import it.gov.digitpa.protocollo.PiuInfo;
import it.gov.digitpa.protocollo.PrimaRegistrazione;
import it.gov.digitpa.protocollo.Riferimenti;
import it.gov.digitpa.protocollo.RiferimentiTelematici;
import it.gov.digitpa.protocollo.RiferimentoDocumentiCartacei;
import npstypes.v1.it.gov.mef.AllCodiceDescrizioneType;
import npstypes.v1.it.gov.mef.OrgOrganigrammaType;
import npstypes.v1.it.gov.mef.ProtAllaccioProtocolloType;
import npstypes.v1.it.gov.mef.ProtAttributiEstesiType;
import npstypes.v1.it.gov.mef.ProtMittenteType;
import npstypes.v1.it.gov.mef.ProtRegistroProtocolloType;


/**
 * Tipo di dato utilizzato nella richiesta di protocollazione asincrona dalla coda.
 * 
 * <p>Classe Java per richiesta_protocollaCoda_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_protocollaCoda_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdMessaggio" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="Registro" type="{http://mef.gov.it.v1.npsTypes}prot_registroProtocollo_type"/>
 *         &lt;element name="Oggetto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodiceFlusso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdentificatoreProcesso" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdentificativoMessaggio" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="Protocollatore" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type" minOccurs="0"/>
 *         &lt;element name="Mittente" type="{http://mef.gov.it.v1.npsTypes}prot_mittente_type" minOccurs="0"/>
 *         &lt;element name="TipologiaDocumento" type="{http://mef.gov.it.v1.npsTypes}prot_attributiEstesi_type"/>
 *         &lt;element name="Allacciati" type="{http://mef.gov.it.v1.npsTypes}prot_allaccioProtocollo_type" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="Acl" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SegnaturaEstesa" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Intestazione" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}PrimaRegistrazione" minOccurs="0"/>
 *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}InterventoOperatore" minOccurs="0"/>
 *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentoDocumentiCartacei" minOccurs="0"/>
 *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentiTelematici" minOccurs="0"/>
 *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Classifica" minOccurs="0"/>
 *                             &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Note" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Riferimenti" minOccurs="0"/>
 *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}PiuInfo" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ChiaveDocumentale" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChiaveFascicolo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChiaveFascicoloRiferimento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoFascicolo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="VoceTitolario" type="{http://mef.gov.it.v1.npsTypes}all_codiceDescrizione_type" minOccurs="0"/>
 *         &lt;element name="Stato">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="DA_ASSEGNARE"/>
 *               &lt;enumeration value="IN_LAVORAZIONE"/>
 *               &lt;enumeration value="AGLI_ATTI"/>
 *               &lt;enumeration value="CHIUSO"/>
 *               &lt;enumeration value="DA_SPEDIRE"/>
 *               &lt;enumeration value="INVIATO"/>
 *               &lt;enumeration value="SPEDITO"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Documenti" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;choice>
 *                   &lt;element name="Interni" type="{http://mef.gov.it.v1.npsMessages}identificativoDocumentiRequest_type"/>
 *                   &lt;element name="Esterni" type="{http://mef.gov.it.v1.npsMessages}identificativoDocumentiEsterniRequest_type"/>
 *                 &lt;/choice>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="SistemaAusiliario" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_protocollaCoda_type", propOrder = {
    "idMessaggio",
    "registro",
    "oggetto",
    "codiceFlusso",
    "identificatoreProcesso",
    "identificativoMessaggio",
    "protocollatore",
    "mittente",
    "tipologiaDocumento",
    "allacciati",
    "acl",
    "segnaturaEstesa",
    "chiaveDocumentale",
    "chiaveFascicolo",
    "chiaveFascicoloRiferimento",
    "tipoFascicolo",
    "voceTitolario",
    "stato",
    "documenti",
    "sistemaAusiliario"
})
public class RichiestaProtocollaCodaType
    implements Serializable
{

    @XmlElement(name = "IdMessaggio", required = true)
    protected String idMessaggio;
    @XmlElement(name = "Registro", required = true)
    protected ProtRegistroProtocolloType registro;
    @XmlElement(name = "Oggetto")
    protected String oggetto;
    @XmlElement(name = "CodiceFlusso", required = true)
    protected String codiceFlusso;
    @XmlElement(name = "IdentificatoreProcesso", required = true)
    protected String identificatoreProcesso;
    @XmlElement(name = "IdentificativoMessaggio")
    protected int identificativoMessaggio;
    @XmlElement(name = "Protocollatore")
    protected OrgOrganigrammaType protocollatore;
    @XmlElement(name = "Mittente")
    protected ProtMittenteType mittente;
    @XmlElement(name = "TipologiaDocumento", required = true)
    protected ProtAttributiEstesiType tipologiaDocumento;
    @XmlElement(name = "Allacciati")
    protected List<ProtAllaccioProtocolloType> allacciati;
    @XmlElement(name = "Acl")
    protected String acl;
    @XmlElement(name = "SegnaturaEstesa")
    protected RichiestaProtocollaCodaType.SegnaturaEstesa segnaturaEstesa;
    @XmlElement(name = "ChiaveDocumentale")
    protected String chiaveDocumentale;
    @XmlElement(name = "ChiaveFascicolo")
    protected String chiaveFascicolo;
    @XmlElement(name = "ChiaveFascicoloRiferimento")
    protected String chiaveFascicoloRiferimento;
    @XmlElement(name = "TipoFascicolo")
    protected String tipoFascicolo;
    @XmlElement(name = "VoceTitolario")
    protected AllCodiceDescrizioneType voceTitolario;
    @XmlElement(name = "Stato", required = true)
    protected String stato;
    @XmlElement(name = "Documenti")
    protected RichiestaProtocollaCodaType.Documenti documenti;
    @XmlElement(name = "SistemaAusiliario")
    protected String sistemaAusiliario;

    /**
     * Recupera il valore della proprietà idMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdMessaggio() {
        return idMessaggio;
    }

    /**
     * Imposta il valore della proprietà idMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdMessaggio(String value) {
        this.idMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà registro.
     * 
     * @return
     *     possible object is
     *     {@link ProtRegistroProtocolloType }
     *     
     */
    public ProtRegistroProtocolloType getRegistro() {
        return registro;
    }

    /**
     * Imposta il valore della proprietà registro.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtRegistroProtocolloType }
     *     
     */
    public void setRegistro(ProtRegistroProtocolloType value) {
        this.registro = value;
    }

    /**
     * Recupera il valore della proprietà oggetto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggetto() {
        return oggetto;
    }

    /**
     * Imposta il valore della proprietà oggetto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggetto(String value) {
        this.oggetto = value;
    }

    /**
     * Recupera il valore della proprietà codiceFlusso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceFlusso() {
        return codiceFlusso;
    }

    /**
     * Imposta il valore della proprietà codiceFlusso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceFlusso(String value) {
        this.codiceFlusso = value;
    }

    /**
     * Recupera il valore della proprietà identificatoreProcesso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificatoreProcesso() {
        return identificatoreProcesso;
    }

    /**
     * Imposta il valore della proprietà identificatoreProcesso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificatoreProcesso(String value) {
        this.identificatoreProcesso = value;
    }

    /**
     * Recupera il valore della proprietà identificativoMessaggio.
     * 
     */
    public int getIdentificativoMessaggio() {
        return identificativoMessaggio;
    }

    /**
     * Imposta il valore della proprietà identificativoMessaggio.
     * 
     */
    public void setIdentificativoMessaggio(int value) {
        this.identificativoMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà protocollatore.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getProtocollatore() {
        return protocollatore;
    }

    /**
     * Imposta il valore della proprietà protocollatore.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setProtocollatore(OrgOrganigrammaType value) {
        this.protocollatore = value;
    }

    /**
     * Recupera il valore della proprietà mittente.
     * 
     * @return
     *     possible object is
     *     {@link ProtMittenteType }
     *     
     */
    public ProtMittenteType getMittente() {
        return mittente;
    }

    /**
     * Imposta il valore della proprietà mittente.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtMittenteType }
     *     
     */
    public void setMittente(ProtMittenteType value) {
        this.mittente = value;
    }

    /**
     * Recupera il valore della proprietà tipologiaDocumento.
     * 
     * @return
     *     possible object is
     *     {@link ProtAttributiEstesiType }
     *     
     */
    public ProtAttributiEstesiType getTipologiaDocumento() {
        return tipologiaDocumento;
    }

    /**
     * Imposta il valore della proprietà tipologiaDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtAttributiEstesiType }
     *     
     */
    public void setTipologiaDocumento(ProtAttributiEstesiType value) {
        this.tipologiaDocumento = value;
    }

    /**
     * Gets the value of the allacciati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allacciati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllacciati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtAllaccioProtocolloType }
     * 
     * 
     */
    public List<ProtAllaccioProtocolloType> getAllacciati() {
        if (allacciati == null) {
            allacciati = new ArrayList<ProtAllaccioProtocolloType>();
        }
        return this.allacciati;
    }

    /**
     * Recupera il valore della proprietà acl.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcl() {
        return acl;
    }

    /**
     * Imposta il valore della proprietà acl.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcl(String value) {
        this.acl = value;
    }

    /**
     * Recupera il valore della proprietà segnaturaEstesa.
     * 
     * @return
     *     possible object is
     *     {@link RichiestaProtocollaCodaType.SegnaturaEstesa }
     *     
     */
    public RichiestaProtocollaCodaType.SegnaturaEstesa getSegnaturaEstesa() {
        return segnaturaEstesa;
    }

    /**
     * Imposta il valore della proprietà segnaturaEstesa.
     * 
     * @param value
     *     allowed object is
     *     {@link RichiestaProtocollaCodaType.SegnaturaEstesa }
     *     
     */
    public void setSegnaturaEstesa(RichiestaProtocollaCodaType.SegnaturaEstesa value) {
        this.segnaturaEstesa = value;
    }

    /**
     * Recupera il valore della proprietà chiaveDocumentale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveDocumentale() {
        return chiaveDocumentale;
    }

    /**
     * Imposta il valore della proprietà chiaveDocumentale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveDocumentale(String value) {
        this.chiaveDocumentale = value;
    }

    /**
     * Recupera il valore della proprietà chiaveFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveFascicolo() {
        return chiaveFascicolo;
    }

    /**
     * Imposta il valore della proprietà chiaveFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveFascicolo(String value) {
        this.chiaveFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà chiaveFascicoloRiferimento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveFascicoloRiferimento() {
        return chiaveFascicoloRiferimento;
    }

    /**
     * Imposta il valore della proprietà chiaveFascicoloRiferimento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveFascicoloRiferimento(String value) {
        this.chiaveFascicoloRiferimento = value;
    }

    /**
     * Recupera il valore della proprietà tipoFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoFascicolo() {
        return tipoFascicolo;
    }

    /**
     * Imposta il valore della proprietà tipoFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoFascicolo(String value) {
        this.tipoFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà voceTitolario.
     * 
     * @return
     *     possible object is
     *     {@link AllCodiceDescrizioneType }
     *     
     */
    public AllCodiceDescrizioneType getVoceTitolario() {
        return voceTitolario;
    }

    /**
     * Imposta il valore della proprietà voceTitolario.
     * 
     * @param value
     *     allowed object is
     *     {@link AllCodiceDescrizioneType }
     *     
     */
    public void setVoceTitolario(AllCodiceDescrizioneType value) {
        this.voceTitolario = value;
    }

    /**
     * Recupera il valore della proprietà stato.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStato() {
        return stato;
    }

    /**
     * Imposta il valore della proprietà stato.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStato(String value) {
        this.stato = value;
    }

    /**
     * Recupera il valore della proprietà documenti.
     * 
     * @return
     *     possible object is
     *     {@link RichiestaProtocollaCodaType.Documenti }
     *     
     */
    public RichiestaProtocollaCodaType.Documenti getDocumenti() {
        return documenti;
    }

    /**
     * Imposta il valore della proprietà documenti.
     * 
     * @param value
     *     allowed object is
     *     {@link RichiestaProtocollaCodaType.Documenti }
     *     
     */
    public void setDocumenti(RichiestaProtocollaCodaType.Documenti value) {
        this.documenti = value;
    }

    /**
     * Recupera il valore della proprietà sistemaAusiliario.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaAusiliario() {
        return sistemaAusiliario;
    }

    /**
     * Imposta il valore della proprietà sistemaAusiliario.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaAusiliario(String value) {
        this.sistemaAusiliario = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;choice>
     *         &lt;element name="Interni" type="{http://mef.gov.it.v1.npsMessages}identificativoDocumentiRequest_type"/>
     *         &lt;element name="Esterni" type="{http://mef.gov.it.v1.npsMessages}identificativoDocumentiEsterniRequest_type"/>
     *       &lt;/choice>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "interni",
        "esterni"
    })
    public static class Documenti
        implements Serializable
    {

        @XmlElement(name = "Interni")
        protected IdentificativoDocumentiRequestType interni;
        @XmlElement(name = "Esterni")
        protected IdentificativoDocumentiEsterniRequestType esterni;

        /**
         * Recupera il valore della proprietà interni.
         * 
         * @return
         *     possible object is
         *     {@link IdentificativoDocumentiRequestType }
         *     
         */
        public IdentificativoDocumentiRequestType getInterni() {
            return interni;
        }

        /**
         * Imposta il valore della proprietà interni.
         * 
         * @param value
         *     allowed object is
         *     {@link IdentificativoDocumentiRequestType }
         *     
         */
        public void setInterni(IdentificativoDocumentiRequestType value) {
            this.interni = value;
        }

        /**
         * Recupera il valore della proprietà esterni.
         * 
         * @return
         *     possible object is
         *     {@link IdentificativoDocumentiEsterniRequestType }
         *     
         */
        public IdentificativoDocumentiEsterniRequestType getEsterni() {
            return esterni;
        }

        /**
         * Imposta il valore della proprietà esterni.
         * 
         * @param value
         *     allowed object is
         *     {@link IdentificativoDocumentiEsterniRequestType }
         *     
         */
        public void setEsterni(IdentificativoDocumentiEsterniRequestType value) {
            this.esterni = value;
        }

    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Intestazione" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}PrimaRegistrazione" minOccurs="0"/>
     *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}InterventoOperatore" minOccurs="0"/>
     *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentoDocumentiCartacei" minOccurs="0"/>
     *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentiTelematici" minOccurs="0"/>
     *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Classifica" minOccurs="0"/>
     *                   &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Note" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Riferimenti" minOccurs="0"/>
     *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}PiuInfo" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "intestazione",
        "riferimenti",
        "piuInfo"
    })
    public static class SegnaturaEstesa
        implements Serializable
    {

        @XmlElement(name = "Intestazione")
        protected RichiestaProtocollaCodaType.SegnaturaEstesa.Intestazione intestazione;
        @XmlElement(name = "Riferimenti", namespace = "http://www.digitPa.gov.it/protocollo/")
        protected Riferimenti riferimenti;
        @XmlElement(name = "PiuInfo", namespace = "http://www.digitPa.gov.it/protocollo/")
        protected PiuInfo piuInfo;

        /**
         * Recupera il valore della proprietà intestazione.
         * 
         * @return
         *     possible object is
         *     {@link RichiestaProtocollaCodaType.SegnaturaEstesa.Intestazione }
         *     
         */
        public RichiestaProtocollaCodaType.SegnaturaEstesa.Intestazione getIntestazione() {
            return intestazione;
        }

        /**
         * Imposta il valore della proprietà intestazione.
         * 
         * @param value
         *     allowed object is
         *     {@link RichiestaProtocollaCodaType.SegnaturaEstesa.Intestazione }
         *     
         */
        public void setIntestazione(RichiestaProtocollaCodaType.SegnaturaEstesa.Intestazione value) {
            this.intestazione = value;
        }

        /**
         * Recupera il valore della proprietà riferimenti.
         * 
         * @return
         *     possible object is
         *     {@link Riferimenti }
         *     
         */
        public Riferimenti getRiferimenti() {
            return riferimenti;
        }

        /**
         * Imposta il valore della proprietà riferimenti.
         * 
         * @param value
         *     allowed object is
         *     {@link Riferimenti }
         *     
         */
        public void setRiferimenti(Riferimenti value) {
            this.riferimenti = value;
        }

        /**
         * Recupera il valore della proprietà piuInfo.
         * 
         * @return
         *     possible object is
         *     {@link PiuInfo }
         *     
         */
        public PiuInfo getPiuInfo() {
            return piuInfo;
        }

        /**
         * Imposta il valore della proprietà piuInfo.
         * 
         * @param value
         *     allowed object is
         *     {@link PiuInfo }
         *     
         */
        public void setPiuInfo(PiuInfo value) {
            this.piuInfo = value;
        }


        /**
         * <p>Classe Java per anonymous complex type.
         * 
         * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}PrimaRegistrazione" minOccurs="0"/>
         *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}InterventoOperatore" minOccurs="0"/>
         *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentoDocumentiCartacei" minOccurs="0"/>
         *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}RiferimentiTelematici" minOccurs="0"/>
         *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Classifica" minOccurs="0"/>
         *         &lt;element ref="{http://www.digitPa.gov.it/protocollo/}Note" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "primaRegistrazione",
            "interventoOperatore",
            "riferimentoDocumentiCartacei",
            "riferimentiTelematici",
            "classifica",
            "note"
        })
        public static class Intestazione
            implements Serializable
        {

            @XmlElement(name = "PrimaRegistrazione", namespace = "http://www.digitPa.gov.it/protocollo/")
            protected PrimaRegistrazione primaRegistrazione;
            @XmlElement(name = "InterventoOperatore", namespace = "http://www.digitPa.gov.it/protocollo/")
            protected InterventoOperatore interventoOperatore;
            @XmlElement(name = "RiferimentoDocumentiCartacei", namespace = "http://www.digitPa.gov.it/protocollo/")
            protected RiferimentoDocumentiCartacei riferimentoDocumentiCartacei;
            @XmlElement(name = "RiferimentiTelematici", namespace = "http://www.digitPa.gov.it/protocollo/")
            protected RiferimentiTelematici riferimentiTelematici;
            @XmlElement(name = "Classifica", namespace = "http://www.digitPa.gov.it/protocollo/")
            protected Classifica classifica;
            @XmlElement(name = "Note", namespace = "http://www.digitPa.gov.it/protocollo/")
            protected Note note;

            /**
             * Recupera il valore della proprietà primaRegistrazione.
             * 
             * @return
             *     possible object is
             *     {@link PrimaRegistrazione }
             *     
             */
            public PrimaRegistrazione getPrimaRegistrazione() {
                return primaRegistrazione;
            }

            /**
             * Imposta il valore della proprietà primaRegistrazione.
             * 
             * @param value
             *     allowed object is
             *     {@link PrimaRegistrazione }
             *     
             */
            public void setPrimaRegistrazione(PrimaRegistrazione value) {
                this.primaRegistrazione = value;
            }

            /**
             * Recupera il valore della proprietà interventoOperatore.
             * 
             * @return
             *     possible object is
             *     {@link InterventoOperatore }
             *     
             */
            public InterventoOperatore getInterventoOperatore() {
                return interventoOperatore;
            }

            /**
             * Imposta il valore della proprietà interventoOperatore.
             * 
             * @param value
             *     allowed object is
             *     {@link InterventoOperatore }
             *     
             */
            public void setInterventoOperatore(InterventoOperatore value) {
                this.interventoOperatore = value;
            }

            /**
             * Recupera il valore della proprietà riferimentoDocumentiCartacei.
             * 
             * @return
             *     possible object is
             *     {@link RiferimentoDocumentiCartacei }
             *     
             */
            public RiferimentoDocumentiCartacei getRiferimentoDocumentiCartacei() {
                return riferimentoDocumentiCartacei;
            }

            /**
             * Imposta il valore della proprietà riferimentoDocumentiCartacei.
             * 
             * @param value
             *     allowed object is
             *     {@link RiferimentoDocumentiCartacei }
             *     
             */
            public void setRiferimentoDocumentiCartacei(RiferimentoDocumentiCartacei value) {
                this.riferimentoDocumentiCartacei = value;
            }

            /**
             * Recupera il valore della proprietà riferimentiTelematici.
             * 
             * @return
             *     possible object is
             *     {@link RiferimentiTelematici }
             *     
             */
            public RiferimentiTelematici getRiferimentiTelematici() {
                return riferimentiTelematici;
            }

            /**
             * Imposta il valore della proprietà riferimentiTelematici.
             * 
             * @param value
             *     allowed object is
             *     {@link RiferimentiTelematici }
             *     
             */
            public void setRiferimentiTelematici(RiferimentiTelematici value) {
                this.riferimentiTelematici = value;
            }

            /**
             * Recupera il valore della proprietà classifica.
             * 
             * @return
             *     possible object is
             *     {@link Classifica }
             *     
             */
            public Classifica getClassifica() {
                return classifica;
            }

            /**
             * Imposta il valore della proprietà classifica.
             * 
             * @param value
             *     allowed object is
             *     {@link Classifica }
             *     
             */
            public void setClassifica(Classifica value) {
                this.classifica = value;
            }

            /**
             * Recupera il valore della proprietà note.
             * 
             * @return
             *     possible object is
             *     {@link Note }
             *     
             */
            public Note getNote() {
                return note;
            }

            /**
             * Imposta il valore della proprietà note.
             * 
             * @param value
             *     allowed object is
             *     {@link Note }
             *     
             */
            public void setNote(Note value) {
                this.note = value;
            }

        }

    }

}
