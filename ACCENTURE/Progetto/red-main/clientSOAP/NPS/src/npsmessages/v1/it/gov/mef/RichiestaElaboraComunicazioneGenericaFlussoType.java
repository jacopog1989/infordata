
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.WkfDatiFlussoEntrataType;


/**
 * Tipo di dato per richiedere l'elaborazione della comunicazione generica
 * 
 * <p>Java class for richiesta_elaboraComunicazioneGenerica_Flusso_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_elaboraComunicazioneGenerica_Flusso_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}wkf_datiFlussoEntrata_type">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_elaboraComunicazioneGenerica_Flusso_type")
public class RichiestaElaboraComunicazioneGenericaFlussoType
    extends WkfDatiFlussoEntrataType
    implements Serializable
{


}
