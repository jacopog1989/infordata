
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.RegRegistroAusiliarioType;


/**
 * Tipo di dato per richiedere la creazione di un registro ausiliario
 * 
 * <p>Classe Java per richiesta_createRegistroAusiliario_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_createRegistroAusiliario_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}reg_registroAusiliario_type">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_createRegistroAusiliario_type")
public class RichiestaCreateRegistroAusiliarioType
    extends RegRegistroAusiliarioType
    implements Serializable
{


}
