
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.RegIdentificatoreRegistrazioneAusiliariaType;


/**
 * Tipo di dato per aprire una registrazione ausiliaria
 * 
 * <p>Classe Java per richiesta_getRegistrazioneAusiliaria_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_getRegistrazioneAusiliaria_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}reg_identificatoreRegistrazioneAusiliaria_type">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_getRegistrazioneAusiliaria_type")
public class RichiestaGetRegistrazioneAusiliariaType
    extends RegIdentificatoreRegistrazioneAusiliariaType
    implements Serializable
{


}
