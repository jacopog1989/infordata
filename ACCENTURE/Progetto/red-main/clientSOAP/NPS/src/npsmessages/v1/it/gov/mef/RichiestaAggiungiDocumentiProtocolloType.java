
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.DocTipoOperazioneType;
import npstypes.v1.it.gov.mef.OrgOrganigrammaType;


/**
 * Tipo di dato utilizzato nella richiesta di aggiungere un documento ad un protocollo esistente.
 * 
 * <p>Classe Java per richiesta_aggiungiDocumentiProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_aggiungiDocumentiProtocollo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Protocollo" type="{http://mef.gov.it.v1.npsMessages}identificativoProtocolloRequest_type"/>
 *         &lt;element name="IsCompleto" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Documenti" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="idDocumento" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *                   &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="TipoOperazione" type="{http://mef.gov.it.v1.npsTypes}doc_tipoOperazione_type" minOccurs="0"/>
 *                   &lt;element name="DaInviare" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                   &lt;element name="IsPrincipale" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                   &lt;element name="idDocumentoOperazione" type="{http://mef.gov.it.v1.npsTypes}Guid" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Operatore" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_aggiungiDocumentiProtocollo_type", propOrder = {
    "protocollo",
    "isCompleto",
    "documenti",
    "operatore"
})
public class RichiestaAggiungiDocumentiProtocolloType
    implements Serializable
{

    @XmlElement(name = "Protocollo", required = true)
    protected IdentificativoProtocolloRequestType protocollo;
    @XmlElement(name = "IsCompleto", defaultValue = "true")
    protected Boolean isCompleto;
    @XmlElement(name = "Documenti", required = true)
    protected List<RichiestaAggiungiDocumentiProtocolloType.Documenti> documenti;
    @XmlElement(name = "Operatore", required = true)
    protected OrgOrganigrammaType operatore;

    /**
     * Recupera il valore della proprietà protocollo.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoProtocolloRequestType }
     *     
     */
    public IdentificativoProtocolloRequestType getProtocollo() {
        return protocollo;
    }

    /**
     * Imposta il valore della proprietà protocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoProtocolloRequestType }
     *     
     */
    public void setProtocollo(IdentificativoProtocolloRequestType value) {
        this.protocollo = value;
    }

    /**
     * Recupera il valore della proprietà isCompleto.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsCompleto() {
        return isCompleto;
    }

    /**
     * Imposta il valore della proprietà isCompleto.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsCompleto(Boolean value) {
        this.isCompleto = value;
    }

    /**
     * Gets the value of the documenti property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the documenti property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocumenti().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RichiestaAggiungiDocumentiProtocolloType.Documenti }
     * 
     * 
     */
    public List<RichiestaAggiungiDocumentiProtocolloType.Documenti> getDocumenti() {
        if (documenti == null) {
            documenti = new ArrayList<RichiestaAggiungiDocumentiProtocolloType.Documenti>();
        }
        return this.documenti;
    }

    /**
     * Recupera il valore della proprietà operatore.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getOperatore() {
        return operatore;
    }

    /**
     * Imposta il valore della proprietà operatore.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setOperatore(OrgOrganigrammaType value) {
        this.operatore = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="idDocumento" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
     *         &lt;element name="Descrizione" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="TipoOperazione" type="{http://mef.gov.it.v1.npsTypes}doc_tipoOperazione_type" minOccurs="0"/>
     *         &lt;element name="DaInviare" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *         &lt;element name="IsPrincipale" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *         &lt;element name="idDocumentoOperazione" type="{http://mef.gov.it.v1.npsTypes}Guid" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "idDocumento",
        "descrizione",
        "tipoOperazione",
        "daInviare",
        "isPrincipale",
        "idDocumentoOperazione"
    })
    public static class Documenti
        implements Serializable
    {

        @XmlElement(required = true)
        protected String idDocumento;
        @XmlElement(name = "Descrizione", required = true)
        protected String descrizione;
        @XmlElement(name = "TipoOperazione")
        @XmlSchemaType(name = "string")
        protected DocTipoOperazioneType tipoOperazione;
        @XmlElement(name = "DaInviare", defaultValue = "true")
        protected Boolean daInviare;
        @XmlElement(name = "IsPrincipale", defaultValue = "true")
        protected boolean isPrincipale;
        protected String idDocumentoOperazione;

        /**
         * Recupera il valore della proprietà idDocumento.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocumento() {
            return idDocumento;
        }

        /**
         * Imposta il valore della proprietà idDocumento.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocumento(String value) {
            this.idDocumento = value;
        }

        /**
         * Recupera il valore della proprietà descrizione.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDescrizione() {
            return descrizione;
        }

        /**
         * Imposta il valore della proprietà descrizione.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDescrizione(String value) {
            this.descrizione = value;
        }

        /**
         * Recupera il valore della proprietà tipoOperazione.
         * 
         * @return
         *     possible object is
         *     {@link DocTipoOperazioneType }
         *     
         */
        public DocTipoOperazioneType getTipoOperazione() {
            return tipoOperazione;
        }

        /**
         * Imposta il valore della proprietà tipoOperazione.
         * 
         * @param value
         *     allowed object is
         *     {@link DocTipoOperazioneType }
         *     
         */
        public void setTipoOperazione(DocTipoOperazioneType value) {
            this.tipoOperazione = value;
        }

        /**
         * Recupera il valore della proprietà daInviare.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isDaInviare() {
            return daInviare;
        }

        /**
         * Imposta il valore della proprietà daInviare.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setDaInviare(Boolean value) {
            this.daInviare = value;
        }

        /**
         * Recupera il valore della proprietà isPrincipale.
         * 
         */
        public boolean isIsPrincipale() {
            return isPrincipale;
        }

        /**
         * Imposta il valore della proprietà isPrincipale.
         * 
         */
        public void setIsPrincipale(boolean value) {
            this.isPrincipale = value;
        }

        /**
         * Recupera il valore della proprietà idDocumentoOperazione.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdDocumentoOperazione() {
            return idDocumentoOperazione;
        }

        /**
         * Imposta il valore della proprietà idDocumentoOperazione.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdDocumentoOperazione(String value) {
            this.idDocumentoOperazione = value;
        }

    }

}
