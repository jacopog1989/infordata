
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.AllDataRangeType;
import npstypes.v1.it.gov.mef.ProtRegistroProtocolloType;


/**
 * Tipo di dato utilizzato nella richiesta di timbro di un documento
 * 
 * <p>Classe Java per richiesta_reportRegistroGiornaliero_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_reportRegistroGiornaliero_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Registro" type="{http://mef.gov.it.v1.npsTypes}prot_registroProtocollo_type"/>
 *         &lt;element name="DataRegistrazione" type="{http://mef.gov.it.v1.npsTypes}all_dataRange_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_reportRegistroGiornaliero_type", propOrder = {
    "registro",
    "dataRegistrazione"
})
public class RichiestaReportRegistroGiornalieroType
    implements Serializable
{

    @XmlElement(name = "Registro", required = true)
    protected ProtRegistroProtocolloType registro;
    @XmlElement(name = "DataRegistrazione", required = true)
    protected AllDataRangeType dataRegistrazione;

    /**
     * Recupera il valore della proprietà registro.
     * 
     * @return
     *     possible object is
     *     {@link ProtRegistroProtocolloType }
     *     
     */
    public ProtRegistroProtocolloType getRegistro() {
        return registro;
    }

    /**
     * Imposta il valore della proprietà registro.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtRegistroProtocolloType }
     *     
     */
    public void setRegistro(ProtRegistroProtocolloType value) {
        this.registro = value;
    }

    /**
     * Recupera il valore della proprietà dataRegistrazione.
     * 
     * @return
     *     possible object is
     *     {@link AllDataRangeType }
     *     
     */
    public AllDataRangeType getDataRegistrazione() {
        return dataRegistrazione;
    }

    /**
     * Imposta il valore della proprietà dataRegistrazione.
     * 
     * @param value
     *     allowed object is
     *     {@link AllDataRangeType }
     *     
     */
    public void setDataRegistrazione(AllDataRangeType value) {
        this.dataRegistrazione = value;
    }

}
