
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.AllDataRangeType;
import npstypes.v1.it.gov.mef.EmailIndirizzoEmailType;
import npstypes.v1.it.gov.mef.EmailStatoSpedizioneType;
import npstypes.v1.it.gov.mef.EmailTipoDettaglioEstrazioneType;
import npstypes.v1.it.gov.mef.OrgCasellaEmailType;
import npstypes.v1.it.gov.mef.ProtIdentificatoreProtocolloType;


/**
 * Tipo di dato per richiedere l'invio delle peo per conoscenza
 * 
 * <p>Classe Java per richiesta_interrogaPostaUscita_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_interrogaPostaUscita_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CasellaEmail" type="{http://mef.gov.it.v1.npsTypes}org_casellaEmail_type"/>
 *         &lt;element name="DataAccodamentoMessaggio" type="{http://mef.gov.it.v1.npsTypes}all_dataRange_type"/>
 *         &lt;element name="DataSpedizioneMessaggio" type="{http://mef.gov.it.v1.npsTypes}all_dataRange_type" minOccurs="0"/>
 *         &lt;element name="OggettoMessaggio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CorpoMessaggio" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/>
 *         &lt;element name="Destinatario" type="{http://mef.gov.it.v1.npsTypes}email_indirizzoEmail_type" minOccurs="0"/>
 *         &lt;element name="SistemaProduttore" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StatoSpedizione" type="{http://mef.gov.it.v1.npsTypes}email_statoSpedizione_type" minOccurs="0"/>
 *         &lt;element name="IdMessaggio" type="{http://mef.gov.it.v1.npsTypes}Guid" minOccurs="0"/>
 *         &lt;element name="CodiMessaggio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CodiceFlusso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Protocollo" type="{http://mef.gov.it.v1.npsTypes}prot_identificatoreProtocollo_type" minOccurs="0"/>
 *         &lt;element name="TipoDettaglioElenco" maxOccurs="unbounded">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://mef.gov.it.v1.npsTypes}email_tipoDettaglioEstrazione_type">
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_interrogaPostaUscita_type", propOrder = {
    "casellaEmail",
    "dataAccodamentoMessaggio",
    "dataSpedizioneMessaggio",
    "oggettoMessaggio",
    "corpoMessaggio",
    "destinatario",
    "sistemaProduttore",
    "statoSpedizione",
    "idMessaggio",
    "codiMessaggio",
    "codiceFlusso",
    "protocollo",
    "tipoDettaglioElenco"
})
public class RichiestaInterrogaPostaUscitaType
    implements Serializable
{

    @XmlElement(name = "CasellaEmail", required = true)
    protected OrgCasellaEmailType casellaEmail;
    @XmlElement(name = "DataAccodamentoMessaggio", required = true)
    protected AllDataRangeType dataAccodamentoMessaggio;
    @XmlElement(name = "DataSpedizioneMessaggio")
    protected AllDataRangeType dataSpedizioneMessaggio;
    @XmlElement(name = "OggettoMessaggio")
    protected String oggettoMessaggio;
    @XmlElement(name = "CorpoMessaggio")
    protected Object corpoMessaggio;
    @XmlElement(name = "Destinatario")
    protected EmailIndirizzoEmailType destinatario;
    @XmlElement(name = "SistemaProduttore", required = true)
    protected String sistemaProduttore;
    @XmlElement(name = "StatoSpedizione")
    @XmlSchemaType(name = "string")
    protected EmailStatoSpedizioneType statoSpedizione;
    @XmlElement(name = "IdMessaggio")
    protected String idMessaggio;
    @XmlElement(name = "CodiMessaggio")
    protected String codiMessaggio;
    @XmlElement(name = "CodiceFlusso")
    protected String codiceFlusso;
    @XmlElement(name = "Protocollo")
    protected ProtIdentificatoreProtocolloType protocollo;
    @XmlElement(name = "TipoDettaglioElenco", required = true)
    protected List<EmailTipoDettaglioEstrazioneType> tipoDettaglioElenco;

    /**
     * Recupera il valore della proprietà casellaEmail.
     * 
     * @return
     *     possible object is
     *     {@link OrgCasellaEmailType }
     *     
     */
    public OrgCasellaEmailType getCasellaEmail() {
        return casellaEmail;
    }

    /**
     * Imposta il valore della proprietà casellaEmail.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgCasellaEmailType }
     *     
     */
    public void setCasellaEmail(OrgCasellaEmailType value) {
        this.casellaEmail = value;
    }

    /**
     * Recupera il valore della proprietà dataAccodamentoMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link AllDataRangeType }
     *     
     */
    public AllDataRangeType getDataAccodamentoMessaggio() {
        return dataAccodamentoMessaggio;
    }

    /**
     * Imposta il valore della proprietà dataAccodamentoMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link AllDataRangeType }
     *     
     */
    public void setDataAccodamentoMessaggio(AllDataRangeType value) {
        this.dataAccodamentoMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà dataSpedizioneMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link AllDataRangeType }
     *     
     */
    public AllDataRangeType getDataSpedizioneMessaggio() {
        return dataSpedizioneMessaggio;
    }

    /**
     * Imposta il valore della proprietà dataSpedizioneMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link AllDataRangeType }
     *     
     */
    public void setDataSpedizioneMessaggio(AllDataRangeType value) {
        this.dataSpedizioneMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà oggettoMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggettoMessaggio() {
        return oggettoMessaggio;
    }

    /**
     * Imposta il valore della proprietà oggettoMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggettoMessaggio(String value) {
        this.oggettoMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà corpoMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getCorpoMessaggio() {
        return corpoMessaggio;
    }

    /**
     * Imposta il valore della proprietà corpoMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setCorpoMessaggio(Object value) {
        this.corpoMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà destinatario.
     * 
     * @return
     *     possible object is
     *     {@link EmailIndirizzoEmailType }
     *     
     */
    public EmailIndirizzoEmailType getDestinatario() {
        return destinatario;
    }

    /**
     * Imposta il valore della proprietà destinatario.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailIndirizzoEmailType }
     *     
     */
    public void setDestinatario(EmailIndirizzoEmailType value) {
        this.destinatario = value;
    }

    /**
     * Recupera il valore della proprietà sistemaProduttore.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSistemaProduttore() {
        return sistemaProduttore;
    }

    /**
     * Imposta il valore della proprietà sistemaProduttore.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSistemaProduttore(String value) {
        this.sistemaProduttore = value;
    }

    /**
     * Recupera il valore della proprietà statoSpedizione.
     * 
     * @return
     *     possible object is
     *     {@link EmailStatoSpedizioneType }
     *     
     */
    public EmailStatoSpedizioneType getStatoSpedizione() {
        return statoSpedizione;
    }

    /**
     * Imposta il valore della proprietà statoSpedizione.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailStatoSpedizioneType }
     *     
     */
    public void setStatoSpedizione(EmailStatoSpedizioneType value) {
        this.statoSpedizione = value;
    }

    /**
     * Recupera il valore della proprietà idMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdMessaggio() {
        return idMessaggio;
    }

    /**
     * Imposta il valore della proprietà idMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdMessaggio(String value) {
        this.idMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà codiMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiMessaggio() {
        return codiMessaggio;
    }

    /**
     * Imposta il valore della proprietà codiMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiMessaggio(String value) {
        this.codiMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà codiceFlusso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceFlusso() {
        return codiceFlusso;
    }

    /**
     * Imposta il valore della proprietà codiceFlusso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceFlusso(String value) {
        this.codiceFlusso = value;
    }

    /**
     * Recupera il valore della proprietà protocollo.
     * 
     * @return
     *     possible object is
     *     {@link ProtIdentificatoreProtocolloType }
     *     
     */
    public ProtIdentificatoreProtocolloType getProtocollo() {
        return protocollo;
    }

    /**
     * Imposta il valore della proprietà protocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtIdentificatoreProtocolloType }
     *     
     */
    public void setProtocollo(ProtIdentificatoreProtocolloType value) {
        this.protocollo = value;
    }

    /**
     * Gets the value of the tipoDettaglioElenco property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the tipoDettaglioElenco property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTipoDettaglioElenco().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EmailTipoDettaglioEstrazioneType }
     * 
     * 
     */
    public List<EmailTipoDettaglioEstrazioneType> getTipoDettaglioElenco() {
        if (tipoDettaglioElenco == null) {
            tipoDettaglioElenco = new ArrayList<EmailTipoDettaglioEstrazioneType>();
        }
        return this.tipoDettaglioElenco;
    }

}
