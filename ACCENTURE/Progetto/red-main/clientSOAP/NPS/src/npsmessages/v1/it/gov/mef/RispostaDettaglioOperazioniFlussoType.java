
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.WkfDatiOperazioneFlussoType;


/**
 * Tipo di dato restituito a seguito della richiesta dettaglio delle operazioni flusso
 * 
 * <p>Classe Java per risposta_dettaglioOperazioniFlusso_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_dettaglioOperazioniFlusso_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsMessages}baseServiceResponse_type">
 *       &lt;sequence>
 *         &lt;element name="Operazione" type="{http://mef.gov.it.v1.npsTypes}wkf_datiOperazioneFlusso_type" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_dettaglioOperazioniFlusso_type", propOrder = {
    "operazione"
})
public class RispostaDettaglioOperazioniFlussoType
    extends BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "Operazione")
    protected List<WkfDatiOperazioneFlussoType> operazione;

    /**
     * Gets the value of the operazione property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the operazione property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOperazione().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link WkfDatiOperazioneFlussoType }
     * 
     * 
     */
    public List<WkfDatiOperazioneFlussoType> getOperazione() {
        if (operazione == null) {
            operazione = new ArrayList<WkfDatiOperazioneFlussoType>();
        }
        return this.operazione;
    }

}
