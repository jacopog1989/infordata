
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.OrgOrganigrammaType;


/**
 * Tipo di dato utilizzato nella richiesta di aggiunta di allacci ad un protocollo esistente
 * 
 * <p>Classe Java per richiesta_aggiungiAllaccioProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_aggiungiAllaccioProtocollo_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdentificativoProtocollo" type="{http://mef.gov.it.v1.npsMessages}identificativoProtocolloRequest_type"/>
 *         &lt;element name="Allaccio" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Protocollo" type="{http://mef.gov.it.v1.npsMessages}identificativoProtocolloRequest_type"/>
 *                   &lt;element name="IsRisposta" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                   &lt;element name="Stato" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                         &lt;enumeration value="AGLI_ATTI"/>
 *                         &lt;enumeration value="CHIUSO"/>
 *                         &lt;enumeration value="SPEDITO"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Operatore" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_aggiungiAllaccioProtocollo_type", propOrder = {
    "identificativoProtocollo",
    "allaccio",
    "operatore"
})
public class RichiestaAggiungiAllaccioProtocolloType
    implements Serializable
{

    @XmlElement(name = "IdentificativoProtocollo", required = true)
    protected IdentificativoProtocolloRequestType identificativoProtocollo;
    @XmlElement(name = "Allaccio", required = true)
    protected List<RichiestaAggiungiAllaccioProtocolloType.Allaccio> allaccio;
    @XmlElement(name = "Operatore", required = true)
    protected OrgOrganigrammaType operatore;

    /**
     * Recupera il valore della proprietà identificativoProtocollo.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoProtocolloRequestType }
     *     
     */
    public IdentificativoProtocolloRequestType getIdentificativoProtocollo() {
        return identificativoProtocollo;
    }

    /**
     * Imposta il valore della proprietà identificativoProtocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoProtocolloRequestType }
     *     
     */
    public void setIdentificativoProtocollo(IdentificativoProtocolloRequestType value) {
        this.identificativoProtocollo = value;
    }

    /**
     * Gets the value of the allaccio property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allaccio property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllaccio().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RichiestaAggiungiAllaccioProtocolloType.Allaccio }
     * 
     * 
     */
    public List<RichiestaAggiungiAllaccioProtocolloType.Allaccio> getAllaccio() {
        if (allaccio == null) {
            allaccio = new ArrayList<RichiestaAggiungiAllaccioProtocolloType.Allaccio>();
        }
        return this.allaccio;
    }

    /**
     * Recupera il valore della proprietà operatore.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getOperatore() {
        return operatore;
    }

    /**
     * Imposta il valore della proprietà operatore.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setOperatore(OrgOrganigrammaType value) {
        this.operatore = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Protocollo" type="{http://mef.gov.it.v1.npsMessages}identificativoProtocolloRequest_type"/>
     *         &lt;element name="IsRisposta" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *         &lt;element name="Stato" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *               &lt;enumeration value="AGLI_ATTI"/>
     *               &lt;enumeration value="CHIUSO"/>
     *               &lt;enumeration value="SPEDITO"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "protocollo",
        "isRisposta",
        "stato"
    })
    public static class Allaccio
        implements Serializable
    {

        @XmlElement(name = "Protocollo", required = true)
        protected IdentificativoProtocolloRequestType protocollo;
        @XmlElement(name = "IsRisposta")
        protected Boolean isRisposta;
        @XmlElement(name = "Stato")
        protected String stato;

        /**
         * Recupera il valore della proprietà protocollo.
         * 
         * @return
         *     possible object is
         *     {@link IdentificativoProtocolloRequestType }
         *     
         */
        public IdentificativoProtocolloRequestType getProtocollo() {
            return protocollo;
        }

        /**
         * Imposta il valore della proprietà protocollo.
         * 
         * @param value
         *     allowed object is
         *     {@link IdentificativoProtocolloRequestType }
         *     
         */
        public void setProtocollo(IdentificativoProtocolloRequestType value) {
            this.protocollo = value;
        }

        /**
         * Recupera il valore della proprietà isRisposta.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isIsRisposta() {
            return isRisposta;
        }

        /**
         * Imposta il valore della proprietà isRisposta.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setIsRisposta(Boolean value) {
            this.isRisposta = value;
        }

        /**
         * Recupera il valore della proprietà stato.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStato() {
            return stato;
        }

        /**
         * Imposta il valore della proprietà stato.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStato(String value) {
            this.stato = value;
        }

    }

}
