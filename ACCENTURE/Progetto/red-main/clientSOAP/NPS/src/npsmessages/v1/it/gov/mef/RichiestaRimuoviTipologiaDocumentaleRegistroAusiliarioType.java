
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.OrgOrganigrammaType;
import npstypes.v1.it.gov.mef.RegIdentificatoreRegistroAusiliarioType;
import npstypes.v1.it.gov.mef.RegTipologiaDocumentoType;


/**
 * Tipo di dato per richiedere la rimozione di una tipologia documentale dal registro
 * 
 * <p>Classe Java per richiesta_rimuoviTipologiaDocumentaleRegistroAusiliario_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_rimuoviTipologiaDocumentaleRegistroAusiliario_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Registro" type="{http://mef.gov.it.v1.npsTypes}reg_identificatoreRegistroAusiliario_type"/>
 *         &lt;element name="TipologiaDocumentale" type="{http://mef.gov.it.v1.npsTypes}reg_tipologiaDocumento_type"/>
 *         &lt;element name="Operatore" type="{http://mef.gov.it.v1.npsTypes}org_organigramma_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_rimuoviTipologiaDocumentaleRegistroAusiliario_type", propOrder = {
    "registro",
    "tipologiaDocumentale",
    "operatore"
})
public class RichiestaRimuoviTipologiaDocumentaleRegistroAusiliarioType
    implements Serializable
{

    @XmlElement(name = "Registro", required = true)
    protected RegIdentificatoreRegistroAusiliarioType registro;
    @XmlElement(name = "TipologiaDocumentale", required = true)
    protected RegTipologiaDocumentoType tipologiaDocumentale;
    @XmlElement(name = "Operatore", required = true)
    protected OrgOrganigrammaType operatore;

    /**
     * Recupera il valore della proprietà registro.
     * 
     * @return
     *     possible object is
     *     {@link RegIdentificatoreRegistroAusiliarioType }
     *     
     */
    public RegIdentificatoreRegistroAusiliarioType getRegistro() {
        return registro;
    }

    /**
     * Imposta il valore della proprietà registro.
     * 
     * @param value
     *     allowed object is
     *     {@link RegIdentificatoreRegistroAusiliarioType }
     *     
     */
    public void setRegistro(RegIdentificatoreRegistroAusiliarioType value) {
        this.registro = value;
    }

    /**
     * Recupera il valore della proprietà tipologiaDocumentale.
     * 
     * @return
     *     possible object is
     *     {@link RegTipologiaDocumentoType }
     *     
     */
    public RegTipologiaDocumentoType getTipologiaDocumentale() {
        return tipologiaDocumentale;
    }

    /**
     * Imposta il valore della proprietà tipologiaDocumentale.
     * 
     * @param value
     *     allowed object is
     *     {@link RegTipologiaDocumentoType }
     *     
     */
    public void setTipologiaDocumentale(RegTipologiaDocumentoType value) {
        this.tipologiaDocumentale = value;
    }

    /**
     * Recupera il valore della proprietà operatore.
     * 
     * @return
     *     possible object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public OrgOrganigrammaType getOperatore() {
        return operatore;
    }

    /**
     * Imposta il valore della proprietà operatore.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgOrganigrammaType }
     *     
     */
    public void setOperatore(OrgOrganigrammaType value) {
        this.operatore = value;
    }

}
