
package npsmessages.v1.it.gov.mef;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the npsmessages.v1.it.gov.mef package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RispostaInviaAtto_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_inviaAtto");
    private final static QName _RispostaRitiroAutotutela_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_ritiroAutotutela");
    private final static QName _RichiestaRitiroAutotutela_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_ritiroAutotutela");
    private final static QName _RispostaDettaglioOperazioniFlusso_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_dettaglioOperazioniFlusso");
    private final static QName _RichiestaMonitoraggioOperazioniSospeso_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_monitoraggioOperazioniSospeso");
    private final static QName _RichiestaInviaAtto_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_inviaAtto");
    private final static QName _RispostaInviaComunicazioneGenerica_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_inviaComunicazioneGenerica");
    private final static QName _RichiestaInviaDocumentazioneGenerica_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_inviaDocumentazioneGenerica");
    private final static QName _RichiestaAcquisisciDocumento_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_acquisisciDocumento");
    private final static QName _RispostaAcquisisciDocumento_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_acquisisciDocumento");
    private final static QName _RichiestaValidazioneStatoFlusso_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_validazioneStato_Flusso");
    private final static QName _RispostaMonitoraggioOperazioniSospeso_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_monitoraggioOperazioniSospeso");
    private final static QName _RichiestaInviaComunicazioneGenerica_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_inviaComunicazioneGenerica");
    private final static QName _RispostaInviaDocumentazioneGenerica_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_inviaDocumentazioneGenerica");
    private final static QName _RichiestaDettaglioOperazioniFlusso_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_dettaglioOperazioniFlusso");
    private final static QName _RispostaInviaDocumentazioneIntegrativa_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_inviaDocumentazioneIntegrativa");

    private final static QName _RispostaCambiaStatoProtocolloEntrata_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_cambiaStatoProtocolloEntrata");
    private final static QName _RichiestaRimuoviAssegnatarioProtocolloEntrata_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_rimuoviAssegnatarioProtocolloEntrata");
    private final static QName _RichiestaCambiaStatoProtocolloEntrata_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_cambiaStatoProtocolloEntrata");
    private final static QName _RispostaProtocollaCoda_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_protocollaCoda");
    private final static QName _RichiestaReportRegistroGiornaliero_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_reportRegistroGiornaliero");
    private final static QName _RichiestaCreateProtocolloEntrata_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_createProtocolloEntrata");
    private final static QName _RichiestaUploadDocumento_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_uploadDocumento");
    private final static QName _RichiestaAggiungiDocumentoProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_aggiungiDocumentoProtocollo");
    private final static QName _RispostaDownloadDocumento_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_downloadDocumento");
    private final static QName _RichiestaAggiungiDocumentiProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_aggiungiDocumentiProtocollo");
    private final static QName _RichiestaRicercaProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_ricercaProtocollo");
    private final static QName _RichiestaRimuoviAnnotazioneProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_rimuoviAnnotazioneProtocollo");
    private final static QName _RichiestaRimuoviAllaccioProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_rimuoviAllaccioProtocollo");
    private final static QName _RichiestaUploadAtmos_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_uploadAtmos");
    private final static QName _RispostaAggiornaAclProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_aggiornaAclProtocollo");
    private final static QName _RispostaAggiungiAssegnatarioProtocolloEntrata_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_aggiungiAssegnatarioProtocolloEntrata");
    private final static QName _RichiestaCambiaAssegnazioneApplicazioneProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_cambiaAssegnazioneApplicazioneProtocollo");
    private final static QName _RispostaRimuoviAllaccioProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_rimuoviAllaccioProtocollo");
    private final static QName _RispostaAggiungiAssegnazioneApplicazioneProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_aggiungiAssegnazioneApplicazioneProtocollo");
    private final static QName _RispostaAnnullamentoProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_annullamentoProtocollo");
    private final static QName _RispostaConvertiDocumento_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_convertiDocumento");
    private final static QName _RispostaCambiaAssegnazioneApplicazioneProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_cambiaAssegnazioneApplicazioneProtocollo");
    private final static QName _RispostaCreateProtocolloEntrata_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_createProtocolloEntrata");
    private final static QName _RichiestaCreateProtocolloUscita_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_createProtocolloUscita");
    private final static QName _RispostaAggiungiDocumentoProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_aggiungiDocumentoProtocollo");
    private final static QName _RispostaRispondiAProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_rispondiAProtocollo");
    private final static QName _RispostaRimuoviAssegnazioneApplicazioneProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_rimuoviAssegnazioneApplicazioneProtocollo");
    private final static QName _RichiestaAggiornaAclProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_aggiornaAclProtocollo");
    private final static QName _RichiestaCambiaStatoAssegnazioneApplicazioneProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_cambiaStatoAssegnazioneApplicazioneProtocollo");
    private final static QName _RispostaRimuoviAssegnatarioProtocolloEntrata_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_rimuoviAssegnatarioProtocolloEntrata");
    private final static QName _RispostaUploadAtmos_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_uploadAtmos");
    private final static QName _RispostaAggiungiDocumentiProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_aggiungiDocumentiProtocollo");
    private final static QName _RichiestaAggiungiAssegnatarioProtocolloEntrata_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_aggiungiAssegnatarioProtocolloEntrata");
    private final static QName _RichiestaReportProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_reportProtocollo");
    private final static QName _RispostaAggiungiDestinatarioProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_aggiungiDestinatarioProtocollo");
    private final static QName _RispostaAggiungiAllaccioProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_aggiungiAllaccioProtocollo");
    private final static QName _RispostaRimuoviDocumentoProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_rimuoviDocumentoProtocollo");
    private final static QName _RispostaGetProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_getProtocollo");
    private final static QName _RichiestaConvertiDocumento_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_convertiDocumento");
    private final static QName _RichiestaAggiungiAnnotazioneProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_aggiungiAnnotazioneProtocollo");
    private final static QName _RichiestaProtocollaCoda_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_protocollaCoda");
    private final static QName _RichiestaAggiungiAssegnazioneApplicazioneProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_aggiungiAssegnazioneApplicazioneProtocollo");
    private final static QName _RispostaRicercaProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_ricercaProtocollo");
    private final static QName _RichiestaRimuoviDestinatarioProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_rimuoviDestinatarioProtocollo");
    private final static QName _RichiestaModificaSpedizioneDestinatarioProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_modificaSpedizioneDestinatarioProtocollo");
    private final static QName _RispostaRimuoviDestinatarioProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_rimuoviDestinatarioProtocollo");
    private final static QName _RichiestaCambiaStatoProtocolloUscita_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_cambiaStatoProtocolloUscita");
    private final static QName _RispostaCambiaStatoAssegnazioneApplicazioneProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_cambiaStatoAssegnazioneApplicazioneProtocollo");
    private final static QName _RichiestaDownloadAtmos_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_downloadAtmos");
    private final static QName _RichiestaRimuoviDocumentoProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_rimuoviDocumentoProtocollo");
    private final static QName _RichiestaDownloadDocumento_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_downloadDocumento");
    private final static QName _RispostaGetProtocolloEntrata_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_getProtocolloEntrata");
    private final static QName _RichiestaAggiungiAllaccioProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_aggiungiAllaccioProtocollo");
    private final static QName _RispostaTimbroDocumento_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_timbroDocumento");
    private final static QName _RichiestaRispondiAProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_rispondiAProtocollo");
    private final static QName _RichiestaGetProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_getProtocollo");
    private final static QName _RichiestaTimbroDocumento_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_timbroDocumento");
    private final static QName _RichiestaUpdateDatiProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_updateDatiProtocollo");
    private final static QName _RispostaCambiaStatoProtocolloUscita_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_cambiaStatoProtocolloUscita");
    private final static QName _RispostaModificaSpedizioneDestinatarioProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_modificaSpedizioneDestinatarioProtocollo");
    private final static QName _RispostaReportProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_reportProtocollo");
    private final static QName _RispostaCreateProtocolloUscita_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_createProtocolloUscita");
    private final static QName _RispostaUpdateDatiProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_updateDatiProtocollo");
    private final static QName _RispostaDownloadAtmos_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_downloadAtmos");
    private final static QName _RispostaUploadDocumento_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_uploadDocumento");
    private final static QName _RispostaSpedisciProtocolloUscita_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_spedisciProtocolloUscita");
    private final static QName _RichiestaRimuoviAssegnazioneApplicazioneProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_rimuoviAssegnazioneApplicazioneProtocollo");
    private final static QName _RichiestaSpedisciProtocolloUscita_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_spedisciProtocolloUscita");
    private final static QName _RispostaGetProtocolloUscita_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_getProtocolloUscita");
    private final static QName _RispostaAggiungiAnnotazioneProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_aggiungiAnnotazioneProtocollo");
    private final static QName _RichiestaGetProtocolloEntrata_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_getProtocolloEntrata");
    private final static QName _RichiestaAggiungiDestinatarioProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_aggiungiDestinatarioProtocollo");
    private final static QName _RispostaRimuoviAnnotazioneProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_rimuoviAnnotazioneProtocollo");
    private final static QName _RichiestaAnnullamentoProtocollo_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_annullamentoProtocollo");
    private final static QName _RispostaReportRegistroGiornaliero_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_reportRegistroGiornaliero");
   
    private final static QName _RichiestaCreateRegistroAusiliario_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_createRegistroAusiliario");
    private final static QName _RichiestaApriRegistroAusiliario_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_apriRegistroAusiliario");
    private final static QName _RispostaRicercaRegistroAusiliario_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_ricercaRegistroAusiliario");
    private final static QName _RichiestaRimuoviApplicazioneRegistroAusiliario_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_rimuoviApplicazioneRegistroAusiliario");
    private final static QName _RichiestaAggiungiDocumentoRegistrazioneAusiliaria_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_aggiungiDocumentoRegistrazioneAusiliaria");
    private final static QName _RichiestaRimuoviTipologiaDocumentaleRegistroAusiliario_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_rimuoviTipologiaDocumentaleRegistroAusiliario");
    private final static QName _RispostaCollegaRegistrazioneAusiliariaProtocolloIngresso_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_collegaRegistrazioneAusiliariaProtocolloIngresso");
    private final static QName _RichiestaAssegnaApplicazioneRegistroAusiliario_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_assegnaApplicazioneRegistroAusiliario");
    private final static QName _RispostaSbloccaRegistroAusiliario_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_sbloccaRegistroAusiliario");
    private final static QName _RichiestaBloccaRegistroAusiliario_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_bloccaRegistroAusiliario");
    private final static QName _RispostaBloccaRegistroAusiliario_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_bloccaRegistroAusiliario");
    private final static QName _RichiestaRimuoviDocumentoRegistrazioneAusiliaria_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_rimuoviDocumentoRegistrazioneAusiliaria");
    private final static QName _RispostaAssegnaApplicazioneRegistroAusiliario_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_assegnaApplicazioneRegistroAusiliario");
    private final static QName _RichiestaRicercaRegistrazioneAusiliaria_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_ricercaRegistrazioneAusiliaria");
    private final static QName _RichiestaCreateRegistrazioneAusiliaria_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_createRegistrazioneAusiliaria");
    private final static QName _RichiestaCollegaRegistrazioneAusiliariaProtocolloUscita_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_collegaRegistrazioneAusiliariaProtocolloUscita");
    private final static QName _RichiestaUpdateRegistrazioneAusiliaria_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_updateRegistrazioneAusiliaria");
    private final static QName _RispostaCreateRegistrazioneAusiliaria_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_createRegistrazioneAusiliaria");
    private final static QName _RispostaCollegaRegistrazioneAusiliariaProtocolloUscita_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_collegaRegistrazioneAusiliariaProtocolloUscita");
    private final static QName _RispostaRimuoviTipologiaDocumentaleRegistroAusiliario_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_rimuoviTipologiaDocumentaleRegistroAusiliario");
    private final static QName _RispostaUpdateRegistroAusiliario_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_updateRegistroAusiliario");
    private final static QName _RispostaApriRegistroAusiliario_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_apriRegistroAusiliario");
    private final static QName _RispostaGetRegistrazioneAusiliaria_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_getRegistrazioneAusiliaria");
    private final static QName _RispostaAggiungiDocumentoRegistrazioneAusiliaria_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_aggiungiDocumentoRegistrazioneAusiliaria");
    private final static QName _RichiestaUpdateRegistroAusiliario_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_updateRegistroAusiliario");
    private final static QName _RispostaUpdateRegistrazioneAusiliaria_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_updateRegistrazioneAusiliaria");
    private final static QName _RichiestaChiudiRegistroAusiliario_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_chiudiRegistroAusiliario");
    private final static QName _RispostaAssociaTipologiaDocumentaleRegistroAusiliario_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_associaTipologiaDocumentaleRegistroAusiliario");
    private final static QName _RispostaCreateRegistroAusiliario_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_createRegistroAusiliario");
    private final static QName _RispostaChiudiRegistroAusiliario_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_chiudiRegistroAusiliario");
    private final static QName _RispostaRimuoviApplicazioneRegistroAusiliario_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_rimuoviApplicazioneRegistroAusiliario");
    private final static QName _RispostaRicercaRegistrazioneAusiliaria_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_ricercaRegistrazioneAusiliaria");
    private final static QName _RispostaRimuoviDocumentoRegistrazioneAusiliaria_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_rimuoviDocumentoRegistrazioneAusiliaria");
    private final static QName _RichiestaCollegaRegistrazioneAusiliariaProtocolloIngresso_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_collegaRegistrazioneAusiliariaProtocolloIngresso");
    private final static QName _RichiestaRicercaRegistroAusiliario_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_ricercaRegistroAusiliario");
    private final static QName _RichiestaGetRegistrazioneAusiliaria_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_getRegistrazioneAusiliaria");
    private final static QName _RispostaAnnullaRegistrazioneAusiliaria_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_annullaRegistrazioneAusiliaria");
    private final static QName _RichiestaSbloccaRegistroAusiliario_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_sbloccaRegistroAusiliario");
    private final static QName _RichiestaAssociaTipologiaDocumentaleRegistroAusiliario_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_associaTipologiaDocumentaleRegistroAusiliario");
    private final static QName _RichiestaCreateRegistrazioneAusiliariaTypeDataRegistrazione_QNAME = new QName("", "DataRegistrazione");
    
    //START VI
    private final static QName _RispostaRielaboraPECRicevuta_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_rielaboraPECRicevuta");
    private final static QName _RichiestaRielaboraPECRicevuta_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_rielaboraPECRicevuta");
    private final static QName _RichiestaDettaglioPEC_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_dettaglioPEC");
    private final static QName _RispostaDettaglioPEC_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_dettaglioPEC");
    private final static QName _RichiestaInterrogaPostaUscita_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_interrogaPostaUscita");
    private final static QName _RispostaRielaboraInvioPEC_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_rielaboraInvioPEC");
    private final static QName _RichiestaInterrogaPostaIngresso_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_interrogaPostaIngresso");
    private final static QName _RichiestaRielaboraInvioPEC_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_rielaboraInvioPEC");
    private final static QName _RichiestaInviaMessaggioPosta_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "richiesta_inviaMessaggioPosta");
    private final static QName _RispostaInviaMessaggioPosta_QNAME = new QName("http://mef.gov.it.v1.npsMessages", "risposta_inviaMessaggioPosta");
    //END VI
    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: npsmessages.v1.it.gov.mef
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RichiestaInviaAttoType }
     * 
     */
    public RichiestaInviaAttoType createRichiestaInviaAttoType() {
        return new RichiestaInviaAttoType();
    }

    /**
     * Create an instance of {@link RispostaDettaglioOperazioniFlussoType }
     * 
     */
    public RispostaDettaglioOperazioniFlussoType createRispostaDettaglioOperazioniFlussoType() {
        return new RispostaDettaglioOperazioniFlussoType();
    }

    /**
     * Create an instance of {@link RichiestaMonitoraggioOperazioniSospesoType }
     * 
     */
    public RichiestaMonitoraggioOperazioniSospesoType createRichiestaMonitoraggioOperazioniSospesoType() {
        return new RichiestaMonitoraggioOperazioniSospesoType();
    }

    /**
     * Create an instance of {@link RispostaAcquisisciDocumentoType }
     * 
     */
    public RispostaAcquisisciDocumentoType createRispostaAcquisisciDocumentoType() {
        return new RispostaAcquisisciDocumentoType();
    }

    /**
     * Create an instance of {@link RispostaRitiroAutotutelaType }
     * 
     */
    public RispostaRitiroAutotutelaType createRispostaRitiroAutotutelaType() {
        return new RispostaRitiroAutotutelaType();
    }

    /**
     * Create an instance of {@link RichiestaValidazioneStatoFlussoType }
     * 
     */
    public RichiestaValidazioneStatoFlussoType createRichiestaValidazioneStatoFlussoType() {
        return new RichiestaValidazioneStatoFlussoType();
    }

    /**
     * Create an instance of {@link RichiestaInviaDocumentazioneIntegrativa }
     * 
     */
    public RichiestaInviaDocumentazioneIntegrativa createRichiestaInviaDocumentazioneIntegrativa() {
        return new RichiestaInviaDocumentazioneIntegrativa();
    }

    /**
     * Create an instance of {@link RichiestaInviaDocumentazioneIntegrativaType }
     * 
     */
    public RichiestaInviaDocumentazioneIntegrativaType createRichiestaInviaDocumentazioneIntegrativaType() {
        return new RichiestaInviaDocumentazioneIntegrativaType();
    }

    /**
     * Create an instance of {@link RispostaValidazioneStatoFlusso }
     * 
     */
    public RispostaValidazioneStatoFlusso createRispostaValidazioneStatoFlusso() {
        return new RispostaValidazioneStatoFlusso();
    }

    /**
     * Create an instance of {@link RispostaValidazioneStatoFlussoType }
     * 
     */
    public RispostaValidazioneStatoFlussoType createRispostaValidazioneStatoFlussoType() {
        return new RispostaValidazioneStatoFlussoType();
    }

    /**
     * Create an instance of {@link ServiceErrorType }
     * 
     */
    public ServiceErrorType createServiceErrorType() {
        return new ServiceErrorType();
    }

    /**
     * Create an instance of {@link RichiestaRitiroAutotutelaType }
     * 
     */
    public RichiestaRitiroAutotutelaType createRichiestaRitiroAutotutelaType() {
        return new RichiestaRitiroAutotutelaType();
    }

    /**
     * Create an instance of {@link RichiestaInviaDocumentazioneGenericaType }
     * 
     */
    public RichiestaInviaDocumentazioneGenericaType createRichiestaInviaDocumentazioneGenericaType() {
        return new RichiestaInviaDocumentazioneGenericaType();
    }

    /**
     * Create an instance of {@link RispostaMonitoraggioOperazioniSospesoType }
     * 
     */
    public RispostaMonitoraggioOperazioniSospesoType createRispostaMonitoraggioOperazioniSospesoType() {
        return new RispostaMonitoraggioOperazioniSospesoType();
    }

    /**
     * Create an instance of {@link RichiestaInviaComunicazioneGenericaType }
     * 
     */
    public RichiestaInviaComunicazioneGenericaType createRichiestaInviaComunicazioneGenericaType() {
        return new RichiestaInviaComunicazioneGenericaType();
    }

    /**
     * Create an instance of {@link RichiestaAcquisisciDocumentoType }
     * 
     */
    public RichiestaAcquisisciDocumentoType createRichiestaAcquisisciDocumentoType() {
        return new RichiestaAcquisisciDocumentoType();
    }

    /**
     * Create an instance of {@link RispostaInviaComunicazioneGenericaType }
     * 
     */
    public RispostaInviaComunicazioneGenericaType createRispostaInviaComunicazioneGenericaType() {
        return new RispostaInviaComunicazioneGenericaType();
    }

    /**
     * Create an instance of {@link RispostaInviaAttoType }
     * 
     */
    public RispostaInviaAttoType createRispostaInviaAttoType() {
        return new RispostaInviaAttoType();
    }

    /**
     * Create an instance of {@link RispostaInviaDocumentazioneIntegrativaType }
     * 
     */
    public RispostaInviaDocumentazioneIntegrativaType createRispostaInviaDocumentazioneIntegrativaType() {
        return new RispostaInviaDocumentazioneIntegrativaType();
    }

    /**
     * Create an instance of {@link RispostaInviaDocumentazioneGenericaType }
     * 
     */
    public RispostaInviaDocumentazioneGenericaType createRispostaInviaDocumentazioneGenericaType() {
        return new RispostaInviaDocumentazioneGenericaType();
    }

    /**
     * Create an instance of {@link RichiestaDettaglioOperazioniFlussoType }
     * 
     */
    public RichiestaDettaglioOperazioniFlussoType createRichiestaDettaglioOperazioniFlussoType() {
        return new RichiestaDettaglioOperazioniFlussoType();
    }

    /**
     * Create an instance of {@link IdentificativoDestinatarioType }
     * 
     */
    public IdentificativoDestinatarioType createIdentificativoDestinatarioType() {
        return new IdentificativoDestinatarioType();
    }

    /**
     * Create an instance of {@link IdentificativoAssegnatarioType }
     * 
     */
    public IdentificativoAssegnatarioType createIdentificativoAssegnatarioType() {
        return new IdentificativoAssegnatarioType();
    }

    /**
     * Create an instance of {@link IdentificativoDocumentoRequestType }
     * 
     */
    public IdentificativoDocumentoRequestType createIdentificativoDocumentoRequestType() {
        return new IdentificativoDocumentoRequestType();
    }

    /**
     * Create an instance of {@link IdentificativoAnnotazioneType }
     * 
     */
    public IdentificativoAnnotazioneType createIdentificativoAnnotazioneType() {
        return new IdentificativoAnnotazioneType();
    }

    /**
     * Create an instance of {@link IdentificativoProtocolloRequestType }
     * 
     */
    public IdentificativoProtocolloRequestType createIdentificativoProtocolloRequestType() {
        return new IdentificativoProtocolloRequestType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaInviaAttoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_inviaAtto")
    public JAXBElement<RispostaInviaAttoType> createRispostaInviaAtto(RispostaInviaAttoType value) {
        return new JAXBElement<RispostaInviaAttoType>(_RispostaInviaAtto_QNAME, RispostaInviaAttoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRitiroAutotutelaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_ritiroAutotutela")
    public JAXBElement<RispostaRitiroAutotutelaType> createRispostaRitiroAutotutela(RispostaRitiroAutotutelaType value) {
        return new JAXBElement<RispostaRitiroAutotutelaType>(_RispostaRitiroAutotutela_QNAME, RispostaRitiroAutotutelaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRitiroAutotutelaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_ritiroAutotutela")
    public JAXBElement<RichiestaRitiroAutotutelaType> createRichiestaRitiroAutotutela(RichiestaRitiroAutotutelaType value) {
        return new JAXBElement<RichiestaRitiroAutotutelaType>(_RichiestaRitiroAutotutela_QNAME, RichiestaRitiroAutotutelaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaDettaglioOperazioniFlussoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_dettaglioOperazioniFlusso")
    public JAXBElement<RispostaDettaglioOperazioniFlussoType> createRispostaDettaglioOperazioniFlusso(RispostaDettaglioOperazioniFlussoType value) {
        return new JAXBElement<RispostaDettaglioOperazioniFlussoType>(_RispostaDettaglioOperazioniFlusso_QNAME, RispostaDettaglioOperazioniFlussoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaMonitoraggioOperazioniSospesoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_monitoraggioOperazioniSospeso")
    public JAXBElement<RichiestaMonitoraggioOperazioniSospesoType> createRichiestaMonitoraggioOperazioniSospeso(RichiestaMonitoraggioOperazioniSospesoType value) {
        return new JAXBElement<RichiestaMonitoraggioOperazioniSospesoType>(_RichiestaMonitoraggioOperazioniSospeso_QNAME, RichiestaMonitoraggioOperazioniSospesoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaInviaAttoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_inviaAtto")
    public JAXBElement<RichiestaInviaAttoType> createRichiestaInviaAtto(RichiestaInviaAttoType value) {
        return new JAXBElement<RichiestaInviaAttoType>(_RichiestaInviaAtto_QNAME, RichiestaInviaAttoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaInviaComunicazioneGenericaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_inviaComunicazioneGenerica")
    public JAXBElement<RispostaInviaComunicazioneGenericaType> createRispostaInviaComunicazioneGenerica(RispostaInviaComunicazioneGenericaType value) {
        return new JAXBElement<RispostaInviaComunicazioneGenericaType>(_RispostaInviaComunicazioneGenerica_QNAME, RispostaInviaComunicazioneGenericaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaInviaDocumentazioneGenericaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_inviaDocumentazioneGenerica")
    public JAXBElement<RichiestaInviaDocumentazioneGenericaType> createRichiestaInviaDocumentazioneGenerica(RichiestaInviaDocumentazioneGenericaType value) {
        return new JAXBElement<RichiestaInviaDocumentazioneGenericaType>(_RichiestaInviaDocumentazioneGenerica_QNAME, RichiestaInviaDocumentazioneGenericaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAcquisisciDocumentoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_acquisisciDocumento")
    public JAXBElement<RichiestaAcquisisciDocumentoType> createRichiestaAcquisisciDocumento(RichiestaAcquisisciDocumentoType value) {
        return new JAXBElement<RichiestaAcquisisciDocumentoType>(_RichiestaAcquisisciDocumento_QNAME, RichiestaAcquisisciDocumentoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaAcquisisciDocumentoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_acquisisciDocumento")
    public JAXBElement<RispostaAcquisisciDocumentoType> createRispostaAcquisisciDocumento(RispostaAcquisisciDocumentoType value) {
        return new JAXBElement<RispostaAcquisisciDocumentoType>(_RispostaAcquisisciDocumento_QNAME, RispostaAcquisisciDocumentoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaValidazioneStatoFlussoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_validazioneStato_Flusso")
    public JAXBElement<RichiestaValidazioneStatoFlussoType> createRichiestaValidazioneStatoFlusso(RichiestaValidazioneStatoFlussoType value) {
        return new JAXBElement<RichiestaValidazioneStatoFlussoType>(_RichiestaValidazioneStatoFlusso_QNAME, RichiestaValidazioneStatoFlussoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaMonitoraggioOperazioniSospesoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_monitoraggioOperazioniSospeso")
    public JAXBElement<RispostaMonitoraggioOperazioniSospesoType> createRispostaMonitoraggioOperazioniSospeso(RispostaMonitoraggioOperazioniSospesoType value) {
        return new JAXBElement<RispostaMonitoraggioOperazioniSospesoType>(_RispostaMonitoraggioOperazioniSospeso_QNAME, RispostaMonitoraggioOperazioniSospesoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaInviaComunicazioneGenericaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_inviaComunicazioneGenerica")
    public JAXBElement<RichiestaInviaComunicazioneGenericaType> createRichiestaInviaComunicazioneGenerica(RichiestaInviaComunicazioneGenericaType value) {
        return new JAXBElement<RichiestaInviaComunicazioneGenericaType>(_RichiestaInviaComunicazioneGenerica_QNAME, RichiestaInviaComunicazioneGenericaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaInviaDocumentazioneGenericaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_inviaDocumentazioneGenerica")
    public JAXBElement<RispostaInviaDocumentazioneGenericaType> createRispostaInviaDocumentazioneGenerica(RispostaInviaDocumentazioneGenericaType value) {
        return new JAXBElement<RispostaInviaDocumentazioneGenericaType>(_RispostaInviaDocumentazioneGenerica_QNAME, RispostaInviaDocumentazioneGenericaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaDettaglioOperazioniFlussoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_dettaglioOperazioniFlusso")
    public JAXBElement<RichiestaDettaglioOperazioniFlussoType> createRichiestaDettaglioOperazioniFlusso(RichiestaDettaglioOperazioniFlussoType value) {
        return new JAXBElement<RichiestaDettaglioOperazioniFlussoType>(_RichiestaDettaglioOperazioniFlusso_QNAME, RichiestaDettaglioOperazioniFlussoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaInviaDocumentazioneIntegrativaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_inviaDocumentazioneIntegrativa")
    public JAXBElement<RispostaInviaDocumentazioneIntegrativaType> createRispostaInviaDocumentazioneIntegrativa(RispostaInviaDocumentazioneIntegrativaType value) {
        return new JAXBElement<RispostaInviaDocumentazioneIntegrativaType>(_RispostaInviaDocumentazioneIntegrativa_QNAME, RispostaInviaDocumentazioneIntegrativaType.class, null, value);
    }
    
    /**
     * Create an instance of {@link RispostaReportRegistroGiornalieroType }
     * 
     */
    public RispostaReportRegistroGiornalieroType createRispostaReportRegistroGiornalieroType() {
        return new RispostaReportRegistroGiornalieroType();
    }

    /**
     * Create an instance of {@link RichiestaUpdateDatiProtocolloType }
     * 
     */
    public RichiestaUpdateDatiProtocolloType createRichiestaUpdateDatiProtocolloType() {
        return new RichiestaUpdateDatiProtocolloType();
    }

    /**
     * Create an instance of {@link RichiestaUpdateDatiProtocolloType.DatiModifica }
     * 
     */
    public RichiestaUpdateDatiProtocolloType.DatiModifica createRichiestaUpdateDatiProtocolloTypeDatiModifica() {
        return new RichiestaUpdateDatiProtocolloType.DatiModifica();
    }

    /**
     * Create an instance of {@link RichiestaUpdateDatiProtocolloType.DatiModifica.SegnaturaEstesa }
     * 
     */
    public RichiestaUpdateDatiProtocolloType.DatiModifica.SegnaturaEstesa createRichiestaUpdateDatiProtocolloTypeDatiModificaSegnaturaEstesa() {
        return new RichiestaUpdateDatiProtocolloType.DatiModifica.SegnaturaEstesa();
    }

    /**
     * Create an instance of {@link RichiestaTimbroDocumentoType }
     * 
     */
    public RichiestaTimbroDocumentoType createRichiestaTimbroDocumentoType() {
        return new RichiestaTimbroDocumentoType();
    }

    /**
     * Create an instance of {@link RispostaTimbroDocumentoType }
     * 
     */
    public RispostaTimbroDocumentoType createRispostaTimbroDocumentoType() {
        return new RispostaTimbroDocumentoType();
    }

    /**
     * Create an instance of {@link RichiestaRispondiAProtocolloType }
     * 
     */
    public RichiestaRispondiAProtocolloType createRichiestaRispondiAProtocolloType() {
        return new RichiestaRispondiAProtocolloType();
    }

    /**
     * Create an instance of {@link RichiestaRispondiAProtocolloType.SegnaturaEstesa }
     * 
     */
    public RichiestaRispondiAProtocolloType.SegnaturaEstesa createRichiestaRispondiAProtocolloTypeSegnaturaEstesa() {
        return new RichiestaRispondiAProtocolloType.SegnaturaEstesa();
    }

    /**
     * Create an instance of {@link RichiestaAggiungiAllaccioProtocolloType }
     * 
     */
    public RichiestaAggiungiAllaccioProtocolloType createRichiestaAggiungiAllaccioProtocolloType() {
        return new RichiestaAggiungiAllaccioProtocolloType();
    }

    /**
     * Create an instance of {@link RichiestaRicercaProtocolloType }
     * 
     */
    public RichiestaRicercaProtocolloType createRichiestaRicercaProtocolloType() {
        return new RichiestaRicercaProtocolloType();
    }

    /**
     * Create an instance of {@link RichiestaAggiungiDocumentiProtocolloType }
     * 
     */
    public RichiestaAggiungiDocumentiProtocolloType createRichiestaAggiungiDocumentiProtocolloType() {
        return new RichiestaAggiungiDocumentiProtocolloType();
    }

    /**
     * Create an instance of {@link RichiestaAggiungiDocumentoProtocolloType }
     * 
     */
    public RichiestaAggiungiDocumentoProtocolloType createRichiestaAggiungiDocumentoProtocolloType() {
        return new RichiestaAggiungiDocumentoProtocolloType();
    }

    /**
     * Create an instance of {@link RichiestaModificaSpedizioneDestinatarioProtocolloType }
     * 
     */
    public RichiestaModificaSpedizioneDestinatarioProtocolloType createRichiestaModificaSpedizioneDestinatarioProtocolloType() {
        return new RichiestaModificaSpedizioneDestinatarioProtocolloType();
    }

    /**
     * Create an instance of {@link RichiestaProtocollaCodaType }
     * 
     */
    public RichiestaProtocollaCodaType createRichiestaProtocollaCodaType() {
        return new RichiestaProtocollaCodaType();
    }

    /**
     * Create an instance of {@link RichiestaProtocollaCodaType.SegnaturaEstesa }
     * 
     */
    public RichiestaProtocollaCodaType.SegnaturaEstesa createRichiestaProtocollaCodaTypeSegnaturaEstesa() {
        return new RichiestaProtocollaCodaType.SegnaturaEstesa();
    }

    /**
     * Create an instance of {@link RispostaCambiaStatoProtocolloEntrataType }
     * 
     */
    public RispostaCambiaStatoProtocolloEntrataType createRispostaCambiaStatoProtocolloEntrataType() {
        return new RispostaCambiaStatoProtocolloEntrataType();
    }

    /**
     * Create an instance of {@link RichiestaAggiungiAssegnazioneApplicazioneProtocolloType }
     * 
     */
    public RichiestaAggiungiAssegnazioneApplicazioneProtocolloType createRichiestaAggiungiAssegnazioneApplicazioneProtocolloType() {
        return new RichiestaAggiungiAssegnazioneApplicazioneProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaRicercaProtocolloType }
     * 
     */
    public RispostaRicercaProtocolloType createRispostaRicercaProtocolloType() {
        return new RispostaRicercaProtocolloType();
    }

    /**
     * Create an instance of {@link RichiestaRimuoviAssegnatarioProtocolloEntrataType }
     * 
     */
    public RichiestaRimuoviAssegnatarioProtocolloEntrataType createRichiestaRimuoviAssegnatarioProtocolloEntrataType() {
        return new RichiestaRimuoviAssegnatarioProtocolloEntrataType();
    }

    /**
     * Create an instance of {@link RichiestaRimuoviDestinatarioProtocolloType }
     * 
     */
    public RichiestaRimuoviDestinatarioProtocolloType createRichiestaRimuoviDestinatarioProtocolloType() {
        return new RichiestaRimuoviDestinatarioProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaRimuoviDestinatarioProtocolloType }
     * 
     */
    public RispostaRimuoviDestinatarioProtocolloType createRispostaRimuoviDestinatarioProtocolloType() {
        return new RispostaRimuoviDestinatarioProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaProtocollaCodaType }
     * 
     */
    public RispostaProtocollaCodaType createRispostaProtocollaCodaType() {
        return new RispostaProtocollaCodaType();
    }

    /**
     * Create an instance of {@link RichiestaCambiaStatoProtocolloUscitaType }
     * 
     */
    public RichiestaCambiaStatoProtocolloUscitaType createRichiestaCambiaStatoProtocolloUscitaType() {
        return new RichiestaCambiaStatoProtocolloUscitaType();
    }

    /**
     * Create an instance of {@link RichiestaReportRegistroGiornalieroType }
     * 
     */
    public RichiestaReportRegistroGiornalieroType createRichiestaReportRegistroGiornalieroType() {
        return new RichiestaReportRegistroGiornalieroType();
    }

    /**
     * Create an instance of {@link RichiestaCambiaStatoProtocolloEntrataType }
     * 
     */
    public RichiestaCambiaStatoProtocolloEntrataType createRichiestaCambiaStatoProtocolloEntrataType() {
        return new RichiestaCambiaStatoProtocolloEntrataType();
    }

    /**
     * Create an instance of {@link RispostaCambiaStatoAssegnazioneApplicazioneProtocolloType }
     * 
     */
    public RispostaCambiaStatoAssegnazioneApplicazioneProtocolloType createRispostaCambiaStatoAssegnazioneApplicazioneProtocolloType() {
        return new RispostaCambiaStatoAssegnazioneApplicazioneProtocolloType();
    }

    /**
     * Create an instance of {@link RichiestaUploadDocumentoType }
     * 
     */
    public RichiestaUploadDocumentoType createRichiestaUploadDocumentoType() {
        return new RichiestaUploadDocumentoType();
    }

    /**
     * Create an instance of {@link RichiestaCreateProtocolloEntrataType }
     * 
     */
    public RichiestaCreateProtocolloEntrataType createRichiestaCreateProtocolloEntrataType() {
        return new RichiestaCreateProtocolloEntrataType();
    }

    /**
     * Create an instance of {@link RispostaDownloadDocumentoType }
     * 
     */
    public RispostaDownloadDocumentoType createRispostaDownloadDocumentoType() {
        return new RispostaDownloadDocumentoType();
    }

    /**
     * Create an instance of {@link RichiestaDownloadAtmosType }
     * 
     */
    public RichiestaDownloadAtmosType createRichiestaDownloadAtmosType() {
        return new RichiestaDownloadAtmosType();
    }

    /**
     * Create an instance of {@link RichiestaRimuoviDocumentoProtocolloType }
     * 
     */
    public RichiestaRimuoviDocumentoProtocolloType createRichiestaRimuoviDocumentoProtocolloType() {
        return new RichiestaRimuoviDocumentoProtocolloType();
    }

    /**
     * Create an instance of {@link RichiestaDownloadDocumentoType }
     * 
     */
    public RichiestaDownloadDocumentoType createRichiestaDownloadDocumentoType() {
        return new RichiestaDownloadDocumentoType();
    }

    /**
     * Create an instance of {@link RichiestaRimuoviAnnotazioneProtocolloType }
     * 
     */
    public RichiestaRimuoviAnnotazioneProtocolloType createRichiestaRimuoviAnnotazioneProtocolloType() {
        return new RichiestaRimuoviAnnotazioneProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaGetProtocolloEntrataType }
     * 
     */
    public RispostaGetProtocolloEntrataType createRispostaGetProtocolloEntrataType() {
        return new RispostaGetProtocolloEntrataType();
    }

    /**
     * Create an instance of {@link RichiestaRimuoviAllaccioProtocolloType }
     * 
     */
    public RichiestaRimuoviAllaccioProtocolloType createRichiestaRimuoviAllaccioProtocolloType() {
        return new RichiestaRimuoviAllaccioProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaAggiornaAclProtocolloType }
     * 
     */
    public RispostaAggiornaAclProtocolloType createRispostaAggiornaAclProtocolloType() {
        return new RispostaAggiornaAclProtocolloType();
    }

    /**
     * Create an instance of {@link RichiestaUploadAtmosType }
     * 
     */
    public RichiestaUploadAtmosType createRichiestaUploadAtmosType() {
        return new RichiestaUploadAtmosType();
    }

    /**
     * Create an instance of {@link RispostaAggiungiAssegnatarioProtocolloEntrataType }
     * 
     */
    public RispostaAggiungiAssegnatarioProtocolloEntrataType createRispostaAggiungiAssegnatarioProtocolloEntrataType() {
        return new RispostaAggiungiAssegnatarioProtocolloEntrataType();
    }

    /**
     * Create an instance of {@link RichiestaGetProtocolloType }
     * 
     */
    public RichiestaGetProtocolloType createRichiestaGetProtocolloType() {
        return new RichiestaGetProtocolloType();
    }

    /**
     * Create an instance of {@link RichiestaCambiaAssegnazioneApplicazioneProtocolloType }
     * 
     */
    public RichiestaCambiaAssegnazioneApplicazioneProtocolloType createRichiestaCambiaAssegnazioneApplicazioneProtocolloType() {
        return new RichiestaCambiaAssegnazioneApplicazioneProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaRimuoviAllaccioProtocolloType }
     * 
     */
    public RispostaRimuoviAllaccioProtocolloType createRispostaRimuoviAllaccioProtocolloType() {
        return new RispostaRimuoviAllaccioProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaAnnullamentoProtocolloType }
     * 
     */
    public RispostaAnnullamentoProtocolloType createRispostaAnnullamentoProtocolloType() {
        return new RispostaAnnullamentoProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaConvertiDocumentoType }
     * 
     */
    public RispostaConvertiDocumentoType createRispostaConvertiDocumentoType() {
        return new RispostaConvertiDocumentoType();
    }

    /**
     * Create an instance of {@link RispostaAggiungiAssegnazioneApplicazioneProtocolloType }
     * 
     */
    public RispostaAggiungiAssegnazioneApplicazioneProtocolloType createRispostaAggiungiAssegnazioneApplicazioneProtocolloType() {
        return new RispostaAggiungiAssegnazioneApplicazioneProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaCambiaAssegnazioneApplicazioneProtocolloType }
     * 
     */
    public RispostaCambiaAssegnazioneApplicazioneProtocolloType createRispostaCambiaAssegnazioneApplicazioneProtocolloType() {
        return new RispostaCambiaAssegnazioneApplicazioneProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaModificaSpedizioneDestinatarioProtocolloType }
     * 
     */
    public RispostaModificaSpedizioneDestinatarioProtocolloType createRispostaModificaSpedizioneDestinatarioProtocolloType() {
        return new RispostaModificaSpedizioneDestinatarioProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaCambiaStatoProtocolloUscitaType }
     * 
     */
    public RispostaCambiaStatoProtocolloUscitaType createRispostaCambiaStatoProtocolloUscitaType() {
        return new RispostaCambiaStatoProtocolloUscitaType();
    }

    /**
     * Create an instance of {@link RispostaCreateProtocolloEntrataType }
     * 
     */
    public RispostaCreateProtocolloEntrataType createRispostaCreateProtocolloEntrataType() {
        return new RispostaCreateProtocolloEntrataType();
    }

    /**
     * Create an instance of {@link RispostaReportProtocolloType }
     * 
     */
    public RispostaReportProtocolloType createRispostaReportProtocolloType() {
        return new RispostaReportProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaAggiungiDocumentoProtocolloType }
     * 
     */
    public RispostaAggiungiDocumentoProtocolloType createRispostaAggiungiDocumentoProtocolloType() {
        return new RispostaAggiungiDocumentoProtocolloType();
    }

    /**
     * Create an instance of {@link RichiestaCreateProtocolloUscitaType }
     * 
     */
    public RichiestaCreateProtocolloUscitaType createRichiestaCreateProtocolloUscitaType() {
        return new RichiestaCreateProtocolloUscitaType();
    }

    /**
     * Create an instance of {@link RichiestaAggiornaAclProtocolloType }
     * 
     */
    public RichiestaAggiornaAclProtocolloType createRichiestaAggiornaAclProtocolloType() {
        return new RichiestaAggiornaAclProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaRispondiAProtocolloType }
     * 
     */
    public RispostaRispondiAProtocolloType createRispostaRispondiAProtocolloType() {
        return new RispostaRispondiAProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaRimuoviAssegnazioneApplicazioneProtocolloType }
     * 
     */
    public RispostaRimuoviAssegnazioneApplicazioneProtocolloType createRispostaRimuoviAssegnazioneApplicazioneProtocolloType() {
        return new RispostaRimuoviAssegnazioneApplicazioneProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaCreateProtocolloUscitaType }
     * 
     */
    public RispostaCreateProtocolloUscitaType createRispostaCreateProtocolloUscitaType() {
        return new RispostaCreateProtocolloUscitaType();
    }

    /**
     * Create an instance of {@link RispostaDownloadAtmosType }
     * 
     */
    public RispostaDownloadAtmosType createRispostaDownloadAtmosType() {
        return new RispostaDownloadAtmosType();
    }

    /**
     * Create an instance of {@link RichiestaCambiaStatoAssegnazioneApplicazioneProtocolloType }
     * 
     */
    public RichiestaCambiaStatoAssegnazioneApplicazioneProtocolloType createRichiestaCambiaStatoAssegnazioneApplicazioneProtocolloType() {
        return new RichiestaCambiaStatoAssegnazioneApplicazioneProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaUpdateDatiProtocolloType }
     * 
     */
    public RispostaUpdateDatiProtocolloType createRispostaUpdateDatiProtocolloType() {
        return new RispostaUpdateDatiProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaSpedisciProtocolloUscitaType }
     * 
     */
    public RispostaSpedisciProtocolloUscitaType createRispostaSpedisciProtocolloUscitaType() {
        return new RispostaSpedisciProtocolloUscitaType();
    }

    /**
     * Create an instance of {@link RichiestaRimuoviAssegnazioneApplicazioneProtocolloType }
     * 
     */
    public RichiestaRimuoviAssegnazioneApplicazioneProtocolloType createRichiestaRimuoviAssegnazioneApplicazioneProtocolloType() {
        return new RichiestaRimuoviAssegnazioneApplicazioneProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaUploadAtmosType }
     * 
     */
    public RispostaUploadAtmosType createRispostaUploadAtmosType() {
        return new RispostaUploadAtmosType();
    }

    /**
     * Create an instance of {@link RispostaRimuoviAssegnatarioProtocolloEntrataType }
     * 
     */
    public RispostaRimuoviAssegnatarioProtocolloEntrataType createRispostaRimuoviAssegnatarioProtocolloEntrataType() {
        return new RispostaRimuoviAssegnatarioProtocolloEntrataType();
    }

    /**
     * Create an instance of {@link RispostaUploadDocumentoType }
     * 
     */
    public RispostaUploadDocumentoType createRispostaUploadDocumentoType() {
        return new RispostaUploadDocumentoType();
    }

    /**
     * Create an instance of {@link RichiestaSpedisciProtocolloUscitaType }
     * 
     */
    public RichiestaSpedisciProtocolloUscitaType createRichiestaSpedisciProtocolloUscitaType() {
        return new RichiestaSpedisciProtocolloUscitaType();
    }

    /**
     * Create an instance of {@link RispostaGetProtocolloUscitaType }
     * 
     */
    public RispostaGetProtocolloUscitaType createRispostaGetProtocolloUscitaType() {
        return new RispostaGetProtocolloUscitaType();
    }

    /**
     * Create an instance of {@link RichiestaAggiungiAssegnatarioProtocolloEntrataType }
     * 
     */
    public RichiestaAggiungiAssegnatarioProtocolloEntrataType createRichiestaAggiungiAssegnatarioProtocolloEntrataType() {
        return new RichiestaAggiungiAssegnatarioProtocolloEntrataType();
    }

    /**
     * Create an instance of {@link RichiestaReportProtocolloType }
     * 
     */
    public RichiestaReportProtocolloType createRichiestaReportProtocolloType() {
        return new RichiestaReportProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaAggiungiDocumentiProtocolloType }
     * 
     */
    public RispostaAggiungiDocumentiProtocolloType createRispostaAggiungiDocumentiProtocolloType() {
        return new RispostaAggiungiDocumentiProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaRimuoviDocumentoProtocolloType }
     * 
     */
    public RispostaRimuoviDocumentoProtocolloType createRispostaRimuoviDocumentoProtocolloType() {
        return new RispostaRimuoviDocumentoProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaAggiungiDestinatarioProtocolloType }
     * 
     */
    public RispostaAggiungiDestinatarioProtocolloType createRispostaAggiungiDestinatarioProtocolloType() {
        return new RispostaAggiungiDestinatarioProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaAggiungiAllaccioProtocolloType }
     * 
     */
    public RispostaAggiungiAllaccioProtocolloType createRispostaAggiungiAllaccioProtocolloType() {
        return new RispostaAggiungiAllaccioProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaGetProtocolloType }
     * 
     */
    public RispostaGetProtocolloType createRispostaGetProtocolloType() {
        return new RispostaGetProtocolloType();
    }

    /**
     * Create an instance of {@link RichiestaConvertiDocumentoType }
     * 
     */
    public RichiestaConvertiDocumentoType createRichiestaConvertiDocumentoType() {
        return new RichiestaConvertiDocumentoType();
    }

    /**
     * Create an instance of {@link RichiestaGetProtocolloEntrataType }
     * 
     */
    public RichiestaGetProtocolloEntrataType createRichiestaGetProtocolloEntrataType() {
        return new RichiestaGetProtocolloEntrataType();
    }

    /**
     * Create an instance of {@link RichiestaGetProtocolloUscita }
     * 
     */
    public RichiestaGetProtocolloUscita createRichiestaGetProtocolloUscita() {
        return new RichiestaGetProtocolloUscita();
    }

    /**
     * Create an instance of {@link RichiestaGetProtocolloUscitaType }
     * 
     */
    public RichiestaGetProtocolloUscitaType createRichiestaGetProtocolloUscitaType() {
        return new RichiestaGetProtocolloUscitaType();
    }

    /**
     * Create an instance of {@link RispostaRimuoviAnnotazioneProtocolloType }
     * 
     */
    public RispostaRimuoviAnnotazioneProtocolloType createRispostaRimuoviAnnotazioneProtocolloType() {
        return new RispostaRimuoviAnnotazioneProtocolloType();
    }

    /**
     * Create an instance of {@link RichiestaAggiungiDestinatarioProtocolloType }
     * 
     */
    public RichiestaAggiungiDestinatarioProtocolloType createRichiestaAggiungiDestinatarioProtocolloType() {
        return new RichiestaAggiungiDestinatarioProtocolloType();
    }

    /**
     * Create an instance of {@link RichiestaAggiungiAnnotazioneProtocolloType }
     * 
     */
    public RichiestaAggiungiAnnotazioneProtocolloType createRichiestaAggiungiAnnotazioneProtocolloType() {
        return new RichiestaAggiungiAnnotazioneProtocolloType();
    }

    /**
     * Create an instance of {@link RichiestaAnnullamentoProtocolloType }
     * 
     */
    public RichiestaAnnullamentoProtocolloType createRichiestaAnnullamentoProtocolloType() {
        return new RichiestaAnnullamentoProtocolloType();
    }

    /**
     * Create an instance of {@link IdentificativoDocumentiRequestType }
     * 
     */
    public IdentificativoDocumentiRequestType createIdentificativoDocumentiRequestType() {
        return new IdentificativoDocumentiRequestType();
    }

    /**
     * Create an instance of {@link IdentificativoDocumentiEsterniRequestType }
     * 
     */
    public IdentificativoDocumentiEsterniRequestType createIdentificativoDocumentiEsterniRequestType() {
        return new IdentificativoDocumentiEsterniRequestType();
    }

    /**
     * Create an instance of {@link RispostaAggiungiAnnotazioneProtocolloType }
     * 
     */
    public RispostaAggiungiAnnotazioneProtocolloType createRispostaAggiungiAnnotazioneProtocolloType() {
        return new RispostaAggiungiAnnotazioneProtocolloType();
    }

    /**
     * Create an instance of {@link RispostaReportRegistroGiornalieroType.Report }
     * 
     */
    public RispostaReportRegistroGiornalieroType.Report createRispostaReportRegistroGiornalieroTypeReport() {
        return new RispostaReportRegistroGiornalieroType.Report();
    }

    /**
     * Create an instance of {@link RichiestaUpdateDatiProtocolloType.DatiModifica.SegnaturaEstesa.Intestazione }
     * 
     */
    public RichiestaUpdateDatiProtocolloType.DatiModifica.SegnaturaEstesa.Intestazione createRichiestaUpdateDatiProtocolloTypeDatiModificaSegnaturaEstesaIntestazione() {
        return new RichiestaUpdateDatiProtocolloType.DatiModifica.SegnaturaEstesa.Intestazione();
    }

    /**
     * Create an instance of {@link RichiestaTimbroDocumentoType.Documento }
     * 
     */
    public RichiestaTimbroDocumentoType.Documento createRichiestaTimbroDocumentoTypeDocumento() {
        return new RichiestaTimbroDocumentoType.Documento();
    }

    /**
     * Create an instance of {@link RispostaTimbroDocumentoType.DatiDocumento }
     * 
     */
    public RispostaTimbroDocumentoType.DatiDocumento createRispostaTimbroDocumentoTypeDatiDocumento() {
        return new RispostaTimbroDocumentoType.DatiDocumento();
    }

    /**
     * Create an instance of {@link RichiestaRispondiAProtocolloType.Documenti }
     * 
     */
    public RichiestaRispondiAProtocolloType.Documenti createRichiestaRispondiAProtocolloTypeDocumenti() {
        return new RichiestaRispondiAProtocolloType.Documenti();
    }

    /**
     * Create an instance of {@link RichiestaRispondiAProtocolloType.SegnaturaEstesa.Intestazione }
     * 
     */
    public RichiestaRispondiAProtocolloType.SegnaturaEstesa.Intestazione createRichiestaRispondiAProtocolloTypeSegnaturaEstesaIntestazione() {
        return new RichiestaRispondiAProtocolloType.SegnaturaEstesa.Intestazione();
    }

    /**
     * Create an instance of {@link RichiestaAggiungiAllaccioProtocolloType.Allaccio }
     * 
     */
    public RichiestaAggiungiAllaccioProtocolloType.Allaccio createRichiestaAggiungiAllaccioProtocolloTypeAllaccio() {
        return new RichiestaAggiungiAllaccioProtocolloType.Allaccio();
    }

    /**
     * Create an instance of {@link RichiestaRicercaProtocolloType.DatiRicerca }
     * 
     */
    public RichiestaRicercaProtocolloType.DatiRicerca createRichiestaRicercaProtocolloTypeDatiRicerca() {
        return new RichiestaRicercaProtocolloType.DatiRicerca();
    }

    /**
     * Create an instance of {@link RichiestaAggiungiDocumentiProtocolloType.Documenti }
     * 
     */
    public RichiestaAggiungiDocumentiProtocolloType.Documenti createRichiestaAggiungiDocumentiProtocolloTypeDocumenti() {
        return new RichiestaAggiungiDocumentiProtocolloType.Documenti();
    }

    /**
     * Create an instance of {@link RichiestaAggiungiDocumentoProtocolloType.DocumentoEsistente }
     * 
     */
    public RichiestaAggiungiDocumentoProtocolloType.DocumentoEsistente createRichiestaAggiungiDocumentoProtocolloTypeDocumentoEsistente() {
        return new RichiestaAggiungiDocumentoProtocolloType.DocumentoEsistente();
    }

    /**
     * Create an instance of {@link RichiestaModificaSpedizioneDestinatarioProtocolloType.DatiDestinatario }
     * 
     */
    public RichiestaModificaSpedizioneDestinatarioProtocolloType.DatiDestinatario createRichiestaModificaSpedizioneDestinatarioProtocolloTypeDatiDestinatario() {
        return new RichiestaModificaSpedizioneDestinatarioProtocolloType.DatiDestinatario();
    }

    /**
     * Create an instance of {@link RichiestaProtocollaCodaType.Documenti }
     * 
     */
    public RichiestaProtocollaCodaType.Documenti createRichiestaProtocollaCodaTypeDocumenti() {
        return new RichiestaProtocollaCodaType.Documenti();
    }

    /**
     * Create an instance of {@link RichiestaProtocollaCodaType.SegnaturaEstesa.Intestazione }
     * 
     */
    public RichiestaProtocollaCodaType.SegnaturaEstesa.Intestazione createRichiestaProtocollaCodaTypeSegnaturaEstesaIntestazione() {
        return new RichiestaProtocollaCodaType.SegnaturaEstesa.Intestazione();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaCambiaStatoProtocolloEntrataType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_cambiaStatoProtocolloEntrata")
    public JAXBElement<RispostaCambiaStatoProtocolloEntrataType> createRispostaCambiaStatoProtocolloEntrata(RispostaCambiaStatoProtocolloEntrataType value) {
        return new JAXBElement<RispostaCambiaStatoProtocolloEntrataType>(_RispostaCambiaStatoProtocolloEntrata_QNAME, RispostaCambiaStatoProtocolloEntrataType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRimuoviAssegnatarioProtocolloEntrataType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_rimuoviAssegnatarioProtocolloEntrata")
    public JAXBElement<RichiestaRimuoviAssegnatarioProtocolloEntrataType> createRichiestaRimuoviAssegnatarioProtocolloEntrata(RichiestaRimuoviAssegnatarioProtocolloEntrataType value) {
        return new JAXBElement<RichiestaRimuoviAssegnatarioProtocolloEntrataType>(_RichiestaRimuoviAssegnatarioProtocolloEntrata_QNAME, RichiestaRimuoviAssegnatarioProtocolloEntrataType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaCambiaStatoProtocolloEntrataType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_cambiaStatoProtocolloEntrata")
    public JAXBElement<RichiestaCambiaStatoProtocolloEntrataType> createRichiestaCambiaStatoProtocolloEntrata(RichiestaCambiaStatoProtocolloEntrataType value) {
        return new JAXBElement<RichiestaCambiaStatoProtocolloEntrataType>(_RichiestaCambiaStatoProtocolloEntrata_QNAME, RichiestaCambiaStatoProtocolloEntrataType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaProtocollaCodaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_protocollaCoda")
    public JAXBElement<RispostaProtocollaCodaType> createRispostaProtocollaCoda(RispostaProtocollaCodaType value) {
        return new JAXBElement<RispostaProtocollaCodaType>(_RispostaProtocollaCoda_QNAME, RispostaProtocollaCodaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaReportRegistroGiornalieroType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_reportRegistroGiornaliero")
    public JAXBElement<RichiestaReportRegistroGiornalieroType> createRichiestaReportRegistroGiornaliero(RichiestaReportRegistroGiornalieroType value) {
        return new JAXBElement<RichiestaReportRegistroGiornalieroType>(_RichiestaReportRegistroGiornaliero_QNAME, RichiestaReportRegistroGiornalieroType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaCreateProtocolloEntrataType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_createProtocolloEntrata")
    public JAXBElement<RichiestaCreateProtocolloEntrataType> createRichiestaCreateProtocolloEntrata(RichiestaCreateProtocolloEntrataType value) {
        return new JAXBElement<RichiestaCreateProtocolloEntrataType>(_RichiestaCreateProtocolloEntrata_QNAME, RichiestaCreateProtocolloEntrataType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaUploadDocumentoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_uploadDocumento")
    public JAXBElement<RichiestaUploadDocumentoType> createRichiestaUploadDocumento(RichiestaUploadDocumentoType value) {
        return new JAXBElement<RichiestaUploadDocumentoType>(_RichiestaUploadDocumento_QNAME, RichiestaUploadDocumentoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAggiungiDocumentoProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_aggiungiDocumentoProtocollo")
    public JAXBElement<RichiestaAggiungiDocumentoProtocolloType> createRichiestaAggiungiDocumentoProtocollo(RichiestaAggiungiDocumentoProtocolloType value) {
        return new JAXBElement<RichiestaAggiungiDocumentoProtocolloType>(_RichiestaAggiungiDocumentoProtocollo_QNAME, RichiestaAggiungiDocumentoProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaDownloadDocumentoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_downloadDocumento")
    public JAXBElement<RispostaDownloadDocumentoType> createRispostaDownloadDocumento(RispostaDownloadDocumentoType value) {
        return new JAXBElement<RispostaDownloadDocumentoType>(_RispostaDownloadDocumento_QNAME, RispostaDownloadDocumentoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAggiungiDocumentiProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_aggiungiDocumentiProtocollo")
    public JAXBElement<RichiestaAggiungiDocumentiProtocolloType> createRichiestaAggiungiDocumentiProtocollo(RichiestaAggiungiDocumentiProtocolloType value) {
        return new JAXBElement<RichiestaAggiungiDocumentiProtocolloType>(_RichiestaAggiungiDocumentiProtocollo_QNAME, RichiestaAggiungiDocumentiProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRicercaProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_ricercaProtocollo")
    public JAXBElement<RichiestaRicercaProtocolloType> createRichiestaRicercaProtocollo(RichiestaRicercaProtocolloType value) {
        return new JAXBElement<RichiestaRicercaProtocolloType>(_RichiestaRicercaProtocollo_QNAME, RichiestaRicercaProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRimuoviAnnotazioneProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_rimuoviAnnotazioneProtocollo")
    public JAXBElement<RichiestaRimuoviAnnotazioneProtocolloType> createRichiestaRimuoviAnnotazioneProtocollo(RichiestaRimuoviAnnotazioneProtocolloType value) {
        return new JAXBElement<RichiestaRimuoviAnnotazioneProtocolloType>(_RichiestaRimuoviAnnotazioneProtocollo_QNAME, RichiestaRimuoviAnnotazioneProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRimuoviAllaccioProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_rimuoviAllaccioProtocollo")
    public JAXBElement<RichiestaRimuoviAllaccioProtocolloType> createRichiestaRimuoviAllaccioProtocollo(RichiestaRimuoviAllaccioProtocolloType value) {
        return new JAXBElement<RichiestaRimuoviAllaccioProtocolloType>(_RichiestaRimuoviAllaccioProtocollo_QNAME, RichiestaRimuoviAllaccioProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaUploadAtmosType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_uploadAtmos")
    public JAXBElement<RichiestaUploadAtmosType> createRichiestaUploadAtmos(RichiestaUploadAtmosType value) {
        return new JAXBElement<RichiestaUploadAtmosType>(_RichiestaUploadAtmos_QNAME, RichiestaUploadAtmosType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaAggiornaAclProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_aggiornaAclProtocollo")
    public JAXBElement<RispostaAggiornaAclProtocolloType> createRispostaAggiornaAclProtocollo(RispostaAggiornaAclProtocolloType value) {
        return new JAXBElement<RispostaAggiornaAclProtocolloType>(_RispostaAggiornaAclProtocollo_QNAME, RispostaAggiornaAclProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaAggiungiAssegnatarioProtocolloEntrataType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_aggiungiAssegnatarioProtocolloEntrata")
    public JAXBElement<RispostaAggiungiAssegnatarioProtocolloEntrataType> createRispostaAggiungiAssegnatarioProtocolloEntrata(RispostaAggiungiAssegnatarioProtocolloEntrataType value) {
        return new JAXBElement<RispostaAggiungiAssegnatarioProtocolloEntrataType>(_RispostaAggiungiAssegnatarioProtocolloEntrata_QNAME, RispostaAggiungiAssegnatarioProtocolloEntrataType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaCambiaAssegnazioneApplicazioneProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_cambiaAssegnazioneApplicazioneProtocollo")
    public JAXBElement<RichiestaCambiaAssegnazioneApplicazioneProtocolloType> createRichiestaCambiaAssegnazioneApplicazioneProtocollo(RichiestaCambiaAssegnazioneApplicazioneProtocolloType value) {
        return new JAXBElement<RichiestaCambiaAssegnazioneApplicazioneProtocolloType>(_RichiestaCambiaAssegnazioneApplicazioneProtocollo_QNAME, RichiestaCambiaAssegnazioneApplicazioneProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRimuoviAllaccioProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_rimuoviAllaccioProtocollo")
    public JAXBElement<RispostaRimuoviAllaccioProtocolloType> createRispostaRimuoviAllaccioProtocollo(RispostaRimuoviAllaccioProtocolloType value) {
        return new JAXBElement<RispostaRimuoviAllaccioProtocolloType>(_RispostaRimuoviAllaccioProtocollo_QNAME, RispostaRimuoviAllaccioProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaAggiungiAssegnazioneApplicazioneProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_aggiungiAssegnazioneApplicazioneProtocollo")
    public JAXBElement<RispostaAggiungiAssegnazioneApplicazioneProtocolloType> createRispostaAggiungiAssegnazioneApplicazioneProtocollo(RispostaAggiungiAssegnazioneApplicazioneProtocolloType value) {
        return new JAXBElement<RispostaAggiungiAssegnazioneApplicazioneProtocolloType>(_RispostaAggiungiAssegnazioneApplicazioneProtocollo_QNAME, RispostaAggiungiAssegnazioneApplicazioneProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaAnnullamentoProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_annullamentoProtocollo")
    public JAXBElement<RispostaAnnullamentoProtocolloType> createRispostaAnnullamentoProtocollo(RispostaAnnullamentoProtocolloType value) {
        return new JAXBElement<RispostaAnnullamentoProtocolloType>(_RispostaAnnullamentoProtocollo_QNAME, RispostaAnnullamentoProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaConvertiDocumentoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_convertiDocumento")
    public JAXBElement<RispostaConvertiDocumentoType> createRispostaConvertiDocumento(RispostaConvertiDocumentoType value) {
        return new JAXBElement<RispostaConvertiDocumentoType>(_RispostaConvertiDocumento_QNAME, RispostaConvertiDocumentoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaCambiaAssegnazioneApplicazioneProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_cambiaAssegnazioneApplicazioneProtocollo")
    public JAXBElement<RispostaCambiaAssegnazioneApplicazioneProtocolloType> createRispostaCambiaAssegnazioneApplicazioneProtocollo(RispostaCambiaAssegnazioneApplicazioneProtocolloType value) {
        return new JAXBElement<RispostaCambiaAssegnazioneApplicazioneProtocolloType>(_RispostaCambiaAssegnazioneApplicazioneProtocollo_QNAME, RispostaCambiaAssegnazioneApplicazioneProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaCreateProtocolloEntrataType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_createProtocolloEntrata")
    public JAXBElement<RispostaCreateProtocolloEntrataType> createRispostaCreateProtocolloEntrata(RispostaCreateProtocolloEntrataType value) {
        return new JAXBElement<RispostaCreateProtocolloEntrataType>(_RispostaCreateProtocolloEntrata_QNAME, RispostaCreateProtocolloEntrataType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaCreateProtocolloUscitaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_createProtocolloUscita")
    public JAXBElement<RichiestaCreateProtocolloUscitaType> createRichiestaCreateProtocolloUscita(RichiestaCreateProtocolloUscitaType value) {
        return new JAXBElement<RichiestaCreateProtocolloUscitaType>(_RichiestaCreateProtocolloUscita_QNAME, RichiestaCreateProtocolloUscitaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaAggiungiDocumentoProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_aggiungiDocumentoProtocollo")
    public JAXBElement<RispostaAggiungiDocumentoProtocolloType> createRispostaAggiungiDocumentoProtocollo(RispostaAggiungiDocumentoProtocolloType value) {
        return new JAXBElement<RispostaAggiungiDocumentoProtocolloType>(_RispostaAggiungiDocumentoProtocollo_QNAME, RispostaAggiungiDocumentoProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRispondiAProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_rispondiAProtocollo")
    public JAXBElement<RispostaRispondiAProtocolloType> createRispostaRispondiAProtocollo(RispostaRispondiAProtocolloType value) {
        return new JAXBElement<RispostaRispondiAProtocolloType>(_RispostaRispondiAProtocollo_QNAME, RispostaRispondiAProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRimuoviAssegnazioneApplicazioneProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_rimuoviAssegnazioneApplicazioneProtocollo")
    public JAXBElement<RispostaRimuoviAssegnazioneApplicazioneProtocolloType> createRispostaRimuoviAssegnazioneApplicazioneProtocollo(RispostaRimuoviAssegnazioneApplicazioneProtocolloType value) {
        return new JAXBElement<RispostaRimuoviAssegnazioneApplicazioneProtocolloType>(_RispostaRimuoviAssegnazioneApplicazioneProtocollo_QNAME, RispostaRimuoviAssegnazioneApplicazioneProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAggiornaAclProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_aggiornaAclProtocollo")
    public JAXBElement<RichiestaAggiornaAclProtocolloType> createRichiestaAggiornaAclProtocollo(RichiestaAggiornaAclProtocolloType value) {
        return new JAXBElement<RichiestaAggiornaAclProtocolloType>(_RichiestaAggiornaAclProtocollo_QNAME, RichiestaAggiornaAclProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaCambiaStatoAssegnazioneApplicazioneProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_cambiaStatoAssegnazioneApplicazioneProtocollo")
    public JAXBElement<RichiestaCambiaStatoAssegnazioneApplicazioneProtocolloType> createRichiestaCambiaStatoAssegnazioneApplicazioneProtocollo(RichiestaCambiaStatoAssegnazioneApplicazioneProtocolloType value) {
        return new JAXBElement<RichiestaCambiaStatoAssegnazioneApplicazioneProtocolloType>(_RichiestaCambiaStatoAssegnazioneApplicazioneProtocollo_QNAME, RichiestaCambiaStatoAssegnazioneApplicazioneProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRimuoviAssegnatarioProtocolloEntrataType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_rimuoviAssegnatarioProtocolloEntrata")
    public JAXBElement<RispostaRimuoviAssegnatarioProtocolloEntrataType> createRispostaRimuoviAssegnatarioProtocolloEntrata(RispostaRimuoviAssegnatarioProtocolloEntrataType value) {
        return new JAXBElement<RispostaRimuoviAssegnatarioProtocolloEntrataType>(_RispostaRimuoviAssegnatarioProtocolloEntrata_QNAME, RispostaRimuoviAssegnatarioProtocolloEntrataType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaUploadAtmosType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_uploadAtmos")
    public JAXBElement<RispostaUploadAtmosType> createRispostaUploadAtmos(RispostaUploadAtmosType value) {
        return new JAXBElement<RispostaUploadAtmosType>(_RispostaUploadAtmos_QNAME, RispostaUploadAtmosType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaAggiungiDocumentiProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_aggiungiDocumentiProtocollo")
    public JAXBElement<RispostaAggiungiDocumentiProtocolloType> createRispostaAggiungiDocumentiProtocollo(RispostaAggiungiDocumentiProtocolloType value) {
        return new JAXBElement<RispostaAggiungiDocumentiProtocolloType>(_RispostaAggiungiDocumentiProtocollo_QNAME, RispostaAggiungiDocumentiProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAggiungiAssegnatarioProtocolloEntrataType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_aggiungiAssegnatarioProtocolloEntrata")
    public JAXBElement<RichiestaAggiungiAssegnatarioProtocolloEntrataType> createRichiestaAggiungiAssegnatarioProtocolloEntrata(RichiestaAggiungiAssegnatarioProtocolloEntrataType value) {
        return new JAXBElement<RichiestaAggiungiAssegnatarioProtocolloEntrataType>(_RichiestaAggiungiAssegnatarioProtocolloEntrata_QNAME, RichiestaAggiungiAssegnatarioProtocolloEntrataType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaReportProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_reportProtocollo")
    public JAXBElement<RichiestaReportProtocolloType> createRichiestaReportProtocollo(RichiestaReportProtocolloType value) {
        return new JAXBElement<RichiestaReportProtocolloType>(_RichiestaReportProtocollo_QNAME, RichiestaReportProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaAggiungiDestinatarioProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_aggiungiDestinatarioProtocollo")
    public JAXBElement<RispostaAggiungiDestinatarioProtocolloType> createRispostaAggiungiDestinatarioProtocollo(RispostaAggiungiDestinatarioProtocolloType value) {
        return new JAXBElement<RispostaAggiungiDestinatarioProtocolloType>(_RispostaAggiungiDestinatarioProtocollo_QNAME, RispostaAggiungiDestinatarioProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaAggiungiAllaccioProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_aggiungiAllaccioProtocollo")
    public JAXBElement<RispostaAggiungiAllaccioProtocolloType> createRispostaAggiungiAllaccioProtocollo(RispostaAggiungiAllaccioProtocolloType value) {
        return new JAXBElement<RispostaAggiungiAllaccioProtocolloType>(_RispostaAggiungiAllaccioProtocollo_QNAME, RispostaAggiungiAllaccioProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRimuoviDocumentoProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_rimuoviDocumentoProtocollo")
    public JAXBElement<RispostaRimuoviDocumentoProtocolloType> createRispostaRimuoviDocumentoProtocollo(RispostaRimuoviDocumentoProtocolloType value) {
        return new JAXBElement<RispostaRimuoviDocumentoProtocolloType>(_RispostaRimuoviDocumentoProtocollo_QNAME, RispostaRimuoviDocumentoProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaGetProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_getProtocollo")
    public JAXBElement<RispostaGetProtocolloType> createRispostaGetProtocollo(RispostaGetProtocolloType value) {
        return new JAXBElement<RispostaGetProtocolloType>(_RispostaGetProtocollo_QNAME, RispostaGetProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaConvertiDocumentoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_convertiDocumento")
    public JAXBElement<RichiestaConvertiDocumentoType> createRichiestaConvertiDocumento(RichiestaConvertiDocumentoType value) {
        return new JAXBElement<RichiestaConvertiDocumentoType>(_RichiestaConvertiDocumento_QNAME, RichiestaConvertiDocumentoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAggiungiAnnotazioneProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_aggiungiAnnotazioneProtocollo")
    public JAXBElement<RichiestaAggiungiAnnotazioneProtocolloType> createRichiestaAggiungiAnnotazioneProtocollo(RichiestaAggiungiAnnotazioneProtocolloType value) {
        return new JAXBElement<RichiestaAggiungiAnnotazioneProtocolloType>(_RichiestaAggiungiAnnotazioneProtocollo_QNAME, RichiestaAggiungiAnnotazioneProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaProtocollaCodaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_protocollaCoda")
    public JAXBElement<RichiestaProtocollaCodaType> createRichiestaProtocollaCoda(RichiestaProtocollaCodaType value) {
        return new JAXBElement<RichiestaProtocollaCodaType>(_RichiestaProtocollaCoda_QNAME, RichiestaProtocollaCodaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAggiungiAssegnazioneApplicazioneProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_aggiungiAssegnazioneApplicazioneProtocollo")
    public JAXBElement<RichiestaAggiungiAssegnazioneApplicazioneProtocolloType> createRichiestaAggiungiAssegnazioneApplicazioneProtocollo(RichiestaAggiungiAssegnazioneApplicazioneProtocolloType value) {
        return new JAXBElement<RichiestaAggiungiAssegnazioneApplicazioneProtocolloType>(_RichiestaAggiungiAssegnazioneApplicazioneProtocollo_QNAME, RichiestaAggiungiAssegnazioneApplicazioneProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRicercaProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_ricercaProtocollo")
    public JAXBElement<RispostaRicercaProtocolloType> createRispostaRicercaProtocollo(RispostaRicercaProtocolloType value) {
        return new JAXBElement<RispostaRicercaProtocolloType>(_RispostaRicercaProtocollo_QNAME, RispostaRicercaProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRimuoviDestinatarioProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_rimuoviDestinatarioProtocollo")
    public JAXBElement<RichiestaRimuoviDestinatarioProtocolloType> createRichiestaRimuoviDestinatarioProtocollo(RichiestaRimuoviDestinatarioProtocolloType value) {
        return new JAXBElement<RichiestaRimuoviDestinatarioProtocolloType>(_RichiestaRimuoviDestinatarioProtocollo_QNAME, RichiestaRimuoviDestinatarioProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaModificaSpedizioneDestinatarioProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_modificaSpedizioneDestinatarioProtocollo")
    public JAXBElement<RichiestaModificaSpedizioneDestinatarioProtocolloType> createRichiestaModificaSpedizioneDestinatarioProtocollo(RichiestaModificaSpedizioneDestinatarioProtocolloType value) {
        return new JAXBElement<RichiestaModificaSpedizioneDestinatarioProtocolloType>(_RichiestaModificaSpedizioneDestinatarioProtocollo_QNAME, RichiestaModificaSpedizioneDestinatarioProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRimuoviDestinatarioProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_rimuoviDestinatarioProtocollo")
    public JAXBElement<RispostaRimuoviDestinatarioProtocolloType> createRispostaRimuoviDestinatarioProtocollo(RispostaRimuoviDestinatarioProtocolloType value) {
        return new JAXBElement<RispostaRimuoviDestinatarioProtocolloType>(_RispostaRimuoviDestinatarioProtocollo_QNAME, RispostaRimuoviDestinatarioProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaCambiaStatoProtocolloUscitaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_cambiaStatoProtocolloUscita")
    public JAXBElement<RichiestaCambiaStatoProtocolloUscitaType> createRichiestaCambiaStatoProtocolloUscita(RichiestaCambiaStatoProtocolloUscitaType value) {
        return new JAXBElement<RichiestaCambiaStatoProtocolloUscitaType>(_RichiestaCambiaStatoProtocolloUscita_QNAME, RichiestaCambiaStatoProtocolloUscitaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaCambiaStatoAssegnazioneApplicazioneProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_cambiaStatoAssegnazioneApplicazioneProtocollo")
    public JAXBElement<RispostaCambiaStatoAssegnazioneApplicazioneProtocolloType> createRispostaCambiaStatoAssegnazioneApplicazioneProtocollo(RispostaCambiaStatoAssegnazioneApplicazioneProtocolloType value) {
        return new JAXBElement<RispostaCambiaStatoAssegnazioneApplicazioneProtocolloType>(_RispostaCambiaStatoAssegnazioneApplicazioneProtocollo_QNAME, RispostaCambiaStatoAssegnazioneApplicazioneProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaDownloadAtmosType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_downloadAtmos")
    public JAXBElement<RichiestaDownloadAtmosType> createRichiestaDownloadAtmos(RichiestaDownloadAtmosType value) {
        return new JAXBElement<RichiestaDownloadAtmosType>(_RichiestaDownloadAtmos_QNAME, RichiestaDownloadAtmosType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRimuoviDocumentoProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_rimuoviDocumentoProtocollo")
    public JAXBElement<RichiestaRimuoviDocumentoProtocolloType> createRichiestaRimuoviDocumentoProtocollo(RichiestaRimuoviDocumentoProtocolloType value) {
        return new JAXBElement<RichiestaRimuoviDocumentoProtocolloType>(_RichiestaRimuoviDocumentoProtocollo_QNAME, RichiestaRimuoviDocumentoProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaDownloadDocumentoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_downloadDocumento")
    public JAXBElement<RichiestaDownloadDocumentoType> createRichiestaDownloadDocumento(RichiestaDownloadDocumentoType value) {
        return new JAXBElement<RichiestaDownloadDocumentoType>(_RichiestaDownloadDocumento_QNAME, RichiestaDownloadDocumentoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaGetProtocolloEntrataType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_getProtocolloEntrata")
    public JAXBElement<RispostaGetProtocolloEntrataType> createRispostaGetProtocolloEntrata(RispostaGetProtocolloEntrataType value) {
        return new JAXBElement<RispostaGetProtocolloEntrataType>(_RispostaGetProtocolloEntrata_QNAME, RispostaGetProtocolloEntrataType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAggiungiAllaccioProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_aggiungiAllaccioProtocollo")
    public JAXBElement<RichiestaAggiungiAllaccioProtocolloType> createRichiestaAggiungiAllaccioProtocollo(RichiestaAggiungiAllaccioProtocolloType value) {
        return new JAXBElement<RichiestaAggiungiAllaccioProtocolloType>(_RichiestaAggiungiAllaccioProtocollo_QNAME, RichiestaAggiungiAllaccioProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaTimbroDocumentoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_timbroDocumento")
    public JAXBElement<RispostaTimbroDocumentoType> createRispostaTimbroDocumento(RispostaTimbroDocumentoType value) {
        return new JAXBElement<RispostaTimbroDocumentoType>(_RispostaTimbroDocumento_QNAME, RispostaTimbroDocumentoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRispondiAProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_rispondiAProtocollo")
    public JAXBElement<RichiestaRispondiAProtocolloType> createRichiestaRispondiAProtocollo(RichiestaRispondiAProtocolloType value) {
        return new JAXBElement<RichiestaRispondiAProtocolloType>(_RichiestaRispondiAProtocollo_QNAME, RichiestaRispondiAProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_getProtocollo")
    public JAXBElement<RichiestaGetProtocolloType> createRichiestaGetProtocollo(RichiestaGetProtocolloType value) {
        return new JAXBElement<RichiestaGetProtocolloType>(_RichiestaGetProtocollo_QNAME, RichiestaGetProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaTimbroDocumentoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_timbroDocumento")
    public JAXBElement<RichiestaTimbroDocumentoType> createRichiestaTimbroDocumento(RichiestaTimbroDocumentoType value) {
        return new JAXBElement<RichiestaTimbroDocumentoType>(_RichiestaTimbroDocumento_QNAME, RichiestaTimbroDocumentoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaUpdateDatiProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_updateDatiProtocollo")
    public JAXBElement<RichiestaUpdateDatiProtocolloType> createRichiestaUpdateDatiProtocollo(RichiestaUpdateDatiProtocolloType value) {
        return new JAXBElement<RichiestaUpdateDatiProtocolloType>(_RichiestaUpdateDatiProtocollo_QNAME, RichiestaUpdateDatiProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaCambiaStatoProtocolloUscitaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_cambiaStatoProtocolloUscita")
    public JAXBElement<RispostaCambiaStatoProtocolloUscitaType> createRispostaCambiaStatoProtocolloUscita(RispostaCambiaStatoProtocolloUscitaType value) {
        return new JAXBElement<RispostaCambiaStatoProtocolloUscitaType>(_RispostaCambiaStatoProtocolloUscita_QNAME, RispostaCambiaStatoProtocolloUscitaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaModificaSpedizioneDestinatarioProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_modificaSpedizioneDestinatarioProtocollo")
    public JAXBElement<RispostaModificaSpedizioneDestinatarioProtocolloType> createRispostaModificaSpedizioneDestinatarioProtocollo(RispostaModificaSpedizioneDestinatarioProtocolloType value) {
        return new JAXBElement<RispostaModificaSpedizioneDestinatarioProtocolloType>(_RispostaModificaSpedizioneDestinatarioProtocollo_QNAME, RispostaModificaSpedizioneDestinatarioProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaReportProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_reportProtocollo")
    public JAXBElement<RispostaReportProtocolloType> createRispostaReportProtocollo(RispostaReportProtocolloType value) {
        return new JAXBElement<RispostaReportProtocolloType>(_RispostaReportProtocollo_QNAME, RispostaReportProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaCreateProtocolloUscitaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_createProtocolloUscita")
    public JAXBElement<RispostaCreateProtocolloUscitaType> createRispostaCreateProtocolloUscita(RispostaCreateProtocolloUscitaType value) {
        return new JAXBElement<RispostaCreateProtocolloUscitaType>(_RispostaCreateProtocolloUscita_QNAME, RispostaCreateProtocolloUscitaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaUpdateDatiProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_updateDatiProtocollo")
    public JAXBElement<RispostaUpdateDatiProtocolloType> createRispostaUpdateDatiProtocollo(RispostaUpdateDatiProtocolloType value) {
        return new JAXBElement<RispostaUpdateDatiProtocolloType>(_RispostaUpdateDatiProtocollo_QNAME, RispostaUpdateDatiProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaDownloadAtmosType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_downloadAtmos")
    public JAXBElement<RispostaDownloadAtmosType> createRispostaDownloadAtmos(RispostaDownloadAtmosType value) {
        return new JAXBElement<RispostaDownloadAtmosType>(_RispostaDownloadAtmos_QNAME, RispostaDownloadAtmosType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaUploadDocumentoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_uploadDocumento")
    public JAXBElement<RispostaUploadDocumentoType> createRispostaUploadDocumento(RispostaUploadDocumentoType value) {
        return new JAXBElement<RispostaUploadDocumentoType>(_RispostaUploadDocumento_QNAME, RispostaUploadDocumentoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaSpedisciProtocolloUscitaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_spedisciProtocolloUscita")
    public JAXBElement<RispostaSpedisciProtocolloUscitaType> createRispostaSpedisciProtocolloUscita(RispostaSpedisciProtocolloUscitaType value) {
        return new JAXBElement<RispostaSpedisciProtocolloUscitaType>(_RispostaSpedisciProtocolloUscita_QNAME, RispostaSpedisciProtocolloUscitaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRimuoviAssegnazioneApplicazioneProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_rimuoviAssegnazioneApplicazioneProtocollo")
    public JAXBElement<RichiestaRimuoviAssegnazioneApplicazioneProtocolloType> createRichiestaRimuoviAssegnazioneApplicazioneProtocollo(RichiestaRimuoviAssegnazioneApplicazioneProtocolloType value) {
        return new JAXBElement<RichiestaRimuoviAssegnazioneApplicazioneProtocolloType>(_RichiestaRimuoviAssegnazioneApplicazioneProtocollo_QNAME, RichiestaRimuoviAssegnazioneApplicazioneProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaSpedisciProtocolloUscitaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_spedisciProtocolloUscita")
    public JAXBElement<RichiestaSpedisciProtocolloUscitaType> createRichiestaSpedisciProtocolloUscita(RichiestaSpedisciProtocolloUscitaType value) {
        return new JAXBElement<RichiestaSpedisciProtocolloUscitaType>(_RichiestaSpedisciProtocolloUscita_QNAME, RichiestaSpedisciProtocolloUscitaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaGetProtocolloUscitaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_getProtocolloUscita")
    public JAXBElement<RispostaGetProtocolloUscitaType> createRispostaGetProtocolloUscita(RispostaGetProtocolloUscitaType value) {
        return new JAXBElement<RispostaGetProtocolloUscitaType>(_RispostaGetProtocolloUscita_QNAME, RispostaGetProtocolloUscitaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BaseServiceResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_aggiungiAnnotazioneProtocollo")
    public JAXBElement<BaseServiceResponseType> createRispostaAggiungiAnnotazioneProtocollo(BaseServiceResponseType value) {
        return new JAXBElement<BaseServiceResponseType>(_RispostaAggiungiAnnotazioneProtocollo_QNAME, BaseServiceResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetProtocolloEntrataType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_getProtocolloEntrata")
    public JAXBElement<RichiestaGetProtocolloEntrataType> createRichiestaGetProtocolloEntrata(RichiestaGetProtocolloEntrataType value) {
        return new JAXBElement<RichiestaGetProtocolloEntrataType>(_RichiestaGetProtocolloEntrata_QNAME, RichiestaGetProtocolloEntrataType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAggiungiDestinatarioProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_aggiungiDestinatarioProtocollo")
    public JAXBElement<RichiestaAggiungiDestinatarioProtocolloType> createRichiestaAggiungiDestinatarioProtocollo(RichiestaAggiungiDestinatarioProtocolloType value) {
        return new JAXBElement<RichiestaAggiungiDestinatarioProtocolloType>(_RichiestaAggiungiDestinatarioProtocollo_QNAME, RichiestaAggiungiDestinatarioProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRimuoviAnnotazioneProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_rimuoviAnnotazioneProtocollo")
    public JAXBElement<RispostaRimuoviAnnotazioneProtocolloType> createRispostaRimuoviAnnotazioneProtocollo(RispostaRimuoviAnnotazioneProtocolloType value) {
        return new JAXBElement<RispostaRimuoviAnnotazioneProtocolloType>(_RispostaRimuoviAnnotazioneProtocollo_QNAME, RispostaRimuoviAnnotazioneProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAnnullamentoProtocolloType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_annullamentoProtocollo")
    public JAXBElement<RichiestaAnnullamentoProtocolloType> createRichiestaAnnullamentoProtocollo(RichiestaAnnullamentoProtocolloType value) {
        return new JAXBElement<RichiestaAnnullamentoProtocolloType>(_RichiestaAnnullamentoProtocollo_QNAME, RichiestaAnnullamentoProtocolloType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaReportRegistroGiornalieroType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_reportRegistroGiornaliero")
    public JAXBElement<RispostaReportRegistroGiornalieroType> createRispostaReportRegistroGiornaliero(RispostaReportRegistroGiornalieroType value) {
        return new JAXBElement<RispostaReportRegistroGiornalieroType>(_RispostaReportRegistroGiornaliero_QNAME, RispostaReportRegistroGiornalieroType.class, null, value);
    }
    
    /**
     * Create an instance of {@link RichiestaAnnullaRegistrazioneAusiliaria }
     * 
     */
    public RichiestaAnnullaRegistrazioneAusiliaria createRichiestaAnnullaRegistrazioneAusiliaria() {
        return new RichiestaAnnullaRegistrazioneAusiliaria();
    }

    /**
     * Create an instance of {@link RichiestaAnnullaRegistrazioneAusiliariaType }
     * 
     */
    public RichiestaAnnullaRegistrazioneAusiliariaType createRichiestaAnnullaRegistrazioneAusiliariaType() {
        return new RichiestaAnnullaRegistrazioneAusiliariaType();
    }

    /**
     * Create an instance of {@link RichiestaCreateRegistroAusiliarioType }
     * 
     */
    public RichiestaCreateRegistroAusiliarioType createRichiestaCreateRegistroAusiliarioType() {
        return new RichiestaCreateRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link RichiestaApriRegistroAusiliarioType }
     * 
     */
    public RichiestaApriRegistroAusiliarioType createRichiestaApriRegistroAusiliarioType() {
        return new RichiestaApriRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link RispostaRicercaRegistroAusiliarioType }
     * 
     */
    public RispostaRicercaRegistroAusiliarioType createRispostaRicercaRegistroAusiliarioType() {
        return new RispostaRicercaRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link RichiestaUpdateRegistrazioneAusiliariaType }
     * 
     */
    public RichiestaUpdateRegistrazioneAusiliariaType createRichiestaUpdateRegistrazioneAusiliariaType() {
        return new RichiestaUpdateRegistrazioneAusiliariaType();
    }

    /**
     * Create an instance of {@link RispostaCreateRegistrazioneAusiliariaType }
     * 
     */
    public RispostaCreateRegistrazioneAusiliariaType createRispostaCreateRegistrazioneAusiliariaType() {
        return new RispostaCreateRegistrazioneAusiliariaType();
    }

    /**
     * Create an instance of {@link RispostaCollegaRegistrazioneAusiliariaProtocolloUscitaType }
     * 
     */
    public RispostaCollegaRegistrazioneAusiliariaProtocolloUscitaType createRispostaCollegaRegistrazioneAusiliariaProtocolloUscitaType() {
        return new RispostaCollegaRegistrazioneAusiliariaProtocolloUscitaType();
    }

    /**
     * Create an instance of {@link RispostaRimuoviTipologiaDocumentaleRegistroAusiliarioType }
     * 
     */
    public RispostaRimuoviTipologiaDocumentaleRegistroAusiliarioType createRispostaRimuoviTipologiaDocumentaleRegistroAusiliarioType() {
        return new RispostaRimuoviTipologiaDocumentaleRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link RispostaSbloccaRegistroAusiliarioType }
     * 
     */
    public RispostaSbloccaRegistroAusiliarioType createRispostaSbloccaRegistroAusiliarioType() {
        return new RispostaSbloccaRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link RispostaCreateRegistroAusiliarioType }
     * 
     */
    public RispostaCreateRegistroAusiliarioType createRispostaCreateRegistroAusiliarioType() {
        return new RispostaCreateRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link RispostaUpdateRegistroAusiliarioType }
     * 
     */
    public RispostaUpdateRegistroAusiliarioType createRispostaUpdateRegistroAusiliarioType() {
        return new RispostaUpdateRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link RichiestaRimuoviApplicazioneRegistroAusiliarioType }
     * 
     */
    public RichiestaRimuoviApplicazioneRegistroAusiliarioType createRichiestaRimuoviApplicazioneRegistroAusiliarioType() {
        return new RichiestaRimuoviApplicazioneRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link RispostaChiudiRegistroAusiliarioType }
     * 
     */
    public RispostaChiudiRegistroAusiliarioType createRispostaChiudiRegistroAusiliarioType() {
        return new RispostaChiudiRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link RichiestaBloccaRegistroAusiliarioType }
     * 
     */
    public RichiestaBloccaRegistroAusiliarioType createRichiestaBloccaRegistroAusiliarioType() {
        return new RichiestaBloccaRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link RispostaRimuoviApplicazioneRegistroAusiliarioType }
     * 
     */
    public RispostaRimuoviApplicazioneRegistroAusiliarioType createRispostaRimuoviApplicazioneRegistroAusiliarioType() {
        return new RispostaRimuoviApplicazioneRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link RispostaRicercaRegistrazioneAusiliariaType }
     * 
     */
    public RispostaRicercaRegistrazioneAusiliariaType createRispostaRicercaRegistrazioneAusiliariaType() {
        return new RispostaRicercaRegistrazioneAusiliariaType();
    }

    /**
     * Create an instance of {@link RispostaBloccaRegistroAusiliarioType }
     * 
     */
    public RispostaBloccaRegistroAusiliarioType createRispostaBloccaRegistroAusiliarioType() {
        return new RispostaBloccaRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link RichiestaRimuoviDocumentoRegistrazioneAusiliariaType }
     * 
     */
    public RichiestaRimuoviDocumentoRegistrazioneAusiliariaType createRichiestaRimuoviDocumentoRegistrazioneAusiliariaType() {
        return new RichiestaRimuoviDocumentoRegistrazioneAusiliariaType();
    }

    /**
     * Create an instance of {@link RispostaRimuoviDocumentoRegistrazioneAusiliariaType }
     * 
     */
    public RispostaRimuoviDocumentoRegistrazioneAusiliariaType createRispostaRimuoviDocumentoRegistrazioneAusiliariaType() {
        return new RispostaRimuoviDocumentoRegistrazioneAusiliariaType();
    }

    /**
     * Create an instance of {@link RichiestaAggiungiDocumentoRegistrazioneAusiliariaType }
     * 
     */
    public RichiestaAggiungiDocumentoRegistrazioneAusiliariaType createRichiestaAggiungiDocumentoRegistrazioneAusiliariaType() {
        return new RichiestaAggiungiDocumentoRegistrazioneAusiliariaType();
    }

    /**
     * Create an instance of {@link RichiestaRicercaRegistroAusiliarioType }
     * 
     */
    public RichiestaRicercaRegistroAusiliarioType createRichiestaRicercaRegistroAusiliarioType() {
        return new RichiestaRicercaRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link RichiestaGetRegistrazioneAusiliariaType }
     * 
     */
    public RichiestaGetRegistrazioneAusiliariaType createRichiestaGetRegistrazioneAusiliariaType() {
        return new RichiestaGetRegistrazioneAusiliariaType();
    }

    /**
     * Create an instance of {@link RispostaAssegnaApplicazioneRegistroAusiliarioType }
     * 
     */
    public RispostaAssegnaApplicazioneRegistroAusiliarioType createRispostaAssegnaApplicazioneRegistroAusiliarioType() {
        return new RispostaAssegnaApplicazioneRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link RichiestaCollegaRegistrazioneAusiliariaProtocolloIngressoType }
     * 
     */
    public RichiestaCollegaRegistrazioneAusiliariaProtocolloIngressoType createRichiestaCollegaRegistrazioneAusiliariaProtocolloIngressoType() {
        return new RichiestaCollegaRegistrazioneAusiliariaProtocolloIngressoType();
    }

    /**
     * Create an instance of {@link RichiestaUpdateRegistroAusiliarioType }
     * 
     */
    public RichiestaUpdateRegistroAusiliarioType createRichiestaUpdateRegistroAusiliarioType() {
        return new RichiestaUpdateRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link RispostaApriRegistroAusiliarioType }
     * 
     */
    public RispostaApriRegistroAusiliarioType createRispostaApriRegistroAusiliarioType() {
        return new RispostaApriRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link RispostaGetRegistrazioneAusiliariaType }
     * 
     */
    public RispostaGetRegistrazioneAusiliariaType createRispostaGetRegistrazioneAusiliariaType() {
        return new RispostaGetRegistrazioneAusiliariaType();
    }

    /**
     * Create an instance of {@link RispostaAggiungiDocumentoRegistrazioneAusiliariaType }
     * 
     */
    public RispostaAggiungiDocumentoRegistrazioneAusiliariaType createRispostaAggiungiDocumentoRegistrazioneAusiliariaType() {
        return new RispostaAggiungiDocumentoRegistrazioneAusiliariaType();
    }

    /**
     * Create an instance of {@link RichiestaRicercaRegistrazioneAusiliariaType }
     * 
     */
    public RichiestaRicercaRegistrazioneAusiliariaType createRichiestaRicercaRegistrazioneAusiliariaType() {
        return new RichiestaRicercaRegistrazioneAusiliariaType();
    }

    /**
     * Create an instance of {@link RispostaUpdateRegistrazioneAusiliariaType }
     * 
     */
    public RispostaUpdateRegistrazioneAusiliariaType createRispostaUpdateRegistrazioneAusiliariaType() {
        return new RispostaUpdateRegistrazioneAusiliariaType();
    }

    /**
     * Create an instance of {@link RichiestaRimuoviTipologiaDocumentaleRegistroAusiliarioType }
     * 
     */
    public RichiestaRimuoviTipologiaDocumentaleRegistroAusiliarioType createRichiestaRimuoviTipologiaDocumentaleRegistroAusiliarioType() {
        return new RichiestaRimuoviTipologiaDocumentaleRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link RispostaAssociaTipologiaDocumentaleRegistroAusiliarioType }
     * 
     */
    public RispostaAssociaTipologiaDocumentaleRegistroAusiliarioType createRispostaAssociaTipologiaDocumentaleRegistroAusiliarioType() {
        return new RispostaAssociaTipologiaDocumentaleRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link RispostaCollegaRegistrazioneAusiliariaProtocolloIngressoType }
     * 
     */
    public RispostaCollegaRegistrazioneAusiliariaProtocolloIngressoType createRispostaCollegaRegistrazioneAusiliariaProtocolloIngressoType() {
        return new RispostaCollegaRegistrazioneAusiliariaProtocolloIngressoType();
    }

    /**
     * Create an instance of {@link RichiestaChiudiRegistroAusiliarioType }
     * 
     */
    public RichiestaChiudiRegistroAusiliarioType createRichiestaChiudiRegistroAusiliarioType() {
        return new RichiestaChiudiRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link RichiestaCreateRegistrazioneAusiliariaType }
     * 
     */
    public RichiestaCreateRegistrazioneAusiliariaType createRichiestaCreateRegistrazioneAusiliariaType() {
        return new RichiestaCreateRegistrazioneAusiliariaType();
    }

    /**
     * Create an instance of {@link RichiestaSbloccaRegistroAusiliarioType }
     * 
     */
    public RichiestaSbloccaRegistroAusiliarioType createRichiestaSbloccaRegistroAusiliarioType() {
        return new RichiestaSbloccaRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link RispostaAnnullaRegistrazioneAusiliariaType }
     * 
     */
    public RispostaAnnullaRegistrazioneAusiliariaType createRispostaAnnullaRegistrazioneAusiliariaType() {
        return new RispostaAnnullaRegistrazioneAusiliariaType();
    }

    /**
     * Create an instance of {@link RichiestaAssociaTipologiaDocumentaleRegistroAusiliarioType }
     * 
     */
    public RichiestaAssociaTipologiaDocumentaleRegistroAusiliarioType createRichiestaAssociaTipologiaDocumentaleRegistroAusiliarioType() {
        return new RichiestaAssociaTipologiaDocumentaleRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link RichiestaAssegnaApplicazioneRegistroAusiliarioType }
     * 
     */
    public RichiestaAssegnaApplicazioneRegistroAusiliarioType createRichiestaAssegnaApplicazioneRegistroAusiliarioType() {
        return new RichiestaAssegnaApplicazioneRegistroAusiliarioType();
    }

    /**
     * Create an instance of {@link RichiestaCollegaRegistrazioneAusiliariaProtocolloUscitaType }
     * 
     */
    public RichiestaCollegaRegistrazioneAusiliariaProtocolloUscitaType createRichiestaCollegaRegistrazioneAusiliariaProtocolloUscitaType() {
        return new RichiestaCollegaRegistrazioneAusiliariaProtocolloUscitaType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaCreateRegistroAusiliarioType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_createRegistroAusiliario")
    public JAXBElement<RichiestaCreateRegistroAusiliarioType> createRichiestaCreateRegistroAusiliario(RichiestaCreateRegistroAusiliarioType value) {
        return new JAXBElement<RichiestaCreateRegistroAusiliarioType>(_RichiestaCreateRegistroAusiliario_QNAME, RichiestaCreateRegistroAusiliarioType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaApriRegistroAusiliarioType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_apriRegistroAusiliario")
    public JAXBElement<RichiestaApriRegistroAusiliarioType> createRichiestaApriRegistroAusiliario(RichiestaApriRegistroAusiliarioType value) {
        return new JAXBElement<RichiestaApriRegistroAusiliarioType>(_RichiestaApriRegistroAusiliario_QNAME, RichiestaApriRegistroAusiliarioType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRicercaRegistroAusiliarioType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_ricercaRegistroAusiliario")
    public JAXBElement<RispostaRicercaRegistroAusiliarioType> createRispostaRicercaRegistroAusiliario(RispostaRicercaRegistroAusiliarioType value) {
        return new JAXBElement<RispostaRicercaRegistroAusiliarioType>(_RispostaRicercaRegistroAusiliario_QNAME, RispostaRicercaRegistroAusiliarioType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRimuoviApplicazioneRegistroAusiliarioType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_rimuoviApplicazioneRegistroAusiliario")
    public JAXBElement<RichiestaRimuoviApplicazioneRegistroAusiliarioType> createRichiestaRimuoviApplicazioneRegistroAusiliario(RichiestaRimuoviApplicazioneRegistroAusiliarioType value) {
        return new JAXBElement<RichiestaRimuoviApplicazioneRegistroAusiliarioType>(_RichiestaRimuoviApplicazioneRegistroAusiliario_QNAME, RichiestaRimuoviApplicazioneRegistroAusiliarioType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAggiungiDocumentoRegistrazioneAusiliariaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_aggiungiDocumentoRegistrazioneAusiliaria")
    public JAXBElement<RichiestaAggiungiDocumentoRegistrazioneAusiliariaType> createRichiestaAggiungiDocumentoRegistrazioneAusiliaria(RichiestaAggiungiDocumentoRegistrazioneAusiliariaType value) {
        return new JAXBElement<RichiestaAggiungiDocumentoRegistrazioneAusiliariaType>(_RichiestaAggiungiDocumentoRegistrazioneAusiliaria_QNAME, RichiestaAggiungiDocumentoRegistrazioneAusiliariaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRimuoviTipologiaDocumentaleRegistroAusiliarioType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_rimuoviTipologiaDocumentaleRegistroAusiliario")
    public JAXBElement<RichiestaRimuoviTipologiaDocumentaleRegistroAusiliarioType> createRichiestaRimuoviTipologiaDocumentaleRegistroAusiliario(RichiestaRimuoviTipologiaDocumentaleRegistroAusiliarioType value) {
        return new JAXBElement<RichiestaRimuoviTipologiaDocumentaleRegistroAusiliarioType>(_RichiestaRimuoviTipologiaDocumentaleRegistroAusiliario_QNAME, RichiestaRimuoviTipologiaDocumentaleRegistroAusiliarioType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaCollegaRegistrazioneAusiliariaProtocolloIngressoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_collegaRegistrazioneAusiliariaProtocolloIngresso")
    public JAXBElement<RispostaCollegaRegistrazioneAusiliariaProtocolloIngressoType> createRispostaCollegaRegistrazioneAusiliariaProtocolloIngresso(RispostaCollegaRegistrazioneAusiliariaProtocolloIngressoType value) {
        return new JAXBElement<RispostaCollegaRegistrazioneAusiliariaProtocolloIngressoType>(_RispostaCollegaRegistrazioneAusiliariaProtocolloIngresso_QNAME, RispostaCollegaRegistrazioneAusiliariaProtocolloIngressoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAssegnaApplicazioneRegistroAusiliarioType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_assegnaApplicazioneRegistroAusiliario")
    public JAXBElement<RichiestaAssegnaApplicazioneRegistroAusiliarioType> createRichiestaAssegnaApplicazioneRegistroAusiliario(RichiestaAssegnaApplicazioneRegistroAusiliarioType value) {
        return new JAXBElement<RichiestaAssegnaApplicazioneRegistroAusiliarioType>(_RichiestaAssegnaApplicazioneRegistroAusiliario_QNAME, RichiestaAssegnaApplicazioneRegistroAusiliarioType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaSbloccaRegistroAusiliarioType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_sbloccaRegistroAusiliario")
    public JAXBElement<RispostaSbloccaRegistroAusiliarioType> createRispostaSbloccaRegistroAusiliario(RispostaSbloccaRegistroAusiliarioType value) {
        return new JAXBElement<RispostaSbloccaRegistroAusiliarioType>(_RispostaSbloccaRegistroAusiliario_QNAME, RispostaSbloccaRegistroAusiliarioType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaBloccaRegistroAusiliarioType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_bloccaRegistroAusiliario")
    public JAXBElement<RichiestaBloccaRegistroAusiliarioType> createRichiestaBloccaRegistroAusiliario(RichiestaBloccaRegistroAusiliarioType value) {
        return new JAXBElement<RichiestaBloccaRegistroAusiliarioType>(_RichiestaBloccaRegistroAusiliario_QNAME, RichiestaBloccaRegistroAusiliarioType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaBloccaRegistroAusiliarioType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_bloccaRegistroAusiliario")
    public JAXBElement<RispostaBloccaRegistroAusiliarioType> createRispostaBloccaRegistroAusiliario(RispostaBloccaRegistroAusiliarioType value) {
        return new JAXBElement<RispostaBloccaRegistroAusiliarioType>(_RispostaBloccaRegistroAusiliario_QNAME, RispostaBloccaRegistroAusiliarioType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRimuoviDocumentoRegistrazioneAusiliariaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_rimuoviDocumentoRegistrazioneAusiliaria")
    public JAXBElement<RichiestaRimuoviDocumentoRegistrazioneAusiliariaType> createRichiestaRimuoviDocumentoRegistrazioneAusiliaria(RichiestaRimuoviDocumentoRegistrazioneAusiliariaType value) {
        return new JAXBElement<RichiestaRimuoviDocumentoRegistrazioneAusiliariaType>(_RichiestaRimuoviDocumentoRegistrazioneAusiliaria_QNAME, RichiestaRimuoviDocumentoRegistrazioneAusiliariaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaAssegnaApplicazioneRegistroAusiliarioType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_assegnaApplicazioneRegistroAusiliario")
    public JAXBElement<RispostaAssegnaApplicazioneRegistroAusiliarioType> createRispostaAssegnaApplicazioneRegistroAusiliario(RispostaAssegnaApplicazioneRegistroAusiliarioType value) {
        return new JAXBElement<RispostaAssegnaApplicazioneRegistroAusiliarioType>(_RispostaAssegnaApplicazioneRegistroAusiliario_QNAME, RispostaAssegnaApplicazioneRegistroAusiliarioType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRicercaRegistrazioneAusiliariaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_ricercaRegistrazioneAusiliaria")
    public JAXBElement<RichiestaRicercaRegistrazioneAusiliariaType> createRichiestaRicercaRegistrazioneAusiliaria(RichiestaRicercaRegistrazioneAusiliariaType value) {
        return new JAXBElement<RichiestaRicercaRegistrazioneAusiliariaType>(_RichiestaRicercaRegistrazioneAusiliaria_QNAME, RichiestaRicercaRegistrazioneAusiliariaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaCreateRegistrazioneAusiliariaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_createRegistrazioneAusiliaria")
    public JAXBElement<RichiestaCreateRegistrazioneAusiliariaType> createRichiestaCreateRegistrazioneAusiliaria(RichiestaCreateRegistrazioneAusiliariaType value) {
        return new JAXBElement<RichiestaCreateRegistrazioneAusiliariaType>(_RichiestaCreateRegistrazioneAusiliaria_QNAME, RichiestaCreateRegistrazioneAusiliariaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaCollegaRegistrazioneAusiliariaProtocolloUscitaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_collegaRegistrazioneAusiliariaProtocolloUscita")
    public JAXBElement<RichiestaCollegaRegistrazioneAusiliariaProtocolloUscitaType> createRichiestaCollegaRegistrazioneAusiliariaProtocolloUscita(RichiestaCollegaRegistrazioneAusiliariaProtocolloUscitaType value) {
        return new JAXBElement<RichiestaCollegaRegistrazioneAusiliariaProtocolloUscitaType>(_RichiestaCollegaRegistrazioneAusiliariaProtocolloUscita_QNAME, RichiestaCollegaRegistrazioneAusiliariaProtocolloUscitaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaUpdateRegistrazioneAusiliariaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_updateRegistrazioneAusiliaria")
    public JAXBElement<RichiestaUpdateRegistrazioneAusiliariaType> createRichiestaUpdateRegistrazioneAusiliaria(RichiestaUpdateRegistrazioneAusiliariaType value) {
        return new JAXBElement<RichiestaUpdateRegistrazioneAusiliariaType>(_RichiestaUpdateRegistrazioneAusiliaria_QNAME, RichiestaUpdateRegistrazioneAusiliariaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaCreateRegistrazioneAusiliariaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_createRegistrazioneAusiliaria")
    public JAXBElement<RispostaCreateRegistrazioneAusiliariaType> createRispostaCreateRegistrazioneAusiliaria(RispostaCreateRegistrazioneAusiliariaType value) {
        return new JAXBElement<RispostaCreateRegistrazioneAusiliariaType>(_RispostaCreateRegistrazioneAusiliaria_QNAME, RispostaCreateRegistrazioneAusiliariaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaCollegaRegistrazioneAusiliariaProtocolloUscitaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_collegaRegistrazioneAusiliariaProtocolloUscita")
    public JAXBElement<RispostaCollegaRegistrazioneAusiliariaProtocolloUscitaType> createRispostaCollegaRegistrazioneAusiliariaProtocolloUscita(RispostaCollegaRegistrazioneAusiliariaProtocolloUscitaType value) {
        return new JAXBElement<RispostaCollegaRegistrazioneAusiliariaProtocolloUscitaType>(_RispostaCollegaRegistrazioneAusiliariaProtocolloUscita_QNAME, RispostaCollegaRegistrazioneAusiliariaProtocolloUscitaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRimuoviTipologiaDocumentaleRegistroAusiliarioType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_rimuoviTipologiaDocumentaleRegistroAusiliario")
    public JAXBElement<RispostaRimuoviTipologiaDocumentaleRegistroAusiliarioType> createRispostaRimuoviTipologiaDocumentaleRegistroAusiliario(RispostaRimuoviTipologiaDocumentaleRegistroAusiliarioType value) {
        return new JAXBElement<RispostaRimuoviTipologiaDocumentaleRegistroAusiliarioType>(_RispostaRimuoviTipologiaDocumentaleRegistroAusiliario_QNAME, RispostaRimuoviTipologiaDocumentaleRegistroAusiliarioType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaUpdateRegistroAusiliarioType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_updateRegistroAusiliario")
    public JAXBElement<RispostaUpdateRegistroAusiliarioType> createRispostaUpdateRegistroAusiliario(RispostaUpdateRegistroAusiliarioType value) {
        return new JAXBElement<RispostaUpdateRegistroAusiliarioType>(_RispostaUpdateRegistroAusiliario_QNAME, RispostaUpdateRegistroAusiliarioType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaApriRegistroAusiliarioType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_apriRegistroAusiliario")
    public JAXBElement<RispostaApriRegistroAusiliarioType> createRispostaApriRegistroAusiliario(RispostaApriRegistroAusiliarioType value) {
        return new JAXBElement<RispostaApriRegistroAusiliarioType>(_RispostaApriRegistroAusiliario_QNAME, RispostaApriRegistroAusiliarioType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaGetRegistrazioneAusiliariaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_getRegistrazioneAusiliaria")
    public JAXBElement<RispostaGetRegistrazioneAusiliariaType> createRispostaGetRegistrazioneAusiliaria(RispostaGetRegistrazioneAusiliariaType value) {
        return new JAXBElement<RispostaGetRegistrazioneAusiliariaType>(_RispostaGetRegistrazioneAusiliaria_QNAME, RispostaGetRegistrazioneAusiliariaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaAggiungiDocumentoRegistrazioneAusiliariaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_aggiungiDocumentoRegistrazioneAusiliaria")
    public JAXBElement<RispostaAggiungiDocumentoRegistrazioneAusiliariaType> createRispostaAggiungiDocumentoRegistrazioneAusiliaria(RispostaAggiungiDocumentoRegistrazioneAusiliariaType value) {
        return new JAXBElement<RispostaAggiungiDocumentoRegistrazioneAusiliariaType>(_RispostaAggiungiDocumentoRegistrazioneAusiliaria_QNAME, RispostaAggiungiDocumentoRegistrazioneAusiliariaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaUpdateRegistroAusiliarioType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_updateRegistroAusiliario")
    public JAXBElement<RichiestaUpdateRegistroAusiliarioType> createRichiestaUpdateRegistroAusiliario(RichiestaUpdateRegistroAusiliarioType value) {
        return new JAXBElement<RichiestaUpdateRegistroAusiliarioType>(_RichiestaUpdateRegistroAusiliario_QNAME, RichiestaUpdateRegistroAusiliarioType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaUpdateRegistrazioneAusiliariaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_updateRegistrazioneAusiliaria")
    public JAXBElement<RispostaUpdateRegistrazioneAusiliariaType> createRispostaUpdateRegistrazioneAusiliaria(RispostaUpdateRegistrazioneAusiliariaType value) {
        return new JAXBElement<RispostaUpdateRegistrazioneAusiliariaType>(_RispostaUpdateRegistrazioneAusiliaria_QNAME, RispostaUpdateRegistrazioneAusiliariaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaChiudiRegistroAusiliarioType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_chiudiRegistroAusiliario")
    public JAXBElement<RichiestaChiudiRegistroAusiliarioType> createRichiestaChiudiRegistroAusiliario(RichiestaChiudiRegistroAusiliarioType value) {
        return new JAXBElement<RichiestaChiudiRegistroAusiliarioType>(_RichiestaChiudiRegistroAusiliario_QNAME, RichiestaChiudiRegistroAusiliarioType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaAssociaTipologiaDocumentaleRegistroAusiliarioType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_associaTipologiaDocumentaleRegistroAusiliario")
    public JAXBElement<RispostaAssociaTipologiaDocumentaleRegistroAusiliarioType> createRispostaAssociaTipologiaDocumentaleRegistroAusiliario(RispostaAssociaTipologiaDocumentaleRegistroAusiliarioType value) {
        return new JAXBElement<RispostaAssociaTipologiaDocumentaleRegistroAusiliarioType>(_RispostaAssociaTipologiaDocumentaleRegistroAusiliario_QNAME, RispostaAssociaTipologiaDocumentaleRegistroAusiliarioType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaCreateRegistroAusiliarioType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_createRegistroAusiliario")
    public JAXBElement<RispostaCreateRegistroAusiliarioType> createRispostaCreateRegistroAusiliario(RispostaCreateRegistroAusiliarioType value) {
        return new JAXBElement<RispostaCreateRegistroAusiliarioType>(_RispostaCreateRegistroAusiliario_QNAME, RispostaCreateRegistroAusiliarioType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaChiudiRegistroAusiliarioType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_chiudiRegistroAusiliario")
    public JAXBElement<RispostaChiudiRegistroAusiliarioType> createRispostaChiudiRegistroAusiliario(RispostaChiudiRegistroAusiliarioType value) {
        return new JAXBElement<RispostaChiudiRegistroAusiliarioType>(_RispostaChiudiRegistroAusiliario_QNAME, RispostaChiudiRegistroAusiliarioType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRimuoviApplicazioneRegistroAusiliarioType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_rimuoviApplicazioneRegistroAusiliario")
    public JAXBElement<RispostaRimuoviApplicazioneRegistroAusiliarioType> createRispostaRimuoviApplicazioneRegistroAusiliario(RispostaRimuoviApplicazioneRegistroAusiliarioType value) {
        return new JAXBElement<RispostaRimuoviApplicazioneRegistroAusiliarioType>(_RispostaRimuoviApplicazioneRegistroAusiliario_QNAME, RispostaRimuoviApplicazioneRegistroAusiliarioType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRicercaRegistrazioneAusiliariaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_ricercaRegistrazioneAusiliaria")
    public JAXBElement<RispostaRicercaRegistrazioneAusiliariaType> createRispostaRicercaRegistrazioneAusiliaria(RispostaRicercaRegistrazioneAusiliariaType value) {
        return new JAXBElement<RispostaRicercaRegistrazioneAusiliariaType>(_RispostaRicercaRegistrazioneAusiliaria_QNAME, RispostaRicercaRegistrazioneAusiliariaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRimuoviDocumentoRegistrazioneAusiliariaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_rimuoviDocumentoRegistrazioneAusiliaria")
    public JAXBElement<RispostaRimuoviDocumentoRegistrazioneAusiliariaType> createRispostaRimuoviDocumentoRegistrazioneAusiliaria(RispostaRimuoviDocumentoRegistrazioneAusiliariaType value) {
        return new JAXBElement<RispostaRimuoviDocumentoRegistrazioneAusiliariaType>(_RispostaRimuoviDocumentoRegistrazioneAusiliaria_QNAME, RispostaRimuoviDocumentoRegistrazioneAusiliariaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaCollegaRegistrazioneAusiliariaProtocolloIngressoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_collegaRegistrazioneAusiliariaProtocolloIngresso")
    public JAXBElement<RichiestaCollegaRegistrazioneAusiliariaProtocolloIngressoType> createRichiestaCollegaRegistrazioneAusiliariaProtocolloIngresso(RichiestaCollegaRegistrazioneAusiliariaProtocolloIngressoType value) {
        return new JAXBElement<RichiestaCollegaRegistrazioneAusiliariaProtocolloIngressoType>(_RichiestaCollegaRegistrazioneAusiliariaProtocolloIngresso_QNAME, RichiestaCollegaRegistrazioneAusiliariaProtocolloIngressoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRicercaRegistroAusiliarioType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_ricercaRegistroAusiliario")
    public JAXBElement<RichiestaRicercaRegistroAusiliarioType> createRichiestaRicercaRegistroAusiliario(RichiestaRicercaRegistroAusiliarioType value) {
        return new JAXBElement<RichiestaRicercaRegistroAusiliarioType>(_RichiestaRicercaRegistroAusiliario_QNAME, RichiestaRicercaRegistroAusiliarioType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetRegistrazioneAusiliariaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_getRegistrazioneAusiliaria")
    public JAXBElement<RichiestaGetRegistrazioneAusiliariaType> createRichiestaGetRegistrazioneAusiliaria(RichiestaGetRegistrazioneAusiliariaType value) {
        return new JAXBElement<RichiestaGetRegistrazioneAusiliariaType>(_RichiestaGetRegistrazioneAusiliaria_QNAME, RichiestaGetRegistrazioneAusiliariaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaAnnullaRegistrazioneAusiliariaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_annullaRegistrazioneAusiliaria")
    public JAXBElement<RispostaAnnullaRegistrazioneAusiliariaType> createRispostaAnnullaRegistrazioneAusiliaria(RispostaAnnullaRegistrazioneAusiliariaType value) {
        return new JAXBElement<RispostaAnnullaRegistrazioneAusiliariaType>(_RispostaAnnullaRegistrazioneAusiliaria_QNAME, RispostaAnnullaRegistrazioneAusiliariaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaSbloccaRegistroAusiliarioType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_sbloccaRegistroAusiliario")
    public JAXBElement<RichiestaSbloccaRegistroAusiliarioType> createRichiestaSbloccaRegistroAusiliario(RichiestaSbloccaRegistroAusiliarioType value) {
        return new JAXBElement<RichiestaSbloccaRegistroAusiliarioType>(_RichiestaSbloccaRegistroAusiliario_QNAME, RichiestaSbloccaRegistroAusiliarioType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaAssociaTipologiaDocumentaleRegistroAusiliarioType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_associaTipologiaDocumentaleRegistroAusiliario")
    public JAXBElement<RichiestaAssociaTipologiaDocumentaleRegistroAusiliarioType> createRichiestaAssociaTipologiaDocumentaleRegistroAusiliario(RichiestaAssociaTipologiaDocumentaleRegistroAusiliarioType value) {
        return new JAXBElement<RichiestaAssociaTipologiaDocumentaleRegistroAusiliarioType>(_RichiestaAssociaTipologiaDocumentaleRegistroAusiliario_QNAME, RichiestaAssociaTipologiaDocumentaleRegistroAusiliarioType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DataRegistrazione", scope = RichiestaCreateRegistrazioneAusiliariaType.class)
    public JAXBElement<XMLGregorianCalendar> createRichiestaCreateRegistrazioneAusiliariaTypeDataRegistrazione(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_RichiestaCreateRegistrazioneAusiliariaTypeDataRegistrazione_QNAME, XMLGregorianCalendar.class, RichiestaCreateRegistrazioneAusiliariaType.class, value);
    }
     
    /**
     * Create an instance of {@link RichiestaInterrogaPostaIngressoType }
     * 
     */
    public RichiestaInterrogaPostaIngressoType createRichiestaInterrogaPostaIngressoType() {
        return new RichiestaInterrogaPostaIngressoType();
    }

    /**
     * Create an instance of {@link RispostaRielaboraPECRicevutaType }
     * 
     */
    public RispostaRielaboraPECRicevutaType createRispostaRielaboraPECRicevutaType() {
        return new RispostaRielaboraPECRicevutaType();
    }

    /**
     * Create an instance of {@link RispostaRielaboraInvioPECType }
     * 
     */
    public RispostaRielaboraInvioPECType createRispostaRielaboraInvioPECType() {
        return new RispostaRielaboraInvioPECType();
    }

    /**
     * Create an instance of {@link RichiestaRielaboraInvioPECType }
     * 
     */
    public RichiestaRielaboraInvioPECType createRichiestaRielaboraInvioPECType() {
        return new RichiestaRielaboraInvioPECType();
    }

    /**
     * Create an instance of {@link RichiestaRielaboraPECRicevutaType }
     * 
     */
    public RichiestaRielaboraPECRicevutaType createRichiestaRielaboraPECRicevutaType() {
        return new RichiestaRielaboraPECRicevutaType();
    }

    /**
     * Create an instance of {@link RichiestaInviaMessaggioPostaType }
     * 
     */
    public RichiestaInviaMessaggioPostaType createRichiestaInviaMessaggioPostaType() {
        return new RichiestaInviaMessaggioPostaType();
    }

    /**
     * Create an instance of {@link RispostaInviaMessaggioPostaType }
     * 
     */
    public RispostaInviaMessaggioPostaType createRispostaInviaMessaggioPostaType() {
        return new RispostaInviaMessaggioPostaType();
    }

    /**
     * Create an instance of {@link RispostaInterrogaPostaUscita }
     * 
     */
    public RispostaInterrogaPostaUscita createRispostaInterrogaPostaUscita() {
        return new RispostaInterrogaPostaUscita();
    }

    /**
     * Create an instance of {@link RispostaInterrogaPostaUscitaType }
     * 
     */
    public RispostaInterrogaPostaUscitaType createRispostaInterrogaPostaUscitaType() {
        return new RispostaInterrogaPostaUscitaType();
    }
 
    /**
     * Create an instance of {@link RichiestaInterrogaPostaUscitaType }
     * 
     */
    public RichiestaInterrogaPostaUscitaType createRichiestaInterrogaPostaUscitaType() {
        return new RichiestaInterrogaPostaUscitaType();
    }

    /**
     * Create an instance of {@link RichiestaDettaglioPECType }
     * 
     */
    public RichiestaDettaglioPECType createRichiestaDettaglioPECType() {
        return new RichiestaDettaglioPECType();
    }

    /**
     * Create an instance of {@link RispostaDettaglioPECType }
     * 
     */
    public RispostaDettaglioPECType createRispostaDettaglioPECType() {
        return new RispostaDettaglioPECType();
    }

    /**
     * Create an instance of {@link RispostaInterrogaPostaIngresso }
     * 
     */
    public RispostaInterrogaPostaIngresso createRispostaInterrogaPostaIngresso() {
        return new RispostaInterrogaPostaIngresso();
    }

    /**
     * Create an instance of {@link RispostaInterrogaPostaIngressoType }
     * 
     */
    public RispostaInterrogaPostaIngressoType createRispostaInterrogaPostaIngressoType() {
        return new RispostaInterrogaPostaIngressoType();
    }
  
    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRielaboraPECRicevutaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_rielaboraPECRicevuta")
    public JAXBElement<RispostaRielaboraPECRicevutaType> createRispostaRielaboraPECRicevuta(RispostaRielaboraPECRicevutaType value) {
        return new JAXBElement<RispostaRielaboraPECRicevutaType>(_RispostaRielaboraPECRicevuta_QNAME, RispostaRielaboraPECRicevutaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRielaboraPECRicevutaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_rielaboraPECRicevuta")
    public JAXBElement<RichiestaRielaboraPECRicevutaType> createRichiestaRielaboraPECRicevuta(RichiestaRielaboraPECRicevutaType value) {
        return new JAXBElement<RichiestaRielaboraPECRicevutaType>(_RichiestaRielaboraPECRicevuta_QNAME, RichiestaRielaboraPECRicevutaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaDettaglioPECType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_dettaglioPEC")
    public JAXBElement<RichiestaDettaglioPECType> createRichiestaDettaglioPEC(RichiestaDettaglioPECType value) {
        return new JAXBElement<RichiestaDettaglioPECType>(_RichiestaDettaglioPEC_QNAME, RichiestaDettaglioPECType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaDettaglioPECType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_dettaglioPEC")
    public JAXBElement<RispostaDettaglioPECType> createRispostaDettaglioPEC(RispostaDettaglioPECType value) {
        return new JAXBElement<RispostaDettaglioPECType>(_RispostaDettaglioPEC_QNAME, RispostaDettaglioPECType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaInterrogaPostaUscitaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_interrogaPostaUscita")
    public JAXBElement<RichiestaInterrogaPostaUscitaType> createRichiestaInterrogaPostaUscita(RichiestaInterrogaPostaUscitaType value) {
        return new JAXBElement<RichiestaInterrogaPostaUscitaType>(_RichiestaInterrogaPostaUscita_QNAME, RichiestaInterrogaPostaUscitaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaRielaboraInvioPECType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_rielaboraInvioPEC")
    public JAXBElement<RispostaRielaboraInvioPECType> createRispostaRielaboraInvioPEC(RispostaRielaboraInvioPECType value) {
        return new JAXBElement<RispostaRielaboraInvioPECType>(_RispostaRielaboraInvioPEC_QNAME, RispostaRielaboraInvioPECType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaInterrogaPostaIngressoType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_interrogaPostaIngresso")
    public JAXBElement<RichiestaInterrogaPostaIngressoType> createRichiestaInterrogaPostaIngresso(RichiestaInterrogaPostaIngressoType value) {
        return new JAXBElement<RichiestaInterrogaPostaIngressoType>(_RichiestaInterrogaPostaIngresso_QNAME, RichiestaInterrogaPostaIngressoType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaRielaboraInvioPECType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_rielaboraInvioPEC")
    public JAXBElement<RichiestaRielaboraInvioPECType> createRichiestaRielaboraInvioPEC(RichiestaRielaboraInvioPECType value) {
        return new JAXBElement<RichiestaRielaboraInvioPECType>(_RichiestaRielaboraInvioPEC_QNAME, RichiestaRielaboraInvioPECType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaInviaMessaggioPostaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "richiesta_inviaMessaggioPosta")
    public JAXBElement<RichiestaInviaMessaggioPostaType> createRichiestaInviaMessaggioPosta(RichiestaInviaMessaggioPostaType value) {
        return new JAXBElement<RichiestaInviaMessaggioPostaType>(_RichiestaInviaMessaggioPosta_QNAME, RichiestaInviaMessaggioPostaType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaInviaMessaggioPostaType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.npsMessages", name = "risposta_inviaMessaggioPosta")
    public JAXBElement<RispostaInviaMessaggioPostaType> createRispostaInviaMessaggioPosta(RispostaInviaMessaggioPostaType value) {
        return new JAXBElement<RispostaInviaMessaggioPostaType>(_RispostaInviaMessaggioPosta_QNAME, RispostaInviaMessaggioPostaType.class, null, value);
    }

}
