
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.DocDocumentoFileType;


/**
 * Tipo di dato utilizzato nella risposta di caricamento di un documento sul documentale
 * 
 * <p>Classe Java per risposta_uploadDocumento_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_uploadDocumento_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsMessages}baseServiceResponse_type">
 *       &lt;sequence>
 *         &lt;element name="DatiDocumento" type="{http://mef.gov.it.v1.npsTypes}doc_documentoFile_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_uploadDocumento_type", propOrder = {
    "datiDocumento"
})
public class RispostaUploadDocumentoType
    extends BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "DatiDocumento")
    protected DocDocumentoFileType datiDocumento;

    /**
     * Recupera il valore della proprietà datiDocumento.
     * 
     * @return
     *     possible object is
     *     {@link DocDocumentoFileType }
     *     
     */
    public DocDocumentoFileType getDatiDocumento() {
        return datiDocumento;
    }

    /**
     * Imposta il valore della proprietà datiDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link DocDocumentoFileType }
     *     
     */
    public void setDatiDocumento(DocDocumentoFileType value) {
        this.datiDocumento = value;
    }

}
