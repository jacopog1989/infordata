
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Tipo di dato utilizzato nella richiesta di timbro di un documento
 * 
 * <p>Classe Java per richiesta_timbroDocumento_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_timbroDocumento_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Protocollo" type="{http://mef.gov.it.v1.npsMessages}identificativoProtocolloRequest_type"/>
 *         &lt;element name="Documento">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="FileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="MIMEType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="Hash" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *                   &lt;element name="Content" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_timbroDocumento_type", propOrder = {
    "protocollo",
    "documento"
})
public class RichiestaTimbroDocumentoType
    implements Serializable
{

    @XmlElement(name = "Protocollo", required = true)
    protected IdentificativoProtocolloRequestType protocollo;
    @XmlElement(name = "Documento", required = true)
    protected RichiestaTimbroDocumentoType.Documento documento;

    /**
     * Recupera il valore della proprietà protocollo.
     * 
     * @return
     *     possible object is
     *     {@link IdentificativoProtocolloRequestType }
     *     
     */
    public IdentificativoProtocolloRequestType getProtocollo() {
        return protocollo;
    }

    /**
     * Imposta il valore della proprietà protocollo.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificativoProtocolloRequestType }
     *     
     */
    public void setProtocollo(IdentificativoProtocolloRequestType value) {
        this.protocollo = value;
    }

    /**
     * Recupera il valore della proprietà documento.
     * 
     * @return
     *     possible object is
     *     {@link RichiestaTimbroDocumentoType.Documento }
     *     
     */
    public RichiestaTimbroDocumentoType.Documento getDocumento() {
        return documento;
    }

    /**
     * Imposta il valore della proprietà documento.
     * 
     * @param value
     *     allowed object is
     *     {@link RichiestaTimbroDocumentoType.Documento }
     *     
     */
    public void setDocumento(RichiestaTimbroDocumentoType.Documento value) {
        this.documento = value;
    }


    /**
     * <p>Classe Java per anonymous complex type.
     * 
     * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="FileName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="MIMEType" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="Hash" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
     *         &lt;element name="Content" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fileName",
        "mimeType",
        "hash",
        "content"
    })
    public static class Documento
        implements Serializable
    {

        @XmlElement(name = "FileName", required = true, nillable = true)
        protected String fileName;
        @XmlElement(name = "MIMEType", required = true)
        protected String mimeType;
        @XmlElement(name = "Hash", required = true, nillable = true)
        protected byte[] hash;
        @XmlElement(name = "Content", required = true)
        protected byte[] content;

        /**
         * Recupera il valore della proprietà fileName.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFileName() {
            return fileName;
        }

        /**
         * Imposta il valore della proprietà fileName.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFileName(String value) {
            this.fileName = value;
        }

        /**
         * Recupera il valore della proprietà mimeType.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMIMEType() {
            return mimeType;
        }

        /**
         * Imposta il valore della proprietà mimeType.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMIMEType(String value) {
            this.mimeType = value;
        }

        /**
         * Recupera il valore della proprietà hash.
         * 
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getHash() {
            return hash;
        }

        /**
         * Imposta il valore della proprietà hash.
         * 
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setHash(byte[] value) {
            this.hash = value;
        }

        /**
         * Recupera il valore della proprietà content.
         * 
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getContent() {
            return content;
        }

        /**
         * Imposta il valore della proprietà content.
         * 
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setContent(byte[] value) {
            this.content = value;
        }

    }

}
