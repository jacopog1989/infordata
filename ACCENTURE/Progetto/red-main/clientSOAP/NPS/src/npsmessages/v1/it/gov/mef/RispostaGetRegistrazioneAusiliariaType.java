
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.RegRegistrazioneAusiliariaType;


/**
 * Tipo di dato restituito a seguito di modifica di un registro ausiliario
 * 
 * <p>Classe Java per risposta_getRegistrazioneAusiliaria_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_getRegistrazioneAusiliaria_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsMessages}baseServiceResponse_type">
 *       &lt;sequence>
 *         &lt;element name="DatiRegistrazioneAusiliaria" type="{http://mef.gov.it.v1.npsTypes}reg_registrazioneAusiliaria_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_getRegistrazioneAusiliaria_type", propOrder = {
    "datiRegistrazioneAusiliaria"
})
public class RispostaGetRegistrazioneAusiliariaType
    extends BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "DatiRegistrazioneAusiliaria")
    protected RegRegistrazioneAusiliariaType datiRegistrazioneAusiliaria;

    /**
     * Recupera il valore della proprietà datiRegistrazioneAusiliaria.
     * 
     * @return
     *     possible object is
     *     {@link RegRegistrazioneAusiliariaType }
     *     
     */
    public RegRegistrazioneAusiliariaType getDatiRegistrazioneAusiliaria() {
        return datiRegistrazioneAusiliaria;
    }

    /**
     * Imposta il valore della proprietà datiRegistrazioneAusiliaria.
     * 
     * @param value
     *     allowed object is
     *     {@link RegRegistrazioneAusiliariaType }
     *     
     */
    public void setDatiRegistrazioneAusiliaria(RegRegistrazioneAusiliariaType value) {
        this.datiRegistrazioneAusiliaria = value;
    }

}
