
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.DocDocumentoType;


/**
 * Tipo di dato utilizzato nella richiesta di caricamento di un documento sul documentale ATMOS
 * 
 * <p>Classe Java per richiesta_uploadAtmos_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_uploadAtmos_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="IdDocumento" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *           &lt;element name="DatiDocumento" type="{http://mef.gov.it.v1.npsTypes}doc_documento_type"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_uploadAtmos_type", propOrder = {
    "idDocumento",
    "datiDocumento"
})
public class RichiestaUploadAtmosType
    implements Serializable
{

    @XmlElement(name = "IdDocumento")
    protected String idDocumento;
    @XmlElement(name = "DatiDocumento")
    protected DocDocumentoType datiDocumento;

    /**
     * Recupera il valore della proprietà idDocumento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumento() {
        return idDocumento;
    }

    /**
     * Imposta il valore della proprietà idDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumento(String value) {
        this.idDocumento = value;
    }

    /**
     * Recupera il valore della proprietà datiDocumento.
     * 
     * @return
     *     possible object is
     *     {@link DocDocumentoType }
     *     
     */
    public DocDocumentoType getDatiDocumento() {
        return datiDocumento;
    }

    /**
     * Imposta il valore della proprietà datiDocumento.
     * 
     * @param value
     *     allowed object is
     *     {@link DocDocumentoType }
     *     
     */
    public void setDatiDocumento(DocDocumentoType value) {
        this.datiDocumento = value;
    }

}
