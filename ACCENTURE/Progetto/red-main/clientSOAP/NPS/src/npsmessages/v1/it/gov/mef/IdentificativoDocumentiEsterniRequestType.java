
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * identificativo del documento da utilizzare da un documentale esterno
 * 
 * <p>Classe Java per identificativoDocumentiEsterniRequest_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="identificativoDocumentiEsterniRequest_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ChiaveDocumentale" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChiaveFascicolo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ChiaveFascicoloRiferimento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChiaveDocumentoPrincipale" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TipoDocumentoPrincipale" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChiaveAllegati" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "identificativoDocumentiEsterniRequest_type", propOrder = {
    "chiaveDocumentale",
    "chiaveFascicolo",
    "chiaveFascicoloRiferimento",
    "chiaveDocumentoPrincipale",
    "tipoDocumentoPrincipale",
    "chiaveAllegati"
})
public class IdentificativoDocumentiEsterniRequestType
    implements Serializable
{

    @XmlElement(name = "ChiaveDocumentale", required = true)
    protected String chiaveDocumentale;
    @XmlElement(name = "ChiaveFascicolo", required = true)
    protected String chiaveFascicolo;
    @XmlElement(name = "ChiaveFascicoloRiferimento")
    protected String chiaveFascicoloRiferimento;
    @XmlElement(name = "ChiaveDocumentoPrincipale")
    protected String chiaveDocumentoPrincipale;
    @XmlElement(name = "TipoDocumentoPrincipale")
    protected String tipoDocumentoPrincipale;
    @XmlElement(name = "ChiaveAllegati")
    protected List<String> chiaveAllegati;

    /**
     * Recupera il valore della proprietà chiaveDocumentale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveDocumentale() {
        return chiaveDocumentale;
    }

    /**
     * Imposta il valore della proprietà chiaveDocumentale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveDocumentale(String value) {
        this.chiaveDocumentale = value;
    }

    /**
     * Recupera il valore della proprietà chiaveFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveFascicolo() {
        return chiaveFascicolo;
    }

    /**
     * Imposta il valore della proprietà chiaveFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveFascicolo(String value) {
        this.chiaveFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà chiaveFascicoloRiferimento.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveFascicoloRiferimento() {
        return chiaveFascicoloRiferimento;
    }

    /**
     * Imposta il valore della proprietà chiaveFascicoloRiferimento.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveFascicoloRiferimento(String value) {
        this.chiaveFascicoloRiferimento = value;
    }

    /**
     * Recupera il valore della proprietà chiaveDocumentoPrincipale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveDocumentoPrincipale() {
        return chiaveDocumentoPrincipale;
    }

    /**
     * Imposta il valore della proprietà chiaveDocumentoPrincipale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveDocumentoPrincipale(String value) {
        this.chiaveDocumentoPrincipale = value;
    }

    /**
     * Recupera il valore della proprietà tipoDocumentoPrincipale.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDocumentoPrincipale() {
        return tipoDocumentoPrincipale;
    }

    /**
     * Imposta il valore della proprietà tipoDocumentoPrincipale.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDocumentoPrincipale(String value) {
        this.tipoDocumentoPrincipale = value;
    }

    /**
     * Gets the value of the chiaveAllegati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the chiaveAllegati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChiaveAllegati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getChiaveAllegati() {
        if (chiaveAllegati == null) {
            chiaveAllegati = new ArrayList<String>();
        }
        return this.chiaveAllegati;
    }

}
