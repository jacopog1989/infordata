
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Tipo di dato utilizzato nella risposta di modifica di un protocollo
 * 
 * <p>Classe Java per risposta_updateDatiProtocollo_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_updateDatiProtocollo_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsMessages}baseServiceResponse_type">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_updateDatiProtocollo_type")
@XmlSeeAlso({
    RispostaRimuoviAnnotazioneProtocolloType.class,
    RispostaAggiungiAnnotazioneProtocolloType.class
})
public class RispostaUpdateDatiProtocolloType
    extends BaseServiceResponseType
    implements Serializable
{


}
