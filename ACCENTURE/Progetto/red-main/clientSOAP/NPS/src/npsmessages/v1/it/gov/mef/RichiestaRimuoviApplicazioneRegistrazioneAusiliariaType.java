
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.RegRegistrazioneAusiliariaType;


/**
 * Tipo di dato per richiedere la modifica di un registro ausiliario
 * 
 * <p>Java class for richiesta_rimuoviApplicazioneRegistrazioneAusiliaria_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_rimuoviApplicazioneRegistrazioneAusiliaria_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsTypes}reg_registrazioneAusiliaria_type">
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_rimuoviApplicazioneRegistrazioneAusiliaria_type")
public class RichiestaRimuoviApplicazioneRegistrazioneAusiliariaType
    extends RegRegistrazioneAusiliariaType
    implements Serializable
{


}
