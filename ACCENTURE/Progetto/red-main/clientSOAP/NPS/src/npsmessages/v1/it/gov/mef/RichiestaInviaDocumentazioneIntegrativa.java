
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.ProtAllaccioProtocolloType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsMessages}richiesta_inviaDocumentazioneIntegrativa_type">
 *       &lt;sequence>
 *         &lt;element name="Risposta" type="{http://mef.gov.it.v1.npsTypes}prot_allaccioProtocollo_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "risposta"
})
@XmlRootElement(name = "richiesta_inviaDocumentazioneIntegrativa")
public class RichiestaInviaDocumentazioneIntegrativa
    extends RichiestaInviaDocumentazioneIntegrativaType
    implements Serializable
{

    @XmlElement(name = "Risposta")
    protected ProtAllaccioProtocolloType risposta;

    /**
     * Recupera il valore della proprietà risposta.
     * 
     * @return
     *     possible object is
     *     {@link ProtAllaccioProtocolloType }
     *     
     */
    public ProtAllaccioProtocolloType getRisposta() {
        return risposta;
    }

    /**
     * Imposta il valore della proprietà risposta.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtAllaccioProtocolloType }
     *     
     */
    public void setRisposta(ProtAllaccioProtocolloType value) {
        this.risposta = value;
    }

}
