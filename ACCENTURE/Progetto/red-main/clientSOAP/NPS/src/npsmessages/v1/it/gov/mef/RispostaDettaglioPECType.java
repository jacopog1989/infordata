
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.EmailMessageType;


/**
 * Tipo di dato restituito a seguito della richiesta il dettaglio della PEC
 * 
 * <p>Classe Java per risposta_dettaglioPEC_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="risposta_dettaglioPEC_type">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mef.gov.it.v1.npsMessages}baseServiceResponse_type">
 *       &lt;sequence>
 *         &lt;element name="DatiMessaggio" type="{http://mef.gov.it.v1.npsTypes}email_message_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_dettaglioPEC_type", propOrder = {
    "datiMessaggio"
})
public class RispostaDettaglioPECType
    extends BaseServiceResponseType
    implements Serializable
{

    @XmlElement(name = "DatiMessaggio")
    protected EmailMessageType datiMessaggio;

    /**
     * Recupera il valore della proprietà datiMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link EmailMessageType }
     *     
     */
    public EmailMessageType getDatiMessaggio() {
        return datiMessaggio;
    }

    /**
     * Imposta il valore della proprietà datiMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailMessageType }
     *     
     */
    public void setDatiMessaggio(EmailMessageType value) {
        this.datiMessaggio = value;
    }

}
