
package npsmessages.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import npstypes.v1.it.gov.mef.EmailIndirizzoEmailType;
import npstypes.v1.it.gov.mef.OrgCasellaEmailType;


/**
 * Tipo di dato per richiedere l'invio di mail dalla casella gestita dal sistema
 * 
 * <p>Classe Java per richiesta_inviaMessaggioPosta_type complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="richiesta_inviaMessaggioPosta_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdMessaggio" type="{http://mef.gov.it.v1.npsTypes}Guid"/>
 *         &lt;element name="CasellaEmail" type="{http://mef.gov.it.v1.npsTypes}org_casellaEmail_type"/>
 *         &lt;element name="CodiceFlusso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ChiaveFascicolo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Messaggio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="CorpoMessaggio" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DestinatarioEmail" type="{http://mef.gov.it.v1.npsTypes}email_indirizzoEmail_type" maxOccurs="unbounded"/>
 *         &lt;element name="Allegati" type="{http://mef.gov.it.v1.npsTypes}Guid" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="NotificaSpedizione" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_inviaMessaggioPosta_type", propOrder = {
    "idMessaggio",
    "casellaEmail",
    "codiceFlusso",
    "chiaveFascicolo",
    "messaggio",
    "corpoMessaggio",
    "destinatarioEmail",
    "allegati",
    "notificaSpedizione"
})
public class RichiestaInviaMessaggioPostaType
    implements Serializable
{

    @XmlElement(name = "IdMessaggio", required = true)
    protected String idMessaggio;
    @XmlElement(name = "CasellaEmail", required = true)
    protected OrgCasellaEmailType casellaEmail;
    @XmlElement(name = "CodiceFlusso")
    protected String codiceFlusso;
    @XmlElement(name = "ChiaveFascicolo")
    protected String chiaveFascicolo;
    @XmlElement(name = "Messaggio", required = true)
    protected String messaggio;
    @XmlElement(name = "CorpoMessaggio", required = true)
    protected String corpoMessaggio;
    @XmlElement(name = "DestinatarioEmail", required = true)
    protected List<EmailIndirizzoEmailType> destinatarioEmail;
    @XmlElement(name = "Allegati")
    protected List<String> allegati;
    @XmlElement(name = "NotificaSpedizione", defaultValue = "false")
    protected boolean notificaSpedizione;

    /**
     * Recupera il valore della proprietà idMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdMessaggio() {
        return idMessaggio;
    }

    /**
     * Imposta il valore della proprietà idMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdMessaggio(String value) {
        this.idMessaggio = value;
    }

    /**
     * Recupera il valore della proprietà casellaEmail.
     * 
     * @return
     *     possible object is
     *     {@link OrgCasellaEmailType }
     *     
     */
    public OrgCasellaEmailType getCasellaEmail() {
        return casellaEmail;
    }

    /**
     * Imposta il valore della proprietà casellaEmail.
     * 
     * @param value
     *     allowed object is
     *     {@link OrgCasellaEmailType }
     *     
     */
    public void setCasellaEmail(OrgCasellaEmailType value) {
        this.casellaEmail = value;
    }

    /**
     * Recupera il valore della proprietà codiceFlusso.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiceFlusso() {
        return codiceFlusso;
    }

    /**
     * Imposta il valore della proprietà codiceFlusso.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiceFlusso(String value) {
        this.codiceFlusso = value;
    }

    /**
     * Recupera il valore della proprietà chiaveFascicolo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChiaveFascicolo() {
        return chiaveFascicolo;
    }

    /**
     * Imposta il valore della proprietà chiaveFascicolo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChiaveFascicolo(String value) {
        this.chiaveFascicolo = value;
    }

    /**
     * Recupera il valore della proprietà messaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessaggio() {
        return messaggio;
    }

    /**
     * Imposta il valore della proprietà messaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessaggio(String value) {
        this.messaggio = value;
    }

    /**
     * Recupera il valore della proprietà corpoMessaggio.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCorpoMessaggio() {
        return corpoMessaggio;
    }

    /**
     * Imposta il valore della proprietà corpoMessaggio.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCorpoMessaggio(String value) {
        this.corpoMessaggio = value;
    }

    /**
     * Gets the value of the destinatarioEmail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the destinatarioEmail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDestinatarioEmail().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EmailIndirizzoEmailType }
     * 
     * 
     */
    public List<EmailIndirizzoEmailType> getDestinatarioEmail() {
        if (destinatarioEmail == null) {
            destinatarioEmail = new ArrayList<EmailIndirizzoEmailType>();
        }
        return this.destinatarioEmail;
    }

    /**
     * Gets the value of the allegati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allegati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllegati().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getAllegati() {
        if (allegati == null) {
            allegati = new ArrayList<String>();
        }
        return this.allegati;
    }

    /**
     * Recupera il valore della proprietà notificaSpedizione.
     * 
     */
    public boolean isNotificaSpedizione() {
        return notificaSpedizione;
    }

    /**
     * Imposta il valore della proprietà notificaSpedizione.
     * 
     */
    public void setNotificaSpedizione(boolean value) {
        this.notificaSpedizione = value;
    }

}
