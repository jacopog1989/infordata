
package nps.v2.it.gov.mef.servizi.interfaccianps;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "InterfacciaFlusso_AUT", targetNamespace = "http://mef.gov.it.v2.nps/servizi/InterfacciaNPS", wsdlLocation = "file:/C:/Users/a.dilegge/Desktop/WSDL%20NPS%20v%202.14/mef_InterfacciaFlusso_AutomatizzatoNPS_2.1.wsdl")
public class InterfacciaFlussoAUT_Service
    extends Service
{

    private final static URL INTERFACCIAFLUSSOAUT_WSDL_LOCATION;
    private final static WebServiceException INTERFACCIAFLUSSOAUT_EXCEPTION;
    private final static QName INTERFACCIAFLUSSOAUT_QNAME = new QName("http://mef.gov.it.v2.nps/servizi/InterfacciaNPS", "InterfacciaFlusso_AUT");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("file:/C:/Users/a.dilegge/Desktop/WSDL%20NPS%20v%202.14/mef_InterfacciaFlusso_AutomatizzatoNPS_2.1.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        INTERFACCIAFLUSSOAUT_WSDL_LOCATION = url;
        INTERFACCIAFLUSSOAUT_EXCEPTION = e;
    }

    public InterfacciaFlussoAUT_Service() {
        super(__getWsdlLocation(), INTERFACCIAFLUSSOAUT_QNAME);
    }

    public InterfacciaFlussoAUT_Service(WebServiceFeature... features) {
        super(__getWsdlLocation(), INTERFACCIAFLUSSOAUT_QNAME, features);
    }

    public InterfacciaFlussoAUT_Service(URL wsdlLocation) {
        super(wsdlLocation, INTERFACCIAFLUSSOAUT_QNAME);
    }

    public InterfacciaFlussoAUT_Service(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, INTERFACCIAFLUSSOAUT_QNAME, features);
    }

    public InterfacciaFlussoAUT_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public InterfacciaFlussoAUT_Service(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns InterfacciaFlussoAUT
     */
    @WebEndpoint(name = "InterfacciaFlusso_AUT_SOAPPort")
    public InterfacciaFlussoAUT getInterfacciaFlussoAUTSOAPPort() {
        return super.getPort(new QName("http://mef.gov.it.v2.nps/servizi/InterfacciaNPS", "InterfacciaFlusso_AUT_SOAPPort"), InterfacciaFlussoAUT.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns InterfacciaFlussoAUT
     */
    @WebEndpoint(name = "InterfacciaFlusso_AUT_SOAPPort")
    public InterfacciaFlussoAUT getInterfacciaFlussoAUTSOAPPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://mef.gov.it.v2.nps/servizi/InterfacciaNPS", "InterfacciaFlusso_AUT_SOAPPort"), InterfacciaFlussoAUT.class, features);
    }

    private static URL __getWsdlLocation() {
        if (INTERFACCIAFLUSSOAUT_EXCEPTION!= null) {
            throw INTERFACCIAFLUSSOAUT_EXCEPTION;
        }
        return INTERFACCIAFLUSSOAUT_WSDL_LOCATION;
    }

}
