
package nps.v2.it.gov.mef.servizi.interfaccianps;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import it.gov.mef.servizi.common.headeraccessoapplicativo.AccessoApplicativo;
import npsmessages.v1.it.gov.mef.RichiestaAcquisisciDocumentoType;
import npsmessages.v1.it.gov.mef.RichiestaDettaglioOperazioniFlussoType;
import npsmessages.v1.it.gov.mef.RichiestaInviaAttoType;
import npsmessages.v1.it.gov.mef.RichiestaInviaComunicazioneGenericaType;
import npsmessages.v1.it.gov.mef.RichiestaInviaDocumentazioneIntegrativa;
import npsmessages.v1.it.gov.mef.RichiestaMonitoraggioOperazioniSospesoType;
import npsmessages.v1.it.gov.mef.RichiestaRitiroAutotutelaType;
import npsmessages.v1.it.gov.mef.RispostaAcquisisciDocumentoType;
import npsmessages.v1.it.gov.mef.RispostaDettaglioOperazioniFlussoType;
import npsmessages.v1.it.gov.mef.RispostaInviaAttoType;
import npsmessages.v1.it.gov.mef.RispostaInviaComunicazioneGenericaType;
import npsmessages.v1.it.gov.mef.RispostaInviaDocumentazioneIntegrativaType;
import npsmessages.v1.it.gov.mef.RispostaMonitoraggioOperazioniSospesoType;
import npsmessages.v1.it.gov.mef.RispostaRitiroAutotutelaType;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "InterfacciaFlusso_AUT", targetNamespace = "http://mef.gov.it.v2.nps/servizi/InterfacciaNPS")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    npstypes.v1.it.gov.mef.ObjectFactory.class,
    it.gov.digitpa.protocollo.ObjectFactory.class,
    it.gov.mef.servizi.common.headeraccessoapplicativo.ObjectFactory.class,
    it.gov.mef.servizi.common.headerfault.ObjectFactory.class,
    npsmessages.v1.it.gov.mef.ObjectFactory.class,
    npstypesestesi.v1.it.gov.mef.ObjectFactory.class,
    org.xmlsoap.schemas.soap.envelope.ObjectFactory.class
})
public interface InterfacciaFlussoAUT {


    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns npsmessages.v1.it.gov.mef.RispostaInviaAttoType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:#inviaAtto")
    @WebResult(name = "risposta_inviaAtto", targetNamespace = "http://mef.gov.it.v1.npsMessages", partName = "parameters")
    public RispostaInviaAttoType inviaAtto(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_inviaAtto", targetNamespace = "http://mef.gov.it.v1.npsMessages", partName = "parameters")
        RichiestaInviaAttoType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns npsmessages.v1.it.gov.mef.RispostaInviaDocumentazioneIntegrativaType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:#invioDocumentazioneIntegrativa")
    @WebResult(name = "risposta_inviaDocumentazioneIntegrativa", targetNamespace = "http://mef.gov.it.v1.npsMessages", partName = "parameters")
    public RispostaInviaDocumentazioneIntegrativaType invioDocumentazioneIntegrativa(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_inviaDocumentazioneIntegrativa", targetNamespace = "http://mef.gov.it.v1.npsMessages", partName = "parameters")
        RichiestaInviaDocumentazioneIntegrativa parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns npsmessages.v1.it.gov.mef.RispostaInviaComunicazioneGenericaType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:#inviaComunicazioneGenerica")
    @WebResult(name = "risposta_inviaComunicazioneGenerica", targetNamespace = "http://mef.gov.it.v1.npsMessages", partName = "parameters")
    public RispostaInviaComunicazioneGenericaType inviaComunicazioneGenerica(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_inviaComunicazioneGenerica", targetNamespace = "http://mef.gov.it.v1.npsMessages", partName = "parameters")
        RichiestaInviaComunicazioneGenericaType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns npsmessages.v1.it.gov.mef.RispostaRitiroAutotutelaType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:#ritiroAutotutela")
    @WebResult(name = "risposta_ritiroAutotutela", targetNamespace = "http://mef.gov.it.v1.npsMessages", partName = "parameters")
    public RispostaRitiroAutotutelaType ritiroAutotutela(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_ritiroAutotutela", targetNamespace = "http://mef.gov.it.v1.npsMessages", partName = "parameters")
        RichiestaRitiroAutotutelaType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns npsmessages.v1.it.gov.mef.RispostaDettaglioOperazioniFlussoType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:#dettaglioOperazioniFlusso")
    @WebResult(name = "risposta_dettaglioOperazioniFlusso", targetNamespace = "http://mef.gov.it.v1.npsMessages", partName = "parameters")
    public RispostaDettaglioOperazioniFlussoType dettaglioOperazioniFlusso(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_dettaglioOperazioniFlusso", targetNamespace = "http://mef.gov.it.v1.npsMessages", partName = "parameters")
        RichiestaDettaglioOperazioniFlussoType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns npsmessages.v1.it.gov.mef.RispostaMonitoraggioOperazioniSospesoType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:#monitoraggioOperazioniSospeso")
    @WebResult(name = "risposta_monitoraggioOperazioniSospeso", targetNamespace = "http://mef.gov.it.v1.npsMessages", partName = "parameters")
    public RispostaMonitoraggioOperazioniSospesoType monitoraggioOperazioniSospeso(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_monitoraggioOperazioniSospeso", targetNamespace = "http://mef.gov.it.v1.npsMessages", partName = "parameters")
        RichiestaMonitoraggioOperazioniSospesoType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns npsmessages.v1.it.gov.mef.RispostaAcquisisciDocumentoType
     * @throws SecurityFault
     * @throws GenericFault
     */
    @WebMethod(action = "urn:#acquisisciDocumento")
    @WebResult(name = "risposta_acquisisciDocumento", targetNamespace = "http://mef.gov.it.v1.npsMessages", partName = "parameters")
    public RispostaAcquisisciDocumentoType acquisisciDocumento(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_acquisisciDocumento", targetNamespace = "http://mef.gov.it.v1.npsMessages", partName = "parameters")
        RichiestaAcquisisciDocumentoType parameters)
        throws GenericFault, SecurityFault
    ;

}
