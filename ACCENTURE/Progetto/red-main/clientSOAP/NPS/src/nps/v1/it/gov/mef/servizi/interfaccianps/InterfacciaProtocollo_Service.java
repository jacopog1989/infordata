
package nps.v1.it.gov.mef.servizi.interfaccianps;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "InterfacciaProtocollo", targetNamespace = "http://mef.gov.it.v1.nps/servizi/InterfacciaNPS", wsdlLocation = "file:/C:/Users/a.dilegge/Desktop/WSDL%20NPS%20v%202.14/mef_InterfacciaProtocolloNPS_2.1.wsdl")
public class InterfacciaProtocollo_Service
    extends Service
{

    private final static URL INTERFACCIAPROTOCOLLO_WSDL_LOCATION;
    private final static WebServiceException INTERFACCIAPROTOCOLLO_EXCEPTION;
    private final static QName INTERFACCIAPROTOCOLLO_QNAME = new QName("http://mef.gov.it.v1.nps/servizi/InterfacciaNPS", "InterfacciaProtocollo");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("file:/C:/Users/a.dilegge/Desktop/WSDL%20NPS%20v%202.14/mef_InterfacciaProtocolloNPS_2.1.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        INTERFACCIAPROTOCOLLO_WSDL_LOCATION = url;
        INTERFACCIAPROTOCOLLO_EXCEPTION = e;
    }

    public InterfacciaProtocollo_Service() {
        super(__getWsdlLocation(), INTERFACCIAPROTOCOLLO_QNAME);
    }

    public InterfacciaProtocollo_Service(WebServiceFeature... features) {
        super(__getWsdlLocation(), INTERFACCIAPROTOCOLLO_QNAME, features);
    }

    public InterfacciaProtocollo_Service(URL wsdlLocation) {
        super(wsdlLocation, INTERFACCIAPROTOCOLLO_QNAME);
    }

    public InterfacciaProtocollo_Service(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, INTERFACCIAPROTOCOLLO_QNAME, features);
    }

    public InterfacciaProtocollo_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public InterfacciaProtocollo_Service(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns InterfacciaProtocollo
     */
    @WebEndpoint(name = "InterfacciaProtocollo_SOAPPort")
    public InterfacciaProtocollo getInterfacciaProtocolloSOAPPort() {
        return super.getPort(new QName("http://mef.gov.it.v1.nps/servizi/InterfacciaNPS", "InterfacciaProtocollo_SOAPPort"), InterfacciaProtocollo.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns InterfacciaProtocollo
     */
    @WebEndpoint(name = "InterfacciaProtocollo_SOAPPort")
    public InterfacciaProtocollo getInterfacciaProtocolloSOAPPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://mef.gov.it.v1.nps/servizi/InterfacciaNPS", "InterfacciaProtocollo_SOAPPort"), InterfacciaProtocollo.class, features);
    }

    private static URL __getWsdlLocation() {
        if (INTERFACCIAPROTOCOLLO_EXCEPTION!= null) {
            throw INTERFACCIAPROTOCOLLO_EXCEPTION;
        }
        return INTERFACCIAPROTOCOLLO_WSDL_LOCATION;
    }

}
