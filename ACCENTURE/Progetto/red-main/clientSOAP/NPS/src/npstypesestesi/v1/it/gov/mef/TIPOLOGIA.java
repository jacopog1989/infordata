
package npstypesestesi.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Il complexType TIPOLOGIA identifica l'insieme dei dati relativi al tipo documento per la protocollazione. La tipologia è composta da NOME, FAMIGLIA, VERSIONE e un insieme di attributi estesi del tipo documento
 * 
 * <p>Classe Java per TIPOLOGIA complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="TIPOLOGIA">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NOME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FAMIGLIA" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="VERSIONE" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="MetadatoAssociato" type="{http://mef.gov.it.v1.npsTypesEstesi/}MetaDatoAssociato" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TIPOLOGIA", propOrder = {
    "nome",
    "famiglia",
    "versione",
    "metadatoAssociato"
})
public class TIPOLOGIA
    implements Serializable
{

    @XmlElement(name = "NOME", required = true)
    protected String nome;
    @XmlElement(name = "FAMIGLIA")
    protected int famiglia;
    @XmlElement(name = "VERSIONE")
    protected int versione;
    @XmlElement(name = "MetadatoAssociato")
    protected List<MetaDatoAssociato> metadatoAssociato;

    /**
     * Recupera il valore della proprietà nome.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNOME() {
        return nome;
    }

    /**
     * Imposta il valore della proprietà nome.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNOME(String value) {
        this.nome = value;
    }

    /**
     * Recupera il valore della proprietà famiglia.
     * 
     */
    public int getFAMIGLIA() {
        return famiglia;
    }

    /**
     * Imposta il valore della proprietà famiglia.
     * 
     */
    public void setFAMIGLIA(int value) {
        this.famiglia = value;
    }

    /**
     * Recupera il valore della proprietà versione.
     * 
     */
    public int getVERSIONE() {
        return versione;
    }

    /**
     * Imposta il valore della proprietà versione.
     * 
     */
    public void setVERSIONE(int value) {
        this.versione = value;
    }

    /**
     * Gets the value of the metadatoAssociato property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the metadatoAssociato property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMetadatoAssociato().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MetaDatoAssociato }
     * 
     * 
     */
    public List<MetaDatoAssociato> getMetadatoAssociato() {
        if (metadatoAssociato == null) {
            metadatoAssociato = new ArrayList<MetaDatoAssociato>();
        }
        return this.metadatoAssociato;
    }

}
