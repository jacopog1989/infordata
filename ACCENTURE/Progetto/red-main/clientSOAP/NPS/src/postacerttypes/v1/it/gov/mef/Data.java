
package postacerttypes.v1.it.gov.mef;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://mef.gov.it.v1.postacertTypes/}giorno"/>
 *         &lt;element ref="{http://mef.gov.it.v1.postacertTypes/}ora"/>
 *       &lt;/sequence>
 *       &lt;attribute name="zona" use="required" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "giorno",
    "ora"
})
@XmlRootElement(name = "data")
public class Data
    implements Serializable
{

    @XmlElement(required = true)
    protected Giorno giorno;
    @XmlElement(required = true)
    protected Ora ora;
    @XmlAttribute(name = "zona", required = true)
    @XmlSchemaType(name = "anySimpleType")
    protected String zona;

    /**
     * Recupera il valore della proprietà giorno.
     * 
     * @return
     *     possible object is
     *     {@link Giorno }
     *     
     */
    public Giorno getGiorno() {
        return giorno;
    }

    /**
     * Imposta il valore della proprietà giorno.
     * 
     * @param value
     *     allowed object is
     *     {@link Giorno }
     *     
     */
    public void setGiorno(Giorno value) {
        this.giorno = value;
    }

    /**
     * Recupera il valore della proprietà ora.
     * 
     * @return
     *     possible object is
     *     {@link Ora }
     *     
     */
    public Ora getOra() {
        return ora;
    }

    /**
     * Imposta il valore della proprietà ora.
     * 
     * @param value
     *     allowed object is
     *     {@link Ora }
     *     
     */
    public void setOra(Ora value) {
        this.ora = value;
    }

    /**
     * Recupera il valore della proprietà zona.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZona() {
        return zona;
    }

    /**
     * Imposta il valore della proprietà zona.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZona(String value) {
        this.zona = value;
    }

}
