
package postacerttypes.v1.it.gov.mef;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the postacerttypes.v1.it.gov.mef package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Postacert_QNAME = new QName("http://mef.gov.it.v1.postacertTypes/", "postacert");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: postacerttypes.v1.it.gov.mef
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Ricezione }
     * 
     */
    public Ricezione createRicezione() {
        return new Ricezione();
    }

    /**
     * Create an instance of {@link ErroreEsteso }
     * 
     */
    public ErroreEsteso createErroreEsteso() {
        return new ErroreEsteso();
    }

    /**
     * Create an instance of {@link Mittente }
     * 
     */
    public Mittente createMittente() {
        return new Mittente();
    }

    /**
     * Create an instance of {@link Data }
     * 
     */
    public Data createData() {
        return new Data();
    }

    /**
     * Create an instance of {@link Giorno }
     * 
     */
    public Giorno createGiorno() {
        return new Giorno();
    }

    /**
     * Create an instance of {@link Ora }
     * 
     */
    public Ora createOra() {
        return new Ora();
    }

    /**
     * Create an instance of {@link GestoreEmittente }
     * 
     */
    public GestoreEmittente createGestoreEmittente() {
        return new GestoreEmittente();
    }

    /**
     * Create an instance of {@link Consegna }
     * 
     */
    public Consegna createConsegna() {
        return new Consegna();
    }

    /**
     * Create an instance of {@link Oggetto }
     * 
     */
    public Oggetto createOggetto() {
        return new Oggetto();
    }

    /**
     * Create an instance of {@link Msgid }
     * 
     */
    public Msgid createMsgid() {
        return new Msgid();
    }

    /**
     * Create an instance of {@link Ricevuta }
     * 
     */
    public Ricevuta createRicevuta() {
        return new Ricevuta();
    }

    /**
     * Create an instance of {@link PostacertType }
     * 
     */
    public PostacertType createPostacertType() {
        return new PostacertType();
    }

    /**
     * Create an instance of {@link Identificativo }
     * 
     */
    public Identificativo createIdentificativo() {
        return new Identificativo();
    }

    /**
     * Create an instance of {@link Destinatari }
     * 
     */
    public Destinatari createDestinatari() {
        return new Destinatari();
    }

    /**
     * Create an instance of {@link Risposte }
     * 
     */
    public Risposte createRisposte() {
        return new Risposte();
    }

    /**
     * Create an instance of {@link Intestazione }
     * 
     */
    public Intestazione createIntestazione() {
        return new Intestazione();
    }

    /**
     * Create an instance of {@link Dati }
     * 
     */
    public Dati createDati() {
        return new Dati();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PostacertType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it.v1.postacertTypes/", name = "postacert")
    public JAXBElement<PostacertType> createPostacert(PostacertType value) {
        return new JAXBElement<PostacertType>(_Postacert_QNAME, PostacertType.class, null, value);
    }

}
