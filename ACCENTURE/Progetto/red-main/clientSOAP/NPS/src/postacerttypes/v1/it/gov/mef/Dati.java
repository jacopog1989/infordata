
package postacerttypes.v1.it.gov.mef;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://mef.gov.it.v1.postacertTypes/}gestore-emittente"/>
 *         &lt;element ref="{http://mef.gov.it.v1.postacertTypes/}data"/>
 *         &lt;element ref="{http://mef.gov.it.v1.postacertTypes/}identificativo"/>
 *         &lt;element ref="{http://mef.gov.it.v1.postacertTypes/}msgid" minOccurs="0"/>
 *         &lt;element ref="{http://mef.gov.it.v1.postacertTypes/}ricevuta" minOccurs="0"/>
 *         &lt;element ref="{http://mef.gov.it.v1.postacertTypes/}consegna" minOccurs="0"/>
 *         &lt;element ref="{http://mef.gov.it.v1.postacertTypes/}ricezione" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{http://mef.gov.it.v1.postacertTypes/}errore-esteso" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "gestoreEmittente",
    "data",
    "identificativo",
    "msgid",
    "ricevuta",
    "consegna",
    "ricezione",
    "erroreEsteso"
})
@XmlRootElement(name = "dati")
public class Dati
    implements Serializable
{

    @XmlElement(name = "gestore-emittente", required = true)
    protected GestoreEmittente gestoreEmittente;
    @XmlElement(required = true)
    protected Data data;
    @XmlElement(required = true)
    protected Identificativo identificativo;
    protected Msgid msgid;
    protected Ricevuta ricevuta;
    protected Consegna consegna;
    protected List<Ricezione> ricezione;
    @XmlElement(name = "errore-esteso")
    protected ErroreEsteso erroreEsteso;

    /**
     * Recupera il valore della proprietà gestoreEmittente.
     * 
     * @return
     *     possible object is
     *     {@link GestoreEmittente }
     *     
     */
    public GestoreEmittente getGestoreEmittente() {
        return gestoreEmittente;
    }

    /**
     * Imposta il valore della proprietà gestoreEmittente.
     * 
     * @param value
     *     allowed object is
     *     {@link GestoreEmittente }
     *     
     */
    public void setGestoreEmittente(GestoreEmittente value) {
        this.gestoreEmittente = value;
    }

    /**
     * Recupera il valore della proprietà data.
     * 
     * @return
     *     possible object is
     *     {@link Data }
     *     
     */
    public Data getData() {
        return data;
    }

    /**
     * Imposta il valore della proprietà data.
     * 
     * @param value
     *     allowed object is
     *     {@link Data }
     *     
     */
    public void setData(Data value) {
        this.data = value;
    }

    /**
     * Recupera il valore della proprietà identificativo.
     * 
     * @return
     *     possible object is
     *     {@link Identificativo }
     *     
     */
    public Identificativo getIdentificativo() {
        return identificativo;
    }

    /**
     * Imposta il valore della proprietà identificativo.
     * 
     * @param value
     *     allowed object is
     *     {@link Identificativo }
     *     
     */
    public void setIdentificativo(Identificativo value) {
        this.identificativo = value;
    }

    /**
     * Recupera il valore della proprietà msgid.
     * 
     * @return
     *     possible object is
     *     {@link Msgid }
     *     
     */
    public Msgid getMsgid() {
        return msgid;
    }

    /**
     * Imposta il valore della proprietà msgid.
     * 
     * @param value
     *     allowed object is
     *     {@link Msgid }
     *     
     */
    public void setMsgid(Msgid value) {
        this.msgid = value;
    }

    /**
     * Recupera il valore della proprietà ricevuta.
     * 
     * @return
     *     possible object is
     *     {@link Ricevuta }
     *     
     */
    public Ricevuta getRicevuta() {
        return ricevuta;
    }

    /**
     * Imposta il valore della proprietà ricevuta.
     * 
     * @param value
     *     allowed object is
     *     {@link Ricevuta }
     *     
     */
    public void setRicevuta(Ricevuta value) {
        this.ricevuta = value;
    }

    /**
     * Recupera il valore della proprietà consegna.
     * 
     * @return
     *     possible object is
     *     {@link Consegna }
     *     
     */
    public Consegna getConsegna() {
        return consegna;
    }

    /**
     * Imposta il valore della proprietà consegna.
     * 
     * @param value
     *     allowed object is
     *     {@link Consegna }
     *     
     */
    public void setConsegna(Consegna value) {
        this.consegna = value;
    }

    /**
     * Gets the value of the ricezione property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ricezione property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRicezione().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Ricezione }
     * 
     * 
     */
    public List<Ricezione> getRicezione() {
        if (ricezione == null) {
            ricezione = new ArrayList<Ricezione>();
        }
        return this.ricezione;
    }

    /**
     * Recupera il valore della proprietà erroreEsteso.
     * 
     * @return
     *     possible object is
     *     {@link ErroreEsteso }
     *     
     */
    public ErroreEsteso getErroreEsteso() {
        return erroreEsteso;
    }

    /**
     * Imposta il valore della proprietà erroreEsteso.
     * 
     * @param value
     *     allowed object is
     *     {@link ErroreEsteso }
     *     
     */
    public void setErroreEsteso(ErroreEsteso value) {
        this.erroreEsteso = value;
    }

}
