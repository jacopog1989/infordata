package it.gov.mef.servizi.interfacciaopsicoge;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;
import it.gov.mef.servizi.common.headeraccessoapplicativo.AccessoApplicativo;
import it.gov.mef.servizi.interfacciaopsicoge.types_1.RichiestaGetOPType;
import it.gov.mef.servizi.interfacciaopsicoge.types_1.RichiestaListaOPType;
import it.gov.mef.servizi.interfacciaopsicoge.types_1.RispostaGetOPType;
import it.gov.mef.servizi.interfacciaopsicoge.types_1.RispostaListaOPType;

public class InterfacciaOPSICOGE_SOAPPortProxy{

    protected Descriptor _descriptor;

    public class Descriptor {
        private it.gov.mef.servizi.interfacciaopsicoge.MefInterfacciaOPSICOGE _service = null;
        private it.gov.mef.servizi.interfacciaopsicoge.InterfacciaOPSICOGE _proxy = null;
        private Dispatch<Source> _dispatch = null;

        public Descriptor() {
            init();
        }

        public Descriptor(URL wsdlLocation, QName serviceName) {
            _service = new it.gov.mef.servizi.interfacciaopsicoge.MefInterfacciaOPSICOGE(wsdlLocation, serviceName);
            initCommon();
        }

        public void init() {
            _service = null;
            _proxy = null;
            _dispatch = null;
            _service = new it.gov.mef.servizi.interfacciaopsicoge.MefInterfacciaOPSICOGE();
            initCommon();
        }

        private void initCommon() {
            _proxy = _service.getInterfacciaOPSICOGESOAPPort();
        }

        public it.gov.mef.servizi.interfacciaopsicoge.InterfacciaOPSICOGE getProxy() {
            return _proxy;
        }

        public Dispatch<Source> getDispatch() {
            if (_dispatch == null ) {
                QName portQName = new QName("", "InterfacciaOPSICOGE_SOAPPort");
                _dispatch = _service.createDispatch(portQName, Source.class, Service.Mode.MESSAGE);

                String proxyEndpointUrl = getEndpoint();
                BindingProvider bp = (BindingProvider) _dispatch;
                String dispatchEndpointUrl = (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
                if (!dispatchEndpointUrl.equals(proxyEndpointUrl))
                    bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, proxyEndpointUrl);
            }
            return _dispatch;
        }

        public String getEndpoint() {
            BindingProvider bp = (BindingProvider) _proxy;
            return (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
        }

        public void setEndpoint(String endpointUrl) {
            BindingProvider bp = (BindingProvider) _proxy;
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);

            if (_dispatch != null ) {
                bp = (BindingProvider) _dispatch;
                bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);
            }
        }

        public void setMTOMEnabled(boolean enable) {
            SOAPBinding binding = (SOAPBinding) ((BindingProvider) _proxy).getBinding();
            binding.setMTOMEnabled(enable);
        }
    }

    public InterfacciaOPSICOGE_SOAPPortProxy() {
        _descriptor = new Descriptor();
        _descriptor.setMTOMEnabled(false);
    }

    public InterfacciaOPSICOGE_SOAPPortProxy(URL wsdlLocation, QName serviceName) {
        _descriptor = new Descriptor(wsdlLocation, serviceName);
        _descriptor.setMTOMEnabled(false);
    }

    public Descriptor _getDescriptor() {
        return _descriptor;
    }

    public RispostaListaOPType listaOP(AccessoApplicativo headerAccesso, RichiestaListaOPType parameters) throws GenericFault, SecurityFault {
        return _getDescriptor().getProxy().listaOP(headerAccesso,parameters);
    }

    public RispostaGetOPType getOP(AccessoApplicativo headerAccesso, RichiestaGetOPType parameters) throws GenericFault, SecurityFault {
        return _getDescriptor().getProxy().getOP(headerAccesso,parameters);
    }

}