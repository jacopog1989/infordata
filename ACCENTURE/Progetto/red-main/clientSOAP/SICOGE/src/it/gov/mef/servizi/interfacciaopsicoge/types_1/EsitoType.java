
package it.gov.mef.servizi.interfacciaopsicoge.types_1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for esito_type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="esito_type">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="OK"/>
 *     &lt;enumeration value="ERRORE_DATI_NON_PRESENTI"/>
 *     &lt;enumeration value="ERRORE_TROPPI_ELEMENTI_SELEZIONATI"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "esito_type")
@XmlEnum
public enum EsitoType {

    OK,
    ERRORE_DATI_NON_PRESENTI,
    ERRORE_TROPPI_ELEMENTI_SELEZIONATI;

    public String value() {
        return name();
    }

    public static EsitoType fromValue(String v) {
        return valueOf(v);
    }

}
