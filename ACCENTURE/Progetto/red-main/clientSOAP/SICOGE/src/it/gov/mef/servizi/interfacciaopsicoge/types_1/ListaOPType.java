
package it.gov.mef.servizi.interfacciaopsicoge.types_1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for listaOP_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="listaOP_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DettaglioOP" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}dettaglioOP_type" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "listaOP_type", propOrder = {
    "dettaglioOP"
})
public class ListaOPType {

    @XmlElement(name = "DettaglioOP", required = true)
    protected List<DettaglioOPType> dettaglioOP;

    /**
     * Gets the value of the dettaglioOP property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dettaglioOP property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDettaglioOP().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DettaglioOPType }
     * 
     * 
     */
    public List<DettaglioOPType> getDettaglioOP() {
        if (dettaglioOP == null) {
            dettaglioOP = new ArrayList<DettaglioOPType>();
        }
        return this.dettaglioOP;
    }

}
