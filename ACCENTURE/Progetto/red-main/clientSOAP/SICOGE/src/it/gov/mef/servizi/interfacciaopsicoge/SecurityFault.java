
package it.gov.mef.servizi.interfacciaopsicoge;

import javax.xml.ws.WebFault;

@WebFault(name = "SecurityFault", targetNamespace = "http://mef.gov.it/servizi/Common/HeaderFault")
public class SecurityFault
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private it.gov.mef.servizi.common.headerfault.SecurityFault faultInfo;

    /**
     * 
     * @param faultInfo
     * @param message
     */
    public SecurityFault(String message, it.gov.mef.servizi.common.headerfault.SecurityFault faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param faultInfo
     * @param cause
     * @param message
     */
    public SecurityFault(String message, it.gov.mef.servizi.common.headerfault.SecurityFault faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: it.gov.mef.servizi.common.headerfault.SecurityFault
     */
    public it.gov.mef.servizi.common.headerfault.SecurityFault getFaultInfo() {
        return faultInfo;
    }

}
