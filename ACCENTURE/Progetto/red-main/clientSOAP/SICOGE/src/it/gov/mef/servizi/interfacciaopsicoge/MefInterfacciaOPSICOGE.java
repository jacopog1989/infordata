
package it.gov.mef.servizi.interfacciaopsicoge;

import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;

@WebServiceClient(name = "mef_InterfacciaOPSICOGE", targetNamespace = "http://mef.gov.it/servizi/InterfacciaOPSICOGE", wsdlLocation = "META-INF/wsdl/mef_InterfacciaOPSICOGE_2_1.wsdl")
public class MefInterfacciaOPSICOGE
    extends Service
{

    private final static URL MEFINTERFACCIAOPSICOGE_WSDL_LOCATION;
    private final static WebServiceException MEFINTERFACCIAOPSICOGE_EXCEPTION;
    private final static QName MEFINTERFACCIAOPSICOGE_QNAME = new QName("http://mef.gov.it/servizi/InterfacciaOPSICOGE", "mef_InterfacciaOPSICOGE");

    static {
            MEFINTERFACCIAOPSICOGE_WSDL_LOCATION = it.gov.mef.servizi.interfacciaopsicoge.MefInterfacciaOPSICOGE.class.getClassLoader().getResource("wsdl/mef_InterfacciaOPSICOGE_2_1.wsdl");
        WebServiceException e = null;
        if (MEFINTERFACCIAOPSICOGE_WSDL_LOCATION == null) {
            e = new WebServiceException("Cannot find 'META-INF/wsdl/mef_InterfacciaOPSICOGE_2_1.wsdl' wsdl. Place the resource correctly in the classpath.");
        }
        MEFINTERFACCIAOPSICOGE_EXCEPTION = e;
    }

    public MefInterfacciaOPSICOGE() {
        super(__getWsdlLocation(), MEFINTERFACCIAOPSICOGE_QNAME);
    }

    public MefInterfacciaOPSICOGE(WebServiceFeature... features) {
        super(__getWsdlLocation(), MEFINTERFACCIAOPSICOGE_QNAME, features);
    }

    public MefInterfacciaOPSICOGE(URL wsdlLocation) {
        super(wsdlLocation, MEFINTERFACCIAOPSICOGE_QNAME);
    }

    public MefInterfacciaOPSICOGE(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, MEFINTERFACCIAOPSICOGE_QNAME, features);
    }

    public MefInterfacciaOPSICOGE(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public MefInterfacciaOPSICOGE(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns InterfacciaOPSICOGE
     */
    @WebEndpoint(name = "InterfacciaOPSICOGE_SOAPPort")
    public InterfacciaOPSICOGE getInterfacciaOPSICOGESOAPPort() {
        return super.getPort(new QName("http://mef.gov.it/servizi/InterfacciaOPSICOGE", "InterfacciaOPSICOGE_SOAPPort"), InterfacciaOPSICOGE.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns InterfacciaOPSICOGE
     */
    @WebEndpoint(name = "InterfacciaOPSICOGE_SOAPPort")
    public InterfacciaOPSICOGE getInterfacciaOPSICOGESOAPPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://mef.gov.it/servizi/InterfacciaOPSICOGE", "InterfacciaOPSICOGE_SOAPPort"), InterfacciaOPSICOGE.class, features);
    }

    private static URL __getWsdlLocation() {
        if (MEFINTERFACCIAOPSICOGE_EXCEPTION!= null) {
            throw MEFINTERFACCIAOPSICOGE_EXCEPTION;
        }
        return MEFINTERFACCIAOPSICOGE_WSDL_LOCATION;
    }

}
