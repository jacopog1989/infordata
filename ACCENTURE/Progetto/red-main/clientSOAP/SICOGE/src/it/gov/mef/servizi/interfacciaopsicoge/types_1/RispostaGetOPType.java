
package it.gov.mef.servizi.interfacciaopsicoge.types_1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Java class for risposta_getOP_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="risposta_getOP_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Esito" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}esito_type"/>
 *         &lt;element name="DocumentoOP" minOccurs="0">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>base64Binary">
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="InfoAggiuntiveXML" minOccurs="0">
 *           &lt;complexType>
 *             &lt;simpleContent>
 *               &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>base64Binary">
 *                 &lt;attribute name="Formato" use="required" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}formato_type" />
 *               &lt;/extension>
 *             &lt;/simpleContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_getOP_type", propOrder = {
    "esito",
    "documentoOP",
    "infoAggiuntiveXML"
})
public class RispostaGetOPType {

    @XmlElement(name = "Esito", required = true)
    @XmlSchemaType(name = "string")
    protected EsitoType esito;
    @XmlElementRef(name = "DocumentoOP", type = JAXBElement.class, required = false)
    protected JAXBElement<RispostaGetOPType.DocumentoOP> documentoOP;
    @XmlElementRef(name = "InfoAggiuntiveXML", type = JAXBElement.class, required = false)
    protected JAXBElement<RispostaGetOPType.InfoAggiuntiveXML> infoAggiuntiveXML;

    /**
     * Gets the value of the esito property.
     * 
     * @return
     *     possible object is
     *     {@link EsitoType }
     *     
     */
    public EsitoType getEsito() {
        return esito;
    }

    /**
     * Sets the value of the esito property.
     * 
     * @param value
     *     allowed object is
     *     {@link EsitoType }
     *     
     */
    public void setEsito(EsitoType value) {
        this.esito = value;
    }

    /**
     * Gets the value of the documentoOP property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link RispostaGetOPType.DocumentoOP }{@code >}
     *     
     */
    public JAXBElement<RispostaGetOPType.DocumentoOP> getDocumentoOP() {
        return documentoOP;
    }

    /**
     * Sets the value of the documentoOP property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link RispostaGetOPType.DocumentoOP }{@code >}
     *     
     */
    public void setDocumentoOP(JAXBElement<RispostaGetOPType.DocumentoOP> value) {
        this.documentoOP = value;
    }

    /**
     * Gets the value of the infoAggiuntiveXML property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link RispostaGetOPType.InfoAggiuntiveXML }{@code >}
     *     
     */
    public JAXBElement<RispostaGetOPType.InfoAggiuntiveXML> getInfoAggiuntiveXML() {
        return infoAggiuntiveXML;
    }

    /**
     * Sets the value of the infoAggiuntiveXML property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link RispostaGetOPType.InfoAggiuntiveXML }{@code >}
     *     
     */
    public void setInfoAggiuntiveXML(JAXBElement<RispostaGetOPType.InfoAggiuntiveXML> value) {
        this.infoAggiuntiveXML = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>base64Binary">
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class DocumentoOP {

        @XmlValue
        protected byte[] value;

        /**
         * Gets the value of the value property.
         * 
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setValue(byte[] value) {
            this.value = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;simpleContent>
     *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>base64Binary">
     *       &lt;attribute name="Formato" use="required" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}formato_type" />
     *     &lt;/extension>
     *   &lt;/simpleContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "value"
    })
    public static class InfoAggiuntiveXML {

        @XmlValue
        protected byte[] value;
        @XmlAttribute(name = "Formato", required = true)
        protected String formato;

        /**
         * Gets the value of the value property.
         * 
         * @return
         *     possible object is
         *     byte[]
         */
        public byte[] getValue() {
            return value;
        }

        /**
         * Sets the value of the value property.
         * 
         * @param value
         *     allowed object is
         *     byte[]
         */
        public void setValue(byte[] value) {
            this.value = value;
        }

        /**
         * Gets the value of the formato property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFormato() {
            return formato;
        }

        /**
         * Sets the value of the formato property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFormato(String value) {
            this.formato = value;
        }

    }

}
