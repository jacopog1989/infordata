
package it.gov.mef.servizi.interfacciaopsicoge;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import it.gov.mef.servizi.common.headeraccessoapplicativo.AccessoApplicativo;
import it.gov.mef.servizi.interfacciaopsicoge.types_1.RichiestaGetOPType;
import it.gov.mef.servizi.interfacciaopsicoge.types_1.RichiestaListaOPType;
import it.gov.mef.servizi.interfacciaopsicoge.types_1.RispostaGetOPType;
import it.gov.mef.servizi.interfacciaopsicoge.types_1.RispostaListaOPType;

@WebService(name = "InterfacciaOPSICOGE", targetNamespace = "http://mef.gov.it/servizi/InterfacciaOPSICOGE")
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
@XmlSeeAlso({
    it.gov.mef.servizi.interfacciaopsicoge.types_1.ObjectFactory.class,
    it.gov.mef.servizi.common.headeraccessoapplicativo.ObjectFactory.class,
    it.gov.mef.servizi.common.headerfault.ObjectFactory.class,
    org.xmlsoap.schemas.soap.envelope.ObjectFactory.class
})
public interface InterfacciaOPSICOGE {


    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns it.gov.mef.servizi.interfacciaopsicoge.types_1.RispostaListaOPType
     * @throws GenericFault
     * @throws SecurityFault
     */
    @WebMethod(action = "listaOP")
    @WebResult(name = "risposta_RichiestaRispostaSincrona_listaOP", targetNamespace = "http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1", partName = "parameters")
    public RispostaListaOPType listaOP(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_RichiestaRispostaSincrona_listaOP", targetNamespace = "http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1", partName = "parameters")
        RichiestaListaOPType parameters)
        throws GenericFault, SecurityFault
    ;

    /**
     * 
     * @param headerAccesso
     * @param parameters
     * @return
     *     returns it.gov.mef.servizi.interfacciaopsicoge.types_1.RispostaGetOPType
     * @throws GenericFault
     * @throws SecurityFault
     */
    @WebMethod(action = "getOP")
    @WebResult(name = "risposta_RichiestaRispostaSincrona_getOP", targetNamespace = "http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1", partName = "parameters")
    public RispostaGetOPType getOP(
        @WebParam(name = "AccessoApplicativo", targetNamespace = "http://mef.gov.it/servizi/Common/HeaderAccessoApplicativo", header = true, partName = "headerAccesso")
        AccessoApplicativo headerAccesso,
        @WebParam(name = "richiesta_RichiestaRispostaSincrona_getOP", targetNamespace = "http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1", partName = "parameters")
        RichiestaGetOPType parameters)
        throws GenericFault, SecurityFault
    ;

}
