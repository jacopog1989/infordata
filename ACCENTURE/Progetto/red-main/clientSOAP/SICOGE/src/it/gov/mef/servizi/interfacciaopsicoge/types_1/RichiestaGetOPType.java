
package it.gov.mef.servizi.interfacciaopsicoge.types_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for richiesta_getOP_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_getOP_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdDocumentoSICOGE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_getOP_type", propOrder = {
    "idDocumentoSICOGE"
})
public class RichiestaGetOPType {

    @XmlElement(name = "IdDocumentoSICOGE", required = true)
    protected String idDocumentoSICOGE;

    /**
     * Gets the value of the idDocumentoSICOGE property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoSICOGE() {
        return idDocumentoSICOGE;
    }

    /**
     * Sets the value of the idDocumentoSICOGE property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoSICOGE(String value) {
        this.idDocumentoSICOGE = value;
    }

}
