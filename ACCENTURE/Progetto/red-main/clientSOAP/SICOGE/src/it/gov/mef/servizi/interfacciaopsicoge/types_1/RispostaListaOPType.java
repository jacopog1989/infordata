
package it.gov.mef.servizi.interfacciaopsicoge.types_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Risponde con la lista dei documenti contabile
 * 
 * <p>Java class for risposta_listaOP_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="risposta_listaOP_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Esito" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}esito_type"/>
 *         &lt;element name="ListaOP" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}listaOP_type" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "risposta_listaOP_type", propOrder = {
    "esito",
    "listaOP"
})
public class RispostaListaOPType {

    @XmlElement(name = "Esito", required = true)
    @XmlSchemaType(name = "string")
    protected EsitoType esito;
    @XmlElement(name = "ListaOP")
    protected ListaOPType listaOP;

    /**
     * Gets the value of the esito property.
     * 
     * @return
     *     possible object is
     *     {@link EsitoType }
     *     
     */
    public EsitoType getEsito() {
        return esito;
    }

    /**
     * Sets the value of the esito property.
     * 
     * @param value
     *     allowed object is
     *     {@link EsitoType }
     *     
     */
    public void setEsito(EsitoType value) {
        this.esito = value;
    }

    /**
     * Gets the value of the listaOP property.
     * 
     * @return
     *     possible object is
     *     {@link ListaOPType }
     *     
     */
    public ListaOPType getListaOP() {
        return listaOP;
    }

    /**
     * Sets the value of the listaOP property.
     * 
     * @param value
     *     allowed object is
     *     {@link ListaOPType }
     *     
     */
    public void setListaOP(ListaOPType value) {
        this.listaOP = value;
    }

}
