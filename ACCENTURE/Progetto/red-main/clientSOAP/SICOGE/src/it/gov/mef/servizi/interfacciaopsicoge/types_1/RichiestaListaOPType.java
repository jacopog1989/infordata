
package it.gov.mef.servizi.interfacciaopsicoge.types_1;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Richiede la lista dei OP presenti in SICOGE
 * 
 * <p>Java class for richiesta_listaOP_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="richiesta_listaOP_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Esercizio" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}esercizio_type"/>
 *         &lt;element name="Amministrazione" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}amministrazione_type"/>
 *         &lt;element name="Ragioneria" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}ragioneria_type"/>
 *         &lt;element name="Capitolo">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="Capitolo_da" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}capitolo_type"/>
 *                   &lt;element name="Capitolo_a" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}capitolo_type"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="PG" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="PG_da" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}pg_type"/>
 *                   &lt;element name="PG_a" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}pg_type"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="NumeroOP">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="NumeroOP_da" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}numeroOP_type"/>
 *                   &lt;element name="NumeroOP_a" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}numeroOP_type"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="TipoOP" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="TipoOP_da" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}tipoOP_type"/>
 *                   &lt;element name="TipoOP_a" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}tipoOP_type"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="Beneficiario" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}beneficiario_type" minOccurs="0"/>
 *         &lt;element name="OggettoSpese" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}oggettoSpesa_type" minOccurs="0"/>
 *         &lt;element name="StatoDocumentaleTitolo" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}statoDocumentaleTitolo_type" minOccurs="0"/>
 *         &lt;element name="DataEmissioneOP" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="DataEmissioneOP_da" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                   &lt;element name="DataEmissioneOP_a" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "richiesta_listaOP_type", propOrder = {
    "esercizio",
    "amministrazione",
    "ragioneria",
    "capitolo",
    "pg",
    "numeroOP",
    "tipoOP",
    "beneficiario",
    "oggettoSpese",
    "statoDocumentaleTitolo",
    "dataEmissioneOP"
})
public class RichiestaListaOPType {

    @XmlElement(name = "Esercizio", required = true)
    protected BigInteger esercizio;
    @XmlElement(name = "Amministrazione", required = true)
    protected String amministrazione;
    @XmlElement(name = "Ragioneria", required = true)
    protected String ragioneria;
    @XmlElement(name = "Capitolo", required = true)
    protected RichiestaListaOPType.Capitolo capitolo;
    @XmlElement(name = "PG")
    protected RichiestaListaOPType.PG pg;
    @XmlElement(name = "NumeroOP", required = true)
    protected RichiestaListaOPType.NumeroOP numeroOP;
    @XmlElement(name = "TipoOP")
    protected RichiestaListaOPType.TipoOP tipoOP;
    @XmlElement(name = "Beneficiario")
    protected BeneficiarioType beneficiario;
    @XmlElement(name = "OggettoSpese")
    protected String oggettoSpese;
    @XmlElement(name = "StatoDocumentaleTitolo")
    protected String statoDocumentaleTitolo;
    @XmlElement(name = "DataEmissioneOP")
    protected RichiestaListaOPType.DataEmissioneOP dataEmissioneOP;

    /**
     * Gets the value of the esercizio property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getEsercizio() {
        return esercizio;
    }

    /**
     * Sets the value of the esercizio property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setEsercizio(BigInteger value) {
        this.esercizio = value;
    }

    /**
     * Gets the value of the amministrazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmministrazione() {
        return amministrazione;
    }

    /**
     * Sets the value of the amministrazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmministrazione(String value) {
        this.amministrazione = value;
    }

    /**
     * Gets the value of the ragioneria property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRagioneria() {
        return ragioneria;
    }

    /**
     * Sets the value of the ragioneria property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRagioneria(String value) {
        this.ragioneria = value;
    }

    /**
     * Gets the value of the capitolo property.
     * 
     * @return
     *     possible object is
     *     {@link RichiestaListaOPType.Capitolo }
     *     
     */
    public RichiestaListaOPType.Capitolo getCapitolo() {
        return capitolo;
    }

    /**
     * Sets the value of the capitolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RichiestaListaOPType.Capitolo }
     *     
     */
    public void setCapitolo(RichiestaListaOPType.Capitolo value) {
        this.capitolo = value;
    }

    /**
     * Gets the value of the pg property.
     * 
     * @return
     *     possible object is
     *     {@link RichiestaListaOPType.PG }
     *     
     */
    public RichiestaListaOPType.PG getPG() {
        return pg;
    }

    /**
     * Sets the value of the pg property.
     * 
     * @param value
     *     allowed object is
     *     {@link RichiestaListaOPType.PG }
     *     
     */
    public void setPG(RichiestaListaOPType.PG value) {
        this.pg = value;
    }

    /**
     * Gets the value of the numeroOP property.
     * 
     * @return
     *     possible object is
     *     {@link RichiestaListaOPType.NumeroOP }
     *     
     */
    public RichiestaListaOPType.NumeroOP getNumeroOP() {
        return numeroOP;
    }

    /**
     * Sets the value of the numeroOP property.
     * 
     * @param value
     *     allowed object is
     *     {@link RichiestaListaOPType.NumeroOP }
     *     
     */
    public void setNumeroOP(RichiestaListaOPType.NumeroOP value) {
        this.numeroOP = value;
    }

    /**
     * Gets the value of the tipoOP property.
     * 
     * @return
     *     possible object is
     *     {@link RichiestaListaOPType.TipoOP }
     *     
     */
    public RichiestaListaOPType.TipoOP getTipoOP() {
        return tipoOP;
    }

    /**
     * Sets the value of the tipoOP property.
     * 
     * @param value
     *     allowed object is
     *     {@link RichiestaListaOPType.TipoOP }
     *     
     */
    public void setTipoOP(RichiestaListaOPType.TipoOP value) {
        this.tipoOP = value;
    }

    /**
     * Gets the value of the beneficiario property.
     * 
     * @return
     *     possible object is
     *     {@link BeneficiarioType }
     *     
     */
    public BeneficiarioType getBeneficiario() {
        return beneficiario;
    }

    /**
     * Sets the value of the beneficiario property.
     * 
     * @param value
     *     allowed object is
     *     {@link BeneficiarioType }
     *     
     */
    public void setBeneficiario(BeneficiarioType value) {
        this.beneficiario = value;
    }

    /**
     * Gets the value of the oggettoSpese property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggettoSpese() {
        return oggettoSpese;
    }

    /**
     * Sets the value of the oggettoSpese property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggettoSpese(String value) {
        this.oggettoSpese = value;
    }

    /**
     * Gets the value of the statoDocumentaleTitolo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatoDocumentaleTitolo() {
        return statoDocumentaleTitolo;
    }

    /**
     * Sets the value of the statoDocumentaleTitolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatoDocumentaleTitolo(String value) {
        this.statoDocumentaleTitolo = value;
    }

    /**
     * Gets the value of the dataEmissioneOP property.
     * 
     * @return
     *     possible object is
     *     {@link RichiestaListaOPType.DataEmissioneOP }
     *     
     */
    public RichiestaListaOPType.DataEmissioneOP getDataEmissioneOP() {
        return dataEmissioneOP;
    }

    /**
     * Sets the value of the dataEmissioneOP property.
     * 
     * @param value
     *     allowed object is
     *     {@link RichiestaListaOPType.DataEmissioneOP }
     *     
     */
    public void setDataEmissioneOP(RichiestaListaOPType.DataEmissioneOP value) {
        this.dataEmissioneOP = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Capitolo_da" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}capitolo_type"/>
     *         &lt;element name="Capitolo_a" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}capitolo_type"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "capitoloDa",
        "capitoloA"
    })
    public static class Capitolo {

        @XmlElement(name = "Capitolo_da", required = true)
        protected String capitoloDa;
        @XmlElement(name = "Capitolo_a", required = true)
        protected String capitoloA;

        /**
         * Gets the value of the capitoloDa property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCapitoloDa() {
            return capitoloDa;
        }

        /**
         * Sets the value of the capitoloDa property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCapitoloDa(String value) {
            this.capitoloDa = value;
        }

        /**
         * Gets the value of the capitoloA property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCapitoloA() {
            return capitoloA;
        }

        /**
         * Sets the value of the capitoloA property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCapitoloA(String value) {
            this.capitoloA = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DataEmissioneOP_da" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *         &lt;element name="DataEmissioneOP_a" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dataEmissioneOPDa",
        "dataEmissioneOPA"
    })
    public static class DataEmissioneOP {

        @XmlElement(name = "DataEmissioneOP_da", required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar dataEmissioneOPDa;
        @XmlElement(name = "DataEmissioneOP_a", required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar dataEmissioneOPA;

        /**
         * Gets the value of the dataEmissioneOPDa property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDataEmissioneOPDa() {
            return dataEmissioneOPDa;
        }

        /**
         * Sets the value of the dataEmissioneOPDa property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDataEmissioneOPDa(XMLGregorianCalendar value) {
            this.dataEmissioneOPDa = value;
        }

        /**
         * Gets the value of the dataEmissioneOPA property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDataEmissioneOPA() {
            return dataEmissioneOPA;
        }

        /**
         * Sets the value of the dataEmissioneOPA property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDataEmissioneOPA(XMLGregorianCalendar value) {
            this.dataEmissioneOPA = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="NumeroOP_da" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}numeroOP_type"/>
     *         &lt;element name="NumeroOP_a" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}numeroOP_type"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "numeroOPDa",
        "numeroOPA"
    })
    public static class NumeroOP {

        @XmlElement(name = "NumeroOP_da", required = true)
        protected String numeroOPDa;
        @XmlElement(name = "NumeroOP_a", required = true)
        protected String numeroOPA;

        /**
         * Gets the value of the numeroOPDa property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNumeroOPDa() {
            return numeroOPDa;
        }

        /**
         * Sets the value of the numeroOPDa property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNumeroOPDa(String value) {
            this.numeroOPDa = value;
        }

        /**
         * Gets the value of the numeroOPA property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNumeroOPA() {
            return numeroOPA;
        }

        /**
         * Sets the value of the numeroOPA property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNumeroOPA(String value) {
            this.numeroOPA = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="PG_da" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}pg_type"/>
     *         &lt;element name="PG_a" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}pg_type"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "pgDa",
        "pga"
    })
    public static class PG {

        @XmlElement(name = "PG_da", required = true)
        protected String pgDa;
        @XmlElement(name = "PG_a", required = true)
        protected String pga;

        /**
         * Gets the value of the pgDa property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPGDa() {
            return pgDa;
        }

        /**
         * Sets the value of the pgDa property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPGDa(String value) {
            this.pgDa = value;
        }

        /**
         * Gets the value of the pga property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPGA() {
            return pga;
        }

        /**
         * Sets the value of the pga property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPGA(String value) {
            this.pga = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TipoOP_da" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}tipoOP_type"/>
     *         &lt;element name="TipoOP_a" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}tipoOP_type"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "tipoOPDa",
        "tipoOPA"
    })
    public static class TipoOP {

        @XmlElement(name = "TipoOP_da", required = true)
        protected String tipoOPDa;
        @XmlElement(name = "TipoOP_a", required = true)
        protected String tipoOPA;

        /**
         * Gets the value of the tipoOPDa property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTipoOPDa() {
            return tipoOPDa;
        }

        /**
         * Sets the value of the tipoOPDa property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTipoOPDa(String value) {
            this.tipoOPDa = value;
        }

        /**
         * Gets the value of the tipoOPA property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTipoOPA() {
            return tipoOPA;
        }

        /**
         * Sets the value of the tipoOPA property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTipoOPA(String value) {
            this.tipoOPA = value;
        }

    }

}
