
package it.gov.mef.servizi.interfacciaopsicoge;

import javax.xml.ws.WebFault;

@WebFault(name = "GenericFault", targetNamespace = "http://mef.gov.it/servizi/Common/HeaderFault")
public class GenericFault
    extends Exception
{

    /**
     * Java type that goes as soapenv:Fault detail element.
     * 
     */
    private it.gov.mef.servizi.common.headerfault.GenericFault faultInfo;

    /**
     * 
     * @param faultInfo
     * @param message
     */
    public GenericFault(String message, it.gov.mef.servizi.common.headerfault.GenericFault faultInfo) {
        super(message);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @param faultInfo
     * @param cause
     * @param message
     */
    public GenericFault(String message, it.gov.mef.servizi.common.headerfault.GenericFault faultInfo, Throwable cause) {
        super(message, cause);
        this.faultInfo = faultInfo;
    }

    /**
     * 
     * @return
     *     returns fault bean: it.gov.mef.servizi.common.headerfault.GenericFault
     */
    public it.gov.mef.servizi.common.headerfault.GenericFault getFaultInfo() {
        return faultInfo;
    }

}
