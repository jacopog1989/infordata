
package it.gov.mef.servizi.interfacciaopsicoge.types_1;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for dettaglioOP_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="dettaglioOP_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IdFascicolo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IdDocumentoSICOGE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DataEmissioneOP" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="Esercizio" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}esercizio_type"/>
 *         &lt;element name="Amministrazione" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}amministrazione_type"/>
 *         &lt;element name="Ragioneria" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}ragioneria_type"/>
 *         &lt;element name="Capitolo" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}capitolo_type"/>
 *         &lt;element name="PG" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}pg_type"/>
 *         &lt;element name="NumeroOP" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}numeroOP_type"/>
 *         &lt;element name="Beneficiario" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}beneficiarioResponse_type" minOccurs="0"/>
 *         &lt;element name="OggettoSpese" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}oggettoSpesa_type"/>
 *         &lt;element name="TipoTitolo" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}tipoTitolo_type"/>
 *         &lt;element name="StatoDocumentaleTitolo" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}statoDocumentaleTitolo_type"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dettaglioOP_type", propOrder = {
    "idFascicolo",
    "idDocumentoSICOGE",
    "dataEmissioneOP",
    "esercizio",
    "amministrazione",
    "ragioneria",
    "capitolo",
    "pg",
    "numeroOP",
    "beneficiario",
    "oggettoSpese",
    "tipoTitolo",
    "statoDocumentaleTitolo"
})
public class DettaglioOPType {

    @XmlElement(name = "IdFascicolo", required = true)
    protected String idFascicolo;
    @XmlElement(name = "IdDocumentoSICOGE")
    protected String idDocumentoSICOGE;
    @XmlElement(name = "DataEmissioneOP", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataEmissioneOP;
    @XmlElement(name = "Esercizio", required = true)
    protected BigInteger esercizio;
    @XmlElement(name = "Amministrazione", required = true)
    protected String amministrazione;
    @XmlElement(name = "Ragioneria", required = true)
    protected String ragioneria;
    @XmlElement(name = "Capitolo", required = true)
    protected String capitolo;
    @XmlElement(name = "PG", required = true)
    protected String pg;
    @XmlElement(name = "NumeroOP", required = true)
    protected String numeroOP;
    @XmlElement(name = "Beneficiario")
    protected BeneficiarioResponseType beneficiario;
    @XmlElement(name = "OggettoSpese", required = true)
    protected String oggettoSpese;
    @XmlElement(name = "TipoTitolo", required = true)
    protected String tipoTitolo;
    @XmlElement(name = "StatoDocumentaleTitolo", required = true)
    protected String statoDocumentaleTitolo;

    /**
     * Gets the value of the idFascicolo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdFascicolo() {
        return idFascicolo;
    }

    /**
     * Sets the value of the idFascicolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdFascicolo(String value) {
        this.idFascicolo = value;
    }

    /**
     * Gets the value of the idDocumentoSICOGE property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumentoSICOGE() {
        return idDocumentoSICOGE;
    }

    /**
     * Sets the value of the idDocumentoSICOGE property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumentoSICOGE(String value) {
        this.idDocumentoSICOGE = value;
    }

    /**
     * Gets the value of the dataEmissioneOP property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDataEmissioneOP() {
        return dataEmissioneOP;
    }

    /**
     * Sets the value of the dataEmissioneOP property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDataEmissioneOP(XMLGregorianCalendar value) {
        this.dataEmissioneOP = value;
    }

    /**
     * Gets the value of the esercizio property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getEsercizio() {
        return esercizio;
    }

    /**
     * Sets the value of the esercizio property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setEsercizio(BigInteger value) {
        this.esercizio = value;
    }

    /**
     * Gets the value of the amministrazione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmministrazione() {
        return amministrazione;
    }

    /**
     * Sets the value of the amministrazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmministrazione(String value) {
        this.amministrazione = value;
    }

    /**
     * Gets the value of the ragioneria property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRagioneria() {
        return ragioneria;
    }

    /**
     * Sets the value of the ragioneria property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRagioneria(String value) {
        this.ragioneria = value;
    }

    /**
     * Gets the value of the capitolo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCapitolo() {
        return capitolo;
    }

    /**
     * Sets the value of the capitolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCapitolo(String value) {
        this.capitolo = value;
    }

    /**
     * Gets the value of the pg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPG() {
        return pg;
    }

    /**
     * Sets the value of the pg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPG(String value) {
        this.pg = value;
    }

    /**
     * Gets the value of the numeroOP property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumeroOP() {
        return numeroOP;
    }

    /**
     * Sets the value of the numeroOP property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumeroOP(String value) {
        this.numeroOP = value;
    }

    /**
     * Gets the value of the beneficiario property.
     * 
     * @return
     *     possible object is
     *     {@link BeneficiarioResponseType }
     *     
     */
    public BeneficiarioResponseType getBeneficiario() {
        return beneficiario;
    }

    /**
     * Sets the value of the beneficiario property.
     * 
     * @param value
     *     allowed object is
     *     {@link BeneficiarioResponseType }
     *     
     */
    public void setBeneficiario(BeneficiarioResponseType value) {
        this.beneficiario = value;
    }

    /**
     * Gets the value of the oggettoSpese property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOggettoSpese() {
        return oggettoSpese;
    }

    /**
     * Sets the value of the oggettoSpese property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOggettoSpese(String value) {
        this.oggettoSpese = value;
    }

    /**
     * Gets the value of the tipoTitolo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoTitolo() {
        return tipoTitolo;
    }

    /**
     * Sets the value of the tipoTitolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoTitolo(String value) {
        this.tipoTitolo = value;
    }

    /**
     * Gets the value of the statoDocumentaleTitolo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatoDocumentaleTitolo() {
        return statoDocumentaleTitolo;
    }

    /**
     * Sets the value of the statoDocumentaleTitolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatoDocumentaleTitolo(String value) {
        this.statoDocumentaleTitolo = value;
    }

}
