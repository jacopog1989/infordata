
package it.gov.mef.servizi.interfacciaopsicoge.types_1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for beneficiarioResponse_type complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="beneficiarioResponse_type">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="PersonaFisica">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="CodiceFiscale" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}codiceFiscale_type" minOccurs="0"/>
 *                     &lt;element name="CodiceFiscaleBenEstero" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}codiceFiscaleBenEstero_type" minOccurs="0"/>
 *                     &lt;element name="Cognome" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}cognome_type"/>
 *                     &lt;element name="Nome" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}nome_type"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="PersonaGiuridica">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="CodiceFiscale" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}codiceFiscale_type" minOccurs="0"/>
 *                     &lt;element name="CodiceFiscaleBenEstero" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}codiceFiscaleBenEstero_type" minOccurs="0"/>
 *                     &lt;element name="PartitaIVA" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}partitaIVA_type" minOccurs="0"/>
 *                     &lt;element name="RagioneSociale" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}ragioneSociale_type"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "beneficiarioResponse_type", propOrder = {
    "personaFisica",
    "personaGiuridica"
})
public class BeneficiarioResponseType {

    @XmlElement(name = "PersonaFisica")
    protected BeneficiarioResponseType.PersonaFisica personaFisica;
    @XmlElement(name = "PersonaGiuridica")
    protected BeneficiarioResponseType.PersonaGiuridica personaGiuridica;

    /**
     * Gets the value of the personaFisica property.
     * 
     * @return
     *     possible object is
     *     {@link BeneficiarioResponseType.PersonaFisica }
     *     
     */
    public BeneficiarioResponseType.PersonaFisica getPersonaFisica() {
        return personaFisica;
    }

    /**
     * Sets the value of the personaFisica property.
     * 
     * @param value
     *     allowed object is
     *     {@link BeneficiarioResponseType.PersonaFisica }
     *     
     */
    public void setPersonaFisica(BeneficiarioResponseType.PersonaFisica value) {
        this.personaFisica = value;
    }

    /**
     * Gets the value of the personaGiuridica property.
     * 
     * @return
     *     possible object is
     *     {@link BeneficiarioResponseType.PersonaGiuridica }
     *     
     */
    public BeneficiarioResponseType.PersonaGiuridica getPersonaGiuridica() {
        return personaGiuridica;
    }

    /**
     * Sets the value of the personaGiuridica property.
     * 
     * @param value
     *     allowed object is
     *     {@link BeneficiarioResponseType.PersonaGiuridica }
     *     
     */
    public void setPersonaGiuridica(BeneficiarioResponseType.PersonaGiuridica value) {
        this.personaGiuridica = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CodiceFiscale" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}codiceFiscale_type" minOccurs="0"/>
     *         &lt;element name="CodiceFiscaleBenEstero" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}codiceFiscaleBenEstero_type" minOccurs="0"/>
     *         &lt;element name="Cognome" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}cognome_type"/>
     *         &lt;element name="Nome" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}nome_type"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "codiceFiscale",
        "codiceFiscaleBenEstero",
        "cognome",
        "nome"
    })
    public static class PersonaFisica {

        @XmlElement(name = "CodiceFiscale")
        protected String codiceFiscale;
        @XmlElement(name = "CodiceFiscaleBenEstero")
        protected String codiceFiscaleBenEstero;
        @XmlElement(name = "Cognome", required = true)
        protected String cognome;
        @XmlElement(name = "Nome", required = true)
        protected String nome;

        /**
         * Gets the value of the codiceFiscale property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodiceFiscale() {
            return codiceFiscale;
        }

        /**
         * Sets the value of the codiceFiscale property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodiceFiscale(String value) {
            this.codiceFiscale = value;
        }

        /**
         * Gets the value of the codiceFiscaleBenEstero property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodiceFiscaleBenEstero() {
            return codiceFiscaleBenEstero;
        }

        /**
         * Sets the value of the codiceFiscaleBenEstero property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodiceFiscaleBenEstero(String value) {
            this.codiceFiscaleBenEstero = value;
        }

        /**
         * Gets the value of the cognome property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCognome() {
            return cognome;
        }

        /**
         * Sets the value of the cognome property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCognome(String value) {
            this.cognome = value;
        }

        /**
         * Gets the value of the nome property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNome() {
            return nome;
        }

        /**
         * Sets the value of the nome property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNome(String value) {
            this.nome = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CodiceFiscale" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}codiceFiscale_type" minOccurs="0"/>
     *         &lt;element name="CodiceFiscaleBenEstero" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}codiceFiscaleBenEstero_type" minOccurs="0"/>
     *         &lt;element name="PartitaIVA" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}partitaIVA_type" minOccurs="0"/>
     *         &lt;element name="RagioneSociale" type="{http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1}ragioneSociale_type"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "codiceFiscale",
        "codiceFiscaleBenEstero",
        "partitaIVA",
        "ragioneSociale"
    })
    public static class PersonaGiuridica {

        @XmlElement(name = "CodiceFiscale")
        protected String codiceFiscale;
        @XmlElement(name = "CodiceFiscaleBenEstero")
        protected String codiceFiscaleBenEstero;
        @XmlElement(name = "PartitaIVA")
        protected String partitaIVA;
        @XmlElement(name = "RagioneSociale", required = true)
        protected String ragioneSociale;

        /**
         * Gets the value of the codiceFiscale property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodiceFiscale() {
            return codiceFiscale;
        }

        /**
         * Sets the value of the codiceFiscale property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodiceFiscale(String value) {
            this.codiceFiscale = value;
        }

        /**
         * Gets the value of the codiceFiscaleBenEstero property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodiceFiscaleBenEstero() {
            return codiceFiscaleBenEstero;
        }

        /**
         * Sets the value of the codiceFiscaleBenEstero property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodiceFiscaleBenEstero(String value) {
            this.codiceFiscaleBenEstero = value;
        }

        /**
         * Gets the value of the partitaIVA property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPartitaIVA() {
            return partitaIVA;
        }

        /**
         * Sets the value of the partitaIVA property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPartitaIVA(String value) {
            this.partitaIVA = value;
        }

        /**
         * Gets the value of the ragioneSociale property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRagioneSociale() {
            return ragioneSociale;
        }

        /**
         * Sets the value of the ragioneSociale property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRagioneSociale(String value) {
            this.ragioneSociale = value;
        }

    }

}
