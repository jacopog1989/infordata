
package it.gov.mef.servizi.interfacciaopsicoge.types_1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the it.gov.mef.servizi.interfacciaopsicoge.types_1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RispostaRichiestaRispostaSincronaGetOP_QNAME = new QName("http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1", "risposta_RichiestaRispostaSincrona_getOP");
    private final static QName _RichiestaRichiestaRispostaSincronaGetOP_QNAME = new QName("http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1", "richiesta_RichiestaRispostaSincrona_getOP");
    private final static QName _RichiestaRichiestaRispostaSincronaListaOP_QNAME = new QName("http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1", "richiesta_RichiestaRispostaSincrona_listaOP");
    private final static QName _RispostaRichiestaRispostaSincronaListaOP_QNAME = new QName("http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1", "risposta_RichiestaRispostaSincrona_listaOP");
    private final static QName _RispostaGetOPTypeDocumentoOP_QNAME = new QName("", "DocumentoOP");
    private final static QName _RispostaGetOPTypeInfoAggiuntiveXML_QNAME = new QName("", "InfoAggiuntiveXML");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: it.gov.mef.servizi.interfacciaopsicoge.types_1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link BeneficiarioType }
     * 
     */
    public BeneficiarioType createBeneficiarioType() {
        return new BeneficiarioType();
    }

    /**
     * Create an instance of {@link BeneficiarioResponseType }
     * 
     */
    public BeneficiarioResponseType createBeneficiarioResponseType() {
        return new BeneficiarioResponseType();
    }

    /**
     * Create an instance of {@link RichiestaListaOPType }
     * 
     */
    public RichiestaListaOPType createRichiestaListaOPType() {
        return new RichiestaListaOPType();
    }

    /**
     * Create an instance of {@link RispostaGetOPType }
     * 
     */
    public RispostaGetOPType createRispostaGetOPType() {
        return new RispostaGetOPType();
    }

    /**
     * Create an instance of {@link RichiestaGetOPType }
     * 
     */
    public RichiestaGetOPType createRichiestaGetOPType() {
        return new RichiestaGetOPType();
    }

    /**
     * Create an instance of {@link RispostaListaOPType }
     * 
     */
    public RispostaListaOPType createRispostaListaOPType() {
        return new RispostaListaOPType();
    }

    /**
     * Create an instance of {@link DettaglioOPType }
     * 
     */
    public DettaglioOPType createDettaglioOPType() {
        return new DettaglioOPType();
    }

    /**
     * Create an instance of {@link ListaOPType }
     * 
     */
    public ListaOPType createListaOPType() {
        return new ListaOPType();
    }

    /**
     * Create an instance of {@link BeneficiarioType.PersonaFisica }
     * 
     */
    public BeneficiarioType.PersonaFisica createBeneficiarioTypePersonaFisica() {
        return new BeneficiarioType.PersonaFisica();
    }

    /**
     * Create an instance of {@link BeneficiarioType.PersonaGiuridica }
     * 
     */
    public BeneficiarioType.PersonaGiuridica createBeneficiarioTypePersonaGiuridica() {
        return new BeneficiarioType.PersonaGiuridica();
    }

    /**
     * Create an instance of {@link BeneficiarioResponseType.PersonaFisica }
     * 
     */
    public BeneficiarioResponseType.PersonaFisica createBeneficiarioResponseTypePersonaFisica() {
        return new BeneficiarioResponseType.PersonaFisica();
    }

    /**
     * Create an instance of {@link BeneficiarioResponseType.PersonaGiuridica }
     * 
     */
    public BeneficiarioResponseType.PersonaGiuridica createBeneficiarioResponseTypePersonaGiuridica() {
        return new BeneficiarioResponseType.PersonaGiuridica();
    }

    /**
     * Create an instance of {@link RichiestaListaOPType.Capitolo }
     * 
     */
    public RichiestaListaOPType.Capitolo createRichiestaListaOPTypeCapitolo() {
        return new RichiestaListaOPType.Capitolo();
    }

    /**
     * Create an instance of {@link RichiestaListaOPType.PG }
     * 
     */
    public RichiestaListaOPType.PG createRichiestaListaOPTypePG() {
        return new RichiestaListaOPType.PG();
    }

    /**
     * Create an instance of {@link RichiestaListaOPType.NumeroOP }
     * 
     */
    public RichiestaListaOPType.NumeroOP createRichiestaListaOPTypeNumeroOP() {
        return new RichiestaListaOPType.NumeroOP();
    }

    /**
     * Create an instance of {@link RichiestaListaOPType.TipoOP }
     * 
     */
    public RichiestaListaOPType.TipoOP createRichiestaListaOPTypeTipoOP() {
        return new RichiestaListaOPType.TipoOP();
    }

    /**
     * Create an instance of {@link RichiestaListaOPType.DataEmissioneOP }
     * 
     */
    public RichiestaListaOPType.DataEmissioneOP createRichiestaListaOPTypeDataEmissioneOP() {
        return new RichiestaListaOPType.DataEmissioneOP();
    }

    /**
     * Create an instance of {@link RispostaGetOPType.DocumentoOP }
     * 
     */
    public RispostaGetOPType.DocumentoOP createRispostaGetOPTypeDocumentoOP() {
        return new RispostaGetOPType.DocumentoOP();
    }

    /**
     * Create an instance of {@link RispostaGetOPType.InfoAggiuntiveXML }
     * 
     */
    public RispostaGetOPType.InfoAggiuntiveXML createRispostaGetOPTypeInfoAggiuntiveXML() {
        return new RispostaGetOPType.InfoAggiuntiveXML();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaGetOPType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1", name = "risposta_RichiestaRispostaSincrona_getOP")
    public JAXBElement<RispostaGetOPType> createRispostaRichiestaRispostaSincronaGetOP(RispostaGetOPType value) {
        return new JAXBElement<RispostaGetOPType>(_RispostaRichiestaRispostaSincronaGetOP_QNAME, RispostaGetOPType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaGetOPType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1", name = "richiesta_RichiestaRispostaSincrona_getOP")
    public JAXBElement<RichiestaGetOPType> createRichiestaRichiestaRispostaSincronaGetOP(RichiestaGetOPType value) {
        return new JAXBElement<RichiestaGetOPType>(_RichiestaRichiestaRispostaSincronaGetOP_QNAME, RichiestaGetOPType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RichiestaListaOPType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1", name = "richiesta_RichiestaRispostaSincrona_listaOP")
    public JAXBElement<RichiestaListaOPType> createRichiestaRichiestaRispostaSincronaListaOP(RichiestaListaOPType value) {
        return new JAXBElement<RichiestaListaOPType>(_RichiestaRichiestaRispostaSincronaListaOP_QNAME, RichiestaListaOPType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaListaOPType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://mef.gov.it/servizi/InterfacciaOPSICOGE/types_1", name = "risposta_RichiestaRispostaSincrona_listaOP")
    public JAXBElement<RispostaListaOPType> createRispostaRichiestaRispostaSincronaListaOP(RispostaListaOPType value) {
        return new JAXBElement<RispostaListaOPType>(_RispostaRichiestaRispostaSincronaListaOP_QNAME, RispostaListaOPType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaGetOPType.DocumentoOP }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "DocumentoOP", scope = RispostaGetOPType.class)
    public JAXBElement<RispostaGetOPType.DocumentoOP> createRispostaGetOPTypeDocumentoOP(RispostaGetOPType.DocumentoOP value) {
        return new JAXBElement<RispostaGetOPType.DocumentoOP>(_RispostaGetOPTypeDocumentoOP_QNAME, RispostaGetOPType.DocumentoOP.class, RispostaGetOPType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RispostaGetOPType.InfoAggiuntiveXML }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "", name = "InfoAggiuntiveXML", scope = RispostaGetOPType.class)
    public JAXBElement<RispostaGetOPType.InfoAggiuntiveXML> createRispostaGetOPTypeInfoAggiuntiveXML(RispostaGetOPType.InfoAggiuntiveXML value) {
        return new JAXBElement<RispostaGetOPType.InfoAggiuntiveXML>(_RispostaGetOPTypeInfoAggiuntiveXML_QNAME, RispostaGetOPType.InfoAggiuntiveXML.class, RispostaGetOPType.class, value);
    }

}
