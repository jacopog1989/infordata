package it.ibm.sicoge;

import it.gov.mef.servizi.interfacciaopsicoge.types_1.EsitoType;

public enum StatoEnum {
    
    OK(EsitoType.OK),
    ERRORE_DATI_NON_PRESENTI(EsitoType.ERRORE_DATI_NON_PRESENTI),
    ERRORE_TROPPI_ELEMENTI_SELEZIONATI(EsitoType.ERRORE_TROPPI_ELEMENTI_SELEZIONATI);

	private EsitoType oldEsito;
	
	private StatoEnum(EsitoType esito) {
		oldEsito = esito;
	}
	
    public static StatoEnum fromValue(EsitoType v) {
    	StatoEnum out = null;
        for (StatoEnum c: StatoEnum.values()) {
            if (c.oldEsito.equals(v)) {
                out = c;
                break;
            }
        }
    	return out;
    }
    
}
