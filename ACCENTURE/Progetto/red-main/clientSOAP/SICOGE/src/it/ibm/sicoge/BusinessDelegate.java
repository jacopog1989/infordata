package it.ibm.sicoge;

import java.math.BigInteger;

import javax.xml.ws.BindingProvider;

import it.gov.mef.servizi.common.headeraccessoapplicativo.AccessoApplicativo;
import it.gov.mef.servizi.interfacciaopsicoge.InterfacciaOPSICOGE;
import it.gov.mef.servizi.interfacciaopsicoge.MefInterfacciaOPSICOGE;
import it.gov.mef.servizi.interfacciaopsicoge.types_1.RichiestaListaOPType;
import it.gov.mef.servizi.interfacciaopsicoge.types_1.RichiestaListaOPType.Capitolo;
import it.gov.mef.servizi.interfacciaopsicoge.types_1.RichiestaListaOPType.NumeroOP;
import it.gov.mef.servizi.interfacciaopsicoge.types_1.RispostaListaOPType;

public class BusinessDelegate {

	public static ListaOPDTO listaOP(String urlSRV, String applicazione, String utente, String password, Integer esercizio, String amministrazione, String ragioneria, String capitoloDa, String capitoloA, String numOPA, String numOPDa) {

		ListaOPDTO out = null;
		try {
			MefInterfacciaOPSICOGE proxy = new MefInterfacciaOPSICOGE();
			InterfacciaOPSICOGE srv = proxy.getInterfacciaOPSICOGESOAPPort();

			BindingProvider bp = (BindingProvider) srv;
			bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, urlSRV);

			AccessoApplicativo headerAccesso;
			
			headerAccesso = new AccessoApplicativo();
			
			headerAccesso.setApplicazione(applicazione);
			headerAccesso.setUtente(utente);
			headerAccesso.setPassword(password);
//			headerAccesso.setOrgID(orgID);
//			headerAccesso.setTipoUtente(tipoUtente);
//			headerAccesso.setClient(client);
//			headerAccesso.setServizio(servizio);
			
			RichiestaListaOPType parameters = new RichiestaListaOPType();
			if (esercizio != null) {
				parameters.setEsercizio(BigInteger.valueOf(esercizio));
			}
			parameters.setAmministrazione(amministrazione);
			parameters.setRagioneria(ragioneria);
			Capitolo capitolo = new Capitolo();
			capitolo.setCapitoloDa(capitoloDa);
			capitolo.setCapitoloA(capitoloA);
			parameters.setCapitolo(capitolo);
			NumeroOP nop = new NumeroOP();
			nop.setNumeroOPA(numOPA);
			nop.setNumeroOPDa(numOPDa);
			parameters.setNumeroOP(nop);
			
			RispostaListaOPType esito = srv.listaOP(headerAccesso, parameters);
			out = new ListaOPDTO(esito);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return out;
	}
//	
//	public static void main(String[] args) {
//
//		String urlSRV = "http://fe-ws-collaudo.tesoro.it/sife-webservices-integrazione-amministrazioni-v2/services/mef_InterfacciaOPSICOGE";
//
//		String applicazione = "RED";
//		String utente = "REDUSER";
//		String password = "RED";
//		String orgID = null;
//		String tipoUtente = null;
//		String client = null;
//		String servizio = null;
//
//		Integer esercizio = 2019;
//		String ragioneria = "0620";
//		String amministrazione = "130";
//		String capitoloDa = "1401";
//		String capitoloA = "1401";
//		String numOPDa = "0000084";
//		String numOPA = "0000084";
//
//		BusinessDelegate.listaOP(urlSRV, applicazione, utente, password, orgID, tipoUtente, client, servizio, esercizio, amministrazione, ragioneria, capitoloDa, capitoloA, numOPA, numOPDa);
//	}
	
}
