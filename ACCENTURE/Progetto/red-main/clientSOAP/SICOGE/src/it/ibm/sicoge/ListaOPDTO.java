package it.ibm.sicoge;

import java.util.List;

import it.gov.mef.servizi.interfacciaopsicoge.types_1.RispostaListaOPType;

public class ListaOPDTO {
	
	private List<DettaglioOPDTO> dettagliOP;
    private StatoEnum stato;

	public ListaOPDTO(RispostaListaOPType response) {
		stato = StatoEnum.fromValue(response.getEsito());
		dettagliOP = DettaglioOPDTO.transform(response.getListaOP());
    }

	public List<DettaglioOPDTO> getDettagliOP() {
		return dettagliOP;
	}

	public StatoEnum getStato() {
		return stato;
	}

}
