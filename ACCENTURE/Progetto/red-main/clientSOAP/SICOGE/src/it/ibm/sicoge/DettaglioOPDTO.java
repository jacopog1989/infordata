package it.ibm.sicoge;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import it.gov.mef.servizi.interfacciaopsicoge.types_1.BeneficiarioResponseType;
import it.gov.mef.servizi.interfacciaopsicoge.types_1.DettaglioOPType;
import it.gov.mef.servizi.interfacciaopsicoge.types_1.ListaOPType;

public class DettaglioOPDTO {
	
	private String idFascicolo;
    private String idDocumentoSICOGE;
    private String amministrazione;
    private String ragioneria;
    private String capitolo;
	private String pianoGestione;
    private String numeroOP;
    private String oggettoSpese;
    private String tipoTitolo;
    private String statoDocumentaleTitolo;
    private String beneficiario;
    private Integer esercizio;
    private Date dataEmissione;

    static public List<DettaglioOPDTO> transform(ListaOPType result) {
    	List<DettaglioOPDTO> out = new ArrayList<DettaglioOPDTO>();
    	if (result!=null && result.getDettaglioOP() != null) {
        	for (DettaglioOPType e:result.getDettaglioOP()) {
        		out.add(new DettaglioOPDTO(e));
        	}
    	}
    	return out;
    }
    
    public DettaglioOPDTO(DettaglioOPType esito) {
		super();
		
	    amministrazione = esito.getAmministrazione();
		capitolo = esito.getCapitolo();
		idDocumentoSICOGE = esito.getIdDocumentoSICOGE();
		idFascicolo = esito.getIdFascicolo();
		numeroOP = esito.getNumeroOP();
		oggettoSpese = esito.getOggettoSpese();
		pianoGestione = esito.getPG();
		statoDocumentaleTitolo = esito.getStatoDocumentaleTitolo();
		ragioneria = esito.getRagioneria();
		tipoTitolo = esito.getTipoTitolo();

	    beneficiario = handleBeneficiario(esito.getBeneficiario());
		dataEmissione = fromCalendarToDate(esito.getDataEmissioneOP());
		esercizio = fromBigDecimalToInteger(esito.getEsercizio());

	}

    private String handleBeneficiario(BeneficiarioResponseType beneficiario) {
    	String out = null;
    	
		if (beneficiario.getPersonaFisica() != null) {
			out = beneficiario.getPersonaFisica().getNome() + beneficiario.getPersonaFisica().getCognome();
		}
		if (beneficiario.getPersonaGiuridica() != null) {
			out = beneficiario.getPersonaGiuridica().getRagioneSociale();
		}
    	
		return out;
	}

	private Date fromCalendarToDate(XMLGregorianCalendar dataEmissioneOP) {
    	Date out = null;
    	if (dataEmissioneOP != null) {
    		out = dataEmissioneOP.toGregorianCalendar().getTime();
    	}
		return out;
	}

	private Integer fromBigDecimalToInteger(BigInteger esercizio) {
    	Integer out = null;
    	if (esercizio != null) {
    		out = esercizio.intValue();
    	}
		return out;
	}

	public String getPianoGestione() {
		return pianoGestione;
	}

	public String getNumeroOP() {
		return numeroOP;
	}
    public String getIdFascicolo() {
 		return idFascicolo;
 	}

 	public String getIdDocumentoSICOGE() {
 		return idDocumentoSICOGE;
 	}

 	public String getAmministrazione() {
 		return amministrazione;
 	}

 	public String getRagioneria() {
 		return ragioneria;
 	}

 	public String getCapitolo() {
 		return capitolo;
 	}

 	public String getOggettoSpese() {
 		return oggettoSpese;
 	}

 	public String getTipoTitolo() {
 		return tipoTitolo;
 	}

 	public String getStatoDocumentaleTitolo() {
 		return statoDocumentaleTitolo;
 	}

 	public String getBeneficiario() {
 		return beneficiario;
 	}

 	public Integer getEsercizio() {
 		return esercizio;
 	}

 	public Date getDataEmissione() {
 		return dataEmissione;
 	}
 	
}
