
package net.protmef.messages.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the net.protmef.messages.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RicercaProtocolloRegistroResponse_QNAME = new QName("urn:protmef.net:messages:v1", "_RicercaProtocolloRegistroResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: net.protmef.messages.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AggiornamentoStatoPreLexRequest }
     * 
     */
    public AggiornamentoStatoPreLexRequest createAggiornamentoStatoPreLexRequest() {
        return new AggiornamentoStatoPreLexRequest();
    }

    /**
     * Create an instance of {@link RicercaProtocolloRequest }
     * 
     */
    public RicercaProtocolloRequest createRicercaProtocolloRequest() {
        return new RicercaProtocolloRequest();
    }

    /**
     * Create an instance of {@link AnnullamentoProtocolloResponse }
     * 
     */
    public AnnullamentoProtocolloResponse createAnnullamentoProtocolloResponse() {
        return new AnnullamentoProtocolloResponse();
    }

    /**
     * Create an instance of {@link AggiornamentoScaricoRequest }
     * 
     */
    public AggiornamentoScaricoRequest createAggiornamentoScaricoRequest() {
        return new AggiornamentoScaricoRequest();
    }

    /**
     * Create an instance of {@link RiassegnazioneResponse }
     * 
     */
    public RiassegnazioneResponse createRiassegnazioneResponse() {
        return new RiassegnazioneResponse();
    }

    /**
     * Create an instance of {@link PresaInCaricoRequest }
     * 
     */
    public PresaInCaricoRequest createPresaInCaricoRequest() {
        return new PresaInCaricoRequest();
    }

    /**
     * Create an instance of {@link ProtocollazioneUscitaRequest }
     * 
     */
    public ProtocollazioneUscitaRequest createProtocollazioneUscitaRequest() {
        return new ProtocollazioneUscitaRequest();
    }

    /**
     * Create an instance of {@link RifiutoAltraApplicazioneRequest }
     * 
     */
    public RifiutoAltraApplicazioneRequest createRifiutoAltraApplicazioneRequest() {
        return new RifiutoAltraApplicazioneRequest();
    }

    /**
     * Create an instance of {@link InoltraProtocolloResponse }
     * 
     */
    public InoltraProtocolloResponse createInoltraProtocolloResponse() {
        return new InoltraProtocolloResponse();
    }

    /**
     * Create an instance of {@link RiassegnazioneRequest }
     * 
     */
    public RiassegnazioneRequest createRiassegnazioneRequest() {
        return new RiassegnazioneRequest();
    }

    /**
     * Create an instance of {@link InviaProtocolloResponse }
     * 
     */
    public InviaProtocolloResponse createInviaProtocolloResponse() {
        return new InviaProtocolloResponse();
    }

    /**
     * Create an instance of {@link TrasferimentoCompetenzaRequest }
     * 
     */
    public TrasferimentoCompetenzaRequest createTrasferimentoCompetenzaRequest() {
        return new TrasferimentoCompetenzaRequest();
    }

    /**
     * Create an instance of {@link RifiutoAltraApplicazioneResponse }
     * 
     */
    public RifiutoAltraApplicazioneResponse createRifiutoAltraApplicazioneResponse() {
        return new RifiutoAltraApplicazioneResponse();
    }

    /**
     * Create an instance of {@link PresaInCaricoResponse }
     * 
     */
    public PresaInCaricoResponse createPresaInCaricoResponse() {
        return new PresaInCaricoResponse();
    }

    /**
     * Create an instance of {@link TrasferimentoCompetenzaResponse }
     * 
     */
    public TrasferimentoCompetenzaResponse createTrasferimentoCompetenzaResponse() {
        return new TrasferimentoCompetenzaResponse();
    }

    /**
     * Create an instance of {@link AggiornamentoStatoPreLexRequest.ASSEGNATARI }
     * 
     */
    public AggiornamentoStatoPreLexRequest.ASSEGNATARI createAggiornamentoStatoPreLexRequestASSEGNATARI() {
        return new AggiornamentoStatoPreLexRequest.ASSEGNATARI();
    }

    /**
     * Create an instance of {@link ProtocollazioneUscitaResponse }
     * 
     */
    public ProtocollazioneUscitaResponse createProtocollazioneUscitaResponse() {
        return new ProtocollazioneUscitaResponse();
    }

    /**
     * Create an instance of {@link AllacciaProtocolloResponse }
     * 
     */
    public AllacciaProtocolloResponse createAllacciaProtocolloResponse() {
        return new AllacciaProtocolloResponse();
    }

    /**
     * Create an instance of {@link AnnullamentoProtocolloRequest }
     * 
     */
    public AnnullamentoProtocolloRequest createAnnullamentoProtocolloRequest() {
        return new AnnullamentoProtocolloRequest();
    }

    /**
     * Create an instance of {@link AggiornamentoStatoPreLexResponse }
     * 
     */
    public AggiornamentoStatoPreLexResponse createAggiornamentoStatoPreLexResponse() {
        return new AggiornamentoStatoPreLexResponse();
    }

    /**
     * Create an instance of {@link InoltraProtocolloRequest }
     * 
     */
    public InoltraProtocolloRequest createInoltraProtocolloRequest() {
        return new InoltraProtocolloRequest();
    }

    /**
     * Create an instance of {@link InserisciAllegatiResponse }
     * 
     */
    public InserisciAllegatiResponse createInserisciAllegatiResponse() {
        return new InserisciAllegatiResponse();
    }

    /**
     * Create an instance of {@link ProtocollazioneEntrataResponse }
     * 
     */
    public ProtocollazioneEntrataResponse createProtocollazioneEntrataResponse() {
        return new ProtocollazioneEntrataResponse();
    }

    /**
     * Create an instance of {@link ProtocollazioneEntrataRequest }
     * 
     */
    public ProtocollazioneEntrataRequest createProtocollazioneEntrataRequest() {
        return new ProtocollazioneEntrataRequest();
    }

    /**
     * Create an instance of {@link InviaProtocolloRequest }
     * 
     */
    public InviaProtocolloRequest createInviaProtocolloRequest() {
        return new InviaProtocolloRequest();
    }

    /**
     * Create an instance of {@link RicercaProtocolloRegistroRequest }
     * 
     */
    public RicercaProtocolloRegistroRequest createRicercaProtocolloRegistroRequest() {
        return new RicercaProtocolloRegistroRequest();
    }

    /**
     * Create an instance of {@link AggiornamentoScaricoResponse }
     * 
     */
    public AggiornamentoScaricoResponse createAggiornamentoScaricoResponse() {
        return new AggiornamentoScaricoResponse();
    }

    /**
     * Create an instance of {@link AllacciaProtocolloRequest }
     * 
     */
    public AllacciaProtocolloRequest createAllacciaProtocolloRequest() {
        return new AllacciaProtocolloRequest();
    }

    /**
     * Create an instance of {@link AggiornamentoSpedizioneRequest }
     * 
     */
    public AggiornamentoSpedizioneRequest createAggiornamentoSpedizioneRequest() {
        return new AggiornamentoSpedizioneRequest();
    }

    /**
     * Create an instance of {@link InserisciAllegatiRequest }
     * 
     */
    public InserisciAllegatiRequest createInserisciAllegatiRequest() {
        return new InserisciAllegatiRequest();
    }

    /**
     * Create an instance of {@link RicercaProtocolloResponse }
     * 
     */
    public RicercaProtocolloResponse createRicercaProtocolloResponse() {
        return new RicercaProtocolloResponse();
    }

    /**
     * Create an instance of {@link RicercaProtocolloRegistroResponseType }
     * 
     */
    public RicercaProtocolloRegistroResponseType createRicercaProtocolloRegistroResponseType() {
        return new RicercaProtocolloRegistroResponseType();
    }

    /**
     * Create an instance of {@link AggiornamentoSpedizioneResponse }
     * 
     */
    public AggiornamentoSpedizioneResponse createAggiornamentoSpedizioneResponse() {
        return new AggiornamentoSpedizioneResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RicercaProtocolloRegistroResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:protmef.net:messages:v1", name = "_RicercaProtocolloRegistroResponse")
    public JAXBElement<RicercaProtocolloRegistroResponseType> createRicercaProtocolloRegistroResponse(RicercaProtocolloRegistroResponseType value) {
        return new JAXBElement<RicercaProtocolloRegistroResponseType>(_RicercaProtocolloRegistroResponse_QNAME, RicercaProtocolloRegistroResponseType.class, null, value);
    }

}
