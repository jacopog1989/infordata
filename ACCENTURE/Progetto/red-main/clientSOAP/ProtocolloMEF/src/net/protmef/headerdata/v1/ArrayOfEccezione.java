
package net.protmef.headerdata.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfEccezione complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfEccezione">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Eccezione" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;attribute name="contestoCodifica" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="messaggioEccezione" type="{http://www.w3.org/2001/XMLSchema}string" />
 *                 &lt;attribute name="codiceEccezione">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;enumeration value="GENERIC_EXCEPTION"/>
 *                       &lt;enumeration value="SYSTEM_EXCEPTION"/>
 *                       &lt;enumeration value="SECURITY_CREDENZIAL_MISSING"/>
 *                       &lt;enumeration value="SECURITY_CREDENZIAL_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_ENTRATA_MISSING"/>
 *                       &lt;enumeration value="PROT_ENTRATA_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_ENTRATA_REGISTRO_MISSING"/>
 *                       &lt;enumeration value="PROT_ENTRATA_REGISTRO_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_ENTRATA_OGGETTO_MISSING"/>
 *                       &lt;enumeration value="PROT_ENTRATA_OGGETTO_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_ENTRATA_MITTENTE_MISSING"/>
 *                       &lt;enumeration value="PROT_ENTRATA_MITTENTE_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_ENTRATA_PROTOCOLLATORE_MISSING"/>
 *                       &lt;enumeration value="PROT_ENTRATA_PROTOCOLLATORE_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_ENTRATA_TIPO_DOCUMENTO_MISSING"/>
 *                       &lt;enumeration value="PROT_ENTRATA_TIPO_DOCUMENTO_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_ENTRATA_ASSEGNATARIO_COMPETENZA_MISSING"/>
 *                       &lt;enumeration value="PROT_ENTRATA_ASSEGNATARIO_COMPETENZA_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_ENTRATA_ASSEGNATARIO_CONOSCENZA_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_USCITA_MISSING"/>
 *                       &lt;enumeration value="PROT_USCITA_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_USCITA_REGISTRO_MISSING"/>
 *                       &lt;enumeration value="PROT_USCITA_REGISTRO_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_USCITA_OGGETTO_MISSING"/>
 *                       &lt;enumeration value="PROT_USCITA_OGGETTO_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_USCITA_MITTENTE_MISSING"/>
 *                       &lt;enumeration value="PROT_USCITA_MITTENTE_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_USCITA_PROTOCOLLATORE_MISSING"/>
 *                       &lt;enumeration value="PROT_USCITA_PROTOCOLLATORE_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_USCITA_TIPO_DOCUMENTO_MISSING"/>
 *                       &lt;enumeration value="PROT_USCITA_TIPO_DOCUMENTO_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_USCITA_DESTINATARIO_COMPETENZA_MISSING"/>
 *                       &lt;enumeration value="PROT_USCITA_DESTINATARIO_COMPETENZA_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_USCITA_DESTINATARIO_CONOSCENZA_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_USCITA_DESTINATARIO_DATASPEDIZIONE_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_RICERCA_MISSING"/>
 *                       &lt;enumeration value="PROT_RICERCA_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_RICERCA_NON_TROVATO"/>
 *                       &lt;enumeration value="AGGIORNAMENTO_PRELEX_MISSING"/>
 *                       &lt;enumeration value="AGGIORNAMENTO_PRELEX_NOT_VALID"/>
 *                       &lt;enumeration value="AGGIORNAMENTO_PRELEX_PROTOCOLLO_MISSING"/>
 *                       &lt;enumeration value="AGGIORNAMENTO_PRELEX_PROTOCOLLO_NOT_VALID"/>
 *                       &lt;enumeration value="AGGIORNAMENTO_PRELEX_ASSEGNATARI_MISSING"/>
 *                       &lt;enumeration value="AGGIORNAMENTO_PRELEX_ASSEGNATARI_NOT_VALID"/>
 *                       &lt;enumeration value="AGGIORNAMENTO_PRELEX_ASSEGNATARIO_COMPETENZA_NOT_VALID"/>
 *                       &lt;enumeration value="AGGIORNAMENTO_PRELEX_ASSEGNATARIO_COMPETENZA_MISSING"/>
 *                       &lt;enumeration value="AGGIORNAMENTO_SCARICO_MISSING"/>
 *                       &lt;enumeration value="AGGIORNAMENTO_SCARICO_NOT_VALID"/>
 *                       &lt;enumeration value="AGGIORNAMENTO_SCARICO_PROTOCOLLO_MISSING"/>
 *                       &lt;enumeration value="AGGIORNAMENTO_SCARICO_PROTOCOLLO_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_INSERIMENTO_DOC_MISSING"/>
 *                       &lt;enumeration value="PROT_INSERIMENTO_DOC_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_INSERIMENTO_DOC_EMPTY"/>
 *                       &lt;enumeration value="PROT_INSERIMENTO_DOC_NON_TROVATO"/>
 *                       &lt;enumeration value="PROT_INSERIMENTO_PROT_MISSING"/>
 *                       &lt;enumeration value="PROT_INSERIMENTO_PROT_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_INSERIMENTO_PROT_NON_TROVATO"/>
 *                       &lt;enumeration value="ORGANIGRAMMA_MISSING"/>
 *                       &lt;enumeration value="ORGANIGRAMMA_NOT_VALID"/>
 *                       &lt;enumeration value="ANNULLAMENTOPROTOCOLLO_MISSING"/>
 *                       &lt;enumeration value="ANNULLAMENTOPROTOCOLLO_NOT_VALID"/>
 *                       &lt;enumeration value="ANNULLAMENTOPROTOCOLLO_PROTOCOLLO_MISSING"/>
 *                       &lt;enumeration value="ANNULLAMENTOPROTOCOLLO_PROTOCOLLO_NOT_VALID"/>
 *                       &lt;enumeration value="PRESAINCARICO_MISSING"/>
 *                       &lt;enumeration value="PRESAINCARICO_NOT_VALID"/>
 *                       &lt;enumeration value="PRESAINCARICO_PROTOCOLLO_MISSING"/>
 *                       &lt;enumeration value="PRESAINCARICO_PROTOCOLLO_NOT_VALID"/>
 *                       &lt;enumeration value="PRESAINCARICO_ASSEGNATARI_MISSING"/>
 *                       &lt;enumeration value="PRESAINCARICO_ASSEGNATARI_NOT_VALID"/>
 *                       &lt;enumeration value="PRESAINCARICO_ASSEGNATARIO_COMPETENZA_NOT_VALID"/>
 *                       &lt;enumeration value="PRESAINCARICO_ASSEGNATARIO_COMPETENZA_MISSING"/>
 *                       &lt;enumeration value="RIASSEGNAZIONE_MISSING"/>
 *                       &lt;enumeration value="RIASSEGNAZIONE_NOT_VALID"/>
 *                       &lt;enumeration value="RIASSEGNAZIONE_PROTOCOLLO_MISSING"/>
 *                       &lt;enumeration value="RIASSEGNAZIONE_PROTOCOLLO_NOT_VALID"/>
 *                       &lt;enumeration value="RIASSEGNAZIONE_ASSEGNATARI_MISSING"/>
 *                       &lt;enumeration value="RIASSEGNAZIONE_ASSEGNATARI_NOT_VALID"/>
 *                       &lt;enumeration value="RIASSEGNAZIONE_ASSEGNATARIO_COMPETENZA_NOT_VALID"/>
 *                       &lt;enumeration value="RIASSEGNAZIONE_ASSEGNATARIO_COMPETENZA_MISSING"/>
 *                       &lt;enumeration value="TRASFERIMENTOCOMPETENZA_MISSING"/>
 *                       &lt;enumeration value="TRASFERIMENTOCOMPETENZA_NOT_VALID"/>
 *                       &lt;enumeration value="TRASFERIMENTOCOMPETENZA_PROTOCOLLO_MISSING"/>
 *                       &lt;enumeration value="TRASFERIMENTOCOMPETENZA_PROTOCOLLO_NOT_VALID"/>
 *                       &lt;enumeration value="TRASFERIMENTOCOMPETENZA_ASSEGNATARI_MISSING"/>
 *                       &lt;enumeration value="TRASFERIMENTOCOMPETENZA_ASSEGNATARI_NOT_VALID"/>
 *                       &lt;enumeration value="TRASFERIMENTOCOMPETENZA_ASSEGNATARIO_COMPETENZA_NOT_VALID"/>
 *                       &lt;enumeration value="TRASFERIMENTOCOMPETENZA_ASSEGNATARIO_COMPETENZA_MISSING"/>
 *                       &lt;enumeration value="PROT_AGGIORNAMENTO_SPEDIZIONE_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_INSERISCIALLEGATI_MISSING"/>
 *                       &lt;enumeration value="PROT_AGGIORNAMENTO_SPEDIZIONE_MISSING"/>
 *                       &lt;enumeration value="PROT_INSERISCIALLEGATI_PROTOCOLLO_MISSING"/>
 *                       &lt;enumeration value="PROT_INSERISCIALLEGATI_PROTOCOLLO_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_RICERCA_REGISTRO_NOT_VALID"/>
 *                       &lt;enumeration value="RIFIUTO_ALTRA_APP_NOT_VALID"/>
 *                       &lt;enumeration value="INVIA_PROTOCOLLO_NOT_VALID"/>
 *                       &lt;enumeration value="INOLTRA_PROTOCOLLO_NOT_VALID"/>
 *                       &lt;enumeration value="PROT_ALLACCI_MISSING"/>
 *                       &lt;enumeration value="PROT_ALLACCI_PROTOCOLLO_MISSING"/>
 *                       &lt;enumeration value="PROT_ALLACCI_PROTOCOLLO_NOT_VALID"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *                 &lt;attribute name="rilevanza">
 *                   &lt;simpleType>
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;enumeration value="INFO"/>
 *                       &lt;enumeration value="LIEVE"/>
 *                       &lt;enumeration value="GRAVE"/>
 *                     &lt;/restriction>
 *                   &lt;/simpleType>
 *                 &lt;/attribute>
 *                 &lt;attribute name="posizione" type="{http://www.w3.org/2001/XMLSchema}string" />
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfEccezione", propOrder = {
    "eccezione"
})
public class ArrayOfEccezione {

    @XmlElement(name = "Eccezione")
    protected List<ArrayOfEccezione.Eccezione> eccezione;

    /**
     * Gets the value of the eccezione property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the eccezione property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEccezione().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ArrayOfEccezione.Eccezione }
     * 
     * 
     */
    public List<ArrayOfEccezione.Eccezione> getEccezione() {
        if (eccezione == null) {
            eccezione = new ArrayList<ArrayOfEccezione.Eccezione>();
        }
        return this.eccezione;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;attribute name="contestoCodifica" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="messaggioEccezione" type="{http://www.w3.org/2001/XMLSchema}string" />
     *       &lt;attribute name="codiceEccezione">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;enumeration value="GENERIC_EXCEPTION"/>
     *             &lt;enumeration value="SYSTEM_EXCEPTION"/>
     *             &lt;enumeration value="SECURITY_CREDENZIAL_MISSING"/>
     *             &lt;enumeration value="SECURITY_CREDENZIAL_NOT_VALID"/>
     *             &lt;enumeration value="PROT_ENTRATA_MISSING"/>
     *             &lt;enumeration value="PROT_ENTRATA_NOT_VALID"/>
     *             &lt;enumeration value="PROT_ENTRATA_REGISTRO_MISSING"/>
     *             &lt;enumeration value="PROT_ENTRATA_REGISTRO_NOT_VALID"/>
     *             &lt;enumeration value="PROT_ENTRATA_OGGETTO_MISSING"/>
     *             &lt;enumeration value="PROT_ENTRATA_OGGETTO_NOT_VALID"/>
     *             &lt;enumeration value="PROT_ENTRATA_MITTENTE_MISSING"/>
     *             &lt;enumeration value="PROT_ENTRATA_MITTENTE_NOT_VALID"/>
     *             &lt;enumeration value="PROT_ENTRATA_PROTOCOLLATORE_MISSING"/>
     *             &lt;enumeration value="PROT_ENTRATA_PROTOCOLLATORE_NOT_VALID"/>
     *             &lt;enumeration value="PROT_ENTRATA_TIPO_DOCUMENTO_MISSING"/>
     *             &lt;enumeration value="PROT_ENTRATA_TIPO_DOCUMENTO_NOT_VALID"/>
     *             &lt;enumeration value="PROT_ENTRATA_ASSEGNATARIO_COMPETENZA_MISSING"/>
     *             &lt;enumeration value="PROT_ENTRATA_ASSEGNATARIO_COMPETENZA_NOT_VALID"/>
     *             &lt;enumeration value="PROT_ENTRATA_ASSEGNATARIO_CONOSCENZA_NOT_VALID"/>
     *             &lt;enumeration value="PROT_USCITA_MISSING"/>
     *             &lt;enumeration value="PROT_USCITA_NOT_VALID"/>
     *             &lt;enumeration value="PROT_USCITA_REGISTRO_MISSING"/>
     *             &lt;enumeration value="PROT_USCITA_REGISTRO_NOT_VALID"/>
     *             &lt;enumeration value="PROT_USCITA_OGGETTO_MISSING"/>
     *             &lt;enumeration value="PROT_USCITA_OGGETTO_NOT_VALID"/>
     *             &lt;enumeration value="PROT_USCITA_MITTENTE_MISSING"/>
     *             &lt;enumeration value="PROT_USCITA_MITTENTE_NOT_VALID"/>
     *             &lt;enumeration value="PROT_USCITA_PROTOCOLLATORE_MISSING"/>
     *             &lt;enumeration value="PROT_USCITA_PROTOCOLLATORE_NOT_VALID"/>
     *             &lt;enumeration value="PROT_USCITA_TIPO_DOCUMENTO_MISSING"/>
     *             &lt;enumeration value="PROT_USCITA_TIPO_DOCUMENTO_NOT_VALID"/>
     *             &lt;enumeration value="PROT_USCITA_DESTINATARIO_COMPETENZA_MISSING"/>
     *             &lt;enumeration value="PROT_USCITA_DESTINATARIO_COMPETENZA_NOT_VALID"/>
     *             &lt;enumeration value="PROT_USCITA_DESTINATARIO_CONOSCENZA_NOT_VALID"/>
     *             &lt;enumeration value="PROT_USCITA_DESTINATARIO_DATASPEDIZIONE_NOT_VALID"/>
     *             &lt;enumeration value="PROT_RICERCA_MISSING"/>
     *             &lt;enumeration value="PROT_RICERCA_NOT_VALID"/>
     *             &lt;enumeration value="PROT_RICERCA_NON_TROVATO"/>
     *             &lt;enumeration value="AGGIORNAMENTO_PRELEX_MISSING"/>
     *             &lt;enumeration value="AGGIORNAMENTO_PRELEX_NOT_VALID"/>
     *             &lt;enumeration value="AGGIORNAMENTO_PRELEX_PROTOCOLLO_MISSING"/>
     *             &lt;enumeration value="AGGIORNAMENTO_PRELEX_PROTOCOLLO_NOT_VALID"/>
     *             &lt;enumeration value="AGGIORNAMENTO_PRELEX_ASSEGNATARI_MISSING"/>
     *             &lt;enumeration value="AGGIORNAMENTO_PRELEX_ASSEGNATARI_NOT_VALID"/>
     *             &lt;enumeration value="AGGIORNAMENTO_PRELEX_ASSEGNATARIO_COMPETENZA_NOT_VALID"/>
     *             &lt;enumeration value="AGGIORNAMENTO_PRELEX_ASSEGNATARIO_COMPETENZA_MISSING"/>
     *             &lt;enumeration value="AGGIORNAMENTO_SCARICO_MISSING"/>
     *             &lt;enumeration value="AGGIORNAMENTO_SCARICO_NOT_VALID"/>
     *             &lt;enumeration value="AGGIORNAMENTO_SCARICO_PROTOCOLLO_MISSING"/>
     *             &lt;enumeration value="AGGIORNAMENTO_SCARICO_PROTOCOLLO_NOT_VALID"/>
     *             &lt;enumeration value="PROT_INSERIMENTO_DOC_MISSING"/>
     *             &lt;enumeration value="PROT_INSERIMENTO_DOC_NOT_VALID"/>
     *             &lt;enumeration value="PROT_INSERIMENTO_DOC_EMPTY"/>
     *             &lt;enumeration value="PROT_INSERIMENTO_DOC_NON_TROVATO"/>
     *             &lt;enumeration value="PROT_INSERIMENTO_PROT_MISSING"/>
     *             &lt;enumeration value="PROT_INSERIMENTO_PROT_NOT_VALID"/>
     *             &lt;enumeration value="PROT_INSERIMENTO_PROT_NON_TROVATO"/>
     *             &lt;enumeration value="ORGANIGRAMMA_MISSING"/>
     *             &lt;enumeration value="ORGANIGRAMMA_NOT_VALID"/>
     *             &lt;enumeration value="ANNULLAMENTOPROTOCOLLO_MISSING"/>
     *             &lt;enumeration value="ANNULLAMENTOPROTOCOLLO_NOT_VALID"/>
     *             &lt;enumeration value="ANNULLAMENTOPROTOCOLLO_PROTOCOLLO_MISSING"/>
     *             &lt;enumeration value="ANNULLAMENTOPROTOCOLLO_PROTOCOLLO_NOT_VALID"/>
     *             &lt;enumeration value="PRESAINCARICO_MISSING"/>
     *             &lt;enumeration value="PRESAINCARICO_NOT_VALID"/>
     *             &lt;enumeration value="PRESAINCARICO_PROTOCOLLO_MISSING"/>
     *             &lt;enumeration value="PRESAINCARICO_PROTOCOLLO_NOT_VALID"/>
     *             &lt;enumeration value="PRESAINCARICO_ASSEGNATARI_MISSING"/>
     *             &lt;enumeration value="PRESAINCARICO_ASSEGNATARI_NOT_VALID"/>
     *             &lt;enumeration value="PRESAINCARICO_ASSEGNATARIO_COMPETENZA_NOT_VALID"/>
     *             &lt;enumeration value="PRESAINCARICO_ASSEGNATARIO_COMPETENZA_MISSING"/>
     *             &lt;enumeration value="RIASSEGNAZIONE_MISSING"/>
     *             &lt;enumeration value="RIASSEGNAZIONE_NOT_VALID"/>
     *             &lt;enumeration value="RIASSEGNAZIONE_PROTOCOLLO_MISSING"/>
     *             &lt;enumeration value="RIASSEGNAZIONE_PROTOCOLLO_NOT_VALID"/>
     *             &lt;enumeration value="RIASSEGNAZIONE_ASSEGNATARI_MISSING"/>
     *             &lt;enumeration value="RIASSEGNAZIONE_ASSEGNATARI_NOT_VALID"/>
     *             &lt;enumeration value="RIASSEGNAZIONE_ASSEGNATARIO_COMPETENZA_NOT_VALID"/>
     *             &lt;enumeration value="RIASSEGNAZIONE_ASSEGNATARIO_COMPETENZA_MISSING"/>
     *             &lt;enumeration value="TRASFERIMENTOCOMPETENZA_MISSING"/>
     *             &lt;enumeration value="TRASFERIMENTOCOMPETENZA_NOT_VALID"/>
     *             &lt;enumeration value="TRASFERIMENTOCOMPETENZA_PROTOCOLLO_MISSING"/>
     *             &lt;enumeration value="TRASFERIMENTOCOMPETENZA_PROTOCOLLO_NOT_VALID"/>
     *             &lt;enumeration value="TRASFERIMENTOCOMPETENZA_ASSEGNATARI_MISSING"/>
     *             &lt;enumeration value="TRASFERIMENTOCOMPETENZA_ASSEGNATARI_NOT_VALID"/>
     *             &lt;enumeration value="TRASFERIMENTOCOMPETENZA_ASSEGNATARIO_COMPETENZA_NOT_VALID"/>
     *             &lt;enumeration value="TRASFERIMENTOCOMPETENZA_ASSEGNATARIO_COMPETENZA_MISSING"/>
     *             &lt;enumeration value="PROT_AGGIORNAMENTO_SPEDIZIONE_NOT_VALID"/>
     *             &lt;enumeration value="PROT_INSERISCIALLEGATI_MISSING"/>
     *             &lt;enumeration value="PROT_AGGIORNAMENTO_SPEDIZIONE_MISSING"/>
     *             &lt;enumeration value="PROT_INSERISCIALLEGATI_PROTOCOLLO_MISSING"/>
     *             &lt;enumeration value="PROT_INSERISCIALLEGATI_PROTOCOLLO_NOT_VALID"/>
     *             &lt;enumeration value="PROT_RICERCA_REGISTRO_NOT_VALID"/>
     *             &lt;enumeration value="RIFIUTO_ALTRA_APP_NOT_VALID"/>
     *             &lt;enumeration value="INVIA_PROTOCOLLO_NOT_VALID"/>
     *             &lt;enumeration value="INOLTRA_PROTOCOLLO_NOT_VALID"/>
     *             &lt;enumeration value="PROT_ALLACCI_MISSING"/>
     *             &lt;enumeration value="PROT_ALLACCI_PROTOCOLLO_MISSING"/>
     *             &lt;enumeration value="PROT_ALLACCI_PROTOCOLLO_NOT_VALID"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *       &lt;attribute name="rilevanza">
     *         &lt;simpleType>
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;enumeration value="INFO"/>
     *             &lt;enumeration value="LIEVE"/>
     *             &lt;enumeration value="GRAVE"/>
     *           &lt;/restriction>
     *         &lt;/simpleType>
     *       &lt;/attribute>
     *       &lt;attribute name="posizione" type="{http://www.w3.org/2001/XMLSchema}string" />
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Eccezione {

        @XmlAttribute(name = "contestoCodifica")
        protected String contestoCodifica;
        @XmlAttribute(name = "messaggioEccezione")
        protected String messaggioEccezione;
        @XmlAttribute(name = "codiceEccezione")
        protected String codiceEccezione;
        @XmlAttribute(name = "rilevanza")
        protected String rilevanza;
        @XmlAttribute(name = "posizione")
        protected String posizione;

        /**
         * Gets the value of the contestoCodifica property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContestoCodifica() {
            return contestoCodifica;
        }

        /**
         * Sets the value of the contestoCodifica property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContestoCodifica(String value) {
            this.contestoCodifica = value;
        }

        /**
         * Gets the value of the messaggioEccezione property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMessaggioEccezione() {
            return messaggioEccezione;
        }

        /**
         * Sets the value of the messaggioEccezione property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMessaggioEccezione(String value) {
            this.messaggioEccezione = value;
        }

        /**
         * Gets the value of the codiceEccezione property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCodiceEccezione() {
            return codiceEccezione;
        }

        /**
         * Sets the value of the codiceEccezione property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCodiceEccezione(String value) {
            this.codiceEccezione = value;
        }

        /**
         * Gets the value of the rilevanza property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRilevanza() {
            return rilevanza;
        }

        /**
         * Sets the value of the rilevanza property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRilevanza(String value) {
            this.rilevanza = value;
        }

        /**
         * Gets the value of the posizione property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPosizione() {
            return posizione;
        }

        /**
         * Sets the value of the posizione property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPosizione(String value) {
            this.posizione = value;
        }

    }

}
