
package net.protmef.data.common.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CredenzialiUtenteProtocolloType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CredenzialiUtenteProtocolloType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UTENTE" type="{urn:protmef.net:data:common:v1}UtenteType" minOccurs="0"/>
 *         &lt;element name="UFFICIO" type="{urn:protmef.net:data:common:v1}UfficioType" minOccurs="0"/>
 *         &lt;element name="REGISTRO" type="{urn:protmef.net:data:common:v1}RegistroType" minOccurs="0"/>
 *         &lt;element name="PASSWORD" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CredenzialiUtenteProtocolloType", propOrder = {
    "utente",
    "ufficio",
    "registro",
    "password"
})
public class CredenzialiUtenteProtocolloType {

    @XmlElement(name = "UTENTE")
    protected UtenteType utente;
    @XmlElement(name = "UFFICIO")
    protected UfficioType ufficio;
    @XmlElement(name = "REGISTRO")
    protected RegistroType registro;
    @XmlElement(name = "PASSWORD")
    protected String password;

    /**
     * Gets the value of the utente property.
     * 
     * @return
     *     possible object is
     *     {@link UtenteType }
     *     
     */
    public UtenteType getUTENTE() {
        return utente;
    }

    /**
     * Sets the value of the utente property.
     * 
     * @param value
     *     allowed object is
     *     {@link UtenteType }
     *     
     */
    public void setUTENTE(UtenteType value) {
        this.utente = value;
    }

    /**
     * Gets the value of the ufficio property.
     * 
     * @return
     *     possible object is
     *     {@link UfficioType }
     *     
     */
    public UfficioType getUFFICIO() {
        return ufficio;
    }

    /**
     * Sets the value of the ufficio property.
     * 
     * @param value
     *     allowed object is
     *     {@link UfficioType }
     *     
     */
    public void setUFFICIO(UfficioType value) {
        this.ufficio = value;
    }

    /**
     * Gets the value of the registro property.
     * 
     * @return
     *     possible object is
     *     {@link RegistroType }
     *     
     */
    public RegistroType getREGISTRO() {
        return registro;
    }

    /**
     * Sets the value of the registro property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistroType }
     *     
     */
    public void setREGISTRO(RegistroType value) {
        this.registro = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPASSWORD() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPASSWORD(String value) {
        this.password = value;
    }

}
