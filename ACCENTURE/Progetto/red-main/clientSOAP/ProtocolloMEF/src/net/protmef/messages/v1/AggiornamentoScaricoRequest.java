
package net.protmef.messages.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import net.protmef.data.protocollo.v1.AggiornamentoScaricoType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DatiAggiornamentoScarico" type="{urn:protmef.net:data:protocollo:v1}AggiornamentoScaricoType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "datiAggiornamentoScarico"
})
@XmlRootElement(name = "_AggiornamentoScaricoRequest")
public class AggiornamentoScaricoRequest {

    @XmlElement(name = "DatiAggiornamentoScarico")
    protected AggiornamentoScaricoType datiAggiornamentoScarico;

    /**
     * Gets the value of the datiAggiornamentoScarico property.
     * 
     * @return
     *     possible object is
     *     {@link AggiornamentoScaricoType }
     *     
     */
    public AggiornamentoScaricoType getDatiAggiornamentoScarico() {
        return datiAggiornamentoScarico;
    }

    /**
     * Sets the value of the datiAggiornamentoScarico property.
     * 
     * @param value
     *     allowed object is
     *     {@link AggiornamentoScaricoType }
     *     
     */
    public void setDatiAggiornamentoScarico(AggiornamentoScaricoType value) {
        this.datiAggiornamentoScarico = value;
    }

}
