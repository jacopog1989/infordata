
package net.protmef.messages.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import net.protmef.data.protocollo.v1.ProtocolloUscitaType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DatiProtocollo" type="{urn:protmef.net:data:protocollo:v1}ProtocolloUscitaType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "datiProtocollo"
})
@XmlRootElement(name = "_ProtocollazioneUscitaRequest")
public class ProtocollazioneUscitaRequest {

    @XmlElement(name = "DatiProtocollo")
    protected ProtocolloUscitaType datiProtocollo;

    /**
     * Gets the value of the datiProtocollo property.
     * 
     * @return
     *     possible object is
     *     {@link ProtocolloUscitaType }
     *     
     */
    public ProtocolloUscitaType getDatiProtocollo() {
        return datiProtocollo;
    }

    /**
     * Sets the value of the datiProtocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtocolloUscitaType }
     *     
     */
    public void setDatiProtocollo(ProtocolloUscitaType value) {
        this.datiProtocollo = value;
    }

}
