
package net.protmef.headerdata.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import net.protmef.data.common.v1.CredenzialiSicurezzaType;
import net.protmef.data.common.v1.CredenzialiUtenteProtocolloType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="IntestazioneMessaggio" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="CredenzialiUtente" type="{urn:protmef.net:data:common:v1}CredenzialiUtenteProtocolloType" minOccurs="0"/>
 *                   &lt;element name="CredenzialiSicurezza" type="{urn:protmef.net:data:common:v1}CredenzialiSicurezzaType" minOccurs="0"/>
 *                   &lt;element name="Servizio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="Azione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="ServerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="ListaEccezioni" type="{urn:protmef.net:headerdata:v1}ArrayOfEccezione" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "intestazioneMessaggio",
    "listaEccezioni"
})
@XmlRootElement(name = "_HeaderProtMef")
public class HeaderProtMef {

    @XmlElement(name = "IntestazioneMessaggio")
    protected HeaderProtMef.IntestazioneMessaggio intestazioneMessaggio;
    @XmlElement(name = "ListaEccezioni")
    protected ArrayOfEccezione listaEccezioni;

    /**
     * Gets the value of the intestazioneMessaggio property.
     * 
     * @return
     *     possible object is
     *     {@link HeaderProtMef.IntestazioneMessaggio }
     *     
     */
    public HeaderProtMef.IntestazioneMessaggio getIntestazioneMessaggio() {
        return intestazioneMessaggio;
    }

    /**
     * Sets the value of the intestazioneMessaggio property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeaderProtMef.IntestazioneMessaggio }
     *     
     */
    public void setIntestazioneMessaggio(HeaderProtMef.IntestazioneMessaggio value) {
        this.intestazioneMessaggio = value;
    }

    /**
     * Gets the value of the listaEccezioni property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfEccezione }
     *     
     */
    public ArrayOfEccezione getListaEccezioni() {
        return listaEccezioni;
    }

    /**
     * Sets the value of the listaEccezioni property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfEccezione }
     *     
     */
    public void setListaEccezioni(ArrayOfEccezione value) {
        this.listaEccezioni = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="CredenzialiUtente" type="{urn:protmef.net:data:common:v1}CredenzialiUtenteProtocolloType" minOccurs="0"/>
     *         &lt;element name="CredenzialiSicurezza" type="{urn:protmef.net:data:common:v1}CredenzialiSicurezzaType" minOccurs="0"/>
     *         &lt;element name="Servizio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Azione" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="ServerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "credenzialiUtente",
        "credenzialiSicurezza",
        "servizio",
        "azione",
        "serverName"
    })
    public static class IntestazioneMessaggio {

        @XmlElement(name = "CredenzialiUtente")
        protected CredenzialiUtenteProtocolloType credenzialiUtente;
        @XmlElement(name = "CredenzialiSicurezza")
        protected CredenzialiSicurezzaType credenzialiSicurezza;
        @XmlElement(name = "Servizio")
        protected String servizio;
        @XmlElement(name = "Azione")
        protected String azione;
        @XmlElement(name = "ServerName")
        protected String serverName;

        /**
         * Gets the value of the credenzialiUtente property.
         * 
         * @return
         *     possible object is
         *     {@link CredenzialiUtenteProtocolloType }
         *     
         */
        public CredenzialiUtenteProtocolloType getCredenzialiUtente() {
            return credenzialiUtente;
        }

        /**
         * Sets the value of the credenzialiUtente property.
         * 
         * @param value
         *     allowed object is
         *     {@link CredenzialiUtenteProtocolloType }
         *     
         */
        public void setCredenzialiUtente(CredenzialiUtenteProtocolloType value) {
            this.credenzialiUtente = value;
        }

        /**
         * Gets the value of the credenzialiSicurezza property.
         * 
         * @return
         *     possible object is
         *     {@link CredenzialiSicurezzaType }
         *     
         */
        public CredenzialiSicurezzaType getCredenzialiSicurezza() {
            return credenzialiSicurezza;
        }

        /**
         * Sets the value of the credenzialiSicurezza property.
         * 
         * @param value
         *     allowed object is
         *     {@link CredenzialiSicurezzaType }
         *     
         */
        public void setCredenzialiSicurezza(CredenzialiSicurezzaType value) {
            this.credenzialiSicurezza = value;
        }

        /**
         * Gets the value of the servizio property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getServizio() {
            return servizio;
        }

        /**
         * Sets the value of the servizio property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setServizio(String value) {
            this.servizio = value;
        }

        /**
         * Gets the value of the azione property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAzione() {
            return azione;
        }

        /**
         * Sets the value of the azione property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAzione(String value) {
            this.azione = value;
        }

        /**
         * Gets the value of the serverName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getServerName() {
            return serverName;
        }

        /**
         * Sets the value of the serverName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setServerName(String value) {
            this.serverName = value;
        }

    }

}
