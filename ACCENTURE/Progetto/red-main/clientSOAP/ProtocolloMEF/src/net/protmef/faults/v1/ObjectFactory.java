
package net.protmef.faults.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the net.protmef.faults.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ProtMefServiceFaultType_QNAME = new QName("urn:protmef.net:faults:v1", "ProtMefServiceFaultType");
    private final static QName _ProtMefFault_QNAME = new QName("urn:protmef.net:faults:v1", "ProtMefFault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: net.protmef.faults.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ProtMefFault }
     * 
     */
    public ProtMefFault createProtMefFault() {
        return new ProtMefFault();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:protmef.net:faults:v1", name = "ProtMefServiceFaultType")
    public JAXBElement<String> createProtMefServiceFaultType(String value) {
        return new JAXBElement<String>(_ProtMefServiceFaultType_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProtMefFault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:protmef.net:faults:v1", name = "ProtMefFault")
    public JAXBElement<ProtMefFault> createProtMefFault(ProtMefFault value) {
        return new JAXBElement<ProtMefFault>(_ProtMefFault_QNAME, ProtMefFault.class, null, value);
    }

}
