
package net.protmef.data.protocollo.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AzionePresaInCaricoType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="AzionePresaInCaricoType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Accetta"/>
 *     &lt;enumeration value="Respingi"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "AzionePresaInCaricoType")
@XmlEnum
public enum AzionePresaInCaricoType {

    @XmlEnumValue("Accetta")
    ACCETTA("Accetta"),
    @XmlEnumValue("Respingi")
    RESPINGI("Respingi");
    private final String value;

    AzionePresaInCaricoType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static AzionePresaInCaricoType fromValue(String v) {
        for (AzionePresaInCaricoType c: AzionePresaInCaricoType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
