
package net.protmef.messages.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import net.protmef.data.protocollo.v1.PresaInCaricoType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DatiPresaInCarico" type="{urn:protmef.net:data:protocollo:v1}PresaInCaricoType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "datiPresaInCarico"
})
@XmlRootElement(name = "_PresaInCaricoRequest")
public class PresaInCaricoRequest {

    @XmlElement(name = "DatiPresaInCarico")
    protected PresaInCaricoType datiPresaInCarico;

    /**
     * Gets the value of the datiPresaInCarico property.
     * 
     * @return
     *     possible object is
     *     {@link PresaInCaricoType }
     *     
     */
    public PresaInCaricoType getDatiPresaInCarico() {
        return datiPresaInCarico;
    }

    /**
     * Sets the value of the datiPresaInCarico property.
     * 
     * @param value
     *     allowed object is
     *     {@link PresaInCaricoType }
     *     
     */
    public void setDatiPresaInCarico(PresaInCaricoType value) {
        this.datiPresaInCarico = value;
    }

}
