
package net.protmef.data.protocollo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import net.protmef.data.common.v1.UfficioUtenteType;


/**
 * <p>Java class for SPEDIZIONETypeFEDERATO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SPEDIZIONETypeFEDERATO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DESTINATARIO_FEDERATO" type="{urn:protmef.net:data:common:v1}UfficioUtenteType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SPEDIZIONETypeFEDERATO", propOrder = {
    "destinatariofederato"
})
public class SPEDIZIONETypeFEDERATO {

    @XmlElement(name = "DESTINATARIO_FEDERATO")
    protected UfficioUtenteType destinatariofederato;

    /**
     * Gets the value of the destinatariofederato property.
     * 
     * @return
     *     possible object is
     *     {@link UfficioUtenteType }
     *     
     */
    public UfficioUtenteType getDESTINATARIOFEDERATO() {
        return destinatariofederato;
    }

    /**
     * Sets the value of the destinatariofederato property.
     * 
     * @param value
     *     allowed object is
     *     {@link UfficioUtenteType }
     *     
     */
    public void setDESTINATARIOFEDERATO(UfficioUtenteType value) {
        this.destinatariofederato = value;
    }

}
