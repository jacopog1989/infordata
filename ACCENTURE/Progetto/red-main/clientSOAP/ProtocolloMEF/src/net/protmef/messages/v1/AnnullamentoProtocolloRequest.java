
package net.protmef.messages.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import net.protmef.data.protocollo.v1.AnnullamentoProtocolloType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ANNULLAMENTOPROTOCOLLO" type="{urn:protmef.net:data:protocollo:v1}AnnullamentoProtocolloType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "annullamentoprotocollo"
})
@XmlRootElement(name = "_AnnullamentoProtocolloRequest")
public class AnnullamentoProtocolloRequest {

    @XmlElement(name = "ANNULLAMENTOPROTOCOLLO")
    protected AnnullamentoProtocolloType annullamentoprotocollo;

    /**
     * Gets the value of the annullamentoprotocollo property.
     * 
     * @return
     *     possible object is
     *     {@link AnnullamentoProtocolloType }
     *     
     */
    public AnnullamentoProtocolloType getANNULLAMENTOPROTOCOLLO() {
        return annullamentoprotocollo;
    }

    /**
     * Sets the value of the annullamentoprotocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link AnnullamentoProtocolloType }
     *     
     */
    public void setANNULLAMENTOPROTOCOLLO(AnnullamentoProtocolloType value) {
        this.annullamentoprotocollo = value;
    }

}
