
package net.protmef.messages.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import net.protmef.data.protocollo.v1.ProtocolloAllacciType;
import net.protmef.data.protocollo.v1.RegistrazioneProtocolloType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PROTOCOLLO" type="{urn:protmef.net:data:protocollo:v1}RegistrazioneProtocolloType" minOccurs="0"/>
 *         &lt;element name="ALLACCI" type="{urn:protmef.net:data:protocollo:v1}ProtocolloAllacciType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "protocollo",
    "allacci"
})
@XmlRootElement(name = "_AllacciaProtocolloRequest")
public class AllacciaProtocolloRequest {

    @XmlElement(name = "PROTOCOLLO")
    protected RegistrazioneProtocolloType protocollo;
    @XmlElement(name = "ALLACCI")
    protected ProtocolloAllacciType allacci;

    /**
     * Gets the value of the protocollo property.
     * 
     * @return
     *     possible object is
     *     {@link RegistrazioneProtocolloType }
     *     
     */
    public RegistrazioneProtocolloType getPROTOCOLLO() {
        return protocollo;
    }

    /**
     * Sets the value of the protocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrazioneProtocolloType }
     *     
     */
    public void setPROTOCOLLO(RegistrazioneProtocolloType value) {
        this.protocollo = value;
    }

    /**
     * Gets the value of the allacci property.
     * 
     * @return
     *     possible object is
     *     {@link ProtocolloAllacciType }
     *     
     */
    public ProtocolloAllacciType getALLACCI() {
        return allacci;
    }

    /**
     * Sets the value of the allacci property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtocolloAllacciType }
     *     
     */
    public void setALLACCI(ProtocolloAllacciType value) {
        this.allacci = value;
    }

}
