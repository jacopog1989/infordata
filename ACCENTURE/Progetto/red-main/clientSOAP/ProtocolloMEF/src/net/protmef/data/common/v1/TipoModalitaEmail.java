
package net.protmef.data.common.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TipoModalitaEmail.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TipoModalitaEmail">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="None"/>
 *     &lt;enumeration value="Ingresso"/>
 *     &lt;enumeration value="Uscita"/>
 *     &lt;enumeration value="DaProtocollare"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TipoModalitaEmail")
@XmlEnum
public enum TipoModalitaEmail {

    @XmlEnumValue("None")
    NONE("None"),
    @XmlEnumValue("Ingresso")
    INGRESSO("Ingresso"),
    @XmlEnumValue("Uscita")
    USCITA("Uscita"),
    @XmlEnumValue("DaProtocollare")
    DA_PROTOCOLLARE("DaProtocollare");
    private final String value;

    TipoModalitaEmail(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TipoModalitaEmail fromValue(String v) {
        for (TipoModalitaEmail c: TipoModalitaEmail.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
