
package net.protmef.data.common.v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MezzoSpedizioneType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MezzoSpedizioneType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SEQU_K_MEZZO_SPED" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="CODI_SPEDIZIONE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DESC_SPEDIZIONE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="isPostaElettronica" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="isPostaElettronicaCertificata" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MezzoSpedizioneType", propOrder = {
    "sequkmezzosped",
    "codispedizione",
    "descspedizione",
    "isPostaElettronica",
    "isPostaElettronicaCertificata"
})
public class MezzoSpedizioneType {

    @XmlElement(name = "SEQU_K_MEZZO_SPED", required = true)
    protected BigDecimal sequkmezzosped;
    @XmlElement(name = "CODI_SPEDIZIONE")
    protected String codispedizione;
    @XmlElement(name = "DESC_SPEDIZIONE")
    protected String descspedizione;
    protected boolean isPostaElettronica;
    protected boolean isPostaElettronicaCertificata;

    /**
     * Gets the value of the sequkmezzosped property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSEQUKMEZZOSPED() {
        return sequkmezzosped;
    }

    /**
     * Sets the value of the sequkmezzosped property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSEQUKMEZZOSPED(BigDecimal value) {
        this.sequkmezzosped = value;
    }

    /**
     * Gets the value of the codispedizione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODISPEDIZIONE() {
        return codispedizione;
    }

    /**
     * Sets the value of the codispedizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODISPEDIZIONE(String value) {
        this.codispedizione = value;
    }

    /**
     * Gets the value of the descspedizione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESCSPEDIZIONE() {
        return descspedizione;
    }

    /**
     * Sets the value of the descspedizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESCSPEDIZIONE(String value) {
        this.descspedizione = value;
    }

    /**
     * Gets the value of the isPostaElettronica property.
     * 
     */
    public boolean isIsPostaElettronica() {
        return isPostaElettronica;
    }

    /**
     * Sets the value of the isPostaElettronica property.
     * 
     */
    public void setIsPostaElettronica(boolean value) {
        this.isPostaElettronica = value;
    }

    /**
     * Gets the value of the isPostaElettronicaCertificata property.
     * 
     */
    public boolean isIsPostaElettronicaCertificata() {
        return isPostaElettronicaCertificata;
    }

    /**
     * Sets the value of the isPostaElettronicaCertificata property.
     * 
     */
    public void setIsPostaElettronicaCertificata(boolean value) {
        this.isPostaElettronicaCertificata = value;
    }

}
