
package net.protmef.messages.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import net.protmef.data.protocollo.v1.RegistrazioneProtocolloType;
import net.protmef.data.protocollo.v1.SPEDIZIONETypeSTANDARD;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ESTREMI_PROTOCOLLO" type="{urn:protmef.net:data:protocollo:v1}RegistrazioneProtocolloType" minOccurs="0"/>
 *         &lt;element name="DESTINATARI_EMAIL" type="{urn:protmef.net:data:protocollo:v1}SPEDIZIONETypeSTANDARD" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "estremiprotocollo",
    "destinatariemail"
})
@XmlRootElement(name = "_InoltraProtocolloRequest")
public class InoltraProtocolloRequest {

    @XmlElement(name = "ESTREMI_PROTOCOLLO")
    protected RegistrazioneProtocolloType estremiprotocollo;
    @XmlElement(name = "DESTINATARI_EMAIL")
    protected List<SPEDIZIONETypeSTANDARD> destinatariemail;

    /**
     * Gets the value of the estremiprotocollo property.
     * 
     * @return
     *     possible object is
     *     {@link RegistrazioneProtocolloType }
     *     
     */
    public RegistrazioneProtocolloType getESTREMIPROTOCOLLO() {
        return estremiprotocollo;
    }

    /**
     * Sets the value of the estremiprotocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrazioneProtocolloType }
     *     
     */
    public void setESTREMIPROTOCOLLO(RegistrazioneProtocolloType value) {
        this.estremiprotocollo = value;
    }

    /**
     * Gets the value of the destinatariemail property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the destinatariemail property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDESTINATARIEMAIL().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SPEDIZIONETypeSTANDARD }
     * 
     * 
     */
    public List<SPEDIZIONETypeSTANDARD> getDESTINATARIEMAIL() {
        if (destinatariemail == null) {
            destinatariemail = new ArrayList<SPEDIZIONETypeSTANDARD>();
        }
        return this.destinatariemail;
    }

}
