
package net.protmef.data.protocollo.v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the net.protmef.data.protocollo.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: net.protmef.data.protocollo.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MittenteEntrataType }
     * 
     */
    public MittenteEntrataType createMittenteEntrataType() {
        return new MittenteEntrataType();
    }

    /**
     * Create an instance of {@link ProtocolloUscitaType }
     * 
     */
    public ProtocolloUscitaType createProtocolloUscitaType() {
        return new ProtocolloUscitaType();
    }

    /**
     * Create an instance of {@link ProtocolloEntrataType }
     * 
     */
    public ProtocolloEntrataType createProtocolloEntrataType() {
        return new ProtocolloEntrataType();
    }

    /**
     * Create an instance of {@link RicercaProtocolloType }
     * 
     */
    public RicercaProtocolloType createRicercaProtocolloType() {
        return new RicercaProtocolloType();
    }

    /**
     * Create an instance of {@link ProtocolloType }
     * 
     */
    public ProtocolloType createProtocolloType() {
        return new ProtocolloType();
    }

    /**
     * Create an instance of {@link AggiornamentoScaricoType }
     * 
     */
    public AggiornamentoScaricoType createAggiornamentoScaricoType() {
        return new AggiornamentoScaricoType();
    }

    /**
     * Create an instance of {@link PresaInCaricoType }
     * 
     */
    public PresaInCaricoType createPresaInCaricoType() {
        return new PresaInCaricoType();
    }

    /**
     * Create an instance of {@link RegistrazioneProtocolloType }
     * 
     */
    public RegistrazioneProtocolloType createRegistrazioneProtocolloType() {
        return new RegistrazioneProtocolloType();
    }

    /**
     * Create an instance of {@link RequestCOMPETENZAPROTOCOLLOType }
     * 
     */
    public RequestCOMPETENZAPROTOCOLLOType createRequestCOMPETENZAPROTOCOLLOType() {
        return new RequestCOMPETENZAPROTOCOLLOType();
    }

    /**
     * Create an instance of {@link AnnullamentoProtocolloType }
     * 
     */
    public AnnullamentoProtocolloType createAnnullamentoProtocolloType() {
        return new AnnullamentoProtocolloType();
    }

    /**
     * Create an instance of {@link SPEDIZIONETypeSTANDARD }
     * 
     */
    public SPEDIZIONETypeSTANDARD createSPEDIZIONETypeSTANDARD() {
        return new SPEDIZIONETypeSTANDARD();
    }

    /**
     * Create an instance of {@link InvioProtocolloUscitaTypeDESTINATARI }
     * 
     */
    public InvioProtocolloUscitaTypeDESTINATARI createInvioProtocolloUscitaTypeDESTINATARI() {
        return new InvioProtocolloUscitaTypeDESTINATARI();
    }

    /**
     * Create an instance of {@link RicercaProtocolloRegistroType }
     * 
     */
    public RicercaProtocolloRegistroType createRicercaProtocolloRegistroType() {
        return new RicercaProtocolloRegistroType();
    }

    /**
     * Create an instance of {@link ProtocolloAllacciType }
     * 
     */
    public ProtocolloAllacciType createProtocolloAllacciType() {
        return new ProtocolloAllacciType();
    }

    /**
     * Create an instance of {@link ProtocolloDocumentiType }
     * 
     */
    public ProtocolloDocumentiType createProtocolloDocumentiType() {
        return new ProtocolloDocumentiType();
    }

    /**
     * Create an instance of {@link TipoDocumentoType }
     * 
     */
    public TipoDocumentoType createTipoDocumentoType() {
        return new TipoDocumentoType();
    }

    /**
     * Create an instance of {@link SPEDIZIONETypeFEDERATO }
     * 
     */
    public SPEDIZIONETypeFEDERATO createSPEDIZIONETypeFEDERATO() {
        return new SPEDIZIONETypeFEDERATO();
    }

    /**
     * Create an instance of {@link CapitoloType }
     * 
     */
    public CapitoloType createCapitoloType() {
        return new CapitoloType();
    }

    /**
     * Create an instance of {@link AssegnatarioType }
     * 
     */
    public AssegnatarioType createAssegnatarioType() {
        return new AssegnatarioType();
    }

    /**
     * Create an instance of {@link SPEDIZIONETypeIPA }
     * 
     */
    public SPEDIZIONETypeIPA createSPEDIZIONETypeIPA() {
        return new SPEDIZIONETypeIPA();
    }

    /**
     * Create an instance of {@link DocumentoProtocolloType }
     * 
     */
    public DocumentoProtocolloType createDocumentoProtocolloType() {
        return new DocumentoProtocolloType();
    }

    /**
     * Create an instance of {@link DestinatarioType }
     * 
     */
    public DestinatarioType createDestinatarioType() {
        return new DestinatarioType();
    }

    /**
     * Create an instance of {@link MittenteEntrataType.PERSONAFISICA }
     * 
     */
    public MittenteEntrataType.PERSONAFISICA createMittenteEntrataTypePERSONAFISICA() {
        return new MittenteEntrataType.PERSONAFISICA();
    }

    /**
     * Create an instance of {@link MittenteEntrataType.PERSONAGIURIDICA }
     * 
     */
    public MittenteEntrataType.PERSONAGIURIDICA createMittenteEntrataTypePERSONAGIURIDICA() {
        return new MittenteEntrataType.PERSONAGIURIDICA();
    }

    /**
     * Create an instance of {@link ProtocolloUscitaType.DESTINATARI }
     * 
     */
    public ProtocolloUscitaType.DESTINATARI createProtocolloUscitaTypeDESTINATARI() {
        return new ProtocolloUscitaType.DESTINATARI();
    }

    /**
     * Create an instance of {@link ProtocolloUscitaType.ALLACCI }
     * 
     */
    public ProtocolloUscitaType.ALLACCI createProtocolloUscitaTypeALLACCI() {
        return new ProtocolloUscitaType.ALLACCI();
    }

    /**
     * Create an instance of {@link ProtocolloEntrataType.ASSEGNATARI }
     * 
     */
    public ProtocolloEntrataType.ASSEGNATARI createProtocolloEntrataTypeASSEGNATARI() {
        return new ProtocolloEntrataType.ASSEGNATARI();
    }

    /**
     * Create an instance of {@link ProtocolloEntrataType.ALLACCI }
     * 
     */
    public ProtocolloEntrataType.ALLACCI createProtocolloEntrataTypeALLACCI() {
        return new ProtocolloEntrataType.ALLACCI();
    }

}
