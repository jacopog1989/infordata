
package net.protmef.data.protocollo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProtocolloAllacciType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProtocolloAllacciType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ALLACCIO_PRINCIPALE" type="{urn:protmef.net:data:protocollo:v1}RegistrazioneProtocolloType" minOccurs="0"/>
 *         &lt;element name="ALTRI_ALLACCI" type="{urn:protmef.net:data:protocollo:v1}RegistrazioneProtocolloType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProtocolloAllacciType", propOrder = {
    "allaccioprincipale",
    "altriallacci"
})
public class ProtocolloAllacciType {

    @XmlElement(name = "ALLACCIO_PRINCIPALE")
    protected RegistrazioneProtocolloType allaccioprincipale;
    @XmlElement(name = "ALTRI_ALLACCI")
    protected List<RegistrazioneProtocolloType> altriallacci;

    /**
     * Gets the value of the allaccioprincipale property.
     * 
     * @return
     *     possible object is
     *     {@link RegistrazioneProtocolloType }
     *     
     */
    public RegistrazioneProtocolloType getALLACCIOPRINCIPALE() {
        return allaccioprincipale;
    }

    /**
     * Sets the value of the allaccioprincipale property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrazioneProtocolloType }
     *     
     */
    public void setALLACCIOPRINCIPALE(RegistrazioneProtocolloType value) {
        this.allaccioprincipale = value;
    }

    /**
     * Gets the value of the altriallacci property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the altriallacci property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getALTRIALLACCI().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RegistrazioneProtocolloType }
     * 
     * 
     */
    public List<RegistrazioneProtocolloType> getALTRIALLACCI() {
        if (altriallacci == null) {
            altriallacci = new ArrayList<RegistrazioneProtocolloType>();
        }
        return this.altriallacci;
    }

}
