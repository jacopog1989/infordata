
package net.protmef.messages.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import net.protmef.data.protocollo.v1.AssegnatarioType;
import net.protmef.data.protocollo.v1.RegistrazioneProtocolloType;
import net.protmef.data.protocollo.v1.StatoProtocolloType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PROTOCOLLO" type="{urn:protmef.net:data:protocollo:v1}RegistrazioneProtocolloType" minOccurs="0"/>
 *         &lt;element name="ASSEGNATARI" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ASSEGNATARIO_COMPETENZA" type="{urn:protmef.net:data:protocollo:v1}AssegnatarioType" minOccurs="0"/>
 *                   &lt;element name="ASSEGNATARIO_CONOSCENZA" type="{urn:protmef.net:data:protocollo:v1}AssegnatarioType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="STATO" type="{urn:protmef.net:data:protocollo:v1}StatoProtocolloType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "protocollo",
    "assegnatari",
    "stato"
})
@XmlRootElement(name = "_AggiornamentoStatoPreLexRequest")
public class AggiornamentoStatoPreLexRequest {

    @XmlElement(name = "PROTOCOLLO")
    protected RegistrazioneProtocolloType protocollo;
    @XmlElement(name = "ASSEGNATARI")
    protected AggiornamentoStatoPreLexRequest.ASSEGNATARI assegnatari;
    @XmlElement(name = "STATO", required = true)
    protected StatoProtocolloType stato;

    /**
     * Gets the value of the protocollo property.
     * 
     * @return
     *     possible object is
     *     {@link RegistrazioneProtocolloType }
     *     
     */
    public RegistrazioneProtocolloType getPROTOCOLLO() {
        return protocollo;
    }

    /**
     * Sets the value of the protocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrazioneProtocolloType }
     *     
     */
    public void setPROTOCOLLO(RegistrazioneProtocolloType value) {
        this.protocollo = value;
    }

    /**
     * Gets the value of the assegnatari property.
     * 
     * @return
     *     possible object is
     *     {@link AggiornamentoStatoPreLexRequest.ASSEGNATARI }
     *     
     */
    public AggiornamentoStatoPreLexRequest.ASSEGNATARI getASSEGNATARI() {
        return assegnatari;
    }

    /**
     * Sets the value of the assegnatari property.
     * 
     * @param value
     *     allowed object is
     *     {@link AggiornamentoStatoPreLexRequest.ASSEGNATARI }
     *     
     */
    public void setASSEGNATARI(AggiornamentoStatoPreLexRequest.ASSEGNATARI value) {
        this.assegnatari = value;
    }

    /**
     * Gets the value of the stato property.
     * 
     * @return
     *     possible object is
     *     {@link StatoProtocolloType }
     *     
     */
    public StatoProtocolloType getSTATO() {
        return stato;
    }

    /**
     * Sets the value of the stato property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatoProtocolloType }
     *     
     */
    public void setSTATO(StatoProtocolloType value) {
        this.stato = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ASSEGNATARIO_COMPETENZA" type="{urn:protmef.net:data:protocollo:v1}AssegnatarioType" minOccurs="0"/>
     *         &lt;element name="ASSEGNATARIO_CONOSCENZA" type="{urn:protmef.net:data:protocollo:v1}AssegnatarioType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "assegnatariocompetenza",
        "assegnatarioconoscenza"
    })
    public static class ASSEGNATARI {

        @XmlElement(name = "ASSEGNATARIO_COMPETENZA")
        protected AssegnatarioType assegnatariocompetenza;
        @XmlElement(name = "ASSEGNATARIO_CONOSCENZA")
        protected List<AssegnatarioType> assegnatarioconoscenza;

        /**
         * Gets the value of the assegnatariocompetenza property.
         * 
         * @return
         *     possible object is
         *     {@link AssegnatarioType }
         *     
         */
        public AssegnatarioType getASSEGNATARIOCOMPETENZA() {
            return assegnatariocompetenza;
        }

        /**
         * Sets the value of the assegnatariocompetenza property.
         * 
         * @param value
         *     allowed object is
         *     {@link AssegnatarioType }
         *     
         */
        public void setASSEGNATARIOCOMPETENZA(AssegnatarioType value) {
            this.assegnatariocompetenza = value;
        }

        /**
         * Gets the value of the assegnatarioconoscenza property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the assegnatarioconoscenza property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getASSEGNATARIOCONOSCENZA().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AssegnatarioType }
         * 
         * 
         */
        public List<AssegnatarioType> getASSEGNATARIOCONOSCENZA() {
            if (assegnatarioconoscenza == null) {
                assegnatarioconoscenza = new ArrayList<AssegnatarioType>();
            }
            return this.assegnatarioconoscenza;
        }

    }

}
