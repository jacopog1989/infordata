
package net.protmef.data.common.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UfficioUtenteType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UfficioUtenteType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UFFICIO" type="{urn:protmef.net:data:common:v1}UfficioType" minOccurs="0"/>
 *         &lt;element name="UTENTE" type="{urn:protmef.net:data:common:v1}UtenteType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UfficioUtenteType", propOrder = {
    "ufficio",
    "utente"
})
public class UfficioUtenteType {

    @XmlElement(name = "UFFICIO")
    protected UfficioType ufficio;
    @XmlElement(name = "UTENTE")
    protected UtenteType utente;

    /**
     * Gets the value of the ufficio property.
     * 
     * @return
     *     possible object is
     *     {@link UfficioType }
     *     
     */
    public UfficioType getUFFICIO() {
        return ufficio;
    }

    /**
     * Sets the value of the ufficio property.
     * 
     * @param value
     *     allowed object is
     *     {@link UfficioType }
     *     
     */
    public void setUFFICIO(UfficioType value) {
        this.ufficio = value;
    }

    /**
     * Gets the value of the utente property.
     * 
     * @return
     *     possible object is
     *     {@link UtenteType }
     *     
     */
    public UtenteType getUTENTE() {
        return utente;
    }

    /**
     * Sets the value of the utente property.
     * 
     * @param value
     *     allowed object is
     *     {@link UtenteType }
     *     
     */
    public void setUTENTE(UtenteType value) {
        this.utente = value;
    }

}
