
package net.protmef.messages.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import net.protmef.data.protocollo.v1.InvioProtocolloUscitaTypeDESTINATARI;
import net.protmef.data.protocollo.v1.RegistrazioneProtocolloType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ESTREMI_PROTOCOLLO" type="{urn:protmef.net:data:protocollo:v1}RegistrazioneProtocolloType" minOccurs="0"/>
 *         &lt;element name="DESTINATARI" type="{urn:protmef.net:data:protocollo:v1}InvioProtocolloUscitaTypeDESTINATARI" minOccurs="0"/>
 *         &lt;element name="ID_DOCUMENTO_PRINCIPALE" type="{http://microsoft.com/wsdl/types/}guid"/>
 *         &lt;element name="ID_DOCUMENTO_ORIGINALE" type="{http://microsoft.com/wsdl/types/}guid"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "estremiprotocollo",
    "destinatari",
    "iddocumentoprincipale",
    "iddocumentooriginale"
})
@XmlRootElement(name = "_InviaProtocolloRequest")
public class InviaProtocolloRequest {

    @XmlElement(name = "ESTREMI_PROTOCOLLO")
    protected RegistrazioneProtocolloType estremiprotocollo;
    @XmlElement(name = "DESTINATARI")
    protected InvioProtocolloUscitaTypeDESTINATARI destinatari;
    @XmlElement(name = "ID_DOCUMENTO_PRINCIPALE", required = true)
    protected String iddocumentoprincipale;
    @XmlElement(name = "ID_DOCUMENTO_ORIGINALE", required = true)
    protected String iddocumentooriginale;

    /**
     * Gets the value of the estremiprotocollo property.
     * 
     * @return
     *     possible object is
     *     {@link RegistrazioneProtocolloType }
     *     
     */
    public RegistrazioneProtocolloType getESTREMIPROTOCOLLO() {
        return estremiprotocollo;
    }

    /**
     * Sets the value of the estremiprotocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrazioneProtocolloType }
     *     
     */
    public void setESTREMIPROTOCOLLO(RegistrazioneProtocolloType value) {
        this.estremiprotocollo = value;
    }

    /**
     * Gets the value of the destinatari property.
     * 
     * @return
     *     possible object is
     *     {@link InvioProtocolloUscitaTypeDESTINATARI }
     *     
     */
    public InvioProtocolloUscitaTypeDESTINATARI getDESTINATARI() {
        return destinatari;
    }

    /**
     * Sets the value of the destinatari property.
     * 
     * @param value
     *     allowed object is
     *     {@link InvioProtocolloUscitaTypeDESTINATARI }
     *     
     */
    public void setDESTINATARI(InvioProtocolloUscitaTypeDESTINATARI value) {
        this.destinatari = value;
    }

    /**
     * Gets the value of the iddocumentoprincipale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDDOCUMENTOPRINCIPALE() {
        return iddocumentoprincipale;
    }

    /**
     * Sets the value of the iddocumentoprincipale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDDOCUMENTOPRINCIPALE(String value) {
        this.iddocumentoprincipale = value;
    }

    /**
     * Gets the value of the iddocumentooriginale property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDDOCUMENTOORIGINALE() {
        return iddocumentooriginale;
    }

    /**
     * Sets the value of the iddocumentooriginale property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDDOCUMENTOORIGINALE(String value) {
        this.iddocumentooriginale = value;
    }

}
