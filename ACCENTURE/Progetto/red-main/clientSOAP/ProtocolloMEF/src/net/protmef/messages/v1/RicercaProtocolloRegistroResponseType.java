
package net.protmef.messages.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import net.protmef.data.protocollo.v1.ProtocolloType;


/**
 * <p>Java class for RicercaProtocolloRegistroResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RicercaProtocolloRegistroResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ElencoProtocolli" type="{urn:protmef.net:data:protocollo:v1}ProtocolloType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RicercaProtocolloRegistroResponseType", propOrder = {
    "elencoProtocolli"
})
public class RicercaProtocolloRegistroResponseType {

    @XmlElement(name = "ElencoProtocolli")
    protected List<ProtocolloType> elencoProtocolli;

    /**
     * Gets the value of the elencoProtocolli property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elencoProtocolli property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getElencoProtocolli().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProtocolloType }
     * 
     * 
     */
    public List<ProtocolloType> getElencoProtocolli() {
        if (elencoProtocolli == null) {
            elencoProtocolli = new ArrayList<ProtocolloType>();
        }
        return this.elencoProtocolli;
    }

}
