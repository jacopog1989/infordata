
package net.protmef.data.protocollo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import net.protmef.data.common.v1.EmailOrganigrammaType;


/**
 * <p>Java class for SPEDIZIONETypeSTANDARD complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SPEDIZIONETypeSTANDARD">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EMAIL_DESTINATARIO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="EmailSpedizione" type="{urn:protmef.net:data:common:v1}EmailOrganigrammaType" minOccurs="0"/>
 *         &lt;element name="Codi_Messaggio_Posta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SPEDIZIONETypeSTANDARD", propOrder = {
    "emaildestinatario",
    "emailSpedizione",
    "codiMessaggioPosta"
})
public class SPEDIZIONETypeSTANDARD {

    @XmlElement(name = "EMAIL_DESTINATARIO")
    protected String emaildestinatario;
    @XmlElement(name = "EmailSpedizione")
    protected EmailOrganigrammaType emailSpedizione;
    @XmlElement(name = "Codi_Messaggio_Posta")
    protected String codiMessaggioPosta;

    /**
     * Gets the value of the emaildestinatario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEMAILDESTINATARIO() {
        return emaildestinatario;
    }

    /**
     * Sets the value of the emaildestinatario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEMAILDESTINATARIO(String value) {
        this.emaildestinatario = value;
    }

    /**
     * Gets the value of the emailSpedizione property.
     * 
     * @return
     *     possible object is
     *     {@link EmailOrganigrammaType }
     *     
     */
    public EmailOrganigrammaType getEmailSpedizione() {
        return emailSpedizione;
    }

    /**
     * Sets the value of the emailSpedizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailOrganigrammaType }
     *     
     */
    public void setEmailSpedizione(EmailOrganigrammaType value) {
        this.emailSpedizione = value;
    }

    /**
     * Gets the value of the codiMessaggioPosta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodiMessaggioPosta() {
        return codiMessaggioPosta;
    }

    /**
     * Sets the value of the codiMessaggioPosta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodiMessaggioPosta(String value) {
        this.codiMessaggioPosta = value;
    }

}
