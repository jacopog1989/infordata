
package net.protmef.data.protocollo.v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TipoDocumentoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TipoDocumentoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="SEQU_K_TIPO_DOCUMENTO" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="CODICE_TIPO_DOCUMENTO" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="TIPO_DOCUMENTO" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="FK_AOO" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="FLAG_DEFAULT_01" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TipoDocumentoType")
public class TipoDocumentoType {

    @XmlAttribute(name = "SEQU_K_TIPO_DOCUMENTO", required = true)
    protected BigDecimal sequktipodocumento;
    @XmlAttribute(name = "CODICE_TIPO_DOCUMENTO")
    protected String codicetipodocumento;
    @XmlAttribute(name = "TIPO_DOCUMENTO")
    protected String tipodocumento;
    @XmlAttribute(name = "FK_AOO")
    protected BigDecimal fkaoo;
    @XmlAttribute(name = "FLAG_DEFAULT_01")
    protected Integer flagdefault01;

    /**
     * Gets the value of the sequktipodocumento property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSEQUKTIPODOCUMENTO() {
        return sequktipodocumento;
    }

    /**
     * Sets the value of the sequktipodocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSEQUKTIPODOCUMENTO(BigDecimal value) {
        this.sequktipodocumento = value;
    }

    /**
     * Gets the value of the codicetipodocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODICETIPODOCUMENTO() {
        return codicetipodocumento;
    }

    /**
     * Sets the value of the codicetipodocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODICETIPODOCUMENTO(String value) {
        this.codicetipodocumento = value;
    }

    /**
     * Gets the value of the tipodocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTIPODOCUMENTO() {
        return tipodocumento;
    }

    /**
     * Sets the value of the tipodocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTIPODOCUMENTO(String value) {
        this.tipodocumento = value;
    }

    /**
     * Gets the value of the fkaoo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFKAOO() {
        return fkaoo;
    }

    /**
     * Sets the value of the fkaoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFKAOO(BigDecimal value) {
        this.fkaoo = value;
    }

    /**
     * Gets the value of the flagdefault01 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFLAGDEFAULT01() {
        return flagdefault01;
    }

    /**
     * Sets the value of the flagdefault01 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFLAGDEFAULT01(Integer value) {
        this.flagdefault01 = value;
    }

}
