
package net.protmef.data.protocollo.v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import net.protmef.data.common.v1.TitolarioType;
import net.protmef.data.common.v1.UfficioUtenteType;


/**
 * <p>Java class for RicercaProtocolloRegistroType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RicercaProtocolloRegistroType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ANNO" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="NUME_PROTOCOLLO_DA" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="NUME_PROTOCOLLO_A" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DATA_REGISTRAZIONE_DA" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DATA_REGISTRAZIONE_A" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="FK_REGISTRO" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="OGGETTO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TITOLARIO" type="{urn:protmef.net:data:common:v1}TitolarioType" minOccurs="0"/>
 *         &lt;element name="STATO_PROTOCOLLO" type="{urn:protmef.net:data:protocollo:v1}StatoProtocolloType"/>
 *         &lt;element name="TIPO_DOCUMENTO" type="{urn:protmef.net:data:protocollo:v1}TipoDocumentoType" minOccurs="0"/>
 *         &lt;element name="TIPO_PROTOCOLLO" type="{urn:protmef.net:data:protocollo:v1}TipoProtocolloType"/>
 *         &lt;element name="DATA_PROTOCOLLOMITTENTE_DA" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="DATA_PROTOCOLLOMITTENTE_A" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="MITTENTE_ENTRATA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MITTENTE_USCITA" type="{urn:protmef.net:data:common:v1}UfficioUtenteType" minOccurs="0"/>
 *         &lt;element name="DESTINATARIO_USCITA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DESTINATARIO_ENTRATA" type="{urn:protmef.net:data:common:v1}UfficioUtenteType" minOccurs="0"/>
 *         &lt;element name="FLAG_ANNULLATO_01" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="FLAG_SPEDITO_01" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RicercaProtocolloRegistroType", propOrder = {
    "anno",
    "numeprotocolloda",
    "numeprotocolloa",
    "dataregistrazioneda",
    "dataregistrazionea",
    "fkregistro",
    "oggetto",
    "titolario",
    "statoprotocollo",
    "tipodocumento",
    "tipoprotocollo",
    "dataprotocollomittenteda",
    "dataprotocollomittentea",
    "mittenteentrata",
    "mittenteuscita",
    "destinatariouscita",
    "destinatarioentrata",
    "flagannullato01",
    "flagspedito01"
})
public class RicercaProtocolloRegistroType {

    @XmlElement(name = "ANNO")
    protected int anno;
    @XmlElement(name = "NUME_PROTOCOLLO_DA")
    protected Integer numeprotocolloda;
    @XmlElement(name = "NUME_PROTOCOLLO_A")
    protected Integer numeprotocolloa;
    @XmlElement(name = "DATA_REGISTRAZIONE_DA")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataregistrazioneda;
    @XmlElement(name = "DATA_REGISTRAZIONE_A")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataregistrazionea;
    @XmlElement(name = "FK_REGISTRO", required = true)
    protected BigDecimal fkregistro;
    @XmlElement(name = "OGGETTO")
    protected String oggetto;
    @XmlElement(name = "TITOLARIO")
    protected TitolarioType titolario;
    @XmlElement(name = "STATO_PROTOCOLLO", required = true)
    protected StatoProtocolloType statoprotocollo;
    @XmlElement(name = "TIPO_DOCUMENTO")
    protected TipoDocumentoType tipodocumento;
    @XmlElement(name = "TIPO_PROTOCOLLO", required = true)
    protected TipoProtocolloType tipoprotocollo;
    @XmlElement(name = "DATA_PROTOCOLLOMITTENTE_DA")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataprotocollomittenteda;
    @XmlElement(name = "DATA_PROTOCOLLOMITTENTE_A")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dataprotocollomittentea;
    @XmlElement(name = "MITTENTE_ENTRATA")
    protected String mittenteentrata;
    @XmlElement(name = "MITTENTE_USCITA")
    protected UfficioUtenteType mittenteuscita;
    @XmlElement(name = "DESTINATARIO_USCITA")
    protected String destinatariouscita;
    @XmlElement(name = "DESTINATARIO_ENTRATA")
    protected UfficioUtenteType destinatarioentrata;
    @XmlElement(name = "FLAG_ANNULLATO_01")
    protected Integer flagannullato01;
    @XmlElement(name = "FLAG_SPEDITO_01")
    protected Integer flagspedito01;

    /**
     * Gets the value of the anno property.
     * 
     */
    public int getANNO() {
        return anno;
    }

    /**
     * Sets the value of the anno property.
     * 
     */
    public void setANNO(int value) {
        this.anno = value;
    }

    /**
     * Gets the value of the numeprotocolloda property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNUMEPROTOCOLLODA() {
        return numeprotocolloda;
    }

    /**
     * Sets the value of the numeprotocolloda property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNUMEPROTOCOLLODA(Integer value) {
        this.numeprotocolloda = value;
    }

    /**
     * Gets the value of the numeprotocolloa property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNUMEPROTOCOLLOA() {
        return numeprotocolloa;
    }

    /**
     * Sets the value of the numeprotocolloa property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNUMEPROTOCOLLOA(Integer value) {
        this.numeprotocolloa = value;
    }

    /**
     * Gets the value of the dataregistrazioneda property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATAREGISTRAZIONEDA() {
        return dataregistrazioneda;
    }

    /**
     * Sets the value of the dataregistrazioneda property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATAREGISTRAZIONEDA(XMLGregorianCalendar value) {
        this.dataregistrazioneda = value;
    }

    /**
     * Gets the value of the dataregistrazionea property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATAREGISTRAZIONEA() {
        return dataregistrazionea;
    }

    /**
     * Sets the value of the dataregistrazionea property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATAREGISTRAZIONEA(XMLGregorianCalendar value) {
        this.dataregistrazionea = value;
    }

    /**
     * Gets the value of the fkregistro property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFKREGISTRO() {
        return fkregistro;
    }

    /**
     * Sets the value of the fkregistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFKREGISTRO(BigDecimal value) {
        this.fkregistro = value;
    }

    /**
     * Gets the value of the oggetto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOGGETTO() {
        return oggetto;
    }

    /**
     * Sets the value of the oggetto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOGGETTO(String value) {
        this.oggetto = value;
    }

    /**
     * Gets the value of the titolario property.
     * 
     * @return
     *     possible object is
     *     {@link TitolarioType }
     *     
     */
    public TitolarioType getTITOLARIO() {
        return titolario;
    }

    /**
     * Sets the value of the titolario property.
     * 
     * @param value
     *     allowed object is
     *     {@link TitolarioType }
     *     
     */
    public void setTITOLARIO(TitolarioType value) {
        this.titolario = value;
    }

    /**
     * Gets the value of the statoprotocollo property.
     * 
     * @return
     *     possible object is
     *     {@link StatoProtocolloType }
     *     
     */
    public StatoProtocolloType getSTATOPROTOCOLLO() {
        return statoprotocollo;
    }

    /**
     * Sets the value of the statoprotocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatoProtocolloType }
     *     
     */
    public void setSTATOPROTOCOLLO(StatoProtocolloType value) {
        this.statoprotocollo = value;
    }

    /**
     * Gets the value of the tipodocumento property.
     * 
     * @return
     *     possible object is
     *     {@link TipoDocumentoType }
     *     
     */
    public TipoDocumentoType getTIPODOCUMENTO() {
        return tipodocumento;
    }

    /**
     * Sets the value of the tipodocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDocumentoType }
     *     
     */
    public void setTIPODOCUMENTO(TipoDocumentoType value) {
        this.tipodocumento = value;
    }

    /**
     * Gets the value of the tipoprotocollo property.
     * 
     * @return
     *     possible object is
     *     {@link TipoProtocolloType }
     *     
     */
    public TipoProtocolloType getTIPOPROTOCOLLO() {
        return tipoprotocollo;
    }

    /**
     * Sets the value of the tipoprotocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoProtocolloType }
     *     
     */
    public void setTIPOPROTOCOLLO(TipoProtocolloType value) {
        this.tipoprotocollo = value;
    }

    /**
     * Gets the value of the dataprotocollomittenteda property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATAPROTOCOLLOMITTENTEDA() {
        return dataprotocollomittenteda;
    }

    /**
     * Sets the value of the dataprotocollomittenteda property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATAPROTOCOLLOMITTENTEDA(XMLGregorianCalendar value) {
        this.dataprotocollomittenteda = value;
    }

    /**
     * Gets the value of the dataprotocollomittentea property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATAPROTOCOLLOMITTENTEA() {
        return dataprotocollomittentea;
    }

    /**
     * Sets the value of the dataprotocollomittentea property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATAPROTOCOLLOMITTENTEA(XMLGregorianCalendar value) {
        this.dataprotocollomittentea = value;
    }

    /**
     * Gets the value of the mittenteentrata property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMITTENTEENTRATA() {
        return mittenteentrata;
    }

    /**
     * Sets the value of the mittenteentrata property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMITTENTEENTRATA(String value) {
        this.mittenteentrata = value;
    }

    /**
     * Gets the value of the mittenteuscita property.
     * 
     * @return
     *     possible object is
     *     {@link UfficioUtenteType }
     *     
     */
    public UfficioUtenteType getMITTENTEUSCITA() {
        return mittenteuscita;
    }

    /**
     * Sets the value of the mittenteuscita property.
     * 
     * @param value
     *     allowed object is
     *     {@link UfficioUtenteType }
     *     
     */
    public void setMITTENTEUSCITA(UfficioUtenteType value) {
        this.mittenteuscita = value;
    }

    /**
     * Gets the value of the destinatariouscita property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESTINATARIOUSCITA() {
        return destinatariouscita;
    }

    /**
     * Sets the value of the destinatariouscita property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESTINATARIOUSCITA(String value) {
        this.destinatariouscita = value;
    }

    /**
     * Gets the value of the destinatarioentrata property.
     * 
     * @return
     *     possible object is
     *     {@link UfficioUtenteType }
     *     
     */
    public UfficioUtenteType getDESTINATARIOENTRATA() {
        return destinatarioentrata;
    }

    /**
     * Sets the value of the destinatarioentrata property.
     * 
     * @param value
     *     allowed object is
     *     {@link UfficioUtenteType }
     *     
     */
    public void setDESTINATARIOENTRATA(UfficioUtenteType value) {
        this.destinatarioentrata = value;
    }

    /**
     * Gets the value of the flagannullato01 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFLAGANNULLATO01() {
        return flagannullato01;
    }

    /**
     * Sets the value of the flagannullato01 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFLAGANNULLATO01(Integer value) {
        this.flagannullato01 = value;
    }

    /**
     * Gets the value of the flagspedito01 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFLAGSPEDITO01() {
        return flagspedito01;
    }

    /**
     * Sets the value of the flagspedito01 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFLAGSPEDITO01(Integer value) {
        this.flagspedito01 = value;
    }

}
