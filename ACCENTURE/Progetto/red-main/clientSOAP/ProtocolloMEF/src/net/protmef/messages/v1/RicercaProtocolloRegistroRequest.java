
package net.protmef.messages.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import net.protmef.data.protocollo.v1.RicercaProtocolloRegistroType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DatiRicerca" type="{urn:protmef.net:data:protocollo:v1}RicercaProtocolloRegistroType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "datiRicerca"
})
@XmlRootElement(name = "_RicercaProtocolloRegistroRequest")
public class RicercaProtocolloRegistroRequest {

    @XmlElement(name = "DatiRicerca")
    protected RicercaProtocolloRegistroType datiRicerca;

    /**
     * Gets the value of the datiRicerca property.
     * 
     * @return
     *     possible object is
     *     {@link RicercaProtocolloRegistroType }
     *     
     */
    public RicercaProtocolloRegistroType getDatiRicerca() {
        return datiRicerca;
    }

    /**
     * Sets the value of the datiRicerca property.
     * 
     * @param value
     *     allowed object is
     *     {@link RicercaProtocolloRegistroType }
     *     
     */
    public void setDatiRicerca(RicercaProtocolloRegistroType value) {
        this.datiRicerca = value;
    }

}
