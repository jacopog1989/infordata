
package net.protmef.data.protocollo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RequestCOMPETENZAPROTOCOLLOType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestCOMPETENZAPROTOCOLLOType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PROTOCOLLO" type="{urn:protmef.net:data:protocollo:v1}RegistrazioneProtocolloType" minOccurs="0"/>
 *         &lt;element name="ASSEGNATARIO_COMPETENZA" type="{urn:protmef.net:data:protocollo:v1}AssegnatarioType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestCOMPETENZAPROTOCOLLOType", propOrder = {
    "protocollo",
    "assegnatariocompetenza"
})
public class RequestCOMPETENZAPROTOCOLLOType {

    @XmlElement(name = "PROTOCOLLO")
    protected RegistrazioneProtocolloType protocollo;
    @XmlElement(name = "ASSEGNATARIO_COMPETENZA")
    protected AssegnatarioType assegnatariocompetenza;

    /**
     * Gets the value of the protocollo property.
     * 
     * @return
     *     possible object is
     *     {@link RegistrazioneProtocolloType }
     *     
     */
    public RegistrazioneProtocolloType getPROTOCOLLO() {
        return protocollo;
    }

    /**
     * Sets the value of the protocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrazioneProtocolloType }
     *     
     */
    public void setPROTOCOLLO(RegistrazioneProtocolloType value) {
        this.protocollo = value;
    }

    /**
     * Gets the value of the assegnatariocompetenza property.
     * 
     * @return
     *     possible object is
     *     {@link AssegnatarioType }
     *     
     */
    public AssegnatarioType getASSEGNATARIOCOMPETENZA() {
        return assegnatariocompetenza;
    }

    /**
     * Sets the value of the assegnatariocompetenza property.
     * 
     * @param value
     *     allowed object is
     *     {@link AssegnatarioType }
     *     
     */
    public void setASSEGNATARIOCOMPETENZA(AssegnatarioType value) {
        this.assegnatariocompetenza = value;
    }

}
