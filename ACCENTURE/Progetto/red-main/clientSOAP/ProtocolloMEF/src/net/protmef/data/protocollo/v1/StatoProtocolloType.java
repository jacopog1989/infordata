
package net.protmef.data.protocollo.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for StatoProtocolloType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="StatoProtocolloType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Nessuno"/>
 *     &lt;enumeration value="Accettato"/>
 *     &lt;enumeration value="Respinto"/>
 *     &lt;enumeration value="Sospeso"/>
 *     &lt;enumeration value="Risposta"/>
 *     &lt;enumeration value="Atti"/>
 *     &lt;enumeration value="Spedito"/>
 *     &lt;enumeration value="Non_Spedito"/>
 *     &lt;enumeration value="InLavorazione"/>
 *     &lt;enumeration value="DaAssegnare"/>
 *     &lt;enumeration value="DaSpedire"/>
 *     &lt;enumeration value="DaFirmare"/>
 *     &lt;enumeration value="AssegnatoAltraApplicazione"/>
 *     &lt;enumeration value="ProvenienteAltraApplicazione"/>
 *     &lt;enumeration value="DaInviare"/>
 *     &lt;enumeration value="DaDestinare"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "StatoProtocolloType")
@XmlEnum
public enum StatoProtocolloType {

    @XmlEnumValue("Nessuno")
    NESSUNO("Nessuno"),
    @XmlEnumValue("Accettato")
    ACCETTATO("Accettato"),
    @XmlEnumValue("Respinto")
    RESPINTO("Respinto"),
    @XmlEnumValue("Sospeso")
    SOSPESO("Sospeso"),
    @XmlEnumValue("Risposta")
    RISPOSTA("Risposta"),
    @XmlEnumValue("Atti")
    ATTI("Atti"),
    @XmlEnumValue("Spedito")
    SPEDITO("Spedito"),
    @XmlEnumValue("Non_Spedito")
    NON_SPEDITO("Non_Spedito"),
    @XmlEnumValue("InLavorazione")
    IN_LAVORAZIONE("InLavorazione"),
    @XmlEnumValue("DaAssegnare")
    DA_ASSEGNARE("DaAssegnare"),
    @XmlEnumValue("DaSpedire")
    DA_SPEDIRE("DaSpedire"),
    @XmlEnumValue("DaFirmare")
    DA_FIRMARE("DaFirmare"),
    @XmlEnumValue("AssegnatoAltraApplicazione")
    ASSEGNATO_ALTRA_APPLICAZIONE("AssegnatoAltraApplicazione"),
    @XmlEnumValue("ProvenienteAltraApplicazione")
    PROVENIENTE_ALTRA_APPLICAZIONE("ProvenienteAltraApplicazione"),
    @XmlEnumValue("DaInviare")
    DA_INVIARE("DaInviare"),
    @XmlEnumValue("DaDestinare")
    DA_DESTINARE("DaDestinare");
    private final String value;

    StatoProtocolloType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static StatoProtocolloType fromValue(String v) {
        for (StatoProtocolloType c: StatoProtocolloType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
