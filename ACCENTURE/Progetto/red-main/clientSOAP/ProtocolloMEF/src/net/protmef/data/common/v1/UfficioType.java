
package net.protmef.data.common.v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UfficioType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UfficioType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="SEQU_K_UFFICIO" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="FK_STORICO_UFFICIO" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="FK_AOO" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="FK_DIREZIONE" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="CODICE_DIREZIONE" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="CODICE_UFFICIO" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="UFFICIO" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="IS_UFFICIOESTERNO" use="required" type="{http://www.w3.org/2001/XMLSchema}boolean" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UfficioType")
public class UfficioType {

    @XmlAttribute(name = "SEQU_K_UFFICIO", required = true)
    protected BigDecimal sequkufficio;
    @XmlAttribute(name = "FK_STORICO_UFFICIO")
    protected BigDecimal fkstoricoufficio;
    @XmlAttribute(name = "FK_AOO")
    protected BigDecimal fkaoo;
    @XmlAttribute(name = "FK_DIREZIONE")
    protected BigDecimal fkdirezione;
    @XmlAttribute(name = "CODICE_DIREZIONE")
    protected String codicedirezione;
    @XmlAttribute(name = "CODICE_UFFICIO")
    protected String codiceufficio;
    @XmlAttribute(name = "UFFICIO")
    protected String ufficio;
    @XmlAttribute(name = "IS_UFFICIOESTERNO", required = true)
    protected boolean isufficioesterno;

    /**
     * Gets the value of the sequkufficio property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSEQUKUFFICIO() {
        return sequkufficio;
    }

    /**
     * Sets the value of the sequkufficio property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSEQUKUFFICIO(BigDecimal value) {
        this.sequkufficio = value;
    }

    /**
     * Gets the value of the fkstoricoufficio property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFKSTORICOUFFICIO() {
        return fkstoricoufficio;
    }

    /**
     * Sets the value of the fkstoricoufficio property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFKSTORICOUFFICIO(BigDecimal value) {
        this.fkstoricoufficio = value;
    }

    /**
     * Gets the value of the fkaoo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFKAOO() {
        return fkaoo;
    }

    /**
     * Sets the value of the fkaoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFKAOO(BigDecimal value) {
        this.fkaoo = value;
    }

    /**
     * Gets the value of the fkdirezione property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFKDIREZIONE() {
        return fkdirezione;
    }

    /**
     * Sets the value of the fkdirezione property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFKDIREZIONE(BigDecimal value) {
        this.fkdirezione = value;
    }

    /**
     * Gets the value of the codicedirezione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODICEDIREZIONE() {
        return codicedirezione;
    }

    /**
     * Sets the value of the codicedirezione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODICEDIREZIONE(String value) {
        this.codicedirezione = value;
    }

    /**
     * Gets the value of the codiceufficio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODICEUFFICIO() {
        return codiceufficio;
    }

    /**
     * Sets the value of the codiceufficio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODICEUFFICIO(String value) {
        this.codiceufficio = value;
    }

    /**
     * Gets the value of the ufficio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUFFICIO() {
        return ufficio;
    }

    /**
     * Sets the value of the ufficio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUFFICIO(String value) {
        this.ufficio = value;
    }

    /**
     * Gets the value of the isufficioesterno property.
     * 
     */
    public boolean isISUFFICIOESTERNO() {
        return isufficioesterno;
    }

    /**
     * Sets the value of the isufficioesterno property.
     * 
     */
    public void setISUFFICIOESTERNO(boolean value) {
        this.isufficioesterno = value;
    }

}
