
package net.protmef.data.protocollo.v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import net.protmef.data.common.v1.RegistroType;
import net.protmef.data.common.v1.TitolarioType;
import net.protmef.data.common.v1.UfficioUtenteType;


/**
 * <p>Java class for ProtocolloEntrataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProtocolloEntrataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SEQU_K_PROTOCOLLO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="REGISTRO" type="{urn:protmef.net:data:common:v1}RegistroType" minOccurs="0"/>
 *         &lt;element name="TIPO_DOCUMENTO" type="{urn:protmef.net:data:protocollo:v1}TipoDocumentoType" minOccurs="0"/>
 *         &lt;element name="NUME_PROTOCOLLO" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DATA_REGISTRAZIONE" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="DATA_PROTOCOLLO_MITTENTE" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="NUMERO_PROTOCOLLO_MITTENTE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DATA_RICEZIONE" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="MITTENTE" type="{urn:protmef.net:data:protocollo:v1}MittenteEntrataType" minOccurs="0"/>
 *         &lt;element name="OGGETTO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PROTOCOLLATORE" type="{urn:protmef.net:data:common:v1}UfficioUtenteType" minOccurs="0"/>
 *         &lt;element name="ASSEGNATARI" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ASSEGNATARIO_COMPETENZA" type="{urn:protmef.net:data:protocollo:v1}AssegnatarioType" minOccurs="0"/>
 *                   &lt;element name="ASSEGNATARIO_CONOSCENZA" type="{urn:protmef.net:data:protocollo:v1}AssegnatarioType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="FLAG_RISERVATO_01" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="TITOLARIO" type="{urn:protmef.net:data:common:v1}TitolarioType" minOccurs="0"/>
 *         &lt;element name="CAPITOLO" type="{urn:protmef.net:data:protocollo:v1}CapitoloType" minOccurs="0"/>
 *         &lt;element name="ALLACCIO" type="{urn:protmef.net:data:protocollo:v1}RegistrazioneProtocolloType" minOccurs="0"/>
 *         &lt;element name="CHIAVE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="POSIZIONE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="NOTE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DATA_DOCUMENTO" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="FLAG_ANNULLATO_01" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DATA_ANNULLAMENTO" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="NOTE_ANNULLAMENTO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PROVVEDIMENTO_ANNULLAMENTO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STATO_PROTOCOLLO" type="{urn:protmef.net:data:protocollo:v1}StatoProtocolloType"/>
 *         &lt;element name="FLAG_PDF_01" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DOCUMENTI" type="{urn:protmef.net:data:protocollo:v1}ProtocolloDocumentiType" minOccurs="0"/>
 *         &lt;element name="ALLACCI" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ALLACCIO_PRINCIPALE" type="{urn:protmef.net:data:protocollo:v1}RegistrazioneProtocolloType" minOccurs="0"/>
 *                   &lt;element name="ALTRI_ALLACCI" type="{urn:protmef.net:data:protocollo:v1}RegistrazioneProtocolloType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProtocolloEntrataType", propOrder = {
    "sequkprotocollo",
    "registro",
    "tipodocumento",
    "numeprotocollo",
    "dataregistrazione",
    "dataprotocollomittente",
    "numeroprotocollomittente",
    "dataricezione",
    "mittente",
    "oggetto",
    "protocollatore",
    "assegnatari",
    "flagriservato01",
    "titolario",
    "capitolo",
    "allaccio",
    "chiave",
    "posizione",
    "note",
    "datadocumento",
    "flagannullato01",
    "dataannullamento",
    "noteannullamento",
    "provvedimentoannullamento",
    "statoprotocollo",
    "flagpdf01",
    "documenti",
    "allacci"
})
public class ProtocolloEntrataType {

    @XmlElement(name = "SEQU_K_PROTOCOLLO")
    protected BigDecimal sequkprotocollo;
    @XmlElement(name = "REGISTRO")
    protected RegistroType registro;
    @XmlElement(name = "TIPO_DOCUMENTO")
    protected TipoDocumentoType tipodocumento;
    @XmlElement(name = "NUME_PROTOCOLLO")
    protected Integer numeprotocollo;
    @XmlElement(name = "DATA_REGISTRAZIONE")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataregistrazione;
    @XmlElement(name = "DATA_PROTOCOLLO_MITTENTE")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataprotocollomittente;
    @XmlElement(name = "NUMERO_PROTOCOLLO_MITTENTE")
    protected String numeroprotocollomittente;
    @XmlElement(name = "DATA_RICEZIONE")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataricezione;
    @XmlElement(name = "MITTENTE")
    protected MittenteEntrataType mittente;
    @XmlElement(name = "OGGETTO")
    protected String oggetto;
    @XmlElement(name = "PROTOCOLLATORE")
    protected UfficioUtenteType protocollatore;
    @XmlElement(name = "ASSEGNATARI")
    protected ProtocolloEntrataType.ASSEGNATARI assegnatari;
    @XmlElement(name = "FLAG_RISERVATO_01")
    protected Integer flagriservato01;
    @XmlElement(name = "TITOLARIO")
    protected TitolarioType titolario;
    @XmlElement(name = "CAPITOLO")
    protected CapitoloType capitolo;
    @XmlElement(name = "ALLACCIO")
    protected RegistrazioneProtocolloType allaccio;
    @XmlElement(name = "CHIAVE")
    protected String chiave;
    @XmlElement(name = "POSIZIONE")
    protected String posizione;
    @XmlElement(name = "NOTE")
    protected String note;
    @XmlElement(name = "DATA_DOCUMENTO")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datadocumento;
    @XmlElement(name = "FLAG_ANNULLATO_01")
    protected Integer flagannullato01;
    @XmlElement(name = "DATA_ANNULLAMENTO")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataannullamento;
    @XmlElement(name = "NOTE_ANNULLAMENTO")
    protected String noteannullamento;
    @XmlElement(name = "PROVVEDIMENTO_ANNULLAMENTO")
    protected String provvedimentoannullamento;
    @XmlElement(name = "STATO_PROTOCOLLO", required = true)
    protected StatoProtocolloType statoprotocollo;
    @XmlElement(name = "FLAG_PDF_01")
    protected Integer flagpdf01;
    @XmlElement(name = "DOCUMENTI")
    protected ProtocolloDocumentiType documenti;
    @XmlElement(name = "ALLACCI")
    protected ProtocolloEntrataType.ALLACCI allacci;

    /**
     * Gets the value of the sequkprotocollo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSEQUKPROTOCOLLO() {
        return sequkprotocollo;
    }

    /**
     * Sets the value of the sequkprotocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSEQUKPROTOCOLLO(BigDecimal value) {
        this.sequkprotocollo = value;
    }

    /**
     * Gets the value of the registro property.
     * 
     * @return
     *     possible object is
     *     {@link RegistroType }
     *     
     */
    public RegistroType getREGISTRO() {
        return registro;
    }

    /**
     * Sets the value of the registro property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistroType }
     *     
     */
    public void setREGISTRO(RegistroType value) {
        this.registro = value;
    }

    /**
     * Gets the value of the tipodocumento property.
     * 
     * @return
     *     possible object is
     *     {@link TipoDocumentoType }
     *     
     */
    public TipoDocumentoType getTIPODOCUMENTO() {
        return tipodocumento;
    }

    /**
     * Sets the value of the tipodocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDocumentoType }
     *     
     */
    public void setTIPODOCUMENTO(TipoDocumentoType value) {
        this.tipodocumento = value;
    }

    /**
     * Gets the value of the numeprotocollo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNUMEPROTOCOLLO() {
        return numeprotocollo;
    }

    /**
     * Sets the value of the numeprotocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNUMEPROTOCOLLO(Integer value) {
        this.numeprotocollo = value;
    }

    /**
     * Gets the value of the dataregistrazione property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATAREGISTRAZIONE() {
        return dataregistrazione;
    }

    /**
     * Sets the value of the dataregistrazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATAREGISTRAZIONE(XMLGregorianCalendar value) {
        this.dataregistrazione = value;
    }

    /**
     * Gets the value of the dataprotocollomittente property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATAPROTOCOLLOMITTENTE() {
        return dataprotocollomittente;
    }

    /**
     * Sets the value of the dataprotocollomittente property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATAPROTOCOLLOMITTENTE(XMLGregorianCalendar value) {
        this.dataprotocollomittente = value;
    }

    /**
     * Gets the value of the numeroprotocollomittente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNUMEROPROTOCOLLOMITTENTE() {
        return numeroprotocollomittente;
    }

    /**
     * Sets the value of the numeroprotocollomittente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNUMEROPROTOCOLLOMITTENTE(String value) {
        this.numeroprotocollomittente = value;
    }

    /**
     * Gets the value of the dataricezione property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATARICEZIONE() {
        return dataricezione;
    }

    /**
     * Sets the value of the dataricezione property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATARICEZIONE(XMLGregorianCalendar value) {
        this.dataricezione = value;
    }

    /**
     * Gets the value of the mittente property.
     * 
     * @return
     *     possible object is
     *     {@link MittenteEntrataType }
     *     
     */
    public MittenteEntrataType getMITTENTE() {
        return mittente;
    }

    /**
     * Sets the value of the mittente property.
     * 
     * @param value
     *     allowed object is
     *     {@link MittenteEntrataType }
     *     
     */
    public void setMITTENTE(MittenteEntrataType value) {
        this.mittente = value;
    }

    /**
     * Gets the value of the oggetto property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOGGETTO() {
        return oggetto;
    }

    /**
     * Sets the value of the oggetto property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOGGETTO(String value) {
        this.oggetto = value;
    }

    /**
     * Gets the value of the protocollatore property.
     * 
     * @return
     *     possible object is
     *     {@link UfficioUtenteType }
     *     
     */
    public UfficioUtenteType getPROTOCOLLATORE() {
        return protocollatore;
    }

    /**
     * Sets the value of the protocollatore property.
     * 
     * @param value
     *     allowed object is
     *     {@link UfficioUtenteType }
     *     
     */
    public void setPROTOCOLLATORE(UfficioUtenteType value) {
        this.protocollatore = value;
    }

    /**
     * Gets the value of the assegnatari property.
     * 
     * @return
     *     possible object is
     *     {@link ProtocolloEntrataType.ASSEGNATARI }
     *     
     */
    public ProtocolloEntrataType.ASSEGNATARI getASSEGNATARI() {
        return assegnatari;
    }

    /**
     * Sets the value of the assegnatari property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtocolloEntrataType.ASSEGNATARI }
     *     
     */
    public void setASSEGNATARI(ProtocolloEntrataType.ASSEGNATARI value) {
        this.assegnatari = value;
    }

    /**
     * Gets the value of the flagriservato01 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFLAGRISERVATO01() {
        return flagriservato01;
    }

    /**
     * Sets the value of the flagriservato01 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFLAGRISERVATO01(Integer value) {
        this.flagriservato01 = value;
    }

    /**
     * Gets the value of the titolario property.
     * 
     * @return
     *     possible object is
     *     {@link TitolarioType }
     *     
     */
    public TitolarioType getTITOLARIO() {
        return titolario;
    }

    /**
     * Sets the value of the titolario property.
     * 
     * @param value
     *     allowed object is
     *     {@link TitolarioType }
     *     
     */
    public void setTITOLARIO(TitolarioType value) {
        this.titolario = value;
    }

    /**
     * Gets the value of the capitolo property.
     * 
     * @return
     *     possible object is
     *     {@link CapitoloType }
     *     
     */
    public CapitoloType getCAPITOLO() {
        return capitolo;
    }

    /**
     * Sets the value of the capitolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CapitoloType }
     *     
     */
    public void setCAPITOLO(CapitoloType value) {
        this.capitolo = value;
    }

    /**
     * Gets the value of the allaccio property.
     * 
     * @return
     *     possible object is
     *     {@link RegistrazioneProtocolloType }
     *     
     */
    public RegistrazioneProtocolloType getALLACCIO() {
        return allaccio;
    }

    /**
     * Sets the value of the allaccio property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrazioneProtocolloType }
     *     
     */
    public void setALLACCIO(RegistrazioneProtocolloType value) {
        this.allaccio = value;
    }

    /**
     * Gets the value of the chiave property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCHIAVE() {
        return chiave;
    }

    /**
     * Sets the value of the chiave property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCHIAVE(String value) {
        this.chiave = value;
    }

    /**
     * Gets the value of the posizione property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPOSIZIONE() {
        return posizione;
    }

    /**
     * Sets the value of the posizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPOSIZIONE(String value) {
        this.posizione = value;
    }

    /**
     * Gets the value of the note property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNOTE() {
        return note;
    }

    /**
     * Sets the value of the note property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNOTE(String value) {
        this.note = value;
    }

    /**
     * Gets the value of the datadocumento property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATADOCUMENTO() {
        return datadocumento;
    }

    /**
     * Sets the value of the datadocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATADOCUMENTO(XMLGregorianCalendar value) {
        this.datadocumento = value;
    }

    /**
     * Gets the value of the flagannullato01 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFLAGANNULLATO01() {
        return flagannullato01;
    }

    /**
     * Sets the value of the flagannullato01 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFLAGANNULLATO01(Integer value) {
        this.flagannullato01 = value;
    }

    /**
     * Gets the value of the dataannullamento property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATAANNULLAMENTO() {
        return dataannullamento;
    }

    /**
     * Sets the value of the dataannullamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATAANNULLAMENTO(XMLGregorianCalendar value) {
        this.dataannullamento = value;
    }

    /**
     * Gets the value of the noteannullamento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNOTEANNULLAMENTO() {
        return noteannullamento;
    }

    /**
     * Sets the value of the noteannullamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNOTEANNULLAMENTO(String value) {
        this.noteannullamento = value;
    }

    /**
     * Gets the value of the provvedimentoannullamento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPROVVEDIMENTOANNULLAMENTO() {
        return provvedimentoannullamento;
    }

    /**
     * Sets the value of the provvedimentoannullamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPROVVEDIMENTOANNULLAMENTO(String value) {
        this.provvedimentoannullamento = value;
    }

    /**
     * Gets the value of the statoprotocollo property.
     * 
     * @return
     *     possible object is
     *     {@link StatoProtocolloType }
     *     
     */
    public StatoProtocolloType getSTATOPROTOCOLLO() {
        return statoprotocollo;
    }

    /**
     * Sets the value of the statoprotocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatoProtocolloType }
     *     
     */
    public void setSTATOPROTOCOLLO(StatoProtocolloType value) {
        this.statoprotocollo = value;
    }

    /**
     * Gets the value of the flagpdf01 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFLAGPDF01() {
        return flagpdf01;
    }

    /**
     * Sets the value of the flagpdf01 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFLAGPDF01(Integer value) {
        this.flagpdf01 = value;
    }

    /**
     * Gets the value of the documenti property.
     * 
     * @return
     *     possible object is
     *     {@link ProtocolloDocumentiType }
     *     
     */
    public ProtocolloDocumentiType getDOCUMENTI() {
        return documenti;
    }

    /**
     * Sets the value of the documenti property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtocolloDocumentiType }
     *     
     */
    public void setDOCUMENTI(ProtocolloDocumentiType value) {
        this.documenti = value;
    }

    /**
     * Gets the value of the allacci property.
     * 
     * @return
     *     possible object is
     *     {@link ProtocolloEntrataType.ALLACCI }
     *     
     */
    public ProtocolloEntrataType.ALLACCI getALLACCI() {
        return allacci;
    }

    /**
     * Sets the value of the allacci property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtocolloEntrataType.ALLACCI }
     *     
     */
    public void setALLACCI(ProtocolloEntrataType.ALLACCI value) {
        this.allacci = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ALLACCIO_PRINCIPALE" type="{urn:protmef.net:data:protocollo:v1}RegistrazioneProtocolloType" minOccurs="0"/>
     *         &lt;element name="ALTRI_ALLACCI" type="{urn:protmef.net:data:protocollo:v1}RegistrazioneProtocolloType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "allaccioprincipale",
        "altriallacci"
    })
    public static class ALLACCI {

        @XmlElement(name = "ALLACCIO_PRINCIPALE")
        protected RegistrazioneProtocolloType allaccioprincipale;
        @XmlElement(name = "ALTRI_ALLACCI")
        protected List<RegistrazioneProtocolloType> altriallacci;

        /**
         * Gets the value of the allaccioprincipale property.
         * 
         * @return
         *     possible object is
         *     {@link RegistrazioneProtocolloType }
         *     
         */
        public RegistrazioneProtocolloType getALLACCIOPRINCIPALE() {
            return allaccioprincipale;
        }

        /**
         * Sets the value of the allaccioprincipale property.
         * 
         * @param value
         *     allowed object is
         *     {@link RegistrazioneProtocolloType }
         *     
         */
        public void setALLACCIOPRINCIPALE(RegistrazioneProtocolloType value) {
            this.allaccioprincipale = value;
        }

        /**
         * Gets the value of the altriallacci property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the altriallacci property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getALTRIALLACCI().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link RegistrazioneProtocolloType }
         * 
         * 
         */
        public List<RegistrazioneProtocolloType> getALTRIALLACCI() {
            if (altriallacci == null) {
                altriallacci = new ArrayList<RegistrazioneProtocolloType>();
            }
            return this.altriallacci;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ASSEGNATARIO_COMPETENZA" type="{urn:protmef.net:data:protocollo:v1}AssegnatarioType" minOccurs="0"/>
     *         &lt;element name="ASSEGNATARIO_CONOSCENZA" type="{urn:protmef.net:data:protocollo:v1}AssegnatarioType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "assegnatariocompetenza",
        "assegnatarioconoscenza"
    })
    public static class ASSEGNATARI {

        @XmlElement(name = "ASSEGNATARIO_COMPETENZA")
        protected AssegnatarioType assegnatariocompetenza;
        @XmlElement(name = "ASSEGNATARIO_CONOSCENZA")
        protected List<AssegnatarioType> assegnatarioconoscenza;

        /**
         * Gets the value of the assegnatariocompetenza property.
         * 
         * @return
         *     possible object is
         *     {@link AssegnatarioType }
         *     
         */
        public AssegnatarioType getASSEGNATARIOCOMPETENZA() {
            return assegnatariocompetenza;
        }

        /**
         * Sets the value of the assegnatariocompetenza property.
         * 
         * @param value
         *     allowed object is
         *     {@link AssegnatarioType }
         *     
         */
        public void setASSEGNATARIOCOMPETENZA(AssegnatarioType value) {
            this.assegnatariocompetenza = value;
        }

        /**
         * Gets the value of the assegnatarioconoscenza property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the assegnatarioconoscenza property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getASSEGNATARIOCONOSCENZA().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link AssegnatarioType }
         * 
         * 
         */
        public List<AssegnatarioType> getASSEGNATARIOCONOSCENZA() {
            if (assegnatarioconoscenza == null) {
                assegnatarioconoscenza = new ArrayList<AssegnatarioType>();
            }
            return this.assegnatarioconoscenza;
        }

    }

}
