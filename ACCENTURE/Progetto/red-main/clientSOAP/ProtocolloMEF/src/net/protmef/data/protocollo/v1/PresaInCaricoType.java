
package net.protmef.data.protocollo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import net.protmef.data.common.v1.TitolarioType;


/**
 * <p>Java class for PresaInCaricoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PresaInCaricoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PROTOCOLLO" type="{urn:protmef.net:data:protocollo:v1}RegistrazioneProtocolloType" minOccurs="0"/>
 *         &lt;element name="TITOLARIO" type="{urn:protmef.net:data:common:v1}TitolarioType" minOccurs="0"/>
 *         &lt;element name="AZIONE" type="{urn:protmef.net:data:protocollo:v1}AzionePresaInCaricoType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PresaInCaricoType", propOrder = {
    "protocollo",
    "titolario",
    "azione"
})
public class PresaInCaricoType {

    @XmlElement(name = "PROTOCOLLO")
    protected RegistrazioneProtocolloType protocollo;
    @XmlElement(name = "TITOLARIO")
    protected TitolarioType titolario;
    @XmlElement(name = "AZIONE", required = true)
    protected AzionePresaInCaricoType azione;

    /**
     * Gets the value of the protocollo property.
     * 
     * @return
     *     possible object is
     *     {@link RegistrazioneProtocolloType }
     *     
     */
    public RegistrazioneProtocolloType getPROTOCOLLO() {
        return protocollo;
    }

    /**
     * Sets the value of the protocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrazioneProtocolloType }
     *     
     */
    public void setPROTOCOLLO(RegistrazioneProtocolloType value) {
        this.protocollo = value;
    }

    /**
     * Gets the value of the titolario property.
     * 
     * @return
     *     possible object is
     *     {@link TitolarioType }
     *     
     */
    public TitolarioType getTITOLARIO() {
        return titolario;
    }

    /**
     * Sets the value of the titolario property.
     * 
     * @param value
     *     allowed object is
     *     {@link TitolarioType }
     *     
     */
    public void setTITOLARIO(TitolarioType value) {
        this.titolario = value;
    }

    /**
     * Gets the value of the azione property.
     * 
     * @return
     *     possible object is
     *     {@link AzionePresaInCaricoType }
     *     
     */
    public AzionePresaInCaricoType getAZIONE() {
        return azione;
    }

    /**
     * Sets the value of the azione property.
     * 
     * @param value
     *     allowed object is
     *     {@link AzionePresaInCaricoType }
     *     
     */
    public void setAZIONE(AzionePresaInCaricoType value) {
        this.azione = value;
    }

}
