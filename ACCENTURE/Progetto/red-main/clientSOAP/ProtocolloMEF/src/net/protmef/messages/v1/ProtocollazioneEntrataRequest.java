
package net.protmef.messages.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import net.protmef.data.protocollo.v1.ProtocolloEntrataType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DatiProtocollo" type="{urn:protmef.net:data:protocollo:v1}ProtocolloEntrataType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "datiProtocollo"
})
@XmlRootElement(name = "_ProtocollazioneEntrataRequest")
public class ProtocollazioneEntrataRequest {

    @XmlElement(name = "DatiProtocollo")
    protected ProtocolloEntrataType datiProtocollo;

    /**
     * Gets the value of the datiProtocollo property.
     * 
     * @return
     *     possible object is
     *     {@link ProtocolloEntrataType }
     *     
     */
    public ProtocolloEntrataType getDatiProtocollo() {
        return datiProtocollo;
    }

    /**
     * Sets the value of the datiProtocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtocolloEntrataType }
     *     
     */
    public void setDatiProtocollo(ProtocolloEntrataType value) {
        this.datiProtocollo = value;
    }

}
