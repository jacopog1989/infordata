
package net.protmef.messages.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EsitoOperazioneType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="EsitoOperazioneType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Riuscita"/>
 *     &lt;enumeration value="Fallita"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EsitoOperazioneType")
@XmlEnum
public enum EsitoOperazioneType {

    @XmlEnumValue("Riuscita")
    RIUSCITA("Riuscita"),
    @XmlEnumValue("Fallita")
    FALLITA("Fallita");
    private final String value;

    EsitoOperazioneType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static EsitoOperazioneType fromValue(String v) {
        for (EsitoOperazioneType c: EsitoOperazioneType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
