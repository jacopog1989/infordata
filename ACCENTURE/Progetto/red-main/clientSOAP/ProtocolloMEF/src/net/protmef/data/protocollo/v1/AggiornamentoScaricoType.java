
package net.protmef.data.protocollo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for AggiornamentoScaricoType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AggiornamentoScaricoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PROTOCOLLO" type="{urn:protmef.net:data:protocollo:v1}RegistrazioneProtocolloType" minOccurs="0"/>
 *         &lt;element name="TIPOSCARICO" type="{urn:protmef.net:data:protocollo:v1}ScaricoProtocolloType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AggiornamentoScaricoType", propOrder = {
    "protocollo",
    "tiposcarico"
})
public class AggiornamentoScaricoType {

    @XmlElement(name = "PROTOCOLLO")
    protected RegistrazioneProtocolloType protocollo;
    @XmlElement(name = "TIPOSCARICO", required = true)
    protected ScaricoProtocolloType tiposcarico;

    /**
     * Gets the value of the protocollo property.
     * 
     * @return
     *     possible object is
     *     {@link RegistrazioneProtocolloType }
     *     
     */
    public RegistrazioneProtocolloType getPROTOCOLLO() {
        return protocollo;
    }

    /**
     * Sets the value of the protocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrazioneProtocolloType }
     *     
     */
    public void setPROTOCOLLO(RegistrazioneProtocolloType value) {
        this.protocollo = value;
    }

    /**
     * Gets the value of the tiposcarico property.
     * 
     * @return
     *     possible object is
     *     {@link ScaricoProtocolloType }
     *     
     */
    public ScaricoProtocolloType getTIPOSCARICO() {
        return tiposcarico;
    }

    /**
     * Sets the value of the tiposcarico property.
     * 
     * @param value
     *     allowed object is
     *     {@link ScaricoProtocolloType }
     *     
     */
    public void setTIPOSCARICO(ScaricoProtocolloType value) {
        this.tiposcarico = value;
    }

}
