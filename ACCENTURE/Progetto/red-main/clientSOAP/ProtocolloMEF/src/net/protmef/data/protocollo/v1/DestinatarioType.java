
package net.protmef.data.protocollo.v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import net.protmef.data.common.v1.MezzoSpedizioneType;


/**
 * <p>Java class for DestinatarioType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DestinatarioType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SEQU_K_DESTINATARIO" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="TIPO_DESTINATARIO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DESTINATARIO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MEZZO_SPEDIZIONE" type="{urn:protmef.net:data:common:v1}MezzoSpedizioneType" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element name="SPEDIZIONE_FEDERATO" type="{urn:protmef.net:data:protocollo:v1}SPEDIZIONETypeFEDERATO" minOccurs="0"/>
 *           &lt;element name="SPEDIZIONE_STANDARD" type="{urn:protmef.net:data:protocollo:v1}SPEDIZIONETypeSTANDARD" minOccurs="0"/>
 *           &lt;element name="SPEDIZIONE_IPA" type="{urn:protmef.net:data:protocollo:v1}SPEDIZIONETypeIPA" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="DATA_SPEDIZIONE" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="isCompetenza" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="isSpedito" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DestinatarioType", propOrder = {
    "sequkdestinatario",
    "tipodestinatario",
    "destinatario",
    "mezzospedizione",
    "spedizionefederato",
    "spedizionestandard",
    "spedizioneipa",
    "dataspedizione",
    "isCompetenza",
    "isSpedito"
})
public class DestinatarioType {

    @XmlElement(name = "SEQU_K_DESTINATARIO", required = true)
    protected BigDecimal sequkdestinatario;
    @XmlElement(name = "TIPO_DESTINATARIO")
    protected String tipodestinatario;
    @XmlElement(name = "DESTINATARIO")
    protected String destinatario;
    @XmlElement(name = "MEZZO_SPEDIZIONE")
    protected MezzoSpedizioneType mezzospedizione;
    @XmlElement(name = "SPEDIZIONE_FEDERATO")
    protected SPEDIZIONETypeFEDERATO spedizionefederato;
    @XmlElement(name = "SPEDIZIONE_STANDARD")
    protected SPEDIZIONETypeSTANDARD spedizionestandard;
    @XmlElement(name = "SPEDIZIONE_IPA")
    protected SPEDIZIONETypeIPA spedizioneipa;
    @XmlElement(name = "DATA_SPEDIZIONE")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataspedizione;
    protected boolean isCompetenza;
    protected boolean isSpedito;

    /**
     * Gets the value of the sequkdestinatario property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSEQUKDESTINATARIO() {
        return sequkdestinatario;
    }

    /**
     * Sets the value of the sequkdestinatario property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSEQUKDESTINATARIO(BigDecimal value) {
        this.sequkdestinatario = value;
    }

    /**
     * Gets the value of the tipodestinatario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTIPODESTINATARIO() {
        return tipodestinatario;
    }

    /**
     * Sets the value of the tipodestinatario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTIPODESTINATARIO(String value) {
        this.tipodestinatario = value;
    }

    /**
     * Gets the value of the destinatario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESTINATARIO() {
        return destinatario;
    }

    /**
     * Sets the value of the destinatario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESTINATARIO(String value) {
        this.destinatario = value;
    }

    /**
     * Gets the value of the mezzospedizione property.
     * 
     * @return
     *     possible object is
     *     {@link MezzoSpedizioneType }
     *     
     */
    public MezzoSpedizioneType getMEZZOSPEDIZIONE() {
        return mezzospedizione;
    }

    /**
     * Sets the value of the mezzospedizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link MezzoSpedizioneType }
     *     
     */
    public void setMEZZOSPEDIZIONE(MezzoSpedizioneType value) {
        this.mezzospedizione = value;
    }

    /**
     * Gets the value of the spedizionefederato property.
     * 
     * @return
     *     possible object is
     *     {@link SPEDIZIONETypeFEDERATO }
     *     
     */
    public SPEDIZIONETypeFEDERATO getSPEDIZIONEFEDERATO() {
        return spedizionefederato;
    }

    /**
     * Sets the value of the spedizionefederato property.
     * 
     * @param value
     *     allowed object is
     *     {@link SPEDIZIONETypeFEDERATO }
     *     
     */
    public void setSPEDIZIONEFEDERATO(SPEDIZIONETypeFEDERATO value) {
        this.spedizionefederato = value;
    }

    /**
     * Gets the value of the spedizionestandard property.
     * 
     * @return
     *     possible object is
     *     {@link SPEDIZIONETypeSTANDARD }
     *     
     */
    public SPEDIZIONETypeSTANDARD getSPEDIZIONESTANDARD() {
        return spedizionestandard;
    }

    /**
     * Sets the value of the spedizionestandard property.
     * 
     * @param value
     *     allowed object is
     *     {@link SPEDIZIONETypeSTANDARD }
     *     
     */
    public void setSPEDIZIONESTANDARD(SPEDIZIONETypeSTANDARD value) {
        this.spedizionestandard = value;
    }

    /**
     * Gets the value of the spedizioneipa property.
     * 
     * @return
     *     possible object is
     *     {@link SPEDIZIONETypeIPA }
     *     
     */
    public SPEDIZIONETypeIPA getSPEDIZIONEIPA() {
        return spedizioneipa;
    }

    /**
     * Sets the value of the spedizioneipa property.
     * 
     * @param value
     *     allowed object is
     *     {@link SPEDIZIONETypeIPA }
     *     
     */
    public void setSPEDIZIONEIPA(SPEDIZIONETypeIPA value) {
        this.spedizioneipa = value;
    }

    /**
     * Gets the value of the dataspedizione property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATASPEDIZIONE() {
        return dataspedizione;
    }

    /**
     * Sets the value of the dataspedizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATASPEDIZIONE(XMLGregorianCalendar value) {
        this.dataspedizione = value;
    }

    /**
     * Gets the value of the isCompetenza property.
     * 
     */
    public boolean isIsCompetenza() {
        return isCompetenza;
    }

    /**
     * Sets the value of the isCompetenza property.
     * 
     */
    public void setIsCompetenza(boolean value) {
        this.isCompetenza = value;
    }

    /**
     * Gets the value of the isSpedito property.
     * 
     */
    public boolean isIsSpedito() {
        return isSpedito;
    }

    /**
     * Sets the value of the isSpedito property.
     * 
     */
    public void setIsSpedito(boolean value) {
        this.isSpedito = value;
    }

}
