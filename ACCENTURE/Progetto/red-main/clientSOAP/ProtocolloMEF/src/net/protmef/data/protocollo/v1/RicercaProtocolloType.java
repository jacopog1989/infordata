
package net.protmef.data.protocollo.v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for RicercaProtocolloType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RicercaProtocolloType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SEQU_K_PROTOCOLLO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="ANNO" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="NUME_PROTOCOLLO" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="DATA_REGISTRAZIONE" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="FK_REGISTRO" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RicercaProtocolloType", propOrder = {
    "sequkprotocollo",
    "anno",
    "numeprotocollo",
    "dataregistrazione",
    "fkregistro"
})
public class RicercaProtocolloType {

    @XmlElement(name = "SEQU_K_PROTOCOLLO")
    protected BigDecimal sequkprotocollo;
    @XmlElement(name = "ANNO")
    protected Integer anno;
    @XmlElement(name = "NUME_PROTOCOLLO")
    protected Integer numeprotocollo;
    @XmlElement(name = "DATA_REGISTRAZIONE")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataregistrazione;
    @XmlElement(name = "FK_REGISTRO")
    protected BigDecimal fkregistro;

    /**
     * Gets the value of the sequkprotocollo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSEQUKPROTOCOLLO() {
        return sequkprotocollo;
    }

    /**
     * Sets the value of the sequkprotocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSEQUKPROTOCOLLO(BigDecimal value) {
        this.sequkprotocollo = value;
    }

    /**
     * Gets the value of the anno property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getANNO() {
        return anno;
    }

    /**
     * Sets the value of the anno property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setANNO(Integer value) {
        this.anno = value;
    }

    /**
     * Gets the value of the numeprotocollo property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNUMEPROTOCOLLO() {
        return numeprotocollo;
    }

    /**
     * Sets the value of the numeprotocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNUMEPROTOCOLLO(Integer value) {
        this.numeprotocollo = value;
    }

    /**
     * Gets the value of the dataregistrazione property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATAREGISTRAZIONE() {
        return dataregistrazione;
    }

    /**
     * Sets the value of the dataregistrazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATAREGISTRAZIONE(XMLGregorianCalendar value) {
        this.dataregistrazione = value;
    }

    /**
     * Gets the value of the fkregistro property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFKREGISTRO() {
        return fkregistro;
    }

    /**
     * Sets the value of the fkregistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFKREGISTRO(BigDecimal value) {
        this.fkregistro = value;
    }

}
