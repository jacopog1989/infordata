
package net.protmef.data.protocollo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for InvioProtocolloUscitaTypeDESTINATARI complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="InvioProtocolloUscitaTypeDESTINATARI">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DESTINATARIO_COMPETENZA" type="{urn:protmef.net:data:protocollo:v1}DestinatarioType" minOccurs="0"/>
 *         &lt;element name="DESTINATARIO_CONOSCENZA" type="{urn:protmef.net:data:protocollo:v1}DestinatarioType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "InvioProtocolloUscitaTypeDESTINATARI", propOrder = {
    "destinatariocompetenza",
    "destinatarioconoscenza"
})
public class InvioProtocolloUscitaTypeDESTINATARI {

    @XmlElement(name = "DESTINATARIO_COMPETENZA")
    protected DestinatarioType destinatariocompetenza;
    @XmlElement(name = "DESTINATARIO_CONOSCENZA")
    protected List<DestinatarioType> destinatarioconoscenza;

    /**
     * Gets the value of the destinatariocompetenza property.
     * 
     * @return
     *     possible object is
     *     {@link DestinatarioType }
     *     
     */
    public DestinatarioType getDESTINATARIOCOMPETENZA() {
        return destinatariocompetenza;
    }

    /**
     * Sets the value of the destinatariocompetenza property.
     * 
     * @param value
     *     allowed object is
     *     {@link DestinatarioType }
     *     
     */
    public void setDESTINATARIOCOMPETENZA(DestinatarioType value) {
        this.destinatariocompetenza = value;
    }

    /**
     * Gets the value of the destinatarioconoscenza property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the destinatarioconoscenza property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDESTINATARIOCONOSCENZA().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DestinatarioType }
     * 
     * 
     */
    public List<DestinatarioType> getDESTINATARIOCONOSCENZA() {
        if (destinatarioconoscenza == null) {
            destinatarioconoscenza = new ArrayList<DestinatarioType>();
        }
        return this.destinatarioconoscenza;
    }

}
