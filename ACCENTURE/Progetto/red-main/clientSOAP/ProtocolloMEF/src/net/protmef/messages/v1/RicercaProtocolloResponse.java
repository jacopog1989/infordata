
package net.protmef.messages.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import net.protmef.data.protocollo.v1.ProtocolloType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EstremiProtocollo" type="{urn:protmef.net:data:protocollo:v1}ProtocolloType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "estremiProtocollo"
})
@XmlRootElement(name = "_RicercaProtocolloResponse")
public class RicercaProtocolloResponse {

    @XmlElement(name = "EstremiProtocollo")
    protected ProtocolloType estremiProtocollo;

    /**
     * Gets the value of the estremiProtocollo property.
     * 
     * @return
     *     possible object is
     *     {@link ProtocolloType }
     *     
     */
    public ProtocolloType getEstremiProtocollo() {
        return estremiProtocollo;
    }

    /**
     * Sets the value of the estremiProtocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtocolloType }
     *     
     */
    public void setEstremiProtocollo(ProtocolloType value) {
        this.estremiProtocollo = value;
    }

}
