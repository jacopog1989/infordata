
package net.protmef.data.protocollo.v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DocumentoProtocolloType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DocumentoProtocolloType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SEQU_K_DOCUMENTI_PROT" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="TipoDocumento" type="{urn:protmef.net:data:protocollo:v1}TipoDocumentoProtocolloType"/>
 *         &lt;element name="IdDocumento" type="{http://microsoft.com/wsdl/types/}guid"/>
 *         &lt;element name="FileNameDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="TitoloDocumento" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DaConvertire" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="isConvertito" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="DaInviare" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="DaFirmare" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="isFirmato" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsRiservato" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsCompleto" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="IsAttivo" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DocumentoProtocolloType", propOrder = {
    "sequkdocumentiprot",
    "tipoDocumento",
    "idDocumento",
    "fileNameDocumento",
    "titoloDocumento",
    "daConvertire",
    "isConvertito",
    "daInviare",
    "daFirmare",
    "isFirmato",
    "isRiservato",
    "isCompleto",
    "isAttivo"
})
public class DocumentoProtocolloType {

    @XmlElement(name = "SEQU_K_DOCUMENTI_PROT", required = true)
    protected BigDecimal sequkdocumentiprot;
    @XmlElement(name = "TipoDocumento", required = true)
    protected TipoDocumentoProtocolloType tipoDocumento;
    @XmlElement(name = "IdDocumento", required = true)
    protected String idDocumento;
    @XmlElement(name = "FileNameDocumento")
    protected String fileNameDocumento;
    @XmlElement(name = "TitoloDocumento")
    protected String titoloDocumento;
    @XmlElement(name = "DaConvertire")
    protected boolean daConvertire;
    protected boolean isConvertito;
    @XmlElement(name = "DaInviare")
    protected boolean daInviare;
    @XmlElement(name = "DaFirmare")
    protected boolean daFirmare;
    protected boolean isFirmato;
    @XmlElement(name = "IsRiservato")
    protected boolean isRiservato;
    @XmlElement(name = "IsCompleto")
    protected boolean isCompleto;
    @XmlElement(name = "IsAttivo")
    protected boolean isAttivo;

    /**
     * Gets the value of the sequkdocumentiprot property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSEQUKDOCUMENTIPROT() {
        return sequkdocumentiprot;
    }

    /**
     * Sets the value of the sequkdocumentiprot property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSEQUKDOCUMENTIPROT(BigDecimal value) {
        this.sequkdocumentiprot = value;
    }

    /**
     * Gets the value of the tipoDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link TipoDocumentoProtocolloType }
     *     
     */
    public TipoDocumentoProtocolloType getTipoDocumento() {
        return tipoDocumento;
    }

    /**
     * Sets the value of the tipoDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoDocumentoProtocolloType }
     *     
     */
    public void setTipoDocumento(TipoDocumentoProtocolloType value) {
        this.tipoDocumento = value;
    }

    /**
     * Gets the value of the idDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdDocumento() {
        return idDocumento;
    }

    /**
     * Sets the value of the idDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdDocumento(String value) {
        this.idDocumento = value;
    }

    /**
     * Gets the value of the fileNameDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFileNameDocumento() {
        return fileNameDocumento;
    }

    /**
     * Sets the value of the fileNameDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFileNameDocumento(String value) {
        this.fileNameDocumento = value;
    }

    /**
     * Gets the value of the titoloDocumento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitoloDocumento() {
        return titoloDocumento;
    }

    /**
     * Sets the value of the titoloDocumento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitoloDocumento(String value) {
        this.titoloDocumento = value;
    }

    /**
     * Gets the value of the daConvertire property.
     * 
     */
    public boolean isDaConvertire() {
        return daConvertire;
    }

    /**
     * Sets the value of the daConvertire property.
     * 
     */
    public void setDaConvertire(boolean value) {
        this.daConvertire = value;
    }

    /**
     * Gets the value of the isConvertito property.
     * 
     */
    public boolean isIsConvertito() {
        return isConvertito;
    }

    /**
     * Sets the value of the isConvertito property.
     * 
     */
    public void setIsConvertito(boolean value) {
        this.isConvertito = value;
    }

    /**
     * Gets the value of the daInviare property.
     * 
     */
    public boolean isDaInviare() {
        return daInviare;
    }

    /**
     * Sets the value of the daInviare property.
     * 
     */
    public void setDaInviare(boolean value) {
        this.daInviare = value;
    }

    /**
     * Gets the value of the daFirmare property.
     * 
     */
    public boolean isDaFirmare() {
        return daFirmare;
    }

    /**
     * Sets the value of the daFirmare property.
     * 
     */
    public void setDaFirmare(boolean value) {
        this.daFirmare = value;
    }

    /**
     * Gets the value of the isFirmato property.
     * 
     */
    public boolean isIsFirmato() {
        return isFirmato;
    }

    /**
     * Sets the value of the isFirmato property.
     * 
     */
    public void setIsFirmato(boolean value) {
        this.isFirmato = value;
    }

    /**
     * Gets the value of the isRiservato property.
     * 
     */
    public boolean isIsRiservato() {
        return isRiservato;
    }

    /**
     * Sets the value of the isRiservato property.
     * 
     */
    public void setIsRiservato(boolean value) {
        this.isRiservato = value;
    }

    /**
     * Gets the value of the isCompleto property.
     * 
     */
    public boolean isIsCompleto() {
        return isCompleto;
    }

    /**
     * Sets the value of the isCompleto property.
     * 
     */
    public void setIsCompleto(boolean value) {
        this.isCompleto = value;
    }

    /**
     * Gets the value of the isAttivo property.
     * 
     */
    public boolean isIsAttivo() {
        return isAttivo;
    }

    /**
     * Sets the value of the isAttivo property.
     * 
     */
    public void setIsAttivo(boolean value) {
        this.isAttivo = value;
    }

}
