
package net.protmef.data.common.v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UtenteType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UtenteType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="SEQU_K_UTENTE" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="CODICE_UTENTE" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="UTENTE" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UtenteType")
public class UtenteType {

    @XmlAttribute(name = "SEQU_K_UTENTE", required = true)
    protected BigDecimal sequkutente;
    @XmlAttribute(name = "CODICE_UTENTE")
    protected String codiceutente;
    @XmlAttribute(name = "UTENTE")
    protected String utente;

    /**
     * Gets the value of the sequkutente property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSEQUKUTENTE() {
        return sequkutente;
    }

    /**
     * Sets the value of the sequkutente property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSEQUKUTENTE(BigDecimal value) {
        this.sequkutente = value;
    }

    /**
     * Gets the value of the codiceutente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODICEUTENTE() {
        return codiceutente;
    }

    /**
     * Sets the value of the codiceutente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODICEUTENTE(String value) {
        this.codiceutente = value;
    }

    /**
     * Gets the value of the utente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUTENTE() {
        return utente;
    }

    /**
     * Sets the value of the utente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUTENTE(String value) {
        this.utente = value;
    }

}
