
package net.protmef.data.protocollo.v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import net.protmef.data.common.v1.UfficioType;
import net.protmef.data.common.v1.UfficioUtenteType;


/**
 * <p>Java class for AssegnatarioType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AssegnatarioType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ID_ASSEGNAZIONE" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="UFFICIO_ASSEGNANTE" type="{urn:protmef.net:data:common:v1}UfficioType" minOccurs="0"/>
 *         &lt;element name="ASSEGNATARIO" type="{urn:protmef.net:data:common:v1}UfficioUtenteType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AssegnatarioType", propOrder = {
    "idassegnazione",
    "ufficioassegnante",
    "assegnatario"
})
public class AssegnatarioType {

    @XmlElement(name = "ID_ASSEGNAZIONE", required = true)
    protected BigDecimal idassegnazione;
    @XmlElement(name = "UFFICIO_ASSEGNANTE")
    protected UfficioType ufficioassegnante;
    @XmlElement(name = "ASSEGNATARIO")
    protected UfficioUtenteType assegnatario;

    /**
     * Gets the value of the idassegnazione property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIDASSEGNAZIONE() {
        return idassegnazione;
    }

    /**
     * Sets the value of the idassegnazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIDASSEGNAZIONE(BigDecimal value) {
        this.idassegnazione = value;
    }

    /**
     * Gets the value of the ufficioassegnante property.
     * 
     * @return
     *     possible object is
     *     {@link UfficioType }
     *     
     */
    public UfficioType getUFFICIOASSEGNANTE() {
        return ufficioassegnante;
    }

    /**
     * Sets the value of the ufficioassegnante property.
     * 
     * @param value
     *     allowed object is
     *     {@link UfficioType }
     *     
     */
    public void setUFFICIOASSEGNANTE(UfficioType value) {
        this.ufficioassegnante = value;
    }

    /**
     * Gets the value of the assegnatario property.
     * 
     * @return
     *     possible object is
     *     {@link UfficioUtenteType }
     *     
     */
    public UfficioUtenteType getASSEGNATARIO() {
        return assegnatario;
    }

    /**
     * Sets the value of the assegnatario property.
     * 
     * @param value
     *     allowed object is
     *     {@link UfficioUtenteType }
     *     
     */
    public void setASSEGNATARIO(UfficioUtenteType value) {
        this.assegnatario = value;
    }

}
