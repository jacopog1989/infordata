
package net.protmef.messages.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import net.protmef.data.protocollo.v1.RegistrazioneProtocolloType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="EstremiProtocollo" type="{urn:protmef.net:data:protocollo:v1}RegistrazioneProtocolloType" minOccurs="0"/>
 *         &lt;element name="DATA_SPEDIZIONE" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "estremiProtocollo",
    "dataspedizione"
})
@XmlRootElement(name = "_AggiornamentoSpedizioneRequest")
public class AggiornamentoSpedizioneRequest {

    @XmlElement(name = "EstremiProtocollo")
    protected RegistrazioneProtocolloType estremiProtocollo;
    @XmlElement(name = "DATA_SPEDIZIONE", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataspedizione;

    /**
     * Gets the value of the estremiProtocollo property.
     * 
     * @return
     *     possible object is
     *     {@link RegistrazioneProtocolloType }
     *     
     */
    public RegistrazioneProtocolloType getEstremiProtocollo() {
        return estremiProtocollo;
    }

    /**
     * Sets the value of the estremiProtocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrazioneProtocolloType }
     *     
     */
    public void setEstremiProtocollo(RegistrazioneProtocolloType value) {
        this.estremiProtocollo = value;
    }

    /**
     * Gets the value of the dataspedizione property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATASPEDIZIONE() {
        return dataspedizione;
    }

    /**
     * Sets the value of the dataspedizione property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATASPEDIZIONE(XMLGregorianCalendar value) {
        this.dataspedizione = value;
    }

}
