
package net.protmef.data.protocollo.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ScaricoProtocolloType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ScaricoProtocolloType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Lavorazione"/>
 *     &lt;enumeration value="Risposta"/>
 *     &lt;enumeration value="Atti"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ScaricoProtocolloType")
@XmlEnum
public enum ScaricoProtocolloType {

    @XmlEnumValue("Lavorazione")
    LAVORAZIONE("Lavorazione"),
    @XmlEnumValue("Risposta")
    RISPOSTA("Risposta"),
    @XmlEnumValue("Atti")
    ATTI("Atti");
    private final String value;

    ScaricoProtocolloType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ScaricoProtocolloType fromValue(String v) {
        for (ScaricoProtocolloType c: ScaricoProtocolloType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
