
package net.protmef.data.common.v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RegistroType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RegistroType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="SEQU_K_REGISTRO" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="FK_AOO" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="CODICE_REGISTRO" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="ANNO_REGISTRO" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="FLAG_UFFICIALE_01" type="{http://www.w3.org/2001/XMLSchema}int" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RegistroType")
public class RegistroType {

    @XmlAttribute(name = "SEQU_K_REGISTRO", required = true)
    protected BigDecimal sequkregistro;
    @XmlAttribute(name = "FK_AOO")
    protected BigDecimal fkaoo;
    @XmlAttribute(name = "CODICE_REGISTRO")
    protected String codiceregistro;
    @XmlAttribute(name = "ANNO_REGISTRO")
    protected Integer annoregistro;
    @XmlAttribute(name = "FLAG_UFFICIALE_01")
    protected Integer flagufficiale01;

    /**
     * Gets the value of the sequkregistro property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSEQUKREGISTRO() {
        return sequkregistro;
    }

    /**
     * Sets the value of the sequkregistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSEQUKREGISTRO(BigDecimal value) {
        this.sequkregistro = value;
    }

    /**
     * Gets the value of the fkaoo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFKAOO() {
        return fkaoo;
    }

    /**
     * Sets the value of the fkaoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFKAOO(BigDecimal value) {
        this.fkaoo = value;
    }

    /**
     * Gets the value of the codiceregistro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODICEREGISTRO() {
        return codiceregistro;
    }

    /**
     * Sets the value of the codiceregistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODICEREGISTRO(String value) {
        this.codiceregistro = value;
    }

    /**
     * Gets the value of the annoregistro property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getANNOREGISTRO() {
        return annoregistro;
    }

    /**
     * Sets the value of the annoregistro property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setANNOREGISTRO(Integer value) {
        this.annoregistro = value;
    }

    /**
     * Gets the value of the flagufficiale01 property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getFLAGUFFICIALE01() {
        return flagufficiale01;
    }

    /**
     * Sets the value of the flagufficiale01 property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setFLAGUFFICIALE01(Integer value) {
        this.flagufficiale01 = value;
    }

}
