
package net.protmef.data.protocollo.v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CapitoloType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CapitoloType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="SEQU_K_CAPITOLO" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="CODICE_CAPITOLO" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="CAPITOLO" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="FK_AOO" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CapitoloType")
public class CapitoloType {

    @XmlAttribute(name = "SEQU_K_CAPITOLO", required = true)
    protected BigDecimal sequkcapitolo;
    @XmlAttribute(name = "CODICE_CAPITOLO")
    protected String codicecapitolo;
    @XmlAttribute(name = "CAPITOLO")
    protected String capitolo;
    @XmlAttribute(name = "FK_AOO")
    protected BigDecimal fkaoo;

    /**
     * Gets the value of the sequkcapitolo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSEQUKCAPITOLO() {
        return sequkcapitolo;
    }

    /**
     * Sets the value of the sequkcapitolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSEQUKCAPITOLO(BigDecimal value) {
        this.sequkcapitolo = value;
    }

    /**
     * Gets the value of the codicecapitolo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODICECAPITOLO() {
        return codicecapitolo;
    }

    /**
     * Sets the value of the codicecapitolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODICECAPITOLO(String value) {
        this.codicecapitolo = value;
    }

    /**
     * Gets the value of the capitolo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCAPITOLO() {
        return capitolo;
    }

    /**
     * Sets the value of the capitolo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCAPITOLO(String value) {
        this.capitolo = value;
    }

    /**
     * Gets the value of the fkaoo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFKAOO() {
        return fkaoo;
    }

    /**
     * Sets the value of the fkaoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFKAOO(BigDecimal value) {
        this.fkaoo = value;
    }

}
