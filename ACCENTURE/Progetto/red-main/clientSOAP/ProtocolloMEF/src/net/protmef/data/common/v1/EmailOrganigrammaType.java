
package net.protmef.data.common.v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for EmailOrganigrammaType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EmailOrganigrammaType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SEQU_K_EMAIL" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="FK_AOO" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="FK_DIREZIONE" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="FK_UFFICIO" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;element name="INDI_EMAIL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="STAT_MODALITA" type="{urn:protmef.net:data:common:v1}TipoModalitaEmail"/>
 *         &lt;element name="isUfficiale" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="isPEC" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EmailOrganigrammaType", propOrder = {
    "sequkemail",
    "fkaoo",
    "fkdirezione",
    "fkufficio",
    "indiemail",
    "statmodalita",
    "isUfficiale",
    "isPEC"
})
public class EmailOrganigrammaType {

    @XmlElement(name = "SEQU_K_EMAIL", required = true)
    protected BigDecimal sequkemail;
    @XmlElement(name = "FK_AOO", required = true)
    protected BigDecimal fkaoo;
    @XmlElement(name = "FK_DIREZIONE", required = true)
    protected BigDecimal fkdirezione;
    @XmlElement(name = "FK_UFFICIO", required = true)
    protected BigDecimal fkufficio;
    @XmlElement(name = "INDI_EMAIL")
    protected String indiemail;
    @XmlElement(name = "STAT_MODALITA", required = true)
    protected TipoModalitaEmail statmodalita;
    protected boolean isUfficiale;
    protected boolean isPEC;

    /**
     * Gets the value of the sequkemail property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSEQUKEMAIL() {
        return sequkemail;
    }

    /**
     * Sets the value of the sequkemail property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSEQUKEMAIL(BigDecimal value) {
        this.sequkemail = value;
    }

    /**
     * Gets the value of the fkaoo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFKAOO() {
        return fkaoo;
    }

    /**
     * Sets the value of the fkaoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFKAOO(BigDecimal value) {
        this.fkaoo = value;
    }

    /**
     * Gets the value of the fkdirezione property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFKDIREZIONE() {
        return fkdirezione;
    }

    /**
     * Sets the value of the fkdirezione property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFKDIREZIONE(BigDecimal value) {
        this.fkdirezione = value;
    }

    /**
     * Gets the value of the fkufficio property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFKUFFICIO() {
        return fkufficio;
    }

    /**
     * Sets the value of the fkufficio property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFKUFFICIO(BigDecimal value) {
        this.fkufficio = value;
    }

    /**
     * Gets the value of the indiemail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getINDIEMAIL() {
        return indiemail;
    }

    /**
     * Sets the value of the indiemail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setINDIEMAIL(String value) {
        this.indiemail = value;
    }

    /**
     * Gets the value of the statmodalita property.
     * 
     * @return
     *     possible object is
     *     {@link TipoModalitaEmail }
     *     
     */
    public TipoModalitaEmail getSTATMODALITA() {
        return statmodalita;
    }

    /**
     * Sets the value of the statmodalita property.
     * 
     * @param value
     *     allowed object is
     *     {@link TipoModalitaEmail }
     *     
     */
    public void setSTATMODALITA(TipoModalitaEmail value) {
        this.statmodalita = value;
    }

    /**
     * Gets the value of the isUfficiale property.
     * 
     */
    public boolean isIsUfficiale() {
        return isUfficiale;
    }

    /**
     * Sets the value of the isUfficiale property.
     * 
     */
    public void setIsUfficiale(boolean value) {
        this.isUfficiale = value;
    }

    /**
     * Gets the value of the isPEC property.
     * 
     */
    public boolean isIsPEC() {
        return isPEC;
    }

    /**
     * Sets the value of the isPEC property.
     * 
     */
    public void setIsPEC(boolean value) {
        this.isPEC = value;
    }

}
