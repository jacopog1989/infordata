
package net.protmef.data.protocollo.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProtocolloDocumentiType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProtocolloDocumentiType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DOCUMENTO_PRINCIPALE" type="{urn:protmef.net:data:protocollo:v1}DocumentoProtocolloType" minOccurs="0"/>
 *         &lt;element name="ALLEGATI" type="{urn:protmef.net:data:protocollo:v1}DocumentoProtocolloType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProtocolloDocumentiType", propOrder = {
    "documentoprincipale",
    "allegati"
})
public class ProtocolloDocumentiType {

    @XmlElement(name = "DOCUMENTO_PRINCIPALE")
    protected DocumentoProtocolloType documentoprincipale;
    @XmlElement(name = "ALLEGATI")
    protected List<DocumentoProtocolloType> allegati;

    /**
     * Gets the value of the documentoprincipale property.
     * 
     * @return
     *     possible object is
     *     {@link DocumentoProtocolloType }
     *     
     */
    public DocumentoProtocolloType getDOCUMENTOPRINCIPALE() {
        return documentoprincipale;
    }

    /**
     * Sets the value of the documentoprincipale property.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentoProtocolloType }
     *     
     */
    public void setDOCUMENTOPRINCIPALE(DocumentoProtocolloType value) {
        this.documentoprincipale = value;
    }

    /**
     * Gets the value of the allegati property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allegati property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getALLEGATI().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentoProtocolloType }
     * 
     * 
     */
    public List<DocumentoProtocolloType> getALLEGATI() {
        if (allegati == null) {
            allegati = new ArrayList<DocumentoProtocolloType>();
        }
        return this.allegati;
    }

}
