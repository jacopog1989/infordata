
package net.protmef.data.protocollo.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TipoProtocolloType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TipoProtocolloType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Tutti"/>
 *     &lt;enumeration value="Ingresso"/>
 *     &lt;enumeration value="Uscita"/>
 *     &lt;enumeration value="Mozione"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TipoProtocolloType")
@XmlEnum
public enum TipoProtocolloType {

    @XmlEnumValue("Tutti")
    TUTTI("Tutti"),
    @XmlEnumValue("Ingresso")
    INGRESSO("Ingresso"),
    @XmlEnumValue("Uscita")
    USCITA("Uscita"),
    @XmlEnumValue("Mozione")
    MOZIONE("Mozione");
    private final String value;

    TipoProtocolloType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TipoProtocolloType fromValue(String v) {
        for (TipoProtocolloType c: TipoProtocolloType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
