
package net.protmef.data.protocollo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for AnnullamentoProtocolloType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AnnullamentoProtocolloType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="PROTOCOLLO" type="{urn:protmef.net:data:protocollo:v1}RegistrazioneProtocolloType" minOccurs="0"/>
 *         &lt;element name="DATA_ANNULLAMENTO" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="NOTE_ANNULLAMENTO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PROVVEDIMENTO_ANNULLAMENTO" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AnnullamentoProtocolloType", propOrder = {
    "protocollo",
    "dataannullamento",
    "noteannullamento",
    "provvedimentoannullamento"
})
public class AnnullamentoProtocolloType {

    @XmlElement(name = "PROTOCOLLO")
    protected RegistrazioneProtocolloType protocollo;
    @XmlElement(name = "DATA_ANNULLAMENTO")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dataannullamento;
    @XmlElement(name = "NOTE_ANNULLAMENTO")
    protected String noteannullamento;
    @XmlElement(name = "PROVVEDIMENTO_ANNULLAMENTO")
    protected String provvedimentoannullamento;

    /**
     * Gets the value of the protocollo property.
     * 
     * @return
     *     possible object is
     *     {@link RegistrazioneProtocolloType }
     *     
     */
    public RegistrazioneProtocolloType getPROTOCOLLO() {
        return protocollo;
    }

    /**
     * Sets the value of the protocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrazioneProtocolloType }
     *     
     */
    public void setPROTOCOLLO(RegistrazioneProtocolloType value) {
        this.protocollo = value;
    }

    /**
     * Gets the value of the dataannullamento property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDATAANNULLAMENTO() {
        return dataannullamento;
    }

    /**
     * Sets the value of the dataannullamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDATAANNULLAMENTO(XMLGregorianCalendar value) {
        this.dataannullamento = value;
    }

    /**
     * Gets the value of the noteannullamento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNOTEANNULLAMENTO() {
        return noteannullamento;
    }

    /**
     * Sets the value of the noteannullamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNOTEANNULLAMENTO(String value) {
        this.noteannullamento = value;
    }

    /**
     * Gets the value of the provvedimentoannullamento property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPROVVEDIMENTOANNULLAMENTO() {
        return provvedimentoannullamento;
    }

    /**
     * Sets the value of the provvedimentoannullamento property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPROVVEDIMENTOANNULLAMENTO(String value) {
        this.provvedimentoannullamento = value;
    }

}
