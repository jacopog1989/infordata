
package net.protmef.messages.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import net.protmef.data.protocollo.v1.RequestCOMPETENZAPROTOCOLLOType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DatiRiassegnazione" type="{urn:protmef.net:data:protocollo:v1}RequestCOMPETENZAPROTOCOLLOType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "datiRiassegnazione"
})
@XmlRootElement(name = "_RiassegnazioneRequest")
public class RiassegnazioneRequest {

    @XmlElement(name = "DatiRiassegnazione")
    protected RequestCOMPETENZAPROTOCOLLOType datiRiassegnazione;

    /**
     * Gets the value of the datiRiassegnazione property.
     * 
     * @return
     *     possible object is
     *     {@link RequestCOMPETENZAPROTOCOLLOType }
     *     
     */
    public RequestCOMPETENZAPROTOCOLLOType getDatiRiassegnazione() {
        return datiRiassegnazione;
    }

    /**
     * Sets the value of the datiRiassegnazione property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestCOMPETENZAPROTOCOLLOType }
     *     
     */
    public void setDatiRiassegnazione(RequestCOMPETENZAPROTOCOLLOType value) {
        this.datiRiassegnazione = value;
    }

}
