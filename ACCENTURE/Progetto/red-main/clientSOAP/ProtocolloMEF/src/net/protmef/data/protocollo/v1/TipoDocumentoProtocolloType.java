
package net.protmef.data.protocollo.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TipoDocumentoProtocolloType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="TipoDocumentoProtocolloType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="DocumentoPrincipale"/>
 *     &lt;enumeration value="Allegato"/>
 *     &lt;enumeration value="Originale"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TipoDocumentoProtocolloType")
@XmlEnum
public enum TipoDocumentoProtocolloType {

    @XmlEnumValue("DocumentoPrincipale")
    DOCUMENTO_PRINCIPALE("DocumentoPrincipale"),
    @XmlEnumValue("Allegato")
    ALLEGATO("Allegato"),
    @XmlEnumValue("Originale")
    ORIGINALE("Originale");
    private final String value;

    TipoDocumentoProtocolloType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static TipoDocumentoProtocolloType fromValue(String v) {
        for (TipoDocumentoProtocolloType c: TipoDocumentoProtocolloType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
