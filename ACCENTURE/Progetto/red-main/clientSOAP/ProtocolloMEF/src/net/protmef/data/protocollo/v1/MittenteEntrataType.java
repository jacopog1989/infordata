
package net.protmef.data.protocollo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MittenteEntrataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MittenteEntrataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="PERSONA_FISICA" minOccurs="0">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="COGNOME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="NOME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="PERSONA_GIURIDICA" minOccurs="0">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="DENOMINAZIONE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MittenteEntrataType", propOrder = {
    "personafisica",
    "personagiuridica"
})
public class MittenteEntrataType {

    @XmlElement(name = "PERSONA_FISICA")
    protected MittenteEntrataType.PERSONAFISICA personafisica;
    @XmlElement(name = "PERSONA_GIURIDICA")
    protected MittenteEntrataType.PERSONAGIURIDICA personagiuridica;

    /**
     * Gets the value of the personafisica property.
     * 
     * @return
     *     possible object is
     *     {@link MittenteEntrataType.PERSONAFISICA }
     *     
     */
    public MittenteEntrataType.PERSONAFISICA getPERSONAFISICA() {
        return personafisica;
    }

    /**
     * Sets the value of the personafisica property.
     * 
     * @param value
     *     allowed object is
     *     {@link MittenteEntrataType.PERSONAFISICA }
     *     
     */
    public void setPERSONAFISICA(MittenteEntrataType.PERSONAFISICA value) {
        this.personafisica = value;
    }

    /**
     * Gets the value of the personagiuridica property.
     * 
     * @return
     *     possible object is
     *     {@link MittenteEntrataType.PERSONAGIURIDICA }
     *     
     */
    public MittenteEntrataType.PERSONAGIURIDICA getPERSONAGIURIDICA() {
        return personagiuridica;
    }

    /**
     * Sets the value of the personagiuridica property.
     * 
     * @param value
     *     allowed object is
     *     {@link MittenteEntrataType.PERSONAGIURIDICA }
     *     
     */
    public void setPERSONAGIURIDICA(MittenteEntrataType.PERSONAGIURIDICA value) {
        this.personagiuridica = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="COGNOME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="NOME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cognome",
        "nome"
    })
    public static class PERSONAFISICA {

        @XmlElement(name = "COGNOME")
        protected String cognome;
        @XmlElement(name = "NOME")
        protected String nome;

        /**
         * Gets the value of the cognome property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCOGNOME() {
            return cognome;
        }

        /**
         * Sets the value of the cognome property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCOGNOME(String value) {
            this.cognome = value;
        }

        /**
         * Gets the value of the nome property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNOME() {
            return nome;
        }

        /**
         * Sets the value of the nome property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNOME(String value) {
            this.nome = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DENOMINAZIONE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "denominazione"
    })
    public static class PERSONAGIURIDICA {

        @XmlElement(name = "DENOMINAZIONE")
        protected String denominazione;

        /**
         * Gets the value of the denominazione property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDENOMINAZIONE() {
            return denominazione;
        }

        /**
         * Sets the value of the denominazione property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDENOMINAZIONE(String value) {
            this.denominazione = value;
        }

    }

}
