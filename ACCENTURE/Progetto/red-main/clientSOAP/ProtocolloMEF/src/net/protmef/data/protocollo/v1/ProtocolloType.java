
package net.protmef.data.protocollo.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ProtocolloType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProtocolloType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="REGISTRAZIONE_PROTOCOLLO" type="{urn:protmef.net:data:protocollo:v1}RegistrazioneProtocolloType" minOccurs="0"/>
 *           &lt;element name="PROTOCOLLO_ENTRATA" type="{urn:protmef.net:data:protocollo:v1}ProtocolloEntrataType" minOccurs="0"/>
 *           &lt;element name="PROTOCOLLO_USCITA" type="{urn:protmef.net:data:protocollo:v1}ProtocolloUscitaType" minOccurs="0"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProtocolloType", propOrder = {
    "registrazioneprotocollo",
    "protocolloentrata",
    "protocollouscita"
})
public class ProtocolloType {

    @XmlElement(name = "REGISTRAZIONE_PROTOCOLLO")
    protected RegistrazioneProtocolloType registrazioneprotocollo;
    @XmlElement(name = "PROTOCOLLO_ENTRATA")
    protected ProtocolloEntrataType protocolloentrata;
    @XmlElement(name = "PROTOCOLLO_USCITA")
    protected ProtocolloUscitaType protocollouscita;

    /**
     * Gets the value of the registrazioneprotocollo property.
     * 
     * @return
     *     possible object is
     *     {@link RegistrazioneProtocolloType }
     *     
     */
    public RegistrazioneProtocolloType getREGISTRAZIONEPROTOCOLLO() {
        return registrazioneprotocollo;
    }

    /**
     * Sets the value of the registrazioneprotocollo property.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrazioneProtocolloType }
     *     
     */
    public void setREGISTRAZIONEPROTOCOLLO(RegistrazioneProtocolloType value) {
        this.registrazioneprotocollo = value;
    }

    /**
     * Gets the value of the protocolloentrata property.
     * 
     * @return
     *     possible object is
     *     {@link ProtocolloEntrataType }
     *     
     */
    public ProtocolloEntrataType getPROTOCOLLOENTRATA() {
        return protocolloentrata;
    }

    /**
     * Sets the value of the protocolloentrata property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtocolloEntrataType }
     *     
     */
    public void setPROTOCOLLOENTRATA(ProtocolloEntrataType value) {
        this.protocolloentrata = value;
    }

    /**
     * Gets the value of the protocollouscita property.
     * 
     * @return
     *     possible object is
     *     {@link ProtocolloUscitaType }
     *     
     */
    public ProtocolloUscitaType getPROTOCOLLOUSCITA() {
        return protocollouscita;
    }

    /**
     * Sets the value of the protocollouscita property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProtocolloUscitaType }
     *     
     */
    public void setPROTOCOLLOUSCITA(ProtocolloUscitaType value) {
        this.protocollouscita = value;
    }

}
