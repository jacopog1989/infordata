
package net.protmef.data.common.v1;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TitolarioType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TitolarioType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;attribute name="SEQU_K_TITOLARIO" use="required" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="FK_AOO" type="{http://www.w3.org/2001/XMLSchema}decimal" />
 *       &lt;attribute name="CODICE_TITOLARIO" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="TITOLARIO" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="DESC_PATH_TITOLARIO" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TitolarioType")
public class TitolarioType {

    @XmlAttribute(name = "SEQU_K_TITOLARIO", required = true)
    protected BigDecimal sequktitolario;
    @XmlAttribute(name = "FK_AOO")
    protected BigDecimal fkaoo;
    @XmlAttribute(name = "CODICE_TITOLARIO")
    protected String codicetitolario;
    @XmlAttribute(name = "TITOLARIO")
    protected String titolario;
    @XmlAttribute(name = "DESC_PATH_TITOLARIO")
    protected String descpathtitolario;

    /**
     * Gets the value of the sequktitolario property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSEQUKTITOLARIO() {
        return sequktitolario;
    }

    /**
     * Sets the value of the sequktitolario property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSEQUKTITOLARIO(BigDecimal value) {
        this.sequktitolario = value;
    }

    /**
     * Gets the value of the fkaoo property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFKAOO() {
        return fkaoo;
    }

    /**
     * Sets the value of the fkaoo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFKAOO(BigDecimal value) {
        this.fkaoo = value;
    }

    /**
     * Gets the value of the codicetitolario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODICETITOLARIO() {
        return codicetitolario;
    }

    /**
     * Sets the value of the codicetitolario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODICETITOLARIO(String value) {
        this.codicetitolario = value;
    }

    /**
     * Gets the value of the titolario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTITOLARIO() {
        return titolario;
    }

    /**
     * Sets the value of the titolario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTITOLARIO(String value) {
        this.titolario = value;
    }

    /**
     * Gets the value of the descpathtitolario property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDESCPATHTITOLARIO() {
        return descpathtitolario;
    }

    /**
     * Sets the value of the descpathtitolario property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDESCPATHTITOLARIO(String value) {
        this.descpathtitolario = value;
    }

}
