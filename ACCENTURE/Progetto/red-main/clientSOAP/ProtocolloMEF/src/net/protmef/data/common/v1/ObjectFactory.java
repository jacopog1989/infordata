
package net.protmef.data.common.v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the net.protmef.data.common.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: net.protmef.data.common.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EmailOrganigrammaType }
     * 
     */
    public EmailOrganigrammaType createEmailOrganigrammaType() {
        return new EmailOrganigrammaType();
    }

    /**
     * Create an instance of {@link UtenteType }
     * 
     */
    public UtenteType createUtenteType() {
        return new UtenteType();
    }

    /**
     * Create an instance of {@link CredenzialiUtenteProtocolloType }
     * 
     */
    public CredenzialiUtenteProtocolloType createCredenzialiUtenteProtocolloType() {
        return new CredenzialiUtenteProtocolloType();
    }

    /**
     * Create an instance of {@link MezzoSpedizioneType }
     * 
     */
    public MezzoSpedizioneType createMezzoSpedizioneType() {
        return new MezzoSpedizioneType();
    }

    /**
     * Create an instance of {@link TitolarioType }
     * 
     */
    public TitolarioType createTitolarioType() {
        return new TitolarioType();
    }

    /**
     * Create an instance of {@link CredenzialiSicurezzaType }
     * 
     */
    public CredenzialiSicurezzaType createCredenzialiSicurezzaType() {
        return new CredenzialiSicurezzaType();
    }

    /**
     * Create an instance of {@link RegistroType }
     * 
     */
    public RegistroType createRegistroType() {
        return new RegistroType();
    }

    /**
     * Create an instance of {@link UfficioType }
     * 
     */
    public UfficioType createUfficioType() {
        return new UfficioType();
    }

    /**
     * Create an instance of {@link UfficioUtenteType }
     * 
     */
    public UfficioUtenteType createUfficioUtenteType() {
        return new UfficioUtenteType();
    }

}
