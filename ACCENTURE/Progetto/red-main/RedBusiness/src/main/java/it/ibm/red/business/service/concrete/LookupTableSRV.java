package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.ILookupTableDAO;
import it.ibm.red.business.dto.SelectItemDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.LookupTable;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.ILookupTableSRV;

/**
 * Service per la gestione delle funzionalità associate alle liste utenti.
 * 
 * @author APerquoti
 * @author SimoneLungarella
 */
@Service
public class LookupTableSRV extends AbstractService implements ILookupTableSRV {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 7722091334783177150L;

	/**
	 * Messaggio errore recupero informazioni del selettore.
	 */
	private static final String ERROR_RECUPERO_INFO_SELETTORE_MSG = "Errore durante il recupero e la trasformazione dei valori che fanno riferimento al selettore <";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(LookupTableSRV.class.getName());

	/**
	 * Dao gestione lookupTable.
	 */
	@Autowired
	private ILookupTableDAO lookupDAO;

	/**
	 * Restituisce la lista dei selettori esistenti e validi per l'area
	 * organizzativa identificata dall' <code> idAoo </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.ILookupTableFacadeSRV#getSelectors(long)
	 */
	@Override
	public Collection<String> getSelectors(final long idAoo) {
		Collection<String> output = new ArrayList<>();
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			output = lookupDAO.getSelectorByIdAoo(idAoo, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei Selettori attivi per AOO con id <" + idAoo + ">", e);
			throw new RedException("Errore durante il recupero dei Selettori attivi per AOO con id <" + idAoo + ">", e);
		} finally {
			closeConnection(connection);
		}
		return output;
	}

	/**
	 * Restituisce i valori che può assumere il selettore identificato da
	 * <code> selector </code>.
	 * 
	 * @see it.ibm.red.business.service.ILookupTableSRV#getValues(java.lang.String,
	 *      java.util.Date, java.lang.Long, java.sql.Connection)
	 */
	@Override
	public List<SelectItemDTO> getValues(final String selector, final Date dateTarget, final Long idAoo, Connection connection) {
		final List<SelectItemDTO> output = new ArrayList<>();
		try {
			if (connection == null) {
				connection = setupConnection(getDataSource().getConnection(), false);
			}
			final Collection<LookupTable> values = lookupDAO.getValuesBySelector(selector, dateTarget, idAoo, connection);
			
			for (final LookupTable lt : values) {
				final SelectItemDTO value = new SelectItemDTO(lt.getIdLookup(), lt.getDisplayName());
				output.add(value);
			}
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_INFO_SELETTORE_MSG + selector + ">", e);
			throw new RedException(ERROR_RECUPERO_INFO_SELETTORE_MSG + selector + ">", e);
		}
		return output;
	}

	/**
	 * Recupera la collection di valori di un selettore dato il nome del selettore e
	 * l'id dell'AOO.
	 * 
	 * @param selector
	 *            Nome del selettore
	 * @param dateTarget
	 *            Data di creazione, usata per recuperare i valori creati prima di
	 *            una certa data
	 * @param forCreazione
	 * @param idAoo
	 *            Id dell'AOO, se null, vuol dire che il selettore è usato da tutte
	 *            le AOO
	 */
	@Override
	public List<SelectItemDTO> getValuesBySelector(final String selector, final Date dateTarget, final Long idAoo, Connection connection) {
		final List<SelectItemDTO> values = new ArrayList<>();
		try {
			if (connection == null) {
				connection = setupConnection(getDataSource().getConnection(), false);
			}
			final Collection<LookupTable> col = lookupDAO.getValuesBySelector(selector, dateTarget, idAoo, connection);
			
			for (final LookupTable l : col) {
				if (!"ANAGSEDEST".equals(selector)) {
					values.add(new SelectItemDTO(l.getDisplayName(), l.getValue()));
				} else {
					values.add(new SelectItemDTO(l.getValue(), l.getDisplayName()));
				}
			}
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_INFO_SELETTORE_MSG + selector + ">", e);
			throw new RedException(ERROR_RECUPERO_INFO_SELETTORE_MSG + selector + ">", e);
		}
		return values;
	}

	/**
	 * Consente di memorizzare sulla base dati le liste utenti:
	 * <code> lookups </code>.
	 * 
	 * @see it.ibm.red.business.service.ILookupTableSRV#saveLookupTables(java.util.List,
	 *      java.sql.Connection)
	 */
	@Override
	public void saveLookupTables(final List<LookupTable> lookups, Connection connection) {
		try {
			for (final LookupTable lookup : lookups) {
				lookupDAO.saveLookupTable(lookup, connection);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il salvataggio delle lookup table ", e);
			throw new RedException("Errore durante il salvataggio delle lookup table ", e);
		} 
	}

	/**
	 * Recupera le lookup table disponibili per l'aoo in ingresso.
	 * 
	 * @see it.ibm.red.business.service.facade.ILookupTableFacadeSRV#getLookupTables(java.lang.Long)
	 */
	@Override
	public Collection<LookupTable> getLookupTables(final Long idAoo) {
		Collection<LookupTable> output = new ArrayList<>();
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			output = lookupDAO.getLookupTables(idAoo, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero delle liste utenti per AOO con id <" + idAoo + ">", e);
			throw new RedException("Errore durante il recupero delle liste utenti per AOO con id <" + idAoo + ">", e);
		} finally {
			closeConnection(connection);
		}
		return output;
	}
	
	/**
	 * Restituisce true se il metadato appartiene ad una lista specifica di metadati
	 * per i quali occorre adottare il criterio "LIKE" invece che equal per
	 * effettuarne la validazione.
	 * 
	 * @param idMetadato
	 *            identificativo del metadato da valutare
	 * @param codiceAOO
	 *            codice dell'AOO
	 * @return <code> true </code> se il metadato appartiene alla lista dei metadati
	 *         specifici, <code> false </code> altrimenti
	 */
	@Override
	public boolean hasToCompareWithLike(final Integer idMetadato, final String codiceAOO) {
		String[] ids = {};
		try {
			final String idsMetadatiLike = PropertiesProvider.getIstance().getParameterByString(
					codiceAOO + "." + (PropertiesNameEnum.METADATO_LOOKUPTABLE_LIKE).getKey());
			
			ids = StringUtils.split(idsMetadatiLike, ";");
		} catch (Exception e) {
			LOGGER.error("Errore riscontrato durante il recupero degli id dei metadati per i quali la validazione è superata con criterio \"LIKE\".", e);
		}
		
		if(ids != null) {
			return Arrays.asList(ids).contains(idMetadato.toString());
		} else {
			return false;
		}
		
	}
}
