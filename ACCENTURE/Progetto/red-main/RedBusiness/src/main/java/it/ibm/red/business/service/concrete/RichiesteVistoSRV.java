/**
 * 
 */
package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dto.AssegnatarioOrganigrammaDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DetailAssegnazioneDTO;
import it.ibm.red.business.dto.DetailDocumentoDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.PEDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoStrutturaNodoEnum;
import it.ibm.red.business.enums.TipologiaNodoEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.exception.FilenetException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.TrasformPE;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IContributoSRV;
import it.ibm.red.business.service.IRichiesteVistoSRV;
import it.ibm.red.business.service.ISecuritySRV;
import it.ibm.red.business.service.ISignSRV;
import it.ibm.red.business.utils.DocumentoUtils;
import it.ibm.red.business.utils.OrganigrammaRetrieverUtils;

/**
 * @author adilegge
 *
 */
@Service
public class RichiesteVistoSRV extends AbstractService implements IRichiesteVistoSRV {

	/**
	 * La costante SerialVersionUID.
	 */
	private static final long serialVersionUID = -7319614039900345733L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RichiesteVistoSRV.class.getName());

	/**
	 * Messaggio errore processamento richiesta.
	 */
	private static final String ERROR_PROCESSAMENTO_RICHIESTA_MSG = "Errore durante il processamento della richiesta.";

	/**
	 * Messaggio errore recupero workflow.
	 */
	private static final String ERROR_RECUPERO_WORKFLOW_MSG = "Errore durante il recupero dei workflow.";

	/**
	 * Messaggio errore avanzamento workflow.
	 */
	private static final String ERROR_AVANZAMENTO_WORKFLOW_MSG = "Attenzione, avanzamento del workflow non riuscito.";

	/**
	 * Messaggio finale di operazione generica non riuscita.
	 */
	private static final String NON_RIUSCITO_LITERAL = " ) non riuscito.";

	/**
	 * Service.
	 */
	@Autowired
	private ISecuritySRV securitySRV;

	/**
	 * Service.
	 */
	@Autowired
	private ISignSRV signSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IContributoSRV contributoSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * Fornitore della properties dell'applicazione.
	 */
	private PropertiesProvider pp;

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRichiesteVistoFacadeSRV#richiediVisti(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String, java.util.Collection).
	 */
	@Override
	public EsitoOperazioneDTO richiediVisti(final UtenteDTO utente, final String wobNumber, final String motivazione,
			final Collection<AssegnazioneDTO> assegnazioneVistoList) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		Connection connection = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;

		try {
			// Fetch resource
			connection = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final VWWorkObject workObject = fpeh.getWorkFlowByWob(wobNumber, true);
			final Integer documentTitleInteger = (Integer) TrasformerPE.getMetadato(workObject, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));

			// recupero il documento corrispondente al workspace
			final DetailAssegnazioneDTO detailDocument = securitySRV.getDocumentDetailAssegnazione(fceh, utente.getIdAoo(), documentTitleInteger.toString(), true);
			esito.setIdDocumento(detailDocument.getNumeroDocumento());

			final String[] inputAssegnazioniConoscenzaArray = DocumentoUtils.initArrayAssegnazioni(TipoAssegnazioneEnum.VISTO, assegnazioneVistoList);
			final ListSecurityDTO securitiesModificaDocumento = updateSecurityFascicoloAndDocumento(utente, connection, fceh, detailDocument,
					inputAssegnazioniConoscenzaArray);

			String idUff = "";
			String idDest = "";
			for (final AssegnazioneDTO assegnazione : assegnazioneVistoList) {
				if (assegnazione.getUtente() != null) {
					idUff = assegnazione.getUtente().getIdUfficio().toString();
					idDest = assegnazione.getUtente().getId().toString();
				}
			}

			final Map<String, Object> map = new HashMap<>();
			map.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY), idDest);
			map.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY), idUff);

			map.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), motivazione);
			map.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId().toString());
			map.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_VISTI_METAKEY), inputAssegnazioniConoscenzaArray);
			fpeh.nextStep(workObject, map, ResponsesRedEnum.RICHIEDI_VISTO.getResponse());

			updateSecurityContributiAndAllacciati(utente, documentTitleInteger, inputAssegnazioniConoscenzaArray, securitiesModificaDocumento, fceh, connection);

			esito.setEsito(true);
			esito.setNote("Richiedi visto effettuata con successo.");
		} catch (final SQLException e) {
			LOGGER.error("Attenzione, errore durante la response RICHIEDI_VISTI: impossibile stabilire connessione con il database.", e);
			esito.setNote(ERROR_RECUPERO_WORKFLOW_MSG);
		} catch (final FilenetException eFile) {
			LOGGER.error("Attenzione, avanzamento del workFlow con wobNumber: " + wobNumber + " non riuscito.", eFile);
			esito.setNote(ERROR_AVANZAMENTO_WORKFLOW_MSG);
		} catch (final Exception ex) {
			LOGGER.error("Attenzione, errore durante RICHIEDI_VISTO.", ex);
			esito.setNote(ERROR_PROCESSAMENTO_RICHIESTA_MSG);
		} finally {
			closeConnection(connection);
			logoff(fpeh);
			popSubject(fceh);
		}

		return esito;
	}

	private void updateSecurityContributiAndAllacciati(final UtenteDTO utente, final Integer documentTitleInteger, final String[] inputAssegnazioniConoscenzaArray,
			final ListSecurityDTO securitiesModificaDocumento, final IFilenetCEHelper fceh, final Connection connection) {
		securitySRV.aggiornaSecurityContributiInseriti(utente, documentTitleInteger.toString(), inputAssegnazioniConoscenzaArray, fceh, connection);
		securitySRV.aggiornaSecurityDocumentiAllacciati(utente, documentTitleInteger, securitiesModificaDocumento, inputAssegnazioniConoscenzaArray, fceh, connection);
	}

	@Override
	public final Collection<EsitoOperazioneDTO> siglaERichiediVisti(final UtenteDTO utente, final Collection<String> wobNumbers, final List<AssegnazioneDTO> assegnazioniVisto,
			final String motivoAssegnazione) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();

		for (final String wobNumber : wobNumbers) {
			esiti.add(siglaERichiediVisti(utente, wobNumber, assegnazioniVisto, motivoAssegnazione));
		}

		return esiti;
	}

	@Override
	public final EsitoOperazioneDTO siglaERichiediVisti(final UtenteDTO utente, final String wobNumber, final List<AssegnazioneDTO> assegnazioniVisto,
			final String motivoAssegnazione) {
		final EsitoOperazioneDTO esito = gestisciRichiestaVisti(utente, wobNumber, ResponsesRedEnum.SIGLA_RICHIEDI_VISTI, motivoAssegnazione, assegnazioniVisto, false);
		if (esito.isEsito()) {
			esito.setNote("Sigla e richiesta visti per il documento effettuata con successo.");
		}

		return esito;
	}

	@Override
	public final EsitoOperazioneDTO richiestaVisti(final UtenteDTO utente, final String wobNumber, final String motivazione,
			final List<AssegnazioneDTO> assegnazioneVistoList, final boolean isDestDelegato) {
		return gestisciRichiestaVisti(utente, wobNumber, ResponsesRedEnum.RICHIESTA_VISTI, motivazione, assegnazioneVistoList, isDestDelegato);
	}

	@Override
	public final EsitoOperazioneDTO richiestaVisti2(final UtenteDTO utente, final String wobNumber, final String motivazione,
			final List<AssegnazioneDTO> assegnazioneVistoList, final boolean isDestDelegato) {
		return gestisciRichiestaVisti(utente, wobNumber, ResponsesRedEnum.RICHIESTA_VISTI_2, motivazione, assegnazioneVistoList, isDestDelegato);
	}

	private EsitoOperazioneDTO gestisciRichiestaVisti(final UtenteDTO utente, final String wobNumber, final ResponsesRedEnum response, final String motivazione,
			final List<AssegnazioneDTO> assegnazioneVistoList, final boolean isDestDelegato) {
		EsitoOperazioneDTO esito = null;
		Connection connection = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;

		try {
			final Collection<String> assegnazioneIspettoratoVistoList = DocumentoUtils.filterByTipoStruttura(assegnazioneVistoList, true, TipoStrutturaNodoEnum.ISPETTORATO);
			final Collection<String> assegnazioneUfficioVistoList = DocumentoUtils.filterByTipoStruttura(assegnazioneVistoList, true, TipoStrutturaNodoEnum.SERVIZIO,
					TipoStrutturaNodoEnum.SEGRETERIA, TipoStrutturaNodoEnum.SETTORE, TipoStrutturaNodoEnum.IGNOTO);
			// Fetch resource
			connection = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final VWWorkObject vwrq = fpeh.getWorkFlowByWob(wobNumber, true);
			final PEDocumentoDTO wf = TrasformPE.transform(vwrq, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO);

			if (CollectionUtils.isEmpty(assegnazioneUfficioVistoList) && CollectionUtils.isEmpty(assegnazioneIspettoratoVistoList)) {
				esito = new EsitoOperazioneDTO(wobNumber, false, "Inserire almeno un ufficio e/o un ispettorato");
			} else {
				esito = richiestaVisti(utente, wf, response, motivazione, assegnazioneVistoList, isDestDelegato, assegnazioneUfficioVistoList, assegnazioneIspettoratoVistoList, fpeh, fceh,
						connection);
			}
		} catch (final Exception e) {
			LOGGER.error("Attenzione, errore durante la response " + response.getResponse() + ": impossibile stabilire connessione DB", e);
			esito = new EsitoOperazioneDTO(wobNumber, false, ERROR_RECUPERO_WORKFLOW_MSG);
		} finally {
			closeConnection(connection);
			logoff(fpeh);
			popSubject(fceh);
		}

		return esito;
	}

	private EsitoOperazioneDTO richiestaVisti(final UtenteDTO utente, final PEDocumentoDTO wf, final ResponsesRedEnum response, final String motivazione,
			final Collection<AssegnazioneDTO> assegnazioneVistoList, final boolean isDestDelegato, final Collection<String> assegnazioneUfficioVistoList, 
			final Collection<String> assegnazioneIspettoratoVistoList, final FilenetPEHelper fpeh, final IFilenetCEHelper fceh, final Connection connection) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wf.getWobNumber());

		try {
			// recupero il documento corrispondente al workspace
			final DetailAssegnazioneDTO detailDocument = securitySRV.getDocumentDetailAssegnazione(fceh, utente.getIdAoo(), wf.getIdDocumento(), true);
			esito.setIdDocumento(detailDocument.getNumeroDocumento());

			// 6) modificare la security del fascicolo
			// //con il coordinatore il delegato è a true
			// Individuo tutte le securities
			// Su NSD il flag aggiungiDelegato e' a true ma solo per questa funzionalità
			// inoltre per i fascicoli il flag riservato e' false
			final String[] assegnazioneVistoArray = DocumentoUtils.initArrayAssegnazioni(TipoAssegnazioneEnum.VISTO, assegnazioneVistoList);
			final ListSecurityDTO securitiesModificaDocumento = updateSecurityFascicoloAndDocumento(utente, connection, fceh, detailDocument,
					assegnazioneVistoArray);

			// 9) workflowAvvio
			// // idUtente Mittente
			// // motivoAssegnazione
			// // idTipospedizione (Informazione ricavata dal workflows) se è 0 ignora
			// (Bisogna filtrare tra i valori contenuti nell'array che passano come input)
			// // valori assegnazioni se vuoti ignora
			// // valoriassegnazione ispettorati se vuoto ignora
			Map<String, Object> map = new HashMap<>();
			String motivazioneDaSalvare = motivazione;
			if(isDestDelegato) {
				if(motivazioneDaSalvare == null) {
					motivazioneDaSalvare = "";
				}
				motivazioneDaSalvare +=" L'utente delegato "+ utente.getNome()+" "+utente.getCognome()+ " ha indicato che al termine del giro visti il documento dovrà andare nel suo libro firma";
			}
			map.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), motivazioneDaSalvare);
			map.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId().toString());

			final Document documentForDetails = fceh.getDocumentRedForDetail(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), wf.getIdDocumento());
			final DetailDocumentoDTO documentDetailDTO = TrasformCE.transform(documentForDetails, TrasformerCEEnum.FROM_DOCUMENTO_TO_DETAIL);
			final int tipoSpedizione = signSRV.getTipoSpedizione(wf, documentDetailDTO);
			if (tipoSpedizione >= 0) {
				final Integer tipoSpedizioneInteger = Integer.valueOf(tipoSpedizione);
				map.put(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_SPEDIZIONE_METAKEY), tipoSpedizioneInteger.toString());
			}
			if (CollectionUtils.isNotEmpty(assegnazioneUfficioVistoList)) {
				String[] assegnazioneUfficioVistoArray = new String[assegnazioneUfficioVistoList.size()];
				assegnazioneUfficioVistoArray = assegnazioneUfficioVistoList.toArray(assegnazioneUfficioVistoArray);
				map.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_UFFICI_VISTI_METAKEY), assegnazioneUfficioVistoArray);
			}
			if (CollectionUtils.isNotEmpty(assegnazioneIspettoratoVistoList)) {
				String[] assegnazioneIspettoratoVistoArray = new String[assegnazioneIspettoratoVistoList.size()];
				assegnazioneIspettoratoVistoArray = assegnazioneIspettoratoVistoList.toArray(assegnazioneIspettoratoVistoArray);
				map.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_VISTI_ISPETTORATI_METAKEY), assegnazioneIspettoratoVistoArray);
			}
			fpeh.nextStep(wf.getWobNumber(), map, response.getResponse());
			

			// Logica che in base alla scelta dell'utente aggiorna il WF con le informazioni necessarie 
			// per determinare a CHI tornra il documento al termine del giro visti.
			if (isDestDelegato) {
				map = new HashMap<>();
				map.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DELEGATO_VISTO_WF_METAKEY), utente.getId().intValue());
				VWWorkObject worfklowToUpdate = fpeh.getWorkFlowByWob(wf.getWobNumber(), true);
				fpeh.updateWorkflow(worfklowToUpdate, map);
			}
			
			//
			// 10) aggiorna secury documenti allacciati
			final Integer idDocumentoDb = Integer.parseInt(wf.getIdDocumento());
			updateSecurityContributiAndAllacciati(utente, idDocumentoDb, assegnazioneVistoArray, securitiesModificaDocumento, fceh, connection);

			esito.setEsito(true);
		} catch (final FilenetException eFile) {
			LOGGER.error("Attenzione, avanzamento del workflow errore durante " + response.getResponse() + " (wobNumber: " + wf.getWobNumber() + NON_RIUSCITO_LITERAL, eFile);
			esito.setNote("Attenzione, avanzamento del workflow non riuscito");
			esito.setEsito(false);
		} catch (final Exception ex) {
			LOGGER.error("Attenzione, errore durante " + response.getResponse(), ex);
			esito.setNote("Errore durante il processamento della richiesta");
			esito.setEsito(false);
		}
		return esito;
	}

	/**
	 * Avanza il workflow per richiedi visto ispettore
	 * 
	 * @param utente
	 *            mittente
	 * 
	 * @param wobNumbers
	 *            lista dei workflow selezionati
	 * 
	 * @param motivazione
	 *            motivazione addotta dall'utente
	 * 
	 * @return l'esito dell'operazione effetuata per ciascun workflow
	 */
	@Override
	public EsitoOperazioneDTO richiediVistoIspettore(final UtenteDTO utente, final String wobNumber, final String motivazione) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;

		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final VWWorkObject workObject = fpeh.getWorkFlowByWob(wobNumber, true);
			final Integer documentTitle = (Integer) TrasformerPE.getMetadato(workObject, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
			final DetailAssegnazioneDTO detailDocument = securitySRV.getDocumentDetailAssegnazione(fceh, utente.getIdAoo(), documentTitle.toString(), false);

			final Map<String, Object> map = new HashMap<>();
			map.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), motivazione);
			map.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId().toString());
			fpeh.nextStep(workObject, map, ResponsesRedEnum.RICHIEDI_VISTO_ISPETTORE.getResponse());

			esito.setEsito(true);
			esito.setIdDocumento(detailDocument.getNumeroDocumento());
			esito.setNote("Richiesta visto all'Ispettore effettuata con successo.");
		} catch (final FilenetException eFile) {
			LOGGER.error("Attenzione, avanzamento del workFlow errore durante RICHIEDI_VISTO_ISPETTORE ( wobNumber: " + wobNumber + NON_RIUSCITO_LITERAL, eFile);
			esito.setNote(ERROR_AVANZAMENTO_WORKFLOW_MSG);
		} catch (final Exception ex) {
			LOGGER.error("Attenzione, errore durante RICHIEDI_VISTO_ISPETTORE.", ex);
			esito.setNote(ERROR_PROCESSAMENTO_RICHIESTA_MSG);
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}

		return esito;
	}

	@Override
	public final EsitoOperazioneDTO richiediVistoUfficio(final UtenteDTO utente, final String wobNumber, final String motivazione,
			final List<AssegnatarioOrganigrammaDTO> assegnazioneUfficioList) {
		final ResponsesRedEnum response = ResponsesRedEnum.RICHIEDI_VISTO_UFFICIO;
		return richiediVistoUfficioInterno(utente, wobNumber, motivazione, assegnazioneUfficioList, response);
	}

	private EsitoOperazioneDTO richiediVistoUfficioInterno(final UtenteDTO utente, final String wobNumber, final String motivazione,
			final List<AssegnatarioOrganigrammaDTO> assegnazioneUfficioList, final ResponsesRedEnum response) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		Connection connection = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final VWWorkObject workObject = fpeh.getWorkFlowByWob(wobNumber, true);
			final Integer documentTitleInteger = (Integer) TrasformerPE.getMetadato(workObject, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
			final DetailAssegnazioneDTO detailDocument = securitySRV.getDocumentDetailAssegnazione(fceh, utente.getIdAoo(), documentTitleInteger.toString(), true);
			// Gestione coordinatore ##################################################

			// Modifica delle security sul fascicolo
			final String[] assegnazioneVistoArray = OrganigrammaRetrieverUtils.initArrayAssegnazioni(assegnazioneUfficioList);
			final ListSecurityDTO securitiesModificaDocumento = updateSecurityFascicoloAndDocumento(utente, connection, fceh, detailDocument,
					assegnazioneVistoArray);

			// Avvio Workflow su filenet
			final String[] elencoVistiUfficiArray = OrganigrammaRetrieverUtils.initArrayUfficio(assegnazioneUfficioList);

			final Map<String, Object> map = new HashMap<>();
			map.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), motivazione);
			map.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId().toString());
			map.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_UFFICI_VISTI_METAKEY), elencoVistiUfficiArray);
			fpeh.nextStep(workObject, map, response.getResponse());

			updateSecurityContributiAndAllacciati(utente, documentTitleInteger, assegnazioneVistoArray, securitiesModificaDocumento, fceh, connection);

			esito.setEsito(true);
			esito.setIdDocumento(detailDocument.getNumeroDocumento());
			esito.setNote("Richiedi visto a Ufficio effettuato con successo.");
		} catch (final FilenetException eFile) {
			LOGGER.error("Attenzione, avanzamento del workFlow errore durante RICHIEDI_VISTO_UFFICIO ( wobNumber: " + wobNumber + NON_RIUSCITO_LITERAL, eFile);
			esito.setNote(ERROR_AVANZAMENTO_WORKFLOW_MSG);
		} catch (final SQLException e) {
			LOGGER.error("Attenzione, errore durante la response RICHIEDI_VISTO_UFFICIO ( wobNumber: " + wobNumber + " ): impossibile stabilire connessione con il database",
					e);
			esito.setNote(ERROR_RECUPERO_WORKFLOW_MSG);
		} catch (final Exception ex) {
			LOGGER.error("Attenzione, errore durante RICHIEDI_VISTO_ISPETTORE.", ex);
			esito.setNote(ERROR_PROCESSAMENTO_RICHIESTA_MSG);
		} finally {
			closeConnection(connection);
			logoff(fpeh);
			popSubject(fceh);
		}

		return esito;
	}

	private ListSecurityDTO updateSecurityFascicoloAndDocumento(final UtenteDTO utente, final Connection connection, final IFilenetCEHelper fceh,
			final DetailAssegnazioneDTO detailDocument, final String[] assegnazioneVistoArray) {
		final String assegnazioneCoordinatore = detailDocument.getAssegnazionePerCoordinatore();
		final String documentTitle = detailDocument.getDocumentTitle();

		final boolean trueAggiungiDelegato = true;
		final boolean isDocumentoFascicolo = false;
		final boolean isRiservatoFascicolo = false;
		final ListSecurityDTO securitiesModificaFascicolo = securitySRV.getSecurityPerRiassegnazione(utente, documentTitle, assegnazioneVistoArray,
				assegnazioneCoordinatore, trueAggiungiDelegato, isDocumentoFascicolo, isRiservatoFascicolo, false, connection);
		// Imposto l'utente alle security dei fascicoli
		securitySRV.verificaSecurities(securitiesModificaFascicolo, connection);

		securitySRV.modificaSecurityFascicoli(utente, securitiesModificaFascicolo, documentTitle);

		// Modifica delle security sul documento
		// Ottieni le security
		final boolean isDocumento = true;
		final boolean aggiungiUfficioFalse = false;
		final boolean aggiungiDelelegatoTrue = true;
		final ListSecurityDTO securitiesModificaDocumento = securitySRV.getSecurityPerAddAssegnazione(utente, documentTitle, utente.getId(),
				utente.getIdUfficio(), assegnazioneVistoArray, isDocumento, aggiungiUfficioFalse, detailDocument.isRiservato(), aggiungiDelelegatoTrue,
				assegnazioneCoordinatore, connection);

		// modifica security documento
		final boolean allVersionFalse = false;
		final boolean securityToOverrideFalse = false;
		securitySRV.aggiornaEVerificaSecurityDocumento(documentTitle, utente.getIdAoo(), securitiesModificaDocumento, allVersionFalse, securityToOverrideFalse, fceh,
				connection);
		return securitiesModificaDocumento;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRichiesteVistoFacadeSRV#richiediVistoInterno(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String, java.util.List).
	 */
	@Override
	public EsitoOperazioneDTO richiediVistoInterno(final UtenteDTO utente, final String wobNumber, final String motivazione,
			final List<AssegnatarioOrganigrammaDTO> assegnazioneUfficioList) {
		final ResponsesRedEnum response = ResponsesRedEnum.RICHIEDI_VISTO_INTERNO;
		return richiediVistoUfficioInterno(utente, wobNumber, motivazione, assegnazioneUfficioList, response);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRichiesteVistoFacadeSRV#richiediVerifica(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection,
	 *      it.ibm.red.business.dto.AssegnatarioOrganigrammaDTO, java.lang.String).
	 */
	@Override
	public final Collection<EsitoOperazioneDTO> richiediVerifica(final UtenteDTO utente, final Collection<String> wobNumbers,
			final AssegnatarioOrganigrammaDTO inputAssegnazioneVerifica, final String motivoAssegnazione) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();

		for (final String wobNumber : wobNumbers) {
			esiti.add(richiediVerifica(utente, wobNumber, inputAssegnazioneVerifica, motivoAssegnazione));
		}

		return esiti;
	}

	@Override
	public final EsitoOperazioneDTO richiediVerifica(final UtenteDTO utente, final String wobNumber, final AssegnatarioOrganigrammaDTO inputAssegnazioneVerifica,
			final String motivoAssegnazione) {
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		Connection connection = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);
			final Integer idDocumento = (Integer) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));

			if (idDocumento != null && inputAssegnazioneVerifica != null) {
				final String idDocumentoString = String.valueOf(idDocumento);

				// Si inserisce l'assegnazione per verifica in input in un array
				final String[] inputAssegnazioneVerificaArray = new String[1];
				inputAssegnazioneVerificaArray[0] = OrganigrammaRetrieverUtils.getArrayAssegnazione(inputAssegnazioneVerifica);

				final DetailAssegnazioneDTO detailDocument = securitySRV.getDocumentDetailAssegnazione(fceh, utente.getIdAoo(), idDocumentoString, true);
				final boolean isRiservato = Boolean.TRUE.equals(detailDocument.isRiservato());
				final String assegnazioneCoordinatore = detailDocument.getAssegnazionePerCoordinatore();

				// ### Si modificano le security dei fascicoli in cui è presente il documento
				final boolean esitoModificaSecurityFascicoli = securitySRV.modificaSecurityFascicoli(idDocumentoString, utente, inputAssegnazioneVerificaArray,
						assegnazioneCoordinatore, true, connection);

				if (esitoModificaSecurityFascicoli) {
					// ### Si ottengono le nuove security...
					final ListSecurityDTO securityDoc = securitySRV.getSecurityPerAddAssegnazione(utente, idDocumentoString, utente.getId(),
							utente.getIdUfficio(), inputAssegnazioneVerificaArray, true, false, isRiservato, true, assegnazioneCoordinatore, connection);

					// ### ...e si usano per aggiornare le security del documento
					securitySRV.aggiornaEVerificaSecurityDocumento(idDocumentoString, utente.getIdAoo(), securityDoc, false, false, fceh, connection);

					final Map<String, Object> metadatiWorkflow = new HashMap<>();
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY), inputAssegnazioneVerifica.getIdNodo());
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY), inputAssegnazioneVerifica.getIdUtente());
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), motivoAssegnazione);
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId());

					// ### Si avanza il workflow su FileNet
					final boolean avanzamentoWorkflowOk = fpeh.nextStep(wobNumber, metadatiWorkflow, ResponsesRedEnum.RICHIEDI_VERIFICA.getResponse());

					if (avanzamentoWorkflowOk) {
						// Si aggiornano le security dei documenti allacciati
						securitySRV.aggiornaSecurityDocumentiAllacciati(utente, idDocumento, securityDoc, inputAssegnazioneVerificaArray, fceh, connection);

						// Si aggiornano le security dei contributi (interni)
						securitySRV.aggiornaSecurityContributiInseriti(utente, idDocumentoString, inputAssegnazioneVerificaArray, fceh, connection);

						esito = new EsitoOperazioneDTO(null, null, detailDocument.getNumeroDocumento(), wobNumber);
						esito.setNote("Richiesta verifica effettuata con successo");
						LOGGER.info("Inserimento di nuovi uffici/utenti in contributo eseguito correttamente per il documento: " + idDocumentoString);
					} else {
						final String errore = "Errore durante l'avanzamento del workflow: " + wobNumber + " per il documento: " + idDocumentoString + " con la response: "
								+ ResponsesRedEnum.RICHIEDI_VERIFICA.name();
						LOGGER.error(errore);
						throw new FilenetException(errore);
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la richiesta di verifica del visto per il workflow: " + wobNumber, e);
			rollbackConnection(connection);
			// Si genera l'esito in caso di eccezione
			esito.setNote("Errore durante la richiesta di verifica del visto per il workflow:" + wobNumber + ".\nErrore: " + e.getMessage() + "\n");
		} finally {
			closeConnection(connection);
			logoff(fpeh);
			popSubject(fceh);
		}

		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRichiesteVistoFacadeSRV#getUfficioContributi(java.lang.String,
	 *      java.lang.String, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Set<NodoOrganigrammaDTO> getUfficioContributi(final String wobNumber, final String documentTitle, final UtenteDTO utente) {
		// Recuperare le assegnazioni per i contributi dallo storico e non
		// Per i nodi assegnatari che non appartengono all'ispettorato dell'utente
		// Recuperare il nodo assegnatario dell'ispettorato.
		FilenetPEHelper fpeh = null;
		Connection connection = null;
		final Set<NodoOrganigrammaDTO> assegnazioniList = new HashSet<>();
		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			connection = setupConnection(getDataSource().getConnection(), false);
			final Set<AssegnatarioOrganigrammaDTO> assegnatari = new HashSet<>();

			final List<AssegnatarioOrganigrammaDTO> assegnatariDocumento = contributoSRV.getAssegnazioniContributi(wobNumber, fpeh);
			if (CollectionUtils.isNotEmpty(assegnatariDocumento)) {
				assegnatari.addAll(assegnatariDocumento);
			}

			final Integer documentTitleInInt = Integer.parseInt(documentTitle);
			final List<AssegnatarioOrganigrammaDTO> assegnatariAllacci = contributoSRV.getAssegnazioniContributiAllaccio(documentTitleInInt, utente, fpeh, connection);
			if (CollectionUtils.isNotEmpty(assegnatariAllacci)) {
				assegnatari.addAll(assegnatariAllacci);
			}

			// Pulisco gli uffici e tutti eventuali dati sporchi
			final Set<AssegnatarioOrganigrammaDTO> onlyUfficioList = assegnatari.stream()
					.filter(a -> a != null && a.getIdNodo() != null && a.getIdNodo() > 0L && a.isUfficio()).collect(Collectors.toSet());

			if (CollectionUtils.isNotEmpty(onlyUfficioList)) {
				final Integer idNodoSegreteria = nodoDAO.getSegreteriaDirezione(utente.getIdUfficio().intValue(), connection);

				for (final AssegnatarioOrganigrammaDTO assegnatario : onlyUfficioList) {
					NodoOrganigrammaDTO nodoCorrente;

					final Integer idNodoSegreteriaCorrente = nodoDAO.getSegreteriaDirezione(assegnatario.getIdNodo().intValue(), connection);
					if (idNodoSegreteria.equals(idNodoSegreteriaCorrente)) {
						// L'ufficio da inserire è proprio il corrente
						nodoCorrente = getNodoOrganigrammaDTO(assegnatario.getIdNodo(), connection);
					} else {
						// Ottieni l'ispettorato del nodo in esame
						final Long idNodoSegreteriaCorrenteLong = Long.valueOf(idNodoSegreteriaCorrente);
						nodoCorrente = getNodoOrganigrammaDTO(idNodoSegreteriaCorrenteLong, connection);
					}
					assegnazioniList.add(nodoCorrente);
				}

			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei contributi per il workflow: " + wobNumber, e);
			throw new RedException("Errore durante il recupero dei contributi");
		} finally {
			closeConnection(connection);
			logoff(fpeh);
		}

		return assegnazioniList;
	}

	private NodoOrganigrammaDTO getNodoOrganigrammaDTO(final Long idNodo, final Connection connection) {
		final Nodo segreteria = nodoDAO.getNodo(idNodo, connection);
		// Nodo riempito di valori di default che ha il solo scopo di far funzionare la
		// visualizzazione dell'organigramma sono dei valori mockati quello che è di
		// interesse per la funzionalità.
		// corrisponde esclusivamente a idNodo.
		return new NodoOrganigrammaDTO(segreteria.getIdNodo(), segreteria.getAoo().getIdAoo(), segreteria.getIdNodoPadre(), segreteria.getCodiceNodo(),
				segreteria.getDescrizione(), TipologiaNodoEnum.get(segreteria.getIdTipoNodo()), TipoStrutturaNodoEnum.get(segreteria.getIdTipoStruttura()),
				segreteria.getFlagSegreteria() == 1, 0, 0, null, null, null, null, null, null, null);

	}
}