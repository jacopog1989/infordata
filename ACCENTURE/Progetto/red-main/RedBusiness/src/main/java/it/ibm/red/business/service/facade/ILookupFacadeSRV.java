package it.ibm.red.business.service.facade;

import java.io.Serializable;

import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.Ruolo;

/**
 * The Interface ILookupFacadeSRV.
 *
 * @author CPIERASC
 * 
 *         Facade lookup service.
 */
public interface ILookupFacadeSRV extends Serializable {
	
	/**
	 * Recupero ufficio per id.
	 * 
	 * @param id	identificativo ufficio
	 * @return		ufficio
	 */
	Nodo getUfficio(Long id);

	/**
	 * 
	 * Recupero ruolo per id.
	 * 
	 * @param id	identificativo
	 * @return		euolo
	 */
	Ruolo getRuolo(Long id);

}
