/**
 * @author CPIERASC
 *
 *	Questo package conterrà l'helper per gestire documenti in formato PDF. La firma PADES è demandata a PK, ma tutte le
 *	manipolazioni (come ad esempio il conteggio dei campi firma o la creazione di PDF on-the-fly), fatta eccezione per 
 *	la conversione, sono eseguite attraverso l'helper contenuto in questo package.
 *	
 */
package it.ibm.red.business.helper.pdf;
