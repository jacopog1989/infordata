package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IRegioneFacadeSRV;

/**
 * Interfaccia del servizio di gestione regioni.
 */
public interface IRegioneSRV extends IRegioneFacadeSRV {

}
