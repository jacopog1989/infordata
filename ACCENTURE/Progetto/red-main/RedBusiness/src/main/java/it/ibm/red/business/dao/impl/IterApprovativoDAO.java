package it.ibm.red.business.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IIterApprovativoDAO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.exception.RedException;

/**
 * 
 * Dao gestione iter approvativo.
 * 
 * @author CRISTIANOPierascenzi
 *
 */
@Repository
public class IterApprovativoDAO extends AbstractDAO implements IIterApprovativoDAO {

	/**
	 * Messaggio errore recupero workflow.
	 */
	private static final String ERROR_RECUPERO_WORKFLOW_MSG = "Errore durante il recupero del nome del workflow ";
	
	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Gets the WF name.
	 *
	 * @param idIterApprovativo
	 *            the id iter approvativo
	 * @param con
	 *            the con
	 * @return the WF name
	 */
	@Override
	public final String getWFName(final int idIterApprovativo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String output = null;
		try {
			String querySQL = " SELECT wf.WORKFLOWNAME " + " FROM iterapprovativo ia, workflow wf "
					+ " WHERE ia.iditerapprovativo = ? " + " AND ia.idworkflow = wf.idworkflow";
			ps = con.prepareStatement(querySQL);
			ps.setInt(1, idIterApprovativo);
			rs = ps.executeQuery();
			while (rs.next()) {
				output = rs.getString("WORKFLOWNAME");
			}
		} catch (SQLException e) {
			throw new RedException(ERROR_RECUPERO_WORKFLOW_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}

	/**
	 * Gets the WF name Generico Entrata.
	 *
	 * @param idIterApprovativo
	 *            the id iter approvativo
	 * @param con
	 *            the con
	 * @return the WF name
	 */
	@Override
	public final String getWFNameGenerico(final long idAoo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String output = null;
		try {
			String querySQL = "select workflowname from workflow where FLAGGENERICOENTRATA=1 and idaoo = ?";
			ps = con.prepareStatement(querySQL);
			ps.setLong(1, idAoo);
			rs = ps.executeQuery();
			while (rs.next()) {
				output = rs.getString("WORKFLOWNAME");
			}
		} catch (SQLException e) {
			throw new RedException(ERROR_RECUPERO_WORKFLOW_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}

	@Override
	public final Integer getTipoFirma(final Integer idAoo, final String wfName, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer output = null;
		try {
			String querySQL = "SELECT distinct ia.TIPOFIRMA FROM iterapprovativo ia, workflow wf "
					+ "WHERE ia.idworkflow = wf.idworkflow AND " + "wf.IDAOO = ? AND " + "wf.WORKFLOWNAME = ? ";
			ps = con.prepareStatement(querySQL);
			ps.setInt(1, idAoo);
			ps.setString(2, wfName);
			rs = ps.executeQuery();
			while (rs.next()) {
				output = rs.getInt("TIPOFIRMA");
			}
		} catch (SQLException e) {
			throw new RedException("Errore durante il recupero del tipo firma dell'iter approvativo. ", e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IIterApprovativoDAO#getFirmatario(java.lang.Long,
	 *      java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public String getFirmatario(final Long idNodo, final Integer tipoFirma, final Connection connection) {
		CallableStatement cs = null;
		String result = null;
		try {
			int index = 0;
			cs = connection.prepareCall("{call PKG_NSD_PE_RGS.getfirmatario(?,?,?,?)}");
			cs.setInt(++index, idNodo.intValue());
			cs.setInt(++index, tipoFirma);
			cs.registerOutParameter(++index, Types.INTEGER);
			cs.registerOutParameter(++index, Types.INTEGER);
			cs.execute();
			result = cs.getInt(index - 1) + "," + cs.getInt(index);
			return result;
		} catch (SQLException e) {
			throw new RedException(
					"Errore durante il recupero del firmatario per l'iter " + tipoFirma + " ed il nodo " + idNodo, e);
		} finally {
			closeStatement(cs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IIterApprovativoDAO#getAllByIdAOO(java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public List<IterApprovativoDTO> getAllByIdAOO(final Integer idAoo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<IterApprovativoDTO> list = null;
		try {
			String querySQL = " SELECT ia.iditerapprovativo, " 
					+ "       ia.descrizione, " 
					+ "       ia.info, "
					+ "       ia.tipofirma, " 
					+ "       ia.parziale, " 
					+ "       iaa.idaoo, " 
					+ "       wf.idworkflow, "
					+ "       wf.workflowname, " 
					+ "       wf.workflowdescription, " 
					+ "       wf.flaggenericoentrata, "
					+ "       wf.flaggenericouscita "
					+ "  FROM iterapprovativo ia, iterapprovativoaoo iaa, workflow wf "
					+ " WHERE ia.iditerapprovativo = iaa.iditerapprovativo " 
					+ "   AND ia.idworkflow = wf.idworkflow "
					+ "   AND ia.descrizione != 'Firma Ministro'" 
					+ "   AND iaa.idaoo = ? "
					+ " ORDER BY ia.iditerapprovativo ";
			ps = con.prepareStatement(querySQL);
			ps.setInt(1, idAoo);
			rs = ps.executeQuery();
			list = new ArrayList<>();
			while (rs.next()) {
				list.add(new IterApprovativoDTO(rs.getInt("iditerapprovativo"), rs.getString("descrizione"),
						rs.getString("info"), rs.getInt("tipofirma"), rs.getInt("parziale"), rs.getInt("idaoo"),
						rs.getInt("idworkflow"), rs.getString("workflowname"), rs.getString("workflowdescription"),
						rs.getInt("flaggenericoentrata") == 1, rs.getInt("flaggenericouscita") == 1));
			}
		} catch (SQLException e) {
			throw new RedException(ERROR_RECUPERO_WORKFLOW_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		return list;
	}

	/**
	 * @see it.ibm.red.business.dao.IIterApprovativoDAO#getIterApprovativoById(java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public IterApprovativoDTO getIterApprovativoById(final Integer id, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		IterApprovativoDTO item = null;
		try {
			String querySQL = " SELECT ia.iditerapprovativo, " + "       ia.descrizione, " + "       ia.info, "
					+ "       ia.tipofirma, " + "       ia.parziale, " + "       iaa.idaoo, " + "       wf.idworkflow, "
					+ "       wf.workflowname, " + "       wf.workflowdescription, " + "       wf.flaggenericoentrata, "
					+ "       wf.flaggenericouscita "
					+ "  FROM iterapprovativo ia, iterapprovativoaoo iaa, workflow wf "
					+ " WHERE ia.iditerapprovativo = iaa.iditerapprovativo " + "   AND ia.idworkflow = wf.idworkflow "
					+ "   AND iaa.iditerapprovativo = ? " + " ORDER BY ia.iditerapprovativo ";
			ps = con.prepareStatement(querySQL);
			ps.setInt(1, id);
			rs = ps.executeQuery();
			if (rs.next()) {
				item = new IterApprovativoDTO(rs.getInt("iditerapprovativo"), rs.getString("descrizione"),
						rs.getString("info"), rs.getInt("tipofirma"), rs.getInt("parziale"), rs.getInt("idaoo"),
						rs.getInt("idworkflow"), rs.getString("workflowname"), rs.getString("workflowdescription"),
						rs.getInt("flaggenericoentrata") == 1, rs.getInt("flaggenericouscita") == 1);
			}
		} catch (SQLException e) {
			throw new RedException(ERROR_RECUPERO_WORKFLOW_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		return item;
	}

}
