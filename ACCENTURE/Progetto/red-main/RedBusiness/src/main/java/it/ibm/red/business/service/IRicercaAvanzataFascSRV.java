package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IRicercaAvanzataFascFacadeSRV;

/**
 * The Interface IRicercaAvanzataFascSRV.
 *
 * @author m.crescentini
 * 
 *         Interfaccia del servizio per la gestione della ricerca avanzata dei fascicoli.
 */
public interface IRicercaAvanzataFascSRV extends IRicercaAvanzataFascFacadeSRV {
	
	
}