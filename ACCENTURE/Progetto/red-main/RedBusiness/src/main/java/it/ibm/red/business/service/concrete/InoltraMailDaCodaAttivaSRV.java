package it.ibm.red.business.service.concrete;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants.AzioneMail;
import it.ibm.red.business.constants.Constants.TipologiaInvio;
import it.ibm.red.business.dao.impl.TestoPredefinitoDAO;
import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.TestoPredefinitoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TestoPredefinitoEnum;
import it.ibm.red.business.enums.TipoRubricaEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.ICodaMailSRV;
import it.ibm.red.business.service.IInoltraMailDaCodaAttivaSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * Service inolto mail da coda attiva.
 */
@Service
public class InoltraMailDaCodaAttivaSRV extends AbstractService implements IInoltraMailDaCodaAttivaSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 1575542012423781112L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(InoltraMailDaCodaAttivaSRV.class.getName());

	/**
	 * DAO.
	 */
	@Autowired
	private TestoPredefinitoDAO testoPredefinitoDAO;

	/**
	 * Service.
	 */
	@Autowired
	private ICodaMailSRV codaMailSRV;

	/**
	 * @see it.ibm.red.business.service.facade.IInoltraMailDaCodaAttivaFacadeSRV#getTestiPredefiniti(it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public List<TestoPredefinitoDTO> getTestiPredefiniti(final UtenteDTO utente) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return testoPredefinitoDAO.getTestiByTipoAndAOO(TestoPredefinitoEnum.TESTO_PREDEFINITO_INOLTRO_MAIL_DA_CODA_ATTIVA.getId(), utente.getIdAoo(), connection);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dei testi predefiniti", e);
			rollbackConnection(connection);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IInoltraMailDaCodaAttivaFacadeSRV#inviaMail(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, it.ibm.red.business.dto.CasellaPostaDTO,
	 *      java.util.List, java.lang.String, java.lang.String, java.lang.Integer,
	 *      java.util.List).
	 */
	@Override
	public EsitoOperazioneDTO inviaMail(final UtenteDTO utente, final String wobNumber, final CasellaPostaDTO mittente, final List<DestinatarioRedDTO> destinatari,
			final String oggettoMail, final String testoMail, final Integer modalitaSpedizioneSc, final List<DocumentoAllegabileDTO> docSelezionati) {

		Connection con = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final VWWorkObject wo = fpeh.getWorkFlowByWob(wobNumber, true);
			final String documentTitle = TrasformerPE.getMetadato(wo, PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY).toString();

			final Document document = fceh.getDocumentByDTandAOO(documentTitle, utente.getIdAoo());
			final String guid = document.get_Id().toString();

			final List<String> destinatariStringListTo = new ArrayList<>();
			final List<String> destinatariStringListCc = new ArrayList<>();
			final StringBuilder destinatariMailTo = new StringBuilder();
			final StringBuilder destinatariMailCc = new StringBuilder();

			// Creazione stringhe con mail destinatari TO/CC separati da ";" e stringhe con
			// info complete destinatari
			for (final DestinatarioRedDTO destinatario : destinatari) {
				final Contatto contatto = destinatario.getContatto();
				if (!ModalitaDestinatarioEnum.CC.equals(destinatario.getModalitaDestinatarioEnum())) {
					if (destinatariMailTo.length() > 0) {
						destinatariMailTo.append(";");
					}
					destinatariMailTo.append(contatto.getMailSelected());
				} else {
					if (destinatariMailCc.length() > 0) {
						destinatariMailCc.append(";");
					}
					destinatariMailCc.append(contatto.getMailSelected());
				}

				final StringBuilder destinatarioFullString = new StringBuilder().append(contatto.getContattoID()).append(",").append(contatto.getMailSelected());

				if (!ModalitaDestinatarioEnum.CC.equals(destinatario.getModalitaDestinatarioEnum())) {
					// es. 298434,direzione.uslnordovest@postacert.toscana.it,2,E,TO
					destinatarioFullString.append(",2,E,TO");
					destinatariStringListTo.add(destinatarioFullString.toString());
				} else {
					// es. 298434,direzione.uslnordovest@postacert.toscana.it,2,E,CC
					destinatarioFullString.append(",2,E,CC");
					destinatariStringListCc.add(destinatarioFullString.toString());
				}
			}

			final List<String> destinatariStringList = new ArrayList<>();
			destinatariStringList.addAll(destinatariStringListTo);
			destinatariStringList.addAll(destinatariStringListCc);

			// Inserimento dei Document Title dei documenti selezionati nel Messaggio
			final List<String> allegatiMailDocTitle = new ArrayList<>();
			for (final DocumentoAllegabileDTO docSelezionato : docSelezionati) {
				// Si inserisce come prefisso del Document Title la tipologia di
				// DocumentoAllegabile, come segue:
				// "DOCUMENTO_XXXXX" oppure "ALLEGATO_XXXXX"
				final StringBuilder docTitlePrefisso = new StringBuilder().append(docSelezionato.getTipo().toString()).append("_").append(docSelezionato.getDocumentTitle());
				allegatiMailDocTitle.add(docTitlePrefisso.toString());
			}

			codaMailSRV.creaMailEInserisciInCoda(documentTitle, guid, mittente.getIndirizzoEmail(), destinatariStringList, destinatariMailTo.toString(),
					destinatariMailCc.toString(), oggettoMail, testoMail, AzioneMail.INOLTRA_DA_CODA, null, TipologiaInvio.NUOVOMESSAGGIO, mittente.getTipologia(), null,
					false, true, modalitaSpedizioneSc, EventTypeEnum.INOLTRO_MAIL_DA_CODA_ATTIVA.getIntValue(), utente, con, null, allegatiMailDocTitle, false);

			return new EsitoOperazioneDTO(wobNumber, true, "Il messaggio è stato correttamente aggiunto alla coda di inoltro. Il sistema provvederà all'invio in automatico.");

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di registrazione della mail in coda", e);
			return new EsitoOperazioneDTO(wobNumber, false, "Si è verificato un errore in fase di inoltro della mail");
		} finally {
			closeConnection(con);
			logoff(fpeh);
			popSubject(fceh);
		}

	}

	/**
	 * Verifica che il contatto sia corretto e lo trasforma in un
	 * destinatarioRedDTO. In caso il contatto non sia corretto ritorna null.
	 * 
	 * 
	 * @param c Comtatto da aggiungere.
	 * 
	 * @return il destinatario esterno e elettronico corrispondente al contatto
	 *         inserito.
	 */
	@Override
	public DestinatarioRedDTO checkAndTrasformFromContattoToDestinatario(final Contatto c) {
		if (org.apache.commons.lang3.StringUtils.isBlank(c.getMailSelected()) && org.apache.commons.lang3.StringUtils.isNotBlank(c.getMailPec())) {
			c.setMailSelected(c.getMailPec());
		}
		// I destinatari interni o cartacei non sono supportati.
		if (StringUtils.isNullOrEmpty(c.getMailSelected()) || TipoRubricaEnum.INTERNO.equals(c.getTipoRubricaEnum()) || c.getContattoID() == null
				|| c.getContattoID().longValue() == 0) {
			return null;
		}

		final DestinatarioRedDTO destToAdd = new DestinatarioRedDTO();
		destToAdd.setContatto(c);
		destToAdd.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.ELETTRONICO);
		destToAdd.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.ESTERNO);
		destToAdd.setModalitaDestinatarioEnum(ModalitaDestinatarioEnum.TO);
		destToAdd.setIdNodo(c.getIdNodo());
		destToAdd.setIdUtente(c.getIdUtente());

		return destToAdd;
	}

	/**
	 * Scrive l'oggetto come questo esempio: [PROT: 2193/2019 - IDDOC: 2002 -
	 * IDFASC: 11659] [Biagio Mazzotta].
	 */
	@Override
	public String createDefaultOggetto(final MasterDocumentRedDTO master, final FascicoloDTO fascicolo, final UtenteDTO utente) {
		String oggetto = "";
		try {
			final String documentTitle = master.getDocumentTitle();
			final Integer numeroProtocollo = master.getNumeroProtocollo();
			final Integer annoProtocollo = master.getAnnoProtocollo();
			final Integer numeroDocumento = master.getNumeroDocumento();

			if (org.apache.commons.lang3.StringUtils.isBlank(documentTitle) || numeroDocumento == null) {
				throw new IllegalArgumentException("Nel master non sono valorizzati gli identificativi necessari a costruire l'oggeto dell'email");
			}

			String protocollo = null;
			if (numeroProtocollo != null && annoProtocollo != null) {
				protocollo = org.apache.commons.lang3.StringUtils.join("PROT: ", numeroProtocollo, "/", annoProtocollo, " - ");
			}

			oggetto = org.apache.commons.lang3.StringUtils.join("[", protocollo, "IDDOC: ", numeroDocumento, " - IDFASC: ", fascicolo.getNomeDocumento(), "]", "[",
					utente.getNome(), " ", utente.getCognome(), "]");
		} catch (final Exception e) {
			LOGGER.error("Impossibile generare l'oggetto della mail.", e);
			throw e;
		}
		return oggetto;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IInoltraMailDaCodaAttivaFacadeSRV#getDimensioneMassimaTotaleAllegatiMailBigDecimalKB().
	 */
	@Override
	public BigDecimal getDimensioneMassimaTotaleAllegatiMailBigDecimalKB() {
		try {
			final PropertiesProvider pp = PropertiesProvider.getIstance();
			final String vlaue = pp.getParameterByKey(PropertiesNameEnum.INOLTRA_MAIL_CODA_ALLEGATI_MAX_DIM);
			final Integer maxDimInteger = Integer.valueOf(vlaue);
			final BigDecimal maxDimByte = BigDecimal.valueOf(maxDimInteger);
			return maxDimByte.divide(BigDecimal.valueOf(1024));
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero della proprerties: " + PropertiesNameEnum.INOLTRA_MAIL_CODA_ALLEGATI_MAX_DIM.getKey(), e);
			throw new RedException(e);
		}
	}
}
