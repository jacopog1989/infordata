package it.ibm.red.business.dto;

import java.sql.Date;

import it.ibm.red.business.enums.ColoreNotaEnum;

/**
 * Classe NotaDocumentoDescrizioneDTO.
 */
public class NotaDocumentoDescrizioneDTO extends NotaDocumentoDTO {
	
	/** 
	 * The Constant serialVersionUID. 
	 */
	private static final long serialVersionUID = -5446189334440394849L;

	/**
	 * Nome dell'utente recuperato dalla tabella utente.
	 */
	private String nomeUtenteOwner;
	
	/**
	 * Cognome dell'utente recuperato dalla tabella utente.
	 */
	private String cognomeUtenteOwner;
	
	/**
	 * Descrizione dell'utente recuperato dalla tabella utente.
	 */
	private String descrizioneNodoOwner;
	
	/**
	 * Origine della nota completa di eventuale descrizione.
	 * Viene ricavata tramite idOrigine.
	 */
	private OrigineNota origine;

	/**
	 * Descrizione dell'utente owner
	 */
	private String descrizioneUtenteOwner;
	 
	/**
	 * Descrizione preassegnatario interoperabilità
	 */
	private String descrPreassegnatarioInterop;
 
	/**
	 * Flag che abilita il pregresso dello storico note
	 */
	private boolean pregressoStoricoNote;
	
	/**
	 * Costruttore nota documento descrizione DTO.
	 */
	public NotaDocumentoDescrizioneDTO() {
		super();
	}
	
	/**
	 * Costruttore nota documento descrizione DTO.
	 *
	 * @param idDocumento the id documento
	 * @param numeroNota the numero nota
	 * @param colore the colore
	 * @param contentNota the content nota
	 * @param idNodoOwner the id nodo owner
	 * @param idUtenteOwner the id utente owner
	 * @param idOrigine the id origine
	 */
	public NotaDocumentoDescrizioneDTO(final Integer idDocumento, final Integer numeroNota, final ColoreNotaEnum colore,
			final String contentNota, final Integer idNodoOwner, final Integer idUtenteOwner, final Integer idOrigine) {
		super(idDocumento, numeroNota, colore, contentNota, idNodoOwner, idUtenteOwner, idOrigine);
	}
	
	/**
	 * Costruttore nota documento descrizione DTO.
	 *
	 * @param idDocumento the id documento
	 * @param numeroNota the numero nota
	 * @param colore the colore
	 * @param contentNota the content nota
	 */
	public NotaDocumentoDescrizioneDTO(final Integer idDocumento, final Integer numeroNota, final ColoreNotaEnum colore,
			final String contentNota) {
		super(idDocumento, numeroNota, colore, contentNota);
	}
	
	/**
	 * Costruttore nota documento descrizione DTO.
	 *
	 * @param idDocumento the id documento
	 * @param numeroNota the numero nota
	 * @param idNodoOwner the id nodo owner
	 * @param idUtenteOwner the id utente owner
	 * @param dataRegistrazioneNota the data registrazione nota
	 * @param contentNota the content nota
	 * @param idOrigine the id origine
	 * @param colore the colore
	 * @param dataUltimaModificaNota the data ultima modifica nota
	 * @param deleted the deleted
	 */
	public NotaDocumentoDescrizioneDTO(final Integer idDocumento, final Integer numeroNota, final Integer idNodoOwner,
			final Integer idUtenteOwner, final Date dataRegistrazioneNota, final String contentNota, final Integer idOrigine,
			final ColoreNotaEnum colore, final Date dataUltimaModificaNota, final boolean deleted) {
		super(idDocumento, numeroNota, idNodoOwner, idUtenteOwner, dataRegistrazioneNota, contentNota, idOrigine, colore,
				dataUltimaModificaNota, deleted);
	}
	
	/**
	 * Costruttore nota documento descrizione DTO.
	 *
	 * @param idDocumento the id documento
	 * @param numeroNota the numero nota
	 */
	public NotaDocumentoDescrizioneDTO(final Integer idDocumento, final Integer numeroNota) {
		super(idDocumento, numeroNota);
	}
	
	/** 
	 *
	 * @return the descrizione utente owner
	 */
	public String getDescrizioneUtenteOwner() {
		return descrizioneUtenteOwner;
	}

	/** 
	 *
	 * @param descrizioneUtenteOwner the new descrizione utente owner
	 */
	public void setDescrizioneUtenteOwner(final String descrizioneUtenteOwner) {
		this.descrizioneUtenteOwner = descrizioneUtenteOwner;
	}

	/** 
	 *
	 * @return the nome utente owner
	 */
	public String getNomeUtenteOwner() {
		return nomeUtenteOwner;
	}
	
	/** 
	 *
	 * @param nomeUtenteOwner the new nome utente owner
	 */
	public void setNomeUtenteOwner(final String nomeUtenteOwner) {
		this.nomeUtenteOwner = nomeUtenteOwner;
	}
	
	/** 
	 *
	 * @return the cognome utente owner
	 */
	public String getCognomeUtenteOwner() {
		return cognomeUtenteOwner;
	}
	
	/** 
	 *
	 * @param cognomeUtenteOwner the new cognome utente owner
	 */
	public void setCognomeUtenteOwner(final String cognomeUtenteOwner) {
		this.cognomeUtenteOwner = cognomeUtenteOwner;
	}
	
	/** 
	 *
	 * @return the descrizione nodo owner
	 */
	public String getDescrizioneNodoOwner() {
		return descrizioneNodoOwner;
	}
	
	/** 
	 *
	 * @param descrizioneNodoOwner the new descrizione nodo owner
	 */
	public void setDescrizioneNodoOwner(final String descrizioneNodoOwner) {
		this.descrizioneNodoOwner = descrizioneNodoOwner;
	}
	
	/** 
	 *
	 * @return the origine
	 */
	public OrigineNota getOrigine() {
		return origine;
	}
	
	/** 
	 *
	 * @param origine the new origine
	 */
	public void setOrigine(final OrigineNota origine) {
		this.origine = origine;
	}
	
	/** 
	 *
	 * @return the descr preassegnatario interop
	 */
	public String getDescrPreassegnatarioInterop() {
		return descrPreassegnatarioInterop;
	}
	
	/** 
	 *
	 * @param descrPreassegnatarioInterop the new preassegnatario interop
	 */
	public void setPreassegnatarioInterop(final String descrPreassegnatarioInterop) {
		this.descrPreassegnatarioInterop = descrPreassegnatarioInterop;
	}
	
	/** 
	 *
	 * @return the pregresso storico note
	 */
	public boolean getPregressoStoricoNote() {
		return pregressoStoricoNote;
	}
	
	/** 
	 *
	 * @param pregressoStoricoNote the new pregresso storico note
	 */
	public void setPregressoStoricoNote(final boolean pregressoStoricoNote) {
		this.pregressoStoricoNote = pregressoStoricoNote;
	}
}