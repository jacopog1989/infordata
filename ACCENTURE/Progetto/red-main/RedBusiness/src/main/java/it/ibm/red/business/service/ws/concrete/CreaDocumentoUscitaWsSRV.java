package it.ibm.red.business.service.ws.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.ProvenienzaSalvaDocumentoEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.service.concrete.CreaDocumentoRedUscitaAbstractSRV;
import it.ibm.red.business.service.flusso.IAzioniFlussoBaseSRV;
import it.ibm.red.business.service.ws.ICreaDocumentoUscitaWsSRV;
import it.ibm.red.business.service.ws.auth.DocumentServiceAuthorizable;
import it.ibm.red.webservice.model.documentservice.dto.Servizio;
import it.ibm.red.webservice.model.documentservice.types.documentale.Allegato;
import it.ibm.red.webservice.model.documentservice.types.documentale.Destinatario;
import it.ibm.red.webservice.model.documentservice.types.documentale.TipoDestinatario;
import it.ibm.red.webservice.model.documentservice.types.messages.RedCreazioneDocumentoUscitaType;
import it.ibm.red.webservice.model.documentservice.types.rubrica.ContattoBase;

/**
 * @author m.crescentini
 * 
 * Servizio per la creazione di un documento in uscita tramite web service.
 *
 */
@Service
@Qualifier("CreaDocumentoUscitaWsSRV")
public class CreaDocumentoUscitaWsSRV extends CreaDocumentoRedUscitaAbstractSRV implements ICreaDocumentoUscitaWsSRV {

	private static final long serialVersionUID = -3933447329746707313L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(CreaDocumentoUscitaWsSRV.class.getName());

	/**
	 * Servizio utente.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;

	/**
	 * Servizio azioni.
	 */
	@Autowired
	private IAzioniFlussoBaseSRV azioniFlussoBaseSRV;

	/**
	 * Dao aoo.
	 */
	@Autowired
	private IAooDAO aooDAO;

	/**
	 * @see it.ibm.red.business.service.concrete.CreaDocumentoRedAbstractSRV#getProvenienza().
	 */
	@Override
	protected ProvenienzaSalvaDocumentoEnum getProvenienza() {
		return ProvenienzaSalvaDocumentoEnum.WEB_SERVICE;
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.ICreaDocumentoWsFacadeSRV#creaDocumentoUscitaRed
	 *      (it.ibm.red.webservice.model.documentservice.types.messages.RedCreazioneDocumentoUscitaType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_CREAZIONE_DOCUMENTO_USCITA)
	public final EsitoSalvaDocumentoDTO creaDocumentoUscitaRed(final RedWsClient client, final RedCreazioneDocumentoUscitaType docUscitaInput) {
		EsitoSalvaDocumentoDTO esito = new EsitoSalvaDocumentoDTO();
		IFilenetCEHelper fceh = null;
		Connection con = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), true);
			
			// Si recupera l'AOO associato al client
			final Aoo aoo = aooDAO.getAoo(client.getIdAoo(), con);
			
			if (aoo != null) {
				// Utente creatore
				UtenteDTO utenteCreatore = null;
				if (docUscitaInput.getIdUfficioCreatore() != null && docUscitaInput.getIdUfficioCreatore().longValue() != 0) {
					utenteCreatore = utenteSRV.getById(docUscitaInput.getIdUtenteCreatore().longValue(), null, docUscitaInput.getIdUfficioCreatore().longValue(), con);
				} else {
					utenteCreatore = utenteSRV.getById(docUscitaInput.getIdUtenteCreatore().longValue(), con);
				}
				
				fceh = FilenetCEHelperProxy.newInstance(utenteCreatore.getFcDTO(),utenteCreatore.getIdAoo());
				
				// Destinatari
				final List<DestinatarioRedDTO> destinatari = getDestinatariPerCreazioneDocumento(docUscitaInput.getDestinatari(), utenteCreatore.getIdAoo());
				
				// Allegati
				final List<AllegatoDTO> allegati = getAllegati(docUscitaInput.getAllegati(), utenteCreatore.getCodiceAoo());
				
				// Assegnatario
				Long idUtenteAssegnatario = null;
				Long idUfficioAssegnatario = null;
				if (docUscitaInput.getAssegnatarioCompetenza() != null) {
					idUtenteAssegnatario = docUscitaInput.getAssegnatarioCompetenza().getIdUtente() != null ? 
							docUscitaInput.getAssegnatarioCompetenza().getIdUtente().longValue() : null;
					idUfficioAssegnatario = docUscitaInput.getAssegnatarioCompetenza().getIdGruppo().longValue();
				}
				
				// ### Creazione del documento
				esito = creaDocumentoRedUscitaDaContent(docUscitaInput.getDataHandler(), docUscitaInput.getDocType(), docUscitaInput.getNomeFile(), utenteCreatore, 
						null, docUscitaInput.getIndiceClassificazione(), docUscitaInput.getIdTipologiaDocumento(), docUscitaInput.getIdTipologiaProcedimento(),
						docUscitaInput.getIdIter(), TipoAssegnazioneEnum.get(docUscitaInput.getIdTipoAssegnazione()), 
						MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD, allegati, null, destinatari, docUscitaInput.getOggetto(), docUscitaInput.getOggettoMail(), 
						docUscitaInput.getTestoMail(), docUscitaInput.getMittenteMail(), docUscitaInput.getMittenteMail(), idUtenteAssegnatario, idUfficioAssegnatario, 
						docUscitaInput.getFlusso(), aoo, con);
				
				if (esito.isEsitoOk()) {
					// ### Si eseguono le azioni successive alla creazione del documento per il flusso indicato in input (se presente)
					azioniFlussoBaseSRV.doAfterCreazioneDocumento(Integer.parseInt(esito.getDocumentTitle()), docUscitaInput.getFlusso(), con);
					
					commitConnection(con);
					
					LOGGER.info("Documento in uscita creato correttamente. Document Title: " + esito.getDocumentTitle() 
							+ ", Numero Documento: " + esito.getNumeroDocumento());
				} else {
					final String erroreSalvaDocumento = esito.getErrori().get(0).getMessaggio();
					LOGGER.error("Si è verificato un errore durante il salvataggio del documento: " + erroreSalvaDocumento);
					throw new RedException("Errore durante il salvataggio del documento: " + erroreSalvaDocumento);
				}
			} else {
				LOGGER.error("AOO associato al client WS con ID " + client.getIdClient() + " non trovato");
				throw new RedException("AOO associato al client WS con ID " + client.getIdClient() + " non trovato");
			}
			
		} catch (final Exception e) {
			rollbackConnection(con);
			LOGGER.error("Si è verificato un errore durante la creazione del documento: " + e.getMessage(), e);
			throw new RedException("Errore durante la creazione del documento: " + e.getMessage());
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}
		
		return esito;
	}


	/**
	 * @param allegatiInput
	 * @return
	 */
	private List<AllegatoDTO> getAllegati(final List<Allegato> allegatiInput, final String codiceAoo) {
		final List<AllegatoDTO> allegati = new ArrayList<>();
		
		if (!CollectionUtils.isEmpty(allegatiInput)) {
			AllegatoDTO allegato = null;
			final Integer idTipologiaDocumento = Integer.parseInt(PropertiesProvider.getIstance().getParameterByString(codiceAoo + ".tipologia.generica.default"));
			for (final Allegato all : allegatiInput) {
				allegato = new AllegatoDTO(all.getNomeFile(), all.getDataHandler(), all.getDocType(), all.getOggetto(), 
						all.isDaFirmare(), all.isMantieniFormatoOriginale(), idTipologiaDocumento);
				
				allegati.add(allegato);
			}
		}
		
		return allegati;
	}


	/**
	 * @param destinatariInput
	 * @return
	 */
	private List<DestinatarioRedDTO> getDestinatariPerCreazioneDocumento(final List<Destinatario> destinatariInput, final Long idAoo) {
		final List<DestinatarioRedDTO> destinatari = new ArrayList<>();
		
		if (!CollectionUtils.isEmpty(destinatariInput)) {
			DestinatarioRedDTO destinatario = null;
			
			for (final Destinatario dest : destinatariInput) {
				
				// Destinatario interno
				if (TipoDestinatario.INTERNO.equals(dest.getTipoDestinatario())) {
					
					destinatario = new DestinatarioRedDTO(dest.getIdUfficio().longValue(), dest.getIdUtente() != null ?  dest.getIdUtente().longValue() : null, 
							TipologiaDestinatarioEnum.INTERNO, ModalitaDestinatarioEnum.TO);
				
				} else {
					
					MezzoSpedizioneEnum mezzoSpedizione = MezzoSpedizioneEnum.ELETTRONICO;
					
					if (TipoDestinatario.CARTACEO.equals(dest.getTipoDestinatario())) {
						mezzoSpedizione = MezzoSpedizioneEnum.CARTACEO;
					}
					
					// Destinatario esterno presente in rubrica
					if (dest.getIdDestinatario() != null) {
						
						destinatario = new DestinatarioRedDTO(dest.getIdDestinatario().longValue(), mezzoSpedizione, 
								ModalitaDestinatarioEnum.getModalita(dest.getToCc().name()));
						
					// Destinatario esterno da inserire in rubrica	
					} else {
						final ContattoBase nuovoContattoDestinatario = dest.getDestinatario();
						
						destinatario = new DestinatarioRedDTO(nuovoContattoDestinatario.getNome(), nuovoContattoDestinatario.getCognome(), 
								nuovoContattoDestinatario.getAliasContatto(), nuovoContattoDestinatario.getMail(), nuovoContattoDestinatario.getMailPec(),
								nuovoContattoDestinatario.getTipoPersona(), idAoo, mezzoSpedizione, ModalitaDestinatarioEnum.getModalita(dest.getToCc().name()));
					}
				}
				
				destinatari.add(destinatario);
			}
		}
		
		return destinatari;
	}
	
}