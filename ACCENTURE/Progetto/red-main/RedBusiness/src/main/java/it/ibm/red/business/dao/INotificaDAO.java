package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.NotificaDTO;

/**
 * 
 * @author CPIERASC
 *
 *	Interfaccia per il dao della gestione norifiche.
 */
public interface INotificaDAO extends Serializable {

	/**
	 * Metodo per inserire una notifica.
	 * 
	 * @param notifica		dto contenente le informazioni di una notifica
	 * @param connection	connessione al database
	 */
	void insert(NotificaDTO notifica, Connection connection);

	
	/**
	 * Metodo per recuperare la prossima notifica.
	 * 
	 * @param connection	connessione al database
	 */
	NotificaDTO getNext(int idAoo, Connection connection);
	
	/**
	 * Aggiorna la notifica con l'errore in input.
	 * @param idNotifica
	 * @param descErrore
	 * @param connection
	 * @return
	 */
	int updateNotifica(int idNotifica, String descErrore, Connection connection);
	
	/**
	 * Aggiorna la notifica in input
	 * 
	 * @param notifica
	 * @param connection
	 * @return
	 */
	int updateNotifica(NotificaDTO notifica, Connection connection);
	
	
	/**
	 * Verifica se la notifica è presente sul db.
	 * 
	 * @param notifica
	 * @param con
	 * @return
	 */
	boolean isPresente(NotificaDTO notifica, Connection con);
	
	/**
	 * Recupera le notifiche dell'utente in input non più vecchie di aPartireDaNgiorni giorni.
	 * 
	 * @param idAoo
	 * @param idUtente
	 * @param aPartireDaNgiorni
	 * @param connection
	 * @return
	 */
	List<NotificaDTO> getNotifiche(Long idAoo, Long idUtente, Integer aPartireDaNgiorni, Connection connection);
	
	/**
	 * Elimina logicamente la notifica in input.
	 * 
	 * @param idNotifica
	 * @param con
	 * @return
	 */
	boolean eliminaNotifica(int idNotifica, Connection con);
	
	/**
	 * Contrassegna come visualizzata la notifica in input
	 * 
	 * @param idNotifica
	 * @param con
	 * @return
	 */
	boolean contrassegnaNotifica(int idNotifica, Connection con);
	
	/**
	 * Metodo per recuperare singola notifica dato id
	 * 
	 * @param idNotifica,idUtente,idAoo,connection
	 * @return
	 */
	List<NotificaDTO> getNotifiche(Integer idNotifica, Long idUtente, Long idAoo, Connection connection);


	/**
	 * Ottiene le notifiche sull'homepage.
	 * @param idAoo
	 * @param idUtente
	 * @param aPartireDaNgiorni
	 * @param maxRowNum
	 * @param maxNotificheNonLette
	 * @param connection
	 * @return lista delle notifiche
	 */
	List<NotificaDTO> getNotificheHomepage(Long idAoo, Long idUtente, Integer aPartireDaNgiorni, Integer maxRowNum, int maxNotificheNonLette, Connection connection);

	/**
	 * Ottiene il numero delle notifiche sull'homepage.
	 * @param idAoo
	 * @param idUtente
	 * @param maxRowNum
	 * @param maxNotificheNonLette
	 * @param connection
	 * @return numero delle notifiche
	 */
	Integer getCountNotificheHomepage(Long idAoo, Long idUtente, Integer maxRowNum, int maxNotificheNonLette, Connection connection);
}
