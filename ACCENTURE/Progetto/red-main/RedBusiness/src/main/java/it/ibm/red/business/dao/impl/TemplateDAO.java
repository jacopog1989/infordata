package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.ITemplateDAO;
import it.ibm.red.business.dto.TemplateDTO;
import it.ibm.red.business.dto.TemplateMetadatoDTO;
import it.ibm.red.business.dto.TemplateMetadatoValueDTO;
import it.ibm.red.business.dto.TemplateMetadatoVersion;
import it.ibm.red.business.dto.TemplateValueDTO;
import it.ibm.red.business.enums.TipoTemplateMetadatoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.StringUtils;

/**
 * @author SLac
 * 
 *         TABELLA: TEMPLATE
 */
@Repository
public class TemplateDAO extends AbstractDAO implements ITemplateDAO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -765282996435963678L;

	/**
	 * LOGGER per la gestione dei messaggi in console.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TemplateDAO.class);

	/**
	 * Messaggio errore recupero versione template.
	 */
	private static final String ERROR_RECUPERO_VERSIONE_TEMPLATE_MSG = "Errore durante il recupero delle versione del template";

	/**
	 * Messaggio errore recupero valori documento.
	 */
	private static final String ERROR_RECUPERO_VALORI_DOCUMENTO_MSG = "Errore durante il recupero dei valori del documento ";

	/**
	 * @see it.ibm.red.business.dao.ITemplateDAO#getAll(java.sql.Connection,
	 *      java.lang.Long, boolean).
	 */
	@Override
	public List<TemplateDTO> getAll(final Connection connection, final Long idAOO, final boolean applet) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<TemplateDTO> list = null;
		try {
			final String querySQL = "select * " + "  from template t " + " where t.dataattivazione <= sysdate "
					+ "   AND (t.datadisattivazione is null OR t.datadisattivazione > sysdate) " + "   AND (t.idAOO = ? OR  t.idaoo is null) " + "   AND flag_applet = ? "
					+ " order by t.descrizione ";
			ps = connection.prepareStatement(querySQL);
			ps.setLong(1, idAOO);
			ps.setInt(2, (applet ? 1 : 0));
			rs = ps.executeQuery();

			list = new ArrayList<>();
			TemplateDTO item = null;
			while (rs.next()) {
				item = new TemplateDTO();
				populateVO(item, rs);
				list.add(item);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero dei template", e);
			throw new RedException("Errore in fase di recupero dei template", e);
		} finally {
			closeStatement(ps, rs);
		}
		return list;
	}

	private static void populateVO(final TemplateDTO template, final ResultSet rs) throws SQLException {
		template.setIdTemplate(rs.getString("idtemplate"));
		template.setDescrizione(rs.getString("descrizione"));
	}

	/**
	 * @see it.ibm.red.business.dao.ITemplateDAO#getMetadati(java.sql.Connection,
	 *      java.lang.String).
	 */
	@Override
	public List<TemplateMetadatoDTO> getMetadati(final Connection connection, final String idTemplate) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement("SELECT * FROM template_metadati WHERE idtemplate = ? ORDER BY ordine");
			ps.setString(1, idTemplate);
			rs = ps.executeQuery();
			return fetchMultiResultMetadati(rs);
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero dei metadati del template " + idTemplate, e);
			throw new RedException("Errore durante il recupero dei metadati del template dal DB: " + e.getMessage(), e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}

	private static List<TemplateMetadatoDTO> fetchMultiResultMetadati(final ResultSet rs) throws SQLException {
		final List<TemplateMetadatoDTO> resultList = new ArrayList<>();
		while (rs.next()) {
			final TemplateMetadatoDTO templateMetadato = new TemplateMetadatoDTO();
			populateVOMetadato(templateMetadato, rs);
			resultList.add(templateMetadato);
		}
		return resultList;
	}

	private static void populateVOMetadato(final TemplateMetadatoDTO templateMetadato, final ResultSet rs) throws SQLException {
		templateMetadato.setIdTemplate(rs.getString("idtemplate"));
		templateMetadato.setIdMetadato(rs.getInt("idmetadato"));
		templateMetadato.setLabel(rs.getString("label"));
		templateMetadato.setMaxLength(rs.getInt("maxlength"));
		templateMetadato.setOrdine(rs.getInt("ordine"));
		templateMetadato.setDefaultValue(rs.getString("default_value"));
		templateMetadato.setTestoPredefinito(rs.getInt("testopredefinito"));
		templateMetadato.setTipoMetadato(TipoTemplateMetadatoEnum.getById(rs.getInt("idtipometadato")));
		templateMetadato.setTemplateValue(rs.getString("template_value"));
		templateMetadato.setTemplateLabel((rs.getInt("template_label") == 1));
		templateMetadato.setTemplateTesto((rs.getInt("template_testo") == 1));
		templateMetadato.setCaseSensitive((rs.getInt("case_sensitive") == 1));
		Integer metadatoIntest = rs.getInt("idMetadatoIntestazione");
		if(metadatoIntest!=null && metadatoIntest!=0) {
			templateMetadato.setIdMetadatoIntestazione(metadatoIntest);
		}
		templateMetadato.setInfo(rs.getString("info"));
	}

	/**
	 * @see it.ibm.red.business.dao.ITemplateDAO#getTemplateValue(java.sql.Connection,
	 *      java.lang.String).
	 */
	@Override
	public TemplateValueDTO getTemplateValue(final Connection connection, final String idDocumento) {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			final StringBuilder sql = new StringBuilder();
			sql.append("SELECT t.*, td.iddocumento, td.nomedocumento ");
			sql.append("  FROM template_doc td, ");
			sql.append("        template t ");
			sql.append(" WHERE td.iddocumento = ?  ");
			sql.append("   AND td.idtemplate = t.idtemplate ");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, idDocumento);
			rs = ps.executeQuery();

			return populateVOValue(rs);
		} catch (final SQLException e) {
			LOGGER.error(ERROR_RECUPERO_VALORI_DOCUMENTO_MSG + idDocumento, e);
			throw new RedException(ERROR_RECUPERO_VALORI_DOCUMENTO_MSG + e.getMessage(), e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}

	private static TemplateValueDTO populateVOValue(final ResultSet rs) throws SQLException {
		TemplateValueDTO templateValue = null;

		while (rs.next()) {
			templateValue = new TemplateValueDTO();
			populateVO(templateValue, rs);
			templateValue.setIdDocumento(rs.getString("iddocumento"));
			templateValue.setNomeDocumento(rs.getString("nomedocumento"));
		}

		return templateValue;
	}

	/**
	 * @see it.ibm.red.business.dao.ITemplateDAO#getMetadatiValues(java.sql.Connection,
	 *      java.lang.String, java.lang.String).
	 */
	@Override
	public List<TemplateMetadatoValueDTO> getMetadatiValues(final Connection connection, final String idDocumento, final String idTemplate) {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			final StringBuilder sql = new StringBuilder();
			sql.append("SELECT tm.*, tdv.value ");
			sql.append("  FROM template_doc_value tdv, ");
			sql.append("       template_metadati tm ");
			sql.append(" WHERE tdv.iddocumento = ? ");
			sql.append("   AND tdv.idtemplate = ? ");
			sql.append("   AND tdv.idtemplate = tm.idtemplate ");
			sql.append("   AND tdv.idmetadato = tm.idmetadato ");
			sql.append(" ORDER BY tm.ordine ");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, idDocumento);
			ps.setString(2, idTemplate);
			rs = ps.executeQuery();

			return fetchMultiResultValues(rs);
		} catch (final SQLException e) {
			LOGGER.error(ERROR_RECUPERO_VALORI_DOCUMENTO_MSG + idDocumento, e);
			throw new RedException(ERROR_RECUPERO_VALORI_DOCUMENTO_MSG + e.getMessage(), e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}

	private static List<TemplateMetadatoValueDTO> fetchMultiResultValues(final ResultSet rs) throws SQLException {
		final List<TemplateMetadatoValueDTO> resultList = new ArrayList<>();

		while (rs.next()) {
			final TemplateMetadatoValueDTO templateMetadatoValues = new TemplateMetadatoValueDTO();
			populateVOMetadatoValue(templateMetadatoValues, rs);
			resultList.add(templateMetadatoValues);
		}

		return resultList;
	}

	private static void populateVOMetadatoValue(final TemplateMetadatoValueDTO templateMetadatoValues, final ResultSet rs) throws SQLException {
		populateVOMetadato(templateMetadatoValues, rs);
		templateMetadatoValues.setValue(rs.getString("value"));
	}

	/**
	 * @see it.ibm.red.business.dao.ITemplateDAO#deleteTemplateValue(java.sql.Connection,
	 *      java.lang.String).
	 */
	@Override
	public void deleteTemplateValue(final Connection con, final String idDocumento) {
		PreparedStatement ps = null;
		int index = 1;
		try {
			ps = con.prepareStatement("DELETE FROM template_doc td WHERE td.iddocumento = ?");
			ps.setString(index++, idDocumento);
			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante la cancellazione dei valori del documento " + idDocumento, e);
			throw new RedException("Errore durante la cancellazione dei valori del documento dal DB: " + e.getMessage(), e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ITemplateDAO#insertTemplateValue(java.sql.Connection,
	 *      it.ibm.red.business.dto.TemplateValueDTO, java.util.List).
	 */
	@Override
	public void insertTemplateValue(final Connection con, final TemplateValueDTO template, final List<TemplateMetadatoValueDTO> values) {
		int index = 1;
		PreparedStatement ps = null;

		try {
			ps = con.prepareStatement("INSERT INTO template_doc ( iddocumento,idtemplate,nomedocumento ) VALUES (?, ?, ?)");
			ps.setString(index++, template.getIdDocumento());
			ps.setString(index++, template.getIdTemplate());
			ps.setString(index++, template.getNomeDocumento());
			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'inserimento del template del documento " + template, e);
			throw new RedException("Errore durante l'inserimento del template del documento sul DB: " + e.getMessage(), e);
		} finally {
			closeStatement(ps);
		}

		TemplateMetadatoValueDTO metadato = null;
		try {

			ps = con.prepareStatement("INSERT INTO template_doc_value ( iddocumento,idtemplate,idmetadato,value ) VALUES (?, ?, ?, ?)");
			for (int i = 0; i < values.size(); i++) {
				metadato = values.get(i);

				index = 1;
				ps.setString(index++, template.getIdDocumento());
				ps.setString(index++, template.getIdTemplate());
				ps.setInt(index++, metadato.getIdMetadato());
				ps.setString(index++, ((metadato.getValue() != null) ? metadato.getValue() : ""));
				ps.executeUpdate();
			}

		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'inserimento dei valori [" + (metadato != null ? metadato : "") + "] del documento " + template, e);
			throw new RedException("Errore durante l'inserimento dei valori del documento sul DB: " + e.getMessage(), e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ITemplateDAO#insertTemplateValueNew(java.sql.Connection,
	 *      it.ibm.red.business.dto.TemplateValueDTO, java.util.List,
	 *      java.lang.Integer, java.lang.Long).
	 */
	@Override
	public void insertTemplateValueNew(final Connection con, final TemplateValueDTO template, final List<TemplateMetadatoValueDTO> values, final Integer versione,
			final Long idUtenteCreatore) {
		int index = 1;
		PreparedStatement ps = null;

		try {
			ps = con.prepareStatement("INSERT INTO template_doc ( iddocumento,idtemplate,nomedocumento,versione,idutentecreatore ) VALUES (?, ?, ?,?,?)");
			ps.setString(index++, template.getIdDocumento());
			ps.setString(index++, template.getIdTemplate());
			ps.setString(index++, template.getNomeDocumento());
			ps.setInt(index++, versione);
			ps.setLong(index++, idUtenteCreatore);
			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'inserimento del template del documento " + template, e);
			throw new RedException("Errore durante l'inserimento del template del documento sul DB: " + e.getMessage(), e);
		} finally {
			closeStatement(ps);
		}

		TemplateMetadatoValueDTO metadato = null;
		try {

			ps = con.prepareStatement("INSERT INTO template_doc_value ( iddocumento,idtemplate,idmetadato,value,versione ) VALUES (?, ?, ?, ?,?)");
			for (int i = 0; i < values.size(); i++) {
				metadato = values.get(i);

				index = 1;
				ps.setString(index++, template.getIdDocumento());
				ps.setString(index++, template.getIdTemplate());
				ps.setInt(index++, metadato.getIdMetadato());
				ps.setString(index++, ((metadato.getValue() != null) ? metadato.getValue() : ""));
				ps.setInt(index++, versione);
				ps.executeUpdate();
			}

		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'inserimento dei valori [" + (metadato != null ? metadato : "") + "] del documento " + template, e);
			throw new RedException("Errore durante l'inserimento dei valori del documento sul DB: " + e.getMessage(), e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ITemplateDAO#getVersioniDocumento(java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public List<TemplateMetadatoVersion> getVersioniDocumento(final Integer idDocumento, final Connection conn) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<TemplateMetadatoVersion> listVersion = null;
		try {
			int index = 1;

			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT DISTINCT TD.IDDOCUMENTO,TD.NOMEDOCUMENTO,TD.VERSIONE,TD.IDTEMPLATE, TEMPLATE.DESCRIZIONE,U.NOME,U.COGNOME ");
			sb.append("FROM TEMPLATE_DOC TD JOIN TEMPLATE_DOC_VALUE TDV ");
			sb.append("ON TD.IDDOCUMENTO = TDV.IDDOCUMENTO ");
			sb.append(" JOIN TEMPLATE ");
			sb.append(" ON TEMPLATE.IDTEMPLATE = TD.IDTEMPLATE ");
			sb.append(" LEFT OUTER JOIN UTENTE U ON TD.IDUTENTECREATORE = U.IDUTENTE ");
			sb.append("WHERE TD.IDDOCUMENTO = ?");
			sb.append("ORDER BY TD.VERSIONE DESC");

			ps = conn.prepareStatement(sb.toString());

			ps.setInt(index++, idDocumento);
			rs = ps.executeQuery();

			listVersion = new ArrayList<>();
			while (rs.next()) {
				String utenteCreatore = null;
				if (!StringUtils.isNullOrEmpty(rs.getString("NOME")) && !StringUtils.isNullOrEmpty(rs.getString("COGNOME"))) {
					utenteCreatore = rs.getString("NOME") + " " + rs.getString("COGNOME");
				}
				final TemplateMetadatoVersion version = new TemplateMetadatoVersion(rs.getInt("IDDOCUMENTO"), rs.getString("NOMEDOCUMENTO"), rs.getInt("VERSIONE"),
						rs.getString("IDTEMPLATE"), rs.getString("DESCRIZIONE"), utenteCreatore);
				listVersion.add(version);
			}
		} catch (final SQLException ex) {
			LOGGER.error(ERROR_RECUPERO_VERSIONE_TEMPLATE_MSG + ex);
			throw new RedException(ERROR_RECUPERO_VERSIONE_TEMPLATE_MSG + ex);
		} finally {
			closeStatement(ps, rs);
		}

		return listVersion;
	}

	/**
	 * @see it.ibm.red.business.dao.ITemplateDAO#getValueMetadato(java.lang.String,
	 *      java.lang.Integer, java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public List<TemplateMetadatoValueDTO> getValueMetadato(final String idTemplate, final Integer idDocumento, final Integer versione, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT tm.*, tdv.value ,tdv.versione ");
			sb.append("FROM template_doc_value tdv,  template_metadati tm ");
			sb.append("WHERE tdv.iddocumento = ? AND tdv.idtemplate =? AND tdv.idtemplate = tm.idtemplate ");
			sb.append("AND tdv.idmetadato = tm.idmetadato  and tdv.versione = ? ");
			sb.append("ORDER BY tm.ordine ");

			ps = connection.prepareStatement(sb.toString());
			ps.setInt(index++, idDocumento);
			ps.setString(index++, idTemplate);
			ps.setInt(index++, versione);
			rs = ps.executeQuery();

			return fetchMultiResultValues(rs);
		} catch (final SQLException e) {
			LOGGER.error(ERROR_RECUPERO_VALORI_DOCUMENTO_MSG + idDocumento, e);
			throw new RedException("Errore durante il recupero dei valori del documento dal DB: " + e.getMessage(), e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ITemplateDAO#getVersionTemplate(java.sql.Connection,
	 *      java.lang.String).
	 */
	@Override
	public Integer getVersionTemplate(final Connection con, final String idDocumento) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		Integer output = null;
		try {
			ps = con.prepareStatement("SELECT VERSIONE FROM template_doc td WHERE td.iddocumento = ? ORDER BY VERSIONE DESC");
			ps.setString(index++, idDocumento);
			rs = ps.executeQuery();

			if (rs.next()) {
				output = rs.getInt("VERSIONE");
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante la cancellazione dei valori del documento " + idDocumento, e);
			throw new RedException("Errore durante la cancellazione dei valori del documento dal DB: " + e.getMessage(), e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}

}
