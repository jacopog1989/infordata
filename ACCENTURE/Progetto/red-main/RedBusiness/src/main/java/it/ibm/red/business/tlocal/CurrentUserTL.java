package it.ibm.red.business.tlocal;

/**
 * Classe CurrentUserTL.
 */
public final class CurrentUserTL {
	
	/**
	 * Thread local.
	 */
	private static final ThreadLocal<String> CURRENT = new ThreadLocal<>();
	
	/**
	 * Costruttore vuoto.
	 */
	private CurrentUserTL() {
		// Costruttore vutoo.
	}

	/**
	 * Restituisce il value se non è null, restituisce una stringa vuota se null.
	 * @return value
	 */
	public static String getValue() {
    	String output = CURRENT.get();
    	if (output == null) {
    		output = "";
    	}
        return output;
    }

	/**
	 * Imposta l'uuid.
	 * @param uuid
	 */
	public static void setValue(final String uuid) {
        CURRENT.set(uuid);
    }

	/**
	 * Rimuove l'uuid.
	 */
	public static void unsetValue() {
        CURRENT.remove();
    }

}