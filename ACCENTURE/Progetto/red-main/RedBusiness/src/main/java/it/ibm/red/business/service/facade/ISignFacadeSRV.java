package it.ibm.red.business.service.facade;

import java.io.InputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.DetailDocumentoDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.LocalSignContentDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.ProtocolloDTO;
import it.ibm.red.business.dto.ProtocolloEmergenzaDocDTO; 
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.VerificaFirmaPrincipaleAllegatoDTO;
import it.ibm.red.business.dto.VerifyInfoDTO;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.service.concrete.SignSRV.VerificaFirmaItem;

/**
 * Facade per l'implementazione dei servizi di firma.
 * 
 * @author CPAOLUZI
 *
 */
public interface ISignFacadeSRV extends Serializable {

	/**
	 * Metodo per pades non visibile multipla.
	 * 
	 * @param wobs   identificativi documenti
	 * @param utente credenziali utente
	 * @param otp    otp firma
	 * @return esito operazione
	 */
	Collection<EsitoOperazioneDTO> padesNonVisibile(Collection<String> wobs, UtenteDTO utente, String otp);

	/**
	 * Pades visibile.
	 * 
	 * @param fcDTO  credenziali filenet
	 * @param wobs   identificativi documenti
	 * @param utente credenziali utente
	 * @param otp    otp
	 * @return esito operazione
	 */
	Collection<EsitoOperazioneDTO> padesVisibile(Collection<String> wobs, UtenteDTO utente, String otp);

	/**
	 * Metodo cades multipla.
	 * 
	 * @param fcDTO  credenziali filenet
	 * @param wobs   identificativi documenti
	 * @param utente credenziali utente
	 * @param otp    otp
	 * @return esito operazione
	 */
	Collection<EsitoOperazioneDTO> cades(Collection<String> wobs, UtenteDTO utente, String otp);

	/**
	 * Firma autografa.
	 * 
	 * @param wobNumber identificativo documento
	 * @param utente    credenziali utente
	 * @return esito operazione
	 */
	EsitoOperazioneDTO firmaAutografaMobile(String wobNumber, UtenteDTO utente);

	/**
	 * Firma autografa multipla.
	 * 
	 * @param wobs   identificativi documenti
	 * @param utente credenziali utente
	 * @return esito operazione
	 */
	Collection<EsitoOperazioneDTO> multiFirmaAutografaMobile(Collection<String> wobs, UtenteDTO utente);

	/**
	 * Pades posizionale.
	 * 
	 * @param fcDTO  credenziali filenet
	 * @param idDoc  identificativo documento
	 * @param utente credenziali utente
	 * @param page   numero pagina
	 * @param x      ascissa firma
	 * @param y      ordinata firma
	 * @param otp    otp
	 * @return esito operazione
	 */
	EsitoOperazioneDTO padesPosizionale(String idDoc, UtenteDTO utente, Integer page, Integer x, Integer y, String otp);

	/**
	 * Recupera i documenti (principali e allegati) la cui firma è da verificare. La
	 * lista è ordinata dando priorità agli ultimi documenti caricati.
	 * 
	 * @param idAoo
	 * @return
	 */
	List<VerificaFirmaItem> recuperaContentDaVerificare(int idAoo);

	/**
	 * Controlla se i documenti selezionati per la firma sono congruenti per firma
	 * remota o locale.
	 * 
	 * @param utente
	 * @param documentiSelezionati
	 * 
	 * @return il messaggio d'errore, stringa vuota se è tutto ok
	 */
	String checkDocumentsPreSign(UtenteDTO utente, List<MasterDocumentRedDTO> documentiSelezionati);

	/**
	 * Controlla se i documenti in input siano caratterizzati da condizioni che
	 * debbano essere confermate dall'utente:
	 * <ul>
	 * <li>documenti già predisposti per la firma digitale</li>
	 * <li>documenti con destinatari elettronici</li>
	 * <li>documenti già associati a protocollo</li>
	 * </ul>
	 * 
	 * @param utente
	 * @param documentiSelezionati
	 * 
	 * @return il messaggio di conferma, stringa vuota se è tutto ok
	 */
	String checkDocumentsPreSignAutografa(UtenteDTO utente, List<MasterDocumentRedDTO> documentiSelezionati);

	/**
	 * Controlla se fra quelli in input ci siano documenti (non di classe DECRETO
	 * FEPA), per i quali sia già stato staccato un numero di protocollo.
	 * 
	 * @param documentiSelezionati
	 * 
	 * @return il messaggio d'errore, stringa vuota se è tutto ok
	 */
	String checkProtocolsPreSign(List<MasterDocumentRedDTO> documentiSelezionati);
	
	/**
	 * Esegue la firma remota.
	 * 
	 * @param signTransactionId
	 * @param signType                   cades/pades
	 * @param selectedWobNumbers         lista dei wob number relativi ai documenti
	 *                                   da firmare
	 * @param utente
	 * @param otp
	 * @param pin
	 * @param principaleOnlyPAdESVisible se true, è consentita solo la firma PAdES visibile sul documento principale. 
	 * Se false, in caso manchino i presupposti (glifo, spazio vuoto per la firma, etc...), viene apposta una firma invisibile
	 * @param isGlifoDelegato se true, si firmerà col glifo dell'utente delegato se presente nella tabella di mapping.
	 * @return 
	 */
	Collection<EsitoOperazioneDTO> firmaRemota(String signTransactionId, SignTypeEnum signType, Collection<String> selectedWobNumbers, UtenteDTO utente, String otp, String pin, 
			boolean principaleOnlyPAdESVisible);
	
	/**
	 * Esegue la firma remota MULTIPLA.
	 * 
	 * @param signTransactionId
	 * @param signType
	 * @param wobs
	 * @param utente
	 * @param otp
	 * @param pin
	 * @param principaleOnlyPAdESVisible
	 * @return
	 */
	Collection<EsitoOperazioneDTO> firmaRemotaMultipla(String signTransactionId, SignTypeEnum signType, Collection<String> wobs, UtenteDTO utente, String otp, String pin, 
			boolean principaleOnlyPAdESVisible);

	/**
	 * Controlla se i documenti non firmati in input abbiano approvazioni (sigle o
	 * visti) non selezionate per la stampigliatura.
	 * 
	 * @param documentiSelezionati
	 * @param utente
	 * @return null se non esistono approvazioni da non stampigliare
	 */
	String checkApprovazioniDaNonStampigliare(List<MasterDocumentRedDTO> documentiSelezionati, UtenteDTO utente);

	/**
	 * Converti i destinatari.
	 * 
	 * @param utente
	 * @param documentiSelezionati
	 */
	void convertiDestinatariElettroniciInCartacei(UtenteDTO utente, List<MasterDocumentRedDTO> documentiSelezionati);
	
	/**
	 * Esegue la firma autografa.
	 * 
	 * @param signTransactionId
	 * @param selectedWobNumbers  lista dei wob number relativi ai documenti da
	 *                            firmare
	 * @param utente
	 * @param protocolloEmergenza
	 * @return
	 */
	Collection<EsitoOperazioneDTO> firmaAutografa(String signTransactionId, Collection<String> selectedWobNumbers, UtenteDTO utente, ProtocolloEmergenzaDocDTO protocolloEmergenza);

	/**
	 * Controlla che i documenti selezionati per la firma autografa non contengano
	 * allegati di tipo "FIRMATO DIGITALMENTE" o "FORMATO ORIGINALE".
	 * 
	 * @param utente
	 * @param documentiSelezionati
	 * @return
	 */
	String checkErrorDocumentsPreSignAutografa(UtenteDTO utente, List<MasterDocumentRedDTO> documentiSelezionati);

	/**
	 * Recupera i documentTitle degli allegati da firmare del documento principale
	 * in input.
	 * 
	 * @param documentTitle - Document Title del documento principale
	 * @param guid - GUID del documento principale
	 * @param utente
	 * @param ste tipo di firma richiesta
	 * @return allegati lista dei documentTitle/nomeFile degli allegati recuperati
	 */
	List<AllegatoDTO> getAllegatiToSignByDocPrincipale(String documentTitle, String guid, UtenteDTO utente, SignTypeEnum ste);

	/**
	 * 
	 * @param utente
	 * @param wobNumber
	 * @param documentTitle
	 * @param ste
	 * @return
	 */
	LocalSignContentDTO getDocumentoPrincipale4FirmaLocale(UtenteDTO utente, String wobNumber, String documentTitle, SignTypeEnum ste, SottoCategoriaDocumentoUscitaEnum sottoCategoriaDocumentoUscita);

	/**
	 * 
	 * @param utente
	 * @param wobNumber
	 * @param documentTitle
	 * @param ste
	 * @return
	 */
	LocalSignContentDTO getAllegato4FirmaLocale(UtenteDTO utente, String documentTitle, SignTypeEnum ste, SottoCategoriaDocumentoUscitaEnum sottoCategoriaDocumentoUscita);

	/**
	 * In caso si stia eseguento una firma pades, esegue il merge di content, digest firmato ed eventuale glifo di firma.	  
	 * 
	 * @param utente
	 * @param ste
	 * @param content - La versione stampigliata del documento
	 * @param docSigned - La versione firmata del documento
	 * @param dataFirma
	 * @param documentTitle
	 * @param principale
	 * @return
	 */
	byte[] manageSignedContent(UtenteDTO utente, SignTypeEnum ste, byte[] content, byte[] docSigned, Date dataFirma, String documentTitle, boolean principale, boolean copiaConforme);

	/**
	 * Completa il processo di firma nel momento in cui sono stati firmati
	 * correttamente documento principale e tutti gli allegati ad esso associati.
	 * 
	 * @param utente
	 * @param ste
	 * @param wobNumber
	 * @param firmaMultipla
	 * @param idDocumentiNPS
	 * @return
	 */
	EsitoOperazioneDTO completeLocalSignProcess(UtenteDTO utente, SignTypeEnum ste, String wobNumber, boolean firmaMultipla, List<String> idDocumentiNPS,
			List<String> guidAttachSentToNPS);

	/**
	 * Aggiorna il metadato trasformazionePDFErrore.
	 * 
	 * @param signTransactionId
	 * @param documentTitle
	 * @param utente
	 * @param metadatoPdfErrorValue
	 */
	void aggiornaMetadatoTrasformazione(String signTransactionId, String documentTitle, UtenteDTO utente, Integer metadatoPdfErrorValue);

	/**
	 * Verifica firma e aggiorna il metadato verificaFirma su CE.
	 * 
	 * @param guid   GUID del documento
	 * @param utente Utente che esegue l'operazione
	 * @return Il DTO contenente le informazioni di firma
	 */
	VerifyInfoDTO aggiornaVerificaFirma(String guid, UtenteDTO utente);

	/**
	 * Verifica la validità della firma per il content passato in input.
	 * 
	 * @param contentFile
	 * @return
	 */
	boolean isValidSignature(InputStream contentFile, UtenteDTO utente, boolean isXades);

	/**
	 * Restituisce true se il documento ha almeno una firma. Non controlla se le
	 * firme sono valide.
	 * 
	 * @param contentFile
	 * @return
	 */
	boolean hasSignatures(InputStream contentFile, UtenteDTO utente);

	/**
	 * Verifica se per tutti i documenti in input esiste almeno un allegato da
	 * attestare per copia conforme.
	 * 
	 * @param utente
	 * @param documentiSelezionati
	 * @return il messaggio d'errore utente in caso esista almeno un documento in
	 *         input non associato ad allegati da attestare per copia conforme
	 */
	String checkAllegatiCopiaConforme(UtenteDTO utente, List<MasterDocumentRedDTO> documentiSelezionati);

	/**
	 * Recupera gli allegati da attestare per copia conforme relativi al documento
	 * in input.
	 * 
	 * @param documentTitle - Document Title del documento principale
	 * @param guid - GUID del documento principale
	 * @param utente
	 * @return allegati lista dei documentTitle/nomeFile degli allegati recuperati
	 */
	 List<AllegatoDTO> getAllegatiToSignCopiaConformeByDocPrincipale(String documentTitle, String guid, UtenteDTO utente);

	/**
	 * 
	 * @param utente
	 * @param wobNumber
	 * @param documentTitle
	 * @return
	 */
	LocalSignContentDTO getAllegato4CopiaConforme(UtenteDTO utente, String documentTitle);

	/**
	 * Salva l'allegato firmato per copia conforme (eseguendo prima il merge di
	 * content con il digest firmato) su filenet.
	 * 
	 * @param utente
	 * @param content
	 * @param docSigned
	 * @param dataFirma
	 * @param documentTitle
	 * @param nomeAllegato
	 * @return
	 */
	String saveAllegatoCopiaConforme(UtenteDTO utente, byte[] content, byte[] docSigned, Date dataFirma, String documentTitle, String nomeAllegato);

	/**
	 * Completa il processo di firma per copia conforme nel momento in cui sono
	 * stati firmati correttamente tutti gli allegati di un documento principale.
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	EsitoOperazioneDTO completeCopiaConformeProcess(UtenteDTO utente, String wobNumber);

	/**
	 * Metodo che controlla i destinatari interni dei documenti selezionati.
	 * 
	 * @param docList
	 * @return
	 */
	Boolean checkDocumentsHasSoloDestinatariInterni(UtenteDTO utente, List<MasterDocumentRedDTO> docList);

	/**
	 * Metodo che controlla il signer del timbro sul documento
	 * 
	 * @param contentFile
	 * @param utente
	 * @param signerAoo
	 * @return
	 */
	boolean hasSignaturesForSpedizione(InputStream contentFile, UtenteDTO utente, String signerAoo);

	/**
	 * Ottiene una mappa dei metadati del protocollo da aggiornare a seguito della
	 * protocollazione.
	 * 
	 * @param protocollo
	 * @param utente
	 * @param aggiornaDataProtocollo
	 * @return mappa property oggetto
	 */
	/**
	 * @param protocollo
	 * @param utente
	 * @param aggiornaDataProtocollo
	 * @return
	 */
	Map<String, Object> getMetadatiProtocolloToUpdate(ProtocolloDTO protocollo, UtenteDTO utente, boolean aggiornaDataProtocollo);

	/**
	 * ottiente la verifica delle info di firma.
	 * 
	 * @param docContent
	 * @param utente
	 * @return verifica delle info
	 */
	VerifyInfoDTO obtainVerifyInfoFirma(InputStream docContent, String mimeType, UtenteDTO utente);

	/**
	 * Gestisce i destinatari interni.
	 * 
	 * @param documentFilenet
	 * @param documentDetail
	 * @param protocollo
	 * @param utenteFirmatario
	 * @param fceh
	 * @param con
	 */
	void gestisciDestinatariInterni(Document documentFilenet, DetailDocumentoDTO documentDetail, ProtocolloDTO protocollo, UtenteDTO utenteFirmatario, IFilenetCEHelper fceh,
			Connection con);
	
	/**
	 * Recupera il glifo del delegato.
	 * 
	 * @param utente
	 * @return 
	 */
	HashMap<String, byte[]> recuperaGlifoDelegato(UtenteDTO utente);


	/**
	 * Verifica la firma del documento.
	 * 
	 * @param fceh
	 * @param docPrincipale
	 */
	void aggiornaVerificaFirmaDocAllegato(IFilenetCEHelper fceh, Document docPrincipale);

	/**
	 * Verifica firma dell'allegato.
	 * 
	 * @param idAoo
	 * @param guid
	 * @param fcDTO
	 */
	void verificaFirmaAllegatoVisualizzaComponent(Long idAoo, String guid, FilenetCredentialsDTO fcDTO);
	/**
	 * Verifica la firma dei documenti.
	 * @param idAoo
	 * @param documentTitle
	 * @param utente
	 * @param hasAllegati
	 * @return lista delle verifiche
	 */

	List<VerificaFirmaPrincipaleAllegatoDTO> getMetadatoVerificaFirmaPrincAllegato(Long idAoo, String documentTitle,
			UtenteDTO utente, boolean hasAllegati, MasterDocumentRedDTO dto);

	/**
	 * Restituisce true se esistono allegati firmati non verificati.
	 * @param idAoo
	 * @param guid
	 * @param utente
	 * @return true se esistono allegati firmati non verificati, false altrimenti
	 */
	boolean sonoPresentiAllegatiFirmatiNonVerificati(Long idAoo, String guid, UtenteDTO utente);

	/**
	 * @param utenteDelegato
	 */
	void recuperaGlifoDelegante(UtenteDTO utenteDelegato);

	/**
	 * @param signTransactionId
	 * @param wobNumbers
	 * @param utente
	 * @return EsitoOperazioneDTO
	 */
	Collection<EsitoOperazioneDTO> protocollazione(String signTransactionId, Collection<String> wobNumbers, UtenteDTO utente);

	/**
	 * @param signTransactionId
	 * @param wobNumbers
	 * @param utente
	 * @return EsitoOperazioneDTO
	 */
	Collection<EsitoOperazioneDTO> registrazioneAusiliaria(String signTransactionId, Collection<String> wobNumbers, UtenteDTO utente);

	/**
	 * Restituisce l'immagine del glifo della specifica aoo.
	 * 
	 * @param idAOO
	 *            id area organizzativa
	 * @param confPDFA
	 *            flag associato alla transparency della postilla
	 * @return byte array dell'immagine
	 */
	byte[] getImageFirmaAOO(Long idAOO, boolean confPDFA);


	
}