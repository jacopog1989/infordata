package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IScompattaMailFacadeSRV;

/**
 * Interfaccia del servizio di scompattamento mail.
 */
public interface IScompattaMailSRV extends IScompattaMailFacadeSRV {
 
}
