package it.ibm.red.business.service.ws;

import it.ibm.red.business.service.ws.facade.IOrganigrammaWsFacadeSRV;

/**
 * The Interface IOrganigrammaWsSRV.
 *
 * @author a.dilegge
 * 
 *         Interfaccia servizio gestione organigramma web service.
 */
public interface IOrganigrammaWsSRV extends IOrganigrammaWsFacadeSRV {

	
	
}