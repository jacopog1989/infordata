package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;

import it.ibm.red.business.persistence.model.RedWsFlusso;

/**
 * 
 * @author m.crescentini
 *
 *	DAO per la gestione dei flussi associati ai web service esposti da Red EVO.
 */
public interface IRedWsFlussoDAO extends Serializable {
	
	
	/**
	 * @param idFlusso
	 * @param con
	 * @return
	 */
	RedWsFlusso getById(int idFlusso, Connection con);
	
	
	/**
	 * @param codiceFlusso
	 * @param con
	 * @return
	 */
	RedWsFlusso getByCodice(String codiceFlusso, Connection con);

}
