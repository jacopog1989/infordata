package it.ibm.red.business.service.facade;

import java.io.Serializable;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;

/**
 * Facade del servizio di rifiuto spedizione.
 */
public interface IRifiutaSpedizioneFacadeSRV extends Serializable {

	/**
	 * Rifiuta la spedizione.
	 * @param fcDTO
	 * @param wobNumber
	 * @param motivazione
	 * @param idUtente
	 * @param numDocumento
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO rifiuta(FilenetCredentialsDTO fcDTO, String wobNumber, String motivazione, int idUtente, Integer numDocumento);
	
}
