package it.ibm.red.business.service.ws;

import it.ibm.red.business.service.ws.facade.IRedStampigliaturaDocWsFacadeSRV;

/**
 * The Interface IRedStampigliaturaDocWsSRV.
 *
 * @author m.crescentini
 * 
 *         Interfaccia servizio stampigliatura documenti web service.
 */
public interface IRedStampigliaturaDocWsSRV extends IRedStampigliaturaDocWsFacadeSRV {

	
	
}