package it.ibm.red.business.service.ws.auth;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import it.ibm.red.webservice.model.documentservice.dto.Servizio;

/**
 * Interface Document service authorizable.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface DocumentServiceAuthorizable {

	/**
	 * @return servizio
	 */
	Servizio servizio();
	
}
