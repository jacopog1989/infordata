package it.ibm.red.business.service.facade;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.TestoPredefinitoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.persistence.model.Contatto;

/**
 * Facade del servizio che gestisce l'inoltro di mail da coda attiva.
 */
public interface IInoltraMailDaCodaAttivaFacadeSRV extends Serializable {
	
	/**
	 * Ottiene i  testi predefiniti.
	 * @param utente
	 * @return lista di testi predefiniti
	 */
	List<TestoPredefinitoDTO> getTestiPredefiniti(UtenteDTO utente);
	
	/**
	 * Aggiunge il messaggio alla coda di inoltro per l'invio in automatico.
	 * @param utente
	 * @param wobNumber
	 * @param mittente
	 * @param destinatari
	 * @param oggettoMail
	 * @param testoMail
	 * @param modalitaSpedizioneSc
	 * @param docSelezionati
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO inviaMail(UtenteDTO utente, String wobNumber, CasellaPostaDTO mittente, List<DestinatarioRedDTO> destinatari, String oggettoMail, 
			String testoMail, Integer modalitaSpedizioneSc, List<DocumentoAllegabileDTO> docSelezionati);

	/**
	 * Verifica che il contatto sia corretto e lo trasforma in un destinatarioRedDTO.
	 * In caso il contatto non sia corretto ritorna null.
	 * 
	 * @param c
	 * 	Comtatto da aggiungere.
	 * 
	 * @return
	 * 	il destinatario esterno e elettronico corrispondente al contatto inserito.
	 */
	DestinatarioRedDTO checkAndTrasformFromContattoToDestinatario(Contatto c);

	/**
	 * Crea l'oggetto mail di default.
	 * 
	 * @param master		master recuperato dalla lista, corrisponde al documento su cui è stata invocata la repsonse.
	 * @param fascicolo		fascicolo procedimentale del master.
	 */
	String createDefaultOggetto(MasterDocumentRedDTO master, FascicoloDTO fascicolo, UtenteDTO utente);

	/**
	 * Restituisce la properties con key
	 * {@link PropertiesNameEnum.INOLTRA_MAIL_CODA_ALLEGATI_MAX_DIM}
	 * .
	 * Divide inoltre la properties per 1024 al fine di mostrare i KB piuttosto che i byte.
	 * 
	 * @return 
	 * il BigDecimal corrispondente ai KB massimi raggiungibili dagli allegati.
	 */
	BigDecimal getDimensioneMassimaTotaleAllegatiMailBigDecimalKB();
	
}