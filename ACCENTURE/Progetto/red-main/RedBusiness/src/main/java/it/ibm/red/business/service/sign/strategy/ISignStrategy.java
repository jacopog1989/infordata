package it.ibm.red.business.service.sign.strategy;

import java.util.Collection;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.ProtocolloEmergenzaDocDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.SignTypeEnum;

/**
 * Interfaccia della strategy di firma.
 */
public interface ISignStrategy {
	
	/**
	 * Esegue il processo di firma remota sui documenti identificati dai wobnumber:
	 * <code> wobNumbers </code>.
	 * 
	 * @param signTransactionId
	 *            transactionId dell'operazione di firma
	 * @param wobNumbers
	 *            wobnumbers dei documenti da firmare
	 * @param pin
	 *            pin per la firma digitale
	 * @param otp
	 *            one-time-password per la firma digitale
	 * @param utenteFirmatario
	 *            utente firmatario dei documenti identificati dai
	 *            <code> wobNumbers </code>
	 * @param signType
	 *            tipologia della firma, @see SignTypeEnum
	 * @param principaleOnlyPAdESVisible
	 *            flag associato alla visibilità PADES
	 * @param firmaMultipla
	 *            true se firma multipla, false altrimenti
	 * @return gli esiti delle firme per ogni documenti
	 */
	Collection<EsitoOperazioneDTO> firmaRemota(String signTransactionId, Collection<String> wobNumbers, String pin, String otp, UtenteDTO utenteFirmatario, SignTypeEnum signType,
			boolean principaleOnlyPAdESVisible, boolean firmaMultipla);
	
	/**
	 * Esegue il processo di firma autografa sui documenti identificati dai
	 * <code> wobNumbers </code>.
	 * 
	 * @param signTransactionId
	 *            transactionId dell'operazione di firma
	 * @param wobNumbers
	 *            identificativi univoci dei documenti
	 * @param utenteFirmatario
	 *            utente responsabile per la firma dei documenti identificati dai
	 *            <code> wobNumbers </code>
	 * @param protocolloEmergenza
	 *            protocollo emergenza specificato
	 * @return lista degli esiti associati ad ogni documento mandato in firma
	 */
	Collection<EsitoOperazioneDTO> firmaAutografa(String signTransactionId, Collection<String> wobNumbers, UtenteDTO utenteFirmatario, ProtocolloEmergenzaDocDTO protocolloEmergenza);
	
	/**
	 * Esegue le azioni precedenti alla firma locale del documento identificato dal:
	 * <code> wobNumber </code>.
	 * 
	 * @param signTransactionId
	 *            transactionId dell'operazione di firma
	 * @param wobNumber
	 *            identificativo univoco del documento in firma
	 * @param utenteFirmatario
	 *            utente responsabile per la firma locale del documento identificato
	 *            dal: <code> wobNumber </code>
	 * @param signType
	 *            tipologia di firma, @see SignTypeEnum
	 * @param firmaMultipla
	 *            flag che indica se la firma locale è una firma multipla
	 * @return esito della firma
	 */
	EsitoOperazioneDTO preFirmaLocale(String signTransactionId, String wobNumber, UtenteDTO utenteFirmatario, SignTypeEnum signType, boolean firmaMultipla);
	
	/**
	 * Esegue le azioni successive alla firma locale.
	 * 
	 * @param signTransactionId
	 *            transactionId dell'operazione di firma
	 * @param signOutcome
	 *            esito della firma locale
	 * @param utenteFirmatario
	 *            utente responsabile della firma locale
	 * @param signType
	 *            tipologia della firma applicata, @see SignTypeEnum
	 * @param copiaConforme
	 *            flag associato alla copia conforme
	 */
	void postFirmaLocale(String signTransactionId, EsitoOperazioneDTO signOutcome, UtenteDTO utenteFirmatario, SignTypeEnum signType, boolean copiaConforme);
	
}
