package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IProvinciaDAO;
import it.ibm.red.business.dto.ProvinciaDTO;
import it.ibm.red.business.dto.SelectItemDTO;
import it.ibm.red.business.exception.RedException;

/**
 * DAO gestione province.
 */
@Repository
public class ProvinciaDAO extends AbstractDAO implements IProvinciaDAO {

	/**
	 * La costante serial Version UID.
	 */
	private static final long serialVersionUID = -6374892148516415990L;

	/**
	 * Campo id provincia.
	 */
	private static final String PROVINCIA_ID = "IDPROVINCIAISTAT";

	/**
	 * Campo denominazione provincia.
	 */
	private static final String PROVINCIA_DENOMINAZIONE = "DENOMINAZIONE";

	/**
	 * Campo id regione.
	 */
	private static final String PROVINCIA_ID_REGIONE = "IDREGIONEISTAT";

	/**
	 * Campo sigla provincia.
	 */
	private static final String PROVINCIA_SIGLA = "SIGLAPROVINCIA";

	/**
	 * Campo flag provincia soppressa.
	 */
	private static final String PROVINCIA_FLAG_SOPPRESSA = "SOPPRESSA";

	/**
	 * Restituisce tutte le province complete delle informazioni.
	 *
	 * @param connection
	 * @return province
	 */
	@Override
	public Collection<ProvinciaDTO> getAll(final Connection connection) {

		PreparedStatement ps = null;
		ResultSet rs = null;
		final String sql = "SELECT * FROM provincia";
		final Collection<ProvinciaDTO> out = new ArrayList<>();
		try {
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				out.add(populateProvincia(rs));
			}
		} catch (final SQLException e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return out;
	}

	/**
	 * Restituisce la provincia identificata dall'id.
	 *
	 * @param connection
	 * @param idProvincia
	 * @return provincia
	 */
	@Override
	public ProvinciaDTO getProvincia(final String idProvincia, final Connection connection) {

		PreparedStatement ps = null;
		ResultSet rs = null;
		final String sql = "SELECT * FROM provincia WHERE IDPROVINCIAISTAT = ?";
		ProvinciaDTO out = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, idProvincia);
			rs = ps.executeQuery();
			while (rs.next()) {
				out = populateProvincia(rs);
			}
		} catch (final SQLException e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return out;
	}

	/**
	 * @see it.ibm.red.business.dao.IProvinciaDAO#getProvince(java.sql.Connection,
	 *      java.lang.Long).
	 */
	@Override
	public List<ProvinciaDTO> getProvince(final Connection conn, final Long idRegione) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final String sql = "SELECT * FROM provincia WHERE IDREGIONEISTAT = ?";
		final List<ProvinciaDTO> out = new ArrayList<>();
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, idRegione.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				out.add(populateProvincia(rs));
			}
		} catch (final SQLException e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return out;
	}

	/**
	 * @see it.ibm.red.business.dao.IProvinciaDAO#getProvinceFromDesc(java.sql.Connection,
	 *      java.lang.String).
	 */
	@Override
	public List<ProvinciaDTO> getProvinceFromDesc(final Connection conn, final String query) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final String sql = "SELECT * FROM provincia WHERE lower(denominazione) like ?";
		final List<ProvinciaDTO> out = new ArrayList<>();
		try {
			ps = conn.prepareStatement(sql);
			ps.setString(1, "%" + query.toLowerCase() + "%");
			rs = ps.executeQuery();
			while (rs.next()) {
				out.add(populateProvincia(rs));
			}
		} catch (final SQLException e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return out;
	}

	/**
	 * @see it.ibm.red.business.dao.IProvinciaDAO#getProvinciaByNome(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public ProvinciaDTO getProvinciaByNome(final String nomeProvincia, final Connection connection) {

		PreparedStatement ps = null;
		ResultSet rs = null;
		final String sql = "SELECT * FROM provincia WHERE DENOMINAZIONE = ?";
		ProvinciaDTO out = null;
		try {
			ps = connection.prepareStatement(sql);
			ps.setString(1, nomeProvincia);
			rs = ps.executeQuery();
			while (rs.next()) {
				out = populateProvincia(rs);
			}
		} catch (final SQLException e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return out;
	}

	/**
	 * @see it.ibm.red.business.dao.IProvinciaDAO#getProvince(java.sql.Connection).
	 */
	@Override
	public Collection<SelectItemDTO> getProvince(final Connection connection) {

		PreparedStatement ps = null;
		ResultSet rs = null;
		final String sql = "SELECT IDPROVINCIAISTAT, DENOMINAZIONE FROM provincia WHERE SOPPRESSA <> 1 ORDER BY DENOMINAZIONE ASC";
		final Collection<SelectItemDTO> out = new ArrayList<>();
		try {
			ps = connection.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				out.add(new SelectItemDTO(rs.getString(PROVINCIA_ID), rs.getString(PROVINCIA_DENOMINAZIONE)));
			}
		} catch (final SQLException e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return out;
	}
	
	/**
	 * Restituisce un DTO di Provincia recuperando le informazioni da un resultSet.
	 * 
	 * @param rs
	 *            resultSet da cui recuperare le informazioni
	 * @return ProvinciaDTO completo
	 * @throws SQLException
	 */
	private static ProvinciaDTO populateProvincia(ResultSet rs) throws SQLException {
		final ProvinciaDTO pTemp = new ProvinciaDTO();
		pTemp.setIdProvincia(rs.getString(PROVINCIA_ID));
		pTemp.setDenominazione(rs.getString(PROVINCIA_DENOMINAZIONE));
		pTemp.setIdRegione(rs.getString(PROVINCIA_ID_REGIONE));
		pTemp.setSiglaProvincia(rs.getString(PROVINCIA_SIGLA));
		pTemp.setSoppressa(rs.getInt(PROVINCIA_FLAG_SOPPRESSA));
		return pTemp;
	}

}
