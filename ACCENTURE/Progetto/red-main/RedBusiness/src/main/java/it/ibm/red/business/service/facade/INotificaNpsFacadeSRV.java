package it.ibm.red.business.service.facade;

import java.io.Serializable;

import it.ibm.red.business.dto.NotificaAzioneNpsDTO;
import it.ibm.red.business.dto.NotificaNpsDTO;
import it.ibm.red.business.dto.NotificaOperazioneNpsDTO;
import it.ibm.red.webservice.model.interfaccianps.dto.ParametersElaboraNotificaAzione;
import it.ibm.red.webservice.model.interfaccianps.dto.ParametersElaboraNotificaOperazione;

/**
 * Facade del servizio di gestione notifiche NPS.
 */
public interface INotificaNpsFacadeSRV extends Serializable {

	/**
     * Accoda una richiesta di elaborazione di una notifica di operazione eseguita da NPS, restituendo la PK dell'item della coda inserito.
     * 
     * @param request   Request NPS
     * @return          PK della coda
     */
	Long accodaNotificaOperazione(ParametersElaboraNotificaOperazione richiesta);
	
	/**
	 * Accoda una richiesta di elaborazione di una notifica relativa a un'azione avvenuta su un sistema esterno, restituendo la PK dell'item della coda inserito.
     * La request verrà trasformata in un nuovo DTO, e questo sarà serializzato nella base dati.
	 * 
	 * @param richiesta
	 * @param idTrack
	 * @return
	 */
	Long accodaNotificaAzione(ParametersElaboraNotificaAzione richiesta, Long idTrack);

	/**
	 * Recupera la prima richiesta da elaborare aggiornandone lo stato IN LAVORAZIONE 
	 * 
	 * @param idAoo
	 * @return
	 */
	NotificaNpsDTO recuperaProssimaRichiestaElabDaLavorare(long idAoo);

	/**
	 * Elabora la spedizione avvenuta su NPS. 
	 * Aggiorna lo stato in coda mail e aggiorna la persistenza su Filenet cosi come avviene i caso di invio mail da parte dell'applicativo.
	 * 
	 * @param item
	 */
	void elaboraNotificaSpedizione(NotificaOperazioneNpsDTO item);

	/**
	 * Elabora una notifica relativa a un'azione avvenuta su un sistema esterno e notifica da NPS.
	 * 
	 * @param item
	 */
	String elaboraNotificaAzione(NotificaAzioneNpsDTO item);
	
	/**
	 * Recupera la notifica NPS di tipo azione associata al protocollo in input.
	 * 
	 * @param idAoo
	 * @param numeroProtocollo
	 * @param annoProtocollo
	 * 
	 * @return notifica NPS associato al protocollo
	 * */
	NotificaNpsDTO getNotificaAzione(Long idAoo, Integer numeroProtocollo, Integer annoProtocollo);
	
	   
	/**
	 * Aggiorna lo stato della notifica di spedizione.
	 * @param codiMessaggio
	 * @param msgId
	 */
	void updateStatoAndManageEmailByCodiMessage(String codiMessaggio, String msgId);
	
	/**
	 * Recupera la richiesta da elaborare identificata dall'id in input aggiornandone lo stato IN LAVORAZIONE 
	 * 
	 * @param idAoo
	 * @return
	 */
	NotificaNpsDTO recuperaRichiestaElab(int idCoda);
}