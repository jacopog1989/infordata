/**
 * 
 */
package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.RegistroRepertorioDTO;
import it.ibm.red.business.service.facade.IRegistroRepertorioFacadeSRV;

/**
 * @author APerquoti
 *
 */
public interface IRegistroRepertorioSRV extends IRegistroRepertorioFacadeSRV {

	/**
	 * Metodo per recuperare le occorrenze configurate per Aoo e restituire una struttura 
	 * che tenga conto per un 'TipoProcedimento' più elementi di 'TipologiaDocumento'.
	 * 
	 * @param idAoo
	 * @return
	 */
	Map<Long, List<RegistroRepertorioDTO>> getRegistriRepertorioConfigurati(Long idAoo, Connection connection);

	
	/**
	 * Metodo per verificare che un data coppia TipologiaDocumento/TipoProcedimento faccia parte dei Registri Repertorio configurati.
	 * 
	 * @param idAoo
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @param registriRepertorioConfigured
	 * @param connection
	 * @return
	 */
	Boolean checkIsRegistroRepertorio(Long idAoo, Long idTipologiaDocumento, Long idTipoProcedimento, 
			Map<Long, List<RegistroRepertorioDTO>> registriRepertorioConfigured, Connection connection);
	
}
