package it.ibm.red.business.exception;

/**
 * The Class SearchException.
 *
 * @author CPIERASC
 * 
 *         Eccezione in fase di ricerca.
 */
public class SearchException extends Exception {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -6387963369831550300L;
	
	/**
	 * Costruttore.
	 * 
	 * @param message	messaggio
	 */
	public SearchException(final String message) {
		super(message);
	}

}
