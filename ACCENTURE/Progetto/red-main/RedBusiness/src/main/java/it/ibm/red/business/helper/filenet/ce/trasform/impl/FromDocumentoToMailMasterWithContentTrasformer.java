package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;

import org.apache.commons.io.IOUtils;

import com.filenet.api.core.Document;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.provider.ApplicationContextProvider;

/**
 * Trasformer dal Documento al MailMaster con Content.
 */
public class FromDocumentoToMailMasterWithContentTrasformer extends FromDocumentoToMailAbstractTrasformer {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -7061988396258187018L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FromDocumentoToMailMasterWithContentTrasformer.class);

	/**
	 * Dao gestione utente.
	 */
	private final IUtenteDAO utenteDAO;
	
	/**
	 * Costruttore.
	 */
	public FromDocumentoToMailMasterWithContentTrasformer() {
		super(TrasformerCEEnum.FROM_DOCUMENTO_TO_MAIL_MASTER_WITH_CONTENT);
		utenteDAO = ApplicationContextProvider.getApplicationContext().getBean(IUtenteDAO.class);
	}

	/**
	 * Esegue la trasformazione del document.
	 * @param document
	 * @param connection
	 * @return EmailDTO
	 */
	@Override
	public EmailDTO trasform(final Document document, final Connection connection) {
		try {
			final EmailDTO m = trasform(document, connection, TrasformerCEEnum.FROM_DOCUMENTO_TO_MAIL_MASTER_WITH_CONTENT);
			
			if (m.getIdUtenteProtocollatore() != null && m.getIdUtenteProtocollatore() != 0) {
				final Utente utenteProtocollatore = utenteDAO.getUtente(m.getIdUtenteProtocollatore().longValue(), connection);
				if (utenteProtocollatore != null) {
					m.setDescUtenteProtocollatore(utenteProtocollatore.getNome() + " " + utenteProtocollatore.getCognome());
				}
			}

			String testoMail = Constants.EMPTY_STRING;
			if (FilenetCEHelper.hasDocumentContentTransfer(document)) {
				testoMail = IOUtils.toString(FilenetCEHelper.getDocumentContentAsInputStream(document));
			}
			m.setTesto(testoMail);
			
			return m;
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero di un metadato del dettaglio del documento: ", e);
			return null;
		}
	}
	
}