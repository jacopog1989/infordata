package it.ibm.red.business.helper.signing;

import java.io.Serializable;

import it.ibm.red.business.enums.FontFamilyEnum;
import it.ibm.red.business.enums.FontStyleEnum;
import it.ibm.red.business.enums.TextAlignEnum;

/**
 * The Class SignatureLayout.
 *
 * @author CPIERASC
 * 
 *         Classe utilizzata per definire il layout delle firme.
 */
public class SignatureLayout implements Serializable {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 2445551628547805056L;

	/**
	 * Special field: given name.
	 */
	public static final String SF_GIVEN_NAME = "%g%";
	
	/**
	 * Special field: subject name.
	 */
	public static final String SF_SUBJECT_DISTINGUISHED_NAME = "%dn%";
	
	/**
	 * Special field: surname.
	 */
	public static final String SF_SURENAME = "%sn%";
	
	/**
	 * Special field:common name.
	 */
	public static final String SF_COMMON_NAME = "%cn%";
	
	/**
	 * Special field:organization.
	 */
	public static final String SF_ORGANIZATION = "%o%";
	
	/**
	 * Special field:organization unit.
	 */
	public static final String SF_ORGANIZATION_UNIT = "%ou%";
	
	/**
	 * Special field: serial number.
	 */
	public static final String SF_SERIAL_NUMBER = "%SerialNumber%";
	
	/**
	 * Special field: pseudonimo.
	 */
	public static final String SF_PSEUDONYM = "%pseudonym%";
	
	/**
	 * Special field: name qualifier.
	 */
	public static final String SF_DISTINGUISHED_NAME_QUALIFIER = "%dnQualifier%";
	
	/**
	 * Special field: title.
	 */
	public static final String SF_TITLE = "%title%";
	
	/**
	 * Special field: locality.
	 */
	public static final String SF_LOCALITY = "%l%";
	
	/**
	 * Special field: state.
	 */
	public static final String SF_STATE_OR_PROVINCE = "%st%";
	
	/**
	 * Special field: description.
	 */
	public static final String SF_DESCRIPTION = "%d%";
	
	/**
	 * Special field: reason.
	 */
	public static final String SF_REASON = "%reason%";
	
	/**
	 * Special field: contact.
	 */
	public static final String SF_CONTACT = "%contact%";
	
	/**
	 * Special field: location.
	 */
	public static final String SF_LOCATION = "%location%";
	
	/**
	 * Special field: signing date.
	 */
	public static final String SF_SIGNING_DATE = "%date%";
	
	/**
	 * Format date.
	 */
	private static final String DATE_FORMAT = "dd MMMM yyyy";

	/**
	 * Language it.
	 */
	private static final String DATE_LANG = "it";

	/**
	 * Empty layout.
	 */
	public static final SignatureLayout EMPTY = new SignatureLayout("", TextAlignEnum.CENTER, FontFamilyEnum.COURIER, 6, FontStyleEnum.NORMAL, 1);

	/**
	 * Standard layout.
	 */
	public static final SignatureLayout STANDARD_SIGNATURE_LAYOUT = new SignatureLayout("Firmatario: " + SF_COMMON_NAME + "\n" 
            + SF_ORGANIZATION + "\n", TextAlignEnum.CENTER, FontFamilyEnum.HELVETICA, 6, FontStyleEnum.NORMAL, 1);
	
	/**
	 * Testo.
	 */
	private String text;

	/**
	 * Allineamento testo.
	 */
	private TextAlignEnum textAlign;

	/**
	 * Font testo.
	 */
	private FontFamilyEnum fontFamily;

	/**
	 * Dimensione testo.
	 */
	private Integer fontSize;

	/**
	 * Style testo.
	 */
	private FontStyleEnum fontStyle;

	/**
	 * Colore testo.
	 */
	private Integer fontColorRGB;


	/**
	 * Costruttore.
	 * 
	 * @param inText			testo
	 * @param inTextAlign		allineamento testo
	 * @param inFontFamily		font
	 * @param inFontSize		dimensione font
	 * @param inFontStyle		stile font
	 * @param inFontColorRGB	colore font
	 */
	public SignatureLayout(final String inText, final TextAlignEnum inTextAlign, final FontFamilyEnum inFontFamily, final Integer inFontSize, 
			final FontStyleEnum inFontStyle, final Integer inFontColorRGB) {
		this.text = inText;
		this.textAlign = inTextAlign;
		this.fontFamily = inFontFamily;
		this.fontSize = inFontSize;
		this.fontStyle = inFontStyle;
		this.fontColorRGB = inFontColorRGB;
	}
	
	/**
	 * 
	 * tags:
	 * @return
	 */
	@Override
	public final String toString() {
		StringBuilder str = new StringBuilder();
		str.append("<Text>").append(text).append("</Text>");
		str.append("<TextAlign>").append(textAlign.getValue()).append("</TextAlign>");
		str.append("<FontColor>").append(fontColorRGB).append("</FontColor>");
		str.append("<FontStyle>").append(fontStyle.getValue()).append("</FontStyle>");
		str.append("<FontSize>").append(fontSize).append("</FontSize>");
		str.append("<FontFamily>").append(fontFamily.getValue()).append("</FontFamily>");
		str.append("<DateFormat>").append(DATE_FORMAT).append("</DateFormat>");
		str.append("<DateLang>").append(DATE_LANG).append("</DateLang>");
		return str.toString();
	}
}
