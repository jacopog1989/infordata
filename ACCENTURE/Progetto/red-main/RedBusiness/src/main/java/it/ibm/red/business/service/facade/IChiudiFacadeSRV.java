package it.ibm.red.business.service.facade;

import java.io.Serializable;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Facade del servizio di gestione chiusura.
 */
public interface IChiudiFacadeSRV extends Serializable {

	/**
	 * Esegue la chiusura.
	 * @param utente
	 * @param wobNumber
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO chiudi(UtenteDTO utente, String wobNumber);
	
}
