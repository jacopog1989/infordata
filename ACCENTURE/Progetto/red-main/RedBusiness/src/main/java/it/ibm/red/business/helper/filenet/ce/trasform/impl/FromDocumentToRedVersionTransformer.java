package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.Date;
import java.util.Map;

import com.filenet.api.core.Document;
import com.google.common.net.MediaType;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.VersionDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.enums.ValueMetadatoVerificaFirmaEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * Trasformer dal Documento to RedVersion.
 */
public class FromDocumentToRedVersionTransformer extends ContextTrasformerCE<VersionDTO> {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 1795229027161001185L;
	
	/**
	 * LOGGER.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FromDocumentToRedVersionTransformer.class);
	
	/**
	 * @param inEnumKey
	 */
	public FromDocumentToRedVersionTransformer() {
		super(TrasformerCEEnum.FROM_DOCUMENT_TO_RED_VERSION);
	}
	
	/**
	 * Esegue la trasformazione del document.
	 * @param document
	 * @param context
	 * @return VersionDTO
	 */
	@Override
	public VersionDTO trasform(final Document document, final Map<ContextTrasformerCEEnum, Object> context) {
		VersionDTO v = null;
		
		try {
			if (document == null) {
				throw new RedException("Il document di filenet e' null!");
			}
			
			final PropertiesProvider pp = PropertiesProvider.getIstance();
			
			final String guid = StringUtils.cleanGuidToString(document.get_Id());
			final Date dateCheckedIn = document.get_DateCheckedIn();
			final String mimeType = document.get_MimeType();
			final boolean currentVersion = document.get_IsCurrentVersion();
			final boolean frozenVersion = document.get_IsFrozenVersion();
			final String className = document.getClassName();
			final String creator = document.get_Creator();
			String fileName = (String) getMetadato(document, PropertiesNameEnum.NOME_FILE_METAKEY);
			final Integer formatoDocumento = (Integer) getMetadato(document, PropertiesNameEnum.ID_FORMATO_DOCUMENTO);
			
			// Gestione nome file per documento cartaceo
			if (formatoDocumento != null && FormatoDocumentoEnum.CARTACEO.getId().intValue() == formatoDocumento && StringUtils.isNullOrEmpty(fileName) 
					&& StringUtils.isNullOrEmpty(mimeType)) {
				fileName = getMetadato(document, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY) + "-" + getMetadato(document, PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY);
			}
			
			// ### Validità firma -> START
			ValueMetadatoVerificaFirmaEnum valoreVerificaFirma = null;
			
			final String classeDocumentale = document.getClassName();
			final Integer idCategoriaDocumento = (Integer) getMetadato(document, PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY);
			
			Integer idFormatoAllegato = null;
			Integer idCategoriaDocumentoPrincipale = null;
			boolean isFlussoSipad = false;
			final String classeDocumentaleAllegato = pp.getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY);
			if (classeDocumentaleAllegato.equals(classeDocumentale)) {
				idFormatoAllegato = (Integer) getMetadato(document, PropertiesNameEnum.FORMATO_ALLEGATO_ID_METAKEY);
				
				// Si recupera la categoria del documento principale
				idCategoriaDocumentoPrincipale = (Integer) context.get(ContextTrasformerCEEnum.ID_CATEGORIA_DOC_PRINCIPALE);
				
				// Codice flusso documento principale.
				isFlussoSipad = Constants.Varie.CODICE_FLUSSO_SIPAD.equals(context.get(ContextTrasformerCEEnum.CODICE_FLUSSO_PRINCIPALE_METAKEY));
			}
			
			boolean visualizzaInfoVerificaFirma = false;
			// Va avviato il controllo di firma solo su file PDF e P7M e se...
			if ((MediaType.PDF.toString().equalsIgnoreCase(mimeType)
					|| (fileName != null && (FileUtils.isP7MFile(mimeType) || fileName.endsWith(".pdf") || fileName.endsWith(".p7m") || fileName.endsWith(".PDF") || fileName.endsWith(".P7M")))) 
					|| (fileName != null && isFlussoSipad && Constants.ContentType.TEXT_XML.equals(mimeType) && !Constants.Varie.SEGNATURA_FILENAME.equals(fileName) && fileName.endsWith(".xml"))
					&&
					((// ...è la prima versione del documento (principale e allegati) e il documento è in entrata (1) o in uscita (2, 3, 8)
						(//numVersione != null && numVersione == 1 
							/*&&*/ (CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0].equals(idCategoriaDocumento) 
									|| CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0].equals(idCategoriaDocumentoPrincipale))
								|| (CategoriaDocumentoEnum.USCITA.equals(CategoriaDocumentoEnum.get(idCategoriaDocumento))
									|| CategoriaDocumentoEnum.USCITA.equals(CategoriaDocumentoEnum.get(idCategoriaDocumentoPrincipale)))) 
							&& 
							(// Principale
								(!classeDocumentaleAllegato.equals(classeDocumentale))
								||
								// Allegati
								(FormatoAllegatoEnum.FIRMATO_DIGITALMENTE.getId().equals(idFormatoAllegato) 
									|| FormatoAllegatoEnum.ELETTRONICO.getId().equals(idFormatoAllegato))
							)
						)
						||  
						(// ...oppure se tratta di un allegato e la categoria del documento principale è una tra USCITA - MOZIONE - INTERNO 
							(classeDocumentaleAllegato.equals(classeDocumentale) && FormatoAllegatoEnum.FIRMATO_DIGITALMENTE.getId().equals(idFormatoAllegato)) 
							&&
							((CategoriaDocumentoEnum.INTERNO.getIds()[0].equals(idCategoriaDocumentoPrincipale) /*|| OR NSD */) 
									|| (CategoriaDocumentoEnum.MOZIONE.getIds()[0].equals(idCategoriaDocumentoPrincipale)) 
									|| (CategoriaDocumentoEnum.DOCUMENTO_USCITA.getIds()[0].equals(idCategoriaDocumentoPrincipale)))
						) 
					)
			    ) {
				visualizzaInfoVerificaFirma = true;
				
				final Integer metadatoValiditaFirma = (Integer) getMetadato(document, PropertiesNameEnum.METADATO_DOCUMENTO_VALIDITA_FIRMA);
				
				if (metadatoValiditaFirma != null) {
					valoreVerificaFirma = ValueMetadatoVerificaFirmaEnum.get(metadatoValiditaFirma);
				}
			}
			// ### Validità firma -> END
			   
			v = new VersionDTO(guid, dateCheckedIn, mimeType, currentVersion, frozenVersion, className, fileName, valoreVerificaFirma, visualizzaInfoVerificaFirma, creator);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		
		return v;
	}

	/**
	 * Metodo non implementato, da non usare.
	 * @throws RedException.
	 */
	@Override
	public VersionDTO trasform(final Document document, final Connection connection, final Map<ContextTrasformerCEEnum, Object> context) {
		throw new RedException();
	}
	
}