package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.ModificaIterCEDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.utils.DestinatariRedUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * Trasformer modifica iter.
 * 
 * @author CRISTIANOPierascenzi
 *
 */
public class ModificaIterTrasformer extends TrasformerCE<ModificaIterCEDTO> {

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Costruttore.
	 */
	public ModificaIterTrasformer() {
		super(TrasformerCEEnum.MODIFICA_ITER);
	}

	/**
	 * Trasform.
	 *
	 * @param document
	 *            the document
	 * @param connection
	 *            the connection
	 * @return the modifica iter CEDTO
	 */
	@Override
	public final ModificaIterCEDTO trasform(final Document document, final Connection connection) {

		Integer idCategoriaDocumento = (Integer) getMetadato(document, PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY);
		String guid = StringUtils.cleanGuidToString(document.get_Id());
		Integer numDoc = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
		Date dataScadenza = (Date) getMetadato(document, PropertiesNameEnum.DATA_SCADENZA_METAKEY);
		Integer inAnnoProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
		Date inDataProtocollo = (Date) getMetadato(document, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY);
		String inIdProtocollo =  (String) getMetadato(document, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY);
		Integer idFormatoDocumento = (Integer) getMetadato(document, PropertiesNameEnum.ID_FORMATO_DOCUMENTO);
		Integer registroRiservato = (Integer) getMetadato(document, PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY);
		Integer riservato = (Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.RISERVATO_METAKEY);
		Boolean flagRiservato = false;
		if (riservato != null && riservato > 0) {
			flagRiservato = true;
		}
		
		Integer idTipoProcedimento = (Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY);
		String destinatariContributoEsterno = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.DESTINATARIO_CONTRIBUTO_ESTERNO);
		Collection<?> inDestinatari = (Collection<?>) getMetadato(document, PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY);
		// Si normalizza il metadato per evitare errori comuni durante lo split delle singole stringhe dei destinatari
		List<String[]> destinatariDocumento = DestinatariRedUtils.normalizzaMetadatoDestinatariDocumento(inDestinatari);

		return new ModificaIterCEDTO(idCategoriaDocumento, idFormatoDocumento, idTipoProcedimento, registroRiservato, 
				destinatariContributoEsterno, flagRiservato, dataScadenza, destinatariDocumento, inIdProtocollo, inAnnoProtocollo, inDataProtocollo, guid, numDoc);
	}

}