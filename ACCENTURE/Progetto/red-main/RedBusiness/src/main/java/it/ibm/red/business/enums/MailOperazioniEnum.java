package it.ibm.red.business.enums;

/**
 * The Enum MailOperazioniEnum.
 *
 * @author adilegge
 * 
 *         Enum delle operazioni relative alla coda di inoltro rifiuto
 */
public enum MailOperazioniEnum {

	/**
	 * Valore.
	 */
	RIFIUTO_CON_PROTOCOLLAZIONE(1),

	/**
	 * Valore.
	 */
	INOLTRO_CON_PROTOCOLLAZIONE(2),

	/**
	 * Valore.
	 */
	RIFIUTO(3),

	/**
	 * Valore.
	 */
	INOLTRO(4);

	/**
	 * Nome dell'icona.
	 */
	private Integer id; 

	/**
	 * Costruttore.
	 * 
	 * @param inIconName	descrizione icona
	 */
	MailOperazioniEnum(final Integer inId) {
		id = inId;
	}

	/**
	 * Restitusice l'id dell'Enum.
	 * @return id enum
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * Restituisce l'enum associata al value.
	 * @param value
	 * @return enum associata al value
	 */
	public static MailOperazioniEnum get(final Integer value) {
		MailOperazioniEnum output = null;
		for (MailOperazioniEnum q: MailOperazioniEnum.values()) {
			if (q.getId().equals(value)) {
				output = q;
				break;
			}
		}
		return output;
	}

}
