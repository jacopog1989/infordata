package it.ibm.red.business.servlet;

import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
 
/**
 * Log level servlet.
 */
public class LogLevelServlet extends HttpServlet {

	/**
	 * Livello priorità alta.
	 */
	private static final String HIGH = "HIGH";

	/**
	 * Serialization.
	 */
	private static final long serialVersionUID = 9014033592221211376L;
	
	/**LogLevelServlet
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(LogLevelServlet.class.getName());

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected void doGet(final HttpServletRequest request, final HttpServletResponse response) {
    	try {
    		
    		final String secLev = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ACCESS_SECURITY_LEVEL);
    		if (HIGH.equalsIgnoreCase(secLev)) {
    			throw new RedException("L'attuale livello di sicurezza non consente il cambiamento del livello dei log.");
    		}
    		
			final String level = request.getParameter("level");
			final Level logLevel = getLevel(level);
    		((ch.qos.logback.classic.Logger) LoggerFactory.getLogger("root")).setLevel(logLevel);
    		final PrintWriter writer = response.getWriter();
    		LOGGER.error("LOGGATA ERRORE");
    		LOGGER.warn("LOGGATA WARNING");
    		LOGGER.info("LOGGATA INFO");
    		writer.print("NUOVO LIVELLO DI LOG PER ROOT :" + level);
    		writer.close();
    	} catch (final Exception e) {
    		LOGGER.error("Errore nell'aggiornamento del livello di log: ", e);
    	}
	}

	private Level getLevel(final String level) {
		Level output = Level.ALL;
		if ("DEBUG".equalsIgnoreCase(level)) {
			output = Level.DEBUG;
		} else if ("ERROR".equalsIgnoreCase(level)) {
			output = Level.ERROR;
		} else if ("INFO".equalsIgnoreCase(level)) {
			output = Level.INFO;
		} else if ("OFF".equalsIgnoreCase(level)) {
			output = Level.OFF;
		} else if ("TRACE".equalsIgnoreCase(level)) {
			output = Level.TRACE;
		} else if ("WARN".equalsIgnoreCase(level)) {
			output = Level.WARN;
		}
		return output;
	}

}