package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.TemplateDTO;
import it.ibm.red.business.dto.TemplateMetadatoDTO;
import it.ibm.red.business.dto.TemplateMetadatoValueDTO;
import it.ibm.red.business.dto.TemplateMetadatoVersion;
import it.ibm.red.business.dto.TemplateValueDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Facade per l'implementazione dei servizi relativi ai template applicativi.
 * 
 * @author adilegge
 *
 */
public interface ITemplateDocUscitaFacadeSRV extends Serializable {

	/**
	 * Viene restituito l'elenco dei template (no applet) relativi all'aoo
	 * dell'utente in input
	 * 
	 * @param utente
	 * @return
	 */
	List<TemplateDTO> getTemplateDocUscita(UtenteDTO utente);

	/**
	 * Viene restituito l'elenco dei metadati del template in input
	 * 
	 * @param template
	 * @return
	 */
	List<TemplateMetadatoDTO> getMetadati(UtenteDTO utente, TemplateDTO template);

	/**
	 * Vengono restituite le informazioni registrate per il documento in input
	 * 
	 * @param utente
	 * @param idDocumento
	 * @return una mappa vuota se non ci sono info per il documento, altrimenti
	 *         contenente la seguente coppia di valori: il template e la lista dei
	 *         metadati con relativi valori associati
	 */
	Map<TemplateValueDTO, List<TemplateMetadatoValueDTO>> getValues(UtenteDTO utente, String idDocumento);

	/**
	 * Genera il documento a partire dal guid del template, al nome del file ed si
	 * valori coi quali sostituire i placeholder del template
	 * 
	 * @param utente
	 * @param guidTemplate
	 * @param nomeFile
	 * @param metadati
	 * @return
	 */
	FileDTO generaDocumento(UtenteDTO utente, String guidTemplate, String nomeFile, List<TemplateMetadatoValueDTO> metadati);

	/**
	 * Recupera il valore di default del metadato
	 * 
	 * @param dbDefaultValue
	 * @param destinatariDoc
	 * @param oggetto
	 * @param numeroProtocollo
	 * @param annoProtocollo
	 * @param utente
	 * @return
	 */
	String getMetadatoValue(String dbDefaultValue, List<DestinatarioRedDTO> destinatariDoc, String oggetto, Integer numeroProtocollo, Integer annoProtocollo,
			UtenteDTO utente, Integer idMetadatoIntestazione);

	/**
	 * Ottiene le versioni del documento.
	 * 
	 * @param idDocumento
	 * @return lista delle versioni dei metadati del template
	 */
	List<TemplateMetadatoVersion> getVersioniDocumento(Integer idDocumento);

	/**
	 * Ottiene i nuovi metadati del template.
	 * 
	 * @param utente
	 * @param template
	 * @return lista dei metadati del template
	 */
	List<TemplateMetadatoDTO> getMetadatiNew(UtenteDTO utente, TemplateDTO template);

	/**
	 * Ottiene i valori dei metadati.
	 * 
	 * @param idTemplate
	 * @param idDocumento
	 * @param versione
	 * @return lista dei valori dei metadati del template
	 */
	List<TemplateMetadatoValueDTO> getValueMetadato(String idTemplate, Integer idDocumento, Integer versione);

}