package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;

import it.ibm.red.business.dto.AttributiEstesiAttoDecretoDTO;

/**
 * Interfaccia DAO atto decreto.
 */
public interface IAttoDecretoUCBDAO extends Serializable {

	/**
	 * Ottiene le estensioni dei file.
	 * @param connection
	 * @return estensioni dei file
	 */
	Collection<String> getExtension(Connection connection);

	/**
	 * Ottiene i metadati estesi relativi all'Aoo.
	 * @param connection
	 * @param idAOO - id dell'Aoo
	 * @return metadati estesi
	 */
	AttributiEstesiAttoDecretoDTO getAttributiEstesi(Connection connection, Integer idAOO);
	
}
