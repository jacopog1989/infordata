package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IProvinciaDAO;
import it.ibm.red.business.dto.ProvinciaDTO;
import it.ibm.red.business.dto.SelectItemDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IProvinciaSRV;

/**
 * Service che gestisce le province.
 */
@Service
@Component
public class ProvinciaSRV extends AbstractService implements IProvinciaSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -5628674112709215732L;
	
	/**
	 * Messaggio inizio ricerca province.
	 */
	private static final String INIZIO_RICERCA_PROVINCE_MSG = "Inizio Ricerca delle province ";

	/**
	 * Bean name del datasource.
	 */
	private static final String DATA_SOURCE = "DataSource";

	/**
	 * DAO.
	 */
	@Autowired
	private IProvinciaDAO provinciaDAO;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ProvinciaSRV.class);

	/**
	 * Restituisce tutte le province configurate sulla base dati.
	 * 
	 * @see it.ibm.red.business.service.facade.IProvinciaFacadeSRV#getAll()
	 */
	@Override
	public Collection<ProvinciaDTO> getAll() {
		Collection<ProvinciaDTO> pList = null;
		Connection conn = null;
		try {
			LOGGER.info(INIZIO_RICERCA_PROVINCE_MSG);
			conn = ((DataSource) ApplicationContextProvider.getApplicationContext().getBean(DATA_SOURCE))
					.getConnection();
			pList = provinciaDAO.getAll(conn);
		} catch (BeansException | SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(conn);
		}
		return pList;
	}

	/**
	 * Restituisce la provincia identificata dall' <code> id </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IProvinciaFacadeSRV#get(java.lang.Long)
	 */
	@Override
	public ProvinciaDTO get(final String id) {
		ProvinciaDTO prov = null;
		Connection conn = null;
		try {
			LOGGER.info(INIZIO_RICERCA_PROVINCE_MSG);
			conn = ((DataSource) ApplicationContextProvider.getApplicationContext().getBean(DATA_SOURCE))
					.getConnection();
			prov = provinciaDAO.getProvincia(id, conn);
		} catch (BeansException | SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(conn);
		}
		return prov;
	}

	/**
	 * Restituisce le province di riferimento della regione identificata dall'
	 * <code> idRegione </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IProvinciaFacadeSRV#getProvFromRegione(java.lang.Long)
	 */
	@Override
	public List<ProvinciaDTO> getProvFromRegione(final Long idRegione) {
		List<ProvinciaDTO> pList = null;
		Connection conn = null;
		try {
			LOGGER.info(INIZIO_RICERCA_PROVINCE_MSG);
			conn = ((DataSource) ApplicationContextProvider.getApplicationContext().getBean(DATA_SOURCE))
					.getConnection();
			pList = provinciaDAO.getProvince(conn, idRegione);
		} catch (BeansException | SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(conn);
		}
		return pList;
	}

	/**
	 * Restituisce le province associate alla descrizione: <code> query </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IProvinciaFacadeSRV#getProvincieFromDesc(java.lang.String)
	 */
	@Override
	public List<ProvinciaDTO> getProvincieFromDesc(final String query) {
		List<ProvinciaDTO> pList = null;
		Connection conn = null;
		try {
			LOGGER.info(INIZIO_RICERCA_PROVINCE_MSG);
			conn = ((DataSource) ApplicationContextProvider.getApplicationContext().getBean(DATA_SOURCE))
					.getConnection();
			pList = provinciaDAO.getProvinceFromDesc(conn, query);
		} catch (BeansException | SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(conn);
		}
		return pList;
	}

	/**
	 * Restituisce le province recuperate a partire dalla descrizione e dalla
	 * regione.
	 * 
	 * @see it.ibm.red.business.service.facade.IProvinciaFacadeSRV#getProvFromRegioneAndDesc(java.lang.String)
	 */
	@Override
	public List<ProvinciaDTO> getProvFromRegioneAndDesc(final String query) {
		List<ProvinciaDTO> pList = null;
		Connection conn = null;
		try {
			LOGGER.info(INIZIO_RICERCA_PROVINCE_MSG);
			conn = ((DataSource) ApplicationContextProvider.getApplicationContext().getBean(DATA_SOURCE))
					.getConnection();
			pList = provinciaDAO.getProvinceFromDesc(conn, query);
		} catch (BeansException | SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(conn);
		}
		return pList;
	}

	/**
	 * Restituisce le province come lista di @see SelectItemDTO.
	 * 
	 * @see it.ibm.red.business.service.facade.IProvinciaFacadeSRV#getProvince()
	 */
	@Override
	public Collection<SelectItemDTO> getProvince() {
		Collection<SelectItemDTO> out = null;
		Connection conn = null;
		try {
			LOGGER.info(INIZIO_RICERCA_PROVINCE_MSG);
			conn = ((DataSource) ApplicationContextProvider.getApplicationContext().getBean(DATA_SOURCE))
					.getConnection();
			out = provinciaDAO.getProvince(conn);
		} catch (BeansException | SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(conn);
		}
		return out;
	}

}
