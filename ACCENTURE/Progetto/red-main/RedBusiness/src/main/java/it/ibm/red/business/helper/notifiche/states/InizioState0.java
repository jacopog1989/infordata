package it.ibm.red.business.helper.notifiche.states;

import java.util.Map;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.enums.StatoCodaEmailEnum;
import it.ibm.red.business.helper.notifiche.IGestioneMailState;
import it.ibm.red.business.helper.notifiche.NotificaHelper;
import it.ibm.red.business.persistence.model.CodaEmail;

/**
 * Stato 0 - Inizio.
 */
public class InizioState0 implements IGestioneMailState {

	/**
	 * @see it.ibm.red.business.helper.notifiche.IGestioneMailState#getNextNotificaEmailToUpdate(it.ibm.red.business.helper.notifiche.NotificaHelper,
	 *      it.ibm.red.business.persistence.model.CodaEmail, java.util.Map,
	 *      boolean).
	 */
	@Override
	public CodaEmail getNextNotificaEmailToUpdate(final NotificaHelper nh, final CodaEmail notificaEmail, final Map<String, EmailDTO> notifiche, final boolean reInvio) {
		CodaEmail result = notificaEmail;
		
		IGestioneMailState nextState = new InizioState0();
		int nextStatoRicevuta =  notificaEmail.getStatoRicevuta();
			
		int tipoMittente = notificaEmail.getTipoMittente();
				
		if ((tipoMittente == Constants.TipologiaDestinatariMittenti.TIPOLOGIA_DESTINATARI_MITTENTI_PEO)) {
			nextState = new ChiusuraState3();
			nextStatoRicevuta = StatoCodaEmailEnum.CHIUSURA.getStatus();
		} else if (tipoMittente == Constants.TipologiaDestinatariMittenti.TIPOLOGIA_DESTINATARI_MITTENTI_PEC) {
			nextState = new AttesaAccettazioneState1();
			nextStatoRicevuta = StatoCodaEmailEnum.ATTESA_ACCETTAZIONE.getStatus();
		}	
		
		nh.setGestioneMailState(nextState);
		result.setStatoRicevuta(nextStatoRicevuta);

		return result;
	}

}
