package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;

/**
 * 
 * @author a.dilegge
 *
 *	DAO per il log delle invocazioni a RedEvoWS.
 */
public interface IRedWsTrackDAO extends Serializable {

	/**
	 * @param request
	 * @param con
	 * @return l'id con qui è stato inserito il dto
	 */
	long insert(String request, String serviceName, String remoteAddress, Connection con);
	
	/**
	 * @param idTrack
	 * @param esitoOperazione
	 * @param response
	 * @param con
	 * @return
	 */
	void update(Long idTrack, Integer esitoOperazione, String response, Connection con);
	
}