package it.ibm.red.business.service;

import java.io.Serializable;
import java.sql.Connection;

import it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO;

/**
 * Interfaccia del servizio di salvataggio messaggio interoperabile.
 */
public interface ISalvaMessaggioInteropSRV extends Serializable {

	/**
     * Persiste sullo strato di persistenza il messaggio fornito in ingresso e restituisce il guid della mail salvata su Filenet.
     * 
     * @param messaggio messaggio
     * @return GUID della mail salvata su Filenet
     */
	String salvaMessaggio(RichiestaElabMessaggioPostaNpsDTO richiesta, Connection con);
}
