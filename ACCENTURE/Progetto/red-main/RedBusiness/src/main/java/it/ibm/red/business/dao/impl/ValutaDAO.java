package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IValutaDAO;
import it.ibm.red.business.dto.LookupTableDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.ValutaMetadatiDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.utils.StringUtils;

/**
 * DAO per la gestione delle valute.
 * 
 * @author DarioVentimiglia
 */
@Repository
public class ValutaDAO extends AbstractDAO implements IValutaDAO {

	/**
	 * serial version UID.
	 */
	private static final long serialVersionUID = 4350080742375953357L;

	/**
	 * Costruisce le triplette dei metadati per la gestione dei valori a partire dai
	 * {@code metadatiValutaDoc}.
	 * 
	 * @see it.ibm.red.business.dao.IValutaDAO#raggruppaValutaMetadati(java.util.List,
	 *      java.sql.Connection)
	 */
	@Override
	public List<ValutaMetadatiDTO> raggruppaValutaMetadati(List<MetadatoDTO> metadatiValutaDoc, Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ValutaMetadatiDTO> valutaMetadatiList = new ArrayList<>();
		try {
			ps = connection.prepareStatement("SELECT vm.* FROM VALUTA_METADATI vm ORDER BY vm.IDMETADATO_VALUTA");
			
			rs = ps.executeQuery();
			
			while(rs.next()) {
				Integer idMetadatoValuta = rs.getInt("IDMETADATO_VALUTA");
				Integer idMetadatoImporto = rs.getInt("IDMETADATO_IMPORTO");
				Integer idMetadatoImportoEur = rs.getInt("IDMETADATO_IMPORTO_EURO");
				
				ValutaMetadatiDTO metadatoValuta = new ValutaMetadatiDTO();
				
				for(MetadatoDTO m : metadatiValutaDoc) {
					if(m.getId().equals(idMetadatoValuta)) {
						metadatoValuta.setValuta((LookupTableDTO) m);
					}
					else if(m.getId().equals(idMetadatoImporto)) {
						metadatoValuta.setImporto(m);
					}
					else if(m.getId().equals(idMetadatoImportoEur)) {
						metadatoValuta.setImportoEur(m);
					}
				}
				valutaMetadatiList.add(metadatoValuta);
			}
		} catch (Exception e) {
			throw new RedException("Errore durante il caricamento dei metadati di tipo valuta", e);
		} finally {
			closeStatement(ps, rs);
		}
		return valutaMetadatiList;
	}

	/**
	 * Restituisce le associazioni tra i nomi delle valute e il suo valore di
	 * conversione.
	 * 
	 * @see it.ibm.red.business.dao.IValutaDAO#getMetadatiConversione(java.util.Date,
	 *      java.sql.Connection)
	 */
	@Override
	public Map<String, Double> getMetadatiConversione(Date dataCreazione, Connection connection) {
		return getMetadatiConversione(null, dataCreazione, connection);
	}
	
	/**
	 * Restituisce la lista dei metadati per la conversione.
	 * 
	 * @param inCodiceValuta
	 *            codice valuta
	 * @param inDataCreazione
	 *            data creazione
	 * @param connection
	 *            connessione al database
	 * @return mappa di associazione valori di conversione
	 */
	public Map<String, Double> getMetadatiConversione(String inCodiceValuta, Date inDataCreazione, Connection connection){
		PreparedStatement ps = null;
		ResultSet rs = null;
		Map<String, Double> mapConversioneValuta = new HashMap<>();
		try {
			StringBuilder query = new StringBuilder();
			query.append("SELECT vm.* FROM VALUTA_CONVERSIONE vm WHERE TRUNC(vm.DATA_INIZIO) <= ? AND (TRUNC(vm.DATA_FINE) > ? OR vm.DATA_FINE IS NULL)");
			
			if(!StringUtils.isNullOrEmpty(inCodiceValuta)) {
				query.append(" AND vm.CODICE_VALUTA = ?");
			}

			Date dataCreazione = inDataCreazione;
			if(dataCreazione == null) {
				dataCreazione = new Date();
			}
			
			int i = 1;
			ps = connection.prepareStatement(query.toString());
			ps.setDate(i++, new java.sql.Date(dataCreazione.getTime()));
			ps.setDate(i++, new java.sql.Date(dataCreazione.getTime()));
			
			if(!StringUtils.isNullOrEmpty(inCodiceValuta)) {
				ps.setString(i++, inCodiceValuta);
			}
			rs = ps.executeQuery();
			
			while(rs.next()) {
				String codiceValuta = rs.getString("CODICE_VALUTA");
				Double cambio = rs.getDouble("CAMBIO");
				
				mapConversioneValuta.put(codiceValuta, cambio);
			}
		} catch (Exception e) {
			throw new RedException("Errore durante il caricamento dei metadati di tipo valuta", e);
		} finally {
			closeStatement(ps, rs);
		}
		return mapConversioneValuta;
	}
}
