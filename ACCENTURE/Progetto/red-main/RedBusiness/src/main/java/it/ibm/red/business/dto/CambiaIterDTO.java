package it.ibm.red.business.dto;

/**
 * Dto gestione cambio iter.
 * 
 * @author CRISTIANOPierascenzi
 *
 */
public class CambiaIterDTO extends AbstractDTO {

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificativo nuovo iter.
	 */
	private String idNuovoIter;

	/**
	 * Identificativo coordinatore.
	 */
	private Integer idCoordinatore;

	/**
	 * Flag urgente.
	 */
	private Boolean flagUrgente;

	/**
	 * Flag firma digitale RGS.
	 */
	private Boolean flagFirmaDigRGS;
	
	/**
	 * Getter.
	 * 
	 * @return	identificativo nuovo iter
	 */
	public String getIdNuovoIter() {
		return idNuovoIter;
	}
	
	/**
	 * Setter.
	 * 
	 * @param inIdNuovoIter	identificativo nuovo iter
	 */
	public void setIdNuovoIter(final String inIdNuovoIter) {
		this.idNuovoIter = inIdNuovoIter;
	}

	/**
	 * Getter identificativo coordinatore.
	 * 
	 * @return	identificativo coordinatore
	 */
	public Integer getIdCoordinatore() {
		return idCoordinatore;
	}

	/**
	 * Setter.
	 * 
	 * @param inIdCoordinatore	identificativo coordinatore
	 */
	public void setIdCoordinatore(final Integer inIdCoordinatore) {
		this.idCoordinatore = inIdCoordinatore;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	flag urgente
	 */
	public Boolean getFlagUrgente() {
		return flagUrgente;
	}

	/**
	 * Setter flag urgente.
	 * 
	 * @param inFlagUrgente	flag urgente
	 */
	public void setFlagUrgente(final Boolean inFlagUrgente) {
		this.flagUrgente = inFlagUrgente;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	flag firma digitale RGS
	 */
	public Boolean getFlagFirmaDigRGS() {
		return flagFirmaDigRGS;
	}
	
	/**
	 * Setter.
	 * 
	 * @param inFlagFirmaDigRGS	flag firma digitale RGS
	 */
	public void setFlagFirmaDigRGS(final Boolean inFlagFirmaDigRGS) {
		this.flagFirmaDigRGS = inFlagFirmaDigRGS;
	}
}
