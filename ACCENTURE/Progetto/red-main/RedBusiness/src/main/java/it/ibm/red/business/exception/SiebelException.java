package it.ibm.red.business.exception;

/**
 * The Class SiebelException.
 *
 * @author adilegge
 * 
 *         Eccezione Siebel dell'applicativo.
 */
public class SiebelException extends RuntimeException {
	
	/**
	 * Codice.
	 */
	private final Integer code; 

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Costruttore.
	 * 
	 * @param msg	messaggio
	 * @param e		eccezione root
	 */
	public SiebelException(final Integer code, final String msg, final Exception e) {
		super(msg, e);
		this.code = code;
	}

	/**
	 * Costruttore.
	 * @param code
	 * @param msg
	 */
	public SiebelException(final Integer code, final String msg) {
		super(msg);
		this.code = code;
	}

	/**
	 * Restituisce il codice errore.
	 * @return codice
	 */
	public int getCode() {
		return code;
	}
	
}
