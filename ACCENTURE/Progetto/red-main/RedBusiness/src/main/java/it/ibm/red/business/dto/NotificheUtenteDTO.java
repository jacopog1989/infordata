package it.ibm.red.business.dto;
  
 
import it.ibm.red.business.persistence.model.Evento;
import it.ibm.red.business.persistence.model.NotificaContatto;

/**
 * The Class NotificheUtenteDTO.
 *
 * @author m.crescentini
 * 
 * 	Data Transfer Object per le notifiche utente (rubrica, calendario, sottoscrizioni).
 */
public class NotificheUtenteDTO extends AbstractDTO {

	/** 
	 * The Constant serialVersionUID. 
	 */
	private static final long serialVersionUID = -7881998897392420791L;

	/**
	 * DTO per le notifiche sottoscrizioni
	 */
	private final NotificaDTO notificheSottoscrizioni;
	
	/**
	 * Model per le notifiche calendario
	 */
	private final Evento notificheCalendario;
	
	/**
	 * Modele per le notifiche rubrica
	 */
	private final NotificaContatto notificheRubrica;
	  
	/**
	 * rowKey. Parametro per gestire la chiave della riga lato fe
	 */
	private int rowKey; 
	
	  
	
	/**
	 * Costruttore notifiche utente DTO.
	 *
	 * @param notificheSottoscrizioni the notifiche sottoscrizioni
	 * @param notificheCalendario the notifiche calendario
	 * @param notificheRubrica the notifiche rubrica
	 * @param rowKey the row key
	 */
	public NotificheUtenteDTO(final NotificaDTO notificheSottoscrizioni, final Evento notificheCalendario, final NotificaContatto notificheRubrica, final int rowKey) {
		super();
		this.notificheSottoscrizioni = notificheSottoscrizioni;
		this.notificheCalendario = notificheCalendario; 
		this.notificheRubrica = notificheRubrica;
		this.rowKey = rowKey;
		 
	}


	/** 
	 *
	 * @return the notificheCalendario
	 */
	public Evento getNotificheCalendario() {
		return notificheCalendario;
	}


	/**  
	 *
	 * @return the notificheRubrica
	 */
	public NotificaContatto getNotificheRubrica() {
		return notificheRubrica;
	}


	/** 
	 *
	 * @return the notificheSottoscrizioni
	 */
	public NotificaDTO getNotificheSottoscrizioni() {
		return notificheSottoscrizioni;
	}
 
	/** 
	 *
	 * @return the rowKey
	 */
	public int getRowKey() {
		return rowKey;
	}
	
	/** 
	 *
	 * @param rowKey the new row key
	 */
	public void setRowKey(final int rowKey) {
		this.rowKey = rowKey;
	}
}
