/**
 * @author CPIERASC
 *
 *	Questo package contiene le implementazioni dei servizi: un servizio concreto implementerà l'interfaccia contenente
 *	i servizi a grana fine, che a sua volta estenderà l'interfaccia contenente i servizi a grana grossa; ovviamente il
 *	servizio concreto dovrà implementare sia i servizi a grana fine che quelli a grana grossa, sarà a discrezione del
 *	client selezionare quale interfaccia utilizzare, ovviamente sarebbe consigliabile utilizzare la facade, introdotta
 *	appositamente per semplificare l'accesso alla logica di business.
 *
 */
package it.ibm.red.business.service.concrete;
