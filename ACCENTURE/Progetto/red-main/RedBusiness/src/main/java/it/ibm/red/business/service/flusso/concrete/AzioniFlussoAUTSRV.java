package it.ibm.red.business.service.flusso.concrete;

import java.util.Iterator;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import it.ibm.red.business.dto.ResponsesDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.service.flusso.IAzioniFlussoAUTSRV;

/**
 * Servizio azioni flusso AUT.
 */
@Service
public class AzioniFlussoAUTSRV extends AzioniFlussoBaseSRV implements IAzioniFlussoAUTSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -3669117957231931931L;

	/**
	 * Eliminazione response Firma Autografa, Rispondi e Inoltra.
	 */
	@Override
	public final ResponsesDTO refineResponses(final DocumentQueueEnum coda, final ResponsesDTO inResponses) {
		if (inResponses != null && !CollectionUtils.isEmpty(inResponses.getResponsesEnum())) {
			ResponsesRedEnum responseEnum = null;

			Iterator<ResponsesRedEnum> itAllResponsesEnum = inResponses.getResponsesEnum().iterator();
			while (itAllResponsesEnum.hasNext()) {
				responseEnum = itAllResponsesEnum.next();
				
				// Se il documento si trova:
				// a) in Da Lavorare UCB, si rimuovono le response Rispondi e Inoltra
				// b) sul Libro Firma, si rimuove la response Firma Autografa
				if (DocumentQueueEnum.isOneKindOfDaLavorare(coda)
						&& (ResponsesRedEnum.INOLTRA_INGRESSO.getResponse().equals(responseEnum.getResponse()) 
								|| ResponsesRedEnum.RISPONDI_INGRESSO.getResponse().equals(responseEnum.getResponse())) 
					) {
					
					itAllResponsesEnum.remove();
				}
				
				if (DocumentQueueEnum.NSD.equals(coda)
						&& ResponsesRedEnum.FIRMA_AUTOGRAFA.getResponse().equals(responseEnum.getResponse()) 
					) {
					
					itAllResponsesEnum.remove();
				}
			}
		}
		
		return inResponses;
	}
	
}