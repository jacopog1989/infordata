package it.ibm.red.business.dto;

import java.util.List;

/**
 * DTO documento Red Fascicolo FEPA.
 */
public class DocumentoRedFascicoloFepaDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 2307629591661213670L;
	
	/**
	 * Nome fascicolo.
	 */
	private String nomeFascicoloDocumentoRed;
	
	/**
	 * Id fascicolo FEPA.
	 */
	private String idFascicoloFepa;
	
	/**
	 * Documenti.
	 */
	private List<DocumentoFascicoloFepaDTO> documentiFattura;

	/**
	 * Restituisce il nome del fascicolo documento Red.
	 * @return nome fascicolo
	 */
	public String getNomeFascicoloDocumentoRed() {
		return nomeFascicoloDocumentoRed;
	}

	/**
	 * Imposta il nome del fascicolo documento Red.
	 * @param nomeFascicoloDocumentoRed
	 */
	public void setNomeFascicoloDocumentoRed(final String nomeFascicoloDocumentoRed) {
		this.nomeFascicoloDocumentoRed = nomeFascicoloDocumentoRed;
	}

	/**
	 * Restituisce l'id del fascicolo FEPA.
	 * @return id fascicolo FEPA
	 */
	public String getIdFascicoloFepa() {
		return idFascicoloFepa;
	}

	/**
	 * Imposta l'id del fascicolo FEPA.
	 * @param idFascicoloFepa
	 */
	public void setIdFascicoloFepa(final String idFascicoloFepa) {
		this.idFascicoloFepa = idFascicoloFepa;
	}

	/**
	 * Restituisce la lista dei documenti fattura.
	 * @return documenti fattura
	 */
	public List<DocumentoFascicoloFepaDTO> getDocumentiFattura() {
		return documentiFattura;
	}

	/**
	 * Imposta la lista dei documenti fattura.
	 * @param documentiFattura
	 */
	public void setDocumentiFattura(final List<DocumentoFascicoloFepaDTO> documentiFattura) {
		this.documentiFattura = documentiFattura;
	}


}
