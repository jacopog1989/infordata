package it.ibm.red.business.service.concrete;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.collection.FolderSet;
import com.filenet.api.collection.PageIterator;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.Document;
import com.filenet.api.core.Folder;
import com.filenet.apiimpl.core.SubSetImpl;
import com.google.common.io.ByteStreams;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.IEmailBckDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnatarioInteropDTO;
import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.DestinatarioCodaMailDTO;
import it.ibm.red.business.dto.DetailEmailDTO;
import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.DocumentoAllegabileDTO.DocumentoAllegabileType;
import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.dto.ErroreValidazioneDTO;
import it.ibm.red.business.dto.EsitoOperazioneMailDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.MailAttachmentDTO;
import it.ibm.red.business.dto.NotaDTO;
import it.ibm.red.business.dto.SegnaturaMessaggioInteropDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.AnnotationDTO;
import it.ibm.red.business.enums.FilenetAnnotationEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.StatoMailEnum;
import it.ibm.red.business.enums.StatoMailGUIEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.adobe.AdobeLCHelper;
import it.ibm.red.business.helper.adobe.IAdobeLCHelper;
import it.ibm.red.business.helper.factory.DocumentoAllegabileDTOFactory;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper.Metadato;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.html.HtmlFileHelper;
import it.ibm.red.business.helper.html.IHtmlFileHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.ICasellePostaliSRV;
import it.ibm.red.business.service.ICodaMailSRV;
import it.ibm.red.business.service.IMailSRV;
import it.ibm.red.business.template.TemplateDocumento;
import it.ibm.red.business.template.TemplateDocumentoEntrata;
import it.ibm.red.business.utils.FileUtils;

/**
 * Servizio gestione mail.
 * 
 * @author CRISTIANOPierascenzi
 *
 */
@Service
@Component
public class MailSRV extends AbstractService implements IMailSRV {

	/**
	 * Serializzazione.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(MailSRV.class.getName());

	/**
	 * Messaggio errore eliminazione mail da Filenet.
	 */
	private static final String ERROR_ELIMINAZIONE_MAIL_MSG = "eliminaBozzaEmailDocumento -> Si è verificato un errore durante l'eliminazione da FileNet dell'e-mail: ";

	/**
	 * Messaggio errore recupero mail.
	 */
	private static final String ERROR_RECUPERO_MAIL_MSG = "Errore nel recupero delle mail: ";

	/**
	 * Label parametro di configurazione.
	 */
	private static final String PARAMETRO_DI_CONFIGURAZIONE_LABEL = "Parametro di configurazione ";

	/**
	 * Messaggio errore parziale: parametro non trovato. Occorre appendere questo
	 * messaggio al parametro non trovato.
	 */
	private static final String ERROR_PARAMETRO_NON_TROVATO_MSG = " non trovato: verificare le properties!";

	/**
	 * Fornitore della properties dell'applicazione.
	 */
	private PropertiesProvider pp;

	/**
	 * Service.
	 */
	@Autowired
	private ICodaMailSRV codaMailSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ICasellePostaliSRV casellePostaliSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private IEmailBckDAO emailBckDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;
	
	/**
	 * Html helper.
	 */
	private IHtmlFileHelper htmlFileHelper;

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * Metodo per il recupero del dettaglio della mail da guid.
	 *
	 * @param fcDTO the fc DTO
	 * @param guid  the guid
	 * @return the detail from guid
	 */
	@Override
	public DetailEmailDTO getDetailFromGuid(final FilenetCredentialsDTO fcDTO, final String guid, final Long idAoo) {
		DetailEmailDTO dtemail = null;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(fcDTO, idAoo);
			// Recupero documenti FN
			LOGGER.info("####INFO#### Inizio getDocumentByGuid");
			final Document doc = fceh.getDocumentByGuid(guid);
			LOGGER.info("####INFO#### Fine getDetailFromGuid");
			// Converto Documenti FN in DTO
			LOGGER.info("####INFO#### Inizio transformer");
			dtemail = TrasformCE.transform(doc, TrasformerCEEnum.FROM_DOCUMENTO_TO_MAIL_DETAIL);
			LOGGER.info("####INFO#### Fine transformer");

			// Converto eventuali allegati del dettaglio mail
			LOGGER.info("####INFO#### Inizio transformerAllegati");
			final Collection<MailAttachmentDTO> at = TrasformCE.transform(doc.get_ChildDocuments(), TrasformerCEEnum.FROM_DOCUMENTO_TO_ALLEGATO_MAIL);
			LOGGER.info("####INFO#### Fine transformerAllegati");
			dtemail.setAllegati(at);
		} finally {
			popSubject(fceh);
		}

		return dtemail;
	}

	/**
	 * Metodo per il recupero dell'allegato mail tramite il guid.
	 *
	 * @param fcDTO the fc DTO
	 * @param guid  the guid
	 * @return the file allegato from guid
	 */
	@Override
	public final FileDTO getFileAllegatoFromGuid(final FilenetCredentialsDTO fcDTO, final String guid, final Long idAoo) {
		FileDTO output = null;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(fcDTO, idAoo);

			final Document doc = fceh.getDocumentByGuid(guid);

			final ContentTransfer content = FilenetCEHelper.getDocumentContentTransfer(doc);
			final byte[] bytes = FileUtils.getByteFromInputStream(content.accessContentStream());
			final String fileName = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);

			output = new FileDTO(fileName, bytes, content.get_ContentType());
		} finally {
			popSubject(fceh);
		}

		return output;
	}

	/**
	 * Metodo per cambiare stato delle mail da eliminate a "inbox" Massivo.
	 *
	 * @param fcDto the fc dto
	 * @param guids the guids
	 * @return the collection
	 */
	@Override
	public final Collection<EsitoOperazioneMailDTO> ripristina(final FilenetCredentialsDTO fcDto, final Collection<DetailEmailDTO> mails) {
		final Collection<EsitoOperazioneMailDTO> output = new ArrayList<>();
		for (final DetailEmailDTO mail : mails) {
			final EsitoOperazioneMailDTO esito = new EsitoOperazioneMailDTO(mail.getGuid());
			try {
				ripristina(fcDto, mails);
				esito.setEsito(true);
			} catch (final Exception e) {
				LOGGER.error(e.getMessage(), e);
				esito.setEsito(false);
			}
			output.add(esito);
		}
		return output;
	}

	/**
	 * Metodo per cambiare stato delle mail da "inbox" a "eliminate" Massivo.
	 *
	 * @param fcDto    the fc dto
	 * @param guids    the guids
	 * @param motivo   the motivo
	 * @param idUtente the id utente
	 * @return the collection
	 */
	@Override
	public final Collection<EsitoOperazioneMailDTO> cancella(final FilenetCredentialsDTO fcDto, final Collection<String> guids, final String motivo, final UtenteDTO utente) {
		final Collection<EsitoOperazioneMailDTO> output = new ArrayList<>();
		for (final String guid : guids) {
			final EsitoOperazioneMailDTO esito = new EsitoOperazioneMailDTO(guid);
			try {
				cancella(fcDto, guid, motivo, utente);
				esito.setEsito(true);
			} catch (final Exception e) {
				LOGGER.error(e.getMessage(), e);
				esito.setEsito(false);
			}
			output.add(esito);
		}
		return output;
	}

	/**
	 * Metodo per ripristinare una mail eliminata.
	 *
	 * @param fcDto the fc dto
	 * @param guid  the guid
	 */
	@Override
	public final EmailDTO ripristina(final FilenetCredentialsDTO fcDTO, final DetailEmailDTO mail, final Long idAoo) {
		final Document doc = ripristina(fcDTO, mail.getGuid(), mail.getStatoMail(), idAoo);
		IFilenetCEHelper fceh = null;
		EmailDTO email;

		try {
			fceh = FilenetCEHelperProxy.newInstance(fcDTO, idAoo);
			final Document d = fceh.getDocument(doc.get_Id());
			email = TrasformCE.transform(d, TrasformerCEEnum.FROM_DOCUMENTO_TO_MAIL_MASTER);
			return email;
		} finally {
			popSubject(fceh);
		}
	}

	@Override
	public final void ripristina(final FilenetCredentialsDTO fcDTO, final EmailDTO mail, final Long idAoo) {
		ripristina(fcDTO, mail.getGuid(), mail.getStato(), idAoo);
	}

	private Document ripristina(final FilenetCredentialsDTO fcDto, final String guidMail, final StatoMailEnum statoMail, final Long idAoo) {
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(fcDto, idAoo);

			Document output = null;
			final Document docSet = fceh.getDocumentByGuid(guidMail);
			final SubSetImpl ssi = (SubSetImpl) docSet.get_VersionSeries().get_Versions();
			final List<?> list = ssi.getList();
			final Document doc = (Document) list.get(list.size() - 1);

			// Se esiste la data scarico sto facendo riferimento a un doc già ripristinato
			Date dataCreazioneDocRipristinato = (Date) TrasformerCE.getMetadato(doc, PropertiesNameEnum.DATA_SCARICO_METAKEY);
			if (dataCreazioneDocRipristinato == null) {
				dataCreazioneDocRipristinato = (Date) TrasformerCE.getMetadato(doc, PropertiesNameEnum.DATA_CREAZIONE_METAKEY);
			}
			Folder rootFolder = null;
			final FolderSet fs = docSet.get_FoldersFiledIn();
			if (fs != null) {
				final Iterator<?> itf = fs.iterator();
				if (itf.hasNext()) {
					rootFolder = (Folder) itf.next();
				}
			}

			if (rootFolder == null) {
				LOGGER.error("Folder non inizializzata prima della chiamata ad un metodo della classe.");
				throw new RedException("Folder non inizializzata prima della chiamata ad un metodo della classe.");
			}

			if (rootFolder.get_FolderName().equalsIgnoreCase(pp.getParameterByKey(PropertiesNameEnum.FOLDER_MAIL_INBOX_FN_METAKEY))) {
				if (statoMail.equals(StatoMailEnum.INOLTRATA) || statoMail.equals(StatoMailEnum.RIFIUTATA)) {
					// clonazione mail a partire dalla versione 1 per ripristino in posta in arrivo
					// di email rifiutate o inoltrate
					// leggo la prima versione

					// determino la folder dove e presente il file (utilizzando l'ultima versione)

					final String[] splitName = docSet.get_Name().split("_");
					final Document documentoRipristinato = fceh.copyDocument(doc, rootFolder, true, splitName[0] + "_" + System.currentTimeMillis());
					final HashMap<String, Object> metadati = new HashMap<>();
					metadati.put(pp.getParameterByKey(PropertiesNameEnum.STATO_MAIL_METAKEY), StatoMailEnum.INARRIVO.getStatus());
					metadati.put(pp.getParameterByKey(PropertiesNameEnum.DATA_SCARICO_METAKEY), dataCreazioneDocRipristinato);
					final String guidDocRipristinato = documentoRipristinato.get_Id().toString().replace("{", "").replace("}", "");
					fceh.updateMetadati(guidDocRipristinato, metadati);

					output = documentoRipristinato;
				} else {
					final HashMap<String, Object> metadati = new HashMap<>();
					metadati.put(pp.getParameterByKey(PropertiesNameEnum.STATO_MAIL_METAKEY), StatoMailEnum.INARRIVO.getStatus());

					if (statoMail.equals(StatoMailEnum.ARCHIVIATA)) {
						// registrazione data di reinoltro/rispristino
						metadati.put(pp.getParameterByKey(PropertiesNameEnum.DATA_CAMBIO_STATO_EMAIL_METAKEY), new Date());
					}

					metadati.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVO_ELIMINAZIONE_EMAIL_METAKEY), "");
					metadati.put(pp.getParameterByKey(PropertiesNameEnum.DATA_SCARICO_METAKEY), dataCreazioneDocRipristinato);

					// aggiorno alla fine
					fceh.updateMetadati(docSet, metadati);

					output = docSet;
				}
			} else {
				final HashMap<String, Object> metadati = new HashMap<>();
				// leggo la prima versione

				// Determino la folder dove e presente il file (utilizzando l'ultima versione)
				final String[] splitName = docSet.get_Name().split("_");
				Document documentoRipristinato = fceh.copyDocumentInbox(doc, rootFolder.get_Parent().get_FolderName(), true, splitName[0] + "_" + System.currentTimeMillis());
				metadati.put(pp.getParameterByKey(PropertiesNameEnum.STATO_MAIL_METAKEY), StatoMailEnum.INARRIVO.getStatus());
				metadati.put(pp.getParameterByKey(PropertiesNameEnum.DATA_CAMBIO_STATO_EMAIL_METAKEY), new Date());
				metadati.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVO_ELIMINAZIONE_EMAIL_METAKEY), "");
				metadati.put(pp.getParameterByKey(PropertiesNameEnum.DATA_SCARICO_METAKEY), dataCreazioneDocRipristinato);

				final String guidDocRipristinato = documentoRipristinato.get_Id().toString().replace("{", "").replace("}", "");
				fceh.updateMetadati(guidDocRipristinato, metadati);

				// lo rileggo dopo aver aggiornato i metadati
				documentoRipristinato = fceh.getDocumentByGuid(guidMail);

				output = documentoRipristinato;
			}

			return output;

		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IMailFacadeSRV#inoltra(it.ibm.red.business
	 *      .dto.FilenetCredentialsDTO, java.lang.String, java.lang.String,
	 *      java.lang.String, java.util.List, java.util.List, java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.Integer,
	 *      java.lang.Integer, java.lang.Boolean, java.lang.Boolean,
	 *      java.lang.Integer, java.lang.Integer,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public final void inoltra(final FilenetCredentialsDTO fcDto, final String guid, final String casellaPostale, final String mittente,
			final List<DestinatarioCodaMailDTO> destinatariToList, final List<DestinatarioCodaMailDTO> destinatariCcList, final String oggettoMail, final String testoMail,
			final String azioneMail, final String motivazioneMail, final Integer tipologiaInvioId, final Integer tipologiaMessaggioId, final Boolean isNotifica,
			final Boolean hasAllegati, final Integer modalitaSpedizione, final Integer tipoEvento, final UtenteDTO utente) {
		Connection connection = null;

		try {
			IFilenetCEHelper fceh = null;
			fceh = FilenetCEHelperProxy.newInstance(fcDto, utente.getIdAoo());
			final HashMap<String, Object> metadati = new HashMap<>();
			final Document doc = fceh.getDocumentByGuid(guid);
			final String operazionePostaElettronicaValue = operazionePostaElettronica(utente, doc, "Inoltra:");
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.OPERAZIONE_POSTA_ELETTRONICA_FN_METAKEY), operazionePostaElettronicaValue);
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.STATO_MAIL_METAKEY), StatoMailEnum.INUSCITA.getStatus());
			fceh.updateMetadati(guid, metadati);

			connection = setupConnection(getDataSource().getConnection(), true);

			// Il batch aggiorna lo stato
			codaMailSRV.creaMailEInserisciInCoda(guid, casellaPostale, mittente, destinatariToList, destinatariCcList, oggettoMail, testoMail, azioneMail, motivazioneMail,
					tipologiaInvioId, tipologiaMessaggioId, isNotifica, hasAllegati, modalitaSpedizione, tipoEvento, utente, connection, false);

			commitConnection(connection);
		} catch (final Exception e) {
			rollbackConnection(connection);
			throw new RedException("Si è verificato un errore durante l'operazione di inoltro dell'e-mail.", e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IMailFacadeSRV#rifiuta(it.ibm.red.business.dto.FilenetCredentialsDTO,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 *      java.lang.String, java.lang.Integer, java.lang.Integer,
	 *      java.lang.Boolean, java.lang.Boolean, java.lang.Integer,
	 *      java.lang.Integer, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public final void rifiuta(final FilenetCredentialsDTO fcDto, final String guid, final String casellaPostale, final String mittente, final String destinatario,
			final String destinatarioToForFilenet, final String oggettoMail, final String testoMail, final String azioneMail, final String motivazioneMail,
			final Integer tipologiaInvioId, final Integer inTipologiaMessaggioId, final Boolean isNotifica, final Boolean hasAllegati, final Integer modalitaSpedizione,
			final Integer tipoEvento, final UtenteDTO utente) {
		Connection connection = null;
		IFilenetCEHelper fceh = null;
		Integer tipologiaMessaggioId = inTipologiaMessaggioId;
		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			fceh = FilenetCEHelperProxy.newInstance(fcDto, utente.getIdAoo());

			final Document doc = fceh.getDocumentByGuid(guid);
			final String messageId = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.MESSAGE_ID);
			final String documentTitle = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);

			final CasellaPostaDTO casellaPostaleRed = casellePostaliSRV.getCasellaPostale(fceh, casellaPostale);
			if (casellaPostaleRed != null && casellaPostaleRed.getIndirizzoEmail().equals(casellaPostale) && tipologiaMessaggioId == null) {
				tipologiaMessaggioId = casellaPostaleRed.getTipologia();
			}

			// Il Batch aggiorna lo stato
			codaMailSRV.creaMailEInserisciInCoda(documentTitle, guid, mittente, Arrays.asList(destinatarioToForFilenet), destinatario, "", oggettoMail, testoMail, azioneMail,
					motivazioneMail, tipologiaInvioId, tipologiaMessaggioId, messageId, isNotifica, hasAllegati, modalitaSpedizione, tipoEvento, utente, connection, null,
					false);

			final HashMap<String, Object> metadati = new HashMap<>();
			final String operazionePostaElettronicaValue = operazionePostaElettronica(utente, doc, "Rifiuta : ");
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.OPERAZIONE_POSTA_ELETTRONICA_FN_METAKEY), operazionePostaElettronicaValue);
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.STATO_MAIL_METAKEY), StatoMailEnum.INUSCITA.getStatus());

			fceh.updateMetadati(guid, metadati);
			commitConnection(connection);
		} catch (final Exception e) {
			rollbackConnection(connection);
			throw new RedException("Si è verificato un errore durante l'operazione di rifiuto dell'e-mail.", e);
		} finally {
			closeConnection(connection);
			popSubject(fceh);
		}
	}

	/**
	 * Metodo per eliminare una mail in inbox.
	 *
	 * @param fcDto    the fc dto
	 * @param guid     the guid
	 * @param motivo   the motivo
	 * @param idUtente the id utente
	 */
	@Override
	public final void cancella(final FilenetCredentialsDTO fcDto, final String guid, final String motivo, final UtenteDTO utente) {
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(fcDto, utente.getIdAoo());

			final Document doc = fceh.getDocumentByGuid(guid);

			final EmailDTO email = TrasformCE.transform(doc, TrasformerCEEnum.FROM_DOCUMENTO_TO_MAIL_MASTER);
			fillProtocollatore(email);

			final HashMap<String, Object> metadati = new HashMap<>();
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.STATO_MAIL_METAKEY), StatoMailEnum.ELIMINATA.getStatus());
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVO_ELIMINAZIONE_EMAIL_METAKEY), motivo);

			if (StatoMailEnum.ARCHIVIATA.equals(email.getStato())) {
				metadati.put(pp.getParameterByKey(PropertiesNameEnum.DATA_CAMBIO_STATO_EMAIL_METAKEY), new Date());
			}
			final String operazionePostaElettronicaValue = operazionePostaElettronica(utente, doc, "Cancella:");
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.OPERAZIONE_POSTA_ELETTRONICA_FN_METAKEY), operazionePostaElettronicaValue);

			fceh.updateMetadati(doc, metadati);

		} finally {
			popSubject(fceh);
		}
	}

	private void fillProtocollatore(final EmailDTO email) {
		final Collection<EmailDTO> allPosta = new ArrayList<>();
		allPosta.add(email);
		fillProtocollatore(allPosta);
	}

	private void fillProtocollatore(final Collection<EmailDTO> allPosta) {
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			final List<Integer> idsProtocollatori = new ArrayList<>();
			for (final EmailDTO email : allPosta) {
				if (email.getIdUtenteProtocollatore() != null) {
					idsProtocollatori.add(email.getIdUtenteProtocollatore());
				}
			}
			final Map<Integer, String> descProtocollatori = utenteDAO.getDescUtenti(idsProtocollatori, con);
			for (final EmailDTO email : allPosta) {
				if (email.getIdUtenteProtocollatore() != null) {
					email.setDescUtenteProtocollatore(descProtocollatori.get(email.getIdUtenteProtocollatore()));
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la ricerca degli utenti protocollatori dell'e-mail.", e);
			throw new RedException("Errore durante la ricerca degli utenti protocollatori dell'e-mail");
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * Metodo per inserimento nota.
	 *
	 * @param fcDto the fc dto
	 * @param guid  the guid
	 * @param nota  the nota
	 */
	@Override
	public final void setNotaMail(final FilenetCredentialsDTO fcDto, final String guid, final NotaDTO nota, final AssegnatarioInteropDTO preassegnatario, final Long idAoo) {
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(fcDto, idAoo);
			final HashMap<String, Object> metadati = new HashMap<>();

			String notaValue = null;
			if (nota != null && !nota.getContentNota().isEmpty()) {
				notaValue = "#" + (nota.getColore().getId()) + "_" + nota.getContentNota();
			}

			String preassegnatarioValue = null;
			if (preassegnatario != null) {
				preassegnatarioValue = preassegnatario.getDescrizioneUfficio();
				if (preassegnatario.getDescrizioneUtente() != null) {
					preassegnatarioValue += "|" + preassegnatario.getDescrizioneUtente();
				}
			}
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.PREASSEGNATARIO_SEGN_INTEROP_MAIL_METAKEY), preassegnatarioValue);
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.NOTA_EMAIL_METAKEY), notaValue);

			fceh.updateMetadati(guid, metadati);
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * Metodo per l'eliminazione della mail.
	 *
	 * @param fcDto the fc dto
	 * @param guid  the guid
	 */

	@Override
	public final void cancellaNotaMail(final FilenetCredentialsDTO fcDto, final String guid, final Long idAoo) {
		setNotaMail(fcDto, guid, null, null, idAoo);
	}

	/**
	 * Metodo per recuperare tutte le mail di una determinata casella postale .
	 *
	 * @param fcDTO            the fc DTO
	 * @param cpAttiva         the cp attiva
	 * @param idUtente         the id utente
	 * @param selectedStatList the selected stat list
	 * @param orderByClause    the selected sorting method
	 * @return the mails
	 */
	@Override
	public final Collection<EmailDTO> getMails(final FilenetCredentialsDTO fcDTO, final String cpAttiva, final StatoMailGUIEnum selectedStat, final Integer idUtente,
			final String orderByClause, final Long idAoo) {
		IFilenetCEHelper fceh = null;
		Collection<EmailDTO> mails = new ArrayList<>();
		try {
			fceh = FilenetCEHelperProxy.newInstance(fcDTO, idAoo);
			final List<StatoMailEnum> selectedStatList = new ArrayList<>();

			if (StatoMailGUIEnum.ELIMINATA.equals(selectedStat)) {
				selectedStatList.add(StatoMailEnum.ELIMINATA);
			} else if (StatoMailGUIEnum.INOLTRATA.equals(selectedStat)) {
				selectedStatList.add(StatoMailEnum.INOLTRATA);
			} else if (StatoMailGUIEnum.RIFIUTATA.equals(selectedStat)) {
				selectedStatList.add(StatoMailEnum.RIFIUTATA);
			} else if (StatoMailGUIEnum.INUSCITA.equals(selectedStat)) {
				selectedStatList.add(StatoMailEnum.INUSCITA);
				// RIFIUTO START
			} else if (StatoMailGUIEnum.RIFAUTO.equals(selectedStat)) {
				selectedStatList.add(StatoMailEnum.RIFIUTATA_AUTOMATICAMENTE);
				// RIFIUTO END
			} else {
				selectedStatList.add(StatoMailEnum.INARRIVO);
				selectedStatList.add(StatoMailEnum.INARRIVO_NON_PROTOCOLLABILE_AUTOMATICAMENTE);
				selectedStatList.add(StatoMailEnum.IN_FASE_DI_PROTOCOLLAZIONE_MANUALE);
			}

			mails = getMailsWithOrder(fceh, cpAttiva, selectedStatList, orderByClause);
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_MAIL_MSG + e.getMessage(), e);
		} finally {
			popSubject(fceh);
		}

		return mails;
	}

	@Override
	public final Collection<EmailDTO> getMails(final FilenetCredentialsDTO fcDTO, final String cpAttiva, final StatoMailGUIEnum selectedStat, final Integer idUtente,
			final Long idAoo) {
		return getMails(fcDTO, cpAttiva, selectedStat, idUtente, null, idAoo);
	}

	@Override
	public final PageIterator createPaginator(final FilenetCredentialsDTO fcDTO, final String cpAttiva, final List<StatoMailGUIEnum> selectedStatGuiList, final int idUtente,
			final Integer pageSize, final String orderByClause, final Long idAoo) {
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(fcDTO, idAoo);
			final List<StatoMailEnum> selectedStatList = new ArrayList<>();
			boolean inArrivoGiaInserito = false;
			for (final StatoMailGUIEnum selectedStat : selectedStatGuiList) {
				if (StatoMailGUIEnum.ELIMINATA.equals(selectedStat)) {
					selectedStatList.add(StatoMailEnum.ELIMINATA);
				} else if (StatoMailGUIEnum.INOLTRATA.equals(selectedStat)) {
					selectedStatList.add(StatoMailEnum.INOLTRATA);
				} else if (StatoMailGUIEnum.RIFIUTATA.equals(selectedStat)) {
					selectedStatList.add(StatoMailEnum.RIFIUTATA);
				} else if (StatoMailGUIEnum.INUSCITA.equals(selectedStat)) {
					selectedStatList.add(StatoMailEnum.INUSCITA);
				} else if (StatoMailGUIEnum.RIFAUTO.equals(selectedStat)) {
					selectedStatList.add(StatoMailEnum.RIFIUTATA_AUTOMATICAMENTE);
				} else if (!inArrivoGiaInserito) {
					selectedStatList.add(StatoMailEnum.INARRIVO);
					selectedStatList.add(StatoMailEnum.INARRIVO_NON_PROTOCOLLABILE_AUTOMATICAMENTE);
					selectedStatList.add(StatoMailEnum.IN_FASE_DI_PROTOCOLLAZIONE_MANUALE);
					inArrivoGiaInserito = true;
				}
			}
			return createPaginator(fceh, cpAttiva, selectedStatList, idUtente, pageSize, orderByClause);
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_MAIL_MSG + e.getMessage(), e);
		} finally {
			popSubject(fceh);
		}

		return null;
	}

	/**
	 * Metodo per recuperare tutte le mail di una determinata casella postale .
	 *
	 * @param fcDTO            the fc DTO
	 * @param cpAttiva         the cp attiva
	 * @param idUtente         the id utente
	 * @param selectedStatList the selected stat list
	 * @return the mails
	 */
	private Collection<EmailDTO> getMailsWithOrder(final IFilenetCEHelper fceh, final String cpAttiva, final List<StatoMailEnum> selectedStatList,
			final String orderByClause) {
		StringBuilder filter = null;
		final String casellePostaliFolder = pp.getParameterByKey(PropertiesNameEnum.FOLDER_ELETTRONICI_FN_METAKEY).split("\\|")[1];
		final String folder = "/" + casellePostaliFolder + "/" + cpAttiva + "/" + pp.getParameterByKey(PropertiesNameEnum.FOLDER_MAIL_INBOX_FN_METAKEY);
		final String classeDocumentale = pp.getParameterByKey(PropertiesNameEnum.MAIL_CLASSNAME_FN_METAKEY);

		if (selectedStatList != null && !selectedStatList.isEmpty()) {
			StatoMailEnum selectedStat = null;
			filter = new StringBuilder("AND (");

			for (int i = 0; i < selectedStatList.size(); i++) {

				selectedStat = selectedStatList.get(i);
				filter.append("stato = ").append(selectedStat.getStatus());

				if (i < (selectedStatList.size() - 1)) {
					filter.append(" OR ");
				}

			}
			filter.append(")");
		}

		Collection<EmailDTO> allPosta = null;
		try {
			LOGGER.info("###INFO#### getMailAsDocument loadEmails start");
			final DocumentSet ds = fceh.getMailsAsDocument(folder, classeDocumentale, filter != null ? filter.toString() : null, selectedStatList, orderByClause);
			LOGGER.info("###INFO#### getMailAsDocument loadEmails end");

			LOGGER.info("###INFO#### transformer loadEmails start");
			allPosta = TrasformCE.transform(ds, TrasformerCEEnum.FROM_DOCUMENTO_TO_MAIL_MASTER);
			LOGGER.info("###INFO#### transformer loadEmails end");

			LOGGER.info("###INFO#### fillProtocollatore loadEmails start");
			fillProtocollatore(allPosta);
			LOGGER.info("###INFO#### fillProtocollatore loadEmails end");
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_MAIL_MSG + e.getMessage(), e);
		}

		return allPosta;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IMailFacadeSRV#getMails(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      java.lang.String, java.util.List, java.lang.Integer).
	 */
	@Override
	public final Collection<EmailDTO> getMails(final IFilenetCEHelper fceh, final String cpAttiva, final List<StatoMailEnum> selectedStatList, final Integer idUtente) {
		return getMailsWithOrder(fceh, cpAttiva, selectedStatList, null);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IMailFacadeSRV#createPaginator(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      java.lang.String, java.util.List, java.lang.Integer, java.lang.Integer,
	 *      java.lang.String).
	 */
	@Override
	public PageIterator createPaginator(final IFilenetCEHelper fceh, final String cpAttiva, final List<StatoMailEnum> selectedStatList, final Integer idUtente,
			final Integer pageSize, final String orderByClause) {
		StringBuilder filter = null;
		final String casellePostaliFolder = pp.getParameterByKey(PropertiesNameEnum.FOLDER_ELETTRONICI_FN_METAKEY).split("\\|")[1];
		final String folder = "/" + casellePostaliFolder + "/" + cpAttiva + "/" + pp.getParameterByKey(PropertiesNameEnum.FOLDER_MAIL_INBOX_FN_METAKEY);
		final String classeDocumentale = pp.getParameterByKey(PropertiesNameEnum.MAIL_CLASSNAME_FN_METAKEY);

		if (selectedStatList != null && !selectedStatList.isEmpty()) {
			StatoMailEnum selectedStat = null;
			filter = new StringBuilder("AND (");

			for (int i = 0; i < selectedStatList.size(); i++) {

				selectedStat = selectedStatList.get(i);
				filter.append("stato = ").append(selectedStat.getStatus());

				if (i < (selectedStatList.size() - 1)) {
					filter.append(" OR ");
				}

			}
			filter.append(")");
		}

		try {
			LOGGER.info("###INFO#### getMailAsDocument loadEmails iterator start");
			final PageIterator piDocuments = fceh.getPageIteratorForMail(folder, classeDocumentale, filter != null ? filter.toString() : null, selectedStatList, pageSize,
					orderByClause);
			LOGGER.info("###INFO#### getMailAsDocument loadEmails iterator end");
			return piDocuments;
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_MAIL_MSG + e.getMessage(), e);
			return null;
		}

	}

	/**
	 * @see it.ibm.red.business.service.facade.IMailFacadeSRV#refineIterator(it.ibm.red.business.dto.UtenteDTO,
	 *      com.filenet.api.collection.PageIterator).
	 */
	@Override
	public Collection<EmailDTO> refineIterator(final UtenteDTO utente, final PageIterator pageIteratorMail) {
		IFilenetCEHelper fceh = null;
		Collection<EmailDTO> allMails = new ArrayList<>();
		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final Collection<EmailDTO> allPosta = new ArrayList<>();
			if (pageIteratorMail != null && pageIteratorMail.nextPage()) {
				for (final Object document : pageIteratorMail.getCurrentPage()) {
					final Document doc = (Document) document;
					final EmailDTO singleMail = TrasformCE.transform(doc, TrasformerCEEnum.FROM_DOCUMENTO_TO_MAIL_MASTER);
					allPosta.add(singleMail);
				}
			}
			fillProtocollatore(allPosta);
			allMails = allPosta;
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_MAIL_MSG + e.getMessage(), e);
		} finally {
			popSubject(fceh);
		}
		return allMails;
	}

	/**
	 * Metodo per la ricerca tra tutte le email della casella postale.
	 * 
	 * @see it.ibm.red.business.service.facade.IMailFacadeSRV#searchMails(it.ibm.red.business.dto.FilenetCredentialsDTO,
	 *      java.lang.String, java.lang.Integer, java.lang.Integer, java.util.Date,
	 *      java.util.Date, java.lang.String, java.lang.String, java.lang.String,
	 *      boolean, java.lang.String, java.util.List, java.lang.Long)
	 */
	@Override
	public final Collection<EmailDTO> searchMails(final FilenetCredentialsDTO fcDTO, final String cpAttiva, final Integer idUtente, final Integer anno, final Date inDataDa,
			final Date inDataA, final String searchString, final String oggettoRicerca, final String mittenteRicerca, final boolean isFullTextRicerca,
			final String numeroProtocolloRicerca, final List<StatoMailEnum> statiMailSelezionati, final Long idAoo) {
		Connection connection = null;
		final Set<EmailDTO> allEmails = new LinkedHashSet<>();
		IFilenetCEHelper fceh = null;

		try {
			final StringBuilder folderBuilder = new StringBuilder();
			String folderCorrenti = "";
			String folderArchiviate = "";
			List<String> filter = new ArrayList<>();

			folderCorrenti = generateDefaultFolder(cpAttiva, pp);
			filter.add(folderCorrenti);

			// Ricerca in archiviate
			folderArchiviate = generateDefaultFolderArchiviate(cpAttiva, pp);
			filter.add(folderArchiviate);

			filter = filter.stream().filter(s -> StringUtils.isNotBlank(s)).collect(Collectors.toList());
			folderBuilder.append(String.join(" OR ", filter));
			final String classeDocumentale = pp.getParameterByKey(PropertiesNameEnum.MAIL_CLASSNAME_FN_METAKEY);

			Date dataDa = inDataDa;
			Date dataA = inDataA;
			// Le date, se specificate, hanno precedenza rispetto all'anno
			// Se cerco per protocollo non setto le date mi interessa solo quella del
			// protocollo
			if (anno != null && dataDa == null && dataA == null && (numeroProtocolloRicerca == null || StringUtils.isEmpty(numeroProtocolloRicerca))) {
				final Calendar cal = Calendar.getInstance();

				cal.set(anno, 0, 1, 0, 0, 0);
				dataDa = cal.getTime();

				cal.set(anno, 11, 31, 0, 0, 0);
				dataA = cal.getTime();
			}

			if (!(numeroProtocolloRicerca == null || StringUtils.isEmpty(numeroProtocolloRicerca))) {
				dataDa = null;
				dataA = null;
				// Se cerco per protocollo non setto le date mi interessa solo quella del
				// protocollo
			}

			fceh = FilenetCEHelperProxy.newInstance(fcDTO, idAoo);

			LOGGER.info("####INFO#### Inizio chiamata getMailAsDocument searchString= " + searchString);
			final List<String> searchFields = getSearchField();

			final DocumentSet ds = fceh.getMailsAsDocumentRicerca(folderCorrenti, folderArchiviate, classeDocumentale, dataDa, dataA, searchFields, statiMailSelezionati,
					oggettoRicerca, mittenteRicerca, numeroProtocolloRicerca);
			LOGGER.info("####INFO#### Fine chiamata getMailAsDocument");

			connection = setupConnection(getDataSource().getConnection(), false);
			LOGGER.info("####INFO#### Inizio chiamata al transformer");
			final Collection<EmailDTO> simpleSearchRes = TrasformCE.transform(ds, TrasformerCEEnum.FROM_DOCUMENTO_TO_MAIL_RICERCA, connection);
			LOGGER.info("####INFO#### Fine chiamata al transformer");
			allEmails.addAll(simpleSearchRes);

			// Se non faccio la ricerca per stringa non ho necessita' di eseguire la full
			// text
			if (isFullTextRicerca && (StringUtils.isNotBlank(oggettoRicerca) || StringUtils.isNotBlank(mittenteRicerca) || StringUtils.isNotBlank(numeroProtocolloRicerca))) {
				// Costruisco l'insieme delle strutture dati necessarie a verificare che non ho
				// gia' recuperato la medesima email
				Set<String> insertedGuid;
				if (CollectionUtils.isNotEmpty(allEmails)) {
					insertedGuid = allEmails.stream().map(EmailDTO::getGuid).collect(Collectors.toSet());
				} else {
					insertedGuid = new HashSet<>();
				}
				LOGGER.info("####INFO#### Inizio chiamata getMailAsDocumentFullText");
				final DocumentSet dsFullText = fceh.getMailAsDocumentFullText(folderCorrenti, folderArchiviate, classeDocumentale, oggettoRicerca, mittenteRicerca, dataDa,
						dataA, null, statiMailSelezionati, numeroProtocolloRicerca);
				LOGGER.info("####INFO#### Fine chiamata getMailAsDocumentFullText");
				final Iterator<?> itFT = dsFullText.iterator();
				while (itFT.hasNext()) {
					final Document doc = (Document) itFT.next();
					final String id = doc.get_Id().toString();

					if (!insertedGuid.contains(id)) {
						final EmailDTO email = TrasformCE.transform(doc, TrasformerCEEnum.FROM_DOCUMENTO_TO_MAIL_MASTER, connection);
						allEmails.add(email);
					}
				}
				LOGGER.info("####INFO#### Inizio chiamata al transformer fullText");
				final Collection<EmailDTO> fullTextSearchRes = TrasformCE.transform(dsFullText, TrasformerCEEnum.FROM_DOCUMENTO_TO_MAIL_MASTER, connection);
				LOGGER.info("####INFO#### Fine chiamata al transformer fullText");
				allEmails.addAll(fullTextSearchRes);
			}
//			//START VI
			for (final EmailDTO email : allEmails) {
				if (email.getFolderMailArchiviate() != null && !StatoMailEnum.PROTOCOLLATA_AUTOMATICAMENTE.equals(email.getStato())) {
					email.setAttivaRipristinaResponse(true);
				}
			}
//			//END VI
			fillProtocollatore(allEmails);
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_MAIL_MSG + e.getMessage(), e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
			closeConnection(connection);
		}

		return allEmails;
	}

	/**
	 * Costruisce il folder nel quale ricercare le email.
	 * 
	 * @param cpAttiva
	 * @param pp
	 * @return la stringa del folder correttamente costruita.
	 */
	public static String generateDefaultFolder(final String cpAttiva, final PropertiesProvider pp) {
		final String casellePostaliFolder = pp.getParameterByKey(PropertiesNameEnum.FOLDER_ELETTRONICI_FN_METAKEY).split("\\|")[1];
		return "/" + casellePostaliFolder + "/" + cpAttiva + "/" + pp.getParameterByKey(PropertiesNameEnum.FOLDER_MAIL_INBOX_FN_METAKEY);
	}

	/**
	 * Genera la folder di default: "Archiviate".
	 * 
	 * @param cpAttiva
	 * @param pp
	 * @return informazioni sulla folder generata
	 */
	public static String generateDefaultFolderArchiviate(final String cpAttiva, final PropertiesProvider pp) {
		final String casellePostaliFolder = pp.getParameterByKey(PropertiesNameEnum.FOLDER_ELETTRONICI_FN_METAKEY).split("\\|")[1];
		return "/" + casellePostaliFolder + "/" + cpAttiva + "/" + pp.getParameterByKey(PropertiesNameEnum.FOLDER_MAIL_ARCHIVIATE_FN_METAKEY);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IMailFacadeSRV#archiviaMail(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      it.ibm.red.business.enums.StatoMailEnum).
	 */
	@Override
	public void archiviaMail(final IFilenetCEHelper fceh, final StatoMailEnum statoIniziale) {

		final StatoMailEnum statoFinale = StatoMailEnum.ARCHIVIATA;

		if (statoIniziale != null && statoFinale != null) {

			final FilenetCEHelper.Metadato oldMetadato = new Metadato(pp.getParameterByKey(PropertiesNameEnum.STATO_MAIL_METAKEY), statoIniziale.getStatusString());
			final FilenetCEHelper.Metadato newMetadato = new Metadato(pp.getParameterByKey(PropertiesNameEnum.STATO_MAIL_METAKEY), statoFinale.getStatusString());

			LOGGER.info("Vecchio stato: " + statoIniziale.getDescription());
			LOGGER.info("Nuovo stato: " + statoFinale.getDescription());

			final String folder = pp.getParameterByKey(PropertiesNameEnum.FOLDER_ELETTRONICI_FN_METAKEY).split("\\|")[1];

			final FolderSet folderSet = fceh.getFoldersByRoot(folder);

			if (folderSet != null) {

				final String dayByProperties = pp.getParameterByKey(PropertiesNameEnum.DELTA_GIORNI_ELIMINAZIONE_MAIL);

				if (dayByProperties == null) {
					throw new RedException(PARAMETRO_DI_CONFIGURAZIONE_LABEL + PropertiesNameEnum.DELTA_GIORNI_ELIMINAZIONE_MAIL + ERROR_PARAMETRO_NON_TROVATO_MSG);
				}

				final int dayByPropertiesInt = Integer.parseInt(dayByProperties);

				final Calendar c = new GregorianCalendar();
				c.add(Calendar.DAY_OF_MONTH, -dayByPropertiesInt);
				fceh.updateStateMailByState(folder, folderSet, oldMetadato, newMetadato, c.getTime());

			}

			LOGGER.info("UpdateStateMailsServlet -> Cambio di stato delle mails completato con successo");

		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IMailFacadeSRV#aggiornaUtenteProtocollatoreMail(java.lang.String,
	 *      java.lang.Long, it.ibm.red.business.dto.FilenetCredentialsDTO,
	 *      java.lang.Long).
	 */
	@Override
	public void aggiornaUtenteProtocollatoreMail(final String guidMailFilenet, final Long idUtente, final FilenetCredentialsDTO fcDTO, final Long idAoo) {
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(fcDTO, idAoo);

			final Document mailFilenet = fceh.getDocumentByGuid(guidMailFilenet);
			aggiornaUtenteProtocollatoreMail(mailFilenet, idUtente, fceh);
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IMailFacadeSRV#aggiornaUtenteProtocollatoreMail(com.filenet.api.core.Document,
	 *      java.lang.Long, it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public void aggiornaUtenteProtocollatoreMail(final Document mailFilenet, final Long idUtente, final IFilenetCEHelper fceh) {
		final Map<String, Object> metadatiMail = new HashMap<>(1);
		metadatiMail.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_PROTOCOLLATORE_METAKEY), idUtente);

		fceh.updateMetadati(mailFilenet, metadatiMail);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IMailFacadeSRV#isEmailProtocollabile(it.ibm.red.business.dto.FilenetCredentialsDTO,
	 *      java.lang.String, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public boolean isEmailProtocollabile(final FilenetCredentialsDTO fcDto, final String guid, final UtenteDTO utente) {
		final DetailEmailDTO email = getDetailFromGuid(fcDto, guid, utente.getIdAoo());
		return isProtocollabile(email.getStatoMail());
	}

	/**
	 * Verifica che lo stato mail sia protocollabile o meno, quando una email ha lo
	 * stato protocollabile l'email è protocollabile.
	 * 
	 * @param st
	 *            Stato dell'email
	 * 
	 * @return ritorna true quando l'email è protocollabile
	 */
	@Override
	public boolean isProtocollabile(final StatoMailEnum st) {
		return StatoMailEnum.INARRIVO.equals(st) || StatoMailEnum.INARRIVO_NON_PROTOCOLLABILE_AUTOMATICAMENTE.equals(st)
				|| StatoMailEnum.IN_FASE_DI_PROTOCOLLAZIONE_MANUALE.equals(st);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IMailFacadeSRV#hasToAskIfEmailIsProtocollabile(it.ibm.red.business.dto.FilenetCredentialsDTO,
	 *      java.lang.String, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public boolean hasToAskIfEmailIsProtocollabile(final FilenetCredentialsDTO fcDto, final String guid, final UtenteDTO utente) {
		final DetailEmailDTO email = getDetailFromGuid(fcDto, guid, utente.getIdAoo());

		final Long idUtenteProtocollatore = email.getIdUtenteProtocollatore() == null ? null : Long.valueOf(email.getIdUtenteProtocollatore());
		return StatoMailEnum.IN_FASE_DI_PROTOCOLLAZIONE_MANUALE.equals(email.getStatoMail())
				&& (idUtenteProtocollatore == null || !idUtenteProtocollatore.equals(utente.getId()));
	}

	/**
	 * @see it.ibm.red.business.service.IMailSRV#eliminaBozzaEmailDocumento(java.lang.String,
	 *      it.ibm.red.business.dto.FilenetCredentialsDTO, java.lang.Long).
	 */
	@Override
	public final void eliminaBozzaEmailDocumento(final String documentTitle, final FilenetCredentialsDTO fcUtente, final Long idAoo) {
		String emailGuid = null;
		// Connessione al CE come admin
		final FilenetCredentialsDTO fcAdmin = new FilenetCredentialsDTO(fcUtente);
		IFilenetCEHelper fcehAdmin = null;

		try {
			fcehAdmin = FilenetCEHelperProxy.newInstance(fcAdmin, idAoo);

			// Si verifica innanzitutto se esiste un'e-mail associata, tramite la relativa
			// annotation, al documento
			emailGuid = getEmailGuidByDocumentTitle(documentTitle, idAoo, fcehAdmin);

			eliminaBozzaEmailDocumento(documentTitle, emailGuid, idAoo, fcehAdmin);
		} catch (final Exception e) {
			LOGGER.error(ERROR_ELIMINAZIONE_MAIL_MSG + emailGuid, e);
			throw new RedException(ERROR_ELIMINAZIONE_MAIL_MSG + emailGuid, e);
		} finally {
			popSubject(fcehAdmin);
		}
	}

	/**
	 * @see it.ibm.red.business.service.IMailSRV#eliminaBozzaEmailDocumento(java.lang.String,
	 *      java.lang.String, java.lang.Long,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public final void eliminaBozzaEmailDocumento(final String documentTitle, final String emailGuid, final Long idAoo, final IFilenetCEHelper fcehAdmin) {
		try {
			// Esiste una mail associata...
			if (StringUtils.isNotBlank(emailGuid)) {

				// Si rimuove la relativa annotation dal documento
				fcehAdmin.eliminaAnnotationDocumento(documentTitle, FilenetAnnotationEnum.TEXT_MAIL, idAoo);
				
				// Si elimina il "documento" e-mail da FileNet
				fcehAdmin.eliminaMail(emailGuid);

			}
		} catch (final Exception e) {
			LOGGER.error(ERROR_ELIMINAZIONE_MAIL_MSG + emailGuid, e);
			throw new RedException(ERROR_ELIMINAZIONE_MAIL_MSG + emailGuid, e);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IMailFacadeSRV#getMailOriginale(java.lang.String,
	 *      java.lang.String, java.lang.Long,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public DocumentoAllegabileDTO getMailOriginale(final String documentTitle, final String nomeEmail, final Long idAoo, final IFilenetCEHelper fceh) {
		final DocumentoAllegabileDTO emailOriginale = null;

		try {
			final String mailGuid = getEmailGuidByDocumentTitle(documentTitle, idAoo, fceh);
			if (mailGuid != null) {
				final Document documentMail = fceh.getDocumentByGuid(mailGuid);
				if (documentMail != null && documentMail.getProperties().isPropertyPresent(pp.getParameterByKey(PropertiesNameEnum.MESSAGE_ID))) {
					final String messageId = documentMail.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.MESSAGE_ID));
					final String oggettoMail = documentMail.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_EMAIL_METAKEY));
					final String documentTitleMail = documentMail.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
					final String casellaPostale = documentMail.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.CASELLA_POSTALE_METAKEY));

					if (messageId != null) {
						final byte[] emailOriginaleContent = getBlobMailOriginale(messageId, casellaPostale);
						if (emailOriginaleContent != null) {

							return DocumentoAllegabileDTOFactory.getDocumentoAllegabileDTO(documentTitleMail, emailOriginaleContent, oggettoMail, nomeEmail,
									DocumentoAllegabileType.EMAIL, Constants.ContentType.EML, mailGuid, documentMail.get_DateCreated());

						}
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error("getMailOriginale -> Si è verificato un errore durante il recupero dell'email originale relativa al documento: " + documentTitle, e);
			throw new RedException("getMailOriginale -> Si è verificato un errore durante il recupero dell'email originale relativa al documento: " + documentTitle, e);
		}

		return emailOriginale;
	}

	private byte[] getBlobMailOriginale(final String messageId, final String casellaPostale) {
		Connection connection = null;

		try {

			connection = setupConnection(getDataSource().getConnection(), false);
			return emailBckDAO.getBlobMailOriginale(messageId, casellaPostale, connection);

		} catch (final Exception e) {
			rollbackConnection(connection);
			throw new RedException("Si è verificato un errore durante il recupero del content della mail.", e);
		} finally {
			closeConnection(connection);
		}
	}

	private static String getEmailGuidByDocumentTitle(final String documentTitle, final Long idAoo, final IFilenetCEHelper fceh) throws IOException {
		String emailGuid = null;
		final AnnotationDTO annotationTextMail = fceh.getAnnotationDocumentContent(documentTitle, FilenetAnnotationEnum.TEXT_MAIL, idAoo);
		if (annotationTextMail != null) {
			emailGuid = IOUtils.toString(annotationTextMail.getContent());
		}
		return emailGuid;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IMailFacadeSRV#getNomeMailOriginale(com.filenet.api.core.Document).
	 */
	@Override
	public String getNomeMailOriginale(final Document document) {
		String nomeEmail = "emailOriginaleProtocollo";
		final Integer numeroProtocollo = (Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
		final Integer annoProtocollo = (Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
		nomeEmail = nomeEmail + numeroProtocollo + "del" + annoProtocollo + ".eml";
		return nomeEmail;
	}

	/**
	 * @param fcDTO
	 * @param guid
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public SegnaturaMessaggioInteropDTO getSegnaturaFromGUID(final FilenetCredentialsDTO fcDTO, final String guid, final Long idAoo) {
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(fcDTO, idAoo);
			final Document doc = fceh.getDocumentByGuid(guid);
			final String preAssegnatario = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.PREASSEGNATARIO_SEGN_INTEROP_MAIL_METAKEY);
			final String indirizzoMailMittente = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.MAIL_MITTENTE_SEGN_INTEROP_MAIL_METAKEY);
			final String idDocumentoPrincipale = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.ID_DOC_PRINCIPALE_SEGN_INTEROP_MAIL_METAKEY);
			final List<String> allegati = (List<String>) TrasformerCE.getMetadato(doc, PropertiesNameEnum.IDS_ALLEGATI_SEGN_INTEROP_MAIL_METAKEY);

			return new SegnaturaMessaggioInteropDTO(indirizzoMailMittente, idDocumentoPrincipale, preAssegnatario, allegati);
		} catch (final Exception e) {
			LOGGER.error(e);
			return null;
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @param fcDTO
	 * @param guid
	 * @return
	 */
	@Override
	public List<ErroreValidazioneDTO> getErroriInteroperabilita(final FilenetCredentialsDTO fcDTO, final String guid) {
		return new ArrayList<>();
	}

	/**
	 * @param guid
	 * @param fcDTO
	 * @param idAoo
	 * @return Allegato
	 */
	@Override
	public final List<AllegatoDTO> getDocPrincEAllegatiMailInterop(final String guid, final FilenetCredentialsDTO fcDTO, final Long idAoo) {
		final List<AllegatoDTO> allegati = new ArrayList<>();
		AllegatoDTO allegatoMailInterop = null;
		IFilenetCEHelper fceh = null;
		try {

			fceh = FilenetCEHelperProxy.newInstance(fcDTO, idAoo);
			final String guidMail = it.ibm.red.business.utils.StringUtils.restoreGuidFromString(guid);
			final Document doc = fceh.getDocumentByGuid(guidMail);
			final SubSetImpl ssi = (SubSetImpl) doc.get_ChildDocuments();
			final List<?> list = ssi.getList();
			final Iterator<?> itAllegatiMail = list.iterator();

			while (itAllegatiMail.hasNext()) {
				final Document allegatoMailInteropFilenet = (Document) itAllegatiMail.next();
				allegatoMailInterop = new AllegatoDTO();
				allegatoMailInterop.setNomeFile((String) TrasformerCE.getMetadato(allegatoMailInteropFilenet, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
				allegatoMailInterop.setMimeType(allegatoMailInteropFilenet.get_MimeType());
				allegatoMailInterop.setContent(FilenetCEHelper.getDocumentContentAsByte(allegatoMailInteropFilenet));
				allegatoMailInterop.setGuid(allegatoMailInteropFilenet.get_Id().toString());
				allegati.add(allegatoMailInterop);
			}

		} catch (final Exception e) {
			LOGGER.error(e);
		} finally {
			popSubject(fceh);
		}

		return allegati;
	}

	private String operazionePostaElettronica(final UtenteDTO utente, final Document doc, final String tipoOperazione) {
		final String operazioneString = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.OPERAZIONE_POSTA_ELETTRONICA_FN_METAKEY);
		if (utente != null) {
			if (StringUtils.isEmpty(operazioneString)) {
				return tipoOperazione + (utente.getNome() + " " + utente.getCognome());
			} else {
				return tipoOperazione + (utente.getNome() + " " + utente.getCognome()) + "#" + operazioneString;
			}
		}
		return null;

	}

	// START VI
	private List<String> getSearchField() {
		final List<String> searchFields = new ArrayList<>();
		searchFields.add("Id");
		searchFields.add("ComponentBindingLabel");
		searchFields.add("ChildDocuments");
		searchFields.add(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_EMAIL_METAKEY));
		searchFields.add("\"" + pp.getParameterByKey(PropertiesNameEnum.FROM_MAIL_METAKEY) + "\"");
		searchFields.add(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_EMAIL_METAKEY));
		searchFields.add(pp.getParameterByKey(PropertiesNameEnum.NOTA_EMAIL_METAKEY));
		searchFields.add(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_CC_LONG));
		searchFields.add(pp.getParameterByKey(PropertiesNameEnum.MESSAGE_ID));
		searchFields.add(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
		searchFields.add(pp.getParameterByKey(PropertiesNameEnum.STATO_MAIL_METAKEY));
		searchFields.add(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_PROTOCOLLATORE_METAKEY));
		searchFields.add(pp.getParameterByKey(PropertiesNameEnum.DATA_RICEZIONE_MAIL_METAKEY));
		searchFields.add(pp.getParameterByKey(PropertiesNameEnum.DATA_INVIO_MAIL_METAKEY));
		searchFields.add(pp.getParameterByKey(PropertiesNameEnum.DATA_CAMBIO_STATO_EMAIL_METAKEY));
		searchFields.add(pp.getParameterByKey(PropertiesNameEnum.METADATO_STATO_PRE_ARCHIVIAZIONE));
		searchFields.add(pp.getParameterByKey(PropertiesNameEnum.MOTIVO_ELIMINAZIONE_EMAIL_METAKEY));
		searchFields.add(pp.getParameterByKey(PropertiesNameEnum.PREASSEGNATARIO_SEGN_INTEROP_MAIL_METAKEY));
		searchFields.add(pp.getParameterByKey(PropertiesNameEnum.MAIL_MITTENTE_SEGN_INTEROP_MAIL_METAKEY));
		searchFields.add(pp.getParameterByKey(PropertiesNameEnum.MOTIVO_ELIMINAZIONE_EMAIL_METAKEY));
		searchFields.add(pp.getParameterByKey(PropertiesNameEnum.WARNINGS_VALIDAZIONE_SEGN_INTEROP_MAIL_METAKEY));
		searchFields.add(pp.getParameterByKey(PropertiesNameEnum.ERRORI_VALIDAZIONE_SEGN_INTEROP_MAIL_METAKEY));
		searchFields.add(pp.getParameterByKey(PropertiesNameEnum.ID_DOC_PRINCIPALE_SEGN_INTEROP_MAIL_METAKEY));
		searchFields.add(pp.getParameterByKey(PropertiesNameEnum.IDS_ALLEGATI_SEGN_INTEROP_MAIL_METAKEY));
		searchFields.add(pp.getParameterByKey(PropertiesNameEnum.VALIDAZIONE_SEGN_INTEROP_MAIL_METAKEY));
		searchFields.add(pp.getParameterByKey(PropertiesNameEnum.PROTOCOLLI_GENERATI_MAIL_METAKEY));
		searchFields.add(pp.getParameterByKey(PropertiesNameEnum.OPERAZIONE_POSTA_ELETTRONICA_FN_METAKEY));
		searchFields.add(PropertyNames.FOLDERS_FILED_IN);
		return searchFields;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IMailFacadeSRV#spostaMail(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public void spostaMail(final IFilenetCEHelper fceh) {
		final String folderDestinazione = pp.getParameterByKey(PropertiesNameEnum.FOLDER_DESTINAZIONE_MAIL_FN_METAKEY);
		if (folderDestinazione == null) {
			throw new RedException(PARAMETRO_DI_CONFIGURAZIONE_LABEL + PropertiesNameEnum.FOLDER_DESTINAZIONE_MAIL_FN_METAKEY + ERROR_PARAMETRO_NON_TROVATO_MSG);
		}
		LOGGER.info("Folder di destinazione: " + folderDestinazione);

		final String folder = pp.getParameterByKey(PropertiesNameEnum.FOLDER_ELETTRONICI_FN_METAKEY).split("\\|")[1];
		final FolderSet folderSet = fceh.getFoldersByRoot(folder);

		if (folderSet != null) {

			final String monthByProperties = pp.getParameterByKey(PropertiesNameEnum.DELTA_MESI_SPOSTA_MAIL);
			if (monthByProperties == null) {
				throw new RedException(PARAMETRO_DI_CONFIGURAZIONE_LABEL + PropertiesNameEnum.DELTA_MESI_SPOSTA_MAIL + ERROR_PARAMETRO_NON_TROVATO_MSG);
			}
			final int monthByPropertiesInt = Integer.parseInt(monthByProperties);

			final Calendar c = new GregorianCalendar();
			c.add(Calendar.MONTH, -monthByPropertiesInt);

			fceh.spostaMail(folderDestinazione, folder, folderSet, c.getTime());

		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IMailFacadeSRV#orderByDataRicezioneClause().
	 */
	@Override
	public String orderByDataRicezioneClause() {
		return "ORDER BY " + pp.getParameterByKey(PropertiesNameEnum.DATA_RICEZIONE_MAIL_METAKEY);
	}

	/**
	 * Restituisce l'helper per la gestione dei file HTML.
	 * 
	 * @return helper file html
	 */
	public IHtmlFileHelper getHtmlFileHelper() {
		return htmlFileHelper;
	}

	/**
	 * Imposta l'helper per la gestione dei file HTML.
	 * 
	 * @param htmlFileHelper
	 */
	public void setHtmlFileHelper(final IHtmlFileHelper htmlFileHelper) {
		this.htmlFileHelper = htmlFileHelper;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IMailFacadeSRV#creaFileVelinaEmail(it.ibm.red.business.dto.DetailEmailDTO).
	 */
	@Override
	public byte[] creaFileVelinaEmail(final DetailEmailDTO email) throws IOException {
		final InputStream is = creaPdfPreview(email);
		return ByteStreams.toByteArray(is);
	}

	private InputStream creaPdfPreview(final DetailEmailDTO detailEmail) throws IOException {
		setHtmlFileHelper(new HtmlFileHelper());
		InputStream pdfPreview = null;
		// Si genera il content HTML per il documento
		try {
			final TemplateDocumento templateDocumento = new TemplateDocumentoEntrata();

			final ByteArrayInputStream htmlFile = getHtmlFileHelper().creaEmailHtml(templateDocumento, detailEmail);

			final IAdobeLCHelper adobeh = new AdobeLCHelper();
			pdfPreview = adobeh.htmlToPdf(IOUtils.toByteArray(htmlFile)); // Conversione HTML -> PDF
		} catch (final Exception e) {
			LOGGER.warn(e);
			
			final TemplateDocumento templateDocumento = new TemplateDocumentoEntrata();
			final ByteArrayInputStream htmlFile = getHtmlFileHelper().creaEmailHtmlPlainText(templateDocumento, detailEmail);
			final IAdobeLCHelper adobeh = new AdobeLCHelper();
			pdfPreview = adobeh.htmlToPdf(IOUtils.toByteArray(htmlFile)); // Conversione HTML -> PDF
		}

		return pdfPreview;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IMailFacadeSRV#getMailAggiornata(java.lang.String,
	 *      it.ibm.red.business.dto.FilenetCredentialsDTO, java.lang.Long).
	 */
	@Override
	public EmailDTO getMailAggiornata(final String documentTitle, final FilenetCredentialsDTO fcDTO, final Long idAoo) {
		IFilenetCEHelper fceh = null;
		EmailDTO emailAggiornata = null;
		try {
			fceh = FilenetCEHelperProxy.newInstance(fcDTO, idAoo);
			final Document docMail = fceh.getMailAsDocument(documentTitle);
			emailAggiornata = TrasformCE.transform(docMail, TrasformerCEEnum.FROM_DOCUMENTO_TO_MAIL_RICERCA);
		} catch (final Exception ex) {
			LOGGER.error("Errore nel recuperare la mail aggiornata : " + ex);
			throw new RedException("Errore nel recuperare la mail aggiornata : " + ex);
		} finally {
			popSubject(fceh);
		}
		return emailAggiornata;
	}

}
