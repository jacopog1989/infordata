/**
 * 
 */
package it.ibm.red.business.service.ws.concrete;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.business.service.facade.IREDServiceFacadeSRV;
import it.ibm.red.business.service.ws.ICheckDocumentWsSRV;
import it.ibm.red.business.service.ws.auth.DocumentServiceAuthorizable;
import it.ibm.red.webservice.model.documentservice.dto.Servizio;
import it.ibm.red.webservice.model.documentservice.types.documentale.ClasseDocumentale;
import it.ibm.red.webservice.model.documentservice.types.documentale.Metadato;
import it.ibm.red.webservice.model.documentservice.types.messages.RedCheckInType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedCheckOutType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedUndoCheckOutType;

/**
 * @author APerquoti
 *
 */
@Service
public class CheckDocumentWsSRV implements ICheckDocumentWsSRV {

	/**
	 * Serializable.
	 */
	private static final long serialVersionUID = -1570754634990189661L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(CheckDocumentWsSRV.class.getName());
	
	/**
	 * Servizio red.
	 */
	@Autowired
	private IREDServiceFacadeSRV redServiceSRV;

	/**
	 * @see it.ibm.red.business.service.ws.facade.ICheckDocumentWsFacadeSRV#checkOut(it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.RedCheckOutType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_CHECK_OUT)
	public void checkOut(final RedWsClient client, final RedCheckOutType request) {
		
		try {
			redServiceSRV.checkOut(request.getDocumentTitle(), request.getIdUtente().toString());
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il checkout del documento: " + e.getMessage(), e);
			throw new RedException("Si è verificato un errore durante il checkout del documento: " + e.getMessage(), e);
		}
		
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.ICheckDocumentWsFacadeSRV#undoCheckout(it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.RedUndoCheckOutType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_UNDO_CHECK_OUT)
	public void undoCheckout(final RedWsClient client, final RedUndoCheckOutType request) {
		
		try {
			redServiceSRV.undoCheckOut(request.getDocumentTitle(), request.getIdUtente().toString());
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la cancellazione del checkout del documento: " + e.getMessage(), e);
			throw new RedException("Si è verificato un errore durante la cancellazione del checkout del documento: " + e.getMessage(), e);
		}
		
	}
	
	/**
	 * @see it.ibm.red.business.service.ws.facade.ICheckDocumentWsFacadeSRV#checkIn(it.
	 *      ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.RedCheckInType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_CHECK_IN)
	public void checkIn(final RedWsClient client, final RedCheckInType request) {
		
		try {
			
			final String documentTitle = request.getDocumento().getIdDocumento();
			final String idUtente = request.getIdUtente().toString();
			final byte[] content = request.getDocumento().getDataHandler();
			final ClasseDocumentale classeDocumentale = request.getDocumento().getClasseDocumentale();
			
			final Map<String, Object> metadatiMap = new HashMap<>();
			if (classeDocumentale != null && classeDocumentale.getMetadato() != null && !classeDocumentale.getMetadato().isEmpty()) {
				
				for (final Metadato m : classeDocumentale.getMetadato()) {
					if (m.getValue() != null) {
						metadatiMap.put(m.getKey(), m.getValue());
					} else {
						metadatiMap.put(m.getKey(), m.getMultiValues());
					}
				}
				
			}
			
			redServiceSRV.checkIn(documentTitle, idUtente, metadatiMap, null, content);
			
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'operazione di checkin del documento: " + e.getMessage(), e);
			throw new RedException("Si è verificato un errore durante l'operazione di checkin del documento: " + e.getMessage(), e);
		}
	}

}
