package it.ibm.red.business.dto;

/**
 * Raccoglie gli id con cui indicare un documento.
 * @author a.difolca
 *
 */
public class IdGenericoDTO {
    /**
     * Document title.
     */
	private Integer documentTitle;
	
    /**
     * Numero documento.
     */
	private Integer numeroDocumento;

	/**
	 * Recupera il document title.
	 * @return
	 */
	public Integer getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Imposta il document title.
	 * @param documentTitle document title
	 */
	public void setDocumentTitle(final Integer documentTitle) {
		this.documentTitle = documentTitle;
	}

	/**
	 * Ottiene il numero di documento.
	 * @return numero di documento
	 */
	public Integer getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * Imposta il numero di documento.
	 * @param numeroDocumento numero di documento
	 */
	public void setNumeroDocumento(final Integer numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
}
