package it.ibm.red.business.dto;

import java.util.Date;

import it.ibm.red.business.utils.StringUtils;

/**
 * Dto per la gestione delle approvazioni.
 * 
 * @author CRISTIANOPierascenzi
 *
 */
public class ApprovazioneDTO extends AbstractDTO {

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Id approvazione.
	 */
	private Integer idApprovazione;

	/**
	 * Data approvazione.
	 */
	private Date dataApprovazione;
	
	/**
	 * Descrizione approvazione.
	 */
	private String descApprovazione;

	/**
	 * Descrizione utente.
	 */
	private String descUtente;

	/**
	 * Descrizione ufficio.
	 */
	private String descUfficio;
	
	/**
	 * Motivazione.
	 */
	private String motivazione;
	
	/**
	 * Stampigliatura firma.
	 */
	private Integer stampigliaturaFirma;
	
	/**
	 * Nome utente.
	 */
	private String nome;
	
	/**
	 * Cognome utente.
	 */
	private String cognome;
	
	/**
	 * Nome ruolo.
	 */
	private String nomeRuolo;
	
	/**
	 * Tipo Approvazione.
	 */
	private Integer idTipoApprovazione;
	
	/**
	 * Costruttore.
	 * 
	 * @param inIdApprovazione		identificativo approvazione
	 * @param inDataApprovazione	data approvazione
	 * @param inDescApprovazione	descrizione approvazione
	 * @param inMotivazione			motivazione
	 * @param inDescUtente			descrizione utente
	 * @param inDescUfficio			descrizione ufficio
	 * @param inStampigliaturaFirma	tipo stampigliatura firma
	 * @param inStampigliaturaFirma	tipo stampigliatura firma
	 * @param inStampigliaturaFirma	tipo stampigliatura firma
	 * @param inStampigliaturaFirma	tipo stampigliatura firma
	 */
	public ApprovazioneDTO(final Integer inIdApprovazione, final Date inDataApprovazione, final String inDescApprovazione, final String inMotivazione, 
			final String inDescUtente, final String inDescUfficio, final Integer inStampigliaturaFirma, final String inNome, final String inCognome, 
			final String inNomeRuolo, final Integer inIdTipoApprovazione) {
		idApprovazione = inIdApprovazione;
		dataApprovazione = inDataApprovazione;
		descApprovazione = inDescApprovazione;
		descUtente = inDescUtente;
		descUfficio = inDescUfficio;
		motivazione = inMotivazione;
		stampigliaturaFirma = inStampigliaturaFirma;
		nome = inNome;
		cognome = inCognome;
		nomeRuolo = inNomeRuolo;
		idTipoApprovazione = inIdTipoApprovazione;
	}
	
	
	/**
	 * Getter.
	 * 
	 * @return	identificativo approvazione
	 */
	public Integer getIdApprovazione() {
		return idApprovazione;
	}

	/**
	 * Getter.
	 * 
	 * @return	data approvazione
	 */
	public Date getDataApprovazione() {
		return dataApprovazione;
	}

	/**
	 * Getter.
	 * 
	 * @return	descrizione approvazione
	 */
	public String getDescApprovazione() {
		String output = descApprovazione;
		if (!StringUtils.isNullOrEmpty(motivazione)) {
			output = motivazione + ": " + descApprovazione;
		}
		return output;
	}

	/**
	 * Getter.
	 * 
	 * @return	descrizione utente
	 */
	public String getDescUtente() {
		return descUtente;
	}

	/**
	 * Getter.
	 * 
	 * @return	descrizione ufficio
	 */
	public String getDescUfficio() {
		return descUfficio;
	}

	/**
	 * Restituisce la stampigliatura di firma.
	 * @return stampigliaturaFirma
	 */
	public Integer getStampigliaturaFirma() {
		return stampigliaturaFirma;
	}

	/**
	 * Restituisce la motivazione come String.
	 * @return motivazione
	 */
	public String getMotivazione() {
		return motivazione;
	}

	/**
	 * Imposta la motivazione.
	 * @param motivazione
	 */
	public void setMotivazione(final String motivazione) {
		this.motivazione = motivazione;
	}

	/**
	 * Restituisce il nome.
	 * @return nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Imposta il nome.
	 * @param nome
	 */
	public void setNome(final String nome) {
		this.nome = nome;
	}

	/**
	 * Restituisce il cognome.
	 * @return cognome
	 */
	public String getCognome() {
		return cognome;
	}

	/**
	 * Imposta il cognome.
	 * @param cognome
	 */
	public void setCognome(final String cognome) {
		this.cognome = cognome;
	}

	/**
	 * Restituisce il nome del ruolo.
	 * @return nomeRuolo
	 */
	public String getNomeRuolo() {
		return nomeRuolo;
	}

	/**
	 * Imposta il nome del ruolo.
	 * @param nomeRuolo
	 */
	public void setNomeRuolo(final String nomeRuolo) {
		this.nomeRuolo = nomeRuolo;
	}

	/**
	 * Imposta l'id dell'approvazione.
	 * @param idApprovazione
	 */
	public void setIdApprovazione(final Integer idApprovazione) {
		this.idApprovazione = idApprovazione;
	}

	/**
	 * Imposta la data di approvazione.
	 * @param dataApprovazione
	 */
	public void setDataApprovazione(final Date dataApprovazione) {
		this.dataApprovazione = dataApprovazione;
	}

	/**
	 * Imposta la descrizione dell'approvazione.
	 * @param descApprovazione
	 */
	public void setDescApprovazione(final String descApprovazione) {
		this.descApprovazione = descApprovazione;
	}

	/**
	 * Imposta la descrizione dell'utente.
	 * @param descUtente
	 */
	public void setDescUtente(final String descUtente) {
		this.descUtente = descUtente;
	}

	/**
	 * Imposta la descrizione dell'ufficio.
	 * @param descUfficio
	 */
	public void setDescUfficio(final String descUfficio) {
		this.descUfficio = descUfficio;
	}

	/**
	 * Imposta la stampigliatura di firma.
	 * @param stampigliaturaFirma
	 */
	public void setStampigliaturaFirma(final Integer stampigliaturaFirma) {
		this.stampigliaturaFirma = stampigliaturaFirma;
	}

	/**
	 * Restituisce l'id del tipo approvazione.
	 * @return idTipoApprovazione
	 */
	public Integer getIdTipoApprovazione() {
		return idTipoApprovazione;
	}
}
