package it.ibm.red.business.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author CristianoPierascenzi
 *
 * @param <T>	Tipo generico del nodo
 */
public class NodoDTO<T> extends AbstractDTO {

	/** 
	 * The Constant serialVersionUID. 
	 */
	private static final long serialVersionUID = -7380754837318673150L;

	/** 
	 * Data 
	 */
	private transient T data;
	
	/** 
	 * Parent 
	 */
	private NodoDTO<T> parent;
	
	/** 
	 * Childer 
	 */
	private List<NodoDTO<T>> children;
	
	
	/**
	 * Costruttore nodo DTO.
	 *
	 * @param data the data
	 */
	public NodoDTO(final T data) {
		this.data = data;
	}
	
	
	/**
	 * Costruttore nodo DTO.
	 *
	 * @param data the data
	 * @param parent the parent
	 * @param children the children
	 */
	public NodoDTO(final T data, final NodoDTO<T> parent, final List<NodoDTO<T>> children) {
		this.data = data;
		this.parent = parent;
		this.children = children;
	}


	/** 
	 *
	 * @return the data
	 */
	public T getData() {
		return data;
	}


	/** 
	 *
	 * @return the parent
	 */
	public NodoDTO<T> getParent() {
		return parent;
	}


	/** 
	 *
	 * @return the children
	 */
	public List<NodoDTO<T>> getChildren() {
		return children;
	}
	
	
	/**
	 * Adds the child.
	 *
	 * @param childData the child data
	 */
	public void addChild(final T childData) {
		if (this.children == null) {
			this.children = new ArrayList<>();
		}
		
		final NodoDTO<T> childNode = new NodoDTO<>(childData, this, null);
		this.children.add(childNode);
	}
}
