package it.ibm.red.business.dto;

import java.io.Serializable;

/**
 * DTO per la definizione di un lock.
 */
public class GestioneLockDTO implements Serializable {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 677662406947036694L;

	/**
	 * Utente locker.
	 */
	private Long idUtenteCheHaLock;
	
	/**
	 * Username locker.
	 */
	private String usernameUtenteLock;
	
	/**
	 * Flag no lock.
	 */
	private Boolean nessunLock;
	
	/**
	 * Flag carico.
	 */
	private boolean inCarico;
	
	/**
	 * Nome utente.
	 */
	private String nome;
	
	/**
	 * Cognome utente.
	 */
	private String cognome;
	
	/**
	 * Document title documento.
	 */
	private Long documentTitle;
	
	/**
	 * Costruttore vuoto.
	 */
	public GestioneLockDTO() {
	}
	
	/**
	 * Costruttore di default
	 * @param idUtenteCheHaLock
	 * @param nessunLock
	 */
	public GestioneLockDTO(final Long idUtenteCheHaLock, final Boolean nessunLock) {
		this.idUtenteCheHaLock = idUtenteCheHaLock;
		this.nessunLock = nessunLock;
	}
	
	/**
	 * Recupera l'idUtente del detentore del lock
	 * @return id dell'Utente che detiene il lock
	 */
	public Long getIdUtenteCheHaLock() {
		return idUtenteCheHaLock;
	}
	
	/**
	 * Imposta l'idUtente che detiene il lock
	 * @param idUtenteCheHaLock
	 */
	public void setIdUtenteCheHaLock(final Long idUtenteCheHaLock) {
		this.idUtenteCheHaLock = idUtenteCheHaLock;
	}
	
	/**
	 * Restituisce se l'utente detiene o meno il lock
	 * @return true, se l'utente non detiene lock; false, se lo detiene, null se è di un altro utente
	 */
	public Boolean getNessunLock() {
		return nessunLock;
	}
	
	/**
	 * Imposta il booleano che definisce chi detiene il lock rispetto all'utente.
	 * @param nessunLock
	 */
	public void setNessunLock(final Boolean nessunLock) {
		this.nessunLock = nessunLock;
	}
	
	/**
	 * Recupera l'username dell'utente che detiene il lock
	 * @return username utente
	 */
	public String getUsernameUtenteLock() {
		return usernameUtenteLock;
	}
	
	/** Imposta l'username dell'utente che detiene il lock.
	 * @param usernameUtenteLock
	 */
	public void setUsernameUtenteLock(final String usernameUtenteLock) {
		this.usernameUtenteLock = usernameUtenteLock;
	}

	/** Definisce se l'utente ha in carico il documento.
	 * {@link GestioneLockDTO.documentTitle}
	 * @return isInCarico
	 */
	public boolean isInCarico() {
		return inCarico;
	}

	/**
	 * Imposta la presa in carico del documento da parte dell'utente.
	 * @param inCarico
	 */
	public void setInCarico(final boolean inCarico) {
		this.inCarico = inCarico;
	}

	/**
	 * Restituisce il nome dell'utente.
	 * @return nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Imposta il nome dell'utente.
	 * @param nome
	 */
	public void setNome(final String nome) {
		this.nome = nome;
	}

	/**
	 * Restituisce il cognome dell'utente.
	 * @return cognome
	 */
	public String getCognome() {
		return cognome;
	}

	/**
	 * Imposta il cognome dell'utente.
	 * @param cognome
	 */
	public void setCognome(final String cognome) {
		this.cognome = cognome;
	}

	/**
	 * Restituisce il documentTitle del documento.
	 * @return documentTitle
	 */
	public Long getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Imposta il documentTitle del documento.
	 * @param documentTitle
	 */
	public void setDocumentTitle(final Long documentTitle) {
		this.documentTitle = documentTitle;
	}
}
