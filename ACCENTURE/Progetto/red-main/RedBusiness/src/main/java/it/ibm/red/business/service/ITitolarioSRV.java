package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.service.facade.ITitolarioFacadeSRV;

/**
 * Interfaccia del servizio che gestisce i titolari.
 */
public interface ITitolarioSRV extends ITitolarioFacadeSRV {

	/**
	 * Ottiene i figli radice tramite l'id del nodo.
	 * @param idAOO
	 * @param idNodo
	 * @param connection
	 * @return lista di titolari
	 */
	List<TitolarioDTO> getFigliRadice(Long idAOO, Long idNodo, Connection connection);
	
	/**
	 * Ottiene il nodo tramite l'indice.
	 * @param idAOO
	 * @param indice
	 * @param connection
	 * @return titolario
	 */
	TitolarioDTO getNodoByIndice(Long idAOO, String indice, Connection connection);
	
}
