package it.ibm.red.business.persistence.model;

import java.io.Serializable;
import java.util.Date;

/**
 * Model che definisce una Legislatura.
 */
public class Legislatura implements Serializable {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 2484053786339940208L;

    /**
     * Numero.
     */
	private int numero;
	
    /**
     * Descrizione.
     */
	private String descrizione;
	
    /**
     * Data avvio.
     */
	private Date dataInizio;
	
    /**
     * Data fine.
     */
	private Date dataFine;

	/**
	 * Costruttore vuoto.
	 */
	public Legislatura() {
	}

	/**
	 * Costruttore di default.
	 * @param num
	 * @param desc
	 * @param dataInizio
	 * @param dataFine
	 */
	public Legislatura(final int num, final String desc, final Date dataInizio, final Date dataFine) {
		this.numero = num;
		this.descrizione = desc;
		this.dataInizio = dataInizio;
		this.dataFine = dataFine;
	}

	/**
	 * Restitusisce il numero della legistlatura.
	 * @return numero
	 */
	public int getNumero() {
		return numero;
	}

	/**
	 * Imposta il numero della legislatura.
	 * @param numero
	 */
	public void setNumero(final int numero) {
		this.numero = numero;
	}

	/**
	 * Restitusisce la descrizione della legislatura.
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Imposta la descrizione della legislatura.
	 * @param descrizione
	 */
	public void setDescrizione(final String descrizione) {
		this.descrizione = descrizione;
	}

	/**
	 * Restituisce la data dell'inizio della legislatura.
	 * @return dataInizio
	 */
	public Date getDataInizio() {
		return dataInizio;
	}

	/**
	 * Imposta la data dell'inizio della legislatura.
	 * @param dataInizio
	 */
	public void setDataInizio(final Date dataInizio) {
		this.dataInizio = dataInizio;
	}

	/**
	 * Restituisce la data della fine della legislatura.
	 * @return dataFine
	 */
	public Date getDataFine() {
		return dataFine;
	}

	/**
	 * Imposta la data della fine della legislatura.
	 * @param dataFine
	 */
	public void setDataFine(final Date dataFine) {
		this.dataFine = dataFine;
	}
}