package it.ibm.red.business.service.facade;

import java.io.Serializable;

import it.ibm.red.business.dto.UtenteDTO;

/**
 * Facade del servizio che gestisce l'aggiornamento del content.
 */
public interface IUpdateContentFacadeSRV extends Serializable {
	
	/**
	 * Inserisce la nuova versione del documento su FileNet.
	 * @param workflowNumber
	 * @param newContent
	 * @param utente
	 */
	void updateContent(String workflowNumber, byte[] newContent, UtenteDTO utente);

}
