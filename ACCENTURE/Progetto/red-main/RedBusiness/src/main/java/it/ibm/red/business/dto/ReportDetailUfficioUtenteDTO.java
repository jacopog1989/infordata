package it.ibm.red.business.dto;

import java.util.Date;

/**
 * @author m.crescentini
 */
public class ReportDetailUfficioUtenteDTO extends AbstractDTO{
	
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -4844175454462323883L;

	/**
	 * Numero protocollo.
	 */
	private String numeroProtocollo;
	
	/**
	 * Numero documento.
	 */
	private String numeroDocumento;
	
	/**
	 * Anno documento.
	 */
	private String annoDocumento;
	
	/**
	 * Numero fasccicolo.
	 */
	private String numeroFascicolo;
	
	/**
	 * Oggetto.
	 */
	private String oggetto;
	
	/**
	 * Oggetto breve.
	 */
	private String oggettoBreve;
	
	/**
	 * Data creazione.
	 */
	private String dataCreazione;
	
	/**
	 * Utente creatore.
	 */
	private String utenteCreatore;
	
	/**
	 * Anno di creazione del documento preso dalla d_eventi_custom.
	 */
	private String anno;
	
	/**
	 * Descrizione ufficio della tabella nodo.
	 */
	private String descrizione;
	
	/**
	 * Tipo assegnazione.
	 */
	private String tipoAssegnazione;
	
	/**
	 * Data assegnazione.
	 */
	private Date dataAssegnazione;
	
	/**
	 * Data assegnazione formato stringa.
	 */
	private String dataAssegnazioneStr;
	
	/**
	 * Identificativo documento.
	 */
	private String idDocumento;
	
	/**
	 * Restituisce il numero del protocollo.
	 * @return numero protocollo
	 */
	public String getNumeroProtocollo() {
		return numeroProtocollo;
	}
	
	/**
	 * Imposta il numero del protocollo.
	 * @param numeroProtocollo
	 */
	public void setNumeroProtocollo(final String numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}
	
	/**
	 * Restituisce il numero del documento.
	 * @return numero documento
	 */
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	
	/**
	 * Imposta il numero del documento.
	 * @param numeroDocumento
	 */
	public void setNumeroDocumento(final String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	
	/**
	 * Restituisce l'anno del documento.
	 * @return anno documento
	 */
	public String getAnnoDocumento() {
		return annoDocumento;
	}
	
	/**
	 * Imposta l'anno del documento.
	 * @param annoDocumento
	 */
	public void setAnnoDocumento(final String annoDocumento) {
		this.annoDocumento = annoDocumento;
	}
	
	/**
	 * Restituisce il numero del fascicolo.
	 * @return numero fascicolo
	 */
	public String getNumeroFascicolo() {
		return numeroFascicolo;
	}
	
	/**
	 * Imposta il numero del fascicolo.
	 * @param numeroFascicolo
	 */
	public void setNumeroFascicolo(final String numeroFascicolo) {
		this.numeroFascicolo = numeroFascicolo;
	}
	
	/**
	 * Restituisce l'oggetto.
	 * @return oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}
	
	/**
	 * Imposta l'oggetto.
	 * @param oggetto
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}
	
	/**
	 * Restituisce la data di creazione.
	 * @return data creazione
	 */
	public String getDataCreazione() {
		return dataCreazione;
	}
	
	/**
	 * Imposta la data di creazione.
	 * @param dataCreazione
	 */
	public void setDataCreazione(final String dataCreazione) {
		this.dataCreazione = dataCreazione;
	}
	
	/**
	 * Restituisce l'utente creatore.
	 * @return utente creatore
	 */
	public String getUtenteCreatore() {
		return utenteCreatore;
	}
	
	/**
	 * Imposta l'utente creatore.
	 * @param utenteCreatore
	 */
	public void setUtenteCreatore(final String utenteCreatore) {
		this.utenteCreatore = utenteCreatore;
	}
	
	/**
	 * Restituisce l'anno.
	 * @return anno
	 */
	public String getAnno() {
		return anno;
	}
	
	/**
	 * Imposta l'anno.
	 * @param anno
	 */
	public void setAnno(final String anno) {
		this.anno = anno;
	}
	
	/**
	 * Restituisce la descrizione.
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}
	
	/**
	 * Imposta la descrizione.
	 * @param descrizione
	 */
	public void setDescrizione(final String descrizione) {
		this.descrizione = descrizione;
	}
	
	/**
	 * Restituisce il tipo assegnazione.
	 * @return tipo assegnazione
	 */
	public String getTipoAssegnazione() {
		return tipoAssegnazione;
	}
	
	/**
	 * Imposta il tipo assegnazione.
	 * @param tipoAssegnazione
	 */
	public void setTipoAssegnazione(final String tipoAssegnazione) {
		this.tipoAssegnazione = tipoAssegnazione;
	}
	
	/**
	 * Restituisce la data assegnazione.
	 * @return data assegnazione
	 */
	public Date getDataAssegnazione() {
		return dataAssegnazione;
	}
	
	/**
	 * Imposta la data assegnazione.
	 * @param dataAssegnazione
	 */
	public void setDataAssegnazione(final Date dataAssegnazione) {
		this.dataAssegnazione = dataAssegnazione;
	}
	
	/**
	 * Restituisce la data assegnazione come String.
	 * @return data assegnazione come String
	 */
	public String getDataAssegnazioneStr() {
		return dataAssegnazioneStr;
	}
	
	/**
	 * Imposta la data assegnazione come String.
	 * @param dataAssegnazioneStr
	 */
	public void setDataAssegnazioneStr(final String dataAssegnazioneStr) {
		this.dataAssegnazioneStr = dataAssegnazioneStr;
	}
	
	/**
	 * Restituisce l'oggetto abbreviato.
	 * @return oggetto breve
	 */
	public String getOggettoBreve() {
		return oggettoBreve;
	}
	
	/**
	 * Imposta l'oggetto abbreviato.
	 * @param oggettoBreve
	 */
	public void setOggettoBreve(final String oggettoBreve) {
		this.oggettoBreve = oggettoBreve;
	}
	
	/**
	 * Restituisce l'id del documento.
	 * @return id documento
	 */
	public String getIdDocumento() {
		return idDocumento;
	}
	
	/**
	 * Imposta l'id del documento.
	 * @param idDocumento
	 */
	public void setIdDocumento(final String idDocumento) {
		this.idDocumento = idDocumento;
	}
}