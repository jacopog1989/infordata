/**
 * 
 */
package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.core.Document;

import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.IProtocollaMailDAO;
import it.ibm.red.business.dao.ITestoPredefinitoDAO;
import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.DestinatarioEsternoDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.ProtocollaMailAooConfDTO;
import it.ibm.red.business.dto.ProtocollaMailCodaDTO;
import it.ibm.red.business.dto.TestoPredefinitoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.MailOperazioniEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ProtocollaMailStatiEnum;
import it.ibm.red.business.enums.ProvenienzaSalvaDocumentoEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.SalvaDocumentoErroreEnum;
import it.ibm.red.business.enums.StatoMailEnum;
import it.ibm.red.business.enums.TestoPredefinitoEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.ICasellePostaliSRV;
import it.ibm.red.business.service.ICreaDocumentoRedUscitaSRV;
import it.ibm.red.business.service.IProtocollaMailSRV;
import it.ibm.red.business.service.facade.IRiassegnazioneFacadeSRV;
import it.ibm.red.business.service.facade.IWorkflowFacadeSRV;

/**
 * @author adilegge
 *
 */
@Service
public class ProtocollaMailSRV extends CreaDocumentoRedEntrataAbstractSRV implements IProtocollaMailSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 7824237207445011961L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ProtocollaMailSRV.class.getName());
	
	/**
	 * DAO.
	 */
	@Autowired
	private IProtocollaMailDAO protocollaMailDAO;
	
	/**
	 * DAO.
	 */
	@Autowired
	private IAooDAO aooDAO;
	
	/**
	 * DAO.
	 */
	@Autowired
	private ITestoPredefinitoDAO testoPredefinitoDAO;
	
	/**
	 * Service.
	 */
	@Autowired
	private ICasellePostaliSRV casellePostaliSRV;
	
	/**
	 * Service.
	 */
	@Autowired
	private IRiassegnazioneFacadeSRV riassegnaSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IWorkflowFacadeSRV workflowSRV;
	
	/**
	 * Service.
	 */
	@Autowired
	@Qualifier("CreaDocumentoUscitaWsSRV")
	private ICreaDocumentoRedUscitaSRV creaDocumentoUscitaSRV;
	
	/**
	 * @see it.ibm.red.business.service.concrete.CreaDocumentoRedAbstractSRV#getProvenienza().
	 */
	@Override
	protected ProvenienzaSalvaDocumentoEnum getProvenienza() {
		return ProvenienzaSalvaDocumentoEnum.INOLTRO_RIFIUTO_AUTOMATICO;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IProtocollaMailFacadeSRV#getConfAoo(java.lang.Long,
	 *      it.ibm.red.business.enums.MailOperazioniEnum).
	 */
	@Override
	public ProtocollaMailAooConfDTO getConfAoo(final Long idAoo, final MailOperazioniEnum operazione) {
		Connection connection =  null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return protocollaMailDAO.getConfigurazione(idAoo, operazione, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero delle configurazioni per l'aoo " + idAoo + " e l'operazione " + operazione, e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}	
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IProtocollaMailFacadeSRV#getFirstItemToProcess(java.lang.Long).
	 */
	@Override
	public ProtocollaMailCodaDTO getFirstItemToProcess(final Long idAoo) {
		Connection connection =  null;
		ProtocollaMailCodaDTO item = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			item = protocollaMailDAO.getFirstInCodaLocked(idAoo, connection);
			
			if (item == null) {
				return null;
			}
			
			//aggiorna stato a "preso in carico"
			item.setStato(ProtocollaMailStatiEnum.PRESO_IN_CARICO);
			updateItemInCoda(item, connection);
			
			return item;
		} catch (final Exception e) {
			LOGGER.error("Errore nella presa in carico del primo item in coda per l'aoo " + idAoo, e);
			if (item != null) {
				item.setStato(ProtocollaMailStatiEnum.ERRORE_IN_FASE_DI_PRESA_IN_CARICO);
				item.setDescrizioneErrore(e.getMessage());
				updateItemInCoda(item, connection);
			}
			
		} finally {
			try {
				commitConnection(connection); // committo sempre in modo da aggiornare lo stato dell'item in coda anche nel
												// caso di errore
			} catch (final Exception e) {
				LOGGER.error(e);
				rollbackConnection(connection);
			}
			closeConnection(connection);
		}
		
		return item;
	}
	
	private void updateItemInCoda(final ProtocollaMailCodaDTO item, final Connection connection) {
		protocollaMailDAO.updateCoda(item.getIdCoda(), item.getStato().getId(), item.getDescrizioneErrore(),
				item.getIdDocumentoEntrata(), item.getWobNumberEntrata(), item.getIdDocumentoUscita(), 
				item.getWobNumberUscita(), connection);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IProtocollaMailFacadeSRV#processItem(it.ibm.red.business.dto.ProtocollaMailCodaDTO,
	 *      it.ibm.red.business.dto.ProtocollaMailAooConfDTO).
	 */
	@Override
	public void processItem(final ProtocollaMailCodaDTO item, final ProtocollaMailAooConfDTO conf) {
		IFilenetCEHelper fceh = null;
		Connection connection =  null;
		String idDocumentoEntrata = null;
		String wobNumberEntrata = null;
		String idDocumentoUscita = null;
		String wobNumberUscita = null;
		
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			
			final Aoo aoo = aooDAO.getAoo(item.getIdAoo(), connection);
			final String usernameKey = aoo.getCodiceAoo() + "." + PropertiesNameEnum.AOO_SISTEMA_AMMINISTRATORE.getKey();
			final String username = PropertiesProvider.getIstance().getParameterByString(usernameKey);
			final AooFilenet aooFilenet = aoo.getAooFilenet();
			
			fceh = FilenetCEHelperProxy.newInstance(username, aooFilenet.getPassword(), aooFilenet.getUri(), 
					aooFilenet.getStanzaJaas(), aooFilenet.getObjectStore(), aoo.getIdAoo());
			
			final Document documentMail = fceh.getDocumentByGuid(item.getGuidMail());
			final String oggetto = getOggetto(documentMail);
			final String notaMail = (String) TrasformerCE.getMetadato(documentMail, PropertiesNameEnum.NOTA_EMAIL_METAKEY);
			final String mittenteMail = (String) TrasformerCE.getMetadato(documentMail, PropertiesNameEnum.FROM_MAIL_METAKEY);
			
			UtenteDTO utenteProtocollatore = null;
			boolean esitoStep = false;
			try {
				// #### Creazione documento in ingresso
				
				// Crea l'utente
				utenteProtocollatore = getUtente(item.getIdUtenteOperazione(), item.getIdRuoloOperazione(), item.getIdNodoOperazione(), connection);
				
				final EsitoSalvaDocumentoDTO esitoDocIngresso = creaDocumentoRedEntrataDaMail(item.getGuidMail(), oggetto, item.isMantieniAllegatiOriginali(), 
						conf.getIndiceClassificazione(), item.getIdContattoMittente(), notaMail, utenteProtocollatore, aoo, connection);
				
				if (esitoDocIngresso.isEsitoOk()) {
				
					idDocumentoEntrata = esitoDocIngresso.getDocumentTitle();
					wobNumberEntrata = esitoDocIngresso.getWobNumber();
					
					item.setIdDocumentoEntrata(idDocumentoEntrata);
					item.setWobNumberEntrata(wobNumberEntrata);
					item.setStato(ProtocollaMailStatiEnum.DOCUMENTO_IN_ENTRATA_CREATO);
					item.setDescrizioneErrore(null);
					updateItemInCoda(item, connection);
					
					//assegna processo all'utente => non server differenziare per UCB, la ricerca può essere fatta su tutta la coda Da Lavorare
					final boolean assegnatoAdUtente = workflowSRV.isDocumentInQueue(utenteProtocollatore, DocumentQueueEnum.DA_LAVORARE, 
							idDocumentoEntrata, item.getIdNodoOperazione(), item.getIdUtenteOperazione(), 
							Arrays.asList((long) TipoAssegnazioneEnum.COMPETENZA.getId()));
					// Se non è stato assegnato automaticamente all'utente
					if (!assegnatoAdUtente) {
						final Collection<EsitoOperazioneDTO> esitiAssegnazione = riassegnaSRV.riassegna(utenteProtocollatore, Arrays.asList(wobNumberEntrata), 
								ResponsesRedEnum.ASSEGNA, item.getIdNodoOperazione(), item.getIdUtenteOperazione(), conf.getOggettoTemplate());
						final Iterator<EsitoOperazioneDTO> it = esitiAssegnazione.iterator();
						while (it.hasNext()) {
							final EsitoOperazioneDTO esitoAssegnazione = it.next();
							if (!esitoAssegnazione.isEsito()) {
								throw new RedException(esitoAssegnazione.getNote());
							}
							break;
						}
					}
					
				} else {
					final StringBuilder errorMsg = new StringBuilder("");
					for (final SalvaDocumentoErroreEnum e : esitoDocIngresso.getErrori()) {
						if (e.getMessaggio() != null) {
							errorMsg.append(e.getMessaggio());
						} else {
							errorMsg.append(e.toString());
						}
					}
					throw new RedException(errorMsg.toString());
				}
				
				esitoStep = true;
			} catch (final Exception e) {
				LOGGER.error("Errore in fase di creazione del documento in ingresso per l'item " + item.getIdCoda(), e);
				item.setStato(ProtocollaMailStatiEnum.ERRORE_IN_FASE_DI_CREAZIONE_DOCUMENTO_IN_ENTRATA);
				item.setDescrizioneErrore(e.getMessage());
				updateItemInCoda(item, connection);
			} 
			
			if (esitoStep) {
				esitoStep = false;
				try {
					final List<DestinatarioRedDTO> destinatariRed = getDestinatariRed(item.getContattiDestinatari());
					
					// #### Creazione documento in uscita
					final EsitoSalvaDocumentoDTO esitoDocUscita = creaDocumentoUscitaSRV.creaDocumentoRedUscitaRispostaDaTemplate(idDocumentoEntrata, utenteProtocollatore, 
							conf.getIndiceClassificazione(), conf.getGuidTemplate(), conf.getNomeTemplate(), conf.getOggettoTemplate(), item.getTestoTemplate(), 
							destinatariRed, item.getCasella(), mittenteMail, oggetto, conf.getIdUtenteResponsabile(), conf.getIdRuoloResponsabile(), 
							conf.getIdNodoResponsabile(), aoo, fceh, connection);
					
					if (esitoDocUscita.isEsitoOk()) {
					
						idDocumentoUscita = esitoDocUscita.getDocumentTitle();
						wobNumberUscita = esitoDocUscita.getWobNumber();
						
						item.setIdDocumentoUscita(idDocumentoUscita);
						item.setWobNumberUscita(wobNumberUscita);
						item.setStato(ProtocollaMailStatiEnum.DOCUMENTO_IN_USCITA_CREATO);
						item.setDescrizioneErrore(null);
						updateItemInCoda(item, connection);
						
					} else {
						final StringBuilder errorMsg = new StringBuilder("");
						for (final SalvaDocumentoErroreEnum e: esitoDocUscita.getErrori()) {
							if (e.getMessaggio() != null) {
								errorMsg.append(e.getMessaggio());
							} else {
								errorMsg.append(e.toString());
							}
						}
						throw new RedException(errorMsg.toString());					
					}
					
					esitoStep = true;
				} catch (final Exception e) {
					LOGGER.error("Errore in fase di creazione del documento in uscita per l'item " + item.getIdCoda(), e);
					item.setStato(ProtocollaMailStatiEnum.ERRORE_IN_FASE_DI_CREAZIONE_DOCUMENTO_IN_USCITA);
					item.setDescrizioneErrore(e.getMessage());
					updateItemInCoda(item, connection);
					
				}
			}
			
		} catch (final Exception e) {
		
			LOGGER.error("Errore generico nell'elaborazione dell'item " + item, e);
			
		} finally {
			
			closeConnection(connection);
			popSubject(fceh);
		
		}
		
	}
	
	private List<DestinatarioRedDTO> getDestinatariRed(final List<DestinatarioEsternoDTO> destinatariEsterni) {
		List<DestinatarioRedDTO> destinatariRed = null;
		
		if (!CollectionUtils.isEmpty(destinatariEsterni)) {
			destinatariRed = new ArrayList<>();
			
			DestinatarioRedDTO destinatarioRed = null;
			for (final DestinatarioEsternoDTO destinatarioEsterno : destinatariEsterni) {
				destinatarioRed = new DestinatarioRedDTO(
						destinatarioEsterno.getIdDestinatario(), 
						MezzoSpedizioneEnum.getMezzoSpedizioneByTipologiaSpedizione(destinatarioEsterno.getTipoSpedizioneDestinatario()), 
						destinatarioEsterno.getModalitaDestinatario());
				
				destinatariRed.add(destinatarioRed);
			}
		}
		
		return destinatariRed;
	}

	private String getOggetto(final Document documentMail) {
		String oggetto = "oggetto della mail mancante";
		
		final String oggettoMail = (String) TrasformerCE.getMetadato(documentMail, PropertiesNameEnum.OGGETTO_EMAIL_METAKEY);
		if (oggettoMail != null) {
			oggetto = oggettoMail;
			if (oggetto.length() < 4) {
				oggetto = oggetto + "____";
			}
		}
		
		return oggetto;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IProtocollaMailFacadeSRV#inserisciInCoda(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.enums.MailOperazioniEnum, java.lang.String,
	 *      java.lang.String, java.lang.Long, java.lang.String, java.util.List).
	 */
	@Override
	public void inserisciInCoda(final UtenteDTO utente, final MailOperazioniEnum operazione, final String guidMail, final String casella, final Long idContattoMittente, final String testoTemplate, 
			final List<DestinatarioEsternoDTO> contattiDestinatari) {
		
		Connection connection =  null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			
			final CasellaPostaDTO casellaPostale = casellePostaliSRV.getCasellaPostale(utente, casella);
			
			final ProtocollaMailCodaDTO item = new ProtocollaMailCodaDTO(guidMail, utente.getIdAoo(), operazione, utente.getId(), utente.getIdRuolo(), 
					utente.getIdUfficio(), idContattoMittente, testoTemplate, casellaPostale.isMantieniAllegatiOriginali(), casella, 
					ProtocollaMailStatiEnum.DA_ELABORARE, contattiDestinatari);
			
			protocollaMailDAO.inserisciInCoda(item, connection);
			
			//update stato in uscita
			final IFilenetCEHelper fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final HashMap<String, Object> metadati = new HashMap<>();
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.STATO_MAIL_METAKEY), StatoMailEnum.IN_FASE_DI_PROTOCOLLAZIONE_MANUALE.getStatus());
			fceh.updateMetadati(guidMail, metadati);
			
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di inserimento della mail " + guidMail + " in coda per l'operazione " + operazione, e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}	
	}

	/**
	 * @see it.ibm.red.business.service.facade.IProtocollaMailFacadeSRV#getTestiPredefiniti(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.enums.TestoPredefinitoEnum).
	 */
	@Override
	public List<TestoPredefinitoDTO> getTestiPredefiniti(final UtenteDTO utente, final TestoPredefinitoEnum tipoTesto) {
		Connection connection =  null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return testoPredefinitoDAO.getTestiByTipoAndAOO(tipoTesto.getId(), utente.getIdAoo(), connection);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dei testi predefiniti", e);
			rollbackConnection(connection);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IProtocollaMailFacadeSRV#existItemsToProcess(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public boolean existItemsToProcess(final String guidMail, final UtenteDTO utente) {
		Connection connection =  null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return protocollaMailDAO.existItemsToProcess(guidMail, utente.getIdAoo(), connection);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dei testi predefiniti", e);
			rollbackConnection(connection);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}

}