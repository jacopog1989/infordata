/**
 * @author CPIERASC
 *
 *	In questo package verranno riportati i DTO utilizzati per mappare le informazioni recuperate dal database interno di Filenet, 
 *	per ricostruire lo storico degli eventi che hanno interessato uno specifico documento.
 *
 */
package it.ibm.red.business.dto.filenet;
