package it.ibm.red.business.enums;

/**
 * @author Simone Lacarpia
 *
 * Mappa la tabella TIPOCATEGORIA
 */
public enum TipoCategoriaEnum {

	/**
	 * Valore.
	 */
	ENTRATA(1, "ENTRATA"),

	/**
	 * Valore.
	 */
	USCITA(2, "USCITA"),

	/**
	 * Valore.
	 */
	ENTRATA_ED_USCITA(3, "ENTRATA ED USCITA"),

	/**
	 * Valore.
	 */
	INTERNO(4, "INTERNO"),

	/**
	 * Valore.
	 */
	ENTRATA_LIGHT(5, "ENTRATA_LIGHT"),

	/**
	 * Valore.
	 */
	USCITA_LIGHT(6, "USCITA_LIGHT");


	/**
	 * Tipo categoria.
	 */
	private Integer idTipoCategoria;

	/**
	 * Descrizione.
	 */
	private String descrizione;
	
	/**
	 * Costruttore di default.
	 * @param idTipoCategoria
	 * @param descrizione
	 */
	TipoCategoriaEnum(final Integer idTipoCategoria, final String descrizione) {
		this.idTipoCategoria = idTipoCategoria;
		this.descrizione = descrizione;
	}

	/**
	 * Restituisce il valore dell'enum corrispondente all'id in input.
	 * @param inIdTipoCategoria
	 * @return tipoCategoriaEnum
	 */
	public static TipoCategoriaEnum getById(final Integer inIdTipoCategoria) {
		TipoCategoriaEnum out = null;

		if (inIdTipoCategoria == null) { 
			return out;
		}
		for (TipoCategoriaEnum t : TipoCategoriaEnum.values()) {
			if (t.getIdTipoCategoria().intValue() == inIdTipoCategoria.intValue()) {
				out = t;
				break;
			}
		}
		return out;
	}

	/**
	 * Restituisce il valore dell'enum corrispondente alla descrizione in input.
	 * @param descrizione
	 * @return tipoCategoriaEnum
	 */
	public static TipoCategoriaEnum getByDescrizione(final String descrizione) {
		TipoCategoriaEnum out = null;

		for (TipoCategoriaEnum t : TipoCategoriaEnum.values()) {
			if (t.getDescrizione().equalsIgnoreCase(descrizione)) {
				out = t;
				break;
			}
		}
		return out;
	}

	/**
	 * @return idTipoCategoria
	 */
	public Integer getIdTipoCategoria() {
		return idTipoCategoria;
	}

	/**
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}
}
