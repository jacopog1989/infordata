package it.ibm.red.business.service.concrete;

import java.sql.Connection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IVerificaFirmaDAO;
import it.ibm.red.business.enums.StatoVerificaFirmaEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.IVerificaFirmaSRV;

/**
 * Service che gestisce il processo di verifica firma.
 */
@Service
public class VerificaFirmaSRV extends AbstractService implements IVerificaFirmaSRV {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = 1191732777548149077L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(VerificaFirmaSRV.class.getName());

	/**
	 * DAO.
	 */
	@Autowired
	IVerificaFirmaDAO verificaFirmaDAO;

	/**
	 * @see it.ibm.red.business.service.IVerificaFirmaSRV#insertDopoCreazioneSuFilenet(java.lang.Long,
	 *      java.lang.String, java.lang.Integer, java.lang.String, java.lang.String,
	 *      java.lang.Integer).
	 */
	@Override
	public boolean insertDopoCreazioneSuFilenet(final Long idAoo, final String documentTitle, final Integer stato, final String objectStore, final String classeDocumentale,
			final Integer versDocumento) {
		Connection conn = null;
		boolean output = false;
		try {
			conn = setupConnection(getDataSource().getConnection(), true);
			output = verificaFirmaDAO.insertDopoCreazioneSuFilenet(idAoo, documentTitle, stato, objectStore, classeDocumentale, versDocumento, conn);
			commitConnection(conn);
		} catch (final Exception ex) {
			rollbackConnection(conn);
			insertLogError(idAoo, StatoVerificaFirmaEnum.IN_ERRORE.getStatoVerificaFirma(), Integer.parseInt(documentTitle), versDocumento, ex.getMessage());
			LOGGER.error("Errore durante l'inserimento in tabella da parte di filenet proxy : "+ex);
			throw new RedException("Errore durante l'inserimento in tabella da parte di filenet proxy : "+ex);
		} finally {
			closeConnection(conn);
		}
		return output;
	}
	
	private boolean insertLogError(final Long idAoo, final Integer stato, final Integer docTitle, final Integer versDocumento, final String eccezione) {
		Connection connError = null;
		final boolean output = false;
		try {
			connError = setupConnection(getDataSource().getConnection(), true);
			verificaFirmaDAO.insertLogError(idAoo, stato, docTitle, eccezione, versDocumento, connError);
			commitConnection(connError);
		} catch (final Exception ex) {
			rollbackConnection(connError);
			LOGGER.error("Errore durante l'inserimento in tabella del logger Error : " + ex);
			throw new RedException("Errore durante l'inserimento in tabella del logger Error : " + ex);
		} finally {
			closeConnection(connError);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.IVerificaFirmaSRV#deleteSingoloElemento(java.lang.Long,
	 *      java.lang.String, java.lang.String, java.lang.String,
	 *      java.lang.Integer).
	 */
	@Override
	public boolean deleteSingoloElemento(final Long idAoo,final String documentTitle,final String objectStore,
			final String classeDocumentale,final Integer versDocumento) {
		Connection conn = null;
		boolean output = false;
		try { 
			conn = setupConnection(getDataSource().getConnection(), true); 
 			output = verificaFirmaDAO.deleteSingoloElemento(idAoo,documentTitle,objectStore,classeDocumentale,versDocumento,conn);
 			commitConnection(conn);
		} catch (Exception ex) {
			rollbackConnection(conn);
			LOGGER.error("Errore durante la delete in tabella da parte di filenet proxy : "+ex);
			throw new RedException("Errore durante la delete in tabella da parte di filenet proxy : "+ex);
		} finally {
			closeConnection(conn);
		}
		return output;
	}
}