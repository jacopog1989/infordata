package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.ModificaIterCEDTO;
import it.ibm.red.business.dto.ModificaIterPEDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.service.facade.IModificaProcedimentoFacadeSRV;
	
/**
 * Interfaccia modifica procedimento.
 * 
 * @author CRISTIANOPierascenzi
 *
 */
public interface IModificaProcedimentoSRV extends IModificaProcedimentoFacadeSRV {

	/**
	 * @param utente
	 * @param modIterCEDTO
	 * @param modIterPEDTO
	 * @param flagUrgente
	 * @param tipoAssegnazione
	 * @param idUtenteDestinatarioNew
	 * @param idNodoDestinatarioNew
	 * @param doc
	 * @param fceh
	 * @param connection
	 * @return
	 */
	HashMap<String, Object> getMetadatiModificaIterManuale(UtenteDTO utente, ModificaIterCEDTO modIterCEDTO, ModificaIterPEDTO modIterPEDTO, String flagUrgente, 
			TipoAssegnazioneEnum tipoAssegnazione, Long idUtenteDestinatarioNew, Long idNodoDestinatarioNew, Document doc, IFilenetCEHelper fceh, Connection connection);
	
	/**
	 * @param utente
	 * @param modIterCEDTO
	 * @param modIterPEDTO
	 * @param metadati
	 * @param flagUrgente
	 * @param fceh
	 * @param fpeh
	 * @param connection
	 */
	void avviaNuovoIterManuale(UtenteDTO utente, ModificaIterCEDTO modIterCEDTO, ModificaIterPEDTO modIterPEDTO, Map<String, Object> metadati,  
			String flagUrgente, IFilenetCEHelper fceh, FilenetPEHelper fpeh, Connection connection);

}
