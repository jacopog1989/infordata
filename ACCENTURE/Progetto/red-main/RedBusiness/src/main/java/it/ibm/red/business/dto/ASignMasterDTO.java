package it.ibm.red.business.dto;

import it.ibm.red.business.service.asign.step.StatusEnum;

/**
 * DTO per la gestione dei dettagli del master dei documenti associati ad item di firma asincrona ancora in lavorazione.
 */
public class ASignMasterDTO extends AbstractDTO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -9104930230206705584L;

	/**
	 * Stato corrente del documento.
	 */
	private StatusEnum status;
	
	/**
	 * Informazioni sullo stato corrente del documento.
	 */
	private String info;
	
	/**
	 * Restituisce lo stato corrente del documento.
	 * @return stato corrente
	 */
	public StatusEnum getStatus() {
		return status;
	}
	
	/**
	 * Imposta lo stato attuale del documento.
	 * @param status
	 */
	public void setStatus(StatusEnum status) {
		this.status = status;
	}
	
	/**
	 * Restituisce le informazioni sullo stato corrente del documento.
	 * @return informazioni su stato corrente
	 */
	public String getInfo() {
		return info;
	}
	
	/**
	 * Imposta le informazioni sullo stato corrente del documento.
	 * @param info
	 */
	public void setInfo(String info) {
		this.info = info;
	}

}
