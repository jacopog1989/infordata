package it.ibm.red.business.enums;

/**
 * Enum che definisce tutti gli stati di lavorazione.
 */
public enum StatoLavorazioneEnum {
	
	/**
	 * Valore.
	 */
	LAVORATE(1L, "LAVORATE"),
	
	/**
	 * Valore.
	 */
	IN_ACQUISIZIONE(2L, "IN ACQUISIZIONE"),
	
	/**
	 * Valore.
	 */
	ASSEGNATE(4L, "ASSEGNATE"),
	
	/**
	 * Valore.
	 */
	IN_SOSPESO(8L, "IN SOSPESO"),
	
	/**
	 * Valore.
	 */
	PRECENSITI(32L, "PRECENSITI"),
	
	/**
	 * Valore.
	 */
	ATTI(64L, "ATTI"),
	
	/**
	 * Valore.
	 */
	RISPOSTA(128L, "RISPOSTA"),
	
	/**
	 * Valore.
	 */
	RICHIESTE_CHIUSE(256L, "RICHIESTE CHIUSE"),
	
	/**
	 * Valore.
	 */
	OSSERVAZIONE(512L, "OSSERVAZIONE"),
	
	/**
	 * Valore.
	 */
	MOZIONE(1024L, "MOZIONE"),
	
	/**
	 * Valore.
	 */
	LAVORATE_TEMP(2048L, "LAVORATE_TEMP"),
	 
	/**
	 * Valore.
	 */
	RICHIAMA_DOCUMENTI_STATO(4096L, "RICHIAMA_DOCUMENTI");
	
	
	/**
	 * Identificativo.
	 */
	private Long id; 

	/**
	 * Descrizione.
	 */
	private String descrizione;
	
	/**
	 * Costruttore.
	 * @param inId
	 * @param inDescrizione
	 */
	StatoLavorazioneEnum(final Long inId, final String inDescrizione) {
		id = inId; 
		descrizione = inDescrizione;
	}

	/**
	 * Restituisce l'id dell'enum.
	 * @return id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Restituisce la descrizione dell'enum.
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Restituisce l'enum associata all'id in ingresso.
	 * @param idStatoLav
	 * @return enum associata all'id
	 */
	public static StatoLavorazioneEnum get(final Long idStatoLav) {
		StatoLavorazioneEnum output = null;
		for (final StatoLavorazioneEnum sle:StatoLavorazioneEnum.values()) {
			if (sle.getId().equals(idStatoLav)) {
				output = sle;
				break;
			}
		}
		return output;
	}

	/**
	 * Restituisce true se l'enum in ingresso: sle, è contenuta nella lista in ingresso: sles.
	 * @param sle
	 * @param sles
	 * @return true se l'enum in ingresso: sle, è contenuta nella lista in ingresso: sles, false altrimenti
	 */
	public static boolean isOneOf(final StatoLavorazioneEnum sle, final StatoLavorazioneEnum... sles) {
		Boolean output = false;
		for (final StatoLavorazioneEnum s:sles) {
			if (sle.equals(s)) {
				output = true;
			}
		}
		return output;
	}
}
