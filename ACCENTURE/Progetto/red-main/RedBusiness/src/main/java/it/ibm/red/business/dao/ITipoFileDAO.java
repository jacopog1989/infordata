package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.HashMap;

import it.ibm.red.business.persistence.model.TipoFile;

/**
 * 
 * @author adilegge
 *
 *	Interfaccia dao per la gestione dei tipi file.
 */
public interface ITipoFileDAO extends Serializable {
	
	/**
	 * Recupera tutti i tipi file.
	 * 
	 * @param connection	connessione al db
	 * @return				
	 */
	Collection<TipoFile> getAll(Connection connection);
	
	/**
	 * Recupera il tipo file a partire dall'estensione dei file in input.
	 * 
	 * @param ext			estensione del file
	 * @param connection	connessione al db
	 * @return				tipo file
	 */
	TipoFile getByExtension(String ext, Connection connection);

	/**
	 * Recupera i tipi di file per i quali è consentita la Copia Conforme.
	 * 
	 * @param connection	connessione al db
	 * @return				collection di tipo file
	 */
	Collection<TipoFile> getTipiFileCopiaConforme(Connection con);
	
	/**
	 * @param con
	 * @return
	 */
	Collection<TipoFile> getTipiFileDocUscita(Connection con);
	
	/**
	 * @param con
	 * @return
	 */
	Collection<TipoFile> getTipiFileEventoAllegato(Connection con);
	
	/**
	 * @param con
	 * @param filename
	 * @return
	 */
	String normalizzaNomeFile(Connection con, String filename);

	/**
	 * @param con
	 * @param content
	 * @param mimeType
	 * @param filename
	 * @return
	 */
	Boolean analizzaFile(Connection con, byte[] content, String mimeType, String filename);

	/**
	 * Verifica se l'estensione è convertibile.
	 * @param ext
	 * @param connection
	 * @return true o false
	 */
	boolean isEstensioneConvertibile(String ext, Connection connection);

	/**
	 * Verifica se il mime type è convertibile.
	 * @param mimeType
	 * @param connection
	 * @return true o false
	 */
	boolean isMimeTypeConvertibile(String mimeType, Connection connection);

	/**
	 * Restituisce il content del file.
	 * @param con
	 * @param idAOO
	 * @param idUtente
	 * @param idNodo
	 * @param idRuolo
	 * @param path
	 * @return content come byte array.
	 */
	byte[] getContent(Connection con, Integer idAOO, Integer idUtente, Integer idNodo, Integer idRuolo, String path);

	/**
	 * Inserisce il record mail.
	 * @param con
	 * @param idAOO
	 * @param idUtente
	 * @param idNodo
	 * @param idRuolo
	 * @param guidMail
	 * @param recordtoInsert
	 */
	void insertRecordEmail(Connection con, Integer idAOO, Integer idUtente, Integer idNodo, Integer idRuolo,
			String guidMail, HashMap<String, byte[]> recordtoInsert);

	/**
	 * Elimina i content.
	 * @param con
	 * @param idAOO
	 * @param idUtente
	 * @param idNodo
	 * @param idRuolo
	 * @param guidMail
	 */
	void deleteContents(Connection con, Integer idAOO, Integer idUtente, Integer idNodo, Integer idRuolo,
			String guidMail);
	
}