package it.ibm.red.business.exception;

/**
 * Exception generata dal client Adobe.
 * 
 * @author CPAOLUZI
 *
 */
public class AdobeException extends RuntimeException {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Costruttore.
	 * 
	 * @param e	eccezione
	 */
	public AdobeException(final Exception e) {
		super(e);
	}

	/**
	 * Costruttore.
	 * 
	 * @param msg	messaggio
	 */
	public AdobeException(final String msg) {
		super(msg);
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param msg	messaggio
	 * @param e		eccezione root
	 */
	public AdobeException(final String msg, final Exception e) {
		super(msg, e);
	}
	
}
