/**
 * 
 */
package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * @author APerquoti
 *
 */
public interface INextStepFacadeSRV extends Serializable {
	
	/**
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	EsitoOperazioneDTO presaVisione(UtenteDTO utente, String wobNumber);
	
	/**
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	Collection<EsitoOperazioneDTO> presaVisione(UtenteDTO utente, Collection<String> wobNumber);
	
	/**
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	EsitoOperazioneDTO procedi(UtenteDTO utente, String wobNumber);
	
	/**
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	Collection<EsitoOperazioneDTO> procedi(UtenteDTO utente, Collection<String> wobNumber);
	
	/**
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	EsitoOperazioneDTO prosegui(UtenteDTO utente, String wobNumber);
	
	/**
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	Collection<EsitoOperazioneDTO> prosegui(UtenteDTO utente, Collection<String> wobNumber);
	
	/**
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	EsitoOperazioneDTO inviaIspettore(UtenteDTO utente, String wobNumber);
	
	/**
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	Collection<EsitoOperazioneDTO> inviaIspettore(UtenteDTO utente, Collection<String> wobNumber);
	
	/**
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	EsitoOperazioneDTO stornaAlCorriere(UtenteDTO utente, String wobNumber);
	
	/**
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	Collection<EsitoOperazioneDTO> stornaAlCorriere(UtenteDTO utente, Collection<String> wobNumber);
	
	/**
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	EsitoOperazioneDTO mettiInSospeso(UtenteDTO utente, String wobNumber);
	
	/**
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	Collection<EsitoOperazioneDTO> mettiInSospeso(UtenteDTO utente, Collection<String> wobNumber);
	
	/**
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	EsitoOperazioneDTO inviaUffCoordinamento(UtenteDTO utente, String wobNumber);
	
	/**
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	Collection<EsitoOperazioneDTO> inviaUffCoordinamento(UtenteDTO utente, Collection<String> wobNumber);
	
	/**
	 * 
	 * @param utente
	 * @param wobNumebr
	 * @return
	 */
	EsitoOperazioneDTO inviaUCP(UtenteDTO utente, String wobNumebr);
	
	/**
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	Collection<EsitoOperazioneDTO> inviaUCP(UtenteDTO utente, Collection<String> wobNumber);
	
	/**
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	EsitoOperazioneDTO inviaFirmaDigitale(UtenteDTO utente, String wobNumber);
	
	/**
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	Collection<EsitoOperazioneDTO> inviaFirmaDigitale(UtenteDTO utente, Collection<String> wobNumber);
	
	
	/**
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	EsitoOperazioneDTO verificato(UtenteDTO utente, String wobNumber);

	/**
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	Collection<EsitoOperazioneDTO> verificato(UtenteDTO utente, Collection<String> wobNumber);

	/**
	 * Metodo per la messa agli atti.
	 * @param utente
	 * @param wobNumber
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO mettiAgliAtti(UtenteDTO utente, String wobNumber);
	
	/**
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	EsitoOperazioneDTO inviaDirettore(UtenteDTO utente, String wobNumber);
	
	/**
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	Collection<EsitoOperazioneDTO> inviaDirettore(UtenteDTO utente, Collection<String> wobNumber);
	
	/**
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	EsitoOperazioneDTO spostaNelLibroFirma(UtenteDTO utente, String wobNumber);
	
	/**
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	Collection<EsitoOperazioneDTO> spostaNelLibroFirma(UtenteDTO utente, Collection<String> wobNumber);
	
}