package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IDatiPredefinitiMozioneDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.DatiPredefinitiMozione;

/**
 * Dao per la gestione dei Dati Predefiniti Mozione.
 *
 * @author m.crescentini
 * 
 */
@Repository
public class DatiPredefinitiMozioneDAO extends AbstractDAO implements IDatiPredefinitiMozioneDAO {
	
	/**
	 * SERIAL VERSION UID.
	 */
	private static final long serialVersionUID = -7104950624259449041L;
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DatiPredefinitiMozioneDAO.class.getName());

	/**
	 * @see it.ibm.red.business.dao.IDatiPredefinitiMozioneDAO#getDatiPredefinitiMozione(int,
	 *      long, java.lang.Long, java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public DatiPredefinitiMozione getDatiPredefinitiMozione(final int idTipologiaDocumento, final long idTipoProcedimento, final Long idUfficio,
			final Integer idIterApprovativo, 
			final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		DatiPredefinitiMozione datiPredefinitiMozione = null;
		
		try {
			ps = connection.prepareStatement(
					"SELECT idDatiPredefinitiMozione, idTipologiaDocumento, idTipologiaProcedimento, idNodo, idIterApprovativo, email, emailPredefinita, firma_dig_rgs"
					+ " FROM dati_predefiniti_mozione WHERE idTipologiaDocumento = ? AND idTipologiaProcedimento = ? AND idNodo = ? AND idIterApprovativo = ?");
			ps.setInt(1, idTipologiaDocumento);
			ps.setLong(2, idTipoProcedimento);
			ps.setLong(3, idUfficio);
			ps.setInt(4, idIterApprovativo);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				 datiPredefinitiMozione = new DatiPredefinitiMozione();
				 populateVO(datiPredefinitiMozione, rs);
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero dei dati predefiniti mozione."
					+ " Tipologia Documento: " + idTipologiaDocumento + ", Tipo Procedimento: " + idTipoProcedimento + ", ID ufficio (nodo): " + idUfficio
					+ ", ID Iter Approvativo: " + idIterApprovativo, e);
			throw new RedException("Errore durante il recupero dei dati predefiniti mozione."
					+ " Tipologia Documento: " + idTipologiaDocumento + ", Tipo Procedimento: " + idTipoProcedimento + ", ID ufficio (nodo): " + idUfficio
					+ ", ID Iter Approvativo: " + idIterApprovativo, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return datiPredefinitiMozione;
	}
	
	
	private static void populateVO(final DatiPredefinitiMozione datiPredefinitiMozione, final ResultSet rs) throws SQLException {
		 datiPredefinitiMozione.setIdDatiPredefinitiMozione(rs.getInt("IdDatiPredefinitiMozione"));
		 datiPredefinitiMozione.setEmail(rs.getString("email"));
		 datiPredefinitiMozione.setEmailPredefinita(rs.getInt("emailPredefinita") == 1);
		 datiPredefinitiMozione.setFirmaDigRGS(rs.getInt("firma_dig_rgs"));
		 datiPredefinitiMozione.setIdUfficio(rs.getLong("idnodo"));
		 datiPredefinitiMozione.setIdTipologiaDocumento(rs.getInt("IdTipologiaDocumento"));
		 datiPredefinitiMozione.setIdTipologiaProcedimento(rs.getInt("IdTipologiaProcedimento"));
		 datiPredefinitiMozione.setIdIterApprovativo(rs.getInt("IdIterApprovativo"));
	}
	
	/**
	 * @see it.ibm.red.business.dao.IDatiPredefinitiMozioneDAO#getFirmaDigitaleRGS(int,
	 *      long, java.sql.Connection).
	 */
	@Override
	public DatiPredefinitiMozione getFirmaDigitaleRGS(final int idTipologiaDocumento, final long idTipoProcedimento, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		DatiPredefinitiMozione datiPredefinitiMozione = null;
		
		try {
			ps = connection.prepareStatement(
					"SELECT idDatiPredefinitiMozione, " 
					+ "       idTipologiaDocumento, " 
					+ "       idTipologiaProcedimento, " 
					+ "       idNodo, " 
					+ "       idIterApprovativo, " 
					+ "       email, " 
					+ "       EmailPredefinita, " 
					+ "       firma_Dig_Rgs " 
					+ "  FROM dati_predefiniti_mozione " 
					+ " WHERE idTipologiaProcedimento = ? " 
					+ "   AND idTipologiaDocumento = ? " 
					+ "   AND emailpredefinita IS NULL");
			ps.setLong(1, idTipoProcedimento);
			ps.setInt(2, idTipologiaDocumento);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				 datiPredefinitiMozione = new DatiPredefinitiMozione();
				 populateVO(datiPredefinitiMozione, rs);
			}
		} catch (final SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return datiPredefinitiMozione;
	}

	
	
}