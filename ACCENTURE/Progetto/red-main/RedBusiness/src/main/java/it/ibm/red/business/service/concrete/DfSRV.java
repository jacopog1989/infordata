package it.ibm.red.business.service.concrete;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.entrate.protocollo.ejb.DocumentoBean;
import it.ibm.red.business.dao.ITipologiaDocumentoDAO;
import it.ibm.red.business.df.BusinessDelegate;
import it.ibm.red.business.dto.AllegatoNsdDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DetailFascicoloPregressoDTO;
import it.ibm.red.business.dto.DocumentiFascicoloNsdDTO;
import it.ibm.red.business.dto.EsitoVerificaProtocolloNsdDTO;
import it.ibm.red.business.dto.FascicoliPregressiDTO;
import it.ibm.red.business.dto.MappingOrgNsdDTO;
import it.ibm.red.business.dto.ParamsRicercaFascicoliPregressiDfDTO;
import it.ibm.red.business.dto.ParamsRicercaProtocolliPregressiDfDTO;
import it.ibm.red.business.dto.ProtocolliPregressiDTO;
import it.ibm.red.business.dto.RiferimentoStoricoNsdDTO;
import it.ibm.red.business.dto.TipologiaRegistroNsdDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.provider.WebServiceClientProvider;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.IDfSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.StringUtils;
import it.sogei.gestionedocumentale.documentale.ejbprotocollo.AllegatoBean;
import it.sogei.gestionedocumentale.documentale.ejbprotocollo.RisultatiRicercaBean;
import it.sogei.servizi.fascicoli.ws.Documento;
import it.sogei.servizi.fascicoli.ws.Fascicolo;
import it.sogei.servizi.fascicoli.ws.Folder;

/**
 * 
 * @author VINGENITO
 * 
 *
 */
@SuppressWarnings({ "static-access" })
@Service
public class DfSRV extends AbstractService implements IDfSRV {

	private static final long serialVersionUID = 2346686944474724318L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DfSRV.class.getName());

	/**
	 * Service.
	 */
	@Autowired
	private IAooSRV aooSRV;

	/**
	 * Dao.
	 */
	@Autowired
	private ITipologiaDocumentoDAO tipologiaDocumentoDAO;

	/**
	 * Properties.
	 */
	private PropertiesProvider pp;

	/**
	 * @see it.ibm.red.business.service.facade.IDfFacadeSRV#ricercaFascicoliPregressi(it.ibm.red.business.dto.ParamsRicercaFascicoliPregressiDfDTO,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.Integer,
	 *      java.lang.Integer).
	 */
	@Override
	public List<FascicoliPregressiDTO> ricercaFascicoliPregressi(final ParamsRicercaFascicoliPregressiDfDTO paramsRicercaFascicoliPregressi, final UtenteDTO utente,
			final Integer numPagina, final Integer recordPerPagina) {
		final List<FascicoliPregressiDTO> fascicoliTrovatiPregressiDTO = new ArrayList<>();
		try {
			LOGGER.info("ricercaFascicoliPregressi -> START");

			final MappingOrgNsdDTO mappingNsd = aooSRV.getMappingOrgNsd(utente.getIdUfficio(), utente.getId(), utente.getIdAoo());
			final Set<FascicoliPregressiDTO> protocolliTrovatiSet = eseguiRicercaFascicoli(mappingNsd, paramsRicercaFascicoliPregressi, numPagina, recordPerPagina);

			fascicoliTrovatiPregressiDTO.addAll(protocolliTrovatiSet);
		} catch (final Exception e) {
			LOGGER.error("ricercaFascicoliNsd -> Si è verificato un errore durante la ricerca dei fascicoli" + e);
			throw new RedException(e);
		}
		LOGGER.info("ricercaFascicoliPregressi -> END");
		return fascicoliTrovatiPregressiDTO;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDfFacadeSRV#ricercaFascicoliPregressiCount(it.ibm.red.business.dto.ParamsRicercaFascicoliPregressiDfDTO,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public int ricercaFascicoliPregressiCount(final ParamsRicercaFascicoliPregressiDfDTO paramsFascicoliDTO, final UtenteDTO utente) {
		LOGGER.info("ricercaFascicoliPregressiCount -> START");

		try {
			// per la paginazione
			final String dataAperturaDa = DateUtils.dateToString(paramsFascicoliDTO.getDataAperturaDa(), null);
			final String dataAperturaA = DateUtils.dateToString(paramsFascicoliDTO.getDataAperturaA(), null);
			final String dataChiusuraDa = DateUtils.dateToString(paramsFascicoliDTO.getDataChiusuraDa(), null);
			final String dataChiusuraA = DateUtils.dateToString(paramsFascicoliDTO.getDataChiusuraA(), null);
			final String dataScadenzaDa = DateUtils.dateToString(paramsFascicoliDTO.getDataScadenzaDa(), null);
			final String dataScadenzaA = DateUtils.dateToString(paramsFascicoliDTO.getDataScadenzaA(), null);

			final MappingOrgNsdDTO mappingNsd = aooSRV.getMappingOrgNsd(utente.getIdUfficio(), utente.getId(), utente.getIdAoo());
			
			initBusinessDelegate();
			
			return BusinessDelegate.Fascicolo.ricercaFascicoliCount(/* codFiscale */mappingNsd.getCodFiscaleNsd(), /* nomeEnte */mappingNsd.getEnteNsd(),
					/* idFlusso */"RED", /* codiceAoo */mappingNsd.getCodAooNsd(), /* nome */paramsFascicoliDTO.getNomeFascicolo(),
					/* descrizione */paramsFascicoliDTO.getDescrizioneFascicolo(), /* responsabile */paramsFascicoliDTO.getResponsabileFascicolo(),
					/* stato */paramsFascicoliDTO.getStatoFascicolo(), /* codiceTitolario */paramsFascicoliDTO.getCodiceTitolarioFascicolo(),
					/* numeroFascicolo */paramsFascicoliDTO.getNumeroFascicolo(), /* dataAperturaDal */dataAperturaDa, /* dataAperturaAl */dataAperturaA,
					/* dataChiusuraDal */dataChiusuraDa, /* dataChiusuraAl */dataChiusuraA, /* dataScadenzaDal */dataScadenzaDa, /* dataScadenzaAl */dataScadenzaA,
					/* numProtocollo */paramsFascicoliDTO.getNumeroProtocolloFascicolo(), /* annoProtocollo */paramsFascicoliDTO.getAnnoProtocolloFascicolo(),
					/* tipoRegistro */paramsFascicoliDTO.getIdRegistroFascicolo(), /* uffProprietario */paramsFascicoliDTO.getUfficioProprietarioFascicolo(),
					/* dimensionePagina */null, /* pagina */null);

		} catch (final Exception e) {
			LOGGER.error("ricercaFascicoliPregressiCount -> Si è verificato un errore durante la ricerca count dei fascicoli" + e);
			throw new RedException(e);
		}
	}

	private static Set<FascicoliPregressiDTO> eseguiRicercaFascicoli(final MappingOrgNsdDTO mappingNsd, final ParamsRicercaFascicoliPregressiDfDTO paramsFascicoliDTO,
			final int numPagina, final int recordPerPagina) {

		final Set<FascicoliPregressiDTO> fascicoliTrovatiSet = new HashSet<>();

		try {
			final String dataAperturaDa = DateUtils.dateToString(paramsFascicoliDTO.getDataAperturaDa(), null);
			final String dataAperturaA = DateUtils.dateToString(paramsFascicoliDTO.getDataAperturaA(), null);
			final String dataChiusuraDa = DateUtils.dateToString(paramsFascicoliDTO.getDataChiusuraDa(), null);
			final String dataChiusuraA = DateUtils.dateToString(paramsFascicoliDTO.getDataChiusuraA(), null);
			final String dataScadenzaDa = DateUtils.dateToString(paramsFascicoliDTO.getDataScadenzaDa(), null);
			final String dataScadenzaA = DateUtils.dateToString(paramsFascicoliDTO.getDataScadenzaA(), null);

			initBusinessDelegate();
			
			final List<Fascicolo> rispostaRicercaFascicoli = BusinessDelegate.Fascicolo.ricercaFascicoli(/* codFiscale */mappingNsd.getCodFiscaleNsd(),
					/* nomeEnte */mappingNsd.getEnteNsd(), /* idFlusso */"RED", /* codiceAoo */mappingNsd.getCodAooNsd(), /* nome */paramsFascicoliDTO.getNomeFascicolo(),
					/* descrizione */paramsFascicoliDTO.getDescrizioneFascicolo(), /* responsabile */paramsFascicoliDTO.getResponsabileFascicolo(),
					/* stato */paramsFascicoliDTO.getStatoFascicolo(), /* codiceTitolario */paramsFascicoliDTO.getCodiceTitolarioFascicolo(),
					/* numeroFascicolo */paramsFascicoliDTO.getNumeroFascicolo(), /* dataAperturaDal */dataAperturaDa, /* dataAperturaAl */dataAperturaA,
					/* dataChiusuraDal */dataChiusuraDa, /* dataChiusuraAl */dataChiusuraA, /* dataScadenzaDal */dataScadenzaDa, /* dataScadenzaAl */dataScadenzaA,
					/* numProtocollo */paramsFascicoliDTO.getNumeroProtocolloFascicolo(), /* annoProtocollo */paramsFascicoliDTO.getAnnoProtocolloFascicolo(),
					/* tipoRegistro */paramsFascicoliDTO.getIdRegistroFascicolo(), /* uffProprietario */paramsFascicoliDTO.getUfficioProprietarioFascicolo(),
					/* dimensionePagina */recordPerPagina, /* pagina */numPagina);

			if (rispostaRicercaFascicoli != null) {
				LOGGER.info("Risultati ricerca START");
				for (final Fascicolo fascicolo : rispostaRicercaFascicoli) {
					LOGGER.info("Id Fascicolo:" + fascicolo.getId());
					LOGGER.info("Numero Fascicolo:" + fascicolo.getNumeroFascicolo());

					final FascicoliPregressiDTO fascicoloDTO = new FascicoliPregressiDTO();

					if (!fascicolo.isVisibile()) {
						fascicoloDTO.setIdFascicolo(fascicolo.getId());
						fascicoloDTO.setResponsabileFascicolo(fascicolo.getBusinessOwner());

						// codice titolario e descrizione titolario
						fascicoloDTO.setCodiceTitolarioFascicolo(fascicolo.getCodTitolario());
						fascicoloDTO.setDescrizioneTitolarioFascicolo(fascicolo.getDescrTitolario());

						// codice ufficio proprietario e descrizione ufficio proprietario
						fascicoloDTO.setCodUffProprietarioFascicolo(fascicolo.getCodUffProprietario());
						fascicoloDTO.setDescrizioneUffProprietarioFascicolo(fascicolo.getDescrUffProprietario());

						// descrizione fascicolo
						fascicoloDTO.setDescrizioneFascicolo("FASCICOLO NON VISIBILE");

						// stato fascicolo
						fascicoloDTO.setStatoFascicolo(String.valueOf(fascicolo.getStato()));

						// numero fascicolo
						fascicoloDTO.setNumFascicolo(fascicolo.getNumeroFascicolo());

						fascicoloDTO.setNomeCartella("FASCICOLO NON VISIBILE");
						fascicoloDTO.setVisible(false);
						fascicoliTrovatiSet.add(fascicoloDTO);
					} else {
						// id fascicolo proprietario fascicolo
						fascicoloDTO.setIdFascicolo(fascicolo.getId());
						fascicoloDTO.setResponsabileFascicolo(fascicolo.getBusinessOwner());

						// codice titolario e descrizione titolario
						fascicoloDTO.setCodiceTitolarioFascicolo(fascicolo.getCodTitolario());
						fascicoloDTO.setDescrizioneTitolarioFascicolo(fascicolo.getDescrTitolario());

						// codice ufficio proprietario e descrizione ufficio proprietario
						fascicoloDTO.setCodUffProprietarioFascicolo(fascicolo.getCodUffProprietario());
						fascicoloDTO.setDescrizioneUffProprietarioFascicolo(fascicolo.getDescrUffProprietario());

						// descrizione fascicolo
						fascicoloDTO.setDescrizioneFascicolo(fascicolo.getDescrizione());

						// stato fascicolo
						fascicoloDTO.setStatoFascicolo(String.valueOf(fascicolo.getStato()));

						// numero fascicolo
						fascicoloDTO.setNumFascicolo(fascicolo.getNumeroFascicolo());

						fascicoloDTO.setNomeCartella(fascicolo.getNomeCartella());
						fascicoloDTO.setVisible(true);
						fascicoliTrovatiSet.add(fascicoloDTO);
					}
				}

				LOGGER.info("Risultati ricerca END");
			}
		} catch (final Exception e) {
			LOGGER.error("eseguiRicercaFascicoli -> Si è verificato un errore durante la ricerca  dei fascicoli" + e);
			throw new RedException(e);
		}
		return fascicoliTrovatiSet;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDfFacadeSRV#getFascicoloById(it.ibm.red.business.dto.FascicoliPregressiDTO,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public DetailFascicoloPregressoDTO getFascicoloById(final FascicoliPregressiDTO fascicoloDTO, final UtenteDTO utente) {

		final MappingOrgNsdDTO mappingNsd = aooSRV.getMappingOrgNsd(utente.getIdUfficio(), utente.getId(), utente.getIdAoo());

		DetailFascicoloPregressoDTO dettaglioFascicolo = null;
		try {
			
			initBusinessDelegate();
			
			final Fascicolo fascicolo = BusinessDelegate.Fascicolo.getFascicoloById(mappingNsd.getCodFiscaleNsd(), mappingNsd.getEnteNsd(), "",
					fascicoloDTO.getIdFascicolo());

			if (fascicolo != null) {
				dettaglioFascicolo = new DetailFascicoloPregressoDTO();
				dettaglioFascicolo.setBusinessOwner(fascicolo.getBusinessOwner());
				dettaglioFascicolo.setClasseDocumentale(fascicolo.getClasseDocumentale());
				dettaglioFascicolo.setClassificazione(fascicolo.getClassificazione());
				dettaglioFascicolo.setDescrTitolario(fascicolo.getDescrTitolario());
				dettaglioFascicolo.setCodAoo(fascicolo.getCodAoo());
				dettaglioFascicolo.setCodTitolario(fascicolo.getCodTitolario());
				dettaglioFascicolo.setCodUffDestinatario(fascicolo.getCodUffDestinatario());

				if (fascicolo.getDataApertura() != null) {
					dettaglioFascicolo.setDataApertura(fascicolo.getDataApertura().toGregorianCalendar().getTime());
				}
				if (fascicolo.getDataChiusura() != null) {
					dettaglioFascicolo.setDataChiusura(fascicolo.getDataChiusura().toGregorianCalendar().getTime());
				}
				if (fascicolo.getDataScadenza() != null) {
					dettaglioFascicolo.setDataScadenza(fascicolo.getDataScadenza().toGregorianCalendar().getTime());
				}

				dettaglioFascicolo.setNomeCartella(fascicolo.getNomeCartella());
				dettaglioFascicolo.setDescrAoo(fascicolo.getDescrAoo());
				dettaglioFascicolo.setDescrizione(fascicolo.getDescrizione());
				dettaglioFascicolo.setDescrUffProprietario(fascicolo.getDescrUffProprietario());
				dettaglioFascicolo.setDestinatario(fascicolo.getDestinatario());
				dettaglioFascicolo.setIdFascicolo(fascicolo.getId());
				dettaglioFascicolo.setIdFolder(fascicolo.getIdFolderId());
				dettaglioFascicolo.setIdFolderNsd(fascicolo.getIdFolderNSD());
				dettaglioFascicolo.setFolderSignature(fascicolo.getFolderSignature());
				dettaglioFascicolo.setVisible(fascicolo.isVisibile());
				dettaglioFascicolo.setiChronicleId(fascicolo.getIChronicleId());
				List<DocumentiFascicoloNsdDTO> listDocumentiNelFascicolo = null;
				if (fascicolo.getListaDocumenti() != null) {
					listDocumentiNelFascicolo = new ArrayList<>();
					for (final Documento doc : fascicolo.getListaDocumenti()) {
						final DocumentiFascicoloNsdDTO documentiNelFascicolo = new DocumentiFascicoloNsdDTO();
						documentiNelFascicolo.setClasseDocumentale(doc.getClasseDocumentale());
						documentiNelFascicolo.setDescrizione(doc.getDescrizione());
						documentiNelFascicolo.setiChronicleId(doc.getIChronicleId());
						documentiNelFascicolo.setIdFolderId(doc.getIdFolderId());
						documentiNelFascicolo.setNome(doc.getNome());
						documentiNelFascicolo.setSegnatura(doc.getSegnatura());
						listDocumentiNelFascicolo.add(documentiNelFascicolo);
					}
					dettaglioFascicolo.setListaDocumenti(listDocumentiNelFascicolo);
				}

				if (fascicolo.getListaCartelle() != null) {
					final List<DocumentiFascicoloNsdDTO> listaFascicoliNefFascicolo = new ArrayList<>();
					for (final Folder folder : fascicolo.getListaCartelle()) {
						final DocumentiFascicoloNsdDTO figlio = retrieveRecursiveFolder(folder);
						if (figlio != null) {
							listaFascicoliNefFascicolo.add(figlio);
						}
					}
					dettaglioFascicolo.setListaFascicolo(listaFascicoliNefFascicolo);
				}
				// END RICORSIONE

				dettaglioFascicolo.setMotivoChiusura(fascicolo.getMotivoChiusura());
				dettaglioFascicolo.setNote(fascicolo.getNote());
				dettaglioFascicolo.setNumDocumenti(fascicolo.getNumDocumenti());
				dettaglioFascicolo.setNumeroFascicolo(fascicolo.getNumeroFascicolo());
				dettaglioFascicolo.setOwner(fascicolo.getOwner());
				dettaglioFascicolo.setProprietario(fascicolo.getProprietario());
				dettaglioFascicolo.setResponsabile(fascicolo.getResponsabile());
				dettaglioFascicolo.setStato(fascicolo.getStato());
				dettaglioFascicolo.setTipologia(fascicolo.getTipologia());
			}

		} catch (final Exception e) {
			LOGGER.error("getFascicoloById -> Si è verificato un errore durante la ricerca  by dei fascicoli" + e);
			throw new RedException(e);
		}
		return dettaglioFascicolo;
	}

	private static DocumentiFascicoloNsdDTO retrieveRecursiveFolder(final Folder folder) {
		final DocumentiFascicoloNsdDTO folderDoc = new DocumentiFascicoloNsdDTO();
		if (folder != null) {
			folderDoc.setiChronicleId(folder.getIChronicleId());
			folderDoc.setIdFolderId(folder.getIdFolderId());
			folderDoc.setNomeCartella(folder.getNomeCartella());
			final List<DocumentiFascicoloNsdDTO> listDocInFascicoloDTO = new ArrayList<>();
			if (folder.getListaDocumenti() != null) {

				for (final Documento doc : folder.getListaDocumenti()) {
					final DocumentiFascicoloNsdDTO docInFascicoloDTO = new DocumentiFascicoloNsdDTO();
					docInFascicoloDTO.setClasseDocumentale(doc.getClasseDocumentale());
					docInFascicoloDTO.setDescrizione(doc.getDescrizione());
					docInFascicoloDTO.setiChronicleId(doc.getIChronicleId());
					docInFascicoloDTO.setIdFolderId(doc.getIdFolderId());
					docInFascicoloDTO.setNome(doc.getNome());
					docInFascicoloDTO.setSegnatura(doc.getSegnatura());
					listDocInFascicoloDTO.add(docInFascicoloDTO);
				}
				folderDoc.setListaDocumenti(listDocInFascicoloDTO);
			}
			if (folder.getListaCartelle() != null) {
				final List<DocumentiFascicoloNsdDTO> listaFascicoliNefFascicolo = new ArrayList<>();
				for (final Folder figlioFolder : folder.getListaCartelle()) {
					final DocumentiFascicoloNsdDTO figlio = retrieveRecursiveFolder(figlioFolder);
					if (figlio != null) {
						listaFascicoliNefFascicolo.add(figlio);
					}
				}
				folderDoc.setListaCartelle(listaFascicoliNefFascicolo);
			}
		}
		return folderDoc;

	}
	// END VI - RICORSIONE

	/**
	 * @see it.ibm.red.business.service.facade.IDfFacadeSRV#ricercaProtocolliPregressi(it.ibm.red.business.dto.ParamsRicercaProtocolliPregressiDfDTO,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.String, java.lang.String).
	 */
	@Override
	public List<ProtocolliPregressiDTO> ricercaProtocolliPregressi(final ParamsRicercaProtocolliPregressiDfDTO paramsRicercaProtocolliPregressi, final UtenteDTO utente,
			final String lastRowNum, final String maxNumIteratore) {
		final List<ProtocolliPregressiDTO> protocolliTrovatiPregressiDTO = new ArrayList<>();
		try {
			LOGGER.info("ricercaProtocolliPregressi -> START");

			final MappingOrgNsdDTO mappingNsd = aooSRV.getMappingOrgNsd(utente.getIdUfficio(), utente.getId(), utente.getIdAoo());
			final Set<ProtocolliPregressiDTO> protocolliTrovatiSet = eseguiRicercaProtocolli(paramsRicercaProtocolliPregressi, mappingNsd, lastRowNum, maxNumIteratore);

			protocolliTrovatiPregressiDTO.addAll(protocolliTrovatiSet);
			Collections.sort(protocolliTrovatiPregressiDTO, new Comparator<ProtocolliPregressiDTO>() {

				@Override
				public int compare(final ProtocolliPregressiDTO o1, final ProtocolliPregressiDTO o2) {
					Integer output = null;
					if (o1.getIdProtocollo() > o2.getIdProtocollo()) {
						output = 1;
					} else if (o1.getIdProtocollo().equals(o2.getIdProtocollo())) {
						output = 0;
					} else {
						output = -1;
					}
					return output;
				}

			});
		} catch (final Exception e) {
			LOGGER.error("ricercaProtocolliPregressi -> Si è verificato un errore durante la ricerca dei protocolli" + e);
			throw new RedException(e);
		}
		LOGGER.info("ricercaProtocolliPregressi -> END");
		return protocolliTrovatiPregressiDTO;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDfFacadeSRV#getmaxRowPageProtocolli().
	 */
	@Override
	public Integer getmaxRowPageProtocolli() {
		pp = PropertiesProvider.getIstance();
		return Integer.valueOf(pp.getParameterByKey(PropertiesNameEnum.NSD_MAX_ROW_RICERCA_PROTOCOLLI));

	}

	/**
	 * @see it.ibm.red.business.service.facade.IDfFacadeSRV#getmaxRowPageFascicoli().
	 */
	@Override
	public Integer getmaxRowPageFascicoli() {
		pp = PropertiesProvider.getIstance();
		return Integer.valueOf(pp.getParameterByKey(PropertiesNameEnum.NSD_MAX_ROW_RICERCA_FASCICOLI));

	}

	/**
	 * @see it.ibm.red.business.service.facade.IDfFacadeSRV#ricercaProtocolliPregressiCount(it.ibm.red.business.dto.ParamsRicercaProtocolliPregressiDfDTO,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public int ricercaProtocolliPregressiCount(final ParamsRicercaProtocolliPregressiDfDTO paramsProtocolliDTO, final UtenteDTO utente) {
		LOGGER.info("ricercaProtocolliCountNsd -> START");

		try {

			// per la paginazione

			String annoProtocollazione = "";
			if (paramsProtocolliDTO.getAnnoProtocollo() != null) {
				annoProtocollazione = String.valueOf(paramsProtocolliDTO.getAnnoProtocollo());
			}

			String numProtocolloDa = "";
			if (paramsProtocolliDTO.getNumProtocolloDa() != null) {
				numProtocolloDa = String.valueOf(paramsProtocolliDTO.getNumProtocolloDa());
			}
			String numProtocolloA = "";
			if (paramsProtocolliDTO.getNumProtocolloA() != null) {
				numProtocolloA = String.valueOf(paramsProtocolliDTO.getNumProtocolloA());
			}

			// Gestione date START
			final String dataProtocollazioneDa = DateUtils.dateToString(paramsProtocolliDTO.getDataProtocollazioneDa(), null);
			final String dataProtocollazioneA = DateUtils.dateToString(paramsProtocolliDTO.getDataProtocollazioneA(), null);
			// Gestione date END

			final MappingOrgNsdDTO mappingNsd = aooSRV.getMappingOrgNsd(utente.getIdUfficio(), utente.getId(), utente.getIdAoo());
			
			initBusinessDelegate();
			
			return BusinessDelegate.Protocollo.ricercaProtocolliCount(/* codFiscale */mappingNsd.getCodFiscaleNsd(), /* nomeEnte */mappingNsd.getEnteNsd(),
					/* idFlusso */"RED", /* oggetto */paramsProtocolliDTO.getOggetto(), /* idRegistro */paramsProtocolliDTO.getIdRegistro(),
					/* modalita */paramsProtocolliDTO.getModalita(), /* numProtocolloDa */numProtocolloDa, /* numProtocolloA */numProtocolloA,
					/* annoProtocollazione */annoProtocollazione, /* dataProtocollazioneDa */dataProtocollazioneDa, /* dataProtocollazioneA */dataProtocollazioneA,
					/* tipologiaDocumento */paramsProtocolliDTO.getTipologiaDocumento(), /* mittDest */paramsProtocolliDTO.getMittDestProtocolloNsdDTO().getMittDest(),
					paramsProtocolliDTO.getMittDestProtocolloNsdDTO().getnProtMittente()/* nProtMittente */,
					/* cFpIvaMittDest */paramsProtocolliDTO.getMittDestProtocolloNsdDTO().getcFpIva(), /* idAoo */mappingNsd.getCodAooNsd(), /* lastRowNum */null,
					/* maxNumIteratore */ null);

		} catch (final Exception e) {
			LOGGER.error("ricercaProtocolliPregressiCount -> Si è verificato un errore durante la count :" + e);
			throw new RedException(e);
		}
	}

	private static Set<ProtocolliPregressiDTO> eseguiRicercaProtocolli(final ParamsRicercaProtocolliPregressiDfDTO paramsProtocolliDTO, final MappingOrgNsdDTO mappingNsd,
			final String lastRowNum, final String maxNumIteratore) {

		final Set<ProtocolliPregressiDTO> protocolliTrovatiSet = new HashSet<>();

		try {

			String annoProtocollazione = "";
			if (paramsProtocolliDTO.getAnnoProtocollo() != null) {
				annoProtocollazione = String.valueOf(paramsProtocolliDTO.getAnnoProtocollo());
			}

			String numProtocolloDa = "";
			if (paramsProtocolliDTO.getNumProtocolloDa() != null) {
				numProtocolloDa = String.valueOf(paramsProtocolliDTO.getNumProtocolloDa());
			}
			String numProtocolloA = "";
			if (paramsProtocolliDTO.getNumProtocolloA() != null) {
				numProtocolloA = String.valueOf(paramsProtocolliDTO.getNumProtocolloA());
			}

			// Gestione date START
			final String dataProtocollazioneDa = DateUtils.dateToString(paramsProtocolliDTO.getDataProtocollazioneDa(), null);
			final String dataProtocollazioneA = DateUtils.dateToString(paramsProtocolliDTO.getDataProtocollazioneA(), null);
			// Gestione date END
			
			initBusinessDelegate();

			final List<RisultatiRicercaBean> rispostaRicercaProtocolli = BusinessDelegate.Protocollo.ricercaProtocolli(/* codFiscale */mappingNsd.getCodFiscaleNsd(),
					/* nomeEnte */mappingNsd.getEnteNsd(), /* idFlusso */"RED", /* oggetto */paramsProtocolliDTO.getOggetto(),
					/* idRegistro */paramsProtocolliDTO.getIdRegistro(), /* modalita */paramsProtocolliDTO.getModalita(), /* numProtocolloDa */numProtocolloDa,
					/* numProtocolloA */numProtocolloA, /* annoProtocollazione */annoProtocollazione, /* dataProtocollazioneDa */dataProtocollazioneDa,
					/* dataProtocollazioneA */dataProtocollazioneA, /* tipologiaDocumento */paramsProtocolliDTO.getTipologiaDocumento(),
					/* mittDest */paramsProtocolliDTO.getMittDestProtocolloNsdDTO().getMittDest(),
					paramsProtocolliDTO.getMittDestProtocolloNsdDTO().getnProtMittente()/* nProtMittente */,
					/* cFpIvaMittDest */paramsProtocolliDTO.getMittDestProtocolloNsdDTO().getcFpIva(), /* idAoo */mappingNsd.getCodAooNsd(), /* lastRowNum */lastRowNum,
					/* maxNumIteratore */ maxNumIteratore);

			if (rispostaRicercaProtocolli != null) {
				for (final RisultatiRicercaBean protocollo : rispostaRicercaProtocolli) {
					final ProtocolliPregressiDTO protocolloDTO = new ProtocolliPregressiDTO();
					if ((!StringUtils.isNullOrEmpty(protocollo.getIsVisible()) && !Boolean.parseBoolean(protocollo.getIsVisible()))) {
						protocolloDTO.setDescOggetto("DOCUMENTO RISERVATO");
						protocolloDTO.setIdProtocollo(Integer.parseInt(protocollo.getIdProtocollo()));
						protocolloDTO.setCodiceAoo(protocollo.getCodiceAoo());
						if (protocollo.getDataProtocollo() != null) {
							protocolloDTO.setDataProtocollo(DateUtils.parseDate(protocollo.getDataProtocollo()));
							final Calendar cal = Calendar.getInstance();
							cal.setTime(DateUtils.parseDate(protocollo.getDataProtocollo()));
							protocolloDTO.setAnnoProtocollo("" + cal.get(Calendar.YEAR));
						}
						protocolloDTO.setModalita(protocollo.getFlagModalita());
						protocolliTrovatiSet.add(protocolloDTO);
					} else {
						protocolloDTO.setCodiceAoo(protocollo.getCodiceAoo());
						protocolloDTO.setCodRegistro(protocollo.getCodiceRegistro());
						protocolloDTO.setIdProtocollo(Integer.parseInt(protocollo.getIdProtocollo()));
						protocolloDTO.setDescProtocollatore(protocollo.getDescProtocollatore());
						protocolloDTO.setDescOggetto(protocollo.getDescOggetto());
						protocolloDTO.setDescUfficio(protocollo.getDescUfficio());
						protocolloDTO.setCampoTitolario(protocollo.getCampoTitolario());
						protocolloDTO.setDescFileName(protocollo.getDescFileName());
						if (protocollo.getDataProtocollo() != null) {
							protocolloDTO.setDataProtocollo(DateUtils.parseDate(protocollo.getDataProtocollo()));
							final Calendar cal = Calendar.getInstance();
							cal.setTime(DateUtils.parseDate(protocollo.getDataProtocollo()));
							protocolloDTO.setAnnoProtocollo("" + cal.get(Calendar.YEAR));
						}

						protocolloDTO.setRegistro(protocollo.getRegistro());
						protocolloDTO.setTipoDocumento(protocollo.getDescTipoDocumento());
						protocolloDTO.setOriginalDocId(protocollo.getOriginalDocId());
						protocolloDTO.setNumAllegati(protocollo.getNumAllegati());

						if (protocolloDTO.getNumAllegati() != null) {
							Integer.parseInt(protocolloDTO.getNumAllegati());
						}

						final List<AllegatoNsdDTO> listAllegati = new ArrayList<>();
						for (final AllegatoBean allegatoBean : protocollo.getListaAllegati()) {
							final AllegatoNsdDTO allegato = new AllegatoNsdDTO();
							allegato.setIdDocumentale(allegatoBean.getIdDocumentale());
							allegato.setNomeFile(allegatoBean.getNome());
							listAllegati.add(allegato);
						}
						protocolloDTO.setListaAllegati(listAllegati);

						protocolloDTO.setMittente(protocollo.getMitteneteDestinatario());
						protocolloDTO.setDestinatario(protocollo.getMitteneteDestinatario());
						protocolloDTO.setFlagAnnullato(Boolean.parseBoolean(protocollo.getFlagAnnullato()));
						protocolloDTO.setFlagChiuso(Boolean.parseBoolean(protocollo.getFlagChiuso()));
						protocolloDTO.setModalita(protocollo.getFlagModalita());

						protocolliTrovatiSet.add(protocolloDTO);
					}
				}

			}

		} catch (final Exception e) {
			LOGGER.error("eseguiRicercaProtocolli -> Si è verificato un errore durante la eseguiRicercaProtocolli :" + e);
			throw new RedException(e);
		}
		return protocolliTrovatiSet;
	}

	private static void initBusinessDelegate() {
		WebServiceClientProvider.getIstance().checkInitializationBdDF();
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDfFacadeSRV#getDocumentoConContent(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public AllegatoNsdDTO getDocumentoConContent(final String originalDocId, final UtenteDTO utente) {
		final sun.misc.BASE64Decoder b64decoder = new sun.misc.BASE64Decoder();
		final AllegatoNsdDTO documento = new AllegatoNsdDTO();

		final MappingOrgNsdDTO mappingNsd = aooSRV.getMappingOrgNsd(utente.getIdUfficio(), utente.getId(), utente.getIdAoo());

		try {
			
			initBusinessDelegate();
			
			final DocumentoBean doc = BusinessDelegate.Protocollo.getDocumentoConContent(mappingNsd.getCodFiscaleNsd(), mappingNsd.getEnteNsd(), "", originalDocId);

			documento.setNomeFile(doc.getNomeFile());
			final String strBase64 = doc.getContenuto();
			documento.setContent(b64decoder.decodeBuffer(strBase64));
			documento.setContentType(doc.getContentType());

		} catch (final IOException e) {
			LOGGER.error("getDocumentoConContent -> Si è verificato un errore durante il download del doc :" + e);
			throw new RedException(e);
		}

		return documento;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDfFacadeSRV#verificaRiferimentoStorico(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.RiferimentoStoricoNsdDTO).
	 */
	@Override
	public EsitoVerificaProtocolloNsdDTO verificaRiferimentoStorico(final UtenteDTO utente, final RiferimentoStoricoNsdDTO rifStorico) {
		DetailDocumentRedDTO detailDocument = null;
		final EsitoVerificaProtocolloNsdDTO esito = new EsitoVerificaProtocolloNsdDTO();
		try {
			final MappingOrgNsdDTO mappingNsd = aooSRV.getMappingOrgNsd(utente.getIdUfficio(), utente.getId(), utente.getIdAoo());

			String annoProtocolloString = "";
			if (rifStorico.getAnnoProtocolloNsd() != null) {
				annoProtocolloString = String.valueOf(rifStorico.getAnnoProtocolloNsd());
			}

			String numProtocolloString = "";
			if (rifStorico.getNumeroProtocolloNsd() != null) {
				numProtocolloString = String.valueOf(rifStorico.getNumeroProtocolloNsd());
			}

			final ProtocolliPregressiDTO protocolloById = ricercaProtocolliById(mappingNsd, numProtocolloString, annoProtocolloString);

			if (protocolloById == null) {
				esito.setNote("Il protocollo " + rifStorico.getNumeroProtocolloNsd() + "/" + rifStorico.getAnnoProtocolloNsd() + " non è stato trovato nel sistema");
				return esito;
			}
			detailDocument = new DetailDocumentRedDTO();
			detailDocument.setProtocolloVerificatoNsd(new ProtocolliPregressiDTO());
			detailDocument.setProtocolloVerificatoNsd(protocolloById);

			esito.setEsito(true);
			esito.setDocument(detailDocument);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}
		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDfFacadeSRV#getProtocolloById(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String).
	 */
	@Override
	public ProtocolliPregressiDTO getProtocolloById(final UtenteDTO utente, final String nProtocolloNsd, final String annoProtocolloNsd) {
		try {
			final MappingOrgNsdDTO mappingNsd = aooSRV.getMappingOrgNsd(utente.getIdUfficio(), utente.getId(), utente.getIdAoo());

			return ricercaProtocolliById(mappingNsd, nProtocolloNsd, annoProtocolloNsd);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}
	}

	private static ProtocolliPregressiDTO ricercaProtocolliById(final MappingOrgNsdDTO mappingNsd, final String idProtocollo, final String annoProtocollazione) {
		ProtocolliPregressiDTO protocolloDTO = null;
		try {
			
			initBusinessDelegate();
			
			// Questo servizio ritorna una lista di protocolli , mi aspetto che passando in
			// ingresso id e anno venga
			// ritornato cmq un unico risultato
			final List<RisultatiRicercaBean> ricercaProtById = BusinessDelegate.Protocollo.ricercaProtocolliById(mappingNsd.getCodFiscaleNsd(), mappingNsd.getEnteNsd(),
					"RED", "REGISTRO UFFICIALE", mappingNsd.getCodAooNsd(), idProtocollo, annoProtocollazione, "I");
			if (!ricercaProtById.isEmpty()) {
				final RisultatiRicercaBean ricerca = ricercaProtById.get(0);
				protocolloDTO = new ProtocolliPregressiDTO();
				if ((!StringUtils.isNullOrEmpty(ricerca.getIsVisible()) && !Boolean.parseBoolean(ricerca.getIsVisible()))) {
					protocolloDTO.setDescOggetto("DOCUMENTO RISERVATO");
					protocolloDTO.setIdProtocollo(Integer.parseInt(ricerca.getIdProtocollo()));
					protocolloDTO.setCodiceAoo(ricerca.getCodiceAoo());
					protocolloDTO.setModalita(ricerca.getFlagModalita());
					protocolloDTO.setAnnoProtocollo(annoProtocollazione);
					if (ricerca.getDataProtocollo() != null) {
						protocolloDTO.setDataProtocollo(DateUtils.parseDate(ricerca.getDataProtocollo()));
						final Calendar cal = Calendar.getInstance();
						cal.setTime(DateUtils.parseDate(ricerca.getDataProtocollo()));
						protocolloDTO.setAnnoProtocollo("" + cal.get(Calendar.YEAR));
					}
				} else {
					protocolloDTO.setCodiceAoo(ricerca.getCodiceAoo());
					protocolloDTO.setCodRegistro(ricerca.getCodiceRegistro());
					protocolloDTO.setIdProtocollo(Integer.parseInt(ricerca.getIdProtocollo()));
					protocolloDTO.setDescProtocollatore(ricerca.getDescProtocollatore());
					protocolloDTO.setDescOggetto(ricerca.getDescOggetto());
					protocolloDTO.setDescUfficio(ricerca.getDescUfficio());
					protocolloDTO.setCampoTitolario(ricerca.getCampoTitolario());
					protocolloDTO.setAnnoProtocollo(annoProtocollazione);
					protocolloDTO.setRegistro(ricerca.getRegistro());
					protocolloDTO.setTipoDocumento(ricerca.getDescTipoDocumento());
					protocolloDTO.setOriginalDocId(ricerca.getOriginalDocId());
					protocolloDTO.setDescFileName(ricerca.getDescFileName());
					// ALLEGATI
					final List<AllegatoNsdDTO> listAllegati = new ArrayList<>();
					for (final AllegatoBean allegatoBean : ricerca.getListaAllegati()) {
						final AllegatoNsdDTO allegato = new AllegatoNsdDTO();
						allegato.setIdDocumentale(allegatoBean.getIdDocumentale());
						allegato.setNomeFile(allegatoBean.getNome());
						listAllegati.add(allegato);
					}
					protocolloDTO.setListaAllegati(listAllegati);
					protocolloDTO.setMittente(ricerca.getMittenete());
					protocolloDTO.setDestinatario(ricerca.getDestinatario());
					if (ricerca.getDataProtocollo() != null) {
						protocolloDTO.setDataProtocollo(DateUtils.parseDate(ricerca.getDataProtocollo()));
						final Calendar cal = Calendar.getInstance();
						cal.setTime(DateUtils.parseDate(ricerca.getDataProtocollo()));
						protocolloDTO.setAnnoProtocollo("" + cal.get(Calendar.YEAR));
					}
				}
			}
		} catch (final Exception ex) {
			LOGGER.error("ricerca protocolli by id -> Si è verificato un errore durante la ricerca" + ex);
			throw new RedException(ex);
		}
		return protocolloDTO;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDfFacadeSRV#getTipologiaRegistroNsd(java.lang.Long).
	 */
	@Override
	public List<TipologiaRegistroNsdDTO> getTipologiaRegistroNsd(final Long idAOO) {
		List<TipologiaRegistroNsdDTO> tipologiaRegistroNsdList = new ArrayList<>();
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			tipologiaRegistroNsdList = tipologiaDocumentoDAO.getTipologiaRegistroNsd(conn, idAOO);
		} catch (final Exception ex) {
			LOGGER.error("Errore durante il recupero del tipo registro nsd :" + ex);
			throw new RedException("Errore durante il recupero del tipo registro nsd :" + ex);
		} finally {
			closeConnection(conn);
		}
		return tipologiaRegistroNsdList;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDfFacadeSRV#getTipologiaDocumentoNsd(java.lang.Long,
	 *      java.lang.Long).
	 */
	@Override
	public List<TipologiaRegistroNsdDTO> getTipologiaDocumentoNsd(final Long idufficio, final Long idUfficioPadre) {
		List<TipologiaRegistroNsdDTO> tipologiaDocumentoNsdList = new ArrayList<>();
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			tipologiaDocumentoNsdList = tipologiaDocumentoDAO.getTipologiaDocumentoNsd(idufficio, idUfficioPadre, conn);
		} catch (final Exception ex) {
			LOGGER.error("Errore durante il recupero del tipo documento nsd :" + ex);
			throw new RedException("Errore durante il recupero del tipo documento nsd :" + ex);
		} finally {
			closeConnection(conn);
		}
		return tipologiaDocumentoNsdList;
	}
}
