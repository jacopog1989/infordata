package it.ibm.red.business.service.concrete;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.xml.AbstractXmlHelper;
import it.ibm.red.business.helper.xml.VerbaleRevisoriIgfXmlHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.service.IProtocollaPECVRIGFSRV;
import it.ibm.red.business.service.facade.IFepaFacadeSRV;

/**
 * Service che gestisce la protocollazione PEC VRIGF.
 */
@Service
@Component
public class ProtocollaPECVRIGFSRV extends ProtocollaPECAbstractSRV implements IProtocollaPECVRIGFSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 5072215933806523575L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ProtocollaPECVRIGFSRV.class.getName());

	/**
	 * Service.
	 */
	@Autowired
	private IFepaFacadeSRV fepaSRV;

	/**
	 * @see it.ibm.red.business.service.concrete.ProtocollaPECAbstractSRV#initXmlHelper(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      com.filenet.api.core.Document).
	 */
	@Override
	public AbstractXmlHelper initXmlHelper(final IFilenetCEHelper fceh, final Document email) {
		final Document xmlDocument = (Document) email.get_ChildDocuments().iterator().next();
		try {
			final AbstractXmlHelper helper = new VerbaleRevisoriIgfXmlHelper();
			helper.setXmlDocument(xmlDocument.accessContentStream(0));
			return helper;
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di parsing dell'allegato della mail " + email.get_Id().toString(), e);
			throw new RedException(e);
		}
	}
	
	/**
	 * @see it.ibm.red.business.service.concrete.ProtocollaPECAbstractSRV#getUfficioDestinatarioFromMail(com.filenet.api.core.Document,
	 *      it.ibm.red.business.helper.xml.AbstractXmlHelper).
	 */
	@Override
	public Integer getUfficioDestinatarioFromMail(final Document email, final AbstractXmlHelper helper) {
		try {
			return ((VerbaleRevisoriIgfXmlHelper) helper).getCodiceUfficioAssegnatario();
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero del codice ufficio assegnatario per la mail " + email.get_Id().toString(), e);
			return null;
		}
	}

	/**
	 * @see it.ibm.red.business.service.IProtocollaPECSRV#extractMailAttachments(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      com.filenet.api.core.Document, it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.DetailDocumentRedDTO,
	 *      it.ibm.red.business.helper.xml.AbstractXmlHelper).
	 */
	@Override
	public void extractMailAttachments(final IFilenetCEHelper fceh, final Document email, final UtenteDTO utente, final DetailDocumentRedDTO documento, final AbstractXmlHelper helper) {
	
		extractMailAttachments(email, documento, ((VerbaleRevisoriIgfXmlHelper) helper).getCasellaPecEnte());
		
	}

	/**
	 * @see it.ibm.red.business.service.concrete.ProtocollaPECAbstractSRV#getIdFascicoloDaAssociare(com.filenet.api.core.Document,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      it.ibm.red.business.helper.xml.AbstractXmlHelper).
	 */
	@Override
	public String getIdFascicoloDaAssociare(final Document mail, final IFilenetCEHelper fceh, final AbstractXmlHelper helper) {
		
		return getIdFascicoloDaAssociare(mail, fceh, ((VerbaleRevisoriIgfXmlHelper) helper).getIdFascicoloFepa());
		
	}
	
	/**
	 * @see it.ibm.red.business.service.concrete.ProtocollaPECAbstractSRV#getMetadatiFascicoloDaAssociare(com.filenet.api.core.Document,
	 *      it.ibm.red.business.helper.xml.AbstractXmlHelper).
	 */
	@Override
	public Map<String, Object> getMetadatiFascicoloDaAssociare(final Document mail, final AbstractXmlHelper helper) {
		
		return getMetadatiFascicoloDaAssociare(mail, ((VerbaleRevisoriIgfXmlHelper) helper).getIdFascicoloFepa());
		
	}
	
	/**
	 * @see it.ibm.red.business.service.concrete.ProtocollaPECAbstractSRV#eseguiAttivitaPostProtocollazione(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      com.filenet.api.core.Document, java.lang.String,
	 *      it.ibm.red.business.persistence.model.Aoo,
	 *      it.ibm.red.business.helper.xml.AbstractXmlHelper).
	 */
	@Override
	public void eseguiAttivitaPostProtocollazione(final IFilenetCEHelper fceh, final Document email, final String guidDocumento, final Aoo aoo, final AbstractXmlHelper helper) {
		
		try {
			
			final Document documento = fceh.getDocumentByGuid(guidDocumento);
		
			final String idFascicoloFepa = ((VerbaleRevisoriIgfXmlHelper) helper).getIdFascicoloFepa();
			final List<String> idDocumenti = ((VerbaleRevisoriIgfXmlHelper) helper).getIdDocumenti();
			final String numeroProtocollo = documento.getProperties().getInteger32Value(getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY)).toString();
			final Date dataProtocollo = documento.getProperties().getDateTimeValue(getPP().getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY));
			fepaSRV.associaProtocolloVerbaleRevisione(idFascicoloFepa, idDocumenti, numeroProtocollo, dataProtocollo);
			
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di associazione protocollo per la mail " + email.get_Id().toString(), e);
			throw new RedException(e);
		}
		
	}
	
}