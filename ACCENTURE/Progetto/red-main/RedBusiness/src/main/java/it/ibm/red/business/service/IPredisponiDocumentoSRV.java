/**
 * 
 */
package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IPredisponiDocumentoFacadeSRV;

/**
 * Interfaccia del servizio di predisposizione documento.
 */
public interface IPredisponiDocumentoSRV extends IPredisponiDocumentoFacadeSRV {

}
