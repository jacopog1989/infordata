package it.ibm.red.business.enums;

/**
 * Enum che definisce tutti i tipi di contesto procedurale dei flussi.
 */
public enum TipoContestoProceduraleEnum {

	/**
	 * Valore.
	 */
	BASIC("INTEROPERABILITA", false, false, false),

	/**
	 * Valore.
	 */
	ORD("FLUSSO_ORDINARIO", true, true, false),
	
	/**
	 * Valore.
	 */
	SILICE("FLUSSO_SILICE", true, true, false),

	/**
	 * Valore.
	 */
	FLUSSO_AUT("FLUSSO_AUT", true, true, true),

	/**
	 * Valore.
	 */
	SICOGE("SICOGE", false, true, false),
	
	/**
	 * Valore.
	 */
	FLUSSO_CG2("FLUSSO_CG2", true, true, true);

	/**
	 * Identificativo.
	 */
	private final String id;
	
	/**
	 * aggiornaMetadatiPostProtAuto.
	 */
	private final boolean aggiornaMetadatiPostProtAuto;
	
	/**
	 * blindaFascicolo.
	 */
	private final boolean blindaFascicolo;
	
	/**
	 * famigliaFlussiAut.
	 */
	private final boolean famigliaFlussiAut;

	/**
	 * Costruttore di default.
	 * @param id
	 */
	TipoContestoProceduraleEnum(final String id, final boolean aggiornaMetadatiPostProtAuto, final boolean blindaFascicolo, final boolean famigliaFlussiAut) {
		this.id = id;
		this.aggiornaMetadatiPostProtAuto = aggiornaMetadatiPostProtAuto;
		this.blindaFascicolo = blindaFascicolo;
		this.famigliaFlussiAut = famigliaFlussiAut;
	}

	/**
	 * @return id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * @return aggiornaMetadatiPostProtAuto
	 */
	public boolean isAggiornaMetadatiPostProtAuto() {
		return aggiornaMetadatiPostProtAuto;
	}
	
	/**
	 * @return blindaFascicolo
	 */
	public boolean isBlindaFascicolo() {
		return blindaFascicolo;
	}
	
	/**
	 * @return famigliaFlussiAut
	 */
	public boolean isFamigliaFlussiAut() {
		return famigliaFlussiAut;
	}

	/**
	 * Restituisce il valore dell'enum corrispettivo all'id in input.
	 * @param id
	 * @return tipoContestoProceduraleEnum
	 */
	public static TipoContestoProceduraleEnum getEnumById(final String id) {
		TipoContestoProceduraleEnum output = null;
		
		for (TipoContestoProceduraleEnum item : TipoContestoProceduraleEnum.values()) {
			if (item.getId().equals(id)) {
				output = item;
				break;
			}
		}
		return output;
	}
	
}