package it.ibm.red.business.enums;

/**
 * @author SLac
 *
 * Serve a distinguere il chiamante della rubrica
 */
public enum RubricaChiamanteEnum {

	/**
	 * DocumentManagerBean
	 */
	DOCUMENTMANAGERBEAN_DESTINATARI,

	/**
	 * DocumentManagerBean
	 */
	DOCUMENTMANAGERBEAN_MITTENTE,

	/**
	 * MailBean
	 */
	MAILBEAN_DESTINATARI;
	
}
