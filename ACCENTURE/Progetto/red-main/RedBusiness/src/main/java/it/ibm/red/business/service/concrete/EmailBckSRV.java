package it.ibm.red.business.service.concrete;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Date;

import javax.mail.Message;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IEmailBckDAO;
import it.ibm.red.business.dto.EmailBckDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IEmailBckSRV;
import it.ibm.red.business.utils.EmailUtils;

/**
 * Service per la gestione delle mail.
 */
@Service
@Component
public class EmailBckSRV extends AbstractService implements IEmailBckSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -2359657111392344730L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(EmailBckSRV.class.getName());
	
	/**
	 * Dao.
	 */
	@Autowired
	private IEmailBckDAO emailBckDAO;
	
	
	@Override
	public final int salvaEmailBck(final Message message, final String messageId, final String indirizzoEmailCasella, final String mittente, 
			final boolean isPec, final boolean isNotifica, final Connection con) {
		int output = 0;
		InputStream is = null;
		try {
			final EmailBckDTO emailBck = new EmailBckDTO();
			emailBck.setDataScarico(new Date());
			emailBck.setDataInvio(message.getSentDate());
			emailBck.setMessageId(message.getHeader("Message-ID")[0]); // In caso di notifica, può essere l'identificativo assegnato dal gestore PEC
			emailBck.setOggetto(message.getSubject());
			Integer maxSize = Integer.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.MAX_SIZE_EMAIL_BCK));
			if(message!=null && message.getInputStream()!=null && (IOUtils.toByteArray(message.getInputStream()).length/1048576 )<= maxSize ) {
				is = EmailUtils.getCompleteEmailBlob(message);
			} else {
				is = message.getInputStream();
			}
			emailBck.setISEmail(is);
			emailBck.setMittente(mittente);
			emailBck.setCasella(indirizzoEmailCasella);
			emailBck.setIsPec(isPec ?  1 : 0);
			emailBck.setIsNotifica(isNotifica ? 1 : 0);
			emailBck.setMessageIdClasseEmail(messageId); // In caso di notifica, è il Message-ID del messaggio originale
			
			output = emailBckDAO.saveEmail(emailBck, con);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'inserimento del backup della mail con Message-ID: " + messageId, e);
			throw new RedException("Si è verificato un errore durante l'inserimento del backup della mail con Message-ID: " + messageId, e);
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (final IOException e) {
					LOGGER.warn(e);
				}
			}
		}
		
		return output;
	}
}