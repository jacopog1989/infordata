package it.ibm.red.business.dto;


import java.sql.Date;

import it.ibm.red.business.enums.ColoreNotaEnum;

/**
 * Classe NotaDocumentoDTO.
 */
public class NotaDocumentoDTO extends NotaDTO {
	
	/** 
	 * The Constant serialVersionUID. 
	 */
	private static final long serialVersionUID = -1596502881774894060L;

	/**
	 * Identificativo documento
	 */
	private Integer idDocumento;
	
	/**
	 * Numero della nota del documento
	 */
	private Integer numeroNota;
	
	/**
	 * Identificato nodo owner
	 */
	private Integer idNodoOwner;
	
	/**
	 * Identificativo utente owner
	 */
	private Integer idUtenteOwner;
	
	/**
	 * Data registrazione nota
	 */
	private Date dataRegistrazioneNota;
	
	/**
	 * Identificativo origine nota
	 */
	private Integer idOrigine;
	
	/**
	 * Data ultima modifica nota
	 */
	private Date dataUltimaModificaNota;
	
	/**
	 * Flag che identifica se la nota può essere eliminata
	 */
	private boolean deleted;
	
	/**
	 * Flag che identifica se la nota può essere eliminata
	 */
	private boolean removable;
	
	/**
	 * Costruttore nota documento DTO.
	 */
	public NotaDocumentoDTO() {
		super();
	}

	/**
	 * Costruttore nota documento DTO.
	 *
	 * @param idDocumento the id documento
	 * @param numeroNota the numero nota
	 * @param idNodoOwner the id nodo owner
	 * @param idUtenteOwner the id utente owner
	 * @param dataRegistrazioneNota the data registrazione nota
	 * @param contentNota the content nota
	 * @param idOrigine the id origine
	 * @param colore the colore
	 * @param dataUltimaModificaNota the data ultima modifica nota
	 * @param deleted the deleted
	 */
	public NotaDocumentoDTO(final Integer idDocumento, final Integer numeroNota, final Integer idNodoOwner, final Integer idUtenteOwner,
			final Date dataRegistrazioneNota, final String contentNota, final Integer idOrigine, final ColoreNotaEnum colore,
			final Date dataUltimaModificaNota, final boolean deleted) {
		this(idDocumento, numeroNota, colore, contentNota, idNodoOwner, idUtenteOwner, idOrigine);
		this.dataRegistrazioneNota = dataRegistrazioneNota;
		this.dataUltimaModificaNota = dataUltimaModificaNota;
		this.deleted = deleted;
	}
	
	/**
	 * Costruttore nota documento DTO.
	 *
	 * @param idDocumento the id documento
	 * @param numeroNota the numero nota
	 */
	public NotaDocumentoDTO(final Integer idDocumento, final Integer numeroNota) {
		super();
		this.idDocumento = idDocumento;
		this.numeroNota = numeroNota;
	}
	
	/**
	 * Costruttore nota documento DTO.
	 *
	 * @param idDocumento the id documento
	 * @param numeroNota the numero nota
	 * @param colore the colore
	 * @param contentNota the content nota
	 */
	public NotaDocumentoDTO(final Integer idDocumento, final Integer numeroNota, final ColoreNotaEnum colore, final String contentNota) {
		this(idDocumento, numeroNota);
		setColore(colore);
		setContentNota(contentNota);
	}
	
	/**
	 * Costruttore nota documento DTO.
	 *
	 * @param idDocumento the id documento
	 * @param numeroNota the numero nota
	 * @param colore the colore
	 * @param contentNota the content nota
	 * @param idNodoOwner the id nodo owner
	 * @param idUtenteOwner the id utente owner
	 * @param idOrigine the id origine
	 */
	public NotaDocumentoDTO(final Integer idDocumento, final Integer numeroNota, final ColoreNotaEnum colore, final String contentNota, 
			final Integer idNodoOwner, final Integer idUtenteOwner, final Integer idOrigine) {
		this(idDocumento, numeroNota, colore, contentNota);
		this.idNodoOwner = idNodoOwner;
		this.idUtenteOwner = idUtenteOwner;
		this.idOrigine = idOrigine;
	}
	
	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "NotaDocumentoDTO [idDocumento=" + idDocumento + ", numeroNota=" + numeroNota + "]";
	}

	/**
	 * The Enum OrigineNota.
	 */
	public enum OrigineNota {
		
		/**
		 * Valore.
		 */
		CODA(0, "Coda"), 
		
		/**
		 * Valore.
		 */
		STORICO(1, "Storico"), 

		/**
		 * Valore.
		 */
		MAIL(2, "Coda Mail");
		
		/**
		 * Identificativo origine.
		 */
		private Integer idOrigine;
		
		/**
		 * Descrizione origine.
		 */
		private String descrOrigine;
		
		/**
		 * Costruttore.
		 */
		OrigineNota(final Integer idOrigine, final String descrOrigine) {
			this.idOrigine = idOrigine;
			this.descrOrigine = descrOrigine;
		}
		
		/** 
		 *
		 * @return the id origine
		 */
		public Integer getIdOrigine() {
			return idOrigine;
		}
		
		/** 
		 *
		 * @param idOrigine the new id origine
		 */
		protected void setIdOrigine(final Integer idOrigine) {
			this.idOrigine = idOrigine;
		}
		
		/** 
		 *
		 * @return the descr origine
		 */
		public String getDescrOrigine() {
			return descrOrigine;
		}
		
		/** 
		 *
		 * @param descrOrigine the new descr origine
		 */
		protected void setDescrOrigine(final String descrOrigine) {
			this.descrOrigine = descrOrigine;
		}
		
		/** 
		 *
		 * @param idOrigine the id origine
		 * @return the by id
		 */
		public static OrigineNota getById(final Integer idOrigine) {
			for (final OrigineNota on: OrigineNota.values()) {
				if (on.getIdOrigine().equals(idOrigine)) {
					return on;
				}
			}
			return null;
		}
	}

	/** 
	 *
	 * @return the id documento
	 */
	public Integer getIdDocumento() {
		return idDocumento;
	}

	/** 
	 *
	 * @param idDocumento the new id documento
	 */
	public void setIdDocumento(final Integer idDocumento) {
		this.idDocumento = idDocumento;
	}

	/** 
	 *
	 * @return the numero nota
	 */
	public Integer getNumeroNota() {
		return numeroNota;
	}

	/** 
	 *
	 * @param numeroNota the new numero nota
	 */
	public void setNumeroNota(final Integer numeroNota) {
		this.numeroNota = numeroNota;
	}

	/** 
	 *
	 * @return the id nodo owner
	 */
	public Integer getIdNodoOwner() {
		return idNodoOwner;
	}

	/** 
	 *
	 * @param idNodoOwner the new id nodo owner
	 */
	public void setIdNodoOwner(final Integer idNodoOwner) {
		this.idNodoOwner = idNodoOwner;
	}

	/** 
	 *
	 * @return the id utente owner
	 */
	public Integer getIdUtenteOwner() {
		return idUtenteOwner;
	}

	/** 
	 *
	 * @param idUtenteOwner the new id utente owner
	 */
	public void setIdUtenteOwner(final Integer idUtenteOwner) {
		this.idUtenteOwner = idUtenteOwner;
	}

	/** 
	 *
	 * @return the data registrazione nota
	 */
	public Date getDataRegistrazioneNota() {
		return dataRegistrazioneNota;
	}

	/** 
	 *
	 * @param dataRegistrazioneNota the new data registrazione nota
	 */
	public void setDataRegistrazioneNota(final Date dataRegistrazioneNota) {
		this.dataRegistrazioneNota = dataRegistrazioneNota;
	}

	/** 
	 *
	 * @return the id origine
	 */
	public Integer getIdOrigine() {
		return idOrigine;
	}
	
	/** 
	 *
	 * @param idOrigine the new id origine
	 */
	public void setIdOrigine(final Integer idOrigine) {
		this.idOrigine = idOrigine;
	}

	/** 
	 *
	 * @return the data ultima modifica nota
	 */
	public Date getDataUltimaModificaNota() {
		return dataUltimaModificaNota;
	}

	/** 
	 *
	 * @param dataUltimaModificaNota the new data ultima modifica nota
	 */
	public void setDataUltimaModificaNota(final Date dataUltimaModificaNota) {
		this.dataUltimaModificaNota = dataUltimaModificaNota;
	}

	/** 
	 *
	 * @return the deleted
	 */
	public boolean getDeleted() {
		return deleted;
	}

	/** 
	 *
	 * @param deleted the new deleted
	 */
	public void setDeleted(final boolean deleted) {
		this.deleted = deleted;
	}
	
	/** 
	 *
	 * @return true, if is removable
	 */
	public boolean isRemovable() {
		return removable;
	}

	/** 
	 *
	 * @param removable the new removable
	 */
	public void setRemovable(final boolean removable) {
		this.removable = removable;
	}
}