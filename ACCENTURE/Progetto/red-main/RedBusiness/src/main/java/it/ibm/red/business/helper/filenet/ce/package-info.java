/**
 * @author CPIERASC
 *
 *	In questo package avremo l'helper contenente tutte le query che agiscono sul content engine: da notare che mentre
 *	ogni singolo accesso al database è confinato in un DAO, qui le query sono tutte contenute in un unico helper, dato
 *	l'esiguo numero di classi documentali gestite.
 *
 */
package it.ibm.red.business.helper.filenet.ce;
