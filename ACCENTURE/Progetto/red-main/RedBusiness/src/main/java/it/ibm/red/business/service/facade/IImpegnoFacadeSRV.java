package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.ImpegnoDTO;
import it.ibm.red.business.dto.MasterDocumentoDTO;
import it.ibm.red.business.enums.TipoAzioneEnum;

/**
 * The Interface IImpegnoFacadeSRV.
 *
 * @author CPIERASC
 * 
 *         Facade gestione impegni.
 */
public interface IImpegnoFacadeSRV extends Serializable {
	
	/**
	 * Recupero impegni in scadenza.
	 * 
	 * @param fcDTO		credenziali filenet
	 * @param idUtente	identificativo utente
	 * @param idUfficio	identificativo ufficio
	 * @param idRuolo	identificativo ruolo
	 * @param idAoo		identificativo aoo
	 * @param filter	filtro tipo azione
	 * @return			impegni filtrati
	 */
	Collection<ImpegnoDTO> getImpegniInScadenza(FilenetCredentialsDTO fcDTO, Long idUtente, Long idUfficio, Long idRuolo, Long idAoo, TipoAzioneEnum filter);

	/**
	 * Gets the impegni in scadenza.
	 *
	 * @param fcDTO
	 *            credenziali filenet
	 * @param idUtente
	 *            identificativo utente
	 * @param idUfficio
	 *            identificativo ufficio
	 * @param idRuolo
	 *            identificativo ruolo
	 * @param idAoo
	 *            identificativo aoo
	 * @param date
	 *            data impegni
	 * @param filter
	 *            filtro tipo azione
	 * @return impegni filtrati
	 */
	Collection<ImpegnoDTO> getImpegniInScadenza(FilenetCredentialsDTO fcDTO, Long idUtente, Long idUfficio, Long idRuolo, Long idAoo, Date date, TipoAzioneEnum filter);

	/**
	 * Recupero documenti in scadenza.
	 * 
	 * @param fcDTO		credenziali filenet
	 * @param idUtente	identificativo utente
	 * @param idUfficio	identificativo ufficio
	 * @param idRuolo	identificativo ruolo
	 * @param idAoo		identificativo aoo
	 * @param date		data impegni
	 * @return			impegni filtrati
	 */
	Collection<MasterDocumentoDTO> getDocumentiInScadenza(FilenetCredentialsDTO fcDTO, Long idUtente, Long idUfficio, Long idRuolo, Long idAoo, Date date);
	
	/**
	 * Recupero impegno per id.
	 * 
	 * @param fcDTO		credenziali filenet
	 * @param idUtente	identificativo utente
	 * @param idUfficio	identificativo ufficio
	 * @param idRuolo	identificativo ruolo
	 * @param id		id calendario
	 * @return			impegno
	 */
	ImpegnoDTO getCalendarioById(FilenetCredentialsDTO fcDTO, Long idUtente, Long idUfficio, Long idRuolo, Integer id);

}
