package it.ibm.red.business.service.flusso.concrete;

import java.sql.Connection;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dto.ProtocolloDTO;
import it.ibm.red.business.dto.ResponsesDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.IconaFlussoEnum;
import it.ibm.red.business.service.concrete.AbstractService;
import it.ibm.red.business.service.flusso.IAzioniFlussoBaseSRV;
import it.ibm.red.business.service.ws.IRedWsDocGenSRV;

/**
 * La classe implementa i comportamenti base di un flusso generico: nella maggior parte dei casi, di fatto, non compie alcuna azione.
 * 
 * @author a.dilegge
 *
 */
@Service
public class AzioniFlussoBaseSRV extends AbstractService implements IAzioniFlussoBaseSRV {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -2431526819921211036L;
	
	/**
	 * Servizio generazione documento.
	 */
	@Autowired
	private IRedWsDocGenSRV redWsDocGenSRV;
	
	
	/**
	 * Implementa le azioni da eseguire a valle della creazione del documento identificato da <code> idDocumento </code> per
	 * il flusso specifico e definito dal <code> codiceFlusso </code>.
	 * 
	 * @param idDocumento
	 *            identificativo documento creato
	 * @param codiceFlusso
	 *            codice flusso di riferimento
	 * @param con
	 *            connessione al database
	 */
	@Override
	public void doAfterCreazioneDocumento(final int idDocumento, final String codiceFlusso, final Connection con) {
		// Se il flusso è presente, si traccia la creazione del documento
		if (StringUtils.isNotBlank(codiceFlusso)) {
			redWsDocGenSRV.insertDocGen(idDocumento, codiceFlusso, con);
		}
	}
	
	
	/* (non-Javadoc)
	 * @see it.ibm.red.business.service.flusso.IAzioniFlussoBaseSRV#doAfterProtocollazioneUscita
	 * (int, it.ibm.red.business.dto.ProtocolloDTO,java.lang.Long, java.sql.Connection)
	 */
	@Override
	public void doAfterProtocollazioneUscita(final int idDocumento, final ProtocolloDTO protocollo, final Long idAoo, final Connection con) {
		// Metodo intenzionalmente vuoto.
	}
	
	/**
	 * Restituisce le response valide tra le response presenti nel parametro:
	 * <code> responses </code> rimuovendone quelle non valide per il flusso di riferimento.
	 * 
	 * @see it.ibm.red.business.service.flusso.IAzioniFlussoBaseSRV#refineResponses
	 *      (it.ibm.red.business.enums.DocumentQueueEnum,
	 *      it.ibm.red.business.dto.ResponsesDTO).
	 * @param coda
	 *            nella quale sono valide le responses
	 * @param responses
	 *            le responses da raffinare
	 * @return responses raffinate
	 */
	@Override
	public ResponsesDTO refineResponses(final DocumentQueueEnum coda, final ResponsesDTO responses) {
		return responses;
	}
	
	/**
	 * Restituisce l'icona da mostrare nelle code documenti per l'identificazione del flusso specifico a cui appartiene il documento.
	 * 
	 * @see it.ibm.red.business.service.flusso.IAzioniFlussoBaseSRV#getIconaFlusso().
	 * @return icona flusso, @see IconaFlussoEnum
	 */
	@Override
	public IconaFlussoEnum getIconaFlusso() {
		return null;
	}
	
}
