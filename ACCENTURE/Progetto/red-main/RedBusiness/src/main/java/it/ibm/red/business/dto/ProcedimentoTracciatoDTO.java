package it.ibm.red.business.dto;



/**
 *Clsse ProcedimentoTracciatoDTO.
 */
public class ProcedimentoTracciatoDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Identificativo documento
	 */
	private int idDocumento;
	
	/**
	 * Numero documento
	 */
	private String numeroDocumento;
	
	/**
	 * Oggetto
	 */
	private String oggetto;
	
	/**
	 * Tipo procedimento
	 */
	private String tipoProcedimento;
	  
	/**
	 * Categoria documento id
	 */
	private Integer categoriaDocumentoId;
	
	/**
	 * Tipo firma
	 */
	private Integer tipoFirma;
	
	/**
	 * Tipologia documento
	 */
	private Integer tipologiaDocumento;
	
	/**
	 * Identificativo ufficio creatore
	 */
	private Integer idUfficioCreatore; 
	
	/** 
	 * @return the idDocumento
	 */
	public int getIdDocumento() {
		return idDocumento;
	}


	/** 
	 * @return the tipologia documento
	 */
	public Integer getTipologiaDocumento() {
		return tipologiaDocumento;
	}


	/** 
	 * @param tipologiaDocumento the new tipologia documento
	 */
	public void setTipologiaDocumento(final Integer tipologiaDocumento) {
		this.tipologiaDocumento = tipologiaDocumento;
	}


	/** 
	 * @return the id ufficio creatore
	 */
	public Integer getIdUfficioCreatore() {
		return idUfficioCreatore;
	}


	/** 
	 * @param idUfficioCreatore the new id ufficio creatore
	 */
	public void setIdUfficioCreatore(final Integer idUfficioCreatore) {
		this.idUfficioCreatore = idUfficioCreatore;
	}


	/** 
	 * @param idDocumento the idDocumento to set
	 */
	public void setIdDocumento(final int idDocumento) {
		this.idDocumento = idDocumento;
	}


	/** 
	 * @return the numeroDocumento
	 */
	public String getNumeroDocumento() {
		return numeroDocumento;
	}


	/** 
	 * @param numeroDocumento the numeroDocumento to set
	 */
	public void setNumeroDocumento(final String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}


	/** 
	 * @return the oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}


	/** 
	 * @param oggetto the oggetto to set
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}


	/** 
	 * @return the tipoProcedimento
	 */
	public String getTipoProcedimento() {
		return tipoProcedimento;
	}


	/** 
	 * @param tipoProcedimento the tipoProcedimento to set
	 */
	public void setTipoProcedimento(final String tipoProcedimento) {
		this.tipoProcedimento = tipoProcedimento;
	}
	

	/**
	 * Equals.
	 *
	 * @param other the other
	 * @return true, if successful
	 */
	@Override
	public boolean equals(final Object other) {
		boolean result = false;
		if ((other instanceof ProcedimentoTracciatoDTO) && (this.getNumeroDocumento() == ((ProcedimentoTracciatoDTO) other).getNumeroDocumento())) {
			result = true;
		}
        return result;
	}
	
	/**
	 * Hash code.
	 *
	 * @return the int
	 */
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	/** 
	 * @return the categoria documento id
	 */
	public Integer getCategoriaDocumentoId() {
		return categoriaDocumentoId;
	}
	
	/** 
	 * @param categoriaDocumentoId the new categoria documento id
	 */
	public void setCategoriaDocumentoId(final Integer categoriaDocumentoId) {
		this.categoriaDocumentoId = categoriaDocumentoId;
	}
	
	/** 
	 * @return the tipo firma
	 */
	public Integer getTipoFirma() {
		return tipoFirma;
	}
	
	/** 
	 * @param tipoFirma the new tipo firma
	 */
	public void setTipoFirma(final Integer tipoFirma) {
		this.tipoFirma = tipoFirma;
	}
}
