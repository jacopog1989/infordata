package it.ibm.red.business.dto;

import java.io.Serializable;

/**
 * DTO select item.
 * 
 * @author CRISTIANOPierascenzi
 *
 */
public class SelectItemDTO extends AbstractDTO {

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = -5883821853675799524L;

	/**
	 * Valore elemento.
	 */
	private Serializable value;

	/**
	 * Descrizione elemento.
	 */
	private String description;
	
	/**
	 * Costruttore vuoto.
	 */
	public SelectItemDTO() {
	}

	/**
	 * Costruttore.
	 * 
	 * @param inValue		valore 
	 * @param inDescription	descrizione
	 */
	public SelectItemDTO(final Serializable inValue, final Object inDescription) {
		this.value = inValue;
		if (inDescription != null) {
			this.description = inDescription.toString();
		}
	}

	/**
	 * Costruttore.
	 * 
	 * @param inValue		valore
	 * @param inDescription	descrizione
	 */
	public SelectItemDTO(final Serializable inValue, final String inDescription) {
		super();
		this.value = inValue;
		this.description = inDescription;
	}

	/**
	 * Getter valore.
	 * 
	 * @return	valore
	 */
	public final Object getValue() {
		return value;
	}
	
	/**
	 * @param value the value to set
	 */
	public void setValue(final Serializable value) {
		this.value = value;
	}

	/**
	 * Getter descrizione.
	 *
	 * @return	descrizione
	 */
	public final String getDescription() {
		return description;
	}
	
	/**
	 * Setter descrizione.
	 * 
	 * @param description
	 */
	public void setDescription(final String description) {
		this.description = description;
	}

	/**
	 * @see java.lang.Object#hashCode().
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object).
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof SelectItemDTO)) {
			return false;
		}
		final SelectItemDTO other = (SelectItemDTO) obj;
		if (description == null) {
			if (other.description != null) {
				return false;
			}
		} else if (!description.equals(other.description)) {
			return false;
		}
		if (value == null) {
			if (other.value != null) {
				return false;
			}
		} else if (other.value == null || !value.toString().equals(other.value.toString())) {
			return false;
		}
		return true;
	}

	
}
