package it.ibm.red.business.dto;

import java.util.Date;

/**
 * The Class ProtocolloEmergenzaDocDTO.
 *
 * @author a.dilegge
 * 
 * 	DTO protocollo emergenza.
 */
public class ProtocolloEmergenzaDocDTO extends AbstractDTO {
	
	

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 5899907999995977843L;
	
	/**
	 * Identificativo documento
	 */
	private String idDocumento;
	
	/**
	 * Identificativo aoo
	 */
	private Long idAoo;
	
	/**
	 * Identificativo protocollo emergenza
	 */
	private String idProtocolloEmergenza;
	
	/**
	 * Numero protocollo emergenza
	 */
	private Integer numeroProtocolloEmergenza;
	
	/**
	 * Anno protocollo emergenza
	 */
	private Integer annoProtocolloEmergenza;
	
	/**
	 * Data protocollo emergenza
	 */
	private Date dataProtocolloEmergenza;
	
	/**
	 * Tipo protocollo
	 */
	private Integer tipoProtocollo;
	
	/**
	 * Identificativo protocollo
	 */
	private String idProtocollo;
	
	/**
	 * Message id NPS
	 */
	private String messageIdNPS;
	
	/**
	 * Numero protocollo
	 */
	private Integer numeroProtocollo;
	
	/**
	 * Anno protocollo
	 */
	private Integer annoProtocollo;
	
	/**
	 * Data protocollo
	 */
	private Date dataProtocollo;
	
	/**
	 * Data riconciliazione
	 */
	private Date dataRiconciliazione;
	
	/**
	 * Costruttore protocollo emergenza doc DTO.
	 */
	public ProtocolloEmergenzaDocDTO() {
		super();
	}

	/**
	 * Costruttore protocollo emergenza doc DTO.
	 *
	 * @param numeroProtocolloEmergenza the numero protocollo emergenza
	 * @param annoProtocolloEmergenza the anno protocollo emergenza
	 * @param dataProtocolloEmergenza the data protocollo emergenza
	 */
	public ProtocolloEmergenzaDocDTO(final Integer numeroProtocolloEmergenza, final Integer annoProtocolloEmergenza, final Date dataProtocolloEmergenza) {
		super();
		this.numeroProtocolloEmergenza = numeroProtocolloEmergenza;
		this.annoProtocolloEmergenza = annoProtocolloEmergenza;
		this.dataProtocolloEmergenza = dataProtocolloEmergenza;
	}
	
	/**
	 * Costruttore protocollo emergenza doc DTO.
	 *
	 * @param numeroProtocolloEmergenza the numero protocollo emergenza
	 * @param annoProtocolloEmergenza the anno protocollo emergenza
	 * @param dataProtocolloEmergenza the data protocollo emergenza
	 * @param tipoProtocollo the tipo protocollo
	 */
	public ProtocolloEmergenzaDocDTO(final Integer numeroProtocolloEmergenza, final Integer annoProtocolloEmergenza, final Date dataProtocolloEmergenza, final Integer tipoProtocollo) {
		super();
		this.numeroProtocolloEmergenza = numeroProtocolloEmergenza;
		this.annoProtocolloEmergenza = annoProtocolloEmergenza;
		this.dataProtocolloEmergenza = dataProtocolloEmergenza;
		this.tipoProtocollo = tipoProtocollo;
	}
	
	/**
	 * Costruttore protocollo emergenza doc DTO.
	 *
	 * @param idDocumento the id documento
	 * @param idAoo the id aoo
	 * @param idProtocolloEmergenza the id protocollo emergenza
	 * @param numeroProtocolloEmergenza the numero protocollo emergenza
	 * @param annoProtocolloEmergenza the anno protocollo emergenza
	 * @param dataProtocolloEmergenza the data protocollo emergenza
	 * @param tipoProtocollo the tipo protocollo
	 */
	public ProtocolloEmergenzaDocDTO(final String idDocumento, final Long idAoo, final String idProtocolloEmergenza,
			final Integer numeroProtocolloEmergenza, final Integer annoProtocolloEmergenza, final Date dataProtocolloEmergenza,
			final Integer tipoProtocollo) {
		this(numeroProtocolloEmergenza, annoProtocolloEmergenza, dataProtocolloEmergenza, tipoProtocollo);
		this.idDocumento = idDocumento;
		this.idAoo = idAoo;
		this.idProtocolloEmergenza = idProtocolloEmergenza;
	}
	
	/**
	 * Costruttore protocollo emergenza doc DTO.
	 *
	 * @param idDocumento
	 *            the id documento
	 * @param idAoo
	 *            the id aoo
	 * @param idProtocolloEmergenza
	 *            the id protocollo emergenza
	 * @param numeroProtocolloEmergenza
	 *            the numero protocollo emergenza
	 * @param annoProtocolloEmergenza
	 *            the anno protocollo emergenza
	 * @param dataProtocolloEmergenza
	 *            the data protocollo emergenza
	 * @param tipoProtocollo
	 *            the tipo protocollo
	 * @param messageIdNPS
	 *            the message id NPS
	 * @param idProtocollo
	 *            the id protocollo
	 * @param numeroProtocollo
	 *            the numero protocollo
	 * @param annoProtocollo
	 *            the anno protocollo
	 * @param dataProtocollo
	 *            the data protocollo
	 * @param dataRiconciliazione
	 *            the data riconciliazione
	 */
	public ProtocolloEmergenzaDocDTO(final String idDocumento, final Long idAoo, final String idProtocolloEmergenza,
			final Integer numeroProtocolloEmergenza, final Integer annoProtocolloEmergenza, final Date dataProtocolloEmergenza, final Integer tipoProtocollo, 
			final String messageIdNPS, final String idProtocollo, final Integer numeroProtocollo, final Integer annoProtocollo, final Date dataProtocollo,
			final Date dataRiconciliazione) {
		this(idDocumento, idAoo, idProtocolloEmergenza, numeroProtocolloEmergenza, annoProtocolloEmergenza, dataProtocolloEmergenza, tipoProtocollo);
		this.messageIdNPS = messageIdNPS;
		this.idProtocollo = idProtocollo;
		this.numeroProtocollo = numeroProtocollo;
		this.annoProtocollo = annoProtocollo;
		this.dataProtocollo = dataProtocollo;
		this.dataRiconciliazione = dataRiconciliazione;
	}	
	
	/** 
	 * @return the id documento
	 */
	public String getIdDocumento() {
		return idDocumento;
	}
	
	/** 
	 * @param idDocumento the new id documento
	 */
	public void setIdDocumento(final String idDocumento) {
		this.idDocumento = idDocumento;
	}
	
	/** 
	 * @return the id aoo
	 */
	public Long getIdAoo() {
		return idAoo;
	}
	
	/** 
	 * @param idAoo the new id aoo
	 */
	public void setIdAoo(final Long idAoo) {
		this.idAoo = idAoo;
	}
	
	/** 
	 * @return the id protocollo emergenza
	 */
	public String getIdProtocolloEmergenza() {
		return idProtocolloEmergenza;
	}
	
	/** 
	 * @param idProtocolloEmergenza the new id protocollo emergenza
	 */
	public void setIdProtocolloEmergenza(final String idProtocolloEmergenza) {
		this.idProtocolloEmergenza = idProtocolloEmergenza;
	}
	
	/** 
	 * @return the numero protocollo emergenza
	 */
	public Integer getNumeroProtocolloEmergenza() {
		return numeroProtocolloEmergenza;
	}
	
	/** 
	 * @param numeroProtocolloEmergenza the new numero protocollo emergenza
	 */
	public void setNumeroProtocolloEmergenza(final Integer numeroProtocolloEmergenza) {
		this.numeroProtocolloEmergenza = numeroProtocolloEmergenza;
	}
	
	/** 
	 * @return the anno protocollo emergenza
	 */
	public Integer getAnnoProtocolloEmergenza() {
		return annoProtocolloEmergenza;
	}
	
	/** 
	 * @param annoProtocolloEmergenza the new anno protocollo emergenza
	 */
	public void setAnnoProtocolloEmergenza(final Integer annoProtocolloEmergenza) {
		this.annoProtocolloEmergenza = annoProtocolloEmergenza;
	}
	
	/** 
	 * @return the data protocollo emergenza
	 */
	public Date getDataProtocolloEmergenza() {
		return dataProtocolloEmergenza;
	}
	
	/** 
	 * @param dataProtocolloEmergenza the new data protocollo emergenza
	 */
	public void setDataProtocolloEmergenza(final Date dataProtocolloEmergenza) {
		this.dataProtocolloEmergenza = dataProtocolloEmergenza;
	}
	
	/** 
	 * @return the tipo protocollo
	 */
	public Integer getTipoProtocollo() {
		return tipoProtocollo;
	}
	
	/** 
	 * @param tipoProtocollo the new tipo protocollo
	 */
	public void setTipoProtocollo(final Integer tipoProtocollo) {
		this.tipoProtocollo = tipoProtocollo;
	}
	
	/** 
	 * @return the message id NPS
	 */
	public String getMessageIdNPS() {
		return messageIdNPS;
	}
	
	/** 
	 * @param messageIdNPS the new message id NPS
	 */
	public void setMessageIdNPS(final String messageIdNPS) {
		this.messageIdNPS = messageIdNPS;
	}
	
	/** 
	 * @return the id protocollo
	 */
	public String getIdProtocollo() {
		return idProtocollo;
	}
	
	/** 
	 * @param idProtocollo the new id protocollo
	 */
	public void setIdProtocollo(final String idProtocollo) {
		this.idProtocollo = idProtocollo;
	}
	
	/** 
	 * @return the numero protocollo
	 */
	public Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}
	
	/** 
	 * @param numeroProtocollo the new numero protocollo
	 */
	public void setNumeroProtocollo(final Integer numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}
	
	/** 
	 * @return the anno protocollo
	 */
	public Integer getAnnoProtocollo() {
		return annoProtocollo;
	}
	
	/** 
	 * @param annoProtocollo the new anno protocollo
	 */
	public void setAnnoProtocollo(final Integer annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}
	
	/** 
	 * @return the data protocollo
	 */
	public Date getDataProtocollo() {
		return dataProtocollo;
	}
	
	/** 
	 * @param dataProtocollo the new data protocollo
	 */
	public void setDataProtocollo(final Date dataProtocollo) {
		this.dataProtocollo = dataProtocollo;
	}
	
	/** 
	 * @return the data riconciliazione
	 */
	public Date getDataRiconciliazione() {
		return dataRiconciliazione;
	}
	
	/** 
	 * @param dataRiconciliazione the new data riconciliazione
	 */
	public void setDataRiconciliazione(final Date dataRiconciliazione) {
		this.dataRiconciliazione = dataRiconciliazione;
	}
	
}