package it.ibm.red.business.enums;

/**
 * The Enum InoltroRifiutoStatiEnum.
 *
 * @author adilegge
 * 
 *         Enum degli stati relativi alla coda di inoltro rifiuto
 */
public enum ProtocollaMailStatiEnum {
	
	/**
	 * Valore.
	 */
	ERRORE_IN_FASE_DI_PRESA_IN_CARICO(-1), 
	
	/**
	 * Valore.
	 */
	ERRORE_IN_FASE_DI_CREAZIONE_DOCUMENTO_IN_ENTRATA(-2),
	
	/**
	 * Valore.
	 */
	ERRORE_IN_FASE_DI_CREAZIONE_DOCUMENTO_IN_USCITA(-3),
	
	/**
	 * Valore.
	 */
	DA_ELABORARE(0), 
	
	/**
	 * Valore.
	 */
	PRESO_IN_CARICO(1), 
	
	/**
	 * Valore.
	 */
	DOCUMENTO_IN_ENTRATA_CREATO(2), 
	
	/**
	 * Valore.
	 */
	DOCUMENTO_IN_USCITA_CREATO(3);

	/**
	 * Nome dell'icona.
	 */
	private Integer id; 

	/**
	 * Costruttore.
	 * 
	 * @param inIconName	descrizione icona
	 */
	ProtocollaMailStatiEnum(final Integer inId) {
		id = inId;
	}

	/**
	 * Restituisce l'id.
	 * @return id
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * Restituisce l'enum associata al value.
	 * @param value
	 * @return enum associata
	 */
	public static ProtocollaMailStatiEnum get(final Integer value) {
		ProtocollaMailStatiEnum output = null;
		for (ProtocollaMailStatiEnum q: ProtocollaMailStatiEnum.values()) {
			if (q.getId().equals(value)) {
				output = q;
				break;
			}
		}
		return output;
	}

}
