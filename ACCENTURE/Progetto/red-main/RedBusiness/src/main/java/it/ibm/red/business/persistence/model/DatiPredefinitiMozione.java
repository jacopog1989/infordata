package it.ibm.red.business.persistence.model;

import java.io.Serializable;

/**
 * The Class DatiPredefinitiMozione.
 *
 * @author m.crescentini
 * 
 *         Entità che mappa la tabella DATI_PREDEFINITI_MOZIONE.
 */
public class DatiPredefinitiMozione implements Serializable {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 738464163169478738L;

	/**
	 * Dati predefiniti mozione.
	 */
	private Integer idDatiPredefinitiMozione;
	
	/**
	 * Tipologia documento.
	 */
	private Integer idTipologiaDocumento;
	
	/**
	 * Tipologia procedimento.
	 */
	private Integer idTipologiaProcedimento;
	
	/**
	 * Ufficio.
	 */
	private Long idUfficio;
	
	/**
	 * Iter approvativo.
	 */
	private Integer idIterApprovativo;
	
	/**
	 * Email.
	 */
	private String email;
	
	/**
	 * Flag mail predefinita.
	 */
	private boolean isEmailPredefinita;
	
	/**
	 * Flag firma RGS.
	 */
	private int firmaDigRGS;
	
	/**
	 * @return the idDatiPredefinitiMozione
	 */
	public Integer getIdDatiPredefinitiMozione() {
		return idDatiPredefinitiMozione;
	}


	/**
	 * @param idDatiPredefinitiMozione the idDatiPredefinitiMozione to set
	 */
	public void setIdDatiPredefinitiMozione(final Integer idDatiPredefinitiMozione) {
		this.idDatiPredefinitiMozione = idDatiPredefinitiMozione;
	}


	/**
	 * @return the idTipologiaDocumento
	 */
	public Integer getIdTipologiaDocumento() {
		return idTipologiaDocumento;
	}


	/**
	 * @param idTipologiaDocumento the idTipologiaDocumento to set
	 */
	public void setIdTipologiaDocumento(final Integer idTipologiaDocumento) {
		this.idTipologiaDocumento = idTipologiaDocumento;
	}


	/**
	 * @return the idTipologiaProcedimento
	 */
	public Integer getIdTipologiaProcedimento() {
		return idTipologiaProcedimento;
	}


	/**
	 * @param idTipologiaProcedimento the idTipologiaProcedimento to set
	 */
	public void setIdTipologiaProcedimento(final Integer idTipologiaProcedimento) {
		this.idTipologiaProcedimento = idTipologiaProcedimento;
	}


	/**
	 * @return the idUfficio
	 */
	public Long getIdUfficio() {
		return idUfficio;
	}


	/**
	 * @param idUfficio the idUfficio to set
	 */
	public void setIdUfficio(final Long idUfficio) {
		this.idUfficio = idUfficio;
	}


	/**
	 * @return the idIterApprovativo
	 */
	public Integer getIdIterApprovativo() {
		return idIterApprovativo;
	}


	/**
	 * @param idIterApprovativo the idIterApprovativo to set
	 */
	public void setIdIterApprovativo(final Integer idIterApprovativo) {
		this.idIterApprovativo = idIterApprovativo;
	}


	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}


	/**
	 * @param email the email to set
	 */
	public void setEmail(final String email) {
		this.email = email;
	}


	/**
	 * @return the isEmailPredefinita
	 */
	public boolean isEmailPredefinita() {
		return isEmailPredefinita;
	}


	/**
	 * @param isEmailPredefinita the isEmailPredefinita to set
	 */
	public void setEmailPredefinita(final boolean isEmailPredefinita) {
		this.isEmailPredefinita = isEmailPredefinita;
	}


	/**
	 * @return the firmaDigRGS
	 */
	public int getFirmaDigRGS() {
		return firmaDigRGS;
	}


	/**
	 * @param firmaDigRGS the firmaDigRGS to set
	 */
	public void setFirmaDigRGS(final int firmaDigRGS) {
		this.firmaDigRGS = firmaDigRGS;
	}
}