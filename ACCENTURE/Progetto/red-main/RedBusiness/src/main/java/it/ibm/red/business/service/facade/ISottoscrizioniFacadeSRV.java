package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.CanaleTrasmissioneDTO;
import it.ibm.red.business.dto.DisabilitazioneNotificheUtenteDTO;
import it.ibm.red.business.dto.EventoSottoscrizioneDTO;
import it.ibm.red.business.dto.MailAddressDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.IntervalloDisabilitazioneEnum;

/**
 * Facade del servizio di gestione sottoscrizioni.
 */
public interface ISottoscrizioniFacadeSRV extends Serializable {

	/**
	 * Carica le costanti per popolare le selct nelle configurazioni dei canali di trasmissione.
	 * 
	 * @return
	 */
	Collection<CanaleTrasmissioneDTO> loadCanaliTrasmissione();

	/**
	 * Popola le select delle configurazioni dei canali di trasmissione.
	 * 
	 * @return
	 */
	Collection<IntervalloDisabilitazioneEnum> loadIntervalloDisabilitazione();
	
	/**
	 * Ritorna le configurazioni impostate dall'utente.
	 * 
	 * @param utente
	 * @return
	 */
	Collection<DisabilitazioneNotificheUtenteDTO> loadDisabilitazioniNotificheUtente(UtenteDTO utente);
	
	/**
	 * Registra le disabilitazioni in input per l'utente.
	 * 
	 * @param utente
	 * @param disabilitazioniNotificheUtente
	 * @return true, se l'operazione si è conclusa positivamente, false altrimenti
	 */
	boolean registraDisabilitazioneNotificheUtente(UtenteDTO utente, Collection<DisabilitazioneNotificheUtenteDTO> disabilitazioniNotificheUtente);
	
	/**
	 * Registra le sottoscrizioni in input per l'utente.
	 * 
	 * @param utente
	 * @param listaSottoscrizioni
	 * @return true, se l'operazione si è conclusa positivamente, false altrimenti
	 */
	boolean registraSottoscrizione(UtenteDTO utente, List<EventoSottoscrizioneDTO> listaSottoscrizioni);
	
	/**
	 * Aggiorna la lista delle mail utente in input in input.
	 * 
	 * @param utente
	 * @param mail
	 * @return true, se l'operazione si è conclusa positivamente, false altrimenti
	 */
	boolean registraMail(UtenteDTO utente, List<String> listaMail);
	
	/**
	 * Ritorna gli indirizzi email utilizzati per le notifiche dell'utente.
	 * 
	 * @param utente
	 * @return
	 */
	Collection<MailAddressDTO> loadMailAddress(UtenteDTO utente);
	
	/**
	 * Ritorna gli eventi sottoscritti dall'utente.
	 * 
	 * @param utente
	 * @return
	 */
	List<EventoSottoscrizioneDTO> getEventiSottoscritti(UtenteDTO utente); 
	
	/**
	 * Ritorna gli eventi sottoscritti dall'utente.
	 * 
	 * @param utente
	 * @return
	 */
	List<EventoSottoscrizioneDTO> getEventiByAoo(Integer idAoo); 
	
	/**
	 * Recupera il numero di sottoscrizioni non ancora visualizzate dall'utente.
	 * 
	 * @param utente
	 * @return
	 */
	int getCountSottoscrizioni(UtenteDTO utente, int numeroGiorni);
}