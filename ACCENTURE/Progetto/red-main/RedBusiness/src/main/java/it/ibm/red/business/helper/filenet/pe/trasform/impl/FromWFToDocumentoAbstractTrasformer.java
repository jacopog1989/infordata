package it.ibm.red.business.helper.filenet.pe.trasform.impl;

import filenet.vw.api.VWException;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.PEDocumentoAbsrtractDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;

/**
 * Trasformer da WF a Document - Abstract.
 * @param <T>
 */
public abstract class FromWFToDocumentoAbstractTrasformer<T extends PEDocumentoAbsrtractDTO> extends TrasformerPE<T> {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -3656609274899549589L;
	
	/**
	 * Properties.
	 */
	private final PropertiesProvider pp;
	
	/**
	 *  Costruttore.
	 * @param inEnumKey
	 */
	FromWFToDocumentoAbstractTrasformer(final TrasformerPEEnum inEnumKey) {
		super(inEnumKey);
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE#trasform(filenet.vw.api.VWWorkObject).
	 */
	@Override
	public T trasform(final VWWorkObject object) {
		try {
			final String idDocumento = getMetadato(object, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY)).toString();
			
			final String wobNumber = getWobNumber(object);
			final String queueName = getQueueName(object);
			
			final Integer idUtenteDestinatario = (Integer) getMetadato(object, pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY));
			final Integer idNodoDestinatario = (Integer) getMetadato(object, pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY));
			final Integer tipoAssegnazioneId = (Integer) getMetadato(object, pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY));
			final String codaCorrente = (String) getMetadato(object, pp.getParameterByKey(PropertiesNameEnum.NOME_CODA_METAKEY));
	
			final Boolean inLavorazione = getInLavorazione(object);
			return createAbstract(object, wobNumber, idDocumento, tipoAssegnazioneId, codaCorrente, DocumentQueueEnum.get(queueName, inLavorazione),  
					idNodoDestinatario, idUtenteDestinatario);
			
		} catch (final Exception e) {
			getLogger().error("Errore nel recupero di un metadato durante la trasformazione da WorkFlow a PEDocumentoDTO : ", e);
			return null;
		}
	}

	/**
	 * Restituisce il flag associato al metadato IN_LAVORAZIONE associato al
	 * workflow.
	 * 
	 * @param workflow
	 * @return true se in lavorazione, false altrimenti.
	 */
	private Boolean getInLavorazione(final VWWorkObject workflow) {
		Boolean inLavorazione = null;
		try {
			inLavorazione = ((Integer) getMetadatoWithVWException(workflow, pp.getParameterByKey(PropertiesNameEnum.IN_LAVORAZIONE_METAKEY))) == 1;
		} catch (final VWException e) {
			getLogger().warn(e);
			//Se non è nel wf, significa che è gestita e rimane a null.
		}
		return inLavorazione;
	}

	/**
	 * @param object
	 * @return wobnumber del workflow
	 */
	private String getWobNumber(final VWWorkObject object) {
		String wobNumber = null;
		try {
			wobNumber = object.getWorkObjectNumber();
		} catch (final VWException e) {
			getLogger().error("Errore nel recupero del Wob Number.", e);
		}
		return wobNumber;
	}
	
	/**
	 * @param object
	 * @return nome coda
	 */
	private String getQueueName(final VWWorkObject object) {
		String queueName = null;
		
		try {
			queueName = object.getCurrentQueueName();
		} catch (final VWException e) {
			getLogger().error("Errore nel recupero del Nome Coda.", e);
		}
		return queueName;
	}
	
	
	
	abstract T createAbstract(VWWorkObject object, String wobNumber, String idDocumento, Integer tipoAssegnazioneId, String codaCorrente, DocumentQueueEnum queue, Integer idNodoDestinatario, Integer idUtenteDestinatario);

	protected abstract REDLogger getLogger();
}
