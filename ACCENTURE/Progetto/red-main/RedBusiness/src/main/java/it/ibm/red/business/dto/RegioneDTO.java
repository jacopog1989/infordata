package it.ibm.red.business.dto;

import java.io.Serializable;

/**
 * DTO che definisce le caratteristiche di un regione.
 */
public class RegioneDTO implements Serializable {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1048344785481289154L;
	
	/**
	 * Redione.
	 */
	private String idRegione; 

	/**
	 * Denominazione.
	 */
	private String denominazione;
	
	/**
	 * Restituisce l'id della Regione.
	 * @return id regione
	 */
	public String getIdRegione() {
		return idRegione;
	}
	
	/**
	 * Imposta l'id della Regione.
	 * @param idRegione
	 */
	public void setIdRegione(final String idRegione) {
		this.idRegione = idRegione;
	}
	
	/**
	 * Restituisce la denominazione della regione.
	 * @return denominazione regione
	 */
	public String getDenominazione() {
		return denominazione;
	}
	
	/**
	 * Imposta la denominazione della regione
	 * @param denominazione
	 */
	public void setDenominazione(final String denominazione) {
		this.denominazione = denominazione;
	}
	
	/**
	 * @see java.lang.Object#toString().
	 */
	@Override
	public String toString() {
		return "RegioneETY [idRegione=" + idRegione + ", denominazione=" + denominazione + "]";
	} 

}
