/**
 * 
 */
package it.ibm.red.business.service.facade;

import java.io.Serializable;

import it.ibm.red.business.dto.AllaccioDocUscitaDTO; 

/**
 * @author VINGENITO
 *
 */
public interface IAllacciaDocUscitaFacadeSRV extends Serializable {
	
	/**
	 * Inserisce i documenti nella tabella di allaccio documenti di uscita.
	 * @param estendiVisibilitaDTO
	 */
	void estendiVisibilitaDocumentiRisposta(AllaccioDocUscitaDTO estendiVisibilitaDTO);

	
	

}
