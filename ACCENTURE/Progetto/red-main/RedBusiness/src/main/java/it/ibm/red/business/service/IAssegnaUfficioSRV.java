package it.ibm.red.business.service;

import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.service.facade.IAssegnaUfficioFacadeSRV;

/**
 * Interface del service che gestisce l'assegnamento ad ufficio.
 */
public interface IAssegnaUfficioSRV extends IAssegnaUfficioFacadeSRV {

	/**
	 * Ottiene la descrizione dell'ufficio dal nodo.
	 * @param nodo
	 * @return descrizione dell'ufficio
	 */
	String getDescrizioneUfficioFromNodo(Nodo nodo);
}
