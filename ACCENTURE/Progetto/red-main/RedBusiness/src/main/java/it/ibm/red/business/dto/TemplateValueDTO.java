package it.ibm.red.business.dto;

import java.io.Serializable;

/**
 * DTO che definisce il valore template.
 */
public class TemplateValueDTO extends TemplateDTO implements Serializable {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 8884824499272657064L;

	/**
	 * Identificativo documento.
	 */
	private String idDocumento;
	
	/**
	 * Nome documento.
	 */
	private String nomeDocumento;

	/**
	 * Costruttore vuoto
	 */
	public TemplateValueDTO() {
	}

	/**
	 * Costruttore di default.
	 * @param template
	 * @param idDocumento
	 * @param nomeDocumento
	 */
	public TemplateValueDTO(final TemplateDTO template, final String idDocumento, final String nomeDocumento) {
		setDescrizione(template.getDescrizione());
		setIdDocumento(idDocumento);
		setIdTemplate(template.getIdTemplate());
		setNomeDocumento(nomeDocumento);
	}

	/**
	 * Restituisce l'id del documento.
	 * @return idDocumento
	 */
	public String getIdDocumento() {
		return idDocumento;
	}

	/**
	 * Imposta l'id del documento.
	 * @param idDocumento
	 */
	public void setIdDocumento(final String idDocumento) {
		this.idDocumento = idDocumento;
	}

	/**
	 * @return nomeDocumento
	 */
	public String getNomeDocumento() {
		return nomeDocumento;
	}

	/**
	 * Imposta il nome del documento.
	 * @param nomeDocumento
	 */
	public void setNomeDocumento(final String nomeDocumento) {
		this.nomeDocumento = nomeDocumento;
	}

	/**
	 * @see java.lang.Object#toString().
	 */
	@Override
	public String toString() {
		return super.toString() + " | TemplateValue [idDocumento=" + idDocumento + ", nomeDocumento=" + nomeDocumento + "]";
	}
	
	/**
	 * @see java.lang.Object#hashCode().
	 */
	@Override
	public int hashCode() {
		return super.hashCode();
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object).
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		if (!super.equals(obj)) {
			return false;
		}
		final TemplateValueDTO other = (TemplateValueDTO) obj;
		if (idDocumento == null) {
			if (other.idDocumento != null) {
				return false;
			}
		} else if (!idDocumento.equals(other.idDocumento)) {
			return false;
		}
		if (nomeDocumento == null) {
			if (other.nomeDocumento != null) {
				return false;
			}
		} else if (!nomeDocumento.equals(other.nomeDocumento)) {
			return false;
		}
		return true;
	}
}