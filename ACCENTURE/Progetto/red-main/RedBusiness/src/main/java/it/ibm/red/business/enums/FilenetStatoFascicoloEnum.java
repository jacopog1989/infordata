package it.ibm.red.business.enums;

/**
 * The Enum FilenetStatoFascicoloEnum.
 *
 * @author m.crescentini
 * 
 *         Enum degli stati di un fascicolo.
 */
public enum FilenetStatoFascicoloEnum {
	
	/**
	 * Property "statoFascicolo" del fascicolo FN.
	 */
	APERTO(0, "Aperto"),
	
	/**
	 * Property "statoFascicolo" del fascicolo FN.
	 */
	CHIUSO(1, "Chiuso"),
	
	/**
	 * Property "titolario" del fascicolo FN.
	 */
	NON_CATALOGATO(-1, "Non Catalogato");
	

	/**
	 * Identificativo.
	 */
	private final Integer id;
	

	/**
	 * Nome.
	 */
	private final String nome;
	
	/**
	 * Costruttore dell'Enum.
	 * @param id
	 * @param nome
	 */
	FilenetStatoFascicoloEnum(final Integer id, final String nome) {
		this.id = id;
		this.nome = nome;
	}
	
	/**
	 * Restituisce l'id.
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}
	
	/**
	 * Restituisce l'enum identificata dall'id in ingresso.
	 * @param id
	 * @return Enum
	 */
	public static FilenetStatoFascicoloEnum getEnumById(final Integer id) {
		FilenetStatoFascicoloEnum output = null;
		for (FilenetStatoFascicoloEnum item : FilenetStatoFascicoloEnum.values()) {
			if (item.getId().equals(id)) {
				output = item;
				break;
			}
		}

		return output;
	}
}