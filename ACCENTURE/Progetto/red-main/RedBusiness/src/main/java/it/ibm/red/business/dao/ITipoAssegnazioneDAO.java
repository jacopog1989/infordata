package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.enums.TipoAssegnazioneEnum;

/**
 * Interfaccia DAO tipo assegnazione.
 */
public interface ITipoAssegnazioneDAO extends Serializable {

	/**
	 * Ottiene i tipi assegnazione manuale tramite l'id dell'aoo.
	 * @param idAOO
	 * @param con
	 * @return lista di tipi assegnazione
	 */
	List<TipoAssegnazioneEnum> getTipiAssegnazioneManualeByIdAoo(Integer idAOO, Connection con);
}
