package it.ibm.red.business.helper.notifiche;

import java.util.Map;

import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.persistence.model.CodaEmail;

/**
 * Interface delle classi di gestione Mail State.
 */
public interface IGestioneMailState {

	/**
	 * Ottiene la successiva notifica email da aggiornare.
	 * @param nh
	 * @param notificaEmail
	 * @param notifiche
	 * @param reInvio
	 * @return coda mail
	 */
	CodaEmail getNextNotificaEmailToUpdate(NotificaHelper nh, CodaEmail notificaEmail, Map<String, EmailDTO> notifiche, boolean reInvio);

}
