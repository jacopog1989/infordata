package it.ibm.red.business.dto;

import java.util.Date;
import java.util.List;

import it.ibm.red.business.enums.ColoreInteroperabilitaEnum;
import it.ibm.red.business.enums.EsitoValidazioneSegnaturaInteropEnum;
import it.ibm.red.business.enums.IconaStatoEnum;
import it.ibm.red.business.enums.ImageMailEnum;
import it.ibm.red.business.enums.StatoMailEnum;

/**
 * Email DTO.
 * 
 * @author CPAOLUZI
 *
 */
public class EmailDTO extends AbstractDTO {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1064315324890933134L;

	/**
	 * Stato.
	 */
	private StatoMailEnum stato;
	
	/**
	 * Stato precedente all'archiviazione.
	 */
	private transient StatoMailEnum statoPreArchiviazione;	
	
	/**
	 * Identificativo utente protocollatore.
	 */
	private transient Integer idUtenteProtocollatore;
	
	/**
	 * Descrizione utente protocollatore.
	 */
	private transient String descUtenteProtocollatore;

	/**
	 * guid emailDTO.
	 */
	private String guid;
	
	/**
	 * Mittente.
	 */
	private String mittente;

	/**
	 * Oggetto.
	 */
	private String oggetto;

	/**
	 * Testo.
	 */
	private String testo;
	
	/**
	 * Testo HTML.
	 */
	private String testoHTML;
	
	/**
	 * Destinatari.
	 */
	private String destinatari;

	/**
	 * Destinatari in cc.
	 */
	private String destinatariCC;
	
	/**
	 * Identificativo messaggio.
	 */
	private String msgID;
	
	/**
	 * Nota della mail (colore + testo).
	 */
	private transient NotaDTO nota;
	
	/**
	 * Flag allegati.
	 */
	private Boolean hasAllegati;
	
	/**
	 * Motivo eliminazione.
	 */
	private transient String motivoEliminazione;
	
	/**
	 * Data ricezione.
	 */
	private transient Date dataRicezione;

	/**
	 * Data invio.
	 */
	private transient Date dataInvio;
	
	/**
	 * Data cambio stato email.
	 */
	private transient Date dataCambioStato;
	
	/**
	 * Data scarico / ripristino email.
	 */
	private transient Date dataScarico;
	
	/**
	 * Allegati della mail.
	 */
	private transient List<String> idAllegati;
	
    
	/**
	 * Stato notifica.
	 */
	private transient String statoNotifica;
	
	/**
	 * Icona email.
	 */
	private transient ImageMailEnum imageMailEnum;
	
	/**
	 * Flag reinvio mail.
	 */
	private transient boolean reinvioMail;
	
	/**
	 * Utilizzato da Reinvia per Protocollo NPS.
	 */
	private transient String mittenteDestinatariTemp;
	
	/**
	 * Colorere interop.
	 */
	private ColoreInteroperabilitaEnum coloreInteroperabilita;

	/**
	 * Colore pre-assegnatario.
	 */
	private ColoreInteroperabilitaEnum colorePreassegnatario; 
	
	/**
	 * Esito validazione.
	 */
	private EsitoValidazioneSegnaturaInteropEnum esitoValidazioneEnum;

	/**
	 * Tooltip preassegnatario.
	 */
	private String tooltipPreassegnatario;

	/**
	 * Preassegnatario.
	 */
	private AssegnatarioInteropDTO preassegnatarioInterop;
	
	/**
	 * Lista protocolli gnerati.
	 */
	private List<String> protocolliGenerati;
		 
	/**
	 * Flag provenienza archivio.
	 */
	private boolean isFromArchiviate; 
	 
	/**
	 * Operazione email.
	 */
	private String operazionePostaElettronica;
	
	/**
	 * Lista operazioni.
	 */
	private List<String> listOperazionePostaElettronica;
	  
	/**
	 * Folder mail inbox.
	 */
	private String folderMailInbox;
	
	/**
	 * Folder mail archiviate.
	 */
	private String folderMailArchiviate;
	
	/**
	 * Attiva response ripristino.
	 */
	private boolean attivaRipristinaResponse;
	
	/**
	 * Document title.
	 */
	private String documentTitle;
	
	/**
	 * Document title.
	 */
	private IconaStatoEnum iconaStato;
	

	/**
	 * Protocolla e mantieni
	 */
	private boolean protocollaEMantieni;
		
	
	
	/**
	 * Costruttore.
	 */
	public EmailDTO() {
		super();
	}

	/**
	 * Costruttore.
	 * @param inMittente - mittente
	 * @param inOggetto - oggetto
	 * @param inTesto - testo
	 * @param inDestinatari - destinatari
	 * @param inDestinatariCC - destinatariCC
	 * @param inMsgID - sgID
	 * @param inMotivoEliminazione - motivoEliminazione
	 */
	public EmailDTO(final String inMittente, final String inOggetto, final String inTesto, final String inDestinatari, final String inDestinatariCC,
			final String inMsgID, final String inMotivoEliminazione, final List<String> inIdAllegati) {
		super();
		this.mittente = inMittente;
		this.oggetto = inOggetto;
		this.testo = inTesto;
		this.destinatari = inDestinatari;
		this.destinatariCC = inDestinatariCC;
		this.msgID = inMsgID;
		this.motivoEliminazione = inMotivoEliminazione;
		this.idAllegati = inIdAllegati;
	}

	/**
	 * Recupera la data.
	 * @return data di invio o di ricezione
	 */
	public Date getData() {
		Date data = dataInvio;
		if (data == null) {
			data = dataRicezione;
		}
		return data;
	}
	
	/**
	 * Recupera la data di ricezione della mail.
	 * @return data di ricezione della mail
	 */
	public Date getDataRicezione() {
		return dataRicezione;
	}
	
	/**
	 * Imposta la data di ricezione della mail.
	 * @param dataRicezione data di ricezione della mail
	 */
	public void setDataRicezione(final Date dataRicezione) {
		this.dataRicezione = dataRicezione;
	}
	
	/**
	 * Setter.
	 * 
	 * @param inMotivoEliminazione	motivo eliminazione
	 */
	public void setMotivoEliminazione(final String inMotivoEliminazione) {
		this.motivoEliminazione = inMotivoEliminazione;
	}

	/**
	 * Getter.
	 * 
	 * @return	locked
	 */
	public Boolean getIsLocked() {
		Boolean output = false; //puoi procedere
		if (StatoMailEnum.IN_FASE_DI_PROTOCOLLAZIONE_MANUALE.equals(stato) || (idUtenteProtocollatore != null && idUtenteProtocollatore != 0)) {
			output = true; // non puoi procedere
		}
		return output;
	}

	/**
	 * Getter.
	 * @return mittente
	 */
	public String getMittente() {
		return mittente;
	}

	/**
	 * Getter.
	 * @return oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * Getter.
	 * @return testo
	 */
	public String getTesto() {
		return testo;
	}

	/**
	 * Getter.
	 * @return destinatari
	 */
	public String getDestinatari() {
		return destinatari;
	}

	/**
	 * Getter.
	 * @return destinatariCC
	 */
	public String getDestinatariCC() {
		return destinatariCC;
	}
	
	/**
	 * Getter.
	 * @return msgID
	 */
	public String getMsgID() {
		return msgID;
	}

	/**
	 * Setter.
	 * 
	 * @param inMittente	mittente
	 */
	public void setMittente(final String inMittente) {
		this.mittente = inMittente;
	}

	/**
	 * Setter.
	 * 
	 * @param inOggetto	oggetto
	 */
	public void setOggetto(final String inOggetto) {
		this.oggetto = inOggetto;
	}

	/**
	 * Setter.
	 * 
	 * @param inTesto	testo
	 */
	public void setTesto(final String inTesto) {
		this.testo = inTesto;
	}

	/**
	 * Setter.
	 * 
	 * @param inDestinatari	destinatari
	 */
	public void setDestinatari(final String inDestinatari) {
		this.destinatari = inDestinatari;
	}

	/**
	 * Setter.
	 * 
	 * @param inDestinatariCC	destinatari cc
	 */
	public void setDestinatariCC(final String inDestinatariCC) {
		this.destinatariCC = inDestinatariCC;
	}

	/**
	 * Setter.
	 * 
	 * @param inMsgID	message id
	 */
	public void setMsgID(final String inMsgID) {
		this.msgID = inMsgID;
	}

	/**
	 * Getter.
	 * 
	 * @return	flag has allegati
	 */
	public Boolean getHasAllegati() {
		return hasAllegati;
	}

	/**
	 * Setter.
	 * 
	 * @param inHasAllegati	flag has allegati
	 */
	public void setHasAllegati(final Boolean inHasAllegati) {
		this.hasAllegati = inHasAllegati;
	}

	/**
	 * Getter.
	 * 
	 * @return	guid
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * Setter.
	 * 
	 * @param inGuid	guid
	 */
	public void setGuid(final String inGuid) {
		this.guid = inGuid;
	}

	/**
	 * Gets the motivo eliminazione.
	 *
	 * @return the motivoEliminazione
	 */
	public String getMotivoEliminazione() {
		return motivoEliminazione;
	}

	/**
	 * Getter.
	 * 
	 * @return	stato
	 */
	public StatoMailEnum getStato() {
		return stato;
	}

	/**
	 * Setter.
	 * 
	 * @param inStato	stato
	 */
	public void setStato(final StatoMailEnum inStato) {
		this.stato = inStato;
	}

	/**
	 * Getter.
	 * 
	 * @return	identificativo utente protocollatore
	 */
	public Integer getIdUtenteProtocollatore() {
		return idUtenteProtocollatore;
	}

	/**
	 * Setter identificativo utente protocollatore.
	 * 
	 * @param inIdUtenteProtocollatore	identificativo utente protocollatore
	 */
	public void setIdUtenteProtocollatore(final Integer inIdUtenteProtocollatore) {
		this.idUtenteProtocollatore = inIdUtenteProtocollatore;
	}

	/**
	 * Getter.
	 * 
	 * @return	descrizione utente protocollatore
	 */
	public String getDescUtenteProtocollatore() {
		return descUtenteProtocollatore;
	}

	/**
	 * Setter.
	 * 
	 * @param inDescUtenteProtocollatore	descrizione utente protocollatore
	 */
	public void setDescUtenteProtocollatore(final String inDescUtenteProtocollatore) {
		this.descUtenteProtocollatore = inDescUtenteProtocollatore;
	}

	/**
	 * Getter.
	 * 
	 * @return Allegati della mail
	 */
	public List<String> getIdAllegati() {
		return idAllegati;
	}

	/**
	 * Setter.
	 * 
	 * @param allegati Allegati della mail
	 */
	public void setIdAllegati(final List<String> idAllegati) {
		this.idAllegati = idAllegati;
	}

	/**
	 * Recupera la data di invio della mail.
	 * @return data di invio della mail
	 */
	public Date getDataInvio() {
		return dataInvio;
	}

	/**
	 * Imposta la data di invio della mail.
	 * @param dataInvio data di invio della mail
	 */
	public void setDataInvio(final Date dataInvio) {
		this.dataInvio = dataInvio;
	}

	/**
	 * Recupera la data di cambio stato.
	 * @return data di cambio stato
	 */
	public Date getDataCambioStato() {
		return dataCambioStato;
	}

	/**
	 * Imposta la data di cambio stato.
	 * @param dataCambioStato data di cambio stato
	 */
	public void setDataCambioStato(final Date dataCambioStato) {
		this.dataCambioStato = dataCambioStato;
	}

	/**
	 * Recupera lo stato della notifica.
	 * @return stato della notifica
	 */
	public String getStatoNotifica() {
		return statoNotifica;
	}

	/**
	 * Imposta lo stato della notifica.
	 * @param statoNotifica stato della notifica
	 */
	public void setStatoNotifica(final String statoNotifica) {
		this.statoNotifica = statoNotifica;
	}

	/**
	 * Recupera l'enum associato all'immagine nella mail.
	 * @return enum associato all'immagine nella mail
	 */
	public ImageMailEnum getImageMailEnum() {
		return imageMailEnum;
	}

	/**
	 * Imposta l'enum associato all'immagine nella mail.
	 * @param imageMailEnum enum associato all'immagine nella mail
	 */
	public void setImageMailEnum(final ImageMailEnum imageMailEnum) {
		this.imageMailEnum = imageMailEnum;
	}

	/**
	 * Recupera l'attributo reinvioMail.
	 * @return reinvioMail
	 */
	public boolean isReinvioMail() {
		return reinvioMail;
	}

	/**
	 * Imposta l'attributo reinvioMail.
	 * @param reinvioMail
	 */
	public void setReinvioMail(final boolean reinvioMail) {
		this.reinvioMail = reinvioMail;
	}

	/**
	 * Recupera mittente e destinatari temp.
	 * @return mittente e destinatari temp
	 */
	public String getMittenteDestinatariTemp() {
		return mittenteDestinatariTemp;
	}

	/**
	 * Imposta mittente e destinatari temp.
	 * @param mittenteDestinatariTemp mittente e destinatari temp
	 */
	public void setMittenteDestinatariTemp(final String mittenteDestinatariTemp) {
		this.mittenteDestinatariTemp = mittenteDestinatariTemp;
	}

	/**
	 * Recupera lo stato pre archiviazione.
	 * @return stato pre archiviazione
	 */
	public StatoMailEnum getStatoPreArchiviazione() {
		return statoPreArchiviazione;
	}

	/**
	 * Imposta lo stato pre archiviazione.
	 * @param statoPreArchiviazione stato pre archiviazione
	 */
	public void setStatoPreArchiviazione(final StatoMailEnum statoPreArchiviazione) {
		this.statoPreArchiviazione = statoPreArchiviazione;
	}
	
	/**
	 * Recupera la nota.
	 * @return nota
	 */
	public NotaDTO getNota() {
		return nota;
	}

	/**
	 * Imposta la nota.
	 * 
	 * @param nota nota
	 */
	public void setNota(final NotaDTO nota) {
		this.nota = nota;
	}

	/**
	 * Recupera i protocolli generati.
	 * @return lista dei protocolli generati
	 */
	public List<String> getProtocolliGenerati() {
		return protocolliGenerati;
	}
	
	/**
	 * Recupera i protocolli generati.
	 * @return lista dei protocolli generati
	 */
	public String getProtocolliGeneratiPlain() {
		String returnString="";
		if(protocolliGenerati!=null) {
			int x=0;
			for(String s: protocolliGenerati) {
				if(x==0) {
					returnString+=s;
				}else {
					returnString+=", "+s;
				}
				x++;
			}
		}
		return returnString;
	}

	/**
	 * Imposta i protocolli generati.
	 * @param protocolliGenerati lista dei protocolli generati
	 */
	public void setProtocolliGenerati(final List<String> protocolliGenerati) {
		this.protocolliGenerati = protocolliGenerati;
	}

	/**
	 * @see java.lang.Object#hashCode().
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((guid == null) ? 0 : guid.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object).
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final EmailDTO other = (EmailDTO) obj;
		if (guid == null) {
			if (other.guid != null) {
				return false;
			}
		} else if (!guid.equals(other.guid)) {
			return false;
		}
		
		return true;
	}

	/**
	 * Recupera il colore del flag interoperabilità.
	 * @return colore del flag interoperabilità
	 */
	public ColoreInteroperabilitaEnum getColoreInteroperabilita() {
		return coloreInteroperabilita;
	}
	
	/**
	 * Imposta il colore del flag interoperabilità.
	 * @param coloreInteroperabilita colore del flag interoperabilità
	 */
	public void setColoreInteroperabilita(final ColoreInteroperabilitaEnum coloreInteroperabilita) {
		this.coloreInteroperabilita = coloreInteroperabilita;
	}
	
	/**
	 * Recupera l'esito della validazione dell'interoperabilità.
	 * @return esito della validazione dell'interoperabilità
	 */
	public EsitoValidazioneSegnaturaInteropEnum getEsitoValidazioneEnum() {
		return esitoValidazioneEnum;
	}
	
	/**
	 * Imposta l'esito della validazione dell'interoperabilità.
	 * @param esitoValidazioneEnum esito della validazione dell'interoperabilità
	 */
	public void setEsitoValidazioneEnum(final EsitoValidazioneSegnaturaInteropEnum esitoValidazioneEnum) {
		this.esitoValidazioneEnum = esitoValidazioneEnum;
	}
	
	/**
	 * Recupera il colore del flag preassegnatario.
	 * @return colore del flag preassegnatario
	 */
	public ColoreInteroperabilitaEnum getColorePreassegnatario() {
		return colorePreassegnatario;
	}
	
	/**
	 * Imposta il colore del flag preassegnatario.
	 * @param colorePreassegnatario colore del flag preassegnatario
	 */
	public void setColorePreassegnatario(final ColoreInteroperabilitaEnum colorePreassegnatario) {
		this.colorePreassegnatario = colorePreassegnatario;
	} 
 
	/**
	 * Recupera il tooltip preassegnatario.
	 * @return tooltip preassegnatario
	 */
	public String getTooltipPreassegnatario() {
		return tooltipPreassegnatario;
	}
	
	/**
	 * Imposta il tooltip preassegnatario.
	 * @param tooltipPreassegnatario tooltip preassegnatario
	 */
	public void setTooltipPreassegnatario(final String tooltipPreassegnatario) {
		this.tooltipPreassegnatario = tooltipPreassegnatario;
	}
	
	/**
	 * Recupera il preassegnatario dell'interoperabilità.
	 * @return preassegnatario dell'interoperabilità
	 */
	public AssegnatarioInteropDTO getPreassegnatarioInterop() {
		return preassegnatarioInterop;
	}
	
	/**
	 * Imposta il preassegnatario dell'interoperabilità.
	 * @param preassegnatarioInterop preassegnatario dell'interoperabilità
	 */
	public void setPreassegnatarioInterop(final AssegnatarioInteropDTO preassegnatarioInterop) {
		this.preassegnatarioInterop = preassegnatarioInterop;
	}
	
	/**
	 * Recupera l'attributo isFromArchiviate.
	 * @return isFromArchiviate
	 */
	public boolean getIsFromArchiviate() {
		return isFromArchiviate;
	}
	
	/**
	 * Imposta attributo isFromArchiviate.
	 * @param isFromArchiviate
	 */
	public void setIsFromArchiviate(final boolean isFromArchiviate) {
		this.isFromArchiviate = isFromArchiviate;
	}
	
	/**
	 * Recupera l'operazione di posta elettronica.
	 * @return operazione di posta elettronica
	 */
	public String getOperazionePostaElettronica() {
		return operazionePostaElettronica;
	}
	
	/**
	 * Imposta l'operazione di posta elettronica.
	 * 
	 * @param operazionePostaElettronica operazione di posta elettronica
	 */
	public void setOperazionePostaElettronica(final String operazionePostaElettronica) {
		this.operazionePostaElettronica = operazionePostaElettronica;
	}
	
	/**
	 * Recupera le operazioni di posta elettronica.
	 * @return lista delle operazioni di posta elettronica
	 */
	public List<String> getListOperazionePostaElettronica() {
		return listOperazionePostaElettronica;
	}
	
	/**
	 * Imposta le operazioni di posta elettronica.
	 * @param listOperazionePostaElettronica lista delle operazioni di posta elettronica
	 */
	public void setListOperazionePostaElettronica(final List<String> listOperazionePostaElettronica) {
		this.listOperazionePostaElettronica = listOperazionePostaElettronica;
	} 
	
	/**
	 * Recupera la folder mail inbox.
	 * @return folder mail inbox
	 */
	public String getFolderMailInbox() {
		return folderMailInbox;
	}
	
	/**
	 * Imposta la folder mail inbox.
	 * @param folderMailInbox folder mail inbox
	 */
	public void setFolderMailInbox(final String folderMailInbox) {
		this.folderMailInbox = folderMailInbox;
	}
	
	/**
	 * Recupera la folder mail archiviate.
	 * @return folder mail archiviate
	 */
	public String getFolderMailArchiviate() {
		return folderMailArchiviate;
	}
	
	/**
	 * Imposta la folder mail archiviate.
	 * @param folderMailArchiviate folder mail archiviate
	 */
	public void setFolderMailArchiviate(final String folderMailArchiviate) {
		this.folderMailArchiviate = folderMailArchiviate;
	}
	 
	/**
	 * Recupera l'attributo attivaRipristinaResponse.
	 * 
	 * @return attivaRipristinaResponse
	 */
	public boolean getAttivaRipristinaResponse() {
		return attivaRipristinaResponse;
	}
	
	/**
	 * Imposta l'attributo attivaRipristinaResponse.
	 * 
	 * @param attivaRipristinaResponse
	 */
	public void setAttivaRipristinaResponse(final boolean attivaRipristinaResponse) {
		this.attivaRipristinaResponse = attivaRipristinaResponse;
	}

	/**
	 * Recupera la data di scarico della mail.
	 * @return data di scarico della mail
	 */
	public Date getDataScarico() {
		return dataScarico;
	}

	/**
	 * Imposta la data di scarico della mail.
	 * @param dataScarico data di scarico della mail
	 */
	public void setDataScarico(final Date dataScarico) {
		this.dataScarico = dataScarico;
	}

	/**
	 * Recupera il document title.
	 * @return document title
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Imposta il document title.
	 * @param documentTitle document title
	 */
	public void setDocumentTitle(final String documentTitle) {
		this.documentTitle = documentTitle;
	}

	/**
	 * @return the iconaStato
	 */
	public IconaStatoEnum getIconaStato() {
		return iconaStato;
	}

	/**
	 * @param iconaStato the iconaStato to set
	 */
	public void setIconaStato(IconaStatoEnum iconaStato) {
		this.iconaStato = iconaStato;
	}

	/**
	 * Restituisce il test html.
	 * @return testo html
	 */
	public String getTestoHTML() {
		return testoHTML;
	}

	/**
	 * Imposta il testo html nella variabile della classe {@link #testoHTML}.
	 * @param testoHTML
	 */
	public void setTestoHTML(String testoHTML) {
		this.testoHTML = testoHTML;
	}

	public boolean isProtocollaEMantieni() {
		return protocollaEMantieni;
	}

	public void setProtocollaEMantieni(boolean protocollaEMantieni) {
		this.protocollaEMantieni = protocollaEMantieni;
	}
	
}