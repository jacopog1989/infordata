package it.ibm.red.business.enums;

/**
 * Enum che definisce la fase di lavorazione.
 */
public enum FlagLavoratoEnum {

	/**
	 * Valore.
	 */
	DA_LAVORARE(0, "DA LAVORARE"),
	
	/**
	 * Valore.
	 */
	IN_ELABORAZIONE(1, "IN ELABORAZIONE"),
	
	/**
	 * Valore.
	 */
	LAVORATO(2, "LAVORATO"),
	
	/**
	 * Valore.
	 */
	OBSOLETO(3, "OBSOLETO"),
	
	/**
	 * Valore.
	 */
	IN_ERRORE(4, "IN ERRORE");
	
	/**
	 * 	id stato flag.
	 */
	private int id;
	
	/**
	 * nome stato flag
	 */
	private String stato;

	/**
	 * Costruttore dell'enum.
	 * @param id
	 * @param stato
	 */
	FlagLavoratoEnum(final int id, final String stato) {
		this.id = id;
		this.stato = stato;
	}

	/**
	 * Restituisce l'id dell'Enum.
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Imposta l'id dell'Enum.
	 * @param id
	 */
	protected void setId(final int id) {
		this.id = id;
	}

	/**
	 * Restituisce lo stato associato all'Enum.
	 * @return stato
	 */
	public String getStato() {
		return stato;
	}

	/**
	 * Imposta lo stato associato all'Enum.
	 * @param stato
	 */
	protected void setStato(final String stato) {
		this.stato = stato;
	}
}
