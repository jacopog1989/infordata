package it.ibm.red.business.dto;

import java.util.Date;

/**
 * @author SLac
 *
 */
public class TitolarioDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1240675242553149575L;

	/**
	 * Indice classificazione.
	 */
	private String indiceClassificazione;

	/**
	 * Indice classificazione padre.
	 */
	private String indiceClassificazionePadre;
	
	/**
	 * Descrizione.
	 */
	private String descrizione;

	/**
	 * Livello.
	 */
	private int livello;

	/**
	 * Aoo.
	 */
	private Long idAOO;

	/**
	 * Data attivazione.
	 */
	private Date dataAttivazione;

	/**
	 * Data disattivazione.
	 */
	private Date dataDisattivazione;

	/**
	 * Numero figli.
	 */
	private int numeroFigli;
	
	/**
	 * 
	 */
	public TitolarioDTO() { }

	/**
	 * @param indiceClassificazione
	 * @param indiceClassificazionePadre
	 * @param descrizione
	 * @param livello
	 * @param idAOO
	 * @param dataAttivazione
	 * @param dataDisattivazione
	 */
	public TitolarioDTO(final String indiceClassificazione, final String indiceClassificazionePadre, final String descrizione, 
			final int livello, final Long idAOO, final Date dataAttivazione, final Date dataDisattivazione, final int numeroFigli) { 
		this.indiceClassificazione = indiceClassificazione; 
		this.indiceClassificazionePadre = indiceClassificazionePadre; 
		this.descrizione = descrizione; 
		this.livello = livello; 
		this.idAOO = idAOO;
		this.dataAttivazione = dataAttivazione;
		this.dataDisattivazione = dataDisattivazione;
		this.numeroFigli = numeroFigli;
	}
	
	/** GET & SET.**************************************************************************************************/
	/**
	 * Restituisce l'indice di classificazione.
	 * @return indice classificazione
	 */
	public String getIndiceClassificazione() {
		return indiceClassificazione;
	}

	/**
	 * Restituisce l'indice di classificazione padre.
	 * @return indice di classificazione padre
	 */
	public String getIndiceClassificazionePadre() {
		return indiceClassificazionePadre;
	}

	/**
	 * Restituisce la descrizione.
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Restituisce il livello.
	 * @return livello
	 */
	public int getLivello() {
		return livello;
	}

	/**
	 * Restituisce l'id dell'AOO.
	 * @return id AOO
	 */
	public Long getIdAOO() {
		return idAOO;
	}

	/**
	 * Restituisce la data attivazione.
	 * @return data attivazione
	 */
	public Date getDataAttivazione() {
		return dataAttivazione;
	}

	/**
	 * Restituisce la data disattivazione.
	 * @return data disattivazione
	 */
	public Date getDataDisattivazione() {
		return dataDisattivazione;
	}

	/**
	 * Restituisce numero figli.
	 * @return numero figli
	 */
	public int getNumeroFigli() {
		return numeroFigli;
	}

	/**
	 * Imposta il numero figli.
	 * @param inNumeroFigli
	 */
	public void setNumeroFigli(final int inNumeroFigli) {
		this.numeroFigli = inNumeroFigli;
	}
	
	
}
