package it.ibm.red.business.service.concrete;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.fileupload.FileItem;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;
import com.filenet.api.property.Properties;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.xml.AbstractXmlHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.IProtocollaPECPCCSRV;
import it.ibm.red.business.utils.FileUtils;

/**
 * Service che gestisce la funzionalità di protocollazione PEC PCC.
 */
@Service
@Component
public class ProtocollaPECPCCSRV extends ProtocollaPECAbstractSRV implements IProtocollaPECPCCSRV {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = 5072215933806523575L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ProtocollaPECPCCSRV.class.getName());
	
	/**
	 * @see it.ibm.red.business.service.IProtocollaPECSRV#extractMailAttachments(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      com.filenet.api.core.Document, it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.DetailDocumentRedDTO,
	 *      it.ibm.red.business.helper.xml.AbstractXmlHelper).
	 */
	@Override
	public void extractMailAttachments(final IFilenetCEHelper fceh, final Document email, final UtenteDTO utente, final DetailDocumentRedDTO documento, final AbstractXmlHelper helper) {
		FileItem primoAllegatoMail = null;
		
		try {
			primoAllegatoMail = extractAllegato(fceh, email, utente, null);
			populateDocumento(documento, email, primoAllegatoMail, utente);
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di estrazione degli allegati della mail " + email.get_Id().toString(), e);
			throw new RedException(e);
		} finally {
			if (primoAllegatoMail != null) {
				primoAllegatoMail.delete();
			}
		}
	}
	
	/**
	 * Recupera l'allegato della email. In caso sia un p7m, sbusta ricorsivamente e restituisce il suo contenuto.
	 * 
	 * @param fceh
	 * @param mail
	 * @param utente
	 * @param nomeAllegato
	 * @return
	 */
	private FileItem extractAllegato(final IFilenetCEHelper fceh, final Document mail, final UtenteDTO utente, final String nomeAllegato) {
		final FileItem fi = extractAllegato(fceh, mail, nomeAllegato);
		return FileUtils.p7mExtract(fi, getDiskFileItemFactory(), utente.getSignerInfo().getPkHandlerVerifica().getHandler(), 
				utente.getSignerInfo().getPkHandlerVerifica().getSecurePin(), utente.getDisableUseHostOnly());
	}
	
	/**
	 * Costruisce il documento, comprensivo di metadati e allegato, da registrare su Filenet.
	 * 
	 * @param documento
	 * @param email
	 * @param allegatoMail
	 * @throws IOException
	 */
	private void populateDocumento(final DetailDocumentRedDTO documento, final Document email, final FileItem allegatoMail, final UtenteDTO utente) throws IOException {
		
		documento.setDocumentClass(getPP().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));
		documento.setMimeType(allegatoMail.getContentType());
		documento.setContent(FileUtils.getByteFromInputStream(allegatoMail.getInputStream()));
		
		final Properties propMail = email.getProperties();
		
		documento.setNomeFile(allegatoMail.getName());
		documento.setOggetto(propMail.getStringValue(getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_EMAIL_METAKEY)));
		documento.setMittente(propMail.getStringValue(getPP().getParameterByKey(PropertiesNameEnum.FROM_MAIL_METAKEY)));
		documento.setTipoProtocollo(Constants.Protocollo.TIPO_PROTOCOLLO_ENTRATA);
		
		final AllegatoDTO allegato = new AllegatoDTO();
		allegato.setMimeType(Constants.ContentType.TEXT);
		allegato.setContent(FileUtils.toByteArrayHtml(email.accessContentStream(0)));
		
		allegato.setNomeFile(Constants.Mail.TESTO_MAIL_HTML);
		allegato.setOggetto(Constants.Mail.TESTO_MAIL_HTML);
		allegato.setDocumentTitle(Constants.Mail.TESTO_MAIL_HTML);
		allegato.setFormato(FormatoAllegatoEnum.ELETTRONICO.getId());
		allegato.setFormatoOriginale(utente.getMantieniFormatoOriginale() == 1);
						
		final List<AllegatoDTO> allegati = new ArrayList<>();
		allegati.add(allegato);
		
		documento.setAllegati(allegati);
	
	}

}