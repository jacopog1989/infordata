/**
 * 
 */
package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.util.Base64;

import org.springframework.web.util.JavaScriptUtils;

import it.ibm.red.business.logger.REDLogger;

import com.filenet.api.core.Document;
import com.google.common.net.MediaType;

import it.ibm.red.business.dto.DocumentAppletContentDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.utils.FileUtils;

/**
 * Trasformer documento generic Doc
 * 
 * @author APerquoti
 */
public class FromDocumentoToAppletContent extends TrasformerCE<DocumentAppletContentDTO> {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 7200912184718179392L;
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FromDocumentoToAppletContent.class.getName());
	
	/**
	 * Costruttore.
	 */
	public FromDocumentoToAppletContent() {
		super(TrasformerCEEnum.FROM_DOCUMENTO_TO_APPLET_CONTENT);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE#trasform(com.filenet.api.core.Document, java.sql.Connection).
	 */
	@Override
	public DocumentAppletContentDTO trasform(final Document document, final Connection connection) {
		try {
			String nomeFile = JavaScriptUtils.javaScriptEscape((String) getMetadato(document, PropertiesNameEnum.NOME_FILE_METAKEY));
			String mimeType = document.get_MimeType();
			byte[] bytes = FileUtils.getByteFromInputStream(document.accessContentStream(0));
			String strBase64 = new String(Base64.getEncoder().encode(bytes));
			String documentBase64 = URLEncoder.encode(strBase64, StandardCharsets.UTF_8.name());
			
			// Se il file è un DOCX, imponiamo il MIME Type "application/msword" per far funzionare l'applet
			if (nomeFile.endsWith("x")) {
				mimeType = MediaType.MICROSOFT_WORD.toString();
			}
			
			return new DocumentAppletContentDTO(nomeFile, mimeType, documentBase64);
		} catch (Exception e) {
			LOGGER.error("Errore nel recupero di un metadato durante la trasformazione da Document a FromDocumentoToAppletContent : ", e);
			return null;
		}
	}
	
}
