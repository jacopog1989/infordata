/**
 * 
 */
package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

import it.ibm.red.business.dto.AllaccioDocUscitaDTO;

/**
 * @author VINGENITO
 *
 */
public interface IAllaccioDocUscitaDAO extends Serializable {

	/**
	 * Inserisce il documento nella tabella del riferimento storico.
	 * 
	 * @param docTitleUscita
	 *            - document title in uscita
	 * @param nProtocolloRifStorico
	 *            - numero di protocollo del riferimento storico
	 * @param idDocPrincipale
	 *            - id documento principale
	 * @param stato
	 * @param errorMsg
	 *            - messaggio di errore
	 * @param dataCreazione
	 *            - data di creazione
	 * @param dataAggiornamento
	 *            - data di aggiornamento
	 * @param conn
	 * @return Esito dell'update.
	 */
	int insertRiferimentoStorico(String docTitleUscita, String nProtocolloRifStorico, String idDocPrincipale, Integer stato, String errorMsg, Date dataCreazione, Date dataAggiornamento, Connection conn);

	/**
	 * Aggiorna lo stato.
	 * 
	 * @param docTitleUscita
	 *            - document title in uscita
	 * @param stato
	 * @param errorMsg
	 *            - messaggio di errore
	 * @param idAoo
	 *            - id dell'Aoo
	 * @param conn
	 * @return Esito dell'update.
	 */
	int updateStato(String docTitleUscita, Integer stato, String errorMsg, Integer idAoo, Connection conn);

	/**
	 * Recupera i documenti.
	 * 
	 * @param stato
	 * @param idAoo
	 *            - id dell'Aoo
	 * @param conn
	 * @return Document title del documento da processare.
	 */
	String selectDocumentTileToProcess(Integer stato, Integer idAoo, Connection conn);

	/**
	 * Aggiorna lo stato.
	 * 
	 * @param docTitleUscita
	 *            - document title in uscita
	 * @param docTitleIngresso
	 *            - document title in ingresso
	 * @param statoFinale
	 *            - stato finale
	 * @param statoIniziale
	 *            - stato iniziale
	 * @param errorMsg
	 *            - messaggio di errore
	 * @param idAoo
	 *            - id dell'Aoo
	 * @param conn
	 * @return Esito dell'update.
	 */
	int updateStato(String docTitleUscita, String docTitleIngresso, Integer statoFinale, Integer statoIniziale,
			String errorMsg, Integer idAoo, Connection conn);

	/**
	 * Aggiorna lo stato.
	 * 
	 * @param docTitleUscita
	 *            - document title in uscita
	 * @param docTitleIngresso
	 *            - document title in ingresso
	 * @param statoFinale
	 *            - stato finale
	 * @param errorMsg
	 *            - messaggio di errore
	 * @param idAoo
	 *            - id dell'Aoo
	 * @param conn
	 * @return Esito dell'update.
	 */
	int updateStato(String docTitleUscita, String docTitleIngresso, Integer statoFinale, String errorMsg, Integer idAoo,
			Connection conn);

	/**
	 * Ottiene i documenti.
	 * 
	 * @param documentTitleUscita
	 *            - document title in uscita
	 * @param stato
	 * @param idAoo
	 *            - id dell'Aoo
	 * @param conn
	 * @return lista dei documenti
	 */
	List<AllaccioDocUscitaDTO> getAllaccioDocUscitaByAoo(String documentTitleUscita, Integer stato, Integer idAoo, Connection conn);

	/**
	 * Inserisce il documento allacciato
	 * 
	 * @param docTitleUscita
	 *            - document title in uscita
	 * @param docTitleIngresso
	 *            - doc title in ingresso
	 * @param stato
	 * @param errorMsg
	 *            - messaggio di errore
	 * @param dataCreazione
	 *            - data di creazione
	 * @param dataAggiornamento
	 *            - data di aggiornamento
	 * @param idAOO
	 *            - id dell'Aoo
	 * @param conn
	 * @return Esito della insert.
	 */
	int insertDocEstendiVisibilita(String docTitleUscita, String docTitleIngresso, Integer stato, String errorMsg,
			Date dataCreazione, Date dataAggiornamento, Integer idAOO, Connection conn);
}
