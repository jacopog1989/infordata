package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.filenet.api.core.Document;

import it.ibm.red.business.constants.Constants.TipoDestinatario;
import it.ibm.red.business.dao.ILookupDAO;
import it.ibm.red.business.dao.IMetadatoDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.ITipologiaDocumentoDAO;
import it.ibm.red.business.dto.DestinatarioDTO;
import it.ibm.red.business.dto.DetailDocumentoDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.RegistrazioneAusiliariaDTO;
import it.ibm.red.business.dto.filenet.StoricoDTO;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;
import it.ibm.red.business.enums.TrasformazionePDFInErroreEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.helper.metadatiestesi.MetadatiEstesiHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IRegistrazioniAusiliarieSRV;
import it.ibm.red.business.service.facade.IStoricoFacadeSRV;
import it.ibm.red.business.utils.DestinatariRedUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class FromDocumentoToDetailTrasformer.
 *
 * @author CPIERASC
 * 
 *         Trasformer dettaglio documento.
 */
public class FromDocumentoToDetailTrasformer extends TrasformerCE<DetailDocumentoDTO> {

	/**
	 * Posizione tipo destinario.
	 */
	private static final int TIPO_DEST_POSITION = 3;

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 7115280317344104212L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FromDocumentoToDetailTrasformer.class.getName());

	/**
	 * Dao lookup.
	 */
	private final ILookupDAO lookupDAO;

	/**
	 * Dao nodo.
	 */
	private final INodoDAO nodoDAO;

	/**
	 * Dao storico.
	 */
	private final IStoricoFacadeSRV storicoSRV;

	/**
	 * DAO per recuperare la tipologia di documento.
	 */
	private final ITipologiaDocumentoDAO tipologiaDocumentoDAO;

	/**
	 * DAO gestione metadati estesi.
	 */
	private final IMetadatoDAO metadatoDAO;

	/**
	 * Service.
	 */
	private final IRegistrazioniAusiliarieSRV registrazioniAusiliarieSRV;

	/**
	 * Costruttore.
	 */
	public FromDocumentoToDetailTrasformer() {
		super(TrasformerCEEnum.FROM_DOCUMENTO_TO_DETAIL);
		lookupDAO = ApplicationContextProvider.getApplicationContext().getBean(ILookupDAO.class);
		nodoDAO = ApplicationContextProvider.getApplicationContext().getBean(INodoDAO.class);
		storicoSRV = ApplicationContextProvider.getApplicationContext().getBean(IStoricoFacadeSRV.class);
		tipologiaDocumentoDAO = ApplicationContextProvider.getApplicationContext().getBean(ITipologiaDocumentoDAO.class);
		metadatoDAO = ApplicationContextProvider.getApplicationContext().getBean(IMetadatoDAO.class);
		registrazioniAusiliarieSRV = ApplicationContextProvider.getApplicationContext().getBean(IRegistrazioniAusiliarieSRV.class);
	}

	/**
	 * Trasform.
	 *
	 * @param document   the document
	 * @param connection the connection
	 * @return the detail documento DTO
	 */
	@Override
	public final DetailDocumentoDTO trasform(final Document document, final Connection connection) {
		try {
			final String guid = StringUtils.cleanGuidToString(document.get_Id());
			final Integer inNumeroDocumento = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
			final Integer formatoAllegatoId = (Integer) getMetadato(document, PropertiesNameEnum.FORMATO_ALLEGATO_ID_METAKEY);
			final Integer idUtenteCreatore = (Integer) getMetadato(document, PropertiesNameEnum.UTENTE_CREATORE_ID_METAKEY);

			final Integer idUfficioCreatore = (Integer) getMetadato(document, PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY);
			final String inUfficioMittente = nodoDAO.getNodo(idUfficioCreatore.longValue(), connection).getDescrizione();

			final String iterApprovativoSemaforo = (String) getMetadato(document, PropertiesNameEnum.ITER_APPROVATIVO_METAKEY);
			final String inIterApprovativo = lookupDAO.getIterApprovativo(iterApprovativoSemaforo, connection);

			final Long idTipoDocumento = Long.valueOf((Integer) getMetadato(document, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY));
			final String inTipologiaDocumento = lookupDAO.getDescTipoDocumento(idTipoDocumento, connection);

			final Long idTipologiaProcedimento = Long.valueOf((Integer) getMetadato(document, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY));

			final String inOggetto = (String) getMetadato(document, PropertiesNameEnum.OGGETTO_METAKEY);
			final String inDocumentTitle = (String) getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
			final Integer inAnnoProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
			final Integer inNumeroProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
			final String inIdProtocollo = (String) getMetadato(document, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY);
			final Date inDataProtocollo = (Date) getMetadato(document, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY);

			final Integer attachment = document.get_CompoundDocumentState().getValue();

			final Integer trasfPdfErrore = (Integer) getMetadato(document, PropertiesNameEnum.TRASFORMAZIONE_PDF_ERRORE_METAKEY);
			Boolean bTrasfPdfErrore = false;
			Boolean bTrasfPdfWarning = false;
			if (trasfPdfErrore != null) {
				bTrasfPdfErrore = TrasformazionePDFInErroreEnum.getErrorCodes().contains(trasfPdfErrore);
				bTrasfPdfWarning = TrasformazionePDFInErroreEnum.getWarnCodes().contains(trasfPdfErrore);
			}

			// gestione del metadato firmaPDF
			Boolean flagFirmaPDF = (Boolean) getMetadato(document, PropertiesNameEnum.FIRMA_PDF_METAKEY);
			if (flagFirmaPDF == null) {
				flagFirmaPDF = true;
			}

			final Integer idCategoriaDocumento = (Integer) getMetadato(document, PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY);
			final String nomeFile = (String) getMetadato(document, PropertiesNameEnum.NOME_FILE_METAKEY);
			final Boolean flagFirmaAutografaRM = (Boolean) getMetadato(document, PropertiesNameEnum.FLAG_FIRMA_AUTOGRAFA_RM_METAKEY);
			final Date dataCreazione = (Date) getMetadato(document, PropertiesNameEnum.DATA_CREAZIONE_METAKEY);
			final Date dataScadenza = (Date) getMetadato(document, PropertiesNameEnum.DATA_SCADENZA_METAKEY);
			final Integer urgenza = (Integer) getMetadato(document, PropertiesNameEnum.URGENTE_METAKEY);
			Boolean flagUrgenza = false;
			if (urgenza != null && urgenza == 1) {
				flagUrgenza = true;
			}

			// ### DESTINATARI
			// #########################################################################################
			final Collection<?> metadatoDestinatari = (Collection<?>) getMetadato(document, PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY);

			// Si normalizza il metadato per evitare errori comuni durante lo split delle
			// singole stringhe dei destinatari
			final List<String[]> destinatariDocumento = DestinatariRedUtils.normalizzaMetadatoDestinatariDocumento(metadatoDestinatari);

			final Collection<String> inDestinatariClean = new ArrayList<>();
			final List<DestinatarioDTO> inDestinatariDocumento = new ArrayList<>();
			boolean hasDestinatariDocEsterni = false;
			boolean hasDestinatariDocInterni = false;
			String modalitaDestinatario = null;

			for (final String[] destSplit : destinatariDocumento) {

				final String tipoDestinatario = destSplit[TIPO_DEST_POSITION];
				String descrizione = null;
				Integer tipoSpedizione = -1;
				Long id = null;
				Long idUfficio = null;

				if (TipoDestinatario.ESTERNO.equalsIgnoreCase(tipoDestinatario)) {

					hasDestinatariDocEsterni = true;
					id = Long.parseLong(destSplit[0]);
					descrizione = destSplit[1];
					tipoSpedizione = Integer.parseInt(destSplit[2]);

				} else if (TipoDestinatario.INTERNO.equalsIgnoreCase(tipoDestinatario)) {

					hasDestinatariDocInterni = true;
					idUfficio = Long.parseLong(destSplit[0]);
					id = Long.parseLong(destSplit[1]);
					descrizione = destSplit[2];

				}

				inDestinatariClean.add(descrizione);

				modalitaDestinatario = destSplit[4];

				final DestinatarioDTO destinatarioDoc = new DestinatarioDTO(id, descrizione, tipoDestinatario, null, null, tipoSpedizione,
						ModalitaDestinatarioEnum.CC.name().equals(modalitaDestinatario), idUfficio);

				inDestinatariDocumento.add(destinatarioDoc);

			}
			// ###################################################################################################################
			final Integer idMomentoProtocollazione = (Integer) getMetadato(document, PropertiesNameEnum.MOMENTO_PROTOCOLLAZIONE_ID_METAKEY);

			final Integer idAOO = (Integer) getMetadato(document, PropertiesNameEnum.ID_AOO_METAKEY);
			int idAoo = 0;
			if (idAOO != null) {
				idAoo = idAOO;
			}
			final Collection<StoricoDTO> storico = storicoSRV.getStorico(Integer.parseInt(inDocumentTitle), idAoo);
			final Integer riservato = (Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.RISERVATO_METAKEY);
			Boolean flagRiservato = false;
			if (riservato != null && riservato > 0) {
				flagRiservato = true;
			}
			final String descrizioneTipoDocumentoNps = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.DESCRIZIONE_TIPOLOGIA_DOCUMENTO_NPS_METAKEY);
			final String oggettoProtocolloNps = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.OGGETTO_PROTOCOLLO_NPS_METAKEY);

			final String mimeType = document.get_MimeType();

			final String codiceFlusso = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY);

			final Integer sottoCategoriaUscitaId = (Integer) getMetadato(document, PropertiesNameEnum.SOTTOCATEGORIA_DOCUMENTO_USCITA);
			SottoCategoriaDocumentoUscitaEnum sottoCategoriaUscita = null;
			if (sottoCategoriaUscitaId != null) {
				sottoCategoriaUscita = SottoCategoriaDocumentoUscitaEnum.get(sottoCategoriaUscitaId);
			}

			/*************************************
			 * METADATI ESTESI
			 ********************************************/
			Collection<MetadatoDTO> metadatiEstesi = null;
			if (idTipoDocumento != null) {
				final String xmlMetadatiEstesi = tipologiaDocumentoDAO.recuperaMetadatiEstesi(connection, Integer.parseInt(inDocumentTitle), Long.valueOf(idAoo),
						idTipoDocumento, idTipologiaProcedimento);
				if (!StringUtils.isNullOrEmpty(xmlMetadatiEstesi)) {

					// Si recuperano i metadati associati alla tipologia documento
					final List<MetadatoDTO> metadatiTipologiaDocumento = metadatoDAO.caricaMetadati(idTipoDocumento.intValue(), idTipologiaProcedimento.intValue(),
							connection);

					if (!CollectionUtils.isEmpty(metadatiTipologiaDocumento)) {
						metadatiEstesi = MetadatiEstesiHelper.deserializeMetadatiEstesi(xmlMetadatiEstesi, metadatiTipologiaDocumento);
					}
				}
			}

			final RegistrazioneAusiliariaDTO registrazioneAusiliaria = registrazioniAusiliarieSRV.getRegistrazioneAusiliaria(inDocumentTitle, idAoo, connection);

			return new DetailDocumentoDTO(guid, inNumeroProtocollo, inAnnoProtocollo, inIdProtocollo, inDataProtocollo, descrizioneTipoDocumentoNps, oggettoProtocolloNps,
					inDocumentTitle, inNumeroDocumento, inTipologiaDocumento, inOggetto, null, inUfficioMittente, idUfficioCreatore, 
					idUtenteCreatore, inIterApprovativo, inDestinatariClean, inDestinatariDocumento, null, null, attachment, bTrasfPdfErrore, bTrasfPdfWarning, 
					idCategoriaDocumento, nomeFile, hasDestinatariDocInterni, hasDestinatariDocEsterni, formatoAllegatoId, flagFirmaAutografaRM,
					storico, idMomentoProtocollazione, dataCreazione, dataScadenza, flagUrgenza, flagRiservato, flagFirmaPDF, 
					idTipoDocumento, idTipologiaProcedimento, mimeType, codiceFlusso, sottoCategoriaUscita, metadatiEstesi, registrazioneAusiliaria);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero di un metadato del dettaglio del documento: ", e);
			return null;
		}
	}

}
