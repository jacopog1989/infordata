package it.ibm.red.business.enums;

/**
 * @author a.dilegge
 *
 */
public enum StatoElabNotificaNpsEnum {

	/**
	 * Valore.
	 */
	DA_ELABORARE(1),
	
	/**
	 * Valore.
	 */
	IN_ELABORAZIONE(2),
	
	/**
	 * Valore.
	 */
	ELABORATA(3),
	
	/**
	 * Valore.
	 */
	IN_ERRORE(4);
	
	/**
	 * Identificativo.
	 */
	private Integer id;
	
	/**
	 * Costruttore dell'enum.
	 * @param id
	 */
	StatoElabNotificaNpsEnum(final Integer id) {
		this.id = id;
	}

	/**
	 * Restituisce l'id dell'enum.
	 * @return id
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * Restituisce l'enum associata al codice in ingresso.
	 * @param codice
	 * @return enum associata al codice
	 */
	public static StatoElabNotificaNpsEnum get(final Integer codice) {
		StatoElabNotificaNpsEnum output = null;
		
		for (final StatoElabNotificaNpsEnum stato : StatoElabNotificaNpsEnum.values()) {
			if (stato.getId().equals(codice)) {
				output = stato;
				break;
			}
		}
		
		return output;
	}
}
