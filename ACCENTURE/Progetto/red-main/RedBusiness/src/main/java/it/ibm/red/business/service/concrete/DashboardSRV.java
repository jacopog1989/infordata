package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IDashboardDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Widget;
import it.ibm.red.business.service.IDashboardSRV;

/**
 * Service gestione Dashboard.
 */
@Service
@Component
public class DashboardSRV extends AbstractService implements IDashboardSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -2110783812808462225L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DashboardSRV.class.getName());

	/**
	 * Dao dashboard.
	 */
	@Autowired
	private IDashboardDAO dashboardDAO;

	/**
	 * @see it.ibm.red.business.service.facade.IDashboardFacadeSRV#salvaPreferenzeWdg(java.util.List,
	 *      java.lang.Long, java.lang.Long).
	 */
	@Override
	public void salvaPreferenzeWdg(final List<Long> wdgIdList, final Long utenteId, final Long ruoloUtente) {
		Connection connection = null;
		try {
			Long posizione = 0L;
			connection = setupConnection(getDataSource().getConnection(), true);
			dashboardDAO.rimuoviPreferenzeWidget(utenteId, ruoloUtente, connection);
			for (final Long wdgId : wdgIdList) {
				dashboardDAO.salvaPreferenzeWdg(wdgId, utenteId, ruoloUtente, posizione++, connection);
			}
			commitConnection(connection);
		} catch (final Exception e) {
			rollbackConnection(connection);
			LOGGER.error(e);
			throw new RedException("Errore durante il salvataggio dei widget", e);

		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDashboardFacadeSRV#rimuoviPreferenze(java.util.List,
	 *      java.lang.Long).
	 */
	@Override
	public void rimuoviPreferenze(final List<Long> wdgIdList, final Long utenteId) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			for (final Long wdgId : wdgIdList) {
				dashboardDAO.rimuoviPreferenze(wdgId, utenteId, connection);
			}
			commitConnection(connection);
		} catch (final Exception e) {
			rollbackConnection(connection);
			LOGGER.error(e);
			throw new RedException("Errore durante la rimozione dei widget", e);

		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDashboardFacadeSRV#getWidgetPreferenze(java.lang.Long,
	 *      java.lang.Long).
	 */
	@Override
	public List<Widget> getWidgetPreferenze(final Long utenteId, final Long utenteRuolo) {
		Connection connection = null;
		List<Widget> widgets = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			widgets = dashboardDAO.getWidgetPreferenze(utenteId, utenteRuolo, connection);
		} catch (final Exception e) {
			rollbackConnection(connection);
			LOGGER.error(e);
			throw new RedException("Errore durante il caricamento dei widget", e);
		} finally {
			closeConnection(connection);
		}
		return widgets;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDashboardFacadeSRV#getWidgetRuolo(java.lang.Long).
	 */
	@Override
	public List<Widget> getWidgetRuolo(final Long idRuolo) {
		Connection connection = null;
		List<Widget> widgets = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			widgets = dashboardDAO.getWidgetRuolo(idRuolo, connection);
		} catch (final Exception e) {
			rollbackConnection(connection);
			LOGGER.error(e);
			throw new RedException("Errore durante il caricamento dei widget", e);
		} finally {
			closeConnection(connection);
		}
		return widgets;
	}
}
