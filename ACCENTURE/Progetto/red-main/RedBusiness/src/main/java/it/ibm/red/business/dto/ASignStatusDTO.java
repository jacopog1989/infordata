package it.ibm.red.business.dto;

import java.util.Date;

import it.ibm.red.business.service.asign.step.StatusEnum;

/**
 * DTO per la gestione degli stati degli item di firma asincrona.
 */
public class ASignStatusDTO extends AbstractDTO {

	/**
	 * La costante serial version UID.
	 */
	private static final long serialVersionUID = 622461807406459234L;
	
	/**
	 * Stato dell'item.
	 */
	private StatusEnum stato;
	
	/**
	 * Data inserimento stato.
	 */
	private Date dataDa;
	
	/**
	 * Data completamento dello status.
	 */
	private Date dataA;
	
	/**
	 * Restituisce lo stato.
	 * @return stato
	 */
	public StatusEnum getStato() {
		return stato;
	}
	
	/**
	 * Imposta lo stato.
	 * @param stato
	 */
	public void setStato(StatusEnum stato) {
		this.stato = stato;
	}
	
	/**
	 * Restituisce la data di inserimento dello stato.
	 * @return data inserimento stato
	 */
	public Date getDataDa() {
		return dataDa;
	}
	
	/**
	 * Imposta la data di inserimento dello stato.
	 * @param dataDa
	 */
	public void setDataDa(Date dataDa) {
		this.dataDa = dataDa;
	}
	
	/**
	 * Restituisce la data di chiusura dello stato.
	 * @return data chiusura stato
	 */
	public Date getDataA() {
		return dataA;
	}
	
	/**
	 * Imposta la data di chiusura dello stato.
	 * @param dataA
	 */
	public void setDataA(Date dataA) {
		this.dataA = dataA;
	}

}
