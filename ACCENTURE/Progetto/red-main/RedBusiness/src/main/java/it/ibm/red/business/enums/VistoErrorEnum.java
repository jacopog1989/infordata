package it.ibm.red.business.enums;

/**
 * The Enum VistoErrorEnum.
 *
 * @author CPIERASC
 * 
 *         Errori generabili in fase di visto.
 */
public enum VistoErrorEnum {
	
	/**
	 * CODICI ERRORI IN FASE DI VISTO.
	 */
	POST_VISTO_COD_ERROR(1, "Uno dei workflow legati alla procedura di visto risulta in errore. Contattare l'assistenza.");
	
	/**
	 * Codice di errore.
	 */
	private int codError;

	/**
	 * Messaggio d'errore.
	 */
	private String message;
	
	/**
	 * Costruttore.
	 * 
	 * @param inCodError	codice errore
	 * @param inMessage		messaggio errore
	 */
	VistoErrorEnum(final int inCodError, final String inMessage) {
		codError = inCodError;
		message = inMessage;
	}

	/**
	 * Getter codice errore.
	 * 
	 * @return	codice errore
	 */
	public int getCodError() {
		return codError;
	}

	/**
	 * Getter messaggio d'errore.
	 * 
	 * @return	messaggio d'errore
	 */
	public String getMessage() {
		return message;
	}

}
