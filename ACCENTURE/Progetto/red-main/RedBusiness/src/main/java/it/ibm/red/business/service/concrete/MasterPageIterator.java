package it.ibm.red.business.service.concrete;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.filenet.api.collection.PageIterator;

import filenet.vw.api.VWQueueQuery;
import filenet.vw.api.VWRosterQuery;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.provider.PropertiesProvider;

/**
 * Iterator pagine master.
 */
public class MasterPageIterator implements Serializable {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 8704626288721641892L;
	
	/**
	 * flag ulteriori pagine.
	 */
	private Boolean morePages;
	
	/**
	 * Query.	
	 */
	private transient Object query;
	
	/**
	 * WFS.
	 */
	private List<VWWorkObject> wfs;
	
	/**
	 * Insieme dei document title.
	 */
	private Set<String> documentTitleSet;
	
	/**
	 * Iteratore pagine.
	 */
	private transient PageIterator pageIterator;
	
	/**
	 * Struttura wf.
	 */
	private Map<String, Collection<VWWorkObject>> hmWf;
	
	/**
	 * Coda di riferimento.
	 */
	private DocumentQueueEnum queue;
	
	/**
	 * Costruttore della classe.
	 * @param inQuery
	 * @param inQueue
	 */
	public MasterPageIterator(final Object inQuery, final DocumentQueueEnum inQueue) {
		morePages = true;
		query = inQuery;
		queue = inQueue;
		wfs = new ArrayList<>();
		documentTitleSet = new HashSet<>();
		hmWf = new HashMap<>();
		
		if (query instanceof VWQueueQuery) {
			VWQueueQuery qq = (VWQueueQuery) query;
			handleQueueQuery(qq);
		} else if (query instanceof VWRosterQuery) {
			VWRosterQuery rq = (VWRosterQuery) query;
			while (rq.hasNext()) {
				VWWorkObject wo = (VWWorkObject) rq.next();
				handleWorkflow(wo);
			}
		} else if (query instanceof Collection) {
			Collection<?> queueQueries = (Collection<?>) query;
			for (Object qq : queueQueries) {
				handleQueueQuery((VWQueueQuery) qq);
			}
		}
	}

	private void handleQueueQuery(final VWQueueQuery qq) {
		while (qq.hasNext()) {
			VWWorkObject wo = (VWWorkObject) qq.next();
			handleWorkflow(wo);
		}
	}

	private void handleWorkflow(final VWWorkObject wo) {
		wfs.add(wo);
		String documentTitle = (TrasformerPE.getMetadato(wo, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY)) 
				+ Constants.EMPTY_STRING);
		documentTitleSet.add(documentTitle);
		Collection<VWWorkObject> wfsdt = hmWf.get(documentTitle);
		if (wfsdt == null) {
			wfsdt = new ArrayList<>();
		}
		wfsdt.add(wo);
		hmWf.put(documentTitle, wfsdt);
	}
	
	/**
	 * Restituisce i VWWOrkObject associati al document title all'interno del {@link #hmWf}.
	 * @param dt
	 * @return Collectiion di VWWOrkObject
	 */
	public Collection<VWWorkObject> filterWFByDocumentTitle(final String dt) {
		return hmWf.get(dt);
	}

	/**
	 * Restituisce il set dei documentTitle.
	 * @return i documentTitle
	 */
	public Set<String> getDocumentTitleSet() {
		return documentTitleSet;
	}

	/**
	 * Restituisce l'iterator per le pagine.
	 * @return PageIterator
	 */
	public PageIterator getPageIterator() {
		return pageIterator;
	}

	/**
	 * Imposta l'iterator per le pagine.
	 * @param pageIterator
	 */
	public void setPageIterator(final PageIterator pageIterator) {
		this.pageIterator = pageIterator;
	}

	/**
	 * @return morePages
	 */
	public Boolean getMorePages() {
		return morePages;
	}

	/**
	 * @param morePages
	 */
	public void setMorePages(final Boolean morePages) {
		this.morePages = morePages;
	}

	/**
	 * Restituisce la coda.
	 * @return DocumentQueueEnum
	 */
	public DocumentQueueEnum getQueue() {
		return queue;
	}

	/**
	 * Restituisce i workflow.
	 * 
	 * @return List di VWWorkObject
	 */
	public List<VWWorkObject> getWfs() {
		return wfs;
	}
}