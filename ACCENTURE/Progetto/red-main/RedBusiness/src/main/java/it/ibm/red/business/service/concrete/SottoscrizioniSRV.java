package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.ICanaleTrasmissioneDAO;
import it.ibm.red.business.dao.IDisabilitazioneNotificheUtenteDAO;
import it.ibm.red.business.dao.ISottoscrizioniDAO;
import it.ibm.red.business.dto.CanaleTrasmissioneDTO;
import it.ibm.red.business.dto.DisabilitazioneNotificheUtenteDTO;
import it.ibm.red.business.dto.EventoSottoscrizioneDTO;
import it.ibm.red.business.dto.MailAddressDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.IntervalloDisabilitazioneEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.DisabilitazioneNotificheUtente;
import it.ibm.red.business.service.ISottoscrizioniSRV;

/**
 * Service che gestisce le sottoscrizioni.
 */
@Service
@Component
public class SottoscrizioniSRV extends AbstractService implements ISottoscrizioniSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 8447337068295228612L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SottoscrizioniSRV.class.getName());

	/**
	 * DAO.
	 */
	@Autowired
	private ISottoscrizioniDAO sottoscrizioniDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private ICanaleTrasmissioneDAO canaleDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IDisabilitazioneNotificheUtenteDAO disabilitazioneNotificheUtenteDAO;

	/**
	 * @see it.ibm.red.business.service.ISottoscrizioniSRV#getEventiSottoscrittiByCategoria(int, int, java.sql.Connection).
	 */
	@Override
	public List<EventoSottoscrizioneDTO> getEventiSottoscrittiByCategoria(final int idAoo, final int idCategoriaEvento,
			final Connection con) {

		List<EventoSottoscrizioneDTO> eventi = null;
		try {
			eventi = sottoscrizioniDAO.getSottoscrizioniListByIdCategoria(con, idCategoriaEvento, idAoo);
		} catch (final Exception e) {
			LOGGER.error("SottoscrizioniSRV.getEventiSottoscrittiByCategoria: errore generico.", e);
		}

		return eventi;
	}

	/**
	 * @see it.ibm.red.business.service.ISottoscrizioniSRV#getEventiSottoscrittiByListaEventi(int, java.lang.Integer[], java.sql.Connection).
	 */
	@Override
	public List<EventoSottoscrizioneDTO> getEventiSottoscrittiByListaEventi(final int idAoo, final Integer[] idEventi,
			final Connection con) {

		List<EventoSottoscrizioneDTO> eventi = null;
		try {
			eventi = sottoscrizioniDAO.getSottoscrizioniListByIdEventi(con, idEventi, idAoo);
		} catch (final Exception e) {
			LOGGER.error("SottoscrizioniSRV.getEventiSottoscrittiByListaEventi: errore generico.", e);
		}

		return eventi;
	}

	/**
	 * Carica i canali di trasmissioni, sono delle costanti, devono essere
	 * valorizzate.
	 * 
	 * @return if return null something went wrong
	 */
	@Override
	public Collection<CanaleTrasmissioneDTO> loadCanaliTrasmissione() {
		Connection con = null;
		Collection<CanaleTrasmissioneDTO> all = null;
		try {
			con = getFilenetDataSource().getConnection();
			all = canaleDAO.getAll(con);
		} catch (final RedException red) {
			LOGGER.error(red);
		} catch (final SQLException e) {
			LOGGER.error("Impossibile stabilire la connessione con il DWH filenet", e);
		} finally {
			closeConnection(con);
		}
		return all;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISottoscrizioniFacadeSRV#loadIntervalloDisabilitazione().
	 */
	@Override
	public Collection<IntervalloDisabilitazioneEnum> loadIntervalloDisabilitazione() {
		return EnumSet.allOf(IntervalloDisabilitazioneEnum.class);
	}

	private static DisabilitazioneNotificheUtente dtoToBean(final DisabilitazioneNotificheUtenteDTO dto) {
		final DisabilitazioneNotificheUtente dnu = new DisabilitazioneNotificheUtente();

		dnu.setIdUtente(Long.parseLong(dto.getIdUtente()));
		dnu.setIdCanaleTrasmissione(dto.getIdCanaleTrasmissione());

		if (dto.getDataDa() != null) {
			dnu.setDataDa(new java.sql.Date(dto.getDataDa().getTime()));
		}

		if (dto.getDataDa() != null) {
			dnu.setDataA(new java.sql.Date(dto.getDataA().getTime()));
		}

		return dnu;
	}

	/**
	 * Recupera le configurazioni scelte dall'utente rispetto alle notificheDTO.
	 * 
	 * @param utente
	 * 
	 * @return
	 */
	@Override
	public Collection<DisabilitazioneNotificheUtenteDTO> loadDisabilitazioniNotificheUtente(final UtenteDTO utente) {
		Connection con = null;
		Collection<DisabilitazioneNotificheUtenteDTO> all = null;
		try {
			con = getFilenetDataSource().getConnection();
			final Collection<DisabilitazioneNotificheUtente> confList = disabilitazioneNotificheUtenteDAO
					.getbyUser(utente.getId(), con);

			all = new ArrayList<>(confList.size());
			for (final DisabilitazioneNotificheUtente conf : confList) {
				all.add(new DisabilitazioneNotificheUtenteDTO(conf));
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero delle notifiche utente", e);
		} finally {
			closeConnection(con);
		}
		return all;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISottoscrizioniFacadeSRV#registraDisabilitazioneNotificheUtente(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection).
	 */
	@Override
	public boolean registraDisabilitazioneNotificheUtente(final UtenteDTO utente,
			final Collection<DisabilitazioneNotificheUtenteDTO> disabilitazioniNotificheUtente) {

		Connection con = null;

		try {

			con = setupConnection(getFilenetDataSource().getConnection(), true);

			disabilitazioneNotificheUtenteDAO.delete(utente.getId(), con);

			for (final DisabilitazioneNotificheUtenteDTO disabilitazione : disabilitazioniNotificheUtente) {

				disabilitazioneNotificheUtenteDAO.insert(dtoToBean(disabilitazione), con);

			}

			commitConnection(con);
			return true;

		} catch (final Exception e) {
			LOGGER.error("Errore durante l'aggiornamento delle notifiche utente", e);
			rollbackConnection(con);
		} finally {
			closeConnection(con);
		}

		return false;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISottoscrizioniFacadeSRV#registraSottoscrizione(it.ibm.red.business.dto.UtenteDTO, java.util.List).
	 */
	@Override
	public boolean registraSottoscrizione(final UtenteDTO utente, final List<EventoSottoscrizioneDTO> listaSottoscrizioni) {

		Connection con = null;

		try {

			con = setupConnection(getFilenetDataSource().getConnection(), true);

			// recupera le sottoscrizioni attualmente registrate
			final List<EventoSottoscrizioneDTO> listaSottoscrizioniAttuali = sottoscrizioniDAO
					.getSottoscrizioniListByUser(con, utente.getId(), utente.getIdUfficio(), utente.getIdRuolo());

			// elimina le sottoscrizioni registrate e non più selezionate dall'utente
			final List<EventoSottoscrizioneDTO> listaSottoscrizioniToDelete = listaSottoscrizioniAttuali.stream()
					.filter(sottoscrizione -> !listaSottoscrizioni.contains(sottoscrizione))
					.collect(Collectors.toList());
			for (final EventoSottoscrizioneDTO sottoscrizioneToDelete : listaSottoscrizioniToDelete) {
				sottoscrizioniDAO.rimuoviSottoscrizione(con, sottoscrizioneToDelete.getIdUtente(),
						sottoscrizioneToDelete.getIdNodo(), sottoscrizioneToDelete.getIdEvento(),
						sottoscrizioneToDelete.getIdRuolo());
			}

			// inserisci o aggiorna le sottoscrizioni selezionate dall'utente
			for (final EventoSottoscrizioneDTO sottoscrizioneToPersist : listaSottoscrizioni) {
				if (!listaSottoscrizioniAttuali.contains(sottoscrizioneToPersist)) {
					// insert
					sottoscrizioniDAO.insertSottoscrizione(con, sottoscrizioneToPersist.getIdEvento(),
							utente.getIdUfficio(), utente.getId(), utente.getIdRuolo(),
							sottoscrizioneToPersist.getTracciamento(),
							sottoscrizioneToPersist.getCanaleTrasmissione().getId());

				} else { // update tracciamento evento
					sottoscrizioniDAO.updateTracciamentoAndCanaleNotifica(con, sottoscrizioneToPersist.getIdEvento(),
							utente.getIdUfficio(), utente.getId(), utente.getIdRuolo(),
							sottoscrizioneToPersist.getTracciamento(),
							sottoscrizioneToPersist.getCanaleTrasmissione().getId());

				}

				final List<Integer> idDocTracciati = sottoscrizioniDAO.getDocumentiTracciati(con, utente.getId(),
						utente.getIdUfficio(), utente.getIdRuolo());
				if (idDocTracciati != null) {
					for (final Integer idDoc : idDocTracciati) {

						sottoscrizioniDAO.rimuoviDoc(utente.getId(), utente.getIdUfficio(), utente.getIdRuolo(), idDoc,
								sottoscrizioneToPersist.getIdEvento(), con);

						if (sottoscrizioneToPersist.getTracciamento() == 1) {
							// se tracciamento singolo => associa anche a questo evento
							sottoscrizioniDAO.aggiungiDoc(con, utente.getId(), utente.getIdUfficio(),
									utente.getIdRuolo(), idDoc, sottoscrizioneToPersist.getIdEvento());
						}

					}
				}
			}

			commitConnection(con);
			return true;

		} catch (final Exception e) {
			LOGGER.error("Errore durante l'aggiornamento delle sottoscrizioni utente", e);
			rollbackConnection(con);
		} finally {
			closeConnection(con);
		}

		return false;

	}

	/**
	 * @see it.ibm.red.business.service.facade.ISottoscrizioniFacadeSRV#registraMail(it.ibm.red.business.dto.UtenteDTO, java.util.List).
	 */
	@Override
	public boolean registraMail(final UtenteDTO utente, final List<String> listaAddress) {
		Connection con = null;

		final List<MailAddressDTO> listaMail = new ArrayList<>(listaAddress.size());
		for (final String address : listaAddress) {
			final MailAddressDTO mail = new MailAddressDTO(utente.getId().intValue(), 0, address);
			listaMail.add(mail);
		}

		try {

			con = setupConnection(getFilenetDataSource().getConnection(), true);

			sottoscrizioniDAO.deleteMails(utente.getId().intValue(), con);

			for (final MailAddressDTO mail : listaMail) {
				sottoscrizioniDAO.insertMail(mail, con);

			}

			commitConnection(con);
			return true;

		} catch (final Exception e) {
			LOGGER.error("Errore durante l'aggiornamento delle email utente", e);
			rollbackConnection(con);
		} finally {
			closeConnection(con);
		}

		return false;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISottoscrizioniFacadeSRV#loadMailAddress(it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Collection<MailAddressDTO> loadMailAddress(final UtenteDTO utente) {
		Connection con = null;

		try {

			con = setupConnection(getFilenetDataSource().getConnection(), false);
			return sottoscrizioniDAO.getMailsSottoscrizioni(con, utente.getId().intValue());

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero delle email utente " + utente.getId(), e);
			throw new RedException("Errore durante il recupero delle email utente " + utente.getId(), e);
		} finally {
			closeConnection(con);
		}

	}

	/**
	 * @see it.ibm.red.business.service.facade.ISottoscrizioniFacadeSRV#getEventiSottoscritti(it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public List<EventoSottoscrizioneDTO> getEventiSottoscritti(final UtenteDTO utente) {
		Connection con = null;

		try {

			con = setupConnection(getFilenetDataSource().getConnection(), false);
			return sottoscrizioniDAO.getSottoscrizioniListByUser(con, utente.getId(), utente.getIdUfficio(),
					utente.getIdRuolo());

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero delle sottoscrizioni dell'utente " + utente.getId(), e);
			throw new RedException("Errore durante il recupero delle sottoscrizioni dell'utente " + utente.getId(), e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISottoscrizioniFacadeSRV#getEventiByAoo(java.lang.Integer).
	 */
	@Override
	public List<EventoSottoscrizioneDTO> getEventiByAoo(final Integer idAoo) {
		Connection con = null;

		try {

			con = setupConnection(getFilenetDataSource().getConnection(), false);
			final CanaleTrasmissioneDTO canaleDefault = canaleDAO.getById(CanaleTrasmissioneDTO.ID_TUTTI, con);
			return sottoscrizioniDAO.getEventiListByIdAoo(con, idAoo, canaleDefault);

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero degli eventi dell'aoo " + idAoo, e);
			throw new RedException("Errore durante il recupero degli eventi dell'aoo " + idAoo, e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISottoscrizioniFacadeSRV#getCountSottoscrizioni(it.ibm.red.business.dto.UtenteDTO, int).
	 */
	@Override
	public int getCountSottoscrizioni(final UtenteDTO utente, final int numeroGiorni) {
		Connection con = null;

		try {

			con = setupConnection(getFilenetDataSource().getConnection(), false);
			return sottoscrizioniDAO.getCountSottoscrizioni(con, utente.getId(), numeroGiorni, utente.getIdAoo());

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
	}

}