package it.ibm.red.business.enums;

/**
 * Enum che definisce le tipologie di statistiche uffici.
 */
public enum TipoStatisticaUfficioReportEnum {

	/**
	 * Valore.
	 */
	INGRESSO_TUTTI("Documenti in ingresso"), 

	/**
	 * Valore.
	 */
	INGRESSO_TUTTI_APERTI("Documenti aperti in ingresso"), 

	/**
	 * Valore.
	 */
	INGRESSO_IN_LAVORAZIONE("Documenti in lavorazione in ingresso"), 

	/**
	 * Valore.
	 */
	INGRESSO_IN_SOSPESO("Documenti in sospeso in ingresso"), 

	/**
	 * Valore.
	 */
	INGRESSO_IN_SCADENZA("Documenti in scadenza in ingresso"), 

	/**
	 * Valore.
	 */
	INGRESSO_SCADUTI("Documenti scaduti in ingresso"), 

	/**
	 * Valore.
	 */
	INGRESSO_TUTTI_CHIUSI("Documenti chiusi in ingresso"), 

	/**
	 * Valore.
	 */
	INGRESSO_ATTI("Documenti agli atti in ingresso"), 

	/**
	 * Valore.
	 */
	INGRESSO_RISPOSTA("Documenti chiusi con risposta in ingresso"), 

	/**
	 * Valore.
	 */
	USCITA_TUTTI("Documenti in uscita"), 

	/**
	 * Valore.
	 */
	USCITA_IN_LAVORAZIONE("Documenti in lavorazione in uscita"), 

	/**
	 * Valore.
	 */
	USCITA_SPEDITI("Documenti spediti in uscita");

	/**
	 * Descrizione.
	 */
	private String descrizione;

	/**
	 * Costruttore.
	 * @param inDescrizione
	 */
	TipoStatisticaUfficioReportEnum(final String inDescrizione) {
		descrizione = inDescrizione;
	}

	/**
	 * Restituisce la descrizione.
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}
}