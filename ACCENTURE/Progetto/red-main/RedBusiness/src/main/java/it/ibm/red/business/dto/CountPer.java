package it.ibm.red.business.dto;

/**
 * 
 * Astrazione di un contenitore che contiene un qualche numero di elementi
 * 
 * La spiegazione del nome è che serve a eseguire un count raggruppato per
 * l'oggetto <E>
 * 
 * E.G. Voglio avere un oggetto che registri quanti documenti sono presenti
 * nella cartella x
 * 
 * Cartella_x (10)
 * 
 * @author a.difolca
 *
 * @param <E>
 *            Descrittore del contenitore aka metadati del contenitore
 */
public class CountPer<E> extends AbstractDTO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5472535696970217060L;

	/**
	 * Contenitore degli elementi calcolati.
	 */
	private transient E data;

	/**
	 * Numero degli elementi calcolati.
	 */
	private Integer count;

	/**
	 * Costruttore vuoto.
	 */
	public CountPer() {
	}

	/**
	 * Costruttore che imposta data e count.
	 * @param data
	 * @param count
	 */
	public CountPer(final E data, final Integer count) {
		super();
		this.data = data;
		this.count = count;
		
	}

	/**
	 * Restituisce la data.
	 * @return data
	 */
	public E getData() {
		return data;
	}

	/**
	 * Imposta la data.
	 * @param data
	 */
	public void setData(final E data) {
		this.data = data;
	}

	/**
	 * Restituisce il count.
	 * @return count
	 */
	public Integer getCount() {			
		return count;
	}

	/**
	 * Imposta il count con il valore in ingresso.
	 * @param inCount
	 */
	public void setCount(final Integer inCount) {
		this.count = inCount;
	}

	/**
	 * @see java.lang.Object#hashCode().
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((count == null) ? 0 : count.hashCode());
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object).
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CountPer<E> other = (CountPer<E>) obj;
		if (count == null) {
			if (other.count != null) {
				return false;
			}
		} else if (!count.equals(other.count)) {
			return false;
		}
		if (data == null) {
			if (other.data != null) {
				return false;
			}
		} else if (!data.equals(other.data)) {
			return false;
		}
		return true;
	}
	
}
