package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.AllegatoNsdDTO;
import it.ibm.red.business.dto.DetailFascicoloPregressoDTO;
import it.ibm.red.business.dto.EsitoVerificaProtocolloNsdDTO;
import it.ibm.red.business.dto.FascicoliPregressiDTO;
import it.ibm.red.business.dto.ParamsRicercaFascicoliPregressiDfDTO;
import it.ibm.red.business.dto.ParamsRicercaProtocolliPregressiDfDTO;
import it.ibm.red.business.dto.ProtocolliPregressiDTO;
import it.ibm.red.business.dto.RiferimentoStoricoNsdDTO;
import it.ibm.red.business.dto.TipologiaRegistroNsdDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * 
 * @author VINGENITO
 * 
 *
 */
public interface IDfFacadeSRV extends Serializable {

	/******** CONSULTAZIONE FASCICOLI PREGRESSI ******/
	/**
	 * @param paramsRicercaFascicoliPregressi
	 * @param utente
	 * @param numPagina
	 * @param recordPerPagina
	 * 
	 * @return List di FascicoliPregressiDTO
	 */
	List<FascicoliPregressiDTO> ricercaFascicoliPregressi(ParamsRicercaFascicoliPregressiDfDTO paramsRicercaFascicoliPregressi, UtenteDTO utente, Integer numPagina,
			Integer recordPerPagina);

	/******** CONSULTAZIONE PROTOCOLLI PREGRESSI ******/
	/**
	 * @param ParamsRicercaProtocolliPregressiDfDTO
	 * @param utente
	 * @return List di ProtocolliPregressiDTO
	 */
	List<ProtocolliPregressiDTO> ricercaProtocolliPregressi(ParamsRicercaProtocolliPregressiDfDTO paramsRicercaProtocolliPregressi, UtenteDTO utente, String lastRowNum,
			String maxNumIteratore);

	/******** DOCUMENTO CON CONTENT ******/
	/**
	 * @return AllegatoNsdDTO
	 */
	AllegatoNsdDTO getDocumentoConContent(String originalDocId, UtenteDTO utente);

	/******** RECUPERO DETTAGLIO FASCICOLO BY ID ******/
	/**
	 * @return DetailFascicoloPregressoDTO
	 */
	DetailFascicoloPregressoDTO getFascicoloById(FascicoliPregressiDTO fascicolo, UtenteDTO utente);

	/******** COUNT PER LA PAGINAZIONE DELLA RICERCA PROT ******/
	/**
	 * @return int
	 */

	int ricercaProtocolliPregressiCount(ParamsRicercaProtocolliPregressiDfDTO paramsProtocolliDTO, UtenteDTO utente);

	/******** COUNT PER LA PAGINAZIONE DELLA RICERCA FASCICOLO ******/
	/**
	 * @return int
	 */
	int ricercaFascicoliPregressiCount(ParamsRicercaFascicoliPregressiDfDTO paramsFascicoliDTO, UtenteDTO utente);

	/******** VERIFICA ALLACCIO PROTOCOLLO STORICO ******/
	/**
	 * @return EsitoVerificaProtocolloNsdDTO
	 */
	EsitoVerificaProtocolloNsdDTO verificaRiferimentoStorico(UtenteDTO utente, RiferimentoStoricoNsdDTO rifStorico);

	/**
	 * Ottiene il protocollo pregresso.
	 * 
	 * @param utente
	 * @param nProtocolloNsd
	 * @param annoProtocolloNsd
	 * @return protocollo pregresso
	 */
	ProtocolliPregressiDTO getProtocolloById(UtenteDTO utente, String nProtocolloNsd, String annoProtocolloNsd);

	/**
	 * Ottiene il numero massimo di righe per pagina per i protocolli.
	 * 
	 * @return numero massimo di righe
	 */
	Integer getmaxRowPageProtocolli();

	/**
	 * Ottiene il numero massimo di righe per pagina per i fascicoli.
	 * 
	 * @return numero massimo di righe
	 */
	Integer getmaxRowPageFascicoli();

	/**
	 * Ottiene le tipologie di documento Nsd.
	 * 
	 * @param idufficio
	 * @param idUfficioPadre
	 * @return lista di tipologie di documento Nsd
	 */
	List<TipologiaRegistroNsdDTO> getTipologiaDocumentoNsd(Long idufficio, Long idUfficioPadre);

	/**
	 * Ottiene le tipologie di registro Nsd.
	 * 
	 * @return lista di tipologie di registro Nsd
	 */

	List<TipologiaRegistroNsdDTO> getTipologiaRegistroNsd(Long idAOO);

}
