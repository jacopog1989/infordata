/**
 * 
 */
package it.ibm.red.business.service.ws.facade;

import java.io.Serializable;

import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.webservice.model.documentservice.types.messages.RedAvanzaWorkflowType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedAvviaIstanzaWorkflowType;
import it.ibm.red.webservice.model.documentservice.types.processo.Workflow;

/**
 * @author APerquoti
 * 
 * Facade WorkFlow service da web service.
 *
 */
public interface IWorkFlowWsFacadeSRV extends Serializable {

	/**
	 * Esegue l'avanzamento del workflow.
	 * @param client
	 * @param request
	 * @return workflow
	 */
	Workflow avanzaWorkFlow(RedWsClient client, RedAvanzaWorkflowType request);
	
	/**
	 * Avvia una nuova istanza di workflow.
	 * @param client
	 * @param request
	 * @return workflow
	 */
	Workflow avviaIstanzaWorkFlow(RedWsClient client, RedAvviaIstanzaWorkflowType request);
	
}
