/**
 * 
 */
package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IFadDAO;
import it.ibm.red.business.dto.FadAmministrazioneDTO;
import it.ibm.red.business.dto.FadRagioneriaDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * @author APerquoti
 *
 */
@Repository
public class FadDAO extends AbstractDAO implements IFadDAO {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 431465960425233940L;
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FadDAO.class.getName());

	/**
	 * @see it.ibm.red.business.dao.IFadDAO#getRagioneriaByCodiceRagioneria(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public FadRagioneriaDTO getRagioneriaByCodiceRagioneria(final String codiceRagioneria, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		FadRagioneriaDTO fadRagioneria = null;
		try {
			ps = con.prepareStatement("SELECT codice_ragioneria, descrizione_ragioneria " + "FROM fad_ragioneria "
					+ "WHERE codice_ragioneria = ?");
			ps.setString(1, codiceRagioneria);
			rs = ps.executeQuery();
			if (rs.next()) {
				fadRagioneria = new FadRagioneriaDTO(rs.getString("codice_ragioneria"),
						rs.getString("descrizione_ragioneria"));
			}
		} catch (SQLException e) {
			LOGGER.error("Errore durante il recupero dei dati FadRagioneria dal DB", e);
			throw new RedException(
					"Errore durante il recupero dei dati per il FadRagioneria dal DB: " + e.getMessage());
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
		return fadRagioneria;
	}

	/**
	 * @see it.ibm.red.business.dao.IFadDAO#getAmministrazioneByCodiceAmministrazione(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public FadAmministrazioneDTO getAmministrazioneByCodiceAmministrazione(final String codiceAmministrazione,
			final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		FadAmministrazioneDTO fadAmministrazione = null;
		try {
			ps = con.prepareStatement("SELECT codice_amministrazione, descrizione_amministrazione "
					+ "FROM fad_amministrazione " + "WHERE codice_amministrazione = ?");
			ps.setString(1, codiceAmministrazione);
			rs = ps.executeQuery();
			if (rs.next()) {
				fadAmministrazione = new FadAmministrazioneDTO(rs.getString("codice_amministrazione"),
						rs.getString("descrizione_amministrazione"));
			}
		} catch (SQLException e) {
			LOGGER.error("Errore durante il recupero dei dati FadAmministrazione dal DB", e);
			throw new RedException(
					"Errore durante il recupero dei dati per il FadAmministrazione dal DB: " + e.getMessage());
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
		return fadAmministrazione;
	}

	/**
	 * @see it.ibm.red.business.dao.IFadDAO#getAmministrazioni(java.sql.Connection).
	 */
	@Override
	public List<FadAmministrazioneDTO> getAmministrazioni(final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<FadAmministrazioneDTO> list = null;
		try {
			ps = con.prepareStatement("SELECT f.* FROM fad_amministrazione f, fad_amministrazione_ragioneria far " 
					+ "WHERE f.codice_amministrazione = far.codice_amministrazione");
			rs = ps.executeQuery();

			list = new ArrayList<>();
			while (rs.next()) {
				list.add(new FadAmministrazioneDTO(rs.getString("codice_amministrazione"), rs.getString("descrizione_amministrazione")));
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage(), e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
		return list;
	}

	/**
	 * @see it.ibm.red.business.dao.IFadDAO#findRagioneriaByCodiceAmministrazione(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public FadRagioneriaDTO findRagioneriaByCodiceAmministrazione(final String codiceAmministrazione, final Connection con) {

		PreparedStatement ps = null;
		ResultSet rs = null;
		FadRagioneriaDTO item = null;

		try {
			String query = "SELECT fad_ragioneria.codice_ragioneria, " 
					+ "       fad_ragioneria.descrizione_ragioneria "
					+ "  FROM fad_amministrazione_ragioneria " 
					+ " INNER JOIN fad_amministrazione "
					+ "    on fad_amministrazione_ragioneria.codice_amministrazione = "
					+ "       fad_amministrazione.codice_amministrazione " 
					+ " INNER JOIN fad_ragioneria "
					+ "    on fad_amministrazione_ragioneria.codice_ragioneria = "
					+ "       fad_ragioneria.codice_ragioneria "
					+ " WHERE fad_amministrazione_ragioneria.codice_amministrazione = "
					+ "       fad_amministrazione.codice_amministrazione "
					+ "   and fad_amministrazione_ragioneria.codice_ragioneria = "
					+ "       fad_ragioneria.codice_ragioneria "
					+ "   and fad_amministrazione.codice_amministrazione = ?";
			ps = con.prepareStatement(query);
			ps.setString(1, codiceAmministrazione);
			rs = ps.executeQuery();

			if (rs.next()) {
				item = new FadRagioneriaDTO(rs.getString("codice_ragioneria"), rs.getString("descrizione_ragioneria"));
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage(), e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
		return item;
	}

}
