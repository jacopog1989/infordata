package it.ibm.red.business.utils;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.springframework.web.util.HtmlUtils;

import com.filenet.api.util.Id;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.enums.FilenetStatoFascicoloEnum;
import it.ibm.red.business.enums.ModalitaRicercaAvanzataTestoEnum;
import it.ibm.red.business.enums.RicercaGenericaTypeEnum;

/**
 * The Class StringUtils.
 *
 * @author CPIERASC
 * 
 *         Utility per la gestione delle stringe.
 */
public final class StringUtils {

	/**
	 * Mappa sostituzioni.
	 */
	private static final Map<Character, String> SUBSTITUTES;

	static {
		SUBSTITUTES = new HashMap<>();
		SUBSTITUTES.put('€', "&#128;");
		SUBSTITUTES.put('!', "&#33;");
		SUBSTITUTES.put('"', "&#34;");
		SUBSTITUTES.put('#', "&#35;");
		SUBSTITUTES.put('$', "&#36;");
		SUBSTITUTES.put('%', "&#37;");
		SUBSTITUTES.put('&', "&#38;");
		SUBSTITUTES.put('\'', "&#39;");
		SUBSTITUTES.put('(', "&#40;");
		SUBSTITUTES.put(')', "&#41;");
		SUBSTITUTES.put('*', "&#42;");
		SUBSTITUTES.put('+', "&#43;");
		SUBSTITUTES.put(',', "&#44;");
		SUBSTITUTES.put('-', "&#45;");
		SUBSTITUTES.put('.', "&#46;");
		SUBSTITUTES.put('/', "&#47;");
		SUBSTITUTES.put('<', "&#60;");
		SUBSTITUTES.put('=', "&#61;");
		SUBSTITUTES.put('>', "&#62;");
		SUBSTITUTES.put('?', "&#63;");
		SUBSTITUTES.put('@', "&#64;");
		SUBSTITUTES.put('[', "&#91;");
		SUBSTITUTES.put('\\', "&#92;");
		SUBSTITUTES.put(']', "&#93;");
		SUBSTITUTES.put('^', "&#94;");
		SUBSTITUTES.put('_', "&#95;");
		SUBSTITUTES.put('`', "&#96;");
		SUBSTITUTES.put('{', "&#123;");
		SUBSTITUTES.put('|', "&#124;");
		SUBSTITUTES.put('}', "&#125;");
		SUBSTITUTES.put('~', "&#126;");
		SUBSTITUTES.put('¡', "&#161;");
		SUBSTITUTES.put('¢', "&#162;");
		SUBSTITUTES.put('£', "&#163;");
		SUBSTITUTES.put('¤', "&#164;");
		SUBSTITUTES.put('¥', "&#165;");
		SUBSTITUTES.put('¦', "&#166;");
		SUBSTITUTES.put('§', "&#167;");
		SUBSTITUTES.put('¨', "&#168;");
		SUBSTITUTES.put('©', "&#169;");
		SUBSTITUTES.put('ª', "&#170;");
		SUBSTITUTES.put('«', "&#171;");
		SUBSTITUTES.put('¬', "&#172;");
		SUBSTITUTES.put('­', "&#173;");
		SUBSTITUTES.put('®', "&#174;");
		SUBSTITUTES.put('¯', "&#175;");
		SUBSTITUTES.put('°', "&#176;");
		SUBSTITUTES.put('±', "&#177;");
		SUBSTITUTES.put('²', "&#178;");
		SUBSTITUTES.put('³', "&#179;");
		SUBSTITUTES.put('´', "&#180;");
		SUBSTITUTES.put('·', "&#183;");
		SUBSTITUTES.put('¸', "&#184;");
		SUBSTITUTES.put('¹', "&#185;");
		SUBSTITUTES.put('º', "&#186;");
		SUBSTITUTES.put('»', "&#187;");
		SUBSTITUTES.put('À', "&#192;");
		SUBSTITUTES.put('Á', "&#193;");
		SUBSTITUTES.put('È', "&#200;");
		SUBSTITUTES.put('É', "&#201;");
		SUBSTITUTES.put('Ì', "&#204;");
		SUBSTITUTES.put('Í', "&#205;");
		SUBSTITUTES.put('Ò', "&#210;");
		SUBSTITUTES.put('Ó', "&#211;");
		SUBSTITUTES.put('Ù', "&#217;");
		SUBSTITUTES.put('Ú', "&#218;");
		SUBSTITUTES.put('à', "&#224;");
		SUBSTITUTES.put('á', "&#225;");
		SUBSTITUTES.put('è', "&#232;");
		SUBSTITUTES.put('é', "&#233;");
		SUBSTITUTES.put('ì', "&#236;");
		SUBSTITUTES.put('í', "&#237;");
		SUBSTITUTES.put('ò', "&#242;");
		SUBSTITUTES.put('ó', "&#243;");
		SUBSTITUTES.put('ú', "&#250;");
		SUBSTITUTES.put('ù', "&#274;");
		SUBSTITUTES.put('ç', "&#199;");
		SUBSTITUTES.put('\n', "<br />");
		SUBSTITUTES.put('\r', "<br />");
	}

	/**
	 * Lista caratteri speciali.
	 */
	public static final String LISTA_SPECIAL_CHARACTERS = "\\;:ç°§*é^?\"!£$%&=";

	/**
	 * MAX_COUNT_IN_CONDITON.
	 */
	private static final int MAX_COUNT_IN_CONDITON = 999;

	/**
	 * Costruttore.
	 */
	private StringUtils() {
	}

	/**
	 * Metodo per il test di una stringa (true se la stringa è null o vuota).
	 * 
	 * @param str
	 *            la stringa da testare
	 * @return true se la stringa è null o vuota
	 */
	public static boolean isNullOrEmpty(final String str) {
		return str == null || str.trim().length() == 0;
	}

	/**
	 * Metodo per eliminare l'ultimo carattere di una stringa.
	 * 
	 * @param str
	 *            stringa
	 * @return stringa senza l'ultimo carattere
	 */
	public static String cutLast(final String str) {
		String tmp = str;
		if (tmp != null && tmp.length() > 0) {
			tmp = tmp.substring(0, tmp.length() - 1);
		}
		return tmp;
	}

	/**
	 * Metodo per serializzare una lista di stringhe.
	 * 
	 * @param strs
	 *            lista di stringhe
	 * @return collezione di stringhe
	 */
	public static String fromStringListToString(final Collection<String> strs) {
		final StringBuilder output = new StringBuilder("");
		if (strs != null && !strs.isEmpty()) {
			for (final String s : strs) {
				output.append(s + "; ");
			}
		}
		return StringUtils.cutLast(StringUtils.cutLast(output.toString()));
	}

	/**
	 * Metodo per la serializzazione di una lista di long.
	 * 
	 * @param longs
	 *            lista di interi
	 * @return stringa risultante
	 */
	public static String fromLongListToCommaString(final Collection<Long> longs) {
		final StringBuilder output = new StringBuilder("");

		if (longs != null && !longs.isEmpty()) {
			for (final Long l : longs) {
				output.append(l + ", ");
			}
		}
		return StringUtils.cutLast(StringUtils.cutLast(output.toString()));
	}

	/**
	 * Sostituisce i caratteri speciali (compreso apice, con doppio apice) inserisce
	 * uno spazio al loro posto.
	 * 
	 * @param s
	 *            stringa da modificare
	 * @return stringa modificata
	 */
	public static String deleteSpecialCharacterForSpace(final String s) {
		return deleteSpecialCharacterForSpace(s, true);
	}

	/**
	 * Rimuove tutti i caratteri speciali dalla stringa e li sostituisce con uno spazio vuoto.
	 * @param s
	 * @param apix se true vengono sostituiti i singoli apici con i doppi apici
	 * @return la stringa modificata
	 */
	public static String deleteSpecialCharacterForSpace(final String s, final Boolean apix) {
		String retvalue = s;

		for (int i = 0; i < LISTA_SPECIAL_CHARACTERS.length(); i++) {
			final char sc = LISTA_SPECIAL_CHARACTERS.charAt(i);

			if (s.indexOf(sc) != -1) {
				retvalue = deleteCharForSpace(retvalue, sc);
			}
		}

		if (Boolean.TRUE.equals(apix)) {
			retvalue = retvalue.replace("'", "''");
		}

		return retvalue;
	}

	/**
	 * sostituisci il carattere con uno spazio.
	 * 
	 * @param s
	 *            stringa da modificare
	 * @param sc
	 *            carattere da sostituire
	 * @return stringa modificata
	 */
	private static String deleteCharForSpace(final String s, final char sc) {
		final StringBuilder hold = new StringBuilder();
		char c;
		for (int i = 0; i < s.length(); i++) {
			c = s.charAt(i);
			if (c == sc) {
				hold.append(" ");
			} else {
				hold.append(c);
			}
		}
		return hold.toString();
	}

	/**
	 * Metodo per il recupero dei primi caratteri di una stringa anteponendo il
	 * suffisso "..." se alcuni caratteri vengono omessi.
	 * 
	 * @param str
	 *            stringa sorgente
	 * @param maxLength
	 *            numero di caratteri iniziali della stringa
	 * @return la stringa risultante
	 */
	public static String getTextAbstract(final String str, final Integer maxLength) {
		String output = str;
		if (output != null && output.length() > maxLength) {
			output = output.substring(0, maxLength - 1) + "...";
		}
		return output;
	}

	/**
	 * Metodo per il troncamento di una stringa ad una specifica dimensione.
	 * 
	 * @param str
	 *            stringa
	 * @param size
	 *            dimensione
	 * @return stringa troncata
	 */
	public static String truncateWithPoints(final String str, final Integer size) {
		String output = str;
		if (str != null && str.length() > size) {
			output = str.substring(0, size) + "...";
		}
		return output;
	}

	/**
	 * Abbrevia il label presente in una colonna di un datatable con i puntini.
	 * Inserire il label intero con i puntini utilizzanfo l'apposito metodo
	 * getColumnTooltip
	 * 
	 * Gestisce il caso label vuoto restituisce il classico " - "
	 * 
	 * @param columnLabel
	 *            Stringa da visualizzare nella colonna
	 * @param size
	 *            massimo numero di caratteri del label che si vogliono visualizzare
	 * 
	 * @return la stringa columnLabel se maggiore di size troncata di size e
	 *         aggiungendo i punti '...' alla fine se columnLabel è null ritorna " -
	 *         "
	 * 
	 */
	public static String truncateColumnTable(final String str, final Integer size) {
		return org.apache.commons.lang3.StringUtils.isNotBlank(str) ? StringUtils.truncateWithPoints(str, size) : " - ";
	}

	/**
	 * Quando una colonna necessita di essere abbreviata con i puntini. Usare questo
	 * metodo nel xhtml nel tooltip della colonna per permettere di visualizzare il
	 * dato per esteso qualora questo sia sostituito dai puntini
	 * 
	 * @param columnLabel
	 *            Stringa da visualizzare nella colonna
	 * @param size
	 *            massimo numero di caratteri del label che si vogliono visualizzare
	 * @return Se il columnLabel non supera la size allora ritorna null (Non viene
	 *         visualizzato il tooltip)
	 */
	public static String calculateColumnTooltip(final String str, final Integer size) {
		return org.apache.commons.lang3.StringUtils.isNotEmpty(str) && str.length() > size ? str : null;
	}

	/**
	 * Metodo per verificare la presenza di una sottostringa in una stringa.
	 * 
	 * @param container
	 *            stringa contenitore
	 * @param subString
	 *            stringa contenuta
	 * @return true se il contenitore contiene la stringa contenuta
	 */
	public static boolean containsIgnoreCase(final String container, final String subString) {
		if (container != null) {
			return container.toLowerCase().contains(subString.toLowerCase());
		}
		return false;
	}

	/**
	 * Metodo per la conversione di un intero in una stringa.
	 * 
	 * @param integer
	 *            intero da convertire
	 * @return intero convertito
	 */
	public static String integerToString(final Integer integer) {
		if (integer != null) {
			return integer.toString();
		}
		return null;
	}

	/**
	 * Metodo per ripulire il guid dai simboli {}.
	 * 
	 * @param guid
	 *            il guid con simboli contenitori
	 * @return il guid senza simboli contenitori
	 */
	public static String cleanGuidToString(final Id guid) {
		if (guid != null) {
			return cleanGuidToString(guid.toString());
		}
		return null;
	}

	/**
	 * Metodo per ripulire il guid dai simboli {}.
	 * 
	 * @param guid
	 *            il guid con simboli contenitori
	 * @return il guid senza simboli contenitori
	 */
	public static String cleanGuidToString(final String guid) {
		if (guid != null) {
			return guid.trim().replace("{", "").replace("}", "");
		}
		return null;
	}

	/**
	 * Crea la 'IN' condition per la query per più di 1000 elementi.
	 * 
	 * @param attribute
	 *            - attributo da inserire in IN condition
	 * @param iterator
	 *            lista di oggetti
	 * @param flagStringify
	 *            flag che indica se gli oggetti devono essere inseriti tra apici
	 * @return flusso CSV
	 */
	public static String createInCondition(final String attribute, final Iterator<?> iterator, final Boolean flagStringify) {
		
		StringBuilder buffer = new StringBuilder();

		if (iterator != null && iterator.hasNext()) {
			buffer.append(attribute + " IN (");
			int count = 0;
			while (iterator.hasNext()) {
				final Object obj = iterator.next();
				String tmp = obj.toString();
				if (flagStringify != null && flagStringify) {
					tmp = stringify(obj);
				}
				buffer.append(tmp);
				buffer.append(",");
				count++;
				if (count == MAX_COUNT_IN_CONDITON && iterator.hasNext() && buffer.toString().endsWith(",")) {
					
					String output = buffer.toString();
					output = buffer.substring(0, output.length() - 1);
					output = output + ") OR " + createInCondition(attribute, iterator, flagStringify);
					buffer = new StringBuilder(output);
					break;
				}
			}
		} else {
			buffer.append(" 1 <> 1");
		}
		
		String output = buffer.toString();
		if (output.endsWith(",")) {
			output = output.substring(0, output.length() - 1);
			output = output + ") ";
		}

		return output;
	}
	


	/**
	 * Crea la 'LIKE' condition per la query.
	 * 
	 * @param attribute
	 *            - attributo da inserire in IN condition
	 * @param iterator
	 *            lista di oggetti
	 * @return
	 */
	public static String createLikeLowerCaseCondition(final String attribute, final Iterator<?> iterator) {
		final StringBuilder buffer = new StringBuilder();

		if (iterator != null && iterator.hasNext()) {
			buffer.append("LOWER(" + attribute + ") LIKE ");
			while (iterator.hasNext()) {
				final Object obj = iterator.next();
				final String value = obj.toString().replace("'", "");
				buffer.append("LOWER('%").append(value).append("%')");
				buffer.append(" OR ");
				buffer.append(createLikeLowerCaseCondition(attribute, iterator));
			}
		}
		String output = buffer.toString();
		if (output.endsWith(" OR ")) {
			output = output.substring(0, output.length() - 4);
		}

		return output;
	}

	/**
	 * Dato un oggetto lo trasforma in stringa ed inserisce il risultato tra apici.
	 * 
	 * @param obj
	 *            oggetto
	 * @return oggetto in forma di stringa
	 */
	private static String stringify(final Object obj) {
		return "'" + obj.toString() + "'";
	}

	/**
	 * Recupero estensione file.
	 * 
	 * @param fileName
	 *            nome del file
	 * @return estensione
	 */
	public static String getExtension(final String fileName) {
		return FilenameUtils.getExtension(fileName);
	}

	/**
	 * Metodo che capitalizza il primo carattere.
	 * 
	 * @param str
	 *            stringa
	 * @return stringa con primo carattere superiore
	 */
	public static String firstUppercase(final String str) {
		return str.substring(0, 1).toUpperCase() + str.substring(1);
	}

	/**
	 * Metodo che concatena l'indice di classificazione alla aoo.
	 * 
	 * @param idAoo
	 * @param indiceClassificazione
	 * @return
	 */
	public static String getIndiceClassificazione(final Long idAoo, final String indiceClassificazione) {
		return idAoo + "_" + indiceClassificazione;
	}

	/**
	 * Metodo che ottiene l'indice di classificazione togliendo dalla stringa
	 * l'eventuale aoo.
	 * 
	 * @param indiceClassificazione
	 * @return
	 */
	public static String getIndiceClassificazione(final String indiceClassificazione) {
		String indiceSenzaAoo = "";

		// Controlla se la stringa non è vuota
		if (indiceClassificazione != null && !indiceClassificazione.isEmpty()) {
			// Se il fascicolo è classificato procedere allo split per eliminare
			// l'id aoo dall'indice di classificazione,
			// altrimenti la stringa da ritornare è quella dei fascicoli non
			// classificati
			if (!FilenetStatoFascicoloEnum.NON_CATALOGATO.getNome().equals(indiceClassificazione)) {
				final String[] splittedIndice = indiceClassificazione.split("_");
				if (splittedIndice.length > 1) {
					indiceSenzaAoo = splittedIndice[1];
				} else {
					indiceSenzaAoo = splittedIndice[0];
				}
			} else {
				indiceSenzaAoo = indiceClassificazione;
			}
		}

		return indiceSenzaAoo;
	}

	/**
	 * Metodo che ottiene la descrizione dell'indice di classificazione togliendo dalla stringa
	 * l'indice
	 * 
	 * @param indiceClassificazione - indice con descriione separati da uno spazio
	 * @return descrizione
	 */
	public static String getDescrizioneIndiceClassificazione(final String indiceClassificazione) {
		String descrizione = "";
		
		// Controlla se la stringa non è vuota
		if (indiceClassificazione != null && !indiceClassificazione.isEmpty()) {
			if (!FilenetStatoFascicoloEnum.NON_CATALOGATO.getNome().equals(indiceClassificazione)) {
				final int index = indiceClassificazione.indexOf(" ");
				descrizione = indiceClassificazione.substring(index + 1);
			} else {
				descrizione = indiceClassificazione;
			}
		}
		
		return descrizione;
	}
	
	/**
	 * Ritorna il prefisso dell'oggetto del fascicolo e l'oggetto. Oppure ritorna la
	 * scritta non catalogato.
	 * 
	 * 
	 * @param fascicoloOggetto
	 * @return
	 */
	public static String getPrefixFascicoloProcedimentale(final String fascicoloOggetto) {
		String prefixProcedimentale = null;
		if (!StringUtils.isNullOrEmpty(fascicoloOggetto)) {
			final String[] arr = fascicoloOggetto.split("_");
			if (arr.length >= 3) {
				prefixProcedimentale = arr[0] + "_" + arr[1] + "_";

			}
		}
		return prefixProcedimentale;
	}

	/**
	 * Sostituisce tutti i caratteri speciali della stringa in input con le
	 * corrispondenti entità HTML.
	 * 
	 * @param s
	 *            Stringa in input
	 * @return La stringa con le entità HTML al posto dei caratteri speciali.
	 */
	public static String replaceWithHtmlEntities(final String s) {
		final StringBuilder sb = new StringBuilder();

		if (!StringUtils.isNullOrEmpty(s)) {
			final int n = s.length();
			for (int i = 0; i < n; i++) {
				final char c = s.charAt(i);
				final String str = SUBSTITUTES.get(c);
				if (!StringUtils.isNullOrEmpty(str)) {
					sb.append(str);
				} else {
					sb.append(c);
				}
			}
		}

		return sb.toString();
	}

	/**
	 * Restituisce la clausula ACL per il gruppo in ingresso.
	 * @param groupList
	 * @return ACL Clause
	 */
	public static String getACLClause(final List<String> groupList) {
		final StringBuilder output = new StringBuilder("(");
		for (final String str : groupList) {
			output.append("'" + str + "',");
		}

		return cutLast(output.toString()) + ")";
	}

	/**
	 * Effettua il parsing della stringa in ingresso apportando eventuali modifiche per al corretta rappresentazione
	 * in String di un GUID.
	 * @param inGuidStringyfied
	 * @return
	 */
	public static String restoreGuidFromString(final String inGuidStringyfied) {
		String guidStringyfied = inGuidStringyfied;
		guidStringyfied = org.apache.commons.lang3.StringUtils.prependIfMissing(guidStringyfied, "{");
		return org.apache.commons.lang3.StringUtils.appendIfMissing(guidStringyfied, "}");
	}

	/**
	 * Duplica apici in una stringa.
	 * 
	 * @param value
	 * @return
	 */
	public static String duplicaApici(final String value) {
		int index = 0;
		String sResult = value;

		index = value.indexOf('\'');
		if (index != -1) {
			sResult = value.substring(0, index) + "''" + duplicaApici(value.substring(index + 1));
		}

		return sResult;
	}

	/**
	 * Formatta la stringa: testo, in modo da prepararla per una query di ricerca.
	 * @param testo
	 * @param modalitaRicercaTestuale
	 * @return testo modificato per query
	 */
	public static String formattaStringaPerRicercaTestuale(final String testo, final ModalitaRicercaAvanzataTestoEnum modalitaRicercaTestuale) {
		final StringBuilder queryRicercaTestuale = new StringBuilder(Constants.EMPTY_STRING);
		final String[] testoSplit = testo.split(" ");

		for (int index = 0; index < testoSplit.length; index++) {
			if (testoSplit[index] != null && testoSplit[index].trim().length() > 0) {
				queryRicercaTestuale.append(duplicaApici(testoSplit[index]));

				if (index != testoSplit.length - 1) {
					if (ModalitaRicercaAvanzataTestoEnum.TUTTE_LE_PAROLE.equals(modalitaRicercaTestuale)) {
						queryRicercaTestuale.append(" AND ");
					} else if (ModalitaRicercaAvanzataTestoEnum.UNA_DELLE_PAROLE.equals(modalitaRicercaTestuale)) {
						queryRicercaTestuale.append(" OR ");
					}
				}
			}
		}

		return queryRicercaTestuale.toString();
	}

	/**
	 * Converte il testo in input trasformando tutti i tag html in caratteri testuali.
	 * @param inputMailHtml
	 * @return rappresentazione testuale del file html
	 */
	public static String convertiTestoMailHtml(final String inputMailHtml) {
		String outputMailHtml = Constants.EMPTY_STRING;

		if (!isNullOrEmpty(inputMailHtml)) {
			// Si sostituiscono i tag HTML <p> e <br>
			outputMailHtml = inputMailHtml.replace("</p><br><p>", String.format("%n%n")) // %n%n -> a capo + riga vuota
					.replace("<p><br></p>", String.format("%n%n")).replace("</p><p>", String.format("%n")) // %n -> a capo
					.replace("<br>", String.format("%n")).replace("<p>", Constants.EMPTY_STRING)
					.replace("</p>", Constants.EMPTY_STRING);

			// Si effettua l'unescape delle entità HTML eventualmente inserite dall'utente
			// di cui è stato eseguito l'escape dal componente PrimeFaces "textEditor"
			outputMailHtml = HtmlUtils.htmlUnescape(outputMailHtml);
		}

		return outputMailHtml;
	}

	/**
	 * Effettua l'operazione di LIKE in maniera case insensitive.
	 * @param outer
	 * @param inner
	 * @return true se inner è contenuto in outer a prescindere dal case; false altrimenti
	 */
	public static Boolean likeInsensitive(final String outer, final String inner) {
		Boolean output = false;
		if (StringUtils.isNullOrEmpty(inner)) {
			output = true;
		} else if (!StringUtils.isNullOrEmpty(outer)) {
			output = outer.toUpperCase().contains(inner.toUpperCase());
		}
		return output;
	}
	
	/**
	 * Controlla che la stringa contenga tutte le parole dell'array passatogli
	 * @param word
	 * @param keywords
	 * @return
	 */
	public static boolean containsAllWords(final String word, final String...keywords) {
	    for (final String k : keywords) {
	        if (!word.contains(k)) {
	        	return false;
	        }
	    }
	    return true;
	}
	
	/**
	 * Costruisce una stringa come concatenazione dei parametri in input separati da uno spazio. </br>
	 * Qualora uno dei parametri è blank  {@link org.apache.commons.lang3.StringUtils#join} viene ignorato.
	 * 
	 * Se tutti i parametri sono nulli ritorna null.
	 * 
	 * @param stringList stringhe da unificare.
	 * @return
	 *  Una stringa concatenazione della prima separata da spazi.
	 */
	public static String joinWithwhiteSpace(final String... stringList) {
		final List<String> list = Arrays.asList(stringList);
		final String separator = " ";
		
		return nullIgnoreJoin(separator, list);
	}

	/**
	 * Costruisce una stringa come concatenazione degli elementi di list sfruttando il loro toString dove possibile, ignora inoltre gli elementi nulli della lista.
	 * 
	 * Per concatenare gli elementi di list usa il separatore separator.
	 * 
	 * Molto utile nel caso ci sia la necessita' di fare controlli per costruire dei label composti come ad esempio: <b> numProtocollo/annoProtocollo </b> </br>
	 * ma sia necessario mostrare solo l'anno o il protocollo quando uno dei due non e' presente.
	 * 
	 * Il metodo è liberamente ispirato dal metodo {@link String#join} e dal metodo {@link org.apache.commons.lang3.StringUtils#join}.
	 * 
	 * @param separator	separatore tra i vari elementi della lista.
	 * @param list		lista di oggetti da convertire in stringa.
	 * @return			Una stringa di oggetti stringifati separati dal separator.
	 */
	public static String nullIgnoreJoin(final String separator, final List<? extends Object> list) {
		final List<String> stringList = list.stream()
		.map(o -> o == null ? null : o.toString())
		.filter(s -> org.apache.commons.lang3.StringUtils.isNotBlank(s))
		.collect(Collectors.toList());
		
		return stringList.isEmpty() ? null : String.join(separator, stringList);
	}
	
	/**
	 * Metodo che imposta le variabili relative alla nota della mail
	 * @param nota della mail
	 */
	public static String[] splittaNota(final String nota) {
		String[] notaParts = new String[2];
		//Per le vecchie note
		if (nota.contains("_")) {
			notaParts = nota.split("_");
		} else {
			notaParts[0] = "#0";
			notaParts[1] = nota;
		}
		
		return notaParts;
	}

	/**
	 * Formatta la stringa: testo, in modo da prepararla per una query di ricerca.
	 * @param testo
	 * @param modalitaRicercaTestuale
	 * @return stringa formattata per la query
	 */
	public static String formattaStringaPerRicercaTestualeRapida(final String testo, final RicercaGenericaTypeEnum modalitaRicercaTestuale) {
		final StringBuilder queryRicercaTestuale = new StringBuilder(Constants.EMPTY_STRING);
		final String[] testoSplit = testo.split(" ");

		for (int index = 0; index < testoSplit.length; index++) {
			if (testoSplit[index] != null && testoSplit[index].trim().length() > 0) {
				queryRicercaTestuale.append(duplicaApici(testoSplit[index]));

				if (index != testoSplit.length - 1) {
					if (RicercaGenericaTypeEnum.TUTTE.equals(modalitaRicercaTestuale)) {
						queryRicercaTestuale.append(" AND ");
					} else if (RicercaGenericaTypeEnum.QUALSIASI.equals(modalitaRicercaTestuale)) {
						queryRicercaTestuale.append(" OR ");
					} else {
						queryRicercaTestuale.append(" ");
					}
				}
			}

		}
		return queryRicercaTestuale.toString();
	}

	/**
	 * Restituisce il parametro param - value definito come JSON per l'aggiunta dello stesso ad un oggetto JSON.
	 * @param param
	 * @param value
	 * @param stringifyValue
	 * @param firstParam
	 * @param lastParam
	 * @return json creato per param - value
	 */
	public static String addParamToJsonObject(final String param, final String value, final boolean stringifyValue, 
			final boolean firstParam, final boolean lastParam) {
		final StringBuilder output = new StringBuilder();
		
		if (firstParam) {
			output.append("{");
		}
		
		output.append("\"").append(param).append("\": "); // parametro

		String jsonValue = value;
		
		if (stringifyValue) {
			jsonValue = org.apache.commons.lang3.StringUtils.prependIfMissing(jsonValue, "\"");
			jsonValue = org.apache.commons.lang3.StringUtils.appendIfMissing(jsonValue, "\"");
		}
		
		output.append(jsonValue); // chiave
		
		if (lastParam) {
			output.append("}");
		} else {
			output.append(",");
		}
		
		return output.toString();
	}

	
	/**
	 * Sostituisce alcune tipologie di trattini (- – — _) con uno spazio per evitare che l'OAM blocchi la stringa.
	 * 
	 * @param inString
	 * @return
	 */
	public static String sostituisciTrattiniPerOAM(final String inString) {
		String outString = inString;
		
		if (!isNullOrEmpty(inString)) {
			outString = org.apache.commons.lang3.StringUtils.replaceChars(inString, "-–—_", " ");
		}
		
		return outString;
	}
	
	/**
	 * Rimpiazza il carattere "\\"
	 * 
	 * @param s
	 * @return
	 */
	public static String unescapeString(final String s) {
		String ss = s;
		if (s != null) {
			ss = s.replace("\\\\", "__").replace("\\", "").replace("__", "\\");
		}
		return ss;
	}

	
	/**
	 * @param haystack
	 * @param needle
	 * @return
	 */
	public static int indexOfIgnoreCase(final String haystack, final String needle) {
		if (needle.isEmpty() || haystack.isEmpty()) {
			// Fallback to legacy behavior.
			return haystack.indexOf(needle);
		}
		
		for (int i = 0; i < haystack.length(); ++i) {
			// Early out, if possible.
			if (i + needle.length() > haystack.length()) {
				return -1;
			}
		
			// Attempt to match substring starting at position i of haystack.
			int j = 0;
			int ii = i;
			while (ii < haystack.length() && j < needle.length()) {
				final char c = Character.toLowerCase(haystack.charAt(ii));
				final char c2 = Character.toLowerCase(needle.charAt(j));
				if (c != c2) {
					break;
				}
				j++;
				ii++;
			}
			// Walked all the way to the end of the needle, return the start
			// position that this was found.
			if (j == needle.length()) {
				return i;
			}
		}
		
		return -1;
	}
	
	/**
	 * Rimuove tutti i caratteri non ASCII dalla stringa in input.
	 * @param input
	 * @return stringa formattata
	 */
	public static String cleanNonAsciiCharacters(String input) {
		
		input = input.replace("à", "a");
		input = input.replace("è", "e");
		input = input.replace("é", "e");
		input = input.replace("ò", "o");
		input = input.replace("ù", "u");
		input = input.replace("ì", "i");
		
		final Pattern unicodeOutliers = Pattern.compile("[^\\x20-\\x7E]",
				Pattern.UNICODE_CASE | Pattern.CANON_EQ | Pattern.CASE_INSENSITIVE);
		final Matcher unicodeOutlierMatcher = unicodeOutliers.matcher(input);

		return unicodeOutlierMatcher.replaceAll("");
		 
	}
	
	/**
	 * Restituisce true se la stringa in input contiene numeri, false altrimenti.
	 * @param s
	 * @return true se la stringa contiene un numero, false altrimenti
	 * */
	public static boolean containsDigit(final String s) {
	    boolean containsDigit = false;

	    if (!StringUtils.isNullOrEmpty(s)) {
	        for (final char c : s.toCharArray()) {
	        	containsDigit = Character.isDigit(c);
	            if (containsDigit) {
	                break;
	            }
	        }
	    }

	    return containsDigit;
	}
	
	/**
	 * Questo metodo consente di verificare che il codice fiscale sia formalmente corretto, non vengono presi in considerazione
	 * i numeri presenti nel codice fiscali perché questi, in alcuni casi, vengono sostituiti da caratteri alfabetici.
	 * @param codiceFiscale
	 * @return true se il codice fiscale è formalmente corretto, false altrimenti
	 * */
	public static boolean isCodiceFiscaleFormallyCorrect(final String inCodiceFiscale) {
		String codiceFiscale = inCodiceFiscale;
		if (!isNullOrEmpty(codiceFiscale)) {
			
			codiceFiscale = StringUtils.cleanNonAsciiCharacters(codiceFiscale);
			
			if (codiceFiscale.length() != 16) {
				return false;
			}
			if (StringUtils.containsDigit(codiceFiscale.substring(0, 6))) {
				return false;
			}
			if (Character.isDigit(codiceFiscale.charAt(8))) {
				return false;
			}
			if (Character.isDigit(codiceFiscale.charAt(15))) {
				return false;
			}
		} else {
			return false;
		}
		
		return true;
	}
}
