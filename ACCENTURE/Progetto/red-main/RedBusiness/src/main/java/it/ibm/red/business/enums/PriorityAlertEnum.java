package it.ibm.red.business.enums;

/**
 * The Enum PriorityAlertEnum.
 *
 * @author CPIERASC
 * 
 *         Enum per gestire la priorità dei messagi da visualizzare all'interno
 *         di un pdf.
 */
public enum PriorityAlertEnum {
	/**
	 * Messaggio informativo.
	 */
	INFO(3), 

	/**
	 * Messaggio di avviso.
	 */
	WARN(1), 

	/**
	 * Messaggio di errore.
	 */
	ERROR(0);
	
	/**
	 * Valore.
	 */
	private Integer value; 

	/**
	 * Costruttore.
	 * 
	 * @param inValue	valore
	 */
	PriorityAlertEnum(final Integer inValue) {
		value = inValue;
	}

	/**
	 * Getter valore.
	 * 
	 * @return	valore
	 */
	public Integer getValue() {
		return value;
	}

}
