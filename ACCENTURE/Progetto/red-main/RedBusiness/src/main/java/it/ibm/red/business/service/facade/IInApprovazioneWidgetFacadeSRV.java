package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.persistence.model.Nodo;

/**
 * Facade del servizio che gestisce il widget In Approvazione.
 */
public interface IInApprovazioneWidgetFacadeSRV extends Serializable {

	/**
	 * Ottiene gli ids dei nodi dall'id dell'utente e dall'id dell'aoo.
	 * @param utente
	 * @param inApprovazioneDirigente
	 * @return lista degli ids dei nodi
	 */
	List<Nodo> getNodiFromIdUtenteandIdAOO(UtenteDTO utente, boolean inApprovazioneDirigente);
}
