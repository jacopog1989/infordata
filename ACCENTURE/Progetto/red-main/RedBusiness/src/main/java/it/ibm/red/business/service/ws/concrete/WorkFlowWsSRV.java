/**
 * 
 */
package it.ibm.red.business.service.ws.concrete;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dto.RouteWsDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.WobNumberWsDTO;
import it.ibm.red.business.dto.WorkFlowWsDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.business.service.IOperationWorkFlowSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.service.concrete.AbstractService;
import it.ibm.red.business.service.ws.IWorkFlowWsSRV;
import it.ibm.red.business.service.ws.auth.DocumentServiceAuthorizable;
import it.ibm.red.webservice.model.documentservice.dto.Servizio;
import it.ibm.red.webservice.model.documentservice.types.documentale.Metadato;
import it.ibm.red.webservice.model.documentservice.types.messages.RedAvanzaWorkflowType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedAvviaIstanzaWorkflowType;
import it.ibm.red.webservice.model.documentservice.types.processo.Route;
import it.ibm.red.webservice.model.documentservice.types.processo.WobNumber;
import it.ibm.red.webservice.model.documentservice.types.processo.Workflow;

/**
 * @author APerquoti
 *
 */
@Service
public class WorkFlowWsSRV extends AbstractService implements IWorkFlowWsSRV {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2523097434328693301L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(WorkFlowWsSRV.class.getName());

	/**
	 * Servizio.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;
	
	/**
	 * Servizio.
	 */
	@Autowired
	private IOperationWorkFlowSRV operationWorkflowSRV;

	/**
	 * @see it.ibm.red.business.service.ws.facade.IWorkFlowWsFacadeSRV#avanzaWorkFlow(it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.RedAvanzaWorkflowType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_AVANZA_WORKFLOW)
	public Workflow avanzaWorkFlow(final RedWsClient client, final RedAvanzaWorkflowType request) {
		Workflow output = null;
		boolean isWithRouting = Boolean.FALSE;
		FilenetPEHelper fpeh = null;
		Connection con = null;
		
		try {
		
			con = setupConnection(getDataSource().getConnection(), false);
			
			final UtenteDTO utente = utenteSRV.getById(request.getIdUtente().longValue(), con);
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			
			output = request.getWorkflow();
			final String wobNumber = request.getWorkflow().getWorkFlowNumber();
			final String response = request.getWorkflow().getResponse();
			
			if (request.getWorkflow().getOptionWorkflow() != null) {
				isWithRouting = request.getWorkflow().getOptionWorkflow().isWithRouting();
			}
			
			final Map<String, Object> metadati = new HashMap<>();
			if (request.getWorkflow().getMetadato() != null && !request.getWorkflow().getMetadato().isEmpty()) {

  				for (final Metadato m : request.getWorkflow().getMetadato()) {
					if (m.getValue() != null) {
						metadati.put(m.getKey(), m.getValue());
					} 
				}
				
			}
			
			final WorkFlowWsDTO result = operationWorkflowSRV.avanzaWorkFlowDaWs(wobNumber, metadati, response, isWithRouting, fpeh);
			
			if (result != null) {
				output.setWorkFlowNumber(result.getWorkflowNumber());
				
				for (final WobNumberWsDTO wobNum : result.getWobNumbers()) {
					final WobNumber item = fromDtoToWobNumber(wobNum);
					output.getWobNumber().add(item);
				}
				
			}
			
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'avanzamento del workflow: ", e);
			throw new RedException("Si è verificato un errore durante l'avanzamento del workflow: ", e);
		} finally {
			closeConnection(con);
			logoff(fpeh);
		}
		
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IWorkFlowWsFacadeSRV#avviaIstanzaWorkFlow(it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.RedAvviaIstanzaWorkflowType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_AVVIA_ISTANZA_WORKFLOW)
	public Workflow avviaIstanzaWorkFlow(final RedWsClient client, final RedAvviaIstanzaWorkflowType request) {
		Workflow output = null;
		boolean isWithRouting = Boolean.TRUE;
		FilenetPEHelper fpeh = null;
		Connection con = null;
		
		try {
			
			con = setupConnection(getDataSource().getConnection(), false);
			
			final UtenteDTO utente = utenteSRV.getById(request.getIdUtente().longValue(), con);
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			
			output = request.getWorkflow();
			final String name = request.getWorkflow().getName();
			final String subject = request.getWorkflow().getSubject();
			final String comment = request.getWorkflow().getComment();
			final String response = request.getWorkflow().getResponse();
			
			// isWithRouting
			if (request.getWorkflow().getOptionWorkflow() != null) {
				isWithRouting = request.getWorkflow().getOptionWorkflow().isWithRouting();
			}

			// metadati
			final Map<String, Object> metadati = new HashMap<>();
			if (request.getWorkflow().getMetadato() != null && !request.getWorkflow().getMetadato().isEmpty()) {

  				for (final Metadato m : request.getWorkflow().getMetadato()) {
					if (m.getValue() != null) {
						metadati.put(m.getKey(), m.getValue());
					} 
				}
				
			}
			
			final WorkFlowWsDTO result = operationWorkflowSRV.avviaIstanzaWorkFlowDaWs(name, subject, comment, metadati, response, isWithRouting, fpeh);
			
			if (result != null) {
				output.setWorkFlowNumber(result.getWorkflowNumber());
				output.setSubject(result.getSubject());
				
				for (final WobNumberWsDTO wobNum : result.getWobNumbers()) {
					final WobNumber item = fromDtoToWobNumber(wobNum);
					output.getWobNumber().add(item);
				}
				
			}
			
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'avvio di una nuova istanza di un workflow ", e);
			throw new RedException("Si è verificato un errore durante l'avvio di una nuova istanza di un workflow ", e);
		} finally {
			closeConnection(con);
			logoff(fpeh);
		}
		
		return output;
	}

	/**
	 * Metodo utilizzato per trasformare un'istanza di un DTO iin un'oggetto 'WobNumber'.
	 * 
	 * @param toTrasform
	 * @return un'istanza di tipo 'WobNumber'
	 */
	private static WobNumber fromDtoToWobNumber(final WobNumberWsDTO toTrasform) {
		final WobNumber output = new WobNumber();
		
		try {
			
			output.setStepDescription(toTrasform.getStepDescription());
			output.setStepName(toTrasform.getStepName());
			output.setWobNumber(toTrasform.getWobNumber());

			if (toTrasform.getResponses() != null) {
				output.getResponses().addAll(toTrasform.getResponses());
			}
			
			for (final RouteWsDTO routeDto : toTrasform.getRoute()) {
				final Route route = new Route();
				route.setCondition(routeDto.getCondition());
				route.setDestinationStep(routeDto.getDestinationStep());
				route.setRouteName(routeDto.getRouteName());
				output.getRoute().add(route);
			}
			
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la trasformazione da DTO a WobNumber: ", e);
			throw new RedException("Si è verificato un errore durante la trasformazione da DTO a WobNumber: ", e);
		}
		
		return output;
	}
	
	
}
