package it.ibm.red.business.dao.impl;

import java.io.OutputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.ICodaTrasformazioneDAO;
import it.ibm.red.business.enums.StatoCodaTrasformazioneEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.CodaTrasformazione;
import it.ibm.red.business.utils.FileUtils;

/**
 * The Class CodaTrasformazioneDAO.
 *
 * @author AndreaP
 * 
 * 	Dao per la gestione della coda trasformazione.
 */
@Repository
public class CodaTrasformazioneDAO extends AbstractDAO implements ICodaTrasformazioneDAO {

	private static final String UGUALE_PLACEHOLDER_VIRGOLA = " = ?,  ";
	private static final String WHERE = " WHERE ";
	private static final String UGUALE_PLACEHOLDER = " = ? ";
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 7868445369155518149L;
	/**
	 * tabella.
	 */
	private static final String CODATRASFORMAZIONE = "CODATRASFORMAZIONE_EVO";
	/**
	 * Sequence.
	 */
	private static final String SEQUENCE_CODATRASFORMAZIONE = "SEQ_CODATRASFORMAZIONE_EVO";
	
	/**
	 * Colonna.
	 */
	private static final String IDCODA = "IDCODA";
	/**
	 * Colonna.
	 */
	private static final String IDAOO = "IDAOO";
	/**
	 * Colonna.
	 */
	private static final String IDDOCUMENTO = "IDDOCUMENTO";
	/**
	 * Colonna.
	 */
	private static final String PARAMETRI = "PARAMETRI";
	/**
	 * Colonna.
	 */
	private static final String PROCESSOCHIAMANTE = "PROCESSOCHIAMANTE";
	/**
	 * Colonna.
	 */
	private static final String TIPOTRASFORMAZIONE = "TIPOTRASFORMAZIONE";
	/**
	 * Colonna.
	 */
	private static final String STATO = "STATO";
	/**
	 * Colonna.
	 */
	private static final String DATAINSERIMENTO = "DATAINSERIMENTO";
	/**
	 * Colonna.
	 */
	private static final String MESSAGGIOERRORE = "MESSAGGIOERRORE";
	/**
	 * Colonna.
	 */
	private static final String PRIORITARIA = "PRIORITARIA";
	/**
	 * Colonna.
	 */
	private static final String TIME_ELAPSED_MILLIS = "TIME_ELAPSED_MILLIS";
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(CodaTrasformazioneDAO.class.getName());

	/**
	 * Gets the stato documento from coda trasformazione.
	 *
	 * @param idDocumento the id documento
	 * @param idAoo the id aoo
	 * @param con the con
	 * @return the stato documento from coda trasformazione
	 */
	@Override
	public final int getStatoDocumentoFromCodaTrasformazione(final int idDocumento, final int idAoo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int stato = 0;
		try {
			int index = 1;
			final String querySQL = " SELECT " + STATO
					+ " FROM " + CODATRASFORMAZIONE + " "
					+ WHERE + IDDOCUMENTO + UGUALE_PLACEHOLDER
					+ " AND " + IDAOO + UGUALE_PLACEHOLDER;
			ps = con.prepareStatement(querySQL);
			ps.setInt(index++, idDocumento);
			ps.setInt(index++, idAoo);
			rs = ps.executeQuery();
			if (rs.next()) {
				stato = rs.getInt(STATO);
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero dello stato coda trasformazione ", e);
			throw new RedException("Errore durante il recupero dello stato coda trasformazione", e);
		} finally {
			closeStatement(ps, rs);
		}
		return stato;
	}
	
	/**
	 * Metodo che restituisce il primo elemento nella coda da trasformare lockandolo.
	 * 
	 * @param con
	 * @return
	 */
	@Override
	public CodaTrasformazione getAndLockFirstItem(final int idAoo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		CodaTrasformazione item = null;
		try {
			final String querySQL = "SELECT * FROM " + CODATRASFORMAZIONE + WHERE + IDAOO + " = " + idAoo + " AND " + STATO + " = " + StatoCodaTrasformazioneEnum.DA_ELABORARE.getStatus() + " ORDER BY " + PRIORITARIA + " desc, " + IDCODA + " FOR UPDATE SKIP LOCKED";
			ps = con.prepareStatement(querySQL);
			ps.setMaxRows(1);
			rs = ps.executeQuery();
			if (rs.next()) {
				item = populateItem(rs);
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero del primo item della coda trasformazione ", e);
			throw new RedException("Errore durante il recupero del primo item della coda trasformazione", e);
		} finally {
			closeStatement(ps, rs);
		}
		return item;
	}
	
	private static CodaTrasformazione populateItem(final ResultSet rs) throws SQLException {
		
		byte[] parametri = null;
		final Blob parametriBlob = rs.getBlob(PARAMETRI);
		if (parametriBlob != null) {
			parametri = parametriBlob.getBytes(1, (int) parametriBlob.length());
		}
		
		return new CodaTrasformazione(rs.getInt(IDCODA),
				rs.getInt(IDAOO),
				rs.getString(IDDOCUMENTO),
				rs.getString(PROCESSOCHIAMANTE), 
				rs.getInt(TIPOTRASFORMAZIONE), 
				rs.getInt(STATO), 
				rs.getInt(PRIORITARIA),
				rs.getDate(DATAINSERIMENTO), 
				rs.getString(MESSAGGIOERRORE), 
				parametri);
	}

	/**
	 * @see it.ibm.red.business.dao.ICodaTrasformazioneDAO#changeStatusItem(int,
	 *      int, java.lang.String, java.sql.Connection).
	 */
	@Override
	public void changeStatusItem(final int idCoda, final int stato, final String messaggioErrore, final Connection con) {
		PreparedStatement ps = null;
		try {
			final String updateQuery = "UPDATE " + CODATRASFORMAZIONE + "  SET " 
									+ STATO + UGUALE_PLACEHOLDER_VIRGOLA 
									+ MESSAGGIOERRORE + UGUALE_PLACEHOLDER 
									+ WHERE + IDCODA + UGUALE_PLACEHOLDER;
			
			String message = messaggioErrore;
			if (message != null && message.length() > 3999) {
				message = message.substring(0, 3999);
			}
			
			ps = con.prepareStatement(updateQuery);
			ps.setInt(1, stato);
			ps.setString(2, message);
			ps.setInt(3, idCoda);
			
			ps.executeUpdate();
		
		} catch (final SQLException e) {
			throw new RedException("Errore in fase di aggiornamento dell'item " + idCoda, e);
		} finally {
			closeStatement(ps, null);
		}		
	}

	/**
	 * @see it.ibm.red.business.dao.ICodaTrasformazioneDAO#closeItem(int, long,
	 *      java.sql.Connection).
	 */
	@Override
	public void closeItem(final int idCoda, final long secondElapsed, final Connection connection) {
		PreparedStatement ps = null;
		try {
			final String updateQuery = "UPDATE " + CODATRASFORMAZIONE + " SET " 
									+ STATO + UGUALE_PLACEHOLDER_VIRGOLA 
									+ PARAMETRI + UGUALE_PLACEHOLDER_VIRGOLA 
									+ TIME_ELAPSED_MILLIS + UGUALE_PLACEHOLDER_VIRGOLA 
									+ MESSAGGIOERRORE + UGUALE_PLACEHOLDER
									+ WHERE + IDCODA + UGUALE_PLACEHOLDER;
			
			ps = connection.prepareStatement(updateQuery);
			ps.setInt(1, StatoCodaTrasformazioneEnum.LAVORATA.getStatus());
			ps.setNull(2, Types.VARCHAR);
			ps.setLong(3, secondElapsed);
			ps.setString(4, "");
			ps.setInt(5, idCoda);
			
			ps.executeUpdate();
		
		} catch (final SQLException e) {
			throw new RedException("Errore in fase di aggiornamento dell'item " + idCoda, e);
		} finally {
			closeStatement(ps, null);
		}
		
	}
	
	/**
	 * @see it.ibm.red.business.dao.ICodaTrasformazioneDAO#insert(it.ibm.red.business.persistence.model.CodaTrasformazione,
	 *      java.sql.Connection).
	 */
	@Override
	public int insert(final CodaTrasformazione codaTrasformazione, final Connection con) {
		PreparedStatement blobPS = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int result = 0;
		int idCoda = 0;
		
		try {
			idCoda = (int) getNextId(con, SEQUENCE_CODATRASFORMAZIONE);
			
			ps = con.prepareStatement("INSERT INTO " + CODATRASFORMAZIONE + " (" + IDCODA + ", " + IDAOO + ", " + IDDOCUMENTO + ", " + PROCESSOCHIAMANTE + ", " + TIPOTRASFORMAZIONE 
					+ ", " + STATO + ", " + DATAINSERIMENTO + ", " + MESSAGGIOERRORE + ", " + PRIORITARIA + ") VALUES (?, ?, ?, ?, ?, ?, SYSDATE, ?, ?)");
			
			int index = 1;
			ps.setInt(index++, idCoda);
			ps.setInt(index++, codaTrasformazione.getIdAoo());
			ps.setString(index++, codaTrasformazione.getIdDocumento());
			ps.setString(index++, codaTrasformazione.getProcessoChiamante());
			ps.setInt(index++, codaTrasformazione.getTipoTrasformazione());
			ps.setInt(index++, codaTrasformazione.getStato());
			ps.setString(index++, codaTrasformazione.getMessaggioErrore());
			ps.setInt(index++, codaTrasformazione.getPrioritaria());
			result = ps.executeUpdate();
			
			if (result > 0) {
                blobPS = con.prepareCall("SELECT " + PARAMETRI + " FROM " + CODATRASFORMAZIONE + WHERE + IDCODA + " = ? FOR UPDATE");
                blobPS.setInt(1, idCoda);
                rs = blobPS.executeQuery();

                Blob blob = null;
                if (rs.next()) {
                      blob = rs.getBlob(1);
                      final OutputStream writer = blob.setBinaryStream(1);
                      writer.write(FileUtils.getByteObject(codaTrasformazione.getParametri()));
                      writer.flush();
                      writer.close();
                } else {
                	throw new RedException("Errore nell'inserimento del blob nel DB con idCoda " + idCoda);
                }
			}
		} catch (final Exception e) {
			LOGGER.error("Non è stato possibile recuperare il file originale: " + idCoda, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
			closeStatement(blobPS);
		}
		
		return result;
	}

}
