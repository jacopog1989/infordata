/**
 * 
 */
package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.INotaDocumentoDAO;
import it.ibm.red.business.dto.NotaDocumentoDTO;
import it.ibm.red.business.dto.NotaDocumentoDTO.OrigineNota;
import it.ibm.red.business.dto.NotaDocumentoDescrizioneDTO;
import it.ibm.red.business.enums.ColoreNotaEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.StringUtils;

/**
 * @author a.dilegge
 */
@Repository
public class NotaDocumentoDAO extends AbstractDAO implements INotaDocumentoDAO {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -4410727542082659682L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NotaDocumentoDAO.class);

	/**
	 * @see it.ibm.red.business.dao.INotaDocumentoDAO#getAllNotes(java.lang.String,
	 *      java.lang.Integer, boolean, java.sql.Connection).
	 */
	@Override
	public List<NotaDocumentoDescrizioneDTO> getAllNotes(final String idDocumento, final Integer idAoo, final boolean alsoClosed, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
			
		final StringBuilder query = new StringBuilder();
		try {

			query.append("SELECT nd.*, ");
			query.append(" u.COGNOME as COGNOMEUTENTE, u.NOME as NOMEUTENTE, n.DESCRIZIONE as DESCRIZIONENODO ");
			query.append(" FROM notedocumento nd ");
			query.append(" left join utente u on nd.IDUTENTEOWNER = u.idUtente ");
			query.append(" left join nodo n on nd.IDNODOOWNER = n.idNodo ");
			query.append("WHERE nd.iddocumento = ? ");
			query.append("  AND nd.deleted in (0 , ?) ");
			query.append("  AND n.idaoo = ? ");
			query.append("ORDER BY nd.dataregistrazionenota ");
			
			LOGGER.info("ricerca note documento: " + query.toString());

			ps = con.prepareStatement(query.toString());
			ps.setString(1, idDocumento);
			ps.setInt(2, (alsoClosed ? 1 : 0));
			ps.setInt(3, idAoo);
			rs = ps.executeQuery();
			
			return fetchMultiResult(rs);
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero della lista di note", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}
	
	/**
	 * Ritorna una lista di beans NotaDocumentoDTO in base al resultSet.
	 * @param rs ResultSet
	 * @return NotaDocumentoDTO Beans
	 * @throws SQLException
	 */
	private static List<NotaDocumentoDescrizioneDTO> fetchMultiResult(final ResultSet rs) throws SQLException {
		 final List<NotaDocumentoDescrizioneDTO> resultList = new  ArrayList<>();
		 while (rs.next()) {
			 final NotaDocumentoDescrizioneDTO notaDocumento = new NotaDocumentoDescrizioneDTO();
			 populateVO(notaDocumento, rs);
			 resultList.add(notaDocumento);
		 }
		 return resultList;
	}
	
	private static void populateVO(final NotaDocumentoDescrizioneDTO notaDocumento, final ResultSet rs) throws SQLException {
		if (rs.getInt("IDCOLORE") != 0) {
			notaDocumento.setColore(ColoreNotaEnum.get(rs.getInt("IDCOLORE")));
		}
		final int origine = rs.getInt("ORIGINE");
		if (origine >= 0) {
			final OrigineNota origineEnum = OrigineNota.getById(origine);
			notaDocumento.setOrigine(origineEnum);
		}
		notaDocumento.setIdOrigine(origine);
		
		notaDocumento.setContentNota(rs.getString("TESTONOTA"));
		notaDocumento.setDataRegistrazioneNota(new Date(rs.getTimestamp("DATAREGISTRAZIONENOTA").getTime()));
		notaDocumento.setDataUltimaModificaNota(new Date(rs.getTimestamp("DATAULTIMAMODIFICANOTA").getTime()));
		notaDocumento.setDeleted(rs.getInt("DELETED") == 1);
		notaDocumento.setIdDocumento(rs.getInt("IDDOCUMENTO"));
		notaDocumento.setIdNodoOwner(rs.getInt("IDNODOOWNER"));
		notaDocumento.setIdUtenteOwner(rs.getInt("IDUTENTEOWNER"));
		notaDocumento.setNumeroNota(rs.getInt("NUMERONOTA"));
		notaDocumento.setCognomeUtenteOwner(rs.getString("COGNOMEUTENTE"));
		notaDocumento.setNomeUtenteOwner(rs.getString("NOMEUTENTE"));
		notaDocumento.setDescrizioneNodoOwner(rs.getString("DESCRIZIONENODO"));
		notaDocumento.setPreassegnatarioInterop(rs.getString("PREASSEGNATARIO"));
	}

	/**
	 * @see it.ibm.red.business.dao.INotaDocumentoDAO#insertNote(it.ibm.red.business.dto.NotaDocumentoDTO,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public void insertNote(final NotaDocumentoDTO nota, final String descrPreassegnatario, final Connection con) {
		int index = 1;
		PreparedStatement ps = null;
		final StringBuilder query = new StringBuilder();
		try {
			
			query.append("INSERT INTO notedocumento ( ");
			query.append(" iddocumento, ");
			query.append(" numeronota, ");
			query.append(" idutenteowner, ");
			query.append(" idnodoowner, ");
			query.append(" dataregistrazionenota, ");
			query.append(" testonota, ");
			query.append(" idcolore, ");
			query.append(" origine, ");
			query.append(" dataultimamodificanota, ");
			query.append(" deleted, ");
			query.append(" preassegnatario ");
			query.append(" ) VALUES (?, (select nvl(max(numeronota),0) + 1 from notedocumento nd where nd.iddocumento = ?), ?, ?, sysdate, ?, ?, ?, sysdate, 0,?) ");
			
			ps = con.prepareStatement(query.toString());
			ps.setInt(index++, nota.getIdDocumento());
			ps.setInt(index++, nota.getIdDocumento());
			ps.setInt(index++, nota.getIdUtenteOwner());
			ps.setInt(index++, nota.getIdNodoOwner());
			ps.setString(index++, nota.getContentNota());
			if (nota.getColore() != null) {
				ps.setInt(index++, nota.getColore().getId());
			} else {
				ps.setNull(index++, Types.INTEGER);
			}
			ps.setInt(index++, nota.getIdOrigine());
  
			if (!StringUtils.isNullOrEmpty(descrPreassegnatario)) {
				ps.setString(index++, descrPreassegnatario);
			} else {
				ps.setNull(index++, Types.INTEGER);
			}
 
			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'inserimento della nota del documento", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.INotaDocumentoDAO#deleteNote(java.lang.Integer,
	 *      java.lang.Integer, java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public void deleteNote(final Integer idDocumento, final Integer idAoo, final Integer numeroNota, final Connection con) {
		int index = 1;
		PreparedStatement ps = null;
		final StringBuilder query = new StringBuilder();
		try {
			
			query.append("UPDATE notedocumento ");
			query.append("   SET deleted = 1, ");
			query.append("       dataultimamodificanota = sysdate ");
			query.append(" WHERE iddocumento = ? ");
			query.append("   AND numeronota = ? ");
			query.append("   AND idnodoowner in (select idnodo from nodo where idaoo = ?) ");
			
			ps = con.prepareStatement(query.toString());
			
			ps.setInt(index++, idDocumento);
			ps.setInt(index++, numeroNota);
			ps.setInt(index++, idAoo);
			
			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante la cancellazione (logica) della nota del documento", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}	
	}

}