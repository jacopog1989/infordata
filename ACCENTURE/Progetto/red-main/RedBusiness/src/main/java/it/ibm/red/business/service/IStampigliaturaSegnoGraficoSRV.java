package it.ibm.red.business.service;

import java.util.HashMap;
import java.util.List;

import it.ibm.red.business.dto.StampigliaturaSegnoGraficoDTO;
import it.ibm.red.business.service.facade.IStampigliaturaSegnoGraficoFacadeSRV;

/**
 * 
 * @author Vingenito
 *
 */
public interface IStampigliaturaSegnoGraficoSRV extends IStampigliaturaSegnoGraficoFacadeSRV {
	
	/**
	 * Ottiene il placeholder di sigla.
	 * @param idUtente
	 * @param idUffIstruttore
	 * @param idTipoDoc
	 * @param listStampigliaturaSegnoGrafico
	 * @return mappa placeholder glifo
	 */
	HashMap<String, byte[]> getPlaceholderSigla(Long idUtente, Long idUffIstruttore, Integer idTipoDoc, List<StampigliaturaSegnoGraficoDTO> listStampigliaturaSegnoGrafico);

}
