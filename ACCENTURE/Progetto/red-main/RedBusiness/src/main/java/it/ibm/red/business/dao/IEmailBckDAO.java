package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;

import it.ibm.red.business.dto.EmailBckDTO;

/**
 * 
 * @author a.dilegge
 *
 *	Dao gestione email bck.
 */
public interface IEmailBckDAO extends Serializable {
	
	/**
	 * Recupero content della mail
	 * 
	 * @param messageIdClasseEmail messageIdClasseEmail
	 * @param connection	connessione
	 * @return				mail
	 */
	byte[] getBlobMailOriginale(String messageIdClasseEmail, String casellaPostale, Connection connection);

	
	/**
	 * Salva l'email sul database applicativo. Il blob è completo di header.
	 * 
	 * @param email
	 * @param con
	 * @return
	 */
	int saveEmail(EmailBckDTO email, Connection con);

}
