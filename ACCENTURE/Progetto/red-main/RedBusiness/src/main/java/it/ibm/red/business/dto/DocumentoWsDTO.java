package it.ibm.red.business.dto;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * The Class DocumentoWsDTO.
 *
 * @author m.crescentini
 * 
 *         Classe utilizzata per modellare un documento utilizzato
 *         nell'interfaccia web service.
 */
public class DocumentoWsDTO extends AbstractDTO {

	private static final long serialVersionUID = -1573135429431611056L;

	/**
	 * Guid.
	 */
	private String guid;

	/**
	 * Id documento.
	 */
	private String idDocumento;

	/**
	 * Nne file.
	 */
	private String nomeFile;

	/**
	 * Contenuto.
	 */
	private byte[] content;

	/**
	 * Mime type.
	 */
	private String contentType;

	/**
	 * Numero documento.
	 */
	private Integer numeroDocumento;

	/**
	 * Anno documento.
	 */
	private Integer annoDocumento;

	/**
	 * Numero protocollo.
	 */
	private Integer numeroProtocollo;

	/**
	 * Anno protocollo.
	 */
	private Integer annoProtocollo;

	/**
	 * Stato.
	 */
	private String stato;

	/**
	 * Oggetto.
	 */
	private String oggetto;

	/**
	 * Data modifica.
	 */
	private Date dataModifica;

	/**
	 * Lista allegati.
	 */
	private List<DocumentoWsDTO> allegati;

	/**
	 * Numero versione.
	 */
	private Integer numeroVersione;

	/**
	 * Classe documentale.
	 */
	private String classeDocumentale;

	/**
	 * Security ereditate.
	 */
	private Boolean ereditaSecurity;

	/**
	 * Folder.
	 */
	private String folder;

	/**
	 * Id fascicolo documento.
	 */
	private String idFascicolo;

	/**
	 * Metadati documento.
	 */
	private transient Map<String, Object> metadati;

	/**
	 * Secturity documento.
	 */
	private List<SecurityWsDTO> security;

	/**
	 * @param guid
	 * @param idDocumento
	 * @param nomeFile
	 * @param content
	 * @param contentType
	 * @param numeroDocumento
	 * @param annoDocumento
	 * @param numeroProtocollo
	 * @param annoProtocollo
	 * @param oggetto
	 * @param dataModifica
	 * @param allegati
	 * @param numeroVersione
	 * @param classeDocumentale
	 * @param folder
	 * @param metadati
	 * @param security
	 */
	public DocumentoWsDTO(final String guid, final String idDocumento, final String nomeFile, final byte[] content, final String contentType, final Integer numeroDocumento,
			final Integer annoDocumento, final Integer numeroProtocollo, final Integer annoProtocollo, final String oggetto, final Date dataModifica,
			final List<DocumentoWsDTO> allegati, final Integer numeroVersione, final String classeDocumentale, final String folder, final Map<String, Object> metadati,
			final List<SecurityWsDTO> security) {
		this(guid, idDocumento, nomeFile, content, contentType, numeroDocumento, annoDocumento, numeroProtocollo, annoProtocollo, null, oggetto, dataModifica, allegati,
				numeroVersione, classeDocumentale, null, folder, null, metadati, security);
	}

	/**
	 * @param guid
	 * @param idDocumento
	 * @param nomeFile
	 * @param content
	 * @param contentType
	 * @param numeroDocumento
	 * @param annoDocumento
	 * @param numeroProtocollo
	 * @param annoProtocollo
	 * @param stato
	 * @param oggetto
	 * @param dataModifica
	 * @param allegati
	 * @param numeroVersione
	 * @param classeDocumentale
	 * @param ereditaSecurity
	 * @param folder
	 * @param idFascicolo
	 * @param metadati
	 * @param security
	 */
	public DocumentoWsDTO(final String guid, final String idDocumento, final String nomeFile, final byte[] content, final String contentType, final Integer numeroDocumento,
			final Integer annoDocumento, final Integer numeroProtocollo, final Integer annoProtocollo, final String stato, final String oggetto, final Date dataModifica,
			final List<DocumentoWsDTO> allegati, final Integer numeroVersione, final String classeDocumentale, final Boolean ereditaSecurity, final String folder,
			final String idFascicolo, final Map<String, Object> metadati, final List<SecurityWsDTO> security) {
		super();
		this.guid = guid;
		this.idDocumento = idDocumento;
		this.nomeFile = nomeFile;
		this.content = content;
		this.contentType = contentType;
		this.numeroDocumento = numeroDocumento;
		this.annoDocumento = annoDocumento;
		this.numeroProtocollo = numeroProtocollo;
		this.annoProtocollo = annoProtocollo;
		this.stato = stato;
		this.oggetto = oggetto;
		this.dataModifica = dataModifica;
		this.allegati = allegati;
		this.numeroVersione = numeroVersione;
		this.classeDocumentale = classeDocumentale;
		this.ereditaSecurity = ereditaSecurity;
		this.folder = folder;
		this.idFascicolo = idFascicolo;
		this.metadati = metadati;
		this.security = security;
	}

	/**
	 * Costruttore per la ricerca generica.
	 * 
	 * @param idDocumento
	 * @param content
	 * @param contentType
	 * @param numeroDocumento
	 * @param annoDocumento
	 * @param numeroProtocollo
	 * @param annoProtocollo
	 * @param oggetto
	 */
	public DocumentoWsDTO(final String idDocumento, final byte[] content, final String contentType, final Integer numeroDocumento, final Integer annoDocumento,
			final Integer numeroProtocollo, final Integer annoProtocollo, final String oggetto) {
		super();
		this.idDocumento = idDocumento;
		this.content = content;
		this.contentType = contentType;
		this.numeroDocumento = numeroDocumento;
		this.annoDocumento = annoDocumento;
		this.numeroProtocollo = numeroProtocollo;
		this.annoProtocollo = annoProtocollo;
		this.oggetto = oggetto;
	}

	/**
	 * Costruttore per elenco versioni documento.
	 * 
	 * @param guid
	 * @param idDocumento
	 * @param numeroVersione
	 * @param classeDocumentale
	 * @param dataModifica
	 */
	public DocumentoWsDTO(final String guid, final String idDocumento, final Integer numeroVersione, final String classeDocumentale, final Date dataModifica) {
		super();
		this.guid = guid;
		this.idDocumento = idDocumento;
		this.numeroVersione = numeroVersione;
		this.classeDocumentale = classeDocumentale;
		this.dataModifica = dataModifica;
	}

	/**
	 * Costruttore con il solo content.
	 * 
	 * @param content
	 * @param contentType
	 */
	public DocumentoWsDTO(final byte[] content, final String contentType) {
		super();
		this.content = content;
		this.contentType = contentType;
	}

	/**
	 * Costruttore con il solo numero di versione del documento.
	 * 
	 * @param numeroVersione
	 */
	public DocumentoWsDTO(final Integer numeroVersione) {
		super();
		this.numeroVersione = numeroVersione;
	}

	/**
	 * Csotruttore per documento rappresentante una e-mail.
	 * 
	 * @param guid
	 * @param documentTitle
	 * @param oggetto
	 * @param dataModifica
	 * @param allegati
	 * @param numeroVersione
	 * @param classeDocumentale
	 * @param folder
	 * @param metadati
	 */
	public DocumentoWsDTO(final String guid, final String documentTitle, final String oggetto, final Date dataModifica, final List<DocumentoWsDTO> allegati,
			final Integer numeroVersione, final String classeDocumentale, final String folder, final Map<String, Object> metadati) {
		super();
		this.guid = guid;
		this.idDocumento = documentTitle;
		this.oggetto = oggetto;
		this.dataModifica = dataModifica;
		this.allegati = allegati;
		this.numeroVersione = numeroVersione;
		this.classeDocumentale = classeDocumentale;
		this.folder = folder;
		this.metadati = metadati;
	}

	/**
	 * Costruttore per documento rappresentante l'allegato di una e-mail.
	 * 
	 * @param documentTitle
	 * @param guid
	 * @param classeDocumentale
	 * @param numeroVersione
	 * @param content
	 * @param contentType
	 */
	public DocumentoWsDTO(final String documentTitle, final String guid, final String classeDocumentale, final Integer numeroVersione, final byte[] content,
			final String contentType) {
		super();
		this.guid = guid;
		this.idDocumento = documentTitle;
		this.numeroVersione = numeroVersione;
		this.classeDocumentale = classeDocumentale;
		this.content = content;
		this.contentType = contentType;
	}

	/**
	 * @return the guid
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * @return the idDocumento
	 */
	public String getIdDocumento() {
		return idDocumento;
	}

	/**
	 * @return the nomeFile
	 */
	public String getNomeFile() {
		return nomeFile;
	}

	/**
	 * @return the content
	 */
	public byte[] getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(final byte[] content) {
		this.content = content;
	}

	/**
	 * @return the contentType
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * @param contentType the contentType to set
	 */
	public void setContentType(final String contentType) {
		this.contentType = contentType;
	}

	/**
	 * @return the numeroDocumento
	 */
	public Integer getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * @return the annoDocumento
	 */
	public Integer getAnnoDocumento() {
		return annoDocumento;
	}

	/**
	 * @return the numeroProtocollo
	 */
	public Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * @return the annoProtocollo
	 */
	public Integer getAnnoProtocollo() {
		return annoProtocollo;
	}

	/**
	 * @return the stato
	 */
	public String getStato() {
		return stato;
	}

	/**
	 * @param stato the stato to set
	 */
	public void setStato(final String stato) {
		this.stato = stato;
	}

	/**
	 * @return the oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * @return the dataModifica
	 */
	public Date getDataModifica() {
		return dataModifica;
	}

	/**
	 * @return the allegati
	 */
	public List<DocumentoWsDTO> getAllegati() {
		return allegati;
	}

	/**
	 * @return the numeroVersione
	 */
	public Integer getNumeroVersione() {
		return numeroVersione;
	}

	/**
	 * @return the classeDocumentale
	 */
	public String getClasseDocumentale() {
		return classeDocumentale;
	}

	/**
	 * @return the ereditaSecurity
	 */
	public Boolean isEreditaSecurity() {
		return ereditaSecurity;
	}

	/**
	 * @param ereditaSecurity the ereditaSecurity to set
	 */
	public void setEreditaSecurity(final Boolean ereditaSecurity) {
		this.ereditaSecurity = ereditaSecurity;
	}

	/**
	 * @return the folder
	 */
	public String getFolder() {
		return folder;
	}

	/**
	 * @return the idFascicolo
	 */
	public String getIdFascicolo() {
		return idFascicolo;
	}

	/**
	 * @param idFascicolo the idFascicolo to set
	 */
	public void setIdFascicolo(final String idFascicolo) {
		this.idFascicolo = idFascicolo;
	}

	/**
	 * @return the metadati
	 */
	public Map<String, Object> getMetadati() {
		return metadati;
	}

	/**
	 * @return the security
	 */
	public List<SecurityWsDTO> getSecurity() {
		return security;
	}

}
