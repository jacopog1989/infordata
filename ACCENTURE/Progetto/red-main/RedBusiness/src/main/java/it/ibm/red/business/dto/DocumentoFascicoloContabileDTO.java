package it.ibm.red.business.dto;

/**
 * DTO di un documento fascicolo contabile.
 */
public class DocumentoFascicoloContabileDTO extends AbstractDTO {

	/**
	 * Identificativo documento.
	 */
	private String idDocumento;

	/**
	 * Descrizione.
	 */
	private String descrizione;

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -4529874851684197977L;

	/**
	 * Costruttore del DTO.
	 * @param idDocumento
	 * @param descrizione
	 */
	public DocumentoFascicoloContabileDTO(final String idDocumento, final String descrizione) {
		super();
		this.idDocumento = idDocumento;
		this.descrizione = descrizione;
	}

	/**
	 * Restituisce l'id del documento.
	 * @return id documento
	 */
	public String getIdDocumento() {
		return idDocumento;
	}

	/**
	 * Restituisce la descrizione.
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

}
