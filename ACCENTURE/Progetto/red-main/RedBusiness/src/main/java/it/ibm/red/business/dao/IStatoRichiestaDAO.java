package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;

import it.ibm.red.business.persistence.model.StatoRichiesta;

/**
 * 
 * @author CPIERASC
 *
 *	Interfaccia dao gestione stato richiesta.
 */
public interface IStatoRichiestaDAO extends Serializable {
	
	/**
	 * Metodo per il recupero di tutti gli stati di una richiesta.
	 * 
	 * @param connection	connessione al db
	 * @return				lista stati richiesta
	 */
	Collection<StatoRichiesta> getAll(Connection connection);

}
