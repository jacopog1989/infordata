package it.ibm.red.business.dto;

import it.ibm.red.business.enums.SignErrorEnum;

/**
 * The Class EsitoDTO.
 *
 * @author CPIERASC
 * 
 *         DTO per modellare l'esito di una operazione multipla.
 */
public class EsitoDTO extends AbstractDTO {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -7531233098676889343L;

	/**
	 * Errore.
	 */
	private Enum<?> codiceErrore;

	/**
	 * Esito.
	 */
	private boolean esito;

	/**
	 * Note.
	 */
	private String note;

	/**
	 * Costruttore.
	 */
	public EsitoDTO() {
		super();
	}

	/**
	 * Getter.
	 * 
	 * @return note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * Getter.
	 * 
	 * @return esito
	 */
	public final boolean isEsito() {
		return esito;
	}

	/**
	 * Getter.
	 * 
	 * @return codice errore
	 */
	public final Enum<? extends Object> getCodiceErrore() {
		return codiceErrore;
	}

	/**
	 * Setter.
	 * 
	 * @param inCodiceErrore codice errore
	 */
	public final void setCodiceErrore(final Enum<?> inCodiceErrore) {
		this.codiceErrore = inCodiceErrore;
		if (inCodiceErrore instanceof SignErrorEnum) {
			this.note = ((SignErrorEnum) inCodiceErrore).getMessage();
		}
	}

	/**
	 * Setter.
	 * 
	 * @param inEsito esito
	 */
	public final void setEsito(final boolean inEsito) {
		this.esito = inEsito;
	}

	/**
	 * Setter.
	 * 
	 * @param inNote note
	 */
	public final void setNote(final String inNote) {
		this.note = inNote;
	}

}
