/**
 * 
 */
package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import it.ibm.red.business.dao.IRegistroAusiliarioDAO;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dto.CapitoloSpesaMetadatoDTO;
import it.ibm.red.business.dto.CollectedParametersDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.RegistroDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoContestoProceduraleEnum;
import it.ibm.red.business.enums.TipoMetadatoEnum;
import it.ibm.red.business.enums.TipoRegistroAusiliarioEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.fop.FOPHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.pcollector.ParametersCollector;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IListaDocumentiSRV;
import it.ibm.red.business.service.IRegistroAusiliarioSRV;
import it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV;

/**
 * Service per la gestione dei registri ausiliari.
 * @author APerquoti
 * @author SimoneLungarella
 */
@Service
public class RegistroAusiliarioSRV extends AbstractService implements IRegistroAusiliarioSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -8182644312657284056L;

	/**
	 * Messaggio errore recupero informazioni per la generazione del template.
	 */
	private static final String ERROR_RECUPERO_INFORMAZIONI_TEMPLATE_MSG = "Errore durante il recupero delle informazioni necessarie alla generazione del template.";

	/**
	 * Messaggio errore recupero registri.
	 */
	private static final String ERROR_RECUPERO_REGISTRI_MSG = "Errore durante il recupero dei registri.";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RegistroAusiliarioSRV.class);

	/**
	 * DAO per il recupero delle informazioni sul registro ausiliario dallo strato
	 * di persistenza.
	 */
	@Autowired
	private IRegistroAusiliarioDAO regAusiliarioDAO;

	/**
	 * DAO per il recupero delle informazioni sulla tipologia documento per il quale
	 * deve essere predisposta la registrazione ausiliaria.
	 */
	@Autowired
	private ITipologiaDocumentoFacadeSRV tipologiaDocSRV;
	
	/**
	 * SRV per il recupero delle informazioni dalla lista dei documenti.
	 */
	@Autowired
	private IListaDocumentiSRV listaDocSRV;

	/**
	 * DAO per il recupero delle informazioni sulla tipologia del procedimento per
	 * il quale deve essere predisposta la registrazione ausiliaria.
	 */
	@Autowired
	private ITipoProcedimentoDAO tipoProcedimentoDAO;

	/**
	 * Restituisce tutti i registri configurati sulla base dati.
	 * 
	 * @see it.ibm.red.business.service.facade.IRegistroAusiliarioFacadeSRV#getRegistri()
	 */
	@Override
	public Collection<RegistroDTO> getRegistri() {
		Collection<RegistroDTO> registri = new ArrayList<>();
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			registri = regAusiliarioDAO.getRegistri(connection);

		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_REGISTRI_MSG, e);
			throw new RedException(ERROR_RECUPERO_REGISTRI_MSG, e);
		} finally {
			closeConnection(connection);
		}

		return registri;
	}

	/**
	 * Restituisce tutti i registri validi per coppia tipo documento / tipo
	 * procedimento per il tipo specificato.
	 * 
	 * @see it.ibm.red.business.service.facade.IRegistroAusiliarioFacadeSRV#getRegistri(it.ibm.red.business.enums.TipoRegistroAusiliarioEnum,
	 *      java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public Collection<RegistroDTO> getRegistri(final TipoRegistroAusiliarioEnum tipo, final Integer idTipologiaDocumento, final Integer idTipoProcedimento) {
		Collection<RegistroDTO> registri = new ArrayList<>();
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			registri = regAusiliarioDAO.getRegistri(connection, tipo, idTipologiaDocumento, idTipoProcedimento);

		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_REGISTRI_MSG, e);
			throw new RedException(ERROR_RECUPERO_REGISTRI_MSG, e);
		} finally {
			closeConnection(connection);
		}

		return registri;
	}

	/**
	 * Restituisce i metadati estesi associati al registro ausiliario identificato
	 * dal <code> idRegistro </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IRegistroAusiliarioFacadeSRV#getMetadatiRegistro(java.lang.Integer,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.String)
	 */
	@Override
	public Collection<MetadatoDTO> getMetadatiRegistro(final Integer idRegistro, final UtenteDTO utente, final String documentTitle) {
		Collection<MetadatoDTO> out = null;
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			out = regAusiliarioDAO.getMetadatiRegistro(connection, idRegistro);
			final ParametersCollector collector = regAusiliarioDAO.getCollector(connection, idRegistro);

			// Recuperati i metadati della registrazione, vado a popolare quelli per cui è
			// già stato stabilito un valore in fase di creazione
			final Collection<MetadatoDTO> metadatiToInitialize = getMetadatiRegistrazionePopolatiByDoc(out, idRegistro, documentTitle, utente);

			collector.initParameters(utente, documentTitle, metadatiToInitialize);

		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_INFORMAZIONI_TEMPLATE_MSG, e);
			throw new RedException(ERROR_RECUPERO_INFORMAZIONI_TEMPLATE_MSG, e);
		} finally {
			closeConnection(connection);
		}
		return out;

	}

	/**
	 * Effettua correzioni sulla visibilità, editabilità e obbligatorietà dei
	 * <code> metadatiToConfigure </code> in base alle caratteristiche del documento
	 * identificato dal <code> documentTitle </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IRegistroAusiliarioFacadeSRV#configureMetadatiRegistro(java.lang.Integer,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.String,
	 *      java.util.Collection)
	 */
	@Override
	public void configureMetadatiRegistro(final Integer idRegistro, final UtenteDTO utente, final String documentTitle, final Collection<MetadatoDTO> metadatiToConfigure) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			final ParametersCollector collector = regAusiliarioDAO.getCollector(connection, idRegistro);
			collector.configureMetadati(utente, documentTitle, metadatiToConfigure);
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_INFORMAZIONI_TEMPLATE_MSG, e);
			throw new RedException(ERROR_RECUPERO_INFORMAZIONI_TEMPLATE_MSG, e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Restituisce un collector associato al registro identificato dall'
	 * <code> idRegistro </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IRegistroAusiliarioFacadeSRV#collectParameters(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.Integer, java.util.Collection, boolean)
	 */
	@Override
	public CollectedParametersDTO collectParameters(final UtenteDTO utente, final Integer idRegistro, final Collection<MetadatoDTO> listaMetadatiRegistro,
			final boolean checkConfiguration) {
		CollectedParametersDTO out = null;
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			final ParametersCollector collector = regAusiliarioDAO.getCollector(connection, idRegistro);
			out = collector.collectParameters(utente, listaMetadatiRegistro, checkConfiguration);

		} catch (final Exception e) {
			LOGGER.error("Errore durante la validazione dell'input necessario alla generazione del template.", e);
			throw new RedException("Errore durante la validazione dell'input necessario alla generazione del template.", e);
		} finally {
			closeConnection(connection);
		}
		return out;
	}

	/**
	 * Restituisce il byte array del pdf generato a partire dal template associato
	 * al registro ausiliario identificato dall' <code> id </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IRegistroAusiliarioFacadeSRV#createPdf(java.lang.Integer,
	 *      java.util.Map)
	 */
	@Override
	public byte[] createPdf(final Integer id, final Map<String, String> stringParams) {

		return createPdf(id, stringParams, false);
	}

	/**
	 * Restituisce il byte array del pdf non definitivo generato a partire dal
	 * template associato al registro ausiliario identificato dall'
	 * <code> id </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IRegistroAusiliarioFacadeSRV#createFakePdf(java.lang.Integer,
	 *      java.util.Map)
	 */
	@Override
	public byte[] createFakePdf(final Integer id, final Map<String, String> stringParams) {

		return createPdf(id, stringParams, true);
	}

	/**
	 * Esegue la creazione del pdf associato al template della registrazione
	 * ausiliaria definita dal registro ausilario identificato dall'
	 * <code> id </code>.
	 * 
	 * @param id
	 *            identificativo del registro ausiliario
	 * @param stringParams
	 *            parametri per il popolamento dei placeholder e per il
	 *            completamento del template
	 * @param isFake
	 *            se <code> true </code> i metadati tipici della registrazione
	 *            ausiliaria saranno caratterizzati da uno sfondo colorato, se
	 *            <code> false </code> il pdf generato sarà quello definitivo
	 * @return byte array del pdf generato
	 */
	private byte[] createPdf(final Integer id, final Map<String, String> stringParams, final boolean isFake) {

		if (isFake) {
			stringParams.put("flag-sample", "true");
		} else {
			stringParams.put("flag-sample", "false");
		}

		byte[] out = null;
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			final byte[] xslt = regAusiliarioDAO.getXSLT(connection, id);
			out = FOPHelper.transformMAP2PDF(sanitizeMap(stringParams), xslt);
		} catch (final Exception e) {
			LOGGER.error("Errore durante la creazione del template.", e);
			throw new RedException("Errore durante la creazione del template.", e);
		} finally {
			closeConnection(connection);
		}
		return out;
	}

	/**
	 * Restituisce i metadati validi per il popolamento dei metadati del documento
	 * in uscita.
	 * 
	 * @see it.ibm.red.business.service.facade.IRegistroAusiliarioFacadeSRV#getMetadatiForDocUscita(java.lang.Integer,
	 *      java.lang.String, it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection, boolean)
	 */
	@Override
	public Collection<MetadatoDTO> getMetadatiForDocUscita(final Integer idRegistro, final String documentTitle, final UtenteDTO utente,
			final Collection<MetadatoDTO> listaMetadatiRegistro, final boolean isCreazione) {
		Connection connection = null;
		Collection<MetadatoDTO> metadatiDocUscita = new ArrayList<>();

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			final RegistroDTO registro = regAusiliarioDAO.getRegistro(connection, idRegistro);
			final TipoRegistroAusiliarioEnum tipo = TipoRegistroAusiliarioEnum.get(registro.getIdTipo());

			final ParametersCollector collector = regAusiliarioDAO.getCollector(connection, idRegistro);
			Collection<MetadatoDTO> metadatiDoc = new ArrayList<>();

			switch (tipo) {
			case VISTO:
				// Vengono recuperati i metadati dal documento in entrata
				metadatiDoc = tipologiaDocSRV.recuperaMetadatiEstesi(documentTitle, utente);
				break;
			case OSSERVAZIONE_RILIEVO:
				// Vengono recuperati i metadati dal documento in entrata
				metadatiDoc = recuperaMetadatiOsservazione(idRegistro, documentTitle, utente, connection);
				break;
			case RICHIESTA_ULTERIORI_INFORMAZIONI:
				// Vengono recuperati i metadati dal documento in entrata
				metadatiDoc = tipologiaDocSRV.recuperaMetadatiEstesi(documentTitle, utente);
				break;
			case RESTITUZIONI:
				// Vengono recuperati i metadati dal documento di riferimento al documento della
				// restituzione
				metadatiDoc = recuperaMetadatiRestituzione(documentTitle, utente, isCreazione);
				break;
			default:
				break;
			}

			metadatiDocUscita = collector.prepareMetadatiForDocUscita(metadatiDoc, listaMetadatiRegistro, registro.getNome());

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero della lista dei metadati della tipologia documento in uscita", e);
			throw new RedException("Errore durante il recupero della lista dei metadati della tipologia documento in uscita", e);
		} finally {
			closeConnection(connection);
		}

		return metadatiDocUscita;
	}

	/**
	 * Recupera i metadati dal documento di riferimento della restituzione.
	 * 
	 * @param documentTitle
	 *            Identificativo del documento della restituzione.
	 * @param utente
	 *            Utente in sessione.
	 * @param isCreazione
	 *            Indica se i metadati devono essere recupearti per la creazione o
	 *            per modifica o per ricerca. Questo flag influisce sulla
	 *            valorizzazione dei metadati.
	 * @return Metadati recuperati dal documento di riferimento.
	 */
	private Collection<MetadatoDTO> recuperaMetadatiRestituzione(final String documentTitle, final UtenteDTO utente,
			final boolean isCreazione) {
		
		Collection<MetadatoDTO> metadatiDoc;
		String docTitleRiferimento = null;
		if (!isCreazione) {
			docTitleRiferimento = documentTitle;
		} else {
			final IFilenetCEHelper fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final Document document = fceh.getDocumentByDTandAOO(documentTitle, utente.getIdAoo());
			docTitleRiferimento = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.PROTOCOLLO_RIFERIMENTO_METAKEY);
		}
		metadatiDoc = tipologiaDocSRV.recuperaMetadatiEstesi(docTitleRiferimento, utente);
		return metadatiDoc;
	}

	/**
	 * Recupera i metadati del registro identificato dall' {@code idRegistro} quando
	 * il registro è associato alla tipologia:
	 * TipoRegistroAusiliarioEnum.OSSERVAZIONE_RILIEVO.
	 * 
	 * @param idRegistro
	 *            Identificativo del registro, @see TipoRegistroAusiliarioEnum.
	 * @param documentTitle
	 *            Identificativo del documento.
	 * @param utente
	 *            Utente in sessione.
	 * @param connection
	 *            Connessione al database.
	 * @return Metadati del registro ausiliario recuperati.
	 */
	private Collection<MetadatoDTO> recuperaMetadatiOsservazione(final Integer idRegistro, final String documentTitle,
			final UtenteDTO utente, Connection connection) {
		
		Collection<MetadatoDTO> metadatiDoc;
		final List<String> documentTitles = new ArrayList<>();
		documentTitles.add(documentTitle);
		final List<MasterDocumentRedDTO> mastersCE = new ArrayList<>(listaDocSRV.getMastersFromCEByDocumentTitles(documentTitles, utente, connection));
		MasterDocumentRedDTO master = mastersCE.get(0);				
		
		if (!TipoContestoProceduraleEnum.FLUSSO_CG2.name().equals(master.getCodiceFlusso())) {
			// Vengono recuperati i metadati della tipologia predisposta per le OSSERVAZIONI
			final Integer idTipologiaDocumentoOsservazione = Integer.parseInt(PropertiesProvider.getIstance()
					.getParameterByString(utente.getCodiceAoo() + "." + PropertiesNameEnum.ID_TIPOLOGIA_DOCUMENTO_USCITA_PREDISPONI_OSSERVAZIONE.getKey()));
			final Integer idTipoProcedimento = tipoProcedimentoDAO
					.getIdTipoProcedimentoByTipologiaDocumentoAndRegistroAusiliario(idTipologiaDocumentoOsservazione, idRegistro, connection).intValue();
			metadatiDoc = tipologiaDocSRV.caricaMetadati(idTipologiaDocumentoOsservazione, idTipoProcedimento, null, utente.getIdAoo());
		} else {
			// Vengono recuperati i metadati dal documento in entrata
			metadatiDoc = tipologiaDocSRV.recuperaMetadatiEstesi(documentTitle, utente);
		}
		return metadatiDoc;
	}

	/**
	 * Restituisce i metadati valorizzati dal documento identificato dal
	 * <code> documentTitle </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IRegistroAusiliarioFacadeSRV#getValuesFromDocument(java.lang.Integer,
	 *      java.lang.String, it.ibm.red.business.dto.UtenteDTO)
	 */
	@Override
	public Collection<MetadatoDTO> getValuesFromDocument(final Integer idRegistro, final String documentTitle, final UtenteDTO utente) {
		Collection<MetadatoDTO> metadatiPopolati = new ArrayList<>();

		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			final RegistroDTO registro = regAusiliarioDAO.getRegistro(connection, idRegistro);
			final TipoRegistroAusiliarioEnum tipo = TipoRegistroAusiliarioEnum.get(registro.getIdTipo());

			switch (tipo) {
			case VISTO:

				metadatiPopolati = tipologiaDocSRV.recuperaMetadatiEstesi(documentTitle, utente);
				break;
			case OSSERVAZIONE_RILIEVO:

				metadatiPopolati = tipologiaDocSRV.recuperaMetadatiEstesi(documentTitle, utente);
				break;
			case RICHIESTA_ULTERIORI_INFORMAZIONI:

				metadatiPopolati = tipologiaDocSRV.recuperaMetadatiEstesi(documentTitle, utente);
				break;
			case RESTITUZIONI:

				final IFilenetCEHelper fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
				final Document document = fceh.getDocumentByDTandAOO(documentTitle, utente.getIdAoo());
				final String docTitleRiferimento = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.PROTOCOLLO_RIFERIMENTO_METAKEY);
				metadatiPopolati = tipologiaDocSRV.recuperaMetadatiEstesi(docTitleRiferimento, utente);
				break;
			default:
				break;
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero della lista dei metadati popolati del documento in uscita alla registrazione", e);
			throw new RedException("Errore durante il recupero della lista dei metadati popolati del documento in uscita alla registrazione", e);
		} finally {
			closeConnection(connection);
		}

		return metadatiPopolati;
	}

	/**
	 * Questo metodo consente di recuperare i valori dei metadati del documento in
	 * ingresso da cui viene predisposta la registrazione e popolare i valori dei
	 * metadati del registro con quei valori.
	 * 
	 * @param metadatiToRefine
	 *            i metadati della registrazione recuperati dal database
	 * @param idRegistro
	 *            identificativo del registro ausiliario
	 * @param documentTitle
	 *            del documento sul quale viene effettuata la response
	 * @param utente
	 *            utente in sessione
	 * @return collezione dei metadati
	 */
	private Collection<MetadatoDTO> getMetadatiRegistrazionePopolatiByDoc(final Collection<MetadatoDTO> metadatiToRefine, final Integer idRegistro, final String documentTitle,
			final UtenteDTO utente) {
		final Collection<MetadatoDTO> valuesFromDoc = getValuesFromDocument(idRegistro, documentTitle, utente);

		for (final MetadatoDTO docMetadato : valuesFromDoc) {
			if ("DATA_IMPEGNO_SIRGS".equals(docMetadato.getName())) {
				for (final MetadatoDTO rowMetadato : metadatiToRefine) {
					if ("data-sirgs".equals(rowMetadato.getName()) && docMetadato.getValue4AttrExt() != null) {
						rowMetadato.setSelectedValue(docMetadato.getValue4AttrExt());
					}
				}
			}

			if ("NUMERO_IMPEGNO_SIRGS".equals(docMetadato.getName())) {
				for (final MetadatoDTO rowMetadato : metadatiToRefine) {
					if ("numero-sirgs".equals(rowMetadato.getName()) && docMetadato.getValue4AttrExt() != null) {
						rowMetadato.setSelectedValue(docMetadato.getValue4AttrExt());
					}
				}
			}

			// Recupero dell'eventuale capitolo spesa esistente sul documento
			if (TipoMetadatoEnum.CAPITOLI_SELECTOR.equals(docMetadato.getType())) {
				for (final MetadatoDTO rowMetadato : metadatiToRefine) {
					if ("capitolo-spesa".equals(rowMetadato.getName()) && !StringUtils.isEmpty(docMetadato.getValue4AttrExt())) {
						((CapitoloSpesaMetadatoDTO) rowMetadato).setCapitoloSelected(docMetadato.getValue4AttrExt());
					}
				}
			}

		}

		return metadatiToRefine;
	}

	/**
	 * Consente di rimuovere i parametri <code> null </code> sostituendoli con una
	 * stringa vuota.
	 * 
	 * @param hm
	 *            Mappa da correggere
	 * @return Map Mappa corretta
	 */
	private static Map<String, String> sanitizeMap(final Map<String, String> hm) {
		final Map<String, String> stringParams = new HashMap<>();
		String value = "";
		for (final Entry<String, String> entry : hm.entrySet()) {
			
			final String key = entry.getKey();
			final String valueMap = entry.getValue();
			if (valueMap != null) {
				value = valueMap;
			} else {
				value = "";
			}
			stringParams.put(key, value);
		}
		return stringParams;
	}
}
