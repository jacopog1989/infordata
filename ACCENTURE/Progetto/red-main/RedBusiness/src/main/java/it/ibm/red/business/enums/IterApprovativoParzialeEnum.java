package it.ibm.red.business.enums;

/**
 * The Enum IterApprovativoEnum.
 *
 * @author CPIERASC
 * 
 *         Enum per modellare l'iter approvativo.
 */
public enum IterApprovativoParzialeEnum {
	
	/**
	 * 
	 */
	SIGLA_FIRMA("1", "Parziale Sigla Firma"),
	
	/**
	 * 
	 */
	FIRMA_SIGLA("2", "Parziale Firma Sigla"),
	
	/**
	 * 
	 */
	VISTA_FIRMA("3", "Parziale Vista Firma"),
	
	/**
	 * 
	 */
	FIRMA_VISTA("4", "Parziale Firma Vista");
	

	/**
	 * Valore.
	 */
	private String value;
	
	/**
	 * Descrizione.
	 */
	private String description;
	
	/**
	 * Costruttore.
	 * 
	 * @param inValue		valore
	 * @param inDescription	descrizione
	 */
	IterApprovativoParzialeEnum(final String inValue, final String inDescription) {
		value = inValue;
		description = inDescription;
	}

	/**
	 * Recupera il valore.
	 * @return valore
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Ottiene il valore come intero.
	 * @return valore come intero
	 */
	public Integer getIntValue() {
		return Integer.parseInt(value);
	}
	
	/**
	 * Recupera la descrizione.
	 * @return descrizione
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Metodo per il recupero di un enum a partire dal valore.
	 * 
	 * @param value	valore
	 * @return		enum
	 */
	public static IterApprovativoParzialeEnum get(final String value) {
		IterApprovativoParzialeEnum output = null;
		for (IterApprovativoParzialeEnum q:IterApprovativoParzialeEnum.values()) {
			if (q.getValue().equals(value)) {
				output = q;
				break;
			}
		}
		return output;
	}
}
