/**
 * 
 */
package it.ibm.red.business.dto;

import java.util.Collection;
import java.util.Date;

/**
 * @author m.crescentini
 *
 */
public class RegistrazioneAusiliariaDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -811960466145444563L;

	/**
	 * Il registro associato alla registrazione ausiliaria.
	 */
	private RegistroDTO registroAusiliario;
	
	/**
	 * I metadati associati al {@link #registroAusiliario}, vengono utilizzati per il completamento del template.
	 */
	private Collection<MetadatoDTO> metadatiRegistroAusiliario;

	/**
	 * I metadati estesi associati al {@link #registroAusiliario} sotto forma di XML.
	 * Occore utilizzare @see {@link MetadatiEstesiHelper} per la deserializzazione.
	 */
	private StringBuilder clobMetadatiEstesi;

	/**
	 * L'id della registrazione, fornita da NPS.
	 */
	private String idRegistrazione;
	
	/**
	 * Numero della registrazione, fornito da NPS.
	 */
	private Integer numeroRegistrazione;
	
	/**
	 * Data della registrazione, fornita da NPS.
	 */
	private Date dataRegistrazione;
	
	/**
	 * Indica se la registrazione ausiliaria è stata firmata e portata a termine della procedura.
	 * */
	private boolean documentoDefinitivo;

	/**
	 * Restituisce il registro Ausiliario associato alla registrazione.
	 * @return il registro ausiliario
	 */
	public RegistroDTO getRegistroAusiliario() {
		return registroAusiliario;
	}

	/**
	 * Imposta il registro ausiliario associato alal registrazione.
	 * @param registro ausiliario
	 */
	public void setRegistroAusiliario(final RegistroDTO registroAusiliario) {
		this.registroAusiliario = registroAusiliario;
	}

	/**
	 * Restituisce i metadati del registro ausiliario.
	 * @return metadati registro ausiliario
	 */
	public Collection<MetadatoDTO> getMetadatiRegistroAusiliario() {
		return metadatiRegistroAusiliario;
	}

	/**
	 * Imposta i metadati del registro ausiliario.
	 * @param metadatiRegistroAusiliario
	 */
	public void setMetadatiRegistroAusiliario(final Collection<MetadatoDTO> metadatiRegistroAusiliario) {
		this.metadatiRegistroAusiliario = metadatiRegistroAusiliario;
	}
	
	/**
	 * Restituisce l'xml che rappresenta i metadati estesi.
	 * Per vederlo come lista di Attributi Estesi occorre deserializzarlo utilizzando @see {@link MetadatiEstesiHelper}.
	 * @return metadati estesi come xml
	 */
	public StringBuilder getClobMetadatiEstesi() {
		return clobMetadatiEstesi;
	}

	/**
	 * Imposta l'xml che rappresenta i metadati estesi.
	 * @param clobMetadatiEstesi
	 */
	public void setClobMetadatiEstesi(final StringBuilder clobMetadatiEstesi) {
		this.clobMetadatiEstesi = clobMetadatiEstesi;
	}

	/**
	 * Restituisce l'id della registrazione ausiliaria.
	 * @return id registrazione ausiliaria
	 */
	public String getIdRegistrazione() {
		return idRegistrazione;
	}

	/**
	 * Imposta l'id della registrazione ausiliaria.
	 * @param idRegistrazione
	 */
	public void setIdRegistrazione(final String idRegistrazione) {
		this.idRegistrazione = idRegistrazione;
	}

	/**
	 * Restituisce il numero della registrazione ausiliaria.
	 * @return numero registrazione ausiliaria
	 */
	public Integer getNumeroRegistrazione() {
		return numeroRegistrazione;
	}

	/**
	 * Imposta il numero della registrazione ausiliaria.
	 * @param numeroRegistrazione
	 */
	public void setNumeroRegistrazione(final Integer numeroRegistrazione) {
		this.numeroRegistrazione = numeroRegistrazione;
	}

	/**
	 * Restituisce la data di registrazione del registro ausiliario.
	 * @return data registrazione
	 */
	public Date getDataRegistrazione() {
		return dataRegistrazione;
	}

	/**
	 * Imposta la data di registrazione del registro ausiliario.
	 * @param dataRegistrazione
	 */
	public void setDataRegistrazione(final Date dataRegistrazione) {
		this.dataRegistrazione = dataRegistrazione;
	}

	/**
	 * Restituisce true se la registrazione ausiliaria è stata portata a termine correttamente e non deve essere ancora lavorato.
	 * Restituisce false se non è stato definitivamente elaborato.
	 * @return ture se la registrazione è stata completata senza errori, false altrimenti
	 */
	public boolean isDocumentoDefinitivo() {
		return documentoDefinitivo;
	}

	/**
	 * Imposta il flag che indica lo stato del documento, quando un documento è definitivo vuol dire che la registrazione ausiliaria è stata completata
	 * ed esiste un numero, un id e una data di registrazione memorizzate sullo strato di persistenza.
	 * @param documentoDefinitivo
	 */
	public void setDocumentoDefinitivo(final boolean documentoDefinitivo) {
		this.documentoDefinitivo = documentoDefinitivo;
	}
}