package it.ibm.red.business.dto;

import java.util.Date;

import it.ibm.red.business.enums.TrasformazionePDFInErroreEnum;

/**
 * The Class FileDTO.
 *
 * @author a.dilegge
 * 
 * 	Classe utilizzata per modellare un content recuperato per la firma locale.
 */
public class LocalSignContentDTO extends AbstractDTO {

	/**
	 * Serialization.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * valore del metadato TrasformazionePDFInErrore
	 */
	private TrasformazionePDFInErroreEnum metadato;
	
	/**
	 * Contenuto del file.
	 */
	private byte[] content;
	
	/**
	 * Digest del file.
	 */
	private byte[] digest;
	
	/**
	 * Data della firma
	 */
	private Date dataFirma;
	
	/**
	 * Descrizione.
	 */
	private String errorMsg;
	
	/**
	 * Costruttore.
	 */
	public LocalSignContentDTO() {
		super();
	}

	/**
	 * Restituisce il valore del metadato trasformazionePDFInErrore.
	 * @return
	 */
	public TrasformazionePDFInErroreEnum getMetadato() {
		return metadato;
	}

	/**
	 * Imposta il valore per il medatato del CE trasformazionePDFInErrore.
	 * @param metadato
	 */
	/**
	 * Restituisce il content.
	 * @return content
	 */
	public byte[] getContent() {
		return content;
	}

	/**
	 * Imposta il content.
	 * @param content
	 */
	public void setContent(final byte[] content) {
		this.content = content;
	}

	/**
	 * Restituisce il digest.
	 * @return
	 */
	public byte[] getDigest() {
		return digest;
	}

	/**
	 * Imposta il digest.
	 * @param digest
	 */
	public void setDigest(final byte[] digest) {
		this.digest = digest;
	}

	/**
	 * Restituisce la data della firma.
	 * @return
	 */
	public Date getDataFirma() {
		return dataFirma;
	}

	/**
	 * Imposta la data della firma.
	 * @param dataFirma
	 */
	public void setDataFirma(final Date dataFirma) {
		this.dataFirma = dataFirma;
	}

	/**
	 * Restituisce il messaggi di errore.
	 * @return errorMsg
	 */
	public String getErrorMsg() {
		return errorMsg;
	}
	
	/**
	 * Imposta il messaggio di errore.
	 * @param errorMsg
	 */
	public void setErrorMsg(final String errorMsg) {
		this.errorMsg = errorMsg;
	}
}