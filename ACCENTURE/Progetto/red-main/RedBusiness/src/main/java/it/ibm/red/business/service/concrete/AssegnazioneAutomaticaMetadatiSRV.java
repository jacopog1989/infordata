package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IAssegnazioneAutomaticaMetadatiDAO;
import it.ibm.red.business.dto.AssegnazioneMetadatiDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.IAssegnazioneAutomaticaMetadatiSRV;

/**
 * Servizio di gestione assegnazioni automatiche.
 * 
 * @author SimoneLungarella
 */
@Service
@Component
public class AssegnazioneAutomaticaMetadatiSRV extends AbstractService implements IAssegnazioneAutomaticaMetadatiSRV {

	/**
	 * Costante serial version uid.
	 */
	private static final long serialVersionUID = -9217606282464799213L;

	/**
	 * Messaggio di errore di recupero assegnazione automatica dei metadati, occorre
	 * appendere l'id del documento al messaggio.
	 */
	private static final String ERRORE_RECUPERO_ASSE_AUTO_MSG = "Errore durante il recupero dell'assegnazione automatica dei metadati per il documento con id: ";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AssegnazioneAutomaticaMetadatiSRV.class.getName());

	/**
	 * DAO assegnazione metadati.
	 */
	@Autowired
	private IAssegnazioneAutomaticaMetadatiDAO assegnazioneMetadatiDAO;

	/**
	 * Consente di recupere le assegnazioni automatiche per metadati che esistono
	 * sull'Area Organizzativa identificata dall' <code> idAoo </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAssegnazioneAutomaticaMetadatiFacadeSRV#getByIdAoo(java.lang.Long)
	 */
	@Override
	public List<AssegnazioneMetadatiDTO> getByIdAoo(final Long idAoo) {
		List<AssegnazioneMetadatiDTO> results = new ArrayList<>();
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			results = assegnazioneMetadatiDAO.getByIdAoo(connection, idAoo);
		} catch (final Exception e) {
			LOGGER.error(
					"Errore durante il recupero delle assegnazioni automatiche dei metadati dell'AOO con id: " + idAoo,
					e);
			throw new RedException(
					"Errore durante il recupero delle assegnazioni automatiche dei metadati dell'AOO con id: " + idAoo,
					e);
		} finally {
			closeConnection(connection);
		}

		return results;
	}

	/**
	 * Consente di eliminare tutte le assegnazioni automatiche per metadati
	 * esistenti sull'Area Organizzativa identificata dall' <code> idAoo </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAssegnazioneAutomaticaMetadatiFacadeSRV#deleteByIdAoo(java.lang.Long)
	 */
	@Override
	public void deleteByIdAoo(final Long idAoo) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			assegnazioneMetadatiDAO.deleteByIdAoo(connection, idAoo);
		} catch (final Exception e) {
			LOGGER.error("Errore durante la cancellazione delle assegnazioni automatiche dei metadati dell'AOO con id: "
					+ idAoo, e);
			throw new RedException(
					"Errore durante la cancellazione delle assegnazioni automatiche dei metadati dell'AOO con id: "
							+ idAoo,
					e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Consente di rendere persistente sulla base dati un'assegnazione automatica
	 * valida per l'area organizzativa identificata dall' <code> idAoo </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAssegnazioneAutomaticaMetadatiFacadeSRV#registraAssegnazioneMetadato(it.ibm.red.business.dto.AssegnazioneMetadatiDTO,
	 *      java.lang.Long)
	 */
	@Override
	public void registraAssegnazioneMetadato(final AssegnazioneMetadatiDTO assegnazioneMetadato, final Long idAoo) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			assegnazioneMetadatiDAO.registraAssegnazioneMetadato(connection, assegnazioneMetadato, idAoo);
		} catch (final Exception e) {
			LOGGER.error("Errore durante la registrazione di una assegnazione automatica dei metadati", e);
			throw new RedException("Errore durante la registrazione di una assegnazione automatica dei metadati", e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Consente di eliminare un'assegnazione automatica identificata dall'
	 * <code> idAssegnazione </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAssegnazioneAutomaticaMetadatiFacadeSRV#deleteByIdAssegnazione(java.lang.Integer)
	 */
	@Override
	public void deleteByIdAssegnazione(final Integer idAssegnazione) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			assegnazioneMetadatiDAO.deleteByIdAssegnazione(connection, idAssegnazione);
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'eliminazione dell'assegnazione automatica con id: " + idAssegnazione, e);
			throw new RedException(
					"Errore durante l'eliminazione dell'assegnazione automatica con id: " + idAssegnazione, e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Consente di recuperare un'assegnazione automatica per metadati valida per
	 * l'area organizzativa identificata dall' <code> idAoo </code>. Questo metodo
	 * consente di recuperare qualsiasi tipo di assegnazione automatica specificando
	 * quale dei parametri non deve essere valido nella regola di assegnazione
	 * automatica impostandolo a <code> null </code>.
	 * 
	 * @see it.ibm.red.business.service.IAssegnazioneAutomaticaMetadatiSRV#get(java.lang.Long,
	 *      java.lang.Long, java.lang.Long, java.sql.Connection)
	 */
	@Override
	public List<AssegnazioneMetadatiDTO> get(final Long idAoo, final Long idTipoDocumento,
			final Long idTipoProcedimento, final Connection connection) {
		List<AssegnazioneMetadatiDTO> assegnazioni = new ArrayList<>();

		if (connection != null) {
			try {
				assegnazioni = assegnazioneMetadatiDAO.getAssegnazioniMetadati(connection, idAoo, idTipoDocumento,
						idTipoProcedimento);
			} catch (final Exception e) {
				LOGGER.error(ERRORE_RECUPERO_ASSE_AUTO_MSG + idTipoDocumento, e);
				throw new RedException(ERRORE_RECUPERO_ASSE_AUTO_MSG + idTipoDocumento, e);
			}
		}

		return assegnazioni;
	}

	/**
	 * Consente di recuperare la lista delle assegnazioni automatiche che persistono
	 * dato una tipologia documentale. La tipologia è identificata dall'
	 * <code> idTipoDocumento </code> e si fa riferimento alle regole che valgono
	 * per l'area organizzativa identificata dall' <code> idAoo </code>.
	 * 
	 * @see it.ibm.red.business.service.IAssegnazioneAutomaticaMetadatiSRV#getByTipoDocumento(java.lang.Long,
	 *      java.lang.Long, java.sql.Connection)
	 */
	@Override
	public List<AssegnazioneMetadatiDTO> getByTipoDocumento(final Long idAoo, final Long idTipoDocumento,
			final Connection connection) {
		List<AssegnazioneMetadatiDTO> assegnazioni = new ArrayList<>();

		if (connection != null) {
			try {
				assegnazioni = assegnazioneMetadatiDAO.getAssegnazioniByTipoDoc(idAoo, idTipoDocumento, connection);
			} catch (final Exception e) {
				LOGGER.error(ERRORE_RECUPERO_ASSE_AUTO_MSG + idTipoDocumento, e);
				throw new RedException(ERRORE_RECUPERO_ASSE_AUTO_MSG + idTipoDocumento, e);
			}
		}

		return assegnazioni;
	}

}
