package it.ibm.red.business.service;

import java.sql.Connection;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.facade.IWorkflowFacadeSRV;

/**
 * 
 * @author m.crescentini
 *
 */
public interface IWorkflowSRV extends IWorkflowFacadeSRV {

	/**
	 * Ottiene il nome dello step di processo del workflow.
	 * @param connection
	 * @return nome
	 */
	String getWorkflowNameProcessoStep(Connection connection);
	
	/**
	 * Gestisce la chiusura del documento gestendo gli allacci non protocollati eventualmente esistenti se il documento è In lavorazione o in coda Sospeso.
	 * 
	 * @param workflow del documento in chiusura
	 * @param idDocIngresso documentTitle del documento da chiiudere
	 * @param motivo motivazione dell'annullamento
	 * @param utente utente in sessione
	 * @param fceh helper per la comunicazione con il content engine di Filenet
	 * @param connection connessione al db
	 */
	void gestioneCodaInLavorazioneInSospeso(VWWorkObject workflow, Integer idDocIngresso, Connection con, String motivo,
			UtenteDTO utente, IFilenetCEHelper fceh, PropertiesProvider pp);
}
