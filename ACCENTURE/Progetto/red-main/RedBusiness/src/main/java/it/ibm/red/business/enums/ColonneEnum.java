package it.ibm.red.business.enums;

/**
 * @author VINGENITO
 * Enum che definisce le colonne.
 */
public enum ColonneEnum { 
	
	/**
	 * Colonna.
	 */
	SELEZIONA_CHECKBOX("SEL CHECKBOX", true), 
	
	/**
	 * Colonna.
	 */
	RESP_MENU("RESP MENU", true),
	
	/**
	 * Colonna.
	 */
	ICON_LIBRO_FIRMA("ICON LIBRO FIRMA", true),
	
	/**
	 * Colonna.
	 */
	INFO("INFO", true),
	
	/**
	 * Colonna.
	 */
	DESCRIZIONE("DESCRIZIONE", true),
	
	/**
	 * Colonna.
	 */
	PROCEDIMENTO("PROCEDIMENTO", false),
	
	/**
	 * Colonna.
	 */
	CREAZIONE("CREAZIONE", true),
	
	/**
	 * Colonna.
	 */
	SCADENZA("SCADENZA", false), 
	
	/**
	 * Colonna.
	 */
	MITTENTE("MITTENTE", false),
	
	/**
	 * Colonna.
	 */
	DESTINATARI("DESTINATARI", false),
	
	/**
	 * Colonna.
	 */
	ASSEGNATARIO("ASSEGNATARIO", false),
	
	/**
	 * Colonna.
	 */
	PROTOCOLLAZIONE("PROTOCOLLAZIONE", false),
	
	/**
	 * Colonna.
	 */
	URGENTE("URGENTE", false),
	
	/**
	 * Colonna.
	 */
	SPEDIZIONE("SPEDIZIONE", false),
	
	/**
	 * Colonna.
	 */
	RISERVATO("RISERVATO", false),
	
	/**
	 * Colonna.
	 */
	ASSEGNAZIONE("ASSEGNAZIONE", false),
	
	/**
	 * Colonna.
	 */
	NOTA("NOTA", false),
	
	/**
	 * Colonna.
	 */
	MOTIVAZIONE("MOTIVAZIONE", false),
	
	/**
	 * Colonna.
	 */
	DATA_ASSEGNAZIONE("DATA ASSEGNAZIONE", false),
	
	/**
	 * Colonna.
	 */
	DECRETO_LIQUIDAZIONE("DECRETO_LIQUIDAZIONE", false),
	
	/**
	 * Colonna.
	 */
	UFF_CREATORE("UFF_CRATORE", true);	

	/**
	 * Descrizione.
	 */
	private String descrColonna; 

	/**
	 * Flag visibile di default.
	 */
	private Boolean visibilitaInCodaDefault; 
	
	/**
	 * Costruttore della classe.
	 * @param inDescrColonna
	 * @param inVisibilitaInCodaDefault
	 */
	ColonneEnum(final String inDescrColonna, final Boolean inVisibilitaInCodaDefault) {
		descrColonna = inDescrColonna; 
		visibilitaInCodaDefault = inVisibilitaInCodaDefault; 
	}
	 
	/**
	 * Restituisce la descrizione della colonna.
	 * @return descrColonna
	 */
	public String getDescrColonna() {
		return descrColonna;
	}

	/**
	 * Imposta la descrizione della colonna.
	 * @param descrColonna
	 */
	protected void setDescrColonna(final String descrColonna) {
		this.descrColonna = descrColonna;
	}

	/**
	 * Restituisce il valore del flag visibilitaInCodaDefault.
	 * @return visibilitaInCodaDefault
	 */
	public Boolean getVisibilitaInCodaDefault() {
		return visibilitaInCodaDefault;
	}

	/**
	 * Imposta il valore del flag visibilitaInCodaDefault.
	 * @param visibilitaInCodaDefault
	 */
	protected void setVisibilitaInCodaDefault(final Boolean visibilitaInCodaDefault) {
		this.visibilitaInCodaDefault = visibilitaInCodaDefault;
	}

	/**
	 * Restituisce l'enum associata a pk.
	 * @param pk
	 * @return Enum associata
	 */
	public static ColonneEnum get(final String pk) {
		ColonneEnum output = null;
		for (final ColonneEnum t:ColonneEnum.values()) {
			if (pk.equals(t.getDescrColonna())) {
				output = t;
				break;
			}
		}
		return output;
	}

}