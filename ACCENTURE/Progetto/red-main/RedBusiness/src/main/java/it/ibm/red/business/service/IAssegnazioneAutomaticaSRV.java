package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IAssegnazioneAutomaticaFacadeSRV;

/**
 * Service per la gestione delle assegnazioni automatiche esistenti su un
 * documento.
 * 
 * @author SimoneLungarella
 */
public interface IAssegnazioneAutomaticaSRV extends IAssegnazioneAutomaticaFacadeSRV {

}
