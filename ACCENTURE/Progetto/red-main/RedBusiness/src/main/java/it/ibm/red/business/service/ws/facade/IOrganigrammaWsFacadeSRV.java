package it.ibm.red.business.service.ws.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.webservice.model.documentservice.types.messages.RedGetOrganigrammaType;
import it.ibm.red.webservice.model.documentservice.types.organigramma.Ufficio;


/**
 * The Interface IOrganigrammaWsFacadeSRV.
 *
 * @author a.dilegge
 * 
 *         Facade servizio gestione organigramma web service.
 */
public interface IOrganigrammaWsFacadeSRV extends Serializable {
	
	/**
	 * Ottiene l'organigramma.
	 * @param client
	 * @param request
	 * @return lista di uffici
	 */
	List<Ufficio> getOrganigramma(RedWsClient client, RedGetOrganigrammaType request);
	
}
