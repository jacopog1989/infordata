
package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.constants.Constants.Protocollo;
import it.ibm.red.business.dao.INpsConfigurationDAO;
import it.ibm.red.business.dao.IProtocolliEmergenzaDocDAO;
import it.ibm.red.business.dto.ProtocolloEmergenzaDocDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.persistence.model.NpsConfiguration;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.INpsSRV;
import it.ibm.red.business.service.IProtocolliEmergenzaSRV;
import it.ibm.red.webservice.model.interfaccianps.dto.ParametersElaboraMessaggioProtocollazioneEmergenza;

/**
 * Service che gestisce i protocolli di emergenza.
 */
@Service
@Component
public class ProtocolliEmergenzaSRV extends AbstractService implements IProtocolliEmergenzaSRV {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = 9109661265689686742L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ProtocolliEmergenzaSRV.class.getName());

	/**
	 * DAO.
	 */
	@Autowired
	private IProtocolliEmergenzaDocDAO protocolliEmergenzaDocDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private INpsConfigurationDAO npsConfigurationDAO;

	/**
	 * Service.
	 */
	@Autowired
	private INpsSRV npsSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IAooSRV aooSRV;

	/**
	 * Properties.
	 */
	private PropertiesProvider pp;

	/**
	 * Post construct del service.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.service.IProtocolliEmergenzaSRV#registraInfoEmergenza(java.lang.String,
	 *      java.lang.Long, java.lang.String, java.lang.Integer, java.lang.Integer,
	 *      java.util.Date, java.lang.Integer, java.lang.String, java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public ProtocolloNpsDTO registraInfoEmergenza(final String idDocumento, final Long idAoo, final String codiceAoo, final Integer numeroProtocolloEmergenza,
			final Integer annoProtocolloEmergenza, final Date dataProtocolloEmergenza, final Integer tipoProtocollo, final String oggetto, final String tipologiaDocumento,
			final Connection conn) {
		try {
			
			final boolean isPresent = isProtocolloEmergenzaPresent(numeroProtocolloEmergenza, annoProtocolloEmergenza, idAoo, conn);
			if (isPresent) {
				throw new RedException("Il protocollo di emergenza " + numeroProtocolloEmergenza + "/" + annoProtocolloEmergenza + " è già presente in base dati per l'aoo " + idAoo);
			}
			
			final String idProtocolloEmergenza = generaIdProtocolloEmergenza(codiceAoo, numeroProtocolloEmergenza, annoProtocolloEmergenza);
			
			final ProtocolloEmergenzaDocDTO dto = new ProtocolloEmergenzaDocDTO(idDocumento, idAoo, idProtocolloEmergenza, 
					numeroProtocolloEmergenza, annoProtocolloEmergenza, dataProtocolloEmergenza, tipoProtocollo);
			
			final boolean registrato = protocolliEmergenzaDocDAO.insert(dto, conn) > 0;
			if (registrato) {
				final ProtocolloNpsDTO protocolloNpsDTO = new ProtocolloNpsDTO();
				protocolloNpsDTO.setDataProtocollo(dataProtocolloEmergenza);
				protocolloNpsDTO.setAnnoProtocollo(annoProtocolloEmergenza.toString());
				protocolloNpsDTO.setIdProtocollo(idProtocolloEmergenza);
				protocolloNpsDTO.setNumeroProtocollo(numeroProtocolloEmergenza);
				protocolloNpsDTO.setOggetto(oggetto);
				protocolloNpsDTO.setDescrizioneTipologiaDocumento(tipologiaDocumento);
				return protocolloNpsDTO;
			}

		} catch (final Exception e) {
			LOGGER.error("ProtocolliEmergenzaSRV.registraInfoEmergenza. Errore generico: " + e.getMessage(), e);
			throw new RedException(e);
		}

		return null;
		
	}

	/**
	 * Genera la stringa da registrare come idProtocolloEmergenza
	 * 
	 * @param codiceAoo
	 * @param numeroProtocolloEmergenza
	 * @param annoProtocolloEmergenza
	 * @return "R.E. " + codiceAoo + " " + numeroProtocolloEmergenza + "/" +
	 *         annoProtocolloEmergenza
	 */
	private static String generaIdProtocolloEmergenza(final String codiceAoo, final Integer numeroProtocolloEmergenza, final Integer annoProtocolloEmergenza) {
		return "R.E. " + codiceAoo + " " + numeroProtocolloEmergenza + "/" + annoProtocolloEmergenza;
	}

	/**
	 * @see it.ibm.red.business.service.IProtocolliEmergenzaSRV#isEmergenzaNotUpdated(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean isEmergenzaNotUpdated(final String idProtocolloEmergenza, final Connection connection) {
		boolean isTrue = false;

		try {

			isTrue = protocolliEmergenzaDocDAO.isEmergenzaNotUpdated(idProtocolloEmergenza, connection);

		} catch (final Exception e) {
			LOGGER.error("ProtocolliEmergenzaSRV.isEmergenzaNotUpdated: errore generico.", e);
			throw new RedException(e);
		}

		return isTrue;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IProtocolliEmergenzaFacadeSRV#riconciliaDatiProtocolloUfficiale(it.ibm.red.webservice.model.interfaccianps.dto.ParametersElaboraMessaggioProtocollazioneEmergenza).
	 */
	@Override
	public String riconciliaDatiProtocolloUfficiale(final ParametersElaboraMessaggioProtocollazioneEmergenza parameters) {

		Connection connection = null;
		String errorMessage = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), true);

			// recupera id aoo
			final NpsConfiguration npsConfiguration = npsConfigurationDAO.getByCodiceAoo(parameters.getCodiceAooNPS(), connection);
			if (npsConfiguration == null) {
				throw new RedException("Aoo (" + parameters.getCodiceAooNPS() + ") non riconosciuta");
			}

			// recupera protocollo
			final Integer tipoProtocollo = parameters.isEntrata() ? Protocollo.TIPO_PROTOCOLLO_ENTRATA : Protocollo.TIPO_PROTOCOLLO_USCITA;
			final ProtocolloNpsDTO protocolloNpsDTO = npsSRV.getDatiMinimiByIdProtocollo(npsConfiguration.getIdAoo(), parameters.getIdProtocolloUfficiale(), tipoProtocollo);

			// aggiorna db
			ProtocolloEmergenzaDocDTO dto = new ProtocolloEmergenzaDocDTO(null, (long) npsConfiguration.getIdAoo(), null, parameters.getNumeroProtocolloEmergenza(),
					parameters.getAnnoProtocolloEmergenza(), parameters.getDataProtocolloEmergenza(), tipoProtocollo, parameters.getMessageIdNPS(),
					protocolloNpsDTO.getIdProtocollo(), protocolloNpsDTO.getNumeroProtocollo(), Integer.parseInt(protocolloNpsDTO.getAnnoProtocollo()),
					protocolloNpsDTO.getDataProtocollo(), new Date());

			final int numItemUpdated = protocolliEmergenzaDocDAO.riconcilia(dto, connection);

			// se non sono stati aggiornati item, cerca la causa e riportala al chiamante
			if (numItemUpdated != 1) {

				// check ERRORE_MESSAGGIO_DUPLICATO
				final boolean isMessageIdPresent = protocolliEmergenzaDocDAO.isMessageIdNPSPresent(parameters.getMessageIdNPS(), connection);
				if (isMessageIdPresent) {
					errorMessage = "ERRORE_MESSAGGIO_DUPLICATO";
				} else {
					// protocollo emergenza assente
					errorMessage = "ERRORE_DATI_NON_PRESENTI";
				}

			} else {

				// recupera l'aoo
				final Aoo aoo = aooSRV.recuperaAoo(dto.getIdAoo().intValue(), connection);

				// recupera tutte le informazioni appena aggiornate sulla base dati, compreso
				// l'iddocumento
				dto = protocolliEmergenzaDocDAO.getByIdProtocollo(protocolloNpsDTO.getIdProtocollo(), connection);

				// aggiorna item in coda NPS
				npsSRV.aggiornaItemPostEmergenza(dto, aoo.getCodiceAoo(), connection);

				// aggiorna filenet
				aggiornaClasseDocumentale(aoo, Integer.parseInt(dto.getIdDocumento()), protocolloNpsDTO);

			}

			commitConnection(connection);

		} catch (final Exception e) {
			rollbackConnection(connection);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return errorMessage;

	}

	private void aggiornaClasseDocumentale(final Aoo aoo, final int idDocumento, final ProtocolloNpsDTO protocolloNpsDTO) {
		IFilenetCEHelper fceh = null;

		try {
			final String usernameKey = aoo.getCodiceAoo() + "." + PropertiesNameEnum.AOO_SISTEMA_AMMINISTRATORE.getKey();
			final String username = PropertiesProvider.getIstance().getParameterByString(usernameKey);
			final AooFilenet aooFilenet = aoo.getAooFilenet();

			fceh = FilenetCEHelperProxy.newInstance(username, aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(), aooFilenet.getObjectStore(),
					aoo.getIdAoo());

			final Map<String, Object> metadati = new HashMap<>();
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY), Integer.parseInt(protocolloNpsDTO.getAnnoProtocollo()));
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY), protocolloNpsDTO.getNumeroProtocollo());
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY), protocolloNpsDTO.getDataProtocollo());
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY), protocolloNpsDTO.getIdProtocollo());

			fceh.updateMetadatiOnCurrentVersion(aoo.getIdAoo(), idDocumento, metadati);
		} finally {

			popSubject(fceh);

		}
	}

	/**
	 * @see it.ibm.red.business.service.IProtocolliEmergenzaSRV#isProtocolloEmergenzaPresent(java.lang.Integer,
	 *      java.lang.Integer, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public boolean isProtocolloEmergenzaPresent(final Integer numeroProtocolloEmergenza, final Integer annoProtocolloEmergenza, final Long idAoo, final Connection connection) {
		boolean isTrue = false;

		try {

			isTrue = protocolliEmergenzaDocDAO.isProtocolloEmergenzaPresent(numeroProtocolloEmergenza, annoProtocolloEmergenza, idAoo, connection);

		} catch (final Exception e) {
			LOGGER.error("ProtocolliEmergenzaSRV.isProtocolloEmergenzaPresent: errore generico.", e);
			throw new RedException(e);
		}

		return isTrue;
	}
	
	/**
	 * @see it.ibm.red.business.service.IProtocolliEmergenzaSRV#getInfoEmergenzaPerProtocolloNps(java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public ProtocolloNpsDTO getInfoEmergenzaPerProtocolloNps(String idDocumento, String oggetto, String tipologiaDocumento, Long idAoo, Connection conn) {
		ProtocolloNpsDTO protocolloNps = null;
		
		try {
			ProtocolloEmergenzaDocDTO infoEmergenza = protocolliEmergenzaDocDAO.getByIdDocumento(idDocumento, idAoo, conn);
			
			if (infoEmergenza != null) {
				protocolloNps = new ProtocolloNpsDTO();
				protocolloNps.setDataProtocollo(infoEmergenza.getDataProtocolloEmergenza());
				protocolloNps.setAnnoProtocollo(infoEmergenza.getAnnoProtocolloEmergenza().toString());
				protocolloNps.setIdProtocollo(infoEmergenza.getIdProtocolloEmergenza());
				protocolloNps.setNumeroProtocollo(infoEmergenza.getNumeroProtocolloEmergenza());
				protocolloNps.setOggetto(oggetto);
				protocolloNps.setDescrizioneTipologiaDocumento(tipologiaDocumento);
			}
			
		} catch (Exception e) {
			LOGGER.error("ProtocolliEmergenzaSRV.getInfoEmergenzaPerProtocolloNps: errore generico.", e);
			throw new RedException(e);
		}

		return protocolloNps;
	}

}
