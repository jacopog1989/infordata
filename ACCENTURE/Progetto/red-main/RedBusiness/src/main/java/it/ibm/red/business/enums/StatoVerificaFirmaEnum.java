package it.ibm.red.business.enums;

/**
 * Enum stati verifica firma.
 */
public enum StatoVerificaFirmaEnum {
	
	/**
	 * Valore.
	 */
	DA_LAVORARE(0),
	
	/**
	 * Valore.
	 */
	IN_LAVORAZIONE(1),
	
	/**
	 * Valore.
	 */
	SCARTATO(2),
	 
	/**
	 * Valore.
	 */
	ELABORATO(3),
	
	/**
	 * Valore.
	 */
	IN_ERRORE(4);
	
	
	/**
	 * Stato verifica firma.
	 */
	private int statoVerificaFirma;
	
	/**
	 * Costruttore.
	 * @param statoVerificaFirma
	 */
	StatoVerificaFirmaEnum(final int statoVerificaFirma) {
		this.statoVerificaFirma = statoVerificaFirma;
	}
	
	/**
	 * Restituisce l'indice dello stato verifica firma.
	 * @return id stato verifica firma
	 */
	public int getStatoVerificaFirma() {
		return statoVerificaFirma;
	}
	
	/**
	 * Restituisce lo stato associato all'id.
	 * @param id
	 * @return stato associato all'id
	 */
	public static StatoVerificaFirmaEnum getStatoVerificaFirma(final int id) {
		for (final StatoVerificaFirmaEnum s: StatoVerificaFirmaEnum.values()) {
			if (s.getStatoVerificaFirma() == id) {
				return s;
			}
		}
		return null;
	}
	
	/**
	 * Restituisce lo stato associato all'evento in ingresso.
	 * @param evento
	 * @return enum associata all'evento
	 */
	public static StatoRdSSiebelEnum getByEvento(final String evento) {
		for (final StatoRdSSiebelEnum s: StatoRdSSiebelEnum.values()) {
			if (s.getEvento().equals(evento)) {
				return s;
			}
		}
		return null;
	}
}