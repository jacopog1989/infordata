package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.DetailAssegnazioneDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;

/**
 * Trasformer dal Documento al DetailAssegnazione.
 */
public class FromDocumentoToDetailAssegnazione extends TrasformerCE<DetailAssegnazioneDTO> {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 7443723974005156942L;

	/**
	 * Costruttore.
	 * @param inEnumKey
	 */
	protected FromDocumentoToDetailAssegnazione(final TrasformerCEEnum inEnumKey) {
		super(inEnumKey);
	}
	
	/**
	 * Costruttore.
	 */
	public FromDocumentoToDetailAssegnazione() {
		super(TrasformerCEEnum.FROM_DOCUMENT_TO_DETAIL_ASSEGNAZIONE);
	}

	/**
	 * Esegue la trasformazione del document.
	 * @param document
	 * @param connection
	 * @return DetailAssegnazioneDTO
	 */
	@Override
	public DetailAssegnazioneDTO trasform(final Document document, final Connection connection) {
		Integer numeroDocumento = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
		Boolean isRiservato = ((Integer) getMetadato(document, PropertiesNameEnum.RISERVATO_METAKEY)) == 1;
		String documentTitle = (String) getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
		
		DetailAssegnazioneDTO assegnazione = new DetailAssegnazioneDTO(numeroDocumento, isRiservato, documentTitle);
		addFurtherInformation(document, assegnazione);
		return assegnazione;
	}
	
	/**
	 * @param document
	 * @param assegnazione
	 */
	protected void addFurtherInformation(final Document document, final DetailAssegnazioneDTO assegnazione) {
		// Metodo intenzionalmente vuoto.
	}
}
