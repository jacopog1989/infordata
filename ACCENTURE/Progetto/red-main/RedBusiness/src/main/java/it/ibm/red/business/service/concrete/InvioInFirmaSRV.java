package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.constants.PropertyNames;
import com.filenet.api.core.Document;

import filenet.vw.api.VWRosterQuery;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.PEDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.exception.FilenetException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.TrasformPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IInvioInFirmaSRV;
import it.ibm.red.business.service.ISecuritySRV;
import it.ibm.red.business.service.ITrasformazionePDFSRV;

/**
 * Service invio in firma.
 */
@Service
public class InvioInFirmaSRV extends AbstractService implements IInvioInFirmaSRV {

	/**
	 * Auto generated serial version.
	 */
	private static final long serialVersionUID = 1819756854147027113L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(InvioInFirmaSRV.class.getName());

	/**
	 * Servizio.
	 */
	@Autowired
	private ISecuritySRV securitySRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private ITrasformazionePDFSRV trasformazionePDFSRV;

	/**
	 * @see it.ibm.red.business.service.facade.IInvioInFirmaFacadeSRV#invioInFirma(it.ibm
	 *      .red.business.dto.UtenteDTO, java.util.Collection, java.lang.Long,
	 *      java.lang.Long, java.lang.String, java.lang.Boolean).
	 */
	@Override
	public List<EsitoOperazioneDTO> invioInFirma(final UtenteDTO utente, final Collection<String> wobNumbers, final Long idNodoDestinatarioNew,
			final Long idUtenteDestinatarioNew, final String motivazione, final Boolean urgente) {
		return invioInFirmaSiglaVisto(utente, wobNumbers, idNodoDestinatarioNew, idUtenteDestinatarioNew, motivazione, ResponsesRedEnum.INVIO_IN_FIRMA,
				getValoreUrgenteFromCheckbox(urgente));
	}

	/**
	 * @see it.ibm.red.business.service.facade.IInvioInFirmaFacadeSRV#invioInSigla(it.ibm
	 *      .red.business.dto.UtenteDTO, java.util.Collection, java.lang.Long,
	 *      java.lang.Long, java.lang.String, java.lang.Boolean).
	 */
	@Override
	public List<EsitoOperazioneDTO> invioInSigla(final UtenteDTO utente, final Collection<String> wobNumbers, final Long idNodoDestinatarioNew,
			final Long idUtenteDestinatarioNew, final String motivazione, final Boolean urgente) {
		return invioInFirmaSiglaVisto(utente, wobNumbers, idNodoDestinatarioNew, idUtenteDestinatarioNew, motivazione, ResponsesRedEnum.INVIO_IN_SIGLA,
				getValoreUrgenteFromCheckbox(urgente));
	}

	/**
	 * @see it.ibm.red.business.service.facade.IInvioInFirmaFacadeSRV#
	 *      invioInFirmaMultipla(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection, java.lang.Long, java.lang.Long, java.lang.String,
	 *      java.lang.Boolean).
	 */
	@Override
	public List<EsitoOperazioneDTO> invioInFirmaMultipla(final UtenteDTO utente, final Collection<String> wobNumbers, final Long idNodoDestinatarioNew,
			final Long idUtenteDestinatarioNew, final String motivazione, final Boolean urgente) {

		return invioInFirmaSiglaVisto(utente, wobNumbers, idNodoDestinatarioNew, idUtenteDestinatarioNew, motivazione, ResponsesRedEnum.INVIO_IN_FIRMA_MULTIPLA,
				getValoreUrgenteFromCheckbox(urgente));
	}

	/**
	 * @see it.ibm.red.business.service.facade.IInvioInFirmaSRV#invioInFirmaSiglaVisto(it
	 *      .ibm.red.business.dto.UtenteDTO, java.util.Collection, java.lang.Long,
	 *      java.lang.Long, java.lang.String,
	 *      it.ibm.red.business.enums.ResponsesRedEnum, java.lang.Integer).
	 */
	@Override
	public List<EsitoOperazioneDTO> invioInFirmaSiglaVisto(final UtenteDTO utente, final Collection<String> wobNumbers, final Long idNodoDestinatarioNew,
			final Long idUtenteDestinatarioNew, final String motivazione, final ResponsesRedEnum tipoResponse, final Integer urgente) {
		Connection connection = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		final List<EsitoOperazioneDTO> esitoList = new ArrayList<>(wobNumbers.size());
		EsitoOperazioneDTO esito = null;

		try {
			// Fetch resource
			connection = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final VWRosterQuery vwrq = fpeh.getDocumentWorkFlowsByWobNumbers(wobNumbers);
			final Collection<PEDocumentoDTO> workFlows = TrasformPE.transform(vwrq, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO);

			for (final PEDocumentoDTO wf : workFlows) {
				esito = invioInFirmaPEDocumentoDTO(utente, idNodoDestinatarioNew, idUtenteDestinatarioNew, motivazione, tipoResponse, urgente, connection, fpeh, fceh, wf);

				esitoList.add(esito);
			}
		} catch (final SQLException e) {
			LOGGER.error("Attenzione, errore durante la riassegnazione di uno dei documenti selezionati: impossibile stabilire connessione DB", e);
			esito = new EsitoOperazioneDTO(wobNumbers.toString(), false, "Errore durante il recupero dei workflow");
			esitoList.add(esito);
		} finally {
			closeConnection(connection);
			logoff(fpeh);
			popSubject(fceh);
		}

		return esitoList;
	}

	/**
	 * Esegue l'invio in firma del documento sul PE.
	 * 
	 * @param utente
	 * @param idNodoDestinatarioNew
	 * @param idUtenteDestinatarioNew
	 * @param motivazione
	 * @param tipoResponse
	 * @param urgente
	 * @param connection
	 * @param fpeh
	 * @param fceh
	 * @param wf
	 * @return esito operazione
	 */
	private EsitoOperazioneDTO invioInFirmaPEDocumentoDTO(final UtenteDTO utente, final Long idNodoDestinatarioNew, final Long idUtenteDestinatarioNew, final String motivazione, 
			final ResponsesRedEnum tipoResponse, final Integer urgente,	final Connection connection, final FilenetPEHelper fpeh, final IFilenetCEHelper fceh, final PEDocumentoDTO wf) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wf.getWobNumber());
		final PropertiesProvider pp = PropertiesProvider.getIstance();
		
		try {
			LOGGER.info("START RESPONSE");
			LOGGER.info("INVIO IN..: " + tipoResponse.getDisplayName());
			LOGGER.info("UTENTE: " + utente.getUsername());
			// Individuo tutte le securities
			// Su NSD il flag aggiungiDelegato e' a true ma solo per questa funzionalità
			// inoltre per i fascicoli il flag riservato e' false

			LOGGER.info("START SecuritiesModificaFascicolo");
			ListSecurityDTO securitiesModificaFascicolo = securitySRV.getSecurityPerRiassegnazione(utente, wf.getIdDocumento(), idUtenteDestinatarioNew,
					idNodoDestinatarioNew, null, true, false, false, false, connection);
			// Imposto l'utente alle security dei fascicoli
			securitySRV.verificaSecurities(securitiesModificaFascicolo, connection);

			securitySRV.modificaSecurityFascicoli(utente, securitiesModificaFascicolo, wf.getIdDocumento());

			LOGGER.info("END SecuritiesModificaFascicolo");
			// verifica isFlagRiservato
			final String[] parametriDocumento = new String[] { pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY),
					pp.getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY), PropertyNames.ID };
			final Document doc = fceh.getDocumentByIdGestionale(wf.getIdDocumento(), Arrays.asList(parametriDocumento), null, utente.getIdAoo().intValue(), null,
					pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));
			final Integer numeroDocumento = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
			final boolean isRiservato = ((Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.RISERVATO_METAKEY)) == 1;
			esito.setIdDocumento(numeroDocumento);

			LOGGER.info("START AggiornaEVerificaSecurity");
			// Aggiorno le security per il documento
			if (isRiservato) {
				// Se il flag riservato è true la funzione ritorna dei valori differenti dai
				// valori ritornati per i fascicoli
				securitiesModificaFascicolo = securitySRV.getSecurityPerRiassegnazione(utente, wf.getIdDocumento(), idUtenteDestinatarioNew, idNodoDestinatarioNew, null, true,
						false, isRiservato, false, connection);
				securitySRV.aggiornaEVerificaSecurityDocumento(wf.getIdDocumento(), utente.getIdAoo(), securitiesModificaFascicolo, true, false, fceh, connection);
			} else {
				securitySRV.aggiornaSecurityDocumento(wf.getIdDocumento(), utente.getIdAoo(), securitiesModificaFascicolo, true, false, fceh);
			}

			LOGGER.info("END AggiornaEVerificaSecurity");
			final String[] elencoAssegnazioniAutomatiche = {idNodoDestinatarioNew + "," + idUtenteDestinatarioNew};
			
			LOGGER.info("START Aggiorna WF idNodoDestinatarioNew: " + idNodoDestinatarioNew);
			LOGGER.info("START Aggiorna WF idUtenteDestinatarioNew: " + idUtenteDestinatarioNew);
			aggiornaWorkflowInvio(motivazione, tipoResponse, urgente, fpeh, wf, elencoAssegnazioniAutomatiche);
			LOGGER.info("END Aggiorna WF");

			final Integer idDocumentoDb = Integer.valueOf(wf.getIdDocumento());
			// Aggiorna le security di tutti i documenti allacciati

			LOGGER.info("START Aggiorna SEC e contributi");
			securitySRV.aggiornaSecurityDocumentiAllacciati(utente, idDocumentoDb, securitiesModificaFascicolo, elencoAssegnazioniAutomatiche, fceh, connection);

			// Aggiorna le security di tutti i contributi inseriti
			securitySRV.aggiornaSecurityContributiInseriti(utente, wf.getIdDocumento(), elencoAssegnazioniAutomatiche, fceh, connection);
			LOGGER.info("END Aggiorna SEC e contributi");

			// Si richiama il processo di trasformazione PDF asincrono
			String[] firmatari = null;
			Integer tipoFirma = null;
			Integer idTipoAssegnazione = null;

			switch (tipoResponse) {
			case INVIO_IN_FIRMA:
				idTipoAssegnazione = TipoAssegnazioneEnum.FIRMA.getId();
				firmatari = elencoAssegnazioniAutomatiche;
				tipoFirma = IterApprovativoDTO.ITER_FIRMA_MANUALE;
				break;
			case INVIO_IN_FIRMA_MULTIPLA:
				idTipoAssegnazione = TipoAssegnazioneEnum.FIRMA_MULTIPLA.getId();
				firmatari = elencoAssegnazioniAutomatiche;
				tipoFirma = IterApprovativoDTO.ITER_FIRMA_MANUALE;
				break;
			case INVIO_IN_SIGLA:
				idTipoAssegnazione = TipoAssegnazioneEnum.SIGLA.getId();
				break;
			case INVIO_IN_VISTO:
				idTipoAssegnazione = TipoAssegnazioneEnum.VISTO.getId();
				break;
			default:
				break;
			}

			LOGGER.info("START TRASFORMA PDF");
			trasformazionePDFSRV.avviaTrasformazionePDFPerNuovaAssegnazioneFirmaSiglaVisto(wf.getIdDocumento(), doc.get_Id().toString(), idTipoAssegnazione, utente, firmatari,
					tipoFirma, this.getClass().getSimpleName(), fceh, connection, false);

			LOGGER.info("END TRASFORMA PDF");

			esito.setNote(tipoResponse.getDisplayName() + " del documento effettuato con successo.");
			esito.setEsito(true);
		} catch (final FilenetException eFile) {
			LOGGER.error("Attenzione, avanzamento del workFlow con wobNumber: " + wf.getWobNumber() + " non riuscito.", eFile);
			esito.setNote("Attenzione, avanzamento del workflow non riuscito.");
			LOGGER.info("END RESPONSE FILENET EXCEPTION");
		} catch (final Exception ex) {
			LOGGER.error("Attenzione, errore durante Invia_IN_", ex);
			esito.setNote("Errore durante il processamento della richiesta.");
			LOGGER.info("END RESPONSE  EXCEPTION");
		}
		LOGGER.info("END RESPONSE NO EXCEPTION");
		return esito;
	}

	/**
	 * Aggiorna il workflow per l'invio impostando tutti i metadati necessari.
	 * 
	 * @param motivazione
	 * @param tipoResponse
	 * @param urgente
	 * @param fpeh
	 * @param wf
	 * @param elencoAssegnazioniAutomatiche
	 */
	private static void aggiornaWorkflowInvio(final String motivazione, final ResponsesRedEnum tipoResponse, final Integer urgente,
			final FilenetPEHelper fpeh, final PEDocumentoDTO wf, final String[] elencoAssegnazioniAutomatiche) {
		PropertiesProvider prop = PropertiesProvider.getIstance();
		
		Map<String, Object> metadatiWorkflow = new HashMap<>();
		metadatiWorkflow.put(prop.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), motivazione);
		metadatiWorkflow.put(prop.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY), wf.getTipoAssegnazioneId());
		metadatiWorkflow.put(prop.getParameterByKey(PropertiesNameEnum.URGENTE_METAKEY), urgente.toString());
		metadatiWorkflow.put(prop.getParameterByKey(PropertiesNameEnum.ELENCO_LIBROFIRMA_METAKEY), elencoAssegnazioniAutomatiche);
		
		// Se la response è 'Invio in Firma' o 'Invio in Firma Multipla', si resetta il metadato del workflow relativo all'avvio del processo di firma asincrona
		if ((ResponsesRedEnum.INVIO_IN_FIRMA.equals(tipoResponse) || ResponsesRedEnum.INVIO_IN_FIRMA_MULTIPLA.equals(tipoResponse))
				&& Boolean.TRUE.equals(wf.getFirmaAsincronaAvviata())) {
			metadatiWorkflow.put(prop.getParameterByKey(PropertiesNameEnum.FLAG_FIRMA_ASINCRONA_AVVIATA_WF_METAKEY), false);
		}
		
		fpeh.nextStep(wf.getWobNumber(), metadatiWorkflow, tipoResponse.getResponse());
	}

	/**
	 * @param checked
	 * @return
	 */
	private static Integer getValoreUrgenteFromCheckbox(final Boolean checked) {
		return checked != null && checked ? 1 : 0;
	}
}
