package it.ibm.red.business.dto;

/**
 * The Class EsitoPredisponiDocumentoDTO.
 *
 * @author m.crescentini
 * 
 * 	DTO per modellare l'esito di una operazione di predisposizione documento.
 */
public class EsitoPredisponiDocumentoDTO extends EsitoDTO {
	
	private static final long serialVersionUID = -146456788361117415L;

    /**
	 * Dettaglio.
	 */
	private DetailDocumentRedDTO docPredisposto;

	
	/**
	 * @return the docPredisposto
	 */
	public DetailDocumentRedDTO getDocPredisposto() {
		return docPredisposto;
	}

	/**
	 * @param docPredisposto the docPredisposto to set
	 */
	public void setDocPredisposto(final DetailDocumentRedDTO docPredisposto) {
		this.docPredisposto = docPredisposto;
	}
	
}
