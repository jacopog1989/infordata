/**
 * 
 */
package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.FadAmministrazioneDTO;
import it.ibm.red.business.dto.FadRagioneriaDTO;
import it.ibm.red.business.dto.RaccoltaFadDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.service.facade.IFepaFacadeSRV;

/**
 * @author APerquoti
 *
 */
public interface IFepaSRV extends IFepaFacadeSRV {
	
	/**
	 * @param con
	 * @return
	 */
	List<FadAmministrazioneDTO> getComboAmministrazioni(Connection con);
	
	/**
	 * @param codiceAmministrazione
	 * @param con
	 * @return
	 */
	FadRagioneriaDTO findRagioneriaByCodiceAmministrazione(String codiceAmministrazione, Connection con);
	
	/**
	 * Creazione Raccolta fad invocando DEMBIL
	 * 
	 * @param fdaDTO 			- DTO contenente solo i dati per la creazione di una raccolta
	 * @param processoManuale	- flag utilizzato per la creazione manuale della raccolta
	 * @param utente			- informazioni utente
	 * @param connection		- eventuale connessione se si proviene da un'altro service (se null verrà creata)
	 * @return
	 */
	String creaRaccoltaFad(RaccoltaFadDTO fdaDTO, Boolean processoManuale, UtenteDTO utente, Connection connection);
	
	/**
	 * 
	 * @param fadDTO
	 * @param utente
	 * @param processoManuale
	 */
	void inviaDocumentoInSospeso(RaccoltaFadDTO fadDTO, UtenteDTO utente, Boolean processoManuale);

}
