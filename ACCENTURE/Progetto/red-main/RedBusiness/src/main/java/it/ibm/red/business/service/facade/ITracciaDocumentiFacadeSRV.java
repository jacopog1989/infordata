package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.ProcedimentoTracciatoDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Facade del servizio che gestisce il tracciamento documenti.
 */
public interface ITracciaDocumentiFacadeSRV extends Serializable {

	/**
	 * Traccia il documento.
	 * @param idUfficio
	 * @param idUtente
	 * @param idRuolo
	 * @param idDocumento
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO tracciaDocumento(Long idUfficio, Long idUtente, Long idRuolo, int idDocumento);
	
	/**
	 * Rimuove il documento.
	 * @param idUfficio
	 * @param idUtente
	 * @param idRuolo
	 * @param idDocumento
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO rimuoviDocumento(Long idUfficio, Long idUtente, Long idRuolo, int idDocumento);
	
	/**
	 * Rimuove i documenti.
	 * @param idUfficio
	 * @param idUtente
	 * @param idRuolo
	 * @param idDocumenti
	 * @return esiti dell'operazione
	 */
	Collection<EsitoOperazioneDTO> rimuoviDocumenti(Long idUfficio, Long idUtente, Long idRuolo, Collection<Integer> idDocumenti);
	
	/**
	 * Traccia i documenti.
	 * @param idUfficio
	 * @param idUtente
	 * @param idRuolo
	 * @param idDocumenti
	 * @return esiti dell'operazione
	 */
	Collection<EsitoOperazioneDTO> tracciaDocumenti(Long idUfficio, Long idUtente, Long idRuolo, Collection<Integer> idDocumenti);
	
	/**
	 * Verifica se vi sono tracciamenti singoli.
	 * @param utente
	 * @return true o false
	 */
	boolean hasTracciamentiSingoli(UtenteDTO utente);
	
	/**
	 * Verifica se il documento è tracciato.
	 * @param utente
	 * @param idDocumento
	 * @return true o false
	 */
	boolean isDocumentoTracciato(UtenteDTO utente, int idDocumento);
	
	/**
	 * Restituisce la lista dei documenti tracciati dall'utente in input.
	 * 
	 * @param utente
	 * @return
	 */
	Collection<ProcedimentoTracciatoDTO> loadProcedimentiTracciati(UtenteDTO utente);
	
	/**
	 * Controlla se per l'utente esistono documenti tracciati.
	 * 
	 * @param utente
	 * @return true se l'utente ha documenti tracciati, false altrimenti
	 */
	boolean hasProcedimentiTracciati(UtenteDTO utente);

	/**
	 * Traccia il documento.
	 * @param utente
	 * @param wobNumber
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO tracciaDocumento(UtenteDTO utente, String wobNumber);

	/**
	 * Rimuove il documento.
	 * @param utente
	 * @param wobNumber
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO rimuoviDocumento(UtenteDTO utente, String wobNumber);
	
	/**
	 * Traccia i documenti.
	 * @param utente
	 * @param wobNumber
	 * @return esiti dell'operazione
	 */
	Collection<EsitoOperazioneDTO> tracciaDocumento(UtenteDTO utente, Collection<String> wobNumber);

	/**
	 * Rimuove i documenti.
	 * @param utente
	 * @param wobNumber
	 * @return esiti dell'operazione
	 */
	Collection<EsitoOperazioneDTO> rimuoviDocumento(UtenteDTO utente, Collection<String> wobNumber);
	
	
}