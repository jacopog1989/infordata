package it.ibm.red.business.dto;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.IconaExtensionEnum;
import it.ibm.red.business.enums.ValueMetadatoVerificaFirmaEnum;
import it.ibm.red.business.utils.StringUtils;

/**
 * DTO per la gestione degli allegati.
 */
public class AllegatoDTO extends AbstractAllegatoDTO {

	/**
	 * La costante serial Version UID.
	 */
	private static final long serialVersionUID = 1593704137322105868L;

	/**
	 * Identificativo documento.
	 */
	private String idDocumento;

	/**
	 * Docuemnt title.
	 */
	private String documentTitle;

	/**
	 * Formato.
	 */
	private Integer formato;

	/**
	 * Identificativo tipologia documento.
	 */
	private Integer idTipologiaDocumento;

	/**
	 * Tipologia.
	 */
	private String tipologia;

	/**
	 * Barcode.
	 */
	private String barcode;

	/**
	 * Formato originale.
	 */
	private Boolean formatoOriginale;

	/**
	 * Flag da firmare.
	 */
	private Integer daFirmare;

	/**
	 * Tipo mime.
	 */
	private String mimeType;

	/**
	 * Content.
	 */
	private byte[] content;

	/**
	 * Posizione.
	 */
	private String posizione;

	/**
	 * Lista versioni.
	 */
	private List<VersionDTO> versions;

	/**
	 * MASCHERA INSERIMENTO / MODIFICA START.
	 **************************************************/
	private FormatoAllegatoEnum formatoSelected;

	/**
	 * Flag posizione.
	 */
	private boolean posizioneVisible;

	/**
	 * Flag mantieni formato originale.
	 */
	private boolean checkMantieniFormatoOriginaleVisible;

	/**
	 * Flag mantieni formato originale disabilitato.
	 */
	private boolean checkMantieniFormatoOriginaleDisable;

	/**
	 * Flag barcode visibile.
	 */
	private boolean barcodeVisible;

	/**
	 * Flag file visibile.
	 */
	private boolean uploadFileVisible;

	/**
	 * Flag da firmare.
	 */
	private boolean daFirmareBoolean;

	/**
	 * Flag da firmare visibile.
	 */
	private boolean checkDaFirmareVisible;

	/**
	 * Flag firma visibile.
	 */
	private Boolean firmaVisibile;

	/**
	 * Flag visibile check firma visibile.
	 */
	private boolean checkFirmaVisibileVisible;

	/**
	 * Flag copia conforme.
	 */
	private Boolean copiaConforme;

	/**
	 * Identificativo ufficio copia conforme.
	 */
	private Long idUfficioCopiaConforme;

	/**
	 * Identificativo utente copia conforme.
	 */
	private Long idUtenteCopiaConforme;

	/**
	 * Mi serve nella maschera di creazione modifica ed e' costituito da:
	 * idUfficioCopiaConforme + " " + idUtenteCopiaConforme.
	 */
	private String idCopiaConforme;

	/**
	 * Flag copia conforme visibile.
	 */
	private boolean checkCopiaConformeVisible;

	/**
	 * Flag copia conforme disabilitato.
	 */
	private boolean checkCopiaConformeDisabled;
	/** COPIA CONFORME -> END **/

	/**
	 * In maschera di modifica solo alcuni campi si possono modificare, se
	 * inModifica = true quei campi sono disabilitati.
	 */
	private boolean inModifica;

	/**
	 * per capire se è stato appena caricato oppure no e quindi abilitare o
	 * disabilitare il download.
	 */
	private boolean newAllegato;

	/**
	 * utilizzato per il download di un allegato alla mail.
	 */
	private String nodeIdFromHierarchicalFile;

	/**
	 * Solo per protocollazione mail: indica se l'allegato è uno di quelli
	 * selezionati in fase di protocollazione.
	 */
	private boolean daProtocollaMail;

	/**
	 * indica se l'allegato deve essere mantenuto in formato originale (se quindi
	 * l'utente non può richiederne la stampigliatura).
	 */
	private boolean mantieniFormatoOriginale;

	/**
	 * Flag creazione da allacci.
	 */
	private boolean createFromAllacci;

	/**
	 * Identificativo fascicolo proveninenza.
	 */
	private String idFascicoloProvenienza;

	/**
	 * Descrizione fascicolo fascicolo provenienza.
	 */
	private String descrizioneFascicoloProvenienza;

	/**
	 * Document titlte documento provenienza.
	 */
	private String documentTitleDocumentoProvenienza;

	/**
	 * Descrizione documento provenienza.
	 */
	private String descrizioneDocumentoProvenienza;

	/**
	 * Docuemtn title allegato provenienza.
	 */
	private String documentTitleAllegatoProvenienza;

	/**
	 * Nome file allegato provenienza.
	 */
	private String nomeFileAllegatoProvenienza;
	/* allegati da allacci */

	/**
	 * Tra le versioni del documento corrisponde alla corrente.
	 * 
	 * Viene aggiornata al primo get, quindi in debug non riusciresti a vedere
	 * niente.
	 */
	private VersionDTO currentVersion;

	/**
	 * Per indicare se abilitare o meno il tasto genera anteprima della dialog
	 * visualizza anteprima.
	 */
	private Boolean generaAnteprimaDisabled;

	/**
	 * Per indicare se abilitare o meno se il tasto genera anteprima mostrerà il
	 * documento col campo firma.
	 */
	private Boolean campoFirma;

	/**
	 * Flag cernsito cartaceo.
	 */
	private boolean daCensisciCartaceo;

	/**
	 * Lista di tipologie Documento disponibili.
	 */
	private Collection<TipologiaDocumentoDTO> comboTipologieDocumentoAllegati;

	/**
	 * Flag file non sbustato.
	 */
	private boolean fileNonSbustato;

	/**
	 * Flag da predisponi.
	 */
	private boolean daPredisponi;

	/**
	 * MASCHERA INSERIMENTO / MODIFICA END
	 **************************************************/

	/**
	 * Flag stampigliatura sigla.
	 */
	private boolean stampigliaturaSigla;

	/**
	 * Flag check stampigliatura sigla visibile.
	 */
	private boolean checkStampigliaturaSiglaVisible;

	/**
	 * String placeholder sigla.
	 */
	private String placeholderSigla;

	/**
	 * Flag timbro uscita aoo.
	 */
	private boolean timbroUscitaAoo;

	/**
	 * Flag timbro uscita aoo disabilitato.
	 */
	private boolean checkTimbroUscitaAooDisabled;

	/**
	 * Dimensione content.
	 */
	private Float contentSize;
	
	/**
	 * Document title da allacci.
	 */
	private String documentTitleDaAllacci;

	/**
	 * Esito verifica firma.
	 */
	private ValueMetadatoVerificaFirmaEnum valueMetaadatoVerificaFirmaEnum;

	/**
	 * Copy constructor.
	 * 
	 * @param al
	 */
	public AllegatoDTO(final AllegatoDTO al) {
		super(al.getGuid(), al.getNomeFile(), al.getOggetto(), null);
		this.documentTitle = al.getDocumentTitle();
		this.formato = al.getFormato();
		this.formatoSelected = FormatoAllegatoEnum.getById(al.getFormato());
		this.idTipologiaDocumento = al.getIdTipologiaDocumento();
		this.tipologia = al.getTipologia();
		this.barcode = al.getBarcode();
		this.formatoOriginale = al.getFormatoOriginale();
		this.checkMantieniFormatoOriginaleDisable = al.isCheckMantieniFormatoOriginaleDisable();
		this.daFirmare = al.getDaFirmare();
		this.copiaConforme = al.getCopiaConforme();
		this.mimeType = al.getMimeType();
		this.content = al.getContent();
		this.idUfficioCopiaConforme = al.getIdUfficioCopiaConforme();
		this.idUtenteCopiaConforme = al.getIdUtenteCopiaConforme();
		this.posizione = al.getPosizione();
		this.versions = al.getVersions();
		this.daProtocollaMail = al.isDaProtocollaMail();
		this.daFirmareBoolean = al.isDaFirmareBoolean();
		this.daCensisciCartaceo = al.isDaCensisciCartaceo();
		this.stampigliaturaSigla = al.getStampigliaturaSigla();
		this.setInfoAllacci(al.getIdFascicoloProvenienza(), al.getDescrizioneFascicoloProvenienza(), al.getDocumentTitleDocumentoProvenienza(),
				al.getDescrizioneDocumentoProvenienza(), al.getDocumentTitleAllegatoProvenienza(), al.getNomeFileAllegatoProvenienza());
		this.comboTipologieDocumentoAllegati = al.getComboTipologieDocumentoAllegati();
		this.daPredisponi = al.isDaPredisponi();
		this.timbroUscitaAoo = al.isTimbroUscitaAoo();
		this.firmaVisibile = al.getFirmaVisibile();
	}

	/**
	 * Costruttore di classe.
	 * 
	 * @param documentTitle
	 * @param formato
	 * @param idTipologiaDocumento
	 * @param tipologia
	 * @param oggetto
	 * @param barcode
	 * @param formatoOriginale
	 * @param checkFormatoOriginaleDisable
	 * @param daFirmare
	 * @param copiaConforme
	 * @param nomeFile
	 * @param mimeType
	 * @param guid
	 * @param content
	 * @param idUfficioCopiaConforme
	 * @param idUtenteCopiaConforme
	 * @param posizione
	 * @param versions
	 * @param daProtocollaMail
	 * @param comboTipologieDocumentoAllegati
	 * @param inFileNonSbustato
	 * @param inDaPredisponi
	 */
	public AllegatoDTO(final String documentTitle, final Integer formato, final Integer idTipologiaDocumento, final String tipologia, final String oggetto,
			final String barcode, final Boolean formatoOriginale, final Boolean checkFormatoOriginaleDisable, final Integer daFirmare, final Boolean copiaConforme,
			final String nomeFile, final String mimeType, final String guid, final byte[] content, final Long idUfficioCopiaConforme, final Long idUtenteCopiaConforme,
			final String posizione, final List<VersionDTO> versions, final boolean daProtocollaMail, final Collection<TipologiaDocumentoDTO> comboTipologieDocumentoAllegati,
			final boolean inFileNonSbustato, final boolean inDaPredisponi) {
		super(guid, nomeFile, oggetto, null);
		this.documentTitle = documentTitle;
		this.formato = formato;
		this.formatoSelected = FormatoAllegatoEnum.getById(formato);
		this.idTipologiaDocumento = idTipologiaDocumento;
		this.tipologia = tipologia;
		this.barcode = barcode;
		this.formatoOriginale = formatoOriginale;
		this.checkMantieniFormatoOriginaleDisable = checkFormatoOriginaleDisable;
		this.daFirmare = daFirmare;
		this.copiaConforme = copiaConforme;
		this.mimeType = mimeType;
		this.content = content;
		this.idUfficioCopiaConforme = idUfficioCopiaConforme;
		this.idUtenteCopiaConforme = idUtenteCopiaConforme;
		if (this.idUfficioCopiaConforme != null && this.idUtenteCopiaConforme != null) {
			this.idCopiaConforme = idUfficioCopiaConforme + " " + idUtenteCopiaConforme;
		}
		this.posizione = posizione;
		this.versions = versions;
		this.daProtocollaMail = daProtocollaMail;
		this.comboTipologieDocumentoAllegati = comboTipologieDocumentoAllegati;
		this.fileNonSbustato = inFileNonSbustato;
		this.daPredisponi = inDaPredisponi;
	}

	/**
	 * Costruttore di classe.
	 * 
	 * @param documentTitle
	 * @param formato
	 * @param idTipologiaDocumento
	 * @param tipologia
	 * @param oggetto
	 * @param barcode
	 * @param formatoOriginale
	 * @param daFirmare
	 * @param copiaConforme
	 * @param guid
	 * @param nomeFile
	 * @param mimeType
	 * @param dateCreated
	 * @param idUfficioCopiaConforme
	 * @param idUtenteCopiaConforme
	 * @param idFascicoloProvenienza
	 * @param descrizioneFascicoloProvenienza
	 * @param documentTitleDocumentoProvenienza
	 * @param descrizioneDocumentoProvenienza
	 * @param documentTitleAllegatoProvenienza
	 * @param nomeFileAllegatoProvenienza
	 * @param inTipiDocDisponibili
	 * @param inFileNonSbustato
	 * @param stampigliaturaSigla
	 * @param placeholderSigla
	 * @param timbroUscita
	 * @param contentSize
	 * @param firmaVisibile
	 */
	public AllegatoDTO(final String documentTitle, final Integer formato, final Integer idTipologiaDocumento, final String tipologia, final String oggetto,
			final String barcode, final Boolean formatoOriginale, final Integer daFirmare, final Boolean copiaConforme, final String guid, final String nomeFile,
			final String mimeType, final Date dateCreated, final Long idUfficioCopiaConforme, final Long idUtenteCopiaConforme, final String idFascicoloProvenienza,
			final String descrizioneFascicoloProvenienza, final String documentTitleDocumentoProvenienza, final String descrizioneDocumentoProvenienza,
			final String documentTitleAllegatoProvenienza, final String nomeFileAllegatoProvenienza, final Collection<TipologiaDocumentoDTO> inTipiDocDisponibili,
			final boolean inFileNonSbustato, final boolean stampigliaturaSigla, final String placeholderSigla, final boolean timbroUscita, final Float contentSize,
			final Boolean firmaVisibile, final ValueMetadatoVerificaFirmaEnum valueMetadatoVerificaFirmaEnum) {
		super(guid, nomeFile, oggetto, dateCreated);
		this.documentTitle = documentTitle;
		this.formato = formato;
		this.formatoSelected = FormatoAllegatoEnum.getById(formato);
		this.idTipologiaDocumento = idTipologiaDocumento;
		this.tipologia = tipologia;
		this.barcode = barcode;
		this.formatoOriginale = formatoOriginale;
		this.daFirmare = daFirmare;
		this.copiaConforme = copiaConforme;
		this.mimeType = mimeType;
		this.idUfficioCopiaConforme = idUfficioCopiaConforme;
		this.idUtenteCopiaConforme = idUtenteCopiaConforme;
		if (this.idUfficioCopiaConforme != null && this.idUtenteCopiaConforme != null) {
			this.idCopiaConforme = idUfficioCopiaConforme + " " + idUtenteCopiaConforme;
		}

		this.comboTipologieDocumentoAllegati = inTipiDocDisponibili;
		this.stampigliaturaSigla = stampigliaturaSigla;

		setInfoAllacci(idFascicoloProvenienza, descrizioneFascicoloProvenienza, documentTitleDocumentoProvenienza, descrizioneDocumentoProvenienza,
				documentTitleAllegatoProvenienza, nomeFileAllegatoProvenienza);
		this.fileNonSbustato = inFileNonSbustato;
		this.placeholderSigla = placeholderSigla;

		this.timbroUscitaAoo = timbroUscita;

		if (contentSize == null) {
			this.contentSize = 0.0F;
		} else {
			final String sizeMBRound = String.format("%.2f", contentSize).replace(",", ".");
			this.contentSize = Float.parseFloat(sizeMBRound);
		}

		this.firmaVisibile = firmaVisibile;
		this.valueMetaadatoVerificaFirmaEnum = valueMetadatoVerificaFirmaEnum;
	}

	/**
	 * Costruttore per il censimento di un documento come allegato cartaceo.
	 * 
	 * @param barcode
	 * @param content
	 * @param mimeType
	 */
	public AllegatoDTO(final String barcode, final byte[] content, final String mimeType, final List<TipologiaDocumentoDTO> tipologieDocumento) {
		this.barcode = barcode;
		this.content = content;
		this.mimeType = mimeType;
		this.formato = FormatoAllegatoEnum.ELETTRONICO.getId();
		this.comboTipologieDocumentoAllegati = tipologieDocumento;
		this.idTipologiaDocumento = tipologieDocumento.get(0).getIdTipologiaDocumento();
		this.newAllegato = true;
		this.daCensisciCartaceo = true;
	}

	/**
	 * Costruttore per allegato da web service.
	 * 
	 * @param nomeFile
	 * @param content
	 * @param mimeType
	 * @param oggetto
	 */
	public AllegatoDTO(final String nomeFile, final byte[] content, final String mimeType, final String oggetto, final Boolean daFirmare,
			final Boolean mantieniFormatoOriginale, final Integer idTipologiaDocumento) {
		super(null, nomeFile, oggetto, null);
		this.content = content;
		this.mimeType = mimeType;
		this.formato = FormatoAllegatoEnum.ELETTRONICO.getId();
		this.daFirmareBoolean = Boolean.TRUE.equals(daFirmare);
		this.daFirmare = this.daFirmareBoolean ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue();
		this.mantieniFormatoOriginale = Boolean.TRUE.equals(mantieniFormatoOriginale);
		this.idTipologiaDocumento = idTipologiaDocumento;
	}

	/**
	 * @param nomeFile
	 * @param content
	 * @param mimeType
	 * @param formato
	 * @param oggetto
	 * @param daFirmare
	 * @param mantieniFormatoOriginale
	 * @param idTipologiaDocumento
	 */
	public AllegatoDTO(final String nomeFile, final byte[] content, final String mimeType, final FormatoAllegatoEnum formato, final String oggetto, final Boolean daFirmare,
			final Boolean mantieniFormatoOriginale, final Integer idTipologiaDocumento) {
		super(null, nomeFile, oggetto, null);
		this.content = content;
		this.mimeType = mimeType;
		this.formato = formato.getId();
		this.daFirmareBoolean = Boolean.TRUE.equals(daFirmare);
		this.daFirmare = this.daFirmareBoolean ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue();
		this.mantieniFormatoOriginale = Boolean.TRUE.equals(mantieniFormatoOriginale);
		this.idTipologiaDocumento = idTipologiaDocumento;
	}

	/**
	 * Costruttore di default.
	 */
	public AllegatoDTO() {
		super();
	}
	
	private void setInfoAllacci(final String idFascicoloProvenienza, final String descrizioneFascicoloProvenienza, final String documentTitleDocumentoProvenienza,
			final String descrizioneDocumentoProvenienza, final String documentTitleAllegatoProvenienza, final String nomeFileAllegatoProvenienza) {
		if (!StringUtils.isNullOrEmpty(idFascicoloProvenienza)) {
			createFromAllacci = true;
			setIdFascicoloProvenienza(idFascicoloProvenienza);
			setDescrizioneFascicoloProvenienza(descrizioneFascicoloProvenienza);
			setDocumentTitleDocumentoProvenienza(documentTitleDocumentoProvenienza);
			setDescrizioneDocumentoProvenienza(descrizioneDocumentoProvenienza);
			setDocumentTitleAllegatoProvenienza(documentTitleAllegatoProvenienza);
			setNomeFileAllegatoProvenienza(nomeFileAllegatoProvenienza);
		}
	}

	/**
	 * @param idFascicoloProvenienza
	 * @param documentTitleDocumentoProvenienza
	 * @param documentTitleAllegatoProvenienza
	 * @return true se l'allegato è uguale all'allegato definito dai parametri,
	 *         false altrimenti
	 */
	public boolean equalsFromAllacci(final String idFascicoloProvenienza, final String documentTitleDocumentoProvenienza, final String documentTitleAllegatoProvenienza) {
		// se sono allegati da allacci (controllo che dovrebbe essere sempre
		// soddisfatto)
		if (!StringUtils.isNullOrEmpty(getIdFascicoloProvenienza()) && !StringUtils.isNullOrEmpty(idFascicoloProvenienza)
				&& getIdFascicoloProvenienza().equals(idFascicoloProvenienza) && getDocumentTitleDocumentoProvenienza().equals(documentTitleDocumentoProvenienza)) {

			// se fascicolo e documento principale uguali
			if (StringUtils.isNullOrEmpty(getDocumentTitleAllegatoProvenienza()) && StringUtils.isNullOrEmpty(documentTitleAllegatoProvenienza)) {
				// se non sono allegati, ho trovato la corrispondenza fascicolo/documento
				return true;
			} else {
				// se è lo stesso allegato, ho trovato la corrispondenza
				// fascicolo/documento/allegato
				if (!StringUtils.isNullOrEmpty(getDocumentTitleAllegatoProvenienza()) && getDocumentTitleAllegatoProvenienza().equals(documentTitleAllegatoProvenienza)) {
					return true;
				}
			}

		}

		return false;
	}

	/**
	 * La VersioneDTO per il documento corrente, il più delle volte si tratta di
	 * quella valida più recente.
	 * 
	 * In caso il campo sia nullo non sono state recuperate le versioni del
	 * documento.
	 * 
	 */
	public VersionDTO getCurrentVersion() {
		if (currentVersion == null) {
			final Optional<VersionDTO> currentVersionOptional = getVersions().stream().filter(VersionDTO::isCurrentVersion).findAny();
			if (currentVersionOptional.isPresent()) {
				currentVersion = currentVersionOptional.get();
			}
		}

		return currentVersion;
	}

	/**
	 * @return formato
	 */
	public Integer getFormato() {
		return formato;
	}

	/**
	 * @param inFormato
	 */
	public void setFormato(final Integer inFormato) {
		this.formato = inFormato;
	}

	/**
	 * @return tipologia
	 */
	public String getTipologia() {
		return tipologia;
	}

	/**
	 * @return barcode
	 */
	public String getBarcode() {
		return barcode;
	}

	/**
	 * @return formatoOriginale
	 */
	public Boolean getFormatoOriginale() {
		return formatoOriginale;
	}

	/**
	 * @return 0 se daFirmare è null, restituisce daFirmare se non è null
	 */
	public Integer getDaFirmare() {
		Integer output = 0;
		if (daFirmare != null) {
			output = daFirmare;
		}
		return output;
	}

	/**
	 * @return copiaConforme
	 */
	public Boolean getCopiaConforme() {
		return copiaConforme;
	}

	/**
	 * Restituisce le versioni dell'allegato.
	 * 
	 * @return List di VersionDTO
	 */
	public List<VersionDTO> getVersions() {
		return versions;
	}

	/**
	 * @return documentTitle
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * @param inDocumentTitle
	 */
	public void setDocumentTitle(final String inDocumentTitle) {
		this.documentTitle = inDocumentTitle;
	}

	/**
	 * @return mimeType
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * @return IconaExtensionEnum associata al nome file
	 */
	public IconaExtensionEnum getIconaExt() {
		final String nomeFile = getNomeFile();
		return IconaExtensionEnum.get(nomeFile);
	}

	/**
	 * @return idUfficioCopiaConforme
	 */
	public Long getIdUfficioCopiaConforme() {
		return idUfficioCopiaConforme;
	}

	/**
	 * @return idUtenteCopiaConforme
	 */
	public Long getIdUtenteCopiaConforme() {
		return idUtenteCopiaConforme;
	}

	/**
	 * @return posizione
	 */
	public String getPosizione() {
		return posizione;
	}

	/**
	 * Restituisce il content dell'allegato.
	 * 
	 * @return content
	 */
	public byte[] getContent() {
		return content;
	}

	/**
	 * @param inMimeType
	 */
	public void setMimeType(final String inMimeType) {
		this.mimeType = inMimeType;
	}

	/**
	 * @param inContent
	 */
	public void setContent(final byte[] inContent) {
		this.content = inContent;
	}

	/**
	 * @return idDocumento
	 */
	public String getIdDocumento() {
		return idDocumento;
	}

	/**
	 * @param inIdDocumento
	 */
	public void setIdDocumento(final String inIdDocumento) {
		this.idDocumento = inIdDocumento;
	}

	/**
	 * @return posizioneVisible
	 */
	public boolean isPosizioneVisible() {
		return posizioneVisible;
	}

	/**
	 * @param inPosizioneVisible
	 */
	public void setPosizioneVisible(final boolean inPosizioneVisible) {
		this.posizioneVisible = inPosizioneVisible;
	}

	/**
	 * @return checkMantieniFormatoOriginaleVisible
	 */
	public boolean isCheckMantieniFormatoOriginaleVisible() {
		return checkMantieniFormatoOriginaleVisible;
	}

	/**
	 * Imposta l'attributo checkMantieniFormatoOriginaleVisible.
	 * 
	 * @param inCheckMantieniFormatoOriginaleVisible
	 */
	public void setCheckMantieniFormatoOriginaleVisible(final boolean inCheckMantieniFormatoOriginaleVisible) {
		this.checkMantieniFormatoOriginaleVisible = inCheckMantieniFormatoOriginaleVisible;
	}

	/**
	 * @return barcodeVisible
	 */
	public boolean isBarcodeVisible() {
		return barcodeVisible;
	}

	/**
	 * @param inBarcodeVisible
	 */
	public void setBarcodeVisible(final boolean inBarcodeVisible) {
		this.barcodeVisible = inBarcodeVisible;
	}

	/**
	 * @return uploadFileVisible
	 */
	public boolean isUploadFileVisible() {
		return uploadFileVisible;
	}

	/**
	 * @param inUploadFileVisible
	 */
	public void setUploadFileVisible(final boolean inUploadFileVisible) {
		this.uploadFileVisible = inUploadFileVisible;
	}

	/**
	 * @return idTipologiaDocumento
	 */
	public Integer getIdTipologiaDocumento() {
		return idTipologiaDocumento;
	}

	/**
	 * @return daFirmareBoolean
	 */
	public boolean isDaFirmareBoolean() {
		return daFirmareBoolean;
	}

	/**
	 * @param inDaFirmareBoolean
	 */
	public void setDaFirmareBoolean(final boolean inDaFirmareBoolean) {
		this.daFirmareBoolean = inDaFirmareBoolean;
	}

	/**
	 * @return idCopiaConforme
	 */
	public String getIdCopiaConforme() {
		return idCopiaConforme;
	}

	/**
	 * @param inIdCopiaConforme
	 */
	public void setIdCopiaConforme(final String inIdCopiaConforme) {
		this.idCopiaConforme = inIdCopiaConforme;
	}

	/**
	 * @param inIdTipologiaDocumento
	 */
	public void setIdTipologiaDocumento(final Integer inIdTipologiaDocumento) {
		this.idTipologiaDocumento = inIdTipologiaDocumento;
	}

	/**
	 * @param inTipologia
	 */
	public void setTipologia(final String inTipologia) {
		this.tipologia = inTipologia;
	}

	/**
	 * @param inBarcode
	 */
	public void setBarcode(final String inBarcode) {
		this.barcode = inBarcode;
	}

	/**
	 * @param inFormatoOriginale
	 */
	public void setFormatoOriginale(final Boolean inFormatoOriginale) {
		this.formatoOriginale = inFormatoOriginale;
	}

	/**
	 * @param inDaFirmare
	 */
	public void setDaFirmare(final Integer inDaFirmare) {
		this.daFirmare = inDaFirmare;
	}

	/**
	 * @param inCopiaConforme
	 */
	public void setCopiaConforme(final Boolean inCopiaConforme) {
		this.copiaConforme = inCopiaConforme;
	}

	/**
	 * @param inIdUfficioCopiaConforme
	 */
	public void setIdUfficioCopiaConforme(final Long inIdUfficioCopiaConforme) {
		this.idUfficioCopiaConforme = inIdUfficioCopiaConforme;
	}

	/**
	 * @param inIdUtenteCopiaConforme
	 */
	public void setIdUtenteCopiaConforme(final Long inIdUtenteCopiaConforme) {
		this.idUtenteCopiaConforme = inIdUtenteCopiaConforme;
	}

	/**
	 * @param inPosizione
	 */
	public void setPosizione(final String inPosizione) {
		this.posizione = inPosizione;
	}

	/**
	 * @return formatoSelected
	 */
	public FormatoAllegatoEnum getFormatoSelected() {
		return formatoSelected;
	}

	/**
	 * @param formatoSelected
	 */
	public void setFormatoSelected(final FormatoAllegatoEnum formatoSelected) {
		this.formatoSelected = formatoSelected;
	}

	/**
	 * @return checkDaFirmareVisible
	 */
	public boolean isCheckDaFirmareVisible() {
		return checkDaFirmareVisible;
	}

	/**
	 * @param checkDaFirmareVisible
	 */
	public void setCheckDaFirmareVisible(final boolean checkDaFirmareVisible) {
		this.checkDaFirmareVisible = checkDaFirmareVisible;
	}

	/**
	 * @return checkCopiaConformeVisible
	 */
	public boolean isCheckCopiaConformeVisible() {
		return checkCopiaConformeVisible;
	}

	/**
	 * @param inCheckCopiaConformeVisible
	 */
	public void setCheckCopiaConformeVisible(final boolean inCheckCopiaConformeVisible) {
		this.checkCopiaConformeVisible = inCheckCopiaConformeVisible;
	}

	/**
	 * @param inVersions
	 */
	public void setVersions(final List<VersionDTO> inVersions) {
		this.versions = inVersions;
		this.currentVersion = null;
	}

	/**
	 * @return true se l'utente in fase di modifica, false altrimenti
	 */
	public boolean isInModifica() {
		return inModifica;
	}

	/**
	 * @param inModifica
	 */
	public void setInModifica(final boolean inModifica) {
		this.inModifica = inModifica;
	}

	/**
	 * @return newAllegato
	 */
	public boolean isNewAllegato() {
		return newAllegato;
	}

	/**
	 * @param inNewAllegato
	 */
	public void setNewAllegato(final boolean inNewAllegato) {
		this.newAllegato = inNewAllegato;
	}

	/**
	 * @return nodeIdFromHierarchicalFile
	 */
	public String getNodeIdFromHierarchicalFile() {
		return nodeIdFromHierarchicalFile;
	}

	/**
	 * @param inNodeIdFromHierarchicalFile
	 */
	public void setNodeIdFromHierarchicalFile(final String inNodeIdFromHierarchicalFile) {
		this.nodeIdFromHierarchicalFile = inNodeIdFromHierarchicalFile;
	}

	/**
	 * Restituisce true se la provenienza è da protocollaMail.
	 * 
	 * @return daProtocollaMail
	 */
	public boolean isDaProtocollaMail() {
		return daProtocollaMail;
	}

	/**
	 * @return mantieniFormatoOriginale
	 */
	public boolean isMantieniFormatoOriginale() {
		return mantieniFormatoOriginale;
	}

	/**
	 * @param mantieniFormatoOriginale
	 */
	public void setMantieniFormatoOriginale(final boolean mantieniFormatoOriginale) {
		this.mantieniFormatoOriginale = mantieniFormatoOriginale;
	}

	/**
	 * @return createFromAllacci
	 */
	public boolean isCreateFromAllacci() {
		return createFromAllacci;
	}

	/**
	 * @param createFromAllacci
	 */
	public void setCreateFromAllacci(final boolean createFromAllacci) {
		this.createFromAllacci = createFromAllacci;
	}

	/**
	 * @return idFascicoloProvenienza
	 */
	public String getIdFascicoloProvenienza() {
		return idFascicoloProvenienza;
	}

	/**
	 * @param idFascicoloProvenienza
	 */
	public void setIdFascicoloProvenienza(final String idFascicoloProvenienza) {
		this.idFascicoloProvenienza = idFascicoloProvenienza;
	}

	/**
	 * @return descrizioneFascicoloProvenienza
	 */
	public String getDescrizioneFascicoloProvenienza() {
		return descrizioneFascicoloProvenienza;
	}

	/**
	 * @param descrizioneFascicoloProvenienza
	 */
	public void setDescrizioneFascicoloProvenienza(final String descrizioneFascicoloProvenienza) {
		this.descrizioneFascicoloProvenienza = descrizioneFascicoloProvenienza;
	}

	/**
	 * @return documentTitleDocumentoProvenienza
	 */
	public String getDocumentTitleDocumentoProvenienza() {
		return documentTitleDocumentoProvenienza;
	}

	/**
	 * @param documentTitleDocumentoProvenienza
	 */
	public void setDocumentTitleDocumentoProvenienza(final String documentTitleDocumentoProvenienza) {
		this.documentTitleDocumentoProvenienza = documentTitleDocumentoProvenienza;
	}

	/**
	 * @return descrizioneDocumentoProvenienza
	 */
	public String getDescrizioneDocumentoProvenienza() {
		return descrizioneDocumentoProvenienza;
	}

	/**
	 * @param descrizioneDocumentoProvenienza
	 */
	public void setDescrizioneDocumentoProvenienza(final String descrizioneDocumentoProvenienza) {
		this.descrizioneDocumentoProvenienza = descrizioneDocumentoProvenienza;
	}

	/**
	 * @return documentTitleAllegatoProvenienza
	 */
	public String getDocumentTitleAllegatoProvenienza() {
		return documentTitleAllegatoProvenienza;
	}

	/**
	 * @param documentTitleAllegatoProvenienza
	 */
	public void setDocumentTitleAllegatoProvenienza(final String documentTitleAllegatoProvenienza) {
		this.documentTitleAllegatoProvenienza = documentTitleAllegatoProvenienza;
	}

	/**
	 * @return nomeFileAllegatoProvenienza
	 */
	public String getNomeFileAllegatoProvenienza() {
		return nomeFileAllegatoProvenienza;
	}

	/**
	 * @param nomeFileAllegatoProvenienza
	 */
	public void setNomeFileAllegatoProvenienza(final String nomeFileAllegatoProvenienza) {
		this.nomeFileAllegatoProvenienza = nomeFileAllegatoProvenienza;
	}

	/**
	 * @return checkCopiaConformeDisabled
	 */
	public boolean isCheckCopiaConformeDisabled() {
		return checkCopiaConformeDisabled;
	}

	/**
	 * @param checkCopiaConformeDisabled
	 */
	public void setCheckCopiaConformeDisabled(final boolean checkCopiaConformeDisabled) {
		this.checkCopiaConformeDisabled = checkCopiaConformeDisabled;
	}

	/**
	 * @return checkMantieniFormatoOriginaleDisable
	 */
	public boolean isCheckMantieniFormatoOriginaleDisable() {
		return checkMantieniFormatoOriginaleDisable;
	}

	/**
	 * @param checkMantieniFormatoOriginaleDisable
	 */
	public void setCheckMantieniFormatoOriginaleDisable(final boolean checkMantieniFormatoOriginaleDisable) {
		this.checkMantieniFormatoOriginaleDisable = checkMantieniFormatoOriginaleDisable;
	}

	/**
	 * @return generaAnteprimaDisabled
	 */
	public Boolean getGeneraAnteprimaDisabled() {
		return generaAnteprimaDisabled;
	}

	/**
	 * @param generaAnteprimaDisabled
	 */
	public void setGeneraAnteprimaDisabled(final Boolean generaAnteprimaDisabled) {
		this.generaAnteprimaDisabled = generaAnteprimaDisabled;
	}

	/**
	 * @return campoFirma
	 */
	public Boolean getCampoFirma() {
		return campoFirma;
	}

	/**
	 * @param campoFirma
	 */
	public void setCampoFirma(final Boolean campoFirma) {
		this.campoFirma = campoFirma;
	}

	/**
	 * @return daCensisciCartaceo
	 */
	public boolean isDaCensisciCartaceo() {
		return daCensisciCartaceo;
	}

	/**
	 * @return comboTipologieDocumentoAllegati
	 */
	public Collection<TipologiaDocumentoDTO> getComboTipologieDocumentoAllegati() {
		return comboTipologieDocumentoAllegati;
	}

	/**
	 * @param comboTipologieDocumentoAllegati
	 */
	public void setComboTipologieDocumentoAllegati(final Collection<TipologiaDocumentoDTO> comboTipologieDocumentoAllegati) {
		this.comboTipologieDocumentoAllegati = comboTipologieDocumentoAllegati;
	}

	/**
	 * @return fileNonSbustato
	 */
	public boolean getFileNonSbustato() {
		return fileNonSbustato;
	}

	/**
	 * @param fileNonSbustato
	 */
	public void setFileNonSbustato(final boolean fileNonSbustato) {
		this.fileNonSbustato = fileNonSbustato;
	}

	/**
	 * @return stampigliaturaSigla
	 */
	public boolean getStampigliaturaSigla() {
		return stampigliaturaSigla;
	}

	/**
	 * @param stampigliaturaSigla
	 */
	public void setStampigliaturaSigla(final boolean stampigliaturaSigla) {
		this.stampigliaturaSigla = stampigliaturaSigla;
	}

	/**
	 * Restituisce true se la provenienza è da Predisponi
	 *
	 * @return daPredisponi
	 */
	public boolean isDaPredisponi() {
		return daPredisponi;
	}

	/**
	 * @param daPredisponi
	 */
	public void setDaPredisponi(final boolean daPredisponi) {
		this.daPredisponi = daPredisponi;
	}

	/**
	 * @return checkStampigliaturaSiglaVisibile
	 */
	public boolean getCheckStampigliaturaSiglaVisible() {
		return checkStampigliaturaSiglaVisible;
	}

	/**
	 * @param checkStampigliaturaSiglaVisible
	 */
	public void setCheckStampigliaturaSiglaVisible(final boolean checkStampigliaturaSiglaVisible) {
		this.checkStampigliaturaSiglaVisible = checkStampigliaturaSiglaVisible;
	}

	/**
	 * @return placeholderSigla
	 */
	public String getPlaceholderSigla() {
		return placeholderSigla;
	}

	/**
	 * @param placeholderSigla
	 */
	public void setPlaceholderSigla(final String placeholderSigla) {
		this.placeholderSigla = placeholderSigla;
	}

	/**
	 * @return the timbroUscitaAoo
	 */
	public boolean isTimbroUscitaAoo() {
		return timbroUscitaAoo;
	}

	/**
	 * @param timbroUscitaAoo the timbroUscitaAoo to set
	 */
	public void setTimbroUscitaAoo(final boolean timbroUscitaAoo) {
		this.timbroUscitaAoo = timbroUscitaAoo;
	}

	/**
	 * @return the checkTimbroUscitaAooDisabled
	 */
	public boolean isCheckTimbroUscitaAooDisabled() {
		return checkTimbroUscitaAooDisabled;
	}

	/**
	 * @param checkTimbroUscitaAooDisabled the checkTimbroUscitaAooDisabled to set
	 */
	public void setCheckTimbroUscitaAooDisabled(final boolean checkTimbroUscitaAooDisabled) {
		this.checkTimbroUscitaAooDisabled = checkTimbroUscitaAooDisabled;
	}

	/**
	 * @return contentSize
	 */
	public Float getContentSize() {
		return contentSize;
	}

	/**
	 * @param contentSize
	 */
	public void setContentSize(final Float contentSize) {
		this.contentSize = contentSize;
	}

	/**
	 * @return result
	 */
	public Integer getContentSizeInKiloByte() {
		return null;
	}

	/**
	 * @return firmaVisibile
	 */
	public Boolean getFirmaVisibile() {
		return firmaVisibile;
	}

	/**
	 * @param firmaVisibile
	 */
	public void setFirmaVisibile(final Boolean firmaVisibile) {
		this.firmaVisibile = firmaVisibile;
	}

	/**
	 * @return checkFirmaVisibileVisible
	 */
	public boolean isCheckFirmaVisibileVisible() {
		return checkFirmaVisibileVisible;
	}

	/**
	 * @param checkFirmaVisibileVisible
	 */
	public void setCheckFirmaVisibileVisible(final boolean checkFirmaVisibileVisible) {
		this.checkFirmaVisibileVisible = checkFirmaVisibileVisible;
	}

	/**
	 * @return the documentTitleDaAllacci
	 */
	public String getDocumentTitleDaAllacci() {
		return documentTitleDaAllacci;
	}

	/**
	 * @param documentTitleDaAllacci the documentTitleDaAllacci to set
	 */
	public void setDocumentTitleDaAllacci(final String documentTitleDaAllacci) {
		this.documentTitleDaAllacci = documentTitleDaAllacci;
	}

	/**
	 * @return valueMetaadatoVerificaFirmaEnum
	 */
	public ValueMetadatoVerificaFirmaEnum getValueMetaadatoVerificaFirmaEnum() {
		return valueMetaadatoVerificaFirmaEnum;
	}

	/**
	 * @param valueMetaadatoVerificaFirmaEnum
	 */
	public void setValueMetaadatoVerificaFirmaEnum(final ValueMetadatoVerificaFirmaEnum valueMetaadatoVerificaFirmaEnum) {
		this.valueMetaadatoVerificaFirmaEnum = valueMetaadatoVerificaFirmaEnum;
	}

	/**
	 * Restituisce true se il formato selezionato è firmato digitalmente.
	 * @return true se firmato, false altrimenti
	 */
	public boolean getFirmato() {
		return formatoSelected!=null && formatoSelected.equals(FormatoAllegatoEnum.FIRMATO_DIGITALMENTE);
	}
}