package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.AssegnazioneMetadatiDTO;
import it.ibm.red.business.service.facade.IAssegnazioneAutomaticaMetadatiFacadeSRV;

/**
 * Interfaccia del servizio di gestione delle assegnazioni automatiche per
 * metadati.
 * 
 * @author SimoneLungarella
 */
public interface IAssegnazioneAutomaticaMetadatiSRV extends IAssegnazioneAutomaticaMetadatiFacadeSRV {

	/**
	 * Metodo che consente il recupero dei DTO associati ad una specifica coppia
	 * documento - procedimento Un assegnazione automatica di questo tipo è definita
	 * per uno specifico metadato esistente in uno specifico procedimento e
	 * documento. Inoltre l'assegnazione ha un cono di visibilità a livello di AOO.
	 * 
	 * @param idAoo
	 *            identificativo dell'area organizzativa
	 * @param idTipoDocumento
	 *            identificativo della tipologia del documento
	 * @param idTipoProcedimento
	 *            identificativo della tipologia del procedimento
	 * 
	 */
	List<AssegnazioneMetadatiDTO> get(Long idAoo, Long idTipoDocumento, Long idTipoProcedimento, Connection connection);

	/**
	 * Ottiene le assegnazioni automatiche per il tipo documento.
	 * 
	 * @param idAoo
	 *            - identificativo dell'Aoo
	 * @param idTipoDocumento
	 *            - identificativo della tipologia documento
	 * @param connection
	 *            connessione al database
	 * @return lista delle assegnazioni recuperate
	 */
	List<AssegnazioneMetadatiDTO> getByTipoDocumento(Long idAoo, Long idTipoDocumento, Connection connection);
}
