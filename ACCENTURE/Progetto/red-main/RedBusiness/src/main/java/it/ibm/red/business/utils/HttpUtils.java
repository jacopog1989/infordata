package it.ibm.red.business.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;

import it.ibm.red.business.logger.REDLogger;

/**
 * Classe di utility http.
 */
public final class HttpUtils {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(HttpUtils.class.getName());

	/**
	 * Costruttore vuoto.
	 */
	private HttpUtils() {
		// Costruttore vuoto.
	}

	/**
	 * Ottiene il nodo.
	 * 
	 * @return nodo o null in caso di errore
	 */
	public static String getNodo() {
		String nodo = null;
		InetAddress localhost = null;
		try {
			localhost = InetAddress.getLocalHost();
			nodo = localhost.getHostName();
		} catch (final UnknownHostException e) {
			LOGGER.error("Errore nel recupero del nodo :" + e);
		}
		return nodo;
	}
}
