/**
 * 
 */
package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import it.ibm.red.business.dao.IIterApprovativoDAO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.IiterApprovativoSRV;
import it.ibm.red.business.utils.DestinatariRedUtils;

/**
 * @author APerquoti
 *
 */
@Service
public class IterApprovativoSRV extends AbstractService implements IiterApprovativoSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 7824237207445011961L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(IterApprovativoSRV.class.getName());
	
	/**
	 * Dao per la gestione degli iter approvativi.
	 */
	@Autowired
	private IIterApprovativoDAO iterApprovativoDAO;

	/**
	 * @see it.ibm.red.business.service.facade.IiterApprovativoFacadeSRV#getAllByIdAoo(java.lang.Integer).
	 */
	@Override
	public List<IterApprovativoDTO> getAllByIdAoo(final Integer idAoo) {
		List<IterApprovativoDTO> resultQuery = new ArrayList<>();
		Connection connection = null;
		
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			
			resultQuery =  iterApprovativoDAO.getAllByIdAOO(idAoo, connection);
		} catch (final SQLException e) {
			LOGGER.error("Errore nella connessione verso il database durante il recupero degli iter approvativi.", e);
		} finally {
			closeConnection(connection);
		}
		
		return resultQuery;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IiterApprovativoFacadeSRV#getAllByDocument(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public List<IterApprovativoDTO> getAllByDocument(final String documentTitle, final UtenteDTO utente) {
		final List<IterApprovativoDTO> output = new ArrayList<>();
		List<IterApprovativoDTO> resultQuery = new ArrayList<>();
		Connection connection = null;
		IFilenetCEHelper fceh = null;
		boolean excludeRagioniere = false;
		
		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final Document d = fceh.getDocumentForModificaIter(Integer.valueOf(documentTitle));
			MezzoSpedizioneEnum mezzoSelected = null;
			//### DESTINATARI #########################################################################################
			final Collection<?> inDestinatari = (Collection<?>) TrasformerCE.getMetadato(d, PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY);
			// Si normalizza il metadato per evitare errori comuni durante lo split delle singole stringhe dei destinatari
			final List<String[]> destinatariDocumento = DestinatariRedUtils.normalizzaMetadatoDestinatariDocumento(inDestinatari);
			
			if (!CollectionUtils.isEmpty(destinatariDocumento)) {
				Integer idMezzo = null;
				
				for (final String[] destSplit : destinatariDocumento) {
					
					if (destSplit.length >= 5) {
						final TipologiaDestinatarioEnum tde = TipologiaDestinatarioEnum.getByTipologia(destSplit[3]);
						if (TipologiaDestinatarioEnum.ESTERNO == tde) {
							idMezzo = Integer.valueOf(destSplit[2]);
							if (mezzoSelected == null) {
								mezzoSelected = MezzoSpedizioneEnum.getById(idMezzo);
							} else if (mezzoSelected != MezzoSpedizioneEnum.getById(idMezzo)) {
								excludeRagioniere = true;
								break;
							}
						}
					}
					
				}
			}
			//###################################################################################################################
			
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}
		
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			resultQuery =  iterApprovativoDAO.getAllByIdAOO(utente.getIdAoo().intValue(), connection);
			
			for (final IterApprovativoDTO it : resultQuery) {
				if ((excludeRagioniere && it.getIdIterApprovativo().intValue() != IterApprovativoDTO.ITER_FIRMA_RAGIONIERE.intValue()) || (!excludeRagioniere)) {
					output.add(it);
				}
			}
			
		} catch (final SQLException e) {
			LOGGER.error("Errore nella connessione verso il database durante il recupero degli iter approvativi.", e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
		
		return output;
	}
	
}