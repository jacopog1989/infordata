package it.ibm.red.business.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * DTO che definisce la trasmissione.
 */
public class TrasmissioneDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -2943775960335028506L;

	/**
	 * Guid.
	 */
	private String guid;

	/**
	 * Da.
	 */
	private String from;

	/**
	 * Data ricezione.
	 */
	private Date dataRicezione;

	/**
	 * A.
	 */
	private String to;

	/**
	 * Conoscenza.
	 */
	private String cc;

	/**
	 * Oggetto.
	 */
	private String oggetto;

	/**
	 * Testo.
	 */
	private String testo;

	/**
	 * Allegati.
	 */
	private List<AllegatoDTO> allegati;

	/**
	 * Costruttore.
	 */
	public TrasmissioneDTO() {
		allegati = new ArrayList<>();
	}

	/**
	 * Restituisce mittente.
	 * 
	 * @return from
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * Imposta mittente.
	 * 
	 * @param inFrom
	 */
	public void setFrom(final String inFrom) {
		this.from = inFrom;
	}

	/**
	 * Restituisce la data ricezione.
	 * 
	 * @return data ricezione
	 */
	public Date getDataRicezione() {
		return dataRicezione;
	}

	/**
	 * Imposta la data ricezione.
	 * 
	 * @param inDataRicezione
	 */
	public void setDataRicezione(final Date inDataRicezione) {
		this.dataRicezione = inDataRicezione;
	}

	/**
	 * Restituisce destinatario.
	 * 
	 * @return destinatario
	 */
	public String getTo() {
		return to;
	}

	/**
	 * Imposta destinatario.
	 * 
	 * @param inTo
	 */
	public void setTo(final String inTo) {
		this.to = inTo;
	}

	/**
	 * Restituisce destinatario copia conoscenza.
	 * 
	 * @return destinatario copia conoscenza
	 */
	public String getCc() {
		return cc;
	}

	/**
	 * Imposta destinatario copia conoscenza.
	 * 
	 * @param inCc
	 */
	public void setCc(final String inCc) {
		this.cc = inCc;
	}

	/**
	 * Restituisce l'oggetto.
	 * 
	 * @return oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * Imposta l'oggetto.
	 * 
	 * @param inOggetto
	 */
	public void setOggetto(final String inOggetto) {
		this.oggetto = inOggetto;
	}

	/**
	 * Restituisce la lista degli allegati.
	 * 
	 * @return lista allegati
	 */
	public List<AllegatoDTO> getAllegati() {
		return allegati;
	}

	/**
	 * Imposta la lista degli allegati.
	 * 
	 * @param inAllegati
	 */
	public void setAllegati(final List<AllegatoDTO> inAllegati) {
		this.allegati = inAllegati;
	}

	/**
	 * Restituisce il guid.
	 * 
	 * @return guid
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * Imposta il guid.
	 * 
	 * @param inGuid
	 */
	public void setGuid(final String inGuid) {
		this.guid = inGuid;
	}

	/**
	 * @return testo
	 */
	public String getTesto() {
		return testo;
	}

	/**
	 * @param testo
	 */
	public void setTesto(final String inTesto) {
		this.testo = inTesto;
	}
}
