package it.ibm.red.business.dto;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author d.ventimiglia
 */
public class TimerRichiesteIntegrazioniDTO implements Serializable {

	/**
	 *	serial version UID.
	 */
	private static final long serialVersionUID = 8898909743128001910L;

	/**
	 * Id timer.
	 */
	private long idTimer;

	/**
	 * Id documento.
	 */
	private long idDocumento;

	/**
	 * Ruolo.
	 */
	private long idNodo;

	/**
	 * utente.
	 */
	private long idUtente;

	/**
	 * Timestamp.
	 */
	private Timestamp timestamp;

	/**
	 * Flag lavorato.
	 */
	private int flagLavorato;

	/**
	 * Costruttore vuoto.
	 * Non istanziabile senza parametri
	 */
	private TimerRichiesteIntegrazioniDTO() { }

	/**
	 * Costruttore di default.
	 * @param idTimer
	 * @param idDocumento
	 * @param idNodo
	 * @param idUtente
	 * @param timestamp
	 * @param flagLavorato
	 */
	public TimerRichiesteIntegrazioniDTO(final long idTimer, final long idDocumento, final long idNodo, final long idUtente, final Timestamp timestamp, final int flagLavorato) {
		setIdTimer(idTimer);
		setIdDocumento(idDocumento);
		setIdNodo(idNodo);
		setIdUtente(idUtente);
		setTimestamp(timestamp);
		setFlagLavorato(flagLavorato);
	}

	/**
	 * @return idTimer
	 */
	public long getIdTimer() {
		return idTimer;
	}

	/**
	 * Imposta l'id del timer.
	 * @param id
	 */
	public void setIdTimer(final long id) {
		this.idTimer = id;
	}

	/**
	 * @return idDocumento
	 */
	public long getIdDocumento() {
		return idDocumento;
	}

	/**
	 * Imposta l'id del documento.
	 * @param idDocumento
	 */
	public void setIdDocumento(final long idDocumento) {
		this.idDocumento = idDocumento;
	}

	/**
	 * Restituisce l'id dell'ufficio.
	 * @return idNodo
	 */
	public long getIdNodo() {
		return idNodo;
	}

	/**
	 * Imposta l'id dell'ufficio.
	 * @param idNodo
	 */
	public void setIdNodo(final long idNodo) {
		this.idNodo = idNodo;
	}

	/**
	 * @return idUtente
	 */
	public long getIdUtente() {
		return idUtente;
	}

	/**
	 * Imposta l'id dell'utente.
	 * @param idUtente
	 */
	public void setIdUtente(final long idUtente) {
		this.idUtente = idUtente;
	}

	/**
	 * Restituisce il timestamp dell'operazione.
	 * @return timestamp
	 */
	public Timestamp getTimestamp() {
		return timestamp;
	}

	/**
	 * Imposta il timestamp dell'operazione.
	 * @param timestamp
	 */
	public void setTimestamp(final Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * Restituisce il flag {@link #flagLavorato}.
	 * @return flagLavorato
	 */
	public int getFlagLavorato() {
		return flagLavorato;
	}

	/**
	 * Imposta il flag {@link #flagLavorato}.
	 * @param flagLavorato
	 */
	public void setFlagLavorato(final int flagLavorato) {
		this.flagLavorato = flagLavorato;
	}
}
