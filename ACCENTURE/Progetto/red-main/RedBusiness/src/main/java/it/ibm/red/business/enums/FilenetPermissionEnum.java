package it.ibm.red.business.enums;

/**
 * The Enum FilenetPermissionEnum.
 *
 * @author mcrescentini
 * 
 *         Enum delle permission filenet.
 */
public enum FilenetPermissionEnum {
	
	
	/**
	 * Valore.
	 */
	NO_INHERITANCE(0),
	
	/**
	 * Valore.
	 */
	OBJECT_AND_IMMEDIATE_CHILDREN(1),
	
	/**
	 * Valore.
	 */
	OBJECT_AND_ALL_CHILDREN(-1),
	
	/**
	 * Valore.
	 */
	ALL_CHILDREN_BUT_NOT_OBJECT(-2),
	
	/**
	 * Valore.
	 */
	IMMEDIATE_ALL_CHILDREN_ONLY_BUT_NOT_OBJECT(-3);
	
	

	/**
	 * Profondità ricorsiva.
	 */
	private final int inheritableDepth;
	
	
	FilenetPermissionEnum(final int inheritableDepth) {
		this.inheritableDepth = inheritableDepth;
	}

	/**
	 * @return the inheritableDepth
	 */
	public int getInheritableDepth() {
		return inheritableDepth;
	}
}