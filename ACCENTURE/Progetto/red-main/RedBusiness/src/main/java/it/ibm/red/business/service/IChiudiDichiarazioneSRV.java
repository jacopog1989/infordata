package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IChiudiDichiarazioneFacadeSRV;

/**
 * Interface del servizio di chiusura dichiarazione.
 */
public interface IChiudiDichiarazioneSRV extends IChiudiDichiarazioneFacadeSRV {

}
