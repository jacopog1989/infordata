package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import it.ibm.red.business.enums.EsitoValidazioneSegnaturaInteropEnum;
import it.ibm.red.business.enums.StatoMailEnum;

/**
 * The Class MessaggioEmailDTO.
 *
 * @author CPIERASC
 * 
 * 	DTO mail.
 */
public class MessaggioEmailDTO extends EmailDTO implements Serializable {

	/**
	 * Flag che determina un utente come TO - Principale.
	 */
	public static final Integer FLAG_TO = 1;
	
	/**
	 * Flag che determina un utente come CC - Copia conforme.
	 */
	public static final Integer FLAG_CC = 2;
	
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 231956620497883459L;
	
	/**
	 * Document title.
	 */
	private String documentTitle;

	/**
	 * Identificativo tipologia invio.
	 */
	private transient Integer tipologiaInvioId;
	
	/**
	 * Tipo invio.
	 */
	private TipologiaInvio tipologiaInvio;

	/**
	 * Identificativo tipologia messaggio.
	 */
	private transient Integer tipologiaMessaggioId;
	
	/**
	 * Tipo messaggio.
	 */
	private TipologiaMessaggio tipologiaMessaggio;

	/**
	 * Flag notifica.
	 */
	private Boolean isNotifica;

	/**
	 * Action.
	 */
	private String action;

	/**
	 * Destinatari.
	 */
	private transient String destinatari2;

	/**
	 * Destinatario.
	 */
	private String destinatario;

	/**
	 * Ultimo messaggio.
	 */
	private Integer lastMsg;

	/**
	 * Flag TO/CC.
	 */
	private	Integer flagToCc;
	
	/**
	 * tipo ricevuta.
	 */
	private String tipoRicevuta;
	
	/**
	 * Flag utlimo tentativo.
	 */
	private Boolean ultimoTentativo;
	
	/**
	 * Documento.
	 */
	private String idDocumento;
 
	/**
	 * ID Notifica.
	 */
	private	Integer idNotifica;
	
	/**
	 * Tipo destinatario.
	 */
	private	Integer tipoDestinatario;
	
	/**
	 * ID Spedizione Unica.
	 */
	private	Integer idSpedizioneUnica;
	
	/**
	 * Tipo evento.
	 */
	private	Integer tipoEvento;
	
	/**
	 * Allegati Mail.
	 */
	private transient List<AllegatoDTO> allegatiMail;
	
	/**
	 * AllegatiDocTitle .
	 */
	private List<String> allegatiDocTitle;
	
	/**
	 * Nodo mittente.
	 */
	private Long idNodoMittente;
	
	/**
	 * Utente mittente.
	 */
	private Long idUtenteMittente;
	
	/**
	 * Folder.
	 */
	private String folder;
	
	/**
	 * Lista errori.
	 */
	private List<String> erroriValidazioneInterop;
	
	/**
	 * Lista warning.
	 */
	private List<String> warningValidazioneInterop;
	
	/**
	 * Esito validazione segnatura.
	 */
	private EsitoValidazioneSegnaturaInteropEnum esitoValidazioneInterop;
	
	/**
	 * Segnatura.
	 */
	private SegnaturaMessaggioInteropDTO segnaturaInterop;
	
	/**
	 * Id messaggio.
	 */
	private String idMessaggioPostaNps;
	
	/**
	 * Id spedizione.
	 */
	private Integer idSpedizioneNps;
	 
	/**
	 * Tipo mittente.
	 */
	private Integer tipoMittente;
	 
	/**
	 * Id aoo.
	 */
	private Long idAoo;
	
	 
	/**
	 * Costruttore.
	 * 
	 * @param inDocumentTitle
	 *            - Document title
	 * @param inDestinatari
	 *            - destinatari
	 * @param inDestinatariCC
	 *            - destinatari CC
	 * @param inMittente
	 *            - mittente
	 * @param inOggetto
	 *            - oggetto
	 * @param inTesto
	 *            - testo
	 * @param inTipologiaInvioId
	 *            - tipologia invio
	 * @param inTipologiaMessaggioId
	 *            - tipologia messaggio
	 * @param inMsgID
	 *            - messageID
	 * @param inGuid
	 *            - guid
	 * @param inIsNotifica
	 *            - ha notifica
	 * @param inHasAllegati
	 *            - ha allegati
	 * @param inAction
	 *            - action
	 * @param inDestinatari2
	 *            - destinatari 2
	 * @param inMotivazione
	 *            - motivazione
	 * @param inDataInvio
	 *            - data invio
	 */
	public MessaggioEmailDTO(final String inDocumentTitle, final String inDestinatari, final String inDestinatariCC, final String inMittente, final String inOggetto, 
			final String inTesto, final int inTipologiaInvioId, final int inTipologiaMessaggioId, final String inMsgID, final String inGuid, final Boolean inIsNotifica, 
			final Boolean inHasAllegati, final String inAction, final String inDestinatari2, final String inMotivazione) {
		super(inMittente, inOggetto, inTesto, inDestinatari, inDestinatariCC, inMsgID, inMotivazione, null);
		this.documentTitle = inDocumentTitle;
		this.tipologiaMessaggio = new TipologiaMessaggio();
		this.tipologiaMessaggio.setIdTipologiaMessaggio(inTipologiaMessaggioId);
		this.tipologiaInvio = new TipologiaInvio();
		this.tipologiaInvio.setIdTipologiaInvio(inTipologiaInvioId);
		this.tipologiaInvioId = inTipologiaInvioId;
		this.tipologiaMessaggioId = inTipologiaMessaggioId;
		setGuid(inGuid);
		this.isNotifica = inIsNotifica;
		setHasAllegati(inHasAllegati);
		this.action = inAction;
		this.destinatari2 = inDestinatari2;
	}
	
	/**
	 * Costruttore.
	 * @param inDocumentTitle
	 * @param inDestinatari
	 * @param inDestinatariCC
	 * @param inMittente
	 * @param inOggetto
	 * @param inTesto
	 * @param inTipologiaInvioId
	 * @param inTipologiaMessaggioId
	 * @param inMsgID
	 * @param inGuid
	 * @param inIsNotifica
	 * @param inHasAllegati
	 * @param inAction
	 * @param inDestinatari2
	 * @param inMotivazione
	 * @param allegatiDocTitle
	 * @param idNodoMittente
	 * @param idUtenteMittente
	 */
	public MessaggioEmailDTO(final String inDocumentTitle, final String inDestinatari, final String inDestinatariCC, final String inMittente, final String inOggetto, 
			final String inTesto, final int inTipologiaInvioId, final int inTipologiaMessaggioId, final String inMsgID, final String inGuid, final Boolean inIsNotifica, 
			final Boolean inHasAllegati, final String inAction, final String inDestinatari2, final String inMotivazione, final List<String> allegatiDocTitle, 
			final Long idNodoMittente, final Long idUtenteMittente) {
		this(inDocumentTitle, inDestinatari, inDestinatariCC, inMittente, inOggetto, inTesto, inTipologiaInvioId, inTipologiaMessaggioId, 
				inMsgID, inGuid, inIsNotifica, inHasAllegati, inAction, inDestinatari2, inMotivazione);
		this.allegatiDocTitle = allegatiDocTitle;
		this.idNodoMittente = idNodoMittente;
		this.idUtenteMittente = idUtenteMittente;
	}
	
	
	/**
	 * Costruttore per messaggio email proveniente da NPS.
	 * 
	 * @param inDestinatari
	 * @param inDestinatariCC
	 * @param inMittente
	 * @param inOggetto
	 * @param inTipologiaInvioId
	 * @param inTipologiaMessaggioId
	 * @param inMsgID
	 * @param inIsNotifica
	 * @param inHasAllegati
	 * @param inDataRicezione
	 * @param inFolder
	 * @param idMessaggioPostaNps
	 * @param inSegnaturaInterop
	 * @param inErroriValidazioneInterop
	 * @param inWarningValidazioneInterop
	 * @param inEsitoValidazioneInterop
	 * @param inStato
	 */
	public MessaggioEmailDTO(final String inDestinatari, final String inDestinatariCC, final String inMittente, final String inOggetto, 
			final int inTipologiaInvioId, final int inTipologiaMessaggioId, final String inMsgID, final Boolean inHasAllegati, 
			final List<AllegatoDTO> inAllegati, final Date inDataRicezione, final String inFolder, final String idMessaggioPostaNps,
			final SegnaturaMessaggioInteropDTO inSegnaturaInterop, final List<String> inErroriValidazioneInterop, 
			final List<String> inWarningValidazioneInterop, final EsitoValidazioneSegnaturaInteropEnum inEsitoValidazioneInterop,
			final StatoMailEnum inStato) {
		super(inMittente, inOggetto, null, inDestinatari, inDestinatariCC, inMsgID, null, null);
		this.tipologiaMessaggio = new TipologiaMessaggio();
		this.tipologiaMessaggio.setIdTipologiaMessaggio(inTipologiaMessaggioId);
		this.tipologiaInvio = new TipologiaInvio();
		this.tipologiaInvio.setIdTipologiaInvio(inTipologiaInvioId);
		this.tipologiaInvioId = inTipologiaInvioId;
		this.tipologiaMessaggioId = inTipologiaMessaggioId;
		setHasAllegati(inHasAllegati);
		this.allegatiMail = inAllegati;
		setDataRicezione(inDataRicezione);
		this.folder = inFolder;
		this.idMessaggioPostaNps = idMessaggioPostaNps;
		this.segnaturaInterop = inSegnaturaInterop;
		this.erroriValidazioneInterop = inErroriValidazioneInterop;
		this.warningValidazioneInterop = inWarningValidazioneInterop;
		this.esitoValidazioneInterop = inEsitoValidazioneInterop;
		setStato(inStato);
	}
	
	/**
	 * Costruttore di default.
	 */
	public MessaggioEmailDTO() {
		super();
	}

	/**
	 * Getter.
	 * 
	 * @return	destinatario
	 */
	public final String getDestinatario() {
		return destinatario;
	}

	/**
	 * Getter.
	 * 
	 * @return	ultimo messaggio
	 */
	public final Integer getLastMsg() {
		return lastMsg;
	}

	/**
	 * Getter.
	 * 
	 * @return	flag TO/CC
	 */
	public final Integer getFlagToCc() {
		return flagToCc;
	}

	/**
	 * Getter.
	 * 
	 * @return	document title
	 */
	@Override
	public final String getDocumentTitle() {
		return documentTitle;
	}
	
	/**
	 * @param inDocumentTitle
	 */
	@Override
	public void setDocumentTitle(final String inDocumentTitle) {
		this.documentTitle=inDocumentTitle;
	}

	/**
	 * Getter.
	 * 
	 * @return	identificativo tipologia invio
	 */
	public final Integer getTipologiaInvioId() {
		if (tipologiaInvio == null) {
			tipologiaInvio = new TipologiaInvio();
		}
		tipologiaInvio.setIdTipologiaInvio(tipologiaInvioId);
		
		return tipologiaInvioId;
	}
	
	/**
	 * Imposta {@link #tipologiaInvio} e {@link #tipologiaInvioId}.
	 * @param inTipologiaInvioId
	 */
	public final void setTipologiaInvioId(final Integer inTipologiaInvioId) {
		if (tipologiaInvio == null) {
			tipologiaInvio = new TipologiaInvio();
		}
		tipologiaInvioId = inTipologiaInvioId;
		tipologiaInvio.setIdTipologiaInvio(tipologiaInvioId);
	}

	/**
	 * Getter.
	 * 
	 * @return	identificativo tipologia messaggio
	 */
	public final Integer getTipologiaMessaggioId() {
		if (tipologiaMessaggio == null) {
			tipologiaMessaggio = new TipologiaMessaggio();
		}
		tipologiaMessaggio.setIdTipologiaMessaggio(tipologiaMessaggioId);
		
		return tipologiaMessaggioId;
	}
	
	/**
	 * Imposta la tipologia del messaggio.
	 * @param inTipologiaMessaggioId
	 */
	public final void setTipologiaMessaggioId(final Integer inTipologiaMessaggioId) {
		if (tipologiaMessaggio == null) {
			tipologiaMessaggio = new TipologiaMessaggio();
		}
		tipologiaMessaggioId = inTipologiaMessaggioId;
		tipologiaMessaggio.setIdTipologiaMessaggio(tipologiaMessaggioId);
	}

	/**
	 * Getter.
	 * 
	 * @return	flag notifica
	 */
	public final Boolean getIsNotifica() {
		return isNotifica;
	}

	/**
	 * Getter.
	 * 
	 * @return	action
	 */
	public final String getAction() {
		return action;
	}

	/**
	 * @param inAction
	 */
	public void setAction(final String inAction) {
		this.action = inAction;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	destinatari
	 */
	public final String getDestinatari2() {
		return destinatari2;
	}
	
	/**
	 * Setter. 
	 * 
	 * @param inDestinatario - destinatario
	 */
	public void setDestinatario(final String inDestinatario) {
		this.destinatario = inDestinatario;
	}

	/**
	 * Setter. 
	 * 
	 * @param inLastMsg - Msg
	 */
	public void setLastMsg(final Integer inLastMsg) {
		this.lastMsg = inLastMsg;
	}
	
	/**
	 * Setter. 
	 * 
	 * @param inFlagToCc - flagToCC
	 */
	public void setFlagToCc(final Integer inFlagToCc) {
		this.flagToCc = inFlagToCc;
	}

	/**
	 * @return
	 */
	public TipologiaInvio getTipologiaInvio() {
		return tipologiaInvio;
	}
	
	/**
	 * @return
	 */
	public String getFolder() {
		return folder;
	}

	/**
	 * @param folder
	 */
	public void setFolder(final String folder) {
		this.folder = folder;
	}

	/**
	 * @return
	 */
	public Integer getIdNotifica() {
		return idNotifica;
	}

	/**
	 * @param idNotifica
	 */
	public void setIdNotifica(final Integer idNotifica) {
		this.idNotifica = idNotifica;
	}

	/**
	 * @return
	 */
	public Integer getTipoDestinatario() {
		return tipoDestinatario;
	}

	/**
	 * @param tipoDestinatario
	 */
	public void setTipoDestinatario(final Integer tipoDestinatario) {
		this.tipoDestinatario = tipoDestinatario;
	}

	/**
	 * @return
	 */
	public Integer getIdSpedizioneUnica() {
		return idSpedizioneUnica;
	}
	
	/**
	 * @param inIdSpedizioneUnica
	 */
	public void setIdSpedizioneUnica(final Integer inIdSpedizioneUnica) {
		idSpedizioneUnica = inIdSpedizioneUnica;
	}
	
	/**
	 * @return
	 */
	public boolean getIsSpedizioneUnica() {
		return idSpedizioneUnica != null && idSpedizioneUnica != 0;
	}

	/**
	 * @return
	 */
	public Integer getTipoEvento() {
		return tipoEvento;
	}

	/**
	 * @param tipoEvento
	 */
	public void setTipoEvento(final Integer tipoEvento) {
		this.tipoEvento = tipoEvento;
	}
	
	/**
	 * @return
	 */
	public List<AllegatoDTO> getAllegatiMail() {
		return allegatiMail;
	}

	/**
	 * @param allegatiMail
	 */
	public void setAllegatiMail(final List<AllegatoDTO> allegatiMail) {
		this.allegatiMail = allegatiMail;
	}
	
	/**
	 * @return
	 */
	public List<String> getAllegatiDocTitle() {
		return allegatiDocTitle;
	}

	/**
	 * @param allegatiDocTitle
	 */
	public void setAllegatiDocTitle(final List<String> allegatiDocTitle) {
		this.allegatiDocTitle = allegatiDocTitle;
	}

	/**
	 * @param tipologiaInvio
	 */
	public void setTipologiaInvio(final TipologiaInvio tipologiaInvio) {		
		this.tipologiaInvio = tipologiaInvio;
		
		if (tipologiaInvio!=null) {
			tipologiaInvioId = tipologiaInvio.getIdTipologiaInvio();
		}
	}

	/**
	 * Restituisce la tipologia del messaggio.
	 * @return tipologia messaggio
	 */
	public TipologiaMessaggio getTipologiaMessaggio() {
		return tipologiaMessaggio;
	}

	/**
	 * Imposta la tipologia del messaggio.
	 * @param tipologiaMessaggio
	 */
	public void setTipologiaMessaggio(final TipologiaMessaggio tipologiaMessaggio) {
		this.tipologiaMessaggio = tipologiaMessaggio;
		if (tipologiaMessaggio != null) {
			tipologiaMessaggioId = tipologiaMessaggio.getIdTipologiaMessaggio();
		}
	}
	
	/**
	 * Imposta lo stato con lo stato identificato dallo status, @see StatoMailEnum.
	 * @param inStato
	 */
	public void setStato(final Integer inStato) {
		super.setStato(StatoMailEnum.get(inStato));
	}
	
	/**
	 * @param isNotifica the isNotifica to set
	 */
	public void setIsNotifica(final Boolean isNotifica) {
		this.isNotifica = isNotifica;
	}

	/**
	 * Imposta i destinatari {@link #destinatari2}.
	 * @param inDestinatari_
	 */
	public void setDestinatari2(final String destinatari2) {
		this.destinatari2 = destinatari2;
	}

	/**
	 * @return
	 */
	public String getTipoRicevuta() {
		return tipoRicevuta;
	}

	/**
	 * @param tipoRicevuta
	 */
	public void setTipoRicevuta(final String tipoRicevuta) {
		this.tipoRicevuta = tipoRicevuta;
	}
	
	/**
	 * @return
	 */
	public Long getIdNodoMittente() {
		return idNodoMittente;
	}

	/**
	 * @param idNodoMittente
	 */
	public void setIdNodoMittente(final Long idNodoMittente) {
		this.idNodoMittente = idNodoMittente;
	}

	/**
	 * @return
	 */
	public Long getIdUtenteMittente() {
		return idUtenteMittente;
	}

	/**
	 * @param idUtenteMittente
	 */
	public void setIdUtenteMittente(final Long idUtenteMittente) {
		this.idUtenteMittente = idUtenteMittente;
	}

	/**
	 * Restituisce true se ultimo tentativo, false altrimenti.
	 * @return true se ultimo tentativo, false altrimenti
	 */
	public Boolean getUltimoTentativo() {
		return ultimoTentativo;
	}
	
	/**
	 * Imposta il flag associato all'ultimo tentativo.
	 * @param ultimoTentativo
	 */
	public void setUltimoTentativo(final Boolean ultimoTentativo) {
		this.ultimoTentativo = ultimoTentativo;
	}
	
	/**
	 * Restituisce l'id del documento.
	 * @return id documento
	 */
	public String getIdDocumento() {
		return idDocumento;
	}
	
	/**
	 * Imposta l'id del documento.
	 * @param idDocumento
	 */
	public void setIdDocumento(final String idDocumento) {
		this.idDocumento = idDocumento;
	}
 
	
	/**
	 * @return erroriValidazioneInterop
	 */
	public List<String> getErroriValidazioneInterop() {
		return erroriValidazioneInterop;
	}

	
	/**
	 * @return warningValidazioneInterop
	 */
	public List<String> getWarningValidazioneInterop() {
		return warningValidazioneInterop;
	}


	/**
	 * @return the esitoValidazioneInterop
	 */
	public EsitoValidazioneSegnaturaInteropEnum getEsitoValidazioneInterop() {
		return esitoValidazioneInterop;
	}


	/**
	 * @return the segnaturaInterop
	 */
	public SegnaturaMessaggioInteropDTO getSegnaturaInterop() {
		return segnaturaInterop;
	}


	/**
	 * @return the idMessaggioPostaNps
	 */
	public String getIdMessaggioPostaNps() {
		return idMessaggioPostaNps;
	}


	/**
	 * @return the idSpedizioneNps
	 */
	public Integer getIdSpedizioneNps() {
		return idSpedizioneNps;
	}


	/**
	 * @param spedizioneNps the idSpedizioneNps to set
	 */
	public void setIdSpedizioneNps(final Integer idSpedizioneNps) {
		this.idSpedizioneNps = idSpedizioneNps;
	}
	
	/**
	 * Restituisce il tipo mittente.
	 * @return tipo mittente
	 */
	public Integer getTipoMittente() {
		return tipoMittente;
	}
	
	/**
	 * Imposta il tipo mittente.
	 * @param tipoMittente
	 */
	public void setTipoMittente(final Integer tipoMittente) {
		this.tipoMittente = tipoMittente;
	} 
	
	/**
	 * Restituisce l'id dell'AOO.
	 * @return
	 */
	public Long getIdAoo() {
		return idAoo;
	}
	
	/**
	 * Imposta l'id dell'AOO.
	 * @param idAoo
	 */
	public void setIdAoo(final Long idAoo) {
		this.idAoo = idAoo;
	}
	
	/**
	 * @see java.lang.Object#hashCode().
	 */
	@Override
	public int hashCode() {
		return super.hashCode();
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object).
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		return super.equals(obj);
	}
	
}
