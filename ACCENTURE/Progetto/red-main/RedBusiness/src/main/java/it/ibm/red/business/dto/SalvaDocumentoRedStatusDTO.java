package it.ibm.red.business.dto;

import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.filenet.DocumentoRedFnDTO;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.helper.html.IHtmlFileHelper;

/**
 * Struttura dati di servizio per l'elaborazione del salvataggio di un documento.
 * 
 * @author m.crescentini
 *
 */
public class SalvaDocumentoRedStatusDTO extends AbstractDTO {

	private static final long serialVersionUID = -5626430795881576537L;


	/**
	 * Helper html.
	 */
	private IHtmlFileHelper htmlFileHelper;
	

	/**
	 * Flag creazione bozza.
	 */
	private boolean creazioneBozza;
	
	/**
	 * Flag spedisci mail destinatari elettronici.
	 */
	private boolean spedisciMailDestElettronici;
	
	/**
	 * Flag bozza.
	 */
	private boolean isBozza;
	
	/**
	 * Flag documento entrata.
	 */
	private boolean isDocEntrata;
	
	/**
	 * Flag docimento interno.
	 */
	private boolean isDocInterno;
	
	/**
	 * Flag contributo esterno.
	 */
	private boolean isDocContributoEsterno;
	
	/**
	 * Flag documento assegnazione interna.
	 */
	private boolean isDocAssegnazioneInterna;
	
	/**
	 * Flag documento uscita.
	 */
	private boolean isDocUscita;
	
	/**
	 * Flag mozione.
	 */
	private boolean isMozione;
	
	/**
	 * Flag precesito.
	 */
	private boolean isPrecensito;
	
	/**
	 * Flag modalità insert.
	 */
	private boolean isModalitaInsert;
	
	/**
	 * Flag modalità update.
	 */
	private boolean isModalitaUpdate;
	
	/**
	 * Flag modalità update ibrida.
	 */
	private boolean isModalitaUpdateIbrida;
	
	/**
	 * Flag modalità mail entrata.
	 */
	private boolean isModalitaMailEntrata;
	
	/**
	 * Flag modalità post cens.
	 */
	private boolean isModalitaPostCens;
	
	/**
	 * Flag iter manuale.
	 */
	private boolean isIterManuale;
		
	/**
	 * Flag da protocollare.
	 */
	private boolean isDaProtocollare;
	
	/**
	 * Flag content scannerizzato.
	 */
	private boolean isContentScannerizzato;
	
	/**
	 * Flag content modificato.
	 */
	private boolean isContentVariato;
	
	/**
	 * Flag documento inserito.
	 */
	private boolean isDocumentoInserito;
	
	/**
	 * Flag documento protocollato.
	 */
	private boolean isDocumentoProtocollato;
		
	/**
	 * Flag fscicolo automatico.
	 */
	private boolean isFascicoloAutomatico;
	
	/**
	 * Flag invia notifica protocollazione mail.
	 */
	private boolean inviaNotificaProtocollazioneMail;
			
	/**
	 * Flag utente coordinatore.
	 */
	private boolean utenteCoordinatore;
	
	/**
	 * Flag aggiorna oggetto protocollo NPS.
	 */
	private boolean aggiornaOggettoProtocolloNPS;
	
	/**
	 * Flag aggiorna tipologia documento NPS.
	 */
	private boolean aggiornaTipologiaDocumentoNPS;
	
	/**
	 * Flag aggiorna metadati estesi NPS.
	 */
	private boolean aggiornaMetadatiEstesiNPS;
	
	/**
	 * Flag titolario modificato.
	 */
	private Boolean titolarioChanged;
	
	/**
	 * Flag forza protocollazione mail.
	 */
	private boolean forzaProtocollazioneMail;
	
	/**
	 * Flag forza risposta pubblica.
	 */
	private boolean forzaRispostaPubblica;
	
	/**
	 * Flag forza atto decreto manuale.
	 */
	private boolean forzaAttoDecretoManuale;
	
	/**
	 * Flag forza spedizione destinatari elettronici.
	 */
	private boolean forzaAssSpedizioneDestElettronici;
	
	/**
	 * Assegnazioni per competenza.
	 */
	private String[] assegnazioniCompetenza;
	
	/**
	 * Assegnazioni per conoscenza.
	 */
	private String[] assegnazioniConoscenza;
	
	/**
	 * Assegnaizoni per contributo.
	 */
	private String[] assegnazioniContributo;
	
	/**
	 * Assegnazioni per firma.
	 */
	private String[] assegnazioniFirma;
	
	/**
	 * Assegnazioni per sigla.
	 */
	private String[] assegnazioniSigla;
	
	/**
	 * Assegnazioni per visto.
	 */
	private String[] assegnazioniVisto;
	
	/**
	 * Assegnazioni per fima multipla.
	 */
	private String[] assegnazioniFirmaMultipla;
	
	/**
	 * Documento da salvare.
	 */
	private DocumentoRedFnDTO docDaSalvare;
	
	/**
	 * Guid mail.
	 */
	private String inputMailGuid;
		
	/**
	 * Nome faldoni selezionati.
	 */
	private List<String> nomeFaldoniSelezionati;
	
	/**
	 * Assegnaizoni fascicolo.
	 */
	private List<AssegnazioneWorkflowDTO> assegnazioniFascicolo;
	
	/**
	 * Destinatari.
	 */
	private String[] destinatariString;
	
	/**
	 * Destinatari interni.
	 */
	private String[] destinatariInterniString;
	
	/**
	 * Lista wf.
	 */
	private List<String> workflowList;
	
	/**
	 * Allegati da firmare.
	 */
	private List<String> idAllegatiDaFirmare;
	
	/**
	 * Iter approvativo.
	 */
	private IterApprovativoDTO iterApprovativo;
	
	/**
	 * Security documento.
	 */
	private ListSecurityDTO securityDoc;

	/**
	 * Security fascicolo.
	 */
	private ListSecurityDTO securityFascicolo;
	
	/**
	 * Security documenti allacciati.
	 */
	private ListSecurityDTO securityDocAllacciati;
	
	/**
	 * Security fascicolo documenti allacciati.
	 */
	private ListSecurityDTO securityFascicoloDocAllacciati;
	
	/**
	 * Identificativo fascicolo selezionato.
	 */
	private String idFascicoloSelezionato;
	
	/**
	 * Metadati fascicolo.
	 */
	private transient Map<String, Object> metadatiFascicolo;

	/**
	 * Indice di classificazione fascicolo.
	 */
	private String indiceClassificazioneFascicolo;
	
	/**
	 * Numero protocolo.
	 */
	private int numeroProtocollo;
	
	/**
	 * Anno protocollo.
	 */
	private String annoProtocollo;
	
	/**
	 * Identificativo protocollo.
	 */
	private String idProtocollo;
	
	/**
	 * Data protocollo.
	 */
	private String dataProtocollo;
	
	/**
	 * Iter approvativo.
	 */
	private IterApprovativoDTO iterPdf;
	
	/**
	 * Utente mittente wf.
	 */
	private Long idUtenteMittenteWorkflow;
	
	/**
	 * Ufficio mittente wf.
	 */
	private Long idUfficioMittenteWorkflow;
	
	/**
	 * Utente destinatario wf.
	 */
	private Long idUtenteDestinatarioWorkflow;
	
	/**
	 * Ufficio destinatario wf.
	 */
	private Long idUfficioDestinatarioWorkflow;
	
	/**
	 * Tipologia assegnazione.
	 */
	private TipoAssegnazioneEnum isAssegnazioneInternaWorkflow;
	
	/**
	 * Identificativo documento precedente wf.
	 */
	private String idDocumentoOldWorkflow;
	
	/**
	 * Motivo assegnazione wf.
	 */
	private String motivazioneAssegnazioneWorkflow;
	 
	/**
	 * Flag iter manuale spedizione.
	 */
	private boolean iterManualeSpedizione;
	 
	/**
	 * Identificativo notifica spedizione.
	 */
	private Long idNotificaSped;
	
	/**
	 * Flag protocollazione automatica.
	 */
	private boolean protocollazioneAutomatica;
	
	/**
	 * Lista allegati con firma visibile.
	 */
	private List<String> idAllegatiConFirmaVisibile;
	 
	/**
	 * Flag protocollazione P7M.
	 */
	private boolean protocollazioneP7M;
	
	/**
	 * Flag velina documento principale.
	 */
	private boolean velinaAsDocPrincipale;
	
	/**
	 * Flag firma copia conforme.
	 */
	private boolean firmaCopiaConforme;

	/**
	 * Flag associato alla modifica del flag riservato.
	 */
	private boolean aggiornaRiservatezza;
	
	/**
	 * Flag content da recuperare su NPS.
	 */
	private boolean isContentFromNPS;
	
	/**
	 * guid seconda versione content principale su NPS.
	 */
	private String guidLastVersionNPSDocPrincipale;
	
	/**
	 * Restituisce l'helper per la gestione del file HTML.
	 * @return helper file HTML
	 */
	public IHtmlFileHelper getHtmlFileHelper() {
		return htmlFileHelper;
	}

	/**
	 * Imposta l'helper per la gestione del file HTML.
	 * @param htmlFileHelper
	 */
	public void setHtmlFileHelper(final IHtmlFileHelper htmlFileHelper) {
		this.htmlFileHelper = htmlFileHelper;
	}

	/**
	 * Restituisce il flag associato alla creazione bozza.
	 * @return flag creazione bozza
	 */
	public boolean isCreazioneBozza() {
		return creazioneBozza;
	}

	/**
	 * Imposta il flag associato alla creazione bozza.
	 * @param creazioneBozza
	 */
	public void setCreazioneBozza(final boolean creazioneBozza) {
		this.creazioneBozza = creazioneBozza;
	}

	/**
	 * Restituisce il flag associato alla spedizione della mail con destinatari elettronici.
	 * @return spedisciMailDestElettronici
	 */
	public boolean isSpedisciMailDestElettronici() {
		return spedisciMailDestElettronici;
	}

	/**
	 * Imposta il flag associato alla spedizione della mail con destinatari elettronici.
	 * @param spedisciMailDestElettronici
	 */
	public void setSpedisciMailDestElettronici(final boolean spedisciMailDestElettronici) {
		this.spedisciMailDestElettronici = spedisciMailDestElettronici;
	}

	/**
	 * Restituisce true se bozza, false altrimenti.
	 * @return true se bozza, false altrimenti
	 */
	public boolean isBozza() {
		return isBozza;
	}

	/**
	 * Imposta il flag associato alla bozza.
	 * @param isBozza
	 */
	public void setBozza(final boolean isBozza) {
		this.isBozza = isBozza;
	}

	/**
	 * Restituisce true se documento in entrata, false altrimenti.
	 * @return true se documento in entrata, false altrimenti
	 */
	public boolean isDocEntrata() {
		return isDocEntrata;
	}

	/**
	 * Imposta il flag associato al documento in entrata.
	 * @param isDocEntrata
	 */
	public void setDocEntrata(final boolean isDocEntrata) {
		this.isDocEntrata = isDocEntrata;
	}

	/**
	 * Restituisce true se il documento è internom false altrimenti.
	 * @return true se il documento è internom false altrimenti
	 */
	public boolean isDocInterno() {
		return isDocInterno;
	}

	/**
	 * Imposta il flag associato al documento interno.
	 * @param isDocInterno
	 */
	public void setDocInterno(final boolean isDocInterno) {
		this.isDocInterno = isDocInterno;
	}

	/**
	 * Restitusice true se documento di contributo esterno, false altrimenti.
	 * @return true se documento di contributo esterno, false altrimenti
	 */
	public boolean isDocContributoEsterno() {
		return isDocContributoEsterno;
	}

	/**
	 * Imposta il flag associato al contributo esterno.
	 * @param isDocContributoEsterno
	 */
	public void setDocContributoEsterno(final boolean isDocContributoEsterno) {
		this.isDocContributoEsterno = isDocContributoEsterno;
	}

	/**
	 * Restituisce true se assegnazione interna, false altrimenti.
	 * @return true se assegnazione interna, false altrimenti
	 */
	public boolean isDocAssegnazioneInterna() {
		return isDocAssegnazioneInterna;
	}

	/**
	 * Imposta il flag associato all'assegnazione interna.
	 * @param isDocAssegnazioneInterna
	 */
	public void setDocAssegnazioneInterna(final boolean isDocAssegnazioneInterna) {
		this.isDocAssegnazioneInterna = isDocAssegnazioneInterna;
	}

	/**
	 * Restituisce true se il documento è in uscita, false altrimenti.
	 * @return true se il documento è in uscita, false altrimenti
	 */
	public boolean isDocUscita() {
		return isDocUscita;
	}

	/**
	 * Imposta il flag associato al documento in uscita.
	 * @param isDocUscita
	 */
	public void setDocUscita(final boolean isDocUscita) {
		this.isDocUscita = isDocUscita;
	}

	/**
	 * Restituisce true se mozione, false altrimenti.
	 * @return true se mozione, false altrimenti
	 */
	public boolean isMozione() {
		return isMozione;
	}

	/**
	 * Imposta il flag associato alla mozione.
	 * @param isMozione
	 */
	public void setMozione(final boolean isMozione) {
		this.isMozione = isMozione;
	}

	/**
	 * Restituisce true se precensito, false altrimenti.
	 * @return true se precensito, false altrimenti
	 */
	public boolean isPrecensito() {
		return isPrecensito;
	}

	/**
	 * Imposta il flag associato al precensimento.
	 * @param isPrecensito
	 */
	public void setPrecensito(final boolean isPrecensito) {
		this.isPrecensito = isPrecensito;
	}

	/**
	 * Restituisce true se modalita insert, false altrimenti.
	 * @return true se modalita insert, false altrimenti
	 */
	public boolean isModalitaInsert() {
		return isModalitaInsert;
	}

	/**
	 * Imposta il flag associato alla modalita insert.
	 * @param isModalitaInsert
	 */
	public void setModalitaInsert(final boolean isModalitaInsert) {
		this.isModalitaInsert = isModalitaInsert;
	}

	/**
	 * Restitusice true se modalita update, false altrimenti.
	 * @return true se modalita update, false altrimenti
	 */
	public boolean isModalitaUpdate() {
		return isModalitaUpdate;
	}

	/**
	 * Imposta il flag associato alla modalita update.
	 * @param isModalitaUpdate
	 */
	public void setModalitaUpdate(final boolean isModalitaUpdate) {
		this.isModalitaUpdate = isModalitaUpdate;
	}

	/**
	 * Restituisce true se modalita update ibrida, false altrimenti.
	 * @return true se modalita update ibrida, false altrimenti
	 */
	public boolean isModalitaUpdateIbrida() {
		return isModalitaUpdateIbrida;
	}

	/**
	 * Imposta il flag associato alla modalita update ibrida.
	 * @param isModalitaUpdateIbrida
	 */
	public void setModalitaUpdateIbrida(final boolean isModalitaUpdateIbrida) {
		this.isModalitaUpdateIbrida = isModalitaUpdateIbrida;
	}

	/**
	 * Restituisce true se modalita mail entrata, false altrimenti.
	 * @return true se modalita mail entrata, false altrimenti
	 */
	public boolean isModalitaMailEntrata() {
		return isModalitaMailEntrata;
	}

	/**
	 * Imposta il flag associato alla modalita mail entrata.
	 * @param isModalitaMailEntrata
	 */
	public void setModalitaMailEntrata(final boolean isModalitaMailEntrata) {
		this.isModalitaMailEntrata = isModalitaMailEntrata;
	}

	/**
	 * Restituisce true se modalita post cens, false altrimenti.
	 * @return true se modalita post cens, false altrimenti
	 */
	public boolean isModalitaPostCens() {
		return isModalitaPostCens;
	}

	/**
	 * Imposta il flag associato alla modalita post cens.
	 * @param isModalitaPostCens
	 */
	public void setModalitaPostCens(final boolean isModalitaPostCens) {
		this.isModalitaPostCens = isModalitaPostCens;
	}

	/**
	 * Restituisce true se iter manuale, false altrimenti.
	 * @return true se iter manuale, false altrimenti
	 */
	public boolean isIterManuale() {
		return isIterManuale;
	}

	/**
	 * Imposta il flag associato all'iter manuale, false altrimenti.
	 * @param isIterManuale
	 */
	public void setIterManuale(final boolean isIterManuale) {
		this.isIterManuale = isIterManuale;
	}

	/**
	 * Restituisce true se da protocollare, false altrimenti.
	 * @return true se da protocollare, false altrimenti
	 */
	public boolean isDaProtocollare() {
		return isDaProtocollare;
	}

	/**
	 * Imposta il flag associato alla necessita di protocllazione del documento.
	 * @param isDaProtocollare
	 */
	public void setDaProtocollare(final boolean isDaProtocollare) {
		this.isDaProtocollare = isDaProtocollare;
	}

	/**
	 * Restituisce true se il content risulta scannerizzato, false altrimenti.
	 * @return true se il content risulta scannerizzato, false altrimenti
	 */
	public boolean isContentScannerizzato() {
		return isContentScannerizzato;
	}

	/**
	 * Imposta il flag associato alla scansione del content.
	 * @param isContentScannerizzato
	 */
	public void setContentScannerizzato(final boolean isContentScannerizzato) {
		this.isContentScannerizzato = isContentScannerizzato;
	}

	/**
	 * Restituisce true se il content risulta variato, false altrimenti.
	 * @return true se il content risulta variato, false altrimenti
	 */
	public boolean isContentVariato() {
		return isContentVariato;
	}

	/**
	 * Imposta il flag associato alla variazione del content.
	 * @param isContentVariato
	 */
	public void setContentVariato(final boolean isContentVariato) {
		this.isContentVariato = isContentVariato;
	}

	/**
	 * Restituisce true se documento inserito, false altrimenti.
	 * @return true se documento inserito, false altrimenti
	 */
	public boolean isDocumentoInserito() {
		return isDocumentoInserito;
	}

	/**
	 * Imposta il flag associato al documento inserito.
	 * @param isDocumentoInserito
	 */
	public void setDocumentoInserito(final boolean isDocumentoInserito) {
		this.isDocumentoInserito = isDocumentoInserito;
	}
	
	/**
	 * Restituisce true se protocollato, false altrimenti.
	 * @return true se protocollato, false altrimenti
	 */
	public boolean isDocumentoProtocollato() {
		return isDocumentoProtocollato;
	}

	/**
	 * Imposta il flag associato al documento protocollato.
	 * @param isDocumentoProtocollato
	 */
	public void setDocumentoProtocollato(final boolean isDocumentoProtocollato) {
		this.isDocumentoProtocollato = isDocumentoProtocollato;
	}

	/**
	 * Restituisce true se fascicolo automatico, false altrimenti.
	 * @return true se fascicolo automatico, false altrimenti
	 */
	public boolean isFascicoloAutomatico() {
		return isFascicoloAutomatico;
	}

	/**
	 * Imposta il flag associato al fascicolo automatico.
	 * @param isFascicoloAutomatico
	 */
	public void setFascicoloAutomatico(final boolean isFascicoloAutomatico) {
		this.isFascicoloAutomatico = isFascicoloAutomatico;
	}

	/**
	 * Restituisce true se invia notifica protocollazione mail, false altrimenti.
	 * @return true se invia notifica protocollazione mail, false altrimenti
	 */
	public boolean isInviaNotificaProtocollazioneMail() {
		return inviaNotificaProtocollazioneMail;
	}

	/**
	 * imposta il flag associato all'invio della notifica protocollazione mail.
	 * @param inviaNotificaProtocollazioneMail
	 */
	public void setInviaNotificaProtocollazioneMail(final boolean inviaNotificaProtocollazioneMail) {
		this.inviaNotificaProtocollazioneMail = inviaNotificaProtocollazioneMail;
	}

	/**
	 * Restituisce true se utente coordinatore, false altrimenti.
	 * @return true se utente coordinatore, false altrimenti
	 */
	public boolean isUtenteCoordinatore() {
		return utenteCoordinatore;
	}

	/**
	 * Imposta l'utente coordinatore.
	 * @param utenteCoordinatore
	 */
	public void setUtenteCoordinatore(final boolean utenteCoordinatore) {
		this.utenteCoordinatore = utenteCoordinatore;
	}

	/**
	 * Restituisce true se aggiorna oggetto protocollo Nps, false altrimenti.
	 * @return true se aggiorna oggetto protocollo Nps, false altrimenti
	 */
	public boolean isAggiornaOggettoProtocolloNPS() {
		return aggiornaOggettoProtocolloNPS;
	}

	/**
	 * Imposta il flag associato all'aggiornamento dell'oggetto protocollo NPS.
	 * @param aggiornaOggettoProtocolloNPS
	 */
	public void setAggiornaOggettoProtocolloNPS(final boolean aggiornaOggettoProtocolloNPS) {
		this.aggiornaOggettoProtocolloNPS = aggiornaOggettoProtocolloNPS;
	}

	/**
	 * Restituisce true se aggiorna la tipologia documento NPS, false altrimenti.
	 * @return true se aggiorna la tipologia documento NPS, false altrimenti
	 */
	public boolean isAggiornaTipologiaDocumentoNPS() {
		return aggiornaTipologiaDocumentoNPS;
	}

	/**
	 * Imposta il flag associato all'aggionamento della tipologia del documento NPS.
	 * @param aggiornaTipologiaDocumentoNPS
	 */
	public void setAggiornaTipologiaDocumentoNPS(final boolean aggiornaTipologiaDocumentoNPS) {
		this.aggiornaTipologiaDocumentoNPS = aggiornaTipologiaDocumentoNPS;
	}

	/**
	 * Restituisce true se aggiorna metadati estesi NPS, false altrimenti.
	 * @return true se aggiorna metadati estesi NPS, false altrimenti
	 */
	public boolean isAggiornaMetadatiEstesiNPS() {
		return aggiornaMetadatiEstesiNPS;
	}

	/**
	 * Imposta il flag associato all'aggiornametno dei metadati estesi NPS.
	 * @param aggiornaMetadatiEstesiNPS
	 */
	public void setAggiornaMetadatiEstesiNPS(final boolean aggiornaMetadatiEstesiNPS) {
		this.aggiornaMetadatiEstesiNPS = aggiornaMetadatiEstesiNPS;
	}

	/**
	 * Restituisce il parametro associato alla forzatura della protocollazione mail.
	 * @return forzaProtocollazioneMail
	 */
	public boolean isForzaProtocollazioneMail() {
		return forzaProtocollazioneMail;
	}

	/**
	 * Imposta il parametro associato alla forzatura della protocollazione mail.
	 * @param forzaProtocollazioneMail
	 */
	public void setForzaProtocollazioneMail(final boolean forzaProtocollazioneMail) {
		this.forzaProtocollazioneMail = forzaProtocollazioneMail;
	}

	/**
	 * Restituisce il parametro associato alla forzatura della risposta pubblica.
	 * @return
	 */
	public boolean isForzaRispostaPubblica() {
		return forzaRispostaPubblica;
	}

	/**
	 * Imposta il parametro associato alla forzatura della risposta pubblica.
	 * @param forzaRispostaPubblica
	 */
	public void setForzaRispostaPubblica(final boolean forzaRispostaPubblica) {
		this.forzaRispostaPubblica = forzaRispostaPubblica;
	}

	/**
	 * Restituisce il parametro associato alla forzatura dell'atto decreto manuale.
	 * @return forzaAttoDecretoManuale
	 */
	public boolean isForzaAttoDecretoManuale() {
		return forzaAttoDecretoManuale;
	}

	/**
	 * Imposta il parametro associato alla forzatura dell'atto decreto manuale.
	 * @param forzaAttoDecretoManuale
	 */
	public void setForzaAttoDecretoManuale(final boolean forzaAttoDecretoManuale) {
		this.forzaAttoDecretoManuale = forzaAttoDecretoManuale;
	}

	/**
	 * Restituisce il parametro associato alla forzatura dell'AssSpedizioneDestElettronici.
	 * @return forzaAssSpedizioneDestElettronici
	 */
	public boolean isForzaAssSpedizioneDestElettronici() {
		return forzaAssSpedizioneDestElettronici;
	}

	/**
	 * Imposta il parametro associato alla forzatura dell'AssSpedizioneDestElettronici.
	 * @param forzaAssSpedizioneDestElettronici
	 */
	public void setForzaAssSpedizioneDestElettronici(final boolean forzaAssSpedizioneDestElettronici) {
		this.forzaAssSpedizioneDestElettronici = forzaAssSpedizioneDestElettronici;
	}

	/**
	 * Restituisce le assegnazioni per competenza.
	 * @return assegnazioni competenza
	 */
	public String[] getAssegnazioniCompetenza() {
		return assegnazioniCompetenza;
	}

	/**
	 * Imposta le assegnazioni per competenza.
	 * @param assegnazioniCompetenza
	 */
	public void setAssegnazioniCompetenza(final String[] assegnazioniCompetenza) {
		this.assegnazioniCompetenza = assegnazioniCompetenza;
	}

	/**
	 * Restituisce le assegnazioni per conoscenza.
	 * @return assegnazioni per conoscenza
	 */
	public String[] getAssegnazioniConoscenza() {
		return assegnazioniConoscenza;
	}

	/**
	 * Imposta le assegnazioni per conoscenza.
	 * @param assegnazioniConoscenza
	 */
	public void setAssegnazioniConoscenza(final String[] assegnazioniConoscenza) {
		this.assegnazioniConoscenza = assegnazioniConoscenza;
	}

	/**
	 * Restituisce le assegnazioni per Contributo.
	 * @return assegnazioni per Contributo
	 */
	public String[] getAssegnazioniContributo() {
		return assegnazioniContributo;
	}

	/**
	 * Imposta le assegnazioni per Contributo.
	 * @param assegnazioniContributo
	 */
	public void setAssegnazioniContributo(final String[] assegnazioniContributo) {
		this.assegnazioniContributo = assegnazioniContributo;
	}

	/**
	 * Restituisce le assegnazioni firma.
	 * @return assegnazioniFirma
	 */
	public String[] getAssegnazioniFirma() {
		return assegnazioniFirma;
	}

	/**
	 * Imposta le assegnaizoni per firma.
	 * @param assegnazioniFirma
	 */
	public void setAssegnazioniFirma(final String[] assegnazioniFirma) {
		this.assegnazioniFirma = assegnazioniFirma;
	}

	/**
	 * Restituisce le assegnaizoni per sigla.
	 * @return assegnaizoni sigla
	 */
	public String[] getAssegnazioniSigla() {
		return assegnazioniSigla;
	}

	/**
	 * Imposta le assegnaizoni per sigla.
	 * @param assegnazioniSigla
	 */
	public void setAssegnazioniSigla(final String[] assegnazioniSigla) {
		this.assegnazioniSigla = assegnazioniSigla;
	}

	/**
	 * Restituisce le assegnazioni per visto.
	 * @return assegnazioni visto
	 */
	public String[] getAssegnazioniVisto() {
		return assegnazioniVisto;
	}

	/**
	 * Imposta le assegnazioni per visto.
	 * @param assegnazioniVisto
	 */
	public void setAssegnazioniVisto(final String[] assegnazioniVisto) {
		this.assegnazioniVisto = assegnazioniVisto;
	}

	/**
	 * Restituisce le assegnazioni per firma multipla.
	 * @return assegnazioni per firma multipla
	 */
	public String[] getAssegnazioniFirmaMultipla() {
		return assegnazioniFirmaMultipla;
	}

	/**
	 * Imposta le assegnazioni per firma multipla.
	 * @param assegnazioniFirmaMultipla
	 */
	public void setAssegnazioniFirmaMultipla(final String[] assegnazioniFirmaMultipla) {
		this.assegnazioniFirmaMultipla = assegnazioniFirmaMultipla;
	}

	/**
	 * Restituisce il documento da salvare.
	 * @return documento da salvare
	 */
	public DocumentoRedFnDTO getDocDaSalvare() {
		return docDaSalvare;
	}

	/**
	 * Imposta il documento da salvare.
	 * @param docDaSalvare
	 */
	public void setDocDaSalvare(final DocumentoRedFnDTO docDaSalvare) {
		this.docDaSalvare = docDaSalvare;
	}

	/**
	 * Restituisce il guid input mail.
	 * @return input mail guid
	 */
	public String getInputMailGuid() {
		return inputMailGuid;
	}

	/**
	 * @param inputMailGuid
	 */
	public void setInputMailGuid(final String inputMailGuid) {
		this.inputMailGuid = inputMailGuid;
	}

	/**
	 * Restituisce i nomi faldoni selezionati.
	 * @return nomi faldoni selezionati
	 */
	public List<String> getNomeFaldoniSelezionati() {
		return nomeFaldoniSelezionati;
	}

	/**
	 * Imposta i nomi faldoni selezionati.
	 * @param nomeFaldoniSelezionati
	 */
	public void setNomeFaldoniSelezionati(final List<String> nomeFaldoniSelezionati) {
		this.nomeFaldoniSelezionati = nomeFaldoniSelezionati;
	}

	/**
	 * Restituisce le assegnaizoni fascicolo.
	 * @return assegnazioni fascicolo
	 */
	public List<AssegnazioneWorkflowDTO> getAssegnazioniFascicolo() {
		return assegnazioniFascicolo;
	}

	/**
	 * Imposta le assegnazioni fascicolo.
	 * @param assegnazioniFascicolo
	 */
	public void setAssegnazioniFascicolo(final List<AssegnazioneWorkflowDTO> assegnazioniFascicolo) {
		this.assegnazioniFascicolo = assegnazioniFascicolo;
	}

	/**
	 * Restituisce la lista dei destinatari come string.
	 * @return destinatari come String
	 */
	public String[] getDestinatariString() {
		return destinatariString;
	}

	/**
	 * Imposta la lista dei destinatari come string.
	 * @param destinatariString
	 */
	public void setDestinatariString(final String[] destinatariString) {
		this.destinatariString = destinatariString;
	}

	/**
	 * Restituisce la lista dei destinatari interni come String.
	 * @return destinatari interni come String
	 */
	public String[] getDestinatariInterniString() {
		return destinatariInterniString;
	}

	/**
	 * Imposta la lista dei destinatari interni come String.
	 * @param destinatariInterniString
	 */
	public void setDestinatariInterniString(final String[] destinatariInterniString) {
		this.destinatariInterniString = destinatariInterniString;
	}

	/**
	 * Restituisce la lista dei workflow.
	 * @return workflow lista
	 */
	public List<String> getWorkflowList() {
		return workflowList;
	}
	
	/**
	 * Imposta la lista dei workflow.
	 * @param workflowList
	 */
	public void setWorkflowList(final List<String> workflowList) {
		this.workflowList = workflowList;
	}

	/**
	 * Restituisce l'id degli allegati da firmare.
	 * @return id degli allegati da firmare
	 */
	public List<String> getIdAllegatiDaFirmare() {
		return idAllegatiDaFirmare;
	}

	/**
	 * Imposta gli id degli allegati da firmare.
	 * @param idAllegatiDaFirmare
	 */
	public void setIdAllegatiDaFirmare(final List<String> idAllegatiDaFirmare) {
		this.idAllegatiDaFirmare = idAllegatiDaFirmare;
	}

	/**
	 * Restitusce l'iter approvativo.
	 * @return iter approvativo
	 */
	public IterApprovativoDTO getIterApprovativo() {
		return iterApprovativo;
	}

	/**
	 * imposta l'iter approvativo.
	 * @param iterApprovativo
	 */
	public void setIterApprovativo(final IterApprovativoDTO iterApprovativo) {
		this.iterApprovativo = iterApprovativo;
	}

	/**
	 * Restituisce le security.
	 * @return security
	 */
	public ListSecurityDTO getSecurityDoc() {
		return securityDoc;
	}

	/**
	 * Imposta le security.
	 * @param securityDoc
	 */
	public void setSecurityDoc(final ListSecurityDTO securityDoc) {
		this.securityDoc = securityDoc;
	}

	/**
	 * Restituisce le security del fascicolo.
	 * @return security del fascicolo
	 */
	public ListSecurityDTO getSecurityFascicolo() {
		return securityFascicolo;
	}

	/**
	 * Imposta le security del fascicolo.
	 * @param securityFascicolo
	 */
	public void setSecurityFascicolo(final ListSecurityDTO securityFascicolo) {
		this.securityFascicolo = securityFascicolo;
	}

	/**
	 * Restituisce le security dei documenti allacciati.
	 * @return security dei documenti alalcciati
	 */
	public ListSecurityDTO getSecurityDocAllacciati() {
		return securityDocAllacciati;
	}

	/**
	 * Imposta le security dei documenti allacciati.
	 * @param securityDocAllacciati
	 */
	public void setSecurityDocAllacciati(final ListSecurityDTO securityDocAllacciati) {
		this.securityDocAllacciati = securityDocAllacciati;
	}

	/**
	 * Restituisce le security fascicolo dei documenti alacciati.
	 * @return security fascicolo dei documenti alacciati
	 */
	public ListSecurityDTO getSecurityFascicoloDocAllacciati() {
		return securityFascicoloDocAllacciati;
	}

	/**
	 * Imposta le security fascicolo dei documenti alacciati.
	 * @param securityFascicoloDocAllacciati
	 */
	public void setSecurityFascicoloDocAllacciati(final ListSecurityDTO securityFascicoloDocAllacciati) {
		this.securityFascicoloDocAllacciati = securityFascicoloDocAllacciati;
	}

	/**
	 * Restituisce l'id del fascicolo selezionato.
	 * @return id fascicolo selezionato
	 */
	public String getIdFascicoloSelezionato() {
		return idFascicoloSelezionato;
	}

	/**
	 * Imposta l'id del fascicolo selezionato.
	 * @param idFascicoloSelezionato
	 */
	public void setIdFascicoloSelezionato(final String idFascicoloSelezionato) {
		this.idFascicoloSelezionato = idFascicoloSelezionato;
	}

	/**
	 * Restituisce i metadati del fascicolo.
	 * @return metadati fascicolo
	 */
	public Map<String, Object> getMetadatiFascicolo() {
		return metadatiFascicolo;
	}

	/**
	 * Imposta i metadati fascicolo.
	 * @param metadatiFascicolo
	 */
	public void setMetadatiFascicolo(final Map<String, Object> metadatiFascicolo) {
		this.metadatiFascicolo = metadatiFascicolo;
	}

	/**
	 * Restituisce l'indice di classificazione del fascicolo.
	 * @return indice di classificazione del fascicolo
	 */
	public String getIndiceClassificazioneFascicolo() {
		return indiceClassificazioneFascicolo;
	}

	/**
	 * Imposta l'indice di classificazione del fascicolo.
	 * @param indiceClassificazioneFascicolo
	 */
	public void setIndiceClassificazioneFascicolo(final String indiceClassificazioneFascicolo) {
		this.indiceClassificazioneFascicolo = indiceClassificazioneFascicolo;
	}

	/**
	 * Restituisce il numero protocollo.
	 * @return numero protocollo
	 */
	public int getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * Imposta il numero protocollo.
	 * @param numeroProtocollo
	 */
	public void setNumeroProtocollo(final int numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}

	/**
	 * Restituisce l'anno protocollo.
	 * @return anno protocollo
	 */
	public String getAnnoProtocollo() {
		return annoProtocollo;
	}

	/**
	 * Impsota l'anno protocollo.
	 * @param annoProtocollo
	 */
	public void setAnnoProtocollo(final String annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}

	/**
	 * Restituisce l'id del protocollo.
	 * @return id protocollo
	 */
	public String getIdProtocollo() {
		return idProtocollo;
	}

	/**
	 * Imposta l'id del protocollo.
	 * @param idProtocollo
	 */
	public void setIdProtocollo(final String idProtocollo) {
		this.idProtocollo = idProtocollo;
	}

	/**
	 * Restituisce la data protocollo.
	 * @return data protocollo
	 */
	public String getDataProtocollo() {
		return dataProtocollo;
	}

	/**
	 * Imposta la data protocollo.
	 * @param dataProtocollo
	 */
	public void setDataProtocollo(final String dataProtocollo) {
		this.dataProtocollo = dataProtocollo;
	}

	/**
	 * Restituisce l'iter approvativo.
	 * @return iter approvativo
	 */
	public IterApprovativoDTO getIterPdf() {
		return iterPdf;
	}

	/**
	 * Imposta l'iter Pdf.
	 * @param iterPdf
	 */
	public void setIterPdf(final IterApprovativoDTO iterPdf) {
		this.iterPdf = iterPdf;
	}

	/**
	 * Restituisce l'id dell'utente mittente del workflow.
	 * @return id dell'utente mittente del workflow
	 */
	public Long getIdUtenteMittenteWorkflow() {
		return idUtenteMittenteWorkflow;
	}

	/**
	 * Imposta l'id dell'utente mittente del workflow.
	 * @param idUtenteMittenteWorkflow
	 */
	public void setIdUtenteMittenteWorkflow(final Long idUtenteMittenteWorkflow) {
		this.idUtenteMittenteWorkflow = idUtenteMittenteWorkflow;
	}

	/**
	 * Restituisce l'id dell'ufficio mittente del workflow.
	 * @return id dell'ufficio mittente del workflow
	 */
	public Long getIdUfficioMittenteWorkflow() {
		return idUfficioMittenteWorkflow;
	}

	/**
	 * Imposta l'id dell'ufficio mittente del workflow
	 * @param idUfficioMittenteWorkflow
	 */
	public void setIdUfficioMittenteWorkflow(final Long idUfficioMittenteWorkflow) {
		this.idUfficioMittenteWorkflow = idUfficioMittenteWorkflow;
	}

	/**
	 * Restituisce l'id dell'utente destinatario del workflow.
	 * @return id utente destinatario del workflow
	 */
	public Long getIdUtenteDestinatarioWorkflow() {
		return idUtenteDestinatarioWorkflow;
	}

	/**
	 * Imposta l'id dell'utente destinatario del workflow.
	 * @param idUtenteDestinatarioWorkflow
	 */
	public void setIdUtenteDestinatarioWorkflow(final Long idUtenteDestinatarioWorkflow) {
		this.idUtenteDestinatarioWorkflow = idUtenteDestinatarioWorkflow;
	}

	/**
	 * Restituisce l'id dell'ufficio destinatario del workflow.
	 * @return id ufficio destinatario del workflow
	 */
	public Long getIdUfficioDestinatarioWorkflow() {
		return idUfficioDestinatarioWorkflow;
	}

	/**
	 * Imposta l'id dell'ufficio destinatario del workflow.
	 * @param idUfficioDestinatarioWorkflow
	 */
	public void setIdUfficioDestinatarioWorkflow(final Long idUfficioDestinatarioWorkflow) {
		this.idUfficioDestinatarioWorkflow = idUfficioDestinatarioWorkflow;
	}

	/**
	 * Restituisce il tipo assegnazione.
	 * @return tipo assegnazione
	 */
	public TipoAssegnazioneEnum getIsAssegnazioneInternaWorkflow() {
		return isAssegnazioneInternaWorkflow;
	}

	/**
	 * Imposta il tipo assegnazione.
	 * @param isAssegnazioneInternaWorkflow
	 */
	public void setIsAssegnazioneInternaWorkflow(final TipoAssegnazioneEnum isAssegnazioneInternaWorkflow) {
		this.isAssegnazioneInternaWorkflow = isAssegnazioneInternaWorkflow;
	}

	/**
	 * Restituisce l'id del documento old.
	 * @return id documento old
	 */
	public String getIdDocumentoOldWorkflow() {
		return idDocumentoOldWorkflow;
	}

	/**
	 * Imposta l'id del documento old.
	 * @param idDocumentoOldWorkflow
	 */
	public void setIdDocumentoOldWorkflow(final String idDocumentoOldWorkflow) {
		this.idDocumentoOldWorkflow = idDocumentoOldWorkflow;
	}

	/**
	 * Restituisce la motivazione assegnazione del workflow.
	 * @return motivazione assegnazione workflow
	 */
	public String getMotivazioneAssegnazioneWorkflow() {
		return motivazioneAssegnazioneWorkflow;
	}

	/**
	 * Imposta la motivazione assegnazione del workflow.
	 * @param motivazioneAssegnazioneWorkflow
	 */
	public void setMotivazioneAssegnazioneWorkflow(final String motivazioneAssegnazioneWorkflow) {
		this.motivazioneAssegnazioneWorkflow = motivazioneAssegnazioneWorkflow;
	}

	/**
	 * Restituisce true se il titolario ha subito modifiche, false altrimenti.
	 * @return true se il titolario ha subito modifiche, false altrimenti
	 */
	public Boolean isTitolarioChanged() {
		return titolarioChanged;
	}

	/**
	 * Imposta il flag associato alle variazioni del titolario.
	 * @param isTitolarioChanged
	 */
	public void setTitolarioChanged(final Boolean isTitolarioChanged) {
		this.titolarioChanged = isTitolarioChanged;
	}

	/**
	 * Restituisce true se iter manuale spedizione, false altrimenti.
	 * @return true se iter manuale spedizione, false altrimenti
	 */
	public boolean getIterManualeSpedizione() {
		return iterManualeSpedizione;
	}

	/**
	 * Imposta il flag associato all'iter manuale spedizione.
	 * @param iterManualeSpedizione
	 */
	public void setIterManualeSpedizione(final boolean iterManualeSpedizione) {
		this.iterManualeSpedizione = iterManualeSpedizione;
	}

	/**
	 * Restituisce l'id della notifica spedizione.
	 * @return id della notifica spedizione
	 */
	public Long getIdNotificaSped() {
		return idNotificaSped;
	}

	/**
	 * Imposta l'id della notifica spedizione.
	 * @param idNotificaSped
	 */
	public void setIdNotificaSped(final Long idNotificaSped) {
		this.idNotificaSped = idNotificaSped;
	}

	/**
	 * Restituisce true se protocollazione automatica, false altrimenti.
	 * @return true se protocollazione automatica, false altrimenti
	 */
	public boolean isProtocollazioneAutomatica() {
		return protocollazioneAutomatica;
	}

	/**
	 * Imposta il flag associato alla protocollazione automatica.
	 * @param protocollazioneAutomatica
	 */
	public void setProtocollazioneAutomatica(final boolean protocollazioneAutomatica) {
		this.protocollazioneAutomatica = protocollazioneAutomatica;
	}

	/**
	 * Restituisce gli id degli allegati con firma visibile.
	 * @return gli id degli allegati con firma visibile
	 */
	public List<String> getIdAllegatiConFirmaVisibile() {
		return idAllegatiConFirmaVisibile;
	}

	/**
	 * Imposta gli id degli allegati con firma visibile.
	 * @param idAllegatiConFirmaVisibile
	 */
	public void setIdAllegatiConFirmaVisibile(final List<String> idAllegatiConFirmaVisibile) {
		this.idAllegatiConFirmaVisibile = idAllegatiConFirmaVisibile;
	}

	/**
	 * Restituisce true se protocollazione P7M, false altrimenti.
	 * @return true se protocollazione P7M, false altrimenti
	 */
	public boolean isProtocollazioneP7M() {
		return protocollazioneP7M;
	}

	/**
	 * Imposta il flag associato alla protocollazione P7M.
	 * @param protocollazioneP7M
	 */
	public void setProtocollazioneP7M(final boolean protocollazioneP7M) {
		this.protocollazioneP7M = protocollazioneP7M;
	}

	/**
	 * Restituisce true se firma copia conforme, false altrimenti
	 * @return flag associato alla firma copia conforme
	 */
	public boolean isFirmaCopiaConforme() {
		return firmaCopiaConforme;
	}

	/**
	 * Imposta il flag associato alal firma copia conforme.
	 * @param firmaCopiaConforme
	 */
	public void setFirmaCopiaConforme(boolean firmaCopiaConforme) {
		this.firmaCopiaConforme = firmaCopiaConforme;
	}

	/**
	 * Restituisce true se mostrare la velina come documento principale, false altrimenti.
	 * @return true se mostrare la velina come documento principale, false altrimenti
	 */
	public boolean isVelinaAsDocPrincipale() {
		return velinaAsDocPrincipale;
	}

	/**
	 * Imposta la visibilita della velina.
	 * @param velinaAsDocPrincipale
	 */
	public void setVelinaAsDocPrincipale(final boolean velinaAsDocPrincipale) {
		this.velinaAsDocPrincipale = velinaAsDocPrincipale;
	}
	
	/**
	 * Restituisce true se il flag riservato è stato aggiornato.
	 * @return true se flag riservato è stato modificato, false altrimenti
	 */
	public boolean isAggiornaRiservatezza() {
		return aggiornaRiservatezza;
	}

	/**
	 * Imposta il valore associato al flag Riservato.
	 * @param aggiornaRiservatezza
	 */
	public void setAggiornaRiservatezza(boolean aggiornaRiservatezza) {
		this.aggiornaRiservatezza = aggiornaRiservatezza;
	}

	/**
	 * Restituisce true se è necessario recuperare i content da NPS, false altrimenti.
	 * 
	 * @return the isContentFromNPS
	 */
	public boolean isContentFromNPS() {
		return isContentFromNPS;
	}

	/**
	 * Imposta il valore associato al flag isContentFromNPS.
	 * 
	 * @param isContentFromNPS the isContentFromNPS to set
	 */
	public void setContentFromNPS(boolean isContentFromNPS) {
		this.isContentFromNPS = isContentFromNPS;
	}
	
	/**
	 * @return the guidLastVersionNPSDocPrincipale
	 */
	public String getGuidLastVersionNPSDocPrincipale() {
		return guidLastVersionNPSDocPrincipale;
	}

	/**
	 * @param guidLastVersionNPSDocPrincipale the guidLastVersionNPSDocPrincipale to set
	 */
	public void setGuidLastVersionNPSDocPrincipale(String guidLastVersionNPSDocPrincipale) {
		this.guidLastVersionNPSDocPrincipale = guidLastVersionNPSDocPrincipale;
	}
	
}