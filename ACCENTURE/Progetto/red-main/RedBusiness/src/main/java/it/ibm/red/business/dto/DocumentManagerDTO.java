package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.RicercaGenericaTypeEnum;
import it.ibm.red.business.enums.RiservatezzaEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.Legislatura;
import it.ibm.red.business.persistence.model.NodoUtenteCoordinatore;
import it.ibm.red.business.persistence.model.TipoProcedimento;

/**
 * DTO document manager.
 */
public class DocumentManagerDTO implements Serializable {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -6698982027339584741L;
	
	/**
	 * Categoria.
	 */
	private CategoriaDocumentoEnum categoria;

	/**
	 * Modalità maschera.
	 */
	private String modalitaMaschera;
	
	/**
	 * Flag combo formati visibile.
	 */
	private boolean isComboFormatiDocumentoVisible;

	/**
	 * Lista formati.
	 */
	private List<FormatoDocumentoEnum> comboFormatiDocumento;
	
	/**
	 * Lista tipologie documento.
	 */
	private List<TipologiaDocumentoDTO> comboTipologiaDocumento;
	
	/**
	 * Liste tipologie documento allegati.
	 */
	private List<TipologiaDocumentoDTO> comboTipologiaDocumentoAllegati;

	/**
	 * Lista tipi procedimento.
	 */
	private List<TipoProcedimento> comboTipiProcedimento;

	/**
	 * Flag tipologia disabilitata.
	 */
	private boolean tipologiaDocumentoDisable;
	
	/**
	 * Flag mittente visibile.
	 */
	private boolean isMittenteVisible;
	
	/**
	 * Flag destinatarti visibili.
	 */
	private boolean isDestinatariVisible;
	
	/**
	 * Lista mezzi ricezione.
	 */
	private List<MezzoRicezioneDTO> allMezziRicezione;
	
	/**
	 * Lista mezzi spedizione.
	 */
	private List<MezzoRicezioneDTO> comboMezziRicezione;
	
	/**
	 * Contatti preferiti.
	 */
	private List<Contatto> contattiPreferiti;
	
	/**
	 * Contatti preferiti filtrati.
	 */
	private List<Contatto> contattiPreferitiFiltered;
	
	/**
	 * Lista destinatari.
	 */
	private List<DestinatarioRedDTO> destinatari;
	
	/**
	 * Destinatario.
	 */
	private DestinatarioRedDTO destinatarioVisualizzato;
	
	/**
	 * Contatto.
	 */
	private Contatto contattoDaVisualizzare;
	
	/**
	 * Lista mezzi.
	 */
	private List<MezzoSpedizioneEnum> comboMezziSpedizione;
	
	/**
	 * Flag tracciamento visibile.
	 */
	private boolean isTracciamentoVisible;
	
	/**
	 * Flag integrazione dati visibile.
	 */
	private boolean isIntegrazioneDatiVisible;
	
	/**
	 * Lista riservato.
	 */
	private List<RiservatezzaEnum> comboRiservatezza;
	
	/**
	 * Flag riservato.
	 */
	private boolean riservato;
	
	/**
	 * INTERNO/ESTERNO.
	 */
	private List<TipologiaDestinatarioEnum> comboTipologiaDestinatari;
	
	/**
	 * TO / CC.
	 */
	private List<ModalitaDestinatarioEnum> comboModalitaDestinatario;
	
	
	/**
	 * Se l'iter approvativo selezionato è manuale abilita il campo tipo assegnazione - assegnatario.
	 */
	private List<IterApprovativoDTO> comboIterApprovativi;
	
	
	/**
	 * Lista tipi assegnazione.
	 */
	private List<TipoAssegnazioneEnum> comboTipoAssegnazione;
	
	/**
	 * Flag combo tipi assegnazione visibile.
	 */
	private boolean comboTipoAssegnazioneVisible;
	
	/**
	 * Flagt assegnatario visibile.
	 */
	private boolean assegnatarioOrganigrammaVisible;
	
	/**
	 * Flag responsabile visibile.
	 */
	private boolean comboResponsabileCopiaConformeVisible;
	
	/**
	 * Lista responsabili.
	 */
	private List<ResponsabileCopiaConformeDTO> comboResponsabileCopiaConforme;
	
	/**
	 * Flag firma copia conforme visibile.
	 */
	private boolean checkFirmaConCopiaConformeVisible;
	
	/**
	 * Flag firma digitale RGS visibile.
	 */
	private boolean firmaDigitaleRGSVisible;
	
	/**
	 * Flag Corte dei Conti visisbile.
	 */
	private boolean flagCorteContiVisible;
	
	/**
	 * Lista momenti protocollazione.
	 */
	private List<MomentoProtocollazioneEnum> comboMomentoProtocollazione;
	
	/**
	 * Flag combo momento protocollazione disabilitato.
	 */
	private boolean comboMomentoProtocollazioneDisabled;
	
	/**
	 * Flag da non protocollare visibile.
	 */
	private boolean daNonProtocollareVisible;
	
	/**
	 * Flag registro riservato visibile.
	 */
	private boolean registroRiservatoVisible;
	
	/**
	 * Flag iter visibile.
	 */
	private boolean iterDocUscitaVisible;
	
	/**
	 * Lista coordinatori.
	 */
	private List<NodoUtenteCoordinatore> comboCoordinatori;
	
	/**
	 * Flag coordinatore visibile.
	 */
	private boolean comboCoordinatoriVisible;
	
	/**
	 * Identificativo ragioniere.
	 */
	private Long idRagioniere;
	
	/**
	 * Mac anno.
	 */
	private int maxAnno;
	
	/**
	 * Flag pannello barcode visibile.
	 */
	private boolean barcodePanelVisible;
	
	/**
	 * Flag numero rdp read only.
	 */
	private boolean numeroRdpReadOnly;
	
	/**
	 * Flag pannello rdp visibile.
	 */
	private boolean rdpPanelVisible;
	
	/**
	 * Flag pannello doc principale visibile.
	 */
	private boolean docPrincipalePanelVisible;
	
	/**
	 * TRUE = Modifica
	 * FALSE = Creazione.
	 */
	private boolean inModifica;
	
	/**
	 * Lista template.
	 */
	private List<TemplateDTO> templateFileList;
	
	/**
	 * Flag lista template visibile.
	 */
	private boolean templateFileListVisible;
	
	/**
	 * Flag verifica firma visibile.
	 */
	private boolean verificaFirmaDocPrincipaleVisible;
	
	/**
	 * Flag contributo numero/anno visibile.
	 */
	private boolean contributoNumAnnoEsternoVisible;
	
	/**
	 * Serve sia all'indice di classificazione che al nome del fascicolo.
	 * sono nello stesso div 
	 */
	private boolean indiceDiClassificazioneVisible;
	
	/**
	 * Flag indice necessario.
	 */
	private boolean indiceDiClassificazioneRequired;
	
	/**
	 * Flag assegnatario per competenza visibile.
	 */
	private boolean assegnatarioPerCompetenzaVisible;
	
	/**
	 * Lista titolari.
	 */
	private List<TitolarioDTO> titolarioRootList;
	
	/**
	 * Flag protocollo mittente visibile.
	 */
	private boolean protocolloMittenteRispostaVisible;
	
	/**
	 * Flag protocollo risposta verificato.
	 */
	private boolean protocolloRispostaVerificato;
	
	/**
	 * Flag raccolta visibile.
	 */
	private boolean raccoltaFadVisible;
	
	/**
	 * Lista amministrazioni.
	 */
	private List<FadAmministrazioneDTO> comboFadAmministrazioni;
	
	/** ALLEGATI START ************************************************************/
	
	/**
	 * Lista formati.
	 */
	private List<FormatoAllegatoDTO> comboFormatiAllegato;
	
	/**
	 * Flag copia conforme.
	 */
	private boolean allegatiResponsabiliCopiaConformeVisible;
	
	/**
	 * Flag allegati da firmare.
	 */
	private boolean allegatiCheckDaFirmareVisible;
	
	/**
	 * Flag allegati modificabili.
	 */
	private boolean allegatiModificabili;
	/** ALLEGATI END   ************************************************************/
	
	/**SPEDIZIONE START****************************************************************************/
	/**
	 * Carico le caselle impostate per quell'utente, me le tengo per non ricaricarle.
	 * ogni volta che viene cambiato l'iter approvativo 
	 */
	private List<CasellaPostaDTO> caselleSpedizioneByUser;

	/**
	 * Lista caselle spedizione.
	 */
	private List<CasellaPostaDTO> comboCaselleSpedizione;

	/**
	 * Lista testi predefiniti.
	 */
	private List<TestoPredefinitoDTO> comboTestiPredefiniti;
	
	/**
	 * Prefisso oggetto mail.
	 */
	private String prefixOggettoMail;
	
	/**
	 * E' la lista di documenti contenuti nello stesso fascicolo del documento principale.
	 * che possono essere inclusi come allegati nella mail di spedizione 
	 */
	private List<DetailDocumentRedDTO> allegatiMail;
	
	/**
	 * Flag tab spezione disabilitato.
	 */
	private boolean tabSpedizioneDisabled;
	
	/**
	 * Flag tab spedizione visibile.
	 */
	private boolean tabSpedizioneVisible;
	
	/**SPEDIZIONE END******************************************************************************/
	
	/**
	 * Combo ricerca fascicolo.
	 */
	private List<RicercaGenericaTypeEnum> comboRicercaFascicoloModalita;
	
	/** CONFIRM START gestione dialog di conferma protocollazione*********************************/

	/**
	 * Messaggio conferma.
	 */
	private String dialogConfirmMessage;
	/** CONFIRM END *******************************************************************************/
	
	/**
	 * Quando il documento è in modifica gli assegnatari non possono essere modificati.
	 */
	private boolean assegnazioneBtnDisabled;
	
	/**
	 * Il tab assegnazioni e' abilitato solo quando l'iter e' manuale.
	 */
	private boolean tabAssegnazioniDisabled;
	
	/** INDICANO LO STATO DELLA MASCHERA START*******************************************/

	/**
	 * Flag modifia ingresso.
	 */
	private boolean isInModificaIngresso;
	
	/**
	 * Flag modifica uscita.
	 */
	private boolean isInModificaUscita;
	
	/**
	 * Flag creazione ingresso.
	 */
	private boolean isInCreazioneIngresso;
	
	/**
	 * Flag creazione uscita.
	 */
	private boolean isInCreazioneUscita;
	/** INDICANO LO STATO DELLA MASCHERA END*******************************************/
	
	/** LEGISLATURA START*********************************************************************************/
	/**
	 * Ultima legislatura.
	 */
	private Legislatura ultimaLegislatura;
	/** LEGISLATURA END*********************************************************************************/
	
	/**
	 * Flag protocolla mail (INDICA SE SI STA CREANDO UN DOCUMENTO TRAMITE LA PROTOCOLLAZIONE DI UNA E-MAIL ).
	 */
	private boolean flagProtocollaMail;
	
	/**
	 * Lista estensioni ammesse.
	 */
	private List<String> estensioniAmmesse;
	
	/**
	 * Lista estensioni.
	 */
	private List<String> estensioniAll;
	
	/**
	 * Flag raccolta fad.
	 */
	private boolean isCreaRaccoltaFadDisabled;
	
	/**
	 * Flag tab contributi esterni visibile.
	 */
	private boolean tabContributiEsterniVisible;
	
	/**
	 * Flag siebel.
	 */
	private boolean rdsSiebel;
	
	/**
	 * Flag documento uscita visibile.
	 */
	private boolean templateDocUscitaVisible;
	
	/**
	 * Flag documento uscita generato da template.
	 */
	private boolean generatedByTemplateDocUscita;
	
	/** Shortcut contatti. **** start**/
	/**
	 * Contatto da inserire.
	 */
	private Contatto inserisciContattoItem;
	
	/**
	 * DTO ricerca rubrica.
	 */
	private RicercaRubricaDTO ricercaRubricaDTO;
	
	/**
	 * Lista contatti.
	 */
	private List<Contatto> resultRicercaContatti;
	
	/**
	 * Lista contatti filtrati.
	 */
	private List<Contatto> resultRicercaContattiFiltered;
	/** Shortcut contatti. **** end**/
	
	/**
	 * Flag formato disabilitato.
	 */
	private boolean formatoDocumentoDisabled;
	
	/**
	 * Flag disabilita bottone protocollo.
	 */
	private boolean disabilitaButtonProtSemi;
	
	/**
	 * Registri repertorio configurati.
	 */
	private Map<Long, List<RegistroRepertorioDTO>> registriRepertorioConfigured;
	
	/**
	 * Flag metadati estesi non editabili.
	 */
	private boolean detailMetadatiEstesiNonEditabili;
 	
	/**
	 * Flag indice classificazione disabilitato.
	 */
	private boolean indiceDiClassificazioneEFascicoloDisabled;
		
	/**
	 * Flag check stampigliatura.
	 */
	private boolean checkStampigliaturaSiglaVisible;
	 
		 
	/*******************************************GET & SET**********************************************************************************/
	/**
	 * Restituisce la lista dei formati documento per la popolazione della combobox.
	 * @return lista FormatoDocumentoEnum
	 */
	public List<FormatoDocumentoEnum> getComboFormatiDocumento() {
		return comboFormatiDocumento;
	}

	/**
	 * Imposta la lista dei formati documento per la popolazione della combobox.
	 * @param comboFormatiDocumento
	 */
	public void setComboFormatiDocumento(final List<FormatoDocumentoEnum> comboFormatiDocumento) {
		this.comboFormatiDocumento = comboFormatiDocumento;
	}

	/**
	 * Restituisce la lista delle tipologie documento per la popolazione della combobox.
	 * @return lista di TipologiaDocumentoDTO
	 */
	public List<TipologiaDocumentoDTO> getComboTipologiaDocumento() {
		return comboTipologiaDocumento;
	}

	/**
	 * Imposta la lista delle tipologie documento per la popolazione della combobox.
	 * @param comboTipologiaDocumento
	 */
	public void setComboTipologiaDocumento(final List<TipologiaDocumentoDTO> comboTipologiaDocumento) {
		this.comboTipologiaDocumento = comboTipologiaDocumento;
	}

	/**
	 * Restituisce la lista delle tipologie documento degli allegati per la popolazione della combobox.
	 * @return lista di TipologiaDocumentoDTO
	 */
	public List<TipologiaDocumentoDTO> getComboTipologiaDocumentoAllegati() {
		return comboTipologiaDocumentoAllegati;
	}

	/**
	 * Imposta la lista delle tipologie documento degli allegati per la popolazione della combobox.
	 * @param comboTipologiaDocumentoAllegati
	 */
	public void setComboTipologiaDocumentoAllegati(final List<TipologiaDocumentoDTO> comboTipologiaDocumentoAllegati) {
		this.comboTipologiaDocumentoAllegati = comboTipologiaDocumentoAllegati;
	}

	/**
	 * Restituisce true se la combo dei formati documento è visibile, false altrimenti.
	 * @return true se la combo dei formati documento è visibile, false altrimenti
	 */
	public boolean isComboFormatiDocumentoVisible() {
		return isComboFormatiDocumentoVisible;
	}

	/**
	 * Imposta il flag: isComboFormatiDocumentoVisible.
	 * @param isComboFormatiDocumentoVisible
	 */
	public void setComboFormatiDocumentoVisible(final boolean isComboFormatiDocumentoVisible) {
		this.isComboFormatiDocumentoVisible = isComboFormatiDocumentoVisible;
	}

	/**
	 * Restituisce la lista dei tipi procedimento per la popolazione della combobox.
	 * @return lista di TipoProcedimento
	 */
	public List<TipoProcedimento> getComboTipiProcedimento() {
		return comboTipiProcedimento;
	}

	/**
	 * Imposta la lista dei tipi procedimento per la popolazione della combobox.
	 * @param comboTipiProcedimento
	 */
	public void setComboTipiProcedimento(final List<TipoProcedimento> comboTipiProcedimento) {
		this.comboTipiProcedimento = comboTipiProcedimento;
	}

	/**
	 * Restituisce true se il mittente è visibile, false altrimenti.
	 * @return true se il mittente è visibile, false altrimenti
	 */
	public boolean isMittenteVisible() {
		return isMittenteVisible;
	}

	/**
	 * Imposta la visibilita del mittente.
	 * @param isMittenteVisible
	 */
	public void setMittenteVisible(final boolean isMittenteVisible) {
		this.isMittenteVisible = isMittenteVisible;
	}

	/**
	 * Restituisce true se i destinatari sono visibili, false altrimenti.
	 * @return true se i destinatari sono visibili, false altrimenti
	 */
	public boolean isDestinatariVisible() {
		return isDestinatariVisible;
	}

	/**
	 * Imposta la visibilita dei destinatari.
	 * @param isDestinatariVisible
	 */
	public void setDestinatariVisible(final boolean isDestinatariVisible) {
		this.isDestinatariVisible = isDestinatariVisible;
	}

	/**
	 * Restituisce la lista dei Mezzi di ricezione per il popolamento della combobox.
	 * @return lista di MezzoRicezioneDTO
	 */
	public List<MezzoRicezioneDTO> getComboMezziRicezione() {
		return comboMezziRicezione;
	}

	/**
	 * Imposta la lista dei Mezzi di ricezione per il popolamento della combobox.
	 * @param comboMezziRicezione
	 */
	public void setComboMezziRicezione(final List<MezzoRicezioneDTO> comboMezziRicezione) {
		this.comboMezziRicezione = comboMezziRicezione;
	}

	/**
	 * Restituisce la lista di tutti i mezzi di ricezione.
	 * @return lista di MezzoRicezioneDTO
	 */
	public List<MezzoRicezioneDTO> getAllMezziRicezione() {
		return allMezziRicezione;
	}

	/**
	 * Imposta la lista dei mezzi di ricezione.
	 * @param allMezziRicezione
	 */
	public void setAllMezziRicezione(final List<MezzoRicezioneDTO> allMezziRicezione) {
		this.allMezziRicezione = allMezziRicezione;
	}

	/**
	 * Restituisce la lista dei contatti preferiti.
	 * @return lista di contatti
	 */
	public List<Contatto> getContattiPreferiti() {
		return contattiPreferiti;
	}

	/**
	 * Imposta la lista dei contatti preferiti.
	 * @param contattiPreferiti
	 */
	public void setContattiPreferiti(final List<Contatto> contattiPreferiti) {
		this.contattiPreferiti = contattiPreferiti;
	}

	/**
	 * Restituisce la lista dei destinatari.
	 * @return destinatari
	 */
	public List<DestinatarioRedDTO> getDestinatari() {
		return destinatari;
	}

	/**
	 * Imposta la lista dei destinatari.
	 * @param destinatari
	 */
	public void setDestinatari(final List<DestinatarioRedDTO> destinatari) {
		this.destinatari = destinatari;
	}

	/**
	 * Restituisce la categoria del documento.
	 * @return categoria documento
	 */
	public CategoriaDocumentoEnum getCategoria() {
		return categoria;
	}

	/**
	 * Imposta la categoria del documento.
	 * @param categoria
	 */
	public void setCategoria(final CategoriaDocumentoEnum categoria) {
		this.categoria = categoria;
	}

	/**
	 * Restituisce la modalita della maschera.
	 * @return modalita maschera
	 */
	public String getModalitaMaschera() {
		return modalitaMaschera;
	}

	/**
	 * Imposta la modalita della maschera.
	 * @param modalitaMaschera
	 */
	public void setModalitaMaschera(final String modalitaMaschera) {
		this.modalitaMaschera = modalitaMaschera;
	}

	/**
	 * Restituisce la lista dei tipi di destinatario.
	 * @return lista di TipologiaDestinatariEnum
	 */
	public List<TipologiaDestinatarioEnum> getComboTipologiaDestinatari() {
		return comboTipologiaDestinatari;
	}

	/**
	 * Imposta la lista dei tipi di destinatari.
	 * @param comboTipologiaDestinatari
	 */
	public void setComboTipologiaDestinatari(final List<TipologiaDestinatarioEnum> comboTipologiaDestinatari) {
		this.comboTipologiaDestinatari = comboTipologiaDestinatari;
	}

	/**
	 * Restituisce la lista delle modalita dei destinatari per il popolamento della combobox.
	 * @return lista di ModalitaDestinatarioEnum
	 */
	public List<ModalitaDestinatarioEnum> getComboModalitaDestinatario() {
		return comboModalitaDestinatario;
	}

	/**
	 * Imposta la lista delle modalita dei destinatari per il popolamento della combobox.
	 * @param comboModalitaDestinatario
	 */
	public void setComboModalitaDestinatario(final List<ModalitaDestinatarioEnum> comboModalitaDestinatario) {
		this.comboModalitaDestinatario = comboModalitaDestinatario;
	}

	/**
	 * Restituisce la lista dei contatti preferiti filtrati.
	 * @return lista di contatti
	 */
	public List<Contatto> getContattiPreferitiFiltered() {
		return contattiPreferitiFiltered;
	}

	/**
	 * Imposta la lista dei contatti preferiti filtrati.
	 * @param contattiPreferitiFiltered
	 */
	public void setContattiPreferitiFiltered(final List<Contatto> contattiPreferitiFiltered) {
		this.contattiPreferitiFiltered = contattiPreferitiFiltered;
	}

	/**
	 * Restituisce il destinatario visualizzato.
	 * @return destinatario visualizzato
	 */
	public DestinatarioRedDTO getDestinatarioVisualizzato() {
		return destinatarioVisualizzato;
	}

	/**
	 * Imposta il destinatario visualizzato.
	 * @param destinatarioVisualizzato
	 */
	public void setDestinatarioVisualizzato(final DestinatarioRedDTO destinatarioVisualizzato) {
		this.destinatarioVisualizzato = destinatarioVisualizzato;
	}

	/**
	 * Restituisce il contatto da visualizzare.
	 * @return contatto
	 */
	public Contatto getContattoDaVisualizzare() {
		return contattoDaVisualizzare;
	}

	/**
	 * Imposta il contatto da visualizzare.
	 * @param contattoDaVisualizzare
	 */
	public void setContattoDaVisualizzare(final Contatto contattoDaVisualizzare) {
		this.contattoDaVisualizzare = contattoDaVisualizzare;
	}

	/**
	 * Restituisce la lista dei mezzi di spedizione.
	 * @return lista MezzoSpedizioneEnum
	 */
	public List<MezzoSpedizioneEnum> getComboMezziSpedizione() {
		return comboMezziSpedizione;
	}

	/**
	 * Imposta la lista dei mezzi di spedizione.
	 * @param comboMezziSpedizione
	 */
	public void setComboMezziSpedizione(final List<MezzoSpedizioneEnum> comboMezziSpedizione) {
		this.comboMezziSpedizione = comboMezziSpedizione;
	}

	/**
	 * Restituisce true se il tracciamento è visibile, false altrimenti.
	 * @return true se il tracciamento è visibile, false altrimenti
	 */
	public boolean isTracciamentoVisible() {
		return isTracciamentoVisible;
	}

	/**
	 * Imposta il flag: isTracciamentoVisible.
	 * @param isTracciamentoVisible
	 */
	public void setTracciamentoVisible(final boolean isTracciamentoVisible) {
		this.isTracciamentoVisible = isTracciamentoVisible;
	}

	/**
	 * Restituisce true se l'integrazione dati è visibile, false altrimenti.
	 * @return true se l'integrazione dati è visibile, false altrimenti
	 */
	public boolean isIntegrazioneDatiVisible() {
		return isIntegrazioneDatiVisible;
	}

	/**
	 * Imposta il flag: isIntegrazioneDatiVisible.
	 * @param isIntegrazioneDatiVisible
	 */
	public void setIntegrazioneDatiVisible(final boolean isIntegrazioneDatiVisible) {
		this.isIntegrazioneDatiVisible = isIntegrazioneDatiVisible;
	}

	/**
	 * Restituisce la lista per la popolazione della combobox associata alla riservatezza.
	 * @return lista di RiservatezzaEnum
	 */
	public List<RiservatezzaEnum> getComboRiservatezza() {
		return comboRiservatezza;
	}

	/**
	 * Imposta la lista per la popolazione della combobox associata alla riservatezza.
	 * @param comboRiservatezza
	 */
	public void setComboRiservatezza(final List<RiservatezzaEnum> comboRiservatezza) {
		this.comboRiservatezza = comboRiservatezza;
	}

	/**
	 * Restituisce la lista di IterApprovativoDTO per il popolamento della combobox.
	 * @return lista di IterApprovativoDTO
	 */
	public List<IterApprovativoDTO> getComboIterApprovativi() {
		return comboIterApprovativi;
	}

	/**
	 * Imposta la lista per la popolazione della combobox degli Iter Approvativi.
	 * @param comboIterApprovativi
	 */
	public void setComboIterApprovativi(final List<IterApprovativoDTO> comboIterApprovativi) {
		this.comboIterApprovativi = comboIterApprovativi;
	}

	/**
	 * Restituisce la lista dei TipoAssegnazioneEnum.
	 * @return Lista di TipoAssegnazioneEnum
	 */
	public List<TipoAssegnazioneEnum> getComboTipoAssegnazione() {
		return comboTipoAssegnazione;
	}

	/**
	 * Imposta la lista per la popolazione della combobox associata al tipo assegnazione.
	 * @param comboTipoAssegnazione
	 */
	public void setComboTipoAssegnazione(final List<TipoAssegnazioneEnum> comboTipoAssegnazione) {
		this.comboTipoAssegnazione = comboTipoAssegnazione;
	}

	/**
	 * Restituisce true se la combo per il tipo assegnazione è visibile, false altrimenti.
	 * @return true se la combo per il tipo assegnazione è visibile, false altrimenti
	 */
	public boolean isComboTipoAssegnazioneVisible() {
		return comboTipoAssegnazioneVisible;
	}

	/**
	 * Imposta la visibilita della combo sui tipi assegnazione.
	 * @param comboTipoAssegnazioneVisible
	 */
	public void setComboTipoAssegnazioneVisible(final boolean comboTipoAssegnazioneVisible) {
		this.comboTipoAssegnazioneVisible = comboTipoAssegnazioneVisible;
	}

	/**
	 * Restituisce true se l'organigramma degli assegnatari è visibile, false altrimenti.
	 * @return true se l'organigramma degli assegnatari è visibile, false altrimenti
	 */
	public boolean isAssegnatarioOrganigrammaVisible() {
		return assegnatarioOrganigrammaVisible;
	}

	/**
	 * Imposta il flag associato alla visibilita dell'organigramma degli assegnatari.
	 * @param assegnatarioVisible
	 */
	public void setAssegnatarioOrganigrammaVisible(final boolean assegnatarioVisible) {
		this.assegnatarioOrganigrammaVisible = assegnatarioVisible;
	}

	/**
	 * Restituisce true se la firma digitale RGS è visibile, false altrimenti.
	 * @return true se la firma digitale RGS è visibile, false altrimenti
	 */
	public boolean isFirmaDigitaleRGSVisible() {
		return firmaDigitaleRGSVisible;
	}

	/**
	 * Imposta il flag: firmaDigitaleRGSVisible.
	 * @param firmaDigitaleRGSVisible
	 */
	public void setFirmaDigitaleRGSVisible(final boolean firmaDigitaleRGSVisible) {
		this.firmaDigitaleRGSVisible = firmaDigitaleRGSVisible;
	}

	/**
	 * Restituisce true se la combobox del responsabile copia conforme è visibile, false altrimenti.
	 * @return true se la combobox del responsabile copia conforme è visibile, false altrimenti
	 */
	public boolean isComboResponsabileCopiaConformeVisible() {
		return comboResponsabileCopiaConformeVisible;
	}

	/**
	 * Imposta la visibilita della combobox del responsabile copia conforme.
	 * @param assegnatarioComboVisible
	 */
	public void setComboResponsabileCopiaConformeVisible(final boolean assegnatarioComboVisible) {
		this.comboResponsabileCopiaConformeVisible = assegnatarioComboVisible;
	}

	/**
	 * Restituisce la lista dei MomentoProtocollazioneEnum per il popolamento della combobox.
	 * @return lista di MomentoProtocollazioneEnum
	 */
	public List<MomentoProtocollazioneEnum> getComboMomentoProtocollazione() {
		return comboMomentoProtocollazione;
	}

	/**
	 * Imposta la lista dei MomentoProtocollazioneEnum per il popolamento della combobox.
	 * @param comboMomentoProtocollazione
	 */
	public void setComboMomentoProtocollazione(final List<MomentoProtocollazioneEnum> comboMomentoProtocollazione) {
		this.comboMomentoProtocollazione = comboMomentoProtocollazione;
	}

	/**
	 * Restituisce true se la combobox dei momenti protocollazione è disabilitata, false altrimenti.
	 * @return true se la combobox dei momenti protocollazione è disabilitata, false altrimenti
	 */
	public boolean isComboMomentoProtocollazioneDisabled() {
		return comboMomentoProtocollazioneDisabled;
	}

	/**
	 * Imposta la lista dei momenti protocollazione per la combobox.
	 * @param momentoProtocollazioneDisabled
	 */
	public void setComboMomentoProtocollazioneDisabled(final boolean momentoProtocollazioneDisabled) {
		this.comboMomentoProtocollazioneDisabled = momentoProtocollazioneDisabled;
	}

	/**
	 * Restituisce true se "da non protocollare" è visibile, false altrimenti.
	 * @return true se "da non protocollare" è visibile, false altrimenti
	 */
	public boolean isDaNonProtocollareVisible() {
		return daNonProtocollareVisible;
	}

	/**
	 * Imposta il flag: daNonProtocollareVisible.
	 * @param daNonProtocollareVisible
	 */
	public void setDaNonProtocollareVisible(final boolean daNonProtocollareVisible) {
		this.daNonProtocollareVisible = daNonProtocollareVisible;
	}

	/**
	 * Restituisce true se il registro riservato è visibile, false altrimenti.
	 * @return true se il registro riservato è visibile, false altrimenti
	 */
	public boolean isRegistroRiservatoVisible() {
		return registroRiservatoVisible;
	}

	/**
	 * Imposta il flag: registroRiservatoVisible.
	 * @param registroRiservatoVisible
	 */
	public void setRegistroRiservatoVisible(final boolean registroRiservatoVisible) {
		this.registroRiservatoVisible = registroRiservatoVisible;
	}

	/**
	 * Restituisce true se l'iter doc uscita è visibile, false altrimenti.
	 * @return iterDocUscitaVisible
	 */
	public boolean isIterDocUscitaVisible() {
		return iterDocUscitaVisible;
	}

	/**
	 * Imposta la visibilita dell'iter documento uscita.
	 * @param iterDocUscitaVisible
	 */
	public void setIterDocUscitaVisible(final boolean iterDocUscitaVisible) {
		this.iterDocUscitaVisible = iterDocUscitaVisible;
	}

	/**
	 * Restituisce true se la combo coordinatore è visibile.
	 * @return true se la combo coordinatore è visibile, false altrimenti
	 */
	public boolean isComboCoordinatoriVisible() {
		return comboCoordinatoriVisible;
	}

	/**
	 * Imposta la visibilita della combobox dei coordinatori.
	 * @param comboCoordinatoriVisible
	 */
	public void setComboCoordinatoriVisible(final boolean comboCoordinatoriVisible) {
		this.comboCoordinatoriVisible = comboCoordinatoriVisible;
	}

	/**
	 * Restituisce la lista dei coordinatori per il popolamento della combobox.
	 * @return lista NodoUtenteCoordinatore
	 */
	public List<NodoUtenteCoordinatore> getComboCoordinatori() {
		return comboCoordinatori;
	}

	/**
	 * Imposta la lista dei coordinatori per il popolamento della combobox.
	 * @param comboCoordinatori
	 */
	public void setComboCoordinatori(final List<NodoUtenteCoordinatore> comboCoordinatori) {
		this.comboCoordinatori = comboCoordinatori;
	}

	/**
	 * Restituisce flagCorteContiVisible.
	 * @return flagCorteContiVisible
	 */
	public boolean isFlagCorteContiVisible() {
		return flagCorteContiVisible;
	}

	/**
	 * Imposta il flag: flagCorteContiVisible.
	 * @param flagCorteContiVisible
	 */
	public void setFlagCorteContiVisible(final boolean flagCorteContiVisible) {
		this.flagCorteContiVisible = flagCorteContiVisible;
	}

	/**
	 * Restituisce la lista per il popolamento della combobox dei responsabili copia conforme.
	 * @return lista di ResponsabileCopiaConformeDTO
	 */
	public List<ResponsabileCopiaConformeDTO> getComboResponsabileCopiaConforme() {
		return comboResponsabileCopiaConforme;
	}

	/**
	 * Imposta la lista per il popolamento della combobox dei responsabili copia conforme.
	 * @param comboResponsabileCopiaConforme
	 */
	public void setComboResponsabileCopiaConforme(final List<ResponsabileCopiaConformeDTO> comboResponsabileCopiaConforme) {
		this.comboResponsabileCopiaConforme = comboResponsabileCopiaConforme;
	}

	/**
	 * Restituisce l'id del ragioniere.
	 * @return id ragioniere
	 */
	public Long getIdRagioniere() {
		return idRagioniere;
	}

	/**
	 * Imposta l'id del ragioniere.
	 * @param idRagioniere
	 */
	public void setIdRagioniere(final Long idRagioniere) {
		this.idRagioniere = idRagioniere;
	}

	/**
	 * @return maxAnno
	 */
	public int getMaxAnno() {
		return maxAnno;
	}

	/**
	 * @param maxAnno
	 */
	public void setMaxAnno(final int maxAnno) {
		this.maxAnno = maxAnno;
	}

	/**
	 * Restituisce true se il panel barcode è visibile, false altrimenti.
	 * @return true se il panel barcode è visibile, false altrimenti
	 */
	public boolean isBarcodePanelVisible() {
		return barcodePanelVisible;
	}

	/**
	 * Imposta la visibilita del panel barcode.
	 * @param barcodePanelVisible
	 */
	public void setBarcodePanelVisible(final boolean barcodePanelVisible) {
		this.barcodePanelVisible = barcodePanelVisible;
	}

	/**
	 * Restituisce true se il numero RDP è in sola lettura, false altrimenti.
	 * @return true se il numero RDP è in sola lettura, false altrimenti
	 */
	public boolean isNumeroRdpReadOnly() {
		return numeroRdpReadOnly;
	}

	/**
	 * Imposta numeroRdpReadOnly.
	 * @param numeroRdpReadOnly
	 */
	public void setNumeroRdpReadOnly(final boolean numeroRdpReadOnly) {
		this.numeroRdpReadOnly = numeroRdpReadOnly;
	}

	/**
	 * Restituisce true se il panel Rdp è visibile.
	 * @return true se panel rdp visibile, false altrimenti.
	 */
	public boolean isRdpPanelVisible() {
		return rdpPanelVisible;
	}

	/**
	 * Imposta la visibilita dell'rdp panel.
	 * @param rdpPanelVisible
	 */
	public void setRdpPanelVisible(final boolean rdpPanelVisible) {
		this.rdpPanelVisible = rdpPanelVisible;
	}

	/**
	 * Restituisce true se il pannello del doc principale è visibile, false altrimenti.
	 * @return true se il pannello del doc principale è visibile, false altrimenti
	 */
	public boolean isDocPrincipalePanelVisible() {
		return docPrincipalePanelVisible;
	}

	/**
	 * Imposta il flag: docPrincipalePanelVisible.
	 * @param docPrincipalePanelVisible
	 */
	public void setDocPrincipalePanelVisible(final boolean docPrincipalePanelVisible) {
		this.docPrincipalePanelVisible = docPrincipalePanelVisible;
	}

	/**
	 * Restituisce true se in modifica, false altrimenti.
	 * @return true se in modifica, false altrimenti
	 */
	public boolean isInModifica() {
		return inModifica;
	}

	/**
	 * Imposta il flag: inModifica.
	 * @param inModifica
	 */
	public void setInModifica(final boolean inModifica) {
		this.inModifica = inModifica;
	}

	/**
	 * Restituisce la lista dei TemplateDTO.
	 * @return templateFileList
	 */
	public List<TemplateDTO> getTemplateFileList() {
		return templateFileList;
	}

	/**
	 * Imposta templateFileList.
	 * @param templateFileList
	 */
	public void setTemplateFileList(final List<TemplateDTO> templateFileList) {
		this.templateFileList = templateFileList;
	}

	/**
	 * Restituisce true se il template file è visibile, false altrimenti.
	 * @return templateFileListVisible
	 */
	public boolean isTemplateFileListVisible() {
		return templateFileListVisible;
	}

	/**
	 * Imposta la visibilita del template file.
	 * @param templateFileListVisible
	 */
	public void setTemplateFileListVisible(final boolean templateFileListVisible) {
		this.templateFileListVisible = templateFileListVisible;
	}

	/**
	 * Restituisce verificaFirmaDocPrincipaleVisible.
	 * @return verificaFirmaDocPrincipaleVisible
	 */
	public boolean isVerificaFirmaDocPrincipaleVisible() {
		return verificaFirmaDocPrincipaleVisible;
	}

	/**
	 * Imposta il flag: verificaFirmaDocPrincipaleVisible.
	 * @param verificaFirmaDocPrincipaleVisible
	 */
	public void setVerificaFirmaDocPrincipaleVisible(final boolean verificaFirmaDocPrincipaleVisible) {
		this.verificaFirmaDocPrincipaleVisible = verificaFirmaDocPrincipaleVisible;
	}

	/**
	 * Restituisce true se il contributo numero anno esterno è visibile, false altrimenti.
	 * @return contributoNumAnnoEsternoVisible
	 */
	public boolean isContributoNumAnnoEsternoVisible() {
		return contributoNumAnnoEsternoVisible;
	}

	/**
	 * Imposta la visibilità del contributo Num Anno Esterno.
	 * @param contributoNumAnnoEsternoVisible
	 */
	public void setContributoNumAnnoEsternoVisible(final boolean contributoNumAnnoEsternoVisible) {
		this.contributoNumAnnoEsternoVisible = contributoNumAnnoEsternoVisible;
	}

	/**
	 * Restituisce true se check firma con copia conforme è visibile, false altrimenti.
	 * @return checkFirmaConCopiaConformeVisible
	 */
	public boolean isCheckFirmaConCopiaConformeVisible() {
		return checkFirmaConCopiaConformeVisible;
	}

	/**
	 * Imposta checkFirmaConCopiaConformeVisible.
	 * @param checkFirmaConCopiaConformeVisible
	 */
	public void setCheckFirmaConCopiaConformeVisible(final boolean checkFirmaConCopiaConformeVisible) {
		this.checkFirmaConCopiaConformeVisible = checkFirmaConCopiaConformeVisible;
	}

	/**
	 * Restituisce true se l'indice di classificazione è visibile, false altrimenti.
	 * @return true se l'indice di classificazione è visibile, false altrimenti
	 */
	public boolean isIndiceDiClassificazioneVisible() {
		return indiceDiClassificazioneVisible;
	}

	/**
	 * Imposta la visibilita dell'indice di classificazione.
	 * @param indiceDiClassificazioneVisible
	 */
	public void setIndiceDiClassificazioneVisible(final boolean indiceDiClassificazioneVisible) {
		this.indiceDiClassificazioneVisible = indiceDiClassificazioneVisible;
	}

	/**
	 * Restituisce true se l'assegnatario per competenza è visibile, false altrimenti.
	 * @return true se l'assegnatario per competenza è visibile, false altrimenti
	 */
	public boolean isAssegnatarioPerCompetenzaVisible() {
		return assegnatarioPerCompetenzaVisible;
	}

	/**
	 * Imposta la visibilita dell'assegnatario per competenza.
	 * @param assegnatarioPerCompetenzaVisible
	 */
	public void setAssegnatarioPerCompetenzaVisible(final boolean assegnatarioPerCompetenzaVisible) {
		this.assegnatarioPerCompetenzaVisible = assegnatarioPerCompetenzaVisible;
	}

	/**
	 * Restituisce la lista dei titolari root.
	 * @return list di TitolarioDTO
	 */
	public List<TitolarioDTO> getTitolarioRootList() {
		return titolarioRootList;
	}

	/**
	 * Imposta la lista dei titolari root.
	 * @param titolarioRootList
	 */
	public void setTitolarioRootList(final List<TitolarioDTO> titolarioRootList) {
		this.titolarioRootList = titolarioRootList;
	}

	/**
	 * Restituisce true se il protocollo mittente risposta è visibile, false altrimenti.
	 * @return true se il protocollo mittente risposta è visibile, false altrimenti
	 */
	public boolean isProtocolloMittenteRispostaVisible() {
		return protocolloMittenteRispostaVisible;
	}

	/**
	 * Imposta la visibilita del protocollo mittente risposta.
	 * @param protocolloMittenteRispostaVisible
	 */
	public void setProtocolloMittenteRispostaVisible(final boolean protocolloMittenteRispostaVisible) {
		this.protocolloMittenteRispostaVisible = protocolloMittenteRispostaVisible;
	}

	/**
	 * Restituisce true se il protocollo risposta è verificato, false altrimenti.
	 * @return true se il protocollo risposta è verificato, false altrimenti
	 */
	public boolean isProtocolloRispostaVerificato() {
		return protocolloRispostaVerificato;
	}

	/**
	 * Imposta il flag protocolloRispostaVerificato.
	 * @param protocolloRispostaVerificato
	 */
	public void setProtocolloRispostaVerificato(final boolean protocolloRispostaVerificato) {
		this.protocolloRispostaVerificato = protocolloRispostaVerificato;
	}

	/**
	 * Restituisce true se raccoltaFad è visibile, false altrimenti.
	 * @return true se raccoltaFad è visibile, false altrimenti
	 */
	public boolean isRaccoltaFadVisible() {
		return raccoltaFadVisible;
	}

	/**
	 * Imposta la visibilita di raccoltaFad.
	 * @param raccoltaFadVisible
	 */
	public void setRaccoltaFadVisible(final boolean raccoltaFadVisible) {
		this.raccoltaFadVisible = raccoltaFadVisible;
	}

	/**
	 * Restituisce la lista di FadAmministrazioneDTO per la popolazione della combobox.
	 * @return list di FadAmministrazioneDTO
	 */
	public List<FadAmministrazioneDTO> getComboFadAmministrazioni() {
		return comboFadAmministrazioni;
	}

	/**
	 * Imposta la lista di FadAmministrazioneDTO per la combobox.
	 * @param comboFadAmministrazioni
	 */
	public void setComboFadAmministrazioni(final List<FadAmministrazioneDTO> comboFadAmministrazioni) {
		this.comboFadAmministrazioni = comboFadAmministrazioni;
	}

	/**
	 * Restituisce la lista dei formati allegato per la combobox.
	 * @return list di FormatoAllegatoDTO
	 */
	public List<FormatoAllegatoDTO> getComboFormatiAllegato() {
		return comboFormatiAllegato;
	}

	/**
	 * Imposta la lista dei formati allegato.
	 * @param comboFormatiAllegato
	 */
	public void setComboFormatiAllegato(final List<FormatoAllegatoDTO> comboFormatiAllegato) {
		this.comboFormatiAllegato = comboFormatiAllegato;
	}

	/**
	 * Restituisce true se allegati responsabili copia conforme è visibile, false altrimenti.
	 * @return true se allegati responsabili copia conforme è visibile, false altrimenti
	 */
	public boolean isAllegatiResponsabiliCopiaConformeVisible() {
		return allegatiResponsabiliCopiaConformeVisible;
	}

	/**
	 * Imposta il flag: allegatiResponsabiliCopiaConformeVisible.
	 * @param allegatiResponsabiliCopiaConformeVisible
	 */
	public void setAllegatiResponsabiliCopiaConformeVisible(final boolean allegatiResponsabiliCopiaConformeVisible) {
		this.allegatiResponsabiliCopiaConformeVisible = allegatiResponsabiliCopiaConformeVisible;
	}

	/**
	 * Restituisce true se "allegati check da firmare" è visibile, false altrimenti.
	 * @return visibilita allegatiCheckDaFirmare
	 */
	public boolean isAllegatiCheckDaFirmareVisible() {
		return allegatiCheckDaFirmareVisible;
	}

	/**
	 * Imposta la visibilità di allegatiCheckDaFirmareVisible.
	 * @param allegatiCheckDaFirmareVisible
	 */
	public void setAllegatiCheckDaFirmareVisible(final boolean allegatiCheckDaFirmareVisible) {
		this.allegatiCheckDaFirmareVisible = allegatiCheckDaFirmareVisible;
	}

	/**
	 * Restituisce la lista di CasellaPostaDTO per la popolazione della combobox delle caselle spedizione.
	 * @return lista di CasellaPostaDTO
	 */
	public List<CasellaPostaDTO> getComboCaselleSpedizione() {
		return comboCaselleSpedizione;
	}

	/**
	 * Imposta la lista di caselle spedizione per la combobox associata.
	 * @param comboCaselleSpedizione
	 */
	public void setComboCaselleSpedizione(final List<CasellaPostaDTO> comboCaselleSpedizione) {
		this.comboCaselleSpedizione = comboCaselleSpedizione;
	}

	/**
	 * Restituisce la lista di TestoPredefinitoDTO per la popolazione della combobox.
	 * @return comboTestiPredefiniti
	 */
	public List<TestoPredefinitoDTO> getComboTestiPredefiniti() {
		return comboTestiPredefiniti;
	}

	/**
	 * Imposta la lista TestoPredifinitoDTO per la combobox.
	 * @param comboTestiPredefiniti
	 */
	public void setComboTestiPredefiniti(final List<TestoPredefinitoDTO> comboTestiPredefiniti) {
		this.comboTestiPredefiniti = comboTestiPredefiniti;
	}

	/**
	 * Restituisce il prefisso per l'oggetto della mail.
	 * @return prefisso oggetto mail
	 */
	public String getPrefixOggettoMail() {
		return prefixOggettoMail;
	}

	/**
	 * Imposta il prefisso dell'oggetto della mail.
	 * @param prefixOggettoMail
	 */
	public void setPrefixOggettoMail(final String prefixOggettoMail) {
		this.prefixOggettoMail = prefixOggettoMail;
	}

	/**
	 * Restituisce la lista degli allegati mail.
	 * @return allegati mail
	 */
	public List<DetailDocumentRedDTO> getAllegatiMail() {
		return allegatiMail;
	}

	/**
	 * Imposta la lista degli allegati mail.
	 * @param allegatiMail
	 */
	public void setAllegatiMail(final List<DetailDocumentRedDTO> allegatiMail) {
		this.allegatiMail = allegatiMail;
	}

	/**
	 * Restituisce true se il tab spedizione è disabilitato, altrimenti restituisce false.
	 * @return true se il tab spedizione è disabilitato, false altrimenti.
	 */
	public boolean isTabSpedizioneDisabled() {
		return tabSpedizioneDisabled;
	}

	/**
	 * Imposta il flag: tabSpedizioneDisabled.
	 * @param tabSpedizioneDisabled
	 */
	public void setTabSpedizioneDisabled(final boolean tabSpedizioneDisabled) {
		this.tabSpedizioneDisabled = tabSpedizioneDisabled;
	}

	/**
	 * Restituisce la lista di tipi di ricerca generica per la popolazione della combobox.
	 * @return lista di tipi di ricerca generica
	 */
	public List<RicercaGenericaTypeEnum> getComboRicercaFascicoloModalita() {
		return comboRicercaFascicoloModalita;
	}

	/**
	 * Imposta la lista dei tipi di ricerca fascicolo per la popolazione della combobox.
	 * @param comboRicercaFascicoloModalita
	 */
	public void setComboRicercaFascicoloModalita(final List<RicercaGenericaTypeEnum> comboRicercaFascicoloModalita) {
		this.comboRicercaFascicoloModalita = comboRicercaFascicoloModalita;
	}

	/**
	 * Restituisce true se il tabSpedizione è visibile, false altrimenti.
	 * @return true se il tabSpedizione è visibile, false altrimenti
	 */
	public boolean isTabSpedizioneVisible() {
		return tabSpedizioneVisible;
	}

	/**
	 * Imposta la visibilita del tab di spedizione.
	 * @param tabSpedizioneVisible
	 */
	public void setTabSpedizioneVisible(final boolean tabSpedizioneVisible) {
		this.tabSpedizioneVisible = tabSpedizioneVisible;
	}

	/**
	 * Restituisce il messaggio di conferma per la dialog.
	 * @return messaggio conferma
	 */
	public String getDialogConfirmMessage() {
		return dialogConfirmMessage;
	}

	/**
	 * Imposta il messaggio per la dialog di conferma.
	 * @param dialogConfirmMessage
	 */
	public void setDialogConfirmMessage(final String dialogConfirmMessage) {
		this.dialogConfirmMessage = dialogConfirmMessage;
	}

	/**
	 * Restituisce true se l'indice di classificazione è richiesto, false altrimenti.
	 * @return true se l'indice di classificazione è richiesto, false altrimenti
	 */
	public boolean isIndiceDiClassificazioneRequired() {
		return indiceDiClassificazioneRequired;
	}

	/**
	 * Imposta la necessita dell'indice di classificazione.
	 * @param indiceDiClassificazioneRequired
	 */
	public void setIndiceDiClassificazioneRequired(final boolean indiceDiClassificazioneRequired) {
		this.indiceDiClassificazioneRequired = indiceDiClassificazioneRequired;
	}

	/**
	 * Restituisce true se il button dell'assegnazione è disabilitato, false altrimenti.
	 * @return true se il button assegnazione è disabilitato, false altrimenti
	 */
	public boolean isAssegnazioneBtnDisabled() {
		return assegnazioneBtnDisabled;
	}

	/**
	 * Imposta il flag associato all'abilitazione del button per l'assegnazione.
	 * @param assegnazioneBtnDisabled
	 */
	public void setAssegnazioneBtnDisabled(final boolean assegnazioneBtnDisabled) {
		this.assegnazioneBtnDisabled = assegnazioneBtnDisabled;
	}

	/**
	 * Restituisce true se il tab delle assegnazioni è disabilitato, false altrimenti.
	 * @return tabAssegnazioniDisabled
	 */
	public boolean isTabAssegnazioniDisabled() {
		return tabAssegnazioniDisabled;
	}

	/**
	 * Imposta il flag associato all'abilitazione del tab assegnazioni.
	 * @param tabAssegnazioniDisabled
	 */
	public void setTabAssegnazioniDisabled(final boolean tabAssegnazioniDisabled) {
		this.tabAssegnazioniDisabled = tabAssegnazioniDisabled;
	}

	/**
	 * Restituisce true se in modifica ingresso, false altrimenti.
	 * @return true se in modifica ingresso, false altrimenti
	 */
	public boolean isInModificaIngresso() {
		return isInModificaIngresso;
	}

	/**
	 * Imposta il flag: isInModificaIngresso.
	 * @param isInModificaIngresso
	 */
	public void setInModificaIngresso(final boolean isInModificaIngresso) {
		this.isInModificaIngresso = isInModificaIngresso;
	}

	/**
	 * Restituisce true se in modifica uscita, false altrimenti.
	 * @return isInModificaUscita
	 */
	public boolean isInModificaUscita() {
		return isInModificaUscita;
	}

	/**
	 * @param isInModificaUscita
	 */
	public void setInModificaUscita(final boolean isInModificaUscita) {
		this.isInModificaUscita = isInModificaUscita;
	}

	/**
	 * Restituisce true se in creazione ingresso, false altrimenti.
	 * @return isInCreazioneIngresso
	 */
	public boolean isInCreazioneIngresso() {
		return isInCreazioneIngresso;
	}

	/**
	 * @param isInCreazioneIngresso
	 */
	public void setInCreazioneIngresso(final boolean isInCreazioneIngresso) {
		this.isInCreazioneIngresso = isInCreazioneIngresso;
	}

	/**
	 * Restituisce true se in creazione uscita, false altrimenti.
	 * @return isInCreazioneUscita
	 */
	public boolean isInCreazioneUscita() {
		return isInCreazioneUscita;
	}

	/**
	 * @param isInCreazioneUscita
	 */
	public void setInCreazioneUscita(final boolean isInCreazioneUscita) {
		this.isInCreazioneUscita = isInCreazioneUscita;
	}

	/**
	 * Restituisce true se riservato, false altrimenti.
	 * @return riservato
	 */
	public boolean isRiservato() {
		return riservato;
	}

	/**
	 * @param riservato
	 */
	public void setRiservato(final boolean riservato) {
		this.riservato = riservato;
	}

	/**
	 * @return ultimaLegislatura
	 */
	public Legislatura getUltimaLegislatura() {
		return ultimaLegislatura;
	}

	/**
	 * @param ultimaLegislatura
	 */
	public void setUltimaLegislatura(final Legislatura ultimaLegislatura) {
		this.ultimaLegislatura = ultimaLegislatura;
	}

	/**
	 * @return flagProtocollaMail
	 */
	public boolean isFlagProtocollaMail() {
		return flagProtocollaMail;
	}

	/**
	 * @param flagProtocollaMail
	 */
	public void setFlagProtocollaMail(final boolean flagProtocollaMail) {
		this.flagProtocollaMail = flagProtocollaMail;
	}

	/**
	 * Restituisce la lista delle estensioni ammesse.
	 * @return lista di estensioni
	 */
	public List<String> getEstensioniAmmesse() {
		return estensioniAmmesse;
	}

	/**
	 * Imposta la lista delle estensioni ammesse.
	 * @param estensioniAmmesse
	 */
	public void setEstensioniAmmesse(final List<String> estensioniAmmesse) {
		this.estensioniAmmesse = estensioniAmmesse;
	}

	/**
	 * Restituisce la lista di tutte le estensioni.
	 * @return lista di estensioni
	 */
	public List<String> getEstensioniAll() {
		return estensioniAll;
	}

	/**
	 * Imposta la lista delle estensioni.
	 * @param estensioniAll
	 */
	public void setEstensioniAll(final List<String> estensioniAll) {
		this.estensioniAll = estensioniAll;
	}

	/**
	 * Restituisce true se la tipologia documento è disabilitata, false altrimenti.
	 * @return true se la tipologia documento è disabilitata, false altrimenti
	 */
	public boolean isTipologiaDocumentoDisable() {
		return tipologiaDocumentoDisable;
	}

	/**
	 * Imposta il flag associato all'abilitazione della tipologia documento.
	 * @param tipologiaDocumentoDisable
	 */
	public void setTipologiaDocumentoDisable(final boolean tipologiaDocumentoDisable) {
		this.tipologiaDocumentoDisable = tipologiaDocumentoDisable;
	}

	/**
	 * Restituisce la data odierna come java.util.Date.
	 * @return data odierna
	 */
	public Date getToday() {
		return new Date();
	}

	/**
	 * Resituisce true se gli allegati sono modificabili, false altrimenti.
	 * @return true se gli allegati sono modificabili, false altrimenti
	 */
	public boolean isAllegatiModificabili() {
		return allegatiModificabili;
	}

	/**
	 * Imposta la modificabilita degli allegati.
	 * @param allegatiModificabili
	 */
	public void setAllegatiModificabili(final boolean allegatiModificabili) {
		this.allegatiModificabili = allegatiModificabili;
	}

	/**
	 * Restituisce true se la funzionalita "crea raccolta Fad" è disabilitata, altrimenti restituisce false.
	 * @return isCreaRaccoltaFadDisabled
	 */
	public boolean isCreaRaccoltaFadDisabled() {
		return isCreaRaccoltaFadDisabled;
	}

	/**
	 * @param isCreaRaccoltaFadDisabled
	 */
	public void setCreaRaccoltaFadDisabled(final boolean isCreaRaccoltaFadDisabled) {
		this.isCreaRaccoltaFadDisabled = isCreaRaccoltaFadDisabled;
	}

	/**
	 * Restituisce la lista di Caselle spedizione.
	 * @return lista di caselle postali
	 */
	public List<CasellaPostaDTO> getCaselleSpedizioneByUser() {
		return caselleSpedizioneByUser;
	}

	/**
	 * Imposta la lista delle caselle spedizione.
	 * @param caselleSpedizioneByUser
	 */
	public void setCaselleSpedizioneByUser(final List<CasellaPostaDTO> caselleSpedizioneByUser) {
		this.caselleSpedizioneByUser = caselleSpedizioneByUser;
	}

	/**
	 * Restituisce true se il tab "contributi esterni" è visibile, altrimenti restituisce false.
	 * @return true se il tab "contributi esterni" è visibile, altrimenti restituisce false
	 */
	public boolean isTabContributiEsterniVisible() {
		return tabContributiEsterniVisible;
	}

	/**
	 * Imposta la visibilita del tab "contributi esterni".
	 * @param tabContributiEsterniVisible
	 */
	public void setTabContributiEsterniVisible(final boolean tabContributiEsterniVisible) {
		this.tabContributiEsterniVisible = tabContributiEsterniVisible;
	}

	/**
	 * @return rdsSiebel
	 */
	public boolean hasRdsSiebel() {
		return this.rdsSiebel;
	}

	/**
	 * @param hasRds
	 */
	public void setSiebel(final boolean hasRds) {
		this.rdsSiebel = hasRds;
	}

	/**
	 * Restituisce true se il template doc uscita è visibile, false altrimenti.
	 * @return true se il template doc uscita è visibile, false altrimenti
	 */
	public boolean isTemplateDocUscitaVisible() {
		return templateDocUscitaVisible;
	}

	/**
	 * Imposta la visibilita del template documento uscita.
	 * @param templateDocUscitaVisible
	 */
	public void setTemplateDocUscitaVisible(final boolean templateDocUscitaVisible) {
		this.templateDocUscitaVisible = templateDocUscitaVisible;
	}

	/**
	 * Restituisce true se generato dal template documento uscita, false altrimenti.
	 * @return true se generato dal template documento uscita, false altrimenti
	 */
	public boolean isGeneratedByTemplateDocUscita() {
		return generatedByTemplateDocUscita;
	}

	/**
	 * Imposta il flag associato alla creazione dal template documento uscita.
	 * @param generatedByTemplateDocUscita
	 */
	public void setGeneratedByTemplateDocUscita(final boolean generatedByTemplateDocUscita) {
		this.generatedByTemplateDocUscita = generatedByTemplateDocUscita;
	}

	/**
	 * Restituisce il contatto in fase di inserimento.
	 * @return contatto
	 */
	public Contatto getInserisciContattoItem() {
		return inserisciContattoItem;
	}

	/**
	 * Imposta il contatto in fase di inserimento.
	 * @param inserisciContattoItem
	 */
	public void setInserisciContattoItem(final Contatto inserisciContattoItem) {
		this.inserisciContattoItem = inserisciContattoItem;
	}

	/**
	 * Restitusice il DTO per la gestione della ricerca in rubrica.
	 * @return ricercaRubricaDTO
	 */
	public RicercaRubricaDTO getRicercaRubricaDTO() {
		return ricercaRubricaDTO;
	}

	/**
	 * Imposta il DTO per la gestione dei risultati della ricerca sulla rubrica.
	 * @param ricercaRubricaDTO
	 */
	public void setRicercaRubricaDTO(final RicercaRubricaDTO ricercaRubricaDTO) {
		this.ricercaRubricaDTO = ricercaRubricaDTO;
	}

	/**
	 * Restituisce la lista di contatti recuperata in fase di ricerca.
	 * @return risultati ricerca contatti
	 */
	public List<Contatto> getResultRicercaContatti() {
		return resultRicercaContatti;
	}

	/**
	 * Imposta la lista di contatti recuperata in fase di ricerca.
	 * @param resultRicercaContatti
	 */
	public void setResultRicercaContatti(final List<Contatto> resultRicercaContatti) {
		this.resultRicercaContatti = resultRicercaContatti;
	}

	/**
	 * Restituisce la lista filtrata dei risultati della ricerca dei contatti.
	 * @return risultati ricerca filtrati
	 */
	public List<Contatto> getResultRicercaContattiFiltered() {
		return resultRicercaContattiFiltered;
	}

	/**
	 * Imposta la lista filtrata dei contatti recuperati in fase di ricerca.
	 * @param resultRicercaContattiFiltered
	 */
	public void setResultRicercaContattiFiltered(final List<Contatto> resultRicercaContattiFiltered) {
		this.resultRicercaContattiFiltered = resultRicercaContattiFiltered;
	}

	/**
	 * Restituisce true se formato documento è disabilitato, false altrimenti.
	 * @return formatoDocumentoDisabled
	 */
	public boolean isFormatoDocumentoDisabled() {
		return formatoDocumentoDisabled;
	}

	/**
	 * Imposta il flag associato a formatoDocumentoDisabled.
	 * @param formatoDocumentoDisabled
	 */
	public void setFormatoDocumentoDisabled(final boolean formatoDocumentoDisabled) {
		this.formatoDocumentoDisabled = formatoDocumentoDisabled;
	}

	/**
	 * @return disabilitaButtonProtSemi
	 */
	public boolean getDisabilitaButtonProtSemi() {
		return disabilitaButtonProtSemi;
	}

	/**
	 * @param disabilitaButtonProtSemi
	 */
	public void setDisabilitaProtSemi(final boolean disabilitaButtonProtSemi) {
		this.disabilitaButtonProtSemi = disabilitaButtonProtSemi;
	}

	/**
	 * Restituisce la lista dei registri di repertorio configurati.
	 * @return registri di repertorio configurati
	 */
	public Map<Long, List<RegistroRepertorioDTO>> getRegistriRepertorioConfigured() {
		return registriRepertorioConfigured;
	}

	/**
	 * Imposta la lisa dei registri di repertorio configurati.
	 * @param registriRepertorioConfigured
	 */
	public void setRegistriRepertorioConfigured(final Map<Long, List<RegistroRepertorioDTO>> registriRepertorioConfigured) {
		this.registriRepertorioConfigured = registriRepertorioConfigured;
	}
	
	/**
	 * Restituisce true se i metadati estisi non sono editabili, false altrimenti.
	 * @return true se i metadati estisi non sono editabili, false altrimenti
	 */
	public boolean isDetailMetadatiEstesiNonEditabili() {
		return detailMetadatiEstesiNonEditabili;
	}

	/**
	 * Imposta l'editabilita dei metadati estesi.
	 * @param detailMetadatiEstesiNonEditabili
	 */
	public void setDetailMetadatiEstesiNonEditabili(final boolean detailMetadatiEstesiNonEditabili) {
		this.detailMetadatiEstesiNonEditabili = detailMetadatiEstesiNonEditabili;
	}

	/**
	 * Restituisce true se l'indice di classificazione E Fascicolo è disabilitato.
	 * @return indiceDiClassificazioneEFascicoloDisabled
	 */
	public boolean isIndiceDiClassificazioneEFascicoloDisabled() {
		return indiceDiClassificazioneEFascicoloDisabled;
	}

	/**
	 * @param indiceDiClassificazioneEFascicoloDisabled
	 */
	public void setIndiceDiClassificazioneEFascicoloDisabled(final boolean indiceDiClassificazioneEFascicoloDisabled) {
		this.indiceDiClassificazioneEFascicoloDisabled = indiceDiClassificazioneEFascicoloDisabled;
	}

	/**
	 * @return checkStampigliaturaSiglaVisible
	 */
	public boolean getCheckStampigliaturaSiglaVisible() {
		return checkStampigliaturaSiglaVisible;
	}

	/**
	 * @param checkStampigliaturaSiglaVisible
	 */
	public void setCheckStampigliaturaSiglaVisible(final boolean checkStampigliaturaSiglaVisible) {
		this.checkStampigliaturaSiglaVisible = checkStampigliaturaSiglaVisible;
	}

}