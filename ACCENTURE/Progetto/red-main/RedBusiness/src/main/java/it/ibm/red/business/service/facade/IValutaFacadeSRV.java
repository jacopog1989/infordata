package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.ValutaMetadatiDTO;

/**
 * Facade del servizio che gestisce le funzionalità legate ai metadati di tipo Valuta.
 * @author DarioVentimiglia
 */
public interface IValutaFacadeSRV extends Serializable {

	/**
	 * Costruisce la lista dei metadati valuta a partire da metadati generici.
	 * @param metadatiValuta come metadati
	 * @return lista dei metadati valuta
	 */
	List<ValutaMetadatiDTO> buildListValutaMetadati(List<MetadatoDTO> metadatiValuta);

	/**
	 * Restituisce la mappa dei cambi associati alle valute.
	 * @param dataCreazione
	 * @return mappa dei cambi associati alle valute
	 */
	Map<String, Double> getCambiValute(Date dataCreazione);

}
