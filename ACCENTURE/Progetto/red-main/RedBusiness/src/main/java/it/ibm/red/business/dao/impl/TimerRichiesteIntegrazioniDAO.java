package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.ITimerRichiesteIntegrazioniDAO;
import it.ibm.red.business.dto.TimerRichiesteIntegrazioniDTO;
import it.ibm.red.business.enums.FlagLavoratoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * DAO del Timer per la gestione della persistenza.
 * 
 * @author d.ventimiglia
 */
@Repository
public class TimerRichiesteIntegrazioniDAO extends AbstractDAO implements ITimerRichiesteIntegrazioniDAO {

	/**
	 * serial version UID.
	 */
	private static final long serialVersionUID = -4637898229376471568L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TimerRichiesteIntegrazioniDAO.class);

	/**
	 * @see it.ibm.red.business.dao.ITimerRichiesteIntegrazioniDAO#registraTimestamp(long,
	 *      long, long, java.sql.Timestamp, int, java.sql.Connection).
	 */
	@Override
	public void registraTimestamp(final long idDocumento, final long idNodo, final long idUtente, final Timestamp timestamp, final int flagLavorato, final Connection conn) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("INSERT INTO TIMER_RICHIESTA_CHIARIMENTI (IDTIMER, IDDOCUMENTO, IDNODO, IDUTENTE, DATAINVIO, FLAGLAVORATO) "
					+ "VALUES (SEQ_TIMER.NEXTVAL, ? , ? , ? , ? , ?)");
			int index = 1;
			ps.setLong(index++, idDocumento);
			ps.setLong(index++, idNodo);
			ps.setLong(index++, idUtente);
			ps.setTimestamp(index++, timestamp);
			ps.setInt(index++, flagLavorato);

			rs = ps.executeQuery();

		} catch (final SQLException e) {
			LOGGER.error("Errore durante la registazione del timestamp", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ITimerRichiesteIntegrazioniDAO#getAndLockNextDaLavorare(java.sql.Connection).
	 */
	@Override
	public Collection<TimerRichiesteIntegrazioniDTO> getAndLockNextDaLavorare(final Connection conn) {
		final Collection<TimerRichiesteIntegrazioniDTO> items = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			final String sql = "SELECT * FROM TIMER_RICHIESTA_CHIARIMENTI WHERE FLAGLAVORATO = ?"
					+ " AND NVL(DATA_ULTIMA_ELABORAZIONE + 1, TO_DATE('01/01/1900', 'mm/dd/yyyy')) <= SYSDATE FOR UPDATE SKIP LOCKED ORDER BY DATAINVIO";
			ps = conn.prepareStatement(sql);
			ps.setLong(index, FlagLavoratoEnum.DA_LAVORARE.getId());
			
			rs = ps.executeQuery();
			while (rs.next()) {
				final TimerRichiesteIntegrazioniDTO item = new TimerRichiesteIntegrazioniDTO(rs.getLong("IDTIMER"), rs.getLong("IDDOCUMENTO"), rs.getLong("IDNODO"),
						rs.getLong("IDUTENTE"), rs.getTimestamp("DATAINVIO"), rs.getInt("FLAGLAVORATO"));
				items.add(item);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del primo item lavorabile");
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return items;
	}

	/**
	 * @see it.ibm.red.business.dao.ITimerRichiesteIntegrazioniDAO#aggiornaFlagLavorato(long,
	 *      int, java.sql.Connection).
	 */
	@Override
	public void aggiornaFlagLavorato(final long idTimer, final int flagLavorato, final Connection conn) {
		PreparedStatement ps = null;
		final ResultSet rs = null;
		try {
			ps = conn.prepareStatement("UPDATE TIMER_RICHIESTA_CHIARIMENTI SET FLAGLAVORATO = ?, DATA_ULTIMA_ELABORAZIONE = ? WHERE IDTIMER = ?");

			ps.setInt(1, flagLavorato);
			ps.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
			ps.setLong(3, idTimer);
			ps.executeUpdate();

		} catch (final Exception e) {
			LOGGER.error("Errore durante l'aggiornamento dello stato lavorazione del documento", e);
			aggiornaFlagLavorato(idTimer, FlagLavoratoEnum.IN_ERRORE.getId(), conn);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ITimerRichiesteIntegrazioniDAO#getTimerConfigurazione(long,
	 *      long, java.sql.Connection).
	 */
	@Override
	public long getTimerConfigurazione(final long idTD, final long idTP, final Connection conn) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		long t = -1;
		try {
			ps = conn.prepareStatement("SELECT MINUTISCADENZASOSPESO FROM TIPOLOGIADOCUMENTOPROCEDIMENTO WHERE IDTIPOLOGIADOCUMENTO = ? AND IDTIPOPROCEDIMENTO = ?");
			ps.setLong(1, idTD);
			ps.setLong(2, idTP);
			rs = ps.executeQuery();

			if (rs.next()) {
				t = rs.getLong("MINUTISCADENZASOSPESO");
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del tempo configurato per la coppia tipo documento/procedimento con ids: " + idTD + "/" + idTP, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return t;
	}
}
