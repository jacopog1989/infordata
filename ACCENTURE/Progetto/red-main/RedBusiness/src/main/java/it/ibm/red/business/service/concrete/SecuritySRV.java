package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.collection.AccessPermissionList;
import com.filenet.api.collection.DocumentSet;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.core.Document;
import com.filenet.api.core.Factory;
import com.filenet.api.core.Folder;
import com.filenet.api.core.IndependentlyPersistableObject;
import com.filenet.api.property.FilterElement;
import com.filenet.api.property.Properties;
import com.filenet.api.property.PropertyFilter;
import com.filenet.api.security.AccessPermission;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.IAllaccioDAO;
import it.ibm.red.business.dao.IContributoDAO;
import it.ibm.red.business.dao.IIterApprovativoDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DetailAssegnazioneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.SecurityDTO;
import it.ibm.red.business.dto.SecurityDTO.GruppoS;
import it.ibm.red.business.dto.SecurityDTO.UtenteS;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.StoricoDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.FilenetException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contributo;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.NodoUtenteRuolo;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.ISecuritySRV;
import it.ibm.red.business.service.IStoricoSRV;

/**
 * The Class SecuritySRV.
 *
 * @author CPIERASC
 * 
 *         Servizio gestione security.
 */
@Service
public class SecuritySRV extends AbstractService implements ISecuritySRV {

	/**
	 * Messaggio.
	 */
	private static final String ERRORE_NELLA_MODIFICA_DELLE_SECURITY_DEI_DOCUMENTI_ALLACCIATI = "Errore nella modifica delle security dei documenti allacciati.";

	/**
	 * Dimensione iniziale lista security.
	 */
	private static final int INITIAL_CAPACITY = 10;

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -2370909204921499965L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SecuritySRV.class.getName());

	/**
	 * Dao nodo.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * Dao utente.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private IIterApprovativoDAO iterApprovativoDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private IAllaccioDAO allaccioDAO;

	/**
	 * Service.
	 */
	@Autowired
	private IStoricoSRV storicoSRV;

	/**
	 * Dao.
	 */
	@Autowired
	private IContributoDAO contributoDAO;
	
	/**
	 * Generate riservato security. Aggiunge le security puntuali al documento dopo
	 * aver effettuato un avanzamento del workflow.
	 *
	 * @param utente         the utente
	 * @param idDocumento    the id documento
	 * @param workflowNumber the work flow number
	 * @param fpeh           the fpeh
	 * @param connection     the connection
	 */
	@Override
	public final void generateRiservatoSecurity(final UtenteDTO utente, final String idDocumento, final String workflowNumber, final FilenetPEHelper fpeh,
			final Connection connection) {
		try {
			final ListSecurityDTO securities = getSecurityRiservatoAfterAvanzaWf(workflowNumber, fpeh, connection);
			if (securities != null) {
				updateSecurity(utente, idDocumento, null, securities, true);
				final boolean modificaSecFascicolo = modificaSecurityFascicoli(utente, securities, idDocumento);
				if (!modificaSecFascicolo) {
					throw new RedException("Errore nella modifica delle security del fascicolo legato al documento " + idDocumento);
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore nella gestione delle security del RISERVATO", e);
			throw new RedException(e);
		}
	}

	/**
	 * Generate riservato security.
	 * Aggiunge le security puntuali al documento dopo aver effettuato un avanzamento del workflow.
	 *
	 * @param utente
	 *            the utente
	 * @param idDocumento
	 *            the id documento
	 * @param workflowNumber
	 *            the work flow number
	 * @param fpeh
	 *            the fpeh
	 * @param connection
	 *            the connection
	 */
	@Override
	public final void generateRiservatoSecurity(final UtenteDTO utente, final String idDocumento, final String workflowNumber, 
				final IFilenetCEHelper fceh, final FilenetPEHelper fpeh, final Connection connection) {
		try {
			ListSecurityDTO securities = getSecurityRiservatoAfterAvanzaWf(workflowNumber, fpeh, connection);
			if (securities != null) {
				updateSecurity(utente, idDocumento, securities, true, fceh);
				boolean modificaSecFascicolo = modificaSecurityFascicoli(utente.getIdAoo(), securities, idDocumento, fceh);
				if (!modificaSecFascicolo) {
					throw new RedException("Errore nella modifica delle security del fascicolo legato al documento " + idDocumento);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Errore nella gestione delle security del RISERVATO", e);
			throw new RedException(e);
		}
	}
	
	/**
	 * Ricrea le security in base alla coda del workflow.
	 * 
	 * @param workflowNumber - Workflow del documento
	 * @param fpeh           - Connessione al ProcessEngine di Filenet
	 * @param connection     - Connessione al DB
	 * @return Array delle security
	 */
	private ListSecurityDTO getSecurityRiservatoAfterAvanzaWf(final String workflowNumber, final FilenetPEHelper fpeh, final Connection connection) {
		ListSecurityDTO securityList = null;

		try {
			final VWWorkObject workflow = fpeh.getWorkFlowByWob(workflowNumber, true);
			final PropertiesProvider pp = PropertiesProvider.getIstance();
			final Integer idNodoDestinatario = (Integer) TrasformerPE.getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY));
			Integer idUtenteDestinatario = (Integer) TrasformerPE.getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY));
			// se il wf si trova in coda corriere, sovrascrive la security puntuale
			// dell'utente
			if (DocumentQueueEnum.CORRIERE.getName().equals(workflow.getCurrentQueueName())) {
				idUtenteDestinatario = 0;
			}
			final List<String[]> assegnazione = new ArrayList<>();
			assegnazione.add(new String[] { idNodoDestinatario + "," + idUtenteDestinatario });

			securityList = getRiservatoSecurity(assegnazione, connection);

			if (!securityList.isEmpty()) {
				securityList.setNullableUtente();

				// Si verificano le security
				verificaSecurities(securityList, connection);
			} else {
				securityList = null;
			}
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero delle security dop ol'avanzamento del WF", e);
		}

		return securityList;
	}

	/**
	 * Da una lista di assegnazioni vengono ricavate le security puntuali del
	 * nodo/corriere o dell'utente.
	 * 
	 * @param assegnazioniTotali -
	 * @param connection         - Connessione al DB.
	 * @return Lista di assegnazioni vengono ricavate le security puntuali del
	 *         nodo/corriere o dell'utente
	 */
	private ListSecurityDTO getRiservatoSecurity(final List<String[]> assegnazioniTotali, final Connection connection) {
		final ListSecurityDTO securityList = new ListSecurityDTO();

		for (final String[] assegnazione : assegnazioniTotali) {
			for (final String assegnazioneString : assegnazione) {
				final String[] splittedValue = assegnazioneString.split(",");
				final Long idNodo = Long.parseLong(splittedValue[0]);
				final Long idUtenteCorrente = Long.parseLong(splittedValue[1]);
				final Long idNodoCorriere = nodoDAO.getNodoCorriereId(idNodo, connection);
				securityList.add(creaSecurity(idNodo, idUtenteCorrente, idNodoCorriere));
			}
		}

		return securityList;
	}

	/**
	 * Da una lista di assegnazioni vengono ricavate le security puntuali del
	 * nodo/corriere o dell'utente.
	 * 
	 * @param assegnazioni - Lista di utenti (id e idUfficio) assegnatari.
	 * @return Lista di assegnazioni vengono ricavate le security puntuali del
	 *         nodo/corriere o dell'utente
	 */
	@Override
	public ListSecurityDTO getSecurityPerRegistroGiornaliero(final Collection<UtenteDTO> utentiAssegnazioni, final Connection connection) {
		LOGGER.info("getSecurityPerRegistroGiornaliero -> START");
		ListSecurityDTO securityList = new ListSecurityDTO();

		try {
			final List<String[]> assegnazioniTotali = new ArrayList<>();
			if (utentiAssegnazioni != null && !utentiAssegnazioni.isEmpty()) {
				for (final UtenteDTO utenteAssegnazione : utentiAssegnazioni) {
					final String[] assegnazione = new String[] { utenteAssegnazione.getIdUfficio() + "," + utenteAssegnazione.getId() };
					assegnazioniTotali.add(assegnazione);
				}
				securityList = getRiservatoSecurity(assegnazioniTotali, connection);
			}
			if (!securityList.isEmpty()) {
				// Si imposta l'utente delle security a null se l'idUtente è 0
				securityList.setNullableUtente();
				// Si verificano le security
				verificaSecurities(securityList, connection);
			} else {
				securityList = null;
			}
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero delle security per registro giornaliero: " + e.getMessage(), e);
			throw new RedException(e);
		}

		LOGGER.info("getSecurityPerRegistroGiornaliero -> END");
		return securityList;
	}

	/**
	 * Crea la security a partire dall'idnodo, idutente e idcorriere.
	 * 
	 * @param idNodoCorrente   - Identificativo del nodo corrente
	 * @param idUtenteCorrente - Identificativo dell'utente corriere
	 * @param idNodoCorriere   - Identificativo del nodo corriere
	 * @return secury a partire dall idnodo, idutente e idcorriere
	 */
	@Override
	public SecurityDTO creaSecurity(final Long idNodoCorrente, final Long idUtenteCorrente, final Long idNodoCorriere) {
		final SecurityDTO newSecurity = new SecurityDTO();

		final GruppoS newGruppo = newSecurity.getGruppo();
		if (idNodoCorriere != null && idNodoCorriere > 0) {
			newGruppo.setIdUfficio(idNodoCorriere.intValue());
			newGruppo.setIdSiap(-1);
		} else if (idNodoCorrente != null && idNodoCorrente > 0) {
			newGruppo.setIdUfficio(idNodoCorrente.intValue());
		}
		newSecurity.setGruppo(newGruppo);

		final UtenteS newUtente = newSecurity.getUtente();
		if (idUtenteCorrente != null && idUtenteCorrente > 0) {
			newUtente.setIdUtente(idUtenteCorrente.intValue());
		} else {
			newUtente.setIdUtente(0);
		}
		newSecurity.setUtente(newUtente);

		return newSecurity;
	}

	/**
	 * Gets the security utente per ufficio fascicolo.
	 *
	 * @param utente                   the utente
	 * @param idDocumento              the id documento
	 * @param idNodoDestinatario       the id nodo destinatario
	 * @param destinatariInterni       the destinatari interni
	 * @param assegnazioneCoordinatore the coordinatore
	 * @param isRiservato              the is riservato
	 * @return the security utente per ufficio fascicolo
	 */
	@Override
	public final ListSecurityDTO getSecurityUtentePerUfficioFascicolo(final UtenteDTO utente, final String idDocumento, final String idNodoDestinatario,
			final List<String> destinatariInterni, final String assegnazioneCoordinatore, final Boolean isRiservato) {
		return getSecurityUtentePerUfficio(utente, idDocumento, idNodoDestinatario, destinatariInterni, assegnazioneCoordinatore, false, isRiservato);
	}

	/**
	 * Gets the security utente per ufficio.
	 *
	 * @param utente             the utente
	 * @param idDocumento        the id documento
	 * @param idNodoDestinatario the id nodo destinatario
	 * @param destinatariInterni the destinatari interni
	 * @param coordinatore       the coordinatore
	 * @param isRiservato        the is riservato
	 * @return the security utente per ufficio
	 */
	@Override
	public final ListSecurityDTO getSecurityUtentePerUfficio(final UtenteDTO utente, final String idDocumento, final String idNodoDestinatario,
			final List<String> destinatariInterni, final String coordinatore, final Boolean isRiservato) {
		return getSecurityUtentePerUfficio(utente, idDocumento, idNodoDestinatario, destinatariInterni, coordinatore, true, isRiservato);
	}

	/**
	 * Calcolo delle security per documento.
	 * 
	 * @param idDocumento
	 * @param account
	 * @param assegnazioniCompetenza
	 * @param assegnazioniConoscenza
	 * @param assegnazioniContributo
	 * @param assegnazioniFirma
	 * @param assegnazioniSigla
	 * @param assegnazioniVisto
	 * @param idNodoCreatore
	 * @param idUtenteCreatore
	 * @param isDocumento
	 * @param isDocEntrata
	 * @param idIterAutomatico
	 * @param isRiservato
	 * @param coordinatore
	 * @param aggiungiDelegato
	 * @param destinatariInterni
	 * @return
	 */
	@Override
	public ListSecurityDTO getSecurityPerDocumento(final String idDocumento, final UtenteDTO utente, final String[] assegnazioniCompetenza,
			final String[] assegnazioniConoscenza, final String[] assegnazioniContributo, final String[] assegnazioniFirma, final String[] assegnazioniSigla,
			final String[] assegnazioniVisto, final String[] assegnazioniFirmaMultipla, final Long idNodoCreatore, final Long idUtenteCreatore, final Boolean isDocumento,
			final Boolean isDocEntrata, final int idIterApprovativo, final Boolean isRiservato, final String assegnazioneCoordinatore, final Boolean inAggiungiDelegato,
			final Boolean isNodoUcp, final String[] destinatariInterni, final Connection connection) {
		LOGGER.info("getSecurityPerDocumento -> START. Documento: " + idDocumento);
		// Lista delle security
		final ListSecurityDTO securityList = new ListSecurityDTO();
		Boolean aggiungiDelegato = inAggiungiDelegato;

		try {
			// Si uniscono le assegnazioni
			final List<String[]> assegnazioniTotali = getAssegnazioniTotali(assegnazioniCompetenza, assegnazioniConoscenza, assegnazioniContributo, assegnazioniFirma,
					assegnazioniSigla, assegnazioniVisto, assegnazioniFirmaMultipla, destinatariInterni, aggiungiDelegato, connection);

			if (idNodoCreatore != null && idUtenteCreatore != null && isNodoUcp != null && isDocEntrata != null) {
				final String[] assegnazioneUtenteCreatore = new String[] { idNodoCreatore + "," + idUtenteCreatore };
				// Documento pubblico
				if (isRiservato == null || Boolean.FALSE.equals(isRiservato)) {
					// Si aggiungono le assegnazioni per l'utente creatore
					assegnazioniTotali.add(assegnazioneUtenteCreatore);

					// Si recuperano le security per tutte le assegnazioni
					for (final String[] assegnazione : assegnazioniTotali) {
						securityList.addAll(getSecurityAssegnazioni(assegnazione, isDocumento, false, isRiservato, connection));
					}
					// Documento riservato
				} else {
					// Nel caso di documento in ingresso, l'utente dell'UCP non deve avere
					// visibilità sul documento
					if (!(isDocEntrata && isNodoUcp)) {
						assegnazioniTotali.add(assegnazioneUtenteCreatore);
					}

					securityList.addAll(getRiservatoSecurity(assegnazioniTotali, connection));

					// Se è riservato, si posticipano le security del delegato
					aggiungiDelegato = false;
				}
			} else {
				// Si recuperano le security per tutte le assegnazioni
				for (final String[] assegnazione : assegnazioniTotali) {
					securityList.addAll(getSecurityAssegnazioni(assegnazione, isDocumento, false, isRiservato, connection));
				}
			}

			// Security iter automatico, delegato e coordinatore
			if (assegnazioniCompetenza.length == 0 && assegnazioniConoscenza.length == 0 && assegnazioniContributo.length == 0 && assegnazioniFirma.length == 0
					&& assegnazioniFirmaMultipla.length == 0 && assegnazioniSigla.length == 0 && assegnazioniVisto.length == 0) {
				securityList.addAll(getSecurityIterAutomaticoAndDelegato(idDocumento, idIterApprovativo, aggiungiDelegato, utente, isDocumento, isRiservato, connection));
				if (StringUtils.isNotBlank(assegnazioneCoordinatore)) {
					securityList.addAll(getSecurityAssegnazione(assegnazioneCoordinatore, isDocumento, false, isRiservato, connection));
				}
			}

			securityList.setNullableUtente();

			// Si verificano le security
			verificaSecurities(securityList, connection);

			LOGGER.info("getSecurityPerDocumento -> Security ottenute per il documento: " + idDocumento);
		} catch (final Exception e) {
			LOGGER.error("getSecurityPerDocumento -> Errore nel recupero delle security per il documento: " + idDocumento, e);
			throw new RedException(e);
		}

		LOGGER.info("getSecurityPerDocumento -> END. Documento: " + idDocumento);
		return securityList;
	}

	/**
	 * @see it.ibm.red.business.service.ISecuritySRV#getSecurityDocumentoPerDocumentiAllacciati(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.String[],
	 *      java.lang.String[], java.lang.String[], java.lang.String[],
	 *      java.lang.String[], java.lang.String[], java.lang.String[],
	 *      java.lang.Long, java.lang.Long, java.lang.Boolean, int,
	 *      java.lang.Boolean, java.lang.String, java.lang.Boolean,
	 *      java.lang.String[], java.sql.Connection).
	 */
	@Override
	public ListSecurityDTO getSecurityDocumentoPerDocumentiAllacciati(final String idDocumento, final UtenteDTO utente, final String[] assegnazioniCompetenza,
			final String[] assegnazioniConoscenza, final String[] assegnazioniContributo, final String[] assegnazioniFirma, final String[] assegnazioniSigla,
			final String[] assegnazioniVisto, final String[] assegnazioniFirmaMultipla, final Long idNodoCreatore, final Long idUtenteCreatore, final Boolean isDocumento,
			final int idIterApprovativo, final Boolean isRiservato, final String assegnazioneCoordinatore, final Boolean inAggiungiDelegato, final String[] destinatariInterni,
			final Connection connection) {
		LOGGER.info("getSecurityDocumentoPerDocumentiAllacciati -> START. Documento: " + idDocumento);
		// Lista delle security
		final ListSecurityDTO securityList = new ListSecurityDTO();
		Boolean aggiungiDelegato = inAggiungiDelegato;

		try {
			// Si ottiene il nodo corriere
			final Long idNodoCorriere = nodoDAO.getNodoCorriereId(idNodoCreatore, connection);

			SecurityDTO newSecurity = null;
			GruppoS newGruppo = null;
			UtenteS newUtente = null;

			// Se il documento non è riservato, si utilizza l'alberatura degli uffici
			if (isRiservato == null || Boolean.FALSE.equals(isRiservato)) {
				// Si inserisce la gerarchia di chi crea il documento
				final List<Long> alberatura = nodoDAO.getAlberaturaUfficio(idNodoCreatore, connection);
				// Si inserisce la gerarchia di uffici
				for (Long nodoCorrente : alberatura) {
					// Si esclude l'ufficio reale e si inserisce il gruppo corriere
					if (nodoCorrente.equals(idNodoCreatore) && Boolean.TRUE.equals(isDocumento)) {
						nodoCorrente = idNodoCorriere;
					}
					newSecurity = new SecurityDTO();
					newGruppo = newSecurity.getGruppo();
					newGruppo.setIdUfficio(nodoCorrente.intValue());
					newSecurity.setGruppo(newGruppo);
					newUtente = newSecurity.getUtente();
					newUtente.setIdUtente(0);
					newSecurity.setUtente(newUtente);
					// Si aggiunge la nuova security alla lista
					securityList.add(newSecurity);
				}
			} else if (idUtenteCreatore == 0) {
				// Se riservato, si sovrascrive il delegato
				aggiungiDelegato = false;
				// Si inserisce la security al corriere
				securityList.add(creaSecurity(idNodoCreatore, 0L, idNodoCorriere));
			}

			// Se l'assegnazione è ad un utente si inserisce la security puntuale
			// sull'utente
			if (idUtenteCreatore > 0 && Boolean.TRUE.equals(isDocumento)) {
				// Inserisci l'utente creatore
				newSecurity = new SecurityDTO();
				newGruppo = newSecurity.getGruppo();
				newGruppo.setIdUfficio(idNodoCorriere.intValue());
				newSecurity.setGruppo(newGruppo);
				newUtente = newSecurity.getUtente();
				newUtente.setIdUtente(idUtenteCreatore.intValue());
				newSecurity.setUtente(newUtente);
				// Si aggiunge la nuova security alla lista
				securityList.add(newSecurity);
			}

			// Si inseriscono nella gerarchia tutti i destinatari
			final ListSecurityDTO destinatari = getSecurityDestinatari(idDocumento, utente, assegnazioniCompetenza, assegnazioniConoscenza,
					assegnazioniContributo, assegnazioniFirma, assegnazioniSigla, assegnazioniVisto, assegnazioniFirmaMultipla, isDocumento, idIterApprovativo, isRiservato,
					assegnazioneCoordinatore, aggiungiDelegato, destinatariInterni, connection);
			securityList.addAll(destinatari);

			// Si verificano le security
			verificaSecurities(securityList, connection);

			// Logga la lista delle security
			printSecurityList(securityList);
			LOGGER.info("getSecurityDocumentoPerDocumentiAllacciati -> Security calcolate per il documento: " + idDocumento);
		} catch (final Exception e) {
			rollbackConnection(connection);
			LOGGER.error("getSecurityDocumentoPerDocumentiAllacciati -> Errore nel calcolo delle security per il documento: " + idDocumento, e);
			throw new RedException(e);
		}

		LOGGER.info("getSecurityDocumentoPerDocumentiAllacciati -> END. Documento: " + idDocumento);
		return securityList;
	}

	private List<String[]> getAssegnazioniTotali(final String[] assegnazioniCompetenza, final String[] assegnazioniConoscenza, final String[] assegnazioniContributo,
			final String[] assegnazioniFirma, final String[] assegnazioniSigla, final String[] assegnazioniVisto, final String[] assegnazioniFirmaMultipla,
			final String[] destinatariInterni, final boolean addDelegato, final Connection connection) {
		final List<String[]> assegnazioniTotali = new ArrayList<>();

		if (assegnazioniCompetenza != null && assegnazioniCompetenza.length > 0) {
			assegnazioniTotali.add(assegnazioniCompetenza);
		}
		if (assegnazioniConoscenza != null && assegnazioniConoscenza.length > 0) {
			assegnazioniTotali.add(assegnazioniConoscenza);
		}
		if (assegnazioniContributo != null && assegnazioniContributo.length > 0) {
			assegnazioniTotali.add(assegnazioniContributo);
		}

		aggiungiAssegnazioni(assegnazioniTotali, assegnazioniFirma, addDelegato, connection);
		aggiungiAssegnazioni(assegnazioniTotali, assegnazioniSigla, addDelegato, connection);
		aggiungiAssegnazioni(assegnazioniTotali, assegnazioniVisto, addDelegato, connection);

		if (assegnazioniFirmaMultipla != null && assegnazioniFirmaMultipla.length > 0) { // Gestione del delegato non presente per la Firma Multipla
			assegnazioniTotali.add(assegnazioniFirmaMultipla);
		}

		if (destinatariInterni != null && destinatariInterni.length > 0) {
			assegnazioniTotali.add(destinatariInterni);
		}

		return assegnazioniTotali;
	}

	private void aggiungiAssegnazioni(final List<String[]> assegnazioniTotali, final String[] assegnazioni, final boolean addDelegato, final Connection connection) {
		if (assegnazioni != null && assegnazioni.length > 0) {
			assegnazioniTotali.add(assegnazioni);
			if (addDelegato) {
				final String[] assegnazioniDelegato = getAssegnazioniUtentiDelegati(assegnazioni, connection);
				if (assegnazioniDelegato != null && assegnazioniDelegato.length > 0) {
					assegnazioniTotali.add(assegnazioniDelegato);
				}
			}
		}
	}

	private ListSecurityDTO getSecurityIterAutomaticoAndDelegato(final String idDocumento, final int idIterApprovativo, final boolean addDelegato,
			final UtenteDTO utente, final boolean isDocumento, final boolean isRiservato, final Connection connection) {
		LOGGER.info("getSecurityIterAutomaticoAndDelegato -> START, idDocumento: " + idDocumento);
		final ListSecurityDTO securityList = new ListSecurityDTO();

		final Nodo nodoUtente = nodoDAO.getNodo(utente.getIdUfficio(), connection);
		final Collection<NodoUtenteRuolo> nurDirigente = utenteDAO.getNodoUtenteRuolo(nodoUtente.getDirigente().getIdUtente(), null, connection);

		NodoUtenteRuolo nodoUtenteRuoloPredefinito = null;
		for (final NodoUtenteRuolo nodoUtenteRuolo : nurDirigente) {
			if (nodoUtenteRuolo.getPredefinito() != 0) {
				nodoUtenteRuoloPredefinito = nodoUtenteRuolo;
				break;
			}
		}

		if (nodoUtenteRuoloPredefinito == null) {
			throw new RedException("Non risulta un dirigente configurato per questo ufficio.");
		}

		final Long idNodoDirigente = nodoUtenteRuoloPredefinito.getIdnodo();

		if (idIterApprovativo > 0) {
			final String assegnazioneDirigente = idNodoDirigente + "," + nodoUtente.getDirigente().getIdUtente();
			securityList.addAll(getSecurityAssegnazione(assegnazioneDirigente, isDocumento, false, isRiservato, connection));
		}

		if (addDelegato) {
			final List<Utente> delegatiDirigente = utenteDAO.getUtentiDelegati(idNodoDirigente.intValue(), connection);
			if (!delegatiDirigente.isEmpty()) {
				final String[] assegnazioniDelegati = new String[delegatiDirigente.size()];
				for (int index = 0; index < delegatiDirigente.size(); index++) {
					assegnazioniDelegati[index] = idNodoDirigente + "," + delegatiDirigente.get(index).getIdUtente();
				}
				final ListSecurityDTO securityDelegato = getSecurityAssegnazioni(assegnazioniDelegati, isDocumento, false, isRiservato, connection);
				securityList.addAll(securityDelegato);
			}

			// Se l'iter è automatico, allora si aggiunge anche il delegato del firmatario
			if (idIterApprovativo != IterApprovativoDTO.ITER_FIRMA_MANUALE.intValue()) {

				final IterApprovativoDTO iterApprovativo = iterApprovativoDAO.getIterApprovativoById(idIterApprovativo, connection);

				// Si ottiene il firmatario
				final String firmatario = iterApprovativoDAO.getFirmatario(utente.getIdUfficio(), iterApprovativo.getTipoFirma(), connection);

				// Si ottengono le security
				final List<String> assegnazioniUtentiDelegati = getAssegnazioneUtentiDelegati(firmatario, connection);
				if (!assegnazioniUtentiDelegati.isEmpty()) {
					final String[] assegnazioniUtentiDelegatiArray = new String[assegnazioniUtentiDelegati.size()];
					assegnazioniUtentiDelegati.toArray(assegnazioniUtentiDelegatiArray);
					final ListSecurityDTO securityDestinatarioIspettoreDelegato = getSecurityAssegnazioni(assegnazioniUtentiDelegatiArray, isDocumento, false,
							isRiservato, connection);
					securityList.addAll(securityDestinatarioIspettoreDelegato);
				}

			}
		}

		LOGGER.info("getSecurityIterAutomaticoAndDelegato END, idDocumento: " + idDocumento);
		return securityList;
	}

	/**
	 * Restituisce le security destinatari.
	 * 
	 * @param idDocumento
	 *            id del documento
	 * @param utente
	 *            utente
	 * @param assegnazioniCompetenza
	 *            assegnazioni per competenza
	 * @param assegnazioniConoscenza
	 *            assegnazioni per conoscenza
	 * @param assegnazioniContributo
	 *            assegnazioni per contributo
	 * @param assegnazioniFirma
	 *            assegnazioni firma
	 * @param assegnazioniSigla
	 *            assegnazioni sigla
	 * @param assegnazioniVisto
	 *            assegnazioni visto
	 * @param assegnazioniFirmaMultipla
	 *            assegnazioni per firma multipla
	 * @param isDocumento
	 *            true se documento, false altrimenti
	 * @param idIterApprovativo
	 *            id iter approvativo
	 * @param isRiservato
	 *            flag riservato
	 * @param assegnazioneCoordinatore
	 *            assegnazione coordinatore
	 * @param aggiungiDelegato
	 *            flag aggiungi delegato
	 * @param destinatariInterni
	 *            destinatari interni
	 * @param connection
	 *            connessione al database
	 * @return lista delle security
	 */
	private ListSecurityDTO getSecurityDestinatari(final String idDocumento, final UtenteDTO utente, final String[] assegnazioniCompetenza,
			final String[] assegnazioniConoscenza, final String[] assegnazioniContributo, final String[] assegnazioniFirma, final String[] assegnazioniSigla,
			final String[] assegnazioniVisto, final String[] assegnazioniFirmaMultipla, final Boolean isDocumento, final int idIterApprovativo, final Boolean isRiservato,
			final String assegnazioneCoordinatore, final Boolean aggiungiDelegato, final String[] destinatariInterni, final Connection connection) {
		LOGGER.info("getSecurityDestinatari -> START. Documento: " + idDocumento);
		ListSecurityDTO securityList;

		// N.B. IL CODICE DEL METODO CORRISPONDENTE SU NSD DOVREBBE POTER ESSER
		// SOSTITUITO DALLA CHIAMATA AL METODO QUI SOTTO
		securityList = getSecurityPerDocumento(idDocumento, utente, assegnazioniCompetenza, assegnazioniConoscenza, assegnazioniContributo, assegnazioniFirma,
				assegnazioniSigla, assegnazioniVisto, assegnazioniFirmaMultipla, null, null, isDocumento, null, idIterApprovativo, isRiservato, assegnazioneCoordinatore,
				aggiungiDelegato, null, destinatariInterni, connection);

		LOGGER.info("getSecurityDestinatari END. Documento: " + idDocumento);
		return securityList;
	}

	/**
	 * @see it.ibm.red.business.service.ISecuritySRV#getSecurityPerBozza(java.lang.Long,
	 *      java.lang.Long, boolean, java.sql.Connection).
	 */
	@Override
	public ListSecurityDTO getSecurityPerBozza(final Long idNodoCreatore, final Long idUtenteCreatore, final boolean isDocumento, final Connection connection) {
		// Lista delle security
		final ListSecurityDTO securityList = getSecurityMittenteBozza(idNodoCreatore, idUtenteCreatore, isDocumento, connection);

		securityList.setNullableUtente();

		// Si verificano le security
		verificaSecurities(securityList, connection);

		return securityList;
	}

	private ListSecurityDTO getSecurityMittenteBozza(final Long idNodoCreatore, final Long idUtenteCreatore, final boolean isDocumento,
			final Connection connection) {
		LOGGER.info("getSecurityMittenteBozza -> START, idNodoCreatore: " + idNodoCreatore + ", idUtenteCreatore: " + idUtenteCreatore);
		// Lista delle security
		final ListSecurityDTO securityList = new ListSecurityDTO();

		try {
			// Ottieni il nodo corriere
			final Long idNodoCorriere = nodoDAO.getNodoCorriereId(idNodoCreatore, connection);

			// Se l'assegnazione è ad un utente, si inserisce la security puntuale
			// sull'utente
			if (idUtenteCreatore > 0 && isDocumento) {
				securityList.add(creaSecurity(idNodoCreatore, idUtenteCreatore, idNodoCorriere));
			}
		} catch (final Exception e) {
			LOGGER.warn(e);
		}

		printSecurityList(securityList);

		LOGGER.info("getSecurityMittenteBozza END, idNodoCreatore: " + idNodoCreatore + ", idUtenteCreatore: " + idUtenteCreatore);
		return securityList;
	}

	@Override
	public final boolean modificaSecurityFascicoli(final String idDocumento, final UtenteDTO utente, final String[] assegnazioni, final Connection con) {
		return modificaSecurityFascicoli(idDocumento, utente, assegnazioni, null, false, con);
	}

	@Override
	public final boolean modificaSecurityFascicoli(final String idDocumento, final UtenteDTO utente, final String[] assegnazioni, final String coordinatore,
			final boolean aggiungiDelegato, final Connection con) {
		LOGGER.info("modificaSecurityFascicoli -> START. Documento: " + idDocumento);
		boolean modificato = false;

		// Si ottengono le security
		final ListSecurityDTO security = getSecurityPerRiassegnazioneFascicolo(utente, idDocumento, assegnazioni, coordinatore, aggiungiDelegato, con);

		// Si modificano le security dei fascicoli del documento
		modificato = modificaSecurityFascicoli(utente, security, idDocumento);

		LOGGER.info("modificaSecurityFascicoli -> END. Documento: " + idDocumento);
		return modificato;
	}

	/**
	 * Modifica security fascicoli.
	 *
	 * @param utente      the utente
	 * @param securities  the securities
	 * @param idDocumento the id documento
	 * @return true, if successful
	 */
	@Override
	public final boolean modificaSecurityFascicoli(final UtenteDTO utente, final ListSecurityDTO securities, final String idDocumento) {
		boolean modificato = false;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			LOGGER.info("Modifica delle security dei fascicoli associati al documento: " + idDocumento);
			modificato = modificaSecurityFascicoli(utente.getIdAoo(), securities, idDocumento, fceh);
		} finally {
			popSubject(fceh);
		}

		return modificato;
	}

	@Override
	public final boolean modificaSecurityFascicoli(final Long idAoo, final ListSecurityDTO securities, final String idDocumento, final IFilenetCEHelper fceh) {
		boolean modificato = false;

		LOGGER.info("Modifica delle security dei fascicoli associati al documento: " + idDocumento);

		final List<FascicoloDTO> fascicoli = fceh.getFascicoliDocumento(idDocumento, idAoo.intValue());
		// Modifica delle security sul fascicolo ####################
		// N.B. cambia le security per tutti i fascicoli del documento
		for (final FascicoloDTO fascicoloNsd : fascicoli) {
			fceh.updateFascicoloSecurity(fascicoloNsd.getIdFascicolo(), idAoo, securities);
			modificato = true;
		}

		return modificato;
	}

	/**
	 * update security.
	 *
	 * @param utente      the utente
	 * @param idDocumento the id documento
	 * @param folderName  the folder name
	 * @param securities  the securities
	 * @param allVersion  the all version
	 */
	@Override
	public final void updateSecurity(final UtenteDTO utente, final String idDocumento, final String folderName, final ListSecurityDTO securities,
			final Boolean allVersion) {
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(),utente.getIdAoo());
			updateSecurity(utente, idDocumento, securities, allVersion, fceh);
		} catch (Exception e) {
			LOGGER.error("Errore durante l'aggiornamento delle security per il documento: " + idDocumento, e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}
	}
	
	private final void updateSecurity(final UtenteDTO utente, final String idDocumento, final ListSecurityDTO securities, 
			final Boolean allVersion, final IFilenetCEHelper fceh) {
		Connection connection =  null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			aggiornaEVerificaSecurityDocumento(idDocumento, utente.getIdAoo(), securities, allVersion, false, fceh, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'aggiornamento delle security per il documento: " + idDocumento, e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Recupero security utente per ufficio.
	 * 
	 * @param utente                   credenziali utente
	 * @param idDocumento              identificativo documento
	 * @param idNodoDestinatario       id nodo destinatario
	 * @param destinatariInterni       lista destinatari interni
	 * @param assegnazioneCoordinatore coordinatore
	 * @param isDocumento              flag is documento
	 * @param isRiservato              flag riservato
	 * @return lista security
	 */
	private ListSecurityDTO getSecurityUtentePerUfficio(final UtenteDTO utente, final String idDocumento, final String idNodoDestinatario,
			final List<String> destinatariInterni, final String assegnazioneCoordinatore, final boolean isDocumento, final Boolean isRiservato) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			final ListSecurityDTO riassegnazioniSecurity = getStoricoSecurity(utente, idDocumento, isDocumento, true, isRiservato, false,
					destinatariInterni, connection);

			final SecurityDTO security = new SecurityDTO();
			final GruppoS gruppoS = security.getGruppo();
			gruppoS.setIdUfficio(Integer.parseInt(idNodoDestinatario));
			final UtenteS utenteS = security.getUtente();
			utenteS.setIdUtente(utente.getId().intValue());
			security.setGruppo(gruppoS);
			security.setUtente(utenteS);
			riassegnazioniSecurity.add(security);

			if (StringUtils.isNotBlank(assegnazioneCoordinatore)) {
				final ListSecurityDTO securityDestinatarioCoordinatore = getSecurityAssegnazione(assegnazioneCoordinatore, isDocumento, true, isRiservato,
						connection);
				riassegnazioniSecurity.addAll(securityDestinatarioCoordinatore);
			}

			//rimuovi le assegnazioni senza ufficio
			final ListSecurityDTO finalRiassegnazioniSecurity = new ListSecurityDTO();
			for (int i = 0; i < riassegnazioniSecurity.size(); i++) {
				if (riassegnazioniSecurity.get(i).getGruppo().getIdUfficio() == 0) {
					continue;
				}
				finalRiassegnazioniSecurity.add(riassegnazioniSecurity.get(i));
			}
			
			verificaSecurities(finalRiassegnazioniSecurity, connection);

			return finalRiassegnazioniSecurity;
		} catch (final Exception e) {
			LOGGER.error("Errore nel calcolo delle security utente per ufficio [idDocumento: " + idDocumento + "] ", e);
			throw new FilenetException(e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Verifica security. Aggiunge a tutte le security lo username perché serve
	 * successivamente per recuperare un AD
	 * 
	 * @param securities security
	 * @param connection connessione
	 */
	@Override
	public void verificaSecurities(final ListSecurityDTO securities, final Connection connection) {
		for (final SecurityDTO sec : securities) {
			if (sec.getUtente() != null && sec.getUtente().getIdUtente() > 0) {
				final Utente u = utenteDAO.getUtente(Long.valueOf(sec.getUtente().getIdUtente()), connection);
				sec.getUtente().setUsername(u.getUsername());
			}
		}
	}

	/**
	 * Recupera security assegnazione.
	 * 
	 * @param assegnazione    assegazione
	 * @param isDocumento     flag documento
	 * @param aggiungiUfficio flag add ufficio
	 * @param isRiservato     flag riservato
	 * @param connection      connessione
	 * @return lista security
	 */
	@Override
	public ListSecurityDTO getSecurityAssegnazione(final String assegnazione, final boolean isDocumento, final boolean aggiungiUfficio, final boolean isRiservato,
			final Connection connection) {
		final ListSecurityDTO securityList = new ListSecurityDTO();

		Long idNodoCorrente = 0L;
		Long idUtenteCorrente = 0L;
		final String[] splittedValue = assegnazione.split(",");
		if (splittedValue != null && splittedValue.length > 1) {
			idNodoCorrente = Long.parseLong(splittedValue[0]);
			idUtenteCorrente = Long.parseLong(splittedValue[1]);
		}

		// Si ottiene il nodo corriere
		final Long idNodoCorriere = nodoDAO.getNodoCorriereId(idNodoCorrente, connection);

		// Se il documento non è riservato, si utilizza l'alberatura degli uffici
		if (!isRiservato) {
			// Si calcola la gerarchia
			final List<Long> alberatura = nodoDAO.getAlberaturaUfficio(idNodoCorrente, connection);
			// Si aggiunge la gerarchia di security sui nodi
			for (Long nodoCorrente : alberatura) {
				// Si esclude l'ufficio reale e si inserisce il gruppo corriere
				final SecurityDTO newSecurity = new SecurityDTO();
				final GruppoS newGruppo = newSecurity.getGruppo();
				if (nodoCorrente.equals(idNodoCorrente) && isDocumento) {
					nodoCorrente = idNodoCorriere;
					newGruppo.setIdSiap(-1);
				}
				newGruppo.setIdUfficio(nodoCorrente.intValue());
				newSecurity.setGruppo(newGruppo);
				final UtenteS newUtente = newSecurity.getUtente();
				newUtente.setIdUtente(0);
				newSecurity.setUtente(newUtente);
				// Si aggiunge la security alla lista
				securityList.add(newSecurity);
			}
		} else if (idUtenteCorrente == 0) {
			// Security al corriere
			securityList.add(creaSecurity(idNodoCorrente, 0L, idNodoCorriere));
		}

		// Se l'assegnazione è ad un utente, si crea e inserisce la security puntuale
		// sull'utente
		if (idUtenteCorrente > 0) {
			// Si inserisce l'utente creatore
			securityList.add(creaSecurity(0L, idUtenteCorrente, idNodoCorriere));
		}

		// Se richiesto, si crea e inserisce la security per l'ufficio reale
		if (aggiungiUfficio && !isRiservato) {
			securityList.add(creaSecurity(idNodoCorrente, 0L, 0L));
		}

		return securityList;
	}

	private ListSecurityDTO getStoricoSecurity(final UtenteDTO utente, final String idDocumento, final boolean isDocumento, final boolean aggiungiUfficio,
			final boolean isRiservato, final boolean escludiCreatore, final Connection connection) {
		return getStoricoSecurity(utente, idDocumento, isDocumento, aggiungiUfficio, isRiservato, escludiCreatore, null, connection);
	}

	/**
	 * Recupera storico security.
	 * 
	 * @param utente             credenziali utente
	 * @param idDocumento        identificativo documento
	 * @param isDocumento        identificativi documenti
	 * @param addUfficio         flag aggiungi ufficio
	 * @param isRiservato        flag riservato
	 * @param destinatariInterni lista destinatari interni
	 * @param connection         connessione
	 * @return lista security
	 */
	@Override
	public ListSecurityDTO getStoricoSecurity(final UtenteDTO utente, final String idDocumento, final boolean isDocumento, final boolean addUfficio,
			final boolean isRiservato, final boolean escludiCreatore, final List<String> destinatariInterni, final Connection connection) {
		final ListSecurityDTO riassegnazioniSecurity = new ListSecurityDTO();
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;

		try {
			LOGGER.info(" ######## getStoricoSecurity ########");
			// L'accesso a filenet viene fatto come admin per il recupero dello storico security
			fceh = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()), utente.getIdAoo());
			fpeh = new FilenetPEHelper(new FilenetCredentialsDTO(utente.getFcDTO()));
			final PropertiesProvider pp = PropertiesProvider.getIstance();

			// ##############################################################
			Date d1 = new Date();
			if(!escludiCreatore) {
				final ListSecurityDTO securityCreatore = getStoricoSecurityCreatore(utente, idDocumento, isDocumento, addUfficio, fceh, pp, isRiservato, connection);
				riassegnazioniSecurity.addAll(securityCreatore);
			}
			Date d22 = new Date();
			LOGGER.info("Timing esecuzione SecurityUtilities getStoricoSecurityCreatore ===>>>> " + (d22.getTime() - d1.getTime()));
			// ##############################################################

			// ##############################################################
			// Ottieni le assegnazioni dal roster tranne quella dell'utente che effettua la riassegnazione
			d1 = new Date();
			final String[] assegnazioniArray = fpeh.getAssegnazioniStorico(idDocumento, utente.getFcDTO().getIdClientAoo());
			d22 = new Date();
			LOGGER.info("Timing esecuzione SecurityUtilities pe.getAssegnazioniStorico ===>>>> " + (d22.getTime() - d1.getTime()));

			// Ottieni la lista delle gerarchie delle security
			if (assegnazioniArray != null) {
				for (final String assegnazione : assegnazioniArray) {
					final ListSecurityDTO assegnazioni = getSecurityAssegnazione(assegnazione, isDocumento, addUfficio, isRiservato, connection);
					riassegnazioniSecurity.addAll(assegnazioni);
				}
			}
			// ##############################################################

			// ##############################################################
			if (destinatariInterni != null) {
				for (final String assInt : destinatariInterni) {
					final ListSecurityDTO assegnazioniInterne = getSecurityAssegnazione(assInt, isDocumento, true, isRiservato, connection);
					riassegnazioniSecurity.addAll(assegnazioniInterne);
				}
			}
			// ##############################################################
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero delle security storico: " + e.getMessage(), e);
			throw new FilenetException(e);
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}

		return riassegnazioniSecurity;
	}

	/**
	 * @see it.ibm.red.business.service.ISecuritySRV#getStoricoSecurity(int,
	 *      boolean, it.ibm.red.business.dto.UtenteDTO, boolean,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public ListSecurityDTO getStoricoSecurity(final int idDocumento, final boolean isDocEntrata, final UtenteDTO utente, final boolean isRiservato,
			final IFilenetCEHelper fceh, final Connection con) {
		final ListSecurityDTO securityList = new ListSecurityDTO();

		try {
			// Si recupera lo storico
			final Collection<StoricoDTO> storicoList = storicoSRV.getStorico(idDocumento, utente.getIdAoo().intValue());
			for (final StoricoDTO storico : storicoList) {
				String assegnazione = null;
				// Se l'evento è la creazione del documento, si controlla che l'utente non sia
				// dell'ufficio UCP e che il doc non sia in entrata
				// per aggiungere le security dell'utente
				if (EventTypeEnum.DOCUMENTO.getIntValue().equals(storico.getIdTipoOperazione())) {
					// Si controlla se l'utente appartiene all'UCP
					final boolean isNodoUcp = nodoDAO.isNodoUcp(storico.getIdNodoAssegnante(), con);
					if (!isDocEntrata && !isNodoUcp) {
						assegnazione = storico.getIdNodoAssegnante() + "," + storico.getIdUtenteAssegnante();
						securityList.addAll(getSecurityAssegnazione(assegnazione, true, false, isRiservato, con));
					}
					// per gli altri eventi
				} else if (storico.getIdNodoAssegnatario() != null && storico.getIdNodoAssegnatario() != 0) {
					assegnazione = storico.getIdNodoAssegnatario() + "," + storico.getIdUtenteAssegnatario();
					securityList.addAll(getSecurityAssegnazione(assegnazione, true, false, isRiservato, con));

					// In caso di assegnazione per sigla, visto e firma si aggiungono le security
					// del delegato
					if (EventTypeEnum.ASSEGNAZIONE_FIRMA.getIntValue().equals(storico.getIdTipoOperazione())
							|| EventTypeEnum.ASSEGNAZIONE_SIGLA.getIntValue().equals(storico.getIdTipoOperazione())
							|| EventTypeEnum.ASSEGNAZIONE_VISTO.getIntValue().equals(storico.getIdTipoOperazione())) {
						final List<String> assegnazioneUtentiDelegati = getAssegnazioneUtentiDelegati(assegnazione, con);
						final String[] assegnazioneUtentiDelegatiArray = new String[assegnazioneUtentiDelegati.size()];
						assegnazioneUtentiDelegati.toArray(assegnazioneUtentiDelegatiArray);
						securityList.addAll(getSecurityAssegnazioni(assegnazioneUtentiDelegatiArray, true, false, isRiservato, con));
					}
				}
			}

			if (!securityList.isEmpty()) {
				securityList.setNullableUtente();
			}
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dello storico delle security per il documento: " + idDocumento, e);
		}

		return securityList;
	}

	/**
	 * Metodo per adeguare le security dei documenti allacciati.
	 * 
	 */
	@Override
	public void aggiornaSecurityDocumentiAllacciati(final UtenteDTO utente, final int idDocumentoPrincipale, final ListSecurityDTO securityDocAllacciati,
			final ListSecurityDTO securityFascicoloDocAllacciati, final IFilenetCEHelper fceh, final Connection con) {
		LOGGER.info("aggiornaSecurityDocumentiAllacciati -> START. Si aggiornano le security dei documenti allacciati.");
		final PropertiesProvider pp = PropertiesProvider.getIstance();

		try {
			final Date dataInizio = new Date();
			// Si ottengono tutti i documenti allacciati
			final List<RispostaAllaccioDTO> documentiDaAllacciare = allaccioDAO.getDocumentiRispostaAllaccio(idDocumentoPrincipale, utente.getIdAoo().intValue(), con);

			if (!CollectionUtils.isEmpty(documentiDaAllacciare)) {
				// Loop su tutti i documenti allacciati
				for (final RispostaAllaccioDTO rispostaAllaccio : documentiDaAllacciare) {
					boolean riservato = false;
					final Document documento = fceh.getDocumentByIdGestionale(rispostaAllaccio.getIdDocumentoAllacciato(),
							Arrays.asList(pp.getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY)), null, utente.getIdAoo().intValue(), null,
							pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));

					final Integer metadatoRiservato = (Integer) TrasformerCE.getMetadato(documento, PropertiesNameEnum.RISERVATO_METAKEY);
					if (metadatoRiservato != null && metadatoRiservato == 1) {
						riservato = true;
					}

					if (!riservato) {
						securityFascicoloDocAllacciati.setDeNullableUtente();
						securityDocAllacciati.setDeNullableUtente();

						// ### Fascicolo -> START
						final ListSecurityDTO securityFascAllacciato = getSecurityListFascicoloDocumentoAllacciato(utente,
								rispostaAllaccio.getIdDocumentoAllacciato(), con);
						securityFascAllacciato.addAll(securityFascicoloDocAllacciati);
						securityFascAllacciato.setNullableUtente();
						// Si verificano le security del fascicolo
						verificaSecurities(securityFascAllacciato, con);
						// ### Fascicolo -> END

						// ### Documento -> START
						final ListSecurityDTO securityDocAllacciato = getSecurityListDocumentoAllacciato(utente, rispostaAllaccio.getIdDocumentoAllacciato(),
								false, con);
						securityDocAllacciato.addAll(securityDocAllacciati);
						securityDocAllacciato.setNullableUtente();
						// ### Documento -> END

						LOGGER.info("Adeguamento e modifica delle security del fascicolo per il documento: " + rispostaAllaccio.getIdDocumentoAllacciato());
						// ### Si modificano le security del fascicolo del documento allacciato
						final boolean modificaSecFascicolo = modificaSecurityFascicoli(utente, securityFascAllacciato, rispostaAllaccio.getIdDocumentoAllacciato());

						// Se la modifica delle security del fascicolo è andata a buon fine, si
						// modificano le security del documento allacciato
						if (modificaSecFascicolo) {
							LOGGER.info("Adeguamento e modifica delle security del documento: " + rispostaAllaccio.getIdDocumentoAllacciato());
							// ### Si modificano le security del documento allacciato
							aggiornaEVerificaSecurityDocumento(rispostaAllaccio.getIdDocumentoAllacciato(), utente.getIdAoo(), securityDocAllacciato, true, false, fceh, con);
						}
					}
				}
			} else {
				LOGGER.info("aggiornaSecurityDocumentiAllacciati -> Nessun documento allacciato su cui modificare le security per il documento: " + idDocumentoPrincipale);
			}

			final Date dataFine = new Date();
			LOGGER.info("aggiornaSecurityDocumentiAllacciati -> Timing aggiornamento security doc allacciati (dopo le modifiche) per il documento: " + idDocumentoPrincipale
					+ " ===> " + (dataFine.getTime() - dataInizio.getTime()));
		} catch (final Exception e) {
			LOGGER.error(ERRORE_NELLA_MODIFICA_DELLE_SECURITY_DEI_DOCUMENTI_ALLACCIATI, e);
			throw new RedException(ERRORE_NELLA_MODIFICA_DELLE_SECURITY_DEI_DOCUMENTI_ALLACCIATI, e);
		}

		LOGGER.info("aggiornaSecurityDocumentiAllacciati -> END. Aggiornamento delle security dei documenti allacciati effettuato per il documento: " + idDocumentoPrincipale);
	}

	/**
	 * @see it.ibm.red.business.service.ISecuritySRV#aggiornaSecurityDocumentiAllacciati(it.ibm.red.business.dto.UtenteDTO,
	 *      int, it.ibm.red.business.dto.ListSecurityDTO, java.lang.String[],
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public void aggiornaSecurityDocumentiAllacciati(final UtenteDTO utente, final int idDocumentoPrincipale, final ListSecurityDTO securityDocAllacciati,
			final String[] assegnazioni, final IFilenetCEHelper fceh, final Connection con) {
		LOGGER.info("aggiornaSecurityDocumentiAllacciati -> START. Si aggiornano le security dei documenti allacciati al documento: " + idDocumentoPrincipale);
		final PropertiesProvider pp = PropertiesProvider.getIstance();

		try {
			final Date dataInizio = new Date();
			// Si ottengono tutti i documenti allacciati
			final List<RispostaAllaccioDTO> documentiDaAllacciare = allaccioDAO.getDocumentiRispostaAllaccio(idDocumentoPrincipale, utente.getIdAoo().intValue(), con);

			if (!CollectionUtils.isEmpty(documentiDaAllacciare)) {
				// Loop su tutti i documenti allacciati
				for (final RispostaAllaccioDTO rispostaAllaccio : documentiDaAllacciare) {
					boolean riservato = false;
					final Document documento = fceh.getDocumentByIdGestionale(rispostaAllaccio.getIdDocumentoAllacciato(),
							Arrays.asList(pp.getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY)), null, utente.getIdAoo().intValue(), null,
							pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));

					final Integer metadatoRiservato = (Integer) TrasformerCE.getMetadato(documento, PropertiesNameEnum.RISERVATO_METAKEY);
					if (metadatoRiservato != null && metadatoRiservato == 1) {
						riservato = true;
					}

					if (!riservato) {
						LOGGER.info("Adeguamento e modifica delle security del fascicolo per il documento: " + rispostaAllaccio.getIdDocumentoAllacciato());
						// ### Si modificano le security del fascicolo del documento allacciato
						final boolean modificaSecFascicolo = modificaSecurityFascicoli(rispostaAllaccio.getIdDocumentoAllacciato(), utente, assegnazioni, con);

						// Se la modifica delle security del fascicolo è andata a buon fine, si
						// modificano le security del documento allacciato
						if (modificaSecFascicolo) {
							LOGGER.info("Adeguamento e modifica delle security del documento: " + rispostaAllaccio.getIdDocumentoAllacciato());
							// ### Si modificano le security del documento allacciato
							aggiornaEVerificaSecurityDocumento(rispostaAllaccio.getIdDocumentoAllacciato(), utente.getIdAoo(), securityDocAllacciati, true, false, fceh, con);
						}
					}
				}
			} else {
				LOGGER.info("aggiornaSecurityDocumentiAllacciati -> Nessun documento allacciato su cui modificare le security per il documento: " + idDocumentoPrincipale);
			}

			final Date dataFine = new Date();
			LOGGER.info("aggiornaSecurityDocumentiAllacciati -> Timing aggiornamento security doc allacciati (dopo le modifiche) per il documento: " + idDocumentoPrincipale
					+ " ===> " + (dataFine.getTime() - dataInizio.getTime()));
		} catch (final Exception e) {
			LOGGER.error(ERRORE_NELLA_MODIFICA_DELLE_SECURITY_DEI_DOCUMENTI_ALLACCIATI, e);
			throw new RedException(ERRORE_NELLA_MODIFICA_DELLE_SECURITY_DEI_DOCUMENTI_ALLACCIATI, e);
		}

		LOGGER.info("aggiornaSecurityDocumentiAllacciati -> END. Aggiornamento delle security dei documenti allacciati effettuato per il documento: " + idDocumentoPrincipale);
	}

	/**
	 * @see it.ibm.red.business.service.ISecuritySRV#aggiornaSecurityContributiInseriti(
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.String, java.lang.String,
	 *      java.lang.String, it.ibm.red.business.helper.filenet.ce.IFilenetHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public void aggiornaSecurityContributiInseriti(final UtenteDTO utente, final String idDocumento, final String idUtenteDestinatario, final String idNodoDestinatario,
			final IFilenetCEHelper fceh, final Connection connection) {
		final String[] assegnazioni = new String[1];
		assegnazioni[0] = idNodoDestinatario + "," + idUtenteDestinatario;

		aggiornaSecurityContributiInseriti(utente, idDocumento, assegnazioni, fceh, connection);
	}

	/**
	 * @see it.ibm.red.business.service.ISecuritySRV#aggiornaSecurityContributiInseriti(
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.String, java.lang.String[],
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public void aggiornaSecurityContributiInseriti(final UtenteDTO utente, final String idDocumento, final String[] assegnazioni, final IFilenetCEHelper fceh,
			final Connection connection) {
		LOGGER.info("aggiornaSecurityContributiInseriti -> START. Documento: " + idDocumento);

		try {
			aggiornaSecurityContributiInterni(utente, idDocumento, assegnazioni, fceh, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante la modifica delle security dei contributi.", e);
			throw new RedException("Errore durante la modifica delle security dei contributi.", e);
		}

		LOGGER.info("aggiornaSecurityContributiInseriti -> END. Documento: " + idDocumento);
	}

	/**
	 * @see it.ibm.red.business.service.ISecuritySRV#aggiornaSecurityContributiInterni(it
	 *      .ibm.red.business.dto.UtenteDTO, java.lang.String, java.lang.String[],
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public void aggiornaSecurityContributiInterni(final UtenteDTO utente, final String idDocumentoPrincipale, final String[] assegnazioni, final IFilenetCEHelper fceh,
			final Connection con) {
		LOGGER.info("aggiornaSecurityContributiInterni -> START. Aggiorna le security dei contributi interni per il documento: " + idDocumentoPrincipale);

		// Si ottengono tutti i contributi interni
		final Collection<Contributo> contributi = contributoDAO.getContributiInterni(idDocumentoPrincipale, utente.getIdAoo(), con);

		if (!CollectionUtils.isEmpty(contributi)) {
			// Loop su tutti i documenti allacciati
			for (final Contributo contributo : contributi) {

				// Si verifica se è stato inserito su FileNet
				if (StringUtils.isNotBlank(contributo.getGuid())) {

					// Si modificano le security sul documento Contributo
					LOGGER.info("aggiornaSecurityContributiInterni -> Adeguamento e modifica delle security del documento: " + contributo.getGuid());
					final ListSecurityDTO security = getSecurityPerRiassegnazione(utente, idDocumentoPrincipale, assegnazioni, null, false, true, false, false, con);
					aggiornaEVerificaSecurityDocumento(contributo.getGuid(), utente.getIdAoo(), security, true, false, fceh, con);
				}

			}
			LOGGER.info("aggiornaSecurityContributiInterni -> Aggiornamento delle security dei contributi interni effettuato per il documento: " + idDocumentoPrincipale);
		} else {
			LOGGER.info("aggiornaSecurityContributiInterni -> Nessun contributo interno su cui modificare le security per il documento: " + idDocumentoPrincipale);
		}

		LOGGER.info("aggiornaSecurityContributiInterni -> END. Documento: " + idDocumentoPrincipale);
	}

	/**
	 * @see it.ibm.red.business.service.ISecuritySRV#aggiornaEVerificaSecurityDocumento(
	 *      java.lang.String, java.lang.Long,
	 *      it.ibm.red.business.dto.ListSecurityDTO, boolean, boolean,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public void aggiornaEVerificaSecurityDocumento(final String idDocumento, final Long idAoo, final ListSecurityDTO security, final boolean allVersion,
			final boolean securityToOverride, final IFilenetCEHelper fceh, final Connection con) {

		// Verifica delle security
		verificaSecurities(security, con);

		aggiornaSecurityDocumento(idDocumento, idAoo, security, allVersion, securityToOverride, fceh);
	}

	/**
	 * @see it.ibm.red.business.service.ISecuritySRV#aggiornaSecurityDocumento(java.lang.String,
	 *      java.lang.Long, com.filenet.api.collection.AccessPermissionList,
	 *      boolean, boolean,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public void aggiornaSecurityDocumento(final String idDocumento, final Long idAoo, final AccessPermissionList acl, final boolean allVersion,
			final boolean securityToOverride, final IFilenetCEHelper fceh) {
		aggiornaSecurityDocumento(idDocumento, idAoo, acl, null, allVersion, securityToOverride, fceh);
	}

	/**
	 * ATTENZIONE : leggere il javadoc sull'interfaccia per evitare errori di
	 * FileNet.
	 * 
	 * @see it.ibm.red.business.service.ISecuritySRV#aggiornaSecurityDocumento(java.lang.
	 *      String, java.lang.Long, it.ibm.red.business.dto.ListSecurityDTO,
	 *      boolean, boolean, it.ibm.red.business.helper.filenet.ce.IFilenetHelper)
	 */
	@Override
	public void aggiornaSecurityDocumento(final String idDocumento, final Long idAoo, final ListSecurityDTO security, final boolean allVersion,
			final boolean securityToOverride, final IFilenetCEHelper fceh) {
		final AccessPermissionList acl = Factory.AccessPermission.createList();

		aggiornaSecurityDocumento(idDocumento, idAoo, acl, security, allVersion, securityToOverride, fceh);
	}

	/**
	 * ATTENZIONE : leggere il javadoc sull'interfaccia per evitare errori di
	 * FileNet (non-Javadoc).
	 * 
	 * @see it.ibm.red.business.service.ISecuritySRV#aggiornaSecurityDocumento(java.lang.
	 *      String, java.lang.Long, it.ibm.red.business.dto.ListSecurityDTO,
	 *      boolean, boolean, it.ibm.red.business.helper.filenet.ce.IFilenetHelper).
	 */
	private void aggiornaSecurityDocumento(final String idDocumento, final Long idAoo, final AccessPermissionList acl, final ListSecurityDTO security,
			final boolean allVersion, final boolean securityToOverride, final IFilenetCEHelper fceh) {
		LOGGER.info("aggiornaSecurityDocumento -> START. Documento: " + idDocumento);

		// Si aggiornano le security del documento su FileNet
		final List<IndependentlyPersistableObject> docs = new ArrayList<>(INITIAL_CAPACITY);

		if (acl != null) {
			// Si aggiungono le security alle ACL
			fceh.addSecurity(acl, security, 0);

			final Document doc = fceh.getDocumentByIdGestionale(idDocumento, Arrays.asList(PropertyNames.ID, PropertyNames.PERMISSIONS, PropertyNames.VERSION_SERIES), null,
					idAoo.intValue(), null, null);

			if (doc == null) {
				throw new FilenetException("Documento: " + idDocumento + " non trovato su FileNet.");
			}

			if (allVersion) {
				final Iterator<?> iter = doc.get_VersionSeries().get_Versions().iterator();
				while (iter.hasNext()) {
					final Document d = (Document) iter.next();
					if (securityToOverride) {
						fceh.sovrascriviSecurity(d.get_Permissions(), acl);
					}
					d.get_Permissions().addAll(acl);
					docs.add(d);
				}
			} else {
				if (securityToOverride) {
					fceh.sovrascriviSecurity(doc.get_Permissions(), acl);
				}
				doc.get_Permissions().addAll(acl);
				docs.add(doc);
			}
			LOGGER.info(docs.size() + " versioni del documento principale da aggiornare.");

			final DocumentSet ds = fceh.getAllegatiByGuid(doc.get_Id(), Arrays.asList(PropertyNames.PERMISSIONS, PropertyNames.VERSION_SERIES));
			if (ds != null) {
				final Iterator<?> it = ds.iterator();
				while (it.hasNext()) {
					final Document allegato = (Document) it.next();
					if (allVersion) {
						final Iterator<?> iterAllegati = allegato.get_VersionSeries().get_Versions().iterator();
						while (iterAllegati.hasNext()) {
							final Document allegatoV = (Document) iterAllegati.next();
							if (securityToOverride) {
								fceh.sovrascriviSecurity(allegatoV.get_Permissions(), acl);
							}
							allegatoV.get_Permissions().addAll(acl);
							docs.add(allegatoV);
						}
					} else {
						if (securityToOverride) {
							fceh.sovrascriviSecurity(allegato.get_Permissions(), acl);
						}
						allegato.get_Permissions().addAll(acl);
						docs.add(allegato);
					}
				}
			}
			LOGGER.info(docs.size() + " documenti totali (tutte le versioni degli allegati comprese) da aggiornare.");
			fceh.batchSave(docs);
		}
		LOGGER.info("aggiornaSecurityDocumento -> END. " + docs.size() + " documenti totali (tutte le versioni degli allegati comprese) aggiornate.");
	}

	/**
	 * Ottieni la lista di security per il fascicolo riferito al documento.
	 * 
	 * @param utente
	 * @param idDocumento
	 * @param con
	 * @return
	 */
	private ListSecurityDTO getSecurityListFascicoloDocumentoAllacciato(final UtenteDTO utente, final String idDocumento, final Connection con) {
		ListSecurityDTO riassegnazioniSecurity;

		// Si ottiene lo storico security per fascicolo sul documento
		riassegnazioniSecurity = getStoricoSecurity(utente, idDocumento, false, false, false, false, null, con);
		LOGGER.info("Security per il fascicolo per il documento: " + idDocumento + " generate con successo.");

		return riassegnazioniSecurity;
	}

	/**
	 * Ottieni la lista di security per il documento allacciato.
	 * 
	 * @param utente
	 * @param idDocumento
	 * @param con
	 * @return
	 */
	private ListSecurityDTO getSecurityListDocumentoAllacciato(final UtenteDTO utente, final String idDocumento, final boolean isRiservato,
			final Connection con) {
		ListSecurityDTO riassegnazioniSecurity;

		// Si ottiene lo storico security per fascicolo sul documento
		riassegnazioniSecurity = getStoricoSecurity(utente, idDocumento, true, false, isRiservato, false, null, con);
		LOGGER.info("Security per il documento: " + idDocumento + " generate con successo.");

		return riassegnazioniSecurity;
	}

	/**
	 * Recupera security creatore.
	 * 
	 * @param utente      credenziali utente
	 * @param idDocumento identificativo documento
	 * @param isDocumento identificativo documenti
	 * @param addUfficio  flag aggiungi ufficio
	 * @param fceh        helper ce
	 * @param pp          property di sistema
	 * @param isRiservato flag riservato
	 * @param connection  connessione
	 * @return lista security
	 */
	private ListSecurityDTO getStoricoSecurityCreatore(final UtenteDTO utente, final String idDocumento, final boolean isDocumento, final boolean addUfficio,
			final IFilenetCEHelper fceh, final PropertiesProvider pp, final boolean isRiservato, final Connection connection) {
		// ##############################################################
		// Ottieni la gerarchia di security dell'utente creatore del documento

		final String idUtenteCreatoreMK = pp.getParameterByKey(PropertiesNameEnum.UTENTE_CREATORE_ID_METAKEY);
		final String idUfficioCreatoreMK = pp.getParameterByKey(PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY);

		final PropertyFilter pf = new PropertyFilter();
		pf.addIncludeProperty(new FilterElement(null, null, null, idUtenteCreatoreMK, null));
		pf.addIncludeProperty(new FilterElement(null, null, null, idUfficioCreatoreMK, null));

		final Date d11 = new Date();
		// Ottieni il documento
		final Document doc = fceh.getDocumentByIdGestionale(idDocumento, Arrays.asList(idUtenteCreatoreMK, idUfficioCreatoreMK), pf, utente.getIdAoo().intValue(), null,
				pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));
		final Date d22 = new Date();
		LOGGER.info("Timing esecuzione SecurityUtilities getStoricoSecurityCreatore getDocumentByIdGestionale ====>>>> " + (d22.getTime() - d11.getTime()));

		final Properties prop = doc.getProperties();
		final String idUtenteCreatore = prop.getInteger32Value(idUtenteCreatoreMK) + "";
		final String idNodoCreatore = prop.getInteger32Value(idUfficioCreatoreMK) + "";
		LOGGER.info("idNodoCreatore: " + idNodoCreatore + " idUtenteCreatore: " + idUtenteCreatore);
		final String assegnazione = prop.getInteger32Value(idUfficioCreatoreMK) + "," + prop.getInteger32Value(idUtenteCreatoreMK);

		// Ottieni la lista delle gerarchie delle security
		return getSecurityAssegnazione(assegnazione, isDocumento, addUfficio, isRiservato, connection);
	}

	/**
	 * @see it.ibm.red.business.service.ISecuritySRV#getSecurityPerRiassegnazioneDocumento(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.Long, java.lang.Long, boolean,
	 *      java.sql.Connection).
	 */
	@Override
	public ListSecurityDTO getSecurityPerRiassegnazioneDocumento(final UtenteDTO utente, final String idDocumento, final Long idUtenteDestinatario,
			final Long idUfficioDestinatario, final boolean isRiservato, final Connection con) {
		return getSecurityPerRiassegnazione(utente, idDocumento, idUtenteDestinatario, idUfficioDestinatario, null, false, true, isRiservato, false, con);
	}

	/**
	 * @see it.ibm.red.business.service.ISecuritySRV#getSecurityPerRiassegnazioneFascicolo(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String[], java.lang.String, boolean,
	 *      java.sql.Connection).
	 */
	@Override
	public ListSecurityDTO getSecurityPerRiassegnazioneFascicolo(final UtenteDTO utente, final String idDocumento, final String[] assegnazioni,
			final String coordinatore, final boolean aggiungiDelegato, final Connection con) {
		return getSecurityPerRiassegnazione(utente, idDocumento, assegnazioni, coordinatore, aggiungiDelegato, false, false, false, con);
	}

	/**
	 * @see it.ibm.red.business.service.ISecuritySRV#getSecurityPerRiassegnazioneFascicolo(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.Long, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public ListSecurityDTO getSecurityPerRiassegnazioneFascicolo(final UtenteDTO utente, final String idDocumento, final Long idUtenteDestinatario,
			final Long idUfficioDestinatario, final Connection con) {
		return getSecurityPerRiassegnazione(utente, idDocumento, idUtenteDestinatario, idUfficioDestinatario, null, false, false, false, false, con);
	}

	/**
	 * Recupero di una lista security.
	 * 
	 * @param utente
	 * @param idDocumento
	 * @param idUtenteDestinatario
	 * @param idUfficioDestinatario
	 * @param coordinatore
	 * @param con
	 * @return
	 */
	@Override
	public ListSecurityDTO getSecurityPerRiassegnazioneFascicolo(final UtenteDTO utente, final String idDocumento, final Long idUtenteDestinatario,
			final Long idUfficioDestinatario, final String coordinatore, final Connection con) {
		return getSecurityPerRiassegnazione(utente, idDocumento, idUtenteDestinatario, idUfficioDestinatario, coordinatore, false, false, false, false, con);
	}

	/**
	 * @see it.ibm.red.business.service.ISecuritySRV#getSecurityPerRiassegnazione(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.Long, java.lang.Long, java.lang.String,
	 *      boolean, boolean, boolean, boolean, java.sql.Connection).
	 */
	@Override
	public ListSecurityDTO getSecurityPerRiassegnazione(final UtenteDTO utente, final String idDocumento, final Long inIdUtenteDestinatario,
			final Long inIdUfficioDestinatario, final String coordinatore, final boolean aggiungiDelegato, final boolean isDocumento, final boolean isRiservato,
			final boolean escludiCreatore, final Connection con) {
		Long idUfficioDestinatario = inIdUfficioDestinatario;
		final String[] assegnazioni = new String[1];
		if (idUfficioDestinatario == null) {
			idUfficioDestinatario = 0L;
		}
		Long idUtenteDestinatario = inIdUtenteDestinatario;
		if (idUtenteDestinatario == null) {
			idUtenteDestinatario = 0L;
		}
		assegnazioni[0] = Long.toString(idUfficioDestinatario) + "," + Long.toString(idUtenteDestinatario);
		return getSecurityPerRiassegnazione(utente, idDocumento, assegnazioni, coordinatore, aggiungiDelegato, isDocumento, isRiservato, escludiCreatore, con);
	}

	/**
	 * @see it.ibm.red.business.service.ISecuritySRV#getSecurityPerRiassegnazioneDocumento(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String[], boolean, java.sql.Connection).
	 */
	@Override
	public ListSecurityDTO getSecurityPerRiassegnazioneDocumento(final UtenteDTO utente, final String idDocumento, final String[] assegnazioni,
			final boolean isRiservato, final Connection con) {
		return getSecurityPerRiassegnazione(utente, idDocumento, assegnazioni, null, false, true, isRiservato, false, con);
	}

	/**
	 * @see it.ibm.red.business.service.ISecuritySRV#getSecurityPerRiassegnazione(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String[], java.lang.String, boolean,
	 *      boolean, boolean, boolean, java.sql.Connection).
	 */
	@Override
	public ListSecurityDTO getSecurityPerRiassegnazione(final UtenteDTO utente, final String idDocumento, final String[] assegnazioni, final String coordinatore,
			final boolean aggiungiDelegato, final boolean isDocumento, final boolean isRiservato, final boolean escludiCreatore, final Connection con) {
		LOGGER.info("getSecurityPerRiassegnazione -> START. Documento: " + idDocumento);
		ListSecurityDTO riassegnazioniSecurity;

		// Si ottiene lo storico security sul documento
		Date dataInizio = new Date();
		riassegnazioniSecurity = getStoricoSecurity(utente, idDocumento, isDocumento, false, isRiservato, escludiCreatore, con);
		Date dataFine = new Date();
		LOGGER.info("Timing getStoricoSecurity ===> " + (dataFine.getTime() - dataInizio.getTime()));

		// Si aggiunge la gerarchia della nuova competenza
		// Si ottiene la lista delle gerarchie delle security
		dataInizio = new Date();
		final ListSecurityDTO securityDestinatario = getSecurityAssegnazioni(assegnazioni, isDocumento, false, isRiservato, con);
		dataFine = new Date();
		LOGGER.info("Timing getSecurityAssegnazioni ===> " + (dataFine.getTime() - dataInizio.getTime()));
		riassegnazioniSecurity.addAll(securityDestinatario);

		// R.C. Nuove Richieste Marzo 2013
		// Si aggiunge il coordinatore nello step se esiste per l'ufficio
		if (StringUtils.isNotBlank(coordinatore)) {
			final ListSecurityDTO securityDestinatarioCoordinatore = getSecurityAssegnazione(coordinatore, isDocumento, false, isRiservato, con);
			riassegnazioniSecurity.addAll(securityDestinatarioCoordinatore);
		}

		// R.C. Nuove Richieste Marzo 2013
		// Si aggiunge il delegato nelle security se previsto per l'ufficio
		if (aggiungiDelegato) {
			final String[] assegnazioniUtentiDelegati = getAssegnazioniUtentiDelegati(assegnazioni, con);
			final ListSecurityDTO securityDestinatarioDelegato = getSecurityAssegnazioni(assegnazioniUtentiDelegati, isDocumento, false, isRiservato, con);
			riassegnazioniSecurity.addAll(securityDestinatarioDelegato);
		}

		riassegnazioniSecurity.setNullableUtente();

		// Si verificano le security impostando l'utente
		verificaSecurities(riassegnazioniSecurity, con);

		LOGGER.info("Security del fascicolo generate con successo per la riassegnazione del documento: " + idDocumento);
		LOGGER.info("getSecurityPerRiassegnazione -> END. Documento: " + idDocumento);
		return riassegnazioniSecurity;
	}

	/**
	 * Metodo che costruisce le security per il contributo partendo dal documento
	 * collegato.
	 * 
	 * @param utente
	 * @param idDocumento
	 * @param isRiservato
	 * @param con
	 * @return
	 */
	@Override
	public ListSecurityDTO getSecurityPerContributo(final UtenteDTO utente, final String idDocumento, final boolean isRiservato, final Connection con) {
		ListSecurityDTO security;

		// Si ottiene lo storico delle security sul documento
		security = getStoricoSecurity(utente, idDocumento, false, true, isRiservato, false, con);

		security.setNullableUtente();

		// Si verificano le security
		verificaSecurities(security, con);

		LOGGER.info("Security per inserimento contributo generate con successo: " + security);
		return security;
	}

	/**
	 * Ottieni le security per la riassegnazione.
	 * 
	 * @param utente
	 * @param idDocumento
	 * @param idUtenteDestinatario [utente loggato]
	 * @param idNodoDestinatario   [ufficio loggato]
	 * @param isRiservato
	 * @param con
	 * @return
	 */
	@Override
	public ListSecurityDTO getSecurityPerAggiornamento(final UtenteDTO utente, final String idDocumento, final String idUtenteDestinatario,
			final String idNodoDestinatario, final boolean isRiservato, final Connection con) {
		LOGGER.info("getSecurityPerAggiornamento -> START. Documento: " + idDocumento);
		ListSecurityDTO riassegnazioniSecurity;

		// Si ottiene lo storico security sul documento
		riassegnazioniSecurity = getStoricoSecurity(utente, idDocumento, true, false, isRiservato, false, con);

		// Si aggiunge la gerarchia dell'utente escluso nello storico
		final String nuovaAssegnazione = idNodoDestinatario + "," + (idUtenteDestinatario.equals(Constants.EMPTY_STRING) ? "0" : idUtenteDestinatario);

		// Si ottiene la lista delle gerarchie delle security
		final ListSecurityDTO securityDestinatario = getSecurityAssegnazione(nuovaAssegnazione, true, false, isRiservato, con);
		riassegnazioniSecurity.addAll(securityDestinatario);

		riassegnazioniSecurity.setNullableUtente();

		// Si verificano le security
		verificaSecurities(riassegnazioniSecurity, con);

		LOGGER.info("getSecurityPerAggiornamento -> END. Security per aggiornamento generate con successo per il documento: " + idDocumento);
		return riassegnazioniSecurity;
	}

	/**
	 * @see it.ibm.red.business.service.ISecuritySRV#getSecurityDopoBozza(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String[], java.lang.String[],
	 *      java.lang.String[], java.lang.String[], java.lang.String[],
	 *      java.lang.String[], java.lang.String[], boolean, int, java.lang.Boolean,
	 *      java.lang.String, java.lang.Boolean, java.lang.String[],
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public ListSecurityDTO getSecurityDopoBozza(final UtenteDTO utente, final String idDocumento, final String[] assegnazioniCompetenza,
			final String[] assegnazioniConoscenza, final String[] assegnazioniContributo, final String[] assegnazioniFirma, final String[] assegnazioniSigla,
			final String[] assegnazioniVisto, final String[] assegnazioniFirmaMultipla, final boolean isDocumento, final int idIterAutomatico, final Boolean isRiservato,
			final String assegnazioneCoordinatore, final Boolean aggiungiDelegato, final String[] destinatariInterni, final IFilenetCEHelper fceh, final Connection con) {
		LOGGER.info("getSecurityDopoBozza -> START. Documento: " + idDocumento);
		// Lista delle security
		final ListSecurityDTO securityList = new ListSecurityDTO();

		try {
			// ### Si inserisce nella gerarchia il creatore
			final ListSecurityDTO securityListCreatore = getStoricoSecurityCreatore(utente, idDocumento, isDocumento, false, fceh,
					PropertiesProvider.getIstance(), isRiservato, con);
			securityList.addAll(securityListCreatore);

			// ### Si inseriscono nella gerarchia tutti i destinatari
			final ListSecurityDTO destinatari = getSecurityDestinatari(idDocumento, utente, assegnazioniCompetenza, assegnazioniConoscenza,
					assegnazioniContributo, assegnazioniFirma, assegnazioniSigla, assegnazioniVisto, assegnazioniFirmaMultipla, isDocumento, idIterAutomatico, isRiservato,
					assegnazioneCoordinatore, aggiungiDelegato, destinatariInterni, con);
			securityList.addAll(destinatari);

			securityList.setNullableUtente();

			// Si verificano le security
			verificaSecurities(securityList, con);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il calcolo delle security per il documento: " + idDocumento, e);
			throw new RedException("Si è verificato un errore durante il calcolo delle security per il documento: " + idDocumento, e);
		}

		LOGGER.info("getSecurityDopoBozza -> END. Security generate con successo per il documento: " + idDocumento);
		return securityList;
	}

	/**
	 * Gets the assegnazione utenti delegati.
	 *
	 * @param assegnazione the assegnazione
	 * @param connection   the connection
	 * @return the assegnazione utente delegato
	 */
	@Override
	public final List<String> getAssegnazioneUtentiDelegati(final String assegnazione, final Connection connection) {
		// Lista delle security
		final List<String> securityList = new ArrayList<>();

		if (StringUtils.isNotBlank(assegnazione)) {
			final String[] splittedValue = assegnazione.split(",");
			final int idNodoCorrente = Integer.parseInt(splittedValue[0]);
			final int idUtenteCorrente = Integer.parseInt(splittedValue[1]);
			// Si aggiunge solo se l'assegnazione è verso l'utente
			if (idUtenteCorrente > 0) {
				// Si ottengono tutti i delegati dell'ufficio
				final List<Utente> utentiDelegati = utenteDAO.getUtentiDelegati(idNodoCorrente, connection);
				for (final Utente utente : utentiDelegati) {
					String assegnazioneDelegato = "";
					assegnazioneDelegato = idNodoCorrente + "," + utente.getIdUtente();
					securityList.add(assegnazioneDelegato);
				}
			}
		}

		return securityList;
	}

	/**
	 * Gets the assegnazioni utenti delegati.
	 *
	 * @param assegnazioniPerDelegato the assegnazioni per delegato
	 * @param con                     the con
	 * @return the assegnazioni utente delegato
	 */
	@Override
	public final String[] getAssegnazioniUtentiDelegati(final String[] assegnazioniPerDelegato, final Connection con) {
		// Lista delle assegnazioni
		final List<String> securityList = new ArrayList<>();

		if (assegnazioniPerDelegato != null && assegnazioniPerDelegato.length > 0) {
			// Loop sulle assegnazioni
			for (int i = 0; i < assegnazioniPerDelegato.length; i++) {
				// Si aggiungono le assegnazioni
				securityList.addAll(getAssegnazioneUtentiDelegati(assegnazioniPerDelegato[i], con));
			}
		}

		final String[] array = new String[securityList.size()];
		return securityList.toArray(array);
	}

	/**
	 * Gets the security assegnazioni.
	 *
	 * @param assegnazioniArray the assegnazioni array
	 * @param isDocumento       the is documento
	 * @param addUfficio        the add ufficio
	 * @param isRiservato       the is riservato
	 * @param con               the con
	 * @return the security assegnazioni
	 */
	@Override
	public final ListSecurityDTO getSecurityAssegnazioni(final String[] assegnazioniArray, final boolean isDocumento, final boolean addUfficio,
			final boolean isRiservato, final Connection con) {
		LOGGER.info("getSecurityAssegnazioni -> START");
		// Lista delle security
		final ListSecurityDTO securityList = new ListSecurityDTO();

		if (assegnazioniArray != null && assegnazioniArray.length > 0) {
			ListSecurityDTO subSecurityList = null;
			// loop sulle assegnazioni
			for (int i = 0; i < assegnazioniArray.length; i++) {
				subSecurityList = getSecurityAssegnazione(assegnazioniArray[i], isDocumento, addUfficio, isRiservato, con);
				securityList.addAll(subSecurityList);
			}
		}

		LOGGER.info("getSecurityAssegnazioni END");
		return securityList;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISecurityFacadeSRV#getACLDocRiservatoPerRicercaAvanzata(java.util.List,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public String getACLDocRiservatoPerRicercaAvanzata(final List<AssegnazioneDTO> assList, final IFilenetCEHelper fceh, final Connection connection) {
		LOGGER.info("getACLFromSecurity -> START");
		// Lista delle security
		final ListSecurityDTO securityList = new ListSecurityDTO();
		final Map<Long, Long> idUfficioIdNodoCorriere = new HashMap<>();

		if (assList != null && !assList.isEmpty()) {
			// loop sulle assegnazioni
			for (final AssegnazioneDTO ass : assList) {

				final Long idNodoCorrente = ass.getUfficio() != null && ass.getUfficio().getId() != null ? ass.getUfficio().getId() : 0L;

				final Long idUtenteCorrente = ass.getUtente() != null && ass.getUtente().getId() != null ? ass.getUtente().getId() : 0L;

				// Si ottiene il nodo corriere
				Long idNodoCorriere = null;

				if (idUfficioIdNodoCorriere.containsKey(idNodoCorrente)) {
					idNodoCorriere = idUfficioIdNodoCorriere.get(idNodoCorrente);
				} else {
					idNodoCorriere = nodoDAO.getNodoCorriereId(idNodoCorrente, connection);
					idUfficioIdNodoCorriere.put(idNodoCorrente, idNodoCorriere);

				}

				if (idUtenteCorrente == 0) {
					// Security al corriere
					final SecurityDTO secDTO = creaSecurity(idNodoCorrente, 0L, idNodoCorriere);
					securityList.add(secDTO);
				}
				// Se l'assegnazione è ad un utente, si crea e inserisce la security puntuale
				// sull'utente
				if (idUtenteCorrente > 0) {
					// Si inserisce l'utente creatore
					final SecurityDTO secDTO = creaSecurity(0L, idUtenteCorrente, idNodoCorriere);
					if (ass.getUtente().getUsername() != null && !ass.getUtente().getUsername().trim().isEmpty()) {
						secDTO.getUtente().setUsername(ass.getUtente().getUsername());
					}
					securityList.add(secDTO);
				}

			}
		}

		final StringBuilder acl = new StringBuilder("");

		if (securityList != null && !securityList.isEmpty()) {
			try {
				for (final SecurityDTO security : securityList) {
					final GruppoS gruppo = security.getGruppo();
					final UtenteS utente = security.getUtente();
					final String granteeName = getGranteeName(fceh, gruppo, utente);

					acl.append(granteeName + ",");
				}
				if (acl.length() > 0) {
					acl.setLength(acl.length() - 1);
					LOGGER.info("getACLFromSecurity -> ACL recuperate.");
				}
			} catch (final Exception e) {
				LOGGER.error("Errore durante il recupero delle ACL.", e);
			}
		}

		LOGGER.info("getACLFromSecurity -> END");
		return acl.toString();

	}

	/**
	 * @see it.ibm.red.business.service.ISecuritySRV#getAclFromSecurity(java.util.List,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public String getAclFromSecurity(final List<SecurityDTO> securityList, final IFilenetCEHelper fceh) {
		final StringBuilder acl = new StringBuilder("");

		if (securityList != null && !securityList.isEmpty()) {
			try {
				for (final SecurityDTO security : securityList) {
					final GruppoS gruppo = security.getGruppo();
					final UtenteS utente = security.getUtente();
					getGranteeName(fceh, gruppo, utente);
				}
				if (acl.length() > 0) {
					acl.setLength(acl.length() - 1);
					LOGGER.info("getACLFromSecurity -> ACL recuperate.");
				}
			} catch (final Exception e) {
				LOGGER.error("Errore durante il recupero delle ACL.", e);
			}
		}
		return acl.toString();
	}

	/**
	 * @param fceh
	 * @param gruppo
	 * @param utente
	 */
	private static String getGranteeName(final IFilenetCEHelper fceh, final GruppoS gruppo, final UtenteS utente) {
		String granteeName = null;

		if (utente != null && utente.getIdUtente() > 0 && utente.getUsername() != null) {
			try {
				granteeName = fceh.getADUsernameName(utente.getUsername());
			} catch (final Exception e) {
				// Se non è riuscito a trovare l'username passato
				LOGGER.warn(e);
				granteeName = fceh.getADGroupNameFormId(Integer.toString(gruppo.getIdUfficio()));
			}
		} else {
			granteeName = fceh.getADGroupNameFormId(Integer.toString(gruppo.getIdUfficio()));
		}

		return granteeName;
	}

	/**
	 * @see it.ibm.red.business.service.ISecuritySRV#getSecurityByGruppo(java.util.List).
	 */
	@Override
	public ListSecurityDTO getSecurityByGruppo(final List<Nodo> organigramma) {
		final ListSecurityDTO securityList = new ListSecurityDTO();

		for (final Nodo nodoOrganigramma : organigramma) {
			final SecurityDTO security = new SecurityDTO();
			final GruppoS g = security.getGruppo();
			g.setIdUfficio(nodoOrganigramma.getIdNodo().intValue());
			securityList.add(security);
		}

		return securityList;
	}

	/**
	 * @see it.ibm.red.business.service.ISecuritySRV#getSecurityPuntuale(java.lang.Long, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public ListSecurityDTO getSecurityPuntuale(final Long idUfficio, final Long idUtente, final Connection con) {
		final ListSecurityDTO securityList = new ListSecurityDTO();

		// Si ottiene il nodo corriere
		final Long idNodoCorriere = nodoDAO.getNodoCorriereId(idUfficio, con);

		SecurityDTO newSecurity = null;
		GruppoS newGruppo = null;
		UtenteS newUtente = null;

		// Se l'assegnazione è ad un utente, si inserisce la security puntuale
		// sull'utente
		if (idUtente > 0) {
			// Si inserisce l'utente creatore
			newSecurity = new SecurityDTO();
			newGruppo = newSecurity.getGruppo();
			newGruppo.setIdUfficio(idNodoCorriere.intValue());
			newSecurity.setGruppo(newGruppo);
			newUtente = newSecurity.getUtente();
			newUtente.setIdUtente(idUtente.intValue());
			newSecurity.setUtente(newUtente);
			// Si aggiunge la security alla lista
			securityList.add(newSecurity);
		} else {
			newSecurity = new SecurityDTO();
			newGruppo = newSecurity.getGruppo();
			newGruppo.setIdUfficio(idUfficio.intValue());
			newSecurity.setGruppo(newGruppo);
			newUtente = newSecurity.getUtente();
			newUtente.setIdUtente(0);
			newSecurity.setUtente(newUtente);
			// Si aggiunge la security alla lista
			securityList.add(newSecurity);
		}

		printSecurityList(securityList);

		securityList.setNullableUtente();

		// Si verificano le security
		verificaSecurities(securityList, con);

		return securityList;
	}

	/**
	 * Restituisce le security del delegato.
	 * 
	 * @param idNodoUfficio
	 * @param con
	 * @return
	 */
	@Override
	public ListSecurityDTO getSecurityUtenteDelegato(final Long idUfficioDelegato, final Connection con) {
		final ListSecurityDTO securityList = new ListSecurityDTO();

		// Si recuperano tutti i delegati dell'ufficio
		final List<Utente> utentiDelegati = utenteDAO.getUtentiDelegati(idUfficioDelegato.intValue(), con);

		if (utentiDelegati != null && !utentiDelegati.isEmpty()) {
			for (final Utente utente : utentiDelegati) {
				securityList.add(creaSecurity(idUfficioDelegato, utente.getIdUtente(), 0L));
			}
		}

		// Si verificano le security
		verificaSecurities(securityList, con);

		return securityList;
	}

	@Override
	public final ListSecurityDTO getSecurityPerAddAssegnazione(final UtenteDTO utente, final String idDocumento, final Long idUtenteCreatore,
			final Long idUfficioCreatore, final String[] assegnazioni, final boolean isDocumento, final boolean aggiungiUfficio, final boolean isRiservato,
			final Connection con) {
		return getSecurityPerAddAssegnazione(utente, idDocumento, idUtenteCreatore, idUfficioCreatore, assegnazioni, isDocumento, aggiungiUfficio, isRiservato, false, null,
				con);
	}

	@Override
	public final ListSecurityDTO getSecurityPerAddAssegnazione(final UtenteDTO utente, final String idDocumento, final Long idUtenteCreatore,
			final Long idUfficioCreatore, final String[] assegnazioni, final boolean isDocumento, final boolean aggiungiUfficio, final boolean isRiservato,
			final boolean aggiungiDelegato, final String assegnazioneCoordinatore, final Connection con) {
		LOGGER.info("getSecurityPerAddAssegnazione -> START. Documento: " + idDocumento);
		final ListSecurityDTO securityList = new ListSecurityDTO();

		// Si ottiene lo storico security sul documento
		securityList.addAll(getStoricoSecurity(utente, idDocumento, isDocumento, aggiungiUfficio, isRiservato, false, con));

		// Si aggiunge l'utente corrente non considerato nello storico
		securityList.addAll(getSecurityMittente(utente, idUtenteCreatore, idUfficioCreatore, isDocumento, isRiservato, con));

		// Si aggiunge la gerarchia della nuova assegnazione
		securityList.addAll(getSecurityAssegnazioni(assegnazioni, isDocumento, aggiungiUfficio, isRiservato, con));

		// R.C. Nuove Richieste Marzo 2013
		// Si aggiunge il delegato nelle security, se previsto per l'ufficio
		if (aggiungiDelegato) {
			final String[] assegnazioniUtentiDelegati = getAssegnazioniUtentiDelegati(assegnazioni, con);
			securityList.addAll(getSecurityAssegnazioni(assegnazioniUtentiDelegati, isDocumento, aggiungiUfficio, isRiservato, con));
		}

		// R.C. Nuove Richieste Marzo 2013
		// Si aggiunge il coordinatore nello step, se esiste per l'ufficio
		if (StringUtils.isNotBlank(assegnazioneCoordinatore)) {
			securityList.addAll(getSecurityAssegnazione(assegnazioneCoordinatore, isDocumento, aggiungiUfficio, isRiservato, con));
		}

		printSecurityList(securityList);

		securityList.setNullableUtente();

		LOGGER.info("getSecurityPerAddAssegnazione -> END. Security generate con successo per il documento: " + idDocumento);
		return securityList;
	}

	/**
	 * @see it.ibm.red.business.service.ISecuritySRV#getSecurityMittente(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.Long, java.lang.Long, boolean, boolean, java.sql.Connection).
	 */
	@Override
	public ListSecurityDTO getSecurityMittente(final UtenteDTO utente, final Long idUtenteCreatore, final Long idUfficioCreatore, final boolean isDocumento,
			final boolean isRiservato, final Connection connection) {
		final ListSecurityDTO securityList = new ListSecurityDTO();

		// Si ottiene il nodo corriere
		final Long idNodoCorriere = nodoDAO.getNodoCorriereId(idUfficioCreatore, connection);

		// Se il documento non è riservato, si utilizza l'alberatura degli uffici
		if (!isRiservato) {
			// Si inserisce la gerarchia di chi crea il documento
			final List<Long> alberatura = nodoDAO.getAlberaturaUfficio(utente.getIdUfficio(), connection);
			// Si aggiunge la gerarchia di security sui nodi
			for (Long nodoCorrente : alberatura) {
				// Si esclude l'ufficio reale e si inserisce il gruppo corriere
				final SecurityDTO newSecurity = new SecurityDTO();
				final GruppoS newGruppo = newSecurity.getGruppo();
				if (idUfficioCreatore.equals(nodoCorrente) && isDocumento) {
					nodoCorrente = idNodoCorriere;
					newGruppo.setIdSiap(-1);
				}
				newGruppo.setIdUfficio(nodoCorrente.intValue());
				newSecurity.setGruppo(newGruppo);
				final UtenteS newUtente = newSecurity.getUtente();
				newUtente.setIdUtente(0);
				newSecurity.setUtente(newUtente);
				// Si aggiunge la security alla lista
				securityList.add(newSecurity);
			}
		} else if (idUtenteCreatore == 0) {
			// Security al corriere
			securityList.add(creaSecurity(idUfficioCreatore, 0L, idNodoCorriere));
		}

		// Se l'assegnazione è ad un utente, si inserisce la security puntuale
		// sull'utente
		if (idUtenteCreatore > 0 && isDocumento) {
			// Si inserisce la security per l'utente creatore
			securityList.add(creaSecurity(0L, idUtenteCreatore, idNodoCorriere));
		}

		printSecurityList(securityList);

		return securityList;
	}

	/**
	 * @see it.ibm.red.business.service.ISecuritySRV#getSecurityPerFascicolazione(java.lang.String,
	 *      java.lang.Long, it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public ListSecurityDTO getSecurityPerFascicolazione(final String idFascicolo, final Long idAoo, final IFilenetCEHelper fceh) {
		final ListSecurityDTO securityList = new ListSecurityDTO();

		SecurityDTO security = null;
		UtenteS utente = null;
		GruppoS gruppo = null;
		String idUfficio = null;

		final Folder folderFascicolo = fceh.getFolderFascicolo(idFascicolo, idAoo.intValue());

		final AccessPermissionList listaPermessi = folderFascicolo.get_Permissions();
		final Iterator<?> itListaPermessi = listaPermessi.iterator();
		while (itListaPermessi.hasNext()) {
			final AccessPermission permesso = (AccessPermission) itListaPermessi.next();
			final String nomePermesso = permesso.get_GranteeName();
			security = new SecurityDTO();

			if (nomePermesso != null && !Constants.EMPTY_STRING.equals(nomePermesso)) {
				gruppo = security.getGruppo();
				utente = security.getUtente();
				// Gestione per il fascicolo
				final String[] splittedUtentiUffici = nomePermesso.split("@");

				if (splittedUtentiUffici != null && splittedUtentiUffici.length > 0) {
					// Si ottiene la posizione 0
					final String utenteUfficio = splittedUtentiUffici[0];
					// Si ottiene l'identificativo
					final String[] splittedId = utenteUfficio.split("_");

					if (splittedId != null && splittedId.length > 1) {
						// Si tratta di un ufficio
						idUfficio = splittedId[1];
						gruppo.setIdUfficio(Integer.parseInt(idUfficio));
						utente.setIdUtente(0);
						security.setGruppo(gruppo);
						security.setUtente(utente);
						securityList.add(security);
					}
				}
			}
		}

		securityList.setNullableUtente();

		return securityList;
	}

	private static void printSecurityList(final ListSecurityDTO securityList) {
		UtenteS utenteCurr = null;
		GruppoS gruppoCurr = null;

		for (final SecurityDTO currSec : securityList) {
			utenteCurr = currSec.getUtente();
			gruppoCurr = currSec.getGruppo();
			if (utenteCurr != null && gruppoCurr != null) {
				LOGGER.info("Ufficio<" + gruppoCurr.getIdUfficio() + ">, Utente<" + utenteCurr.getIdUtente() + ">");
			}
		}
	}

	/**
	 * @see it.ibm.red.business.service.ISecuritySRV#getDocumentDetailAssegnazione(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      java.lang.Long, java.lang.String, boolean).
	 */
	@Override
	public DetailAssegnazioneDTO getDocumentDetailAssegnazione(final IFilenetCEHelper fceh, final Long idAooUtente, final String idDocumento, final boolean coordinatore) {
		final PropertiesProvider pp = PropertiesProvider.getIstance();

		DetailAssegnazioneDTO detailAssegnazione = null;

		final List<String> listaParametri = new ArrayList<>(5);

		listaParametri.add(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
		listaParametri.add(pp.getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY));
		listaParametri.add(pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY));

		if (coordinatore) {
			listaParametri.add(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_COORDINATORE_METAKEY));
			listaParametri.add(pp.getParameterByKey(PropertiesNameEnum.ID_UFFICIO_COORDINATORE_METAKEY));
		}
		final Document doc = fceh.getDocumentByIdGestionale(idDocumento, listaParametri, null, idAooUtente.intValue(), null,
				pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));

		if (coordinatore) {
			detailAssegnazione = TrasformCE.transform(doc, TrasformerCEEnum.FROM_DOCUMENT_TO_DETAIL_ASSEGNAZIONE_COORDINATORE);
		} else {
			detailAssegnazione = TrasformCE.transform(doc, TrasformerCEEnum.FROM_DOCUMENT_TO_DETAIL_ASSEGNAZIONE);
		}

		return detailAssegnazione;
	}

	/**
	 * @see it.ibm.red.business.service.ISecuritySRV#getSecurityAssegnazionePerWS(java.lang.String,
	 *      boolean, boolean, boolean, java.sql.Connection,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public ListSecurityDTO getSecurityAssegnazionePerWS(final String assegnazione, final boolean isDocumento, final boolean aggiungiUfficio,
			final boolean isRiservato, final Connection connection, final UtenteDTO utente) {
		final ListSecurityDTO securityList = new ListSecurityDTO();

		Long idNodoCorrente = 0L;
		Long idUtenteCorrente = 0L;
		final String[] splittedValue = assegnazione.split(",");
		if (splittedValue != null && splittedValue.length > 1) {
			idNodoCorrente = Long.parseLong(splittedValue[0]);
			idUtenteCorrente = Long.parseLong(splittedValue[1]);
		}

		// Si ottiene il nodo corriere
		final Long idNodoCorriere = nodoDAO.getNodoCorriereId(idNodoCorrente, connection);

		// Se il documento non è riservato, si utilizza l'alberatura degli uffici
		if (!isRiservato) {
			// Si calcola la gerarchia
			final List<Long> alberatura = nodoDAO.getAlberaturaUfficio(idNodoCorrente, connection);
			// Si aggiunge la gerarchia di security sui nodi
			for (Long nodoCorrente : alberatura) {
				// Si esclude l'ufficio reale e si inserisce il gruppo corriere
				final SecurityDTO newSecurity = new SecurityDTO();
				final GruppoS newGruppo = newSecurity.getGruppo();
				if (nodoCorrente.equals(idNodoCorrente) && isDocumento) {
					nodoCorrente = idNodoCorriere;
					newGruppo.setIdSiap(-1);
				}
				newGruppo.setIdUfficio(nodoCorrente.intValue());
				newSecurity.setGruppo(newGruppo);
				final UtenteS newUtente = newSecurity.getUtente();
				newUtente.setIdUtente(0);
				newSecurity.setUtente(newUtente);
				// Si aggiunge la security alla lista
				securityList.add(newSecurity);
			}
		} else if (idUtenteCorrente == 0) {
			// Security al corriere
			securityList.add(creaSecurity(idNodoCorrente, 0L, idNodoCorriere));
		}

		// Se l'assegnazione è ad un utente, si crea e inserisce la security puntuale
		// sull'utente
		if (idUtenteCorrente > 0) {
			// Si inserisce l'utente creatore
			securityList.add(creaSecurityPerWs(0L, idUtenteCorrente, idNodoCorriere, utente.getUsername()));
		}

		// Se richiesto, si crea e inserisce la security per l'ufficio reale
		if (aggiungiUfficio && !isRiservato) {
			securityList.add(creaSecurity(idNodoCorrente, 0L, 0L));
		}

		return securityList;
	}

	private static SecurityDTO creaSecurityPerWs(final Long idNodoCorrente, final Long idUtenteCorrente, final Long idNodoCorriere, final String username) {
		final SecurityDTO newSecurity = new SecurityDTO();

		final GruppoS newGruppo = newSecurity.getGruppo();
		if (idNodoCorriere != null && idNodoCorriere > 0) {
			newGruppo.setIdUfficio(idNodoCorriere.intValue());
			newGruppo.setIdSiap(-1);
		} else if (idNodoCorrente != null && idNodoCorrente > 0) {
			newGruppo.setIdUfficio(idNodoCorrente.intValue());
		}
		newSecurity.setGruppo(newGruppo);

		final UtenteS newUtente = newSecurity.getUtente();
		if (idUtenteCorrente != null && idUtenteCorrente > 0) {
			newUtente.setIdUtente(idUtenteCorrente.intValue());
			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(username)) {
				newUtente.setUsername(username);
			}
		} else {
			newUtente.setIdUtente(0);
		}
		newSecurity.setUtente(newUtente);

		return newSecurity;
	}

}