/**
 * 
 */
package it.ibm.red.business.service.ws.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IContattoDAO;
import it.ibm.red.business.dto.RicercaRubricaResultDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoRubricaEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.concrete.AbstractService;
import it.ibm.red.business.service.ws.IContattoWsSRV;
import it.ibm.red.business.service.ws.auth.DocumentServiceAuthorizable;
import it.ibm.red.webservice.model.documentservice.dto.Servizio;
import it.ibm.red.webservice.model.documentservice.types.messages.RedGetContattiType;

/**
 * @author APerquti
 *
 */
@Service
public class ContattoWsSRV extends AbstractService implements IContattoWsSRV {

	/**
	 * 
	 */
	private static final long serialVersionUID = -808162608784391713L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ContattoWsSRV.class.getName());

	/**
	 * Dao contatto.
	 */
	@Autowired
	private IContattoDAO contattoDAO;

	/**
	 * @see it.ibm.red.business.service.ws.facade.IContattoWsFacadeSRV#getContatti(it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.RedGetContattiType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_GET_CONTATTI)
	public RicercaRubricaResultDTO getContatti(final RedWsClient client, final RedGetContattiType request) {
		RicercaRubricaResultDTO output = null;
		Connection con = null;

		try {

			con = setupConnection(getDataSource().getConnection(), false);

			final String descrizione = request.getDescrizione();
			final String mail = request.getMail();
			final String tipoPersona = request.getTipoPersona();
			final Integer idAoo = request.getIdAoo();
			final Integer idContatto = request.getIdContatto();

			// Filtri per Rubrica
			final boolean onRubricaRED = request.isRicercaRED();
			final boolean onRubricaIPA = request.isRicercaIPA();
			final boolean onRubricaMEF = request.isRicercaMEF();

			// Limiti di ricerca
			final int maxRed = Integer.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.REDWS_CAP_RUBRICA_RED));
			final int maxIpa = Integer.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.REDWS_CAP_RUBRICA_IPA));
			final int maxMef = Integer.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.REDWS_CAP_RUBRICA_MEF));
			int limiteRicerca = 0;

			final List<Contatto> resultRicerca = new ArrayList<>();

			if (onRubricaRED) {
				resultRicerca.addAll(contattoDAO.getContattiByTipoRubrica(descrizione, mail, tipoPersona, idAoo, idContatto, TipoRubricaEnum.RED, maxRed, con));
				limiteRicerca = limiteRicerca + maxRed;
			}

			if (onRubricaIPA) {
				resultRicerca.addAll(contattoDAO.getContattiByTipoRubrica(descrizione, mail, tipoPersona, idAoo, idContatto, TipoRubricaEnum.IPA, maxIpa, con));
				limiteRicerca = limiteRicerca + maxIpa;
			}

			if (onRubricaMEF) {
				resultRicerca.addAll(contattoDAO.getContattiByTipoRubrica(descrizione, mail, tipoPersona, idAoo, idContatto, TipoRubricaEnum.MEF, maxMef, con));
				limiteRicerca = limiteRicerca + maxMef;
			}

			String msg = null;
			if (!onRubricaRED && !onRubricaIPA && !onRubricaMEF) {
				msg = new StringBuilder("Necessario dichiarare una Rubrica su cui effettuare la ricerca.").toString();
			} else if (limiteRicerca == resultRicerca.size()) {
				msg = new StringBuilder("La ricerca ha raggiunto il limite massimo di ").append(limiteRicerca)
						.append(" elementi per le rubriche richieste, restringere i criteri di ricerca.").toString();
			} else {
				msg = new StringBuilder("Sono stati trovati ").append(resultRicerca.size()).append(" elementi.").toString();
			}

			output = new RicercaRubricaResultDTO(resultRicerca, msg);

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la ricerca dei contatti: ", e);
			throw new RedException("Si è verificato un errore durante la ricerca dei contatti: ", e);
		} finally {
			closeConnection(con);
		}

		return output;
	}

}
