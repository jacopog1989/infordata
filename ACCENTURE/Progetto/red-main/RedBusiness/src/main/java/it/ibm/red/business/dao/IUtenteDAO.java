package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.CoordinatoreUfficioDTO;
import it.ibm.red.business.dto.PreferenzeDTO;
import it.ibm.red.business.dto.ResponsabileCopiaConformeDTO;
import it.ibm.red.business.dto.TracciaDocUscitaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.NodoUtenteCoordinatore;
import it.ibm.red.business.persistence.model.NodoUtenteRuolo;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.persistence.model.UtenteFirma;

/**
 * 
 * Interfaccia del dao per la gestione di un utente.
 * 
 * @author CPIERASC
 */
public interface IUtenteDAO extends Serializable {

	/**
	 * Recupero utente per username.
	 * 
	 * @param username
	 *            username
	 * @param connection
	 *            connnessione
	 * @return dati utente
	 */
	Utente getByUsername(String username, Connection connection);

	/**
	 * Recupero nodo utente ruolo per id utente ed id ruolo.
	 * 
	 * @param idUtente
	 *            id utente
	 * @param idRuolo
	 *            id ruolo
	 * @param connection
	 *            connessione al db
	 * @return collezione nodo utente ruolo
	 */
	Collection<NodoUtenteRuolo> getNodoUtenteRuolo(Long idUtente, Long idRuolo, Connection connection);

	/**
	 * Recupero nodo utente ruolo per id utente ed id ruolo.
	 * 
	 * @param idUtente
	 *            id utente
	 * @param idNodo
	 *            id nodo
	 * @param idRuolo
	 *            id ruolo
	 * @param connection
	 *            connessione al db
	 * @return nodo utente ruolo
	 */
	NodoUtenteRuolo getNodoUtenteRuolo(Long idUtente, Long idNodo, Long idRuolo, Connection connection);

	/**
	 * Metodo per verificare se un utente è delegato.
	 * 
	 * @param idUtente
	 *            id Ruolo
	 * @param connection
	 *            connessione al db
	 * @return flag che indica se l'utente è delegato
	 */
	boolean isDelegato(Long idRuolo, Connection connection);

	/**
	 * Metodo per il recupero di un utente.
	 * 
	 * @param idUtente
	 *            identificativo utente
	 * @param connection
	 *            connessione al db
	 * @return dati utente
	 */
	Utente getUtente(Long idUtente, Connection connection);

	/**
	 * Metodo per il recupero di un utente.
	 * 
	 * @param idUtente
	 *            identificativo utente
	 * @param connection
	 *            connessione al db
	 * @return dati utente
	 */
	UtenteFirma getUtenteFirma(Long idUtente, Connection connection);

	/**
	 * Metodo per il recupero di un flag che indica se un utente ha come privilegio
	 * CORRIERE sulla base del suo ruolo.
	 * 
	 * @param idRuolo
	 *            Id Ruolo utente
	 * @param connection
	 *            connessione al db
	 * @return flag che indica se il ruolo è di un utente delegato
	 */
	boolean isCorriere(Long idRuolo, Connection connection);

	/**
	 * Metodo per il recupero di un flag che indica se un utente ha come privilegio
	 * TAB SCADENZARIO sulla base del suo ruolo.
	 * 
	 * @param idRuolo
	 *            Id Ruolo utente
	 * @param connection
	 *            connessione al db
	 * @return flag che indica se il ruolo è di un utente abilitato al tab
	 *         scadenzario
	 */
	Boolean isTabScadenzario(Long idRuolo, Connection connection);

	/**
	 * Metodo che restituisce true se l'utente ha il libro firma.
	 * 
	 * @param idRuolo
	 *            identificativo del ruolo dell'utente in esame
	 * @param connection
	 *            connessione al db
	 * @return true se l'utente ha il libro firma
	 */
	Boolean hasLibroFirma(Long idRuolo, Connection connection);

	/**
	 * Metodo per il recupero della nodo-utente-ruolo sulla base dell'id utente e
	 * dell'ufficio.
	 * 
	 * @param idUtente
	 *            identificativo utente
	 * @param idUfficio
	 *            identificativo ufficio
	 * @param connection
	 *            connessione al db
	 * @return nodo-utente-ruolo associata a quell'utente ed a quell'ufficio
	 */
	NodoUtenteRuolo getUtenteUfficioByIdUtenteAndIdNodo(Long idUtente, Long idUfficio, Connection connection);

	/**
	 * Metodo per l'update del pin verificato.
	 * 
	 * @param idUtente
	 *            - Identificato dell'utente
	 * @param pin
	 *            - PIN
	 * @param pinVerificato
	 *            - bool del pin verificato
	 * @param connection
	 *            - connessione al db
	 */
	void setPinVerificato(Long idUtente, String pin, boolean pinVerificato, Connection connection);

	/**
	 * Recupero utenti delegati.
	 * 
	 * @param idNodoDestinatario
	 *            identificativo nodo destinatario
	 * @param con
	 *            connessione
	 * @return lista utenti
	 */
	List<Utente> getUtentiDelegati(int idNodoDestinatario, Connection con);

	/**
	 * Recupero coordinatori ufficio.
	 * 
	 * @param idUfficio
	 *            identificativo ufficio
	 * @param con
	 *            connessione
	 * @return lista coordinatori
	 */
	Collection<CoordinatoreUfficioDTO> getCoordinatoriUfficio(Integer idUfficio, Connection con);

	/**
	 * Recupero utenti con registro riservato.
	 * 
	 * @param idAoo
	 * @param con
	 *            connessione
	 * @return
	 */
	Collection<Utente> getUtentiRegistroRiservato(Long idAoo, Connection con);

	/**
	 * Metodo per il recupero di un flag che indica se un utente ha come privilegio
	 * GESTIONE AOO sulla base del suo ruolo.
	 * 
	 * @param ruolo
	 *            ruolo utente
	 * @return flag che indica se il ruolo è di un utente abilitato al tab
	 *         scadenzario
	 */
	Boolean isAmministratore(Long idRuolo, Connection connection);

	/**
	 * Ottiene il nodo utente coordinatore.
	 * 
	 * @param idUfficio
	 *            Identificativo dell'ufficio coordinatore.
	 * @param idUtenteCoordinatore
	 *            Identificativo dell'utente coordinatore.
	 * @param con
	 *            Connessione al database.
	 * @return nodo utente coordinatore
	 */
	NodoUtenteCoordinatore getNodoUtenteCoordinatore(Long idUfficio, Long idUtenteCoordinatore, Connection con);

	/**
	 * Ottiene i nodi utenti coordinatori.
	 * 
	 * @param idUfficioMittente
	 *            Identificativo ufficio mittente.
	 * @param con
	 *            Connessione al database.
	 * @return lista di nodi utenti coordinatori
	 */
	List<NodoUtenteCoordinatore> getComboNodiUtenteCoordinatore(Long idUfficioMittente, Connection con);

	/**
	 * Ottiene i responsabili copia conforme.
	 * 
	 * @param idAOO
	 *            Identificativo dell'area organizzativa.
	 * @param con
	 *            Connessione al database.
	 * @return lista di responsabili copia conforme.
	 */
	List<ResponsabileCopiaConformeDTO> getComboResponsabiliCopiaConforme(Long idAOO, Connection con);

	/**
	 * Ottiene l'id del ragioniere dello stato.
	 * 
	 * @param con
	 *            Connessione al database.
	 * @return id del ragioniere dello stato
	 */
	Long getIdRagioniereGeneraleDelloStato(Connection con);

	/**
	 * Aggiorna l'utente di firma.
	 * 
	 * @param idUtente
	 *            Identificativo dell'utente.
	 * @param key
	 *            Chiave da aggiornare.
	 * @param value
	 *            Valore da aggiornare.
	 * @param con
	 *            Connessione al database.
	 * @return Esito aggiornamento.
	 */
	int aggiornaUtenteFirma(Long idUtente, String key, String value, Connection con);

	/**
	 * Modifica le preferenze mobile dell'utente.
	 * 
	 * @param idUtente
	 *            Identificativo dell'utente.
	 * @param pref
	 *            Preferenze Mobile.
	 * @param connection
	 *            Connessione al database.
	 */
	void changeMobilePreference(long idUtente, PreferenzeDTO pref, Connection connection);

	/**
	 * Ottiene gli utenti dalle descrizioni del nodo e del permesso.
	 * 
	 * @param descrizioneNodo
	 *            Descrizione nodo.
	 * @param descrizionePermesso
	 *            Descrizione permesso.
	 * @param con
	 *            Connessione al database.
	 * @return lista di utenti
	 */
	List<UtenteDTO> getUtentiByDescNodoByDescPermesso(String descrizioneNodo, String descrizionePermesso,
			Connection con);

	/**
	 * Controllo sul libro firma mobile.
	 * 
	 * @param idRuolo
	 *            Identificativo del ruolo.
	 * @param aoo
	 *            Identificativo area organizzativa.
	 * @param con
	 *            Connessione al database.
	 * @return true o false
	 */
	boolean hasLibroFirmaMobile(Long idRuolo, Aoo aoo, Connection con);

	/**
	 * Ottiene l'utente dallo username e dalla descrizione del nodo.
	 * 
	 * @param username
	 *            nome utente.
	 * @param descNodo
	 *            Descrizione Nodo.
	 * @param con
	 *            Connessione al database.
	 * @return utente
	 */
	UtenteDTO getUtenteByUsernameByDescNodo(String username, String descNodo, Connection con);

	/**
	 * Ottiene la descrizione degli utenti.
	 * 
	 * @param idsProtocollatori
	 *            Identificativi utenti protocollatori.
	 * @param con
	 *            Connessione al database.
	 * @return mappa id descrizione
	 */
	Map<Integer, String> getDescUtenti(List<Integer> idsProtocollatori, Connection con);

	/**
	 * Registra la chiusura della sessione.
	 * 
	 * @param id
	 *            Identificativo sessione.
	 * @param user
	 *            Nome utente.
	 * @param connection
	 *            Connessione al database.
	 */
	void registerCloseSession(String id, String user, Connection connection);

	/**
	 * Registra l'apertura della sessione.
	 * 
	 * @param id
	 *            Identificativo sessione.
	 * @param ip
	 *            Indirizzo IP.
	 * @param user
	 *            Nome utente.
	 * @param connection
	 *            Connessione al database.
	 */
	void registerOpenSession(String id, String ip, String user, Connection connection);

	/**
	 * Ottiene gli utenti nel nodo.
	 * 
	 * @param con
	 *            Connessione al database.
	 * @param ip
	 *            Indirizzo IP.
	 * @return username degli utenti
	 */
	Collection<String> getUtentiInNodo(Connection con, String ip);

	/**
	 * Ottiene il numero di sessioni aperte.
	 * 
	 * @param con
	 *            Connessione al database.
	 * @return numero di sessioni aperte
	 */
	Integer getSessioniAperte(Connection con);

	/**
	 * Ottiene le sessioni aperte per utente.
	 * 
	 * @param con
	 *            Connessione al database.
	 * @return mappa username numero di sessioni
	 */
	Map<String, Integer> getSessioniApertePerUtente(Connection con);

	/**
	 * Ottiene le sessioni aperte per nodo.
	 * 
	 * @param con
	 *            Connessione al database.
	 * @return mappa ip numero di sessioni
	 */
	Map<String, Integer> getSessioniApertePerNodo(Connection con);

	/**
	 * Ottiene l'utente tramite username per la spedizione.
	 * 
	 * @param username
	 *            Nome utente.
	 * @param connection
	 *            Connessione al database.
	 * @return nome dell'utente
	 */
	String getByUsernameForSpedizione(String username, Connection connection);

	/**
	 * Ottiene il predefinito tramite id.
	 * 
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @param idUtente
	 *            Identificativo utente.
	 * @param conn
	 *            Connessione al database.
	 * @return traccia documento in uscita
	 */
	TracciaDocUscitaDTO getPredefinitoById(Long idAoo, Long idUtente, Connection conn);

	/**
	 * Ottiene l'id dell'utente tramite il codice fiscale.
	 * 
	 * @param codiceFiscale
	 *            Codice fiscale utente.
	 * @param con
	 *            Connessione al database.
	 * @return id dell'utente
	 */
	Long getIdByCodiceFiscale(String codiceFiscale, Connection con);

	/**
	 * Restituisce l'utente firma per il glifo delegato.
	 * 
	 * @param idUtente
	 *            Identificativo utente.
	 * @param idUtenteDelegante
	 *            Identificativo utente delegante.
	 * @param con
	 *            Connessione al database.
	 * @return @see UtenteFirma
	 */
	UtenteFirma getGlifoDelegato(Long idUtente, Long idUtenteDelegante, Connection con);

	/**
	 * Restituisce l'immagine della postilla dell'AOO.
	 * 
	 * @param idAOO
	 *            identificativo dell'AOO
	 * @param confPDFA
	 *            flag gestione transparency
	 * @param connection
	 *            connession al db
	 * @return byte array immagine postilla
	 */
	byte[] getImageFirmaAOO(Long idAOO, boolean confPDFA, Connection connection);

	/**
	 * Restituisce il testo della postilla che definisce la postilla.
	 * 
	 * @param idAOO
	 *            identificativo dell'AOO.
	 * @param connection
	 *            connessione al database
	 * @return testo della postilla
	 */
	String getTestoPostilla(Long idAOO, Connection connection);

}