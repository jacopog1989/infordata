package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.Date;

import it.ibm.red.business.logger.REDLogger;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.utils.StringUtils;

/**
 * Trasformer Document to FaldoneDTO.
 */
public class FromDocumentToFaldoneDtoTransformer extends TrasformerCE<FaldoneDTO> {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FromDocumentToFaldoneDtoTransformer.class);

	/**
	 * Costruttore.
	 */
	public FromDocumentToFaldoneDtoTransformer() {
		super(TrasformerCEEnum.FROM_DOCUMENT_TO_FALDONE);
	}
	
	/**
	 * Esegue la trasformazione del document.
	 * @param document
	 * @param connection
	 * @return FaldoneDTO
	 */
	@Override
	public FaldoneDTO trasform(final Document document, final Connection connection) {
		FaldoneDTO f = null;
		
		try {
			String guid = StringUtils.cleanGuidToString(document.get_Id()); 
			String name = document.get_Name();
			String documentTitle = (String) getMetadato(document, PropertiesNameEnum.METADATO_FALDONE_DOCUMENTTITLE);
			String creator = document.get_Creator();
			Date dateCreated = document.get_DateCreated(); 
			String owner = document.get_Owner(); 
			String nomeFaldone = (String) getMetadato(document, PropertiesNameEnum.METADATO_FALDONE_NOMEFALDONE);
			String parentFaldone = (String) getMetadato(document, PropertiesNameEnum.METADATO_FALDONE_PARENTFALDONE);
			String descrizioneFaldone = (String) getMetadato(document, PropertiesNameEnum.METADATO_FALDONE_DESCRIZIONEFALDONE);
			String oggetto = (String) getMetadato(document, PropertiesNameEnum.METADATO_FALDONE_OGGETTO);
			Integer idAOO = (Integer) getMetadato(document, PropertiesNameEnum.METADATO_FALDONE_IDAOO); 
			Boolean reserved = document.get_IsReserved();
			
			f = new FaldoneDTO(guid, name, documentTitle, creator, dateCreated, owner, nomeFaldone, parentFaldone, descrizioneFaldone, idAOO, 
					reserved, oggetto);
			
			if (document.get_SecurityFolder() != null) {
				f.setAbsolutePath(document.get_SecurityFolder().get_PathName());
			}
		} catch (Exception e) {
			LOGGER.error("Errore nella trasformazione del faldone: " + e.getMessage(), e);
			f = null;
		}
		
		return f;
	}

}
