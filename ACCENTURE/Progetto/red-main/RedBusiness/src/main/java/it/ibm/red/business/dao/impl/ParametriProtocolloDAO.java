package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IParametriProtocolloDAO;
import it.ibm.red.business.dto.SequKDTO;
import it.ibm.red.business.exception.RedException;

/**
 * The Class ParametriProtocolloDAO.
 *
 * @author CPIERASC
 * 
 * 	Dao gestione parametro protocollo
 */
@Repository
public class ParametriProtocolloDAO extends AbstractDAO implements IParametriProtocolloDAO {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -2051164458360995374L;

	/**
	 * Gets the parametri protocollo by id aoo.
	 *
	 * @param idAoo the id aoo
	 * @param connection the connection
	 * @return the parametri protocollo by id aoo
	 */
	@Override
	public final SequKDTO getParametriProtocolloByIdAoo(final Long idAoo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String querySQL = "SELECT * FROM parametriprotocollo WHERE idaoo = ?";
			ps = connection.prepareStatement(querySQL);
			ps.setLong(1, idAoo);
			rs = ps.executeQuery();
			if (rs.next()) {
				return new SequKDTO(rs.getInt("sequ_k_aoo"), 
									rs.getInt("sequ_k_destinatario"),
									rs.getInt("sequ_k_mezzo_spedizione"),
									rs.getInt("sequ_k_registro"),
									rs.getInt("sequ_k_storico_uff"),
									rs.getInt("sequ_k_tipodocumento"),
									rs.getInt("sequ_k_titolario"),
									rs.getInt("sequ_k_ufficio"),
									rs.getInt("sequ_k_ufficio_mittente"),
									rs.getInt("sequ_k_ufficio_protocollatore"),
									rs.getInt("sequ_k_utente"),
									rs.getInt("sequ_k_utente_mittente"),
									rs.getInt("sequ_k_utente_protocollatore"),
									rs.getString("assegnatari"));
			}
		} catch (SQLException e) {
			throw new RedException("Errore durante il recupero dei parametri del protocollo tramite id=" + idAoo, e);
		} finally {
			closeStatement(ps, rs);
		}
		return null;
	}

}
