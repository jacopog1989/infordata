package it.ibm.red.business.dto;

import it.ibm.red.business.enums.TipoAllaccioEnum;
import it.ibm.red.business.enums.TipoCategoriaEnum;

/**
 * The Class RispostaAllaccioDTO.
 *
 * @author CPIERASC
 * 
 * 	DTO risposta allaccio.
 */
public class RispostaAllaccioDTO extends AbstractDTO {

	/**
	 * Serializzazione.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificativo documento a cui l'allaccio fa riferimento.
	 */
	private Integer idDocumento;

	/**
	 * Identificativo documento allacciato.
	 */
	private String idDocumentoAllacciato;

	/**
	 * Identificativo tipo allaccio.
	 */
	private Integer idTipoAllaccio;

	/**
	 * Flag documento da chiudere.
	 */
	private Boolean documentoDaChiudere;
	
	/**
	 * Indica se il il check di chiusura sia disabilitato 
	 */
	private boolean chiusuraDisabled;

	/**
	 * Identificativo protocollo.
	 */
	private String idProtocollo;
	
	/**
	 * Dettagli light del documento dal ce 
	 */
	private DetailDocumentRedDTO document;
	
	/**
	 * etichetta di idTipoAllaccio 
	 */
	private TipoAllaccioEnum tipoAllaccioEnum;
	
	/**
	 * Fascicolo "procedimentale" dell'allaccio.
	 */
	private FascicoloDTO fascicolo;
	
	/**
	 * Serve per il settaggio del numero protocollo senza dover fare un converter su DetailDocumentRedDTO document
	 */
	private Integer numeroProtocollo;
	/**
	 * Serve per il settaggio dell'anno protocollo senza dover fare un converter su DetailDocumentRedDTO document
	 */
	private int annoProtocollo;

	/**
	 * Indica se il protocollo e' stato verificato per l'allaccio 
	 */
	private boolean verificato;
	
	/**
	 * Indica se il livello di riservatezza dell'allaccio è Pubblico o Riservato.
	 */
	private boolean riservato;
	
	/**
	 * Booleano per la presentation che indica se l'allaccio è modificabile/rimuovibile dalla UI.
	 */
	private boolean disabled;
	 
	/**
	 * Numero documento.
	 */
	private Integer numDocumento;
	
	/**
	 * Flag principale.
	 */
	private boolean principale;
	
	/**
	 * Tipo categoria.
	 */
	private TipoCategoriaEnum tipoCategoriaEnum;
 
	/**
	 * Costruttore vuoto per maschera di inserimento modifica .
	 */
	public RispostaAllaccioDTO() { }
	
	
	/**
	 * Costruttore.
	 * 
	 * @param inIdDocumento				identificativo documento
	 * @param inIdDocumentoAllacciato	identificativo documento allacciato
	 * @param inIdTipoAllaccio			identificativo tipo allaccio
	 * @param inDocumentodachiudere		flag documento da chiudere
	 */
	public RispostaAllaccioDTO(final Integer inIdDocumento, final String inIdDocumentoAllacciato, final Integer inIdTipoAllaccio, 
			final Integer inDocumentoDaChiudere, final boolean inPrincipale) {
		idDocumento = inIdDocumento;
		idDocumentoAllacciato = inIdDocumentoAllacciato;
		idTipoAllaccio = inIdTipoAllaccio;
		if (idTipoAllaccio != null) {
			this.tipoAllaccioEnum = TipoAllaccioEnum.get(inIdTipoAllaccio);
		}
		documentoDaChiudere = (inDocumentoDaChiudere == 1);
		principale = inPrincipale;
	}

	/**
	 * Getter.
	 * 
	 * @return	identificativo documento
	 */
	public final Integer getIdDocumento() {
		return idDocumento;
	}

	/**
	 * Getter.
	 * 
	 * @return	identificativo documento allacciato
	 */
	public final String getIdDocumentoAllacciato() {
		return idDocumentoAllacciato;
	}

	/**
	 * Getter.
	 * 
	 * @return	identificativo tipo allaccio
	 */
	public final Integer getIdTipoAllaccio() {
		return idTipoAllaccio;
	}

	
	/**
	 * Getter.
	 * 
	 * @return	identificativo protocollo
	 */
	public final String getIdProtocollo() {
		return idProtocollo;
	}
	
	
	/**
	 * Restiusce true se il documento è da chiudere, false altrimenti.
	 * @return boolean che indica se il documento è da chiudere
	 */
	public Boolean getDocumentoDaChiudere() {
		return documentoDaChiudere;
	}
	
	/**
	 * Restituisce true se la chiusura è disabilitata, false altrimenti.
	 * @return true se la chiusura è disabilitata, false altrimenti
	 */
	public boolean isChiusuraDisabled() {
		return chiusuraDisabled;
	}

	/**
	 * Imposta il flag che stabilisce l'abilitazione della funzionalita di chiusura.
	 * @param chiusuraDisabled
	 */
	public void setChiusuraDisabled(final boolean chiusuraDisabled) {
		this.chiusuraDisabled = chiusuraDisabled;
	}

	/**
	 * Setter identificativo protocollo.
	 * 
	 * @param inIdProtocollo	identificativo protocollo
	 */
	public final void setIdProtocollo(final String inIdProtocollo) {
		this.idProtocollo = inIdProtocollo;
	}

	/**
	 * Restituisce il documento.
	 * @return document
	 */
	public DetailDocumentRedDTO getDocument() {
		return document;
	}

	/**
	 * Impsota il documento.
	 * @param document
	 */
	public void setDocument(final DetailDocumentRedDTO document) {
		this.document = document;
	}

	/**
	 * Restitusice il tipo allaccio.
	 * @return tipo allaccio
	 */
	public TipoAllaccioEnum getTipoAllaccioEnum() {
		return tipoAllaccioEnum;
	}

	/**
	 * Restituisce il fascicolo.
	 * @return fascicolo
	 */
	public FascicoloDTO getFascicolo() {
		return fascicolo;
	}

	/**
	 * Imposta il fascicolo.
	 * @param fascicolo
	 */
	public void setFascicolo(final FascicoloDTO fascicolo) {
		this.fascicolo = fascicolo;
	}

	/**
	 * Imposta il flag: documento da chiudere.
	 * @param documentoDaChiudere
	 */
	public void setDocumentoDaChiudere(final Boolean documentoDaChiudere) {
		this.documentoDaChiudere = documentoDaChiudere;
	}

	/**
	 * Restituisce il numero protocollo.
	 * @return numero protocollo
	 */
	public Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * Imposta il numero protocollo.
	 * @param numeroProtocollo
	 */
	public void setNumeroProtocollo(final Integer numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}

	/**
	 * Restituisce l'anno protocollo.
	 * @return anno protocollo
	 */
	public int getAnnoProtocollo() {
		return annoProtocollo;
	}

	/**
	 * Imposta l'anno protocollo.
	 * @param annoProtocollo
	 */
	public void setAnnoProtocollo(final int annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}

	/**
	 * Restituisce true se verificato, false altrimenti.
	 * @return true se verificato, false altrimenti
	 */
	public boolean isVerificato() {
		return verificato;
	}

	/**
	 * Imposta il flag associato alla verifica.
	 * @param verificato
	 */
	public void setVerificato(final boolean verificato) {
		this.verificato = verificato;
	}

	/**
	 * Restituisce true se riservato, false altrimenti.
	 * @return true se riservato, false altrimenti
	 */
	public boolean isRiservato() {
		return riservato;
	}

	/**
	 * Imposta il flag associato a: documento riservato.
	 * @param riservato
	 */
	public void setRiservato(final boolean riservato) {
		this.riservato = riservato;
	}

	/**
	 * Imposta l'id documento.
	 * @param idDocumento
	 */
	public void setIdDocumento(final Integer idDocumento) {
		this.idDocumento = idDocumento;
	}

	/**
	 * Imposta l'id del documento allacciato.
	 * @param idDocumentoAllacciato
	 */
	public void setIdDocumentoAllacciato(final String idDocumentoAllacciato) {
		this.idDocumentoAllacciato = idDocumentoAllacciato;
	}

	/**
	 * Imposta l'id del tipo di allaccio.
	 * @param idTipoAllaccio
	 */
	public void setIdTipoAllaccio(final Integer idTipoAllaccio) {
		this.idTipoAllaccio = idTipoAllaccio;
	}

	/**
	 * Imposta il tipo di allaccio.
	 * @param tipoAllaccioEnum
	 */
	public void setTipoAllaccioEnum(final TipoAllaccioEnum tipoAllaccioEnum) {
		this.tipoAllaccioEnum = tipoAllaccioEnum;
	}

	/**
	 * Restituisce true se disabilitato, false altrimenti.
	 * @return true se disabilitato, false altrimenti
	 */
	public boolean isDisabled() {
		return disabled;
	}
	
	/**
	 * Imposta il flag associato al: disabled.
	 * @param disabled
	 */
	public void setDisabled(final boolean disabled) {
		this.disabled = disabled;
	}
	
	/**
	 * Restituisce il numero documento.
	 * @return numero documento
	 */
	public Integer getNumDocumento() {
		return numDocumento;
	}
	
	/**
	 * Imposta il numero documento.
	 * @param numDocumento
	 */
	public void setNumDocumento(final Integer numDocumento) {
		this.numDocumento = numDocumento;
	}
	
	/**
	 * Restituisce true se principale, false altrimenti.
	 * @return true se principale, false altrimenti
	 */
	public boolean isPrincipale() {
		return principale;
	}

	/**
	 * Imposta il flag associato al documento principale.
	 * @param principale
	 */
	public void setPrincipale(final boolean principale) {
		this.principale = principale;
	}

	/**
	 * Restituisce il tipo categoria.
	 * 
	 * @return tipo categoria
	 */
	public TipoCategoriaEnum getTipoCategoriaEnum() {
		return tipoCategoriaEnum;
	}

	/**
	 * Imposta il tipo categoria.
	 * 
	 * @param tipoCategoriaEnum
	 */
	public void setTipoCategoriaEnum(TipoCategoriaEnum tipoCategoriaEnum) {
		this.tipoCategoriaEnum = tipoCategoriaEnum;
	}
	
}