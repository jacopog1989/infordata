package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;

import it.ibm.red.business.dto.AnagraficaDipendenteDTO;
import it.ibm.red.business.dto.IndirizzoDTO;
import it.ibm.red.business.dto.RicercaAnagDipendentiDTO;
import it.ibm.red.business.dto.SelectItemDTO;

/**
 * Interfaccia DAO anagrafica dipendenti.
 */
public interface IAnagraficaDipendenteDAO extends Serializable {

	/**
	 * Salva l'anagrafica del dipendente.
	 * 
	 * @param con
	 * @param idAoo
	 *            - id dell'Aoo
	 * @param newAnagrafica
	 *            - nuova anagrafica
	 * @return anagrafica
	 */
	AnagraficaDipendenteDTO salva(Connection con, long idAoo, AnagraficaDipendenteDTO newAnagrafica);

	/**
	 * Ricerca le anagrafiche dei dipendenti.
	 * 
	 * @param con
	 * @param idAoo
	 *            - id dell'Aoo
	 * @param ricercaDTO
	 *            - anagrafiche dei dipendenti da ricercare
	 * @return anagrafiche
	 */
	Collection<AnagraficaDipendenteDTO> ricerca(Connection con, long idAoo, RicercaAnagDipendentiDTO ricercaDTO);

	/**
	 * Ottiene dug.
	 * 
	 * @param con
	 * @return dug
	 */
	Collection<SelectItemDTO> getDug(Connection con);

	/**
	 * Salva l'indirizzo del dipendente.
	 * 
	 * @param con
	 * @param idAnagraficaDipendente
	 *            - id dell'anagrafica del dipendente
	 * @param newIndirizzo
	 *            - nuovo indirizzo
	 * @return indirizzo
	 */
	IndirizzoDTO salvaIndirizzo(Connection con, Integer idAnagraficaDipendente, IndirizzoDTO newIndirizzo);

	/**
	 * Ottiene gli indirizzi.
	 * 
	 * @param con
	 * @param idAnagrafica
	 *            - id dell'anagrafica
	 * @return indirizzi
	 */
	Collection<IndirizzoDTO> getIndirizzi(Connection con, Integer idAnagrafica);

	/**
	 * Elimina l'indirizzo.
	 * 
	 * @param con
	 * @param idIndirizzo
	 *            - id dell'indirizzo
	 */
	void removeIndirizzo(Connection con, Integer idIndirizzo);

	/**
	 * Cancella l'item della tabella anagrafica_dipendenti relativo all'id.
	 * 
	 * @param con
	 * @param id
	 */
	void remove(Connection con, Integer id);

	/**
	 * Aggiorna la tabella anagrafica_dipendenti.
	 * 
	 * @param con
	 * @param newAnagrafica
	 *            - nuova anagrafica
	 */
	void modifica(Connection con, AnagraficaDipendenteDTO newAnagrafica);

	/**
	 * Modifica l'indirizzo del dipendente.
	 * 
	 * @param con
	 * @param idAnagraficaDipendente
	 *            - id dell'anagrafica del dipendente
	 * @param newIndirizzo
	 *            - nuovo indirizzo
	 */
	void modificaIndirizzo(Connection con, Integer idAnagraficaDipendente, IndirizzoDTO newIndirizzo);

	/**
	 * Ottiene le anagrafiche dei dipendenti tramite pks.
	 * 
	 * @param connection
	 * @param pks
	 * @return anagrafiche
	 */
	Collection<AnagraficaDipendenteDTO> getByPks(Connection connection, Collection<Integer> pks);

}