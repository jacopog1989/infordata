package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Questo DTO rappresenta un insieme di identificativi per l'utente recuperati da FileNet CE
 * 
 * Tutti hanno l'obbiettivo di rappresentare il documento in maniera univoca per l'utente o per FileNet.
 * 
 */
public class IdDocumentDTO implements Serializable {
	
	private static final long serialVersionUID = -9025074996121867763L;


	/**
	 * id del documento considerato come un insiemee delle versioni
	 * Permette di identificare anche i documenti non protocollati 
	 * 
	 * Nel PE viene chiamato con l'alias idDocumento
	 */
	private String documentTitle;
	
	
	/**
	 * Solo per i documenti protocollati
	 * 
	 * Parte di una chiave composita (idAoo, numeroProtocollo, annoProtocollo) che identifica il documento inteso come insieme di tutte le sue versioni
	 * 
	 * La chiave composita e il documentTitle rappresentano la stessa cosa
	 * 
	 */
	private Integer idAOO;
	
	
	/**
	 * Solo per i documenti protocollati
	 * 
	 * Parte di una chiave composita (idAoo, numeroProtocollo, annoProtocollo) che identifica il documento inteso come insieme di tutte le sue versioni
	 * 
	 */
	private Integer annoProtocollo;
	
	
	/**
	 * Solo per i documenti protocollati
	 * 
	 * Parte di una chiave composita (idAoo, numeroProtocollo, annoProtocollo) che identifica il documento inteso come insieme di tutte le sue versioni
	 * 
	 */
	private Integer numeroProtocollo;
	
	
	/**
	 * Usato dall'utente per identificare il documento, inteso come insieme delle sue verisoni
	 * 
	 * Il numerodocumento viene resettato ogni tot per fare in modo che rimanga un numero basso memorizzabile dall'utente 
 	 */
	private Integer numeroDocumento;
	
	
	/**
	 * 
	 */
	private Date dataCreazione;
	
	/**
	 * Costruttore.
	 * @param indocumentTitle - document title
	 * @param innumeroDocumento - numero documento
	 * @param inidAOO id dell'Aoo
	 * @param inannoProtocollo - anno di protocollo
	 * @param innumeroProtocollo - numero di protocollo
	 * @param inDataCreazione - data di creazione
	 */
	public IdDocumentDTO(final String indocumentTitle, final Integer innumeroDocumento, final Integer inidAOO, final Integer inannoProtocollo, 
			final Integer innumeroProtocollo, final Date inDataCreazione) {
		this.documentTitle = indocumentTitle;
		this.idAOO = inidAOO;
		this.annoProtocollo = inannoProtocollo;
		this.numeroProtocollo = innumeroProtocollo; 
		this.numeroDocumento = innumeroDocumento;
		this.dataCreazione = inDataCreazione;
	}

	/**
	 * Recupera il document title.
	 * @return document title
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Imposta il document title.
	 * @param documentTitle docuement title
	 */
	public void setDocumentTitle(final String documentTitle) {
		this.documentTitle = documentTitle;
	}

	/**
	 * Recupera l'anno di protocollo.
	 * @return anno di protocollo
	 */
	public Integer getAnnoProtocollo() {
		return annoProtocollo;
	}

	/**
	 * Imposta l'anno di protocollo.
	 * @param annoProtocollo anno di protocollo
	 */
	public void setAnnoProtocollo(final Integer annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}

	/**
	 * Recupera il numero documento.
	 * @return numero documento
	 */
	public Integer getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * Imposta il numero documento.
	 * @param numeroDocumento numero documento
	 */
	public void setNumeroDocumento(final Integer numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * Recupera il numero di protocollo.
	 * @return numero documento
	 */
	public Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * Imposta il numero di protocollo.
	 * @param numeroProtocollo numero di protocollo
	 */
	public void setNumeroProtocollo(final Integer numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}

	/**
	 * Recupera l'id dell'Aoo.
	 * @return id dell'Aoo
	 */
	public Integer getIdAOO() {
		return idAOO;
	}

	/**
	 * Imposta l'id dell'Aoo.
	 * @param idAOO id dell'Aoo
	 */
	public void setIdAOO(final Integer idAOO) {
		this.idAOO = idAOO;
	}

	/**
	 * Recupera la data di creazione.
	 * @return data di creazione
	 */
	public Date getDataCreazione() {
		return dataCreazione;
	}
}
