package it.ibm.red.business.dto;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.enums.LookupTablePresentationModeEnum;
import it.ibm.red.business.enums.StringMatchModeEnum;
import it.ibm.red.business.enums.TipoDocumentoModeEnum;
import it.ibm.red.business.enums.TipoMetadatoEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.DateUtils;

/**
 * DTO che definisce un Metadato o attributo esteso.
 */
public class MetadatoDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 4110454755356607522L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(MetadatoDTO.class.getName());
	
	/**
	 * Id metadato
	 */
	private Integer id;

	/**
	 * Valore selezionato.
	 */
	private transient Object selectedValue;
	
	/**
	 * Nome del metadato.
	 */
	private String name;
	
	/**
	 * Tipologia di dato del metadato.
	 */
	private TipoMetadatoEnum type;
	
	/**
	 * Dimensione del valore del metadato (se il tipo di dato del metadato ammette tale caratterizzazione).
	 */
	private Integer dimension;
	
	/**
	 * Indica se per il metadato è necessario implementare la logica di range.
	 */
	private Boolean flagRange;
	
	/**
	 * Indica se il metadato è uno di quelli caratteristici del tipo di documento
	 */
	private Boolean flagOut;
	
	/**
	 * Display name del metadato.
	 */
	private String displayName;

	/**
	 * Caratterizzazione della lista utente.
	 */
	private String lookupTableSelector;
	
	/**
	 * Combobox o datatable.
	 */
	private LookupTablePresentationModeEnum lookupTableMode;
	
	/**
	 * Visibilità del metadato.
	 */
	private TipoDocumentoModeEnum visibility; // 
	
	/**
	 * Editabilità del metadato.
	 */
	private TipoDocumentoModeEnum editability;
	
	/**
	 * Obbligatorietà del metadato.
	 */
	private TipoDocumentoModeEnum obligatoriness;
	
	/**
	 * Tipologia di ricerca per stringhe.
	 */
	private StringMatchModeEnum stringMatchMode;
	
	/**
	 * In fase di ricerca questo flag viene utilizzato per indicare che questo è il limite inferiore del range 
	 */
	private Boolean flagRangeStart;
	
	/**
	 * Flag limite superiore range (vedi flagRangeStart)
	 */
	private Boolean flagRangeStop;
	
	/**
	 * Flag che indica l'appartenenza ad una tripletta di metadati valuta.
	 */
	private boolean metadatoValutaFlag;

	/**
	 * Costruttore del metadato completo.
	 * 
	 * @param name
	 *            nome metadato
	 * @param type
	 *            tipo metadato, @see TipoMetadatoEnum
	 * @param displayName
	 *            display name del metadato
	 * @param lookupTableSelector
	 *            selector del metadato se di tipo
	 *            {@link TipoMetadatoEnum#LOOKUP_TABLE}
	 * @param lookupTableMode
	 *            modalità di visualizzazione lookup se di tipo
	 *            {@link TipoMetadatoEnum#LOOKUP_TABLE}
	 * @param dimension
	 *            dimensione massima del metadato
	 * @param visibility
	 *            visibilità del metadato
	 * @param editability
	 *            editabilità del metadato
	 * @param obligatoriness
	 *            obbligatorietà del metadato
	 * @param flagRange
	 *            flag range del metadato
	 * @param flagOut
	 *            flag out del metadato
	 * @param stringMatchMode
	 *            modalità di match ricerca delle String
	 */
	public MetadatoDTO(final Integer id, final String name, final TipoMetadatoEnum type, final String displayName, final String lookupTableSelector,
			final LookupTablePresentationModeEnum lookupTableMode, final Integer dimension, final TipoDocumentoModeEnum visibility,
			final TipoDocumentoModeEnum editability, final TipoDocumentoModeEnum obligatoriness, final Boolean flagRange,
			final Boolean flagOut, final StringMatchModeEnum stringMatchMode, final boolean inIsMetadatoValuta) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.displayName = displayName;
		this.lookupTableSelector = lookupTableSelector;
		this.lookupTableMode = lookupTableMode;
		this.dimension = dimension;
		this.visibility = visibility;
		this.editability = editability;
		this.obligatoriness = obligatoriness;
		this.flagRange = flagRange;
		this.flagOut = flagOut;
		this.flagRangeStart = false;
		this.flagRangeStop = false;
		this.stringMatchMode = stringMatchMode;
		this.metadatoValutaFlag = inIsMetadatoValuta;
	}

	/**
	 * Costruttore copia.
	 * 
	 * @param newMetadato
	 *            metadato dal quale effettuare la copia
	 */
	public MetadatoDTO(final MetadatoDTO newMetadato) {
		this(newMetadato.getId(), newMetadato.getName(), newMetadato.getType(), newMetadato.getDisplayName(), newMetadato.getLookupTableSelector(), 
				newMetadato.getLookupTableMode(), newMetadato.getDimension(), newMetadato.getVisibility(), 
				newMetadato.getEditability(), newMetadato.getObligatoriness(), newMetadato.getFlagRange(), newMetadato.getFlagOut(), 
				newMetadato.getStringMatchMode(), newMetadato.isMetadatoValutaFlag());
	}
	
	/**
	 * Costruttore vuoto, inizalizza i parametri per evitare null.
	 */
	public MetadatoDTO() {
		this.stringMatchMode = StringMatchModeEnum.CONTAINS;
		this.lookupTableMode = LookupTablePresentationModeEnum.DATATABLE;
		this.visibility = TipoDocumentoModeEnum.SEMPRE;
		this.editability = TipoDocumentoModeEnum.SEMPRE;
		this.obligatoriness = TipoDocumentoModeEnum.MAI;
		this.flagRange = false;
		this.flagOut = false;
		this.flagRangeStart = false;
		this.flagRangeStop = false;
	}

	/**
	 * Costruttore.
	 * 
	 * @param name
	 *            nome metadato
	 * @param type
	 *            tipo metadato, @see TipoMetadatoEnum
	 * @param displayName
	 *            display name del metadato
	 * @param selectedValue
	 *            valore selezionato per il metadato
	 * @param visibility
	 *            visibilità del metadato
	 * @param editability
	 *            possibilità di modifica del metadato
	 * @param obligatoriness
	 *            obbligatorietà del metadato
	 */
	public MetadatoDTO(final String name, final TipoMetadatoEnum type, final String displayName, final Object selectedValue, 
			final TipoDocumentoModeEnum visibility, final TipoDocumentoModeEnum editability, final TipoDocumentoModeEnum obligatoriness) {
		this();
		this.name = name;
		this.displayName = displayName;
		this.type = type;
		this.selectedValue = selectedValue;
		this.visibility = visibility;
		this.editability = editability;
		this.obligatoriness = obligatoriness;
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param name
	 *            nome metadato
	 * @param type
	 *            tipo metadato
	 * @param selectedValue
	 *            valore del metadato
	 */
	public MetadatoDTO(final String name, final TipoMetadatoEnum type, final Object selectedValue) {
		this();
		this.name = name;
		this.type = type;
		this.selectedValue = selectedValue;
	}

	/**
	 * Restituisce l'id del metadato.
	 * 
	 * @return id metadato
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Imposta l'id del metadato.
	 * 
	 * @param id
	 *            identificativo da impostare
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * Restituisce il nome proprio del metadato.
	 * 
	 * @return nome metadato
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Restituisce il display name per la visualizzazione in front end.
	 * 
	 * @return display name
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * Restituisce un nome sempre valido per la gestione dello stesso in front-end.
	 * 
	 * @return display name da visualizzare
	 */
	public String getDisplayNameView() {
		String out = displayName;
		
		if (StringUtils.isBlank(out)) {
			out = name;
		}
		
		if (Boolean.TRUE.equals(flagRangeStart)) {
			out += " da";
		} else if (Boolean.TRUE.equals(flagRangeStop)) {
			out += " a";
		}
		
		return out;
	}

	/**
	 * Imposta il nome proprio del metadato.
	 * 
	 * @param name
	 *            nome da impostare al metadato
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * Restituisce il selettore delle liste utenti se valorizzata.
	 * 
	 * @return selettore liste utenti
	 */
	public String getLookupTableSelector() {
		return lookupTableSelector;
	}

	/**
	 * Imposta il selettore delle liste utenti.
	 * 
	 * @param lookupTableSelector
	 *            selector da impostare sul metadato
	 */
	public void setLookupTableSelector(final String lookupTableSelector) {
		this.lookupTableSelector = lookupTableSelector;
	}

	/**
	 * Restituisce la dimensione del metadato. Per alcuni tipi di metadato questo
	 * parametro è null poiché non ha senso parlare di dimensione. LOOKUP_TABLE,
	 * CAPITOLI_SELECTOR, DATE e PERSONE_SELECTOR non hanno dimensione. @see
	 * TipoMetadatoEnum.
	 * 
	 * @return dimensione metadato
	 */
	public Integer getDimension() {
		return dimension;
	}

	/**
	 * Imposta la dimensione del metadato.
	 * 
	 * @param dimension
	 *            dimensione da impostare sul metadato
	 */
	public void setDimension(final Integer dimension) {
		this.dimension = dimension;
	}

	/**
	 * Restituisce true se occorre gestire il metadato come un range. Valido per
	 * DATE, INTEGER, DOUBLE. @see TipoMetadatoEnum.
	 * 
	 * @return true se il metadato è utilizzato come range, false altrimenti.
	 */
	public Boolean getFlagRange() {
		return flagRange;
	}

	/**
	 * Imposta il flag associato alla modalita range del metadato.
	 * 
	 * @param flagRange
	 *            flag da impostare sul metadato
	 */
	public void setFlagRange(final Boolean flagRange) {
		this.flagRange = flagRange;
	}

	/**
	 * Restituisce true se un metadato è etichettato come OUT, se true questo
	 * metadato verrà mostrato nel master del documento, se false verrà mostrato
	 * solo nel dettaglio.
	 * 
	 * @return <code> true </code> se in fase di ricerca, nella presentazione dei
	 *         risultati verrà visualizzato tra le colonne della ricerca avanzata
	 *         ucb, <code> false </code> se, invece, verrà trattato come un normale
	 *         metadato
	 */
	public Boolean getFlagOut() {
		return flagOut;
	}

	/**
	 * Imposta il flag che definisce il tipo di visibilita del metadato.
	 * 
	 * @param flagOut
	 *            se <code> true </code> in fase di ricerca, nella presentazione dei
	 *            risultati verrà visualizzato tra le colonne della ricerca avanzata
	 *            ucb, se <code> false </code> verrà trattato come un normale
	 *            metadato
	 */
	public void setFlagOut(final Boolean flagOut) {
		this.flagOut = flagOut;
	}

	/**
	 * Restituisce il tipo del metadato.
	 * 
	 * @return tipo metadato
	 */
	public TipoMetadatoEnum getType() {
		return type;
	}

	/**
	 * Imposta il tipo del metadato.
	 * 
	 * @param type
	 *            da impostare
	 */
	public void setType(final TipoMetadatoEnum type) {
		this.type = type;
	}

	/**
	 * Imposta il display name del metadato.
	 * 
	 * @param displayName
	 *            display name da impostare
	 */
	public void setDisplayName(final String displayName) {
		this.displayName = displayName;
	}

	/**
	 * Restituisce la modalita di visualizzazione delle liste utenti qualora il
	 * metadato dovesse essere LOOKUP_TABLE.
	 * 
	 * @return modalita di visualizzazione delle lookupTable.
	 */
	public LookupTablePresentationModeEnum getLookupTableMode() {
		return lookupTableMode;
	}

	/**
	 * Imposta la modalita di visualizzazione delle liste utenti.
	 * 
	 * @param lookupTableMode
	 *            modalità di presentazione della lista utente
	 */
	public void setLookupTableMode(final LookupTablePresentationModeEnum lookupTableMode) {
		this.lookupTableMode = lookupTableMode;
	}

	/**
	 * Restituisce la visibilita del metadato.
	 * 
	 * @return visibilita metadato
	 */
	public TipoDocumentoModeEnum getVisibility() {
		return visibility;
	}

	/**
	 * Imposta la visibilita del metadato.
	 * 
	 * @param visibility
	 *            visibilità da impostare
	 */
	public void setVisibility(final TipoDocumentoModeEnum visibility) {
		this.visibility = visibility;
	}

	/**
	 * Restituisce l'editabilita del metadato.
	 * 
	 * @return editabilita metadato
	 */
	public TipoDocumentoModeEnum getEditability() {
		return editability;
	}

	/**
	 * Imposta l'editabilita del metadato.
	 * 
	 * @param editability
	 *            editabilità da impostare
	 */
	public void setEditability(final TipoDocumentoModeEnum editability) {
		this.editability = editability;
	}

	/**
	 * Restituisce l'obbligatorieta del metadato.
	 * 
	 * @return obbligatorieta del metadato
	 */
	public TipoDocumentoModeEnum getObligatoriness() {
		return obligatoriness;
	}

	/**
	 * Imposta l'obbligatorieta del metadato.
	 * 
	 * @param obligatoriness
	 *            obbligatorietà da impostare
	 */
	public void setObligatoriness(final TipoDocumentoModeEnum obligatoriness) {
		this.obligatoriness = obligatoriness;
	}

	/**
	 * Restituisce la modalita di ricerca delle stringhe.
	 * 
	 * @return modalita ricerca stringhe
	 */
	public StringMatchModeEnum getStringMatchMode() {
		return stringMatchMode;
	}

	/**
	 * Imposta la modalita di ricerca delle stringhe.
	 * 
	 * @param stringMatchMode
	 *            modalità da impostare sul metadato
	 */
	public void setStringMatchMode(final StringMatchModeEnum stringMatchMode) {
		this.stringMatchMode = stringMatchMode;
	}

	/**
	 * Restituisce <code> true </code> se il metadato è identificato come la parte iniziale del range, ha senso solo se il metadato
	 * ha {@link #flagRange} impostato a <code> true </code>.
	 * @return <code> true </code> se il metadato è associato alla parte iniziale del range, <code> false </code> altrimenti
	 */
	public Boolean getFlagRangeStart() {
		return flagRangeStart;
	}

	/**
	 * Imposta il flag associato alla parte iniziale del range del metadato.
	 * 
	 * @param flagRangeStart
	 *            flag range da impostare
	 */
	public void setFlagRangeStart(final Boolean flagRangeStart) {
		this.flagRangeStart = flagRangeStart;
	}

	/**
	 * Restituisce <code> true </code> se il metadato è identificato come la parte finale del range, ha senso solo se il metadato
	 * ha {@link #flagRange} impostato a <code> true </code>.
	 * @return <code> true </code> se il metadato è associato alla parte finale del range, <code> false </code> altrimenti
	 */
	public Boolean getFlagRangeStop() {
		return flagRangeStop;
	}

	/**
	 * Imposta il flag associato alla parte finale del range del metadato.
	 * 
	 * @param flagRangeStop
	 *            flag range da impostare
	 */
	public void setFlagRangeStop(final Boolean flagRangeStop) {
		this.flagRangeStop = flagRangeStop;
	}

	/**
	 * Restituisce il valore selezionato tra i possibili associati al metadato.
	 * 
	 * @return valore selezionato
	 */
	public Object getSelectedValue() {
		return selectedValue;
	}

	/**
	 * Imposta il valore selezionato del metadato.
	 * 
	 * @param selectedValue
	 *            valore selezionato da impostare
	 */
	public void setSelectedValue(final Object selectedValue) {
		this.selectedValue = selectedValue;
	}

	/**
	 * Restituisce il flag {@link #metadatoValutaFlag}
	 * 
	 * @return isMetadatoValuta
	 */
	public boolean isMetadatoValutaFlag() {
		return metadatoValutaFlag;
	}

	/**
	 * Imposta il flag {@link #metadatoValutaFlag}
	 * 
	 * @param inMetadatoValutaFlag
	 *            flag da impostare
	 */
	public void setMetadatoValutaFlag(boolean inMetadatoValutaFlag) {
		this.metadatoValutaFlag = inMetadatoValutaFlag;
	}

	/**
	 * Restituisce il valore selezionato come String. Questo metodo viene ridefinito
	 * per metadati la cui rappresentazione non è banalmente identificata dal valore
	 * selezionato. Per {@link TipoMetadatoEnum#LOOKUP_TABLE},
	 * {@link TipoMetadatoEnum#CAPITOLI_SELECTOR} e
	 * {@link TipoMetadatoEnum#PERSONE_SELECTOR} questo metodo deve essere
	 * ridefinito e restituiranno una rappresentazione del valore come xml.
	 * 
	 * @return rappresentazione del valore selezionato come String
	 */
	public String getValue4AttrExt() {
		if (TipoMetadatoEnum.DATE.equals(getType())) {
			try {
				final SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.DD_MM_YYYY);
				return sdf.format((Date) getSelectedValue());
			} catch (final Exception e) {
				LOGGER.warn(e);
				return (getSelectedValue() != null) ? getSelectedValue().toString() : null;
			}
		} else if (TipoMetadatoEnum.DOUBLE.equals(getType())) {
			try {  
				DecimalFormat df = new DecimalFormat("0.00");
				String valueDouble = df.format(getSelectedValue());
				valueDouble = valueDouble.replace(",", ".");
				return valueDouble;
			} catch (final Exception e) {
				LOGGER.warn(e);
				return (getSelectedValue() != null) ? getSelectedValue().toString() : null;
			}
		} else {
			return (getSelectedValue() != null) ? getSelectedValue().toString() : null;
		}
	}

}
