package it.ibm.red.business.persistence.model;

import java.io.Serializable;

/**
 * The Class UtenteFirma.
 *
 * @author CPIERASC
 * 
 *         Entità che mappa la tabella UTENTEFIRMA.
 */
public class UtenteFirma implements Serializable {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Identificativo utente.
	 */
	private final long idUtente;

	/**
	 * Glifo firma.
	 */
	private final byte[] imageFirma;
	
	/**
	 * Identificativo utente delegante.
	 */
	private final Long idUtenteDelegante;
	
	/**
	 * Url handler firma.
	 */
	private final String urlHandler;
	
	/**
	 * Identificativo Signer.
	 */
	private final String signer;
	
	/**
	 * Pin signer.
	 */
	private String pin;
	
	/**
	 * Signer pin -> otp.
	 */
	private final String signerPin;
	
	/**
	 * Reason (valore da inserire nel campo reason della firma).
	 */
	private final String reason;
	
	/**
	 * Informazioni sul customer (valore da inserire nel campo customerInfo della firma).
	 */
	private final String customerInfo;

	/**
	 * Certificato da utilizzare in fase di creazione dell'oggetto pkbox.
	 */
	private final byte[] securePinCert;
	
	/**
	 * Pin verificato.
	 */
	private boolean pinVerificato;

	/**
	 * Costruttore.
	 * 
	 * @param inIdUtente			identificativo utente
	 * @param inImageFirma			glifo firma
	 * @param inIdUtenteDelegante	identificativo utente delegante
	 * @param inUrlHandler			url handler
	 * @param inSigner				signer
	 * @param inPin					pin
	 * @param inSignerPin			signerpin
	 * @param inReason				reason campo firma
	 * @param inCustomerInfo		customer info campo firma
	 * @param inSecurePinCert		secure pin pkbox
	 * @param inPinVerificato		pin verificato
	 */
	public UtenteFirma(final long inIdUtente, final byte[] inImageFirma, final Long inIdUtenteDelegante, final String inUrlHandler, final String inSigner,
			final String inPin, final String inSignerPin, final String inReason, final String inCustomerInfo, final byte[] inSecurePinCert,
			final boolean inPinVerificato) {
		super();
		this.idUtente = inIdUtente;
		this.imageFirma = inImageFirma;
		this.idUtenteDelegante = inIdUtenteDelegante;
		this.urlHandler = inUrlHandler;
		this.signer = inSigner;
		this.pin = inPin;
		this.signerPin = inSignerPin;
		this.reason = inReason;
		this.customerInfo = inCustomerInfo;
		this.securePinCert = inSecurePinCert;
		this.pinVerificato = inPinVerificato;
	}

	/**
	 * Getter url handler.
	 * 
	 * @return url handler
	 */
	public final String getUrlHandler() {
		return urlHandler;
	}

	/**
	 * Getter signer.
	 * 
	 * @return signer
	 */
	public final String getSigner() {
		return signer;
	}

	/**
	 * Getter pin.
	 * 
	 * @return pin
	 */
	public final String getPin() {
		return pin;
	}

	/**
	 * Getter signer pin.
	 * 
	 * @return signer pin
	 */
	public final String getSignerPin() {
		return signerPin;
	}

	/**
	 * Getter reason.
	 * 
	 * @return reason
	 */
	public final String getReason() {
		return reason;
	}

	/**
	 * Getter customer info.
	 * 
	 * @return customerInfo
	 */
	public final String getCustomerInfo() {
		return customerInfo;
	}

	/**
	 * Getter id utente.
	 * 
	 * @return	id utente
	 */
	public final long getIdUtente() {
		return idUtente;
	}

	/**
	 * Getter glifo firma.
	 * 
	 * @return	glifo firma
	 */
	public final byte[] getImageFirma() {
		return imageFirma;
	}

	/**
	 * Getter id utente delegante.
	 * 
	 * @return	id utente delegante
	 */
	public final Long getIdUtenteDelegante() {
		return idUtenteDelegante;
	}

	/**
	 * Getter secure pin pkbox.
	 * 
	 * @return	secure pin pkbox
	 */
	public final byte[] getSecurePinCert() {
		return securePinCert;
	}

	/**
	 * Getter PIN verificato.
	 * @return pin verificato
	 */
	public final boolean isPinVerificato() {
		return pinVerificato;
	}
	
	/**
	 * Imposta il flag associato alla verifica del PIN.
	 * @param pinVerificato
	 */
	public void setPinVerificato(final Boolean pinVerificato) {
		this.pinVerificato = pinVerificato;
	}
	
	/**
	 * Imposta il flag associato alla verifica del PIN.
	 * @param pin
	 */
	public void setPin(final String pin) {
		this.pin = pin;
	}

}
