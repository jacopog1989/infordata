/**
 * @author CPIERASC
 *
 *	In questo package avremo le eccezioni applicative; la strategia utilizzata per la gestione delle eccezioni è la
 *	seguente: nel punto in cui si genera l'eccezione questa viene loggata come erroe e rilanciata wrappandola attraverso
 *	una eccezione runtime specifica, oppure la generica RedException; in questo modo non necessiteremo di usare
 *	l'approccio catch-or-declare nella pila dei chiamanti, lasciando al punto di contatto con il front end la scelta di
 *	come gestire l'eccezione generata (ad esempio visualizzandola attraverso un dialog opportunamente customizzato per
 *	evidenziare la presenza di un errore applicativo).
 *	
 */
package it.ibm.red.business.exception;
