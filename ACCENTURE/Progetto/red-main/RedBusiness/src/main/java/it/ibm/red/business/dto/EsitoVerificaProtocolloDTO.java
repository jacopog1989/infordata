package it.ibm.red.business.dto;

/**
 * DTO esito verifica protocollo.
 */
public class EsitoVerificaProtocolloDTO extends EsitoDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1604131231558260163L;
	
	/**
	 * Flag che identificato il tipo categoria del documento da verificare.
	 */
	private boolean isEntrata;

	/**
	 * Documento.
	 */
	private DetailDocumentRedDTO document;
	
	/**
	 * Costruttore del DTO.
	 */
	public EsitoVerificaProtocolloDTO() {
		super();
	}
	
	/**
	 * Recupera il documento.
	 * @return documento
	 */
	public DetailDocumentRedDTO getDocument() {
		return document;
	}

	/**
	 * Imposta il documento.
	 * @param document documento
	 */
	public void setDocument(final DetailDocumentRedDTO document) {
		this.document = document;
	}

	/**
	 * Restituisce true se l'esito è associato ad un allaccio in entrata, false altrimenti.
	 * @return true se associato ad allaccio in entrata, false altrimenti
	 */
	public boolean isEntrata() {
		return isEntrata;
	}

	/**
	 * Imposta il flag associato al tipo categoria dell'allaccio.
	 * @param isEntrata
	 */
	public void setEntrata(boolean isEntrata) {
		this.isEntrata = isEntrata;
	}
}
