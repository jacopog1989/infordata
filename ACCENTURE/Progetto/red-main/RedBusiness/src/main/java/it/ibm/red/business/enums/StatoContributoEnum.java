/**
 * 
 */
package it.ibm.red.business.enums;

/**
 * @author APerquoti
 *
 */
public enum StatoContributoEnum {

	/**
	 * Valore.
	 */
	MODIFICATO(1),
	
	/**
	 * Valore.
	 */
	VALIDATO(2),
	
	/**
	 * Valore.
	 */
	ELIMINATO(3);
	
	/**
	 * Identificativo.
	 */
	private Integer id;
	
	/**
	 * Costruttore.
	 * @param inId
	 */
	StatoContributoEnum(final Integer inId) {
		id = inId;
	}
	
	/**
	 * Restituisce l'id dell'enum.
	 * @return id
	 */
	public Integer getId() {
		return id;
	}
}