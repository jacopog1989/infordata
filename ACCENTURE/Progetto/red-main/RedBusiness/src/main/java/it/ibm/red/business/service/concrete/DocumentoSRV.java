package it.ibm.red.business.service.concrete;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.collection.AnnotationSet;
import com.filenet.api.collection.ContentElementList;
import com.filenet.api.collection.DocumentSet;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.Annotation;
import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.Document;
import com.filenet.api.property.FilterElement;
import com.filenet.api.property.PropertyFilter;
import com.filenet.apiimpl.core.SubSetImpl;
import com.google.common.net.MediaType;

import filenet.vw.api.VWQueueQuery;
import filenet.vw.api.VWRosterQuery;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.BooleanFlag;
import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.dao.IAllaccioDAO;
import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.IDocumentoDAO;
import it.ibm.red.business.dao.IEventoLogDAO;
import it.ibm.red.business.dao.IFascicoloDAO;
import it.ibm.red.business.dao.ILookupDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IStoricoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.ApprovazioneDTO;
import it.ibm.red.business.dto.AttachmentDTO;
import it.ibm.red.business.dto.ContributoDTO;
import it.ibm.red.business.dto.DetailDocumentoDTO;
import it.ibm.red.business.dto.DocumentoFascicoloDTO;
import it.ibm.red.business.dto.EventoLogDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.GestioneLockDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.MasterDocumentoDTO;
import it.ibm.red.business.dto.PEDocumentoDTO;
import it.ibm.red.business.dto.PlaceholderInfoDTO;
import it.ibm.red.business.dto.ProtocolloDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.StepDTO;
import it.ibm.red.business.dto.filenet.StoricoDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.FilenetAnnotationEnum;
import it.ibm.red.business.enums.IconaStatoEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.RicercaGenericaTypeEnum;
import it.ibm.red.business.enums.StatoDocumentoFuoriLibroFirmaEnum;
import it.ibm.red.business.enums.TipoApprovazioneEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoOperazioneLibroFirmaEnum;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.exception.FilenetException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.exception.SearchException;
import it.ibm.red.business.helper.adobe.AdobeLCHelper;
import it.ibm.red.business.helper.adobe.IAdobeLCHelper;
import it.ibm.red.business.helper.adobe.dto.approvazioni.ApprovazioneType;
import it.ibm.red.business.helper.adobe.dto.approvazioni.ApprovazioniType;
import it.ibm.red.business.helper.adobe.dto.approvazioni.FormType;
import it.ibm.red.business.helper.adobe.dto.approvazioni.ObjectFactory;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.TrasformPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.Ruolo;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.persistence.model.UtenteFirma;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAssegnaUfficioSRV;
import it.ibm.red.business.service.IContributoSRV;
import it.ibm.red.business.service.IDocumentoSRV;
import it.ibm.red.business.service.IListaDocumentiSRV;
import it.ibm.red.business.service.ILookupSRV;
import it.ibm.red.business.service.INodoSRV;
import it.ibm.red.business.service.INpsSRV;
import it.ibm.red.business.service.ISignSRV;
import it.ibm.red.business.service.IStampigliaturaSegnoGraficoSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.service.facade.IStoricoFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class DocumentoSRV.
 *
 * @author CPIERASC
 * 
 *         Service per la gestione dei documenti.
 */
@Service
public class DocumentoSRV extends AbstractService implements IDocumentoSRV {

	private static final String ERRORE_DURANTE_IL_RECUPERO_DEL_CONTENT_DEL_DOCUMENTO_CON_DOCUMENT_TITLE = "Errore durante il recupero del content del documento con Document Title: ";

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -8865512104011215519L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DocumentoSRV.class.getName());

	/**
	 * Messaggio errore durante la procedura di protocollazione.
	 */
	private static final String ERROR_STACCO_PROTOCOLLO_MSG = "Eccezione durante lo stacco del protocollo";

	/**
	 * Messaggio errore recupero content del documento. Occorre appendere il
	 * document title del documento che ha generato l'errore a valle del messaggio.
	 */
	private static final String ERROR_RECUPERO_CONTENT_MSG = ERRORE_DURANTE_IL_RECUPERO_DEL_CONTENT_DEL_DOCUMENTO_CON_DOCUMENT_TITLE;

	/**
	 * Messaggio errore recupero lock.
	 */
	private static final String ERROR_RECUPERO_LOCK_MSG = "Errore durante il recupero del lock del documento :";

	/**
	 * Servizio per la gestione dei contributi.
	 */
	@Autowired
	private IContributoSRV contributoSRV;

	/**
	 * Servizio per la gestione dello storico.
	 */
	@Autowired
	private IStoricoFacadeSRV storicoSRV;

	/**
	 * Dao gestione fascicolo.
	 */
	@Autowired
	private IFascicoloDAO fascicoloDAO;

	/**
	 * Dao gestione allaccio.
	 */
	@Autowired
	private IAllaccioDAO allaccioDAO;

	/**
	 * Dao gestione lookup.
	 */
	@Autowired
	private ILookupDAO lookupDAO;

	/**
	 * Dao gestione nodo.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * Dao gestione storico.
	 */
	@Autowired
	private IStoricoDAO storicoDAO;

	/**
	 * Dao gestione utente.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;

	/**
	 * Dao gestione eventi.
	 */
	@Autowired
	private IEventoLogDAO eventoLogDAO;

	/**
	 * Service gestione firma.
	 */
	@Autowired
	private ISignSRV signSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;

	/**
	 * Service.
	 */
	@Autowired
	private INodoSRV nodoSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ILookupSRV lookupSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IAssegnaUfficioSRV assegnaUfficioSRV;
	/**
	 * NpsSRV.
	 */
	@Autowired
	private INpsSRV npsSRV;

	/**
	 * Dao gestione aoo.
	 */
	@Autowired
	private IAooDAO aooDAO;

	/**
	 * Service.
	 */
	@Autowired
	private IListaDocumentiSRV listaDocumentiSRV;

	/**
	 * Dao.
	 */
	@Autowired
	private IDocumentoDAO documentoDAO;

	/**
	 * Service.
	 */
	@Autowired
	private IStampigliaturaSegnoGraficoSRV stampigliaturaSiglaSRV;

	/**
	 * Recupero content da filenet.
	 * 
	 * @param fcDTO						credenziali FileNet
	 * @param documentClass			classe documentale
	 * @param documentTitle			identificativo documento
	 * @param onlyPdf				posto a true impone la ricerca della velina in assenza di un content pdf
	 * @param nomeFile				posto a true recupera il metadato che contiene il nome del file
	 * @param contentLibroFirma		posto a true impone il recupero dell'annotation CONTENT_LIBRO_FIRMA come content del documento al posto dell'originale
	 * @return				file
	 */
	private static FileDTO getContentPreview(final FilenetCredentialsDTO fcDTO, final String documentClass, final String documentTitle, final boolean onlyPdf,
			final boolean nomeFile, final Long idAoo, final boolean contentLibroFirma) {
		FileDTO output = null;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(fcDTO, idAoo);

			final Document d = fceh.getDocumentForDownload(documentClass, documentTitle, nomeFile);
			output = getContentFromDocument(d, onlyPdf, contentLibroFirma);
		} catch (final FilenetException e) {
			LOGGER.error(ERROR_RECUPERO_CONTENT_MSG + documentTitle, e);
			throw e;
		} finally {
			if (fceh != null) {
				fceh.popSubject();
			}
		}

		return output;
	}
	
	/**
	 * Recupero content da filenet.
	 * 
	 * @param fcDTO			credenziali filenet
	 * @param documentClass	classe documentale
	 * @param documentTitle	identificativo documento
	 * @param onlyPdf		posto a true impone la ricerca della velina in assenza di un content pdf
	 * @param nomeFile		posto a true recupera il metadato che contiene il nome del file
	 * @return				file
	 */
	private static FileDTO getAllegatoP7MPreview(final IFilenetCEHelper fcehInput, final FilenetCredentialsDTO fcDTO, final String documentClass, final String documentTitle, final Boolean onlyPdf,
			final boolean nomeFile,final Long idAoo) {
		FileDTO output = null;
		IFilenetCEHelper fceh = null;
		
		try {
			if(fcehInput == null) {
				fceh =  FilenetCEHelperProxy.newInstance(fcDTO,idAoo);
			}else {
				fceh = fcehInput;
			}
			
			Document d = fceh.getDocumentForDownload(documentClass, documentTitle, nomeFile);
			output = getContentFromDocumentAllegato(d, onlyPdf);
		} catch (FilenetException e) {
			LOGGER.error(ERROR_RECUPERO_CONTENT_MSG + documentTitle, e);
			throw e;
		} finally {
			if (fceh != null && fcehInput==null) {
				fceh.popSubject();
			}
		}
		
		return output;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IDocumentoFacadeSRV#getContentPreviewAllegato(it.ibm.red.business.dto.FilenetCredentialsDTO,
	 *      java.lang.String, java.lang.Boolean, boolean, java.lang.Long, boolean).
	 */
	@Override
	public FileDTO getContentPreviewAllegato(final IFilenetCEHelper fcehInput, final FilenetCredentialsDTO fcDTO, final String guid, final Boolean onlyPdf, final boolean nomeFile, 
			final Long idAoo, final boolean contentLibroFirma) {
		FileDTO output = null;
		IFilenetCEHelper fceh = null;

		try {
			if(fcehInput == null) {
				fceh = FilenetCEHelperProxy.newInstance(fcDTO, idAoo);
			}else {
				fceh = fcehInput;
			}

			final Document d = fceh.getDocumentForDownloadAllegato(guid, nomeFile);
			output = getContentFromDocument(d, onlyPdf, contentLibroFirma);
		} catch (final FilenetException e) {
			LOGGER.error(ERROR_RECUPERO_CONTENT_MSG + guid, e);
			throw e;
		} finally {
			if(fcehInput == null) {
				popSubject(fceh);
			}
		}

		return output;
	}
	
	@Override
	public FileDTO getContentPreviewAllegato(final FilenetCredentialsDTO fcDTO, final String guid, final Boolean onlyPdf, final boolean nomeFile, 
			final Long idAoo, final boolean contentLibroFirma) {
				return getContentPreviewAllegato(null, fcDTO,  guid, onlyPdf, nomeFile, idAoo,contentLibroFirma) ;	
		}
	@Override
		public FileDTO getContentPreviewAllegato(final IFilenetCEHelper fcehInput, final String guid, final Boolean onlyPdf, final boolean nomeFile, 
				final Long idAoo, final boolean contentLibroFirma) {	
				return getContentPreviewAllegato(fcehInput, null,  guid, onlyPdf, nomeFile, idAoo,contentLibroFirma) ;	
		}
	
	/**
	 * Gets the documento content preview.
	 *
	 * @param fcDTO
	 *            the fc DTO
	 * @param documentTitle
	 *            the document title
	 * @param onlyPdf
	 *            the only pdf
	 * @return the documento content preview
	 */
	@Override
	public final FileDTO getAllegatoP7MPreview(final FilenetCredentialsDTO fcDTO, final String documentTitle, final Boolean onlyPdf,final Long idAoo) {
		return getAllegatoP7MPreview(null, fcDTO, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY),
				documentTitle, onlyPdf, true,idAoo);
	}
	
	@Override
	public final FileDTO getAllegatoP7MPreview(final IFilenetCEHelper fcehInput , final String documentTitle, final Boolean onlyPdf,final Long idAoo) {
		return getAllegatoP7MPreview(fcehInput, null, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY),
				documentTitle, onlyPdf, true,idAoo);
	}
	
	private static FileDTO getContentFromDocumentAllegato(final Document d, final boolean onlyPdf) {
		FileDTO output = null;

		if (d != null) {
			List<?> versioniDoc = ((SubSetImpl) d.get_VersionSeries().get_Versions()).getList();
			if (versioniDoc != null) {
				// Si itera l'elenco delle versioni, a partire dalla più recente
				Iterator<?> itVersioniDoc = versioniDoc.iterator();
				while (itVersioniDoc.hasNext()) {
					Document docVersione = (Document) itVersioniDoc.next();
					if (FilenetCEHelper.hasDocumentContentTransfer(docVersione)) {
						try {
							ContentTransfer content = FilenetCEHelper.getDocumentContentTransfer(docVersione);
							// Se si tratta di un PDF, oppure se non si vuole necessariamente un PDF, si restituisce il contenuto del documento
							if (MediaType.PDF.toString().equalsIgnoreCase(content.get_ContentType()) || !onlyPdf) {
								byte[] bytes = FileUtils.getByteFromInputStream(content.accessContentStream());
								output = new FileDTO((String) TrasformerCE.getMetadato(d, PropertiesNameEnum.NOME_FILE_METAKEY), 
										bytes, content.get_ContentType(), d.get_Id().toString());
								break;
							}
						} catch (Exception e) {
							throw new FilenetException(e);
						}
					}
				}
			}
		}

		return output;
	}
	/* (non-Javadoc)
	 * @see it.ibm.red.business.service.facade.IDocumentoFacadeSRV#getDocumentoContentPreview(it.ibm.red.business.dto.FilenetCredentialsDTO, java.lang.String, java.lang.Boolean)
	 */
	@Override
	public final FileDTO getDocumentoContentPreview(final FilenetCredentialsDTO fcDTO, final String documentTitle, final boolean onlyPdf, final Long idAoo) {
		return getContentPreview(fcDTO, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY),
				documentTitle, onlyPdf, true, idAoo, false);
	}
	
	
	@Override
	public final FileDTO getDocumentoContentPreview(final FilenetCredentialsDTO fcDTO, final String documentTitle, final boolean onlyPdf, final Long idAoo, final boolean contentLibroFirma) {
		return getContentPreview(fcDTO, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY),
				documentTitle, onlyPdf, true, idAoo, contentLibroFirma);
	}

	/**
	 * @param docFilenet
	 * @param onlyPdf
	 * @param contentLibroFirma
	 * @return
	 */
	private static FileDTO getContentFromDocument(final Document docFilenet, final boolean onlyPdf, final boolean contentLibroFirma) {
		FileDTO output = null;

		if (docFilenet != null) {
			if (contentLibroFirma) { // Recupero del content dall'annotation CONTENT_LIBRO_FIRMA
				output = getContentLibroFirma(docFilenet);
			}
			
			if (output == null) { // Se NON è stato richiesto il content dell'annotation CONTENT_LIBRO_FIRMA, oppure se è stato richiesto ma non è presente
				final List<?> versioniDoc = ((SubSetImpl) docFilenet.get_VersionSeries().get_Versions()).getList();
				if (versioniDoc != null) {
					// Si itera l'elenco delle versioni, a partire dalla più recente
					final Iterator<?> itVersioniDoc = versioniDoc.iterator();
					while (itVersioniDoc.hasNext()) {
						final Document docVersione = (Document) itVersioniDoc.next();
						if (FilenetCEHelper.hasDocumentContentTransfer(docVersione)) {
							try {
								final ContentTransfer content = FilenetCEHelper.getDocumentContentTransfer(docVersione);
								// Se si tratta di un PDF, oppure se non si vuole necessariamente un PDF, si restituisce il contenuto del documento
								if (MediaType.PDF.toString().equalsIgnoreCase(content.get_ContentType()) || !onlyPdf) {
									final byte[] bytes = FileUtils.getByteFromInputStream(content.accessContentStream());
									output = new FileDTO((String) TrasformerCE.getMetadato(docFilenet, PropertiesNameEnum.NOME_FILE_METAKEY), 
											bytes, content.get_ContentType(), docFilenet.get_Id().toString());
									break;
								}
							} catch (final Exception e) {
								throw new FilenetException(e);
							}
						}
					}
					// Si procede con il recupero della velina, ricavata tramite l'annotation PREVIEW presente sulla PRIMA VERSIONE del documento
					if (output == null) {
						final Document primaVersione = (Document) versioniDoc.get(versioniDoc.size() - 1);
						final AnnotationSet annotations = primaVersione.get_Annotations();
						final Iterator<?> itAnnotations = annotations.iterator();
						while (itAnnotations.hasNext()) {
							final Annotation annObject = (Annotation) itAnnotations.next();
							if (FilenetAnnotationEnum.TEXT_PREVIEW.getNome().equals(annObject.get_DescriptiveText())) {
								final ContentElementList cel = annObject.get_ContentElements();
								final Iterator<?> iCel = cel.iterator();
								if (iCel.hasNext()) {
									final ContentTransfer ct = (ContentTransfer) iCel.next();
									final byte[] contentPreview = FileUtils.getByteFromInputStream(ct.accessContentStream());
									output = new FileDTO(FilenetAnnotationEnum.TEXT_PREVIEW.getNome() + ".pdf", contentPreview, MediaType.PDF.toString(), docFilenet.get_Id().toString());
									break;
								}
							}
						}
					}
				}
			}
		}

		if (output == null) {
			output = new FileDTO(null, null, null);
		}

		return output;
	}

	/**
	 * Gets the contributo content preview.
	 *
	 * @param fcDTO         the fc DTO
	 * @param documentTitle the document title
	 * @return the contributo content preview
	 */
	@Override
	public final FileDTO getContributoContentPreview(final FilenetCredentialsDTO fcDTO, final String documentTitle, final Long idAoo) {
		return getContentPreview(fcDTO, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.CONTRIBUTO_CLASSNAME_FN_METAKEY), documentTitle,
				false, true, idAoo, false);
	}

	/**
	 * Gets the contributo content preview.
	 *
	 * @param fcDTO         the fc DTO
	 * @param documentTitle the document title
	 * @return the contributo content preview
	 */
	@Override
	public final FileDTO getRegistroProtocolloContentPreview(final FilenetCredentialsDTO fcDTO, final String documentTitle, final Long idAoo) {
		return getContentPreview(fcDTO, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.REGISTRO_PROTOCOLLO_CLASSNAME_FN_METAKEY), documentTitle,
				false, true, idAoo, false);
	}

	/**
	 * Gets the contributo content preview.
	 *
	 * @param fcDTO         the fc DTO
	 * @param documentTitle the document title
	 * @return the contributo content preview
	 */
	@Override
	public final FileDTO getDocumentoSIGIContentPreview(final FilenetCredentialsDTO fcDTO, final String documentTitle, final Long idAoo) {
		return getContentPreview(fcDTO, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_SIGI_CLASSNAME_FN_METAKEY), documentTitle, 
				false, true, idAoo, false);
	}

	/**
	 * Gets the documento cartaceo content preview.
	 *
	 * @param fcDTO         the fc DTO
	 * @param documentTitle the document title
	 * @return the documento cartaceo content preview
	 */
	@Override
	public final FileDTO getDocumentoCartaceoContentPreview(final FilenetCredentialsDTO fcDTO, final String documentTitle, final Long idAoo) {
		return getContentPreview(fcDTO, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CARTACEO_CLASSNAME_FN_METAKEY), documentTitle, 
				true, false, idAoo, false);
	}

	/* (non-Javadoc)
	 * @see it.ibm.red.business.service.facade.IDocumentoFacadeSRV#getContentAllegato(it.ibm.red.business.dto.FilenetCredentialsDTO, java.lang.String)
	 */
	@Override
	public final AttachmentDTO getContentAllegato(final String documentTitle, final FilenetCredentialsDTO fcDTO, final Long idAoo) {
		return getContentAllegato(documentTitle, null, fcDTO, false, false, null, idAoo, false);
	}
	
	/* (non-Javadoc)
	 * @see it.ibm.red.business.service.facade.IDocumentoFacadeSRV#getContentAllegato(it.ibm.red.business.dto.FilenetCredentialsDTO, java.lang.String)
	 */
	@Override
	public final AttachmentDTO getContentAllegato(final String documentTitle, final IFilenetCEHelper fceh, final Long idAoo) {
		return getContentAllegato(documentTitle, fceh, null, false, false, null, idAoo, false);
	}
	
	/**
	 * @param documentTitle
	 * @param fcDTO
	 * @param preview
	 * @param contentLibroFirma
	 * @return
	 */
	private final AttachmentDTO getContentAllegato(final String documentTitle, final IFilenetCEHelper fcehInput, final FilenetCredentialsDTO fcDTO, final boolean preview, final boolean isDownload, final PlaceholderInfoDTO placeholderInfo, final Long idAoo, final boolean contentLibroFirma) {
		AttachmentDTO output = null;
		IFilenetCEHelper fceh = null;

		try {
			if(fcehInput == null) {
				fceh = FilenetCEHelperProxy.newInstance(fcDTO, idAoo);
			}else {
				fceh = fcehInput;
			}

			final Document d = fceh.getAllegatoForDownload(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY), documentTitle);
			if (d != null && d.get_ContentElements() != null) {
				FileDTO contentFile = null;
				ContentTransfer ct = null;
				
				if (FilenetCEHelper.hasDocumentContentTransfer(d)) {
					ct = FilenetCEHelper.getDocumentContentTransfer(d);
				} else {
					throw new RedException("Errore durante il recupero del content di un allegato con ID={" + documentTitle + "}");
				}
				
				if (!preview || ContentType.PDF.equals(ct.get_ContentType()) || isDownload) {
					if (!contentLibroFirma) {
						String nomeFile = (String) TrasformerCE.getMetadato(d, PropertiesNameEnum.NOME_FILE_METAKEY);
						if (StringUtils.isNullOrEmpty(nomeFile)) {
							nomeFile = ct.get_RetrievalName();
						}
						contentFile = new FileDTO(nomeFile, FileUtils.getByteFromInputStream(ct.accessContentStream()), ct.get_ContentType());
					} else {
						contentFile = getContentLibroFirma(d);
						if (contentFile == null) {
							String nomeFile = (String) TrasformerCE.getMetadato(d, PropertiesNameEnum.NOME_FILE_METAKEY);
							if (StringUtils.isNullOrEmpty(nomeFile)) {
								nomeFile = ct.get_RetrievalName();
							}
							contentFile = new FileDTO(nomeFile, FileUtils.getByteFromInputStream(ct.accessContentStream()), ct.get_ContentType());							
						}else {
							ct=getContentLibroFirmaCt(d);
						}
						
					}
					
					if (!ContentType.PDF.equals(contentFile.getMimeType())) {
						contentFile = getContentPreviewAllegatoNonPdf(fceh, Constants.EMPTY_STRING + d.get_Id(), true, true);
					}
				} else {
					contentFile = getContentPreviewAllegatoNonPdf(fceh, Constants.EMPTY_STRING + d.get_Id(), true, true);
				}
				
				if (contentFile == null || contentFile.getContent() == null || contentFile.getContent().length == 0) {
					throw new RedException("Errore durante il recupero del content di un allegato con ID={" + documentTitle + "}");
				}
				
				final Integer daFirmare = (Integer) (d.getProperties().get(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DA_FIRMARE_METAKEY)).getObjectValue());
				Boolean bDaFirmare = false;
				if (daFirmare != null && daFirmare == 1) {
					bDaFirmare = true;
				}
				final Integer formatoAllegato = (Integer) TrasformerCE.getMetadato(d, PropertiesNameEnum.FORMATO_ALLEGATO_ID_METAKEY);

				if (placeholderInfo != null) {
					placeholderInfo.setGuidAllegato(d.get_Id().toString());
					final Boolean mantieniFormatoOriginale = (Boolean) TrasformerCE.getMetadato(d, PropertiesNameEnum.IS_FORMATO_ORIGINALE_METAKEY);
					placeholderInfo.setMantieniFormatoOriginale(false);
					if (mantieniFormatoOriginale != null) {
						placeholderInfo.setMantieniFormatoOriginale(mantieniFormatoOriginale);
					}
					final Boolean firmaVisibile = (Boolean) TrasformerCE.getMetadato(d, PropertiesNameEnum.FIRMA_VISIBILE_METAKEY);
					placeholderInfo.setFirmaVisibile(firmaVisibile);
					placeholderInfo.setDaFirmare(bDaFirmare);

					final Boolean stampigliaturaSiglaAllegato = (Boolean) TrasformerCE.getMetadato(d, PropertiesNameEnum.STAMPIGLIATURA_SIGLA_ALLEGATO);
					placeholderInfo.setStampigliaturaSiglaAllegato(false);
					if (stampigliaturaSiglaAllegato != null) {
						placeholderInfo.setStampigliaturaSiglaAllegato(stampigliaturaSiglaAllegato);
					}
				}

				if (!preview || MediaType.PDF.toString().equals(ct.get_ContentType()) || isDownload) {
					final byte[] bytes = FileUtils.getByteFromInputStream(ct.accessContentStream());
					output = new AttachmentDTO(ct.get_RetrievalName(), bytes, ct.get_ContentType(), d.get_Id().toString(), bDaFirmare, documentTitle, formatoAllegato);
					if(isDownload) {
						final String nomeFile = (String) (d.getProperties().get(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY)).getObjectValue());
						if(nomeFile!=null && !"".equals(nomeFile)) {
							output = new AttachmentDTO(nomeFile, bytes, ct.get_ContentType(), d.get_Id().toString(), bDaFirmare, documentTitle, formatoAllegato);	
						} 
					}
				} else if (preview && (ContentType.P7M_MIME.equals(ct.get_ContentType()) || ContentType.P7M_X_MIME.equals(ct.get_ContentType()) || ContentType.P7M.equals(ct.get_ContentType()))) {
					FileDTO allegatoPrevVers  = getAllegatoP7MPreview(fceh, documentTitle, true,idAoo);		
					output = new AttachmentDTO(ct.get_RetrievalName(), allegatoPrevVers.getContent(), allegatoPrevVers.getMimeType(), d.get_Id().toString(), bDaFirmare, documentTitle, formatoAllegato);
				}
				
				if(output == null){
					final FileDTO allegatoNonConv = getContentPreviewAllegato(fceh, Constants.EMPTY_STRING + d.get_Id(), true ,true, idAoo, contentLibroFirma);
					output = new AttachmentDTO(ct.get_RetrievalName(), allegatoNonConv.getContent(), allegatoNonConv.getMimeType(), d.get_Id().toString(), bDaFirmare, documentTitle, formatoAllegato);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			if(fcehInput==null) {
				popSubject(fceh);
			}
		}

		return output;
	}

	/**
	 * N.B. Recupera il content del principale dell'allegato in input.
	 * 
	 * @param fceh
	 * @param guid
	 * @param onlyPdf
	 * @param nomeFile
	 * @return
	 */
	private FileDTO getContentPreviewAllegatoNonPdf(final IFilenetCEHelper fceh, final String guid, final Boolean onlyPdf, final boolean nomeFile) {
		FileDTO output = null;
		
		try {
			Document d = fceh.getDocumentForDownloadAllegato(guid, nomeFile);
			output = getContentFromDocument(d, onlyPdf, false);
		} catch (FilenetException e) {
			LOGGER.error("Errore durante il recupero del content del documento con GUID: " + guid, e);
			throw e;
		}
		
		return output;
	}
	
	/* (non-Javadoc)
	 * @see it.ibm.red.business.service.facade.IDocumentoFacadeSRV#getContentAllegato4Preview(java.lang.String, it.ibm.red.business.dto.FilenetCredentialsDTO, boolean)
	 */
	@Override
	public final AttachmentDTO getContentAllegato4Preview(final String id, final FilenetCredentialsDTO fcDTO, final boolean isDownload, final PlaceholderInfoDTO placeholderInfo, final Long idAoo, final boolean contentLibroFirma) {
		return getContentAllegato(id, null, fcDTO, true, isDownload, placeholderInfo, idAoo, contentLibroFirma);
	}

	
	/* (non-Javadoc)
	 * @see it.ibm.red.business.service.facade.IDocumentoFacadeSRV#getCountDocumenti(
	 * it.ibm.red.business.dto.FilenetCredentialsDTO, java.lang.Long, java.lang.Long, java.lang.Long, java.lang.Long, it.ibm.red.business.enums.TipoOperazioneLibroFirmaEnum)
	 */
	@Override
	public final int getCountDocumenti(final FilenetCredentialsDTO fcDTO, final Long idUtente, final Long idUfficio, final Long idAoo, final Long idRuolo,
			final TipoOperazioneLibroFirmaEnum filter) {
		return this.getDocumenti(fcDTO, idUtente, idUfficio, idRuolo, idAoo, filter, true).size();
	}

	
	/**
	 * @param docFilenet
	 * @return
	 */
	private static FileDTO getContentLibroFirma(Document docFilenet) {
		FileDTO contentLibroFirma = null;
		
		try {
			final AnnotationSet as = docFilenet.get_Annotations();
			
			final Iterator<?> iter = as.iterator();
			while (iter.hasNext()) {
				final Annotation annObject = (Annotation) iter.next();
				if (FilenetAnnotationEnum.CONTENT_LIBRO_FIRMA.getNome().equals(annObject.get_DescriptiveText())) {
					final ContentElementList cel = annObject.get_ContentElements();
					final Iterator<?> iCel = cel.iterator();
					if (iCel.hasNext()) {
						final ContentTransfer ct = (ContentTransfer) iCel.next();
						final String nomeFile = (String) TrasformerCE.getMetadato(docFilenet, PropertiesNameEnum.NOME_FILE_METAKEY);
						contentLibroFirma = new FileDTO(nomeFile, IOUtils.toByteArray(ct.accessContentStream()), ContentType.PDF, true);
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Errore nel recupero dell'annotation CONTENT_LIBRO_FIRMA per il documento con GUID: " + docFilenet.get_Id(), e);
			throw new RedException("Errore nel recupero dell'annotation CONTENT_LIBRO_FIRMA per il documento con GUID: " + docFilenet.get_Id(), e);
		}
		
		return contentLibroFirma;
	}
	
	/**
	 * @param docFilenet
	 * @return
	 */
	private static ContentTransfer getContentLibroFirmaCt(Document docFilenet) {
		try {
			final AnnotationSet as = docFilenet.get_Annotations();
			
			final Iterator<?> iter = as.iterator();
			while (iter.hasNext()) {
				final Annotation annObject = (Annotation) iter.next();
				if (FilenetAnnotationEnum.CONTENT_LIBRO_FIRMA.getNome().equals(annObject.get_DescriptiveText())) {
					final ContentElementList cel = annObject.get_ContentElements();
					final Iterator<?> iCel = cel.iterator();
					if (iCel.hasNext()) {
						final ContentTransfer ct = (ContentTransfer) iCel.next();
						return ct;
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Errore nel recupero dell'annotation CONTENT_LIBRO_FIRMA per il documento con GUID: " + docFilenet.get_Id(), e);
			throw new RedException("Errore nel recupero dell'annotation CONTENT_LIBRO_FIRMA per il documento con GUID: " + docFilenet.get_Id(), e);
		}
		
		return null;
	}
	
	
	/**
	 * Servizio chiamato da RedMobile. Deprecato. Recupera i WF che rispettano i
	 * criteri di ricerca.
	 * @param fcDTO     - Credenziali per l'accesso a FN
	 * @param idUtente  - Id dell'utente
	 * @param idUfficio - Id dell'ufficio.
	 * @param idRuolo   - Id del ruolo
	 * @param filter    - Filtro
	 * @return DTOs dei WF che rispettano i criteri di ricerca
	 * @deprecated
	 */
	@Deprecated
	private Collection<PEDocumentoDTO> getDocumentiPEByFilter(final FilenetCredentialsDTO fcDTO, final Long idUtente, final Long idUfficio, final Long idRuolo,
			final TipoOperazioneLibroFirmaEnum filter) {
		final Collection<PEDocumentoDTO> peDocs = new ArrayList<>();
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			final FilenetPEHelper fpeh = new FilenetPEHelper(fcDTO);
			if (TipoOperazioneLibroFirmaEnum.CORRIERE.equals(filter)) {
				final String indexName = "Destinatario";
				if (utenteDAO.isCorriere(idRuolo, connection)) {
					final VWQueueQuery query = fpeh.getWorkFlowsByWobQueueFilter(null, // WobNumber
							DocumentQueueEnum.CORRIERE, // Nome cosa
							indexName, // indexName
							idUfficio, // idUfficio
							null, // lista degli utenti destinatari
							fcDTO.getIdClientAoo(), // Client AOO
							null, // tipo assegnaione(vale per la firma)
							null, // flag Renderizzato
							false // registro riservato
					);
					final Collection<PEDocumentoDTO> queryPEResults = TrasformPE.transform(query, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO, null);
					for (final PEDocumentoDTO doc : queryPEResults) {
						doc.setQueue(DocumentQueueEnum.CORRIERE);
						peDocs.add(doc);
					}
				}
			} else if (TipoOperazioneLibroFirmaEnum.DA_LAVORARE.equals(filter)) {
				final List<Long> idUtenteDestinatari = new ArrayList<>();
				idUtenteDestinatari.add(idUtente);
				final VWQueueQuery query = fpeh.getWorkFlowsByWobQueueFilter(null, DocumentQueueEnum.DA_LAVORARE, null, idUfficio, idUtenteDestinatari, fcDTO.getIdClientAoo(),
						null, null, false);
				final Collection<PEDocumentoDTO> queryPEResults = TrasformPE.transform(query, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO, null);
				for (final PEDocumentoDTO doc : queryPEResults) {
					doc.setQueue(DocumentQueueEnum.DA_LAVORARE);
					peDocs.add(doc);
				}
			} else {
				final List<Long> idUtenteDestinatari = new ArrayList<>();
				Long idTipoAssegnazione = null;
				idUtenteDestinatari.add(idUtente);
				if (utenteDAO.isDelegato(idRuolo, connection)) {
					final Utente dirigente = nodoDAO.getNodo(idUfficio, connection).getDirigente();
					if (dirigente != null) {
						idUtenteDestinatari.add(dirigente.getIdUtente());
					}
				}
				if (filter != null && !TipoOperazioneLibroFirmaEnum.LIBRO_FIRMA.getIdFilenet().equals(filter.getIdFilenet())) {
					idTipoAssegnazione = filter.getIdFilenet();
				}
				DocumentQueueEnum queueQuery = DocumentQueueEnum.NSD;
				VWQueueQuery query = fpeh.getWorkFlowsByWobQueueFilter(null, queueQuery, null, idUfficio, idUtenteDestinatari, fcDTO.getIdClientAoo(), idTipoAssegnazione,
						BooleanFlag.TRUE, false);
				Collection<PEDocumentoDTO> queryPEResults = TrasformPE.transform(query, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO, null);
				for (final PEDocumentoDTO doc : queryPEResults) {
					doc.setQueue(queueQuery);
					peDocs.add(doc);
				}
				queueQuery = DocumentQueueEnum.DD_DA_FIRMARE;
				query = fpeh.getWorkFlowsByWobQueueFilter(null, queueQuery, null, idUfficio, idUtenteDestinatari, fcDTO.getIdClientAoo(), idTipoAssegnazione, BooleanFlag.TRUE,
						false);
				queryPEResults = TrasformPE.transform(query, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO, null);
				for (final PEDocumentoDTO doc : queryPEResults) {
					doc.setQueue(queueQuery);
					peDocs.add(doc);
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dei documenti: " + e.getMessage(), e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}
		return peDocs;
	}

	/**
	 * Gets the documenti.
	 *
	 * @param fcDTO     the fc DTO
	 * @param idUtente  the id utente
	 * @param idUfficio the id ufficio
	 * @param idRuolo   the id ruolo
	 * @param idAoo     the id aoo
	 * @param filter    the filter
	 * @param forCount  the for count
	 * @return the documenti
	 */
	@Override
	public final Collection<MasterDocumentoDTO> getDocumenti(final FilenetCredentialsDTO fcDTO, final Long idUtente, final Long idUfficio, final Long idRuolo,
			final Long idAoo, final TipoOperazioneLibroFirmaEnum filter, final boolean forCount) {
		Connection connection = null;
		IFilenetCEHelper fceh = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(fcDTO, idAoo);
			final Set<String> documentTitleSet = new HashSet<>();

			final Collection<PEDocumentoDTO> peDocs = getDocumentiPEByFilter(fcDTO, idUtente, idUfficio, idRuolo, filter);
			for (final PEDocumentoDTO doc : peDocs) {
				documentTitleSet.add(doc.getIdDocumento());
			}
			final TreeSet<MasterDocumentoDTO> output = new TreeSet<>(new DocumentoComparator());
			if (peDocs != null && !peDocs.isEmpty()) {
				final DocumentSet dsFirma = fceh.getDocumentsForMasters(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY),
						documentTitleSet);

				// TX WITHOUT CONTEXT!
				// INIZIO
				final Collection<MasterDocumentoDTO> cMasters = TrasformCE.transform(dsFirma, TrasformerCEEnum.FROM_DOCUMENTO_TO_MASTER);
				// FINE

				for (final PEDocumentoDTO doc : peDocs) {
					for (final MasterDocumentoDTO master : cMasters) {
						if (doc.getIdDocumento().equalsIgnoreCase(master.getDocumentTitle())) {
							MasterDocumentoDTO masterTemp = master;
							final boolean isPresent = output.contains(master);
							if (isPresent) {
								masterTemp = master.getClone();
							}
							masterTemp.setUrgente(doc.getUrgente());
							masterTemp.setOperazione(doc.getOperazione());
							masterTemp.setWobNumber(doc.getWobNumber());
							if (!forCount) {
								masterTemp.setQueue(doc.getQueue());
								masterTemp.setIdUtenteDestinatario(doc.getIdUtenteDestinatario());
								masterTemp.setFlagFirmaDig(doc.getFlagFirmaDig());
								masterTemp.setMotivazioneAssegnazione(doc.getMotivazioneAssegnazione());
								if (filter == null || filter.getIdFilenet() <= TipoOperazioneLibroFirmaEnum.LIBRO_FIRMA.getIdFilenet() && !masterTemp.getbTrasfPdfErrore()) {
									masterTemp.setFlagEnableFirma(doc.getFlagEnableFirma());
									masterTemp.setFlagEnableFirmaAutografa(doc.getFlagEnableFirmaAutografa());
									masterTemp.setFlagEnableRifiuta(doc.getFlagEnableRifiuta());
									masterTemp.setFlagEnableSigla(doc.getFlagEnableSigla());
									masterTemp.setFlagEnableVista(doc.getFlagEnableVista());

								}
								masterTemp.setFlagEnableProcediCorriere(doc.getFlagProcediDaCorriere());
								masterTemp.setFlagRenderizzato(doc.getFlagRenderizzato());
							}
							masterTemp.setFirmaCopiaConforme(doc.getFirmaCopiaConforme());
							masterTemp.setTipoAssegnazione(TipoAssegnazioneEnum.get(doc.getTipoAssegnazioneId()));
							updateIconaStato(masterTemp, idAoo, connection);
							output.add(masterTemp);
							break;
						}
					}
				}
			}

			return output;
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dei documenti: " + e.getMessage(), e);
		} finally {
			closeConnection(connection);
			popSubject(fceh);
		}

		return new TreeSet<>();
	}

	/**
	 * Gets the documento.
	 *
	 * @param utente    the utente
	 * @param wobNumber the wob number
	 * @param idAoo     the id aoo
	 * @return the documento
	 */
	@Override
	public final DetailDocumentoDTO getDocumento(final UtenteDTO utente, final String wobNumber, final Long idAoo) {
		DetailDocumentoDTO detail = null;
		Connection connection = null;
		IFilenetCEHelper fceh = null;

		try {
			int numAllegati = 0;
			final FilenetCredentialsDTO fcDTO = utente.getFcDTO();
			final FilenetPEHelper fpeh = new FilenetPEHelper(fcDTO);
			final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);
			final PEDocumentoDTO doc = TrasformPE.transform(wob, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO);

			fceh = FilenetCEHelperProxy.newInstance(fcDTO, utente.getIdAoo());
			final Document dFirma = fceh.getDocumentForDetail(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY),
					doc.getIdDocumento());
			detail = TrasformCE.transform(dFirma, TrasformerCEEnum.FROM_DOCUMENTO_TO_DETAIL);

			final DocumentSet allegati = fceh.getAllegatiConContent(detail.getGuid());
			if (allegati != null) {
				final Iterator<?> it = allegati.iterator();
				while (it.hasNext()) {
					it.next();
					numAllegati++;
				}
				detail.setNumAttachment(numAllegati);
			}

			connection = setupConnection(getDataSource().getConnection(), false);

			if (detail.getIterApprovativo() == null) {
				final Boolean iterManuale = doc.getFlagIterManuale() != null && "1".equals(doc.getFlagIterManuale() + "");
				detail.setIterApprovativo(lookupDAO.getTipoAssegnazioneDescById(doc.getTipoAssegnazioneId(), iterManuale, connection));
			}

			final Collection<ContributoDTO> contributi = contributoSRV.getContributi(fceh, detail.getDocumentTitle(), utente.getIdAoo());
			detail.setContributi(contributi);

			final Collection<String> inApprovazioni = lookupDAO.getApprovazioni(detail.getDocumentTitle(), idAoo, connection);
			detail.setApprovazioni(inApprovazioni);
			detail.setOperazione(doc.getOperazione());
			detail.setQueue(doc.getQueue());
			detail.setWobNumber(doc.getWobNumber());
			detail.setUrgenza(doc.getUrgente());
			Integer idUtenteDelegante = null;
			if (utenteDAO.isDelegato(utente.getIdRuolo(), connection)) {
				final Utente dirigente = nodoDAO.getNodo(utente.getIdUfficio(), connection).getDirigente();
				if (dirigente != null && dirigente.getIdUtente() != null) {
					idUtenteDelegante = dirigente.getIdUtente().intValue();
				}
			}

			boolean isInLibroFirmaUtente = true;
			final Integer idUtente = utente.getId().intValue();
			final Integer idUfficio = utente.getIdUfficio().intValue();
			if (Boolean.TRUE.equals(detail.getbTrasfPdfErrore())) {
				isInLibroFirmaUtente = false;
			} else if (!idUfficio.equals(doc.getIdNodoDestinatario())) {
				isInLibroFirmaUtente = false;
			} else if (idUtenteDelegante == null) {
				if (!idUtente.equals(doc.getIdUtenteDestinatario())) {
					isInLibroFirmaUtente = false;
				}
			} else if (!idUtente.equals(doc.getIdUtenteDestinatario()) && !idUtenteDelegante.equals(doc.getIdUtenteDestinatario())) {
				isInLibroFirmaUtente = false;
			} else if (Boolean.FALSE.equals(doc.getFlagRenderizzato())) {
				isInLibroFirmaUtente = false;
			}

			if (isInLibroFirmaUtente) {
				final UtenteFirma utenteFirma = utenteDAO.getUtenteFirma(utente.getId(), connection);
				detail.setFlagEnableFirma(doc.getFlagEnableFirma() && doc.getFlagRenderizzato() && utenteFirma.getUrlHandler() != null);
				detail.setFlagEnableFirmaAutografa(doc.getFlagEnableFirmaAutografa());
				detail.setFlagEnableRifiuta(doc.getFlagEnableRifiuta());
				detail.setFlagEnableSigla(doc.getFlagEnableSigla());
				detail.setFlagEnableVista(doc.getFlagEnableVista());
				detail.setFlagFirmaDig(doc.getFlagFirmaDig());
			}

			detail.setFlagEnableProcediCorriere(doc.getFlagProcediDaCorriere());

			detail.setMotivazioneAssegnazione(doc.getMotivazioneAssegnazione());
			detail.setIdUtenteDestinatario(doc.getIdUtenteDestinatario());

			detail.setFirmaPades(fceh.canPades(detail.getGuid()));

			updateDatiFascicolo(detail.getDocumentTitle(), idAoo, doc.getIdFascicolo(), fceh, detail, connection);

			final List<StepDTO> steps = storicoSRV.getModelloStoricoVisuale(utente.getFcDTO(), detail.getWobNumber(), detail.getDocumentTitle(), utente.getIdAoo(),
					detail.getDataCreazione());
			final List<ApprovazioneDTO> stepsApprovazioni = storicoSRV.getModelloApprovazioniVisuale(Integer.parseInt(detail.getDocumentTitle()),
					utente.getIdAoo().intValue());
			detail.setSteps(steps);
			detail.setStepsApprovazioni(stepsApprovazioni);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero del documento: " + e.getMessage(), e);
		} finally {
			closeConnection(connection);
			popSubject(fceh);
		}

		return detail;
	}

	/**
	 * Aggiornamento dati fascicolo.
	 * 
	 * @param idDocumento  identificativo documento
	 * @param idAoo        identificativo aoo
	 * @param nIdFascicolo identificativo fascicolo
	 * @param fceh         helper gestione ce
	 * @param detail       dati dettaglio
	 * @param connection   connessione
	 */
	private void updateDatiFascicolo(final String idDocumento, final Long idAoo, final Integer nIdFascicolo, final IFilenetCEHelper fceh, final DetailDocumentoDTO detail,
			final Connection connection) {
		Integer idFascicolo = nIdFascicolo;
		if (idFascicolo == null || idFascicolo == 0) {
			idFascicolo = fascicoloDAO.getIdFascicoloProcedimentale(idDocumento, idAoo, connection);
		}

		if (idFascicolo != null && idFascicolo > 0) {
			final Document dFascicolo = fceh.getFascicolo(idFascicolo + "", idAoo.intValue());
			final FascicoloDTO fascicoloDTO = TrasformCE.transform(dFascicolo, TrasformerCEEnum.FROM_DOCUMENTO_TO_FASCICOLO);

			detail.setOggettoFascicolo(fascicoloDTO.getDescrizione());
			detail.setStatoFascicolo(fascicoloDTO.getStato());
			detail.setIndiceFascicolo(fascicoloDTO.getIndiceClassificazione());
			detail.setDataCreazioneFascicolo(fascicoloDTO.getDataCreazione());
			detail.setNumeroDocumentoFascicolo(fascicoloDTO.getIdFascicolo());

			final Collection<DocumentoFascicoloDTO> documentiFascicoloDTO = TrasformCE.transform(fceh.getDocumentiFascicolo(idFascicolo, idAoo),
					TrasformerCEEnum.FROM_DOCUMENTO_TO_DOCUMENTO_FASCICOLO);
			detail.setDocumentiFascicolo(documentiFascicoloDTO);
			detail.setFlagDisableFascicolo(false);
		}
	}

	/**
	 * Gets the documento from CE.
	 *
	 * @param fcDTO         the fc DTO
	 * @param documentTitle the document title
	 * @param utente        the utente
	 * @return the documento from CE
	 */
	@Override
	public final DetailDocumentoDTO getDocumentoFromCE(final FilenetCredentialsDTO fcDTO, final String documentTitle, final UtenteDTO utente) {
		DetailDocumentoDTO detail = null;
		Connection connection = null;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(fcDTO, utente.getIdAoo());

			final Document dFirma = fceh.getDocumentForDetail(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY),
					documentTitle);
			detail = TrasformCE.transform(dFirma, TrasformerCEEnum.FROM_DOCUMENTO_TO_DETAIL);

			final Collection<ContributoDTO> contributi = contributoSRV.getContributi(fceh, detail.getDocumentTitle(), utente.getIdAoo());
			detail.setContributi(contributi);

			connection = setupConnection(getDataSource().getConnection(), false);

			final Collection<String> inApprovazioni = lookupDAO.getApprovazioni(detail.getDocumentTitle(), utente.getIdAoo(), connection);
			detail.setApprovazioni(inApprovazioni);
			detail.setFlagEnableFirma(false);
			detail.setFlagEnableFirmaAutografa(false);
			detail.setFlagEnableRifiuta(false);
			detail.setFlagEnableSigla(false);
			detail.setFlagEnableVista(false);

			detail.setFlagFirmaDig(false);

			detail.setFirmaPades(fceh.canPades(detail.getGuid()));
			detail.setFlagDisableFascicolo(true);

			updateDatiFascicolo(documentTitle, utente.getIdAoo(), null, fceh, detail, connection);

			final List<StepDTO> steps = storicoSRV.getModelloStoricoVisuale(utente.getFcDTO(), detail.getWobNumber(), detail.getDocumentTitle(), utente.getIdAoo(),
					detail.getDataCreazione());
			detail.setSteps(steps);
			final List<ApprovazioneDTO> stepsApprovazioni = storicoSRV.getModelloApprovazioniVisuale(Integer.parseInt(detail.getDocumentTitle()),
					utente.getIdAoo().intValue());
			detail.setStepsApprovazioni(stepsApprovazioni);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero del documento: " + e.getMessage(), e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
			popSubject(fceh);
		}

		return detail;
	}

	/**
	 * Metodo merge dati ce/pe.
	 * 
	 * @param ceDocs     dati content
	 * @param peDocs     dati process
	 * @param utente     dati utente
	 * @param connection connessione
	 */
	private void mergeCePeData(final Collection<MasterDocumentoDTO> ceDocs, final Collection<PEDocumentoDTO> peDocs, final UtenteDTO utente, final Connection connection) {
		if (ceDocs != null) {
			Integer idUtenteDelegante = null;
			if (utenteDAO.isDelegato(utente.getIdRuolo(), connection)) {
				final Utente dirigente = nodoDAO.getNodo(utente.getIdUfficio(), connection).getDirigente();
				if (dirigente != null) {
					idUtenteDelegante = dirigente.getIdUtente().intValue();
				}
			}
			for (final Iterator<MasterDocumentoDTO> iterator = ceDocs.iterator(); iterator.hasNext();) {
				final MasterDocumentoDTO master = iterator.next();
				for (final Iterator<PEDocumentoDTO> iterator2 = peDocs.iterator(); iterator2.hasNext();) {
					final PEDocumentoDTO peDoc = iterator2.next();
					if (peDoc.getIdDocumento().equals(master.getDocumentTitle())) {
						master.setFlagRenderizzato(peDoc.getFlagRenderizzato());
						master.setOperazione(peDoc.getOperazione());
						master.setQueue(peDoc.getQueue());
						master.setWobNumber(peDoc.getWobNumber());
						master.setIdUtenteDestinatario(peDoc.getIdUtenteDestinatario());
						master.setFlagFirmaDig(peDoc.getFlagFirmaDig());
						master.setMotivazioneAssegnazione(peDoc.getMotivazioneAssegnazione());
						master.setUrgente(peDoc.getUrgente());

						boolean isInLibroFirmaUtente = true;
						final Integer idUtente = utente.getId().intValue();
						final Integer idUfficio = utente.getIdUfficio().intValue();
						if (Boolean.TRUE.equals(master.getbTrasfPdfErrore())) {
							isInLibroFirmaUtente = false;
						} else if (!idUfficio.equals(peDoc.getIdNodoDestinatario())) {
							isInLibroFirmaUtente = false;
						} else if (idUtenteDelegante == null) {
							if (!idUtente.equals(peDoc.getIdUtenteDestinatario())) {
								isInLibroFirmaUtente = false;
							}
						} else if (!idUtente.equals(peDoc.getIdUtenteDestinatario()) && !idUtenteDelegante.equals(peDoc.getIdUtenteDestinatario())) {
							isInLibroFirmaUtente = false;
						} else if (Boolean.FALSE.equals(peDoc.getFlagRenderizzato())) {
							isInLibroFirmaUtente = false;
						}

						if (isInLibroFirmaUtente) {
							master.setFlagEnableFirma(peDoc.getFlagEnableFirma());
							master.setFlagEnableFirmaAutografa(peDoc.getFlagEnableFirmaAutografa());
							master.setFlagEnableRifiuta(peDoc.getFlagEnableRifiuta());
							master.setFlagEnableSigla(peDoc.getFlagEnableSigla());
							master.setFlagEnableVista(peDoc.getFlagEnableVista());
						}
						if (master.getOperazione() == null || (!TipoOperazioneLibroFirmaEnum.FIRMA.equals(master.getOperazione())
								&& !TipoOperazioneLibroFirmaEnum.SIGLA.equals(master.getOperazione()) && !TipoOperazioneLibroFirmaEnum.VISTO.equals(master.getOperazione()))) {

							master.setStato(getStato(master, peDoc, utente, connection));
						}
						break;
					}
				}
				if (master.getStato() == null) {
					master.setStato(getStato(master, null, utente, connection));
				}
			}

		}
	}

	/**
	 * Metodo per il recupero dello stato di un documento.
	 * 
	 * @param master     dati ce
	 * @param peDoc      dati pe
	 * @param utente     dati utente
	 * @param connection connessione
	 * @return stato documento
	 */
	private StatoDocumentoFuoriLibroFirmaEnum getStato(final MasterDocumentoDTO master, final PEDocumentoDTO peDoc, final UtenteDTO utente, final Connection connection) {
		StatoDocumentoFuoriLibroFirmaEnum stato;
		if (peDoc != null) {
			stato = StatoDocumentoFuoriLibroFirmaEnum.get(peDoc.getTipoAssegnazioneId(), master.getIdCategoriaDocumento(), master.getIdFormatoDocumento(),
					master.getTipoSpedizione());
		} else {
			stato = storicoDAO.getStatoDocumentoChiuso(master.getDocumentTitle(), utente.getId(), utente.getIdUfficio(), connection);
		}
		return stato;
	}

	/**
	 * Ricerca generica.
	 *
	 * @param key    the key
	 * @param anno   the anno
	 * @param type   the type
	 * @param utente the utente
	 * @return the collection
	 * @throws SearchException the search exception
	 */
	@Override
	public final Collection<MasterDocumentoDTO> ricercaGenerica(final String key, final Integer anno, final RicercaGenericaTypeEnum type, final UtenteDTO utente)
			throws SearchException {
		final TreeSet<MasterDocumentoDTO> output = new TreeSet<>(new DocumentoComparator());
		if (anno == null) {
			if (key == null) {
				throw new SearchException("Per continuare è necessario valorizzare i campi obbligatori.");
			}

		} else if (anno > Calendar.getInstance().get(Calendar.YEAR)) {
			throw new SearchException("L'anno inserito non risulta valido. [" + anno + " > " + Calendar.getInstance().get(Calendar.YEAR) + "]");
		}

		if (type == null) {
			throw new SearchException("Il tipo di ricerca non è stato valorizzato correttamente.");
		}

		Connection connection = null;
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpehAdmin = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			final FilenetCredentialsDTO fcDTO = utente.getFcDTO();
			fceh = FilenetCEHelperProxy.newInstance(fcDTO, utente.getIdAoo());

			final DocumentSet documents = fceh.ricercaGenerica(key, anno, type, utente.getIdAoo());
			final Collection<MasterDocumentoDTO> documentiCE = TrasformCE.transform(documents, TrasformerCEEnum.FROM_DOCUMENTO_TO_MASTER);

			final FilenetCredentialsDTO fcAdmin = new FilenetCredentialsDTO(fcDTO);
			fpehAdmin = new FilenetPEHelper(fcAdmin);
			final List<Long> idDocumenti = new ArrayList<>();
			if (documentiCE != null) {
				for (final Iterator<MasterDocumentoDTO> iterator = documentiCE.iterator(); iterator.hasNext();) {
					final MasterDocumentoDTO master = iterator.next();
					idDocumenti.add(new Long(master.getDocumentTitle()));
				}
			}
			if (!idDocumenti.isEmpty()) {
				final VWRosterQuery rQueryNSD = fpehAdmin.getDocumentWorkFlowsByIdDocumenti(idDocumenti);
				final Collection<PEDocumentoDTO> peDocs = TrasformPE.transform(rQueryNSD, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO);
				mergeCePeData(documentiCE, peDocs, utente, connection);
			}
			for (final MasterDocumentoDTO master : documentiCE) {
				updateIconaStato(master, utente.getId(), connection);
				output.add(master);
			}

			return output;
		} catch (final Exception e) {
			LOGGER.error("Errore durante la ricerca dei documenti: " + e.getMessage(), e);
			throw new SearchException("Errore durante la ricerca dei documenti: " + e.getMessage());
		} finally {
			closeConnection(connection);
			logoff(fpehAdmin);
			popSubject(fceh);
		}
	}

	/**
	 * Gets the attachments.
	 *
	 * @param fcDTO the fc DTO
	 * @param id    the id
	 * @return the attachments
	 */
	@Override
	public final Collection<AttachmentDTO> getAttachments(final FilenetCredentialsDTO fcDTO, final String id, final Long idAoo) {
		final Collection<AttachmentDTO> output = new ArrayList<>();
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(fcDTO, idAoo);

			final DocumentSet allegati = fceh.getAllegati(id, false, false);
			final Iterator<?> it = allegati.iterator();
			while (it.hasNext()) {
				final Document docAllegato = (Document) it.next();
				byte[] bytes = null;
				ContentTransfer ct = null;
				if (FilenetCEHelper.hasDocumentContentTransfer(docAllegato)) {
					ct = FilenetCEHelper.getDocumentContentTransfer(docAllegato);
					bytes = FileUtils.getByteFromInputStream(ct.accessContentStream());
				}
				Boolean bToSign = false;
				if (1 == (Integer) TrasformerCE.getMetadato(docAllegato, PropertiesNameEnum.DA_FIRMARE_METAKEY)) {
					bToSign = true;
				}
				String retrievalName = "Allegato non elettronico";
				String contentType = "";
				if (ct != null) {
					retrievalName = ct.get_RetrievalName();
					contentType = ct.get_ContentType();
				}
				final AttachmentDTO file = new AttachmentDTO(retrievalName, bytes, contentType,
						(String) TrasformerCE.getMetadato(docAllegato, PropertiesNameEnum.OGGETTO_METAKEY), bToSign,
						(String) TrasformerCE.getMetadato(docAllegato, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY),
						(Integer) TrasformerCE.getMetadato(docAllegato, PropertiesNameEnum.FORMATO_ALLEGATO_ID_METAKEY));

				output.add(file);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new FilenetException(e);
		} finally {
			popSubject(fceh);
		}

		return output;
	}

	/**
	 * Gets the documento.
	 *
	 * @param documentTitle the document title
	 * @param utente        the utente
	 * @return the documento
	 * @throws SearchException the search exception
	 */
	@Override
	public final MasterDocumentoDTO getDocumento(final String documentTitle, final UtenteDTO utente) throws SearchException {
		Connection connection = null;
		MasterDocumentoDTO output = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			final Collection<MasterDocumentoDTO> masters = ricercaGenerica(documentTitle, null, RicercaGenericaTypeEnum.ESATTA, utente);
			if (masters != null && !masters.isEmpty()) {
				output = masters.iterator().next();
			}

			updateIconaStato(output, utente.getIdAoo(), connection);

			return output;
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del documento: " + documentTitle, e);
			throw new SearchException("Errore durante il recupero del documento: " + documentTitle);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Update master icona stato.
	 *
	 * @param master the master
	 * @param idAoo  the id aoo
	 * @throws SearchException the search exception
	 */
	@Override
	public final void updateMasterIconaStato(final MasterDocumentoDTO master, final Long idAoo) throws SearchException {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			updateIconaStato(master, idAoo, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'update dello stato icona del documento " + master.getDocumentTitle(), e);
			rollbackConnection(connection);
			throw new SearchException("Errore durante l'update dello stato icona del documento " + master.getDocumentTitle());
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Metodo per aggiornamento dell'icona del master.
	 * 
	 * @param master     master
	 * @param idAoo      identificativo aoo
	 * @param connection connessione
	 */
	private void updateIconaStato(final MasterDocumentoDTO master, final Long idAoo, final Connection connection) {
		IconaStatoEnum iconaStato;

		if (TipoAssegnazioneEnum.COPIA_CONFORME.equals(master.getTipoAssegnazione())) {
			iconaStato = IconaStatoEnum.DOCUMENTO_IN_FIRMA_PER_COPIA_CONFORME;
		} else if (DocumentQueueEnum.SOSPESO.equals(master.getQueue())) {
			iconaStato = IconaStatoEnum.DOCUMENTO_IN_ATTESA;
		} else if (StatoDocumentoFuoriLibroFirmaEnum.COMPETENZA.equals(master.getStato()) || TipoOperazioneLibroFirmaEnum.COMPETENZA.equals(master.getOperazione())) {
			iconaStato = IconaStatoEnum.COMPETENZA;
		} else if (StatoDocumentoFuoriLibroFirmaEnum.CONOSCENZA.equals(master.getStato()) || TipoOperazioneLibroFirmaEnum.CONOSCENZA.equals(master.getOperazione())) {
			iconaStato = IconaStatoEnum.CONOSCENZA;
		} else if (StatoDocumentoFuoriLibroFirmaEnum.CONTRIBUTO.equals(master.getStato()) || TipoOperazioneLibroFirmaEnum.CONTRIBUTO.equals(master.getOperazione())) {
			iconaStato = IconaStatoEnum.CONTRIBUTO;
		} else if (StatoDocumentoFuoriLibroFirmaEnum.ASSEGNAZIONE_INTERNA.equals(master.getStato())
				|| StatoDocumentoFuoriLibroFirmaEnum.PRECENSITO_INTERNO.equals(master.getStato())) {
			iconaStato = IconaStatoEnum.DOCUMENTO_ASSEGNAZIONE_INTERNA;
		} else if (StatoDocumentoFuoriLibroFirmaEnum.CHIUSO_ATTI.equals(master.getStato())) {
			iconaStato = IconaStatoEnum.DOCUMENTO_IN_ENTRATA_CHIUSO_SENZA_RISPOSTA;
		} else if (StatoDocumentoFuoriLibroFirmaEnum.CHIUSO_RISPOSTA.equals(master.getStato()) || Boolean.TRUE.equals(isInAttesaChiusuraRisposta(master, idAoo, connection))) {
			iconaStato = IconaStatoEnum.DOCUMENTO_IN_ENTRATA_O_USCITA_CHIUSO_CON_RISPOSTA;
		} else if (StatoDocumentoFuoriLibroFirmaEnum.CHIUSO_MOZIONE.equals(master.getStato())) {
			iconaStato = IconaStatoEnum.DOCUMENTO_IN_USCITA_CHIUSO_SENZA_RISPOSTA;
		} else if (TipoOperazioneLibroFirmaEnum.FIRMA.equals(master.getOperazione())) {
			if (master.getFlagFirmaAutografaRM() != null && master.getFlagFirmaAutografaRM()) {
				iconaStato = IconaStatoEnum.FIRMA_AUTOGRAFA_SU_RED;
			} else {
				iconaStato = IconaStatoEnum.FIRMA;
			}
		} else if (StatoDocumentoFuoriLibroFirmaEnum.PRECENSITO.equals(master.getStato())) {
			iconaStato = IconaStatoEnum.PRECENSITO;
		} else if (StatoDocumentoFuoriLibroFirmaEnum.RIFIUTO.equals(master.getStato()) || TipoOperazioneLibroFirmaEnum.RIFIUTO_ASSEGNAZIONE.equals(master.getOperazione())) {
			iconaStato = IconaStatoEnum.RIFIUTO_ASSEGNAZIONE;
		} else if (StatoDocumentoFuoriLibroFirmaEnum.RIFIUTO_FIRMA.equals(master.getStato()) || TipoOperazioneLibroFirmaEnum.RIFIUTO_FIRMA.equals(master.getOperazione())) {
			iconaStato = IconaStatoEnum.RIFIUTO_FIRMA;
		} else if (StatoDocumentoFuoriLibroFirmaEnum.RIFIUTO_SIGLA.equals(master.getStato()) || TipoOperazioneLibroFirmaEnum.RIFIUTO_SIGLA.equals(master.getOperazione())) {
			iconaStato = IconaStatoEnum.RIFIUTO_SIGLA;
		} else if (StatoDocumentoFuoriLibroFirmaEnum.RIFIUTO_VISTO.equals(master.getStato())) {
			iconaStato = IconaStatoEnum.RIFIUTO_VISTO;
		} else if (TipoOperazioneLibroFirmaEnum.SIGLA.equals(master.getOperazione())) {
			iconaStato = IconaStatoEnum.SIGLA;
		} else if (StatoDocumentoFuoriLibroFirmaEnum.SPEDIZIONE_CARTACEA.equals(master.getStato())) {
			iconaStato = IconaStatoEnum.SPEDIZIONE_CARTACEA;
		} else if (StatoDocumentoFuoriLibroFirmaEnum.SPEDIZIONE_ELETTRONICA.equals(master.getStato())) {
			iconaStato = IconaStatoEnum.SPEDIZIONE_ELETTRONICA;
		} else if (StatoDocumentoFuoriLibroFirmaEnum.SPEDIZIONE_MISTA.equals(master.getStato())) {
			iconaStato = IconaStatoEnum.SPEDIZIONE_MISTA;
		} else if (TipoOperazioneLibroFirmaEnum.VISTA.equals(master.getOperazione())) {
			iconaStato = IconaStatoEnum.VISTO;
		} else if (TipoOperazioneLibroFirmaEnum.FIRMA_MULTIPLA.equals(master.getOperazione())) {
			iconaStato = IconaStatoEnum.FIRMA_MULTIPLA;
		} else if (StatoDocumentoFuoriLibroFirmaEnum.RIFIUTO_FIRMA_MULTIPLA.equals(master.getStato())
				|| TipoOperazioneLibroFirmaEnum.RIFIUTO_FIRMA_MULTIPLA.equals(master.getOperazione())) {
			iconaStato = IconaStatoEnum.RIFIUTO_FIRMA_MULTIPLA;
		} else {
			iconaStato = IconaStatoEnum.DOCUMENTO_IN_ATTESA;
		}

		master.setIconaStato(iconaStato);
	}

	/**
	 * Metodo per verificare se il documento è in entrata, è stato aperto un
	 * documento in risposta, ma questo non è stato ancora chiuso.
	 * 
	 * @param master     master
	 * @param idAoo      identificativo aoo
	 * @param connection connessione
	 * @return esito test
	 */
	private Boolean isInAttesaChiusuraRisposta(final MasterDocumentoDTO master, final Long idAoo, final Connection connection) {
		final Boolean isEntrata = isEntrata(master.getTipoProtocollo(), master.getIdMomentoProtocollazione());
		final Boolean hasWf = !StringUtils.isNullOrEmpty(master.getWobNumber());
		final Boolean bHasAllacci = allaccioDAO.hasAllacci(master.getDocumentTitle(), idAoo, connection);
		Boolean isLavorato = false;
		if (master.getStato() != null) {
			isLavorato = master.getStato().getValue() == 1;
		}
		return isEntrata && !hasWf && isLavorato && bHasAllacci;
	}

	/**
	 * Checks if is entrata.
	 *
	 * @param tipoProtocollo         the tipo protocollo
	 * @param momentoProtocollazione the momento protocollazione
	 * @return the boolean
	 */
	@Override
	public final Boolean isEntrata(final Integer tipoProtocollo, final Integer momentoProtocollazione) {
		return !isUscita(tipoProtocollo, momentoProtocollazione);
	}

	/**
	 * Checks if is uscita.
	 *
	 * @param tipoProtocollo         the tipo protocollo
	 * @param momentoProtocollazione the momento protocollazione
	 * @return the boolean
	 */
	@Override
	public final Boolean isUscita(final Integer tipoProtocollo, final Integer momentoProtocollazione) {
		Boolean output = false;
		if ((tipoProtocollo == null && momentoProtocollazione != null)
				|| (tipoProtocollo != null && Constants.Protocollo.TIPO_PROTOCOLLO_USCITA == tipoProtocollo.intValue())) {
			output = true;
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentoFacadeSRV#annullaDocumento
	 *      (com.filenet.api.core.Document, java.lang.String,
	 *      it.ibm.red.business.helper.filenet.pe.FilenetPEHelper,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetHelper,
	 *      filenet.vw.api.VWWorkObject).
	 */
	@Override
	public void annullaDocumento(final Document inDocAnnullato, final String motivoAnnullamento, final FilenetPEHelper peh, final IFilenetCEHelper ceh, final VWWorkObject wo,
			final Long idAoo) throws IOException {
		final IAdobeLCHelper adobeh = new AdobeLCHelper();

		Document docAnnullato = inDocAnnullato;

		final InputStream isAnnullato = adobeh.insertWatermark(docAnnullato.accessContentStream(0), "ANNULLATO");

		final Map<String, Object> map = new HashMap<>();
		map.put("motivazioneAnnullamento", motivoAnnullamento);
		map.put("dataAnnullamentoDocumento", new Date());

		docAnnullato = ceh.addVersion(docAnnullato, isAnnullato, map, docAnnullato.get_Name(), URLConnection.guessContentTypeFromStream(isAnnullato), idAoo);

		final Map<String, Object> metadata = new HashMap<>();
		metadata.put("Annullato", true);
		peh.updateWorkflow(wo, metadata);
		peh.chiudiWF(wo);

		docAnnullato.save(RefreshMode.REFRESH);
	}

	@Override
	public final boolean hasRichiesteContributoPendenti(final String documentTitle, final Long idAoo) {
		boolean hasRichiesteContributoPendenti = false;
		Connection dwhCon = null;

		try {
			if (!StringUtils.isNullOrEmpty(documentTitle)) {
				dwhCon = setupConnection(getFilenetDataSource().getConnection(), false);

				// Si recuperano gli eventi relativi al documento
				final List<EventoLogDTO> eventiDocumento = eventoLogDAO.getEventiStoricoByIdDocumento(Integer.parseInt(documentTitle), idAoo, dwhCon);

				// Si cerca l'evento di Richiesta Contributo
				if (!CollectionUtils.isEmpty(eventiDocumento)) {
					for (final EventoLogDTO eventoDocumento : eventiDocumento) {
						if (EventTypeEnum.RICHIESTA_CONTRIBUTO.getValue().equals(eventoDocumento.getEventType())) {
							hasRichiesteContributoPendenti = true;
							break;
						}
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error(
					"hasRichiesteContributoPendenti -> Si è verificato un errore durante la verifica di richieste di contributo pendenti per il documento: " + documentTitle,
					e);
			throw new RedException(e);
		} finally {
			closeConnection(dwhCon);
		}

		return hasRichiesteContributoPendenti;
	}

	@Override
	public final boolean isDocumentoFaldonaturaObbligatoria(final String documentTitle, final Long idAoo, final FilenetCredentialsDTO fcDTO) {
		boolean isDocumentoFaldonaturaObbligatoria = false;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(fcDTO, idAoo);
			final PropertiesProvider pp = PropertiesProvider.getIstance();
			final List<String> selectList = new ArrayList<>(2);
			final PropertyFilter pf = new PropertyFilter();

			final FilterElement feIdTipologiaDoc = new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY), null);
			final FilterElement feIdTipoProc = new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY), null);

			selectList.add(feIdTipologiaDoc.getValue());
			selectList.add(feIdTipoProc.getValue());

			pf.addIncludeProperty(feIdTipologiaDoc);
			pf.addIncludeProperty(feIdTipoProc);

			final Document doc = fceh.getDocumentByIdGestionale(documentTitle, selectList, pf, idAoo.intValue(), null, null);

			final Integer idTipologiaDocumento = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY);
			final Integer idTipoProcedimento = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY);

			isDocumentoFaldonaturaObbligatoria = isDocumentoFaldonaturaObbligatoria(idTipologiaDocumento, idTipoProcedimento);
		} catch (final Exception e) {
			LOGGER.error(e);
		} finally {
			popSubject(fceh);
		}

		return isDocumentoFaldonaturaObbligatoria;
	}

	@Override
	public final boolean isDocumentoFaldonaturaObbligatoria(final Integer idTipologiaDocumento, final Integer idTipoProcedimento) {
		boolean isDocumentoFaldonaturaObbligatoria = false;
		final PropertiesProvider pp = PropertiesProvider.getIstance();

		final String coppieTipoDocProcFaldonaturaObbligatoria = pp.getParameterByKey(PropertiesNameEnum.COPPIA_TIPOLOGIA_DOC_PROC_FALDONATURA_OBBLIGATORIA);
		if (!StringUtils.isNullOrEmpty(coppieTipoDocProcFaldonaturaObbligatoria)) {
			final String[] coppie = coppieTipoDocProcFaldonaturaObbligatoria.split(";");

			for (final String coppia : coppie) {
				final String[] coppiaFaldonaturaObbligatoria = coppia.split(",");
				final Integer idTipologiaDocumentoCoppia = Integer.valueOf(coppiaFaldonaturaObbligatoria[0]);
				final Integer idTipoProcedimentoCoppia = Integer.valueOf(coppiaFaldonaturaObbligatoria[1]);

				if (idTipologiaDocumentoCoppia.equals(idTipologiaDocumento) && idTipoProcedimentoCoppia.equals(idTipoProcedimento)) {
					isDocumentoFaldonaturaObbligatoria = true;
					break;
				}
			}
		}

		return isDocumentoFaldonaturaObbligatoria;
	}

	@Override
	public final InputStream createPdfApprovazioniStampigliate(final UtenteDTO utente, final Integer numeroDocumento, final String documentTitle,
			final List<ApprovazioneDTO> approvazioneList) {
		Connection con = null;
		Connection conDWH = null;

		try {
			final ObjectFactory factory = new ObjectFactory();

			final ApprovazioniType approvazioni = factory.createApprovazioniType();
			for (final ApprovazioneDTO current : approvazioneList) {
				final ApprovazioneType app = factory.createApprovazioneType();

				final Date data = current.getDataApprovazione();
				final SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateUtils.DD_MM_YYYY);
				app.setData(simpleDateFormat.format(data));
				app.setStampigliatura(current.getStampigliaturaFirma().toString());

				// Per qualche ragione questo non lo converte
				final TipoApprovazioneEnum tipo = TipoApprovazioneEnum.get(current.getIdTipoApprovazione());
				final String tipoApprovazione = tipo.getDescrizione().toUpperCase();
				app.setTipoApprovazione(tipoApprovazione);

				app.setUfficio(current.getDescUfficio());
				app.setUtente(current.getDescUtente());

				approvazioni.getApprovazione().add(app);
			}

			con = setupConnection(getDataSource().getConnection(), false);
			conDWH = setupConnection(getFilenetDataSource().getConnection(), false);
			final EventoLogDTO evento = eventoLogDAO.getFirstUserEvent(Integer.parseInt(documentTitle), utente.getIdAoo(), conDWH);
			final Nodo ispettorato = nodoDAO.getNodo(evento.getIdUfficioMittente(), con);

			final FormType form = factory.createFormType();
			form.setProtocollo(numeroDocumento.toString());
			form.setApprovazioni(approvazioni);
			form.setIspettorato(ispettorato.getDescrizione());

			final IAdobeLCHelper adobeh = new AdobeLCHelper();
			return adobeh.generateApprovazioniPdf(utente.getCodiceAoo(), form);
		} catch (final Exception e) {
			LOGGER.error("createPdfApprovazioniStampigliate -> Si è verificato un errore durante la creazione del PDF per stampigliare le approvazioni", e);
			throw new RedException("Errore durante la creazione del PDF per stampigliare le approvazioni");
		} finally {
			closeConnection(con);
			closeConnection(conDWH);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentoFacadeSRV#codiceAmministrazioneFromWob(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public String[] codiceAmministrazioneFromWob(final String idDocumento, final UtenteDTO utente) {
		IFilenetCEHelper fceh = null;
		final String[] infoFad = new String[2];
		String amministrazione = null;
		String identificativoRaccolta = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final Document doc = fceh.getDocumentByIdGestionale(idDocumento, null, null, utente.getIdAoo().intValue(), null, null);
			amministrazione = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.AMMINISTRAZIONE_FAD_METAKEY);
			identificativoRaccolta = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.ID_RACCOLTA_FAD_METAKEY);

			if (identificativoRaccolta == null) {
				identificativoRaccolta = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.ID_RACCOLTA_PROVVISORIA_METAKEY);
			}

			infoFad[0] = identificativoRaccolta;
			infoFad[1] = amministrazione;
		} catch (final Exception e) {
			LOGGER.error(e);
		} finally {
			popSubject(fceh);
		}

		return infoFad;
	}

	/**
	 * Restituisce {@code true} se la tipologia documentale identificata dall'
	 * {@code idTipologiaDocumento} è associata al flusso Atto Decreto.
	 * 
	 * @param utente
	 *            Utente in sessione.
	 * @param idTipologiaDocumento
	 *            Identificativo della tipologia documento.
	 * @return {@code true} se tipologia associata flusso Atto Decreto,
	 *         {@code false} altrimenti.
	 */
	@Override
	public boolean checkIsFlussoAttoDecretoUCB(final UtenteDTO utente, final Long idTipologiaDocumento) {
		return signSRV.checkIsFlussoAttoDecretoUCB(utente, idTipologiaDocumento);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentoFacadeSRV#checkIsFlussiAutUCB(java.lang.String,
	 *      it.ibm.red.business.dto.RispostaAllaccioDTO,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public boolean checkIsFlussiAutUCB(final String idDocUscita, final RispostaAllaccioDTO allaccioPrincipale, final UtenteDTO utente) {
		IFilenetCEHelper fceh = null;
		Connection con = null;
		boolean output = false;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			con = setupConnection(getDataSource().getConnection(), false);

			output = signSRV.checkIsFlussiAutUCB(idDocUscita, allaccioPrincipale, utente, fceh, con);

		} catch (final Exception e) {
			LOGGER.error(e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}

		return output;
	}

	/*** REALIZZAZIONE Servizi collegati a Protocollo START ***/

	private UtenteDTO preparaUtenteDTO(final Long idUtente, final Long idUfficio, final Long idRuolo) {
		Nodo ufficioUtente = null;
		Ruolo ruoloUtente = null;
		UtenteDTO utente = null;
		utente = utenteSRV.getById(idUtente);
		ufficioUtente = nodoSRV.getNodo(idUfficio);
		ruoloUtente = lookupSRV.getRuolo(idRuolo);
		assegnaUfficioSRV.popolaUtente(utente, ufficioUtente, ruoloUtente);
		return utente;
	}

	private DetailDocumentoDTO prepareDetail(final String documentTitle, final Integer idAOO, final IFilenetCEHelper fceh) {
		DetailDocumentoDTO documentDetailDTO = null;
		final Document document = fceh.getDocumentByIdGestionale("" + documentTitle, null, null, idAOO, null, null);
		documentDetailDTO = TrasformCE.transform(document, TrasformerCEEnum.FROM_DOCUMENTO_TO_DETAIL);
		return documentDetailDTO;
	}

	private VWWorkObject prepareWO(final String wobNumber, final FilenetPEHelper fpeh) {
		VWWorkObject wo = null;
		wo = fpeh.getWorkFlowByWob(wobNumber, true);
		return wo;
	}

	/**
	 * @see it.ibm.red.business.service.IDocumentoSRV#richiediProtocolloNPSync(java.lang.String,
	 *      java.lang.String, java.lang.Integer, java.lang.Long, java.lang.Long,
	 *      java.lang.Long, boolean).
	 */
	@Override
	public ProtocolloNpsDTO richiediProtocolloNPSync(final String wobNumber, final String documentTitle, final Integer idAOO, final Long idUtente, final Long idUfficio,
			final Long idRuolo, final boolean isRiservato) {
		DetailDocumentoDTO documentDetailDTO = null;
		UtenteDTO utente = null;
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;
		Connection connection = null;
		VWWorkObject wo = null;
		ProtocolloNpsDTO protocollo = null;
		try {
			utente = preparaUtenteDTO(idUtente, idUfficio, idRuolo);
			connection = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			documentDetailDTO = prepareDetail(documentTitle, idAOO, fceh);
			signSRV.checkIsRegistroRepertorio(utente, documentDetailDTO, connection);
			wo = prepareWO(wobNumber, fpeh);
			protocollo = signSRV.getNewProtocolloNps(wo, documentDetailDTO, utente, fceh, connection, isRiservato);
		} catch (final Exception e) {
			LOGGER.error(ERROR_STACCO_PROTOCOLLO_MSG, e);
		} finally {
			closeConnection(connection);
			popSubject(fceh);
			logoff(fpeh);
		}
		return protocollo;
	}

	/**
	 * @see it.ibm.red.business.service.IDocumentoSRV#associateDocumentoProtocolloToAsyncNps(java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.Integer,
	 *      java.lang.Long, java.lang.Long, java.lang.Long, boolean, boolean,
	 *      java.lang.String, int, boolean).
	 */
	@Override
	public final boolean associateDocumentoProtocolloToAsyncNps(final String documentTitle, final String protocolloGuid, final String nomeFile, final String oggetto,
			final Integer idAOO, final Long idUtente, final Long idUfficio, final Long idRuolo, final boolean isPrincipale, final boolean daInviare,
			final String numeroAnnoProtocollo, final int narUpload, final boolean isProtocollazioneP7M) {

		boolean esito = false;
		UtenteDTO utente = null;
		IFilenetCEHelper fceh = null;
		Connection connection = null;
		try {
			utente = preparaUtenteDTO(idUtente, idUfficio, idRuolo);
			connection = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			Document documento = fceh.getDocumentByIdGestionale(documentTitle, null, null, idAOO, null, null);
			if (documento == null) {
				// Se è null non è un principale ma un allegato
				documento = fceh.getAllegatoForDownload(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY), documentTitle);
			}
			final String guidNewDoc4NPS = StringUtils.cleanGuidToString(documento.get_Id());

			npsSRV.associateDocumentoProtocolloToAsyncNps(narUpload, guidNewDoc4NPS, protocolloGuid, nomeFile, oggetto, utente, isPrincipale, daInviare, null, null,
					numeroAnnoProtocollo, documentTitle, connection);

			esito = true;
		} catch (final Exception e) {
			LOGGER.error(ERROR_STACCO_PROTOCOLLO_MSG, e);
		} finally {
			closeConnection(connection);
			popSubject(fceh);
		}
		return esito;

	}

	/**
	 * upload document.
	 *
	 * @param contentIS   the content IS
	 * @param guid        the guid
	 * @param contentType the content type
	 * @param nomefile    the nomefile
	 * @param descrizione the descrizione
	 * @param connection  the connection
	 */
	@Override
	public final int uploadDocToAsyncNps(final InputStream contentIS, final String documentTitle, final String protocolloGuid, final String contentType, final String nomefile,
			final String oggetto, final String numeroAnnoProtocollo, final Integer idAOO, final Long idUtente, final Long idUfficio, final Long idRuolo) {

		int idNar = 0;
		UtenteDTO utente = null;
		IFilenetCEHelper fceh = null;
		Connection connection = null;
		try {
			utente = preparaUtenteDTO(idUtente, idUfficio, idRuolo);
			connection = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			Document documento = fceh.getDocumentByIdGestionale(documentTitle, null, null, idAOO, null, null);
			if (documento == null) {
				// se è null non è un principale ma un allegato
				documento = fceh.getAllegatoForDownload(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY), documentTitle);
			}
			final String guidNewDoc4NPS = StringUtils.cleanGuidToString(documento.get_Id());

			idNar = npsSRV.uploadDocToAsyncNps(contentIS, guidNewDoc4NPS, protocolloGuid, contentType, nomefile, oggetto, numeroAnnoProtocollo, utente, documentTitle,
					connection);
		} catch (final Exception e) {
			LOGGER.error(ERROR_STACCO_PROTOCOLLO_MSG, e);
			idNar = 0;
		} finally {
			closeConnection(connection);
			popSubject(fceh);
		}
		return idNar;

	}

	/**
	 * aggiungi allacci.
	 *
	 * @param utente               the utente
	 * @param connection           the connection
	 * @param idProtocollo         the id protocollo
	 * @param allacci              the allacci
	 * @param numeroAnnoProtocollo the numero anno protocollo
	 * @param isRisposta           the is risposta
	 */
	@Override
	public final boolean aggiungiAllacciAsync(final Integer idAOO, final Long idUtente, final Long idUfficio, final Long idRuolo, final String documentTitle,
			final String idProtocollo, final String numeroAnnoProtocollo, final boolean isRisposta) {
		Collection<RispostaAllaccioDTO> documentiDaAllacciare = null;
		boolean esito = false;
		UtenteDTO utente = null;
		Connection connection = null;
		Aoo aoo;

		try {
			utente = preparaUtenteDTO(idUtente, idUfficio, idRuolo);
			connection = setupConnection(getDataSource().getConnection(), false);
			aoo = aooDAO.getAoo(utente.getIdAoo(), connection);
			documentiDaAllacciare = allaccioDAO.getDocumentiRispostaAllaccio(Integer.parseInt(documentTitle), idAOO, connection);

			if (!CollectionUtils.isEmpty(documentiDaAllacciare)) {
				final Iterator<RispostaAllaccioDTO> itDocumentiDaAllacciare = documentiDaAllacciare.iterator();
				while (itDocumentiDaAllacciare.hasNext()) {
					// allaccio to NPS solo se il documento è stato selezionato come "da chiudere"
					if (!Boolean.TRUE.equals(itDocumentiDaAllacciare.next().getDocumentoDaChiudere())) {
						itDocumentiDaAllacciare.remove();
					}
				}

				// allacci to NPS
				if (TipologiaProtocollazioneEnum.isProtocolloNPS(aoo.getTipoProtocollo().getIdTipoProtocollo()) && !documentiDaAllacciare.isEmpty()) {
					npsSRV.aggiungiAllacciAsync(utente, connection, idProtocollo, documentiDaAllacciare, numeroAnnoProtocollo, true, documentTitle);
				}
			}

			esito = true;
		} catch (final Exception e) {
			LOGGER.error(ERROR_STACCO_PROTOCOLLO_MSG, e);
		} finally {
			closeConnection(connection);
		}

		return esito;
	}

	@Override
	public final boolean spedisciProtocolloUscitaAsync(final String protocolloGuid, final String infoProtocollo, final Integer idAOO, final Long idUtente,
			final Long idUfficio, final Long idRuolo, final String documentTitle, final String numeroAnnoProtocollo, final String dataSpedizione) {
		boolean esito = false;
		UtenteDTO utente = null;
		IFilenetCEHelper fcehAdmin = null;
		Connection connection = null;
		try {
			utente = preparaUtenteDTO(idUtente, idUfficio, idRuolo);
			connection = setupConnection(getDataSource().getConnection(), false);

			fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()), utente.getIdAoo());
			final Document mail = fcehAdmin.fetchMailSpedizione(documentTitle, idAOO);

			String oggettoMessaggio = null;
			String corpoMessaggio = null;
			if (mail != null) {
				oggettoMessaggio = mail.getProperties().getStringValue(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.OGGETTO_EMAIL_METAKEY));

				ContentTransfer ct;
				if (FilenetCEHelper.hasDocumentContentTransfer(mail)) {
					ct = FilenetCEHelper.getDocumentContentTransfer(mail);
				} else {
					throw new RedException("Errore durante il recupero del content della mail con idDocumento: " + documentTitle);
				}

				if (ct != null) {
					corpoMessaggio = new String(FileUtils.toByteArrayHtml(mail.accessContentStream(0)), StandardCharsets.UTF_8.name());
				}
			}

			npsSRV.spedisciProtocolloUscitaAsync(utente, infoProtocollo, protocolloGuid, dataSpedizione, oggettoMessaggio, corpoMessaggio, documentTitle, connection);

			esito = true;
		} catch (final Exception e) {
			LOGGER.error(ERROR_STACCO_PROTOCOLLO_MSG, e);
		} finally {
			closeConnection(connection);
			popSubject(fcehAdmin);
		}
		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentoFacadeSRV#gestisciDestinatariInterni(java.lang.String,
	 *      java.lang.String, java.lang.Integer, java.lang.Long, java.lang.Long,
	 *      java.lang.Long, java.lang.Integer, java.lang.String, boolean).
	 */
	@Override
	public boolean gestisciDestinatariInterni(final String wobNumber, final String documentTitle, final Integer idAOO, final Long idUtente, final Long idUfficio,
			final Long idRuolo, final Integer numeroProtocollo, final String annoProtocollo, final boolean isRiservato) {
		DetailDocumentoDTO documentDetailDTO = null;
		UtenteDTO utente = null;
		IFilenetCEHelper fceh = null;
		Connection connection = null;
		ProtocolloDTO protocollo = null;
		Document docFilenet = null;
		try {
			utente = preparaUtenteDTO(idUtente, idUfficio, idRuolo);
			connection = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			documentDetailDTO = prepareDetail(documentTitle, idAOO, fceh);
			signSRV.checkIsRegistroRepertorio(utente, documentDetailDTO, connection);
			docFilenet = fceh.getDocumentRedForDetail(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documentTitle);
			documentDetailDTO.setContent(FilenetCEHelper.getDocumentContentAsByte(docFilenet));
			// dovrebbe essere presente per i repertori
			if (numeroProtocollo != null && numeroProtocollo != 0 && !StringUtils.isNullOrEmpty(annoProtocollo) && annoProtocollo.length() == 4) {
				protocollo = new ProtocolloDTO(null, numeroProtocollo, annoProtocollo, null, null, null, null);
			}
			signSRV.gestisciDestinatariInterni(docFilenet, documentDetailDTO, protocollo, utente, fceh, connection);
		} catch (final Exception e) {
			LOGGER.error(ERROR_STACCO_PROTOCOLLO_MSG, e);
			protocollo = null;
			return false;
		} finally {
			closeConnection(connection);
			popSubject(fceh);
		}

		return true;
	}
	
	
	/**
	 * @see it.ibm.red.business.service.facade.IDocumentoFacadeSRV#getMasterFromGuidPrinc(it.ibm.red.business.dto.UtenteDTO, java.lang.String)
	 */
	@Override
	public MasterDocumentRedDTO getMasterFromGuidPrinc(final UtenteDTO utente, final String guid) {
		MasterDocumentRedDTO filePrinc = null;
		IFilenetCEHelper fceh = null;
		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			Document d = fceh.getDocumentByGuid(guid);

			filePrinc = TrasformCE.transform(d, TrasformerCEEnum.FROM_DOCUMENTO_TO_GENERIC_DOC);

		} catch (FilenetException e) {
			LOGGER.error(ERRORE_DURANTE_IL_RECUPERO_DEL_CONTENT_DEL_DOCUMENTO_CON_DOCUMENT_TITLE + guid, e);
			throw e;
		} finally {
			popSubject(fceh);
		}

		return filePrinc;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentoFacadeSRV#getFilePrincipaleFromAllegato(it.ibm.red.business.dto.FilenetCredentialsDTO,
	 *      java.lang.String, java.lang.Boolean, boolean,
	 *      it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.enums.TipoAssegnazioneEnum, java.lang.String[],
	 *      it.ibm.red.business.dto.PlaceholderInfoDTO).
	 */
	@Override
	public Long getFilePrincipaleFromAllegato(final FilenetCredentialsDTO fcDTO, final String guid, final Boolean onlyPdf, final boolean nomeFile, final UtenteDTO utente,
			final TipoAssegnazioneEnum tipoFirma, final String[] siglatarioPrincipaleString, final PlaceholderInfoDTO placeholderInfo) {
		Long idUtenteFirmatario = null;
		IFilenetCEHelper fceh = null;
		try {
			fceh = FilenetCEHelperProxy.newInstance(fcDTO, utente.getIdAoo());

			final Document d = fceh.getDocumentForAllegato(guid, nomeFile);
			final String docTitle = (String) TrasformerCE.getMetadato(d, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
			final String iterApprovativo = (String) TrasformerCE.getMetadato(d, PropertiesNameEnum.ITER_APPROVATIVO_METAKEY);

			if ("Manuale".equalsIgnoreCase(iterApprovativo) && TipoAssegnazioneEnum.FIRMA.equals(tipoFirma)) {
				idUtenteFirmatario = listaDocumentiSRV.getMetadatoIdUtenteFirmatario(docTitle, utente);
			} else if (!"Manuale".equalsIgnoreCase(iterApprovativo)) {
				final String[] siglatariAll = stampigliaturaSiglaSRV.getFirmatariStringArray(siglatarioPrincipaleString, utente.getIdUfficio(), tipoFirma.getId());
				if (siglatariAll != null && siglatariAll.length > 0) {
					idUtenteFirmatario = Long.valueOf(siglatariAll[0]);
				}
			}

			storicoSRV = ApplicationContextProvider.getApplicationContext().getBean(IStoricoFacadeSRV.class);
			final Collection<StoricoDTO> eventiStorico = storicoSRV.getStorico(Integer.parseInt(docTitle), utente.getIdAoo().intValue());
			if (eventiStorico != null && !eventiStorico.isEmpty()) {
				placeholderInfo.setDaFirmare(false);
				for (final StoricoDTO evento : eventiStorico) {
					final boolean isFirmato = EventTypeEnum.FIRMATO.getIntValue().equals(evento.getIdTipoOperazione());
					if (isFirmato) {
						placeholderInfo.setDaFirmare(isFirmato);
						break;
					}
				}
			}

		} catch (final FilenetException e) {
			LOGGER.error(ERROR_RECUPERO_CONTENT_MSG + guid, e);
			throw e;
		} finally {
			popSubject(fceh);
		}

		return idUtenteFirmatario;
	}

	/*** REALIZZAZIONE Servizi collegati a Protocollo END ***/

	@Override
	public GestioneLockDTO getLockDocumento(final Integer documentTitle, final Long idAoo, final Integer statoLock, final Long idUtente) {
		Connection conn = null;
		GestioneLockDTO gestioneLockDTO;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			gestioneLockDTO = documentoDAO.getLockDocumento(documentTitle, idAoo, statoLock, idUtente, conn);

		} catch (final Exception ex) {
			LOGGER.error(ERROR_RECUPERO_LOCK_MSG + ex);
			throw new RedException(ERROR_RECUPERO_LOCK_MSG + ex);
		} finally {
			closeConnection(conn);
		}
		return gestioneLockDTO;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentoFacadeSRV#insertLockDocumento(java.lang.Integer,
	 *      java.lang.Long, java.lang.Integer, java.lang.Long, java.lang.String,
	 *      java.lang.String, boolean).
	 */
	@Override
	public boolean insertLockDocumento(final Integer documentTitle, final Long idAoo, final Integer statoLock, final Long idUtente, final String nome, final String cognome,
			final boolean isIncarico) {
		Connection conn = null;
		boolean output = false;
		try {
			conn = setupConnection(getDataSource().getConnection(), true);
			output = documentoDAO.insertLockDocumento(documentTitle, idAoo, idUtente, statoLock, isIncarico, nome, cognome, conn);
			commitConnection(conn);
		} catch (final Exception ex) {
			rollbackConnection(conn);
			LOGGER.error("Errore durante l'inserimento del lock del documento :" + ex);
			throw new RedException("Errore durante l'inserimento del lock del documento :" + ex);
		} finally {
			closeConnection(conn);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentoFacadeSRV#updateLockDocumento(java.lang.Integer,
	 *      java.lang.Long, java.lang.Integer, boolean, java.lang.Long,
	 *      java.lang.String, java.lang.String).
	 */
	@Override
	public boolean updateLockDocumento(final Integer documentTitle, final Long idAoo, final Integer statoLock, final boolean isIncarico, final Long idUtente,
			final String nome, final String cognome) {
		Connection conn = null;
		boolean output = false;
		try {
			conn = setupConnection(getDataSource().getConnection(), true);
			output = documentoDAO.updateLockDocumento(documentTitle, idAoo, statoLock, idUtente, isIncarico, nome, cognome, conn);
			commitConnection(conn);
		} catch (final Exception ex) {
			rollbackConnection(conn);
			LOGGER.error("Errore durante l'update del lock del documento :" + ex);
			throw new RedException("Errore durante l'update del lock del documento :" + ex);
		} finally {
			closeConnection(conn);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentoFacadeSRV#deleteLockDocumento(java.lang.Integer,
	 *      java.lang.Long, java.lang.Integer, java.lang.Long).
	 */
	@Override
	public boolean deleteLockDocumento(final Integer documentTitle, final Long idAoo, final Integer statoLock, final Long idUtente) {
		Connection conn = null;
		boolean output = false;
		try {
			conn = setupConnection(getDataSource().getConnection(), true);
			output = documentoDAO.deleteLockDocumento(documentTitle, idAoo, statoLock, idUtente, conn);
			commitConnection(conn);
		} catch (final Exception ex) {
			rollbackConnection(conn);
			LOGGER.error("Errore durante l'eliminazione del lock del documento :" + ex);
			throw new RedException("Errore durante l'eliminazione del lock del documento :" + ex);
		} finally {
			closeConnection(conn);
		}
		return output;

	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentoFacadeSRV#getLockDocumentoMassivo(java.util.List,
	 *      java.lang.Long, java.lang.Integer, java.lang.Long).
	 */
	@Override
	public GestioneLockDTO getLockDocumentoMassivo(final List<Integer> documentTitle, final Long idAoo, final Integer statoLock, final Long idUtente) {
		Connection conn = null;
		GestioneLockDTO gestioneLockDTO;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			gestioneLockDTO = documentoDAO.getLockDocumentoMassivo(documentTitle, idAoo, statoLock, idUtente, conn);
			if (gestioneLockDTO.getNessunLock() != null && !gestioneLockDTO.getNessunLock()) {
				final UtenteDTO utenteCheHaLock = utenteSRV.getById(gestioneLockDTO.getIdUtenteCheHaLock());
				gestioneLockDTO.setUsernameUtenteLock(utenteCheHaLock.getNome() + " " + utenteCheHaLock.getCognome());
			}
		} catch (final Exception ex) {
			LOGGER.error(ERROR_RECUPERO_LOCK_MSG + ex);
			throw new RedException(ERROR_RECUPERO_LOCK_MSG + ex);
		} finally {
			closeConnection(conn);
		}
		return gestioneLockDTO;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentoFacadeSRV#getLockDocumentiMaster(java.util.List,
	 *      java.lang.Long).
	 */
	@Override
	public HashMap<Long, GestioneLockDTO> getLockDocumentiMaster(final List<String> documentTitle, final Long idAoo) {
		Connection conn = null;
		HashMap<Long, GestioneLockDTO> gestioneLockDTO = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			gestioneLockDTO = documentoDAO.getLockDocumentiMaster(documentTitle, idAoo, conn);
		} catch (final Exception ex) {
			LOGGER.error(ERROR_RECUPERO_LOCK_MSG + ex);
			throw new RedException(ERROR_RECUPERO_LOCK_MSG + ex);
		} finally {
			closeConnection(conn);
		}
		return gestioneLockDTO;
	}

}