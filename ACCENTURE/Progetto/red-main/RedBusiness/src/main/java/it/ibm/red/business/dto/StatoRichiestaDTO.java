package it.ibm.red.business.dto;

import it.ibm.red.business.persistence.model.StatoRichiesta;

/**
 * The Class StatoRichiestaDTO.
 *
 * @author CPIERASC
 * 
 * 	DTO stato richiesta.
 */
public class StatoRichiestaDTO extends AbstractDTO {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificativo stato richiesta.
	 */
	private int statoRichiestaID;

	/**
	 * Descrizione.
	 */
	private String descrizione;

	/**
	 * Nome step.
	 */
	private String stepName;
	
	/**
	 * Costruttore.
	 * 
	 * @param sr	model
	 */
	public StatoRichiestaDTO(final StatoRichiesta sr) {
		super();
		if (sr != null) {
			this.statoRichiestaID = sr.getStatoRichiestaID();
			this.descrizione = sr.getDescrizione();
			this.stepName = sr.getStepName();
		}
	}

	/**
	 * Getter.
	 * 
	 * @return	identificativo stato richiesta
	 */
	public final int getStatoRichiestaID() {
		return statoRichiestaID;
	}

	/**
	 * Getter.
	 * 
	 * @return	descrizione
	 */
	public final String getDescrizione() {
		return descrizione;
	}

	/**
	 * Getter.
	 * 
	 * @return	nome step
	 */
	public final String getStepName() {
		return stepName;
	}

}
