package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.springframework.util.CollectionUtils;

import com.filenet.api.core.Document;

import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.DetailEmailDTO;
import it.ibm.red.business.dto.ErroreValidazioneDTO;
import it.ibm.red.business.dto.NotaDTO;
import it.ibm.red.business.enums.ColoreInteroperabilitaEnum;
import it.ibm.red.business.enums.ColoreNotaEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.StatoMailEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * Trasformer dettaglio mail.
 *
 * @author CPIERASC
 */
public class FromDocumentoToMailDetailTrasformer extends TrasformerCE<DetailEmailDTO> {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 7115280317344104212L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FromDocumentoToMailDetailTrasformer.class.getName());
	
	/**
	 * Dao gestione utente.
	 */
	private final IUtenteDAO utenteDAO;
	
	/**
	 * Costruttore.
	 */
	public FromDocumentoToMailDetailTrasformer() {
		super(TrasformerCEEnum.FROM_DOCUMENTO_TO_MAIL_DETAIL);
		utenteDAO = ApplicationContextProvider.getApplicationContext().getBean(IUtenteDAO.class);
	}

	/**
	 * Trasform.
	 *
	 * @param document
	 *            the document
	 * @param connection
	 *            the connection
	 * @return the detail email DTO
	 */
	@Override
	public final DetailEmailDTO trasform(final Document document, final Connection connection) {
		DetailEmailDTO mail = null;
		
		try {
			final String inGuid = StringUtils.cleanGuidToString(document.get_Id());
			final String inOggetto = (String) getMetadato(document, PropertiesNameEnum.OGGETTO_EMAIL_METAKEY);
			final String inMittente = (String) getMetadato(document, PropertiesNameEnum.FROM_MAIL_METAKEY);
			final String inDestinatario = (String) getMetadato(document, PropertiesNameEnum.DESTINATARI_EMAIL_METAKEY);
			final String inDestinatarioCC = (String) getMetadato(document, PropertiesNameEnum.DESTINATARI_CC_EMAIL_METAKEY);
			
			final String inMotivoEliminazione = (String) getMetadato(document, PropertiesNameEnum.MOTIVO_ELIMINAZIONE_EMAIL_METAKEY);
			
			final Date di = (Date) getMetadato(document, PropertiesNameEnum.DATA_INVIO_MAIL_METAKEY);
			final String inDataInvio = DateUtils.dateToString(di, false);
			
			final Date d = (Date) getMetadato(document, PropertiesNameEnum.DATA_RICEZIONE_MAIL_METAKEY);
			final String inDataArrivo = DateUtils.dateToString(d, false);
			
			final Date ds = (Date) getMetadato(document, PropertiesNameEnum.DATA_CREAZIONE_METAKEY);
			final String inDataScarico = DateUtils.dateToString(ds, false);
			
			final String inTesto = getText(document);
			String contentNote = (String) getMetadato(document, PropertiesNameEnum.NOTA_EMAIL_METAKEY);

			final Integer nStatoMail = (Integer) getMetadato(document, PropertiesNameEnum.STATO_MAIL_METAKEY);
			final StatoMailEnum statoMail = StatoMailEnum.get(nStatoMail);
			final Integer idUtenteProtocollatore = (Integer) getMetadato(document, PropertiesNameEnum.ID_UTENTE_PROTOCOLLATORE_METAKEY);
			Utente utenteProtocollatore = null;
			if (idUtenteProtocollatore != null) {
				utenteProtocollatore = utenteDAO.getUtente(idUtenteProtocollatore.longValue(), connection);
			}
			
			final Integer nStatoMailPreArch = (Integer) getMetadato(document, PropertiesNameEnum.METADATO_STATO_PRE_ARCHIVIAZIONE);
			final StatoMailEnum statoMailPreArch = StatoMailEnum.get(nStatoMailPreArch);
			

			String descProtocollatore = null;
			if (utenteProtocollatore != null) {
				descProtocollatore = utenteProtocollatore.getNome() + " " + utenteProtocollatore.getCognome();
			}
			
			final NotaDTO nota = new NotaDTO();
			if (contentNote != null) {
				final String[] note = StringUtils.splittaNota(contentNote);
				contentNote = note[1];
				final int index = getIndex(note);
				nota.setColore(ColoreNotaEnum.get(index));
				nota.setContentNota(contentNote);
			} 
			
			// ### Interoperabilità -> START
			final List<ErroreValidazioneDTO> erroriInteroperabilita = new ArrayList<>();
			ErroreValidazioneDTO err = null;
			
			// Si recuperano eventuali errori relativi all'interoperabilità
			final List<?> listErroriInteroperabilita = (List<?>) getMetadato(document, PropertiesNameEnum.ERRORI_VALIDAZIONE_SEGN_INTEROP_MAIL_METAKEY);
	 		if (!CollectionUtils.isEmpty(listErroriInteroperabilita)) {
	 			
 				for (final Object descrError : listErroriInteroperabilita) {
 					err = new ErroreValidazioneDTO((String) descrError, ColoreInteroperabilitaEnum.get(0));
 					erroriInteroperabilita.add(err);
 				}
 				
	 		}
	 		
	 		// Si recuperano eventuali warning  relativi all'interoperabilità
	 		final List<?> listWarningInteroperabilita = (List<?>) getMetadato(document, PropertiesNameEnum.WARNINGS_VALIDAZIONE_SEGN_INTEROP_MAIL_METAKEY);
	 		if (!CollectionUtils.isEmpty(listWarningInteroperabilita)) {	
	 			
		 		for (final Object descrWarning : listWarningInteroperabilita) {
 					err = new ErroreValidazioneDTO((String) descrWarning, ColoreInteroperabilitaEnum.get(1));
 					erroriInteroperabilita.add(err);
 				}
		 		
	 		}
			// ### Interoperabilità -> END

			mail = new DetailEmailDTO(inGuid, inOggetto, inMittente, inDestinatario, inDestinatarioCC, inDataArrivo, inTesto, contentNote, null,
					inMotivoEliminazione, statoMail, idUtenteProtocollatore, descProtocollatore, inDataInvio, nota, statoMailPreArch, erroriInteroperabilita, inDataScarico);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero di un metadato del dettaglio del documento: ", e);
		}
		
		return mail;
	}

	/**
	 * @param note
	 * @return indice
	 */
	private static int getIndex(final String[] note) {
		int index = 0;
		try {
			index = Integer.parseInt(note[0].substring(1));
		} catch (final Exception e) {
			LOGGER.warn(e);
		}
		return index;
	}

	/**
	 * @param document
	 * @return testo
	 */
	private String getText(final Document document) {
		String inTesto = "";
		
		try {
			
			if (FilenetCEHelper.hasDocumentContentTransfer(document)) {
				inTesto = IOUtils.toString(FilenetCEHelper.getDocumentContentAsInputStream(document)).replace("\\r", "").replace("\\n", "<br>").replace("\\p{C}", "");
			}
			
		} catch (final Exception e) {
			LOGGER.error("Problemi nel recupero della mail.", e);
		}
		return inTesto;
	}

}
