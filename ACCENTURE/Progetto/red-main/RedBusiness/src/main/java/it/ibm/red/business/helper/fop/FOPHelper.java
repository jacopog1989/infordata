package it.ibm.red.business.helper.fop;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.CharArrayWriter;
import java.io.File;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.poi.ss.formula.functions.T;
import org.apache.xmlgraphics.util.MimeConstants;
import org.exolab.castor.xml.MarshalException;
import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.ValidationException;

import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * Helper FOP.
 */
public final class FOPHelper {
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FOPHelper.class.getName());
	
	/**
	 * Factory.
	 */
	private static TransformerFactory tFactory = TransformerFactory.newInstance();
	
	/**
	 * Fop Factory.
	 */
	private static FopFactory fopFactory = FopFactory.newInstance(new File(".").toURI());
	
	/**
	 * Costruttore.
	 */
	private FOPHelper() {
		// Costruttore vuoto.
	}

	/**
	 * Metodo per eseguire il marshalling dell'oggetto java (trasformazione in una stringa che poi viene inserita in un Source).
	 * 
	 * @param objectToTrasform	oggetto da trasformare
	 * @return	la Source contenente la stringa in cui è stato trasformato l'oggetto fornito in ingresso
	 * @throws MarshalException		generata in caso d'errore della trasformazione
	 * @throws ValidationException	generata in caso d'errore della trasformazione
	 */
	private static Source getXMLSourceFromOBJ(final Object objectToTrasform) throws MarshalException, ValidationException {
		final String str = marshal(objectToTrasform);
	    LOGGER.info("Marshalling per FOP: " + str);
	    return new StreamSource(new StringReader(str));
	}

	/**
	 * Esegue il marshal dell'oggetto in ingresso restituendone la stringa.
	 * @param objectToTrasform
	 * @return String ottenuta dal marshalling
	 * @throws MarshalException
	 * @throws ValidationException
	 */
	public static String marshal(final Object objectToTrasform) throws MarshalException, ValidationException {
		final CharArrayWriter writer = new CharArrayWriter();
	    Marshaller.marshal(objectToTrasform, writer);
	    final StringBuilder result = new StringBuilder().append(writer.toCharArray());    
	    return result.toString();
	}
	
	private static Source getXMLSourceFromMAP(final Map<String, String> hm) {
	    final StringBuilder result = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
	    result.append("<parameters>");
	    for (final Entry<String, String> e:hm.entrySet()) {
		    result.append("<" + e.getKey() + ">");
		    result.append(StringEscapeUtils.escapeXml10(e.getValue()));
		    result.append("</" + e.getKey() + ">");
	    }
	    result.append("</parameters>");
	    final String str = result.toString();
	    LOGGER.info("Marshalling per FOP: " + str);
	    return new StreamSource(new StringReader(str));
	}
	
	/**
	 * Esegue la trasformazione in PDF tramite Apache FOP.
	 * @param objectToTrasform
	 * @param xslt
	 * @return byte array del pdf generato
	 */
	public static byte[] transformOBJ2PDF(final T objectToTrasform, final byte[] xslt) {
		try {
			return transformSRC2PDF(getXMLSourceFromOBJ(objectToTrasform), xslt);
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		}
	}
	
	/**
	 * Esegue la trasformazione in PDF tramite Apache FOP popolando i metadati richiesti dal template utilizzando la mappa di parametri in ingresso.
	 * @param hm
	 * @param xslt
	 * @return byte array del pdf generato
	 */
	public static byte[] transformMAP2PDF(final Map<String, String> hm, final byte[] xslt) {
		try {
			return transformSRC2PDF(getXMLSourceFromMAP(hm), xslt);
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		}
	}
	
	/**
	 * @param xml byte array del file xml
	 * @param xslt byte array della trasformata xslt
	 * @return byte array del pdf generato
	 */
	public static byte[] transformToPdf(final byte[] xml, final byte[] xslt) {
		final InputStream streamXML = new ByteArrayInputStream(xml);
	    final Source srcXML = new StreamSource(streamXML);
	    return transformSRC2PDF(srcXML, xslt);
	}

	private static byte[] transformSRC2PDF(final Source srcXML, final byte[] xslt) {
		try {
			final InputStream streamXSLT = new ByteArrayInputStream(xslt);
		    final Source srcXSLT = new StreamSource(streamXSLT);
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
	        final Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, out);
	        final Result res = new SAXResult(fop.getDefaultHandler());
	        final Transformer transformer = tFactory.newTransformer(srcXSLT);
	        transformer.transform(srcXML, res);
	        out.flush();
	        out.close();
	        return out.toByteArray();
		} catch (final Exception e) {
			throw new RedException(e);
		}
	}
	
	
}