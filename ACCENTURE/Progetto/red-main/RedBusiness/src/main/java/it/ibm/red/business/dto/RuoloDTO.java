package it.ibm.red.business.dto;

/**
 * The Class RuoloDTO.
 *
 * @author CPIERASC
 * 
 * 	Data Transfer Object per un ruolo.
 */
public class RuoloDTO extends AbstractDTO {

	/**
	 * Serializzable.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Descrizione.
	 */
	private String descrizione;

	/**
	 * Identificativo.
	 */
	private Long id;

	/**
	 * Costruttore.
	 * 
	 * @param inId			identificativo
	 * @param inDescrizione	descrizione
	 */
	public RuoloDTO(final Long inId, final String inDescrizione) {
		super();
		this.descrizione = inDescrizione;
		this.id = inId;
	}
	
	/**
	 * Getter identificativo.
	 * 
	 * @return	identificativo
	 */
	public final Long getId() {
		return id;
	}

	/**
	 * Getter descrizione.
	 * 
	 * @return	descrizione
	 */
	public final String getDescrizione() {
		return descrizione;
	}

}
