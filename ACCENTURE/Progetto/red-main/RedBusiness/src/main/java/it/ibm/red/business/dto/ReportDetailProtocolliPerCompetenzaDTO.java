package it.ibm.red.business.dto;

import java.util.Date;

/**
 * DTO per il detail dei report protocolli per competenza.
 */
public class ReportDetailProtocolliPerCompetenzaDTO extends AbstractDTO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Numero protocollo.
	 */
	private String numeroProtocollo;
	
	/**
	 * Numero documento.
	 */
	private String numeroDocumento;
	
	/**
	 * Anno documento.
	 */
	private String annoDocumento;

	/**
	 * Data protocollo.
	 */
	private Date dataProtocollo;

	/**
	 * Oggetto.
	 */
	private String oggetto;
	
	/**
	 * Tipologia documento.
	 */
	private String tipologiaDocumento;

	/**
	 * Ufficio assegnatario.
	 */
	private String ufficioAssegnatario;

	/**
	 * Utente assegnatario.
	 */
	private String utenteAssegnatario;
	
	/**
	 * Data chiusura.
	 */
	private Date dataChiusura;
	
	/**
	 * Tipo chiusura.
	 */
	private String tipoChiusura;
	
	/**
	 * Stato.
	 */
	private String stato;

	/**
	 * Restituisce il numero del protocollo.
	 * 
	 * @return numero protocollo
	 */
	public String getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * Imposta il numero del protocollo.
	 * 
	 * @param numeroProtocollo
	 */
	public void setNumeroProtocollo(final String numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}
	
	/**
	 * Restituisce il numero del documento.
	 * 
	 * @return numero documento
	 */
	public String getNumeroDocumento() {
		return numeroDocumento;
	}
	
	/**
	 * Imposta il numero del documento.
	 * 
	 * @param numeroDocumento
	 */
	public void setNumeroDocumento(final String numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * Restituisce l'anno del documento.
	 * 
	 * @return anno documento
	 */
	public String getAnnoDocumento() {
		return annoDocumento;
	}

	/**
	 * Imposta l'anno del documento.
	 * 
	 * @param annoDocumento
	 */
	public void setAnnoDocumento(final String annoDocumento) {
		this.annoDocumento = annoDocumento;
	}

	/**
	 * Restituisce la data di protocollo.
	 * 
	 * @return data protocollo
	 */
	public Date getDataProtocollo() {
		return dataProtocollo;
	}

	/**
	 * Imposta la data di protocollo.
	 * 
	 * @param dataProtocollo
	 */
	public void setDataProtocollo(final Date dataProtocollo) {
		this.dataProtocollo = dataProtocollo;
	}

	/**
	 * Restituisce l'oggetto.
	 * 
	 * @return oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * Imposta l'oggetto.
	 * 
	 * @param oggetto
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}

	/**
	 * Restituisce la tipologia del documento.
	 * 
	 * @return tipologia documento
	 */
	public String getTipologiaDocumento() {
		return tipologiaDocumento;
	}

	/**
	 * Imposta la tipologia del documento.
	 * 
	 * @param tipologiaDocumento
	 */
	public void setTipologiaDocumento(final String tipologiaDocumento) {
		this.tipologiaDocumento = tipologiaDocumento;
	}

	/**
	 * Restituisce l'ufficio assegnatario.
	 * 
	 * @return ufficio assegnatario
	 */
	public String getUfficioAssegnatario() {
		return ufficioAssegnatario;
	}

	/**
	 * Imposta l'ufficio assegnatario.
	 * 
	 * @param ufficioAssegnatario
	 */
	public void setUfficioAssegnatario(final String ufficioAssegnatario) {
		this.ufficioAssegnatario = ufficioAssegnatario;
	}

	/**
	 * Restituisce l'utente assegnatario.
	 * 
	 * @return utente assegnatario
	 */
	public String getUtenteAssegnatario() {
		return utenteAssegnatario;
	}

	/**
	 * Imposta l'utente assegnatario.
	 * 
	 * @param utenteAssegnatario
	 */
	public void setUtenteAssegnatario(final String utenteAssegnatario) {
		this.utenteAssegnatario = utenteAssegnatario;
	}

	/**
	 * Restituisce la data di chiusura.
	 * 
	 * @return data chiusura
	 */
	public Date getDataChiusura() {
		return dataChiusura;
	}

	/**
	 * Imposta la data di chiusura.
	 * 
	 * @param dataChiusura
	 */
	public void setDataChiusura(Date dataChiusura) {
		this.dataChiusura = dataChiusura;
	}

	/**
	 * Restituisce il tipo di chiusura.
	 * 
	 * @return tipo chiusura
	 */
	public String getTipoChiusura() {
		return tipoChiusura;
	}

	/**
	 * Imposta il tipo di chiusura.
	 * 
	 * @param tipoChiusura
	 */
	public void setTipoChiusura(String tipoChiusura) {
		this.tipoChiusura = tipoChiusura;
	}

	/**
	 * Restituisce lo stato.
	 * 
	 * @return stato
	 */
	public String getStato() {
		return stato;
	}

	/**
	 * Imposta lo stato.
	 * 
	 * @param stato
	 */
	public void setStato(String stato) {
		this.stato = stato;
	}

}