package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.CountPer;
import it.ibm.red.business.dto.DocumentoScadenzatoDTO;
import it.ibm.red.business.dto.PEDocumentoScadenzatoDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PeriodoScadenzaEnum;
import it.ibm.red.business.enums.ScopeScadenziarioStrutturaEnum;

/**
 * Facade del servizio di gestione struttura scadenziario.
 */
public interface IScadenziaroStrutturaFacadeSRV extends Serializable {
	
	/**
	 * 
	 * Recupera tutti gli uffici dell'ispettorato e esegue la count per i suoi uffici.
	 * 
	 * la funzionalità si chiama su nsd:
	 * 
	 * 					loadCruscottoIspettorato 
	 * 
	 * @param utente
	 * 	utente che richiede l'operazione
	 * 	
	 * @return
	 * 	Una lista degli uffici presenti nell'ispettorato con il rispettivo numero di documenti scadenzati presenti
	 */
	List<CountPer<UfficioDTO>> retrieveCountDocumentoInScadenzaPerUfficiDaIspettorato(UtenteDTO utente);
	
	/**
	 * 
	 * 
	 * Qualora l'utente appartenga a un nodo di segreteria l'utente deve visualizzare i figli di una segreteria e le segreterie.
	 * Altrimenti deve poter vedere tutti gli uffici sottostanti a lui
	 * 
	 * la funzionalità si chiama su nsd loadCruscottoIspettorato / loadCruscottoRagioniere eccetera
	 * 
	 * @param utente
	 * 	utente che richiede l'operazione
	 * 	
	 * @return
	 * 	Una lista di una coppia ufficio utente
	 */
	Map<CountPer<UfficioDTO>, List<CountPer<UfficioDTO>>> retrieveCountDocumentoInScadenzaPerUfficiDaSegreteria(UtenteDTO utente);
	
	/**
	 * Trova il numero dei documenti con scadenza suddiviso per il periodo della scadenza.
	 *  
	 * @param idUfficio
	 * 	Filtra i documenti in scadenza per ufficio
	 * @param utente
	 * 	Utente richiedente dell'operazione
	 * @return
	 */
	Map<CountPer<PeriodoScadenzaEnum>, List<PEDocumentoScadenzatoDTO>> retrieveDocumentoInScadenzaPerPeriodoScadenza(Long idUfficio, UtenteDTO utente);
	
	/**
	 * Discerne il livello di dettaglio in cui l'utente ha diritto di visualizzare e recuperare le liste di CountPer
	 * 
	 * E.G. un utente di segreteria può visualizzare tutti i suoi sotto nodi di tipo ispettorato
	 * un utente di tipo ispettorato può visualizzare il count di tutte le scadenze dei suoi uffici
	 * 
	 * Molto analogo a un permesso di fatto
	 * 
	 * @param utente
	 * @return
	 */
	ScopeScadenziarioStrutturaEnum getScopeScadenziarioStruttura(UtenteDTO utente);
	
	/**
	 * Ritorna un insieme di Oggetti IdDocumentDTO associati al wobNumber.
	 * 
	 * @param peList
	 * 	Lista di idDocument(AKA documentTitle) a cui associare l'idDocumento
	 * @param utente
	 * 	Utente richiedente l'operazione
	 * @return
	 * 	Una mappa wobNUmber idDocumentoDTO che contiene un insieme di identificativi applicativi 
	 */
	List<DocumentoScadenzatoDTO> retrieveIdDocumentoByPeIdDocument(List<PEDocumentoScadenzatoDTO> peList, UtenteDTO utente);

	/**
	 * @param idUfficio
	 * @param utente
	 * @return
	 */
	Map<PeriodoScadenzaEnum, List<PEDocumentoScadenzatoDTO>> retrieveDocumentiPerPeriodoScadenza(Long idUfficio, UtenteDTO utente);

	 
}

