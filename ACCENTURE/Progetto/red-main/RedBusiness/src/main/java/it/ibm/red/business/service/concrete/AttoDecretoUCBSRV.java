package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.TipologiaInvio;
import it.ibm.red.business.dao.IAttoDecretoUCBDAO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AttributiEstesiAttoDecretoDTO;
import it.ibm.red.business.dto.DestinatarioCodaMailDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.MessaggioEmailDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAttoDecretoUCBSRV;
import it.ibm.red.business.service.ICodaMailSRV;
import it.ibm.red.business.service.concrete.CodaMailSRV.ResponseSendMail;
import it.ibm.red.business.utils.StringUtils;

/**
 * Service Atto Decreto UCB.
 */
@Service
public class AttoDecretoUCBSRV extends AbstractService implements IAttoDecretoUCBSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 2146877867512850990L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AttoDecretoUCBSRV.class.getName());

	/**
	 * DAO atto decreto UCB.
	 */
	@Autowired
	private IAttoDecretoUCBDAO attoDecretoUCBDAO;

	/**
	 * Servizio coda mail.
	 */
	@Autowired
	private ICodaMailSRV codaMailSRV;

	/**
	 * @see it.ibm.red.business.service.facade.IAttoDecretoUCBFacadeSRV#getAttributiEstesi(java.lang.Integer).
	 */
	@Override
	public AttributiEstesiAttoDecretoDTO getAttributiEstesi(final Integer idAOO) {

		AttributiEstesiAttoDecretoDTO out = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			out = attoDecretoUCBDAO.getAttributiEstesi(connection, idAOO);

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei metadati estesi aoo:" + idAOO, e);
			throw new RedException("Errore durante il recupero dei metadati estesi aoo:" + idAOO, e);
		} finally {
			closeConnection(connection);
		}
		return out;
	}

	/**
	 * Dato un insieme di nomi di file ritorna l'insieme non valido di file.
	 */
	@Override
	public Collection<String> validateFilenames(final Collection<String> filenames) {
		final Collection<String> out = new ArrayList<>();
		Connection connection = null;

		try {
			if (!CollectionUtils.isEmpty(filenames)) {
				connection = setupConnection(getDataSource().getConnection(), false);

				final Map<String, List<String>> classifiedFnames = new HashMap<>();

				for (final String fname : filenames) {
					final String ext = StringUtils.getExtension(fname);
					List<String> extFnames = classifiedFnames.get(ext);
					if (extFnames == null) {
						extFnames = new ArrayList<>();
					}
					extFnames.add(fname);
					classifiedFnames.put(ext, extFnames);
				}

				final Collection<String> allowedExts = attoDecretoUCBDAO.getExtension(connection);
				for (final Entry<String, List<String>> extIn : classifiedFnames.entrySet()) {
					boolean bFound = false;
					for (final String extDB : allowedExts) {
						if (extDB.equalsIgnoreCase(extIn.getKey())) {
							bFound = true;
							break;
						}
					}
					if (!bFound) {
						out.addAll(classifiedFnames.get(extIn.getKey()));
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la validazione dei nomi dei files.", e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}

		return out;
	}

	private static String getTestoMail(final String nomeMessaggioAllegato, final String mittenteMessaggio, final Integer numeroProtocollo, final Date dataProtocollo,
			final Collection<String> filesNonConformi, final String aooProtocollante) {
		String testoMail = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.TESTO_MAIL_ATTO_DECRETO);
		testoMail = testoMail.replaceFirst("<<NOME_MESSAGGIO_ALLEGATO>>", nomeMessaggioAllegato);
		testoMail = testoMail.replaceFirst("<<MITTENTE_MESSAGGIO>>", mittenteMessaggio);
		testoMail = testoMail.replaceFirst("<<AOO_PROTOCOLLANTE>>", aooProtocollante);
		testoMail = testoMail.replaceFirst("<<NUMERO_PROTOCOLLO_MESSAGGIO>>", numeroProtocollo + "");
		testoMail = testoMail.replaceFirst("<<DATA_PROTOCOLLO_MESSAGGIO>>", new SimpleDateFormat("dd/MM/yyyy").format(dataProtocollo));
		testoMail = testoMail.replaceFirst("<<ELENCO_FILE_NON_CONFORMI>>", org.apache.commons.lang3.StringUtils.join(filesNonConformi));
		return testoMail;
	}

	/**
	 * 
	 * Scenario: stiamo protocollando una mail generando un tipo documento ATTO
	 * DECRETO, ma ci rendiamo conto che gli allegati forniti non sono conformi agli
	 * accordi definiti in precedenza. Dobbiamo quindi rispondere al mittente di
	 * quella mail difforme con una nuova mail in cui evidenziamo la difformità,
	 * fornendo in allegato la mail difforme.
	 * 
	 * @param utente            utente
	 * @param contentMessaggio  content che rappresenta la mail difforme da
	 *                          notificare
	 * @param destinatario      destinatario a cui inviare il messaggio per
	 *                          notificare la mail difforme
	 * @param casellaPostale    casella postale dalla quale verrà inviato il
	 *                          messaggio per notificare la mail difforme
	 * @param mittenteMessaggio mittente della mail con allegati difformi
	 * @param numeroProtocollo  numero protocollo mail con allegati difformi
	 * @param dataProtocollo    data protocollo mail con allegati difformi
	 * @param filesNonConformi  nomi degli allegati difformi
	 */
	@Override
	public ResponseSendMail comunicaErroreProtocollazione(final UtenteDTO utente, final byte[] contentMessaggio, final DestinatarioCodaMailDTO destinatario,
			final String casellaPostale, final String mittenteMessaggio, final Integer numeroProtocollo, final Date dataProtocollo,
			final Collection<String> filesNonConformi) {
		// Invio sincrono di una mail
		IFilenetCEHelper fcehAdmin = null;
		ResponseSendMail esitoInvio = null;

		try {

			final String nomeMessaggioAllegato = "mail.eml";
			final String inDocumentTitle = null;
			final String inDestinatariCC = null;
			final String mittente = casellaPostale;
			final String oggettoMail = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.OGGETTO_MAIL_ATTO_DECRETO);
			final String aooProtocollante = utente.getAooDesc();
			final String testoMail = getTestoMail(nomeMessaggioAllegato, mittenteMessaggio, numeroProtocollo, dataProtocollo, filesNonConformi, aooProtocollante);
			final Integer inTipologiaInvioId = TipologiaInvio.NUOVOMESSAGGIO;
			final Integer tipologiaMessaggioId = Constants.TipologiaMessaggio.TIPO_MSG_EMAIL; // PEC/PEO ma se lo calcola in automatico
			final String messageId = null;
			final String inGuidEmailSpedizione = null;
			final Boolean inIsNotifica = false; // non è una modifica
			final Boolean inHasAllegati = true; // c'è la mail in allegato
			final String inAction = "Notifica Atto Decreto";
			final String inDestinatari2 = null;
			final String inMotivazione = null;

			final MessaggioEmailDTO messaggio = new MessaggioEmailDTO(inDocumentTitle, destinatario.getIndirizzoMail(), inDestinatariCC, mittente, oggettoMail, testoMail,
					inTipologiaInvioId, tipologiaMessaggioId, messageId, inGuidEmailSpedizione, inIsNotifica, inHasAllegati, inAction, inDestinatari2, inMotivazione);

			final List<AllegatoDTO> allegatiMail = new ArrayList<>();
			final AllegatoDTO allegato = new AllegatoDTO();
			allegato.setContent(contentMessaggio);
			allegato.setNomeFile(nomeMessaggioAllegato);
			allegato.setMimeType(it.ibm.red.business.constants.Constants.ContentType.EML);
			messaggio.setAllegatiMail(allegatiMail);

			fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()), utente.getIdAoo());
			final Integer idAOOUtente = utente.getIdAoo().intValue();
			esitoInvio = codaMailSRV.sendMail(messaggio, idAOOUtente, fcehAdmin);

			// Esito invio mail di notifica a valle della validazione negativa degli
			// allegati di un ATTO DECRETO entrata.
			if (esitoInvio != null) {
				LOGGER.info("ESITO INVIO MAIL VALIDAZIONE ATTO DECRETO");

				LOGGER.info("<Evidenza esito invio>");
				LOGGER.info(esitoInvio.toString());

				LOGGER.info("<Evidenza composizione messaggio>");
				LOGGER.info("Nome Messaggio Allegato: " + nomeMessaggioAllegato);
				LOGGER.info("Document Title: " + inDocumentTitle);
				LOGGER.info("Destinatari TO: " + destinatario.getIndirizzoMail());
				LOGGER.info("Destinatari CC: " + inDestinatariCC);
				LOGGER.info("MIttente: " + mittente);
				LOGGER.info("Oggetto: " + oggettoMail);
				LOGGER.info("Aoo protocollante: " + aooProtocollante);
				LOGGER.info("Testo mail: " + testoMail);
				LOGGER.info("Tipologia invio ID: " + inTipologiaInvioId);
				LOGGER.info("Message ID: " + messageId);
				LOGGER.info("Guid mail spedizione: " + inGuidEmailSpedizione);
				LOGGER.info("isNotifica: " + inIsNotifica);
				LOGGER.info("hasAllegati: " + inHasAllegati);
				LOGGER.info("Action: " + inAction);
				LOGGER.info("Destinatari2: " + inDestinatari2);
				LOGGER.info("Motivazione: " + inMotivazione);

			}

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il tentativo di segnalare l'esito negativo della validazione allegati.", e);
			throw new RedException("Si è verificato un errore durante il tentativo di segnalare l'esito negativo della validazione allegati.", e);
		} finally {
			popSubject(fcehAdmin);
		}

		return esitoInvio;
	}

}
