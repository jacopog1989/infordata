package it.ibm.red.business.template;

import org.springframework.util.CollectionUtils;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.logger.REDLogger;

/**
 * Classe che definisce il template di un documento in uscita.
 */
public class TemplateDocumentoUscita extends TemplateDocumento {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -6825428175191502004L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TemplateDocumentoUscita.class.getName());
	
	/**
	 * Label.
	 */
	private static final String LABEL_TD = "</label></td>";

	/**
	 * Label.
	 */
	private static final String TR = "</tr>";

	/**
	 * @see it.ibm.red.business.template.TemplateDocumento#getDettaglioDocumentoTable(it.ibm.red.business.dto.SalvaDocumentoRedDTO).
	 */
	@Override
	public StringBuilder getDettaglioDocumentoTable(final SalvaDocumentoRedDTO documento) {
		final StringBuilder dettaglioTable = new StringBuilder();
		try {
			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Tipo iter: </label></td>");
			dettaglioTable.append("<td colspan=\"5\"><label id=\"tipoIter\" name=\"tipoIter\" class=\"scripta\">" 
					+ (documento.getIterApprovativo() != null 
						? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(documento.getIterApprovativo().getDescrizione()) : Constants.EMPTY_STRING) + LABEL_TD);
			dettaglioTable.append(TR);
			// Sezione: Destinatari
			dettaglioTable.append((getDestinatari(documento)).toString());
			
			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Protocollazione: </label></td>");
			dettaglioTable.append("<td><label id=\"protocollazione\" name=\"protocollazione\" class=\"scripta\">" 
					+ (documento.getMomentoProtocollazioneEnum() != null 
						? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(documento.getMomentoProtocollazioneEnum().getDescrizione()) : Constants.EMPTY_STRING) 
					+ LABEL_TD);
			dettaglioTable.append(TR);
			
			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Riservato: </label></td>");
			dettaglioTable.append("<td><label id=\"riservato\" name=\"riservato\" class=\"scripta\">" 
					+ (Boolean.TRUE.equals(documento.getIsRiservato()) ? BooleanFlagEnum.SI.getDescription() : BooleanFlagEnum.NO.getDescription()) + LABEL_TD);
			dettaglioTable.append(TR);
			
			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Corte dei conti: </label></td>");
			dettaglioTable.append("<td><label id=\"corteconti\" name=\"corteconti\" class=\"scripta\">" 
					+ (Boolean.TRUE.equals(documento.getFlagCorteConti()) ? BooleanFlagEnum.SI.getDescription() : BooleanFlagEnum.NO.getDescription()) + LABEL_TD);
			dettaglioTable.append(TR);
			
			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Tipo assegnazione: </label></td>");
			dettaglioTable.append("<td><label id=\"barcode\" name=\"tipoAssegnazione\" class=\"scripta\">" 
					+ (!CollectionUtils.isEmpty(documento.getAssegnazioni()) 
							? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(getDescrizioneTipoAssegnazione(documento)) : Constants.EMPTY_STRING) + LABEL_TD);
			dettaglioTable.append(TR);
			
			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Assegnatario: </label></td>");
			dettaglioTable.append("<td><label id=\"assegnatario\" name=\"assegnatario\" class=\"scripta\">" 
					+ (!CollectionUtils.isEmpty(documento.getAssegnazioni()) 
							? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(getAssegnatarioCompetenza(documento)) : Constants.EMPTY_STRING) + LABEL_TD);
			dettaglioTable.append(TR);
			
			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Tipo Visto: </label></td>");
			dettaglioTable.append("<td><label id=\"tipoVisto\" name=\"tipoVisto\" class=\"scripta\">" 
					+ Constants.EMPTY_STRING + LABEL_TD);
			dettaglioTable.append(TR);
			// Sezione: In risposta al protocollo (anno / numero)
			dettaglioTable.append((getProtocolliAllacciati(documento)).toString());
			
			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td colspan=\"6\"><hr style=\"width: 100%\"></hr></td>");
			dettaglioTable.append(TR);
		} catch (final Exception e) {
			LOGGER.error("Errore nella creazione della sezione dettaglio documento per la creazione dell'html.", e);
		}
		return dettaglioTable;
	}

	/**
	 * Restituisce la lista destinatari in HTML.
	 * @param documento
	 * @return sb
	 */
	private StringBuilder getDestinatari(final SalvaDocumentoRedDTO documento) {
		final StringBuilder destinatari = new StringBuilder();

		if (!CollectionUtils.isEmpty(documento.getDestinatari())) {
			int i = 1;
			for (final DestinatarioRedDTO destinatario : documento.getDestinatari()) {
				destinatari.append("<tr>");
				destinatari.append("<td><label class=\"scriptaLabelBold\">Destinatario " + i + LABEL_TD);
				destinatari.append("<td colspan=\"5\"><label id=\"" + ("destinatario" + i) + "\" name=\"" + ("destinatario" + i) + "\" class=\"scripta\">" 
					+ (destinatario.getContatto() != null && destinatario.getContatto().getAliasContatto() != null 
						? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(destinatario.getContatto().getAliasContatto()) : Constants.EMPTY_STRING) + LABEL_TD);
				destinatari.append(TR);
				i++;
			}
		}
		return destinatari;
	}

	/**
	 * Restituisce la lista dei protocolli allacciati in HTML.
	 * @param documento
	 * @return sb
	 */
	private StringBuilder getProtocolliAllacciati(final SalvaDocumentoRedDTO documento) {
		final StringBuilder protocolliAllacciati = new StringBuilder();
		
		if (!CollectionUtils.isEmpty(documento.getAllacci())) {
			int i = 1;
			for (final RispostaAllaccioDTO rispostaAllaccio : documento.getAllacci()) {
				protocolliAllacciati.append("<tr>");
				protocolliAllacciati.append("<td><label class=\"scriptaLabelBold\">In risposta al protocollo (anno / numero):</label></td>");
				protocolliAllacciati.append("<td colspan=\"5\"><label id=\"rispostaProtocollo" + i + "\" name=\"rispostaProtocollo" + i + "\" class=\"scripta\">"
					+ (it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(rispostaAllaccio.getDocument().getDescTipologiaDocumento()) 
							+ " - " 
							+ rispostaAllaccio.getDocument().getAnnoProtocollo() + " / " 
					+ rispostaAllaccio.getDocument().getNumeroProtocollo()) + LABEL_TD);
				protocolliAllacciati.append(TR);
				i++;
			}
		}
		return protocolliAllacciati;
	}
}