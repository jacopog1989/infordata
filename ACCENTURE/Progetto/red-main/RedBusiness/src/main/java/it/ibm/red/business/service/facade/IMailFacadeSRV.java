package it.ibm.red.business.service.facade;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.filenet.api.collection.PageIterator;
import com.filenet.api.core.Document;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnatarioInteropDTO;
import it.ibm.red.business.dto.DestinatarioCodaMailDTO;
import it.ibm.red.business.dto.DetailEmailDTO;
import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.dto.ErroreValidazioneDTO;
import it.ibm.red.business.dto.EsitoOperazioneMailDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.NotaDTO;
import it.ibm.red.business.dto.SegnaturaMessaggioInteropDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.StatoMailEnum;
import it.ibm.red.business.enums.StatoMailGUIEnum;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;

/**
 * Facade del servizio mail.
 */
public interface IMailFacadeSRV extends Serializable {

	/**
	 * Archivia le mail nello stato in input.
	 * 
	 * @param fceh
	 * @param eliminata
	 */
	void archiviaMail(IFilenetCEHelper fceh, StatoMailEnum eliminata);

	/**
	 * Verifica se l'email, recuperata tramite guid, può essere protocollata.
	 * 
	 * @param filenetCredential Credenziali utente per l'accesso a fileNet
	 * @param guid              identificativo dell'email
	 * @param idUtente          id dell'utente protocollatore
	 * @return il permesso a protocollare l'email
	 */
	boolean isEmailProtocollabile(FilenetCredentialsDTO filenetCredential, String guid, UtenteDTO utente);

	/**
	 * Questo metodo verifica se una email non protocollabile, può essere
	 * protocollata dopo aver notificato il suo stato all'utente.
	 * 
	 * @param filenetCredential Credenziali utente per l'accesso a fileNet
	 * @param guid              identificativo dell'email
	 * @param idUtente          id dell'utente protocollatore
	 * @return il permesso a richiedere il permesso alla protocollazione l'email
	 */
	boolean hasToAskIfEmailIsProtocollabile(FilenetCredentialsDTO filenetCredential, String guid, UtenteDTO utente);

	/**
	 * Metodo per la restituzione del dettaglio di una mail.
	 * 
	 * @param fcDTO credenziali filenet
	 * @param guid  guid mail
	 * @return dettaglio mail
	 */
	DetailEmailDTO getDetailFromGuid(FilenetCredentialsDTO fcDTO, String guid, Long idAoo);

	/**
	 * metodo per il set della note email delle email.
	 * 
	 * @param fcDto credenziali filenet
	 * @param guid  guid mail
	 * @param nota  nota
	 */
	void setNotaMail(FilenetCredentialsDTO fcDto, String guid, NotaDTO nota, AssegnatarioInteropDTO preassegnatario, Long idAoo);

	/**
	 * Recupero allegato da guid.
	 * 
	 * @param fcDTO credenziali filenet
	 * @param guid  guid allegato
	 * @return struttura contenente l'allegato
	 */
	FileDTO getFileAllegatoFromGuid(FilenetCredentialsDTO fcDTO, String guid, Long idAoo);

	/**
	 * Cancellazione nota mail.
	 * 
	 * @param fcDto credenziali filenet
	 * @param guid  guid mail
	 */
	void cancellaNotaMail(FilenetCredentialsDTO fcDto, String guid, Long idAoo);

	/**
	 * Ripristino mail.
	 * 
	 * @param fcDto credenziali filenet
	 * @param guid  guid mail
	 */
	EmailDTO ripristina(FilenetCredentialsDTO fcDTO, DetailEmailDTO mail, Long idAoo);

	/**
	 * Cancellazione mail.
	 * 
	 * @param fcDto    credenziali filenet
	 * @param guid     guid mail
	 * @param motivo   motivo cancellazione
	 * @param idUtente identificativo utente
	 */
	void cancella(FilenetCredentialsDTO fcDto, String guid, String motivo, UtenteDTO idUtente);

	/**
	 * Metodo per cancellare una mail.
	 * 
	 * @param fcDto    credenziali filenet
	 * @param guids    lista guid mail
	 * @param motivo   motivo dell'eliminazione
	 * @param idUtente identificativo utente
	 * @return collezione esiti cancellazione
	 */
	Collection<EsitoOperazioneMailDTO> cancella(FilenetCredentialsDTO fcDto, Collection<String> guids, String motivo, UtenteDTO utente);

	/**
	 * Recupero delle mail.
	 * 
	 * @param fcDTO        credenziali filenet
	 * @param cpAttiva     casella attiva
	 * @param selectedStat stato selezionato
	 * @param idUtente     identificativo utente
	 * @param string
	 * @return collezione mail
	 */
	Collection<EmailDTO> getMails(FilenetCredentialsDTO fcDTO, String cpAttiva, StatoMailGUIEnum selectedStat, Integer idUtente, String orderByClause, Long idAoo);

	/**
	 * Recupero delle mail.
	 * 
	 * @param fcDTO
	 * @param cpAttiva
	 * @param selectedStat
	 * @param idUtente
	 * @param idAoo
	 * @return collezione mail
	 */
	Collection<EmailDTO> getMails(FilenetCredentialsDTO fcDTO, String cpAttiva, StatoMailGUIEnum selectedStat, Integer idUtente, Long idAoo);

	/**
	 * Recupero delle mail.
	 * 
	 * @param fceh     helper filenet
	 * @param cpAttiva casella attiva
	 * @param stato    lista degli stati delle mail da ricercare, recuperate in OR
	 * @param idUtente identificativo utente
	 * @return collezione mail
	 */
	Collection<EmailDTO> getMails(IFilenetCEHelper fceh, String cpAttiva, List<StatoMailEnum> stati, Integer idUtente);

	/**
	 * @param fcDTO
	 * @param cpAttiva
	 * @param idUtente
	 * @param dataDa
	 * @param dataA
	 * @param searchString
	 * @param oggettoRicerca
	 * @param mittenteRicerca
	 * @param isFullTextRicerca
	 * @param statiMailSelezionati    lista di StatoEmailEnum corrispondenti alle
	 *                                checkbox in maschera di ricerca mail
	 * @param numeroProtocolloRicerca
	 * @return mail filtrate per parametri ricerca
	 */
	Collection<EmailDTO> searchMails(FilenetCredentialsDTO fcDTO, String cpAttiva, Integer idUtente, Integer anno, Date inDataDa, Date inDataA, String searchString,
			String oggettoRicerca, String mittenteRicerca, boolean isFullTextRicerca, String numeroProtocolloRicerca, List<StatoMailEnum> statiMailSelezionati, Long idAoo);

	/**
	 * @param fcDto
	 * @param guid
	 * @param casellaPostale
	 * @param mittente
	 * @param destinatariToList
	 * @param destinatariCcList
	 * @param oggettoMail
	 * @param testoMail
	 * @param azioneMail
	 * @param motivazioneMail
	 * @param tipologiaInvioId
	 * @param tipologiaMessaggioId
	 * @param isNotifica
	 * @param hasAllegati
	 * @param modalitaSpedizione
	 * @param tipoEvento
	 * @param utente
	 */
	void inoltra(FilenetCredentialsDTO fcDto, String guid, String casellaPostale, String mittente, List<DestinatarioCodaMailDTO> destinatariToList,
			List<DestinatarioCodaMailDTO> destinatariCcList, String oggettoMail, String testoMail, String azioneMail, String motivazioneMail, Integer tipologiaInvioId,
			Integer tipologiaMessaggioId, Boolean isNotifica, Boolean hasAllegati, Integer modalitaSpedizione, Integer tipoEvento, UtenteDTO utente);

	/**
	 * @param fcDto
	 * @param guid
	 * @param casellaPostale
	 * @param mittente
	 * @param destinatario
	 * @param destinatarioToForFilenet
	 * @param oggettoMail
	 * @param testoMail
	 * @param azioneMail
	 * @param motivazioneMail
	 * @param tipologiaInvioId
	 * @param tipologiaMessaggioId
	 * @param isNotifica
	 * @param hasAllegati
	 * @param modalitaSpedizione
	 * @param tipoEvento
	 * @param utente
	 */
	void rifiuta(FilenetCredentialsDTO fcDto, String guid, String casellaPostale, String mittente, String destinatario, String destinatarioToForFilenet, String oggettoMail,
			String testoMail, String azioneMail, String motivazioneMail, Integer tipologiaInvioId, Integer tipologiaMessaggioId, Boolean isNotifica, Boolean hasAllegati,
			Integer modalitaSpedizione, Integer tipoEvento, UtenteDTO utente);

	/**
	 * Aggiorna l'utente protocollatore.
	 * 
	 * @param mailFilenet
	 * @param idUtente
	 * @param fceh
	 */
	void aggiornaUtenteProtocollatoreMail(Document mailFilenet, Long idUtente, IFilenetCEHelper fceh);

	/**
	 * Cambia lo stato delle mails da eliminare a "inbox" massivo.
	 * 
	 * @param fcDto
	 * @param mails
	 * @return esito dell'operazione sulle mails
	 */
	Collection<EsitoOperazioneMailDTO> ripristina(FilenetCredentialsDTO fcDto, Collection<DetailEmailDTO> mails);

	/**
	 * Ottiene la mail originale relativa al documento.
	 * 
	 * @param documentTitle
	 * @param nomeEmail
	 * @param idAoo
	 * @param fceh
	 * @return mail originale
	 */
	DocumentoAllegabileDTO getMailOriginale(String documentTitle, String nomeEmail, Long idAoo, IFilenetCEHelper fceh);

	/**
	 * Ottiene il nome della mail originale relativa al documento.
	 * 
	 * @param document
	 * @return nome della mail originale
	 */
	String getNomeMailOriginale(Document document);

	/**
	 * Recupero degli errori/warning interoperabilità relativi ad una mail.
	 * 
	 * @param fcDTO    helper filenet
	 * @param guid     guid
	 * @param stato    lista degli stati delle mail da ricercare, recuperate in OR
	 * @param idUtente identificativo utente
	 * @return List ErroreValidazioneDTO
	 */
	List<ErroreValidazioneDTO> getErroriInteroperabilita(FilenetCredentialsDTO fcDTO, String guid);

	/**
	 * Sposta la mail.
	 * 
	 * @param fceh
	 */
	void spostaMail(IFilenetCEHelper fceh);

	/**
	 * Crea il paginator.
	 * 
	 * @param fceh
	 * @param cpAttiva
	 * @param selectedStatList
	 * @param idUtente
	 * @param pageSize
	 * @param orderByClause
	 * @return paginator
	 */
	PageIterator createPaginator(IFilenetCEHelper fceh, String cpAttiva, List<StatoMailEnum> selectedStatList, Integer idUtente, Integer pageSize, String orderByClause);

	/**
	 * Crea il paginator.
	 * 
	 * @param fcDTO
	 * @param cpAttiva
	 * @param selectedStatGuiList
	 * @param idUtente
	 * @param pageSize
	 * @param orderByClause
	 * @param idAoo
	 * @return paginator
	 */
	PageIterator createPaginator(FilenetCredentialsDTO fcDTO, String cpAttiva, List<StatoMailGUIEnum> selectedStatGuiList, int idUtente, Integer pageSize,
			String orderByClause, Long idAoo);

	/**
	 * Raffina l'iterazione.
	 * 
	 * @param utente
	 * @param pageIteratorMail
	 * @return emails
	 */
	Collection<EmailDTO> refineIterator(UtenteDTO utente, PageIterator pageIteratorMail);

	/**
	 * Ordina per data di ricezione.
	 * 
	 * @return query
	 */
	String orderByDataRicezioneClause();

	/**
	 * Crea il file velina per la mail.
	 * 
	 * @param email
	 * @return file
	 * @throws IOException
	 */
	byte[] creaFileVelinaEmail(DetailEmailDTO email) throws IOException;

	/**
	 * Ottiene il documento principale e gli allegati della mail interoperabile.
	 * 
	 * @param guid
	 * @param fcDTO
	 * @param idAoo
	 * @return lista di allegati
	 */
	List<AllegatoDTO> getDocPrincEAllegatiMailInterop(String guid, FilenetCredentialsDTO fcDTO, Long idAoo);

	/**
	 * Ottiene la segnatura da guid.
	 * 
	 * @param fcDTO
	 * @param guid
	 * @param idAoo
	 * @return segnatura del messaggio
	 */
	SegnaturaMessaggioInteropDTO getSegnaturaFromGUID(FilenetCredentialsDTO fcDTO, String guid, Long idAoo);

	/**
	 * Aggiorna l'utente protocollatore della mail.
	 * 
	 * @param guidMailFilenet
	 * @param idUtente
	 * @param fcDTO
	 * @param idAoo
	 */
	void aggiornaUtenteProtocollatoreMail(String guidMailFilenet, Long idUtente, FilenetCredentialsDTO fcDTO, Long idAoo);

	/**
	 * Ripristina il documento.
	 * 
	 * @param fcDTO
	 * @param mail
	 * @param idAoo
	 */
	void ripristina(FilenetCredentialsDTO fcDTO, EmailDTO mail, Long idAoo);

	/**
	 * Ottiene la mail aggiornata.
	 * 
	 * @param documentTitle
	 * @param fcDTO
	 * @param idAoo
	 * @return mail
	 */
	EmailDTO getMailAggiornata(String documentTitle, FilenetCredentialsDTO fcDTO, Long idAoo);

}