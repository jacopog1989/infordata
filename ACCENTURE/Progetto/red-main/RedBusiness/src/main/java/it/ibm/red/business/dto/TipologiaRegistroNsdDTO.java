package it.ibm.red.business.dto;

import java.io.Serializable;

/**
 * @author VINGENITO
 *
 */
public class TipologiaRegistroNsdDTO implements Serializable {

	private static final long serialVersionUID = -7415298857478108208L;

	/**
	 * Valore.
	 */
	private String valore;

	/**
	 * Label.
	 */
	private String label;

	/**
	 * Ordinamento.
	 */
	private Integer ordinamento;

	/**
	 * Costruttore.
	 * 
	 * @param inValore
	 * @param inLabel
	 * @param inOrdinamento
	 */
	public TipologiaRegistroNsdDTO(final String inValore, final String inLabel, final Integer inOrdinamento) {
		valore = inValore;
		label = inLabel;
		ordinamento = inOrdinamento;
	}

	/**
	 * Restituisce il valore.
	 * 
	 * @return valore
	 */
	public String getValore() {
		return valore;
	}

	/**
	 * Imposta il valore.
	 * 
	 * @param valore
	 */
	public void setValore(final String valore) {
		this.valore = valore;
	}

	/**
	 * Restituisce la label.
	 * 
	 * @return label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * Imposta la label.
	 * 
	 * @param label
	 */
	public void setLabel(final String label) {
		this.label = label;
	}

	/**
	 * Restituisce l'ordinamento.
	 * 
	 * @return ordinamento
	 */
	public Integer getOrdinamento() {
		return ordinamento;
	}

	/**
	 * Imposta l'ordinamento.
	 * 
	 * @param ordinamento
	 */
	public void setOrdinamento(final Integer ordinamento) {
		this.ordinamento = ordinamento;
	}
}
