package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import filenet.vw.api.VWRosterQuery;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants.StatoRichiesta;
import it.ibm.red.business.constants.Constants.TipologiaInvio;
import it.ibm.red.business.dao.IAllaccioDAO;
import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.MasterDocumentoDTO;
import it.ibm.red.business.dto.MessaggioEmailDTO;
import it.ibm.red.business.dto.ModificaIterCEDTO;
import it.ibm.red.business.dto.ModificaIterPEDTO;
import it.ibm.red.business.dto.StatoRichiestaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.LibroFirmaPerFirmaResponseEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoSpedizioneEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.enums.VistoErrorEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.TrasformPE;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.provider.StatoRichiestaProvider;
import it.ibm.red.business.service.ICasellePostaliSRV;
import it.ibm.red.business.service.ICodaMailSRV;
import it.ibm.red.business.service.ICodeDocumentiSRV;
import it.ibm.red.business.service.IContributoSRV;
import it.ibm.red.business.service.IEventoLogSRV;
import it.ibm.red.business.service.IModificaProcedimentoSRV;
import it.ibm.red.business.service.INotificaPostRifiutoConFirmaSRV;
import it.ibm.red.business.service.IOperationDocumentSRV;
import it.ibm.red.business.service.ISecuritySRV;
import it.ibm.red.business.service.IVistiSRV;
import it.ibm.red.business.service.concrete.CodaMailSRV.ResponseSendMail;
import it.ibm.red.business.service.facade.IOperationWorkFlowFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.DestinatariRedUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * Metodi delle operazioni di sigla/vista/rifiuta.
 * @author CPAOLUZI
 */
@Service
public class OperationDocumentSRV extends AbstractService implements IOperationDocumentSRV {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3326037794069230544L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(OperationDocumentSRV.class.getName());

	/**
	 * Oggetto per il recupero/update delle security FN.
	 */
	@Autowired
	private ISecuritySRV securitySRV;

	/**
	 * Service.
	 */
	@Autowired
	private IContributoSRV contributoSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IVistiSRV vistoSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IEventoLogSRV eventoLogSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ICodaMailSRV codaMailSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ICasellePostaliSRV casellePostaliSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ICodeDocumentiSRV codeApplicativeSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IModificaProcedimentoSRV modIterSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IOperationWorkFlowFacadeSRV owfSRV;

	/**
	 * Service.
	 */
	@Autowired
	private INotificaPostRifiutoConFirmaSRV notificaSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IEventoLogSRV logSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private IAllaccioDAO allaccioDAO;

	/**
	 * Vista.
	 *
	 * @param fcDTO the fc DTO
	 * @param wobNumbers the wob numbers
	 * @param idUtente the id utente
	 * @param idUfficio the id ufficio
	 * @param motivoVista the motivo vista
	 * @return the collection
	 */
	@Override
	public final Collection<EsitoOperazioneDTO> vista(final FilenetCredentialsDTO fcDTO, final Collection<String> wobNumbers, final Long idUtente, final Long idUfficio,
			final String motivoVista) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();
		for (final String wobNumber:wobNumbers) {
			esiti.add(vista(fcDTO, wobNumber, idUtente, idUfficio, motivoVista));
		}
		return esiti;
	}

	/**
	 * Vista.
	 *
	 * @param fcDTO the fc DTO
	 * @param wobNumber the wob number
	 * @param idUtente the id utente
	 * @param idUfficio the id ufficio
	 * @param motivoVista the motivo vista
	 * @return the esito operazione DTO
	 */
	@Override
	public final EsitoOperazioneDTO vista(final FilenetCredentialsDTO fcDTO, final String wobNumber, final Long idUtente, final Long idUfficio, final String motivoVista) {
		final EsitoOperazioneDTO esito =  new EsitoOperazioneDTO(wobNumber);
		try {
			final FilenetPEHelper fpeh = new FilenetPEHelper(fcDTO);
			final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);
			final Integer idDocumento = (Integer) TrasformerPE.getMetadato(wob, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));

			final HashMap<String, Object> metadati = new HashMap<>();
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_TIPO_APPROVAZIONE_FN_METAKEY), 1);
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NOTE_VISTO_FN_METAKEY), motivoVista);
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), idUtente.intValue());
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), motivoVista);
			
			
			final String[] responses = fpeh.getResponses(wob);
			final String response = LibroFirmaPerFirmaResponseEnum.VISTA.firstIn(responses);
			fpeh.nextStep(wob, metadati, response);
			
			//### POST VISTO ########################################################################################
			final Boolean esitoPostVisto = postVisto(idDocumento, fpeh);
			if (Boolean.FALSE.equals(esitoPostVisto)) {
				final EsitoOperazioneDTO esitoPostVistoDTO = new EsitoOperazioneDTO(wobNumber);
				esitoPostVistoDTO.setCodiceErrore(VistoErrorEnum.POST_VISTO_COD_ERROR);
				esitoPostVistoDTO.setNote(VistoErrorEnum.POST_VISTO_COD_ERROR.getMessage());
				return esitoPostVistoDTO;
			}
			//#######################################################################################################
			
			return new EsitoOperazioneDTO(null, null, 0, wobNumber); 
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'avanzamento del WF per l'operazione VISTA: ", e);
			esito.setEsito(false);
			esito.setNote("Errore durante l'avanzamento del WF. " + e.getMessage());
		}
		return esito;
	}
	
	/**
	 * Sigla.
	 *
	 * @param fcDTO
	 *            the fc DTO
	 * @param wobNumbers
	 *            the wob numbers
	 * @param utente
	 *            the utente
	 * @param motivazione
	 *            the motivazione
	 * @return the collection
	 */
	@Override
	public final Collection<EsitoOperazioneDTO> sigla(final FilenetCredentialsDTO fcDTO, final Collection<String> wobNumbers, 
			final UtenteDTO utente, final String motivazione) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();
		for (final String wobNumber : wobNumbers) {
			esiti.add(sigla(fcDTO, wobNumber, utente, motivazione));
		}
		return esiti;
	}

	/**
	 * Sigla.
	 *
	 * @param fcDTO
	 *            the fc DTO
	 * @param wobNumber
	 *            the wob number
	 * @param utente
	 *            the utente
	 * @param motivazione
	 *            the motivazione
	 * @return the esito operazione DTO
	 */
	@Override
	public final EsitoOperazioneDTO sigla(final FilenetCredentialsDTO fcDTO, final String wobNumber, final UtenteDTO utente, final String motivazione) {
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		final PropertiesProvider pp = PropertiesProvider.getIstance();
		Integer numDoc = null;
		Connection connection =  null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(fcDTO);
			
			final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);
			final Integer nIdDocumento = (Integer) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
			if (nIdDocumento == null) {
				throw new RedException("ID documento non trovato per il documento da siglare.");
			}
			final String idDocumento = nIdDocumento.toString();
			
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final Document documentCE = fceh.getDocumentForDetail(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), idDocumento);
			numDoc = (Integer) TrasformerCE.getMetadato(documentCE, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);

//			<- Recupero id Tipo Spedizione ->
			final Collection<?> inDestinatari = (Collection<?>) TrasformerCE.getMetadato(documentCE, PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY);
			// Si normalizza il metadato per evitare errori comuni durante lo split delle singole stringhe dei destinatari
			final List<String[]> destinatariDocumento = DestinatariRedUtils.normalizzaMetadatoDestinatariDocumento(inDestinatari);
			final int idTipoSpedizione = TipoSpedizioneEnum.getIdTipoSpedizione(destinatariDocumento);
//			<- Fine Blocco ->

			final HashMap<String, Object> metadati = new HashMap<>();
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId().intValue());
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), motivazione);
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_TIPO_SPEDIZIONE_METAKEY), idTipoSpedizione);
			//unica differenza con NSD. qui viene imposto un tipoSpedizione 'IGNOTO' invece nsd lo recupera dal wf   

			final String[] responses = fpeh.getResponses(wob);
			final String response = LibroFirmaPerFirmaResponseEnum.SIGLA.firstIn(responses);
			fpeh.nextStep(wob, metadati, response);
			
			generaSecurity(wobNumber, utente, connection, fpeh, idDocumento, documentCE);

			esito = new EsitoOperazioneDTO(null, null, numDoc, wobNumber);
			if (esito.isEsito()) {
				esito.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
			}
			return esito;
		} catch (final Exception e) {
			rollbackConnection(connection);
			LOGGER.error("Errore durante l'avanzamento del WF per l'operazione SIGLA: ", e);
			esito.setNote("Errore durante l'avanzamento del WF.");
			esito.setIdDocumento(numDoc);
		} finally {
			closeConnection(connection);
			logoff(fpeh);
			popSubject(fceh);
		}
		return esito;
	}

	/**
	 * @param wobNumber
	 * @param utente
	 * @param connection
	 * @param fpeh
	 * @param idDocumento
	 * @param documentCE
	 */
	private void generaSecurity(final String wobNumber, final UtenteDTO utente, final Connection connection,
			final FilenetPEHelper fpeh, final String idDocumento, final Document documentCE) {
		
		final Integer riservato = (Integer) TrasformerCE.getMetadato(documentCE, PropertiesNameEnum.RISERVATO_METAKEY);
		final boolean isRiservato = riservato != null && riservato > 0;
		try {
			if (isRiservato) {
				securitySRV.generateRiservatoSecurity(utente, idDocumento, wobNumber, fpeh, connection);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante le operazioni post avanzamento del WF durante l'operazione di sigla ", e);
		}
	}

	/**
	 * Rifiuta.
	 *
	 * @param utente
	 * @param wobNumbers
	 * @param motivoRifiuto
	 * @param responseEnum
	 * @param isOneKindOfCorriere
	 * @return the collection
	 */
	@Override
	public final Collection<EsitoOperazioneDTO> rifiuta(final UtenteDTO utente, final Collection<String> wobNumbers, final String motivoRifiuto, final ResponsesRedEnum responseEnum, final boolean isOneKindOfCorriere) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();
		
		for (final String wobNumber : wobNumbers) {
			esiti.add(rifiuta(utente, wobNumber, motivoRifiuto, responseEnum, isOneKindOfCorriere));
		}
		return esiti;
	}

	/**
	 * Rifiuta.
	 * @param utente
	 * @param wobNumbers
	 * @param motivoRifiuto
	 * @param responseEnum
	 * @param isOneKindOfCorriere
	 * @return the esito operazione DTO
	 */
	@Override
	public final EsitoOperazioneDTO rifiuta(final UtenteDTO utente, final String wobNumber, final String motivoRifiuto, 
			final ResponsesRedEnum responseEnum, final boolean isOneKindOfCorriere) {

		final FilenetCredentialsDTO fcDTO = utente.getFcDTO();
		final Long idUtente = utente.getId();
		final Long idAoo = utente.getIdAoo();
		final Long idNodoAssegnazioneIndiretta = utente.getIdNodoAssegnazioneIndiretta();
		final Long idUfficioMittente = utente.getIdUfficio();
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		String wobNumberPadre = null;
		if (idAoo == null || idUtente == null) {
			throw new RedException("L'identificativo dell'AOO e dell'utente devono essere valorizzati.");
		}
		try {
			fpeh = new FilenetPEHelper(fcDTO);
			fceh = FilenetCEHelperProxy.newInstance(fcDTO, idAoo);

			final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);
			
			wobNumberPadre = (String) TrasformerPE.getMetadato(wob, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.WF_PARENT_NUMBER_METAKEY));
	
			esito = validateRifiuto(fcDTO, wobNumber, idUtente, idAoo);
			if (!esito.isEsito()) {
				esito.setIdDocumento(0);
				return esito;
			}

			final boolean copiaConforme = Boolean.TRUE.equals(TrasformerPE.getMetadato(wob, PropertiesNameEnum.FIRMA_COPIA_CONFORME_METAKEY));

			final HashMap<String, Object> metadati = new HashMap<>();
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), motivoRifiuto);
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DATA_ASSEGNAZIONE_WF_METAKEY), 
					new SimpleDateFormat(DateUtils.DD_MM_YYYY_HH_MM_SS).format(new Date()));
			
			final boolean aooHasAssegnazioneIndiretta = idNodoAssegnazioneIndiretta != null && idNodoAssegnazioneIndiretta != 0;
			
			if (isOneKindOfCorriere && aooHasAssegnazioneIndiretta && isWorkflowPrincipale(fpeh, wob, fcDTO.getIdClientAoo())) {
				metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ASSEGNAZIONE_INDIRETTA_METAKEY), BooleanFlagEnum.NO.getIntValue());
			}

			// Se è per Copia Conforme NON si passa l'ID utente mittente
			if (!copiaConforme) {
				metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), idUtente.intValue());
			}
			
			final String response = recuperaResponse(responseEnum);
			
			fpeh.nextStep(wob, metadati, response);

			final Integer idDocumento = (Integer) TrasformerPE.getMetadato(wob, PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY);
			final Integer tipoAssegnazioneId = (Integer) TrasformerPE.getMetadato(wob, PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY);

			//se è un annullamento di un visto, osservazione, richiesta chiarimenti, relazione positiva o relazione negativa si valorizza il metadato annullaTemplate
			if (ResponsesRedEnum.ANNULLA_OSSERVAZIONE.equals(responseEnum) 
					|| ResponsesRedEnum.ANNULLA_RICHIESTA_INTEGRAZIONI.equals(responseEnum) 
					|| ResponsesRedEnum.ANNULLA_VISTO.equals(responseEnum)
					|| ResponsesRedEnum.ANNULLA_RELAZIONE_POSITIVA.equals(responseEnum)
					|| ResponsesRedEnum.ANNULLA_RELAZIONE_NEGATIVA.equals(responseEnum)) {
				metadati.clear();
				metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ANNULLA_TEMPLATE_METAKEY), BooleanFlagEnum.SI.getIntValue());
				fpeh.updateWorkflow(wob, metadati);
				
				// Si effettua il salvataggio del rifiuto sullo storico
				salvaRifiuto(motivoRifiuto, responseEnum, idUtente, idAoo, idUfficioMittente, idDocumento);
			}
			
			final Document doc = fceh.getDocumentForRifiuto(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), String.valueOf(idDocumento));
			final MasterDocumentoDTO masterDTO = TrasformCE.transform(doc, TrasformerCEEnum.FROM_DOCUMENTO_TO_MASTER);
			
			postRifiuto(fpeh, fceh, wobNumber, idAoo, idDocumento, tipoAssegnazioneId, masterDTO.getIdCategoriaDocumento(), wobNumberPadre);
			
			esito = new EsitoOperazioneDTO(null, null, masterDTO.getNumeroDocumento(), wobNumber);
			if (esito.isEsito()) {
				esito.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
				if (tipoAssegnazioneId != null && (tipoAssegnazioneId.equals(TipoAssegnazioneEnum.FIRMA.getId()) 
				|| tipoAssegnazioneId.equals(TipoAssegnazioneEnum.FIRMA_MULTIPLA.getId()))) {
					notificaSRV.writeNotifiche(idAoo.intValue(), masterDTO);
				}
			}
			
			return esito;
		} catch (final Exception e) {
			LOGGER.error("Errore durante il rifiuto del documento con WOB_NUMBER: " + wobNumber + " ", e);
			esito.setEsito(false);
			esito.setNote("Errore durante il rifiuto del documento");
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}
		return esito;
	}

	/**
	 * Restituisce {@code true} se il workflow identificato dal {@code wob} è un
	 * workflow principale, restituisce {@code false} altrimenti.
	 * 
	 * @param fpeh
	 *            Filenet PE helper.
	 * @param wob
	 *            Workflow per il quale
	 * @param idClientAoo
	 *            Identificativo client AOO.
	 * @return {@code true} se il {@code wob} è principale.
	 */
	private boolean isWorkflowPrincipale(final FilenetPEHelper fpeh, final VWWorkObject wob, final String idClientAoo) {
		final String documentTitle = String.valueOf(TrasformerPE.getMetadato(wob, PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
		return fpeh.getWorkflowPrincipale(documentTitle, idClientAoo).getWorkflowNumber().equals(wob.getWorkflowNumber());
	}

	/**
	 * @param responseEnum
	 * @return response
	 */
	private static String recuperaResponse(final ResponsesRedEnum responseEnum) {
		String response = responseEnum.getResponse();
		
		if (ResponsesRedEnum.ANNULLA_OSSERVAZIONE.equals(responseEnum) 
				|| ResponsesRedEnum.ANNULLA_RICHIESTA_INTEGRAZIONI.equals(responseEnum) 
				|| ResponsesRedEnum.ANNULLA_VISTO.equals(responseEnum)
				|| ResponsesRedEnum.ANNULLA_RELAZIONE_POSITIVA.equals(responseEnum)
				|| ResponsesRedEnum.ANNULLA_RELAZIONE_NEGATIVA.equals(responseEnum)) {
			
			response = ResponsesRedEnum.RIFIUTA.getResponse();
		}
		return response;
	}

	/**
	 * Gestisce il salvataggio del rifiuto sullo storico.
	 * @param motivoRifiuto
	 * @param responseEnum
	 * @param idUtente
	 * @param idAoo
	 * @param idUfficioMittente
	 * @param idDocumento
	 * @param connectionApp
	 * @param connectionDwh
	 */
	private void salvaRifiuto(final String motivoRifiuto, final ResponsesRedEnum responseEnum, final Long idUtente, final Long idAoo, final Long idUfficioMittente, final Integer idDocumento) {
		Connection connectionApp = null;
		Connection connectionDwh = null;
		
		try {
			connectionApp = setupConnection(getDataSource().getConnection(), false);
			connectionDwh = setupConnection(getFilenetDataSource().getConnection(), false);
			final Collection<String> allacci = allaccioDAO.getAllacciPrincipaliByDocumentTitle((Collection<String>) Arrays.asList(idDocumento.toString()), idAoo, connectionApp);
			EventTypeEnum ev = null;
			if (ResponsesRedEnum.ANNULLA_OSSERVAZIONE.equals(responseEnum)) {
				ev = EventTypeEnum.OSSERVAZIONE_ANNULLATA;
			} else if (ResponsesRedEnum.ANNULLA_RICHIESTA_INTEGRAZIONI.equals(responseEnum)) {
				ev = EventTypeEnum.RICHIESTA_INTEGRAZIONE_ANNULLATA;
			} else if (ResponsesRedEnum.ANNULLA_VISTO.equals(responseEnum)) {
				ev = EventTypeEnum.VISTO_ANNULLATO;
			} else if (ResponsesRedEnum.ANNULLA_RELAZIONE_POSITIVA.equals(responseEnum)) {
				ev = EventTypeEnum.RELAZIONE_POSITIVA_ANNULLATA;
			} else if (ResponsesRedEnum.ANNULLA_RELAZIONE_NEGATIVA.equals(responseEnum)) {
				ev = EventTypeEnum.RELAZIONE_NEGATIVA_ANNULLATA;
			}
			logSRV.inserisciEventoLog(Integer.parseInt(allacci.iterator().next()), idUfficioMittente, idUtente, 0L, 0L, new Date(), ev, motivoRifiuto, 0, idAoo, connectionDwh);
		} catch (final Exception e) {
			LOGGER.error("Erorre durante il salvataggio sullo storico del rifiuto.", e);
			throw new RedException(e);
		} finally {
			closeConnection(connectionApp);
			closeConnection(connectionDwh);
		}
	}

	/**
	 * Esegue le operazioni a seguito dell'operazione di visto su un WF.
	 * @param idDocumento
	 * @param fpeh - PE Helper per eseguire le operazioni su Filenet
	 * @return Esito delle operazioni post visto.
	 */
	private static Boolean postVisto(final Integer idDocumento, final FilenetPEHelper fpeh) {
		Boolean esito = true;
		final String wfNumber = null;
		final String wfSubject = null;
		try {
			// i workflow al giro visti
			final Collection<VWWorkObject> workflowsAlGiroVisti = fpeh.fetchWorkflowsAlGiroVistiByDocumentoId(idDocumento);
			
			for (final Iterator<VWWorkObject> iterator = workflowsAlGiroVisti.iterator(); iterator.hasNext();) {
				final VWWorkObject workflow = iterator.next();
				final boolean esitoWF = eseguiStornoAutomatico(fpeh, workflow);
				
				if (!esitoWF) {
					LOGGER.error("Storno automatico non riuscito per il workflow[" + workflow.getWorkflowNumber() + "]");
					esito = esitoWF;
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la procedura dello storno automatico del workflow : " + wfNumber + " - " + wfSubject, e);
			throw new RedException("Errore durante la procedura dello storno automatico del workflow : " + wfNumber + " - " + wfSubject);
		}
		return esito;
	}

	/**
	 * @param fpeh
	 * @param workflow
	 * @return esito storno
	 */
	private static boolean eseguiStornoAutomatico(final FilenetPEHelper fpeh, final VWWorkObject workflow) {
		boolean esitoWF = false;
		try {
			LOGGER.info("Storno del workflow : " + workflow.getWorkflowNumber() + " - " + workflow.getSubject());
			final String[] responses = fpeh.getResponses(workflow);
			final String response = LibroFirmaPerFirmaResponseEnum.IN_LAVORAZIONE.firstIn(responses);
			esitoWF =  fpeh.nextStep(workflow, new HashMap<>(), response);
		} catch (final Exception e) {
			LOGGER.error("Storno automatico non riuscito per il workflow[" + workflow.getWorkflowNumber() + "]", e);
		}
		return esitoWF;
	}

	/**
	 * Metodo per la validazione di una operazione di rifiuto.
	 *
	 * @param fcDTO		credenziali filenet
	 * @param wobNumber	wob number
	 * @param idUtente	identificativo utente
	 * @param idAoo		identificativo aoo
	 * @return			esito operazione rifiuto
	 */
	private static EsitoOperazioneDTO validateRifiuto(final FilenetCredentialsDTO fcDTO, final String wobNumber, final Long idUtente, final Long idAoo) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		if (fcDTO == null) {
			esito.setNote("Non sono state definite le credenziali per accedere al repository.");
			return esito;
		} else if (wobNumber == null || wobNumber.trim().length() <= 0) {
			esito.setNote("Non è stato definito nessun documento su cui eseguire l'operazione di rifiuto.");
			return esito;
		} else if (idUtente == null || idUtente <= 0) {
			esito.setNote("Non è stato definito l'identificativo dell'utente con cui eseguire l'operazione di rifiuto.");
			return esito;
		} else if (idAoo == null || idAoo <= 0) {
			esito.setNote("Non è stato definito l'identificativo dell'AOO con cui eseguire l'operazione di rifiuto.");
			return esito;
		}
		esito.setEsito(true);
		return esito;
	}

	/**
	 * Metodo invocato in seguito all'eseguimento dell'operazione di rifiuto.
	 *
	 * @param fpeh			helper gestione pe
	 * @param fceh			helper gestione ce
	 * @param wobNumber		identificativo workflow
	 * @param idAoo			identificativo aoo
	 * @param idDocumento
	 * @param tipoAssegnazioneId
	 * @param idCategoriaDocumento
	 */
	private void postRifiuto(final FilenetPEHelper fpeh, final IFilenetCEHelper fceh, final String wobNumber, final Long idAoo,
			final Integer idDocumento, final Integer tipoAssegnazioneId, final Integer idCategoriaDocumento,
			final String wobNumberPadre) {
		int idCategoria = -1;
		if (idCategoriaDocumento != null) {
			idCategoria = idCategoriaDocumento;
		}

		int idTipoAssegnazione = -1;
		if (tipoAssegnazioneId != null) {
			idTipoAssegnazione = tipoAssegnazioneId;
		}
		if (CategoriaDocumentoEnum.ASSET.getIds()[0].equals(idCategoria)) {
			String wfStepDescription = null;
			try {
				final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);
				wfStepDescription = wob.getStepDescription();
			} catch (final Exception e) {
				throw new RedException("Errore in fase di recupero next step", e);
			}
			final StatoRichiestaDTO statoCorrente = StatoRichiestaProvider.getIstance().getStatoRichiestaByStepName(wfStepDescription);
			final HashMap<String, Object> metadati = new HashMap<>();
			if (statoCorrente != null) {
				metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.STATO_RICHIESTA_ID_METAKEY), statoCorrente.getStatoRichiestaID());
			} else {
				metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.STATO_RICHIESTA_ID_METAKEY), StatoRichiesta.ID_RIFIUTATA);
			}
			fceh.updateMetadatiOnCurrentVersion(idAoo, idDocumento, metadati);
		}

		if (!CategoriaDocumentoEnum.ASSET.getIds()[0].equals(idCategoria)) { 
			if (idTipoAssegnazione == TipoAssegnazioneEnum.CONTRIBUTO.getId()) { 
				stornaWorkflowPerContributo(wobNumber, idDocumento.toString(), fpeh, idAoo);
			} else if (idTipoAssegnazione == TipoAssegnazioneEnum.VISTO.getId()) {
				vistoSRV.stornaDalGiroVistiByIdDocumento(idDocumento, fpeh, null);
				if(!StringUtils.isNullOrEmpty(wobNumberPadre)) {
					vistoSRV.updateWorkflowPadre(wobNumber, wobNumberPadre, idDocumento,idAoo, fpeh);
				}
			} else {
				LOGGER.warn("Per il tipo assegnazione corrente non è previsto lo storno automatico del documento con ID: " + idDocumento);
			}
		}
	}

	/**
	 * Metodo per lo storno workflow per contributo.
	 * 
	 * @param currentWobNumber workflow
	 * @param idDocumento	identificativo documento
	 * @param fpeh			helper per la gestione del pe
	 * @param idAoo			identificativo aoo
	 */
	@Override
	public void stornaWorkflowPerContributo(final String currentWobNumber, final String idDocumento, final FilenetPEHelper fpeh, final Long idAoo) {
		final String codaInSospeso = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.STEP_IN_SOSPESO_MAPPING_WFKEY).split("\\|")[1];
		try {
			final List<Long> idDocumenti = new ArrayList<>();
			idDocumenti.add(Long.parseLong(idDocumento));
			//Recupero tutti i workflow legati al documento
			final VWRosterQuery roster = fpeh.getDocumentWorkFlowsByIdDocumenti(idDocumenti);
			VWWorkObject workFlowPrincipale = null;
			int countAttivi = 0;
			if (roster != null) {
				while (roster.hasNext()) {
					final VWWorkObject workFlow = (VWWorkObject) roster.next();
					if (!currentWobNumber.equals(workFlow.getWorkflowNumber())) {

						if (workFlow.getSubject() != null 
								&& workFlow.getSubject().startsWith(PropertiesProvider.getIstance().
										getParameterByKey(PropertiesNameEnum.WF_PROCESSO_CONTRIBUTO_WFKEY).split("\\|")[0])) {
							countAttivi++;
						} else {
							final Integer idTipoAssegnazione = (Integer) workFlow.getFieldValue(PropertiesProvider.getIstance().
									getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY));
							if (Boolean.TRUE.equals(contributoSRV.isWorkflowPadrePerContributo(workFlow, idAoo)) 
								&& isAssegnazioneCompetenzaPerStorno(idTipoAssegnazione)) {
								final String codaCorrente = (String) workFlow.getFieldValue(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NOME_CODA_METAKEY));
								if (codaInSospeso.equals(codaCorrente)) {
									workFlowPrincipale = workFlow;
								} else {
									LOGGER.warn("Il Workflow principale si trova già in stato lavorazione,"
											+ " non occorre lo storno automatico del documento con id=" + idDocumento);
								}
							}
						}
					}
				}
			}
			if (countAttivi == 0 && workFlowPrincipale != null) {
				fpeh.nextStep(workFlowPrincipale, new HashMap<>(), ResponsesRedEnum.SPOSTA_IN_LAVORAZIONE.getResponse());
			} else {
				LOGGER.warn("Il Visto/Contributo non è l'ultimo, non viene effettuato lo storno automatico del documento con ID=" + idDocumento);
			}
		} catch (final Exception e) {
			throw new RedException("Errore durante lo storno per contributo del WF legato al documento con ID=" + idDocumento, e);
		}
	}

	@Override
	public final EsitoOperazioneDTO richiediSollecito(final String wobNumber, final String documentTitle, final String documentTitleDocContributo, 
			final String oggettoDocContributo, final Collection<String> mailDestinatari, final UtenteDTO utente) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		IFilenetCEHelper fcehAdmin = null;
		Connection dwhCon = null;
		try {
			fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()), utente.getIdAoo());
			
			// Si concatenano (separandoli con la virgola) gli indirizzi mail dei destinatari a cui inviare l'e-mail di sollecito
			final String destinatariMailTo = mailDestinatari.stream().collect(Collectors.joining(","));
			
			// ### Invio sincrono della mail di sollecito ai destinatari selezionati -> START
			// 1 - Si recupera la mail associata al contributo esterno
			final Document emailSpedizione = fcehAdmin.fetchMailSpedizione(documentTitleDocContributo, utente.getIdAoo().intValue());
			
			// 2 - Si modificano alcuni dati della mail per adattarli all'invio per sollecito
			final String messageId = (String) TrasformerCE.getMetadato(emailSpedizione, PropertiesNameEnum.MESSAGE_ID);
			final String from = (String) TrasformerCE.getMetadato(emailSpedizione, PropertiesNameEnum.FROM_MAIL_METAKEY);
			
			Integer tipologiaMessaggioId = null;
			final CasellaPostaDTO casellaPostaleRed = casellePostaliSRV.getCasellaPostale(fcehAdmin, from);
			if (casellaPostaleRed != null && casellaPostaleRed.getIndirizzoEmail().equals(from)) {
				tipologiaMessaggioId = casellaPostaleRed.getTipologia();
			}
			final String testoMailSollecito = StringUtils.convertiTestoMailHtml("Sollecito contributo esterno; oggetto: " + oggettoDocContributo);

			// 3 - Si crea la mail di sollecito
			final MessaggioEmailDTO messaggio = new MessaggioEmailDTO(documentTitleDocContributo, destinatariMailTo, null, from,
					"SOLLECITO - " + oggettoDocContributo, testoMailSollecito, TipologiaInvio.NUOVOMESSAGGIO, tipologiaMessaggioId, messageId, 
					emailSpedizione.get_Id().toString(), false, false, null, String.join(destinatariMailTo, "###;###"), null);
			
			// 4 - Si invia la mail
			final ResponseSendMail esitoInvio = codaMailSRV.sendMail(messaggio, utente.getIdAoo().intValue(), fcehAdmin);
			// ### Invio sincrono della mail di sollecito ai destinatari selezionati -> END
			// Se l'invio è andato a buon fine, si traccia l'evento di invio della mail di sollecito
			if (esitoInvio.isInviato()) {
				dwhCon = setupConnection(getFilenetDataSource().getConnection(), false);
				
				eventoLogSRV.inserisciEventoLog(NumberUtils.toInt(documentTitle), utente.getIdUfficio(), utente.getId(), 0L, 0L, 
						Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant()), EventTypeEnum.SOLLECITO_CONTRIBUTO, 
						"Sollecito verso: " + destinatariMailTo.replace(",", ", "), 0, utente.getIdAoo(), dwhCon);
				esito.setEsito(true);
				esito.setNote("Invio e-mail di sollecito effettuato");
				LOGGER.info("L'invio dell'e-mail di sollecito dal documento: " + documentTitle + " è stato effettuato con successo.");
			} else {
				esito.setNote("Si è verificato un errore durante l'invio dell'e-mail di sollecito; messaggio di errore: " + esitoInvio.getErroreInvio());
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			esito.setEsito(false);
			esito.setNote("Si è verificato un errore durante l'invio dell'e-mail di sollecito");
		} finally {
			closeConnection(dwhCon);
			popSubject(fcehAdmin);
		}
		return esito;
	}

	/**
	 * Getter flag assegnazione competenza per storno.
	 *
	 * @param idTipoAssegnazione 	  identificativo tipo assegnazione
	 * @return assegnazioneCompetenza flag assegnazione competenza per storno
	 */
	private static boolean isAssegnazioneCompetenzaPerStorno(final int idTipoAssegnazione) {
		boolean assegnazioneCompetenza = false;
		if (idTipoAssegnazione == TipoAssegnazioneEnum.COMPETENZA.getId()
				|| idTipoAssegnazione == TipoAssegnazioneEnum.FIRMA.getId()
				|| idTipoAssegnazione == TipoAssegnazioneEnum.SIGLA.getId()
				|| idTipoAssegnazione == TipoAssegnazioneEnum.VISTO.getId()
				|| idTipoAssegnazione == TipoAssegnazioneEnum.RIFIUTO_ASSEGNAZIONE.getId()
				|| idTipoAssegnazione == TipoAssegnazioneEnum.RIFIUTO_FIRMA.getId()
				|| idTipoAssegnazione == TipoAssegnazioneEnum.RIFIUTO_SIGLA.getId()
				|| idTipoAssegnazione == TipoAssegnazioneEnum.RIFIUTO_VISTO.getId()) {
			assegnazioneCompetenza = true;
		}
		return assegnazioneCompetenza;
	}

	@Override
	public final EsitoOperazioneDTO recall(final UtenteDTO utente, final String wobNumber, final String motivoRecall) {
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		Integer numeroDocumento = null;
		Connection connection = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final VWWorkObject wf = fpeh.getWorkFlowByWob(wobNumber, true);
			final ModificaIterPEDTO modIterPEDTO = TrasformPE.transform(wf, TrasformerPEEnum.MODIFICA_ITER);

			final Document doc = fceh.getDocumentForModificaIter(modIterPEDTO.getIdDocumento());
			final ModificaIterCEDTO modIterCEDTO = TrasformCE.transform(doc, TrasformerCEEnum.MODIFICA_ITER);

			numeroDocumento = modIterCEDTO.getNumDoc();

			final String[] responses = fpeh.getResponses(wf);
			if (!Arrays.asList(responses).contains(ResponsesRedEnum.RECALL.getResponse())) {
				//cancella item
				codeApplicativeSRV.deleteRecallItem(modIterPEDTO.getIdDocumento(), connection);
				esito.setNote("Il documento non è più disponibile per l'operazione richiesta");
				esito.setIdDocumento(numeroDocumento);
				return esito;
			}
			//preparazione info start nuovo workflow a partire dal workflow che si sta per killare
			final HashMap<String, Object> metadati = modIterSRV.getMetadatiModificaIterManuale(utente, modIterCEDTO, modIterPEDTO, "0", TipoAssegnazioneEnum.COMPETENZA, 
					utente.getId(), utente.getIdUfficio(), doc, fceh, connection);

			esito = owfSRV.genericNextStep(wobNumber, utente, ResponsesRedEnum.RECALL.getResponse(), motivoRecall, fceh, fpeh, connection);

			//startare il nuovo workflow
			modIterSRV.avviaNuovoIterManuale(utente, modIterCEDTO, modIterPEDTO, metadati, "0", fceh, fpeh, connection);
			
			esito = new EsitoOperazioneDTO(null, null, modIterCEDTO.getNumDoc(), wobNumber);
			esito.setNote("Operazione effettuata con successo.");

		} catch (final Exception e) {
			LOGGER.error("Errore durante l'esecuzione della recall per il workflow " + wobNumber, e);
			esito.setNote("Errore durante l'avanzamento del WF.");
			esito.setIdDocumento(numeroDocumento);
		} finally {
			closeConnection(connection);
			logoff(fpeh);
			popSubject(fceh);
		}
		return esito;
	}

	/**
	 * StornaACorriere.
	 *
	 * @param fcDTO
	 * @param wobNumbers
	 * @param utente
	 * @param stornaACorriere
	 * @return esiti
	 */
	@Override 												 	 				
	public final Collection<EsitoOperazioneDTO> stornaACorriere(final FilenetCredentialsDTO fcDTO, final Collection<String> wobNumbers, final UtenteDTO utente, final String stornaACorriere) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();
		
		for (final String wobNumber : wobNumbers) {
			esiti.add(stornaACorriere(fcDTO, wobNumber, utente, stornaACorriere));
		}
		return esiti;
	}

	/**
	 * StornaACorriere.
	 *
	 * @param fcDTO
	 * @param wobNumber
	 * @param utente
	 * @param stornaACorriere
	 * @return the esito operazione DTO
	 */
	@Override
	public final EsitoOperazioneDTO stornaACorriere(final FilenetCredentialsDTO fcDTO, final String wobNumber, final UtenteDTO utente, final String stornaACorriere) {
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		try {
			fpeh = new FilenetPEHelper(fcDTO);
			fceh = FilenetCEHelperProxy.newInstance(fcDTO, utente.getIdAoo());

			final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);

			final HashMap<String, Object> metadati = new HashMap<>();
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), stornaACorriere);
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DATA_ASSEGNAZIONE_WF_METAKEY), 
					new SimpleDateFormat(DateUtils.DD_MM_YYYY_HH_MM_SS).format(new Date()));

			String response;
			response = ResponsesRedEnum.STORNA_AL_CORRIERE.getResponse(); // Response sollecitata in caso di workflow per Copia Conforme

			fpeh.nextStep(wob, metadati, response);

			esito = new EsitoOperazioneDTO(null, null, null, wobNumber);
			if (esito.isEsito()) {
				esito.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
			}
			return esito;
		} catch (final Exception e) {
			LOGGER.error("Errore durante il rifiuto del documento con WOB_NUMBER: " + wobNumber + " ", e);
			esito.setEsito(false);
			esito.setNote("Errore durante il rifiuto del documento");
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}
		return esito;
	}
}
