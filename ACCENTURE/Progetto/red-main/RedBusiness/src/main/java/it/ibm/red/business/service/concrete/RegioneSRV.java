package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IRegioneDAO;
import it.ibm.red.business.dto.RegioneDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.IRegioneSRV;

/**
 * Service che gestisce le regioni.
 */
@Service
@Component
public class RegioneSRV extends AbstractService implements IRegioneSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -5952866762299190927L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RegioneSRV.class);

	/**
	 * DAO.
	 */
	@Autowired
	private IRegioneDAO regioneDAO;

	/**
	 * @see it.ibm.red.business.service.facade.IRegioneFacadeSRV#getAll().
	 */
	@Override
	public List<RegioneDTO> getAll() {
		Connection conn = null;
		List<RegioneDTO> list = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);	
			list = regioneDAO.getAll(conn);
		} catch (BeansException | SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(conn);
		}
		return list;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRegioneFacadeSRV#get(java.lang.Long).
	 */
	@Override
	public RegioneDTO get(final Long idRegione) {
		Connection conn = null;
		RegioneDTO regione = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);	
			regione = regioneDAO.get(idRegione, conn);
		} catch (BeansException | SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(conn);
		}
		return regione;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRegioneFacadeSRV#getRegioni(java.lang.String).
	 */
	@Override
	public List<RegioneDTO> getRegioni(final String query) {
		Connection conn = null;
		List<RegioneDTO> list = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			list = regioneDAO.getRegioni(conn, query);
		} catch (BeansException | SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(conn);
		}
		return list;
	}

}
