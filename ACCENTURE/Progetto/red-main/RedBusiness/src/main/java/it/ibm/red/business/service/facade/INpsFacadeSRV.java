package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.ParamsRicercaProtocolloDTO;
import it.ibm.red.business.dto.ParamsRicercaRegistrazioniAusiliarieDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.RegistrazioneAusiliariaNPSDTO;
import it.ibm.red.business.dto.RegistroProtocolloDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.nps.dto.DocumentoNpsDTO;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.NpsConfiguration;

/**
 * The Interface INpsFacadeSRV.
 *
 * @author CPIERASC
 * 
 *         Facade gestione NPS.
 */
public interface INpsFacadeSRV extends Serializable {

	/**
	 * Esegui l'upload di documento principale ed eventuali allegati (da
	 * protocollazione automatica via batch)
	 * 
	 * @param documento
	 * @param idNodoDestinatario
	 * @param fceh
	 * @param allegati
	 * @param codiceAoo
	 */
	void uploadContentToAsyncNps(Document documento, Integer idNodoDestinatario, IFilenetCEHelper fceh, boolean allegati, Aoo aoo, String idDocumento);

	/**
	 * Lavora la prossima request in coda per l'AOO in input.
	 * 
	 * @param partition
	 * @return
	 */
	void workNextAsyncRequests(int idAoo);

	/**
	 * Chiama il ws per annullare il protocollo, Dopo aver annullato il protocollo
	 * sarebbe buona norma annullare il documento Esiste un metodo di documentRedSRV
	 * che fa entrambi
	 * 
	 * @param idProtocollo          Corrisponde al medatadato.idprotocollo di un
	 *                              document
	 * @param motivoAnnullamento    stringa inserita dall'utente
	 * @param provvedimentoAnnulato stringa inserita dall'utente
	 * @param utente
	 */
	void annullaProtocollo(String idProtocollo, String infoProtocollo, String motivoAnnullamento, String provvedimentoAnnulato, UtenteDTO utente, String idDocumento);

	/******** CONSULTAZIONE INFO PROTOCOLLO ******/
	/**
	 * @param paramsRicercaProtocollo
	 * @param utente
	 * @return
	 */
	List<ProtocolloNpsDTO> ricercaProtocolli(ParamsRicercaProtocolloDTO paramsRicercaProtocollo, UtenteDTO utente);

	/**
	 * @param idAoo
	 * @param inProtocolloNps
	 * @param utente
	 * @return
	 */
	ProtocolloNpsDTO getDettagliProtocollo(ProtocolloNpsDTO inProtocolloNps, Integer idAoo,UtenteDTO utente,boolean aperturaDaFascicolo);

	/**
	 * @param idAoo
	 * @param guid
	 * @param accessoAdmin
	 * @return
	 */
	DocumentoNpsDTO downloadDocumento(Integer idAoo, String guid,boolean accessoAdmin);

	/**
	 * Ricerca dei Registri giornalieri dei protocolli.
	 * 
	 * @param paramsRicercaRegistro
	 * @param utente
	 * @return
	 */
	Collection<RegistroProtocolloDTO> ricercaStampaRegistro(ParamsRicercaAvanzataDocDTO paramsRicercaRegistro, UtenteDTO utente);

	/**
	 * Ricerca i dati minimi di protocollo su NPS
	 * 
	 * @param idAoo
	 * @param idProtocollo
	 * @param tipoProtocollo
	 * @return
	 */
	ProtocolloNpsDTO getDatiMinimiByIdProtocollo(Integer idAoo, String idProtocollo, Integer tipoProtocollo);

	/******** CONSULTAZIONE INFO PROTOCOLLO ******/

	/**
	 * Stampa etichette.
	 * 
	 * @param utente
	 * @param wobNumber
	 * @param contente
	 * @return byte[]
	 */
	byte[] stampaEtichette(String wobNumber, UtenteDTO utente);

	/**
	 * Ricerca delle registrazioni ausiliarie nei registri ausiliari.
	 * 
	 * @param paramsRicerca
	 * @param utente
	 * @return
	 */
	List<RegistrazioneAusiliariaNPSDTO> ricercaRegistrazioniAusiliarie(ParamsRicercaRegistrazioniAusiliarieDTO paramsRicerca, UtenteDTO utente);

	/**
	 * @param idAoo
	 * @param protocolloNps
	 * @param perProtocollazioneAutomatica
	 * @param ricercaAdminNps
	 * @return
	 */
	ProtocolloNpsDTO getDettagliProtocollo(Integer idAoo, ProtocolloNpsDTO protocolloNps, boolean perProtocollazioneAutomatica,UtenteDTO utente,boolean aperturaDaFascicolo);

	/**
	 * Annullamento parziale di protocollo.
	 * 
	 * @param utente
	 * @param infoProtocollo
	 * @param idProtocollo
	 * @param oggetto
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @param descrizioneTipologiaDocumento
	 * @param indiceClassificazione
	 * @param idDocumento
	 * @param metadatiEstesi
	 * @return
	 */
	boolean aggiornaDatiProtocollo(UtenteDTO utente, String infoProtocollo, String idProtocollo, String oggetto, Integer idTipologiaDocumento, Integer idTipoProcedimento,
			String descrizioneTipologiaDocumento, String indiceClassificazione, String idDocumento, Collection<MetadatoDTO> metadatiEstesi);

	/**
	 * Recupera l'utente dalla base dati applicativa a partire dal codice fiscale
	 * del protocollatore. In caso il recupero non abbia esito positivo, viene
	 * indicato come protocollatore il default dell'aoo prelevato dalle properties.
	 * 
	 * @param protocolloNps
	 * @return
	 */
	UtenteDTO getUtenteProtocollatore(ProtocolloNpsDTO protocolloNps, boolean onlyChiaveEsterna);

	/**
	 * Ottiene tutte le configurazioni Nps.
	 * 
	 * @return lista delle configurazioni Nps
	 */
	List<NpsConfiguration> getAll();

	/**
	 * @param paramsRicercaProtocollo
	 * @param utente
	 * @param fromFascicolo
	 * @return
	 */
	List<ProtocolloNpsDTO> ricercaProtocolli(ParamsRicercaProtocolloDTO paramsRicercaProtocollo, UtenteDTO utente,
			boolean fromFascicolo);
 

	/**
	 * Recupera la descrizione della tipologia documento da comunicare ad NPS
	 * 
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @param tipologiaDocumentoDesc
	 * @param idAoo
	 * @return
	 */
	String getTipologiaDocumentoNPS(Integer idTipologiaDocumento, Integer idTipoProcedimento, String tipologiaDocumentoDesc, Integer idAoo);
}