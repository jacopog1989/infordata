/**
 * 
 */
package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IiterApprovativoFacadeSRV;

/**
 * @author APerquoti
 *
 */
public interface IiterApprovativoSRV extends IiterApprovativoFacadeSRV {

}
