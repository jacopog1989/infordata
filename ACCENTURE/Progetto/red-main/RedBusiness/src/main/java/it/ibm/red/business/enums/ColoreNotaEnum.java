package it.ibm.red.business.enums;

import it.ibm.red.business.dto.NotaDTO;
import it.ibm.red.business.utils.StringUtils;

/**
 * @author APerquoti
 *
 */
public enum ColoreNotaEnum {

	/**
	 * Valore.
	 */
	NERO(0, "black", "Nero"),
	
	/**
	 * Valore.
	 */
	ROSSO(1, "red", "Rosso"),
	
	/**
	 * Valore.
	 */
	GIALLO(2, "yellow", "Giallo"),
	
	/**
	 * Valore.
	 */
	VERDE(3, "green", "Verde"),
	
	/**
	 * Valore.
	 */
	ARANCIONE(4, "orange", "Arancione"),
	
	/**
	 * Valore.
	 */
	BLU(5, "blue", "Blu");

	/**
	 * Descrizione da visualizzare in qulche modo a schermo per accessibilita'.
	 */
	private String descrizione;

	/**
	 * Identificativo.
	 */
	private Integer id;

	/**
	 * Colore.
	 */
	private String color;
	
	/**
	 * Costruttore.
	 * @param inId
	 * @param inColor
	 * @param inDescrizione
	 */
	ColoreNotaEnum(final Integer inId, final String inColor, final String inDescrizione) {
		this.id = inId;
		this.color = inColor;
		this.descrizione = inDescrizione;
	}

	/**
	 * @return the id
	 */
	public final Integer getId() {
		return id;
	}

	/**
	 * @return the color
	 */
	public final String getColor() {
		return color;
	}

	/**
	 * Restituisce la descrizione dell'Enum.
	 * @return
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Imposta la descrizione.
	 * @param descrizione
	 */
	protected void setDescrizione(final String descrizione) {
		this.descrizione = descrizione;
	}
	
	/**
	  * Metodo per il recupero di un'enum dato il valore caratteristico.
	  * 
	  * @param value	valore
	  * @return		enum
	  */
	 public static ColoreNotaEnum get(final Integer color) {
		 ColoreNotaEnum output = null;
		 for (final ColoreNotaEnum c : ColoreNotaEnum.values()) {
			 if (c.getId().equals(color)) {
				 output = c;
				 break;
			 }
		 }
		 return output;
	 }

	 /**
	  * Restituisce una NotaDTO a partira da una nota RAW.
	  * 
	  * @param notaRaw
	  * @return NotaDTO
	  */
	 public static NotaDTO getNotaDTO(final String notaRaw) {
		 final NotaDTO output = new NotaDTO();
		 if (!StringUtils.isNullOrEmpty(notaRaw)) {
			 final String[] assArr = notaRaw.split("_");
			 output.setColore(ColoreNotaEnum.get(Integer.parseInt(assArr[0].substring(1))));
			 if (assArr.length > 1) {
				 output.setContentNota(assArr[1]);
			 }
		 }
		 return output;
	 }
}
