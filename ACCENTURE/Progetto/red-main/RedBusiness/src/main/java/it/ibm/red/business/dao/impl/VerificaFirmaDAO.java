package it.ibm.red.business.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IVerificaFirmaDAO;
import it.ibm.red.business.enums.StatoVerificaFirmaEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.concrete.SignSRV;
import it.ibm.red.business.service.concrete.SignSRV.VerificaFirmaItem;
import it.ibm.red.business.utils.StringUtils;
import oracle.jdbc.OracleTypes;

/**
 * DAO per la verifica della firma.
 */
@Repository
public class VerificaFirmaDAO extends AbstractDAO implements IVerificaFirmaDAO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 403891631285700403L;

	/**
	 * logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(VerificaFirmaDAO.class);

	/**
	 * Messaggio di errore riscontrato nell'insert successiva alla creazione
	 * Filenet.
	 */
	private static final String ERROR_INSERT_CREAZIONE_FILENET_MSG = "Errore nella insert dopo creazione su filenet :";
	
	/**
	 * Messaggio di errore riscontrato nella delete prima della creazione Filenet.
	 */
	private static final String ERROR_DELETE_CREAZIONE_FILENET_MSG = "Errore nella delete prima della creazione su filenet :";

	/**
	 * Select.
	 */
	private static final String SQL_SELECT = "{call p_cerca_valida_firma(?,?,?)}";

	/**
	 * Restituisce gli item di verifica firma.
	 * 
	 * @see it.ibm.red.business.dao.IVerificaFirmaDAO#getContentsToVerify(java.sql.Connection,
	 *      int, int)
	 */
	@Override
	public List<VerificaFirmaItem> getContentsToVerify(final Connection connection, final int idAoo, final int maxResults) {
		CallableStatement cs = null;
		ResultSet rs = null;
		final List<VerificaFirmaItem> items = new ArrayList<>();
		SignSRV.VerificaFirmaItem item = null;
		try {
			cs = connection.prepareCall(SQL_SELECT);
			cs.setInt(1, idAoo);
			cs.setInt(2, maxResults);
			cs.registerOutParameter(3, OracleTypes.CURSOR);

			cs.execute();

			rs = (ResultSet) cs.getObject(3);

			while (rs.next()) {
				item = new VerificaFirmaItem(rs.getString(3), rs.getString(1));
				items.add(item);
			}
			return items;
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero dei content la cui firma è da verificare per idAoo = " + idAoo + ". " + e.getMessage(), e);
			throw new RedException("Errore durante il recupero dei content la cui firma è da verificare per idAoo = " + idAoo + ". " + e.getMessage());
		} finally {
			closeStatement(cs);
			closeResultset(rs);
		}
	}

	/**
	 * Effettua la insert dopo la creazione su FileNet.
	 * 
	 * @see it.ibm.red.business.dao.IVerificaFirmaDAO#insertDopoCreazioneSuFilenet(java.lang.Long,
	 *      java.lang.String, java.lang.Integer, java.lang.String, java.lang.String,
	 *      java.lang.Integer, java.sql.Connection)
	 */
	@Override
	public boolean insertDopoCreazioneSuFilenet(final Long idAoo, final String documentTitle, final Integer stato, final String objectStore, final String classeDocumentale,
			final Integer versDocumento, final Connection conn) {
		boolean output = false;
		PreparedStatement ps = null;
		try {
			int index=1;
			StringBuilder sb = new StringBuilder();
			if(classeDocumentale !=null && (classeDocumentale.contains("_DocumentoGenerico") || "Allegato_NSD".equals(classeDocumentale))) {
				sb.append("INSERT INTO VERIFICA_FIRMA (IDAOO,STATO,DOC_TITLE,OBJECT_STORE,CLASSE_DOCUMENTALE,VERS_DOCUMENTO,DATA_INSERIMENTO,DATA_PROCESSAMENTO) VALUES(?,?,?,?,?,?,?,?)");

				ps = conn.prepareStatement(sb.toString());
				ps.setInt(index++, idAoo.intValue());
				ps.setInt(index++, stato);
				ps.setInt(index++, Integer.parseInt(documentTitle));
				ps.setString(index++, objectStore);
				ps.setString(index++, classeDocumentale);
				ps.setInt(index++, versDocumento);
				//Data inserimento
				ps.setTimestamp(index++, new Timestamp((new Date()).getTime()));
				//Data processamento
				ps.setTimestamp(index++, new Timestamp((new Date()).getTime()));
				output = ps.executeUpdate() > 0;
			}  
		} catch (final Exception ex) {
			LOGGER.error(ERROR_INSERT_CREAZIONE_FILENET_MSG + ex);
			throw new RedException(ERROR_INSERT_CREAZIONE_FILENET_MSG + ex);
		} finally {
			closeStatement(ps);
		}
		return output;
	}

	/**
	 * Inserisce l'errore nel log.
	 * 
	 * @see it.ibm.red.business.dao.IVerificaFirmaDAO#insertLogError(java.lang.Long,
	 *      java.lang.Integer, java.lang.Integer, java.lang.String,
	 *      java.lang.Integer, java.sql.Connection)
	 */
	@Override
	public boolean insertLogError(final Long idAoo, final Integer stato, final Integer docTitle, final String eccezione, final Integer versione, final Connection conn) {
		PreparedStatement ps = null;
		boolean output = false;
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append("INSERT INTO VERIFICA_FIRMA (IDAOO,STATO,DOC_TITLE,ECCEZIONE,VERS_DOCUMENTO) VALUES(?,?,?,?,?)");

			ps = conn.prepareStatement(sb.toString());

			ps.setInt(index++, idAoo.intValue());
			ps.setInt(index++, stato);
			ps.setInt(index++, docTitle);
			ps.setString(index++, eccezione);
			ps.setInt(index++, versione);
			output = ps.executeUpdate() > 0;
		} catch (final Exception ex) {
			LOGGER.error(ERROR_INSERT_CREAZIONE_FILENET_MSG + ex);
			throw new RedException(ERROR_INSERT_CREAZIONE_FILENET_MSG + ex);
		} finally {
			closeStatement(ps);
		}
		
		return output;
	}

	/**
	 * Restituisce i documenti da verificare.
	 * 
	 * @see it.ibm.red.business.dao.IVerificaFirmaDAO#getDocumentToVerify(java.sql.Connection,
	 *      int, int, java.lang.Integer).
	 */
	@Override
	public List<VerificaFirmaItem> getDocumentToVerify(final Connection connection, final int idAoo, final int maxResults,final Integer verificaFirmaMinutiDelay) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<VerificaFirmaItem> items = new ArrayList<>();
		SignSRV.VerificaFirmaItem item = null;
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM VERIFICA_FIRMA WHERE IDAOO = ? AND STATO = ? AND ROWNUM < ? ");

			if(verificaFirmaMinutiDelay!=null) { 
				  sb.append(" AND  DATA_INSERIMENTO <  (SYSTIMESTAMP  -  INTERVAL '" + verificaFirmaMinutiDelay + "' MINUTE )"); 
			}
			
			sb.append("ORDER BY VERS_DOCUMENTO DESC");
			ps = connection.prepareCall(sb.toString());

			ps.setInt(index++, idAoo);
			ps.setInt(index++, StatoVerificaFirmaEnum.DA_LAVORARE.getStatoVerificaFirma());
			ps.setInt(index++, maxResults);

			  
			rs = ps.executeQuery();

			while (rs.next()) {
				item = new VerificaFirmaItem(rs.getInt("IDELEM"), rs.getString("CLASSE_DOCUMENTALE"), String.valueOf(rs.getInt("DOC_TITLE")), rs.getInt("VERS_DOCUMENTO"));
				items.add(item);
			}
			return items;
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero dei documenti la cui firma è da verificare per idAoo = " + idAoo + ". " + e.getMessage(), e);
			throw new RedException("Errore durante il recupero dei documenti la cui firma è da verificare per idAoo = " + idAoo + ". " + e.getMessage());
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IVerificaFirmaDAO#updateStatoDocumentTitleVerificato(java.util.List,
	 *      java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public boolean updateStatoDocumentTitleVerificato(final List<Integer> idDocRecuperati, final Integer stato, final Connection conn) {
		boolean output = false;
		PreparedStatement ps = null;
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append("UPDATE VERIFICA_FIRMA SET STATO = ?, DATA_PROCESSAMENTO = ? WHERE ");
			sb.append(StringUtils.createInCondition("IDELEM", idDocRecuperati.iterator(), false));

			ps = conn.prepareStatement(sb.toString());

			ps.setInt(index++, stato); 
			//Data processamento
			ps.setTimestamp(index++, new Timestamp((new Date()).getTime()));

			output = ps.executeUpdate() > 0;
		} catch (final Exception ex) {
			LOGGER.error("Errore nell'aggiornamento dello stato del doc title verificato : " + ex);
			throw new RedException("Errore nell'aggiornamento dello stato del doc title verificato : " + ex);
		} finally {
			closeStatement(ps);
		}
		return output;
	}
	
	/**
	 * @see it.ibm.red.business.dao.IVerificaFirmaDAO#deleteSingoloElemento(java.lang.Long,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean deleteSingoloElemento(final Long idAoo, final String documentTitle, final String objectStore, final String classeDocumentale, final Integer versDocumento, final Connection conn) {
		boolean output = false;
		PreparedStatement ps = null;
		try {
			int index=1;
			final StringBuilder sb = new StringBuilder();
			if(classeDocumentale !=null && (classeDocumentale.contains("_DocumentoGenerico") || "Allegato_NSD".equals(classeDocumentale))) {
				sb.append("DELETE FROM VERIFICA_FIRMA WHERE IDAOO = ? AND DOC_TITLE = ? AND OBJECT_STORE = ? AND CLASSE_DOCUMENTALE = ? AND VERS_DOCUMENTO = ?");
				
				ps = conn.prepareStatement(sb.toString());
				ps.setInt(index++, idAoo.intValue());
				ps.setInt(index++, Integer.parseInt(documentTitle));
				ps.setString(index++, objectStore);
				ps.setString(index++, classeDocumentale);
				ps.setInt(index++, versDocumento);
				output = ps.executeUpdate()>0;
			}
		} catch (Exception ex) {
			LOGGER.error(ERROR_DELETE_CREAZIONE_FILENET_MSG+ex);
			throw new RedException(ERROR_DELETE_CREAZIONE_FILENET_MSG+ex);
		} finally {
			closeStatement(ps);
		}
		return output;
	}
}
