package it.ibm.red.business.dto;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.exception.ExceptionUtils;

import it.ibm.red.business.service.asign.step.StepEnum;

/**
 * DTO per la gestione degli step degli item di firma asincrona.
 */
public class ASignStepResultDTO extends AbstractDTO {

	/**
	 * La costante serial version UID.
	 */
	private static final long serialVersionUID = 7450922045097587306L;

	/**
	 * Data iniziale step.
	 */
	private Date start;
	
	/**
	 * Tipo step.
	 */
	private StepEnum step;
	
	/**
	 * Data finale step.
	 */
	private Date stop;
	
	/**
	 * Status dello step.
	 */
	private Boolean status;
	
	/**
	 * StringBuilder per la gestione del log legato allo step.
	 */
	private StringBuilder sb;
	
	/**
	 * Id item a cui è associato lo step.
	 */
	private Long idItem;
	
	/**
	 * Costruttore.
	 * @param idItem
	 * @param step
	 */
	public ASignStepResultDTO(Long idItem, StepEnum step) {
		start = new Date();
		this.step = step;
		sb = new StringBuilder("");
		this.idItem = idItem;
	}

	/**
	 * Costruttore con log.
	 * @param idItem
	 * @param step
	 * @param log
	 */
	public ASignStepResultDTO(Long idItem, StepEnum step, String log) {
		this(idItem, step);
		sb.append(log);
	}

	/**
	 * Costruttore completo.
	 * @param idItem
	 * @param step
	 * @param log
	 * @param inStart
	 * @param inStop
	 */
	public ASignStepResultDTO(Long idItem, StepEnum step, String log, Date inStart, Date inStop) {
		this(idItem, step, log);
		start = inStart; 
		stop = inStop;
	}

	/**
	 * Consente di aggiornare il log.
	 * @param obj
	 */
	private void appendObj(Object obj) {
		if (obj != null) {
			String time = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss").format(new Date());
			sb.append("[" + time + "] " + obj.toString());
			sb.append(System.lineSeparator());
		}
	}

	/**
	 * Consente di aggiornare il log con un exception.
	 * @param e
	 */
	public void appendException(Exception e) {
		appendObj(ExceptionUtils.getStackTrace(e));
	}
	
	/**
	 * Consente di aggiornare il log con un messaggio aggiuntivo.
	 * @param str
	 */
	public void appendMessage(String str) {
		appendObj(str);
	}

	/**
	 * Imposta la data di stop dello step.
	 * @param stop
	 */
	public void setStop(Date stop) {
		this.stop = stop;
	}
	
	/**
	 * Restituisce il log associato allo step.
	 * @return log step
	 */
	public String getLog() {
		return sb.toString();
	}

	/**
	 * Restituisce l'enum che definisce lo step.
	 * @return enum dello step
	 */
	public StepEnum getStep() {
		return step;
	}

	/**
	 * Restituisce la data iniziale dello step.
	 * @return data inserimento step
	 */
	public Date getStart() {
		return start;
	}

	/**
	 * Restituisce la data di chiusura dello step.
	 * @return data fine lavorazione step
	 */
	public Date getStop() {
		return stop;
	}

	/**
	 * Restituisce lo status dello step.
	 * @return status step
	 */
	public Boolean getStatus() {
		return status;
	}

	/**
	 * Imposta lo status dello step.
	 * @param status
	 */
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	/**
	 * Restituisce l'id dell'item a cui fa riferimento lo step.
	 * @return id item riferimento step
	 */
	public Long getIdItem() {
		return idItem;
	}

}
