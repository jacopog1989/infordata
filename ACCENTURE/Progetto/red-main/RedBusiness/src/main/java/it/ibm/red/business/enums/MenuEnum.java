package it.ibm.red.business.enums;

/**
 * The Enum MenuEnum.
 *
 * @author CPIERASC
 * 
 *         Enum voci menu.
 */
public enum MenuEnum {
	
	/**
	 * Homepage.
	 */
	HOMEPAGE(-1),
	
	/**
	 * Menu libro firma complessivo.
	 */
	LIBRO_FIRMA(3),
	
	/**
	 * Menu coda corriere.
	 */
	CODA_CORRIERE(4),
	
	/**
	 * Menu coda da lavorare.
	 */
	DA_LAVORARE(5),
	
	/**
	 * Menu scadenzario.
	 */
	SCADENZARIO(7),
	
	/**
	 * Menu ricerca generica.
	 */
	RICERCA(301),
	
	/**
	 * Menu libro firma operazione di firma.
	 */
	LIBRO_FIRMA_FIRMA(101),
	
	/**
	 * Menu libro firma operazione vista.
	 */
	LIBRO_FIRMA_VISTA(102),
	
	/**
	 * Menu libro firma operazione di sigla.
	 */
	LIBRO_FIRMA_SIGLA(103),
	
	/**
	 * Menu scadenzario eventi.
	 */
	SCADENZARIO_EVENTI(201),
	
	/**
	 * Menu scadenzario documenti.
	 */
	SCADENZARIO_DOCUMENTI(202),
	
	/**
	 * Menu mail generica.
	 */
	MAIL(401);

	/**
	 * Valore.
	 */
	private Integer value;
	
	/**
	 * Costruttore.
	 * 
	 * @param inValue	valore
	 */
	MenuEnum(final Integer inValue) {
		value = inValue;
	}

	/**
	 * Getter valore.
	 * 
	 * @return	valore
	 */
	public Integer getValue() {
		return value;
	}
	
	/**
	 * Recupero enum.
	 * 
	 * @param activeToken	token di navigazione
	 * @return				enum
	 */
	public static MenuEnum get(final NavigationTokenMobileEnum activeToken) {
		MenuEnum output = null;
		for (MenuEnum me:MenuEnum.values()) {
			if (me.getValue().equals(activeToken.getId())) {
				output = me;
			}
		}
		return output;
	}
	
	/**
	 * Recupero enum.
	 * 
	 * @param activeToken		token di navigazione
	 * @param filter			filtro libro firma
	 * @param filterScheduler	filtro scheduler	
	 * @return					enum
	 */
	public static MenuEnum get(final NavigationTokenMobileEnum activeToken, final TipoOperazioneLibroFirmaEnum filter, final TipoAzioneEnum filterScheduler) {
		MenuEnum output = get(activeToken);
		if (filter == null && filterScheduler == null) {
			//HOMEPAGE
			output = HOMEPAGE;
		} else if (TipoOperazioneLibroFirmaEnum.RICERCA.equals(filter)) {
			output = RICERCA;
		} else if (NavigationTokenMobileEnum.LIBRO_FIRMA.equals(activeToken)) {
			if (TipoOperazioneLibroFirmaEnum.FIRMA.equals(filter)) {
				output = LIBRO_FIRMA_FIRMA;
			} else if (TipoOperazioneLibroFirmaEnum.VISTA.equals(filter)) {
				output = LIBRO_FIRMA_VISTA;
			} else if (TipoOperazioneLibroFirmaEnum.SIGLA.equals(filter)) {
				output = LIBRO_FIRMA_SIGLA;
			}
		} else if (NavigationTokenMobileEnum.SCADENZARIO.equals(activeToken)) {
			if (TipoAzioneEnum.EVENTO.equals(filterScheduler)) {
				output = SCADENZARIO_EVENTI;
			} else if (TipoAzioneEnum.SCADENZA_DOCUMENTO.equals(filterScheduler)) {
				output = SCADENZARIO_DOCUMENTI;
			}
		}
		return output;
	}

}
