package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import filenet.vw.api.VWQueueQuery;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dao.IAllaccioDAO;
import it.ibm.red.business.dao.ITimerRichiesteIntegrazioniDAO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.TimerRichiesteIntegrazioniDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.FlagLavoratoEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.INotificaMessaInLavorazioneAutomaticaSRV;
import it.ibm.red.business.service.ITimerRichiesteIntegrazioniSRV;

/**
 * Service che gestisce il timer delel richieste integrazioni.
 */
@Service
@Component
public class TimerRichiesteIntegrazioniSRV extends AbstractService implements ITimerRichiesteIntegrazioniSRV {

	/**
	 * 	serial version UID.
	 */
	private static final long serialVersionUID = 6305695546208041594L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TimerRichiesteIntegrazioniSRV.class.getName());

	/**
	 * DAO.
	 */
	@Autowired
	private ITimerRichiesteIntegrazioniDAO timerDAO;
	
	/**
	 * DAO.
	 */
	@Autowired
	private IAllaccioDAO allaccioDAO;
	
	/**
	 * Fornitore della properties dell'applicazione.
	 */
	private PropertiesProvider pp;

	/**
	 * Service.
	 */
	@Autowired
	private IAooSRV aooSRV;

	/**
	 * Service.
	 */
	@Autowired
	private INotificaMessaInLavorazioneAutomaticaSRV notificaMessaInLavorazioneAutomaticaSRV;

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.service.ITimerRichiesteIntegrazioniSRV#registraDataInvio(java.lang.String,
	 *      long, it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public void registraDataInvio(final String idDocumento, final long idAoo, final IFilenetCEHelper fceh, final Connection con) {
		//recupero doc in uscita
		final Document doc = fceh.getDocumentByDTandAOO(idDocumento + "", idAoo);
		if (doc.getProperties().isPropertyPresent(pp.getParameterByKey(PropertiesNameEnum.SOTTOCATEGORIA_DOCUMENTO_USCITA))
				&& SottoCategoriaDocumentoUscitaEnum.RICHIESTA_INTEGRAZIONI.getId().equals(
						doc.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.SOTTOCATEGORIA_DOCUMENTO_USCITA)))) {
			
			Connection connection = null;
			try {
				if (con == null) {
					connection = setupConnection(getDataSource().getConnection(), true);
				} else {
					connection = con;
				}

				final Collection<RispostaAllaccioDTO> allacci = allaccioDAO.getDocumentiRispostaAllaccio(Integer.parseInt(idDocumento), Integer.parseInt("" + idAoo), connection);
				if (!allacci.isEmpty()) {
					//seleziono allaccio principale
					RispostaAllaccioDTO allaccioPrincipale = null;
					for (final RispostaAllaccioDTO a : allacci) {
						if (a.isPrincipale()) {
							allaccioPrincipale = a;
							break;
						}
					}
					//se non esiste, lancio eccezione
					if (allaccioPrincipale == null) {
						throw new RedException("Allaccio principale inesistente.");
					}
					final String idDocumentoAllaccio = "" + allaccioPrincipale.getIdDocumentoAllacciato();
					
					//recupero workflow principale documento
					final Aoo aoo = aooSRV.recuperaAoo(Integer.parseInt("" + idAoo), con);
					final AooFilenet aooFilenet = aoo.getAooFilenet();
					final FilenetPEHelper fpeh = new FilenetPEHelper(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(),
							aooFilenet.getStanzaJaas(), aooFilenet.getConnectionPoint());
					final VWWorkObject wo = fpeh.getWorkflowPrincipale(idDocumentoAllaccio, aooFilenet.getIdClientAoo());
					
					final Integer idNodoDestinatario = (Integer) TrasformerPE.getMetadato(wo, PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY);
					final Integer idUtenteDestinatario = (Integer) TrasformerPE.getMetadato(wo, PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY);
					
					if (idNodoDestinatario != null && idUtenteDestinatario != null && idNodoDestinatario != 0 && idUtenteDestinatario != 0) {
						final Timestamp timestamp = new Timestamp(System.currentTimeMillis());
						
						registraTimestamp(Long.parseLong(idDocumentoAllaccio), idNodoDestinatario, idUtenteDestinatario, timestamp, FlagLavoratoEnum.DA_LAVORARE.getId());
					}
				}
				
				if (con == null) {
					commitConnection(connection);
				}
			} catch (final Exception e) {
				LOGGER.error("Errore nel tentativo di registrazione del timestamp.", e);
				if (con == null) {
					rollbackConnection(connection);
				}
				throw new RedException(e);
			} finally {
				if (con == null) {
					closeConnection(connection);
				}	
			}
		}
		
	}
	
	private void registraTimestamp(final long idDocumento, final long idNodo, final long idUtente, final Timestamp timestamp, final int flagLavorato) {
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			
			timerDAO.registraTimestamp(idDocumento, idNodo, idUtente, timestamp, flagLavorato, conn);
			
		} catch (final Exception e) {
			LOGGER.error("Errore durante la registrazione del timestamp", e);
			throw new RedException(e);
		} finally {
			closeConnection(conn);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITimerRichiesteIntegrazioniFacadeSRV#getNextDaLavorare().
	 */
	@Override
	public Collection<TimerRichiesteIntegrazioniDTO> getNextDaLavorare() {
		Connection connection = null;
		Collection<TimerRichiesteIntegrazioniDTO> itemsToProcess = new ArrayList<>();
		try {
			
			connection = setupConnection(getDataSource().getConnection(), true);
			
			// Recupera la richiesta
			itemsToProcess = timerDAO.getAndLockNextDaLavorare(connection);
			
			// Si aggiorna lo stato della richiesta in coda impostando la presa in carico
			for (final TimerRichiesteIntegrazioniDTO item : itemsToProcess) {
				timerDAO.aggiornaFlagLavorato(item.getIdTimer(), FlagLavoratoEnum.IN_ELABORAZIONE.getId(), connection);
			}

			commitConnection(connection);
			
		} catch (final Exception e) {
			rollbackConnection(connection);
			LOGGER.error("Errore durante il recupero della prima richiesta di elaborazione", e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
		
		return itemsToProcess;
	}

	private void aggiornaFlagLavorato(final long idTimer, final FlagLavoratoEnum flagLavorato) {
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), true);
			
			Integer flagLav = FlagLavoratoEnum.IN_ERRORE.getId();
			if (flagLavorato != null) {
				flagLav = flagLavorato.getId();
			}
			timerDAO.aggiornaFlagLavorato(idTimer, flagLav, con);
			
			commitConnection(con);
		} catch (final Exception e) {
			rollbackConnection(con);
			LOGGER.error("Errore durante l'aggiornamento dello stato lavorazione del documento", e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
	}

	private boolean checkTimerConfigurazione(final IFilenetCEHelper fceh, final long idAoo, final long idDocumento, final Timestamp ts) {
		
		final Document doc = fceh.getDocumentByDTandAOO("" + idDocumento, idAoo);
		
		final Integer idTipoDocumento = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY);
		final Integer idTipoProcedimento = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY);
		
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			
			final long tempoConf = timerDAO.getTimerConfigurazione(idTipoDocumento, idTipoProcedimento, con);
			
			//recupero il timestamp corrente e calcolo la differenza di minuti da questo e quello in tabella
			final Timestamp oggi = new Timestamp(System.currentTimeMillis());
			final long minDiff = TimeUnit.MILLISECONDS.toMinutes(oggi.getTime() - ts.getTime());
			
			return minDiff > tempoConf;
			
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'aggiornamento dello stato lavorazione del documento", e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITimerRichiesteIntegrazioniFacadeSRV#elaboraItem(it.ibm.red.business.dto.TimerRichiesteIntegrazioniDTO,
	 *      long).
	 */
	@Override
	public void elaboraItem(final TimerRichiesteIntegrazioniDTO item, final long idAoo) {
		FlagLavoratoEnum enumLavorato = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		try {
			final Aoo aoo = aooSRV.recuperaAoo(Integer.parseInt("" + idAoo));
			final AooFilenet aooFilenet = aoo.getAooFilenet();
			
			fpeh = new FilenetPEHelper(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(),
					aooFilenet.getStanzaJaas(), aooFilenet.getConnectionPoint());
			fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(),
					aooFilenet.getStanzaJaas(), aooFilenet.getObjectStore(), aoo.getIdAoo());
			//recupero wf coda "in sospeso" utente in tabella
			final Collection<String> documentTitles = new ArrayList<>();
			documentTitles.add(item.getIdDocumento() + "");
			
			final VWQueueQuery vwqqSospeso = fpeh.getWorkFlowsByWobQueueFilterRed(null, DocumentQueueEnum.SOSPESO, null, item.getIdNodo(), 
					Arrays.asList(item.getIdUtente()), aooFilenet.getIdClientAoo(), null, null, null, documentTitles, null, null, null, false);

			if (vwqqSospeso == null || !vwqqSospeso.hasNext()) {
				//se il documento non è più in sospeso, il job non deve far altro che marcare l'item come OBSOLETO
				enumLavorato = FlagLavoratoEnum.OBSOLETO;
				
			} else {
			
				//controllo se il tempo configurato per la coppia tipo documento/ tipo procedimento
				//sia stato superato a partire dal timestamp della data invio del documento
				final boolean scaduto = checkTimerConfigurazione(fceh, idAoo, item.getIdDocumento(), item.getTimestamp());
				
				if (scaduto) {
				
					//sollecito la respose per spostare il documento in lavorazione
					VWWorkObject wo = null;
					if (vwqqSospeso.hasNext()) {
			           wo = (VWWorkObject) vwqqSospeso.next();
					}
					fpeh.nextStep(wo, null, ResponsesRedEnum.SPOSTA_IN_LAVORAZIONE.getResponse());
					
					//aggiorno flag
					enumLavorato = FlagLavoratoEnum.LAVORATO;
					
					notificaMessaInLavorazioneAutomaticaSRV.writeNotifiche((int) idAoo, item);
					
				} else {
					enumLavorato = FlagLavoratoEnum.DA_LAVORARE;
				}
			}
			
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'elaborazione dell'item", e);
			throw new RedException(e);
		} finally {
			aggiornaFlagLavorato(item.getIdTimer(), enumLavorato);
			logoff(fpeh);
			popSubject(fceh);
		}
		
	}
}
