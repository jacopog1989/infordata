package it.ibm.red.business.enums;

/**
 * The Enum AccessPermissionsEnum.
 *
 * @author CPIERASC
 * 
 *         Enum per specificare le tipologie di accesso consentite ad un pdf in
 *         seguito all'apposizione di una firma pades.
 */
public enum AccessPermissionsEnum {	

	/**
	 * Nessun restrizione.
	 */
	NO_RESTRICTION(0),
	
	/**
	 * Nessuna modifica permessa.
	 */
	NO_CHANGED_PERMITTED(1),
	
	/**
	 *	filling in forms, instantiating page templates, and signing permitted, other no.
	 */
	PERMITTED_CHANGES_FORMS_TEMPLATES_SIGNING(2), 
	
	/**
	 *  2+annotation creation, deletion, and modification other invalidate.
	 */
	PERMITTED_CHANGES_FORMS_TEMPLATES_SIGNING_ANNOTATION_CREATION_DELETION_MODIFICATION(3);
	
	/**
	 * Valore.
	 */
	private Integer value;

	/**
	 * Costruttore.
	 * 
	 * @param inValue	valore
	 */
	AccessPermissionsEnum(final int inValue) {
		this.value = inValue;
	}

	/**
	 * Getter valore.
	 * 
	 * @return	valore
	 */
	public Integer getValue() {
		return value;
	}

}
