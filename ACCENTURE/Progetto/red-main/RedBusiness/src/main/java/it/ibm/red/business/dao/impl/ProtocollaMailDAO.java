package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IProtocollaMailDAO;
import it.ibm.red.business.dto.DestinatarioEsternoDTO;
import it.ibm.red.business.dto.ProtocollaMailAooConfDTO;
import it.ibm.red.business.dto.ProtocollaMailCodaDTO;
import it.ibm.red.business.enums.MailOperazioniEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.ProtocollaMailStatiEnum;
import it.ibm.red.business.enums.TipologiaDestinatariEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * 
 * Dao per la gestione della coda inoltro rifiuto.
 * 
 * @author adilegge
 *
 */
@Repository
public class ProtocollaMailDAO extends AbstractDAO implements IProtocollaMailDAO {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -4497836567142644770L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ProtocollaMailDAO.class.getName());
	
	/**
	 * @see it.ibm.red.business.dao.IProtocollaMailDAO#getConfigurazione(java.lang.Long,
	 *      it.ibm.red.business.enums.MailOperazioniEnum, java.sql.Connection).
	 */
	@Override
	public ProtocollaMailAooConfDTO getConfigurazione(final Long idAoo, final MailOperazioniEnum operazione, final Connection conn) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ProtocollaMailAooConfDTO item = null;
		try {
			final String querySQL = "SELECT * FROM PROTOCOLLAMAIL_AOO_CONF WHERE IDAOO = ? AND IDOPERAZIONE = ?";
			ps = conn.prepareStatement(querySQL);
			ps.setLong(1, idAoo);
			ps.setLong(2, operazione.getId());
			rs = ps.executeQuery();
			if (rs.next()) {
				item = populateItemConf(rs);
			}
			
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero delle configurazioni della coda ProtocollaMail per l'operazione " + operazione + " dell'aoo " + idAoo, e);
			throw new RedException("Errore durante il recupero delle configurazioni della coda ProtocollaMail", e);
		} finally {
			closeStatement(ps, rs);
		}
		return item;
	}

	private static ProtocollaMailAooConfDTO populateItemConf(final ResultSet rs) throws SQLException {
		return new ProtocollaMailAooConfDTO(rs.getLong("IDAOO"), MailOperazioniEnum.get(rs.getInt("IDOPERAZIONE")), 
				rs.getLong("IDUTENTERESP"), rs.getLong("IDNODORESP"), rs.getLong("IDRUOLORESP"), rs.getString("INDICECLASSIFICAZIONE"), 
				rs.getString("GUID_TEMPLATE"), rs.getString("NOME_TEMPLATE"), rs.getString("OGGETTO_TEMPLATE"));
	}

	/**
	 * @see it.ibm.red.business.dao.IProtocollaMailDAO#getFirstInCodaLocked(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public ProtocollaMailCodaDTO getFirstInCodaLocked(final Long idAoo, final Connection conn) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ProtocollaMailCodaDTO item = null;
		boolean itemTrovato = false;
		int index = 1;
		try {
			String querySQL = "SELECT * FROM PROTOCOLLAMAIL_CODA WHERE IDAOO = ? AND IDSTATO = ? ORDER BY IDCODA FOR UPDATE SKIP LOCKED";
			
			ps = conn.prepareStatement(querySQL);
			ps.setLong(index++, idAoo);
			ps.setLong(index++, ProtocollaMailStatiEnum.DA_ELABORARE.getId());
			ps.setMaxRows(1);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				itemTrovato = true;
				item = populateItemCoda(rs);
			}
			
			final List<DestinatarioEsternoDTO> contattiDestinatari = new ArrayList<>();
			if (itemTrovato) {
				closeStatement(ps, rs);
				
				querySQL = "SELECT * FROM PROTOCOLLAMAIL_CODA_DEST WHERE IDCODA = ?";
				ps = conn.prepareStatement(querySQL);
				ps.setLong(1, item.getIdCoda());
				
				rs = ps.executeQuery();
				
				while (rs.next()) {
					contattiDestinatari.add(new DestinatarioEsternoDTO(rs.getLong("DESTINATARIO"), TipologiaDestinatariEnum.getById(rs.getInt("TIPODESTINATARIO")), 
							ModalitaDestinatarioEnum.getModalita(rs.getString("MODALITADESTINATARIO"))));
				}
				
				item.setContattiDestinatari(contattiDestinatari);
			}
			
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero del primo item della coda ProtocollaMail per l'Aoo " + idAoo, e);
			throw new RedException("Errore durante il recupero del primo item della coda ProtocollaMail per l'Aoo " + idAoo, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return item;
	}

	private static ProtocollaMailCodaDTO populateItemCoda(final ResultSet rs) throws SQLException {
		final boolean mantieniAllegatiOriginali = rs.getInt("MANTIENIALLEGATIORIGINALI") == 1;
		return new ProtocollaMailCodaDTO(rs.getLong("IDCODA"), rs.getString("GUID_MAIL"), 
				rs.getLong("IDAOO"), MailOperazioniEnum.get(rs.getInt("IDOPERAZIONE")), 
				rs.getLong("IDUTENTEOPERAZIONE"), rs.getLong("IDRUOLOOPERAZIONE"), rs.getLong("IDNODOOPERAZIONE"), rs.getLong("MITTENTE"), 
				rs.getString("TESTOTEMPLATE"), mantieniAllegatiOriginali, rs.getString("CASELLA"), ProtocollaMailStatiEnum.get(rs.getInt("IDSTATO")), rs.getString("DESCRIZIONE_ERRORE"), 
				rs.getDate("TS_ULTIMA_OPERAZIONE"), rs.getString("IDDOCENTRATA"), rs.getString("WOBNUMBERENTRATA"), rs.getString("IDDOCUSCITA"), rs.getString("WOBNUMBERUSCITA"));
	}

	/**
	 * @see it.ibm.red.business.dao.IProtocollaMailDAO#updateCoda(java.lang.Long,
	 *      java.lang.Integer, java.lang.String, java.lang.String, java.lang.String,
	 *      java.lang.String, java.lang.String, java.sql.Connection).
	 */
	@Override
	public void updateCoda(final Long idCoda, final Integer idStato, final String descrizioneErrore, final String idDocumentoEntrata, final String wobNumberEntrata,
			final String idDocumentoUscita, final String wobNumberUscita, final Connection conn) {
		
		PreparedStatement ps = null;
		try {
			
			final StringBuilder updateQuery = new StringBuilder("UPDATE PROTOCOLLAMAIL_CODA SET IDSTATO = ?, DESCRIZIONE_ERRORE = ?, IDDOCENTRATA = ?, WOBNUMBERENTRATA = ?,");
			updateQuery.append("IDDOCUSCITA = ?, WOBNUMBERUSCITA = ?, TS_ULTIMA_OPERAZIONE = sysdate WHERE IDCODA = ? ");
									
			ps = conn.prepareStatement(updateQuery.toString());
			ps.setInt(1, idStato);
			if (StringUtils.isEmpty(descrizioneErrore)) {
				ps.setNull(2, Types.VARCHAR);
			} else {
				ps.setString(2, descrizioneErrore);
			}
			if (StringUtils.isEmpty(idDocumentoEntrata)) {
				ps.setNull(3, Types.VARCHAR);
			} else {
				ps.setString(3, idDocumentoEntrata);
			}
			if (StringUtils.isEmpty(wobNumberEntrata)) {
				ps.setNull(4, Types.VARCHAR);
			} else {
				ps.setString(4, wobNumberEntrata);
			}
			if (StringUtils.isEmpty(idDocumentoUscita)) {
				ps.setNull(5, Types.VARCHAR);
			} else {
				ps.setString(5, idDocumentoUscita);
			}
			if (StringUtils.isEmpty(wobNumberUscita)) {
				ps.setNull(6, Types.VARCHAR);
			} else {
				ps.setString(6, wobNumberUscita);
			}
			ps.setLong(7, idCoda);
			
			ps.executeUpdate();
		
		} catch (final SQLException e) {
			LOGGER.error("Errore in fase di aggiornamento dell'item " + idCoda, e);
			throw new RedException("Errore in fase di aggiornamento dell'item " + idCoda, e);
		} finally {
			closeStatement(ps, null);
		}
		
	}

	/**
	 * @see it.ibm.red.business.dao.IProtocollaMailDAO#inserisciInCoda(it.ibm.red.business.dto.ProtocollaMailCodaDTO,
	 *      java.sql.Connection).
	 */
	@Override
	public void inserisciInCoda(final ProtocollaMailCodaDTO item, final Connection conn) {
		PreparedStatement ps = null;
		int idCoda = 0;
		
		try {
			idCoda = (int) getNextId(conn, "SEQ_PROTOCOLLAMAIL_CODA");
			
			ps = conn.prepareStatement("INSERT INTO PROTOCOLLAMAIL_CODA (IDCODA, GUID_MAIL, IDAOO, IDOPERAZIONE, IDUTENTEOPERAZIONE, IDNODOOPERAZIONE, IDRUOLOOPERAZIONE, "
					+ "MITTENTE, TESTOTEMPLATE, MANTIENIALLEGATIORIGINALI, CASELLA, IDSTATO, DESCRIZIONE_ERRORE, TS_ULTIMA_OPERAZIONE, IDDOCENTRATA, WOBNUMBERENTRATA, IDDOCUSCITA, WOBNUMBERUSCITA) "
					+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, null, SYSDATE, null, null, null, null)");
			
			ps.setInt(1, idCoda);
			ps.setString(2, item.getGuidMail());
			ps.setLong(3, item.getIdAoo());
			ps.setInt(4, item.getOperazione().getId());
			ps.setLong(5, item.getIdUtenteOperazione());
			ps.setLong(6, item.getIdNodoOperazione());
			ps.setLong(7, item.getIdRuoloOperazione());
			ps.setLong(8, item.getIdContattoMittente());
			ps.setString(9, item.getTestoTemplate());
			ps.setInt(10, item.isMantieniAllegatiOriginali() ? 1 : 0);
			ps.setString(11, item.getCasella());
			ps.setInt(12, ProtocollaMailStatiEnum.DA_ELABORARE.getId());
			ps.executeUpdate();
			ps.close();
			
			ps = conn.prepareStatement("INSERT INTO PROTOCOLLAMAIL_CODA_DEST (IDCODA, DESTINATARIO, TIPODESTINATARIO, MODALITADESTINATARIO) VALUES (?, ?, ?, ?) ");
			for (final DestinatarioEsternoDTO d: item.getContattiDestinatari()) {
				ps.setInt(1, idCoda);
				ps.setLong(2, d.getIdDestinatario());
				ps.setInt(3, d.getTipoSpedizioneDestinatario().getId());
				ps.setString(4, d.getModalitaDestinatario().getLabel());
				
				ps.executeUpdate();
			}
			
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di inserimento dell'item in coda", e);
			throw new RedException("Errore in fase di inserimento dell'item in coda", e);
		} finally {
			closeStatement(ps);
		}
		
	}
	
	/**
	 * @see it.ibm.red.business.dao.IProtocollaMailDAO#existItemsToProcess(java.lang.String,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public boolean existItemsToProcess(final String guidMail, final Long idAoo, final Connection conn) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int numItem = 0;
		int index = 1;
		try {
		
			final String querySQL = "SELECT COUNT(*) NUM_ITEM FROM PROTOCOLLAMAIL_CODA WHERE GUID_MAIL = ? AND IDAOO = ? AND IDSTATO <= ?";
			ps = conn.prepareStatement(querySQL);
			ps.setString(index++, guidMail);
			ps.setLong(index++, idAoo);
			ps.setLong(index++, ProtocollaMailStatiEnum.PRESO_IN_CARICO.getId());
			
			rs = ps.executeQuery();
			if (rs.next()) {
				numItem = rs.getInt("NUM_ITEM");
			}
			
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero delL'item della coda ProtocollaMail relativo al guid mail " + guidMail + " per l'Aoo " + idAoo, e);
			throw new RedException("Errore durante il recupero delL'item della coda ProtocollaMail relativo al guid mail " + guidMail + " per l'Aoo " + idAoo, e);
		} finally {
			closeStatement(ps, rs);
		}
		return numItem > 0;
	}

}