package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.Date;

import it.ibm.red.business.enums.IntervalloDisabilitazioneEnum;
import it.ibm.red.business.persistence.model.DisabilitazioneNotificheUtente;

/**
 * DTO disabilitazione notifiche utente.
 */
public class DisabilitazioneNotificheUtenteDTO implements Serializable {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 3336943470658132007L;

	/**
	 * Identificativo utente.
	 */
	private String idUtente;
	
	/**
	 * Identifiicativo canale.
	 */
	private int idCanaleTrasmissione;

	/**
	 * Data da.
	 */
	private Date dataDa;

	/**
	 * Data a.
	 */
	private Date dataA;
	
	/**
	 * Intervallo.
	 */
	private IntervalloDisabilitazioneDTO intervalloDisabilitazione;

	/**
	 * Costruttore.
	 */
	public DisabilitazioneNotificheUtenteDTO() {
		super();
	}

	/**
	 * @param idUtente
	 * @param idCanaleTrasmissione
	 * @param dataDa
	 * @param dataA
	 * @param intervalloDisabilitazione
	 * @param checked
	 */
	public DisabilitazioneNotificheUtenteDTO(final String idUtente, final int idCanaleTrasmissione, final Date dataDa, final Date dataA,
			final IntervalloDisabilitazioneDTO intervalloDisabilitazione) {
		super();
		this.idUtente = idUtente;
		this.idCanaleTrasmissione = idCanaleTrasmissione;
		setIntervalloDisabilitazione(intervalloDisabilitazione);
		this.dataDa = dataDa;
		this.dataA = dataA;
	}

	/**
	 * @param disabNotificheUtente
	 */
	public DisabilitazioneNotificheUtenteDTO(final DisabilitazioneNotificheUtenteDTO disabNotificheUtente) {
		this(disabNotificheUtente.getIdUtente(), disabNotificheUtente.getIdCanaleTrasmissione(),
				disabNotificheUtente.getDataDa(), disabNotificheUtente.getDataA(),
				disabNotificheUtente.getIntervalloDisabilitazione());
	}

	/**
	 * Costruttore del DTO che parte del Model.
	 * @param dbNotifiche
	 */
	public DisabilitazioneNotificheUtenteDTO(final DisabilitazioneNotificheUtente dbNotifiche) {
		setIdUtente(dbNotifiche.getIdUtente().toString());
		setIdCanaleTrasmissione(dbNotifiche.getIdCanaleTrasmissione());
		setDataDa(dbNotifiche.getDataDa());
		setDataA(dbNotifiche.getDataA());
	}

	/**
	 * @return the idUtente
	 */
	public String getIdUtente() {
		return idUtente;
	}

	/**
	 * @param idUtente
	 *            the idUtente to set
	 */
	public void setIdUtente(final String idUtente) {
		this.idUtente = idUtente;
	}

	/**
	 * @return the idCanaleTrasmissione
	 */
	public int getIdCanaleTrasmissione() {
		return idCanaleTrasmissione;
	}

	/**
	 * @param idCanaleTrasmissione
	 *            the idCanaleTrasmissione to set
	 */
	public void setIdCanaleTrasmissione(final int idCanaleTrasmissione) {
		this.idCanaleTrasmissione = idCanaleTrasmissione;
	}

	/**
	 * @return the dataDa
	 */
	public Date getDataDa() {
		return dataDa;
	}

	/**
	 * @param dataDa
	 *            the dataDa to set
	 */
	public void setDataDa(final Date dataDa) {
		this.dataDa = dataDa;
		calculateIntervallo();
	}

	/**
	 * @return the dataA
	 */
	public Date getDataA() {
		return dataA;
	}

	/**
	 * @param dataA
	 *            the dataA to set
	 */
	public void setDataA(final Date dataA) {
		this.dataA = dataA;
		calculateIntervallo();
	}

	/**
	 * Calcola l'intervallo.
	 */
	protected void calculateIntervallo() {
		this.intervalloDisabilitazione = this.dataA == null && this.dataDa == null
				? IntervalloDisabilitazioneEnum.SEMPRE.getIntervalloDisabilitazioneDTO()
				: IntervalloDisabilitazioneEnum.INTERVALLO.getIntervalloDisabilitazioneDTO();
	}

	/**
	 * @return the intervalloDisabilitazione
	 */
	public IntervalloDisabilitazioneDTO getIntervalloDisabilitazione() {
		return intervalloDisabilitazione;
	}

	/**
	 * @param intervalloDisabilitazione
	 *            the intervalloDisabilitazione to set
	 */
	public void setIntervalloDisabilitazione(final IntervalloDisabilitazioneDTO intervalloDisabilitazione) {
		this.intervalloDisabilitazione = intervalloDisabilitazione;
		if (intervalloDisabilitazione.getId() == IntervalloDisabilitazioneDTO.DISABILITA_SEMPRE) {
			dataDa = null;
			dataA = null;
		}
	}

	/**
	 * @see java.lang.Object#toString().
	 */
	@Override
	public String toString() {
		return "DisabilitazioneNotificheUtente [idUtente=" + idUtente + ", idCanaleTrasmissione=" + idCanaleTrasmissione
				+ ", " + "dataDa=" + dataDa + ", dataA=" + dataA + ", intervalloDisabilitazione="
				+ intervalloDisabilitazione + "]";
	}
}