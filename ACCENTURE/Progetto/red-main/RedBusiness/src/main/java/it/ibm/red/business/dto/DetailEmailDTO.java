package it.ibm.red.business.dto;

import java.util.Collection;
import java.util.List;

import it.ibm.red.business.enums.StatoMailEnum;

/**
 * DTO dettaglio mail.
 * 
 * @author CRISTIANOPierascenzi
 *
 */
public class DetailEmailDTO extends AbstractDTO {

	/**
	 * Serialization.
	 */
	private static final long serialVersionUID = 2621145406329668344L;

	/**
	 * Guid.
	 */
	private String guid;

	/**
	 * Nota.
	 */
	private NotaDTO nota;

	/**
	 * Stato mail.
	 */
	private StatoMailEnum statoMail;

	/**
	 * Stato precedente all'archiviazione.
	 */
	private StatoMailEnum statoPreArchiviazione;

	/**
	 * Testo.
	 */
	private String testo;

	/**
	 * Oggetto.
	 */
	private String oggetto;

	/**
	 * Mittente.
	 */
	private String mittente;

	/**
	 * Destinatario.
	 */
	private String destinatario;

	/**
	 * Destinatario cc.
	 */
	private String destinatarioCC;

	/**
	 * Data arrivo.
	 */
	private String dataArrivo;

	/**
	 * Data invio.
	 */
	private String dataInvio;

	/**
	 * Motivo eliminazione.
	 */
	private String motivoEliminazione;

	/**
	 * Lista allegati.
	 */
	private Collection<MailAttachmentDTO> allegati;

	/**
	 * Identificativo utente protocollatore.
	 */
	private Integer idUtenteProtocollatore;

	/**
	 * Descrizione utente protocollatore.
	 */
	private String descUtenteProtocollatore;

	/**
	 * Flag esito validazione interoperabilità.
	 */
	private String esitoValidazioneInterop;

	/**
	 * Lista errori.
	 */
	private List<ErroreValidazioneDTO> erroriValidazioneInterop;

	/**
	 * Preassegnatario.
	 */
	private AssegnatarioInteropDTO preassegnatarioInterop;

	/**
	 * Preassegnatario per conoscenza.
	 */
	private List<AssegnazioneDTO> preassegnatarioPerConoscenza;

	/**
	 * Data Scarico/Ripristino.
	 */
	private String dataScarico;

	/**
	 * Costruttore.
	 */
	public DetailEmailDTO() {
		super();
	}

	/**
	 * Costruttore.
	 * 
	 * @param inGuid
	 *            guid
	 * @param inOggetto
	 *            oggetto
	 * @param inMittente
	 *            mittente
	 * @param inDestinatario
	 *            destinatario
	 * @param inDestinatarioCC
	 *            destinatario cc
	 * @param inDataArrivo
	 *            data arrivo
	 * @param inTesto
	 *            testo
	 * @param inContentNote
	 *            nota
	 * @param inAllegati
	 *            allegati
	 * @param inMotivoEliminazione
	 *            motivo eliminazione
	 * @param inStatoMail
	 *            stato mail
	 * @param inIdUtenteProtocollatore
	 *            identificativo utente protocollatore
	 * @param inDescUtenteProtocollatore
	 *            descrizione utente protocollatore
	 */
	public DetailEmailDTO(final String inGuid, final String inOggetto, final String inMittente, final String inDestinatario, final String inDestinatarioCC,
			final String inDataArrivo, final String inTesto, final String inContentNote, final List<MailAttachmentDTO> inAllegati, final String inMotivoEliminazione,
			final StatoMailEnum inStatoMail, final Integer inIdUtenteProtocollatore, final String inDescUtenteProtocollatore) {
		super();
		this.oggetto = inOggetto;
		this.testo = inTesto;
		this.mittente = inMittente;
		this.guid = inGuid;
		this.destinatarioCC = inDestinatarioCC;
		this.dataArrivo = inDataArrivo;
		this.destinatario = inDestinatario;
		this.motivoEliminazione = inMotivoEliminazione;
		this.allegati = inAllegati;

		statoMail = inStatoMail;
		idUtenteProtocollatore = inIdUtenteProtocollatore;
		descUtenteProtocollatore = inDescUtenteProtocollatore;
	}

	/**
	 * Costruttore.
	 * 
	 * @param inGuid
	 *            guid
	 * @param inOggetto
	 *            oggetto
	 * @param inMittente
	 *            mittente
	 * @param inDestinatario
	 *            destinatario
	 * @param inDestinatarioCC
	 *            destinatario cc
	 * @param inDataArrivo
	 *            data arrivo
	 * @param inTesto
	 *            testo
	 * @param inContentNote
	 *            nota
	 * @param inAllegati
	 *            allegati
	 * @param inMotivoEliminazione
	 *            motivo eliminazione
	 * @param inStatoMail
	 *            stato mail
	 * @param inIdUtenteProtocollatore
	 *            identificativo utente protocollatore
	 * @param inDescUtenteProtocollatore
	 *            descrizione utente protocollatore
	 */
	public DetailEmailDTO(final String inGuid, final String inOggetto, final String inMittente, final String inDestinatario, final String inDestinatarioCC,
			final String inDataArrivo, final String inTesto, final String inContentNote, final List<MailAttachmentDTO> inAllegati, final String inMotivoEliminazione,
			final StatoMailEnum inStatoMail, final Integer inIdUtenteProtocollatore, final String inDescUtenteProtocollatore, final String inDataInvio) {
		super();
		this.oggetto = inOggetto;
		this.destinatarioCC = inDestinatarioCC;
		this.descUtenteProtocollatore = inDescUtenteProtocollatore;
		this.mittente = inMittente;
		this.dataArrivo = inDataArrivo;
		this.dataInvio = inDataInvio;
		this.statoMail = inStatoMail;
		this.allegati = inAllegati;
		this.guid = inGuid;
		this.testo = inTesto;
		this.destinatario = inDestinatario;
		this.motivoEliminazione = inMotivoEliminazione;
		this.idUtenteProtocollatore = inIdUtenteProtocollatore;
	}

	/**
	 * Costruttore.
	 * 
	 * @param inGuid
	 * @param inOggetto
	 * @param inMittente
	 * @param inDestinatario
	 * @param inDestinatarioCC
	 * @param inDataArrivo
	 * @param inTesto
	 * @param inContentNote
	 * @param inAllegati
	 * @param inMotivoEliminazione
	 * @param inStatoMail
	 * @param inIdUtenteProtocollatore
	 * @param inDescUtenteProtocollatore
	 * @param inDataInvio
	 * @param inNota
	 * @param statoMailPreArch
	 * @param erroriValidazioneInterop
	 */
	public DetailEmailDTO(final String inGuid, final String inOggetto, final String inMittente, final String inDestinatario, final String inDestinatarioCC,
			final String inDataArrivo, final String inTesto, final String inContentNote, final List<MailAttachmentDTO> inAllegati, final String inMotivoEliminazione,
			final StatoMailEnum inStatoMail, final Integer inIdUtenteProtocollatore, final String inDescUtenteProtocollatore, final String inDataInvio, final NotaDTO inNota,
			final StatoMailEnum inStatoMailPreArch, final List<ErroreValidazioneDTO> inErroriValidazioneInterop) {
		this(inGuid, inOggetto, inMittente, inDestinatario, inDestinatarioCC, inDataArrivo, inTesto, inContentNote, inAllegati, inMotivoEliminazione, inStatoMail,
				inIdUtenteProtocollatore, inDescUtenteProtocollatore, inDataInvio);
		this.nota = inNota;
		this.statoPreArchiviazione = inStatoMailPreArch;
		this.erroriValidazioneInterop = inErroriValidazioneInterop;
	}

	/**
	 * Costruttore.
	 * 
	 * @param inGuid
	 * @param inOggetto
	 * @param inMittente
	 * @param inDestinatario
	 * @param inDestinatarioCC
	 * @param inDataArrivo
	 * @param inTesto
	 * @param inContentNote
	 * @param inAllegati
	 * @param inMotivoEliminazione
	 * @param inStatoMail
	 * @param inIdUtenteProtocollatore
	 * @param inDescUtenteProtocollatore
	 * @param inDataInvio
	 * @param inNota
	 * @param statoMailPreArch
	 * @param erroriValidazioneInterop
	 * @param dataScarico
	 */
	public DetailEmailDTO(final String inGuid, final String inOggetto, final String inMittente, final String inDestinatario, final String inDestinatarioCC,
			final String inDataArrivo, final String inTesto, final String inContentNote, final List<MailAttachmentDTO> inAllegati, final String inMotivoEliminazione,
			final StatoMailEnum inStatoMail, final Integer inIdUtenteProtocollatore, final String inDescUtenteProtocollatore, final String inDataInvio, final NotaDTO inNota,
			final StatoMailEnum inStatoMailPreArch, final List<ErroreValidazioneDTO> inErroriValidazioneInterop, final String inDataScarico) {
		this(inGuid, inOggetto, inMittente, inDestinatario, inDestinatarioCC, inDataArrivo, inTesto, inContentNote, inAllegati, inMotivoEliminazione, inStatoMail,
				inIdUtenteProtocollatore, inDescUtenteProtocollatore, inDataInvio, inNota, inStatoMailPreArch, inErroriValidazioneInterop);
		this.setDataScarico(inDataScarico);
	}

	/**
	 * Getter.
	 * 
	 * @return stato mail
	 */
	public StatoMailEnum getStatoMail() {
		return statoMail;
	}

	/**
	 * Getter.
	 * 
	 * @return identificativo utente protocollatore
	 */
	public Integer getIdUtenteProtocollatore() {
		return idUtenteProtocollatore;
	}

	/**
	 * Getter.
	 * 
	 * @return descrizione utente protocollatore
	 */
	public String getDescUtenteProtocollatore() {
		return descUtenteProtocollatore;
	}

	/**
	 * @param descUtenteProtocollatore the descUtenteProtocollatore to set
	 */
	public void setDescUtenteProtocollatore(final String descUtenteProtocollatore) {
		this.descUtenteProtocollatore = descUtenteProtocollatore;
	}

	/**
	 * Getter.
	 * 
	 * @return guid
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * Setter.
	 * 
	 * @param inGuid guid
	 */
	public void setGuid(final String inGuid) {
		this.guid = inGuid;
	}

	/**
	 * Getter.
	 * 
	 * @return oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * Setter.
	 * 
	 * @param inOggetto oggetto
	 */
	public void setOggetto(final String inOggetto) {
		this.oggetto = inOggetto;
	}

	/**
	 * Getter.
	 * 
	 * @return mittente
	 */
	public String getMittente() {
		return mittente;
	}

	/**
	 * Setter.
	 * 
	 * @param inMittente mittente
	 */
	public void setMittente(final String inMittente) {
		this.mittente = inMittente;
	}

	/**
	 * Getter.
	 * 
	 * @return destinatario
	 */
	public String getDestinatario() {
		return destinatario;
	}

	/**
	 * Setter.
	 * 
	 * @param inDestinatario destinatario
	 */
	public void setDestinatario(final String inDestinatario) {
		this.destinatario = inDestinatario;
	}

	/**
	 * Getter.
	 * 
	 * @return destinatario cc
	 */
	public String getDestinatarioCC() {
		return destinatarioCC;
	}

	/**
	 * Setter.
	 * 
	 * @param inDestinatarioCC destinatario cc
	 */
	public void setDestinatarioCC(final String inDestinatarioCC) {
		this.destinatarioCC = inDestinatarioCC;
	}

	/**
	 * Getter.
	 * 
	 * @return data arrivo
	 */
	public String getDataArrivo() {
		return dataArrivo;
	}

	/**
	 * Setter.
	 * 
	 * @param inDataArrivo data arrivo
	 */
	public void setDataArrivo(final String inDataArrivo) {
		this.dataArrivo = inDataArrivo;
	}

	/**
	 * Getter.
	 * 
	 * @return allegati
	 */
	public Collection<MailAttachmentDTO> getAllegati() {
		return allegati;
	}

	/**
	 * Setter.
	 * 
	 * @param inAllegati allegati
	 */
	public void setAllegati(final Collection<MailAttachmentDTO> inAllegati) {
		this.allegati = inAllegati;
	}

	/**
	 * Gets the testo.
	 *
	 * @return the testo
	 */
	public String getTesto() {
		return testo;
	}

	/**
	 * Sets the testo.
	 *
	 * @param inTesto the testo to set
	 */
	public void setTesto(final String inTesto) {
		this.testo = inTesto;
	}

	/**
	 * Gets the motivo eliminazione.
	 *
	 * @return the motivoEliminazione
	 */
	public String getMotivoEliminazione() {
		return motivoEliminazione;
	}

	/**
	 * Sets the motivo eliminazione.
	 *
	 * @param inMotivoEliminazione the motivoEliminazione to set
	 */
	public void setMotivoEliminazione(final String inMotivoEliminazione) {
		this.motivoEliminazione = inMotivoEliminazione;
	}

	/**
	 * Restituisce la data di invio della mail.
	 * 
	 * @return dataInvio
	 */
	public String getDataInvio() {
		return dataInvio;
	}

	/**
	 * Imposta la data di invio della mail.
	 * 
	 * @param dataInvio
	 */
	public void setDataInvio(final String dataInvio) {
		this.dataInvio = dataInvio;
	}

	/**
	 * Restituisce lo stato precedente all'archiviazione.
	 * 
	 * @return stato pre archiviazione
	 */
	public StatoMailEnum getStatoPreArchiviazione() {
		return statoPreArchiviazione;
	}

	/**
	 * Imposta lo stato precedente all'archiviazione.
	 * 
	 * @param statoPreArchiviazione
	 */
	public void setStatoPreArchiviazione(final StatoMailEnum statoPreArchiviazione) {
		this.statoPreArchiviazione = statoPreArchiviazione;
	}

	/**
	 * Restituisce la nota associata alla mail.
	 * 
	 * @return nota
	 */
	public NotaDTO getNota() {
		return nota;
	}

	/**
	 * Imposta la nota associata alla mail.
	 * 
	 * @param nota
	 */
	public void setNota(final NotaDTO nota) {
		this.nota = nota;
	}

	/**
	 * @param idUtenteProtocollatore the idUtenteProtocollatore to set
	 */
	public void setIdUtenteProtocollatore(final Integer idUtenteProtocollatore) {
		this.idUtenteProtocollatore = idUtenteProtocollatore;
	}

	/**
	 * Restituisce l'esito della validazione interoperabile.
	 * 
	 * @return esito validazione
	 */
	public String getEsitoValidazioneInterop() {
		return esitoValidazioneInterop;
	}

	/**
	 * Imposta l'esito di validazione interoperabile.
	 * 
	 * @param esitoValidazioneInterop
	 */
	public void setEsitoValidazioneInterop(final String esitoValidazioneInterop) {
		this.esitoValidazioneInterop = esitoValidazioneInterop;
	}

	/**
	 * Restituisce la lista di errori riscontrati in fase di validazione
	 * interoperabile.
	 * 
	 * @return List di ErroreValidazioneDTO
	 */
	public List<ErroreValidazioneDTO> getErroriValidazioneInterop() {
		return erroriValidazioneInterop;
	}

	/**
	 * Imposta la lista degli errori riscontrati in fase di validazione
	 * interoperabile.
	 * 
	 * @param erroriValidazioneInterop
	 */
	public void setErroriValidazioneInterop(final List<ErroreValidazioneDTO> erroriValidazioneInterop) {
		this.erroriValidazioneInterop = erroriValidazioneInterop;
	}

	/**
	 * Restituisce il preassegnatario Interoperabile.
	 * 
	 * @return AssegnatarioInteropDTO
	 */
	public AssegnatarioInteropDTO getPreassegnatarioInterop() {
		return preassegnatarioInterop;
	}

	/**
	 * Imposta il preassegnatario interoperabile.
	 * 
	 * @param preassegnatarioInterop
	 */
	public void setPreassegnatarioInterop(final AssegnatarioInteropDTO preassegnatarioInterop) {
		this.preassegnatarioInterop = preassegnatarioInterop;
	}

	/**
	 * Restituisce i preassegnatari per conoscenza.
	 * 
	 * @return List di AssegnazioneDTO
	 */
	public List<AssegnazioneDTO> getPreassegnatarioPerConoscenza() {
		return preassegnatarioPerConoscenza;
	}

	/**
	 * Imposta la lista dei preassegnatari per conoscenza.
	 * 
	 * @param preassegnatarioPerConoscenza
	 */
	public void setPreassegnatarioPerConoscenza(final List<AssegnazioneDTO> preassegnatarioPerConoscenza) {
		this.preassegnatarioPerConoscenza = preassegnatarioPerConoscenza;
	}

	/**
	 * @return dataScarico
	 */
	public String getDataScarico() {
		return dataScarico;
	}

	/**
	 * @param dataScarico
	 */
	public void setDataScarico(final String dataScarico) {
		this.dataScarico = dataScarico;
	}

	/**
	 * Restituisce la lista dei mittenti rielaborata con lo spazio dopo la virgola se esiste, altrimenti restituisce null.
	 * @return lista mittenti come string
	 */
	public String getMittenteSplit() {
		if(mittente!=null) {
			return mittente.replace(";", "; ");
		}else {
			return null;
		}
	}

	/**
	 * Restituisce la lista dei destinatari come string.
	 * @return destinatari come stringa
	 */
	public String getDestinatarioSplit() {
		if(destinatario!=null) {
			return destinatario.replace(";", "; ");
		}else {
			return null;
		}
	}

	/**
	 * Restituisce la lista dei destinatari cc come string.
	 * @return destinatari come stringa
	 */
	public String getDestinatarioCCSplit() {
		if(destinatarioCC!=null) {
			return destinatarioCC.replace(";", "; ");
		}else {
			return null;
		}
	}

}
