package it.ibm.red.business.dto;

import java.util.Date;

import it.ibm.red.business.enums.EventoCalendarioEnum;

/**
 * DTO che definisce un evento di ricerca.
 */
public class RicercaEventoDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -4467432114280020729L;
	
	/**
	 * Data inizio.
	 */
	private Date dataInizio;

	/**
	 * Data scadenza.
	 */
	private Date dataScadenza;
	
	/**
	 * Titolo.
	 */
	private String titolo;

	/**
	 * Evento calendaroi.
	 */
	private EventoCalendarioEnum tipologia;

	/**
	 * Restituisce la data inizio.
	 * @return data inizio
	 */
	public Date getDataInizio() {
		return dataInizio;
	}
	
	/**
	 * Imposta la data di inizio.
	 * @param dataInizio
	 */
	public void setDataInizio(final Date dataInizio) {
		this.dataInizio = dataInizio;
	}
	
	/**
	 * Restituisce la data di scadenza.
	 * @return data scadenza
	 */
	public Date getDataScadenza() {
		return dataScadenza;
	}
	
	/**
	 * Imposta la data di scadenza.
	 * @param dataScadenza
	 */
	public void setDataScadenza(final Date dataScadenza) {
		this.dataScadenza = dataScadenza;
	}
	
	/**
	 * Restituisce il titolo.
	 * @return titolo
	 */
	public String getTitolo() {
		return titolo;
	}
	
	/**
	 * Imposta il titolo.
	 * @param titolo
	 */
	public void setTitolo(final String titolo) {
		this.titolo = titolo;
	}
	
	/**
	 * Restituisce la tipologia.
	 * @return tipologia
	 */
	public EventoCalendarioEnum getTipologia() {
		return tipologia;
	}
	
	/**
	 * Imposta la tipologia.
	 * @param tipologia
	 */
	public void setTipologia(final EventoCalendarioEnum tipologia) {
		this.tipologia = tipologia;
	}

}