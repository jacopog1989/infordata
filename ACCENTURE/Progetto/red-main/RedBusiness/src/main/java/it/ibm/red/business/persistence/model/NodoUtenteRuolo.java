package it.ibm.red.business.persistence.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The Class NodoUtenteRuolo.
 *
 * @author CPIERASC
 * 
 *         Entità che mappa la tabella NODOUTENTERUOLO.
 */
public class NodoUtenteRuolo implements Serializable {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Active user.
	 */
	private BigDecimal activeUser;
	
	/**
	 * Data attivazione.
	 */
	private Date dataAttivazione;
	
	/**
	 * Data disattivazione.
	 */
	private Date dataDisattivazione;
	
	/**
	 * Identificativo nodo.
	 */
	private long idnodo;
	
	/**
	 * IDentificativo ruolo.
	 */
	private long idruolo;
	
	/**
	 * Identificativo utente.
	 */
	private long idutente;
	
	/**
	 * Nodo.
	 */
	private Nodo nodo;
	
	/**
	 * Flag che indica se la coppia ufficio/ruolo è la predefinita per l'utente.
	 */
	private Long predefinito;
	
	/**
	 * Ruolo.
	 */
	private Ruolo ruolo;
	
	/**
	 * Utente.
	 */
	private Utente utente;
	
	/**
	 * Gestione Applicativa
	 */
	private Integer gestioneApplicativa;

	/**
	 * Costruttore.
	 */
	public NodoUtenteRuolo() {
		//.
	}

	/**
	 * Costruttore.
	 *
	 * @param inActiveUser 		flag utente attivo
	 * @param inDataAttivazione 	data attivazione
	 * @param inDataDisattivazione data disattivazione
	 * @param inIdnodo 			identificativo nodo
	 * @param inIdruolo 			identificativo ruolo
	 * @param inIdutente 		identificativo utente
	 * @param inNodo 			nodo
	 * @param inPredefinito 		flag predefinito
	 * @param inRuolo 			ruolo
	 * @param inUtente 			utente
	 * @param inGestioneApplicativa the in gestione applicativa
	 */
	public NodoUtenteRuolo(final BigDecimal inActiveUser, final Date inDataAttivazione, final Date inDataDisattivazione, final long inIdnodo, final long inIdruolo, 
			final long inIdutente, final Nodo inNodo, final Long inPredefinito, final Ruolo inRuolo, final Utente inUtente, final Integer inGestioneApplicativa) {
		activeUser = inActiveUser;
		dataAttivazione = inDataAttivazione;
		dataDisattivazione = inDataDisattivazione;
		idnodo = inIdnodo;
		idruolo = inIdruolo;
		idutente = inIdutente;
		nodo = inNodo;
		predefinito = inPredefinito;
		ruolo = inRuolo;
		utente = inUtente;
		gestioneApplicativa = inGestioneApplicativa;
	}

	/**
	 * Getter flag active user.
	 * 
	 * @return	flag active user
	 */
	public final BigDecimal getActiveUser() {
		return activeUser;
	}

	/**
	 * Getter data attivazione.
	 * 
	 * @return	data attivazione
	 */
	public final Date getDataAttivazione() {
		return dataAttivazione;
	}

	/**
	 * Getter data disattivazione.
	 * 
	 * @return	data disattivazione
	 */
	public final Date getDataDisattivazione() {
		return dataDisattivazione;
	}

	/**
	 * Getter dentificativo nodo.
	 * 
	 * @return	identificativo nodo
	 */
	public final long getIdnodo() {
		return idnodo;
	}

	/**
	 * Getter id ruolo.
	 * 
	 * @return	id ruolo
	 */
	public final long getIdruolo() {
		return idruolo;
	}

	/**
	 * Getter identificativo utente.
	 * 
	 * @return	identificativo utente
	 */
	public final long getIdutente() {
		return idutente;
	}

	/**
	 * Getter nodo.
	 * 
	 * @return	nodo
	 */
	public final Nodo getNodo() {
		return nodo;
	}

	/**
	 * Getter flag predefinito.
	 * 
	 * @return	flag predefinito
	 */
	public final Long getPredefinito() {
		return predefinito;
	}

	/**
	 * Getter ruolo.
	 * 
	 * @return	ruolo
	 */
	public final Ruolo getRuolo() {
		return ruolo;
	}

	/**
	 * Getter utente.
	 * 
	 * @return	utente
	 */
	public final Utente getUtente() {
		return utente;
	}

	/**
	 * Setter utente.
	 * 
	 * @param inUtente	utente
	 */
	public final void setUtente(final Utente inUtente) {
		utente = inUtente;
	}

	/**
	 * Setter nodo.
	 * 
	 * @param inNodo	nodo
	 */
	public final void setNodo(final Nodo inNodo) {
		nodo = inNodo;
	}

	/**
	 * Getter gestione applicativa.
	 *
	 * @return the gestione applicativa
	 */
	public Integer getGestioneApplicativa() {
		return gestioneApplicativa;
	}

	/**
	 * Setter gestione applicativa.
	 *
	 * @param gestioneApplicativa the new gestione applicativa
	 */
	public void setGestioneApplicativa(final Integer gestioneApplicativa) {
		this.gestioneApplicativa = gestioneApplicativa;
	}
	
}