package it.ibm.red.business.dto;

/**
 * DTO che definisce un utente con ruolo report.
 */
public class UtenteRuoloReportDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -297911327199703901L;

	/**
	 * Identificativo utente.
	 */
	private Long idUtente;
	
	/**
	 * Ruolo.
	 */
	private Long idRuolo;
	
	/**
	 * ufficio.
	 */
	private Long idUfficio;
	
	/**
	 * Aoo.
	 */
	private Long idAoo;
	
	/**
	 * Username.
	 */
	private String username;
	
	/**
	 * Nome.
	 */
	private String nome;
	
	/**
	 * Cognome.
	 */
	private String cognome;
	
	/**
	 * Ruolo.
	 */
	private String ruolo;
	
	/**
	 * Ufficio.
	 */
	private String ufficio;
	
	/**
	 * Aoo.
	 */
	private String aoo;
	
	/**
	 * Contatore.
	 */
	private Long count;
	
	/**
	 * Restituisce l'id dell'utente.
	 * @return id utente
	 */
	public Long getIdUtente() {
		return idUtente;
	}
	
	/**
	 * Imposta l'id dell'utente.
	 * @param inIdUtente
	 */
	public void setIdUtente(final Long inIdUtente) {
		this.idUtente = inIdUtente;
	}
	
	/**
	 * Restituisce l'id del ruolo.
	 * @return id ruolo
	 */
	public Long getIdRuolo() {
		return idRuolo;
	}
	
	/**
	 * Imposta l'id del ruolo.
	 * @param inIdRuolo
	 */
	public void setIdRuolo(final Long inIdRuolo) {
		this.idRuolo = inIdRuolo;
	}
	
	/**
	 * Restituisce l'id dell'ufficio.
	 * @return id ufficio
	 */
	public Long getIdUfficio() {
		return idUfficio;
	}
	
	/**
	 * Imposta l'id dell'ufficio.
	 * @param inIdUfficio
	 */
	public void setIdUfficio(final Long inIdUfficio) {
		this.idUfficio = inIdUfficio;
	}
	
	/**
	 * Restituisce l'id dell'AOO.
	 * @return id AOO
	 */
	public Long getIdAoo() {
		return idAoo;
	}
	
	/**
	 * Imposta l'id dell'AOO.
	 * @param inIdAoo
	 */
	public void setIdAoo(final Long inIdAoo) {
		this.idAoo = inIdAoo;
	}
	
	/**
	 * Restituisce lo username.
	 * @return username
	 */
	public String getUsername() {
		return username;
	}
	
	/**
	 * Imposta lo username.
	 * @param inUsername
	 */
	public void setUsername(final String inUsername) {
		this.username = inUsername;
	}
	
	/**
	 * Restituisce il nome.
	 * @return nome
	 */
	public String getNome() {
		return nome;
	}
	
	/**
	 * Imposta il nome.
	 * @param inNome
	 */
	public void setNome(final String inNome) {
		this.nome = inNome;
	}
	
	/**
	 * Restituisce il cognome.
	 * @return cognome
	 */
	public String getCognome() {
		return cognome;
	}
	
	/**
	 * Imposta il cognome.
	 * @param inCognome
	 */
	public void setCognome(final String inCognome) {
		this.cognome = inCognome;
	}
	
	/**
	 * Restituisce il ruolo.
	 * @return ruolo
	 */
	public String getRuolo() {
		return ruolo;
	}
	
	/**
	 * Imposta il ruolo.
	 * @param inRuolo
	 */
	public void setRuolo(final String inRuolo) {
		this.ruolo = inRuolo;
	}
	
	/**
	 * Restituisce l'ufficio.
	 * @return ufficio
	 */
	public String getUfficio() {
		return ufficio;
	}
	
	/**
	 * Imposta l'ufficio.
	 * @param inUfficio
	 */
	public void setUfficio(final String inUfficio) {
		this.ufficio = inUfficio;
	}
	
	/**
	 * Restituisce l'AOO.
	 * @return aoo
	 */
	public String getAoo() {
		return aoo;
	}
	
	/**
	 * Imposta l'AOO.
	 * @param inAoo
	 */
	public void setAoo(final String inAoo) {
		this.aoo = inAoo;
	}
	
	/**
	 * Restituisce count.
	 * @return count
	 */
	public Long getCount() {
		return count;
	}
	
	/**
	 * Imposta count.
	 * @param inCount
	 */
	public void setCount(final Long inCount) {
		this.count = inCount;
	}
}