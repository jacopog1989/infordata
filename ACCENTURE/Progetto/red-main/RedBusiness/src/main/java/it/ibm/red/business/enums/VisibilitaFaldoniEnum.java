package it.ibm.red.business.enums;

/**
 * Enum che definisce la visibilità di un faldone.
 */
public enum VisibilitaFaldoniEnum {

	/**
	 * Visibilità Faldoni totale.
	 */
	TOTALE("T"),

	/**
	 * Visibilità Faldoni ristretta.
	 */
	RISTRETTA("R");

	/**
	 * Carattere identificativo delle visibilità.
	 */
	private String value;

	/**
	 * Costruttore di default.
	 * @param inValue
	 */
	VisibilitaFaldoniEnum(final String inValue) {
		value = inValue;
	}

	/**
	 * Restituisce il corrispettivo valore dell'enum.
	 * @return value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Restituisce un oggetto dell'enum a partire dal valore in input.
	 * @param val
	 * @return visibilitaFaldoniEnum
	 */
	public static VisibilitaFaldoniEnum get(final String val) {
		VisibilitaFaldoniEnum output = null;
		for (VisibilitaFaldoniEnum v:VisibilitaFaldoniEnum.values()) {
			if (v.getValue().equalsIgnoreCase(val)) {
				output = v;
			}
		}
		return output;
	}
}
