/**
 * @author CPIERASC
 *
 *	In questo package sono contenute tutte le classi java nate per modellare le tabelle del database relazionale Oracle
 *	applicativo. Non è stato utilizzato un ORM per problemi di incompatibilità con la versione dell'application server 
 *	in uso e per omogenità con l'applicativo RED.
 */
package it.ibm.red.business.persistence.model;
