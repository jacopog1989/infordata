package it.ibm.red.business.validator;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.SalvaDocumentoErroreEnum;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.utils.DateUtils;

/**
 * Classe validator Data Scadenza Documento.
 */
public class DataScadenzaDocumentoValidator extends AbstractValidator<Date, SalvaDocumentoErroreEnum> {
	
	/**
	 * Identificativo wob.
	 */
	private String wobNumber;

	/**
	 * Helper gestione PE FNET.
	 */
	private FilenetPEHelper fpeh;

	/**
	 * Costruttore completo.
	 * 
	 * @param wobNumber
	 * @param fpeh
	 */
	public DataScadenzaDocumentoValidator(final String wobNumber, final FilenetPEHelper fpeh) {
		super();
		this.wobNumber = wobNumber;
		this.fpeh = fpeh;
	}

	/**
	 * @see it.ibm.red.business.validator.AbstractValidator#validaTutti(java.util.Collection).
	 */
	@Override
	public List<SalvaDocumentoErroreEnum> validaTutti(final Collection<Date> dateScadenza) {
		for (Date dataScadenza : dateScadenza) {
			getErrori().addAll(valida(dataScadenza));
		}
		
		return getErrori();
	}
	
	/**
	 * Controlla che la data inserita non sia precedente alla data odierna, o, nel
	 * caso di modifica del documento e data scadenza precedentemente inserita, non
	 * sia precedente alla data già inserita.
	 * 
	 * @param dataScadenza
	 *            Data scadenza del documento da validare
	 */
	@Override
	public List<SalvaDocumentoErroreEnum> valida(final Date dataScadenza) {
		Date dataOdierna = DateUtils.setDateTo0000(new Date());
		Date dataScadenzaWorkflow = null;
		if (StringUtils.isNotBlank(wobNumber)) {
			dataScadenzaWorkflow = DateUtils.setDateTo0000(getDataScadenzaWorkflow());
		}
		
		if (dataScadenza != null) {
			if (dataScadenzaWorkflow != null && dataScadenza.compareTo(dataScadenzaWorkflow) < 0) {
				getErrori().add(SalvaDocumentoErroreEnum.DOC_DATA_SCADENZA_PRECEDENTE_WORKFLOW);
			} else if (dataScadenza.compareTo(dataOdierna) < 0) {
				getErrori().add(SalvaDocumentoErroreEnum.DOC_DATA_SCADENZA_PRECEDENTE_ODIERNA);
			}
		}
		
		return getErrori();
	}
	
	private Date getDataScadenzaWorkflow() {
		VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);
		return (Date) TrasformerPE.getMetadato(wob, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY));
	}
}