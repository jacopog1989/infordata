package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UfficioDTO;

/**
 * Interfaccia DAO gestione organigramma.
 */
public interface IOrganigrammaDAO extends Serializable {
	
	/**
	 * 
	 * @param idUfficio
	 * @param idAOO
	 * @param con
	 * @return
	 */
	List<UfficioDTO> getSottoNodi(Long idUfficio, Long idAOO, Connection con);
	
	/**
	 * 
	 * @param idNodo
	 * @param idAOO
	 * @param visualizzazione
	 * @param connection
	 * @return
	 */
	List<NodoOrganigrammaDTO> getFigliByNodo(Long idNodo, Long idAOO, String visualizzazione, Long idNodoUtente, Connection connection);
	/**
	 * 
	 * @param idNodo
	 * @param consideraDisattivi
	 * @param connection
	 * @return
	 */
	List<NodoOrganigrammaDTO> getUtentiByNodo(boolean consideraDisattivi,Long idNodo, Connection connection);
	


	/**
	 * 
	 * @param idNodo
	 * @param connection
	 * @return
	 */
	NodoOrganigrammaDTO getUtenteDirigenteByNodo(Long idNodo, Connection connection);
	
	/**
	 * 
	 * @param idNodo
	 * @param connection
	 * @return
	 */
	List<NodoOrganigrammaDTO> getUtentiByNodo(Long idNodo, boolean withDirigente, Connection connection);
	
	/**
	 * Metodo per il recupero dei nodi che hanno un dirigente configurato utilizzati per disegnare l'organigramma. 
	 * 
	 * @param idNodo
	 * @param idAOO
	 * @param visualizzazione
	 * @param connection
	 * @return
	 */
	List<NodoOrganigrammaDTO> getNodiConDirigente(Long idNodo, Long idAOO, String visualizzazione, Connection connection);


}
