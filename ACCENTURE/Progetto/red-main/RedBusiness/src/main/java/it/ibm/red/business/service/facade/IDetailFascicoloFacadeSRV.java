package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.DetailFascicoloRedDTO;
import it.ibm.red.business.dto.DetailFatturaFepaDTO;
import it.ibm.red.business.dto.DocumentoFascicoloDTO;
import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Facade del servizio di gestione dettaglio fascicolo.
 */
public interface IDetailFascicoloFacadeSRV extends Serializable {

	/**
	 * @param fascicolo devo passare tutto il fascicolo perché la query dei master
	 *                  gia' tira fuori tutti i metadati li devo quindi riportare su
	 *                  DetailFascicoloRedDTO
	 * @param utente
	 * @return
	 */
	DetailFascicoloRedDTO getFascicolo(FascicoloDTO fascicolo, UtenteDTO utente);

	/**
	 * Converte il dettaglio nel fascicolo.
	 * 
	 * @param detail    dettaglio aggiornato dal master
	 * @param fascicolo fascicolo che deve essere aggiornato con le informazioni
	 *                  presenti nel dettaglio
	 * 
	 */
	void convertDettaglioToFascicolo(DetailFascicoloRedDTO detail, FascicoloDTO fascicolo);

	/**
	 * @param idFascicolo
	 * @param idAOO
	 * @param utente
	 * @return
	 */
	List<DocumentoFascicoloDTO> getDocumentiByIdFascicolo(Integer idFascicolo, Long idAOO, UtenteDTO utente);

	/**
	 * @param idFascicolo
	 * @param idAOO
	 * @param utente
	 * @return
	 */
	List<FaldoneDTO> getFaldoniByIdFascicolo(String idFascicolo, Long idAOO, UtenteDTO utente);

	/**
	 * Ottiene il fascicolo per tab fascicolo e faldone.
	 * 
	 * @param fascicolo
	 * @param utente
	 * @return fascicolo
	 */
	DetailFascicoloRedDTO getFascicoloDTOperTabFascicoloeFaldone(FascicoloDTO fascicolo, UtenteDTO utente);

	/**
	 * Ottiene le fatture FEPA tramite l'id del fascicolo.
	 * 
	 * @param idFascicolo
	 * @param idAOO
	 * @param utente
	 * @return lista di dettagli delle fatture FEPA
	 */
	List<DetailFatturaFepaDTO> getFattureFepaByIdFascicolo(Integer idFascicolo, Long idAOO, UtenteDTO utente);

	/**
	 * Permette di recuperare il dettaglio del fascicolo senza ricalcolarne la lista
	 * di documenti.
	 * 
	 * @param idFascicolo identificativo del fascicolo (nomeFascicolo)
	 * @param documenti   Lista di documenti presenti nel fascicolo
	 * @param utente      Utente che ha richiesto l'operazione
	 * @return Il valore è posto di default a null In caso di errore lancia una
	 *         redException
	 */
	DetailFascicoloRedDTO getDetailFascicolo(Integer idFascicolo, List<DocumentoFascicoloDTO> documenti, UtenteDTO utente);

	/**
	 * Ottiene il fascicolo procedimentale.
	 * 
	 * @param docTitle
	 * @param idAoo
	 * @param fcDTO
	 * @return fascicolo procedimentale
	 */
	String getFascicoloProcedimentale(String docTitle, Integer idAoo, FilenetCredentialsDTO fcDTO);

	/**
	 * Ottiene il dettaglio del fascicolo.
	 * 
	 * @param idFascicolo
	 * @param utente
	 * @return dettaglo del fascicolo
	 */
	DetailFascicoloRedDTO getDetailFascicolo(Integer idFascicolo, UtenteDTO utente);

}
