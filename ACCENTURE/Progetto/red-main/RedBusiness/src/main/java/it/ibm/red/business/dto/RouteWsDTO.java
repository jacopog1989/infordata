/**
 * 
 */
package it.ibm.red.business.dto;

/**
 * @author APerquoti
 *
 */
public class RouteWsDTO extends AbstractDTO {
	
    /**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 266977882009305618L;
	

    /**
     * Condizione.
     */
	private String condition;

    /**
     * Destinatario.
     */
	
	private String destinationStep;
    

    /**
     * Nome rotta.
     */
    private String routeName;
    
    /**
     * Costruttore di default.
     */
	public RouteWsDTO() {
		super();
	}

	/**
	 * @return condition
	 */
	public String getCondition() {
		return condition;
	}

	/**
	 * @param condition
	 */
	public void setCondition(final String condition) {
		this.condition = condition;
	}

	/**
	 * @return destinationStep
	 */
	public String getDestinationStep() {
		return destinationStep;
	}

	/**
	 * @param destinationStep
	 */
	public void setDestinationStep(final String destinationStep) {
		this.destinationStep = destinationStep;
	}

	/**
	 * @return routeName
	 */
	public String getRouteName() {
		return routeName;
	}

	/**
	 * @param routeName
	 */
	public void setRouteName(final String routeName) {
		this.routeName = routeName;
	}
	
	
}
