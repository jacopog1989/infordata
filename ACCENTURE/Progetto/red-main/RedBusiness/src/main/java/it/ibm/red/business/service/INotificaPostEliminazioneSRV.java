package it.ibm.red.business.service;

/**
 * Interfaccia del servizio che gestisce la notifica dopo l'eliminazione.
 */
public interface INotificaPostEliminazioneSRV extends INotificaWriteSRV {

}
