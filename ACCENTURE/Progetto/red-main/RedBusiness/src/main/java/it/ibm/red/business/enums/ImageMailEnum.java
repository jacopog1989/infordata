package it.ibm.red.business.enums;

/**
 * Enum che definisce le diverse immagini associate a tipi di mail.
 */
public enum ImageMailEnum {
	
	/**
	 * Valore.
	 */
	IMAGE_MAIL_NON_SPECIFICATA(1, "/resources/images/mail_non_spec.png"),
	
	/**
	 * Valore.
	 */
	IMAGE_MAIL_ACCETTATA(2, "/resources/images/mail_accettata.png"),
	
	/**
	 * Valore.
	 */
	IMAGE_MAIL_NON_ACCETTATA(3, "/resources/images/mail_non_accettata.png"),
	
	/**
	 * Valore.
	 */
	IMAGE_MAIL_CONSEGNATA(4, "/resources/images/mail_accettata.png"),
	
	/**
	 * Valore.
	 */
	IMAGE_MAIL_NON_CONSEGNATA(5, "/resources/images/mail_non_accettata.png"),
	
	/**
	 * Valore.
	 */
	IMAGE_MAIL_IN_ATTESA(6, "/resources/images/mail_pending.png");
	
	
    /**
     * Valore.
     */
	private Integer value;
	
    /**
     * Path.
     */
	private String path;
	
	ImageMailEnum(final Integer value, final String path) {
		this.value = value;
		this.path = path;
	}
	
	/**
	 * Recupera il valore.
	 * @return value
	 */
	public Integer getValue() {
		return this.value;
	}
	
	/**
	 * Recupera il path.
	 * @return path
	 */
	public String getPath() {
		return this.path;
	}
	
}
