/**
 * 
 */
package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;

import it.ibm.red.business.dto.ConservazioneWsDTO;

/**
 * @author APerquoti
 *
 */
public interface IConservazioneDAO extends Serializable {

	/**
	 * Verifica se il documento è già conservato.
	 * @param uuid
	 * @param con
	 * @return true o false
	 */
	boolean checkDocumentoConservato(String uuid, Connection con);
	
	/**
	 * Esegue l'update/insert di un record sulla tabella conservazione.
	 * @param daConservare
	 * @param con
	 * @return
	 */
	int setConservazione(ConservazioneWsDTO daConservare, Connection con);
	
}
