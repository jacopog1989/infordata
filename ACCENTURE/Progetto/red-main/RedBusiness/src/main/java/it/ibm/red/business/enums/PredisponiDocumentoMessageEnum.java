package it.ibm.red.business.enums;

/**
 * The Enum VistoErrorEnum.
 *
 * @author m.crescentini
 * 
 *         Warning/errori generabili in fase di predisposizione documento.
 */
public enum PredisponiDocumentoMessageEnum {
	
	
	
	/**
	 * Valore.
	 */
	ATTO_DECRETO_ALLEGATI_NON_CONFORMI(1, "Uno o più allegati presentavano file non conformi per la tipologia ATTO DECRETO e sono stati rimossi", false),
	
	
	/**
	 * Valore.
	 */
	ERRORE_GENERICO(99, "Si è verificato un errore nella predisposizione del documento in uscita", true);
	
	
	/**
	 * Codice di errore.
	 */
	private int codError;

	/**
	 * Messaggio d'errore.
	 */
	private String message;
	
	/**
	 * Indica se si tratta di un errore o di un warning.
	 */
	private boolean error;
	
	
	/**
	 * Costruttore.
	 * 
	 * @param inCodError	codice errore
	 * @param inMessage		messaggio errore
	 * @param inError		flag che indica se si tratta di un errore (altrimenti è un warning)
	 */
	PredisponiDocumentoMessageEnum(final int inCodError, final String inMessage, final boolean inError) {
		codError = inCodError;
		message = inMessage;
		error = inError;
	}

	/**
	 * Getter codice errore.
	 * 
	 * @return	codice errore
	 */
	public int getCodError() {
		return codError;
	}

	/**
	 * Getter messaggio d'errore.
	 * 
	 * @return	messaggio d'errore
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Getter flag errore.
	 * 
	 * @return the error
	 */
	public boolean isError() {
		return error;
	}

}
