package it.ibm.red.business.dto;

import java.io.Serializable;

/**
 * @author VINGENITO
 */
public class TemplateMetadatoVersion implements Serializable {

	private static final long serialVersionUID = -5009331295264188090L;

	/**
	 * Identiicativo documento.
	 */
	private Integer idDocumento;
	
	/**
	 * Nome del template.
	 */
	private String nomeTemplate;
	
	/**
	 * Versione.
	 */
	private Integer version;
	
	/**
	 * Identificativo template.
	 */
	private String idTemplate;
	
	/**
	 * Descrizione template.
	 */
	private String descrizioneTemplate;
	 
	/**
	 * Utente creatore.
	 */
	private String utenteCreatore;
	
	/**
	 * Flag selezionato.
	 */
	private boolean selected;

	/**
	 * Costruttore vuoto.
	 */
	public TemplateMetadatoVersion() {
	}
	
	/**
	 * Costruttore di default.
	 * @param idDocumento
	 * @param nomeTemplate
	 * @param version
	 * @param idTemplate
	 * @param descrizioneTemplate
	 * @param utenteCreatore
	 */
	public TemplateMetadatoVersion(final Integer idDocumento, final String nomeTemplate, final Integer version, final String idTemplate, final String descrizioneTemplate, final String utenteCreatore) {
		this.idDocumento = idDocumento;
		this.nomeTemplate = nomeTemplate;
		this.version = version;
		this.idTemplate = idTemplate;
		this.setDescrizioneTemplate(descrizioneTemplate);
		this.utenteCreatore = utenteCreatore;
	}

	/**
	 * @return idDocumento
	 */
	public Integer getIdDocumento() {
		return idDocumento;
	}

	/**
	 * Imposta l'id del documento.
	 * @param idDocumento
	 */
	public void setIdDocumento(final Integer idDocumento) {
		this.idDocumento = idDocumento;
	}

	/**
	 * @return nomeTemplate
	 */
	public String getNomeTemplate() {
		return nomeTemplate;
	}

	/**
	 * Imposta il nome del template.
	 * @param nomeTemplate
	 */
	public void setNomeTemplate(final String nomeTemplate) {
		this.nomeTemplate = nomeTemplate;
	}

	/**
	 * Restituisce il numero di versione del template.
	 * @return version
	 */
	public Integer getVersion() {
		return version;
	}

	/**
	 * Imposta il numero di versione del template.
	 * @param version
	 */
	public void setVersion(final Integer version) {
		this.version = version;
	}

	/**
	 * @return idTemplate
	 */
	public String getIdTemplate() {
		return idTemplate;
	}

	/**
	 * Imposta l'id del template.
	 * @param idTemplate
	 */
	public void setIdTemplate(final String idTemplate) {
		this.idTemplate = idTemplate;
	}

	/**
	 * @return descrizioneTemplate
	 */
	public String getDescrizioneTemplate() {
		return descrizioneTemplate;
	}

	/**
	 * Imposta la descrizione del template.
	 * @param descrizioneTemplate
	 */
	public void setDescrizioneTemplate(final String descrizioneTemplate) {
		this.descrizioneTemplate = descrizioneTemplate;
	}

	/**
	 * @return utenteCreatore
	 */
	public String getUtenteCreatore() {
		return utenteCreatore;
	}

	/**
	 * Imposta l'utente creatore.
	 * @param utenteCreatore
	 */
	public void setUtenteCreatore(final String utenteCreatore) {
		this.utenteCreatore = utenteCreatore;
	}

	/**
	 * Restituisce il flag di selezione.
	 * @return selected
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * Imposta il flag di selezione.
	 * @param selected
	 */
	public void setSelected(final boolean selected) {
		this.selected = selected;
	}
}
