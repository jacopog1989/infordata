package it.ibm.red.business.enums;

/**
 * Enum che definisce la tipologia di gestione del documento.
 */
public enum TipoGestioneEnum {

	/**
	 * Valore.
	 */
	MANUALE("M", "MANUALE"),
	
	/**
	 * Valore.
	 */
	IBRIDA("I", "IBRIDA"),
	
	/**
	 * Valore.
	 */
	AUTOMATICA("A", "AUTOMATICA");

	/**
	 * Codice.
	 */
	private String code;
	
	/**
	 * Descrizione.
	 */
	private String description;

	/**
	 * Costruttore.
	 * @param code
	 * @param description
	 */
	TipoGestioneEnum(final String code, final String description) {
		this.code = code;
		this.description = description;
	}
	
	/**
	 * Restituisce il codice.
	 * @return codice
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * Restituisce la descrizione.
	 * @return descrizione
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Restituisce l'enum associata al codice.
	 * @param inCode
	 * @return enum associata al codice
	 */
	public static TipoGestioneEnum get(final String inCode) {
		TipoGestioneEnum output = null;
		
		for (final TipoGestioneEnum e: TipoGestioneEnum.values()) {
			if (e.getCode().equalsIgnoreCase(inCode)) {
				output = e;
				break;
			}
		}
		
		return output;
	}

}
