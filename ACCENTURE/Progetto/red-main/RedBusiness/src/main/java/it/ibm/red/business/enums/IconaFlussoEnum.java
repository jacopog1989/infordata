package it.ibm.red.business.enums;

/**
 * The Enum IconaFlussoEnum.
 *
 * @author m.crescentini
 * 
 *         Icone per i flussi.
 */
public enum IconaFlussoEnum {
	
	/**
	 * Flusso ARGO.
	 */
	ARGO("Documento Argo", "A", "red");
	
	
	/**
	 * Titolo.
	 */
	private String title;
	
	/**
	 * Display name.
	 */
	private String displayName;
	 
	/**
	 * Colore icona.
	 */
	private String colorIcon; 
	
	/**
	 * @param label
	 */
	IconaFlussoEnum(final String title, final String displayName, final String colorIcon) {
		this.title = title;
		this.displayName = displayName;
		this.colorIcon = colorIcon;
	}


	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}


	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	
	/**
	 * @return the colorIcon
	 */
	public String getColorIcon() {
		return colorIcon;
	}
}
