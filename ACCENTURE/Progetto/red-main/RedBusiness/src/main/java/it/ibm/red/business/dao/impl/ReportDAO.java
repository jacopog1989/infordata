package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IReportDAO;
import it.ibm.red.business.dto.ParamsRicercaElencoDivisionaleDTO;
import it.ibm.red.business.dto.ReportDetailProtocolliPerCompetenzaDTO;
import it.ibm.red.business.dto.ReportDetailStatisticheUfficioUtenteDTO;
import it.ibm.red.business.dto.ReportDetailUfficioUtenteDTO;
import it.ibm.red.business.dto.ReportMasterUfficioUtenteDTO;
import it.ibm.red.business.dto.RisultatoRicercaElencoDivisionaleDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.UtenteRuoloReportDTO;
import it.ibm.red.business.enums.ReportQueryEnum;
import it.ibm.red.business.enums.TargetNodoReportEnum;
import it.ibm.red.business.enums.TipoOperazioneReportEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.CodaDiLavoroReport;
import it.ibm.red.business.persistence.model.DettaglioReport;
import it.ibm.red.business.persistence.model.TipoOperazioneReport;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * Dao per la gestione dei report.
 *
 * @author m.crescentini
 */
@Repository
public class ReportDAO extends AbstractDAO implements IReportDAO {

	/**
	 * Colonna utente assegnatario.
	 */
	private static final String UTENTE_ASSEGNATARIO = "utenteAssegnatario";

	/**
	 * Colonna ufficio assegnatario.
	 */
	private static final String UFFICIO_ASSEGNATARIO = "ufficioAssegnatario";

	/**
	 * Colonna data protocollo.
	 */
	private static final String DATA_PROTOCOLLO = "dataProtocollo";

	/**
	 * Colonna descrizione ufficio.
	 */
	private static final String DESCRIZIONE_UFFICIO = "descrizioneUfficio";

	/**
	 * Colonna Id documento.
	 */
	private static final String ID_DOCUMENTO = "idDocumento";

	/**
	 * Colonna Id Nodo.
	 */
	private static final String IDNODO = "idnodo";

	/**
	 * Colonna utente creatore.
	 */
	private static final String UTENTE_CREATORE = "utenteCreatore";

	/**
	 * Colonna data creazione.
	 */
	private static final String DATA_CREAZIONE = "dataCreazione";

	/**
	 * Colonna oggetto.
	 */
	private static final String OGGETTO = "oggetto";

	/**
	 * Colonna numero fascicolo.
	 */
	private static final String NUMERO_FASCICOLO = "numeroFascicolo";

	/**
	 * Colonna numero documento.
	 */
	private static final String NUMERO_DOCUMENTO = "numeroDocumento";

	/**
	 * Colonna numero protocollo.
	 */
	private static final String NUMERO_PROTOCOLLO = "numeroProtocollo";

	/**
	 * Colonna anno documento.
	 */
	private static final String ANNO_DOCUMENTO = "annoDocumento";

	/**
	 * Label report dao -> query.
	 */
	private static final String LOGINFO_REPORT_DAO_QUERY = "ReportDAO -> Query: ";

	/**
	 * Colonna id utente mittente.
	 */
	private static final String IDUTENTEASSEGNANTE = "idUtenteAssegnante";

	/**
	 * Colonna Id nodo mittente.
	 */
	private static final String IDUFFICIOASSEGNANTE = "idUfficioAssegnante";

	/**
	 * Colonna descrizione.
	 */
	private static final String DESCRIZIONE = "DESCRIZIONE";

	/**
	 * Colonna codice.
	 */
	private static final String CODICE = "CODICE";

	/**
	 * Formato data.
	 */
	private static final String FORMAT_DATA = "%1$td/%1$tm/%1$tY";

	/**
	 * And - label.
	 */
	private static final String AND = " and ";

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ReportDAO.class.getName());

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#getAllDettaglioReport(java.sql.Connection).
	 */
	@Override
	public final List<DettaglioReport> getAllDettaglioReport(final Connection con) {
		List<DettaglioReport> list = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = con.prepareStatement(getQuery(ReportQueryEnum.SQL_DETTAGLIO_REPORT_GET_ALL));
			rs = ps.executeQuery();

			list = new ArrayList<>();
			DettaglioReport item = null;
			while (rs.next()) {
				item = new DettaglioReport();
				item.setIdDettaglio(rs.getInt("ID_DETTAGLIO"));
				item.setCodice(rs.getString(CODICE));
				item.setDescrizione(rs.getString(DESCRIZIONE));
				list.add(item);
			}
		} catch (final SQLException e) {
			LOGGER.error("getAllDettaglioReport -> Si è verificato un errore durante il recupero della lista dei dettagli report.", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return list;
	}
	
	/**
	 * @see it.ibm.red.business.dao.IReportDAO#getTipiOperazioneReportByIdDettaglio(int,
	 *      long, java.sql.Connection).
	 */
	@Override
	public final List<TipoOperazioneReport> getTipiOperazioneReportByIdDettaglio(final int idDettaglioReport, final long idAoo, final Connection con)  {
		List<TipoOperazioneReport> list = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			list = new ArrayList<>();
			TipoOperazioneReport item = null;

			ps = con.prepareStatement(getQuery(ReportQueryEnum.SQL_TIPO_OPERAZIONE_REPORT_GET_ALL_BY_ID_DETTAGLIO_AND_ID_AOO));
			ps.setInt(1, idDettaglioReport);
			ps.setLong(2, idAoo);

			rs = ps.executeQuery();
			while (rs.next()) {
				item = new TipoOperazioneReport();
				item.setIdTipo(rs.getInt("ID_TIPO"));
				item.setCodice(rs.getString(CODICE));
				item.setDescrizione(rs.getString(DESCRIZIONE));
				item.setIdDettaglioReport(rs.getInt("ID_DETTAGLIO_REPORT"));
				list.add(item);
			}
		} catch (final SQLException e) {
			LOGGER.error(
					"getTipiOperazioneReportByIdDettaglio -> Si è verificato un errore durante il recupero dei tipi di operazione del dettaglio report: " + idDettaglioReport,
					e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return list;
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#getCodeDiLavoroReportByIdDettaglio(int,
	 *      java.sql.Connection).
	 */
	@Override
	public final List<CodaDiLavoroReport> getCodeDiLavoroReportByIdDettaglio(final int idDettaglioReport, final Connection con) {
		List<CodaDiLavoroReport> list = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			final int index = 1;
			ps = con.prepareStatement(getQuery(ReportQueryEnum.SQL_CODA_DI_LAVORO_REPORT_GET_ALL_BY_ID_DETTAGLIO));
			ps.setInt(index, idDettaglioReport);

			rs = ps.executeQuery();

			list = new ArrayList<>();
			CodaDiLavoroReport item = null;
			while (rs.next()) {
				item = new CodaDiLavoroReport();
				item.setIdCoda(rs.getInt("ID_CODA"));
				item.setCodice(rs.getString(CODICE));
				item.setDescrizione(rs.getString(DESCRIZIONE));
				item.setIdDettaglioReport(rs.getInt("ID_DETTAGLIO_REPORT"));
				list.add(item);
			}
		} catch (final SQLException e) {
			LOGGER.error("getCodeDiLavoroReportByIdDettaglio -> Si è verificato un errore durante il recupero delle code di lavoro del dettaglio report: " + idDettaglioReport,
					e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return list;
	}

	private static String replaceQueryPlaceholdersUfficiUtenti(final Long[] uffici, final Long[] utenti, final String query) {
		String outputQuery = query;

		StringBuilder sb = new StringBuilder(Constants.EMPTY_STRING);
		for (int i = 0; i < uffici.length; i++) {
			sb.append("?");
			if (i < uffici.length - 1) {
				sb.append(", ");
			}
		}
		outputQuery = outputQuery.replace("#IDNODO", sb.toString());

		if (utenti != null) {
			sb = new StringBuilder("");
			for (int i = 0; i < utenti.length; i++) {
				sb.append("?");
				if (i < utenti.length - 1) {
					sb.append(", ");
				}
			}
			outputQuery = outputQuery.replace("#IDUTENTE", sb.toString());
		}

		LOGGER.info(LOGINFO_REPORT_DAO_QUERY + outputQuery);
		return outputQuery;
	}

	private static void setQueryParametersUfficiUtentiDate(final PreparedStatement ps, final Long idAoo, final Long[] uffici, final Long[] utenti, final Calendar dataInizio,
			final Calendar dataFine) throws SQLException {
		int index = 1;

		if (idAoo != null) {
			ps.setLong(index++, idAoo);
		}

		// Si valorizzano i dati della where condition
		if (utenti != null) {
			for (int i = 0; i < utenti.length; i++) {
				ps.setLong(index++, utenti[i]);
			}
		}

		for (int i = 0; i < uffici.length; i++) {
			ps.setLong(index++, uffici[i]);
		}

		ps.setTimestamp(index++, new Timestamp(dataInizio.getTimeInMillis()));
		ps.setTimestamp(index, new Timestamp(dataFine.getTimeInMillis()));
	}

	private static String replaceQueryPlaceholdersUfficiRuoli(final Long[] uffici, final Long[] ruoli, final String inQuery) {
		String outputQuery = inQuery;

		StringBuilder sb = new StringBuilder("");
		for (int i = 0; i < ruoli.length; i++) {
			sb.append("?");
			if (i < ruoli.length - 1) {
				sb.append(", ");
			}
		}
		outputQuery = outputQuery.replace("#IDRUOLO", sb.toString());

		sb = new StringBuilder("");
		for (int i = 0; i < uffici.length; i++) {
			sb.append("?");
			if (i < uffici.length - 1) {
				sb.append(", ");
			}
		}
		outputQuery = outputQuery.replace("#IDNODO", sb.toString());

		LOGGER.info(LOGINFO_REPORT_DAO_QUERY + outputQuery);
		return outputQuery;
	}

	private static void setQueryParametersUfficiRuoli(final PreparedStatement ps, final Long[] uffici, final Long[] ruoli) throws SQLException {
		int index = 1;

		// Si valorizzano i dati della where condition
		for (int i = 0; i < ruoli.length; i++) {
			ps.setLong(index++, ruoli[i]);
		}
		for (int i = 0; i < uffici.length; i++) {
			ps.setLong(index++, uffici[i]);
		}
	}
	
	/**
	 * Imposta i parametri della query per il report protocolli (pervenuti) per
	 * competenza.
	 * 
	 * @param ps
	 * @param idAoo
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @throws SQLException
	 */
	private static void setQueryParametersProtocolliPervenutiCompetenza(final PreparedStatement ps, final Long idAoo, final Long[] uffici, final Long[] utenti, final Calendar dataInizio,
			final Calendar dataFine) throws SQLException {
		int index = 1;

		if (idAoo != null) {
			ps.setLong(index++, idAoo);
			ps.setLong(index++, idAoo);
			ps.setLong(index++, idAoo);
			ps.setLong(index++, idAoo);
		}
		
		ps.setTimestamp(index++, new Timestamp(dataInizio.getTimeInMillis()));
		ps.setTimestamp(index++, new Timestamp(dataFine.getTimeInMillis()));
		
		for (int i = 0; i < uffici.length; i++) {
			ps.setLong(index++, uffici[i]);
		}
		
		if (utenti != null) {
			for (int i = 0; i < utenti.length; i++) {
				ps.setLong(index++, utenti[i]);
			}
		}

		if (idAoo != null) {
			ps.setLong(index++, idAoo);
			ps.setLong(index++, idAoo);
			ps.setLong(index++, idAoo);
			ps.setLong(index, idAoo);
		}
	}
	
	/**
	 * Imposta i parametri della query per il report protocolli (inevasi) per competenza.
	 * 
	 * @param ps
	 * @param idAoo
	 * @param uffici
	 * @param utenti
	 * @throws SQLException
	 */
	private static void setQueryParametersProtocolliInevasiCompetenza(final PreparedStatement ps, final Long idAoo, final Long[] uffici, final Long[] utenti, final Calendar dataFine) throws SQLException {
		int index = 1;

		index = setParametersCommon(ps, idAoo, uffici, utenti, null, dataFine, index);
		
		ps.setTimestamp(index, new Timestamp(dataFine.getTimeInMillis()));
	}
	
	/**
	 * Imposta i parametri della query per il report protocolli (lavorati) per competenza.
	 * 
	 * @param ps
	 * @param idAoo
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @throws SQLException
	 */
	private static void setQueryParametersProtocolliLavoratiCompetenza(final PreparedStatement ps, final Long idAoo, final Long[] uffici, final Long[] utenti, final Calendar dataInizio,
			final Calendar dataFine) throws SQLException {
		int index = 1;

		index = setParametersCommon(ps, idAoo, uffici, utenti, dataInizio, dataFine, index);

		ps.setTimestamp(index++, new Timestamp(dataInizio.getTimeInMillis()));
		ps.setTimestamp(index, new Timestamp(dataFine.getTimeInMillis()));
	}
	
	/**
	 * @param ps
	 * @param idAoo
	 * @param uffici
	 * @param utenti
	 * @param index
	 * @return
	 * @throws SQLException
	 */
	private static int setParametersCommon(final PreparedStatement ps, final Long idAoo, final Long[] uffici,
			final Long[] utenti, final Calendar dataInizio, final Calendar dataFine, int index) throws SQLException {
		
		if (idAoo != null) {
			ps.setLong(index++, idAoo);
			ps.setLong(index++, idAoo);
			ps.setLong(index++, idAoo);
			ps.setLong(index++, idAoo);
		}
		
		if(dataInizio != null) {
			ps.setTimestamp(index++, new Timestamp(dataInizio.getTimeInMillis()));
		}
		
		ps.setTimestamp(index++, new Timestamp(dataFine.getTimeInMillis()));

		for (int i = 0; i < uffici.length; i++) {
			ps.setLong(index++, uffici[i]);
		}
		
		if (utenti != null) {
			for (int i = 0; i < utenti.length; i++) {
				ps.setLong(index++, utenti[i]);
			}
		}
		
		if (idAoo != null) {
			ps.setLong(index++, idAoo);
			ps.setLong(index++, idAoo);
			ps.setLong(index++, idAoo);
			ps.setLong(index++, idAoo);
		}
		
		return index;
	}
	
	/**
	 * Sostituisce i placeholders tipi evento della query per il report elenco divisionale.
	 * 
	 * @param tipiEvento
	 * @param inQuery
	 * @return query
	 */
	private static String replaceQueryPlaceholdersTipiEvento(final Integer[] tipiEvento, final String inQuery) {
		String outputQuery = inQuery;

		StringBuilder sb = new StringBuilder("");
		for (int i = 0; i < tipiEvento.length; i++) {
			sb.append("?");
			if (i < tipiEvento.length - 1) {
				sb.append(", ");
			}
		}
		outputQuery = outputQuery.replace("#TIPOEVENTO", sb.toString());
		
		LOGGER.info(LOGINFO_REPORT_DAO_QUERY + outputQuery);
		return outputQuery;
	}
	
	/**
	 * Imposta i parametri della query per il report elenco divisionale.
	 * 
	 * @param ps
	 * @param idAoo
	 * @param parametriElencoDivisionale
	 * @param uffici
	 * @param utenti
	 * @param idTipologiaDocumento
	 * @throws SQLException
	 */
	private static void setQueryParametersElencoDivisionale(final PreparedStatement ps, final Long idAoo, final ParamsRicercaElencoDivisionaleDTO parametriElencoDivisionale, 
			final Long[] uffici, final Long[] utenti, final Integer[] tipiEvento) throws SQLException {
		
		int index = 1;

		if (idAoo != null) {
			ps.setLong(index++, idAoo);
			ps.setLong(index++, idAoo);
		}
		
		for (int i = 0; i < tipiEvento.length; i++) {
			ps.setInt(index++, tipiEvento[i]);
		}
		
		final Calendar dataAssegnazioneCal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		dataAssegnazioneCal.setTime(parametriElencoDivisionale.getDataAssegnazione());
		ps.setTimestamp(index++, new Timestamp(dataAssegnazioneCal.getTimeInMillis()));
		
		ps.setLong(index++, parametriElencoDivisionale.getUfficioAssegnante().getId());
			
		for (int i = 0; i < uffici.length; i++) {
			ps.setLong(index++, uffici[i]);
		}
		
		if (utenti != null) {
			for (int i = 0; i < utenti.length; i++) {
				ps.setLong(index++, utenti[i]);
			}
		}
		
		if (idAoo != null) {
			ps.setLong(index++, idAoo);
			ps.setLong(index++, idAoo);
			ps.setLong(index, idAoo);
		}		
		
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#countDocEntrataGroupByUfficioUtente(java.lang.Long[],
	 *      java.lang.Long[], java.util.Calendar, java.util.Calendar,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<ReportMasterUfficioUtenteDTO> countDocEntrataGroupByUfficioUtente(final Long[] uffici, final Long[] utenti, final Calendar dataInizio, final Calendar dataFine,
			final Long idAoo, final Connection con) {
		LOGGER.info("Esecuzione -> countDocEntrataGroupByUfficioUtente");
		return executeMasterQuery(ReportQueryEnum.SQL_REPORT_DOC_ENTRATA_UFFICIO_GROUPBY, ReportQueryEnum.SQL_REPORT_DOC_ENTRATA_UFFICIOUTENTE_GROUPBY, uffici, utenti,
				dataInizio, dataFine, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#docEntrataGroupByUfficioUtente(java.lang.Long[],
	 *      java.lang.Long[], java.util.Calendar, java.util.Calendar,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<ReportDetailUfficioUtenteDTO> docEntrataGroupByUfficioUtente(final Long[] uffici, final Long[] utenti, final Calendar dataInizio, final Calendar dataFine,
			final Long idAoo, final Connection con) {
		LOGGER.info("Esecuzione -> docEntrataGroupByUfficioUtente");
		return executeDetailQuery(ReportQueryEnum.SQL_REPORT_DOC_ENTRATA_UFFICIO, ReportQueryEnum.SQL_REPORT_DOC_ENTRATA_UFFICIOUTENTE, uffici, utenti, dataInizio, dataFine,
				false, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#countDocUscitaGroupByUfficioUtente(java.lang.Long[],
	 *      java.lang.Long[], java.util.Calendar, java.util.Calendar,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<ReportMasterUfficioUtenteDTO> countDocUscitaGroupByUfficioUtente(final Long[] uffici, final Long[] utenti, final Calendar dataInizio, final Calendar dataFine,
			final Long idAoo, final Connection con) {
		LOGGER.info("Esecuzione -> countDocUscitaGroupByUfficioUtente");
		return executeMasterQuery(ReportQueryEnum.SQL_REPORT_DOC_USCITA_UFFICIO_GROUPBY, ReportQueryEnum.SQL_REPORT_DOC_USCITA_UFFICIOUTENTE_GROUPBY, uffici, utenti,
				dataInizio, dataFine, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#docUscitaGroupByUfficioUtente(java.lang.Long[],
	 *      java.lang.Long[], java.util.Calendar, java.util.Calendar,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<ReportDetailUfficioUtenteDTO> docUscitaGroupByUfficioUtente(final Long[] uffici, final Long[] utenti, final Calendar dataInizio, final Calendar dataFine,
			final Long idAoo, final Connection con) {
		LOGGER.info("Esecuzione -> docUscitaGroupByUfficioUtente");
		return executeDetailQuery(ReportQueryEnum.SQL_REPORT_DOC_USCITA_UFFICIO, ReportQueryEnum.SQL_REPORT_DOC_USCITA_UFFICIOUTENTE, uffici, utenti, dataInizio, dataFine,
				false, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#countDocFirmatiGroupByUfficioUtente(java.lang.Long[],
	 *      java.lang.Long[], java.util.Calendar, java.util.Calendar,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<ReportMasterUfficioUtenteDTO> countDocFirmatiGroupByUfficioUtente(final Long[] uffici, final Long[] utenti, final Calendar dataInizio, final Calendar dataFine,
			final Long idAoo, final Connection con) {
		LOGGER.info("Esecuzione -> countDocFirmatiGroupByUfficioUtente");
		return executeMasterQuery(ReportQueryEnum.SQL_REPORT_DOC_FIRMATI_UFFICIO_GROUPBY, ReportQueryEnum.SQL_REPORT_DOC_FIRMATI_UFFICIOUTENTE_GROUPBY, uffici, utenti,
				dataInizio, dataFine, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#docFirmatiGroupByUfficioUtente(java.lang.Long[],
	 *      java.lang.Long[], java.util.Calendar, java.util.Calendar,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<ReportDetailUfficioUtenteDTO> docFirmatiGroupByUfficioUtente(final Long[] uffici, final Long[] utenti, final Calendar dataInizio, final Calendar dataFine,
			final Long idAoo, final Connection con) {
		LOGGER.info("esecuzione ReportDao.docFirmatiGroupByUfficioUtente");
		return executeDetailQuery(ReportQueryEnum.SQL_REPORT_DOC_FIRMATI_UFFICIO, ReportQueryEnum.SQL_REPORT_DOC_FIRMATI_UFFICIOUTENTE, uffici, utenti, dataInizio, dataFine,
				false, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#countDocSiglatiGroupByUfficioUtente(java.lang.Long[],
	 *      java.lang.Long[], java.util.Calendar, java.util.Calendar,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<ReportMasterUfficioUtenteDTO> countDocSiglatiGroupByUfficioUtente(final Long[] uffici, final Long[] utenti, final Calendar dataInizio, final Calendar dataFine,
			final Long idAoo, final Connection con) {
		LOGGER.info("Esecuzione -> countDocSiglatiGroupByUfficioUtente");
		return executeMasterQuery(ReportQueryEnum.SQL_REPORT_DOC_SIGLATI_UFFICIO_GROUPBY, ReportQueryEnum.SQL_REPORT_DOC_SIGLATI_UFFICIOUTENTE_GROUPBY, uffici, utenti,
				dataInizio, dataFine, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#docSiglatiGroupByUfficioUtente(java.lang.Long[],
	 *      java.lang.Long[], java.util.Calendar, java.util.Calendar,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<ReportDetailUfficioUtenteDTO> docSiglatiGroupByUfficioUtente(final Long[] uffici, final Long[] utenti, final Calendar dataInizio, final Calendar dataFine,
			final Long idAoo, final Connection con) {
		LOGGER.info("Esecuzione -> docSiglatiGroupByUfficioUtente");
		return executeDetailQuery(ReportQueryEnum.SQL_REPORT_DOC_SIGLATI_UFFICIO, ReportQueryEnum.SQL_REPORT_DOC_SIGLATI_UFFICIOUTENTE, uffici, utenti, dataInizio, dataFine,
				false, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#countDocVistatiGroupByUfficioUtente(java.lang.Long[],
	 *      java.lang.Long[], java.util.Calendar, java.util.Calendar,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<ReportMasterUfficioUtenteDTO> countDocVistatiGroupByUfficioUtente(final Long[] uffici, final Long[] utenti, final Calendar dataInizio, final Calendar dataFine,
			final Long idAoo, final Connection con) {
		LOGGER.info("Esecuzione -> countDocVistatiGroupByUfficioUtente");
		return executeMasterQuery(ReportQueryEnum.SQL_REPORT_DOC_VISTATI_UFFICIO_GROUPBY, ReportQueryEnum.SQL_REPORT_DOC_VISTATI_UFFICIOUTENTE_GROUPBY, uffici, utenti,
				dataInizio, dataFine, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#docVistatiGroupByUfficioUtente(java.lang.Long[],
	 *      java.lang.Long[], java.util.Calendar, java.util.Calendar,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<ReportDetailUfficioUtenteDTO> docVistatiGroupByUfficioUtente(final Long[] uffici, final Long[] utenti, final Calendar dataInizio, final Calendar dataFine,
			final Long idAoo, final Connection con) {
		LOGGER.info("Esecuzione -> docVistatiGroupByUfficioUtente");
		return executeDetailQuery(ReportQueryEnum.SQL_REPORT_DOC_VISTATI_UFFICIO, ReportQueryEnum.SQL_REPORT_DOC_VISTATI_UFFICIOUTENTE, uffici, utenti, dataInizio, dataFine,
				false, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#countDocMessiAgliAttiGroupByUfficioUtente(java.lang.Long[],
	 *      java.lang.Long[], java.util.Calendar, java.util.Calendar,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<ReportMasterUfficioUtenteDTO> countDocMessiAgliAttiGroupByUfficioUtente(final Long[] uffici, final Long[] utenti, final Calendar dataInizio,
			final Calendar dataFine, final Long idAoo, final Connection con) {
		LOGGER.info("Esecuzione -> countDocMessiAgliAttiGroupByUfficioUtente");
		return executeMasterQuery(ReportQueryEnum.SQL_REPORT_DOC_MESSI_AGLI_ATTI_UFFICIO_GROUPBY, ReportQueryEnum.SQL_REPORT_DOC_MESSI_AGLI_ATTI_UFFICIOUTENTE_GROUPBY, uffici,
				utenti, dataInizio, dataFine, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#docMessiAgliAttiGroupByUfficioUtente(java.lang.Long[],
	 *      java.lang.Long[], java.util.Calendar, java.util.Calendar,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<ReportDetailUfficioUtenteDTO> docMessiAgliAttiGroupByUfficioUtente(final Long[] uffici, final Long[] utenti, final Calendar dataInizio,
			final Calendar dataFine, final Long idAoo, final Connection con) {
		LOGGER.info("Esecuzione -> docMessiAgliAttiGroupByUfficioUtente");
		return executeDetailQuery(ReportQueryEnum.SQL_REPORT_DOC_MESSI_AGLI_ATTI_UFFICIO, ReportQueryEnum.SQL_REPORT_DOC_MESSI_AGLI_ATTI_UFFICIOUTENTE, uffici, utenti,
				dataInizio, dataFine, false, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#countDocContributoInseritoGroupByUfficioUtente(java.lang.Long[],
	 *      java.lang.Long[], java.util.Calendar, java.util.Calendar,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<ReportMasterUfficioUtenteDTO> countDocContributoInseritoGroupByUfficioUtente(final Long[] uffici, final Long[] utenti, final Calendar dataInizio,
			final Calendar dataFine, final Long idAoo, final Connection con) {
		LOGGER.info("Esecuzione -> countDocContributoInseritoGroupByUfficioUtente");
		return executeMasterQuery(ReportQueryEnum.SQL_REPORT_DOC_CONTRIBUTO_INS_UFFICIO_GROUPBY, ReportQueryEnum.SQL_REPORT_DOC_CONTRIBUTO_INS_UFFICIOUTENTE_GROUPBY, uffici,
				utenti, dataInizio, dataFine, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#docContributoInseritoGroupByUfficioUtente(java.lang.Long[],
	 *      java.lang.Long[], java.util.Calendar, java.util.Calendar,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<ReportDetailUfficioUtenteDTO> docContributoInseritoGroupByUfficioUtente(final Long[] uffici, final Long[] utenti, final Calendar dataInizio,
			final Calendar dataFine, final Long idAoo, final Connection con) {
		LOGGER.info("Esecuzione -> docContributoInseritoGroupByUfficioUtente");
		return executeDetailQuery(ReportQueryEnum.SQL_REPORT_DOC_CONTRIBUTO_INS_UFFICIO, ReportQueryEnum.SQL_REPORT_DOC_CONTRIBUTO_INS_UFFICIOUTENTE, uffici, utenti,
				dataInizio, dataFine, false, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#countCodaLibroFirmaByUfficioUtente(java.lang.Long[],
	 *      java.lang.Long[], java.util.Calendar, java.util.Calendar,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<ReportMasterUfficioUtenteDTO> countCodaLibroFirmaByUfficioUtente(final Long[] uffici, final Long[] utenti, final Calendar dataInizio, final Calendar dataFine,
			final Long idAoo, final Connection con) {
		LOGGER.info("Esecuzione -> countCodaLibroFirmaByUfficioUtente");
		return executeMasterQuery(null, ReportQueryEnum.SQL_REPORT_CODA_LIBRO_FIRMA_MASTER, uffici, utenti, dataInizio, dataFine, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#docInCodaLibroFirmaGroupByUfficioUtente(java.lang.Long[],
	 *      java.lang.Long[], java.util.Calendar, java.util.Calendar,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<ReportDetailUfficioUtenteDTO> docInCodaLibroFirmaGroupByUfficioUtente(final Long[] uffici, final Long[] utenti, final Calendar dataInizio,
			final Calendar dataFine, final Long idAoo, final Connection con) {
		LOGGER.info("Esecuzione -> docInCodaLibroFirmaGroupByUfficioUtente");
		return executeDetailQuery(null, ReportQueryEnum.SQL_REPORT_CODA_LIBRO_FIRMA_DETAIL, uffici, utenti, dataInizio, dataFine, true, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#countCodaDaLavorareByUfficioUtente(java.lang.Long[],
	 *      java.lang.Long[], java.util.Calendar, java.util.Calendar,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<ReportMasterUfficioUtenteDTO> countCodaDaLavorareByUfficioUtente(final Long[] uffici, final Long[] utenti, final Calendar dataInizio, final Calendar dataFine,
			final Long idAoo, final Connection con) {
		LOGGER.info("Esecuzione -> countCodaDaLavorareByUfficioUtente");
		return executeMasterQuery(null, ReportQueryEnum.SQL_REPORT_CODA_DA_LAVORARE_MASTER, uffici, utenti, dataInizio, dataFine, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#docInCodaDaLavorareGroupByUfficioUtente(java.lang.Long[],
	 *      java.lang.Long[], java.util.Calendar, java.util.Calendar,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<ReportDetailUfficioUtenteDTO> docInCodaDaLavorareGroupByUfficioUtente(final Long[] uffici, final Long[] utenti, final Calendar dataInizio,
			final Calendar dataFine, final Long idAoo, final Connection con) {
		LOGGER.info("Esecuzione -> docInCodaDaLavorareGroupByUfficioUtente");
		return executeDetailQuery(null, ReportQueryEnum.SQL_REPORT_CODA_DA_LAVORARE_DETAIL, uffici, utenti, dataInizio, dataFine, true, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#countCodaInSospesoByUfficioUtente(java.lang.Long[],
	 *      java.lang.Long[], java.util.Calendar, java.util.Calendar,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<ReportMasterUfficioUtenteDTO> countCodaInSospesoByUfficioUtente(final Long[] uffici, final Long[] utenti, final Calendar dataInizio, final Calendar dataFine,
			final Long idAoo, final Connection con) {
		LOGGER.info("Esecuzione -> countCodaInSospesoByUfficioUtente");
		return executeMasterQuery(null, ReportQueryEnum.SQL_REPORT_CODA_IN_SOSPESO_MASTER, uffici, utenti, dataInizio, dataFine, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#docInCodaInSospesoGroupByUfficioUtente(java.lang.Long[],
	 *      java.lang.Long[], java.util.Calendar, java.util.Calendar,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<ReportDetailUfficioUtenteDTO> docInCodaInSospesoGroupByUfficioUtente(final Long[] uffici, final Long[] utenti, final Calendar dataInizio,
			final Calendar dataFine, final Long idAoo, final Connection con) {
		LOGGER.info("Esecuzione -> docInCodaInSospesoGroupByUfficioUtente");
		return executeDetailQuery(null, ReportQueryEnum.SQL_REPORT_CODA_IN_SOSPESO_DETAIL, uffici, utenti, dataInizio, dataFine, true, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#countCodaCorriereGroupByUfficio(java.lang.Long[],
	 *      java.util.Calendar, java.util.Calendar, java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public List<ReportMasterUfficioUtenteDTO> countCodaCorriereGroupByUfficio(final Long[] uffici, final Calendar dataInizio, final Calendar dataFine, final Long idAoo,
			final Connection con) {
		LOGGER.info("Esecuzione -> countCodaCorriereGroupByUfficio");
		return executeMasterQuery(ReportQueryEnum.SQL_REPORT_CODA_CORRIERE_UFFICIO_GROUPBY, null, uffici, null, dataInizio, dataFine, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#docInCodaCorriereUfficio(java.lang.Long[],
	 *      java.util.Calendar, java.util.Calendar, java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public List<ReportDetailUfficioUtenteDTO> docInCodaCorriereUfficio(final Long[] uffici, final Calendar dataInizio, final Calendar dataFine, final Long idAoo,
			final Connection con) {
		LOGGER.info("Esecuzione -> docInCodaCorriereUfficio");
		return executeDetailQuery(ReportQueryEnum.SQL_REPORT_CODA_CORRIERE_UFFICIO, null, uffici, null, dataInizio, dataFine, true, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#countCodaSpedizioneGroupByUfficio(java.lang.Long[],
	 *      java.util.Calendar, java.util.Calendar, java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public List<ReportMasterUfficioUtenteDTO> countCodaSpedizioneGroupByUfficio(final Long[] uffici, final Calendar dataInizio, final Calendar dataFine, final Long idAoo,
			final Connection con) {
		LOGGER.info("Esecuzione -> countCodaSpedizioneGroupByUfficio");
		return executeMasterQuery(ReportQueryEnum.SQL_REPORT_CODA_SPEDIZIONE_UFFICIO_GROUPBY, null, uffici, null, dataInizio, dataFine, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#docInCodaSpedizioneUfficio(java.lang.Long[],
	 *      java.util.Calendar, java.util.Calendar, java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public List<ReportDetailUfficioUtenteDTO> docInCodaSpedizioneUfficio(final Long[] uffici, final Calendar dataInizio, final Calendar dataFine, final Long idAoo,
			final Connection con) {
		LOGGER.info("Esecuzione -> docInCodaSpedizioneUfficio");
		return executeDetailQuery(ReportQueryEnum.SQL_REPORT_CODA_SPEDIZIONE_UFFICIO, null, uffici, null, dataInizio, dataFine, true, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#countCodaChiusiGroupByUfficio(java.lang.Long[],
	 *      java.util.Calendar, java.util.Calendar, java.sql.Connection).
	 */
	@Override
	public List<ReportMasterUfficioUtenteDTO> countCodaChiusiGroupByUfficio(final Long[] uffici, final Calendar dataInizio, final Calendar dataFine, final Connection con) {
		LOGGER.info("Esecuzione -> countCodaChiusiGroupByUfficio");
		return executeMasterQuery(ReportQueryEnum.SQL_REPORT_CODA_CHIUSI_UFFICIO_GROUPBY, null, uffici, null, dataInizio, dataFine, null, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#idDocInCodaChiusiByUfficio(java.lang.Long[],
	 *      java.util.Calendar, java.util.Calendar, java.sql.Connection).
	 */
	@Override
	public List<ReportDetailUfficioUtenteDTO> idDocInCodaChiusiByUfficio(final Long[] uffici, final Calendar dataInizio, final Calendar dataFine, final Connection con) {
		LOGGER.info("Esecuzione -> idDocInCodaChiusiByUfficio");
		List<ReportDetailUfficioUtenteDTO> list = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		String query = getQuery(ReportQueryEnum.SQL_REPORT_CODA_CHIUSI_UFFICIO_IDDOC);

		return replaceAndExecuteQueryDocInCoda(uffici, null, dataInizio, dataFine, con, list, ps, rs, query);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#docInCodaChiusiByUfficio(java.lang.String[],
	 *      java.util.Calendar, java.util.Calendar, java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public Map<String, ReportDetailUfficioUtenteDTO> docInCodaChiusiByUfficio(final String[] idDocumenti, final Calendar dataInizio, final Calendar dataFine, final Long idAoo,
			final Connection con) {
		LOGGER.info("Esecuzione -> docInCodaChiusiByUfficio");
		Map<String, ReportDetailUfficioUtenteDTO> mapDocInCodaChiusiByUfficio = null;
		ResultSet rs = null;

		try {
			if (idDocumenti == null || dataInizio == null || dataFine == null) {
				LOGGER.error("È necessario passare almeno un documento e le data di inizio e fine");
			} else {
				String query = getQuery(ReportQueryEnum.SQL_REPORT_CODA_CHIUSI_UFFICIO);

				rs = queryBuilderAndExecute(idDocumenti, idAoo, con, query);

				mapDocInCodaChiusiByUfficio = new HashMap<>();
				while (rs.next()) {
					populateMapReport(mapDocInCodaChiusiByUfficio, rs);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage());
		} finally {
			closeResultset(rs);
		}

		return mapDocInCodaChiusiByUfficio;
	}

	/**
	 * @param idDocumenti
	 * @param idAoo
	 * @param con
	 * @param query
	 * @return
	 * @throws SQLException
	 */
	private static ResultSet queryBuilderAndExecute(final String[] idDocumenti, final Long idAoo, final Connection con, String query) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs;
		final StringBuilder sb = new StringBuilder("");

		for (int i = 0; i < idDocumenti.length; i++) {
			sb.append("dv.documenttitle = ?");
			if (i < idDocumenti.length - 1) {
				sb.append(" OR ");
			}
		}
		query = query.replace("#IDDOCUMENTO", sb.toString());

		int index = 1;
		try {
			ps = con.prepareStatement(query);
			
			ps.setLong(index++, idAoo);

			// Si valorizzano i dati della where condition
			for (int i = 0; i < idDocumenti.length; i++) {
				ps.setString(index++, idDocumenti[i]);
			}

			LOGGER.info(query);

			rs = ps.executeQuery();
		} finally {
			closeStatement(ps);
		}
		return rs;
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#countCodaAttiGroupByUfficioUtente(java.lang.Long[],
	 *      java.lang.Long[], java.util.Calendar, java.util.Calendar,
	 *      java.sql.Connection).
	 */
	@Override
	public List<ReportMasterUfficioUtenteDTO> countCodaAttiGroupByUfficioUtente(final Long[] uffici, final Long[] utenti, final Calendar dataInizio, final Calendar dataFine,
			final Connection con) {
		LOGGER.info("Esecuzione -> countCodaAttiByUfficioUtente");
		return executeMasterQuery(null, ReportQueryEnum.SQL_REPORT_CODA_ATTI_UFFICIOUTENTE_GROUPBY, uffici, utenti, dataInizio, dataFine, null, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#idDocInCodaAttiByUfficioUtente(java.lang.Long[],
	 *      java.lang.Long[], java.util.Calendar, java.util.Calendar, java.sql.Connection).
	 */
	@Override
	public List<ReportDetailUfficioUtenteDTO> idDocInCodaAttiByUfficioUtente(final Long[] uffici, final Long[] utenti, final Calendar dataInizio, final Calendar dataFine, final Connection con) {
		LOGGER.info("Esecuzione -> idDocInCodaAttiByUfficioUtente");
		List<ReportDetailUfficioUtenteDTO> list = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		String query = getQuery(ReportQueryEnum.SQL_REPORT_CODA_ATTI_UFFICIOUTENTE_IDDOC);

		return replaceAndExecuteQueryDocInCoda(uffici, utenti, dataInizio, dataFine, con, list, ps, rs, query);
	}

	/**
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param con
	 * @param list
	 * @param ps
	 * @param rs
	 * @param query
	 * @return
	 */
	private static List<ReportDetailUfficioUtenteDTO> replaceAndExecuteQueryDocInCoda(final Long[] uffici, final Long[] utenti,
			final Calendar dataInizio, final Calendar dataFine, final Connection con,
			List<ReportDetailUfficioUtenteDTO> list, PreparedStatement ps, ResultSet rs, String query) {
		try {
			query = replaceQueryPlaceholdersUfficiUtenti(uffici, utenti, query);

			ps = con.prepareStatement(query);
			setQueryParametersUfficiUtentiDate(ps, null, uffici, utenti, dataInizio, dataFine);

			rs = ps.executeQuery();

			list = new ArrayList<>();
			ReportDetailUfficioUtenteDTO r = null;
			while (rs.next()) {
				r = new ReportDetailUfficioUtenteDTO();
				r.setIdDocumento(rs.getString(ID_DOCUMENTO));
				r.setDescrizione(rs.getString(DESCRIZIONE_UFFICIO));

				list.add(r);
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage());
		} finally {
			closeStatement(ps, rs);
		}

		return list;
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#docInCodaAttiByUfficioUtente(java.lang.String[],
	 *      java.util.Calendar, java.util.Calendar, java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public Map<String, ReportDetailUfficioUtenteDTO> docInCodaAttiByUfficioUtente(final String[] idDocumenti, final Calendar dataInizio, final Calendar dataFine,
			final Long idAoo, final Connection con) {
		LOGGER.info("Esecuzione -> docInCodaAttiByUfficioUtente");
		Map<String, ReportDetailUfficioUtenteDTO> mapDocInCodaAttiByUfficioUtente = null;
		ResultSet rs = null;

		try {
			if (idDocumenti == null || dataInizio == null || dataFine == null) {
				LOGGER.error("È necessario passare almeno un documento e le data di inizio e fine");
			} else {
				String query = getQuery(ReportQueryEnum.SQL_REPORT_CODA_ATTI_UFFICIOUTENTE);

				rs = queryBuilderAndExecute(idDocumenti, idAoo, con, query);

				mapDocInCodaAttiByUfficioUtente = new HashMap<>();
				while (rs.next()) {
					populateMapReport(mapDocInCodaAttiByUfficioUtente, rs);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage());
		} finally {
			closeResultset(rs);
		}

		return mapDocInCodaAttiByUfficioUtente;
	}

	/**
	 * @param mapDocInCoda
	 * @param rs
	 * @throws SQLException
	 */
	private static void populateMapReport(Map<String, ReportDetailUfficioUtenteDTO> mapDocInCoda, ResultSet rs) throws SQLException {
		final String idDocumento = rs.getString(ID_DOCUMENTO);
		ReportDetailUfficioUtenteDTO r = new ReportDetailUfficioUtenteDTO();
		
		r.setIdDocumento(idDocumento);
		r.setNumeroProtocollo(rs.getString(NUMERO_PROTOCOLLO));
		r.setNumeroDocumento(rs.getString(NUMERO_DOCUMENTO));
		r.setAnnoDocumento(rs.getString(ANNO_DOCUMENTO));
		r.setNumeroFascicolo(rs.getString(NUMERO_FASCICOLO));
		r.setOggetto(rs.getString(OGGETTO));
		r.setOggettoBreve(StringUtils.getTextAbstract(r.getOggetto(), 200));
		r.setDataCreazione(String.format(FORMAT_DATA, rs.getTimestamp(DATA_CREAZIONE)));
		r.setUtenteCreatore(rs.getString(UTENTE_CREATORE));
		r.setAnno(rs.getString("anno"));

		mapDocInCoda.put(idDocumento, r);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#countUtentiAttiviConUnDatoRuolo(java.lang.Long[],
	 *      java.lang.Long[], java.sql.Connection).
	 */
	@Override
	public List<UtenteRuoloReportDTO> countUtentiAttiviConUnDatoRuolo(final Long[] uffici, final Long[] ruoli, final Connection con) {
		LOGGER.info("Esecuzione -> masterUtentiAttiviConUnDatoRuolo");
		List<UtenteRuoloReportDTO> list = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String query = getQuery(ReportQueryEnum.SQL_REPORT_MASTER_UTENTI_ATTIVI_CON_UN_DATO_RUOLO);

			query = replaceQueryPlaceholdersUfficiRuoli(uffici, ruoli, query);

			ps = con.prepareStatement(query);
			setQueryParametersUfficiRuoli(ps, uffici, ruoli);

			rs = ps.executeQuery();

			list = new ArrayList<>();
			UtenteRuoloReportDTO r = null;
			while (rs.next()) {
				r = new UtenteRuoloReportDTO();

				r.setIdUfficio(rs.getLong(IDNODO));
				r.setUfficio(rs.getString("ufficio"));
				r.setIdRuolo(rs.getLong("idruolo"));
				r.setRuolo(rs.getString("nomeruolo"));
				r.setCount(rs.getLong("countUtenti"));

				list.add(r);
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage());
		} finally {
			closeStatement(ps, rs);
		}

		return list;
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#detailUtentiAttiviConUnDatoRuolo(java.lang.Long[],
	 *      java.lang.Long[], java.sql.Connection).
	 */
	@Override
	public List<UtenteRuoloReportDTO> detailUtentiAttiviConUnDatoRuolo(final Long[] uffici, final Long[] ruoli, final Connection con) {
		LOGGER.info("Esecuzione -> detailUtentiAttiviConUnDatoRuolo");
		List<UtenteRuoloReportDTO> list = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String query = getQuery(ReportQueryEnum.SQL_REPORT_DETAIL_UTENTI_ATTIVI_CON_UN_DATO_RUOLO);

			query = replaceQueryPlaceholdersUfficiRuoli(uffici, ruoli, query);

			ps = con.prepareStatement(query);
			setQueryParametersUfficiRuoli(ps, uffici, ruoli);

			rs = ps.executeQuery();

			list = new ArrayList<>();
			UtenteRuoloReportDTO r = null;
			while (rs.next()) {
				r = new UtenteRuoloReportDTO();

				r.setIdUfficio(rs.getLong(IDNODO));
				r.setUfficio(rs.getString("ufficio"));
				r.setIdRuolo(rs.getLong("idruolo"));
				r.setRuolo(rs.getString("nomeruolo"));
				r.setIdUtente(rs.getLong("idutente"));
				r.setUsername(rs.getString("username"));
				r.setNome(rs.getString("nome"));
				r.setCognome(rs.getString("cognome"));

				list.add(r);
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage());
		} finally {
			closeStatement(ps, rs);
		}

		return list;
	}

	private static List<ReportMasterUfficioUtenteDTO> executeMasterQuery(final ReportQueryEnum queryUffici, final ReportQueryEnum queryUfficiUtenti, final Long[] uffici,
			final Long[] utenti, final Calendar dataInizio, final Calendar dataFine, final Long idAoo, final Connection con) {
		List<ReportMasterUfficioUtenteDTO> risultatiMaster = null;
		ResultSet rs = null;
		PreparedStatement ps = null;

		try {
			
			String query = fillPlaceholders(queryUffici, queryUfficiUtenti, uffici, utenti);
			ps = con.prepareStatement(query);
			setQueryParametersUfficiUtentiDate(ps, idAoo, uffici, utenti, dataInizio, dataFine);

			rs = ps.executeQuery();

			risultatiMaster = populateMasterUfficioUtenteDTO(rs, utenti, dataInizio, dataFine);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return risultatiMaster;
	}

	private static List<ReportDetailUfficioUtenteDTO> executeDetailQuery(final ReportQueryEnum queryUffici, final ReportQueryEnum queryUfficiUtenti, final Long[] uffici,
			final Long[] utenti, final Calendar dataInizio, final Calendar dataFine, final boolean populaConAssegnazione, final Long idAoo, final Connection con) {
		List<ReportDetailUfficioUtenteDTO> risultatiDetail = null;
		ResultSet rs = null;
		PreparedStatement ps = null;
		try {
			String query = fillPlaceholders(queryUffici, queryUfficiUtenti, uffici, utenti);
			
			ps = con.prepareStatement(query);
			setQueryParametersUfficiUtentiDate(ps, idAoo, uffici, utenti, dataInizio, dataFine);

			rs = ps.executeQuery();

			if (populaConAssegnazione) {
				risultatiDetail = populateDetailUfficioUtenteDTOConAssegnazione(rs);
			} else {
				risultatiDetail = populateDetailUfficioUtenteDTO(rs);
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage());
		} finally {
			closeStatement(ps, rs);
		}

		return risultatiDetail;
	}

	/**
	 * Popola i placeholder della query.
	 * 
	 * @param queryUffici
	 *            Query degli uffici.
	 * @param queryUfficiUtenti
	 *            Query degli utenti.
	 * @param uffici
	 *            Identificativi degli uffici con cui effettuare la sostituzione.
	 * @param utenti
	 *            Identificativi degli utenti con cui effettuare la sostituzione.
	 * @return Query costruita.
	 */
	private static String fillPlaceholders(final ReportQueryEnum queryUffici,
			final ReportQueryEnum queryUfficiUtenti, final Long[] uffici, final Long[] utenti) {
		String query = "";

		if (utenti != null) {
			query = getQuery(queryUfficiUtenti);
		} else {
			query = getQuery(queryUffici);
		}

		return replaceQueryPlaceholdersUfficiUtenti(uffici, utenti, query);
	}

	private static List<ReportMasterUfficioUtenteDTO> populateMasterUfficioUtenteDTO(final ResultSet rs, final Long[] utenti, final Calendar dataInizio, final Calendar dataFine)
			throws SQLException {
		final List<ReportMasterUfficioUtenteDTO> output = new ArrayList<>();

		while (rs.next()) {
			final ReportMasterUfficioUtenteDTO r = new ReportMasterUfficioUtenteDTO();

			r.setNumeroDocumenti(rs.getInt("numdoc"));
			r.setIdNodo(rs.getLong(IDNODO));
			try {
				r.setUfficio(rs.getString("descrizione"));
			} catch (final Exception e) {
				LOGGER.error(e);
				r.setUfficio(rs.getString(DESCRIZIONE_UFFICIO));
			}
			if (utenti != null) {
				r.setUsername(rs.getString("username"));
				r.setIdUtente(rs.getLong("idutente"));
			}
			r.setDataDa(dataInizio.getTime());
			r.setDataA(dataFine.getTime());

			output.add(r);
		}

		return output;
	}

	private static List<ReportDetailUfficioUtenteDTO> populateDetailUfficioUtenteDTO(final ResultSet rs) throws SQLException {
		final List<ReportDetailUfficioUtenteDTO> output = new ArrayList<>();

		while (rs.next()) {
			final ReportDetailUfficioUtenteDTO r = getDetailUfficioUtente(rs);
			output.add(r);
		}

		return output;
	}

	private static ReportDetailUfficioUtenteDTO getDetailUfficioUtente(final ResultSet rs) throws SQLException {
		final ReportDetailUfficioUtenteDTO r = new ReportDetailUfficioUtenteDTO();

		r.setNumeroProtocollo(rs.getString(NUMERO_PROTOCOLLO));
		r.setNumeroDocumento(rs.getString(NUMERO_DOCUMENTO));
		r.setAnnoDocumento(rs.getString(ANNO_DOCUMENTO));
		r.setNumeroFascicolo(rs.getString(NUMERO_FASCICOLO));
		r.setOggetto(rs.getString(OGGETTO));
		r.setOggettoBreve(StringUtils.getTextAbstract(r.getOggetto(), 200));
		r.setDataCreazione(String.format(FORMAT_DATA, rs.getTimestamp(DATA_CREAZIONE)));
		r.setUtenteCreatore(rs.getString(UTENTE_CREATORE));
		r.setAnno(rs.getString("anno"));
		r.setDescrizione(rs.getString("descrizione"));

		return r;
	}

	private static List<ReportDetailUfficioUtenteDTO> populateDetailUfficioUtenteDTOConAssegnazione(final ResultSet rs) throws SQLException {
		final SimpleDateFormat ddMMyyyy = new SimpleDateFormat(DateUtils.DD_MM_YYYY);
		final List<ReportDetailUfficioUtenteDTO> output = new ArrayList<>();

		while (rs.next()) {
			final ReportDetailUfficioUtenteDTO r = getDetailUfficioUtente(rs);
			r.setTipoAssegnazione(rs.getString("tipoAssegnazione"));
			r.setDataAssegnazione(new Date(rs.getLong("dataassegnazione") * 1000));
			r.setDataAssegnazioneStr(ddMMyyyy.format(r.getDataAssegnazione()));
			output.add(r);
		}

		return output;
	}

	private static String getQuery(final ReportQueryEnum rqe) {
		return PropertiesProvider.getIstance().getReportQueries(rqe);
	}
	
	/**
	 * Esegue la query per estrarre protocolli in entrata assegnati per competenza all'ufficio/utente (pervenuti, inevasi e lavorati).
	 * 
	 * @param reportQueryEnum
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dettagli report
	 */
	private static List<ReportDetailProtocolliPerCompetenzaDTO> executeDetailProtocolliPerCompetenzaQuery(final ReportQueryEnum reportQueryEnum, final Long[] uffici,
			final Long[] utenti, final Calendar dataInizio, final Calendar dataFine, final Long idAoo, final Connection con) {
		List<ReportDetailProtocolliPerCompetenzaDTO> risultatiDetail = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String query = Constants.EMPTY_STRING;

			query = getQuery(reportQueryEnum);

			query = replaceQueryPlaceholdersUfficiUtenti(uffici, utenti, query);

			ps = con.prepareStatement(query);
			if (ReportQueryEnum.SQL_REPORT_PROT_PERVENUTI_COMPETENZA.equals(reportQueryEnum)) {
				setQueryParametersProtocolliPervenutiCompetenza(ps, idAoo, uffici, utenti, dataInizio, dataFine);
			} else if (ReportQueryEnum.SQL_REPORT_PROT_INEVASI_COMPETENZA.equals(reportQueryEnum)) {
				setQueryParametersProtocolliInevasiCompetenza(ps, idAoo, uffici, utenti, dataFine);
			} else if (ReportQueryEnum.SQL_REPORT_PROT_LAVORATI_COMPETENZA.equals(reportQueryEnum)) {
				setQueryParametersProtocolliLavoratiCompetenza(ps, idAoo, uffici, utenti, dataInizio, dataFine);
			}

			rs = ps.executeQuery();

			risultatiDetail = populateDetailProtocolliPerCompetenzaDTO(rs);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage());
		} finally {
			closeStatement(ps, rs);
		}

		return risultatiDetail;
	}
	
	/**
	 * Popola la lista dei dettagli del report protocolli per competenza.
	 * 
	 * @param rs
	 * @return lista dettagli report
	 * @throws SQLException
	 */
	private static List<ReportDetailProtocolliPerCompetenzaDTO> populateDetailProtocolliPerCompetenzaDTO(final ResultSet rs) throws SQLException {
		final List<ReportDetailProtocolliPerCompetenzaDTO> output = new ArrayList<>();

		while (rs.next()) {
			final ReportDetailProtocolliPerCompetenzaDTO r = getDetailProtocolliPerCompetenza(rs);
			output.add(r);
		}

		return output;
	}

	/**
	 * Restituisce il singolo dettaglio del report protocolli per competenza.
	 * 
	 * @param rs
	 * @return dettaglio report
	 * @throws SQLException
	 */
	private static ReportDetailProtocolliPerCompetenzaDTO getDetailProtocolliPerCompetenza(final ResultSet rs) throws SQLException {
		final ReportDetailProtocolliPerCompetenzaDTO r = new ReportDetailProtocolliPerCompetenzaDTO();

		r.setNumeroProtocollo(rs.getString(NUMERO_PROTOCOLLO));
		r.setNumeroDocumento(rs.getString(NUMERO_DOCUMENTO));
		r.setAnnoDocumento(rs.getString(ANNO_DOCUMENTO));
		r.setDataProtocollo(rs.getDate(DATA_PROTOCOLLO));
		r.setOggetto(rs.getString(OGGETTO));
		r.setTipologiaDocumento(rs.getString("tipoDocumento"));
		if (rs.getString(UFFICIO_ASSEGNATARIO) != null) {
			r.setUfficioAssegnatario(rs.getString(UFFICIO_ASSEGNATARIO));
		}
		if (rs.getString(UTENTE_ASSEGNATARIO) != null) {
			r.setUtenteAssegnatario(rs.getString(UTENTE_ASSEGNATARIO));
		}
		if (rs.getDate("dataChiusura") != null) {
			r.setDataChiusura(rs.getDate("dataChiusura"));
		}
		if (rs.getString("tipoChiusura") != null) {
			r.setTipoChiusura(rs.getString("tipoChiusura"));
		}
		if (rs.getString("stato") != null) {
			r.setStato(rs.getString("stato"));
		}

		return r;
	}
	
	/**
	 * Esegue la query per estrarre i documenti assegnati (per competenza o
	 * conoscenza) alla data assegnazione, dall’ufficio dell’utente in sessione
	 * all’ufficio/utenti selezionati, coerentemente ai filtri di ricerca
	 * eventualmente valorizzati.
	 * 
	 * @param queryUffici
	 * @param queryUfficiUtenti
	 * @param idAoo
	 * @param parametriElencoDivisionale
	 * @param uffici
	 * @param utenti
	 * @param tipoAssegnazione
	 * @param numeroProtocolloDa
	 * @param idTipologiaDocumento
	 * @param con
	 * @return lista dettagli report
	 */
	private static List<RisultatoRicercaElencoDivisionaleDTO> executeDetailElencoDivisionaleQuery(final ReportQueryEnum queryUffici, final ReportQueryEnum queryUfficiUtenti,
			final Long idAoo, final ParamsRicercaElencoDivisionaleDTO parametriElencoDivisionale, final Long[] uffici, final Long[] utenti, final String tipoAssegnazione, 
				final Integer numeroProtocolloDa, final Integer idTipologiaDocumento, final Connection con) {
		List<RisultatoRicercaElencoDivisionaleDTO> risultatiDetail = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String query = Constants.EMPTY_STRING;

			if (utenti != null) {
				query = getQuery(queryUfficiUtenti);
			} else {
				query = getQuery(queryUffici);
			}
			
			final StringBuilder sqlBuffer = new StringBuilder(query);
			sqlBuffer.append(" order by 3 asc, 2 asc");
			query = sqlBuffer.toString();
			
			if (numeroProtocolloDa != null) {
				query = query.replace("#NUMEROPROTOCOLLODA", "and dv.numeroprotocollo >= " + numeroProtocolloDa);
			} else {
				query = query.replace("#NUMEROPROTOCOLLODA", "");
			}
			
			if (idTipologiaDocumento != null) {
				query = query.replace("#TIPODOCUMENTO", "and dv.tipologiadocumento = " + idTipologiaDocumento);
			} else {
				query = query.replace("#TIPODOCUMENTO", "");
			}

			query = replaceQueryPlaceholdersUfficiUtenti(uffici, utenti, query);
			Integer[] tipiEvento = null;
			if ("Competenza".equals(tipoAssegnazione)) {
				final Integer[] eventiCom = {1004, 1013, 1079, 1036};
				tipiEvento = eventiCom;
			} else if ("Conoscenza".equals(tipoAssegnazione)) {
				final Integer[] eventiCon = {1005};
				tipiEvento = eventiCon;
			}
			query = replaceQueryPlaceholdersTipiEvento(tipiEvento, query);

			ps = con.prepareStatement(query);
			setQueryParametersElencoDivisionale(ps, idAoo, parametriElencoDivisionale, uffici, utenti, tipiEvento);

			rs = ps.executeQuery();

			risultatiDetail = populateRisultatoElencoDivisionaleDTO(rs);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return risultatiDetail;
	}
	
	/**
	 * Popola la lista dei dettagli del report elenco divisionale.
	 * 
	 * @param rs
	 * @return lista dettagli report
	 * @throws SQLException
	 */
	private static List<RisultatoRicercaElencoDivisionaleDTO> populateRisultatoElencoDivisionaleDTO(final ResultSet rs) throws SQLException {
		final List<RisultatoRicercaElencoDivisionaleDTO> output = new ArrayList<>();

		while (rs.next()) {
			final RisultatoRicercaElencoDivisionaleDTO r = getRisultatoElencoDivisionale(rs);
			output.add(r);
		}

		return output;
	}
	
	/**
	 * Restituisce il singolo dettaglio del report elenco divisionale.
	 * 
	 * @param rs
	 * @return dettaglio report
	 * @throws SQLException
	 */
	private static RisultatoRicercaElencoDivisionaleDTO getRisultatoElencoDivisionale(final ResultSet rs) throws SQLException {
		final RisultatoRicercaElencoDivisionaleDTO r = new RisultatoRicercaElencoDivisionaleDTO();

		r.setNumeroElenco(rs.getInt(NUMERO_PROTOCOLLO));
		r.setAnnoElenco(rs.getInt(ANNO_DOCUMENTO));
		r.setDataProtocollo(rs.getDate(DATA_PROTOCOLLO));
		r.setDescrizioneTitolario(rs.getString("titolario"));
		r.setOggetto(rs.getString(OGGETTO));
		r.setTipologiaDocumento(rs.getString("tipoDocumento"));
		r.setDescrizioneMittente(rs.getString("mittente"));
		r.setNumeroDocumento(rs.getInt(NUMERO_DOCUMENTO));
		if (rs.getString(UTENTE_ASSEGNATARIO) != null) {
			r.setUtenteAssegnatario(rs.getString(UTENTE_ASSEGNATARIO));
		}

		return r;
	}
	
	/**
	 * @see it.ibm.red.business.dao.IReportDAO#detailStatisticheUfficioUtente(it.ibm.red.business.dto.ReportDetailStatisticheUfficioUtenteDTO,
	 *      java.sql.Connection).
	 */
	@Override
	public List<ReportDetailStatisticheUfficioUtenteDTO> detailStatisticheUfficioUtente(final ReportDetailStatisticheUfficioUtenteDTO form, final Connection con) {
		List<ReportDetailStatisticheUfficioUtenteDTO> list = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			ReportQueryEnum report = null;
			String filter = null;
			String uffDestField = "idUfficioAssegnatario";
			String utDestField = "idUtenteAssegnatario";
			switch (form.getTipoEstrazione()) {
			case INGRESSO_TUTTI_APERTI:
				report = ReportQueryEnum.SQL_REPORT_STATISTICHE_UFFICI_INGRESSO_APERTI;
				break;
			case INGRESSO_IN_LAVORAZIONE:
				filter = " and lower(coda) not like '%sospeso%' ";
				report = ReportQueryEnum.SQL_REPORT_STATISTICHE_UFFICI_INGRESSO_APERTI;
				break;
			case INGRESSO_IN_SCADENZA:
				filter = " and dataScadenza >= sysdate ";
				report = ReportQueryEnum.SQL_REPORT_STATISTICHE_UFFICI_INGRESSO_APERTI;
				break;
			case INGRESSO_SCADUTI:
				filter = " and dataScadenza < sysdate ";
				report = ReportQueryEnum.SQL_REPORT_STATISTICHE_UFFICI_INGRESSO_APERTI;
				break;
			case INGRESSO_IN_SOSPESO:
				filter = " and lower(coda) like '%sospeso%' ";
				report = ReportQueryEnum.SQL_REPORT_STATISTICHE_UFFICI_INGRESSO_APERTI;
				break;
			case INGRESSO_TUTTI_CHIUSI:
				report = ReportQueryEnum.SQL_REPORT_STATISTICHE_UFFICI_INGRESSO_CHIUSI;
				uffDestField = IDUFFICIOASSEGNANTE;
				utDestField = IDUTENTEASSEGNANTE;
				break;
			case INGRESSO_ATTI:
				filter = " and ev.TIPOEVENTO = 1009 ";
				uffDestField = IDUFFICIOASSEGNANTE;
				utDestField = IDUTENTEASSEGNANTE;
				report = ReportQueryEnum.SQL_REPORT_STATISTICHE_UFFICI_INGRESSO_CHIUSI;
				break;
			case INGRESSO_RISPOSTA:
				filter = " and ev.TIPOEVENTO = 1010 ";
				uffDestField = IDUFFICIOASSEGNANTE;
				utDestField = IDUTENTEASSEGNANTE;
				report = ReportQueryEnum.SQL_REPORT_STATISTICHE_UFFICI_INGRESSO_CHIUSI;
				break;
			case USCITA_IN_LAVORAZIONE:
				report = ReportQueryEnum.SQL_REPORT_STATISTICHE_UFFICI_USCITA_APERTI;
				break;
			case USCITA_SPEDITI:
				uffDestField = IDUFFICIOASSEGNANTE;
				utDestField = IDUTENTEASSEGNANTE;
				report = ReportQueryEnum.SQL_REPORT_STATISTICHE_UFFICI_USCITA_CHIUSI;
				break;
			default:
				break;
			}

			final StringBuilder sqlBuffer = new StringBuilder(getQuery(report));
			if (TargetNodoReportEnum.UFFICIO.equals(form.getTipoStruttura())) {
				sqlBuffer.append(AND + uffDestField + " = " + form.getStruttura().getIdNodo());
			} else {
				sqlBuffer.append(AND + uffDestField + " = " + form.getStruttura().getIdNodo() + AND + utDestField + " = "
						+ form.getStruttura().getIdUtente());
			}

			if (filter != null) {
				sqlBuffer.append(filter);
			}

			if (form.getDataDa() != null) {
				sqlBuffer.append(" and dataCreazione >= ? ");
			}

			if (form.getDataA() != null) {
				sqlBuffer.append(" and dataCreazione <= ? ");
			}

			if (form.getAnno() != null) {
				sqlBuffer.append(" and annoDocumento = " + form.getAnno());
			}

			ps = con.prepareStatement(sqlBuffer.toString());
			
			int index = 1;
			
			ps.setLong(index++, form.getStruttura().getIdAOO());
			
			if (form.getDataDa() != null) {
				ps.setTimestamp(index++, new Timestamp(form.getDataDa().getTime()));
			}
			if (form.getDataA() != null) {
				ps.setTimestamp(index++, new Timestamp(form.getDataA().getTime()));
			}

			rs = ps.executeQuery();

			list = new ArrayList<>();
			ReportDetailStatisticheUfficioUtenteDTO item = null;
			LOGGER.info(form.getTipoEstrazione().getDescrizione());
			LOGGER.info("Estrazione tipo: \"" + form.getTipoEstrazione().getDescrizione() + "\" eseguita con query Report DAO: " + sqlBuffer.toString());

			while (rs.next()) {
				item = new ReportDetailStatisticheUfficioUtenteDTO();

				item.setAnnoDocumento(rs.getInt(ANNO_DOCUMENTO));
				item.setDataCreazione(rs.getDate(DATA_CREAZIONE));
				item.setDocumentTitle(rs.getInt("documentTitle"));
				item.setNumeroDocumento(rs.getInt(NUMERO_DOCUMENTO));
				item.setOggetto(rs.getString(OGGETTO));
				item.setTipologiaDocumento(rs.getString("tipologiaDocumento"));

				UfficioDTO ufficioAssegnante = null;
				if (rs.getLong("idUfficioAssegnante") != 0L) {
					ufficioAssegnante = new UfficioDTO(rs.getLong("idUfficioAssegnante"), rs.getString("ufficioAssegnante"));
				}

				UfficioDTO ufficioAssegnatario = null;
				if (rs.getLong("idUfficioAssegnatario") != 0L) {
					ufficioAssegnatario = new UfficioDTO(rs.getLong("idUfficioAssegnatario"), rs.getString(UFFICIO_ASSEGNATARIO));
				}

				item.setUfficioAssegnante(ufficioAssegnante);
				item.setUfficioAssegnatario(ufficioAssegnatario);

				UtenteDTO utenteAssegnante = null;
				if (rs.getLong(IDUTENTEASSEGNANTE) != 0L) {
					utenteAssegnante = new UtenteDTO();
					utenteAssegnante.setId(rs.getLong(IDUTENTEASSEGNANTE));
					utenteAssegnante.setCognome(rs.getString("cognomeAssegnante"));
					utenteAssegnante.setNome(rs.getString("nomeAssegnante"));
				}

				UtenteDTO utenteAssegnatario = null;
				if (rs.getLong("idUtenteAssegnatario") != 0L) {
					utenteAssegnatario = new UtenteDTO();
					utenteAssegnatario.setId(rs.getLong("idUtenteAssegnatario"));
					utenteAssegnatario.setCognome(rs.getString("cognomeAssegnatario"));
					utenteAssegnatario.setNome(rs.getString("nomeAssegnatario"));
				}

				item.setUtenteAssegnante(utenteAssegnante);
				item.setUtenteAssegnatario(utenteAssegnatario);

				if (rs.getInt(NUMERO_PROTOCOLLO) != 0L) {
					item.setNumeroProtocollo(rs.getInt(NUMERO_PROTOCOLLO));
				}
				if (rs.getDate(DATA_PROTOCOLLO) != null) {
					item.setDataProtocollo(rs.getDate(DATA_PROTOCOLLO));
				}

				// solo per ingressi aperti
				if (ReportQueryEnum.SQL_REPORT_STATISTICHE_UFFICI_INGRESSO_APERTI.equals(report) && rs.getDate("dataScadenza") != null) {
					item.setDataScadenza(rs.getDate("dataScadenza"));
				}

				if (ReportQueryEnum.SQL_REPORT_STATISTICHE_UFFICI_INGRESSO_APERTI.equals(report) && rs.getString("coda") != null) {
					item.setCoda(rs.getString("coda"));
				}

				list.add(item);
			}
		} catch (final Exception e) {
			LOGGER.error("detailStatisticheUfficioUtente -> Si è verificato un errore durante il recupero della lista dei dettagli report.", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return list;
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#countProtEntrataGeneratiByUfficioUtente(java.lang.Long[],
	 *      java.lang.Long[], java.util.Calendar, java.util.Calendar,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<ReportMasterUfficioUtenteDTO> countProtEntrataGeneratiByUfficioUtente(final Long[] uffici, final Long[] utenti, final Calendar dataInizio,
			final Calendar dataFine, final Long idAoo, final Connection con) {
		LOGGER.info("Esecuzione -> countProtEntrataGeneratiByUfficioUtente");
		return executeMasterQuery(ReportQueryEnum.SQL_REPORT_COUNT_PROT_ENTRATA_GENERATI_UFFICIO, null, uffici, utenti, dataInizio, dataFine, idAoo, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IReportDAO#docProtGeneratiByUfficio(java.lang.Long[],
	 *      java.lang.Long[], java.util.Calendar, java.util.Calendar,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<ReportDetailUfficioUtenteDTO> docProtGeneratiByUfficio(final Long[] uffici, final Long[] utenti, final Calendar dataInizio, final Calendar dataFine,
			final Long idAoo, final Connection con) {
		LOGGER.info("Esecuzione -> dettaglioProtEntrataGeneratiByUfficioUtente");
		return executeDetailQuery(ReportQueryEnum.SQL_REPORT_PROT_ENTRATA_GENERATI_UFFICIO, null, uffici, utenti, dataInizio, dataFine, false, idAoo, con);
	}
	
	/**
	 * Estrae protocolli in entrata assegnati per competenza all'ufficio/utente
	 * (pervenuti, inevasi e lavorati).
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param tipoOperazioneEnum
	 * @param con
	 * @return lista dettagli report
	 */
	@Override
	public List<ReportDetailProtocolliPerCompetenzaDTO> getProtocolliPerCompetenza(final Long[] uffici, final Long[] utenti, final Calendar dataInizio, final Calendar dataFine,
			final Long idAoo, TipoOperazioneReportEnum tipoOperazioneEnum, final Connection con) {
		LOGGER.info("Esecuzione -> getProtocolliPerCompetenza");
		
		switch (tipoOperazioneEnum) {
			case UTE_PROT_PERVENUTI_COMPETENZA:
				return executeDetailProtocolliPerCompetenzaQuery(ReportQueryEnum.SQL_REPORT_PROT_PERVENUTI_COMPETENZA, uffici, utenti, 
						dataInizio, dataFine, idAoo, con);
			case UTE_PROT_INEVASI_COMPETENZA:
				return executeDetailProtocolliPerCompetenzaQuery(ReportQueryEnum.SQL_REPORT_PROT_INEVASI_COMPETENZA, uffici, utenti, 
						dataInizio, dataFine, idAoo, con);
			case UTE_PROT_LAVORATI_COMPETENZA:
				return executeDetailProtocolliPerCompetenzaQuery(ReportQueryEnum.SQL_REPORT_PROT_LAVORATI_COMPETENZA, uffici, utenti,
						dataInizio, dataFine, idAoo, con);
			default:
				return new ArrayList<>();
		}
	}
	
	/**
	 * Estrae i documenti assegnati (per competenza o conoscenza) alla data
	 * assegnazione, dall’ufficio dell’utente in sessione all’ufficio/utenti
	 * selezionati, coerentemente ai filtri di ricerca eventualmente valorizzati.
	 * 
	 * @param idAoo
	 * @param parametriElencoDivisionale
	 * @param uffici
	 * @param utenti
	 * @param tipoAssegnazione
	 * @param numeroProtocolloDa
	 * @param idTipologiaDocumento
	 * @param con
	 * @return lista dettagli report
	 */
	@Override
	public List<RisultatoRicercaElencoDivisionaleDTO> getElencoDivisionale(final Long idAoo, final ParamsRicercaElencoDivisionaleDTO parametriElencoDivisionale, 
			final Long[] uffici, final Long[] utenti, final String tipoAssegnazione, final Integer numeroProtocolloDa, final Integer idTipologiaDocumento, final Connection con) {
		LOGGER.info("Esecuzione -> getElencoDivisionale");
		
		return executeDetailElencoDivisionaleQuery(ReportQueryEnum.SQL_REPORT_ELENCO_DIVISIONALE_UFFICIO, ReportQueryEnum.SQL_REPORT_ELENCO_DIVISIONALE_UFFICIOUTENTE, idAoo, 
				parametriElencoDivisionale, uffici, utenti, tipoAssegnazione, numeroProtocolloDa, idTipologiaDocumento, con);
	}

}