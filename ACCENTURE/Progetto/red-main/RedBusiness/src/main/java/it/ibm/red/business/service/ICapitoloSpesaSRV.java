/**
 * 
 */
package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.CapitoloSpesaDTO;
import it.ibm.red.business.service.facade.ICapitoloSpesaFacadeSRV;

/**
 * Interfaccia servizio per la gestione dei capitoli di spesa.
 *
 * @author mcrescentini
 * 
 */
public interface ICapitoloSpesaSRV extends ICapitoloSpesaFacadeSRV {

	/**
	 * Esegue l'inserimento multiplo dei capitoli spesa definiti nella lista:
	 * <code> capitoliSpesa </code>. I capitoli spesa inseriti saranno validi solo
	 * per l'AOO identificata da: <code> idAoo </code>.
	 * 
	 * @param capitoliSpesa
	 *            capitoli spesa da inserire
	 * @param idAoo
	 *            identificativo dell'Area organizzativa omogenea
	 * @param con
	 *            connession al database
	 */
	void inserisciMultipli(List<CapitoloSpesaDTO> capitoliSpesa, Long idAoo, Connection con);

}