package it.ibm.red.business.service.ws;

import java.sql.Connection;

import it.ibm.red.business.service.ws.facade.IRedWsDocGenFacadeSRV;

/**
 * The Interface IRedWsDocGenSRV.
 *
 * @author m.crescentini
 * 
 * Interfaccia servizio per il tracciamento dei documenti creati tramite i web service di Red EVO.
 */
public interface IRedWsDocGenSRV extends IRedWsDocGenFacadeSRV {
	
	/**
	 * @param idDocumento
	 * @param codiceFlusso
	 * @param con
	 * @return
	 */
	boolean isDocPresente(int idDocumento, String codiceFlusso, Connection con);

	/**
	 * @param idDocumento
	 * @param codiceFlusso
	 * @param con
	 */
	void insertDocGen(int idDocumento, String codiceFlusso, Connection con);
	
}