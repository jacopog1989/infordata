package it.ibm.red.business.dto;

import java.util.List;

import it.ibm.red.business.enums.LookupTablePresentationModeEnum;
import it.ibm.red.business.enums.TipoDocumentoModeEnum;
import it.ibm.red.business.enums.TipoMetadatoEnum;

/**
 * @author APerquoti
 */
public class LookupTableDTO extends MetadatoDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -6717350337820843906L;

	/**
	 * Prefisso widget.
	 */
	private static final String WDG_PREFIX = "WDG_";


	/**
	 * Lista valori.
	 */
	private List<SelectItemDTO> lookupValues; 

	/**
	 * Valore selezionato tra quelli disponibili nella lookup table.
	 */
	private SelectItemDTO lookupValueSelected;

	/**
	 * Valore selezionato tra quelli disponibili nella lookup table.
	 */
	private String lookupWidgetVar;
	
	/**
	 * Costruttore (con flag metadato valuta).
	 * @param name
	 * @param displayName
	 * @param lookupTableModeEnum
	 * @param visibility
	 * @param editability
	 * @param obligatoriness
	 * @param flagOut
	 * @param inLookupValues
	 */
	public LookupTableDTO(final Integer id, final String name, final String displayName, final LookupTablePresentationModeEnum lookupTableModeEnum, 
			final TipoDocumentoModeEnum visibility, final TipoDocumentoModeEnum editability, final TipoDocumentoModeEnum obligatoriness, 
			final Boolean flagOut, final List<SelectItemDTO> inLookupValues, final boolean isMetadatoValuta) {
		super(id, name, TipoMetadatoEnum.LOOKUP_TABLE, displayName, null, lookupTableModeEnum, null, 
				visibility, editability, obligatoriness, false, flagOut, null, isMetadatoValuta);
		this.lookupValues = inLookupValues;
		this.lookupWidgetVar = new StringBuilder().append(WDG_PREFIX).append(name).toString();
		this.lookupValueSelected = new SelectItemDTO();
	}

	/**
	 * Costruttore (NO flag metadato valuta).
	 * @param name
	 * @param displayName
	 * @param lookupTableModeEnum
	 * @param visibility
	 * @param editability
	 * @param obligatoriness
	 * @param flagOut
	 * @param inLookupValues
	 */
	public LookupTableDTO(final String name, final String displayName, final LookupTablePresentationModeEnum lookupTableModeEnum, 
			final TipoDocumentoModeEnum visibility, final TipoDocumentoModeEnum editability, final TipoDocumentoModeEnum obligatoriness, 
			final Boolean flagOut, final List<SelectItemDTO> inLookupValues) {
		super(null, name, TipoMetadatoEnum.LOOKUP_TABLE, displayName, null, lookupTableModeEnum, null, 
				visibility, editability, obligatoriness, false, flagOut, null, false);
		this.lookupValues = inLookupValues;
		this.lookupWidgetVar = new StringBuilder().append(WDG_PREFIX).append(name).toString();
		this.lookupValueSelected = new SelectItemDTO();
	}

	/**
	 * Costruttore per deserializzazione metadati estesi documento.
	 * 
	 * @param name
	 * @param displayName
	 * @param lookupValueSelected
	 * @param lookupTableModeEnum
	 * @param visibility
	 * @param editability
	 * @param obligatoriness
	 */
	public LookupTableDTO(final String name, final String displayName, final SelectItemDTO lookupValueSelected, final LookupTablePresentationModeEnum lookupTableModeEnum, 
			final TipoDocumentoModeEnum visibility, final TipoDocumentoModeEnum editability, final TipoDocumentoModeEnum obligatoriness) {
		this(name, displayName, lookupTableModeEnum, visibility, editability, obligatoriness, null, null);
		setLookupValueSelected(lookupValueSelected);
	}

	/**
	 * Costruttore vuoto.
	 */
	public LookupTableDTO() {
		super();
	}

	/**
	 * Resetta la selezione della lookUp Table.
	 */
	public void resetValueSelected() {
		this.lookupValueSelected = new SelectItemDTO();
	}

	/**
	 * Restituisce la lista di valori selezionabili per la lookup table.
	 * @return lookupValues
	 */
	public List<SelectItemDTO> getLookupValues() {
		return lookupValues;
	}

	/**
	 * @return lookupValueSelected
	 */
	public SelectItemDTO getLookupValueSelected() {
		return lookupValueSelected;
	}
	
	/**
	 * Imposta la ista di valori selezionabili per la lookup table.
	 * @param lookupValues
	 */
	public void setLookupValues(final List<SelectItemDTO> lookupValues) {
		this.lookupValues = lookupValues;
	}

	/**
	 * @param lookupValueSelected
	 */
	public void setLookupValueSelected(final SelectItemDTO lookupValueSelected) {
		this.lookupValueSelected = lookupValueSelected;
	}

	/**
	 * Restituisce la descrizione della lookup, se selezionata.
	 * @see it.ibm.red.business.dto.MetadatoDTO#getValue4AttrExt()
	 * @return description
	 */
	@Override
	public String getValue4AttrExt() {
		return (getLookupValueSelected() != null) ? getLookupValueSelected().getDescription() : null;
	}

	/**
	 * @return lookupWidgetVar
	 */
	public String getLookupWidgetVar() {
		return lookupWidgetVar;
	}

	/**
	 * Imposta la widgetVar della lookup table.
	 * @param lookupWidgetVar
	 */
	public void setLookupWidgetVar(final String lookupWidgetVar) {
		this.lookupWidgetVar = lookupWidgetVar;
	}

}
