package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.ParamsRicercaElencoDivisionaleDTO;
import it.ibm.red.business.dto.ReportDetailProtocolliPerCompetenzaDTO;
import it.ibm.red.business.dto.ReportDetailStatisticheUfficioUtenteDTO;
import it.ibm.red.business.dto.ReportDetailUfficioUtenteDTO;
import it.ibm.red.business.dto.ReportMasterUfficioUtenteDTO;
import it.ibm.red.business.dto.RisultatoRicercaElencoDivisionaleDTO;
import it.ibm.red.business.dto.UtenteRuoloReportDTO;
import it.ibm.red.business.enums.TipoOperazioneReportEnum;
import it.ibm.red.business.persistence.model.CodaDiLavoroReport;
import it.ibm.red.business.persistence.model.DettaglioReport;
import it.ibm.red.business.persistence.model.TipoOperazioneReport;

/**
 * 
 * @author m.crescentini
 *
 *         Interfaccia dao per la gestione dei report.
 */
public interface IReportDAO extends Serializable {

	/**
	 * Ottiene la lista dei dettagli report.
	 * 
	 * @param con
	 * @return lista dei dettagli report
	 */
	List<DettaglioReport> getAllDettaglioReport(Connection con);

	/**
	 * 
	 * @param idDettaglioReport
	 * @param idAoo
	 * @param con
	 * @return lista dei tipi di operazione del dettaglio report
	 */
	List<TipoOperazioneReport> getTipiOperazioneReportByIdDettaglio(int idDettaglioReport, long idAoo, Connection con);

	/**
	 * Ottiene le code di lavoro del dettaglio report.
	 * 
	 * @param idDettaglioReport
	 * @param con
	 * @return lista delle code di lavoro del dettaglio report
	 */
	List<CodaDiLavoroReport> getCodeDiLavoroReportByIdDettaglio(int idDettaglioReport, Connection con);

	/**
	 * Ottiene i documenti nella coda atti per ufficio utente.
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param con
	 * @return lista dei master per ufficio utente
	 */
	List<ReportMasterUfficioUtenteDTO> countCodaAttiGroupByUfficioUtente(Long[] uffici, Long[] utenti, Calendar dataInizio, Calendar dataFine, Connection con);

	/**
	 * Ottiene i documenti nella coda atti per ufficio utente.
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param con
	 * @return lista dei dettagli dei documenti per ufficio utente
	 */
	List<ReportDetailUfficioUtenteDTO> idDocInCodaAttiByUfficioUtente(Long[] uffici, Long[] utenti, Calendar dataInizio, Calendar dataFine, Connection con);

	/**
	 * Ottiene i documenti nella coda atti per ufficio utente.
	 * 
	 * @param idDocumenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return mappa id documento dettaglio
	 */
	Map<String, ReportDetailUfficioUtenteDTO> docInCodaAttiByUfficioUtente(String[] idDocumenti, Calendar dataInizio, Calendar dataFine, Long idAoo, Connection con);

	/**
	 * Ottiene i documenti in entrata per ufficio utente.
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei master per ufficio utente
	 */
	List<ReportMasterUfficioUtenteDTO> countDocEntrataGroupByUfficioUtente(Long[] uffici, Long[] utenti, Calendar dataInizio, Calendar dataFine, Long idAoo, Connection con);

	/**
	 * Ottiene i documenti in entrata per ufficio utente.
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei dettagli dei documenti per ufficio utente
	 */
	List<ReportDetailUfficioUtenteDTO> docEntrataGroupByUfficioUtente(Long[] uffici, Long[] utenti, Calendar dataInizio, Calendar dataFine, Long idAoo, Connection con);

	/**
	 * Ottiene i documenti in uscita per ufficio utente.
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei master per ufficio utente
	 */
	List<ReportMasterUfficioUtenteDTO> countDocUscitaGroupByUfficioUtente(Long[] uffici, Long[] utenti, Calendar dataInizio, Calendar dataFine, Long idAoo, Connection con);

	/**
	 * Ottiene i documenti in uscita per ufficio utente.
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei dettagli dei documenti per ufficio utente
	 */
	List<ReportDetailUfficioUtenteDTO> docUscitaGroupByUfficioUtente(Long[] uffici, Long[] utenti, Calendar dataInizio, Calendar dataFine, Long idAoo, Connection con);

	/**
	 * Ottiene i documenti firmati per ufficio utente.
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei master per ufficio utente
	 */
	List<ReportMasterUfficioUtenteDTO> countDocFirmatiGroupByUfficioUtente(Long[] uffici, Long[] utenti, Calendar dataInizio, Calendar dataFine, Long idAoo, Connection con);

	/**
	 * Ottiene i documenti firmati per ufficio utente.
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei dettagli dei documenti per ufficio utente
	 */
	List<ReportDetailUfficioUtenteDTO> docFirmatiGroupByUfficioUtente(Long[] uffici, Long[] utenti, Calendar dataInizio, Calendar dataFine, Long idAoo, Connection con);

	/**
	 * Ottiene i documenti siglati per ufficio utente.
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei master per ufficio utente
	 */
	List<ReportMasterUfficioUtenteDTO> countDocSiglatiGroupByUfficioUtente(Long[] uffici, Long[] utenti, Calendar dataInizio, Calendar dataFine, Long idAoo, Connection con);

	/**
	 * Ottiene i documenti siglati per ufficio utente.
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei dettagli dei documenti per ufficio utente
	 */
	List<ReportDetailUfficioUtenteDTO> docSiglatiGroupByUfficioUtente(Long[] uffici, Long[] utenti, Calendar dataInizio, Calendar dataFine, Long idAoo, Connection con);

	/**
	 * Ottiene i documenti vistati per ufficio utente.
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei master per ufficio utente
	 */
	List<ReportMasterUfficioUtenteDTO> countDocVistatiGroupByUfficioUtente(Long[] uffici, Long[] utenti, Calendar dataInizio, Calendar dataFine, Long idAoo, Connection con);

	/**
	 * Ottiene i documenti vistati per ufficio utente.
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei dettagli dei documenti per ufficio utente
	 */
	List<ReportDetailUfficioUtenteDTO> docVistatiGroupByUfficioUtente(Long[] uffici, Long[] utenti, Calendar dataInizio, Calendar dataFine, Long idAoo, Connection con);

	/**
	 * Ottiene i documenti messi agli atti per ufficio utente.
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei master per ufficio utente
	 */
	List<ReportMasterUfficioUtenteDTO> countDocMessiAgliAttiGroupByUfficioUtente(Long[] uffici, Long[] utenti, Calendar dataInizio, Calendar dataFine, Long idAoo,
			Connection con);

	/**
	 * Ottiene i documenti messi agli atti per ufficio utente.
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei dettagli dei documenti per ufficio utente
	 */
	List<ReportDetailUfficioUtenteDTO> docMessiAgliAttiGroupByUfficioUtente(Long[] uffici, Long[] utenti, Calendar dataInizio, Calendar dataFine, Long idAoo, Connection con);

	/**
	 * Ottiene i contributi inseriti per ufficio utente.
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei master per ufficio utente
	 */
	List<ReportMasterUfficioUtenteDTO> countDocContributoInseritoGroupByUfficioUtente(Long[] uffici, Long[] utenti, Calendar dataInizio, Calendar dataFine, Long idAoo,
			Connection con);

	/**
	 * Ottiene i contributi inseriti per ufficio utente.
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei dettagli dei documenti per ufficio utente
	 */
	List<ReportDetailUfficioUtenteDTO> docContributoInseritoGroupByUfficioUtente(Long[] uffici, Long[] utenti, Calendar dataInizio, Calendar dataFine, Long idAoo,
			Connection con);

	/**
	 * Ottiene i documenti nella coda libro firma per ufficio utente.
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei master per ufficio utente
	 */
	List<ReportMasterUfficioUtenteDTO> countCodaLibroFirmaByUfficioUtente(Long[] uffici, Long[] utenti, Calendar dataInizio, Calendar dataFine, Long idAoo, Connection con);

	/**
	 * Ottiene i documenti nella coda libro firma per ufficio utente.
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei dettagli dei documenti per ufficio utente
	 */
	List<ReportDetailUfficioUtenteDTO> docInCodaLibroFirmaGroupByUfficioUtente(Long[] uffici, Long[] utenti, Calendar dataInizio, Calendar dataFine, Long idAoo,
			Connection con);

	/**
	 * Ottiene i documenti nella coda da lavorare per ufficio utente.
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei master per ufficio utente
	 */
	List<ReportMasterUfficioUtenteDTO> countCodaDaLavorareByUfficioUtente(Long[] uffici, Long[] utenti, Calendar dataInizio, Calendar dataFine, Long idAoo, Connection con);

	/**
	 * Ottiene i documenti nella coda da lavorare per ufficio utente.
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei dettagli dei documenti per ufficio utente
	 */
	List<ReportDetailUfficioUtenteDTO> docInCodaDaLavorareGroupByUfficioUtente(Long[] uffici, Long[] utenti, Calendar dataInizio, Calendar dataFine, Long idAoo,
			Connection con);

	/**
	 * Ottiene i documenti nella coda in sospeso per ufficio utente.
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei master per ufficio utente
	 */
	List<ReportMasterUfficioUtenteDTO> countCodaInSospesoByUfficioUtente(Long[] uffici, Long[] utenti, Calendar dataInizio, Calendar dataFine, Long idAoo, Connection con);

	/**
	 * Ottiene i documenti nella coda in sospeso per ufficio utente.
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei dettagli dei documenti per ufficio utente
	 */
	List<ReportDetailUfficioUtenteDTO> docInCodaInSospesoGroupByUfficioUtente(Long[] uffici, Long[] utenti, Calendar dataInizio, Calendar dataFine, Long idAoo,
			Connection con);

	/**
	 * Ottiene i documenti nella coda corriere per ufficio.
	 * 
	 * @param uffici
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei master per ufficio
	 */
	List<ReportMasterUfficioUtenteDTO> countCodaCorriereGroupByUfficio(Long[] uffici, Calendar dataInizio, Calendar dataFine, Long idAoo, Connection con);

	/**
	 * Ottiene i documenti nella coda in sospeso per ufficio.
	 * 
	 * @param uffici
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei dettagli dei documenti per ufficio
	 */
	List<ReportDetailUfficioUtenteDTO> docInCodaCorriereUfficio(Long[] uffici, Calendar dataInizio, Calendar dataFine, Long idAoo, Connection con);

	/**
	 * Ottiene i documenti nella coda spedizione per ufficio.
	 * 
	 * @param uffici
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei master per ufficio
	 */
	List<ReportMasterUfficioUtenteDTO> countCodaSpedizioneGroupByUfficio(Long[] uffici, Calendar dataInizio, Calendar dataFine, Long idAoo, Connection con);

	/**
	 * Ottiene i documenti nella coda spedizione per ufficio.
	 * 
	 * @param uffici
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei dettagli dei documenti per ufficio
	 */
	List<ReportDetailUfficioUtenteDTO> docInCodaSpedizioneUfficio(Long[] uffici, Calendar dataInizio, Calendar dataFine, Long idAoo, Connection con);

	/**
	 * Ottiene i documenti nella coda chiusi per ufficio.
	 * 
	 * @param uffici
	 * @param dataInizio
	 * @param dataFine
	 * @param con
	 * @return lista dei master per ufficio
	 */
	List<ReportMasterUfficioUtenteDTO> countCodaChiusiGroupByUfficio(Long[] uffici, Calendar dataInizio, Calendar dataFine, Connection con);

	/**
	 * Ottiene i documenti nella coda chiusi per ufficio.
	 * 
	 * @param uffici
	 * @param dataInizio
	 * @param dataFine
	 * @param con
	 * @return lista dei dettagli dei documenti per ufficio
	 */
	List<ReportDetailUfficioUtenteDTO> idDocInCodaChiusiByUfficio(Long[] uffici, Calendar dataInizio, Calendar dataFine, Connection con);

	/**
	 * Ottiene i documenti nella coda chiusi per ufficio.
	 * 
	 * @param idDocumenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return mappa id documento dettaglio
	 */
	Map<String, ReportDetailUfficioUtenteDTO> docInCodaChiusiByUfficio(String[] idDocumenti, Calendar dataInizio, Calendar dataFine, Long idAoo, Connection con);

	/**
	 * Ottiene gli utenti attivi per ruolo.
	 * 
	 * @param uffici
	 * @param ruoli
	 * @param con
	 * @return lista degli utenti per ruolo
	 */
	List<UtenteRuoloReportDTO> countUtentiAttiviConUnDatoRuolo(Long[] uffici, Long[] ruoli, Connection con);

	/**
	 * Ottiene il dettaglio degli utenti attivi per ruolo.
	 * 
	 * @param uffici
	 * @param ruoli
	 * @param con
	 * @return lista degli utenti per ruolo
	 */
	List<UtenteRuoloReportDTO> detailUtentiAttiviConUnDatoRuolo(Long[] uffici, Long[] ruoli, Connection con);

	/**
	 * Ottiene il dettaglio delle statistiche per ufficio utente.
	 * 
	 * @param form
	 * @param con
	 * @return lista dei dettagli delle statistiche per ufficio utente
	 */
	List<ReportDetailStatisticheUfficioUtenteDTO> detailStatisticheUfficioUtente(ReportDetailStatisticheUfficioUtenteDTO form, Connection con);

	/**
	 * Ottiene i protocolli in entrata generati per uffico utente.
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei master per ufficio utente
	 */
	List<ReportMasterUfficioUtenteDTO> countProtEntrataGeneratiByUfficioUtente(Long[] uffici, Long[] utenti, Calendar dataInizio, Calendar dataFine, Long idAoo,
			Connection con);

	/**
	 * Ottiene i protocolli generati per ufficio.
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param con
	 * @return lista dei dettagli dei documenti per ufficio utente
	 */
	List<ReportDetailUfficioUtenteDTO> docProtGeneratiByUfficio(Long[] uffici, Long[] utenti, Calendar dataInizio, Calendar dataFine, Long idAoo, Connection con);

	/**
	 * Estrae protocolli in entrata assegnati per competenza all'ufficio/utente (pervenuti, inevasi e lavorati).
	 * 
	 * @param uffici
	 * @param utenti
	 * @param dataInizio
	 * @param dataFine
	 * @param idAoo
	 * @param tipoOperazioneEnum
	 * @param con
	 * @return lista dettagli report
	 */
	List<ReportDetailProtocolliPerCompetenzaDTO> getProtocolliPerCompetenza(Long[] uffici, Long[] utenti,
			Calendar dataInizio, Calendar dataFine, Long idAoo, TipoOperazioneReportEnum tipoOperazioneEnum,
			Connection con);

	/**
	 * Estrae i documenti assegnati (per competenza o conoscenza) alla data assegnazione, dall’ufficio dell’utente in sessione all’ufficio/utenti selezionati,
	 * coerentemente ai filtri di ricerca eventualmente valorizzati.
	 * 
	 * @param idAoo
	 * @param parametriElencoDivisionale
	 * @param uffici
	 * @param utenti
	 * @param tipoAssegnazione
	 * @param numeroProtocolloDa
	 * @param idTipologiaDocumento
	 * @param con
	 * @return lista dettagli report
	 */
	List<RisultatoRicercaElencoDivisionaleDTO> getElencoDivisionale(Long idAoo,
			ParamsRicercaElencoDivisionaleDTO parametriElencoDivisionale, Long[] uffici, Long[] utenti,
			String tipoAssegnazione, Integer numeroProtocolloDa, Integer idTipologiaDocumento, Connection con);

}