package it.ibm.red.business.helper.pdf;

/**
 * Formatter creazione tabelle pdf.
 * 
 * @author CRISTIANOPierascenzi
 *
 */
public interface Formatter {
	
	/**
	 * Metodo di formattazione.
	 * 
	 * @param obj	oggetto da formattare
	 * @return		oggetto formattato
	 */
	String format(Object obj);
}
