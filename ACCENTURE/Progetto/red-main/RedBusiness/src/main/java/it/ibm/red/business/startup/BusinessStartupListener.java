package it.ibm.red.business.startup;

import java.io.ByteArrayInputStream;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.joran.JoranConfigurator;
import ch.qos.logback.core.joran.spi.JoranException;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.provider.WebServiceClientProvider;

/**
 * The listener interface for receiving startup events. The class that is
 * interested in processing a startup event implements this interface, and the
 * object created with that class is registered with a component using the
 * component's addStartupListener method. When the startup event
 * occurs, that object's appropriate method is invoked.
 *
 * @author CPIERASC
 * 
 *         Listener per gestire le operazioni da eseguire all'avvio
 *         dell'applicativo.
 */
public class BusinessStartupListener implements ServletContextListener {
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(BusinessStartupListener.class.getName());
	
	/**
	 * Context destroyed.
	 *
	 * @param event
	 *            the event
	 */
	@Override
	public void contextDestroyed(final ServletContextEvent event) {
		LOGGER.info("STARTUP LISTENER contextDestroyed(): Listener context destroyed in esecuzione...");
	}
 
	/**
	 * Context initialized.
	 *
	 * @param event
	 *            the event
	 */
	@Override 
	public void contextInitialized(final ServletContextEvent event) {
		ApplicationContextProvider.setApplicationContext(event);
		loadLoggingProperties(PropertiesNameEnum.RED_WEB_LOGGING_PROPERTIES);
		WebServiceClientProvider.resetInstance();
		LOGGER.info("ServletContext implementation major version: " + event.getServletContext().getMajorVersion() + "." + event.getServletContext().getMinorVersion());
		LOGGER.info("ServletContext info: " + event.getServletContext().getServerInfo());
		LOGGER.info("STARTUP LISTENER contextInitialized(): Listener context initialized in esecuzione...");
	}
	
	/**
	 * Carica in memoria le proprerties dei log.
	 * 
	 * @param key
	 */
	protected static void loadLoggingProperties(PropertiesNameEnum key) {
		try {
			LOGGER.info("Inizializzazione logger: caricamento delle properties di logging.");			
			LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
			
			PropertiesProvider pp = PropertiesProvider.getIstance();
			String loggingProperties = pp.getParameterByKey(key);
			
			if (StringUtils.isNotBlank(loggingProperties)) {
				JoranConfigurator configurator = new JoranConfigurator();
				configurator.setContext(context);
				context.reset(); 
				configurator.doConfigure(new ByteArrayInputStream(loggingProperties.getBytes()));
				
				LOGGER.info("Inizializzazione logger: caricamento delle properties di logging completato con successo.");	
			}
		} catch (JoranException e) {
			LOGGER.error("Errore durante lo STARTUP LISTENER: si è verificato un errore durante il caricamento delle properties di logging.", e);
		}
	}
}