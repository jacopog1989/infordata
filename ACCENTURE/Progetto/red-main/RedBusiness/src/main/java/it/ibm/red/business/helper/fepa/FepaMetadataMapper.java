package it.ibm.red.business.helper.fepa;

import fepa.metadata.v3.MetadatiDocumentoDDType;
import fepa.metadata.v3.MetadatiDocumentoServiziResiType;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.utils.DateUtils;

/**
 * Mapper metadati FEPA.
 */
public final class FepaMetadataMapper {

	/**
	 * Costruttore vuoto.
	 */
	private FepaMetadataMapper() {
		// Costruttore vuoto.
	}
	
	/**
	 * Restituisce i metadati associato al documento FEPA.
	 * @param docFepa
	 * @return MetadatiDocumentoDDType o MetadatiDocumentoServiziResiType o null
	 * @throws Exception
	 */
	public static Object getMetadata(final DocumentoFepa docFepa) {
		try {
			if (docFepa.getTipoDocumento().equalsIgnoreCase(Constants.Varie.TIPO_DOCUMENTO_GENERICO_FEPA)) {
				
				return null;
			
			} else {
			
				if (docFepa.getTipoDocumento().equalsIgnoreCase(Constants.Varie.TIPO_DOCUMENTO_DECRETO_LIQUIDAZIONE_FEPA)) {
					final MetadatiDocumentoDDType metadatiDocumentoDD = new MetadatiDocumentoDDType();
					metadatiDocumentoDD.setDATADECRETO(DateUtils.buildXmlGregorianCalendarFromDate(docFepa.getDatadecreto()));
					metadatiDocumentoDD.setFIRMATARIO(docFepa.getFirmatario());
					metadatiDocumentoDD.setNUMERODECRETO(docFepa.getNumDecreto());
					return metadatiDocumentoDD;
				}
				
				if (docFepa.getTipoDocumento().equalsIgnoreCase(Constants.Varie.TIPO_DOCUMENTO_LETTERA_SERVIZI_RESI_FEPA)) {
					final MetadatiDocumentoServiziResiType metaDocServ = new MetadatiDocumentoServiziResiType();
					metaDocServ.setFIRMATARIO(docFepa.getFirmatario());
					return metaDocServ;
				}
			
			}
			
			return null;
		} catch (final Exception e) {
			 throw new RedException(e);
		}

	}
	
}
