package it.ibm.red.business.enums;

/**
 * Mappa la tabella momentoprotocollazione sul db NSD_WS
 * 
 * @author Simone Lacarpia
 *
 */
public enum MomentoProtocollazioneEnum {

	/**
	 * 
	 */
	PROTOCOLLAZIONE_NON_STANDARD(1, "Protocollazione non standard"),
	/**
	 * 
	 */
	PROTOCOLLAZIONE_STANDARD(2, "Protocollazione standard"),
	/**
	 * 
	 */
	PROTOCOLLAZIONE_SUCCESSIVA_ALLA_FIRMA(3, "Protocollazione successiva alla firma");
	
	/**
	 * CAMPO IDMOMENTOPROTOCOLLAZIONE
	 */
	private int id;
	/**
	 * CAMPO DESCRIZIONE 
	 */
	private String descrizione;
	
	/**
	 * Costruttore
	 * 
	 * @param idMomentoProtocollazione
	 * @param descrizione
	 */
	MomentoProtocollazioneEnum(final int id, final String descrizione) {
		this.id = id;
		this.descrizione = descrizione;
	}

	/**
	 * Ritorna l'enum selezionato  
	 * @param id
	 * @return
	 */
	public static MomentoProtocollazioneEnum getEnumById(final int id) {
		MomentoProtocollazioneEnum output = null;
		for (MomentoProtocollazioneEnum item : MomentoProtocollazioneEnum.values()) {
			if (item.getId() == id) {
				output = item;
				break;
			}
		}
		
		return output;
	}
	
	/**
	 * @return
	 */
	public int getId() {
		return id;
	}
	
	
	/**
	 * @return
	 */
	public Integer getIdAsInteger() {
		return Integer.valueOf(id);
	}

	/**
	 * @return
	 */
	public String getDescrizione() {
		return descrizione;
	}
	
}
