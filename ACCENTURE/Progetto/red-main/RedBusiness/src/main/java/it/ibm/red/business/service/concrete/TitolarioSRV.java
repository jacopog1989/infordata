package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import it.ibm.red.business.dao.ITitolarioDAO;
import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.ITitolarioSRV;

/**
 * Service che gestisce i titolari.
 */
@Service
@Component
public class TitolarioSRV extends AbstractService implements ITitolarioSRV {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = 9064144180077961155L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TitolarioSRV.class);

	/**
	 * DAO.
	 */
	@Autowired
	private ITitolarioDAO titolarioDAO;

	/**
	 * @see it.ibm.red.business.service.facade.ITitolarioFacadeSRV#getFigliRadice(java.lang.Long,
	 *      java.lang.Long).
	 */
	@Override
	public List<TitolarioDTO> getFigliRadice(final Long idAOO, final Long idNodo) {
		Connection con = null;
		
		try {
			con = getDataSource().getConnection();
			return titolarioDAO.getFigliRadice(idAOO, idNodo, con);
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage(), e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITitolarioFacadeSRV#getFigliRadiceNonDisattivati(java.lang.Long,
	 *      java.lang.Long).
	 */
	@Override
	public List<TitolarioDTO> getFigliRadiceNonDisattivati(final Long idAOO, final Long idNodo) {
		Connection con = null;
		
		try {
			con = getDataSource().getConnection();
			return titolarioDAO.getFigliRadiceNonDisattivati(idAOO, idNodo, con);
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage(), e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITitolarioFacadeSRV#getFigliByIndice(java.lang.Long,
	 *      java.lang.String).
	 */
	@Override
	public List<TitolarioDTO> getFigliByIndice(final Long idAOO, final String indice) {
		Connection con = null;
		try {
			con = getDataSource().getConnection();
			return titolarioDAO.getFigliByIndiceNonDisattivati(idAOO, indice, con);
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage(), e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITitolarioFacadeSRV#getFigliByIndiceNonDisattivati(java.lang.Long,
	 *      java.lang.String).
	 */
	@Override
	public List<TitolarioDTO> getFigliByIndiceNonDisattivati(final Long idAOO, final String indice) {
		Connection con = null;
		try {
			con = getDataSource().getConnection();
			return titolarioDAO.getFigliByIndice(idAOO, indice, con);
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage(), e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ITitolarioSRV#getFigliRadice(java.lang.Long,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<TitolarioDTO> getFigliRadice(final Long idAOO, final Long idNodo, final Connection connection) {
		return titolarioDAO.getFigliRadice(idAOO, idNodo, connection);
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITitolarioFacadeSRV#getFigliRadiceNonDisattivati(java.lang.Long,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<TitolarioDTO> getFigliRadiceNonDisattivati(final Long idAOO, final Long idNodo, final Connection connection) {
		return titolarioDAO.getFigliRadiceNonDisattivati(idAOO, idNodo, connection);
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITitolarioFacadeSRV#getNodoByIndice(java.lang.Long,
	 *      java.lang.String).
	 */
	@Override
	public TitolarioDTO getNodoByIndice(final Long idAOO, final String indice) {
		Connection con = null;
		
		try {
			con = getDataSource().getConnection();
			return getNodoByIndice(idAOO, indice, con);
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage(), e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ITitolarioSRV#getNodoByIndice(java.lang.Long,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public TitolarioDTO getNodoByIndice(final Long idAOO, final String indice, final Connection con) {
		return titolarioDAO.getNodoByIndice(idAOO, indice, con);
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITitolarioFacadeSRV#getNodiAutocomplete(java.lang.Long,
	 *      java.lang.Long, java.lang.String).
	 */
	@Override
	public List<TitolarioDTO> getNodiAutocomplete(final Long idAOO, final Long idUfficio, final String parola) {
		List<TitolarioDTO> alberoTitolari = null;
		
		Connection con = null;
		try {
			con = getDataSource().getConnection();
			
			// Si recuperano i titolari del primo livello
			List<TitolarioDTO> titolariPrimoLivello = titolarioDAO.getFigliRadice(idAOO, idUfficio, con);
			
			if (!CollectionUtils.isEmpty(titolariPrimoLivello)) {
				alberoTitolari = new ArrayList<>();
				
				// Per ogni titolario del primo livello, si recuperano i titolari di tutti i suoi sotto-livelli
				for (TitolarioDTO titolario : titolariPrimoLivello) {
					alberoTitolari.addAll(titolarioDAO.getFigliNodoTitolarioAutocomplete(idAOO, titolario.getIndiceClassificazione(), parola.trim(), con));
				}
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage(), e);
		} finally {
			closeConnection(con);
		}
		
		return alberoTitolari;
	}

}
