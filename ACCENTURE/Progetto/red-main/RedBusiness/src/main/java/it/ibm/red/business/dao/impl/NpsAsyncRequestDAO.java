package it.ibm.red.business.dao.impl;

import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.INpsAsyncRequestDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.nps.dto.AsyncRequestDTO;
import it.ibm.red.business.utils.SerializationUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class NpsAsyncRequestDAO.
 *
 * @author CPIERASC
 * 
 *         Dao per la gestione delle richieste asincrone verso NPS.
 */
@SuppressWarnings("rawtypes")
@Repository
public class NpsAsyncRequestDAO extends AbstractDAO implements INpsAsyncRequestDAO {

	/**
	 * Messaggio di errore aggiornamento richiesta asincrona.
	 */
	private static final String ERROR_UPDATE_ASYNC_REQUEST = "Errore nell'aggiornamento della richiesta asincrona nel db ";
	/**
	 * LABEL Protocollo.
	 */
	private static final String PROT_LABEL = " ( prot: ";
	/**
	 * SERIAL VERSION UID.
	 */
	private static final long serialVersionUID = -8737216208371075810L;
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NpsAsyncRequestDAO.class.getName());

	/**
	 * Insert.
	 *
	 * @param asyncRequest
	 *            the async request
	 * @param connection
	 *            the connection
	 */
	@Override
	public final int insert(final AsyncRequestDTO asyncRequest, final Connection connection) {
		PreparedStatement ps = null;
		int result = 0;
		int idNar = 0;
		ResultSet rs = null;

		try {
			ps = connection.prepareStatement("SELECT SEQ_NPS_ASYNC_REQUEST.NEXTVAL FROM DUAL");
			rs = ps.executeQuery();
			if (rs.next()) {
				idNar = rs.getInt(1);
			}
			rs.close();
			if (idNar <= 0) {
				throw new RedException("Impossibile recuperare la sequence della tabella NPS_ASYNCREQUEST_EVO");
			}

			final byte[] obj = SerializationUtils.serialize(asyncRequest.getInput());
			final Blob blob = connection.createBlob();
			blob.setBytes(1, obj);

			final Clob clob = connection.createClob();
			closeStatement(ps);
			int n = 1;
			ps = connection.prepareStatement("INSERT into NPS_ASYNCREQUEST_EVO(ID_NAR, ASYNC_INPUT_BLOB, ASYNC_INPUT_CLOB, FQN_INPUT,"
					+ "DATA_CREAZIONE, DATA_LAVORAZIONE, PRIORITY, PARTITION, METHOD_NAME, CLASS_NAME, FQN_FINALIZER, STATO, ID_NAR_EXECUTE_REQUIRED, "
					+ "NUMEROANNOPROTOCOLLO, NUMEROANNOPROTOCOLLOEMERGENZA, CLASS_NAME_INPUT, IDDOCUMENTO) "
					+ "VALUES (?, ?, ?, ?, sysdate, sysdate, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			ps.setInt(n++, idNar);
			ps.setBlob(n++, blob);
			ps.setClob(n++, clob);
			ps.setString(n++, asyncRequest.getFqn_input());
			ps.setInt(n++, asyncRequest.getPriority());
			ps.setInt(n++, asyncRequest.getPartition());
			ps.setString(n++, asyncRequest.getMethodName());
			ps.setString(n++, asyncRequest.getClassName());
			ps.setString(n++, asyncRequest.getFqn_finalizer());
			ps.setInt(n++, asyncRequest.getStato());
			ps.setInt(n++, asyncRequest.getId_nar_execute_required());
			if (StringUtils.isNullOrEmpty(asyncRequest.getNumeroAnnoProtocollo())) {
				ps.setNull(n++, Types.VARCHAR);
			} else {
				ps.setString(n++, asyncRequest.getNumeroAnnoProtocollo());
			}
			if (StringUtils.isNullOrEmpty(asyncRequest.getNumeroAnnoProtocolloEmergenza())) {
				ps.setNull(n++, Types.VARCHAR);
			} else {
				ps.setString(n++, asyncRequest.getNumeroAnnoProtocolloEmergenza());
			}
			ps.setString(n++, asyncRequest.getClassNameInput());
			ps.setString(n++, asyncRequest.getIdDocumento());
			result = ps.executeUpdate();

			if (result > 0) {
				LOGGER.info("Inserimento della richiesta asincrona effettuata con successo");
			} else {
				throw new RedException("Errore nella richiesta asincrona nel db " + idNar);
			}
		} catch (final Exception e) {
			LOGGER.error("Non è stato possibile recuperare il file originale: " + idNar, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return idNar;
	}

	/**
	 * @see it.ibm.red.business.dao.INpsAsyncRequestDAO#getAsyncRequests(int, int, int, java.sql.Connection).
	 */
	@Override
	public List<AsyncRequestDTO> getAsyncRequests(final int idAoo, final int partition, final int priority, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<AsyncRequestDTO> richieste = null;
		try {

			final StringBuilder sb = new StringBuilder();
			sb.append("select * from NPS_ASYNCREQUEST_EVO where id_nar in ( ");
			sb.append("select id_nar from ( ");
			sb.append("select * from ( ");
			sb.append("select id_nar,data_creazione,data_lavorazione,numeroannoprotocollo from NPS_ASYNCREQUEST_EVO ");
			sb.append("    where numeroannoprotocollo is not null ");
			sb.append("      and stato IN (1,4)  ");
			sb.append("      and count_retry < max_retry ");
			sb.append("      and partition = ?  ");
			sb.append("      and priority = ? ");
			sb.append("      and id_nar_execute_required = 0 ");
			sb.append(" union ");
			sb.append("select id_nar,data_creazione,data_lavorazione,numeroannoprotocollo from NPS_ASYNCREQUEST_EVO n ");
			sb.append("    where numeroannoprotocollo is not null  ");
			sb.append("      and stato IN (1,4)  ");
			sb.append("      and count_retry < max_retry ");
			sb.append("      and partition = ?  ");
			sb.append("      and priority = ? ");
			sb.append("      and id_nar_execute_required > 0  ");
			sb.append("      and exists (select 1 from NPS_ASYNCREQUEST_EVO  ");
			sb.append("                   where id_nar = n.id_nar_execute_required ");
			sb.append("                     and stato in (-1,3)) ");
			sb.append(") a ");
			sb.append("order by a.id_nar ");
			sb.append(") where rownum <= (select async_max_elaborations from NPS_CONFIGURATION where idaoo = ?)) for update skip locked ");

			final String sql = sb.toString();

			ps = connection.prepareStatement(sql);
			ps.setInt(1, partition);
			ps.setInt(2, priority);
			ps.setInt(3, partition);
			ps.setInt(4, priority);
			ps.setInt(5, idAoo);
			rs = ps.executeQuery();
			richieste = fetchResult(rs);

			// aggiorno lo stato delle requests recuperate
			presaInCaricoAsyncRequests(richieste, connection);

		} catch (final Exception e) {
			LOGGER.error("Non è stato possibile recuperare la richiesta asincrona per NPS", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return richieste;
	}

	private static List<AsyncRequestDTO> fetchResult(final ResultSet rs) throws SQLException {
		final List<AsyncRequestDTO> richieste = new ArrayList<>();
		AsyncRequestDTO asyncRequestDTO = null;
		while (rs.next()) {
			asyncRequestDTO = new AsyncRequestDTO();
			populateVO(asyncRequestDTO, rs);
			richieste.add(asyncRequestDTO);
		}
		return richieste;
	}

	@SuppressWarnings("unchecked")
	private static void populateVO(final AsyncRequestDTO asyncRequestDTO, final ResultSet rs) throws SQLException {
		asyncRequestDTO.setClassName(rs.getString("CLASS_NAME"));
		asyncRequestDTO.setDataCreazione(rs.getDate("DATA_CREAZIONE"));
		asyncRequestDTO.setDataLavorazione(rs.getDate("DATA_LAVORAZIONE"));
		asyncRequestDTO.setFqn_finalizer(rs.getString("FQN_FINALIZER"));
		asyncRequestDTO.setFqn_input(rs.getString("FQN_INPUT"));
		final Blob blob = rs.getBlob("ASYNC_INPUT_BLOB");
		byte[] blobAsBytes = null;

		if (blob != null) {
			final int blobLength = (int) blob.length();
			blobAsBytes = blob.getBytes(1, blobLength);
			blob.free();
			asyncRequestDTO.setInput(SerializationUtils.deserialize(blobAsBytes));
		}

		asyncRequestDTO.setId_nar(rs.getInt("ID_NAR"));
		asyncRequestDTO.setMethodName(rs.getString("METHOD_NAME"));
		asyncRequestDTO.setPartition(rs.getInt("PARTITION"));
		asyncRequestDTO.setPriority(rs.getInt("PRIORITY"));
		asyncRequestDTO.setStato(rs.getInt("STATO"));
		asyncRequestDTO.setNumeroAnnoProtocollo(rs.getString("NUMEROANNOPROTOCOLLO"));
		asyncRequestDTO.setNumeroAnnoProtocolloEmergenza(rs.getString("NUMEROANNOPROTOCOLLOEMERGENZA"));
		asyncRequestDTO.setClassNameInput(rs.getString("CLASS_NAME_INPUT"));
		asyncRequestDTO.setIdDocumento(rs.getString("IDDOCUMENTO"));
	}

	/**
	 * @see it.ibm.red.business.dao.INpsAsyncRequestDAO#updateStatoAsyncRequest(java.lang.String,
	 *      int, int, java.lang.String, int, java.sql.Connection).
	 */
	@Override
	public int updateStatoAsyncRequest(final String numeroAnnoProtocollo, final int idNar, final int stato, final String eccezione, final int countRetry,
			final Connection connection) {
		PreparedStatement ps = null;
		int result = 0;
		try {
			ps = connection.prepareStatement(
					"update NPS_ASYNCREQUEST_EVO set stato = ?, exception = exception ||' '||count_retry||'- ' ||  ?, count_retry = count_retry + ? where id_nar = ?");
			ps.setInt(1, stato);
			ps.setString(2, eccezione);
			ps.setInt(3, countRetry);
			ps.setInt(4, idNar);

			result = ps.executeUpdate();

			if (result > 0) {
				LOGGER.info("Update request asincrona " + idNar + PROT_LABEL + numeroAnnoProtocollo + " ) effettuata con successo. Stato: " + stato);
			} else {
				throw new RedException(ERROR_UPDATE_ASYNC_REQUEST + idNar + PROT_LABEL + numeroAnnoProtocollo + " )");
			}
		} catch (final Exception e) {
			LOGGER.error(ERROR_UPDATE_ASYNC_REQUEST + idNar + PROT_LABEL + numeroAnnoProtocollo + " )", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
		return result;
	}

	/**
	 * @see it.ibm.red.business.dao.INpsAsyncRequestDAO#getPartition(int, java.sql.Connection).
	 */
	@Override
	public int getPartition(final int idAoo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			final String sql = "SELECT PARTITION FROM NPS_PARTITION WHERE DESCRIZIONE = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, Integer.toString(idAoo));
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getInt("PARTITION");
			}

			return 1;

		} catch (final Exception e) {
			LOGGER.error("Non è stato possibile recuperare la partizione dell'AOO " + idAoo, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.INpsAsyncRequestDAO#recuperaItemProtocolliEmergenza(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public List<AsyncRequestDTO> recuperaItemProtocolliEmergenza(final String infoProtocolloEmergenza, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<AsyncRequestDTO> asyncRequests = null;
		try {
			final String sql = "SELECT * FROM NPS_ASYNCREQUEST_EVO n WHERE numeroannoprotocollo is null and numeroannoprotocolloemergenza = ? order by id_nar ";

			ps = connection.prepareStatement(sql);
			ps.setString(1, infoProtocolloEmergenza);
			rs = ps.executeQuery();
			asyncRequests = fetchMultiResult(rs);

		} catch (final Exception e) {
			LOGGER.error("Non è stato possibile recuperare le richieste asincrone per NPS associate al protocollo di emergenza " + infoProtocolloEmergenza, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return asyncRequests;
	}

	/**
	 * @see it.ibm.red.business.dao.INpsAsyncRequestDAO#aggiornaProtocolloUfficiale(it.ibm.red.business.nps.dto.AsyncRequestDTO,
	 *      java.sql.Connection).
	 */
	@Override
	public int aggiornaProtocolloUfficiale(final AsyncRequestDTO item, final Connection connection) {
		PreparedStatement ps = null;

		try {

			final byte[] obj = SerializationUtils.serialize(item.getInput());
			final Blob blob = connection.createBlob();
			blob.setBytes(1, obj);

			ps = connection.prepareStatement("update NPS_ASYNCREQUEST_EVO set numeroannoprotocollo = ?, async_input_blob = ? where id_nar = ?");

			ps.setString(1, item.getNumeroAnnoProtocollo());
			ps.setBlob(2, blob);
			ps.setInt(3, item.getId_nar());

			return ps.executeUpdate();

		} catch (final Exception e) {
			LOGGER.error("Errore nell'aggiornamento della richiesta asincrona " + item.getId_nar() + PROT_LABEL + item.getNumeroAnnoProtocollo() + " )  ( protemergenza: "
					+ item.getNumeroAnnoProtocolloEmergenza() + " )", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}

	}

	/**
	 * @see it.ibm.red.business.dao.INpsAsyncRequestDAO#isActivitiesComplete(java.lang.String,
	 *      int, java.sql.Connection).
	 */
	@Override
	public boolean isActivitiesComplete(final String numeroAnnoProtocollo, final int idNar, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int countError = 0;
		try {
			final String sql = "SELECT count(*) as NUMERR FROM NPS_ASYNCREQUEST_EVO WHERE numeroannoprotocollo = ? AND id_nar < ? AND stato <> 3";

			ps = connection.prepareStatement(sql);
			ps.setString(1, numeroAnnoProtocollo);
			ps.setInt(2, idNar);
			rs = ps.executeQuery();
			if (rs.next()) {
				countError = rs.getInt("NUMERR");
			}

		} catch (final Exception e) {
			LOGGER.error("Non è stato possibile recuperare la richiesta asincrona per NPS", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return (countError == 0);
	}

	/**
	 * @see it.ibm.red.business.dao.INpsAsyncRequestDAO#getMaxIdNarByProtocollo(java.lang.String,
	 *      boolean, java.sql.Connection).
	 */
	@Override
	public int getMaxIdNarByProtocollo(final String infoProtocollo, final boolean isEmergenzaNotUpdated, final Connection connection) {
		int maxIdNar = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			final String infoProtocolloQuery = isEmergenzaNotUpdated ? "NUMEROANNOPROTOCOLLOEMERGENZA" : "NUMEROANNOPROTOCOLLO";

			final String sql = "SELECT MAX(n.ID_NAR) AS MAX_ID_NAR FROM NPS_ASYNCREQUEST_EVO n WHERE n." + infoProtocolloQuery + " = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, infoProtocollo);

			rs = ps.executeQuery();
			if (rs.next()) {
				maxIdNar = rs.getInt("MAX_ID_NAR");
			}
		} catch (final Exception e) {
			LOGGER.error("Non è stato possibile recuperare il max ID_NAR per il protocollo: " + infoProtocollo, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return maxIdNar;
	}

	private static List<AsyncRequestDTO> fetchMultiResult(final ResultSet rs) throws SQLException {
		final List<AsyncRequestDTO> asyncRequests = new ArrayList<>();
		while (rs.next()) {
			final AsyncRequestDTO asyncRequestDTO = new AsyncRequestDTO();
			populateVO(asyncRequestDTO, rs);
			asyncRequests.add(asyncRequestDTO);
		}
		return asyncRequests;
	}

	/**
	 * @see it.ibm.red.business.dao.INpsAsyncRequestDAO#presaInCaricoAsyncRequests(java.util.List,
	 *      java.sql.Connection).
	 */
	@Override
	public int presaInCaricoAsyncRequests(final List<AsyncRequestDTO> richieste, final Connection connection) {
		PreparedStatement ps = null;
		int result = 0;
		try {
			ps = connection.prepareStatement("update NPS_ASYNCREQUEST_EVO set stato = " + Constants.NpsConstants.STATO_IN_LAVORAZIONE + " where id_nar = ?");

			for (final AsyncRequestDTO request : richieste) {
				ps.setInt(1, request.getId_nar());

				result = ps.executeUpdate();

				if (result > 0) {
					LOGGER.info("Update request asincrona " + request.getId_nar() + PROT_LABEL + request.getNumeroAnnoProtocollo() + " ) effettuata con successo. Stato: "
							+ Constants.NpsConstants.STATO_IN_LAVORAZIONE);
				} else {
					closeStatement(ps);
					throw new RedException(ERROR_UPDATE_ASYNC_REQUEST + request.getId_nar() + PROT_LABEL + request.getNumeroAnnoProtocollo() + " )");
				}
			}

		} catch (final RedException e) {
			throw e;
		} catch (final Exception e) {
			LOGGER.error("Errore nell'aggiornamento delle richieste asincrone", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
		return result;
	}
	
	/**
	 * @see it.ibm.red.business.dao.INpsAsyncRequestDAO#isActivityPresent(java.lang.String,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public boolean isActivityPresent(final String documentTitle, final String methodName, final Connection connection) {
		boolean activityPresent = false;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			final String sql = "SELECT 1 FROM NPS_ASYNCREQUEST_EVO WHERE IDDOCUMENTO = ? AND METHOD_NAME = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, documentTitle);
			ps.setString(2, methodName);

			rs = ps.executeQuery();
			if (rs.next()) {
				activityPresent = true;
			}
		} catch (final Exception e) {
			LOGGER.error("Non è stato possibile recuperare l'informazione associata al documento: " + documentTitle, e);
			throw new RedException("Non è stato possibile recuperare l'informazione associata al documento: " + documentTitle, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return activityPresent;
	}

}
