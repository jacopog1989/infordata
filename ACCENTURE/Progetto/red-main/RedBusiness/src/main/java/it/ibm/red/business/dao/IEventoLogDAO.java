package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.EventoLogDTO;
import it.ibm.red.business.dto.FascicoloFlussoDTO;
import it.ibm.red.business.enums.EventTypeEnum;

/**
 * 
 * @author m.crescentini
 *
 *         Dao gestione evento.
 */
public interface IEventoLogDAO extends Serializable {

	/**
	 * Inserisce l'evento.
	 * 
	 * @param eventoLog
	 * @param idAoo
	 * @param dwhCon
	 */
	void inserisciEvento(EventoLogDTO eventoLog, Long idAoo, Connection dwhCon);

	/**
	 * Ottiene l'evento agli atti.
	 * 
	 * @param dwhCon
	 * @param documentTitle
	 * @param idAoo
	 * @return evento
	 */
	EventoLogDTO getAttiEvent(Connection dwhCon, String documentTitle, Long idAoo);

	/**
	 * Ricerca gli eventi per la ricerca dei documenti.
	 * 
	 * @param valoriRicerca
	 * @param dwhCon
	 * @return lista di eventi
	 */
	List<EventoLogDTO> cercaEventiPerRicercaDocumenti(Map<String, ?> valoriRicerca, Connection dwhCon);

	/**
	 * Ricerca gli eventi dello storico dall'id del documento.
	 * 
	 * @param idDocumento
	 * @param idAoo
	 * @param dwhCon
	 * @return lista di eventi
	 */
	List<EventoLogDTO> getEventiStoricoByIdDocumento(int idDocumento, Long idAoo, Connection dwhCon);

	/**
	 * Ottiene l'ultimo evento di chiusura del documento in ingresso.
	 * 
	 * @param dwhCon
	 * @param idDocumento
	 * @param idAoo
	 * @return evento
	 */
	EventoLogDTO getLastChiusuraIngressoEvent(Connection dwhCon, int idDocumento, int idAoo);

	/**
	 * Ottiene il primo evento.
	 * 
	 * @param idDocumento
	 * @param idAoo
	 * @param dwhCon
	 * @return evento
	 */
	EventoLogDTO getFirstUserEvent(int idDocumento, Long idAoo, Connection dwhCon);

	/**
	 * Verifica se l'evento è presente per il documento.
	 * 
	 * @param idDocumento
	 * @param idAoo
	 * @param tipoEvento
	 * @param dwhCon
	 * @return true o false
	 */
	boolean isEventoPresentePerDocumento(int idDocumento, Long idAoo, EventTypeEnum tipoEvento, Connection dwhCon);

	/**
	 * Filtra i documenti per data dell'evento.
	 * 
	 * @param documenti
	 * @param dataDa
	 * @param dataA
	 * @param dwhCon
	 */
	void filtraDocumentiByDataEvento(Map<String, FascicoloFlussoDTO> documenti, Date dataDa, Date dataA, Connection dwhCon);

	/**
	 * Verifica se il documento è stato già spedito.
	 * 
	 * @param con
	 * @param documentTitle
	 * @return true o false
	 */
	boolean giaSpedito(Connection con, String documentTitle);

	/**
	 * Ottiene l'evento di predisposizione.
	 * 
	 * @param documentTitle
	 * @param idAoo
	 * @param con
	 * @return evento
	 */
	EventoLogDTO getPredisponiEvent(String documentTitle, Long idAoo, Connection con);

	/**
	 * Ottieni l'ufficio creatore dalla deventicustom.
	 * @param documentTitle
	 * @param idAoo
	 * @param con
	 * @return idufficio
	 */ 
	HashMap<String, Integer> getIdUfficioCreatore(List<String> documentTitle, Long idAoo, Connection conFilenet);
	
	/**
	 * Ottiene l'evento di predisposizione
	 * che non chiude il workflow.
	 * 
	 * @param documentTitle
	 * @param idAoo
	 * @param filenetConnection
	 * @return evento
	 */
	EventoLogDTO getPredisponiEventWfAperto(String documentTitle, Long idAoo, Connection filenetConnection);

}
