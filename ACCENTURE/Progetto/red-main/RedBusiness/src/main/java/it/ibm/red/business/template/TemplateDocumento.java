package it.ibm.red.business.template;

import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.safety.Whitelist;
import org.springframework.util.CollectionUtils;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DetailEmailDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.logger.REDLogger;

/**
 * @author raffaele.cassandra
 */
public class TemplateDocumento extends Template {

	private static final String TD_COLSPAN_6_HR_STYLE_WIDTH_100_HR_TD = "<td colspan=\"6\"><hr style=\"width: 100%\"></hr></td>";

	private static final String HTTPS = "https";

	private static final String WIDTH = "width";

	private static final String BLOCKQUOTE = "blockquote";

	/**
	 * Stralcio di html.
	 */
	private static final String LABEL_TD = "</label></td>";

	/**
	 * Stralcio di html.
	 */
	private static final String TR = "</tr>";

	private static final long serialVersionUID = 8949100795842297557L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TemplateDocumento.class.getName());

	/**
	 * Costruttore vuoto.
	 */
	public TemplateDocumento() {
		// Questo costruttore è lasciato intenzionalmente vuoto.
	}

	/**
	 * @see it.ibm.red.business.template.Template#getHeaderTable(it.ibm.red.business.dto.SalvaDocumentoRedDTO).
	 */
	@Override
	public StringBuilder getHeaderTable(final SalvaDocumentoRedDTO documento) {
		final StringBuilder headerTable = new StringBuilder();
		LOGGER.info("Generazione dell'Header HTML per il documento.");
		// Si crea la prima tabella
		headerTable.append("<table border=\"0\" style=\"width: 650px;\">");
		try {
			String title;
			if (documento.getAnnoProtocollo() != null && documento.getAnnoProtocollo() > -1 && documento.getNumeroProtocollo() != null
					&& documento.getNumeroProtocollo() > -1) {
				title = "Prot. " + documento.getNumeroProtocollo() + "/" + documento.getAnnoProtocollo();
			} else {
				title = "Documento " + documento.getDocumentTitle();
			}
			// Titolo Documento
			headerTable.append("<tr>");
			headerTable.append("<td><label class=\"docsubtitle\">" + title + LABEL_TD);
			headerTable.append(TR);

			headerTable.append("<tr>");
			headerTable.append(TD_COLSPAN_6_HR_STYLE_WIDTH_100_HR_TD);
			headerTable.append(TR);

			headerTable.append("<tr>");
			headerTable.append("<td><label class=\"scriptaLabelBold\">Tipologia Documento :</label></td>");
			headerTable.append("<td><label id=\"tipoDocumento\" name=\"tipoDocumento\" class=\"scripta\">" + (documento.getTipologiaDocumento() != null
					? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(documento.getTipologiaDocumento().getDescrizione())
					: "") + LABEL_TD);
			headerTable.append(TR);

			headerTable.append("<tr>");
			headerTable.append("<td><label class=\"scriptaLabelBold\">Indice di classificazione del Fascicolo: </label></td>");
			headerTable.append("<td><label id=\"indiceClassificazione\" name=\"indiceClassificazione\" class=\"scripta\">"
					+ (StringUtils.isNotBlank(documento.getDescrizioneIndiceClassificazioneFascicoloProcedimentale())
							? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(documento.getDescrizioneIndiceClassificazioneFascicoloProcedimentale())
							: Constants.EMPTY_STRING)
					+ LABEL_TD);
			headerTable.append(TR);

			headerTable.append("<tr>");
			headerTable.append("<td><label class=\"scriptaLabelBold\">Nome del fascicolo (NUMERO_ANNO_OGGETTO): </label></td>");
			headerTable.append(
					"<td><label id=\"nomeFascicolo\" name=\"nomeFascicolo\" class=\"scripta\">" + (StringUtils.isNotBlank(documento.getDescrizioneFascicoloProcedimentale())
							? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(documento.getDescrizioneFascicoloProcedimentale())
							: Constants.EMPTY_STRING) + LABEL_TD);
			headerTable.append(TR);

			headerTable.append("<tr>");
			headerTable.append("<td><label class=\"scriptaLabelBold\">Oggetto:</label></td>");
			headerTable.append("<td colspan=\"3\"><div id=\"oggetto\" name=\"oggetto\" class=\"scripta\" >"
					+ (documento.getOggetto() != null ? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(documento.getOggetto()) : Constants.EMPTY_STRING)
					+ "</div></td>");
			headerTable.append(TR);

			headerTable.append("<tr>");
			headerTable.append("<td><label class=\"scriptaLabelBold\">Numero di raccomandata / Plico: </label></td>");
			headerTable.append("<td><label id=\"numRaccomandata\" name=\"numRaccomandata\" class=\"scripta\">"
					+ (documento.getNumeroRaccomandata() != null ? documento.getNumeroRaccomandata() : Constants.EMPTY_STRING) + LABEL_TD);
			headerTable.append(TR);

			headerTable.append("<tr>");
			headerTable.append("<td><label class=\"scriptaLabelBold\">Data Scadenza :</label></td>");
			headerTable.append("<td><label id=\"dataScadenza\" name=\"dataScadenza\" class=\"scripta\">"
					+ (documento.getDataScadenza() != null ? documento.getDataScadenza() : Constants.EMPTY_STRING) + LABEL_TD);
			headerTable.append(TR);

			headerTable.append("<tr>");
			headerTable.append("<td><label class=\"scriptaLabelBold\">Note:</label></td>");
			headerTable.append("<td colspan=\"3\"><div id=\"note\" name=\"note\" class=\"scripta\" >"
					+ (StringUtils.isNotBlank(documento.getNote()) ? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(documento.getNote())
							: Constants.EMPTY_STRING)
					+ "</div></td>");
			headerTable.append(TR);
		} catch (final Exception e) {
			LOGGER.error("Errore nella creazione dell'HeaderTable per la creazione dell'html.", e);
		}
		// Si chiude la tabella
		headerTable.append("</table>");

		return headerTable;
	}

	/**
	 * @see it.ibm.red.business.template.Template#getFooterTable(it.ibm.red.business.dto.SalvaDocumentoRedDTO).
	 */
	@Override
	public StringBuilder getFooterTable(final SalvaDocumentoRedDTO documento) {
		final StringBuilder footerTable = new StringBuilder();

		// Si ottengono i Metadati Dinamici
		final Map<String, Object> metadatiDinamici = documento.getMetadatiDinamici();
		if (!CollectionUtils.isEmpty(metadatiDinamici)) {
			for (final Entry<String, Object> metadatoDinamico : metadatiDinamici.entrySet()) {
				final String nomeMetadatoDinamico = metadatoDinamico.getKey();
				Object valoreMetadatoDinamico = metadatoDinamico.getValue();
				if (valoreMetadatoDinamico instanceof Boolean) {
					if (Boolean.TRUE.equals(valoreMetadatoDinamico)) {
						valoreMetadatoDinamico = BooleanFlagEnum.SI.getDescription();
					} else {
						valoreMetadatoDinamico = BooleanFlagEnum.NO.getDescription();
					}
				}
				footerTable.append("<tr>");
				footerTable.append("<td><label class=\"scriptaLabelBold\">" + nomeMetadatoDinamico + ":</label></td>");
				footerTable.append("<td colspan=\"5\"><label id=\"" + nomeMetadatoDinamico + "\" name=\"" + nomeMetadatoDinamico + "\" class=\"scripta\">"
						+ (valoreMetadatoDinamico != null ? valoreMetadatoDinamico : Constants.EMPTY_STRING) + LABEL_TD);
				footerTable.append(TR);
			}
		}
		return footerTable;
	}

	/**
	 * @see it.ibm.red.business.template.Template#getDettaglioDocumentoTable(it.ibm.red.business.dto.SalvaDocumentoRedDTO).
	 */
	@Override
	public StringBuilder getDettaglioDocumentoTable(final SalvaDocumentoRedDTO documento) {
		return null;
	}

	/**
	 * Restituisce la descrizione dell'assegnatario per competenza.
	 * 
	 * @param documento
	 * @return
	 */
	public String getAssegnatarioCompetenza(final SalvaDocumentoRedDTO documento) {
		String descrizioneAssegnatarioCompetenza = Constants.EMPTY_STRING;

		if (!CollectionUtils.isEmpty(documento.getAssegnazioni())) {
			final AssegnazioneDTO assegnazioneCompetenza = documento.getAssegnazioni().get(0);
			if (assegnazioneCompetenza.getUtente() != null) {
				descrizioneAssegnatarioCompetenza = assegnazioneCompetenza.getUtente().getNome() + " " + assegnazioneCompetenza.getUtente().getCognome();
			} else if (assegnazioneCompetenza.getUfficio() != null) {
				descrizioneAssegnatarioCompetenza = assegnazioneCompetenza.getUfficio().getDescrizione();
			}
		}
		return descrizioneAssegnatarioCompetenza;
	}

	/**
	 * Restituisce la descrizione del tipo di assegnazione relativa al documento in
	 * input.
	 * 
	 * @param documento
	 * @return assegnazione
	 */
	public String getDescrizioneTipoAssegnazione(final SalvaDocumentoRedDTO documento) {
		String assegnazione = Constants.EMPTY_STRING;
		if (documento.getAssegnazioni() != null && !documento.getAssegnazioni().isEmpty()) {
			final TipoAssegnazioneEnum tipo = documento.getAssegnazioni().get(0).getTipoAssegnazione();
			switch (tipo) {
			case COMPETENZA:
				assegnazione = "Assegnazione per Competenza";
				break;
			case FIRMA:
				assegnazione = "Assegnazione per Firma";
				break;
			case SIGLA:
				assegnazione = "Assegnazione per Sigla";
				break;
			case VISTO:
				assegnazione = "Assegnazione per Visto";
				break;
			default:
				assegnazione = "Assegnazione non conosciuta";
				break;
			}
		}
		return assegnazione;
	}

	/**
	 * @see it.ibm.red.business.template.Template#getHeaderTableEmail(it.ibm.red.business.dto.DetailEmailDTO).
	 */
	@Override
	public StringBuilder getHeaderTableEmail(final DetailEmailDTO email) {
		final StringBuilder headerTable = new StringBuilder();
		LOGGER.info("Generazione dell'Header HTML per il documento.");
		// Si crea la prima tabella
		headerTable.append("<table border=\"0\" style=\"width: 650px;\">");
		try {
			headerTable.append("<tr>");
			headerTable.append(TD_COLSPAN_6_HR_STYLE_WIDTH_100_HR_TD);
			headerTable.append(TR);

		} catch (final Exception e) {
			LOGGER.error("Errore nella creazione dell'HeaderTable per la creazione dell'html.", e);
		}
		// Si chiude la tabella
		headerTable.append("</table>");

		return headerTable;
	}

	/**
	 * @see it.ibm.red.business.template.Template#getDettaglioDocumentoTableEmail(it.ibm.red.business.dto.DetailEmailDTO).
	 */
	@Override
	public StringBuilder getDettaglioDocumentoTableEmail(final DetailEmailDTO email) {
		final StringBuilder dettaglioTable = new StringBuilder();
		try {
			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Oggetto: </label></td>");
			dettaglioTable.append("<td colspan=\"5\"><label id=\"oggetto\" name=\"oggetto\" class=\"scripta\">"
					+ (email.getOggetto() != null ? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(email.getOggetto()) : Constants.EMPTY_STRING) + LABEL_TD);
			dettaglioTable.append(TR);

			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Mittente: </label></td>");
			dettaglioTable.append("<td colspan=\"5\"><label id=\"mittente\" name=\"mittente\" class=\"scripta\">"
					+ (email.getMittente() != null ? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(email.getMittente()) : Constants.EMPTY_STRING) + LABEL_TD);
			dettaglioTable.append(TR);

			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Destinatario: </label></td>");
			dettaglioTable.append("<td colspan=\"5\"><label id=\"destinatario\" name=\"destinatario\" class=\"scripta\">"
					+ (email.getMittente() != null ? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(email.getDestinatario()) : Constants.EMPTY_STRING)
					+ LABEL_TD);
			dettaglioTable.append(TR);

			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">DestinatariCC: </label></td>");
			dettaglioTable.append("<td colspan=\"5\"><label id=\"destinatariCC\" name=\"destinatariCC\" class=\"scripta\">"
					+ (email.getMittente() != null ? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(email.getDestinatarioCC()) : Constants.EMPTY_STRING)
					+ LABEL_TD);
			dettaglioTable.append(TR);

			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Data ricezione mail: </label></td>");
			dettaglioTable.append("<td colspan=\"5\"><label id=\"dataRicezione\" name=\"dataRicezione\" class=\"scripta\">"
					+ (email.getDataArrivo() != null ? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(email.getDataArrivo()) : Constants.EMPTY_STRING)
					+ LABEL_TD);
			dettaglioTable.append(TR);

			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Testo mail: </label></td>");
			// molte volte ci sono tag <br> senza la chiusra, questo manda in errore il
			// file. E' necessario effettuare la sostituzione.
			final Whitelist wt = new Whitelist();
			wt.addTags("a", "b", BLOCKQUOTE, "br", "caption", "cite", "code", "col", "colgroup", "dd", "div", "dl", "dt", "em", "h1", "h2", "h3", "h4", "h5", "h6", "i",
					"li", "ol", "p", "pre", "q", "small", "strike", "strong", "sub", "sup", "table", "tbody", "td", "tfoot", "th", "thead", "tr", "u", "ul");
			wt.addAttributes("a", "href", "title");
			wt.addAttributes(BLOCKQUOTE, "cite");
			wt.addAttributes("col", "span", WIDTH);
			wt.addAttributes("colgroup", "span", WIDTH);
			wt.addAttributes("align", "alt", "height", "src", "title", WIDTH);
			wt.addAttributes("ol", "start", "type");
			wt.addAttributes("q", "cite");
			wt.addAttributes("table", "summary", WIDTH);
			wt.addAttributes("td", "abbr", "axis", "colspan", "rowspan", WIDTH);
			wt.addAttributes("th", "abbr", "axis", "colspan", "rowspan", "scope", WIDTH);
			wt.addAttributes("ul", "type");
			wt.addProtocols("a", "href", "ftp", "http", HTTPS, "mailto");
			wt.addProtocols(BLOCKQUOTE, "cite", "http", HTTPS);
			wt.addProtocols("cite", "cite", "http", HTTPS);
			wt.addProtocols("src", "http", HTTPS);
			wt.addProtocols("q", "cite", "http", HTTPS);
			String finalHtmlText = "";
			if (email.getTesto() != null) {
				finalHtmlText = Jsoup.clean(email.getTesto(), wt).replace("<br>", "<br/>").replace("<\br>", "").replace("&nbsp;", "");
			}
			dettaglioTable.append("<td colspan=\"5\"><label id=\"testoMail\" name=\"testoMail\" class=\"scripta\">"
					+ (email.getTesto() != null ? finalHtmlText : Constants.EMPTY_STRING) + LABEL_TD);
			dettaglioTable.append(TR);

			dettaglioTable.append("<tr>");
			dettaglioTable.append(TD_COLSPAN_6_HR_STYLE_WIDTH_100_HR_TD);
			dettaglioTable.append(TR);
		} catch (final Exception e) {
			LOGGER.error("Errore nella creazione della sezione dettaglio documento per la creazione dell'html.", e);
		}

		return dettaglioTable;
	}

	/**
	 * @see it.ibm.red.business.template.Template#getDettaglioDocumentoTableEmailPlainText(it.ibm.red.business.dto.DetailEmailDTO).
	 */
	@Override
	public StringBuilder getDettaglioDocumentoTableEmailPlainText(final DetailEmailDTO email) {
		final StringBuilder dettaglioTable = new StringBuilder();
		try {
			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Oggetto: </label></td>");
			dettaglioTable.append("<td colspan=\"5\"><label id=\"oggetto\" name=\"oggetto\" class=\"scripta\">"
					+ (email.getOggetto() != null ? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(email.getOggetto()) : Constants.EMPTY_STRING) + LABEL_TD);
			dettaglioTable.append(TR);

			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Mittente: </label></td>");
			dettaglioTable.append("<td colspan=\"5\"><label id=\"mittente\" name=\"mittente\" class=\"scripta\">"
					+ (email.getMittente() != null ? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(email.getMittente()) : Constants.EMPTY_STRING) + LABEL_TD);
			dettaglioTable.append(TR);

			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Destinatario: </label></td>");
			dettaglioTable.append("<td colspan=\"5\"><label id=\"destinatario\" name=\"destinatario\" class=\"scripta\">"
					+ (email.getMittente() != null ? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(email.getDestinatario()) : Constants.EMPTY_STRING)
					+ LABEL_TD);
			dettaglioTable.append(TR);

			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">DestinatariCC: </label></td>");
			dettaglioTable.append("<td colspan=\"5\"><label id=\"destinatariCC\" name=\"destinatariCC\" class=\"scripta\">"
					+ (email.getMittente() != null ? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(email.getDestinatarioCC()) : Constants.EMPTY_STRING)
					+ LABEL_TD);
			dettaglioTable.append(TR);

			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Data ricezione mail: </label></td>");
			dettaglioTable.append("<td colspan=\"5\"><label id=\"dataRicezione\" name=\"dataRicezione\" class=\"scripta\">"
					+ (email.getDataArrivo() != null ? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(email.getDataArrivo()) : Constants.EMPTY_STRING)
					+ LABEL_TD);
			dettaglioTable.append(TR);

			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Testo mail: </label></td>");
			final String finalText = getFinalText(email);

			dettaglioTable.append("<td colspan=\"5\"><label id=\"testoMail\" name=\"testoMail\" class=\"scripta\">"
					+ (email.getTesto() != null ? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(finalText) : Constants.EMPTY_STRING) + LABEL_TD);
			dettaglioTable.append(TR);

			dettaglioTable.append("<tr>");
			dettaglioTable.append(TD_COLSPAN_6_HR_STYLE_WIDTH_100_HR_TD);
			dettaglioTable.append(TR);
		} catch (final Exception e) {
			LOGGER.error("Errore nella creazione della sezione dettaglio documento per la creazione dell'html.", e);
		}
		return dettaglioTable;
	}

	/**
	 * @param email
	 * @return final text
	 */
	private String getFinalText(final DetailEmailDTO email) {
		String finalText = "";

		if (email.getTesto() != null) {
			try {
				finalText = Jsoup.parse(email.getTesto()).text();
			} catch (final Exception e) {
				LOGGER.warn(e);
				finalText = email.getTesto();
			}
		}
		return finalText;
	}
}