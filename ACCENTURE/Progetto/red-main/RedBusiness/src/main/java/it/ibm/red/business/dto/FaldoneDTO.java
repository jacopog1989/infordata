package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * DTO che definisce il modello di un Faldone.
 */
public class FaldoneDTO implements Serializable, Comparable<FaldoneDTO> {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * su filenet -> id.
	 */
	private String guid;
	/**
	 * Nel name e' memorizzato un id numerico.
	 */
	private String name;
	/**
	 * DocumentTitle.
	 */
	private String documentTitle;
	/**
	 * su filenet -> Creator.
	 */
	private String creator;
	/**
	 * su filenet -> DateCreated.
	 */
	private Date dateCreated;
	/**
	 * su filenet -> Owner.
	 */
	private String owner;
	/**
	 * 
	 */
	private String nomeFaldone;
	/**
	 * 
	 */
	private String parentFaldone;
	/**
	 * 
	 */
	private String descrizioneFaldone;
	/**
	 * 
	 */
	private Integer idAOO;
	/**
	 * su filenet -> IsReserved.
	 */
	private Boolean reserved;

	/**
	 * 
	 */
	private String oggetto;

	/**
	 * 
	 */
	private String absolutePath;

	/**
	 * 
	 */
	private String nomePadre;
	/**
	 * 
	 */
	private String oggettoPadre;
	/**
	 * 
	 */
	private String gerarchiaPath;

	/**
	 * fascicolo selezionato per ottenere il faldone (presentation scoped).
	 */
	private FascicoloDTO fascicoloSelezionato;

	/**
	 * 
	 */
	public FaldoneDTO() {
	}

	/**
	 * @param guid
	 * @param name
	 * @param documentTitle
	 * @param creator
	 * @param dateCreated
	 * @param owner
	 * @param nomeFaldone
	 * @param parentFaldone
	 * @param descrizioneFaldone
	 * @param idAOO
	 * @param reserved
	 * @param oggetto
	 */
	public FaldoneDTO(final String guid, final String name, final String documentTitle, final String creator, final Date dateCreated, final String owner,
			final String nomeFaldone, final String parentFaldone, final String descrizioneFaldone, final Integer idAOO, final Boolean reserved, final String oggetto,
			final String absolutePath) {
		this.guid = guid;
		this.name = name;
		this.documentTitle = documentTitle;
		this.creator = creator;
		this.dateCreated = dateCreated;
		this.owner = owner;
		this.nomeFaldone = nomeFaldone;
		this.parentFaldone = parentFaldone;
		this.descrizioneFaldone = descrizioneFaldone;
		this.idAOO = idAOO;
		this.reserved = reserved;
		this.oggetto = oggetto;
		this.absolutePath = absolutePath;
	}

	/**
	 * @param guid
	 * @param name
	 * @param documentTitle
	 * @param creator
	 * @param dateCreated
	 * @param owner
	 * @param nomeFaldone
	 * @param parentFaldone
	 * @param descrizioneFaldone
	 * @param idAOO
	 * @param reserved
	 * @param oggetto
	 */
	public FaldoneDTO(final String guid, final String name, final String documentTitle, final String creator, final Date dateCreated, final String owner,
			final String nomeFaldone, final String parentFaldone, final String descrizioneFaldone, final Integer idAOO, final Boolean reserved, final String oggetto) {
		this(guid, name, documentTitle, creator, dateCreated, owner, nomeFaldone, parentFaldone, descrizioneFaldone, idAOO, reserved, oggetto, null);
	}

	/**
	 * Costruttore.
	 * 
	 * @param documentTitle
	 * @param dateCreated
	 * @param nomeFaldone
	 * @param parentFaldone
	 * @param descrizioneFaldone
	 * @param oggetto
	 */
	public FaldoneDTO(final String documentTitle, final Date dateCreated, final String nomeFaldone, final String parentFaldone, final String descrizioneFaldone,
			final String oggetto) {
		this(null, null, documentTitle, null, dateCreated, null, nomeFaldone, parentFaldone, descrizioneFaldone, null, null, oggetto);
	}

	/**
	 * Restituisce il guid.
	 * 
	 * @return guid
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * @see java.lang.Object#hashCode().
	 */
	@Override
	public int hashCode() {
		return 1;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object).
	 */
	@Override
	public boolean equals(final Object other) {
		Boolean output = false;
		if (other instanceof FaldoneDTO) {
			final FaldoneDTO obj = (FaldoneDTO) other;
			if (this.getNomeFaldone().equals(obj.getNomeFaldone())) {
				output = true;
			}
		}
		return output;
	}

	/**
	 * Restituisce il nome del faldone.
	 * 
	 * @return nome
	 */
	public String getName() {
		return name;
	}

	/**
	 * Restituisce il creatore del faldone.
	 * 
	 * @return creator
	 */
	public String getCreator() {
		return creator;
	}

	/**
	 * Restituisce la data di creazione del faldone.
	 * 
	 * @return data creazione
	 */
	public Date getDateCreated() {
		return dateCreated;
	}

	/**
	 * Restituisce l'owner del faldone.
	 * 
	 * @return owner
	 */
	public String getOwner() {
		return owner;
	}

	/**
	 * Restituisce il nome del faldone.
	 * 
	 * @return nome faldone
	 */
	public String getNomeFaldone() {
		return nomeFaldone;
	}

	/**
	 * Restituisce il parent del faldone.
	 * 
	 * @return parent
	 */
	public String getParentFaldone() {
		return parentFaldone;
	}

	/**
	 * Restituisce la descrizione del faldone.
	 * 
	 * @return descrizione faldone
	 */
	public String getDescrizioneFaldone() {
		return descrizioneFaldone;
	}

	/**
	 * Restituisce l'id dell'area omogenea organizzativa.
	 * 
	 * @return id AOO
	 */
	public Integer getIdAOO() {
		return idAOO;
	}

	/**
	 * Restituisce true se reserved, false altrimenti.
	 * 
	 * @return true se reserved, false altrimenti
	 */
	public Boolean getReserved() {
		return reserved;
	}

	/**
	 * Restituisce il documentTitle.
	 * 
	 * @return documentTitle
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Restituisce l'oggetto.
	 * 
	 * @return oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * Restituisce il nome del parent.
	 * 
	 * @return nome padre
	 */
	public String getNomePadre() {
		return nomePadre;
	}

	/**
	 * Imposta il nome del padre.
	 * 
	 * @param nomePadre
	 */
	public void setNomePadre(final String nomePadre) {
		this.nomePadre = nomePadre;
	}

	/**
	 * Restituisce l'absolutePath.
	 * 
	 * @return absolutePath
	 */
	public String getAbsolutePath() {
		return absolutePath;
	}

	/**
	 * Imposta l'absolutePath.
	 * 
	 * @param absolutePath
	 */
	public void setAbsolutePath(final String absolutePath) {
		this.absolutePath = absolutePath;
	}

	/**
	 * Restituisce l'oggetto del parent.
	 * 
	 * @return oggetto padre
	 */
	public String getOggettoPadre() {
		return oggettoPadre;
	}

	/**
	 * Imposta l'oggetto del parent.
	 * 
	 * @param oggettoPadre
	 */
	public void setOggettoPadre(final String oggettoPadre) {
		this.oggettoPadre = oggettoPadre;
	}

	/**
	 * @return the gerarchiaPath
	 */
	public final String getGerarchiaPath() {
		return gerarchiaPath;
	}

	/**
	 * @param gerarchiaPath the gerarchiaPath to set
	 */
	public final void setGerarchiaPath(final String gerarchiaPath) {
		this.gerarchiaPath = gerarchiaPath;
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object).
	 */
	@Override
	public int compareTo(final FaldoneDTO o) {
		return this.dateCreated.compareTo(o.getDateCreated());
	}

	/**
	 * Restituisce il fascicolo selezionato.
	 * 
	 * @return fascicolo selezionato
	 */
	public FascicoloDTO getFascicoloSelezionato() {
		return fascicoloSelezionato;
	}

	/**
	 * Imposta il fascicolo selezionato.
	 * 
	 * @param fascicoloSelezionato
	 */
	public void setFascicoloSelezionato(final FascicoloDTO fascicoloSelezionato) {
		this.fascicoloSelezionato = fascicoloSelezionato;
	}
}
