package it.ibm.red.business.enums;

/**
 * Enum che definisce lo stato di un documento cartaceo.
 */
public enum StatoDocCartaceoEnum {
	
	/**
	 * Valore.
	 */
	ACQUISITO(0),

	/**
	 * Valore.
	 */
	ELIMINATO(1);

	/**
	 * Identificativo.
	 */
	private Integer id;
	
	/**
	 * Costruttore.
	 * @param id
	 */
	StatoDocCartaceoEnum(final Integer id) {
		this.id = id;
	}

	/**
	 * Restituisce l'id dell'enum.
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

}
