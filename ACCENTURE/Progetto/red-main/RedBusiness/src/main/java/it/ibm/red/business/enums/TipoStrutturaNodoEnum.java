package it.ibm.red.business.enums;

/**
 * @author SLac
 *
 * Tipo struttura per filtrare i nodi nell'organigramma 
 */
public enum TipoStrutturaNodoEnum {
	

	/**
	 * Valore.
	 */
	IGNOTO(0),
	
	/**
	 * Valore.
	 */
	SEGRETERIA(1),
	
	/**
	 * Valore.
	 */
	SERVIZIO(2),
	
	/**
	 * Valore.
	 */
	SETTORE(3),
	
	/**
	 * Valore.
	 */
	ISPETTORATO(4);
	
	/**
	 * Valore.
	 */
	private int id;
	
	/**
	 * @param id
	 */
	TipoStrutturaNodoEnum(final int id) {
		this.id = id;
	}

	/**
	 * @return
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id
	 * @return
	 */
	public static TipoStrutturaNodoEnum get(final int id) {
		TipoStrutturaNodoEnum t = null;
		for (TipoStrutturaNodoEnum ts : TipoStrutturaNodoEnum.values()) {
			if (ts.getId() == id) {
				t = ts;
				break;
			}
		}
		return t;
	}
	
}
