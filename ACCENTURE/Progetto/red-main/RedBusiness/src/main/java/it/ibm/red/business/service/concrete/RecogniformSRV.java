/**
 * 
 */
package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.core.Document;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.IRecogniformDAO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.CodaTrasformazioneParametriDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DocumentoRecogniformDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.MasterDocCartaceoRedDTO;
import it.ibm.red.business.dto.NodoDTO;
import it.ibm.red.business.dto.SecurityDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.DocumentoRedFnDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.StatoDocCartaceoEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.FilenetException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.IRecogniformSRV;
import it.ibm.red.business.service.ISecuritySRV;
import it.ibm.red.business.service.ITipologiaDocumentoSRV;
import it.ibm.red.business.service.ITrasformazionePDFSRV;
import it.ibm.red.business.service.IUtenteSRV;

/**
 * @author adilegge
 *
 */
@Service
public class RecogniformSRV extends AbstractService implements IRecogniformSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 7824237207445011961L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RecogniformSRV.class.getName());

	/**
	 * Stato elaborata.
	 */
	private static final int STATO_ELABORATA = 2;

	/**
	 * Stato errore.
	 */
	private static final int STATO_ERRORE = 3;

	/**
	 * Service.
	 */
	@Autowired
	private IAooSRV aooSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ITrasformazionePDFSRV trasformazionePDFSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ISecuritySRV securitySRV;

	/**
	 * DAO.
	 */
	@Autowired
	private IRecogniformDAO recogniformDAO;

	/**
	 * Service.
	 */
	@Autowired
	private ITipologiaDocumentoSRV tipologiaDocumentoSRV;

	/**
	 * @see it.ibm.red.business.service.IRecogniformSRV#getCountDocumentiCartacei(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      boolean, boolean).
	 */
	@Override
	public final Integer getCountDocumentiCartacei(final IFilenetCEHelper fceh, final boolean flagRegistroRiservato, final boolean eliminati) {
		Integer count = 0;

		try {
			count = fceh.getCountDocumentiCartacei(eliminati, flagRegistroRiservato);

			LOGGER.info("Conteggio documenti cartacei" + (eliminati ? " eliminati" : "") + ". Totale documenti: " + count);
		} catch (final FilenetException e) {
			LOGGER.error(e);
			throw new RedException("Si è verificato un errore durante il conteggio dei documenti cartacei", e);
		}

		return count;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRecogniformFacadeSRV#getDocumentiCartacei(boolean,
	 *      boolean, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public List<MasterDocCartaceoRedDTO> getDocumentiCartacei(final boolean flagRegistroRiservato, final boolean eliminati, final UtenteDTO utente) {
		final List<MasterDocCartaceoRedDTO> docCartacei = new ArrayList<>();
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			// Si recuperano i documenti cartacei
			final List<Document> docCartaceiFn = fceh.getDocumentiCartacei(eliminati, flagRegistroRiservato);

			if (CollectionUtils.isNotEmpty(docCartaceiFn)) {
				MasterDocCartaceoRedDTO masterCartaceo = null;

				// Si trasformano i documenti FileNet recuperati in master
				for (final Document docCartaceoFn : docCartaceiFn) {
					masterCartaceo = TrasformCE.transform(docCartaceoFn, TrasformerCEEnum.FROM_DOCUMENTO_TO_MASTER_CARTACEO);
					docCartacei.add(masterCartaceo);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException("Si è verificato un errore durante il recupero dei documenti cartacei", e);
		} finally {
			popSubject(fceh);
		}

		return docCartacei;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRecogniformFacadeSRV#aggiornaStatoRecogniform(java.lang.String,
	 *      java.lang.String, it.ibm.red.business.enums.StatoDocCartaceoEnum,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public final EsitoOperazioneDTO aggiornaStatoRecogniform(final String barcode, final String guid, final StatoDocCartaceoEnum nuovoStato, final UtenteDTO utente) {
		LOGGER.info("aggiornaStatoRecogniform -> START. GUID: " + guid + ", nuovo stato: " + nuovoStato.name());
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(guid);
		esito.setNomeDocumento(barcode);
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final Map<String, Object> metadatoStatoRecogniform = new HashMap<>();
			metadatoStatoRecogniform.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.STATO_DOC_CARTACEO_METAKEY), nuovoStato.getId());
			fceh.updateMetadati(guid, metadatoStatoRecogniform);

			esito.setEsito(true);
			esito.setNote("Documento " + (StatoDocCartaceoEnum.ELIMINATO.equals(nuovoStato) ? "eliminato." : "ripristinato."));
		} catch (final Exception e) {
			LOGGER.error(e);
			esito.setNote("Si è verificato un errore durante " + (StatoDocCartaceoEnum.ELIMINATO.equals(nuovoStato) ? "l'eliminazione" : "il ripristino") + " del documento");
		} finally {
			popSubject(fceh);
		}

		LOGGER.info("aggiornaStatoRecogniform -> END. GUID: " + guid);
		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRecogniformFacadeSRV#rimuoviDocumento(java.lang.String,
	 *      java.lang.String, java.lang.String, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public final EsitoOperazioneDTO rimuoviDocumento(final String barcode, final String guid, final String guidPrincipale, final UtenteDTO utente) {
		LOGGER.info("rimuoviDocumento -> START. GUID: " + guid);
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(guid);
		esito.setNomeDocumento(barcode);
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			// Documento principale
			if (guidPrincipale == null) {
				fceh.eliminaDocumento(guid);
				// Allegato
			} else {
				fceh.deleteAllegatiByGuid(guidPrincipale, Arrays.asList(guid));
			}

			esito.setEsito(true);
			esito.setNote("Documento rimosso.");
		} catch (final Exception e) {
			LOGGER.error(e);
			esito.setNote("Si è verificato un errore durante la rimozione del documento");
		} finally {
			popSubject(fceh);
		}

		LOGGER.info("rimuoviDocumento -> END. GUID: " + guid);
		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRecogniformFacadeSRV#acquisisciDocumentiInCoda(int).
	 */
	@Override
	public void acquisisciDocumentiInCoda(final int idAoo) {
		LOGGER.info("AcquisisciDocumentiInCoda per l'AOO " + idAoo);
		Connection con = null;
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;

		List<DocumentoRecogniformDTO> documentiDaElaborare = null;

		try {
			final Integer maxBarcodeLength = Integer.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.MAX_BARCODE_LENGTH));

			// recupera l'AOO
			final Aoo aoo = aooSRV.recuperaAoo(idAoo);

			final String usernameKey = aoo.getCodiceAoo() + "." + PropertiesNameEnum.AOO_SISTEMA_AMMINISTRATORE.getKey();
			final String username = PropertiesProvider.getIstance().getParameterByString(usernameKey);
			final UtenteDTO utente = utenteSRV.getByUsername(username);

			// inizializza accesso al content engine in modalita administrator
			final AooFilenet aooFilenet = aoo.getAooFilenet();
			fceh = FilenetCEHelperProxy.newInstance(username, aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(), aooFilenet.getObjectStore(),
					utente.getIdAoo());
			fpeh = new FilenetPEHelper(username, aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(), aooFilenet.getConnectionPoint());

			con = setupConnection(getDataSource().getConnection(), false);

			final Map<Integer, String> stati = recuperaStati(con);
			documentiDaElaborare = recuperaDoc(idAoo, con);

			if (documentiDaElaborare != null) {

				LOGGER.info("Barcode da lavorare: " + documentiDaElaborare.size());

				String barcode = null;
				for (final DocumentoRecogniformDTO documentoDaElaborare : documentiDaElaborare) {

					try {

						barcode = documentoDaElaborare.getBarcode();

						// controllo la property lunghezza barcode per verificare se possiamo elaborarlo
						if (barcode.length() > maxBarcodeLength) {
							LOGGER.warn("La lunghezza del barcode da acquisire: " + barcode + " è superiore di quella configurata da sistema : " + maxBarcodeLength);
							recogniformDAO.updateStatoDocumento(STATO_ERRORE, barcode, con);
							continue;
						}

						final String guid = fceh.controllaBarcode(barcode);
						if (!StringUtils.isBlank(guid)) {
							// Barcode presente su Filenet/RedEvo: sincronizzo il content

							Document documentoPrincipaleFilenet = fceh.getDocumentByGuid(guid);
							final String idDoc = (String) TrasformerCE.getMetadato(documentoPrincipaleFilenet, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
							final VWWorkObject workflowPrincipale = fpeh.getWorkflowPrincipale(idDoc, aooFilenet.getIdClientAoo());
							if (workflowPrincipale == null) {
								recogniformDAO.updateStatoDocumento(STATO_ERRORE, barcode, con);
								continue;
							}

							// recupera allegati dalla coda recogniform
							final List<DocumentoRecogniformDTO> allegatiDaElaborare = recogniformDAO.getAllegatiDaPrincipale(barcode, con);
							documentoDaElaborare.setAllegati(allegatiDaElaborare);
							final Map<String, DocumentoRecogniformDTO> barcodeAllegatiMap = new HashMap<>();
							for (final DocumentoRecogniformDTO allegatoDaElaborare : allegatiDaElaborare) {
								barcodeAllegatiMap.put(allegatoDaElaborare.getBarcode(), allegatoDaElaborare);
							}

							// recupera gli allegati del documento filenet con barcode valorizzato
							final List<AllegatoDTO> allegatiFilenetConBarcode = getAllegatiConBarcode(fceh, guid, utente, con);

							if (!checkCorrispondenzaAllegati(allegatiDaElaborare, allegatiFilenetConBarcode, barcodeAllegatiMap, barcode)) {
								recogniformDAO.updateStatoDocumento(STATO_ERRORE, barcode, con);
								continue;
							}

							// aggiorna documento principale
							final String fileNamePrincipale = documentoDaElaborare.getNomeFile() != null ? documentoDaElaborare.getNomeFile()
									: documentoDaElaborare.getIdRecogniform() + ".pdf";
							Map<String, Object> metadati = new HashMap<>();
							metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), fileNamePrincipale);
							documentoPrincipaleFilenet = fceh.addVersion(documentoPrincipaleFilenet, documentoDaElaborare.getDataHandler().getInputStream(), metadati,
									fileNamePrincipale, documentoDaElaborare.getMimetype(), utente.getIdAoo());

							// aggiorna gli eventuali allegati
							DocumentoRecogniformDTO allegatoRecogniform = null;
							Document allegato = null;
							final List<String> idAllegatiDaTrasformare = new ArrayList<>();
							for (final AllegatoDTO all : allegatiFilenetConBarcode) {

								allegatoRecogniform = barcodeAllegatiMap.get(all.getBarcode());
								allegato = fceh.getDocumentByGuid(all.getGuid());

								final String fileNameAllegato = allegatoRecogniform.getNomeFile() != null ? allegatoRecogniform.getNomeFile()
										: allegatoRecogniform.getIdRecogniform() + ".pdf";

								metadati = new HashMap<>();
								metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY), allegatoRecogniform.getOggetto());
								metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), fileNameAllegato);

								fceh.addVersionCompoundDocument(allegato, allegatoRecogniform.getDataHandler().getInputStream(), allegatoRecogniform.getMimetype(), metadati,
										true, utente.getIdAoo());

								idAllegatiDaTrasformare.add(all.getDocumentTitle());
							}

							// Controllo se devo trasformare i documenti
							final Integer idTipoAssegnazione = (Integer) TrasformerPE.getMetadato(workflowPrincipale, PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY);
							trasformazionePDF(documentoPrincipaleFilenet, idAllegatiDaTrasformare, idTipoAssegnazione, utente, con);

							// Avanza il workflow
							final Integer idUtenteMittente = (Integer) TrasformerPE.getMetadato(workflowPrincipale, PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY);
							final boolean esitoWF = fpeh.nextStepMittente(workflowPrincipale.getWorkflowNumber(), idUtenteMittente.longValue(), "", null);
							if (!esitoWF) {
								recogniformDAO.updateStatoDocumento(STATO_ERRORE, barcode, con);
								continue;
							}
						} else {
							// Barcode non ancora presente su Filenet/RedEvo: lo inserisco nel folder
							// Acquisiti

							final DocumentoRedFnDTO docRedFn = new DocumentoRedFnDTO();

							valorizzaDocumento(docRedFn, documentoDaElaborare, idAoo, stati, false);

							final List<DocumentoRecogniformDTO> allegatiDaElaborare = recogniformDAO.getAllegatiDaPrincipale(barcode, con);
							valorizzaAllegati(docRedFn, allegatiDaElaborare, idAoo, stati);

							final Document documentoCreato = fceh.creaDocumento(docRedFn, null, null, utente.getIdAoo());
							if (documentoCreato == null) {
								recogniformDAO.updateStatoDocumento(STATO_ERRORE, barcode, con);
								continue;
							}
						}

						recogniformDAO.updateStatoDocumento(STATO_ELABORATA, barcode, con);

					} catch (final Exception e) {
						LOGGER.error("Errore in fase di elaborazione del barcode: " + barcode, e);
						recogniformDAO.updateStatoDocumento(STATO_ERRORE, barcode, con);
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore generico in fase di recupero dei documenti dalla coda recogniform per l'AOO: " + idAoo, e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
			logoff(fpeh);
		}
	}

	private List<DocumentoRecogniformDTO> recuperaDoc(final int idAoo, final Connection con) {

		List<DocumentoRecogniformDTO> documentiDaElaborare = null;
		try {
			// recupera i documenti principali con barcode
			documentiDaElaborare = recogniformDAO.getBarcodeDaElaborare(idAoo, con);

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero dei documenti dalla coda recogniform per l'aoo " + idAoo, e);
		}

		return documentiDaElaborare;
	}

	/**
	 * @param con
	 * @return stati
	 */
	private Map<Integer, String> recuperaStati(final Connection con) {
		Map<Integer, String> stati = null;
		try {
			// recupera gli stati
			stati = recogniformDAO.getStati(con);

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero degli stati", e);
		}
		return stati;
	}

	private void valorizzaAllegati(final DocumentoRedFnDTO docRedFn, final List<DocumentoRecogniformDTO> allegatiDaElaborare, final int idAoo,
			final Map<Integer, String> stati) {
		final List<DocumentoRedFnDTO> allegatiRedFn = new ArrayList<>();
		DocumentoRedFnDTO allRedFn = null;
		for (final DocumentoRecogniformDTO allegatoDaElaborare : allegatiDaElaborare) {
			allRedFn = new DocumentoRedFnDTO();
			valorizzaDocumento(allRedFn, allegatoDaElaborare, idAoo, stati, true);
			allegatiRedFn.add(allRedFn);
		}
		docRedFn.setAllegati(allegatiRedFn);
	}

	private void valorizzaDocumento(final DocumentoRedFnDTO docRedFn, final DocumentoRecogniformDTO documentoDaElaborare, final int idAoo, final Map<Integer, String> stati,
			final boolean isAllegato) {
		PropertiesProvider pp = PropertiesProvider.getIstance();
		
		docRedFn.setClasseDocumentale(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CARTACEO_CLASSNAME_FN_METAKEY));
		if (!isAllegato) {
			docRedFn.setFolder(pp.getParameterByKey(PropertiesNameEnum.FOLDER_ACQUISITI_FN_METAKEY));
		}
		docRedFn.setDocumentTitle(String.valueOf(documentoDaElaborare.getIdRecogniform()));
		docRedFn.setDataHandler(documentoDaElaborare.getDataHandler());
		docRedFn.setContentType(documentoDaElaborare.getMimetype());

		final Map<String, Object> metadati = new HashMap<>();
		metadati.put(pp.getParameterByKey(PropertiesNameEnum.BARCODE_METAKEY), documentoDaElaborare.getBarcode());
		metadati.put(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), String.valueOf(documentoDaElaborare.getIdRecogniform()));
		String stato = stati.get(documentoDaElaborare.getStato());
		if (stato == null || documentoDaElaborare.getStato() != 5 || documentoDaElaborare.getStato() != 6) { // Lo scrive solo se è in errore
			stato = Constants.EMPTY_STRING;
		}
		metadati.put(pp.getParameterByKey(PropertiesNameEnum.STATO_RECOGNIFORM_METAKEY), stato);
		metadati.put(pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY), idAoo);
		if (!StringUtils.isEmpty(documentoDaElaborare.getBarcodeDocPrincipale())) {
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.BARCODE_PRINCIPALE_METAKEY), documentoDaElaborare.getBarcodeDocPrincipale());
		}
		docRedFn.setMetadati(metadati);

		if (!isAllegato) {
			final SecurityDTO security = securitySRV.creaSecurity((long) documentoDaElaborare.getIdnodo(), 0L, 0L);
			final ListSecurityDTO securities = new ListSecurityDTO(Arrays.asList(security));
			securities.setNullableUtente();
			docRedFn.setSecurity(securities);
		}
	}

	private void trasformazionePDF(final Document documentoPrincipaleFilenet, final List<String> idAllegatiDaTrasformare, final Integer idTipoAssegnazione,
			final UtenteDTO utente, final Connection con) {
		final Integer numeroProtocollo = (Integer) TrasformerCE.getMetadato(documentoPrincipaleFilenet, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
		final Integer annoProtocollo = (Integer) TrasformerCE.getMetadato(documentoPrincipaleFilenet, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
		final Date dataProtocollo = (Date) TrasformerCE.getMetadato(documentoPrincipaleFilenet, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY);

		if (numeroProtocollo != null && numeroProtocollo > 0 && annoProtocollo != null && annoProtocollo > 0 && dataProtocollo != null) {

			final String idDocumento = (String) TrasformerCE.getMetadato(documentoPrincipaleFilenet, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);

			// Impostazione dei parametri per il job di Trasformazione PDF
			final CodaTrasformazioneParametriDTO parametri = new CodaTrasformazioneParametriDTO();
			parametri.setIdDocumento(idDocumento);
			parametri.setIdNodo(utente.getIdUfficio());
			parametri.setIdUtente(utente.getId());
			parametri.setPrincipale(true);
			parametri.setIdCoda(null);
			parametri.setProtocollazioneP7M(false);
			parametri.setFlagFirmaCopiaConforme(false);
			parametri.setAggiornaFirmaPDF(false);
			parametri.setIdTipoAssegnazione(idTipoAssegnazione);

			if (!CollectionUtils.isEmpty(idAllegatiDaTrasformare)) {
				parametri.setAllegati(true);
				parametri.setIdAllegati(idAllegatiDaTrasformare);
			} else {
				parametri.setAllegati(false);
				parametri.setIdAllegati(null);
			}

			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
					&& "true".equals(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[avviaTrasformazionePDF] solo trasformazioni sincrone in regime mock");
				trasformazionePDFSRV.doTransformation(parametri);
			} else {
				trasformazionePDFSRV.insertSchedule(utente, parametri, this.getClass().getSimpleName(), con);
			}
		}
	}

	private static boolean checkCorrispondenzaAllegati(final List<DocumentoRecogniformDTO> allegatiDaElaborare, final List<AllegatoDTO> allegatiFilenetConBarcode,
			final Map<String, DocumentoRecogniformDTO> barcodeAllegatiMap, final String barcode) {

		if (allegatiDaElaborare.size() != allegatiFilenetConBarcode.size()) {
			LOGGER.warn("Il numero di allegati con barcode su Filenet non coincide con gli allegati censiti sul database per il documento con barcode: " + barcode);
			return false;
		} else {
			// Controllo anche se corrispondno i barcode degli allegati recogniform e del
			// documento
			for (final AllegatoDTO allegato : allegatiFilenetConBarcode) {

				final String barcodeAllegato = allegato.getBarcode();
				if (barcodeAllegatiMap.get(barcodeAllegato) != null) {
					LOGGER.warn("Il barcode " + barcodeAllegato + " di un allegato presente su Filenet non è stato trovato sulla base dati " + "per il documento con barcode: "
							+ barcode);
					return false;
				}
			}
		}

		return true;

	}

	private List<AllegatoDTO> getAllegatiConBarcode(final IFilenetCEHelper fceh, final String guidPrincipale, final UtenteDTO utente, final Connection con) {
		final DocumentSet allegatiFilenet = fceh.getAllegati(guidPrincipale, false, false);
		final List<AllegatoDTO> allegatiDTO = new ArrayList<>();

		if (!allegatiFilenet.isEmpty()) {
			final Iterator<?> it = allegatiFilenet.iterator();
			Document alleDocument = null;
			String barcodeAllegato = null;

			final Map<ContextTrasformerCEEnum, Object> context = tipologiaDocumentoSRV.getContextTipologieDocumento(CategoriaDocumentoEnum.ENTRATA.getIds()[0], utente, con);

			while (it.hasNext()) {
				alleDocument = (Document) it.next();
				barcodeAllegato = (String) TrasformerCE.getMetadato(alleDocument, PropertiesNameEnum.BARCODE_METAKEY);
				if (!StringUtils.isEmpty(barcodeAllegato)) {
					allegatiDTO.add(TrasformCE.transform(alleDocument, TrasformerCEEnum.FROM_DOCUMENT_TO_ALLEGATO_CONTEXT, context, con));
				}
			}
		}

		return allegatiDTO;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRecogniformFacadeSRV#
	 *      preparaDetailDocumentoInEntrataCartaceo
	 *      (it.ibm.red.business.dto.MasterDocCartaceoRedDTO, java.util.List,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public DetailDocumentRedDTO preparaDetailDocumentoInEntrataCartaceo(final MasterDocCartaceoRedDTO principaleCartaceo, final List<MasterDocCartaceoRedDTO> allegatiCartacei,
			final UtenteDTO utente) {
		final DetailDocumentRedDTO detailDocInEntrata = new DetailDocumentRedDTO(); // detail che sarà successivamente passato alla maschera di creazione
		IFilenetCEHelper fceh = null;
		Connection con = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			con = setupConnection(getDataSource().getConnection(), false);

			// Struttura dati del detail che dovrà contenere i GUID dei documenti cartacei
			final NodoDTO<String> guidDocumentiCartacei = new NodoDTO<>(principaleCartaceo.getGuid());

			// Si impostano gli eventuali allegati cartacei selezionati nel detail
			if (CollectionUtils.isNotEmpty(allegatiCartacei)) {
				final List<AllegatoDTO> allegati = new ArrayList<>();
				AllegatoDTO allegato = null;

				final List<TipologiaDocumentoDTO> tipologieDoc = tipologiaDocumentoSRV.getComboTipologieDocumentoAttive(utente.getIdAoo(), utente.getIdUfficio(),
						CategoriaDocumentoEnum.ENTRATA.getIds()[0], con);

				for (final MasterDocCartaceoRedDTO allegatoCartaceo : allegatiCartacei) {
					if (allegatoCartaceo.isSelectedAsAllegato()) {
						final Document allegatoFileNet = fceh.getDocumentByGuid(allegatoCartaceo.getGuid());

						allegato = new AllegatoDTO(allegatoCartaceo.getBarcode(), FilenetCEHelper.getDocumentContentAsByte(allegatoFileNet),
								(String) TrasformerCE.getMetadato(allegatoFileNet, PropertyNames.MIME_TYPE), tipologieDoc);

						// Si recuperano l'oggetto e il nome del file dalla tabella del DB popolata da
						// Recogniform
						final List<DocumentoRecogniformDTO> allegatoDBList = recogniformDAO.getDocumentoByBarcode(allegatoCartaceo.getBarcode(), con);
						if (CollectionUtils.isNotEmpty(allegatoDBList)) {
							final DocumentoRecogniformDTO allegatoDB = allegatoDBList.get(0); // Si considera solo il primo

							allegato.setOggetto(allegatoDB.getOggetto());
							allegato.setNomeFile(allegatoDB.getNomeFile() != null ? allegatoDB.getNomeFile() : allegatoDB.getIdRecogniform() + ".pdf");
						}

						guidDocumentiCartacei.addChild(allegatoCartaceo.getGuid());
						allegati.add(allegato);
					}
				}

				detailDocInEntrata.setAllegati(allegati);
			}

			// Si tratta di un documento da censire (in entrata)
			detailDocInEntrata.setIdCategoriaDocumento(CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0]);
			// Si impostano i dati propri del principale cartaceo nel detail
			detailDocInEntrata.setBarcode(principaleCartaceo.getBarcode());

			final Document principaleFileNet = fceh.getDocumentByGuid(principaleCartaceo.getGuid());
			detailDocInEntrata.setContent(FilenetCEHelper.getDocumentContentAsByte(principaleFileNet));
			detailDocInEntrata.setMimeType((String) TrasformerCE.getMetadato(principaleFileNet, PropertyNames.MIME_TYPE));

			// Si recuperano l'oggetto e il nome del file dalla tabella del DB popolata da
			// Recogniform
			final List<DocumentoRecogniformDTO> principaleDBList = recogniformDAO.getDocumentoByBarcode(principaleCartaceo.getBarcode(), con);
			if (CollectionUtils.isNotEmpty(principaleDBList)) {
				final DocumentoRecogniformDTO principaleDB = principaleDBList.get(0); // Si considera solo il primo

				detailDocInEntrata.setOggetto(principaleDB.getOggetto());
				detailDocInEntrata.setNomeFile(principaleDB.getNomeFile() != null ? principaleDB.getNomeFile() : principaleDB.getIdRecogniform() + ".pdf");
			}

			// Si inseriscono nel dettaglio i GUID dei documenti cartacei
			detailDocInEntrata.setGuidCartaceiAcquisiti(guidDocumentiCartacei);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la preparazione del documento cartaceo per il censimento in entrata", e);
			throw new RedException("Si è verificato un errore durante la preparazione del documento cartaceo per il censimento in entrata", e);
		} finally {
			popSubject(fceh);
			closeConnection(con);
		}

		return detailDocInEntrata;
	}
}