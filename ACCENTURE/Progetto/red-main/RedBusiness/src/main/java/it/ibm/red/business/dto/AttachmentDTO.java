package it.ibm.red.business.dto;

/**
 * The Class AttachmentDTO.
 *
 * @author CPIERASC
 * 
 * 	Classe utilizzata per modellare un allegato.
 */
public class AttachmentDTO extends FileDTO {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Flag firma richiesta.
	 */
	private Boolean needSign;
	
	/**
	 * Document title.
	 */
	private String documentTitle;
	
	/**
	 * Formato allegato.
	 */
	private Integer formatoAllegato;

	/**
	 * Costruttore.
	 * 
	 * @param inFileName		nome file
	 * @param inContent			content file
	 * @param inMimeType		mime type
	 * @param inDescription		descrizione
	 * @param inNeedSign		flag firma richiesta
	 * @param inDocumentTitle	docuiment title
	 */
	public AttachmentDTO(final String inFileName, final byte[] inContent, final String inMimeType, final String inDescription, 
			final Boolean inNeedSign, final String inDocumentTitle, final Integer inFormatoAllegato) {
		super(inFileName, inContent, inMimeType, inDescription);
		needSign = inNeedSign;
		documentTitle = inDocumentTitle; 
		formatoAllegato = inFormatoAllegato;
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param inFileName		nome file
	 * @param inContent			content file
	 * @param inMimeType		mime type
	 * @param inDescription		descrizione
	 * @param inNeedSign		flag firma richiesta
	 * @param inDocumentTitle	docuiment title
	 */
	public AttachmentDTO(final String inFileName, final byte[] inContent, final String inMimeType, final String inDescription, 
			final Boolean inNeedSign, final String inDocumentTitle) {
		this(inFileName, inContent, inMimeType, inDescription, inNeedSign, inDocumentTitle, null);
	}

	/**
	 * Getter.
	 * 
	 * @return	document title
	 */
	public final String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Getter.
	 * 
	 * @return	flag firma richiesta
	 */
	public final Boolean getNeedSign() {
		return needSign;
	}
	
	/**
	 * Restituisce il formato dell'allegato.
	 * @return formatoAllegato
	 */
	public Integer getFormatoAllegato() {
		return formatoAllegato;
	}

	/**
	 * Imposta il formato dell'allegato.
	 * @param inFormatoAllegato
	 */
	public void setFormatoAllegato(final Integer inFormatoAllegato) {
		this.formatoAllegato = inFormatoAllegato;
	}
}
