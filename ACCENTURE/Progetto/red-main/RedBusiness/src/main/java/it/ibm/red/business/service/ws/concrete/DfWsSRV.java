package it.ibm.red.business.service.ws.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.INpsConfigurationDAO;
import it.ibm.red.business.dto.AssCompetenzaDestNpsDfWSDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.MappingOrgNsdDTO;
import it.ibm.red.business.dto.ParamNpsDfWSDTO;
import it.ibm.red.business.dto.SecurityDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.UtenteProtocollatoreNpsDfWSDTO;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.NpsConfiguration;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.business.service.ISecuritySRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.service.concrete.AbstractService;
import it.ibm.red.business.service.ws.IDfWsSRV;
import it.ibm.red.business.service.ws.auth.DocumentServiceAuthorizable;
import it.ibm.red.business.utils.CollectionUtils;
import it.ibm.red.business.utils.DocumentoUtils;
import it.ibm.red.webservice.model.dfservice.types.messages.RedMappingOrganigrammaType;
import it.ibm.red.webservice.model.documentservice.dto.Servizio;

/**
 * The Class DfWsSRV.
 *
 * @author a.dilegge
 * 
 *         Servizio gestione servizi DF.
 */
@Service
public class DfWsSRV extends AbstractService implements IDfWsSRV {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -2370909204921499965L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DfWsSRV.class.getName());

	/**
	 * Stringa.
	 */
	private static final String TIPO_PROTOCOLLO = "ENTRATA";

	/**
	 * Dao.
	 */
	@Autowired
	private IAooDAO aooDAO;

	/**
	 * Service.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ISecuritySRV securitySRV;

	/**
	 * Dao.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private INpsConfigurationDAO npsConfigurationDAO;

	/**
	 * @see it.ibm.red.business.service.ws.facade.IDfWsFacadeSRV#mappingOrganigramma(it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.dfservice.types.messages.RedMappingOrganigrammaType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.DF_MAPPING_ORGANIGRAMMA)
	public List<MappingOrgNsdDTO> mappingOrganigramma(final RedWsClient client, final RedMappingOrganigrammaType request) {
		Connection connection = null;
		List<MappingOrgNsdDTO> listMappingOrgNsdDTO = new ArrayList<>();
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			listMappingOrgNsdDTO = aooDAO.getMappingOrgWsNsd(request.getCodAoo(), request.getIdUfficio(), request.getCodiceFiscale(), connection);

			if (!CollectionUtils.isEmptyOrNull(listMappingOrgNsdDTO)) {
				for (final MappingOrgNsdDTO mappingOrgNsdDTO : listMappingOrgNsdDTO) {
					// Recupero conf nps
					final NpsConfiguration npsConfUtente = npsConfigurationDAO.getByIdAoo(mappingOrgNsdDTO.getIdAooRed().intValue(), connection);
					mappingOrgNsdDTO.setParamNpsDTO(new ParamNpsDfWSDTO());
					mappingOrgNsdDTO.getParamNpsDTO().setConfigurazioneNps(npsConfUtente);

					// Calcolo assegnatario per competenza START
					final Nodo nodo = nodoDAO.getNodo(mappingOrgNsdDTO.getIdUfficioRed(), connection);
					final UfficioDTO uff = new UfficioDTO(mappingOrgNsdDTO.getIdUfficioRed(), nodo.getDescrizione());
					String descrAssegnatarioPerCompetenza = uff.getDescrizione();
					UtenteDTO ute = null;
					if (mappingOrgNsdDTO.getIdUtenteRed() != null) {
						ute = utenteSRV.getById(mappingOrgNsdDTO.getIdUtenteRed());
						descrAssegnatarioPerCompetenza += " - " + ute.getNome() + " " + ute.getCognome();
					}

					final AssCompetenzaDestNpsDfWSDTO assegnatarioOutput = calcolaAssegnatarioPerCompetenzaNps(ute, npsConfUtente, mappingOrgNsdDTO.getIdUtenteRed(), false);
					mappingOrgNsdDTO.getParamNpsDTO().setAssCompetenzaDestNpsDfWSDTO(assegnatarioOutput);

					final AssCompetenzaDestNpsDfWSDTO destinatarioOutput = calcolaAssegnatarioPerCompetenzaNps(ute, npsConfUtente, mappingOrgNsdDTO.getIdUtenteRed(), true);
					mappingOrgNsdDTO.getParamNpsDTO().setAssCompetenzaDestNpsDfWSDTO(destinatarioOutput);

					final AssegnazioneDTO assegnatarioPerCompetenza = new AssegnazioneDTO(null, TipoAssegnazioneEnum.COMPETENZA, null, null, descrAssegnatarioPerCompetenza,
							ute, uff);

					final List<AssegnazioneDTO> listAssegnatarioPerCompetenza = new ArrayList<>();
					listAssegnatarioPerCompetenza.add(assegnatarioPerCompetenza);
					// Calcolo assegnatario per competenza END

					// String assegnatario per competenza per output START
					final String[] assegnatarioPerCompetenzaArray = DocumentoUtils.initArrayAssegnazioni(TipoAssegnazioneEnum.COMPETENZA, listAssegnatarioPerCompetenza);
					final String assegnatarioPerCompetenzaString = assegnatarioPerCompetenzaArray[0];
					// String assegnatario per competenza per output END

					// Protocollatore START
					mappingOrgNsdDTO.getParamNpsDTO().setUtenteProtocollatoreDTO(calcolaProtocollatoreNps(npsConfUtente, ute));
					// Protocollatore END

					// Tipo Protocollo
					mappingOrgNsdDTO.getParamNpsDTO().setTipoProtocollo(TIPO_PROTOCOLLO);

					// Recupero Acl START
					mappingOrgNsdDTO.getParamNpsDTO().setAcl(calcolaAcl(assegnatarioPerCompetenzaString, ute, connection));
					// Recupero Acl END

					mappingOrgNsdDTO.getParamNpsDTO().setNomeMittente(ute.getNome());
					mappingOrgNsdDTO.getParamNpsDTO().setCognomeMittente(ute.getCognome());
					mappingOrgNsdDTO.getParamNpsDTO().setCfMittente(ute.getCodFiscale());
					// Mittente END

				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero del mapping nsd.", e);
		} finally {
			closeConnection(connection);

		}
		return listMappingOrgNsdDTO;
	}

	private static AssCompetenzaDestNpsDfWSDTO calcolaAssegnatarioPerCompetenzaNps(final UtenteDTO utenteDTO, final NpsConfiguration npsConf, final Long idUtente,
			final boolean isDestinatario) {

		final AssCompetenzaDestNpsDfWSDTO assegnatarioPerCompetenza = new AssCompetenzaDestNpsDfWSDTO();
		
		if (idUtente > 0) {
			assegnatarioPerCompetenza.setCodiceAmm(npsConf.getCodiceAmministrazione());
			assegnatarioPerCompetenza.setDenominazioneAmm(npsConf.getDenominazioneAmministrazione());
			assegnatarioPerCompetenza.setCodiceAoo(npsConf.getCodiceAoo());
			assegnatarioPerCompetenza.setDenominazioneAoo(npsConf.getDenominazioneAoo());
			assegnatarioPerCompetenza.setCodUO(String.valueOf(utenteDTO.getIdUfficio()));
			assegnatarioPerCompetenza.setDenominazioneUO((utenteDTO.getNodoDesc()));
			assegnatarioPerCompetenza.setChiaveEsternaOperatore(String.valueOf(utenteDTO.getId()));
			assegnatarioPerCompetenza.setNome(utenteDTO.getNome());
			assegnatarioPerCompetenza.setCognome(utenteDTO.getCognome());

			if (isDestinatario) {
				assegnatarioPerCompetenza.setCodFiscale(utenteDTO.getCodFiscale());
			}
		} else {
			// Ufficio
			assegnatarioPerCompetenza.setCodiceAmm(npsConf.getCodiceAmministrazione());
			assegnatarioPerCompetenza.setDenominazioneAmm(npsConf.getDenominazioneAmministrazione());
			assegnatarioPerCompetenza.setCodiceAoo(npsConf.getCodiceAoo());
			assegnatarioPerCompetenza.setDenominazioneAoo(npsConf.getDenominazioneAoo());
			assegnatarioPerCompetenza.setCodUO(String.valueOf(utenteDTO.getIdUfficio()));
			assegnatarioPerCompetenza.setDenominazioneUO((utenteDTO.getNodoDesc()));
		}
		return assegnatarioPerCompetenza;
	}

	private static UtenteProtocollatoreNpsDfWSDTO calcolaProtocollatoreNps(final NpsConfiguration configurazioneNps, final UtenteDTO utente) {
		final UtenteProtocollatoreNpsDfWSDTO utenteProtocollatore = new UtenteProtocollatoreNpsDfWSDTO();
		utenteProtocollatore.setCodiceAmministrazione(configurazioneNps.getCodiceAmministrazione());
		utenteProtocollatore.setDenominazioneAmministrazione(configurazioneNps.getDenominazioneAmministrazione());
		utenteProtocollatore.setCodAoo(configurazioneNps.getCodiceAoo());
		utenteProtocollatore.setDenominazioneAoo(configurazioneNps.getDenominazioneAoo());
		utenteProtocollatore.setCodUO(utente.getUfficioDesc());
		utenteProtocollatore.setDenominazioneUO(utente.getUfficioDesc());
		utenteProtocollatore.setNome(utente.getNome());
		utenteProtocollatore.setCognome(utente.getCognome());
		utenteProtocollatore.setChiaveEsternaOperatore(String.valueOf(utente.getId()));
		return utenteProtocollatore;
	}

	private String calcolaAcl(final String assegnatarioPerCompetenza, final UtenteDTO utenteDTO, final Connection conn) {
		IFilenetCEHelper fcehAdmin = null;
		try {
			fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utenteDTO.getFcDTO()), utenteDTO.getIdAoo());
			final List<SecurityDTO> listSecurity = securitySRV.getSecurityAssegnazionePerWS(assegnatarioPerCompetenza, true, false, false, conn, utenteDTO);
			return securitySRV.getAclFromSecurity(listSecurity, fcehAdmin);
		} catch (final Exception ex) {
			LOGGER.error("Errore durante il calcolo delle acl per il servizio del df : " + ex);
			throw new RedException("Errore durante il calcolo delle acl per il servizio del df : " + ex);
		} finally {
			popSubject(fcehAdmin);
		}
	}

}