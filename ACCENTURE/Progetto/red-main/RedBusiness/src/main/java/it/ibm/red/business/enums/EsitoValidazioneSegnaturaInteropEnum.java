package it.ibm.red.business.enums;

/**
 * @author m.crescentini
 *
 */
public enum EsitoValidazioneSegnaturaInteropEnum {

	/**
	 * Valore.
	 */
	ASSENTE("SEGNATURA_ASSENTE"),
	
	/**
	 * Valore.
	 */
	VALIDA("SEGNATURA_VALIDA"),
	
	/**
	 * Valore.
	 */
	NON_CONFORME("SEGNATURA_NON_CONFORME"),
	
	/**
	 * Valore.
	 */
	ERRATA("SEGNATURA_ERRATA");
	
	/**
	 * Nome.
	 */	
	private String nome;
	
	
	EsitoValidazioneSegnaturaInteropEnum(final String nome) {
		this.nome = nome;
	}

	/**
	 * Ottiene l'esito della validazione della segnatura interoperabilità.
	 * @param nome
	 * @return esito della validazione della segnatura interoperabilità
	 */
	public static EsitoValidazioneSegnaturaInteropEnum get(final String nome) {
		EsitoValidazioneSegnaturaInteropEnum output = null;
		
		for (final EsitoValidazioneSegnaturaInteropEnum esitoValidazioneInterop : EsitoValidazioneSegnaturaInteropEnum.values()) {
			if (esitoValidazioneInterop.getNome().equalsIgnoreCase(nome)) {
				output = esitoValidazioneInterop;
				break;
			}
		}
		
		return output;
	}
	
	/**
	 * Recupera il nome.
	 * @return nome
	 */
	public String getNome() {
		return nome;
	}
	
}
