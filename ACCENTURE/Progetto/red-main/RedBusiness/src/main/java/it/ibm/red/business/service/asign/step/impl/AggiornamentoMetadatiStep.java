package it.ibm.red.business.service.asign.step.impl;

import java.sql.Connection;
import java.util.Map;

import org.springframework.stereotype.Service;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.ASignStepResultDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.asign.step.ASignStepAbstract;
import it.ibm.red.business.service.asign.step.ContextASignEnum;
import it.ibm.red.business.service.asign.step.StepEnum;

/**
 * Step di aggiornamento metadati - firma asincrona.
 */
@Service
public class AggiornamentoMetadatiStep extends ASignStepAbstract {

	/**
	 * Serial version.
	 */
	private static final long serialVersionUID = -7553486970039206304L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AggiornamentoMetadatiStep.class.getName());

	/**
	 * Esecuzione dell'aggiornamento metadati.
	 * @param con
	 * @param item
	 * @param context
	 * @return risultato dell'esecuzione dello step.
	 */
	@Override
	protected ASignStepResultDTO run(Connection con, ASignItemDTO item, Map<ContextASignEnum, Object> context) {
		Long idItem = item.getId();
		LOGGER.info("run -> START [ID item: " + idItem + "]");
		ASignStepResultDTO out = new ASignStepResultDTO(item.getId(), getStep());
		
		// Si recupera l'utente firmatario
		UtenteDTO utenteFirmatario = getUtenteFirmatario(item, con);
		
		// Si recupera il workflow
		VWWorkObject wob = getWorkflow(item, out, utenteFirmatario);
		
		if (wob != null) {
			// ### AGGIORNAMENTO DEI METADATI ###
			EsitoOperazioneDTO esitoAggiornamentoMetadati = signSRV.aggiornamentoMetadati(wob, utenteFirmatario, con);
			
			if (esitoAggiornamentoMetadati.isEsito()) {
				out.setStatus(true);
				out.appendMessage("[OK] Aggiornamento dei metadati eseguito");
			} else {
				throw new RedException("[KO] Errore nello step di aggiornamento dei metadati: " + esitoAggiornamentoMetadati.getNote() 
					+ ". [Codice errore: " + esitoAggiornamentoMetadati.getCodiceErrore() + "]");
			}
		}
		
		LOGGER.info("run -> END [ID item: " + idItem + "]");
		return out;
	}

	/**
	 * Restituisce lo step a cui è associata questa classe.
	 * @return StepEnum
	 */
	@Override
	protected StepEnum getStep() {
		return StepEnum.AGGIORNAMENTO_METADATI;
	}

	/**
	 * @see it.ibm.red.business.service.asign.step.ASignStepAbstract#getErrorMessage().
	 */
	@Override
	protected String getErrorMessage() {
		return "[KO] Errore imprevisto nello step di aggiornamento dei metadati";
	}

}
