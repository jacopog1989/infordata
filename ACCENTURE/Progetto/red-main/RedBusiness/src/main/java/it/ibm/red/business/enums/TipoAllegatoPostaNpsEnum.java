package it.ibm.red.business.enums;

/**
 * @author m.crescentini
 */
public enum TipoAllegatoPostaNpsEnum {

	/**
	 * Valore.
	 */
	ALLEGATO_GENERICO,

	/**
	 * Valore.
	 */
	DOCUMENTO_PRINCIPALE,

	/**
	 * Valore.
	 */
	POSTACERT,

	/**
	 * Valore.
	 */
	DATICERT,

	/**
	 * Valore.
	 */
	INTEROPERABILITA,

	/**
	 * Valore.
	 */
	DATI_FLUSSO;

	/**
	 * Restituisce il valore dell'enum corrispondente al nome in input.
	 * @param nome
	 * @return tipoAllegatoPostaNps
	 */
	public static TipoAllegatoPostaNpsEnum get(final String nome) {
		TipoAllegatoPostaNpsEnum output = null;

		for (final TipoAllegatoPostaNpsEnum tipoAllegatoInterop : TipoAllegatoPostaNpsEnum.values()) {
			if (tipoAllegatoInterop.name().equalsIgnoreCase(nome)) {
				output = tipoAllegatoInterop;
				break;
			}
		}
		return output;
	}
}
