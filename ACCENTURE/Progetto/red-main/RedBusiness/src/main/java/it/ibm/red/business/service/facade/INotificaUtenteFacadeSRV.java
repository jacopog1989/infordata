package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.NotificheUtenteDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * The Interface INotificaUtenteFacadeSRV.
 *
 * @author m.crescentini
 * 
 * 		Servizi per la gestione delle notifiche utente
 * 		(pannello con notifiche sottoscrizioni, calendario e rubrica).
 */
public interface INotificaUtenteFacadeSRV extends Serializable {

	/**
	 * Contrassegna la notifica sul calendario.
	 * @param idUtente
	 * @param idAoo
	 * @param idEvento
	 * @param statoNotifica
	 * @return true o false
	 */
	boolean contrassegnaNotificaCalendario(Long idUtente, Long idAoo, int idEvento, int statoNotifica);

	/**
	 * Elimina la notifica dal calendario.
	 * @param idUtente
	 * @param idAoo
	 * @param idEvento
	 * @param statoNotifica
	 * @return true o false
	 */
	boolean eliminaNotificaCalendario(Long idUtente, Long idAoo, int idEvento, int statoNotifica);

	/**
	 * Contrassegna la notifica in scadenza sul calendario.
	 * @param idUtente
	 * @param idAoo
	 * @param docTitle
	 * @param statoNotifica
	 * @return true o false
	 */
	boolean contrassegnaNotificaCalendarioScadenza(Long idUtente, Long idAoo, String docTitle, int statoNotifica);

	/**
	 * Elimina la notifica in scadenza dal calendario.
	 * @param idUtente
	 * @param idAoo
	 * @param docTitle
	 * @param statoNotifica
	 * @return true o false
	 */
	boolean eliminaNotificaCalendarioScadenza(Long idUtente, Long idAoo, String docTitle, int statoNotifica);

	/**
	 * Ottiene tutte le notifiche relative all'utente.
	 * @param utente
	 * @return lista delle notifiche dell'utente
	 */
	List<NotificheUtenteDTO> getAll(UtenteDTO utente);

	/**
	 * Conta il numero delle notifiche non lette.
	 * @return numero delle notifiche
	 */
	Integer countNotificheNonLette();

	/**
	 * Contrassegna la notifica sulla rubrica.
	 * @param idUtente
	 * @param idAoo
	 * @param idNotificaRubrica
	 * @param statoNotifica
	 * @return true o false
	 */
	boolean contrassegnaNotificaRubrica(Long idUtente, Long idAoo, Integer idNotificaRubrica, int statoNotifica);

	/**
	 * Elimina la notifica dalla rubrica.
	 * @param idUtente
	 * @param idAoo
	 * @param idNotificaRubrica
	 * @param statoNotifica
	 * @return true o false
	 */
	boolean eliminaNotificaRubrica(Long idUtente, Long idAoo, Integer idNotificaRubrica, int statoNotifica);
 
	/**
	 * Conta il numero delle notifiche non lette per un dato utente.
	 * @param utente
	 * @return numero delle notifiche
	 */
	Integer contaNotificheNonLette(UtenteDTO utente);
	
}
