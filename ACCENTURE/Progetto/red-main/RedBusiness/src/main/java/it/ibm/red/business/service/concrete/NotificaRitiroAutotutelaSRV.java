/**
 * 
 */
package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dto.EventoSottoscrizioneDTO;
import it.ibm.red.business.dto.NotificaRitiroAutotutelaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.service.INotificaRitiroAutotutelaSRV;
import it.ibm.red.business.service.ISottoscrizioniSRV;

/**
 * @author APerquoti
 *
 */
@Service
public class NotificaRitiroAutotutelaSRV extends NotificaWriteAbstractSRV implements INotificaRitiroAutotutelaSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 7982117638638744362L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NotificaRitiroAutotutelaSRV.class.getName());
	
	/**
	 * Service.
	 */
	@Autowired
	private ISottoscrizioniSRV sottoscrizioniSRV;

	/**
	 * @see it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV#getSottoscrizioni(int).
	 */
	@Override
	protected List<EventoSottoscrizioneDTO> getSottoscrizioni(final int idAoo) {
		Connection con = null;
		List<EventoSottoscrizioneDTO> eventi = null;
		
		try {
			final Integer[] idEventi = new Integer[]{ Integer.parseInt(EventTypeEnum.RICEZIONE_RITIRO_IN_AUTOTUTELA.getValue()) };
		
			con = setupConnection(getFilenetDataSource().getConnection(), false);
			eventi = sottoscrizioniSRV.getEventiSottoscrittiByListaEventi(idAoo, idEventi, con);			
		
		} catch (final Exception e) {
			LOGGER.error("Errore in fase recupero delle sottoscrizioni per l'evento per l'aoo " + idAoo, e);
		} finally {
			closeConnection(con);
		}
		
		return eventi;
	}

	/**
	 * @see it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV#getDataDocumenti(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.persistence.model.Aoo, int).
	 */
	@Override
	protected List<InfoDocumenti> getDataDocumenti(final UtenteDTO utente, final Aoo aoo, final int idEvento) {
		throw new RedException("Metodo non previsto per il servizio ");
	}

	/**
	 * @see it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV#getDataDocumenti(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.persistence.model.Aoo, int, java.lang.Object).
	 */
	@Override
	protected List<InfoDocumenti> getDataDocumenti(final UtenteDTO utente, final Aoo aoo, final int idEvento, final Object objectForGetDataDocument) {
		final List<InfoDocumenti> documenti = new ArrayList<>();
		
 		try {
 			
 			final NotificaRitiroAutotutelaDTO notifica = (NotificaRitiroAutotutelaDTO) objectForGetDataDocument;
			
 			if (utente.getId() != null && utente.getId().longValue() == notifica.getIdUtente() 
 					&& utente.getIdUfficio() != null && utente.getIdUfficio().longValue() == notifica.getIdNodo()) {
	 			final InfoDocumenti info = new InfoDocumenti();
	 			info.setIdDocumento(Integer.valueOf((int) notifica.getIdDocumento()));
	 			documenti.add(info);
 			}
 			
 		} catch (final Exception e) {
			LOGGER.error("Errore in fase recupero info timer", e);
		}
 		
		return documenti;
	}

}
