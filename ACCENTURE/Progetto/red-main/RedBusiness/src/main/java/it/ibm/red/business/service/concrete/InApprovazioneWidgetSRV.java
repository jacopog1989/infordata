package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IInApprovazioneWidgetDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.service.IInApprovazioneWidgetSRV;

/**
 * Service approvazione widget.
 */
@Service
@Component
public class InApprovazioneWidgetSRV extends AbstractService implements IInApprovazioneWidgetSRV  {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -6970821894113312180L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(InApprovazioneWidgetSRV.class.getName());
	
	/**
	 * DAO.
	 */
	@Autowired
	private IInApprovazioneWidgetDAO nodoUfficioDAO;
	
	/**
	 * DAO.
	 */
	@Autowired
	private INodoDAO nodoDAO;
	
	/**
	 * @see it.ibm.red.business.service.facade.IInApprovazioneWidgetFacadeSRV#getNodiFromIdUtenteandIdAOO(it.ibm.red.business.dto.UtenteDTO,
	 *      boolean).
	 */
	@Override
	public List<Nodo> getNodiFromIdUtenteandIdAOO(final UtenteDTO utente, final boolean inApprovazioneDirigente) {
		Connection connection = null;
		final List<Nodo> listaNodi = new ArrayList<>();
		List<Long> idNodi = new ArrayList<>();
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			if (inApprovazioneDirigente) {
				idNodi = nodoUfficioDAO.getIdNodiFromIdUtenteandIdAOO(utente.getId(), utente.getIdAoo(), inApprovazioneDirigente, connection);
			} else {
				idNodi = nodoUfficioDAO.getIdNodiFromIdUtenteandIdAOO(utente.getIdUfficio(), utente.getIdAoo(), inApprovazioneDirigente, connection);
				//AGGIUNGO ANCHE L'ID PADRE , CIOE' L'UFFICIO DELL'UTENTE
				idNodi.add(0, utente.getIdUfficio());
			}
			
			for (final Long nodoId : idNodi) {
				final Nodo nodo = nodoDAO.getNodo(nodoId, connection);
				if (nodo.getDataDisattivazione() == null) {
					listaNodi.add(nodo);
				}
			} 
			return listaNodi;
		} catch (final Exception e) {
			rollbackConnection(connection); 
			LOGGER.error(e);
			throw new RedException("Errore durante l'estrazione", e);
		} finally {
			closeConnection(connection);
		} 
	}
	
}
