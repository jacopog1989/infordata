package it.ibm.red.business.service.sign.strategy;

import java.util.Collection;
import java.util.EnumMap;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.ProtocolloEmergenzaDocDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.SignStrategyEnum;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;

/**
 * Factory delle strategy di firma.
 */
public class SignStrategyFactory {

	/**
	 * Errore generico sulla strategia di firma.
	 */
	private static final String GENERIC_ERROR_STRATEGY = "Configurazione strategia di firma non corretta.";

	/**
	 * LOGGER.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SignStrategyFactory.class.getName());

	/**
	 * Lista delle strategies.
	 */
	static EnumMap<SignStrategyEnum, Class<? extends ISignStrategy>> strategies;
	
	static {
		strategies = new EnumMap<>(SignStrategyEnum.class);
		strategies.put(SignStrategyEnum.ASYNC_STRATEGY_A, AAsyncSignStrategy.class);
		strategies.put(SignStrategyEnum.ASYNC_STRATEGY_B, BAsyncSignStrategy.class);
	}
	
	/**
	 * Private constructor.
	 */
	private SignStrategyFactory() {
		
	}
	
	/**
	 * @param signMode
	 * @param isUCB
	 * @return
	 */
	private static ISignStrategy create(SignStrategyEnum signMode, boolean isUCB) {
		ISignStrategy out = null;
		
		try {
			if (signMode != null) {
				Class<? extends ISignStrategy> cls = strategies.get(signMode);
				
				// Se l'AOO è un UCB si deve forzare la strategy 'A',
				// in caso contrario verrebbe firmato il content con i placeholder della registrazione ausiliaria
				if (isUCB) {
					cls = AAsyncSignStrategy.class;
				}
				
				out = ApplicationContextProvider.getApplicationContext().getBean(cls);
			}
		} catch (Exception e) {
			LOGGER.error("Impossibile recuperare la strategia di firma.", e);
			throw new RedException(e);
		}
		
		return out;
	}

	
	/**
	 * FIRMA REMOTA.
	 * 
	 * @param signMode
	 * @param wobNumbers
	 * @param pin
	 * @param otp
	 * @param utenteFirmatario
	 * @param signType
	 * @param principaleOnlyPAdESVisible
	 * @param firmaMultipla
	 * @return
	 */
	public static Collection<EsitoOperazioneDTO> firmaRemota(String signTransactionId, SignStrategyEnum signMode, Collection<String> wobNumbers, String pin, String otp, 
			UtenteDTO utenteFirmatario, SignTypeEnum signType, boolean principaleOnlyPAdESVisible, boolean firmaMultipla) {
		
		ISignStrategy strategy = SignStrategyFactory.create(signMode, utenteFirmatario.isUcb());
		if (strategy != null) {
			return strategy.firmaRemota(signTransactionId, wobNumbers, pin, otp, utenteFirmatario, signType, principaleOnlyPAdESVisible, firmaMultipla);
		} else {
			throw new RedException(GENERIC_ERROR_STRATEGY);
		}
	}
	
	
	/**
	 * FIRMA AUTOGRAFA.
	 * 
	 * @param signMode
	 * @param wobNumbers
	 * @param utenteFirmatario
	 * @param protocolloEmergenza
	 * @return
	 */
	public static Collection<EsitoOperazioneDTO> firmaAutografa(String signTransactionId, SignStrategyEnum signMode, Collection<String> wobNumbers, UtenteDTO utenteFirmatario,
			ProtocolloEmergenzaDocDTO protocolloEmergenza) {
		
		ISignStrategy strategy = SignStrategyFactory.create(signMode, utenteFirmatario.isUcb());
		if (strategy != null) {
			return strategy.firmaAutografa(signTransactionId, wobNumbers, utenteFirmatario, protocolloEmergenza);
		} else {
			throw new RedException(GENERIC_ERROR_STRATEGY);
		}
	}
	
	
	/**
	 * PREPARAZIONE ALLA FIRMA LOCALE.
	 * 
	 * @param signTransactionId
	 * @param signMode
	 * @param wobNumber
	 * @param utenteFirmatario
	 * @param signType
	 * @param firmaMultipla
	 * @return
	 */
	public static EsitoOperazioneDTO preFirmaLocale(String signTransactionId, SignStrategyEnum signMode, String wobNumber, UtenteDTO utenteFirmatario,  SignTypeEnum signType, boolean firmaMultipla) {
		ISignStrategy strategy = SignStrategyFactory.create(signMode, utenteFirmatario.isUcb());
		if (strategy != null) {
			return strategy.preFirmaLocale(signTransactionId, wobNumber, utenteFirmatario, signType, firmaMultipla);
		} else {
			throw new RedException(GENERIC_ERROR_STRATEGY);
		}
	}
	
	
	/**
	 * POST FIRMA LOCALE.
	 * 
	 * @param signMode
	 * @param signOutcome
	 * @param utenteFirmatario
	 * @param signType
	 * @param copiaConforme
	 * @return
	 */
	public static void postFirmaLocale(String signTransactionId, SignStrategyEnum signMode, EsitoOperazioneDTO signOutcome, UtenteDTO utenteFirmatario, SignTypeEnum signType, boolean copiaConforme) {
		ISignStrategy strategy = SignStrategyFactory.create(signMode, utenteFirmatario.isUcb());
		if (strategy != null) {
			strategy.postFirmaLocale(signTransactionId, signOutcome, utenteFirmatario, signType, copiaConforme);
		} else {
			throw new RedException(GENERIC_ERROR_STRATEGY);
		}
	}
	
}
