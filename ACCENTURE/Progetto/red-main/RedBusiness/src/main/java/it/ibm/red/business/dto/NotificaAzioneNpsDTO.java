package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.Date;

import it.ibm.red.business.enums.StatoElabNotificaNpsEnum;

/**
 * The Class NotificaAzioneNpsDTO.
 *
 * @author a.dilegge
 * 
 * 	DTO per le notifiche azioni di NPS.
 */
public class NotificaAzioneNpsDTO extends NotificaNpsDTO implements Serializable {

	/** 
	 * The Constant serialVersionUID. 
	 */
	private static final long serialVersionUID = 6773771587816217282L;

	/** 
	 * Dati azione 
	 */

	private transient Object datiAzione;
	
	
	/**
	 * Costruttore notifica azione nps DTO.
	 *
	 * @param messageIdNPS the message id NPS
	 * @param idMessaggioNPS the id messaggio NPS
	 * @param codiceAooNPS the codice aoo NPS
	 * @param dataProtocollo the data protocollo
	 * @param numeroProtocollo the numero protocollo
	 * @param annoProtocollo the anno protocollo
	 * @param idProtocollo the id protocollo
	 * @param tipoProtocollo the tipo protocollo
	 * @param dataNotifica the data notifica
	 * @param tipoNotifica the tipo notifica
	 * @param datiAzione the dati azione
	 */
	public NotificaAzioneNpsDTO(final String messageIdNPS, final String idMessaggioNPS, final String codiceAooNPS, final Date dataProtocollo, 
			final Integer numeroProtocollo, final Integer annoProtocollo, final String idProtocollo, final int tipoProtocollo, final Date dataNotifica, 
			final String tipoNotifica, final Object datiAzione) {
		super(messageIdNPS, idMessaggioNPS, codiceAooNPS, dataProtocollo, numeroProtocollo, annoProtocollo, 
				idProtocollo, tipoProtocollo, dataNotifica, tipoNotifica);
		this.datiAzione = datiAzione;
	}
	
	/**
	 * Costruttore notifica azione nps DTO.
	 *
	 * @param idCoda
	 *            the id coda
	 * @param messageIdNPS
	 *            the message id NPS
	 * @param codiceAooNPS
	 *            the codice aoo NPS
	 * @param dataProtocollo
	 *            the data protocollo
	 * @param numeroProtocollo
	 *            the numero protocollo
	 * @param annoProtocollo
	 *            the anno protocollo
	 * @param idProtocollo
	 *            the id protocollo
	 * @param tipoProtocollo
	 *            the tipo protocollo
	 * @param dataNotifica
	 *            the data notifica
	 * @param tipoNotifica
	 *            the tipo notifica
	 * @param idAoo
	 *            the id aoo
	 * @param dataRicezioneNotifica
	 *            the data ricezione notifica
	 * @param stato
	 *            the stato
	 * @param errore
	 *            the errore
	 * @param datiAzione
	 *            the dati azione
	 */
	public NotificaAzioneNpsDTO(final Long idCoda, final String messageIdNPS, final String codiceAooNPS, final Date dataProtocollo, final int numeroProtocollo, 
			final int annoProtocollo, final String idProtocollo, final int tipoProtocollo, final Date dataNotifica, final String tipoNotifica, final Long idAoo, 
			final Date dataRicezioneNotifica, final StatoElabNotificaNpsEnum stato, final String errore, final Object datiAzione) {
		super(idCoda, messageIdNPS, null, codiceAooNPS, dataProtocollo, numeroProtocollo, annoProtocollo, 
				idProtocollo, tipoProtocollo, dataNotifica, tipoNotifica, idAoo, dataRicezioneNotifica, stato, errore);
		this.datiAzione = datiAzione;
	}

	/** 
	 *
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/** 
	 *
	 * @return the datiAzione
	 */
	public Object getDatiAzione() {
		return datiAzione;
	}
	
}