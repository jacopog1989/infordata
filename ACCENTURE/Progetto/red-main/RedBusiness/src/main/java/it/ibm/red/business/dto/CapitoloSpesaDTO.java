package it.ibm.red.business.dto;

/**
 * DTO che definisce un capitolo di spesa.
 */
public class CapitoloSpesaDTO extends AbstractDTO {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -4108369702099186944L;

	/**
	 * Codice.
	 */
	private String codice;
	
	/**
	 * Descrizione.
	 */
	private String descrizione;
	
	/**
	 * Anno.
	 */
	private Integer anno;
	
	/**
	 * Flag sentenza.
	 */
	private Boolean flagSentenza;
	
	/**
	 * Ufficio competente.
	 */
	private String ufficioCompetente;
	
	/**
	 * Flag riservato.
	 */
	private boolean flagRiservato;
	
	/**
	 * Costruttore di default.
	 */
	public CapitoloSpesaDTO() {
		super();
	}
	

	/**
	 * @param codice
	 */
	public CapitoloSpesaDTO(final String codice) {
		super();
		this.codice = codice;
	}


	/**
	 * @param codice
	 * @param descrizione
	 * @param flagSentenza
	 */
	public CapitoloSpesaDTO(final String codice, final String descrizione, final Integer anno, final Boolean flagSentenza) {
		super();
		this.codice = codice;
		this.descrizione = descrizione;
		this.anno = anno;
		this.flagSentenza = flagSentenza;
	}

	/**
	 * @return the codice
	 */
	public String getCodice() {
		return codice;
	}

	/**
	 * @param codice the codice to set
	 */
	public void setCodice(final String codice) {
		this.codice = codice;
	}

	/**
	 * @return the descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * @param descrizione the descrizione to set
	 */
	public void setDescrizione(final String descrizione) {
		this.descrizione = descrizione;
	}

	/**
	 * @return the anno
	 */
	public Integer getAnno() {
		return anno;
	}

	/**
	 * @param anno the anno to set
	 */
	public void setAnno(final Integer anno) {
		this.anno = anno;
	}

	/**
	 * @return the flagSentenza
	 */
	public Boolean getFlagSentenza() {
		return flagSentenza;
	}

	/**
	 * @param flagSentenza the flagSentenza to set
	 */
	public void setFlagSentenza(final Boolean flagSentenza) {
		this.flagSentenza = flagSentenza;
	}

	/**
	 * Restituisce l'ufficio competente del capitolo spesa.
	 * @return ufficioCompetente
	 */
	public String getUfficioCompetente() {
		return ufficioCompetente;
	}

	/**
	 * Imposta l'ufficio competente legato al capitolo spesa.
	 * @param ufficioCompetente
	 */
	public void setUfficioCompetente(final String ufficioCompetente) {
		this.ufficioCompetente = ufficioCompetente;
	}

	/**
	 * Indica se il capitolo di spesa risulta riservato.
	 * @return flagRiservato
	 */
	public boolean isFlagRiservato() {
		return flagRiservato;
	}

	/**
	 * Imposta il flagRiservato.
	 * 
	 * @param flagRiservato
	 */
	public void setFlagRiservato(boolean flagRiservato) {
		this.flagRiservato = flagRiservato;
	}
	
	
}
