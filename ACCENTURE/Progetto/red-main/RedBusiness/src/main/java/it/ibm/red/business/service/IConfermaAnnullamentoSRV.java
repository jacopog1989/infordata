/**
 * 
 */
package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IConfermaAnnullamentoFacadeSRV;

/**
 * @author a.dilegge
 *
 */
public interface IConfermaAnnullamentoSRV extends IConfermaAnnullamentoFacadeSRV {

	
	
}