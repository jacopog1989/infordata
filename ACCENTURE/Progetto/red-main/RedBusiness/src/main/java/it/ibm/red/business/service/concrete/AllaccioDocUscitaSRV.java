/**
 * 
 */
package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.AccessPermissionList;
import com.filenet.api.constants.SecurityPrincipalType;
import com.filenet.api.core.Document;
import com.filenet.api.security.AccessPermission;

import it.ibm.red.business.dao.IAllaccioDocUscitaDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.AggiornaSecurityNpsDTO;
import it.ibm.red.business.dto.AllaccioDocUscitaDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.SecurityDTO;
import it.ibm.red.business.dto.SecurityDTO.GruppoS;
import it.ibm.red.business.dto.SecurityDTO.UtenteS;
import it.ibm.red.business.dto.TracciaDocUscitaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.AllaccioDocUscitaEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAllaccioDocUscitaSRV;
import it.ibm.red.business.service.ISecuritySRV;
import it.ibm.red.business.service.ITracciaDocumentiSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * @author VINGENITO
 *
 */
@Service
public class AllaccioDocUscitaSRV extends AbstractService implements IAllaccioDocUscitaSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -3294742175473492718L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AllaccioDocUscitaSRV.class.getName());

	/**
	 * Dao allaccio uscita.
	 */
	@Autowired
	private IAllaccioDocUscitaDAO allaccioUscitaDAO;

	/**
	 * Servizio sicurezza.
	 */
	@Autowired
	private ISecuritySRV securitySRV;

	/**
	 * Dao nodo.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * Dao utente.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;

	/**
	 * Servizio tracciamento documento.
	 */
	@Autowired
	private ITracciaDocumentiSRV tracciaDocumentoSRV;

	/**
	 * Servizio utente.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;

	/**
	 * Estende la visibilità del documento <code> allaccioDocUscitaDTO </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAllacciaDocUscitaFacadeSRV#estendiVisibilitaDocumentiRisposta(it.ibm.red.business.dto.AllaccioDocUscitaDTO)
	 */
	@Override
	public void estendiVisibilitaDocumentiRisposta(final AllaccioDocUscitaDTO allaccioDocUscitaDTO) {
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), true);
			if (allaccioDocUscitaDTO.getDocTitleIngressoList() != null) {
				for (int i = 0; i < allaccioDocUscitaDTO.getDocTitleIngressoList().size(); i++) {
					allaccioUscitaDAO.insertDocEstendiVisibilita(allaccioDocUscitaDTO.getDocTitleUscita(), allaccioDocUscitaDTO.getDocTitleIngressoList().get(i),
							allaccioDocUscitaDTO.getStato(), null, allaccioDocUscitaDTO.getDataCreazione(), allaccioDocUscitaDTO.getDataAggiornamento(),
							allaccioDocUscitaDTO.getIdAoo(), conn);
				}
			}
			commitConnection(conn);
		} catch (final Exception e) {
			LOGGER.error("Errore nell'inserimento dei documenti nella tabella di allaccio documenti di uscita :" + e);
			rollbackConnection(conn);
		} finally {
			closeConnection(conn);
		}
	}

	/**
	 * Restituisce i documenti allacciati in uscita.
	 * 
	 * @see it.ibm.red.business.service.IAllaccioDocUscitaSRV#getAllaccioDocUscitaByAoo(int,
	 *      java.lang.Integer)
	 */
	@Override
	public List<AllaccioDocUscitaDTO> getAllaccioDocUscitaByAoo(final int idAoo, final Integer stato) {
		List<AllaccioDocUscitaDTO> allaccioDocUscitaList = null;
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), true);
			final String documentTileToProcess = allaccioUscitaDAO.selectDocumentTileToProcess(stato, idAoo, conn);
			if (!StringUtils.isNullOrEmpty(documentTileToProcess)) {
				allaccioUscitaDAO.updateStato(documentTileToProcess, null, AllaccioDocUscitaEnum.IN_LAVORAZIONE.getStato(), AllaccioDocUscitaEnum.DA_LAVORARE.getStato(), null,
						idAoo, conn);
				allaccioDocUscitaList = new ArrayList<>();
				allaccioDocUscitaList = allaccioUscitaDAO.getAllaccioDocUscitaByAoo(documentTileToProcess, AllaccioDocUscitaEnum.IN_LAVORAZIONE.getStato(), idAoo, conn);
			}
			commitConnection(conn);

		} catch (final Exception e) {
			LOGGER.error("Errore nella ricerca dei documenti allacciati in uscita :" + e);
			rollbackConnection(conn);
		} finally {
			closeConnection(conn);
		}
		return allaccioDocUscitaList;
	}

	/**
	 * Restituisce le acl del documento in uscita e le usa per impostarne le
	 * security.
	 * 
	 * @see it.ibm.red.business.service.IAllaccioDocUscitaSRV#getAclDocUscitaAndSetSecurity(java.lang.String,
	 *      java.util.List, it.ibm.red.business.persistence.model.Aoo,
	 *      it.ibm.red.business.dto.AggiornaSecurityNpsDTO)
	 */
	@Override
	public String getAclDocUscitaAndSetSecurity(final String docTitleUscita, final List<SecurityDTO> securityDTOList, final Aoo aoo,
			final AggiornaSecurityNpsDTO aggiornaSecurityDTO) {
		IFilenetCEHelper fceh = null;
		String ufficioOUtenteList = "";
		Connection conn = null;

		try {
			final AooFilenet aooFilenet = aoo.getAooFilenet();
			fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getObjectStore(), aoo.getIdAoo());
			conn = setupConnection(getDataSource().getConnection(), false);

			final Document documentoUscita = fceh.getDocumentByDTandAOO(docTitleUscita, aoo.getIdAoo());
			if (documentoUscita != null) {
				final AccessPermissionList aclDocUscita = documentoUscita.get_Permissions();
				final Iterator<?> itAclUscita = aclDocUscita.iterator();

				final StringBuilder ufficioOUtenteSb = new StringBuilder();
				while (itAclUscita.hasNext()) {
					final SecurityDTO securityDTO = new SecurityDTO();
					final AccessPermission perm = (AccessPermission) itAclUscita.next();
					final String ufficioOUtente = perm.get_GranteeName().split("@")[0];

					ufficioOUtenteSb.append(ufficioOUtente).append(",");

					setSecurityDocIngressoUscita(conn, securityDTO, perm, ufficioOUtente);
					securityDTOList.add(securityDTO);
				}
				// Recupero lista di acl per confronto
				ufficioOUtenteList = ufficioOUtenteSb.substring(0, ufficioOUtenteSb.length() - 1);

				// Nel caso di protocollazione non standard bisogna aggiornare le acl anche su
				// nps
				final PropertiesProvider pp = PropertiesProvider.getIstance();
				final String idProtocollo = documentoUscita.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY));
				if (idProtocollo != null) {
					aggiornaSecurityDTO.setIdProtocollo(idProtocollo);

					final Integer idUtenteCreatore = documentoUscita.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.UTENTE_CREATORE_ID_METAKEY));
					UtenteDTO utenteDTO = new UtenteDTO();
					if (idUtenteCreatore != null) {
						utenteDTO = utenteSRV.getById(idUtenteCreatore.longValue());
						aggiornaSecurityDTO.setUtente(utenteDTO);
					}

					final Integer annoProtocollo = documentoUscita.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY));
					if (annoProtocollo != null) {
						aggiornaSecurityDTO.setAnnoProtocollo(annoProtocollo);
					}

					final Integer numProtocollo = documentoUscita.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY));
					if (numProtocollo != null) {
						final String infoProtocollo = new StringBuilder().append(numProtocollo).append("/").append(annoProtocollo).append("_").append(aoo.getCodiceAoo())
								.toString();
						aggiornaSecurityDTO.setInfoProtocollo(infoProtocollo);
					}

					aggiornaSecurityDTO.setAggiornaAclNps(true);
				}
				commitConnection(conn);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero delle acl per il documento in uscita :" + e);
			rollbackConnection(conn);
		} finally {
			closeConnection(conn);
			popSubject(fceh);
		}
		return ufficioOUtenteList;
	}

	/**
	 * Restituisce le security associate al documento in ingresso identificato dal:
	 * <code> docAllaccioIngresso </code>.
	 * 
	 * @see it.ibm.red.business.service.IAllaccioDocUscitaSRV#getSecurityByDocIngresso(java.lang.String,
	 *      java.lang.String, it.ibm.red.business.persistence.model.Aoo,
	 *      java.util.List, java.util.List)
	 */
	@Override
	public List<SecurityDTO> getSecurityByDocIngresso(final String aclDocUscita, final String docAllaccioIngresso, final Aoo aoo, final List<SecurityDTO> securityDTOList,
			final List<Integer> idUtenteDaTracciare) {
		IFilenetCEHelper fceh = null;

		Connection conn = null;
		try {
			// inizializza accesso a CE filenet
			final AooFilenet aooFilenet = aoo.getAooFilenet();
			fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getObjectStore(), aoo.getIdAoo());

			conn = setupConnection(getDataSource().getConnection(), false);

			// split acl doc uscita per confronto con quelle in ingresso
			final String[] aclDocUscitaSplit = aclDocUscita.split(",");
			final List<String> aclDocUscitaList = new ArrayList<>();
			aclDocUscitaList.addAll(Arrays.asList(aclDocUscitaSplit));

			// Recupero acl da doc ingresso
			final Document documentoIngresso = fceh.getDocumentByDTandAOO(docAllaccioIngresso, aoo.getIdAoo());
			if (documentoIngresso != null) {
				final AccessPermissionList acl = documentoIngresso.get_Permissions();

				final Iterator<?> itAcl = acl.iterator();
				while (itAcl.hasNext()) {
					final SecurityDTO securityDTO = new SecurityDTO();
					final AccessPermission perm = (AccessPermission) itAcl.next();
					final String ufficioOUtente = perm.get_GranteeName().split("@")[0];

					if (!aclDocUscitaList.contains(ufficioOUtente)) {
						setSecurityDocIngressoUscita(conn, securityDTO, perm, ufficioOUtente);
						securityDTOList.add(securityDTO);

					}

					if (!SecurityPrincipalType.GROUP.equals(perm.get_GranteeType())) {
						final Utente utente = utenteDAO.getByUsername(ufficioOUtente, conn);
						if (utente != null) {
							final UtenteS utenteSecurity = securityDTO.getUtente();
							utenteSecurity.setIdUtente(utente.getIdUtente().intValue());
							utenteSecurity.setUsername(utente.getUsername());
							securityDTO.setUtente(utenteSecurity);
						}
						if (securityDTO.getUtente() != null && !"p8admin".equalsIgnoreCase(securityDTO.getUtente().getUsername())
								&& !idUtenteDaTracciare.contains(securityDTO.getUtente().getIdUtente())) {
							idUtenteDaTracciare.add(securityDTO.getUtente().getIdUtente());
						}
					}
				}
			}
			commitConnection(conn);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero delle security per il documento in ingresso :" + e);
			rollbackConnection(conn);
		} finally {
			closeConnection(conn);
			popSubject(fceh);
		}
		return securityDTOList;
	}

	/**
	 * Aggiorna le security del documento in uscita identificato dal document title:
	 * <code> docTitleUscita </code>.
	 * 
	 * @see it.ibm.red.business.service.IAllaccioDocUscitaSRV#aggiornaSecurityDocUscita(java.lang.String,
	 *      java.lang.String, it.ibm.red.business.dto.ListSecurityDTO,
	 *      it.ibm.red.business.persistence.model.Aoo)
	 */
	@Override
	public void aggiornaSecurityDocUscita(final String docTitleUscita, final String docTitleIngresso, final ListSecurityDTO securityDTOList, final Aoo aoo) {
		IFilenetCEHelper fceh = null;
		Connection conn = null;

		try {
			final AooFilenet aooFilenet = aoo.getAooFilenet();
			conn = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getObjectStore(), aoo.getIdAoo());

			securitySRV.aggiornaEVerificaSecurityDocumento(docTitleUscita, aoo.getIdAoo(), securityDTOList, false, false, fceh, conn);
			updateStato(docTitleUscita, docTitleIngresso, AllaccioDocUscitaEnum.LAVORATO.getStato(), aoo.getIdAoo().intValue(), null);
			commitConnection(conn);
		} catch (final Exception ex) {
			LOGGER.warn(ex);
			updateStato(docTitleUscita, docTitleIngresso, aoo.getIdAoo().intValue(), AllaccioDocUscitaEnum.ERRORE.getStato(), ex.getMessage());
			rollbackConnection(conn);
		} finally {
			closeConnection(conn);
			popSubject(fceh);
		}
	}

	/**
	 * Effettua l'update dello stato.
	 * 
	 * @see it.ibm.red.business.service.IAllaccioDocUscitaSRV#updateStato(java.lang.String,
	 *      java.lang.Integer, java.lang.Integer, java.lang.String)
	 */
	@Override
	public void updateStato(final String docTitleUscita, final Integer statoFinale, final Integer idAoo, final String errorMsg) {
		updateStato(docTitleUscita, null, statoFinale, null, idAoo, errorMsg);
	}

	/**
	 * Effettua l'update dello stato.
	 * 
	 * @see it.ibm.red.business.service.IAllaccioDocUscitaSRV#updateStato(java.lang.String,
	 *      java.lang.String, java.lang.Integer, java.lang.Integer,
	 *      java.lang.String)
	 */
	@Override
	public void updateStato(final String docTitleUscita, final String docTitleIngresso, final Integer statoFinale, final Integer idAoo, final String errorMsg) {
		updateStato(docTitleUscita, docTitleIngresso, statoFinale, null, idAoo, errorMsg);
	}

	/**
	 * Effettua l'update dello stato.
	 * 
	 * @see it.ibm.red.business.service.IAllaccioDocUscitaSRV#updateStato(java.lang.String,
	 *      java.lang.String, java.lang.Integer, java.lang.Integer,
	 *      java.lang.Integer, java.lang.String)
	 */
	@Override
	public void updateStato(final String docTitleUscita, final String docTitleIngresso, final Integer statoFinale, final Integer statoIniziale, final Integer idAoo,
			final String errorMsg) {
		Connection conn = null;
		try {

			conn = setupConnection(getDataSource().getConnection(), false);
			final Integer upd = allaccioUscitaDAO.updateStato(docTitleUscita, docTitleIngresso, statoFinale, statoIniziale, errorMsg, idAoo, conn);

			if (upd != 1) {
				LOGGER.error("Errore aggiornamento per il doc title uscita: " + docTitleUscita);
			}

			commitConnection(conn);
		} catch (final Exception e) {
			LOGGER.error("Errore aggiornamento per il doc title uscita: " + docTitleUscita, e);

			rollbackConnection(conn);
		} finally {
			closeConnection(conn);
		}
	}

	/**
	 * Imposta le security.
	 * 
	 * @param conn
	 *            connessione al database
	 * @param securityDTO
	 *            security da impostare
	 * @param perm
	 *            permessi di accesso
	 * @param ufficioOUtente
	 *            ufficio dell'utente in sessione
	 */
	private void setSecurityDocIngressoUscita(final Connection conn, final SecurityDTO securityDTO, final AccessPermission perm, final String ufficioOUtente) {
		// Ufficio
		if (SecurityPrincipalType.GROUP.equals(perm.get_GranteeType())) {
			final String[] groupPrefixes = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.GROUP_SECURITY_PREFIX).split("\\|");
			final String idGruppo = ufficioOUtente.toLowerCase().replace(groupPrefixes[1].toLowerCase(), "").replace(groupPrefixes[0].toLowerCase(), "");

			final Nodo nodo = nodoDAO.getNodo(Long.valueOf(idGruppo), conn);

			if (nodo != null) {
				final GruppoS gruppoSecurityDTO = securityDTO.getGruppo();
				gruppoSecurityDTO.setIdUfficio(nodo.getIdNodo().intValue());
				gruppoSecurityDTO.setIdSiap(-1);
				securityDTO.setGruppo(gruppoSecurityDTO);

			}
		} else {
			// Utente
			final Utente utente = utenteDAO.getByUsername(ufficioOUtente, conn);

			if (utente != null) {
				final UtenteS utenteSecurity = securityDTO.getUtente();
				utenteSecurity.setIdUtente(utente.getIdUtente().intValue());
				utenteSecurity.setUsername(utente.getUsername());
				securityDTO.setUtente(utenteSecurity);
			}
		}
	}

	/**
	 * Aggiorna le security del documento.
	 * 
	 * @see it.ibm.red.business.service.IAllaccioDocUscitaSRV#aggiornaSecurityDocumento(it.ibm.red.business.persistence.model.Aoo,
	 *      java.util.List, java.lang.String,
	 *      it.ibm.red.business.dto.ListSecurityDTO,
	 *      it.ibm.red.business.dto.AllaccioDocUscitaDTO, java.util.List)
	 */
	@Override
	public void aggiornaSecurityDocumento(final Aoo aoo, final List<SecurityDTO> securityDTOList, final String aclDocUscita,
			final ListSecurityDTO securityDocIngresso, final AllaccioDocUscitaDTO docAllaccio, final List<Integer> idUtenteDaTracciare) {
		try {
			securityDocIngresso.addAll(getSecurityByDocIngresso(aclDocUscita, docAllaccio.getDocTitleIngresso(), aoo, securityDTOList, idUtenteDaTracciare));
			// Aggiorno security doc uscita
			if (!CollectionUtils.isEmpty(securityDocIngresso)) {
				aggiornaSecurityDocUscita(docAllaccio.getDocTitleUscita(), docAllaccio.getDocTitleIngresso(), securityDocIngresso, aoo);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di aggiornamento security", e);
		}
	}

	/**
	 * Traccia procedimento di allaccio del documento identificato da
	 * <code> idDocumento </code>.
	 * 
	 * @see it.ibm.red.business.service.IAllaccioDocUscitaSRV#tracciaProcedimentoAllaccioDoc(java.lang.Integer,
	 *      java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void tracciaProcedimentoAllaccioDoc(final Integer idAoo, final Integer idUtente, final Integer idDocumento) {
		Connection conn = null;
		try {
			conn = setupConnection(getFilenetDataSource().getConnection(), true);
			final TracciaDocUscitaDTO utenteDaTracciare = utenteDAO.getPredefinitoById(idAoo.longValue(), idUtente.longValue(), conn);
			tracciaDocumentoSRV.tracciaDocumento(utenteDaTracciare.getIdNodo(), utenteDaTracciare.getIdUtente(), utenteDaTracciare.getIdRuolo(), idDocumento, conn);
			commitConnection(conn);
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di tracciamento procedimento : " + e);
			rollbackConnection(conn);
		} finally {
			closeConnection(conn);
		}

	}

}
