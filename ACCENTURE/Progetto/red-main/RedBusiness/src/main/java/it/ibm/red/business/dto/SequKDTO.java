package it.ibm.red.business.dto;

/**
 * The Class SequKDTO.
 *
 * @author CPIERASC
 * 
 * 	DTO protollazione PMEF.
 */
public class SequKDTO extends AbstractDTO {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 5925896701039864005L;
	
	/**
	 * Aoo.
	 */
	private int sequKAoo;
	
	/**
	 * Destinatario.
	 */
	private int sequKDestinatario;
	
	/**
	 * Mezzo spedizione.
	 */
	private int sequKMezzoSpedizione;
	
	/**
	 * Registro.
	 */
	private int sequKRegistro;
	
	/**
	 * Storico ufficio.
	 */
	private int sequKStoricoUff;
	
	/**
	 * Tipo documento.
	 */
	private int sequKTipodocumento;
	
	/**
	 * Titolario.
	 */
	private int sequKTitolario;
	
	/**
	 * Ufficio.
	 */
	private int sequKUfficio;
	
	/**
	 * Ufficio mittente.
	 */
	private int sequKUfficioMittente;
	
	/**
	 * Ufficio Protocollatore.
	 */
	private int sequKUfficioProtocollatore;
	
	/**
	 * Utente.
	 */
	private int sequKUtente;
	
	/**
	 * Utente mittente.
	 */
	private int sequKUtenteMittente;
	
	/**
	 * Utente protocollatore.
	 */
	private int sequKUtenteProtocollatore;
	
	/**
	 * Assegnatari.
	 */
	private String assegnatari;
	
	
	/**
	 * Costruttore.
	 * 
	 * @param inSequKAoo
	 *            aoo
	 * @param inSequKDestinatario
	 *            destinatario
	 * @param inSequKMezzoSpedizione
	 *            mezzo spedizione
	 * @param inSequKRegistro
	 *            registro
	 * @param inSequKStoricoUff
	 *            storico ufficio
	 * @param inSequKTipodocumento
	 *            tipo documento
	 * @param inSequKTitolario
	 *            titolario
	 * @param inSequKUfficio
	 *            ufficio
	 * @param inSequKUfficioMittente
	 *            ufficio mittente
	 * @param inSequKUfficioProtocollatore
	 *            ufficio protocollatore
	 * @param inSequKUtente
	 *            utente
	 * @param inSequKUtenteMittente
	 *            utente mittente
	 * @param inSequKUtenteProtocollatore
	 *            utente protocollatore
	 * @param inAssegnatari
	 *            assegnatari
	 */
	public SequKDTO(final int inSequKAoo, final int inSequKDestinatario, final int inSequKMezzoSpedizione, final int inSequKRegistro,
			final int inSequKStoricoUff, final int inSequKTipodocumento, final int inSequKTitolario, final int inSequKUfficio,
			final int inSequKUfficioMittente, final int inSequKUfficioProtocollatore, final int inSequKUtente,
			final int inSequKUtenteMittente, final int inSequKUtenteProtocollatore, final String inAssegnatari) {
		super();
		this.sequKAoo = inSequKAoo;
		this.sequKDestinatario = inSequKDestinatario;
		this.sequKMezzoSpedizione = inSequKMezzoSpedizione;
		this.sequKRegistro = inSequKRegistro;
		this.sequKStoricoUff = inSequKStoricoUff;
		this.sequKTipodocumento = inSequKTipodocumento;
		this.sequKTitolario = inSequKTitolario;
		this.sequKUfficio = inSequKUfficio;
		this.sequKUfficioMittente = inSequKUfficioMittente;
		this.sequKUfficioProtocollatore = inSequKUfficioProtocollatore;
		this.sequKUtente = inSequKUtente;
		this.sequKUtenteMittente = inSequKUtenteMittente;
		this.sequKUtenteProtocollatore = inSequKUtenteProtocollatore;
		this.assegnatari = inAssegnatari;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	sequKAoo
	 */
	public final int getSequKAoo() {
		return sequKAoo;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	sequKDestinatario
	 */
	public final int getSequKDestinatario() {
		return sequKDestinatario;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	sequKMezzoSpedizione
	 */
	public final int getSequKMezzoSpedizione() {
		return sequKMezzoSpedizione;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	sequKRegistro
	 */
	public final int getSequKRegistro() {
		return sequKRegistro;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	sequKStoricoUff
	 */
	public final int getSequKStoricoUff() {
		return sequKStoricoUff;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	sequKTipodocumento
	 */
	public final int getSequKTipodocumento() {
		return sequKTipodocumento;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	sequKTitolario
	 */
	public final int getSequKTitolario() {
		return sequKTitolario;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	sequKUfficio
	 */
	public final int getSequKUfficio() {
		return sequKUfficio;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	sequKUfficioMittente
	 */
	public final int getSequKUfficioMittente() {
		return sequKUfficioMittente;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	sequKUfficioProtocollatore
	 */
	public final int getSequKUfficioProtocollatore() {
		return sequKUfficioProtocollatore;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	sequKUtente
	 */
	public final int getSequKUtente() {
		return sequKUtente;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	sequKUtenteMittente
	 */
	public final int getSequKUtenteMittente() {
		return sequKUtenteMittente;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	sequKUtenteProtocollatore
	 */
	public final int getSequKUtenteProtocollatore() {
		return sequKUtenteProtocollatore;
	}

	/**
	 * Getter.
	 * 
	 * @return assegnatari
	 */
	public String getAssegnatari() {
		return assegnatari;
	}
}