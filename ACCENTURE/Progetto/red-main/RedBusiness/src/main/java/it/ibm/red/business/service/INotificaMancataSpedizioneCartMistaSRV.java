package it.ibm.red.business.service;

import it.ibm.red.business.persistence.model.Aoo;

/**
 * Interfaccia del servizio notifica mancata spedizione cartacea mista.
 * @author VINGENITO
 */
public interface INotificaMancataSpedizioneCartMistaSRV extends INotificaWriteSRV {

	/**
	 * Recupera le notifiche in stato spedito dal pe, accede alla gestione notifiche email per escludere i record con stato inviato 
	 * ed per quelli restanti procede ad inviare la notifica agli utenti sottoscritti all'evento.
	 * @param aoo
	 */
	void getNotificaMancataSpedCartMista(Aoo aoo);
}

