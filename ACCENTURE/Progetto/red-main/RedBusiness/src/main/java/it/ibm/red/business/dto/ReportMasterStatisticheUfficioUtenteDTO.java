package it.ibm.red.business.dto;

import java.util.Date;
import java.util.List;

import it.ibm.red.business.enums.TargetNodoReportEnum;

/**
 * DTO per il master del report statistiche uffici.
 * 
 * @author a.dilegge
 *
 */
public class ReportMasterStatisticheUfficioUtenteDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Ufficio.
	 */
	private UfficioDTO ufficio;

	/**
	 * Utente.
	 */
	private UtenteDTO utente;

	/**
	 * Struttura.
	 */
	private NodoOrganigrammaDTO struttura;

	/**
	 * Numero documenti in ingresso.
	 */
	private Integer numeroDocumentiIngresso;

	/**
	 * Numero documenti in lavorazione.
	 */
	private Integer numeroDocumentiInLavorazioneIngresso;

	/**
	 * Numero documento in sospeso.
	 */
	private Integer numeroDocumentiInSospesoIngresso;

	/**
	 * Numero in scadenza.
	 */
	private Integer numeroDocumentiInScadenzaIngresso;

	/**
	 * Numero scaduti.
	 */
	private Integer numeroDocumentiScadutiIngresso;

	/**
	 * Numero documenti atti.
	 */
	private Integer numeroDocumentiAttiIngresso;

	/**
	 * Numero documenti in risposta.
	 */
	private Integer numeroDocumentiRispostaIngresso;

	/**
	 * Numero documenti in uscita.
	 */
	private Integer numeroDocumentiUscita;

	/**
	 * Numero documenti uscita in lavorazione.
	 */
	private Integer numeroDocumentiInLavorazioneUscita;

	/**
	 * Nmero documenti spediti.
	 */
	private Integer numeroDocumentiSpeditiUscita;

	/**
	 * Lista strutture.
	 */
	private List<NodoOrganigrammaDTO> strutture;

	/**
	 * Tipo struttra.
	 */
	private TargetNodoReportEnum tipoStruttura;

	/**
	 * Anno.
	 */
	private String anno;

	/**
	 * Data da.
	 */
	private Date dataDa;

	/**
	 * Data a.
	 */
	private Date dataA;

	/**
	 * Costruttore.
	 * 
	 * @param strutture
	 * @param tipoStruttura
	 * @param anno
	 * @param dataDa
	 * @param dataA
	 */
	public ReportMasterStatisticheUfficioUtenteDTO(final List<NodoOrganigrammaDTO> strutture, final TargetNodoReportEnum tipoStruttura, final String anno, final Date dataDa,
			final Date dataA) {
		super();
		this.strutture = strutture;
		this.tipoStruttura = tipoStruttura;
		this.anno = anno;
		this.dataDa = dataDa;
		this.dataA = dataA;
	}

	/**
	 * Restituisce l'ufficio.
	 * 
	 * @return ufficio
	 */
	public UfficioDTO getUfficio() {
		return ufficio;
	}

	/**
	 * Imposta l'ufficio.
	 * 
	 * @param ufficio
	 */
	public void setUfficio(final UfficioDTO ufficio) {
		this.ufficio = ufficio;
	}

	/**
	 * Restituisce l'utente.
	 * 
	 * @return utente
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Imposta l'utente.
	 * 
	 * @param utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * Restituisce la struttura.
	 * 
	 * @return struttura
	 */
	public NodoOrganigrammaDTO getStruttura() {
		return struttura;
	}

	/**
	 * Imposta la struttura.
	 * 
	 * @param struttura
	 */
	public void setStruttura(final NodoOrganigrammaDTO struttura) {
		this.struttura = struttura;
	}

	/**
	 * Restituisce il numero documenti in ingresso.
	 * 
	 * @return numero documenti in ingresso
	 */
	public Integer getNumeroDocumentiIngresso() {
		return numeroDocumentiIngresso;
	}

	/**
	 * Imposta il numero dei documenti in ingresso.
	 * 
	 * @param numeroDocumentiIngresso
	 */
	public void setNumeroDocumentiIngresso(final Integer numeroDocumentiIngresso) {
		this.numeroDocumentiIngresso = numeroDocumentiIngresso;
	}

	/**
	 * Restituisce il numero dei documenti in lavorzione in ingresso.
	 * 
	 * @return numero documenti in lavorazione in ingresso
	 */
	public Integer getNumeroDocumentiInLavorazioneIngresso() {
		return numeroDocumentiInLavorazioneIngresso;
	}

	/**
	 * Imposta il numero dei documenti in lavorzione in ingresso.
	 * 
	 * @param numeroDocumentiInLavorazioneIngresso
	 */
	public void setNumeroDocumentiInLavorazioneIngresso(final Integer numeroDocumentiInLavorazioneIngresso) {
		this.numeroDocumentiInLavorazioneIngresso = numeroDocumentiInLavorazioneIngresso;
	}

	/**
	 * Restituisce il numero dei documenti in sospeso in ingresso.
	 * 
	 * @return il numero dei documenti in sospeso in ingresso
	 */
	public Integer getNumeroDocumentiInSospesoIngresso() {
		return numeroDocumentiInSospesoIngresso;
	}

	/**
	 * Imposta il numero dei documenti in sospeso in ingresso.
	 * 
	 * @param numeroDocumentiInSospesoIngresso
	 */
	public void setNumeroDocumentiInSospesoIngresso(final Integer numeroDocumentiInSospesoIngresso) {
		this.numeroDocumentiInSospesoIngresso = numeroDocumentiInSospesoIngresso;
	}

	/**
	 * Restituisce il numero deocumenti in scadenza in ingresso.
	 * 
	 * @return il numero deocumenti in scadenza in ingresso
	 */
	public Integer getNumeroDocumentiInScadenzaIngresso() {
		return numeroDocumentiInScadenzaIngresso;
	}

	/**
	 * Imposta il numero deocumenti in scadenza in ingresso.
	 * 
	 * @param numeroDocumentiInScadenzaIngresso
	 */
	public void setNumeroDocumentiInScadenzaIngresso(final Integer numeroDocumentiInScadenzaIngresso) {
		this.numeroDocumentiInScadenzaIngresso = numeroDocumentiInScadenzaIngresso;
	}

	/**
	 * Restituisce il numero dei documenti scaduti in ingresso.
	 * 
	 * @return numero documenti scaduti in ingresso
	 */
	public Integer getNumeroDocumentiScadutiIngresso() {
		return numeroDocumentiScadutiIngresso;
	}

	/**
	 * Imposta il numero dei documenti scaduti in ingresso.
	 * 
	 * @param numeroDocumentiScadutiIngresso
	 */
	public void setNumeroDocumentiScadutiIngresso(final Integer numeroDocumentiScadutiIngresso) {
		this.numeroDocumentiScadutiIngresso = numeroDocumentiScadutiIngresso;
	}

	/**
	 * Restituisce il numero dei documeni "Atti" in ingresso.
	 * 
	 * @return numero documenti atti in ingresso.
	 */
	public Integer getNumeroDocumentiAttiIngresso() {
		return numeroDocumentiAttiIngresso;
	}

	/**
	 * Imposta il numero dei documeni "Atti" in ingresso.
	 * 
	 * @param numeroDocumentiAttiIngresso
	 */
	public void setNumeroDocumentiAttiIngresso(final Integer numeroDocumentiAttiIngresso) {
		this.numeroDocumentiAttiIngresso = numeroDocumentiAttiIngresso;
	}

	/**
	 * Restituisce il numero dei documenti in risposta in ingresso.
	 * 
	 * @return numero dei documenti in risposta in ingresso
	 */
	public Integer getNumeroDocumentiRispostaIngresso() {
		return numeroDocumentiRispostaIngresso;
	}

	/**
	 * Imposta il numero dei documenti in risposta in ingresso.
	 * 
	 * @param numeroDocumentiRispostaIngresso
	 */
	public void setNumeroDocumentiRispostaIngresso(final Integer numeroDocumentiRispostaIngresso) {
		this.numeroDocumentiRispostaIngresso = numeroDocumentiRispostaIngresso;
	}

	/**
	 * Restitusce il numero documenti in uscita.
	 * 
	 * @return numero documenti in uscita
	 */
	public Integer getNumeroDocumentiUscita() {
		return numeroDocumentiUscita;
	}

	/**
	 * Imposta il numero documenti in uscita.
	 * 
	 * @param numeroDocumentiUscita
	 */
	public void setNumeroDocumentiUscita(final Integer numeroDocumentiUscita) {
		this.numeroDocumentiUscita = numeroDocumentiUscita;
	}

	/**
	 * Restituisce il numero dei documenti in lavorazione in uscita.
	 * 
	 * @return numero dei documenti in lavorazione in uscita
	 */
	public Integer getNumeroDocumentiInLavorazioneUscita() {
		return numeroDocumentiInLavorazioneUscita;
	}

	/**
	 * Imposta il numero dei documenti in lavorazione in uscita.
	 * 
	 * @param numeroDocumentiInLavorazioneUscita
	 */
	public void setNumeroDocumentiInLavorazioneUscita(final Integer numeroDocumentiInLavorazioneUscita) {
		this.numeroDocumentiInLavorazioneUscita = numeroDocumentiInLavorazioneUscita;
	}

	/**
	 * Restituisce il numero documenti spediti in uscita.
	 * 
	 * @return numero documenti spediti in uscita
	 */
	public Integer getNumeroDocumentiSpeditiUscita() {
		return numeroDocumentiSpeditiUscita;
	}

	/**
	 * Imposta il numero documenti spediti in uscita.
	 * 
	 * @param numeroDocumentiSpeditiUscita
	 */
	public void setNumeroDocumentiSpeditiUscita(final Integer numeroDocumentiSpeditiUscita) {
		this.numeroDocumentiSpeditiUscita = numeroDocumentiSpeditiUscita;
	}

	/**
	 * Restituisce la lista delle strutture.
	 * 
	 * @return strutture
	 */
	public List<NodoOrganigrammaDTO> getStrutture() {
		return strutture;
	}

	/**
	 * Imposta la lista delle strutture.
	 * 
	 * @param strutture
	 */
	public void setStrutture(final List<NodoOrganigrammaDTO> strutture) {
		this.strutture = strutture;
	}

	/**
	 * Restituisce il tipo della struttura.
	 * 
	 * @return tipo struttura
	 */
	public TargetNodoReportEnum getTipoStruttura() {
		return tipoStruttura;
	}

	/**
	 * Impota il tipo della struttura.
	 * 
	 * @param tipoStruttura
	 */
	public void setTipoStruttura(final TargetNodoReportEnum tipoStruttura) {
		this.tipoStruttura = tipoStruttura;
	}

	/**
	 * Restituisce l'anno.
	 * 
	 * @return anno
	 */
	public String getAnno() {
		return anno;
	}

	/**
	 * Imposta l'anno.
	 * 
	 * @param anno
	 */
	public void setAnno(final String anno) {
		this.anno = anno;
	}

	/**
	 * Restituisce la data iniziale del range di date.
	 * 
	 * @return data iniziale
	 */
	public Date getDataDa() {
		return dataDa;
	}

	/**
	 * Imposta la data iniziale del range di date.
	 * 
	 * @param dataDa
	 */
	public void setDataDa(final Date dataDa) {
		this.dataDa = dataDa;
	}

	/**
	 * Restituisce la data finale del range di date.
	 * 
	 * @return data finale
	 */
	public Date getDataA() {
		return dataA;
	}

	/**
	 * Imposta la data finale del range di date.
	 * 
	 * @param dataA
	 */
	public void setDataA(final Date dataA) {
		this.dataA = dataA;
	}

}
