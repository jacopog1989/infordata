package it.ibm.red.business.enums;

/**
 * Modalità tipo documento.
 */
public enum TipoDocumentoModeEnum {

	/**
	 * Valore.
	 */
	SOLO_ENTRATA("E", "SOLO ENTRATA"),

	/**
	 * Valore.
	 */
	SOLO_USCITA("U", "SOLO USCITA"),

	/**
	 * Valore.
	 */
	MAI("M", "MAI"),

	/**
	 * Valore.
	 */
	SEMPRE("S", "SEMPRE");


	/**
	 * Codice.
	 */
	private String code;

	/**
	 * Descrizione.
	 */
	private String description;

	TipoDocumentoModeEnum(final String code, final String description) {
		this.code = code;
		this.description = description;
	}
	
	/**
	 * Restituisce il codice.
	 * @return code
	 */
	public String getCode() {
		return code;
	}
	
	/**
	 * Restituisce la descrizione.
	 * @return descrizione
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Restituisce l'enum associata ll codice.
	 * @param inCode
	 * @return enum associata al codice
	 */
	public static TipoDocumentoModeEnum get(final String inCode) {
		TipoDocumentoModeEnum output = null;
		
		for (final TipoDocumentoModeEnum e: TipoDocumentoModeEnum.values()) {
			if (e.getCode().equalsIgnoreCase(inCode)) {
				output = e;
				break;
			}
		}
		
		return output;
	}

}
