package it.ibm.red.business.dto;

import org.apache.commons.lang3.StringUtils;

/**
 * Informazioni necessarie alla modifica della sicurezza per questo documento.
 * 
 * Queste stesse informazioni sono richieste sempre quando avviene una modifica della sicurezza di un documento
 * 
 *
 */
public class DetailAssegnazioneDTO {
	
    /**
     * Numero documento.
     */
	private Integer numeroDocumento;
	
    /**
     * Flag riservato.
     */
	private Boolean riservato;
	
    /**
     * Document title.
     */
	private String documentTitle;
	
    /**
     * Id coordinatore.
     */
	private String coordinatoreIdUtente;
	
    /**
     * Id ufficio coordinatore.
     */
	private String coordinatoreIdUfficio;

	/**
	 * Costruttore del DTO.
	 * @param numeroDocumento
	 * @param riservato
	 * @param documentTitle
	 */
	public DetailAssegnazioneDTO(final Integer numeroDocumento, final Boolean riservato, final String documentTitle) {
		this.numeroDocumento = numeroDocumento;
		this.riservato = riservato;
		this.documentTitle = documentTitle;
	}

	/**
	 * Restituisce il numero documento.
	 * @return numeroDocumento
	 */
	public Integer getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * Restituisce true se è riservato, false altrimenti.
	 * @return
	 */
	public Boolean isRiservato() {
		return riservato;
	}

	/**
	 * Il documentTitle è il corrispettivo CE dell'idDocument PE.
	 * @return
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Definisce il coordinato impostandone ufficio e utente.
	 * @param coordinatoreIdUfficio
	 * @param coordinatoreIdUtente
	 */
	public void setCoordinatore(final String coordinatoreIdUfficio, final String coordinatoreIdUtente) {
		this.coordinatoreIdUfficio = coordinatoreIdUfficio;
		this.coordinatoreIdUtente = coordinatoreIdUtente;
	}

	/**
	 * Restituisce l'id del utente coordinatore.
	 * @return
	 */
	public String getCoordinatoreIdUtente() {
		return coordinatoreIdUtente;
	}

	/**
	 * Restituisce l'id dell'ufficio coordinatore.
	 * @return
	 */
	public String getCoordinatoreIdUfficio() {
		return coordinatoreIdUfficio;
	}

	/**
	 * Restituisce la descrizione del coordinatore comprendendo ufficio e utente, restituisce null se uno dei due è null.
	 * @return descrizioneAssegnazioneCoordinatore
	 */
	public String getAssegnazionePerCoordinatore() {
		String descrizioneAssegnazioneCoordinatore = null;
		if (!StringUtils.isEmpty(getCoordinatoreIdUfficio()) && !StringUtils.isEmpty(getCoordinatoreIdUtente())) {
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.append(getCoordinatoreIdUfficio());
			strBuilder.append(",");
			strBuilder.append(getCoordinatoreIdUtente());
			descrizioneAssegnazioneCoordinatore = strBuilder.toString();
		}
		return descrizioneAssegnazioneCoordinatore;
	}
	
}
