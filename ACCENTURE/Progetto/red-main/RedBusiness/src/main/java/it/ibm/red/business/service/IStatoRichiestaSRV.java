package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IStatoRichiestaFacadeSRV;

/**
 * The Interface IStatoRichiestaSRV.
 *
 * @author CPIERASC
 * 
 *         Servizio gestione stato richiesta.
 */
public interface IStatoRichiestaSRV extends IStatoRichiestaFacadeSRV {

}
