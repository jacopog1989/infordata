package it.ibm.red.business.dto;

import it.ibm.red.business.enums.ValueMetadatoVerificaFirmaEnum;

/**
 *   
 * @author m.crescentini
 *
 */
public class VerificaFirmaDTO extends AbstractDTO {
	
	private static final long serialVersionUID = -1080808610447770904L;

	/**
	 * Validità della firma.
	 */
	protected ValueMetadatoVerificaFirmaEnum valoreVerificaFirma;

	/**
	 * Flag visualizza informazioni verifica firma.
	 */
	protected boolean visualizzaInfoVerificaFirma;
	
	/**
	 * Restituisce il valore verifica firma.
	 * @return valore verifica firma
	 */
	public ValueMetadatoVerificaFirmaEnum getValoreVerificaFirma() {
		return valoreVerificaFirma;
	}

	/**
	 * Imposta il valore verifica firma.
	 * @param inValoreVerificaFirma
	 */
	public void setValoreVerificaFirma(final ValueMetadatoVerificaFirmaEnum inValoreVerificaFirma) {
		this.valoreVerificaFirma = inValoreVerificaFirma;
	}

	/**
	 * Restituisce il flag associato all'info verifica firma.
	 * @return flag associato all'info verifica firma
	 */
	public boolean isVisualizzaInfoVerificaFirma() {
		return visualizzaInfoVerificaFirma;
	}

	/**
	 * Imposta il flag associato all'info verifica firma.
	 * @param visualizzaInfoVerificaFirma
	 */
	public void setVisualizzaInfoVerificaFirma(final boolean visualizzaInfoVerificaFirma) {
		this.visualizzaInfoVerificaFirma = visualizzaInfoVerificaFirma;
	}
	
}
