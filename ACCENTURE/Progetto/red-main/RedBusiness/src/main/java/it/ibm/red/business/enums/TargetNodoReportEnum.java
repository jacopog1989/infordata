package it.ibm.red.business.enums;

/**
 * Si riferisce al codice presente nel dettaglio report
 * Serve all'utente per selezionare una ricerca del report per utente o ispettorato
 * @author a.difolca
 */
public enum TargetNodoReportEnum {

	/**
	 * Ufficio ispettore.
	 */
	UFFICIO("UFFICIO_ISPETTORATO"),

	/**
	 * Utente.
	 */
	UTENTE("UTENTE");


	/**
	 * Codice.
	 */
	private String dbCode;

	/**
	 * Costruttore di default.
	 * @param dbCode
	 */
	TargetNodoReportEnum(final String dbCode) {
		this.dbCode = dbCode;
	}

	/**
	 * @return dbCode
	 */
	public String getDbCode() {
		return dbCode;
	}

	/**
	 * Restituisce un oggetto dell'enum corrispondente al valore il input.
	 * @param dbCode
	 * @return s - TargetNodoReportEnum
	 */
	public static TargetNodoReportEnum getByDBCode(final String dbCode) {
		for (final TargetNodoReportEnum s: TargetNodoReportEnum.values()) {
			if (s.getDbCode().equals(dbCode)) {
				return s;
			}
		}
		return null;
	}
}
