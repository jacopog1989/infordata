package it.ibm.red.business.dto;

import java.util.List;

/**
 * DTO per la gestione del dettaglio fattura fepa.
 */
public class DetailFatturaFepaDTO extends DetailDocumentRedDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -2464817769765581646L;
	
	/**
	 * Id fascicolo fepa.
	 */
	private String idFascicoloFepa;

	/**
	 * Wob number.
	 */
	private String wobNumber;
	
	/**
	 * Documenti fepa.
	 */
	private List<DocumentoFascicoloFepaDTO> documentiFepa;

	/**
	 * Restituisce l'id del fascicolo FEPA.
	 * @return id fascicolo
	 */
	public String getIdFascicoloFepa() {
		return idFascicoloFepa;
	}

	/**
	 * Restituisce il wob number del fascicolo.
	 * @return wob number
	 */
	public String getWobNumber() {
		return wobNumber;
	}

	/**
	 * Imposta l'id del fascicolo FEPA.
	 * @param inIdFascicoloFepa
	 */
	public void setIdFascicoloFepa(final String inIdFascicoloFepa) {
		idFascicoloFepa = inIdFascicoloFepa;
	}

	/**
	 * Imposta il wob number.
	 * @param inWobNumber
	 */
	public void setWobNumber(final String inWobNumber) {
		wobNumber = inWobNumber;
	}

	/**
	 * Restituisce la lista dei documenti contenuti nel fascicolo FEPA.
	 * @return documenti FEPA
	 */
	public List<DocumentoFascicoloFepaDTO> getDocumentiFepa() {
		return documentiFepa;
	}

	/**
	 * Imposta i documenti associati al fascicolo.
	 * @param inDocumentiFepa
	 */
	public void setDocumentiFepa(final List<DocumentoFascicoloFepaDTO> inDocumentiFepa) {
		documentiFepa = inDocumentiFepa;
	}

}