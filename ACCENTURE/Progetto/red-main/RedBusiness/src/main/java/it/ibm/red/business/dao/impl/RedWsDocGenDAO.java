package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IRedWsDocGenDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 *
 * DAO per il tracciamento dei documenti creati tramite i web service esposti da
 * Red EVO.
 * 
 * @author m.crescentini
 */
@Repository
public class RedWsDocGenDAO extends AbstractDAO implements IRedWsDocGenDAO {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -7749947485498962185L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RedWsDocGenDAO.class.getName());


	/**
	 * @see it.ibm.red.business.dao.IRedWsDocGenDAO#insertDocGen(int,
	 *      java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public int insertDocGen(final int idDocumento, final Integer idFlusso, final Connection con) {
		int insert = 0;
		PreparedStatement ps = null;
		
		try {
			ps = con.prepareStatement("INSERT INTO REDWS_DOC_GEN(IDDOCUMENTO, IDFLUSSO) VALUES (?, ?)");
			
			int index = 1;
			ps.setInt(index++, idDocumento);
			if (idFlusso != null) {
				ps.setInt(index++, idFlusso);
			} else {
				ps.setNull(index++, Types.INTEGER);
			}
			
			insert = ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Si è verificato un errore durante l'inserimento nella tabella per il documento: " + idDocumento + " e il flusso: " + idFlusso, e);
			throw new RedException("Errore durante l'inserimento nella tabella per il documento: " + idDocumento + " e il flusso: " + idFlusso, e);
		} finally {
			closeStatement(ps);
		}
		
		return insert;
	}


	/**
	 * @see it.ibm.red.business.dao.IRedWsDocGenDAO#isDocPresente(int,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean isDocPresente(final int idDocumento, final Integer idFlusso, final Connection con) {
		boolean docPresente = false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = con.prepareStatement("SELECT * FROM REDWS_DOC_GEN WHERE IDDOCUMENTO = ? AND IDFLUSSO = ?");
			
			int index = 1;
			ps.setInt(index++, idDocumento);
			ps.setInt(index++, idFlusso);
			
			rs = ps.executeQuery();
			
			if (rs.next()) {
				docPresente = true;
			}
		} catch (final SQLException e) {
			LOGGER.error("Si è verificato un errore la verifica della presenza del documento: " + idDocumento, e);
			throw new RedException("Errore durante la verifica della presenza del documento: " + idDocumento, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return docPresente;
	}
	
}
