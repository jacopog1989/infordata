package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.util.CollectionUtils;

import com.filenet.api.collection.FolderSet;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.core.Document;
import com.filenet.api.core.Folder;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.IAllaccioDAO;
import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.IContattoDAO;
import it.ibm.red.business.dao.IIterApprovativoDAO;
import it.ibm.red.business.dao.ILegislaturaDAO;
import it.ibm.red.business.dao.IMetadatoDAO;
import it.ibm.red.business.dao.IMezzoRicezioneDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IRegistroRepertorioDAO;
import it.ibm.red.business.dao.IRiferimentoStoricoDAO;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dao.ITipologiaDocumentoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.ApprovazioneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.MezzoRicezioneDTO;
import it.ibm.red.business.dto.NotificaAzioneNpsDTO;
import it.ibm.red.business.dto.NotificaNpsDTO;
import it.ibm.red.business.dto.RegistrazioneAusiliariaDTO;
import it.ibm.red.business.dto.RegistroRepertorioDTO;
import it.ibm.red.business.dto.RiferimentoProtNpsDTO;
import it.ibm.red.business.dto.RiferimentoStoricoNsdDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;
import it.ibm.red.business.enums.TipoNotificaAzioneNPSEnum;
import it.ibm.red.business.enums.TipoRubricaEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.enums.ValueMetadatoVerificaFirmaEnum;
import it.ibm.red.business.helper.metadatiestesi.MetadatiEstesiHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.Legislatura;
import it.ibm.red.business.persistence.model.TipoProcedimento;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IRegistrazioniAusiliarieSRV;
import it.ibm.red.business.service.IRegistroRepertorioSRV;
import it.ibm.red.business.service.IRubricaSRV;
import it.ibm.red.business.service.facade.INotificaNpsFacadeSRV;
import it.ibm.red.business.service.facade.IStoricoFacadeSRV;
import it.ibm.red.business.utils.DestinatariRedUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.webservice.model.interfaccianps.dto.ParametersAzioneRispostaAutomatica;

/**
 * @author m.crescentini
 *
 */
public class FromDocumentoToDetailRedTrasformer extends TrasformerCE<DetailDocumentRedDTO> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8212037904935061830L;
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FromDocumentoToDetailRedTrasformer.class.getName());

	/**
	 * DAO per recuperare la tipologia di documento.
	 */
	private final ITipologiaDocumentoDAO tipologiaDocumentoDAO;

	/**
	 * DAO nodo.
	 */
	private final INodoDAO nodoDAO;

	/**
	 * DAO per recuperare l'aoo e l'ente.
	 */
	private final IAooDAO aooDAO;

	/**
	 * DAO per la gestione dei mezzi di ricezione.
	 */
	private final IMezzoRicezioneDAO mezzoRicezioneDAO;

	/**
	 * DAO per recuperare il tipo procedimento
	 */
	private final ITipoProcedimentoDAO tipoProcedimentoDAO;

	/**
	 * DAO per recuperare il tipo procedimento
	 */
	private final IAllaccioDAO allaccioDAO;

	/**
	 * DAO per recuperare le info di contatto dell'utente
	 */
	private final IContattoDAO contattoDAO;

	/**
	 * DAO storico.
	 */
	private final IStoricoFacadeSRV storicoSRV;

	/**
	 * DAO per il recupero dell'iter approvativo.
	 */
	private final IIterApprovativoDAO iterApprovativoDAO;

	/**
	 * DAO.
	 */
	private final ILegislaturaDAO legislaturaDAO;

	/**
	 * DAO.
	 */
	private final IUtenteDAO utenteDAO;

	/**
	 * DAO.
	 */
	private final IRegistroRepertorioDAO registroRepertorioDAO;

	/**
	 * DAO.
	 */
	private final IRiferimentoStoricoDAO rifStoricoDAO;

	/**
	 * Service.
	 */
	private final IRubricaSRV rubricaSRV;

	/**
	 * DAO gestione metadati estesi.
	 */
	private final IMetadatoDAO metadatoDAO;

	/**
	 * Servizio.
	 */
	private final IRegistrazioniAusiliarieSRV registrazioniAusiliarieSRV;

	/**
	 * Servizio.
	 */
	private final IRegistroRepertorioSRV registroRepertorioSRV;

	/**
	 * Servizio.
	 */
	private final INotificaNpsFacadeSRV notificaNpsSRV;

	/**
	 * Costruttore che inizializza tutti i dao e i service che vengono usati dal
	 * trasformer.
	 */
	public FromDocumentoToDetailRedTrasformer() {
		super(TrasformerCEEnum.FROM_DOCUMENTO_TO_DETAIL_RED);
		tipologiaDocumentoDAO = ApplicationContextProvider.getApplicationContext().getBean(ITipologiaDocumentoDAO.class);
		nodoDAO = ApplicationContextProvider.getApplicationContext().getBean(INodoDAO.class);
		storicoSRV = ApplicationContextProvider.getApplicationContext().getBean(IStoricoFacadeSRV.class);
		aooDAO = ApplicationContextProvider.getApplicationContext().getBean(IAooDAO.class);
		mezzoRicezioneDAO = ApplicationContextProvider.getApplicationContext().getBean(IMezzoRicezioneDAO.class);
		tipoProcedimentoDAO = ApplicationContextProvider.getApplicationContext().getBean(ITipoProcedimentoDAO.class);
		allaccioDAO = ApplicationContextProvider.getApplicationContext().getBean(IAllaccioDAO.class);
		contattoDAO = ApplicationContextProvider.getApplicationContext().getBean(IContattoDAO.class);
		iterApprovativoDAO = ApplicationContextProvider.getApplicationContext().getBean(IIterApprovativoDAO.class);
		legislaturaDAO = ApplicationContextProvider.getApplicationContext().getBean(ILegislaturaDAO.class);
		utenteDAO = ApplicationContextProvider.getApplicationContext().getBean(IUtenteDAO.class);
		registroRepertorioDAO = ApplicationContextProvider.getApplicationContext().getBean(IRegistroRepertorioDAO.class);
		rifStoricoDAO = ApplicationContextProvider.getApplicationContext().getBean(IRiferimentoStoricoDAO.class);
		metadatoDAO = ApplicationContextProvider.getApplicationContext().getBean(IMetadatoDAO.class);
		registrazioniAusiliarieSRV = ApplicationContextProvider.getApplicationContext().getBean(IRegistrazioniAusiliarieSRV.class);
		registroRepertorioSRV = ApplicationContextProvider.getApplicationContext().getBean(IRegistroRepertorioSRV.class);
		rubricaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRubricaSRV.class);
		notificaNpsSRV = ApplicationContextProvider.getApplicationContext().getBean(INotificaNpsFacadeSRV.class);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE#trasform(com.filenet.api.core.Document,
	 *      java.sql.Connection).
	 */
	@Override
	public DetailDocumentRedDTO trasform(final Document document, final Connection connection) {
		if (document == null) {
			return null;
		}

		try {
			final DetailDocumentRedDTO d = new DetailDocumentRedDTO();

			/**
			 * Protocollo mittente
			 */
			d.setProtocolloMittente((String) getMetadato(document, PropertiesNameEnum.NUMERO_PROTOCOLLO_MITTENTE_METAKEY));
			d.setAnnoProtocolloMittente((Integer) getMetadato(document, PropertiesNameEnum.ANNO_PROTOCOLLO_MITTENTE_METAKEY));
			d.setDataProtocolloMittente((Date) getMetadato(document, PropertiesNameEnum.DATA_PROTOCOLLO_MITTENTE_METAKEY));

			d.setGuid(StringUtils.cleanGuidToString(document.get_Id()));
			d.setDocumentTitle((String) getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));

			d.setCreator(document.get_Creator());
			d.setAnnoDocumento((Integer) getMetadato(document, PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY));
			d.setNumeroDocumento((Integer) getMetadato(document, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY));
			d.setIdFormatoDocumento((Integer) getMetadato(document, PropertiesNameEnum.ID_FORMATO_DOCUMENTO));
			if (d.getIdFormatoDocumento() != null) {
				d.setFormatoDocumentoEnum(FormatoDocumentoEnum.getEnumById(d.getIdFormatoDocumento().longValue()));
			}

			d.setIdUtenteCreatore((Integer) getMetadato(document, PropertiesNameEnum.UTENTE_CREATORE_ID_METAKEY));
			d.setIdUfficioCreatore((Integer) getMetadato(document, PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY));

			if (d.getIdUfficioCreatore() > 0) {
				d.setUfficioMittente(nodoDAO.getNodo(Long.valueOf(d.getIdUfficioCreatore()), connection));
			}

			// tipologia documento start
			d.setIdTipologiaDocumento((Integer) getMetadato(document, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY));
			if (d.getIdTipologiaDocumento() != null) {
				final TipologiaDocumentoDTO tipologiaDocumento = tipologiaDocumentoDAO.getById(d.getIdTipologiaDocumento(), connection);
				d.setDescTipologiaDocumento(tipologiaDocumento.getDescrizione());
				d.setDocumentClass(tipologiaDocumento.getDocumentClass());
			}
			// tipologia documento end

			d.setOggetto((String) getMetadato(document, PropertiesNameEnum.OGGETTO_METAKEY));
			d.setAnnoProtocollo((Integer) getMetadato(document, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY));
			d.setNumeroProtocollo((Integer) getMetadato(document, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY));
			d.setTipoProtocollo((Integer) getMetadato(document, PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY));
			d.setIdProtocollo((String) getMetadato(document, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY));
			d.setDataProtocollo((Date) getMetadato(document, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY));
			d.setAnnoProtocolloEmergenza((Integer) getMetadato(document, PropertiesNameEnum.ANNO_PROTOCOLLO_EMERGENZA_METAKEY));
			d.setNumeroProtocolloEmergenza((Integer) getMetadato(document, PropertiesNameEnum.NUMERO_PROTOCOLLO_EMERGENZA_METAKEY));
			d.setDataProtocolloEmergenza((Date) getMetadato(document, PropertiesNameEnum.DATA_PROTOCOLLO_EMERGENZA_METAKEY));

			d.setProtocolloRiferimento((String) getMetadato(document, PropertiesNameEnum.PROTOCOLLO_RIFERIMENTO_METAKEY));

			Boolean flagItegrazioneDati = (Boolean) getMetadato(document, PropertiesNameEnum.INTEGRAZIONE_DATI_METAKEY);
			if (flagItegrazioneDati == null) {
				flagItegrazioneDati = false;
			}
			d.setIntegrazioneDati(flagItegrazioneDati);

			d.setTrasformazionePDFInErrore((Integer) getMetadato(document, PropertiesNameEnum.TRASFORMAZIONE_PDF_ERRORE_METAKEY));

			Boolean flagFirmaPDF = (Boolean) getMetadato(document, PropertiesNameEnum.FIRMA_PDF_METAKEY);
			if (flagFirmaPDF == null) {
				flagFirmaPDF = true;
			}
			d.setFirmaPDF(flagFirmaPDF);

			d.setIdCategoriaDocumento((Integer) getMetadato(document, PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY));
			d.setNomeFile((String) getMetadato(document, PropertiesNameEnum.NOME_FILE_METAKEY));
			d.setMimeType((String) getMetadato(document, PropertyNames.MIME_TYPE));
			d.setFlagFirmaAutografaRM((Boolean) getMetadato(document, PropertiesNameEnum.FLAG_FIRMA_AUTOGRAFA_RM_METAKEY));
			d.setDateCreated((Date) getMetadato(document, PropertiesNameEnum.DATA_CREAZIONE_METAKEY));
			d.setDataScadenza((Date) getMetadato(document, PropertiesNameEnum.DATA_SCADENZA_METAKEY));
			final Integer urgente = (Integer) getMetadato(document, PropertiesNameEnum.URGENTE_METAKEY);
			d.setUrgente((urgente != null && urgente == 1));

			final Integer idMomentoProtocollazione = (Integer) getMetadato(document, PropertiesNameEnum.MOMENTO_PROTOCOLLAZIONE_ID_METAKEY);
			if (idMomentoProtocollazione != null) {
				d.setMomentoProtocollazioneEnum(MomentoProtocollazioneEnum.getEnumById(idMomentoProtocollazione));
			}

			d.setIdAOO((Integer) getMetadato(document, PropertiesNameEnum.ID_AOO_METAKEY));
			final Aoo aoo = aooDAO.getAoo(d.getIdAOO().longValue(), connection);
			d.setAoo(aoo);

			/** ITER APPROVATIVO START ******************************************/
			final String iterApprovativoSemaforo = (String) getMetadato(document, PropertiesNameEnum.ITER_APPROVATIVO_METAKEY);
			if (!StringUtils.isNullOrEmpty(iterApprovativoSemaforo)) {
				final IterApprovativoDTO iterApprovativoDTO = iterApprovativoDAO.getIterApprovativoById(Integer.valueOf(iterApprovativoSemaforo), connection);
				if (iterApprovativoDTO != null) {
					d.setIdIterApprovativo(iterApprovativoDTO.getIdIterApprovativo());
					d.setIterApprovativoSemaforo(iterApprovativoDTO.getDescrizione());
				}
			}
			/** ITER APPROVATIVO END ********************************************/

			/**** STORICO START ********************/
			final Integer idDoc = Integer.parseInt(d.getDocumentTitle());
			d.setStorico(storicoSRV.getStorico(idDoc, d.getIdAOO()));
			final List<ApprovazioneDTO> approvazioniList = storicoSRV.getModelloApprovazioniVisuale(idDoc, d.getIdAOO());
			d.setApprovazioni(approvazioniList);
			/**** STORICO START ********************/

			d.setRiservato((Integer) getMetadato(document, PropertiesNameEnum.RISERVATO_METAKEY));

			d.setTipologiadocumentonps((String) getMetadato(document, PropertiesNameEnum.DESCRIZIONE_TIPOLOGIA_DOCUMENTO_NPS_METAKEY));
			d.setOggettoprotocollonps((String) getMetadato(document, PropertiesNameEnum.OGGETTO_PROTOCOLLO_NPS_METAKEY));

			// mezzo di ricezione start
			d.setMezzoRicezione((Integer) getMetadato(document, PropertiesNameEnum.METADATO_DOCUMENTO_MEZZORICEZIONE));
			if (d.getMezzoRicezione() != null) {
				final MezzoRicezioneDTO m = mezzoRicezioneDAO.getMezzoRicezioneByIdMezzo(d.getMezzoRicezione(), connection);
				d.setMezzoRicezioneDTO(m);
			}
			// mezzo di ricezione end

			// tipologia procedimento start
			d.setIdTipologiaProcedimento((Integer) getMetadato(document, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY));
			if (d.getIdTipologiaProcedimento() != null) {
				final TipoProcedimento t = tipoProcedimentoDAO.getTPbyId(connection, d.getIdTipologiaProcedimento());
				d.setDescTipoProcedimento(t.getDescrizione());
			}
			// tipologia procedimento end

			d.setNote((String) getMetadato(document, PropertiesNameEnum.NOTE_METAKEY));
			d.setDataAnnullamentoDocumento((Date) getMetadato(document, PropertiesNameEnum.METADATO_DOCUMENTO_DATA_ANNULLAMENTO));
			d.setBarcode((String) getMetadato(document, PropertiesNameEnum.BARCODE_METAKEY));

			/** FOLDER START ***********************/
			final FolderSet fs = document.get_FoldersFiledIn();
			if (fs != null) {
				String folder = null;
				final Iterator<?> itf = fs.iterator();
				if (itf.hasNext()) {
					folder = ((Folder) itf.next()).get_FolderName();
				}
				d.setFolder(folder);
			}
			/** FOLDER END *************************/

			final String mittenteMailProt = (String) getMetadato(document, PropertiesNameEnum.INDIRIZZO_MAIL_MITTENTE_PROT);
			if (!StringUtils.isNullOrEmpty(mittenteMailProt)) {
				d.setMittenteMailProt(mittenteMailProt);
			}

			boolean cercaMittenteVecchio = true;
			boolean isMittente = false;
			final String objectIdContattoMittente = (String) getMetadato(document, PropertiesNameEnum.OBJECTID_CONTATTO_MITT_METADATO);
			if (!StringUtils.isNullOrEmpty(objectIdContattoMittente)) {
				cercaMittenteVecchio = false;
				isMittente = true;
				final List<Contatto> listContatti = rubricaSRV.fetchContattiById(objectIdContattoMittente, aoo);
				if (!it.ibm.red.business.utils.CollectionUtils.isEmptyOrNull(listContatti)) {
					d.setMittenteContatto(listContatti.get(0));
				}
			}
			isMittente = handleVecchioMittente(document, connection, d, cercaMittenteVecchio, isMittente);

			handleMittente(document, connection, d, aoo, isMittente);

			// Recupero tipologia notifica per la gestione di protocolli associati a
			// RISPONDI_A e AGGIORNAMENTO_COLLEGATI
			final NotificaNpsDTO notificaNps = notificaNpsSRV.getNotificaAzione(d.getIdAOO().longValue(), d.getNumeroProtocollo(), d.getAnnoProtocollo());
			if (notificaNps != null && TipoNotificaAzioneNPSEnum.isRispostaAutomatica(notificaNps.getTipoNotifica())) {
				
				d.setElettronicoViaSistemaAusiliarioNPS(true);
				d.setSistemaAusiliarioNPS(((ParametersAzioneRispostaAutomatica)((NotificaAzioneNpsDTO)notificaNps).getDatiAzione()).getSistemaAusiliario());
				
				if(TipoNotificaAzioneNPSEnum.RISPONDI_A.equals(TipoNotificaAzioneNPSEnum.fromValue(notificaNps.getTipoNotifica()))) {
					d.setSistemaAusiliarioNPS(Constants.Varie.SICOGE);
				}
				
				if (!CollectionUtils.isEmpty(d.getDestinatari())) {
					for (final DestinatarioRedDTO destinatario : d.getDestinatari()) {
						destinatario.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.CARTACEO);
					}
				}
			} else {
				d.setElettronicoViaSistemaAusiliarioNPS(false);
				d.setSistemaAusiliarioNPS(null);
			}

			/**** ALLACCI START *************************************************/
			final Integer idDocumento = new Integer(d.getDocumentTitle());
			final List<RispostaAllaccioDTO> allacci = allaccioDAO.getDocumentiRispostaAllaccio(idDocumento, d.getIdAOO(), connection);

			d.setAllacci(allacci);
			/**** ALLACCI END ***************************************************/

			/****
			 * ALLACCI RIFERIMENTO STORICO START
			 *************************************************/
			final List<RiferimentoStoricoNsdDTO> allaccioRifStorico = rifStoricoDAO.getRiferimentoStorico(d.getDocumentTitle(), Long.valueOf(d.getIdAOO()), connection);
			d.setAllaccioRifStorico(allaccioRifStorico);
			/****
			 * ALLACCI RIFERIMENTO STORICO END
			 *************************************************/

			/**** ALLACCI RIFERIMENTO NPS START *************************************************/
			List<RiferimentoProtNpsDTO> allaccioRifNps = rifStoricoDAO.getRiferimentoAllaccioNps(d.getDocumentTitle(),Long.valueOf(d.getIdAOO()), connection);
			d.setAllaccioRifNps(allaccioRifNps);
			/**** ALLACCI RIFERIMENTO NPS END *************************************************/
			
			/***** IN RISPOSTA AL PROTOCOLLO START ************************************/
			d.setNumeroProtocolloRisposta((String) getMetadato(document, PropertiesNameEnum.NUMERO_PROTOCOLLO_RISPOSTA_STRING_METAKEY));
			d.setAnnoProtocolloRisposta((Integer) getMetadato(document, PropertiesNameEnum.ANNO_PROTOCOLLO_RISPOSTA_METAKEY));
			/***** IN RISPOSTA AL PROTOCOLLO END ************************************/

			/************************************
			 * PRELEX -> START
			 *********************************************/
			d.setNumeroRDP((Integer) getMetadato(document, PropertiesNameEnum.NUMERO_RDP_METAKEY));

			/************************************
			 * PRELEX -> START
			 *********************************************/
			d.setCodiceFlusso((String) getMetadato(document, PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY));

			/**
			 * LEGISLATURA START
			 *****************************************************************************************/
			d.setNumeroLegislatura((Integer) getMetadato(document, PropertiesNameEnum.NUMERO_LEGISLATURA_METAKEY));
			Legislatura l = null;
			if (d.getNumeroLegislatura() != null && d.getNumeroLegislatura().intValue() > 0) {
				l = legislaturaDAO.getLegislaturaByNumero(d.getNumeroLegislatura().intValue(), connection);
				d.setLegislatura(l.getDescrizione());
			} else {
				d.setNumeroLegislatura(null);
			}
			/**
			 * LEGISLATURA END
			 *******************************************************************************************/
			/************************************
			 * PRELEX -> END
			 *********************************************/

			d.setTipoProtocollo((Integer) getMetadato(document, PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY));

			/*************************************
			 * COORDINATORE
			 ***********************************************/
			final Integer idUtenteCoordinatore = (Integer) getMetadato(document, PropertiesNameEnum.ID_UTENTE_COORDINATORE_METAKEY);
			if (idUtenteCoordinatore != null && idUtenteCoordinatore > 0) {
				d.setIdUtenteCoordinatore(Long.valueOf(idUtenteCoordinatore));
			}

			final Integer idUfficioCoordinatore = (Integer) getMetadato(document, PropertiesNameEnum.ID_UFFICIO_COORDINATORE_METAKEY);
			if (idUfficioCoordinatore != null && idUfficioCoordinatore > 0) {
				d.setIdNodoCoordinatore(Long.valueOf(idUfficioCoordinatore));
			}

			/*************************************
			 * REGISTRI REPERTORIO
			 ********************************************/
			final RegistroRepertorioDTO denominazioneRegistro = registroRepertorioDAO.getDenominazioneRegistroUfficiale(d.getIdAOO().longValue(), connection);
			final String registroRepertorio = (String) getMetadato(document, PropertiesNameEnum.REGISTRO_PROTOCOLLO_FN_METAKEY);
			// Gestisce la renderizzazione dell'informazione su registro repertorio in
			// front-end
			final boolean daRepertoriare = false;

			handleRegistroRepertorio(connection, d, denominazioneRegistro, registroRepertorio, daRepertoriare);

			/*************************************
			 * METADATI ESTESI
			 ********************************************/
			if (d.getIdTipologiaDocumento() != null && d.getIdTipologiaProcedimento() != null) {
				final String xmlMetadatiEstesi = tipologiaDocumentoDAO.recuperaMetadatiEstesi(connection, idDocumento, Long.valueOf(d.getIdAOO()),
						Long.valueOf(d.getIdTipologiaDocumento()), Long.valueOf(d.getIdTipologiaProcedimento()));
				if (!StringUtils.isNullOrEmpty(xmlMetadatiEstesi)) {

					// Si recuperano i metadati associati alla tipologia documento
					final List<MetadatoDTO> metadatiTipologiaDocumento = metadatoDAO.caricaMetadati(d.getIdTipologiaDocumento(), d.getIdTipologiaProcedimento(), connection);

					if (!CollectionUtils.isEmpty(metadatiTipologiaDocumento)) {
						d.setMetadatiEstesi(MetadatiEstesiHelper.deserializeMetadatiEstesi(xmlMetadatiEstesi, metadatiTipologiaDocumento));
					}
				}
			}

			/*************************************
			 * SOTTO CATEGORIA USCITA
			 ********************************************/
			final Integer sottoCategoriaUscita = (Integer) getMetadato(document, PropertiesNameEnum.SOTTOCATEGORIA_DOCUMENTO_USCITA);
			if (sottoCategoriaUscita != null) {
				d.setSottoCategoriaUscita(SottoCategoriaDocumentoUscitaEnum.get(sottoCategoriaUscita));
			}

			final RegistrazioneAusiliariaDTO registrazioneAusiliaria = registrazioniAusiliarieSRV.getRegistrazioneAusiliaria(d.getDocumentTitle(), d.getIdAOO(), connection);
			d.setRegistrazioneAusiliaria(registrazioneAusiliaria);

			d.setDescFlusso((String) getMetadato(document, PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY));
			d.setIdProcedimento((String) getMetadato(document, PropertiesNameEnum.IDENTIFICATORE_PROCESSO_METAKEY));

			if (document.get_ContentSize() != null) {
				final float sizeMB = d.convertiContentInMB(document.get_ContentSize().floatValue());
				final String sizeMBRound = String.format("%.2f", sizeMB).replace(",", ".");
				d.setSizeDocPrincipaleMB(Float.parseFloat(sizeMBRound));
				d.setSizeContent(Float.parseFloat(sizeMBRound));
			}

			/*************************************
			 * DATA SCARICO/RIPRISTINO
			 ********************************************/
			final Date dataScarico = (Date) getMetadato(document, PropertiesNameEnum.DATA_SCARICO_METAKEY);
			if (dataScarico != null) {
				d.setDataScarico(dataScarico);
			}

			final Integer metadatoDocumentoValiditaFirma = (Integer) getMetadato(document, PropertiesNameEnum.METADATO_DOCUMENTO_VALIDITA_FIRMA);
			ValueMetadatoVerificaFirmaEnum valueMetadatoEnum = null;
			if (metadatoDocumentoValiditaFirma != null) {
				valueMetadatoEnum = ValueMetadatoVerificaFirmaEnum.get(metadatoDocumentoValiditaFirma);
			}
			d.setValueMetaadatoVerificaFirmaEnum(valueMetadatoEnum);

			return d;
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero di un metadato del dettaglio del documento: ", e);
			return null;
		}
	}

	private void handleRegistroRepertorio(final Connection connection, final DetailDocumentRedDTO d, final RegistroRepertorioDTO denominazioneRegistro,
			final String registroRepertorio, boolean daRepertoriare) {
		if (registroRepertorio != null && !registroRepertorio.equalsIgnoreCase(denominazioneRegistro.getDescrizioneRegistroRepertorio())) {
			d.setIsModalitaRegistroRepertorio(true);
			d.setVisualizzaInformativaRR(true);
			d.setDescrizioneRegistroRepertorio(registroRepertorio);
		} else {
			// di default è il registro ufficiale
			d.setIsModalitaRegistroRepertorio(false);
			d.setVisualizzaInformativaRR(false);
			d.setDescrizioneRegistroRepertorio(denominazioneRegistro.getDescrizioneRegistroRepertorio());

			// cerco eventuali associazioni ai registri di repertorio
			final Map<Long, List<RegistroRepertorioDTO>> registriRepertorioConfigured = registroRepertorioSRV.getRegistriRepertorioConfigurati(d.getIdAOO().longValue(),
					connection);

			if (d.getIdTipologiaDocumento() != null && d.getIdTipologiaProcedimento() != null) {
				daRepertoriare = registroRepertorioSRV.checkIsRegistroRepertorio(d.getIdAOO().longValue(), d.getIdTipologiaDocumento().longValue(),
						d.getIdTipologiaProcedimento().longValue(), registriRepertorioConfigured);
			}

			if (daRepertoriare) {

				final List<RegistroRepertorioDTO> registriRepertorioList = registriRepertorioConfigured.get(d.getIdTipologiaProcedimento().longValue());

				if (!CollectionUtils.isEmpty(registriRepertorioList)) {

					for (final RegistroRepertorioDTO rr : registriRepertorioList) {
						if (rr.getIdTipologiaDocumento().equals(d.getIdTipologiaDocumento().longValue())) {
							d.setVisualizzaInformativaRR(daRepertoriare);
							d.setDescrizioneRegistroRepertorio(rr.getDescrizioneRegistroRepertorio());
							d.setIsModalitaRegistroRepertorio(true);
							break;
						}
					}
				}
			}

		}
	}

	private void handleMittente(final Document document, final Connection connection, final DetailDocumentRedDTO d, final Aoo aoo, final boolean isMittente) {
		if (!isMittente) {
			// ### DESTINATARI
			// #########################################################################################
			final Collection<?> metadatoDestinatari = (Collection<?>) getMetadato(document, PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY);

			// Si normalizza il metadato per evitare errori comuni durante lo split delle
			// singole stringhe dei destinatari
			final List<String[]> destinatariDocumento = DestinatariRedUtils.normalizzaMetadatoDestinatariDocumento(metadatoDestinatari);

			final String objectIdContattoDestinatario = (String) getMetadato(document, PropertiesNameEnum.OBJECTID_CONTATTO_DEST_METADATO);

			final HashMap<Long, Contatto> hasmapDest = new HashMap<>();
			if (!StringUtils.isNullOrEmpty(objectIdContattoDestinatario)) {
				final List<Contatto> listContatti = rubricaSRV.fetchContattiById(objectIdContattoDestinatario, aoo);
				for (final Contatto contatto : listContatti) {
					hasmapDest.put(contatto.getContattoID(), contatto);
				}
			}

			if (!CollectionUtils.isEmpty(destinatariDocumento)) {
				final List<DestinatarioRedDTO> list = new ArrayList<>();

				for (final String[] destSplit : destinatariDocumento) {
					DestinatarioRedDTO desDTO = null;

					final TipologiaDestinatarioEnum tde = TipologiaDestinatarioEnum.getByTipologia(destSplit[3]);
					if (TipologiaDestinatarioEnum.INTERNO == tde) {
						final Long idNodo = Long.valueOf(destSplit[0]);
						final Long idUtente = Long.valueOf(destSplit[1]);
						final String aliasContatto = destSplit[2];
						desDTO = new DestinatarioRedDTO(idNodo, idUtente, destSplit[3], destSplit[4]);
						final Contatto c = new Contatto(null, null, null, null, null, null, null, aliasContatto);
						c.setIdNodo(idNodo);
						c.setIdUtente(idUtente);
						desDTO.setContatto(c);
						desDTO.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.INTERNO);
					} else if (TipologiaDestinatarioEnum.ESTERNO == tde) {
						if (!hasmapDest.isEmpty()) {
							final Contatto cont = hasmapDest.get(Long.valueOf(destSplit[0]));
							final Integer idMezzo = Integer.valueOf(destSplit[2]);
							desDTO = new DestinatarioRedDTO(idMezzo, destSplit[3], destSplit[4]);
							desDTO.setContatto(cont);
							desDTO.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.ESTERNO);
						} else {
							final Long idContatto = Long.valueOf(destSplit[0]);
							final Integer idMezzo = Integer.valueOf(destSplit[2]);
							desDTO = new DestinatarioRedDTO(idMezzo, destSplit[3], destSplit[4]);
							final Contatto c = contattoDAO.getContattoByID(idContatto, connection);
							c.setMailSelected(null);
							desDTO.setContatto(c);
							desDTO.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.ESTERNO);
						}
					}
					if (desDTO != null) {
						list.add(desDTO);
					}
				}
				d.setDestinatari(list);
			}
			// ###################################################################################################################
		}
	}

	private boolean handleVecchioMittente(final Document document, final Connection connection, final DetailDocumentRedDTO d, final boolean cercaMittenteVecchio,
			boolean isMittente) {
		if (cercaMittenteVecchio) {

			final String mittente = (String) getMetadato(document, PropertiesNameEnum.METADATO_DOCUMENTO_MITTENTE);

			if (!StringUtils.isNullOrEmpty(mittente)) {
				isMittente = true;
				d.setMittente(mittente);
				final String[] mittSplit = mittente.split(",");
				final Long idContatto = Long.valueOf(mittSplit[0]);
				Contatto c = contattoDAO.getContattoByID(idContatto, connection);
				// Questa cosa e' profondamente sbagliata ma RED fa così quindi semplicemente
				// copio il comportamente
				// si assume quindi che il l'id estratto dal campo mittente di FileNet dovrebbe
				// essere l'id del contatto.
				// Se però non trova nulla nei contatti allora l'id corrisponde all'id sulla
				// tabella utente.

				boolean cercaUtente = c == null;
				if (!cercaUtente) {

					cercaUtente = true;

					final String mittenteSubs = mittente.substring(mittente.indexOf(",") + 1);
					final String alias = !StringUtils.isNullOrEmpty(mittenteSubs) ? mittenteSubs.toUpperCase() : Constants.EMPTY_STRING;
					final String mail = c != null && c.getMail() != null ? c.getMail().toUpperCase() : null;
					final String mailPEC = c != null && c.getMailPec() != null ? c.getMailPec().toUpperCase() : null;
					String aliasContatto = c != null && c.getAliasContatto() != null ? c.getAliasContatto().toUpperCase() : null;
					final String cognomeContatto = c != null && c.getCognome() != null ? c.getCognome().toUpperCase() : null;
					final String nomeContatto = c != null && c.getNome() != null ? c.getNome().toUpperCase() : null;

					// Così continua a valere il controllo di sotto
					if (TipoRubricaEnum.IPA.equals(c.getTipoRubricaEnum())) {
						aliasContatto = aliasContatto.replace(" " + c.getRiferimento().toUpperCase(), Constants.EMPTY_STRING);
					}

					// Gestione della lunghezza in quanto il metadato (id,alias) su FileNet è
					// troncato a SalvaDocumentoSRV.MAX_LENGTH_MITTENTE_METAKEY
					if (aliasContatto != null && aliasContatto.length() > alias.length()) {
						aliasContatto = aliasContatto.substring(0, alias.length());
					}

					if ((aliasContatto != null && alias.contains(aliasContatto)) || (mail != null && alias.contains(mail)) || (mailPEC != null && alias.contains(mailPEC)) 
							|| (cognomeContatto != null && alias.contains(cognomeContatto) || nomeContatto != null && alias.contains(nomeContatto))) {
						cercaUtente = false;
					}
				}

				if (cercaUtente) {
					final Utente u = utenteDAO.getUtente(idContatto, connection);
					if (u != null) {
						c = new Contatto();
						c.setTipoRubricaEnum(TipoRubricaEnum.INTERNO);
						c.setIdUtente(u.getIdUtente());
						c.setNome(u.getNome());
						c.setCognome(u.getCognome());
						c.setIdNodo(d.getUfficioMittente().getIdNodo());
					}
				}

				if (c != null) {
					if (StringUtils.isNullOrEmpty(c.getAliasContatto()) && mittSplit.length >= 2) {
						c.setAliasContatto(mittSplit[1]);
					}

					d.setMittenteContatto(c);
				}
			}
		}
		return isMittente;
	}

}
