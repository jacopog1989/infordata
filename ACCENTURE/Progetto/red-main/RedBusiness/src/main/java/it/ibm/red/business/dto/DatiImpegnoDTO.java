package it.ibm.red.business.dto;

import java.util.Date;

import it.ibm.red.business.enums.TipoAzioneEnum;

/**
 * The Class DatiImpegnoDTO.
 *
 * @author CPIERASC
 * 
 * 	DTO dati impegno.
 */
public class DatiImpegnoDTO extends AbstractDTO {
	
	/**
	 * Serializzable.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Tipologia impegno.
	 */
	private TipoAzioneEnum tipologia;

	/**
	 * Data impegno.
	 */
	private Date data;
	
	/**
	 * Identificativo.
	 */
	private Integer id;

	/**
	 * Costruttore.
	 * 
	 * @param inTipologia	tipologia
	 * @param inData		data
	 * @param inId			identificativo
	 */
	public DatiImpegnoDTO(final TipoAzioneEnum inTipologia, final Date inData, final Integer inId) {
		tipologia = inTipologia;
		data = inData;
		id = inId;
	}

	/**
	 * Getter.
	 * 
	 * @return	tipologia
	 */
	public final TipoAzioneEnum getTipologia() {
		return tipologia;
	}

	/**
	 * Getter.
	 * 
	 * @return	data
	 */
	public final Date getData() {
		return data;
	}

	/**
	 * Getter.
	 * 
	 * @return	identificativo
	 */
	public final Integer getId() {
		return id;
	}

}
