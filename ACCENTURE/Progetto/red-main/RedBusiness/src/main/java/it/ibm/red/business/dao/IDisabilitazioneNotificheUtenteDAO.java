package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.persistence.model.DisabilitazioneNotificheUtente;

/**
 * Interfaccia DAO disabilitazione notifiche utente.
 */
public interface IDisabilitazioneNotificheUtenteDAO extends Serializable {

	/**
	 * Ritorna le configurazione dell'utente.
	 * @param idUtente
	 * @param connection
	 * @return
	 */
	List<DisabilitazioneNotificheUtente> getbyUser(Long idUtente, Connection connection);

	/**
	 * Elimina gli intervalli di disabilitazione per l'utente.
	 * 
	 * @param idUtente
	 * @param connection
	 */
	void delete(Long idUtente, Connection connection);
	
	/**
	 * Inserisce l'intervallo di disabilitazione in input.
	 * 
	 * @param disabilitazioneNotificheUtente
	 * @param connection
	 */
	void insert(DisabilitazioneNotificheUtente disabilitazioneNotificheUtente, Connection connection);
	
}
