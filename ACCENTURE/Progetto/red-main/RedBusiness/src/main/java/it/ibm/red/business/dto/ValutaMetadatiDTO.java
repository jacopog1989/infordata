package it.ibm.red.business.dto;

/**
 * DTO di un metadato di tipo Valuta.
 * @author DarioVentimiglia
 */
public class ValutaMetadatiDTO extends AbstractDTO {
	
	/**
	 * serial version uid.
	 */
	private static final long serialVersionUID = -5975857661331035135L;

	/**
	 * Valuta.
	 */
	private LookupTableDTO valuta;
	
	/**
	 * Importo.
	 */
	private MetadatoDTO importo;
	
	/**
	 * Importo in euro.
	 */
	private MetadatoDTO importoEur;
	
	/**
	 * Costruttore vuoto.
	 */
	public ValutaMetadatiDTO() { }

	/**
	 * Costruttore di default.
	 * @param valuta
	 * @param importo
	 * @param importoEur
	 */
	public ValutaMetadatiDTO(LookupTableDTO valuta, MetadatoDTO importo, MetadatoDTO importoEur) {
		this.valuta = valuta;
		this.importo = importo;
		this.importoEur = importoEur;
	}
	
	/**
	 * Restituisce i metatadi della sovra-struttura corrente
	 * come una array, ordinati per visualizzazione.
	 * @return tripletta metadati valuta
	 */
	public MetadatoDTO [] getSottoMetadatiValuta() {
		return new MetadatoDTO [] {this.valuta, this.importo, this.importoEur};
	}

	/**
	 * Restituisce la valuta come lookuptable.
	 * @return valuta
	 */
	public LookupTableDTO getValuta() {
		return valuta;
	}

	/**
	 * Imposta la valuta come lookuptable.
	 * @param valuta
	 */
	public void setValuta(LookupTableDTO valuta) {
		this.valuta = valuta;
	}

	/**
	 * Restituisce il metadato che rappresenta l'importo.
	 * @return importo
	 */
	public MetadatoDTO getImporto() {
		return importo;
	}

	/**
	 * Imposta il metadato che rappresenta l'importo.
	 * @param importo
	 */
	public void setImporto(MetadatoDTO importo) {
		this.importo = importo;
	}

	/**
	 * Restituisce il metadato che rappresenta l'importo in EURO.
	 * @return importo in euro
	 */
	public MetadatoDTO getImportoEur() {
		return importoEur;
	}

	/**
	 * Imposta il metadato che rappresenta l'importo in EURO.
	 * @param importoEur
	 */
	public void setImportoEur(MetadatoDTO importoEur) {
		this.importoEur = importoEur;
	}

}
