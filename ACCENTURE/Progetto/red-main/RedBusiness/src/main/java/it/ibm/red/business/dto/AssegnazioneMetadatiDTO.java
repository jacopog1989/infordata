package it.ibm.red.business.dto;

/**
 * 
 * @author d.ventimiglia
 *
 */
public class AssegnazioneMetadatiDTO extends AbstractDTO {

	/**
	 * 	serial version uid.
	 */
	private static final long serialVersionUID = 2085755003920767733L;
	
	/**
	 * Identificativo.
	 */
	private Integer id;
	
	/**
	 * Tipologia documento.
	 */
	private TipologiaDocumentoDTO tipoDoc;
	
	/**
	 * Tipologia procedimento.
	 */
	private TipoProcedimentoDTO tipoProc;
	
	/**
	 * Metadato.
	 */
	private MetadatoDTO metadato;
	
	/**
	 * Assegnatario.
	 */
	private AssegnatarioDTO assegnatario;

	/**
	 * Costruttore di default, inizializza gli attributi in maniera da renderli null safe.
	 */
	public AssegnazioneMetadatiDTO() {
		super();
		//valorizzo con dati di default le informazioni
		// che dovranno essere inputate nella view
		setMetadato(new MetadatoDTO());
		setAssegnatario(new AssegnatarioDTO());
		setTipoDoc(new TipologiaDocumentoDTO());
		final TipoProcedimentoDTO tmp = new TipoProcedimentoDTO();
		tmp.setTipoProcedimentoId(-1);
		setTipoProc(tmp);
	}

	/**
	 * Costruttore che genera un oggetto a partire dagli attributi che definiscono completamente un'assegnazione automatica.
	 * @param tipoDoc
	 * @param tipoProc
	 * @param metadato
	 * @param assegnatario
	 */
	public AssegnazioneMetadatiDTO(final TipologiaDocumentoDTO tipoDoc, final TipoProcedimentoDTO tipoProc, final MetadatoDTO metadato, final AssegnatarioDTO assegnatario) {
		super();
		this.tipoDoc = tipoDoc;
		this.tipoProc = tipoProc;
		this.metadato = metadato;
		this.assegnatario = assegnatario;
	}

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(final Integer id) {
		this.id = id;
	}

	/**
	 * @return tipoDoc
	 */
	public TipologiaDocumentoDTO getTipoDoc() {
		return tipoDoc;
	}

	/**
	 * @param tipoDoc
	 */
	public void setTipoDoc(final TipologiaDocumentoDTO tipoDoc) {
		this.tipoDoc = tipoDoc;
	}

	/**
	 * @return tipoProc
	 */
	public TipoProcedimentoDTO getTipoProc() {
		return tipoProc;
	}

	/**
	 * @param tipoProc
	 */
	public void setTipoProc(final TipoProcedimentoDTO tipoProc) {
		this.tipoProc = tipoProc;
	}

	/**
	 * @return metadato
	 */
	public MetadatoDTO getMetadato() {
		return metadato;
	}

	/**
	 * @param metadato
	 */
	public void setMetadato(final MetadatoDTO metadato) {
		this.metadato = metadato;
	}

	/**
	 * @return assegnatario
	 */
	public AssegnatarioDTO getAssegnatario() {
		return assegnatario;
	}

	/**
	 * @param assegnatario
	 */
	public void setAssegnatario(final AssegnatarioDTO assegnatario) {
		this.assegnatario = assegnatario;
	}
}
