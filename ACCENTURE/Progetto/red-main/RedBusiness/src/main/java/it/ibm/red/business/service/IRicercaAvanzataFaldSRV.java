package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IRicercaAvanzataFaldFacadeSRV;

/**
 * The Interface IRicercaAvanzataFaldSRV.
 *
 * @author m.crescentini
 * 
 *         Interfaccia del servizio per la gestione della ricerca avanzata dei faldoni.
 */
public interface IRicercaAvanzataFaldSRV extends IRicercaAvanzataFaldFacadeSRV {
	
	
}