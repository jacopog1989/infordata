package it.ibm.red.business.enums;

/**
 * The Enum SalvaDocumentoErroreEnum.
 *
 * @author m.crescentini
 * 
 *         Enum codici di errore in fase di validazione/salvataggio di un
 *         documento.
 */
public enum SalvaDocumentoErroreEnum {

	/**
	 * Content del documento principale non presente.
	 */
	DOC_CONTENT_NON_PRESENTE(1, CampoMascheraEnum.DOC_NOME, "Documento non valido. Selezionare un nuovo documento."),

	/**
	 * Content dell'allegato non presente.
	 */
	ALLEGATO_CONTENT_NON_PRESENTE(2, CampoMascheraEnum.ALLEGATO_NOME, "Inserire il content dell'allegato."),

	/**
	 * Tipologia del documento allegato non presente.
	 */
	ALLEGATO_TIPOLOGIA_DOC_NON_PRESENTE(3, CampoMascheraEnum.ALLEGATO_TIPOLOGIA_DOC, "Inserire la tipologia del documento allegato."),

	/**
	 * Responsabile Copia Conforme.
	 */
	ALLEGATO_RESP_COPIA_CONFORME_NON_VALIDO(4, CampoMascheraEnum.ALLEGATO_RESPONSABILE_COPIA_CONFORME,
			"Il responsabile per la Copia Conforme deve essere lo stesso per tutti gli allegati."),

	/**
	 * Content type dell'allegato non valido per la Copia Conforme.
	 */
	ALLEGATO_MIMETYPE_COPIA_CONFORME_NON_VALIDO(5, CampoMascheraEnum.ALLEGATO_NOME, "Il formato dell'allegato per Copia Conforme non è ammesso."),

	/**
	 * Nome del file del documento principale non presente.
	 */
	DOC_NOME_FILE_NON_PRESENTE(6, CampoMascheraEnum.DOC_NOME, "Inserire il nome del file."),

	/**
	 * Tipologia Documento non presente.
	 */
	DOC_TIPOLOGIA_DOC_NON_PRESENTE(7, CampoMascheraEnum.DOC_TIPOLOGIA_DOC, "Specificare la tipologia del documento."),

	/**
	 * Tipo Procedimento non presente.
	 */
	DOC_TIPO_PROCEDIMENTO_NON_PRESENTE(8, CampoMascheraEnum.DOC_TIPO_PROCEDIMENTO, "Specificare il tipo di procedimento del documento."),

	/**
	 * Oggetto non presente.
	 */
	DOC_OGGETTO_NON_PRESENTE(9, CampoMascheraEnum.DOC_OGGETTO, "Oggetto non presente."),

	/**
	 * Mittente non presente.
	 */
	DOC_MITTENTE_NON_PRESENTE(10, CampoMascheraEnum.DOC_MITTENTE, "Inserire il mittente."),

	/**
	 * Indice di classifcazione non presente.
	 */
	DOC_INDICE_CLASSIFICAZIONE_NON_PRESENTE(11, CampoMascheraEnum.DOC_INDICE_CLASSIFICAZIONE, "Inserire l'indice di classificazione."),

	/**
	 * Tipo Assegnazione non presente.
	 */
	DOC_TIPO_ASSEGNAZIONE_NON_PRESENTE(12, CampoMascheraEnum.DOC_TIPO_ASSEGNAZIONE, "Inserire il tipo di assegnazione."),

	/**
	 * Responsabile Copia Conforme non presente.
	 */
	DOC_RESP_COPIA_CONFORME_NON_PRESENTE(13, CampoMascheraEnum.DOC_RESP_COPIA_CONFORME, "Inserire il Responsabile per la Copia Conforme."),

	/**
	 * Mail da protocollare già in fase di Protocollazione Manuale.
	 */
	MAIL_GIA_IN_PROTOCOLLAZIONE_MANUALE(14, null, null),

	/**
	 * Mail da protocollare già presa in carico da un altro utente.
	 */
	MAIL_GIA_PRESA_IN_CARICO(15, null, null),

	/**
	 * Tipologia Documento "Dichiarazione Servizi Resi".
	 */
	DOC_TIPOLOGIA_DOC_DSR(16, null, "Attenzione! Per la lavorazione della DSR utilizzare le funzionalità dedicate."),

	/**
	 * Documento con livello di riservatezza "Pubblico" in risposta ad un allaccio
	 * "Riservato".
	 */
	DOC_PUBBLICO_ALLACCIO_RISERVATO(17, CampoMascheraEnum.DOC_LIVELLO_RISERVATEZZA,
			"Attenzione! Sono stati allacciati dei procedimenti in ingresso riservati"
					+ " e la risposta risulta '\"Pubblica'\". Cliccare su '\"Ok'\" per proseguire o su '\"Annulla'\" per modificare il livello di riservatezza."),

	/**
	 * Se il flag "Firma Digitale RGS" è selezionato, tutti i destinatari devono
	 * avere mezzo di spedizione "ELETTRONICO".
	 */
	DOC_CHECK_FIRMA_RGS_DESTINATARI_CARTACEI(18, CampoMascheraEnum.DOC_DESTINATARI,
			"Attenzione! Se il documento deve essere firmato digitalmente, il mezzo di spedizione per tutti i destinatari esterni deve essere elettronico."),

	/**
	 * Se il documento in creazione è un Contributo Esterno (in uscita), tutti i
	 * destinatari devono avere mezzo di spedizione "ELETTRONICO".
	 */
	DOC_CONTRIBUTO_ESTERNO_DESTINATARI_CARTACEI(19, CampoMascheraEnum.DOC_DESTINATARI,
			"Attenzione! Se il documento deve essere un contributo esterno, il mezzo di spedizione per tutti i destinatari esterni deve essere elettronico."),

	/**
	 * Se i dati predefiniti mozione sono valorizzati, deve essere presente almeno
	 * un destinatario esterno con mezzo di spedizione "ELETTRONICO".
	 */
	DOC_DATI_PREDEFINITI_MOZIONE_DESTINATARIO_ELETTRONICO(20, CampoMascheraEnum.DOC_DESTINATARI,
			"Attenzione! È necessario inserire almeno un destinatario con mezzo di spedizione elettronico."
					+ " Per proseguire sarà quindi necessario modificare il mezzo di spedizione di almeno un destinatario."),

	/**
	 * Se l'iter approvativo è "FIRMA RAGIONEIRE GENERALE DELLO STATO", i mezzi di
	 * spedizione dei destinatari esterni devono essere omogenei.
	 */
	DOC_FLUSSO_RGS_DESTINATARI_MISTI(21, CampoMascheraEnum.DOC_DESTINATARI, "Il mezzo di spedizione per destinatari esterni deve essere omogeneo."),

	/**
	 * Il tipo procedimento è "ATTO DECRETO MANUALE", ma non è presente l'ID
	 * Raccolta FAD.
	 */
	DOC_ID_RACCOLTA_FAD_NON_PRESENTE(22, CampoMascheraEnum.DOC_ID_RACCOLTA_FAD,
			"Attenzione! Non è stata creata la raccolta provvisoria, si vuole procedere con la registrazione?"),

	/**
	 * Il tipo procedimento non è "ATTO DECRETO MANUALE", ma è presente l'ID
	 * Raccolta FAD.
	 */
	DOC_ID_RACCOLTA_FAD_PRESENTE(23, CampoMascheraEnum.DOC_ID_RACCOLTA_FAD,
			"Attenzione! Si sta procedendo alla modifica della tipologia documentale."
					+ "Si ricorda che potrebbe essere stato precendentemente assegnato un codice raccolta provvisoria (FAD). Si vuole procedere con la registrazione?"),

	/**
	 * È specificato un protocollo a cui si sta rispondendo, ma l'esistenza di tale
	 * protocollo nel sistema non è stata verificata.
	 */
	DOC_RISPOSTA_AL_PROTOCOLLO(24, CampoMascheraEnum.DOC_RISPOSTA_AL_PROTOCOLLO,
			"Verificare la correttezza del campo \"In risposta al protocollo\" prima di procedere" + " con la registrazione."),

	/**
	 * Il tipo assegnazione è "SPEDIZIONE", ma è presente almeno un destinatario
	 * interno.
	 */
	DOC_ASSEGNAZIONE_SPEDIZIONE_DESTINATARIO_INTERNO(25, CampoMascheraEnum.DOC_DESTINATARI,
			"Attenzione! Per l’assegnazione manuale per spedizione non è possibile" + " inserire destinatari interni. Modificare i destinatari per proseguire."),

	/**
	 * Il tipo assegnazione è "SPEDIZIONE", ma è presente almeno un destinatario
	 * elettronico.
	 */
	DOC_ASSEGNAZIONE_SPEDIZIONE_DESTINATARIO_ELETTRONICO(26, CampoMascheraEnum.DOC_DESTINATARI,
			"Attenzione! Sono presenti dei destinatari con mezzo di spedizione"
					+ " elettronico. Per l’iter selezionato, il mezzo di spedizione sarà convertito in cartaceo. Continuare con la registrazione?"),

	/**
	 * Numero RDP non presente.
	 */
	DOC_NUMERO_RDP_NON_PRESENTE(27, CampoMascheraEnum.DOC_NUMERO_RDP, "Inserire il Numero RDP."),

	/**
	 * Destinatari non presenti.
	 */
	DOC_DESTINATARI_NON_PRESENTI(28, CampoMascheraEnum.DOC_DESTINATARI, "Inserire almeno un destinatario."),

	/**
	 * Mezzo di spedizione per un destinatario esterno non presente.
	 */
	DOC_DESTINATARIO_MEZZO_SPEDIZIONE(29, CampoMascheraEnum.DOC_DESTINATARIO_MEZZO_SPEDIZIONE, "Mezzo di spedizione non valido."),

	/**
	 * Destinatario interno non valido.
	 */
	DOC_DESTINATARIO_INTERNO_NON_VALIDO(30, CampoMascheraEnum.DOC_DESTINATARI, "Destinatario interno non valorizzato."),

	/**
	 * Destinatario esterno non valido.
	 */
	DOC_DESTINATARIO_ESTERNO_NON_VALIDO(31, CampoMascheraEnum.DOC_DESTINATARI, "Destinatario esterno non valorizzato."),

	/**
	 * Tipologia del destinatario non valida.
	 */
	DOC_DESTINATARIO_TIPOLOGIA(32, CampoMascheraEnum.DOC_DESTINATARIO_TIPOLOGIA, "Tipologia del destinatario non valida."),

	/**
	 * Destinatario duplicato.
	 */
	DOC_DESTINATARIO_DUPLICATO(33, CampoMascheraEnum.DOC_DESTINATARI, "Destinatario duplicato."),

	/**
	 * Assegnazione duplicata.
	 */
	DOC_ASSEGNAZIONE_DUPLICATA(34, null, "Assegnazioni duplicate."),

	/**
	 * La data di scadenza inserita per il documento è antecedente alla data
	 * odierna.
	 */
	DOC_DATA_SCADENZA_PRECEDENTE_ODIERNA(35, CampoMascheraEnum.DOC_DATA_SCADENZA, "La data scadenza deve essere successiva o uguale alla data odierna."),

	/**
	 * La data di scadenza inserita per il documento è antecedente a quella inserita
	 * precedentemente (memorizzata nel workflow).
	 */
	DOC_DATA_SCADENZA_PRECEDENTE_WORKFLOW(36, CampoMascheraEnum.DOC_DATA_SCADENZA, "La data scadenza deve essere successiva o uguale alla data già inserita."),

	/**
	 * Il documento ha più di due stampigliature.
	 */
	DOC_APPROVAZIONI_NUM_STAMPIGLIATURE(37, CampoMascheraEnum.DOC_APPROVAZIONI, "Non è possibile apporre più di due stampigliature."),

	/**
	 * Barcode già presente.
	 */
	BARCODE_NON_VALIDO(38, null, "Il barcode inserito è già presente nel sistema."),

	/**
	 * Se l'assegnazione (manuale) del documento è per Copia Conforme, deve essere
	 * presente almeno un allegato per Copia Conforme.
	 */
	ALLEGATO_COPIA_CONFORME_NON_PRESENTE(39, CampoMascheraEnum.DOC_ALLEGATI, "Inserire almeno un allegato per Copia Conforme."),

	/**
	 * Se l'assegnazione (manuale) del documento è per Copia Conforme, il
	 * Responsabile per Copia Conforme del documento deve coincidere con il
	 * Responsabile per Copia Conforme degli allegati.
	 */
	DOC_RESP_COPIA_CONFORME_NEGLI_ALLEGATI(40, CampoMascheraEnum.DOC_ALLEGATI, "Il responsabile per Copia Conforme deve essere lo stesso negli allegati."),

	/**
	 * Se il documento è in ingresso, deve essere presente l'assegnatario per
	 * competenza.
	 */
	DOC_ING_ASSEGNATARIO_COMPETENZA_NON_PRESENTE(41, CampoMascheraEnum.DOC_ASSEGNATARIO_COMPETENZA, "Inserire l'assegnatario per Competenza."),

	/**
	 * Errore protocollo di emergenza duplicato.
	 */
	ERRORE_PROTOCOLLO_EMERGENZA_DUPLICATO(42, null, "Il protocollo di emergenza acquisito è già stato registrato per l'AOO"),

	/**
	 * Errore dati di emergenza mancanti.
	 */
	ERRORE_PROTOCOLLO_EMERGENZA_MANCANTE(43, null, "Il protocollo di emergenza è obbligatorio"),

	/**
	 * Se la tipologia documento è configurata per i Registri di Repertorio, i
	 * destinatari devono essere TUTTI interni.
	 */
	DOC_REGISTRO_REPERTORIO_DESTINATARI_ESTERNI(44, CampoMascheraEnum.DOC_DESTINATARI,
			"La tipologia documento è configurata per i Registri di Repertorio:" + " non possono essere presenti destinatari esterni"),

	/**
	 * Errore di documento non firmato nel caso di assegnazione per spedizione.
	 */
	DOC_NON_FIRMATO_ITER_SPEDIZIONE(45, CampoMascheraEnum.DOC_NOME,
			"Il documento per l'iter selezionato non risulta firmato.Selezionare un documento firmato o convertire i destinatari in cartacei"),

	/**
	 * Errore di documento non firmato nel caso di assegnazione per spedizione.
	 */
	DOC_FIRMATO_ITER_SPEDIZIONE(46, CampoMascheraEnum.DOC_NOME,
			"Il documento principale risulta firmato digitalmente. Selezionare un documento non firmato o inserire almeno un destinatario elettronico"),

	/**
	 * 
	 */
	STAMPIGLIATURA_SIGLA_ALLEGATO(47, null, "L'allegato non può essere stampigliato se è firmato digitalmente e se il check mantieni formato originale è attivo"),

	/**
	 * 
	 */
	P7M_ERROR_SIGLA_ALLEGATO(48, null, "Non è possibile caricare un file p7m e inserire la stampigliatura del segno grafico"),

	/**
	 * 
	 */
	PDF_MANTIENI_FORMATO_ERROR_SIGLA_ALLEGATO(49, null,
			"Non è possibile caricare un allegato non PDF, indicarlo come da firmare se questo viene mantenuto in formato originale e inserire la stampigliatura del segno grafico"),

	/**
	 * 
	 */
	DOC_ASSEGNAZIONI_NON_PRESENTI(50, null, "Nessuna assegnazione presente."),

	/**
	 * 
	 */
	ATTO_DECRETO_ALLEGATI_NON_VALIDI(51, null, "Non è possibile procedere con la creazione, tra gli allegati sono presenti delle estensioni non conformi."),

	/**
	 * Firma visibile.
	 */
	FIRMA_VISIBILE_FORMATO_ELETTRONICO_FIRMATO_DIGITALMENTE(52, null,
			"Non è possibile selezionare firma visibile su documenti non in formato elettronico o firmato digitalmente."),

	/**
	 * Mantieni formato originale.
	 */
	FIRMA_VISIBILE_MANTIENI_FORMATO_ORIGINALE(53, null, "Non è possibile selezionare firma visibile per un documento in formato originale."),
	
	/**
	 * Mancata assegnazione ad utente per un documento con capitolo di spesa riservato.
	 */
	MANCATA_ASSEGNAZIONE_UTENTE_CAPITOLO_SPESA_RISERVATO(54, null, "Un documento avente un capitolo di spesa riservato deve essere assegnato per competenza automaticamente ad un utente."),

	/**
	 * Errore nelle operazioni di tracciamento del documento (sottoscrizioni).
	 */
	ERRORE_TRACCIA_PROCEDIMENTO(999, null, "Si è verificato un errore durante l'operazione di 'Traccia Procedimento'."),

	/**
	 * Errore generico, il messaggio di errore specifico dovrebbe essere presente
	 * nelle note di EsitoSalvaDocumentoDTO.
	 */
	ERRORE_GENERICO(1000, null, "Si è verificato un errore durante la registrazione del documento.");

	/**
	 * Codice errore.
	 */
	private int codice;

	/**
	 * Campo della maschera di input a cui fa riferimento l'errore.
	 */
	private CampoMascheraEnum campo;

	/**
	 * Messaggio errore.
	 */
	private String messaggio;

	/**
	 * Costruttore.
	 * 
	 * @param inCodice    codice errore
	 * @param inCampo     campo della maschera di input a cui fa riferimento
	 *                    l'errore
	 * @param inMessaggio messaggio errore
	 */
	SalvaDocumentoErroreEnum(final int inCodice, final CampoMascheraEnum inCampo, final String inMessaggio) {
		codice = inCodice;
		campo = inCampo;
		messaggio = inMessaggio;
	}

	/**
	 * Getter codice errore.
	 * 
	 * @return codice errore
	 */
	public int getCodice() {
		return codice;
	}

	/**
	 * Getter campo.
	 * 
	 * @return campo
	 */
	public CampoMascheraEnum getCampo() {
		return campo;
	}

	/**
	 * Getter messaggio errore.
	 * 
	 * @return messaggio errore
	 */
	public String getMessaggio() {
		return messaggio;
	}

	/**
	 * Enum campo maschera.
	 */
	public enum CampoMascheraEnum {

		/**
		 * Valore.
		 */
		DOC_MITTENTE,

		/**
		 * Valore.
		 */
		DOC_DESTINATARI,

		/**
		 * Valore.
		 */
		DOC_DESTINATARIO_MEZZO_SPEDIZIONE,

		/**
		 * Valore.
		 */
		DOC_DESTINATARIO_TIPOLOGIA,

		/**
		 * Valore.
		 */
		DOC_NOME,

		/**
		 * Valore.
		 */
		DOC_TIPOLOGIA_DOC,

		/**
		 * Valore.
		 */
		DOC_TIPO_PROCEDIMENTO,

		/**
		 * Valore.
		 */
		DOC_ASSEGNATARIO_COMPETENZA,

		/**
		 * Valore.
		 */
		DOC_NUMERO_RDP,

		/**
		 * Valore.
		 */
		DOC_OGGETTO,

		/**
		 * Valore.
		 */
		DOC_INDICE_CLASSIFICAZIONE,

		/**
		 * Valore.
		 */
		DOC_RESP_COPIA_CONFORME,

		/**
		 * Valore.
		 */
		DOC_TIPO_ASSEGNAZIONE,

		/**
		 * Valore.
		 */
		DOC_LIVELLO_RISERVATEZZA,

		/**
		 * Valore.
		 */
		DOC_ID_RACCOLTA_FAD,

		/**
		 * Valore.
		 */
		DOC_RISPOSTA_AL_PROTOCOLLO,

		/**
		 * Valore.
		 */
		DOC_DATA_SCADENZA,

		/**
		 * Valore.
		 */
		DOC_APPROVAZIONI,

		/**
		 * Valore.
		 */
		DOC_ALLEGATI,

		/**
		 * Valore.
		 */
		ALLEGATO_NOME,

		/**
		 * Valore.
		 */
		ALLEGATO_TIPOLOGIA_DOC,

		/**
		 * Valore.
		 */
		ALLEGATO_BARCODE,

		/**
		 * Valore.
		 */
		ALLEGATO_RESPONSABILE_COPIA_CONFORME;
	}
}
