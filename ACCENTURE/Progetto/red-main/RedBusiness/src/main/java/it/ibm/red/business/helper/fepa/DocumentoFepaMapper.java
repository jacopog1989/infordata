package it.ibm.red.business.helper.fepa;

import java.io.IOException;
import java.util.Date;

import org.apache.commons.io.IOUtils;

import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.Document;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;

/**
 * Mapper documento FEPA.
 */
public class DocumentoFepaMapper {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DocumentoFepaMapper.class.getName());

	/**
	 * Properties.
	 */
	private final PropertiesProvider pp;

	/**
	 * Costruttore della classe.
	 */
	public DocumentoFepaMapper(){
		this.pp = PropertiesProvider.getIstance();
	}

	/**
	 * Restituisce un documento FEPA (@see DocumentoFepa) lavorado un documento 
	 * (@see Document) e mappandone i valori.
	 * @param doc
	 * @param tipoDocumento
	 * @param utenteFirma
	 * @param utenteUfficioTerzo
	 * @return documento fepa
	 * @throws IOException
	 */
	public DocumentoFepa getDocumentoFepa(final Document doc, final String tipoDocumento, final String utenteFirma, final String utenteUfficioTerzo) throws IOException{
		final DocumentoFepa docFepa = this.newInstanceDocumentoFepa(doc);
		docFepa.setTipoDocumento(tipoDocumento);
		if (Constants.Varie.TIPO_DOCUMENTO_GENERICO_FEPA.equals(tipoDocumento)) {
			docFepa.setDatadecreto((Date)TrasformerCE.getMetadato(doc, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY));
			docFepa.setFirmatario(utenteFirma);
			docFepa.setNumDecreto((Integer)TrasformerCE.getMetadato(doc, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY));
		}
		if (Constants.Varie.TIPO_DOCUMENTO_LETTERA_SERVIZI_RESI_FEPA.equals(tipoDocumento)) {
			docFepa.setFirmatario(utenteUfficioTerzo);
		}
		return docFepa;
	}

	/**
	 * Restituisce un documento FEPA (@see DocumentoFepa) lavorado un documento 
	 * (@see Document) e mappandone i valori.
	 * @param doc
	 * @return documento FEPA
	 * @throws IOException
	 */
	public DocumentoFepa getDocumentoFepa(final Document doc) throws IOException{
		final DocumentoFepa docFepa = this.newInstanceDocumentoFepa(doc);
		if (doc.getProperties().isPropertyPresent(pp.getParameterByKey(PropertiesNameEnum.FEPA_CODICE_TIPO_DOCUMENTO_METAKEY)) && doc.getProperties().getObjectValue(pp.getParameterByKey(PropertiesNameEnum.FEPA_CODICE_TIPO_DOCUMENTO_METAKEY))!=null){
			docFepa.setTipoDocumento((String)TrasformerCE.getMetadato(doc, PropertiesNameEnum.FEPA_CODICE_TIPO_DOCUMENTO_METAKEY));
			LOGGER.info(" Tipologia Allegato: "+docFepa.getTipoDocumento());
		} else {
			docFepa.setTipoDocumento(Constants.Varie.TIPO_DOCUMENTO_DOCUMENTAZIONE_ALLEGATA_FEPA);
			LOGGER.info("codiceTipoDocumentoFepa non rilevato su documento " + doc.get_Name() +", impostato valore di default: "+Constants.Varie.TIPO_DOCUMENTO_DOCUMENTAZIONE_ALLEGATA_FEPA);
		}
		return docFepa;
	}
	
	private DocumentoFepa newInstanceDocumentoFepa(final Document doc) throws IOException{
		final DocumentoFepa docFepa = new DocumentoFepa();
		docFepa.setIdDocumentoFepa(it.ibm.red.business.utils.StringUtils.cleanGuidToString(doc.get_Id().toString()));
		final ContentTransfer ceContent = (ContentTransfer) doc.get_ContentElements().get(0);
		
		docFepa.setContent(IOUtils.toByteArray(ceContent.accessContentStream()));
		docFepa.setContentType(ceContent.get_ContentType());
		docFepa.setDescrizione((String)TrasformerCE.getMetadato(doc, PropertiesNameEnum.OGGETTO_METAKEY));
		docFepa.setFilename((String)TrasformerCE.getMetadato(doc, PropertiesNameEnum.NOME_FILE_METAKEY));
		docFepa.setDataCreazione(doc.get_DateCreated());
		docFepa.setDocumentTitle((String)TrasformerCE.getMetadato(doc, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
		return docFepa;
	}
	
}
