package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.FascicoloRedFnDTO;
import it.ibm.red.business.enums.EsitoFaldoneEnum;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.service.facade.IFaldoneFacadeSRV;

/**
 * The Interface IFaldoneSRV.
 *
 * @author mcrescentibi
 * 
 *         Interfaccia servizio faldone
 */
public interface IFaldoneSRV extends IFaldoneFacadeSRV {
	
	/**
	 * Faldona i fascicoli.
	 * @param nomeFaldone
	 * @param fascicoli
	 * @param idAoo
	 * @param fceh
	 */
	void faldonaFascicoli(String nomeFaldone, List<FascicoloRedFnDTO> fascicoli, Long idAoo, IFilenetCEHelper fceh);

	/**
	 * Faldona il fascicolo.
	 * @param nomeFaldone
	 * @param idFascicolo
	 * @param idAoo
	 * @param fceh
	 * @return true o false
	 */
	boolean faldonaFascicolo(String nomeFaldone, String idFascicolo, Long idAoo, IFilenetCEHelper fceh);
	
	/**
	 * Verifica se l'associazione al faldone può essere rimossa.
	 * @param nomeFaldone
	 * @param idFascicolo
	 * @param idAOO
	 * @param fceh
	 * @return true o false
	 */
	boolean isFaldoneDisassociabile(String nomeFaldone, String idFascicolo, Long idAOO, IFilenetCEHelper fceh);
	
	/**
	 * Rimuove l'associazione del fascicolo al faldone.
	 * @param nomeFaldone
	 * @param idFascicolo
	 * @param idAOO
	 * @param fceh
	 * @return esito
	 */
	EsitoFaldoneEnum disassociaFascicoloDaFaldone(String nomeFaldone, String idFascicolo, Long idAOO, IFilenetCEHelper fceh);
	
	/**
	 * Rimuove l'associazione del fascicolo al faldone.
	 * @param nomeFaldone
	 * @param idFascicolo
	 * @param idAOO
	 * @param checkDisassociazione
	 * @param fceh
	 * @return esito
	 */
	EsitoFaldoneEnum disassociaFascicoloDaFaldone(String nomeFaldone, String idFascicolo, Long idAOO, boolean checkDisassociazione, IFilenetCEHelper fceh);

	/**
	 * Ottiene il faldone OPF IGEPA.
	 * @param oggettoFaldone
	 * @param annoFaldone
	 * @param utente
	 * @return nome del faldone
	 */
	String getFaldoneOPFIgepa(String oggettoFaldone, String annoFaldone, UtenteDTO utente);
	
	/**
	 * Ottiene la gerarchia del faldone.
	 * @param faldone
	 * @param utente
	 * @param fcehAdmin
	 * @return gerarchia del faldone
	 */
	String getGerarchiaFaldone(FaldoneDTO faldone, UtenteDTO utente, IFilenetCEHelper fcehAdmin); 
	
	/**
	 * Ottiene il faldone.
	 * @param documentTitle
	 * @param idAoo
	 * @param fcehAdmin
	 * @return faldone
	 */
	FaldoneDTO getFaldone(String documentTitle, Long idAoo, IFilenetCEHelper fcehAdmin);
	
	/**
	 * Crea il faldone.
	 * @param utente
	 * @param oggetto
	 * @param descrizione
	 * @param faldonePadre
	 * @param canBeDuplicated
	 * @param con
	 * @param fceh
	 * @return nome del faldone
	 */
	String createFaldone(UtenteDTO utente, String oggetto, String descrizione, String faldonePadre, Boolean canBeDuplicated, Connection con, IFilenetCEHelper fceh);

	/**
	 * Cancella il faldone.
	 * @param utente
	 * @param idUfficioSelezionato
	 * @param pathFaldone
	 * @param documentTitle
	 * @param fceh
	 */
	void delete(UtenteDTO utente, Integer idUfficioSelezionato, String pathFaldone, String documentTitle, IFilenetCEHelper fceh);
	
}
