package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import it.ibm.red.business.dto.MessaggioEmailDTO;
import it.ibm.red.business.dto.NotificaEmailDTO;
import it.ibm.red.business.enums.StatoCodaEmailEnum;
import it.ibm.red.business.enums.TipoDestinatarioDatiCertEnum;
import it.ibm.red.business.persistence.model.CodaEmail;
/**
 * 
 * @author CPIERASC
 *
 *	Dao per la gestione della notifica delle mail.
 */
public interface IGestioneNotificheEmailDAO extends Serializable {
	
	/**
	 * Metodo per inserire una notifica mail.
	 * 
	 * @param codaEmail		item da inserire
	 * @param connection	connessione
	 * @return				id della notifica registrata
	 */
	long insertNotificaEmail(CodaEmail codaEmail, Connection connection);

	/**
	 * Il metodo recupera gli item dalla coda lockandoli sulla base dati.
	 * 
	 * @param idAoo
	 * @param isPostaInterna
	 * @param numItemToSend
	 * @param connection
	 * @return
	 */
	List<CodaEmail> lockItemDaSpedire(int idAoo, boolean isPostaInterna, Integer numItemToSend, Connection connection);
	
	/**
	 * Il metodo cambia lo stato degli item in input impostandolo a stato.
	 * 
	 * @param items
	 * @param stato
	 * @param connection
	 * @return
	 */
	void updateStatoRicevuta(List<Integer> items, StatoCodaEmailEnum stato, Connection connection);
	

	/**
	 * Il metodo aggiorna la coda mail identificata da idNotifica con i valori in input.
	 * 
	 * @param statoRicevuta
	 * @param content
	 * @param msgId
	 * @param dataSpedizione
	 * @param idNotifica
	 * @param idSpedizioneUnica
	 * @param spedizioneUnica
	 * @param idSpedizioneNps
	 * @param updateCountRetry
	 * @param connection
	 * @return
	 */
	int aggiornaCodaMail(int statoRicevuta, byte[] content, String msgId, Date dataSpedizione, Integer idNotifica, Integer idSpedizioneUnica, 
			boolean spedizioneUnica, Integer idSpedizioneNps, boolean updateCountRetry, Connection connection);

	/**
	 * Il metodo aggiorna il solo stato ricevuta per l'item con iddocumento in input.
	 * @param connection
	 * @param idDocumento
	 * @param statoRicevuta
	 * @return
	 */
	Integer updateStatoRicevuta(Connection connection, Integer idDocumento, Integer statoRicevuta);
	
	/**
	 * metodo per recuperare una lista di notifiche tramite un Document Title.
	 * 
	 * @param documentTitle
	 * @param connection
	 * @return
	 */
	Collection<CodaEmail> getNotificheEmailByIdDocumento(String documentTitle, Connection connection);

	/**
	 * Esegue l'update dell'id di spedizione unica delle notifiche email.
	 * @param idNotificaToUpdateList
	 * @param idNotificaSpedizioneUnica
	 * @param con
	 * @return stato notifica email
	 */
	int updateSpedizioneUnicaNotificaEmail(List<Long> idNotificaToUpdateList, long idNotificaSpedizioneUnica, Connection con);

	/**
	 * Ottiene le notifiche email per stato non ricevuta.
	 * @param statiDaEscludere
	 * @param idAoo
	 * @param con
	 * @return lista di code mail
	 */
	List<CodaEmail> getNotificheEmailByNoStatoRicevuta(List<Integer> statiDaEscludere, int idAoo, Connection con);

	/**
	 * Ottiene ed elimina i documenti chiusi.
	 * @param idAoo
	 * @param conn
	 * @return indice
	 */
	String getAndDeleteClosedDocument(int idAoo, Connection conn);

	/**
	 * Recupera gli id documento relativi a item non chiusi con destinatari cartacei per i quali non esistano item con destinatari pec o peo.
	 * 
	 * @param connection
	 * @param idAoo
	 * @return
	 */
	List<String> getIdDocumentiNonChiusiDestOnlyCartacei(Connection connection, int idAoo);

	/**
	 * Esegue l'update dello stato della notifica email.
	 * @param idDocumento
	 * @param idMessaggio
	 * @param idDestinatario
	 * @param statoRicevutaNew
	 * @param tipoDestinatario
	 * @param con
	 * @return stato aggiornato
	 */
	int updateStatoNotificaEmail(String idDocumento, String idMessaggio, long idDestinatario, int statoRicevutaNew, Connection con);


	/**
	 * Recupera il numero di item in errore per il documento in input.
	 * 
	 * @param documentTitle
	 * @param connection
	 * @return
	 */
	int getNumErroriSpedizione(String documentTitle, Connection connection);

	/**
	 * Ottiene le notifiche email tramite l'id documento e l'email del destinatario.
	 * @param idDocumento
	 * @param emailDestinatario
	 * @param con
	 * @return coda mail
	 */
	CodaEmail getNotificheEmailByIdDocEmailDest(String idDocumento, String emailDestinatario, Connection con);
	
	/**
	 * Ottiene le notifiche email tramite l'id documento, l'email del destinatario e lo stato.
	 * @param idDocumento
	 * @param emailDestinatario
	 * @param stato
	 * @param con
	 * @return coda mail
	 */
	CodaEmail getNotificheEmailByIdDocEmailDestAndStato(String idDocumento, String emailDestinatario, Integer stato, Connection con);

	/**
	 * Aggiorna la notifica email con stato spedito.
	 * @param idNotifica
	 * @param spedito
	 * @param con
	 * @return stato aggiornato
	 */
	int updateSpeditoNotificaEmail(int idNotifica, int spedito, Connection con);
	
	/**
	 * Esegue il rollback del batch di gestione notifiche.
	 * @param idDocumentToRollback
	 * @param con
	 */
	void rollbackGestioneNoticheBatch(List<Long> idDocumentToRollback, Connection con);

	/**
	 * Conta i documenti in errore di spedizione.
	 * @param documentTitleList
	 * @param connection
	 * @return numero di documenti in errore di spedizione
	 */
	int getNumErroriSpedizioneCoda(List<String> documentTitleList, Connection connection);

	/**
	 * Esegue l'update del tipo destinatario della notifica email.
	 * @param tipoDestinatario
	 * @param messageId
	 * @param mittente
	 * @param con
	 * @return tipo destinatario aggiornato
	 */
	int updateTipoDestinatario(TipoDestinatarioDatiCertEnum tipoDestinatario, String messageId, String mittente, Connection con);

	/**
	 * Esegue l'update della colonna SPEDIZIONENPS delle notifiche email.
	 * @param idNotificaToUpdateList
	 * @param idNotificaSpedizioneNps
	 * @param con
	 * @return stato aggiornato
	 */
	int updateSpedizioneNps(List<Long> idNotificaToUpdateList, long idNotificaSpedizioneNps, Connection con);

	/**
	 * Ottiene i documenti in errore di spedizione.
	 * @param documentTitleList
	 * @param connection
	 * @return lista dei document title dei documenti in errore di spedizione
	 */
	List<String> getDocInErroreCodaSpedizione(List<String> documentTitleList, Connection connection);
 
	/**
	 * Ottiene il widget delle info di spedizione.
	 * @param documentTitleList
	 * @param connection
	 * @return lista dei document title dei documenti
	 */
	List<String> getInfoSpedizioneWidget(List<String> documentTitleList, Connection connection);
	
	/**
	 * Aggiorna lo stato della notifica di spedizione.
	 * @param idNotificaSped
	 * @param conn
	 * @return
	 */
	int updateNotificaSpedizione(Long idNotificaSped, Connection conn);

	/**
	 * Aggiorna codi message di gestione delle notifiche email.
	 * @param codiMessaggio
	 * @param idNotifica
	 * @param conn
	 * @return true o false
	 */
	boolean updateCodiMessageGestioneNotificheEmail(String codiMessaggio, Long idNotifica, Connection conn); 
  
	/**
	 * Ottiene lo stato email tramite codi message.
	 * @param codiMessaggio
	 * @param stato
	 * @param conn
	 * @return messaggio email
	 */
	MessaggioEmailDTO getStatoEmailByCodiMessage(String codiMessaggio, Integer stato, Connection conn);

	/**
	 * Restituisce una lista di notifiche email nello stato identificato da <code> statoRicevuta </code>.
	 * @param idAoo
	 * @param statoRicevuta
	 * @param giorniNotificaMancataSpedizione
	 * @param con
	 * @return lista notifiche email
	 */
	List<NotificaEmailDTO> getNotificheByStatoRicevuta(Long idAoo, Integer statoRicevuta, Integer giorniNotificaMancataSpedizione, Connection con);

	/**
	 * Restituisce una lista di id dei destinatari notifiche nello stato Ricevuta.
	 * @param idAoo
	 * @param idDocumento
	 * @param con
	 * @return lista destinatari
	 */
	List<String> getDestinatariNotificheByStatoRicevuta(Long idAoo, String idDocumento, Connection con);

	/**
	 * Restituisce una lista delle notifiche nello stato Ricevuta diverso da 3 associate ai documenti identificiati
	 * dalla lista <code> docTitleSpeditiPerUfficio </code>.
	 * @param docTitleSpeditiPerUfficio
	 * @param conn
	 * @return lista id documenti
	 */
	List<String> getStatoRicevutaDiversoDa3(List<String> docTitleSpeditiPerUfficio ,Connection conn);

	/**
	 * Effettua l'update del processamento notifica.
	 * @param idAoo
	 * @param idDocumento
	 * @param conn
	 * @return true se l'update viene effettuato, false altrimenti
	 */
	boolean upateDataProcessamentoByDT(Long idAoo, String idDocumento, Connection conn);

	/**
	 * Restituisce i document title dei record già processati.
	 * @param docTitleSpeditiPerUfficio
	 * @param conn
	 * @return lista di document title
	 */
	List<String> getRecordGiaProcessati(List<String> docTitleSpeditiPerUfficio, Connection conn);

	/**
	 * Effettua la insert dei record già processati.
	 * @param idAoo
	 * @param documentTitle
	 * @param stato
	 * @param conn
	 * @return true se viene effettuata la insert, false altrimenti
	 */
	boolean insertRecordProcessati(Long idAoo, String documentTitle, int stato, Connection conn);

	/**
	 * Elimina i record processati da una settimana.
	 * @param idAoo
	 * @param conn
	 * @return true se viene fatto l'update, false altrimenti
	 */
	boolean deleteRecordProcessatiDaUnaSettimana(Long idAoo, Connection conn);

	/**
	 * Restituisce le notifiche mail per l'icona stato.
	 * @param documentTitle
	 * @param idAoo
	 * @param connection
	 * @return lista CodaEmail
	 */
	Collection<CodaEmail> getNotificheEmailForIconaStato(String documentTitle, Long idAoo, Connection connection);

	/**
	 * Restituisce il tipo della casella di posta mittente. 
	 * 1 se è peo 2 se è pec.
	 * @param mittente
	 * @param conn
	 * @return
	 */
	Integer getTipoMittenteCasellaPostale(String mittente, Connection conn);

	/**
	 * Delete by idaoo and document title 
	 * @param idAoo
	 * @param documentTitle 
	 * @param conn
	 * @return
	 */
	boolean deleteByIdAooAndDT(Long idAoo, String documentTitle,  Connection conn);

	/**
	 * Get id destinatario from guid
	 * @param idMessaggio
	 * @param emailMittente 
	 * @param conn
	 * @return
	 */
	Long getIdDestinatarioFromGuid(String idMessaggio, String emailMittente, Connection conn);
	
	/**
	 * Recupera le notifiche per la gestione dell'icona stato reinvia.
	 * 
	 * @param documentTitle
	 * @param idAoo
	 * @param connection
	 * @return
	 */
	Collection<CodaEmail> getNotificheEmailForIconaStatoReinvia(String documentTitle, Long idAoo, Connection connection);
	
	/**
	 * Recupera mail dato un document title e la data di richiesta spedizione
	 * 
	 * @param idDocumento documentTitle se mail non inoltrata, idDocumento generato all'inolto o al rifiuta se inoltrata o rifiutata
	 * @param dataRichiestaSpedizione
	 * @param connection
	 * */
	MessaggioEmailDTO getMail(final String idDocumento, final Date dataRichiestaSpedizione, final Connection connection);
}