package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IAttiFacadeSRV;

/**
 * Interface service ATTI.
 */
public interface IAttiSRV extends IAttiFacadeSRV {

}