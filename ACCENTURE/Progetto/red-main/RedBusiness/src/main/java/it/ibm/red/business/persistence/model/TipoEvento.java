package it.ibm.red.business.persistence.model;

import java.io.Serializable;

/**
 * The Class TipoEvento.
 *
 * @author CPIERASC
 * 
 *         Entità che mappa la tabella TIPOEVENTO.
 */
public class TipoEvento implements Serializable {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 8519525733039157864L;
	
	/**
	 * Identificativo tipo evento.
	 */
	private int idTipoEvento;

	/**
	 * Descrizione.
	 */
	private String descrizione;

	/**
	 * Flag default.
	 */
	private Long flagDefault;

	/**
	 * Tipologia richiesta.
	 */
	private Long tipologiaRichiesta;

	/**
	 * Costruttore.
	 * 
	 * @param inIdTipoEvento		identificativo tipo evento
	 * @param inDescrizione			descrizione
	 * @param inFlagDefault			flag default
	 * @param inTipologiaRichiesta	identificativo tipologia richiesta
	 */
	public TipoEvento(final int inIdTipoEvento, final String inDescrizione, final Long inFlagDefault, final Long inTipologiaRichiesta) {
		super();
		this.idTipoEvento = inIdTipoEvento;
		this.descrizione = inDescrizione;
		this.flagDefault = inFlagDefault;
		this.tipologiaRichiesta = inTipologiaRichiesta;
	}
	
	/**
	 * Getter identificativo.
	 * 
	 * @return	identificativo
	 */
	public final int getIdTipoEvento() {
		return idTipoEvento;
	}

	/**
	 * Getter descrizione.
	 * 
	 * @return	descrizione
	 */
	public final String getDescrizione() {
		return descrizione;
	}
	
	/**
	 * Getter flag default.
	 * 
	 * @return	flag default
	 */
	public final Long getFlagDefault() {
		return flagDefault;
	}

	/**
	 * Getter identificativo tipologia richiesta.
	 * 
	 * @return	identificativo tipologia richiesta
	 */
	public final Long getTipologiaRichiesta() {
		return tipologiaRichiesta;
	}

}
