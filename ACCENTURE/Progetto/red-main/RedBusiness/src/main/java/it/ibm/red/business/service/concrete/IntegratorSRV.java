package it.ibm.red.business.service.concrete;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.core.CustomObject;
import com.filenet.api.property.Properties;

import it.ibm.red.business.dto.SicogeResultDTO;
import it.ibm.red.business.dto.SigiResultDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IIntegratorSRV;
import it.ibm.red.business.service.facade.IAooFacadeSRV;
import it.ibm.sicoge.DettaglioOPDTO;
import it.ibm.sicoge.ListaOPDTO;
import it.ibm.sicoge.StatoEnum;
import it.ibm.sigi.BusinessDelegate;
import it.ibm.sigi.OrdinePagamentoDTO;

/**
 * Service integrator.
 */
@Service
public class IntegratorSRV extends AbstractService implements IIntegratorSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 7893836641005179923L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(IntegratorSRV.class.getName());

	/**
	 * Service.
	 */
	@Autowired
	private IAooFacadeSRV aooSRV;

	/**
	 * Properties.
	 */
	private final PropertiesProvider pp = PropertiesProvider.getIstance();

	/**
	 * Restituisce l'helper PE filenet.
	 * @param idAoo
	 * @return FilenetPEHelper
	 */
	private FilenetPEHelper getFilenetPEHelper(final Integer idAoo) {
		final Aoo aoo = aooSRV.recuperaAoo(idAoo);
		final AooFilenet aooFilenet = aoo.getAooFilenet();
		return new FilenetPEHelper(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(), aooFilenet.getConnectionPoint());
	}

	/**
	 * Restituisce l'interfaccia dell'helper PE filenet.
	 * @param idAoo
	 * @return IFilenetCEHelper
	 */
	private IFilenetCEHelper getIFilenetHelper(final Integer idAoo) {
		final Aoo aoo = aooSRV.recuperaAoo(idAoo);
		final AooFilenet aooFilenet = aoo.getAooFilenet();
		return FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
				aooFilenet.getObjectStore(), idAoo.longValue());
	}

	/**
	 * Valida l'AOO.
	 * @param idAoo
	 */
	private void validateAOO(final Integer idAoo) {
		if (idAoo == null || !idAoo.equals(Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.IDAOO_RGS)))) {
			throw new RedException("Funzionalità abilitata solamente per l'aoo RGS");
		}
	}

	/**
	 * SICOGE
	 */
	@Override
	public SicogeResultDTO syncSICOGE(final Integer idAoo) {
		final SicogeResultDTO out = new SicogeResultDTO();
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;

		try {
			fpeh = getFilenetPEHelper(idAoo);
			fceh = getIFilenetHelper(idAoo);

			validateAOO(idAoo);

			// Recupero id decreti da lavorare
			final List<String> ids = getFilenetPEHelper(idAoo).fetchIdDecretiDaLavorare();

			for (final String idDecretoDaLavorare : ids) {
				populateSicogeResult(out, fceh, idDecretoDaLavorare);
			}
		} catch (final Exception e) {
			LOGGER.warn(e);
		} finally {
			popSubject(fceh);
			logoff(fpeh);
		}

		return out;
	}

	/**
	 * @param out
	 * @param fceh
	 * @param idDecretoDaLavorare
	 */
	private void populateSicogeResult(final SicogeResultDTO out, final IFilenetCEHelper fceh, final String idDecretoDaLavorare) {
		try {
			// Recupero gli OP del decreto (precedentemente importati invocando SIGI)
			final List<Properties> propsOPS = fceh.fetchOPProperties(idDecretoDaLavorare);
			if (propsOPS != null) {
				for (final Properties propsOP : propsOPS) {
					// Per ciascun OP vado a recuperare le informazioni su SICOGE aggiornando quelle
					// gia in nostro possesso, ma soprattutto aggiungendo l'id del fascicolo fepa
					final String idOP = propsOP.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.FEPA_METADATO_NUMERO_OP)) + "";
					final Boolean resultOP = getResult(fceh, propsOP);
					out.add(idDecretoDaLavorare, idOP, resultOP);
				}
			}
		} catch (final Exception e) {
			LOGGER.warn(e);
		}
	}

	/**
	 * @param fceh
	 * @param propsOP
	 * @return result OP
	 */
	private Boolean getResult(final IFilenetCEHelper fceh, final Properties propsOP) {
		Boolean resultOP = false;
		try {
			resultOP = handleOP(fceh, propsOP);
		} catch (final Exception e) {
			LOGGER.warn(e);
		}
		return resultOP;
	}

	/**
	 * Gestisce il fascicolo OP.
	 * @param fceh
	 * @param propsOP
	 * @return esito gestione
	 */
	private Boolean handleOP(final IFilenetCEHelper fceh, final Properties propsOP) {
		Boolean out = false;
		
		final Integer esercizio = propsOP.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.FEPA_METADATO_ESERCIZIO));
		final String amministraz = propsOP.getStringValue(pp.getParameterByKey(PropertiesNameEnum.FEPA_METADATO_AMMINISTRAZ));
		final String codiceRagioneria = StringUtils.leftPad(propsOP.getStringValue(pp.getParameterByKey(PropertiesNameEnum.FEPA_METADATO_CODICE_RAGIONERIA)), 4, "0");
		final String capitolo = propsOP.getStringValue(pp.getParameterByKey(PropertiesNameEnum.FEPA_METADATO_CAPITOLO_NUMERI_SIRGS));
		final String numeroOP = StringUtils.leftPad(propsOP.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.FEPA_METADATO_NUMERO_OP)) + "", 7, "0");

		final String applicazione = pp.getParameterByKey(PropertiesNameEnum.SICOGE_HEADER_APPLICAZIONE);
		final String utente = pp.getParameterByKey(PropertiesNameEnum.SICOGE_HEADER_UTENTE);
		final String password = pp.getParameterByKey(PropertiesNameEnum.SICOGE_HEADER_PASSWORD);
		final String urlSRV = pp.getParameterByKey(PropertiesNameEnum.SICOGE_ENDPOINT);

		final ListaOPDTO response = it.ibm.sicoge.BusinessDelegate.listaOP(urlSRV, applicazione, utente, password, esercizio, amministraz, codiceRagioneria, capitolo,
				capitolo, numeroOP, numeroOP);

		if (response != null && StatoEnum.OK.equals(response.getStato())) {
			if (response.getDettagliOP() != null) {
				for (final DettaglioOPDTO op : response.getDettagliOP()) {
					fceh.saveOPPerFascicoloFepa(op.getNumeroOP(), op.getEsercizio(), op.getAmministrazione(), op.getRagioneria(), op.getCapitolo(), op.getPianoGestione(),
							op.getBeneficiario(), op.getTipoTitolo(), op.getOggettoSpese(), op.getDataEmissione(), op.getIdFascicolo());
				}
			}
			out = true;
		} else {

			if (response == null) {
				LOGGER.error("Oggetto ListaOPDTO non inizializzato prima della chiamata ad un metodo della classe.");
				throw new RedException("Oggetto ListaOPDTO non inizializzato prima della chiamata ad un metodo della classe.");
			}

			LOGGER.warn("LA CHIAMATA VERSO SICOGE PER [NUMERO OP = " + numeroOP + "] HA RESTITUITO [ESITO = " + (response.getStato() != null ? response.getStato() : "ERRORE GENERICO") + "]");
		}

		return out;
	}

	/**
	 * SIGI - Salvo su EVO gli OP dei decreti da lavorare recuperati da SIGI
	 */
	@Override
	public SigiResultDTO syncSIGI(final Integer idAoo) {
		final SigiResultDTO out = new SigiResultDTO();
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;

		try {
			fpeh = getFilenetPEHelper(idAoo);
			fceh = getIFilenetHelper(idAoo);

			validateAOO(idAoo);

			// Recupero id decreti da lavorare
			final List<String> ids = fpeh.fetchIdDecretiDaLavorare();

			for (final String idDecretoDaLavorare : ids) {

				// Recupero numero decreto
				final String numDecreto = fceh.fetchNumeroDecreto(idDecretoDaLavorare);
				populateSigiResult(out, fceh, numDecreto);
			}
		} catch (final Exception e) {
			LOGGER.warn(e);
		} finally {
			popSubject(fceh);
			logoff(fpeh);
		}

		return out;
	}

	/**
	 * @param out
	 * @param fceh
	 * @param numDecreto
	 */
	private void populateSigiResult(final SigiResultDTO out, final IFilenetCEHelper fceh, final String numDecreto) {
		try {
			// Recupero da SIGI gli op del decreto.
			final List<OrdinePagamentoDTO> ops = BusinessDelegate.getOrdinePagareByDecreto(pp.getParameterByKey(PropertiesNameEnum.SIGI_ENDPOINT), numDecreto);

			if (ops != null && !ops.isEmpty()) {
				// Salvo gli OP recuperati
				for (final OrdinePagamentoDTO op : ops) {

					final Boolean bOk = saveOPSigi(fceh, op);
					out.add(numDecreto, op.getNumeroOP(), bOk);
				}
			} else {
				LOGGER.warn("LA CHIAMATA VERSO SIGI PER [NUMERO DECRETO = " + numDecreto + "] NON HA RESTITUITO ALCUN ESITO");
			}
		} catch (final Exception e) {
			LOGGER.warn(e);
		}
	}

	/**
	 * @param fceh
	 * @param op
	 * @return esito operazione
	 */
	private boolean saveOPSigi(final IFilenetCEHelper fceh, final OrdinePagamentoDTO op) {
		Boolean bOk = false;

		try {
			CustomObject elencoOP = fceh.fetchElencoOPByNumDecreto(op.getNumeroDecreto());
			if (elencoOP == null) {
				elencoOP = fceh.createElencoOP(op.getNumeroDecreto());
			}
			final CustomObject opFilenet = fceh.fetchOP(elencoOP, op.getNumeroOP());
			String statoOP = null;
			if (op.getStatoOP() != null) {
				statoOP = op.getStatoOP().value();
			}

			if (opFilenet == null) {
				fceh.createOP(elencoOP, op.getAnnoEsercizio(), op.getCodiceAmministrazione(), op.getCodiceRagioneria(), op.getNumeroCapitolo(), op.getNumeroOP(),
						op.getPianoGestione(), statoOP, op.getTipoOP());
			} else {
				fceh.updateOP(opFilenet, op.getAnnoEsercizio(), op.getCodiceAmministrazione(), op.getCodiceRagioneria(), op.getNumeroCapitolo(), op.getNumeroOP(),
						op.getPianoGestione(), statoOP, op.getTipoOP());
			}
			bOk = true;
		} catch (final Exception e) {
			LOGGER.warn(e);
		}

		return bOk;
	}

}