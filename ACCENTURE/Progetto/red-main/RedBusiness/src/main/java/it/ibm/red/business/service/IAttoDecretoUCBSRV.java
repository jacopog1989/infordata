/**
 * 
 */
package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IAttoDecretoUCBFacadeSRV;

/**
 * Interface del service per la gestione degli Atti Decreto per UCB.
 */
public interface IAttoDecretoUCBSRV extends IAttoDecretoUCBFacadeSRV {

}