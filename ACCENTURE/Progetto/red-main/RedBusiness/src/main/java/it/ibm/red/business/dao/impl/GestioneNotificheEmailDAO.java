package it.ibm.red.business.dao.impl;

import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IGestioneNotificheEmailDAO;
import it.ibm.red.business.dto.MessaggioEmailDTO;
import it.ibm.red.business.dto.NotificaEmailDTO;
import it.ibm.red.business.enums.StatoCodaEmailEnum;
import it.ibm.red.business.enums.TipoDestinatarioDatiCertEnum;
import it.ibm.red.business.enums.TipologiaDestinatariEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.CodaEmail;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class GestioneNotificheEmailDAO.
 *
 * @author CPIERASC
 * 
 *         Dao per gestione delle notifiche delle mail.
 */
@Repository
public class GestioneNotificheEmailDAO extends AbstractDAO implements IGestioneNotificheEmailDAO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 4095090398257995261L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(GestioneNotificheEmailDAO.class.getName());
	
	/**
	 * Label max retry.
	 */
	private static final String MAX_RETRY_LABEL = "MAX_RETRY";

	/**
	 * Label count retry.
	 */
	private static final String COUNT_RETRY_LABEL = "COUNT_RETRY";

	/**
	 * Messaggio di errore generico recupero notifiche.
	 */
	private static final String GENERIC_MSG_ERRORE_RECUPERO_NOTIFICHE = "Errore durante il recupero delle notifiche mail ";

	/**
	 * Messaggio di errore recupero notifiche a cui concatenare il document title.
	 */
	private static final String MSG_ERROR_RECUPERO_NOTIFICHE = "Errore durante il recupero delle notifiche mail di questo document Title : ";

	/**
	 * Messaggio errore inserimento dei record.
	 */
	private static final String ERRORE_DURANTE_L_INSERIMENTO_NELLA_TABELLA_DEI_RECORD_PROCESSATI = "Errore durante l'inserimento nella tabella dei record processati : ";

	/**
	 * Messaggio di errore recupero document title.
	 */
	private static final String ERRORE_DURANTE_IL_RECUPERO_DEI_DOC_TITLE_SPEDITI_PER_UFFICIO = "Errore durante il recupero dei docTitleSpeditiPerUfficio : ";

	/**
	 * Colonna Id Aoo.
	 */
	private static final String IDAOO = "IDAOO";

	/**
	 * Colonna ID Documento.
	 */
	private static final String IDDOCUMENTO = "IDDOCUMENTO";

	/**
	 * Messaggio errore aggiornamento stato notifica di spedizione.
	 */
	private static final String ERROR_AGGIORNAMENTO_STATO_NOTIFICA_MSG = "Errore nell'aggiornamento dello stato della notifica di spedizione ";
	
	/**
	 * Messaggio errore recupero info stato notifica di spedizione.
	 */
	private static final String ERROR_RECUPERO_INFO_STATO_NOTIFICA_MSG = "Errore nel recupero info notifica di spedizione ";

	/**
	 * Numero massimo retry.
	 */
	private static final int MAX_RETRY = 3;

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#insertNotificaEmail(it.ibm.red.business.persistence.model.CodaEmail, java.sql.Connection).
	 */
	@Override
	public long insertNotificaEmail(final CodaEmail codaEmail, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;

		long idNotifica = 0;
		try {
			ps = connection.prepareStatement("SELECT SEQ_GESTIONENOTIFICHEEMAIL.NEXTVAL FROM DUAL");
			rs = ps.executeQuery();

			if (rs.next()) {
				idNotifica = rs.getLong(1);
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero del nextval dalla sequence delle notifiche mail " + codaEmail.toString() + " " + e, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		try {
			final Blob blob = getBlob(connection, codaEmail.getMessaggio());
			ps = connection.prepareStatement(" INSERT INTO GESTIONENOTIFICHEEMAIL (IDNOTIFICA, IDDOCUMENTO, IDAOO, MESSAGGIO, "
					+ "IDDESTINATARIO, EMAILMITTENTE, TIPOMITTENTE, TIPODESTINATARIO, EMAILDESTINATARIO, STATORICEVUTA, "
					+ "DATA_RICHIESTA_SPEDIZIONE, SPEDIZIONE, COUNT_RETRY, MAX_RETRY, FLAG_TO, FLAG_CC, TIPOEVENTO, APPLICAZIONE) "
					+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '" + APPLICAZIONE_DB + "') ");

			int n = 1;
			ps.setLong(n++, idNotifica); // IDNOTIFICA
			ps.setString(n++, codaEmail.getIdDocumento()); // IDDOCUMENTO
			ps.setInt(n++, codaEmail.getIdAoo()); // IDAOO
			ps.setBlob(n++, blob); // MESSAGGIO
			ps.setLong(n++, codaEmail.getIdDestinatario()); // IDDESTINATARIO
			ps.setString(n++, codaEmail.getEmailMittente()); // EMAILMITTENTE
			ps.setInt(n++, codaEmail.getTipoMittente()); // TIPOMITTENTE
			ps.setInt(n++, codaEmail.getTipoDestinatario()); // TIPODESTINATARIO
			ps.setString(n++, codaEmail.getEmailDestinatario()); // EMAILDESTINATARIO
			ps.setInt(n++, codaEmail.getStatoRicevuta()); // STATORICEVUTA
			ps.setDate(n++, new java.sql.Date((new Date()).getTime())); // DATA_RICHIESTA_SPEDIZIONE
			ps.setInt(n++, 0); // SPEDIZIONE
			ps.setInt(n++, 0); // COUNT_RETRY
			ps.setInt(n++, MAX_RETRY); // MAX_RETRY
			ps.setInt(n++, codaEmail.getFlagTO()); // FLAG_TO
			ps.setInt(n++, codaEmail.getFlagCC()); // FLAG_CC
			if (codaEmail.getTipoEvento() != null) { // TIPOEVENTO
				ps.setInt(n++, codaEmail.getTipoEvento());
			} else {
				ps.setNull(n++, Types.INTEGER);
			}

			ps.executeUpdate();

			return idNotifica;

		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'inserimento della notifica email. NOTIFICA = " + codaEmail, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}

	}

	@Override
	public final List<CodaEmail> lockItemDaSpedire(final int idAoo, final boolean isPostaInterna, final Integer numItemToSend, final Connection connection) {
		final String spedizioneNpsCondition = "AND (IDSPEDIZIONENPS IS NULL OR (IDSPEDIZIONENPS IS NOT NULL AND IDSPEDIZIONENPS = IDNOTIFICA)) ";

		final String sql = "SELECT * FROM GESTIONENOTIFICHEEMAIL "
				+ "WHERE (STATORICEVUTA = ? OR (STATORICEVUTA = ? AND COUNT_RETRY < MAX_RETRY)) AND IDAOO = ? AND APPLICAZIONE = '" + APPLICAZIONE_DB + "' "
				// Gestione spedizione unica
				+ "AND (IDSPEDIZIONEUNICA IS NULL OR (IDSPEDIZIONEUNICA IS NOT NULL AND IDSPEDIZIONEUNICA = IDNOTIFICA)) "
				// Gestione per invio mail con destinatari multipli tramite NPS a valle di una
				// firma
				// N.B. La condizione viene inserita SOLO SE la posta NON è gestita
				// dall'applicativo
				+ (isPostaInterna ? Constants.EMPTY_STRING : spedizioneNpsCondition) + "FOR UPDATE SKIP LOCKED";

		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<CodaEmail> listaNotifiche = new ArrayList<>();

		try {
			int index = 1;
			ps = connection.prepareStatement(sql);
			ps.setInt(index++, StatoCodaEmailEnum.DA_ELABORARE.getStatus());
			ps.setInt(index++, StatoCodaEmailEnum.ERRORE.getStatus());
			ps.setInt(index++, idAoo);
			ps.setMaxRows(numItemToSend);
			rs = ps.executeQuery();

			while (rs.next()) {
				listaNotifiche.add(populateVO(rs));
			}

			return listaNotifiche;
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero delle notifiche email da spedire per l'aoo " + idAoo, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}

	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#updateStatoRicevuta(java.util.List, it.ibm.red.business.enums.StatoCodaEmailEnum, java.sql.Connection).
	 */
	@Override
	public void updateStatoRicevuta(final List<Integer> items, final StatoCodaEmailEnum stato, final Connection connection) {
		PreparedStatement ps = null;
		final ResultSet rs = null;

		try {

			if (!items.isEmpty()) {

				final StringBuilder sql = new StringBuilder("UPDATE GESTIONENOTIFICHEEMAIL SET STATORICEVUTA = ? WHERE IDNOTIFICA in (");

				for (int i = 0; i < items.size(); i++) {

					if (i == (items.size() - 1)) {
						sql.append("?)");
					} else {
						sql.append("?,");
					}
				}

				int index = 1;
				ps = connection.prepareStatement(sql.toString());

				ps.setInt(index++, stato.getStatus());
				final Iterator<Integer> itMess = items.iterator();
				while (itMess.hasNext()) {

					ps.setInt(index++, itMess.next());

				}

				ps.executeUpdate();
			}

		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'aggiornamento dello stato delle code mail ", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}

	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#updateStatoRicevuta(java.sql.Connection, java.lang.Integer, java.lang.Integer).
	 */
	@Override
	public Integer updateStatoRicevuta(final Connection connection, final Integer idDocumento, final Integer statoRicevuta) {

		PreparedStatement ps = null;
		try {
			int index = 1;
			ps = connection.prepareStatement("UPDATE GESTIONENOTIFICHEEMAIL SET STATORICEVUTA = ? WHERE IDDOCUMENTO =  ? ");
			ps.setInt(index++, statoRicevuta);
			ps.setString(index++, "" + idDocumento);
			return ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'aggiornamento delle notifiche mail: idDocumento " + idDocumento + " con stato: " + statoRicevuta, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#aggiornaCodaMail(int,
	 *      byte[], java.lang.String, java.util.Date, java.lang.Integer,
	 *      java.lang.Integer, boolean, java.lang.Integer, boolean,
	 *      java.sql.Connection).
	 */
	@Override
	public int aggiornaCodaMail(final int statoRicevuta, final byte[] content, final String msgId, final Date dataSpedizione, final Integer idNotifica,
			final Integer idSpedizioneUnica, final boolean spedizioneUnica, final Integer idSpedizioneNps, final boolean updateCountRetry, final Connection connection) {
		int output = 0;
		PreparedStatement ps = null;

		try {
			final Blob blob = getBlob(connection, content);

			String whereClauseId = "IDNOTIFICA";
			if (spedizioneUnica) {
				whereClauseId = "IDSPEDIZIONEUNICA";
			} else if (idSpedizioneNps != null) {
				whereClauseId = "IDSPEDIZIONENPS";
			}

			final String countRetry = updateCountRetry ? ", COUNT_RETRY = COUNT_RETRY + 1" : "";

			ps = connection.prepareStatement("UPDATE GESTIONENOTIFICHEEMAIL SET STATORICEVUTA = ?, MESSAGGIO = ?, IDMESSAGGIO = ?, DATA_SPEDIZIONE = ?" + countRetry
					+ "  WHERE " + whereClauseId + " = ? ");

			int index = 1;
			ps.setInt(index++, statoRicevuta);
			ps.setBlob(index++, blob);
			ps.setString(index++, msgId);
			ps.setDate(index++, new java.sql.Date(dataSpedizione.getTime()));

			if (spedizioneUnica) {
				ps.setInt(index++, idSpedizioneUnica);
			} else if (idSpedizioneNps != null) {
				ps.setInt(index++, idSpedizioneNps);
			} else {
				ps.setInt(index++, idNotifica);
			}

			output = ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'aggiornamento della coda email (" + statoRicevuta + ", content,  " + msgId + ", " + dataSpedizione + ", " + idNotifica + ") ", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#getIdDocumentiNonChiusiDestOnlyCartacei(java.sql.Connection, int).
	 */
	@Override
	public List<String> getIdDocumentiNonChiusiDestOnlyCartacei(final Connection con, final int idAoo) {

		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<String> listaNotifiche = new ArrayList<>();

		try {
			int index = 1;

			ps = con.prepareStatement("SELECT DISTINCT c.IDDOCUMENTO FROM GESTIONENOTIFICHEEMAIL c " + "WHERE c.STATORICEVUTA <> ? AND c.TIPODESTINATARIO = ? "
					+ "AND c.IDAOO = ? " + "AND NOT EXISTS (select 1 from GESTIONENOTIFICHEEMAIL cc where cc.STATORICEVUTA <> ? "
					+ " and (cc.tipodestinatario = ? OR cc.tipodestinatario=? ) " + " and cc.idaoo = ? ) " + "ORDER BY c.IDDOCUMENTO ASC");

			ps.setInt(index++, StatoCodaEmailEnum.CHIUSURA.getStatus());
			ps.setInt(index++, TipologiaDestinatariEnum.CARTACEO.getId());
			ps.setInt(index++, idAoo);

			ps.setInt(index++, StatoCodaEmailEnum.CHIUSURA.getStatus());
			ps.setInt(index++, TipologiaDestinatariEnum.PEO.getId());
			ps.setInt(index++, TipologiaDestinatariEnum.PEC.getId());
			ps.setInt(index++, idAoo);

			rs = ps.executeQuery();

			while (rs.next()) {
				listaNotifiche.add(rs.getString(IDDOCUMENTO));
			}

			return listaNotifiche;
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero dei documenti con item non chiusi e destinatari solo cartacei ", e);
			throw new RedException("Errore durante il recupero dei documenti con item non chiusi e destinatari solo cartacei ", e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#getNotificheEmailByIdDocumento(java.lang.String, java.sql.Connection).
	 */
	@Override
	public Collection<CodaEmail> getNotificheEmailByIdDocumento(final String documentTitle, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final Collection<CodaEmail> listaNotifiche = new ArrayList<>();

		try {
			final String sql = " SELECT * FROM GESTIONENOTIFICHEEMAIL " + " WHERE IDDOCUMENTO = ? ";
			int index = 1;
			ps = connection.prepareStatement(sql);
			ps.setString(index++, documentTitle);
			rs = ps.executeQuery();

			while (rs.next()) {
				listaNotifiche.add(populateVO(rs));
			}

			return listaNotifiche;
		} catch (final Exception e) {
			LOGGER.error(MSG_ERROR_RECUPERO_NOTIFICHE + documentTitle, e);
			throw new RedException(GENERIC_MSG_ERRORE_RECUPERO_NOTIFICHE, e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	private static CodaEmail populateVO(final ResultSet rs) throws SQLException {
		final Blob msgBlob = rs.getBlob("MESSAGGIO");
		byte[] msgBytes = null;
		if (msgBlob != null) {
			msgBytes = msgBlob.getBytes(1, (int) msgBlob.length());
			msgBlob.free();
		}

		Integer idSpedizioneNps = rs.getInt("IDSPEDIZIONENPS");
		if (rs.wasNull()) {
			idSpedizioneNps = null;
		}

		return new CodaEmail(rs.getInt("IDNOTIFICA"), rs.getString(IDDOCUMENTO), rs.getString("IDMESSAGGIO"), rs.getString("EMAILMITTENTE"), rs.getInt("TIPOMITTENTE"),
				rs.getString("EMAILDESTINATARIO"), rs.getInt("TIPODESTINATARIO"), rs.getLong("IDDESTINATARIO"), rs.getInt("STATORICEVUTA"), rs.getInt(IDAOO),
				rs.getInt(COUNT_RETRY_LABEL), rs.getInt(MAX_RETRY_LABEL), rs.getInt("SPEDIZIONE"), msgBytes, rs.getInt("TIPOEVENTO"), rs.getDate("DATA_RICHIESTA_SPEDIZIONE"),
				rs.getDate("DATA_SPEDIZIONE"), rs.getInt("FLAG_CC"), rs.getInt("FLAG_TO"), rs.getInt("IDSPEDIZIONEUNICA"), idSpedizioneNps);
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#updateSpedizioneUnicaNotificaEmail(java.util.List, long, java.sql.Connection).
	 */
	@Override
	public int updateSpedizioneUnicaNotificaEmail(final List<Long> idNotificaToUpdateList, final long idNotificaSpedizioneUnica, final Connection con) {
		LOGGER.info("updateSpedizioneUnicaNotificaEmail -> START. ID notifica spedizione unica: " + idNotificaSpedizioneUnica);
		final int numOfIdNotificaToUpdate = idNotificaToUpdateList.size();
		int updateStatoNotificaEmail = 0;
		PreparedStatement ps = null;

		try {
			final StringBuilder updateSpedizioneUnicaStatement = new StringBuilder("UPDATE GESTIONENOTIFICHEEMAIL SET IDSPEDIZIONEUNICA = ? WHERE IDNOTIFICA IN");
			updateSpedizioneUnicaStatement.append(" (");

			final Iterator<Long> it = idNotificaToUpdateList.iterator();
			while (it.hasNext()) {
				it.next();
				updateSpedizioneUnicaStatement.append("?,");
			}

			ps = con.prepareStatement(updateSpedizioneUnicaStatement.deleteCharAt(updateSpedizioneUnicaStatement.length() - 1).append(")").toString());
			ps.setLong(1, idNotificaSpedizioneUnica);

			for (int i = 2; i < (numOfIdNotificaToUpdate + 2); i++) {
				ps.setLong(i, idNotificaToUpdateList.get(i - 2));
			}

			updateStatoNotificaEmail = ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'update dell'idSpedizioneUnica delle notifiche email. idNotificaSpedizioneUnica = " + idNotificaSpedizioneUnica, e);
			throw new RedException("Errore durante l'update dell'idSpedizioneUnica delle notifiche email. idNotificaSpedizioneUnica = " + idNotificaSpedizioneUnica, e);
		} finally {
			closeStatement(ps);
		}

		LOGGER.info("updateSpedizioneUnicaNotificaEmail -> END. ID notifica spedizione unica: " + idNotificaSpedizioneUnica);
		return updateStatoNotificaEmail;
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#getNotificheEmailByNoStatoRicevuta(java.util.List,
	 *      int, java.sql.Connection).
	 */
	@Override
	public List<CodaEmail> getNotificheEmailByNoStatoRicevuta(final List<Integer> statiDaEscludere, final int idAoo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<CodaEmail> listaNotifiche = new ArrayList<>();

		try {
			final StringBuilder sql = new StringBuilder("SELECT * FROM GESTIONENOTIFICHEEMAIL WHERE IDAOO = ? ");

			for (int i = 0; i < statiDaEscludere.size(); i++) {
				sql.append(" AND STATORICEVUTA <> ?");
			}

			sql.append(" AND IDDOCUMENTO NOT LIKE '%@%' AND TRIM(TRANSLATE(IDDOCUMENTO, '0123456789',' ')) IS NULL AND SPEDIZIONE = 0 ");
			sql.append(" ORDER BY IDDOCUMENTO ASC");

			int index = 1;
			ps = con.prepareStatement(sql.toString());

			ps.setInt(index++, idAoo);

			for (final Integer s : statiDaEscludere) {
				ps.setInt(index++, s);
			}

			rs = ps.executeQuery();

			while (rs.next()) {
				listaNotifiche.add(populateVO(rs));
			}

			return listaNotifiche;
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero delle notifiche email ", e);
			throw new RedException("Errore durante il recupero delle notifiche email ");
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#updateStatoNotificaEmail(
	 *      java.lang.String, java.lang.String, int, int, java.sql.Connection).
	 * @param idDocumento
	 *            identificativo documento
	 * @param idMessaggio
	 *            identificativo del messaggio
	 * @param idDestinatario
	 *            identificativo destinatario
	 * @param inStatoRicevutaNew
	 *            stato ricevuta da impostare
	 * @param con
	 *            connession al database
	 * @return esito update
	 */
	@Override
	public int updateStatoNotificaEmail(final String idDocumento, final String idMessaggio, final long idDestinatario, final int inStatoRicevutaNew, final Connection con) {
		PreparedStatement ps = null;
		final ResultSet rs = null;
		int updateStatoNotificaEmail = 0;
		try {
			int index = 1;
			int statoRicevutaNew = inStatoRicevutaNew;
			if (statoRicevutaNew == Constants.Notifiche.STATO_RICEVUTA_NOTIFICA_EMAIL_REINVIO) {
				ps = con.prepareStatement("UPDATE GESTIONENOTIFICHEEMAIL SET STATORICEVUTA = ?, COUNT_RETRY = MAX_RETRY " + "WHERE IDDOCUMENTO = ? " + "AND IDMESSAGGIO = ? "
						+ "AND IDDESTINATARIO = ?");
				statoRicevutaNew = Constants.Notifiche.STATO_RICEVUTA_NOTIFICA_EMAIL_ERRORE;
			} else {
				ps = con.prepareStatement(
						"UPDATE GESTIONENOTIFICHEEMAIL SET STATORICEVUTA = ? " + "WHERE IDDOCUMENTO = ? " + "AND IDMESSAGGIO = ? " + "AND IDDESTINATARIO = ?");
			}
			ps.setInt(index++, statoRicevutaNew);
			ps.setString(index++, idDocumento);
			ps.setString(index++, idMessaggio);
			ps.setLong(index++, idDestinatario);

			updateStatoNotificaEmail = ps.executeUpdate();

		} catch (final SQLException e) {
			throw new RedException(
					"Errore durante l'update della notifica email. IdDocumento= " + idDocumento + " IdMessaggio= " + idMessaggio + " IdDestinatario= " + idDestinatario, e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
		return updateStatoNotificaEmail;
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#getAndDeleteClosedDocument
	 *      (int, java.sql.Connection).
	 * @param idAoo
	 *            identificativo Area organizzativa
	 * @param con
	 *            connession al database
	 * @return
	 */
	@Override
	public String getAndDeleteClosedDocument(final int idAoo, final Connection con) {
		String results = null;
		CallableStatement cs = null;

		try {
			int index = 1;
			cs = con.prepareCall("{call GET_AND_DELETE_CLOSED_DOCUMENT(?,?)}");
			cs.setInt(index++, idAoo);
			cs.registerOutParameter(index, Types.VARCHAR);

			cs.execute();
			results = cs.getString(index);

			return results;
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero dei documenti chiusi " + e, e);
			throw new RedException("Errore durante il recupero dei documenti chiusi " + e);
		} finally {
			closeStatement(cs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#getNumErroriSpedizione(
	 *      java.lang.String, java.sql.Connection).
	 * @param documentTitle
	 *            identificativo documento
	 * @param connection
	 *            connesione al database
	 * @return numero errori spedizione
	 */
	@Override
	public int getNumErroriSpedizione(final String documentTitle, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer countMsg = 0;

		try {
			int index = 1;
			ps = connection.prepareStatement("SELECT count(*) FROM GESTIONENOTIFICHEEMAIL "
					+ " WHERE SPEDIZIONE = 0 AND COUNT_RETRY = MAX_RETRY AND STATORICEVUTA = ? AND TIPODESTINATARIO = 2 AND TIPOMITTENTE = 2 AND IDDOCUMENTO = ?");

			ps.setInt(index++, StatoCodaEmailEnum.ERRORE.getStatus());
			ps.setString(index++, documentTitle);

			rs = ps.executeQuery();

			if (rs.next()) {
				countMsg = rs.getInt(1);
			}

		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero degli errori per il documento " + documentTitle, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return countMsg;
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#getNotificheEmailForIconaStato(java.lang.String,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public Collection<CodaEmail> getNotificheEmailForIconaStato(final String documentTitle, final Long idAoo, final Connection connection) {
		Collection<CodaEmail> output = new ArrayList<>();
		StringBuilder sb = new StringBuilder();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			
			sb.append(" SELECT * ");
			sb.append(" FROM GESTIONENOTIFICHEEMAIL ");

			// Documento da ELABORARE
			sb.append(" WHERE IDDOCUMENTO = ? AND IDAOO = ?");
			sb.append(" AND (");

				// Stato ATTESA - CHIUSURA o SPEDITO - ATTESA NPS
				sb.append(" (STATORICEVUTA = ? OR STATORICEVUTA = ? OR STATORICEVUTA = ? OR STATORICEVUTA = ? OR STATORICEVUTA = ? OR STATORICEVUTA = ?) ");
				
				// Stato Spedizioe in ERRORE Se il numero di retry è maggiore o uguale al massimo
				sb.append(" OR (SPEDIZIONE = 0 AND STATORICEVUTA = ?) ");
				
			sb.append(" )");
			
			int index = 1;
			ps = connection.prepareStatement(sb.toString());
			ps.setString(index++, documentTitle);
			ps.setLong(index++, idAoo);
			ps.setInt(index++, StatoCodaEmailEnum.ATTESA_ACCETTAZIONE.getStatus());
			ps.setInt(index++, StatoCodaEmailEnum.ATTESA_AVVENUTA_CONSEGNA.getStatus());
			ps.setInt(index++, StatoCodaEmailEnum.CHIUSURA.getStatus());
			ps.setInt(index++, StatoCodaEmailEnum.SPEDITO.getStatus());
			ps.setInt(index++, StatoCodaEmailEnum.ATTESA_SPEDIZIONE_NPS.getStatus());
			ps.setInt(index++, StatoCodaEmailEnum.DA_ELABORARE.getStatus());
			ps.setInt(index++, StatoCodaEmailEnum.ERRORE.getStatus());
			rs = ps.executeQuery();

			while (rs.next()) {
				CodaEmail obj = populateVO(rs);
				if(rs.getInt(COUNT_RETRY_LABEL)<rs.getInt(MAX_RETRY_LABEL) && (StatoCodaEmailEnum.ERRORE.getStatus().equals(obj.getStatoRicevuta()))){
					obj.setStatoRicevuta(StatoCodaEmailEnum.DA_ELABORARE.getStatus());
				}
				
				output.add(obj);
			}
			
			

		} catch (Exception e) {
			LOGGER.error(MSG_ERROR_RECUPERO_NOTIFICHE + documentTitle, e);
			throw new RedException(GENERIC_MSG_ERRORE_RECUPERO_NOTIFICHE, e);
		} finally {
			closeStatement(ps, rs);
		}

		return output;
	}
	
	@Override
	public Collection<CodaEmail> getNotificheEmailForIconaStatoReinvia(final String documentTitle, final Long idAoo, final Connection connection) {
		Collection<CodaEmail> output = new ArrayList<>();
		StringBuilder sb = new StringBuilder();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			
			sb.append(" SELECT * FROM GESTIONENOTIFICHEEMAIL ");

			// Documento da ELABORARE
			sb.append(" WHERE IDDOCUMENTO = ? AND IDAOO = ?");
			sb.append(" AND (");

				// Stato ATTESA - CHIUSURA o SPEDITO - ATTESA NPS
				sb.append(" (STATORICEVUTA = ? OR STATORICEVUTA = ? OR STATORICEVUTA = ? OR STATORICEVUTA = ? OR STATORICEVUTA = ? OR STATORICEVUTA = ? OR STATORICEVUTA=?) ");
	
				
			sb.append(" )");
			
			int index = 1;
			ps = connection.prepareStatement(sb.toString());
			ps.setString(index++, documentTitle);
			ps.setLong(index++, idAoo);
			ps.setInt(index++, StatoCodaEmailEnum.ATTESA_ACCETTAZIONE.getStatus());
			ps.setInt(index++, StatoCodaEmailEnum.ATTESA_AVVENUTA_CONSEGNA.getStatus());
			ps.setInt(index++, StatoCodaEmailEnum.CHIUSURA.getStatus());
			ps.setInt(index++, StatoCodaEmailEnum.SPEDITO.getStatus());
			ps.setInt(index++, StatoCodaEmailEnum.ATTESA_SPEDIZIONE_NPS.getStatus());
			ps.setInt(index++, StatoCodaEmailEnum.DA_ELABORARE.getStatus());
			ps.setInt(index++, StatoCodaEmailEnum.ERRORE.getStatus());
			rs = ps.executeQuery();

			while (rs.next()) {
				CodaEmail obj = populateVO(rs);
				if((rs.getInt(COUNT_RETRY_LABEL)<rs.getInt(MAX_RETRY_LABEL) && (StatoCodaEmailEnum.ERRORE.getStatus().equals(obj.getStatoRicevuta()))) || (obj.getSpedizione()!=null && obj.getSpedizione()>0)){
					obj.setStatoRicevuta(StatoCodaEmailEnum.DA_ELABORARE.getStatus());
				}
				
				output.add(obj);
			}
			
			

		} catch (Exception e) {
			LOGGER.error(MSG_ERROR_RECUPERO_NOTIFICHE + documentTitle, e);
			throw new RedException(GENERIC_MSG_ERRORE_RECUPERO_NOTIFICHE, e);
		} finally {
			closeStatement(ps, rs);
		}

		return output;
	}


	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#getNumErroriSpedizioneCoda
	 *      (java.util.List, java.sql.Connection).
	 * @param documentTitleList
	 *            identificativi dei documenti
	 * @param connection
	 *            connession al database
	 * @return numero errori spedizione coda
	 */
	@Override
	public final int getNumErroriSpedizioneCoda(final List<String> documentTitleList, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer countMsg = 0;

		try {
			int index = 1;

			final String query = "SELECT count(DISTINCT IDDOCUMENTO) FROM GESTIONENOTIFICHEEMAIL WHERE SPEDIZIONE = 0 "
					+ "AND COUNT_RETRY = MAX_RETRY AND STATORICEVUTA = ? AND " + StringUtils.createInCondition(IDDOCUMENTO, documentTitleList.iterator(), true);

			ps = connection.prepareStatement(query);

			ps.setInt(index++, StatoCodaEmailEnum.ERRORE.getStatus());

			LOGGER.info("###NumErroriSped### - Query getNumErroriSpedizioneCoda: " + query);

			rs = ps.executeQuery();
			if (rs.next()) {
				countMsg = rs.getInt(1);
			}

			LOGGER.info("###NumErroriSped### - Start getNumErroriSpedizioneCoda");
		} catch (final SQLException e) {
			LOGGER.error("Si è verificato un errore durante la count dei documenti in errore di spedizione.", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return countMsg;
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#
	 *      getNotificheEmailByIdDocEmailDest(java.lang.String, java.lang.String,
	 *      java.sql.Connection).
	 * @param idDocumento
	 *            identificativo del documento
	 * @param emailDestinatario
	 *            E-mail del destinatario
	 * @param con
	 *            connession al database
	 * @return CodaEmail recuperata
	 */
	@Override
	public final CodaEmail getNotificheEmailByIdDocEmailDest(final String idDocumento, final String emailDestinatario, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		CodaEmail notificaEmail = null;
		try {
			int index = 1;
			ps = con.prepareStatement("SELECT * FROM GESTIONENOTIFICHEEMAIL WHERE IDDOCUMENTO = ? AND UPPER(EMAILDESTINATARIO) = UPPER(?)");
			ps.setString(index++, idDocumento);
			ps.setString(index++, emailDestinatario);
			rs = ps.executeQuery();

			if (rs.next()) {
				notificaEmail = populateVO(rs);
			}

			return notificaEmail;
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero della notifica email. IdDocumento= " + idDocumento + " EmailDestinatario= " + emailDestinatario + " " + e, e);
			throw new RedException("Errore durante il recupero della notifica email.");
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#getNotificheEmailByIdDocEmailDestAndStato(java.lang.String,
	 *      java.lang.String, java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public CodaEmail getNotificheEmailByIdDocEmailDestAndStato(final String idDocumento, final String emailDestinatario, final Integer stato, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		CodaEmail notificaEmail = null;

		try {
			int index = 1;
			ps = con.prepareStatement("SELECT * FROM GESTIONENOTIFICHEEMAIL WHERE IDDOCUMENTO = ? AND EMAILDESTINATARIO = ? AND STATORICEVUTA = ?");
			ps.setString(index++, idDocumento);
			ps.setString(index++, emailDestinatario);
			ps.setInt(index++, stato);
			rs = ps.executeQuery();

			if (rs.next()) {
				notificaEmail = populateVO(rs);
			}

			return notificaEmail;
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero della notifica email. IdDocumento= " + idDocumento + " EmailDestinatario= " + emailDestinatario + " " + e, e);
			throw new RedException("Errore durante il recupero della notifica email.");
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Aggiorna lo stato di spedizione della notifica.
	 * 
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#updateSpeditoNotificaEmail
	 *      (int, int, java.sql.Connection).
	 * @param idNotifica
	 *            identificativo notifica
	 * @param spedito
	 *            nuovo stato flag spedito da impostare
	 * @param con
	 *            connessione al database
	 * @return esito dell'update SQL
	 */
	@Override
	public int updateSpeditoNotificaEmail(final int idNotifica, final int spedito, final Connection con) {
		PreparedStatement ps = null;
		final ResultSet rs = null;
		int updateSpeditoNotificaEmail = 0;

		try {
			int index = 1;
			ps = con.prepareStatement("UPDATE GESTIONENOTIFICHEEMAIL SET SPEDIZIONE = ? WHERE IDNOTIFICA = ?");
			ps.setInt(index++, spedito);
			ps.setInt(index++, idNotifica);

			updateSpeditoNotificaEmail = ps.executeUpdate();

			LOGGER.info("updateSpeditoNotificaEmail - STATO :" + updateSpeditoNotificaEmail);
		} catch (final Exception e) {
			throw new RedException("Errore durante l'update della notifica email con idNotifica: " + idNotifica, e);
		} finally {
			closeStatement(ps, rs);
		}

		return updateSpeditoNotificaEmail;
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#
	 *      rollbackGestioneNoticheBatch(java.util.List, java.sql.Connection).
	 * @param idDocumentToRollback
	 *            identificativi documenti da rollbackare
	 * @param con
	 *            connession al database
	 */
	@Override
	public final void rollbackGestioneNoticheBatch(final List<Long> idDocumentToRollback, final Connection con) {
		Statement st = null;
		final ResultSet rs = null;

		try {
			final StringBuilder sql = new StringBuilder("UPDATE GESTIONENOTIFICHEEMAIL SET STATORICEVUTA = 3 WHERE IDDOCUMENTO in ( ");
			final Long idDocument = null;
			for (int i = 0; i < idDocumentToRollback.size(); i++) {
				sql.append(idDocument);
				if (i < (idDocumentToRollback.size() - 1)) {
					sql.append(", ");
				}
			}
			sql.append(" ) AND STATORICEVUTA = 7 AND SPEDIZIONE = 0 ");
			st = con.createStatement();
			st.executeUpdate(sql.toString());
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException("Errore durante l'update della notifica email");
		} finally {
			closeStatement(st, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#
	 *      getDocInErroreCodaSpedizione(java.util.List, java.sql.Connection).
	 * @param documentTitleList
	 *            identificativi dei documenti in errore coda spedizione
	 * @param connection
	 *            connessione al database
	 * @return lista degli idnetificativi dei documenti in errore
	 */
	@Override
	public final List<String> getDocInErroreCodaSpedizione(final List<String> documentTitleList, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<String> documentTitleListInErroreCodaSpedizione = new ArrayList<>();

		try {
			final StringBuilder query = new StringBuilder().append(
					"SELECT DISTINCT IDDOCUMENTO FROM GESTIONENOTIFICHEEMAIL WHERE SPEDIZIONE = 0 AND TIPODESTINATARIO = 2 AND TIPOMITTENTE = 2 AND COUNT_RETRY = MAX_RETRY")
					.append(" AND STATORICEVUTA = ").append(StatoCodaEmailEnum.ERRORE.getStatus()).append(" AND ")
					.append(StringUtils.createInCondition(IDDOCUMENTO, documentTitleList.iterator(), true));

			ps = connection.prepareStatement(query.toString());
			rs = ps.executeQuery();

			while (rs.next()) {
				documentTitleListInErroreCodaSpedizione.add(rs.getString(1));
			}
		} catch (final SQLException e) {
			LOGGER.error("Si è verificato un errore durante il recupero dei documenti in errore di spedizione.", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return documentTitleListInErroreCodaSpedizione;
	}

	@Override
	public final List<String> getInfoSpedizioneWidget(final List<String> documentTitleList, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<String> documentTitleListResult = new ArrayList<>();

		try {
			final StringBuilder query = new StringBuilder()
					.append("SELECT DISTINCT IDDOCUMENTO,STATORICEVUTA FROM GESTIONENOTIFICHEEMAIL WHERE SPEDIZIONE = 0 AND TIPODESTINATARIO = 2 AND TIPOMITTENTE = 2 ")
					.append(" AND ").append(StringUtils.createInCondition(IDDOCUMENTO, documentTitleList.iterator(), true)).append(" AND (STATORICEVUTA = ")
					.append(StatoCodaEmailEnum.ATTESA_ACCETTAZIONE.getStatus()).append(" OR STATORICEVUTA = ").append(StatoCodaEmailEnum.ATTESA_AVVENUTA_CONSEGNA.getStatus())
					.append(" OR (STATORICEVUTA = ").append(StatoCodaEmailEnum.ERRORE.getStatus()).append(" AND COUNT_RETRY = MAX_RETRY)").append(" OR (STATORICEVUTA = ")
					.append(StatoCodaEmailEnum.SPEDITO.getStatus()).append(" AND DATA_SPEDIZIONE > SYSDATE - 15))");

			ps = connection.prepareStatement(query.toString());
			rs = ps.executeQuery();

			while (rs.next()) {
				documentTitleListResult.add(rs.getString(1) + "," + rs.getInt(2));
			}
		} catch (final SQLException e) {
			LOGGER.error("Si è verificato un errore durante il recupero dei documenti in errore di spedizione.", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return documentTitleListResult;
	}

	/**
	 * Aggiorna il tipo destinatario.
	 * 
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#updateSpeditoNotificaEmail
	 *      (int, int, java.sql.Connection).
	 * @param tipoDestinatario
	 *            tipo destinatario da impostare
	 * @param messageId
	 *            identificativo messaggio
	 * @param mittente
	 *            mittente
	 * @param con
	 *            connessione al database
	 * @return esito update SQL
	 */
	@Override
	public int updateTipoDestinatario(final TipoDestinatarioDatiCertEnum tipoDestinatario, final String messageId, final String mittente, final Connection con) {
		PreparedStatement ps = null;
		final ResultSet rs = null;
		int updateTipoDestinatario = 0;

		try {
			int index = 1;
			int idTipoDestinatario = 1; // Esterno
			if (TipoDestinatarioDatiCertEnum.CERTIFICATO.equals(tipoDestinatario)) {
				idTipoDestinatario = 2; // Certificato
			}

			ps = con.prepareStatement("UPDATE GESTIONENOTIFICHEEMAIL SET TIPODESTINATARIO = ? WHERE IDMESSAGGIO = ? AND EMAILMITTENTE = ? AND TIPODESTINATARIO <> ?");
			ps.setInt(index++, idTipoDestinatario);
			ps.setString(index++, messageId);
			ps.setString(index++, mittente);
			//in questo modo non aggiorna una riga che è già corretta.
			ps.setInt(index++, idTipoDestinatario);

			updateTipoDestinatario = ps.executeUpdate();
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException("Errore durante l'update del tipo destinatario della notifica email con Message-ID: " + messageId + " e mittente: " + mittente);
		} finally {
			closeStatement(ps, rs);
		}

		return updateTipoDestinatario;
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#updateSpedizioneNps(java.util.List, long, java.sql.Connection).
	 */
	@Override
	public int updateSpedizioneNps(final List<Long> idNotificaToUpdateList, final long idNotificaSpedizioneNps, final Connection con) {
		LOGGER.info("updateSpedizioneNps -> START. ID notifica spedizione NPS: " + idNotificaSpedizioneNps);
		final int numOfIdNotificaToUpdate = idNotificaToUpdateList.size();
		int updateStatoNotificaEmail = 0;
		PreparedStatement ps = null;

		try {
			final StringBuilder updateSpedizioneUnicaStatement = new StringBuilder("UPDATE GESTIONENOTIFICHEEMAIL SET IDSPEDIZIONENPS = ? WHERE IDNOTIFICA IN");
			updateSpedizioneUnicaStatement.append(" (");

			final Iterator<Long> it = idNotificaToUpdateList.iterator();
			while (it.hasNext()) {
				it.next();
				updateSpedizioneUnicaStatement.append("?,");
			}

			ps = con.prepareStatement(updateSpedizioneUnicaStatement.deleteCharAt(updateSpedizioneUnicaStatement.length() - 1).append(")").toString());
			ps.setLong(1, idNotificaSpedizioneNps);

			for (int i = 2; i < (numOfIdNotificaToUpdate + 2); i++) {
				ps.setLong(i, idNotificaToUpdateList.get(i - 2));
			}

			updateStatoNotificaEmail = ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("updateSpedizioneNps ->  Errore durante l'update della colonna SPEDIZIONENPS delle notifiche email. ID notifica spedizione NPS: "
					+ idNotificaSpedizioneNps, e);
			throw new RedException("Errore durante l'update della colonna SPEDIZIONENPS delle notifiche email. ID notifica spedizione NPS: " + idNotificaSpedizioneNps, e);
		} finally {
			closeStatement(ps);
		}
		LOGGER.info("updateSpedizioneNps -> END. ID notifica spedizione NPS: " + idNotificaSpedizioneNps);
		return updateStatoNotificaEmail;
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#updateNotificaSpedizione(java.lang.Long, java.sql.Connection).
	 */
	@Override
	public int updateNotificaSpedizione(final Long idNotificaSped, final Connection conn) {
		PreparedStatement ps = null;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		int output = 0;
		int index = 1;
		String documentTitle = "";
		Long idAOO = 0L;
		try {
			
			ps1 = conn.prepareStatement("SELECT * FROM GESTIONENOTIFICHEEMAIL WHERE IDNOTIFICA=?");
			ps1.setLong(index, idNotificaSped);
			rs1 = ps1.executeQuery();
			if (rs1.next()) {
				documentTitle = rs1.getString(IDDOCUMENTO);
				idAOO = rs1.getLong(IDAOO);
			}
			
		} catch (final SQLException ex) {
			LOGGER.error(ERROR_RECUPERO_INFO_STATO_NOTIFICA_MSG, ex);
			throw new RedException(ERROR_RECUPERO_INFO_STATO_NOTIFICA_MSG, ex);
		} finally {
			closeStatement(ps1, rs1);
		}

		try {
			index = 1;
			ps = conn.prepareStatement("UPDATE GESTIONENOTIFICHEEMAIL SET STATORICEVUTA = 0 WHERE IDAOO = ? AND IDDOCUMENTO = ? AND STATORICEVUTA = -1");
			ps.setLong(index++, idAOO);
			ps.setString(index++, documentTitle);

			output = ps.executeUpdate();

		} catch (final SQLException ex) {
			LOGGER.error(ERROR_AGGIORNAMENTO_STATO_NOTIFICA_MSG, ex);
			throw new RedException(ERROR_AGGIORNAMENTO_STATO_NOTIFICA_MSG, ex);
		} finally {
			closeStatement(ps);
		}
		
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#updateCodiMessageGestioneNotificheEmail(java.lang.String,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public boolean updateCodiMessageGestioneNotificheEmail(final String codiMessaggio, final Long idNotifica, final Connection conn) {
		PreparedStatement ps = null;
		boolean output = false;
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append("UPDATE GESTIONENOTIFICHEEMAIL SET CODI_MESSAGGIO = ? WHERE IDNOTIFICA = ? ");
			ps = conn.prepareStatement(sb.toString());
			ps.setString(index++, codiMessaggio);
			ps.setInt(index++, idNotifica.intValue());

			output = ps.executeUpdate() > 0;

		} catch (final SQLException ex) {
			LOGGER.error(ERROR_AGGIORNAMENTO_STATO_NOTIFICA_MSG, ex);
			throw new RedException(ERROR_AGGIORNAMENTO_STATO_NOTIFICA_MSG, ex);
		} finally {
			closeStatement(ps);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#getStatoEmailByCodiMessage(java.lang.String, java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public MessaggioEmailDTO getStatoEmailByCodiMessage(final String codiMessaggio, final Integer stato, final Connection conn) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		MessaggioEmailDTO output = null;
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM GESTIONENOTIFICHEEMAIL WHERE CODI_MESSAGGIO = ? ");
			ps = conn.prepareStatement(sb.toString());
			ps.setString(index++, codiMessaggio);
			rs = ps.executeQuery();

			while (rs.next()) {
				final CodaEmail resultQuery = populateVO(rs);
				output = fromCodaEmailToDTO(resultQuery);
				output.setIdAoo(rs.getLong(IDAOO));
			}

		} catch (final SQLException ex) {
			LOGGER.error(ERROR_AGGIORNAMENTO_STATO_NOTIFICA_MSG, ex);
			throw new RedException("Errore nell'aggiornamento dello stato della notifica", ex);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}

	private static MessaggioEmailDTO fromCodaEmailToDTO(final CodaEmail item) {
		MessaggioEmailDTO messaggio = null;
		try {
			messaggio = (MessaggioEmailDTO) FileUtils.getObjectFromByteArray(item.getMessaggio());
			messaggio.setIdNotifica(item.getIdNotifica());
			messaggio.setTipoDestinatario(item.getTipoDestinatario());
			messaggio.setIdSpedizioneUnica(item.getIdSpedizioneUnica());
			messaggio.setTipoEvento(item.getTipoEvento());
			messaggio.setUltimoTentativo(item.getCountRetry().equals(item.getMaxRetry() - 1));
			messaggio.setIdDocumento(item.getIdDocumento());
			messaggio.setIdSpedizioneNps(item.getIdSpedizioneNps());
			messaggio.setMsgID(item.getIdMessaggio());
		} catch (final Exception ex) {
			LOGGER.error("Errore nella conversione in dto dell'email" + ex);
			throw new RedException("Errore nella conversione in dto dell'email" + ex);
		}
		return messaggio;
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#getNotificheByStatoRicevuta(java.lang.Long, java.lang.Integer, java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public List<NotificaEmailDTO> getNotificheByStatoRicevuta(final Long idAoo, final Integer statoRicevuta,final Integer giorniNotificaMancataSpedizione, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<NotificaEmailDTO> listaNotifiche = null;

		try {
			StringBuilder sql = new StringBuilder();

			if(giorniNotificaMancataSpedizione!=null) {
				listaNotifiche = new ArrayList<>();
				sql.append("SELECT DISTINCT IDDOCUMENTO,IDAOO,STATORICEVUTA ");
				sql.append("FROM GESTIONENOTIFICHEEMAIL WHERE IDAOO = ? AND STATORICEVUTA = ? ");
				//Data in cui gira il job - num di giorni per aoo > data_spedizione
				sql.append("AND DATA_SPEDIZIONE <= SYSDATE - ? AND (DATA_PROCESSAMENTO IS NULL or TRIM(DATA_PROCESSAMENTO) <> TRIM(SYSDATE)) ");	 

				sql.append("ORDER BY IDDOCUMENTO DESC ");

				int index = 1;
				ps = con.prepareStatement(sql.toString());

				ps.setInt(index++, idAoo.intValue());
				ps.setInt(index++, statoRicevuta);
				ps.setInt(index++, giorniNotificaMancataSpedizione);	

				rs = ps.executeQuery();
				while (rs.next()) {
					NotificaEmailDTO notifica = new NotificaEmailDTO();
					notifica.setIdAoo(rs.getInt(IDAOO));
					notifica.setIdDocumento(rs.getString(IDDOCUMENTO));
					notifica.setStatoRicevuta(rs.getInt("STATORICEVUTA"));
					listaNotifiche.add(notifica);
				}

			}
		} catch (SQLException e) {
			LOGGER.error("Errore durante il recupero delle notifiche con stato ATTESA_SPEDIZIONE_NPS ", e);
			throw new RedException("Errore durante il recupero delle notifiche email ATTESA_SPEDIZIONE_NPS" + e);
		} finally {
			closeStatement(ps, rs);
		}
		return listaNotifiche;
	}
	
	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#getDestinatariNotificheByStatoRicevuta(java.lang.Long, java.lang.String, java.sql.Connection).
	 */
	@Override
	public List<String> getDestinatariNotificheByStatoRicevuta(final Long idAoo, final String idDocumento, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<String> listaEmailDestMancataSped = new ArrayList<>();

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT DISTINCT IDDOCUMENTO,IDAOO,STATORICEVUTA ,EMAILDESTINATARIO ");
			sql.append("FROM GESTIONENOTIFICHEEMAIL WHERE IDAOO = ? AND STATORICEVUTA = 9 AND iddocumento = ?");
			sql.append("ORDER BY IDDOCUMENTO ASC ");

			int index = 1;
			ps = con.prepareStatement(sql.toString());
			
			ps.setInt(index++, idAoo.intValue());
			ps.setString(index++, idDocumento);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				listaEmailDestMancataSped.add(rs.getString("EMAILDESTINATARIO"));
			}
			 
			
		} catch (SQLException e) {
			LOGGER.error("Errore durante il recupero delle notifiche con stato ATTESA_SPEDIZIONE_NPS ", e);
			throw new RedException("Errore durante il recupero delle notifiche email ATTESA_SPEDIZIONE_NPS" + e);
		} finally {
			closeStatement(ps, rs);
		}
		return listaEmailDestMancataSped;
	}
	
	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#getStatoRicevutaDiversoDa3(java.util.List,
	 *      java.sql.Connection).
	 */
	@Override
	public List<String> getStatoRicevutaDiversoDa3(final List<String> docTitleSpeditiPerUfficio ,final Connection conn) {
		List<String> listDaSpedireNotifica = new ArrayList<>();
		listDaSpedireNotifica.addAll(docTitleSpeditiPerUfficio);
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT DISTINCT IDDOCUMENTO FROM GESTIONENOTIFICHEEMAIL ");
			sb.append("WHERE STATORICEVUTA <> 3  AND ");
			sb.append(StringUtils.createInCondition(IDDOCUMENTO, docTitleSpeditiPerUfficio.iterator(),true));
			sb.append(" AND STATORICEVUTA <> 7 ");
			
			ps = conn.prepareStatement(sb.toString());
			rs = ps.executeQuery(); 
			while(rs.next()) { 
				listDaSpedireNotifica.remove(rs.getString(IDDOCUMENTO));
			}  
		} catch(Exception ex) {
			LOGGER.error(ERRORE_DURANTE_IL_RECUPERO_DEI_DOC_TITLE_SPEDITI_PER_UFFICIO,ex);
			throw new RedException(ERRORE_DURANTE_IL_RECUPERO_DEI_DOC_TITLE_SPEDITI_PER_UFFICIO,ex);
		} finally {
			closeStatement(ps, rs);
		}
		
		return listDaSpedireNotifica;
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#upateDataProcessamentoByDT(java.lang.Long, java.lang.String, java.sql.Connection).
	 */
	@Override
	public boolean upateDataProcessamentoByDT(final Long idAoo,final String idDocumento,final Connection conn) {
		PreparedStatement ps = null; 
		try {
			int index = 1;
			StringBuilder sb = new StringBuilder();
			sb.append("UPDATE GESTIONENOTIFICHEEMAIL SET DATA_PROCESSAMENTO = SYSDATE WHERE IDAOO = ? AND IDDOCUMENTO = ?");
			 
			ps = conn.prepareStatement(sb.toString());
			ps.setInt(index++, idAoo.intValue());
			ps.setString(index++, idDocumento);
			return ps.executeUpdate() > 0; 
		} catch(Exception ex) {
			LOGGER.error("Errore durante l'aggiornamento della data di processamento : " + ex);
			throw new RedException("Errore durante l'aggiornamento della data di processamento : " + ex);
		} finally {
			closeStatement(ps);
		}
	}
	
	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#getRecordGiaProcessati(java.util.List, java.sql.Connection).
	 */
	@Override
	public List<String> getRecordGiaProcessati(final List<String> docTitleSpeditiPerUfficio ,final Connection conn) {
		List<String> listDaSpedireNotifica = new ArrayList<>();
		listDaSpedireNotifica.addAll(docTitleSpeditiPerUfficio);
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			StringBuilder sb = new StringBuilder();
			//Stato 0 significa che i record non sono ancora processati o sono andati in errore e devono essere riprocessati
			sb.append("SELECT DISTINCT DOCTITLE FROM CODA_GG_NOTIF_SPED_CART ");
			sb.append("WHERE trunc(DATAPROCESSAMENTO) = trunc(sysdate) AND STATO = 1 ");

			ps = conn.prepareStatement(sb.toString());
			rs = ps.executeQuery(); 
			while(rs.next()) { 
				listDaSpedireNotifica.remove(rs.getString("DOCTITLE"));
			}  
		} catch(Exception ex) {
			LOGGER.error(ERRORE_DURANTE_IL_RECUPERO_DEI_DOC_TITLE_SPEDITI_PER_UFFICIO,ex);
			throw new RedException(ERRORE_DURANTE_IL_RECUPERO_DEI_DOC_TITLE_SPEDITI_PER_UFFICIO,ex);
		} finally {
			closeStatement(ps, rs);
		}
		
		return listDaSpedireNotifica;
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#insertRecordProcessati(java.lang.Long, java.lang.String, int, java.sql.Connection).
	 */
	@Override
	public boolean insertRecordProcessati(final Long idAoo, final String documentTitle, final int stato,final Connection conn) {
		PreparedStatement ps = null; 
		boolean output = false;
		try {
			int index = 1;
			StringBuilder sb = new StringBuilder();
			//Stato 0 significa che i record non sono ancora processati o sono andati in errore e devono essere riprocessati
			sb.append("INSERT INTO CODA_GG_NOTIF_SPED_CART(IDAOO,DATAPROCESSAMENTO,DOCTITLE,STATO) VALUES (?,?,?,?)");
			
			ps = conn.prepareStatement(sb.toString());
			ps.setInt(index++, idAoo.intValue());
			ps.setDate(index++, new java.sql.Date((new Date()).getTime())); 
			ps.setString(index++, documentTitle); 
			ps.setInt(index++, stato);

			output = ps.executeUpdate() > 0;
		} catch(Exception ex) {
			LOGGER.error(ERRORE_DURANTE_L_INSERIMENTO_NELLA_TABELLA_DEI_RECORD_PROCESSATI,ex);
			throw new RedException(ERRORE_DURANTE_L_INSERIMENTO_NELLA_TABELLA_DEI_RECORD_PROCESSATI,ex);
		} finally {
			closeStatement(ps);
		}
		
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#deleteRecordProcessatiDaUnaSettimana(java.lang.Long, java.sql.Connection).
	 */
	@Override
	public boolean deleteRecordProcessatiDaUnaSettimana(final Long idAoo, final Connection conn) {
		PreparedStatement ps = null; 
		boolean output = false;
		try {
			int index = 1;
			StringBuilder sb = new StringBuilder();
			//Elimino i record inseriti una settimana prima 
			sb.append("DELETE FROM CODA_GG_NOTIF_SPED_CART WHERE DATAPROCESSAMENTO <= SYSDATE - 7 AND IDAOO = ?");
			
			ps = conn.prepareStatement(sb.toString()); 
			
			ps.setInt(index, idAoo.intValue());
			output = ps.executeUpdate() > 0;
		} catch(Exception ex) {
			LOGGER.error("Errore durante l'eliminazione nella tabella dei record processati : ",ex);
			throw new RedException("Errore durante l'eliminazione nella tabella dei record processati : ",ex);
		} finally {
			closeStatement(ps);
		}
		
		return output;
	}
	
	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#getTipoMittenteCasellaPostale(java.lang.String, java.sql.Connection).
	 */
	@Override
	public Integer getTipoMittenteCasellaPostale(final String mittente,final Connection conn) {
		Integer output = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			int index = 1;
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM CASELLAPOSTALE WHERE INDIRIZZOEMAIL = ? ");
			
			ps = conn.prepareStatement(sb.toString());
			ps.setString(index++, mittente);
			
			rs = ps.executeQuery();
			if(rs.next()) {
				output = rs.getInt("TIPOMITTENTE");
			}
		} catch(Exception ex) {
			LOGGER.error("Errore durante il recupero del tipo mittente dalla casella postale : "+ex);
			throw new RedException("Errore durante il recupero del tipo mittente dalla casella postale : "+ex);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#deleteByIdAooAndDT(java.lang.Long, java.lang.String, java.sql.Connection).
	 */
	@Override
	public boolean deleteByIdAooAndDT(final Long idAoo, final String documentTitle, final Connection conn) {
		PreparedStatement ps = null; 
		boolean output = false;
		try {
			int index = 1;
			StringBuilder sb = new StringBuilder();
			//Stato 0 significa che i record non sono ancora processati o sono andati in errore e devono essere riprocessati
			sb.append("DELETE FROM CODA_GG_NOTIF_SPED_CART WHERE IDAOO = ? AND DOCTITLE = ?  ");
			
			ps = conn.prepareStatement(sb.toString());
			ps.setInt(index++, idAoo.intValue()); 
			ps.setString(index++, documentTitle);  

			output = ps.executeUpdate() > 0;
		} catch(Exception ex) {
			LOGGER.error(ERRORE_DURANTE_L_INSERIMENTO_NELLA_TABELLA_DEI_RECORD_PROCESSATI,ex);
			throw new RedException(ERRORE_DURANTE_L_INSERIMENTO_NELLA_TABELLA_DEI_RECORD_PROCESSATI,ex);
		} finally {
			closeStatement(ps);
		}
		
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#getIdDestinatarioFromGuid(java.lang.String, java.lang.String, java.sql.Connection).
	 */
	@Override
	public Long getIdDestinatarioFromGuid(final String idMessaggio,final String emailMittente, final Connection conn) {
		Long idDestinatario = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			int index = 1;
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT IDDESTINATARIO FROM GESTIONENOTIFICHEEMAIL ");
			sb.append("WHERE IDMESSAGGIO = ? AND EMAILMITTENTE = ? ");
			
			ps = conn.prepareStatement(sb.toString());
			
			ps.setString(index++, idMessaggio);
			ps.setString(index++, emailMittente);
			
			rs = ps.executeQuery();
			if(rs.next()) {
				idDestinatario = rs.getLong("IDDESTINATARIO");
			} 
		} catch(Exception ex) {
			LOGGER.error("Errore durante il recupero dell'id destinatario dal guid :"+ex);
			throw new RedException("Errore durante il recupero dell'id destinatario dal guid :"+ex);
		} finally {
			closeStatement(ps, rs);
		} 
		return idDestinatario;
	}

	/**
	 * @see it.ibm.red.business.dao.IGestioneNotificheEmailDAO#getMail(java.lang.String, java.util.Date, java.sql.Connection).
	 */
	@Override
	public MessaggioEmailDTO getMail(final String idDocumentoLike, final Date dataRichiestaSpedizione, final Connection conn) {
		PreparedStatement ps = null; 
		ResultSet rs = null; 
		MessaggioEmailDTO output = new MessaggioEmailDTO();
		try {
			int index = 1;
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM GESTIONENOTIFICHEEMAIL WHERE IDDOCUMENTO = ? AND DATA_RICHIESTA_SPEDIZIONE = ?");
			ps = conn.prepareStatement(sb.toString());
			ps.setString(index++, idDocumentoLike);
			ps.setDate(index++, new java.sql.Date(dataRichiestaSpedizione.getTime()));
			
			rs = ps.executeQuery();
						
			while(rs.next()) {
				CodaEmail resultQuery = populateVO(rs);
				output = fromCodaEmailToDTO(resultQuery);
				output.setIdAoo(rs.getLong(IDAOO));
			}
			
		} catch (SQLException ex) {
			LOGGER.error(ERROR_AGGIORNAMENTO_STATO_NOTIFICA_MSG, ex);
			throw new RedException("Errore nell'aggiornamento dello stato della notifica", ex);
		} finally {
			closeStatement(ps,rs);
		}
		return output;
	} 
}