package it.ibm.red.business.dto.filenet;

import java.util.List;
import java.util.Map;

import javax.activation.DataHandler;

import it.ibm.red.business.dto.AbstractDTO;
import it.ibm.red.business.dto.ListSecurityDTO;

/**
 * DTO che definisce un documento Red FN.
 */
public class DocumentoRedFnDTO extends AbstractDTO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 8252874066347577367L;
	
	/**
	 * Document title.
	 */
	private String documentTitle;
	
	/**
	 * Guid.
	 */
	private String guid;
	
	/**
	 * Classe documentale.
	 */
	private String classeDocumentale;
	
	/**
	 * Folder.
	 */
	private String folder;
	
	/**
	 * Metadati documento.
	 */
	private transient Map<String, Object> metadati;
	
	/**
	 * Security documento.
	 */
	private ListSecurityDTO security;
	
	/**
	 * Allegati.
	 */
	private List<DocumentoRedFnDTO> allegati;
	
	/**
	 * Gestore dati.
	 */
	private transient DataHandler dataHandler;
	
	/**
	 * Content type.
	 */
	private String contentType;

	/**
	 * Restituisce il documentTitle.
	 * @return documentTitle
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Imposta il documentTitle.
	 * @param documentTitle
	 */
	public void setDocumentTitle(final String documentTitle) {
		this.documentTitle = documentTitle;
	}

	/**
	 * Restituisce il guid.
	 * @return guid
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * Imposta il guid.
	 * @param guid
	 */
	public void setGuid(final String guid) {
		this.guid = guid;
	}

	/**
	 * Restituisce la classe documentale del documento.
	 * @return classe documentale
	 */
	public String getClasseDocumentale() {
		return classeDocumentale;
	}

	/**
	 * Imposta la classe documentale del documento.
	 * @param classeDocumentale
	 */
	public void setClasseDocumentale(final String classeDocumentale) {
		this.classeDocumentale = classeDocumentale;
	}

	/**
	 * Restituisce la folder.
	 * @return folder
	 */
	public String getFolder() {
		return folder;
	}

	/**
	 * Imposta la folder.
	 * @param folder
	 */
	public void setFolder(final String folder) {
		this.folder = folder;
	}

	/**
	 * Restituisce la lista dei metadati.
	 * @return metadati
	 */
	public Map<String, Object> getMetadati() {
		return metadati;
	}

	/**
	 * Imposta la lista dei metadati.
	 * @param metadati
	 */
	public void setMetadati(final Map<String, Object> metadati) {
		this.metadati = metadati;
	}

	/**
	 * Restituisce le security.
	 * 
	 * @return ListSecurityDTO di SecurityDTO
	 */
	public ListSecurityDTO getSecurity() {
		return security;
	}

	/**
	 * Imposta le security del documento.
	 * @param security
	 */
	public void setSecurity(final ListSecurityDTO security) {
		this.security = security;
	}

	/**
	 * Restituisce la lista degli allegati.
	 * @return allegati
	 */
	public List<DocumentoRedFnDTO> getAllegati() {
		return allegati;
	}

	/**
	 * Imposta la lista degli allegati.
	 * @param allegati
	 */
	public void setAllegati(final List<DocumentoRedFnDTO> allegati) {
		this.allegati = allegati;
	}

	/**
	 * Restituisce il dataHandler.
	 * @return dataHandler
	 */
	public DataHandler getDataHandler() {
		return dataHandler;
	}

	/**
	 * Imposta il dataHandler.
	 * @param dataHandler
	 */
	public void setDataHandler(final DataHandler dataHandler) {
		this.dataHandler = dataHandler;
	}

	/**
	 * Restituisce il content type.
	 * @return content type
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * Imposta il content type.
	 * @param contentType
	 */
	public void setContentType(final String contentType) {
		this.contentType = contentType;
	}
}