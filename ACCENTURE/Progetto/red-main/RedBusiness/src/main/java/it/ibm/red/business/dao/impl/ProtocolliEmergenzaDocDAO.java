package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IProtocolliEmergenzaDocDAO;
import it.ibm.red.business.dto.ProtocolloEmergenzaDocDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.utils.StringUtils;

/**
 * @author a.dilegge
 */
@Repository
public class ProtocolliEmergenzaDocDAO extends AbstractDAO implements IProtocolliEmergenzaDocDAO {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 431465960425233940L;

	/**
	 * @see it.ibm.red.business.dao.IProtocolliEmergenzaDocDAO#isProtocolloEmergenzaPresent(java.lang.Integer,
	 *      java.lang.Integer, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public boolean isProtocolloEmergenzaPresent(final Integer numeroProtocolloEmergenza, final Integer annoProtocolloEmergenza, final Long idAoo,
			final Connection connection) {

		PreparedStatement ps = null;
		ResultSet rs = null;
		int count = 0;
		try {
			int index = 1;
			ps = connection.prepareStatement(
					"SELECT count(*) as num FROM protocolliemergenza_doc WHERE idaoo = ? and numeroprotocolloemergenza = ? and annoprotocolloemergenza = ? ");
			ps.setLong(index++, idAoo);
			ps.setInt(index++, numeroProtocolloEmergenza);
			ps.setInt(index++, annoProtocolloEmergenza);
			rs = ps.executeQuery();
			if (rs.next()) {
				count = rs.getInt("num");
			}
			return count > 0;
		} catch (final SQLException e) {
			throw new RedException("Errore durante la verifica della presenza del protocollo di emergenza " + numeroProtocolloEmergenza + "/" + annoProtocolloEmergenza, e);
		} finally {
			closeStatement(ps, rs);
		}

	}

	/**
	 * @see it.ibm.red.business.dao.IProtocolliEmergenzaDocDAO#insert(it.ibm.red.business.dto.ProtocolloEmergenzaDocDTO,
	 *      java.sql.Connection).
	 */
	@Override
	public int insert(final ProtocolloEmergenzaDocDTO dto, final Connection connection) {
		PreparedStatement ps = null;
		int count = 0;
		try {
			final StringBuilder sql = new StringBuilder("INSERT INTO PROTOCOLLIEMERGENZA_DOC ");
			sql.append(
					"(IDDOCUMENTO, IDAOO, IDPROTOCOLLOEMERGENZA, NUMEROPROTOCOLLOEMERGENZA, ANNOPROTOCOLLOEMERGENZA, DATAPROTOCOLLOEMERGENZA, TIPOPROTOCOLLO, MESSAGEIDNPS, IDPROTOCOLLO, NUMEROPROTOCOLLO, ANNOPROTOCOLLO, DATAPROTOCOLLO, DATARICONCILIAZIONE) ");
			sql.append("VALUES ");
			sql.append("(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

			ps = connection.prepareStatement(sql.toString());

			int index = 1;

			ps.setString(index++, dto.getIdDocumento());
			ps.setLong(index++, dto.getIdAoo());
			ps.setString(index++, dto.getIdProtocolloEmergenza());
			ps.setInt(index++, dto.getNumeroProtocolloEmergenza());
			ps.setInt(index++, dto.getAnnoProtocolloEmergenza());
			ps.setTimestamp(index++, new Timestamp(dto.getDataProtocolloEmergenza().getTime()));
			ps.setInt(index++, dto.getTipoProtocollo());
			if (StringUtils.isNullOrEmpty(dto.getIdProtocollo())) {
				ps.setNull(index++, Types.VARCHAR);
				ps.setNull(index++, Types.VARCHAR);
				ps.setNull(index++, Types.INTEGER);
				ps.setNull(index++, Types.INTEGER);
				ps.setNull(index++, Types.TIMESTAMP);
				ps.setNull(index++, Types.TIMESTAMP);
			} else {
				ps.setString(index++, dto.getMessageIdNPS());
				ps.setString(index++, dto.getIdProtocollo());
				ps.setInt(index++, dto.getNumeroProtocollo());
				ps.setInt(index++, dto.getAnnoProtocollo());
				ps.setTimestamp(index++, new Timestamp(dto.getDataProtocollo().getTime()));
				ps.setTimestamp(index++, new Timestamp(dto.getDataRiconciliazione().getTime()));
			}

			count = ps.executeUpdate();

		} catch (final SQLException e) {
			throw new RedException("Errore durante l'inserimento dei valori delle informazioni di emergenza", e);
		} finally {
			closeStatement(ps);
		}

		return count;
	}

	/**
	 * @see it.ibm.red.business.dao.IProtocolliEmergenzaDocDAO#getByIdDocumento(java.lang.String,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public ProtocolloEmergenzaDocDTO getByIdDocumento(final String idDocumento, final Long idAoo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ProtocolloEmergenzaDocDTO bean = null;

		try {

			int index = 1;
			ps = connection.prepareStatement("SELECT * FROM protocolliemergenza_doc WHERE iddocumento = ? and idaoo = ?");
			ps.setString(index++, idDocumento);
			ps.setLong(index++, idAoo);
			rs = ps.executeQuery();
			if (rs.next()) {
				bean = populateVO(rs);
			}

		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero delle informazioni di emergenza per il documento " + idDocumento + " dell'aoo " + idAoo, e);
		} finally {
			closeStatement(ps, rs);
		}
		return bean;
	}

	private static ProtocolloEmergenzaDocDTO populateVO(final ResultSet rs) throws SQLException {

		return new ProtocolloEmergenzaDocDTO(rs.getString("iddocumento"), rs.getLong("idaoo"), rs.getString("idprotocolloemergenza"), rs.getInt("numeroprotocolloemergenza"),
				rs.getInt("annoprotocolloemergenza"), new Date(rs.getTimestamp("dataprotocolloemergenza").getTime()), rs.getInt("tipoprotocollo"),
				rs.getString("messageidnps"), rs.getString("idProtocollo"), rs.getInt("numeroProtocollo"), rs.getInt("annoProtocollo"),
				rs.getTimestamp("dataProtocollo") != null ? new Date(rs.getTimestamp("dataProtocollo").getTime()) : null,
				rs.getTimestamp("dataRiconciliazione") != null ? new Date(rs.getTimestamp("dataRiconciliazione").getTime()) : null);

	}

	/**
	 * @see it.ibm.red.business.dao.IProtocolliEmergenzaDocDAO#isEmergenzaNotUpdated(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean isEmergenzaNotUpdated(final String idProtocolloEmergenza, final Connection connection) {

		PreparedStatement ps = null;
		ResultSet rs = null;
		int count = 0;
		try {
			int index = 1;
			ps = connection.prepareStatement("SELECT count(*) as num FROM protocolliemergenza_doc WHERE idprotocolloemergenza = ? and idprotocollo is null");
			ps.setString(index++, idProtocolloEmergenza);
			rs = ps.executeQuery();
			if (rs.next()) {
				count = rs.getInt("num");
			}
			return count > 0;
		} catch (final SQLException e) {
			throw new RedException("Errore durante la verifica del protocollo di emergenza " + idProtocolloEmergenza + " non aggiornato con l'ufficiale", e);
		} finally {
			closeStatement(ps, rs);
		}

	}

	/**
	 * @see it.ibm.red.business.dao.IProtocolliEmergenzaDocDAO#isMessageIdNPSPresent(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean isMessageIdNPSPresent(final String messageIdNPS, final Connection connection) {

		PreparedStatement ps = null;
		ResultSet rs = null;
		int count = 0;
		try {
			int index = 1;
			ps = connection.prepareStatement("SELECT count(*) as num FROM protocolliemergenza_doc WHERE messageidnps = ?");
			ps.setString(index++, messageIdNPS);
			rs = ps.executeQuery();
			if (rs.next()) {
				count = rs.getInt("num");
			}
			return count > 0;
		} catch (final SQLException e) {
			throw new RedException("Errore durante la verifica della presenza del messageIdNPS " + messageIdNPS, e);
		} finally {
			closeStatement(ps, rs);
		}

	}

	/**
	 * @see it.ibm.red.business.dao.IProtocolliEmergenzaDocDAO#riconcilia(it.ibm.red.business.dto.ProtocolloEmergenzaDocDTO,
	 *      java.sql.Connection).
	 */
	@Override
	public int riconcilia(final ProtocolloEmergenzaDocDTO dto, final Connection connection) {
		PreparedStatement ps = null;
		int count = 0;
		try {
			final StringBuilder sql = new StringBuilder("UPDATE PROTOCOLLIEMERGENZA_DOC ");
			sql.append("SET MESSAGEIDNPS = ?, IDPROTOCOLLO = ?, NUMEROPROTOCOLLO = ?, ANNOPROTOCOLLO = ?, DATAPROTOCOLLO = ?, DATARICONCILIAZIONE = ? ");
			sql.append("WHERE IDAOO = ? AND NUMEROPROTOCOLLOEMERGENZA = ? AND ANNOPROTOCOLLOEMERGENZA = ? AND TIPOPROTOCOLLO = ? ");
			sql.append("  AND NOT EXISTS (SELECT 1 FROM PROTOCOLLIEMERGENZA_DOC WHERE MESSAGEIDNPS = ?) ");

			ps = connection.prepareStatement(sql.toString());

			int index = 1;
			ps.setString(index++, dto.getMessageIdNPS());
			ps.setString(index++, dto.getIdProtocollo());
			ps.setInt(index++, dto.getNumeroProtocollo());
			ps.setInt(index++, dto.getAnnoProtocollo());
			ps.setTimestamp(index++, new Timestamp(dto.getDataProtocollo().getTime()));
			ps.setTimestamp(index++, new Timestamp(dto.getDataRiconciliazione().getTime()));
			ps.setLong(index++, dto.getIdAoo());
			ps.setInt(index++, dto.getNumeroProtocolloEmergenza());
			ps.setInt(index++, dto.getAnnoProtocolloEmergenza());
			ps.setInt(index++, dto.getTipoProtocollo());
			ps.setString(index++, dto.getMessageIdNPS());

			count = ps.executeUpdate();

		} catch (final SQLException e) {
			throw new RedException("Errore durante l'aggiornamento dei valori delle informazioni ufficiali per il protollo d'emergenza " + dto.getNumeroProtocolloEmergenza()
					+ "/" + dto.getAnnoProtocolloEmergenza() + " dell'aoo " + dto.getIdAoo(), e);
		} finally {
			closeStatement(ps);
		}

		return count;
	}

	/**
	 * @see it.ibm.red.business.dao.IProtocolliEmergenzaDocDAO#getByIdProtocollo(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public ProtocolloEmergenzaDocDTO getByIdProtocollo(final String idProtocollo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ProtocolloEmergenzaDocDTO bean = null;

		try {

			int index = 1;
			ps = connection.prepareStatement("SELECT * FROM PROTOCOLLIEMERGENZA_DOC WHERE IDPROTOCOLLO = ? ");
			ps.setString(index++, idProtocollo);
			rs = ps.executeQuery();
			if (rs.next()) {
				bean = populateVO(rs);
			}

		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero delle informazioni di emergenza per l'identificativo " + idProtocollo, e);
		} finally {
			closeStatement(ps, rs);
		}
		return bean;
	}

}