/**
 * 
 */
package it.ibm.red.business.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author APerquoti
 *
 */
public class ComboTipologiaDocumentoDTO extends AbstractDTO {

	/**
	 * Seriali.
	 */
	private static final long serialVersionUID = -148570898630673216L;

	/**
	 * Lista utilizzata per valorizzare la combo delle tipologie del Documento principale.
	 */
	private List<TipologiaDocumentoDTO> listTipologieDocumentoPrincipale;
	
	/**
	 * Lista utilizzata per valorizzare la combo delle tipologie Documento degli Allegati.
	 */
	private List<TipologiaDocumentoDTO> listTipologieDocumentoAllegati;

	/**
	 * Costruttore di classe.
	 */
	public ComboTipologiaDocumentoDTO() {
		listTipologieDocumentoAllegati = new ArrayList<>();
		listTipologieDocumentoPrincipale = new ArrayList<>();
	}
	
	/**
	 * @return the listTipologieDocumentoPrincipale
	 */
	public List<TipologiaDocumentoDTO> getListTipologieDocumentoPrincipale() {
		return listTipologieDocumentoPrincipale;
	}

	/**
	 * @param listTipologieDocumentoPrincipale the listTipologieDocumentoPrincipale to set
	 */
	public void setListTipologieDocumentoPrincipale(final List<TipologiaDocumentoDTO> listTipologieDocumentoPrincipale) {
		this.listTipologieDocumentoPrincipale = listTipologieDocumentoPrincipale;
	}

	/**
	 * @return the listTipologieDocumentoAllegati
	 */
	public List<TipologiaDocumentoDTO> getListTipologieDocumentoAllegati() {
		return listTipologieDocumentoAllegati;
	}

	/**
	 * @param listTipologieDocumentoAllegati the listTipologieDocumentoAllegati to set
	 */
	public void setListTipologieDocumentoAllegati(final List<TipologiaDocumentoDTO> listTipologieDocumentoAllegati) {
		this.listTipologieDocumentoAllegati = listTipologieDocumentoAllegati;
	}


}
