package it.ibm.red.business.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.enums.PostaEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.helper.signing.SignatureLayout;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class UtenteDTO.
 *
 * @author CPIERASC
 * 
 *         Data Transfer Object per un utente.
 */
public class UtenteDTO extends AbstractDTO {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -5720664442305382477L;

	/**
	 * Dirigente nodo.
	 */
	private String dirigenteNodo;

	/**
	 * Lista gruppi AD.
	 */
	private List<String> gruppiAD;

	/**
	 * Descrizione aoo.
	 */
	private String aooDesc;

	/**
	 * Codice aoo.
	 */
	private String codiceAoo;

	/**
	 * Cognome.
	 */
	private String cognome;

	/**
	 * Descrizione ente.
	 */
	private String descrizioneEnte;

	/**
	 * Credenziali filenet.
	 */
	private FilenetCredentialsDTO fcDTO;

	/**
	 * Homepage predefinita.
	 */
	private String homepage;

	/**
	 * Identificativo utente.
	 */
	private Long id;

	/**
	 * Identificativo utente delegante.
	 */
	private Long idUtenteDelegante;

	/**
	 * Identificativo aoo.
	 */
	private Long idAoo;

	/**
	 * Identificativo ruolo.
	 */
	private Long idRuolo;

	/**
	 * Identificativo permessi ruolo.
	 */
	private Long permessi;

	/**
	 * Identificativo permessi AOO.
	 */
	private Long permessiAOO;

	/**
	 * Identificativo tema prescelto.
	 */
	private Integer idTemaMobile;

	/**
	 * Ritardo ms autocomplete.
	 */
	private Integer autocompleteDelay;

	/**
	 * Identificativo tipo protocollo.
	 */
	private Long idTipoProtocollo;

	/**
	 * Identificativo ufficio.
	 */
	private Long idUfficio;

	/**
	 * Codice ufficio.
	 */
	private String codiceUfficio;

	/**
	 * Nome.
	 */
	private String nome;

	/**
	 * Descrizione ruolo.
	 */
	private String ruoloDesc;

	/**
	 * Informazioni firmatario.
	 */
	private SignerInfoDTO signerInfo;

	/**
	 * Descrizione ufficio.
	 */
	private String ufficioDesc;

	/**
	 * Descrizione nodo.
	 */
	private String nodoDesc;

	/**
	 * Lista coppie ufficio/ruolo a cui appartiene l'utente.
	 */
	private Map<UfficioDTO, Collection<RuoloDTO>> ufficioRuoli;

	/**
	 * Username.
	 */
	private String username;

	/**
	 * Accesso alla coda corriere.
	 */
	private boolean corriere;

	/**
	 * Accesso Scadenzario.
	 */
	private boolean tabScadenzario;

	/**
	 * Accesso alla cosa libro firma.
	 */
	private boolean libroFirma;

	/**
	 * accesso coda giro visti.
	 */
	private boolean giroVisti;

	/**
	 * permesso DSR.
	 */
	private boolean permessoDSR;

	/**
	 * Save PIN.
	 */
	private Boolean savePinAoo;

	/**
	 * Mock delle caselle di posta.
	 * 
	 * @author AndreaP
	 */
	private List<CasellaPostaDTO> casellePosta;

	/**
	 * Indica se puo' vedere il registro riservato e abilitare le check sulle
	 * maschere.
	 */
	private Boolean registroRiservato;

	/**
	 * Flag permesso AOO corte dei conti.
	 */
	private boolean permessoAooCorteDeiConti;

	/**
	 * Flag permesso aoo firma copia conforme.
	 */
	private boolean permessoAooFirmaCopiaConforme;

	/**
	 * Campo della tabella: AOO.IDNODORADICE.
	 */
	private Long idNodoRadiceAOO;

	/**
	 * Identificativo nodo corriere.
	 */
	private Long idNodoCorriere;

	/**
	 * Identificativo logo mef aoo.
	 */
	private Integer idLogoMefAoo;

	/**
	 * Idnetificativo tipo nodo.
	 */
	private Integer idTipoNodo;

	/**
	 * Preferenze.
	 */
	private PreferenzeDTO preferenzeApp;

	/**
	 * Dimensione massima entrata.
	 */
	private int maxSizeEntrata;

	/**
	 * Dimensione massima uscita.
	 */
	private int maxSizeUscita;

	/**
	 * visibilità faldoni..
	 */
	private String visFaldoni;

	/**
	 * Flag Segreteria Ufficio Corrente.
	 */
	private Integer flagSegreteriaUfficio;

	/**
	 * Identificativo Tipo Struttura Ufficio Corrente.
	 */
	private Integer idTipoStrutturaUfficio;

	/**
	 * Flag mantieni formato originale.
	 */
	private int mantieniFormatoOriginale;

	/**
	 * Flag inolta mail.
	 */
	private int flagInoltraMail;

	/**
	 * Flag forza refresh code.
	 */
	private int flagForzaRefreshCode;

	/**
	 * Tipo posta.
	 */
	private PostaEnum tipoPosta;

	/**
	 * Flag gestione applicativa.
	 */
	private boolean gestioneApplicativa;

	/**
	 * Id ruolo delegato libro firma.
	 */
	private Long idRuoloDelegatoLibroFirma;

	/**
	 * Flag ordinamento mail aoo ascendente.
	 */
	private Boolean ordinamentoMailAOOAsc;

	/**
	 * Flag ribalta titolario.
	 */
	private boolean ribaltaTitolario;

	/**
	 * Flag autocomplete rubrica.
	 */
	private boolean autocompleteRubricaCompleta;

	/**
	 * Flag stampa etichette.
	 */
	private boolean stampaEtichetteResponse;

	/**
	 * Codice fiascle.
	 */
	private String codFiscale;

	/**
	 * Flag show dialog aoo.
	 */
	private boolean showDialogAoo;

	/**
	 * Flag timbro uscita aoo.
	 */
	private boolean timbroUscitaAoo;

	/**
	 * Flag estendi visibilità.
	 */
	private boolean isEstendiVisibilita;

	/**
	 * Flag disable utilizza solo host.
	 */
	private boolean disableUseHostOnly;

	/**
	 * Flag visualizza storico.
	 */
	private boolean showRiferimentoStorico;

	/**
	 * Flag visualizza ricerca NSD.
	 */
	private boolean showRicercaNsd;

	/**
	 * Flag seleziona tutti i visti.
	 */
	private boolean showSelezionaTuttiVisto;

	/**
	 * Flag protocolla e mantieni.
	 */
	private boolean protocollaEMantieni;

	/**
	 * Flag download custom excel.
	 */
	private boolean downloadCustomExcel;

	/**
	 * Flag inoltra.
	 */
	private Boolean inoltraResponse;

	/**
	 * Flag rifiuta.
	 */
	private Boolean rifiutaResponse;

	/**
	 * Flag file non sbustato.
	 */
	private boolean fileNonSbustato;

	/**
	 * Flag stmpiglia allegati visibile.
	 */
	private boolean stampigliaAllegatiVisible;

	/**
	 * Identificativo assegnazione indiretta.
	 */
	private Long idNodoAssegnazioneIndiretta;

	/**
	 * Flag ucb.
	 */
	private boolean ucb;

	/**
	 * Flag univocità mail.
	 */
	private boolean checkUnivocitaMail;

	/**
	 * Numero massimo notifiche rubrica.
	 */
	private int maxNotificheRubrica;

	/**
	 * Numero massimo notifiche calendario.
	 */
	private int maxNotificheCalendario;

	/**
	 * Numero massimo notifiche sottoscrizioni.
	 */
	private int maxNotificheSottoscrizioni;

	/**
	 * Numero massimo notifiche non lette.
	 */
	private int maxNotificheNonLette;
	
	/**
	 * Flag allaccio NPS.
	 */
	private boolean showAllaccioNps;

	/**
	 * Identificativo ufficio.
	 */
	private Long idUfficioPadre;

	/**
	 * downloadSistemiEsterni.
	 */
	private boolean downloadSistemiEsterni;
	
	/**
	 * Flag associato alla postilla.
	 */
	private boolean postillaAttiva;
	
	/**
	 * Flag che stabilisce la presenza della postilla solo sulla prima pagina.
	 */
	private boolean postillaSoloPag1;
	
	/**
	 * Utente vuoto.
	 */
	public UtenteDTO() {
		super();
	}

	/**
	 * Costruttore.
	 * 
	 * @param inId					Identificativo utente
	 * @param inIdAoo				identificativo aoo
	 * @param inIdRuolo				identificativo ruolo
	 * @param inIdUfficio			identificativo ufficio
	 * @param inRuoloDesc			descrizione ruolo
	 * @param inUfficioDesc			descrizione ufficio
	 * @param inDescrizioneEnte		descrizione ente
	 * @param inAooDesc				descrizione aoo
	 * @param inCodiceAoo			codice aoo
	 */
	public UtenteDTO(final Long inId, final Long inIdAoo, final Long inIdRuolo, final Long inIdUfficio, final String inCodiceUfficio, final String inRuoloDesc, final String inUfficioDesc, 
			final String inNodoDesc, final String inDescrizioneEnte, final String inAooDesc, final String inCodiceAoo) {
		id = inId;
		idAoo = inIdAoo;
		idRuolo = inIdRuolo;
		idUfficio = inIdUfficio;
		codiceUfficio = inCodiceUfficio;
		ruoloDesc = inRuoloDesc;
		ufficioDesc = inUfficioDesc;
		nodoDesc = inNodoDesc;
		descrizioneEnte = inDescrizioneEnte;
		aooDesc = inAooDesc;
		codiceAoo = inCodiceAoo;
	}

	/**
	 * Costruttore.
	 * 
	 * @param utente model
	 */
	public UtenteDTO(final Utente utente) {
		if (utente == null) {
			return;
		}

		this.username = utente.getUsername();
		this.nome = utente.getNome();
		this.cognome = utente.getCognome();
		this.id = utente.getIdUtente();
		this.ufficioRuoli = new HashMap<>();
		if (utente.getUtenteFirma() != null) {
			this.signerInfo = new SignerInfoDTO(null, // Il PK Handler per la firma deve essere preso dall'AOO
					null, // Il PK Handler per tutto ciò che NON concerne la firma deve essere preso
							// dall'AOO
					utente.getUtenteFirma().getSigner(), utente.getUtenteFirma().getPin(), utente.getUtenteFirma().getSignerPin(), utente.getUtenteFirma().getReason(),
					utente.getUtenteFirma().getCustomerInfo(), utente.getUtenteFirma().getImageFirma(), SignatureLayout.EMPTY, utente.getUtenteFirma().isPinVerificato());
		}
		if (utente.getUtentePreferenze() != null) {
			preferenzeApp = new PreferenzeDTO(utente.getUtentePreferenze());
		}

		this.registroRiservato = Long.valueOf(1).equals(utente.getRegistroRiservato());
		this.codFiscale = utente.getCodiceFiscale();
	}

	/**
	 * Costruttore.
	 * 
	 * @param fcDto
	 * @param noDto
	 */
	public UtenteDTO(final FilenetCredentialsDTO fcDto, final NodoOrganigrammaDTO noDto) {
		super();
		this.fcDTO = fcDto;
		this.idUfficio = noDto.getIdNodo();
		this.id = noDto.getIdUtente();
		this.idAoo = noDto.getIdAOO();
		this.nome = noDto.getNomeUtente();
		this.cognome = noDto.getCognomeUtente();
		this.codiceUfficio = noDto.getCodiceNodo();
	}

	public UtenteDTO(UtenteDTO utenteToCopy) {
		super();
		this.dirigenteNodo = utenteToCopy.getDirigenteNodo();
		this.gruppiAD = utenteToCopy.getGruppiAD();
		this.aooDesc = utenteToCopy.getAooDesc();
		this.codiceAoo = utenteToCopy.getCodiceAoo();
		this.cognome = utenteToCopy.getCognome();
		this.descrizioneEnte = utenteToCopy.getDescrizioneEnte();
		this.fcDTO = utenteToCopy.getFcDTO();
		this.homepage = utenteToCopy.getHomepage();
		this.id = utenteToCopy.getId();
		this.idUtenteDelegante = utenteToCopy.getIdUtenteDelegante();
		this.idAoo = utenteToCopy.getIdAoo();
		this.idRuolo = utenteToCopy.getIdRuolo();
		this.permessi = utenteToCopy.getPermessi();
		this.permessiAOO = utenteToCopy.getPermessiAOO();
		this.idTemaMobile = utenteToCopy.getIdTemaMobile();
		this.autocompleteDelay = utenteToCopy.getAutocompleteDelay();
		this.idTipoProtocollo = utenteToCopy.getIdTipoProtocollo();
		this.idUfficio = utenteToCopy.getIdUfficio();
		this.codiceUfficio = utenteToCopy.getCodiceUfficio();
		this.nome = utenteToCopy.getNome();
		this.ruoloDesc = utenteToCopy.getRuoloDesc();
		this.signerInfo = utenteToCopy.getSignerInfo();
		this.ufficioDesc = utenteToCopy.getUfficioDesc();
		this.nodoDesc = utenteToCopy.getNodoDesc();
		this.ufficioRuoli = utenteToCopy.getUfficioRuoli();
		this.username = utenteToCopy.getUsername();
		this.corriere = utenteToCopy.isCorriere();
		this.tabScadenzario = utenteToCopy.isTabScadenzario();
		this.libroFirma = utenteToCopy.isLibroFirma();
		this.giroVisti = utenteToCopy.isGiroVisti();
		this.permessoDSR = utenteToCopy.isPermessoDSR();
		this.savePinAoo = utenteToCopy.getSavePinAoo();
		this.casellePosta = utenteToCopy.getCasellePosta();
		this.registroRiservato = utenteToCopy.getRegistroRiservato();
		this.permessoAooCorteDeiConti = utenteToCopy.isPermessoAooCorteDeiConti();
		this.permessoAooFirmaCopiaConforme = utenteToCopy.isPermessoAooFirmaCopiaConforme();
		this.idNodoRadiceAOO = utenteToCopy.getIdNodoRadiceAOO();
		this.idNodoCorriere = utenteToCopy.getIdNodoCorriere();
		this.idLogoMefAoo = utenteToCopy.getIdLogoMefAoo();
		this.idTipoNodo = utenteToCopy.getIdTipoNodo();
		this.preferenzeApp = utenteToCopy.getPreferenzeApp();
		this.maxSizeEntrata = utenteToCopy.getMaxSizeEntrata();
		this.maxSizeUscita = utenteToCopy.getMaxSizeUscita();
		this.visFaldoni = utenteToCopy.getVisFaldoni();
		this.flagSegreteriaUfficio = utenteToCopy.getFlagSegreteriaUfficio();
		this.idTipoStrutturaUfficio = utenteToCopy.getIdTipoStrutturaUfficio();
		this.mantieniFormatoOriginale = utenteToCopy.getMantieniFormatoOriginale();
		this.flagInoltraMail = utenteToCopy.getFlagInoltraMail();
		this.flagForzaRefreshCode = utenteToCopy.getFlagForzaRefreshCode();
		this.tipoPosta = utenteToCopy.getTipoPosta();
		this.gestioneApplicativa = utenteToCopy.isGestioneApplicativa();
		this.idRuoloDelegatoLibroFirma = utenteToCopy.getIdRuoloDelegatoLibroFirma();
		this.ordinamentoMailAOOAsc = utenteToCopy.isOrdinamentoMailAOOAsc();
		this.ribaltaTitolario = utenteToCopy.isRibaltaTitolario();
		this.autocompleteRubricaCompleta = utenteToCopy.isAutocompleteRubricaCompleta();
		this.stampaEtichetteResponse = utenteToCopy.getStampaEtichetteResponse();
		this.codFiscale = utenteToCopy.getCodFiscale();
		this.showDialogAoo = utenteToCopy.getShowDialogAoo();
		this.timbroUscitaAoo = utenteToCopy.getTimbroUscitaAoo();
		this.isEstendiVisibilita = utenteToCopy.getIsEstendiVisibilita();
		this.disableUseHostOnly = utenteToCopy.getDisableUseHostOnly();
		this.showRiferimentoStorico = utenteToCopy.getShowRiferimentoStorico();
		this.showRicercaNsd = utenteToCopy.getShowRicercaNsd();
		this.showSelezionaTuttiVisto = utenteToCopy.isShowSelezionaTuttiVisto();
		this.protocollaEMantieni = utenteToCopy.getProtocollaEMantieni();
		this.downloadCustomExcel = utenteToCopy.getDownloadCustomExcel();
		this.inoltraResponse = utenteToCopy.getInoltraResponse();
		this.rifiutaResponse = utenteToCopy.getRifiutaResponse();
		this.fileNonSbustato = utenteToCopy.getFileNonSbustato();
		this.stampigliaAllegatiVisible = utenteToCopy.getStampigliaAllegatiVisible();
		this.idNodoAssegnazioneIndiretta = utenteToCopy.getIdNodoAssegnazioneIndiretta();
		this.ucb = utenteToCopy.isUcb();
		this.checkUnivocitaMail = utenteToCopy.getCheckUnivocitaMail();
		this.maxNotificheRubrica = utenteToCopy.getMaxNotificheRubrica();
		this.maxNotificheCalendario = utenteToCopy.getMaxNotificheCalendario();
		this.maxNotificheSottoscrizioni = utenteToCopy.getMaxNotificheSottoscrizioni();
		this.maxNotificheNonLette = utenteToCopy.getMaxNotificheNonLette();
		this.showAllaccioNps = utenteToCopy.isShowAllaccioNps();
		this.idUfficioPadre = utenteToCopy.getIdUfficioPadre();
		this.downloadSistemiEsterni = utenteToCopy.isDownloadSistemiEsterni();
		this.postillaAttiva = utenteToCopy.isPostillaAttiva();
		this.postillaSoloPag1 = utenteToCopy.isPostillaSoloPag1();
	}

	/**
	 * Metodo per aggiungere una coppia ufficio/ruolo (un utente può afferire a più
	 * uffici, e su ciascuno di essi assumere uno specifico ruolo).
	 * 
	 * @param ufficio ufficio
	 * @param ruolo   ruolo
	 */
	public final void addUfficioRuolo(final UfficioDTO ufficio, final RuoloDTO ruolo) {
		if (ufficioRuoli == null) {
			ufficioRuoli = new HashMap<>();
		}
		Collection<RuoloDTO> ruoliUfficio = ufficioRuoli.get(ufficio);
		if (ruoliUfficio == null) {
			ruoliUfficio = new ArrayList<>();
		}
		ruoliUfficio.add(ruolo);
		ufficioRuoli.put(ufficio, ruoliUfficio);
	}
	
	/**
	 * Restituisce la lista dei gruppi AD.
	 * 
	 * @return gruppi AD
	 */
	public List<String> getGruppiAD() {
		return gruppiAD;
	}

	/**
	 * Imposta la lista dei gruppi AD.
	 * 
	 * @param gruppiAD
	 */
	public void setGruppiAD(final List<String> gruppiAD) {
		this.gruppiAD = gruppiAD;
	}

	/**
	 * Getter.
	 * 
	 * @return descrizione aoo
	 */
	public final String getAooDesc() {
		return aooDesc;
	}

	/**
	 * Getter.
	 * 
	 * @return codice aoo
	 */
	public final String getCodiceAoo() {
		return codiceAoo;
	}

	/**
	 * Getter.
	 * 
	 * @return cognome
	 */
	public final String getCognome() {
		return cognome;
	}

	/**
	 * Getter.
	 * 
	 * @return descrizione
	 */
	public final String getDescrizione() {
		return nome + " " + cognome;
	}

	/**
	 * Getter.
	 * 
	 * @return descrizione ente
	 */
	public final String getDescrizioneEnte() {
		return descrizioneEnte;
	}

	/**
	 * Getter.
	 * 
	 * @return credenziali filenet
	 */
	public final FilenetCredentialsDTO getFcDTO() {
		return fcDTO;
	}

	/**
	 * Getter.
	 * 
	 * @return homepage prescelta dall'utente
	 */
	public final String getHomepage() {
		return homepage;
	}

	/**
	 * Getter.
	 * 
	 * @return identificativo utente
	 */
	public final Long getId() {
		return id;
	}

	/**
	 * Setter.
	 * 
	 * @param id
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * Getter.
	 * 
	 * @return identificativo aoo
	 */
	public final Long getIdAoo() {
		return idAoo;
	}

	/**
	 * Getter.
	 * 
	 * @return identificativo ruolo
	 */
	public final Long getIdRuolo() {
		return idRuolo;
	}

	/**
	 * Getter.
	 * 
	 * @return identificativo tema mobile
	 */
	public final Integer getIdTemaMobile() {
		return idTemaMobile;
	}

	/**
	 * Getter.
	 * 
	 * @return identificativo tipo protocollo
	 */
	public final Long getIdTipoProtocollo() {
		return idTipoProtocollo;
	}

	/**
	 * Getter.
	 * 
	 * @return identificativo ufficio
	 */
	public final Long getIdUfficio() {
		return idUfficio;
	}

	/**
	 * Getter.
	 * 
	 * @return nome
	 */
	public final String getNome() {
		return nome;
	}

	/**
	 * Getter.
	 * 
	 * @return descrizione ruolo
	 */
	public final String getRuoloDesc() {
		return ruoloDesc;
	}

	/**
	 * Getter.
	 * 
	 * @return informazioni firmatario
	 */
	public final SignerInfoDTO getSignerInfo() {
		return signerInfo;
	}

	/**
	 * Setter.
	 * 
	 * 
	 */
	public void setSignerInfo(final SignerInfoDTO signerInfo) {
		this.signerInfo = signerInfo;
	}

	/**
	 * Getter.
	 * 
	 * @return descrizione ufficio
	 */
	public final String getUfficioDesc() {
		return ufficioDesc;
	}

	/**
	 * Getter.
	 * 
	 * @return insieme coppie ufficio/ruolo
	 */
	public final Map<UfficioDTO, Collection<RuoloDTO>> getUfficioRuoli() {
		return ufficioRuoli;
	}

	/**
	 * Getter.
	 * 
	 * @return username
	 */
	public final String getUsername() {
		return username;
	}

	/**
	 * Setter.
	 * 
	 * param username
	 */
	public final void setUsername(final String inUsername) {
		this.username = inUsername;
	}

	/**
	 * Setter.
	 * 
	 * @param inAooDesc	descrizione aoo
	 */
	public final void setAooDesc(final String inAooDesc) {
		this.aooDesc = inAooDesc;
	}

	/**
	 * Setter.
	 * 
	 * @param inCodiceAoo codice aoo
	 */
	public final void setCodiceAoo(final String inCodiceAoo) {
		this.codiceAoo = inCodiceAoo;
	}

	/**
	 * Setter.
	 * 
	 * @param inDescrizioneEnte descrizione ente
	 */
	public final void setDescrizioneEnte(final String inDescrizioneEnte) {
		this.descrizioneEnte = inDescrizioneEnte;
	}

	/**
	 * Setter.
	 * 
	 * @param inFcDTO credenziali filenet
	 */
	public final void setFcDTO(final FilenetCredentialsDTO inFcDTO) {
		this.fcDTO = inFcDTO;
	}

	/**
	 * Setter.
	 * 
	 * @param inIdAoo identificativo aoo
	 */
	public final void setIdAoo(final Long inIdAoo) {
		this.idAoo = inIdAoo;
	}

	/**
	 * Setter.
	 * 
	 * @param inIdRuolo identificativo ruolo
	 */
	public final void setIdRuolo(final Long inIdRuolo) {
		this.idRuolo = inIdRuolo;
	}

	/**
	 * Setter.
	 * 
	 * @param inIdTipoProtocollo identificativo tipo protocollo
	 */
	public final void setIdTipoProtocollo(final Long inIdTipoProtocollo) {
		this.idTipoProtocollo = inIdTipoProtocollo;
	}

	/**
	 * Setter.
	 * 
	 * @param inIdUfficio identificativo ufficio
	 */
	public final void setIdUfficio(final Long inIdUfficio) {
		this.idUfficio = inIdUfficio;
	}

	/**
	 * Restituisce il codice dell'ufficio.
	 * 
	 * @return codice ufficio utente
	 */
	public String getCodiceUfficio() {
		return codiceUfficio;
	}

	/**
	 * Imposta il codice dell'uffico dell'utente.
	 * 
	 * @param codiceUfficio
	 */
	public void setCodiceUfficio(final String codiceUfficio) {
		this.codiceUfficio = codiceUfficio;
	}

	/**
	 * Setter.
	 * 
	 * @param inRuoloDesc descrizione ruolo
	 */
	public final void setRuoloDesc(final String inRuoloDesc) {
		this.ruoloDesc = inRuoloDesc;
	}

	/**
	 * Setter.
	 * 
	 * @param inUfficioDesc descrizione ufficio
	 */
	public final void setUfficioDesc(final String inUfficioDesc) {
		this.ufficioDesc = inUfficioDesc;
	}

	/**
	 * Getter accesso alla coda corriere.
	 * 
	 * @return accesso alla coda corriere
	 */
	public final boolean isCorriere() {
		return corriere;
	}

	/**
	 * Setter.
	 * 
	 * @param inCorriere accesso alla coda corriere
	 */
	public final void setCorriere(final boolean inCorriere) {
		this.corriere = inCorriere;
	}

	/**
	 * Getter accesso al tab scadenzario.
	 * 
	 * @return accesso al tab scadenzario
	 */
	public final boolean isTabScadenzario() {
		return tabScadenzario;
	}

	/**
	 * Setter.
	 * 
	 * @param inTabScadenzario - Accesso al tab scadenzario
	 */
	public final void setTabScadenzario(final boolean inTabScadenzario) {
		this.tabScadenzario = inTabScadenzario;
	}

	/**
	 * Getter accesso alla coda libro firma.
	 * 
	 * @return accesso alla coda libro firma
	 */
	public final boolean isLibroFirma() {
		return libroFirma;
	}

	/**
	 * Setter.
	 * 
	 * @param inLibroFirma - Accesso alla coda libro firma
	 */
	public final void setLibroFirma(final boolean inLibroFirma) {
		this.libroFirma = inLibroFirma;
	}

	/**
	 * Getter.
	 * 
	 * @return Identificativo dell'utente delegante
	 */
	public final Long getIdUtenteDelegante() {
		return idUtenteDelegante;
	}

	/**
	 * Setter.
	 * 
	 * @param inIdUtenteDelegante - Identificativo dell'utente delegante
	 */
	public final void setIdUtenteDelegante(final Long inIdUtenteDelegante) {
		this.idUtenteDelegante = inIdUtenteDelegante;
	}

	/**
	 * Metodo per testare se un utente è amministratore.
	 * 
	 * @return flag
	 */
	public final boolean hasRuoloAmministratore() {
		final String idRuoloAmministratoreAoo = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_RUOLO_AMMINISTRATORE_AOO);
		Long nIdRuolo = null;
		if (!StringUtils.isNullOrEmpty(idRuoloAmministratoreAoo)) {
			nIdRuolo = Long.valueOf(idRuoloAmministratoreAoo);
		}
		return this.idRuolo != null && this.idRuolo.equals(nIdRuolo);
	}

	/**
	 * . tags:
	 * 
	 * @return
	 */
	@Override
	public final int hashCode() {
		final int prime = 31;
		int result = 1;
		int hashCode = 0;
		if (id != null) {
			hashCode = id.hashCode();
		}
		result = prime * result + (hashCode);
		hashCode = 0;
		if (idRuolo != null) {
			hashCode = idRuolo.hashCode();
		}
		result = prime * result + (hashCode);
		hashCode = 0;
		if (idUfficio != null) {
			hashCode = idUfficio.hashCode();
		}
		result = prime * result + (hashCode);
		hashCode = 0;
		if (username != null) {
			hashCode = username.hashCode();
		}
		result = prime * result + (hashCode);
		return result;
	}

	/**
	 * Equals.
	 *
	 * @param obj the obj
	 * @return true, if successful
	 */
	@Override
	public final boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final UtenteDTO other = (UtenteDTO) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		if (idRuolo == null) {
			if (other.idRuolo != null) {
				return false;
			}
		} else if (!idRuolo.equals(other.idRuolo)) {
			return false;
		}
		if (idUfficio == null) {
			if (other.idUfficio != null) {
				return false;
			}
		} else if (!idUfficio.equals(other.idUfficio)) {
			return false;
		}
		if (username == null) {
			if (other.username != null) {
				return false;
			}
		} else if (!username.equals(other.username)) {
			return false;
		}
		return true;
	}

	/**
	 * Save PIN AOO.
	 * 
	 * @return Save PIN
	 */
	public final Boolean getSavePinAoo() {
		return savePinAoo;
	}

	/**
	 * Setter SAVE PIN.
	 * 
	 * @param savePin - SAVE PIN
	 */
	public final void setSavePinAoo(final boolean savePin) {
		this.savePinAoo = savePin;
	}

	/**
	 * Getter.
	 * 
	 * @return caselle posta
	 */
	public List<CasellaPostaDTO> getCasellePosta() {
		return casellePosta;
	}

	/**
	 * Setter.
	 * 
	 * @param inCasellePosta caselle postali
	 */
	public void setCasellePosta(final List<CasellaPostaDTO> inCasellePosta) {
		this.casellePosta = inCasellePosta;
	}

	/**
	 * @return the giroVisti
	 */
	public final boolean isGiroVisti() {
		return giroVisti;
	}

	/**
	 * @param giroVisti the giroVisti to set
	 */
	public final void setGiroVisti(final boolean giroVisti) {
		this.giroVisti = giroVisti;
	}

	/**
	 * Restituisce i permessi dell'utente.
	 * 
	 * @return permessi utente
	 */
	public Long getPermessi() {
		return permessi;
	}

	/**
	 * Imposta i permessi dell'utente.
	 * 
	 * @param permessi
	 */
	public void setPermessi(final Long permessi) {
		this.permessi = permessi;
	}

	/**
	 * Restituisce i permessi dell'AOO.
	 * 
	 * @return permessi AOO
	 */
	public Long getPermessiAOO() {
		return permessiAOO;
	}

	/**
	 * Imposta i permessi dell'AOO.
	 * 
	 * @param permessiAOO
	 */
	public void setPermessiAOO(final Long permessiAOO) {
		this.permessiAOO = permessiAOO;
	}

	/**
	 * @return the permessoDSR
	 */
	public boolean isPermessoDSR() {
		return permessoDSR;
	}

	/**
	 * @param permessoDSR the permessoDSR to set
	 */
	public void setPermessoDSR(final boolean permessoDSR) {
		this.permessoDSR = permessoDSR;
	}

	/**
	 * Restituisce il flag associato al registro riservato.
	 * 
	 * @return flag associato al registro riservato
	 */
	public Boolean getRegistroRiservato() {
		return registroRiservato;
	}

	/**
	 * Restituisce il flag associato al permesso Aoo Corte dei conti.
	 * 
	 * @return flag associato al permesso Aoo Corte dei conti
	 */
	public boolean isPermessoAooCorteDeiConti() {
		return permessoAooCorteDeiConti;
	}

	/**
	 * Imposta il flag associato al permesso Aoo Corte dei conti.
	 * 
	 * @param permessoAooCorteDeiConti
	 */
	public void setPermessoAooCorteDeiConti(final boolean permessoAooCorteDeiConti) {
		this.permessoAooCorteDeiConti = permessoAooCorteDeiConti;
	}

	/**
	 * Restituisce il flag associato al permesso Aoo firma copia conforme.
	 * 
	 * @return flag associato al permesso Aoo firma copia conforme
	 */
	public boolean isPermessoAooFirmaCopiaConforme() {
		return permessoAooFirmaCopiaConforme;
	}

	/**
	 * Imposta il flag associato al permesso Aoo firma copia conforme.
	 * 
	 * @param permessoAooFirmaCopiaConforme
	 */
	public void setPermessoAooFirmaCopiaConforme(final boolean permessoAooFirmaCopiaConforme) {
		this.permessoAooFirmaCopiaConforme = permessoAooFirmaCopiaConforme;
	}

	/**
	 * Restituisce l'id del nodo radice dell'AOO.
	 * 
	 * @return id nodo radice AOO
	 */
	public Long getIdNodoRadiceAOO() {
		return idNodoRadiceAOO;
	}

	/**
	 * Imposta l'id del nodo radice dell'AOO.
	 * 
	 * @param idNodoRadiceAOO
	 */
	public void setIdNodoRadiceAOO(final Long idNodoRadiceAOO) {
		this.idNodoRadiceAOO = idNodoRadiceAOO;
	}

	/**
	 * Imposta il cognome dell'utente.
	 * 
	 * @param cognome
	 */
	public void setCognome(final String cognome) {
		this.cognome = cognome;
	}

	/**
	 * Imposta il nome dell'utente.
	 * 
	 * @param nome
	 */
	public void setNome(final String nome) {
		this.nome = nome;
	}

	/**
	 * Restituisce la descrizione del nodo utente.
	 * 
	 * @return descrizione nodo
	 */
	public String getNodoDesc() {
		return nodoDesc;
	}

	/**
	 * Imposta la descrizione del nodo utente.
	 * 
	 * @param nodoDesc
	 */
	public void setNodoDesc(final String nodoDesc) {
		this.nodoDesc = nodoDesc;
	}

	/**
	 * Restituisce l'id del nodo corriere.
	 * 
	 * @return id nodo corriere
	 */
	public Long getIdNodoCorriere() {
		return idNodoCorriere;
	}

	/**
	 * Imposta l'id del nodo corriere.
	 * 
	 * @param idNodoCorriere
	 */
	public void setIdNodoCorriere(final Long idNodoCorriere) {
		this.idNodoCorriere = idNodoCorriere;
	}

	/**
	 * Restituisce l'id del logo MEF AOO.
	 * 
	 * @return id del logo MEF AOO
	 */
	public Integer getIdLogoMefAoo() {
		return idLogoMefAoo;
	}

	/**
	 * Imposta l'id del logo MEF AOO.
	 * 
	 * @param idLogoMefAoo
	 */
	public void setIdLogoMefAoo(final Integer idLogoMefAoo) {
		this.idLogoMefAoo = idLogoMefAoo;
	}

	/**
	 * Restituisce l'id del tipo nodo.
	 * 
	 * @return id del tipo nodo
	 */
	public Integer getIdTipoNodo() {
		return idTipoNodo;
	}

	/**
	 * Imposta l'id del tipo nodo.
	 * 
	 * @param idTipoNodo
	 */
	public void setIdTipoNodo(final Integer idTipoNodo) {
		this.idTipoNodo = idTipoNodo;
	}

	/**
	 * Restitusice le preferenze APP.
	 * 
	 * @return preferenza APP
	 */
	public final PreferenzeDTO getPreferenzeApp() {
		return preferenzeApp;
	}

	/**
	 * Imposta le preferenze APP.
	 * 
	 * @param preferenzeApp
	 */
	public final void setPreferenzeApp(final PreferenzeDTO preferenzeApp) {
		this.preferenzeApp = preferenzeApp;
	}

	/**
	 * Restituisce il delay dell'autocompletamento.
	 * 
	 * @return delay autocompletamento
	 */
	public Integer getAutocompleteDelay() {
		return autocompleteDelay;
	}

	/**
	 * Restituisce la dimensione massima entrata.
	 * 
	 * @return dimensione massima entrata
	 */
	public int getMaxSizeEntrata() {
		return maxSizeEntrata;
	}

	/**
	 * Restitusice la dimensione massima uscita.
	 * 
	 * @return dimensione massima uscita
	 */
	public int getMaxSizeUscita() {
		return maxSizeUscita;
	}

	/**
	 * Imposta la dimensione massima entrata.
	 * 
	 * @param maxSizeEntrata
	 */
	public void setMaxSizeEntrata(final int maxSizeEntrata) {
		this.maxSizeEntrata = maxSizeEntrata;
	}

	/**
	 * Imposta la dimensione massima uscita.
	 * 
	 * @param maxSizeUscita
	 */
	public void setMaxSizeUscita(final int maxSizeUscita) {
		this.maxSizeUscita = maxSizeUscita;
	}

	/**
	 * Imposta il nodo dirigente.
	 * 
	 * @param inDirigenteNodo
	 */
	public void setDirigenteNodo(final String inDirigenteNodo) {
		dirigenteNodo = inDirigenteNodo;
	}

	/**
	 * Restituisce il nodo dirigente.
	 * 
	 * @return nodo dirigente
	 */
	public String getDirigenteNodo() {
		return dirigenteNodo;
	}

	/**
	 * @return the visFaldoni
	 */
	public final String getVisFaldoni() {
		return visFaldoni;
	}

	/**
	 * @param visFaldoni the visFaldoni to set
	 */
	public final void setVisFaldoni(final String visFaldoni) {
		this.visFaldoni = visFaldoni;
	}

	/**
	 * @return the flagSegreteriaUfficio
	 */
	public final Integer getFlagSegreteriaUfficio() {
		return flagSegreteriaUfficio;
	}

	/**
	 * @param flagSegreteriaUfficio the flagSegreteriaUfficio to set
	 */
	public final void setFlagSegreteriaUfficio(final Integer flagSegreteriaUfficio) {
		this.flagSegreteriaUfficio = flagSegreteriaUfficio;
	}

	/**
	 * @return the idTipoStrutturaUfficio
	 */
	public final Integer getIdTipoStrutturaUfficio() {
		return idTipoStrutturaUfficio;
	}

	/**
	 * @param idTipoStrutturaUfficio the idTipoStrutturaUfficio to set
	 */
	public final void setIdTipoStrutturaUfficio(final Integer idTipoStrutturaUfficio) {
		this.idTipoStrutturaUfficio = idTipoStrutturaUfficio;
	}

	/**
	 * Restituisce l'intero associato al mantenimento del formato originale.
	 * 
	 * @return intero associato al mantenimento del formato originale
	 */
	public int getMantieniFormatoOriginale() {
		return mantieniFormatoOriginale;
	}

	/**
	 * Imposta l'intero associato al mantenimento del formato originale.
	 * 
	 * @param mantieniFormatoOriginale
	 */
	public void setMantieniFormatoOriginale(final int mantieniFormatoOriginale) {
		this.mantieniFormatoOriginale = mantieniFormatoOriginale;
	}

	/**
	 * Restituisce flag inoltra mail.
	 * 
	 * @return flag inoltra mail
	 */
	public int getFlagInoltraMail() {
		return flagInoltraMail;
	}

	/**
	 * Imposta il flag inoltra mail.
	 * 
	 * @param flagInoltraMail
	 */
	public void setFlagInoltraMail(final int flagInoltraMail) {
		this.flagInoltraMail = flagInoltraMail;
	}

	/**
	 * Restituisce il flag associato alla forzatura del refresh delle code.
	 * 
	 * @return flag associato al refresh delle code
	 */
	public int getFlagForzaRefreshCode() {
		return flagForzaRefreshCode;
	}

	/**
	 * Imposta il flag associato alla forzatura del refresh delle code.
	 * 
	 * @param flagForzaRefreshCode
	 */
	public void setFlagForzaRefreshCode(final int flagForzaRefreshCode) {
		this.flagForzaRefreshCode = flagForzaRefreshCode;
	}

	/**
	 * Restituisce true se l'utente è un utente di gestione applicativa, false
	 * altrimenti.
	 * 
	 * @return true se utente GA, false altrimenti
	 */
	public boolean isGestioneApplicativa() {
		return gestioneApplicativa;
	}

	/**
	 * Imposta il flag associato alla gestione applicativa dell'utente.
	 * 
	 * @param gestioneApplicativa
	 */
	public void setGestioneApplicativa(final boolean gestioneApplicativa) {
		this.gestioneApplicativa = gestioneApplicativa;
	}

	/**
	 * Restituisce l'id del ruolo delegato a libro firma.
	 * 
	 * @return id ruolo delegato libro firma
	 */
	public Long getIdRuoloDelegatoLibroFirma() {
		return idRuoloDelegatoLibroFirma;
	}

	/**
	 * Imposta l'id del ruolo delegato a libro firma.
	 * 
	 * @param idRuoloDelegatoLibroFirma
	 */
	public void setIdRuoloDelegatoLibroFirma(final Long idRuoloDelegatoLibroFirma) {
		this.idRuoloDelegatoLibroFirma = idRuoloDelegatoLibroFirma;
	}

	/**
	 * Restituisce l'id dell'ufficio dell'assegnazione indiretta.
	 * 
	 * @return id nodo assegnazione indiretta
	 */
	public Long getIdNodoAssegnazioneIndiretta() {
		return idNodoAssegnazioneIndiretta;
	}

	/**
	 * Imposta l'id dell'ufficio dell'assegnazione indiretta.
	 * 
	 * @param idNodoAssegnazioneIndiretta
	 */
	public void setIdNodoAssegnazioneIndiretta(final Long idNodoAssegnazioneIndiretta) {
		this.idNodoAssegnazioneIndiretta = idNodoAssegnazioneIndiretta;
	}

	/**
	 * Restituisce il tipo posta.
	 * 
	 * @return tipo posta
	 */
	public PostaEnum getTipoPosta() {
		return tipoPosta;
	}

	/**
	 * Imposta il tipo posta.
	 * 
	 * @param tipoPosta
	 */
	public void setTipoPosta(final PostaEnum tipoPosta) {
		this.tipoPosta = tipoPosta;
	}

	/**
	 * Restituisce true se l'ordinamento mail è ascendente, false se discendente.
	 * 
	 * @return true se l'ordinamento mail è ascendente, false se discendente
	 */
	public Boolean isOrdinamentoMailAOOAsc() {
		return ordinamentoMailAOOAsc;
	}

	/**
	 * Imposta il flag associato all'ordinamento delle mail.
	 * 
	 * @param ordinamentoMailAOOAsc
	 */
	public void setOrdinamentoMailAOOAsc(final Integer ordinamentoMailAOOAsc) {
		if (ordinamentoMailAOOAsc != null && ordinamentoMailAOOAsc == 0) {
			this.ordinamentoMailAOOAsc = false;
		} else if (ordinamentoMailAOOAsc != null) {
			this.ordinamentoMailAOOAsc = true;
		}
	}

	/**
	 * Restuisce il flag associato al ribaltamento del titolario.
	 * 
	 * @return flag associato al ribaltamento titolario
	 */
	public boolean isRibaltaTitolario() {
		return ribaltaTitolario;
	}

	/**
	 * Imposta il flag associato al ribaltamento del titolario.
	 * 
	 * @param ribaltaTitolario
	 */
	public void setRibaltaTitolario(final boolean ribaltaTitolario) {
		this.ribaltaTitolario = ribaltaTitolario;
	}

	/**
	 * Restituisce true se l'autocompletamento della rubrica è completo, false
	 * altrimenti.
	 * 
	 * @return true se l'autocompletamento della rubrica è completo, false
	 *         altrimenti
	 */
	public boolean isAutocompleteRubricaCompleta() {
		return autocompleteRubricaCompleta;
	}

	/**
	 * Imposta il flag associato al completamento dell'autocompletamento.
	 * 
	 * @param autocompleteRubricaCompleta
	 */
	public void setAutocompleteRubricaCompleta(final boolean autocompleteRubricaCompleta) {
		this.autocompleteRubricaCompleta = autocompleteRubricaCompleta;
	}

	/**
	 * Restituisce true se l'id ruolo è un ruolo dirigente, false altrimenti.
	 * 
	 * @param codiceAOO
	 * @param idRuolo
	 * @return true se l'id ruolo è un ruolo dirigente, false altrimenti
	 */
	public static final boolean hasRuoloDirigente(final String codiceAOO, final Long idRuolo) {
		boolean isRuoloDirigente = false;

		final String ruoliKey = codiceAOO + "." + PropertiesNameEnum.RUOLI_PREASSEGNATARIO.getKey();
		final PropertiesProvider pp = PropertiesProvider.getIstance();
		final String idRuoliDirigente = pp.getParameterByString(ruoliKey);

		if (idRuoliDirigente != null) {
			final String[] ruoliSingoliDirigente = idRuoliDirigente.split(",");
			for (int i = 0; i < ruoliSingoliDirigente.length; i++) {
				final Long ruoloSingoloDir = Long.valueOf(ruoliSingoliDirigente[i]);
				if (ruoloSingoloDir != null && ruoloSingoloDir.equals(idRuolo)) {
					isRuoloDirigente = true;
					break;
				}
			}
		}
		return isRuoloDirigente;
	}

	/**
	 * Restituisce il flag associato alla stampa etichette.
	 * 
	 * @return flag associato alla stampa etichette
	 */
	public boolean getStampaEtichetteResponse() {
		return stampaEtichetteResponse;
	}

	/**
	 * Imposta il flag associato alal stampa etichette.
	 * 
	 * @param stampaEtichetteResponse
	 */
	public void setStampaEtichetteResponse(final boolean stampaEtichetteResponse) {
		this.stampaEtichetteResponse = stampaEtichetteResponse;
	}

	/**
	 * Restituisce il codice fiscale dell'utente.
	 * 
	 * @return codice fiscale utente
	 */
	public String getCodFiscale() {
		return codFiscale;
	}

	/**
	 * Imposta il codice fiscale dell'utente.
	 * 
	 * @param codFiscale
	 */
	public void setCodFiscale(final String codFiscale) {
		this.codFiscale = codFiscale;
	}

	/**
	 * Restituisce il flag associato alla visibilita della dialog AOO.
	 * 
	 * @return flag associato alla visibilita della dialog AOO
	 */
	public boolean getShowDialogAoo() {
		return showDialogAoo;
	}

	/**
	 * Imposta il flag associato alla visibilita della dialog AOO.
	 * 
	 * @param showDialogAoo
	 */
	public void setShowDialogAoo(final boolean showDialogAoo) {
		this.showDialogAoo = showDialogAoo;
	}

	/**
	 * Restituisce il flag associato al timbro uscita AOO.
	 * 
	 * @return flag associato al timbro uscita AOO
	 */
	public boolean getTimbroUscitaAoo() {
		return timbroUscitaAoo;
	}

	/**
	 * Imposta il flag associato al timbro uscita AOO.
	 * 
	 * @param timbroUscitaAoo
	 */
	public void setTimbroUscitaAoo(final boolean timbroUscitaAoo) {
		this.timbroUscitaAoo = timbroUscitaAoo;
	}

	/**
	 * Restituisce il flag associato all'estensione della visibilita.
	 * 
	 * @return flag associato all'estensione della visibilita
	 */
	public boolean getIsEstendiVisibilita() {
		return isEstendiVisibilita;
	}

	/**
	 * Imposta il flag associato all'estensione della visibilita.
	 * 
	 * @param isEstendiVisibilita
	 */
	public void setIsEstendiVisibilita(final boolean isEstendiVisibilita) {
		this.isEstendiVisibilita = isEstendiVisibilita;
	}

	/**
	 * Restituisce il flag associato a: disableUseHostOnly.
	 * 
	 * @return disableUseHostOnly
	 */
	public boolean getDisableUseHostOnly() {
		return disableUseHostOnly;
	}

	/**
	 * Imposta il flag: disableUseHostOnly.
	 * 
	 * @param disableUseHostOnly
	 */
	public void setDisableUseHostOnly(final boolean disableUseHostOnly) {
		this.disableUseHostOnly = disableUseHostOnly;
	}

	/**
	 * Restituisce il flag associato alla visibilita del riferimento storico.
	 * 
	 * @return flag associato alla visibilita del riferimento storico
	 */
	public boolean getShowRiferimentoStorico() {
		return showRiferimentoStorico;
	}

	/**
	 * Imposta il flag associato alla visibilita del riferimento storico.
	 * 
	 * @param showRiferimentoStorico
	 */
	public void setShowRiferimentoStorico(final boolean showRiferimentoStorico) {
		this.showRiferimentoStorico = showRiferimentoStorico;
	}

	/**
	 * Restituisce il flag associato alla visibilita della ricerca Nsd.
	 * 
	 * @return flag associato alla visibilita della ricerca Nsd
	 */
	public boolean getShowRicercaNsd() {
		return showRicercaNsd;
	}

	/**
	 * Imposta il flag associato alla visibilita della ricerca Nsd.
	 * 
	 * @param showRicercaNsd
	 */
	public void setShowRicercaNsd(final boolean showRicercaNsd) {
		this.showRicercaNsd = showRicercaNsd;
	}

	/**
	 * Restituisce il flag associato alla visbilita della selezione Tutti Visto.
	 * 
	 * @return flag associato alla visbilita della selezione Tutti Visto
	 */
	public boolean isShowSelezionaTuttiVisto() {
		return showSelezionaTuttiVisto;
	}

	/**
	 * Imposta il flag associato alla visbilita della selezione Tutti Visto.
	 * 
	 * @param showSelezionaTuttiVisto
	 */
	public void setShowSelezionaTuttiVisto(final boolean showSelezionaTuttiVisto) {
		this.showSelezionaTuttiVisto = showSelezionaTuttiVisto;
	}

	/**
	 * Restituisce il flag associato alla funzionalita protocolla e mantieni.
	 * 
	 * @return flag associato alla funzionalita protocolla e mantieni
	 */
	public boolean getProtocollaEMantieni() {
		return protocollaEMantieni;
	}

	/**
	 * Imposta il flag associato alla funzionalita protocolla e mantieni.
	 * 
	 * @param protocollaEMantieni
	 */
	public void setProtocollaEMantieni(final boolean protocollaEMantieni) {
		this.protocollaEMantieni = protocollaEMantieni;
	}

	/**
	 * Restituisce il flag associato al download custom excel.
	 * 
	 * @return flag associato al download custom excel
	 */
	public boolean getDownloadCustomExcel() {
		return downloadCustomExcel;
	}

	/**
	 * Imposta il flag associato al download custom excel.
	 * 
	 * @param downloadCustomExcel
	 */
	public void setDownloadCustomExcel(final boolean downloadCustomExcel) {
		this.downloadCustomExcel = downloadCustomExcel;
	}

	/**
	 * // * Restituisce il flag associato alla response Inoltra.
	 * 
	 * @return flag associato alla response Inoltra
	 */
	public Boolean getInoltraResponse() {
		return inoltraResponse;
	}

	/**
	 * Imposta il flag associato alla response Inoltra.
	 * 
	 * @param inoltraResponse
	 */
	public void setInoltraResponse(final Boolean inoltraResponse) {
		this.inoltraResponse = inoltraResponse;
	}

	/**
	 * Restituisce il flag associato alla response Rifiuta.
	 * 
	 * @return flag associato alla response Rifiuta
	 */
	public Boolean getRifiutaResponse() {
		return rifiutaResponse;
	}

	/**
	 * Imposta il flag associato alla response Rifiuta.
	 * 
	 * @param rifiutaResponse
	 */
	public void setRifiutaResponse(final Boolean rifiutaResponse) {
		this.rifiutaResponse = rifiutaResponse;
	}

	/**
	 * Restituisce true se il file non è sbustato, false altrimenti.
	 * 
	 * @return true se il file non è sbustato, false altrimenti
	 */
	public boolean getFileNonSbustato() {
		return fileNonSbustato;
	}

	/**
	 * Imposta il flag associato al file non sbustato.
	 * 
	 * @param fileNonSbustato
	 */
	public void setFileNonSbustato(final boolean fileNonSbustato) {
		this.fileNonSbustato = fileNonSbustato;
	}

	/**
	 * Restituisce true se l'AOO dell'utente è afferente ad uno degli Uffici
	 * Centrali di Bilancio, false altrimenti.
	 * 
	 * @return true se utente afferisce ad un UCB, false altrimenti
	 */
	public boolean isUcb() {
		return ucb;
	}

	/**
	 * Imposta il flag associato all'afferenza dell'utente ad un'AOO di tipo UCB
	 * (Ufficio centrale di bilancio).
	 * 
	 * @param ucb
	 */
	public void setUcb(final boolean ucb) {
		this.ucb = ucb;
	}

	/**
	 * Restituisce l'id dell ufficio padre.
	 * 
	 * @return id ufficio padre
	 */
	public Long getIdUfficioPadre() {
		return idUfficioPadre;
	}

	/**
	 * Imposta l'id dell'ufficio padre.
	 * 
	 * @param idUfficioPadre
	 */
	public void setIdUfficioPadre(final Long idUfficioPadre) {
		this.idUfficioPadre = idUfficioPadre;
	}

	/**
	 * Restituisce il flag associato alla visibilita della stampiglia allegati.
	 * 
	 * @return flag associato alla visibilita della stampiglia allegati
	 */
	public boolean getStampigliaAllegatiVisible() {
		return stampigliaAllegatiVisible;
	}

	/**
	 * Imposta il flag associato alla visibilita della stampiglia allegati.
	 * 
	 * @param stampigliaAllegatiVisible
	 */
	public void setStampigliaAllegatiVisible(final boolean stampigliaAllegatiVisible) {
		this.stampigliaAllegatiVisible = stampigliaAllegatiVisible;
	}

	/**
	 * Restituisce il flag associato all'univocita della mail.
	 * 
	 * @return flag associato all'univocita della mail
	 */
	public boolean getCheckUnivocitaMail() {
		return checkUnivocitaMail;
	}

	/**
	 * Imposta il flag associato all'univocita della mail.
	 * 
	 * @param checkUnivocitaMail
	 */
	public void setCheckUnivocitaMail(final boolean checkUnivocitaMail) {
		this.checkUnivocitaMail = checkUnivocitaMail;
	}

	/**
	 * Restituisce il numero massimo di notifiche rubrica.
	 * 
	 * @return numero massimo di notifiche rubrica
	 */
	public int getMaxNotificheRubrica() {
		return maxNotificheRubrica;
	}

	/**
	 * Imposta il numero massimo di notifiche rubrica.
	 * 
	 * @param maxNotificheRubrica
	 */
	public void setMaxNotificheRubrica(final int maxNotificheRubrica) {
		this.maxNotificheRubrica = maxNotificheRubrica;
	}

	/**
	 * Restituisce il numero masssimo notifiche calendario.
	 * 
	 * @return numero massimo notifiche calendario
	 */
	public int getMaxNotificheCalendario() {
		return maxNotificheCalendario;
	}

	/**
	 * Imposta il il numero masssimo notifiche calendario.
	 * 
	 * @param maxNotificheCalendario
	 */
	public void setMaxNotificheCalendario(final int maxNotificheCalendario) {
		this.maxNotificheCalendario = maxNotificheCalendario;
	}

	/**
	 * Restituisce il numero massimo di notifiche sottoscrizioni.
	 * 
	 * @return numero massimo di notifiche sottoscrizioni
	 */
	public int getMaxNotificheSottoscrizioni() {
		return maxNotificheSottoscrizioni;
	}

	/**
	 * Imposta il numero massimo di notifiche sottoscrizioni.
	 * 
	 * @param maxNotificheSottoscrizioni
	 */
	public void setMaxNotificheSottoscrizioni(final int maxNotificheSottoscrizioni) {
		this.maxNotificheSottoscrizioni = maxNotificheSottoscrizioni;
	}

	/**
	 * Restituisce il numero massimo di notifiche non lette.
	 * 
	 * @return numero massimo di notifiche non lette
	 */
	public int getMaxNotificheNonLette() {
		return maxNotificheNonLette;
	}

	/**
	 * Imposta il numero massimo di notifiche non lette.
	 * 
	 * @param maxNotificheNonLette
	 */
	public void setMaxNotificheNonLette(final int maxNotificheNonLette) {
		this.maxNotificheNonLette = maxNotificheNonLette;
	}
	
	/** 
	 * @return showAllaccioNps
	 */
	public boolean isShowAllaccioNps() {
		return showAllaccioNps;
	}

	/**
	 * Flag che mostra la tabella degli allacci verso nps
	 * @param maxNotificheNonLette
	 */
	public void setShowAllaccioNps(boolean showAllaccioNps) {
		this.showAllaccioNps = showAllaccioNps;
	}
	 
	/** 
	 * @return downloadSistemiEsterni
	 */
	public boolean isDownloadSistemiEsterni() {
		return downloadSistemiEsterni;
	}
	
	/**
	 * Flag per il download dai sistemi esterni
	 * @param maxNotificheNonLette
	 */
	public void setDownloadSistemiEsterni(boolean downloadSistemiEsterni) {
		this.downloadSistemiEsterni = downloadSistemiEsterni;
	}
	
	/**
	 * Restituisce true se postilla attiva, false altrimenti.
	 * @return true se la postilla è attiva
	 */
	public boolean isPostillaAttiva() {
		return postillaAttiva;
	}

	/**
	 * Imposta il flag associato all'attivazione della postilla.
	 * @param postillaAttiva
	 */
	public void setPostillaAttiva(boolean postillaAttiva) {
		this.postillaAttiva = postillaAttiva;
	}

	/**
	 * Restituisce true se la postilla è presente solo sulla prima pagina, false altrimenti.
	 * @return true se la postilla è presente solo sulla prima pagina, false altrimenti
	 */
	public boolean isPostillaSoloPag1() {
		return postillaSoloPag1;
	}

	/**
	 * Imposta il flag associato alla prima pagina della postilla.
	 * @param postillaSoloPag1
	 */
	public void setPostillaSoloPag1(boolean postillaSoloPag1) {
		this.postillaSoloPag1 = postillaSoloPag1;
	}
	
	
}
