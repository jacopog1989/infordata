/**
 * 
 */
package it.ibm.red.business.dto;

import java.util.Date;
import java.util.Map;

/**
 * @author APerquoti
 *
 */
public class RaccoltaFadDTO extends AbstractDTO {

	/**
	 * Serializable.
	 */
	private static final long serialVersionUID = 4595066494016912062L;
	
	/**** METADATI DOCUMENTO ****/
	
	/**
	 * Classe documentale.
	 */
	private String classeDocumentale;

	/**
	 * Document title.
	 */
	private String documentTitle;

	/**
	 * Identificativo fascicolo raccolta provvisoria.
	 */
	private String idFascicoloRaccoltaProvvisoria;

	/**
	 * Anno documneto.
	 */
	private Integer annoDocumento;

	/**
	 * Oggetto.
	 */
	private String oggetto;

	/**
	 * Numero protocollo.
	 */
	private String numeroProtocollo;

	/**
	 * Anno protocollo.
	 */
	private String annoProtocollo;

	/**
	 * Data protocollo.
	 */
	private String dataProtocollo;

	/**
	 * Ragionerie FAD.
	 */
	private String ragioneriaFad;

	/**
	 * Amministrazione FAD.
	 */
	private String amministrazioneFad;

	/**
	 * Prefisso label fascicolo.
	 */
	private String prefixFascicoloLabel;

	//opzionali- utili solo se provieni dal 'processo manuale di creazione RaccoltaFAD'

	/**
	 * Amministrazione FAD label.
	 */
	private String amministrazioneFadLabel;

	/**
	 * Ragioneria FAD label.
	 */
	private String ragioneriaFadLabel;

	/**
	 * Data potocollo manuale.
	 */
	private Date dataProtocolloFromManuale;
	
	/**** METADATI WF ****/

	/**
	 * Wob number.
	 */
	private String wobNumber;

	/**
	 * Identificativo fascicolo.
	 */
	private String idFascicolo;
	
	/**** NEW METADATI WF ****/

	/**
	 * Metadati.
	 */
	private transient Map<String, Object> newMetadati;
	
	/**** DATI NODO DIRIGENTE ****/

	/**
	 * Identificativo nodo dirigente.
	 */
	private String idNodoDirigente;

	/**
	 * Identificativo utente dirigente.
	 */
	private String idUtenteDirigente;

	/**
	 * Descrizione nodo.
	 */
	private String descrizioneNodo;
	
	/**** PROTOCOLLO USCITA AMMINISTRAZIONE ****/

	/**
	 * Numero protocollo amministrazione uscita.
	 */
	private String numeroProtocolloAmmUscita;

	/**
	 * Data protocollazione amministrazione uscita.
	 */
	private Date dataProtocolloAmmUscita;

	/**
	 * Aoo protollo uscita amministrazione.
	 */
	private String aooProtocolloAmmUscita;
	
	/**** PROTOCOLLO INGRESSO UCB ****/

	/**
	 * Numero protocollo UCB ingresso.
	 */
	private String numeroProtocolloUcbIngresso;

	/**
	 * Data protocollo UCB ingresso.
	 */
	private Date dataProtocolloUcbIngresso;

	/**
	 * Aoo protocollpo UCB ingresso.
	 */
	private String aooProtocolloUcbIngresso;
	
	/**** PROTOCOLLO USCITA UCB ****/

	/**
	 * Numero protocollo UCB uscita.
	 */
	private String numeroProtocolloUcbUscita;

	/**
	 * Data protocollo UCB uscita.
	 */
	private Date dataProtocolloUcbUscita;

	/**
	 * Aoo protocollo UCB uscita.
	 */
	private String aooProtocolloUcbUscita;
	
	
	/**
	 * @return the idRaccoltaProvvisoria
	 */
	public final String getIdFascicoloRaccoltaProvvisoria() {
		return idFascicoloRaccoltaProvvisoria;
	}
	/**
	 * @param idRaccoltaProvvisoria the idRaccoltaProvvisoria to set
	 */
	public final void setIdFascicoloRaccoltaProvvisoria(final String idFascicoloRaccoltaProvvisoria) {
		this.idFascicoloRaccoltaProvvisoria = idFascicoloRaccoltaProvvisoria;
	}
	/**
	 * @return the annoDocumento
	 */
	public final Integer getAnnoDocumento() {
		return annoDocumento;
	}
	/**
	 * @param annoDocumento the annoDocumento to set
	 */
	public final void setAnnoDocumento(final Integer annoDocumento) {
		this.annoDocumento = annoDocumento;
	}
	/**
	 * @return the oggetto
	 */
	public final String getOggetto() {
		return oggetto;
	}
	/**
	 * @param oggetto the oggetto to set
	 */
	public final void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}
	/**
	 * @return the numeroProtocollo
	 */
	public final String getNumeroProtocollo() {
		return numeroProtocollo;
	}
	/**
	 * @param numeroProtocollo the numeroProtocollo to set
	 */
	public final void setNumeroProtocollo(final String numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}
	
	/**
	 * Restituisce l'anno del protocollo.
	 * @return anno protocollo
	 */
	public String getAnnoProtocollo() {
		return annoProtocollo;
	}
	
	/**
	 * Imposta l'anno del protocollo.
	 * @param annoProtocollo
	 */
	public void setAnnoProtocollo(final String annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}
	
	/**
	 * @return the dataProtocollo
	 */
	public final String getDataProtocollo() {
		return dataProtocollo;
	}
	/**
	 * @param dataProtocollo the dataProtocollo to set
	 */
	public final void setDataProtocollo(final String dataProtocollo) {
		this.dataProtocollo = dataProtocollo;
	}
	/**
	 * @return the ragioneriaFad
	 */
	public final String getRagioneriaFad() {
		return ragioneriaFad;
	}
	/**
	 * @param ragioneriaFad the ragioneriaFad to set
	 */
	public final void setRagioneriaFad(final String ragioneriaFad) {
		this.ragioneriaFad = ragioneriaFad;
	}
	/**
	 * @return the amministrazioneFad
	 */
	public final String getAmministrazioneFad() {
		return amministrazioneFad;
	}
	/**
	 * @param amministrazioneFad the amministrazioneFad to set
	 */
	public final void setAmministrazioneFad(final String amministrazioneFad) {
		this.amministrazioneFad = amministrazioneFad;
	}
	/**
	 * @return the prefixFascicoloLabel
	 */
	public final String getPrefixFascicoloLabel() {
		return prefixFascicoloLabel;
	}
	/**
	 * @param prefixFascicoloLabel the prefixFascicoloLabel to set
	 */
	public final void setPrefixFascicoloLabel(final String prefixFascicoloLabel) {
		this.prefixFascicoloLabel = prefixFascicoloLabel;
	}
	/**
	 * @return the amministrazioneFadLabel
	 */
	public final String getAmministrazioneFadLabel() {
		return amministrazioneFadLabel;
	}
	/**
	 * @param amministrazioneFadLabel the amministrazioneFadLabel to set
	 */
	public final void setAmministrazioneFadLabel(final String amministrazioneFadLabel) {
		this.amministrazioneFadLabel = amministrazioneFadLabel;
	}
	/**
	 * @return the ragioneriaFadLabel
	 */
	public final String getRagioneriaFadLabel() {
		return ragioneriaFadLabel;
	}
	/**
	 * @param ragioneriaFadLabel the ragioneriaFadLabel to set
	 */
	public final void setRagioneriaFadLabel(final String ragioneriaFadLabel) {
		this.ragioneriaFadLabel = ragioneriaFadLabel;
	}
	/**
	 * @return the idNodoDirigente
	 */
	public final String getIdNodoDirigente() {
		return idNodoDirigente;
	}
	/**
	 * @param idNodoDirigente the idNodoDirigente to set
	 */
	public final void setIdNodoDirigente(final String idNodoDirigente) {
		this.idNodoDirigente = idNodoDirigente;
	}
	/**
	 * @return the descrizioneNodo
	 */
	public final String getDescrizioneNodo() {
		return descrizioneNodo;
	}
	/**
	 * @param descrizioneNodo the descrizioneNodo to set
	 */
	public final void setDescrizioneNodo(final String descrizioneNodo) {
		this.descrizioneNodo = descrizioneNodo;
	}
	/**
	 * @return the idFascicolo
	 */
	public final String getIdFascicolo() {
		return idFascicolo;
	}
	/**
	 * @param idFascicolo the idFascicolo to set
	 */
	public final void setIdFascicolo(final String idFascicolo) {
		this.idFascicolo = idFascicolo;
	}
	/**
	 * @return the newMetadati
	 */
	public final Map<String, Object> getNewMetadati() {
		return newMetadati;
	}
	/**
	 * @param newMetadati the newMetadati to set
	 */
	public final void setNewMetadati(final Map<String, Object> newMetadati) {
		this.newMetadati = newMetadati;
	}
	/**
	 * @return the documentTitle
	 */
	public final String getDocumentTitle() {
		return documentTitle;
	}
	/**
	 * @param documentTitle the documentTitle to set
	 */
	public final void setDocumentTitle(final String documentTitle) {
		this.documentTitle = documentTitle;
	}
	/**
	 * @return the dataProtocolloFromManuale
	 */
	public final Date getDataProtocolloFromManuale() {
		return dataProtocolloFromManuale;
	}
	/**
	 * @param dataProtocolloFromManuale the dataProtocolloFromManuale to set
	 */
	public final void setDataProtocolloFromManuale(final Date dataProtocolloFromManuale) {
		this.dataProtocolloFromManuale = dataProtocolloFromManuale;
	}
	/**
	 * @return the classeDocumentale
	 */
	public final String getClasseDocumentale() {
		return classeDocumentale;
	}
	/**
	 * @param classeDocumentale the classeDocumentale to set
	 */
	public final void setClasseDocumentale(final String classeDocumentale) {
		this.classeDocumentale = classeDocumentale;
	}
	/**
	 * @return the wobNumber
	 */
	public final String getWobNumber() {
		return wobNumber;
	}
	/**
	 * @param wobNumber the wobNumber to set
	 */
	public final void setWobNumber(final String wobNumber) {
		this.wobNumber = wobNumber;
	}
	/**
	 * @return the idUtenteDirigente
	 */
	public final String getIdUtenteDirigente() {
		return idUtenteDirigente;
	}
	/**
	 * @param idUtenteDirigente the idUtenteDirigente to set
	 */
	public final void setIdUtenteDirigente(final String idUtenteDirigente) {
		this.idUtenteDirigente = idUtenteDirigente;
	}
	
	/**
	 * Restituisce il numero protocollo Amm uscita.
	 * @return numero protocollo
	 */
	public String getNumeroProtocolloAmmUscita() {
		return numeroProtocolloAmmUscita;
	}
	
	/**
	 * Imposta il numero protocollo Amm uscita.
	 * @param numeroProtocolloAmmUscita
	 */
	public void setNumeroProtocolloAmmUscita(final String numeroProtocolloAmmUscita) {
		this.numeroProtocolloAmmUscita = numeroProtocolloAmmUscita;
	}
	
	/**
	 * Restituisce la data di protocollazione Amm uscita.
	 * @return data protocollazione
	 */
	public Date getDataProtocolloAmmUscita() {
		return dataProtocolloAmmUscita;
	}
	
	/**
	 * Imposta la data di protocollazione Amm uscita.
	 * @param dataProtocolloAmmUscita
	 */
	public void setDataProtocolloAmmUscita(final Date dataProtocolloAmmUscita) {
		this.dataProtocolloAmmUscita = dataProtocolloAmmUscita;
	}
	
	/**
	 * Restituisce l'aoo protocollo Amm uscita.
	 * @return aoo protocollo Amm uscita
	 */
	public String getAooProtocolloAmmUscita() {
		return aooProtocolloAmmUscita;
	}
	
	/**
	 * Imposta l'aoo protocollo Amm uscita.
	 * @param aooProtocolloAmmUscita
	 */
	public void setAooProtocolloAmmUscita(final String aooProtocolloAmmUscita) {
		this.aooProtocolloAmmUscita = aooProtocolloAmmUscita;
	}
	
	/**
	 * Restituisce il numero protocollo UCB in ingresso.
	 * @return numero protocollo UCB in uscita
	 */
	public String getNumeroProtocolloUcbIngresso() {
		return numeroProtocolloUcbIngresso;
	}
	
	/**
	 * Imposta il numero protocollo UCB in ingresso.
	 * @param numeroProtocolloUcbIngresso
	 */
	public void setNumeroProtocolloUcbIngresso(final String numeroProtocolloUcbIngresso) {
		this.numeroProtocolloUcbIngresso = numeroProtocolloUcbIngresso;
	}
	
	/**
	 * Restituisce la data protocollo UCB in ingresso.
	 * @return data protocollo UCB in ingresso
	 */
	public Date getDataProtocolloUcbIngresso() {
		return dataProtocolloUcbIngresso;
	}
	
	/**
	 * Imposta la data protocollo UCB in ingresso.
	 * @param dataProtocolloUcbIngresso
	 */
	public void setDataProtocolloUcbIngresso(final Date dataProtocolloUcbIngresso) {
		this.dataProtocolloUcbIngresso = dataProtocolloUcbIngresso;
	}
	
	/**
	 * Restituisce l'aoo protocollo UCB in ingresso.
	 * @return aoo protocollo UCB in ingresso
	 */
	public String getAooProtocolloUcbIngresso() {
		return aooProtocolloUcbIngresso;
	}
	
	/**
	 * Imposta l'aoo protocollo UCB in ingresso.
	 * @param aooProtocolloUcbIngresso
	 */
	public void setAooProtocolloUcbIngresso(final String aooProtocolloUcbIngresso) {
		this.aooProtocolloUcbIngresso = aooProtocolloUcbIngresso;
	}
	
	/**
	 * Restituisce il numero protocollo UCB in ingresso.
	 * @return numero protocollo UCB in ingresso
	 */
	public String getNumeroProtocolloUcbUscita() {
		return numeroProtocolloUcbUscita;
	}
	
	/**
	 * Imposta il numero protocollo UCB in uscita.
	 * @param numeroProtocolloUcbUscita
	 */
	public void setNumeroProtocolloUcbUscita(final String numeroProtocolloUcbUscita) {
		this.numeroProtocolloUcbUscita = numeroProtocolloUcbUscita;
	}
	
	/**
	 * Restituisce la data di protocollo UCB in uscita.
	 * @return data protocollo UCB in uscita
	 */
	public Date getDataProtocolloUcbUscita() {
		return dataProtocolloUcbUscita;
	}
	
	/**
	 * Imposta la data protocollo UCB in uscita.
	 * @param dataProtocolloUcbUscita
	 */
	public void setDataProtocolloUcbUscita(final Date dataProtocolloUcbUscita) {
		this.dataProtocolloUcbUscita = dataProtocolloUcbUscita;
	}
	
	/**
	 * Restituisce l'AOO protocollo UCB in uscita.
	 * @return aoo protocollo UCB in uscita
	 */
	public String getAooProtocolloUcbUscita() {
		return aooProtocolloUcbUscita;
	}
	
	/**
	 * Imposta l'AOO protocollo UCB in uscita.
	 * @param aooProtocolloUcbUscita
	 */
	public void setAooProtocolloUcbUscita(final String aooProtocolloUcbUscita) {
		this.aooProtocolloUcbUscita = aooProtocolloUcbUscita;
	}

}
