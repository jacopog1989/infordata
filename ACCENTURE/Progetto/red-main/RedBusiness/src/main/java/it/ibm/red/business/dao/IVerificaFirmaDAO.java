package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.service.concrete.SignSRV.VerificaFirmaItem;

/**
 * Interfaccia DAO verifica firma.
 */
public interface IVerificaFirmaDAO extends Serializable {

	/**
	 * Restituisce la lista dei documenti la cui firma è ancora da verificare per l'AOO in input
	 * 
	 * @param conn : Connessione verso la base dati (DWH_RED).
	 * @param idAoo
	 * @return
	 */
	List<VerificaFirmaItem> getContentsToVerify(Connection conn, int idAoo, int maxResults);

	/**
	 * Log di errore per l'inserimento dopo creazione su filenet.
	 * @param idAoo
	 * @param stato
	 * @param docTitle
	 * @param eccezione
	 * @param conn
	 * @return true o false
	 */
	boolean insertLogError(Long idAoo, Integer stato, Integer docTitle, String eccezione,Integer versione, Connection conn);
 

	/**
	 * Ottiene gli items la cui firma è da verificare per l'aoo.
	 * @param connection
	 * @param idAoo
	 * @param maxResults
	 * @param verificaFirmaMinutiDelay
	 * @return lista degli items la cui firma è da verificare
	 */
	List<VerificaFirmaItem> getDocumentToVerify(Connection connection, int idAoo, int maxResults,Integer verificaFirmaMinutiDelay);

  
	/**
	 * Metodo per l'inserimento dopo la creazione su filenet.
	 * @param idAoo
	 * @param documentTitle
	 * @param stato
	 * @param objectStore
	 * @param classeDocumentale
	 * @param versDocumento
	 * @param conn
	 * @return true o false
	 */
	boolean insertDopoCreazioneSuFilenet(Long idAoo, String documentTitle, Integer stato, String objectStore,
			String classeDocumentale, Integer versDocumento, Connection conn);

 	/**
	 * Aggiorna lo stato del document title verificato.
	 * @param idDocRecuperati
	 * @param stato
	 * @param conn
	 * @return true o false
	 */
	boolean updateStatoDocumentTitleVerificato(List<Integer> idDocRecuperati, Integer stato, Connection conn);
	
	/**
	 * Cancellazione del record dalla tab verifica_firma prima dell'inserimento per il verifica firma
	 * @param iAoo
	 * @param documentTitle
	 * @param stato
	 * @param objectStore
	 * @param classeDocumentale
	 * @param versDocumento
	 * @param conn
	 * @return true o false
	 */
	boolean deleteSingoloElemento(Long idAoo,String documentTitle,String objectStore,
			String classeDocumentale,Integer versDocumento,Connection conn);
}
