/**
 * @author CPIERASC
 *
 *	In questo package avremo la factory delle trasformazioni dei documenti presenti nel content engine ed i metodi di
 *	utilità per trasformare un singolo documento o una collezione di documenti (DocumentSet).
 *
 */
package it.ibm.red.business.helper.filenet.ce.trasform;
