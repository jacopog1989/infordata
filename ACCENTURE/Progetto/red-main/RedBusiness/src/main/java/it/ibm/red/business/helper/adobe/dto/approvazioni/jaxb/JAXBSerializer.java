package it.ibm.red.business.helper.adobe.dto.approvazioni.jaxb;

import java.io.IOException;
import java.io.OutputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;

import it.ibm.red.business.exception.RedException;

/**
 * Serializzatore JAXB.
 */
public final class JAXBSerializer {
	
    /**
     * Factory.
     */
	private static DiskFileItemFactory diskFileItemFactory = new DiskFileItemFactory();
	
	/**
	 * Costruttore vuoto.
	 */
	private JAXBSerializer() {
		// Costruttore vuoto.
	}

	/**
	 * Marshal the content tree rooted at obj into an output stream.
	 * @param <T>
	 * @param obj
	 * @param s
	 * @throws JAXBException
	 */
	public static <T> void marshal(final T obj, final OutputStream s) throws JAXBException {
		JAXBContext ctx = JAXBContext.newInstance(obj.getClass());
		Marshaller m = ctx.createMarshaller();
		m.marshal(obj, s);
	}

	/**
	 * Marshal the content tree rooted at obj into an output stream and returns a file item.
	 * @param <T>
	 * @param obj
	 * @return file item
	 * @throws JAXBException
	 * @throws IOException
	 */
	public static <T> FileItem marshal(final T obj) {
		FileItem fi = diskFileItemFactory.createItem(obj.getClass().getSimpleName(), "text/xml", false, obj.getClass()
				.getSimpleName() + ".xml");
		try {
			JAXBSerializer.marshal(obj, fi.getOutputStream());
		} catch (JAXBException | IOException e) {
			throw new RedException(e);
		}
		return fi;
	}
}
