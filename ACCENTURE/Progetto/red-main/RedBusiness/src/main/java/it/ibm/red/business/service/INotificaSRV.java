package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.NotificaDTO;
import it.ibm.red.business.service.facade.INotificaFacadeSRV;

/**
 * Interfaccia del servizio di notifica.
 */
public interface INotificaSRV extends INotificaFacadeSRV {
	
	/**
	 * Ritorna le notifiche dell'utente non più vecchie di aPartireDaNgiorni giorni
	 *  
	 * @param idUtente
	 * @param idAoo
	 * @param fcUtente
	 * @param aPartireDaNgiorni se null, il servizio imposta di default 7300 giorni (20 anni)
	 * @param dwhCon Connessione al datasource FileNet
	 * @return
	 */
	List<NotificaDTO> recuperaNotifiche(Long idUtente, Long idAoo, FilenetCredentialsDTO fcUtente, Integer aPartireDaNgiorni, Connection dwhCon);

	/**
	 * Conta il numero delle notifiche di sottoscrizione.
	 * @param idUtente
	 * @param idAoo
	 * @param maxRowNum
	 * @param maxNotificheNonLette
	 * @return numero delle notifiche
	 */
	Integer countNotificheSottoscrizione(Long idUtente, Long idAoo, Integer maxRowNum, int maxNotificheNonLette);
}