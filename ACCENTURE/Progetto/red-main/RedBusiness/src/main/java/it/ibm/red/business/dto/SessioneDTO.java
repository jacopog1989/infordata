package it.ibm.red.business.dto;

/**
 * DTO che definisce la sessione.
 */
public class SessioneDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -6858630069388451219L;

	/**
	 * Ip server.
	 */
	private final String ip;
	
	/**
	 * Identificativo.
	 */
	private final String id;
	
	/**
	 * Utente.
	 */
	private final String user;

	/**
	 * Costruttore del DTO.
	 * @param ip
	 * @param id
	 * @param user
	 */
	public SessioneDTO(final String ip, final String id, final String user) {
		super();
		this.ip = ip;
		this.id = id;
		this.user = user;
	}

	/**
	 * Restituisce l'indirizzo ip.
	 * @return IP
	 */
	public String getIp() {
		return ip;
	}

	/**
	 * Restituisce l'id della sessione.
	 * @return id sessione
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * Restituisce l'utente della sessione.
	 * @return user
	 */
	public String getUser() {
		return user;
	}

}