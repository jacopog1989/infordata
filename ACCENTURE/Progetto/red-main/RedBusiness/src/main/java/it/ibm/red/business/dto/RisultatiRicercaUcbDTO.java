/**
 * 
 */
package it.ibm.red.business.dto;

import java.util.Collection;

/**
 * @author APerquoti
 *
 */
public class RisultatiRicercaUcbDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6965856843268944617L;

	/**
	 * Masters che rappresentano i risultati della ricerca.
	 */
	private final Collection<MasterDocumentRedDTO> masters;
	
	/**
	 * Booleano che determina se i risultati sono omogenei.
	 */
	private final boolean isRisultatiOmogenei;
	
	/**
	 * Lista di metadati configurati con il flagOut.
	 */
	private final Collection<MetadatoDTO> metadatiOut;
	
	/**
	 * Identificativo della ricerca.
	 */
	private final String ricercaKey;
	
	/**
	 * Costruttore.
	 * 
	 * @param inMasters
	 * @param inIsRisultatiOmogenei
	 * @param inMetadatiOut
	 */
	public RisultatiRicercaUcbDTO(final Collection<MasterDocumentRedDTO> inMasters, final boolean inIsRisultatiOmogenei, final Collection<MetadatoDTO> inMetadatiOut, final String inRicercaKey) {
		this.masters = inMasters;
		this.isRisultatiOmogenei = inIsRisultatiOmogenei;
		this.metadatiOut = inMetadatiOut;
		this.ricercaKey = inRicercaKey;
	}

	/**
	 * @return the masters
	 */
	public Collection<MasterDocumentRedDTO> getMasters() {
		return masters;
	}

	/**
	 * @return the isRisultatiOmogenei
	 */
	public boolean isRisultatiOmogenei() {
		return isRisultatiOmogenei;
	}

	/**
	 * @return the metadatiOut
	 */
	public Collection<MetadatoDTO> getMetadatiOut() {
		return metadatiOut;
	}

	/**
	 * @return the ricercaKey
	 */
	public String getRicercaKey() {
		return ricercaKey;
	}
	
	
}
