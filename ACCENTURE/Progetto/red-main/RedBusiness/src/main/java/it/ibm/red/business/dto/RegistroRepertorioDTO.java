/**
 * 
 */
package it.ibm.red.business.dto;

import java.io.Serializable;

/**
 * @author APerquoti
 *
 */
public class RegistroRepertorioDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8448465108954817819L;
	
	/**
	 * Messaggio.
	 */
	private static final String REGISTRO_UFFICIALE = "REGISTRO UFFICIALE";
	
	/**
	 * Aoo.
	 */
	private Long idAoo;
	
	/**
	 * Tipo procedimento.
	 */
	private Long idTipoProcedimento;

	/**
	 * Tipo documento.
	 */
	private Long idTipologiaDocumento;
	
	/**
	 * Descrizione registro repertorio.
	 */
	private final String descrizioneRegistroRepertorio;
	
	/**
	 * Flag registro repertorio.
	 */
	private Boolean isRegistroRepertorio;

	/**
	 * Label stampigliatura.
	 */
	private String labelStampigliatura;
	
	/**
	 * Costruttore del DTO.
	 */
	public RegistroRepertorioDTO() {
		descrizioneRegistroRepertorio = REGISTRO_UFFICIALE;
	}
	
	/**
	 * Costruttore del DTO.
	 * @param inIdAoo
	 * @param inDescrizioneRegistroRepertorio
	 */
	public RegistroRepertorioDTO(final Long inIdAoo, final String inDescrizioneRegistroRepertorio) {
		idAoo = inIdAoo;
		descrizioneRegistroRepertorio = inDescrizioneRegistroRepertorio;
	}
	
	/**
	 * Costruttore del DTO.
	 * @param inIsRegistroRepertorio
	 * @param inDescrizioneRegistroRepertorio
	 */
	public RegistroRepertorioDTO(final Boolean inIsRegistroRepertorio, final String inDescrizioneRegistroRepertorio) {
		isRegistroRepertorio = inIsRegistroRepertorio;
		descrizioneRegistroRepertorio = inDescrizioneRegistroRepertorio;
	}

	/**
	 * Costruttore del DTO.
	 * @param inIdAoo
	 * @param inIdTipoProcedimento
	 * @param inIdTipologiaDocumento
	 * @param inDescrizioneRegistroRepertorio
	 * @param inLabelStampigliatura
	 */
	public RegistroRepertorioDTO(final Long inIdAoo, final Long inIdTipoProcedimento, final Long inIdTipologiaDocumento, final String inDescrizioneRegistroRepertorio, final String inLabelStampigliatura) {
		this.idAoo = inIdAoo;
		this.idTipoProcedimento = inIdTipoProcedimento;
		this.idTipologiaDocumento = inIdTipologiaDocumento;
		this.descrizioneRegistroRepertorio = inDescrizioneRegistroRepertorio;
		this.labelStampigliatura = inLabelStampigliatura;
	}

	/**
	 * @return the idAoo
	 */
	public Long getIdAoo() {
		return idAoo;
	}

	/**
	 * @return the idTipoProcedimento
	 */
	public Long getIdTipoProcedimento() {
		return idTipoProcedimento;
	}

	/**
	 * @return the idTipologiaDocumento
	 */
	public Long getIdTipologiaDocumento() {
		return idTipologiaDocumento;
	}

	/**
	 * @return the descrizioneRegistroRepertorio
	 */
	public String getDescrizioneRegistroRepertorio() {
		return descrizioneRegistroRepertorio;
	}

	/**
	 * Restituisce true se registro repertorio, false altrimenti.
	 * @return true se registro repertorio, false altrimenti
	 */
	public Boolean getIsRegistroRepertorio() {
		return isRegistroRepertorio;
	}
	
	/**
	 * Imposta il flag: isRegistroRepertorio.
	 * @param isRegistroRepertorio
	 */
	public void setIsRegistroRepertorio(final Boolean isRegistroRepertorio) {
		this.isRegistroRepertorio = isRegistroRepertorio;
	}

	/**
	 * Restituisce la label di stampigliatura.
	 * @return label stampigliatura
	 */
	public String getLabelStampigliatura() {
		return labelStampigliatura;
	}
	
	/**
	 * Imposta la label di stampigliatura.
	 * @param labelStampigliatura
	 */
	public void setLabelStampigliatura(final String labelStampigliatura) {
		this.labelStampigliatura = labelStampigliatura;
	}
}
