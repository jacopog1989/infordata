package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IProvinciaFacadeSRV;

/**
 * Interfaccia del servizio di gestione province.
 */
public interface IProvinciaSRV extends IProvinciaFacadeSRV {

}
