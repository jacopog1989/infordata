package it.ibm.red.business.dto;

/**
 * @author SLac
 * 
 * Classe per creare delle option nelle select
 * utilizzata per i metadati dinamici 
 */
public class KeyValueDTO extends AbstractDTO {

	private static final long serialVersionUID = -8712299608936543794L;

    /**
     * Chiave.
     */
	private String key;
	
    /**
     * Valore.
     */
	private transient Object value;
	
	/**
	 * Costruttore del DTO.
	 */
	public KeyValueDTO() {
		
	}
	

	/**
	 * @param key
	 * @param value
	 */
	public KeyValueDTO(final String key, final Object value) {
		this.key = key;
		this.value = value;
	}

	/**
	 * Recupera la chiave.
	 * @return chiave
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Recupera il valore.
	 * @return valore
	 */
	public Object getValue() {
		return value;
	}
	
}
