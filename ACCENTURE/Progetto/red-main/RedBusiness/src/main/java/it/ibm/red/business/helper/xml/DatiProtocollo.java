package it.ibm.red.business.helper.xml;

import java.io.Serializable;

import it.ibm.red.business.service.concrete.FepaSRV.ProvenienzaType;

/**
 * Model Tipo Protocollo.
 */
public class DatiProtocollo implements Serializable {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 559111745702970385L;

	/**
	 * Aoo.
	 */
	private String aoo;

	/**
	 * Tipo protocollo.
	 */
	private String tipoProtocollo;

	/**
	 * Numero protocollo.
	 */
	private Integer numeroProtocollo;

	/**
	 * Data protocollo.
	 */
	private String dataProtocollo;

	/**
	 * Provenieneza.
	 */
	private ProvenienzaType provenienza;
	
	/**
	 * Costruttore vuoto.
	 */
	public DatiProtocollo() { }
	
	/**
	 * Costruttore di default.
	 * @param aoo
	 * @param tipoProtocollo
	 * @param numeroProtocollo
	 * @param dataProtocollo
	 * @param provenienza
	 */
	public DatiProtocollo(final String aoo, final String tipoProtocollo, final Integer numeroProtocollo, final String dataProtocollo, final ProvenienzaType provenienza) {
		super();
		this.aoo = aoo;
		this.tipoProtocollo = tipoProtocollo;
		this.numeroProtocollo = numeroProtocollo;
		this.dataProtocollo = dataProtocollo;
		this.provenienza = provenienza;
	}

	/**
	 * @return aoo
	 */
	public String getAoo() {
		return aoo;
	}

	/**
	 * Imposta l'aoo.
	 * @param aoo
	 */
	public void setAoo(final String aoo) {
		this.aoo = aoo;
	}

	/**
	 * @return tipoProtocollo
	 */
	public String getTipoProtocollo() {
		return tipoProtocollo;
	}

	/**
	 * Imposta il tipo di protocollo.
	 * @param tipoProtocollo
	 */
	public void setTipoProtocollo(final String tipoProtocollo) {
		this.tipoProtocollo = tipoProtocollo;
	}

	/**
	 * @return numeroProtocollo
	 */
	public Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * Imposta il numero di protocollo.
	 * @param numeroProtocollo
	 */
	public void setNumeroProtocollo(final Integer numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}

	/**
	 * @return dataProtocollo
	 */
	public String getDataProtocollo() {
		return dataProtocollo;
	}

	/**
	 * Imposta data protocollo.
	 * @param dataProtocollo
	 */
	public void setDataProtocollo(final String dataProtocollo) {
		this.dataProtocollo = dataProtocollo;
	}

	/**
	 * @return provenienza
	 */
	public ProvenienzaType getProvenienza() {
		return provenienza;
	}

	/**
	 * Imposta la provenienza.
	 * @param provenienza
	 */
	public void setProvenienza(final ProvenienzaType provenienza) {
		this.provenienza = provenienza;
	}
}