package it.ibm.red.business.service.concrete;

import java.sql.Connection;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.IPostaNpsDAO;
import it.ibm.red.business.dto.AssegnatarioInteropDTO;
import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.MessaggioPostaNpsDTO;
import it.ibm.red.business.dto.MessaggioPostaNpsFlussoDTO;
import it.ibm.red.business.dto.NotificaInteropPostaNpsDTO;
import it.ibm.red.business.dto.NotificaPECPostaNpsDTO;
import it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO;
import it.ibm.red.business.dto.SegnaturaMessaggioInteropDTO;
import it.ibm.red.business.enums.PostaEnum;
import it.ibm.red.business.enums.SalvaDocumentoErroreEnum;
import it.ibm.red.business.enums.StatoElabMessaggioPostaNpsEnum;
import it.ibm.red.business.enums.TipoContestoProceduraleEnum;
import it.ibm.red.business.enums.TipoMessaggioPostaNpsIngressoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.ICasellePostaliSRV;
import it.ibm.red.business.service.IPostaNpsSRV;
import it.ibm.red.business.service.ISalvaMessaggioPostaNpsSRV;
import it.ibm.red.business.service.pstrategy.AutoProtocolStrategyFactory;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraErroreProtocollazioneAutomaticaType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioFlussoType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioPostaType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioProtocollazioneAutomaticaType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioProtocollazioneFlussoType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraNotificaInteroperabilitaProtocolloType;
import it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraNotificaPECType;
import it.ibm.red.webservice.model.interfaccianps.npstypes.OrgCasellaEmailType;

/**
 * Service gestione posta NPS.
 */
@Service
@Component
public class PostaNpsSRV extends AbstractService implements IPostaNpsSRV {
	
	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = 9139106437661064833L;
	
	/**
	 * Logger.
	 */	
	private static final REDLogger LOGGER = REDLogger.getLogger(PostaNpsSRV.class.getName());
	
	/**
	 * DAO.
	 */	
	@Autowired
	private IPostaNpsDAO postaNpsDAO;
	
	/**
	 * DAO.
	 */	
	@Autowired
	private IAooDAO aooDAO;
	
	/**
	 * Service.
	 */	
	@Autowired
	private ICasellePostaliSRV casellePostaliSRV;
	
	/**
	 * @see it.ibm.red.business.service.facade.IInteropFacadeSRV#accodaMessaggio
	 *      (it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioPostaType).
	 */
	@Override
	public Long accodaMessaggioPosta(final String messageIdNps, final RichiestaElaboraMessaggioPostaType richiesta, final Long idTrack) {
		final MessaggioPostaNpsDTO messaggio = new MessaggioPostaNpsDTO(richiesta);
		
		return accodaRichiesta(messageIdNps, messaggio, idTrack);
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IInteropFacadeSRV#accodaNotificaPec
	 *      (it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraNotificaPECType).
	 */
	@Override
	public Long accodaNotificaPEC(final String messageIdNps, final RichiestaElaboraNotificaPECType richiesta, final Long idTrack) {
		final NotificaPECPostaNpsDTO messaggio = new NotificaPECPostaNpsDTO(richiesta);
		
		return accodaRichiesta(messageIdNps, messaggio, idTrack);
	}
	
	
	/**
	 * @see it.ibm.red.business.service.facade.IPostaNpsFacadeSRV#accodaNotificaInteroperabilita
	 *      (it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraNotificaInteroperabilitaProtocolloType).
	 */
	@Override
	public Long accodaNotificaInteroperabilita(final String messageIdNps, final RichiestaElaboraNotificaInteroperabilitaProtocolloType richiesta, final Long idTrack) {
		final NotificaInteropPostaNpsDTO messaggio = new NotificaInteropPostaNpsDTO(richiesta);
		
		return accodaRichiesta(messageIdNps, messaggio, idTrack);
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IPostaNpsFacadeSRV#accodaMessaggioProtocollazioneAutomatica(
	 *      java.lang.String,
	 *      it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioProtocollazioneAutomaticaType,
	 *      java.lang.Long).
	 */
	@Override
	public Long accodaMessaggioProtocollazioneAutomatica(final String messageIdNps, final RichiestaElaboraMessaggioProtocollazioneAutomaticaType richiesta, final Long idTrack) {
		final MessaggioPostaNpsDTO messaggio = new MessaggioPostaNpsDTO(richiesta.getIdMessaggio(), richiesta.getIdDocumento(), 
				TipoMessaggioPostaNpsIngressoEnum.PEO_PEC_PROT_AUTO, richiesta.getMessaggioRicevuto(), richiesta.getProtocollo().getProtocollo(), null);
		
		return accodaRichiesta(messageIdNps, messaggio, idTrack);
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IPostaNpsFacadeSRV#accodaNotificaErroreProtocollazioneAutomatica(
	 *      java.lang.String,
	 *      it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraErroreProtocollazioneAutomaticaType,
	 *      java.lang.Long).
	 */
	@Override
	public Long accodaNotificaErroreProtocollazioneAutomatica(final String messageIdNps, final RichiestaElaboraErroreProtocollazioneAutomaticaType richiesta, final Long idTrack) {
		final MessaggioPostaNpsDTO messaggio = new MessaggioPostaNpsDTO(richiesta.getIdMessaggio(), richiesta.getIdDocumento(), 
				TipoMessaggioPostaNpsIngressoEnum.NOTIFICA_ERRORE_PROT_AUTO, richiesta.getMessaggioRicevuto());
		
		return accodaRichiesta(messageIdNps, messaggio, idTrack);
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IPostaNpsFacadeSRV#accodaMessaggioProtocollazioneFlusso(
	 *      java.lang.String,
	 *      it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioProtocollazioneFlussoType,
	 *      java.lang.Long).
	 */
	@Override
	public Long accodaMessaggioProtocollazioneFlusso(final String messageIdNps, final RichiestaElaboraMessaggioProtocollazioneFlussoType richiesta, final Long idTrack) {
		MessaggioPostaNpsFlussoDTO messaggio = null;
		final TipoContestoProceduraleEnum tcp = TipoContestoProceduraleEnum.getEnumById(richiesta.getCodiceFlusso());
		
		// SICOGE
		if (TipoContestoProceduraleEnum.SICOGE.equals(tcp)) {
			messaggio = new MessaggioPostaNpsFlussoDTO(richiesta.getIdMessaggio(), richiesta.getProtocollo().getProtocollo(), 
					richiesta.getCodiceFlusso(), richiesta.getProtocollazioneCoda(), richiesta.getChiaveFascicolo(), // Identificatore processo = ID fascicolo FEPA
					richiesta.getChiaveFascicolo(), richiesta.getTipoFascicolo());
		// Flusso ORDINARIO, SILICE o AUT, CG2
		} else {
			messaggio = new MessaggioPostaNpsFlussoDTO(richiesta.getIdMessaggio(), richiesta.getIdDocumento(),
					richiesta.getMessaggioRicevuto(), richiesta.getProtocollo().getProtocollo(), richiesta.getProtocolloNotifica() != null ? richiesta.getProtocolloNotifica().getProtocollo() : null, richiesta.getCodiceFlusso(), 
					richiesta.getContestoProcedurale().get(0), richiesta.getIdentificatoreProcesso());
		}
		
		return accodaRichiesta(messageIdNps, messaggio, idTrack);
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IPostaNpsFacadeSRV#accodaMessaggioFlusso(
	 *      java.lang.String,
	 *      it.ibm.red.webservice.model.interfaccianps.npsmessages.RichiestaElaboraMessaggioFlussoType,
	 *      java.lang.Long).
	 */
	@Override
	public Long accodaMessaggioFlusso(final String messageIdNps, final RichiestaElaboraMessaggioFlussoType richiesta, final Long idTrack) {
		final MessaggioPostaNpsFlussoDTO messaggio = new MessaggioPostaNpsFlussoDTO(richiesta.getIdMessaggio(), richiesta.getIdDocumento(),
				richiesta.getMessaggioRicevuto(), richiesta.getProtocollo().getProtocollo(), richiesta.getProtocolloNotifica() != null ? richiesta.getProtocolloNotifica().getProtocollo() : null, richiesta.getCodiceFlusso(),
				richiesta.getContestoProcedurale().get(0), richiesta.getIdentificatoreProcesso());
		
		return accodaRichiesta(messageIdNps, messaggio, idTrack);
	}
	
	
	/**
	 * Effettua l'inserimento del messaggio NPS nella coda di elaborazione.
	 * 
	 * @param messageIdNps
	 * @param messaggio
	 * @param idTrack
	 * @return
	 */
	private <T extends MessaggioPostaNpsDTO> Long accodaRichiesta(final String messageIdNps, final T messaggio, final Long idTrack) {
		Connection con = null;
		Long idRichiesta = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			
			String codiceAooNps = null;
			if (messaggio.getCasellaDestinataria() != null) {
				codiceAooNps = messaggio.getCasellaDestinataria().getCodiceAoo();
			} else if (messaggio.getProtocollo() != null) {
				codiceAooNps = messaggio.getProtocollo().getCodiceAoo();
			}
			
			final Aoo aoo = aooDAO.getAooFromNPS(codiceAooNps, con);
			
			// Se l'AOO è abilitato alla gestione della posta in modalità esterna interoperabile (semi-automatica o automatica)
			// oppure se il messaggio NPS non è associato ad un'e-mail
			if (TipoMessaggioPostaNpsIngressoEnum.NO_MAIL.equals(messaggio.getTipoIngresso()) 
					|| PostaEnum.isInteroperabile(aoo.getPostaInteropEsterna())) {
				
				// Gestione dell'eventuale pre-assegnatario presente nella segnatura
				gestisciPreAssegnatarioInteroperabilita(messaggio.getSegnatura(), con);
				
				// Si costruisce la richiesta di elaborazione del messaggio di posta NPS
				final RichiestaElabMessaggioPostaNpsDTO<T> richiesta = 
						new RichiestaElabMessaggioPostaNpsDTO<>(messageIdNps, messaggio, StatoElabMessaggioPostaNpsEnum.DA_ELABORARE, aoo.getIdAoo(), idTrack);
				
				// Si inserisce la richiesta nella coda di elaborazione
				idRichiesta = postaNpsDAO.inserisciRichiestaElabMessaggioInCoda(richiesta, con);
				
			} else {
				throw new RedException("L'AOO non è abilitata alla gestione dei messaggi in modalità esterna interoperabile. Codice AOO: " + aoo.getCodiceAoo());
			}
			
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inserimento di una richiesta nella coda di elaborazione dei messaggi di posta NPS. ID messaggio: " 
					+ messaggio.getIdMessaggio(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
		
		return idRichiesta;
	}
	
	
	/**
	 * Nel caso sia specificato il pre-assegnatario nella segnatura (accordo di
	 * servizio interoperabile), verifica se questo sia presente all'interno di RED
	 * tramite una verifica nel DB. In caso negativo, sostituisce l'assegnatario con
	 * un ufficio/utente nullo (ID ufficio e ID utente uguali a 0).
	 * 
	 * @param segnatura
	 * @param con
	 * @return
	 */
	private void gestisciPreAssegnatarioInteroperabilita(final SegnaturaMessaggioInteropDTO segnatura, final Connection con) {
		if (segnatura != null && segnatura.getPreAssegnatario() != null) {
			final boolean isValido = postaNpsDAO.verificaPreAssegnatario(segnatura.getPreAssegnatario(), con);
			
			if (!isValido) {
				LOGGER.info("Il pre-assegnatario indicato nella segnatura di interoperabilità non risulta presente nella base dati");
				segnatura.setPreAssegnatario(new AssegnatarioInteropDTO(0L, 0L));
			}
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IInteropFacadeSRV#elaboraMessaggioPostaNpsInIngresso(it.ibm.red.business.dto.RichiestaElabMessaggioInteropDTO).
	 */
	@Override
	public <T extends MessaggioPostaNpsDTO> String elaboraMessaggioPostaNpsInIngresso(final RichiestaElabMessaggioPostaNpsDTO<T> richiestaElabMessaggio) {
		String guidMessaggio = null;
		Connection con = null;
		
		final Long idCoda = richiestaElabMessaggio.getIdCoda();
		final TipoMessaggioPostaNpsIngressoEnum tipoIngresso = richiestaElabMessaggio.getMessaggio().getTipoIngresso();
		
		// Memorizzazione del messaggio di posta su FileNet SOLO SE presente
		if (!TipoMessaggioPostaNpsIngressoEnum.NO_MAIL.equals(tipoIngresso)) {
			try {
				con = setupConnection(getDataSource().getConnection(), false);
				
				// ###### SALVATAGGIO DEL MESSAGGIO DI POSTA SU FILENET ######
				guidMessaggio = salvaMessaggioPosta(richiestaElabMessaggio, idCoda, con);
				
				if (StringUtils.isNotBlank(guidMessaggio)) {
					LOGGER.info("Il messaggio di posta in ingresso NPS è stato salvato correttamente su FileNet. ID coda: " + idCoda);
				}
			} catch (final Exception e) {
				LOGGER.error("Il messaggio di posta in ingresso NPS NON è stato salvato su FileNet. ID coda " + idCoda, e);
				gestisciErroreElaborazioneMessaggioPostaNps(idCoda, e, con);
			} finally {
				closeConnection(con);
			}
		}
		
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			
			if (TipoMessaggioPostaNpsIngressoEnum.NO_MAIL.equals(tipoIngresso) || StringUtils.isNotBlank(guidMessaggio)) {
			
				// ### Se richiesto, si procede con la protocollazione automatica
				if (tipoIngresso.isProtocollazioneAutomatica()) {
					// ###### PROTOCOLLAZIONE AUTOMATICA ######
					final EsitoSalvaDocumentoDTO esitoProtAuto = eseguiProtocollazioneAutomatica(richiestaElabMessaggio, guidMessaggio, idCoda);
					
					// Protocollazione automatica KO
					if (!esitoProtAuto.isEsitoOk()) {
						StringBuilder messaggioErrore = new StringBuilder("Errore in fase di protocollazione automatica: ");
						
						if (!CollectionUtils.isEmpty(esitoProtAuto.getErrori())) {
							// Se l'errore dell'esito è generico, si estra il messaggio di errore specifico dalle note
							if (SalvaDocumentoErroreEnum.ERRORE_GENERICO.equals(esitoProtAuto.getErrori().get(0))) {
								messaggioErrore.append(esitoProtAuto.getNote());
							} else {
								messaggioErrore = new StringBuilder("<errori>");
								for (final SalvaDocumentoErroreEnum err : esitoProtAuto.getErrori()) {  
									messaggioErrore.append("<errore>").append(err.getMessaggio()).append("</errore>"); 
								}  
								messaggioErrore.append("</errori>");
							}
						}
						
					    // Si aggiorna lo stato della richiesta in coda inidicando l'errore riscontrato in fase di protocollazione automatica
						gestisciErroreElaborazioneMessaggioPostaNps(idCoda, messaggioErrore.toString(), con);
					
					// Protocollazione automatica OK
					} else {
						String messaggioEsitoOk = "[OK] Protocollazione automatica eseguita con successo. Numero documento: " + esitoProtAuto.getNumeroDocumento()
								+ ", Document title: " + esitoProtAuto.getDocumentTitle()
								+ ", Workflow number: " + esitoProtAuto.getWobNumber();
						if (StringUtils.isNotBlank(guidMessaggio)) {
							messaggioEsitoOk += ", GUID del messaggio di posta su FileNet: " + guidMessaggio;
						}
						
						// Si aggiorna lo stato della richiesta in coda impostando l'avvenuta elaborazione
						postaNpsDAO.aggiornaStatoRichiestaElabMessaggioPostaNps(idCoda, StatoElabMessaggioPostaNpsEnum.ELABORATA.getId(), messaggioEsitoOk, con);
					}
					
				// ### Altrimenti, si aggiorna lo stato della richiesta in coda impostando l'avvenuta elaborazione
				} else {
					postaNpsDAO.aggiornaStatoRichiestaElabMessaggioPostaNps(idCoda, StatoElabMessaggioPostaNpsEnum.ELABORATA.getId(), 
							"[OK] Creazione del messaggio di posta su FileNet eseguita con successo. GUID messaggio: " + guidMessaggio, con);
				}
				
			}
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di elaborazione del messaggio di posta NPS in ingresso", e);
			gestisciErroreElaborazioneMessaggioPostaNps(idCoda, e, con);
		} finally {
			closeConnection(con);
		}
		
		return guidMessaggio;
	}


	/**
	 * @param richiestaElabMessaggio
	 * @param guidMessaggio
	 * @param idCoda
	 */
	private static <T extends MessaggioPostaNpsDTO> EsitoSalvaDocumentoDTO eseguiProtocollazioneAutomatica(final RichiestaElabMessaggioPostaNpsDTO<T> richiestaElabMessaggio,
			final String guidMessaggio, final Long idCoda) {
		LOGGER.info("eseguiProtocollazioneAutomatica -> MESSAGGIO: " + richiestaElabMessaggio.getMessaggio());
		// Si deduce dal messaggio il Tipo Contesto Procedurale per attuare la strategia di protocollazione automatica corrispondente
		String codiceFlusso = TipoContestoProceduraleEnum.BASIC.getId();
		if (richiestaElabMessaggio.getMessaggio() instanceof MessaggioPostaNpsFlussoDTO) {
			codiceFlusso = ((MessaggioPostaNpsFlussoDTO) richiestaElabMessaggio.getMessaggio()).getDatiFlusso().getCodiceFlusso();
		}
		
		LOGGER.info("eseguiProtocollazioneAutomatica -> Protocollazione automatica per il messaggio in ingresso NPS con ID coda: " + idCoda 
				+ ". Flusso: " + codiceFlusso + " -> START");
		final EsitoSalvaDocumentoDTO esitoProtocollazioneAuto = AutoProtocolStrategyFactory.protocol(codiceFlusso, richiestaElabMessaggio, guidMessaggio);
		LOGGER.info("Protocollazione automatica per il messaggio in ingresso NPS con ID coda: " + idCoda + ". Flusso: " + codiceFlusso 
				+ ". ESITO OK? " + esitoProtocollazioneAuto.isEsitoOk() + " -> END");
		
		return esitoProtocollazioneAuto;
	}
	
	/**
	 * @param idCoda
	 * @param eccezione
	 * @param con
	 */
	private void gestisciErroreElaborazioneMessaggioPostaNps(final Long idCoda, final Exception eccezione, final Connection con) {
		String messaggioEccezione = "Dettaglio del messaggio di errore NON presente";
		if (eccezione != null) {
			messaggioEccezione = eccezione.getMessage();
			
			if (StringUtils.isBlank(messaggioEccezione)) {
				messaggioEccezione = ExceptionUtils.getStackTrace(eccezione);
			}
		}
		
		gestisciErroreElaborazioneMessaggioPostaNps(idCoda, messaggioEccezione, con);
	}

	/**
	 * @param idCoda
	 * @param errore
	 * @param con
	 */
	private void gestisciErroreElaborazioneMessaggioPostaNps(final Long idCoda, final String errore, final Connection con) {
		if (idCoda != null) {
			LOGGER.error("Errore nell'elaborazione del messaggio di posta in ingresso NPS. ID coda: " + idCoda + ". Errore: " + errore);
			
			// Si aggiorna lo stato della richiesta in coda impostando l'errore
			postaNpsDAO.aggiornaStatoRichiestaElabMessaggioPostaNps(idCoda, StatoElabMessaggioPostaNpsEnum.IN_ERRORE.getId(), "[KO] Errore nell'elaborazione => " + errore, con);
		} else {
			LOGGER.error("Errore nel recupero di una richiesta dalla coda dei messaggi di posta NPS in ingresso (REDBATCH_CODA_POSTANPS_ING). Errore: " + errore);
		}
	}

	/**
	 * @param richiestaElabMessagio
	 * @param idCoda
	 * @param con
	 */
	@SuppressWarnings("unchecked")
	private static <T extends MessaggioPostaNpsDTO> String salvaMessaggioPosta(final RichiestaElabMessaggioPostaNpsDTO<T> richiestaElabMessagio, final Long idCoda, 
			final Connection con) {
		String salvaMessaggioPostaNpsServiceName;
		
		// Notifica PEC
		if (richiestaElabMessagio.getMessaggio() instanceof NotificaPECPostaNpsDTO) {
			salvaMessaggioPostaNpsServiceName = "salvaNotificaPECPostaNpsSRV";
			
		// Notifica di Interoperabilità
		} else if (richiestaElabMessagio.getMessaggio() instanceof NotificaInteropPostaNpsDTO) {
			salvaMessaggioPostaNpsServiceName = "salvaNotificaInteropPostaNpsSRV";
			
		// Messaggio PEO/PEC
		} else {
			switch (richiestaElabMessagio.getMessaggio().getTipoMessaggio()) {
			case PEC_POSTA_CERTIFICATA:
			case PEC_RICEVUTA:
			case PEC_ACCETTAZIONE:
			case PEC_NON_ACCETTAZIONE:
			case PEC_PRESA_IN_CARICO:
			case PEC_AVVENUTA_CONSEGNA:
			case PEC_ERRORE_CONSEGNA:
			case PEC_PREAVVISO_ERRORE_CONSEGNA:
			case PEC_ANOMALIA_MESSAGGIO:
			case PEC_RILEVAZIONE_VIRUS:
				salvaMessaggioPostaNpsServiceName = "salvaMessaggioPECPostaNpsSRV";
				break;
			case GENERICO_EMAIL:
				salvaMessaggioPostaNpsServiceName = "salvaMessaggioPEOPostaNpsSRV";
				break;
			case SEGNATURA_CONFERMA_RICEZIONE:
			case SEGNATURA_AGGIORNAMENTO_CONFERMA:
			case SEGNATURA_NOTIFICA_ECCEZIONE:
			case SEGNATURA_ANNULLAMENTO_PROTOCOLLAZIONE:
			default:
				throw new RedException("Tipologia di messaggio di posta NPS non gestita: " + richiestaElabMessagio.getMessaggio().getTipoMessaggio() 
						+ ". ID coda: " + idCoda);
			}
		}
		
		// ### Si procede alla memorizzazione del messaggio su FileNet
		final ISalvaMessaggioPostaNpsSRV<T> salvaMessaggioPostaNpsSRV = 
				(ISalvaMessaggioPostaNpsSRV<T>) ApplicationContextProvider.getApplicationContext().getBean(salvaMessaggioPostaNpsServiceName);
		
		return salvaMessaggioPostaNpsSRV.salvaMessaggio(richiestaElabMessagio, con);
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IInteropFacadeSRV#getNextRichiestaDiElaborazione(java.lang.Long).
	 */
	@Override
	public <T extends MessaggioPostaNpsDTO> RichiestaElabMessaggioPostaNpsDTO<T> recuperaRichiestaElabDaLavorare(final Long idAoo) {
		RichiestaElabMessaggioPostaNpsDTO<T> richiesta = null;
		Connection con = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), true);
			
			// Si recupera una richiesta di elaborazione da lavorare
			richiesta = postaNpsDAO.getAndLockRichiestaElabMessaggio(idAoo, con);
			
			// Si aggiorna lo stato della richiesta in coda impostando la presa in carico
			if (richiesta != null) {
				postaNpsDAO.aggiornaStatoRichiestaElabMessaggioPostaNps(richiesta.getIdCoda(), StatoElabMessaggioPostaNpsEnum.IN_ELABORAZIONE.getId(), con);
			}
			
			commitConnection(con);
		} catch (final Exception e) {
			rollbackConnection(con);
			LOGGER.error("Si è verificato un errore durante il recupero della prossima richiesta di elaborazione da lavorare. ID AOO: " + idAoo, e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
		
		return richiesta;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IPostaNpsFacadeSRV#isCasellaPostaleProtocollazioneAutomatica(it.ibm.red.webservice.model.interfaccianps.npstypes.OrgCasellaEmailType).
	 */
	@Override
	public boolean isCasellaPostaleProtocollazioneAutomatica(final OrgCasellaEmailType casellaPostaNPS) {
		
		IFilenetCEHelper fceh = null;
		Connection con = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			
			final String codiceAooNps = casellaPostaNPS.getAoo().getCodiceAOO();
			final Aoo aoo = aooDAO.getAooFromNPS(codiceAooNps, con);
			final AooFilenet aooFilenet = aoo.getAooFilenet();
			
			// Accesso come p8admin
			fceh = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getConnectionPoint(), aooFilenet.getObjectStore(), aooFilenet.getIdClientAoo()), aoo.getIdAoo());
			
			final CasellaPostaDTO casellaPostale = casellePostaliSRV.getCasellaPostale(fceh, casellaPostaNPS.getIndirizzoEmail().getEmail());
			return casellaPostale.isFlagProtocollazioneAutomatica();
			
			
		} catch (final RedException e) {
			LOGGER.error("Si è verificato un errore durante il recupero della casella mail", e);
			throw e;
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero della casella mail: " + e.getMessage(), e);
			throw new RedException("Errore durante il recupero della casella mail: " + e.getMessage(), e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}
		
	}
	
}