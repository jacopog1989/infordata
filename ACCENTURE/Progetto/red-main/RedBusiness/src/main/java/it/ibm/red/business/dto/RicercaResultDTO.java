package it.ibm.red.business.dto;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Wrapper per i risulati della ricerca.
 * 
 * La ricerca può ritornare dei documenti o dei fascicoli
 * Questo wrapper offre entrmabi i valori di ritorno insieme a dei metodi di utilità che verificano se sono stati ritornati dei valori
 *
 */
public class RicercaResultDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -4780068093394371074L;

	/**
	 * Lista fascicoli.
	 */
	private Collection<FascicoloDTO> fascicoli;
	
	/**
	 * Lista documenti.
	 */
	private Collection<MasterDocumentRedDTO> documenti;
	
	/**
	 * Soglia.
	 */
	private Integer soglia = 0;
	
	/**
	 * Costruttore di defualt.
	 */
	public RicercaResultDTO() {
		super();
	}
	
	/**
	 * Costruttore.
	 * @param fascicoli
	 * @param documenti
	 */
	public RicercaResultDTO(final Collection<FascicoloDTO> fascicoli, final Collection<MasterDocumentRedDTO> documenti) {
		this.fascicoli = fascicoli;
		this.documenti = documenti;
	}

	/**
	 * Null-safe getter dei fascicoli.
	 * @return fascicoli
	 */
	public Collection<FascicoloDTO> getFascicoli() {
		return fascicoli ==  null ? new ArrayList<>() : fascicoli;
	}

	/**
	 * Null-safe getter dei documenti.
	 * @return documenti
	 */
	public Collection<MasterDocumentRedDTO> getDocumenti() {
		return documenti == null ? new ArrayList<>() : documenti;
	}

	/**
	 * Restituisce la soglia.
	 * @return soglia
	 */
	public Integer getSoglia() {
		return soglia;
	}

	/**
	 * Imposta la soglia.
	 * @param soglia
	 */
	public void setSoglia(final Integer soglia) {
		this.soglia = soglia;
	}
	
	/**
	 * Restituisce true se una delle due liste supera la soglia, false altrimenti.
	 * @return true se una delle due liste supera la soglia, false altrimenti
	 */
	public boolean isOutOfSoglia() {
		return fascicoli.size() > soglia || documenti.size() > soglia;
	}
}
