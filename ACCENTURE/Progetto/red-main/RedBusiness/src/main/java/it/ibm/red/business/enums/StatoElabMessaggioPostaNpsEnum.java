package it.ibm.red.business.enums;

/**
 * @author m.crescentini
 *
 */
public enum StatoElabMessaggioPostaNpsEnum {

	/**
	 * Valore.
	 */
	DA_ELABORARE(1),
	
	/**
	 * Valore.
	 */
	IN_ELABORAZIONE(2),
	
	/**
	 * Valore.
	 */
	ELABORATA(3),
	
	/**
	 * Valore.
	 */
	IN_ERRORE(4);
	
	
	/**
	 * Identificagtivo.
	 */
	private Integer id;
	
	/**
	 * Costruttore.
	 * @param id
	 */
	StatoElabMessaggioPostaNpsEnum(final Integer id) {
		this.id = id;
	}

	/**
	 * Restituisce l'id dell'enum.
	 * @return id
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * Restituisce l'enum associata al codice in ingresso.
	 * @param codice
	 * @return enum associata al codice
	 */
	public static StatoElabMessaggioPostaNpsEnum get(final Integer codice) {
		StatoElabMessaggioPostaNpsEnum output = null;
		
		for (final StatoElabMessaggioPostaNpsEnum stato : StatoElabMessaggioPostaNpsEnum.values()) {
			if (stato.getId().equals(codice)) {
				output = stato;
				break;
			}
		}
		
		return output;
	}
}
