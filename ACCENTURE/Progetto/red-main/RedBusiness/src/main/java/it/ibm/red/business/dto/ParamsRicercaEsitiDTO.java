/**
 * 
 */
package it.ibm.red.business.dto;

import java.util.Date;

import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;

/**
 * Classe ParamsRicercaEsitiDTO.
 *
 * @author APerquoti.
 */
public class ParamsRicercaEsitiDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6991441636993895958L;

	/**
	 * Tipo esito 
	 */
	private SottoCategoriaDocumentoUscitaEnum tipoEsito;
	
	/**
	 * Numero protocollo da 
	 */
	private Integer numeroProtocolloDa;
	
	/**
	 * Numero protocollo a 
	 */
	private Integer numeroProtocolloA;
	
	/**
	 * Data creazione da 
	 */
	private Date dataCreazioneDa;

	/**
	 * Data creazione a 
	 */
	private Date dataCreazioneA;

	/** 
	 *
	 * @return the tipoEsito
	 */
	public SottoCategoriaDocumentoUscitaEnum getTipoEsito() {
		return tipoEsito;
	}

	/** 
	 *
	 * @param tipoEsito the tipoEsito to set
	 */
	public void setTipoEsito(final SottoCategoriaDocumentoUscitaEnum tipoEsito) {
		this.tipoEsito = tipoEsito;
	}

	/** 
	 *
	 * @return the numeroProtocolloDa
	 */
	public Integer getNumeroProtocolloDa() {
		return numeroProtocolloDa;
	}

	/** 
	 *
	 * @param numeroProtocolloDa the numeroProtocolloDa to set
	 */
	public void setNumeroProtocolloDa(final Integer numeroProtocolloDa) {
		this.numeroProtocolloDa = numeroProtocolloDa;
	}

	/** 
	 *
	 * @return the numeroProtocolloA
	 */
	public Integer getNumeroProtocolloA() {
		return numeroProtocolloA;
	}

	/** 
	 *
	 * @param numeroProtocolloA the numeroProtocolloA to set
	 */
	public void setNumeroProtocolloA(final Integer numeroProtocolloA) {
		this.numeroProtocolloA = numeroProtocolloA;
	}

	/** 
	 *
	 * @return the dataCreazioneDa
	 */
	public Date getDataCreazioneDa() {
		return dataCreazioneDa;
	}

	/** 
	 *
	 * @param dataCreazioneDa the dataCreazioneDa to set
	 */
	public void setDataCreazioneDa(final Date dataCreazioneDa) {
		this.dataCreazioneDa = dataCreazioneDa;
	}

	/** 
	 *
	 * @return the dataCreazioneA
	 */
	public Date getDataCreazioneA() {
		return dataCreazioneA;
	}

	/** 
	 *
	 * @param dataCreazioneA the dataCreazioneA to set
	 */
	public void setDataCreazioneA(final Date dataCreazioneA) {
		this.dataCreazioneA = dataCreazioneA;
	}

	
}
