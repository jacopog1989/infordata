package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IAssegnazioneAutomaticaCapSpesaDAO;
import it.ibm.red.business.dto.AssegnatarioDTO;
import it.ibm.red.business.dto.AssegnazioneAutomaticaCapSpesaDTO;
import it.ibm.red.business.dto.CapitoloSpesaDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * The Class AssegnazioneAutomaticaDAO.
 *
 * @author mcrescentini
 * 
 * 	DAO per la gestione delle assegnazioni automatiche dei capitoli di spesa.
 */
@Repository
public class AssegnazioneAutomaticaCapSpesaDAO extends AbstractDAO implements IAssegnazioneAutomaticaCapSpesaDAO {

	private static final String NON = " NON ";

	private static final long serialVersionUID = -6156937938043769522L;

	/**
	 * Label START.
	 */
	private static final String START_MSG = "] -> START";
	
	/**
	 * Label END.
	 */
	private static final String END_MSG = "] -> END";

	/**
	 * Prefisso messaggio AOO.
	 */
	private static final String AND_AOO_LITERAL = "] e l'AOO [";

	/**
	 * Messaggio inserimento assegnazioni automatiche per capitolo.
	 */
	private static final String INSERIMENTO_ASSE_AUTO_CAPITOLO_MSG = "Inserimento di un'assegnazione automatica per il capitolo [";

	/**
	 * Messaggio eliminazione massiva assegnazioni automatiche.
	 */
	private static final String ELIMINAZIONE_MASSIVA_ASSEGNAZIONI_AUTOMATICHE_MSG = "Eliminazione massiva di assegnazioni automatiche per l'AOO [";
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AssegnazioneAutomaticaCapSpesaDAO.class.getName());
	
	/**
	 * Query select.
	 */
	private static final String SELECT = String.join(" ", "SELECT cs.CODICE, cs.DESCRIZIONE AS DESCRIZIONE_CAP, cs.ANNO, cs.FLAGSENTENZA,",
			"csa.IDNODOASS, n.DESCRIZIONE AS DESCRIZIONE_NODO, IDUTENTEASS, NULL AS DESCRIZIONE_UTENTE",
			"FROM CAPITOLI_SPESA_ASSEGNAZIONI csa, CAPITOLO_SPESA cs, NODO n",
			"WHERE csa.CODICECAPITOLO = cs.CODICE AND csa.IDNODOASS = n.IDNODO");
	
	/**
	 * Query insert.
	 */
	private static final String INSERT = String.join(" ", "INSERT INTO CAPITOLI_SPESA_ASSEGNAZIONI",
			"(CODICECAPITOLO, IDAOO, IDNODOASS, IDUTENTEASS) VALUES (?, ?, ?, ?)");
	
	/**
	 * Query delete.
	 */
	private static final String DELETE = "DELETE FROM CAPITOLI_SPESA_ASSEGNAZIONI WHERE CODICECAPITOLO = ? AND IDAOO = ? AND IDNODOASS = ?";
	
	/**
	 * Query delete per aoo.
	 */
	private static final String DELETE_BY_IDAOO = "DELETE FROM CAPITOLI_SPESA_ASSEGNAZIONI WHERE IDAOO = ?";
	
	/**
	 * Delete massiva.
	 */
	private static final String DELETE_MASSIVA = String.join(" ", "DELETE (SELECT * FROM CAPITOLI_SPESA_ASSEGNAZIONI",
			"WHERE CODICECAPITOLO = ? AND IDAOO = ?)");
	
	
	/**
	 * @see it.ibm.red.business.dao.IAssegnazioneAutomaticaDAO#get(java.lang.String,
	 *      java.lang.Long, java.lang.Long, java.sql.Connection).
	 * @param codiceCapitoloSpesa
	 *            il codice identificativo del capitolo spesa
	 * @param idAoo
	 *            identificativo dell'AOO
	 * @param idUfficio
	 *            identificativo dell'ufficio
	 * @param con
	 *            connessione al database
	 * @return assegnazione automatica per capitolo spesa se esistente, null
	 *         altrimenti
	 */
	@Override
	public final AssegnazioneAutomaticaCapSpesaDTO get(final String codiceCapitoloSpesa, final Long idAoo, final Long idUfficio, 
			final Connection con) {
		LOGGER.info("Recupero dell'assegnazione automatica per il capitolo [" + codiceCapitoloSpesa + "], l'ufficio [" + idUfficio 
				+ AND_AOO_LITERAL + idAoo + START_MSG);
		AssegnazioneAutomaticaCapSpesaDTO assegnazioneAutomatica = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			final StringBuilder sql = new StringBuilder(SELECT).append(" AND csa.CODICECAPITOLO = ? AND csa.IDAOO = ?");
			if (idUfficio != null && idUfficio != 0) {
				sql.append(" AND csa.IDNODOASS = ?");
			}
			
			ps = con.prepareStatement(sql.toString());
			ps.setString(1, codiceCapitoloSpesa);
			ps.setLong(2, idAoo);
			if (idUfficio != null && idUfficio != 0) {
				ps.setLong(3, idUfficio);
			}
			
			// Esecuzione della SELECT
			rs = ps.executeQuery();
			
			if (rs.next()) {
				
				assegnazioneAutomatica = populate(rs);
			}
		} catch (final Exception e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		LOGGER.info("Recupero dell'assegnazione automatica per il capitolo [" + codiceCapitoloSpesa + "], l'ufficio [" + idUfficio 
				+ AND_AOO_LITERAL + idAoo + END_MSG);
		return assegnazioneAutomatica;
	}

	/**
	 * @see it.ibm.red.business.dao.IAssegnazioneAutomaticaDAO#getByIdAoo(java.lang.Long,
	 *      java.sql.Connection).
	 * @param idAoo
	 *            identificativo dell'AOO
	 * @param con
	 *            connession al database
	 * @return lista delle assegnazioni automatiche recuperate
	 */
	@Override
	public final List<AssegnazioneAutomaticaCapSpesaDTO> getByIdAoo(final Long idAoo, final Connection con) {
		LOGGER.info("Recupero delle assegnazioni automatiche per l'AOO [" + idAoo + START_MSG);
		final List<AssegnazioneAutomaticaCapSpesaDTO> assegnazioniAutomatichePerAoo = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = con.prepareStatement("SELECT cs.CODICE, cs.DESCRIZIONE AS DESCRIZIONE_CAP, cs.ANNO, cs.FLAGSENTENZA,"
					+ " csa.IDNODOASS, n.DESCRIZIONE AS DESCRIZIONE_NODO, csa.IDUTENTEASS,"
					+ " NULLIF((u.COGNOME || ' ' || u.NOME), ' ') AS DESCRIZIONE_UTENTE"
					+ " FROM CAPITOLI_SPESA_ASSEGNAZIONI csa, CAPITOLO_SPESA cs, NODO n, UTENTE u"
					+ " WHERE csa.CODICECAPITOLO = cs.CODICE AND csa.IDNODOASS = n.IDNODO AND csa.IDUTENTEASS = u.IDUTENTE(+)"
					+ " AND csa.IDAOO = ?");
			ps.setLong(1, idAoo);
			
			// Esecuzione della SELECT
			rs = ps.executeQuery();
			
			AssegnazioneAutomaticaCapSpesaDTO assegnazioneAutomatica = null;
			while (rs.next()) {

				assegnazioneAutomatica = populate(rs);
				assegnazioniAutomatichePerAoo.add(assegnazioneAutomatica);
			}
		} catch (final Exception e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		LOGGER.info("Recupero delle assegnazioni automatiche per l'AOO [" + idAoo + END_MSG);
		return assegnazioniAutomatichePerAoo;
	}

	
	/**
	 * @see it.ibm.red.business.dao.IAssegnazioneAutomaticaDAO#elimina(
	 *      java.lang.String, java.lang.Long, java.lang.Long, java.sql.Connection).
	 * @param codiceCapitoloSpesa
	 *            codice del capitolo spesa
	 * @param idAoo
	 *            identificativo dell'AOO
	 * @param idUfficio
	 *            identificativo dell'ufficio
	 * @param con
	 *            connessione al database
	 * @return esito dell'eliminazione
	 */
	@Override
	public final int elimina(final String codiceCapitoloSpesa, final Long idAoo, final Long idUfficio, final Connection con) {
		LOGGER.info("Eliminazione di un'assegnazione automatica per il capitolo [" + codiceCapitoloSpesa 
				+ AND_AOO_LITERAL + idAoo + START_MSG);
		int result = 0;
		PreparedStatement ps = null;
		
		try {
			ps = con.prepareStatement(DELETE);
			ps.setString(1, codiceCapitoloSpesa);
			ps.setLong(2, idAoo);
			ps.setLong(3, idUfficio);
			
			// Esecuzione della DELETE
			result = ps.executeUpdate();
			
			LOGGER.info("Assegnazione automatica per il capitolo [" + codiceCapitoloSpesa + AND_AOO_LITERAL + idAoo + "]" 
					+ (result > 0 ? " eliminata" : " NON trovata/eliminata"));
		} catch (final Exception e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
		
		LOGGER.info("Eliminazione di un'assegnazione automatica per il capitolo [" + codiceCapitoloSpesa 
				+ AND_AOO_LITERAL + idAoo + END_MSG);
		return result;
	}
	
	/**
	 * @see it.ibm.red.business.dao.IAssegnazioneAutomaticaDAO#eliminaMultiple(java.util.List,
	 *      java.lang.Long, java.sql.Connection).
	 * @param assegnazioniAutomatiche
	 *            lista delle assegnazioni automatiche per capitoli di spesa
	 * @param idAoo
	 *            identificativo dell'AOO
	 * @param con
	 *            connession al database
	 */
	@Override
	public final void eliminaMultiple(final List<AssegnazioneAutomaticaCapSpesaDTO> assegnazioniAutomatiche, final Long idAoo, 
			final Connection con) {
		LOGGER.info(ELIMINAZIONE_MASSIVA_ASSEGNAZIONI_AUTOMATICHE_MSG + idAoo + START_MSG);
		PreparedStatement ps = null;
		
		try {
			ps = con.prepareStatement(DELETE_MASSIVA);
			
			for (final AssegnazioneAutomaticaCapSpesaDTO assegnazioneAutomatica : assegnazioniAutomatiche) {
				ps.setString(1, assegnazioneAutomatica.getCapitoloSpesa().getCodice());
				ps.setLong(2, idAoo);
				
				ps.addBatch();
			}
			
			// Esecuzione della DELETE MASSIVA
			final int[] result = ps.executeBatch();
			
			LOGGER.info(ELIMINAZIONE_MASSIVA_ASSEGNAZIONI_AUTOMATICHE_MSG + idAoo + "]" 
					+ (result.length > 0 ? " " : NON) + "effettuata");
		} catch (final Exception e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
		
		LOGGER.info(ELIMINAZIONE_MASSIVA_ASSEGNAZIONI_AUTOMATICHE_MSG + idAoo + END_MSG);
	}
	
	
	/**
	 * @see it.ibm.red.business.dao.IAssegnazioneAutomaticaDAO#eliminaTutteByIdAoo(java.lang.Long,
	 *      java.sql.Connection).
	 * @param idAoo
	 *            identificativo dell'AOO
	 * @param con
	 *            connession al database
	 * @return esito de
	 */
	@Override
	public final int eliminaTutteByIdAoo(final Long idAoo, final Connection con) {
		LOGGER.info("Eliminazione delle assegnazioni automatiche per l'AOO [" + idAoo + START_MSG);
		int result = 0;
		PreparedStatement ps = null;
		
		try {
			ps = con.prepareStatement(DELETE_BY_IDAOO);
			ps.setLong(1, idAoo);
			
			// Esecuzione della DELETE
			result = ps.executeUpdate();
			
			LOGGER.info("Assegnazioni automatiche per l'AOO [" + idAoo + "]" 
					+ (result > 0 ? " eliminate" : " NON trovate/eliminate"));
		} catch (final Exception e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
		
		LOGGER.info("Eliminazione delle assegnazioni automatiche per l'AOO [" + idAoo + END_MSG);
		return result;
	}

	
	/**
	 * @see it.ibm.red.business.dao.IAssegnazioneAutomaticaDAO#inserisci(
	 *      java.lang.String, java.lang.Long, java.lang.Long, java.lang.Long,
	 *      java.sql.Connection).
	 * @param codiceCapitoloSpesa
	 *            codice del capitolo spesa
	 * @param idAoo
	 *            identificativo dell'AOO
	 * @param idUfficio
	 *            identificativo dell'ufficio
	 * @param idUtente
	 *            identificativo dell'utente
	 * @param con
	 *            connession al database
	 */
	@Override
	public final void inserisci(final String codiceCapitoloSpesa, final Long idAoo, final Long idUfficio, final Long idUtente, 
			final Connection con) {
		LOGGER.info(INSERIMENTO_ASSE_AUTO_CAPITOLO_MSG + codiceCapitoloSpesa 
				+ AND_AOO_LITERAL + idAoo + START_MSG);
		PreparedStatement ps = null;
		
		try {
			ps = con.prepareStatement(INSERT);
			
			ps.setString(1, codiceCapitoloSpesa);
			ps.setLong(2, idAoo);
			ps.setLong(3, idUfficio);
			if (idUtente != null) {
				ps.setLong(4, idUtente);
			} else {
				ps.setNull(4, Types.INTEGER);
			}
			
			// Esecuzione della INSERT
			final int result = ps.executeUpdate();
			
			LOGGER.info(INSERIMENTO_ASSE_AUTO_CAPITOLO_MSG + codiceCapitoloSpesa + AND_AOO_LITERAL 
					+ idAoo + "]" + (result > 0 ? " " : NON) + "effettuato");
		} catch (final Exception e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
		
		LOGGER.info(INSERIMENTO_ASSE_AUTO_CAPITOLO_MSG + codiceCapitoloSpesa 
				+ AND_AOO_LITERAL + idAoo + START_MSG);
	}


	/**
	 * @see it.ibm.red.business.dao.IAssegnazioneAutomaticaDAO#inserisciMultiple(java.util.List,
	 *      java.lang.Long, java.sql.Connection).
	 * @param assegnazioniAutomatiche
	 *            lista assegnazioni automatiche
	 * @param idAoo
	 *            id dell'AOO
	 */
	@Override
	public final void inserisciMultiple(final List<AssegnazioneAutomaticaCapSpesaDTO> assegnazioniAutomatiche, final Long idAoo, 
			final Connection con) {
		final String codiceCapitoloSpesa = assegnazioniAutomatiche.get(0).getCapitoloSpesa().getCodice();
		LOGGER.info("Inserimento massivo di assegnazioni automatiche per il capitolo [" + codiceCapitoloSpesa
				+ AND_AOO_LITERAL + idAoo + START_MSG);
		PreparedStatement ps = null;
		String query = "";
		try {
			for (final AssegnazioneAutomaticaCapSpesaDTO assegnazioneAutomatica : assegnazioniAutomatiche) {
				if (assegnazioneAutomatica.getAssegnatario().getIdUtente() != null) {
					query = "INSERT INTO CAPITOLI_SPESA_ASSEGNAZIONI (CODICECAPITOLO, IDAOO, IDUTENTEASS, IDNODOASS) VALUES "
							+ "(?, ?, ?, (SELECT IDNODO FROM NODO WHERE LOWER(DESCRIZIONE) = ? AND IDAOO = ?))";
				} else {
					query = "INSERT INTO CAPITOLI_SPESA_ASSEGNAZIONI (CODICECAPITOLO, IDAOO, IDNODOASS) VALUES "
							+ "(?, ?, (SELECT IDNODO FROM NODO WHERE LOWER(DESCRIZIONE) = ? AND IDAOO = ?))";
				}
				int index = 1;
				
				ps = con.prepareStatement(query);
				ps.setString(index++, assegnazioneAutomatica.getCapitoloSpesa().getCodice());
				ps.setLong(index++, idAoo);
				
				if (assegnazioneAutomatica.getAssegnatario().getIdUtente() != null) {
					ps.setLong(index++, assegnazioneAutomatica.getAssegnatario().getIdUtente());
				}
				ps.setString(index++, assegnazioneAutomatica.getAssegnatario().getDescrizioneUfficio().trim().toLowerCase());
				ps.setLong(index++, idAoo);
				
				ps.execute();
				closeStatement(ps);
			}
			
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage(), e);
		} finally {
			closeStatement(ps);
		}
		
		LOGGER.info("Inserimento massivo di assegnazioni automatiche per il capitolo [" + codiceCapitoloSpesa 
				+ AND_AOO_LITERAL + idAoo + END_MSG);
	}
	
	
	/**
	 * @see it.ibm.red.business.dao.IAssegnazioneAutomaticaDAO#verificaAssegnazioneAutomatica(
	 *      java.lang.Long, java.lang.String, java.sql.Connection).
	 * @param idAoo
	 *            identificativo dell'AOO
	 * @param utente
	 *            utente in sessione
	 * @param descrizioneUfficio
	 *            descrizione dell'ufficio
	 * @param con
	 *            connession al database
	 * @return true se l'assegnazione automatica risulta valida, false altrimenti
	 */
	@Override
	public final boolean verificaAssegnazioneAutomatica(final Long idAoo, final UtenteDTO utente, final String descrizioneUfficio, final Connection con) {
		boolean isValida = false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long idUfficio = null;
		
		try {
			// Verifica validità ufficio
			ps = con.prepareStatement("SELECT IDNODO FROM NODO WHERE IDAOO = ? AND LOWER(DESCRIZIONE) = ?");
			ps.setLong(1, idAoo);
			ps.setString(2, descrizioneUfficio.trim().toLowerCase());
			rs = ps.executeQuery();
			
			if (rs.next()) {
				idUfficio = rs.getLong("IDNODO");
			}
			closeStatement(ps, rs);

			// Utente non selezionato e ufficio valido
			if (utente == null && idUfficio != null) {
				isValida = true;
			}
			
			// Utente selezionato afferente all'ufficio selezionato
			if (idUfficio != null && utente != null) {
				for (final UfficioDTO uff: utente.getUfficioRuoli().keySet()) {
					if (uff.getId().equals(idUfficio)) {
						isValida = true;
						break;
					}
				}
			}
			
			if (!isValida && utente != null) {
				LOGGER.info(
						"Assegnazione automatica {" + utente.getId() + "," + utente.getUsername() + "}{" + idUfficio + "," + descrizioneUfficio + "}" + NON + "VALIDA");
			}
			
		} catch (final Exception e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return isValida;
	}
	
	
	/**
	 * @param rs
	 * @return AssegnazioneAutomaticaCapSpesaDTO
	 * @throws SQLException
	 */
	private static AssegnazioneAutomaticaCapSpesaDTO populate(final ResultSet rs) throws SQLException {
		
		final CapitoloSpesaDTO capitoloSpesa = new CapitoloSpesaDTO(rs.getString("CODICE"), rs.getString("DESCRIZIONE_CAP"), rs.getInt("ANNO"), 
				BooleanFlagEnum.SI.getIntValue().equals(rs.getInt("FLAGSENTENZA")));
		
		Long idUtente = rs.getLong("IDUTENTEASS");
		if (rs.wasNull()) {
			idUtente = null;
		}
		
		final AssegnatarioDTO assegnatario = new AssegnatarioDTO(rs.getLong("IDNODOASS"), idUtente, rs.getString("DESCRIZIONE_NODO"), rs.getString("DESCRIZIONE_UTENTE"));
		return new AssegnazioneAutomaticaCapSpesaDTO(capitoloSpesa, assegnatario);
	}

}
