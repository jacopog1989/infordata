package it.ibm.red.business.service;

import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.persistence.model.TipoFile;

/**
 * Interfaccia del servizio che gestisce le tipologia di file.
 */
public interface ITipoFileSRV extends Serializable {
	
	/**
	 * Ottiene tutti i tipi di file.
	 * @return tipi di file
	 */
	Collection<TipoFile> getAll();

	/**
	 * Verifica che il file name abbia una estensione presente nelle estensioni censite dalla tipologica
	 * 
	 * @param fileName nome del file da verificare
	 * @param allTipoDati collezione di tipoFile in cui deve essere contenuta l'estensione
	 * @return
	 */
	boolean isSupportedFileName(String fileName, Collection<TipoFile> allTipoDati);
	
	/**
	 * Restituisce il tipo tra i tipoFile consentiti dato un determinato fileName.
	 * 
	 * @param fileName
	 * @param allTipoDati
	 * @return
	 */
	TipoFile getTipoFileFromFileName(String fileName, Collection<TipoFile> allTipoDati);

	/**
	 * Verifica che il file sia convertibile .
	 * 
	 * @param fileName
	 * @param allTipoDati
	 * @return
	 */
	boolean isConvertibile(String fileName, Collection<TipoFile> allTipoDati);
	
	/**
	 * Verifica la corrispondenza tra il mimeType e il magic number
	 * 
	 * @param content
	 * @param mimeType
	 * @param filename
	 * @return
	 */
	Boolean analizzaFile(byte[] content, String mimeType, String filename);

	/**
	 * Verifica la corrispondenza tra il mimeType e il magic number
	 * 
	 * @param content
	 * @param mimeType
	 * @param filename
	 * @return
	 */
	void verificaFile(byte[] content, String mimeType, String filename);

	/**
	 * Riscrive il file in maniera non critica per problemi di security
	 * 
	 * @param fileName 
	 * @return
	 */
	String normalizzaNomeFile(String filename);
}
