package it.ibm.red.business.enums;

/**
 * Enum configurazione firma.
 */
public enum ConfigurazioneFirmaAooEnum {
	
	/**
	 * Disabilitata.
	 */
	DISABILITATA(-1),

	/**
	 * Remota.
	 */
	LOCALE_REMOTA(0),
	
	/**
	 * Locale.
	 */
	SOLO_LOCALE(1),
	
	/**
	 * Remota.
	 */
	SOLO_REMOTA(2);
	
	
	/**
	 * Identificativo.
	 */
	private int id;

	/**
	 * Costruttore.
	 * @param id
	 */
	ConfigurazioneFirmaAooEnum(final int id) {
		this.id = id;
	}


	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Restituisce l'Enum associata all'Id.
	 * @param id
	 * @return Enum associata
	 */
	public static ConfigurazioneFirmaAooEnum getById(final int id) {
		ConfigurazioneFirmaAooEnum output = null;
		
		for (ConfigurazioneFirmaAooEnum configFirmaAoo : ConfigurazioneFirmaAooEnum.values()) {
			if (configFirmaAoo.getId() == id) {
				output = configFirmaAoo;
				break;
			}
		}
		
		return output;
	}
}