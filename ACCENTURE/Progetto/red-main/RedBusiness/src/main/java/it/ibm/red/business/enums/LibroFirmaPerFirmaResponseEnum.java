package it.ibm.red.business.enums;

/**
 * The Enum LibroFirmaPerFirmaResponseEnum.
 *
 * @author CPIERASC
 * 
 *         Enum per esplicitare l'associazione tra response ed operazioni
 *         possibili sul libro firma.
 */
public enum LibroFirmaPerFirmaResponseEnum {	

	/**
	 * Firma digitale.
	 */
	FIRMA_DIGITALE(ResponsesRedEnum.FIRMA_DIGITALE.getResponse(), ResponsesRedEnum.FIRMA_DIGITALE_MULTIPLA.getResponse()),

	/**
	 * Firma autografa.
	 */
	FIRMA_AUTOGRAFA(ResponsesRedEnum.FIRMA_AUTOGRAFA.getResponse(), ResponsesRedEnum.PREDISPOSIZIONE_FIRMA_AUTOGRAFA.getResponse()),

	/**
	 * Rifiuto.
	 */
	RIFIUTA(ResponsesRedEnum.RIFIUTA.getResponse(), "Rifiuta Assegnazione", ResponsesRedEnum.RIFIUTATA.getResponse()),

	/**
	 * Visto.
	 */
	VISTA(ResponsesRedEnum.VISTO.getResponse()),

	/**
	 * Sigla.
	 */
	SIGLA("Sigla", ResponsesRedEnum.SIGLA_E_INVIA.getResponse()), //"Sigla e richiedi visti"  richiede apertura organigramma attualmente non presente nel mobile.
	
	/**
	 * In lavorazione.
	 */
	IN_LAVORAZIONE(ResponsesRedEnum.SPOSTA_IN_LAVORAZIONE.getResponse()),
	
	/**
	 * Procedi da Corriere.
	 * Aggiugere le response alla lista nel caso in futuro ne servano altre
	 * 
	 */
	PROCEDI_DA_CORRIERE(ResponsesRedEnum.INVIA_ISPETTORE.getResponse(), ResponsesRedEnum.PROCEDI.getResponse());

	/**
	 * Response associate a quella specifica operazione.
	 */
	private String[] responses;

	/**
	 * Costruttore.
	 * 
	 * @param inResponses	lista delle response associate
	 */
	LibroFirmaPerFirmaResponseEnum(final String... inResponses) {
		this.responses = inResponses;
	}

	/**
	 * Getter response associate.
	 * 
	 * @return	response associate
	 */
	public String[] getResponses() {
		return responses;
	}

	/**
	 * Metodo per recuperare la prima response che appartiene all'intersezione tra l'insieme delle response fornite in input (ed 
	 * appartenenti ad uno specifico workflow) e l'insieme delle response associate all'enum in esame.
	 * 
	 * @param otherResponses	response del workflow
	 * @return					la prima response trovata nell'intersezione tra i due insiemi
	 */
	public String firstIn(final String... otherResponses) {
		for (String otherResponse : otherResponses) {
			for (String response : responses) {
				if (response.equalsIgnoreCase(otherResponse)) {
					return otherResponse;
				}
			}
		}
		return null;
	}
	
}
