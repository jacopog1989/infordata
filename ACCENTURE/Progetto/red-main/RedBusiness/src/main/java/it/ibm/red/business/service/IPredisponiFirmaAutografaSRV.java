/**
 * 
 */
package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IPredisponiFirmaAutografaFacadeSRV;

/**
 * @author m.crescentini
 *
 */
public interface IPredisponiFirmaAutografaSRV extends IPredisponiFirmaAutografaFacadeSRV {

}
