package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;

/**
 * 
 * @author mcrescentini
 *
 *	DAO riferimento.
 */
public interface IRiferimentoDAO extends Serializable {
	
	/**
	 * Salva sul db un riferimento del fascicolo corrente.
	 * @param idRisorsa risorsa
	 * @param idRiferimento riferimento
	 * @param tipoRiferimento tipologia
	 * @return un intero che indica l'esito dell'operazione.
	 */
	int insertRiferimentoFascicolo(int idRisorsa, int idRiferimento, int tipoRiferimento, Connection con);
}
