/**
 * 
 */
package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.RecuperoCodeDTO;
import it.ibm.red.business.dto.RicercaResultDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.RicercaGenericaTypeEnum;
import it.ibm.red.business.enums.RicercaPerBusinessEntityEnum;

/**
 * @author APerquoti
 *
 */
public interface IRicercaFacadeSRV extends Serializable {

	/**
	 * Metodo per la ricerca generica dei documenti.
	 * @param key
	 * @param anno
	 * @param type
	 * @param entity
	 * @param utente
	 * @return documenti master
	 */
	Collection<MasterDocumentRedDTO> ricercaGenerica(String key, Integer anno, RicercaGenericaTypeEnum type,
			RicercaPerBusinessEntityEnum entity, UtenteDTO utente);

	/**
	 * Metodo per recuperare code filenet e applicative da un documentTitle.
	 * 
	 * @param utente
	 * @param documentTitle
	 * @return
	 */

	Collection<FascicoloDTO> ricercaGenericaFascicoli(String key, Integer anno, RicercaGenericaTypeEnum type,
			RicercaPerBusinessEntityEnum entity, UtenteDTO utente);

	/**
	 * Metodo per la ricerca generica dei faldoni.
	 * @param key
	 * @param anno
	 * @param type
	 * @param entity
	 * @param utente
	 * @return faldoni
	 */
	Collection<FaldoneDTO> ricercaGenericaFaldoni(String key, Integer anno, RicercaGenericaTypeEnum type,
			RicercaPerBusinessEntityEnum entity, UtenteDTO utente);

	/**
	 * Restituisce tutte le modalità di ricerca gestite.
	 * 
	 * Nella creazione del documento questo metodo non viene utilizzato, viene
	 * invece generato direttamente a partire dall'enum
	 * 
	 * @return
	 */
	Collection<RicercaGenericaTypeEnum> retrieveRicercaGenericaModalita();
	
	/**
	 * Metodo per la ricerca rapida dei documenti.
	 * @param key
	 * @param anno
	 * @param type
	 * @param categoria
	 * @param utente
	 * @param onlyProtocollati
	 * @return documenti master
	 */
	Collection<MasterDocumentRedDTO> ricercaRapidaDocumenti(String key, Integer anno, RicercaGenericaTypeEnum type,
			CategoriaDocumentoEnum categoria, UtenteDTO utente, boolean onlyProtocollati);
	
	/**
	 * Metodo per la ricerca rapida dei documenti.
	 * @param key
	 * @param anno
	 * @param type
	 * @param categoria
	 * @param utente
	 * @return documenti master
	 */
	Collection<MasterDocumentRedDTO> ricercaRapidaDocumenti(String key, Integer anno, RicercaGenericaTypeEnum type,
			CategoriaDocumentoEnum categoria, UtenteDTO utente);

	/**
	 * Metodo per la ricerca rapida dei documenti.
	 * @param key
	 * @param anno
	 * @param type
	 * @param utente
	 * @return documenti master
	 */
	Collection<MasterDocumentRedDTO> ricercaRapidaDocumenti(String key, Integer anno, RicercaGenericaTypeEnum type,
			UtenteDTO utente);

	/**
	 * Recupera la coda dal document title.
	 * @param utente
	 * @param documentTitle
	 * @param classeDocumentale
	 * @param idCategoriaDocumento
	 * @param fullQuery
	 * @return coda recuperata
	 */
	RecuperoCodeDTO getQueueNameFromDocumentTitle(UtenteDTO utente, String documentTitle, String classeDocumentale,
			Integer idCategoriaDocumento, boolean fullQuery);

	/**
	 * Esegue una ricerca con le seguenti caratteristische.
	 * 
	 * @param key
	 *            Questo campo contiene testo libero che viene tokenizzato secondo
	 *            il type. Si comporta differentemente per quanto riguarda i
	 *            documenti e i fascicoli </br>
	 *            Per i docuemnti verifica i seguenti campi: </br>
	 *            Chiave di ricerca(match con Numero protocollo || anno protocollo
	 *            || anno creazione || Oggetto || Barcode || numero docuemnto) </br>
	 * 			</br>
	 *            Per i fascicoli verifica i seguenti campi: </br>
	 *            Chiave di ricerca(match con Numero || anno creazione || Oggetto ||
	 *            indice di classificazione
	 * 
	 *            </br>
	 * 			</br>
	 *            In entrambi i casi se il campo anno è valorizzato non viene
	 *            matchato come testo da inserire nella key
	 * 
	 * @param anno
	 *            Valorizzabile a null Quando l'anno viene matchato dal campo key
	 * 
	 * @param type
	 *            Tipo della ricerca. </br>
	 *            Controlla la tokenizzazione della key e la modalità in vui vengono
	 *            considerati i vari token
	 *            <ul>
	 *            <li>ESATTA: la key è presa per intero</li>
	 *            <li>QUALSAISI: la key è tokenizzata in parole (suddivise da spazi)
	 *            e i match sono applicate a ciascun token, è sufficiente che siano
	 *            valide per un token</li>
	 *            <li>TUTTE: la key è tokenizzata in parole (suddivise da spazi) e i
	 *            match sono applicate a ciascun token, le condizioni devono essere
	 *            valide per tutti i token</li>
	 *            </ul>
	 * 
	 * @param entity
	 *            Supporta le seguenti entità documentali:
	 *            <ul>
	 *            <li>documenti</li>
	 *            <li>fascicoli</li>
	 *            <li>entrambi</li>
	 *            </ul>
	 * 
	 *            <b>Attenzione non supporta i faldoni<b>
	 * 
	 * 
	 * @param searchIncontent
	 *            Attiva la ricerca nel content dei file oppure la ricerca per
	 *            metadati in maniera esclusiva. L'input viene preso dal campo key.
	 *            </br>
	 *            <b> I fascicoli non hanno content. </b> Per i fascicoli la
	 *            condizione non è considerata
	 * 
	 * @param utente
	 *            utente che ha richiesto l'operazione
	 * 
	 * @return
	 * 
	 * 		Un oggetto che contiene una lista di documenti, di fascicoli o di
	 *         entrambi
	 * 
	 * @throws java.lang.IllegalArgumentException
	 *             Quando viene inserita l'entità fascicoli Oppure quando non viene
	 *             inserita key o anno
	 * 
	 */

	RicercaResultDTO ricercaRapidaUtente(String key, Integer anno, RicercaGenericaTypeEnum type,
			RicercaPerBusinessEntityEnum entity, boolean searchInContent, UtenteDTO utente, boolean orderbyInQuery);
}
