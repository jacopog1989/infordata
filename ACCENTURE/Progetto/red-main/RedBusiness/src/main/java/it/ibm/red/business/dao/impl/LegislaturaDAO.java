package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import it.ibm.red.business.logger.REDLogger;
import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.ILegislaturaDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.persistence.model.Legislatura;

/**
 * Dao per la gestione delle legislature.
 *
 * @author mcrescentini
 */
@Repository
public class LegislaturaDAO extends AbstractDAO implements ILegislaturaDAO {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 825657946267375095L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(LegislaturaDAO.class.getName());
	
	/**
	 * @see it.ibm.red.business.dao.ILegislaturaDAO#getLegislaturaByNumero(int, java.sql.Connection).
	 */
	@Override
	public Legislatura getLegislaturaByNumero(final int numeroLegislatura, final Connection connection) {
		Legislatura legislatura = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = connection.prepareStatement("SELECT * FROM legislatura WHERE numero = ?");
			ps.setInt(1, numeroLegislatura);
			rs = ps.executeQuery();
			
			if (rs.next()) {
				legislatura = new Legislatura();
				populateVO(legislatura, rs);
			}
		} catch (SQLException e) {
			LOGGER.error("Errore nel recupero della legislatura dal numero. Numero legislatura: " + numeroLegislatura, e);
			throw new RedException("Errore nel recupero della legislatura dal numero. Numero legislatura: " + numeroLegislatura, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return legislatura;
	}
	
	
	private static void populateVO(final Legislatura legislatura, final ResultSet rs) throws SQLException {
		legislatura.setNumero(rs.getInt("numero"));
		legislatura.setDescrizione(rs.getString("descrizione"));
		legislatura.setDataInizio(rs.getDate("datainizio"));
		legislatura.setDataFine(rs.getDate("datafine"));		
	}

	/**
	 * @see it.ibm.red.business.dao.ILegislaturaDAO#getLegislaturaCorrente(java.sql.Connection).
	 */
	@Override
	public Legislatura getLegislaturaCorrente(final Connection connection) {
		Legislatura legislatura = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = connection.prepareStatement("select * from LEGISLATURA where DATAFINE is null");
			rs = ps.executeQuery();
			
			if (rs.next()) {
				legislatura = new Legislatura();
				populateVO(legislatura, rs);
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return legislatura;
	}

	/**
	 * @see it.ibm.red.business.dao.ILegislaturaDAO#getLegislature(java.sql.Connection).
	 */
	@Override
	public List<Legislatura> getLegislature(final Connection connection) {
		List<Legislatura> list = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = connection.prepareStatement("select * from LEGISLATURA order by datainizio");
			rs = ps.executeQuery();
			
			Legislatura legislatura = null;
			list = new ArrayList<>();
			while (rs.next()) {
				legislatura = new Legislatura();
				populateVO(legislatura, rs);
				list.add(legislatura);
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return list;
	}
}