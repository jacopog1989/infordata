package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.ProvinciaDTO;
import it.ibm.red.business.dto.SelectItemDTO;

/**
 * Interfaccia DAO gestione provincia.
 */
public interface IProvinciaDAO extends Serializable {
	
	/**
	 * Ottiene tutte le province.
	 * @param connection
	 * @return province
	 */
	Collection<ProvinciaDTO> getAll(Connection connection);
	
	/**
	 * Ottiene le province dalla descrizione.
	 * @param conn
	 * @param query
	 * @return lista di province
	 */
	List<ProvinciaDTO> getProvinceFromDesc(Connection conn, String query);

	/**
	 * Ottiene la provincia tramite il nome.
	 * @param nomeProvincia
	 * @param connection
	 * @return provincia
	 */
	ProvinciaDTO getProvinciaByNome(String nomeProvincia, Connection connection);

	/**
	 * Ottiene la provincia tramite l'id.
	 * @param idProvincia
	 * @param connection
	 * @return provincia
	 */
	ProvinciaDTO getProvincia(String idProvincia, Connection connection);

	/**
	 * Ottiene le province tramite l'id della regione.
	 * @param conn
	 * @param idRegione
	 * @return lista di province
	 */
	List<ProvinciaDTO> getProvince(Connection conn, Long idRegione);

	/**
	 * Ottiene le province.
	 * @param connection
	 * @return item selezionati
	 */
	Collection<SelectItemDTO> getProvince(Connection connection);

}
