package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataFaldDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * The Interface IRicercaAvanzataFaldFacadeSRV.
 *
 * @author m.crescentini
 * 
 *         Facade del servizio per la gestione della ricerca avanzata dei faldoni.
 */
public interface IRicercaAvanzataFaldFacadeSRV extends Serializable {
	
	/**
	 * Esegue la ricerca dei faldoni.
	 * @param paramsRicercaAvanzataFaldoni
	 * @param utente
	 * @param orderbyInQuery
	 * @return faldoni
	 */
	Collection<FaldoneDTO> eseguiRicercaFaldoni(ParamsRicercaAvanzataFaldDTO paramsRicercaAvanzataFaldoni,
			UtenteDTO utente, boolean orderbyInQuery);
}