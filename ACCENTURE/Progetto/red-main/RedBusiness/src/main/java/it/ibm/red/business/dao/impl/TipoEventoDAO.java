package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.ITipoEventoDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.TipoEvento;

/**
 * The Class TipoEventoDAO.
 *
 * @author CPIERASC
 * 
 *         Dao per la gestione dei tipi evento.
 */
@Repository
public class TipoEventoDAO extends AbstractDAO implements ITipoEventoDAO {

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TipoEventoDAO.class.getName());

	/**
	 * Gets the by id.
	 *
	 * @param id
	 *            the id
	 * @param connection
	 *            the connection
	 * @return the by id
	 */
	@Override
	public final TipoEvento getById(final int id, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			String querySQL = "SELECT te.* FROM TIPOEVENTO te WHERE te.IDTIPOEVENTO = ?";
			ps = connection.prepareStatement(querySQL);
			ps.setInt(index, id);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				return new TipoEvento(rs.getInt("IDTIPOEVENTO"), rs.getString("DESCRIZIONE"), rs.getLong("FLAGDEFAULT"),
						rs.getLong("TIPOLOGIARICHIESTA"));
			}
			return null;
		} catch (SQLException e) {
			LOGGER.error("Errore durante il recupero del tipo evento tramite id: " + id, e);
			throw new RedException("Errore durante il recupero del tipo evento tramite id: " + id, e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	@Override
	public final List<TipoEvento> getAllEntitaDocumento(final Long idAoo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<TipoEvento> list = null;

		try {
			String querySQL = " select idtipoevento, descrizione, FLAGDEFAULT, TIPOLOGIARICHIESTA "
					+ "  from tipoevento " 
					+ " where flagdefault = 1 "
					+ " ORDER BY descrizione ";
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();

			list = new ArrayList<>();
			while (rs.next()) {
				list.add(new TipoEvento(rs.getInt("IDTIPOEVENTO"), rs.getString("DESCRIZIONE"),
						rs.getLong("FLAGDEFAULT"), rs.getLong("TIPOLOGIARICHIESTA")));
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return list;
	}
}