package it.ibm.red.business.persistence.model;

import java.io.Serializable;
import java.util.List;

/**
 * Classe di gestione dettaglio report.
 */
public class DettaglioReport implements Serializable {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Identificativo dettaglio.
	 */
	private int idDettaglio;
	
	/**
	 * Codice.
	 */
	private String codice;
	
	/**
	 * Descrizione.
	 */
	private String descrizione;
	
	/**
	 * Lista tipi operazione.
	 */
	private List<TipoOperazioneReport> tipiOperazione;
	
	/**
	 * Lista code di lavoro.
	 */
	private List<CodaDiLavoroReport> codeDiLavoro;

	/**
	 * Restituisce l'id del dettaglio.
	 * @return id del dettaglio
	 */
	public int getIdDettaglio() {
		return idDettaglio;
	}

	/**
	 * Imposta l'id del dettaglio.
	 * @param idDettaglio
	 */
	public void setIdDettaglio(final int idDettaglio) {
		this.idDettaglio = idDettaglio;
	}

	/**
	 * Restituisce il codice.
	 * @return codice
	 */
	public String getCodice() {
		return codice;
	}

	/**
	 * Imposta il codice.
	 * @param codice
	 */
	public void setCodice(final String codice) {
		this.codice = codice;
	}

	/**
	 * Restituisce la descrizione del dettaglio report.
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Imposta la descrizione del dettaglio report.
	 * @param descrizione
	 */
	public void setDescrizione(final String descrizione) {
		this.descrizione = descrizione;
	}

	/**
	 * Restituisce la lista dei tipi operazione.
	 * @return tipi operazione
	 */
	public List<TipoOperazioneReport> getTipiOperazione() {
		return tipiOperazione;
	}

	/**
	 * Imposta la lista dei tipi operazione.
	 * @param tipiOperazione
	 */
	public void setTipiOperazione(final List<TipoOperazioneReport> tipiOperazione) {
		this.tipiOperazione = tipiOperazione;
	}

	/**
	 * Restituisce la lista delle code di lavoro Report.
	 * @return code di lavoro: CodaDiLavoroReport
	 */
	public List<CodaDiLavoroReport> getCodeDiLavoro() {
		return codeDiLavoro;
	}

	/**
	 * Imposta la lista delle code di lavoro Report.
	 * @param codeDiLavoro
	 */
	public void setCodeDiLavoro(final List<CodaDiLavoroReport> codeDiLavoro) {
		this.codeDiLavoro = codeDiLavoro;
	}
}