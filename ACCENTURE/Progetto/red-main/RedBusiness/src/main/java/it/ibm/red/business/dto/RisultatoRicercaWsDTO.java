package it.ibm.red.business.dto;

import java.util.List;

import org.apache.commons.collections4.CollectionUtils;

/**
 * Wrapper per i risultati della ricerca eseguita tramite web service.
 * 
 * La ricerca può ritornare dei documenti, dei fascicoli o dei faldoni.
 *
 */
public class RisultatoRicercaWsDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -1913256821021732987L;
	

    /**
     * Documenti.
     */
	private List<DocumentoWsDTO> documenti;
	
    /**
     * Fascicoli.
     */
	private List<FascicoloWsDTO> fascicoli;
	
    /**
     * Faldoni.
     */
	private List<FaldoneWsDTO> faldoni;
	
    /**
     * Numero elementi.
     */
	private int numeroElementi;
	
	/**
	 * Costruttore.
	 */
	public RisultatoRicercaWsDTO() {
		super();
	}
	
	/**
	 * @param documenti
	 * @param fascicoli
	 */
	public RisultatoRicercaWsDTO(final List<DocumentoWsDTO> documenti, final List<FascicoloWsDTO> fascicoli) {
		super();
		this.documenti = documenti;
		this.fascicoli = fascicoli;
		if (!CollectionUtils.isEmpty(documenti)) {
			numeroElementi = documenti.size();
		}
		if (!CollectionUtils.isEmpty(fascicoli)) {
			numeroElementi += fascicoli.size();
		}
	}

	/**
	 * @return the documenti
	 */
	public List<DocumentoWsDTO> getDocumenti() {
		return documenti;
	}

	/**
	 * @return the fascicoli
	 */
	public List<FascicoloWsDTO> getFascicoli() {
		return fascicoli;
	}

	/**
	 * @return the faldoni
	 */
	public List<FaldoneWsDTO> getFaldoni() {
		return faldoni;
	}
	
	/**
	 * @param documenti
	 */
	public void setDocumenti(final List<DocumentoWsDTO> documenti) {
		this.documenti = documenti;
		if (!CollectionUtils.isEmpty(documenti)) {
			numeroElementi = documenti.size();
		}
	}

	/**
	 * @param fascicoli
	 */
	public void setFascicoli(final List<FascicoloWsDTO> fascicoli) {
		this.fascicoli = fascicoli;
		if (!CollectionUtils.isEmpty(fascicoli)) {
			numeroElementi = fascicoli.size();
		}
	}

	/**
	 * @param faldoni
	 */
	public void setFaldoni(final List<FaldoneWsDTO> faldoni) {
		this.faldoni = faldoni;
		if (!CollectionUtils.isEmpty(faldoni)) {
			numeroElementi = faldoni.size();
		}
	}

	/**
	 * @return the numeroElementi
	 */
	public int getNumeroElementi() {
		return numeroElementi;
	}
	
}
