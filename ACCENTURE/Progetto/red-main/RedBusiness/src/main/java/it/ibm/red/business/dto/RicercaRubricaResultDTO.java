/**
 * 
 */
package it.ibm.red.business.dto;

import java.util.List;

import it.ibm.red.business.persistence.model.Contatto;

/**
 * @author APerquoti
 *
 */
public class RicercaRubricaResultDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -2369845963609849100L;

	/**
	 * Risultati della ricerca.
	 */
	private final List<Contatto> contatti;
	
	/**
	 * Messaggio di ritorno.
	 */
	private final String messaggio;
	
	/**
	 * Costruttore.
	 * @param inContatti
	 * @param inMessaggio
	 */
	public RicercaRubricaResultDTO(final List<Contatto> inContatti, final String inMessaggio) {
		contatti = inContatti;
		messaggio = inMessaggio;
	}

	/**
	 * @return the contatti
	 */
	public List<Contatto> getContatti() {
		return contatti;
	}

	/**
	 * @return the messaggio
	 */
	public String getMessaggio() {
		return messaggio;
	}
	
	
	
}
