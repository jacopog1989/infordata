package it.ibm.red.business.service.concrete;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.DocumentSet;

import it.ibm.red.business.constants.Constants.Ricerca;
import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataFaldDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IFaldoneSRV;
import it.ibm.red.business.service.IRicercaAvanzataFaldSRV;
import it.ibm.red.business.utils.DateUtils;

/**
 * The Class RicercaAvanzataFaldSRV.
 *
 * @author m.crescentini
 * 
 *         Servizio per la gestione della ricerca avanzata dei faldoni.
 */
@Service
@Component
public class RicercaAvanzataFaldSRV extends AbstractService implements IRicercaAvanzataFaldSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -7975005494736680746L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaAvanzataFaldSRV.class.getName());

	/**
	 * Service.
	 */
	@Autowired
	private IFaldoneSRV faldoneSRV;

	/**
	 * PRoperties.
	 */
	private PropertiesProvider pp;

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaAvanzataFaldFacadeSRV#eseguiRicercaFaldoni(it.ibm.red.business.dto.ParamsRicercaAvanzataFaldDTO,
	 *      it.ibm.red.business.dto.UtenteDTO, boolean).
	 */
	@Override
	public Collection<FaldoneDTO> eseguiRicercaFaldoni(final ParamsRicercaAvanzataFaldDTO paramsRicercaAvanzataFaldoni, final UtenteDTO utente, final boolean orderbyInQuery) {
		LOGGER.info("eseguiRicercaFaldoni -> START");
		Collection<FaldoneDTO> faldoniRicerca = new ArrayList<>();
		final HashMap<String, Object> mappaValoriRicercaFilenet = new HashMap<>();
		IFilenetCEHelper fceh = null;
		IFilenetCEHelper fcehAdmin = null;

		try {
			// Si convertono i parametri di ricerca relativi in una mappa chiave -> valore
			convertiInParametriRicercaFilenet(mappaValoriRicercaFilenet, paramsRicercaAvanzataFaldoni);

			// ### Si esegue la ricerca nel CE
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final DocumentSet faldoniRicercaFilenet = fceh.ricercaAvanzataFaldoni(mappaValoriRicercaFilenet, utente, false);

			if (faldoniRicercaFilenet != null && !faldoniRicercaFilenet.isEmpty()) {
				faldoniRicerca = TrasformCE.transform(faldoniRicercaFilenet, TrasformerCEEnum.FROM_DOCUMENT_TO_FALDONE);

				// Va creata la connection a Filenet con utenza ADMIN per recuperare la
				// gerarchia completa dei risultati di ricerca
				fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()), utente.getIdAoo());

				// Recupera il path completo di ogni singolo risultato di ricerca
				for (final FaldoneDTO figlio : faldoniRicerca) {
					figlio.setGerarchiaPath(faldoneSRV.getGerarchiaFaldone(figlio, utente, fcehAdmin));
				}

				if (!orderbyInQuery) {
					final List<FaldoneDTO> faldList = new ArrayList<>(faldoniRicerca);
					// implementa Comparable
					Collections.sort(faldList, Collections.reverseOrder());
					faldoniRicerca = faldList;
				}

			}
		} catch (final Exception e) {
			LOGGER.error("eseguiRicercaFaldoni -> Si è verificato un errore durante l'esecuzione della ricerca.", e);
			throw new RedException("eseguiRicercaFaldoni -> Si è verificato un errore durante l'esecuzione della ricerca.", e);
		} finally {
			popSubject(fceh);
			popSubject(fcehAdmin);
		}

		LOGGER.info("eseguiRicercaFaldoni -> END. Documenti trovati: " + faldoniRicerca.size());
		return faldoniRicerca;
	}

	private void convertiInParametriRicercaFilenet(final HashMap<String, Object> paramsRicercaFilenet, final ParamsRicercaAvanzataFaldDTO paramsRicerca) {
		LOGGER.info("convertiInParametriRicercaFilenet -> START");

		// Descrizione Faldone
		if (StringUtils.isNotBlank(paramsRicerca.getDescrizioneFaldone())) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY), paramsRicerca.getDescrizioneFaldone());
		}

		// Data Creazione Da
		if (paramsRicerca.getDataCreazioneDa() != null) {
			paramsRicercaFilenet.put(Ricerca.DATA_CREAZIONE_DA, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataCreazioneDa(), true));
		}

		// Data Creazione A
		if (paramsRicerca.getDataCreazioneA() != null) {
			paramsRicercaFilenet.put(Ricerca.DATA_CREAZIONE_A, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataCreazioneA(), false));
		}

		LOGGER.info("convertiInParametriRicercaFilenet -> END");
	}
}