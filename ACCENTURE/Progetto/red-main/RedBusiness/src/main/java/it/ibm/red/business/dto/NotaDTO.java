package it.ibm.red.business.dto;

import it.ibm.red.business.enums.ColoreNotaEnum;

/**
 * The Class NotaDTO.
 */
public class NotaDTO extends AbstractDTO {

	/**
	 * Serialization.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Il colore della nota.
	 */
	private ColoreNotaEnum colore;
	
	/**
	 * Il testo della nota.
	 */
	private String contentNota;
	
	/**
	 * Il testo della nota.
	 */
 
	/**
	 * Costruttore.
	 */
	public NotaDTO() {
		super();
		contentNota = "";
	}

	/**
	 * Getter colore.
	 * 
	 * @return	colore
	 */
	public ColoreNotaEnum getColore() {
		return colore;
	}

	/**
	 * tags:.
	 *
	 * @param i the colore to set
	 */
	public void setColore(final ColoreNotaEnum i) {
		this.colore = i;
	}

	/**
	 * Getter content della nota.
	 * 
	 * @return	content della nota
	 */
	public String getContentNota() {
		return contentNota;
	}

	/**
	 * tags:.
	 *
	 * @param contentNota the contentNota to set
	 */
	public void setContentNota(final String contentNota) {
		this.contentNota = contentNota;
	}
	 
}
