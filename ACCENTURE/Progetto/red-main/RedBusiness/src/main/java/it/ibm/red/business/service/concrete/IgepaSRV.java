package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import filenet.vw.api.VWQueueQuery;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.IContattoDAO;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dao.ITipologiaDocumentoDAO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.MetadatoDinamicoDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedParametriDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.FilenetStatoFascicoloEnum;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ProvenienzaSalvaDocumentoEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAllegatoSRV;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.IIgepaSRV;
import it.ibm.red.business.service.ISalvaDocumentoSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.utils.DateUtils;

/**
 * Service Igepa.
 */
@Service
@Component
public class IgepaSRV extends AbstractService implements IIgepaSRV {

	private static final long serialVersionUID = 654592484235968794L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(IgepaSRV.class.getName());
	
	/**
	 * Servizio.
	 */
	@Autowired
	private ISalvaDocumentoSRV salvaDocSrv;
	
	/**
	 * Servizio.
	 */
	@Autowired
	private IAllegatoSRV allegatoSRV;
	
	/**
	 * Servizio.
	 */
	@Autowired
	private IFascicoloSRV fascicoloSRV;
	
	/**
	 * Servizio.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;
	
	/**
	 * DAO.
	 */
	@Autowired
	private ITipoProcedimentoDAO tipoProcedimentoDAO;
	
	/**
	 * DAO.
	 */
	@Autowired
	private IContattoDAO contattoDAO;
	
	/**
	 * DAO.
	 */
	@Autowired
	private ITipologiaDocumentoDAO tipologiaDocumentoDAO;

	/**
	 * @see it.ibm.red.business.service.facade.IIgepaFacadeSRV#insertDocumento(java.lang.String,
	 *      java.lang.String, int, int, int, java.lang.String, byte[],
	 *      java.lang.String, byte[], java.lang.String, java.lang.String,
	 *      java.lang.String).
	 */
	@Override
	public String insertDocumento(final String oggetto, final String nomeFile, final int idNodoDestinatario, final int idUtenteDestinatario,
			final int numProtocollo, final String dataProtocollo, final byte[] content, final String contentType, final byte[] contentAll,
			final String contentTypeAll, final String nomeAll, final String aliasMittente) {
		String idDocRitornato;
		
		final SalvaDocumentoRedParametriDTO parametri = new SalvaDocumentoRedParametriDTO();
		parametri.setModalita(Constants.Modalita.MODALITA_INS);
		parametri.setIdUfficioMittenteWorkflow((long) idNodoDestinatario);
		parametri.setIdUtenteMittenteWorkflow((long) idUtenteDestinatario);
		
		final String utenteCedente = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IGEPA_UTENTE_CEDENTE);
		final UtenteDTO utente = utenteSRV.getById(Long.parseLong(utenteCedente));

		final DetailDocumentRedDTO dto = getDetailDocumentRedDTO(oggetto, nomeFile, idNodoDestinatario, "B.05",
				numProtocollo, dataProtocollo, content, contentType, contentAll,
				contentTypeAll, nomeAll, aliasMittente, utente.getIdAoo());

		idDocRitornato = salvaDocSrv.salvaDocumento(dto, parametri, utente, ProvenienzaSalvaDocumentoEnum.IGEPA).getDocumentTitle();
		
		return idDocRitornato;
	}

	/**
	 * Restituisce il dettaglio del documento RED.
	 * 
	 * @param oggetto
	 *            oggetto documento
	 * @param nomeFile
	 *            nome file documento
	 * @param idNodoDestinatario
	 *            id nodo destinatario
	 * @param indiceClassificazione
	 *            indice di classificazione
	 * @param numProtocollo
	 *            numero protocollo
	 * @param dataProtocollo
	 *            data protocollo
	 * @param content
	 *            content documento
	 * @param contentType
	 *            content type del content del documento
	 * @param contentAll
	 *            content dell'allegato del documento
	 * @param contentTypeAll
	 *            content type dell'allegato del documento
	 * @param nomeAll
	 *            nome allegato
	 * @param aliasMittente
	 *            alias dell'utente mittente
	 * @param idAoo
	 *            identificativo area organizzativa
	 * @return dettaglio recuperato
	 */
	private DetailDocumentRedDTO getDetailDocumentRedDTO(final String oggetto, final String nomeFile, final int idNodoDestinatario, final String indiceClassificazione, 
			final int numProtocollo, final String dataProtocollo, final byte[] content, final String contentType, final byte[] contentAll, final String contentTypeAll, final String nomeAll, final String aliasMittente, final long idAoo) { 
		//utenteCedente
		Connection connection = null;
		DetailDocumentRedDTO documento = null;
		PropertiesProvider pp = PropertiesProvider.getIstance();
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			
			TipologiaDocumentoDTO tipologiaDoc;
			documento = loadDefaultData(oggetto, nomeFile, numProtocollo, dataProtocollo, content, contentType);

			tipologiaDoc = tipologiaDocumentoDAO.getById(
					(tipologiaDocumentoDAO.getIdsByDescrizione("Certificazione digitale", idAoo, connection).get(0)), connection);
			documento.setIdTipologiaDocumento(tipologiaDoc.getIdTipologiaDocumento());
			final int tipoProcedimento = (int) tipoProcedimentoDAO.getTipiProcedimentoByTipologiaDocumento(connection, tipologiaDoc.getIdTipologiaDocumento()).get(0).getTipoProcedimentoId();
			documento.setIdTipologiaProcedimento(tipoProcedimento);
			final Date now = new Date();
			documento.getAssegnazioni().add(new AssegnazioneDTO("", TipoAssegnazioneEnum.COMPETENZA, DateUtils.dateToString(now, null), now, null, null, new UfficioDTO((long) idNodoDestinatario, null)));
			documento.setAnnoProtocollo(Integer.parseInt(dataProtocollo.substring(dataProtocollo.lastIndexOf('/') + 1)));
			
			if (contentAll != null) {
				final List<AllegatoDTO> allegati = new ArrayList<>();
				final AllegatoDTO allegato = new AllegatoDTO();
				allegato.setContent(contentAll);
				allegato.setMimeType(contentTypeAll);
				allegato.setNomeFile(nomeAll);
				allegato.setFormato(FormatoAllegatoEnum.NON_SPECIFICATO.getId());
				allegati.add(allegato);
				documento.setAllegati(allegati);
			}
			
			final String[] nomeFileSplit = nomeFile.split("_");
	
			documento.setDescrizioneFascicoloProcedimentale(nomeFileSplit[1] + "_" + nomeFileSplit[2] + "_" + nomeFileSplit[3]);
			documento.setIndiceClassificazioneFascicoloProcedimentale(indiceClassificazione);
	
			documento.setMetadatiDinamici(new ArrayList<>());
			documento.getMetadatiDinamici().add(new MetadatoDinamicoDTO(pp.getParameterByKey(PropertiesNameEnum.IGEPA_CODICE_ENTE_IGF_METAKEY), nomeFileSplit[1]));
			documento.getMetadatiDinamici().add(new MetadatoDinamicoDTO(pp.getParameterByKey(PropertiesNameEnum.IGEPA_ANNO_METAKEY), Integer.parseInt(nomeFileSplit[0])));
			documento.getMetadatiDinamici().add(new MetadatoDinamicoDTO(pp.getParameterByKey(PropertiesNameEnum.IGEPA_TIPOLOGIA_ENTE_METAKEY), nomeFileSplit[2]));
			documento.getMetadatiDinamici().add(new MetadatoDinamicoDTO(pp.getParameterByKey(PropertiesNameEnum.IGEPA_DESCRIZIONE_ENTE_METAKEY), nomeFileSplit[3]));
			
			documento.setMittenteContatto(getMittente(connection, aliasMittente, nomeFileSplit[3], idAoo));
		} catch (final Exception e) {
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
		
		return documento;
	}

	private Contatto getMittente(final Connection connection, final String aliasMittente, final String descrizioneEnte, final long idAoo) {
		Contatto mittente = null;
		
		if (aliasMittente == null) {
			mittente = contattoDAO.getContattoByID(Long.parseLong(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IGEPA_ID_CONTATTO)), connection);

		} else {
			mittente = contattoDAO.getContattoByAlias(aliasMittente, connection);

			if (mittente == null) {
				mittente = new Contatto();
				mittente.setCognome(descrizioneEnte);
				mittente.setDatacreazione(new Date());
				mittente.setAliasContatto(aliasMittente);
				mittente.setPubblico(0);
				mittente.setIdAOO(idAoo);
				mittente.setTipoRubrica("RED");
				mittente.setDatacreazione(new Date());
				mittente.setOnTheFly(1);
				final Long idContatto = contattoDAO.insertContatto(mittente, connection);
				mittente.setContattoID(idContatto);
			}
		}
		return mittente;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IIgepaFacadeSRV#insertDocumentoOPF(int,
	 *      java.lang.Integer, byte[], java.lang.String, java.lang.Integer,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 *      java.lang.String, java.lang.Integer, java.lang.Integer,
	 *      java.lang.Integer, java.lang.String, java.lang.String, java.lang.String).
	 */
	@Override
	public String insertDocumentoOPF(final int idNodoDest, final Integer idUtenteDest, final byte[] content, final String contentType,
			final Integer numeroRichiesta, final String annoRichiesta, final String indiceClassificazione, final String numeroConto,
			final String sezione, final String dataProtocollo, final Integer numeroProtocollo, final Integer tipodocumento,
			final Integer tipoProcedimento, final String oggetto, final String enteMittente, final String fileName) { 
		String idDocRitornato;
		
		final String utenteCedente = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IGEPA_UTENTE_CEDENTE);
		final UtenteDTO utente = utenteSRV.getById(Long.parseLong(utenteCedente));
		
		final SalvaDocumentoRedParametriDTO parametri = new SalvaDocumentoRedParametriDTO();
		parametri.setModalita(Constants.Modalita.MODALITA_INS);
		parametri.setIdUfficioDestinatarioWorkflow((long) idNodoDest);
		parametri.setIdUtenteDestinatarioWorkflow((long) idUtenteDest);
		parametri.setIdUtenteMittenteWorkflow(utente.getId());
		parametri.setIdUfficioMittenteWorkflow(utente.getIdUfficio());

		final DetailDocumentRedDTO dto = getDetailDocumentRedDTOfromOPF(idNodoDest, content, contentType,
				numeroRichiesta, annoRichiesta, indiceClassificazione, numeroConto,
				sezione, dataProtocollo, numeroProtocollo, tipodocumento,
				tipoProcedimento, oggetto, enteMittente, fileName, utente.getIdAoo(), utente.getId());

		idDocRitornato = salvaDocSrv.salvaDocumento(dto, parametri, utente, ProvenienzaSalvaDocumentoEnum.IGEPA_OPF).getDocumentTitle();
		
		return idDocRitornato;
	}

	/**
	 * Restituisce il dettaglio documenot red.
	 * 
	 * @param idNodoDest
	 *            nodo destinatario
	 * @param content
	 *            content documento
	 * @param contentType
	 *            content type documento
	 * @param numeroRichiesta
	 *            numero richiesta
	 * @param annoRichiesta
	 *            anno richiesta
	 * @param indiceClassificazione
	 *            indice classificazione
	 * @param numeroConto
	 *            numero conto
	 * @param sezione
	 *            sezione
	 * @param dataProtocollo
	 *            data protocollo
	 * @param numeroProtocollo
	 *            numero protocollo
	 * @param tipodocumento
	 *            tipo documento
	 * @param tipoProcedimento
	 *            tipo procedimento
	 * @param oggetto
	 *            oggetto
	 * @param enteMittente
	 *            ente mittente
	 * @param fileName
	 *            filename del content
	 * @param idAoo
	 *            id area organizzativa
	 * @param idUtenteCreatore
	 *            id utente creatore
	 * @return dettaglio documento
	 */
	private DetailDocumentRedDTO getDetailDocumentRedDTOfromOPF(final int idNodoDest, final byte[] content, final String contentType,
			final Integer numeroRichiesta, final String annoRichiesta, final String indiceClassificazione, final String numeroConto,
			final String sezione, final String dataProtocollo, final Integer numeroProtocollo, final Integer tipodocumento,
			final Integer tipoProcedimento, final String oggetto, final String enteMittente, final String fileName, final long idAoo, final Long idUtenteCreatore) {
		Connection connection = null;
		DetailDocumentRedDTO documento = null;
		PropertiesProvider pp = PropertiesProvider.getIstance();
		
		try {
			connection = getDataSource().getConnection();
			
			TipologiaDocumentoDTO tipologiaDoc;
			
			documento = loadDefaultData(oggetto, fileName, numeroProtocollo, dataProtocollo, content, contentType);
	
			documento.setIdTipologiaProcedimento(tipoProcedimento);
			tipologiaDoc = tipologiaDocumentoDAO.getById(tipodocumento, connection);
			documento.setIdTipologiaDocumento(tipologiaDoc.getIdTipologiaDocumento());
			
			final Date now = new Date();
			documento.getAssegnazioni().add(new AssegnazioneDTO("", TipoAssegnazioneEnum.COMPETENZA, DateUtils.dateToString(now, null), now, null, null, new UfficioDTO((long) idNodoDest, null)));
			documento.setAnnoProtocollo(Integer.parseInt(dataProtocollo.split("/")[2]));
			documento.setIdNodoCreatore(idNodoDest);
			documento.setIdUtenteCreatore(idUtenteCreatore.intValue());
	
			documento.setDescrizioneFascicoloProcedimentale(numeroRichiesta + "_" + annoRichiesta + "_" + tipologiaDoc.getDocumentClass());
			documento.setIndiceClassificazioneFascicoloProcedimentale(indiceClassificazione);
	
			documento.setMetadatiDinamici(new ArrayList<>());
			documento.getMetadatiDinamici().add(new MetadatoDinamicoDTO(pp.getParameterByKey(PropertiesNameEnum.PROVVEDIMENTO_NUMERO_CONTO_METAKEY), numeroConto));
			documento.getMetadatiDinamici().add(new MetadatoDinamicoDTO(pp.getParameterByKey(PropertiesNameEnum.IGEPA_OPF_ANNO_RICHIESTA_METAKEY), Integer.parseInt(annoRichiesta)));
			documento.getMetadatiDinamici().add(new MetadatoDinamicoDTO(pp.getParameterByKey(PropertiesNameEnum.IGEPA_OPF_NUMERO_RICHIESTA_METAKEY), numeroRichiesta.toString()));
			documento.getMetadatiDinamici().add(new MetadatoDinamicoDTO(pp.getParameterByKey(PropertiesNameEnum.IGEPA_OPF_SEZIONE_METAKEY), sezione));
			
			final String[] enteMittenteSplit = enteMittente.split("_");
			documento.setMittenteContatto(getMittente(connection, enteMittenteSplit[0], enteMittenteSplit[1], idAoo));
		} catch (final Exception e) {
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
		return documento;
	}

	private static DetailDocumentRedDTO loadDefaultData(final String oggetto, final String filename, final Integer numProtocollo, final String dataProtocollo, final byte[] content, final String contentType) {
		final DetailDocumentRedDTO documento = new DetailDocumentRedDTO();
		documento.setFormatoDocumentoEnum(FormatoDocumentoEnum.ELETTRONICO);
		documento.setIdCategoriaDocumento(CategoriaDocumentoEnum.ENTRATA.getIds()[0]);
		documento.setIdFormatoDocumento(FormatoDocumentoEnum.ELETTRONICO.getId().intValue());
		documento.setOggetto(oggetto);
		documento.setNomeFile(filename);
		documento.setAssegnazioni(new ArrayList<>());
		documento.setNumeroProtocollo(numProtocollo);
		documento.setDataProtocollo(DateUtils.parseDate(dataProtocollo));
		documento.setContent(content);
		documento.setMimeType(contentType);
		return documento;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IIgepaFacadeSRV#allegaRichiestaIgepa(java.lang.String,
	 *      java.lang.Integer, java.lang.Integer, java.lang.String,
	 *      java.lang.Integer, java.lang.String, byte[], java.lang.String).
	 */
	@Override
	public String allegaRichiestaIgepa(final String annoRichiesta, final Integer numeroRichiesta, final Integer numeroProtocollo, 
			final String oggetto, final Integer tipologia, final String filename, final byte[] content, final String mimeType) {
		String idDoc = null;
		IFilenetCEHelper fceh = null;
		PropertiesProvider pp = PropertiesProvider.getIstance();
		try {
			UtenteDTO utente = utenteSRV.getById(Long.parseLong(pp.getParameterByKey(PropertiesNameEnum.IGEPA_UTENTE_CEDENTE)));
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			
			LOGGER.info("RECUPERO DELLA RICHIESTA OPF");
			final Document d = fceh.getRichiestaOPF(numeroRichiesta, annoRichiesta, numeroProtocollo);
			final FilenetPEHelper pe = new FilenetPEHelper(utente.getFcDTO());
			idDoc = d.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
			
			LOGGER.info("VERIFICA SE LA RICHIESTA OPF è STATA ASSEGNATA");
			final String filter = pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY) + "=" + idDoc;
			final VWQueueQuery query = (VWQueueQuery) pe.getQueueQuery(DocumentQueueEnum.DA_LAVORARE.getName(), DocumentQueueEnum.DA_LAVORARE.getIndexName(), filter);
			if (!query.hasNext()) {
				throw new RedException("02 - Richiesta non assegnata");
			}
			
			LOGGER.info("ALLEGA IL DOCUMENTO ALLA RICHIESTA OPF");
			utente = utenteSRV.getById((long) (d.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.UTENTE_CREATORE_ID_METAKEY))));
			final AllegatoDTO allegato = new AllegatoDTO();
			allegato.setContent(content);
			allegato.setNomeFile(filename);
			allegato.setOggetto(oggetto);
			allegato.setIdTipologiaDocumento(tipologia);
			allegato.setFormato(FormatoDocumentoEnum.ELETTRONICO.getId().intValue());
			allegato.setMimeType(mimeType);
			allegatoSRV.allegaARichiestaOPFIgepa(d, allegato, utente);
			
			LOGGER.info("METTI AGLI ATTI LA RICHIESTA OPF");
			while (query.hasNext()) {
				final VWWorkObject wob = (VWWorkObject) query.next();
				List<String> responseList = null;
				if (wob.getStepResponses() != null) {
					responseList = Arrays.asList(wob.getStepResponses());
				}
				if (responseList != null && responseList.contains(ResponsesRedEnum.ATTI.getResponse())) {
					wob.doLock(true);
					wob.setSelectedResponse(ResponsesRedEnum.ATTI.getResponse());
					wob.doSave(false);
					wob.doDispatch();
	
					utente = utenteSRV.getById(Long.parseLong(wob.getFieldValue(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY)).toString()));
					
					final Integer idFascicolo = (Integer) wob.getFieldValue(pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY));
					// Chiusura del fascicolo
					fascicoloSRV.aggiornaStato(String.valueOf(idFascicolo), FilenetStatoFascicoloEnum.CHIUSO, utente.getIdAoo(), fceh);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}
		
		return idDoc;
	}
	
}
