package it.ibm.red.business.service;

import java.io.Serializable;
import java.sql.Connection;

import it.ibm.red.business.dto.MessaggioPostaNpsDTO;
import it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO;

/**
 * Interfaccia del servizio salvataggio messaggio posta NPS.
 * @param <T>
 */
public interface ISalvaMessaggioPostaNpsSRV<T extends MessaggioPostaNpsDTO> extends Serializable {

	/**
     * Persiste sullo strato di persistenza il messaggio fornito in ingresso e restituisce il guid della mail salvata su Filenet.
     * 
     * @param messaggio messaggio
     * @return GUID della mail salvata su Filenet
     */
	String salvaMessaggio(RichiestaElabMessaggioPostaNpsDTO<T> richiesta, Connection con);
}
