package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Set;

import it.ibm.red.business.dto.TitolarioDTO;

/**
 * 
 * @author CPIERASC
 *
 *	Dao gestione fascicolo.
 */
public interface IFascicoloDAO extends Serializable {
	
	/**
	 * Recupero fascicolo procedimentale.
	 * 
	 * @param idDocumento	identificativo documento
	 * @param idAoo			identificativo aoo
	 * @param connection	connessione
	 * @return				id fascicolo
	 */
	Integer getIdFascicoloProcedimentale(String idDocumento, Long idAoo, Connection connection);

	/**
	 * Crea il fascicolo sulla base dati.
	 * @param indiceClassificazione
	 * @param idAoo
	 * @param con
	 * @return
	 */
	int insert(String indiceClassificazione, Long idAoo, Connection con);
	
	/**
	 * Inserisce il documento nel fascicolo.
	 * @param idDocumento
	 * @param idFascicolo
	 * @param automatico
	 * @param idAoo
	 * @param con
	 * @return
	 */
	int insertDocumentoFascicolo(int idDocumento, int idFascicolo, int automatico, Long idAoo, Connection con);

	/**
	 * Aggiorna il documento nel fascicolo.
	 * @param idDocumento
	 * @param idFascicoloNew
	 * @param idFascicoloOld
	 * @param automatico
	 * @param idAoo
	 * @param con
	 * @return
	 */
	int updateDocumentoFascicolo(int idDocumento, int idFascicoloNew, int idFascicoloOld, int automatico, Long idAoo, Connection con);

	/**
	 * Ottiene l'indice di classificazione.
	 * @param idFascicolo
	 * @param idAoo
	 * @param con
	 * @return indice di classificazione
	 */
	String getIndiceClassificazione(String idFascicolo, Long idAoo, Connection con);

	/**
	 * Aggiorna l'indice di classificazione.
	 * @param idFascicolo
	 * @param indiceClassificazione
	 * @param idAoo
	 * @param con
	 * @return
	 */
	int aggiornaIndiceClassificazione(int idFascicolo, String indiceClassificazione, Long idAoo, Connection con);

	/**
	 * Verifica l'associazione del documento al fascicolo.
	 * @param idDocumento
	 * @param idFascicolo
	 * @param idAoo
	 * @param con
	 * @return true o false
	 */
	boolean isDocumentoAssociato(int idDocumento, int idFascicolo, Long idAoo, Connection con);

	/**
	 * Elimina il documento dal fascicolo.
	 * @param idDocumento
	 * @param idFascicolo
	 * @param idAoo
	 * @param con
	 * @return
	 */
	int deleteDocumentoFascicolo(int idDocumento, int idFascicolo, Long idAoo, Connection con);

	/**
	 * Ottiene i fascicoli associati al documento.
	 * @param idDocumento
	 * @param idAoo
	 * @param con
	 * @return ids dei fascicoli
	 */
	Set<String> getFascicoliDocumento(int idDocumento, Long idAoo, Connection con);
	
	/**
	 * Ottiene l'indice di classificazione per il fascicolo.
	 * @param idFascicolo
	 * @param idAoo
	 * @param con
	 * @return titolario
	 */
	TitolarioDTO getIndiceClassificazioneByIdFascicolo(String idFascicolo, Long idAoo, Connection con);
}