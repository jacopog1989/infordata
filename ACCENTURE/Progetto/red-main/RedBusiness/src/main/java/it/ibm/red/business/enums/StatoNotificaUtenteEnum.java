package it.ibm.red.business.enums;
 

/**
 * StatoNotificaUtenteEnum
 *
 * @author VINGENITO
 * 
 *         Tipologie di codifica.
 */
public enum StatoNotificaUtenteEnum {
	
	/**
	 * Valore.
	 */
	NOTIFICA_DA_LEGGERE(0, "NOTIFICA_DA_LEGGERE"),
	
	/**
	 * Valore.
	 */
	NOTIFICA_LETTA(1, "NOTIFICA_LETTA"),
	
	/**
	 * Valore.
	 */
	NOTIFICA_ELIMINATA(2, "NOTIFICA_ELIMINATA");
	  	
	
	/**
	 * Identificativo.
	 */
	private Integer idStato;
	
	/**
	 * Descrizione.
	 */
	private String stato;
	
	/**
	 * Costruttore.
	 * @param inIdStato
	 * @param inStato
	 */
	StatoNotificaUtenteEnum(final Integer inIdStato, final String inStato) {
		idStato = inIdStato;
		stato = inStato;
	}
	
	/**
	 * Restituisce l'id dello stato.
	 * @return id stato
	 */
	public Integer getIdStato() {
		return idStato;
	}
	
	/**
	 * Restituisce lo stato dell'enum.
	 * @return stato
	 */
	public String getStato() {
		return stato;
	}

	/**
	 * Restituisce l'enum associato all'id dello stato.
	 * @param idStato
	 * @return stato associato all'id
	 */
	public static StatoNotificaUtenteEnum get(final Integer idStato) {
		StatoNotificaUtenteEnum output = null;
		for (final StatoNotificaUtenteEnum t:StatoNotificaUtenteEnum.values()) {
			if (idStato.equals(t.getIdStato())) {
				output = t;
				break;
			}
		}
		return output;
	}
	 
}