package it.ibm.red.business.enums;

/**
 * Enum fascicolo fepa.
 */
public enum FascicoloFepaEnum {
	
	/**
	 * Valore.
	 */
	FASCICOLO_DECRETO_IMPEGNO("DI"),
	
	/**
	 * Valore.
	 */
	FASCICOLO_SPECIALE_ORDINE_DI_PAGAMENTO("FS"),
	
	/**
	 * Valore.
	 */
	FASCICOLO_ORDINE_PAGAMENTO("FO"),
	
	/**
	 * Valore.
	 */
	FASCICOLO_VERBALE_REVISIONE("FV"),
	
	/**
	 * Valore.
	 */
	FASCICOLO_DECRETO_ASSEGNAZIONE_FONDI("DS"),
	
	/**
	 * Valore.
	 */
	FASCICOLO_ORDINE_PRELEVAMENTO_FONDI("OD"),
	
	/**
	 * Valore.
	 */
	FASCICOLO_FATTURA("FF"),
	
	/**
	 * Valore.
	 */
	FASCICOLO_IMPEGNO("IM"),
	
	/**
	 * Valore.
	 */
	FASCICOLO_IPE("IP"),
	
	/**
	 * Valore.
	 */
	FASCICOLO_MANDATO("MA"),
	
	/**
	 * Valore.
	 */
	FASCICOLO_OIL("OI"),
	
	/**
	 * Valore.
	 */
	FASCICOLO_CRONOPROGRAMMA_AMMNE_CENTRALE("PN"),
	
	/**
	 * Valore.
	 */
	FASCICOLO_CRONOPROGRAMMA_FUNZIONARIO_DELEGATO("FD"),
	
	/**
	 * Valore.
	 */
	FASCICOLO_REVERSALE("RE"),
	
	/**
	 * Valore.
	 */
	FASCICOLO_RENDICONTO("RT");
	

	/**
	 * Tipologia fascicolo.
	 */
	private String tipoFascicolo;
	
	FascicoloFepaEnum(final String tipoFascicolo) {
		this.tipoFascicolo = tipoFascicolo;
	}

	/**
	 * Restituisce il tipo fascicolo.
	 * @return tipo fascicolo
	 */
	public String getTipoFascicolo() {
		return tipoFascicolo;
	}
	
	/**
	 * Restituisce l'Enum associata all'id.
	 * @param id
	 * @return FascicoloFepaEnum
	 */
	public static FascicoloFepaEnum getEnumById(final String id) {
		FascicoloFepaEnum output = null;
		
		for (final FascicoloFepaEnum item : FascicoloFepaEnum.values()) {
			if (item.getTipoFascicolo().equals(id)) {
				output = item;
				break;
			}
		}

		return output;
	}
	
}
