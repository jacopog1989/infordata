package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author m.crescentini
 *
 */
public interface IPkHandlerFacadeSRV extends Serializable {
	
	/**
	 * Il metodo recupera le URL di tutti gli handler PK configurati nella base dati.
	 * 
	 * @return
	 */
	List<String> getAllUrl();
}