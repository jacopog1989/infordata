package it.ibm.red.business.dto;

import java.util.Date;
import java.util.List; 

/**
 * 
 * @author VINGENITO
 *
 */
public class DetailFascicoloPregressoDTO extends AbstractDTO {
 
	private static final long serialVersionUID = 890027409989647008L;
	
	/**
	 * Business owner.
	 */
	private String businessOwner;
	
	/**
	 * Classe documentale.
	 */
	private String classeDocumentale;
	
	/**
	 * Classificazione.
	 */
	private String classificazione;
	
	/**
	 * Codice aoo.
	 */
	private String codAoo;
	
	/**
	 * Codice titolario.
	 */
	private String codTitolario;
	
	/**
	 * Codice ufficio destinatario.
	 */
	private String codUffDestinatario;
	
	/**
	 * Data apertura.
	 */
	private Date dataApertura;
	
	/**
	 * Data chiusura.
	 */
	private Date dataChiusura;
	
	/**
	 * Data scadenza.
	 */
	private Date dataScadenza;
	
	/**
	 * Descrizione aoo.
	 */
	private String descrAoo;
	
	/**
	 * Descrizione.
	 */
	private String descrizione;
	
	/**
	 * Descrizione tiotlario.
	 */
	private String descrTitolario;
	
	/**
	 * Descrizione ufficio proprietario.
	 */
	private String descrUffProprietario;
	
	/**
	 * Destinarario.
	 */
	private String destinatario;
	
	/**
	 * Identificativo fascicolo.
	 */
	private String idFascicolo;
	
	/**
	 * Identificativo folder.
	 */
	private String idFolder;
	
	/**
	 * Identificativo folder nsd.
	 */
	private String idFolderNsd;
	
	/**
	 * Lista documenti.
	 */
	private List<DocumentiFascicoloNsdDTO> listaDocumenti;
	
	/**
	 * Lista fascicoli.
	 */
	private List<DocumentiFascicoloNsdDTO> listaFascicolo;
	
	/**
	 * Motivo chiusura.
	 */
	private String motivoChiusura;
	
	/**
	 * Note.
	 */
	private String note;
	
	/**
	 * Numero documenti.
	 */
	private Integer numDocumenti;
	
	/**
	 * Numero fascicolo.
	 */
	private Integer numeroFascicolo;
	
	/**
	 * Owner.
	 */
	private String owner;
	
	/**
	 * Proprietario.
	 */
	private String proprietario;
	
	/**
	 * Responsabile.
	 */
	private String responsabile;
	
	/**
	 * Stato.
	 */
	private Integer stato;
	
	/**
	 * Tipologia.
	 */
	private String tipologia;
	
	/**
	 * Folder.
	 */
	private String folderSignature;
	
	/**
	 * Flag visibile.
	 */
	private boolean visible;
	
	/**
	 * ID.
	 */
	private String iChronicleId;
	
	/**
	 * Nome cartella.
	 */
	private String nomeCartella;
	
	/**
	 * @param nomeCartella the nomeCartella to set
	 */
	public void setNomeCartella(final String nomeCartella) {
		this.nomeCartella = nomeCartella;
	}
	
	/**
	 * @return the nomeCartella
	 */
	public String getNomeCartella() {
		return nomeCartella;
	}
	
	/**
	 * @return the businessOwner
	 */
	public String getBusinessOwner() {
		return businessOwner;
	}
	
	/**
	 * @param businessOwner the businessOwner to set
	 */
	public void setBusinessOwner(final String businessOwner) {
		this.businessOwner = businessOwner;
	}
	/**
	 * @return the classeDocumentale
	 */
	public String getClasseDocumentale() {
		return classeDocumentale;
	}
	/**
	 * @param classeDocumentale the classeDocumentale to set
	 */
	public void setClasseDocumentale(final String classeDocumentale) {
		this.classeDocumentale = classeDocumentale;
	}
	/**
	 * @return the classificazione
	 */
	public String getClassificazione() {
		return classificazione;
	}
	/**
	 * @param classificazione the classificazione to set
	 */
	public void setClassificazione(final String classificazione) {
		this.classificazione = classificazione;
	}
	/**
	 * @return the codAoo
	 */
	public String getCodAoo() {
		return codAoo;
	}
	/**
	 * @param codAoo the codAoo to set
	 */
	public void setCodAoo(final String codAoo) {
		this.codAoo = codAoo;
	}
	/**
	 * @return the codTitolario
	 */
	public String getCodTitolario() {
		return codTitolario;
	}
	/**
	 * @param codTitolario the codTitolario to set
	 */
	public void setCodTitolario(final String codTitolario) {
		this.codTitolario = codTitolario;
	}
	/**
	 * @return the codUffDestinatario
	 */
	public String getCodUffDestinatario() {
		return codUffDestinatario;
	}
	/**
	 * @param codUffDestinatario the codUffDestinatario to set
	 */
	public void setCodUffDestinatario(final String codUffDestinatario) {
		this.codUffDestinatario = codUffDestinatario;
	}
	/**
	 * @return the dataApertura
	 */
	public Date getDataApertura() {
		return dataApertura;
	}
	/**
	 * @param dataApertura the dataApertura to set
	 */
	public void setDataApertura(final Date dataApertura) {
		this.dataApertura = dataApertura;
	}
	/**
	 * @return the dataChiusura
	 */
	public Date getDataChiusura() {
		return dataChiusura;
	}
	/**
	 * @param dataChiusura the dataChiusura to set
	 */
	public void setDataChiusura(final Date dataChiusura) {
		this.dataChiusura = dataChiusura;
	}
	/**
	 * @return the dataScadenza
	 */
	public Date getDataScadenza() {
		return dataScadenza;
	}
	/**
	 * @param dataScadenza the dataScadenza to set
	 */
	public void setDataScadenza(final Date dataScadenza) {
		this.dataScadenza = dataScadenza;
	}
	/**
	 * @return the descrAoo
	 */
	public String getDescrAoo() {
		return descrAoo;
	}
	/**
	 * @param descrAoo the descrAoo to set
	 */
	public void setDescrAoo(final String descrAoo) {
		this.descrAoo = descrAoo;
	}
	/**
	 * @return the descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}
	/**
	 * @param descrizione the descrizione to set
	 */
	public void setDescrizione(final String descrizione) {
		this.descrizione = descrizione;
	}
	/**
	 * @return the descrTitolario
	 */
	public String getDescrTitolario() {
		return descrTitolario;
	}
	/**
	 * @param descrTitolario the descrTitolario to set
	 */
	public void setDescrTitolario(final String descrTitolario) {
		this.descrTitolario = descrTitolario;
	}
	/**
	 * @return the descrUffProprietario
	 */
	public String getDescrUffProprietario() {
		return descrUffProprietario;
	}
	/**
	 * @param descrUffProprietario the descrUffProprietario to set
	 */
	public void setDescrUffProprietario(final String descrUffProprietario) {
		this.descrUffProprietario = descrUffProprietario;
	}
	/**
	 * @return the destinatario
	 */
	public String getDestinatario() {
		return destinatario;
	}
	/**
	 * @param destinatario the destinatario to set
	 */
	public void setDestinatario(final String destinatario) {
		this.destinatario = destinatario;
	}
	/**
	 * @return the idFascicolo
	 */
	public String getIdFascicolo() {
		return idFascicolo;
	}
	/**
	 * @param idFascicolo the idFascicolo to set
	 */
	public void setIdFascicolo(final String idFascicolo) {
		this.idFascicolo = idFascicolo;
	}
	/**
	 * @return the idFolder
	 */
	public String getIdFolder() {
		return idFolder;
	}
	/**
	 * @param idFolderId the idFolderId to set
	 */
	public void setIdFolder(final String idFolder) {
		this.idFolder = idFolder;
	}
	/**
	 * @return the idFolderNsd
	 */
	public String getIdFolderNsd() {
		return idFolderNsd;
	}
	/**
	 * @param idFolderNsd the idFolderNsd to set
	 */
	public void setIdFolderNsd(final String idFolderNsd) {
		this.idFolderNsd = idFolderNsd;
	}
	/**
	 * @return the listaDocumenti
	 */
	public List<DocumentiFascicoloNsdDTO> getListaDocumenti() {
		return listaDocumenti;
	}
	/**
	 * @param listaDocumenti the listaDocumenti to set
	 */
	public void setListaDocumenti(final List<DocumentiFascicoloNsdDTO> listaDocumenti) {
		this.listaDocumenti = listaDocumenti;
	}
	/**
	 * @return the motivoChiusura
	 */
	public String getMotivoChiusura() {
		return motivoChiusura;
	}
	/**
	 * @param motivoChiusura the motivoChiusura to set
	 */
	public void setMotivoChiusura(final String motivoChiusura) {
		this.motivoChiusura = motivoChiusura;
	}
	/**
	 * @return the note
	 */
	public String getNote() {
		return note;
	}
	/**
	 * @param note the note to set
	 */
	public void setNote(final String note) {
		this.note = note;
	}
	/**
	 * @return the numDocumenti
	 */
	public Integer getNumDocumenti() {
		return numDocumenti;
	}
	/**
	 * @param numDocumenti the numDocumenti to set
	 */
	public void setNumDocumenti(final Integer numDocumenti) {
		this.numDocumenti = numDocumenti;
	}
	/**
	 * @return the numeroFasciolo
	 */
	public Integer getNumeroFascicolo() {
		return numeroFascicolo;
	}
	/**
	 * @param numeroFasciolo the numeroFasciolo to set
	 */
	public void setNumeroFascicolo(final Integer numeroFascicolo) {
		this.numeroFascicolo = numeroFascicolo;
	}
	/**
	 * @return the owner
	 */
	public String getOwner() {
		return owner;
	}
	/**
	 * @param owner the owner to set
	 */
	public void setOwner(final String owner) {
		this.owner = owner;
	}
	/**
	 * @return the proprietario
	 */
	public String getProprietario() {
		return proprietario;
	}
	/**
	 * @param proprietario the proprietario to set
	 */
	public void setProprietario(final String proprietario) {
		this.proprietario = proprietario;
	}
	/**
	 * @return the responsabile
	 */
	public String getResponsabile() {
		return responsabile;
	}
	/**
	 * @param responsabile the responsabile to set
	 */
	public void setResponsabile(final String responsabile) {
		this.responsabile = responsabile;
	}
	/**
	 * @return the stato
	 */
	public Integer getStato() {
		return stato;
	}
	/**
	 * @param stato the stato to set
	 */
	public void setStato(final Integer stato) {
		this.stato = stato;
	}
	/**
	 * @return the tipologia
	 */
	public String getTipologia() {
		return tipologia;
	}
	/**
	 * @param tipologia the tipologia to set
	 */
	public void setTipologia(final String tipologia) {
		this.tipologia = tipologia;
	}
	/**
	 * @return the listaFascicolo
	 */
	public List<DocumentiFascicoloNsdDTO> getListaFascicolo() {
		return listaFascicolo;
	}
	/**
	 * @param listaFascicolo the listaFascicolo to set
	 */
	public void setListaFascicolo(final List<DocumentiFascicoloNsdDTO> listaFascicolo) {
		this.listaFascicolo = listaFascicolo;
	}
	/**
	 * @return the folderSignature
	 */
	public String getFolderSignature() {
		return folderSignature;
	}
	/**
	 * @param folderSignature the folderSignature to set
	 */
	public void setFolderSignature(final String folderSignature) {
		this.folderSignature = folderSignature;
	}
	/**
	 * @return the visible
	 */
	public boolean isVisible() {
		return visible;
	}
	/**
	 * @param visible the visible to set
	 */
	public void setVisible(final boolean visible) {
		this.visible = visible;
	}
	/**
	 * @return the iChronicleId
	 */
	public String getiChronicleId() {
		return iChronicleId;
	}
	/**
	 * @param iChronicleId the iChronicleId to set
	 */
	public void setiChronicleId(final String iChronicleId) {
		this.iChronicleId = iChronicleId;
	}

	
}
