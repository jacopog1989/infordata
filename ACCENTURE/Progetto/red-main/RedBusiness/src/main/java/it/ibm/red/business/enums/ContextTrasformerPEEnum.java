package it.ibm.red.business.enums;

/**
 * Enum che definisce i Context Trasformer PE.
 */
public enum ContextTrasformerPEEnum {
	
    /**
     * Uffici mittenti.
     */
	UFFICI_MITTENTI,
	
    /**
     * Utenti mittenti.
     */
	UTENTI_MITTENTI,
	
    /**
     * UCB.
     */
	UCB;
}
