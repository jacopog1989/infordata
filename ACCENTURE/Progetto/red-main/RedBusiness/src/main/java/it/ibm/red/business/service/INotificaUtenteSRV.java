package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.INotificaUtenteFacadeSRV;

/**
 * The Interface INotificaUtenteSRV.
 *
 * @author m.crescentini
 * 
 * 		Servizi per la gestione delle notifiche utente 
 * 		(pannello con notifiche sottoscrizioni, calendario e rubrica).
 */
public interface INotificaUtenteSRV extends INotificaUtenteFacadeSRV {

}
