package it.ibm.red.business.dto;

import java.util.Date;

/**
 * Classe ProtocolloDTO.
 *
 * @author CPIERASC
 * 
 * 	DTO per i dati del protocollo.
 */
public class ProtocolloDTO extends AbstractDTO {

	/**
	 * Constante serialVersionUID.
	 */
	private static final long serialVersionUID = -3960999096373425926L;

	/**
	 * Anno protocollo.
	 */
	private final String annoProtocollo;

	/**
	 * Data protocollo.
	 */
	private final Date dataProtocollo;

	/**
	 * Identificativo protocollo.
	 */
	private final String idProtocollo;

	/**
	 * Numero protocollo.
	 */
	private final Integer numeroProtocollo;
	
	/**
	 * Descrizione tipologia documento NPS.
	 */
	private final String descrizioneTipologiaDocumentoNps;
	
	/**
	 * Oggetto protocollo NPS.
	 */
	private final String oggettoProtocolloNps;
	
	/**
	 * Identificativo (codice) del registro di protocollo.
	 */
	private final String registroProtocollo;
	
	
	/**
	 * Costruttore.
	 * 
	 * @param inIdProtocollo		id protocollo
	 * @param inNumeroProtocollo	numero protocollo
	 * @param inAnnoProtocollo		anno protocollo
	 * @param inDataProtocollo		data protocollo
	 * @param inDescrizioneTipoDocumentoNps - Descrizione tipologia documento NPS
	 * @param inOggettoProtocolloNps - Oggetto protocollo NPS
	 */
	public ProtocolloDTO(final String inIdProtocollo, final Integer inNumeroProtocollo, final String inAnnoProtocollo, final Date inDataProtocollo, 
			final String inDescrizioneTipoDocumentoNps, final String inOggettoProtocolloNps) {
		this(inIdProtocollo, inNumeroProtocollo, inAnnoProtocollo, inDataProtocollo, inDescrizioneTipoDocumentoNps, inOggettoProtocolloNps, null);
	}
	

	/**
	 * Costruttore.
	 * 
	 * @param inIdProtocollo					ID protocollo
	 * @param inNumeroProtocollo				Numero protocollo
	 * @param inAnnoProtocollo					Anno protocollo
	 * @param inDataProtocollo					Data protocollo
	 * @param inDescrizioneTipoDocumentoNps		Descrizione tipologia documento NPS
	 * @param inOggettoProtocolloNps			Oggetto protocollo NPS
	 * @param inRegistroProtocollo				Identificativo (codice) del registro di protocollo
	 */
	public ProtocolloDTO(final String inIdProtocollo, final Integer inNumeroProtocollo, final String inAnnoProtocollo, final Date inDataProtocollo, 
			final String inDescrizioneTipoDocumentoNps, final String inOggettoProtocolloNps, final String inRegistroProtocollo) {
		super();
		this.idProtocollo = inIdProtocollo;
		this.numeroProtocollo = inNumeroProtocollo;
		this.annoProtocollo = inAnnoProtocollo;
		this.dataProtocollo = inDataProtocollo;
		this.descrizioneTipologiaDocumentoNps = inDescrizioneTipoDocumentoNps;
		this.oggettoProtocolloNps = inOggettoProtocolloNps;
		this.registroProtocollo = inRegistroProtocollo;
	}
	
	/** 
	 * @return	oggetto protocollo NPS
	 */
	public final String getOggettoProtocolloNps() {
		return oggettoProtocolloNps;
	}

	/** 
	 * @return	anno protocollo
	 */
	public final String getAnnoProtocollo() {
		return annoProtocollo;
	}
	
	/** 
	 * @return descrizioneTipologiaDocumentoNps
	 */
	public final String getDescrizioneTipologiaDocumentoNps() {
		return descrizioneTipologiaDocumentoNps;
	}

	/** 
	 * @return	anno protocollo
	 */
	public final Integer getAnnoProtocolloInt() {
		if (annoProtocollo != null) {
			return Integer.parseInt(annoProtocollo);
		}
		return null;
	}

	/** 
	 * @return	data protocollo
	 */
	public final Date getDataProtocollo() {
		return dataProtocollo;
	}

	/** 
	 * @return	id protocollo
	 */
	public final String getIdProtocollo() {
		return idProtocollo;
	}

	/** 
	 * @return	numero protocollo
	 */
	public final int getNumeroProtocollo() {
		if (numeroProtocollo != null) {
			return numeroProtocollo.intValue();
		}
		
		return 0;
	}
	
	/** 
	 * @param codiceAoo the codice aoo
	 * @return the numero anno protocollo NPS
	 */
	public String getNumeroAnnoProtocolloNPS(final String codiceAoo) {
		return numeroProtocollo + "/" + annoProtocollo + "_" + codiceAoo;
	}

	/** 
	 * @return the registroProtocollo
	 */
	public String getRegistroProtocollo() {
		return registroProtocollo;
	}
	
}
