package it.ibm.red.business.dto;

/**
 * DTO che definisce la ricerca rubrica.
 */
public class RicercaRubricaDTO extends AbstractDTO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Nome.
	 */
	private String nome;

	/**
	 * Cognome.
	 */
	private String cognome;

	/**
	 * Mail.
	 */
	private String mail;

	/**
	 * Alias descrizione.
	 */
	private String aliasDescrizione;

	/**
	 * Aoo.
	 */
	private Integer idAoo;
	

	/**
	 * Flag ufficio.
	 */
	private Boolean isUfficio = false;

	/**
	 * Livello I.
	 */
	private String struLivelloI;

	/**
	 * Livello II.
	 */
	private String struLivelloII;

	/**
	 * Livello III.
	 */
	private String struLivelloIII;
	
	/**
	 * Flag ricerca RED.
	 */
	private Boolean ricercaRED = false;
	
	/**
	 * Flag ricerca MEF.
	 */
	private Boolean ricercaMEF = false;
	
	/**
	 * Flag ricerca IPA.
	 */
	private Boolean ricercaIPA = false;
	
	/**
	 * Flag ricerca gruppo.
	 */
	private Boolean ricercaGruppo = false;  
	
	/**
	 * Flag ricerca ricerca rubrica.
	 */
	private Boolean ricercaRubricaUfficio = false;
	
	/**
	 * Flag show ufficio/personale.
	 */
	private Boolean showUfficioOPersonale = false;
	 
	/**
	 * Identificativo gruppo.
	 */
	private Long idGruppo;
	 
	/**
	 * Query ricerca.
	 */
	private String queryRicerca;
	
	
	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * @param nome the nome to set
	 */
	public void setNome(final String nome) {
		this.nome = nome;
	}
	/**
	 * @return the cognome
	 */
	public String getCognome() {
		return cognome;
	}
	/**
	 * @param cognome the cognome to set
	 */
	public void setCognome(final String cognome) {
		this.cognome = cognome;
	}
	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}
	/**
	 * @param mail the mail to set
	 */
	public void setMail(final String mail) {
		this.mail = mail;
	}
	/**
	 * @return the aliasDescrizione
	 */
	public String getAliasDescrizione() {
		return aliasDescrizione;
	}
	/**
	 * @param aliasDescrizione the aliasDescrizione to set
	 */
	public void setAliasDescrizione(final String aliasDescrizione) {
		this.aliasDescrizione = aliasDescrizione;
	}
	/**
	 * @return the ricercaRED
	 */
	public Boolean getRicercaRED() {
		return ricercaRED;
	}
	/**
	 * @param ricercaRED the ricercaRED to set
	 */
	public void setRicercaRED(final Boolean ricercaRED) {
		this.ricercaRED = ricercaRED;
		if (Boolean.TRUE.equals(this.ricercaRED)) {
			this.ricercaMEF = true;
			this.ricercaRubricaUfficio = false;
		}
	}
	/**
	 * @return the ricercaMEF
	 */
	public Boolean getRicercaMEF() {
		return ricercaMEF;
	}
	/**
	 * @param ricercaMEF the ricercaMEF to set
	 */
	public void setRicercaMEF(final Boolean ricercaMEF) {
		this.ricercaMEF = ricercaMEF;
	}
	/**
	 * @return the ricercaIPA
	 */
	public Boolean getRicercaIPA() {
		return ricercaIPA;
	}
	/**
	 * @param ricercaIPA the ricercaIPA to set
	 */
	public void setRicercaIPA(final Boolean ricercaIPA) {
		this.ricercaIPA = ricercaIPA;
		if (Boolean.TRUE.equals(this.ricercaIPA)) {
			this.ricercaRubricaUfficio = false;
		}
	}
	/**
	 * @return the ricercaGruppo
	 */
	public Boolean getRicercaGruppo() {
		return ricercaGruppo;
	}
	/**
	 * @param ricercaGruppo the ricercaGruppo to set
	 */
	public void setRicercaGruppo(final Boolean ricercaGruppo) {
		this.ricercaGruppo = ricercaGruppo;
	}

	/**
	 * Restituisce l'id dell'Area Organizzativa Omogenea.
	 * @return id AOO
	 */
	public Integer getIdAoo() {
		return idAoo;
	}
	
	/**
	 * Imposta l'id dell'Area Organizzativa Omogenea.
	 * @param idAoo
	 */
	public void setIdAoo(final Integer idAoo) {
		this.idAoo = idAoo;
	}
	
	/**
	 * Restituisce true se un ufficio, false altrimenti.
	 * @return true se ufficio, false altrimenti
	 */
	public Boolean getIsUfficio() {
		return isUfficio;
	}
	
	/**
	 * Imposta il flag associato all'ufficio.
	 * @param isUfficio
	 */
	public void setIsUfficio(final Boolean isUfficio) {
		this.isUfficio = isUfficio;
	}
	
	/**
	 * Restituisce la struttura del primo lviello.
	 * @return struttura primo livello
	 */
	public String getStruLivelloI() {
		return struLivelloI;
	}
	
	/**
	 * Imposta la struttura del primo lviello.
	 * @param struLivelloI
	 */
	public void setStruLivelloI(final String struLivelloI) {
		this.struLivelloI = struLivelloI;
	}
	
	/**
	 * Restituisce la struttura del secondo livello.
	 * @return struttura secondo livello
	 */
	public String getStruLivelloII() {
		return struLivelloII;
	}
	
	/**
	 * Imposta la struttura del secondo livello.
	 * @param struLivelloII
	 */
	public void setStruLivelloII(final String struLivelloII) {
		this.struLivelloII = struLivelloII;
	}

	/**
	 * Restituisce la struttura del terzo livello.
	 * @return struttura terzo livello
	 */
	public String getStruLivelloIII() {
		return struLivelloIII;
	}
	
	/**
	 * Imposta la struttura del terzo livello.
	 * @param struLivelloIII
	 */
	public void setStruLivelloIII(final String struLivelloIII) {
		this.struLivelloIII = struLivelloIII;
	}
	
	/**
	 * Restituisce l'id del gruppo.
	 * @return id gruppo
	 */
	public Long getIdGruppo() {
		return idGruppo;
	}
	
	/**
	 * Imposta l'id del gruppo.
	 * @param idGruppo
	 */
	public void setIdGruppo(final Long idGruppo) {
		this.idGruppo = idGruppo;
	}

	/**
	 * Restituisce true se in ricerca rubrica ufficio, false altrimenti.
	 * @return true se in ricerca rubrica ufficio, false altrimenti
	 */
	public Boolean getRicercaRubricaUfficio() {
		return ricercaRubricaUfficio;
	}

	/**
	 * Imposta il flag associato alla ricerca rubrica ufficio.
	 * @param ricercaRubricaUfficio
	 */
	public void setRicercaRubricaUfficio(final Boolean ricercaRubricaUfficio) {
		this.ricercaRubricaUfficio = ricercaRubricaUfficio;
		if (Boolean.TRUE.equals(this.ricercaRubricaUfficio)) {
			this.ricercaRED = false;
			this.ricercaIPA = false;
		}
	}
	
	/**
	 * Restituisce true se ufficio o personale visibile, false altrimenti.
	 * @return sgowUfficioOPersonale
	 */
	public Boolean getShowUfficioOPersonale() {
		return showUfficioOPersonale;
	}
	
	/**
	 * Imposta il flag associato alla visibilita di Ufficio O Personale.
	 * @param showUfficioOPersonale
	 */
	public void setShowUfficioOPersonale(final Boolean showUfficioOPersonale) {
		this.showUfficioOPersonale = showUfficioOPersonale;
	}
	
	/**
	 * Restituisce la query di ricerca.
	 * @return query ricerca
	 */
	public String getQueryRicerca() {
		return queryRicerca;
	}
	
	/**
	 * Imposta la query di ricerca.
	 * @param queryRicerca
	 */
	public void setQueryRicerca(final String queryRicerca) {
		this.queryRicerca = queryRicerca;
	}
}
