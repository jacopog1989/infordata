package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.UtenteDTO;

/**
 * Facade del servizio di gestione code documenti.
 */
public interface ICodeDocumentiFacadeSRV extends Serializable {
	
	/**
	 * Archivia i documenti nelle code applicative di ufficio e utente.
	 */
	void archiviaCodeApplicative();
	
	/**
	 * Aggiorna gli item recall nella documentoutentestato in lavorate per il documento in input
	 *
	 * @param idDocumento
	 */
	void fromRecallToLavorate(String idDocumento, UtenteDTO utente);
	
	/**
	 * Recupera i document title dei documenti "in scadenza" (con la data scadenza valorizzata), suddivisi per coda.
	 * N.B. Sono prese in considerazione solo le code FileNet mostrate nella home page, accanto al Calendario.
	 * 
	 * @param utente
	 * @return Una mappa Nome coda -> Lista di document title dei documenti "in scadenza"
	 */
	Map<String, List<String>> getDocumentiInScadenzaCodeHomePage(UtenteDTO utente);
	
}