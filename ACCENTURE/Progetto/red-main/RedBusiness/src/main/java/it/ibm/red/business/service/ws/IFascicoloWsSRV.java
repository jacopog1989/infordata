package it.ibm.red.business.service.ws;

import java.sql.Connection;
import java.util.List;
import java.util.Set;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.core.Document;

import it.ibm.red.business.dto.FascicoloWsDTO;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.service.ws.facade.IFascicoloWsFacadeSRV;

/**
 * The Interface IFascicoloWsSRV.
 *
 * @author a.dilegge
 * 
 *         Interfaccia servizio gestione fascicolo web service.
 */
public interface IFascicoloWsSRV extends IFascicoloWsFacadeSRV {

	/**
	 * @param fascicoliFilenet
	 * @param con
	 * @return
	 */
	List<FascicoloWsDTO> transformToFascicoliWs(DocumentSet fascicoliFilenet, Connection con);
	
	/**
	 * @param fascicoliFilenet
	 * @param withMetadati
	 * @param inMetadatiDaRecuperare
	 * @param withSecurity
	 * @param fceh
	 * @param con
	 * @return
	 */
	List<FascicoloWsDTO> transformToFascicoliWs(DocumentSet fascicoliFilenet, boolean withMetadati, Set<String> inMetadatiDaRecuperare, 
			boolean withSecurity, IFilenetCEHelper fceh, Connection con);
	
	/**
	 * @param fascicoloFilenet
	 * @param withMetadati
	 * @param inMetadatiDaRecuperare
	 * @param withSecurity
	 * @param fceh
	 * @param con
	 * @return
	 */
	FascicoloWsDTO transformToFascicoloWs(Document fascicoloFilenet, boolean withMetadati, Set<String> inMetadatiDaRecuperare, boolean withSecurity, 
			IFilenetCEHelper fceh, Connection con);

}