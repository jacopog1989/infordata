package it.ibm.red.business.helper.filenet.ce;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.activation.DataHandler;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.util.CollectionUtils;

import com.filenet.api.admin.ClassDefinition;
import com.filenet.api.admin.PropertyDefinition;
import com.filenet.api.admin.PropertyTemplate;
import com.filenet.api.collection.AccessPermissionList;
import com.filenet.api.collection.AnnotationSet;
import com.filenet.api.collection.ComponentRelationshipSet;
import com.filenet.api.collection.ContentElementList;
import com.filenet.api.collection.DocumentSet;
import com.filenet.api.collection.EngineCollection;
import com.filenet.api.collection.FolderSet;
import com.filenet.api.collection.GroupSet;
import com.filenet.api.collection.IndependentObjectSet;
import com.filenet.api.collection.Integer32List;
import com.filenet.api.collection.LinkSet;
import com.filenet.api.collection.PageIterator;
import com.filenet.api.collection.PropertyDefinitionList;
import com.filenet.api.collection.PropertyDescriptionList;
import com.filenet.api.collection.PropertyTemplateSet;
import com.filenet.api.collection.RepositoryRowSet;
import com.filenet.api.collection.StringList;
import com.filenet.api.collection.UserSet;
import com.filenet.api.constants.AccessLevel;
import com.filenet.api.constants.AccessType;
import com.filenet.api.constants.AutoClassify;
import com.filenet.api.constants.AutoUniqueName;
import com.filenet.api.constants.Cardinality;
import com.filenet.api.constants.CheckinType;
import com.filenet.api.constants.ComponentRelationshipType;
import com.filenet.api.constants.CompoundDocumentState;
import com.filenet.api.constants.DefineSecurityParentage;
import com.filenet.api.constants.JoinComparison;
import com.filenet.api.constants.JoinOperator;
import com.filenet.api.constants.PrincipalSearchAttribute;
import com.filenet.api.constants.PrincipalSearchType;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.constants.ReservationType;
import com.filenet.api.constants.SecurityPrincipalType;
import com.filenet.api.constants.TypeID;
import com.filenet.api.constants.VersionBindType;
import com.filenet.api.core.Annotation;
import com.filenet.api.core.ComponentRelationship;
import com.filenet.api.core.Connection;
import com.filenet.api.core.Containable;
import com.filenet.api.core.ContentElement;
import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.CustomObject;
import com.filenet.api.core.Document;
import com.filenet.api.core.Factory;
import com.filenet.api.core.Folder;
import com.filenet.api.core.IndependentlyPersistableObject;
import com.filenet.api.core.Link;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.core.ReferentialContainmentRelationship;
import com.filenet.api.core.UpdatingBatch;
import com.filenet.api.core.VersionSeries;
import com.filenet.api.exception.EngineRuntimeException;
import com.filenet.api.meta.ClassDescription;
import com.filenet.api.meta.PropertyDescription;
import com.filenet.api.property.FilterElement;
import com.filenet.api.property.Properties;
import com.filenet.api.property.Property;
import com.filenet.api.property.PropertyFilter;
import com.filenet.api.query.RepositoryRow;
import com.filenet.api.query.SearchSQL;
import com.filenet.api.query.SearchScope;
import com.filenet.api.security.AccessPermission;
import com.filenet.api.security.Group;
import com.filenet.api.security.Realm;
import com.filenet.api.security.User;
import com.filenet.api.util.Id;
import com.filenet.api.util.UserContext;
import com.filenet.apiimpl.core.SubSetImpl;
import com.filenet.apiimpl.property.PropertyImpl;
import com.filenet.apiimpl.wsi.WSICredential;
import com.google.common.net.MediaType;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.constants.Constants.Mail;
import it.ibm.red.business.constants.Constants.Ricerca;
import it.ibm.red.business.constants.Constants.TipoDestinatario;
import it.ibm.red.business.constants.Constants.TipoSpedizione;
import it.ibm.red.business.constants.Constants.TipologiaInvio;
import it.ibm.red.business.constants.Constants.Varie;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.DatiCertDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DetailFatturaFepaDTO;
import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.DocumentoFascicoloDTO;
import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.MessaggioEmailDTO;
import it.ibm.red.business.dto.NotificaInteroperabilitaEmailDTO;
import it.ibm.red.business.dto.PropertyTemplateDTO;
import it.ibm.red.business.dto.SecurityDTO;
import it.ibm.red.business.dto.SecurityDTO.GruppoS;
import it.ibm.red.business.dto.SecurityDTO.UtenteS;
import it.ibm.red.business.dto.SegnaturaMessaggioInteropDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.AnnotationDTO;
import it.ibm.red.business.dto.filenet.DocumentoFascicolazioneRedFnDTO;
import it.ibm.red.business.dto.filenet.DocumentoRedFnDTO;
import it.ibm.red.business.dto.filenet.FascicoloRedFnDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.enums.CampoRicercaEnum;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.FilenetAnnotationEnum;
import it.ibm.red.business.enums.FilenetPermissionEnum;
import it.ibm.red.business.enums.FilenetStatoFascicoloEnum;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.ModalitaRicercaAvanzataTestoEnum;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.RicercaGenericaTypeEnum;
import it.ibm.red.business.enums.StatoDocCartaceoEnum;
import it.ibm.red.business.enums.StatoMailEnum;
import it.ibm.red.business.enums.StatoVerificaFirmaEnum;
import it.ibm.red.business.enums.TipoContestoProceduraleEnum;
import it.ibm.red.business.enums.TipoNotificaPecEnum;
import it.ibm.red.business.enums.TipologiaIndirizzoEmailEnum;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.enums.ValueMetadatoVerificaFirmaEnum;
import it.ibm.red.business.exception.FilenetException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.factory.DocumentoAllegabileDTOFactory;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.common.AbstractFilenetHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.TipoFile;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IVerificaFirmaSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.EmailUtils;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.PermessiUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * Classe per la gestione del CE di Filenet.
 * 
 * @author CPIERASC
 */
public class FilenetCEHelper extends AbstractFilenetHelper implements IFilenetCEHelper {

	/**
	 * Label query string.
	 */
	private static final String QUERY_STRING = "query string: ";

	/**
	 * Label stato OP.
	 */
	private static final String STATO_OP = "statoOP";

	/**
	 * Label numero OP.
	 */
	private static final String NUMERO_OP = "numeroOP";

	/**
	 * Label id fascicolo FEPA.
	 */
	private static final String ID_FASCICOLO_FEPA = "idFascicoloFEPA";

	/**
	 * Label document title.
	 */
	private static final String D_DOCUMENT_TITLE = "d.DocumentTitle";

	/**
	 * Select ALL from d.
	 */
	private static final String SELECT_D_FROM = "SELECT d.* FROM ";

	/**
	 * Order By d.
	 */
	private static final String ORDER_BY_D = " ORDER BY d.";

	/**
	 * OR Lower d.
	 */
	private static final String OR_LOWER_D = " OR LOWER(d.";

	/**
	 * Label iniziale query Like.
	 */
	private static final String LIKE_MODUL = " LIKE '%";

	/**
	 * Is Null - literal.
	 */
	private static final String IS_NULL = " IS NULL ";

	/**
	 * Is Not Null - literal.
	 */
	private static final String IS_NOT_NULL = " IS NOT NULL ";

	/**
	 * Inner join content search.
	 */
	private static final String INNER_JOIN_CONTENT_SEARCH_C_ON_D_THIS_C_QUERIED_OBJECT = " INNER JOIN ContentSearch c ON d.This = c.QueriedObject ";

	/**
	 * Inner join ComponentRelationship.
	 */
	private static final String INNER_JOIN_COMPONENT_RELATIONSHIP_R_ON_R_CHILD_COMPONENT_D_THIS = " INNER JOIN ComponentRelationship r on r.ChildComponent=[d].This ";

	/**
	 * Label descrizione.
	 */
	private static final String DESC = " DESC";

	/**
	 * Literal.
	 */
	private static final String AND_R_PARENT_COMPONENT_OBJECT = " AND r.ParentComponent = OBJECT('{";

	/**
	 * And Id Aoo - label.
	 */
	private static final String AND_ID_AOO = " AND idAoo = ";

	/**
	 * Select d.
	 */
	private static final String SELECT_D = "SELECT d.";

	/**
	 * Select ID.
	 */
	private static final String SELECT_ID = "SELECT ID";

	/**
	 * Label query da eseguire.
	 */
	private static final String QUERY_DA_ESEGUIRE = "Query da eseguire = ";

	/**
	 * Order by generico.
	 */
	private static final String ORDER_BY = " ORDER BY ";

	/**
	 * Or lower - label.
	 */
	private static final String OR_LOWER = " OR LOWER(";

	/**
	 * Lower - Label.
	 */
	private static final String LOWER = "LOWER(";

	/**
	 * Document Title - Label.
	 */
	private static final String DOCUMENT_TITLE = "DocumentTitle";

	/**
	 * Costante.
	 */
	private static final String CONSTANT_1231T23 = "1231T23";

	/**
	 * Costante.
	 */
	private static final String CONSTANT_1231T22 = "1231T22";

	/**
	 * Label fascicolo.
	 */
	private static final String VIRGOLA_FASCICOLO = ", fascicolo: ";

	/**
	 * Label finale clausula Like.
	 */
	private static final String PAR_CHIUSA_LIKE_MODUL = ") LIKE '%";

	/**
	 * Label.
	 */
	private static final String PAR_CHIUSA_UGUALE = ") = '";

	/**
	 * Clausula che imposta IsCurrentVersion a {@code true}.
	 */
	private static final String AND_IS_CURRENT_VERSION_TRUE = "' AND IsCurrentVersion = TRUE";

	/**
	 * Per documento - label.
	 */
	private static final String PER_IL_DOCUMENTO = " per il documento: ";

	/**
	 * Tutte le colonne di d.
	 */
	private static final String D_STAR = " d.* ";

	/**
	 * WHERE - Label.
	 */
	private static final String WHERE = " WHERE ";

	/**
	 * Clausula che richiede IsCurrentVersion a {@code true}.
	 */
	private static final String WHERE_IS_CURRENT_VERSION_TRUE = " WHERE IsCurrentVersion = TRUE";

	/**
	 * Where d.
	 */
	private static final String WHERE_D = " WHERE d.";

	/**
	 * Where f.
	 */
	private static final String WHERE_F = " WHERE f.";

	/**
	 * FROM - Label.
	 */
	private static final String FROM = " FROM ";

	/**
	 * AND f.
	 */
	private static final String AND_F = " AND f.";

	/**
	 * AND d.
	 */
	private static final String AND_D_TABLE = " AND d.";

	/**
	 * AND ACL Intersects.
	 */
	private static final String AND_ACL_INTERSECTS = " AND ACL INTERSECTS ";

	/**
	 * Literal.
	 */
	private static final String AND_D = " AND (d.";

	/**
	 * SELECT - Label.
	 */
	private static final String SELECT = " SELECT ";

	/**
	 * Literal.
	 */
	private static final String AND_PARENTESI_DX_AP = " AND ( ";

	/**
	 * AND - Label.
	 */
	private static final String AND = " AND ";

	/**
	 * Equal {@code true}.
	 */
	private static final String UGUALE_TRUE = " = TRUE ";

	/**
	 * <em> Equal </em> 1 AND ACL INTERSECTS.
	 */
	private static final String CONSTANT_1_AND_ACL_INTERSECTS = " = 1 AND ACL INTERSECTS ";

	/**
	 * Spaziatura e d.
	 */
	private static final String D = "       d.";

	/**
	 * SELECT * FROM - Label.
	 */
	private static final String SELECT_FROM = "SELECT * FROM ";

	/**
	 * Delimitatore del percorso della folder.
	 */
	private static final String FOLDER_PATH_DELIMITER = "/";

	/**
	 * Label.
	 */
	private static final String AUTHENTICATED_USERS = "#AUTHENTICATED-USERS";

	/**
	 * Label.
	 */
	private static final String GRANTEE_TYPE = "GranteeType";

	/**
	 * Literal allegato documento.
	 */
	private static final String ALLEGATO_DOCUMENTO_LABEL = "Allegato documento: ";

	/**
	 * Label numero op trovato. Occorre appendere il numero op trovato.
	 */
	private static final String TROVATO_NUMERO_OP_INFO = " trovato numero op: ";

	/**
	 * Label.
	 */
	private static final String PER_IL_DOCUMENTO_LITERAL = " per il documento ";

	/**
	 * Label attesa.
	 */
	private static final String IN_CORSO_LITERAL = " in corso...";

	/**
	 * Nome metodo recuper fascicoli documento.
	 */
	private static final String GET_FASCICOLI_DOCUMENTO_LITERAL = "getFascicoliDocumento(): ";

	/**
	 * Label.
	 */
	private static final String DOCUMENTO_LITERAL = " documento: ";

	/**
	 * Messaggio di errore fascicolo non presente.
	 */
	private static final String ERROR_FOLDER_FASCICOLO_ASSENTE_MSG = "Folder fascicolo non presente.";

	/**
	 * Messaggio errore folder titolario non presente.
	 */
	private static final String ERROR_FOLDER_TITOLARIO_ASSENTE_MSG = "Folder del titolario non presente.";

	/**
	 * Messaggio errore fascicolo non presente.
	 */
	private static final String ERROR_FASCICOLO_ASSENTE_MSG = "Fascicolo non presente.";

	/**
	 * Messaggio errore recupero fascicolo.
	 */
	private static final String ERROR_RECUPERO_FASCICOLO_MSG = "Errore in fase di recupero del fascicolo.";

	/**
	 * Messaggio errore creazione figli compound document.
	 */
	private static final String ERROR_CREAZIONE_FIGLI_MSG = "Errore durante la creazione dei figli del compound document";

	/**
	 * Messaggio errore recupero documento.
	 */
	private static final String ERROR_RECUPERO_DOCUMENTO_MSG = "Errore durante il recupero del documento";

	/**
	 * Messaggio errore recupero Document da EMail.
	 */
	private static final String ERROR_RECUPERO_DOCUMENT_DA_MAIL_MSG = "Errore durante il recupero del Document della email";

	/**
	 * Messaggio errore recupero fascicoli dal documento.
	 */
	private static final String ERROR_RECUPERO_FASCICOLI_MSG = "Errore durante il recupero dei fascicoli del documento: ";

	/**
	 * Messaggio errore documento non presente.
	 */
	private static final String ERROR_DOCUMENTO_ASSENTE_MSG = "Documento non presente.";

	/**
	 * Label TOP.
	 */
	private static final String TOP_LABEL = " TOP ";

	/**
	 * Literal che viene utilizzato nei messaggi di errore come possibile causa di
	 * valorizzazione.
	 */
	private static final String EQUAL_0 = " = 0) ";

	/**
	 * Literal fascicolo FEPA.
	 */
	private static final String FASCICOLO_FEPA_LITERAL = " - fascicolo FEPA: ";

	/**
	 * Dimensione massima username.
	 */
	private static final int MAX_LENGTH_USERNAME = 20;

	/**
	 * Dimensione iniziale per i gruppo AD.
	 */
	private static final int GRUPPI_AD_INITIAL_CAPACITY = 10;

	/**
	 * Dimensione iniziale per il fetch delle email.
	 */
	private static final int FETCH_EMAIL_INITIAL_CAPACITY = 5;

	/**
	 * Dimensione della massima dimensione della pagine di un content.
	 */
	private static final int PAGE_SIZE = 1500;
	
	/**
	 * Classe documento generica.
	 */
	public static final String DOCUMENT = "Document";

	/**
	 * Logger FilenetPEHelper.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FilenetCEHelper.class.getName());

	/**
	 * ObjectStore FileNet.
	 */
	private ObjectStore os;
	
	/**
	 * Provider delle properties di sistema.
	 */
	private PropertiesProvider pp;

	/**
	 * Costruttore vuoto.
	 */
	protected FilenetCEHelper() {
		// Costruttore vuoto.
	}

	/**
	 * Costruttore a partire dal DTO delle credenziali FileNet.
	 * 
	 * @param dto - DTO delle credenziali FileNet
	 */
	protected FilenetCEHelper(final FilenetCredentialsDTO dto) {
		this(dto.getUserName(), dto.getPassword(), dto.getUri(), dto.getStanzaJAAS(), dto.getObjectStore());
		initPP();
	}

	/**
	 * Costruttore a partire dalle credenziali FileNet.
	 * 
	 * @param userName
	 *            - Username per la connessione a FN.
	 * @param password
	 *            - Pwd per la connessione a FN.
	 * @param urlCE
	 *            - URL dell'infrastruttura CE di FileNet.
	 * @param stanzaJAAS
	 *            - Stanza JAAS
	 * @param objectStore
	 *            - Object store di FN.
	 */
	protected FilenetCEHelper(final String userName, final String password, final String urlCE, final String stanzaJAAS, final String objectStore) {
		final Connection con = initUserContext(userName, password, urlCE, stanzaJAAS);
		os = getObjectStore(objectStore, con);
		initPP();
	}

	/**
	 * Inizializzazione property provider.
	 */
	private void initPP() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * Chiusura della sessione utente del CE FileNet.
	 */
	@Override
	public void popSubject() {
		closeUserContext();
	}

	/**
	 * Restituisce la lista di tutte le versioni di un documento identificato
	 * dall'id e dall'id dell'AOO.
	 * 
	 * @param idDocumento
	 * @param idAoo
	 * @return lista versioni
	 */
	@SuppressWarnings("unchecked")
	public List<Document> getDocumentVersionList(final String idDocumento, final Long idAoo) {
		List<Document> output = new ArrayList<>();

		Document doc = getDocumentByIdGestionale(idDocumento, null, null, idAoo.intValue(), null, null);
		if (doc != null) {
			SubSetImpl ssi = (SubSetImpl) doc.get_VersionSeries().get_Versions();
			output = ssi.getList();
		}

		return output;
	}

	/**
	 * Dump di tutte le 'Class Definition'.
	 */
	@Override
	public void dumpAllClassDefinitions() {
		final SearchSQL searchSQL = new SearchSQL(SELECT_FROM + PropertyNames.CLASS_DEFINITION);
		final SearchScope scope = new SearchScope(getOs());
		final EngineCollection coll = scope.fetchObjects(searchSQL, null, null, false);
		final Iterator<?> it = coll.iterator();
		while (it.hasNext()) {
			final ClassDefinition def = (ClassDefinition) it.next();
			LOGGER.info(def.get_SymbolicName());
		}
	}

	/**
	 * Dump di tutte le 'Class Definition' by nome della classe.
	 * 
	 * @param className
	 *            - Nome della classe
	 */
	@Override
	public void dumpClassDefinitionProperties(final String className) {
		final ClassDefinition cls = Factory.ClassDefinition.fetchInstance(getOs(), className, null);
		final PropertyDefinitionList propLst = cls.get_PropertyDefinitions();
		final Iterator<?> propIter = propLst.iterator();
		while (propIter.hasNext()) {
			final PropertyDefinition prop = (PropertyDefinition) propIter.next();
			LOGGER.info(prop.get_SymbolicName() + "[" + prop.get_DataType() + "]");
		}
	}

	/**
	 * Dump di tutte le 'Class Definition' by nome della classe.
	 * 
	 * @param className
	 *            - Nome della classe
	 */
	@Override
	public final void dumpClassDefinitionPropertiesWithoutSystemProps(final String className) {
		final ClassDefinition cls = Factory.ClassDefinition.fetchInstance(getOs(), className, null);
		final PropertyDefinitionList propLst = cls.get_PropertyDefinitions();
		final Iterator<?> propIter = propLst.iterator();
		int i = 0;
		while (propIter.hasNext()) {
			final PropertyDefinition prop = (PropertyDefinition) propIter.next();
			if (Boolean.FALSE.equals(prop.get_IsSystemOwned())) {
				LOGGER.info(i++ + " - " + prop.get_SymbolicName() + " [" + prop.get_DataType() + "]");
			}
		}
	}

	/**
	 * Metodo per visualizzare le coppie (chiave,valore) di tutte le proprietà di un
	 * documento.
	 * 
	 * @param doc
	 *            documento da analizzare
	 * @param suppressNull
	 *            flag che se posto a true inibisce la visualizzazione delle chiavi
	 *            nulle
	 */
	public static final void dumpDocumentProperty(final Document doc, final Boolean suppressNull) {
		LOGGER.info("--------------------- DOC VALUE ---------------------");
		final Iterator<?> it = doc.getProperties().iterator();
		while (it.hasNext()) {
			final PropertyImpl property = (PropertyImpl) it.next();
			final Object obj = property.getObjectValue();
			if (Boolean.FALSE.equals(suppressNull) || obj != null) {
				LOGGER.info("KEY[" + property.getKey() + "] / VALUE [" + obj + "] / CLASS [" + property.getValue().getClass() + "]");
			}
		}
		LOGGER.info("-----------------------------------------------------");
	}

	/**
	 * Get di tutte le 'Class Definition' by nome della classe.
	 * 
	 * @param className
	 *            - Nome della classe
	 */
	@Override
	public final Set<String> getClassDefinitionPropertiesNameWithoutSystemProps(final String className) {
		final Set<String> propertyDefinitionSet = new HashSet<>();

		final ClassDefinition cls = Factory.ClassDefinition.fetchInstance(getOs(), className, null);
		final PropertyDefinitionList propLst = cls.get_PropertyDefinitions();

		final Iterator<?> propIter = propLst.iterator();
		while (propIter.hasNext()) {
			final PropertyDefinition prop = (PropertyDefinition) propIter.next();
			if (Boolean.FALSE.equals(prop.get_IsSystemOwned()) && Boolean.FALSE.equals(prop.get_IsHidden())) {
				propertyDefinitionSet.add(prop.get_SymbolicName());
			}
		}

		return propertyDefinitionSet;
	}

	/**
	 * Ritorna un documento dato document title (identificativo documento) e
	 * identificativo dell'aoo.
	 * 
	 * @param documentTitle
	 *            identificativo documento
	 * @param idAoo
	 *            identificativo aoo
	 * @return documento
	 */
	@Override
	public Document getDocumentByDTandAOO(final String documentTitle, final Long idAoo) {
		Document output = null;

		final String sql = SELECT_D_FROM + getPP().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY) + " d " + WHERE_D
				+ getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY) + " = " + idAoo + " AND d.DocumentTitle = '" + documentTitle + "' and d."
				+ PropertyNames.IS_CURRENT_VERSION + UGUALE_TRUE;

		final DocumentSet documents = (DocumentSet) new SearchScope(getOs()).fetchObjects(new SearchSQL(sql), 1, null, false);
		final Iterator<?> it = documents.iterator();

		if (it.hasNext()) {
			output = (Document) it.next();
		}

		return output;
	}
	
	/**
	 * Ritorna un documento dato document title (identificativo documento) e
	 * identificativo dell'aoo.
	 * 
	 * @param documentTitle
	 *            identificativo documento
	 * @param idAoo
	 *            identificativo aoo
	 * @return documento
	 */ 
	@Override 
	public Document getDocumentByDTandAOOForVerificaFirma(final String documentTitle, final Long idAoo,final Integer versione) {
		Document output = null;
		
		String sql = SELECT_D_FROM + getPP().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY) + " d " 
				+ WHERE_D + getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY)	+ " = " + idAoo 
				+ " AND d.DocumentTitle = '" + documentTitle + "'";
		
		if(versione!=null) {
			sql += " AND d.majorVersionNumber  = " + versione + " ";
		} else {
			LOGGER.info("getDocumentByDTandAOOForVerificaFirma : versione documento " + documentTitle + " null " );
			sql += " and d." + PropertyNames.IS_CURRENT_VERSION + UGUALE_TRUE;
		}
			
		
		DocumentSet documents = (DocumentSet) new SearchScope(getOs()).fetchObjects(new SearchSQL(sql), 1, null, false);
		Iterator<?> it = documents.iterator();
		
		if (it.hasNext()) {
			output = (Document) it.next();
		}
		
		return output;
	}

	/**
	 * Ritorna la versione del documento richiesta.
	 * 
	 * @param d
	 * @param majorVersionNumber
	 * @return la versione in input del documento
	 */
	@Override
	public Document getDocumentVersion(final Document d, final Integer majorVersionNumber) {
		Document output = null;

		final SearchSQL searchSQL = new SearchSQL(SELECT_FROM + d.getClassName() + WHERE + getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY)
				+ " = '" + d.getProperties().getStringValue(getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY)) + "'" + AND
				+ getPP().getParameterByKey(PropertiesNameEnum.MAJOR_VERSION_NUMBER_METAKEY) + " = " + majorVersionNumber);

		final SearchScope searchScope = new SearchScope(getOs());
		final DocumentSet result = (DocumentSet) searchScope.fetchObjects(searchSQL, 1, null, false);

		if (!result.isEmpty()) {
			output = (Document) result.iterator().next();
		}

		return output;
	}

	private Document getFirstVersionById(final String idDocumento, final Long idAoo) {
		Document firstVersion = null;

		final List<?> list = getDocumentVersionList(idDocumento, idAoo.intValue());
		if (list != null) {
			firstVersion = (Document) list.get(list.size() - 1);
		}

		return firstVersion;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getDocumentVersionList(java.lang.String,
	 *      java.lang.Integer).
	 */
	@Override
	public List<?> getDocumentVersionList(final String idDocumento, final Integer idAoo) {
		List<?> output = new ArrayList<>();

		final Document doc = getDocumentByIdGestionale(idDocumento, null, null, idAoo, null, null);
		if (doc != null) {
			final SubSetImpl ssi = (SubSetImpl) doc.get_VersionSeries().get_Versions();
			output = ssi.getList();
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getDocumentVersionListByGuid(java.lang.String,
	 *      java.lang.Integer).
	 */
	@Override
	public List<?> getDocumentVersionListByGuid(final String guid, final Integer idAoo) {
		final Document doc = getDocumentByGuid(guid);
		final SubSetImpl ssi = (SubSetImpl) doc.get_VersionSeries().get_Versions();

		return ssi.getList();
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getFirstVersionByGuid(java.lang.String).
	 */
	@Override
	public Document getFirstVersionByGuid(final String guid) {
		Document firstVersion = null;

		final Document doc = getDocumentByGuid(guid);
		if (doc != null) {
			final SubSetImpl ssi = (SubSetImpl) doc.get_VersionSeries().get_Versions();
			final List<?> list = ssi.getList();
			firstVersion = (Document) list.get(list.size() - 1);
		}

		return firstVersion;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getFirstVersionById(java.lang.String,
	 *      java.lang.Integer).
	 */
	@Override
	public Document getFirstVersionById(final String idDocumento, final Integer idAoo) {
		Document firstVersion = null;

		final Document doc = getDocumentByIdGestionale(idDocumento, null, null, idAoo, "", "");
		if (doc != null) {
			final SubSetImpl ssi = (SubSetImpl) doc.get_VersionSeries().get_Versions();
			final List<?> list = ssi.getList();
			firstVersion = (Document) list.get(list.size() - 1);
		}

		return firstVersion;
	}

	/**
	 * Fetch delle email di un documento.
	 * 
	 * @param idDocumento
	 *            - Id del documento
	 * @param idAOO
	 *            - Id dell'aOO
	 * @return Document FN
	 */
	@Override
	public Document fetchMailSpedizione(final String idDocumento, final Integer idAOO) {
		try {
			String query = SELECT + PropertyNames.ID + " FROM Document" + " WHERE DocumentTitle = '" + idDocumento + "' " + "AND "
					+ getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY) + " = " + idAOO;
			final RepositoryRowSet rrs = new SearchScope(getOs()).fetchRows(new SearchSQL(query), 1, null, false);

			final Iterator<?> it = rrs.iterator();
			final ArrayList<String> idList = new ArrayList<>(FETCH_EMAIL_INITIAL_CAPACITY);
			while (it.hasNext()) {
				final RepositoryRow rr = (RepositoryRow) it.next();
				idList.add(rr.getProperties().getObjectValue(PropertyNames.ID).toString());
			}
			final StringBuilder where = new StringBuilder("");
			for (final String id : idList) {
				if (!StringUtils.isNullOrEmpty(where.toString())) {
					where.append(" OR ");
				}
				where.append(" " + PropertyNames.ANNOTATED_OBJECT + " = OBJECT('" + id + "')");
			}

			query = SELECT + PropertyNames.CONTENT_ELEMENTS + " FROM Annotation " + WHERE + PropertyNames.DESCRIPTIVE_TEXT + "= 'MAIL'" + AND_PARENTESI_DX_AP + where.toString()
					+ ")";
			final AnnotationSet as = (AnnotationSet) new SearchScope(getOs()).fetchObjects(new SearchSQL(query), 1, null, false);

			String mailGuid = null;
			final Iterator<?> it2 = as.iterator();
			while (it2.hasNext()) {
				final Annotation a = (Annotation) it2.next();
				mailGuid = IOUtils.toString(a.accessContentStream(0));
			}
			if (mailGuid == null) {
				return null;
			}

			return getDocumentByGuid(mailGuid);
		} catch (final IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}
	}

	/**
	 * Recupera l'identificativo di una casella di posta dal nome.
	 * 
	 * @param nomeCasella
	 *            - Nome della casella.
	 * @return - Identificativo della casella.
	 */
	@Override
	public Integer getTipoCasellaPostaleFromNome(final String nomeCasella) {
		Integer tipoCasellaPostale = null;

		try {
			final FolderSet myFolder = getCasellePostaliAsFolder(nomeCasella);

			final Iterator<?> it = myFolder.iterator();
			while (it.hasNext()) {
				final Folder f = getFolder(((Folder) it.next()).get_Id());

				tipoCasellaPostale = f.getProperties().getInteger32Value(getPP().getParameterByKey(PropertiesNameEnum.TIPOLOGIA_CASELLA_POSTA_METAKEY));
			}
		} catch (final Exception e) {
			throw new FilenetException("Si è verificato un errore durante il recupero del tipo di casella postale", e);
		}

		return tipoCasellaPostale;
	}

	/**
	 * Restituisce una folder.
	 *
	 * @author AndreaP
	 * @param id
	 *            identificativo folder
	 * @return oggetto folder
	 */
	@Override
	public Folder getFolder(final Id id) {
		return Factory.Folder.fetchInstance(getOs(), id, null);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getFolder(java.lang.String).
	 */
	@Override
	public Folder getFolder(final String folder) {
		return Factory.Folder.fetchInstance(getOs(), folder.startsWith(FOLDER_PATH_DELIMITER) ? folder : FOLDER_PATH_DELIMITER + folder, null);
	}

	/**
	 * Restituisce un document.
	 *
	 * @author AndreaP
	 * @param id
	 *            Identificativo documento
	 * @return documento
	 */
	@Override
	public Document getDocument(final Id id) {
		return Factory.Document.fetchInstance(getOs(), id, null);
	}

	/**
	 * Recupera il Set di folder delle casella di posta by nome della casella.
	 * 
	 * @param nomeCasella
	 *            - Nome della casella di posta.
	 * @return Set delle folder FN.
	 */
	private FolderSet getCasellePostaliAsFolder(final String nomeCasella) {
		try {
			final StringBuilder queryString = new StringBuilder();
			queryString.append("SELECT f." + PropertyNames.ID + FROM + getPP().getParameterByKey(PropertiesNameEnum.MAILBOX_CLASSNAME_FN_METAKEY) + " f");
			queryString.append(
					WHERE_F + PropertyNames.PARENT + " = OBJECT('/" + getPP().getParameterByKey(PropertiesNameEnum.FOLDER_ELETTRONICI_FN_METAKEY).split("\\|")[1] + "')");
			if (nomeCasella != null && !"".equals(nomeCasella.trim())) {
				queryString.append(AND_F + PropertyNames.FOLDER_NAME + " = '" + nomeCasella + "'");
			}
			final SearchSQL searchSQL = new SearchSQL();
			searchSQL.setQueryString(queryString.toString());
			final SearchScope scope = new SearchScope(getOs());
			return (FolderSet) scope.fetchObjects(searchSQL, null, null, false);
		} catch (final Exception e) {
			throw new FilenetException(" Si è verificato un errore durante il recupero del folder.", e);
		}
	}

	/**
	 * Metodo lista caselle di posta.
	 * 
	 * @return lista caselle postali
	 */
	@Override
	public List<CasellaPostaDTO> getCasellePostaliDTO() {
		return getCasellePostaliDTO(null);
	}

	private List<CasellaPostaDTO> getCasellePostaliDTO(final String nomeCasella) {
		final List<CasellaPostaDTO> allCaselle = new ArrayList<>();

		final FolderSet myFolder = getCasellePostaliAsFolder(nomeCasella);

		LOGGER.info("####INFO#### getCasellePostaliAsFolder FINE");

		final Iterator<?> it = myFolder.iterator();
		while (it.hasNext()) {
			LOGGER.info("####INFO#### getFolder INIZIO");
			final Folder f = getFolder(((Folder) it.next()).get_Id());
			final CasellaPostaDTO cpSingola = transformFolderToCasellaPostaDTO(f);
			LOGGER.info("####INFO#### transformFolderToCasellaPostaDTO FINE");

			allCaselle.add(cpSingola);
		}

		return allCaselle;
	}

	/**
	 * Metodo che restituisce la casella postale il cui nome è in input.
	 * 
	 * @param nomeCasella
	 * @return CasellaPostaDTO
	 */
	@Override
	public CasellaPostaDTO getCasellaPostaleDTO(final String nomeCasella) {
		final List<CasellaPostaDTO> caselle = getCasellePostaliDTO(nomeCasella);

		if (!caselle.isEmpty()) {
			return caselle.get(0);
		}

		return null;
	}

	/**
	 * Metodo per la trasformazione da folder in DTO CasellaPosta.
	 *
	 * @param f the f
	 * @return cp
	 */
	private CasellaPostaDTO transformFolderToCasellaPostaDTO(final Folder f) {
		final Properties pFolder = f.getProperties();
		final CasellaPostaDTO cp = new CasellaPostaDTO();

		cp.setHostInvio(pFolder.getStringValue(getPP().getParameterByKey(PropertiesNameEnum.MAILBOX_HOSTINVIO_MAIL_METAKEY)));
		cp.setHostRicezione(pFolder.getStringValue(getPP().getParameterByKey(PropertiesNameEnum.MAILBOX_HOSTRICEZIONE_METAKEY)));
		cp.setIndirizzoEmail(pFolder.getStringValue(getPP().getParameterByKey(PropertiesNameEnum.MAILBOX_INDIRIZZO_FN_METAKEY)));
		cp.setPasswordInvio(pFolder.getStringValue(getPP().getParameterByKey(PropertiesNameEnum.MAILBOX_PASSWORDINVIO_METAKEY)));
		cp.setPasswordRicezione(pFolder.getStringValue(getPP().getParameterByKey(PropertiesNameEnum.MAILBOX_PASSWORDRICEZIONE_METAKEY)));
		cp.setPortInvio(pFolder.getStringValue(getPP().getParameterByKey(PropertiesNameEnum.MAILBOX_PORTINVIO_METAKEY)));
		cp.setPortRicezione(pFolder.getStringValue(getPP().getParameterByKey(PropertiesNameEnum.MAILBOX_PORTRICEZIONE_METAKEY)));
		cp.setProxyHost(pFolder.getStringValue(getPP().getParameterByKey(PropertiesNameEnum.MAILBOX_PROXYHOST_METAKEY)));
		cp.setProxyPort(pFolder.getStringValue(getPP().getParameterByKey(PropertiesNameEnum.MAILBOX_PROXYPORT_METAKEY)));

		final Integer tipologia = pFolder.getInteger32Value(getPP().getParameterByKey(PropertiesNameEnum.TIPOLOGIA_CASELLA_POSTA_METAKEY));
		if (tipologia != null) {
			cp.setTipologia(tipologia);
		}

		cp.setUsernameInvio(pFolder.getStringValue(getPP().getParameterByKey(PropertiesNameEnum.MAILBOX_USERNAMEINVIO_METAKEY)));
		cp.setUsernameRicezione(pFolder.getStringValue(getPP().getParameterByKey(PropertiesNameEnum.MAILBOX_USERNAMERICEZIONE_METAKEY)));
		cp.setServerInType(pFolder.getStringValue(getPP().getParameterByKey(PropertiesNameEnum.MAILBOX_SERVERINTYPE_METAKEY)));
		cp.setGestioneWhiteList(pFolder.getStringValue(getPP().getParameterByKey(PropertiesNameEnum.MAILBOX_GESTIONEWHITELIST_METAKEY)));

		if (pFolder.getObjectValue(getPP().getParameterByKey(PropertiesNameEnum.MAILBOX_SERVEROUTAUTENTICAZIONE_METAKEY)) != null) {
			cp.setServerOutAutenticazione(pFolder.getInteger32Value(getPP().getParameterByKey(PropertiesNameEnum.MAILBOX_SERVEROUTAUTENTICAZIONE_METAKEY)));
		}

		boolean abilitaTLS12 = false;
		if (pFolder.getObjectValue(getPP().getParameterByKey(PropertiesNameEnum.MAILBOX_TLS12_METAKEY)) != null) {
			abilitaTLS12 = pFolder.getBooleanValue(getPP().getParameterByKey(PropertiesNameEnum.MAILBOX_TLS12_METAKEY));
		}
		cp.setAbilitaTLS12(abilitaTLS12);

		final Boolean mantieniAllegatiOriginali = pFolder.getBooleanValue(getPP().getParameterByKey(PropertiesNameEnum.MAILBOX_MANTIENI_ALLEGATI_MAIL_ORIGINALI_METAKEY));
		cp.setMantieniAllegatiOriginali(Boolean.TRUE.equals(mantieniAllegatiOriginali));

		boolean flagProtAuto = false;
		if (pFolder.getObjectValue(getPP().getParameterByKey(PropertiesNameEnum.MAILBOX_FLAG_PROT_AUTO_METAKEY)) != null) {
			flagProtAuto = pFolder.getBooleanValue(getPP().getParameterByKey(PropertiesNameEnum.MAILBOX_FLAG_PROT_AUTO_METAKEY));
		}
		cp.setFlagProtocollazioneAutomatica(flagProtAuto);

		return cp;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getMailsAsDocument(java.lang.String,
	 *      java.lang.String, java.lang.String, java.util.List, java.lang.String).
	 */
	@Override
	public DocumentSet getMailsAsDocument(final String path, final String documentClass, final String filter, final List<StatoMailEnum> selectedStatList,
			final String orderByClause) {
		final String searchFields = "d.Id, d.ComponentBindingLabel, d.ChildDocuments" + ", d." + getPP().getParameterByKey(PropertiesNameEnum.DESTINATARI_EMAIL_METAKEY)
				+ ", \"" + getPP().getParameterByKey(PropertiesNameEnum.FROM_MAIL_METAKEY) + "\"" + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_EMAIL_METAKEY) + ", d." + getPP().getParameterByKey(PropertiesNameEnum.NOTA_EMAIL_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.DESTINATARI_CC_LONG) + ", d." + getPP().getParameterByKey(PropertiesNameEnum.MESSAGE_ID) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY) + ", d." + getPP().getParameterByKey(PropertiesNameEnum.STATO_MAIL_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ID_UTENTE_PROTOCOLLATORE_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.DATA_RICEZIONE_MAIL_METAKEY) + ", d." + getPP().getParameterByKey(PropertiesNameEnum.DATA_INVIO_MAIL_METAKEY)
				+ ", d." + getPP().getParameterByKey(PropertiesNameEnum.DATA_CAMBIO_STATO_EMAIL_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.METADATO_STATO_PRE_ARCHIVIAZIONE) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.MOTIVO_ELIMINAZIONE_EMAIL_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.PREASSEGNATARIO_SEGN_INTEROP_MAIL_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.MAIL_MITTENTE_SEGN_INTEROP_MAIL_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.WARNINGS_VALIDAZIONE_SEGN_INTEROP_MAIL_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ERRORI_VALIDAZIONE_SEGN_INTEROP_MAIL_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ID_DOC_PRINCIPALE_SEGN_INTEROP_MAIL_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.IDS_ALLEGATI_SEGN_INTEROP_MAIL_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.VALIDAZIONE_SEGN_INTEROP_MAIL_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.PROTOCOLLI_GENERATI_MAIL_METAKEY) + ", d."
//				+ pp.getParameterByKey(PropertiesNameEnum.PREASSEGNATARIO_CONOSCENZA_INTEROP_MAIL_METAKEY ) + ", d."
//				+ pp.getParameterByKey(PropertiesNameEnum.AUTORE_ULTIMA_MODIFICA_NOTA_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY) + ", d." + getPP().getParameterByKey(PropertiesNameEnum.DATA_SCARICO_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.OPERAZIONE_POSTA_ELETTRONICA_FN_METAKEY)
				+ ", d." +getPP().getParameterByKey(PropertiesNameEnum.PROTOCOLLA_E_MANTIENI_METAKEY);
		return getMailsAsDocument(path, documentClass, filter, searchFields, false, selectedStatList, orderByClause);
	}

	/**
	 * Query per la restituzione di tutte le mails.
	 * 
	 * @param path
	 *            identificativo document
	 * @param documentClass
	 *            classe documentale
	 * @param filter
	 *            folder mail
	 * @return insieme delle mail ricercate
	 */
	@Override
	public DocumentSet getMailsAsDocument(final String path, final String documentClass, final String filter, final List<StatoMailEnum> selectedStatList) {
		return getMailsAsDocument(path, documentClass, filter, selectedStatList, null);
	}

	/**
	 * Query per la restituzione di tutte le mails.
	 * 
	 * @param path
	 *            identificativo document
	 * @param documentClass
	 *            classe documentale
	 * @param filter
	 *            folder mail
	 * @return insieme delle mail ricercate
	 */
	private DocumentSet getMailsAsDocument(final String path, final String documentClass, final String filter, final String searchFields, final boolean onlyFirst,
			final List<StatoMailEnum> selectedStatList, final String orderByClause) {

		try {
			final StringBuilder queryString = new StringBuilder();

			String projection = "d.*";
			if (searchFields != null) {
				projection = searchFields;
			}

			queryString.append(FilenetCERicercaMailUtils.generateBody(onlyFirst, projection, path, documentClass, "d"));
			if (filter != null) {
				queryString.append(filter);
			}

			if (selectedStatList != null && StringUtils.isNullOrEmpty(orderByClause)) {

				if (selectedStatList.contains(StatoMailEnum.RIFIUTATA) || selectedStatList.contains(StatoMailEnum.INOLTRATA)) {
					queryString.append(ORDER_BY + getPP().getParameterByKey(PropertiesNameEnum.DATA_INVIO_MAIL_METAKEY) + DESC);
				} else if (selectedStatList.contains(StatoMailEnum.ELIMINATA) || selectedStatList.contains(StatoMailEnum.INARRIVO)
						|| selectedStatList.contains(StatoMailEnum.INARRIVO_NON_PROTOCOLLABILE_AUTOMATICAMENTE)) {
					queryString.append(ORDER_BY + getPP().getParameterByKey(PropertiesNameEnum.DATA_RICEZIONE_MAIL_METAKEY) + DESC);
				}
			} else if (!StringUtils.isNullOrEmpty(orderByClause)) {
				queryString.append(orderByClause);
			}

			final SearchSQL searchSQL = new SearchSQL();
			searchSQL.setQueryString(queryString.toString());
			final SearchScope scope = new SearchScope(getOs());

			return (DocumentSet) scope.fetchObjects(searchSQL, null, null, false);
		} catch (final Exception e) {
			throw new FilenetException("Si è verificato un errore durante il recupero della mail", e);
		}
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getMailsAsDocument(java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.String, boolean,
	 *      java.util.List).
	 */
	@Override
	public DocumentSet getMailsAsDocument(final String path, final String documentClass, final String filter, final String searchFields, final boolean onlyFirst,
			final List<StatoMailEnum> selectedStatList) {
		return getMailsAsDocument(path, documentClass, filter, searchFields, onlyFirst, selectedStatList, null);
	}

	// Esempio System.out.println(count(os, "t", "Email t INNER JOIN
	// ReferentialContainmentRelationship r ON t.This = r.Head", "r.Tail =
	// OBJECT('/CasellePostali/testred2@pec.mef.gov.it/Inbox') AND stato = 1"))
	private Integer count(final String alias, final String className, final String where) {
		final Integer limit = 500; // Non potremmo contare più di 1000 elementi
		Integer output = null;

		try {
			final SearchScope scope = new SearchScope(getOs());
			final SearchSQL searchSQL = new SearchSQL();
			searchSQL.setQueryString(SELECT + alias + ".ID FROM " + className + WHERE + where + " OPTIONS (COUNT_LIMIT " + limit + ")");
			final DocumentSet myDoc = (DocumentSet) scope.fetchObjects(searchSQL, 1, null, true);
			final PageIterator pi = myDoc.pageIterator();
			output = pi.getTotalCount();

		} catch (final Exception e) {
			throw new FilenetException(" Si è verificato un errore durante la count Generica.", e);
		}

		if (output == null) {
			output = 0;
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#countMailFolder(java.lang.String,
	 *      java.lang.String, java.util.List).
	 */
	@Override
	public Integer countMailFolder(final String path, final String documentClass, final List<String> statoMail) {
		Integer count = null;
		try {
			final StringBuilder className = new StringBuilder();
			final StringBuilder where = new StringBuilder();
			final StringBuilder inCondition = new StringBuilder();
			final String alias = "t";

			// predisposizione 'IN' condition
			inCondition.append("(");
			for (final String sm : statoMail) {
				if ("(".equals(inCondition.toString())) {
					inCondition.append(sm);
				} else {
					inCondition.append(",").append(sm);
				}
			}
			inCondition.append(")");

			className.append(documentClass + " t INNER JOIN ReferentialContainmentRelationship r ON " + alias + ".This = r.Head");
			where.append("r.Tail = OBJECT('" + path + "') AND stato IN " + inCondition);

			count = count(alias, className.toString(), where.toString());
		} catch (final Exception e) {
			throw new FilenetException(" Si è verificato un errore durante la count delle mail.", e);
		}

		return count;
	}

	/**
	 * Ritorna la count degli allegati da firmare dato DocumentTitle e Document
	 * class.
	 * 
	 * @param documentClass
	 *            - Document class del documento
	 * @param documentTitle
	 *            - Document title del documento
	 * @return Count
	 */
	@Override
	public int getCountAllegatiToSign(final String documentClass, final String documentTitle) {
		final String select = getSelectCommonPropsDocument();
		final Collection<String> documentTitles = new ArrayList<>();
		documentTitles.add(documentTitle);
		final DocumentSet ds = getDocumentBase(select, documentClass, documentTitles);
		return getCountAllegatiToSign(StringUtils.cleanGuidToString(getFirstDocument(ds).get_Id()));
	}

	/**
	 * Recupera Document contributo.
	 * 
	 * @param documentTitle
	 *            document title
	 * @return document
	 */
	@Override
	public Document getDocumentForContributo(final String documentTitle) {
		final String selectDetail = getSelectCommonPropsDocument() + ", d." + getPP().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY);

		final Collection<String> ids = new ArrayList<>();
		ids.add(documentTitle);
		final DocumentSet ds = getDocumentBase(selectDetail, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.CONTRIBUTO_CLASSNAME_FN_METAKEY), ids);

		return getFirstDocument(ds);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getFileInfoAndContentByDocumentTitle(java.lang.String,
	 *      java.lang.Long).
	 */
	@Override
	public Document getFileInfoAndContentByDocumentTitle(final String documentTitle, final Long idAOO) {
		final String select = " d." + PropertyNames.ID + "," + " d." + PropertyNames.MIME_TYPE + "," + " d." + PropertyNames.DATE_CREATED + "," + " d."
				+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY) + "," + " d." + PropertyNames.CONTENT_ELEMENTS + " ";

		final Collection<String> ids = new ArrayList<>();
		ids.add(documentTitle);

		final DocumentSet ds = getDocumentBase(select, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), ids);

		return getFirstDocument(ds);
	}

	/**
	 * Recupera Document per storico visuale.
	 * 
	 * @param documentTitle
	 *            document title
	 * @return document
	 */
	@Override
	public Document getDocumentForStoricoVisuale(final String documentTitle) {
		final String selectDetail = getSelectCommonPropsDocument() + ", d." + getPP().getParameterByKey(PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.MOMENTO_PROTOCOLLAZIONE_ID_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ITER_APPROVATIVO_SEMAFORO_METAKEY);

		final Collection<String> ids = new ArrayList<>();
		ids.add(documentTitle);
		final DocumentSet ds = getDocumentBase(selectDetail, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), ids);

		return getFirstDocument(ds);
	}

	/**
	 * Ritorna il Document FN dato DocumentTitle e Document class.
	 * 
	 * @param documentClass
	 *            - Document class del documento
	 * @param documentTitle
	 *            - Document title del documento
	 * @param idTipoProtocollo
	 *            - Tipo del protocollo(PMEF,NPS)
	 * @return Document FN.
	 */
	@Override
	public Document getDocumentForDetail(final String documentClass, final String documentTitle) {
		final String selectDetail = getSelectCommonPropsDocument() + ", d." + getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY) + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY)
				+ "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY) + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY)
				+ "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY) + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY) + ","
				+ " d." + getPP().getParameterByKey(PropertiesNameEnum.TRASFORMAZIONE_PDF_ERRORE_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ITER_APPROVATIVO_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.UTENTE_CREATORE_ID_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.MOMENTO_PROTOCOLLAZIONE_ID_METAKEY) + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY)
				+ "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.FLAG_FIRMA_AUTOGRAFA_RM_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.DESCRIZIONE_TIPOLOGIA_DOCUMENTO_NPS_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_PROTOCOLLO_NPS_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_EMERGENZA_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_EMERGENZA_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_EMERGENZA_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.FIRMA_PDF_METAKEY) + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY) + ","
				+ " d." + getPP().getParameterByKey(PropertiesNameEnum.SOTTOCATEGORIA_DOCUMENTO_USCITA);

		final Collection<String> ids = new ArrayList<>();
		ids.add(documentTitle);
		final DocumentSet ds = getDocumentBase(selectDetail, documentClass, ids);

		return getFirstDocument(ds);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getDocumentRedForDetail(java.lang.String,
	 *      java.lang.String).
	 */
	@Override
	public Document getDocumentRedForDetail(final String documentClass, final String documentTitle) {
		final Collection<String> ids = new ArrayList<>();
		ids.add(documentTitle);
		final String selectDetail = D_STAR; // mi servono tutti perché devo distinguere i metadati dinamici
		final DocumentSet ds = getDocumentBase(selectDetail, documentClass, ids);

		return getFirstDocument(ds);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getDocumentRedForDetailLIGHT(java.lang.String,
	 *      java.lang.String).
	 */
	@Override
	public Document getDocumentRedForDetailLIGHT(final String documentClass, final String documentTitle) {
		final String selectDetail = " d." + PropertyNames.ID + ", " + " d.annoDocumento, " //
				+ " d.annoProtocollo, " //
				+ " d.numeroProtocollo, " //
				+ " d.idProtocollo, " + " d.tipoProtocollo, " + " d.numeroDocumento, " //
				+ " d.dataProtocollo, " //
				+ " d.idTipologiaDocumento, " //
				+ " d.idTipologiaProcedimento, " //
				+ " d.DocumentTitle, " //
				+ " d.nomeFile, " //
				+ " d.idAOO, " //
				+ " d.oggetto, " //
				+ " d.Owner, " 
				+ " d.idUfficioCreatore, " 
				+ " d.idUtenteCreatore, " 
				+ " d." + PropertyNames.DATE_CREATED
				+ ", " + " d." + PropertyNames.MIME_TYPE
				+ ", " + " d." + PropertyNames.CONTENT_SIZE
				+ ", " + " d." + getPP().getParameterByKey(PropertiesNameEnum.DESCRIZIONE_TIPOLOGIA_DOCUMENTO_NPS_METAKEY) + " "
				+ ", " + " d." + getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_PROTOCOLLO_NPS_METAKEY) + " "
				+ ", " + " d." + getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_EMERGENZA_METAKEY) + " "
				+ ", " + " d." + getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_EMERGENZA_METAKEY) + " "
				+ ", " + " d." + getPP().getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_EMERGENZA_METAKEY) + " "
				+ ", " + " d." + pp.getParameterByKey(PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY) + " "
				;
		
		Collection<String> ids = new ArrayList<>();
		ids.add(documentTitle);
		final DocumentSet ds = getDocumentBase(selectDetail, documentClass, ids);

		return getFirstDocument(ds);
	}

	/**
	 * Ritorna il Document FN di un allegato dato DocumentTitle e Document class.
	 * 
	 * @param documentClass
	 *            	- Document class del documento
	 * @param documentTitle
	 *            	- Document title del documento
	 * @return Document FN dell'allegato.
	 */
	@Override
	public Document getAllegatoForDownload(final String documentClass, final String documentTitle) {
		String selectDetail = new StringBuilder(getSelectCommonPropsDocument()) 
				.append(",").append(" d.").append(PropertyNames.CONTENT_ELEMENTS)
				.append(",").append(" d.").append(PropertyNames.ANNOTATIONS)
				.append(",").append(" d.").append(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY))
				.append(",").append(" d.").append(pp.getParameterByKey(PropertiesNameEnum.DA_FIRMARE_METAKEY))
				.append(",").append(" d.").append(pp.getParameterByKey(PropertiesNameEnum.FORMATO_ALLEGATO_ID_METAKEY))
				.append(",").append(" d.").append(pp.getParameterByKey(PropertiesNameEnum.IS_FORMATO_ORIGINALE_METAKEY))
				.append(",").append(" d.").append(pp.getParameterByKey(PropertiesNameEnum.FIRMA_VISIBILE_METAKEY))
				.append(",").append(" d.").append(pp.getParameterByKey(PropertiesNameEnum.STAMPIGLIATURA_SIGLA_ALLEGATO))
				.append(",").append(" d.").append(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY))
				.toString();
		
		Collection<String> ids = new ArrayList<>();
		ids.add(documentTitle);

		final DocumentSet ds = getDocumentBase(selectDetail, documentClass, ids);

		return getFirstDocument(ds);
	}

	/**
	 * Ritorna il content Document FN dato DocumentTitle e Document Class.
	 * 
	 * @param documentClass
	 *            - Document class del documento
	 * @param documentTitle
	 *            - Document title del documento
	 * @return Document FN del content.
	 */
	@Override
	public Document getDocumentForDownload(final String documentClass, final String documentTitle) {
		return getDocumentForDownload(documentClass, documentTitle, true);
	}

	/**
	 * getDocumentForDownloadAllegato.
	 * 
	 * @param documentClass
	 * @param documentTitle
	 * @param nomeFile
	 * @return
	 */
	@Override
	public Document getDocumentForDownload(final String documentClass, final String documentTitle, final boolean nomeFile) {
		final StringBuilder selectDetail = new StringBuilder(getSelectCommonPropsDocument()).append(",").append(" d.").append(PropertyNames.CONTENT_ELEMENTS).append(",")
				.append(" d.").append(PropertyNames.ANNOTATIONS).append(",").append(" d.").append(PropertyNames.VERSION_SERIES)
				// .append(",").append("
				// d.").append(getPP().getParameterByKey(PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY))
				.append(nomeFile ? ", d." + getPP().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY) : Constants.EMPTY_STRING);

		final Collection<String> documentTitles = new ArrayList<>();
		documentTitles.add(documentTitle);
		final DocumentSet ds = getDocumentBase(selectDetail.toString(), documentClass, documentTitles);

		return getFirstDocument(ds);
	}
	
	/**
	 * getDocumentForDownloadAllegato.
	 * 
	 * @param guid
	 * @param nomeFile
	 * @return
	 */
	@Override 
	public Document getDocumentForDownloadAllegato(final String guid, final boolean nomeFile) {
		StringBuilder selectDetail = new StringBuilder(getSelectCommonPropsDocument())
				.append(",").append(" d.").append(PropertyNames.CONTENT_ELEMENTS)
				.append(",").append(" d.").append(PropertyNames.ANNOTATIONS)
				.append(",").append(" d.").append(PropertyNames.VERSION_SERIES)
				.append(nomeFile ? ", d." + getPP().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY) : Constants.EMPTY_STRING);
	 
		final DocumentSet ds = getParentDocumentAllegato(selectDetail.toString(), guid);
		
		return getFirstDocument(ds);
	}
	
	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getDocumentForAllegato(java.lang.String,
	 *      boolean).
	 */
	@Override
	public Document getDocumentForAllegato(final String guid, final boolean nomeFile, final boolean selectAll) {
		StringBuilder selectDetail = null;
		if(selectAll) {
			selectDetail = new StringBuilder("d.*");
		} else {
			selectDetail = new StringBuilder(getSelectCommonPropsDocument())
				.append(",d." + getPP().getParameterByKey(PropertiesNameEnum.ITER_APPROVATIVO_METAKEY))
				.append(",d." + getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY))
				.append(",d." + getPP().getParameterByKey(PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY));
		}
		
		final DocumentSet ds = getParentDocumentAllegato(selectDetail.toString(), guid);
		return getFirstDocument(ds);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getDocumentForAllegato(java.lang.String, boolean).
	 */
	@Override 
	public Document getDocumentForAllegato( final String guid, final boolean nomeFile) {
		return getDocumentForAllegato(guid, nomeFile, false);
	}

	/**
	 * Ritorna la stringa per la 'select' FN degli attributi comuni a tutte le
	 * select sui Document FN.
	 * 
	 * @return Stringa per la 'select'
	 */
	private static String getSelectCommonPropsDocument() {
		return " d." + PropertyNames.ID + "," + " d." + PropertyNames.IS_RESERVED + "," + " d." + PropertyNames.RESERVATION + "," + " d." + PropertyNames.MIME_TYPE + ","
				+ " d." + PropertyNames.COMPOUND_DOCUMENT_STATE + " ";
	}

	/**
	 * Recupera il Document FN per le operazione post Rifiuto.
	 * 
	 * @param documentClass
	 *            - Document class del documento.
	 * @param documentTitle
	 *            - Document title del documento.
	 * @return Document FN
	 */
	@Override
	public Document getDocumentForRifiuto(final String documentClass, final String documentTitle) {
		final String selectDetail = "*";
		final Collection<String> ids = new ArrayList<>();
		ids.add(documentTitle);
		final DocumentSet ds = getDocumentBase(selectDetail, documentClass, ids);
		return getFirstDocument(ds);
	}

	/**
	 * Ritorno il primo Document FN trovato dato un DocumentSet.
	 * 
	 * @param ds
	 *            - Document Set
	 * @return Il primo Document
	 */
	private static Document getFirstDocument(final DocumentSet ds) {
		Document output = null;
		final Iterator<?> it = ds.iterator();
		if (it != null && it.hasNext()) {
			output = (Document) ds.iterator().next();
		} else {
			throw new RedException("Nessun documento presente.");
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getDocumentForFilteredMasters(java.util.Collection,
	 *      java.util.List, java.util.List, java.util.List, java.util.List,
	 *      java.util.List, java.lang.Long, java.lang.String).
	 */
	@Override
	public DocumentSet getDocumentForFilteredMasters(final Collection<String> documentTitles, final List<String> allTokensFiltro, final List<String> numericTokens,
			final List<String> numeroProtocolloTokens, final List<String> annoProtocolloTokens, final List<Long> idsTipologiaDocumento, final Long idAoo,
			final String documentClass) {
		final String select = new StringBuilder(baseSelect()).append(",").append(" d.").append(getPP().getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY))
				.append(",").append(" d.").append(getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY)).append(",").append(" d.")
				.append(getPP().getParameterByKey(PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY)).append(",").append(" d.")
				.append(getPP().getParameterByKey(PropertiesNameEnum.ID_FORMATO_DOCUMENTO)).append(",").append(" d.")
				.append(getPP().getParameterByKey(PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY)).toString();

		final StringBuilder sql = new StringBuilder(SELECT).append(select).append(FROM).append(documentClass).append(" d").append(WHERE)
				.append("d." + PropertyNames.IS_CURRENT_VERSION).append(UGUALE_TRUE).append(AND).append("d." + getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY))
				.append(" = ").append(idAoo).append(AND).append("(")
				.append(StringUtils.createInCondition("d." + getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), documentTitles.iterator(), true))
				.append(")").append(AND_PARENTESI_DX_AP).append("( ")
				.append(StringUtils.createLikeLowerCaseCondition("d." + getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY), allTokensFiltro.iterator()))
				.append(")");

		// Si aggiungono le eventuali condizioni per i metadati Numero Protocollo e Anno
		// Protocollo
		// Se le liste contenenti Numero Protocollo e Anno Protocollo sono valorizzate,
		// si inserisce una condizione "AND" specifica
		if (!CollectionUtils.isEmpty(numeroProtocolloTokens) && !CollectionUtils.isEmpty(annoProtocolloTokens)) {
			sql.append(" OR ( ");

			for (int index = 0; index < numeroProtocolloTokens.size() && index < annoProtocolloTokens.size(); index++) {
				sql.append("(").append("d." + getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY)).append(" = ").append(numeroProtocolloTokens.get(index))
						.append(AND) // Numero Protocollo =
											// ... AND Anno
											// Protocollo = ...
						.append("d." + getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY)).append(" = ").append(annoProtocolloTokens.get(index))
						.append(")");

				if (index < (numeroProtocolloTokens.size() - 1) && index < (annoProtocolloTokens.size() - 1)) {
					sql.append(" OR ");
				}
			}

			sql.append(" ) ");
		} else if (!CollectionUtils.isEmpty(numericTokens)) {
			sql.append(" OR ").append("( ")
					.append(StringUtils.createInCondition("d." + getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY), numericTokens.iterator(), false))
					.append(")").append(" OR ") // Numero Protocollo IN (...) OR Anno Protocollo IN (...)
					.append("( ")
					.append(StringUtils.createInCondition("d." + getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY), numericTokens.iterator(), false))
					.append(")");
		}

		// Si aggiunge l'eventuale condizione per il metadato Numero Documento
		if (!CollectionUtils.isEmpty(numericTokens)) {
			sql.append(" OR ").append("( ")
					.append(StringUtils.createInCondition("d." + getPP().getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY), numericTokens.iterator(), false))
					.append(")");
		}

		// Si aggiunge l'eventuale condizione sul metadato Tipologia Documento
		if (!CollectionUtils.isEmpty(idsTipologiaDocumento)) {
			sql.append(" OR ").append("(").append(StringUtils.createInCondition("d." + getPP().getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY),
					idsTipologiaDocumento.iterator(), false)).append(")");
		}

		// Si aggiunge la condizione di ORDER BY
		sql.append(" )").append(ORDER_BY).append("d." + getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY)).append(DESC).toString();

		final String sqlString = sql.toString();
		LOGGER.info(sqlString);
		final SearchSQL searchSQL = new SearchSQL(sqlString);
		final SearchScope scope = new SearchScope(getOs());

		return (DocumentSet) scope.fetchObjects(searchSQL, PAGE_SIZE, null, true);
	}

	/**
	 * Ricerca i Document FN per la visualizzazione del master.
	 * 
	 * @param documentClass
	 *            - Document class dei documenti da ricercare
	 * @param documentTitles
	 *            - Collection dei Document Title dei documenti da ricercare.
	 * @return Document Set dei documenti ricercati.
	 */
	@Override
	public DocumentSet getDocumentsForMasters(final String documentClass, final Collection<String> documentTitles) {
		final String selectMasters = baseSelect() + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY) + "," + 
				" d." + getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY) + "," + 
				" d." + getPP().getParameterByKey(PropertiesNameEnum.ID_FORMATO_DOCUMENTO)  + "," +
				" d." + getPP().getParameterByKey(PropertiesNameEnum.VERIFICA_FIRMA_DOC_ALLEGATO)  + "," +
				" d." + getPP().getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_VALIDITA_FIRMA)  + "," +
				" d." + getPP().getParameterByKey(PropertiesNameEnum.MAJOR_VERSION_NUMBER_METAKEY)  + "," +
				" d." + getPP().getParameterByKey(PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY) + " ";
		return getDocumentBase(selectMasters, documentClass, documentTitles, true);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getPageIteratorForMasters(java.lang.String,
	 *      java.util.Collection, java.lang.Integer).
	 */
	@Override
	public PageIterator getPageIteratorForMasters(final String documentClass, final Collection<String> documentTitles, final Integer pageSize) {
		final String selectMasters = baseSelect() + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY) + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.ID_FORMATO_DOCUMENTO) + ","
				+ " d." + getPP().getParameterByKey(PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.VERIFICA_FIRMA_DOC_ALLEGATO) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.DECRETO_LIQUIDAZIONE_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_VALIDITA_FIRMA) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.MAJOR_VERSION_NUMBER_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY);

		final StringBuilder sql = new StringBuilder();
		sql.append(SELECT + selectMasters);
		sql.append(FROM + documentClass + " d ");
		sql.append(WHERE_D + PropertyNames.IS_CURRENT_VERSION + UGUALE_TRUE);

		if (!documentTitles.isEmpty()) {
			sql.append(AND_PARENTESI_DX_AP + StringUtils.createInCondition(D_DOCUMENT_TITLE, documentTitles.iterator(), true) + ") ");
		}

		sql.append(ORDER_BY + getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY) + DESC);

		final SearchSQL searchSQL = new SearchSQL(sql.toString());
		final SearchScope scope = new SearchScope(getOs());
		final DocumentSet ds = (DocumentSet) scope.fetchObjects(searchSQL, pageSize, null, true);
		return ds.pageIterator();
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getPageIteratorForMail(java.lang.String,
	 *      java.lang.String, java.lang.String, java.util.List, java.lang.Integer,
	 *      java.lang.String).
	 */
	@Override
	public PageIterator getPageIteratorForMail(final String path, final String documentClass, final String filter, final List<StatoMailEnum> selectedStatList,
			final Integer pageSize, final String orderbyClause) {

		final String searchFields = "d.Id, d.dateCreated, d.ComponentBindingLabel, d.ChildDocuments" + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.DESTINATARI_EMAIL_METAKEY) + ", \"" + getPP().getParameterByKey(PropertiesNameEnum.FROM_MAIL_METAKEY) + "\""
				+ ", d." + getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_EMAIL_METAKEY) + ", d." + getPP().getParameterByKey(PropertiesNameEnum.NOTA_EMAIL_METAKEY)
				+ ", d." + getPP().getParameterByKey(PropertiesNameEnum.DESTINATARI_CC_LONG) + ", d." + getPP().getParameterByKey(PropertiesNameEnum.MESSAGE_ID) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY) + ", d." + getPP().getParameterByKey(PropertiesNameEnum.STATO_MAIL_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ID_UTENTE_PROTOCOLLATORE_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.DATA_RICEZIONE_MAIL_METAKEY) + ", d." + getPP().getParameterByKey(PropertiesNameEnum.DATA_INVIO_MAIL_METAKEY)
				+ ", d." + getPP().getParameterByKey(PropertiesNameEnum.DATA_CAMBIO_STATO_EMAIL_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.METADATO_STATO_PRE_ARCHIVIAZIONE) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.MOTIVO_ELIMINAZIONE_EMAIL_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.PREASSEGNATARIO_SEGN_INTEROP_MAIL_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.MAIL_MITTENTE_SEGN_INTEROP_MAIL_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.WARNINGS_VALIDAZIONE_SEGN_INTEROP_MAIL_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ERRORI_VALIDAZIONE_SEGN_INTEROP_MAIL_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ID_DOC_PRINCIPALE_SEGN_INTEROP_MAIL_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.IDS_ALLEGATI_SEGN_INTEROP_MAIL_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.VALIDAZIONE_SEGN_INTEROP_MAIL_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.PROTOCOLLI_GENERATI_MAIL_METAKEY) + ", d." + getPP().getParameterByKey(PropertiesNameEnum.DATA_SCARICO_METAKEY)
				+ ", d."
//				+ pp.getParameterByKey(PropertiesNameEnum.PREASSEGNATARIO_CONOSCENZA_INTEROP_MAIL_METAKEY ) + ", d."
//				+ pp.getParameterByKey(PropertiesNameEnum.AUTORE_ULTIMA_MODIFICA_NOTA_METAKEY) + ", d." 
				+ getPP().getParameterByKey(PropertiesNameEnum.OPERAZIONE_POSTA_ELETTRONICA_FN_METAKEY)
				+ ", d." +getPP().getParameterByKey(PropertiesNameEnum.PROTOCOLLA_E_MANTIENI_METAKEY);

		try {
			final StringBuilder queryString = new StringBuilder();

			final String projection = searchFields;

			queryString.append(FilenetCERicercaMailUtils.generateBody(false, projection, path, documentClass, "d"));
			if (filter != null) {
				queryString.append(filter);
			}

			if (selectedStatList != null && StringUtils.isNullOrEmpty(orderbyClause)) {

				if (selectedStatList.contains(StatoMailEnum.RIFIUTATA) || selectedStatList.contains(StatoMailEnum.INOLTRATA)) {
					queryString.append(ORDER_BY + getPP().getParameterByKey(PropertiesNameEnum.DATA_INVIO_MAIL_METAKEY) + DESC);
				} else if (selectedStatList.contains(StatoMailEnum.ELIMINATA) || selectedStatList.contains(StatoMailEnum.INARRIVO)
						|| selectedStatList.contains(StatoMailEnum.INARRIVO_NON_PROTOCOLLABILE_AUTOMATICAMENTE)) {
					queryString.append(ORDER_BY + getPP().getParameterByKey(PropertiesNameEnum.DATA_RICEZIONE_MAIL_METAKEY) + DESC);
				}
			} else if (!StringUtils.isNullOrEmpty(orderbyClause)) {
				queryString.append(orderbyClause);
			}

			final SearchSQL searchSQL = new SearchSQL();
			searchSQL.setQueryString(queryString.toString());
			final SearchScope scope = new SearchScope(getOs());

			final DocumentSet ds = (DocumentSet) scope.fetchObjects(searchSQL, pageSize, null, true);
			return ds.pageIterator();
		} catch (final Exception e) {
			throw new FilenetException("Si è verificato un errore durante il recupero della mail", e);
		}

	}

	/**
	 * metodo per recuperare il numero di un documento partendo dal documentTitle,
	 * utile quando si stanno gestendo le response
	 * 
	 * @param documentTitle
	 * @return numero Documento
	 */
	@Override
	public Integer getNumDocByDocumentTitle(final String documentTitle) {
		final String selectMasters = " d." + getPP().getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);

		final Collection<String> documentTitles = new ArrayList<>();
		documentTitles.add(documentTitle);

		final DocumentSet documents = getDocumentBase(selectMasters, getPP().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documentTitles);

		Integer numDoc = null;

		try {
			final Document doc = (Document) documents.iterator().next();
			numDoc = (Integer) TrasformerCE.getMetadato(doc, getPP().getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY));
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del numero delle versioni del documento con ID=" + documentTitle, e);
			throw new FilenetException(" Errore durante il recupero del numero delle versioni del documento con ID=" + documentTitle);
		}

		return numDoc;
	}

	/**
	 * metodo per recuperare il tipo documento di un documento partendo dal
	 * documentTitle
	 * 
	 * @param documentTitle
	 * @return id tipo Documento
	 */
	@Override
	public Integer getTipoDocumentoByDocumentTitle(final String documentTitle) {
		final String selectMasters = " d." + getPP().getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY);

		final Collection<String> documentTitles = new ArrayList<>();
		documentTitles.add(documentTitle);

		final DocumentSet documents = getDocumentBase(selectMasters, getPP().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documentTitles);

		Integer tipoDoc = null;

		try {
			final Document doc = (Document) documents.iterator().next();
			tipoDoc = (Integer) TrasformerCE.getMetadato(doc, getPP().getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY));
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del tipo documento del documento con ID=" + documentTitle, e);
			throw new FilenetException(" Errore durante il recupero del numero del tipo documento del documento con ID=" + documentTitle);
		}

		return tipoDoc;
	}

	private String baseSelect() {
		return " d." + getPP().getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY) + "," + " d." + PropertyNames.ID + "," + " d." + PropertyNames.DATE_CREATED + ","
				+ " d." + getPP().getParameterByKey(PropertiesNameEnum.URGENTE_METAKEY) + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY)
				+ "," + " d." + PropertyNames.COMPOUND_DOCUMENT_STATE + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_EMERGENZA_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_EMERGENZA_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY) + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.TRASFORMAZIONE_PDF_ERRORE_METAKEY)
				+ "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY) + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.FIRMA_PDF_METAKEY)
				+ "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.FLAG_FIRMA_AUTOGRAFA_RM_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY) + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.MITTENTE_METAKEY)
				+ "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.REGISTRO_PROTOCOLLO_FN_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.SOTTOCATEGORIA_DOCUMENTO_USCITA) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.PROTOCOLLO_RIFERIMENTO_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.INTEGRAZIONE_DATI_METAKEY) + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY);
	}

	/**
	 * Recupera il Document FN dato un GUID.
	 * 
	 * @param guid
	 *            - GUID del Document FN da ricercare
	 * @return Document FN.
	 */
	@Override
	public Document getDocumentByGuid(final String guid) {
		return getDocument(idFromGuid(guid));
	}

	/**
	 * Recupera il Document FN dato un GUID, selezionando le property in input.
	 * 
	 * @param guid
	 *            - GUID del Document FN da ricercare.
	 * @param selectList
	 *            - Lista delle property da recuperare.
	 * @return Document FN.
	 */
	@Override
	public Document getDocumentByGuid(final String guid, final String[] selectList) {
		final Id id = new Id(guid);

		PropertyFilter pf = null;
		if (selectList != null && selectList.length > 0) {
			pf = new PropertyFilter();
			for (final String property : selectList) {
				pf.addIncludeProperty(new FilterElement(0, null, false, property, 1));
			}
		}

		return Factory.Document.fetchInstance(getOs(), id, pf);
	}

	/**
	 * Restituisce il Document Set da ricercare tramite parametri.
	 * 
	 * @param select
	 *            - Attributi da ricercare nella select.
	 * @param documentClass
	 *            - Document class dei documenti.
	 * @param documentTitles
	 *            - Collection dei Document Title da ricercare.
	 * @return Document Set dei documenti trovati.
	 */
	private DocumentSet getDocumentBase(final String select, final String documentClass, final Collection<String> documentTitles) {
		return getDocumentBase(select, documentClass, documentTitles, false);
	}

	private DocumentSet getParentDocumentAllegato(final String select, final String guid) {
		return getParentDocumentAllegato(select, guid, false);
	}

	private DocumentSet getDocumentBase(final String select, final String documentClass, final Collection<String> documentTitles, final Boolean orderByCreationDate) {
		final Boolean continuable = true;

		String sql = SELECT + select + FROM + documentClass + " d WHERE d."
				+ PropertyNames.IS_CURRENT_VERSION + UGUALE_TRUE + AND_PARENTESI_DX_AP
				+ StringUtils.createInCondition(D_DOCUMENT_TITLE, documentTitles.iterator(), true) + ") ";

		if (Boolean.TRUE.equals(orderByCreationDate)) {
			// va aggiunto in select altrimenti va in errore
			sql += ORDER_BY + getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY) + DESC;
		}

		LOGGER.info(sql);
		final SearchSQL searchSQL = new SearchSQL(sql);
		final SearchScope scope = new SearchScope(getOs());
		return (DocumentSet) scope.fetchObjects(searchSQL, PAGE_SIZE, null, continuable);
	}

	private DocumentSet getParentDocumentAllegato(final String select, final String guid, final Boolean orderByCreationDate) {
		final Boolean continuable = true;

		String guidWithoutBrachets = guid;
		if (guid.startsWith("{") && guid.endsWith("}")) {
			guidWithoutBrachets = guid.substring(1, guid.length() - 1);
		}

		String sql = SELECT + select + " FROM Documento_NSD d INNER JOIN ComponentRelationship r on r.ParentComponent=[d].This " + WHERE_D
				+ PropertyNames.IS_CURRENT_VERSION + UGUALE_TRUE + " AND r.ChildComponent= OBJECT('{" + guidWithoutBrachets + "}')";

		if (Boolean.TRUE.equals(orderByCreationDate)) {
			// va aggiunto in select altrimenti va in errore
			sql += ORDER_BY + getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY) + DESC;
		}

		LOGGER.info(sql);
		final SearchSQL searchSQL = new SearchSQL(sql);
		final SearchScope scope = new SearchScope(getOs());
		return (DocumentSet) scope.fetchObjects(searchSQL, PAGE_SIZE, null, continuable);
	}

	/**
	 * Ritorna il numero di versioni del documento.
	 * 
	 * @param documentTitle
	 *            - Document Title del documento da ricercare.
	 * @param idAoo
	 *            - Id dell'AOO del documento
	 * @return Numero delle versioni.
	 */
	@Override
	public int getNumVersions(final Integer documentTitle, final Long idAoo) {
		int nVersion;
		final String sql = "SELECT * FROM Document d " + " WHERE NOT IsClass(d," + getPP().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY) + ")"
				+ " AND NOT IsClass(d, " + getPP().getParameterByKey(PropertiesNameEnum.FALDONE_CLASSNAME_FN_METAKEY) + ")" + AND_D_TABLE
				+ getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY) + "=" + idAoo + " AND d.DocumentTitle='" + documentTitle + "'" + AND_D_TABLE
				+ PropertyNames.IS_CURRENT_VERSION + UGUALE_TRUE;
		final DocumentSet documents = (DocumentSet) new SearchScope(getOs()).fetchObjects(new SearchSQL(sql), 1, null, false);
		if (documents == null || documents.isEmpty()) {
			throw new FilenetException("Nessun documento trovato sul CE attraverso la seguente query: " + sql);
		}
		try {
			final Document doc = (Document) documents.iterator().next();
			final SubSetImpl ssi = (SubSetImpl) doc.get_VersionSeries().get_Versions();
			final List<?> list = ssi.getList();
			nVersion = list.size();
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del numero delle versioni del documento con ID=" + documentTitle, e);
			throw new FilenetException(" Errore durante il recupero del numero delle versioni del documento con ID=" + documentTitle);
		}
		return nVersion;
	}

	/**
	 * Ritorna il Fascicolo FN.
	 * 
	 * @param idFascicolo
	 *            - Id del fascicolo FN
	 * @param idAoo
	 *            - Id dell'AOO
	 * @return Fascicolo FN
	 */
	@Override
	public Document getFascicolo(final String idFascicolo, final Integer idAoo) {
		final Boolean continuable = true;
		String sql = SELECT + PropertyNames.ID + ", " + PropertyNames.NAME + "," + PropertyNames.FOLDERS_FILED_IN + ", "
				+ getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY) + ", " + getPP().getParameterByKey(PropertiesNameEnum.STATO_FASCICOLO_METAKEY) + ", "
				+ getPP().getParameterByKey(PropertiesNameEnum.TITOLARIO_METAKEY) + ", " + getPP().getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA)
				+ ", " + PropertyNames.DATE_CREATED + ", " + getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY) + ", "
				+ getPP().getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY) + ", " + getPP().getParameterByKey(PropertiesNameEnum.FALDONATO_METAKEY) + ", "
				+ getPP().getParameterByKey(PropertiesNameEnum.DATA_TERMINAZIONE_METAKEY) + ", " + getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY) + ", "
				+ getPP().getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_TIPO_FASCICOLO_FEPA_METAKEY) + FROM
				+ getPP().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY) + " f " + WHERE_F
				+ getPP().getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY) + " = '" + idFascicolo + "'";
		if (idAoo != null) {
			sql += AND + getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY) + " = " + idAoo;
		}
		
		return returnDocumentFromSql(sql, continuable);
	}
	
	private Document returnDocumentFromSql(final String sql, final Boolean continuable) {
		LOGGER.info(sql);
		final SearchSQL searchSQL = new SearchSQL(sql);
		final SearchScope scope = new SearchScope(getOs());

		final DocumentSet ds = (DocumentSet) scope.fetchObjects(searchSQL, PAGE_SIZE, null, continuable);

		Document output = null;
		final Iterator<?> it = ds.iterator();
		if (it != null && it.hasNext()) {
			output = (Document) it.next();
		}
		return output;
	}

	/**
	 * Ritorna il Fascicolo FN.
	 * 
	 * @param idFascicoloFepa
	 *            - Id del fascicolo FEPA
	 * @return Fascicolo FN
	 */
	@Override
	public Document getFascicoloByIdFascicoloFepa(final String idFascicoloFepa) {
		final Boolean continuable = true;
		final String sql = SELECT + PropertyNames.ID + ", " + PropertyNames.NAME + ", " + PropertyNames.FOLDERS_FILED_IN + ", "
				+ getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY) + ", " + getPP().getParameterByKey(PropertiesNameEnum.STATO_FASCICOLO_METAKEY) + ", "
				+ getPP().getParameterByKey(PropertiesNameEnum.TITOLARIO_METAKEY) + ", " + getPP().getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA)
				+ ", " + getPP().getParameterByKey(PropertiesNameEnum.FALDONATO_METAKEY) + ", " + getPP().getParameterByKey(PropertiesNameEnum.DATA_TERMINAZIONE_METAKEY)
				+ ", " + PropertyNames.DATE_CREATED + ", " + getPP().getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY) + FROM
				+ getPP().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY) + " f " + WHERE_F
				+ getPP().getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA) + " = '" + idFascicoloFepa + "'";

		return returnDocumentFromSql(sql, continuable);
	}

	/**
	 * Ritorna il Set dei documenti FN contenuti nel fascicolo desiderato. N.B.
	 * IMPORTANTE! Questo metodo restituisce tutti i documenti contenuti nel
	 * fascicolo, quindi include il "documento" del fascicolo e gli eventuali
	 * contributi. Per ottenere solo i documenti RED, utilizzare il metodo
	 * "getOnlyDocumentiRedFascicolo".
	 * 
	 * @param idfascicolo
	 *            - Id del fascicolo.
	 * @param idAoo
	 *            - Id dell'AOO
	 * @return Set dei documenti FN contenuti nel fascicolo desiderato;
	 */
	@Override
	public DocumentSet getDocumentiFascicolo(final Integer idfascicolo, final Long idAoo) {
		Folder folder = null;
		String sql = SELECT + PropertyNames.ID + "," + PropertyNames.DATE_CREATED + "," + PropertyNames.SECURITY_FOLDER + "," + " "
				+ getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY) + "," + " " + getPP().getParameterByKey(PropertiesNameEnum.STATO_FASCICOLO_METAKEY) + "," + " "
				+ getPP().getParameterByKey(PropertiesNameEnum.TITOLARIO_METAKEY) + "," + " " + getPP().getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY) + FROM
				+ getPP().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY) + WHERE
				+ getPP().getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY) + " = '" + idfascicolo + "'" + AND + PropertyNames.IS_CURRENT_VERSION + UGUALE_TRUE;
		if (idAoo != null) {
			sql += AND + getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY) + " = " + idAoo;
		}
		LOGGER.info(sql);
		final DocumentSet documents = (DocumentSet) new SearchScope(getOs()).fetchObjects(new SearchSQL(sql), 1, null, false);
		final Iterator<?> it = documents.iterator();
		if (it.hasNext()) {
			final Document doc = (Document) it.next();
			folder = doc.get_SecurityFolder();
		}
		if (folder != null) {
			return folder.get_ContainedDocuments();
		} else {
			return null;
		}
	}

	/**
	 * Restituisce il set dei soli Documenti FN RED contenuti nel fascicolo
	 * desiderato. La collection quindi NON include il "documento" del fascicolo e
	 * gli eventuali contributi.
	 * 
	 * @param idfascicolo
	 *            - Id del fascicolo.
	 * @param idAoo
	 *            - Id dell'AOO
	 * @return Set dei Documenti RED FN contenuti nel fascicolo desiderato;
	 */
	@Override
	public Collection<DocumentoFascicoloDTO> getOnlyDocumentiRedFascicolo(final Integer idfascicolo, final Long idAoo) {
		final Collection<DocumentoFascicoloDTO> listaDocumenti = new ArrayList<>();
		final Collection<Document> documentFn = getOnlyDocumentiRedFascicoloFn(idfascicolo, idAoo);
		final Iterator<Document> it = documentFn.iterator();

		while (it.hasNext()) {
			listaDocumenti.add(TrasformCE.transform(it.next(), TrasformerCEEnum.FROM_DOCUMENTO_TO_DOCUMENTO_FASCICOLO));
		}

		return listaDocumenti;
	}

	/**
	 * Restituisce il set dei soli Documenti FN RED contenuti nel fascicolo
	 * desiderato. La collection quindi NON include il "documento" del fascicolo e
	 * gli eventuali contributi.
	 * 
	 * @param idfascicolo
	 *            - Id del fascicolo.
	 * @param idAoo
	 *            - Id dell'AOO
	 * @return Set dei Documenti RED FN contenuti nel fascicolo desiderato;
	 */
	@Override
	public Collection<DocumentoAllegabileDTO> getOnlyDocumentiRedFascicoloAllegabili(final FascicoloDTO fascicolo, final Long idAoo, final boolean originale4DocEntrata,
			final boolean withContent) {
		final Collection<DocumentoAllegabileDTO> listaDocumenti = new ArrayList<>();

		final Collection<Document> documentFn = getOnlyDocumentiRedFascicoloFn(Integer.parseInt(fascicolo.getIdFascicolo()), idAoo);
		final Iterator<Document> it = documentFn.iterator();
		Document documentoFn = null;
		Document documentoFnFirstVersion = null;
		DocumentoAllegabileDTO documentoAllegabile = null;
		boolean isDocEntrata = false;
		String guid = null;

		while (it.hasNext()) {

			documentoFn = it.next();
			guid = documentoFn.get_Id().toString();

			if (!FilenetCEHelper.hasDocumentContentTransfer(documentoFn)) {
				continue;
			}

			documentoAllegabile = DocumentoAllegabileDTOFactory.getDocumentoAllegabileDTO(documentoFn, fascicolo, withContent);
			
			isDocEntrata = originale4DocEntrata && documentoFn.get_MajorVersionNumber() != 1
					&& ((CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0]).equals(documentoAllegabile.getIdCategoria())
							|| (CategoriaDocumentoEnum.INTERNO.getIds()[0]).equals(documentoAllegabile.getIdCategoria())
							|| (CategoriaDocumentoEnum.ASSEGNAZIONE_INTERNA.getIds()[0]).equals(documentoAllegabile.getIdCategoria()));
			
			if (isDocEntrata) {
				// modifica il nome del documento con "_PROT" prima dell'estensione del file in
				// caso
				// sia necessario estrarre anche la prima versione del file (valido solo per doc
				// entrata)
				String nomeDocumento = documentoAllegabile.getNomeFile();
				final String estensione = FilenameUtils.getExtension(documentoAllegabile.getNomeFile());
				if (!estensione.isEmpty()) {
					nomeDocumento = nomeDocumento.substring(0, nomeDocumento.lastIndexOf('.')) + "_PROT." + estensione;
				} else {
					nomeDocumento = nomeDocumento + "_PROT";
				}

				documentoAllegabile.setNomeFile(nomeDocumento);
			}

			listaDocumenti.add(documentoAllegabile);

			if (isDocEntrata) {
				// se è un documento in ingresso, prendi anche la versione originale del
				// documento
				documentoFnFirstVersion = getFirstVersionByGuid(guid);

				if (!documentoFnFirstVersion.getClassName().equalsIgnoreCase(getPP().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY))
						&& !documentoFnFirstVersion.getClassName().equalsIgnoreCase(getPP().getParameterByKey(PropertiesNameEnum.CONTRIBUTO_CLASSNAME_FN_METAKEY))) {
					final Properties propsDocRed = documentoFnFirstVersion.getProperties();
					if (BooleanFlagEnum.SI.getIntValue().equals(propsDocRed.getInteger32Value(getPP().getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY)))) {
						continue;
					}

					documentoAllegabile = DocumentoAllegabileDTOFactory.getDocumentoAllegabileDTO(documentoFnFirstVersion, fascicolo, withContent);

					if (!new BigDecimal(0).equals(documentoAllegabile.getDimensione())) {
						listaDocumenti.add(documentoAllegabile);
					}

				}

			}

		}

		return listaDocumenti;
	}

	/**
	 * Restituisce il set dei soli Documenti FN RED contenuti nel fascicolo
	 * desiderato. La collection quindi NON include il "documento" del fascicolo e
	 * gli eventuali contributi.
	 * 
	 * @param idfascicolo
	 *            - ID del fascicolo.
	 * @param idAoo
	 *            - ID dell'AOO
	 * @return Set dei Documenti RED FN contenuti nel fascicolo desiderato;
	 */
	private Collection<Document> getOnlyDocumentiRedFascicoloFn(final Integer idfascicolo, final Long idAoo) {
		final Collection<Document> listaDocumenti = new ArrayList<>();
		
		Document fascicolo = retrieveFascicolo(idfascicolo, idAoo);
		
		Folder folderFascicolo = null;
		if (fascicolo != null) {
			folderFascicolo = fascicolo.get_SecurityFolder(); // Folder del Fascicolo
		}
		
		if (folderFascicolo != null) {
			boolean isFascicoloFaldonato = false;
			boolean isFascicoloNonCatalogato = false;
			final Properties propsFascicolo = fascicolo.getProperties();
			if (Boolean.TRUE.equals(propsFascicolo.getBooleanValue(getPP().getParameterByKey(PropertiesNameEnum.FALDONATO_METAKEY)))) {
				isFascicoloFaldonato = true;
			}
			if (FilenetStatoFascicoloEnum.NON_CATALOGATO.getNome()
					.equalsIgnoreCase(propsFascicolo.getStringValue(getPP().getParameterByKey(PropertiesNameEnum.TITOLARIO_METAKEY)))) {
				isFascicoloNonCatalogato = true;
			}

			final DocumentSet listaDocumentiFilenet = folderFascicolo.get_ContainedDocuments();
			final Iterator<?> itListaDocumenti = listaDocumentiFilenet.iterator();

			while (itListaDocumenti.hasNext()) {
				final Document documentoFilenet = getDocument(((Document) itListaDocumenti.next()).get_Id());

				// Si skippano i documenti FileNet che rappresentano fascicoli o contributi,
				// considerando solo i documenti RED
				if (!documentoFilenet.getClassName().equalsIgnoreCase(getPP().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY))
						&& !documentoFilenet.getClassName().equalsIgnoreCase(getPP().getParameterByKey(PropertiesNameEnum.CONTRIBUTO_CLASSNAME_FN_METAKEY))) {
					final Properties propsDocRed = documentoFilenet.getProperties();

					boolean isRegistroRiservato = false;
					if (BooleanFlagEnum.SI.getIntValue().equals(propsDocRed.getInteger32Value(getPP().getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY)))) {
						isRegistroRiservato = true;
					}

					// Se il fascicolo non è catalogato oppure non è faldonato, si skippano i
					// documenti RED con "Registro Riservato"
					if (isRegistroRiservato && (isFascicoloNonCatalogato || !isFascicoloFaldonato)) {
						continue;
					}

					listaDocumenti.add(documentoFilenet);
				}
			}
		}

		return listaDocumenti;
	}

	/**
	 * Recupera il fascicolo identificato dall' {@code idAoo}.
	 * 
	 * @param idfascicolo
	 *            Identificativo del fascicolo da recuperae.
	 * @param idAoo
	 *            Identificativo dell'area organizzativa, se {@code null} non viene
	 *            specificato nella query.
	 * @return Fascicolo recuperato come Document.
	 */
	private Document retrieveFascicolo(final Integer idfascicolo, final Long idAoo) {
		
		Document fascicolo = null;
		String sql = SELECT + PropertyNames.ID + "," + PropertyNames.SECURITY_FOLDER + "," + " " + getPP().getParameterByKey(PropertiesNameEnum.TITOLARIO_METAKEY) + ","
				+ " " + getPP().getParameterByKey(PropertiesNameEnum.FALDONATO_METAKEY) + FROM
				+ getPP().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY) + WHERE
				+ getPP().getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY) + " = '" + idfascicolo + "'" + AND + PropertyNames.IS_CURRENT_VERSION + UGUALE_TRUE;
		
		if (idAoo != null) {
			sql += AND + getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY) + " = " + idAoo;
		}
		LOGGER.info(sql);

		final DocumentSet documents = (DocumentSet) new SearchScope(getOs()).fetchObjects(new SearchSQL(sql), 1, null, false);
		final Iterator<?> itFascicolo = documents.iterator();
		if (itFascicolo.hasNext()) {
			fascicolo = (Document) itFascicolo.next();
		}
		return fascicolo;
	}

	/**
	 * Restituisce il set delle sole Fatture contenute nel fascicolo desiderato.
	 * 
	 * @param idfascicolo
	 *            - Id del fascicolo.
	 * @param idAoo
	 *            - Id dell'AOO
	 * @return Set delle Fatture contenute nel fascicolo desiderato;
	 */
	@Override
	public Collection<DetailFatturaFepaDTO> getOnlyFattureFromRedFascicolo(final Integer idfascicolo, final Long idAoo) {
		final Collection<DetailFatturaFepaDTO> listaDocumenti = new ArrayList<>();
		
		Document fascicolo = retrieveFascicolo(idfascicolo, idAoo);
		
		Folder folderFascicolo = null;
		if (fascicolo != null) {
			folderFascicolo = fascicolo.get_SecurityFolder(); // Folder del Fascicolo
		}
		
		if (folderFascicolo != null) {
			final DocumentSet listaDocumentiFilenet = folderFascicolo.get_ContainedDocuments();
			final Iterator<?> itListaDocumenti = listaDocumentiFilenet.iterator();

			while (itListaDocumenti.hasNext()) {
				final Document documentoFilenet = getDocument(((Document) itListaDocumenti.next()).get_Id());

				if (documentoFilenet.getClassName().equalsIgnoreCase(getPP().getParameterByKey(PropertiesNameEnum.FATTURA_FEPA_CLASSNAME_FN_METAKEY))) {
					listaDocumenti.add(TrasformCE.transform(documentoFilenet, TrasformerCEEnum.FROM_DOCUMENT_TO_FATTURA));
				}
			}
		}

		return listaDocumenti;
	}

	/**
	 * Ricerca i documenti tramite criteri di ricerca.
	 * 
	 * @param key
	 *            - Chiave di ricerca(match con DocumentTitle || Oggetto || Barcode)
	 * @param anno
	 *            - Anno dei documenti da ricercare.
	 * @param type
	 *            - Tipo della ricerca.
	 * @param idAoo
	 *            - Id dell'AOO.
	 * @return DocumentSet FN dei documenti trovati.
	 */
	@Override
	public DocumentSet ricercaGenerica(final String key, final Integer anno, final RicercaGenericaTypeEnum type, final Long idAoo) {
		return ricercaGenerica(key, anno, type, idAoo, false);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#ricercaGenericaDocumenti(java.lang.String,
	 *      java.lang.Integer, it.ibm.red.business.enums.RicercaGenericaTypeEnum,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public DocumentSet ricercaGenericaDocumenti(final String valoreDaRicercare, final Integer anno, final RicercaGenericaTypeEnum type, final UtenteDTO utente) {
		final Boolean flagRegistroRiservato = false;
		final String[] valoriDaRicercare = StringUtils.deleteSpecialCharacterForSpace(valoreDaRicercare).split(" ");
		final String classeDocumentale = getPP().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY);
		final String documentTitle = getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
		final String oggetto = getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY);
		final String barcode = getPP().getParameterByKey(PropertiesNameEnum.BARCODE_METAKEY);
		final String numeroDocumento = getPP().getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
		final String numeroProtocollo = getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
		final String registroRiservato = getPP().getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY);
		final String riservato = getPP().getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY);

		final StringBuilder whereCondition = new StringBuilder();

		String controlloAnno = "";

		if (anno != null) {
			controlloAnno = AND_PARENTESI_DX_AP + getPP().getParameterByKey(PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY) + "=" + anno + " OR "
					+ getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY) + "=" + anno + ") ";
		}

		if (RicercaGenericaTypeEnum.ESATTA.equals(type)) {
			final StringBuilder toSearch = new StringBuilder("");
			for (int i = 0; i < valoriDaRicercare.length; i++) {
				// aggiunge lo spazio in caso di più parole
				if (i != 0) {
					toSearch.append(" ");
				}
				toSearch.append(valoriDaRicercare[i]);
			}
			whereCondition.append(documentTitle + " = '" + toSearch + "'");
			whereCondition.append(" OR " + oggetto + " like '%" + toSearch + "%'");
			whereCondition.append(" OR " + barcode + " = '" + toSearch + "'");
		} else {
			final Pattern numericPattern = Pattern.compile("\\d*");
			for (int i = 0; i < valoriDaRicercare.length; i++) {
				if (!StringUtils.isNullOrEmpty(valoriDaRicercare[i])) {
					// Si verifica se il numero è un integer
					boolean isInt = true;
					try {
						Integer.parseInt(valoriDaRicercare[i]);
					} catch (final NumberFormatException nfe) {
						LOGGER.warn(nfe);
						isInt = false;
					}

					if (whereCondition.length() > 0) {
						String operator = "AND";
						if (RicercaGenericaTypeEnum.QUALSIASI.equals(type)) {
							operator = "OR";
						}
						whereCondition.append(" ) " + operator + " ( ");
					}

					if (numericPattern.matcher(valoriDaRicercare[i]).matches() && isInt) {
						whereCondition.append(numeroDocumento + " = " + valoriDaRicercare[i] + " OR ");
					}

					whereCondition.append(barcode + " = '" + valoriDaRicercare[i] + "'");
					whereCondition.append(" OR " + oggetto + " like '%" + valoriDaRicercare[i] + "%'");

					if (numericPattern.matcher(valoriDaRicercare[i]).matches() && isInt) {
						whereCondition.append(" OR " + numeroProtocollo + " = " + valoriDaRicercare[i]);
					}
				}
			}
		}

		String regRisCondition = "";
		if (!"".equals(whereCondition.toString())) {
			if (Boolean.TRUE.equals(flagRegistroRiservato)) {
				regRisCondition = " (" + registroRiservato + " is not null AND " + registroRiservato + " = 1) ";
			} else {
				regRisCondition = " (" + registroRiservato + " is null OR " + registroRiservato + EQUAL_0;
			}
		}

		String whereConditionPart = " where (( " + whereCondition + " ))" + controlloAnno + " AND IsCurrentVersion = TRUE AND " + regRisCondition;
		if ("".equals(whereCondition.toString())) {
			whereConditionPart = "";
		}

		whereConditionPart += AND_ID_AOO + utente.getIdAoo();

		// gestione riservato: nella ricerca controllo che il documento può essere
		// ricercato anche in caso fosse riservato gesendo le security dell utente
		final List<String> gruppiAD = getGruppiAD();
		if (!CollectionUtils.isEmpty(gruppiAD)) {
			whereCondition.append(AND_PARENTESI_DX_AP);

			final boolean hasPermessoCodaCorriere = PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.CODA_CORRIERE);
			whereCondition.append(riservato).append(CONSTANT_1_AND_ACL_INTERSECTS).append(getACLClause(restrictVisibility(utente.getId(), utente.getIdNodoCorriere().intValue(),
					utente.getIdUfficio(), null, null, utente.getUsername(), gruppiAD, hasPermessoCodaCorriere, false, true, utente.getVisFaldoni())));

			whereCondition.append(" )");
		}

		final String sqlStr = SELECT + PropertyNames.ID + "," + " d." + PropertyNames.DATE_CREATED + "," + " d." + PropertyNames.COMPOUND_DOCUMENT_STATE + ","
				+ " d.DocumentTitle," + " d." + getPP().getParameterByKey(PropertiesNameEnum.URGENTE_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY) + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.ID_FORMATO_DOCUMENTO)
				+ "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.BARCODE_METAKEY) + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY) + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY)
				+ "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.FIRMA_PDF_METAKEY) + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.TRASFORMAZIONE_PDF_ERRORE_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.FLAG_FIRMA_AUTOGRAFA_RM_METAKEY) + FROM + classeDocumentale + " d " + whereConditionPart + ORDER_BY
				+ PropertyNames.DATE_CREATED + DESC;

		final SearchSQL sql = new SearchSQL(sqlStr);

		final SearchScope searchScope = new SearchScope(getOs());
		return (DocumentSet) searchScope.fetchObjects(sql, null, null, false);

	}

	/**
	 * Ricerca i documenti tramite criteri di ricerca.
	 * 
	 * @param key
	 *            - Chiave di ricerca(match con DocumentTitle || Oggetto || Barcode
	 *            || Numero Protocollo)
	 * @param anno
	 *            - Anno dei documenti da ricercare.
	 * @param type
	 *            - Tipo della ricerca.
	 * @param idAoo
	 *            - Id dell'AOO.
	 * @param onlyDocumentTitle
	 *            - TRUE esclude tra i match Oggetto,Barcode
	 * @return DocumentSet FN dei documenti trovati.
	 */
	@Override
	public DocumentSet ricercaGenerica(final String key, final Integer anno, final RicercaGenericaTypeEnum type, final Long idAoo, final Boolean onlyDocumentTitle) {
		return ricercaGenerica(key, anno, type, null, idAoo, onlyDocumentTitle, false);
	}

	/**
	 * Ricerca i documenti tramite criteri di ricerca.
	 * 
	 * @param key
	 *            - Chiave di ricerca(match con DocumentTitle || Oggetto || Barcode
	 *            || Numero Protocollo)
	 * @param anno
	 *            - Anno dei documenti da ricercare.
	 * @param type
	 *            - Tipo della ricerca.
	 * @param categoria
	 *            - Categoria documento da ricercare.
	 * @param idAoo
	 *            - Id dell'AOO.
	 * @param onlyDocumentTitle
	 *            - TRUE esclude tra i match Oggetto,Barcode
	 * @param onlyProtocollati
	 *            - TRUE esclude documenti senza idProtocollo
	 * @return DocumentSet FN dei documenti trovati.
	 */
	@Override
	public DocumentSet ricercaGenerica(final String key, final Integer anno, final RicercaGenericaTypeEnum type, final CategoriaDocumentoEnum categoria, final Long idAoo,
			final Boolean onlyDocumentTitle, final Boolean onlyProtocollati) {
		final StringBuilder whereConditionSQL = new StringBuilder("");

		final String keyLowerCase = key.toLowerCase(); // Gestione case insensitive
		final String[] valoriDaRicercare = StringUtils.deleteSpecialCharacterForSpace(keyLowerCase).split(" ");

		final Pattern numericPattern = Pattern.compile("\\d*");

		if (RicercaGenericaTypeEnum.ESATTA.equals(type)) {
			final StringBuilder toSearch = new StringBuilder("");
			for (int i = 0; i < valoriDaRicercare.length; i++) {
				if (i != 0) {
					toSearch.append(" ");
				}
				toSearch.append(valoriDaRicercare[i]);
			}
			final String strToSearch = toSearch.toString();

			whereConditionSQL.append(D_DOCUMENT_TITLE + " = '" + strToSearch + "'");
			if (onlyDocumentTitle == null || !onlyDocumentTitle) {

				final String numericStrToSearch = strToSearch.trim();
				if (numericPattern.matcher(numericStrToSearch.trim()).matches()) {
					whereConditionSQL.append("d." + getPP().getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY) + " = " + numericStrToSearch);
					whereConditionSQL.append(" OR ");
					whereConditionSQL.append(" d." + getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY) + " = " + numericStrToSearch);
					whereConditionSQL.append(" OR ");
				}

				whereConditionSQL.append(OR_LOWER_D + getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY) + PAR_CHIUSA_LIKE_MODUL + strToSearch + "%'");
				whereConditionSQL.append(OR_LOWER_D + getPP().getParameterByKey(PropertiesNameEnum.BARCODE_METAKEY) + PAR_CHIUSA_UGUALE + strToSearch + "'");
			}
		} else {

			String operatore = "OR"; // caso QUALSIASI
			if (RicercaGenericaTypeEnum.TUTTE.equals(type)) {
				operatore = "AND";
			}

			for (int i = 0; i < valoriDaRicercare.length; i++) {
				if (!"".equals(valoriDaRicercare[i])) {
					if (whereConditionSQL.length() > 0) {
						whereConditionSQL.append(" ) " + operatore + " ( ");
					}
					if (numericPattern.matcher(valoriDaRicercare[i]).matches()) {
						whereConditionSQL.append("d." + getPP().getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY) + " = " + valoriDaRicercare[i]);
						whereConditionSQL.append(" OR ");
						whereConditionSQL.append(" d." + getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY) + " = " + valoriDaRicercare[i]);
						whereConditionSQL.append(" OR ");
					}
					whereConditionSQL.append("LOWER(d." + getPP().getParameterByKey(PropertiesNameEnum.BARCODE_METAKEY) + PAR_CHIUSA_UGUALE + valoriDaRicercare[i] + "'");
					whereConditionSQL.append(OR_LOWER_D + getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY) + PAR_CHIUSA_LIKE_MODUL + valoriDaRicercare[i] + "%'");
				}
			}
		}

		String regRisCondition = "";
		if (!"".equals(whereConditionSQL.toString())) {
			regRisCondition = AND_D + getPP().getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY) + "  is null" + " OR " + " d."
					+ getPP().getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY) + EQUAL_0;
		}

		StringBuilder whereCondition = new StringBuilder("");
		// " WHERE d." + PropertyNames.IS_CURRENT_VERSION + " = true AND " +

		if (!"".equals(whereConditionSQL.toString())) {
			whereCondition.append(" (( " + whereConditionSQL + " ))");
		}

		if (anno != null) {
			if (!"".equals(whereCondition.toString())) {
				whereCondition.append(AND);
			}
			whereCondition.append("(d." + getPP().getParameterByKey(PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY) + "=" + anno + " OR d."
					+ getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY) + "=" + anno + ")");
		}

		if (categoria != null) {
			if (!"".equals(whereCondition.toString())) {
				whereCondition.append(AND);
			}
			whereCondition.append("( ");
			final Integer[] categorie = categoria.getIds();
			for (int i = 0; i < categorie.length; i++) {
				whereCondition.append("d.idCategoriaDocumento = " + categorie[i]);
				if (i < categorie.length - 1) {
					whereCondition.append(" OR ");
				}
			}
			whereCondition.append(" ) ");

		} else {
			if (!"".equals(whereCondition.toString())) {
				whereCondition.append(AND);
			}
			whereCondition.append("d.idCategoriaDocumento <> 5 ");
		}

		if (Boolean.TRUE.equals(onlyProtocollati)) {
			whereCondition.append(AND + getPP().getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY) + IS_NOT_NULL);
		}

		whereCondition.append(regRisCondition);

		whereCondition.append(AND + getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY) + " = " + idAoo);

		whereCondition = new StringBuilder(WHERE_D + PropertyNames.IS_CURRENT_VERSION + " = true AND (" + whereCondition + ")");

		final List<String> gruppiAD = getGruppiAD();
		if (gruppiAD != null && !gruppiAD.isEmpty()) {
			whereCondition.append(AND_ACL_INTERSECTS + getACLClause(gruppiAD));
		}

		final String sql = "SELECT TOP " + getPP().getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS) + " " + PropertyNames.ID + "," + " d."
				+ PropertyNames.DATE_CREATED + "," + " d." + PropertyNames.COMPOUND_DOCUMENT_STATE + "," + " d.DocumentTitle," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.URGENTE_METAKEY) + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY) + ","
				+ " d." + getPP().getParameterByKey(PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY) + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.ID_FORMATO_DOCUMENTO)
				+ "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.BARCODE_METAKEY) + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY) + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY)
				+ "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY) + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.FIRMA_PDF_METAKEY) + ","
				+ " d." + getPP().getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY) + ","
				// + " d.riservato,"
				// + " d.assegnatariConoscenza,"
				// + " d.assegnatariContributo,"
				// + " d.dataannullamentodocumento,"
				+ " d." + getPP().getParameterByKey(PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.TRASFORMAZIONE_PDF_ERRORE_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY) + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.INTEGRAZIONE_DATI_METAKEY)
				+ "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.FLAG_FIRMA_AUTOGRAFA_RM_METAKEY) + FROM
				+ getPP().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY) + " d " + whereCondition.toString() + ORDER_BY + PropertyNames.DATE_CREATED
				+ DESC;

		final Boolean continuable = true;

		LOGGER.info(sql);
		final SearchSQL searchSQL = new SearchSQL(sql);
		final SearchScope scope = new SearchScope(getOs());
		return (DocumentSet) scope.fetchObjects(searchSQL, PAGE_SIZE, null, continuable);
	}

	/**
	 * Restituisce la stringa della ACL separate da comma dato una lista di
	 * GroupList.
	 * 
	 * @param groupList
	 *            - GroupList dela ACL
	 * @return Stringa della ACL separate da comma
	 */
	private static String getACLClause(final List<String> groupList) {
		final StringBuilder sb = new StringBuilder("(");

		if (groupList.isEmpty()) {
			sb.append("''");
		}

		for (int i = 0; i < groupList.size(); i++) {
			if (i == (groupList.size() - 1)) {
				sb.append("'" + groupList.get(i) + "'");
			} else {
				sb.append("'" + groupList.get(i) + "',");
			}
		}
		sb.append(")");

		return sb.toString();
	}

	/**
	 * Ritorna la lista dei Gruppi AD.
	 * 
	 * @return Lista dei Gruppi AD
	 */
	@Override
	public List<String> getGruppiAD() {
		List<String> gruppi = null;
		final User user = Factory.User.fetchCurrent(getOs().getConnection(), null);
		final GroupSet gs = user.get_MemberOfGroups();
		if (!gs.isEmpty()) {
			final Iterator<?> iter = gs.iterator();
			while (iter.hasNext()) {
				final Group g = (Group) iter.next();
				if (gruppi == null) {
					gruppi = new ArrayList<>(GRUPPI_AD_INITIAL_CAPACITY);
				}
				gruppi.add(g.get_Name());
			}
		}
		if (gruppi == null) {
			gruppi = new ArrayList<>(1);
		}

		gruppi.add(user.get_Name());
		return gruppi;
	}

	/**
	 * Effettua il Checkout di un Document FN.
	 * 
	 * @param doc
	 *            - Document FN.
	 */
	public static void checkout(final Document doc) {
		if (Boolean.FALSE.equals(doc.get_IsReserved())) {
			doc.checkout(ReservationType.EXCLUSIVE, null, null, null);
			doc.save(RefreshMode.REFRESH);
		}
	}

	/**
	 * Effettua il Checkin di un Document FN.
	 * 
	 * @param doc
	 *            - Document FN.
	 * @param is
	 *            - InputStream del content del documento da salvare.
	 * @param isPreview
	 *            - InputStream del content della preview del documento da salvare.
	 * @param isPreview
	 *            - InputStream del content da mostrare nel Libro Firma del
	 *            documento da salvare.
	 * @param metadati
	 *            - Metadati da salvare.
	 * @param fileName
	 *            - Nome del file del documento.
	 * @param mimeType
	 *            - MimeType del documento.
	 */
	@Override 
	public void checkin(final Document doc, final InputStream is, final InputStream isPreview, final InputStream isLibroFirma,
			final Map<String, Object> metadati, final String fileName, final String mimeType,
			final Long idAoo) {
		try {
			// Setto il content solo se presente
			if (is != null) {
				setContentDoc(doc, fileName, mimeType, is, isPreview, isLibroFirma);
			}
			setPropertiesAndSaveDocument(doc, metadati, false);
			doc.checkin(AutoClassify.DO_NOT_AUTO_CLASSIFY, CheckinType.MAJOR_VERSION);
			doc.save(RefreshMode.REFRESH);

			if (NumberUtils.isParsable(doc.get_Name())) {
				final IVerificaFirmaSRV verificaFirmaSRV = ApplicationContextProvider.getApplicationContext().getBean(IVerificaFirmaSRV.class);
				verificaFirmaSRV.deleteSingoloElemento(idAoo, doc.get_Name(), os.get_Name(), doc.getClassName(), doc.get_MajorVersionNumber());
				verificaFirmaSRV.insertDopoCreazioneSuFilenet(idAoo,doc.get_Name(),StatoVerificaFirmaEnum.DA_LAVORARE.getStatoVerificaFirma(),os.get_Name(),doc.getClassName(),doc.get_MajorVersionNumber());
			} 
		} catch (final Exception e) {
			LOGGER.error("Errore durante il checkin del documento:", e);
			throw new FilenetException(e);
		}
	}

	/**
	 * Esegue il setting delle properties di un document.
	 * 
	 * @param document
	 *            - Document Fn
	 * @param metadati
	 *            - Metadati da settare
	 * @param saveDoc
	 *            - se 'TRUE' esegue il save in REFRESH.MODE del document.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void setPropertiesAndSaveDocument(final Document document, final Map<String, Object> metadati, final boolean saveDoc) {
		if (metadati != null && !metadati.keySet().isEmpty()) {
			final Properties props = document.getProperties();

			for (final Entry<String, Object> entry : metadati.entrySet()) {
				if (entry.getValue() instanceof Date) {
					props.putValue(entry.getKey(), (Date) entry.getValue());
				} else if (entry.getValue() instanceof Boolean) {
					props.putValue(entry.getKey(), (Boolean) entry.getValue());
				} else if (entry.getValue() instanceof Double) {
					props.putValue(entry.getKey(), (Double) entry.getValue());
				} else if (entry.getValue() instanceof Integer) {
					props.putValue(entry.getKey(), (Integer) entry.getValue());
				} else if (entry.getValue() instanceof Long) {
					props.putValue(entry.getKey(), ((Long) entry.getValue()).intValue());
				} else if (entry.getValue() instanceof String) {
					props.putValue(entry.getKey(), (String) entry.getValue());
				} else if (entry.getValue() instanceof String[]) {
					final String[] arrayString = (String[]) entry.getValue();
					final StringList sl = Factory.StringList.createList();
					sl.addAll(Arrays.asList(arrayString));
					
					props.putValue(entry.getKey(), sl);
				} else if (entry.getValue() instanceof Integer[]) {
					final Integer[] arrayInt = (Integer[]) entry.getValue();
					final Integer32List il = Factory.Integer32List.createList();
					il.addAll(Arrays.asList(arrayInt));
					props.putValue(entry.getKey(), il);
				} else if (entry.getValue() instanceof List) {
					final List<Object> objectList = (List<Object>) entry.getValue();
					Object fl = null;
					Object obj = null;
					for (int i = 0; i < objectList.size(); i++) {
						obj = objectList.get(i);

						if (obj instanceof Integer || obj instanceof Long) {
							if (fl == null) {
								fl = Factory.Integer32List.createList();
							}

							((Integer32List) fl).add(obj);

							if (i == (objectList.size() - 1)) {
								props.putValue(entry.getKey(), (Integer32List) fl);
							}
						} else if (obj instanceof String) {
							if (fl == null) {
								fl = Factory.StringList.createList();
							}

							((StringList) fl).add(obj);

							if (i == (objectList.size() - 1)) {
								props.putValue(entry.getKey(), (StringList) fl);
							}
						}
					}
				} else {
					props.putObjectValue(entry.getKey(), entry.getValue());
				}
			}
		}
		if (saveDoc) {
			document.save(RefreshMode.REFRESH);
		}
	}

	/**
	 * Set del content alla classe Document FN.
	 *
	 * @param doc
	 *            - Document FN.
	 * @param fileName
	 *            - Nome del file.
	 * @param mimeType
	 *            - MimeType del file.
	 * @param content
	 *            - Content del documento da salvare.
	 * @param contentPreview
	 *            - Content della preview del content da salvare.
	 */
	private void setContentDoc(final Document doc, final String fileName, final String mimeType, final InputStream content, 
			final InputStream contentPreview) {
		setContentDoc(doc, fileName, mimeType, content, contentPreview, null);
	}

	
	private void setContentDoc(final Document doc, final String fileName, final String mimeType, final InputStream content, 
			final InputStream contentPreview, final InputStream contentLibroFirma) {
		ContentElementList list = Factory.ContentElement.createList();
		
		ContentTransfer element = Factory.ContentTransfer.createInstance();
		element.set_ContentType(mimeType);
		element.set_RetrievalName(fileName);
		element.setCaptureSource(content);

		list.add(element);
		doc.set_ContentElements(list);

		if (contentPreview != null) {
			creaESalvaAnnotation(doc.get_Name(), doc, FilenetAnnotationEnum.TEXT_PREVIEW, contentPreview, "PRE_" + doc.get_Name());
		}
		
		if (contentLibroFirma != null) {
			creaESalvaAnnotation(doc.get_Name(), doc, FilenetAnnotationEnum.CONTENT_LIBRO_FIRMA, contentLibroFirma, 
					FilenetAnnotationEnum.CONTENT_LIBRO_FIRMA.getNome() + doc.get_Name());
		}
	}

	private void setContentFromFile(final FileDTO file, final Document d) {
		try (
			InputStream is = new ByteArrayInputStream(file.getContent());
		) {
			setContentDoc(d, file.getFileName(), file.getMimeType(), is, null);
		} catch (final Exception e) {
			throw new FilenetException(" Errore durante la valorizzazione del content sul documento " + d.get_Id(), e);
		}
	}

	/**
	 * Inserisce una nuova versione ad un documento.
	 * 
	 * @param doc
	 *            - Document FN.
	 * @param stream
	 *            - Inputstream del content da salvare.
	 * @param metadati
	 *            - Metadati del Document FN da salvare.
	 * @param fileName
	 *            - Nome del file
	 * @param mimeType
	 *            - MimeType del file
	 * @return Document memeorizzato su cui è stato fatto il refresh.
	 */
	@Override
	public Document addVersion(final Document doc, final InputStream stream, final Map<String, Object> metadati, final String fileName, final String mimeType,
			final Long idAoo) {
		return addVersion(doc, stream, null, metadati, fileName, mimeType, idAoo);
	}

	/**
	 * Metodo che aggiorna i metadati del documento e ne ripristina la versione
	 * precedente.
	 * 
	 * @param document
	 *            documento
	 * @param metadati
	 *            metadati del documento
	 */
	@Override
	public void updateMetadataAndDeleteLastVersion(final Document document, final Map<String, Object> metadati) {
		try {
			// Si aggiornano dei metadati
			setPropertiesAndSaveDocument(document, metadati, true);

			// Si elimina l'ultima versione del documento
			eliminaUltimaVersione(document);
		} catch (final Exception e) {
			throw new FilenetException("Errore durante il recupero delle versioni del documento per l'eliminazione dell'ultima versione.", e);
		}
	}

	/**
	 * Inserisce una nuova versione ad un documento.
	 * 
	 * @param doc
	 *            - Document FN.
	 * @param stream
	 *            - Inputstream del content da salvare.
	 * @param preview
	 *            - Inputstream della preview del content da salvare.
	 * @param metadati
	 *            - Metadati del Document FN da salvare.
	 * @param fileName
	 *            - Nome del file
	 * @param mimeType
	 *            - MimeType del file
	 * @return Document memorizzato su cui è stato fatto il refresh.
	 */ 
	@Override 
	public Document addVersion(final Document doc, final InputStream stream, final InputStream preview,	final Map<String, Object> metadati, 
			final String fileName, final String mimeType, final Long idAoo) {
		return addVersion(doc, stream, preview, null, metadati, fileName, mimeType, idAoo);
	}
	
	
	/**
	 * Inserisce una nuova versione di un documento.
	 * 
	 * @param doc
	 *            - Document FN.
	 * @param stream
	 *            - Inputstream del content da salvare.
	 * @param preview
	 *            - Inputstream della preview del content da salvare.
	 * @param contentLibroFirma
	 *            - Inputstream del content da salvare da mostrare nel Libro Firma.
	 * @param metadati
	 *            - Metadati del Document FN da salvare.
	 * @param fileName
	 *            - Nome del file
	 * @param mimeType
	 *            - MimeType del file
	 * @return Document memorizzato su cui è stato fatto il refresh.
	 */
	@Override
	public Document addVersion(final Document doc, final InputStream stream, final InputStream preview,	final InputStream contentLibroFirma, 
			final Map<String, Object> metadati, final String fileName, final String mimeType, final Long idAoo) {
		Document docRet = null;
		
		try {
			checkout(doc);
			final Document docRes = (Document) doc.get_Reservation();
			checkin(docRes, stream, preview, contentLibroFirma, metadati, fileName, mimeType, idAoo);
			
			docRet = docRes;
		} catch (final Exception e) {
			LOGGER.error("Errore durante il checkin/checkout del documento con GUID: "	+ StringUtils.cleanGuidToString(doc.get_Id()), e);
			throw new FilenetException(e);
		}

		return docRet;
	}

	/**
	 * Restituisce se è possibile eseguire una firma PADES su un Document FN.
	 * 
	 * @param guid
	 *            - GUID del Document FN.
	 * @return Esito del controllo.
	 */
	@Override
	public Boolean canPades(final String guid) {
		Boolean output = true;
		final DocumentSet allegati = getAllegatiConContentDaFirmare(guid);
		final Iterator<?> it = allegati.iterator();
		while (it.hasNext()) {
			final Document docAllegato = (Document) it.next();
			String tipoMimeAllegato = null;
			if (FilenetCEHelper.hasDocumentContentTransfer(docAllegato)) {
				tipoMimeAllegato = FilenetCEHelper.getDocumentContentTransfer(docAllegato).get_ContentType();
			}
			if (StringUtils.isNullOrEmpty(tipoMimeAllegato) || !MediaType.PDF.toString().equalsIgnoreCase(tipoMimeAllegato)) {
				output = false;
			}
		}
		return output;
	}

	/**
	 * Ritorna il DocumentSet FN degli allegati (con content) da firmare di un
	 * documento.
	 * 
	 * @param guid
	 *            - GUID del documento principale
	 * @return DocumentSet degli allegati.
	 */
	@Override
	public final DocumentSet getAllegatiConContentDaFirmare(final String guid) {
		return getAllegati(guid, true, true);
	}

	@Override
	public final DocumentSet getAllegatiConContentDaFirmareOSiglare(final String guid) {
		return getAllegati(guid, true, true, null, null, null, true);
	}

	@Override
	public final DocumentSet allegatiTimbroProtocolloCheNonRientranoNeiFirmatiStampigliati(final String guid) {
		return allegatiTimbroProtocolloCheNonRientranoNeiFirmatiStampigliati(guid, true, null, null, null);
	}

	/**
	 * Ritorna il DocumentSet FN degli allegati (content incluso) NON da firmare di
	 * un documento.
	 * 
	 * @param guid
	 *            - GUID del documento principale
	 * @return DocumentSet degli allegati.
	 */
	@Override
	public final DocumentSet getAllegatiConContent(final String guid) {
		return getAllegati(guid, false, true);
	}

	/**
	 * Ritorna il DocumentSet FN degli allegati (content incluso) NON da firmare di
	 * un documento.
	 * 
	 * @param guid
	 *            - GUID del documento principale
	 * @param idAllegati
	 *            - Document Title degli allegati da recuperare
	 * @return DocumentSet degli allegati.
	 */
	@Override
	public final DocumentSet getAllegatiConContent(final String guid, final List<String> idAllegati) {
		return getAllegati(guid, false, true, null, idAllegati, null);
	}

	/**
	 * Ritorna il DocumentSet FN degli allegati di un documento.
	 * 
	 * @param guid
	 *            - GUID del documento principale
	 * @param bDaFirmare
	 *            - TRUE se solo allegati da firmare.
	 * @param bWithContent
	 *            - TRUE per recuperare solo gli allegati con content.
	 * @return DocumentSet degli allegati.
	 */
	@Override
	public DocumentSet getAllegati(final String guid, final Boolean bDaFirmare, final Boolean bWithContent) {
		return getAllegati(guid, bDaFirmare, bWithContent, null, null);
	}

	/**
	 * Ritorna il DocumentSet FN degli allegati di un documento.
	 * 
	 * @param guid
	 *            - GUID del documento principale
	 * @param bDaFirmare
	 *            - TRUE se solo allegati da firmare.
	 * @param bWithContent
	 *            - TRUE per recuperare solo gli allegati con content
	 * @return DocumentSet degli allegati.
	 */
	@Override
	public DocumentSet getAllegati(final String guid, final Boolean bDaFirmare, final Boolean bWithContent, final List<String> unsupportedMimeTypes,
			final List<String> idAllegati) {
		return getAllegati(guid, bDaFirmare, bWithContent, unsupportedMimeTypes, idAllegati, null);
	}
	
	 /**
	 * @param guid
	 * @param bDaFirmare
	 * @param bWithContent
	 * @param unsupportedMimeTypes
	 * @param idAllegati
	 * @param classNameExt
	 * @return
	 */
	@Override 
	public DocumentSet getAllegati(final String guid, final Boolean bDaFirmare, final Boolean bWithContent, final List<String> unsupportedMimeTypes, 
				final List<String> idAllegati, String classNameExt) {
		 return getAllegati(guid, bDaFirmare, bWithContent, unsupportedMimeTypes, idAllegati, classNameExt, null);
	 }

	/**
	 * @param guid
	 * @param bDaFirmare
	 * @param bWithContent
	 * @param unsupportedMimeTypes
	 * @param idAllegati
	 * @param classNameExt
	 * @param stampigliaturaSigla
	 * @return
	 */
	public DocumentSet getAllegati(final String guid, final Boolean bDaFirmare, final Boolean bWithContent, final List<String> unsupportedMimeTypes, 
			final List<String> idAllegati, final String classNameExt, final Boolean stampigliaturaSigla) {
		String daFirmareDaSiglareWhere = "";

		String strDaFirmareWhere = "";
		final String strDaFirmareSelect = "d." + getPP().getParameterByKey(PropertiesNameEnum.DA_FIRMARE_METAKEY) + ", ";
		if (bDaFirmare != null && bDaFirmare) {
			strDaFirmareWhere = "AND d." + getPP().getParameterByKey(PropertiesNameEnum.DA_FIRMARE_METAKEY) + " = 1 ";
		}

		String stampigliaturaSiglaWhere = "";
		if (stampigliaturaSigla != null && stampigliaturaSigla) {
			stampigliaturaSiglaWhere = "AND d." + getPP().getParameterByKey(PropertiesNameEnum.STAMPIGLIATURA_SIGLA_ALLEGATO) + UGUALE_TRUE;
		}
		final boolean firmaSigla = (bDaFirmare != null && bDaFirmare) && (stampigliaturaSigla != null && stampigliaturaSigla);
		if (firmaSigla) {
			strDaFirmareWhere = strDaFirmareWhere.replaceFirst("AND", "");
			stampigliaturaSiglaWhere = stampigliaturaSiglaWhere.replace("AND", "OR");
			daFirmareDaSiglareWhere = "AND (" + strDaFirmareWhere + stampigliaturaSiglaWhere + ")";
		}

		String guidWithoutBrachets = guid;
		if (guid.startsWith("{") && guid.endsWith("}")) {
			guidWithoutBrachets = guid.substring(1, guid.length() - 1);
		}

		String className = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY);
		String selectFields = strDaFirmareSelect + " d." + PropertyNames.ID + "," + " d." + PropertyNames.IS_RESERVED + "," + " d." + PropertyNames.RESERVATION + "," + " d."
				+ PropertyNames.CONTENT_ELEMENTS + "," + " d." + PropertyNames.MIME_TYPE + "," + " d." + PropertyNames.VERSION_SERIES + "," + " d."
				+ PropertyNames.DATE_CREATED + "," + " d." + PropertyNames.CONTENT_SIZE + "," + " d."
				+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.BARCODE_METAKEY) + "," + " d."
				+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY) + "," + " d."
				+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IS_FORMATO_ORIGINALE_METAKEY) + "," + " d."
				+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY) + "," + "d."
				+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FORMATO_ALLEGATO_ID_METAKEY) + "," + "d."
				+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY) + "," + " d."
				+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY) + "," + " d."
				+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IS_COPIA_CONFORME_METAKEY) + "," + " d."
				+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UFFICIO_COPIA_CONFORME_METAKEY) + "," + " d."
				+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UTENTE_COPIA_CONFORME_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ALLEGATO_IDFASCICOLOPROVENIENZA_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ALLEGATO_DESCRIZIONEFASCICOLOPROVENIENZA_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ALLEGATO_DOCUMENTTITLEDOCUMENTOPROVENIENZA_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ALLEGATO_DESCRIZIONEDOCUMENTOPROVENIENZA_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ALLEGATO_DOCUMENTTITLEALLEGATOPROVENIENZA_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ALLEGATO_NOMEFILEALLEGATOPROVENIENZA_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ALLEGATO_NON_SBUSTATO) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.STAMPIGLIATURA_SIGLA_ALLEGATO) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.IS_TIMBRO_USCITA_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.PLACEHOLDER_SIGLA_ALLEGATO) + "," + " d."
				// START VI
				+ getPP().getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_VALIDITA_FIRMA) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.FIRMA_VISIBILE_METAKEY);
		if (!StringUtils.isNullOrEmpty(classNameExt)) {
			className = classNameExt;
			selectFields = "d.*";
		}

		String stringSql = buildSearchQuery(bWithContent, unsupportedMimeTypes, idAllegati, guidWithoutBrachets, className, selectFields);

		SearchSQL sql = null;
		if (firmaSigla) {
			sql = new SearchSQL(stringSql + daFirmareDaSiglareWhere + ORDER_BY_D
					+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY) + " ASC");
		} else {
			sql = new SearchSQL(stringSql + strDaFirmareWhere + stampigliaturaSiglaWhere + ORDER_BY_D
					+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY) + " ASC");
		}
		final SearchScope searchScope = new SearchScope(getOs());

		return (DocumentSet) searchScope.fetchObjects(sql, 1, null, false);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#sonoPresentiAllegatiFirmatiNonVerificati(java.lang.String).
	 */
	@Override 
	public boolean sonoPresentiAllegatiFirmatiNonVerificati(String guid) {
		boolean sonoPresentiAllegatiFirmatiNonVerificati = false;

		final String  firmatoDigitalmente = " ( d."+getPP().getParameterByKey(PropertiesNameEnum.FORMATO_ALLEGATO_ID_METAKEY) + " = "+FormatoAllegatoEnum.FIRMATO_DIGITALMENTE.getId()+" )";
		final String 	nonVerificato = "( d."+getPP().getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_VALIDITA_FIRMA) + " = "+ValueMetadatoVerificaFirmaEnum.FIRMA_NON_VERIFICATA.getValue()+" )";
	
		final String className = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY);
		final String selectFields =  " d." + PropertyNames.ID+", d."+ PropertyNames.IS_CURRENT_VERSION+",d."+getPP().getParameterByKey(PropertiesNameEnum.FORMATO_ALLEGATO_ID_METAKEY)+",d."+getPP().getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_VALIDITA_FIRMA)   ; 
		String guidWithoutBrachets = guid;
		if (guid.startsWith("{") && guid.endsWith("}")) {
			guidWithoutBrachets = guid.substring(1, guid.length() - 1);
		}
	
		String stringSql = SELECT + selectFields
				+ FROM + className + " d " + INNER_JOIN_COMPONENT_RELATIONSHIP_R_ON_R_CHILD_COMPONENT_D_THIS + WHERE_D
				+ PropertyNames.IS_CURRENT_VERSION + UGUALE_TRUE + AND_R_PARENT_COMPONENT_OBJECT
				+ guidWithoutBrachets + "}') ";
		
		final SearchSQL sql = new SearchSQL(stringSql + AND+ firmatoDigitalmente+AND+nonVerificato);
		
		SearchScope searchScope = new SearchScope(getOs());
		DocumentSet ds = (DocumentSet) searchScope.fetchObjects(sql, 1, null, false);
		sonoPresentiAllegatiFirmatiNonVerificati = ds !=null && !ds.isEmpty();
		return sonoPresentiAllegatiFirmatiNonVerificati;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#allegatiTimbroProtocolloCheNonRientranoNeiFirmatiStampigliati(java.lang.String,
	 *      java.lang.Boolean, java.util.List, java.util.List, java.lang.String).
	 */
	@Override 
	public DocumentSet allegatiTimbroProtocolloCheNonRientranoNeiFirmatiStampigliati(final String guid, final Boolean bWithContent, final List<String> unsupportedMimeTypes, 
			final List<String> idAllegati, String classNameExt) {
		
		final String strDaFirmareWhere = " d." + getPP().getParameterByKey(PropertiesNameEnum.DA_FIRMARE_METAKEY) + " = 0 ";
		final String strDaFirmareSelect = "d." + getPP().getParameterByKey(PropertiesNameEnum.DA_FIRMARE_METAKEY) + ", ";
 		final String	stampigliaturaSiglaWhere = AND_D_TABLE + getPP().getParameterByKey(PropertiesNameEnum.STAMPIGLIATURA_SIGLA_ALLEGATO) + " = false ";
		
		final String  noFormatoOriginaleSiFirmatoDigitalmente = AND_D + getPP().getParameterByKey(PropertiesNameEnum.IS_FORMATO_ORIGINALE_METAKEY) + " = false "
				+ "OR d."+getPP().getParameterByKey(PropertiesNameEnum.FORMATO_ALLEGATO_ID_METAKEY) + " = "+FormatoAllegatoEnum.FIRMATO_DIGITALMENTE.getId()+" )";
		
		final String timbraturaUscitaWhere = AND_D_TABLE + getPP().getParameterByKey(PropertiesNameEnum.IS_TIMBRO_USCITA_METAKEY) + UGUALE_TRUE;
		
		final String daNONFirmareDaSiglareNOOriginaleWhere = "AND (" + strDaFirmareWhere + stampigliaturaSiglaWhere + noFormatoOriginaleSiFirmatoDigitalmente + timbraturaUscitaWhere + ")"; 
		
		String guidWithoutBrachets = guid;
		if (guid.startsWith("{") && guid.endsWith("}")) {
			guidWithoutBrachets = guid.substring(1, guid.length() - 1);
		}

		String className = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY);
		String selectFields = strDaFirmareSelect + " d." + PropertyNames.ID + "," + " d." + PropertyNames.IS_RESERVED + "," + " d." + PropertyNames.RESERVATION + "," + " d."
				+ PropertyNames.CONTENT_ELEMENTS + "," + " d." + PropertyNames.MIME_TYPE + "," + " d." + PropertyNames.VERSION_SERIES + "," + " d."
				+ PropertyNames.DATE_CREATED + "," + " d." + PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.BARCODE_METAKEY) + "," + " d."
				+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY) + "," + " d."
				+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IS_FORMATO_ORIGINALE_METAKEY) + "," + " d."
				+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY) + "," + "d."
				+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FORMATO_ALLEGATO_ID_METAKEY) + "," + "d."
				+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY) + "," + " d."
				+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY) + "," + " d."
				+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IS_COPIA_CONFORME_METAKEY) + "," + " d."
				+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UFFICIO_COPIA_CONFORME_METAKEY) + "," + " d."
				+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UTENTE_COPIA_CONFORME_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ALLEGATO_IDFASCICOLOPROVENIENZA_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ALLEGATO_DESCRIZIONEFASCICOLOPROVENIENZA_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ALLEGATO_DOCUMENTTITLEDOCUMENTOPROVENIENZA_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ALLEGATO_DESCRIZIONEDOCUMENTOPROVENIENZA_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ALLEGATO_DOCUMENTTITLEALLEGATOPROVENIENZA_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ALLEGATO_NOMEFILEALLEGATOPROVENIENZA_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ALLEGATO_NON_SBUSTATO) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.STAMPIGLIATURA_SIGLA_ALLEGATO) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.IS_TIMBRO_USCITA_METAKEY) + "," + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.PLACEHOLDER_SIGLA_ALLEGATO);
		if (!StringUtils.isNullOrEmpty(classNameExt)) {
			className = classNameExt;
			selectFields = "d.*";
		}
		
		String stringSql = buildSearchQuery(bWithContent, unsupportedMimeTypes, idAllegati, guidWithoutBrachets, className, selectFields);
		
		SearchSQL sql = null;
		sql = new SearchSQL(stringSql + daNONFirmareDaSiglareNOOriginaleWhere + ORDER_BY_D
				+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY) + " ASC");

		final SearchScope searchScope = new SearchScope(getOs());

		return (DocumentSet) searchScope.fetchObjects(sql, 1, null, false);
	}

	/**
	 * Costruisce la query a partire dai parametri.
	 * 
	 * @param bWithContent
	 *            Flag associato al content.
	 * @param unsupportedMimeTypes
	 *            Lista dei mime type non supportati.
	 * @param idAllegati
	 *            Identificativi degli allegati.
	 * @param guidWithoutBrachets
	 *            Guid tra parentesi graffe.
	 * @param className
	 *            Nome classe.
	 * @param selectFields
	 *            Campi per la costruzione della select.
	 * @return Query.
	 */
	private String buildSearchQuery(final Boolean bWithContent, final List<String> unsupportedMimeTypes,
			final List<String> idAllegati, String guidWithoutBrachets, String className, String selectFields) {
		
		StringBuilder stringSql = new StringBuilder(SELECT).append(selectFields).append(FROM).append(className).append(" d ")
				.append(INNER_JOIN_COMPONENT_RELATIONSHIP_R_ON_R_CHILD_COMPONENT_D_THIS).append(WHERE_D).append(PropertyNames.IS_CURRENT_VERSION)
				.append(UGUALE_TRUE).append(AND_R_PARENT_COMPONENT_OBJECT).append(guidWithoutBrachets).append("}') ");
		
		if (Boolean.TRUE.equals(bWithContent)) {
			stringSql.append(AND_D_TABLE).append(PropertyNames.CONTENT_SIZE).append(" > 0 ");
		}
		if (!CollectionUtils.isEmpty(unsupportedMimeTypes)) {
			stringSql.append(" AND NOT d.MimeType IN ('").append(org.apache.commons.lang3.StringUtils.join(unsupportedMimeTypes, "','")).append("')");
		}
		if (!CollectionUtils.isEmpty(idAllegati)) {
			stringSql.append(" AND d.DocumentTitle IN ('").append(org.apache.commons.lang3.StringUtils.join(idAllegati, "','")).append("')");
		}
		return stringSql.toString();
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getAllegatiAllegabili(it.ibm.red.business.dto.FascicoloDTO,
	 *      java.lang.String, boolean, java.lang.Long, boolean, boolean).
	 */
	@Override
	public Collection<DocumentoAllegabileDTO> getAllegatiAllegabili(final FascicoloDTO fascicolo, final String idDocumento, final boolean noContent, final Long idAoo,
			final boolean originale4DocEntrata, final boolean withContent) {

		final Document document = getDocumentByDTandAOO(idDocumento, idAoo);

		final Collection<DocumentoAllegabileDTO> allegati = new ArrayList<>();

		final ComponentRelationshipSet crs = document.get_ChildRelationships();
		if (crs != null && !crs.isEmpty()) {
			final Iterator<?> it = crs.iterator();
			while (it.hasNext()) {
				final ComponentRelationship cr = (ComponentRelationship) it.next();
				Document allegatoDocument = cr.get_ChildComponent();
				Document allegatoDocumentoFirstVersion = null;
				if (allegatoDocument == null) {
					continue;
				}

				if (allegatoDocument.get_VersionSeries() != null) {

					final SubSetImpl ssi = (SubSetImpl) allegatoDocument.get_VersionSeries().get_Versions();
					final List<?> list = ssi.getList();
					allegatoDocument = (Document) list.get(0);

					if (!FilenetCEHelper.hasDocumentContentTransfer(allegatoDocument)) {
						continue;
					}

					DocumentoAllegabileDTO allegato = DocumentoAllegabileDTOFactory.getDocumentoAllegabileDTO(allegatoDocument, document, fascicolo, withContent);

					if (list.size() > 1 && originale4DocEntrata) {
						// modifica il nome del documento con "_PROT" prima dell'estensione del file in
						// caso
						// sia necessario estrarre anche la prima versione del file (valido solo per doc
						// entrata)

						String nomeAllegato = allegato.getNomeFile();
						final String estensione = FilenameUtils.getExtension(nomeAllegato);
						if (!estensione.isEmpty()) {
							nomeAllegato = nomeAllegato.substring(0, nomeAllegato.lastIndexOf(".")) + "_PROT." + estensione;
						} else {
							nomeAllegato = nomeAllegato + "_PROT";
						}

						allegato.setNomeFile(nomeAllegato);

					}

					allegati.add(allegato);

					if (list.size() > 1 && originale4DocEntrata) {
						allegatoDocumentoFirstVersion = (Document) list.get(list.size() - 1);
						allegato = DocumentoAllegabileDTOFactory.getDocumentoAllegabileDTO(allegatoDocumentoFirstVersion, document, fascicolo, withContent);
						if (new BigDecimal(0).equals(allegato.getDimensione())) {
							continue;
						}
						allegati.add(allegato);
					}
				}
			}
		}

		return allegati;
	}

	/**
	 * Restituisce il numero di documenti allegati da firmare per un documento.
	 * 
	 * @param guid
	 *            - Identificativo FN del documento.
	 * @return Numero di documenti allegati da firmare.
	 */
	private int getCountAllegatiToSign(final String guid) {
		final SearchSQL sql = new SearchSQL(SELECT_D + PropertyNames.ID + " FROM Document d " + INNER_JOIN_COMPONENT_RELATIONSHIP_R_ON_R_CHILD_COMPONENT_D_THIS
				+ WHERE_D + PropertyNames.IS_CURRENT_VERSION + UGUALE_TRUE + " AND r.ParentComponent = OBJECT('" + guid + "')" + AND_D_TABLE + PropertyNames.CONTENT_SIZE
				+ " > 0" + AND_D_TABLE + PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DA_FIRMARE_METAKEY) + " = 1 ");
		final SearchScope searchScope = new SearchScope(getOs());
		final DocumentSet output = (DocumentSet) searchScope.fetchObjects(sql, 1, null, false);
		final Iterator<?> i = output.iterator();
		int count = 0;
		while (i.hasNext()) {
			i.next();
			count++;
		}
		return count;
	}

	/**
	 * Recuperare il DataHandler del content di un Document FN.
	 * 
	 * @param doc
	 *            - Document FN.
	 * @return DataHandler del content.
	 */
	public static DataHandler getDocumentContentAsDataHandler(final Document doc) {
		try {
			return FileUtils.toDataHandler(getDocumentContentAsInputStream(doc), doc.get_MimeType());
		} catch (final Exception e) {
			LOGGER.error("Errore durante la conversione del Document in DataHandler", e);
			throw new FilenetException(e);
		}
	}

	/**
	 * Recuperare il byte[] del content di un Document FN.
	 * 
	 * @param doc
	 *            - Document FN.
	 * @return byte[] del content.
	 */
	public static byte[] getDocumentContentAsByte(final Document doc) {
		try {
			return FileUtils.getByteFromInputStream(getDocumentContentAsInputStream(doc));
		} catch (final Exception e) {
			LOGGER.error("Errore durante la conversione del Document in byte array", e);
			throw new FilenetException(e);
		}
	}

	/**
	 * Recuperare l'InputStream del content di un Document FN.
	 * 
	 * @param doc
	 *            - Document FN.
	 * @return InputStream del content.
	 */
	public static InputStream getDocumentContentAsInputStream(final Document doc) {
		if (hasDocumentContentTransfer(doc)) {
			return getDocumentContentTransfer(doc).accessContentStream();
		}

		throw new RedException("Errore durante il recupero del content.");
	}

	/**
	 * Recuperare il ContentTransfer del content di un Document FN.
	 * 
	 * @param doc
	 *            - Document FN.
	 * @return ContentTransfer del content.
	 */
	public static ContentTransfer getDocumentContentTransfer(final Document doc) {
		return (ContentTransfer) doc.get_ContentElements().get(0);
	}

	/**
	 * Verifica se il Document presenta il content.
	 * 
	 * @param doc
	 *            - Documento da verificare
	 * @return Esito della verifica
	 */
	public static boolean hasDocumentContentTransfer(final Document doc) {
		boolean hasDocumentContentTransfer = false;

		try {
			if (doc != null && doc.get_ContentElements() != null && !doc.get_ContentElements().isEmpty()) {
				hasDocumentContentTransfer = true;
			}
		} catch (final EngineRuntimeException e) { // Property "ContentElements" non presente
			LOGGER.error("Errore nel recupero del content non presente: " + e);
			hasDocumentContentTransfer = false;
		}

		return hasDocumentContentTransfer;
	}

	/**
	 * Recupero del metadato per capire se è da firmare.
	 * 
	 * @param docAllegato
	 *            documento
	 * @return flag
	 */
	public static boolean isDaFirmare(final Document docAllegato) {
		final Integer daFirmare = (Integer) TrasformerCE.getMetadato(docAllegato, PropertiesNameEnum.DA_FIRMARE_METAKEY);
		return (daFirmare != null && daFirmare == 1);
	}

	/**
	 * Recupero del metadato per capire se è necessario stampigliare la sigla.
	 * 
	 * @param docAllegato
	 * @return
	 */
	public static boolean hasStampigliaturaSigla(final Document docAllegato) {
		return Boolean.TRUE.equals(TrasformerCE.getMetadato(docAllegato, PropertiesNameEnum.STAMPIGLIATURA_SIGLA_ALLEGATO));
	}

	/**
	 * Verifica se il Document relativo al documentTitle in input presenta il
	 * content.
	 * 
	 * @param documentTitle
	 *            del Documento da verificare
	 * @param idAoo
	 * @return Esito della verifica
	 */
	@Override
	public boolean hasDocumentContentTransfer(final String documentTitle, final int idAoo) {
		final Document doc = getDocumentByIdGestionale(documentTitle, null, null, idAoo, Constants.EMPTY_STRING, Constants.EMPTY_STRING);
		return hasDocumentContentTransfer(doc);
	}

	/**
	 * Recupera lo short name dell'AD Group.
	 * 
	 * @param idNodo
	 *            - Id dell'AOO.
	 * @return short name dell'AD Group
	 */
	@Override
	public String getADGroupNameFormId(final String idNodo) {
		final Realm realm = Factory.Realm.fetchCurrent(getOs().getConnection(), null);
		final String groupPrefix = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.SUFFISSO_GRUPPOSECURITY_KEY);
		if (groupPrefix == null) {
			throw new RedException("Property " + PropertiesNameEnum.SUFFISSO_GRUPPOSECURITY_KEY
					+ " non trovata, impossibile determinare il gruppo corrispondente all'ufficio con id " + idNodo);
		}
		final String[] groupPrefixies = groupPrefix.split("\\|");
		GroupSet gs = null;
		for (int i = 0; i < groupPrefixies.length && (gs == null || gs.isEmpty()); i++) {
			gs = realm.findGroups(groupPrefixies[i] + idNodo, PrincipalSearchType.EXACT, PrincipalSearchAttribute.SHORT_NAME, null, null, null);
		}
		if (gs == null || gs.isEmpty()) {
			throw new FilenetException("Nessun Gruppo trovato per " + ("nsd" + idNodo) + ", " + " VIRnsd" + idNodo + ", " + "VIRred" + idNodo + ", red" + idNodo);
		}
		return ((Group) gs.iterator().next()).get_ShortName();
	}

	/**
	 * Ricerca documento.
	 * 
	 * @param id
	 *            - Identificativo
	 * @param selectList
	 *            - Lista degli attributi da ricercare nella select.
	 * @param pf
	 *            - Properties FN da recuperare per il Document FN.
	 * @param idAoo
	 *            - Id dell'AOO.
	 * @param searchCondition
	 *            - Search condition
	 * @param inClassName
	 *            - Class name del documento.
	 * @return Document FN.
	 */
	@Override
	public Document getDocumentByIdGestionale(final String id, final List<String> selectList, final PropertyFilter pf, final Integer idAoo, final String searchCondition,
			final String inClassName) {

		String select = " * ";

		String className = inClassName;
		if (StringUtils.isNullOrEmpty(className)) {
			className = DOCUMENT;
		}

		if (selectList != null && !selectList.isEmpty()) {
			select = org.apache.commons.lang3.StringUtils.join(selectList, ",");
		}
		String sqlNonFaldoni = "";
		if (className != null && !className.equals(getPP().getParameterByKey(PropertiesNameEnum.FALDONE_CLASSNAME_FN_METAKEY))) {
			sqlNonFaldoni = " AND NOT IsClass(d," + getPP().getParameterByKey(PropertiesNameEnum.FALDONE_CLASSNAME_FN_METAKEY) + ")";
		}

		String sqlIdAOO = "";
		if (id != null && !(id.contains("@")) && idAoo != null) {
			sqlIdAOO = AND_D_TABLE + PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY) + " = " + idAoo;
		}

		String sql = SELECT + select + FROM + className + " d " + " WHERE NOT IsClass(d," + getPP().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY)
				+ ") " + (sqlNonFaldoni) + sqlIdAOO + AND_D_TABLE + getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY) + " = '" + id + "'" + AND_D_TABLE
				+ PropertyNames.IS_CURRENT_VERSION + UGUALE_TRUE;
		if (!StringUtils.isNullOrEmpty(searchCondition)) {
			sql += AND_PARENTESI_DX_AP + searchCondition + ")";
		}
		LOGGER.info("searching " + sql);
		final DocumentSet documents = (DocumentSet) new SearchScope(getOs()).fetchObjects(new SearchSQL(sql), 1, pf, false);
		if (!documents.isEmpty()) {
			return (Document) documents.iterator().next();
		}
		LOGGER.info("No results found.");
		return null;
	}

	/**
	 * Ricerca i fascicoli legati ad un documento.
	 * 
	 * @param documentTitle
	 *            - Document title del documento.
	 * @param idAoo
	 *            - Id dell'AOO.
	 * @return Lista dei DTO dei fascicoli trovati.
	 */
	@Override
	public List<FascicoloDTO> getFascicoliDocumento(final String documentTitle, final Integer idAoo) {
		List<FascicoloDTO> fascicoli = new ArrayList<>();
		try {
			final Document documento = getDocumentByIdGestionale(documentTitle, Arrays.asList(PropertyNames.FOLDERS_FILED_IN, PropertyNames.NAME), null, idAoo, null,
					PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));
			if (documento == null) {
				throw new RedException("Errore in fase di recupero del documento.");
			}

			fascicoli = getFascicoliFromDocumento(idAoo, documento);
			
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_FASCICOLI_MSG + documentTitle, e);
			throw new FilenetException(ERROR_RECUPERO_FASCICOLI_MSG + documentTitle + " : " + e.getMessage(), e);
		}

		return fascicoli;
	}

	/**
	 * Ricerca i fascicoli legati ad un documento.
	 * 
	 * @param documentTitle
	 *            - Document title del documento.
	 * @param idAoo
	 *            - Id dell'AOO.
	 * @return Lista dei DTO dei fascicoli trovati.
	 */
	@Override
	public List<FascicoloDTO> getFascicoliDocumentoPerMetodoAllacci(final String documentTitle, final Integer idAoo) {
		List<FascicoloDTO> fascicoli = new ArrayList<>();
		try {
			
			// Recupero documento
			final Document documento = getDocumentByIdGestionale(documentTitle, Arrays.asList(PropertyNames.FOLDERS_FILED_IN, PropertyNames.NAME), null, idAoo, null,
					PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));
			
			// Recupero fascicoli dal documento.
			if (documento != null) {
				fascicoli = getFascicoliFromDocumento(idAoo, documento);
			}

		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_FASCICOLI_MSG + documentTitle, e);
			throw new FilenetException(ERROR_RECUPERO_FASCICOLI_MSG + documentTitle + " : " + e.getMessage(), e);
		}

		return fascicoli;
	}

	/**
	 * Recupera i fascicoli a partire da un documento.
	 * 
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @param documento
	 *            Documento dal quale recuperare i fascicoli.
	 * @return Fascicoli recupearti, lista vuota se non ne vengono recuperati.
	 */
	private List<FascicoloDTO> getFascicoliFromDocumento(final Integer idAoo, final Document documento) {
		List<FascicoloDTO> fascicoli = new ArrayList<>();
		
		final FolderSet folders = documento.get_FoldersFiledIn();
		final Iterator<?> it = folders.iterator();
		while (it.hasNext()) {
			final Folder fascicoloFilenet = (Folder) it.next();
			LOGGER.info(GET_FASCICOLI_DOCUMENTO_LITERAL + fascicoloFilenet.get_FolderName() + DOCUMENTO_LITERAL + documento.get_Name());
			final Document docFascicolo = getFascicolo(fascicoloFilenet.get_FolderName(), idAoo);
			if (docFascicolo != null) {
				final FascicoloDTO fascicoloDTO = TrasformCE.transform(docFascicolo, TrasformerCEEnum.FROM_DOCUMENTO_TO_FASCICOLO);
				fascicoli.add(fascicoloDTO);
			}
		}
		
		return fascicoli;
	}
	
	/**
	 * Ricerca i fascicoli legati ad un documento.
	 * 
	 * @param docFilenet
	 *            - Documento di cui recuperare i fascicoli.
	 * @param idAoo
	 *            - ID dell'AOO.
	 * @return Lista dei DTO dei fascicoli trovati.
	 */
	@Override
	public final List<FascicoloDTO> getFascicoliDocumento(final Document docFilenet, final Integer idAoo) {
		final List<FascicoloDTO> fascicoli = new ArrayList<>();

		try {
			final FolderSet folders = docFilenet.get_FoldersFiledIn();
			final Iterator<?> it = folders.iterator();
			while (it.hasNext()) {
				final Folder fascicoloFilenet = (Folder) it.next();
				LOGGER.info(GET_FASCICOLI_DOCUMENTO_LITERAL + fascicoloFilenet.get_FolderName() + DOCUMENTO_LITERAL + docFilenet.get_Name());
				final Document docFascicolo = getFascicolo(fascicoloFilenet.get_FolderName(), idAoo);
				if (docFascicolo != null) {
					final FascicoloDTO fascicoloDTO = TrasformCE.transform(docFascicolo, TrasformerCEEnum.FROM_DOCUMENTO_TO_FASCICOLO);
					fascicoli.add(fascicoloDTO);
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei fascicoli del documento con GUID: " + docFilenet.get_Id(), e);
			throw new FilenetException("Errore durante il recupero dei fascicoli del documento con GUID: " + docFilenet.get_Id() + " : " + e.getMessage(), e);
		}

		return fascicoli;
	}

	/**
	 * Restituisce una lista di document grezzi che contengono i fascicoli.
	 * 
	 * @param foldersFiledIn
	 * @param idAoo
	 * @return
	 */
	@Override
	public List<Document> getFascicoliByFoldersFiledIn(final FolderSet foldersFiledIn, final Integer idAoo) {
		List<Document> fascicoli = null;

		try {
			if (foldersFiledIn.isEmpty()) {
				return fascicoli;
			}

			fascicoli = new ArrayList<>();
			final Iterator<?> it = foldersFiledIn.iterator();
			while (it.hasNext()) {
				final Folder fascicoloFilenet = (Folder) it.next();
				final Document docFascicolo = getFascicolo(fascicoloFilenet.get_FolderName(), idAoo);
				if (docFascicolo != null) {
					fascicoli.add(docFascicolo);
				}
			}

		} catch (final Exception e) {
			throw new FilenetException("Errore durante il recupero dei fascicoli: " + e.getMessage(), e);
		}

		return fascicoli;
	}

	/**
	 * Aggiornamento delle security di un fascicolo.
	 * 
	 * @param idFascicolo
	 *            - Identificativo del fascicolo
	 * @param idAoo
	 *            - Identificativo dell'AOO di appartenenza dell'utente che esegue
	 *            l'operazione
	 * @param securities
	 *            - Securities
	 */
	@Override
	public void updateFascicoloSecurity(final String idFascicolo, final Long idAoo, final List<SecurityDTO> securities) {
		updateFascicolo(idFascicolo, idAoo, securities, null, null);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#updateFascicoloSecurity(java.lang.String,
	 *      java.lang.Integer, com.filenet.api.collection.AccessPermissionList).
	 */
	@Override
	public void updateFascicoloSecurity(final String idFascicolo, final Integer idAoo, final AccessPermissionList acl) {
		try {
			boolean securityAggiornate = false;
			final UpdatingBatch ub = UpdatingBatch.createUpdatingBatchInstance(getOs().get_Domain(), RefreshMode.REFRESH);

			if (!CollectionUtils.isEmpty(acl)) {
				final Folder f = getFolderFascicolo(idFascicolo, idAoo);
				if (f == null) {
					throw new RedException(ERROR_RECUPERO_FASCICOLO_MSG);
				}
				f.get_Permissions().addAll(acl);
				securityAggiornate = true;
				ub.add(f, null);
			}

			if (securityAggiornate) {
				ub.updateBatch();
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new FilenetException(e);
		}
	}

	/**
	 * Aggiornamento dei metadati di un fascicolo.
	 * 
	 * @param idFascicolo
	 *            Identificativo fascicolo
	 * @param idAoo
	 *            Identificativo dell'AOO di appartenenza dell'utente che esegue
	 *            l'operazione
	 * @param classeDocumentale
	 *            Classe documentale
	 * @param metadatei
	 *            Metadati da aggiornare
	 */
	@Override
	public void updateFascicoloMetadati(final String idFascicolo, final Long idAoo, final String classeDocumentale, final Map<String, Object> metadati) {
		updateFascicolo(idFascicolo, idAoo, null, classeDocumentale, metadati);
	}

	/**
	 * Aggiornamento di un fascicolo.
	 * 
	 * @param idFascicolo
	 *            - Identificativo del fascicolo
	 * @param idAoo
	 *            - Identificativo dell'AOO di appartenenza dell'utente che esegue
	 *            l'operazione
	 * @param securities
	 *            - Securities
	 * @param classeDocumentale
	 *            - Nome della classe documentale del fascicolo
	 * @param metadati
	 *            - Metadati del fascicolo.
	 */
	@Override
	public void updateFascicolo(final String idFascicolo, final Long idAoo, final List<SecurityDTO> securities, final String classeDocumentale,
			final Map<String, Object> metadati) {
		try {
			int count = 0;
			final AccessPermissionList acl = Factory.AccessPermission.createList();
			final UpdatingBatch ub = UpdatingBatch.createUpdatingBatchInstance(getOs().get_Domain(), RefreshMode.REFRESH);
			Folder f = null;
			Document d = null;
			if (securities != null) {
				addSecurity(acl, securities, -1);
				f = getFolderFascicolo(idFascicolo, idAoo.intValue());
				if (f == null) {
					throw new RedException(ERROR_RECUPERO_FASCICOLO_MSG);
				}
				f.get_Permissions().addAll(acl);
				ub.add(f, null);
				count++;
			}
			if (classeDocumentale != null && metadati != null) {
				d = getFascicolo(idFascicolo, idAoo.intValue());
				if (d == null) {
					throw new RedException(ERROR_RECUPERO_FASCICOLO_MSG);
				}
				for (final Entry<String, Object> entry : metadati.entrySet()) {
					final String key = entry.getKey();
					final Object obj = metadati.get(key);
					d.getProperties().putObjectValue(key, obj);
				}
				ub.add(d, null);
				count++;
			}
			if (count > 0) {
				ub.updateBatch();
				if (f != null) {
					f.refresh();
				}
				if (d != null) {
					d.refresh();
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new FilenetException(e);
		}
	}

	/**
	 * Recupera la Folder FN dato l'identificativo.
	 * 
	 * @param idfascicolo
	 *            - Id del fascicolo.
	 * @param idAoo
	 *            - Id dell'AOO
	 * @return Folder FN
	 */
	@Override
	public Folder getFolderFascicolo(final String idfascicolo, final Integer idAoo) {
		Folder folder = null;
		String sql = SELECT + PropertyNames.ID + "," + " " + PropertyNames.SECURITY_FOLDER + FROM
				+ getPP().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY) + WHERE
				+ getPP().getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY) + " = '" + idfascicolo + "'" + AND + PropertyNames.IS_CURRENT_VERSION + UGUALE_TRUE;
		if (idAoo != null) {
			sql += AND + getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY) + " = " + idAoo;
		}
		final DocumentSet documents = (DocumentSet) new SearchScope(getOs()).fetchObjects(new SearchSQL(sql), 1, null, false);
		final Iterator<?> it = documents.iterator();
		if (it.hasNext()) {
			final Document doc = (Document) it.next();
			folder = doc.get_SecurityFolder();
		}

		return folder;
	}

	@SuppressWarnings("unchecked")
	private void setSecurity(final Document d, final ListSecurityDTO security) {
		final AccessPermissionList acl = Factory.AccessPermission.createList();
		acl.add(getAdminPermission());

		if (security == null || security.isEmpty()) {
			final AccessPermission perm = Factory.AccessPermission.createInstance();
			perm.set_AccessMask(AccessLevel.FULL_CONTROL_AS_INT);
			perm.set_GranteeName(AUTHENTICATED_USERS);
			perm.set_AccessType(AccessType.ALLOW);
			perm.set_InheritableDepth(0);
			perm.getProperties().putValue(GRANTEE_TYPE, SecurityPrincipalType.GROUP_AS_INT);
			acl.add(perm);
		} else {
			addSecurity(acl, security, FilenetPermissionEnum.NO_INHERITANCE.getInheritableDepth());
		}

		d.set_Permissions(acl);
	}

	/**
	 * Aggiunge delle security alle ACL.
	 * 
	 * @param acl
	 * @param securities
	 * @param inheritableDepth
	 */
	@Override
	public void addSecurity(final AccessPermissionList acl, final List<SecurityDTO> securities, final int inheritableDepth) {
		try {
			if (!CollectionUtils.isEmpty(securities)) {
				for (final SecurityDTO s : securities) {
					final GruppoS gruppo = s.getGruppo();
					final UtenteS utente = s.getUtente();
					String granteeName;
					int securityType = SecurityPrincipalType.GROUP_AS_INT;

					if (utente != null && utente.getIdUtente() > 0) {
						granteeName = getADUsernameName(utente.getUsername());
						securityType = SecurityPrincipalType.USER_AS_INT;
					} else {
						granteeName = getADGroupNameFormId(Integer.toString(gruppo.getIdUfficio()));
					}

					final AccessPermission perm = Factory.AccessPermission.createInstance();
					perm.set_AccessMask(AccessLevel.FULL_CONTROL_AS_INT);
					perm.set_GranteeName(granteeName);
					perm.set_AccessType(AccessType.ALLOW);
					perm.getProperties().putValue(GRANTEE_TYPE, securityType);
					if (inheritableDepth != 0) {
						perm.set_InheritableDepth(inheritableDepth);
					} else {
						perm.set_InheritableDepth(0);
					}
					acl.add(perm);
				}
			}
		} catch (final Exception e) {
			throw new FilenetException(e);
		}
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getAdminPermission().
	 */
	@Override
	public AccessPermission getAdminPermission() {
		final AccessPermission perm = Factory.AccessPermission.createInstance();
		perm.set_AccessMask(AccessLevel.FULL_CONTROL_AS_INT);
		perm.set_GranteeName("P8Admin");
		perm.set_AccessType(AccessType.ALLOW);
		perm.set_InheritableDepth(0);
		perm.getProperties().putValue(GRANTEE_TYPE, SecurityPrincipalType.USER_AS_INT);

		return perm;
	}

	/**
	 * Metodo per il passaggio da guid ad id.
	 * 
	 * @param guid
	 *            guid del documento
	 * @return id del documento
	 */
	@Override
	public Id idFromGuid(final String guid) {
		return new Id(guid);
	}

	/**
	 * Recupera gli allegati di un documento tramite ID FN.
	 * 
	 * @param id
	 *            - Id FN del documento.
	 * @param selectList
	 *            - Lista degli attributi in select
	 * @return DocumentSet FN degli allegati.
	 */
	@Override
	public DocumentSet getAllegatiByGuid(final Id id, final List<String> selectList) {
		return getChildsByGuid(id, selectList, getPP().getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY), 1, null);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getChildsByGuid(com.filenet.api.util.Id,
	 *      java.util.List, java.lang.String, java.lang.Integer, java.lang.String).
	 */
	@Override
	public DocumentSet getChildsByGuid(final Id id, final List<String> selectList, final String inClassName, final Integer pageSize, final String inFilter) {
		StringBuilder select = new StringBuilder("");

		if (selectList != null && !selectList.isEmpty()) {
			for (final String idToJoin : selectList) {
				select.append(" d.").append(idToJoin).append(",");
			}
			select = new StringBuilder(select.substring(0, select.length() - 1));
		} else {
			select = new StringBuilder(D_STAR);
		}

		String filter = Constants.EMPTY_STRING;
		if (inFilter != null) {
			filter = inFilter;
		}

		String className = DOCUMENT;
		if (inClassName != null) {
			className = inClassName;
		}

		final String sqlString = SELECT + select + FROM + className + " d " + " INNER JOIN ComponentRelationship r on  r.ChildComponent=[d].This"
				+ " WHERE [r].ParentComponent = OBJECT(" + id + ")" + filter;

		LOGGER.info("searching childs " + sqlString);

		final SearchScope searchScope = new SearchScope(getOs());
		final SearchSQL sql = new SearchSQL(sqlString);

		final DocumentSet documents = (DocumentSet) searchScope.fetchObjects(sql, pageSize, null, false);
		if (!documents.isEmpty()) {
			return documents;
		}

		return null;
	}

	/**
	 * Esegue l'esecuzione di un batch FN.
	 * 
	 * @param objects
	 *            - Oggetti persistenti.
	 */
	@Override
	public void batchSave(final List<IndependentlyPersistableObject> objects) {
		try {
			final UpdatingBatch ub = UpdatingBatch.createUpdatingBatchInstance(getOs().get_Domain(), RefreshMode.REFRESH);
			for (final IndependentlyPersistableObject obj : objects) {
				ub.add(obj, null);
			}
			ub.updateBatch();
		} catch (final EngineRuntimeException ere) {
			LOGGER.error(ere.getMessage(), ere);
			throw new FilenetException(ere);
		}
	}

	/**
	 * Recuperera l'AD by Username.
	 * 
	 * @param inUsername
	 *            - Username
	 * @return AD
	 */
	@Override
	public String getADUsernameName(final String inUsername) {
		final String username = getTruncUsername(inUsername);
		final Realm realm = Factory.Realm.fetchCurrent(getOs().getConnection(), null);
		final UserSet us = realm.findUsers(username, PrincipalSearchType.EXACT, PrincipalSearchAttribute.SHORT_NAME, null, null, null);
		if (us.isEmpty()) {
			throw new FilenetException("Nessun utente trovato per lo username " + username + " username non troncato: " + inUsername);
		}

		return ((User) us.iterator().next()).get_ShortName();
	}

	/**
	 * Tronca la lunghezza della username.
	 * 
	 * @param username
	 *            - Username
	 * @return Username troncata.
	 */
	private static String getTruncUsername(final String username) {
		String truncUsername;
		if (username != null && username.length() > MAX_LENGTH_USERNAME) {
			truncUsername = username.substring(0, MAX_LENGTH_USERNAME);
		} else {
			truncUsername = username;
		}
		return truncUsername;
	}

	/**
	 * Recupero documento da identificativo per modifica iter.
	 * 
	 * @param idDocumento
	 *            identificativo documento
	 * @return il documento contenente i metadati necessari all'esecuzione della
	 *         modifica dell'iter
	 */
	@Override
	public Document getDocumentForModificaIter(final Integer idDocumento) {
		final String selectDetail = getSelectCommonPropsDocument() + ", d." + PropertyNames.CONTENT_ELEMENTS + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY) + ", d." + getPP().getParameterByKey(PropertiesNameEnum.ID_FORMATO_DOCUMENTO)
				+ ", d." + getPP().getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY) + ", d." + getPP().getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY)
				+ ", d." + getPP().getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY) + ", d." + getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY)
				+ ", d." + getPP().getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY) + ", d."
				+ getPP().getParameterByKey(PropertiesNameEnum.DESTINATARIO_CONTRIBUTO_ESTERNO) + ", d." + getPP().getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY)
				+ ", d." + getPP().getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
		final Collection<String> ids = new ArrayList<>();
		ids.add(idDocumento.toString());
		final DocumentSet ds = getDocumentBase(selectDetail, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), ids);
		return getFirstDocument(ds);
	}

	/**
	 * Metodo per l'aggiornamento dei metadati.
	 * 
	 * @param guid
	 *            Identificativo del documento.
	 * @param metadati
	 *            metadati
	 */
	@Override
	public void updateMetadati(final String guid, final Map<String, Object> metadati) {
		final Document doc = getDocumentByGuid(guid);
		setPropertiesAndSaveDocument(doc, metadati, true);
	}

	/**
	 * Metodo per l'aggiornamento dei metadati.
	 * 
	 * @param doc
	 *            documento da aggiornare
	 * @param metadati
	 *            metadati
	 */
	@Override
	public void updateMetadati(final Document doc, final Map<String, Object> metadati) {
		setPropertiesAndSaveDocument(doc, metadati, true);
	}

	/**
	 * Metodo che aggiorna i metadati del documento senza creare una nuova versione
	 * del documento.
	 * 
	 * @param idAoo
	 * @param idDocumento
	 * @param metadati
	 */
	@Override
	public void updateMetadatiOnCurrentVersion(final Long idAoo, final Integer idDocumento, final Map<String, Object> metadati) {
		final Document docFilenet = getDocumentByIdGestionale(idDocumento.toString(), null, null, idAoo.intValue(), null, null);

		if (docFilenet != null) {
			updateMetadati(docFilenet, metadati);
		} else {
			final String strErrorMsg = "Errore in fase di recupero del documento: " + idDocumento + " per l'AOO: " + idAoo;
			LOGGER.error(strErrorMsg);
			throw new FilenetException(strErrorMsg);
		}
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#controllaBarcode(java.lang.String).
	 */
	@Override
	public String controllaBarcode(final String barcode) {
		return controllaBarcode(barcode, null);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#controllaBarcode(java.lang.String,
	 *      java.lang.String).
	 */
	@Override
	public String controllaBarcode(final String barcode, final String className) {
		if (!StringUtils.isNullOrEmpty(barcode)) {
			String queryClassName = getPP().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY);
			if (!StringUtils.isNullOrEmpty(className)) {
				queryClassName = className;
			}

			final StringBuilder query = new StringBuilder();
			query.append("SELECT id, barcode FROM ").append(queryClassName).append(WHERE_IS_CURRENT_VERSION_TRUE).append(" AND barcode = '").append(barcode).append("'");

			final SearchSQL sql = new SearchSQL(query.toString());
			LOGGER.info(QUERY_DA_ESEGUIRE + sql.toString());

			final SearchScope searchScope = new SearchScope(getOs());
			final DocumentSet documents = (DocumentSet) searchScope.fetchObjects(sql, null, null, false);
			final Iterator<?> it = documents.iterator();

			String propBarcode;
			Document doc = null;
			if (it.hasNext()) {
				doc = (Document) it.next();
				final Properties prop = doc.getProperties();
				propBarcode = prop.getStringValue(getPP().getParameterByKey(PropertiesNameEnum.BARCODE_METAKEY));

				if (!StringUtils.isNullOrEmpty(propBarcode)) {
					LOGGER.info("Barcode trovato.");
					return doc.get_Id().toString();
				}
			}
		}

		return null;
	}

	/**
	 * Aggiorna il metadato firmaPDF che guida la tipologia di firma Pades/Cades
	 * 
	 * @param guid
	 */
	@Override
	public void updateFirmaPDF(final String guid) {
		Boolean firmaPDF = true;
		final Document documentoPrincipale = getDocumentByGuid(guid);
		if (!documentoPrincipale.get_MimeType().endsWith("pdf")) {
			firmaPDF = false;
		} else {
			final DocumentSet allegati = getAllegati(guid, true, true);
			final Iterator<?> i = allegati.iterator();
			while (i.hasNext()) {
				final Document all = (Document) i.next();
				if (!all.get_MimeType().endsWith("pdf")) {
					firmaPDF = false;
					break;
				}
			}
		}

		final HashMap<String, Object> metadati = new HashMap<>();
		metadati.put(getPP().getParameterByKey(PropertiesNameEnum.FIRMA_PDF_METAKEY), firmaPDF);

		// aggiorno su filenet il valore del metadato firmaPDF associato al documento
		updateMetadati(documentoPrincipale, metadati);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getFaldoneByFascicoloGuid(java.lang.String,
	 *      java.lang.Integer).
	 */
	@Override
	public Document getFaldoneByFascicoloGuid(final String fascicoloGuid, final Integer idAoo) {
		final String sql = SELECT_D + PropertyNames.ID + ", " + D + PropertyNames.NAME + ", " + D
				+ getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_DOCUMENTTITLE) + ", " + D + PropertyNames.CREATOR + ", " + D
				+ PropertyNames.DATE_CREATED + ", " + D + PropertyNames.OWNER + ", " + D
				+ getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_NOMEFALDONE) + ", " + D
				+ getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_PARENTFALDONE) + ", " + D
				+ getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_DESCRIZIONEFALDONE) + ", " + D
				+ getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_IDAOO) + ", " + D
				+ getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_OGGETTO) + ", " + "       d.SecurityFolder, " + D + PropertyNames.IS_RESERVED + " "
				+ "  FROM " + getPP().getParameterByKey(PropertiesNameEnum.FALDONE_CLASSNAME_FN_METAKEY) + " d " + " INNER JOIN ReferentialContainmentRelationship r "
				+ "    ON d.This = r.Head " + WHERE_D + getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_IDAOO) + " = " + idAoo + " "
				+ "   AND d.SecurityFolder = r.Tail " + "   AND r.Tail = OBJECT('{" + fascicoloGuid + "}') ";

		final SearchSQL searchSQL = new SearchSQL(sql);
		final SearchScope scope = new SearchScope(getOs());
		final Boolean continuable = Boolean.valueOf(true);
		final DocumentSet ds = (DocumentSet) scope.fetchObjects(searchSQL, PAGE_SIZE, null, continuable);
		if (ds.isEmpty()) {
			return null;
		}
		return getFirstDocument(ds);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#creaDocumento(it.ibm.red.business.dto.filenet.DocumentoRedFnDTO,
	 *      it.ibm.red.business.dto.filenet.FascicoloRedFnDTO, java.lang.String,
	 *      java.lang.Long).
	 */
	@Override
	public Document creaDocumento(final DocumentoRedFnDTO docRedFn, final FascicoloRedFnDTO fascicoloRedFn, final String guidMail, final Long idAoo) {
		LOGGER.info("creaDocumento -> START");
		final String documentTitle = docRedFn.getDocumentTitle();
		final UpdatingBatch ub = UpdatingBatch.createUpdatingBatchInstance(getOs().get_Domain(), RefreshMode.REFRESH);

		Document docFilenet = null;
		try {
			// Impostazione del contenuto (se presente), dei metadati e delle security
			docFilenet = costruisciDocumentoFilenet(docRedFn, idAoo);

			if (!CollectionUtils.isEmpty(docRedFn.getAllegati())) {
				docFilenet.set_CompoundDocumentState(CompoundDocumentState.COMPOUND_DOCUMENT);

				// Check-in del documento
				docFilenet.checkin(AutoClassify.DO_NOT_AUTO_CLASSIFY, CheckinType.MAJOR_VERSION);
				ub.add(docFilenet, null);

				// Impostazione e check-in degli allegati
				aggiungiAllegati(docFilenet, docRedFn.getSecurity(), docRedFn.getAllegati(), ub, idAoo);
				LOGGER.info("creaDocumento -> Saranno creati " + docRedFn.getAllegati().size() + " allegati per il documento: " + documentTitle);
			} else {
				// Check-in del documento
				docFilenet.checkin(AutoClassify.DO_NOT_AUTO_CLASSIFY, CheckinType.MAJOR_VERSION);
				ub.add(docFilenet, null);
			}

			// Se è specificato il GUID dell'e-mail, si crea la relativa Annotation
			// TEXT_MAIL e la si collega al documento
			if (guidMail != null) {
				final Integer elementSequenceNumber = hasDocumentContentTransfer(docFilenet) ? 0 : null;
				ub.add(costruisciAnnotationFilenet(docFilenet, FilenetAnnotationEnum.TEXT_MAIL, new ByteArrayInputStream(guidMail.getBytes()), getOs(), elementSequenceNumber,
						null, null), null);
				LOGGER.info("creaDocumento -> Sarà creata l'annotation: " + FilenetAnnotationEnum.TEXT_MAIL.getNome() + PER_IL_DOCUMENTO + documentTitle);
			}

			// Crea un nuovo fascicolo e fascicola al suo interno il documento
			if (fascicoloRedFn != null && fascicoloRedFn.getClasseDocumentale() != null) {
				final DocumentoFascicolazioneRedFnDTO docFascicolazione = new DocumentoFascicolazioneRedFnDTO();
				docFascicolazione.setDocumentTitle(documentTitle);
				creaFascicolo(fascicoloRedFn, docFascicolazione, docFilenet, ub);
				LOGGER.info("creaDocumento -> Sarà creato il nuovo fascicolo: " + fascicoloRedFn.getIdFascicolo() + PER_IL_DOCUMENTO + documentTitle);
			}
			// Fascicola il documento in un fascicolo esistente
			if (fascicoloRedFn != null && fascicoloRedFn.getClasseDocumentale() == null) {
				fascicolaDocumento(docFilenet, fascicoloRedFn, (Long) docRedFn.getMetadati().get(getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY)), ub);
			}

			LOGGER.info("creaDocumento -> Si richiama l'updateBatch per la creazione del documento e l'esecuzione delle operazioni correlate su FileNet...");
			ub.updateBatch();
			LOGGER.info("creaDocumento -> Le operazioni sono state eseguite correttamente. Document Title: " + documentTitle + ". GUID: " + docFilenet.get_Id().toString());
		} catch (final Exception e) {
			LOGGER.error("creaDocumento -> Errore durante la creazione su FileNet del documento con Document Title: " + documentTitle, e);
			throw new FilenetException(e);
		}

		LOGGER.info("creaDocumento -> END");
		return docFilenet;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#creaFascicolo(it.ibm.red.business.dto.filenet.FascicoloRedFnDTO,
	 *      it.ibm.red.business.dto.filenet.DocumentoFascicolazioneRedFnDTO,
	 *      com.filenet.api.core.Document, com.filenet.api.core.UpdatingBatch).
	 */
	@Override
	public final void creaFascicolo(final FascicoloRedFnDTO fascicolo, final DocumentoFascicolazioneRedFnDTO docFascicolazione, final Document docFilenet,
			final UpdatingBatch inUb) {
		LOGGER.info("creaFascicolo -> START");
		try {
			UpdatingBatch ub = inUb;
			final boolean commit = (ub == null);

			if (ub == null) {
				ub = UpdatingBatch.createUpdatingBatchInstance(getOs().get_Domain(), RefreshMode.REFRESH);
			}

			// Creazione fascicolo
			final Document fascicoloFilenet = Factory.Document.createInstance(getOs(), getPP().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY));

			// Recupero e impostazione dei metadati del fascicolo
			final Map<String, Object> properties = fascicolo.getMetadati();
			if (!properties.isEmpty()) {
				setPropertiesAndSaveDocument(fascicoloFilenet, properties, false);
			}

			// Check-in del fascicolo
			fascicoloFilenet.checkin(AutoClassify.DO_NOT_AUTO_CLASSIFY, CheckinType.MAJOR_VERSION);
			ub.add(fascicoloFilenet, null);

			// Creazione del folder del fascicolo
			final Folder folderFascicolo = aggiungiFolderFascicolo(fascicoloFilenet, fascicolo.getSecurity());
			ub.add(folderFascicolo, null);

			// Inserimento del fascicolo nel folder del fascicolo
			final ReferentialContainmentRelationship relazioneFascicoloFolderFascicolo = folderFascicolo.file(fascicoloFilenet, AutoUniqueName.NOT_AUTO_UNIQUE,
					(String) properties.get(getPP().getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY)), DefineSecurityParentage.DEFINE_SECURITY_PARENTAGE);
			ub.add(relazioneFascicoloFolderFascicolo, null);

			// Inserimento del fascicolo nel folder del titolario
			Folder folderTitolario = null;
			final String titolario = (String) properties.get(getPP().getParameterByKey(PropertiesNameEnum.TITOLARIO_METAKEY));
			if (!StringUtils.isNullOrEmpty(titolario) && !FilenetStatoFascicoloEnum.NON_CATALOGATO.getNome().equals(titolario)) {
				folderTitolario = getFolderTitolario(titolario);
				if (folderTitolario == null) {
					throw new FilenetException(ERROR_FOLDER_TITOLARIO_ASSENTE_MSG);
				}
				final ReferentialContainmentRelationship relazioneFascicoloFolderTitolario = folderTitolario.file(fascicoloFilenet, AutoUniqueName.NOT_AUTO_UNIQUE,
						(String) properties.get(getPP().getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY)),
						DefineSecurityParentage.DO_NOT_DEFINE_SECURITY_PARENTAGE);
				ub.add(relazioneFascicoloFolderTitolario, null);
			}

			// Se il documento è presente, lo si inserisce nel fascicolo
			if (docFilenet != null) {
				final ReferentialContainmentRelationship relazioneDocumentoFolderFascicolo = folderFascicolo.file(docFilenet, AutoUniqueName.NOT_AUTO_UNIQUE,
						docFascicolazione.getDocumentTitle(), DefineSecurityParentage.DO_NOT_DEFINE_SECURITY_PARENTAGE);
				ub.add(relazioneDocumentoFolderFascicolo, null);
			} else if (docFascicolazione != null) {
				ub.add(creaRCRFascicolazioneDocumento(docFascicolazione, folderFascicolo, (Long) properties.get(getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY))),
						null);
			}

			if (commit) {
				LOGGER.info("creaFascicolo -> Si richiama l'updateBatch per la creazione del fascicolo...");
				ub.updateBatch();
				LOGGER.info("creaFascicolo -> Chiamata a updateBatch eseguita con successo. GUID del fascicolo creato: " + fascicoloFilenet.get_Id().toString());
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la creazione su FileNet del fascicolo: " + fascicolo.getIdFascicolo(), e);
			throw new FilenetException("Errore durante la creazione su FileNet del fascicolo: " + fascicolo.getIdFascicolo(), e);
		}

		LOGGER.info("creaFascicolo -> END");
	}

	private Folder aggiungiFolderFascicolo(final Document fascicolo, final ListSecurityDTO security) {
		final String parentFolder = getRootFolderFascicoli(fascicolo);
		return aggiungiFolder(fascicolo.getProperties().getStringValue(getPP().getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY)), security, parentFolder);
	}

	/**
	 * @param fascicolo
	 *            Fascicolo
	 * @return path composto da
	 *         {@code config.getProperty(PropertiesConstants.FOLDER_FASCICOLI) + "/AOO_" + d.getProperties().getObjectValue("idAOO").toString()}
	 */
	private String getRootFolderFascicoli(final Document fascicolo) {
		return getPP().getParameterByKey(PropertiesNameEnum.FOLDER_FASCICOLI_FN_METAKEY) + Constants.Varie.AOO_PREFIX
				+ fascicolo.getProperties().getObjectValue("idAOO").toString();
	}

	private Folder aggiungiFolder(final String folderName, final ListSecurityDTO security, final String parentFolder) {
		final AccessPermissionList acl = Factory.AccessPermission.createList();
		acl.add(getAdminPermission());

		if (security != null && !security.isEmpty()) {
			addSecurity(acl, security, FilenetPermissionEnum.OBJECT_AND_IMMEDIATE_CHILDREN.getInheritableDepth());
		}

		final Folder folder = Factory.Folder.createInstance(getOs(), null);
		folder.set_Parent(getFolder(parentFolder));
		folder.set_FolderName(folderName);
		folder.set_Permissions(acl);
		LOGGER.info("Inserimento nuovo folder. Path: " + parentFolder + FOLDER_PATH_DELIMITER + folderName);

		return folder;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#eliminaFascicoloSeVuoto(java.lang.String,
	 *      java.lang.Long).
	 */
	@Override
	public boolean eliminaFascicoloSeVuoto(final String idFascicolo, final Long idAoo) {
		LOGGER.info("eliminaFascicoloSeVuoto -> START. Fascicolo: " + idFascicolo);
		boolean fascicoloEliminato = false;

		final Folder folderFascicolo = getFolderFascicolo(idFascicolo, idAoo.intValue());

		if (folderFascicolo != null) {
			final Document doc = isFascicoloEliminabile(folderFascicolo);
			if (doc != null) {
				// Si elimina prima il documento FileNet del fascicolo con tutti i riferimenti
				doc.delete();
				doc.save(RefreshMode.REFRESH);
				// Si elimina il folder FileNet del fascicolo
				folderFascicolo.delete();
				folderFascicolo.save(RefreshMode.REFRESH);
				fascicoloEliminato = true;
				LOGGER.info("eliminaFascicoloSeVuoto -> Il fascicolo: " + idFascicolo + " è stato eliminato.");
			} else {
				LOGGER.info("eliminaFascicoloSeVuoto -> Impossibile eliminare il fascicolo: " + idFascicolo + " in quanto contiene dei documenti.");
			}
		} else {
			LOGGER.info("eliminaFascicoloSeVuoto -> Impossibile eliminare il fascicolo: " + idFascicolo + " in quanto non è stato trovato.");
		}

		LOGGER.info("eliminaFascicoloSeVuoto -> END. Fascicolo: " + idFascicolo);
		return fascicoloEliminato;
	}

	private Document isFascicoloEliminabile(final Folder folderFascicolo) {
		Document docFascicolo = null;

		// N.B. Il primo documento del folder è quello corrispondente al fascicolo
		if (folderFascicolo != null) {
			final Iterator<?> it = folderFascicolo.get_ContainedDocuments().iterator();
			int count = 0;
			while (it.hasNext()) {
				count++;
				// Se il folder contiene almeno un altro documento oltre a quello del fascicolo,
				// allora non è possibile eliminarlo in quanto contiene ancora documenti
				if (count > 1) {
					docFascicolo = null;
					break;
				}
				docFascicolo = ((Document) it.next());
			}
		}

		return docFascicolo;
	}

	private void fascicolaDocumento(final Document documento, final FascicoloRedFnDTO fascicolo, final Long idAoo, final UpdatingBatch ub) {
		final ReferentialContainmentRelationship refContRel = creaRCRFascicolazioneDocumento(documento, fascicolo, idAoo);
		if (ub == null) {
			refContRel.save(RefreshMode.REFRESH);
		} else {
			ub.add(refContRel, null);
		}
	}

	private ReferentialContainmentRelationship creaRCRFascicolazioneDocumento(final DocumentoFascicolazioneRedFnDTO docFascicolazione, final Folder folderFascicolo,
			final Long idAoo) {
		final Document doc = getDocumentByIdGestionale(docFascicolazione.getDocumentTitle(),
				Arrays.asList(PropertyNames.ID, getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY)), null, idAoo.intValue(), null, null);

		final DefineSecurityParentage securityParentage = docFascicolazione.isEreditaSecurity() ? DefineSecurityParentage.DEFINE_SECURITY_PARENTAGE
				: DefineSecurityParentage.DO_NOT_DEFINE_SECURITY_PARENTAGE;

		return creaRCRFascicolazioneDocumento(doc, folderFascicolo, securityParentage);
	}

	private ReferentialContainmentRelationship creaRCRFascicolazioneDocumento(final Document doc, final FascicoloRedFnDTO fascicolo, final Long idAoo) {
		final Folder folderFascicolo = getFolderFascicolo(fascicolo.getIdFascicolo(), idAoo.intValue());
		return creaRCRFascicolazioneDocumento(doc, folderFascicolo, DefineSecurityParentage.DO_NOT_DEFINE_SECURITY_PARENTAGE);
	}

	private ReferentialContainmentRelationship creaRCRFascicolazioneDocumento(final Document doc, final Folder folderFascicolo,
			final DefineSecurityParentage securityParentage) {

		if (folderFascicolo == null) {
			throw new RedException(ERROR_FOLDER_FASCICOLO_ASSENTE_MSG);
		}

		return folderFascicolo.file(doc, AutoUniqueName.AUTO_UNIQUE, doc.getProperties().getStringValue(getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY)),
				securityParentage);
	}

	/**
	 * Il nome del titolario è formato da idaoo_vocetitolario (es. 25_H)
	 */
	private Folder getFolderTitolario(final String nomeTitolario) {
		final Folder folder = null;

		final String classeDocumentaleTitolario = getPP().getParameterByKey(PropertiesNameEnum.TITOLARIO_CLASSNAME_FN_METAKEY);
		final StringBuilder query = new StringBuilder().append("SELECT ID, FoldersFiledIn FROM ").append(classeDocumentaleTitolario).append(" WHERE nomeTitolario = '")
				.append(nomeTitolario).append(AND_IS_CURRENT_VERSION_TRUE);
		final SearchSQL sql = new SearchSQL(query.toString());

		final SearchScope searchScope = new SearchScope(getOs());
		final DocumentSet documents = (DocumentSet) searchScope.fetchObjects(sql, 1, null, false);
		final Iterator<?> it = documents.iterator();

		if (it.hasNext()) {
			final Document doc = (Document) it.next();
			final FolderSet listaDocumenti = doc.get_FoldersFiledIn();
			final Iterator<?> itFolder = listaDocumenti.iterator();
			if (itFolder.hasNext()) {
				return (Folder) itFolder.next();
			}
		}

		return folder;
	}

	private Document costruisciDocumentoFilenet(final DocumentoRedFnDTO docRedFn, final Long idAoo) {
		Document docFilenet = null;
		final String documentTitle = docRedFn.getDocumentTitle();

		try {
			docFilenet = Factory.Document.createInstance(getOs(), docRedFn.getClasseDocumentale());

			// Impostazione del contenuto, se presente
			if (docRedFn.getDataHandler() != null) {
				setContentDoc(docFilenet, documentTitle, docRedFn.getContentType(), docRedFn.getDataHandler().getInputStream(), null);
			}

			// Impostazione dei metadati (properties)
			setPropertiesAndSaveDocument(docFilenet, docRedFn.getMetadati(), false);

			// Impostazione delle security
			setSecurity(docFilenet, docRedFn.getSecurity());

			if(NumberUtils.isParsable(documentTitle)) { 
				final IVerificaFirmaSRV verificaFirmaSRV = ApplicationContextProvider.getApplicationContext().getBean(IVerificaFirmaSRV.class);
				verificaFirmaSRV.deleteSingoloElemento(idAoo, documentTitle, os.get_Name(), docRedFn.getClasseDocumentale(),1);
				verificaFirmaSRV.insertDopoCreazioneSuFilenet(idAoo,documentTitle,StatoVerificaFirmaEnum.DA_LAVORARE.getStatoVerificaFirma(),os.get_Name(),docRedFn.getClasseDocumentale(),1);
			}

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la creazione dell'oggetto FileNet per il documento: " + documentTitle, e);
			throw new FilenetException("Si è verificato un errore durante la creazione dell'oggetto FileNet per il documento: " + documentTitle, e);
		}

		return docFilenet;
	}

	private void aggiungiAllegati(final Document docPrincipale, final ListSecurityDTO securityDocPrincipale, final List<DocumentoRedFnDTO> allegati,
			final UpdatingBatch ub, final Long idAoo) {
		for (final DocumentoRedFnDTO allegatoRedFn : allegati) {
			// Si impostano sull'allegato le security del documento principale
			allegatoRedFn.setSecurity(securityDocPrincipale);

			// Si costruisce l'allegato FileNet
			final Document allegatoFilenet = costruisciDocumentoFilenet(allegatoRedFn, idAoo);

			// Check-in dell'allegato
			allegatoFilenet.checkin(AutoClassify.DO_NOT_AUTO_CLASSIFY, CheckinType.MAJOR_VERSION);
			ub.add(allegatoFilenet, null);

			aggiungiComponentRelationship(docPrincipale, allegatoFilenet, ub);
		}
	}

	private void aggiungiComponentRelationship(final Document documentoPrincipale, final Document child, final UpdatingBatch ub) {
		final ComponentRelationship cr = Factory.ComponentRelationship.createInstance(getOs(), null);
		cr.set_ParentComponent(documentoPrincipale);
		cr.set_ChildComponent(child);

		cr.set_ComponentRelationshipType(ComponentRelationshipType.DYNAMIC_CR);
		cr.set_VersionBindType(VersionBindType.LATEST_VERSION);

		cr.set_Name(ALLEGATO_DOCUMENTO_LABEL + child.getProperties().getStringValue(DOCUMENT_TITLE));
		ub.add(cr, null);
	}

	private static Annotation costruisciAnnotationFilenet(final Document doc, final FilenetAnnotationEnum annotationType, final InputStream annotationContent,
			final ObjectStore os, final Integer elementSequenceNumber, final String objectContentType, final String objectRetrievalName) {
		final Annotation annObject = Factory.Annotation.createInstance(os, "Annotation");
		annObject.set_AnnotatedObject(doc);
		if (elementSequenceNumber != null) {
			annObject.set_AnnotatedContentElement(elementSequenceNumber);
		}
		annObject.set_DescriptiveText(annotationType.getNome());

		final ContentTransfer ctObject = Factory.ContentTransfer.createInstance();
		ctObject.setCaptureSource(annotationContent);
		if (!StringUtils.isNullOrEmpty(objectContentType)) {
			ctObject.set_ContentType(objectContentType);
		}
		if (!StringUtils.isNullOrEmpty(objectRetrievalName)) {
			ctObject.set_RetrievalName(objectRetrievalName);
		}

		final ContentElementList annContentList = Factory.ContentElement.createList();
		annContentList.add(ctObject);
		annObject.set_ContentElements(annContentList);

		return annObject;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#creaAnnotation(java.lang.String,
	 *      java.lang.String, it.ibm.red.business.enums.FilenetAnnotationEnum,
	 *      java.lang.Long).
	 */
	@Override
	public void creaAnnotation(final String idDocumento, final String text, final FilenetAnnotationEnum annotationType, final Long idAoo) {
		LOGGER.info("Creazione annotazione di tipo: " + annotationType + PER_IL_DOCUMENTO + idDocumento + " -> START");
		Document doc = null;

		final PropertyFilter pfDocClass = new PropertyFilter();
		pfDocClass.addIncludeProperty(new FilterElement(null, null, null, DOCUMENT_TITLE, null));
		pfDocClass.addIncludeProperty(new FilterElement(null, null, null, PropertyNames.ANNOTATIONS, null));
		pfDocClass.addIncludeProperty(new FilterElement(null, null, null, PropertyNames.VERSION_SERIES, null));
		pfDocClass.addIncludeProperty(new FilterElement(null, null, null, PropertyNames.CONTENT_ELEMENTS, null));

		// Recupero del documento
		try {
			List<String> selectList = null;
			final FilterElement[] fe = pfDocClass.getIncludeProperties();
			if (fe.length > 0) {
				selectList = new ArrayList<>(fe.length);
				for (final FilterElement filterElement : fe) {
					selectList.add(filterElement.getValue());
				}
			}

			doc = getDocumentByIdGestionale(idDocumento, selectList, pfDocClass, idAoo.intValue(), null, null);
		} catch (final Exception e) {
			throw new FilenetException(ERROR_RECUPERO_DOCUMENTO_MSG, e);
		}

		// Creazione e salvataggio dell'Annotation associata al Document
		if (doc != null) {
			creaAnnotation(doc, text, annotationType);
		}
		LOGGER.info("Creazione annotazione di tipo: " + annotationType + PER_IL_DOCUMENTO + idDocumento + " -> END");
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#creaAnnotation(com.filenet.api.core.Document,
	 *      java.lang.String, it.ibm.red.business.enums.FilenetAnnotationEnum).
	 */
	@Override
	public void creaAnnotation(final Document inDoc, final String text, final FilenetAnnotationEnum annotationType) {
		Document doc = inDoc;
		if (FilenetAnnotationEnum.TEXT_MAIL.equals(annotationType) || FilenetAnnotationEnum.CURRENT_VERSION.equals(annotationType)) {
			final SubSetImpl ssi = (SubSetImpl) doc.get_VersionSeries().get_Versions();
			final List<?> list = ssi.getList();
			doc = (Document) list.get(list.size() - 1);
		}

		Integer elementSequenceNumber = null;
		try {
			elementSequenceNumber = ((ContentElement) getDocumentContentTransfer(doc)).get_ElementSequenceNumber();
		} catch (final Exception ex) {
			LOGGER.warn("Non è stato possibile estrarre l'elementSequenceNumber dal ContentElement del documento.", ex);
		}

		final Annotation annObject = costruisciAnnotationFilenet(doc, annotationType, new ByteArrayInputStream(text.getBytes()), getOs(), elementSequenceNumber, null, null);

		annObject.save(RefreshMode.REFRESH);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#fascicolaDocumento(java.lang.String,
	 *      it.ibm.red.business.dto.filenet.DocumentoFascicolazioneRedFnDTO,
	 *      java.lang.Long).
	 */
	@Override
	public void fascicolaDocumento(final String idFascicolo, final DocumentoFascicolazioneRedFnDTO docFascicolazione, final Long idAoo) {
		fascicolaDocumento(idFascicolo, docFascicolazione, idAoo, null);
	}

	private void fascicolaDocumento(final String idFascicolo, final DocumentoFascicolazioneRedFnDTO docFascicolazione, final Long idAoo, final UpdatingBatch ub) {
		final Folder folderFascicolo = getFolderFascicolo(idFascicolo, idAoo.intValue());
		final ReferentialContainmentRelationship refContRel = creaRCRFascicolazioneDocumento(docFascicolazione, folderFascicolo, idAoo);
		if (ub == null) {
			refContRel.save(RefreshMode.REFRESH);
		} else {
			ub.add(refContRel, null);
		}
	}

	/**
	 * @param idFascicolo
	 * @param docFascicolazione
	 * @param idAoo
	 */
	@Override
	public void rimuoviDocumentoFascicolo(final String idFascicolo, final DocumentoFascicolazioneRedFnDTO docFascicolazione, final Long idAoo) {
		final String idDocumento = docFascicolazione.getDocumentTitle();
		LOGGER.info("rimuoviDocumentoFascicolo -> START. Documento: " + idDocumento + VIRGOLA_FASCICOLO + idFascicolo);
		final Folder folderFascicolo = getFolderFascicolo(idFascicolo, idAoo.intValue());

		final Document doc = getDocumentByIdGestionale(idDocumento, Arrays.asList(PropertyNames.ID, getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY)),
				null, idAoo.intValue(), null, null);

		if (folderFascicolo == null) {
			throw new FilenetException(ERROR_FOLDER_FASCICOLO_ASSENTE_MSG);
		}

		LOGGER.info("rimuoviDocumentoFascicolo -> Inizio rimozione del documento: " + idDocumento + " dal fascicolo: " + idFascicolo);
		folderFascicolo.unfile(doc).save(RefreshMode.REFRESH);
		LOGGER.info("rimuoviDocumentoFascicolo -> END. Documento: " + idDocumento + VIRGOLA_FASCICOLO + idFascicolo);
	}

	/**
	 * @param idFascicolo
	 * @param docFascicolazione
	 * @param idAoo
	 */
	@Override
	public void copiaDocumentoFascicolo(final String idFascicolo, final DocumentoFascicolazioneRedFnDTO docFascicolazione, final Long idAoo) {
		final String idDocumento = docFascicolazione.getDocumentTitle();
		LOGGER.info("copiaDocumentoFascicolo -> START. Documento: " + idDocumento + VIRGOLA_FASCICOLO + idFascicolo);

		final Document doc = getDocumentByIdGestionale(idDocumento, Arrays.asList(PropertyNames.ID, getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY)),
				null, idAoo.intValue(), null, null);

		if (doc == null) {
			throw new FilenetException(ERROR_DOCUMENTO_ASSENTE_MSG);
		}

		final Folder folderNuovoFascicolo = getFolderFascicolo(idFascicolo, idAoo.intValue());

		if (folderNuovoFascicolo == null) {
			throw new FilenetException(ERROR_FOLDER_FASCICOLO_ASSENTE_MSG);
		}

		DefineSecurityParentage securityParentage = DefineSecurityParentage.DO_NOT_DEFINE_SECURITY_PARENTAGE;
		if (docFascicolazione.isEreditaSecurity()) {
			securityParentage = DefineSecurityParentage.DEFINE_SECURITY_PARENTAGE;
		}

		LOGGER.info("copiaDocumentoFascicolo -> Inizio copia documento: " + idDocumento + " nel fascicolo: " + idFascicolo);

		folderNuovoFascicolo.file(doc, AutoUniqueName.AUTO_UNIQUE, doc.getProperties().getStringValue(getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY)),
				securityParentage).save(RefreshMode.REFRESH);
		LOGGER.info("copiaDocumentoFascicolo -> END. Documento: " + idDocumento + VIRGOLA_FASCICOLO + idFascicolo);
	}

	/**
	 * @param idDocumento
	 * @param allegato
	 * @param tipoAnnotazione
	 * @param idAoo
	 * @param onlyFirst
	 * @return
	 */
	@Override 
	public List<AnnotationDTO> getAnnotationsDocument(final String idDocumento, final boolean allegato, final FilenetAnnotationEnum tipoAnnotazione,
			final Long idAoo, final boolean onlyFirst) {
		final List<AnnotationDTO> annotationsCustom = new ArrayList<>();
		final List<Document> docVersions = new ArrayList<>();

		try {
			
			if (!allegato) {
				if (FilenetAnnotationEnum.TEXT_MAIL.equals(tipoAnnotazione)
						|| FilenetAnnotationEnum.CURRENT_VERSION.equals(tipoAnnotazione)) {
					Document doc = getFirstVersionById(idDocumento, idAoo);
					if(doc != null){
						docVersions.add(doc);	// Annotations da cercare nella prima versione
					}
				} else if (FilenetAnnotationEnum.CONTENT_LIBRO_FIRMA.equals(tipoAnnotazione)) {
					docVersions.addAll(getDocumentVersionList(idDocumento, idAoo));	// Annotation da cercare in tutte le versioni
				} else {
					Document doc = getDocumentForAnnotations(idDocumento, idAoo);
					if(doc != null){
						docVersions.add(doc);	// Si cerca l'annotation nella versione più recente
					}
				}
			} else {
				docVersions.add(getAllegatoForDownload(pp.getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY), idDocumento));
			}

			if (docVersions.isEmpty()) {
				
				LOGGER.warn("Documento " + idDocumento + " non presente per il recupero delle annotation");
				
			} else {
				annotationsCustom.addAll(getAnnotationsFromDocumentVersions(docVersions, tipoAnnotazione, onlyFirst));
			}

		} catch (Exception e) {
			LOGGER.error("Errore nell'ottenere le annotation per il documento ID: " + idDocumento, e);
		}

		return annotationsCustom;
	}
	
	/**
	 * @param docVersions
	 * @param tipoAnnotazione
	 * @param onlyFirst
	 * @return
	 */
	private List<AnnotationDTO> getAnnotationsFromDocumentVersions(final Collection<Document> docVersions, final FilenetAnnotationEnum tipoAnnotazione, final boolean onlyFirst) {
		List<AnnotationDTO> annotationsCustom = new ArrayList<>();

		if (!CollectionUtils.isEmpty(docVersions)) {
			boolean found = false;
			
			for (Document docVersion : docVersions) {
				AnnotationSet as = docVersion.get_Annotations();

				Iterator<?> iter = as.iterator();
				while (iter.hasNext()) {
					final Annotation annObject = (Annotation) iter.next();
					final Property dateCreated = annObject.getProperties().get("DateCreated");
					final Date dateCreatedDate = dateCreated.getDateTimeValue();
					if (tipoAnnotazione.getNome().equals(annObject.get_DescriptiveText())) {
						found = true;
						final AnnotationDTO annotationCustom = new AnnotationDTO();
						annotationCustom.setDescriptiveText(annObject.get_DescriptiveText());
						ContentElementList cel = annObject.get_ContentElements();
						
						Iterator<?> iCel = cel.iterator();
						if (iCel.hasNext()) {
							final ContentTransfer ct = (ContentTransfer) iCel.next();
							annotationCustom.setContent(ct.accessContentStream());
						}
						annotationCustom.setDateCreated(dateCreatedDate);
						annotationsCustom.add(annotationCustom);
						if (onlyFirst) {
							break;
						}
					}
				}
				
				if (found) {
					break;
				}
			}
		}
		
		return annotationsCustom;
	}
	
	/**
	 * @param idDocumento
	 * @param tipoAnnotazione
	 * @param idAoo
	 * @return
	 */
	@Override 
	public final AnnotationDTO getAnnotationDocumentContent(final String idDocumento, final FilenetAnnotationEnum tipoAnnotazione, final Long idAoo) {
		return getAnnotationDocumentContent(idDocumento, false, tipoAnnotazione, idAoo);
	}

	/**
	 * @param idDocumento
	 * @param allegato
	 * @param tipoAnnotazione
	 * @param idAoo
	 * @return
	 */
	@Override 
	public final AnnotationDTO getAnnotationDocumentContent(final String idDocumento, final boolean allegato, final FilenetAnnotationEnum tipoAnnotazione, final Long idAoo) {
		AnnotationDTO annotation = null;

		final List<AnnotationDTO> annotations = getAnnotationsDocument(idDocumento, allegato, tipoAnnotazione, idAoo, true);
		if (!CollectionUtils.isEmpty(annotations)) {
			annotation = annotations.get(0);
		}

		return annotation;
	}

	/**
	 * Metodo che ripristina la versione precedente del documento.
	 * 
	 * @param inDocument
	 *            Documento da cui eliminare l'ultima versione.
	 */
	@Override
	public final void eliminaUltimaVersione(final Document inDocument) {
		Document document = inDocument;
		try {
			// Si recuperano di tutte le versioni del documento
			final SubSetImpl ssi = (SubSetImpl) document.get_VersionSeries().get_Versions();
			final List<?> list = ssi.getList();

			final int nVersion = list.size();
			LOGGER.info("eliminaUltimaVersione -> Sono state trovate " + nVersion + " versioni del documento con GUID: " + document.get_Id().toString() + ")");

			// Si procede con l'eliminazione solo se esiste più di una versione
			if (nVersion > 1) {
				for (int i = 0; i < list.size(); i++) {
					// Recupero delle versioni: dalla più recente alla più vecchia
					document = (Document) list.get(i);
					if (Boolean.TRUE.equals(document.get_IsCurrentVersion())) {
						document.delete();
						document.save(RefreshMode.REFRESH);
						LOGGER.info("eliminaUltimaVersione -> È stata eliminata l'ultima versione del documento: " + document.get_Id());
						break;
					}
				}
			}
		} catch (final Exception e) {
			throw new FilenetException("eliminaUltimaVersione -> Errore durante l'eliminazione dell'ultima versione del documento con GUID: " + document.get_Id(), e);
		}
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#eliminaVersioneDocumentoByGuid(java.lang.String).
	 */
	@Override
	public void eliminaVersioneDocumentoByGuid(final String guid) {
		if (!StringUtils.isNullOrEmpty(guid)) {
			deleteVersion(idFromGuid(guid));
		}
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#eliminaDocumentoByDocumentTitle(java.lang.String,
	 *      java.lang.Long).
	 */
	@Override
	public void eliminaDocumentoByDocumentTitle(final String documentTitle, final Long idAoo) {
		final PropertyFilter pf = new PropertyFilter();
		pf.addIncludeProperty(new FilterElement(null, null, null, PropertyNames.ID, null));
		pf.addIncludeProperty(new FilterElement(null, null, null, PropertyNames.VERSION_SERIES, null));

		final List<String> selectList = new ArrayList<>();
		for (final FilterElement fe : pf.getIncludeProperties()) {
			selectList.add(fe.getValue());
		}

		final Document documentoFilenet = getDocumentByIdGestionale(documentTitle, selectList, pf, idAoo.intValue(), null, null);

		eliminaDocumento(documentoFilenet);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#eliminaDocumento(java.lang.String).
	 */
	@Override
	public void eliminaDocumento(final String guid) {
		eliminaDocumento(getDocumentByGuid(guid));
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#eliminaDocumento(com.filenet.api.core.Document).
	 */
	@Override
	public void eliminaDocumento(final Document document) {
		try {
			if (document != null) {
				document.get_VersionSeries().delete();
				document.get_VersionSeries().save(RefreshMode.REFRESH);
				LOGGER.info("eliminaDocumento -> Sono state eliminate tutte le versioni del documento: " + document.get_Id());
			}
		} catch (final Exception e) {
			LOGGER.error("eliminaDocumento -> Errore durante l'eliminazione del documento: " + document.get_Id(), e);
			unfileAndHide(document);
			throw new FilenetException(e);
		}
	}

	private void unfileAndHide(final Document document) {
		final FolderSet folders = document.get_FoldersFiledIn();

		if (!folders.isEmpty()) {
			final Iterator<?> it = folders.iterator();
			while (it.hasNext()) {
				final Folder folder = (Folder) it.next();
				folder.unfile(document).save(RefreshMode.REFRESH);
			}
		}

		final AccessPermission perm = Factory.AccessPermission.createInstance();
		perm.set_AccessMask(AccessLevel.FULL_CONTROL_AS_INT);
		perm.set_GranteeName("P8Admin");
		perm.set_AccessType(AccessType.ALLOW);
		perm.set_InheritableDepth(-1);
		document.refresh();
		final AccessPermissionList permList = Factory.AccessPermission.createList();
		permList.add(perm);
		document.set_Permissions(permList);

		document.save(RefreshMode.REFRESH);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#eliminaMail(java.lang.String).
	 */
	@Override
	public void eliminaMail(final String guid) {
		final String query = SELECT + PropertyNames.VERSIONS + ", " + PropertyNames.VERSION_SERIES + ", " + PropertyNames.DATE_CREATED + " FROM Email WHERE Id = '" + guid
				+ "' ORDER BY DateCreated ASC";
		final DocumentSet documents = (DocumentSet) new SearchScope(getOs()).fetchObjects(new SearchSQL(query), 1, null, false);
		if (!documents.isEmpty()) {
			final VersionSeries mail = ((Document) documents.iterator().next()).get_VersionSeries();
			mail.delete();
			mail.save(RefreshMode.NO_REFRESH);
		}
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#deleteAllegatiByGuid(java.lang.String,
	 *      java.util.List).
	 */
	@Override
	public List<Document> deleteAllegatiByGuid(final String guid, final List<String> allegatiDaEliminareGuid) {
		final Document document = getDocument(idFromGuid(guid));
		final ComponentRelationshipSet crs = document.get_ChildRelationships();
		final List<Document> allegatiEliminati = new ArrayList<>();

		try {
			if (crs != null) {
				final Iterator<?> it = crs.iterator();
				int countTotal = 0;
				int countDeleted = 0;
				while (it.hasNext()) {
					countTotal++;
					final ComponentRelationship cr = (ComponentRelationship) it.next();
					final Document allegatoDocument = cr.get_ChildComponent();
					final String allegatoGuid = allegatoDocument.get_Id().toString();

					for (final String allegatoDaEliminareGuid : allegatiDaEliminareGuid) {
						if (!StringUtils.isNullOrEmpty(allegatoDaEliminareGuid) && allegatoDaEliminareGuid.equals(allegatoGuid)) {
							final SubSetImpl ssi = (SubSetImpl) allegatoDocument.get_VersionSeries().get_Versions();
							final List<?> list = ssi.getList();
							Document versioneAllegato = null;
							for (final Object versione : list) {
								versioneAllegato = (Document) versione;
								if (versioneAllegato == null) {
									continue;
								}
								versioneAllegato.delete();
								versioneAllegato.save(RefreshMode.REFRESH);
							}
							cr.delete();
							cr.save(RefreshMode.REFRESH);
							allegatiEliminati.add(allegatoDocument);
							countDeleted++;
							break;
						}
					}
				}
				document.save(RefreshMode.REFRESH);
				if (countDeleted == countTotal) {
					document.set_CompoundDocumentState(CompoundDocumentState.STANDARD_DOCUMENT);
					document.save(RefreshMode.REFRESH);
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la cancellazione degli allegati: ", e);
			throw new RedException("Errore durante la cancellazione degli allegati");
		}

		return allegatiEliminati;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#eliminaAnnotationDocumento(java.lang.String,
	 *      it.ibm.red.business.enums.FilenetAnnotationEnum, java.lang.Long).
	 */
	@Override
	public void eliminaAnnotationDocumento(final String idDocumento, final FilenetAnnotationEnum tipoAnnotazione, final Long idAoo) {
		LOGGER.info("eliminaAnnotationDocumento -> START. Documento: " + idDocumento);

		try {
			Document doc = null;

			if (FilenetAnnotationEnum.TEXT_MAIL.equals(tipoAnnotazione) || FilenetAnnotationEnum.CURRENT_VERSION.equals(tipoAnnotazione)) {
				doc = getFirstVersionById(idDocumento, idAoo);
			} else {
				doc = getDocumentByIdGestionale(idDocumento, null, null, idAoo.intValue(), null, null);
			}

			if (doc != null) {
				final AnnotationSet as = doc.get_Annotations();
				final Iterator<?> iter = as.iterator();
				while (iter.hasNext()) {
					final Annotation annObject = (Annotation) iter.next();
					if (tipoAnnotazione.getNome().equals(annObject.get_DescriptiveText())) {
						// Eliminazione dell'annotation
						annObject.delete();
						save(doc, annObject);
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore nella cancellazione dell'annotation per il documento: " + idDocumento, e);
		}
		LOGGER.info("eliminaAnnotationDocumento -> END. Documento: " + idDocumento);
	}

	/**
	 * @param doc
	 * @param annObject
	 */
	private void save(final Document doc, final Annotation annObject) {
		try {
			annObject.save(RefreshMode.REFRESH);
		} catch (final Exception ex) {
			LOGGER.warn(ex);
			doc.save(RefreshMode.REFRESH);
		}
	}

	/**
	 * Verifica che il folder [folderName] sia nel folder [rootFolder]
	 * 
	 * @param rootFolder
	 * @param folderName
	 * @return
	 */
	@Override
	public boolean checkFolder(final String rootFolder, final String folderName) {
		LOGGER.info("Verifico che il folder [" + folderName + "] sia nel folder [" + rootFolder + "]");
		final Folder f = getFolder(rootFolder, folderName);
		return f != null;
	}

	private Folder getFolder(final String rootFolder, final String folderName) {

		LOGGER.info("Recupero il folder [" + folderName + "] dal folder[" + rootFolder + "]");
		Folder f = null;
		try {
			final StringBuilder queryString = new StringBuilder();
			queryString.append("SELECT f.Id");
			queryString.append(" FROM");
			queryString.append(" Folder f");
			queryString.append(WHERE);
			if (rootFolder != null) {
				queryString.append(" f.Parent = OBJECT('" + FOLDER_PATH_DELIMITER + rootFolder + "') AND");
			}
			queryString.append(" f.FolderName = '" + folderName + "'");

			final SearchSQL searchSQL = new SearchSQL();
			searchSQL.setQueryString(queryString.toString());
			final SearchScope scope = new SearchScope(getOs());
			final FolderSet myFolder = (FolderSet) scope.fetchObjects(searchSQL, null, null, false);

			final Iterator<?> it = myFolder.iterator();
			if (it.hasNext()) {
				f = getFolder(((Folder) it.next()).get_Id());
			}
		} catch (final Exception e) {
			throw new FilenetException("Errore durante il recupero del folder [" + folderName + "] dal folder[" + rootFolder + "] ", e);
		}

		return f;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#createSubFolder(java.lang.String,
	 *      java.lang.String).
	 */
	@Override
	public Folder createSubFolder(final String folderRoot, final String folderName) {
		Folder folder = null;

		try {
			final Folder rootFolder = Factory.Folder.fetchInstance(getOs(), FOLDER_PATH_DELIMITER + folderRoot, null);
			folder = rootFolder.createSubFolder(folderName);
			folder.set_Permissions(rootFolder.get_Permissions());
			folder.save(RefreshMode.NO_REFRESH);
		} catch (final EngineRuntimeException ere) {
			LOGGER.error(ere);
			folder = Factory.Folder.fetchInstance(getOs(), FOLDER_PATH_DELIMITER + folderRoot + FOLDER_PATH_DELIMITER + folderName, null);
		}

		return folder;
	}

	/**
	 * Creazione di un documento di classe "Email" su FileNet.
	 * 
	 * @param messaggio
	 * @param indirizzoEmail
	 * @param pathFolderDestinazione
	 * @param documentFile
	 * @param conAllegati
	 * @return
	 */
	private <T extends MessaggioEmailDTO> Document createDocumentForMail(final T messaggio, final String indirizzoEmail, final String pathFolderDestinazione,
			final AccessPermissionList rootFolderAcl, final FileDTO documentFile, final boolean conAllegati, final boolean isInvio) {
		LOGGER.info("createDocumentForMail -> Creazione sul CE del documento relativo al messaggio mail in input");
		Document d = null;
		Date d2 = null;
		final Date d1 = new Date();

		try {
			final PropertyFilter pf = new PropertyFilter();
			pf.addIncludeProperty(new FilterElement(null, null, null, PropertyNames.ID, null));
			pf.addIncludeProperty(new FilterElement(null, null, null, getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), null));
			pf.addIncludeProperty(new FilterElement(null, null, null, PropertyNames.PROPERTY_DEFINITIONS, null));
			pf.addIncludeProperty(new FilterElement(null, null, null, PropertyNames.SYMBOLIC_NAME, null));
			pf.addIncludeProperty(new FilterElement(null, null, null, PropertyNames.CLASS_DESCRIPTION, null));

			ClassDefinition ts;
			if (!(messaggio instanceof NotificaInteroperabilitaEmailDTO)) {
				ts = Factory.ClassDefinition.fetchInstance(getOs(), getPP().getParameterByKey(PropertiesNameEnum.MAIL_CLASSNAME_FN_METAKEY), pf);
			} else {
				ts = Factory.ClassDefinition.fetchInstance(getOs(), getPP().getParameterByKey(PropertiesNameEnum.NOTIFICA_INTEROP_CLASSNAME_FN_METAKEY), pf);
			}

			// Creazione documento
			d = Factory.Document.createInstance(getOs(), ts.get_SymbolicName());

			// Popola i metadati dell'oggetto mail da registrare su FileNet
			final List<Metadato> metadati = populateMetadatiEmail(messaggio, indirizzoEmail, isInvio);

			// Impostazione dei metadati del documento mail
			setPropertiesDoc(metadati, d.getProperties(), ts);

			// Impostazione delle security
			final AccessPermissionList acl = Factory.AccessPermission.createList();
			AccessPermission perm = Factory.AccessPermission.createInstance();
			perm.set_AccessMask(AccessLevel.FULL_CONTROL_AS_INT);
			perm.set_GranteeName(AUTHENTICATED_USERS);
			perm.set_AccessType(AccessType.ALLOW);
			perm.set_InheritableDepth(0);
			perm.getProperties().putValue(GRANTEE_TYPE, SecurityPrincipalType.GROUP_AS_INT);
			acl.add(perm);

			if (!CollectionUtils.isEmpty(rootFolderAcl)) {
				final Iterator<?> itRootFolderAcl = rootFolderAcl.iterator();

				while (itRootFolderAcl.hasNext()) {
					final AccessPermission permRootFolder = (AccessPermission) itRootFolderAcl.next();

					perm = Factory.AccessPermission.createInstance();
					perm.set_AccessMask(permRootFolder.get_AccessMask());
					perm.set_GranteeName(permRootFolder.get_GranteeName());
					perm.set_AccessType(permRootFolder.get_AccessType());
					perm.set_InheritableDepth(permRootFolder.get_InheritableDepth());
					perm.getProperties().putValue(GRANTEE_TYPE, permRootFolder.get_GranteeType().getValue());

					acl.add(perm);
				}
			}

			d.set_Permissions(acl);

			if (conAllegati) {
				d.set_CompoundDocumentState(CompoundDocumentState.COMPOUND_DOCUMENT);
			}

			final DefineSecurityParentage securityParentage = DefineSecurityParentage.DO_NOT_DEFINE_SECURITY_PARENTAGE;

			if (documentFile != null) {
				// Aggiunge il content al documento
				setContentFromFile(documentFile, d);
			}

			d.checkin(AutoClassify.DO_NOT_AUTO_CLASSIFY, CheckinType.MAJOR_VERSION);
			d.save(RefreshMode.REFRESH);

			final Folder rootFolder = getFolder(null, pathFolderDestinazione, pf);
			final ReferentialContainmentRelationship rr = rootFolder.file(d, AutoUniqueName.NOT_AUTO_UNIQUE, d.get_Name(), securityParentage);

			rr.save(RefreshMode.REFRESH);
		} catch (final Exception e) {
			throw new FilenetException("createDocumentForMail -> Errore durante la creazione del documento mail su FileNet: " + e.getMessage(), e);
		}

		d2 = new Date();
		LOGGER.info("createDocumentForMail -> Timing: " + (d2.getTime() - d1.getTime()) + "ms");
		LOGGER.info("createDocumentForMail -> GUID mail: " + d.get_Id().toString());

		return d;
	}

	private Folder getFolder(final String idFascicolo, final String folder, final PropertyFilter pf) {
		try {
			String rootFolder;

			if (!StringUtils.isNullOrEmpty(idFascicolo)) {
				rootFolder = getPP().getParameterByKey(PropertiesNameEnum.FOLDER_FASCICOLI_FN_METAKEY) + FOLDER_PATH_DELIMITER + idFascicolo;
			} else if (folder != null && !"".equals(folder.trim())) {
				rootFolder = folder;
			} else {
				rootFolder = getPP().getParameterByKey(PropertiesNameEnum.FOLDER_DOCUMENTI_CE_KEY);
			}

			LOGGER.info("Recupero il folder [" + rootFolder + "]");

			return Factory.Folder.fetchInstance(getOs(), FOLDER_PATH_DELIMITER + rootFolder, pf);
		} catch (final Exception e) {
			throw new FilenetException("Errore durante il recupero del folder", e);
		}
	}

	private void setPropertiesDoc(final List<Metadato> metadatiNSD, final Properties props, final ClassDefinition ts) {
		try {
			if (metadatiNSD != null) {
				for (int i = 0; i < metadatiNSD.size(); i++) {
					setSingleMetadato(metadatiNSD.get(i), props, ts);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw e;
		}
	}

	/**
	 * Imposta un metadato filenet.
	 * 
	 * @param metadato
	 * @param props
	 * @param ts
	 */
	public void setSingleMetadato(final Metadato metadato, final Properties props, final ClassDefinition ts) {
		try {

			if (metadato.getValue() != null || metadato.getMultiValues() != null) {
				final Object valuePropertyClass = getValueObjectClass(metadato.getKey(), metadato.getValue(), metadato.getMultiValues(), ts);

				if (valuePropertyClass instanceof StringList) {
					props.putValue(metadato.getKey(), (StringList) valuePropertyClass);
				} else if (valuePropertyClass instanceof Integer32List) {
					props.putValue(metadato.getKey(), (Integer32List) valuePropertyClass);
				} else {
					props.putObjectValue(metadato.getKey(), valuePropertyClass);
				}
			} else {
				props.putObjectValue(metadato.getKey(), null);
			}

		} catch (final Exception e) {
			LOGGER.error(e);
			throw e;
		}
	}

	/**
	 * @param key
	 * @param value
	 * @param multiValues
	 * @param classeDocumentale
	 * @return
	 */
	@Override
	public Object getValueObjectClass(final String key, final String value, final String[] multiValues, final String classeDocumentale) {
		final ClassDefinition ts = Factory.ClassDefinition.fetchInstance(getOs(), classeDocumentale, null);

		return getValueObjectClass(key, value, multiValues, ts);
	}

	/**
	 * @param key
	 * @param value
	 * @param multiValues
	 * @param ts
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public Object getValueObjectClass(final String key, final String value, final String[] multiValues, final ClassDefinition ts) {
		final PropertyDefinitionList properties = ts.get_PropertyDefinitions();

		for (final Iterator<PropertyDefinition> propertyIter = properties.iterator(); propertyIter.hasNext();) {
			final PropertyDefinition property = propertyIter.next();
			final int tipo = property.get_DataType().getValue();
			final int cardinadility = property.get_Cardinality().getValue();

			if (key.equals(property.get_SymbolicName())) {
				if (tipo == TypeID.BOOLEAN_AS_INT) {
					return new Boolean(value);
				} else if (tipo == TypeID.DATE_AS_INT) {
					return DateUtils.parseDate(value);
				} else if (tipo == TypeID.DOUBLE_AS_INT) {
					return new Double(value);
				} else if (tipo == TypeID.LONG_AS_INT) {
					if (cardinadility == Cardinality.LIST_AS_INT) {
						final Integer32List il = Factory.Integer32List.createList();
						il.addAll(Arrays.asList(multiValues));
						return il;
					} else {
						return new Integer(value);
					}
				} else if (tipo == TypeID.STRING_AS_INT) {
					if (cardinadility == Cardinality.LIST_AS_INT) {
						final StringList sl = Factory.StringList.createList();
						sl.addAll(Arrays.asList(multiValues));
						return sl;
					} else {
						return value;
					}
				}
			}
		}

		return null;
	}

	/**
	 * Model di un Metadato CE.
	 */
	public static class Metadato {

		/**
		 * Chiave.
		 */
		private String key;

		/**
		 * Valore.
		 */
		private String value;

		/**
		 * Valori multipli.
		 */
		private String[] multiValues;

		/**
		 * Costruttore del metadato Filenet.
		 */
		public Metadato() {
			super();
		}

		/**
		 * Costruttore completo.
		 * 
		 * @param key
		 * @param value
		 * @param multiValues
		 */
		public Metadato(final String key, final String value, final String[] multiValues) {
			super();
			this.key = key;
			this.value = value;
			this.multiValues = multiValues;
		}

		/**
		 * Costruttore chiave - valore.
		 * 
		 * @param key
		 * @param value
		 */
		public Metadato(final String key, final String value) {
			this(key, value, null);
		}

		/**
		 * Costruttre chiave - valori.
		 * 
		 * @param key
		 * @param multiValues
		 */
		public Metadato(final String key, final String[] multiValues) {
			this(key, null, multiValues);
		}

		/**
		 * Restituisce la chiave del metadato.
		 * 
		 * @return key
		 */
		public String getKey() {
			return key;
		}

		/**
		 * Imposta la chiave del metadato.
		 * 
		 * @param key
		 */
		public void setKey(final String key) {
			this.key = key;
		}

		/**
		 * Restituisce il valore del metadato.
		 * 
		 * @return value
		 */
		public String getValue() {
			return value;
		}

		/**
		 * Imposta il valore del metadato.
		 * 
		 * @param value
		 */
		public void setValue(final String value) {
			this.value = value;
		}

		/**
		 * Restituisce la lista dei valore del metadato.
		 * 
		 * @return valori metadato
		 */
		public String[] getMultiValues() {
			return multiValues;
		}

		/**
		 * Imposta la lista dei valori del metadato.
		 * 
		 * @param multiValues
		 */
		public void setMultiValues(final String[] multiValues) {
			this.multiValues = multiValues;
		}

	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#updateStateMailByState(java.lang.String,
	 *      com.filenet.api.collection.FolderSet,
	 *      it.ibm.red.business.helper.filenet.ce.FilenetCEHelper.Metadato,
	 *      it.ibm.red.business.helper.filenet.ce.FilenetCEHelper.Metadato,
	 *      java.util.Date).
	 */
	@Override
	public void updateStateMailByState(final String folder, final FolderSet mailBoxs, final Metadato oldMetadato, final Metadato newMetadato, final Date milestoneDate) {
		if (mailBoxs != null) {
			final Iterator<?> itf = mailBoxs.iterator();
			while (itf.hasNext()) {
				try {
					String mailBox = ((Folder) itf.next()).get_FolderName();
					LOGGER.info("Casella mail: " + mailBox);
					mailBox += FOLDER_PATH_DELIMITER + getPP().getParameterByKey(PropertiesNameEnum.FOLDER_MAIL_INBOX_FN_METAKEY);
					final String path = FOLDER_PATH_DELIMITER + folder + FOLDER_PATH_DELIMITER + mailBox;
					LOGGER.info("path: " + path);
					String filter = AND_PARENTESI_DX_AP;

					filter += "" + oldMetadato.getKey() + " = " + oldMetadato.getValue();
					filter += ") ";

					filter += AND_D + getPP().getParameterByKey(PropertiesNameEnum.DATA_RICEZIONE_MAIL_METAKEY) + " is null OR d."
							+ getPP().getParameterByKey(PropertiesNameEnum.DATA_RICEZIONE_MAIL_METAKEY) + " < " + DateUtils.getIsoDatetimeFormat(milestoneDate) + ")";
					filter += AND_D + getPP().getParameterByKey(PropertiesNameEnum.DATA_INVIO_MAIL_METAKEY) + " is null OR d."
							+ getPP().getParameterByKey(PropertiesNameEnum.DATA_INVIO_MAIL_METAKEY) + " < " + DateUtils.getIsoDatetimeFormat(milestoneDate) + ")";

					LOGGER.info("filter: " + filter);
					final DocumentSet docs = getMailsAsDocument(path, getPP().getParameterByKey(PropertiesNameEnum.MAIL_CLASSNAME_FN_METAKEY), filter, null);
					final Iterator<?> it = docs.iterator();

					if (it != null && it.hasNext()) {

						final UpdatingBatch ub = UpdatingBatch.createUpdatingBatchInstance(getOs().get_Domain(), RefreshMode.REFRESH);

						while (it.hasNext()) {

							final Document doc = (Document) it.next();

							doc.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.METADATO_STATO_PRE_ARCHIVIAZIONE),
									Integer.parseInt(oldMetadato.getValue()));
							doc.getProperties().putValue(newMetadato.getKey(), Integer.parseInt(newMetadato.getValue()));

							ub.add(doc, null);
							LOGGER.info(" updating " + doc.getProperties().getStringValue(DOCUMENT_TITLE));
						}

						if (ub.hasPendingExecute()) {
							ub.updateBatch();
							LOGGER.info("update stato eseguito per la casella mail: " + mailBox);
						}

					}

				} catch (final Exception e) {
					throw new RedException("updateStateMailByState ->  Errore durante l'aggiornamento dello stato delle mails non protocollate", e);
				}
			}
		}

	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getFoldersByRoot(java.lang.String).
	 */
	@Override
	public FolderSet getFoldersByRoot(final String folder) {
		FolderSet childFolders = null;
		LOGGER.info("CEEngine -> getFoldersByRoot");

		try {
			final Folder currentFolder = Factory.Folder.fetchInstance(getOs(), FOLDER_PATH_DELIMITER + folder, null);
			childFolders = currentFolder.get_SubFolders();
		} catch (final Exception e) {
			LOGGER.error("Errore nella ricerca delle cartelle su FileNet", e);
		}

		return childFolders;
	}

	private <T extends MessaggioEmailDTO> List<Metadato> populateMetadatiEmail(final T messaggio, final String indirizzoEmail, final boolean isInvio) {
		final List<Metadato> metadati = new ArrayList<>();

		final String idDocumento = indirizzoEmail + "_" + System.currentTimeMillis();
		metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), idDocumento));

		Date dataInvio = messaggio.getDataInvio();
		if (dataInvio == null && isInvio) {
			dataInvio = new Date();
		}
		metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.DATA_INVIO_MAIL_METAKEY), DateUtils.dateToString(dataInvio, false)));

		if (messaggio.getDataRicezione() != null) {
			metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.DATA_RICEZIONE_MAIL_METAKEY), DateUtils.dateToString(messaggio.getDataRicezione(), false)));
		}

		if (isInvio) {
			metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.MITTENTE_MAIL_METAKEY), messaggio.getMittente()));
		} else {
			metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.FROM_MAIL_METAKEY), messaggio.getMittente()));
		}

		metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.DESTINATARI_EMAIL_METAKEY), messaggio.getDestinatari()));

		if (!StringUtils.isNullOrEmpty(messaggio.getDestinatariCC())) {
			metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.DESTINATARI_CC_LONG), messaggio.getDestinatariCC()));
		}

		metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_EMAIL_METAKEY), messaggio.getOggetto()));

		if (!StringUtils.isNullOrEmpty(messaggio.getMsgID())) {
			metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.MESSAGE_ID), messaggio.getMsgID()));
		}

		metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.CASELLA_POSTALE_METAKEY), indirizzoEmail));

		if (messaggio.getTipologiaInvioId() != null) {
			metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.TIPOLOGIA_INVIO_METAKEY), Constants.EMPTY_STRING + messaggio.getTipologiaInvioId()));
		}

		Integer statoMail = StatoMailEnum.INARRIVO.getStatus();
		if (messaggio.getTipologiaInvioId() == TipologiaInvio.NUOVOMESSAGGIO) {
			// Stato forzato esternamente per le nuove mail
			if (messaggio.getStato() != null && !StatoMailEnum.INARRIVO.equals(messaggio.getStato())) {
				statoMail = messaggio.getStato().getStatus();
			}
			// Stato calcolato in base alla tipologia di invio (inoltro/rifiuto)
		} else if (messaggio.getTipologiaInvioId() == TipologiaInvio.INOLTRA) {
			statoMail = StatoMailEnum.INOLTRATA.getStatus();
		} else if (messaggio.getTipologiaInvioId() == TipologiaInvio.RISPONDI) {
			statoMail = StatoMailEnum.RIFIUTATA.getStatus();
		}

		metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.STATO_MAIL_METAKEY), Constants.EMPTY_STRING + statoMail));

		if (!StringUtils.isNullOrEmpty(messaggio.getIdMessaggioPostaNps())) {
			metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.ID_MESSAGGIO_INTEROP_POSTA_NPS_MAIL_METAKEY), messaggio.getIdMessaggioPostaNps()));
		}

		// ### Metadati relativi alla segnatura di Interoperabilità
		populateMetadatiEmailSegnatura(messaggio, metadati);

		// ### Metadati relativi al protocollo (nel caso il messaggio sia una notifica
		// di Interoperabilità)
		if (messaggio instanceof NotificaInteroperabilitaEmailDTO) {
			final NotificaInteroperabilitaEmailDTO notifica = (NotificaInteroperabilitaEmailDTO) messaggio;
			metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY), Constants.EMPTY_STRING + notifica.getNumeroProtocollo()));
			metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY), Constants.EMPTY_STRING + notifica.getAnnoProtocollo()));
			metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY), DateUtils.dateToString(notifica.getDataProtocollo(), false)));
			metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY), Constants.EMPTY_STRING + notifica.getIdProtocollo()));
		}

		return metadati;
	}

	private List<Metadato> populateMetadatiEmailSegnatura(final MessaggioEmailDTO messaggio, final List<Metadato> metadati) {
		final SegnaturaMessaggioInteropDTO segnaturaInterop = messaggio.getSegnaturaInterop();

		if (segnaturaInterop != null) {
			// Pre-assegnatario
			if (segnaturaInterop.getPreAssegnatario() != null) {
				metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.PREASSEGNATARIO_SEGN_INTEROP_MAIL_METAKEY),
						segnaturaInterop.getPreAssegnatario().toString()));
			}

			// Mittente
			if (!StringUtils.isNullOrEmpty(segnaturaInterop.getIndirizzoMailMittente())) {
				metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.MAIL_MITTENTE_SEGN_INTEROP_MAIL_METAKEY), segnaturaInterop.getIndirizzoMailMittente()));
			}

			// Esito validazione
			metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.VALIDAZIONE_SEGN_INTEROP_MAIL_METAKEY), segnaturaInterop.getEsitoValidazione().getNome()));

			// Warning validazione
			metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.WARNINGS_VALIDAZIONE_SEGN_INTEROP_MAIL_METAKEY),
					messaggio.getWarningValidazioneInterop().toArray(new String[0])));

			// Errori validazione
			metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.ERRORI_VALIDAZIONE_SEGN_INTEROP_MAIL_METAKEY),
					messaggio.getErroriValidazioneInterop().toArray(new String[0])));

			// GESTIONE DOCUMENTO PRINCIPALE SEGNATURA -> START
			// Se nel DTO della segnatura è presente l'identificativo del documento
			// principale (idDocPrincipale), allora il documento principale
			// ai fini della protocollazione è uno degli allegati della mail. In questo
			// caso, il Document Title dell'allegato della mail
			// è proprio il nome dell'allegato, che coincide con l'identificatore usato
			// nella segnatura.
			if (!StringUtils.isNullOrEmpty(segnaturaInterop.getIdDocumentoPrincipale())) {
				metadati.add(
						new Metadato(getPP().getParameterByKey(PropertiesNameEnum.ID_DOC_PRINCIPALE_SEGN_INTEROP_MAIL_METAKEY), segnaturaInterop.getIdDocumentoPrincipale()));
			}
			// ...altrimenti, il documento principale ai fini della protocollazione coincide
			// con il testo della mail e il metadato non viene impostato.
			// GESTIONE DOCUMENTO PRINCIPALE SEGNATURA -> END

			// GESTIONE ALLEGATI SEGNATURA -> START
			// N.B. Non è previsto (e gestito) il caso in cui nella segnatura sia indicato
			// il testo della mail tra gli allegati
			if (!CollectionUtils.isEmpty(segnaturaInterop.getAllegati())) {
				metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.IDS_ALLEGATI_SEGN_INTEROP_MAIL_METAKEY),
						segnaturaInterop.getAllegati().toArray(new String[0])));
			}
			// GESTIONE ALLEGATI SEGNATURA -> END
		}

		return metadati;
	}

	/**
	 * @param messaggio
	 * @param indirizzoEmail
	 * @return
	 */
	@Override
	public Map<String, Object> populateMetadatiEmailInoltraRifiuta(final MessaggioEmailDTO messaggio, final String indirizzoEmail) {
		final HashMap<String, Object> metadati = new HashMap<>();

		metadati.put(getPP().getParameterByKey(PropertiesNameEnum.DATA_INVIO_MAIL_METAKEY), new Date());

		metadati.put(getPP().getParameterByKey(PropertiesNameEnum.MITTENTE_MAIL_METAKEY), messaggio.getMittente());
		metadati.put(getPP().getParameterByKey(PropertiesNameEnum.DESTINATARI_EMAIL_METAKEY), messaggio.getDestinatari());

		if (messaggio.getDestinatariCC() != null) {
			metadati.put(getPP().getParameterByKey(PropertiesNameEnum.DESTINATARI_CC_LONG), messaggio.getDestinatariCC());
		}

		metadati.put(getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_EMAIL_METAKEY), messaggio.getOggetto());

		if (messaggio.getMsgID() != null) {
			metadati.put(getPP().getParameterByKey(PropertiesNameEnum.MESSAGE_ID), messaggio.getMsgID());
		}

		metadati.put(getPP().getParameterByKey(PropertiesNameEnum.CASELLA_POSTALE_METAKEY), indirizzoEmail);

		if (messaggio.getTipologiaInvioId() != null) {
			metadati.put(getPP().getParameterByKey(PropertiesNameEnum.TIPOLOGIA_INVIO_METAKEY), messaggio.getTipologiaInvioId());
		}

		Integer statoMail = StatoMailEnum.INARRIVO.getStatus();
		if (messaggio.getTipologiaInvioId() == TipologiaInvio.INOLTRA) {
			statoMail = StatoMailEnum.INOLTRATA.getStatus();
		} else if (messaggio.getTipologiaInvioId() == TipologiaInvio.RISPONDI) {
			statoMail = StatoMailEnum.RIFIUTATA.getStatus();
		}

		metadati.put(getPP().getParameterByKey(PropertiesNameEnum.STATO_MAIL_METAKEY), statoMail);

		return metadati;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#setChildCompoundDocument(com.filenet.api.core.Document,
	 *      java.util.List, java.util.Collection).
	 */
	@Override
	public void setChildCompoundDocument(final Document doc, final List<AllegatoDTO> allegati, final Collection<TipoFile> tipiFile) {
		try {
			if (allegati != null) {
				for (int i = 0; i < allegati.size(); i++) {
					final AllegatoDTO allegato = allegati.get(i);

					final ClassDefinition ts = Factory.ClassDefinition.fetchInstance(getOs(), DOCUMENT, null);
					final Document docChildren = Factory.Document.createInstance(getOs(), ts.get_SymbolicName());
					final Properties props = docChildren.getProperties();

					String nomeFile = null;
					String contentType = null;
					byte[] content = null;

					if (!StringUtils.isNullOrEmpty(allegato.getIdDocumento())) {
						final Document docAllegatoFilenet = getDocumentByGuid(allegato.getGuid());
						final ContentTransfer ct = getDocumentContentTransfer(docAllegatoFilenet);
						contentType = ct.get_ContentType();
						content = FileUtils.getByteFromInputStream(ct.accessContentStream());

						nomeFile = (String) TrasformerCE.getMetadato(docAllegatoFilenet, PropertiesNameEnum.NOME_FILE_METAKEY);

					} else {
						nomeFile = allegato.getNomeFile();

						final String extension = FilenameUtils.getExtension(nomeFile);

						contentType = getContentType(tipiFile, extension.toLowerCase());

						if (contentType == null) {
							contentType = ContentType.MIMETYPE_ENC;
						}

						content = allegato.getContent();
					}

					final List<Metadato> metadatiAllegato = populateMetadatiAllegatoEmail(allegato);
					if (!CollectionUtils.isEmpty(metadatiAllegato)) {
						setPropertiesDoc(metadatiAllegato, props, ts);
					}

					if (content != null) {
						setContentDoc(docChildren, nomeFile, contentType, new ByteArrayInputStream(content), null);
					}

					// Setto le security del padre
					docChildren.set_Permissions(clonePermissions(doc));
					docChildren.checkin(null, CheckinType.MAJOR_VERSION);
					docChildren.save(RefreshMode.REFRESH);

					final ComponentRelationship cr = Factory.ComponentRelationship.createInstance(getOs(), null);
					cr.set_ParentComponent(doc);
					cr.set_ChildComponent(docChildren);
					cr.set_ComponentSortOrder(i + 1);

					cr.set_ComponentRelationshipType(ComponentRelationshipType.DYNAMIC_CR);
					cr.set_VersionBindType(VersionBindType.LATEST_VERSION);

					cr.set_Name(ALLEGATO_DOCUMENTO_LABEL + nomeFile);
					cr.save(RefreshMode.REFRESH);
				}

			}

		} catch (final Exception e) {
			throw new FilenetException(" Errore durante la creazione dei figli del compound document", e);
		}
	}

	private static String getContentType(final Collection<TipoFile> tipiFile, final String ext) {
		String mimeType = null;

		for (final TipoFile tf : tipiFile) {
			if (tf.getEstensione().equals(ext)) {
				mimeType = tf.getMimeType();
				break;
			}
		}

		return mimeType;
	}

	private List<Metadato> populateMetadatiAllegatoEmail(final AllegatoDTO allegato) {
		final List<Metadato> metadati = new ArrayList<>();

		metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), allegato.getNomeFile()));

		return metadati;
	}

	private AccessPermissionList clonePermissions(final Document doc) {
		final AccessPermissionList acl = Factory.AccessPermission.createList();
		doc.fetchProperties(new String[] { "Permissions", "AccessMask", "AccessType", "GranteeName", "InheritableDepth" });
		final Iterator<?> iter = doc.get_Permissions().iterator();

		while (iter.hasNext()) {
			final AccessPermission ace = (AccessPermission) iter.next();
			final AccessPermission perm = Factory.AccessPermission.createInstance();
			perm.set_AccessMask(ace.get_AccessMask());
			perm.set_AccessType(ace.get_AccessType());
			perm.set_GranteeName(ace.get_GranteeName());
			perm.set_InheritableDepth(ace.get_InheritableDepth());
			acl.add(perm);
		}

		return acl;
	}

	/**
	 * 
	 * Creazione di un documento di classe "Email" su FileNet.
	 * 
	 * @param messaggio
	 * @param indirizzoCasellaPostale
	 * @param documentFile
	 * @param tipiFile
	 * @return
	 */
	@Override
	public <T extends MessaggioEmailDTO> Document createMailAsDocument(final T messaggio, final String indirizzoCasellaPostale, final FileDTO documentFile,
			final Collection<TipoFile> tipiFile) {
		// Si controlla se esiste il folder di destinazione dove memorizzare la mail
		String pathFolderDestinazione;
		Folder rootFolder;

		final String rootCasella = getPP().getParameterByKey(PropertiesNameEnum.FOLDER_CASELLA_POSTALE_FN_METAKEY);
		final boolean conAllegati = !CollectionUtils.isEmpty(messaggio.getAllegatiMail());
		boolean isInvio = false;

		if (messaggio.getFolder() != null && !Constants.EMPTY_STRING.equals(messaggio.getFolder().trim())) {

			if (!checkFolder(FOLDER_PATH_DELIMITER + rootCasella + FOLDER_PATH_DELIMITER + indirizzoCasellaPostale, messaggio.getFolder())) {
				rootFolder = createSubFolder(rootCasella + FOLDER_PATH_DELIMITER + indirizzoCasellaPostale, messaggio.getFolder());
			} else {
				rootFolder = getFolder(FOLDER_PATH_DELIMITER + rootCasella + FOLDER_PATH_DELIMITER + indirizzoCasellaPostale + FOLDER_PATH_DELIMITER + messaggio.getFolder());
			}

			pathFolderDestinazione = rootCasella + FOLDER_PATH_DELIMITER + indirizzoCasellaPostale + FOLDER_PATH_DELIMITER + messaggio.getFolder();
		} else {

			if (!checkFolder(FOLDER_PATH_DELIMITER + rootCasella + FOLDER_PATH_DELIMITER + indirizzoCasellaPostale, getPP().getParameterByKey(PropertiesNameEnum.FOLDER_MAIL_INVIATA_FN_METAKEY))) {

				rootFolder = createSubFolder(rootCasella + FOLDER_PATH_DELIMITER + indirizzoCasellaPostale, getPP().getParameterByKey(PropertiesNameEnum.FOLDER_MAIL_INVIATA_FN_METAKEY));
			} else {
				rootFolder = getFolder(FOLDER_PATH_DELIMITER + rootCasella + FOLDER_PATH_DELIMITER + indirizzoCasellaPostale + FOLDER_PATH_DELIMITER + getPP().getParameterByKey(PropertiesNameEnum.FOLDER_MAIL_INVIATA_FN_METAKEY));
			}

			pathFolderDestinazione = rootCasella + FOLDER_PATH_DELIMITER + indirizzoCasellaPostale + FOLDER_PATH_DELIMITER + getPP().getParameterByKey(PropertiesNameEnum.FOLDER_MAIL_INVIATA_FN_METAKEY);

			isInvio = true;
		}

		// Si procede alla creazione al documento di classe "Email"
		final Document message = createDocumentForMail(messaggio, indirizzoCasellaPostale, pathFolderDestinazione, rootFolder.get_Permissions(), documentFile, conAllegati,
				isInvio);

		if (conAllegati) {
			setChildCompoundDocument(message, messaggio.getAllegatiMail(), tipiFile);
		}

		return message;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getMailAsDocument(it.ibm.red.business.dto.MessaggioEmailDTO).
	 */
	@Override
	public Document getMailAsDocument(final MessaggioEmailDTO messaggio) {
		Document doc = null;

		try {
			final StringBuilder queryString = new StringBuilder();
			queryString.append(SELECT_ID);
			queryString.append(FROM + getPP().getParameterByKey(PropertiesNameEnum.MAIL_CLASSNAME_FN_METAKEY));
			queryString.append(WHERE + getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY) + " = '" + messaggio.getDocumentTitle()
					+ AND_IS_CURRENT_VERSION_TRUE);

			final SearchSQL sql = new SearchSQL(queryString.toString());
			// Search the object store with the sql and get the returned items
			final SearchScope searchScope = new SearchScope(getOs());
			final DocumentSet documents = (DocumentSet) searchScope.fetchObjects(sql, null, null, false);
			final Iterator<?> it = documents.iterator();

			// fix inoltroMail begin
			int contaRes = 0;
			String guid = null;
			while (it.hasNext()) {
				guid = ((Document) it.next()).get_Id().toString();
				doc = getDocumentByGuid(guid);
				// fix begin
				final String oggetto = doc.getProperties().getStringValue(getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_EMAIL_METAKEY));
				if (!EmailUtils.isNotificaPecRed(oggetto)) {
					contaRes++;
				}
				// fix end
				if (contaRes > 1) {
					throw new FilenetException("DUPLICATE_MSGID " + guid);
				}

			}
			// fix inoltroMail end

			if (doc == null) {
				throw new FilenetException(ERROR_DOCUMENTO_ASSENTE_MSG);
			}

			LOGGER.info("Email - Guid: " + doc.get_Id().toString());
			return doc;

			// fix inoltroMail begin
		} catch (final FilenetException e) {
			if ((e.getMessage() != null) && (e.getMessage().indexOf("DUPLICATE_MSGID") == -1)) {
				throw new FilenetException("Impossibile inviare la mail, contattare l'Assistenza", e);
			} else {
				throw new FilenetException(ERROR_RECUPERO_DOCUMENT_DA_MAIL_MSG, e);
			}
			// fix inoltroMail end
		} catch (final Exception e) {
			throw new FilenetException(ERROR_RECUPERO_DOCUMENT_DA_MAIL_MSG, e);
		}
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#aggiornaDocumentoProtocollo(java.lang.Long,
	 *      java.lang.Long, java.lang.Long, int, int, java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 *      java.lang.String, java.lang.Long).
	 */
	@Override
	public void aggiornaDocumentoProtocollo(final Long idUtente, final Long idUfficio, final Long idAoo, final int tipoProtocollo, final int numeroProtocollo,
			final String idProtocollo, final String oggettoProtocollo, final String descrizioneTipologiaDocumento, final String annoProtocollo, final String dataProtocollo,
			final String documentTitle, final Long tipologiaProtocollazione) {
		LOGGER.info("aggiornaDocumentoProtocollo -> START, tipologiaProtocollazione: " + tipologiaProtocollazione);

		try {
			final PropertyFilter pf = new PropertyFilter();
			pf.addIncludeProperty(new FilterElement(null, null, null, getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY), null));
			pf.addIncludeProperty(new FilterElement(null, null, null, getPP().getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY), null));
			pf.addIncludeProperty(new FilterElement(null, null, null, getPP().getParameterByKey(PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY), null));
			pf.addIncludeProperty(new FilterElement(null, null, null, getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY), null));
			pf.addIncludeProperty(new FilterElement(null, null, null, getPP().getParameterByKey(PropertiesNameEnum.ID_UTENTE_PROTOCOLLATORE_METAKEY), null));
			pf.addIncludeProperty(new FilterElement(null, null, null, getPP().getParameterByKey(PropertiesNameEnum.ID_GRUPPO_PROTOCOLLATORE_METAKEY), null));

			if (TipologiaProtocollazioneEnum.isProtocolloNPS(tipologiaProtocollazione)) {
				pf.addIncludeProperty(new FilterElement(null, null, null, getPP().getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY), null));
				pf.addIncludeProperty(new FilterElement(null, null, null, getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_PROTOCOLLO_NPS_METAKEY), null));
				pf.addIncludeProperty(new FilterElement(null, null, null, getPP().getParameterByKey(PropertiesNameEnum.DESCRIZIONE_TIPOLOGIA_DOCUMENTO_NPS_METAKEY), null));
				if (TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(tipologiaProtocollazione)) {
					pf.addIncludeProperty(new FilterElement(null, null, null, getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_EMERGENZA_METAKEY), null));
					pf.addIncludeProperty(new FilterElement(null, null, null, getPP().getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_EMERGENZA_METAKEY), null));
					pf.addIncludeProperty(new FilterElement(null, null, null, getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_EMERGENZA_METAKEY), null));
				}
			}

			final FilterElement[] fe = pf.getIncludeProperties();
			final List<String> selectList = new ArrayList<>(fe.length);
			for (final FilterElement filterElement : fe) {
				selectList.add(filterElement.getValue());
			}

			final Document doc = getDocumentByIdGestionale(documentTitle, selectList, pf, idAoo.intValue(), null, null);

			if (doc == null) {
				throw new FilenetException(ERROR_DOCUMENTO_ASSENTE_MSG);
			}

			final Properties propProto = doc.getProperties();

			propProto.putObjectValue(getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY), numeroProtocollo);
			propProto.putObjectValue(getPP().getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY), DateUtils.parseDate(dataProtocollo));
			propProto.putObjectValue(getPP().getParameterByKey(PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY), tipoProtocollo);
			propProto.putObjectValue(getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY), Integer.parseInt(annoProtocollo));
			propProto.putObjectValue(getPP().getParameterByKey(PropertiesNameEnum.ID_UTENTE_PROTOCOLLATORE_METAKEY), idUtente.intValue());
			propProto.putObjectValue(getPP().getParameterByKey(PropertiesNameEnum.ID_GRUPPO_PROTOCOLLATORE_METAKEY), idUfficio.intValue());

			if (TipologiaProtocollazioneEnum.isProtocolloNPS(tipologiaProtocollazione)) {
				propProto.putObjectValue(getPP().getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY), idProtocollo);
				// Si applica la substring() dato che il metadato sul CE è limitato a max 64
				// caratteri
				propProto.putObjectValue(getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_PROTOCOLLO_NPS_METAKEY),
						org.apache.commons.lang3.StringUtils.substring(oggettoProtocollo, 0, 64));
				propProto.putObjectValue(getPP().getParameterByKey(PropertiesNameEnum.DESCRIZIONE_TIPOLOGIA_DOCUMENTO_NPS_METAKEY), descrizioneTipologiaDocumento);
				if (TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(tipologiaProtocollazione)) {
					propProto.putObjectValue(getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_EMERGENZA_METAKEY), numeroProtocollo);
					propProto.putObjectValue(getPP().getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_EMERGENZA_METAKEY), DateUtils.parseDate(dataProtocollo));
					propProto.putObjectValue(getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_EMERGENZA_METAKEY), Integer.parseInt(annoProtocollo));
				}
			}

			doc.save(RefreshMode.REFRESH);
		} catch (final Exception e) {
			LOGGER.error("Errore nell'aggiornamento dei dati del protocollo per il documento: " + documentTitle + ", tipologiaProtocollazione: " + tipologiaProtocollazione,
					e);
			throw new FilenetException(
					"Errore nell'aggiornamento dei dati del protocollo per il documento: " + documentTitle + ", tipologiaProtocollazione: " + tipologiaProtocollazione, e);
		}

		LOGGER.info("aggiornaDocumentoProtocollo -> END, tipologiaProtocollazione: " + tipologiaProtocollazione);
	}

	/**
	 * Aggiorna elenco trasmissione
	 * 
	 * @param documentTitle
	 * @param idAoo
	 * @param destinatari
	 */
	@Override
	public void aggiornaElencoTrasmisione(final String documentTitle, final int idAoo, final String destinatari) {
		// Aggiornamento elencoTrasmissione del documento con i destinatari elettronici
		final String dataSpedizione = DateUtils.dateToString(new Date(), true);

		// Recupero destinatari già presenti
		final Document document = getDocumentByPath(FOLDER_PATH_DELIMITER + getPP().getParameterByKey(PropertiesNameEnum.FOLDER_DOCUMENTI_CE_KEY) + FOLDER_PATH_DELIMITER + documentTitle, idAoo);

		if (document == null) {
			throw new FilenetException(ERROR_DOCUMENTO_ASSENTE_MSG);
		}

		final ArrayList<String> elencoDestEsistentiList = new ArrayList<>();
		final Properties prop = document.getProperties();
		final StringList destinatariEsistenti = prop.getStringListValue(getPP().getParameterByKey(PropertiesNameEnum.ELENCO_TRASMISSIONE_LIST_METAKEY));
		if (destinatariEsistenti != null) {
			final Iterator<?> itDestinatariEsistenti = destinatariEsistenti.iterator();
			while (itDestinatariEsistenti.hasNext()) {
				final String destinatarioEsistente = (String) itDestinatariEsistenti.next();
				elencoDestEsistentiList.add(destinatarioEsistente);
			}
		}

		final String[] dstArr = destinatari != null ? destinatari.split("###;###") : new String[0];
		// Aggiungo i destinatari elettronici, se presenti
		for (int i = 0; i < dstArr.length; i++) {
			final String[] destinatario = dstArr[i].split(",");
			if (TipoDestinatario.ESTERNO.equalsIgnoreCase(destinatario[Mail.IE_MAIL_POSITION])
					&& TipoSpedizione.MEZZO_ELETTRONICO.equals(Integer.parseInt(destinatario[Mail.MEZZO_SPEDIZIONE_POSITION]))) {
				final String destinatarioTrasm = destinatario[0] + "," + destinatario[1] + "," + Varie.ELENCOTRASMISSIONEELETTRONICO + "," + dataSpedizione;
				elencoDestEsistentiList.add(destinatarioTrasm);
			}
		}
		final String[] elencoDestinatari = new String[elencoDestEsistentiList.size()];
		elencoDestEsistentiList.toArray(elencoDestinatari);

		// Aggiornamento del metadato elencoTrasmissioneList
		if (!elencoDestEsistentiList.isEmpty()) {
			final Map<String, Object> metadati = new HashMap<>();
			metadati.put(getPP().getParameterByKey(PropertiesNameEnum.ELENCO_TRASMISSIONE_LIST_METAKEY), elencoDestinatari);
			updateMetadati(document, metadati);
		}
	}

	private Document getDocumentByPath(final String path, final Integer idAoo) {
		// ricerca del documento nel folder documenti da conservare
		Document doc = null;
		try {
			doc = getDocumentByPath(path, null, idAoo);
		} catch (final Exception e) {
			throw new FilenetException(" Errore durante il recupero del documento", e);
		}
		return doc;
	}

	private Document getDocumentByPath(final String path, final PropertyFilter pf, final Integer idAoo) {
		// ricerca del documento nel folder documenti da conservare
		Document doc = null;
		List<String> selectList = null;
		try {
			if (pf != null) {
				final FilterElement[] fe = pf.getIncludeProperties();
				selectList = new ArrayList<>(fe.length);
				for (int i = 0; i < fe.length; i++) {
					final FilterElement filterElement = fe[i];
					selectList.add(filterElement.getValue());
				}
			}
			final String idDocumento = path.substring(path.lastIndexOf(FOLDER_PATH_DELIMITER) + 1, path.length());
			doc = getDocumentByIdGestionale(idDocumento, selectList, pf, idAoo, null, null);
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_DOCUMENTO_MSG, e);
			throw new FilenetException(ERROR_RECUPERO_DOCUMENTO_MSG, e);
		}
		return doc;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#verifyFascicoloFepaDisponibilePerOPCollegati(java.lang.String,
	 *      java.lang.Long).
	 */
	@Override
	public Boolean verifyFascicoloFepaDisponibilePerOPCollegati(final String docuementTitle, final Long idAoo) {
		Boolean validOPList = true;

		try {
			final Document doc = getDocumentByIdGestionale(docuementTitle, null, null, idAoo.intValue(), null, null);

			if (doc == null) {
				throw new FilenetException(ERROR_DOCUMENTO_ASSENTE_MSG);
			}

			final CustomObject registroOP = (CustomObject) doc.getProperties().getObjectValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_OP_PROXY));
			if (registroOP == null) {
				validOPList = false;
			} else {
				final SearchSQL searchSQL = new SearchSQL();
				final String query = "SELECT top 1 this " + "FROM OrdineDiPagamentoDD " + "WHERE proxyDecreto = OBJECT('" + registroOP.get_Id() + "') "
						+ "AND statoOP IN ('Nuovo','Inviato') " + "AND idFascicoloFEPA is null";
				searchSQL.setQueryString(query);
				final SearchScope scope = new SearchScope(getOs());
				final DocumentSet ds = (DocumentSet) scope.fetchObjects(searchSQL, 100, null, false);
				final Iterator it = ds.iterator();
				if (it.hasNext()) {
					validOPList = false;
				}
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante la verifica degli ordini pagamento per il documento: " + docuementTitle, e);
		}
		return validOPList;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getAllOPId(com.filenet.api.core.Document).
	 */
	@Override
	public List<String> getAllOPId(final Document decreto) {
		LOGGER.info("Ricerca IdFascicoloFEPA per tutti gli OP collegati al decreto");
		if (decreto.getProperties().getObjectValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_OP_PROXY)) != null) {
			final List<String> stati = Arrays.asList("Nuovo", "Inviato");
			return getIdFascicoliFepaOP((CustomObject) decreto.getProperties().getObjectValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_OP_PROXY)), stati);
		}
		return new ArrayList<>();
	}

	private List<String> getIdFascicoliFepaOP(final CustomObject opProxy, final List<String> stati) {
		final List<String> listaIdFascicoloFepaOp = new ArrayList<>(10);
		final IndependentObjectSet elencoOP = opProxy.getProperties().getIndependentObjectSetValue("elencoOP");
		for (final Iterator iterator2 = elencoOP.iterator(); iterator2.hasNext();) {
			final CustomObject op = (CustomObject) iterator2.next();
			if (op.getProperties().getStringValue(ID_FASCICOLO_FEPA) != null && op.getProperties().getObjectValue(STATO_OP) != null
					&& stati.contains(op.getProperties().getStringValue(STATO_OP))) {
				if (!listaIdFascicoloFepaOp.contains(op.getProperties().getStringValue(ID_FASCICOLO_FEPA))) {
					listaIdFascicoloFepaOp.add(op.getProperties().getStringValue(ID_FASCICOLO_FEPA));
					LOGGER.info(TROVATO_NUMERO_OP_INFO + op.getProperties().getInteger32Value(NUMERO_OP) + FASCICOLO_FEPA_LITERAL
							+ op.getProperties().getStringValue(ID_FASCICOLO_FEPA));
				} else {
					LOGGER.info(TROVATO_NUMERO_OP_INFO + op.getProperties().getInteger32Value(NUMERO_OP) + FASCICOLO_FEPA_LITERAL
							+ op.getProperties().getStringValue(ID_FASCICOLO_FEPA) + " non inserito perchè già in lista");
				}
			} else {
				LOGGER.info(" scartato numero op: " + op.getProperties().getInteger32Value(NUMERO_OP) + FASCICOLO_FEPA_LITERAL
						+ op.getProperties().getStringValue(ID_FASCICOLO_FEPA));
			}
		}
		return listaIdFascicoloFepaOp;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getOPIdsAllegato(com.filenet.api.core.Document).
	 */
	@Override
	public List<String> getOPIdsAllegato(final Document allegato) {
		LOGGER.info("Ricerca OP allegati al documento " + allegato.get_Name());
		final List<String> listaIdFascicoloFepaOp = new ArrayList<>(10);
		if (allegato.getProperties().getIndependentObjectSetValue("listaOP") != null) {
			final LinkSet links = (LinkSet) allegato.getProperties().getIndependentObjectSetValue("listaOP");
			for (final Iterator iterator = links.iterator(); iterator.hasNext();) {
				final Link link = (Link) iterator.next();
				if (link.get_Tail() != null) {
					LOGGER.info(TROVATO_NUMERO_OP_INFO + link.get_Tail().getProperties().getInteger32Value(NUMERO_OP) + FASCICOLO_FEPA_LITERAL
							+ link.get_Tail().getProperties().getStringValue(ID_FASCICOLO_FEPA));
					if (!listaIdFascicoloFepaOp.contains(link.get_Tail().getProperties().getStringValue(ID_FASCICOLO_FEPA))) {
						listaIdFascicoloFepaOp.add(link.get_Tail().getProperties().getStringValue(ID_FASCICOLO_FEPA));
					} else {
						LOGGER.warn("fascicolo FEPA: " + link.get_Tail().getProperties().getStringValue(ID_FASCICOLO_FEPA) + " già in elenco invii.");
					}
				} else {
					LOGGER.warn("relazione spezzata :( " + "nessun op in coda al link!");
				}
			}
		}
		return listaIdFascicoloFepaOp;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#aggiornaFatturaConDSR(com.filenet.api.core.Document,
	 *      java.util.List).
	 */
	@Override
	public void aggiornaFatturaConDSR(final Document docFattura, final List<Document> docDichiarazioneServiziResi) throws IOException {
		for (final Document doc : docDichiarazioneServiziResi) {

			if (!docFattura.getProperties().getStringListValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_ELENCO_DICHIARAZIONI)).contains(doc.get_Name())) {
				if (!doc.get_ContentElements().isEmpty()) {
					LOGGER.info("trasmissione eseguita, si aggiorna l'elenco delle DSR (" + doc.get_Name() + ") per la fattura " + docFattura.get_Name());
					final StringList elenco = docFattura.getProperties().getStringListValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_ELENCO_DICHIARAZIONI));
					elenco.add(doc.get_Name());
					docFattura.save(RefreshMode.REFRESH);
				}
			} else {
				LOGGER.info("Dichiarazione gia trasmessa per la fattura corrente.");
			}
		}
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getDocuments(java.lang.Integer,
	 *      java.lang.String, java.lang.String, boolean).
	 */
	@Override
	public DocumentSet getDocuments(final Integer idAoo, final String searchCondition, final String className, final boolean currentVersion, final boolean setCap) {
		
		String numberOfElements = Constants.EMPTY_STRING;
		if (setCap && getPP().getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS) != null) {
			numberOfElements = TOP_LABEL + getPP().getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS) + " ";
		}	
		
		String sql = "SELECT " + numberOfElements + " * " + "FROM " + className + " d " + "WHERE NOT IsClass(d,"
				+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY) + ") "
				+ (!(className.equals(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FALDONE_CLASSNAME_FN_METAKEY)))
						? " " + "AND NOT IsClass(d, " + PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FALDONE_CLASSNAME_FN_METAKEY) + ")"
						: "")
				+ " ";

		if (idAoo != null) {
			sql += " AND d.idAoo = " + idAoo + " ";
		}
		if (currentVersion) {
			sql += " AND d.IsCurrentVersion = TRUE ";
		}
		if (searchCondition != null && !"".equals(searchCondition)) {
			sql += AND_PARENTESI_DX_AP + searchCondition + " )";
		}
		final DocumentSet documents = (DocumentSet) new SearchScope(getOs()).fetchObjects(new SearchSQL(sql), 1, null, false);
		if (!documents.isEmpty()) {
			return documents;
		} else {
			LOGGER.info("Nessun risultato trovato");
			return null;
		}
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getContributiAttivi(java.lang.Integer,
	 *      java.lang.Integer, java.lang.Long).
	 */
	@Override
	public DocumentSet getContributiAttivi(final Integer numeroDocumento, final Integer annoDocumento, final Long idAoo) {
		DocumentSet ds = null;

		try {
			final String whereCondition = "d." + getPP().getParameterByKey(PropertiesNameEnum.METADATO_NUMERO_MITTENTE_CONTRIBUTO) + " = " + numeroDocumento // aggiunta da
																																								// cristiano
					+ AND_D_TABLE + getPP().getParameterByKey(PropertiesNameEnum.METADATO_ANNO_MITTENTE_CONTRIBUTO) + " = " + annoDocumento; // aggiunta da cristiano

			ds = getDocuments(idAoo.intValue(), whereCondition, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), true, false);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il controllo dei contributi attivi per il documento numero (non documentTitle): " + numeroDocumento, e);
		}

		return ds;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#associaFascicoloATitolario(java.lang.String,
	 *      java.lang.String, java.lang.Long).
	 */
	@Override
	public void associaFascicoloATitolario(final String idFascicolo, final String titolarioDestinazione, final Long idAoo) {
		LOGGER.info("associaFascicoloATitolario -> START. Fascicolo: " + idFascicolo + ". Titolario destinazione: " + titolarioDestinazione);
		final UpdatingBatch ub = UpdatingBatch.createUpdatingBatchInstance(getOs().get_Domain(), RefreshMode.REFRESH);

		final Document fascicolo = getFascicolo(idFascicolo, idAoo.intValue());

		if (fascicolo == null) {
			throw new FilenetException(ERROR_FASCICOLO_ASSENTE_MSG);
		}

		String titolarioCorrente = null;
		if (fascicolo.getProperties().getObjectValue(getPP().getParameterByKey(PropertiesNameEnum.TITOLARIO_METAKEY)) != null) {
			titolarioCorrente = fascicolo.getProperties().getStringValue(getPP().getParameterByKey(PropertiesNameEnum.TITOLARIO_METAKEY));
		}

		if (!titolarioDestinazione.equals(titolarioCorrente)) {
			final Folder folderTitolario = getFolderTitolario(titolarioDestinazione);

			if (folderTitolario == null) {
				throw new FilenetException(ERROR_FOLDER_TITOLARIO_ASSENTE_MSG);
			}

			// Associazione del fascicolo al titolario di destinatazione
			final ReferentialContainmentRelationship rr = folderTitolario.file(fascicolo, AutoUniqueName.NOT_AUTO_UNIQUE, fascicolo.get_Name(),
					DefineSecurityParentage.DO_NOT_DEFINE_SECURITY_PARENTAGE);
			ub.add(rr, null);

			// Eventuale rimozione del fascicolo dal titolario corrente
			if (titolarioCorrente != null && !FilenetStatoFascicoloEnum.NON_CATALOGATO.getNome().equals(titolarioCorrente)) {
				final Folder folder = getFolderTitolario(fascicolo.getProperties().getStringValue(getPP().getParameterByKey(PropertiesNameEnum.TITOLARIO_METAKEY)));

				if (folder == null) {
					throw new FilenetException(ERROR_FOLDER_TITOLARIO_ASSENTE_MSG);
				}

				final ReferentialContainmentRelationship rrold = folder.unfile(fascicolo);
				ub.add(rrold, null);
			}

			fascicolo.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.TITOLARIO_METAKEY), titolarioDestinazione);
			ub.add(fascicolo, null);

			LOGGER.info("associaFascicoloATitolario -> Associazione del fascicolo: " + idFascicolo + " al titolario: " + titolarioDestinazione + IN_CORSO_LITERAL);
			ub.updateBatch();
			LOGGER.info("associaFascicoloATitolario -> END. Fascicolo: " + idFascicolo + " associato al titolario: " + titolarioDestinazione);
		} else {
			LOGGER.info("associaFascicoloATitolario -> END. Fascicolo: " + idFascicolo + " già associato al titolario: " + titolarioDestinazione);
		}
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getFaldoniFascicolo(java.lang.String,
	 *      java.lang.Long).
	 */
	@Override
	public DocumentSet getFaldoniFascicolo(final String idFascicolo, final Long idAoo) {
		LOGGER.info("getFaldoniFascicolo -> START. Fascicolo: " + idFascicolo);
		DocumentSet faldoniFascicolo = null;

		try {
			final Document docFasciolo = getFascicolo(idFascicolo, idAoo.intValue());

			if (docFasciolo == null) {
				throw new FilenetException(ERROR_FASCICOLO_ASSENTE_MSG);
			}

			final Iterator<?> fSetIt = docFasciolo.get_FoldersFiledIn().iterator();
			final StringBuilder where = new StringBuilder("");
			while (fSetIt.hasNext()) {
				if (!StringUtils.isNullOrEmpty(where.toString())) {
					where.append(" OR ");
				}
				where.append(" r.Tail = OBJECT('" + ((Containable) fSetIt.next()).get_Id() + "')");
			}

			final String sql = SELECT_D_FROM + getPP().getParameterByKey(PropertiesNameEnum.FALDONE_CLASSNAME_FN_METAKEY)
					+ " d INNER JOIN ReferentialContainmentRelationship r ON d.This = r.Head WHERE d.idAOO = " + idAoo + " AND d.SecurityFolder = r.Tail AND (" + where + ")";

			faldoniFascicolo = (DocumentSet) new SearchScope(getOs()).fetchObjects(new SearchSQL(sql), null, null, false);
			LOGGER.info("getFaldoniFascicolo -> I faldoni in cui si trova il fascicolo: " + idFascicolo + " sono stati recuperati");
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dei faldoni in cui si trova il fascicolo: " + idFascicolo, e);
			throw new FilenetException("Errore nel recupero dei faldoni in cui si trova il fascicolo: " + idFascicolo, e);
		}

		LOGGER.info("getFaldoniFascicolo -> END. Fascicolo: " + idFascicolo);
		return faldoniFascicolo;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#associaFascicoloAFaldone(java.lang.String,
	 *      java.lang.String, java.lang.Long).
	 */
	@Override
	public void associaFascicoloAFaldone(final String idFascicolo, final String nomeFaldone, final Long idAoo) {
		LOGGER.info("associaFascicoloFaldone -> START. Faldone: " + nomeFaldone + ". Fascicolo: " + idFascicolo);

		if (idAoo == null) {
			throw new FilenetException("Identificativo aoo non presente.");
		}

		final Folder folderFaldone = getFolderFaldone(nomeFaldone, idAoo);

		if (folderFaldone == null) {
			throw new FilenetException("Folder faldone non presente.");
		}

		final UpdatingBatch ub = UpdatingBatch.createUpdatingBatchInstance(getOs().get_Domain(), RefreshMode.REFRESH);
		final Document documentoFascicolo = getFascicolo(idFascicolo, idAoo.intValue());

		if (documentoFascicolo == null) {
			throw new FilenetException(ERROR_FASCICOLO_ASSENTE_MSG);
		}

		documentoFascicolo.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.FALDONATO_METAKEY), true);
		ub.add(documentoFascicolo, null);

		ReferentialContainmentRelationship rr = folderFaldone.file(documentoFascicolo, AutoUniqueName.AUTO_UNIQUE,
				documentoFascicolo.get_Name(), DefineSecurityParentage.DO_NOT_DEFINE_SECURITY_PARENTAGE);
		ub.add(rr, null);

		LOGGER.info("associaFascicoloFaldone -> Associazione del fascicolo: " + idFascicolo + " al faldone: " + nomeFaldone + IN_CORSO_LITERAL);
		ub.updateBatch();
		LOGGER.info("associaFascicoloFaldone -> END. Fascicolo: " + idFascicolo + " associato al faldone: " + nomeFaldone);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#disassociaFascicoloDaFaldone(java.lang.String,
	 *      java.lang.String, java.lang.Long).
	 */
	@Override
	public void disassociaFascicoloDaFaldone(final String idFascicolo, final String nomeFaldone, final Long idAoo) {
		LOGGER.info("disassociaFascicoloDaFaldone -> START. Faldone: " + nomeFaldone + ". Fascicolo: " + idFascicolo);

		if (idAoo == null) {
			throw new FilenetException("Identificativo aoo non presente.");
		}

		final UpdatingBatch ub = UpdatingBatch.createUpdatingBatchInstance(getOs().get_Domain(), RefreshMode.REFRESH);

		final Folder folderFaldone = getFolderFaldone(nomeFaldone, idAoo);

		if (folderFaldone == null) {
			throw new FilenetException("Folder faldone non presente.");
		}

		final Document documentoFascicolo = getFascicolo(idFascicolo, idAoo.intValue());

		if (documentoFascicolo == null) {
			throw new FilenetException(ERROR_FASCICOLO_ASSENTE_MSG);
		}

		documentoFascicolo.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.FALDONATO_METAKEY), false);
		ub.add(documentoFascicolo, null);

		final ReferentialContainmentRelationship rr = folderFaldone.unfile(documentoFascicolo);
		ub.add(rr, null);

		LOGGER.info("disassociaFascicoloDaFaldone -> Disassociazione del fascicolo: " + idFascicolo + " al faldone: " + nomeFaldone + IN_CORSO_LITERAL);
		ub.updateBatch();
		LOGGER.info("disassociaFascicoloDaFaldone -> END. Fascicolo: " + idFascicolo + " disassociato dal faldone: " + nomeFaldone);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getFolderFaldone(java.lang.String,
	 *      java.lang.Long).
	 */
	@Override
	public Folder getFolderFaldone(final String nomeFaldone, final Long idAoo) {
		LOGGER.info("getFolderFaldone -> START. Faldone: " + nomeFaldone);
		Folder folder = null;

		String sql = "SELECT ID, SecurityFolder FROM " + getPP().getParameterByKey(PropertiesNameEnum.FALDONE_CLASSNAME_FN_METAKEY) + " WHERE nomeFaldone = '" + nomeFaldone
				+ AND_IS_CURRENT_VERSION_TRUE;
		if (idAoo != null) {
			sql += " AND idAOO = " + idAoo;
		}

		final DocumentSet documents = (DocumentSet) new SearchScope(getOs()).fetchObjects(new SearchSQL(sql), 1, null, false);
		final Iterator<?> it = documents.iterator();
		if (it.hasNext()) {
			final Document doc = (Document) it.next();
			folder = doc.get_SecurityFolder();
		}

		LOGGER.info("getFolderFaldone -> END. Faldone: " + nomeFaldone);
		return folder;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getLastEditableVersion(java.lang.String, java.lang.Integer).
	 */
	@Override
	public Document getLastEditableVersion(final String idDocumento, final Integer idAoo) {
		Document output = null;
		final List<?> versions = getDocumentVersionList(idDocumento, idAoo);
		// 0 rappresenta l'ultima versione del documento, avanzo di indice sino a che
		// non trovo una versione editabile
		for (int i = 0; i < versions.size(); i++) {
			final Document currentVersion = (Document) versions.get(i);
			if (ContentType.DOC.equals(currentVersion.get_MimeType()) || ContentType.DOCX.equals(currentVersion.get_MimeType())) {
				output = currentVersion;
				break;
			}
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#deleteVersion(com.filenet.api.util.Id).
	 */
	@Override
	public void deleteVersion(final Id id) {
		if (id != null) {
			final Document doc = getDocument(id);
			doc.delete();
			doc.save(RefreshMode.NO_REFRESH);
		}
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#hasSupportedFileExtensionsAttachment(com.filenet.api.core.Document, java.util.List).
	 */
	@Override
	public boolean hasSupportedFileExtensionsAttachment(final Document email, final List<String> allowedFileExtensions) {
		final String queryString = "SELECT r.Name FROM ComponentRelationship r INNER JOIN Document d on r.ChildComponent=[d].This where r.ParentComponent = OBJECT('"
				+ email.get_Id() + "') ";

		final RepositoryRowSet rrs = new SearchScope(getOs()).fetchRows(new SearchSQL(queryString), 1, null, false);

		if (rrs.isEmpty()) {
			LOGGER.info("No mail attachment found, mail id: " + email.get_Id().toString());
			return false;
		}

		String fileExtension = null;
		final Iterator<?> it = rrs.iterator();
		Boolean ret = false;
		Integer count = 0;
		String nomeFile = null;
		while (it.hasNext()) {
			final RepositoryRow childLink = (RepositoryRow) it.next();
			nomeFile = childLink.getProperties().getStringValue("Name");
			count++;
			fileExtension = FilenameUtils.getExtension(nomeFile);
			if (allowedFileExtensions.contains(fileExtension)) {
				ret = true;
			} else {
				LOGGER.info(nomeFile + " - file extension <" + fileExtension + "> not supported");
			}
			if (count > 1) {
				return false;
			}
		}

		return ret;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#sovrascriviSecurity(com.filenet.api.collection.AccessPermissionList,
	 *      com.filenet.api.collection.AccessPermissionList).
	 */
	@Override
	public void sovrascriviSecurity(final AccessPermissionList inputAcl, final AccessPermissionList newAcl) {
		inputAcl.clear();
		newAcl.add(getAdminPermission());
	}

	private Document getDocumentForAnnotations(final String idDocumento, final Long idAoo) {
		Document doc = null;

		final FilterElement feDocTitle = new FilterElement(null, null, null, getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), null);
		final FilterElement feAnnotations = new FilterElement(null, null, null, PropertyNames.ANNOTATIONS, null);

		final List<String> selectList = new ArrayList<>(2);
		selectList.add(feDocTitle.getValue());
		selectList.add(feAnnotations.getValue());

		final PropertyFilter pfDocClass = new PropertyFilter();
		pfDocClass.addIncludeProperty(feDocTitle);
		pfDocClass.addIncludeProperty(feAnnotations);

		// Si ottiene il documento
		doc = getDocumentByIdGestionale(idDocumento, selectList, pfDocClass, idAoo.intValue(), null, null);

		return doc;
	}

	/**
	 * Controlla se il documento possiede un'annotation Preview
	 * 
	 * @param idDocumento
	 * @param idAoo
	 * @return
	 */
	@Override
	public boolean isDocumentoConPreview(final String idDocumento, final Long idAoo) {
		boolean documentoPreview = false;
		LOGGER.info("isDocumentoConPreview -> START. Controllo annotation TEXT_PREVIEW per il documento: " + idDocumento);

		// Si recupera il documento (Document Title e lista di Annotation)
		final Document doc = getDocumentForAnnotations(idDocumento, idAoo);

		if (doc == null) {
			throw new FilenetException(ERROR_DOCUMENTO_ASSENTE_MSG);
		}

		if (doc.get_Annotations() != null) {
			final Iterator<?> iter = doc.get_Annotations().iterator();
			while (iter.hasNext()) {
				final Annotation annObject = (Annotation) iter.next();
				if (FilenetAnnotationEnum.TEXT_PREVIEW.getNome().equals(annObject.get_DescriptiveText())) {
					documentoPreview = true;
					break;
				}
			}
		}

		LOGGER.info("isDocumentoConPreview -> END. Annotation TEXT_PREVIEW" + (documentoPreview ? " " : " NON ") + "presente" + PER_IL_DOCUMENTO + idDocumento);
		return documentoPreview;
	}

	/**
	 * Metodo che crea un annotation a partire da un file che non è il content del documento.
	 * 
	 * @param idDocumento
	 * @param guid
	 * @param annotationType
	 * @param contentPreview
	 * @param creazione
	 * @param idAoo
	 */
	@Override
	public void creaAnnotation(final String idDocumento, final String guid, final FilenetAnnotationEnum annotationType, final InputStream contentPreview,
			final boolean creazione, final Long idAoo) {
		try {
			LOGGER.info("creaAnnotation -> START. Creazione annotazione: " + annotationType + PER_IL_DOCUMENTO + idDocumento);
			final Document doc = getDocumentForAnnotations(idDocumento, idAoo);

			final Date dataInizio = new Date();
			if (creazione) {
				// ### Modalità Creazione
				creaESalvaAnnotation(idDocumento, doc, annotationType, contentPreview, "PRE_" + idDocumento);
			} else {

				if (doc == null) {
					throw new FilenetException(ERROR_DOCUMENTO_ASSENTE_MSG);
				}

				// ### Modalità Modifica
				final Iterator<?> iterAnnotations = doc.get_Annotations().iterator();

				// Creazione ex-novo e salvataggio
				if (!iterAnnotations.hasNext()) {
					creaESalvaAnnotation(idDocumento, doc, annotationType, contentPreview, "PRE_" + idDocumento);
					// Modifica e salvataggio
				} else {
					while (iterAnnotations.hasNext()) {
						final Annotation annObject = (Annotation) iterAnnotations.next();
						if (annotationType.getNome().equals(annObject.get_DescriptiveText())) {
							final ContentTransfer ctObject = Factory.ContentTransfer.createInstance();
							ctObject.setCaptureSource(contentPreview);
							annObject.get_ContentElements().set(0, ctObject);
							annObject.save(RefreshMode.NO_REFRESH);
							break;
						}
					}
				}
			}
			final Date dataFine = new Date();

			LOGGER.info(
					"creaAnnotation -> Timing creazione: " + creazione + PER_IL_DOCUMENTO + idDocumento + " ===> " + (dataFine.getTime() - dataInizio.getTime() + "ms"));
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la creazione di un annotation: " + annotationType + PER_IL_DOCUMENTO + idDocumento, e);
			throw new FilenetException("Si è verificato un errore durante la creazione di un annotation: " + annotationType + PER_IL_DOCUMENTO + idDocumento, e);
		}

		LOGGER.info("creaAnnotation -> END. Creazione annotazione: " + annotationType + PER_IL_DOCUMENTO + idDocumento + " completata.");
	}

	private void creaESalvaAnnotation(final String idDocumento, final Document doc, final FilenetAnnotationEnum annotationType, final InputStream annotationContent,
			final String retrievalName) {
		// Si crea la ContentElementList
		final ContentElementList list = Factory.ContentElement.createList();
		final ContentTransfer element = Factory.ContentTransfer.createInstance();
		element.set_ContentType(MediaType.PDF.toString());
		element.set_RetrievalName(retrievalName);
		element.setCaptureSource(annotationContent);
		list.add(element);

		Integer elementSequenceNumber = null;
		try {
			elementSequenceNumber = ((ContentElement) list.get(0)).get_ElementSequenceNumber();
		} catch (final Exception ex) {
			LOGGER.warn("Non è stato possibile estrarre l'elementSequenceNumber dal ContentElement del documento: " + idDocumento, ex);
		}

		final Annotation annObject = costruisciAnnotationFilenet(doc, annotationType, annotationContent, getOs(),
				elementSequenceNumber, MediaType.PDF.toString(), null);
		annObject.save(RefreshMode.NO_REFRESH);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#verificaProtocollo(java.util.List, int, int, java.lang.String, int).
	 */
	@Override
	public Document verificaProtocollo(final List<String> classiDocumentali, final int annoProtocollo, final int numProtocollo, final String idProtocollo,
			final int tipoProtocollo) {
		final String campoDocumentTitle = getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
		final String campoIdProtocollo = getPP().getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY);
		final String campoNumeroProtocollo = getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
		final String campoAnnoProtocollo = getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
		final String campoTipoProtocollo = getPP().getParameterByKey(PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY);
		final String campoDataProtocollo = getPP().getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY);
		final String campoDataAnnullamento = getPP().getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_DATA_ANNULLAMENTO);
		final String campoDestinatari = getPP().getParameterByKey(PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY);
		final String campoidTipologiaDocumento = getPP().getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY);
		final String campoidTipologiaProcedimento = getPP().getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY);

		final String select = SELECT_D + PropertyNames.ID + ", " + " d." + PropertyNames.DATE_CREATED + ", " + " d." + campoDestinatari + ", " + " d." + campoDocumentTitle
				+ ", " + " d." + campoNumeroProtocollo + ", " + " d." + campoAnnoProtocollo + ", " + " d." + campoTipoProtocollo + ", " + " d." + campoDataProtocollo + ", "
				+ " d." + campoDataAnnullamento + ", " + " d." + campoidTipologiaDocumento + ", " + " d." + campoidTipologiaProcedimento + " ";

		String whereProtocollo = "((" + campoNumeroProtocollo + " = " + numProtocollo + AND + campoAnnoProtocollo + " = " + annoProtocollo + ")";
		if (StringUtils.isNullOrEmpty(idProtocollo)) {
			whereProtocollo += " OR " + campoIdProtocollo + " = '" + idProtocollo + "')";
		} else {
			whereProtocollo += ")";
		}

		final String where = WHERE_D + PropertyNames.IS_CURRENT_VERSION + " = true AND ( " + whereProtocollo + AND + campoTipoProtocollo + " = " + tipoProtocollo
				+ AND + campoDataAnnullamento + " IS NULL )";
		String sql = null;
		DocumentSet ds = null;
		for (final String classeDocumentale : classiDocumentali) {

			sql = select + " from " + classeDocumentale + " d " + where;
			LOGGER.info(sql);
			final SearchSQL searchSQL = new SearchSQL(sql);
			final SearchScope scope = new SearchScope(getOs());

			final Boolean continuable = true;
			ds = (DocumentSet) scope.fetchObjects(searchSQL, PAGE_SIZE, null, continuable);
			if (ds != null && !ds.isEmpty()) {
				break;
			}
		}
		if (ds != null && !ds.isEmpty()) {
			return getFirstDocument(ds);
		} else {
			return null;
		}
	}

	/**
	 * @param idDocumento
	 * @param newClasseDocumentale
	 * @param newMetadatiDocumento
	 * @param newDataHandler
	 * @param idAoo
	 * @return
	 */
	@Override
	public Document cambiaClasseDocumentale(final String idDocumento, final String newClasseDocumentale, final Map<String, Object> newMetadatiDocumento,
			final DataHandler newDataHandler, final Long idAoo) {
		Document docRet = null;
		Document doc = null;

		try {
			InputStream contentDoc = null;
			String contentTypeDoc = null;
			doc = getDocumentByIdGestionale(idDocumento, null, null, idAoo.intValue(), null, null);

			if (hasDocumentContentTransfer(doc)) {
				contentDoc = doc.accessContentStream(0);
			}
			if (contentDoc != null && newDataHandler == null) {
				contentTypeDoc = doc.get_MimeType();
			} else if (newDataHandler != null) {
				contentDoc = newDataHandler.getInputStream();
				contentTypeDoc = newDataHandler.getContentType();
			}

			checkout(doc);
			final Document docRes = (Document) doc.get_Reservation();
			// Si cambia la classe documentale
			docRes.changeClass(newClasseDocumentale);
			checkin(docRes, contentDoc, null, null, newMetadatiDocumento, docRes.get_Name(), contentTypeDoc, idAoo);
			docRet = docRes;
		} catch (final Exception e) {
			if (doc != null) {
				LOGGER.error("Errore durante il checkin/checkout del documento con GUID: " + doc.get_Id(), e);
			}
			throw new FilenetException(e);
		}

		return docRet;
	}

	/**
	 * Il metodo ritorna la lista dei documenti ricercati con i parametri passati in
	 * input.
	 * 
	 * @param inValoreDaRicercare
	 * @param type
	 * @param idAoo
	 * @param anno
	 * @return
	 */
	@Override
	public final DocumentSet ricercaGenericaFascicoli(final String inValoreDaRicercare, final RicercaGenericaTypeEnum type, final Long idAoo, final String anno) {
		String valoreDaRicercare = inValoreDaRicercare;
		final List<String> gruppiAD = getGruppiAD();

		String operator = "OR";
		if (RicercaGenericaTypeEnum.TUTTE.equals(type)) {
			operator = "AND";
		}

		// Campi DB
		final String campoIndiceClassificazione = getPP().getParameterByKey(PropertiesNameEnum.TITOLARIO_METAKEY); // titolario
		final String campoOggetto = getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY); // oggetto
		final String dataCreazione = getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY); // DateCreated

		valoreDaRicercare = valoreDaRicercare.toLowerCase(); // Gestione case insensitive
		final String[] valoriDaRicercare = StringUtils.deleteSpecialCharacterForSpace(valoreDaRicercare).split(" ");

		final StringBuilder whereCondition = new StringBuilder();

		if (RicercaGenericaTypeEnum.ESATTA.equals(type)) {
			final StringBuilder toSearch = new StringBuilder("");
			for (int i = 0; i < valoriDaRicercare.length; i++) {
				// aggiunge lo spazio in caso di più parole
				if (i != 0) {
					toSearch.append(" ");
				}
				toSearch.append(valoriDaRicercare[i]);
			}
			whereCondition.append(LOWER + campoIndiceClassificazione + PAR_CHIUSA_UGUALE + toSearch + "'");
			whereCondition.append(OR_LOWER + campoOggetto + PAR_CHIUSA_LIKE_MODUL + toSearch + "%'");
		} else {
			for (int i = 0; i < valoriDaRicercare.length; i++) {
				if (!Constants.EMPTY_STRING.equals(valoriDaRicercare[i])) {
					if (whereCondition.length() > 0) {
						if (!"".equals(operator) && "AND".equals(operator)) {
							whereCondition.append(" ) " + operator + " ( ");
						} else {
							whereCondition.append(" OR ");
						}
					}
					whereCondition.append(LOWER + campoIndiceClassificazione + PAR_CHIUSA_UGUALE + StringUtils.getIndiceClassificazione(idAoo, valoriDaRicercare[i]) + "'");
					whereCondition.append(OR_LOWER + campoOggetto + PAR_CHIUSA_LIKE_MODUL + valoriDaRicercare[i] + "%'");
				}
			}
		}
		String whereConditionPart = Constants.EMPTY_STRING;

		String controlloAnno = "";
		if (!StringUtils.isNullOrEmpty(anno)) {
			controlloAnno += (AND + dataCreazione + " >= " + anno + "0101T000000Z");
			controlloAnno += (AND + dataCreazione + " <= " + anno + "1231T235959Z");
		}
		if (Constants.EMPTY_STRING.equals(whereCondition.toString())) {
			if (gruppiAD != null && !gruppiAD.isEmpty()) {
				whereConditionPart = " WHERE ACL INTERSECTS " + StringUtils.getACLClause(gruppiAD) + AND_ID_AOO + idAoo;
			}
		} else {
			whereConditionPart = " WHERE ( " + whereCondition + " )" + controlloAnno + AND_ID_AOO + idAoo + AND_ACL_INTERSECTS + StringUtils.getACLClause(gruppiAD);
		}

		final String campi = getPP().getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY) + ", "
				+ getPP().getParameterByKey(PropertiesNameEnum.STATO_FASCICOLO_METAKEY) + ", " + getPP().getParameterByKey(PropertiesNameEnum.TITOLARIO_METAKEY) + ", "
				+ getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY) + ", " + getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY) + ", "
				+ getPP().getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA) + ", "
				+ getPP().getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_TIPO_FASCICOLO_FEPA_METAKEY) + ", "
				+ getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY) + ", " + getPP().getParameterByKey(PropertiesNameEnum.FALDONATO_METAKEY) + ", "
				+ getPP().getParameterByKey(PropertiesNameEnum.DATA_TERMINAZIONE_METAKEY) + ", " + getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY) + ", "
				+ PropertyNames.ID + " ";
		final String sqlStr = SELECT + campi + FROM + getPP().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY) + whereConditionPart;

		final SearchSQL sql = new SearchSQL(sqlStr);

		// Search the object store with the sql and get the returned items
		final SearchScope searchScope = new SearchScope(getOs());
		return (DocumentSet) searchScope.fetchObjects(sql, null, null, false);
	}

	/**
	 * @param idDocumenti
	 * @return
	 */
	@Override
	public Map<String, DetailDocumentRedDTO> getDocumentiByIdList(final List<String> idDocumenti) {
		final Map<String, DetailDocumentRedDTO> hashDocumenti = new HashMap<>();

		final String campoDocumentTitle = getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);

		final String campoAnnoProtocollo = getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
		final String campoNumeroProtocollo = getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_MITTENTE_METAKEY);
		final String campoCategoriaDocumento = getPP().getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY);

		final StringBuilder queryString = new StringBuilder();
		queryString.append(SELECT + campoDocumentTitle + " , " + campoCategoriaDocumento + " , " + campoAnnoProtocollo + " , " + campoNumeroProtocollo);
		queryString.append(FROM + getPP().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));
		queryString.append(WHERE_IS_CURRENT_VERSION_TRUE);

		// Se gli ID Documenti sono valorizzati allora filtro anche per quelli
		if (idDocumenti != null && !idDocumenti.isEmpty()) {
			int limit = idDocumenti.size();
			if (limit > Constants.Varie.MAXLENGTH_SQL_QUERY_IN) {
				limit = Constants.Varie.MAXLENGTH_SQL_QUERY_IN;
			}

			final StringBuilder whereCondition = new StringBuilder(AND).append(campoDocumentTitle).append(" IN (");

			for (int i = 0; i < limit; i++) {
				whereCondition.append("'").append(idDocumenti.get(i)).append("',");
			}

			whereCondition.substring(0, whereCondition.length() - 1);
			whereCondition.append(")");

			queryString.append(whereCondition);
		}

		final SearchSQL searchSQL = new SearchSQL();
		searchSQL.setQueryString(queryString.toString());
		final SearchScope scope = new SearchScope(getOs());

		final DocumentSet documents = (DocumentSet) scope.fetchObjects(searchSQL, 500, null, true);
		final Iterator<?> it = documents.iterator();
		DetailDocumentRedDTO documentoRed = null;
		while (it.hasNext()) {

			try {
				final Document doc = (Document) it.next();
				final Properties prop = doc.getProperties();
				documentoRed = new DetailDocumentRedDTO();

				documentoRed.setIdTipologiaDocumento(Integer.parseInt(prop.getStringValue(campoDocumentTitle)));
				if (prop.getInteger32Value(campoNumeroProtocollo) != null) {
					documentoRed.setNumeroProtocollo(prop.getInteger32Value(campoNumeroProtocollo));
				}
				if (prop.getInteger32Value(campoAnnoProtocollo) != null) {
					documentoRed.setAnnoProtocollo(prop.getInteger32Value(campoAnnoProtocollo));
				}
				documentoRed.setIdCategoriaDocumento(prop.getInteger32Value(campoCategoriaDocumento));

				hashDocumenti.put(prop.getStringValue(campoDocumentTitle), documentoRed);
			} catch (final Exception e) {
				LOGGER.error("Attezione, errore nel recupero del documento: " + documentoRed, e);
			}
		}

		return hashDocumenti;
	}

	/**
	 * Metodo per ottenere la prima versione degli allegati.
	 * 
	 * @param documentTitle
	 *            Identificativo del documento dal quale ottenere la prima versione
	 *            degli allegati.
	 * @return Lista degli allegati con content prima versione.
	 */
	@Override
	public Collection<Document> getAllegatiConContentFirstVersion(final String documentTitle) {
		final Collection<Document> allegatiFirstVersion = new ArrayList<>();
		final DocumentSet allegati = getAllegatiConContent(documentTitle);
		if (allegati != null && !allegati.isEmpty()) {
			final Iterator<Document> iter = allegati.iterator();
			Document allegato = null;
			while (iter.hasNext()) {
				allegato = iter.next();
				final SubSetImpl ssi = (SubSetImpl) allegato.get_VersionSeries().get_Versions();
				final List<?> list = ssi.getList();
				final Document allegatoFirstVersion = (Document) list.get(list.size() - 1);
				allegatiFirstVersion.add(allegatoFirstVersion);
			}
		}
		return allegatiFirstVersion;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getAllegatiConContentFirstVersion(com.filenet.api.core.Document).
	 */
	@Override
	public Collection<Document> getAllegatiConContentFirstVersion(final Document d) {
		
		return getAllegatiConContentFirstVersion(d.get_Id().toString());
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getDocumentFromCEByIdApplicativo(java.lang.String, int).
	 */
	@Override
	public DetailDocumentRedDTO getDocumentFromCEByIdApplicativo(final String iddocument, final int idAoo) {

		final Document doc = getDocumentByIdGestionale(iddocument, null, null, idAoo, null, null);
		if (doc == null) {
			return null;
		}
		final DetailDocumentRedDTO documento = new DetailDocumentRedDTO();
		documento.setDocumentTitle(iddocument);

		if (doc.get_MimeType() != null) {
			final ContentElementList ceList = doc.get_ContentElements();
			if (ceList != null && !ceList.isEmpty()) {
				final ContentTransfer ct = (ContentTransfer) ceList.get(0);
				final String contentType = ct.get_ContentType();
				documento.setMimeType(contentType);
			}
		}
		documento.setDocumentClass(doc.getClassName());
		documento.setMajorVersionNumber(doc.get_MajorVersionNumber());
		documento.setGuid(doc.get_Id().toString());
		return documento;

	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#ricercaGenericaFaldoni(java.lang.String,
	 *      it.ibm.red.business.enums.RicercaGenericaTypeEnum, java.lang.Long,
	 *      java.lang.String, java.lang.Long, java.lang.Integer, java.lang.Long,
	 *      java.lang.String, java.lang.String, boolean).
	 */
	@Override
	public DocumentSet ricercaGenericaFaldoni(final String inValoreDaRicercare, final RicercaGenericaTypeEnum type, final Long idAoo, final String anno,
			final Long idUfficioUtente, final Integer idUfficioCorriereUtente, final Long idUtente, final String utenteUsername, final String visFaldoni,
			final boolean onlyOggetto) {

		DocumentSet faldoni = null;
		String sql = null;
		String valoreDaRicercare = inValoreDaRicercare;
		try {
			List<String> gruppiAD = getGruppiAD();

			gruppiAD = restrictVisibility(idUtente, null, null, idUfficioUtente.intValue(), idUfficioCorriereUtente, null, gruppiAD, true, true, false, visFaldoni);

			String operator = "OR";
			if (RicercaGenericaTypeEnum.TUTTE.equals(type)) {
				operator = "AND";
			}

			final String faldoneDocumentTitle = getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_DOCUMENTTITLE);
			final String faldoneNome = getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_NOMEFALDONE);
			final String faldoneParent = getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_PARENTFALDONE);
			final String faldoneOggetto = getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_OGGETTO);
			final String faldoneDescrizione = getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_DESCRIZIONEFALDONE);
			final String faldoneDataCreazione = getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY);

			final String selectList = "Name, SecurityFolder, " + faldoneDocumentTitle + "," + faldoneNome + "," + faldoneParent + "," + faldoneOggetto + ","
					+ faldoneDescrizione + "," + faldoneDataCreazione;

			if (StringUtils.isNullOrEmpty(valoreDaRicercare)) {
				valoreDaRicercare = "";
			}

			valoreDaRicercare = valoreDaRicercare.toLowerCase(); // Gestione case insensitive
			final String[] valoriDaRicercare = StringUtils.deleteSpecialCharacterForSpace(valoreDaRicercare).split(" ");

			final StringBuilder whereCondition = new StringBuilder();

			if (RicercaGenericaTypeEnum.ESATTA.equals(type)) {
				final StringBuilder toSearch = new StringBuilder("");
				for (int i = 0; i < valoriDaRicercare.length; i++) {
					// aggiunge lo spazio in caso di più parole
					if (i != 0) {
						toSearch.append(" ");
					}
					toSearch.append(valoriDaRicercare[i]);
				}
				whereCondition.append(LOWER + faldoneOggetto + PAR_CHIUSA_LIKE_MODUL + toSearch + "%'");
				if (!onlyOggetto) {
					whereCondition.append(OR_LOWER + faldoneNome + PAR_CHIUSA_LIKE_MODUL + toSearch + "%'");
					whereCondition.append(OR_LOWER + faldoneDescrizione + PAR_CHIUSA_LIKE_MODUL + toSearch + "%'");
				}
			} else {
				for (int i = 0; i < valoriDaRicercare.length; i++) {
					if (!Constants.EMPTY_STRING.equals(valoriDaRicercare[i])) {
						if (whereCondition.length() > 0) {
							if (!"".equals(operator) && "AND".equals(operator)) {
								whereCondition.append(" ) " + operator + " ( ");
							} else {
								whereCondition.append(" OR ");
							}
						}
						whereCondition.append(LOWER + faldoneOggetto + PAR_CHIUSA_LIKE_MODUL + valoriDaRicercare[i] + "%'");
						if (!onlyOggetto) {
							whereCondition.append(OR_LOWER + faldoneNome + PAR_CHIUSA_LIKE_MODUL + valoriDaRicercare[i] + "%'");
							whereCondition.append(OR_LOWER + faldoneDescrizione + PAR_CHIUSA_LIKE_MODUL + valoriDaRicercare[i] + "%'");
						}
					}
				}
			}
			String whereConditionPart = Constants.EMPTY_STRING;

			String controlloAnno = "";
			if (!StringUtils.isNullOrEmpty(anno)) {
				controlloAnno += (AND + faldoneDataCreazione + " >= " + anno + "0101T000000Z");
				controlloAnno += (AND + faldoneDataCreazione + " <= " + anno + "1231T235959Z");
			}

			if (Constants.EMPTY_STRING.equals(whereCondition.toString())) {
				if (gruppiAD != null && !gruppiAD.isEmpty()) {
					whereConditionPart = " WHERE ACL INTERSECTS " + StringUtils.getACLClause(gruppiAD) + AND_ID_AOO + idAoo;
				}
			} else {
				whereConditionPart = " where ( " + whereCondition + " )" + controlloAnno + AND_ID_AOO + idAoo + AND_ACL_INTERSECTS
						+ StringUtils.getACLClause(gruppiAD);
			}

			whereConditionPart += " AND IsCurrentVersion = TRUE ";

			sql = "SELECT ID, CREATOR, OWNER, ISRESERVED, " + selectList + FROM + getPP().getParameterByKey(PropertiesNameEnum.FALDONE_CLASSNAME_FN_METAKEY)
					+ whereConditionPart;

			sql += ORDER_BY + getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_OGGETTO) + " ASC";

			LOGGER.info(QUERY_DA_ESEGUIRE + sql);

			faldoni = (DocumentSet) new SearchScope(getOs()).fetchObjects(new SearchSQL(sql), null, null, false);

			return faldoni;

		} catch (final Exception e) {
			throw new RedException(" Errore durante il recupero del Faldone per la ricerca ", e);
		}
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getNotifichePEC(java.lang.String,
	 *      java.lang.String, java.lang.String,
	 *      it.ibm.red.business.persistence.model.Aoo, boolean).
	 */
	@Override
	public Map<String, EmailDTO> getNotifichePEC(final String idDocumento, final String mittente, final String path, final Aoo aoo, final boolean isTabSpedizione) {
		final HashMap<String, EmailDTO> result = new HashMap<>();

		// Verifica la presenza di messageid collegato al documento
		
		List<AnnotationDTO> annotationList = getAnnotationsDocument(idDocumento, false, FilenetAnnotationEnum.TEXT_MAIL_MESSAGE_ID, aoo.getIdAoo(), false);
		annotationList = annotationList.stream().sorted().collect(Collectors.toList());

		LOGGER.info("recuperate " + annotationList.size() + " annotation di tipo " + FilenetAnnotationEnum.TEXT_MAIL_MESSAGE_ID.getNome() + PER_IL_DOCUMENTO_LITERAL
				+ idDocumento);

		byte[] contentMail = null;
		String messageID = null;
		if (!CollectionUtils.isEmpty(annotationList)) {
			for (final AnnotationDTO annotation : annotationList) {
				try {
					contentMail = IOUtils.toByteArray(annotation.getContent());
					if (contentMail != null && contentMail.length > 0) {
						messageID = new String(contentMail);

						final List<EmailDTO> mailList = getNotificheByMessageID(messageID, path);
						LOGGER.info("recuperate " + mailList.size() + " notifiche con messageID " + messageID + PER_IL_DOCUMENTO_LITERAL + idDocumento);

						for (int i = 0; i < mailList.size(); i++) {
							final EmailDTO mailApp = mailList.get(i);
							LOGGER.info("notifica " + mailApp.getOggetto() + PER_IL_DOCUMENTO_LITERAL + idDocumento);
							if (mailApp.getMittente() != null && mailApp.getOggetto() != null && TipoNotificaPecEnum.checkPrefissoNotifichePec(mailApp.getOggetto(),
									TipoNotificaPecEnum.ACCETTAZIONE, TipoNotificaPecEnum.NON_ACCETTAZIONE, TipoNotificaPecEnum.NON_ACCETTAZIONE_VIRUS)) {

								if (isTabSpedizione) {
									if (result.containsKey(mittente.toLowerCase() + "_MIT")) {
										result.put(mittente.toLowerCase() + "#" + messageID, mailApp);
									} else {
										result.put(mittente.toLowerCase() + "_MIT", mailApp);
									}
								} else {
									result.put(mittente.toLowerCase() + "#" + messageID + "_MIT", mailApp);
								}
								
								
							} else if (mailApp.getMittente() != null && mailApp.getOggetto() != null
									&& TipoNotificaPecEnum.checkPrefissoNotifichePec(mailApp.getOggetto(), TipoNotificaPecEnum.AVVENUTA_CONSEGNA,
											TipoNotificaPecEnum.PRESA_IN_CARICO, TipoNotificaPecEnum.ERRORE_CONSEGNA, TipoNotificaPecEnum.PREAVVISO_ERRORE_CONSEGNA)) {

								result.put(mailApp.getMittente().toLowerCase() + "#" + messageID, mailApp);

							}
						}
					}
				} catch (final IOException e) {
					LOGGER.error("Errore in fase di recupero delle notifiche PEC per il documento: " + idDocumento, e);
					throw new FilenetException("Errore in fase di recupero delle notifiche PEC", e);
				}
			}
		}

		return result;
	}

	/**
	 * Il metodo restituisce la notifica PEC relativa al messageID in input.
	 * 
	 * @param idMessage
	 * @param path
	 * @return
	 */
	private List<EmailDTO> getNotificheByMessageID(final String idMessage, final String path) {
		final StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT d.ID FROM " + getPP().getParameterByKey(PropertiesNameEnum.MAIL_CLASSNAME_FN_METAKEY) + " d INNER JOIN");
		queryString.append(" ReferentialContainmentRelationship r ON d.This = r.Head");
		queryString.append(" WHERE r.Tail = OBJECT('" + path + "') ");
		queryString.append(AND + getPP().getParameterByKey(PropertiesNameEnum.MESSAGE_ID) + " = '" + idMessage + "'");

		final SearchSQL sql = new SearchSQL(queryString.toString());
		final SearchScope searchScope = new SearchScope(getOs());
		final DocumentSet documents = (DocumentSet) searchScope.fetchObjects(sql, null, null, false);

		final Iterator<?> it = documents.iterator();
		final List<EmailDTO> result = new ArrayList<>();

		while (it.hasNext()) {
			final Document doc = getDocument(((Document) it.next()).get_Id());

			final Properties prop = doc.getProperties();

			final EmailDTO mail = new EmailDTO();
			mail.setMittente(prop.getStringValue(getPP().getParameterByKey(PropertiesNameEnum.FROM_MAIL_METAKEY)));
			mail.setOggetto(prop.getStringValue(getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_EMAIL_METAKEY)));

			try {
				final ContentElementList ceList = doc.get_ContentElements();
				final ContentTransfer ct = (ContentTransfer) ceList.get(0);
				final InputStream docContent = ct.accessContentStream();
				mail.setTesto(new String(IOUtils.toByteArray(docContent)));
			} catch (final Exception e) {
				LOGGER.warn(e);
				mail.setTesto(null);
			}

			mail.setGuid(doc.get_Id().toString());
			mail.setDataRicezione(prop.getDateTimeValue(getPP().getParameterByKey(PropertiesNameEnum.DATA_RICEZIONE_MAIL_METAKEY)));
			mail.setMsgID(prop.getStringValue(getPP().getParameterByKey(PropertiesNameEnum.MESSAGE_ID)));
			mail.setDestinatari(prop.getStringValue(getPP().getParameterByKey(PropertiesNameEnum.DESTINATARI_EMAIL_METAKEY)));

			result.add(mail);
		}

		return result;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getNotificheInteroperabilitaByProtocollo(java.lang.String, int, int).
	 */
	@Override
	public final DocumentSet getNotificheInteroperabilitaByProtocollo(final String idProtocollo, final int numeroProtocollo, final int annoProtocollo) {
		DocumentSet notificheInteropFilenet = null;

		try {
			final StringBuilder queryString = new StringBuilder().append(SELECT_D).append(PropertyNames.ID).append(", d.")
					.append(getPP().getParameterByKey(PropertiesNameEnum.MESSAGE_ID)).append(", d.\"").append(getPP().getParameterByKey(PropertiesNameEnum.FROM_MAIL_METAKEY))
					.append("\", d.").append(getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_EMAIL_METAKEY)).append(", d.")
					.append(getPP().getParameterByKey(PropertiesNameEnum.DATA_RICEZIONE_MAIL_METAKEY)).append(", d.").append(PropertyNames.CONTENT_ELEMENTS).append(", d.")
					.append(getPP().getParameterByKey(PropertiesNameEnum.DESTINATARI_EMAIL_METAKEY)).append(FROM)
					.append(getPP().getParameterByKey(PropertiesNameEnum.NOTIFICA_INTEROP_CLASSNAME_FN_METAKEY)).append(" d").append(WHERE_D)
					.append(getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY)).append(" = ").append(numeroProtocollo).append(AND_D_TABLE)
					.append(getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY)).append(" = ").append(annoProtocollo);

			if (!StringUtils.isNullOrEmpty(idProtocollo)) {
				queryString.append(AND_D_TABLE).append(getPP().getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY)).append(" = '").append(idProtocollo).append("'");
			}

			final SearchScope searchScope = new SearchScope(getOs());
			notificheInteropFilenet = (DocumentSet) searchScope.fetchObjects(new SearchSQL(queryString.toString()), null, null, false);

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore in fase di recupero delle notifiche di interoperabilità per il protocollo: " + numeroProtocollo + FOLDER_PATH_DELIMITER + annoProtocollo
					+ (!StringUtils.isNullOrEmpty(idProtocollo) ? ", ID: " + idProtocollo : Constants.EMPTY_STRING), e);
			throw new FilenetException("Errore durante il recupero delle notifiche di interoperabilità", e);
		}

		return notificheInteropFilenet;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getCountFascicoliNonClassificati(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.Integer, java.util.List, java.lang.Boolean, java.lang.Boolean,
	 *      java.lang.Boolean).
	 */
	@Override
	public final Integer getCountFascicoliNonClassificati(final UtenteDTO utente, final Integer idUfficioCorriereUtente, final List<String> gruppiAD,
			final Boolean aclCorriere, final Boolean aclNodo, final Boolean aclUtente) {

		Integer output = null;
		List<String> acl = gruppiAD;

		try {
			acl = restrictVisibility(null, idUfficioCorriereUtente, utente.getIdUfficio(), null, null, utente.getUsername(), acl, aclCorriere, aclNodo, aclUtente,
					utente.getVisFaldoni());

			final String alias = "f";
			final String className = getPP().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY) + " " + alias;

			final String where = " idAOO = " + utente.getIdAoo() + AND + getPP().getParameterByKey(PropertiesNameEnum.TITOLARIO_METAKEY) + " = 'Non Catalogato' "
					+ AND_ACL_INTERSECTS + StringUtils.getACLClause(acl);

			output = count(alias, className, where);

		} catch (final Exception e) {
			LOGGER.error("Errore durante la count dei fascicoli non classificati", e);
			throw new FilenetException(" Si è verificato un errore durante la count delle mail.", e);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getFascicoliNonClassificati(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.Integer, java.util.List, java.lang.Boolean, java.lang.Boolean,
	 *      java.lang.Boolean).
	 */
	@Override
	public DocumentSet getFascicoliNonClassificati(final UtenteDTO utente, final Integer idUfficioCorriereUtente, final List<String> gruppiAD, final Boolean aclCorriere,
			final Boolean aclNodo, final Boolean aclUtente) {
		List<String> acl = gruppiAD;

		acl = restrictVisibility(null, idUfficioCorriereUtente, utente.getIdUfficio(), null, null, utente.getUsername(), acl, aclCorriere, aclNodo, aclUtente,
				utente.getVisFaldoni());

		final String sql = SELECT + getPP().getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY) + ","
				+ getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY) + "," + getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY) + ","
				+ getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY) + FROM + getPP().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY)
				+ WHERE + getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY) + " = " + utente.getIdAoo() + AND
				+ getPP().getParameterByKey(PropertiesNameEnum.TITOLARIO_METAKEY) + " = 'Non Catalogato' " + AND_ACL_INTERSECTS + StringUtils.getACLClause(acl)
				+ ORDER_BY + getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY) + DESC;

		return (DocumentSet) new SearchScope(getOs()).fetchObjects(new SearchSQL(sql), null, null, false);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getFascicoliByIndice(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.Integer, java.util.List, java.lang.Boolean, java.lang.Boolean,
	 *      java.lang.Boolean, java.lang.String).
	 */
	@Override
	public DocumentSet getFascicoliByIndice(final UtenteDTO utente, final Integer idUfficioCorriereUtente, final List<String> gruppiAD, final Boolean aclCorriere,
			final Boolean aclNodo, final Boolean aclUtente, final String indiceDiClassificazione) {
		List<String> acl = gruppiAD;

		acl = restrictVisibility(null, idUfficioCorriereUtente, utente.getIdUfficio(), null, null, utente.getUsername(), acl, aclCorriere, aclNodo, aclUtente,
				utente.getVisFaldoni());

		final StringBuilder sql = new StringBuilder().append(SELECT).append(getPP().getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY)).append(", ")
				.append(getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY)).append(", ")
				.append(getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY)).append(", ").append(getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY))
				.append(", ").append(getPP().getParameterByKey(PropertiesNameEnum.STATO_FASCICOLO_METAKEY)).append(FROM)
				.append(getPP().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY)).append(WHERE)
				.append(getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY)).append(" = ").append(utente.getIdAoo()).append(AND)
				.append(getPP().getParameterByKey(PropertiesNameEnum.TITOLARIO_METAKEY)).append(" IN ('").append(utente.getIdAoo()).append("_").append(indiceDiClassificazione)
				.append("') ").append(AND_ACL_INTERSECTS + StringUtils.getACLClause(acl)).append(ORDER_BY)
				.append(getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY)).append(DESC);

		return (DocumentSet) new SearchScope(getOs()).fetchObjects(new SearchSQL(sql.toString()), null, null, false);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#restrictVisibility(java.lang.Long,
	 *      java.lang.Integer, java.lang.Long, java.lang.Integer, java.lang.Integer,
	 *      java.lang.String, java.util.List, boolean, boolean, boolean,
	 *      java.lang.String).
	 */
	@Override
	public List<String> restrictVisibility(final Long idUtente, final Integer idUfficioCorriereUtente, final Long idUfficioUtente, final Integer idUfficioSelezionato,
			final Integer idUfficioCorriereSelezionato, final String username, final List<String> gruppiAD, final boolean aclCorriere, final boolean aclNodo,
			final boolean aclUtente, final String visFaldoni) {

		// Verifico che la visibilità dell'aoo sia totale
		if (Constants.Aoo.VIS_FALDONI_TOTALE.equals(visFaldoni)) {
			return gruppiAD;
		}

		// Altrimenti applico la visibilità ristretta
		final List<String> output = new ArrayList<>();
		final String groupPrefix = getPP().getParameterByKey(PropertiesNameEnum.GROUP_SECURITY_PREFIX);
		final String[] groupPrefixies = groupPrefix.split("\\|");

		if (idUfficioSelezionato == null) {
			for (final String g : gruppiAD) {
				for (final String pref : groupPrefixies) {
					if ((aclNodo && g.toLowerCase().contains(pref.toLowerCase() + idUfficioUtente + "@")) || (aclCorriere
							&& (g.toLowerCase().contains(pref.toLowerCase() + idUfficioCorriereUtente + "@") || g.toLowerCase().contains(username.toLowerCase() + "@")))
							|| (aclUtente && g.toLowerCase().contains(username.toLowerCase() + "@"))) {
						output.add(g);
						break;
					}
				}
			}
		} else {
			// Devo reperire i soli faldoni del nodo scelto
			final String[] arrDomain = gruppiAD.get(0).split("@");
			final String domain = "@" + arrDomain[1];

			for (final String pref : groupPrefixies) {
				if (aclNodo) {
					output.add(pref.toLowerCase() + idUfficioSelezionato + domain);
				}

				if (aclCorriere) {
					output.add(pref.toLowerCase() + idUfficioCorriereSelezionato + domain);
				}

				if (aclUtente) {
					output.add(pref.toLowerCase() + idUtente + domain);
				}
			}
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getFascicoliByFaldone(java.lang.String, java.util.List).
	 */
	@Override
	public RepositoryRowSet getFascicoliByFaldone(final String pathFaldone, final List<String> gruppiAD) {
		String queryString = SELECT + getPP().getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY) + ", "
				+ getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY) + ", " + " d." + PropertyNames.DATE_CREATED + ", " + " d."
				+ getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY) + FROM + getPP().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY)
				+ " d INNER JOIN ReferentialContainmentRelationship r ON d.This = r.Head WHERE r.Tail = OBJECT('" + pathFaldone + "')";

		LOGGER.info("getFascicoliByFaldone 1" + queryString);

		if (gruppiAD != null && !gruppiAD.isEmpty()) {
			queryString += (" AND d.ACL INTERSECTS " + StringUtils.getACLClause(gruppiAD) + ORDER_BY + getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY)
					+ " ASC");
		}
		LOGGER.info("getFascicoliByFaldone 2" + queryString);
		return new SearchScope(this.getOs()).fetchRows(new SearchSQL(queryString), 500, null, true);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#deleteFaldone(java.lang.Long, java.lang.String).
	 */
	@Override
	public void deleteFaldone(final Long idAoo, final String documentTitle) {
		final Document ceDocument = getFaldoneByDocumentTitle(documentTitle, idAoo.intValue());
		if (ceDocument != null) {
			ceDocument.get_VersionSeries().delete();
			ceDocument.get_VersionSeries().save(RefreshMode.REFRESH);
		} else {
			final String msg = "Errore in fase di cancellazione, faldone non esistente.";
			LOGGER.error(msg);
			throw new RedException(msg);
		}
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getFaldoneByDocumentTitle(java.lang.String, java.lang.Integer).
	 */
	@Override
	public Document getFaldoneByDocumentTitle(final String documentTitle, final Integer idAoo) {
		Document output = null;

		String sql = SELECT_FROM + getPP().getParameterByKey(PropertiesNameEnum.FALDONE_CLASSNAME_FN_METAKEY) + WHERE
				+ getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_DOCUMENTTITLE) + " = '" + documentTitle + "' AND IsCurrentVersion = TRUE ";

		if (idAoo != null) {
			sql += " AND idAOO=" + idAoo;
		}

		final DocumentSet documents = (DocumentSet) new SearchScope(getOs()).fetchObjects(new SearchSQL(sql), 1, null, false);

		final Iterator<?> it = documents.iterator();
		if (it.hasNext()) {
			output = (Document) it.next();
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getFaldoneChildren(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.Boolean, java.lang.Integer,
	 *      java.lang.Integer, java.lang.Integer, java.util.List, java.lang.Boolean,
	 *      java.lang.Boolean, java.lang.Boolean).
	 */
	@Override
	public DocumentSet getFaldoneChildren(final UtenteDTO utente, final String documentTitle, final Boolean bAooVisTotale, final Integer idUfficioCorriereUtente,
			final Integer idUfficioSelezionato, final Integer idUfficioCorriereSelezionato, final List<String> gruppiAD, final Boolean aclCorriere, final Boolean aclNodo,
			final Boolean aclUtente) {
		List<String> acl = gruppiAD;
		if (bAooVisTotale != null && !bAooVisTotale) {
			acl = restrictVisibility(utente.getId(), idUfficioCorriereUtente, utente.getIdUfficio(), idUfficioSelezionato, idUfficioCorriereSelezionato, utente.getUsername(),
					acl, aclCorriere, aclNodo, aclUtente, utente.getVisFaldoni());
		}
		final String selectList = SELECT + PropertyNames.NAME + ", " + PropertyNames.DATE_CREATED + ", " + PropertyNames.SECURITY_FOLDER + ", "
				+ getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_DOCUMENTTITLE) + ","
				+ getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_NOMEFALDONE) + ","
				+ getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_PARENTFALDONE) + "," + getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_OGGETTO)
				+ "," + getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_DESCRIZIONEFALDONE);

		String sql = selectList + " from " + getPP().getParameterByKey(PropertiesNameEnum.FALDONE_CLASSNAME_FN_METAKEY) + WHERE_IS_CURRENT_VERSION_TRUE;

		if (StringUtils.isNullOrEmpty(documentTitle)) {
			sql += AND + getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_PARENTFALDONE) + " IS NULL";
		} else {
			sql += AND + getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_PARENTFALDONE) + " ='" + documentTitle + "'";
		}

		sql += AND_ACL_INTERSECTS + StringUtils.getACLClause(acl);
		sql += " AND idAOO = " + utente.getIdAoo() + ORDER_BY + getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_OGGETTO) + " ASC";

		return (DocumentSet) new SearchScope(getOs()).fetchObjects(new SearchSQL(sql), null, null, false);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#createFaldone(java.lang.Integer,
	 *      java.lang.Integer, java.lang.String, java.lang.String, java.lang.String,
	 *      java.lang.String).
	 */
	@Override
	public void createFaldone(final Integer idAoo, final Integer idUfficio, final String nomeFaldone, final String parentFaldone, final String oggetto,
			final String descrizione) {
		try {
			if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(nomeFaldone)) {
				throw new RedException("Faldone non valido: nome faldone mancante");
			}

			// Aggiungi come security solamente l'ufficio del creatore
			final ListSecurityDTO security = new ListSecurityDTO();
			final SecurityDTO sec = new SecurityDTO();
			sec.getGruppo().setIdUfficio(idUfficio);
			security.add(sec);

			final Document d = Factory.Document.createInstance(getOs(), getPP().getParameterByKey(PropertiesNameEnum.FALDONE_CLASSNAME_FN_METAKEY));
			final UpdatingBatch ub = UpdatingBatch.createUpdatingBatchInstance(getOs().get_Domain(), RefreshMode.REFRESH);

			final ClassDefinition ts = Factory.ClassDefinition.fetchInstance(getOs(), getPP().getParameterByKey(PropertiesNameEnum.FALDONE_CLASSNAME_FN_METAKEY), null);

			final List<Metadato> metadati = new ArrayList<>();
			metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_NOMEFALDONE), nomeFaldone));
			metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_OGGETTO), oggetto));
			metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_DESCRIZIONEFALDONE), descrizione));
			metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_IDAOO), String.valueOf(idAoo)));
			metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), nomeFaldone));
			metadati.add(new Metadato(getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_PARENTFALDONE), parentFaldone));
			setPropertiesDoc(metadati, d.getProperties(), ts);

			d.checkin(AutoClassify.DO_NOT_AUTO_CLASSIFY, CheckinType.MAJOR_VERSION);
			ub.add(d, null);
			final String parentFolder = getPP().getParameterByKey(PropertiesNameEnum.FALDONE_PARENT_FOLDER_PREFIX) + Constants.Varie.AOO_PREFIX + idAoo;
			final Folder folderFaldone = aggiungiFolder(nomeFaldone, security, parentFolder);
			ub.add(folderFaldone, null);
			final ReferentialContainmentRelationship rr = folderFaldone.file(d, AutoUniqueName.AUTO_UNIQUE, nomeFaldone, DefineSecurityParentage.DEFINE_SECURITY_PARENTAGE);
			ub.add(rr, null);
			ub.updateBatch();
		} catch (final Exception e) {
			throw new FilenetException(" Errore durante la creazione del faldone", e);
		}
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#updateMetadatiFaldone(java.lang.Long, java.lang.String, java.util.Map).
	 */
	@Override
	public void updateMetadatiFaldone(final Long idAoo, final String documentTitle, final Map<String, Object> metadati) {
		final Document ceDocument = getFaldoneByDocumentTitle(documentTitle, idAoo.intValue());
		if (ceDocument != null) {
			checkout(ceDocument);
			final Document docRes = (Document) ceDocument.get_Reservation();
			checkin(docRes, null, null, null, metadati, null, null, idAoo);
		} else {
			final String strErrorMsg = "Errore in fase di recupero del documento [idAoo=" + idAoo + ", idDocumento=" + documentTitle + "]";
			LOGGER.error(strErrorMsg);
			throw new RedException(strErrorMsg);
		}
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#inserisciAllegati(java.lang.String,
	 *      java.util.List, java.lang.Long).
	 */
	@Override
	public void inserisciAllegati(final String idDocumento, final List<DocumentoRedFnDTO> allegati, final Long idAoo) {
		final Document documentoPrincipaleFilenet = getDocumentByIdGestionale(idDocumento, null, null, idAoo.intValue(), null, null);

		inserisciAllegati(documentoPrincipaleFilenet, allegati);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#inserisciAllegati(com.filenet.api.core.Document,
	 *      java.util.List).
	 */
	@Override
	public void inserisciAllegati(final Document documentoPrincipale, final List<DocumentoRedFnDTO> allegati) {
		if (!CollectionUtils.isEmpty(allegati)) {
			impostaCompoundDocument(documentoPrincipale);
			impostaFigliCompoundDocument(documentoPrincipale, allegati);
		}
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#inserisciAllegatoOP(com.filenet.api.core.Document,
	 *      it.ibm.red.business.dto.filenet.DocumentoRedFnDTO).
	 */
	@Override
	public void inserisciAllegatoOP(final Document documentoPrincipale, final DocumentoRedFnDTO allegato) {
		impostaCompoundDocument(documentoPrincipale);
		impostaFigliCompoundDocumentPerOP(documentoPrincipale, allegato);
	}

	/**
	 * Trasforma un documento in compound document.
	 * 
	 * @param doc
	 */
	private void impostaCompoundDocument(final Document doc) {
		try {
			LOGGER.info("### IMPOSTO COMPOUNT PRINC##");
			doc.set_CompoundDocumentState(CompoundDocumentState.COMPOUND_DOCUMENT);
			doc.save(RefreshMode.REFRESH);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il set del compound document", e);
			throw new FilenetException("Errore durante il set del compound document", e);
		}
	}

	private void impostaFigliCompoundDocument(final Document docPrincipale, final List<DocumentoRedFnDTO> docFigli) {
		LOGGER.info("### IMPOSTO COMPOUNT FIGLI START##");
		try {
			if (!CollectionUtils.isEmpty(docFigli)) {
				Integer order = 1;
				for (final DocumentoRedFnDTO docFiglio : docFigli) {
					
					final Document docFiglioFilenet = getDocFiglioFilenet(docPrincipale, docFiglio);
					final ComponentRelationship cr = updateRelationShipComponent(docPrincipale, docFiglio, docFiglioFilenet);

					cr.set_ComponentSortOrder(order++);
				}
			}

			LOGGER.info("### IMPOSTO COMPOUNT FIGLI END##");
		} catch (final Exception e) {
			throw new FilenetException(ERROR_CREAZIONE_FIGLI_MSG, e);
		}
	}

	/**
	 * Aggiorna e restituisce il component della relationship.
	 * 
	 * @param docPrincipale
	 * @param docFiglio
	 * @param docFiglioFilenet
	 * @return Relationship Component aggiornato.
	 */
	private ComponentRelationship updateRelationShipComponent(final Document docPrincipale,
			final DocumentoRedFnDTO docFiglio, final Document docFiglioFilenet) {
		
		final ComponentRelationship cr = Factory.ComponentRelationship.createInstance(getOs(), null);
		cr.set_ParentComponent(docPrincipale);
		cr.set_ChildComponent(docFiglioFilenet);
		cr.set_ComponentRelationshipType(ComponentRelationshipType.DYNAMIC_CR);
		cr.set_VersionBindType(VersionBindType.LATEST_VERSION);
		cr.set_Name(ALLEGATO_DOCUMENTO_LABEL + docFiglio.getDocumentTitle());
		cr.save(RefreshMode.REFRESH);
		return cr;
	}

	/**
	 * Recupera il documento figlio Filenet dal documento figlio e ne aggiorna le
	 * properties..
	 * 
	 * @param docPrincipale
	 * @param docFiglio
	 * @return Documento figlio Filenet.
	 * @throws IOException
	 */
	private Document getDocFiglioFilenet(final Document docPrincipale, final DocumentoRedFnDTO docFiglio)
			throws IOException {
		
		final ClassDefinition ts = Factory.ClassDefinition.fetchInstance(getOs(), docFiglio.getClasseDocumentale(), null);
		final Document docFiglioFilenet = Factory.Document.createInstance(getOs(), ts.get_SymbolicName());

		String nomeFile = docFiglio.getDocumentTitle();
		LOGGER.info("### IMPOSTO COMPOUNT FIGLI nomeFile: " + nomeFile);
		if (!CollectionUtils.isEmpty(docFiglio.getMetadati())) {
			nomeFile = (String) docFiglio.getMetadati().get(getPP().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY));
			setPropertiesAndSaveDocument(docFiglioFilenet, docFiglio.getMetadati(), false);
		}

		if (docFiglio.getDataHandler() != null && docFiglio.getDataHandler().getInputStream() != null) {
			setContentDoc(docFiglioFilenet, nomeFile, docFiglio.getContentType(), docFiglio.getDataHandler().getInputStream(), null);
		}

		// Si impostano le security del padre
		docFiglioFilenet.set_Permissions(clonePermissions(docPrincipale));
		docFiglioFilenet.checkin(null, CheckinType.MAJOR_VERSION);
		docFiglioFilenet.save(RefreshMode.REFRESH);
		return docFiglioFilenet;
	}

	private void impostaFigliCompoundDocumentPerOP(final Document docPrincipale, final DocumentoRedFnDTO docFiglio) {
		try {
			final Document docFiglioFilenet = getDocFiglioFilenet(docPrincipale, docFiglio);
			updateRelationShipComponent(docPrincipale, docFiglio, docFiglioFilenet);
			associaAllegatoToOP(docPrincipale, docFiglioFilenet);
		} catch (final Exception e) {
			throw new FilenetException(ERROR_CREAZIONE_FIGLI_MSG, e);
		}
	}

	/**
	 * @param docPrincipale
	 * @param allegato
	 * @param contentAllegato
	 * @param impostaMetadati
	 * @param idAoo
	 */
	@Override
	public void aggiornaAllegato(final DocumentoRedFnDTO docPrincipale, final DocumentoRedFnDTO allegato, final InputStream contentAllegato, final boolean impostaMetadati,
			final Long idAoo) {
		// Si ottiene il documento principale
		Document docPrincipaleFilenet = null;
		if (!StringUtils.isNullOrEmpty(docPrincipale.getDocumentTitle())) {
			docPrincipaleFilenet = getDocumentByIdGestionale(docPrincipale.getDocumentTitle(), null, null, idAoo.intValue(), null, null);
		} else {
			docPrincipaleFilenet = getDocumentByGuid(docPrincipale.getGuid());
		}

		aggiornaAllegato(docPrincipaleFilenet, allegato, contentAllegato, impostaMetadati, idAoo);
	}

	/**
	 * @param docPrincipaleFilenet
	 * @param allegato
	 * @param contentAllegato
	 * @param impostaMetadati
	 */
	@Override
	public void aggiornaAllegato(final Document docPrincipaleFilenet, final DocumentoRedFnDTO allegato, final InputStream contentAllegato, final boolean impostaMetadati,
			final Long idAoo) {
		// Si cerca l'allegato di interesse tramite il nome file
		final String nomeFile = (String) allegato.getMetadati().get(getPP().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY));
		Document allegatoFilenet = getAllegatoFromDocumentByDocumentTitle(docPrincipaleFilenet, nomeFile);

		// Se non lo si trova, si riprova a cercarlo tramite il Document Title
		if (allegatoFilenet == null) {
			final String documentTitle = (String) allegato.getMetadati().get(getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
			allegatoFilenet = getAllegatoFromDocumentByDocumentTitle(docPrincipaleFilenet, documentTitle);
		}

		// Si esegue la modifica dell'allegato
		addVersionCompoundDocument(allegatoFilenet, contentAllegato, allegato.getContentType(), allegato.getMetadati(), impostaMetadati, idAoo);
	}

	/**
	 * @param documentoPrincipale
	 * @param docTitleAllegato
	 * @return
	 */
	private Document getAllegatoFromDocumentByDocumentTitle(final Document documentoPrincipale, final String docTitleAllegato) {
		Document allegato = null;

		try {
			final ComponentRelationshipSet crs = documentoPrincipale.get_ChildRelationships();

			if (crs != null) {

				final Iterator<?> it = crs.iterator();
				while (it.hasNext()) {
					final ComponentRelationship cr = (ComponentRelationship) it.next();
					final Document allegatoDocument = cr.get_ChildComponent();
					if (allegatoDocument != null && !StringUtils.isNullOrEmpty(docTitleAllegato) && docTitleAllegato
							.equals(allegatoDocument.getProperties().getStringValue(getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY)))) {
						allegato = allegatoDocument;
						cr.set_CopyToReservation(Boolean.TRUE);
						cr.save(RefreshMode.REFRESH);
						break;
					}
				}

			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dell'allegato: " + docTitleAllegato + " dal documento", e);
			throw new FilenetException("Errore durante il recupero dell'allegato: " + docTitleAllegato + " dal documento", e);
		}

		return allegato;
	}

	/**
	 * @param guidDocumentoPrincipale
	 * @param docTitleAllegato
	 * @return
	 */
	@Override
	public final Document getAllegatoFromDocumentByDocumentTitle(final String guidDocumentoPrincipale, final String docTitleAllegato) {
		return getAllegatoFromDocumentByDocumentTitle(getDocumentByGuid(guidDocumentoPrincipale), docTitleAllegato);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#addVersionCompoundDocument(com.filenet.api.core.Document,
	 *      java.io.InputStream, java.lang.String, java.util.Map, boolean,
	 *      java.lang.Long).
	 */
	@Override
	public void addVersionCompoundDocument(final Document document, final InputStream content, final String mimeType, final Map<String, Object> metadati,
			final boolean impostaMetadati, final Long idAoo) {
		try {
			if (content != null) {
				// Check-out del documento
				checkout(document);

				final Document docRes = (Document) document.get_Reservation();
				final String nomeFile = document.getProperties().getStringValue(getPP().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY));

				// Check-in del documento
				checkin(docRes, content, null, null, metadati, nomeFile, mimeType, idAoo);
			} else if (!CollectionUtils.isEmpty(metadati) && impostaMetadati) {
				setPropertiesAndSaveDocument(document, metadati, true);
			}
		} catch (final Exception e) {
			if (content != null) {
				// Cancellazione del checkout
				cancelCheckout(document);
			}
			LOGGER.error("Errore nel versionamento dell'allegato.", e);
			throw new FilenetException("Errore nel versionamento dell'allegato.", e);
		}
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#cancelCheckout(com.filenet.api.core.Document).
	 */
	@Override
	public void cancelCheckout(final Document doc) {
		try {
			final Document canlDoc = (Document) doc.cancelCheckout();
			canlDoc.save(RefreshMode.REFRESH);
		} catch (final Exception e) {
			LOGGER.error("Errore durante la cancellazione del checkout del documento.", e);
			throw new FilenetException("Errore durante la cancellazione del checkout del documento.", e);
		}
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#isCheckOut(com.filenet.api.core.Document).
	 */
	@Override
	public boolean isCheckOut(final Document doc) {
		Boolean result = Boolean.TRUE;

		try {

			if (doc != null && !doc.getProperties().getBooleanValue(PropertyNames.IS_RESERVED)
					&& Boolean.TRUE.equals(doc.getProperties().getBooleanValue(PropertyNames.IS_CURRENT_VERSION))) {

				result = Boolean.FALSE;
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante la verifica del checkout del documento.", e);
			throw new FilenetException("Errore durante la verifica del checkout del documento.", e);
		}

		return result;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#eliminaAllegatiByGuid(java.lang.String, java.util.List).
	 */
	@Override
	public List<Document> eliminaAllegatiByGuid(final String guid, final List<String> allegatiGuid) {
		final Document documentoPrincipaleFilenet = getDocumentByGuid(guid);

		return eliminaAllegatiByGuid(documentoPrincipaleFilenet, allegatiGuid);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#eliminaAllegatiByGuid(com.filenet.api.core.Document,
	 *      java.util.List).
	 */
	@Override
	public List<Document> eliminaAllegatiByGuid(final Document documentoPrincipale, final List<String> allegatiDaEliminareGuid) {
		LOGGER.info("eliminaAllegatiByGuid -> START");
		final ComponentRelationshipSet crs = documentoPrincipale.get_ChildRelationships();
		final List<Document> allegatiEliminati = new ArrayList<>();

		try {
			if (crs != null) {
				final Iterator<?> it = crs.iterator();
				int countTotal = 0;
				int countDeleted = 0;
				while (it.hasNext()) {
					countTotal++;
					final ComponentRelationship cr = (ComponentRelationship) it.next();
					final Document allegatoDocument = cr.get_ChildComponent();
					final String allegatoFilenetGuid = allegatoDocument.get_Id().toString();

					for (final String allegatoDaEliminareGuid : allegatiDaEliminareGuid) {
						if (allegatoDaEliminareGuid.equals(allegatoFilenetGuid)) {
							final SubSetImpl versioniAllegato = (SubSetImpl) allegatoDocument.get_VersionSeries().get_Versions();
							final List<?> listaVersioniAllegato = versioniAllegato.getList();
							Document versioneAllegato = null;
							for (final Object versione : listaVersioniAllegato) {
								versioneAllegato = (Document) versione;
								if (versioneAllegato == null) {
									continue;
								}
								versioneAllegato.delete();
							}

							cr.delete();
							cr.save(RefreshMode.REFRESH);
							allegatiEliminati.add(allegatoDocument);
							countDeleted++;
							break;
						}
					}
				}
				documentoPrincipale.save(RefreshMode.REFRESH);

				// Sono stati eliminati tutti gli allegati del documento
				if (countDeleted == countTotal) {
					documentoPrincipale.set_CompoundDocumentState(CompoundDocumentState.STANDARD_DOCUMENT);
					documentoPrincipale.save(RefreshMode.REFRESH);
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la cancellazione degli allegati.", e);
			throw new FilenetException("Errore durante la cancellazione degli allegati.", e);
		}

		LOGGER.info("eliminaAllegatiByGuid -> END");
		return allegatiEliminati;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getPreviousVersionWordFileName(java.lang.String).
	 */
	@Override
	public String getPreviousVersionWordFileName(final String guid) {
		String nomeFile = null;

		final Document doc = getLastEditableVersion(guid);
		if (doc != null && doc.getProperties() != null && doc.getProperties().isPropertyPresent(getPP().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY))) {
			nomeFile = doc.getProperties().getStringValue(getPP().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY));
		}

		return nomeFile;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getPreviousVersionWordRevisionNumber(java.lang.String).
	 */
	@Override
	public Integer getPreviousVersionWordRevisionNumber(final String guid) {
		Integer previous = null;

		if (!StringUtils.isNullOrEmpty(guid)) {
			final Document document = getLastEditableVersion(guid);
			if (document != null) {
				previous = document.get_MajorVersionNumber();
			}
		}

		return previous;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getLastEditableVersion(java.lang.String).
	 */
	@Override
	public Document getLastEditableVersion(final String guid) {
		final Document doc = Factory.Document.fetchInstance(getOs(), new Id(guid), null);
		return getLastEditableVersion(doc);
	}

	private Document getLastEditableVersion(final Document doc) {
		Document previous = getPreviousVersion(doc);

		if (previous != null && previous.getProperties() != null) {
			String mimeType = previous.get_MimeType();

			while (MediaType.PDF.toString().equalsIgnoreCase(mimeType)) {
				previous = getPreviousVersion(previous);
				if (previous == null) {
					return null;
				}
				mimeType = previous.get_MimeType();
			}
		}

		return previous;
	}

	private Document getPreviousVersion(final Document doc) {
		Document previous = null;

		final Iterator<?> itVersions = doc.get_Versions().iterator();
		final int majorV = doc.get_MajorVersionNumber();
		final int minorV = doc.get_MinorVersionNumber() == 0 ? doc.get_MinorVersionNumber() : doc.get_MinorVersionNumber() - 1;
		while (itVersions.hasNext()) {
			final Document itPrev = (Document) itVersions.next();
			if (!itPrev.get_Id().equals(doc.get_Id()) && (itPrev.get_MajorVersionNumber() <= majorV && itPrev.get_MinorVersionNumber() == minorV)) {
				previous = itPrev;
				break;
			}
		}

		if (previous != null) {
			previous.refresh();
		}

		return previous;
	}

	/**
	 * Recupera tutte le proprietà della classe dato il nome della classe
	 * documentale.
	 * 
	 * @param className
	 * @param withSystemProperties
	 * @return
	 */
	@Override
	public final List<PropertyDefinition> getClassProperties(final String className, final boolean withSystemProperties) {
		final List<PropertyDefinition> list = new ArrayList<>();

		try {
			final ClassDefinition cls = Factory.ClassDefinition.fetchInstance(getOs(), className, null);
			final PropertyDefinitionList propLst = cls.get_PropertyDefinitions();

			final Iterator<?> propIter = propLst.iterator();
			while (propIter.hasNext()) {
				final PropertyDefinition prop = (PropertyDefinition) propIter.next();
				if (!withSystemProperties || Boolean.TRUE.equals(prop.get_IsSystemOwned())) {
					// Con le property di sistema le metto tutte, altrimenti metto solo quelle non
					// di sistema
					list.add(prop);
				}
			}
		} catch (final FilenetException e) {
			throw new FilenetException("Si è verificato un error durante il recupero delle proprietà di classe ", e);
		}

		return list;

	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#associaAllegatoAOrdineDiPagamento(com.filenet.api.core.Document,
	 *      com.filenet.api.core.Document).
	 */
	@Override
	public void associaAllegatoAOrdineDiPagamento(final Document decreto, final Document allegatoDecreto) {
		IndependentObjectSet elencoOrdiniDiPagamento = null;

		final CustomObject registroOrdiniDiPagamento = (CustomObject) decreto.getProperties().getObjectValue("opProxy");
		if (registroOrdiniDiPagamento != null) {
			elencoOrdiniDiPagamento = registroOrdiniDiPagamento.getProperties().getIndependentObjectSetValue("elencoOP");

			if (elencoOrdiniDiPagamento != null) {
				CustomObject ordineDiPagamento = null;

				final Iterator<?> itOrdiniDiPagamento = elencoOrdiniDiPagamento.iterator();
				while (itOrdiniDiPagamento.hasNext()) {
					ordineDiPagamento = (CustomObject) itOrdiniDiPagamento.next();
					if (!"Annullato".equals(ordineDiPagamento.getProperties().getStringValue(STATO_OP))) {
						final Link link = Factory.Link.createInstance(getOs(), "AllegatiOPLink");
						link.getProperties().putObjectValue("Head", allegatoDecreto);
						link.getProperties().putObjectValue("Tail", ordineDiPagamento);
						link.save(RefreshMode.REFRESH);
					}
				}
			}
		}
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getIdDocumentoByMetadatoFepa(java.lang.String,
	 *      java.lang.String).
	 */
	@Override
	public String getIdDocumentoByMetadatoFepa(final String key, final String value) {
		final StringBuilder sbQuery = new StringBuilder();
		final StringBuilder whereCondition = new StringBuilder();
		whereCondition.append(" " + key + " = '" + value + "'");
		String iddocumento = null;
		sbQuery.append(SELECT);
		sbQuery.append(" DateCreated");
		sbQuery.append(" ," + getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
		sbQuery.append(FROM + getPP().getParameterByKey(PropertiesNameEnum.FATTURA_FEPA_CLASSNAME_FN_METAKEY));
		sbQuery.append(WHERE);
		sbQuery.append(whereCondition);
		final SearchSQL sql = new SearchSQL(sbQuery.toString());
		LOGGER.info(" query " + sbQuery.toString());
		final SearchScope searchScope = new SearchScope(getOs());
		final DocumentSet documents = (DocumentSet) searchScope.fetchObjects(sql, null, null, false);
		final Iterator<?> it = documents.iterator();
		while (it.hasNext()) {
			final Document doc = (Document) it.next();
			final Properties prop = doc.getProperties();
			iddocumento = prop.getStringValue(getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
		}
		return iddocumento;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getDefaultInstancePermissions(java.lang.String).
	 */
	@Override
	public AccessPermissionList getDefaultInstancePermissions(final String className) {
		final ClassDefinition documentClassDefinition = Factory.ClassDefinition.fetchInstance(getOs(), className, null);
		return translateAcl(documentClassDefinition.get_DefaultInstancePermissions());
	}

	private AccessPermissionList translateAcl(final AccessPermissionList classAcl) {
		final AccessPermissionList acl = Factory.AccessPermission.createList();

		final Iterator<?> itClassAcl = classAcl.iterator();
		while (itClassAcl.hasNext()) {
			final AccessPermission accessPermission = (AccessPermission) itClassAcl.next();

			String accessPermissionName = accessPermission.get_GranteeName();
			if (!AUTHENTICATED_USERS.equals(accessPermissionName) && !"#CREATOR-OWNER".equals(accessPermissionName)
					&& !SecurityPrincipalType.UNKNOWN.equals(accessPermission.get_GranteeType())) {
				if (accessPermissionName.indexOf(',') != -1) {
					accessPermissionName = accessPermissionName.substring(3, accessPermissionName.indexOf(',')).toLowerCase();
				}

				final AccessPermission newAccessPermission = Factory.AccessPermission.createInstance();
				newAccessPermission.set_GranteeName(accessPermissionName);
				newAccessPermission.set_AccessMask(accessPermission.get_AccessMask());
				newAccessPermission.set_AccessType(accessPermission.get_AccessType());
				newAccessPermission.set_InheritableDepth(accessPermission.get_InheritableDepth());

				acl.add(newAccessPermission);
			}
		}

		return acl;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getFascicoliPerRicercaDocumenti(java.lang.String,
	 *      java.lang.String, java.lang.Long).
	 */
	@Override
	public DocumentSet getFascicoliPerRicercaDocumenti(final String nomeFascicolo, final String titolario, final Long idAoo) {
		final String campoNomeFascicolo = getPP().getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY);
		final String campoTitolario = getPP().getParameterByKey(PropertiesNameEnum.TITOLARIO_METAKEY);
		final String campoOggettoFascicolo = getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY);

		final StringBuilder whereCondition = new StringBuilder();
		if (!StringUtils.isNullOrEmpty(nomeFascicolo) && !StringUtils.isNullOrEmpty(titolario)) {
			whereCondition.append(LOWER + campoNomeFascicolo + PAR_CHIUSA_UGUALE + nomeFascicolo.toLowerCase() + "' AND " + campoTitolario + " = '" + titolario + "'");
		} else if (!StringUtils.isNullOrEmpty(nomeFascicolo)) {
			whereCondition.append(LOWER + campoOggettoFascicolo + PAR_CHIUSA_LIKE_MODUL + nomeFascicolo.toLowerCase() + "%'");
		} else {
			whereCondition.append(campoTitolario + " = '" + titolario + "'");
		}

		final String sqlStr = "SELECT ID, " + campoNomeFascicolo + FROM + getPP().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY) + WHERE
				+ getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY) + " = " + idAoo + AND + whereCondition;

		final SearchSQL sql = new SearchSQL(sqlStr);

		final SearchScope searchScope = new SearchScope(getOs());
		return (DocumentSet) searchScope.fetchObjects(sql, null, null, false);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#ricercaAvanzataDocumenti(java.util.Map,
	 *      java.util.Set, it.ibm.red.business.dto.UtenteDTO, boolean,
	 *      java.lang.String, boolean).
	 */
	@Override
	public DocumentSet ricercaAvanzataDocumenti(final Map<String, Object> mappaValoriRicerca, final Set<String> idDocumenti, final UtenteDTO utente,
			final boolean orderbyInQuery, final String acl, final boolean setCap) {
		return ricercaAvanzata(mappaValoriRicerca, idDocumenti, utente, orderbyInQuery, acl, setCap);
	}

	/**
	 * Metodo per la ricerca avanzata documenti.
	 * 
	 * @param mappaValoriRicerca
	 * @param idDocumenti
	 * @param setCap
	 * @param utente
	 * @param orderbyInQuery
	 * @param acl
	 * @return
	 */
	private DocumentSet ricercaAvanzata(final Map<String, Object> mappaValoriRicerca, final Set<String> idDocumenti, final UtenteDTO utente, final boolean orderbyInQuery,
			final String acl, final boolean setCap) {
		LOGGER.info("ricercaAvanzataDocumenti -> START");
		DocumentSet documentiRicerca = null;
		final StringBuilder queryFullText = new StringBuilder();
		final StringBuilder whereCondition = new StringBuilder();

		try {
			final String classeDocumentale = (String) mappaValoriRicerca.get(Ricerca.CLASSE_DOCUMENTALE_FWS);
			mappaValoriRicerca.remove(Ricerca.CLASSE_DOCUMENTALE_FWS);

			// Campi del database
			final String campoTipologiaDocumento = getPP().getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY);
			final String campoDocumentTitle = getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
			final String numeroDocumento = getPP().getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
			final String campoBarcode = getPP().getParameterByKey(PropertiesNameEnum.BARCODE_METAKEY);
			final String campoOggetto = getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY);
			final String campoNumeroProtocollo = getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
			final String campoNumeroProtocolloEmergenza = getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_EMERGENZA_METAKEY);
			final String campoDataProtocollo = getPP().getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY);
			final String campoDataProtocolloEmergenza = getPP().getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_EMERGENZA_METAKEY);
			final String campoAnnoProtocollo = getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
			final String campoAnnoProtocolloEmergenza = getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_EMERGENZA_METAKEY);
			final String campoNomeFile = getPP().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY);
			final String campoCategoriaDocumento = getPP().getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY);
			final String campoSottoCategoriaDocUscita = getPP().getParameterByKey(PropertiesNameEnum.SOTTOCATEGORIA_DOCUMENTO_USCITA);
			final String campoDataAnnullamentoDocumento = getPP().getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_DATA_ANNULLAMENTO);
			final String campoAnnoDocumento = getPP().getParameterByKey(PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY);
			final String campoTipoProtocollo = getPP().getParameterByKey(PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY);
			final String campoRiservato = getPP().getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY);
			final String campoDataScadenza = getPP().getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY);
			final String destinatariDocumento = getPP().getParameterByKey(PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY);
			final String mittente = getPP().getParameterByKey(PropertiesNameEnum.MITTENTE_METAKEY);
			final String campoTipoProcedimento = getPP().getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY);
			final String campoRegistroProtocollo = getPP().getParameterByKey(PropertiesNameEnum.REGISTRO_PROTOCOLLO_FN_METAKEY);
			final String codiceFlusso = getPP().getParameterByKey(PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY);
			final String integrazioneDati = getPP().getParameterByKey(PropertiesNameEnum.INTEGRAZIONE_DATI_METAKEY);

			final String verificaFirmaDocAllegato = getPP().getParameterByKey(PropertiesNameEnum.VERIFICA_FIRMA_DOC_ALLEGATO);
			final String metadatoDocumentoValFirma = getPP().getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_VALIDITA_FIRMA);
			final String versionNumber = getPP().getParameterByKey(PropertiesNameEnum.MAJOR_VERSION_NUMBER_METAKEY);
			
			// ### COSTRUZIONE WHERE CONDITION -> START
			// Si calcola la IN condition
			String inCondition = Constants.EMPTY_STRING;
			if (!CollectionUtils.isEmpty(idDocumenti)) {
				inCondition = StringUtils.createInCondition(campoDocumentTitle, idDocumenti.iterator(), true);
			}

			final Set<String> chiaviRicerca = mappaValoriRicerca.keySet();
			final Iterator<String> itChiaviRicerca = chiaviRicerca.iterator();

			whereCondition.append("WHERE IsCurrentVersion = TRUE ");

			final String datada = "";
			final String dataa = "";

			handleChiaviRicerca(mappaValoriRicerca, queryFullText, whereCondition, campoTipologiaDocumento, campoBarcode, campoOggetto, campoNumeroProtocollo,
					campoNumeroProtocolloEmergenza, campoDataProtocollo, campoDataProtocolloEmergenza, campoDataAnnullamentoDocumento, campoAnnoDocumento, campoDataScadenza,
					campoTipoProcedimento, campoRegistroProtocollo, itChiaviRicerca, datada, dataa);

			// Si aggiunge la IN condition
			if (!StringUtils.isNullOrEmpty(inCondition)) {
				whereCondition.append(AND_PARENTESI_DX_AP).append(inCondition).append(" ) ");
			}

			// Esclusione della Categoria Documento "ASSET"
			whereCondition.append(AND + campoCategoriaDocumento + " <> " + CategoriaDocumentoEnum.ASSET.getIds()[0]);

			// Gestione riservato: nella ricerca si controlla che il documento possa essere
			// ricercato anche nel caso sia riservato, gestendo le security dell utente
			final List<String> gruppiAD = getGruppiAD();
			if (!CollectionUtils.isEmpty(gruppiAD)) {
				whereCondition.append(AND_PARENTESI_DX_AP);
				final Integer riservato = (Integer) mappaValoriRicerca.get(campoRiservato);

				final boolean hasPermessoCodaCorriere = PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.CODA_CORRIERE);

				if (!StringUtils.isNullOrEmpty(acl)) {
					String[] aclSplit = null;
					final String domain = gruppiAD.get(0).split("@")[1];
					final List<String> aclList = new ArrayList<>();
					aclSplit = acl.split(",");
					for (int i = 0; i < aclSplit.length; i++) {
						aclList.add(aclSplit[i] + "@" + domain);
					}

					final int listSize = aclList.size();
					if (listSize > 1000) {
						whereCondition.append(campoRiservato).append(" = 1 AND (");
						for (int x = 0; x <= (listSize / 1000); x++) {
							int endlimit = 1000 * (x + 1) - 1;
							if (endlimit >= listSize) {
								endlimit = listSize - 1;
							}
							String orCondition = Constants.EMPTY_STRING;
							if (x != 0) {
								orCondition = " OR ";
							}

							whereCondition.append(orCondition + "ACL INTERSECTS ");
							whereCondition.append(getACLClause(aclList.subList(x * 1000, endlimit)));

						}
						whereCondition.append(")");
					} else {
						whereCondition.append(campoRiservato).append(CONSTANT_1_AND_ACL_INTERSECTS).append(getACLClause(aclList));
					}
				} else if (BooleanFlagEnum.SI.getIntValue().equals(riservato)) {
					whereCondition.append(campoRiservato).append(CONSTANT_1_AND_ACL_INTERSECTS)
							.append(getACLClause(restrictVisibility(utente.getId(), utente.getIdNodoCorriere().intValue(), utente.getIdUfficio(), null, null,
									utente.getUsername(), gruppiAD, hasPermessoCodaCorriere, false, true, utente.getVisFaldoni())));
				} else {
					whereCondition.append(" (( ").append(campoRiservato).append(" = 0 OR riservato IS NULL ) AND ACL INTERSECTS ").append(getACLClause(gruppiAD))
							.append(" ) OR ( ").append(campoRiservato).append(CONSTANT_1_AND_ACL_INTERSECTS)
							.append(getACLClause(restrictVisibility(utente.getId(), utente.getIdNodoCorriere().intValue(), utente.getIdUfficio(), null, null,
									utente.getUsername(), gruppiAD, hasPermessoCodaCorriere, false, true, utente.getVisFaldoni())))
							.append(" ) ");
				}

				whereCondition.append(" )");

				if (orderbyInQuery) {
					whereCondition.append(" ORDER BY d.dateCreated DESC");
				}
			}

			// Proprietà FileNet di sistema
			final String id = PropertyNames.ID;
			final String compoundDocumentState = PropertyNames.COMPOUND_DOCUMENT_STATE;
			final String dataCreazione = PropertyNames.DATE_CREATED;
			final String mimeType = PropertyNames.MIME_TYPE;

			String numberOfElements = Constants.EMPTY_STRING;
			if (setCap && getPP().getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS) != null) {
				numberOfElements = TOP_LABEL + getPP().getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS) + " ";
			}

			String query = null;
			if (queryFullText.length() > 0) {
				query = SELECT + numberOfElements + "d." + id + ", d." + campoDocumentTitle + ", d." + numeroDocumento + ", d." + campoAnnoDocumento + ", d."
						+ campoTipoProtocollo + ", d." + campoBarcode + ", d." + campoOggetto + ", d." + campoNumeroProtocollo + ", d." + campoNumeroProtocolloEmergenza
						+ ", d." + campoAnnoProtocollo + ", d." + campoAnnoProtocolloEmergenza + ", d." + campoNomeFile + ", d." + campoTipologiaDocumento + ", d."
						+ campoDataAnnullamentoDocumento + ", d." + campoRiservato + ", d." + campoDataScadenza + ", d." + campoDataProtocollo + ", d." + dataCreazione
						+ ", d." + mimeType + ", d." + compoundDocumentState + ", d." + destinatariDocumento + ", d." + mittente + ", d." + campoCategoriaDocumento + ", d."
						+ campoSottoCategoriaDocUscita + ", d." + campoTipoProcedimento + ", d." + campoRegistroProtocollo + ", d." + codiceFlusso + ", "
						+ verificaFirmaDocAllegato + "," + metadatoDocumentoValFirma + "," + versionNumber + FROM + classeDocumentale + " d ";
			} else {
				// Dovrebbero essere uguali a parte il prefisso d.
				query = SELECT + numberOfElements + id + ", " + campoDocumentTitle + ", " + numeroDocumento + ", " + campoAnnoDocumento + ", " + campoTipoProtocollo + ", "
						+ campoBarcode + ", " + campoOggetto + ", " + campoNumeroProtocollo + ", " + campoNumeroProtocolloEmergenza + ", " + campoAnnoProtocollo + ", "
						+ campoAnnoProtocolloEmergenza + ", " + campoNomeFile + ", " + campoTipologiaDocumento + ", " + campoDataAnnullamentoDocumento + ", " + campoRiservato
						+ ", " + campoDataScadenza + ", " + campoDataProtocollo + ", " + dataCreazione + ", " + mimeType + ", " + compoundDocumentState + ", "
						+ destinatariDocumento + ", " + mittente + ", " + campoTipoProcedimento + ", " + campoCategoriaDocumento + ", " + campoSottoCategoriaDocUscita + ", "
						+ campoRegistroProtocollo + ", " + codiceFlusso + ", " + integrazioneDati + "," + verificaFirmaDocAllegato + ", " + metadatoDocumentoValFirma + "," + versionNumber
						+ FROM + classeDocumentale + " ";
			}

			final SearchSQL sql = new SearchSQL();
			final StringBuilder sbSql = new StringBuilder();

			sbSql.append(query);
			if (queryFullText.length() > 0) {
				sbSql.append(INNER_JOIN_CONTENT_SEARCH_C_ON_D_THIS_C_QUERIED_OBJECT);
			}

			sbSql.append(whereCondition);

			if (queryFullText.length() > 0) {
				sbSql.append(" AND CONTAINS(d.*, '" + queryFullText + "') ");
				if (setCap) {
					setMaxRecord(sql, getPP());
				}
			}

			sql.setQueryString(sbSql.toString());
			LOGGER.info("ricercaAvanzataDocumenti -> Query da eseguire: " + sql.toString());

			final SearchScope searchScope = new SearchScope(getOs());
			documentiRicerca = (DocumentSet) searchScope.fetchObjects(sql, null, null, false);
		} catch (final Exception e) {
			LOGGER.error("ricercaAvanzataDocumenti -> Si è verificato un errore durante l'esecuzione della ricerca avanzata di documenti nel CE.", e);
			throw new FilenetException("Si è verificato un errore durante l'esecuzione della ricerca avanzata di documenti nel CE.", e);
		}

		LOGGER.info("ricercaAvanzataDocumenti -> END");
		return documentiRicerca;
	}

	/**
	 * Gestisce le chiavi di ricerca.
	 * 
	 * @param mappaValoriRicerca
	 *            mappa valori ricerca
	 * @param queryFullText
	 *            query full-text
	 * @param whereCondition
	 *            condition where
	 * @param campoTipologiaDocumento
	 *            campo tipologia documento
	 * @param campoBarcode
	 *            campo del barcode
	 * @param campoOggetto
	 *            campo oggetto
	 * @param campoNumeroProtocollo
	 *            campo del numero protocollo
	 * @param campoNumeroProtocolloEmergenza
	 *            campo numero protocollo di emergenza
	 * @param campoDataProtocollo
	 *            campo data protocollo
	 * @param campoDataProtocolloEmergenza
	 *            campo della data protocollo di emergenza
	 * @param campoDataAnnullamentoDocumento
	 *            campo data annullamento del documento
	 * @param campoAnnoDocumento
	 *            campo anno documento
	 * @param campoDataScadenza
	 *            campo data scadenza
	 * @param campoTipoProcedimento
	 *            campo tipo procedimento
	 * @param campoRegistroProtocollo
	 *            campo registro protocollo
	 * @param itChiaviRicerca
	 *            chiavi ricerca
	 * @param datada
	 *            data iniziale
	 * @param dataa
	 *            data finale
	 */
	private void handleChiaviRicerca(final Map<String, Object> mappaValoriRicerca, final StringBuilder queryFullText, final StringBuilder whereCondition,
			final String campoTipologiaDocumento, final String campoBarcode, final String campoOggetto, final String campoNumeroProtocollo,
			final String campoNumeroProtocolloEmergenza, final String campoDataProtocollo, final String campoDataProtocolloEmergenza,
			final String campoDataAnnullamentoDocumento, final String campoAnnoDocumento, final String campoDataScadenza, final String campoTipoProcedimento,
			final String campoRegistroProtocollo, final Iterator<String> itChiaviRicerca, String datada, String dataa) {
		while (itChiaviRicerca.hasNext()) {
			final String chiave = itChiaviRicerca.next();
			String valoreString = String.valueOf(mappaValoriRicerca.get(chiave));

			// Si rimuovono i caratteri speciali, sostituendoli con degli spazi che poi
			// vengono rimossi
			valoreString = StringUtils.deleteSpecialCharacterForSpace(valoreString).trim();

			// Operatore LIKE per i campi Oggetto e Note
			if (campoOggetto.equals(chiave) || getPP().getParameterByKey(PropertiesNameEnum.NOTE_METAKEY).equals(chiave)) {
				whereCondition.append(AND_PARENTESI_DX_AP);
				whereCondition.append(chiave + PAR_CHIUSA_LIKE_MODUL + valoreString + "%'");

			} else if (ModalitaRicercaAvanzataTestoEnum.TUTTE_LE_PAROLE.getValore().equals(chiave)) {
				final String[] val = valoreString.split(" ");
				for (int i = 0; i < val.length; i++) {
					if (val[i] != null && val[i].trim().length() > 0) {
						queryFullText.append(StringUtils.duplicaApici(val[i]));
						if (i < val.length - 1) {
							queryFullText.append(AND);
						}
					}
				}

			} else if (ModalitaRicercaAvanzataTestoEnum.UNA_DELLE_PAROLE.getValore().equals(chiave)) {
				final String[] val = valoreString.split(" ");
				for (int i = 0; i < val.length; i++) {
					if (val[i] != null && val[i].trim().length() > 0) {
						queryFullText.append(StringUtils.duplicaApici(val[i]));
						if (i < val.length - 1) {
							queryFullText.append(" OR ");
						}
					}
				}

			} else if (getPP().getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY).equals(chiave)) {
				// Se la chiave indica la categoria del documento, la stringa contiene più
				// valori
				whereCondition.append(AND);
				whereCondition.append(" ( ");
				final String[] categorie = valoreString.split(",");
				for (int i = 0; i < categorie.length; i++) {
					whereCondition.append(chiave + " = " + CategoriaDocumentoEnum.valueOf(categorie[i]).getIds()[0]);
					if (i < categorie.length - 1) {
						whereCondition.append(" OR ");
					}
				}
				whereCondition.append(" ) ");

			} else if (campoTipologiaDocumento.equals(chiave) || campoTipoProcedimento.equals(chiave) 
					|| getPP().getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_MEZZORICEZIONE).equals(chiave)) {
				// Se la chiave è indica la tipologia, la tipologia procedimento o il mezzo di ricezione del documento, la stringa può contenere
				// più valori
				whereCondition.append(AND);
				if (valoreString.indexOf(',') > -1) {
					whereCondition.append(" (");
					final String[] tipologie = valoreString.split(",");
					for (int i = 0; i < tipologie.length; i++) {
						whereCondition.append(chiave).append(" = ").append(tipologie[i]);
						if (i < tipologie.length - 1) {
							whereCondition.append(" OR ");
						}
					}
					whereCondition.append(" ) ");
				} else {
					whereCondition.append(chiave).append(" = ").append(valoreString).append(" ");
				}

			} else if (getPP().getParameterByKey(PropertiesNameEnum.MITTENTE_METAKEY).equals(chiave)) {
				whereCondition.append(AND);
				whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.MITTENTE_METAKEY) + LIKE_MODUL + valoreString + "%'");
			} else if (getPP().getParameterByKey(PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY).equals(chiave)) {
				whereCondition.append(AND);
				whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY) + LIKE_MODUL + valoreString + "%'");
			} else if (campoAnnoDocumento.equals(chiave)) {
				// Come RED
				whereCondition.append(AND);
				whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY)).append(" = ").append(valoreString);
			} else if (Ricerca.NUMERO_PROTOCOLLO_DA.equals(chiave)) {
				// Numero protocollo da
				whereCondition.append(AND);
				whereCondition.append(campoNumeroProtocollo).append(" >= ").append(valoreString);
			} else if (Ricerca.NUMERO_PROTOCOLLO_EMERGENZA_DA.equals(chiave)) {
				// Solo Numero protocollo Emergenza Da
				whereCondition.append(AND);
				whereCondition.append(campoNumeroProtocolloEmergenza).append(" >= ").append(valoreString);
			} else if (Ricerca.NUMERO_PROTOCOLLO_A.equals(chiave)) {
				// Numero protocollo a
				whereCondition.append(AND);
				whereCondition.append(campoNumeroProtocollo).append(" <= ").append(valoreString);
			} else if (Ricerca.NUMERO_PROTOCOLLO_EMERGENZA_A.equals(chiave)) {
				// Solo Numero protocollo Emergenza A
				whereCondition.append(AND);
				whereCondition.append(campoNumeroProtocolloEmergenza).append(" <= ").append(valoreString);
			} else if (Ricerca.DATA_PROTOCOLLO_DA.equals(chiave)) {
				// Data protocollo da
				whereCondition.append(AND);
				whereCondition.append(campoDataProtocollo).append(" >= ").append(valoreString);
			} else if (Ricerca.DATA_PROTOCOLLO_EMERGENZA_DA.equals(chiave)) {
				// Data protocollo da
				whereCondition.append(AND);
				whereCondition.append(campoDataProtocolloEmergenza).append(" >= ").append(valoreString);
			} else if (Ricerca.DATA_PROTOCOLLO_A.equals(chiave)) {
				// Data protocollo a
				whereCondition.append(AND);
				whereCondition.append(campoDataProtocollo).append(" <= ").append(valoreString);
			} else if (Ricerca.DATA_PROTOCOLLO_EMERGENZA_A.equals(chiave)) {
				// Data protocollo a
				whereCondition.append(AND);
				whereCondition.append(campoDataProtocolloEmergenza).append(" <= ").append(valoreString);
			} else if (Ricerca.DATA_CREAZIONE_DA.equals(chiave)) {
				// Data creazione da
				whereCondition.append(AND);
				whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY)).append(" >= ").append(valoreString);

				datada = valoreString;

				// ### Questa logica è necessaria da quando è stato rimosso l'anno dalla ricerca
				// in caso di valorizzazione della data a e data da
				// L'ultimo processato setta l'anno
				if (!StringUtils.isNullOrEmpty(datada) && !StringUtils.isNullOrEmpty(dataa)) {
					int annoDa = Integer.parseInt(datada.substring(0, 4));
					if (datada.contains(CONSTANT_1231T22) || datada.contains(CONSTANT_1231T23)) {
						annoDa += 1;
					}

					int annoA = Integer.parseInt(dataa.substring(0, 4));
					if (dataa.contains(CONSTANT_1231T22) || dataa.contains(CONSTANT_1231T23)) {
						annoA += 1;
					}

					whereCondition.append(AND_PARENTESI_DX_AP);
					whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY)).append(" = ").append(annoDa);
					for (int x = 1; x <= annoA - annoDa; x++) {
						whereCondition.append(" OR ");
						whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY)).append(" = ").append(annoDa + x);
					}

					whereCondition.append(" ) ");
				}

			} else if (Ricerca.DATA_CREAZIONE_A.equals(chiave)) {
				// Data creazione a
				whereCondition.append(AND);
				whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY)).append(" <= ").append(valoreString);
				// Questa logica è necessaria da quando è stato rimosso l'anno dalla ricerca in
				// caso di valorizzazione della data a e data da
				// L'ultimo processato setta l'anno
				dataa = valoreString;

				if (!StringUtils.isNullOrEmpty(datada) && !StringUtils.isNullOrEmpty(dataa)) {
					int annoDa = Integer.parseInt(datada.substring(0, 4));
					if (datada.contains(CONSTANT_1231T22) || datada.contains(CONSTANT_1231T23)) {
						annoDa += 1;
					}

					int annoA = Integer.parseInt(dataa.substring(0, 4));
					if (dataa.contains(CONSTANT_1231T22) || dataa.contains(CONSTANT_1231T23)) {
						annoA += 1;
					}
					whereCondition.append(AND_PARENTESI_DX_AP);
					whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY)).append(" = ").append(annoDa);
					for (int x = 1; x <= annoDa - annoA; x++) {
						whereCondition.append(" OR ");
						whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY)).append(" = ").append(annoDa + x);
					}

					whereCondition.append(" ) ");
				}
			} else if (Ricerca.DATA_SCADENZA_DA.equals(chiave)) {
				// Data scadenza da
				whereCondition.append(AND);
				whereCondition.append(campoDataScadenza).append(" >= ").append(valoreString);
			} else if (Ricerca.DATA_SCADENZA_A.equals(chiave)) {
				// Data scadenza a
				whereCondition.append(AND);
				whereCondition.append(campoDataScadenza).append(" <= ").append(valoreString);
			} else if (campoDataAnnullamentoDocumento.equals(chiave)) {
				// Data annullamento documento
				whereCondition.append(AND);
				whereCondition.append(campoDataAnnullamentoDocumento + " IS " + valoreString);
			} else if (getPP().getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY).equals(chiave)) {
				// Registro riservato
				whereCondition.append(AND);
				if (BooleanFlagEnum.SI.getIntValueInString().equals(valoreString)) {
					whereCondition.append("( " + getPP().getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY) + IS_NOT_NULL + AND
							+ getPP().getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY) + " = " + valoreString + " )");
				} else {
					whereCondition.append("( " + getPP().getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY) + IS_NULL + " OR "
							+ getPP().getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY) + " = " + valoreString + " )");
				}
			} else if (getPP().getParameterByKey(PropertiesNameEnum.NUMERO_RDP_METAKEY).equals(chiave)) {
				// Numero RDP
				whereCondition.append(AND);
				whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.NUMERO_RDP_METAKEY)).append(" = ").append(valoreString);
			} else if (getPP().getParameterByKey(PropertiesNameEnum.NUMERO_LEGISLATURA_METAKEY).equals(chiave)) {
				// Numero Legislatura
				whereCondition.append(AND);
				whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.NUMERO_LEGISLATURA_METAKEY)).append(" = ").append(valoreString);
			} else if (getPP().getParameterByKey(PropertiesNameEnum.URGENTE_METAKEY).equals(chiave)) {
				// Urgente
				whereCondition.append(AND);
				whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.URGENTE_METAKEY)).append(" = ").append(valoreString);
			} else if (campoBarcode.equals(chiave)) {
				// Barcode
				whereCondition.append(" AND LOWER(");
				whereCondition.append(campoBarcode).append(") = ").append(valoreString.toLowerCase());
			} else if (campoRegistroProtocollo.equals(chiave)) {
				// Registro Protocollo
				if (!"REGISTRO UFFICIALE".equalsIgnoreCase(valoreString)) {
					whereCondition.append(AND);
					whereCondition.append(campoRegistroProtocollo).append(" = ").append("'").append(valoreString).append("'");
					whereCondition.append(AND_PARENTESI_DX_AP);
					whereCondition.append(campoNumeroProtocollo).append(IS_NOT_NULL);
					whereCondition.append(" OR ").append(campoNumeroProtocolloEmergenza).append(" IS NOT NULL ) ");
				} else {
					whereCondition.append(AND_PARENTESI_DX_AP);
					whereCondition.append(campoRegistroProtocollo).append(IS_NULL);
					whereCondition.append(" OR ").append(campoRegistroProtocollo).append(" = 'REGISTRO UFFICIALE'  ");
					whereCondition.append(" ) AND ( ");
					whereCondition.append(campoNumeroProtocollo).append(IS_NOT_NULL);
					whereCondition.append(" OR ").append(campoNumeroProtocolloEmergenza).append(" IS NOT NULL ) ");
				}
			} else {
				// Caso generico
				whereCondition.append(AND).append(chiave).append(" = ");
				// Valore reale (non trasformato in stringa)
				final Object valore = mappaValoriRicerca.get(chiave);
				// Caso generico testo
				if (valore instanceof String) {
					whereCondition.append("'").append(valoreString).append("'");
					// Caso generico numerico
				} else {
					whereCondition.append(valoreString);
				}
			}
		}
	}

	/**
	 * Imposta il numero massimimo di record che restituisce la query di ricerca.
	 * 
	 * @param sql
	 * @param pp
	 */
	public static void setMaxRecord(final SearchSQL sql, final PropertiesProvider pp) {
		Integer maxRecords = 100;
		final String strMaxRes = pp.getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS);
		if (!StringUtils.isNullOrEmpty(strMaxRes)) {
			maxRecords = Integer.valueOf(strMaxRes);
		} else {
			final String maxStr = pp.getParameterByKey(PropertiesNameEnum.RICERCA_FULLTEXT_MAX_RESULTS);
			if (!StringUtils.isNullOrEmpty(maxStr)) {
				maxRecords = Integer.valueOf(maxStr);
			}
		}
		sql.setMaxRecords(maxRecords);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#ricercaAllegatiFullText(java.util.Map,
	 *      it.ibm.red.business.enums.ModalitaRicercaAvanzataTestoEnum).
	 */
	@Override
	public DocumentSet ricercaAllegatiFullText(final Map<String, Object> mappaValoriRicerca, final ModalitaRicercaAvanzataTestoEnum modalitaRicercaTestuale) {
		LOGGER.info("ricercaAllegatiFullText -> START");
		DocumentSet allegatiRicercaTesto = null;

		try {
			final StringBuilder query = buildQueryRicercaAllegati();

			// ### Gestione ricerca testuale
			String stringheVal = "";

			if (modalitaRicercaTestuale != null) {
				final String testoRicercaTestuale = (String) mappaValoriRicerca.get(modalitaRicercaTestuale.getValore());
				stringheVal = StringUtils.formattaStringaPerRicercaTestuale(testoRicercaTestuale, modalitaRicercaTestuale);
				query.append(" WHERE CONTAINS(d.*, '").append(stringheVal).append("') ");
			}

			// ### Creazione delle date creazione rispetto all'anno
			final Integer annoDocumento = (Integer) mappaValoriRicerca.get(getPP().getParameterByKey(PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY));
			query.append(updateQueryRicercaAllegati(annoDocumento));

			final SearchSQL sql = new SearchSQL(query.toString());
			LOGGER.info("ricercaAllegatiFullText -> Query da eseguire: " + sql.toString());

			final SearchScope searchScope = new SearchScope(getOs());
			allegatiRicercaTesto = (DocumentSet) searchScope.fetchObjects(sql, null, null, false);
		} catch (final Exception e) {
			LOGGER.error("ricercaAllegatiFullText -> Si è verificato un errore durante l'esecuzione della ricerca avanzata nel CE.", e);
			throw new FilenetException("Si è verificato un errore durante l'esecuzione della ricerca avanzata nel CE.", e);
		}

		LOGGER.info("ricercaAllegatiFullText -> END");
		return allegatiRicercaTesto;
	}

	/**
	 * Aggiorna e restituisce la query di ricerca per gli allegati popolando i
	 * restanti parametri. Questo metodo è legato a
	 * {@link #buildQueryRicercaAllegati()}.
	 * 
	 * @param annoDocumento
	 *            Anno documento da ricercare.
	 * @return Query parziale da aggiungere alla query generata dal metodo di
	 *         costruzione della query di ricerca allegati.
	 */
	private StringBuilder updateQueryRicercaAllegati(final Integer annoDocumento) {
		
		StringBuilder query = new StringBuilder("");
		if (annoDocumento != null) {
			query.append(AND_D_TABLE);
			query.append(getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY)).append(" >= ")
					.append(DateUtils.buildFilenetDataForSearchFromYear(annoDocumento, true));
			query.append(AND_D_TABLE);
			query.append(getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY)).append(" <= ")
					.append(DateUtils.buildFilenetDataForSearchFromYear(annoDocumento, false));
		}

		// ### Solo la versione corrente
		query.append(AND_D_TABLE).append(PropertyNames.IS_CURRENT_VERSION).append(UGUALE_TRUE);

		// ### Gestione ACL
		final List<String> gruppiAD = getGruppiAD();
		if (!CollectionUtils.isEmpty(gruppiAD)) {
			query.append(AND_ACL_INTERSECTS + getACLClause(gruppiAD) + " ");
		}
		
		return query;
	}

	/**
	 * Restituisce la query di ricerca degli allegati costruita recuperando le
	 * properties.
	 * 
	 * @return Query ricerca allegati.
	 */
	private StringBuilder buildQueryRicercaAllegati() {
		
		final StringBuilder query = new StringBuilder();

		// Campi del database
		final String campoTipologiaDocumento = getPP().getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY);
		final String campoDocumentTitle = getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
		final String campoBarcode = getPP().getParameterByKey(PropertiesNameEnum.BARCODE_METAKEY);
		final String campoOggetto = getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY);
		final String campoNomeFile = getPP().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY);

		// Proprietà FileNet di sistema
		final String id = PropertyNames.ID;
		final String compoundDocumentState = PropertyNames.COMPOUND_DOCUMENT_STATE;
		final String dataCreazione = PropertyNames.DATE_CREATED;
		final String mimeType = PropertyNames.MIME_TYPE;
		final String parentDocuments = PropertyNames.PARENT_DOCUMENTS;

		query.append(SELECT).append(" d.").append(id).append(", d.").append(campoBarcode).append(", d.").append(compoundDocumentState).append(", d.")
				.append(campoDocumentTitle).append(", d.").append(campoOggetto).append(", d.").append(campoTipologiaDocumento).append(", d.")
				.append(dataCreazione + ", d.").append(campoNomeFile).append(", d.").append(mimeType).append(", d.").append(parentDocuments).append(FROM)
				.append(getPP().getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY)).append(" d ")
				.append(INNER_JOIN_CONTENT_SEARCH_C_ON_D_THIS_C_QUERIED_OBJECT); // CBR
		
		return query;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#ricercaAvanzataFascicoli(java.util.Map,
	 *      java.util.Set, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public DocumentSet ricercaAvanzataFascicoli(final Map<String, Object> mappaValoriRicerca, final Set<String> idFascicoli, final UtenteDTO utente) {
		LOGGER.info("ricercaAvanzataFascicoli -> START");
		DocumentSet fascicoliRicerca = null;
		final StringBuilder whereCondition = new StringBuilder();
		Boolean faldonato = null;
		boolean orderBy = false;

		try {
			// ### COSTRUZIONE WHERE CONDITION -> START
			final List<String> gruppiAD = getGruppiAD();

			String inCondition = null;

			// Si calcola la IN condition
			if (!CollectionUtils.isEmpty(idFascicoli)) {
				inCondition = StringUtils.createInCondition(getPP().getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY), idFascicoli.iterator(), true);
			}

			final Set<String> chiaviRicerca = mappaValoriRicerca.keySet();
			final Iterator<String> itChiaviRicerca = chiaviRicerca.iterator();

			// Campi del database
			final String campoTitolario = getPP().getParameterByKey(PropertiesNameEnum.TITOLARIO_METAKEY);
			final String campoNomeFascicolo = getPP().getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY);
			final String campoOggettoFascicolo = getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY);
			final String campoNumeroFascicolo = getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
			final String campoFaldonato = getPP().getParameterByKey(PropertiesNameEnum.FALDONATO_METAKEY);
			final String campoDocumentTitle = getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
			final String campoStatoFascicolo = getPP().getParameterByKey(PropertiesNameEnum.STATO_FASCICOLO_METAKEY);
			final String campoDataTerminazione = getPP().getParameterByKey(PropertiesNameEnum.DATA_TERMINAZIONE_METAKEY);
			final String campoIdFascicoloFEPA = getPP().getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA);
			final String campoIdAoo = getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY);

			while (itChiaviRicerca.hasNext()) {
				final String chiave = itChiaviRicerca.next();
				String valoreString = String.valueOf(mappaValoriRicerca.get(chiave));

				// Si rimuovono i caratteri speciali, sostituendoli con degli spazi che poi
				// vengono rimossi
				valoreString = StringUtils.deleteSpecialCharacterForSpace(valoreString).trim();

				if (campoOggettoFascicolo.equals(chiave)) {
					// Descrizione fascicolo
					whereCondition.append(" AND (f.");
					whereCondition.append(chiave + PAR_CHIUSA_LIKE_MODUL + valoreString + "%'");
				} else if (campoNumeroFascicolo.equals(chiave)) {
					// Numero fascicolo
					whereCondition.append(AND_F);
					whereCondition.append(campoOggettoFascicolo + " LIKE '" + valoreString + "%'");
				} else if (campoTitolario.equals(chiave)) {
					// Titolario
					whereCondition.append(AND_F);
					whereCondition.append(chiave + " = '" + StringUtils.getIndiceClassificazione(utente.getIdAoo(), valoreString) + "'");
					orderBy = true;
				} else if (campoFaldonato.equals(chiave)) {
					if (Boolean.TRUE.toString().equals(valoreString)) {
						faldonato = true;
						// Faldonato
						whereCondition.append(" AND (f2.This insubfolder '/" + getPP().getParameterByKey(PropertiesNameEnum.FALDONE_PARENT_FOLDER_PREFIX)
								+ "') AND (f2.ACL INTERSECTS " + getACLClause(gruppiAD) + ")");
					} else {
						faldonato = false;
						// Non faldonato
						whereCondition.append(" AND ((NOT (f.this insubfolder '/" + getPP().getParameterByKey(PropertiesNameEnum.FALDONE_PARENT_FOLDER_PREFIX) + "'))");
						// Diversamente faldonato (?!)
						whereCondition.append(" OR ((f2.This insubfolder '/" + getPP().getParameterByKey(PropertiesNameEnum.FALDONE_PARENT_FOLDER_PREFIX) + "')");
						whereCondition.append(" AND (NOT f2.ACL INTERSECTS " + getACLClause(gruppiAD) + ")");
						whereCondition.append(" AND NOT EXISTS (SELECT f3.This FROM (Folder f3 INNER JOIN ReferentialContainmentRelationship r2 ON f3.This = r2.Tail)");
						whereCondition.append(" INNER JOIN " + getPP().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY));
						whereCondition.append(" f4 ON f4.This = r2.Head WHERE f4.This = f.This AND f3.ACL INTERSECTS " + getACLClause(gruppiAD));
						whereCondition.append(" AND (f3.This insubfolder '/" + getPP().getParameterByKey(PropertiesNameEnum.FALDONE_PARENT_FOLDER_PREFIX) + "'))))");
					}
				} else if (Ricerca.DATA_CREAZIONE_DA.equals(chiave)) {
					// Data creazione da
					whereCondition.append(AND_F);
					whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY)).append(" >= ").append(valoreString);
				} else if (Ricerca.DATA_CREAZIONE_A.equals(chiave)) {
					// Data creazione a
					whereCondition.append(AND_F);
					whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY)).append(" <= ").append(valoreString);
				} else if (Ricerca.DATA_CHIUSURA_DA.equals(chiave)) {
					// Data chiusura da
					whereCondition.append(AND_F);
					whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.DATA_TERMINAZIONE_METAKEY)).append(" >= ").append(valoreString);
				} else if (Ricerca.DATA_CHIUSURA_A.equals(chiave)) {
					// Data chiusura a
					whereCondition.append(AND_F);
					whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.DATA_TERMINAZIONE_METAKEY)).append(" <= ").append(valoreString);
				} else {
					whereCondition.append(AND_F).append(chiave).append(" = ");
					final Object valore = mappaValoriRicerca.get(chiave); // Valore reale (non trasformato in stringa)
					// Caso base testo
					if (valore instanceof String) {
						whereCondition.append("'").append(valoreString).append("'");
						// Caso numerico
					} else {
						whereCondition.append(valoreString);
					}
				}
			}

			if (!StringUtils.isNullOrEmpty(inCondition)) {
				whereCondition.append(AND_PARENTESI_DX_AP).append(inCondition).append(" ) ");
			}

			whereCondition.append(AND).append(campoIdAoo).append(" = ").append(utente.getIdAoo());

			// Gestione riservato: nella ricerca controllo che il documento può essere
			// ricercato anche in caso fosse riservato gestendo le security dell utente
			if (!CollectionUtils.isEmpty(gruppiAD)) {
				final String aclClause = getACLClause(restrictVisibility(utente.getId(), utente.getIdNodoCorriere().intValue(), utente.getIdUfficio(), null, null,
						utente.getUsername(), gruppiAD, true, true, false, utente.getVisFaldoni()));
				whereCondition.append(" AND f.ACL INTERSECTS ").append(aclClause);
			}
			// ### COSTRUZIONE WHERE CONDITION -> END

			// Proprietà FileNet di sistema
			final String id = PropertyNames.ID;
			final String dateCreated = PropertyNames.DATE_CREATED;

			String numberOfElements = "";
			if (getPP().getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS) != null) {
				numberOfElements = TOP_LABEL + getPP().getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS);
			}
			String query = "SELECT DISTINCT " + numberOfElements + " f." + id + ", f." + campoDocumentTitle + ", f." + campoIdAoo + ", f." + campoNomeFascicolo + ", f."
					+ campoTitolario + ", f." + campoOggettoFascicolo + ", f." + campoStatoFascicolo + ", f." + dateCreated + ", f." + campoFaldonato + ", f."
					+ campoDataTerminazione + ", f." + campoIdFascicoloFEPA + FROM;

			if (faldonato != null) {
				query = query + "(" + getPP().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY)
						+ " f INNER JOIN ReferentialContainmentRelationship r ON f.This = r.Head) INNER JOIN Folder f2 ON f2.This = r.Tail ";
			} else {
				query = query + getPP().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY) + " f ";
			}

			final SearchSQL sql = new SearchSQL();
			final StringBuilder sbSql = new StringBuilder();
			sbSql.append(query);

			sbSql.append("WHERE IsCurrentVersion = TRUE ").append(whereCondition);

			if (orderBy) {
				sbSql.append(" ORDER BY f.DateCreated DESC");
			}

			sql.setQueryString(sbSql.toString());
			LOGGER.info("ricercaAvanzataFascicoli -> Query da eseguire: " + sql.toString());

			final SearchScope searchScope = new SearchScope(getOs());
			fascicoliRicerca = (DocumentSet) searchScope.fetchObjects(sql, null, null, false);
		} catch (final Exception e) {
			LOGGER.error("ricercaAvanzataFascicoli -> Si è verificato un errore durante l'esecuzione della ricerca avanzata di fascicoli nel CE.", e);
			throw new FilenetException("Si è verificato un errore durante l'esecuzione della ricerca avanzata di fascicoli nel CE.", e);
		}

		LOGGER.info("ricercaAvanzataFascicoli -> END");
		return fascicoliRicerca;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#ricercaAvanzataFaldoni(java.util.Map,
	 *      it.ibm.red.business.dto.UtenteDTO, boolean).
	 */
	@Override
	public DocumentSet ricercaAvanzataFaldoni(final Map<String, Object> mappaValoriRicerca, final UtenteDTO utente, final boolean orderbyInQuery) {
		LOGGER.info("ricercaAvanzataFaldoni -> START");
		DocumentSet faldoniRicerca = null;
		final StringBuilder whereCondition = new StringBuilder();

		try {
			// Informazioni faldone
			final String campoDocumentTitle = getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_DOCUMENTTITLE);
			final String campoNomeFaldone = getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_NOMEFALDONE);
			final String campoOggettoFaldone = getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_OGGETTO);
			final String campoDescrizioneFaldone = getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_DESCRIZIONEFALDONE);
			final String campoParentFaldone = getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_PARENTFALDONE);
			final String campoIdAoo = getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_IDAOO);

			// ### COSTRUZIONE WHERE CONDITION -> START
			final Set<String> chiaviRicerca = mappaValoriRicerca.keySet();
			final Iterator<String> itChiaviRicerca = chiaviRicerca.iterator();

			final List<String> gruppiAD = getGruppiAD();

			whereCondition.append(WHERE_IS_CURRENT_VERSION_TRUE);
			whereCondition.append(AND).append(campoIdAoo).append(" = ").append(utente.getIdAoo());

			// Gestione riservato: nella ricerca controllo che il documento può essere
			// ricercato anche in caso fosse riservato gestendo le security dell utente
			if (!CollectionUtils.isEmpty(gruppiAD)) {
				final String aclClause = getACLClause(restrictVisibility(utente.getId(), null, null, utente.getIdUfficio().intValue(), utente.getIdNodoCorriere().intValue(),
						null, gruppiAD, true, true, false, utente.getVisFaldoni()));
				whereCondition.append(AND_ACL_INTERSECTS).append(aclClause);
			}

			while (itChiaviRicerca.hasNext()) {
				final String chiave = itChiaviRicerca.next();
				String valoreString = String.valueOf(mappaValoriRicerca.get(chiave));

				valoreString = StringUtils.deleteSpecialCharacterForSpace(valoreString).trim();

				if (campoOggettoFaldone.equals(chiave)) {
					// Descrizione faldone
					whereCondition.append(AND_PARENTESI_DX_AP);
					whereCondition.append(chiave + PAR_CHIUSA_LIKE_MODUL + valoreString + "%'");
				} else if (Ricerca.DATA_CREAZIONE_DA.equals(chiave)) {
					// Data creazione da
					whereCondition.append(AND);
					whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY)).append(" >= ").append(valoreString);
				} else if (Ricerca.DATA_CREAZIONE_A.equals(chiave)) {
					// Data creazione a
					whereCondition.append(AND);
					whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY)).append(" <= ").append(valoreString);
				}
			}
			// ### COSTRUZIONE WHERE CONDITION -> END

			String orderByString = "";
			if (orderbyInQuery) {
				orderByString = ORDER_BY + getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY) + DESC;
			}

			// Proprietà FileNet di sistema
			final String id = PropertyNames.ID;
			final String name = PropertyNames.NAME;
			final String creator = PropertyNames.CREATOR;
			final String owner = PropertyNames.OWNER;
			final String dateCreated = PropertyNames.DATE_CREATED;
			final String isReserved = PropertyNames.IS_RESERVED;
			final String securityFolder = PropertyNames.SECURITY_FOLDER;

			String numberOfElements = "";
			if (getPP().getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS) != null) {
				numberOfElements = TOP_LABEL + getPP().getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS);
			}
			final StringBuilder query = new StringBuilder(SELECT).append(numberOfElements).append(" ").append(id).append(", ").append(name).append(", ")
					.append(securityFolder).append(", ").append(creator).append(", ").append(owner).append(", ").append(campoDocumentTitle).append(", ")
					.append(campoNomeFaldone).append(", ").append(campoParentFaldone).append(", ").append(campoOggettoFaldone).append(", ").append(campoDescrizioneFaldone)
					.append(", ").append(dateCreated).append(", ").append(isReserved).append(", ").append(campoIdAoo).append(FROM)
					.append(getPP().getParameterByKey(PropertiesNameEnum.FALDONE_CLASSNAME_FN_METAKEY));

			final SearchSQL sql = new SearchSQL();

			query.append(whereCondition.toString());
			query.append(orderByString);

			sql.setQueryString(query.toString());
			LOGGER.info("ricercaAvanzataFaldoni -> Query da eseguire: " + sql.toString());

			final SearchScope searchScope = new SearchScope(getOs());
			faldoniRicerca = (DocumentSet) searchScope.fetchObjects(sql, null, null, false);
		} catch (final Exception e) {
			LOGGER.error("ricercaAvanzataFaldoni -> Si è verificato un errore durante l'esecuzione della ricerca avanzata di faldoni nel CE.", e);
			throw new FilenetException("Si è verificato un errore durante l'esecuzione della ricerca avanzata di faldoni nel CE.", e);
		}

		LOGGER.info("ricercaAvanzataFaldoni -> END");
		return faldoniRicerca;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getFascicoloByOggetto(java.lang.String, java.util.List).
	 */
	@Override
	public Document getFascicoloByOggetto(final String oggettoFascicolo, final List<String> gruppiAD) {
		Document fascicolo = null;
		StringBuilder queryString = null;

		try {
			queryString = new StringBuilder();
			queryString.append("SELECT f.Id, f." + getPP().getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY));
			queryString.append(", f." + getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY));
			queryString.append(FROM);
			queryString.append(getPP().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY) + " f");
			queryString.append(WHERE);
			queryString.append(getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY) + LIKE_MODUL + oggettoFascicolo + "'");

			if (gruppiAD != null) {
				queryString.append(AND_ACL_INTERSECTS + getACLClause(gruppiAD));
			}

			final SearchScope searchScope = new SearchScope(getOs());
			final DocumentSet documents = (DocumentSet) searchScope.fetchObjects(new SearchSQL(queryString.toString()), null, null, false);
			final Iterator<?> it = documents.iterator();

			if (it.hasNext()) {
				fascicolo = (Document) it.next();
			}
		} catch (final Exception e) {
			throw new FilenetException("getFascicoloByOggetto -> Errore durante il recupero del fascicolo con oggetto: " + oggettoFascicolo, e);
		}

		return fascicolo;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getFaldoneByMetadato(java.lang.String, java.lang.String, java.util.List).
	 */
	@Override
	public Document getFaldoneByMetadato(final String chiaveMetadato, final String valoreMetadato, final List<String> gruppiAD) {
		Document faldone = null;
		StringBuilder queryString = null;

		try {
			queryString = new StringBuilder();
			queryString.append("SELECT f." + getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_NOMEFALDONE));
			queryString.append(", f." + getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_OGGETTO));
			queryString.append(FROM);
			queryString.append(getPP().getParameterByKey(PropertiesNameEnum.FALDONE_CLASSNAME_FN_METAKEY) + " f");
			queryString.append(WHERE_F);
			queryString.append(chiaveMetadato + " = '" + valoreMetadato + AND_IS_CURRENT_VERSION_TRUE);

			if (gruppiAD != null) {
				queryString.append(AND_ACL_INTERSECTS + getACLClause(gruppiAD));
			}

			final DocumentSet documents = (DocumentSet) new SearchScope(getOs()).fetchObjects(new SearchSQL(queryString.toString()), 1, null, false);
			final Iterator<?> it = documents.iterator();

			if (it.hasNext()) {
				faldone = (Document) it.next();
			}
		} catch (final Exception e) {
			throw new FilenetException("getFaldoneByMetadato -> Errore durante il recupero del faldone.", e);
		}

		return faldone;
	}

	/**
	 * Il metodo ritorna la lista dei faldoni contentuti nel faldone passato in
	 * input. Nel caso in cui il nomeFaldone è null ritorna tutti i faldoni radice.
	 */
	@Override
	public DocumentSet getFaldoniFigli(final String nomeFaldonePadre, final List<String> gruppiAD, final Long idAoo, final String idFaldonePartenza) {
		DocumentSet faldoniFigli = null;
		String sql = null;

		final String chiaveOggettoFaldone = getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_OGGETTO);
		final String chiaveParentFaldone = getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_PARENTFALDONE);

		try {
			final String selectList = PropertyNames.NAME + ", " + PropertyNames.SECURITY_FOLDER + ", "
					+ getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_DOCUMENTTITLE) + ", "
					+ getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_NOMEFALDONE) + "," + chiaveParentFaldone + "," + chiaveOggettoFaldone + ","
					+ getPP().getParameterByKey(PropertiesNameEnum.METADATO_FALDONE_DESCRIZIONEFALDONE) + ","
					+ getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY);

			sql = SELECT + selectList + FROM + getPP().getParameterByKey(PropertiesNameEnum.FALDONE_CLASSNAME_FN_METAKEY) + WHERE;

			if (StringUtils.isNullOrEmpty(nomeFaldonePadre)) {
				sql += chiaveParentFaldone + IS_NULL;
			} else {
				sql += chiaveParentFaldone + " = '" + nomeFaldonePadre + "'";
			}

			if (gruppiAD != null) {
				sql += AND_ACL_INTERSECTS + getACLClause(gruppiAD);
			}

			if (idFaldonePartenza != null) {
				sql += AND + chiaveOggettoFaldone + " > '" + idFaldonePartenza.replace("'", "''") + "' ";
			}

			sql += " AND IsCurrentVersion = TRUE AND idAOO = " + idAoo + ORDER_BY + chiaveOggettoFaldone + " ASC";

			faldoniFigli = (DocumentSet) new SearchScope(getOs()).fetchObjects(new SearchSQL(sql), 100, null, true);
		} catch (final Exception e) {
			LOGGER.error("getFaldoniFigli -> Si è verificato un errore durante il recupero dei faldoni con padre: " + nomeFaldonePadre, e);
			throw new FilenetException(e);
		}

		return faldoniFigli;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#ricercaSigi(java.util.Map, it.ibm.red.business.dto.UtenteDTO, boolean).
	 */
	@Override
	public DocumentSet ricercaSigi(final Map<String, Object> mappaValoriRicerca, final UtenteDTO utente, final boolean orderbyInQuery) {
		LOGGER.info("ricercaSIGI -> START");
		DocumentSet documentiRicerca = null;
		final StringBuilder whereCondition = new StringBuilder();

		try {
			// Campi del database
			final String campoDocumentTitle = getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
			final String campoOggetto = getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY);
			final String campoNumeroProtocollo = getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
			final String campoDataProtocollo = getPP().getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY);
			final String campoAnnoProtocollo = getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
			final String campoDataAnnullamentoDocumento = getPP().getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_DATA_ANNULLAMENTO);
			final String campoNomeFile = getPP().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY);

			// ### COSTRUZIONE WHERE CONDITION -> START
			final Set<String> chiaviRicerca = mappaValoriRicerca.keySet();
			final Iterator<String> itChiaviRicerca = chiaviRicerca.iterator();

			whereCondition.append(WHERE_IS_CURRENT_VERSION_TRUE);

			while (itChiaviRicerca.hasNext()) {
				final String chiave = itChiaviRicerca.next();
				String valoreString = String.valueOf(mappaValoriRicerca.get(chiave));

				// Si rimuovono i caratteri speciali, sostituendoli con degli spazi che poi
				// vengono rimossi
				valoreString = StringUtils.deleteSpecialCharacterForSpace(valoreString).trim();

				// Operatore LIKE per il campo Oggetto
				if (campoOggetto.equals(chiave)) {
					whereCondition.append(AND);
					whereCondition.append(chiave + LIKE_MODUL + valoreString + "%'");

				} else if (Ricerca.NUMERO_PROTOCOLLO_DA.equals(chiave)) {
					// Numero protocollo da
					whereCondition.append(AND);
					whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY)).append(" >= ").append(valoreString);
				} else if (Ricerca.NUMERO_PROTOCOLLO_A.equals(chiave)) {
					// Numero protocollo a
					whereCondition.append(AND);
					whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY)).append(" <= ").append(valoreString);
				} else if (Ricerca.DATA_PROTOCOLLO_DA.equals(chiave)) {
					// Data protocollo da
					whereCondition.append(AND);
					whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY)).append(" >= ").append(valoreString);
				} else if (Ricerca.DATA_PROTOCOLLO_A.equals(chiave)) {
					// Data protocollo a
					whereCondition.append(AND);
					whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY)).append(" <= ").append(valoreString);
				} else if (Ricerca.DATA_CREAZIONE_DA.equals(chiave)) {
					// Data creazione da
					whereCondition.append(AND);
					whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY)).append(" >= ").append(valoreString);
				} else if (Ricerca.DATA_CREAZIONE_A.equals(chiave)) {
					// Data creazione a
					whereCondition.append(AND);
					whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY)).append(" <= ").append(valoreString);
				} else if (campoDataAnnullamentoDocumento.equals(chiave)) {
					// Data annullamento documento
					whereCondition.append(AND);
					whereCondition.append(campoDataAnnullamentoDocumento + " IS " + valoreString);
				} else {
					whereCondition.append(AND).append(chiave).append(" = ");
					final Object valore = mappaValoriRicerca.get(chiave); // Valore reale (non trasformato in stringa)
					// Caso base testo
					if (valore instanceof String) {
						whereCondition.append("'").append(valoreString).append("'");
						// Caso numerico
					} else {
						whereCondition.append(valoreString);
					}
				}
			}

			whereCondition.append(AND_ACL_INTERSECTS + getACLClause(utente.getGruppiAD())); // ACL
			// ### COSTRUZIONE WHERE CONDITION -> END

			// Proprietà FileNet di sistema
			final String id = PropertyNames.ID;
			final String versionSeries = PropertyNames.VERSION_SERIES;

			String numberOfElements = "";
			if (getPP().getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS) != null) {
				numberOfElements = TOP_LABEL + getPP().getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS);
			}
			String query = org.apache.commons.lang3.StringUtils.join(SELECT, numberOfElements, " ", versionSeries, ", ", campoDataProtocollo, ", ", campoOggetto, ", ",
					campoDocumentTitle, ", ", campoAnnoProtocollo, ", ", campoNumeroProtocollo, ", ", id, ", ", campoNomeFile, ", ", campoDataAnnullamentoDocumento, ", ",
					getPP().getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_MOTIVAZIONE_ANNULLAMENTO), FROM,
					getPP().getParameterByKey(PropertiesNameEnum.DOCUMENTO_SIGI_CLASSNAME_FN_METAKEY), " WITH EXCLUDESUBCLASSES", whereCondition);

			if (orderbyInQuery) {
				query = query + ORDER_BY + campoAnnoProtocollo + ", " + campoNumeroProtocollo + DESC;
			}

			final SearchSQL sql = new SearchSQL();
			sql.setQueryString(query);
			LOGGER.info("ricercaSIGI -> Query da eseguire: " + sql.toString());

			final SearchScope searchScope = new SearchScope(getOs());
			documentiRicerca = (DocumentSet) searchScope.fetchObjects(sql, 50, null, false);
		} catch (final Exception e) {
			LOGGER.error("ricercaSIGI -> Si è verificato un errore durante l'esecuzione della ricerca SIGI.", e);
			throw new FilenetException("ricercaSIGI -> Si è verificato un errore durante l'esecuzione della ricerca SIGI.", e);
		}

		LOGGER.info("ricercaSIGI -> END");
		return documentiRicerca;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#ricercaRegistroProtocollo(java.util.Map,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public DocumentSet ricercaRegistroProtocollo(final Map<String, Object> mappaValoriRicerca, final UtenteDTO utente) {
		LOGGER.info("ricercaRegistroProtocollo -> START");
		DocumentSet documentiRicerca = null;
		final StringBuilder whereCondition = new StringBuilder();

		try {
			// Campi del database
			final String campoDocumentTitle = getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
			final String campoDataProtocollo = getPP().getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY);
			final String campoAnnoProtocollo = getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
			final String campoDataElencoTrasmissione = getPP().getParameterByKey(PropertiesNameEnum.DATA_ELENCO_TRASMISSIONE_METAKEY);
			final String campoNomeFile = getPP().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY);
			final String campoIdAoo = getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY);

			// ### COSTRUZIONE WHERE CONDITION -> START
			final Set<String> chiaviRicerca = mappaValoriRicerca.keySet();
			final Iterator<String> itChiaviRicerca = chiaviRicerca.iterator();

			whereCondition.append(WHERE_IS_CURRENT_VERSION_TRUE);

			while (itChiaviRicerca.hasNext()) {
				final String chiave = itChiaviRicerca.next();
				String valoreString = String.valueOf(mappaValoriRicerca.get(chiave));

				// Si rimuovono i caratteri speciali, sostituendoli con degli spazi che poi
				// vengono rimossi
				valoreString = StringUtils.deleteSpecialCharacterForSpace(valoreString).trim();

				if (Ricerca.DATA_CREAZIONE_DA.equals(chiave)) {
					// Data creazione da
					whereCondition.append(AND);
					whereCondition.append(campoDataProtocollo).append(" >= ").append(valoreString);
				} else if (Ricerca.DATA_CREAZIONE_A.equals(chiave)) {
					// Data creazione a
					whereCondition.append(AND);
					whereCondition.append(campoDataProtocollo).append(" <= ").append(valoreString);
				} else {
					whereCondition.append(AND).append(chiave).append(" = ");
					final Object valore = mappaValoriRicerca.get(chiave); // Valore reale (non trasformato in stringa)
					// Caso base testo
					if (valore instanceof String) {
						whereCondition.append("'").append(valoreString).append("'");
						// Caso numerico
					} else {
						whereCondition.append(valoreString);
					}
				}
			}

			whereCondition.append(ORDER_BY + campoDataProtocollo + DESC);
			// ### COSTRUZIONE WHERE CONDITION -> END

			// Proprietà FileNet di sistema
			final String id = PropertyNames.ID;

			String numberOfElements = "";
			if (getPP().getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS) != null) {
				numberOfElements = TOP_LABEL + getPP().getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS);
			}
			final StringBuilder query = new StringBuilder(SELECT).append(numberOfElements).append(" " + id).append(", " + campoDocumentTitle)
					.append(", " + campoDataElencoTrasmissione).append(", " + campoNomeFile).append(", " + campoAnnoProtocollo).append(", " + campoDataProtocollo)
					.append(", " + campoIdAoo).append(FROM + getPP().getParameterByKey(PropertiesNameEnum.REGISTRO_PROTOCOLLO_CLASSNAME_FN_METAKEY));

			// Si aggiunge la WHERE condition
			query.append(whereCondition);

			final SearchSQL sql = new SearchSQL();
			sql.setQueryString(query.toString());
			LOGGER.info("ricercaRegistroProtocollo -> Query da eseguire: " + sql.toString());

			final SearchScope searchScope = new SearchScope(getOs());
			documentiRicerca = (DocumentSet) searchScope.fetchObjects(sql, null, null, false);
		} catch (final Exception e) {
			LOGGER.error("ricercaRegistroProtocollo -> Si è verificato un errore durante l'esecuzione della ricerca SIGI.", e);
			throw new FilenetException("ricercaRegistroProtocollo -> Si è verificato un errore durante l'esecuzione della ricerca SIGI.", e);
		}

		LOGGER.info("ricercaRegistroProtocollo -> END");
		return documentiRicerca;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#associaAllegatoToOP(com.filenet.api.core.Document,
	 *      com.filenet.api.core.Document).
	 */
	@Override
	public void associaAllegatoToOP(final Document decreto, final Document allegatoDecreto) {
		IndependentObjectSet elencoOP = null;
		final CustomObject registroOP = (CustomObject) decreto.getProperties().getObjectValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_OP_PROXY));

		if (registroOP != null) {
			elencoOP = registroOP.getProperties().getIndependentObjectSetValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_ELENCO_OP));
			if (elencoOP != null) {
				CustomObject customObject = null;
				for (final Iterator<?> iterator = elencoOP.iterator(); iterator.hasNext();) {
					customObject = (CustomObject) iterator.next();
					if (!"Annullato".equals(customObject.getProperties().getStringValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_STATO_OP)))) {
						salvaOPAllegato(customObject, allegatoDecreto);
					}
				}
			}
		}
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#salvaOPAllegato(com.filenet.api.core.CustomObject,
	 *      com.filenet.api.core.Document).
	 */
	@Override
	public void salvaOPAllegato(final CustomObject op, final Document allegatoDecreto) {
		final Link link = com.filenet.api.core.Factory.Link.createInstance(getOs(), "AllegatiOPLink");
		link.getProperties().putObjectValue("Head", allegatoDecreto);
		link.getProperties().putObjectValue("Tail", op);
		link.save(RefreshMode.REFRESH);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getOrdiniPagamentoAllegatoOp(java.lang.String).
	 */
	@Override
	public IndependentObjectSet getOrdiniPagamentoAllegatoOp(final String idAllegatoOP) {
		final SearchSQL searchSQL = new SearchSQL();

		final String query = "SELECT * FROM [AllegatiOPLink] WHERE Head = OBJECT('" + idAllegatoOP + "')";
		searchSQL.setQueryString(query);
		final SearchScope scope = new SearchScope(getOs());

		return scope.fetchObjects(searchSQL, null, null, false);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#inserisciAllegatoComeDocumento(it.ibm.red.business.dto.filenet.DocumentoRedFnDTO,
	 *      com.filenet.api.collection.AccessPermissionList).
	 */
	@Override
	public Document inserisciAllegatoComeDocumento(final DocumentoRedFnDTO allegatoToDoc, final AccessPermissionList permessi) {
		Document docFiglioFilenet = null;
		try {
			final ClassDefinition ts = Factory.ClassDefinition.fetchInstance(getOs(), allegatoToDoc.getClasseDocumentale(), null);
			docFiglioFilenet = Factory.Document.createInstance(getOs(), ts.get_SymbolicName());

			String nomeFile = allegatoToDoc.getDocumentTitle();

			if (!CollectionUtils.isEmpty(allegatoToDoc.getMetadati())) {
				nomeFile = (String) allegatoToDoc.getMetadati().get(getPP().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY));
				setPropertiesAndSaveDocument(docFiglioFilenet, allegatoToDoc.getMetadati(), false);
			}

			if (allegatoToDoc.getDataHandler() != null && allegatoToDoc.getDataHandler().getInputStream() != null) {
				setContentDoc(docFiglioFilenet, nomeFile, allegatoToDoc.getContentType(), allegatoToDoc.getDataHandler().getInputStream(), null);
			}

			// Si impostano le security del padre
			docFiglioFilenet.set_Permissions(permessi);
			docFiglioFilenet.checkin(null, CheckinType.MAJOR_VERSION);
			docFiglioFilenet.save(RefreshMode.REFRESH);

			docFiglioFilenet.save(RefreshMode.REFRESH);
		} catch (final Exception e) {
			throw new FilenetException(ERROR_CREAZIONE_FIGLI_MSG, e);
		}

		return docFiglioFilenet;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getRichiestaOPF(int, java.lang.String, int).
	 */
	@Override
	public Document getRichiestaOPF(final int numeroRichiesta, final String annoRichiesta, final int numeroProtocollo) {
		final SearchSQL searchSQL = new SearchSQL(SELECT_FROM + getPP().getParameterByKey(PropertiesNameEnum.IGEPA_OPF_CLASSNAME_FN_METAKEY) + " where Numero = '"
				+ numeroRichiesta + "' AND Anno = " + annoRichiesta + " AND numeroProtocollo = " + numeroProtocollo);

		LOGGER.info(QUERY_DA_ESEGUIRE + searchSQL.toString());
		final SearchScope searchScope = new SearchScope(getOs());
		final DocumentSet result = (DocumentSet) searchScope.fetchObjects(searchSQL, 1, null, false);
		if (!result.isEmpty()) {
			return (Document) result.iterator().next();
		}
		throw new FilenetException("No results found for query: " + searchSQL.toString());
	}

	/**
	 * La mail è sempre contenuta nella prima versione del documento.
	 * 
	 * @param guid
	 * @return
	 * @throws Exception
	 */
	@Override
	public Document getMailAllegata(final String guid) {
		Document docMail = null;

		try {
			Document doc = Factory.Document.fetchInstance(getOs(), idFromGuid(guid), null);
			final SubSetImpl ssi = (SubSetImpl) doc.get_VersionSeries().get_Versions();
			final List<?> list = ssi.getList();
			doc = (Document) list.get(list.size() - 1);

			Annotation annotation = null;
			ContentTransfer result = null;
			final Iterator<?> iter = doc.get_Annotations().iterator();
			while (iter.hasNext()) {
				annotation = (Annotation) iter.next();
				if (FilenetAnnotationEnum.TEXT_MAIL.getNome().equals(annotation.get_DescriptiveText())) {
					result = (ContentTransfer) annotation.get_ContentElements().get(0);
					break;
				}
			}

			if (result != null) {
				final String strId = IOUtils.toString(result.accessContentStream(), null);
				docMail = Factory.Document.fetchInstance(getOs(), new Id(strId), null);
			}

		} catch (final Exception e) {
			LOGGER.error(e);
			throw new FilenetException("Errore nel recupero della mail associata al documento", e);
		}

		return docMail;
	}

	/***********************************************************************************************************/
	/************************************
	 * 11-01-2019 NUOVA RICERCA RAPIDA
	 ****************************************/
	/***********************************************************************************************************/
	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#ricercaGenericaUtente(java.lang.String,
	 *      java.lang.Integer, it.ibm.red.business.enums.RicercaGenericaTypeEnum,
	 *      it.ibm.red.business.dto.UtenteDTO, boolean, boolean).
	 */
	@Override
	public DocumentSet ricercaGenericaUtente(final String key, final Integer anno, final RicercaGenericaTypeEnum type, final UtenteDTO utente, final boolean fullText,
			final boolean orderbyInQuery) {
		return ricercaGenericaUtente(key, anno, type, utente, CampoRicercaEnum.ALL, fullText, orderbyInQuery, true, false);
	}

	/**
	 * Ricerca i documenti tramite criteri di ricerca.
	 * 
	 * @param key
	 *            - Chiave di ricerca(match con Numero protocollo || anno protocollo
	 *            || anno creazione || Oggetto || Barcode || numero documento) testo
	 *            libero che viene tokenizzato secondo il type</br>
	 *            Se anno è valorizzato anno protocollo e creazione sono confrontati
	 *            rispetto quel valore
	 * 
	 * 
	 * @param anno
	 *            - Anno dei documenti da ricercare se null è valido l'anno di
	 *            protocollo scritto in key
	 * 
	 * @param type
	 *            - Tipo della ricerca. </br>
	 *            Controlla la tokenizzazione della key e la modalità in vui vengono
	 *            considerati i vari token
	 *            <ul>
	 *            <li>ESATTA: la key è presa per intero </ QUALSAISI: la key è
	 *            tokenizzata in parole (suddivise da spazi) e i match sono
	 *            applicate a ciascun token, è sufficiente che siano valide per un
	 *            token TUTTE: la key è tokenizzata in parole (suddivise da spazi) e
	 *            i match sono applicate a ciascun token, le condizioni devono
	 *            essere valide per tutti i token
	 *            </ul>
	 * 
	 * @param utente
	 *            - Utente che effettua la ricerca
	 * 
	 * @param fullText
	 *            - Abilita il fullText che verifica se la key è contenuta nel
	 *            contents dei documenti (documento principale, allegati eccetera)
	 *            Quando valorizzata a true, la ricerca avviene solo per le key
	 *            contenute nel documento.
	 * 
	 * @param campoRicerca
	 *            -
	 * 
	 * @param orderbyInQuery
	 *            - Indica se inserire la clausola "ORDER BY" nella query di ricerca
	 * 
	 * @param limitMaxResults
	 *            - Indica se limitare i risultati della ricerca con la property
	 *            impostata a DB
	 * 
	 * @param withContent
	 *            - Indica se recuperare o meno il content dei documenti
	 * 
	 * @return DocumentSet FN dei documenti trovati.
	 */
	@Override
	public DocumentSet ricercaGenericaUtente(final String key, final Integer anno, final RicercaGenericaTypeEnum type, final UtenteDTO utente,
			final CampoRicercaEnum campoRicerca, final boolean fullText, final boolean orderbyInQuery, final boolean limitMaxResults, final boolean withContent) {

		String operatore = "OR"; // caso QUALSIASI
		if (RicercaGenericaTypeEnum.TUTTE.equals(type)) {
			operatore = "AND";
		}
		final List<String> valoriDaRicercareList = FilenetCERicercaRapidaUtenteUtils.tokenizeFreeText(key, type);

		// Condizioni da mettere in AND tra loro
		List<String> inAndconditionList = new LinkedList<>();
		final String freeTextCondition = FilenetCERicercaRapidaUtenteUtils.generateWhereConditionFromFreeText(valoriDaRicercareList, operatore, anno, fullText, campoRicerca,
				getPP());
		inAndconditionList.add(freeTextCondition);

		String yearCondition = "";
		if (anno != null) { // Costruisco la condizione relativa all'anno
			yearCondition = FilenetCERicercaRapidaUtenteUtils.generateYearCondition(anno.toString(), getPP());

			inAndconditionList.add(yearCondition);
		}

		final String currentVersCondition = " d." + PropertyNames.IS_CURRENT_VERSION + UGUALE_TRUE;

		inAndconditionList.add(currentVersCondition);

		final String categoriaDiversaDA5Condition = "d." + getPP().getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY) + " <> 5 "; // in and
		inAndconditionList.add(categoriaDiversaDA5Condition);

		String regRisCondition = ""; // in AND
		if (org.apache.commons.lang3.StringUtils.isNotBlank(freeTextCondition)) {
			regRisCondition = " (d." + getPP().getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY) + "  is null" + " OR " + " d."
					+ getPP().getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY) + EQUAL_0;

			inAndconditionList.add(regRisCondition);
		}

		final String idAooCondition = "d." + getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY) + " = " + utente.getIdAoo(); // in AND
		inAndconditionList.add(idAooCondition);

		final List<String> gruppiAD = getGruppiAD();
		String reservedCondition;
		String campoRiservato;
		boolean hasPermessoCodaCorriere;
		if (gruppiAD != null && !gruppiAD.isEmpty()) {
			campoRiservato = getPP().getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY);
			hasPermessoCodaCorriere = PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.CODA_CORRIERE);
			reservedCondition = org.apache.commons.lang3.StringUtils.join("( ( ( ", campoRiservato, " = 0 OR riservato IS NULL ) AND ACL INTERSECTS ", getACLClause(gruppiAD),
					") OR ( ", campoRiservato, CONSTANT_1_AND_ACL_INTERSECTS, getACLClause(restrictVisibility(utente.getId(), utente.getIdNodoCorriere().intValue(),
							utente.getIdUfficio(), null, null, utente.getUsername(), gruppiAD, hasPermessoCodaCorriere, false, true, utente.getVisFaldoni())),
					" ) )");

			inAndconditionList.add(reservedCondition);
		}

		inAndconditionList = inAndconditionList.stream().filter(condition -> org.apache.commons.lang3.StringUtils.isNotBlank(condition)).collect(Collectors.toList());
		final String conditionINAnd = String.join(AND, inAndconditionList);

		// Costruisco la condizione relativa all'aoo
		final String whereCondition = org.apache.commons.lang3.StringUtils.join(WHERE, conditionINAnd);

		final List<String> selectColumns = new ArrayList<>(
			Arrays.asList(
				PropertyNames.ID, 
				PropertyNames.DATE_CREATED,
				PropertyNames.COMPOUND_DOCUMENT_STATE,
				getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.URGENTE_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.SOTTOCATEGORIA_DOCUMENTO_USCITA),
				getPP().getParameterByKey(PropertiesNameEnum.ID_FORMATO_DOCUMENTO),
				getPP().getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.BARCODE_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_EMERGENZA_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_EMERGENZA_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.FIRMA_PDF_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.TRASFORMAZIONE_PDF_ERRORE_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.FLAG_FIRMA_AUTOGRAFA_RM_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_EMERGENZA_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.MITTENTE_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_PROTOCOLLO_NPS_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.INTEGRAZIONE_DATI_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.ITER_APPROVATIVO_SEMAFORO_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.VERIFICA_FIRMA_DOC_ALLEGATO),
				getPP().getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_VALIDITA_FIRMA),
				getPP().getParameterByKey(PropertiesNameEnum.MAJOR_VERSION_NUMBER_METAKEY)
				
			)
		);

		if (withContent) {
			selectColumns.add(PropertyNames.CONTENT_ELEMENTS);
			selectColumns.add(PropertyNames.MIME_TYPE);
		}

		// ## Inserimento dell'eventuale limitazione al numero massimo dei risultati
		final String limitString = limitMaxResults ? TOP_LABEL + getPP().getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS) : Constants.EMPTY_STRING;

		String sql = SELECT + limitString + " d." + String.join(", d.", selectColumns)
				+ FilenetCERicercaRapidaUtenteUtils.generateFromRicercaGenerica(fullText, valoriDaRicercareList, getPP()) + whereCondition;

		// ## Se desidero effettuare l'ordinamento durante l'esecuzione della query
		// In questo modo impiega più tempo se ha molti record ma da risultati più
		// precisi
		if (orderbyInQuery) {
			sql = sql + ORDER_BY + getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY) + DESC;
		}

		LOGGER.info("Ricerca Generica query: " + sql);
		final SearchSQL searchSQL = new SearchSQL();
		if (fullText && !valoriDaRicercareList.isEmpty()) {
			FilenetCEHelper.setMaxRecord(searchSQL, getPP());
		}
		searchSQL.setQueryString(sql);
		final SearchScope scope = new SearchScope(getOs());
		final DocumentSet ds = (DocumentSet) scope.fetchObjects(searchSQL, null, null, false);
		LOGGER.info("Ricerca Generica query eseguita");

		return ds;
	}

	/**
	 * Ricerca i fascicoli tramite criteri di ricerca.
	 * 
	 * @param key
	 *            - Chiave di ricerca(match con Numero || anno creazione || Oggetto
	 *            || indice di classificazione ) testo libero che viene tokenizzato
	 *            secondo il type
	 * 
	 * @param anno
	 *            - Anno dei documenti da ricercare se null è valido l'anno di
	 *            protocollo scritto in key
	 * 
	 * @param type
	 *            - Tipo della ricerca. Controlla la tokenizzazione della key e la
	 *            modalità in cui vengono considerati i vari token ESATTA: la key è
	 *            presa per intero QUALSIASI: la key è tokenizzata in parole
	 *            (suddivise da spazi) e i match sono applicate a ciascun token, è
	 *            sufficiente che siano valide per un token TUTTE: la key è
	 *            tokenizzata in parole (suddivise da spazi) e i match sono
	 *            applicate a ciascun token, le condizioni devono essere valide per
	 *            tutti i token
	 * 
	 * @param idAoo
	 *            - Id dell'AOO dell'utente
	 * 
	 * @return DocumentSet FN dei documenti trovati.
	 */
	@Override
	public DocumentSet ricercaGenericaFascicoliUtente(final String key, final RicercaGenericaTypeEnum type, final Integer anno, final Long idAoo,
			final boolean orderbyInQuery) {
		String operatore = "OR"; // caso QUALSIASI
		if (RicercaGenericaTypeEnum.TUTTE.equals(type)) {
			operatore = "AND";
		}
		final List<String> valoriDaRicercareList = FilenetCERicercaRapidaUtenteUtils.tokenizeFreeText(key, type);

		List<String> inAndconditionList = new ArrayList<>();

		// creaConditionFromFreeText
		final List<String> inOperatoreCondition = valoriDaRicercareList.stream()
				.map(value -> FilenetCERicercaRapidaUtenteUtils.generateFascicoliConditionForFreeTextSearch(value, getPP())).collect(Collectors.toList());
		String freeTextCondition = String.join(" ) " + operatore + " ( ", inOperatoreCondition);
		// chiudo le parentesi se ho scritto qualcosa
		// Racchiudo anche tutti gli Or tra parentesi più grandi E.G. ( (condition) or
		// (condition) or (condition) )
		// Se c'è un solo elemento sarà racchiuso tra due parentesi... pazienza
		if (!inOperatoreCondition.isEmpty()) {
			freeTextCondition = org.apache.commons.lang3.StringUtils.join("( ", freeTextCondition, " )");
			inAndconditionList.add(freeTextCondition);
		}

		if (anno != null) {
			final String controlloAnno = FilenetCERicercaRapidaUtenteUtils.generateYearFascicoloCondition(anno.toString(), getPP());
			inAndconditionList.add(controlloAnno);
		}

		final String idAooCondition = getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY) + " = " + idAoo.toString(); // in
		// AND
		inAndconditionList.add(idAooCondition);

		final List<String> gruppiAD = getGruppiAD();
		if (gruppiAD != null && !gruppiAD.isEmpty()) {
			final String gruppiAdCondition = " ACL INTERSECTS " + getACLClause(gruppiAD);
			inAndconditionList.add(gruppiAdCondition);
		}

		inAndconditionList = inAndconditionList.stream().filter(condition -> org.apache.commons.lang3.StringUtils.isNotBlank(condition)).collect(Collectors.toList());
		final String inAndconditions = String.join(AND, inAndconditionList);

		// select condition
		final List<String> columnColumns = Arrays.asList(PropertyNames.ID, getPP().getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.STATO_FASCICOLO_METAKEY), getPP().getParameterByKey(PropertiesNameEnum.TITOLARIO_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY), getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA), getPP().getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.FALDONATO_METAKEY), getPP().getParameterByKey(PropertiesNameEnum.DATA_TERMINAZIONE_METAKEY),
				getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
		final String select = String.join(", ", columnColumns);

		String sqlStr = org.apache.commons.lang3.StringUtils.join("SELECT TOP ", getPP().getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS), " ", select, FROM,
				getPP().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY), WHERE, inAndconditions);

		if (orderbyInQuery) {
			sqlStr = sqlStr + ORDER_BY + getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY) + DESC;
		}

		LOGGER.info("ricercaGenericaFascicoliUtente query: " + sqlStr);

		final SearchSQL sql = new SearchSQL(sqlStr);

		// Search the object store with the sql and get the returned items
		final SearchScope searchScope = new SearchScope(getOs());
		return (DocumentSet) searchScope.fetchObjects(sql, null, null, false);
	}

	/***********************************************************************************************************/
	/************************************
	 * END NUOVA RICERCA RAPIDA
	 ***********************************************/
	/***********************************************************************************************************/
	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#copyDocument(com.filenet.api.core.Document,
	 *      com.filenet.api.core.Folder, java.lang.Boolean, java.lang.String).
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Document copyDocument(final Document doc, final Folder rootFolder, final Boolean ancheAllegati, final String docTitle) {
		Document docNew = null;
		List<Document> allegati = null;
		if (Boolean.TRUE.equals(ancheAllegati)) {
			allegati = getChildrens(doc);
		}

		try {
			// creo l'istanza del nuovo documento
			final ClassDefinition ts = Factory.ClassDefinition.fetchInstance(this.getOs(), doc.getClassName(), null);
			// creazione documento
			docNew = Factory.Document.createInstance(this.getOs(), ts.get_SymbolicName());
			final Properties propNew = docNew.getProperties();

			// setto le properties custom della classe documentale
			final Properties props = doc.getProperties();
			final PropertyDefinitionList properties = ts.get_PropertyDefinitions();

			for (final Property pro : props.toArray()) {
				if (pro.isSettable() && isPropertyHidden(properties, pro.getPropertyName())) {
					propNew.putObjectValue(pro.getPropertyName(), pro.getObjectValue());
				}
			}

			if (docTitle != null) {
				propNew.putObjectValue(getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), docTitle);
			}

			// Recupero le info per settare il nuovo content
			if (doc.get_ContentElements() != null && !doc.get_ContentElements().isEmpty()) {
				final ContentElementList elementList = doc.get_ContentElements();
				final ContentTransfer element = (ContentTransfer) elementList.get(0);
				final String mimeType = element.get_ContentType();
				final String fileName = element.get_RetrievalName();
				final InputStream is = doc.accessContentStream(0);

				// set del content del nuovo documento
				setContentDoc(docNew, fileName, mimeType, is, null);
			}

			if (!CollectionUtils.isEmpty(doc.get_Permissions())) {
				final AccessPermissionList acl = Factory.AccessPermission.createList();
				AccessPermission perm = null;
				final Iterator<?> itDocAcl = doc.get_Permissions().iterator();

				while (itDocAcl.hasNext()) {
					final AccessPermission permDoc = (AccessPermission) itDocAcl.next();

					perm = Factory.AccessPermission.createInstance();
					perm.set_AccessMask(permDoc.get_AccessMask());
					perm.set_GranteeName(permDoc.get_GranteeName());
					perm.set_AccessType(permDoc.get_AccessType());
					perm.set_InheritableDepth(permDoc.get_InheritableDepth());
					perm.getProperties().putValue(GRANTEE_TYPE, permDoc.get_GranteeType().getValue());

					acl.add(perm);
				}

				docNew.set_Permissions(acl);
			}

			if (!CollectionUtils.isEmpty(allegati)) {
				impostaCompoundDocument(docNew);
				setChildCompoundDocument(docNew, allegati);
			}

			// checkin e save
			docNew.checkin(AutoClassify.DO_NOT_AUTO_CLASSIFY, CheckinType.MAJOR_VERSION);
			docNew.save(RefreshMode.REFRESH);

			// associazione al folder/Fascicolo
			final ReferentialContainmentRelationship rr = rootFolder.file(docNew, AutoUniqueName.NOT_AUTO_UNIQUE, docNew.get_Name(),
					DefineSecurityParentage.DEFINE_SECURITY_PARENTAGE);
			rr.save(RefreshMode.REFRESH);
		} catch (final Exception e) {
			LOGGER.error(" Errore durante la copia del documento " + doc.get_Name() + " - ", e);
			throw new FilenetException(" Errore durante la copia del documento " + doc.get_Name() + " - ", e);
		}

		LOGGER.info("Copia documento effettuata con successo - document title: " + docNew.get_Name());

		return docNew;

	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#copyDocumentInbox(com.filenet.api.core.Document,
	 *      java.lang.String, java.lang.Boolean, java.lang.String).
	 */
	@Override
	public Document copyDocumentInbox(final Document doc, final String casellaPostale, final Boolean ancheAllegati, final String docTitle) {
		return copyDocument(doc, getFolder(FOLDER_PATH_DELIMITER + getPP().getParameterByKey(PropertiesNameEnum.FOLDER_CASELLA_POSTALE_FN_METAKEY) + FOLDER_PATH_DELIMITER + casellaPostale + FOLDER_PATH_DELIMITER
				+ getPP().getParameterByKey(PropertiesNameEnum.FOLDER_MAIL_INBOX_FN_METAKEY)), ancheAllegati, docTitle);
	}

	private List<Document> getChildrens(final Document docPadre) {
		List<Document> listaAllegati = new ArrayList<>();

		if (CompoundDocumentState.COMPOUND_DOCUMENT_AS_INT == docPadre.get_CompoundDocumentState().getValue()) {
			final Iterator<?> childrens = docPadre.get_ChildDocuments().iterator();
			while (childrens.hasNext()) {
				final Document figlio = (Document) childrens.next();
				listaAllegati.add(figlio);
			}
		}

		return listaAllegati;
	}

	private boolean isPropertyHidden(final PropertyDefinitionList properties, final String propertyName) {
		boolean ris = false;

		for (final Iterator<?> propertyIter = properties.iterator(); propertyIter.hasNext();) {
			final PropertyDefinition property = (PropertyDefinition) propertyIter.next();
			if (propertyName.equals(property.get_SymbolicName()) && Boolean.FALSE.equals(property.get_IsHidden())) {
				ris = true;
				break;
			}
		}

		return ris;
	}

	private void setChildCompoundDocument(final Document doc, final List<Document> allegati) {
		try {
			if (allegati != null) {

				final Iterator<Document> itAllegati = allegati.iterator();
				int i = 0;
				while (itAllegati.hasNext()) {
					final Document allegato = itAllegati.next();

					final ClassDefinition ts = Factory.ClassDefinition.fetchInstance(getOs(), allegato.getClassName(), null);
					final Document docChildren = Factory.Document.createInstance(getOs(), ts.get_SymbolicName());
					final Properties propsNew = docChildren.getProperties();

					final Properties props = allegato.getProperties();
					final PropertyDefinitionList properties = ts.get_PropertyDefinitions();
					for (final Property pro : props.toArray()) {
						if (pro.isSettable() && isPropertyHidden(properties, pro.getPropertyName())) {
							propsNew.putObjectValue(pro.getPropertyName(), pro.getObjectValue());
						}
					}

					// recupero le info per settare il nuovo content
					if (allegato.get_ContentElements() != null && !allegato.get_ContentElements().isEmpty()) {
						final ContentElementList elementList = allegato.get_ContentElements();
						final ContentTransfer element = (ContentTransfer) elementList.get(0);
						final String mimeType = element.get_ContentType();
						final String fileName = element.get_RetrievalName();
						final InputStream is = allegato.accessContentStream(0);

						// set del content del nuovo documento
						setContentDoc(docChildren, fileName, mimeType, is, null);
					}

					docChildren.set_Permissions(allegato.get_Permissions());
					docChildren.checkin(null, CheckinType.MAJOR_VERSION);
					docChildren.save(RefreshMode.REFRESH);

					final ComponentRelationship cr = Factory.ComponentRelationship.createInstance(getOs(), null);
					cr.set_ParentComponent(doc);
					cr.set_ChildComponent(docChildren);
					cr.set_ComponentSortOrder(++i);

					cr.set_ComponentRelationshipType(ComponentRelationshipType.DYNAMIC_CR);
					cr.set_VersionBindType(VersionBindType.LATEST_VERSION);

					cr.set_Name(ALLEGATO_DOCUMENTO_LABEL + allegato.get_Name());
					cr.save(RefreshMode.REFRESH);
				}

			}

		} catch (final Exception e) {
			LOGGER.error(e);
			throw new FilenetException(" Errore durante la creazione dei figli (ArrayList<Document>) del compound document", e);
		}
	}

	/**
	 * Recupera gli allegati di un documento tramite ID FN.
	 * 
	 * @param guid
	 *            - guid del documento .
	 * @param selectList
	 *            - Lista degli attributi in select
	 * @return DocumentSet FN degli allegati.
	 */
	@Override
	public DocumentSet getAllegatiByGuid(final String guid, final List<String> selectList) {
		return getChildsByGuid(new Id(guid), selectList, getPP().getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY), 1, null);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getMailsAsDocument(java.lang.String,
	 *      java.lang.String, java.util.Date, java.util.Date, java.util.List,
	 *      java.util.List, java.lang.String, java.lang.String).
	 */
	@Override
	public final DocumentSet getMailsAsDocument(final String path, final String documentClass, final Date dataDa, final Date dataA, final List<String> searchFields,
			final List<StatoMailEnum> selectedStatList, final String oggettoRicerca, final String mittenteRicerca) {

		try {
			final StringBuilder queryString = new StringBuilder();

			final String projection = FilenetCERicercaMailUtils.generateProjection("d", searchFields);

			queryString.append(FilenetCERicercaMailUtils.generateBody(false, projection, path, documentClass, "d"));

			final String filter = FilenetCERicercaMailUtils.generateSearchFilter(false, dataDa, dataA, selectedStatList, "d", oggettoRicerca, mittenteRicerca, getPP());

			if (org.apache.commons.lang3.StringUtils.isNotBlank(filter)) {
				queryString.append(AND);
				queryString.append(filter);
			}

			final SearchSQL searchSQL = new SearchSQL();
			searchSQL.setQueryString(queryString.toString());

			LOGGER.info(QUERY_STRING + searchSQL.toString());

			final SearchScope scope = new SearchScope(getOs());

			return (DocumentSet) scope.fetchObjects(searchSQL, PAGE_SIZE, null, true);
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new FilenetException(" Si è verificato un errore durante il recupero del document.", e);
		}
	}

	/**
	 * @param pathRecenti
	 * @param pathArchiviate
	 * @param documentClass
	 * @param oggettoRicerca
	 * @param mittenteRicerca
	 * @param dataDa
	 * @param dataA
	 * @param searchFields
	 * @param selectedStatList
	 * @param numeroProtocolloRicerca
	 * @return
	 */
	@Override
	public DocumentSet getMailAsDocumentFullText(final String pathRecenti, final String pathArchiviate, final String documentClass, final String oggettoRicerca,
			final String mittenteRicerca, final Date dataDa, final Date dataA, final List<String> searchFields, final List<StatoMailEnum> selectedStatList,
			final String numeroProtocolloRicerca) {

		try {
			final String projection = FilenetCERicercaMailUtils.generateProjection("d", searchFields);

			String filter = FilenetCERicercaMailUtils.generateFullTextSearchFilter(false, oggettoRicerca, mittenteRicerca, dataDa, dataA, selectedStatList,
					numeroProtocolloRicerca, "d", getPP());

			final Integer maxInt = Integer.parseInt(getPP().getParameterByKey(PropertiesNameEnum.RICERCA_FULLTEXT_MAX_RESULTS));

			filter += "AND (d.this INFOLDER '" + pathRecenti + "' OR d.this INFOLDER '" + pathArchiviate + "')";

			final SearchSQL searchSQL = new SearchSQL();
			searchSQL.setSelectList(projection);
			searchSQL.setFromClauseInitialValue(documentClass, "d", true);
			searchSQL.setFromClauseAdditionalJoin(JoinOperator.INNER, "ContentSearch", "c", "d.This", JoinComparison.EQUAL, "c.QueriedObject", false);
			searchSQL.setWhereClause(filter);
			searchSQL.setMaxRecords(maxInt);

			LOGGER.info(QUERY_STRING + searchSQL.toString());

			final SearchScope scope = new SearchScope(getOs());

			return (DocumentSet) scope.fetchObjects(searchSQL, PAGE_SIZE, null, true);
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new FilenetException("Si è verificato un errore durante il recupero del document.", e);
		}
	}

	/**
	 * Effettua un refresh sulle credenziali filenet dell'utente.
	 * 
	 * @param utente
	 */
	public static void refreshCredential(final UtenteDTO utente) {
		Boolean found = false;

		if (UserContext.get().getSubject() != null) {
			for (final Object obj : UserContext.get().getSubject().getPrivateCredentials()) {
				final WSICredential cr = (WSICredential) obj;
				final String usr = cr.getSecurityToken().UsernameToken.Username;
				if (utente.getFcDTO().getUserName().equalsIgnoreCase(usr)) {
					found = true;
					break;
				}
			}
		}

		if (Boolean.FALSE.equals(found)) {
			initUserContext(utente.getFcDTO().getUserName(), utente.getFcDTO().getPassword(), utente.getFcDTO().getUri(), utente.getFcDTO().getStanzaJAAS());
		}
	}

	/**
	 * @param eliminati
	 * @param registroRiservato
	 * @return
	 */
	@Override
	public final Integer getCountDocumentiCartacei(final boolean eliminati, final boolean registroRiservato) {
		// Si recuperano i documenti cartacei (principali e gli eventuali allegati)
		final List<Document> docCartacei = getDocumentiCartacei(eliminati, registroRiservato);

		return docCartacei.size();
	}

	/**
	 * Recupera i documenti cartacei (principali e gli eventuali allegati).
	 * 
	 * @param eliminati
	 * @param registroRiservato
	 * @return
	 */
	@Override
	public final List<Document> getDocumentiCartacei(final boolean eliminati, final boolean registroRiservato) {
		final List<Document> docCartacei = new ArrayList<>();

		try {
			final StringBuilder condizioneRegistroRiservato = new StringBuilder().append(AND_D)
					.append(getPP().getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY)).append(" IS ");
			if (registroRiservato) {
				condizioneRegistroRiservato.append("NOT NULL AND d.").append(getPP().getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY)).append(" = 1)");
			} else {
				condizioneRegistroRiservato.append("NULL OR d.").append(getPP().getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY)).append(" = 0)");
			}

			final StringBuilder condizioneEliminati = new StringBuilder().append(AND_D).append(getPP().getParameterByKey(PropertiesNameEnum.STATO_DOC_CARTACEO_METAKEY))
					.append(" = ");
			if (eliminati) {
				condizioneEliminati.append(StatoDocCartaceoEnum.ELIMINATO.getId()).append(")");
			} else {
				condizioneEliminati.append(StatoDocCartaceoEnum.ACQUISITO.getId()).append(" OR d.")
						.append(getPP().getParameterByKey(PropertiesNameEnum.STATO_DOC_CARTACEO_METAKEY)).append(" IS NULL)");
			}

			final List<String> selectList = new ArrayList<>();
			selectList.add(PropertyNames.ID);
			selectList.add(PropertyNames.MIME_TYPE);
			selectList.add(PropertyNames.PARENT_RELATIONSHIPS);
			selectList.add(getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
			selectList.add(getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY));
			selectList.add(getPP().getParameterByKey(PropertiesNameEnum.BARCODE_METAKEY));
			selectList.add(getPP().getParameterByKey(PropertiesNameEnum.STATO_RECOGNIFORM_METAKEY));
			selectList.add(getPP().getParameterByKey(PropertiesNameEnum.BARCODE_PRINCIPALE_METAKEY));

			final StringBuilder querySql = new StringBuilder().append(SELECT);

			for (final String select : selectList) {
				querySql.append("d.").append(select).append(", ");
			}
			querySql.delete(querySql.length() - 2, querySql.length());

			querySql.append(FROM).append(getPP().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CARTACEO_CLASSNAME_FN_METAKEY)).append(" d").append(WHERE).append("d.")
					.append(PropertyNames.IS_CURRENT_VERSION).append(UGUALE_TRUE).append(condizioneRegistroRiservato).append(condizioneEliminati);

			final DocumentSet docPrincipaliCartacei = (DocumentSet) new SearchScope(getOs()).fetchObjects(new SearchSQL(querySql.toString()), null, null, false);

			if (docPrincipaliCartacei != null && !docPrincipaliCartacei.isEmpty()) {
				final Iterator<?> itDocPrincipaliCartacei = docPrincipaliCartacei.iterator();

				while (itDocPrincipaliCartacei.hasNext()) {
					docCartacei.add((Document) itDocPrincipaliCartacei.next());
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new FilenetException("Si è verificato un errore durante il recupero dei documenti cartacei", e);
		}

		return docCartacei;
	}

	/**
	 * @param messageId
	 * @param indirizzoEmail
	 * @param from
	 * @return
	 */
	@Override
	public boolean checkEmailDuplicata(final String messageId, final String indirizzoEmail, final String from) {
		return checkEmailDuplicata(messageId, indirizzoEmail, from, getPP().getParameterByKey(PropertiesNameEnum.FOLDER_MAIL_INBOX_FN_METAKEY), null);
	}

	/**
	 * @param messageId
	 * @param indirizzoEmail
	 * @param folder
	 * @param from
	 * @return
	 */
	@Override
	public final boolean checkEmailDuplicata(final String messageId, final String indirizzoEmail, final String from, final String folder,
			final String classeDocumentaleEmail) {
		boolean isEmailDuplicata = false;

		String classeDocumentaleEmailRicerca = getPP().getParameterByKey(PropertiesNameEnum.MAIL_CLASSNAME_FN_METAKEY);
		if (!StringUtils.isNullOrEmpty(classeDocumentaleEmail)) {
			classeDocumentaleEmailRicerca = classeDocumentaleEmail;
		}

		final StringBuilder queryString = new StringBuilder().append(SELECT_ID).append(FROM).append(classeDocumentaleEmailRicerca).append(WHERE)
				.append(getPP().getParameterByKey(PropertiesNameEnum.MESSAGE_ID)).append(" = '").append(messageId).append("' AND ").append(PropertyNames.IS_CURRENT_VERSION)
				.append(" = TRUE AND ").append(getPP().getParameterByKey(PropertiesNameEnum.CASELLA_POSTALE_METAKEY)).append(" = '").append(indirizzoEmail)
				.append("' AND [This] INSUBFOLDER '/").append(getPP().getParameterByKey(PropertiesNameEnum.FOLDER_CASELLA_POSTALE_FN_METAKEY)).append(FOLDER_PATH_DELIMITER)
				.append(indirizzoEmail).append(FOLDER_PATH_DELIMITER).append(folder).append("'");

		if (from != null) {
			queryString.append(AND).append(getPP().getParameterByKey(PropertiesNameEnum.MITTENTE_MAIL_METAKEY)).append(" = '").append(from).append("'");
		}

		/*
		 * Esempio di query costruita: SELECT [This], [Id] FROM [Email] WHERE
		 * ([messageId] = '<84542730.29.1434984133342.JavaMail.root@bllr-was7-prod04>'
		 * AND [IsCurrentVersion] = TRUE AND [casellaPostale] =
		 * 'rgs.centrale.gedoc@pec.mef.gov.it') AND [This] INSUBFOLDER
		 * '/CasellePostali/testred@pec.mef.gov.it/Inbox')
		 */

		LOGGER.info(queryString.toString());
		final SearchSQL sql = new SearchSQL(queryString.toString());
		final SearchScope searchScope = new SearchScope(getOs());
		final DocumentSet documents = (DocumentSet) searchScope.fetchObjects(sql, null, null, false);
		final Iterator<?> it = documents.iterator();

		if (it.hasNext()) {
			isEmailDuplicata = true;
			LOGGER.info("E-mail duplicata -> Message-ID: " + messageId);
		}

		return isEmailDuplicata;
	}

	/**
	 * Restituisce il documento di (classe documentale) spedizione associato alla
	 * mail identificata dal Message-ID in input.
	 * 
	 * @param messageId
	 *            Message-ID della mail di cui si vuole recuperare il documento di
	 *            spedizione
	 * @return Documento FileNet di spedizione
	 */
	@Override
	public final Document getSpedizioneByMessageId(final String messageId) {
		LOGGER.info("Recupero del documento di spedizione associato alla mail identificata dal Message-ID: " + messageId);
		Document documentoSpedizione = null;

		try {
			final StringBuilder queryString = new StringBuilder().append("SELECT d.Id").append(FROM)
					.append(getPP().getParameterByKey(PropertiesNameEnum.DOCUMENTO_SPEDIZIONE_CLASSNAME_FN_METAKEY)).append(" d").append(" INNER JOIN")
					.append(" ReferentialContainmentRelationship r ON d.This = r.Head").append(WHERE).append(" r.Tail = OBJECT('/")
					.append(getPP().getParameterByKey(PropertiesNameEnum.FOLDER_SPEDIZIONI_FN_METAKEY)).append("')").append(AND_D_TABLE)
					.append(getPP().getParameterByKey(PropertiesNameEnum.MESSAGE_ID)).append(" = '").append(messageId).append("'");

			final SearchSQL searchSQL = new SearchSQL();
			searchSQL.setQueryString(queryString.toString());
			final SearchScope scope = new SearchScope(getOs());

			final DocumentSet documentoSpedizioneSet = (DocumentSet) scope.fetchObjects(searchSQL, null, null, false);
			final Iterator<?> it = documentoSpedizioneSet.iterator();
			if (it.hasNext()) {
				documentoSpedizione = getDocument(((Document) it.next()).get_Id());
			}

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero del documento di spedizione associato alla mail con Message-ID: " + messageId, e);
		}

		return documentoSpedizione;
	}

	/**
	 * @param messageId
	 * @param datiCert
	 */
	@SuppressWarnings("unchecked")
	@Override
	public final void gestisciSpedizioneDestinatario(final String messageId, final DatiCertDTO datiCert) {
		final Document docFnSpedizione = getSpedizioneByMessageId(messageId);

		if (docFnSpedizione != null) {
			// Si recupera il valore del metadato o, in alternativa, si procede alla
			// creazione
			StringList sl = (StringList) TrasformerCE.getMetadato(docFnSpedizione, PropertiesNameEnum.NOTIFICA_DESTINATARIO_SPEDIZIONE_FN_METAKEY);
			if (sl == null) {
				sl = Factory.StringList.createList();
			}

			final Map<String, Object> metadati = new HashMap<>();

			// Si aggiorna l'esito della spedizione per il destinatario (attributo
			// "consegna" del DatiCert) con la tipologia di notifica
			final String spedizionePerDestinatario = datiCert.getConsegna() + "|" + datiCert.getTipoNotificaPec().getNomeFilenet();
			sl.add(spedizionePerDestinatario);

			metadati.put(getPP().getParameterByKey(PropertiesNameEnum.NOTIFICA_DESTINATARIO_SPEDIZIONE_FN_METAKEY), sl);

			// Si procede con l'aggiornamento del metadato
			updateMetadati(docFnSpedizione, metadati);
		} else {
			LOGGER.info("Non è stato trovato il documento di spedizione associato alla mail con Message-ID: " + messageId);
		}
	}

	/*********************
	 * SICOGE
	 *********************/
	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#fetchOPProperties(java.lang.String).
	 */
	@Override
	public List<Properties> fetchOPProperties(final String idDocumento) {
		List<Properties> output = null;
		final CustomObject elencoOP = fetchElencoOP(idDocumento);
		if (elencoOP != null) {
			output = fetchOP(elencoOP);
		}
		return output;
	}

	private CustomObject fetchElencoOP(final String idDocumentoDecreto) {
		final String opProxy = getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_OP_PROXY);
		final SearchScope ss = new SearchScope(getOs());
		final String queryString = "SELECT This, " + opProxy + " FROM DecretoDirigenzialeFEPA WHERE IsCurrentVersion=TRUE AND " + "DocumentTitle = '" + idDocumentoDecreto
				+ "'";
		final SearchSQL searchSQL = new SearchSQL(queryString);
		final IndependentObjectSet rows = ss.fetchObjects(searchSQL, null, null, false);
		final Iterator<?> iter = rows.iterator();
		CustomObject output = null;
		while (iter.hasNext()) {
			final Document decreto = (Document) iter.next();
			if (decreto.getProperties().getObjectValue(opProxy) != null) {
				output = (CustomObject) decreto.getProperties().getObjectValue(opProxy);
			}
		}
		return output;
	}

	private List<Properties> fetchOP(final CustomObject opProxy) {
		final List<Properties> listaDatiOp = new ArrayList<>(15);
		final IndependentObjectSet elencoOP = opProxy.getProperties().getIndependentObjectSetValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_ELENCO_OP));
		for (final Iterator<?> iterator2 = elencoOP.iterator(); iterator2.hasNext();) {
			final CustomObject op = (CustomObject) iterator2.next();
			listaDatiOp.add(op.getProperties());
		}
		return listaDatiOp;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#saveOPPerFascicoloFepa(java.lang.String,
	 *      java.lang.Integer, java.lang.String, java.lang.String, java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 *      java.util.Date, java.lang.String).
	 */
	@Override
	public void saveOPPerFascicoloFepa(final String numeroOP, final Integer esercizio, final String amministrazione, final String ragioneria, final String capitolo,
			final String pianoGestione, final String beneficiario, final String tipoTitolo, final String oggettoSpese, final Date dataEmissione, final String idFascicolo) {
		final SearchScope ss = new SearchScope(getOs());
		final String queryString = SELECT_FROM + getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_CLASSNAME_ORDINE_DI_PAGAMENTO_DD) + WHERE
				+ getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_NUMERO_OP) + " = " + Integer.parseInt(numeroOP) + AND
				+ getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_ESERCIZIO) + " = " + esercizio + AND
				+ getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_AMMINISTRAZ) + " = '" + amministrazione + "'" + AND_PARENTESI_DX_AP
				+ getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_CODICE_RAGIONERIA) + " ='" + Integer.parseInt(ragioneria) + "'" + " OR "
				+ getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_CODICE_RAGIONERIA) + " ='" + ragioneria + "')" + AND
				+ getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_CAPITOLO_NUMERI_SIRGS) + " ='" + capitolo + "'" + AND
				+ getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_PIANO_GESTIONE) + " = " + Integer.parseInt(pianoGestione);

		final SearchSQL searchSQL = new SearchSQL(queryString);
		final IndependentObjectSet cos = ss.fetchObjects(searchSQL, null, null, false);
		if (cos.isEmpty()) {
			LOGGER.error("OP numero " + numeroOP + " non trovato");
		} else {
			final CustomObject opCo = (CustomObject) cos.iterator().next();
			opCo.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA), idFascicolo);
			opCo.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_AMMINISTRAZ), amministrazione);
			opCo.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_CODICE_RAGIONERIA), Integer.parseInt(ragioneria) + "");
			opCo.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_CAPITOLO_NUMERI_SIRGS), capitolo);
			opCo.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_PIANO_GESTIONE), Integer.parseInt(pianoGestione));
			opCo.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_DATA_EMISSIONE_OP), dataEmissione);
			opCo.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_TIPO_OP), Integer.parseInt(tipoTitolo));
			opCo.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY), oggettoSpese);
			opCo.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_BENEFICIARIO), beneficiario);
			opCo.save(RefreshMode.REFRESH);
		}
	}

	/*********************
	 * SIGI
	 *********************/
	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#fetchElencoOPByNumDecreto(java.lang.String).
	 */
	@Override
	public CustomObject fetchElencoOPByNumDecreto(final String numeroDecreto) {

		final Document doc = this.fetchDecreto(numeroDecreto);
		if (doc != null) {
			return (CustomObject) doc.getProperties().getObjectValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_OP_PROXY));
		} else {
			return null;
		}
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#fetchNumeroDecreto(java.lang.String).
	 */
	@Override
	public String fetchNumeroDecreto(final String idDocumento) {
		final SearchScope ss = new SearchScope(getOs());
		final String queryString = SELECT + getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY) + ", "
				+ getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY) + " FROM DecretoDirigenzialeFEPA WHERE " + "DocumentTitle = '" + idDocumento
				+ "' AND IsCurrentVersion=TRUE";
		final SearchSQL searchSQL = new SearchSQL(queryString);
		final RepositoryRowSet rows = ss.fetchRows(searchSQL, null, null, false);
		final Iterator<?> iter = rows.iterator();
		final RepositoryRow row = (RepositoryRow) iter.next();
		return row.getProperties().getInteger32Value(getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY)) + "-"
				+ row.getProperties().getInteger32Value(getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY));
	}

	private Document fetchDecreto(final String numeroDecreto) {
		final SearchScope ss = new SearchScope(getOs());
		final String queryString = "SELECT This, opProxy FROM DecretoDirigenzialeFEPA WHERE IsCurrentVersion=TRUE AND "
				+ getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY) + " = " + numeroDecreto.split("-")[1] + AND
				+ getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY) + " = " + numeroDecreto.split("-")[0];
		final SearchSQL searchSQL = new SearchSQL(queryString);
		final IndependentObjectSet rows = ss.fetchObjects(searchSQL, null, null, false);
		final Iterator<?> iter = rows.iterator();
		Document decreto = null;
		while (iter.hasNext()) {
			decreto = (Document) iter.next();
		}
		return decreto;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#createElencoOP(java.lang.String).
	 */
	@Override
	public CustomObject createElencoOP(final String numeroDecreto) {
		final CustomObject co = Factory.CustomObject.createInstance(getOs(),
				getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_CLASSNAME_ELENCO_ORDINE_DI_PAGAMENTO_DD));
		co.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY), Integer.parseInt(numeroDecreto.split("-")[0]));
		co.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY), Integer.parseInt(numeroDecreto.split("-")[1]));
		co.save(RefreshMode.REFRESH);
		final Document decreto = fetchDecreto(numeroDecreto);

		if (decreto != null) {
			decreto.getProperties().putObjectValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_OP_PROXY), co);
			decreto.save(RefreshMode.NO_REFRESH);
		} else {
			LOGGER.error("Errore recupero decreto, risulta null.");
			throw new RedException("Errore recupero decreto, risulta null.");
		}

		return co;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#salvaContatto(java.lang.String,
	 *      java.util.List).
	 */
	@Override
	public CustomObject salvaContatto(final String objectIdCont, final List<Contatto> listContatti) {
		CustomObject contattoProxy = null;

		if (StringUtils.isNullOrEmpty(objectIdCont)) {
			contattoProxy = Factory.CustomObject.createInstance(getOs(), getPP().getParameterByKey(PropertiesNameEnum.LISTA_CONTATTI_METADATO_CLASSNAME));
			contattoProxy.save(RefreshMode.REFRESH);
		} else {
			final Id objectId = Id.asIdOrNull(objectIdCont);
			contattoProxy = Factory.CustomObject.fetchInstance(getOs(), objectId, null);
		}

		for (final Contatto contatto : listContatti) {
			final CustomObject co = Factory.CustomObject.createInstance(getOs(), getPP().getParameterByKey(PropertiesNameEnum.CONTATTO_METADATO_CLASSNAME));
			co.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.CONTATTO_METADATO_PROXY), contattoProxy);
			co.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.ALIAS_CONTATTO_ON_THE_FLY), contatto.getAliasContatto());
			co.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.NOME_CONTATTO_ON_THE_FLY), contatto.getNome());
			co.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.COGNOME_CONTATTO_ON_THE_FLY), contatto.getCognome());
			co.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.MAILPEC_CONTATTO_ON_THE_FLY), contatto.getMailPec());
			co.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.MAILPEO_CONTATTO_ON_THE_FLY), contatto.getMail());
			co.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.ID_CONTATTO_ON_THE_FLY), String.valueOf(contatto.getContattoID()));

			co.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.TIPO_CONTATTO_ON_THE_FLY), contatto.getTipoPersona());
			co.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.INDIRIZZO_CONTATTO_ON_THE_FLY), contatto.getIndirizzo());
			co.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.CAP_CONTATTO_ON_THE_FLY), contatto.getCAP());
			co.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.TELEFONO_CONTATTO_ON_THE_FLY), contatto.getTelefono());
			co.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.CELLULARE_CONTATTO_ON_THE_FLY), contatto.getCellulare());
			co.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.FAX_CONTATTO_ON_THE_FLY), contatto.getFax());
			Boolean isPEC = null;
			if (contatto.getTipologiaEmail() != null) {
				isPEC = TipologiaIndirizzoEmailEnum.PEC.equals(contatto.getTipologiaEmail());
			}
			co.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.IS_TIPOLOGIA_PEC_CONTATTO), isPEC);
			if (contatto.getOnTheFly() != null) {
				co.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.IS_ON_THE_FLY), contatto.getOnTheFly());
			}

			if (!StringUtils.isNullOrEmpty(contatto.getIdRegioneIstat()) && !"NULL".equalsIgnoreCase(contatto.getIdRegioneIstat())) {
				try {
					final Integer idRegioneIstato = Integer.parseInt(contatto.getIdRegioneIstat());
					co.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.ID_REGIONE_CONTATTO_ON_THE_FLY), idRegioneIstato);
				} catch (final NumberFormatException e) {
					// In questo caso mettiamo solo warning perchè gli utenti non usano piu l'input
					// regione
					LOGGER.warn("L'idRegione inserito non è corretto", e);
				}
			}
			if (!StringUtils.isNullOrEmpty(contatto.getIdProvinciaIstat()) && !"NULL".equalsIgnoreCase(contatto.getIdProvinciaIstat())) {
				try {
					final Integer idProvinciaIstat = Integer.parseInt(contatto.getIdProvinciaIstat());
					co.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.ID_PROVINCIA_CONTATTO_ON_THE_FLY), idProvinciaIstat);
				} catch (final NumberFormatException e) {
					LOGGER.error("L'idProvincia inserito non è corretto", e);
				}
			}
			if (!StringUtils.isNullOrEmpty(contatto.getIdComuneIstat()) && !"NULL".equalsIgnoreCase(contatto.getIdComuneIstat())) {
				try {
					final Integer idComuneIstato = Integer.parseInt(contatto.getIdComuneIstat());
					co.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.ID_COMUNE_CONTATTO_ON_THE_FLY), idComuneIstato);
				} catch (final NumberFormatException e) {
					LOGGER.error("L'idComune inserito non è corretto", e);
				}
			}
			co.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.TITOLO_CONTATTO_ON_THE_FLY), contatto.getTitolo());
			co.save(RefreshMode.REFRESH);
		}
		return contattoProxy;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#fetchContatti(java.lang.String).
	 */
	@Override
	public IndependentObjectSet fetchContatti(final String objectIdCont) {
		final SearchScope ss = new SearchScope(getOs());

		final String queryString = "SELECT * FROM Contatto " + " WHERE contattoProxy = OBJECT(" + objectIdCont + ")";
		final SearchSQL searchSQL = new SearchSQL(queryString);
		return ss.fetchObjects(searchSQL, null, null, false);

	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#fetchOP(com.filenet.api.core.CustomObject, java.lang.Integer).
	 */
	@Override
	public CustomObject fetchOP(final CustomObject elencoOP, final Integer numeroOp) {
		final SearchScope ss = new SearchScope(getOs());

		final String queryString = SELECT_FROM + getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_CLASSNAME_ORDINE_DI_PAGAMENTO_DD) + WHERE
				+ getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_NUMERO_OP) + " = " + numeroOp + AND
				+ getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_PROXY_DECRETO) + " = OBJECT(" + elencoOP.get_Id() + ")";
		final SearchSQL searchSQL = new SearchSQL(queryString);
		final IndependentObjectSet cos = ss.fetchObjects(searchSQL, null, null, false);
		CustomObject out = null;
		if (!cos.isEmpty()) {
			out = (CustomObject) cos.iterator().next();
		}
		return out;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#updateOP(com.filenet.api.core.CustomObject,
	 *      java.lang.Integer, java.lang.String, java.lang.String,
	 *      java.lang.Integer, java.lang.Integer, java.lang.Integer,
	 *      java.lang.String, java.lang.Integer).
	 */
	@Override
	public void updateOP(final CustomObject opFilenet, final Integer annoEseOP, final String codiceAmministrazione, final String codiceRagioneria,
			final Integer numeroCapitolo, final Integer numeroOP, final Integer pianoGestione, final String statoOP, final Integer tipoOP) {
		storeOP(null, opFilenet, annoEseOP, codiceAmministrazione, codiceRagioneria, numeroCapitolo, numeroOP, pianoGestione, statoOP, tipoOP);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#createOP(com.filenet.api.core.CustomObject,
	 *      java.lang.Integer, java.lang.String, java.lang.String,
	 *      java.lang.Integer, java.lang.Integer, java.lang.Integer,
	 *      java.lang.String, java.lang.Integer).
	 */
	@Override
	public void createOP(final CustomObject elencoOP, final Integer annoEseOP, final String codiceAmministrazione, final String codiceRagioneria, final Integer numeroCapitolo,
			final Integer numeroOP, final Integer pianoGestione, final String statoOP, final Integer tipoOP) {
		storeOP(elencoOP, null, annoEseOP, codiceAmministrazione, codiceRagioneria, numeroCapitolo, numeroOP, pianoGestione, statoOP, tipoOP);
	}

	private void storeOP(final CustomObject elencoOP, CustomObject opFilenet, final Integer annoEseOP, final String codiceAmministrazione, final String codiceRagioneria,
			final Integer numeroCapitolo, final Integer numeroOP, final Integer pianoGestione, final String statoOP, final Integer tipoOP) {
		String strNumCap = null;
		if (numeroCapitolo != null) {
			strNumCap = numeroCapitolo + "";
		}

		if (opFilenet == null) {
			opFilenet = Factory.CustomObject.createInstance(getOs(), getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_CLASSNAME_ORDINE_DI_PAGAMENTO_DD));
		}
		if (elencoOP != null) {
			opFilenet.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_PROXY_DECRETO), elencoOP);
		}
		opFilenet.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_ESERCIZIO), annoEseOP);
		opFilenet.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_AMMINISTRAZ), codiceAmministrazione);
		// //codice gestionale non viene salvato
		opFilenet.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_CODICE_RAGIONERIA), codiceRagioneria);
		opFilenet.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_CAPITOLO_NUMERI_SIRGS), strNumCap);
		opFilenet.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_NUMERO_OP), numeroOP);
		opFilenet.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_PIANO_GESTIONE), pianoGestione);
		opFilenet.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_STATO_OP), statoOP);
		opFilenet.getProperties().putValue(getPP().getParameterByKey(PropertiesNameEnum.FEPA_METADATO_TIPO_OP), tipoOP);
		opFilenet.save(RefreshMode.REFRESH);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#ricercaAvanzataDaWs(java.lang.String, java.lang.String, java.lang.String, java.lang.Integer).
	 */
	@Override
	public final DocumentSet ricercaAvanzataDaWs(final String classeDocumentale, final String fullTextCondition, final String whereCondition,
			final Integer inLimitMaxResults) {
		DocumentSet documentiRicerca = null;

		try {
			// Se il limite massimo non è impostato, si restituiscono al più 100 risultati
			final int limitMaxResults = (inLimitMaxResults == null || inLimitMaxResults <= 0) ? 100 : inLimitMaxResults;

			final SearchSQL searchSQL = new SearchSQL();

			// Limite massimo di record restituiti
			searchSQL.setMaxRecords(limitMaxResults);
			searchSQL.setSelectList(PropertyNames.ID);

			// Classe documentale
			searchSQL.setFromClauseInitialValue(classeDocumentale, null, false);

			// Clausola WHERE
			if (!StringUtils.isNullOrEmpty(whereCondition)) {
				searchSQL.setWhereClause(whereCondition);
			}

			// Clausola FULL TEXT
			if (!StringUtils.isNullOrEmpty(fullTextCondition)) {
				searchSQL.setContainsRestriction(classeDocumentale, fullTextCondition);
			}

			final SearchScope scope = new SearchScope(getOs());
			documentiRicerca = (DocumentSet) scope.fetchObjects(searchSQL, null, null, false);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'esecuzione della query FileNet per la ricerca avanzata sollecitata tramite web service: " + e.getMessage(), e);
			throw new FilenetException("Errore durante l'esecuzione della query per la ricerca avanzata", e);
		}

		return documentiRicerca;
	}

	private List<PropertyTemplate> getAllPropertyTemplate() {
		final List<PropertyTemplate> output = new ArrayList<>();

		try {

			final SearchSQL sql = new SearchSQL("SELECT * FROM PropertyTemplate");
			final SearchScope searchScope = new SearchScope(getOs());
			final PropertyTemplateSet pts = (PropertyTemplateSet) searchScope.fetchObjects(sql, null, null, false);
			final Iterator<?> ptsIterator = pts.iterator();

			while (ptsIterator.hasNext()) {
				final PropertyTemplate pt = (PropertyTemplate) ptsIterator.next();
				output.add(pt);
			}

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'esecuzione della query per il recupero dell'elenco delle Properties da Filenet: ", e);
			throw new FilenetException("Si è verificato un errore durante l'esecuzione della query per il recupero dell'elenco delle Properties da Filenet: ", e);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#elencoProperties().
	 */
	@Override
	public List<PropertyTemplateDTO> elencoProperties() {
		final List<PropertyTemplateDTO> output = new ArrayList<>();
		PropertyTemplateDTO property = null;

		try {

			final List<PropertyTemplate> properties = getAllPropertyTemplate();

			for (final PropertyTemplate prop : properties) {

				final int tipo = prop.get_DataType().getValue();

				if (prop.get_IsHidden() == null || !prop.get_IsHidden()) {

					if (TypeID.BOOLEAN_AS_INT == tipo || TypeID.DOUBLE_AS_INT == tipo || TypeID.LONG_AS_INT == tipo) {

						property = new PropertyTemplateDTO();
						property.setNomeProperty(prop.get_SymbolicName());
						property.setTipo(PropertyTemplateDTO.TIPOINT);
						output.add(property);

					} else if (TypeID.DATE_AS_INT == tipo) {

						property = new PropertyTemplateDTO();
						property.setNomeProperty(prop.get_SymbolicName());
						property.setTipo(PropertyTemplateDTO.TIPODATE);
						output.add(property);

					} else if (TypeID.STRING_AS_INT == tipo) {

						property = new PropertyTemplateDTO();
						property.setNomeProperty(prop.get_SymbolicName());
						property.setTipo(PropertyTemplateDTO.TIPOSTRING);
						output.add(property);

					} else if (TypeID.BINARY_AS_INT == tipo) {

						property = new PropertyTemplateDTO();
						property.setNomeProperty(prop.get_SymbolicName());
						property.setTipo(PropertyTemplateDTO.TIPOBINARY);
						output.add(property);

					}
				}
			}

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la codifica delle Properties Filenet in DTO: ", e);
			throw new FilenetException("Si è verificato un errore durante la codifica delle Properties Filenet in DTO: ", e);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#ricercaAllegatiGenerica(java.lang.String,
	 *      java.lang.Integer, it.ibm.red.business.enums.RicercaGenericaTypeEnum).
	 */
	@Override
	public DocumentSet ricercaAllegatiGenerica(final String stringaRicerca, final Integer anno, final RicercaGenericaTypeEnum modalitaRicercaTestuale) {
		LOGGER.info("Ricerca Generica - ricercaAllegatiFullText -> START");
		DocumentSet allegatiRicercaTesto = null;

		try {
			final StringBuilder query = buildQueryRicercaAllegati();

			// ### Gestione ricerca testuale
			String stringheVal = "";
			if (modalitaRicercaTestuale != null) {
				stringheVal = StringUtils.formattaStringaPerRicercaTestualeRapida(stringaRicerca, modalitaRicercaTestuale);
				query.append(" WHERE CONTAINS(d.*, '").append(stringheVal).append("') ");
			}

			// ### Creazione delle date creazione rispetto all'anno
			final Integer annoDocumento = anno;
			query.append(updateQueryRicercaAllegati(annoDocumento));

			final SearchSQL sql = new SearchSQL(query.toString());
			LOGGER.info("Ricerca Generica - ricercaAllegatiFullText -> Query da eseguire: " + sql.toString());

			final SearchScope searchScope = new SearchScope(getOs());
			allegatiRicercaTesto = (DocumentSet) searchScope.fetchObjects(sql, null, null, false);
		} catch (final Exception e) {
			LOGGER.error("Ricerca Generica - ricercaAllegatiFullText -> Si è verificato un errore durante l'esecuzione della ricerca avanzata nel CE.", e);
			throw new FilenetException("Ricerca Generica - Si è verificato un errore durante l'esecuzione della ricerca avanzata nel CE.", e);
		}

		LOGGER.info("Ricerca Generica - ricercaAllegatiFullText -> END");
		return allegatiRicercaTesto;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getMailsAsDocumentRicerca(java.lang.String,
	 *      java.lang.String, java.lang.String, java.util.Date, java.util.Date,
	 *      java.util.List, java.util.List, java.lang.String, java.lang.String,
	 *      java.lang.String).
	 */
	@Override
	public final DocumentSet getMailsAsDocumentRicerca(final String pathRecenti, final String pathArchiviate, final String documentClass, final Date dataDa, final Date dataA,
			final List<String> searchFields, final List<StatoMailEnum> selectedStatList, final String oggettoRicerca, final String mittenteRicerca,
			final String numeroProtocolloRicerca) {
		DocumentSet outputRicercaMail = null;

		try {
			final StringBuilder queryString = new StringBuilder();

			final String projection = FilenetCERicercaMailUtils.generateProjection("d", searchFields);

			queryString.append(FilenetCERicercaMailUtils.generateBodyRicerca(false, projection, pathRecenti, pathArchiviate, documentClass, "d"));

			final String filter = FilenetCERicercaMailUtils.generateSearchFilter(false, dataDa, dataA, selectedStatList, "d", oggettoRicerca, mittenteRicerca,
					numeroProtocolloRicerca, getPP());

			if (org.apache.commons.lang3.StringUtils.isNotBlank(filter)) {
				queryString.append(AND);
				queryString.append(filter);
			}

			final SearchSQL searchSQL = new SearchSQL();
			searchSQL.setQueryString(queryString.toString());

			LOGGER.info(QUERY_STRING + searchSQL.toString());

			final SearchScope scope = new SearchScope(getOs());
			outputRicercaMail = (DocumentSet) scope.fetchObjects(searchSQL, PAGE_SIZE, null, true);
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new FilenetException(" Si è verificato un errore durante il recupero del document.", e);
		}

		return outputRicercaMail;
	}

	/**
	 * Restituisce l'hashMap che definisce le propertyDescription.
	 * 
	 * @param document
	 * @return HashMap<String, PropertyDescription>
	 */
	@SuppressWarnings("rawtypes")
	public static Map<String, PropertyDescription> getHasMapClassDefinition(final Document document) {

		final HashMap<String, PropertyDescription> hmp = new HashMap<>();

		final ClassDescription cd = document.get_ClassDescription();
		final PropertyDescriptionList pdl = cd.get_PropertyDescriptions();

		final Iterator it = pdl.iterator();
		while (it.hasNext()) {
			final PropertyDescription pd = (PropertyDescription) it.next();
			hmp.put(pd.get_SymbolicName(), pd);
		}

		return hmp;

	}

	/**
	 * Effettua un parsing del valore del metadato in modo da recuperarne l'oggetto
	 * piu rappresentativo.
	 * 
	 * @param key
	 * @param value
	 * @param multiValues
	 * @param type
	 * @param cardinality
	 * @return un'istanza della classe piu opportuna valorizzata con il value
	 */
	public static Object getObjectByProperty(final String key, final String value, final List<String> multiValues, final int type, final int cardinality) {

		try {

			if (value == null && multiValues == null) {
				return null;
			}

			if (type == TypeID.BOOLEAN_AS_INT) {
				return new Boolean(value);
			} else if (type == TypeID.DATE_AS_INT) {

				final Date parserDate = DateUtils.parseDate(value);
				if (parserDate == null) {
					throw new FilenetException("Errore durante la conversione del metadato [ " + key + " ] da String --> Date");
				}

				return DateUtils.parseDate(value);

			} else if (type == TypeID.DOUBLE_AS_INT) {
				return new Double(value);
			} else if (type == TypeID.LONG_AS_INT) {

				if (cardinality == Cardinality.LIST_AS_INT) {
					final Integer32List il = Factory.Integer32List.createList();
					for (final String val : multiValues) {
						il.add(val);
					}
					return il;
				} else {
					return new Integer(value);
				}

			} else if (type == TypeID.STRING_AS_INT) {

				if (cardinality == Cardinality.LIST_AS_INT) {
					final StringList sl = Factory.StringList.createList();
					for (final String val : multiValues) {
						sl.add(val);
					}
					return sl;
				} else {
					return value;
				}

			}

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il parsing del valore del metadato [" + key + "]. ", e);
			throw new FilenetException("Si è verificato un errore durante il parsing del valore del metadato [" + key + "]. ", e);
		}

		return null;

	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#ricercaEsitiUscita(java.util.Map).
	 */
	@Override
	public DocumentSet ricercaEsitiUscita(final Map<String, Object> mappaValoriRicerca) {
		DocumentSet documentiRicerca = null;

		try {

			// Se il limite massimo non è impostato, si restituiscono al più 100 risultati
			Integer numberOfElements = 100;
			if (getPP().getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS) != null) {
				numberOfElements = Integer.parseInt(getPP().getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS));
			}

			final SearchSQL searchSQL = new SearchSQL();

			// Limite massimo di record restituiti
			searchSQL.setMaxRecords(numberOfElements);
			searchSQL.setSelectList(D_STAR);

			// Classe documentale
			final String classeDocumentale = (String) mappaValoriRicerca.get(Ricerca.CLASSE_DOCUMENTALE_FWS);
			searchSQL.setFromClauseInitialValue(classeDocumentale, "d", true);
			mappaValoriRicerca.remove(Ricerca.CLASSE_DOCUMENTALE_FWS);

			// Clausola WHERE
			final StringBuilder whereCondition = new StringBuilder();

			whereCondition.append(" IsCurrentVersion = TRUE ");

			final Set<String> chiaviRicerca = mappaValoriRicerca.keySet();
			final Iterator<String> itChiaviRicerca = chiaviRicerca.iterator();

			while (itChiaviRicerca.hasNext()) {
				final String chiave = itChiaviRicerca.next();
				String valoreString = String.valueOf(mappaValoriRicerca.get(chiave));

				// Si rimuovono i caratteri speciali, sostituendoli con degli spazi che poi
				// vengono rimossi
				valoreString = StringUtils.deleteSpecialCharacterForSpace(valoreString).trim();

				if (Ricerca.TIPO_ESITO_DOC.equals(chiave)) {
					// Numero protocollo da
					whereCondition.append(AND);
					whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.SOTTOCATEGORIA_DOCUMENTO_USCITA)).append(" = ").append(valoreString);
				} else if (Ricerca.NUMERO_PROTOCOLLO_DA.equals(chiave)) {
					// Numero protocollo da
					whereCondition.append(AND);
					whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY)).append(" >= ").append(valoreString);
				} else if (Ricerca.NUMERO_PROTOCOLLO_A.equals(chiave)) {
					// Numero protocollo a
					whereCondition.append(AND);
					whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY)).append(" <= ").append(valoreString);
				} else if (Ricerca.DATA_CREAZIONE_DA.equals(chiave)) {
					// Data creazione da
					whereCondition.append(AND);
					whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY)).append(" >= ").append(valoreString);
				} else if (Ricerca.DATA_CREAZIONE_A.equals(chiave)) {
					// Data creazione a
					whereCondition.append(AND);
					whereCondition.append(getPP().getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY)).append(" <= ").append(valoreString);
				}
			}

			if (!StringUtils.isNullOrEmpty(whereCondition.toString())) {
				searchSQL.setWhereClause(whereCondition.toString());
			}

			final SearchScope scope = new SearchScope(getOs());
			documentiRicerca = (DocumentSet) scope.fetchObjects(searchSQL, null, null, false);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la ricerca dei documenti/esiti. ", e);
			throw new FilenetException("Si è verificato un errore durante la ricerca dei documenti/esiti. ", e);
		}

		return documentiRicerca;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#spostaMail(java.lang.String,
	 *      java.lang.String, com.filenet.api.collection.FolderSet, java.util.Date).
	 */
	@Override
	public void spostaMail(final String folderDestinazione, final String folder, final FolderSet mailBoxs, final Date milestoneDate) {
		if (mailBoxs != null) {

			final Iterator<?> itf = mailBoxs.iterator();
			while (itf.hasNext()) {
				try {
					String mailBox = ((Folder) itf.next()).get_FolderName();

					final String rootFolderDestinazione = folder + FOLDER_PATH_DELIMITER + mailBox + FOLDER_PATH_DELIMITER + folderDestinazione;
					final Folder folderDestinazioneFN = getFolder(rootFolderDestinazione);

					mailBox += FOLDER_PATH_DELIMITER + getPP().getParameterByKey(PropertiesNameEnum.FOLDER_MAIL_INBOX_FN_METAKEY);
					final String path = FOLDER_PATH_DELIMITER + folder + FOLDER_PATH_DELIMITER + mailBox;
					final Folder folderOrigine = getFolder(folder + FOLDER_PATH_DELIMITER + mailBox);
					LOGGER.info("Casella mail: " + mailBox);
					LOGGER.info("Path mail: " + path);
					LOGGER.info("Folder destinazione: " + rootFolderDestinazione);

					String filter = AND_D + getPP().getParameterByKey(PropertiesNameEnum.DATA_RICEZIONE_MAIL_METAKEY) + " < "
							+ DateUtils.getIsoDatetimeFormat(milestoneDate) + ")";
					filter += AND_D + getPP().getParameterByKey(PropertiesNameEnum.STATO_MAIL_METAKEY) + " <> " + StatoMailEnum.INARRIVO.getStatus() + ")";
					LOGGER.info("filter: " + filter);

					final DocumentSet docs = getMailsAsDocument(path, getPP().getParameterByKey(PropertiesNameEnum.MAIL_CLASSNAME_FN_METAKEY), filter, null);
					final Iterator<?> it = docs.iterator();

					if (it != null && it.hasNext()) {

						while (it.hasNext()) {

							final Document doc = (Document) it.next();
							LOGGER.info("DocumentTitle: " + doc.getProperties().getStringValue(getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY)));
							LOGGER.info("Stato mail: " + doc.getProperties().getInteger32Value(getPP().getParameterByKey(PropertiesNameEnum.STATO_MAIL_METAKEY)));

							final ReferentialContainmentRelationship rcr = folderDestinazioneFN.file(doc, AutoUniqueName.NOT_AUTO_UNIQUE,
									doc.getProperties().getStringValue(getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY)),
									DefineSecurityParentage.DO_NOT_DEFINE_SECURITY_PARENTAGE);
							rcr.save(RefreshMode.REFRESH);

							final ReferentialContainmentRelationship rcrold = folderOrigine.unfile(doc);
							rcrold.save(RefreshMode.REFRESH);
						}
					}
				} catch (final Exception e) {
					throw new RedException("spostaMail ->  Errore durante lo spostamento delle mail", e);
				}
			}
		}
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getDocumentTitlesFromDocumentSet(com.filenet.api.collection.DocumentSet).
	 */
	@Override
	public List<String> getDocumentTitlesFromDocumentSet(final DocumentSet docs) {
		List<String> ids = null;

		if (!docs.isEmpty()) {
			ids = new ArrayList<>();
			while (docs.iterator().hasNext()) {
				final Document doc = (Document) docs.iterator().next();
				final String id = doc.getProperties().getStringValue(getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
				ids.add(id);
			}
		} else {
			LOGGER.error("Document set vuoto");
		}
		return ids;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getDocumentTitleByIdProcesso(it.ibm.red.business.enums.TipoContestoProceduraleEnum,
	 *      java.lang.String).
	 */
	@Override
	public String getDocumentTitleByIdProcesso(final TipoContestoProceduraleEnum flusso, final String idProcesso) {
		String out = null;
		final String sql = SELECT + getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY) + FROM + DOCUMENT + " d " + WHERE_D
				+ getPP().getParameterByKey(PropertiesNameEnum.IDENTIFICATORE_PROCESSO_METAKEY) + " = '" + idProcesso + "' AND d."
				+ getPP().getParameterByKey(PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY) + " = '" + flusso.getId() + "'";
		final DocumentSet documents = (DocumentSet) new SearchScope(getOs()).fetchObjects(new SearchSQL(sql), 1, null, false);
		if (!documents.isEmpty()) {
			final Document doc = (Document) documents.iterator().next();
			out = doc.getProperties().getStringValue(getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
		}
		return out;
	}

	/**
	 * Restituisce l'object store.
	 * 
	 * @return object store
	 */
	public ObjectStore getOs() {
		return os;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getDocumentsForVerificaFirma(java.lang.Integer,
	 *      java.lang.String, java.lang.String, java.lang.Integer).
	 */
	@Override
	public DocumentSet getDocumentsForVerificaFirma(final Integer idAoo, final String searchCondition, final String className, final Integer version) {
		String sql = "SELECT * " + "FROM " + className + " d " + "WHERE NOT IsClass(d,"
				+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY) + ") "
				+ (!(className.equals(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FALDONE_CLASSNAME_FN_METAKEY)))
						? " " + "AND NOT IsClass(d, " + PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FALDONE_CLASSNAME_FN_METAKEY) + ")"
						: "")
				+ " ";

		if (idAoo != null && !PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY).equals(className)) {
			sql += " AND d.idAoo = " + idAoo + " ";
		}

		if (version != null) {
			sql += " AND d.majorVersionNumber  = " + version + " ";
		}

		if (searchCondition != null && !"".equals(searchCondition)) {
			sql += AND_PARENTESI_DX_AP + searchCondition + " )";
		}
		final DocumentSet documents = (DocumentSet) new SearchScope(getOs()).fetchObjects(new SearchSQL(sql), 1, null, false);
		if (!documents.isEmpty()) {
			return documents;
		} else {
			LOGGER.info("Nessun risultato trovato");
			return null;
		}
	}

	/**
	 * Restituisce il property provider.
	 * 
	 * @return properties provider
	 */
	public PropertiesProvider getPP() {
		return pp;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getAllegatiFirmatiNonVerificatiNonValidi(java.lang.String).
	 */
	@Override
	public DocumentSet getAllegatiFirmatiNonVerificatiNonValidi(final String guid) {
		final String strDaFirmareSelect = "d." + getPP().getParameterByKey(PropertiesNameEnum.DA_FIRMARE_METAKEY) + ", ";

		String guidWithoutBrachets = guid;
		if (guid.startsWith("{") && guid.endsWith("}")) {
			guidWithoutBrachets = guid.substring(1, guid.length() - 1);
		}

		final String className = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY);
		final String selectFields = strDaFirmareSelect + " d." + PropertyNames.ID + "," + " d." + PropertyNames.IS_RESERVED + "," + " d." + PropertyNames.RESERVATION + ","
				+ " d." + PropertyNames.CONTENT_ELEMENTS + "," + " d." + PropertyNames.MIME_TYPE + "," + " d." + PropertyNames.VERSION_SERIES + "," + " d."
				+ PropertyNames.DATE_CREATED + "," + " d." + PropertyNames.CONTENT_SIZE + "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY)
				+ "," + " d." + getPP().getParameterByKey(PropertiesNameEnum.FIRMA_VISIBILE_METAKEY);

		String stringSql = SELECT + selectFields + FROM + className + " d " + INNER_JOIN_COMPONENT_RELATIONSHIP_R_ON_R_CHILD_COMPONENT_D_THIS + WHERE
				+ " r.ParentComponent = OBJECT('{" + guidWithoutBrachets + "}') ";

		stringSql += AND_D_TABLE + PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FORMATO_ALLEGATO_ID_METAKEY) + "= "
				+ FormatoAllegatoEnum.FIRMATO_DIGITALMENTE.getId();
		stringSql += AND_D + PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_VALIDITA_FIRMA) + " <> " + 1 + " OR " + "d."
				+ PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_VALIDITA_FIRMA) + " <> " + 3 + ")";

		SearchSQL sql = null;
		sql = new SearchSQL(stringSql + ORDER_BY_D + PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY) + " ASC");
		final SearchScope searchScope = new SearchScope(getOs());

		return (DocumentSet) searchScope.fetchObjects(sql, 1, null, false);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper#getMailAsDocument(java.lang.String).
	 */
	@Override
	public Document getMailAsDocument(final String documentTitle) {
		Document doc = null;

		try {
			final StringBuilder queryString = new StringBuilder();
			queryString.append(SELECT_ID);
			queryString.append(FROM + getPP().getParameterByKey(PropertiesNameEnum.MAIL_CLASSNAME_FN_METAKEY));
			queryString.append(WHERE + getPP().getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY) + " = '" + documentTitle + AND_IS_CURRENT_VERSION_TRUE);

			final SearchSQL sql = new SearchSQL(queryString.toString());
			// Search the object store with the sql and get the returned items
			final SearchScope searchScope = new SearchScope(getOs());
			final DocumentSet documents = (DocumentSet) searchScope.fetchObjects(sql, null, null, false);
			final Iterator<?> it = documents.iterator();

			String guid = null;
			if (it.hasNext()) {
				guid = ((Document) it.next()).get_Id().toString();
				doc = getDocumentByGuid(guid);
			}
			// fix inoltroMail end

			if (doc == null) {
				throw new FilenetException(ERROR_DOCUMENTO_ASSENTE_MSG);
			}

			LOGGER.info("Email - Guid: " + doc.get_Id().toString());
			return doc;

		} catch (final Exception e) {
			throw new FilenetException(ERROR_RECUPERO_DOCUMENT_DA_MAIL_MSG, e);
		}
	}
}