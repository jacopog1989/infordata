package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;

import it.ibm.red.business.dto.SequKDTO;

/**
 * 
 * @author CPIERASC
 *
 *	Dao per gestione dei parametri del protocollo.
 */
public interface IParametriProtocolloDAO extends Serializable {

	/**
	 * Metodo per il recupero dei parametri di protocollazione per una specifica aoo.
	 * 
	 * @param idAoo			identificativo aoo
	 * @param connection	connessione al db
	 * @return				parametri di protocollazione
	 */
	SequKDTO getParametriProtocolloByIdAoo(Long idAoo, Connection connection);
}
