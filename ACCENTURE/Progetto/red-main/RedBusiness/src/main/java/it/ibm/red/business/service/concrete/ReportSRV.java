package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IReportDAO;
import it.ibm.red.business.dao.IRuoloDAO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.ParamsRicercaElencoDivisionaleDTO;
import it.ibm.red.business.dto.ReportDetailProtocolliPerCompetenzaDTO;
import it.ibm.red.business.dto.ReportDetailStatisticheUfficioUtenteDTO;
import it.ibm.red.business.dto.ReportDetailUfficioUtenteDTO;
import it.ibm.red.business.dto.ReportMasterStatisticheUfficioUtenteDTO;
import it.ibm.red.business.dto.ReportMasterUfficioUtenteDTO;
import it.ibm.red.business.dto.RisultatoRicercaElencoDivisionaleDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.UtenteRuoloReportDTO;
import it.ibm.red.business.enums.CodaDiLavoroReportEnum;
import it.ibm.red.business.enums.PeriodoReportEnum;
import it.ibm.red.business.enums.TargetNodoReportEnum;
import it.ibm.red.business.enums.TipoOperazioneReportEnum;
import it.ibm.red.business.enums.TipoStatisticaUfficioReportEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.CodaDiLavoroReport;
import it.ibm.red.business.persistence.model.DettaglioReport;
import it.ibm.red.business.persistence.model.Ruolo;
import it.ibm.red.business.persistence.model.TipoOperazioneReport;
import it.ibm.red.business.service.IReportSRV;

/**
 * Servizio per la gestione dei report.
 *
 * @author m.crescentini
 */
@Service
@Component
public class ReportSRV extends AbstractService implements IReportSRV {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 2205974455583440932L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ReportSRV.class.getName());
	
	/**
	 * Dao.
	 */
	@Autowired
	private IReportDAO reportDAO;
	
	/**
	 * Dao.
	 */
	@Autowired
	private IRuoloDAO ruoloDAO;
	
	/**
	 * @see it.ibm.red.business.service.facade.IReportFacadeSRV#getComboPeriodo().
	 */
	@Override
	public List<PeriodoReportEnum> getComboPeriodo() {
		final List<PeriodoReportEnum> list = new ArrayList<>();
		list.addAll(Arrays.asList(PeriodoReportEnum.values()));
		return list;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IReportFacadeSRV#getComboDettaglioReport(long).
	 */
	@Override
	public final List<DettaglioReport> getComboDettaglioReport(long idAoo) {
		List<DettaglioReport> list = null;
		Connection con = null;
		List<TipoOperazioneReport> tipiOperazione = null;
		List<CodaDiLavoroReport> codeLavoro = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			
			list = reportDAO.getAllDettaglioReport(con);
			
			for (final DettaglioReport dettaglioReport : list) {
				// Si recuperano i Tipi Operazione
				tipiOperazione = reportDAO.getTipiOperazioneReportByIdDettaglio(dettaglioReport.getIdDettaglio(), idAoo, con);
				dettaglioReport.setTipiOperazione(tipiOperazione);
				
				// Si recuperano le code di lavoro
				codeLavoro = reportDAO.getCodeDiLavoroReportByIdDettaglio(dettaglioReport.getIdDettaglio(), con);
				dettaglioReport.setCodeDiLavoro(codeLavoro);
			}
		} catch (final SQLException e) {
			LOGGER.error("getComboDettaglioReport -> Si è verificato un errore nella connessione verso il database.", e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
		
		return list;
	}
	
	
	/**
	 * Restituisce la lista dei ruoli da visualizzare nelle checkbox.
	 */
	@Override
	public final List<Ruolo> getAllRuoliReport(final Long idAoo) {
		List<Ruolo> ruoli = null;
		Connection con = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			
			ruoli = ruoloDAO.getListaRuoliAttivati(idAoo, con);
		} catch (final SQLException e) {
			LOGGER.error("getAllRuoliReport -> Si è verificato un errore nella connessione verso il database.", e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
		
		return ruoli;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IReportFacadeSRV#ricercaMasterDocumenti(java.lang.Long,
	 *      java.util.Calendar, java.util.Calendar, java.lang.Long[],
	 *      java.lang.Long[], it.ibm.red.business.enums.TipoOperazioneReportEnum,
	 *      it.ibm.red.business.enums.CodaDiLavoroReportEnum).
	 */
	@Override
	public List<ReportMasterUfficioUtenteDTO> ricercaMasterDocumenti(final Long idAoo, final Calendar dataInizio, final Calendar dataFine, 
			final Long[] uffici, final Long[] utenti,  final TipoOperazioneReportEnum tipoOperazioneEnum, final CodaDiLavoroReportEnum codaDiLavoroEnum) {
		List<ReportMasterUfficioUtenteDTO> risultati = null;
		Connection con = null;
		
		try {
			
			if ((TipoOperazioneReportEnum.UFF_DOC_CODA_LAVORO.equals(tipoOperazioneEnum) && CodaDiLavoroReportEnum.UFF_CHIUSI.equals(codaDiLavoroEnum))
					|| (TipoOperazioneReportEnum.UTE_DOC_IN_CODA_LAVORO.equals(tipoOperazioneEnum) && CodaDiLavoroReportEnum.UTE_ATTI.equals(codaDiLavoroEnum))) {
				con = setupConnection(getDataSource().getConnection(), false);
			} else {
				con = setupConnection(getFilenetDataSource().getConnection(), false);
			}
			
			switch (tipoOperazioneEnum) {
			case UFF_DOC_CODA_LAVORO:
				risultati = getRisultatiCodaLavoroUfficio(idAoo, dataInizio, dataFine, uffici, codaDiLavoroEnum, con);
				break;
			case UTE_DOC_IN_CODA_LAVORO:
				risultati = getRisultatiCodaLavoroUtente(idAoo, dataInizio, dataFine, uffici, utenti, codaDiLavoroEnum, con);
				break;
			case UFF_DOC_USCITA_GENERATI:
			case UTE_DOC_USCITA_GENERATI:
				risultati = reportDAO.countDocUscitaGroupByUfficioUtente(uffici, utenti, dataInizio, dataFine, idAoo, con);
				break;
			case UFF_PROT_ENTRATA_ASSEGNATI:
			case UTE_PROT_ENTRATA_ASSEGNATI:
				risultati = reportDAO.countDocEntrataGroupByUfficioUtente(uffici, utenti, dataInizio, dataFine, idAoo, con);
				break;
			case UTE_DOC_AGLI_ATTI:
				risultati = reportDAO.countDocMessiAgliAttiGroupByUfficioUtente(uffici, utenti, dataInizio, dataFine, idAoo, con);
				break;
			case UTE_DOC_CON_CONTRIBUTO:
				risultati = reportDAO.countDocContributoInseritoGroupByUfficioUtente(uffici, utenti, dataInizio, dataFine, idAoo, con);
				break;
			case UFF_DOC_FIRMATI:	
			case UTE_DOC_FIRMATI:
				risultati = reportDAO.countDocFirmatiGroupByUfficioUtente(uffici, utenti, dataInizio, dataFine, idAoo, con);
				break;
			case UTE_DOC_SIGLATI:
				risultati = reportDAO.countDocSiglatiGroupByUfficioUtente(uffici, utenti, dataInizio, dataFine, idAoo, con);
				break;
			case UTE_DOC_VISTATI:
				risultati = reportDAO.countDocVistatiGroupByUfficioUtente(uffici, utenti, dataInizio, dataFine, idAoo, con);
				break;
			case UFF_PROT_ENTR_GENERATI:
				risultati = reportDAO.countProtEntrataGeneratiByUfficioUtente(uffici, utenti, dataInizio, dataFine, idAoo, con);
				break;
			default:
				break;
			}
			
			if (risultati == null) {
				risultati = new ArrayList<>();
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
		
		return risultati;
	}

	/**
	 * Recupera i risultati della ricerca report sulla coda lavoro identificata da
	 * {@code codaDilavoroEnum} raggruppandoli sugli utenti. per ufficio.
	 * 
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @param dataInizio
	 *            Data inizio ricerca.
	 * @param dataFine
	 *            Data fine ricerca.
	 * @param uffici
	 *            Uffici degli utenti sulla quale raggruppare i risultati.
	 * @param utenti
	 *            Utenti sulla quale raggruppare i risultati.
	 * @param codaDiLavoroEnum
	 *            Coda di lavoro selezionata.
	 * @param con
	 *            Connessione al database.
	 * @return Risultati della ricerca.
	 */
	private List<ReportMasterUfficioUtenteDTO> getRisultatiCodaLavoroUtente(final Long idAoo, final Calendar dataInizio, final Calendar dataFine, 
			final Long[] uffici, final Long[] utenti, final CodaDiLavoroReportEnum codaDiLavoroEnum, Connection con) {
		
		List<ReportMasterUfficioUtenteDTO> risultati = null;
		
		if (CodaDiLavoroReportEnum.UTE_DA_LAVORARE.equals(codaDiLavoroEnum)) {
			risultati = reportDAO.countCodaDaLavorareByUfficioUtente(uffici, utenti, dataInizio, dataFine, idAoo, con);
		} else if (CodaDiLavoroReportEnum.UTE_LIBRO_FIRMA.equals(codaDiLavoroEnum)) {
			risultati = reportDAO.countCodaLibroFirmaByUfficioUtente(uffici, utenti, dataInizio, dataFine, idAoo, con);
		} else if (CodaDiLavoroReportEnum.UTE_IN_SOSPESO.equals(codaDiLavoroEnum)) {
			risultati = reportDAO.countCodaInSospesoByUfficioUtente(uffici, utenti, dataInizio, dataFine, idAoo, con);
		} else if (CodaDiLavoroReportEnum.UTE_ATTI.equals(codaDiLavoroEnum)) {
			risultati = reportDAO.countCodaAttiGroupByUfficioUtente(uffici, utenti, dataInizio, dataFine, con);
		}
		return risultati;
	}

	/**
	 * Recupera i risultati della ricerca report sulla coda di lavoro identificata
	 * da {@code codaDilavoroEnum} raggruppandoli per ufficio.
	 * 
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @param dataInizio
	 *            Data inizio ricerca.
	 * @param dataFine
	 *            Data fine ricerca.
	 * @param uffici
	 *            Uffici sulla quale raggruppare i risultati.
	 * @param codaDiLavoroEnum
	 *            Coda di lavoro selezionata.
	 * @param con
	 *            Connessione al database.
	 * @return Risultati della ricerca.
	 */
	private List<ReportMasterUfficioUtenteDTO> getRisultatiCodaLavoroUfficio(final Long idAoo, final Calendar dataInizio, final Calendar dataFine,
			final Long[] uffici, final CodaDiLavoroReportEnum codaDiLavoroEnum, Connection con) {
		
		List<ReportMasterUfficioUtenteDTO> risultati = null;
		
		if (CodaDiLavoroReportEnum.UFF_CORRIERE.equals(codaDiLavoroEnum)) {
			risultati = reportDAO.countCodaCorriereGroupByUfficio(uffici, dataInizio, dataFine, idAoo, con);
		} else if (CodaDiLavoroReportEnum.UFF_IN_SPEDIZIONE.equals(codaDiLavoroEnum)) {
			risultati = reportDAO.countCodaSpedizioneGroupByUfficio(uffici, dataInizio, dataFine, idAoo, con);
		} else if (CodaDiLavoroReportEnum.UFF_CHIUSI.equals(codaDiLavoroEnum)) {
			risultati = reportDAO.countCodaChiusiGroupByUfficio(uffici, dataInizio, dataFine, con);
		}
		return risultati;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IReportFacadeSRV#ricercaMasterUtentiAttiviConUnDatoRuoloForUff(java.lang.Long[],
	 *      java.lang.Long[]).
	 */
	@Override
	public List<UtenteRuoloReportDTO> ricercaMasterUtentiAttiviConUnDatoRuoloForUff(final Long[] uffici, final Long[] ruoli) {
		List<UtenteRuoloReportDTO> risultati = null;
		Connection con = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			
			risultati = reportDAO.countUtentiAttiviConUnDatoRuolo(uffici, ruoli, con);
			
			if (risultati == null) {
				risultati = new ArrayList<>();
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
		
		return risultati;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IReportFacadeSRV#ricercaDetailDocumenti(java.lang.Long,
	 *      it.ibm.red.business.dto.ReportMasterUfficioUtenteDTO,
	 *      it.ibm.red.business.persistence.model.TipoOperazioneReport,
	 *      it.ibm.red.business.persistence.model.CodaDiLavoroReport).
	 */
	@Override
	public List<ReportDetailUfficioUtenteDTO> ricercaDetailDocumenti(final Long idAoo, final ReportMasterUfficioUtenteDTO master, final TipoOperazioneReport tipoRicerca, 
			final CodaDiLavoroReport codaDiLavoro) {
		List<ReportDetailUfficioUtenteDTO> risultati = null;
		Connection dwhCon = null;
		
		try {
			dwhCon = setupConnection(getFilenetDataSource().getConnection(), false);
			Calendar dataInizio = null;
			Calendar dataFine = null;
			final Long[] uffici = new Long[1];
			Long[] utenti = null;
			
			// Si impostano le date di inizio e fine per la query di dettaglio a partire dal master selezionato
			if (master.getDataDa() != null || master.getDataA() != null) {
				dataInizio = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
				dataFine = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
				dataInizio.setTime(master.getDataDa());
				dataFine.setTime(master.getDataA());
			}
			
			// Si imposta l'ufficio e l'utente per la query di dettaglio a partire dal master selezionato
			uffici[0] = master.getIdNodo();
			
			final Long inputIdUtente = master.getIdUtente();
			if (inputIdUtente != null && inputIdUtente > 0L) {
				utenti = new Long[1];
				utenti[0] = inputIdUtente;
			}
			
			final TipoOperazioneReportEnum tipoOperazioneEnum = TipoOperazioneReportEnum.get(tipoRicerca.getCodice());
			CodaDiLavoroReportEnum codaDiLavoroEnum = null;
			if (codaDiLavoro != null) {
				codaDiLavoroEnum = CodaDiLavoroReportEnum.get(codaDiLavoro.getCodice());
			}

			switch (tipoOperazioneEnum) {
				case UFF_DOC_CODA_LAVORO:
				risultati = getDocumentiCodaLavoroUfficio(idAoo, dwhCon, dataInizio, dataFine, uffici, utenti, codaDiLavoroEnum);
					break;
				case UTE_DOC_IN_CODA_LAVORO:
				risultati = getDocumentiCodaLavoroUtenti(idAoo, dwhCon, dataInizio, dataFine, uffici, utenti, codaDiLavoroEnum);
					break;
				case UFF_DOC_USCITA_GENERATI:
				case UTE_DOC_USCITA_GENERATI:
					risultati = reportDAO.docUscitaGroupByUfficioUtente(uffici, utenti, dataInizio, dataFine, idAoo, dwhCon);
					break;
				case UFF_PROT_ENTRATA_ASSEGNATI:
				case UTE_PROT_ENTRATA_ASSEGNATI:
					risultati = reportDAO.docEntrataGroupByUfficioUtente(uffici, utenti, dataInizio, dataFine, idAoo, dwhCon);
					break;
				case UTE_DOC_AGLI_ATTI:
					risultati = reportDAO.docMessiAgliAttiGroupByUfficioUtente(uffici, utenti, dataInizio, dataFine, idAoo, dwhCon);
					break;
				case UTE_DOC_CON_CONTRIBUTO:
					risultati = reportDAO.docContributoInseritoGroupByUfficioUtente(uffici, utenti, dataInizio, dataFine, idAoo, dwhCon);
					break;
				case UFF_DOC_FIRMATI:	
				case UTE_DOC_FIRMATI:
					risultati = reportDAO.docFirmatiGroupByUfficioUtente(uffici, utenti, dataInizio, dataFine, idAoo, dwhCon);
					break;
				case UTE_DOC_SIGLATI:
					risultati = reportDAO.docSiglatiGroupByUfficioUtente(uffici, utenti, dataInizio, dataFine, idAoo, dwhCon);
					break;
				case UTE_DOC_VISTATI:
					risultati = reportDAO.docVistatiGroupByUfficioUtente(uffici, utenti, dataInizio, dataFine, idAoo, dwhCon);
					break;
				case UFF_PROT_ENTR_GENERATI:
					risultati = reportDAO.docProtGeneratiByUfficio(uffici, utenti, dataInizio, dataFine, idAoo, dwhCon);
					break;
				default:
					break;
			}
			
			if (risultati == null) {
				risultati = new ArrayList<>();
			}
		} catch (final Exception e) {
			LOGGER.error("ricercaDetail -> Si è verificato un errore durante la ricerca di tipo: " + tipoRicerca.getCodice(), e);
			throw new RedException(e);
		} finally {
			closeConnection(dwhCon);
		}
		
		return risultati;
	}

	/**
	 * Recupera i documenti associati ai report sulla coda lavoro identificata dalla
	 * {@code codaDilavoroEnum} raggruppando i risultati per utenti.
	 * 
	 * @param idAoo
	 *            Identificativo dell'Area organizzativa.
	 * @param dwhCon
	 *            Connessione al dwh.
	 * @param dataInizio
	 *            Data inizio ricerca.
	 * @param dataFine
	 *            Data fine ricerca.
	 * @param uffici
	 *            Uffici degli utenti sulla quale raggruppare i risultati.
	 * @param utenti
	 *            Utenti sulla quale raggruppare i risultati.
	 * @param codaDiLavoroEnum
	 *            Coda di lavoro sulla quale ricercare.
	 * @return Esito della ricerca.
	 */
	private List<ReportDetailUfficioUtenteDTO> getDocumentiCodaLavoroUtenti(final Long idAoo, Connection dwhCon, Calendar dataInizio, Calendar dataFine,
			final Long[] uffici, Long[] utenti, CodaDiLavoroReportEnum codaDiLavoroEnum) {
		
		List<ReportDetailUfficioUtenteDTO> risultati = null;
		
		if (CodaDiLavoroReportEnum.UTE_DA_LAVORARE.equals(codaDiLavoroEnum)) {
			risultati = reportDAO.docInCodaDaLavorareGroupByUfficioUtente(uffici, utenti, dataInizio, dataFine, idAoo, dwhCon);
		} else if (CodaDiLavoroReportEnum.UTE_LIBRO_FIRMA.equals(codaDiLavoroEnum)) {
			risultati = reportDAO.docInCodaLibroFirmaGroupByUfficioUtente(uffici, utenti, dataInizio, dataFine, idAoo, dwhCon);
		} else if (CodaDiLavoroReportEnum.UTE_IN_SOSPESO.equals(codaDiLavoroEnum)) {
			risultati = reportDAO.docInCodaInSospesoGroupByUfficioUtente(uffici, utenti, dataInizio, dataFine, idAoo, dwhCon);
		} else if (CodaDiLavoroReportEnum.UTE_ATTI.equals(codaDiLavoroEnum)) {
			risultati = ricercaDetailDocumentiInCodaChiusiOAtti(uffici, utenti, dataInizio, dataFine, codaDiLavoroEnum, idAoo, dwhCon);
		}
		return risultati;
	}

	/**
	 * Recupera i documenti associati ai report sulla coda lavoro identificata dalla
	 * {@code codaDilavoroEnum} raggruppando i risultati per uffici.
	 * 
	 * @param idAoo
	 *            Identificativo dell'Area organizzativa.
	 * @param dwhCon
	 *            Connessione al dwh.
	 * @param dataInizio
	 *            Data inizio ricerca.
	 * @param dataFine
	 *            Data fine ricerca.
	 * @param uffici
	 *            Uffici sulla quale raggruppare i risultati.
	 * @param utenti
	 *            Utenti per la ricerca.
	 * @param codaDiLavoroEnum
	 *            Coda di lavoro sulla quale ricercare.
	 * @return Esito della ricerca.
	 */
	private List<ReportDetailUfficioUtenteDTO> getDocumentiCodaLavoroUfficio(final Long idAoo, Connection dwhCon, Calendar dataInizio, Calendar dataFine,
			final Long[] uffici, Long[] utenti, CodaDiLavoroReportEnum codaDiLavoroEnum) {
		
		List<ReportDetailUfficioUtenteDTO> risultati = null;
		
		if (CodaDiLavoroReportEnum.UFF_CORRIERE.equals(codaDiLavoroEnum)) {
			risultati = reportDAO.docInCodaCorriereUfficio(uffici, dataInizio, dataFine, idAoo, dwhCon);	
		} else if (CodaDiLavoroReportEnum.UFF_IN_SPEDIZIONE.equals(codaDiLavoroEnum)) {
			risultati = reportDAO.docInCodaSpedizioneUfficio(uffici, dataInizio, dataFine, idAoo, dwhCon);	
		} else if (CodaDiLavoroReportEnum.UFF_CHIUSI.equals(codaDiLavoroEnum)) {
			risultati = ricercaDetailDocumentiInCodaChiusiOAtti(uffici, utenti, dataInizio, dataFine, codaDiLavoroEnum, idAoo, dwhCon);
		}
		return risultati;
	}
	
	private List<ReportDetailUfficioUtenteDTO> ricercaDetailDocumentiInCodaChiusiOAtti(final Long[] uffici, final Long[] utenti, final Calendar dataInizio, final Calendar dataFine, 
			final CodaDiLavoroReportEnum codaDiLavoro, final Long idAoo, final Connection dwhCon) {
		List<ReportDetailUfficioUtenteDTO> risultati = null;
		
		// 1) Si recuperano info documenti (id e descrizione ufficio) dalla vista RED
		List<ReportDetailUfficioUtenteDTO> idDocList = null;
		Connection con = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			
			if (CodaDiLavoroReportEnum.UFF_CHIUSI.equals(codaDiLavoro)) {
				idDocList = reportDAO.idDocInCodaChiusiByUfficio(uffici, dataInizio, dataFine, con);
			} else if (CodaDiLavoroReportEnum.UTE_ATTI.equals(codaDiLavoro)) {
				idDocList = reportDAO.idDocInCodaAttiByUfficioUtente(uffici, utenti, dataInizio, dataFine, con);
			}
		} catch (final SQLException e) {
			LOGGER.error("ricercaDetailDocumentiInCodaChiusiOAtti -> Si è verificato un errore nella connessione verso il database.", e);
		} finally {
			closeConnection(con);
		}
		
		if (idDocList != null && !idDocList.isEmpty()) {
			final String[] idDocumenti = new String[idDocList.size()];
			for (int index = 0; index < idDocList.size(); index++) {
				idDocumenti[index] = idDocList.get(index).getIdDocumento();
			}
			
			// 2) Si recuperano dettagli documenti dalla vista DWH
			Map<String, ReportDetailUfficioUtenteDTO> mapReportDetailUfficioUtente = null;
			
			if (CodaDiLavoroReportEnum.UFF_CHIUSI.equals(codaDiLavoro)) {
				mapReportDetailUfficioUtente = reportDAO.docInCodaChiusiByUfficio(idDocumenti, dataInizio, dataFine, idAoo, dwhCon);
			} else if (CodaDiLavoroReportEnum.UTE_ATTI.equals(codaDiLavoro)) {
				mapReportDetailUfficioUtente = reportDAO.docInCodaAttiByUfficioUtente(idDocumenti, dataInizio, dataFine, idAoo, dwhCon);				
			}
			
			// 3) Per ogni documento estratto dalla vista RED
			if (mapReportDetailUfficioUtente != null) {
				risultati = new ArrayList<>();
				for (final ReportDetailUfficioUtenteDTO reportDetail : idDocList) {
					// Si recuperano i dati di dettaglio estratti dalla vista DWH associati al documento estratto dalla vista RED
					final ReportDetailUfficioUtenteDTO reportDetailFilled = mapReportDetailUfficioUtente.get(reportDetail.getIdDocumento());
					reportDetailFilled.setDescrizione(reportDetail.getDescrizione());
					
					risultati.add(reportDetailFilled);
				}
			}
		}
		
		return risultati;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IReportFacadeSRV#ricercaDetailUtentiAttiviConUnDatoRuoloForUff(it.ibm.red.business.dto.UtenteRuoloReportDTO).
	 */
	@Override
	public List<UtenteRuoloReportDTO> ricercaDetailUtentiAttiviConUnDatoRuoloForUff(final UtenteRuoloReportDTO master) {
		List<UtenteRuoloReportDTO> risultati = null;
		Connection con = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			
			final Long[] uffici = new Long[1];
			uffici[0] = master.getIdUfficio();
			
			final Long[] ruoli = new Long[1];
			ruoli[0] = master.getIdRuolo();
			
			risultati = reportDAO.detailUtentiAttiviConUnDatoRuolo(uffici, ruoli, con);
			
			if (risultati == null) {
				risultati = new ArrayList<>();
			}
		} catch (final Exception e) {
			LOGGER.error("ricercaDetailUtentiAttiviConUnDatoRuoloForUff -> Si è verificato un errore durante la ricerca.", e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
		
		return risultati;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IReportFacadeSRV#getStatisticheUfficiMaster(it.ibm.red.business.dto.ReportMasterStatisticheUfficioUtenteDTO).
	 */
	@Override
	public List<ReportMasterStatisticheUfficioUtenteDTO> getStatisticheUfficiMaster(final ReportMasterStatisticheUfficioUtenteDTO form) {
		
		LOGGER.info("getStatisticheUfficiMaster " + form);
		final List<ReportMasterStatisticheUfficioUtenteDTO> risultati = new ArrayList<>();
		Connection con = null;
		
		try {
			
			con = setupConnection(getFilenetDataSource().getConnection(), false);
			
			//ingresso
			Integer numeroDocumentiInLavorazioneIngresso = 0; 
			Integer numeroDocumentiInSospesoIngresso = 0; 
			Integer numeroDocumentiInScadenzaIngresso = 0; 
			Integer numeroDocumentiScadutiIngresso = 0; 
			Integer numeroDocumentiAttiIngresso = 0; 
			Integer numeroDocumentiRispostaIngresso = 0; 
			Integer numeroDocumentiIngresso = 0;
			//uscita
			Integer numeroDocumentiInLavorazioneUscita = 0; 
			Integer numeroDocumentiSpeditiUscita = 0; 
			Integer numeroDocumentiUscita = 0;
			
			ReportMasterStatisticheUfficioUtenteDTO master = null;
			NodoOrganigrammaDTO struttura = null;
			UfficioDTO ufficio = null;
			UtenteDTO utente = null;
			for (int i=0; i < form.getStrutture().size(); i++) {
				
				struttura = form.getStrutture().get(i);
				
				final ReportDetailStatisticheUfficioUtenteDTO formDettaglio = 
						new ReportDetailStatisticheUfficioUtenteDTO(struttura, form.getTipoStruttura(), form.getAnno(), form.getDataDa(), form.getDataA(), null);
				
				ufficio = new UfficioDTO(struttura.getIdNodo(), struttura.getDescrizioneNodo());
				if (TargetNodoReportEnum.UTENTE.equals(form.getTipoStruttura())) {
					utente = new UtenteDTO();
					utente.setId(struttura.getIdUtente());
					utente.setCognome(struttura.getCognomeUtente());
					utente.setNome(struttura.getNomeUtente());
				}
				
				formDettaglio.setTipoEstrazione(TipoStatisticaUfficioReportEnum.INGRESSO_IN_LAVORAZIONE);
				numeroDocumentiInLavorazioneIngresso = (getStatisticheUfficiDetail(formDettaglio, con)).size();
				LOGGER.info("Numero documenti in lavorazione ingresso " + numeroDocumentiInLavorazioneIngresso);
				
				formDettaglio.setTipoEstrazione(TipoStatisticaUfficioReportEnum.INGRESSO_IN_SOSPESO);
				numeroDocumentiInSospesoIngresso = (getStatisticheUfficiDetail(formDettaglio, con)).size();
				LOGGER.info("Numero documenti in sospeso ingresso " + numeroDocumentiInSospesoIngresso);
				
				formDettaglio.setTipoEstrazione(TipoStatisticaUfficioReportEnum.INGRESSO_IN_SCADENZA);
				numeroDocumentiInScadenzaIngresso = (getStatisticheUfficiDetail(formDettaglio, con)).size();
				LOGGER.info("Numero documenti in scadenza ingresso " + numeroDocumentiInScadenzaIngresso);
				
				formDettaglio.setTipoEstrazione(TipoStatisticaUfficioReportEnum.INGRESSO_SCADUTI);
				numeroDocumentiScadutiIngresso = (getStatisticheUfficiDetail(formDettaglio, con)).size();
				LOGGER.info("Numero documenti scaduti ingresso " + numeroDocumentiScadutiIngresso);
				
				formDettaglio.setTipoEstrazione(TipoStatisticaUfficioReportEnum.INGRESSO_ATTI);
				numeroDocumentiAttiIngresso = (getStatisticheUfficiDetail(formDettaglio, con)).size();
				LOGGER.info("Numero documenti agli atti ingresso " + numeroDocumentiAttiIngresso);
				
				formDettaglio.setTipoEstrazione(TipoStatisticaUfficioReportEnum.INGRESSO_RISPOSTA);
				numeroDocumentiRispostaIngresso = (getStatisticheUfficiDetail(formDettaglio, con)).size();
				LOGGER.info("Numero documenti in risposta ingresso " + numeroDocumentiRispostaIngresso);
				
				numeroDocumentiIngresso = numeroDocumentiInLavorazioneIngresso + numeroDocumentiInSospesoIngresso + numeroDocumentiAttiIngresso + numeroDocumentiRispostaIngresso;
				LOGGER.info("Numero documenti ingresso " + numeroDocumentiIngresso);
				
				formDettaglio.setTipoEstrazione(TipoStatisticaUfficioReportEnum.USCITA_IN_LAVORAZIONE);
				numeroDocumentiInLavorazioneUscita = (getStatisticheUfficiDetail(formDettaglio, con)).size();
				LOGGER.info("Numero documenti in lavorazione uscita " + numeroDocumentiInLavorazioneUscita);
				
				formDettaglio.setTipoEstrazione(TipoStatisticaUfficioReportEnum.USCITA_SPEDITI);
				numeroDocumentiSpeditiUscita = (getStatisticheUfficiDetail(formDettaglio, con)).size();
				LOGGER.info("Numero documenti spediti uscita " + numeroDocumentiSpeditiUscita);
				
				numeroDocumentiUscita = numeroDocumentiInLavorazioneUscita + numeroDocumentiSpeditiUscita;
				LOGGER.info("Numero documenti uscita " + numeroDocumentiUscita);
				
				master = new ReportMasterStatisticheUfficioUtenteDTO(form.getStrutture(), form.getTipoStruttura(), form.getAnno(), form.getDataDa(), form.getDataA());
				master.setUfficio(ufficio);
				master.setUtente(utente);
				master.setStruttura(struttura);
				master.setNumeroDocumentiAttiIngresso(numeroDocumentiAttiIngresso);
				master.setNumeroDocumentiIngresso(numeroDocumentiIngresso);
				master.setNumeroDocumentiInLavorazioneIngresso(numeroDocumentiInLavorazioneIngresso);
				master.setNumeroDocumentiInLavorazioneUscita(numeroDocumentiInLavorazioneUscita);
				master.setNumeroDocumentiInScadenzaIngresso(numeroDocumentiInScadenzaIngresso);
				master.setNumeroDocumentiInSospesoIngresso(numeroDocumentiInSospesoIngresso);
				master.setNumeroDocumentiRispostaIngresso(numeroDocumentiRispostaIngresso);
				master.setNumeroDocumentiScadutiIngresso(numeroDocumentiScadutiIngresso);
				master.setNumeroDocumentiSpeditiUscita(numeroDocumentiSpeditiUscita);
				master.setNumeroDocumentiUscita(numeroDocumentiUscita);
				
				risultati.add(master);
				
			}
			
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
		
		return risultati;
	}
	
	private List<ReportDetailStatisticheUfficioUtenteDTO> getStatisticheUfficiDetail(final ReportDetailStatisticheUfficioUtenteDTO form, final Connection con) {
		
		List<ReportDetailStatisticheUfficioUtenteDTO> risultati = null;
		
		try {
			
			if (TipoStatisticaUfficioReportEnum.INGRESSO_TUTTI.equals(form.getTipoEstrazione())) {
				
				form.setTipoEstrazione(TipoStatisticaUfficioReportEnum.INGRESSO_TUTTI_APERTI);
				final List<ReportDetailStatisticheUfficioUtenteDTO> risultati1 = reportDAO.detailStatisticheUfficioUtente(form, con);
				
				form.setTipoEstrazione(TipoStatisticaUfficioReportEnum.INGRESSO_TUTTI_CHIUSI);
				final List<ReportDetailStatisticheUfficioUtenteDTO> risultati2 = reportDAO.detailStatisticheUfficioUtente(form, con);
				
				risultati = new ArrayList<>();
				
				if (risultati1 != null) {
					risultati.addAll(risultati1);
				}
				
				if (risultati2 != null) {
					risultati.addAll(risultati2);
				}
				
				form.setTipoEstrazione(TipoStatisticaUfficioReportEnum.INGRESSO_TUTTI);
				
			} else if (TipoStatisticaUfficioReportEnum.USCITA_TUTTI.equals(form.getTipoEstrazione())) {
				
				form.setTipoEstrazione(TipoStatisticaUfficioReportEnum.USCITA_IN_LAVORAZIONE);
				final List<ReportDetailStatisticheUfficioUtenteDTO> risultati1 = reportDAO.detailStatisticheUfficioUtente(form, con);
				
				form.setTipoEstrazione(TipoStatisticaUfficioReportEnum.USCITA_SPEDITI);
				final List<ReportDetailStatisticheUfficioUtenteDTO> risultati2 = reportDAO.detailStatisticheUfficioUtente(form, con);
				
				risultati = new ArrayList<>();
				
				if (risultati1 != null) {
					risultati.addAll(risultati1);
				}
				
				if (risultati2 != null) {
					risultati.addAll(risultati2);
				}
				
				form.setTipoEstrazione(TipoStatisticaUfficioReportEnum.USCITA_TUTTI);
				
			} else {
			
				risultati = reportDAO.detailStatisticheUfficioUtente(form, con);
			
			}
						
			
			if (risultati == null) {
				risultati = new ArrayList<>();
			}
			
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} 
		
		return risultati;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IReportFacadeSRV#getStatisticheUfficiDetail(it.ibm.red.business.dto.ReportDetailStatisticheUfficioUtenteDTO).
	 */
	@Override
	public List<ReportDetailStatisticheUfficioUtenteDTO> getStatisticheUfficiDetail(final ReportDetailStatisticheUfficioUtenteDTO form) {
		
		LOGGER.info("getStatisticheUfficiDetail " + form);
		List<ReportDetailStatisticheUfficioUtenteDTO> risultati = new ArrayList<>();
		Connection con = null;
		
		try {
			
			con = setupConnection(getFilenetDataSource().getConnection(), false);
			
			risultati = getStatisticheUfficiDetail(form, con);
			
			Collections.sort(risultati, Collections.reverseOrder());
			
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
		
		return risultati;
		
	}
	
	
	/**
	 * Estrae protocolli in entrata assegnati per competenza all'ufficio/utente
	 * (pervenuti, inevasi e lavorati).
	 * 
	 * @param idAoo
	 * @param dataInizio
	 * @param dataFine
	 * @param uffici
	 * @param utenti
	 * @param tipoRicerca
	 * @return lista dettagli report
	 */
	@Override
	public List<ReportDetailProtocolliPerCompetenzaDTO> ricercaDetailProtocolliPerCompetenza(final Long idAoo, final Calendar dataInizio, final Calendar dataFine, 
			final Long[] uffici, final Long[] utenti,  final TipoOperazioneReport tipoRicerca) {
		List<ReportDetailProtocolliPerCompetenzaDTO> risultati = null;
		Connection dwhCon = null;
		
		try {
			dwhCon = setupConnection(getFilenetDataSource().getConnection(), false);
			
			final TipoOperazioneReportEnum tipoOperazioneEnum = TipoOperazioneReportEnum.get(tipoRicerca.getCodice());
			
			risultati = reportDAO.getProtocolliPerCompetenza(uffici, utenti, dataInizio, dataFine, idAoo, tipoOperazioneEnum, dwhCon);
			
			if (risultati == null) {
				risultati = new ArrayList<>();
			}
		} catch (final Exception e) {
			LOGGER.error("ricercaDetailProtocolliPerCompetenza -> Si è verificato un errore durante la ricerca di tipo: " + tipoRicerca.getCodice(), e);
			throw new RedException(e);
		} finally {
			closeConnection(dwhCon);
		}
		
		return risultati;
	}
	
	/**
	 * Estrae i documenti assegnati (per competenza o conoscenza) alla data
	 * assegnazione, dall’ufficio dell’utente in sessione all’ufficio/utenti
	 * selezionati, coerentemente ai filtri di ricerca eventualmente valorizzati.
	 * 
	 * @param idAoo
	 * @param parametriElencoDivisionale
	 * @param uffici
	 * @param utenti
	 * @param tipoRicerca
	 * @param tipoAssegnazione
	 * @param numeroProtocolloDa
	 * @param idTipologiaDocumento
	 * @return lista dettagli report
	 */
	@Override
	public List<RisultatoRicercaElencoDivisionaleDTO> ricercaDetailElencoDivisionale(final Long idAoo, final ParamsRicercaElencoDivisionaleDTO parametriElencoDivisionale, 
			final Long[] uffici, final Long[] utenti,  final TipoOperazioneReport tipoRicerca, final String tipoAssegnazione, final Integer numeroProtocolloDa, final Integer idTipologiaDocumento) {
		List<RisultatoRicercaElencoDivisionaleDTO> risultati = null;
		Connection dwhCon = null;
		
		try {
			dwhCon = setupConnection(getFilenetDataSource().getConnection(), false);
			
			risultati = reportDAO.getElencoDivisionale(idAoo, parametriElencoDivisionale, uffici, utenti, tipoAssegnazione, numeroProtocolloDa, idTipologiaDocumento, dwhCon);
			
			if (risultati == null) {
				risultati = new ArrayList<>();
			}
		} catch (final Exception e) {
			LOGGER.error("ricercaDetailElencoDivisionale -> Si è verificato un errore durante la ricerca di tipo: " + tipoRicerca.getCodice(), e);
			throw new RedException(e);
		} finally {
			closeConnection(dwhCon);
		}
		
		return risultati;
	}
	
}