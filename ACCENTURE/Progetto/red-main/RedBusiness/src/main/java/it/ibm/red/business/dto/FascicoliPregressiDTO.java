package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author VINGENITO
 * 
 */
public class FascicoliPregressiDTO extends AbstractDTO implements Serializable {

	private static final long serialVersionUID = 9163393525855026227L;

	/**
	 * Identificativo fascicolo.
	 */
	private String idFascicolo;

	/**
	 * Respnsabile.
	 */
	private String responsabileFascicolo;

	/**
	 * Codice titolario.
	 */
	private String codiceTitolarioFascicolo;

	/**
	 * Descrizione titolario.
	 */
	private String descrizioneTitolarioFascicolo;

	/**
	 * Ufficio proprietario fascicolo.
	 */
	private String codUffProprietarioFascicolo;

	/**
	 * Descrizione ufficio proprietario fascicolo.
	 */
	private String descrizioneUffProprietarioFascicolo;

	/**
	 * Descrizione fascicolo.
	 */
	private String descrizioneFascicolo;

	/**
	 * Stato fascicolo.
	 */
	private String statoFascicolo;

	/**
	 * Numero fascicolo.
	 */
	private Integer numFascicolo;

	/**
	 * Lista documenti.
	 */
	private List<DocumentiFascicoloNsdDTO> documentiDTO;

	/**
	 * Nome della cartella.
	 */
	private String nomeCartella;

	/**
	 * Flag visibile.
	 */
	private boolean visible;

	/**
	 * @return the idFascicolo
	 */
	public String getIdFascicolo() {
		return idFascicolo;
	}

	/**
	 * @param idFascicolo the idFascicolo to set
	 */
	public void setIdFascicolo(final String idFascicolo) {
		this.idFascicolo = idFascicolo;
	}

	/**
	 * @return the responsabileFascicolo
	 */
	public String getResponsabileFascicolo() {
		return responsabileFascicolo;
	}

	/**
	 * @param responsabileFascicolo the responsabileFascicolo to set
	 */
	public void setResponsabileFascicolo(final String responsabileFascicolo) {
		this.responsabileFascicolo = responsabileFascicolo;
	}

	/**
	 * @return the codiceTitolarioFascicolo
	 */
	public String getCodiceTitolarioFascicolo() {
		return codiceTitolarioFascicolo;
	}

	/**
	 * @param codiceTitolarioFascicolo the codiceTitolarioFascicolo to set
	 */
	public void setCodiceTitolarioFascicolo(final String codiceTitolarioFascicolo) {
		this.codiceTitolarioFascicolo = codiceTitolarioFascicolo;
	}

	/**
	 * @return the descrizioneTitolarioFascicolo
	 */
	public String getDescrizioneTitolarioFascicolo() {
		return descrizioneTitolarioFascicolo;
	}

	/**
	 * @param descrizioneTitolarioFascicolo the descrizioneTitolarioFascicolo to set
	 */
	public void setDescrizioneTitolarioFascicolo(final String descrizioneTitolarioFascicolo) {
		this.descrizioneTitolarioFascicolo = descrizioneTitolarioFascicolo;
	}

	/**
	 * @return the codUffProprietarioFascicolo
	 */
	public String getCodUffProprietarioFascicolo() {
		return codUffProprietarioFascicolo;
	}

	/**
	 * @param codUffProprietarioFascicolo the codUffProprietarioFascicolo to set
	 */
	public void setCodUffProprietarioFascicolo(final String codUffProprietarioFascicolo) {
		this.codUffProprietarioFascicolo = codUffProprietarioFascicolo;
	}

	/**
	 * @return the descrizioneUffProprietarioFascicolo
	 */
	public String getDescrizioneUffProprietarioFascicolo() {
		return descrizioneUffProprietarioFascicolo;
	}

	/**
	 * @param descrizioneUffProprietarioFascicolo the
	 *                                            descrizioneUffProprietarioFascicolo
	 *                                            to set
	 */
	public void setDescrizioneUffProprietarioFascicolo(final String descrizioneUffProprietarioFascicolo) {
		this.descrizioneUffProprietarioFascicolo = descrizioneUffProprietarioFascicolo;
	}

	/**
	 * @return the descrizioneFascicolo
	 */
	public String getDescrizioneFascicolo() {
		return descrizioneFascicolo;
	}

	/**
	 * @param descrizioneFascicolo the descrizioneFascicolo to set
	 */
	public void setDescrizioneFascicolo(final String descrizioneFascicolo) {
		this.descrizioneFascicolo = descrizioneFascicolo;
	}

	/**
	 * @return the statoFascicolo
	 */
	public String getStatoFascicolo() {
		return statoFascicolo;
	}

	/**
	 * @param statoFascicolo the statoFascicolo to set
	 */
	public void setStatoFascicolo(final String statoFascicolo) {
		this.statoFascicolo = statoFascicolo;
	}

	/**
	 * @return the documentiDTO
	 */
	public List<DocumentiFascicoloNsdDTO> getDocumentiDTO() {
		return documentiDTO;
	}

	/**
	 * @param documentiDTO the documentiDTO to set
	 */
	public void setDocumentiDTO(final List<DocumentiFascicoloNsdDTO> documentiDTO) {
		this.documentiDTO = documentiDTO;
	}

	/**
	 * @return the numFascicolo
	 */
	public Integer getNumFascicolo() {
		return numFascicolo;
	}

	/**
	 * @param numFascicolo the numFascicolo to set
	 */
	public void setNumFascicolo(final Integer numFascicolo) {
		this.numFascicolo = numFascicolo;
	}

	/**
	 * Restituisce il nome della cartella.
	 * 
	 * @return nome cartella
	 */
	public String getNomeCartella() {
		return nomeCartella;
	}

	/**
	 * Imposta il nome della cartella.
	 * 
	 * @param nomeCartella
	 */
	public void setNomeCartella(final String nomeCartella) {
		this.nomeCartella = nomeCartella;
	}

	/**
	 * Restituisce true se visibile, false altrimenti.
	 * 
	 * @return visibilita
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * Imposta la visibilita.
	 * 
	 * @param visible
	 */
	public void setVisible(final boolean visible) {
		this.visible = visible;
	}

}
