package it.ibm.red.business.helper.filenet.common;

import java.util.Comparator;
import java.util.Date;

import it.ibm.red.business.dto.NotificheUtenteDTO;

/**
 * @author VINGENITO
 * Comparatore Notifiche.
 */
public class ComparatorNotifiche implements Comparator<NotificheUtenteDTO> {

	/**
	 * Esegue la comparazione delle notifiche.
	 * @param n1
	 * @param n2
	 * @return int
	 */
	@Override
	public int compare(final NotificheUtenteDTO n1, final NotificheUtenteDTO n2) {
		Integer output = 0;
		if (n1.getNotificheCalendario() != null) {
			final Date data1 = n1.getNotificheCalendario().getDataScadenza();
			output = compareTwoDate(n2, data1);
		} else if (n1.getNotificheRubrica() != null) {
			final Date data1 = n1.getNotificheRubrica().getDataOperazione();
			output = compareTwoDate(n2, data1);
			
		} else if (n1.getNotificheSottoscrizioni() != null) {
			final Date data1 = n1.getNotificheSottoscrizioni().getDataEvento();
			output = compareTwoDate(n2, data1);
		}
		 
		return output;
	}

	/**
	 * Confronta le due date delle notifiche.
	 * @param n2
	 * @param data1
	 * @return return del compareTo
	 */
	private static Integer compareTwoDate(final NotificheUtenteDTO n2, final Date data1) {
		Integer outputDate2 = 0;
		if (n2.getNotificheCalendario() != null) {
			final Date data2 = n2.getNotificheCalendario().getDataScadenza();
			outputDate2 =  data2.compareTo(data1);
		} else if (n2.getNotificheSottoscrizioni() != null) {
			final Date data2 = n2.getNotificheSottoscrizioni().getDataEvento();
			outputDate2 = data2.compareTo(data1);
		} else if (n2.getNotificheRubrica() != null) {
			final Date data2 = n2.getNotificheRubrica().getDataOperazione();
			outputDate2 = data2.compareTo(data1);
		}
		return outputDate2;
	}
 
}
