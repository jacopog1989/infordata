package it.ibm.red.business.enums;

/**
 * Enum che definisce gli esiti faldoni.
 */
public enum EsitoFaldoneEnum {

    /**
	 * Valore.
	 */
	FALDONE_NON_DISASSOCIABILE("Attenzione, non è possibile rimuovere il faldone, prima bisogna faldonare il fascicolo in un altro faldone"),

    /**
	 * Valore.
	 */
	FALDONE_DISASSOCIATO_DA_FASCICOLO_CON_SUCCESSO("Il faldone è stato disassociato con successo"),

    /**
	 * Valore.
	 */
	ERRORE_GENERICO("Si è verificato un errore durante l'operazione");
	
    /**
	 * Testo.
	 */
	private String text;
	
	EsitoFaldoneEnum(final String text) {
		this.text = text;
	}

	/**
	 * Recupera il testo.
	 * @return testo
	 */
	public String getText() {
		return text;
	}
	
	
}
