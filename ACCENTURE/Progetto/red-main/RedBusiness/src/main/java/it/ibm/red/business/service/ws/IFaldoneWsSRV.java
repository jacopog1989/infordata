package it.ibm.red.business.service.ws;

import java.util.List;

import com.filenet.api.collection.DocumentSet;

import it.ibm.red.business.dto.FaldoneWsDTO;
import it.ibm.red.business.service.ws.facade.IFaldoneWsFacadeSRV;

/**
 * The Interface IFaldoneWsSRV.
 *
 * @author a.dilegge
 * 
 *         Interfaccia servizio gestione faldone web service.
 */
public interface IFaldoneWsSRV extends IFaldoneWsFacadeSRV {

	
	/**
	 * @param faldoniRicercaFilenet
	 * @return
	 */
	List<FaldoneWsDTO> transformToFaldoniWs(DocumentSet faldoniRicercaFilenet);
	
}