/**
 * 
 */
package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.Set;

import it.ibm.red.business.dto.DatiCodaApplicativaDTO;
import it.ibm.red.business.dto.UtenteDTO; 

/**
 * @author APerquoti
 *
 */
public interface ICodeApplicativeDAO extends Serializable {

	/**
	 * 
	 * @param idNodoUtente
	 * @param idUtente
	 * @param idsStatoLavorazione
	 * @return
	 */
	Set<String> getCodeAppl(Long idNodoUtente, Long idUtente, Collection<Long> idsStatoLavorazione, Integer numElements, Connection connection);

	/**
	 * 
	 * @param idNodoUtente
	 * @param idUtente
	 * @param idsStatoLavorazione
	 * @return
	 */
	Integer countCodeAppl(Long idNodoUtente, Long idUtente, Collection<Long> idsStatoLavorazione, Connection connection);
	
	/**
	 * 
	 * @param documentTitle
	 * @return
	 */
	Collection<DatiCodaApplicativaDTO> getQueueName(String documentTitle, Long idUtente, Long idNodo, Connection connection);
	
	/**
	 * Archivia i documenti nelle code applicative di ufficio e utente.
	 * 
	 * @param connection
	 */
	void archiviaCodeApplicative(Connection connection);
	 
	/**
	 * Controlla se il document title sia presente nella tabella DOCUMENTOUTENTESTATO.
	 * 
	 * @param documentTitle
	 * @param utente
	 * @param idStatoLavorazione
	 * @param isAzioniMassive
	 * @param connection
	 * @return
	 */
	boolean checkDocumentTitleRecall(String documentTitle, UtenteDTO utente, Long idStatoLavorazione, boolean isAzioniMassive, Connection connection);
	
	/**
	 * Aggiorna gli item recall nella DOCUMENTOUTENTESTATO in lavorate per il documentTitle in input.
	 * 
	 * @param documentTitle
	 * @param connection
	 */
	void fromRecallToLavorate(String documentTitle, Connection connection);

	/**
	 * Elimina gli item recall dalla DOCUMENTOUTENTESTATO per il documento in input
	 * 
	 * @param idDocumento
	 * @param connection
	 */
	void deleteRecall(Integer idDocumento, Connection connection);
	
	/**
	 * Elimina gli item DOCUMENTOUTENTESTATO per il documento in input
	 * 
	 * @param idDocumento
	 * @param connection
	 */
	void deleteAll(Integer idDocumento, Connection connection);

}