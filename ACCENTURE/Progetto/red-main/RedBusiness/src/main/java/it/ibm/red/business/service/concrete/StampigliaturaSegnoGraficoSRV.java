package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IIterApprovativoDAO;
import it.ibm.red.business.dao.IStampigliaturaSegnoGraficoDAO;
import it.ibm.red.business.dto.IconaStampigliaturaSegnoGraficoDTO;
import it.ibm.red.business.dto.StampigliaturaSegnoGraficoDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.IStampigliaturaSegnoGraficoSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * 
 * @author Vingenito
 *
 */
@Service
public class StampigliaturaSegnoGraficoSRV extends AbstractService implements IStampigliaturaSegnoGraficoSRV {

	private static final String ERRORE_DURANTE_IL_RECUPERO_DEL_SIGLATARIO = "Errore durante il recupero del siglatario: ";

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 1193566021289853898L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(StampigliaturaSegnoGraficoSRV.class.getName());

	/**
	 * DAO.
	 */
	@Autowired
	private IStampigliaturaSegnoGraficoDAO stampigliaturaSegnoGraficoDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IIterApprovativoDAO iterApprovativoDAO;

	/**
	 * @see it.ibm.red.business.service.facade.IStampigliaturaSegnoGraficoFacadeSRV#getIconByPlaceholder(java.lang.String,
	 *      java.lang.String).
	 */
	@Override
	public HashMap<String, String> getIconByPlaceholder(final String placeholder, final String numDocumento) {
		Connection connection = null;
		List<IconaStampigliaturaSegnoGraficoDTO> listIcone = null;
		final HashMap<String, String> hashMapIcone = new HashMap<>();
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			if (!StringUtils.isNullOrEmpty(placeholder)) {
				listIcone = new ArrayList<>();
				final String[] splitPlaceholder = placeholder.split(Pattern.quote("||"));
				for (int i = 0; i < splitPlaceholder.length; i++) {
					final IconaStampigliaturaSegnoGraficoDTO iconOrImage = stampigliaturaSegnoGraficoDAO.getIconByPlaceholder(splitPlaceholder[i], numDocumento, connection);
					listIcone.add(iconOrImage);
				}

				for (final IconaStampigliaturaSegnoGraficoDTO elementoListaCompleta : listIcone) {
					if (!hashMapIcone.containsKey(elementoListaCompleta.getFontAwesomeIcon())) {
						hashMapIcone.put(elementoListaCompleta.getFontAwesomeIcon(), elementoListaCompleta.getDescrIcona());
					} else {
						final String elemento = hashMapIcone.get(elementoListaCompleta.getFontAwesomeIcon());
						hashMapIcone.replace(elementoListaCompleta.getFontAwesomeIcon(), elemento + "\n" + elementoListaCompleta.getDescrIcona());
					}
				}
			}

		} catch (final Exception ex) {
			rollbackConnection(connection);
			LOGGER.error("Errore durante il recupero dell'icona dal placeholder : " + ex);
			throw new RedException("Errore durante il recupero dell'icona dal placeholder : " + ex);
		} finally {
			closeConnection(connection);
		}
		return hashMapIcone;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IStampigliaturaSegnoGraficoFacadeSRV#getFirmatariStringArray(java.lang.String[],
	 *      java.lang.Long, java.lang.Integer).
	 */
	@Override
	public String[] getFirmatariStringArray(final String[] siglatariString, final Long idNodo, final Integer tipoFirma) {
		String[] arraySiglatari = null;
		int lunghezza = 0;
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			if (siglatariString == null || siglatariString.length == 0) {
				final String siglatarioAutomatico = iterApprovativoDAO.getFirmatario(idNodo, tipoFirma, connection);
				if (!"0".equals(siglatarioAutomatico.split("\\,")[1])) {
					arraySiglatari = new String[1];
					arraySiglatari[0] = siglatarioAutomatico.split("\\,")[1];
				}
			} else {
				for (final String siglatario : siglatariString) {
					if (siglatario.split("\\,") != null && siglatario.split("\\,").length == 3) {
						if ("4".equals(siglatario.split("\\,")[2])) {
							lunghezza = lunghezza + 1;
						}
					} else {
						lunghezza = lunghezza + 1;
					}
				}

				arraySiglatari = new String[lunghezza];
				int i = 0;
				for (final String siglatario : siglatariString) {
					if (siglatario.split("\\,") != null && siglatario.split("\\,").length == 3) {
						if ("4".equals(siglatario.split("\\,")[2])) {
							arraySiglatari[i] = siglatario.split("\\,")[1];
							i++;
						}
					} else {
						final String[] arrF = siglatario.split("\\,");
						if (arrF == null || arrF.length < 2) {
							continue;
						}
						arraySiglatari[i] = arrF[1];
						i++;
					}
				}
			}
		} catch(final Exception ex) {
			LOGGER.error(ERRORE_DURANTE_IL_RECUPERO_DEL_SIGLATARIO + ex);
			throw new RedException(ERRORE_DURANTE_IL_RECUPERO_DEL_SIGLATARIO + ex);	
		} finally {
			closeConnection(connection);
		}
		return arraySiglatari;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IStampigliaturaSegnoGraficoFacadeSRV#getTipoFirma(java.lang.Integer).
	 */
	@Override
	public final Integer getTipoFirma(final Integer idIterApprovativo) {
		Integer output = null;
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(),false);
			output = stampigliaturaSegnoGraficoDAO.getTipoFirma(idIterApprovativo, conn); 
		} catch (final Exception ex) {
			LOGGER.error(ERRORE_DURANTE_IL_RECUPERO_DEL_SIGLATARIO + ex);
			throw new RedException(ERRORE_DURANTE_IL_RECUPERO_DEL_SIGLATARIO + ex);	
		} finally {
			closeConnection(conn);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.IStampigliaturaSegnoGraficoSRV#getPlaceholderSigla(java.lang.Long,
	 *      java.lang.Long, java.lang.Integer, java.util.List).
	 */
	@Override
	public HashMap<String, byte[]> getPlaceholderSigla(final Long idUtente, final Long idUffIstruttore, final Integer idTipoDoc,
			final List<StampigliaturaSegnoGraficoDTO> listStampigliaturaSegnoGrafico) {
		HashMap<String, byte[]> placeholderList = new HashMap<>();
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			placeholderList = stampigliaturaSegnoGraficoDAO.getPlaceholderSegnoGrafico(idUtente, idUffIstruttore, idTipoDoc, listStampigliaturaSegnoGrafico, conn);
		} catch(final Exception ex) {
			LOGGER.error("Errore nel recupero del placeholder di sigla: " + ex);
			throw new RedException("Errore nel recupero del placeholder di sigla: " + ex);
		} finally {
			closeConnection(conn);
		}
		return placeholderList;
	}

}
