package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.Date;

import com.filenet.api.constants.PropertyNames;
import com.filenet.api.core.Document;

import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.ILookupDAO;
import it.ibm.red.business.dto.DocumentoFascicoloDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class FromDocumentoToDocumentoFascicoloTrasformer.
 *
 * @author CPIERASC
 * 
 *         Trasformer documento fascicolo.
 */
public class FromDocumentoToDocumentoFascicoloTrasformer extends TrasformerCE<DocumentoFascicoloDTO> {
	
	/**
	 * Dimensione massima abstract oggetto.
	 */
	private static final int ABSTRACT_MAX_DIMENSION = 200;

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -1520929066920434528L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FromDocumentoToDocumentoFascicoloTrasformer.class.getName());

	/**
	 * Dao lookup.
	 */
	private final ILookupDAO lookupDAO;
	
	/**
	 * DAO per recuperare l'aoo e l'ente
	 */
	private final IAooDAO aooDAO;
	
	/**
	 * Costruttore.
	 */
	public FromDocumentoToDocumentoFascicoloTrasformer() {
		super(TrasformerCEEnum.FROM_DOCUMENTO_TO_DOCUMENTO_FASCICOLO);
		lookupDAO = ApplicationContextProvider.getApplicationContext().getBean(ILookupDAO.class);
		aooDAO = ApplicationContextProvider.getApplicationContext().getBean(IAooDAO.class);
	}
	
	/**
	 * Trasform.
	 *
	 * @param document
	 *            the document
	 * @param connection
	 *            the connection
	 * @return the documento fascicolo DTO
	 */
	@Override
	public final DocumentoFascicoloDTO trasform(final Document document, final Connection connection) {
		try {
			final String documentTitle = (String) getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);

			final String oggetto = (String) getMetadato(document, PropertiesNameEnum.OGGETTO_METAKEY);
			final String oggettoCutted = StringUtils.getTextAbstract(oggetto, ABSTRACT_MAX_DIMENSION);
			
			Integer inNumeroDocumento = null;
			Integer numeroProtocollo = null; 
			Date dataProtocollo = null; 
			Integer idCategoria = null; //E/U
			Integer idTipologiaDocumento = null; //Tipologia Generica

			Date dataCreazione = null; 
			String guid = null;
			String nomeFile = null;
			String mimeType = null;
			Integer tipoProtocollo = null;
			Integer idAOO = null;
			Aoo aoo = null; 
			Integer numAllegati = null;
			
			Boolean bIsFascicolo = false;
			final String fascicoloClassName = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY);
			if (fascicoloClassName.equalsIgnoreCase(document.getClassName())) {
				bIsFascicolo = true;
			} else {
				inNumeroDocumento = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
				numeroProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY); 
				dataProtocollo = (Date) getMetadato(document, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY); 
				idCategoria = (Integer) getMetadato(document, PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY); //E/U
				idTipologiaDocumento = (Integer) getMetadato(document, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY); //Tipologia Generica
				
				dataCreazione = (Date) getMetadato(document, PropertiesNameEnum.DATA_CREAZIONE_METAKEY); 
				guid = StringUtils.cleanGuidToString(document.get_Id());
				nomeFile = (String) getMetadato(document, PropertiesNameEnum.NOME_FILE_METAKEY);
				mimeType = (String) getMetadato(document, PropertyNames.MIME_TYPE);
				tipoProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY);
				idAOO = (Integer) getMetadato(document, PropertiesNameEnum.ID_AOO_METAKEY); 
				if (idAOO != null) {
					aoo = aooDAO.getAoo(idAOO.longValue(), connection);
				}
				
				//Calcolo numero di Allegati
				numAllegati = document.get_CompoundDocumentState().getValue();
			}
			final String idProtocollo = (String) getMetadato(document, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY);
			
			final Integer sottoCategoriaDocumentoUscita = (Integer) getMetadato(document, PropertiesNameEnum.SOTTOCATEGORIA_DOCUMENTO_USCITA);
			
			Boolean flagItegrazioneDati = (Boolean) getMetadato(document, PropertiesNameEnum.INTEGRAZIONE_DATI_METAKEY);
			if (flagItegrazioneDati == null) {
				flagItegrazioneDati = false;
			}
						
			return new DocumentoFascicoloDTO(inNumeroDocumento, fromIdToDescTipoDoc(idTipologiaDocumento, connection), idCategoria, 
					fromIdToDescCategoriaDoc(idCategoria), numeroProtocollo, dataProtocollo, oggettoCutted, bIsFascicolo, documentTitle,
					guid, dataCreazione, nomeFile, mimeType, tipoProtocollo, aoo, numAllegati, idProtocollo, sottoCategoriaDocumentoUscita,
					flagItegrazioneDati);
		} catch (final Exception e) {
			LOGGER.error("Errore nel trasform del fascicolo: ", e);
			return null;
		}
	}
	
	/**
	 * Da id tipo documento a descrizione.
	 * 
	 * @param idTipDoc		identificativo
	 * @param connection	connessione
	 * @return				descrizione
	 */
	private String fromIdToDescTipoDoc(final Integer idTipDoc, final Connection connection) {
		String output = "";
		if (idTipDoc != null) {
			output = lookupDAO.getDescTipoDocumento(idTipDoc.longValue(), connection);
		}
		return output;
	}
	/**
	 * Da id categoria documento a descrizione.
	 * 
	 * @param idCatDoc	identificativo
	 * @return			descrizione
	 */
	private static String fromIdToDescCategoriaDoc(final Integer idCatDoc) {
		String output = null;
		if (idCatDoc != null) {
			output = CategoriaDocumentoEnum.get(idCatDoc).getDescrizione();
		}
		return output;
	}

}
