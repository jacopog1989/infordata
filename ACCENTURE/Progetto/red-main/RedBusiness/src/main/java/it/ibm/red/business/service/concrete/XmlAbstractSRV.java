package it.ibm.red.business.service.concrete;

import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.DateUtils;

/**
 * Abstract srv per i service che si basano sulla creazione di dom xml.
 * 
 * @author s.lungarella.ibm
 */
public abstract class XmlAbstractSRV extends AbstractService {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1896813636392489734L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(XmlAbstractSRV.class);
	
	/**
	 * Messaggio di warning in caso di errore nella popolazione di un parametro.
	 */
	private static final String WARNING_RECUPERO_VALORE_TAG = "Errore nel popolamento di un tag dei parameter, il tag verrà popolato con una stringa vuota.";
	
	/**
	 * Crea e restituisce il document per la gestione dei parameters per il
	 * popolamento di un xslt.
	 * 
	 * @return Document document xml generato
	 */
	protected Document prepareParameters() {
		DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
		documentFactory.setAttribute(Constants.ACCESS_EXTERNAL_DTD, "");
		documentFactory.setAttribute(Constants.ACCESS_EXTERNAL_SCHEMA, "");
		
		DocumentBuilder documentBuilder = null;
		Document document = null;
		try {
			documentBuilder = documentFactory.newDocumentBuilder();
			document = documentBuilder.newDocument();
			Element root = document.createElement("parameters");
            document.appendChild(root);
			
		} catch (Exception e) {
			LOGGER.error("Errore riscontrato nella creazione del document xml per la gestione dei parameters.", e);
			throw new RedException("Errore riscontrato nella creazione del document xml per la gestione dei parameters.", e);
		}

		return document;
        
	}

	/**
	 * Restituisce il contenuto del document xml sotto forma di String.
	 * 
	 * @param document
	 *            documento dal quale recuperare lo stream <em> xml </em>
	 * @return String xml rappresentante il contenuto del document
	 */
	protected String getXmlFromDocument(Document document) {
		StringWriter stringWriter=new StringWriter();
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		transformerFactory.setAttribute(Constants.ACCESS_EXTERNAL_DTD, "");
		transformerFactory.setAttribute(Constants.ACCESS_EXTERNAL_STYLESHEET, "");
		Transformer transformer;
        try {
			transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty("indent","yes");
	        DOMSource domSource = new DOMSource(document);
	        StreamResult result = new StreamResult(stringWriter);
	        transformer.transform(domSource, result);     
		} catch (Exception e) {
			LOGGER.error("Errore riscontrato durante il parsing del document per la conversione in String.", e);
			throw new RedException("Errore riscontrato durante il parsing del document per la conversione in String.", e);
		}
          
        return stringWriter.toString();
	}

	/**
	 * Aggiunge un element al document con tag {@code parameter} e valore tipo <em>
	 * String </em> {@code value}.
	 * 
	 * @param parameter
	 *            tag dell'elemento
	 * @param value
	 *            valore dell'elemento
	 * @param document
	 *            document modificato
	 */
	private static void addStringChild(final String parameter, String value, Document document) {
		
		Element param = document.createElement(parameter);
		if(value == null) {
			value = "";
		}
		param.appendChild(document.createTextNode(value));
		document.getLastChild().appendChild(param);
	}

	/**
	 * Aggiunge un element al document con tag {@code parameter} e valore tipo <em>
	 * Date </em> {@code value}.
	 * 
	 * @param parameter
	 *            tag dell'elemento
	 * @param value
	 *            valore dell'elemento
	 * @param document
	 *            document modificato
	 */
	private static void addDateChild(final String parameter, Date value, Document document) {
		Element param = document.createElement(parameter);
		String date = "";
		if(value != null) {
			try {
				date = new SimpleDateFormat(DateUtils.DD_MM_YYYY).format(value);
			} catch (Exception e) {
				LOGGER.warn(WARNING_RECUPERO_VALORE_TAG, e);
			}
		}
		
		param.appendChild(document.createTextNode(date));
		document.getLastChild().appendChild(param);
	}

	/**
	 * Aggiunge un element al document con tag {@code parameter} e valore tipo <em>
	 * Integer </em> {@code value}.
	 * 
	 * @param parameter
	 *            tag dell'elemento
	 * @param value
	 *            valore dell'elemento
	 * @param document
	 *            document modificato
	 */
	private static void addNumberChild(final String parameter, Integer value, Document document) {
		Element param = document.createElement(parameter);
		String number = "";
		if(value != null) {
			try {
				number = value.toString();
			} catch (Exception e){
				LOGGER.warn(WARNING_RECUPERO_VALORE_TAG, e);
			}
		}
		param.appendChild(document.createTextNode(number));
		document.getLastChild().appendChild(param);
	}

	/**
	 * Aggiunge un element all'element <em> element </em> con tag {@code parameter} e
	 * valore {@code value}.
	 * 
	 * @param parameter
	 *            tag dell'elemento
	 * @param value
	 *            valore dell'elemento
	 * @param document
	 *            document modificato
	 * @param element
	 *            element modificato
	 */
	protected void addToElement(final String parameter, Object value, Document document, Element element) {
		
		Element param = document.createElement(parameter);
		String valueToSet = "";
		try {
			if (value == null) {
				valueToSet = "";
			} else if (value instanceof Date) {
				valueToSet = new SimpleDateFormat(DateUtils.DD_MM_YYYY).format(value);
			} else {
				valueToSet = value.toString();
			}
		} catch (Exception e) {
			LOGGER.warn(WARNING_RECUPERO_VALORE_TAG, e);
		}
		
		
		param.appendChild(document.createTextNode(valueToSet));
		element.appendChild(param);
	}
	
	/**
	 * Aggiunge un element al document <em> element </em> con tag {@code parameter}
	 * e valore {@code value}.
	 * 
	 * @param parameter
	 *            tag dell'elemento
	 * @param value
	 *            valore dell'elemento
	 * @param document
	 *            document modificato
	 * @param element
	 *            element modificato
	 */
	protected void addToDocument(final String parameter, Object value, Document document) {
		
		try {
			if (value instanceof Date) {
				addDateChild(parameter, (Date) value, document);
			} else if (value instanceof Integer){
				addNumberChild(parameter, (Integer) value, document);
			} else {
				addStringChild(parameter, value.toString(), document);
			}
		} catch (Exception e) {
			LOGGER.warn(WARNING_RECUPERO_VALORE_TAG, e);
		}
	}
}
