package it.ibm.red.business.enums;

/**
 * The Enum StepTypeEnum.
 *
 * @author CPIERASC
 * 
 *         Enum tipo step.
 */
public enum StepTypeEnum {

	/**
	 * Tipo step completato.
	 */
	COMPLETATO,

	/**
	 * Tipo step corso.
	 */
	IN_CORSO,

	/**
	 * Tipo step da completare.
	 */
	DA_COMPLETARE,

	/**
	 * Tipo step rifiutato.
	 */
	RIFIUTATO,

	/**
	 * Tipo step inizio.
	 */
	INIZIO,

	/**
	 * Step in pausa 
	 */
	IN_PAUSA,
	
	/**
	 * Step procedimento concluso 
	 */
	PROCEDIMENTO_CONCLUSO
}
