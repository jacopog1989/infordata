/**
 * 
 */
package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.persistence.model.LookupTable;

/**
 * @author APerquoti
 *
 */
public interface ILookupTableFacadeSRV extends Serializable {

	/**
	 * Metodo per recuperare i selettori attivi per una specifica AOO.
	 * 
	 * @param idAoo
	 *            identificativo dell'area organizzativa per la quale occorre
	 *            recuperare i selettori validi
	 * @return collezione dei nomi dei selettori
	 */
	Collection<String> getSelectors(long idAoo);

	/**
	 * Restituisce le liste utenti complete per uno specifico AOO.
	 * 
	 * @param identificativo
	 *            dell'Area organizzativa per la quale ricercare i selettori se non
	 *            <code> null </code>. Se l'identificativo dell'AOO è
	 *            <code> null </code> restituisce le liste utenti valide per tutte
	 *            le AOO
	 */
	Collection<LookupTable> getLookupTables(Long idAoo);
}
