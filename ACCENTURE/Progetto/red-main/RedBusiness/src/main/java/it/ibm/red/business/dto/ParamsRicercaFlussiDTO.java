/**
 * 
 */
package it.ibm.red.business.dto;

import java.util.Date;
import java.util.List;

import it.ibm.red.business.enums.AmbitoTemporaleEnum;
import it.ibm.red.business.enums.FilenetStatoFascicoloEnum;
import it.ibm.red.business.enums.TipoContestoProceduraleEnum;

/**
 * Classe ParamsRicercaFlussiDTO.
 */
public class ParamsRicercaFlussiDTO extends AbstractDTO {

	private static final long serialVersionUID = -769228097947382221L;

	/**
	 * Stato fascicolo 
	 */
	private FilenetStatoFascicoloEnum statoFascicolo;
	
	/**
	 * Flusso 
	 */
	private TipoContestoProceduraleEnum flusso;
	
	/**
	 * Ambito temporale 
	 */
	private AmbitoTemporaleEnum ambitoTemporale;
	
	/**
	 * Nome fascicolo 
	 */
	private String nomeFascicolo;
	
	/**
	 * Descrizione Fascicolo 
	 */
	private String descrizioneFascicolo;
	
	/**
	 * Numero pratica 
	 */
	private Integer numeroPratica;
	
	/**
	 * Identificativo provvedimento 
	 */
	private Integer idProvvedimento;
	
	/**
	 * Numero protocollo mittente 
	 */
	private Integer numProtMittente;
	
	/**
	 * Data protocollo mittente 
	 */
	private Date dataProtMittente;
	
	/**
	 * Data apertura da 
	 */
	private Date dataAperturaDa;
	
	/**
	 * Data apertura a 
	 */
	private Date dataAperturaA;
	
	/**
	 * Data creazione da 
	 */
	private Date dataCreazioneDa;
	
	/**
	 * Data creazione a 
	 */
	private Date dataCreazioneA;
	
	/**
	 * Ordine pagamento 
	 */
	private String ordinePagamento;
	
	/**
	 * Camicia 
	 */
	private String camicia;
	
	/**
	 * Capitolo 
	 */
	private String capitolo;
		
	/**
	 * Decreto liquidazione 
	 */
	private String decretoLiquidazione;
	
	/**
	 * Tipologia contabile
	 */
	private String tipoCont;
	
	/**
	 * Agente contabile 
	 */
	private String ageCont;
	
	/**
	 * Esercizio
	 */
	private String esercizio;
	
	/**
	 * Lista delle tipologie contabili.
	 */
	private List<SelectItemDTO> tipologieContabili;
	
	/**
	 * Lista degli agenti contabili.
	 */
	private List<SelectItemDTO> agentiContabili;
	
	/** 
	 *
	 * @return the stato fascicolo
	 */
	public FilenetStatoFascicoloEnum getStatoFascicolo() {
		return statoFascicolo;
	}
	
	/** 
	 *
	 * @param statoFascicolo the new stato fascicolo
	 */
	public void setStatoFascicolo(final FilenetStatoFascicoloEnum statoFascicolo) {
		this.statoFascicolo = statoFascicolo;
	}
	
	/** 
	 *
	 * @return the nome fascicolo
	 */
	public String getNomeFascicolo() {
		return nomeFascicolo;
	}
	
	/** 
	 *
	 * @param nomeFascicolo the new nome fascicolo
	 */
	public void setNomeFascicolo(final String nomeFascicolo) {
		this.nomeFascicolo = nomeFascicolo;
	}
	
	/** 
	 *
	 * @return the descrizione fascicolo
	 */
	public String getDescrizioneFascicolo() {
		return descrizioneFascicolo;
	}
	
	/** 
	 *
	 * @param descrizioneFascicolo the new descrizione fascicolo
	 */
	public void setDescrizioneFascicolo(final String descrizioneFascicolo) {
		this.descrizioneFascicolo = descrizioneFascicolo;
	}
	
	/** 
	 * @return the numero pratica
	 */
	public Integer getNumeroPratica() {
		return numeroPratica;
	}
	
	/** 
	 *
	 * @param numeroPratica the new numero pratica
	 */
	public void setNumeroPratica(final Integer numeroPratica) {
		this.numeroPratica = numeroPratica;
	}
	
	/** 
	 *
	 * @return the id provvedimento
	 */
	public Integer getIdProvvedimento() {
		return idProvvedimento;
	}
	
	/** 
	 *
	 * @param idProvvedimento the new id provvedimento
	 */
	public void setIdProvvedimento(final Integer idProvvedimento) {
		this.idProvvedimento = idProvvedimento;
	}
	
	/** 
	 *
	 * @return the num prot mittente
	 */
	public Integer getNumProtMittente() {
		return numProtMittente;
	}
	
	/** 
	 *
	 * @param numProtMittente the new num prot mittente
	 */
	public void setNumProtMittente(final Integer numProtMittente) {
		this.numProtMittente = numProtMittente;
	}
	
	/** 
	 *
	 * @return the data prot mittente
	 */
	public Date getDataProtMittente() {
		return dataProtMittente;
	}
	
	/** 
	 *
	 * @param dataProtMittente the new data prot mittente
	 */
	public void setDataProtMittente(final Date dataProtMittente) {
		this.dataProtMittente = dataProtMittente;
	}
	
	/** 
	 *
	 * @return the data apertura da
	 */
	public Date getDataAperturaDa() {
		return dataAperturaDa;
	}
	
	/** 
	 *
	 * @param dataAperturaDa the new data apertura da
	 */
	public void setDataAperturaDa(final Date dataAperturaDa) {
		this.dataAperturaDa = dataAperturaDa;
	}
	
	/** 
	 *
	 * @return the data apertura A
	 */
	public Date getDataAperturaA() {
		return dataAperturaA;
	}
	
	/** 
	 *
	 * @param dataAperturaA the new data apertura A
	 */
	public void setDataAperturaA(final Date dataAperturaA) {
		this.dataAperturaA = dataAperturaA;
	}

	/** 
	 *
	 * @return the flusso
	 */
	public TipoContestoProceduraleEnum getFlusso() {
		return flusso;
	}
	
	/** 
	 *
	 * @param flusso the new flusso
	 */
	public void setFlusso(final TipoContestoProceduraleEnum flusso) {
		this.flusso = flusso;
	}
	
	/** 
	 *
	 * @return the ambito temporale
	 */
	public AmbitoTemporaleEnum getAmbitoTemporale() {
		return ambitoTemporale;
	}
	
	/** 
	 *
	 * @param ambitoTemporale the new ambito temporale
	 */
	public void setAmbitoTemporale(final AmbitoTemporaleEnum ambitoTemporale) {
		this.ambitoTemporale = ambitoTemporale;
	}
	
	/** 
	 *
	 * @return the data creazione da
	 */
	public Date getDataCreazioneDa() {
		return dataCreazioneDa;
	}
	
	/** 
	 *
	 * @param dataCreazioneDa the new data creazione da
	 */
	public void setDataCreazioneDa(final Date dataCreazioneDa) {
		this.dataCreazioneDa = dataCreazioneDa;
	}
	
	/** 
	 *
	 * @return the data creazione A
	 */
	public Date getDataCreazioneA() {
		return dataCreazioneA;
	}
	
	/** 
	 *
	 * @param dataCreazioneA the new data creazione A
	 */
	public void setDataCreazioneA(final Date dataCreazioneA) {
		this.dataCreazioneA = dataCreazioneA;
	}
	
	/** 
	 *
	 * @return the ordinePagamento
	 */
	public String getOrdinePagamento() {
		return ordinePagamento;
	}
	
	/** 
	 *
	 * @param ordinePagamento the ordinePagamento to set
	 */
	public void setOrdinePagamento(final String ordinePagamento) {
		this.ordinePagamento = ordinePagamento;
	}
	
	/** 
	 *
	 * @return the camicia
	 */
	public String getCamicia() {
		return camicia;
	}
	
	/** 
	 *
	 * @param camicia the camicia to set
	 */
	public void setCamicia(final String camicia) {
		this.camicia = camicia;
	}
	
	/** 
	 *
	 * @return the capitolo
	 */
	public String getCapitolo() {
		return capitolo;
	}
	
	/** 
	 *
	 * @param capitolo the capitolo to set
	 */
	public void setCapitolo(final String capitolo) {
		this.capitolo = capitolo;
	}
	
	/** 
	 *
	 * @return the decreto liquidazione
	 */
	public String getDecretoLiquidazione() {
		return decretoLiquidazione;
	}
	
	/** 
	 *
	 * @param decretoLiquidazione the new decreto liquidazione
	 */
	public void setDecretoLiquidazione(final String decretoLiquidazione) {
		this.decretoLiquidazione = decretoLiquidazione;
	}

	/**
	 * 
	 * @return la tipologia contabile
	 */
	public String getTipoCont() {
		return tipoCont;
	}

	/** 
	 *
	 * @param tipoCont the new tipologia contabile
	 */
	public void setTipoCont(String tipoCont) {
		this.tipoCont = tipoCont;
	}

	/**
	 * 
	 * @return l'agente contabile
	 */
	public String getAgeCont() {
		return ageCont;
	}

	/** 
	 *
	 * @param ageCont the new agente contabile
	 */
	public void setAgeCont(String ageCont) {
		this.ageCont = ageCont;
	}

	/**
	 * 
	 * @return esercizio
	 */
	public String getEsercizio() {
		return esercizio;
	}

	/** 
	 *
	 * @param esercizio the new esercizio
	 */
	public void setEsercizio(String esercizio) {
		this.esercizio = esercizio;
	}

	/**
	 * @return the tipologieContabili
	 */
	public List<SelectItemDTO> getTipologieContabili() {
		return tipologieContabili;
	}

	/**
	 * @param tipologieContabili the tipologieContabili to set
	 */
	public void setTipologieContabili(List<SelectItemDTO> tipologieContabili) {
		this.tipologieContabili = tipologieContabili;
	}

	/**
	 * @return the agentiContabili
	 */
	public List<SelectItemDTO> getAgentiContabili() {
		return agentiContabili;
	}

	/**
	 * @param agentiContabili the agentiContabili to set
	 */
	public void setAgentiContabili(List<SelectItemDTO> agentiContabili) {
		this.agentiContabili = agentiContabili;
	}
	
}