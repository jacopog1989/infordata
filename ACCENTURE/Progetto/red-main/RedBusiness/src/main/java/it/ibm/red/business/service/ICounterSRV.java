/**
 * 
 */
package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.ICounterFacadeSRV;

/**
 * @author APerquoti
 *
 */
public interface ICounterSRV extends ICounterFacadeSRV {

}
