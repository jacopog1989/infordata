package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IApprovazioneDAO;
import it.ibm.red.business.dto.ApprovazioneDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * The Class ApprovazioneDAO.
 *
 * @author m.crescentini
 * 
 *         Dao per la gestione delle approvazioni.
 */
@Repository
public class ApprovazioneDAO extends AbstractDAO implements IApprovazioneDAO {

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1087447120707389680L;
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ApprovazioneDAO.class.getName());

	/**
	 * Metodo che consente di aggiornare la stampigliatura di un'approvazione.
	 * @param stampigliatura
	 * @param idApprovazione
	 * @param con
	 * @return esisto della query di Update
	 */
	public int aggiornaStampigliatura(final int stampigliatura, final int idApprovazione, final Connection con) {
		LOGGER.info("aggiornaStampigliatura -> START");
		PreparedStatement ps = null;
		int esito = 0;

		try {
			int index = 1;
			ps = con.prepareStatement("UPDATE approvazione SET stampigliaturafirma = ? WHERE idapprovazione = ?");
			ps.setInt(index++, stampigliatura);
			ps.setInt(index++, idApprovazione);

			esito = ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("Errore nell'aggiornamento della stampigliatura", e);
			throw new RedException("Errore nell'aggiornamento della stampigliatura", e);
		} finally {
			closeStatement(ps);
		}

		LOGGER.info("aggiornaStampigliatura -> END");
		return esito;
	}

	/**
	 * @see it.ibm.red.business.dao.IApprovazioneDAO#getApprovazioniByIdDocumento(int,
	 *      int, java.sql.Connection).
	 */
	@Override
	public Collection<ApprovazioneDTO> getApprovazioniByIdDocumento(final int idDocumento, final int idAoo,
			final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			String sql = " SELECT " 
					+ " a.idapprovazione AS idapprovazione, "
					+ " a.iddocumento AS iddocumento, "
					+ " a.motivazione AS motivazione, "
					+ " n.idnodo AS idufficio, "
					+ " n.descrizione AS descufficio, "
					+ " u.idutente AS idutente, "
					+ " u.nome AS nome, "
					+ " u.cognome AS cognome, "
					+ " u.nome||' ' ||u.cognome AS descutente, "
					+ " ta.idtipoapprovazione AS idtipoapprovazione, "
					+ " ta.descrizione AS desctipoapprovazione, "
					+ " a.dataapprovazione, "
					+ " a.stampigliaturafirma AS stampigliaturafirma, "
					+ " (select r.nomeruolo AS nomeruolo "
					+ " from ruolo r join nodoutenteruolo nur on r.idruolo = nur.idruolo and nur.predefinito = 1 and nur.datadisattivazione is null "
					+ " join approvazione a2 on a2.idnodo = nur.idnodo "
					+ " where a2.iddocumento = a.iddocumento and nur.idutente = u.idutente and rownum = 1 "
					+ " ) AS nomeruolo "
					+ " FROM approvazione a, tipoapprovazione ta, utente u, nodo n "
					+ " WHERE a.iddocumento = ? "
					+ " AND a.idaoo = ? "
					+ " AND n.idnodo = a.idnodo "
					+ " AND u.idutente = a.idutente "
					+ " AND ta.idtipoapprovazione = a.idtipoapprovazione "
					+ " ORDER BY a.dataapprovazione ASC";
			ps = con.prepareStatement(sql);
			ps.setInt(index++, idDocumento);
			ps.setInt(index++, idAoo);
			rs = ps.executeQuery();
			return fitResultsetIntoObj(rs);
		} catch (SQLException e) {
			LOGGER.error("Errore nel recupero delle approvazioni per il documento " + idDocumento, e);
			throw new RedException("Errore nel recupero delle approvazioni per il documento " + idDocumento, e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * 
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private static Collection<ApprovazioneDTO> fitResultsetIntoObj(final ResultSet rs) throws SQLException {
		Collection<ApprovazioneDTO> approvazioniList = new ArrayList<>();
		if (null != rs) {
			while (rs.next()) {
				approvazioniList.add(getApprovazione(rs));
			}
		}
		return approvazioniList;
	}

	private static ApprovazioneDTO getApprovazione(final ResultSet rs) throws SQLException {

		return new ApprovazioneDTO(rs.getInt("idapprovazione"), rs.getDate("dataapprovazione"),
				rs.getString("desctipoapprovazione"), rs.getString("motivazione"), rs.getString("descutente"),
				rs.getString("descufficio"), rs.getInt("stampigliaturafirma"), rs.getString("nome"),
				rs.getString("cognome"), rs.getString("nomeRuolo"), rs.getInt("idtipoapprovazione"));
	}
}