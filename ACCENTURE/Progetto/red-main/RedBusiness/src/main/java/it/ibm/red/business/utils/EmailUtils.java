package it.ibm.red.business.utils;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.regex.Pattern;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Header;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hsmf.MAPIMessage;
import org.apache.poi.hsmf.datatypes.AttachmentChunks;
import org.apache.poi.hsmf.datatypes.ByteChunk;
import org.apache.poi.hsmf.datatypes.StringChunk;
import org.apache.poi.hsmf.exceptions.ChunkNotFoundException;

import com.sun.mail.util.BASE64DecoderStream;

import it.ibm.red.business.dto.NamedStreamDTO;
import it.ibm.red.business.enums.TipoNotificaPecEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * Utility per le email.
 */
public final class EmailUtils {

	/**
	 * Descrizione line separator.
	 */
	private static final String LINE_SEPARATOR = "line.separator";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(EmailUtils.class.getName());
	
	/**
	 * Messaggio.
	 */
	private static final String VALID_MESSAGEID = "[a-zA-Z0-9.<!#-'*+\\-/=?^_`{-~]+@[a-z!A-Z0-9-.>^_`{-~]+";
	
	/**
	 * Regex indirizzo mail valido.
	 */
	public static final String VALID_MAIL_ADDRESS = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9\\-]+(\\.[A-Za-z0-9\\-]+)*(\\.[A-Za-z]{2,})$";

	/**
	 * Nome file del testo della mail.
	 */
	public static final String NOME_NODO_TESTO_MAIL = "Testo_Mail.html";
	
	/**
	 * Messaggio.
	 */
	private static final Pattern VALID_MESSAGEID_PATTERN = Pattern.compile(VALID_MESSAGEID);
	
	// Costruttore private vuoto.
	private EmailUtils() {
		// Non deve fare niente.
	}
	
	/**
	 * Estrae testo e allegati dall'email.
	 * 
	 * @param content
	 *            mail in formato byteArray deve essere valorizzato
	 * @return Un lista del contenuto dell'email
	 * @throws IOException
	 */
	public static List<NamedStreamDTO> extractBodyTextAndAttachmentsFromMsg(final byte[] content) throws IOException {
		final List<NamedStreamDTO> fileList = new ArrayList<>();

		try (InputStream in = new ByteArrayInputStream(content); MAPIMessage mapiMessage = new MAPIMessage(in);) {
			try {
				final String textBody = mapiMessage.getTextBody();
				initializeTextNode(fileList, textBody);
			} catch (final ChunkNotFoundException e) {
				LOGGER.warn("questo messaggio non ha testo", e);
			}

			final AttachmentChunks[] attachments = mapiMessage.getAttachmentFiles();
			for (final AttachmentChunks attach : attachments) {
				final String fileName = getAttachmentFileName(attach);
				final ByteChunk byteChunk = attach.getAttachData();
				// Siccome è possibile allegare dei dati senza titolo all'email si vanno a
				// generare delle situazioni difficilmente gestibili
				// In quei casi io ignorerò l'allegato
				if (fileName != null && byteChunk != null) {
					final byte[] attachContent = byteChunk.getValue();
					final NamedStreamDTO attachNode = new NamedStreamDTO();
					attachNode.setContent(attachContent);
					attachNode.setName(fileName);
					fileList.add(attachNode);
				}
			}
		}
		return fileList;
	}
	
	private static Session getMailSession() {
		final Properties prop = new Properties();
		prop.setProperty("mail.mime.base64.ignoreerrors", "true");
		
		System.setProperty("mail.mime.multipart.ignoreexistingboundaryparameter", "true");
		System.setProperty("mail.mime.parameters.strict", "false");
		System.setProperty("mail.mime.decodeparameters", "true");
		
		return Session.getInstance(prop); 
	}
	
	/**
	 * Istanzia un oggetto MimeMessage.
	 * @param is
	 * @return oggetto MimeMessage
	 * @throws MessagingException
	 */
	public static MimeMessage getMimeMessageFromInputStream(final InputStream is) throws MessagingException {
		return new MimeMessage(getMailSession(), is);
	}
	
	/**
	 * Estrae testo e allegati dal messaggio.
	 * @param message messaggio
	 * @return lista di file
	 * @throws MessagingException
	 * @throws IOException
	 */
	public static List<NamedStreamDTO> extractBodyTextAndAttachmentsFromMimeMessage(final MimeMessage message) throws MessagingException, IOException {
		final List<NamedStreamDTO> fileList = new LinkedList<>();
		parseEmlPart(message, fileList);

		return fileList;
	}
	
	/**
	 * Estrae testo e allegati da eml.
	 * @param content
	 * @return lista di file
	 * @throws MessagingException
	 * @throws IOException
	 */
	public static List<NamedStreamDTO> extractBodyTextAndAttachmentsFromEml(final byte[] content) throws MessagingException, IOException {
		final List<NamedStreamDTO> fileList = new LinkedList<>();
		try (InputStream is = new ByteArrayInputStream(content)) {
			final MimeMessage message = new MimeMessage(getMailSession(), is);
			parseEmlPart(message, fileList);
		}

		return fileList;
	}
	
	/**
	 * Inizializza textNode.
	 * @return textNode
	 */
	public static NamedStreamDTO initializeTextNode() {
		final NamedStreamDTO textNode = new NamedStreamDTO();
		textNode.setName(NOME_NODO_TESTO_MAIL);
		return textNode;
	}

	/**
	 * Inizializza textNode con estensione pdf.
	 * @return textNode
	 */
	public static NamedStreamDTO initializeTextNodePdf() {
		final NamedStreamDTO textNode = new NamedStreamDTO();
		textNode.setName("Testo_Mail.pdf");
		return textNode;
	}
	
	private static void parseEmlPart(final Part message, final List<NamedStreamDTO> fileList) throws IOException, MessagingException {
		if (!message.isMimeType("text/rfc822headers")) { // Esclusione degli headers
			if (!Part.ATTACHMENT.equals(message.getDisposition()) && message.isMimeType("text/calendar")) {
				
				final BASE64DecoderStream base64DecoderStream = (BASE64DecoderStream) message.getContent();
			    final byte[] byteArray = IOUtils.toByteArray(base64DecoderStream);
			    final byte[] content = Base64.encodeBase64(byteArray);
			    final NamedStreamDTO textNode = new NamedStreamDTO();
				textNode.setName("calendar.ics");
				textNode.setContent(content);
				fileList.add(textNode);
				
			} else if (!Part.ATTACHMENT.equals(message.getDisposition()) && message.isMimeType("text/*")) {
				final String txt = (String) message.getContent();
				initializeTextNode(fileList, txt);
			} else if (message.isMimeType("multipart/*")) {
				recursiveParseMultipart(message, fileList);
			} else {
				
				String fileName = message.getFileName();
				
				byte[] content = null;
				boolean contentRecuperato = false;
				try {
					content = IOUtils.toByteArray(message.getInputStream());
					contentRecuperato = true;
				} catch (final Exception e) {
					LOGGER.warn(e);
					content = ("Impossibile scaricare il file originale: il file risulta corrotto.").getBytes();
					if (StringUtils.isBlank(fileName)) {
						fileName = UUID.randomUUID().toString();
					}
					fileName = fileName + "_CORROTTO";
				}
				
				if (contentRecuperato) {
					if (StringUtils.isBlank(fileName)) {
						fileName = "temp" + UUID.nameUUIDFromBytes(content);
						
						if (message.isMimeType("message/rfc822")) {
							fileName = fileName + ".eml";
						} else {
							fileName = fileName + ".dat";
						}
					}
					
					if (StringUtils.isNotBlank(fileName) && fileName.startsWith("=?") && fileName.endsWith("?=")) { // Gestione encoding MIME
						fileName = MimeUtility.decodeText(fileName);
					}
					
					// Elimina caratteri non ascii
					fileName = it.ibm.red.business.utils.StringUtils.cleanNonAsciiCharacters(fileName);
				}
				
				final NamedStreamDTO textNode = new NamedStreamDTO();
				textNode.setName(fileName);
				textNode.setContent(content);
				fileList.add(textNode);
			}
			
		}
	}
	

	private static void recursiveParseMultipart(final Part message, final List<NamedStreamDTO> fileList) throws IOException, MessagingException {
		final Multipart multipart = (Multipart) message.getContent();

		for (int i = 0; i < multipart.getCount(); i++) {
			final BodyPart bodyPart = multipart.getBodyPart(i);
			// Torna al caso base
			parseEmlPart(bodyPart, fileList);
		}
	}
	
	/**
	 * Estrae eml messaggio originale dalla busta di trasporto.
	 * @param bustaDiTrasporto busta di trasporto
	 * @return eml messaggio originale
	 * @throws MessagingException
	 * @throws IOException
	 */
	public static MimeBodyPart estraiEmlMessaggioOriginaleDaBustaDiTrasporto(final MimeMessage bustaDiTrasporto) throws MessagingException, IOException {
		final MimeBodyPart emlMessaggioOriginale = new MimeBodyPart();
		
		parseEmlPart(bustaDiTrasporto, emlMessaggioOriginale);
		
		return emlMessaggioOriginale;
	}
	
	
	private static void parseEmlPart(final Part message, final MimeBodyPart emlMessaggioOriginale) throws IOException, MessagingException {
		if (message.isMimeType("multipart/*")) {
			recursiveParseMultipart(message, emlMessaggioOriginale);
		} else if (message.isMimeType("message/*")) {
			emlMessaggioOriginale.setContent(message.getContent(), message.getContentType());
		}
	}
	

	private static void recursiveParseMultipart(final Part message, final MimeBodyPart emlMessaggioOriginale) throws IOException, MessagingException {
		final Multipart multipart = (Multipart) message.getContent();
		
		for (int i = 0; i < multipart.getCount(); i++) {
			final BodyPart bodyPart = multipart.getBodyPart(i);
			// Torna al caso base
			parseEmlPart(bodyPart, emlMessaggioOriginale);
		}
	}

	private static void initializeTextNode(final List<NamedStreamDTO> fileList, final String textBody) {
		final NamedStreamDTO textNode = initializeTextNode();
		textNode.setContent(textBody.getBytes());
		fileList.add(textNode);
	}


	private static String getAttachmentFileName(final AttachmentChunks attach) {
		String fileName = null;

		final StringChunk titleChunk = attach.getAttachFileName();
		final StringChunk longTitleChunk = attach.getAttachLongFileName();
		if (titleChunk != null) {
			fileName = titleChunk.getValue();
		}
		if (longTitleChunk != null && (org.apache.commons.lang3.StringUtils.isBlank(fileName) || StringUtils.contains(fileName, '~'))) {
			fileName = longTitleChunk.getValue();
		}
		
		if(fileName==null && "multipart/signed".equals(attach.getAttachMimeTag().toString())) {
			throw new RedException("##EXTRACTFAILED##");
		}
		
		fileName = it.ibm.red.business.utils.StringUtils.cleanNonAsciiCharacters(fileName);	
			 
		return fileName;
	}
	
	/**
	 * Valida l'id del messaggio.
	 * @param inMessageId id del messaggio
	 * @return true se l'id del messaggio è valido, false in caso contrario
	 */
	public static boolean validaMessageId(final String inMessageId) {
		boolean output = false;
	    String messageId = inMessageId;
	    
	    if (StringUtils.countMatches(messageId, "<") > 1 || StringUtils.countMatches(messageId, ">") > 1) {
	        return output;
	    }

	    if (StringUtils.contains(messageId, "<>")) {
	        messageId = StringUtils.substringBetween(messageId, "<", ">");
	        if (StringUtils.isBlank(messageId)) {
	            return output;
	        }
	    }
	    
	    if (StringUtils.contains(messageId, "..")) {
	        return output;
	    }
	    
	    // Extract from <>
	    messageId = messageId.trim();
	    // Now validate
	    output = VALID_MESSAGEID_PATTERN.matcher(messageId).matches();
	    
	    return output;
	}
	
	/**
	 * Ottiene l'id del messaggio tra parentesi.
	 * @param inMessageId id del messaggio
	 * @return id del messaggio tra parentesi
	 */
	public static String getMessageIdConParentesi(final String inMessageId) {
		String outMessageId = inMessageId;
		
		if (StringUtils.isNotBlank(inMessageId)) {
			
			if (!inMessageId.startsWith("<")) {
				outMessageId = "<" + outMessageId;
			}
			
			if (!inMessageId.endsWith(">")) {
				outMessageId += ">";
			}
		}
		
		return outMessageId;
	}
	
	
	/**
	 * Estrae gli indirizzi mail dall'array di Address.
	 * 
	 * @param addresses
	 * @return
	 */
	public static List<String> getIndirizziMailDaAddresses(final Address[] addresses) {
		List<String> indirizziMail = null;
		
		if (addresses != null) {
			indirizziMail = new ArrayList<>(addresses.length);
			
			String indirizzo;
			for (final Address address : addresses) {
				indirizzo = getIndirizzoMailDaAddress(address);
				
				if (StringUtils.isNotBlank(indirizzo)) {
					indirizziMail.add(indirizzo);
				}
			}
		}
		
		return indirizziMail;
	}
	
	
	/**
	 * @param address
	 * @return
	 */
	public static String getIndirizzoMailDaAddress(final Address address) {
		String indirizzo;
		
		final InternetAddress ia = (InternetAddress) address;
		
		// Si recupera l'indirizzo e-mail
		if (ia.getAddress() != null) {
			indirizzo = ia.getAddress();
		} else {
			indirizzo = ia.toUnicodeString();
		}
		
		return checkFormatoIndirizzoMail(indirizzo);
	}
	
	
	/**
	 * @param indirizzo
	 * @return
	 */
	private static String checkFormatoIndirizzoMail(String indirizzo) {
		if (StringUtils.isNotBlank(indirizzo)) {
			final int firstTag = indirizzo.indexOf('<');
			final int secondTag = indirizzo.indexOf('>');
			
			if (firstTag > 0 && secondTag > 0) {
				indirizzo = indirizzo.substring(firstTag + 1, secondTag);
			}
		}
		
		return indirizzo;
	}
	
	/**
	 * @param message
	 * @return
	 * @throws MessagingException
	 * @throws IOException
	 */
	public static InputStream getCompleteEmailBlob(final Message message) throws MessagingException, IOException {
		
		InputStream is = null;
		InputStreamReader isr = null;
		BufferedReader br = null;
		final StringBuilder sb = new StringBuilder();
		try {
			String line = null;	
			for (final Enumeration<?> e = message.getAllHeaders(); e.hasMoreElements();) {
				final Header h = (Header) e.nextElement();
				line = h.getName() + ": " + h.getValue() + System.getProperty(LINE_SEPARATOR);
				convertUnix2WindowsEOL(sb, line.getBytes(StandardCharsets.UTF_8.name()));
			}
	
			line = System.getProperty(LINE_SEPARATOR);
			convertUnix2WindowsEOL(sb, line.getBytes(StandardCharsets.UTF_8.name()));
				
			is = message.getInputStream();
			isr = new InputStreamReader(is);
			br = new BufferedReader(isr);
			
			while ((line = br.readLine()) != null) {
				line = line + System.getProperty(LINE_SEPARATOR);
				convertUnix2WindowsEOL(sb, line.getBytes(StandardCharsets.UTF_8.name()));
			}
			
		} finally {
			if (br != null) {
				br.close();
			}
			if (isr != null) {
				isr.close();
			}
			if (is != null) {
				is.close();
			}
		}
		
		final byte[] email = sb.toString().getBytes(StandardCharsets.UTF_8.name());
		return new ByteArrayInputStream(email);
	}
	
	private static void convertUnix2WindowsEOL(final StringBuilder sb, final byte[] lineArray) {
		boolean writeCRLF = true;
		char lastEOL = ' ';
		
		for (final byte b: lineArray) {
			if ((char) b == '\r' || (char) b == '\n') {
				if (writeCRLF) {
					sb.append("\r\n");
					lastEOL = (char) b;
					writeCRLF = false;
				} else {
					if (lastEOL == (char) b) {
						sb.append("\r\n");
					}
				}
			} else {
				sb.append((char) b);
				if (!writeCRLF) {
					writeCRLF = true;
				}
			}
		}
		
	}
	
	/**
	 * N.B. Prende in considerazione solo le tipologie di notifiche PEC che RED (in particolare GestioneMail) considera effettivamente notifiche.
	 * 
	 * @param oggetto
	 * @return
	 */
	public static boolean isNotificaPecRed(final String oggetto) {
		boolean isNotifica = false;
		
		if (TipoNotificaPecEnum.checkPrefissoNotifichePec(oggetto, 
				TipoNotificaPecEnum.ACCETTAZIONE, TipoNotificaPecEnum.AVVENUTA_CONSEGNA, TipoNotificaPecEnum.PREAVVISO_ERRORE_CONSEGNA, 
				TipoNotificaPecEnum.NON_ACCETTAZIONE, TipoNotificaPecEnum.ERRORE_CONSEGNA)) {
			isNotifica = true;		
		}
		
		return isNotifica;
	}
	
	/**
	 * Metodo che controlla se il formato della mail è corretto.
	 * 
	 * @param email
	 * @return
	 */
	public static boolean checkFormatoPecPeo(final String email) {
		boolean flagMailOk = true;
		final Pattern patternEmail = Pattern.compile("^[a-zA-Z0-9_\\.-]+@[a-zA-Z0-9-_\\.]+[a-zA-Z0-9-_]\\.[a-zA-Z0-9-]+$");
		//controllo formato E-Mail 
		if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(email) && !patternEmail.matcher(email).matches()) {
				flagMailOk = false;
		}
		
		return flagMailOk;
		
	}
}
