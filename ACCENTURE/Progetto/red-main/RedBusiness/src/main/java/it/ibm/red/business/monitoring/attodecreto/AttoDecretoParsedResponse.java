package it.ibm.red.business.monitoring.attodecreto;

/**
 * Classe parsed response - Atto Decreto.
 */
public class AttoDecretoParsedResponse {

	/**
	 * Identificativo raccolta provvisoria.
	 */
	private String idRaccoltaProvvisoria;

	/**
	 * Esito.
	 */
	private Integer esito;

	/**
	 * Descriione esito.
	 */
	private String descrizioneEsito;

	/**
	 * Costruttore di default, imposta l'esito a 0.
	 */
	public AttoDecretoParsedResponse() {
		super();
		this.esito = 0;
	}

	/**
	 * Costruttore di classe.
	 * @param esito
	 * @param descrizioneEsito
	 * @param idRaccoltaProvvisoria
	 */
	public AttoDecretoParsedResponse(final Integer esito, final String descrizioneEsito, final String idRaccoltaProvvisoria) {
		super();
		this.esito = esito;
		this.descrizioneEsito = descrizioneEsito;
		this.idRaccoltaProvvisoria = idRaccoltaProvvisoria;
	}

	/**
	 * Restituisce l'id della raccolat provvisoria.
	 * @return idRaccoltaProvvisoria
	 */
	public String getIdRaccoltaProvvisoria() {
		return idRaccoltaProvvisoria;
	}

	/**
	 * Imposta l'id della raccolta provvisoria.
	 * @param idRaccoltaProvvisoria
	 */
	public void setIdRaccoltaProvvisoria(final String idRaccoltaProvvisoria) {
		this.idRaccoltaProvvisoria = idRaccoltaProvvisoria;
	}

	/**
	 * Restituisce l'esito.
	 * @return esito
	 */
	public Integer getEsito() {
		return esito;
	}

	/**
	 * Imposta l'estio.
	 * @param esito
	 */
	public void setEsito(final Integer esito) {
		this.esito = esito;
	}

	/**
	 * Restituisce la descrizione dell'esito.
	 * @return descrizioneEsito
	 */
	public String getDescrizioneEsito() {
		return descrizioneEsito;
	}

	/**
	 * Imposta la descrizione dell'esito.
	 * @param descrizioneEsito
	 */
	public void setDescrizioneEsito(final String descrizioneEsito) {
		this.descrizioneEsito = descrizioneEsito;
	}

}