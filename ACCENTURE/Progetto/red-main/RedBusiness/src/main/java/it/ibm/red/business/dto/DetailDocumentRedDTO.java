package it.ibm.red.business.dto;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.filenet.StepDTO;
import it.ibm.red.business.dto.filenet.StoricoDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.ValueMetadatoVerificaFirmaEnum;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * DTO che definisce il dettaglio di un documento RED.
 */
public class DetailDocumentRedDTO extends AbstractDTO {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 1L;

	/**************
	 * ATTRIBUTI CLASSE "Documento_NSD" DEL CE START
	 **********************/

	/**
	 * Acl aggiornate.
	 */
	private Boolean aclAggiornata;

	/**
	 * Anno protocollazione mittente.
	 */
	private Integer annoProtocolloMittente;

	/**
	 * Anon protocollazione risposta.
	 */
	private Integer annoProtocolloRisposta;

	/**
	 * Anno documento.
	 */
	private Integer annoDocumento;

	/**
	 * Anno mittente contributo.
	 */
	private Integer annoMittenteContributo;

	/**
	 * Anno protocollo.
	 */
	private Integer annoProtocollo;

	/**
	 * Id applicativo client.
	 */
	private Integer idApplicativoClient;

	/**
	 * Ultima modifica.
	 */
	private String lastModifier;

	/**
	 * Barcode.
	 */
	private String barcode;

	/**
	 * Owner del lock.
	 */
	private String lockOwner;

	/**
	 * Flag declare.
	 */
	private Boolean canDeclare;

	/**
	 * Label component biding.
	 */
	private String componentBindingLabel;

	/**
	 * Flag version enable.
	 */
	private Boolean isVersioningEnabled;

	/**
	 * Utente creatore.
	 */
	private String creator;

	/**
	 * Data annullamento docunento.
	 */
	private Date dataAnnullamentoDocumento;

	/**
	 * Data annullamento protocollo.
	 */
	private Date dataAnnullamentoProtocollo;

	/**
	 * Data archiviazione.
	 */
	private Date dataArchiviazione;

	/**
	 * Data content retention.
	 */
	private Date contentRetentionDate;

	/**
	 * Data creazione.
	 */
	private Date dateCreated;

	/**
	 * Data check in.
	 */
	private Date dateCheckedIn;

	/**
	 * Data protocollo.
	 */
	private Date dataProtocollo;

	/**
	 * Data protocollo mittente.
	 */
	private Date dataProtocolloMittente;

	/**
	 * Data ultima modifica.
	 */
	private Date dateLastModified;

	/**
	 * Data ultimo accesso content.
	 */
	private Date dateContentLastAccessed;

	/**
	 * Data elenco divisionale.
	 */
	private Date dataElencoDivisionale;

	/**
	 * Data elenco trasmissione.
	 */
	private Date dataElencoTrasmissione;

	/**
	 * Data scadenza.
	 */
	private Date dataScadenza;

	/**
	 * Destinatario contributo esterno.
	 */
	private String destinatarioContributoEsterno;

	/**
	 * Document title.
	 */
	private String documentTitle;

	/**
	 * Flag exception state.
	 */
	private Boolean isInExceptionState;

	/**
	 * Flag current version.
	 */
	private Boolean isCurrentVersion;

	/**
	 * Flag reserved.
	 */
	private Boolean isReserved;

	/**
	 * Flag versione frozen.
	 */
	private Boolean isFrozenVersion;

	/**
	 * Elenco trasmissione.
	 */
	private String elencoTrasmissione;

	/**
	 * Nome file.
	 */
	private String nomeFile;

	/**
	 * Flag firma PDF.
	 */
	private Boolean firmaPDF;

	/**
	 * Flag corte dei conti.
	 */
	private Boolean flagCorteConti;

	/**
	 * Flag firma autografa.
	 */
	private Boolean flagFirmaAutografaRM;

	/**
	 * Id gruppo protocollatore.
	 */
	private Integer idGruppoProtocollatore;

	/**
	 * Guid documento.
	 */
	private String guid; // PropertyIdImpl (GUID -> PropertyNames.ID)

	/**
	 * Identificativo AOO.
	 */
	private Integer idAOO;

	/**
	 * Identificativo.
	 */
	private String indexationId; // PropertyIdImpl

	/**
	 * Entry.
	 */
	private String entryTemplateId; // PropertyIdImpl

	/**
	 * Id categoria documento.
	 */
	private Integer idCategoriaDocumento;

	/**
	 * Id formato documento.
	 */
	private Integer idFormatoDocumento;

	/**
	 * Id nodo coordinatore.
	 */
	private Long idNodoCoordinatore;

	/**
	 * Id nodo cretatore.
	 */
	private Integer idNodoCreatore;

	/**
	 * Id protocollo.
	 */
	private String idProtocollo;

	/**
	 * Id template.
	 */
	private Integer idTemplate;

	/**
	 * Id Tipologia documento.
	 */
	private Integer idTipologiaDocumento;

	/**
	 * Id tipo procedimento.
	 */
	private Integer idTipologiaProcedimento;

	/**
	 * Id ufficio creatore.
	 */
	private Integer idUfficioCreatore;

	/**
	 * Id utente protcollatore.
	 */
	private Long idUtenteCoordinatore;

	/**
	 * Id utente creatore.
	 */
	private Integer idUtenteCreatore;

	/**
	 * Flag ignora redirect.
	 */
	private Boolean ignoreRedirect;

	/**
	 * Info pubblication.
	 */
	private String publicationInfo; // PropertyBinaryImpl

	/**
	 * Flag stampigliatura.
	 */
	private Integer isStampigliato;

	/**
	 * Semaforo iter approvativo.
	 */
	private String iterApprovativoSemaforo;

	/**
	 * Mezzo ricezione.
	 */
	private Integer mezzoRicezione;

	/**
	 * Descrizione mezzo ricezione.
	 */
	private String mezzoRicezioneDescrizione; // non sta su filenet, serve alla maschera

	/**
	 * Mittente.
	 */
	private String mittente;

	/**
	 * Motivazione annullamento.
	 */
	private String motivazioneAnnullamento;

	/**
	 * Name.
	 */
	private String name;

	/**
	 * Object store name.
	 */
	private String entryTemplateObjectStoreName;

	/**
	 * Note.
	 */
	private String note;

	/**
	 * Wf.
	 */
	private String entryTemplateLaunchedWorkflowNumber;

	/**
	 * Numero protocollo.
	 */
	private Integer numeroProtocollo;

	/**
	 * Protocollo mittente.
	 */
	private String protocolloMittente;

	/**
	 * Numero protocollo risposta.
	 */
	private String numeroProtocolloRisposta;

	/**
	 * Identificativo protocollo risposta.
	 */
	private String idProtocolloRisposta;

	/**
	 * Numero raccomandata.
	 */
	private String numeroRaccomandata;

	/**
	 * Major version.
	 */
	private Integer majorVersionNumber;

	/**
	 * Minor version.
	 */
	private Integer minorVersionNumber;

	/**
	 * Numero circolare.
	 */
	private Integer numeroCircolare;

	/**
	 * Numero documento.
	 */
	private Integer numeroDocumento;

	/**
	 * Numero mittente contributo.
	 */
	private Integer numeroMittenteContributo;

	/**
	 * Oggetto.
	 */
	private String oggetto;

	/**
	 * Oggetto protocollo NPS.
	 */
	private String oggettoprotocollonps;

	/**
	 * Storage location.
	 */
	private String storageLocation;

	/**
	 * Owner documento.
	 */
	private String owner;

	/**
	 * Registro riservato.
	 */
	private Boolean registroRiservato;

	/**
	 * Flag riservato.
	 */
	private Integer riservato;

	/**
	 * Stato classificazione.
	 */
	private Integer classificationStatus;

	/**
	 * Stato corrente.
	 */
	private String currentState;

	/**
	 * Stato compound document.
	 */
	private Integer compoundDocumentState;

	/**
	 * Stato.
	 */
	private Integer versionStatus;

	/**
	 * Stato thread.
	 */
	private Integer statoThreadFirma;

	/**
	 * Storico assegnazione.
	 */
	private String storicoAssegnazione;

	/**
	 * Timeout lock.
	 */
	private Integer lockTimeout;

	/**
	 * Reservaation type.
	 */
	private Integer reservationType;

	/**
	 * Tipologia documento nps.
	 */
	private String tipologiadocumentonps;

	/**
	 * Tipologia protocollo.
	 */
	private Integer tipoProtocollo;

	/**
	 * Flag lock token.
	 */
	private String lockToken; // PropertyIdImpl

	/**
	 * Flag trasformazione pdf in errore.
	 */
	private Integer trasformazionePDFInErrore;

	/**
	 * Flag urgente.
	 */
	private boolean urgente;

	/**
	 * Identificativo utente protocollatore.
	 */
	private Integer idUtenteProtocollatore;

	/**
	 * Flag validità firma.
	 */
	private Integer validitaFirma;

	/**
	 * Versione copia conforme.
	 */
	private Integer versioneCopiaConforme;

	/**
	 * Descrizione flusso.
	 */
	private String descFlusso;

	/**
	 * Identificativo procedimento.
	 */
	private String idProcedimento;

	/**
	 * Flag documento elettronico ingressato tramite sistema ausiliario NPS.
	 */
	private boolean elettronicoViaSistemaAusiliarioNPS;
	
	/**
	 * Sistema Ausiliario NPS.
	 */
	private String sistemaAusiliarioNPS;

	/**************
	 * ATTRIBUTI CLASSE "Documento_NSD" DEL CE END
	 **********************/

	/**
	 * Lista di versioni del documento corrente.
	 */
	private List<VersionDTO> versioni;

	/**
	 * Destinatari.
	 */
	private List<DestinatarioRedDTO> destinatari;

	/**
	 * Allegati.
	 */
	private List<AllegatoDTO> allegati;

	/**
	 * Contributi.
	 */
	private List<ContributoDTO> contributi;

	/**
	 * Fascicoli.
	 */
	private List<FascicoloDTO> fascicoli;

	/**
	 * Faldoni.
	 */
	private Collection<FaldoneDTO> faldoni;

	/**
	 * Formato documento.
	 */
	private FormatoDocumentoEnum formatoDocumentoEnum;

	/*************
	 * PE start.
	 *******************************************************/
	private List<AssegnazioneDTO> assegnazioni;
	/**
	 * Se provengo da una coda avrò un wobnumber selezionato che NON Ã¨
	 * necessariamente quello principale .
	 */
	private String wobNumberSelected;
	/**
	 * Se ci sono più workflow attivi ne esiste uno principale.
	 */
	private String wobNumberPrincipale;
	/************* PE end *******************************************************/

	/** DB start ******************************************************************/

	/**
	 * Aoo.
	 */
	private Aoo aoo;

	/**
	 * Ufficio mittente.
	 */
	private Nodo ufficioMittente;

	/**
	 * Descrizione tipologia documento.
	 */
	private String descTipologiaDocumento;

	/**
	 * sul DB: NSD_WSCLASSEDOCUMENTALE.DOCUMENTCLASS.
	 */
	private String documentClass;

	/**
	 * Info mezzo ricezione.
	 */
	private MezzoRicezioneDTO mezzoRicezioneDTO;

	/**
	 * Descrizione tipo procedimento.
	 */
	private String descTipoProcedimento;

	/**
	 * Momento protocollazione.
	 */
	private MomentoProtocollazioneEnum momentoProtocollazioneEnum;

	/**
	 * Lista approvazioni.
	 */
	private List<ApprovazioneDTO> approvazioni;

	/**
	 * Storico.
	 */
	private Collection<StoricoDTO> storico;

	/**
	 * Lista step storico.
	 */
	private List<StepDTO> storicoSteps;

	/**
	 * Flag traccia procedimento.
	 */
	private Boolean tracciaProcedimento;

	/**
	 * Identificativo iter approvativo.
	 */
	private Integer idIterApprovativo;

	/**
	 * Tipo assegnazione.
	 */
	private TipoAssegnazioneEnum tipoAssegnazioneEnum;

	/**
	 * Flag da non protocollare.
	 */
	private Boolean daNonProtocollare;

	/**
	 * Flag ribalta titolario.
	 */
	private boolean ribaltaTitolario;

	/**
	 * Responsabile copia conforme.
	 */
	private ResponsabileCopiaConformeDTO responsabileCopiaConforme;

	/**
	 * Lista metadati dinamici.
	 */
	private Collection<MetadatoDinamicoDTO> metadatiDinamici;

	/**
	 * Lista metadati estesi.
	 */
	private Collection<MetadatoDTO> metadatiEstesi;
	
	/**
	 * Lista metadati valuta.
	 */
	private Collection<ValutaMetadatiDTO> metadatiValuta;
	
	/**
	 * Mappa valuta/cambi.
	 */
	private Map<String, Double> mapValutaCambio;
	
	/** DB end ********************************************************************/

	/*****************
	 * ATTRIBUTI INSERITI PER MASCHERA INSERIMENTO
	 **************************/

	/**
	 * Lista allacci.
	 */
	private List<RispostaAllaccioDTO> allacci;

	/**
	 * Mail spedizione.
	 */
	private EmailDTO mailSpedizione;

	/**
	 * Mittente contatto.
	 */
	private Contatto mittenteContatto;

	/**
	 * Id fascicolo procedimanetale.
	 */
	private String idFascicoloProcedimentale;

	/**
	 * Descrizione fascicolo procedimentale.
	 */
	private String descrizioneFascicoloProcedimentale;

	/**
	 * Indice classficazione fascicolo procedimentale.
	 */
	private String indiceClassificazioneFascicoloProcedimentale;

	/**
	 * Descrizione titolario fascicolo procedimentale.
	 */
	private String descrizioneTitolarioFascicoloProcedimentale;

	/**
	 * Fascicolo procedimentale prelex.
	 */
	private String prefixFascicoloProcedimentale;

	/**
	 * Data protollo riposta.
	 */
	private Date dataProtocolloRisposta;

	/**
	 * Flag check firma digitale RGS.
	 */
	private Boolean checkFirmaDigitaleRGS;

	/**
	 * Flag check firma copia conforme.
	 */
	private Boolean checkFirmaConCopiaConforme;

	/**
	 * Motivo assegnazione.
	 */
	private String motivoAssegnazione;
	/***************************
	 * ATTRIBUTI CONTENT DOCUMENTO
	 *******************************/

	/**
	 * Contenuto.
	 */
	private byte[] content;

	/**
	 * Mime type.
	 */
	private String mimeType;

	/******************
	 * ATTRIBUTI INSERITI PER MODIFICA DOCUMENTO
	 **************************/

	/**
	 * Folder.
	 */
	private String folder;

	/**
	 * Info code.
	 */
	private RecuperoCodeDTO code;
	/******************
	 * ATTRIBUTI INSERITI PER MODIFICA DOCUMENTO
	 **************************/

	/*********************************
	 * PRELEX
	 **********************************************/

	/**
	 * Numero RDP.
	 */
	private Integer numeroRDP;

	/**
	 * Numero legislatura.
	 */
	private Integer numeroLegislatura;

	/**
	 * Legislatura.
	 */
	private String legislatura;

	/**
	 * Id raccolta FAD.
	 */
	private String idraccoltaFAD;
	/*********************************
	 * PRELEX
	 **********************************************/

	/**
	 * Metadato per documento con categoria Assegnazione Interna.
	 */
	private Integer numDocDestIntUscita;

	/**
	 * Flag documento modificabile.
	 */
	private Boolean modificabile;

	/**
	 * Flag content modificabile.
	 */
	private Boolean contentModificabile;

	/**
	 * PROTOCOLLAZIONE DA MAIL START
	 ****************************************************************************************/
	/**
	 * E' valorizzato se si sta facendo una protocollazione da mail.
	 */
	private String protocollazioneMailGuid;
	/**
	 * Flag notifica.
	 */
	private Boolean isNotifica;
	/**
	 * Protocollazione account.
	 */
	private String protocollazioneMailAccount;
	/**
	 * PROTOCOLLAZIONE DA MAIL END
	 ******************************************************************************************/

	/**
	 * Flag id file principale.
	 */
	private String idFilePrincipaleDaProtocolla;

	/**
	 * Contiene la nota dell'email. Che può essere travasata nel motivo
	 * assegnazione. Vedi il tab assegnazioni.
	 */
	private String contentNoteDaMailProtocolla;

	/**
	 * Data download.
	 */
	private Date dataScarico;

	/**
	 * Identificativo documento.
	 */
	private String identificativoDocumentoEsteso;

	/**
	 * Note mail.
	 */
	private NotaDTO contentNoteDaMailProtocollaDTO;

	/**
	 * Preassegnatario.
	 */
	private AssegnatarioInteropDTO preassegnatarioInteropDTO;

	/**
	 * Booleano che indica se visualizzare il tasto anteprima.
	 */
	private Boolean visualizzaAnteprimBtn;

	/**
	 * Dati template doc uscita.
	 */
	private Map<TemplateValueDTO, List<TemplateMetadatoValueDTO>> templateDocUscitaMap;

	/**
	 * Lista destinatari TO.
	 */
	private List<String> destinatariTO;

	/**
	 * Lista destinatari CC.
	 */
	private List<String> destinatariCC;

	/**
	 * Oggetto popolato esclusivamente nel caso di censimento, tramite creazione in
	 * entrata, di documenti cartacei acquisiti. Il dato contenuto nel nodo è il
	 * GUID del documento principale, mentre i figli contengono i GUID degli
	 * eventuali allegati.
	 */
	private NodoDTO<String> guidCartaceiAcquisiti;

	/**
	 * Anno protocollo emergenza.
	 */
	private Integer annoProtocolloEmergenza;

	/**
	 * Numero protocollo emergenza.
	 */
	private Integer numeroProtocolloEmergenza;

	/**
	 * Data registrazione protocollo emergenza.
	 */
	private Date dataProtocolloEmergenza;

	/**
	 * Flag registro repertorio.
	 */
	private Boolean isModalitaRegistroRepertorio;

	/**
	 * Descrizione registro repertorio.
	 */
	private String descrizioneRegistroRepertorio;

	/**
	 * Flag visualizza informativa.
	 */
	private Boolean visualizzaInformativaRR;

	/**
	 * Codice flusso per la gestione dei flussi esterni.
	 */
	private String codiceFlusso;

	/**
	 * Decreto Liquidazione SICOGE.
	 */
	private String decretoLiquidazioneSicoge;

	/**
	 * Mittente mail.
	 */
	private String mittenteMailProt;

	/**
	 * Flag riservato abilitato.
	 */
	private boolean riservatoAbilitato;

	/**
	 * Flag protocolla e mantieni.
	 */
	private boolean protocollaEMantieni;

	/**
	 * Lista allegati non sbustati.
	 */
	private List<AllegatoDTO> allegatiNonSbustati;

	/**
	 * Lista allacci.
	 */
	private List<RiferimentoStoricoNsdDTO> allaccioRifStorico;

	/**
	 * Dati protocollo NSD.
	 */
	private ProtocolliPregressiDTO protocolloVerificatoNsd;

	/**
	 * Contatto mittente otf.
	 */
	private String objectIdContattoMittOnTheFly;

	/**
	 * Contatto destinatario otf.
	 */
	private String objectIdContattoDestOnTheFly;

	/**
	 * Flag spedizione mail.
	 */
	private boolean spedizioneMail;

	/**
	 * Flag contatti preferiti senza gruppi.
	 */
	private boolean contattiPreferitiSenzaGruppi;

	/**
	 * Flag protocolla mail.
	 */
	private boolean fromProtocollaMail;

	/**
	 * Flag abilita contatto on the fly.
	 */
	private boolean abilitaIlContattoOnTheFlyDaProt;

	/**
	 * Sotto categoria uscita.
	 */
	private SottoCategoriaDocumentoUscitaEnum sottoCategoriaUscita;

	/**
	 * Identificativo registro ausiliario.
	 */
	private Integer idRegistroAusiliario;

	/**
	 * Metadati registro ausiliario.
	 */
	private Collection<MetadatoDTO> metadatiRegistroAusiliario;

	/**
	 * Protocollo riferimento.
	 */
	private String protocolloRiferimento;

	/**
	 * Flag mostra protocollo riferimento.
	 */
	private String protRiferimentoToShow;

	/**
	 * Flag integrazione dati.
	 */
	private boolean integrazioneDati;

	/**
	 * Dati registrazione ausiliaria.
	 */
	private RegistrazioneAusiliariaDTO registrazioneAusiliaria;

	/**
	 * Identificativo processo.
	 */
	private String identificatoreProcesso;

	/**
	 * Lista response grezze.
	 */
	private String[] responsesRaw;

	/**
	 * Flag modifica fascicolo.
	 */
	private boolean modificaFascicolo;

	/**
	 * Dimensione content.
	 */
	private float sizeContent;

	/**
	 * Dimensione massima content.
	 */
	private Float maxSizeContent;

	/**
	 * Dimensione documento principale in MB.
	 */
	private Float sizeDocPrincipaleMB;

	/**
	 * Flag modifica e non predisponi.
	 */
	private boolean isInModificaAndNotPredisponi;
	
	/**
     * Flag metadati minimi.
     */
	private boolean modificaMetadatiMinimi;

	/**
	 * Flag workflow attivo.
	 */
	private boolean haWKFAttivo;

	/**
	 * Verifica firma.
	 */
	private ValueMetadatoVerificaFirmaEnum valueMetaadatoVerificaFirmaEnum;

	/**
	 * Tiplogia firma.
	 */
	private int tipoFirma;
	
	/**
	 * allaccioRifNps.
	 */
	private List<RiferimentoProtNpsDTO> allaccioRifNps;
	
	/**
     * ASign item.
     */
	private ASignItemDTO aSignItem;
	
	/**
	 * Costruttore.
	 */
	public DetailDocumentRedDTO() {
		super();
	}

	/**
	 * Copy constructor.
	 * 
	 * @param d
	 */
	public DetailDocumentRedDTO(final DetailDocumentRedDTO d) {
		super();
		this.aclAggiornata = d.aclAggiornata;
		this.annoProtocolloMittente = d.annoProtocolloMittente;
		this.annoProtocolloRisposta = d.annoProtocolloRisposta;
		this.annoDocumento = d.annoDocumento;
		this.annoMittenteContributo = d.annoMittenteContributo;
		this.annoProtocollo = d.annoProtocollo;
		this.idApplicativoClient = d.idApplicativoClient;
		this.lastModifier = d.lastModifier;
		this.barcode = d.barcode;
		this.lockOwner = d.lockOwner;
		this.canDeclare = d.canDeclare;
		this.componentBindingLabel = d.componentBindingLabel;
		this.isVersioningEnabled = d.isVersioningEnabled;
		this.creator = d.creator;
		this.dataAnnullamentoDocumento = d.dataAnnullamentoDocumento;
		this.dataAnnullamentoProtocollo = d.dataAnnullamentoProtocollo;
		this.dataArchiviazione = d.dataArchiviazione;
		this.contentRetentionDate = d.contentRetentionDate;
		this.dateCreated = d.dateCreated;
		this.dateCheckedIn = d.dateCheckedIn;
		this.dataProtocollo = d.dataProtocollo;
		this.dataProtocolloMittente = d.dataProtocolloMittente;
		this.dateLastModified = d.dateLastModified;
		this.dateContentLastAccessed = d.dateContentLastAccessed;
		this.dataElencoDivisionale = d.dataElencoDivisionale;
		this.dataElencoTrasmissione = d.dataElencoTrasmissione;
		this.dataScadenza = d.dataScadenza;
		this.destinatarioContributoEsterno = d.destinatarioContributoEsterno;
		this.documentTitle = d.documentTitle;
		this.isInExceptionState = d.isInExceptionState;
		this.isCurrentVersion = d.isCurrentVersion;
		this.isReserved = d.isReserved;
		this.isFrozenVersion = d.isFrozenVersion;
		this.elencoTrasmissione = d.elencoTrasmissione;
		this.nomeFile = d.nomeFile;
		this.firmaPDF = d.firmaPDF;
		this.flagCorteConti = d.flagCorteConti;
		this.flagFirmaAutografaRM = d.flagFirmaAutografaRM;
		this.idGruppoProtocollatore = d.idGruppoProtocollatore;
		this.guid = d.guid;
		this.idAOO = d.idAOO;
		this.indexationId = d.indexationId;
		this.entryTemplateId = d.entryTemplateId;
		this.idCategoriaDocumento = d.idCategoriaDocumento;
		this.idFormatoDocumento = d.idFormatoDocumento;
		this.idNodoCoordinatore = d.idNodoCoordinatore;
		this.idNodoCreatore = d.idNodoCreatore;
		this.idProtocollo = d.idProtocollo;
		this.idTemplate = d.idTemplate;
		this.idTipologiaDocumento = d.idTipologiaDocumento;
		this.idTipologiaProcedimento = d.idTipologiaProcedimento;
		this.idUfficioCreatore = d.idUfficioCreatore;
		this.idUtenteCoordinatore = d.idUtenteCoordinatore;
		this.idUtenteCreatore = d.idUtenteCreatore;
		this.ignoreRedirect = d.ignoreRedirect;
		this.publicationInfo = d.publicationInfo;
		this.isStampigliato = d.isStampigliato;
		this.iterApprovativoSemaforo = d.iterApprovativoSemaforo;
		this.mezzoRicezione = d.mezzoRicezione;
		this.mezzoRicezioneDescrizione = d.mezzoRicezioneDescrizione;
		this.mittente = d.mittente;
		this.motivazioneAnnullamento = d.motivazioneAnnullamento;
		this.name = d.name;
		this.entryTemplateObjectStoreName = d.entryTemplateObjectStoreName;
		this.note = d.note;
		this.entryTemplateLaunchedWorkflowNumber = d.entryTemplateLaunchedWorkflowNumber;
		this.numeroProtocollo = d.numeroProtocollo;
		this.protocolloMittente = d.protocolloMittente;
		this.numeroProtocolloRisposta = d.numeroProtocolloRisposta;
		this.numeroRaccomandata = d.numeroRaccomandata;
		this.majorVersionNumber = d.majorVersionNumber;
		this.minorVersionNumber = d.minorVersionNumber;
		this.numeroCircolare = d.numeroCircolare;
		this.numeroDocumento = d.numeroDocumento;
		this.numeroMittenteContributo = d.numeroMittenteContributo;
		this.oggetto = d.oggetto;
		this.oggettoprotocollonps = d.oggettoprotocollonps;
		this.storageLocation = d.storageLocation;
		this.owner = d.owner;
		this.registroRiservato = d.registroRiservato;
		this.riservato = d.riservato;
		this.classificationStatus = d.classificationStatus;
		this.currentState = d.currentState;
		this.compoundDocumentState = d.compoundDocumentState;
		this.versionStatus = d.versionStatus;
		this.statoThreadFirma = d.statoThreadFirma;
		this.storicoAssegnazione = d.storicoAssegnazione;
		this.lockTimeout = d.lockTimeout;
		this.reservationType = d.reservationType;
		this.tipologiadocumentonps = d.tipologiadocumentonps;
		this.tipoProtocollo = d.tipoProtocollo;
		this.lockToken = d.lockToken;
		this.trasformazionePDFInErrore = d.trasformazionePDFInErrore;
		this.urgente = d.urgente;
		this.idUtenteProtocollatore = d.idUtenteProtocollatore;
		this.validitaFirma = d.validitaFirma;
		this.versioneCopiaConforme = d.versioneCopiaConforme;
		this.versioni = d.versioni;
		this.destinatari = d.destinatari;
		this.allegati = d.allegati;
		this.contributi = d.contributi;
		this.fascicoli = d.fascicoli;
		this.faldoni = d.faldoni;
		this.formatoDocumentoEnum = d.formatoDocumentoEnum;
		this.assegnazioni = d.assegnazioni;
		this.wobNumberSelected = d.wobNumberSelected;
		this.wobNumberPrincipale = d.wobNumberPrincipale;
		this.aoo = d.aoo;
		this.ufficioMittente = d.ufficioMittente;
		this.descTipologiaDocumento = d.descTipologiaDocumento;
		this.documentClass = d.documentClass;
		this.mezzoRicezioneDTO = d.mezzoRicezioneDTO;
		this.descTipoProcedimento = d.descTipoProcedimento;
		this.momentoProtocollazioneEnum = d.momentoProtocollazioneEnum;
		this.approvazioni = d.approvazioni;
		this.storico = d.storico;
		this.storicoSteps = d.storicoSteps;
		this.tracciaProcedimento = d.tracciaProcedimento;
		this.idIterApprovativo = d.idIterApprovativo;
		this.tipoAssegnazioneEnum = d.tipoAssegnazioneEnum;
		this.daNonProtocollare = d.daNonProtocollare;
		this.ribaltaTitolario = d.ribaltaTitolario;
		this.responsabileCopiaConforme = d.responsabileCopiaConforme;
		this.metadatiDinamici = d.metadatiDinamici;
		this.metadatiEstesi = d.metadatiEstesi;
		this.allacci = d.allacci;
		this.mailSpedizione = d.mailSpedizione;
		this.mittenteContatto = d.mittenteContatto;
		this.idFascicoloProcedimentale = d.idFascicoloProcedimentale;
		this.descrizioneFascicoloProcedimentale = d.descrizioneFascicoloProcedimentale;
		this.indiceClassificazioneFascicoloProcedimentale = d.indiceClassificazioneFascicoloProcedimentale;
		this.descrizioneTitolarioFascicoloProcedimentale = d.descrizioneTitolarioFascicoloProcedimentale;
		this.prefixFascicoloProcedimentale = d.prefixFascicoloProcedimentale;
		this.dataProtocolloRisposta = d.dataProtocolloRisposta;
		this.checkFirmaDigitaleRGS = d.checkFirmaDigitaleRGS;
		this.checkFirmaConCopiaConforme = d.checkFirmaConCopiaConforme;
		this.motivoAssegnazione = d.motivoAssegnazione;
		this.content = d.content;
		this.mimeType = d.mimeType;
		this.folder = d.folder;
		this.code = d.code;
		this.numeroRDP = d.numeroRDP;
		this.numeroLegislatura = d.numeroLegislatura;
		this.legislatura = d.legislatura;
		this.idraccoltaFAD = d.idraccoltaFAD;
		this.numDocDestIntUscita = d.numDocDestIntUscita;
		this.modificabile = d.modificabile;
		this.contentModificabile = d.contentModificabile;
		this.protocollazioneMailGuid = d.protocollazioneMailGuid;
		this.isNotifica = d.isNotifica;
		this.protocollazioneMailAccount = d.protocollazioneMailAccount;
		this.idFilePrincipaleDaProtocolla = d.idFilePrincipaleDaProtocolla;
		this.contentNoteDaMailProtocolla = d.contentNoteDaMailProtocolla;
		this.identificativoDocumentoEsteso = d.identificativoDocumentoEsteso;
		this.contentNoteDaMailProtocollaDTO = d.contentNoteDaMailProtocollaDTO;
		this.preassegnatarioInteropDTO = d.preassegnatarioInteropDTO;
		this.visualizzaAnteprimBtn = d.visualizzaAnteprimBtn;
		this.templateDocUscitaMap = d.templateDocUscitaMap;
		this.destinatariTO = d.destinatariTO;
		this.destinatariCC = d.destinatariCC;
		this.guidCartaceiAcquisiti = d.guidCartaceiAcquisiti;
		this.annoProtocolloEmergenza = d.annoProtocolloEmergenza;
		this.numeroProtocolloEmergenza = d.numeroProtocolloEmergenza;
		this.dataProtocolloEmergenza = d.dataProtocolloEmergenza;
		this.isModalitaRegistroRepertorio = d.isModalitaRegistroRepertorio;
		this.descrizioneRegistroRepertorio = d.descrizioneRegistroRepertorio;
		this.codiceFlusso = d.codiceFlusso;
		this.decretoLiquidazioneSicoge = d.decretoLiquidazioneSicoge;
		this.mittenteMailProt = d.mittenteMailProt;
		this.riservatoAbilitato = d.riservatoAbilitato;
		this.protocollaEMantieni = d.protocollaEMantieni;
		this.allegatiNonSbustati = d.allegatiNonSbustati;
		this.allaccioRifStorico = d.allaccioRifStorico;
		this.protocolloVerificatoNsd = d.protocolloVerificatoNsd;
		this.spedizioneMail = d.spedizioneMail;
		this.sottoCategoriaUscita = d.sottoCategoriaUscita;
		this.idRegistroAusiliario = d.idRegistroAusiliario;
		this.metadatiRegistroAusiliario = d.metadatiRegistroAusiliario;
		this.registrazioneAusiliaria = d.registrazioneAusiliaria;

		this.protocolloRiferimento = d.getProtocolloRiferimento();
		this.protRiferimentoToShow = d.getProtRiferimentoToShow();

		this.integrazioneDati = d.getIntegrazioneDati();

		this.descFlusso = d.getDescFlusso();
		this.idProcedimento = d.getIdProcedimento();
		this.fromProtocollaMail = d.getFromProtocollaMail();

		final String sizeMBRound = String.format("%.2f", d.getSizeContent()).replace(",", ".");
		this.sizeContent = Float.parseFloat(sizeMBRound);
		this.sizeDocPrincipaleMB = d.getSizeDocPrincipaleMB();

		this.dataScarico = d.dataScarico;
		 
		this.allaccioRifNps = d.allaccioRifNps;
	}

	/**
	 * Restituisce l'assegnatario principale.
	 * 
	 * @return descrizione assegnatario principale
	 */
	public String getAssegnatarioPrincipale() {
		String output = null;

		if (idIterApprovativo == null) {
			for (final AssegnazioneDTO assegnazione : assegnazioni) {
				if (tipoAssegnazioneEnum == assegnazione.getTipoAssegnazione()) {
					output = assegnazione.getDescrizioneAssegnatario();
					break;
				}
			}
		}

		return output;
	}

	/**
	 * Restituisce true se il documento è in Entrata, false altrimenti.
	 * 
	 * @return isEntrata
	 */
	public Boolean getFlagEntrata() {
		Boolean output = false;

		if (CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0].equals(idCategoriaDocumento)
				|| CategoriaDocumentoEnum.ASSEGNAZIONE_INTERNA.getIds()[0].equals(idCategoriaDocumento)) {
			output = true;
		}

		return output;
	}

	/**
	 * Restituisce la descrizione del tipo della categoria del documento.
	 * 
	 * @return descrizione CategoriaDocumentoEnum
	 */
	public String getDescTipoCategoriaDoc() {
		String output = null;
		if (CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0].equals(idCategoriaDocumento)) {
			output = "ENTRATA";
		} else if (CategoriaDocumentoEnum.ASSEGNAZIONE_INTERNA.getIds()[0].equals(idCategoriaDocumento)) {
			// ASSEGNAZIONE_INTERNA firmo con destinatario interno per ogni firmatario
			// documento in entrata con tale valore
			output = "ASSEGNAZIONE INTERNA";
		} else if (CategoriaDocumentoEnum.INTERNO.getIds()[0].equals(idCategoriaDocumento)) {
			// INTERNO documento allegato al fascicolo (alias DOCUMENTO ISTRUTTORIA)
			output = "INTERNO";
		} else {
			if (annoProtocollo == null) {
				output = "USCITA NON PROTOCOLLATA";
			} else {
				output = "USCITA";
			}
		}
		return output;
	}

	/**
	 * Restituisce la descrizione dell'assegnatario per competenza.
	 * 
	 * @return descrizione assegnatario di AssegnazioneDTO
	 */
	public String getAssegnatarioCompetenza() {
		String output = null;
		if (assegnazioni != null) {
			for (final AssegnazioneDTO assegnazione : assegnazioni) {
				if (assegnazione != null && (TipoAssegnazioneEnum.COMPETENZA.equals(assegnazione.getTipoAssegnazione())
						|| TipoAssegnazioneEnum.STORICO.equals(assegnazione.getTipoAssegnazione()))) {
					output = assegnazione.getDescrizioneAssegnatario();
					break;
				}
			}
		}

		return output;
	}

	/******************************************************************************
	 * GET & SET START
	 ****************************************************************************/

	/**
	 * Restituisce il document title del documento.
	 * 
	 * @return documentTitle
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Imposta il document title del documento.
	 * 
	 * @param documentTitle
	 */
	public void setDocumentTitle(final String documentTitle) {
		this.documentTitle = documentTitle;
	}

	/**
	 * Restituisce true se la acl è aggiornata, altrimenti restituisce false.
	 * 
	 * @return aclAggiornata
	 */
	public Boolean isAclAggiornata() {
		return aclAggiornata;
	}

	/**
	 * Imposta il flag aclAggiornata.
	 * 
	 * @param aclAggiornata
	 */
	public void setAclAggiornata(final Boolean aclAggiornata) {
		this.aclAggiornata = aclAggiornata;
	}

	/**
	 * Restituisce l'anno del protocollo mittente.
	 * 
	 * @return annoProtocolloMittente
	 */
	public Integer getAnnoProtocolloMittente() {
		return annoProtocolloMittente;
	}

	/**
	 * Imposta l'anno del protocollo mittente.
	 * 
	 * @param annoProtocolloMittente
	 */
	public void setAnnoProtocolloMittente(final Integer annoProtocolloMittente) {
		this.annoProtocolloMittente = annoProtocolloMittente;
	}

	/**
	 * Restituisce l'anno del protocollo di risposta.
	 * 
	 * @return annoProtocolloRisposta
	 */
	public Integer getAnnoProtocolloRisposta() {
		return annoProtocolloRisposta;
	}

	/**
	 * Imposta l'anno del protocollo di risposta.
	 * 
	 * @param annoProtocolloRisposta
	 */
	public void setAnnoProtocolloRisposta(final Integer annoProtocolloRisposta) {
		this.annoProtocolloRisposta = annoProtocolloRisposta;
	}

	/**
	 * Restituisce l'anno del documento.
	 * 
	 * @return annoDocumento
	 */
	public Integer getAnnoDocumento() {
		return annoDocumento;
	}

	/**
	 * Imposta l'anno del documento.
	 * 
	 * @param annoDocumento
	 */
	public void setAnnoDocumento(final Integer annoDocumento) {
		this.annoDocumento = annoDocumento;
	}

	/**
	 * Restituisce l'anno mittente del contributo.
	 * 
	 * @return annoMittenteContributo
	 */
	public Integer getAnnoMittenteContributo() {
		return annoMittenteContributo;
	}

	/**
	 * Imposta l'anno del mittente contributo.
	 * 
	 * @param annoMittenteContributo
	 */
	public void setAnnoMittenteContributo(final Integer annoMittenteContributo) {
		this.annoMittenteContributo = annoMittenteContributo;
	}

	/**
	 * Restituisce l'anno del protocollo.
	 * 
	 * @return annoProtocollo
	 */
	public Integer getAnnoProtocollo() {
		return annoProtocollo;
	}

	/**
	 * Imposta l'anno del protocollo.
	 * 
	 * @param annoProtocollo
	 */
	public void setAnnoProtocollo(final Integer annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}

	/**
	 * Restituisce l'id dell'applicativo Client.
	 * 
	 * @return idApplicativoClient
	 */
	public Integer getIdApplicativoClient() {
		return idApplicativoClient;
	}

	/**
	 * Imposta l'id dell'applicativo Client.
	 * 
	 * @param idApplicativoClient
	 */
	public void setIdApplicativoClient(final Integer idApplicativoClient) {
		this.idApplicativoClient = idApplicativoClient;
	}

	/**
	 * Restituisce lastModifier.
	 * 
	 * @return lastModifier
	 */
	public String getLastModifier() {
		return lastModifier;
	}

	/**
	 * Imposta lastModifier.
	 * 
	 * @param lastModifier
	 */
	public void setLastModifier(final String lastModifier) {
		this.lastModifier = lastModifier;
	}

	/**
	 * Restituisce il barcode.
	 * 
	 * @return barcode
	 */
	public String getBarcode() {
		return barcode;
	}

	/**
	 * Imposta il barcode.
	 * 
	 * @param barcode
	 */
	public void setBarcode(final String barcode) {
		this.barcode = barcode;
	}

	/**
	 * @return lockOwner
	 */
	public String getLockOwner() {
		return lockOwner;
	}

	/**
	 * @param lockOwner
	 */
	public void setLockOwner(final String lockOwner) {
		this.lockOwner = lockOwner;
	}

	/**
	 * @return canDeclare
	 */
	public Boolean isCanDeclare() {
		return canDeclare;
	}

	/**
	 * @param canDeclare
	 */
	public void setCanDeclare(final Boolean canDeclare) {
		this.canDeclare = canDeclare;
	}

	/**
	 * @return componentBindingLabel
	 */
	public String getComponentBindingLabel() {
		return componentBindingLabel;
	}

	/**
	 * @param componentBindingLabel
	 */
	public void setComponentBindingLabel(final String componentBindingLabel) {
		this.componentBindingLabel = componentBindingLabel;
	}

	/**
	 * Restituisce true se il versioning è abilitato.
	 * 
	 * @return true se il versioning dei documenti è abilitato, false altrimenti
	 */
	public Boolean isVersioningEnabled() {
		return isVersioningEnabled;
	}

	/**
	 * Imposta il flag isVersioningEnabled.
	 * 
	 * @param isVersioningEnabled
	 */
	public void setVersioningEnabled(final Boolean isVersioningEnabled) {
		this.isVersioningEnabled = isVersioningEnabled;
	}

	/**
	 * Restituisce il creator del documento.
	 * 
	 * @return creator
	 */
	public String getCreator() {
		return creator;
	}

	/**
	 * Imposta il creator del documento.
	 * 
	 * @param creator
	 */
	public void setCreator(final String creator) {
		this.creator = creator;
	}

	/**
	 * Restituisce la data di annullamento del documento.
	 * 
	 * @return la data di annullamente se annullato, altrimenti restituisce null
	 */
	public Date getDataAnnullamentoDocumento() {
		return dataAnnullamentoDocumento;
	}

	/**
	 * Imposta la data di annullamento del documento.
	 * 
	 * @param dataAnnullamentoDocumento
	 */
	public void setDataAnnullamentoDocumento(final Date dataAnnullamentoDocumento) {
		this.dataAnnullamentoDocumento = dataAnnullamentoDocumento;
	}

	/**
	 * Restituisce la data di annullamento del protocollo.
	 * 
	 * @return la data di annullamente se annullato, altrimenti restituisce null
	 */
	public Date getDataAnnullamentoProtocollo() {
		return dataAnnullamentoProtocollo;
	}

	/**
	 * Imposta la data di annullamento del protocollo.
	 * 
	 * @param dataAnnullamentoProtocollo
	 */
	public void setDataAnnullamentoProtocollo(final Date dataAnnullamentoProtocollo) {
		this.dataAnnullamentoProtocollo = dataAnnullamentoProtocollo;
	}

	/**
	 * Restituisce la data di archiviazione.
	 * 
	 * @return dataArchiviazione se è stato archiviato, null altrimenti
	 */
	public Date getDataArchiviazione() {
		return dataArchiviazione;
	}

	/**
	 * Imposta la data di archiviazione del documento.
	 * 
	 * @param dataArchiviazione
	 */
	public void setDataArchiviazione(final Date dataArchiviazione) {
		this.dataArchiviazione = dataArchiviazione;
	}

	/**
	 * Restituisce la data di retention del content.
	 * 
	 * @return data di retention del content
	 */
	public Date getContentRetentionDate() {
		return contentRetentionDate;
	}

	/**
	 * Imposta la data di retention del content.
	 * 
	 * @param inContentRetentionDate
	 */
	public void setContentRetentionDate(final Date inContentRetentionDate) {
		contentRetentionDate = inContentRetentionDate;
	}

	/**
	 * Restituisce la data di creazione.
	 * 
	 * @return dateCreated
	 */
	public Date getDateCreated() {
		return dateCreated;
	}

	/**
	 * Imposta la data di creazione.
	 * 
	 * @param dateCreated
	 */
	public void setDateCreated(final Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	/**
	 * Restitusce la data di check in del documento.
	 * 
	 * @return data check in
	 */
	public Date getDateCheckedIn() {
		return dateCheckedIn;
	}

	/**
	 * Imposta la data di check in.
	 * 
	 * @param dateCheckedIn
	 */
	public void setDateCheckedIn(final Date dateCheckedIn) {
		this.dateCheckedIn = dateCheckedIn;
	}

	/**
	 * Restituisce la data del protocollo.
	 * 
	 * @return dataProtocollo
	 */
	public Date getDataProtocollo() {
		return dataProtocollo;
	}

	/**
	 * Imposta la data del protocollo.
	 * 
	 * @param dataProtocollo
	 */
	public void setDataProtocollo(final Date dataProtocollo) {
		this.dataProtocollo = dataProtocollo;
	}

	/**
	 * Restituisce la data di protocollo mittente.
	 * 
	 * @return data del protocollo mittente
	 */
	public Date getDataProtocolloMittente() {
		return dataProtocolloMittente;
	}

	/**
	 * Imposta la data del protocollo mittente.
	 * 
	 * @param dataProtocolloMittente
	 */
	public void setDataProtocolloMittente(final Date dataProtocolloMittente) {
		this.dataProtocolloMittente = dataProtocolloMittente;
	}

	/**
	 * Restituisce la data di ultima modifica.
	 * 
	 * @return data di ultima modifica
	 */
	public Date getDateLastModified() {
		return dateLastModified;
	}

	/**
	 * Imposta la data di ultima modifica.
	 * 
	 * @param dateLastModified
	 */
	public void setDateLastModified(final Date dateLastModified) {
		this.dateLastModified = dateLastModified;
	}

	/**
	 * Restituisce la data di ultimo accesso al content.
	 * 
	 * @return data ultimo accesso al content
	 */
	public Date getDateContentLastAccessed() {
		return dateContentLastAccessed;
	}

	/**
	 * Imposta la data di ultimo accesso al content.
	 * 
	 * @param dateContentLastAccessed
	 */
	public void setDateContentLastAccessed(final Date dateContentLastAccessed) {
		this.dateContentLastAccessed = dateContentLastAccessed;
	}

	/**
	 * Restituisce la data di elenco divisionale.
	 * 
	 * @return data elenco divisionale
	 */
	public Date getDataElencoDivisionale() {
		return dataElencoDivisionale;
	}

	/**
	 * Imposta la data di elenco divisionale.
	 * 
	 * @param dataElencoDivisionale
	 */
	public void setDataElencoDivisionale(final Date dataElencoDivisionale) {
		this.dataElencoDivisionale = dataElencoDivisionale;
	}

	/**
	 * Restituisce la data elenco trasmissione.
	 * 
	 * @return
	 */
	public Date getDataElencoTrasmissione() {
		return dataElencoTrasmissione;
	}

	/**
	 * Imposta la data elenco trasmissione.
	 * 
	 * @param dataElencoTrasmissione
	 */
	public void setDataElencoTrasmissione(final Date dataElencoTrasmissione) {
		this.dataElencoTrasmissione = dataElencoTrasmissione;
	}

	/**
	 * Restituisce la data di scadenza del documento.
	 * 
	 * @return dataScadenza
	 */
	public Date getDataScadenza() {
		return dataScadenza;
	}

	/**
	 * Imposta la data di scadenza del documento.
	 * 
	 * @param dataScadenza
	 */
	public void setDataScadenza(final Date dataScadenza) {
		this.dataScadenza = dataScadenza;
	}

	/**
	 * Restituisce il destinatario del contributo esterno.
	 * 
	 * @return destinatarioContributoEsterno
	 */
	public String getDestinatarioContributoEsterno() {
		return destinatarioContributoEsterno;
	}

	/**
	 * Imposta la data del contributo esterno.
	 * 
	 * @param destinatarioContributoEsterno
	 */
	public void setDestinatarioContributoEsterno(final String destinatarioContributoEsterno) {
		this.destinatarioContributoEsterno = destinatarioContributoEsterno;
	}

	/**
	 * Restituisce true se in stato di Exception.
	 * 
	 * @return isInException
	 */
	public Boolean isInExceptionState() {
		return isInExceptionState;
	}

	/**
	 * Imposta il flag isInExceptionState.
	 * 
	 * @param isInExceptionState
	 */
	public void setInExceptionState(final Boolean isInExceptionState) {
		this.isInExceptionState = isInExceptionState;
	}

	/**
	 * Restituisce true se è la versione corrente del documento.
	 * 
	 * @return true se la versione è quella corrente, false altrimenti
	 */
	public Boolean isCurrentVersion() {
		return isCurrentVersion;
	}

	/**
	 * Imposta isCurrentVersion che definisce se si tratta della versione corrente
	 * del documento.
	 * 
	 * @param isCurrentVersion
	 */
	public void setCurrentVersion(final Boolean isCurrentVersion) {
		this.isCurrentVersion = isCurrentVersion;
	}

	/**
	 * Restituisce true se è riservato, false altrimenti.
	 * 
	 * @return isReserved
	 */
	public Boolean isReserved() {
		return isReserved;
	}

	/**
	 * Imposta il flag: isReserved.
	 * 
	 * @param isReserved
	 */
	public void setReserved(final Boolean isReserved) {
		this.isReserved = isReserved;
	}

	/**
	 * Restituisce true se è una versione "frozen", false altrimenti.
	 * 
	 * @return isFrozenVersion
	 */
	public Boolean isFrozenVersion() {
		return isFrozenVersion;
	}

	/**
	 * Imposta il flag: isFrozenVersion.
	 * 
	 * @param isFrozenVersion
	 */
	public void setFrozenVersion(final Boolean isFrozenVersion) {
		this.isFrozenVersion = isFrozenVersion;
	}

	/**
	 * Restituisce l'elenco trasmissione come String.
	 * 
	 * @return elencoTrasmissione
	 */
	public String getElencoTrasmissione() {
		return elencoTrasmissione;
	}

	/**
	 * Imposta l'elenco trasmissione.
	 * 
	 * @param elencoTrasmissione
	 */
	public void setElencoTrasmissione(final String elencoTrasmissione) {
		this.elencoTrasmissione = elencoTrasmissione;
	}

	/**
	 * Restituisce il nome del file del documento.
	 * 
	 * @return nome file documento
	 */
	public String getNomeFile() {
		return nomeFile;
	}

	/**
	 * Imposta il nome del file del documento.
	 * 
	 * @param nomeFile
	 */
	public void setNomeFile(final String nomeFile) {
		this.nomeFile = nomeFile;
	}

	/**
	 * Restituisce il flag: isFirmaPDF.
	 * 
	 * @return firmaPDF
	 */
	public Boolean isFirmaPDF() {
		return firmaPDF;
	}

	/**
	 * Imposta il flag: isFirmaPDF.
	 * 
	 * @param firmaPDF
	 */
	public void setFirmaPDF(final Boolean firmaPDF) {
		this.firmaPDF = firmaPDF;
	}

	/**
	 * Restituisce il flagCorteConti.
	 * 
	 * @return flagCorteConti
	 */
	public Boolean isFlagCorteConti() {
		return flagCorteConti;
	}

	/**
	 * Imposta il flagCorteConti.
	 * 
	 * @param flagCorteConti
	 */
	public void setFlagCorteConti(final Boolean flagCorteConti) {
		this.flagCorteConti = flagCorteConti;
	}

	/**
	 * Restitusce il valore del flag: flagFirmaAutografaRM.
	 * 
	 * @return flagFirmaAutografaRM
	 */
	public Boolean isFlagFirmaAutografaRM() {
		return flagFirmaAutografaRM;
	}

	/**
	 * Imposta il valore del flag: flagFirmaAutografaRM.
	 * 
	 * @param flagFirmaAutografaRM
	 */
	public void setFlagFirmaAutografaRM(final Boolean flagFirmaAutografaRM) {
		this.flagFirmaAutografaRM = flagFirmaAutografaRM;
	}

	/**
	 * Restituisce l'id del gruppo protocollatore.
	 * 
	 * @return idGruppoProtocollatore
	 */
	public Integer getIdGruppoProtocollatore() {
		return idGruppoProtocollatore;
	}

	/**
	 * Imposta l'id del gruppo protocollatore.
	 * 
	 * @param idGruppoProtocollatore
	 */
	public void setIdGruppoProtocollatore(final Integer idGruppoProtocollatore) {
		this.idGruppoProtocollatore = idGruppoProtocollatore;
	}

	/**
	 * Restituisce l'id dell'Area Organizzativa Omogenea.
	 * 
	 * @return
	 */
	public Integer getIdAOO() {
		return idAOO;
	}

	/**
	 * Imposta l'id dell'Area Organizzativa Omogenea.
	 * 
	 * @param idAOO
	 */
	public void setIdAOO(final Integer idAOO) {
		this.idAOO = idAOO;
	}

	/**
	 * Restituisce l'indexationId.
	 * 
	 * @return indexationId
	 */
	public String getIndexationId() {
		return indexationId;
	}

	/**
	 * Imposta l'indexationId.
	 * 
	 * @param indexationId
	 */
	public void setIndexationId(final String indexationId) {
		this.indexationId = indexationId;
	}

	/**
	 * Restituisce l'id dell'entry template.
	 * 
	 * @return entryTemplateId
	 */
	public String getEntryTemplateId() {
		return entryTemplateId;
	}

	/**
	 * Imposta entryTemplateId.
	 * 
	 * @param entryTemplateId
	 */
	public void setEntryTemplateId(final String entryTemplateId) {
		this.entryTemplateId = entryTemplateId;
	}

	/**
	 * Restituisce l'id della categoria del documento.
	 * 
	 * @return idCategoriaDocumento
	 */
	public Integer getIdCategoriaDocumento() {
		return idCategoriaDocumento;
	}

	/**
	 * Imposta l'id della categoria del documento.
	 * 
	 * @param idCategoriaDocumento
	 */
	public void setIdCategoriaDocumento(final Integer idCategoriaDocumento) {
		this.idCategoriaDocumento = idCategoriaDocumento;
	}

	/**
	 * Restituisce l'id del formato documento.
	 * 
	 * @return idFormatoDocumento
	 */
	public Integer getIdFormatoDocumento() {
		return idFormatoDocumento;
	}

	/**
	 * Imposta l'id del formato documento.
	 * 
	 * @param idFormatoDocumento
	 */
	public void setIdFormatoDocumento(final Integer idFormatoDocumento) {
		this.idFormatoDocumento = idFormatoDocumento;
	}

	/**
	 * Restituisce l'id del nodo coordinatore.
	 * 
	 * @return idNodoCoordinatore
	 */
	public Long getIdNodoCoordinatore() {
		return idNodoCoordinatore;
	}

	/**
	 * Imposta l'id del nodo coordinatore.
	 * 
	 * @param idNodoCoordinatore
	 */
	public void setIdNodoCoordinatore(final Long idNodoCoordinatore) {
		this.idNodoCoordinatore = idNodoCoordinatore;
	}

	/**
	 * Restituisce l'id del nodo creatore del documento.
	 * 
	 * @return idNodoCreatore
	 */
	public Integer getIdNodoCreatore() {
		return idNodoCreatore;
	}

	/**
	 * Imposta l'id del nodo creatore del documento.
	 * 
	 * @param idNodoCreatore
	 */
	public void setIdNodoCreatore(final Integer idNodoCreatore) {
		this.idNodoCreatore = idNodoCreatore;
	}

	/**
	 * Restituisce l'id del protocollo del documento.
	 * 
	 * @return idProtocollo
	 */
	public String getIdProtocollo() {
		return idProtocollo;
	}

	/**
	 * Imposta l'id del protocollo del documento.
	 * 
	 * @param idProtocollo
	 */
	public void setIdProtocollo(final String idProtocollo) {
		this.idProtocollo = idProtocollo;
	}

	/**
	 * Restituisce l'id del template del documento.
	 * 
	 * @return idTemplate
	 */
	public Integer getIdTemplate() {
		return idTemplate;
	}

	/**
	 * Imposta l'id del template del documento.
	 * 
	 * @param idTemplate
	 */
	public void setIdTemplate(final Integer idTemplate) {
		this.idTemplate = idTemplate;
	}

	/**
	 * Restituisce l'id della tipologia del documento associata.
	 * 
	 * @return idTipologiaDocumento
	 */
	public Integer getIdTipologiaDocumento() {
		return idTipologiaDocumento;
	}

	/**
	 * Imposta l'id della tipologia del documento.
	 * 
	 * @param idTipologiaDocumento
	 */
	public void setIdTipologiaDocumento(final Integer idTipologiaDocumento) {
		this.idTipologiaDocumento = idTipologiaDocumento;
	}

	/**
	 * Restituisce l'id della tipologia del procedimento associato.
	 * 
	 * @return idTipologiaProcedimento
	 */
	public Integer getIdTipologiaProcedimento() {
		return idTipologiaProcedimento;
	}

	/**
	 * Imposta l'id della tipologia del procedimento associato.
	 * 
	 * @param idTipologiaProcedimento
	 */
	public void setIdTipologiaProcedimento(final Integer idTipologiaProcedimento) {
		this.idTipologiaProcedimento = idTipologiaProcedimento;
	}

	/**
	 * Restituisce l'id dell ufficio creatore del documento.
	 * 
	 * @return idUfficioCreatore
	 */
	public Integer getIdUfficioCreatore() {
		return idUfficioCreatore;
	}

	/**
	 * Imposta l'id dell'ufficio creatore.
	 * 
	 * @param idUfficioCreatore
	 */
	public void setIdUfficioCreatore(final Integer idUfficioCreatore) {
		this.idUfficioCreatore = idUfficioCreatore;
	}

	/**
	 * Restituisce l'id dell'utente coordinatore.
	 * 
	 * @return idUtenteCoordinatore
	 */
	public Long getIdUtenteCoordinatore() {
		return idUtenteCoordinatore;
	}

	/**
	 * Imposta l'id dell'utente coordinatore.
	 * 
	 * @param idUtenteCoordinatore
	 */
	public void setIdUtenteCoordinatore(final Long idUtenteCoordinatore) {
		this.idUtenteCoordinatore = idUtenteCoordinatore;
	}

	/**
	 * Restituisce l'id dell'utente creatore.
	 * 
	 * @return idUtenteCreatore
	 */
	public Integer getIdUtenteCreatore() {
		return idUtenteCreatore;
	}

	/**
	 * Imposta l'id dell'utente creatore.
	 * 
	 * @param idUtenteCreatore
	 */
	public void setIdUtenteCreatore(final Integer idUtenteCreatore) {
		this.idUtenteCreatore = idUtenteCreatore;
	}

	/**
	 * Restituisce il flag: ignoreRedirect.
	 * 
	 * @return ignoreRedirect
	 */
	public Boolean isIgnoreRedirect() {
		return ignoreRedirect;
	}

	/**
	 * Imposta il flag: ignoreRedirect.
	 * 
	 * @param ignoreRedirect
	 */
	public void setIgnoreRedirect(final Boolean ignoreRedirect) {
		this.ignoreRedirect = ignoreRedirect;
	}

	/**
	 * Restituisce l'attributo publicationInfo.
	 * 
	 * @return publicationInfo
	 */
	public String getPublicationInfo() {
		return publicationInfo;
	}

	/**
	 * Imposta l'attributo publicationInfo.
	 * 
	 * @param publicationInfo
	 */
	public void setPublicationInfo(final String publicationInfo) {
		this.publicationInfo = publicationInfo;
	}

	/**
	 * Restituisce true se il documento è stampigliato, false altrimenti.
	 * 
	 * @return true se il documento è stampigliato, altrimenti restituisce false
	 */
	public Integer getIsStampigliato() {
		return isStampigliato;
	}

	/**
	 * Imposta il flag che fa riferimento alla stampigliatura del documento.
	 * 
	 * @param isStampigliato
	 */
	public void setIsStampigliato(final Integer isStampigliato) {
		this.isStampigliato = isStampigliato;
	}

	/**
	 * Restituisce il semaforo dell'iter approvativo.
	 * 
	 * @return iterApprovativoSemaforo
	 */
	public String getIterApprovativoSemaforo() {
		return iterApprovativoSemaforo;
	}

	/**
	 * Imposta l'attributo: iterApprovativoSemaforo.
	 * 
	 * @param iterApprovativoSemaforo
	 */
	public void setIterApprovativoSemaforo(final String iterApprovativoSemaforo) {
		this.iterApprovativoSemaforo = iterApprovativoSemaforo;
	}

	/**
	 * Restituisce il mezzo di ricezione del documento.
	 * 
	 * @see MezzoRicezioneDTO
	 * @return mezzoRicezione
	 */
	public Integer getMezzoRicezione() {
		return mezzoRicezione;
	}

	/**
	 * Imposta il mezzo di ricezione del documento.
	 * 
	 * @param mezzoRicezione
	 */
	public void setMezzoRicezione(final Integer mezzoRicezione) {
		this.mezzoRicezione = mezzoRicezione;
	}

	/**
	 * Restituisce il mittente del documento.
	 * 
	 * @return mittente
	 */
	public String getMittente() {
		return mittente;
	}

	/**
	 * Imposta il mittente del documento.
	 * 
	 * @param mittente
	 */
	public void setMittente(final String mittente) {
		this.mittente = mittente;
	}

	/**
	 * Restituisce la motivazione dell'annullamento del documento qualora fosse
	 * annullato.
	 * 
	 * @return motivazione di annullamento se annullato, null altrimenti
	 */
	public String getMotivazioneAnnullamento() {
		return motivazioneAnnullamento;
	}

	/**
	 * Imposta la motivazione dell'annullamento del documento.
	 * 
	 * @param motivazioneAnnullamento
	 */
	public void setMotivazioneAnnullamento(final String motivazioneAnnullamento) {
		this.motivazioneAnnullamento = motivazioneAnnullamento;
	}

	/**
	 * Restituisce il nome.
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Imposta il nome.
	 * 
	 * @param name
	 */
	public void setName(final String name) {
		this.name = name;
	}

	/**
	 * @return entryTemplateObjectStoreName
	 */
	public String getEntryTemplateObjectStoreName() {
		return entryTemplateObjectStoreName;
	}

	/**
	 * @param entryTemplateObjectStoreName
	 */
	public void setEntryTemplateObjectStoreName(final String entryTemplateObjectStoreName) {
		this.entryTemplateObjectStoreName = entryTemplateObjectStoreName;
	}

	/**
	 * Restituisce le note del documento.
	 * 
	 * @return note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * Imposta le note del documento.
	 * 
	 * @param note
	 */
	public void setNote(final String note) {
		this.note = note;
	}

	/**
	 * @return entryTemplateLaunchedWorkflowNumber
	 */
	public String getEntryTemplateLaunchedWorkflowNumber() {
		return entryTemplateLaunchedWorkflowNumber;
	}

	/**
	 * @param entryTemplateLaunchedWorkflowNumber
	 */
	public void setEntryTemplateLaunchedWorkflowNumber(final String entryTemplateLaunchedWorkflowNumber) {
		this.entryTemplateLaunchedWorkflowNumber = entryTemplateLaunchedWorkflowNumber;
	}

	/**
	 * Restituisce il numero del protocollo del documento se protocollato, null
	 * altrimenti.
	 * 
	 * @return numero protocollo del documento
	 */
	public Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * Imposta il numero del protocollo.
	 * 
	 * @param numeroProtocollo
	 */
	public void setNumeroProtocollo(final Integer numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}

	/**
	 * Restituisce il protocollo mittente.
	 * 
	 * @return protocolloMittente
	 */
	public String getProtocolloMittente() {
		return protocolloMittente;
	}

	/**
	 * Imposta il protocollo mittente.
	 * 
	 * @param protocolloMittente
	 */
	public void setProtocolloMittente(final String protocolloMittente) {
		this.protocolloMittente = protocolloMittente;
	}

	/**
	 * Restituisce il numero del protocollo di risposta.
	 * 
	 * @return numeroProtocolloRisposta
	 */
	public String getNumeroProtocolloRisposta() {
		return numeroProtocolloRisposta;
	}

	/**
	 * Imposta il numero protocollo di risposta.
	 * 
	 * @param numeroProtocolloRisposta
	 */
	public void setNumeroProtocolloRisposta(final String numeroProtocolloRisposta) {
		this.numeroProtocolloRisposta = numeroProtocolloRisposta;
	}

	/**
	 * Restituisce il numero di raccomandata.
	 * 
	 * @return numeroRaccomandata
	 */
	public String getNumeroRaccomandata() {
		return numeroRaccomandata;
	}

	/**
	 * Imposta il numero di raccomandata.
	 * 
	 * @param numeroRaccomandata
	 */
	public void setNumeroRaccomandata(final String numeroRaccomandata) {
		this.numeroRaccomandata = numeroRaccomandata;
	}

	/**
	 * @return majorVersionNumber
	 */
	public Integer getMajorVersionNumber() {
		return majorVersionNumber;
	}

	/**
	 * @param majorVersionNumber
	 */
	public void setMajorVersionNumber(final Integer majorVersionNumber) {
		this.majorVersionNumber = majorVersionNumber;
	}

	/**
	 * @return minorVersionNumber
	 */
	public Integer getMinorVersionNumber() {
		return minorVersionNumber;
	}

	/**
	 * @param minorVersionNumber
	 */
	public void setMinorVersionNumber(final Integer minorVersionNumber) {
		this.minorVersionNumber = minorVersionNumber;
	}

	/**
	 * Restituisce il numero della circolare.
	 * 
	 * @return numero circolare
	 */
	public Integer getNumeroCircolare() {
		return numeroCircolare;
	}

	/**
	 * Imposta il numero della circolare.
	 * 
	 * @param numeroCircolare
	 */
	public void setNumeroCircolare(final Integer numeroCircolare) {
		this.numeroCircolare = numeroCircolare;
	}

	/**
	 * Restituisce il numero del documento.
	 * 
	 * @return numeroDocumento
	 */
	public Integer getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * Imposta il numero del documento.
	 * 
	 * @param numeroDocumento
	 */
	public void setNumeroDocumento(final Integer numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * Restituisce il numero mittente del contributo.
	 * 
	 * @return numeroMittenteContributo
	 */
	public Integer getNumeroMittenteContributo() {
		return numeroMittenteContributo;
	}

	/**
	 * Imposta il numero mittente del contributo.
	 * 
	 * @param numeroMittenteContributo
	 */
	public void setNumeroMittenteContributo(final Integer numeroMittenteContributo) {
		this.numeroMittenteContributo = numeroMittenteContributo;
	}

	/**
	 * Restituisce l'oggetto del documento.
	 * 
	 * @return oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * Imposta l'oggetto del documento.
	 * 
	 * @param oggetto
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}

	/**
	 * Restituisce l'oggetto del protocollo di NPS.
	 * 
	 * @return oggettoprotocollonps
	 */
	public String getOggettoprotocollonps() {
		return oggettoprotocollonps;
	}

	/**
	 * Imposta l'oggetto del protocollo di NPS.
	 * 
	 * @param oggettoprotocollonps
	 */
	public void setOggettoprotocollonps(final String oggettoprotocollonps) {
		this.oggettoprotocollonps = oggettoprotocollonps;
	}

	/**
	 * Restituisce la location dello storage.
	 * 
	 * @return storageLocation
	 */
	public String getStorageLocation() {
		return storageLocation;
	}

	/**
	 * Imposta la location dello storage.
	 * 
	 * @param storageLocation
	 */
	public void setStorageLocation(final String storageLocation) {
		this.storageLocation = storageLocation;
	}

	/**
	 * Restituisce l'owner del documento.
	 * 
	 * @return owner
	 */
	public String getOwner() {
		return owner;
	}

	/**
	 * Imposta l'owner del documento.
	 * 
	 * @param owner
	 */
	public void setOwner(final String owner) {
		this.owner = owner;
	}

	/**
	 * Restituisce true se è un registro riservato, false altrimenti.
	 * 
	 * @return registroRiservato
	 */
	public Boolean getRegistroRiservato() {
		return registroRiservato;
	}

	/**
	 * Imposta il flag legato al registro riservato.
	 * 
	 * @param registroRiservato
	 */
	public void setRegistroRiservato(final Boolean registroRiservato) {
		this.registroRiservato = registroRiservato;
	}

	/**
	 * @return riservato
	 */
	public Integer getRiservato() {
		return riservato;
	}

	/**
	 * @param riservato
	 */
	public void setRiservato(final Integer riservato) {
		this.riservato = riservato;
	}

	/**
	 * @return classificationStatus
	 */
	public Integer getClassificationStatus() {
		return classificationStatus;
	}

	/**
	 * @param classificationStatus
	 */
	public void setClassificationStatus(final Integer classificationStatus) {
		this.classificationStatus = classificationStatus;
	}

	/**
	 * Restituisce il current state.
	 * 
	 * @return currentState
	 */
	public String getCurrentState() {
		return currentState;
	}

	/**
	 * Imposta il current state.
	 * 
	 * @param currentState
	 */
	public void setCurrentState(final String currentState) {
		this.currentState = currentState;
	}

	/**
	 * @return compoundDocumentState
	 */
	public Integer getCompoundDocumentState() {
		return compoundDocumentState;
	}

	/**
	 * @param compoundDocumentState
	 */
	public void setCompoundDocumentState(final Integer compoundDocumentState) {
		this.compoundDocumentState = compoundDocumentState;
	}

	/**
	 * Restituisce lo status della versione del documento.
	 * 
	 * @return versionStatus
	 */
	public Integer getVersionStatus() {
		return versionStatus;
	}

	/**
	 * Imposta lo status della versione del documento.
	 * 
	 * @param versionStatus
	 */
	public void setVersionStatus(final Integer versionStatus) {
		this.versionStatus = versionStatus;
	}

	/**
	 * @return statoThreadFirma
	 */
	public Integer getStatoThreadFirma() {
		return statoThreadFirma;
	}

	/**
	 * @param statoThreadFirma
	 */
	public void setStatoThreadFirma(final Integer statoThreadFirma) {
		this.statoThreadFirma = statoThreadFirma;
	}

	/**
	 * Restituisce lo storico dell'assegnazione.
	 * 
	 * @return storicoAssegnazione
	 */
	public String getStoricoAssegnazione() {
		return storicoAssegnazione;
	}

	/**
	 * Imposta lo storico dell'assegnazione.
	 * 
	 * @param storicoAssegnazione
	 */
	public void setStoricoAssegnazione(final String storicoAssegnazione) {
		this.storicoAssegnazione = storicoAssegnazione;
	}

	/**
	 * @return lockTimeout
	 */
	public Integer getLockTimeout() {
		return lockTimeout;
	}

	/**
	 * @param lockTimeout
	 */
	public void setLockTimeout(final Integer lockTimeout) {
		this.lockTimeout = lockTimeout;
	}

	/**
	 * @return reservationType
	 */
	public Integer getReservationType() {
		return reservationType;
	}

	/**
	 * @param reservationType
	 */
	public void setReservationType(final Integer reservationType) {
		this.reservationType = reservationType;
	}

	/**
	 * Restituisce il mime type del content del documento.
	 * 
	 * @return mimeType del content
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * Imposta il mime type del content del documento.
	 * 
	 * @param mimeType
	 */
	public void setMimeType(final String mimeType) {
		this.mimeType = mimeType;
	}

	/**
	 * Restituisce la tipologia documento per NPS.
	 * 
	 * @return tipologiadocumentonps
	 */
	public String getTipologiadocumentonps() {
		return tipologiadocumentonps;
	}

	/**
	 * Imposta la tipologia documento per NPS.
	 * 
	 * @param tipologiadocumentonps
	 */
	public void setTipologiadocumentonps(final String tipologiadocumentonps) {
		this.tipologiadocumentonps = tipologiadocumentonps;
	}

	/**
	 * Restituisce il tipo protocollo.
	 * 
	 * @return tipoProtocollo
	 */
	public Integer getTipoProtocollo() {
		return tipoProtocollo;
	}

	/**
	 * Imposta il tipo protocollo.
	 * 
	 * @param tipoProtocollo
	 */
	public void setTipoProtocollo(final Integer tipoProtocollo) {
		this.tipoProtocollo = tipoProtocollo;
	}

	/**
	 * @return lockToken
	 */
	public String getLockToken() {
		return lockToken;
	}

	/**
	 * @param lockToken
	 */
	public void setLockToken(final String lockToken) {
		this.lockToken = lockToken;
	}

	/**
	 * @return trasformazionePDFInErrore
	 */
	public Integer getTrasformazionePDFInErrore() {
		return trasformazionePDFInErrore;
	}

	/**
	 * @param trasformazionePDFInErrore
	 */
	public void setTrasformazionePDFInErrore(final Integer trasformazionePDFInErrore) {
		this.trasformazionePDFInErrore = trasformazionePDFInErrore;
	}

	/**
	 * Restituisce true se urgente, false altrimenti.
	 * 
	 * @return urgente
	 */
	public boolean getUrgente() {
		return urgente;
	}

	/**
	 * Imposta il flag relativo all'urgenza del documento.
	 * 
	 * @param urgente
	 */
	public void setUrgente(final boolean urgente) {
		this.urgente = urgente;
	}

	/**
	 * Restituisce l'id dell'utente protocollatore.
	 * 
	 * @return idUtenteProtocollatore
	 */
	public Integer getIdUtenteProtocollatore() {
		return idUtenteProtocollatore;
	}

	/**
	 * Imposta l'id dell'utente protocollatore.
	 * 
	 * @param idUtenteProtocollatore
	 */
	public void setIdUtenteProtocollatore(final Integer idUtenteProtocollatore) {
		this.idUtenteProtocollatore = idUtenteProtocollatore;
	}

	/**
	 * Restituisce la validita della firma.
	 * 
	 * @return validitaFirma
	 */
	public Integer getValiditaFirma() {
		return validitaFirma;
	}

	/**
	 * Imposta la validita della firma.
	 * 
	 * @param validitaFirma
	 */
	public void setValiditaFirma(final Integer validitaFirma) {
		this.validitaFirma = validitaFirma;
	}

	/**
	 * Restituisce la versione della copia conforme.
	 * 
	 * @return versioneCopiaConforme
	 */
	public Integer getVersioneCopiaConforme() {
		return versioneCopiaConforme;
	}

	/**
	 * Imposta la versione della copia conforme.
	 * 
	 * @param versioneCopiaConforme
	 */
	public void setVersioneCopiaConforme(final Integer versioneCopiaConforme) {
		this.versioneCopiaConforme = versioneCopiaConforme;
	}

	/**
	 * Restituisce lo storico del documento come List di StoricoDTO.
	 *
	 * @return storico del documento
	 */
	public Collection<StoricoDTO> getStorico() {
		return storico;
	}

	/**
	 * Imposta lo storico del documento.
	 * 
	 * @param storico
	 */
	public void setStorico(final Collection<StoricoDTO> storico) {
		this.storico = storico;
	}

	/**
	 * Restituisce la lista dei contributi come List di ContributoDTO.
	 * 
	 * @return contributi
	 */
	public List<ContributoDTO> getContributi() {
		return contributi;
	}

	/**
	 * Imposta la lista dei contributi.
	 * 
	 * @param contributi
	 */
	public void setContributi(final List<ContributoDTO> contributi) {
		this.contributi = contributi;
	}

	/**
	 * Restituisce il guid del documento.
	 * 
	 * @return guid
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * Imposta il guid del documento.
	 *
	 * @param guid
	 */
	public void setGuid(final String guid) {
		this.guid = guid;
	}

	/**
	 * Restituisce l'ufficio mittente come Nodo.
	 * 
	 * @return ufficioMittente
	 */
	public Nodo getUfficioMittente() {
		return ufficioMittente;
	}

	/**
	 * Imposta il Nodo ufficio mittente.
	 * 
	 * @param ufficioMittente
	 */
	public void setUfficioMittente(final Nodo ufficioMittente) {
		this.ufficioMittente = ufficioMittente;
	}

	/**
	 * Restituisce la descrizione della tipologia del documento.
	 * 
	 * @return descrizione Tipologia Documento
	 */
	public String getDescTipologiaDocumento() {
		return descTipologiaDocumento;
	}

	/**
	 * Imposta la descrizione della tipologia documento.
	 * 
	 * @param descTipologiaDocumento
	 */
	public void setDescTipologiaDocumento(final String descTipologiaDocumento) {
		this.descTipologiaDocumento = descTipologiaDocumento;
	}

	/**
	 * Restituisce gli allacci come List di RispostaAllaccioDTO.
	 * 
	 * @return allacci
	 */
	public List<RispostaAllaccioDTO> getAllacci() {
		return allacci;
	}

	/**
	 * Imposta la lista degli allacci dei documenti.
	 * 
	 * @param allacci
	 */
	public void setAllacci(final List<RispostaAllaccioDTO> allacci) {
		this.allacci = allacci;
	}

	/**
	 * Restituisce il wob number selezionato.
	 * 
	 * @return wob number selezionato
	 */
	public String getWobNumberSelected() {
		return wobNumberSelected;
	}

	/**
	 * Imposta il wob number selezionato.
	 * 
	 * @param wobNumber
	 */
	public void setWobNumberSelected(final String wobNumber) {
		this.wobNumberSelected = wobNumber;
	}

	/**
	 * Restituisce la lista delle assegnazioni come List di AssegnazioneDTO.
	 * 
	 * @return assegnazioni
	 */
	public List<AssegnazioneDTO> getAssegnazioni() {
		return assegnazioni;
	}

	/**
	 * Imposta la lista delle assegnazioni.
	 * 
	 * @param assegnazioni
	 */
	public void setAssegnazioni(final List<AssegnazioneDTO> assegnazioni) {
		this.assegnazioni = assegnazioni;
	}

	/**
	 * Restituisce la lista delle versioni dei documenti.
	 * 
	 * @return versioni
	 */
	public List<VersionDTO> getVersioni() {
		return versioni;
	}

	/**
	 * Imposta le versionni dei documenti.
	 * 
	 * @param versioni
	 */
	public void setVersioni(final List<VersionDTO> versioni) {
		this.versioni = versioni;
	}

	/**
	 * Restituisce l'AOO che definisce le caratteristiche dell'Area Organizzativa
	 * Omogenea.
	 * 
	 * @return aoo
	 */
	public Aoo getAoo() {
		return aoo;
	}

	/**
	 * Imposta l'Aoo.
	 * 
	 * @param aoo
	 */
	public void setAoo(final Aoo aoo) {
		this.aoo = aoo;
	}

	/**
	 * Restitusce la lista degli allegati del documento.
	 * 
	 * @return allegati
	 */
	public List<AllegatoDTO> getAllegati() {
		return allegati;
	}

	/**
	 * Imposta la lista degli allegati come List di AllegatoDTO.
	 * 
	 * @param allegati
	 */
	public void setAllegati(final List<AllegatoDTO> allegati) {
		this.allegati = allegati;
	}

	/**
	 * Restituisce la lista dei fascicoli.
	 * 
	 * @return fascicoli
	 */
	public List<FascicoloDTO> getFascicoli() {
		return fascicoli;
	}

	/**
	 * Imposta la lista dei fascicoli.
	 * 
	 * @param fascicoli
	 */
	public void setFascicoli(final List<FascicoloDTO> fascicoli) {
		this.fascicoli = fascicoli;
	}

	/**
	 * Restituisce la lista dei faldoni.
	 * 
	 * @return faldoni
	 */
	public Collection<FaldoneDTO> getFaldoni() {
		return faldoni;
	}

	/**
	 * Imposta la lista dei faldoni.
	 * 
	 * @param faldoni
	 */
	public void setFaldoni(final Collection<FaldoneDTO> faldoni) {
		this.faldoni = faldoni;
	}

	/**
	 * Restituisce il mezzo di ricezione del documento, @see MezzoRicezioneDTO.
	 * 
	 * @return mezzoRicezione
	 */
	public MezzoRicezioneDTO getMezzoRicezioneDTO() {
		return mezzoRicezioneDTO;
	}

	/**
	 * Imposta il mezzo di ricezione del documento.
	 * 
	 * @param mezzoRicezioneDTO
	 */
	public void setMezzoRicezioneDTO(final MezzoRicezioneDTO mezzoRicezioneDTO) {
		this.mezzoRicezioneDTO = mezzoRicezioneDTO;
	}

	/**
	 * Restituisce la folder.
	 * 
	 * @return folder
	 */
	public String getFolder() {
		return folder;
	}

	/**
	 * Imposta la folder.
	 * 
	 * @param folder
	 */
	public void setFolder(final String folder) {
		this.folder = folder;
	}

	/**
	 * Restituisce il content del documento.
	 * 
	 * @return content
	 */
	public byte[] getContent() {
		return content;
	}

	/**
	 * Imposta il content del documento.
	 * 
	 * @param content
	 */
	public void setContent(final byte[] content) {
		this.content = content;
	}

	/**
	 * Restituisce la lista dei destinatari del documento come List di
	 * DestinatarioRedDTO.
	 * 
	 * @return destinatari
	 */
	public List<DestinatarioRedDTO> getDestinatari() {
		return destinatari;
	}

	/**
	 * Imposta la lista dei destinatari.
	 * 
	 * @param destinatari
	 */
	public void setDestinatari(final List<DestinatarioRedDTO> destinatari) {
		this.destinatari = destinatari;
	}

	/**
	 * Restutisce il momento protocollazione come MomentoProtocollazioneEnum.
	 * 
	 * @return momentoProtocollazioneEnum
	 */
	public MomentoProtocollazioneEnum getMomentoProtocollazioneEnum() {
		return momentoProtocollazioneEnum;
	}

	/**
	 * Imposta il momento di protocollazione del documento.
	 * 
	 * @param momentoProtocollazioneEnum
	 */
	public void setMomentoProtocollazioneEnum(final MomentoProtocollazioneEnum momentoProtocollazioneEnum) {
		this.momentoProtocollazioneEnum = momentoProtocollazioneEnum;
	}

	/**
	 * Restituisce il formato del documento. @see FormatoDocumentoEnum.
	 * 
	 * @return
	 */
	public FormatoDocumentoEnum getFormatoDocumentoEnum() {
		return formatoDocumentoEnum;
	}

	/**
	 * Imposta il formato del documento.
	 * 
	 * @param formatoDocumentoEnum
	 */
	public void setFormatoDocumentoEnum(final FormatoDocumentoEnum formatoDocumentoEnum) {
		this.formatoDocumentoEnum = formatoDocumentoEnum;
	}

	/**
	 * Restituisce la mail di spedizione.
	 * 
	 * @return mailSpedizione
	 */
	public EmailDTO getMailSpedizione() {
		return mailSpedizione;
	}

	/**
	 * Imposta la mail di spedizione del documento.
	 * 
	 * @param mailSpedizione
	 */
	public void setMailSpedizione(final EmailDTO mailSpedizione) {
		this.mailSpedizione = mailSpedizione;
	}

	/**
	 * Restituisce la lista delle approvazioni del documento.
	 * 
	 * @return approvazioni
	 */
	public List<ApprovazioneDTO> getApprovazioni() {
		return approvazioni;
	}

	/**
	 * Imposta la lista delle approvazioni.
	 * 
	 * @param approvazioni
	 */
	public void setApprovazioni(final List<ApprovazioneDTO> approvazioni) {
		this.approvazioni = approvazioni;
	}

	/**
	 * Restituisce la lista degli step dello storico del documento.
	 * 
	 * @return storicoSteps
	 */
	public List<StepDTO> getStoricoSteps() {
		return storicoSteps;
	}

	/**
	 * Imposta la lista degli steps dello storico.
	 * 
	 * @param storicoSteps
	 */
	public void setStoricoSteps(final List<StepDTO> storicoSteps) {
		this.storicoSteps = storicoSteps;
	}

	/**
	 * Restituisce il mittente contatto.
	 * 
	 * @return mittenteContatto
	 */
	public Contatto getMittenteContatto() {
		return mittenteContatto;
	}

	/**
	 * Imposta il nuovo mittente contatto.
	 */
	public void setMittenteContattoNew() {
		this.mittenteContatto = new Contatto();
	}

	/**
	 * Imposta il mittente contatto se non è null.
	 * 
	 * @param mittenteContatto
	 */
	public void setMittenteContatto(final Contatto mittenteContatto) {
		// Prima di partire con l'autocomplete viene messo un contatto vuoto, se non
		// seleziono nulla rimane salvato.
		// Questo serve a evitare quel comportamento
		if (mittenteContatto != null && (mittenteContatto.getContattoID() != null || mittenteContatto.getIdUtente() != null)) {
			this.mittenteContatto = mittenteContatto;
		}
	}

	// Questo metodo lo chiamo nel caso in cui provengo da protocollazione,
	// ho il flag univocità mail su quell'aoo e non ho trovato nessun contatto
	// con cui matcha la mail mittente
	/**
	 * Imposta il contatto mittente.
	 * 
	 * @param mittenteContatto
	 */
	public void setMittenteContattoFromMailOnTheFly(final Contatto mittenteContatto) {
		this.mittenteContatto = mittenteContatto;
	}

	/**
	 * Restituisce la data di protocollazione risposta.
	 * 
	 * @return dataProtocolloRisposta
	 */
	public Date getDataProtocolloRisposta() {
		return dataProtocolloRisposta;
	}

	/**
	 * Imposta la data di protocollazione di risposta.
	 * 
	 * @param dataProtocolloRisposta
	 */
	public void setDataProtocolloRisposta(final Date dataProtocolloRisposta) {
		this.dataProtocolloRisposta = dataProtocolloRisposta;
	}

	/**
	 * Restituisce la descrizione del fascicolo procedimentale.
	 * 
	 * @return descrizione fascicolo procedimentale
	 */
	public String getDescrizioneFascicoloProcedimentale() {
		return descrizioneFascicoloProcedimentale;
	}

	/**
	 * Imposta la descrizione del fascicolo procedimentale.
	 * 
	 * @param descrizioneFascicolo
	 */
	public void setDescrizioneFascicoloProcedimentale(final String descrizioneFascicolo) {
		this.descrizioneFascicoloProcedimentale = descrizioneFascicolo;
	}

	/**
	 * Restituisce il valore associato al flag: firma digitale RGS.
	 * 
	 * @return checkFirmaDigitaleRGS
	 */
	public Boolean getCheckFirmaDigitaleRGS() {
		return checkFirmaDigitaleRGS;
	}

	/**
	 * Imposta il valore associato al flag: firma digitale RGS.
	 * 
	 * @param checkFirmaDigitaleRGS
	 */
	public void setCheckFirmaDigitaleRGS(final Boolean checkFirmaDigitaleRGS) {
		this.checkFirmaDigitaleRGS = checkFirmaDigitaleRGS;
	}

	/**
	 * Restituisce il flag copia conforme.
	 * 
	 * @return checkFirmaConCopiaConforme
	 */
	public Boolean getCheckFirmaConCopiaConforme() {
		return checkFirmaConCopiaConforme;
	}

	/**
	 * Imposta il flag copia conforme.
	 * 
	 * @param checkFirmaConCopiaConforme
	 */
	public void setCheckFirmaConCopiaConforme(final Boolean checkFirmaConCopiaConforme) {
		this.checkFirmaConCopiaConforme = checkFirmaConCopiaConforme;
	}

	/**
	 * Restituisce la motivazione dell'assegnazione.
	 * 
	 * @return motivoAssegnazione
	 */
	public String getMotivoAssegnazione() {
		return motivoAssegnazione;
	}

	/**
	 * Imposta il motivo dell'assegnazione.
	 * 
	 * @param motivoAssegnazione
	 */
	public void setMotivoAssegnazione(final String motivoAssegnazione) {
		this.motivoAssegnazione = motivoAssegnazione;
	}

	/**
	 * Restituisce il numero RDP.
	 * 
	 * @return numeroRDP
	 */
	public Integer getNumeroRDP() {
		return numeroRDP;
	}

	/**
	 * Imposta il numero RDP.
	 * 
	 * @param numeroRDP
	 */
	public void setNumeroRDP(final Integer numeroRDP) {
		this.numeroRDP = numeroRDP;
	}

	/**
	 * Restituisce la descrizione del tipo procedimento.
	 * 
	 * @return descrizione del tipo procedimento
	 */
	public String getDescTipoProcedimento() {
		return descTipoProcedimento;
	}

	/**
	 * Imposta la descrizione del tipo procedimento.
	 * 
	 * @param descTipoProcedimento
	 */
	public void setDescTipoProcedimento(final String descTipoProcedimento) {
		this.descTipoProcedimento = descTipoProcedimento;
	}

	/**
	 * Restituisce il flag: traccia procedimento.
	 * 
	 * @return tracciaProcedimento
	 */
	public Boolean getTracciaProcedimento() {
		return tracciaProcedimento;
	}

	/**
	 * Imposta il flag: traccia procedimento.
	 * 
	 * @param tracciaProcedimento
	 */
	public void setTracciaProcedimento(final Boolean tracciaProcedimento) {
		this.tracciaProcedimento = tracciaProcedimento;
	}

	/**
	 * Restituisce il document class.
	 * 
	 * @return documentClass
	 */
	public String getDocumentClass() {
		return documentClass;
	}

	/**
	 * Imposta il document class.
	 * 
	 * @param documentClass
	 */
	public void setDocumentClass(final String documentClass) {
		this.documentClass = documentClass;
	}

	/**
	 * Restituisce l'id dell'iter approvativo.
	 * 
	 * @return idIterApprovativo
	 */
	public Integer getIdIterApprovativo() {
		return idIterApprovativo;
	}

	/**
	 * Imposat l'id dell'iter approvativo.
	 * 
	 * @param idIterApprovativo
	 */
	public void setIdIterApprovativo(final Integer idIterApprovativo) {
		this.idIterApprovativo = idIterApprovativo;
	}

	/**
	 * Restitusce il tipo assegnazione. @see TipoAssegnazioneEnum.
	 * 
	 * @return tipoAssegnazioneEnum
	 */
	public TipoAssegnazioneEnum getTipoAssegnazioneEnum() {
		return tipoAssegnazioneEnum;
	}

	/**
	 * Imposta il tipo assegnazione.
	 * 
	 * @param tipoAssegnazioneEnum
	 */
	public void setTipoAssegnazioneEnum(final TipoAssegnazioneEnum tipoAssegnazioneEnum) {
		this.tipoAssegnazioneEnum = tipoAssegnazioneEnum;
	}

	/**
	 * Restituisce true se il documento non è da protocollare, false se è da
	 * protocollare.
	 * 
	 * @return true se da protocollare, false altrimenti
	 */
	public Boolean getDaNonProtocollare() {
		return daNonProtocollare;
	}

	/**
	 * Imposta il flag: daNonProtocollare.
	 * 
	 * @param daNonProtocollare
	 */
	public void setDaNonProtocollare(final Boolean daNonProtocollare) {
		this.daNonProtocollare = daNonProtocollare;
	}

	/**
	 * Restituisce il responsabile della copia conforme.
	 * 
	 * @return responsabileCopiaConforme
	 */
	public ResponsabileCopiaConformeDTO getResponsabileCopiaConforme() {
		return responsabileCopiaConforme;
	}

	/**
	 * Imposta il responsabile della copia conforme.
	 * 
	 * @param responsabileCopiaConforme
	 */
	public void setResponsabileCopiaConforme(final ResponsabileCopiaConformeDTO responsabileCopiaConforme) {
		this.responsabileCopiaConforme = responsabileCopiaConforme;
	}

	/**
	 * Restituisce il numero di legislatura.
	 * 
	 * @return numeroLegislatura
	 */
	public Integer getNumeroLegislatura() {
		return numeroLegislatura;
	}

	/**
	 * Imposta il numero di legislatura.
	 * 
	 * @param numeroLegislatura
	 */
	public void setNumeroLegislatura(final Integer numeroLegislatura) {
		this.numeroLegislatura = numeroLegislatura;
	}

	/**
	 * Restituisce la legislatura.
	 * 
	 * @return legislatura
	 */
	public String getLegislatura() {
		return legislatura;
	}

	/**
	 * Imposta la legislatura.
	 * 
	 * @param legislatura
	 */
	public void setLegislatura(final String legislatura) {
		this.legislatura = legislatura;
	}

	/**
	 * Restituisce l'id del fascicolo procedimentale.
	 * 
	 * @return idFascicoloProcedimentale
	 */
	public String getIdFascicoloProcedimentale() {
		return idFascicoloProcedimentale;
	}

	/**
	 * Imposta l'id del fascicolo procedimentale.
	 * 
	 * @param idFascicoloProcedimentale
	 */
	public void setIdFascicoloProcedimentale(final String idFascicoloProcedimentale) {
		this.idFascicoloProcedimentale = idFascicoloProcedimentale;
		if (!StringUtils.isNullOrEmpty(idFascicoloProcedimentale) && !isInModificaAndNotPredisponi) {
			modificaFascicolo = true;
		}
	}

	/**
	 * Restitusce il prefisso del fascicolo procedimentale.
	 * 
	 * @return prefixFascicoloProcedimentale
	 */
	public String getPrefixFascicoloProcedimentale() {
		return prefixFascicoloProcedimentale;
	}

	/**
	 * Imposta il prefisso del fascicolo procedimentale.
	 * 
	 * @param prefixFascicoloProcedimentale
	 */
	public void setPrefixFascicoloProcedimentale(final String prefixFascicoloProcedimentale) {
		this.prefixFascicoloProcedimentale = prefixFascicoloProcedimentale;
	}

	/**
	 * Restituisce l'indice di classificazione del fascicolo procedimentale.
	 * 
	 * @return indiceClassificazioneFascicoloProcedimentale
	 */
	public String getIndiceClassificazioneFascicoloProcedimentale() {
		return indiceClassificazioneFascicoloProcedimentale;
	}

	/**
	 * Imposta l'indice di classificazione del fascicolo procedimentale.
	 * 
	 * @param indiceFascicoloProcedimentale
	 */
	public void setIndiceClassificazioneFascicoloProcedimentale(final String indiceFascicoloProcedimentale) {
		this.indiceClassificazioneFascicoloProcedimentale = indiceFascicoloProcedimentale;
	}

	/**
	 * Restituisce l'id raccolta FAD.
	 * 
	 * @return idraccoltaFAD
	 */
	public String getIdraccoltaFAD() {
		return idraccoltaFAD;
	}

	/**
	 * Imposta l'id raccolta FAD.
	 * 
	 * @param idraccoltaFAD
	 */
	public void setIdraccoltaFAD(final String idraccoltaFAD) {
		this.idraccoltaFAD = idraccoltaFAD;
	}

	/**
	 * Restituisce la lista dei metadati dinamici associati al documento.
	 * 
	 * @return metadatiDinamici
	 */
	public Collection<MetadatoDinamicoDTO> getMetadatiDinamici() {
		return metadatiDinamici;
	}

	/**
	 * Imposta la lista dei metadati dinamici associati al documento.
	 * 
	 * @param metadatiDinamici
	 */
	public void setMetadatiDinamici(final Collection<MetadatoDinamicoDTO> metadatiDinamici) {
		this.metadatiDinamici = metadatiDinamici;
	}

	/**
	 * Restituisce la lista dei metadati estesi associati al documento.
	 * 
	 * @return metadatiEstesi
	 */
	public Collection<MetadatoDTO> getMetadatiEstesi() {
		return metadatiEstesi;
	}

	/**
	 * Imposta la lista dei metadati estesi associati al documento.
	 * 
	 * @param metadatiEstesi
	 */
	public void setMetadatiEstesi(final Collection<MetadatoDTO> metadatiEstesi) {
		this.metadatiEstesi = metadatiEstesi;
	}

	/**
	 * Restituisce il guid protocollazione mail.
	 * 
	 * @return protocollazioneMailGuid
	 */
	public String getProtocollazioneMailGuid() {
		return protocollazioneMailGuid;
	}

	/**
	 * Imposta il guid protocollazione mail.
	 * 
	 * @param protocollazioneMailGuid
	 */
	public void setProtocollazioneMailGuid(final String protocollazioneMailGuid) {
		this.protocollazioneMailGuid = protocollazioneMailGuid;
	}

	/**
	 * Imposta l'account protocollazione mail.
	 * 
	 * @param account
	 */
	public void setProtocollazioneMailAccount(final String account) {
		this.protocollazioneMailAccount = account;
	}

	/**
	 * Restituisce l'account protocollazione mail.
	 * 
	 * @return protocollazioneMailAccount
	 */
	public String getProtocollazioneMailAccount() {
		return protocollazioneMailAccount;
	}

	/**
	 * Restituisce il DTO delle code: RecuperoCodeDTO.
	 * 
	 * @return code
	 */
	public RecuperoCodeDTO getCode() {
		return code;
	}

	/**
	 * Imposta il DTO contenente le informazioni sulle code.
	 * 
	 * @param code
	 */
	public void setCode(final RecuperoCodeDTO code) {
		this.code = code;
	}

	/**
	 * Restituisce true se è notifica, false altrimenti.
	 * 
	 * @return isNotifica
	 */
	public Boolean getIsNotifica() {
		return isNotifica;
	}

	/**
	 * Imposta il flag: isNotifica.
	 * 
	 * @param isNotifica
	 */
	public void setIsNotifica(final Boolean isNotifica) {
		this.isNotifica = isNotifica;
	}

	/**
	 * @return numDocDestIntUscita
	 */
	public Integer getNumDocDestIntUscita() {
		return numDocDestIntUscita;
	}

	/**
	 * @param numDocDestIntUscita
	 */
	public void setNumDocDestIntUscita(final Integer numDocDestIntUscita) {
		this.numDocDestIntUscita = numDocDestIntUscita;
	}

	/**
	 * Restituisce true se modificabile, false altrimenti.
	 * 
	 * @return modificabile
	 */
	public Boolean getModificabile() {
		return modificabile;
	}

	/**
	 * Imposta il flag modificabile.
	 * 
	 * @param modificabile
	 */
	public void setModificabile(final Boolean modificabile) {
		this.modificabile = modificabile;
	}

	/**
	 * Restituisce true se il content è modificabile, false altrimenti.
	 * 
	 * @return contentModificabile.
	 */
	public Boolean getContentModificabile() {
		return contentModificabile;
	}

	/**
	 * Imposta il flag contentModificabile.
	 * 
	 * @param contentModificabile
	 */
	public void setContentModificabile(final Boolean contentModificabile) {
		this.contentModificabile = contentModificabile;
	}

	/**
	 * Restituisce il wob number del documento principale.
	 * 
	 * @return wobNumberPrincipale
	 */
	public String getWobNumberPrincipale() {
		return wobNumberPrincipale;
	}

	/**
	 * Imposta il wob number del documento principale.
	 * 
	 * @param wobNumberPrincipale
	 */
	public void setWobNumberPrincipale(final String wobNumberPrincipale) {
		this.wobNumberPrincipale = wobNumberPrincipale;
	}

	/**
	 * Restituisce la descrizione del titolario del fascicolo procedimentale.
	 * 
	 * @return descrizione titolario
	 */
	public String getDescrizioneTitolarioFascicoloProcedimentale() {
		return descrizioneTitolarioFascicoloProcedimentale;
	}

	/**
	 * Imposta la descrizione del titolario del fascicolo procedimentale.
	 * 
	 * @param descrizioneTitolarioFascicoloProcedimentale
	 */
	public void setDescrizioneTitolarioFascicoloProcedimentale(final String descrizioneTitolarioFascicoloProcedimentale) {
		this.descrizioneTitolarioFascicoloProcedimentale = descrizioneTitolarioFascicoloProcedimentale;
	}

	/**
	 * @return idFilePrincipaleDaProtocolla
	 */
	public String getIdFilePrincipaleDaProtocolla() {
		return idFilePrincipaleDaProtocolla;
	}

	/**
	 * @param idFilePrincipaleDaProtocolla
	 */
	public void setIdFilePrincipaleDaProtocolla(final String idFilePrincipaleDaProtocolla) {
		this.idFilePrincipaleDaProtocolla = idFilePrincipaleDaProtocolla;
	}

	/**
	 * Restituisce l'identificativo esteso per il documento.
	 * 
	 * @return identificativo che fa riferimento a protocollo e numero documento
	 */
	public String getIdentificativoDocumentoEsteso() {
		identificativoDocumentoEsteso = Constants.EMPTY_STRING;

		if (numeroProtocollo != null) {
			identificativoDocumentoEsteso = numeroProtocollo + "/" + annoProtocollo + " - " + numeroDocumento;
		} else {
			final Calendar dateCreatedCal = Calendar.getInstance();
			dateCreatedCal.setTime(dateCreated);
			identificativoDocumentoEsteso = numeroDocumento + " - " + dateCreatedCal.get(Calendar.YEAR);
		}

		return identificativoDocumentoEsteso;
	}

	/**
	 * Restituisce true se lo storico è visibile.
	 * 
	 * @return showStoricoVisuale
	 */
	public boolean getShowStoricoVisuale() {
		return !dateCreated.before(new Date(Constants.Storico.START_DATE));
	}

	/**
	 * Restituisce la descrizione del mezzo di ricezione del documento.
	 * 
	 * @return mezzoRicezioneDescrizione
	 */
	public String getMezzoRicezioneDescrizione() {
		return mezzoRicezioneDescrizione;
	}

	/**
	 * Imposta la descrizione del mezzo di ricezione del documento.
	 * 
	 * @param mezzoRicezioneDescrizione
	 */
	public void setMezzoRicezioneDescrizione(final String mezzoRicezioneDescrizione) {
		this.mezzoRicezioneDescrizione = mezzoRicezioneDescrizione;
	}

	/**
	 * Restituisce il flag associato al button di visualizzazione dell'anteprima.
	 * 
	 * @return visualizzaAnteprimBtn
	 */
	public Boolean getVisualizzaAnteprimBtn() {
		return visualizzaAnteprimBtn;
	}

	/**
	 * Imposta il flag associato al button di visualizzazione dell'anteprima.
	 * 
	 * @param visualizzaAnteprimBtn
	 */
	public void setVisualizzaAnteprimBtn(final Boolean visualizzaAnteprimBtn) {
		this.visualizzaAnteprimBtn = visualizzaAnteprimBtn;
	}

	/**
	 * @return templateDocUscitaMap
	 */
	public Map<TemplateValueDTO, List<TemplateMetadatoValueDTO>> getTemplateDocUscitaMap() {
		return templateDocUscitaMap;
	}

	/**
	 * @param templateDocUscitaMap
	 */
	public void setTemplateDocUscitaMap(final Map<TemplateValueDTO, List<TemplateMetadatoValueDTO>> templateDocUscitaMap) {
		this.templateDocUscitaMap = templateDocUscitaMap;
	}

	/**
	 * @return contentNoteDaMailProtocolla
	 */
	public String getContentNoteDaMailProtocolla() {
		return contentNoteDaMailProtocolla;
	}

	/**
	 * @param notaDaMailProtocolla
	 */
	public void setContentNoteDaMailProtocolla(final String notaDaMailProtocolla) {
		this.contentNoteDaMailProtocolla = notaDaMailProtocolla;
	}

	/**
	 * Restituisce la lista dei guid dei cartacei acquisiti.
	 * 
	 * @return guidCartaceiAcquisiti
	 */
	public NodoDTO<String> getGuidCartaceiAcquisiti() {
		return guidCartaceiAcquisiti;
	}

	/**
	 * Imposta la lista dei guid dei cartacei acquisiti.
	 * 
	 * @param guidCartaceiAcquisiti
	 */
	public void setGuidCartaceiAcquisiti(final NodoDTO<String> guidCartaceiAcquisiti) {
		this.guidCartaceiAcquisiti = guidCartaceiAcquisiti;
	}

	/**
	 * @return contentNoteDaMailProtocollaDTO
	 */
	public NotaDTO getContentNoteDaMailProtocollaDTO() {
		return contentNoteDaMailProtocollaDTO;
	}

	/**
	 * @param contentNoteDaMailProtocollaDTO
	 */
	public void setContentNoteDaMailProtocollaDTO(final NotaDTO contentNoteDaMailProtocollaDTO) {
		this.contentNoteDaMailProtocollaDTO = contentNoteDaMailProtocollaDTO;
		if (contentNoteDaMailProtocollaDTO != null) {
			setContentNoteDaMailProtocolla(contentNoteDaMailProtocollaDTO.getContentNota());
		}
	}

	/**
	 * Restituisce l'anno del protocollo di emergenza.
	 * 
	 * @return annoProtocolloEmergenza
	 */
	public Integer getAnnoProtocolloEmergenza() {
		return annoProtocolloEmergenza;
	}

	/**
	 * Imposta l'anno del protocollo di emergenza.
	 * 
	 * @param annoProtocolloEmergenza
	 */
	public void setAnnoProtocolloEmergenza(final Integer annoProtocolloEmergenza) {
		this.annoProtocolloEmergenza = annoProtocolloEmergenza;
	}

	/**
	 * Restituisce il numero di protocollo di emergenza.
	 * 
	 * @return numeroProtocolloEmergenza
	 */
	public Integer getNumeroProtocolloEmergenza() {
		return numeroProtocolloEmergenza;
	}

	/**
	 * Imposta il numero di protocollo di emergenza.
	 * 
	 * @param numeroProtocolloEmergenza
	 */
	public void setNumeroProtocolloEmergenza(final Integer numeroProtocolloEmergenza) {
		this.numeroProtocolloEmergenza = numeroProtocolloEmergenza;
	}

	/**
	 * Restituisce la data del protocollo di emergenza.
	 * 
	 * @return dataProtocolloEmergenza
	 */
	public Date getDataProtocolloEmergenza() {
		return dataProtocolloEmergenza;
	}

	/**
	 * Imposta la data del protocollo di emergenza.
	 * 
	 * @param dataProtocolloEmergenza
	 */
	public void setDataProtocolloEmergenza(final Date dataProtocolloEmergenza) {
		this.dataProtocolloEmergenza = dataProtocolloEmergenza;
	}

	/**
	 * @return preassegnatarioInteropDTO
	 */
	public AssegnatarioInteropDTO getPreassegnatarioInteropDTO() {
		return preassegnatarioInteropDTO;
	}

	/**
	 * @param preassegnatarioInteropDTO
	 */
	public void setPreassegnatarioInteropDTO(final AssegnatarioInteropDTO preassegnatarioInteropDTO) {
		this.preassegnatarioInteropDTO = preassegnatarioInteropDTO;
	}

	/**
	 * Restituisce la data del protocollo sotto forma di String.
	 * 
	 * @return data protocollazione
	 */
	public String getDataProtocolloString() {
		return DateUtils.dateToString(dataProtocollo, true);
	}

	/**
	 * Restituisce la data del protocollo di emergenza sotto forma di String.
	 * 
	 * @return data protocollazione emergenza
	 */
	public String getDataProtocolloEmergenzaString() {
		return DateUtils.dateToString(dataProtocolloEmergenza, true);
	}

	/**
	 * Restituisce la lista dei destinatari TO, cioè destinatari principali e non in
	 * copia conoscenza.
	 * 
	 * @see ModalitaDestinatarioEnum
	 * @return destinatariTO
	 */
	public List<String> getDestinatariTO() {
		return destinatariTO;
	}

	/**
	 * Imposta la lista dei destinatari TO.
	 * 
	 * @param destinatariTO
	 */
	public void setDestinatariTO(final List<String> destinatariTO) {
		this.destinatariTO = destinatariTO;
	}

	/**
	 * Restituisce la lista dei destinatari in copia conoscenza.
	 * 
	 * @return destinatariCC
	 */
	public List<String> getDestinatariCC() {
		return destinatariCC;
	}

	/**
	 * Imposta la lista dei destinatari in copia conoscenza.
	 * 
	 * @param destinatariCC
	 */
	public void setDestinatariCC(final List<String> destinatariCC) {
		this.destinatariCC = destinatariCC;
	}

	/**
	 * Restituisce true se occorre visualizzare l'informativa relativa al registro
	 * di repertorio.
	 * 
	 * @return visualizzaInformativaRR
	 */
	public Boolean getVisualizzaInformativaRR() {
		return visualizzaInformativaRR;
	}

	/**
	 * Imposta il flag associato alla visualizzione dell'informativa sul registro
	 * repertorio RR.
	 * 
	 * @param visualizzaInformativaRR
	 */
	public void setVisualizzaInformativaRR(final Boolean visualizzaInformativaRR) {
		this.visualizzaInformativaRR = visualizzaInformativaRR;
	}

	/**
	 * @return the isModalitaRegistroRepertorio
	 */
	public Boolean getIsModalitaRegistroRepertorio() {
		return isModalitaRegistroRepertorio;
	}

	/**
	 * @param isModalitaRegistroRepertorio the isModalitaRegistroRepertorio to set
	 */
	public void setIsModalitaRegistroRepertorio(final Boolean isModalitaRegistroRepertorio) {
		this.isModalitaRegistroRepertorio = isModalitaRegistroRepertorio;
	}

	/**
	 * @return the descrizioneRegistroRepertorio
	 */
	public String getDescrizioneRegistroRepertorio() {
		return descrizioneRegistroRepertorio;
	}

	/**
	 * @param descrizioneRegistroRepertorio the descrizioneRegistroRepertorio to set
	 */
	public void setDescrizioneRegistroRepertorio(final String descrizioneRegistroRepertorio) {
		this.descrizioneRegistroRepertorio = descrizioneRegistroRepertorio;
	}

	/**
	 * @return the codiceFlusso
	 */
	public String getCodiceFlusso() {
		return codiceFlusso;
	}

	/**
	 * @param codiceFlusso the codiceFlusso to set
	 */
	public void setCodiceFlusso(final String codiceFlusso) {
		this.codiceFlusso = codiceFlusso;
	}

	/**
	 * @return decretoLiquidazioneSicoge
	 */
	public String getDecretoLiquidazioneSicoge() {
		return decretoLiquidazioneSicoge;
	}

	/**
	 * @param decretoLiquidazioneSicoge
	 */
	public void setDecretoLiquidazioneSicoge(final String decretoLiquidazioneSicoge) {
		this.decretoLiquidazioneSicoge = decretoLiquidazioneSicoge;
	}

	/**
	 * Restituisce il mittente della mail di protocollazione.
	 * 
	 * @return mittenteMailProt
	 */
	public String getMittenteMailProt() {
		return mittenteMailProt;
	}

	/**
	 * Imposta il mittente della mail di protocollazione.
	 * 
	 * @param mittenteMailProt
	 */
	public void setMittenteMailProt(final String mittenteMailProt) {
		this.mittenteMailProt = mittenteMailProt;
	}

	/**
	 * Restituisce il flag ribaltaTitolario.
	 * 
	 * @return ribaltaTitolario
	 */
	public boolean isRibaltaTitolario() {
		return ribaltaTitolario;
	}

	/**
	 * Imposta il flag ribaltaTitolario
	 * @param ribaltaTitolario
	 */
	public void setRibaltaTitolario(final boolean ribaltaTitolario) {
		this.ribaltaTitolario = ribaltaTitolario;
	}

	/**
	 * @return spedizioneMail
	 */
	public boolean getSpedizioneMail() {
		return spedizioneMail;
	}

	/**
	 * @param spedizioneMail
	 */
	public void setSpedizioneMail(final boolean spedizioneMail) {
		this.spedizioneMail = spedizioneMail;
	}

	/**
	 * Restituisce la lista dei riferimenti storici Nsd.
	 * 
	 * @return allaccioRifStorico
	 */
	public List<RiferimentoStoricoNsdDTO> getAllaccioRifStorico() {
		return allaccioRifStorico;
	}

	/**
	 * Imposta la lista dei riferimenti storici Nsd.
	 * 
	 * @param allaccioRifStorico
	 */
	public void setAllaccioRifStorico(final List<RiferimentoStoricoNsdDTO> allaccioRifStorico) {
		this.allaccioRifStorico = allaccioRifStorico;
	}

	/**
	 * @return protocolloVerificatoNsd
	 */
	public ProtocolliPregressiDTO getProtocolloVerificatoNsd() {
		return protocolloVerificatoNsd;
	}

	/**
	 * @param protocolloVerificatoNsd
	 */
	public void setProtocolloVerificatoNsd(final ProtocolliPregressiDTO protocolloVerificatoNsd) {
		this.protocolloVerificatoNsd = protocolloVerificatoNsd;
	}

	/**
	 * Restituisce il flag: riservatoAbilitato.
	 * 
	 * @return riservatoAbilitato
	 */
	public boolean isRiservatoAbilitato() {
		return riservatoAbilitato;
	}

	/**
	 * @param riservatoAbilitato
	 */
	public void setRiservatoAbilitato(final boolean riservatoAbilitato) {
		this.riservatoAbilitato = riservatoAbilitato;
	}

	/**
	 * Restituisce il flag: protocollaEMantieni.
	 * 
	 * @return protocollaEMantieni
	 */
	public boolean getProtocollaEMantieni() {
		return protocollaEMantieni;
	}

	/**
	 * @param protocollaEMantieni
	 */
	public void setProtocollaEMantieni(final boolean protocollaEMantieni) {
		this.protocollaEMantieni = protocollaEMantieni;
	}

	/**
	 * Restituisce la lista degli allegati non sbustati.
	 * 
	 * @return allegatiNonSbustati
	 */
	public List<AllegatoDTO> getAllegatiNonSbustati() {
		return allegatiNonSbustati;
	}

	/**
	 * Imposta la lista degli allegati non sbustati.
	 * 
	 * @param allegatiNonSbustati
	 */
	public void setAllegatiNonSbustati(final List<AllegatoDTO> allegatiNonSbustati) {
		this.allegatiNonSbustati = allegatiNonSbustati;
	}

	/**
	 * @return objectIdContattoMittOnTheFly
	 */
	public String getObjectIdContattoMittOnTheFly() {
		return objectIdContattoMittOnTheFly;
	}

	/**
	 * @param objectIdContattoMittOnTheFly
	 */
	public void setObjectIdContattoMittOnTheFly(final String objectIdContattoMittOnTheFly) {
		this.objectIdContattoMittOnTheFly = objectIdContattoMittOnTheFly;
	}

	/**
	 * @return objectIdContattoDestOnTheFly
	 */
	public String getObjectIdContattoDestOnTheFly() {
		return objectIdContattoDestOnTheFly;
	}

	/**
	 * @param objectIdContattoDestOnTheFly
	 */
	public void setObjectIdContattoDestOnTheFly(final String objectIdContattoDestOnTheFly) {
		this.objectIdContattoDestOnTheFly = objectIdContattoDestOnTheFly;
	}

	/**
	 * Restituisce la sotto categoria uscita del documento.
	 * 
	 * @see SottoCategoriaDocumentoUscitaEnum.
	 * @return sottoCategoria uscita del documento
	 */
	public SottoCategoriaDocumentoUscitaEnum getSottoCategoriaUscita() {
		return sottoCategoriaUscita;
	}

	/**
	 * Imposta la sotto categoria uscita del documento.
	 * 
	 * @param sottoCategoriaUscita
	 */
	public void setSottoCategoriaUscita(final SottoCategoriaDocumentoUscitaEnum sottoCategoriaUscita) {
		this.sottoCategoriaUscita = sottoCategoriaUscita;
	}

	/**
	 * Restituisce la lista dei metadati del registro ausiliario associato al
	 * documento.
	 * 
	 * @return metadatiRegistroAusiliario.
	 */
	public Collection<MetadatoDTO> getMetadatiRegistroAusiliario() {
		return metadatiRegistroAusiliario;
	}

	/**
	 * Imposta la lista dei metadati del registro ausiliario associato al documento.
	 * 
	 * @param metadatiRegistroAusiliario
	 */
	public void setMetadatiRegistroAusiliario(final Collection<MetadatoDTO> metadatiRegistroAusiliario) {
		this.metadatiRegistroAusiliario = metadatiRegistroAusiliario;
	}

	/**
	 * Restituisce l'id del registro ausiliario associato al documento.
	 * 
	 * @see RegistroAusiliarioDTO.
	 * @return id registro ausiliario.
	 */
	public Integer getIdRegistroAusiliario() {
		return idRegistroAusiliario;
	}

	/**
	 * Imposta l'id del registro ausiliario associato al documento.
	 * 
	 * @param idRegistroAusiliario
	 */
	public void setIdRegistroAusiliario(final Integer idRegistroAusiliario) {
		this.idRegistroAusiliario = idRegistroAusiliario;
	}

	/**
	 * Restituisce il protocollo di riferimento del documento.
	 * 
	 * @return protocolloRiferimento
	 */
	public String getProtocolloRiferimento() {
		return protocolloRiferimento;
	}

	/**
	 * Imposta il protocollo di riferimetno del documento.
	 * 
	 * @param protocolloRiferimento
	 */
	public void setProtocolloRiferimento(final String protocolloRiferimento) {
		this.protocolloRiferimento = protocolloRiferimento;
	}

	/**
	 * Restituisce true se il documento è un'integrazione dati, false altrimenti.
	 * 
	 * @return true se integrazione dati, false altrimenti
	 */
	public boolean getIntegrazioneDati() {
		return integrazioneDati;
	}

	/**
	 * Imposta il flag: integrazioneDati.
	 * 
	 * @param integrazioneDati
	 */
	public void setIntegrazioneDati(final boolean integrazioneDati) {
		this.integrazioneDati = integrazioneDati;
	}

	/**
	 * Restituice la registrazione ausiliaria associata al documento.
	 * 
	 * @return registrazioneAusiliaria
	 */
	public RegistrazioneAusiliariaDTO getRegistrazioneAusiliaria() {
		return registrazioneAusiliaria;
	}

	/**
	 * Restituisce il protocollo di riferimento da mostrare.
	 * 
	 * @return protRiferimentoToShow
	 */
	public String getProtRiferimentoToShow() {
		return protRiferimentoToShow;
	}

	/**
	 * Imposta il protocollo di riferimento da mostrare.
	 * 
	 * @param protRiferimentoToShow
	 */
	public void setProtRiferimentoToShow(final String protRiferimentoToShow) {
		this.protRiferimentoToShow = protRiferimentoToShow;
	}

	/**
	 * Imposta la registrazione ausiliaia associata al documetno.
	 * 
	 * @param registrazioneAusiliaria
	 */
	public void setRegistrazioneAusiliaria(final RegistrazioneAusiliariaDTO registrazioneAusiliaria) {
		this.registrazioneAusiliaria = registrazioneAusiliaria;
	}

	/**
	 * Restituisce l'identificatore processo.
	 * 
	 * @return identificatoreProcesso
	 */
	public String getIdentificatoreProcesso() {
		return identificatoreProcesso;
	}

	/**
	 * Imposta l'identificatore processo.
	 * 
	 * @param identificatoreProcesso
	 */
	public void setIdentificatoreProcesso(final String identificatoreProcesso) {
		this.identificatoreProcesso = identificatoreProcesso;
	}

	/**
	 * Restituisce la descrizone del flusso del documento.
	 * 
	 * @return descFlusso se esistente, null altrimenti
	 */
	public String getDescFlusso() {
		return descFlusso;
	}

	/**
	 * Imposta la descrizione del flusso del documento.
	 * 
	 * @param descFlusso
	 */
	public void setDescFlusso(final String descFlusso) {
		this.descFlusso = descFlusso;
	}

	/**
	 * Restituisce l'id del procedimento del documento.
	 * 
	 * @return idProcedimento
	 */
	public String getIdProcedimento() {
		return idProcedimento;
	}

	/**
	 * Imposta l'id del procedimento del documento.
	 * 
	 * @param idProcedimento
	 */
	public void setIdProcedimento(final String idProcedimento) {
		this.idProcedimento = idProcedimento;
	}

	/**
	 * @return the responsesRaw
	 */
	public String[] getResponsesRaw() {
		return responsesRaw;
	}

	/**
	 * @param responsesRaw the responsesRaw to set
	 */
	public void setResponsesRaw(final String[] responsesRaw) {
		this.responsesRaw = responsesRaw;
	}

	/**
	 * Restituisce il flag: isModificaFascicolo.
	 * 
	 * @return modificaFascicolo
	 */
	public boolean isModificaFascicolo() {
		return modificaFascicolo;
	}

	/**
	 * @param inModificaFascicolo
	 */
	public void setModificaFascicolo(final boolean inModificaFascicolo) {
		modificaFascicolo = inModificaFascicolo;
	}

	/**
	 * Restituisce true se il documento è elettronico via sistema ausiliario NPS.
	 * 
	 * @return elettronicoViaSistemaAusiliarioNPS
	 */
	public boolean isElettronicoViaSistemaAusiliarioNPS() {
		return elettronicoViaSistemaAusiliarioNPS;
	}

	/**
	 * Imposta il flag elettronicoViaSistemaAusiliarioNPS.
	 * 
	 * @param elettronicoViaSistemaAusiliarioNPS
	 */
	public void setElettronicoViaSistemaAusiliarioNPS(final boolean elettronicoViaSistemaAusiliarioNPS) {
		this.elettronicoViaSistemaAusiliarioNPS = elettronicoViaSistemaAusiliarioNPS;
	}
	
	/**
	 * Restituisce il sistema ausiliario NPS.
	 * 
	 * @return the sistemaAusiliarioNPS
	 */
	public String getSistemaAusiliarioNPS() {
		return sistemaAusiliarioNPS;
	}

	/**
	 * Imposta il sistema ausiliario NPS.
	 * 
	 * @param sistemaAusiliarioNPS the sistemaAusiliarioNPS to set
	 */
	public void setSistemaAusiliarioNPS(String sistemaAusiliarioNPS) {
		this.sistemaAusiliarioNPS = sistemaAusiliarioNPS;
	}

	/**
	 * Restituisce i contatti preferiti senza gruppi.
	 * 
	 * @return contatti preferiti non appartenenti a gruppi.
	 */
	public boolean getContattiPreferitiSenzaGruppi() {
		return contattiPreferitiSenzaGruppi;
	}

	/**
	 * Imposta i contatti preferiti non appartenti ad alcun gruppo.
	 * 
	 * @param contattiPreferitiSenzaGruppi
	 */
	public void setContattiPreferitiSenzaGruppi(final boolean contattiPreferitiSenzaGruppi) {
		this.contattiPreferitiSenzaGruppi = contattiPreferitiSenzaGruppi;
	}

	/**
	 * Restituisce il flag: fromProtocollaMail.
	 * 
	 * @return fromProtocollaMail
	 */
	public boolean getFromProtocollaMail() {
		return fromProtocollaMail;
	}

	/**
	 * Imposta il flag: fromProtocollaMail.
	 * 
	 * @param fromProtocollaMail
	 */
	public void setFromProtocollaMail(final boolean fromProtocollaMail) {
		this.fromProtocollaMail = fromProtocollaMail;
	}

	/**
	 * @return abilitaIlContattoOnTheFlyDaProt
	 */
	public boolean getAbilitaIlContattoOnTheFlyDaProt() {
		return abilitaIlContattoOnTheFlyDaProt;
	}

	/**
	 * @param abilitaIlContattoOnTheFlyDaProt
	 */
	public void setAbilitaIlContattoOnTheFlyDaProt(final boolean abilitaIlContattoOnTheFlyDaProt) {
		this.abilitaIlContattoOnTheFlyDaProt = abilitaIlContattoOnTheFlyDaProt;
	}

	/**
	 * Restituisce la dimensione del content in MegaByte.
	 * 
	 * @return sizeContent
	 */
	public float getSizeContent() {
		return sizeContent;
	}

	/**
	 * Imposta la dimensione, in MB, della dimensione del content.
	 * 
	 * @param sizeContent
	 */
	public void setSizeContent(final float sizeContent) {
		this.sizeContent = sizeContent;
	}

	/**
	 * Restituisce la dimensione massima del content.
	 * 
	 * @return max size content
	 */
	public Float getMaxSizeContent() {
		return maxSizeContent;
	}

	/**
	 * Imposta la dimensione massima del content.
	 * 
	 * @param maxSizeContent
	 */
	public void setMaxSizeContent(final Float maxSizeContent) {
		this.maxSizeContent = maxSizeContent;
	}

	/**
	 * Converte il float in ingresso interpretato come numero di byte, in numero di
	 * MB.
	 * 
	 * @param lenght
	 * @return numero di MB
	 */
	public float convertiContentInMB(final float lenght) {
		float output = 0;
		if (lenght > 0) {
			output = ((lenght / 1024) / 1024);
		}
		return output;
	}

	/**
	 * Restituisce la dimensione del documento principale in MB.
	 * 
	 * @return size documento principale
	 */
	public Float getSizeDocPrincipaleMB() {
		return sizeDocPrincipaleMB;
	}

	/**
	 * Imposta la dimensione del documento princiaple in MB.
	 * 
	 * @param sizeDocPrincipaleMB
	 */
	public void setSizeDocPrincipaleMB(final Float sizeDocPrincipaleMB) {
		this.sizeDocPrincipaleMB = sizeDocPrincipaleMB;
	}
	
	/**
	 * Restituisce true se in modifica e non in fase di predisposizione documento.
	 * 
	 * @return isInModificaAndNotPredisponi
	 */
	public boolean isInModificaAndNotPredisponi() {
		return isInModificaAndNotPredisponi;
	}

	/**
	 * Imposta il flag isInModificaAndNotPredisponi.
	 * 
	 * @param isInModificaAndNotPredisponi
	 */
	public void setInModificaAndNotPredisponi(final boolean isInModificaAndNotPredisponi) {
		this.isInModificaAndNotPredisponi = isInModificaAndNotPredisponi;
	}
	
	/**
	 * Restituisce il flag: modificaMetadatiMinimi.
	 * 
	 * @return
	 */
	public boolean isModificaMetadatiMinimi() {
		return modificaMetadatiMinimi;
	}

	/**
	 * Imposta il flag: modificaMetadatiMinimi.
	 * 
	 * @param modificaMetadatiMinimi
	 */
	public void setModificaMetadatiMinimi(final boolean modificaMetadatiMinimi) {
		this.modificaMetadatiMinimi = modificaMetadatiMinimi;
	}

	/**
	 * Restituisce la data di avvenuto download.
	 * 
	 * @return dataScarico
	 */
	public Date getDataScarico() {
		return dataScarico;
	}

	/**
	 * Imposta la data di download.
	 * 
	 * @param dataScarico
	 */
	public void setDataScarico(final Date dataScarico) {
		this.dataScarico = dataScarico;
	}

	/**
	 * Restituisce l'id del protocollo risposta.
	 * 
	 * @return idProtocolloRisposta
	 */
	public String getIdProtocolloRisposta() {
		return idProtocolloRisposta;
	}

	/**
	 * Imposta l'id del protocolla risposta.
	 * 
	 * @param idProtocolloRisposta
	 */
	public void setIdProtocolloRisposta(final String idProtocolloRisposta) {
		this.idProtocolloRisposta = idProtocolloRisposta;
	}

	/**
	 * @return haWKFAttivo
	 */
	public boolean isHaWKFAttivo() {
		return haWKFAttivo;
	}

	/**
	 * @param haWKFAttivo
	 */
	public void setHaWKFAttivo(final boolean haWKFAttivo) {
		this.haWKFAttivo = haWKFAttivo;
	}

	/**
	 * @see ValueMetadatoVerificaFirmaEnum.
	 * @return valueMetaadatoVerificaFirmaEnum
	 */
	public ValueMetadatoVerificaFirmaEnum getValueMetaadatoVerificaFirmaEnum() {
		return valueMetaadatoVerificaFirmaEnum;
	}

	/**
	 * @param valueMetaadatoVerificaFirmaEnum
	 */
	public void setValueMetaadatoVerificaFirmaEnum(final ValueMetadatoVerificaFirmaEnum valueMetaadatoVerificaFirmaEnum) {
		this.valueMetaadatoVerificaFirmaEnum = valueMetaadatoVerificaFirmaEnum;
	}

	/**
	 * Restituisce il tipo firma.
	 * 
	 * @return tipo firma
	 */
	public int getTipoFirma() {
		return tipoFirma;
	}

	/**
	 * Imposta il tipo firma.
	 * 
	 * @param tipoFirma
	 */
	public void setTipoFirma(final int tipoFirma) {
		this.tipoFirma = tipoFirma;
	}

	/**
	 * Restituisce la lista dei riferimenti NPS.
	 * @return lista riferimenti protocolli NPS
	 */
	public List<RiferimentoProtNpsDTO> getAllaccioRifNps() {
		return allaccioRifNps;
	}

	/**
	 * Imposta la lista dei riferimenti protocolli NPS.
	 * @param allaccioRifNps
	 */
	public void setAllaccioRifNps(List<RiferimentoProtNpsDTO> allaccioRifNps) {
		this.allaccioRifNps = allaccioRifNps;
	}

	/**
	 * Restituisce la lista di metadati valuta.
	 * @return metadatiValuta
	 */
	public Collection<ValutaMetadatiDTO> getMetadatiValuta() {
		return metadatiValuta;
	}

	/**
	 * Imposta la lista di metadati valuta.
	 * @param tripletteMetadatiValuta
	 */
	public void setMetadatiValuta(Collection<ValutaMetadatiDTO> tripletteMetadatiValuta) {
		this.metadatiValuta = tripletteMetadatiValuta;
	}

	/**
	 * Restituisce una mappa con chiave il codice valuta
	 * e valore il tasso di cambio.
	 * @return mapValutaCambio
	 */
	public Map<String, Double> getMapValutaCambio() {
		return mapValutaCambio;
	}

	/**
	 * Imposta la mappa <codice valuta, tasso di cambio>.
	 * @param inMapValutaCambio
	 */
	public void setMapValutaCambio(Map<String, Double> inMapValutaCambio) {
		this.mapValutaCambio = inMapValutaCambio;
	}

	/**
	 * Restituisce l'item di firma asincrona.
	 * @return dettagli dell'item firma asincrona
	 */
	public ASignItemDTO getaSignItem() {
		return aSignItem;
	}

	/**
	 * Imposta l'item di firma asincrona.
	 * @param aSignItem
	 */
	public void setaSignItem(ASignItemDTO aSignItem) {
		this.aSignItem = aSignItem;
	}
}