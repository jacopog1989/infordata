package it.ibm.red.business.service.concrete;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;
import com.siebel.customui.MEFSBLRDSCREATEFROMWSWFInput;
import com.siebel.customui.MEFSBLRDSCREATEFROMWSWFOutput;
import com.siebel.xml.mef.sbl_rds_create_from_ws.dettagliords.io.data.ALLEGATIData;
import com.siebel.xml.mef.sbl_rds_create_from_ws.dettagliords.io.data.DETTAGLIOALLEGATOData;
import com.siebel.xml.mef.sbl_rds_create_from_ws.dettagliords.io.data.DETTAGLIORDSData;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dao.IFascicoloDAO;
import it.ibm.red.business.dao.ISiebelDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dao.impl.TestoPredefinitoDAO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.EsitoCreazioneRdsDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.RdsSiebelDTO;
import it.ibm.red.business.dto.RdsSiebelDescrizioneDTO;
import it.ibm.red.business.dto.RdsSiebelGruppoDTO;
import it.ibm.red.business.dto.RdsSiebelTipologiaDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedParametriDTO;
import it.ibm.red.business.dto.TestoPredefinitoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ProvenienzaSalvaDocumentoEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.SiebelErrorEnum;
import it.ibm.red.business.enums.StatoRdSSiebelEnum;
import it.ibm.red.business.enums.TestoPredefinitoEnum;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.exception.SiebelException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.persistence.model.NodoUtenteRuolo;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.provider.WebServiceClientProvider;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.IMailSRV;
import it.ibm.red.business.service.ISalvaDocumentoSRV;
import it.ibm.red.business.service.ISiebelSRV;
import it.ibm.red.business.service.ITipologiaDocumentoSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.webservice.model.redsiebel.types.AllegatoType;

/**
 * Service che gestisce Siebel.
 */
@Service
public class SiebelSRV extends AbstractService implements ISiebelSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 417846461689650339L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SiebelSRV.class.getName());

	/**
	 * Label richiesta creazione RDS.
	 */
	private static final String RICHIESTA_CREAZIONE_RDS_LABEL = "Richiesta creazione Rds (";

	/**
	 * Messaggio errore recupero RDS aperte per il documento.
	 */
	private static final String ERROR_RECUPERO_RDS_APERTE_MSG = "Errore in fase di recupero delle RdS aperte per il documento ";

	/**
	 * Messaggio errore recupero RDS.
	 */
	private static final String ERROR_RECUPERO_RDS_MSG = "Errore in fase di recupero della RdS ";

	/**
	 * Messaggio errore aggiornamento RDS.
	 */
	private static final String ERROR_AGGIORNAMENTO_RDS_MSG = "Errore in fase di aggiornamento della RdS ";

	/**
	 * DAO.
	 */
	@Autowired
	private ISiebelDAO siebelDAO;

	/**
	 * Service.
	 */
	@Autowired
	private IMailSRV mailSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IAooSRV aooSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;

	/**
	 * Service.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private IFascicoloDAO fascicoloDAO;

	/**
	 * Service.
	 */
	@Autowired
	private ISalvaDocumentoSRV salvaDocSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ITipologiaDocumentoSRV tipologiaDocumentoSRV;

	/**
	 * Service.
	 */
	@Autowired
	private InoltraMailDaCodaAttivaSRV inoltraSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private TestoPredefinitoDAO testoPredefinitoDAO;

	/**
	 * Service.
	 */
	@Autowired
	private IFascicoloSRV fascicoloSRV;

	/**
	 * @see it.ibm.red.business.service.facade.ISiebelFacadeSRV#retrieveIdContattoSiebel(java.lang.Long).
	 */
	@Override
	public String retrieveIdContattoSiebel(final Long idNodo) {
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			return retrieveIdContattoSiebel(idNodo, conn);
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero dell'ID Contatto Siebel per l'ufficio " + idNodo, e);
			throw new RedException(e);
		} finally {
			closeConnection(conn);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ISiebelSRV#retrieveIdContattoSiebel(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public String retrieveIdContattoSiebel(final Long idNodo, final Connection connection) {
		try {
			return siebelDAO.getIdContattoSiebel(idNodo.intValue(), connection);
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero dell'ID Contatto Siebel per l'ufficio " + idNodo, e);
			throw new RedException(e);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISiebelFacadeSRV#hasRdsAperte(java.lang.String,
	 *      java.lang.Long).
	 */
	@Override
	public boolean hasRdsAperte(final String idDocumento, final Long idAOO) {
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			return siebelDAO.hasRdsAperte(idDocumento, idAOO, conn);
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_RDS_APERTE_MSG + idDocumento, e);
			throw new RedException(e);
		} finally {
			closeConnection(conn);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISiebelFacadeSRV#hasRds(java.util.Collection, java.lang.Long).
	 */
	@Override
	public Map<String, Boolean> hasRds(final Collection<String> ids, final Long idAOO) {
		Connection conn = null;

		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			return siebelDAO.hasRds(ids, idAOO, conn);
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di verifica delle RdS per i documenti in input ", e);
			throw new RedException(e);
		} finally {
			closeConnection(conn);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ISiebelSRV#hasRds(java.lang.String,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public boolean hasRds(final String idDocumento, final Long idAOO, final Connection conn) {
		try {
			final List<RdsSiebelDTO> rdsList = siebelDAO.getAllRds(idDocumento, idAOO, conn);
			return rdsList != null && !rdsList.isEmpty();
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero delle RdS per il documento " + idDocumento, e);
			throw new RedException(e);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISiebelFacadeSRV#getRdsDescrizione(java.lang.String, java.lang.Long).
	 */
	@Override
	public List<RdsSiebelDescrizioneDTO> getRdsDescrizione(final String idDocumento, final Long idAOO) {
		Connection conn = null;

		try {
			conn = setupConnection(getDataSource().getConnection(), false);

			return siebelDAO.getAllRdsDescrizione(idDocumento, idAOO, conn);
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_RDS_APERTE_MSG + idDocumento, e);
			throw new RedException(e);
		} finally {
			closeConnection(conn);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISiebelFacadeSRV#getRds(java.lang.String,
	 *      java.lang.Long).
	 */
	@Override
	public List<RdsSiebelDTO> getRds(final String idDocumento, final Long idAOO) {
		Connection conn = null;

		try {
			conn = setupConnection(getDataSource().getConnection(), false);

			return siebelDAO.getAllRds(idDocumento, idAOO, conn);
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_RDS_APERTE_MSG + idDocumento, e);
			throw new RedException(e);
		} finally {
			closeConnection(conn);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISiebelFacadeSRV#getMailDocumentoByWobNumber(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	public DocumentoAllegabileDTO getMailDocumentoByWobNumber(final UtenteDTO utente, final String wobNumber) {
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;

		try {

			fpeh = new FilenetPEHelper(utente.getFcDTO());

			final String username = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.SYSTEM_ADMIN_USERNAME_KEY);
			fceh = FilenetCEHelperProxy.newInstance(username, utente.getFcDTO().getPassword(), utente.getFcDTO().getUri(), utente.getFcDTO().getStanzaJAAS(),
					utente.getFcDTO().getObjectStore(), utente.getIdAoo());

			final VWWorkObject wo = fpeh.getWorkFlowByWob(wobNumber, true);
			final String documentTitle = TrasformerPE.getMetadato(wo, PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY).toString();

			final Document document = fceh.getDocumentByDTandAOO(documentTitle, utente.getIdAoo());
			final String nomeEmail = mailSRV.getNomeMailOriginale(document);

			return mailSRV.getMailOriginale(documentTitle, nomeEmail, utente.getIdAoo(), fceh);

		} catch (final Exception e) {
			LOGGER.warn("Errore in fase di recupero della mail originale", e);
			throw new RedException(e);
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISiebelFacadeSRV#apriRdsSiebel(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 *      java.util.List, java.lang.Integer, java.lang.String).
	 */
	@Override
	public EsitoCreazioneRdsDTO apriRdsSiebel(final UtenteDTO utente, final String wobNumber, final String emailAlternativa, final String oggetto, final String testo,
			final List<DocumentoAllegabileDTO> allegati, final Integer idTipologia, final String codiceGruppo) {

		EsitoCreazioneRdsDTO esito = null;
		Connection con = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final VWWorkObject wo = fpeh.getWorkFlowByWob(wobNumber, true);
			final String documentTitle = TrasformerPE.getMetadato(wo, PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY).toString();
			final Integer idNodoDestinatario = (Integer) TrasformerPE.getMetadato(wo, PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY);

			final String idContattoSiebel = retrieveIdContattoSiebel(idNodoDestinatario.longValue());

			final Document document = fceh.getDocumentByDTandAOO(documentTitle, utente.getIdAoo());

			if (document != null) {

				final Integer numeroProtocollo = (Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
				final Integer annoProtocollo = (Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);

				fascicoloSRV.loadContent4DocumentiAllegabili(utente, allegati);

				// apri rds
				esito = createRdS(wobNumber, numeroProtocollo, annoProtocollo, oggetto, testo, emailAlternativa, idContattoSiebel, allegati, codiceGruppo);

				if (esito.isEsito()) {

					LOGGER.info("RdS " + esito.getNumeroRds() + " Siebel creata con successo.");

					final String elencoFileInviati = getElencoNomiFileDocumento(allegati);

					// scrivi info rds su database
					scriviRdS(documentTitle, utente.getIdAoo().intValue(), esito.getNumeroRds(), StatoRdSSiebelEnum.APERTA.getStatoCode(), esito.getDataAperturaRds(),
							utente.getId().intValue(), idNodoDestinatario, oggetto, testo, emailAlternativa, elencoFileInviati, idTipologia, codiceGruppo);

					// metti documento in sospeso
					final int idUtenteDestinatario = (Integer) TrasformerPE.getMetadato(wo, PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY);

					esito = eseguiMessaInSospeso(wobNumber, esito, fpeh, wo, idNodoDestinatario, idUtenteDestinatario);
				}
			} else {

				esito = new EsitoCreazioneRdsDTO(wobNumber, false, "impossibile collegarsi a Filenet");

			}

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di registrazione della RdS su Siebel", e);
			esito = new EsitoCreazioneRdsDTO(wobNumber, false, e.getMessage());
		} finally {
			closeConnection(con);
			logoff(fpeh);
			popSubject(fceh);
		}

		return esito;

	}

	/**
	 * @param wobNumber
	 * @param esito
	 * @param fpeh
	 * @param wo
	 * @param idNodoDestinatario
	 * @param idUtenteDestinatario
	 * @return esito response
	 */
	private EsitoCreazioneRdsDTO eseguiMessaInSospeso(final String wobNumber, EsitoCreazioneRdsDTO esito, final FilenetPEHelper fpeh, final VWWorkObject wo,
			final Integer idNodoDestinatario, final int idUtenteDestinatario) {

		final HashMap<String, Object> metadati = new HashMap<>();
		try {

			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), idUtenteDestinatario);
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), idNodoDestinatario);
			fpeh.nextStep(wo, metadati, ResponsesRedEnum.METTI_IN_SOSPESO.getResponse());

		} catch (final Exception e) {
			LOGGER.warn(e);
			esito = new EsitoCreazioneRdsDTO(wobNumber, false, "impossibile spostare il documento in sospeso");
		}
		return esito;
	}

	private static EsitoCreazioneRdsDTO createRdS(final String wobNumber, final Integer numeroProtocollo, final Integer annoProtocollo, final String titolo, final String descrizione,
			final String emailAlternativa, final String idContatto, final List<DocumentoAllegabileDTO> allegati, final String codiceGruppo) {

		final String tid = DateUtils.dateToString(new Date(), "yyyyMMddHHmmss");

		LOGGER.info(RICHIESTA_CREAZIONE_RDS_LABEL + tid + "): " + numeroProtocollo + "," + annoProtocollo + "," + titolo + "," + emailAlternativa + "," + idContatto + ","
				+ allegati.size() + " allegati.");

		final DETTAGLIORDSData dettaglio = new DETTAGLIORDSData();
		dettaglio.setTID(tid);
		dettaglio.setSISTEMAESTERNO((PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.SIEBEL_RDS_WS_SISTEMA_ESTERNO)));
		dettaglio.setIDSISTEMAESTERNO1(numeroProtocollo);
		dettaglio.setIDSISTEMAESTERNO2(annoProtocollo);
		dettaglio.setTITOLO(titolo);
		dettaglio.setDESCRIZIONE(descrizione);
		dettaglio.setEMAILSISTEMAESTERNO(emailAlternativa);
		dettaglio.setIDCONTATTO(idContatto);
		dettaglio.setIDGRUPPO(codiceGruppo);

		final ALLEGATIData allegatiDaInviare = new ALLEGATIData();
		DETTAGLIOALLEGATOData allegatoDaInviare = null;
		DocumentoAllegabileDTO allegato = null;
		for (int i = 0; i < allegati.size(); i++) {
			try {
				allegato = allegati.get(i);

				final String fileName = allegato.getNomeFile();
				String nomeAllegatoSenzaEstensione = "";
				String estensioneAllegato = FilenameUtils.getExtension(fileName);
				if (StringUtils.isNullOrEmpty(estensioneAllegato)) {

					estensioneAllegato = FileUtils.getExtensionFileByMimeType(allegato.getContentType());
					nomeAllegatoSenzaEstensione = fileName;
				} else {
					nomeAllegatoSenzaEstensione = fileName.substring(0, fileName.lastIndexOf("."));
				}

				final byte[] byteArrayAllegato = org.apache.commons.io.IOUtils.toByteArray(allegato.getContenuto().getInputStream());
				final int sizeAllegato = byteArrayAllegato.length;

				allegatoDaInviare = new DETTAGLIOALLEGATOData();
				allegatoDaInviare.setALLEGATOATTACHMENTNAME(nomeAllegatoSenzaEstensione);
				allegatoDaInviare.setALLEGATOATTACHMENTEXT(estensioneAllegato);
				allegatoDaInviare.setALLEGATOATTACHMENTDATA(byteArrayAllegato);
				allegatoDaInviare.setALLEGATOATTACHMENTORIGSIZE("" + sizeAllegato);

				allegatiDaInviare.getDETTAGLIOALLEGATO().add(allegatoDaInviare);
			} catch (final IOException e) {
				LOGGER.error(RICHIESTA_CREAZIONE_RDS_LABEL + tid + "): errore in fase di gestione dell'allegato " + allegato.getNomeFile() + " da inviare", e);
				throw new RedException(RICHIESTA_CREAZIONE_RDS_LABEL + tid + "): errore in fase di gestione dell'allegato " + allegato.getNomeFile() + " da inviare", e);
			}

		}
		dettaglio.setALLEGATI(allegatiDaInviare);

		final MEFSBLRDSCREATEFROMWSWFInput request = new MEFSBLRDSCREATEFROMWSWFInput();
		request.setDETTAGLIORDS(dettaglio);

		MEFSBLRDSCREATEFROMWSWFOutput output = null;
		try {
			output = WebServiceClientProvider.getIstance().getSiebelClient().mefSBLRDSCREATEFROMWSWF(request);
		} catch (final Exception e) {
			LOGGER.error(RICHIESTA_CREAZIONE_RDS_LABEL + tid + "): errore in fase di  creazione della Rds su Siebel", e);
			throw new RedException(RICHIESTA_CREAZIONE_RDS_LABEL + tid + "): errore in fase di  creazione della Rds su Siebel", e);
		}

		if (output != null) {

			final String esito = output.getCODICEESITO();
			final String descrizioneEsito = output.getDESCRIZIONEESITO();
			final String tidResponse = output.getTID();

			LOGGER.info("Response creazione Rds (" + tid + "): (" + tidResponse + ") " + esito + " " + descrizioneEsito);

			EsitoCreazioneRdsDTO risposta = null;
			if ("0".equals(esito)) {

				final Date dataAperturaRds = DateUtils.parseDate(output.getDTAPERTURARDS());
				final String numeroRds = output.getIDSIEBEL();

				risposta = new EsitoCreazioneRdsDTO(wobNumber, true, descrizioneEsito, dataAperturaRds, numeroRds);

			} else {
				risposta = new EsitoCreazioneRdsDTO(wobNumber, false, descrizioneEsito);
			}

			return risposta;

		} else {
			throw new RedException(RICHIESTA_CREAZIONE_RDS_LABEL + tid + "): risposta assente da Siebel");
		}

	}

	private static String getElencoNomiFileDocumento(final List<DocumentoAllegabileDTO> allegati) {
		StringBuilder elencoFileRicevuti = new StringBuilder("");
		for (int i = 0; i < allegati.size(); i++) {
			elencoFileRicevuti.append(allegati.get(i).getNomeFile());
			if (i != (allegati.size() - 1)) {
				elencoFileRicevuti.append(",");
			}
		}

		if (elencoFileRicevuti.length() > 150) {
			elencoFileRicevuti = new StringBuilder(elencoFileRicevuti.substring(0, 150));
		}
		return elencoFileRicevuti.toString();
	}

	/**
	 * Scrive RDS.
	 * 
	 * @param idDocumento
	 *            id documento
	 * @param idAoo
	 *            id area organizzativa
	 * @param numeroRdS
	 *            numero RDS
	 * @param statoRdS
	 *            stato RDS
	 * @param dataAperturaRdS
	 *            data apertura RDS
	 * @param idUtente
	 *            id utente
	 * @param idNodo
	 *            id nodo
	 * @param oggetto
	 *            oggetto
	 * @param testo
	 *            testo
	 * @param emailAlternativa
	 *            email alternativa
	 * @param elencoFileInviati
	 *            elenco dei file inviati
	 * @param idTipologia
	 *            id della tipologia
	 * @param codiceGruppo
	 *            codice del gruppo
	 */
	private void scriviRdS(final String idDocumento, final Integer idAoo, final String numeroRdS, final int statoRdS, final Date dataAperturaRdS, final int idUtente,
			final int idNodo, final String oggetto, final String testo, final String emailAlternativa, final String elencoFileInviati, final Integer idTipologia,
			final String codiceGruppo) {
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);

			final RdsSiebelDTO rds = new RdsSiebelDTO(Integer.parseInt(idDocumento), idAoo, numeroRdS, statoRdS, new java.sql.Date(dataAperturaRdS.getTime()), idNodo,
					idUtente, oggetto, testo, emailAlternativa, elencoFileInviati, idTipologia, codiceGruppo);

			siebelDAO.inserisciRdS(rds, conn);

		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_RDS_APERTE_MSG + idDocumento, e);
			throw new RedException(e);
		} finally {
			closeConnection(conn);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISiebelFacadeSRV#checkInfoAggiornaRds(java.lang.String,
	 *      java.lang.Integer, java.lang.Integer).
	 */
	@Override
	public String checkInfoAggiornaRds(final String idRdsSiebel, final Integer numeroProtocolloRed, final Integer annoProtocolloRed) {

		IFilenetCEHelper fceh = null;

		try {
			final RdsSiebelDTO rdsSiebel = recuperaRdsSiebel(idRdsSiebel);

			if (rdsSiebel == null) {
				return "RdS non trovata su Red";
			} else if (rdsSiebel.getIdStatoRdsSiebel() != StatoRdSSiebelEnum.APERTA.getStatoCode()) {
				return "RdS " + StatoRdSSiebelEnum.getByCode(rdsSiebel.getIdStatoRdsSiebel()) + " su Red";
			} else {
				final Integer idDocumento = rdsSiebel.getIdDocumento();
				final Integer idAoo = rdsSiebel.getIdAoo();

				final Aoo aoo = aooSRV.recuperaAoo(idAoo);

				final String usernameKey = aoo.getCodiceAoo() + "." + PropertiesNameEnum.AOO_SISTEMA_AMMINISTRATORE.getKey();
				final String username = PropertiesProvider.getIstance().getParameterByString(usernameKey);

				// inizializza accesso al content engine in modalita administrator
				final AooFilenet aooFilenet = aoo.getAooFilenet();

				fceh = FilenetCEHelperProxy.newInstance(username, aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(), aooFilenet.getObjectStore(),
						aoo.getIdAoo());

				final Document documento = fceh.getDocumentByDTandAOO("" + idDocumento, idAoo.longValue());

				if (documento != null) {
					final Integer annoProtocollo = (Integer) TrasformerCE.getMetadato(documento, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
					final Integer numeroProtocollo = (Integer) TrasformerCE.getMetadato(documento, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);

					if (!numeroProtocollo.equals(numeroProtocolloRed) || !annoProtocollo.equals(annoProtocolloRed)) {
						return "Rds associata su Red al protocollo " + numeroProtocollo + "/" + annoProtocollo;
					}

				} else {
					return "Impossibile recuperare il documento associato alla Rds (" + idDocumento + ")";
				}
			}

		} catch (final SiebelException e) {
			throw e;
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_RDS_MSG + idRdsSiebel, e);
			throw new SiebelException(SiebelErrorEnum.DB_ACCESS_ERROR.getErrorCode(), ERROR_RECUPERO_RDS_MSG + idRdsSiebel, e);
		} finally {
			popSubject(fceh);
		}

		return null;
	}

	/**
	 * @param idRdsSiebel
	 * @return RdsSiebelDTO
	 */
	private RdsSiebelDTO recuperaRdsSiebel(final String idRdsSiebel) {

		Connection conn = null;
		RdsSiebelDTO rdsSiebel = null;

		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			rdsSiebel = siebelDAO.getRds(idRdsSiebel, conn);
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_RDS_MSG + idRdsSiebel, e);
			throw new SiebelException(SiebelErrorEnum.DB_ACCESS_ERROR.getErrorCode(), ERROR_RECUPERO_RDS_MSG + idRdsSiebel, e);
		} finally {
			closeConnection(conn);
		}
		return rdsSiebel;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISiebelFacadeSRV#aggiornaRds(java.lang.String,
	 *      java.lang.Integer, java.lang.Integer, java.util.Date, java.util.Date,
	 *      java.lang.String, java.lang.String, java.util.List).
	 */
	@Override
	public void aggiornaRds(final String idRdsSiebel, final Integer numeroProtocolloRed, final Integer annoProtocolloRed, final Date dataChiusuraRdsSiebel,
			final Date dataEvento, final String soluzione, final String tipoEvento, final List<AllegatoType> allegati) {

		Connection conn = null;

		try {

			final boolean fileRicevutiPresenti = allegati != null && !allegati.isEmpty();
			String elencoFileRicevuti = null;
			if (fileRicevutiPresenti) {
				elencoFileRicevuti = getElencoNomiFileAllegato(allegati);
			}

			conn = setupConnection(getDataSource().getConnection(), true);

			final RdsSiebelDTO rdsSiebel = siebelDAO.getRds(idRdsSiebel, conn);
			rdsSiebel.setDataChiusuraRds(new java.sql.Date(dataChiusuraRdsSiebel.getTime()));
			rdsSiebel.setDataInvioSiebel(new java.sql.Date(dataEvento.getTime()));
			rdsSiebel.setElencoFileRicevuti(elencoFileRicevuti);
			rdsSiebel.setIdStatoRdsSiebel(StatoRdSSiebelEnum.getByEvento(tipoEvento).getStatoCode());

			// Recupera i primi 2000 elementi se il numero complessivo è maggiore di 2000.
			String solutionPartition = soluzione;
			if (soluzione != null && soluzione.length() > 2000) {
				solutionPartition = soluzione.substring(0, 2000);
			}
			rdsSiebel.setSoluzione(solutionPartition);

			eseguiAggiornamentoRds(rdsSiebel, conn);
			final UtenteDTO utente = getUtenteRdsSiebel(conn, rdsSiebel.getIdUtenteAperturaRds(), rdsSiebel.getIdNodoAperturaRds());

			// aggiungi gli allegati ricevuti al fascicolo (come documenti di istruttoria)
			if (fileRicevutiPresenti) {
				aggiungiAllegatiAlFascicolo(conn, rdsSiebel.getIdDocumento(), utente, allegati, rdsSiebel.getIdRdsSiebel());
			}

			eseguiSpostamentoInLavorazione(rdsSiebel, utente);
			commitConnection(conn);

		} catch (final SiebelException e) {
			rollbackConnection(conn);
			throw e;
		} catch (final Exception e) {
			rollbackConnection(conn);
			LOGGER.error(ERROR_AGGIORNAMENTO_RDS_MSG + idRdsSiebel, e);
			throw new SiebelException(SiebelErrorEnum.GENERIC_ERROR.getErrorCode(), ERROR_AGGIORNAMENTO_RDS_MSG, e);
		} finally {
			closeConnection(conn);
		}

	}

	/**
	 * @param rdsSiebel
	 * @param conn
	 */
	private void eseguiAggiornamentoRds(final RdsSiebelDTO rdsSiebel, final Connection conn) {

		try {
			siebelDAO.aggiornaRds(rdsSiebel, conn);
		} catch (final Exception e) {
			LOGGER.error(ERROR_AGGIORNAMENTO_RDS_MSG + rdsSiebel.getIdRdsSiebel(), e);
			throw new SiebelException(SiebelErrorEnum.DB_ACCESS_ERROR.getErrorCode(), "Errore in fase di aggiornamento della RdS", e);
		}
	}

	/**
	 * Rimetti il documento in lavorazione.
	 * 
	 * @param rdsSiebel
	 * @param utente
	 */
	private void eseguiSpostamentoInLavorazione(final RdsSiebelDTO rdsSiebel, final UtenteDTO utente) {

		FilenetPEHelper fpeh = null;

		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			final VWWorkObject wo = fpeh.getWorkflowPrincipale(rdsSiebel.getIdDocumento().toString(), null);
			fpeh.nextStep(wo, null, ResponsesRedEnum.SPOSTA_IN_LAVORAZIONE.getResponse());
		} catch (final Exception e) {
			LOGGER.error("Errore durante lo spostamento in lavorazione del documento " + rdsSiebel.getIdDocumento(), e);
			throw new SiebelException(SiebelErrorEnum.PE_ERROR.getErrorCode(), "Errore durante lo spostamento in lavorazione del documento ", e);
		} finally {
			logoff(fpeh);
		}
	}

	private static String getElencoNomiFileAllegato(final List<AllegatoType> allegati) {
		StringBuilder elencoFileRicevuti = new StringBuilder("");
		for (int i = 0; i < allegati.size(); i++) {
			elencoFileRicevuti.append(allegati.get(i).getNome());
			if (i != (allegati.size() - 1)) {
				elencoFileRicevuti.append(",");
			}
		}

		if (elencoFileRicevuti.length() > 150) {
			elencoFileRicevuti = new StringBuilder(elencoFileRicevuti.substring(0, 150));
		}
		return elencoFileRicevuti.toString();
	}

	private UtenteDTO getUtenteRdsSiebel(final Connection con, final Integer idUtente, final Integer idNodo) {

		final Utente utente = utenteDAO.getUtente(idUtente.longValue(), con);
		if (utente == null) {
			LOGGER.error("Utente apertura RdS (" + idUtente + ") non valido.");
			throw new SiebelException(SiebelErrorEnum.DB_ACCESS_ERROR.getErrorCode(), "Utente apertura RdS (" + idUtente + ") non valido.");
		} else {
			// ottieni la lista di associazioni nodo-ruolo

			final NodoUtenteRuolo nodoRuolo = utenteDAO.getUtenteUfficioByIdUtenteAndIdNodo(idUtente.longValue(), idNodo.longValue(), con);

			if (nodoRuolo == null) {
				LOGGER.error("L'utente " + idUtente + " non è associato all'ufficio di apertura RdS (" + idNodo + ").");
				throw new SiebelException(SiebelErrorEnum.DB_ACCESS_ERROR.getErrorCode(), "L'utente " + idUtente + " non è associato all'ufficio di apertura RdS (" + idNodo + ").");
			} else {
				return utenteSRV.getById(nodoRuolo.getUtente().getIdUtente(), null, nodoRuolo.getNodo().getIdNodo());
			}
		}

	}

	private void aggiungiAllegatiAlFascicolo(final Connection con, final Integer idDocumento, final UtenteDTO utente, final List<AllegatoType> allegati,
			final String idRdsSiebel) {

		IFilenetCEHelper fceh = null;

		try {

			final SalvaDocumentoRedParametriDTO parametri = new SalvaDocumentoRedParametriDTO();
			parametri.setIdFascicoloSelezionato(fascicoloDAO.getIdFascicoloProcedimentale(idDocumento.toString(), utente.getIdAoo(), con).toString());

			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final DetailDocumentRedDTO detailDocInput = new DetailDocumentRedDTO();
			detailDocInput.setIdAOO(Integer.parseInt("" + utente.getIdAoo()));
			detailDocInput.setIdCategoriaDocumento(CategoriaDocumentoEnum.INTERNO.getIds()[0]);
			detailDocInput.setIdUtenteCreatore(utente.getId().intValue());
			detailDocInput.setIdUfficioCreatore(utente.getIdUfficio().intValue());
			detailDocInput.setFormatoDocumentoEnum(FormatoDocumentoEnum.ELETTRONICO);
			detailDocInput.setIdTipologiaDocumento(
					tipologiaDocumentoSRV.getTipologiaDocumentalePredefinita(utente.getIdAoo(), utente.getCodiceAoo(), TipoCategoriaEnum.ENTRATA, con));

			final String oggettoDocumentiInterni = "Rds Siebel " + idRdsSiebel + ": documento ricevuto numero ";
			int numeroDocumento = 0;
			for (final AllegatoType allegato : allegati) {

				detailDocInput.setOggetto(oggettoDocumentiInterni + (++numeroDocumento));
				detailDocInput.setNomeFile(allegato.getNome());
				detailDocInput.setContent(allegato.getData());
				detailDocInput.setMimeType(FileUtils.getMimeType(allegato.getExtension()));

				salvaDocSRV.salvaDocumento(detailDocInput, parametri, utente, ProvenienzaSalvaDocumentoEnum.GUI_RED_FASCICOLO);
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inserimento degli allegati Siebel nel fascicolo per il documento " + idDocumento, e);
			throw new SiebelException(SiebelErrorEnum.CE_ERROR.getErrorCode(), "Errore durante l'inserimento degli allegati Siebel nel fascicolo", e);
		} finally {
			popSubject(fceh);
		}

	}

	/**
	 * @see it.ibm.red.business.service.facade.ISiebelFacadeSRV#getTestiPredefiniti(it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public List<TestoPredefinitoDTO> getTestiPredefiniti(final UtenteDTO utente) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return testoPredefinitoDAO.getTestiByTipoAndAOO(TestoPredefinitoEnum.TESTO_PREDEFINITO_APRI_RDS_SIEBEL.getId(), utente.getIdAoo(), connection);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dei testi predefiniti", e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * L'oggetto di default è in comune alla response INOLTRA DA CODA MAIL SRV.
	 */
	@Override
	public String createDefaultOggetto(final MasterDocumentRedDTO master, final FascicoloDTO fascicolo, final UtenteDTO utente) {
		return inoltraSRV.createDefaultOggetto(master, fascicolo, utente);
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISiebelFacadeSRV#getDimensioneMassimaTotaleAllegatiMailBigDecimalKB().
	 */
	@Override
	public BigDecimal getDimensioneMassimaTotaleAllegatiMailBigDecimalKB() {
		try {
			final PropertiesProvider pp = PropertiesProvider.getIstance();
			final String vlaue = pp.getParameterByKey(PropertiesNameEnum.SIEBEL_APRI_RDS_ALLEGATI_MAX_DIM_TOT);
			final Integer maxDimInteger = Integer.valueOf(vlaue);
			final BigDecimal maxDimByte = BigDecimal.valueOf(maxDimInteger);
			return maxDimByte.divide(BigDecimal.valueOf(1024));
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero della proprerties: " + PropertiesNameEnum.SIEBEL_APRI_RDS_ALLEGATI_MAX_DIM_TOT.getKey(), e);
			throw new RedException(e);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISiebelFacadeSRV#getPrefissoTesto(it.ibm.red.business.dto.MasterDocumentRedDTO,
	 *      java.lang.Long).
	 */
	@Override
	public String getPrefissoTesto(final MasterDocumentRedDTO master, final Long idAOO) {
		final String documentTitle = master.getDocumentTitle();
		final Integer annoProtocollo = master.getAnnoProtocollo();
		final Integer numeroProtocollo = master.getNumeroProtocollo();
		return getPrefissoTesto(documentTitle, annoProtocollo, numeroProtocollo, idAOO);
	}

	private String getPrefissoTesto(final String documentTitle, final Integer annoProtocollo, final Integer numeroProtocollo, final Long idAOO) {
		try {
			final StringBuilder res = new StringBuilder();
			final List<RdsSiebelDTO> rdsList = getRds(documentTitle, idAOO);
			if (annoProtocollo != null && annoProtocollo > 0 && numeroProtocollo != null && numeroProtocollo > 0 && CollectionUtils.isNotEmpty(rdsList)) {
				res.append("Si riportano di seguito i riferimenti dei precedenti ID RDS generati dal prot. N° ");
				res.append(org.apache.commons.lang3.StringUtils.join(numeroProtocollo, "/", annoProtocollo, ":"));

				final List<String> idRdsSiebelList = rdsList.stream().map(RdsSiebelDTO::getIdRdsSiebel).collect(Collectors.toList());
				final String rdsListAsString = String.join("][", idRdsSiebelList);
				res.append(org.apache.commons.lang3.StringUtils.join("[", rdsListAsString, "]"));
			}

			return res.toString();
		} catch (final Exception e) {
			LOGGER.error("Impossibile recuperare il testo con lo storico", e);
			throw new RedException(e);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISiebelFacadeSRV#getProtocolloOriginale(it.ibm.red.business.dto.MasterDocumentRedDTO).
	 */
	@Override
	public String getProtocolloOriginale(final MasterDocumentRedDTO master) {
		String protocolloOriginale = "";
		final Integer annoProtocollo = master.getAnnoProtocollo();
		final Integer numeroProtocollo = master.getNumeroProtocollo();
		if (annoProtocollo != null && annoProtocollo > 0 && numeroProtocollo != null && numeroProtocollo > 0) {
			protocolloOriginale = org.apache.commons.lang3.StringUtils.join(numeroProtocollo, "/", annoProtocollo);
		}
		return protocolloOriginale;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISiebelFacadeSRV#getMailDocumentoByDocumentTitle(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	public DocumentoAllegabileDTO getMailDocumentoByDocumentTitle(final UtenteDTO utente, final String documentTitle) {
		IFilenetCEHelper fceh = null;

		try {
			final String username = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.SYSTEM_ADMIN_USERNAME_KEY);

			fceh = FilenetCEHelperProxy.newInstance(username, utente.getFcDTO().getPassword(), utente.getFcDTO().getUri(), utente.getFcDTO().getStanzaJAAS(),
					utente.getFcDTO().getObjectStore(), utente.getIdAoo());

			final Document document = fceh.getDocumentByDTandAOO(documentTitle, utente.getIdAoo());
			final String nomeEmail = mailSRV.getNomeMailOriginale(document);

			return mailSRV.getMailOriginale(documentTitle, nomeEmail, utente.getIdAoo(), fceh);

		} catch (final Exception e) {
			LOGGER.warn("Errore in fase di recupero della mail originale", e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISiebelFacadeSRV#getMappaTipiGruppiSiebel().
	 */
	@Override
	public Map<RdsSiebelTipologiaDTO, List<RdsSiebelGruppoDTO>> getMappaTipiGruppiSiebel() {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return siebelDAO.getMappaTipiGruppiSiebel(connection);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero della mappa tipi gruppi siebel", e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}
}