/**
 * @author CPIERASC
 *
 *	Questo package conterrà le classi che implementano il middleware necessario ad implementare il meccanismo di trasformazione
 *	dei workflow, in particolare avremo la factory ed i metodi di utilità per applicare la trasformazione su di uno
 *	scalare o su di un vettore (una collection di workflow).
 *
 */
package it.ibm.red.business.helper.filenet.pe.trasform;
