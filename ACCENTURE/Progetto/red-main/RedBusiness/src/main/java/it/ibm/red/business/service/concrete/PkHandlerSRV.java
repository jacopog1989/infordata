package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IPkHandlerDAO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.IPkHandlerSRV;

/**
 * 
 * @author m.crescentini
 *
 */
@Service
@Component
public class PkHandlerSRV extends AbstractService implements IPkHandlerSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 6088787481287153939L;

	/**
	 * Logger.
	 */	
	private static final REDLogger LOGGER = REDLogger.getLogger(PkHandlerSRV.class.getName());
	
	/**
	 * DAO.
	 */	
	@Autowired
	private IPkHandlerDAO pkHandlerDAO;

	/**
	 * @see it.ibm.red.business.service.facade.IPkHandlerFacadeSRV#getAllUrl().
	 */
	@Override
	public List<String> getAllUrl() {
		LOGGER.info("getAllUrl -> START");
		Connection con = null;
		List<String> pkHandlerList = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			
			pkHandlerList = pkHandlerDAO.getAllHandlers(con);
		} catch (Exception e) {
			LOGGER.error(e);
		} finally {
			closeConnection(con);
		}
		
		LOGGER.info("getAllUrl -> END.");
		return pkHandlerList;
	}
}