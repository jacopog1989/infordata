package it.ibm.red.business.dto;

import it.ibm.red.business.enums.TipoDestinatarioDatiCertEnum;

/**
 * DTO destinatario.
 */
public class DestinatarioDatiCertDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -1771760384350680752L;
	
	/**
	 * Tipo destinatario.
	 */
	private final TipoDestinatarioDatiCertEnum tipoDestinatario;
	
	/**
	 * Indirizzo mail.
	 */
	private final String indirizzoMail;

	
	/**
	 * @param tipoDestinatario
	 * @param indirizzoMail
	 */
	public DestinatarioDatiCertDTO(final TipoDestinatarioDatiCertEnum tipoDestinatario, final String indirizzoMail) {
		super();
		this.tipoDestinatario = tipoDestinatario;
		this.indirizzoMail = indirizzoMail;
	}

	/**
	 * @return the tipoDestinatario
	 */
	public TipoDestinatarioDatiCertEnum getTipoDestinatario() {
		return tipoDestinatario;
	}

	/**
	 * @return the indirizzoMail
	 */
	public String getIndirizzoMail() {
		return indirizzoMail;
	}
	

}
