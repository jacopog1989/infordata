package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.filenet.EventLogDTO;
import it.ibm.red.business.dto.filenet.StoricoDTO;
import it.ibm.red.business.enums.StatoDocumentoFuoriLibroFirmaEnum;
import it.ibm.red.business.persistence.model.DEventiCustom;

/**
 * 
 * @author CPIERASC
 *
 *	Interfaccia dao gestione storico.
 */
public interface IStoricoDAO extends Serializable {
	
	/**
	 * Metodo per il recupero dello storico.
	 * 
	 * @param idDocumento		identificativo del documento
	 * @param idAoo				identificativo aoo
	 * @param connection		connessione al db
	 * @param filenetConnection	connessione a filenet
	 * @return					storico
	 */
	Collection<StoricoDTO> getStorico(int idDocumento, Integer idAoo, Connection connection, Connection filenetConnection);

	/**
	 * Metodo per il recupero stato documento.
	 * 
	 * @param documentTitle	id documento
	 * @param idUtente		id utente
	 * @param idUfficio		id ufficio
	 * @param connection	connessione
	 * @return				stato del documento
	 */
	StatoDocumentoFuoriLibroFirmaEnum getStatoDocumentoChiuso(String documentTitle, Long idUtente, Long idUfficio, Connection connection);

	/**
	 * Recupero eventi (master) storico accorpati.
	 * 	
	 * @param filenetConnection	connessione database filenet
	 * @param documentTitle		identificativo documento
	 * @param idAoo				identificativo aoo
	 * @return					lista master eventi accorpati
	 */
	Collection<EventLogDTO> getStoricoEventAcc(Connection filenetConnection, String documentTitle, Long idAoo);

	/**
	 * Recupero eventi (detail) storico accorpati.
	 * 
	 * @param filenetConnection	connessione a filenet
	 * @param documentTitle		identificativo documento
	 * @param idAoo				identificativo aoo
	 * @param idworkflow		identificativo workflow
	 * @return					lista detail eventi accorpati
	 */
	Collection<EventLogDTO> getStoricoEventAccDett(Connection filenetConnection, String documentTitle, Long idAoo, String idworkflow);

	/**
	 * Ritorna l'oggetto DEventoCustom recuperato dal db di fileNet.
	 * 
	 * @param idDocumento
	 *  id del docuemento per il quale è associato l'evento.
	 * @param idAoo
	 *  Id aoo del documento per cui sono associati gli eventi.
	 * @param filenetConnection
	 *  connessione filenet.
	 * @return
	 *  Una lista degli eventi associati al documento.
	 */
	List<DEventiCustom> getStoricoDocumento(Integer idDocumento, Integer idAoo, Connection filenetConnection);

	/**
	 * Verifica se il documento è lavorato da ufficio.
	 * @param idAoo
	 * @param documentTitle
	 * @param idUfficio
	 * @param filenetConnection
	 * @return true o false
	 */
	boolean isDocLavoratoDaUfficio(Long idAoo, String documentTitle, Long idUfficio, Connection filenetConnection);
}
