package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.MappingOrgNsdDTO;
import it.ibm.red.business.enums.SignStrategyEnum;
import it.ibm.red.business.persistence.model.Aoo;

/**
 * Dao per la gestione delle aoo.
 * 
 * @author CPIERASC
 */
public interface IAooDAO extends Serializable {

	/**
	 * Recupero aoo per id.
	 * 
	 * @param idAoo
	 *            id
	 * @param con
	 *            connessione
	 * @return aoo
	 */
	Aoo getAoo(Long idAoo, Connection con);

	/**
	 * @param idAoo
	 * @param con
	 * @return Vis faldoni.
	 */
	String getVisFaldoni(Long idAoo, Connection con);

	/**
	 * Recupero aoo per codice.
	 * 
	 * @param codeAoo
	 *            code
	 * @param con
	 *            connessione
	 * @return aoo
	 */
	Aoo getAoo(String codeAoo, Connection con);

	/**
	 * Recupera il flag di disabilitazione della motivazione per l'operazione di
	 * Sigla.
	 * 
	 * @param idAoo
	 * @param con
	 * @return Flag che indica se effettuare lo skip sulla motivazione sigla.
	 */
	boolean getFlagSkipMotivazioneSigla(Long idAoo, Connection con);

	/**
	 * Mapping organigramma nsd
	 * 
	 * @param idUfficio
	 * @param idUtente
	 * @param idAoo
	 * @param conn
	 * @return Mapping recuperato.
	 */
	MappingOrgNsdDTO getMappingOrgNsd(Long idUfficio, Long idUtente, Long idAoo, Connection conn);

	/**
	 * Mapping organigramma ws nsd
	 * 
	 * @param idAooNsd
	 * @param codUffNsd
	 * @param codFiscaleNsd
	 * @param conn
	 * @return Mapping recuperato.
	 */
	List<MappingOrgNsdDTO> getMappingOrgWsNsd(String idAooNsd, String codUffNsd, String codFiscaleNsd, Connection conn);

	/**
	 * Metodo che consente di recuperare l'id del nodo dell'ufficio di default per
	 * una AOO.
	 * 
	 * @param idAoo
	 * @param connection
	 * @return Id nodo ufficio di default.
	 */
	Long getIdNodoUfficioDiDefault(Long idAoo, Connection connection);

	/**
	 * Decodifica AOO da NPS
	 * 
	 * @param codeAooNPS
	 * @param connection
	 * @return aoo
	 */
	Aoo getAooFromNPS(String codeAooNPS, Connection connection);

	/**
	 * Recupera flag strategia firma
	 *
	 * @param connection
	 * @param idAOO
	 * @return Strategia di firma.
	 */
	SignStrategyEnum getSignStrategy(Connection connection, Long idAOO);

}
