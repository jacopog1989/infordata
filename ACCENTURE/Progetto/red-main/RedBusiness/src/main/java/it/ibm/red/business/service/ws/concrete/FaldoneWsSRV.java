package it.ibm.red.business.service.ws.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.core.Document;

import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.FaldoneWsDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.EsitoFaldoneEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.IFaldoneSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.service.concrete.AbstractService;
import it.ibm.red.business.service.ws.IFaldoneWsSRV;
import it.ibm.red.business.service.ws.auth.DocumentServiceAuthorizable;
import it.ibm.red.webservice.model.documentservice.dto.Servizio;
import it.ibm.red.webservice.model.documentservice.types.messages.RedAssociaFascicoloFaldoneType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedCreaFaldoneType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedEliminaFaldoneType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedRimuoviAssociazioneFascicoloFaldoneType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedSpostaFascicoloFaldoneType;

/**
 * 
 * @author a.dilegge
 *
 */
@Service
public class FaldoneWsSRV extends AbstractService implements IFaldoneWsSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 6088787481287153939L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FaldoneWsSRV.class.getName());

	/**
	 * Messaggio errore disassociazione del fascicolo dal faldone.
	 */
	private static final String ERROR_DISASSOCIAZIONE_FASCICOLO_MSG = "Errore durante la disassociazione del fascicolo dal faldone: ";

	/**
	 * Service.
	 */
	@Autowired
	private IAooSRV aooSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IFaldoneSRV faldoneSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;

	/**
	 * @see it.ibm.red.business.service.ws.facade.IFaldoneWsFacadeSRV#redEliminaFaldone
	 *      (it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedEliminaFaldoneType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_ELIMINA_FALDONE)
	public void redEliminaFaldone(final RedWsClient client, final RedEliminaFaldoneType request) {
		IFilenetCEHelper fceh = null;
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final Aoo aoo = aooSRV.recuperaAoo(client.getIdAoo().intValue(), con);
			final AooFilenet aooFilenet = aoo.getAooFilenet();

			final UtenteDTO utente = utenteSRV.getAmministratoreAoo(aoo.getCodiceAoo());

			// Accesso come p8admin
			fceh = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getConnectionPoint(), aooFilenet.getObjectStore(), aooFilenet.getIdClientAoo()), aoo.getIdAoo());

			final FaldoneDTO faldone = faldoneSRV.getFaldone(request.getIdFaldone(), client.getIdAoo(), fceh);

			faldoneSRV.delete(utente, request.getIdNodo(), faldone.getAbsolutePath(), request.getIdFaldone(), fceh);

		} catch (final RedException e) {
			LOGGER.error("Si è verificato un errore durante l'eliminazione del faldone", e);
			throw e;
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'eliminazione del faldone: " + e.getMessage(), e);
			throw new RedException("Errore durante l'eliminazione del faldone: " + e.getMessage(), e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IFaldoneWsFacadeSRV#redCreaFaldone
	 *      (it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedCreaFaldoneType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_CREA_FALDONE)
	public String redCreaFaldone(final RedWsClient client, final RedCreaFaldoneType request) {
		IFilenetCEHelper fceh = null;
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final UtenteDTO utente = utenteSRV.getById(request.getIdUtente().longValue(), null, request.getIdnodo().longValue(), con);

			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			return faldoneSRV.createFaldone(utente, request.getOggettoFaldone(), request.getDescrizioneFaldone(), request.getFaldonePadreString(), false, con, fceh);

		} catch (final RedException e) {
			LOGGER.error("Si è verificato un errore durante la creazione del faldone", e);
			throw e;
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la creazione del faldone: " + e.getMessage(), e);
			throw new RedException("Errore durante la creazione del faldone: " + e.getMessage(), e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IFaldoneWsFacadeSRV#
	 *      redAssociaFascicoloFaldone
	 *      (it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedAssociaFascicoloFaldoneType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_ASSOCIA_FASCICOLO_FALDONE)
	public void redAssociaFascicoloFaldone(final RedWsClient client, final RedAssociaFascicoloFaldoneType request) {
		IFilenetCEHelper fceh = null;
		Connection con = null;

		try {

			con = setupConnection(getDataSource().getConnection(), false);

			final UtenteDTO utente = utenteSRV.getById(request.getIdUtente().longValue(), null, request.getIdnodo().longValue(), con);

			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			faldoneSRV.faldonaFascicolo(request.getNomeFaldone(), request.getIdFascicolo(), client.getIdAoo(), fceh);

		} catch (final RedException e) {
			LOGGER.error("Si è verificato un errore durante l'associazione del fascicolo al faldone", e);
			throw e;
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'associazione del fascicolo al faldone: " + e.getMessage(), e);
			throw new RedException("Errore durante l'associazione del fascicolo al faldone: " + e.getMessage(), e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IFaldoneWsFacadeSRV#
	 *      redRimuoviAssociazioneFascicoloFaldone
	 *      (it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedRimuoviAssociazioneFascicoloFaldoneType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_RIMUOVI_ASSOCIAZIONE_FASCICOLO_FALDONE)
	public void redRimuoviAssociazioneFascicoloFaldone(final RedWsClient client, final RedRimuoviAssociazioneFascicoloFaldoneType request) {
		IFilenetCEHelper fceh = null;
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final UtenteDTO utente = utenteSRV.getById(request.getIdUtente().longValue(), con);

			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final EsitoFaldoneEnum esito = faldoneSRV.disassociaFascicoloDaFaldone(request.getIdFaldone().toString(), request.getIdFascicolo().toString(), client.getIdAoo(),
					true, fceh);

			if (!EsitoFaldoneEnum.FALDONE_DISASSOCIATO_DA_FASCICOLO_CON_SUCCESSO.equals(esito)) {
				throw new RedException(ERROR_DISASSOCIAZIONE_FASCICOLO_MSG + esito.getText());
			}
		} catch (final RedException e) {
			LOGGER.error("Si è verificato un errore durante la disassociazione del fascicolo dal faldone", e);
			throw e;
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la disassociazione del fascicolo dal faldone: " + e.getMessage(), e);
			throw new RedException(ERROR_DISASSOCIAZIONE_FASCICOLO_MSG + e.getMessage(), e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IFaldoneWsFacadeSRV#
	 *      redSpostaFascicoloFaldone
	 *      (it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedSpostaFascicoloFaldoneType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_SPOSTA_FASCICOLO_FALDONE)
	public void redSpostaFascicoloFaldone(final RedWsClient client, final RedSpostaFascicoloFaldoneType request) {
		IFilenetCEHelper fceh = null;
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final UtenteDTO utente = utenteSRV.getById(request.getIdUtente().longValue(), con);

			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			faldoneSRV.faldonaFascicolo(request.getIdFaldoneNew().toString(), request.getIdFascicolo().toString(), client.getIdAoo(), fceh);

			final EsitoFaldoneEnum esito = faldoneSRV.disassociaFascicoloDaFaldone(request.getIdFaldoneOld().toString(), request.getIdFascicolo().toString(),
					client.getIdAoo(), true, fceh);

			if (!EsitoFaldoneEnum.FALDONE_DISASSOCIATO_DA_FASCICOLO_CON_SUCCESSO.equals(esito)) {

				faldoneSRV.disassociaFascicoloDaFaldone(request.getIdFaldoneNew().toString(), request.getIdFascicolo().toString(), client.getIdAoo(), fceh);

				throw new RedException(ERROR_DISASSOCIAZIONE_FASCICOLO_MSG + esito.getText());
			}
		} catch (final RedException e) {
			LOGGER.error("Si è verificato un errore durante lo spostamento del fascicolo da un faldone ad un altro", e);
			throw e;
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante lo spostamento del fascicolo da un faldone ad un altro: " + e.getMessage(), e);
			throw new RedException("Errore durante lo spostamento del fascicolo da un faldone ad un altro: " + e.getMessage(), e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}

	}

	/**
	 * @see it.ibm.red.business.service.ws.IFaldoneWsSRV#transformToFaldoniWs(com.filenet
	 *      .api.collection.DocumentSet).
	 */
	@Override
	public List<FaldoneWsDTO> transformToFaldoniWs(final DocumentSet faldoniFilenet) {
		List<FaldoneWsDTO> faldoniWsList = null;

		if (faldoniFilenet != null && !faldoniFilenet.isEmpty()) {
			faldoniWsList = new ArrayList<>();

			FaldoneWsDTO faldoneWs = null;
			final Iterator<?> itFaldoniFilenet = faldoniFilenet.iterator();
			while (itFaldoniFilenet.hasNext()) {
				final Document faldoneFilenet = (Document) itFaldoniFilenet.next();

				final String nomeFaldone = (String) TrasformerCE.getMetadato(faldoneFilenet, PropertiesNameEnum.METADATO_FALDONE_NOMEFALDONE);
				final String oggetto = (String) TrasformerCE.getMetadato(faldoneFilenet, PropertiesNameEnum.METADATO_FALDONE_OGGETTO);
				final String descrizione = (String) TrasformerCE.getMetadato(faldoneFilenet, PropertiesNameEnum.METADATO_FALDONE_DESCRIZIONEFALDONE);
				final String parentFaldone = (String) TrasformerCE.getMetadato(faldoneFilenet, PropertiesNameEnum.METADATO_FALDONE_PARENTFALDONE);

				faldoneWs = new FaldoneWsDTO(nomeFaldone, oggetto, descrizione, parentFaldone, faldoneFilenet.getClassName(), null, null);

				faldoniWsList.add(faldoneWs);
			}
		}

		return faldoniWsList;
	}

}