package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IOrganigrammaDAO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.enums.OrganigrammaVisualizzazioneEnum;
import it.ibm.red.business.enums.TipoStrutturaNodoEnum;
import it.ibm.red.business.enums.TipologiaNodoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * DAO gestione organigramma.
 */
@Repository
public class OrganigrammaDAO extends AbstractDAO implements IOrganigrammaDAO {

	private static final String USERNAME = "USERNAME";

	private static final String IDUTENTE = "IDUTENTE";

	private static final String IDNODO_COLUMN = "IDNODO";

	private static final String IDAOO = "IDAOO";

	private static final String EMAIL = "EMAIL";

	private static final String DESCRIZIONE = "DESCRIZIONE";

	private static final String COGNOME = "COGNOME";

	private static final String CODICENODO = "CODICENODO";

	private static final String CODFIS = "CODFIS";

	private static final String ID_NODO = "\nidNodo: ";

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 2249343884039299285L;
	
	/**
	 * Messaggio di errore recupero utente dirigente per l'ufficio. Occorre appendere l'id dell'ufficio al mittente.
	 */
	private static final String ERROR_RECUPERO_UTENTE_DIRIGENTE_MSG = "Errore durante il recupero dell'Utente Dirigente per l'ufficio con id: ";

	/**
	 * Tipo vsualizzazione.
	 */
	private static final String VISUALIZZAZIONE0 = "VISUALIZZAZIONE0";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(OrganigrammaDAO.class);

	/**
	 * @see it.ibm.red.business.dao.IOrganigrammaDAO#getFigliByNodo(java.lang.Long,
	 *      java.lang.Long, java.lang.String, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getFigliByNodo(final Long idNodo, final Long idAOO, final String vInput,
			final Long idNodoUtente, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<NodoOrganigrammaDTO> list = null;
		try {
			int index = 1;

			final OrganigrammaVisualizzazioneEnum vEnum = retrieveNomeColonna(idNodoUtente, vInput, connection);

			final String querySQL = "SELECT u.idnodo, u.codicenodo, " + "       u.idaoo, " + "       Decode(LEVEL, 1, 0, nod."
					+ vEnum.toString() + ") AS idnodopadre, " + "       u.descrizione, " + "       u.idtiponodo, "
					+ "       u.dataattivazione, " + "       u.datadisattivazione, " + "       u.idtipostruttura, "
					+ "       u.flagsegreteria, " + "       nod.ORDINAMENTO, " + "       (select count(*) "
					+ "          from nodo_organigramma nod_count, nodo n " + "         where nod_count."
					+ vEnum.toString() + " = u.idnodo " + "           and nod_count.IDNODO_ORG = n.IDNODO "
					+ "           and n.idnodocorriere > 0 " + "           AND n.datadisattivazione IS NULL) figli, "
					+ "       u.tipovisualizzazione, " + "       nod.* " + "  FROM nodo u, nodo_organigramma nod "
					+ " WHERE u.datadisattivazione IS NULL " + "   AND nod." + vEnum.toString() + " = ? "
					+ "   AND u.idnodocorriere > 0 " + "   AND u.idnodo = nod.idnodo_org "
					+ "CONNECT BY PRIOR u.idnodo = nod." + vEnum.toString() + " "
					+ " START WITH u.idnodo = (SELECT idnodoradice FROM aoo WHERE idaoo = ?) "
					+ " ORDER SIBLINGS BY nod.ordinamento";
			ps = connection.prepareStatement(querySQL);
			LOGGER.info("****ORGANIGRAMMA QUERY (getFigliByNodo): " + querySQL + ID_NODO + idNodo + "\nidAOO: " + idAOO);
			ps.setLong(index++, idNodo);
			ps.setLong(index++, idAOO);

			rs = ps.executeQuery();

			list = new ArrayList<>();
			while (rs.next()) {
				list.add(new NodoOrganigrammaDTO(rs.getLong(IDNODO_COLUMN), rs.getLong(IDAOO), rs.getLong("IDNODOPADRE"),
						rs.getString(CODICENODO), rs.getString(DESCRIZIONE), TipologiaNodoEnum.get(rs.getInt("IDTIPONODO")),
						TipoStrutturaNodoEnum.get(rs.getInt("IDTIPOSTRUTTURA")), rs.getInt("FLAGSEGRETERIA") == 1,
						rs.getInt("ORDINAMENTO"), rs.getInt("FIGLI"), rs.getLong("IDNODO_ORG"),
						rs.getLong("IDNODOPADRE_ORG"), rs.getLong("IDAOO_ORG"), rs.getLong(VISUALIZZAZIONE0),
						rs.getLong("VISUALIZZAZIONE1"), rs.getLong("VISUALIZZAZIONE2"),
						rs.getLong("VISUALIZZAZIONE3")));
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage(), e);
		} finally {
			closeStatement(ps, rs);
		}
		return list;
	}

	private static String getVisualizzazioneByIdNodo(final Long idNodo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String result = null;
		try {
			int index = 1;
			final String querySQL = "select tipovisualizzazione from nodo where idnodo = ?";

			ps = connection.prepareStatement(querySQL);
			ps.setLong(index++, idNodo);

			rs = ps.executeQuery();

			if (rs.next()) {
				result = rs.getString("tipovisualizzazione");
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage(), e);
		} finally {
			closeStatement(ps, rs);
		}
		return result;
	}

	/**
	 * @see it.ibm.red.business.dao.IOrganigrammaDAO#getUtentiByNodo(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getUtentiByNodo(final boolean consideraDisattivi,final Long idNodo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<NodoOrganigrammaDTO> list = null;
		try {
			int index = 1;

			 String querySQL = " SELECT DISTINCT ute.idutente, " + "                ute.nome, "
					+ "                ute.cognome, " + "                ute.username, "
					+ "                ute.dataattivazione, " + "                ute.datadisattivazione, "
					+ "                ute.email, " + "                ute.codfis, "
					+ "                ute.registroriservato, " + "                nod.idnodo, nod.codicenodo, "
					+ "                nod.idaoo, " + "                nod.descrizione "
					+ "  FROM utente ute, nodoutenteruolo nur, nodo nod, ruolo ru, aoo "
					+ " WHERE nur.idruolo = ru.idruolo " + "   AND nur.idutente = ute.idutente "
					+ "   AND nur.idnodo = nod.idnodo " + "   AND aoo.idaoo = nod.idaoo " + "   AND nod.idnodo = ? ";
					if(!consideraDisattivi) {
						querySQL+= "   AND nod.datadisattivazione IS NULL " + "   AND ru.datadisattivazione IS NULL "
								+ "   AND nur.datadisattivazione IS NULL " + "   AND ute.datadisattivazione IS NULL ";
					}
					querySQL+= "   AND nur.gestioneapplicativa in (0, 1) " 
					+ " ORDER BY ute.cognome";
			ps = connection.prepareStatement(querySQL);
			ps.setLong(index++, idNodo);
			LOGGER.info("****ORGANIGRAMMA QUERY (getUtentiByNodo): " + querySQL + ID_NODO + idNodo);
			rs = ps.executeQuery();

			list = new ArrayList<>();
			NodoOrganigrammaDTO n = null;
			while (rs.next()) {
				n = new NodoOrganigrammaDTO(rs.getLong(IDNODO_COLUMN), rs.getLong(IDAOO), rs.getString(CODICENODO), rs.getString(DESCRIZIONE),
						rs.getLong(IDUTENTE), rs.getString("NOME"), rs.getString(COGNOME), rs.getString(USERNAME),
						rs.getString(EMAIL), rs.getString(CODFIS), TipologiaNodoEnum.UTENTE);

				/* prendo i permessi per ogni possibile ruolo dell'utente */
				n.setPermessi(getPermessiByUtenteNodoAoo(n.getIdAOO(), n.getIdNodo(), n.getIdUtente(), connection));

				list.add(n);
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage(), e);
		} finally {
			closeStatement(ps, rs);
		}
		return list;
	}

	private static List<Long> getPermessiByUtenteNodoAoo(final Long idAoo, final Long idNodo, final Long idUtente, final Connection connection)
			throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			List<Long> results = null;
			int index = 1;

			final String querySQL = " select r.permessi " 
					+ "  from nodoutenteruolo nur, ruolo r "
					+ " where r.idruolo = nur.idruolo " 
					+ "   and r.idaoo = ? " 
					+ "   and nur.idnodo = ? "
					+ "   and nur.idutente = ? "
					+ "   and r.datadisattivazione is null " 
					+ "   and nur.datadisattivazione is null ";
			ps = connection.prepareStatement(querySQL);
			ps.setLong(index++, idAoo);
			ps.setLong(index++, idNodo);
			ps.setLong(index++, idUtente);

			rs = ps.executeQuery();
			results = new ArrayList<>();
			while (rs.next()) {
				results.add(rs.getLong("PERMESSI"));
			}

			return results;
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IOrganigrammaDAO#getSottoNodi(java.lang.Long,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<UfficioDTO> getSottoNodi(final Long idUfficio, final Long idAOO, final Connection con) {
		final List<UfficioDTO> output = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			// String sql = "SELECT idnodo, idaoo, Decode(LEVEL, 1, 0, idnodopadre) AS
			// idnodopadre,"
			// + "descrizione, idtipostruttura, idtiponodo, dataattivazione,
			// datadisattivazione, idnododirezione, idnodocorriere,"
			// + "(select count(*) from nodo u_count where u_count.idnodopadre = u.idnodo)
			// figli"
			final String sql = "SELECT idnodo, descrizione " + "FROM nodo u WHERE datadisattivazione IS NULL "
					+ "AND idaoo = idaoo " + "AND idnodopadre = ? " + "AND idnodocorriere > 0 "
					+ "CONNECT BY PRIOR idnodo = idnodopadre "
					+ "START WITH idnodo = (SELECT idnodoradice FROM aoo WHERE idaoo = ?) "
					+ "ORDER SIBLINGS BY ordinamento";
			ps = con.prepareStatement(sql);
			LOGGER.info("****ORGANIGRAMMA QUERY (getFigliByNodo): " + sql + ID_NODO + idUfficio + "\nidAOO: " + idAOO);
			ps.setInt(1, idUfficio.intValue());
			ps.setInt(2, idAOO.intValue());
			rs = ps.executeQuery();
			while (rs.next()) {
				final Integer idNodo = rs.getInt("idnodo");
				output.add(new UfficioDTO(idNodo.longValue(), rs.getString("descrizione")));
			}
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}

	private static OrganigrammaVisualizzazioneEnum retrieveNomeColonna(final Long idNodo, final String hint, final Connection connection) {
		OrganigrammaVisualizzazioneEnum vEnum = OrganigrammaVisualizzazioneEnum.get(hint);

		if (vEnum == null) {
			final String visualizzazione = getVisualizzazioneByIdNodo(idNodo, connection);
			vEnum = OrganigrammaVisualizzazioneEnum.get(visualizzazione);
		}

		return vEnum == null ? OrganigrammaVisualizzazioneEnum.VISUALIZZAZIONE0 : vEnum;
	}

	/**
	 * @see it.ibm.red.business.dao.IOrganigrammaDAO#getNodiConDirigente(java.lang.Long,
	 *      java.lang.Long, java.lang.String, java.sql.Connection).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getNodiConDirigente(final Long idNodo, final Long idAOO, final String vInput, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<NodoOrganigrammaDTO> list = null;
		final StringBuilder queryBuilder = new StringBuilder(); 
		
		try {
			
			int index = 1;

			final OrganigrammaVisualizzazioneEnum vEnum = retrieveNomeColonna(idNodo, vInput, connection);
			
						// SELECT	
			queryBuilder.append("SELECT u.idnodo, u.codicenodo, u.idaoo, Decode(LEVEL, 1, 0, nod.").append(vEnum.toString()).append(") AS idnodopadre, ")
						.append("u.descrizione, u.idtiponodo, u.dataattivazione, u.datadisattivazione, u.idtipostruttura, u.flagsegreteria, nod.ORDINAMENTO, ")
						
						// Count figli del nodo 
						.append("(select count(*) ").append("from nodo_organigramma nod_count, nodo n ")
						.append("where nod_count.").append(vEnum.toString()).append(" = u.idnodo ").append("and nod_count.IDNODO_ORG = n.IDNODO and n.idnodocorriere > 0 AND n.datadisattivazione IS NULL) figli, ")
						
						.append("u.tipovisualizzazione, nod.* ")
						
						// FROM
						.append("FROM nodo u, nodo_organigramma nod ")

						// WHERE
						.append("WHERE u.datadisattivazione IS NULL ").append("AND nod.").append(vEnum.toString()).append(" = ? ")
						.append("AND u.IDUTENTEDIRIGENTE IS NOT NULL ")
						.append("AND u.idnodocorriere > 0 AND u.idnodo = nod.idnodo_org ")
						.append("CONNECT BY PRIOR u.idnodo = nod.").append(vEnum.toString()).append(" ")
						.append("START WITH u.idnodo = (SELECT idnodoradice FROM aoo WHERE idaoo = ?) ")
						.append("ORDER SIBLINGS BY nod.ordinamento");
			
			
			ps = connection.prepareStatement(queryBuilder.toString());
			ps.setLong(index++, idNodo);
			ps.setLong(index++, idAOO);

			rs = ps.executeQuery();

			list = new ArrayList<>();
			while (rs.next()) {
				list.add(new NodoOrganigrammaDTO(rs.getLong(IDNODO_COLUMN), rs.getLong(IDAOO), rs.getLong("IDNODOPADRE"),
						rs.getString(CODICENODO), rs.getString(DESCRIZIONE), TipologiaNodoEnum.get(rs.getInt("IDTIPONODO")),
						TipoStrutturaNodoEnum.get(rs.getInt("IDTIPOSTRUTTURA")), rs.getInt("FLAGSEGRETERIA") == 1,
						rs.getInt("ORDINAMENTO"), rs.getInt("FIGLI"), rs.getLong("IDNODO_ORG"),
						rs.getLong("IDNODOPADRE_ORG"), rs.getLong("IDAOO_ORG"), rs.getLong(VISUALIZZAZIONE0),
						rs.getLong("VISUALIZZAZIONE1"), rs.getLong("VISUALIZZAZIONE2"),
						rs.getLong("VISUALIZZAZIONE3")));
			}
			
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei Nodi figli con dirigente configurato  idNodo: " + idNodo + " - idAoo: " + idAOO, e);
			throw new RedException("Errore durante il recupero dei Nodi figli con dirigente configurato  idNodo: " + idNodo + " - idAoo: " + idAOO, e);
		} finally {
			closeStatement(ps, rs);
		}
		return list;
	}

	/**
	 * @see it.ibm.red.business.dao.IOrganigrammaDAO#getUtenteDirigenteByNodo(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public NodoOrganigrammaDTO getUtenteDirigenteByNodo(final Long idNodo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		NodoOrganigrammaDTO output = null;
		final StringBuilder queryBuilder = new StringBuilder(); 
		try {
			int index = 1;
						// SELECT
			queryBuilder.append("SELECT DISTINCT ute.idutente, ute.nome, ute.cognome, ute.username, ute.dataattivazione, ute.datadisattivazione, ute.email, ute.codfis, ute.registroriservato, nod.idnodo, nod.codicenodo, nod.idaoo, nod.descrizione ")
						// FROM
						.append("FROM utente ute, nodoutenteruolo nur, nodo nod, ruolo ru, aoo ")
						// WHERE
						.append("WHERE nur.idruolo = ru.idruolo AND nur.idutente = ute.idutente ")
						.append("AND nod.idutentedirigente = ute.idutente ")
						.append("AND nur.idnodo = nod.idnodo AND aoo.idaoo = nod.idaoo AND nod.idnodo = ? ")
						.append("AND nod.datadisattivazione IS NULL AND ru.datadisattivazione IS NULL ")
						.append("AND nur.datadisattivazione IS NULL AND ute.datadisattivazione IS NULL ")
						.append("AND nur.gestioneapplicativa in (0, 1) ")
						// ORDER BY
						.append("ORDER BY ute.cognome");

			ps = connection.prepareStatement(queryBuilder.toString());
			ps.setLong(index++, idNodo);

			rs = ps.executeQuery();

			while (rs.next()) {
				output = new NodoOrganigrammaDTO(rs.getLong(IDNODO_COLUMN), rs.getLong(IDAOO), rs.getString(CODICENODO), rs.getString(DESCRIZIONE),
						rs.getLong(IDUTENTE), rs.getString("NOME"), rs.getString(COGNOME), rs.getString(USERNAME),
						rs.getString(EMAIL), rs.getString(CODFIS), TipologiaNodoEnum.UTENTE);
			}
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_UTENTE_DIRIGENTE_MSG + idNodo, e);
			throw new RedException(ERROR_RECUPERO_UTENTE_DIRIGENTE_MSG + idNodo, e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IOrganigrammaDAO#getUtentiByNodo(java.lang.Long,
	 *      boolean, java.sql.Connection).
	 */
	@Override
	public List<NodoOrganigrammaDTO> getUtentiByNodo(final Long idNodo, final boolean withDirigente, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<NodoOrganigrammaDTO> output = null;
		final StringBuilder queryBuilder = new StringBuilder(); 
		try {
			int index = 1;
						// SELECT
			queryBuilder.append("SELECT DISTINCT ute.idutente, ute.nome, ute.cognome, ute.username, ute.dataattivazione, ute.datadisattivazione, ute.email, ute.codfis, ute.registroriservato, nod.idnodo, nod.codicenodo, nod.idaoo, nod.descrizione ")
						// FROM
						.append("FROM utente ute, nodoutenteruolo nur, nodo nod, ruolo ru, aoo ")
						// WHERE
						.append("WHERE nur.idruolo = ru.idruolo AND nur.idutente = ute.idutente ")
						.append("AND nur.idnodo = nod.idnodo AND aoo.idaoo = nod.idaoo AND nod.idnodo = ? ")
						.append("AND nod.datadisattivazione IS NULL AND ru.datadisattivazione IS NULL ")
						.append("AND nur.datadisattivazione IS NULL AND ute.datadisattivazione IS NULL ")
						.append("AND nur.gestioneapplicativa in (0, 1) ");
			if (!withDirigente) {
				queryBuilder.append("AND nod.idutentedirigente != ute.idutente ");
			}
						// ORDER BY
			queryBuilder.append("ORDER BY ute.cognome");

			ps = connection.prepareStatement(queryBuilder.toString());
			ps.setLong(index++, idNodo);

			rs = ps.executeQuery();

			output = new ArrayList<>();
			while (rs.next()) {
				output.add(new NodoOrganigrammaDTO(rs.getLong(IDNODO_COLUMN), rs.getLong(IDAOO), rs.getString(CODICENODO), rs.getString(DESCRIZIONE),
						rs.getLong(IDUTENTE), rs.getString("NOME"), rs.getString(COGNOME), rs.getString(USERNAME),
						rs.getString(EMAIL), rs.getString(CODFIS), TipologiaNodoEnum.UTENTE));
			}
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_UTENTE_DIRIGENTE_MSG + idNodo, e);
			throw new RedException(ERROR_RECUPERO_UTENTE_DIRIGENTE_MSG + idNodo, e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}
}
