package it.ibm.red.business.persistence.model;

import java.io.Serializable;

/**
 * The Class TipoRicevuta.
 *
 * @author CPIERASC
 * 
 *         Entità che mappa la tabella TIPORICEVUTAPEC.
 */
public class TipoRicevuta implements Serializable {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Descrizione.
	 */
	private String descrizione;

	/**
	 * Identificativo.
	 */
	private Long idTipoRicevuta;

	/**
	 * Costruttore.
	 * 
	 * @param inIdTipoRicevuta	identificativo
	 * @param inDescrizione			descrizione
	 */
	public TipoRicevuta(final Long inIdTipoRicevuta, final String inDescrizione) {
		this.descrizione = inDescrizione;
		this.idTipoRicevuta = inIdTipoRicevuta;
	}

	/**
	 * Getter descrizione.
	 * 
	 * @return	descrizione
	 */
	public final String getDescrizione() {
		return descrizione;
	}

	/**
	 * Getter id.
	 * 
	 * @return	identificativo
	 */
	public final Long getIdTipoRicevuta() {
		return idTipoRicevuta;
	}

}