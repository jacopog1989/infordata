/**
 * 
 */
package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.core.Document;

import filenet.vw.api.VWQueueQuery;
import filenet.vw.api.VWRosterQuery;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants.BooleanFlag;
import it.ibm.red.business.dao.ICodeApplicativeDAO;
import it.ibm.red.business.dao.IEventoLogDAO;
import it.ibm.red.business.dao.IGestioneNotificheEmailDAO;
import it.ibm.red.business.dao.ILookupDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IRegistroRepertorioDAO;
import it.ibm.red.business.dao.IStampigliaturaSegnoGraficoDAO;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.DocumentQueueQueryObject;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.PEDocumentoDTO;
import it.ibm.red.business.dto.RegistroRepertorioDTO;
import it.ibm.red.business.dto.TandemDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.IconaFlussoEnum;
import it.ibm.red.business.enums.IconaStatoEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaRicercaCodeEnum;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.QueueGroupEnum;
import it.ibm.red.business.enums.SourceTypeEnum;
import it.ibm.red.business.enums.StatoCodaEmailEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.TrasformPE;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.CodaEmail;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IListaDocumentiSRV;
import it.ibm.red.business.service.IMasterPaginatiSRV;
import it.ibm.red.business.service.IRegistroRepertorioSRV;
import it.ibm.red.business.service.facade.IASignFacadeSRV;
import it.ibm.red.business.service.flusso.IAzioniFlussoBaseSRV;
import it.ibm.red.business.service.flusso.IAzioniFlussoSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.DestinatariRedUtils;
import it.ibm.red.business.utils.PermessiUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * @author APerquoti
 *
 */
@Service
public class ListaDocumentiSRV extends AbstractService implements IListaDocumentiSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1127677370619860401L;

	/**
	 * NSD ROSTER.
	 */
	private static final String NSD_ROSTER = "NSD_Roster";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ListaDocumentiSRV.class.getName());

	/**
	 * DAO gestione utente.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;

	/**
	 * DAO per la gestione del nodo.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * DAO per la gestione delle Code Applicative.
	 */
	@Autowired
	private ICodeApplicativeDAO codeApplicativeDAO;

	/**
	 * DAO coda mail.
	 */
	@Autowired
	private IGestioneNotificheEmailDAO gestioneNotificheMailDAO;

	/**
	 * Servizio.
	 */
	@Autowired
	private ILookupDAO lookupDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IEventoLogDAO eventLogDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private ITipoProcedimentoDAO tipoProcedimentoDAO;

	/**
	 * Servizio.
	 */
	@Autowired
	private IMasterPaginatiSRV masterPaginatiSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private IRegistroRepertorioDAO registroRepertorioDAO;

	/**
	 * Servizio.
	 */
	@Autowired
	private IRegistroRepertorioSRV registroRepertorioSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IAzioniFlussoSRV azioniFlussoSRV;

	/**
	 * DAO.
	 */
	@Autowired
	IStampigliaturaSegnoGraficoDAO stampigliaturaSiglaDAO; 

	/**
	 * Service gestione firma asincrona.
	 */
	@Autowired
	private IASignFacadeSRV aSignSRV;

	/**
	 * Metodo per il recupero dell'entità Documenti per valorizzare le Code.
	 */
	@Override
	public Collection<MasterDocumentRedDTO> getDocumentForMaster(final DocumentQueueEnum queue, final Collection<String> documentTitlesToFilterBy, final UtenteDTO utente) {
		Collection<MasterDocumentRedDTO> masters = new ArrayList<>();
		Integer codaMaxResult = null;
		final String strCodaMaxResult = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.CODA_MAX_RESULTS);
		if (!StringUtils.isNullOrEmpty(strCodaMaxResult)) {
			codaMaxResult = Integer.valueOf(strCodaMaxResult);
		}
		Set<String> documentTitleSet = new HashSet<>();
		final HashMap<String, Collection<TandemDTO>> tandem = new HashMap<>();
		Connection connection = null;
		IFilenetCEHelper fceh = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			if (queue.getType().equals(SourceTypeEnum.FILENET)) { // Gestione code filenet
				Collection<PEDocumentoDTO> workFlowDTO = null;

				if (queue.getTextFilter()) {
					workFlowDTO = getFepaWorflows(queue, utente);
				} else {
					final VWQueueQuery workFlow = getQueueFilenet(queue, utente, documentTitlesToFilterBy, connection);
					workFlowDTO = TrasformPE.transform(workFlow, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO, codaMaxResult);
				}
				for (final PEDocumentoDTO wf : workFlowDTO) {
					final Collection<TandemDTO> dtoColl = new ArrayList<>();
					wf.setQueue(queue);
					if (documentTitleSet.add(wf.getIdDocumento())) {
						final TandemDTO t = new TandemDTO(wf);
						dtoColl.add(t);
						tandem.put(wf.getIdDocumento(), dtoColl);
					} else {
						tandem.get(wf.getIdDocumento()).add(new TandemDTO(wf));
					}
				}
			} else { // Gestione Code Applicative
				if ((DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE.equals(queue) || DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE_FEPA.equals(queue)) && utente.isGestioneApplicativa()) {
					documentTitleSet = aSignSRV.getIncompletedDTs(utente, queue);
				} else {
					if (CollectionUtils.isEmpty(documentTitlesToFilterBy)) {
						final Long idNodoUtente = utente.getIdUfficio();
						Long idUtente = utente.getId();
						if (queue.getGroup().equals(QueueGroupEnum.UFFICIO)) {
							idUtente = 0L;
						}
						
						documentTitleSet = codeApplicativeDAO.getCodeAppl(idNodoUtente, idUtente, queue.getIdStatiLavorazione(), codaMaxResult, connection);
					// Se la lista dei documenti con cui filtrare i risultati è già passato in input, si aggiungono tutti e soli questi elementi all'insieme
					} else {
						documentTitleSet.addAll(documentTitlesToFilterBy);
					}
				}
			}
			
			// Punto comune dove si interroga il CE di FileNet con un set di DocumentTitle
			if (documentTitleSet != null && !documentTitleSet.isEmpty()) {
				final Collection<MasterDocumentRedDTO> cMasters = getMastersFromCEByDocumentTitles(documentTitleSet, utente, connection);

				masters = mergePeCeTandem(tandem, cMasters, utente, connection, queue);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei documenti per popolare i Master ( " + queue.getName() + " ): " + e.getMessage(), e);
		} finally {
			closeConnection(connection);
			popSubject(fceh);
		}
		return masters;
	}

	/**
	 * @see it.ibm.red.business.service.IListaDocumentiSRV#getFilteredMastersFromCE(java.util.Collection,
	 *      java.lang.String, it.ibm.red.business.dto.UtenteDTO,
	 *      java.sql.Connection).
	 */
	@Override
	public Collection<MasterDocumentRedDTO> getFilteredMastersFromCE(final Collection<String> documentTitlesToFilterBy, final String filterString, final UtenteDTO utente,
			final Connection connection) {
		Collection<MasterDocumentRedDTO> mastersCE = new ArrayList<>();
		IFilenetCEHelper fceh = null;
		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			// Si splitta la stringa di ricerca usando il separatore ";"
			final List<String> allTokensFiltro = Arrays.asList(filterString.split(";"));
			// Si estraggono dal DB gli ID delle tipologie documento la cui descrizione
			// corrisponde anche solo parzialmente (LIKE) a uno dei token di ricerca
			final List<Long> idsTipologiaDocumento = lookupDAO.getIdsTipologiaDocumentoByDescrizione(allTokensFiltro, utente.getIdAoo(), connection);
			// Si costruiscono tre liste per gestire rispettivamente i caratteri numerici
			// della stringa di ricerca e i metadati Numero Protocollo e Anno Protocollo
			final List<String> numericTokens = new ArrayList<>();
			final List<String> numeroProtocolloTokens = new ArrayList<>();
			final List<String> annoProtocolloTokens = new ArrayList<>();
			final Pattern numberPattern = Pattern.compile("[0-9]+");
			Matcher matcher;
			String[] slashTokens;
			for (final String tokenFiltro : allTokensFiltro) {
				// Gestione token "numeroProtocollo/annoProtocollo"
				if (tokenFiltro.contains("/")) {
					slashTokens = tokenFiltro.trim().split("/");

					if (slashTokens.length == 2 && numberPattern.matcher(slashTokens[0]).find() && numberPattern.matcher(slashTokens[1]).find()) {
						numeroProtocolloTokens.add(slashTokens[0]);
						annoProtocolloTokens.add(slashTokens[1]);
					}
				} else {
					matcher = numberPattern.matcher(tokenFiltro);

					while (matcher.find()) {
						numericTokens.add(matcher.group());
					}
				}
			}
			final DocumentSet dsFileNet = fceh.getDocumentForFilteredMasters(documentTitlesToFilterBy, allTokensFiltro, numericTokens, numeroProtocolloTokens,
					annoProtocolloTokens, idsTipologiaDocumento, utente.getIdAoo(),
					PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));
			// TX WITH CONTEXT!
			// INIZIO
			final Map<ContextTrasformerCEEnum, Object> context = new EnumMap<>(ContextTrasformerCEEnum.class);
			final Map<Long, String> idsTipoDoc = lookupDAO.getDescTipoDocumento(utente.getIdAoo().intValue(), connection);
			context.put(ContextTrasformerCEEnum.IDENTIFICATIVI_TIPO_DOC, idsTipoDoc);

			final Map<Long, String> tps = tipoProcedimentoDAO.getAllDesc(connection);
			context.put(ContextTrasformerCEEnum.IDENTIFICATIVI_TIPO_PROC, tps);

			context.put(ContextTrasformerCEEnum.GESTISCI_RESPONSE, true);

			final Map<String, String> iconaStampigliaturaMap = new HashMap<>();
			final Map<String, String> contattiMap = new HashMap<>();

			final Map<String, RegistroRepertorioDTO> registroRepertorioMap = new HashMap<>();
			final Map<Long, List<RegistroRepertorioDTO>> registriRepertorioConfigurati = registroRepertorioSRV.getRegistriRepertorioConfigurati(utente.getIdAoo(), connection);
			final RegistroRepertorioDTO denominazioneRegistro = registroRepertorioDAO.getDenominazioneRegistroUfficiale(utente.getIdAoo(), connection);

			if (dsFileNet != null && !dsFileNet.isEmpty()) {
				final Iterator<?> itDsFileNet = dsFileNet.iterator();
				while (itDsFileNet.hasNext()) {
					final Document document = (Document) itDsFileNet.next();

					// ### Gestione Mittente / Destinatari -> START
					final String mittente = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.MITTENTE_METAKEY);

					if (!StringUtils.isNullOrEmpty(mittente)) {
						final String[] mittenteSplit = mittente.split("\\,");

						if (mittenteSplit.length >= 2 && contattiMap.get(mittenteSplit[0]) == null) {
							contattiMap.put(mittenteSplit[0], mittenteSplit[1]);
						}
					} else {
						final Collection<?> destinatari = (Collection<?>) TrasformerCE.getMetadato(document, PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY);
						// Si normalizza il metadato per evitare errori comuni durante lo split delle
						// singole stringhe dei destinatari
						final List<String[]> destinatariDocumento = DestinatariRedUtils.normalizzaMetadatoDestinatariDocumento(destinatari);

						if (!CollectionUtils.isEmpty(destinatariDocumento)) {
							for (final String[] destSplit : destinatariDocumento) {

								if (destSplit.length >= 5) {
									final TipologiaDestinatarioEnum tde = TipologiaDestinatarioEnum.getByTipologia(destSplit[3]);
									if (TipologiaDestinatarioEnum.INTERNO == tde && "0".equals(destSplit[1])) {
										contattiMap.put(destSplit[0], destSplit[2]);
									} else if (TipologiaDestinatarioEnum.INTERNO == tde && contattiMap.get(destSplit[1]) == null) {
										contattiMap.put(destSplit[1], destSplit[2]);
									} else if (TipologiaDestinatarioEnum.ESTERNO == tde && contattiMap.get(destSplit[0]) == null) {
										contattiMap.put(destSplit[0], destSplit[1]);
									}
								}
							}
						}
					}
					// ### Gestione Mittente / Destinatari -> END
					// Gestione Registro Repertorio -> START
					final String registroRepertorio = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.REGISTRO_PROTOCOLLO_FN_METAKEY);
					final String documentTitle = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
					if (registroRepertorio == null) {
						final Integer idTipoDocumento = (Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY);
						final Integer idTipoProcedimento = (Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY);

						Boolean isRegistroRepertorioPreProtocollo = Boolean.FALSE;
						if (idTipoDocumento != null && idTipoProcedimento != null) {
							isRegistroRepertorioPreProtocollo = registroRepertorioSRV.checkIsRegistroRepertorio(utente.getIdAoo(), idTipoDocumento.longValue(),
									idTipoProcedimento.longValue(), registriRepertorioConfigurati, connection);
						}
						registroRepertorioMap.put(documentTitle, new RegistroRepertorioDTO(isRegistroRepertorioPreProtocollo, null));
					} else {
						if (registroRepertorio != null && !registroRepertorio.equalsIgnoreCase(denominazioneRegistro.getDescrizioneRegistroRepertorio())) {
							registroRepertorioMap.put(documentTitle, new RegistroRepertorioDTO(true, registroRepertorio));
						} else {
							registroRepertorioMap.put(documentTitle, new RegistroRepertorioDTO(false, denominazioneRegistro.getDescrizioneRegistroRepertorio()));
						}
					}
//					Gestione Registro Repertorio -> END
					final String placeholder = stampigliaturaSiglaDAO.getPlaceholderByIdDoc(documentTitle, connection);
					if (!StringUtils.isNullOrEmpty(placeholder)) {
						iconaStampigliaturaMap.put(documentTitle, placeholder);
					}
				}
			}
			if (!iconaStampigliaturaMap.isEmpty()) {
				context.put(ContextTrasformerCEEnum.LIBRO_FIRMA_PLACEHOLDER, iconaStampigliaturaMap);
			}
			if (!registroRepertorioMap.isEmpty()) {
				context.put(ContextTrasformerCEEnum.REGISTRO_PROTOCOLLO, registroRepertorioMap);
			}
			if (!contattiMap.isEmpty()) {
				context.put(ContextTrasformerCEEnum.CONTATTI_DISPLAY_NAME, contattiMap);
			}
			context.put(ContextTrasformerCEEnum.ASIGN_INFO, aSignSRV.getMasterInfo(documentTitlesToFilterBy));
			mastersCE = TrasformCE.transform(dsFileNet, TrasformerCEEnum.FROM_DOCUMENTO_TO_GENERIC_DOC_CONTEXT, context);
			// FINE
		} catch (final Exception e) {
			LOGGER.error(e);
		} finally {
			popSubject(fceh);
		}
		return mastersCE;
	}

	/**
	 * @see it.ibm.red.business.service.IListaDocumentiSRV#getMastersFromCEByDocumentTitles(java.util.Collection,
	 *      it.ibm.red.business.dto.UtenteDTO, java.sql.Connection).
	 */
	@Override
	public Collection<MasterDocumentRedDTO> getMastersFromCEByDocumentTitles(final Collection<String> documentTitlesToFilterBy, final UtenteDTO utente,
			final Connection connection) {
		Collection<MasterDocumentRedDTO> mastersCE = new ArrayList<>();
		IFilenetCEHelper fceh = null;
		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final DocumentSet dsFileNet = fceh.getDocumentsForMasters(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY),
					documentTitlesToFilterBy);
			// TX WITH CONTEXT!
			// INIZIO
			final Map<ContextTrasformerCEEnum, Object> context = new EnumMap<>(ContextTrasformerCEEnum.class);
			final Map<Long, String> idsTipoDoc = lookupDAO.getDescTipoDocumento(utente.getIdAoo().intValue(), connection);
			context.put(ContextTrasformerCEEnum.IDENTIFICATIVI_TIPO_DOC, idsTipoDoc);

			final Map<Long, String> tps = tipoProcedimentoDAO.getAllDesc(connection);
			context.put(ContextTrasformerCEEnum.IDENTIFICATIVI_TIPO_PROC, tps);

			context.put(ContextTrasformerCEEnum.GESTISCI_RESPONSE, true);

			// ### Gestione Mittente / Destinatari -> START
			final Map<String, String> contattiMap = new HashMap<>();
			final Map<String, RegistroRepertorioDTO> registroRepertorioMap = new HashMap<>();
			final Map<Long, List<RegistroRepertorioDTO>> registriRepertorioConfigurati = registroRepertorioSRV.getRegistriRepertorioConfigurati(utente.getIdAoo(), connection);

			if (dsFileNet != null && !dsFileNet.isEmpty()) {
				final Iterator<?> itDsFileNet = dsFileNet.iterator();
				while (itDsFileNet.hasNext()) {
					final Document document = (Document) itDsFileNet.next();

					// Gestione Mittente / Destinatari -> START
					final String mittente = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.MITTENTE_METAKEY);

					if (!StringUtils.isNullOrEmpty(mittente)) {
						final String[] mittenteSplit = mittente.split("\\,");

						if (mittenteSplit.length >= 2 && contattiMap.get(mittenteSplit[0]) == null) {
							contattiMap.put(mittenteSplit[0], mittenteSplit[1]);
						}
					} else {
						final Collection<?> destinatari = (Collection<?>) TrasformerCE.getMetadato(document, PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY);
						// Si normalizza il metadato per evitare errori comuni durante lo split delle
						// singole stringhe dei destinatari
						final List<String[]> destinatariDocumento = DestinatariRedUtils.normalizzaMetadatoDestinatariDocumento(destinatari);

						if (!CollectionUtils.isEmpty(destinatariDocumento)) {
							for (final String[] destSplit : destinatariDocumento) {

								if (destSplit.length >= 5) {
									final TipologiaDestinatarioEnum tde = TipologiaDestinatarioEnum.getByTipologia(destSplit[3]);
									if (TipologiaDestinatarioEnum.INTERNO == tde && "0".equals(destSplit[1])) {
										contattiMap.put(destSplit[0], destSplit[2]);
									} else if (TipologiaDestinatarioEnum.INTERNO == tde && contattiMap.get(destSplit[1]) == null) {
										contattiMap.put(destSplit[1], destSplit[2]);
									} else if (TipologiaDestinatarioEnum.ESTERNO == tde && contattiMap.get(destSplit[0]) == null) {
										contattiMap.put(destSplit[0], destSplit[1]);
									}
								}
							}
						}
					}
					// Gestione Mittente / Destinatari -> END

					final String documentTitle = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
					// Gestione Registro Repertorio -> START

					final RegistroRepertorioDTO denominazioneRegistro = registroRepertorioDAO.getDenominazioneRegistroUfficiale(utente.getIdAoo(), connection);
					final String registroRepertorio = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.REGISTRO_PROTOCOLLO_FN_METAKEY);

					if (registroRepertorio == null) {
						final Integer idTipoDocumento = (Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY);
						final Integer idTipoProcedimento = (Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY);

						Boolean isRegistroRepertorioPreProtocollo = Boolean.FALSE;
						if (idTipoDocumento != null && idTipoProcedimento != null) {
							isRegistroRepertorioPreProtocollo = registroRepertorioSRV.checkIsRegistroRepertorio(utente.getIdAoo(), idTipoDocumento.longValue(),
									idTipoProcedimento.longValue(), registriRepertorioConfigurati, connection);
						}
						registroRepertorioMap.put(documentTitle, new RegistroRepertorioDTO(isRegistroRepertorioPreProtocollo, null));
					} else {
						if (registroRepertorio != null && !registroRepertorio.equalsIgnoreCase(denominazioneRegistro.getDescrizioneRegistroRepertorio())) {
							registroRepertorioMap.put(documentTitle, new RegistroRepertorioDTO(true, registroRepertorio));
						} else {
							registroRepertorioMap.put(documentTitle, new RegistroRepertorioDTO(false, denominazioneRegistro.getDescrizioneRegistroRepertorio()));
						}
					}
//					Gestione Registro Repertorio -> END
				}
			}
			if (!registroRepertorioMap.isEmpty()) {
				context.put(ContextTrasformerCEEnum.REGISTRO_PROTOCOLLO, registroRepertorioMap);
			}
			if (!contattiMap.isEmpty()) {
				context.put(ContextTrasformerCEEnum.CONTATTI_DISPLAY_NAME, contattiMap);
			}
			context.put(ContextTrasformerCEEnum.ASIGN_INFO, aSignSRV.getMasterInfo(documentTitlesToFilterBy));
			mastersCE = TrasformCE.transform(dsFileNet, TrasformerCEEnum.FROM_DOCUMENTO_TO_GENERIC_DOC_CONTEXT, context);
			// Flag Siebel
			masterPaginatiSRV.addSiebelFlag(mastersCE, utente.getIdAoo(), connection);
		} catch (final Exception e) {
			LOGGER.error(e);
		} finally {
			popSubject(fceh);

		}
		return mastersCE;
	}

	/**
	 * @see it.ibm.red.business.service.IListaDocumentiSRV#getFepaWorflows(it.ibm.red.business.enums.DocumentQueueEnum,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Collection<PEDocumentoDTO> getFepaWorflows(final DocumentQueueEnum queue, final UtenteDTO utente) {
		Collection<PEDocumentoDTO> workFlowDTO = null;

		if (NSD_ROSTER.equalsIgnoreCase(queue.getName())) {
			final VWRosterQuery queryRoster = (VWRosterQuery) getFepaQueryObject(queue, null, utente);
			workFlowDTO = TrasformPE.transform(queryRoster, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO);
		} else {
			final VWQueueQuery query = (VWQueueQuery) getFepaQueryObject(queue, null, utente);
			workFlowDTO = TrasformPE.transform(query, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO, null);
		}
		return workFlowDTO;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IListaDocumentiFacadeSRV#giaSpedito(java.lang.String).
	 */
	@Override
	public boolean giaSpedito(final String documentTitle) {
		Connection filenetCon = null;
		boolean spedito = false;
		try {
			filenetCon = setupConnection(getFilenetDataSource().getConnection(), false);
			spedito = eventLogDAO.giaSpedito(filenetCon, documentTitle);
		} catch (final Exception e) {
			LOGGER.error("Errore durante la ricerca dello spedito", e);
		} finally {
			closeConnection(filenetCon);
		}
		return spedito;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IListaDocumentiFacadeSRV#getFepaQueryObject(it.ibm.red.business.enums.DocumentQueueEnum,
	 *      java.util.Collection, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Object getFepaQueryObject(final DocumentQueueEnum queue, final Collection<String> documentTitlesToFilterBy, final UtenteDTO utente) {
		final String filter = getFepaQueryFilter(queue, documentTitlesToFilterBy, utente, null);

		final FilenetPEHelper fpeh = new FilenetPEHelper(utente.getFcDTO());
		return fpeh.getQueueQuery(queue.getName(), queue.getIndexName(), filter);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IListaDocumentiFacadeSRV#getFepaQueryObject(it.ibm.red.business.enums.DocumentQueueEnum,
	 *      java.util.Collection, it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.persistence.model.Nodo,
	 *      it.ibm.red.business.helper.filenet.pe.FilenetPEHelper).
	 */
	@Override
	public Object getFepaQueryObject(final DocumentQueueEnum queue, final Collection<String> documentTitlesToFilterBy, final UtenteDTO utente, final Nodo idNodo,
			final FilenetPEHelper fpeh) {
		final String filter = getFepaQueryFilter(queue, documentTitlesToFilterBy, utente, idNodo);

		return fpeh.getQueueQuery(queue.getName(), queue.getIndexName(), filter);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IListaDocumentiFacadeSRV#getFepaQueryObjectForDocumentTitle(it.ibm.red.business.enums.DocumentQueueEnum,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.String).
	 */
	@Override
	public Object getFepaQueryObjectForDocumentTitle(final DocumentQueueEnum queue, final UtenteDTO utente, final String documentTitle) {
		final String filter = getFepaQueryFilter(queue, Arrays.asList(documentTitle), utente, null);

		final FilenetPEHelper fpeh = new FilenetPEHelper(utente.getFcDTO());
		return fpeh.getQueueQuery(queue.getName(), queue.getIndexName(), filter);
	}

	private String getFepaQueryFilter(final DocumentQueueEnum queue, final Collection<String> documentTitlesToFilterBy, final UtenteDTO utente, final Nodo idNodo) {
		String filter;

		if (DocumentQueueEnum.DSR.equals(queue)) {
			filter = "((idUtenteDestinatario = {USERID} and idNodoDestinatario = {NODOID} OR idUtenteMittente = {USERID} AND idNodoMittente = {NODOID})) "
					+ "AND idClient = 'NSD' AND F_Class = 'SIGI-RED-AssegnaDDFEPA'";
		} else if (DocumentQueueEnum.FATTURE_DA_LAVORARE.equals(queue)) {
			filter = "idUtenteDestinatario = {USERID} AND idNodoDestinatario = {NODOID} AND idClient = 'NSD' AND F_Class = 'SIGI-RED-AssegnaFatturaFEPA' "
					+ "AND F_StepName = 'Attesa DD'";
		} else if (DocumentQueueEnum.FATTURE_IN_LAVORAZIONE.equals(queue)) {
			filter = "idUtenteDestinatario = {USERID} AND idNodoDestinatario = {NODOID} AND idClient = 'NSD' AND F_Class = 'SIGI-RED-AssegnaFatturaFEPA' "
					+ "AND F_StepName = 'Attesa Chiusura DD'";
		} else if (DocumentQueueEnum.DD_DA_LAVORARE.equals(queue)) {
			filter = "idUtenteDestinatario = {USERID} AND idNodoDestinatario = {NODOID} AND idClient = 'NSD' AND ((F_Class = 'SIGI-RED- Decreto_Dirigenziale_FEP' "
					+ "AND (F_StepName = 'Attesa Associazione DSR' OR F_StepName = 'DSR Associato')) OR (F_Class = 'SIGI-RED- Decreto_Dirigenziale_FEP_IVA' "
					+ "AND F_StepName = 'Decreto IVA'))";
		} else if (DocumentQueueEnum.DD_IN_FIRMA.equals(queue)) {
			filter = "idUtenteDestinatario = {USERID} AND idNodoDestinatario = {NODOID} AND idClient = 'NSD' AND (F_Class = 'SIGI-RED- Decreto_Dirigenziale_FEP' "
					+ "OR F_Class = 'SIGI-RED- Decreto_Dirigenziale_FEP_IVA') AND F_StepName = ' Attesa Firma Dirigente'";
		} else if (DocumentQueueEnum.DD_FIRMATI.equals(queue)) {
			filter = "idUtenteDestinatario = {USERID} AND idNodoDestinatario = {NODOID} AND idClient = 'NSD' AND (F_Class = 'SIGI-RED- Decreto_Dirigenziale_FEP' "
					+ "OR F_Class = 'SIGI-RED- Decreto_Dirigenziale_FEP_IVA') AND F_StepName = 'Attesa Messa Agli Atti'";
			// Queue -- DIRIGENTE Cambia la query
			if (PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.PERMESSO_DIRIGENTE_FEPA)) {
				filter = "idNodoDestinatario = {NODOID} AND idClient = 'NSD' AND F_Class = 'SIGI_RED_Firma Decreto' AND F_StepName = 'Attesa Messa Atti'";
			}
		} else if (DocumentQueueEnum.DD_DA_FIRMARE.equals(queue)) {
			filter = "idNodoDestinatario = {NODOID} AND idClient = 'NSD' AND F_Class = 'SIGI_RED_Firma Decreto' AND F_StepName <> 'Attesa Messa Atti'"
					+ " AND flag_renderizzato <> 0 AND flag_firmaAsincronaAvviata <> 1";
		} else {
			throw new RedException("Coda non censita");
		}
		filter = filter.replace("{USERID}", utente.getId().toString());
		if (idNodo == null) {
			filter = filter.replace("{NODOID}", utente.getIdUfficio().toString());
		} else {
			filter = filter.replace("{NODOID}", idNodo.getIdNodo().toString());
		}
		if (!CollectionUtils.isEmpty(documentTitlesToFilterBy)) {
			filter += " AND " + StringUtils.createInCondition(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY),
					documentTitlesToFilterBy.iterator(), false);
		}
		return filter;
	}

	/**
	 * Cerca un DocumentTitle su tutte le code FileNet e restituisce i risultati di
	 * ricerca.
	 */
	@Override
	public Collection<VWQueueQuery> getQueueFNbyDocumentTitle(final UtenteDTO utente, final String documentTitle, final String classeDocumentale, final Boolean isDocEntrata) {
		final Collection<VWQueueQuery> output = new ArrayList<>();
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			for (final DocumentQueueEnum queue : DocumentQueueEnum.values()) {
				if (skipCycleForQueue(queue, utente, isDocEntrata)) {
					continue;
				}
				if (queue.getType().equals(SourceTypeEnum.FILENET) && !queue.getName().equals(NSD_ROSTER)) {
					final VWQueueQuery workFlow = getQueueFilenet(queue, utente, Arrays.asList(documentTitle), connection);
					output.add(workFlow);
				}
			}
			return output;
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero delle code Filenet per la Ricerca del DocumentTitle( " + documentTitle + " ): " + e.getMessage(), e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}
		return new ArrayList<>();
	}

	/**
	 * @see it.ibm.red.business.service.facade.IListaDocumentiFacadeSRV#isDocumentoInCoda(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, it.ibm.red.business.enums.DocumentQueueEnum).
	 */
	@Override
	public boolean isDocumentoInCoda(final UtenteDTO utente, final String documentTitle, final DocumentQueueEnum queue) {
		boolean isDocInCoda = false;
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			isDocInCoda = isDocumentoInCoda(utente, documentTitle, queue, con);
		} catch (final Exception e) {
			// Si prosegue, questo controllo mi serve solo per capire se si trova nella coda
			// o no
			LOGGER.error("Errore nel metodo isDocumentoInCoda", e);
		} finally {
			closeConnection(con);
		}

		return isDocInCoda;
	}

	/**
	 * @see it.ibm.red.business.service.IListaDocumentiSRV#isDocumentoInCoda(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, it.ibm.red.business.enums.DocumentQueueEnum,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean isDocumentoInCoda(final UtenteDTO utente, final String documentTitle, final DocumentQueueEnum queue, final Connection con) {
		boolean isDocInCoda = false;
		DocumentQueueQueryObject output = null;
		VWWorkObject wo = null;

		try {
			final DocumentQueueQueryObject dQQObject = new DocumentQueueQueryObject();

			if (queue != null) {
				if (DocumentQueueEnum.DSR.equals(queue) || DocumentQueueEnum.FATTURE_DA_LAVORARE.equals(queue) || DocumentQueueEnum.FATTURE_IN_LAVORAZIONE.equals(queue)
						|| DocumentQueueEnum.DD_DA_LAVORARE.equals(queue) || DocumentQueueEnum.DD_IN_FIRMA.equals(queue) || DocumentQueueEnum.DD_FIRMATI.equals(queue)
						|| DocumentQueueEnum.DD_DA_FIRMARE.equals(queue)) {
					final Object queryObj = getFepaQueryObjectForDocumentTitle(queue, utente, documentTitle);

					if (queryObj instanceof VWQueueQuery) {
						dQQObject.setQueueQueryObj((VWQueueQuery) queryObj);
						dQQObject.setQueueEnum(queue);
						output = dQQObject;

					} else if (queryObj instanceof VWRosterQuery) {
						dQQObject.setRosterQueryObj((VWRosterQuery) queryObj);
						dQQObject.setQueueEnum(queue);
						output = dQQObject;
					}
				} else if ((SourceTypeEnum.FILENET).equals(queue.getType()) && !NSD_ROSTER.equals(queue.getName())) {
					final VWQueueQuery workFlow = getQueueFilenet(queue, utente, Arrays.asList(documentTitle), con);
					dQQObject.setQueueQueryObj(workFlow);
					dQQObject.setQueueEnum(queue);
					output = dQQObject;
				}
			}
			if (output != null) {
				if (output.getQueueQueryObj() != null) {
					wo = (VWWorkObject) output.getQueueQueryObj().next();
				} else if (output.getRosterQueryObj() != null) {
					wo = (VWWorkObject) output.getRosterQueryObj().next();
				}
			}
			isDocInCoda = (wo != null);
		} catch (final Exception e) {
			// Si prosegue, questo controllo mi serve solo per capire se si trova nella coda
			// o no
			LOGGER.error("Errore nel metodo isDocumentoInCoda", e);
		}
		return isDocInCoda;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IListaDocumentiFacadeSRV#getQueueFNbyDocumentTitleFull(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String, java.lang.Boolean).
	 */
	@Override
	public Collection<DocumentQueueQueryObject> getQueueFNbyDocumentTitleFull(final UtenteDTO utente, final String documentTitle, final String classeDocumentale,
			final Boolean isDocEntrata) {
		final Collection<DocumentQueueQueryObject> output = new ArrayList<>();
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			for (final DocumentQueueEnum queue : DocumentQueueEnum.values()) {
				if (skipCycleForQueue(queue, utente, isDocEntrata)) {
					continue;
				}
				final DocumentQueueQueryObject dQQObject = new DocumentQueueQueryObject();
				if (DocumentQueueEnum.DSR.equals(queue) || DocumentQueueEnum.FATTURE_DA_LAVORARE.equals(queue) || DocumentQueueEnum.FATTURE_IN_LAVORAZIONE.equals(queue)
						|| DocumentQueueEnum.DD_DA_LAVORARE.equals(queue) || DocumentQueueEnum.DD_IN_FIRMA.equals(queue) || DocumentQueueEnum.DD_FIRMATI.equals(queue)
						|| DocumentQueueEnum.DD_DA_FIRMARE.equals(queue)) {
					final Object queryObj = getFepaQueryObjectForDocumentTitle(queue, utente, documentTitle);

					if (queryObj instanceof VWQueueQuery) {
						dQQObject.setQueueQueryObj((VWQueueQuery) queryObj);
						dQQObject.setQueueEnum(queue);
						output.add(dQQObject);
					} else if (queryObj instanceof VWRosterQuery) {
						dQQObject.setRosterQueryObj((VWRosterQuery) queryObj);
						dQQObject.setQueueEnum(queue);
						output.add(dQQObject);
					}
				} else if (queue.getType().equals(SourceTypeEnum.FILENET) && !queue.getName().equals(NSD_ROSTER)) {
					final VWQueueQuery workFlow = getQueueFilenet(queue, utente, Arrays.asList(documentTitle), connection);
					dQQObject.setQueueQueryObj(workFlow);
					dQQObject.setQueueEnum(queue);
					output.add(dQQObject);
				}
			}
			return output;
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero delle code Filenet per la Ricerca del DocumentTitle( " + documentTitle + " ): " + e.getMessage(), e);
		} finally {
			closeConnection(connection);
		}
		return new ArrayList<>();
	}

	private static boolean skipCycleForQueue(final DocumentQueueEnum queue, final UtenteDTO utente, final Boolean isDocEntrata) {
		boolean skypCycle = false;
		// Se è un documento in entrata non FEPA ma la coda non è relativa alle entrate
		// salto
		if (/*
			 * pp.getParameterByKey(PropertiesNameEnum.
			 * DOCUMENTO_GENERICO_CLASSNAME_FN_METAKEY).equals(xlasseDocumentale) &&
			 */ isDocEntrata != null && isDocEntrata && !(ModalitaRicercaCodeEnum.ENTRATA.equals(queue.getModalitaRicercaCodeEnum())
				|| ModalitaRicercaCodeEnum.ENTRATA_USCITA.equals(queue.getModalitaRicercaCodeEnum()))) {
			skypCycle = true;
		}
		// Se è un documento in entrata non FEPA ma la coda non è relativa alle entrate
		// salto
		if (/*
			 * pp.getParameterByKey(PropertiesNameEnum.
			 * DOCUMENTO_GENERICO_CLASSNAME_FN_METAKEY).equals(xlasseDocumentale) &&
			 */ isDocEntrata != null && !isDocEntrata && !(ModalitaRicercaCodeEnum.USCITA.equals(queue.getModalitaRicercaCodeEnum())
				|| ModalitaRicercaCodeEnum.ENTRATA_USCITA.equals(queue.getModalitaRicercaCodeEnum()))) {
			skypCycle = true;
		}

		if (!PermessiUtils.haAccessoAllaFunzionalita(queue.getAccessFun(), utente.getPermessi(), utente.getPermessiAOO(), utente.getIdTipoNodo())) {

			skypCycle = true;

		} else {

			if (utente.getIdUfficio().equals(utente.getIdNodoAssegnazioneIndiretta()) && queue.equals(DocumentQueueEnum.CORRIERE)) {
				skypCycle = true;
			}

			if (!utente.getIdUfficio().equals(utente.getIdNodoAssegnazioneIndiretta())
					&& (queue.equals(DocumentQueueEnum.CORRIERE_DIRETTO) || queue.equals(DocumentQueueEnum.CORRIERE_INDIRETTO))) {
				skypCycle = true;
			}

			if (utente.isUcb() && queue.equals(DocumentQueueEnum.DA_LAVORARE)) {
				skypCycle = true;
			}
			if (!utente.isUcb() && (queue.equals(DocumentQueueEnum.DA_LAVORARE_UCB) || queue.equals(DocumentQueueEnum.IN_LAVORAZIONE_UCB))) {
				skypCycle = true;
			}
		}
		return skypCycle;
	}

	/**
	 * Metodo per il recupero di una coda Filenet.
	 * 
	 * @param queue                    enum per determinare la coda da ricercare
	 * @param utente                   DTO utente per determinare cono visibilita
	 * @param documentTitlesToFilterBy id del/i documento/i da cercare nella coda -
	 *                                 può essere null
	 * @param connection               connection
	 * @return insieme di risultati ottenuti dalla query - da Trasformare -
	 */
	@Override
	public VWQueueQuery getQueueFilenet(final DocumentQueueEnum queue, final UtenteDTO utente, final Collection<String> documentTitlesToFilterBy, final FilenetPEHelper fpeh,
			final Connection connection) {
		return getQueueFilenet(queue, utente, documentTitlesToFilterBy, false, fpeh, connection);
	}

	/**
	 * Metodo per il recupero di una coda Filenet.
	 *
	 * @param queue						enum per determinare la coda da ricercare
	 * @param utente					DTO utente per determinare cono visibilita
	 * @param documentTitlesToFilterBy	id del/i documento/i da cercare nella coda - può essere null
	 * @param connection				connection
	 * @return							insieme di risultati ottenuti dalla query - da trasformare -
	 */
	@Override
	public VWQueueQuery getQueueFilenet(final DocumentQueueEnum queue, final UtenteDTO utente, final Collection<String> documentTitlesToFilterBy,
			final Connection connection) {
		return getQueueFilenet(queue, utente, documentTitlesToFilterBy, false, connection);
	}

	/**
	 * @param queue
	 * @param utente
	 * @param documentTitlesToFilterBy
	 * @param connection
	 * @return
	 */
	@Override
	public VWQueueQuery getQueueFilenetPerPaginazione(final DocumentQueueEnum queue, final UtenteDTO utente, final Collection<String> documentTitlesToFilterBy,
			final Connection connection) {
		return getQueueFilenet(queue, utente, documentTitlesToFilterBy, connection);
	}

	/**
	 * Metodo per il recupero di una coda Filenet. N.B. I documenti restituiti sono
	 * quelli con data scadenza valorizzata.
	 * 
	 * @param queue                    enum per determinare la coda da ricercare
	 * @param utente                   DTO utente per determinare cono visibilita
	 * @param documentTitlesToFilterBy id del/i documento/i da cercare nella coda -
	 *                                 può essere null
	 * @param connection               connection
	 * @return insieme di risultati ottenuti dalla query - da Trasformare -
	 */
	@Override
	public VWQueueQuery getQueueFilenetConScadenza(final DocumentQueueEnum queue, final UtenteDTO utente, final Collection<String> documentTitlesToFilterBy,
			final FilenetPEHelper fpeh, final Connection connection) {
		return getQueueFilenet(queue, utente, documentTitlesToFilterBy, true, fpeh, connection);
	}

	/**
	 * Metodo per il recupero di una coda Filenet. N.B. I documenti restituiti sono
	 * quelli con data scadenza valorizzata.
	 * 
	 * @param queue                    enum per determinare la coda da ricercare
	 * @param utente                   DTO utente per determinare cono visibilita
	 * @param documentTitlesToFilterBy id del/i documento/i da cercare nella coda -
	 *                                 può essere null
	 * @param connection               connection
	 * @return insieme di risultati ottenuti dalla query - da Trasformare -
	 */
	@Override
	public VWQueueQuery getQueueFilenetConScadenza(final DocumentQueueEnum queue, final UtenteDTO utente, final Collection<String> documentTitlesToFilterBy,
			final Connection connection) {
		return getQueueFilenet(queue, utente, documentTitlesToFilterBy, true, connection);
	}
	
	
	/**
	 * @param queue
	 * @param utente
	 * @param documentTitlesToFilterBy
	 * @param conScadenza
	 * @param connection
	 * @return
	 */
	private VWQueueQuery getQueueFilenet(final DocumentQueueEnum queue, final UtenteDTO utente, final Collection<String> documentTitlesToFilterBy, 
			final boolean conScadenza, final Connection connection) {
		FilenetPEHelper fpeh = null;

		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			return getQueueFilenet(queue, utente, documentTitlesToFilterBy, conScadenza, fpeh, connection);
		} finally {
			logoff(fpeh);
		}
	}

	/**
	 * Metodo per il recupero di una coda FileNet.
	 * 
	 * @param queue                    enum per determinare la coda da ricercare
	 * @param utente                   DTO utente per determinare cono visibilita
	 * @param documentTitlesToFilterBy id del/i documento/i da cercare nella coda -
	 *                                 può essere null
	 * @param idsUtenteDestinatario    IDs degli utenti destinatari
	 * @param idsTipoAssegnazione      IDs dei tipi assegnazione
	 * @param docConScadenza           indica se cercare o meno documenti con la
	 *                                 data scadenza valorizzata
	 * @param connection               connection
	 * @return insieme di risultati ottenuti dalla query - da Trasformare -
	 */
	private VWQueueQuery getQueueFilenet(final DocumentQueueEnum queue, final UtenteDTO utente, final Collection<String> documentTitlesToFilterBy,
			final Boolean docConScadenza, final FilenetPEHelper fpeh, final Connection connection) {
		final ArrayList<Long> idsUtenteDestinatario = new ArrayList<>();
		final ArrayList<Long> idsTipoAssegnazione = new ArrayList<>();

		Date dataScadenzaDa = null;
		// N.B. RED, cercando i documenti in scadenza nelle diverse code per il tab
		// "Attività ufficiali" del Calendario, in realtà cerca documenti
		// con la data scadenza valorizzata (quindi anche già scaduti), in quanto nella
		// query imposta una data scadenza maggiore di una data fake passata.
		// Qui viene replicato questo comportamento.
		if (Boolean.TRUE.equals(docConScadenza)) {
			dataScadenzaDa = Date.from(DateUtils.NULL_DATE.atZone(ZoneId.systemDefault()).toInstant());
		}

		// Informazioni comuni a tutte le code
		final String indexName = queue.getIndexName();
		final Boolean registroRiservato = false;
		final Long idNodoDestinatario = utente.getIdUfficio();
		idsUtenteDestinatario.add(utente.getId());
		final String idClient = utente.getFcDTO().getIdClientAoo();
		Integer flagRenderizzato = null;

		// Imposto parametri specifici nel caso la coda lo richieda
		if (DocumentQueueEnum.NSD.equals(queue) || DocumentQueueEnum.NSD_LIBRO_FIRMA_DELEGATO.equals(queue)) {
			// Imposto il flag Renderizzato
			flagRenderizzato = BooleanFlag.TRUE;

			// Imposto i tipi Assegnazione di riferimento Firma, Sigla, Visto, Copia
			// Conforme e Firma Multipla
			// solo se non sto cercando documenti con la data scadenza valorizzata
			if (Boolean.FALSE.equals(docConScadenza)) {
				idsTipoAssegnazione.add(Long.valueOf(TipoAssegnazioneEnum.FIRMA.getId()));
				idsTipoAssegnazione.add(Long.valueOf(TipoAssegnazioneEnum.SIGLA.getId()));
				idsTipoAssegnazione.add(Long.valueOf(TipoAssegnazioneEnum.VISTO.getId()));
				idsTipoAssegnazione.add(Long.valueOf(TipoAssegnazioneEnum.COPIA_CONFORME.getId()));
				idsTipoAssegnazione.add(Long.valueOf(TipoAssegnazioneEnum.FIRMA_MULTIPLA.getId()));
			}
		}

		if (DocumentQueueEnum.NSD_LIBRO_FIRMA_DELEGATO.equals(queue) && utenteDAO.isDelegato(utente.getIdRuolo(), connection)) {
			// Nel caso l'utente abbia tra i suoi permessi la delega aggiungo ai destinatari
			// anche il Dirigente
			final Utente dirigente = nodoDAO.getNodo(utente.getIdUfficio(), connection).getDirigente();
			if (dirigente != null) {
				// Verificato che l'utente possa vedere il Libro Firma del suo Dirigente si
				// svuota la lista e viene aggiunto solo l'id del Dirigente
				idsUtenteDestinatario.clear();
				idsUtenteDestinatario.add(dirigente.getIdUtente());
			}
		}

		// Si elimina il destinatario nel caso la coda faccia parte del gruppo UFFICIO o
		// si tratti della coda IN_ACQUISIZIONE (Documenti Cartacei)
		if (queue.getGroup().equals(QueueGroupEnum.UFFICIO) || DocumentQueueEnum.IN_ACQUISIZIONE.equals(queue)) {
			idsUtenteDestinatario.clear();
		}

		// Si esegue query generica per il recupero dei workflow delle code FileNet
		return fpeh.getWorkFlowsByWobQueueFilterRed(null, // WOB Number
				queue, // Coda
				indexName, // Index Name
				idNodoDestinatario, // ID Nodo Destinatario
				idsUtenteDestinatario, // ID Utente/i destinatari/io
				idClient, // ID Client AOO
				idsTipoAssegnazione, // ID Tipo Assegnazione
				flagRenderizzato, // Flag Renderizzato
				registroRiservato, // Registro Riservato
				documentTitlesToFilterBy, // Document title se si proviene dalla ricerca o altre pagine "esterne"
				null, dataScadenzaDa, // DataScadenzaDa
				null, false);
	}

	/**
	 * @param documentTitle
	 * @param idUfficioDestinatario
	 * @param idUtenteDestinatario
	 * @param utenteRichiedente
	 * @return
	 */
	@Override
	public final boolean isDocumentoInLibroFirmaUtente(final String documentTitle, final Long idUfficioDestinatario, final Long idUtenteDestinatario,
			final UtenteDTO utenteRichiedente) {
		boolean isInLibroFirmaUtente = false;
		FilenetPEHelper fpeh = null;
		Connection con = null;
		try {
			fpeh = new FilenetPEHelper(utenteRichiedente.getFcDTO());
			con = setupConnection(getDataSource().getConnection(), false);
			
			Nodo ufficioDestinatario = nodoDAO.getNodo(idUfficioDestinatario, con);
			
			VWQueueQuery queueQuery = fpeh.getWorkFlowsByWobQueueFilterRed(null,    														// WOB Number
																			DocumentQueueEnum.NSD, 											// Coda
																			DocumentQueueEnum.NSD.getIndexName(), 							// Index Name
																			idUfficioDestinatario, 											// ID Nodo Destinatario
																			Arrays.asList(idUtenteDestinatario), 							// ID Utente destinatario
																			ufficioDestinatario.getAoo().getAooFilenet().getIdClientAoo(), 	// ID Client AOO
																			Arrays.asList(
																				Long.valueOf(TipoAssegnazioneEnum.FIRMA.getId()),
																				Long.valueOf(TipoAssegnazioneEnum.FIRMA_MULTIPLA.getId()),
																				Long.valueOf(TipoAssegnazioneEnum.SIGLA.getId()),	
																				Long.valueOf(TipoAssegnazioneEnum.VISTO.getId()),
																				Long.valueOf(TipoAssegnazioneEnum.COPIA_CONFORME.getId())
																			), 																// IDs Tipo Assegnazione 
																			BooleanFlag.TRUE, 												// Flag Renderizzato
																			false,															// Registro Riservato
																			Arrays.asList(documentTitle),									// Document Title
																			null,
																			null,															// Data Scadenza Da
																			null, 
																			false);
			
			if (queueQuery != null) {
				isInLibroFirmaUtente = (queueQuery.next() != null);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException("Si è verificato un errore durante la verifica della presenza nel Libro Firma dell'utente: " + idUtenteDestinatario + " (ufficio: "
					+ idUfficioDestinatario + ") del documento: " + documentTitle, e);
		} finally {
			logoff(fpeh);
			closeConnection(con);
		}
		return isInLibroFirmaUtente;
	}

	/**
	 * Merge tra gli oggetti ottenuti dal PE e dal CE.
	 * 
	 * @param peDocs
	 * @param ceDocs
	 */
	private Collection<MasterDocumentRedDTO> mergePeCeTandem(final HashMap<String, Collection<TandemDTO>> tandem, final Collection<MasterDocumentRedDTO> docCE,
			final UtenteDTO utente, final Connection conn, final DocumentQueueEnum queue) {
		final Collection<MasterDocumentRedDTO> output = new ArrayList<>();

		if (!tandem.isEmpty()) {
			for (final MasterDocumentRedDTO dce : docCE) {
				final Collection<TandemDTO> t = tandem.get(dce.getDocumentTitle());
				if (t != null && !t.isEmpty()) {
					for (final TandemDTO dto : t) {
						// se ci sono risultati vanno ciclati e mergiati n volte con il clone
						// predisposto
						final MasterDocumentRedDTO dolly = new MasterDocumentRedDTO(dce);
						dolly.fill(dto.getDatiPE());
						updateIcone(dolly, utente, conn);
						output.add(dolly);
					}
				} else {
					throw new RedException("Non è stato trovato nessun dato nel PE per effettuare un merge");
				}
			}
		} else {
			for (final MasterDocumentRedDTO dce : docCE) {
				dce.setQueue(queue);
				updateIcone(dce, utente, conn);
				output.add(dce);
			}
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.IListaDocumentiSRV#updateIcone(it.ibm.red.business.dto.MasterDocumentRedDTO,
	 *      it.ibm.red.business.dto.UtenteDTO, java.sql.Connection).
	 */
	@Override
	public void updateIcone(final MasterDocumentRedDTO master, final UtenteDTO utente, final Connection connection) {
		updateIconaStato(master, utente, connection);
		updateIconaFlusso(master);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IListaDocumentiFacadeSRV#getMasterFromScrivania(it.ibm.red.business.enums.DocumentQueueEnum,
	 *      it.ibm.red.business.dto.NodoOrganigrammaDTO,
	 *      it.ibm.red.business.dto.FilenetCredentialsDTO).
	 */
	@Override
	public final Collection<MasterDocumentRedDTO> getMasterFromScrivania(final DocumentQueueEnum queue, final NodoOrganigrammaDTO nodoPadre,
			final FilenetCredentialsDTO fcDto) {
		Collection<MasterDocumentRedDTO> output = new ArrayList<>();
		final UtenteDTO datiAccesso = new UtenteDTO(fcDto, nodoPadre);

		try {
			// se specificata navigo verso la coda richiesta, altrimenti sono state
			// richieste le code di un utente
			if (queue != null) {
				output = getDocumentForMaster(queue, null, datiAccesso);
			} else {
				// eseguo due query separate perchè navigando dall'organigramma verso l'utente
				// va mostrato l'unione della coda 'da lavorare' e quella 'sospeso'
				// non serve differenziare per UCB in quanto la coda recupera tutti i documenti
				// che insistono sulla coda Da Lavorare dell'utente, anche nella coda logica In
				// Lavorazione
				output.addAll(getDocumentForMaster(DocumentQueueEnum.DA_LAVORARE, null, datiAccesso));
				output.addAll(getDocumentForMaster(DocumentQueueEnum.SOSPESO, null, datiAccesso));
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei documenti partendo dall'Organigramma Scrivania ", e);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IListaDocumentiFacadeSRV#getDocInErroreCodaSpedizione(it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public final List<String> getDocInErroreCodaSpedizione(final UtenteDTO utente) {
		Connection con = null;
		FilenetPEHelper fpeh = null;
		final List<String> documentTitleListInErroreSpedizione = new ArrayList<>();

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			// Si recuperano i document title da workflow e si recuperano quelli andati in
			// errore tramite query sulla tabella GESTIONENOTIFICHEMAIL
			final List<String> documentTitleListInSpedizione = fpeh.getIdDocumenti(getQueueFilenet(DocumentQueueEnum.SPEDIZIONE, utente, null, fpeh, con));

			if (!CollectionUtils.isEmpty(documentTitleListInSpedizione)) {
				documentTitleListInErroreSpedizione.addAll(gestioneNotificheMailDAO.getDocInErroreCodaSpedizione(documentTitleListInSpedizione, con));
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero dei documenti in spedizione in errore.", e);
		} finally {
			closeConnection(con);
			logoff(fpeh);
		}
		return documentTitleListInErroreSpedizione;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IListaDocumentiFacadeSRV#getInfoSpedizioneWidget(it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public final List<String> getInfoSpedizioneWidget(final UtenteDTO utente) {
		Connection con = null;
		FilenetPEHelper fpeh = null;
		final List<String> documentTitleList = new ArrayList<>();

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			// Si recuperano i document title da workflow e si recuperano quelli nella coda
			// gestionenotificheemail
			final List<String> documentTitleListInSpedizione = fpeh.getIdDocumenti(getQueueFilenet(DocumentQueueEnum.SPEDIZIONE, utente, null, fpeh, con));
			if (!CollectionUtils.isEmpty(documentTitleListInSpedizione)) {
				documentTitleList.addAll(gestioneNotificheMailDAO.getInfoSpedizioneWidget(documentTitleListInSpedizione, con));
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero dei documenti in spedizione in attesa di riconciliazione.", e);
		} finally {
			closeConnection(con);
			logoff(fpeh);
		}
		return documentTitleList;
	}

	/**
	 * @see it.ibm.red.business.service.IListaDocumentiSRV#getQueueFilenetWidget(it.ibm.red.business.enums.DocumentQueueEnum,
	 *      it.ibm.red.business.dto.UtenteDTO, java.util.Collection,
	 *      java.lang.Boolean, java.util.List, boolean,
	 *      it.ibm.red.business.helper.filenet.pe.FilenetPEHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public VWQueueQuery getQueueFilenetWidget(final DocumentQueueEnum queue, final UtenteDTO utente, final Collection<String> documentTitlesToFilterBy,
			final Boolean docConScadenza, final List<Nodo> listUfficio, final boolean inApprovazioneDirigente, final FilenetPEHelper fpeh, final Connection connection) {
		final ArrayList<Long> idsUtenteDestinatario = new ArrayList<>();
		final ArrayList<Long> idsTipoAssegnazione = new ArrayList<>();

		// Informazioni comuni a tutte le code
		final String queueName = queue.getName();
		final String indexName = queue.getIndexName();
		final Boolean registroRiservato = false;
		idsUtenteDestinatario.add(utente.getId());
		final String idClient = utente.getFcDTO().getIdClientAoo();
		Integer flagRenderizzato = null;

		// Imposto parametri specifici nel caso la coda lo richieda
		if (DocumentQueueEnum.NSD.equals(queue) || DocumentQueueEnum.NSD_LIBRO_FIRMA_DELEGATO.equals(queue)) {
			// Imposto il flag Renderizzato
			flagRenderizzato = BooleanFlag.TRUE;

			// Imposto i tipi Assegnazione di riferimento Firma, Sigla, Visto, Copia
			// Conforme e Firma Multipla
			// solo se non sto cercando documenti con la data scadenza valorizzata
			if (Boolean.FALSE.equals(docConScadenza)) {
				idsTipoAssegnazione.add(Long.valueOf(TipoAssegnazioneEnum.FIRMA.getId()));
				idsTipoAssegnazione.add(Long.valueOf(TipoAssegnazioneEnum.SIGLA.getId()));
				idsTipoAssegnazione.add(Long.valueOf(TipoAssegnazioneEnum.VISTO.getId()));
				idsTipoAssegnazione.add(Long.valueOf(TipoAssegnazioneEnum.COPIA_CONFORME.getId()));
				idsTipoAssegnazione.add(Long.valueOf(TipoAssegnazioneEnum.FIRMA_MULTIPLA.getId()));
			}
		}
		if (DocumentQueueEnum.NSD_LIBRO_FIRMA_DELEGATO.equals(queue)) {
			// Nel caso l'utente abbia tra i suoi permessi la delega aggiungo ai destinatari
			// anche il Dirigente
			final Utente dirigente = nodoDAO.getNodo(utente.getIdUfficio(), connection).getDirigente();
			if (dirigente != null) {
				// Verificato che l'utente possa vedere il Libro Firma del suo Dirigente si
				// svuota la lista e viene aggiunto solo l'id del Dirigente
				idsUtenteDestinatario.clear();
				idsUtenteDestinatario.add(dirigente.getIdUtente());
			}
		}
		// Si elimina il destinatario nel caso la coda faccia parte del gruppo UFFICIO o
		// si tratti della coda IN_ACQUISIZIONE (Documenti Cartacei)
		if (queue.getGroup().equals(QueueGroupEnum.UFFICIO) || DocumentQueueEnum.IN_ACQUISIZIONE.equals(queue)) {
			idsUtenteDestinatario.clear();
		}
		// Si esegue query generica per il recupero dei workflow delle code FileNet
		return fpeh.getWorkFlowsByWobQueueFilterRedWidget(null, // WOB Number
				queueName, // Nome Coda
				indexName, // Index Name
				idsUtenteDestinatario, // ID Utente/i destinatari/io
				idClient, // ID Client AOO
				idsTipoAssegnazione, // ID Tipo Assegnazione
				flagRenderizzato, // Flag Renderizzato
				registroRiservato, // Registro Riservato
				documentTitlesToFilterBy, // Document title se si proviene dalla ricerca o altre pagine "esterne"
				null, null, // DataScadenzaDa
				null, false, listUfficio, inApprovazioneDirigente);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IListaDocumentiFacadeSRV#checkDocumentTitleRecall(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.Long).
	 */
	@Override
	public boolean checkDocumentTitleRecall(final String documentTitle, final UtenteDTO utente, final Long idStatoLavorazione) {
		boolean isPresente = false;
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			isPresente = codeApplicativeDAO.checkDocumentTitleRecall(documentTitle, utente, idStatoLavorazione, false, conn);
		} catch (final Exception ex) {
			LOGGER.error("Errore durante il check recall del documento" + documentTitle, ex);
		} finally {
			closeConnection(conn);
		}
		return isPresente;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IListaDocumentiFacadeSRV#getWorkflowPrincipale(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public String getWorkflowPrincipale(final String documentTitle, final UtenteDTO utente) {
		String wobNumber = null;
		FilenetPEHelper fpeh = null;

		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			final VWWorkObject wo = fpeh.getWorkflowPrincipale(documentTitle, utente.getFcDTO().getIdClientAoo());

			if (wo != null) {
				wobNumber = wo.getWorkObjectNumber();
			}
		} catch (final Exception ex) {
			LOGGER.error("Errore durante il recupero del workflow principale", ex);
		} finally {
			logoff(fpeh);
		}

		return wobNumber;
	}

	/**
	 * Assegnazione di un'icona di STATO al master.
	 * 
	 * @param master
	 * @param utente
	 * @param connection
	 */
	private void updateIconaStato(final MasterDocumentRedDTO master, final UtenteDTO utente, final Connection connection) {
		IFilenetCEHelper fceh = null;

		try {
			IconaStatoEnum iconaStato = null;
			final TipoAssegnazioneEnum tipoAssegnazioneMaster = master.getTipoAssegnazione();
			if (tipoAssegnazioneMaster != null) {

				switch (tipoAssegnazioneMaster) {
				case COMPETENZA:
				case FIRMATO:
				case FIRMATO_E_SPEDITO:
					iconaStato = getIconaStato(master);
					break;

				case CONOSCENZA:
					iconaStato = IconaStatoEnum.CONOSCENZA;
					break;

				case CONTRIBUTO:
					iconaStato = IconaStatoEnum.CONTRIBUTO;
					break;

				case FIRMA:
					iconaStato = IconaStatoEnum.FIRMA;
					break;

				case SIGLA:
					iconaStato = getIconaStatoSigla(utente.getIdAoo().intValue());
					break;
				case VISTO:
					iconaStato = IconaStatoEnum.VISTO;
					break;
				case RIFIUTO_ASSEGNAZIONE:
					iconaStato = IconaStatoEnum.RIFIUTO_ASSEGNAZIONE;
					break;
				case RIFIUTO_FIRMA:
					iconaStato = IconaStatoEnum.RIFIUTO_FIRMA;
					break;
				case RIFIUTO_SIGLA:
					iconaStato = IconaStatoEnum.RIFIUTO_SIGLA;
					break;
				case RIFIUTO_VISTO:
					iconaStato = IconaStatoEnum.RIFIUTO_VISTO;
					break;
				case COPIA_CONFORME:
					iconaStato = IconaStatoEnum.COPIA_CONFORME;
					break;
				case SPEDIZIONE:
					iconaStato = getIconaStatoSpedizione(master, utente.getIdAoo(), connection);
					break;
				case FIRMA_MULTIPLA:
					iconaStato = IconaStatoEnum.FIRMA_MULTIPLA;
					break;
				case RIFIUTO_FIRMA_MULTIPLA:
					iconaStato = IconaStatoEnum.RIFIUTO_FIRMA_MULTIPLA;
					break;
				default:
					LOGGER.warn("Tipo Assegnazione " + master.getTipoAssegnazione() + " non previsto");
					break;
				}
				master.setIconaStato(iconaStato);
			}
		} catch (final Exception e) {
			LOGGER.warn("Impossibile aggiornare l'icona per il documento numero: ' " + master.getNumeroDocumento() + " '.", e);
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * Restutisce l'icona stato per la Sigla.
	 * 
	 * @param idAoo
	 *            Identificativo dell'Area organizzativa.
	 * @return Icona stato della Sigla.
	 */
	private static IconaStatoEnum getIconaStatoSigla(final Integer idAoo) {

		IconaStatoEnum iconaStato = IconaStatoEnum.SIGLA;
		final String aooDag = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IDAOO_DSII);

		if (aooDag != null && idAoo == Integer.parseInt(aooDag)) {
			iconaStato = IconaStatoEnum.SIGLA_TITLE_VISTO;
		}
		return iconaStato;
	}

	/**
	 * Recupera l'icona stato associata alla Spedizione.
	 * 
	 * @param master
	 *            Master del documento dal quale recuperare l'icona stato della
	 *            spedizione.
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @param connection
	 *            Connessione al database.
	 * @return Icona stato Spedizione.
	 */
	private IconaStatoEnum getIconaStatoSpedizione(final MasterDocumentRedDTO master, final Long idAoo, final Connection connection) {

		IconaStatoEnum iconaStato = null;
		final List<String[]> destinatariDocumento = master.getDestinatariRaw();
		
		if (!CollectionUtils.isEmpty(destinatariDocumento)) {
			final int totaleDestinatari = destinatariDocumento.size();
			int totCartacei = 0;
			int totElettronici = 0;

			// Calcolo Stato Spedizione delle notifiche.
			int numChiusuraSpedizione = 0;
			int numErroriSpedizione = 0;
			Collection<CodaEmail> notifiche = gestioneNotificheMailDAO.getNotificheEmailForIconaStato(master.getDocumentTitle(), idAoo, connection);
			for (CodaEmail notifica : notifiche) {
			    if (StatoCodaEmailEnum.SPEDITO.getStatus().equals(notifica.getStatoRicevuta()) ||StatoCodaEmailEnum.CHIUSURA.getStatus().equals(notifica.getStatoRicevuta()) ) {
					numChiusuraSpedizione++;
			    } else if (StatoCodaEmailEnum.ERRORE.getStatus().equals(notifica.getStatoRicevuta())) {
					numErroriSpedizione++;
				}
			}
			
			for (final String[] destSplit : destinatariDocumento) {
				if (destSplit.length >= 4) {
					final TipologiaDestinatarioEnum tde = TipologiaDestinatarioEnum.getByTipologia(destSplit[3]);
					if (TipologiaDestinatarioEnum.ESTERNO.equals(tde)) {
						final MezzoSpedizioneEnum mezzo = MezzoSpedizioneEnum.getById(Integer.valueOf(destSplit[2]));
						if (MezzoSpedizioneEnum.ELETTRONICO.equals(mezzo)) {
							totElettronici++;
						} else if (MezzoSpedizioneEnum.CARTACEO.equals(mezzo)) {
							totCartacei++;
						}
					}
				}
			}
			
			
			if (totCartacei == totaleDestinatari) {
				iconaStato = IconaStatoEnum.SPEDIZIONE_CARTACEA;
			} else {
				//conta errori in fase di spedizione

				// Spedizione Omogenea.
				if (totElettronici == totaleDestinatari) {
					
					// @ Verde - Tutte le notifiche sono state spedite.
					if (totaleDestinatari == numChiusuraSpedizione) {
						iconaStato = IconaStatoEnum.SPEDIZIONE_ELETTRONICA_CHIUSA;
					} else if (numErroriSpedizione > 0) {
						// @ Rossa - Almeno una notifica in stato di errore.
						iconaStato = IconaStatoEnum.SPEDIZIONE_ELETTRONICA_ERRORE;
					} else {
						// @ Gialla - Default
						iconaStato = IconaStatoEnum.SPEDIZIONE_ELETTRONICA_ATTESA_RICONCILIAZIONE;
					} 
					
				} else {
					// Spedizione Mista.
					// @ Verde - Tutte le notifiche sono state spedite.
					if (totElettronici == numChiusuraSpedizione) {
						iconaStato = IconaStatoEnum.SPEDIZIONE_MISTA_CHIUSA;
					} else if (numErroriSpedizione > 0) {
						// @ Rossa - Almeno una notifica in stato di errore.
						iconaStato = IconaStatoEnum.SPEDIZIONE_MISTA_ERRORE;
					}  else  {
						// @ Gialla - default.
						iconaStato = IconaStatoEnum.SPEDIZIONE_MISTA_ATTESA_RICONCILIAZIONE;
					} 
					
				}
			}
			
		}
		return iconaStato;
	}

	/**
	 * Restituisce l'icona stato da impostare in base al tipo assegnazione quando la
	 * tipologia è una delle seguenti:
	 * <ul>
	 * 		<li>COMPETENZA</li>
	 * 		<li>FIRMATO</li>
	 * 		<li>FIRMATO_E_SPEDITO</li>
	 * </ul>
	 * 
	 * @param master
	 *            Master per il quale viene impostata l'icona stato.
	 * @return L'icona stato da impostare in base alle caratteristiche del master.
	 */
	private static IconaStatoEnum getIconaStato(final MasterDocumentRedDTO master) {
		IconaStatoEnum iconaStato;
		if (master.getIdFormatoDocumento() != null && master.getIdFormatoDocumento() == FormatoDocumentoEnum.PRECENSITO.getId().intValue()) {
			iconaStato = IconaStatoEnum.PRECENSITO;
		} else {

			final CategoriaDocumentoEnum categoriaMaster = CategoriaDocumentoEnum.getExactly(master.getIdCategoriaDocumento());

			if (CategoriaDocumentoEnum.ASSEGNAZIONE_INTERNA.equals(categoriaMaster)) {
				iconaStato = IconaStatoEnum.DOCUMENTO_ASSEGNAZIONE_INTERNA;
			} else {
				iconaStato = IconaStatoEnum.COMPETENZA;
			}
		}
		return iconaStato;
	}

	/**
	 * Assegnazione di un'icona per l'identificazione del FLUSSO (se presente) a cui
	 * appartiene il master.
	 * 
	 * @param master
	 */
	private void updateIconaFlusso(final MasterDocumentRedDTO master) {
		if (!StringUtils.isNullOrEmpty(master.getCodiceFlusso())) {
			final IAzioniFlussoBaseSRV azioniFlussoBaseSRV = azioniFlussoSRV.retrieveAzioniFlussoSRV(master.getCodiceFlusso());
			if (azioniFlussoBaseSRV != null) {

				final IconaFlussoEnum iconaFlusso = azioniFlussoBaseSRV.getIconaFlusso();
				if (iconaFlusso != null) {
					master.setIconaFlusso(iconaFlusso);
				}
			}
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IListaDocumentiFacadeSRV#recuperaUfficioPadrePerReport(java.lang.Long).
	 */
	@Override
	public Nodo recuperaUfficioPadrePerReport(final Long idNodo) {
		Nodo nodo = null;
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			nodo = nodoDAO.getNodo(idNodo, conn);
		} catch (final Exception ex) {
			rollbackConnection(conn);
			LOGGER.error("Errore durante il recuper del nodo padre" + ex);
			throw new RedException("Errore durante il recuper del nodo padre" + ex);
		} finally {
			closeConnection(conn);
		}
		return nodo;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IListaDocumentiFacadeSRV#recuperaUfficioPadrePerReportNonCompetenza(java.lang.String,
	 *      java.lang.Long, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Integer recuperaUfficioPadrePerReportNonCompetenza(final String documentTitle, final Long idNodo, final UtenteDTO utente) {
		FilenetPEHelper fpeh = null;
		Integer idNodoDestinatario = null;
		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			final VWWorkObject wo = fpeh.getWorkflowPrincipale(documentTitle, utente.getFcDTO().getIdClientAoo());

			if (wo != null) {
				idNodoDestinatario = (Integer) TrasformerPE.getMetadato(wo, PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY);
			}
		} catch (final Exception ex) {
			LOGGER.error("Errore durante il recupero del nodo destinatario" + ex);
			throw new RedException("Errore durante il recuper del nodo destinatario" + ex);
		} finally {
			logoff(fpeh);
		}
		return idNodoDestinatario;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IListaDocumentiFacadeSRV#getMetadatoIdUtenteFirmatario(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Long getMetadatoIdUtenteFirmatario(final String documentTitle, final UtenteDTO utente) {
		Long idUtenteFirmatario = null;
		FilenetPEHelper fpeh = null;
		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			final VWWorkObject wo = fpeh.getWorkflowPrincipale(documentTitle, utente.getFcDTO().getIdClientAoo());

			if (wo != null) {
				idUtenteFirmatario = Long.valueOf((Integer) TrasformerPE.getMetadato(wo, PropertiesNameEnum.ID_UTENTE_FIRMATARIO_WF_METAKEY));
			}
		} catch (final Exception ex) {
			LOGGER.error("Errore durante il check recall della coda in approvazione", ex);
		} finally {
			logoff(fpeh);
		}
		return idUtenteFirmatario;
	}
}