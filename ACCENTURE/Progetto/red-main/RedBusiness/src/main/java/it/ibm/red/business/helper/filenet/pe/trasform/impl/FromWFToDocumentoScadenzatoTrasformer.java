package it.ibm.red.business.helper.filenet.pe.trasform.impl;

import java.util.Date;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.PEDocumentoScadenzatoDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.logger.REDLogger;

/**
 * Trasformer da WF al Documento Scadenzato.
 */
public class FromWFToDocumentoScadenzatoTrasformer extends FromWFToDocumentoAbstractTrasformer<PEDocumentoScadenzatoDTO> {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 3446357036508385949L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FromWFToDocumentoScadenzatoTrasformer.class.getName());
	
	/**
	 * Costruttore.
	 * @param inEnumKey
	 */
	FromWFToDocumentoScadenzatoTrasformer(final TrasformerPEEnum inEnumKey) {
		super(inEnumKey);
	}
	
	/**
	 * Costruttore.
	 */
	public FromWFToDocumentoScadenzatoTrasformer() {
		super(TrasformerPEEnum.FROM_WF_TO_DOCUMENTO_SCADENZATO);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.pe.trasform.impl.FromWFToDocumentoAbstractTrasformer#createAbstract(filenet.vw.api.VWWorkObject,
	 *      java.lang.String, java.lang.String, java.lang.Integer, java.lang.String,
	 *      it.ibm.red.business.enums.DocumentQueueEnum, java.lang.Integer,
	 *      java.lang.Integer).
	 */
	@Override
	PEDocumentoScadenzatoDTO createAbstract(final VWWorkObject object, final String wobNumber, final String idDocumento,
			final Integer tipoAssegnazioneId, final String codaCorrente, final DocumentQueueEnum queue, final Integer idNodoDestinatario,
			final Integer idUtenteDestinatario) {
		
		Date dataCreazione = (Date) getMetadato(object, PropertiesNameEnum.DATA_CREAZIONE_METAKEY);
		Date dataScadenza = (Date) getMetadato(object, PropertiesNameEnum.DATA_SCADENZA_METAKEY);
		
		return new PEDocumentoScadenzatoDTO(idDocumento, wobNumber, tipoAssegnazioneId, codaCorrente, queue, idNodoDestinatario, idUtenteDestinatario, dataCreazione, dataScadenza);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.pe.trasform.impl.FromWFToDocumentoAbstractTrasformer#getLogger().
	 */
	@Override
	protected REDLogger getLogger() {
		return LOGGER;
	}

}
