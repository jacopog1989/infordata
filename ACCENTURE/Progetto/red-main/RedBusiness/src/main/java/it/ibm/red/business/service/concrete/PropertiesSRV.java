package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IPropertiesDAO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Property;
import it.ibm.red.business.service.IPropertiesSRV;

/**
 * The Class PropertiesSRV.
 *
 * @author CPIERASC
 * 
 *         Servizio gestione properties.
 */
@Service
@Component
public class PropertiesSRV extends AbstractService implements IPropertiesSRV {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -1720173749210830823L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(PropertiesSRV.class.getName());

	/**
	 * Dao gestione properties.
	 */
	@Autowired
	private IPropertiesDAO propertiesDAO;
	

	/**
	 * 
	 * tags.
	 * 
	 * @return
	 */
	@Override
	public final Map<String, String> getAll() {
		Map<String, String> output = null;
		Connection connection =  null;
		
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			Collection<Property> properties = propertiesDAO.getAllValues(connection);
			Collection<Property> blobProperties = propertiesDAO.getAllBlobValues(connection);
			
			if (properties != null) {
				output = new HashMap<>();
				for (Property p : properties) {
					output.put(p.getKey(), p.getValue());
				}
			}
			
			if (blobProperties != null) {
				if (output == null) {
					output = new HashMap<>();
				}
				for (Property p : blobProperties) {
					output.put(p.getKey(), p.getValue());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Errore nel recupero delle properties dal DB: " + e.getMessage(), e);
		} finally {
			closeConnection(connection);
		}
		
		return output;
	}
	

	/**
	 * Gets the by enum.
	 *
	 * @param param
	 *            the param
	 * @return the by enum
	 */
	@Override
	public final String getByEnum(final PropertiesNameEnum param) {
		Connection connection =  null;
		
		try {
			if (param == null) {
				throw new RedException("Non è stata definita la chiave di ricerca della property.");
			}
			connection = setupConnection(getDataSource().getConnection(), false);
			
			return propertiesDAO.getByEnum(param.getKey(), connection);
		} catch (Exception e) {
			String tmp = "Errore nel recupero della property dal DB tramite";
			if (param != null) {
				tmp += " key=" + param.getKey();
			}
			LOGGER.error(tmp, e);
		} finally {
			closeConnection(connection);
		}
		
		return null;
	}

}
