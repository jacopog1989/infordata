package it.ibm.red.business.helper.filenet.pe.trasform.impl;


import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import filenet.vw.api.VWException;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.PEDocumentoDTO;
import it.ibm.red.business.enums.ColoreNotaEnum;
import it.ibm.red.business.enums.ContextTrasformerPEEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoOperazioneLibroFirmaEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.ContextTrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.utils.DateUtils;

/**
 * The Class ContextFromWFToDocumentoTrasformer.
 *
 * @author m.crescentini
 * 
 *         Oggetto per trasformare un workflow in un documento con contesto.
 */
public class ContextFromWFToDocumentoTrasformer extends ContextTrasformerPE<PEDocumentoDTO> {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ContextFromWFToDocumentoTrasformer.class.getName());

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1705454340138423161L;
	
    /**
     * Properties.
     */
	private final PropertiesProvider pp;


	/**
	 * Costruttore.
	 */
	public ContextFromWFToDocumentoTrasformer() {
		super(TrasformerPEEnum.FROM_WF_TO_DOCUMENTO_CONTEXT);
		pp = PropertiesProvider.getIstance();
	}
	
	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.ContextTrasformerPE#trasform(filenet.vw.api.VWWorkObject,
	 *      java.util.Map).
	 */
	@SuppressWarnings("unchecked")
	@Override
	public PEDocumentoDTO trasform(final VWWorkObject workflow, final Map<ContextTrasformerPEEnum, Object> context) {
		PEDocumentoDTO documentoWorkflow = null;
		
		try {
			final String idDocumento = getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY)).toString();
			
			final Date dataAssegnazione = (Date) getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.DATA_ASSEGNAZIONE_WF_METAKEY));

			
			final String wobNumber = retrieveWobNumber(workflow);
			final String queueName = retrieveQueueName(workflow);
			
			final Integer idUtenteDestinatario = (Integer) getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY));
			final Integer idNodoDestinatario = (Integer) getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY));
			final Integer tipoAssegnazioneId = (Integer) getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY));
			final String codaCorrente = (String) getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.NOME_CODA_METAKEY));
	
			final String[] responses = workflow.getStepResponses();
			final String stepName = workflow.getStepName();
			final String fClass = (String) getMetadato(workflow, Constants.PEProperty.VW_CLASS);
			final String dataCreazioneWF = DateUtils.dateToString((Date) getMetadato(workflow, Constants.PEProperty.IW_LAUNCHDATE), null);
			final Date dataScadenzaWF = (Date) getMetadato(workflow, PropertiesNameEnum.DATA_SCADENZA_METAKEY);
			final String subject = (String) getMetadato(workflow, Constants.PEProperty.F_SUBJECT);
			Boolean bFirmaFig = false;
			if ("RED_FirmaRagGenDelloStato".equalsIgnoreCase(fClass)) {
				final Integer firmaDigRGS = (Integer) getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.FIRMA_DIG_RGS_WF_METAKEY));
				if (firmaDigRGS != null && firmaDigRGS > 0) {
					bFirmaFig = true;
				}
			}

			Boolean urgente = false;
			final Integer flagUrgente = (Integer) getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.URGENTE_METAKEY));
			if (flagUrgente != null && flagUrgente == 1) {
				urgente = true;
			}
			
			final Integer flagIterManuale = (Integer) getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.FLAG_ITER_MANUALE_WF_METAKEY));
			final Integer idFascicolo = (Integer) getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY));
			final String motivazioneAssegnazione = (String) getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY));
			int tipoFirma = 0;
			
			final Integer tipoFirmaInteger = (Integer) getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_FIRMA_FN_METAKEY));
			if (tipoFirmaInteger != null) {
				tipoFirma = tipoFirmaInteger.intValue();
			}
			
			final String [] metadatiName = workflow.getFieldNames();
			boolean isElencoLibroFirma = false;
			for (int i = 0; i < metadatiName.length; i++) {
				if (metadatiName[i].equals(pp.getParameterByKey(PropertiesNameEnum.ELENCO_LIBROFIRMA_METAKEY))) {
					isElencoLibroFirma = true;
					break;
				}
			}
			
			String[] elencoLibroFirma = null;
			if (isElencoLibroFirma) {
				elencoLibroFirma = (String[]) getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.ELENCO_LIBROFIRMA_METAKEY));
			}
			
			final Integer count = (Integer)  getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.COUNT_METAKEY));
			final TipoOperazioneLibroFirmaEnum tolf = TipoOperazioneLibroFirmaEnum.get("" + tipoAssegnazioneId);
			final Integer flagRenderizzato = (Integer) getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.FLAG_RENDERIZZATO_METAKEY));
			Boolean flagRenderizzatoBool = Boolean.FALSE;
			if (flagRenderizzato != null) {
				flagRenderizzatoBool = (flagRenderizzato.intValue() == 1);
			}
			final Boolean firmaCopiaConforme = (Boolean) getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.FIRMA_COPIA_CONFORME_METAKEY));
			final String nota = (String) getMetadato(workflow, "F_Comment");
			
			Integer contributiRichiesti = null;
			Integer contributiPervenuti = null;
			
			if (fClass.startsWith("Contributi_Ufficio")) {
				contributiRichiesti = (Integer) getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.CONTRIBUTI_RICHIESTI_METAKEY));
				contributiPervenuti = (Integer) getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.CONTRIBUTI_PERVENUTI_METAKEY));
			}
			
			
			String assegnatario = Constants.EMPTY_STRING;
			
			if (context.get(ContextTrasformerPEEnum.UFFICI_MITTENTI) != null) {
				final Long idUfficioMittente = Long.valueOf((Integer) TrasformerPE.getMetadato(workflow, PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY));
				
				final String descrizioneUfficio = ((Map<Long, String>) context.get(ContextTrasformerPEEnum.UFFICI_MITTENTI)).get(idUfficioMittente);
				if (StringUtils.isNotBlank(descrizioneUfficio)) {
					assegnatario = descrizioneUfficio;
				}
				
				if (context.get(ContextTrasformerPEEnum.UTENTI_MITTENTI) != null) {
					final Long idUtenteMittente = Long.valueOf((Integer) TrasformerPE.getMetadato(workflow, PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY));

					final String descrizioneUtente = ((Map<Long, String>) context.get(ContextTrasformerPEEnum.UTENTI_MITTENTI)).get(idUtenteMittente);
					if (StringUtils.isNotBlank(descrizioneUtente)) {
						assegnatario += " - " + descrizioneUtente;
					}
				}
			}
			
			Boolean inLavorazione = null;
			if (context.get(ContextTrasformerPEEnum.UCB) != null && (boolean) context.get(ContextTrasformerPEEnum.UCB)) {
				inLavorazione = getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.IN_LAVORAZIONE_METAKEY)) != null 
						&& ((Integer) getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.IN_LAVORAZIONE_METAKEY))) == 1;
			}
			
			final Integer annullaTemplateInt = (Integer) getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.ANNULLA_TEMPLATE_METAKEY));
			boolean annullaTemplate = false;
			if (annullaTemplateInt != null) {
				annullaTemplate = (annullaTemplateInt.intValue() == 1);
			}
			
			Boolean firmaAsincronaAvviata = (Boolean) getMetadato(workflow, pp.getParameterByKey(PropertiesNameEnum.FLAG_FIRMA_ASINCRONA_AVVIATA_WF_METAKEY));
			
			documentoWorkflow = new PEDocumentoDTO(dataAssegnazione, idDocumento, tolf, wobNumber, DocumentQueueEnum.get(queueName, inLavorazione), idUtenteDestinatario, 
					idNodoDestinatario, bFirmaFig, motivazioneAssegnazione, responses, idFascicolo, codaCorrente, tipoAssegnazioneId, flagIterManuale, subject, elencoLibroFirma, 
					count, dataCreazioneWF, flagRenderizzatoBool, firmaCopiaConforme, urgente, stepName, ColoreNotaEnum.getNotaDTO(nota), contributiRichiesti, contributiPervenuti, 
					assegnatario, dataScadenzaWF, tipoFirma, annullaTemplate, firmaAsincronaAvviata);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero di un metadato durante la trasformazione da WorkFlow a PEDocumentoDTO.", e);
		}
		
		return documentoWorkflow;
	}


	/**
	 * Recupera il wobnumber del workflow.
	 * @param workflow
	 * @return wobnumber
	 */
	private String retrieveWobNumber(final VWWorkObject workflow) {
		String wobNumber = null;
		try {
			wobNumber = workflow.getWorkObjectNumber();
		} catch (final VWException e) {
			LOGGER.error("Errore nel recupero del Wob Number", e);
		}
		
		return wobNumber;
	}
	
	/**
	 * Recupera il nomde della coda del workflow.
	 * @param workflow
	 * @return queue name
	 */
	private String retrieveQueueName(final VWWorkObject workflow) {
		String queueName = null;
		try {
			queueName = workflow.getCurrentQueueName();
		} catch (final VWException e) {
			LOGGER.error("Errore nel recupero del Nome Coda.", e);
		}
		
		return queueName;
	}
}
