package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.ILogDAO;
import it.ibm.red.business.dao.IOrganigrammaDAO;
import it.ibm.red.business.dao.IPkHandlerDAO;
import it.ibm.red.business.dao.IRuoloDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.ClientDTO;
import it.ibm.red.business.dto.CoordinatoreUfficioDTO;
import it.ibm.red.business.dto.DelegatoDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.PreferenzeDTO;
import it.ibm.red.business.dto.ResponsabileCopiaConformeDTO;
import it.ibm.red.business.dto.RuoloDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ConfigurazioneFirmaOAMEnum;
import it.ibm.red.business.enums.LogDownloadContentEnum;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.NodoUtenteRuolo;
import it.ibm.red.business.persistence.model.PkHandler;
import it.ibm.red.business.persistence.model.Ruolo;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.persistence.model.UtenteFirma;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAssegnaUfficioSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.utils.PermessiUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class UtenteSRV.
 *
 * @author CPIERASC
 * 
 *         Servizio gestione utenti.
 */
@Service
@Component
public class UtenteSRV extends AbstractService implements IUtenteSRV {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 6199666823801860153L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(UtenteSRV.class.getName());

	/**
	 * Messaggio errore recupero utente. Occorre concatenare l'id utente al
	 * messaggio.
	 */
	private static final String ERROR_RECUPERO_UTENTE_DA_ID_MSG = "Errore nel recupero dell'utente con id =";

	/**
	 * Messaggio errore lettura deleghe per libro firma assegnate.
	 */
	private static final String ERROR_LETTURA_DELEGHE_MSG = "Errore durante la lettura delle Deleghe per il Libro Firma assegnate. ";

	/**
	 * Messaggio errore recupero utente. Occorre concatenare lo username dell'utente
	 * al messaggio.
	 */
	private static final String ERROR_RECUPERO_UTENTE_MSG = "Errore durante il recupero dell'utente tramite USERNAME = ";

	/**
	 * Messaggio errore caricamento informazioni utente.
	 */
	private static final String ERROR_CARICAMENTO_INFORMAZIONI_MSG = "Errore durante il caricamento delle informazioni utente";

	/**
	 * Predefinito.
	 */
	private static final Long PREDEFINITO = 1L;

	/**
	 * Service gestione assegnazione ufficio a utente.
	 */
	@Autowired
	private IAssegnaUfficioSRV assegnaUfficioSRV;

	/**
	 * DAO gestione utenti.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;

	/**
	 * DAO gestione ruoli.
	 */
	@Autowired
	private IRuoloDAO ruoloDAO;

	/**
	 * DAO gestione log.
	 */
	@Autowired
	private ILogDAO logDAO;

	/**
	 * DAO gestione log.
	 */
	@Autowired
	private IPkHandlerDAO pkHandlerDAO;

	/**
	 * DAO gestione organigramma.
	 */
	@Autowired
	private IOrganigrammaDAO organigrammaDAO;

	/**
	 * DAO gestione Aoo.
	 */
	@Autowired
	private IAooDAO aooDAO;

	/**
	 * Fornitore della properties dell'applicazione.
	 */
	private PropertiesProvider pp;

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * get utente by username.
	 *
	 * @param username the username
	 * @return the by username
	 */
	@Override
	public final UtenteDTO getByUsername(final String username) {
		return getByUsername(username, null, null);
	}

	/**
	 * Recupera l'utente tramite lo username. Gestisce inoltre i parametri di firma
	 * (URL dell'handler e tipologia di firma) eventualmente passati dall'OAM.
	 *
	 * @param username        username
	 * @param urlHandlerFirma url dell'handler di firma - valida se inizia con
	 *                        'http' o 'https'
	 * @param codiceTipoFirma tipologia di firma (solo locale, solo remota,
	 *                        entrambe) - valida se in (1, 2, 3)
	 * @return
	 */
	@Override
	public final UtenteDTO getByUsernameAndSignParams(final String username, final String urlHandlerFirma, final String codiceTipoFirma) {
		UtenteDTO output = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			final Utente utente = utenteDAO.getByUsername(username, connection);
			output = getDTOFromModel(utente, null, null, connection);

			// Gestione dell'handler per la firma tramite SSO -> START
			// Valida se inizia con 'http' o 'https'
			if (!StringUtils.isNullOrEmpty(urlHandlerFirma) && (urlHandlerFirma.toLowerCase().startsWith("http://") || urlHandlerFirma.toLowerCase().startsWith("https://"))) {
				final PkHandler pkHandlerOAM = pkHandlerDAO.getByHandler(urlHandlerFirma, connection);

				// Si sovrascrive l'handler PK per la firma con quello ottenuto dall'OAM
				if (output != null) {
					output.getSignerInfo().setPkHandlerFirma(pkHandlerOAM);
				}
			}
			// Gestione dell'handler per la firma tramite SSO -> END

			// Gestione del tipo di firma tramite SSO -> START
			if (!StringUtils.isNullOrEmpty(codiceTipoFirma)) {
				final ConfigurazioneFirmaOAMEnum configurazioneFirmaOAM = ConfigurazioneFirmaOAMEnum.getByCodice(codiceTipoFirma);

				if (configurazioneFirmaOAM != null && output != null) {
					output.getSignerInfo().setConfigurazioneFirma(configurazioneFirmaOAM.getAooConfig());
				}
			}
			// Gestione del tipo di firma tramite SSO -> END
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_UTENTE_MSG + username, e);
		} finally {
			closeConnection(connection);
		}

		return output;
	}

	/**
	 * check mobile preference.
	 *
	 * @param idUtente       the id utente
	 * @param idTemaMobile   the id tema mobile
	 * @param paginaIniziale the pagina iniziale
	 */
	@Override
	public final boolean changeMobilePreference(final long idUtente, final PreferenzeDTO pref) {
		Connection connection = null;
		boolean successChange = true;
		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			utenteDAO.changeMobilePreference(idUtente, pref, connection);
			commitConnection(connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'update delle preferenze del mobile dell'utente con ID=" + idUtente, e);
			rollbackConnection(connection);
			successChange = false;
			throw new RedException("Errore durante la modifica delle preferenze utente", e);
		} finally {
			closeConnection(connection);
		}

		return successChange;
	}

	/**
	 * Gets the by username.
	 *
	 * @param username the username
	 * @param idRuolo  the id ruolo
	 * @param idNodo   the id nodo
	 * @return the by username
	 */
	@Override
	public final UtenteDTO getByUsername(final String username, final Long idRuolo, final Long idNodo) {
		UtenteDTO output = null;
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			final Utente utente = utenteDAO.getByUsername(username, connection);
			// Se non ho il ruolo ma ho l'ufficio, prendi il primo ruolo dell'ufficio
			Long idRuoloNew = idRuolo;
			if ((idRuolo == null || idRuolo == 0) && idNodo != null && idNodo != 0) {
				final List<Long> ruoli = ruoloDAO.getListaRuoliUtenteUfficio(utente.getIdUtente(), idNodo, connection);
				if (ruoli != null && !ruoli.isEmpty()) {
					idRuoloNew = ruoli.get(0);
				}
			}
			output = getByUtente(utente, idRuoloNew, idNodo, connection);
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_UTENTE_MSG + username + ", IDNODO = " + idNodo + ", IDRUOLO = " + idRuolo, e);
		} finally {
			closeConnection(connection);
		}
		return output;
	}

	@Override
	public final String getByUsernameForSpedizione(final String username) {
		String nomeCognome = "";
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			nomeCognome = utenteDAO.getByUsernameForSpedizione(username, connection);

		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_UTENTE_MSG + username, e);
		} finally {
			closeConnection(connection);
		}

		return nomeCognome;
	}

	/**
	 * Restituisce l'utente identificato dall' {@code idUtente}.
	 *
	 * @param username
	 *            Nome utente.
	 * @param idRuolo
	 *            Identificativo del ruolo dell'utente.
	 * @param idNodo
	 *            Identificativo dell'ufficio dell'utente.
	 * @return Utente recuperato.
	 */
	@Override
	public final UtenteDTO getById(final Long idUtente, final Long idRuolo, final Long idNodo) {
		UtenteDTO output = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			output = getById(idUtente, idRuolo, idNodo, connection);

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dell'utente tramite ID=" + idUtente + " IDNODO=" + idNodo + " IDRUOLO=" + idRuolo, e);
		} finally {
			closeConnection(connection);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.IUtenteSRV#getById(java.lang.Long,
	 *      java.lang.Long, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public UtenteDTO getById(final Long idUtente, Long idRuolo, final Long idNodo, final Connection conn) {
		final Utente utente = utenteDAO.getUtente(idUtente, conn);

		// Se non ho il ruolo ma ho l'ufficio, prendi il primo ruolo dell'ufficio
		if ((idRuolo == null || idRuolo == 0) && idNodo != null && idNodo != 0) {
			final List<Long> ruoli = ruoloDAO.getListaRuoliUtenteUfficio(idUtente, idNodo, conn);
			if (ruoli != null && !ruoli.isEmpty()) {
				idRuolo = ruoli.get(0);
			}
		}

		return getDTOFromModel(utente, idRuolo, idNodo, conn);
	}

	/**
	 * @see it.ibm.red.business.service.IUtenteSRV#getById(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public UtenteDTO getById(final Long idUtente, final Connection connection) {
		final Utente utenteModel = utenteDAO.getUtente(idUtente, connection);

		return getDTOFromModel(utenteModel, null, null, connection);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IUtenteFacadeSRV#getUtenteById(java.lang.Long).
	 */
	@Override
	public Utente getUtenteById(final Long idUtente) {
		Utente utente = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			utente = utenteDAO.getUtente(idUtente, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dell'utente con ID: " + idUtente, e);
			throw new RedException(ERROR_CARICAMENTO_INFORMAZIONI_MSG);
		} finally {
			closeConnection(connection);
		}

		return utente;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IUtenteFacadeSRV#getUtenteFirmaById(java.
	 *      lang.Long).
	 */
	@Override
	public UtenteFirma getUtenteFirmaById(final Long idUtente) {
		UtenteFirma utente = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			utente = utenteDAO.getUtenteFirma(idUtente, connection);
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_UTENTE_DA_ID_MSG + idUtente, e);
			throw new RedException(ERROR_CARICAMENTO_INFORMAZIONI_MSG);
		} finally {
			closeConnection(connection);
		}

		return utente;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IUtenteFacadeSRV#haPiuUfficiAttivi(java.
	 *      lang.Long).
	 */
	@Override
	public boolean haPiuUfficiAttivi(final Long idUtente) {
		Collection<NodoUtenteRuolo> utenteRuoli = null;
		Connection connection = null;
		boolean haPiuUffici = false;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			utenteRuoli = utenteDAO.getNodoUtenteRuolo(idUtente, null, connection);
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_UTENTE_DA_ID_MSG + idUtente, e);
			throw new RedException(ERROR_CARICAMENTO_INFORMAZIONI_MSG);
		} finally {
			closeConnection(connection);
		}

		if (utenteRuoli != null && utenteRuoli.size() > 1) {
			haPiuUffici = true;
		}

		return haPiuUffici;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IUtenteFacadeSRV#aggiornaUtenteFirma(java.
	 *      lang.Long, java.lang.String, java.lang.String).
	 */
	@Override
	public int aggiornaUtenteFirma(final Long idUtente, final String key, final String value) {
		int aggiornato = 0;
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			aggiornato = utenteDAO.aggiornaUtenteFirma(idUtente, key, value, connection);
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_UTENTE_DA_ID_MSG + idUtente, e);
			throw new RedException(ERROR_CARICAMENTO_INFORMAZIONI_MSG);
		} finally {
			closeConnection(connection);
		}
		return aggiornato;
	}

	/**
	 * @param utente
	 * @param idRuolo
	 * @param idNodo
	 * @param connection
	 * @return
	 */
	private UtenteDTO getDTOFromModel(final Utente utente, final Long idRuolo, final Long idNodo, final Connection connection) {
		UtenteDTO output = null;

		if (utente != null) {
			output = new UtenteDTO(utente);
			final Collection<NodoUtenteRuolo> nurCollection = utenteDAO.getNodoUtenteRuolo(utente.getIdUtente(), idRuolo, connection);
			final boolean isNodoRuoloSpecified = idNodo != null && idRuolo != null;
			for (final NodoUtenteRuolo nur : nurCollection) {
				if (nur.getRuolo().getDataDisattivazione() != null || nur.getNodo().getDataDisattivazione() != null) {
					continue;
				}
				final RuoloDTO ruolo = new RuoloDTO(nur.getRuolo().getIdRuolo(), nur.getRuolo().getNomeRuolo());
				final String ufficioDesc = assegnaUfficioSRV.getDescrizioneUfficioFromNodo(nur.getNodo());
				final UfficioDTO ufficio = new UfficioDTO(nur.getNodo().getIdNodo(), ufficioDesc);
				// Flag che indica se il cursore sta scandendo il nodo-utente-ruolo da
				// utilizzare per descrivere l'utente loggato
				boolean isNurTarget = PREDEFINITO.equals(nur.getPredefinito());
				if (isNodoRuoloSpecified) {
					isNurTarget = false;
					if (ufficio.getId().equals(idNodo) && ruolo.getId().equals(idRuolo)) {
						isNurTarget = true;
					}
				}
				if (isNurTarget) {
					assegnaUfficioSRV.popolaUtente(output, nur.getNodo(), nur.getRuolo(), nur.getGestioneApplicativa() > 0);
				}
				output.addUfficioRuolo(ufficio, ruolo);
			}
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.IUtenteSRV#getComboResponsabiliCopiaConforme(java
	 *      .lang.Long).
	 */
	@Override
	public List<ResponsabileCopiaConformeDTO> getComboResponsabiliCopiaConforme(final Long idAOO) {
		List<ResponsabileCopiaConformeDTO> output = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			output = utenteDAO.getComboResponsabiliCopiaConforme(idAOO, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei responsabili copia conforme", e);
		} finally {
			closeConnection(connection);
		}

		return output;
	}

	/**
	 * Log download.
	 *
	 * @param idUtente
	 *            Identificativo dell'utente.
	 * @param idRuolo
	 *            Identificativo del ruolo.
	 * @param idUfficio
	 *            Identificativo dell'ufficio.
	 * @param documentTitle
	 *            Identificativo del documento.
	 * @param logDownloadEnum
	 *            Tipologia di log, @see LogDownloadContentEnum.
	 * @param ip
	 *            Indirizzo IP.
	 * @param os
	 *            Sistema operativo.
	 * @param browser
	 *            Browser.
	 * @param userAgent
	 *            User Agent.
	 */
	@Override
	public void logDownload(final Long idUtente, final Long idRuolo, final Long idUfficio, final String documentTitle, final LogDownloadContentEnum logDownloadEnum,
			final String ip, final String os, final String browser, final String userAgent, final String source) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			logDAO.insertLogDownload(idUtente, idRuolo, idUfficio, documentTitle, logDownloadEnum.getLabel(), new ClientDTO(ip, os, browser, userAgent), source, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di audit del download [Utente:" + idUtente + "; Ruolo:" + idRuolo + "; Ufficio:" + idUfficio + "; " + "DocumentTitle:" + documentTitle
					+ "; DocumentDescription:" + logDownloadEnum.getLabel() + "; Source:" + source, e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IUtenteFacadeSRV#logDownload(java.lang.Long,
	 *      java.lang.Long, java.lang.Long, java.lang.String,
	 *      it.ibm.red.business.enums.LogDownloadContentEnum, java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.String).
	 */
	@Override
	public void logDownload(final Long idUtente, final Long idRuolo, final Long idUfficio, final String documentTitle, final LogDownloadContentEnum logDownload,
			final String ip, final String os, final String browser, final String userAgent) {
		logDownload(idUtente, idRuolo, idUfficio, documentTitle, logDownload, ip, os, browser, userAgent, null);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IUtenteFacadeSRV#registerOpenSession(java.lang.String,
	 *      java.lang.String, java.lang.String).
	 */
	@Override
	public void registerOpenSession(final String id, final String ip, final String user) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			utenteDAO.registerOpenSession(id, ip, user, connection);
		} catch (final Exception e) {
			final String msg = "Errore durante l'inserimento dell'audit delle sessioni (apertura).";
			LOGGER.error(msg, e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IUtenteFacadeSRV#registerCloseSession(java.lang.String,
	 *      java.lang.String).
	 */
	@Override
	public void registerCloseSession(final String id, final String user) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			utenteDAO.registerCloseSession(id, user, connection);
		} catch (final Exception e) {
			final String msg = "Errore durante l'inserimento dell'audit delle sessioni (chiusura).";
			LOGGER.error(msg, e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Sets the pin verificato.
	 *
	 * @param idUtente      the id utente
	 * @param pin           the pin
	 * @param pinVerificato the pin verificato
	 * @return true, if successful
	 */
	@Override
	public final boolean setPinVerificato(final Long idUtente, final String pin, final boolean pinVerificato) {
		Connection connection = null;
		boolean output = false;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			utenteDAO.setPinVerificato(idUtente, pin, pinVerificato, connection);
			output = true;
		} catch (final Exception e) {
			LOGGER.error("Errore in fase update del flag PIN VERIFICATO a " + pinVerificato + " per l'utente con ID=" + idUtente, e);
		} finally {
			closeConnection(connection);
		}
		return output;
	}

	/**
	 * Gets the coordinatori ufficio.
	 *
	 * @param idUfficio the id ufficio
	 * @return the coordinatori ufficio
	 */
	@Override
	public final Collection<CoordinatoreUfficioDTO> getCoordinatoriUfficio(final Integer idUfficio) {
		Connection connection = null;
		Collection<CoordinatoreUfficioDTO> output = new ArrayList<>();
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			output = utenteDAO.getCoordinatoriUfficio(idUfficio, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero dei coordinatori dell'ufficio con id " + idUfficio, e);
		} finally {
			closeConnection(connection);
		}
		return output;
	}

	/**
	 * Checks if is corriere.
	 *
	 * @param idRuolo    the id ruolo
	 * @param connection the connection
	 * @return the boolean
	 */
	@Override
	public final Boolean hasPermessoAmministratore(final Long idRuolo) {
		Connection connection = null;
		Boolean isAmministratore = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			isAmministratore = utenteDAO.isAmministratore(idRuolo, connection);
		} catch (final SQLException e) {
			LOGGER.error("Errore in fase di convalidazione del ruolo dell'utente " + idRuolo, e);
		} finally {
			closeConnection(connection);
		}
		return isAmministratore;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IUtenteFacadeSRV#getAmministratoreAoo(java.lang.String).
	 */
	@Override
	public UtenteDTO getAmministratoreAoo(final String codeAoo) {
		return getByUsername(pp.getParameterByString(codeAoo + "." + PropertiesNameEnum.AOO_SISTEMA_AMMINISTRATORE.getKey()), null, null);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IUtenteFacadeSRV#getById(java.lang.Long).
	 */
	@Override
	public UtenteDTO getById(final Long idUtente) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			return getById(idUtente, connection);
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_UTENTE_DA_ID_MSG + idUtente, e);
			throw new RedException(ERROR_CARICAMENTO_INFORMAZIONI_MSG);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IUtenteFacadeSRV#getUtentiInNodo(java.lang.String).
	 */
	@Override
	public final Collection<String> getUtentiInNodo(final String ip) {
		Connection connection = null;
		Collection<String> out = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			out = utenteDAO.getUtentiInNodo(connection, ip);
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero degli utenti in un nodo", e);
		} finally {
			closeConnection(connection);
		}
		return out;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IUtenteFacadeSRV#getSessioniAperte().
	 */
	@Override
	public final Integer getSessioniAperte() {
		Connection connection = null;
		Integer out = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			out = utenteDAO.getSessioniAperte(connection);
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero delle sessioni aperte", e);
		} finally {
			closeConnection(connection);
		}
		return out;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IUtenteFacadeSRV#getSessioniApertePerUtente().
	 */
	@Override
	public final Map<String, Integer> getSessioniApertePerUtente() {
		Connection connection = null;
		Map<String, Integer> out = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			out = utenteDAO.getSessioniApertePerUtente(connection);
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero delle sessioni aperte per utente", e);
		} finally {
			closeConnection(connection);
		}
		return out;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IUtenteFacadeSRV#getSessioniApertePerNodo().
	 */
	@Override
	public final Map<String, Integer> getSessioniApertePerNodo() {
		Connection connection = null;
		Map<String, Integer> out = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			out = utenteDAO.getSessioniApertePerNodo(connection);
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero delle sessioni aperte per nodo", e);
		} finally {
			closeConnection(connection);
		}
		return out;
	}

	/**
	 * @see it.ibm.red.business.service.IUtenteSRV#getByUtente(it.ibm.red.business.persistence.model.Utente,
	 *      java.lang.Long, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public final UtenteDTO getByUtente(final Utente utente, final Long idRuolo, final Long idNodo, final Connection connection) {
		return getDTOFromModel(utente, idRuolo, idNodo, connection);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IUtenteFacadeSRV#addRuoloDelegato(java.lang.Long,
	 *      java.lang.Long, java.lang.Long, java.util.Date, java.util.Date,
	 *      java.lang.String).
	 */
	@Override
	public Boolean addRuoloDelegato(final Long idNodo, final Long idUtente, final Long idAoo, final Date dataAttivazione, final Date dataDisattivazione,
			final String motivoDelega) {
		Boolean output = Boolean.FALSE;
		Connection con = null;
		Long idRuolo = null;

		try {

			con = setupConnection(getDataSource().getConnection(), false);

			final Aoo aoo = aooDAO.getAoo(idAoo, con);
			idRuolo = aoo.getIdRuoloDelegatoLibroFirma();

			output = ruoloDAO.addRuoloToUtente(idNodo, idUtente, idRuolo, 0, dataAttivazione, dataDisattivazione, motivoDelega, con);

		} catch (final Exception e) {
			LOGGER.error("Errore durante l'associazione del Ruolo Delegato al Libro Firma per l'utente con id: " + idUtente + " e idNodo: " + idNodo + " in scadenza il "
					+ dataDisattivazione, e);
			throw new RedException("Errore durante l'associazione del Ruolo Delegato al Libro Firma per l'utente con id: " + idUtente + " e idNodo: " + idNodo
					+ " in scadenza il " + dataDisattivazione, e);
		} finally {
			closeConnection(con);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IUtenteFacadeSRV#getDelegatiLibroFirma(java.lang.Long,
	 *      java.lang.Long, boolean).
	 */
	@Override
	public Collection<DelegatoDTO> getDelegatiLibroFirma(final Long idNodo, final Long idAOO, final boolean allNodes) {
		Collection<DelegatoDTO> output = new ArrayList<>();
		Connection con = null;
		Long idRuolo = null;
		List<Long> idsUffici = null;

		try {
		
			con = setupConnection(getDataSource().getConnection(), false);

			final Aoo aoo = aooDAO.getAoo(idAOO, con);
			idRuolo = aoo.getIdRuoloDelegatoLibroFirma();

			if (!allNodes) {
				final List<NodoOrganigrammaDTO> uffici = organigrammaDAO.getNodiConDirigente(idNodo, idAOO, null, con);

				idsUffici = new ArrayList<>();
				// Viene aggiunto il Nodo con cui l'utente è loggato cosi da includere anche le
				// assegnazioni di questo ufficio.
				idsUffici.add(idNodo);
				for (final NodoOrganigrammaDTO u : uffici) {
					idsUffici.add(u.getIdNodo());
				}
			}

			output = ruoloDAO.getAssegnazioniDelegato(idRuolo, idsUffici, idAOO, con);

		} catch (final Exception e) {
			LOGGER.error(ERROR_LETTURA_DELEGHE_MSG, e);
			throw new RedException(ERROR_LETTURA_DELEGHE_MSG, e);
		} finally {
			closeConnection(con);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.IUtenteSRV#getDelegatoLibroFirma(java.lang.Long,
	 *      java.lang.Long).
	 */
	@Override
	public DelegatoDTO getDelegatoLibroFirma(final Long idNodo, final Long idAOO) {
		DelegatoDTO output = null;
		Connection con = null;
		Long idRuolo = null;

		try {
		
			con = setupConnection(getDataSource().getConnection(), false);

			final Aoo aoo = aooDAO.getAoo(idAOO, con);
			idRuolo = aoo.getIdRuoloDelegatoLibroFirma();

			output = ruoloDAO.getDelegatoLibroFirma(idRuolo, idNodo, idAOO, con);

		} catch (final Exception e) {
			LOGGER.error(ERROR_LETTURA_DELEGHE_MSG, e);
			throw new RedException(ERROR_LETTURA_DELEGHE_MSG, e);
		} finally {
			closeConnection(con);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IUtenteFacadeSRV#removeRuoliDelegato(java.util.Collection,
	 *      java.lang.Long).
	 */
	@Override
	public Boolean removeRuoliDelegato(final Collection<DelegatoDTO> masters, final Long idAoo) {
		Boolean output = Boolean.FALSE;
		Long idRuolo = null;
		Connection con = null;

		try {

			con = setupConnection(getDataSource().getConnection(), false);

			final Aoo aoo = aooDAO.getAoo(idAoo, con);
			idRuolo = aoo.getIdRuoloDelegatoLibroFirma();

			for (final DelegatoDTO m : masters) {
				ruoloDAO.removeRuoloFromUtente(m.getIdNodo(), m.getIdUtente(), idRuolo, con);
			}

			output = Boolean.TRUE;

		} catch (final Exception e) {
			LOGGER.error("Errore durante la rimozione delle Deleghe assegnate.", e);
			throw new RedException("Errore durante la rimozione delle Deleghe assegnate.", e);
		} finally {
			closeConnection(con);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IUtenteFacadeSRV#checkRuoliHasPermesso(java.lang.Long,
	 *      java.lang.Long, java.lang.Long, it.ibm.red.business.enums.PermessiEnum).
	 */
	@Override
	public boolean checkRuoliHasPermesso(final Long idNodo, final Long idUtente, final Long idAOO, final PermessiEnum permessoCompare) {
		boolean output = false;
		try {
			final List<Ruolo> ruoliList = assegnaUfficioSRV.getRuoliFromIdNodoAndIdAOO(idUtente, idNodo, idAOO);

			for (final Ruolo r : ruoliList) {
				output = PermessiUtils.hasPermesso(r.getPermessi().longValue(), permessoCompare);
				if (output) {
					break;
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la verifica del permesso " + permessoCompare, e);
			throw new RedException("Errore durante la verifica del permesso " + permessoCompare, e);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.IUtenteSRV#getByCodiceFiscale(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public UtenteDTO getByCodiceFiscale(final String codiceFiscale, final Connection con) {
		final Long idUtente = utenteDAO.getIdByCodiceFiscale(codiceFiscale, con);

		return getById(idUtente, con);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IUtenteFacadeSRV#getByCodiceFiscale(java.lang.String).
	 */
	@Override
	public UtenteDTO getByCodiceFiscale(final String codiceFiscale) {
		UtenteDTO out = null;
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			out = getByCodiceFiscale(codiceFiscale, con);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dell'utente a partire dal codice fiscale.", e);
			throw new RedException("Errore durante il recupero dell'utente a partire dal codice fiscale.", e);
		} finally {
			closeConnection(con);
		}

		return out;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IUtenteFacadeSRV#getIdByCodiceFiscale(java.lang.String).
	 */
	@Override
	public Long getIdByCodiceFiscale(final String codiceFiscale) {
		Long out = null;
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			out = utenteDAO.getIdByCodiceFiscale(codiceFiscale, con);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dell'ID utente a partire dal codice fiscale.", e);
			throw new RedException("Errore durante il recupero dell'ID utente a partire dal codice fiscale.", e);
		} finally {
			closeConnection(con);
		}

		return out;
	}

	/**
	 * @see it.ibm.red.business.service.IUtenteSRV#getGlifoDelegato(java.lang.Long,
	 *      java.lang.Long).
	 */
	@Override
	public UtenteFirma getGlifoDelegato(Long idUtente, Long idUtenteDelegante) {
		UtenteFirma out = null;
		Connection con = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			
			out = utenteDAO.getGlifoDelegato(idUtente, idUtenteDelegante, con);
		} catch (Exception e) {
			LOGGER.error("", e);
			throw new RedException("", e);
		} finally {
			closeConnection(con);
		}
		
		return out;
	}

}