package it.ibm.red.business.service.facade;

import java.io.Serializable;

import it.ibm.red.business.dto.ProtocollazionePECConfigDTO;

/**
 * Facade del servizio di protocollazione PEC.
 */
public interface IProtocollaPECFacadeSRV extends Serializable {
	
	/**
	 * Il metodo si occupa di protocollare le mail secondo le configurazioni in input.
	 * 
	 * @param config
	 */
	void protocollaPEC(ProtocollazionePECConfigDTO config);

}