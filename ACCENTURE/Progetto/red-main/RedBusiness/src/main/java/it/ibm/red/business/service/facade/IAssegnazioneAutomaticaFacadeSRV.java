package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.AssegnatarioDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Facade che gestisce le assegnazioni automatiche dei documenti
 * 
 * @author SimoneLungarella
 */
public interface IAssegnazioneAutomaticaFacadeSRV extends Serializable {

	/**
	 * Consente il recupero dell' assegnatario (utente o ufficio) nel caso in cui
	 * esistano assegnazioni automatiche. Le assegnazioni automatiche possono
	 * riferirsi a capitoli di spesa, a metadati, ai tipi documenti o alle coppie
	 * tipo doc/tipo proc, nel caso in cui su un documento vengono definite due
	 * assegnazioni automatiche diverse per differenti utenti, l'assegnatario sarà
	 * quello stabilito in base ad uno specifico algoritmo implementato da questo
	 * metodo.
	 * 
	 * @param idAoo
	 *            identificativo dell'area organizzativa
	 * @param idTipoDocumento
	 *            identificativo tipologia documento per la quale occorre recuperare
	 *            l'assegnazione automatica
	 * @param idTipoProcedimento
	 *            identificativo tipologia procedimento per la quale occorre
	 *            recuperare l'assegnazione automatica
	 * @param codiceCapSpesa
	 *            codice del capitolo di spesa che caratterizza l'assegnazione
	 *            automatica, <code> null </code> se la regola ricercata non include
	 *            la specificazione di un capitolo di spesa
	 * @param metadati
	 *            metadati associati alla coppia tipologia documento / tipologia
	 *            procedimento
	 * @return assegnatario se esiste un'assegnazione automatica che stabilisce un
	 *         assegnatario in base ai parametri in input, null se non esiste alcuna
	 *         regola di assegnazione automatica
	 */
	AssegnatarioDTO getAssegnatario(Long idAoo, Long idTipoDocumento, Long idTipoProcedimento, String codiceCapSpesa,
			Collection<MetadatoDTO> metadati);

	/**
	 * Metodo che permette di convertire un <code> AssegnatarioDTO </code> in un
	 * <code> AssegnazioneDTO </code>.
	 * 
	 * @param assegnatario
	 *            DTO assegnatario da convertire
	 * @return DTO assegnatario come AssegnazioneDTO
	 */
	AssegnazioneDTO getAssegnazioneFromAssegnatario(AssegnatarioDTO assegnatario);

	/**
	 * Metodo che permette di recuperare le assegnazioni relative a uno specifico
	 * documento identificato dal document title: <code> idDocumento </code>.
	 * 
	 * @param idDocumento
	 *            document title che identifica il documento per la quale occorre
	 *            recuperare le assegnazioni automatiche
	 * @param utente
	 *            utente in sessione
	 * @return output lista di assegnazioni automatiche esistenti sulla tipologia
	 *         documento
	 */
	List<AssegnazioneDTO> getAssegnazioni(String idDocumento, UtenteDTO utente);

	/**
	 * Metodo che permette di recuperare l'assegnazione per COMPETENZA di uno
	 * specifico documento identificato dal document title:
	 * <code> idDocumento </code>.
	 * 
	 * @param idDocumento
	 *            document title del documento per cui recuperare l'assegnazione per
	 *            competenza
	 * @param utente
	 *            utente in sessione
	 * @return output assegnazione per COMPETENZA
	 */
	AssegnazioneDTO getAssegnazioneCompetenza(String idDocumento, UtenteDTO utente);

}
