package it.ibm.red.business.service.concrete;

import java.util.HashMap;
import java.util.Map;

import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.service.IRifiutaSpedizioneSRV;

/**
 * Service che gestisce la funzionalità di rifiuto spedizione.
 */
@Service
@Component
public class RifiutaSpedizioneSRV extends AbstractService implements IRifiutaSpedizioneSRV {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1563093947068735771L;
	
    /**
     * Logger.
     */
	private static final REDLogger LOGGER = REDLogger.getLogger(RifiutaSpedizioneSRV.class.getName());

	/**
	 * @see it.ibm.red.business.service.facade.IRifiutaSpedizioneFacadeSRV#rifiuta(it.ibm.red.business.dto.FilenetCredentialsDTO,
	 *      java.lang.String, java.lang.String, int, java.lang.Integer).
	 */
	@Override
	public EsitoOperazioneDTO rifiuta(final FilenetCredentialsDTO fcDTO, final String wobNumber, final String motivazione, final int idUtente, final Integer numeroDoc) {
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		FilenetPEHelper fpeh = null;
		
		try {
			fpeh = new FilenetPEHelper(fcDTO);
			
			VWWorkObject wo = fpeh.getWorkFlowByWob(wobNumber, true);
			
			Map<String, Object> map = new HashMap<>();
			map.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), motivazione);
			map.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY),  TrasformerPE.getMetadato(wo, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY)));
			map.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY),  TrasformerPE.getMetadato(wo, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY)));
			map.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UTENTE_TMP_WF_METAKEY), idUtente);

			// Si avanza nel workflow
			fpeh.nextStep(wo, map, ResponsesRedEnum.RIFIUTA_SPEDIZIONE.getResponse());
			
			esito.setEsito(true);
			esito.setIdDocumento(numeroDoc);
			esito.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
		} catch (Exception e) {
			LOGGER.error("RifiutaSpedizioneSRV.rifiuta: errore nell'esecuzione della response per il wobNumber: " + wobNumber, e);
			esito.setIdDocumento(numeroDoc);
			esito.setNote("Errore nell'operazione di rifiuto spedizione.");
		} finally {
			logoff(fpeh);
		}
		
		return esito;
	}
}