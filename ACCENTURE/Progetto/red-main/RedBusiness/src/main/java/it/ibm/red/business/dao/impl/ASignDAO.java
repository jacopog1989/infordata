package it.ibm.red.business.dao.impl;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections4.MapUtils;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IASignDAO;
import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.ASignMasterDTO;
import it.ibm.red.business.dto.ASignStatusDTO;
import it.ibm.red.business.dto.ASignStepDTO;
import it.ibm.red.business.dto.ASignStepResultDTO;
import it.ibm.red.business.dto.DocumentoDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.SignModeEnum;
import it.ibm.red.business.enums.SignTypeEnum.SignTypeGroupEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.asign.step.StatusEnum;
import it.ibm.red.business.service.asign.step.StepEnum;
import it.ibm.red.business.utils.StringUtils;

/**
 * Dao gestione item firma asincrona.
 */
@Repository
public class ASignDAO extends AbstractDAO implements IASignDAO {

	/**
	 * Serial version.
	 */
	private static final long serialVersionUID = 6343664528527923355L;
	
	/**
	 * Logger per la gestione degli errori.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ASignDAO.class.getName());
	
	/**
	 * Query parziale.
	 */
	private static final String UPDATE_CODA_PREPARAZIONE_SPEDIZIONE_SET_CPS_TIMER_STARTING_TIME_SYSDATE_WHERE_CPS_ID = "UPDATE coda_preparazione_spedizione SET cps_timer_starting_time = SYSDATE WHERE CPS_ID = ?";

	/**
	 * Colonna ID item firma asincrona.
	 */
	private static final String CPS_ID = "CPS_ID";

	/**
	 * Colonna document title.
	 */
	private static final String CPS_DOC_TITLE = "CPS_DOC_TITLE";

	/**
	 * Colonna Id AOO.
	 */
	private static final String CPS_AOO_ID = "CPS_AOO_ID";

	/**
	 * Colonna Log sullo step.
	 */
	private static final String CPE_LOG = "CPE_LOG";

	/**
	 * Colonna Data Da dello step.
	 */
	private static final String CPE_DATA_DA = "CPE_DATA_DA";

	/**
	 * Colonna Data A dello step.
	 */
	private static final String CPE_DATA_A = "CPE_DATA_A";

	/**
	 * Intero che indica la priorità massima di lavorazione di un item.
	 */
	private static final int ZERO = 0;

	/**
	 * Nome sequence per la tabella degli step.
	 */
	private static final String SEQ_NAME_CODA_PREP_SPED_STEP = "SEQ_CODA_PREP_SPED_STEP";

	/**
	 * Nome sequence per la tabella degli stati.
	 */
	private static final String SEQ_NAME_CODA_PREP_SPED_STATO = "SEQ_CODA_PREP_SPED_STATO";

	/**
	 * Nome sequence per la tabella degli item di firma.
	 */
	private static final String SEQ_NAME_CODA_PREP_SPED = "SEQ_CODA_PREP_SPEDIZIONE";

	/**
	 * Restituice l'item identificato dall' {@code idItem}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#getItem(java.sql.Connection,
	 *      java.lang.Long, java.lang.Boolean)
	 */
	@Override
	public ASignItemDTO getItem(Connection connection, Long idItem, Boolean bLog) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ASignItemDTO output = null;
		try {
			String query = "SELECT * FROM CODA_PREPARAZIONE_SPEDIZIONE C WHERE CPS_ID = ?";
			ps = connection.prepareStatement(query);
			ps.setLong(1, idItem);
			rs = ps.executeQuery();
			if (rs.next()) {
				output = buildItem(rs);
				fillStates(connection, output);
				fillSteps(connection, output, bLog);
			}
			closeStatement(ps, rs);
			return output;
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Popola lo <em> Status </em> dell'item a partire dalle sue informazioni.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param item
	 *            item di firma asincrona per il quale occorre popolare gli status
	 */
	private static void fillStates(Connection connection, ASignItemDTO item) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String query = "SELECT * FROM CODA_PREP_SPED_STATO C WHERE CPT_CPS_ID = ? ORDER BY CPT_ID";
			ps = connection.prepareStatement(query);
			ps.setLong(1, item.getId());
			rs = ps.executeQuery();
			while (rs.next()) {
				ASignStatusDTO status = buildStatus(rs);
				item.getStates().add(status);
				if (status.getDataA() == null) {
					item.setStatus(status.getStato());
				}
			}
			closeStatement(ps, rs);
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Popola gli <em> Step </em> dell'item a partire dalle sue informazioni.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param item
	 *            per il quale occorre popolare gli step
	 * @param bLog
	 *            se {@code true} aggiorna il log
	 */
	private static void fillSteps(Connection connection, ASignItemDTO item, Boolean bLog) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String query = "SELECT * FROM CODA_PREP_SPED_STEP C WHERE CPE_CPS_ID = ? ORDER BY CPE_ID";
			ps = connection.prepareStatement(query);
			ps.setLong(1, item.getId());
			rs = ps.executeQuery();
			while (rs.next()) {
				ASignStepDTO step = buildStep(rs, bLog);
				item.getSteps().add(step);
				if (step.getDataA() == null) {
					item.setStep(step.getStep());
				}
			}
			closeStatement(ps, rs);
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Costruisce l'item a partire dal result set.
	 * 
	 * @param rs
	 *            result set dal quale recupere le informazioni
	 * @param bLog
	 *            se {@code true} aggiorna anche il log
	 * @return item costruito
	 * @throws SQLException
	 */
	private static ASignStepDTO buildStep(ResultSet rs, Boolean bLog) throws SQLException {
		ASignStepDTO out = new ASignStepDTO();
		out.setDataA(getDateWithTime(rs, CPE_DATA_A));
		out.setDataDa(getDateWithTime(rs, CPE_DATA_DA));
		if (Boolean.TRUE.equals(bLog)) {
			out.setLog(rs.getString(CPE_LOG));
		}
		out.setStep(StepEnum.get(rs.getInt("CPE_STEP_ID")));
		return out;
	}

	/**
	 * Costruisce lo <em> Status </em> a partire dal result set
	 * 
	 * @param rs
	 *            result set dal quale recuperare le informazioni
	 * @return status dell'item di firma asincrona
	 * @throws SQLException
	 */
	private static ASignStatusDTO buildStatus(ResultSet rs) throws SQLException {
		ASignStatusDTO out = new ASignStatusDTO();
		out.setDataDa(getDateWithTime(rs, "CPT_DATA_DA"));
		out.setDataA(getDateWithTime(rs, "CPT_DATA_A"));
		out.setStato(StatusEnum.get(rs.getInt("CPT_STATO_ID")));
		return out;
	}

	/**
	 * Costruisce l'item a partire dal result set.
	 * 
	 * @param rs
	 *            result set dal quale recupere le informazioni
	 * @return item costruito
	 * @throws SQLException
	 */
	private static ASignItemDTO buildItem(ResultSet rs) throws SQLException {
		ASignItemDTO out = new ASignItemDTO();

		out.setDataUltimoRetry(getDateWithTime(rs, "CPS_ULTIMO_RETRY"));
		out.setDocumentTitle(rs.getString(CPS_DOC_TITLE));
		out.setNumeroDocumento(rs.getInt("CPS_NUMERO_DOCUMENTO"));
		out.setWobNumber(rs.getString("CPS_WOB_NUMBER"));
		out.setId(rs.getLong(CPS_ID));
		out.setnRetry(rs.getInt("CPS_RETRY"));

		out.setIdAoo(rs.getLong(CPS_AOO_ID));
		out.setPriority(rs.getInt("CPS_PRIORITY"));

		String flagComunicato = rs.getString("CPS_FLAG_COMUNICATO");
		out.setFlagComunicato(BooleanFlagEnum.SI.getValue().contentEquals(flagComunicato));

		out.setNumeroProtocollo(rs.getInt("CPS_NUMERO_PROTOCOLLO"));
		out.setAnnoProtocollo(rs.getInt("CPS_ANNO_PROTOCOLLO"));
		if (out.getNumeroProtocollo() == 0) {
			out.setNumeroProtocollo(null);
			out.setAnnoProtocollo(null);
		}
		out.setIdFirmatario(rs.getLong("CPS_ID_FIRMATARIO"));
		out.setIdRuoloFirmatario(rs.getLong("CPS_ID_RUOLO_FIRMATARIO"));
		out.setIdUfficioFirmatario(rs.getLong("CPS_ID_UFFICIO_FIRMATARIO"));
		out.setSignMode(SignModeEnum.get(rs.getInt("CPS_SIGN_MODE")));
		out.setSignType(SignTypeGroupEnum.get(rs.getInt("CPS_SIGN_TYPE")));
		out.setQueue(DocumentQueueEnum.get(rs.getString("CPS_CODA"), null));

		return out;
	}

	/**
	 * Restituisce gli item associati ai documenti identificati dai {@code dts}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#getItemsFromDTS(java.sql.Connection,
	 *      java.util.Collection)
	 */
	@Override
	public List<ASignItemDTO> getItemsFromDTS(Connection connection, Collection<String> dts) {
		List<ASignItemDTO> output = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String query = "SELECT * FROM CODA_PREPARAZIONE_SPEDIZIONE C ";
			String operatore = " WHERE ";
			if (!CollectionUtils.isEmpty(dts)) {
				query += " WHERE ( ";
				query += StringUtils.createInCondition("CPS_DOC_TITLE", dts.iterator(), true);
				query += " ) ";
				operatore = " AND ";
			}
			query += operatore + " CPS_ID IN (SELECT CPT_CPS_ID FROM CODA_PREP_SPED_STATO WHERE CPT_DATA_A IS NULL)";
			ps = connection.prepareStatement(query);

			rs = ps.executeQuery();
			while (rs.next()) {
				ASignItemDTO item = buildItem(rs);
				fillStates(connection, item);
				fillSteps(connection, item, false);
				output.add(item);
			}

			return output;
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Restituisce l'id del primo item <em> playable </em>.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#getAndLockFirstItem(java.sql.Connection)
	 */
	@Override
	public Long getAndLockFirstItem(Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long idItemDaLavorare = null;
		try {
			// Recupero id item da lavorare effettuando un lock sulla tabella finché l'item
			// selezionato non viene rimosso dagli item lavorabili
			StringBuilder sb = new StringBuilder(
					"SELECT cps_id FROM coda_preparazione_spedizione left join coda_prep_sped_stato on cps_id = cpt_cps_id ")
							.append("WHERE (cpt_stato_id = ? AND cpt_data_a IS NULL AND cps_priority = (SELECT MAX(cps_priority) FROM coda_preparazione_spedizione) ")
							.append("AND ((SYSDATE - CAST(cps_timer_starting_time AS DATE) )* 24 * 60 > ? OR cps_timer_starting_time IS NULL)) ")
							.append("OR (cpt_stato_id <> ? AND cpt_data_a IS NULL AND (SYSDATE - CAST(cps_timer_starting_time AS DATE) )* 24 * 60 > ?) ORDER BY cps_priority desc ")
							.append("FOR UPDATE SKIP LOCKED");

			ps = connection.prepareStatement(sb.toString());
			int index = 1;
			ps.setInt(index++, StatusEnum.PRESO_IN_CARICO.getValue());
			ps.setInt(index++, Integer.parseInt(
					PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.TIMOUT_FIRMA_ASINCRONA)));
			ps.setInt(index++, StatusEnum.PRESO_IN_CARICO.getValue());
			ps.setInt(index++, Integer.parseInt(
					PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.TIMOUT_FIRMA_ASINCRONA)));

			rs = ps.executeQuery();
			if (rs.next()) {
				idItemDaLavorare = rs.getLong(CPS_ID);
			}

			// Aggiornamento timer per la gestione concorrente degli accessi alla tabella
			if (idItemDaLavorare != null) {
				closeStatement(ps, rs);

				String query = UPDATE_CODA_PREPARAZIONE_SPEDIZIONE_SET_CPS_TIMER_STARTING_TIME_SYSDATE_WHERE_CPS_ID;

				ps = connection.prepareStatement(query);
				ps.setLong(1, idItemDaLavorare);
				ps.execute();
			}
			return idItemDaLavorare;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Restituisce le informazioni sullo step definito dallo StepEnum e associato
	 * all'item identificato dall' {@code idItem}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#getInfo(java.sql.Connection,
	 *      java.lang.Long, it.ibm.red.business.service.asign.step.StepEnum)
	 */
	@Override
	public ASignStepResultDTO getInfo(Connection connection, Long idItem, StepEnum step) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ASignStepResultDTO output = null;
		try {
			String query = "SELECT CPE_LOG, CPE_DATA_DA, CPE_DATA_A FROM CODA_PREP_SPED_STEP where CPE_STEP_ID = ? AND CPE_CPS_ID = ?";
			ps = connection.prepareStatement(query);
			ps.setLong(1, step.getSequence());
			ps.setLong(2, idItem);

			rs = ps.executeQuery();
			if (rs.next()) {
				output = new ASignStepResultDTO(idItem, step, rs.getString(CPE_LOG),
						getDateWithTime(rs, CPE_DATA_DA), getDateWithTime(rs, CPE_DATA_A));
			}

			return output;
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Restituisce {@code true} se documento associato all'item identificato dall'
	 * {@code idItem} è protocollato, {@code false} altrimenti.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#isProtocollato(java.sql.Connection,
	 *      java.lang.Long)
	 */
	@Override
	public Boolean isProtocollato(Connection connection, Long idItem) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Boolean output = false;
		try {
			String query = "SELECT CPS_NUMERO_PROTOCOLLO, CPS_ANNO_PROTOCOLLO FROM CODA_PREPARAZIONE_SPEDIZIONE C WHERE CPS_ID = ?";
			ps = connection.prepareStatement(query);
			ps.setLong(1, idItem);

			rs = ps.executeQuery();
			if (rs.next()) {
				Integer numero = rs.getInt(1);
				Integer anno = rs.getInt(2);
				if (anno != null && numero != null && anno != 0 && numero != 0) {
					output = true;
				}
			}

			return output;
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IASignDAO#getMaxIdItem(java.sql.Connection,
	 *      java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public Long getMaxIdItem(Connection con, Integer numeroProtocollo, Integer annoProtocollo) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long output = null;
		try {
			String query = "SELECT MAX(CPS_ID) FROM CODA_PREPARAZIONE_SPEDIZIONE C WHERE CPS_NUMERO_PROTOCOLLO = ? AND CPS_ANNO_PROTOCOLLO = ?";
			ps = con.prepareStatement(query);
			ps.setInt(1, numeroProtocollo);
			ps.setInt(2, annoProtocollo);

			rs = ps.executeQuery();
			if (rs.next()) {
				output = rs.getLong(1);
			}

			return output;
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Restituisce la priorità dell'item da lavorare. NOTE: Attualmente la priorità
	 * è flat.
	 * 
	 * @return priorità di lavorazione
	 */
	private static Integer getPriority() {
		return ZERO;
	}

	/**
	 * Imposta la priorità di lavorazione sull'item identificato dall'
	 * {@code idItem}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#setPriority(java.sql.Connection,
	 *      java.lang.Long, java.lang.Integer)
	 */
	@Override
	public void setPriority(Connection connection, Long idItem, Integer priority) {
		PreparedStatement ps = null;
		try {
			String query = "UPDATE CODA_PREPARAZIONE_SPEDIZIONE set CPS_PRIORITY = ? WHERE CPS_ID = ?";
			ps = connection.prepareStatement(query);

			ps.setLong(1, priority);
			ps.setLong(2, idItem);

			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * Imposta la priorità di lavorazione al massimo per l'item associato al
	 * documento identificato dal {@code numeroProtocollo}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#setMaxPriority(java.sql.Connection,
	 *      java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void setMaxPriority(Connection connection, Integer numeroProtocollo, Integer annoProtocollo) {
		PreparedStatement ps = null;
		try {
			String query = "UPDATE CODA_PREPARAZIONE_SPEDIZIONE set CPS_PRIORITY = (SELECT NVL(MAX(CPS_PRIORITY),0)+1 FROM CODA_PREPARAZIONE_SPEDIZIONE) WHERE CPS_NUMERO_PROTOCOLLO = ? AND CPS_ANNO_PROTOCOLLO = ?";
			ps = connection.prepareStatement(query);

			ps.setLong(1, numeroProtocollo);
			ps.setLong(2, annoProtocollo);

			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * Imposta la priorità di lavorazione al massimo per l'item associato al
	 * documento identificato dal {@code dt}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#setMaxPriority(java.sql.Connection,
	 *      java.lang.String)
	 */
	@Override
	public void setMaxPriority(Connection connection, String dt) {
		PreparedStatement ps = null;
		try {
			String query = "UPDATE CODA_PREPARAZIONE_SPEDIZIONE set CPS_PRIORITY = (SELECT NVL(MAX(CPS_PRIORITY),0)+1 FROM CODA_PREPARAZIONE_SPEDIZIONE) WHERE CPS_DOC_TITLE = ?";
			ps = connection.prepareStatement(query);

			ps.setString(1, dt);

			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * Restituisce il primo item <em> resumable </em>.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#getAndLockFirstResumableItem(java.sql.Connection)
	 */
	@Override
	public ASignItemDTO getAndLockFirstResumableItem(Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ASignItemDTO output = null;
		try {

			StringBuilder sb = new StringBuilder("SELECT * FROM coda_preparazione_spedizione c ").append(
					"WHERE cps_id in (SELECT CPT_CPS_ID FROM (SELECT * FROM ( SELECT cps_id FROM coda_preparazione_spedizione CODA_PREP_SPED_STATO WHERE CPT_DATA_A is null AND CPT_STATO_ID = ?) ")
					.append("AND c.cps_retry <= ? ")
					.append("AND (( SYSDATE - CAST(c.cps_ultimo_retry AS DATE) ) * 24 * 60  <= ? OR c.cps_ultimo_retry IS NULL) ")
					.append("AND (( SYSDATE - CAST(cps_timer_starting_time AS DATE) ) * 24 * 60  > ? OR cps_timer_starting_time IS NULL)")
					.append("ORDER BY cps_priority DESC, cps_id ASC FOR UPDATE SKIP LOCKED");

			ps = connection.prepareStatement(sb.toString());
			int index = 1;

			ps.setInt(index++, StatusEnum.RETRY.getValue());
			ps.setInt(index++, Integer
					.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ASIGN_MAX_RETRY)));
			ps.setInt(index++, Integer
					.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ASIGN_DELTA_RETRY)));
			ps.setInt(index++, Integer.parseInt(
					PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.TIMOUT_FIRMA_ASINCRONA)));
			rs = ps.executeQuery();
			if (rs.next()) {
				output = buildItem(rs);
				fillStates(connection, output);
				fillSteps(connection, output, true);
			}

			if (output != null && output.getId() != null) {
				closeStatement(ps, rs);

				String query = UPDATE_CODA_PREPARAZIONE_SPEDIZIONE_SET_CPS_TIMER_STARTING_TIME_SYSDATE_WHERE_CPS_ID;

				ps = connection.prepareStatement(query);
				ps.setLong(1, output.getId());
				ps.execute();
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}

	/**
	 * Restituisce la data recuperata dal database.
	 * 
	 * @param resultSet
	 *            result set dal quale recuperare la data
	 * @param attribute
	 *            nome della colonna della data
	 * @return java.util.Date
	 * @throws SQLException
	 */
	private static Date getDateWithTime(ResultSet resultSet, String attribute) throws SQLException {
		Date out = null;
		Timestamp timestamp = resultSet.getTimestamp(attribute);
		if (timestamp != null) {
			out = new java.util.Date(timestamp.getTime());
		}
		return out;
	}

	/**
	 * Restitusice le informazioni sull'item identificato dall' {@code idItem}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#getInfos(java.sql.Connection,
	 *      java.lang.Long)
	 */
	@Override
	public List<ASignStepResultDTO> getInfos(Connection connection, Long idItem) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ASignStepResultDTO> output = new ArrayList<>();
		try {
			String query = "SELECT * FROM CODA_PREP_SPED_STEP where CPE_CPS_ID = ?";
			ps = connection.prepareStatement(query);
			ps.setLong(1, idItem);
			rs = ps.executeQuery();
			while (rs.next()) {
				output.add(
						new ASignStepResultDTO(idItem, StepEnum.get(rs.getInt("CPE_STEP_ID")), rs.getString(CPE_LOG),
								getDateWithTime(rs, CPE_DATA_DA), getDateWithTime(rs, CPE_DATA_A)));
			}
			return output;
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Effettua la insert dello step.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#insertStepInfo(java.sql.Connection,
	 *      it.ibm.red.business.dto.ASignStepResultDTO)
	 */
	@Override
	public void insertStepInfo(Connection connection, ASignStepResultDTO lastResult) {
		ASignStepResultDTO step = getInfo(connection, lastResult.getIdItem(), lastResult.getStep());
		if (step == null) {
			// Chiudi
			closeInfo(connection, lastResult.getIdItem());
			// Inserisci il nuovo step
			insertNewStep(connection, lastResult.getIdItem(), lastResult.getStep());
		}

		String newLog = "";

		if (step != null && step.getLog() != null) {
			newLog += step.getLog();
		}
		if (lastResult.getLog() != null) {
			newLog += lastResult.getLog();
		}

		// Aggiorna log
		updateItemLog(connection, lastResult.getIdItem(), newLog);
	}

	/**
	 * Effettua un update al log dell'item identificato dall' {@code idItem}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idItem
	 *            identificativo dell'item da aggiornare
	 * @param log
	 *            log da impostare
	 */
	private static void updateItemLog(Connection connection, Long idItem, String log) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String query = "UPDATE CODA_PREP_SPED_STEP SET CPE_LOG = ? WHERE CPE_CPS_ID = ? AND CPE_DATA_A IS NULL ";
			ps = connection.prepareStatement(query);
			ps.setString(1, log);
			ps.setLong(2, idItem);
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Chiude l'ultimo step che risulta aperto associato all'item identificato dall'
	 * {@code idItem}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idItem
	 *            identificativo dell'item
	 */
	private static void closeInfo(Connection connection, Long idItem) {
		final String query = "UPDATE CODA_PREP_SPED_STEP SET CPE_DATA_A = sysdate WHERE CPE_CPS_ID = ? AND CPE_DATA_A IS NULL";
		executeQueryOnItem(connection, idItem, query);
	}

	/**
	 * Imposta il numero di <em> retry </em> disponibili per l'item identificato
	 * dall' {@code idItem}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#setRetry(java.sql.Connection,
	 *      java.lang.Long, java.lang.Integer, java.lang.Boolean)
	 */
	@Override
	public void setRetry(Connection connection, Long idItem, Integer n, Boolean bResetDate) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String query = "UPDATE CODA_PREPARAZIONE_SPEDIZIONE SET CPS_RETRY = ? ";
			if (Boolean.TRUE.equals(bResetDate)) {
				query += ", CPS_ULTIMO_RETRY = null ";
			} else {
				query += ", CPS_ULTIMO_RETRY = sysdate ";
			}
			query += " WHERE CPS_ID = ?";
			ps = connection.prepareStatement(query);
			ps.setLong(1, n);
			ps.setLong(2, idItem);
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

	}

	/**
	 * Aggiorna lo status dell'item identificato dall' {@code idItem}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#updateStatusItem(java.sql.Connection,
	 *      java.lang.Long, it.ibm.red.business.service.asign.step.StatusEnum)
	 */
	@Override
	public void updateStatusItem(Connection connection, Long idItem, StatusEnum status) {
		closeState(connection, idItem);
		insertNewState(connection, idItem, status);
		clearTimer(connection, idItem);

		// Se lo stato è ELABORATO, si chiude anche l'ultimo stato in quanto il
		// procedimento è stato completato
		if (StatusEnum.ELABORATO.equals(status)) {
			closeState(connection, idItem);
		}
	}

	/**
	 * Chiude l'ultimo <em> Status </em> che risulta aperto associato all'item
	 * identificato dall' {@code idItem}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idItem
	 *            identificativo dell'item
	 */
	private static void closeState(Connection connection, Long idItem) {
		final String query = "UPDATE CODA_PREP_SPED_STATO SET CPT_DATA_A = sysdate WHERE CPT_CPS_ID = ? AND CPT_DATA_A IS NULL";
		executeQueryOnItem(connection, idItem, query);
	}

	/**
	 * Esegue una query di update sull'item identificato dall' {@code idItem}.
	 * 
	 * @param connection
	 *            Connessione al database.
	 * @param idItem
	 *            Identificativo dell'item.
	 * @param query
	 *            Query da eseguire sull'item identificato dall' {@code idItem}.
	 */
	private static void executeQueryOnItem(Connection connection, Long idItem, final String query) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(query);
			ps.setLong(1, idItem);
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}
	
	/**
	 * Effettua la insert dell' item associato al documento in firma identificato
	 * dal {@code documentTitle}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#insertItem(java.sql.Connection,
	 *      java.lang.String, java.lang.String, java.lang.Integer,
	 *      java.lang.Integer, java.lang.Integer, java.lang.Long, java.lang.Long,
	 *      java.lang.Long, java.lang.Long, it.ibm.red.business.enums.SignModeEnum,
	 *      it.ibm.red.business.enums.SignTypeEnum.SignTypeGroupEnum,
	 *      it.ibm.red.business.enums.DocumentQueueEnum)
	 */
	@Override
	public Long insertItem(Connection connection, String documentTitle, String wobNumber, Integer numeroDocumento,
			Integer numeroProtocollo, Integer annoProtocollo, Long idUtenteFirmatario, Long idUfficioUtenteFirmatario,
			Long idRuoloUtenteFirmatario, Long idAOO, SignModeEnum signMode, SignTypeGroupEnum signType,
			DocumentQueueEnum queue) {
		Long idItem = null;
		PreparedStatement ps = null;

		try {
			Integer priority = getPriority();
			idItem = getNextId(connection, SEQ_NAME_CODA_PREP_SPED);

			StringBuilder query = new StringBuilder(
					"INSERT INTO CODA_PREPARAZIONE_SPEDIZIONE (CPS_RETRY, CPS_AOO_ID, CPS_PRIORITY, CPS_ID, CPS_ULTIMO_RETRY, CPS_FLAG_COMUNICATO, ")
							.append("CPS_DOC_TITLE, CPS_WOB_NUMBER, CPS_NUMERO_DOCUMENTO, CPS_NUMERO_PROTOCOLLO, CPS_ANNO_PROTOCOLLO, CPS_ID_FIRMATARIO, CPS_ID_RUOLO_FIRMATARIO, ")
							.append("CPS_ID_UFFICIO_FIRMATARIO, CPS_SIGN_MODE, CPS_SIGN_TYPE, CPS_CODA) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

			ps = connection.prepareStatement(query.toString());

			int index = 1;
			ps.setInt(index++, 0); // CPS_RETRY
			ps.setLong(index++, idAOO);
			ps.setLong(index++, priority);
			ps.setLong(index++, idItem);
			ps.setNull(index++, Types.DATE); // CPS_ULTIMO_RETRY
			ps.setInt(index++, 0); // CPS_FLAG_COMUNICATO
			ps.setString(index++, documentTitle);
			ps.setString(index++, wobNumber);
			ps.setInt(index++, numeroDocumento);
			if (numeroProtocollo != null && numeroProtocollo > 0) {
				ps.setInt(index++, numeroProtocollo);
			} else {
				ps.setNull(index++, Types.INTEGER);
			}
			if (annoProtocollo != null && annoProtocollo > 0) {
				ps.setInt(index++, annoProtocollo);
			} else {
				ps.setNull(index++, Types.INTEGER);
			}
			ps.setLong(index++, idUtenteFirmatario);
			ps.setLong(index++, idRuoloUtenteFirmatario);
			ps.setLong(index++, idUfficioUtenteFirmatario);
			ps.setInt(index++, signMode.getId());
			ps.setInt(index++, signType.getId());
			ps.setString(index++, queue.getName());

			// Insert nella CODA_PREPARAZIONE_SPEDIZIONE
			int insert = ps.executeUpdate();

			if (insert > 0) {
				LOGGER.info("Item inserito nella CODA_PREPARAZIONE_SPEDIZIONE. CPS ID: " + idItem);

				Long cptId = insertNewState(connection, idItem, StatusEnum.PRESO_IN_CARICO);

				if (cptId != null) {
					insertNewStep(connection, idItem, StepEnum.START);
				}
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}

		return idItem;
	}

	/**
	 * Inserisce lo {@code status} associandolo all'item identificato dall'
	 * {@code idItem}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idItem
	 *            identificativo dell'item
	 * @param status
	 *            status da inserire
	 * @return id status inserit
	 */
	private static Long insertNewState(Connection connection, Long idItem, StatusEnum status) {
		Long cptId = null;
		PreparedStatement ps = null;

		try {
			cptId = getNextId(connection, SEQ_NAME_CODA_PREP_SPED_STATO);

			String query = "INSERT INTO CODA_PREP_SPED_STATO (CPT_ID, CPT_STATO_ID, CPT_DATA_DA, CPT_DATA_A, CPT_CPS_ID) VALUES (?, ?, SYSDATE, ?, ?)";
			ps = connection.prepareStatement(query);

			int index = 1;
			ps.setLong(index++, cptId);
			ps.setInt(index++, status.getValue());
			ps.setNull(index++, Types.DATE); // CPT_DATA_A
			ps.setLong(index++, idItem);

			// Insert nella CODA_PREP_SPED_STATO
			int insert = ps.executeUpdate();

			if (insert > 0) {
				LOGGER.info("Stato inserito nella CODA_PREP_SPED_STATO. CPT ID: " + cptId);
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}

		return cptId;
	}

	/**
	 * Inserisce il nuovo {@code step} associandolo all'item identificato dall'
	 * {@code idItem}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idItem
	 *            identificativo dell'item
	 * @param step
	 *            step da inserire
	 * @return id step inserito
	 */
	private static Long insertNewStep(Connection connection, Long idItem, StepEnum step) {
		Long cpeId = null;
		PreparedStatement ps = null;

		try {
			cpeId = getNextId(connection, SEQ_NAME_CODA_PREP_SPED_STEP);

			String query = "INSERT INTO CODA_PREP_SPED_STEP (CPE_ID, CPE_STEP_ID, CPE_DATA_DA, CPE_DATA_A, CPE_CPS_ID) VALUES (?, ?, SYSDATE, ?, ?)";
			ps = connection.prepareStatement(query);

			int index = 1;
			ps.setLong(index++, cpeId);
			ps.setInt(index++, step.getSequence());
			ps.setNull(index++, Types.DATE); // CPE_DATA_A
			ps.setLong(index++, idItem);

			// Insert nella CODA_PREP_SPED_STEP
			int insert = ps.executeUpdate();

			if (insert > 0) {
				LOGGER.info("Step inserito nella CODA_PREP_SPED_STEP. CPE ID: " + cpeId);
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}

		return cpeId;
	}

	/**
	 * Effettua la insert dello step di STOP.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#insertStopStep(java.sql.Connection,
	 *      java.lang.Long)
	 */
	@Override
	public Long insertStopStep(Connection connection, Long idItem) {
		closeInfo(connection, idItem);
		Long idStep = insertNewStep(connection, idItem, StepEnum.STOP);
		closeInfo(connection, idItem);
		return idStep;
	}

	/**
	 * Effettua un reset del timer di firma asincrona.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idItem
	 *            item al quale è associato il timer
	 */
	private static void clearTimer(Connection connection, Long idItem) {
		
		String query = "UPDATE CODA_PREPARAZIONE_SPEDIZIONE SET cps_timer_starting_time = null WHERE CPS_ID = ?";
		executeQueryOnItem(connection, idItem, query);
	}

	/**
	 * Restituisce i document title associati ai documenti presenti logicamente
	 * nella coda {@code queue}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#getDTS(java.sql.Connection,
	 *      it.ibm.red.business.enums.DocumentQueueEnum, java.lang.Long)
	 */
	@Override
	public Set<String> getDTS(Connection connection, DocumentQueueEnum queue, Long idAoo) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Set<String> output = new HashSet<>();
		try {
			String query = "";
			if (queue != null) {
				query = "SELECT CPS_DOC_TITLE FROM CODA_PREPARAZIONE_SPEDIZIONE WHERE CPS_CODA = ? AND CPS_AOO_ID = ?";
			} else {
				query = "SELECT CPS_DOC_TITLE FROM CODA_PREPARAZIONE_SPEDIZIONE WHERE CPS_AOO_ID = ?";
			}
			int index = 1;
			ps = connection.prepareStatement(query);
			if (queue != null) {
				ps.setString(index++, queue.getName());
			}
			ps.setLong(index++, idAoo);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				output.add(rs.getString(CPS_DOC_TITLE));
			}
			return output;
		} catch (SQLException e) {
			LOGGER.error("Errore riscontrato durante il recupero dei document titles sulla coda: " + queue, e);
			throw new RedException("Errore riscontrato durante il recupero dei document titles sulla coda: " + queue, e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Restituisce le informazioni sui master associati ai documenti identificati
	 * dai {@code dts}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#getMasterInfo(java.sql.Connection,
	 *      java.util.Collection)
	 */
	@Override
	public Map<String, ASignMasterDTO> getMasterInfo(Connection connection, Collection<String> dts) {
		Map<String, ASignMasterDTO> out = new HashMap<>();
		List<ASignItemDTO> items = getItemsFromDTS(connection, dts);
		if (items != null) {
			for (ASignItemDTO item : items) {
				out.put(item.getDocumentTitle(), item.getMaster());
			}
		}
		return out;
	}

	/**
	 * Restituisce i dettagli sull'item associato al documento identificato dal
	 * {@code dt}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#getDetailInfo(java.sql.Connection,
	 *      java.lang.String)
	 */
	@Override
	public ASignItemDTO getDetailInfo(Connection connection, String dt) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		ASignItemDTO output = null;
		try {
			String query = "SELECT * FROM CODA_PREPARAZIONE_SPEDIZIONE C WHERE CPS_DOC_TITLE = ? AND CPS_ID IN (SELECT CPT_CPS_ID FROM CODA_PREP_SPED_STATO WHERE CPT_DATA_A IS NULL) ";
			ps = connection.prepareStatement(query);
			ps.setString(1, dt);
			rs = ps.executeQuery();
			if (rs.next()) {
				output = buildItem(rs);
				fillStates(connection, output);
				fillSteps(connection, output, false);
			}
			closeStatement(ps, rs);
			return output;
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Restituisce true se esiste un item associato al documento identificato dal
	 * documentTitle e se questo item non risulta già elaborato.
	 * 
	 * @param connection
	 *            connessione database
	 * @param documentTitle
	 *            identificativo univoco del documento
	 * @return true se presente logicamente nella coda, false altrimenti
	 */
	@Override
	public Boolean isPresent(Connection connection, String documentTitle) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Boolean output = null;
		try {

			String query = "SELECT 1 FROM CODA_PREPARAZIONE_SPEDIZIONE C WHERE CPS_DOC_TITLE = ? "
					+ "AND CPS_ID NOT IN (SELECT CPT_CPS_ID FROM CODA_PREP_SPED_STATO WHERE CPT_STATO_ID = ?)";
			ps = connection.prepareStatement(query);
			ps.setString(1, documentTitle);
			ps.setInt(2, StatusEnum.ELABORATO.getValue());

			rs = ps.executeQuery();
			output = rs.next();
			return output;
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Restituisce il log dello step associato all'item identificato dall'
	 * {@code idItem}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#getLog(java.sql.Connection,
	 *      java.lang.Long, it.ibm.red.business.service.asign.step.StepEnum)
	 */
	@Override
	public String getLog(Connection connection, Long idItem, StepEnum step) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String output = null;
		try {
			String query = "SELECT CPE_LOG FROM CODA_PREP_SPED_STEP WHERE CPE_CPS_ID=? AND CPE_STEP_ID=? ";
			ps = connection.prepareStatement(query);
			ps.setLong(1, idItem);
			ps.setLong(2, step.getSequence());
			rs = ps.executeQuery();
			if (rs.next()) {
				output = rs.getString(CPE_LOG);
			}
			return output;
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Effettua un update sulle informazioni del protocollo del documento associato
	 * all'item identificato dall' {@code idItem}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#updateProtocolloItem(java.sql.Connection,
	 *      java.lang.Long, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void updateProtocolloItem(Connection connection, Long idItem, Integer numeroProtocollo,
			Integer annoProtocollo) {
		PreparedStatement ps = null;
		try {
			String query = "UPDATE CODA_PREPARAZIONE_SPEDIZIONE SET CPS_NUMERO_PROTOCOLLO = ?, CPS_ANNO_PROTOCOLLO = ? WHERE CPS_ID = ?";
			ps = connection.prepareStatement(query);
			ps.setInt(1, numeroProtocollo);
			ps.setInt(2, annoProtocollo);
			ps.setLong(3, idItem);
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(
					"Errore nell'aggiornamento dell'item " + idItem + " con i dati di protocollo: " + e.getMessage(),
					e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * Restitusice l'ultimo stato raggiunto dall'item associato al documento
	 * identificato dal {@code documentTitle}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#checkDocStatus(java.sql.Connection,
	 *      java.lang.String)
	 */
	@Override
	public StatusEnum checkDocStatus(Connection connection, String documentTitle) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		StatusEnum status = null;
		try {
			String query = "SELECT CPT_STATO_ID FROM CODA_PREP_SPED_STATO WHERE CPT_CPS_ID IN (SELECT CPS_ID FROM CODA_PREPARAZIONE_SPEDIZIONE WHERE CPS_DOC_TITLE = ? AND CPT_DATA_A IS NULL)";
			ps = connection.prepareStatement(query);
			ps.setString(1, documentTitle);
			rs = ps.executeQuery();
			if (rs.next()) {
				status = StatusEnum.get(rs.getInt("CPT_STATO_ID"));
			}
		} catch (Exception e) {
			LOGGER.error("Errore nel recupero dello stato del documento con document title: " + documentTitle, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return status;
	}

	/**
	 * Restitusice l'id dell'item associato al documento identificato dal
	 * {@code documentTitle}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#getIdItem(java.sql.Connection,
	 *      java.lang.String)
	 */
	@Override
	public Long getIdItem(Connection connection, String documentTitle) {
		Long idItem = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String query = "SELECT CPS_ID FROM CODA_PREPARAZIONE_SPEDIZIONE WHERE CPS_DOC_TITLE = ?";
			ps = connection.prepareStatement(query);
			ps.setString(1, documentTitle);
			rs = ps.executeQuery();
			if (rs.next()) {
				idItem = rs.getLong(CPS_ID);
			}
		} catch (Exception e) {
			LOGGER.error("Errore nel recupero dell'id item del documento con document title: " + documentTitle, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return idItem;
	}

	/**
	 * Restituisce l'identificativo dell'area organizzativa di riferimento dell'item
	 * identificato dall' {@code idItem}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#getIdAoo(java.sql.Connection,
	 *      java.lang.Long)
	 */
	@Override
	public Long getIdAoo(Connection connection, Long idItem) {
		Long idAoo = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String query = "SELECT CPS_AOO_ID FROM CODA_PREPARAZIONE_SPEDIZIONE WHERE CPS_ID = ?";
			ps = connection.prepareStatement(query);
			ps.setLong(1, idItem);
			rs = ps.executeQuery();
			if (rs.next()) {
				idAoo = rs.getLong(CPS_AOO_ID);
			}

		} catch (Exception e) {
			LOGGER.error("Errore nel recupero dell'aoo dell'item: " + idItem, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return idAoo;
	}

	/**
	 * Restituisce i document title dei documenti presenti logicamente nella coda
	 * {@code queue} e nello {@link StatusEnum#ELABORATO}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#getDTSCompleted(java.sql.Connection,
	 *      it.ibm.red.business.enums.DocumentQueueEnum)
	 */
	@Override
	public Set<String> getDTSCompleted(Connection connection, DocumentQueueEnum queue) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Set<String> output = new HashSet<>();
		try {
			String query = "SELECT DISTINCT CPS_DOC_TITLE FROM CODA_PREPARAZIONE_SPEDIZIONE, CODA_PREP_SPED_STATO WHERE CPS_ID = CPT_CPS_ID AND CPT_STATO_ID = ? AND CPS_CODA = ?";

			ps = connection.prepareStatement(query);
			
			// Documenti già interamente processati
			ps.setInt(1, StatusEnum.ELABORATO.getValue());
			
			// Nome della coda
			ps.setString(2, queue.getName());
			rs = ps.executeQuery();
			
			// Si recuperano i document title.
			while (rs.next()) {
				output.add(rs.getString(CPS_DOC_TITLE));
			}
			return output;
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Restituisce gli item in riferimento all'AOO identificata dall' {@code idAoo}
	 * e nello {@link StatusEnum#ERRORE}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#getItemsDTFailed(java.sql.Connection,
	 *      java.lang.Long)
	 */
	@Override
	public List<ASignItemDTO> getItemsDTFailed(Connection connection, Long idAoo) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<ASignItemDTO> itemsFailed = new ArrayList<>();
		try {
			String query = "SELECT DISTINCT CPS_DOC_TITLE, CPS_AOO_ID, CPS_ID, CPS_ID_FIRMATARIO FROM CODA_PREPARAZIONE_SPEDIZIONE, CODA_PREP_SPED_STATO WHERE CPS_AOO_ID = ? AND CPS_ID = CPT_CPS_ID AND CPT_STATO_ID = ? AND CPS_FLAG_COMUNICATO = 0";
			ps = connection.prepareStatement(query);

			int index = 1;
			ps.setLong(index++, idAoo);
			ps.setInt(index++, StatusEnum.ERRORE.getValue());

			rs = ps.executeQuery();
			while (rs.next()) {
				ASignItemDTO item = new ASignItemDTO();
				item.setId(rs.getLong(CPS_ID));
				item.setIdAoo(rs.getLong(CPS_AOO_ID));
				item.setDocumentTitle(rs.getString(CPS_DOC_TITLE));
				item.setIdFirmatario(rs.getLong("CPS_ID_FIRMATARIO"));
				itemsFailed.add(item);
			}

			return itemsFailed;
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IASignDAO#insertIntoSignedDocsBuffer(java.sql.Connection,
	 *      java.lang.String, java.lang.String, byte[], java.lang.Boolean).
	 */
	@Override
	public void insertIntoSignedDocsBuffer(Connection connection, String wobNumber, String guid, byte[] contentFirmato,
			Boolean flagAllegato) {
		PreparedStatement ps = null;
		try {
			
			cleanBuffer(connection, wobNumber, guid);
			
			String query = "INSERT INTO CODA_PREP_SPED_BUFFER (WOB_NUMBER, GUID, CONTENT, FLAG_ALLEGATO) VALUES (?, ?, ?, ?)";
			ps = connection.prepareStatement(query);
			ps.setString(1, wobNumber);
			ps.setString(2, guid);
			Blob blobContent = connection.createBlob();
			blobContent.setBytes(1, contentFirmato);
			ps.setBlob(3, blobContent);
			ps.setInt(4, Boolean.TRUE.equals(flagAllegato) ? BooleanFlagEnum.SI.getIntValue()
					: BooleanFlagEnum.NO.getIntValue());
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * Rimozione eventuali dati sporchi dal buffer dei content.
	 * 
	 * @param connection
	 *            Connessione al database.
	 * @param wobNumber
	 *            Wob number identificativo del documento.
	 * @param guid
	 *            Guid identificativo del content.
	 */
	private void cleanBuffer(final Connection connection, final String wobNumber, final String guid) {
		
		String polishQuery = "DELETE FROM CODA_PREP_SPED_BUFFER WHERE WOB_NUMBER = ? AND GUID = ?";
		PreparedStatement ps = null;
		try {
			ps = connection.prepareStatement(polishQuery);
			ps.setString(1, wobNumber);
			ps.setString(2, guid);
			ps.executeUpdate();
		} catch (SQLException sqe) {
			LOGGER.error("Errore in fase di rimozione dati sprochi per il wob number: " + wobNumber + " e guid: " + guid, sqe);
			throw new RedException("Errore in fase di rimozione dati sprochi per il wob number: " + wobNumber + " e guid: " + guid, sqe);
		} finally {
			closeStatement(ps);
		}
					
		
	}

	/**
	 * @see it.ibm.red.business.dao.IASignDAO#getFromSignedDocsBuffer(java.sql.Connection,
	 *      java.lang.String).
	 */
	@Override
	public Collection<DocumentoDTO> getFromSignedDocsBuffer(Connection connection, String wobNumber) {
		Collection<DocumentoDTO> out = new HashSet<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String query = "SELECT * FROM CODA_PREP_SPED_BUFFER WHERE WOB_NUMBER = ?";
			ps = connection.prepareStatement(query);
			ps.setString(1, wobNumber);
			rs = ps.executeQuery();
			while (rs.next()) {
				out.add(populateBufferedSignedDoc(rs));
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return out;
	}

	/**
	 * @see it.ibm.red.business.dao.IASignDAO#removeDocFromSignedDocsBuffer(java.sql.Connection,
	 *      java.lang.String, java.lang.String).
	 */
	@Override
	public void removeDocFromSignedDocsBuffer(Connection connection, String wobNumber, String guid) {
		PreparedStatement ps = null;
		try {
			String query = "DELETE FROM CODA_PREP_SPED_BUFFER WHERE WOB_NUMBER = ? AND GUID = ?";
			ps = connection.prepareStatement(query);
			ps.setString(1, wobNumber);
			ps.setString(2, guid);
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IASignDAO#removeFromSignedDocsBufferByWobNumber(java.sql.Connection,
	 *      java.lang.String).
	 */
	@Override
	public void removeFromSignedDocsBufferByWobNumber(Connection connection, String wobNumber) {
		PreparedStatement ps = null;
		try {
			String query = "DELETE FROM CODA_PREP_SPED_BUFFER WHERE WOB_NUMBER = ?";
			ps = connection.prepareStatement(query);
			ps.setString(1, wobNumber);
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IASignDAO#updateContentPrincipaleForSignedDocsBuffer(java.sql.Connection,
	 *      byte[], java.lang.String).
	 */
	@Override
	public void updateContentPrincipaleForSignedDocsBuffer(Connection connection, byte[] contentPrincipale,
			String wobNumber) {
		PreparedStatement ps = null;
		try {
			String query = "UPDATE CODA_PREP_SPED_BUFFER SET CONTENT = ? WHERE WOB_NUMBER = ? AND FLAG_ALLEGATO = 0";
			ps = connection.prepareStatement(query);
			Blob blobContent = connection.createBlob();
			blobContent.setBytes(1, contentPrincipale);
			ps.setBlob(1, blobContent);
			ps.setString(2, wobNumber);
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * Memorizza sulla base dati le informazioni aggiuntive sull'item identificato
	 * dal {@code cpsId}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#saveItemData(java.sql.Connection,
	 *      java.lang.Long, java.util.Map)
	 */
	@Override
	public void saveItemData(Connection connection, Long cpsId, Map<String, String> dataToSave) {
		PreparedStatement ps = null;
		try {
			String query = "INSERT INTO CODA_PREP_SPED_DATI(CPS_ID, KEY, VALUE) VALUES (?, ?, ?)";
			ps = connection.prepareStatement(query);

			if (!MapUtils.isEmpty(dataToSave)) {
				// Si rimuovono i valori non validi (nulli, stringa vuota, etc.)
				dataToSave.keySet()
						.removeAll(dataToSave.entrySet().stream()
								.filter(d -> (StringUtils.isNullOrEmpty(d.getValue()) || "null".equals(d.getValue())))
								.map(Entry<String, String>::getKey).collect(Collectors.toList()));

				for (Entry<String, String> d : dataToSave.entrySet()) {
					ps.setLong(1, cpsId);
					ps.setString(2, d.getKey());
					ps.setString(3, d.getValue());

					ps.addBatch();
				}

				ps.executeBatch();
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * Elimina dalla base dati le informazioni aggiuntive sull'item identificato dal
	 * {@code cpsId}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#deleteItemData(java.sql.Connection,
	 *      java.lang.Long)
	 */
	@Override
	public void deleteItemData(Connection connection, Long cpsId) {
		PreparedStatement ps = null;
		try {
			String query = "DELETE FROM CODA_PREP_SPED_DATI WHERE CPS_ID = ?";
			ps = connection.prepareStatement(query);
			ps.setLong(1, cpsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * Restituisce le informazioni aggiuntive sull'item identificato dal
	 * {@code cpsId}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#getItemData(java.sql.Connection,
	 *      java.lang.Long)
	 */
	@Override
	public Map<String, String> getItemData(Connection connection, Long cpsId) {
		Map<String, String> out = new HashMap<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String query = "SELECT KEY, VALUE FROM CODA_PREP_SPED_DATI WHERE CPS_ID = ?";
			ps = connection.prepareStatement(query);
			ps.setLong(1, cpsId);

			rs = ps.executeQuery();
			while (rs.next()) {
				out.put(rs.getString("KEY"), rs.getString("VALUE"));
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return out;
	}

	/**
	 * Restituisce il documento creato a partire dal result set.
	 * 
	 * @param rs
	 *            result set dal quale recuperare le informazioni
	 * @return documento recuperato
	 * @throws SQLException
	 */
	private static DocumentoDTO populateBufferedSignedDoc(ResultSet rs) throws SQLException {
		return new DocumentoDTO(rs.getString("WOB_NUMBER"), rs.getString("GUID"), null, rs.getBytes("CONTENT"),
				BooleanFlagEnum.SI.getIntValue().equals(rs.getInt("FLAG_ALLEGATO")));
	}

	/**
	 * Imposta il flag associato all'avvenuta comunicazione errore di firma del
	 * documento associato all'item identificato dall' {@code idItem}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#setIsNotified(java.lang.Long, boolean,
	 *      java.sql.Connection)
	 */
	@Override
	public void setIsNotified(Long idItem, boolean comunicato, Connection connection) {
		PreparedStatement ps = null;

		try {
			int index = 1;

			String query = "UPDATE CODA_PREPARAZIONE_SPEDIZIONE SET CPS_FLAG_COMUNICATO = ? WHERE CPS_ID = ?";
			ps = connection.prepareStatement(query);
			ps.setInt(index++, comunicato ? 1 : 0);
			ps.setLong(index++, idItem);

			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("Errore riscontrato durante la modifica del flag CPS_ID_COMUNICATO per l'item: " + idItem, e);
			throw new RedException(
					"Errore riscontrato durante la modifica del flag CPS_ID_COMUNICATO per l'item: " + idItem, e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * Restituisce l'id dell'item associato al documento identificato dall'
	 * {@code idDocumento} e dall' {@code idAoo}.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#getIdItemFromIdDocumento(java.lang.Long,
	 *      java.lang.Long, java.sql.Connection)
	 */
	@Override
	public Long getIdItemFromIdDocumento(Long idDocumento, Long idAoo, Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long idItem = null;
		try {
			int index = 1;

			String query = "SELECT CPS_ID FROM CODA_PREPARAZIONE_SPEDIZIONE WHERE CPS_NUMERO_DOCUMENTO = ? AND CPS_AOO_ID = ? AND CPS_ID IN (SELECT CPT_CPS_ID FROM CODA_PREP_SPED_STATO WHERE CPT_DATA_A IS NULL)";
			ps = connection.prepareStatement(query);
			ps.setLong(index++, idDocumento);
			ps.setLong(index++, idAoo);

			rs = ps.executeQuery();

			if (rs.next()) {
				idItem = rs.getLong(CPS_ID);
			}

		} catch (SQLException e) {
			LOGGER.error("Errore riscontrato durante il recupero dell'id item associato al documento con numero: "
					+ idDocumento, e);
			throw new RedException(
					"Errore riscontrato durante il recupero dell'id item associato al documento con numero: "
							+ idDocumento,
					e);
		} finally {
			closeStatement(ps, rs);
		}

		return idItem;
	}

	/**
	 * Restituisce i document title associati ai documenti presenti logicamente
	 * nella coda {@code queue} che non siano ancora stati processati completamente.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#getIncompletedDTs(java.sql.Connection,
	 *      it.ibm.red.business.enums.DocumentQueueEnum, java.lang.Long)
	 */
	@Override
	public Set<String> getIncompletedDTs(Connection connection, DocumentQueueEnum queue, Long idAoo) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Set<String> output = new HashSet<>();
		try {
			String query = "SELECT CPS_DOC_TITLE, CPS_ID FROM CODA_PREPARAZIONE_SPEDIZIONE WHERE CPS_ID NOT IN (SELECT CPT_CPS_ID FROM CODA_PREP_SPED_STATO WHERE CPT_STATO_ID = ?) AND CPS_CODA = ? AND CPS_AOO_ID = ?";
			
			int index = 1;
			ps = connection.prepareStatement(query);
			ps.setInt(index++, StatusEnum.ELABORATO.getValue());
			ps.setString(index++, queue.getName());
			ps.setLong(index++, idAoo);

			rs = ps.executeQuery();
			while (rs.next()) {
				output.add(rs.getString(CPS_DOC_TITLE));
			}

			return output;
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Recupera e restituisce gli id di tutti gli item per i quali è possibile
	 * avviare il processo di firma asincrona.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#getAndLockAllPlayableItems(java.sql.Connection,
	 *      java.lang.Long, java.lang.String)
	 */
	@Override
	public List<Long> getAndLockAllPlayableItems(Connection connection, Long idAoo, String codiceAoo) {
		List<Long> playables = new ArrayList<>();

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {

			StringBuilder sb = new StringBuilder(
					"SELECT * FROM coda_preparazione_spedizione WHERE cps_id IN (SELECT cps_id FROM ( ").append(
							"SELECT * FROM coda_preparazione_spedizione LEFT JOIN coda_prep_sped_stato ON cps_id = cpt_cps_id WHERE ")
							.append("cps_aoo_id = ? AND ((cpt_stato_id = ? AND cpt_data_a IS NULL AND ((SYSDATE - CAST (cps_timer_starting_time AS DATE)) * 24 * 60 > ? ")
							.append("OR cps_timer_starting_time IS NULL)) OR (cpt_stato_id = ? AND cpt_data_a IS NULL AND (SYSDATE - CAST(cps_timer_starting_time AS DATE )) * 24 * 60 > ?)) ")
							.append("ORDER BY CPS_PRIORITY DESC ) WHERE ROWNUM <= ?) FOR UPDATE SKIP LOCKED");

			ps = connection.prepareStatement(sb.toString());

			int index = 1;
			ps.setLong(index++, idAoo);
			ps.setInt(index++, StatusEnum.PRESO_IN_CARICO.getValue());
			ps.setInt(index++, Integer.parseInt(
					PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.TIMOUT_FIRMA_ASINCRONA)));
			ps.setInt(index++, StatusEnum.IN_ELABORAZIONE.getValue());

			Integer jobPartition = Integer.parseInt(PropertiesProvider.getIstance()
					.getParameterByString(codiceAoo + "." + PropertiesNameEnum.ASIGN_JOB_PARTITION.getKey()));
			Integer timeForItem = Integer.parseInt(
					PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.TIMOUT_FIRMA_ASINCRONA));
			// Il tempo di timeout viene definito sulla base del numero massimo degli
			// elementi lavorabili dallo stesso job.
			ps.setInt(index++, timeForItem * jobPartition);
			ps.setInt(index++, jobPartition);

			rs = ps.executeQuery();
			while (rs.next()) {
				playables.add(rs.getLong(CPS_ID));
			}

			closeStatement(ps, rs);

			// Aggiornamento timer per la gestione concorrente degli accessi alla tabella
			for (Long idItem : playables) {

				String query = UPDATE_CODA_PREPARAZIONE_SPEDIZIONE_SET_CPS_TIMER_STARTING_TIME_SYSDATE_WHERE_CPS_ID;
				ps = connection.prepareStatement(query);
				ps.setLong(1, idItem);
				ps.execute();
				ps.close();
			}
			return playables;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Recupera e restituisce tutti gli item per i quale è possibile eseguire un
	 * ulteriore tentativo di firma.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#getAndLockAllResumableItems(java.sql.Connection,
	 *      java.lang.Long, java.lang.String)
	 */
	@Override
	public List<ASignItemDTO> getAndLockAllResumableItems(Connection connection, Long idAoo, String codiceAoo) {
		List<ASignItemDTO> resumables = new ArrayList<>();

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {

			StringBuilder sb = new StringBuilder(
					"SELECT * FROM coda_preparazione_spedizione WHERE cps_id IN (SELECT cps_id FROM ( ").append(
							"SELECT * FROM coda_preparazione_spedizione WHERE cps_aoo_id = ? AND cps_id in (SELECT cpt_cps_id FROM CODA_PREP_SPED_STATO WHERE ")
							.append("cpt_data_a is null AND CPT_STATO_ID = ?) AND cps_retry <= ? ")
							.append("AND (( SYSDATE - CAST(cps_ultimo_retry AS DATE)) * 24 * 60  >= ? OR cps_ultimo_retry IS NULL) ")
							.append("AND (( SYSDATE - CAST(cps_timer_starting_time AS DATE)) * 24 * 60  > ? OR cps_timer_starting_time IS NULL) ")
							.append("ORDER BY cps_priority DESC ) WHERE ROWNUM <= ?) FOR UPDATE SKIP LOCKED");

			ps = connection.prepareStatement(sb.toString());
			int index = 1;

			Integer jobPartition = Integer.parseInt(PropertiesProvider.getIstance()
					.getParameterByString(codiceAoo + "." + PropertiesNameEnum.ASIGN_JOB_PARTITION.getKey()));
			Integer timeForItem = Integer.parseInt(
					PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.TIMOUT_FIRMA_ASINCRONA));

			ps.setLong(index++, idAoo);
			ps.setInt(index++, StatusEnum.RETRY.getValue());
			ps.setInt(index++, Integer
					.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ASIGN_MAX_RETRY)));
			ps.setInt(index++, Integer
					.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ASIGN_DELTA_RETRY)));

			// Il tempo di timeout viene definito sulla base del numero massimo degli
			// elementi lavorabili dallo stesso job.
			ps.setInt(index++, timeForItem * jobPartition);
			ps.setInt(index++, jobPartition);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				ASignItemDTO item = buildItem(rs);
				fillStates(connection, item);
				fillSteps(connection, item, true);

				resumables.add(item);
			}

			closeStatement(ps, rs);

			// Aggiornamento timer per la gestione concorrente degli accessi alla tabella
			for (ASignItemDTO item : resumables) {

				if (item != null && item.getId() != null) {

					String query = UPDATE_CODA_PREPARAZIONE_SPEDIZIONE_SET_CPS_TIMER_STARTING_TIME_SYSDATE_WHERE_CPS_ID;
					ps = connection.prepareStatement(query);
					ps.setLong(1, item.getId());
					ps.execute();
					ps.close();
				}
			}

			return resumables;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Restituisce {@code true} se il documento identificato dal
	 * {@code documentTitle} risulta firmato.
	 * 
	 * @see it.ibm.red.business.dao.IASignDAO#isSignedOnce(java.sql.Connection,
	 *      java.lang.String)
	 */
	@Override
	public boolean isSignedOnce(Connection connection, String documentTitle) {
		boolean signed = false;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			String query = "SELECT DISTINCT CPS_ID FROM CODA_PREPARAZIONE_SPEDIZIONE, CODA_PREP_SPED_STATO WHERE CPS_ID = CPT_CPS_ID AND CPS_DOC_TITLE = ? AND CPT_STATO_ID = ?";
			ps = connection.prepareStatement(query);

			int index = 1;
			ps.setString(index++, documentTitle);
			ps.setInt(index++, StatusEnum.ELABORATO.getValue());

			rs = ps.executeQuery();
			if (rs.next()) {
				signed = true;
			}

		} catch (Exception e) {
			LOGGER.error("Errore durante la verifica di esistenza di una firma sul documento: " + documentTitle, e);
			throw new RedException(
					"Errore durante la verifica di esistenza di una firma sul documento: " + documentTitle, e);
		} finally {
			closeStatement(ps, rs);
		}

		return signed;
	}
}
