package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Date;

import it.ibm.red.business.dto.RegistrazioneAusiliariaDTO;

/**
 * DAO per la gestione della tabella REGISTRAZIONI_AUSILIARIE. Questa tabella
 * viene utilizzata per rendere persistenti i valori che acquisiscono i metadati
 * all'atto della registrazione.
 * 
 * @author SimoneLungarella
 */
public interface IRegistrazioniAusiliarieDAO extends Serializable {

	/**
	 * Rende persistenti le informazioni sulla registrazione ausiliaria.
	 * 
	 * @param connection
	 *            Connessione al database.
	 * @param documentTitle
	 *            Identificativo del documento a cui è associata la registrazione
	 *            ausiliaria.
	 * @param idRegistro
	 *            Identificativo del registor ausiliario associato alla
	 *            registrazione.
	 * @param xml
	 *            Lista dei metadati del registro ausiliario serializzata. @see
	 *            MetadatiEstesiHelper.
	 */
	void saveRegistrazioneAusiliaria(Connection connection, String documentTitle, int idRegistro, String xml,
			int idAoo);

	/**
	 * Elimina la registrazione ausiliaria eventualemente associata al documento
	 * identificato dal {@code documentTitle}.
	 * 
	 * @param documentTitle
	 *            Identificativo del documento a cui è associata la registraizone
	 *            ausiliaria da eliminare.
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @param con
	 *            Connessione al database.
	 */
	void deleteRegistrazioneAusiliaria(String documentTitle, int idAoo, Connection con);

	/**
	 * Restituisce la registrazione ausiliaria associata eventualmente al documento
	 * identificato dal {@code documentTitle}. Restituisce {@code null} se non
	 * esiste alcuna registrazione ausiliaria associata al documento.
	 * 
	 * @param connection
	 *            Connessione al database.
	 * @param documentTitle
	 *            Identificativo del documento a cui è associata la registrazioen
	 *            ausiliaria.
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @return Registrazione ausiliaria recuperata.
	 */
	RegistrazioneAusiliariaDTO getRegistrazioneAusiliaria(Connection connection, String documentTitle, int idAoo);

	/**
	 * Aggiorna la registrazione ausiliaria esistente sul documento identificata dal
	 * {@code documentTitle}.
	 * 
	 * @param connection
	 *            Connessione al database.
	 * @param documentTitle
	 *            Document Title del documento a cui è associata la registrazione
	 *            ausiliaria.
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @param xml
	 *            Lista dei metadati del registro ausiliario serializzata. @see
	 *            MetadatiEstesiHelper.
	 * @param dataRegistrazione
	 *            Data della registrazione ausiliaria.
	 * @param numeroRegistrazione
	 *            Numero della registrazione ausiliaria.
	 * @param idRegistrazione
	 *            Identificativo della registrazione ausiliaria.
	 * @param documentoDefinitivo
	 *            Indica se la registrazione ausiliaria è definitiva e quindi il
	 *            template ad essa associata non è modificabile.
	 */
	void updateRegistrazioneAusiliaria(Connection connection, String documentTitle, int idAoo, String xml,
			Date dataRegistrazione, Integer numeroRegistrazione, String idRegistrazione, boolean documentoDefinitivo);

	/**
	 * Metodo che consente di effettuare un update a numero registrazione, id
	 * registrazione, data registrazione. Questo metodo non modifica i metadati
	 * della registrazione lasciando il documento in uno stato non definitivo.
	 * 
	 * @param connection
	 *            Connessione al database.
	 * @param documentTitle
	 *            Identificativo del documento a cui è associata la registrazione
	 *            ausiliaria.
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @param numeroRegistrazione
	 *            Numero della registrazione ausiliaria da aggiornare.
	 * @param idRegistrazione
	 *            Identificativo della registrazione ausiliaria da aggiornare.
	 * @param dataRegistrazione
	 *            Data della registrazione ausiliaria da aggiornare.
	 */
	void updateParzialeRegistrazioneAusiliaria(Connection connection, String documentTitle, Long idAoo,
			Integer numeroRegistrazione, String idRegistrazione, Date dataRegistrazione);

	/**
	 * Aggiornamento dei dati in input associati alla registrazione ausiliaria.
	 * 
	 * @param connection
	 *            Connessione al database.
	 * @param documentTitle
	 *            Identificativo del documento a cui è associata la registrazione
	 *            ausiliaria.
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @param xml
	 *            Lista dei metadati del registro ausiliario serializzata. @see
	 *            MetadatiEstesiHelper.
	 * @param documentoDefinitivo
	 *            Indica se la registrazione ausiliaria è definitiva e quindi il
	 *            template ad essa associata non è modificabile.
	 */
	void updateDatiRegistrazioneAusiliaria(Connection connection, String documentTitle, int idAoo, String xml,
			boolean documentoDefinitivo);

}