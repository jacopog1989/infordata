package it.ibm.red.business.crypt;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.utils.StringUtils;

/**
 * Utility per la gestione della codifica.
 */
public final class DesCrypterNew {

	/**
	 * LOGGER.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DesCrypterNew.class.getName());
	
	/**
	 * Messaggio di errore sul padding della codifica.
	 */
	private static final String PADDING_ERRATO_PER = "Padding errato per ";

	/**
	 * Lunghezza chiave.
	 */
	private static final int KEY_LENGTH = 128;

	/**
	 * Numero di iterazioni.
	 */
	private static final int ITERATION_COUNT = 65536;

	/**
	 * Algoritmo di crittografia.
	 */
    private static final String ENCRYPT_ALGO = "AES/GCM/NoPadding";

    /**
     * Tab length in bit.
     */
    private static final int TAG_LENGTH_BIT = 128;

    /**
     * IV length in byte.
     */
    private static final int IV_LENGTH_BYTE = 12;
    
    /**
     * Salt length in byte.
     */
    private static final int SALT_LENGTH_BYTE = 16;

    // Costruttore private e vuoto.
    private DesCrypterNew() {
		// Non deve fare niente.
	}
    
    /**
     * @param numBytes
     * @return flusso byte in nonce
     */
    private static byte[] getRandomNonce(final int numBytes) {
        final byte[] nonce = new byte[numBytes];
        new SecureRandom().nextBytes(nonce);
        return nonce;
    }
    
    /**
     * Restituisce un testo codificato in AES in BASE64.
     * @param doLog
     * @param msg
     * @return stringa codificata
     */
    public static String encrypt(final Boolean doLog, final String msg) {
    	String out = null;
    	try {
    		if (!StringUtils.isNullOrEmpty(msg)) {
        		final byte[] pText = msg.getBytes(StandardCharsets.ISO_8859_1);
        		final String password = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.MAIL_LINK_KEY_ENCRYPTION);

        		final byte[] salt = getRandomNonce(SALT_LENGTH_BYTE);
    	        final byte[] iv = getRandomNonce(IV_LENGTH_BYTE);
    	
    	        // Recupero secret key dalla password
    	        final SecretKey aesKeyFromPassword = getAESKeyFromPassword(password.toCharArray(), salt);
    	
    	        final Cipher cipher = Cipher.getInstance(ENCRYPT_ALGO);
    	
    	        cipher.init(Cipher.ENCRYPT_MODE, aesKeyFromPassword, new GCMParameterSpec(TAG_LENGTH_BIT, iv));
    	
    	        final byte[] cipherText = cipher.doFinal(pText);
				final byte[] cipherTextWithIvSalt = ByteBuffer.allocate(iv.length + salt.length + cipherText.length).put(iv)
						.put(salt).put(cipherText).array();
    	
    	        // Stringa generata dal testo in chiaro
    	        out = Base64.getEncoder().encodeToString(cipherTextWithIvSalt);
    		}
    		return out;
		} catch (final Exception e) {
			if (Boolean.TRUE.equals(doLog)) {
				LOGGER.error(e);
			}
			throw new RedException(e);
		}

    }

    /**
     * Restituisce plain text da stringa codificata da {@link #encrypt(Boolean, String)}.
     * @param doLog
     * @param cText
     * @return testo in chiaro
     */
    public static String decrypt(final boolean doLog, final String cText) {
    	try {
    		final String password = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.MAIL_LINK_KEY_ENCRYPTION);
    		
    		byte[] decode = null;
    		try {
            	decode = Base64.getDecoder().decode(cText.getBytes(StandardCharsets.ISO_8859_1));
    		} catch (IllegalArgumentException e) {
    			if (doLog) {
    				LOGGER.error(PADDING_ERRATO_PER + cText, e);
    			}
    			decode = manageBase64Padding(doLog, cText);
    		}

            final ByteBuffer bb = ByteBuffer.wrap(decode);

            final byte[] iv = new byte[IV_LENGTH_BYTE];
            bb.get(iv);

            final byte[] salt = new byte[SALT_LENGTH_BYTE];
            bb.get(salt);

            final byte[] cipherText = new byte[bb.remaining()];
            bb.get(cipherText);

            final SecretKey aesKeyFromPassword = getAESKeyFromPassword(password.toCharArray(), salt);

            final Cipher cipher = Cipher.getInstance(ENCRYPT_ALGO);

            cipher.init(Cipher.DECRYPT_MODE, aesKeyFromPassword, new GCMParameterSpec(TAG_LENGTH_BIT, iv));

            final byte[] plainText = cipher.doFinal(cipherText);

            return new String(plainText, StandardCharsets.ISO_8859_1);
    	} catch (final Exception e) {
			if (Boolean.TRUE.equals(doLog)) {
				LOGGER.error(e);
			}
    		throw new RedException(e);
    	}

    }
    
    /**
	 * Gestione del Base64 padding in caso di viewer PdfJS.
	 * 
	 * @param doLog
	 * @param cText
	 * @return Byte array della stringa decodificata.
	 */
    private static byte[] manageBase64Padding(final boolean doLog, final String cText) {
    	String textToDecode = cText;
    	byte[] decode = null;
    	if(textToDecode.endsWith("==")) {//se la stringa contiene 2 =
			//prova con 1 =
			textToDecode = textToDecode.substring(0, textToDecode.length()-1);
			try {
				decode = Base64.getDecoder().decode(textToDecode.getBytes(StandardCharsets.ISO_8859_1));
			} catch (IllegalArgumentException e1) {
				if (doLog) {
    				LOGGER.error(PADDING_ERRATO_PER + textToDecode, e1);
    			}
				//prova con 0 =
				textToDecode = textToDecode.substring(0, textToDecode.length()-1);
				decode = Base64.getDecoder().decode(textToDecode.getBytes(StandardCharsets.ISO_8859_1));
			}
		} else if(textToDecode.endsWith("=")) {//se la stringa contiene 1 =
			//prova con 0 =
			textToDecode = textToDecode.substring(0, textToDecode.length()-1);
			try {
				decode = Base64.getDecoder().decode(textToDecode.getBytes(StandardCharsets.ISO_8859_1));
			} catch (IllegalArgumentException e1) {
				if (doLog) {
    				LOGGER.error(PADDING_ERRATO_PER + textToDecode, e1);
    			}
				//prova con 2 =
				textToDecode = textToDecode + "==";
				decode = Base64.getDecoder().decode(textToDecode.getBytes(StandardCharsets.ISO_8859_1));
			}
		} else {//se la stringa contiene 0 =
			//prova con 1 =
			textToDecode = textToDecode + "=";
			try {
				decode = Base64.getDecoder().decode(textToDecode.getBytes(StandardCharsets.ISO_8859_1));
			} catch (IllegalArgumentException e1) {
				if (doLog) {
    				LOGGER.error(PADDING_ERRATO_PER + textToDecode, e1);
    			}
				//prova con 2 =
				textToDecode = textToDecode + "=";
				decode = Base64.getDecoder().decode(textToDecode.getBytes(StandardCharsets.ISO_8859_1));
			}
		}
    	
    	return decode;
    }

    /**
     * Restituisce il secretKeySpec generato a partire dalla password.
     * @param password
     * @param salt
     * @return SecretKeySpec per la gestione della codifica
     */
    private static SecretKey getAESKeyFromPassword(final char[] password, final byte[] salt) {
    	try {
            final SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            final KeySpec spec = new PBEKeySpec(password, salt, ITERATION_COUNT, KEY_LENGTH);
            return new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		}
    }

}
