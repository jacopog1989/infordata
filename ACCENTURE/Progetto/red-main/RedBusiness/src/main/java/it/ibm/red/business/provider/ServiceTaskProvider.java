package it.ibm.red.business.provider;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.HashMap;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.ServiceTaskDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

public final class ServiceTaskProvider {
	
	/**
	 * Messaggio errore caricamento service task.
	 */
	private static final String ERROR_CARICAMENTO_SERVICE_TASK_MSG = "Errore in fase di caricamento service task: [";

	/**
	 * Istanza (singleton).
	 */
	private static ServiceTaskProvider provider = new ServiceTaskProvider();

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ServiceTaskProvider.class.getName());

	/**
	 * Servizi.
	 */
	private HashMap<String, ServiceTaskDTO> services;
	
	/**
	 * Costruttore.
	 */
	private ServiceTaskProvider() {
		services = new HashMap<>();
		ClassPathScanningCandidateComponentProvider scanner =
				new ClassPathScanningCandidateComponentProvider(true);
		for (BeanDefinition bd : scanner.findCandidateComponents("it.ibm.red.business.service.concrete")) {
			try {
				Class<?> serviceCls = Class.forName(bd.getBeanClassName());
				for (Method mtd : serviceCls.getMethods()) {
					ServiceTask st = mtd.getAnnotation(ServiceTask.class);
					if (st != null) {
						ServiceTaskDTO serviceDTO = services.get(st.name());
						if (serviceDTO == null) {
							serviceDTO = new ServiceTaskDTO(serviceCls);
						}
						if (Boolean.TRUE.equals(isMultiService(mtd))) {
							if (serviceDTO.getMultiService() == null) {
								serviceDTO.setMultiService(mtd);
							} else {
								throw new RedException(ERROR_CARICAMENTO_SERVICE_TASK_MSG + st.name() + "] utilizzato per due service task multipli.");
							}
						} else if (Boolean.TRUE.equals(isSingleService(mtd))) {
							if (serviceDTO.getSingleService() == null) {
								serviceDTO.setSingleService(mtd);
							} else {
								throw new RedException(ERROR_CARICAMENTO_SERVICE_TASK_MSG + st.name() + "] utilizzato per due service task singoli.");
							}
						} else {
							throw new RedException(ERROR_CARICAMENTO_SERVICE_TASK_MSG + st.name() + "] associato ad un metodo che non corrisponde alla sintassi dei service task singoli o multipli.");
						}
						services.put(st.name(), serviceDTO);
					}
				}
			} catch (Exception e) {
				LOGGER.error(e);
				throw new RedException(e);
			}
		}
	}
	
	/**
	 * Restituisce il provider.
	 * @return
	 */
	public static ServiceTaskProvider getProvider() {
		return provider;
	}

	private static Boolean isSingleService(final Method mtd) {
		return mtd.getReturnType().equals(EsitoOperazioneDTO.class);
	}
	
	private static Boolean isMultiService(final Method mtd) {
		Boolean output = false;
		Type returnType = mtd.getReturnType();
		if (returnType.getTypeName().equalsIgnoreCase(Collection.class.getName())) {
			ParameterizedType type = (ParameterizedType) mtd.getGenericReturnType();
			Type[] typeArguments = type.getActualTypeArguments();
			if (typeArguments.length == 1) {
				Type typeArgument = typeArguments[0];
				if (typeArgument.getTypeName().equalsIgnoreCase(EsitoOperazioneDTO.class.getName())) {
					output = true; 
				}
			}
		}
		return output;
	} 

	/**
	 * Restituisce le informazioni sul service task con nome: name.
	 * @param name
	 * @return service task info
	 */
	public ServiceTaskDTO getServiceTaskInfo(final String name) {
		return services.get(name);
	}

	@Retention(RetentionPolicy.RUNTIME)
	@Target(ElementType.METHOD)
	public @interface ServiceTask {
		/**
		 * Name.
		 * @return name
		 */
		String name();
	}
}
