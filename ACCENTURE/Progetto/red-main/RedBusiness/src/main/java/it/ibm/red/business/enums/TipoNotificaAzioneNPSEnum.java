package it.ibm.red.business.enums;

/**
 * Enum che definisce i tipi notifica azione.
 */
public enum TipoNotificaAzioneNPSEnum {

	/**
	 * Valore.
	 */
	CREAZIONE_PROTOCOLLO,
	 
	/**
	 * Valore.
	 */
    AGGIORNAMENTO_PROTOCOLLO,
    
	/**
	 * Valore.
	 */
    ANNULLAMENTO_PROTOCOLLO,
    
	/**
	 * Valore.
	 */
    CAMBIO_STATO_ENTRATA,
    
	/**
	 * Valore.
	 */
    CAMBIO_STATO_USCITA,
    
	/**
	 * Valore.
	 */
    RISPONDI_A,
    
	/**
	 * Valore.
	 */
    INSERIMENTO_DOCUMENTI,
    
	/**
	 * Valore.
	 */
    RIMOZIONE_DOCUMENTI,
    
	/**
	 * Valore.
	 */
    INSERIMENTO_DESTINATARIO,
    
	/**
	 * Valore.
	 */
    RIMOZIONE_DESTINATARIO,
    
	/**
	 * Valore.
	 */
    SPEDIZIONE_DESTINATARIO_ELETTRONICO,
    
	/**
	 * Valore.
	 */
    SPEDIZIONE_DESTINATARIO_CARTACEO,
    
	/**
	 * Valore.
	 */
    ASSEGNAZIONE_APPLICAZIONE,
    
	/**
	 * Valore.
	 */
    RIMOZIONE_ASSEGNAZIONE_APPLICAZIONE,
    
	/**
	 * Valore.
	 */
    CAMBIO_ASSEGNATARIO_ASSEGNAZIONE,
    
    /**
     * Valore.
     */
    AGGIORNAMENTO_COLLEGATI;
	
	/**
	 * Restituisce il value.
	 * @return value
	 */
    public String value() {
        return name();
    }

    /**
     * Restituisce l'enum del value.
     * @param v
     * @return enum associata al value v
     */
    public static TipoNotificaAzioneNPSEnum fromValue(final String v) {
    	return valueOf(v);
    }
    
    /**
     * Indica se la notifica in input gestisce un protocollo di risposta pervenuto da sistema esterno.
     * @param v
     * @return true se è una risposta automatica, false altrimenti
     */
    public static boolean isRispostaAutomatica(final String v) {
    	TipoNotificaAzioneNPSEnum e = fromValue(v);
    	return isRispostaAutomatica(e);
    }
    
    /**
     * Indica se la notifica in input gestisce un protocollo di risposta pervenuto da sistema esterno.
     * @param E
     * @return true se è una risposta automatica, false altrimenti
     */
    public static boolean isRispostaAutomatica(final TipoNotificaAzioneNPSEnum e) {
    	return RISPONDI_A.equals(e) || AGGIORNAMENTO_COLLEGATI.equals(e);
    }

}
