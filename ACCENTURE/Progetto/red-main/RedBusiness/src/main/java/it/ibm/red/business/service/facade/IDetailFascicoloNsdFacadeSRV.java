package it.ibm.red.business.service.facade;

import java.io.Serializable;

import it.ibm.red.business.dto.DetailFascicoloPregressoDTO; 
import it.ibm.red.business.dto.FascicoliPregressiDTO; 
import it.ibm.red.business.dto.UtenteDTO;

/**
 * @author VINGENITO
 * 
 */
public interface IDetailFascicoloNsdFacadeSRV extends Serializable {
	
	/**
	 * Ottiene il dettaglio del fascicolo pregresso.
	 * @param fascicolo
	 * @param utente
	 * @return dettaglio del fascicolo pregresso
	 */
	DetailFascicoloPregressoDTO getFascicoloByIdNsd(FascicoliPregressiDTO fascicolo, UtenteDTO utente);
 
}
