package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;

import it.ibm.red.business.persistence.model.DatiPredefinitiMozione;

/**
 * 
 * @author m.crescentini
 *
 *	Dao dati predefiniti mozione.
 */
public interface IDatiPredefinitiMozioneDAO extends Serializable {
	
	/**
	 * Ottiene i dati predefiniti mozione.
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @param idUfficio
	 * @param idIterApprovativo
	 * @param connection
	 * @return
	 */
	DatiPredefinitiMozione getDatiPredefinitiMozione(int idTipologiaDocumento, long idTipoProcedimento, Long idUfficio, Integer idIterApprovativo, Connection connection);
	
	/**
	 * Serve per sapere se bisogna cekkare di default il flag RGS al momento della creazione del documento.
	 * 
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @param connection
	 * @return
	 */
	DatiPredefinitiMozione getFirmaDigitaleRGS(int idTipologiaDocumento, long idTipoProcedimento, Connection connection);
}