package it.ibm.red.business.enums;

/**
 * @author m.crescentini
 *
 */
public enum TipoMessaggioPostaNpsIngressoEnum {

	/**
	 * Valore.
	 */
	PEO_PEC(false),
	
	/**
	 * Valore.
	 */
	PEO_PEC_PROT_AUTO(true),
	
	/**
	 * Valore.
	 */
	PEO_PEC_FLUSSO(true),
	
	/**
	 * Valore.
	 */
	NOTIFICA_PEC(false),
	
	/**
	 * Valore.
	 */
	NOTIFICA_INTEROPERABILITA(false),
	
	/**
	 * Valore.
	 */
	NOTIFICA_ERRORE_PROT_AUTO(false),

	/**
	 * Valore.
	 */
	NO_MAIL(true); // SICOGE
	
	
	/**
	 * Flag protocollazione automatica.
	 */
	private boolean protocollazioneAutomatica; // Indica se il messaggio di posta è già stato protocollato da NPS
	
	
	/**
	 * @param protocollazioneAutomatica
	 */
	TipoMessaggioPostaNpsIngressoEnum(final boolean protocollazioneAutomatica) {
		this.protocollazioneAutomatica = protocollazioneAutomatica;
	}


	/**
	 * @return the protocollazioneAutomatica
	 */
	public boolean isProtocollazioneAutomatica() {
		return protocollazioneAutomatica;
	}

	/**
	 * Restituisce l'enum associata al nome.
	 * @param nome
	 * @return enum associata a nome
	 */
	public static TipoMessaggioPostaNpsIngressoEnum get(final String nome) {
		TipoMessaggioPostaNpsIngressoEnum output = null;
		
		for (final TipoMessaggioPostaNpsIngressoEnum tipoMessaggioInterop : TipoMessaggioPostaNpsIngressoEnum.values()) {
			if (tipoMessaggioInterop.name().equalsIgnoreCase(nome)) {
				output = tipoMessaggioInterop;
				break;
			}
		}
		
		return output;
	}
	
}
