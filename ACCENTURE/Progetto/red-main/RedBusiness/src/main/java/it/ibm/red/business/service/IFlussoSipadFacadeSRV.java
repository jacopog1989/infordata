package it.ibm.red.business.service;

import java.io.Serializable;

import it.ibm.red.business.dto.FileDTO;

/**
 * Facade service flusso SIPAD.
 * @author s.lungarella.ibm
 */
public interface IFlussoSipadFacadeSRV extends Serializable{

	/**
	 * Restituisce il file generato dalla trasformazione <em> xml </em> in pdf, la
	 * trasformazione segue le regole della trasformata <em> xslt </em>.
	 * 
	 * @param xml
	 *            content da trasformare
	 * @param xslt
	 *            trasformata <em> xslt </em> che indica il modo in cui trasformare
	 *            il content
	 * @return file pdf generato dalla trasformazione
	 */
	FileDTO transformXmlForSipad(byte[] xml, byte[] xslt);
}
