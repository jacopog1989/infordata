package it.ibm.red.business.service.ws.concrete;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.core.Document;
import com.filenet.api.meta.PropertyDescription;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.constants.Constants.Ricerca;
import it.ibm.red.business.constants.Constants.StatoConservazione;
import it.ibm.red.business.dao.IConservazioneDAO;
import it.ibm.red.business.dao.IEventoLogDAO;
import it.ibm.red.business.dao.IGestioneNotificheEmailDAO;
import it.ibm.red.business.dao.impl.IterApprovativoDAO;
import it.ibm.red.business.dto.CodaTrasformazioneParametriDTO;
import it.ibm.red.business.dto.ConservazioneWsDTO;
import it.ibm.red.business.dto.DocumentoWsDTO;
import it.ibm.red.business.dto.EventoLogDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.PropertyTemplateDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.SecurityDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.FilenetException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.adobe.AdobeLCHelper;
import it.ibm.red.business.helper.adobe.IAdobeLCHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IDocumentoRedSRV;
import it.ibm.red.business.service.IDocumentoSRV;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.ISecuritySRV;
import it.ibm.red.business.service.ITrasformazionePDFSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.service.concrete.AbstractService;
import it.ibm.red.business.service.ws.IDocumentoWsSRV;
import it.ibm.red.business.service.ws.auth.DocumentServiceAuthorizable;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.webservice.model.documentservice.dto.Servizio;
import it.ibm.red.webservice.model.documentservice.types.documentale.ClasseDocumentale;
import it.ibm.red.webservice.model.documentservice.types.documentale.Metadato;
import it.ibm.red.webservice.model.documentservice.types.documentale.TipologiaPDFOutput;
import it.ibm.red.webservice.model.documentservice.types.messages.AggiungiAllacciAsyncType;
import it.ibm.red.webservice.model.documentservice.types.messages.AssociateDocumentoProtocolloToAsyncNpsType;
import it.ibm.red.webservice.model.documentservice.types.messages.GestisciDestinatariInterniType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedConservaDocumentoType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedElencoPropertiesType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedElencoVersioniDocumentoType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedGetVersioneDocumentoType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedModificaMetadatiType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedModificaSecurityType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedProtocolloNPSSyncType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedTrasformazionePDFType;
import it.ibm.red.webservice.model.documentservice.types.messages.SpedisciProtocolloUscitaAsyncType;
import it.ibm.red.webservice.model.documentservice.types.messages.TrasformazionePDFType;
import it.ibm.red.webservice.model.documentservice.types.messages.UploadDocToAsyncNpsType;
import it.ibm.red.webservice.model.documentservice.types.security.Security;

/**
 * @author m.crescentini
 * 
 *         Servizio che trasforma i documenti RED in DTO per il web service.
 *
 */
@Service
public class DocumentoWsSRV extends AbstractService implements IDocumentoWsSRV {

	private static final long serialVersionUID = -2121106023653725031L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DocumentoWsSRV.class.getName());

	/**
	 * Servizio.
	 */
	@Autowired
	private IFascicoloSRV fascicoloSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IDocumentoRedSRV documentoRedSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private ITrasformazionePDFSRV trasformazionePDFSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private ISecuritySRV securitySRV;

	/**
	 * Dao.
	 */
	@Autowired
	private IEventoLogDAO eventoLogDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private IGestioneNotificheEmailDAO gestioneNotificheEmailDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private IterApprovativoDAO iterApprovativoDao;

	/**
	 * Dao.
	 */
	@Autowired
	private IConservazioneDAO conservaDAO;

	/**
	 * Servizio.
	 */
	@Autowired
	private IDocumentoSRV documentoSRV;

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.service.ws.facade.IDocumentoWsFacadeSRV#
	 * getElencoVersioniDocumento
	 * (it.ibm.red.business.persistence.model.RedWsClient,
	 * it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedElencoVersioniDocumentoType)
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_ELENCO_VERSIONI_DOCUMENTO)
	public final List<DocumentoWsDTO> getElencoVersioniDocumento(final RedWsClient client, final RedElencoVersioniDocumentoType request) {
		final List<DocumentoWsDTO> versioniDocWs = new ArrayList<>();
		Connection con = null;
		IFilenetCEHelper fceh = null;

		final Integer documentTitle = request.getDocumentTitle();

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final UtenteDTO utente = utenteSRV.getById(Long.valueOf(request.getIdUtente()), con);

			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final List<Document> versioniDocFilenet = documentoRedSRV.getElencoVersioniDocumento(String.valueOf(documentTitle), utente.getIdAoo(), fceh);

			if (!CollectionUtils.isEmpty(versioniDocFilenet)) {

				for (final Document versioneDocFilenet : versioniDocFilenet) {
					versioniDocWs.add(transformToDocumentoWsPerElencoVersioni(versioneDocFilenet));
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero delle versioni FileNet del documento con ID: " + documentTitle, e);
			throw new RedException("Errore durante il recupero delle versioni FileNet del documento con ID: " + documentTitle, e);
		} finally {
			closeConnection(con);
		}

		return versioniDocWs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.service.ws.facade.IDocumentoWsFacadeSRV#
	 * getVersioneDocumento (it.ibm.red.business.persistence.model.RedWsClient,
	 * it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedGetVersioneDocumentoType)
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_GET_VERSIONE_DOCUMENTO)
	public final DocumentoWsDTO getVersioneDocumento(final RedWsClient client, final RedGetVersioneDocumentoType request) {
		DocumentoWsDTO docWs = null;
		IFilenetCEHelper fceh = null;
		Connection con = null;

		final Integer numeroVersione = request.getNumeroVersione();
		final Integer documentTitle = request.getDocumentTitle();

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final UtenteDTO utente = utenteSRV.getById(Long.valueOf(request.getIdUtente()), con);

			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final Document docFilenet = documentoRedSRV.getVersioneDocumento(String.valueOf(documentTitle), numeroVersione, utente.getIdAoo(), fceh);

			if (!Boolean.TRUE.equals(request.isOnlyContent())) {
				// Estrazione dei soli metadati eventualmente richiesti
				Set<String> metadatiDaRecuperare = null;

				final List<String> metadatiRichiesti = request.getMetadati();
				if (!CollectionUtils.isEmpty(metadatiRichiesti)) {
					metadatiDaRecuperare = new HashSet<>();
					metadatiRichiesti.stream().forEach(metadatiDaRecuperare::add);
				}

				// Documento completo (la presenza del content dipende dal flag "withContent")
				docWs = transformToDocumentoWs(docFilenet, Boolean.TRUE.equals(request.isWithContent()), true, metadatiDaRecuperare, utente.getIdAoo(), fceh, con);
			} else {
				// Documento con il solo content
				final FileDTO contentFile = getContentDocumento(docFilenet);

				if (contentFile != null && contentFile.getContent() != null && contentFile.getContent().length > 0) {
					docWs = new DocumentoWsDTO(contentFile.getContent(), contentFile.getMimeType());
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero della versione: " + numeroVersione + " del documento con ID: " + documentTitle, e);
			throw new RedException("Errore durante il recupero della versione richiesta del documento. [" + e.getMessage() + "]", e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}

		return docWs;
	}

	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_ELENCO_PROPERTIES)
	public final List<PropertyTemplateDTO> getElencoProperies(final RedWsClient client, final RedElencoPropertiesType request) {
		IFilenetCEHelper fceh = null;
		Connection con = null;
		List<PropertyTemplateDTO> output = new ArrayList<>();

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final UtenteDTO utente = utenteSRV.getById(request.getIdUtente().longValue(), con);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			output = fceh.elencoProperties();

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante delle Properties di Filenet da WS.", e);
			throw new RedException("Si è verificato un errore durante delle Properties di Filenet da WS.", e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}

		return output;
	}

	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_MODIFICA_METADATI)
	public final void modificaMetadati(final RedWsClient client, final RedModificaMetadatiType request) {
		IFilenetCEHelper fceh = null;
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final UtenteDTO utente = utenteSRV.getById(request.getIdUtente().longValue(), con);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final Document documentToUpdate = fceh.getDocumentByDTandAOO(request.getDocumento().getIdDocumento(), request.getIdAoo().longValue());

			final ClasseDocumentale classeDocumentale = request.getDocumento().getClasseDocumentale();
			final Map<String, Object> metadatiMap = new HashMap<>();
			if (classeDocumentale != null && classeDocumentale.getMetadato() != null && !classeDocumentale.getMetadato().isEmpty()) {

				final Map<String, PropertyDescription> hmp = FilenetCEHelper.getHasMapClassDefinition(documentToUpdate);

				for (final Metadato m : classeDocumentale.getMetadato()) {

					final PropertyDescription pd = hmp.get(m.getKey());
					final int type = pd.get_DataType().getValue();
					final int cardinality = pd.get_Cardinality().getValue();

					final Object valueParsed = FilenetCEHelper.getObjectByProperty(m.getKey(), m.getValue(), m.getMultiValues(), type, cardinality);

					if (valueParsed != null) {
						metadatiMap.put(m.getKey(), valueParsed);
					}
				}
			}

			fceh.setPropertiesAndSaveDocument(documentToUpdate, metadatiMap, Boolean.TRUE);

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la modifica dei metadati da WS", e);
			throw new RedException("Si è verificato un errore durante la modifica dei metadati da WS", e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.service.ws.facade.IDocumentoWsFacadeSRV#
	 * redTrasformazionePDF (it.ibm.red.business.persistence.model.RedWsClient,
	 * it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedTrasformazionePDFType)
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_TRASFORMAZIONE_PDF)
	public final DocumentoWsDTO redTrasformazionePDF(final RedWsClient client, final RedTrasformazionePDFType requestTrasformazionePDF) {
		DocumentoWsDTO documentoWs = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			final PropertiesProvider pp = PropertiesProvider.getIstance();

			final String documentTitle = String.valueOf(requestTrasformazionePDF.getDocumentTitle());

			// Si recupera l'utente
			final UtenteDTO utente = utenteSRV.getById(requestTrasformazionePDF.getIdUtente().longValue(), con);

			fpeh = new FilenetPEHelper(utente.getFcDTO());

			// Si recupera il workflow principale
			final VWWorkObject wob = fpeh.getWorkflowPrincipale(documentTitle, utente.getFcDTO().getIdClientAoo());

			if (wob != null) {
				// Tipo firma
				final Integer tipoFirma = iterApprovativoDao.getTipoFirma(utente.getIdAoo().intValue(), wob.getWorkflowName(), con);

				// Tipo assegnazione
				final Integer idTipoAssegnazione = (Integer) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY));

				// Firmatari
				String[] firmatari = (String[]) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ELENCO_LIBROFIRMA_METAKEY));
				if (firmatari == null || firmatari.length == 0 || StringUtils.isNullOrEmpty(firmatari[0])) {
					firmatari = null;
					if (TipoAssegnazioneEnum.isIn(TipoAssegnazioneEnum.get(idTipoAssegnazione), TipoAssegnazioneEnum.FIRMA, TipoAssegnazioneEnum.FIRMA_MULTIPLA,
							TipoAssegnazioneEnum.SIGLA)) {
						firmatari = new String[1];
						firmatari[0] = TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY)) + ","
								+ TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY));
					}
				}

				// Aggiorna PDF
				final boolean aggiornaPdf = TipoAssegnazioneEnum.isIn(TipoAssegnazioneEnum.get(idTipoAssegnazione), TipoAssegnazioneEnum.FIRMA,
						TipoAssegnazioneEnum.FIRMA_MULTIPLA, TipoAssegnazioneEnum.SIGLA, TipoAssegnazioneEnum.VISTO);

				final CodaTrasformazioneParametriDTO parametriTrasformazione = new CodaTrasformazioneParametriDTO();
				parametriTrasformazione.setIdDocumento(documentTitle);
				parametriTrasformazione.setPrincipale(true);

				parametriTrasformazione.setIdNodo(utente.getIdUfficio());
				parametriTrasformazione.setIdUtente(utente.getId());

				parametriTrasformazione.setIdTipoAssegnazione(idTipoAssegnazione);
				parametriTrasformazione.setTipoFirma(tipoFirma);
				parametriTrasformazione.setFirmatariString(firmatari);

				parametriTrasformazione.setAggiornaFirmaPDF(aggiornaPdf);
				parametriTrasformazione.setProtocollazioneP7M(false);
				parametriTrasformazione.setFlagFirmaCopiaConforme(false);

				if (!Boolean.TRUE.equals(requestTrasformazionePDF.isOnlyPrincipale())) {
					parametriTrasformazione.setAllegati(true);

					if (!CollectionUtils.isEmpty(requestTrasformazionePDF.getIdAllegati())) {
						parametriTrasformazione.setIdAllegati(
								requestTrasformazionePDF.getIdAllegati().stream().filter(Objects::nonNull).map(id -> id.toString()).collect(Collectors.toList()));
					}

					if (!CollectionUtils.isEmpty(requestTrasformazionePDF.getIdAllegatiDaFirmare())) {
						parametriTrasformazione.setIdAllegatiDaFirmare(
								requestTrasformazionePDF.getIdAllegatiDaFirmare().stream().filter(Objects::nonNull).map(id -> id.toString()).collect(Collectors.toList()));
					}
				}

				// Si esegue la trasformazione PDF in modalità sincrona
				trasformazionePDFSRV.doTransformation(parametriTrasformazione);

				fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

				final Document nuovaVersioneDocFilenet = fceh.getDocumentByDTandAOO(documentTitle, utente.getIdAoo());

				if (Boolean.TRUE.equals(requestTrasformazionePDF.isReturnDocument())) {
					documentoWs = transformToDocumentoWs(nuovaVersioneDocFilenet, true, false, utente.getIdAoo(), fceh, con);
				} else {
					documentoWs = new DocumentoWsDTO(nuovaVersioneDocFilenet.get_MajorVersionNumber());
				}
			} else {
				throw new RedException("Errore durante il recupero del workflow principale");
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la trasformazione PDF del documento con Document Title [" + requestTrasformazionePDF.getDocumentTitle() + "]: " + e.getMessage(), e);
			throw new RedException(
					"Errore durante la trasformazione PDF del documento con Document Title [" + requestTrasformazionePDF.getDocumentTitle() + "]: " + e.getMessage(), e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
			logoff(fpeh);
		}

		return documentoWs;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.service.ws.facade.IDocumentoWsFacadeSRV#
	 * redTrasformazionePDF (it.ibm.red.business.persistence.model.RedWsClient,
	 * it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedTrasformazionePDFType)
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.TRASFORMAZIONE_PDF)
	public final FileDTO trasformazionePDF(final RedWsClient client, final TrasformazionePDFType requestTrasformazionePDF) {
		FileDTO outputPdfFile = null;

		try {
			final IAdobeLCHelper adobeh = new AdobeLCHelper();

			final boolean outputPdfA = TipologiaPDFOutput.PDF_A.equals(requestTrasformazionePDF.getTipologiaOutput());

			// La trasformazione PDF si esegue sempre
			byte[] contentPdf = adobeh.trasformazionePDF(requestTrasformazionePDF.getNomeFile(), requestTrasformazionePDF.getContent(), outputPdfA,
					"" + requestTrasformazionePDF.getIdDocumentoDaStampigliare());

			final String nomeFilePdf = FilenameUtils.getBaseName(requestTrasformazionePDF.getNomeFile()) + ".pdf";

			// Se richiesto, si stampiglia anche l'ID del documento
			if (TipologiaPDFOutput.PDF_ID_DOC.equals(requestTrasformazionePDF.getTipologiaOutput())) {
				contentPdf = adobeh.insertIdDocumento(contentPdf, ContentType.PDF, nomeFilePdf, requestTrasformazionePDF.getIdDocumentoDaStampigliare());
			}

			outputPdfFile = new FileDTO(nomeFilePdf, contentPdf, ContentType.PDF);
		} catch (final Exception e) {
			LOGGER.error("Errore durante la trasformazione PDF del documento: " + e.getMessage(), e);
			throw new RedException("Errore durante la trasformazione PDF del documento: " + e.getMessage(), e);
		}

		return outputPdfFile;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.ibm.red.business.service.ws.facade.IDocumentoWsFacadeSRV#conservaDocumento
	 * (it.ibm.red.business.persistence.model.RedWsClient,
	 * it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedConservaDocumentoType)
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_CONSERVA_DOCUMENTO)
	public final ConservazioneWsDTO conservaDocumento(final RedWsClient client, final RedConservaDocumentoType request) {
		ConservazioneWsDTO output = null;
		String documentTitle = null;
		IFilenetCEHelper fceh = null;
		Connection con = null;
		boolean isConservato = false;

		try {
			documentTitle = request.getDocumentTitle();
			con = setupConnection(getDataSource().getConnection(), false);

			final UtenteDTO utente = utenteSRV.getById(request.getIdUtente().longValue(), con);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final Document documentToConservare = fceh.getDocumentByDTandAOO(request.getDocumentTitle(), request.getIdAoo().longValue());

			isConservato = conservaDAO.checkDocumentoConservato(documentToConservare.get_Id().toString(), con);
			if (isConservato) {
				throw new RedException("Il Documento risulta già Conservato.");
			}

			output = new ConservazioneWsDTO();
			output.setIdApplicativo(client.getIdClient());
			output.setNomeDocumento(request.getFolder() + File.separator + documentTitle);
			output.setUuidDocumento(documentToConservare.get_Id().toString());
			output.setStato(StatoConservazione.DA_CONSERVARE);

			final int idConservazione = conservaDAO.setConservazione(output, con);
			output.setIdConservazione(idConservazione);

		} catch (final Exception e) {
			String msg = "Si è verificato un errore durante il tentativo di inserire il documento con documentTitle [" + documentTitle
					+ "] sul database con stato DA CONSERVARE. ";
			if (isConservato) {
				msg = "Il Documento risulta già Conservato.";
			}
			LOGGER.error(msg, e);
			throw new RedException(msg, e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IDocumentoWsFacadeSRV#modificaSecurity(it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.RedModificaSecurityType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_MODIFICA_SECURITY)
	public void modificaSecurity(final RedWsClient client, final RedModificaSecurityType request) {
		IFilenetCEHelper fceh = null;
		Connection con = null;
		Boolean isAllVersion = Boolean.FALSE;

		try {

			con = setupConnection(getDataSource().getConnection(), false);

			final UtenteDTO utente = utenteSRV.getById(request.getIdUtente().longValue(), con);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final List<Security> newSecurity = request.getSecurity();
			final String documentTitle = request.getDocumentTitle();
			final Long idAoo = request.getIdAoo().longValue();

			if (request.isAllVersions() != null) {
				isAllVersion = request.isAllVersions();
			}

			final ListSecurityDTO newSecurityDTOList = trasformInListSecurityDTO(newSecurity);

			securitySRV.verificaSecurities(newSecurityDTOList, con);

			securitySRV.aggiornaSecurityDocumento(documentTitle, idAoo, newSecurityDTOList, isAllVersion, false, fceh);

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il tentativo di modifica delle Security", e);
			throw new RedException("Si è verificato un errore durante il tentativo di modifica delle Security", e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}

	}

	private ListSecurityDTO trasformInListSecurityDTO(final List<Security> newSecurity) {
		final ListSecurityDTO output = new ListSecurityDTO();

		try {

			for (final Security sec : newSecurity) {

				// In continuità con quello che faceva FWS con il metodo alignSecurity()
				// gestisco il Gruppo valorizzando l'idUfficio e idSiap
				final SecurityDTO newSecDTO = securitySRV.creaSecurity(null, sec.getUtente().getIdUtente().longValue(), sec.getGruppo().getIdUfficio().longValue());
				output.add(newSecDTO);
			}

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la trasformazione delle Security ricevute in SecurityDTO", e);
			throw new RedException("Si è verificato un errore durante la trasformazione delle Security ricevute in SecurityDTO", e);
		}

		return output;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.service.ws.IDocumentoWsSRV#transformToDocumenti
	 * (com.filenet.api.collection.DocumentSet, boolean, java.lang.Long,
	 * java.sql.Connection)
	 */
	@Override
	public final List<DocumentoWsDTO> transformToDocumentiWsPerRicercaSemplice(final DocumentSet documentiFilenet, final boolean withContent, final Long idAoo,
			final Connection con) {
		List<DocumentoWsDTO> docWsList = null;

		if (!documentiFilenet.isEmpty()) {
			docWsList = new ArrayList<>();

			DocumentoWsDTO doc;
			final Iterator<?> itDocumentiRicercaFilenet = documentiFilenet.iterator();
			while (itDocumentiRicercaFilenet.hasNext()) {
				final Document docRicercaFilenet = (Document) itDocumentiRicercaFilenet.next();

				byte[] content = null;
				String contentType = null;
				if (withContent) {
					final FileDTO contentFile = getContentDocumento(docRicercaFilenet);
					if (contentFile != null) {
						content = contentFile.getContent();
						contentType = contentFile.getMimeType();
					}
				}

				final String idDocumento = (String) TrasformerCE.getMetadato(docRicercaFilenet, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
				final Integer numeroDocumento = (Integer) TrasformerCE.getMetadato(docRicercaFilenet, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
				final Integer annoDocumento = (Integer) TrasformerCE.getMetadato(docRicercaFilenet, PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY);
				final Integer numeroProtocollo = (Integer) TrasformerCE.getMetadato(docRicercaFilenet, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
				final Integer annoProtocollo = (Integer) TrasformerCE.getMetadato(docRicercaFilenet, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
				final String oggetto = (String) TrasformerCE.getMetadato(docRicercaFilenet, PropertiesNameEnum.OGGETTO_METAKEY);

				doc = new DocumentoWsDTO(idDocumento, content, contentType, numeroDocumento, annoDocumento, numeroProtocollo, annoProtocollo, oggetto);

				docWsList.add(doc);
			}

			calcolaStatoDocumenti(docWsList, idAoo, con);
		}

		return docWsList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.service.ws.IDocumentoWsSRV#transformToDocumentoWs
	 * (com.filenet.api.core.Document, boolean, boolean, java.lang.Long,
	 * it.ibm.red.business.helper.filenet.ce.IFilenetHelper, java.sql.Connection)
	 */
	@Override
	public final List<DocumentoWsDTO> transformToDocumentiWs(final DocumentSet documentiFilenet, final boolean withContent, final boolean withMetadati, final Long idAoo,
			final IFilenetCEHelper fceh, final Connection con) {
		List<DocumentoWsDTO> docWsList = null;
		Connection dwhCon = null;

		try {
			dwhCon = setupConnection(getFilenetDataSource().getConnection(), false);

			if (documentiFilenet != null && !documentiFilenet.isEmpty()) {
				docWsList = new ArrayList<>();
				DocumentoWsDTO docWs = null;

				final Iterator<?> itDocumentiFilenet = documentiFilenet.iterator();
				while (itDocumentiFilenet.hasNext()) {
					final Document documentoFilenet = (Document) itDocumentiFilenet.next();
					docWs = transformToDocumentoWs(documentoFilenet, withContent, withMetadati, idAoo, fceh, con, dwhCon);

					docWsList.add(docWs);
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero dei documenti: " + e.getMessage(), e);
			throw new RedException("Errore durante il recupero dei documenti");
		} finally {
			closeConnection(dwhCon);
		}

		return docWsList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.service.ws.IDocumentoWsSRV#transformToDocumentoWs
	 * (com.filenet.api.core.Document, boolean, boolean, java.util.Set,
	 * java.lang.Long, it.ibm.red.business.helper.filenet.ce.IFilenetHelper,
	 * java.sql.Connection)
	 */
	@Override
	public final DocumentoWsDTO transformToDocumentoWs(final Document docFilenet, final boolean withContent, final boolean withMetadati,
			final Set<String> metadatiDaRecuperare, final Long idAoo, final IFilenetCEHelper fceh, final Connection con) {
		DocumentoWsDTO docWs = null;
		Connection dwhCon = null;

		try {
			dwhCon = setupConnection(getFilenetDataSource().getConnection(), false);

			docWs = transformToDocumentoWs(docFilenet, withContent, withMetadati, metadatiDaRecuperare, idAoo, fceh, con, dwhCon);

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero dei documenti: " + e.getMessage(), e);
			throw new RedException("Errore durante il recupero dei documenti");
		} finally {
			closeConnection(dwhCon);
		}

		return docWs;
	}

	/**
	 * @see it.ibm.red.business.service.ws.IDocumentoWsSRV#transformToDocumentoWs
	 *      (com.filenet.api.core.Document, boolean, boolean, java.lang.Long,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public final DocumentoWsDTO transformToDocumentoWs(final Document docFilenet, final boolean withContent, final boolean withMetadati, final Long idAoo,
			final IFilenetCEHelper fceh, final Connection con) {
		return transformToDocumentoWs(docFilenet, withContent, withMetadati, null, idAoo, fceh, con);
	}

	/**
	 * @see it.ibm.red.business.service.ws.IDocumentoWsSRV#transformToDocumentoEmailWs
	 *      (com.filenet.api.core.Document, boolean, boolean, java.util.Set,
	 *      java.lang.Long, it.ibm.red.business.helper.filenet.ce.IFilenetHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public DocumentoWsDTO transformToDocumentoEmailWs(final Document emailFilenet, final boolean withContent, final boolean withMetadati,
			final Set<String> inMetadatiDaRecuperare, final Long idAoo, final IFilenetCEHelper fceh, final Connection con) {
		final Map<ContextTrasformerCEEnum, Object> context = new EnumMap<>(ContextTrasformerCEEnum.class);
		context.put(ContextTrasformerCEEnum.RECUPERA_METADATI, withMetadati);
		if (withMetadati) {
			Set<String> metadatiDaRecuperare = null;
			if (!CollectionUtils.isEmpty(inMetadatiDaRecuperare)) {
				metadatiDaRecuperare = inMetadatiDaRecuperare;
			} else {
				metadatiDaRecuperare = fceh.getClassDefinitionPropertiesNameWithoutSystemProps(emailFilenet.getClassName());
			}
			context.put(ContextTrasformerCEEnum.CLASS_DEFINITION_PROPERTIES, metadatiDaRecuperare);
		}

		return TrasformCE.transform(emailFilenet, TrasformerCEEnum.FROM_DOCUMENT_TO_DOC_EMAIL_WS, context, con);

	}

	/**
	 * @param docFilenet
	 * @return
	 */
	private static FileDTO getContentDocumento(final Document docFilenet) {
		FileDTO contentFile = null;

		try {
			String contentType = null;
			String fileName = null;

			// Si procede al recupero del content
			final byte[] content = FilenetCEHelper.getDocumentContentAsByte(docFilenet);

			if (content != null && content.length > 0) {
				contentType = docFilenet.get_MimeType();
				fileName = (String) TrasformerCE.getMetadato(docFilenet, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);

				contentFile = new FileDTO(fileName, content, contentType);
			}
		} catch (final FilenetException e) {
			LOGGER.warn("Errore nel recupero del content durante la ricerca. Documento con GUID: " + docFilenet.get_Id(), e);
		}

		return contentFile;
	}

	/**
	 * @param docFilenet
	 * @param withContent
	 * @param withMetadati
	 * @param idAoo
	 * @param fceh
	 * @param con
	 * @param dwhCon
	 * @return
	 */
	private DocumentoWsDTO transformToDocumentoWs(final Document docFilenet, final boolean withContent, final boolean withMetadati, final Long idAoo,
			final IFilenetCEHelper fceh, final Connection con, final Connection dwhCon) {
		return transformToDocumentoWs(docFilenet, withContent, withMetadati, true, null, idAoo, fceh, con, dwhCon);
	}

	/**
	 * @param docFilenet
	 * @param withContent
	 * @param withMetadati
	 * @param metadatiRichiesti
	 * @param idAoo
	 * @param fceh
	 * @param con
	 * @param dwhCon
	 * @return
	 */
	private DocumentoWsDTO transformToDocumentoWs(final Document docFilenet, final boolean withContent, final boolean withMetadati, final Set<String> metadatiRichiesti,
			final Long idAoo, final IFilenetCEHelper fceh, final Connection con, final Connection dwhCon) {
		return transformToDocumentoWs(docFilenet, withContent, withMetadati, true, metadatiRichiesti, idAoo, fceh, con, dwhCon);
	}

	/**
	 * @param docFilenet
	 * @param withContent
	 * @param withMetadati
	 * @param getFascicolo
	 * @param inMetadatiDaRecuperare
	 * @param idAoo
	 * @param fceh
	 * @param con
	 * @param dwhCon
	 * @return
	 */
	private DocumentoWsDTO transformToDocumentoWs(final Document docFilenet, final boolean withContent, final boolean withMetadati, final boolean getFascicolo,
			final Set<String> inMetadatiDaRecuperare, final Long idAoo, final IFilenetCEHelper fceh, final Connection con, final Connection dwhCon) {
		final Map<ContextTrasformerCEEnum, Object> context = new EnumMap<>(ContextTrasformerCEEnum.class);
		context.put(ContextTrasformerCEEnum.RECUPERA_CONTENT, withContent);
		context.put(ContextTrasformerCEEnum.RECUPERA_METADATI, withMetadati);
		if (withMetadati) {
			Set<String> metadatiDaRecuperare = null;
			if (!CollectionUtils.isEmpty(inMetadatiDaRecuperare)) {
				metadatiDaRecuperare = inMetadatiDaRecuperare;
			} else {
				metadatiDaRecuperare = fceh.getClassDefinitionPropertiesNameWithoutSystemProps(docFilenet.getClassName());
			}
			context.put(ContextTrasformerCEEnum.CLASS_DEFINITION_PROPERTIES, metadatiDaRecuperare);
		}

		final DocumentoWsDTO docWs = TrasformCE.transform(docFilenet, TrasformerCEEnum.FROM_DOCUMENT_TO_DOC_WS, context, con);

		if (docWs != null) {
			// Fascicolo procedimentale
			if (getFascicolo) {
				FascicoloDTO fascicoloProcedimentale = null;

				// Se il documento FileNet è la versione corrente, si evita di andare a
				// recuperarlo nuovamente nel CE
				if (Boolean.TRUE.equals(docFilenet.get_IsCurrentVersion())) {
					fascicoloProcedimentale = fascicoloSRV.getFascicoloProcedimentale(docFilenet, idAoo.intValue(), fceh, con);
				} else {
					fascicoloProcedimentale = fascicoloSRV.getFascicoloProcedimentale(docWs.getIdDocumento(), idAoo.intValue(), fceh, con);
				}

				if (fascicoloProcedimentale != null) {
					docWs.setIdFascicolo(fascicoloProcedimentale.getIdFascicolo());
				}
			}

			// Stato
			final String statoDocumento = getStatoDocumento(docWs.getIdDocumento(), docWs.getNumeroProtocollo(), idAoo, dwhCon, con);
			docWs.setStato(statoDocumento);
		}

		return docWs;
	}

	/**
	 * @param docFilenet
	 * @return
	 */
	private DocumentoWsDTO transformToDocumentoWsPerElencoVersioni(final Document docFilenet) {
		final String guid = it.ibm.red.business.utils.StringUtils.cleanGuidToString(docFilenet.get_Id());
		final String documentTitle = (String) TrasformerCE.getMetadato(docFilenet, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
		final Date dataModifica = docFilenet.get_DateCheckedIn(); // cfr. FWS
		final Integer numeroVersione = docFilenet.get_MajorVersionNumber();
		final String classeDocumentale = docFilenet.getClassName();

		return new DocumentoWsDTO(guid, documentTitle, numeroVersione, classeDocumentale, dataModifica);

	}

	/**
	 * @param documenti
	 * @param idAoo
	 * @param con
	 */
	private void calcolaStatoDocumenti(final List<DocumentoWsDTO> documenti, final Long idAoo, final Connection con) {
		Connection dwhCon = null;

		try {
			dwhCon = setupConnection(getFilenetDataSource().getConnection(), false);

			if (!CollectionUtils.isEmpty(documenti)) {
				for (final DocumentoWsDTO doc : documenti) {
					doc.setStato(getStatoDocumento(doc.getIdDocumento(), doc.getNumeroProtocollo(), idAoo, dwhCon, con));
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero dello stato dei documenti: " + e.getMessage(), e);
			throw new RedException("Errore durante il recupero dello stato dei documenti: " + e.getMessage());
		} finally {
			closeConnection(dwhCon);
		}
	}

	/**
	 * @param idDocumento
	 * @param numeroProtocollo
	 * @param idAoo
	 * @param dwhCon
	 * @param con
	 * @return
	 */
	private String getStatoDocumento(final String idDocumento, final Integer numeroProtocollo, final Long idAoo, final Connection dwhCon, final Connection con) {
		String statoDoc = Constants.EMPTY_STRING;

		boolean isDaFirmare = false;
		boolean isFirmato = false;
		boolean isSpedito = false;
		boolean isAnnullato = false;
		boolean isRiattivatoDopoAnnullamento = false;
		boolean isRifiutato = false;

		final List<EventoLogDTO> eventiDoc = eventoLogDAO.getEventiStoricoByIdDocumento(Integer.parseInt(idDocumento), idAoo, dwhCon);

		// Si scorrono tutti gli eventi in ordine cronologico (dal meno recente al più
		// recente)
		for (final EventoLogDTO eventoDoc : eventiDoc) {
			if (EventTypeEnum.ASSEGNAZIONE_FIRMA.getValue().equals(eventoDoc.getEventType())) {
				isDaFirmare = true;
			}

			if (EventTypeEnum.ANNULLATO.getValue().equals(eventoDoc.getEventType())) {
				isAnnullato = true;
				isRiattivatoDopoAnnullamento = false;
			}

			if (isAnnullato && EventTypeEnum.ASSEGNAZIONE_DIRETTA_A_UTENTE.getValue().equals(eventoDoc.getEventType())) {
				isRiattivatoDopoAnnullamento = true;
			}

			if (EventTypeEnum.FIRMATO.getValue().equals(eventoDoc.getEventType()) || EventTypeEnum.FIRMA_AUTOGRAFA.getValue().equals(eventoDoc.getEventType())) {
				isFirmato = true;
			}

			if (EventTypeEnum.SPEDITO.getValue().equals(eventoDoc.getEventType()) || EventTypeEnum.FIRMATO_SPEDITO.getValue().equals(eventoDoc.getEventType())
					|| EventTypeEnum.COMPLETAMENTO_SPED_ELETTRONICA.getValue().equals(eventoDoc.getEventType())) {
				isSpedito = true;
			}

			if (EventTypeEnum.RIFIUTO.getValue().equals(eventoDoc.getEventType())) {
				isRifiutato = true;
			}
		}

		if (!isFirmato) {

			// Da firmare
			if (isDaFirmare) {
				statoDoc = Ricerca.STATO_DOC_DA_FIRMARE;
			}

			// Rifiutato
			if (isRifiutato) {
				statoDoc = Ricerca.STATO_DOC_RIFIUTATO;
			}

		} else if (numeroProtocollo != null && numeroProtocollo > 0) { // Protocollato
			// Protocollato e firmato
			statoDoc = Ricerca.STATO_DOC_PROTOCOLLATO_FIRMATO;
		}

		if (isSpedito) {
			// Spedito
			statoDoc = Ricerca.STATO_DOC_SPEDITO;
			if (gestioneNotificheEmailDAO.getNumErroriSpedizione(idDocumento, con) > 0) { // Spedizione con errori
				// Spedito con errore
				statoDoc = Ricerca.STATO_DOC_SPEDITO_ERRORE;
			}
		}

		if (isAnnullato && !isRiattivatoDopoAnnullamento) {
			// Annullato
			statoDoc = Ricerca.STATO_DOC_ANNULLATO;
		}

		return statoDoc;
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IDocumentoWsFacadeSRV#richiediProtocolloNPSync(it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.RedProtocolloNPSSyncType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_PROTOCOLLO_NPS_SYNC)
	public ProtocolloNpsDTO richiediProtocolloNPSync(final RedWsClient client, final RedProtocolloNPSSyncType request) {
		return documentoSRV.richiediProtocolloNPSync(request.getWobNumber(), request.getDocumentTitle(), request.getIdAoo(), (long) request.getIdUtente(),
				(long) request.getIdUfficio(), (long) request.getIdRuolo(), request.isRiservato());
	}
	
	/**
	 * @see it.ibm.red.business.service.ws.facade.IDocumentoWsFacadeSRV#uploadDocToAsyncNps(it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.UploadDocToAsyncNpsType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_UPLOAD_NPS_ASYNC)
	public int uploadDocToAsyncNps(final RedWsClient client, final UploadDocToAsyncNpsType request) {
		final InputStream content = new ByteArrayInputStream(request.getContent());
		return documentoSRV.uploadDocToAsyncNps(content, request.getDocumentTitle(), request.getProtocolloGuid(), request.getContentType(), request.getNomeFile(),
				request.getOggetto(), request.getNumeroAnnoProtocollo(), request.getIdAOO(), (long) request.getIdUtente(), (long) request.getIdUfficio(),
				(long) request.getIdRuolo());
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IDocumentoWsFacadeSRV#associateDocumentoProtocolloToAsyncNps(it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.AssociateDocumentoProtocolloToAsyncNpsType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_ASS_DOC_PROTOCOLLO_ASYNC)
	public boolean associateDocumentoProtocolloToAsyncNps(final RedWsClient client, final AssociateDocumentoProtocolloToAsyncNpsType request) {
		return documentoSRV.associateDocumentoProtocolloToAsyncNps(request.getDocumentTitle(), request.getProtocolloGuid(), request.getNomeFile(), request.getOggetto(),
				request.getIdAOO(), (long) request.getIdUtente(), (long) request.getIdUfficio(), (long) request.getIdRuolo(), request.isIsPrincipale(), request.isDaInviare(),
				request.getNumeroAnnoProtocollo(), request.getNarUpload(), request.isIsProtocollazioneP7M());
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IDocumentoWsFacadeSRV#aggiungiAllacciAsync(it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.AggiungiAllacciAsyncType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_AGGIUNGI_ALLACCI_ASYNC)
	public boolean aggiungiAllacciAsync(final RedWsClient client, final AggiungiAllacciAsyncType request) {
		return documentoSRV.aggiungiAllacciAsync(request.getIdAOO(), (long) request.getIdUtente(), (long) request.getIdUfficio(), (long) request.getIdRuolo(),
				request.getDocumentTitle(), request.getIdProtocollo(), request.getNumeroAnnoProtocollo(), request.isRisposta());
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IDocumentoWsFacadeSRV#spedisciProtocolloUscitaAsync(it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.SpedisciProtocolloUscitaAsyncType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_SPEDISCI_PROT_USCITA_ASYNC)
	public boolean spedisciProtocolloUscitaAsync(final RedWsClient client, final SpedisciProtocolloUscitaAsyncType request) {
		return documentoSRV.spedisciProtocolloUscitaAsync(request.getProtocolloGuid(), request.getInfoProtocollo(), request.getIdAOO(), (long) request.getIdUtente(),
				(long) request.getIdUfficio(), (long) request.getIdRuolo(), request.getDocumentTitle(), request.getNumeroAnnoProtocollo(), request.getDataSpedizione());
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IDocumentoWsFacadeSRV#gestisciDestinatariInterni(it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.GestisciDestinatariInterniType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_GESTISCI_DESTINATARI_INTERNI)
	public boolean gestisciDestinatariInterni(final RedWsClient client, final GestisciDestinatariInterniType request) {
		return documentoSRV.gestisciDestinatariInterni(request.getWobNumber(), request.getDocumentTitle(), request.getIdAoo(), (long) request.getIdUtente(),
				(long) request.getIdUfficio(), (long) request.getIdRuolo(), request.getNumeroProtocollo(), request.getAnnoProtocollo(), request.isRiservato());
	}

}