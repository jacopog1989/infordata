package it.ibm.red.business.enums;

/**
 * @author SLac
 * 
 * Tipologia del nodo utilizzata nell'organigramma 
 */
public enum TipologiaNodoEnum {

	/**
	 * 
	 */
	UFFICIO(1),
	
	/**
	 * 
	 */
	UFFICIO_VIRTUALE(2),
	
	/**
	 * 
	 */
	GRUPPO(3),
	
	/**
	 * Non ha un id specifico.
	 */
	UTENTE(0);
	
	/**
	 * 
	 */
	private int id;
	
	/**
	 * @param id
	 */
	TipologiaNodoEnum(final int inId) {
		this.id = inId;
	}

	/**
	 * @return
	 */
	public int getId() {
		return id;
	}
	
	/**
	 * @param id
	 * @return
	 */
	public static TipologiaNodoEnum get(final int id) {
		TipologiaNodoEnum t = null;
		
		for (TipologiaNodoEnum tn : TipologiaNodoEnum.values()) {
			if (tn.getId() == id) {
				t = tn;
			}
		}
		
		return t;
	}
	
}
