/**
 * 
 */
package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IAttrExtFacadeSRV;

/**
 * Interface del service gestione attributi estesi.
 */
public interface IAttrExtSRV extends IAttrExtFacadeSRV {

}