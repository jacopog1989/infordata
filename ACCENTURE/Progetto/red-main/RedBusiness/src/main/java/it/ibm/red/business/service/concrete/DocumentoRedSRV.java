package it.ibm.red.business.service.concrete;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.core.Containable;
import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.Document;
import com.filenet.api.property.FilterElement;
import com.filenet.api.property.PropertyFilter;
import com.google.common.net.MediaType;

import filenet.vw.api.VWRosterQuery;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.constants.Constants.Varie;
import it.ibm.red.business.dao.IAllaccioDAO;
import it.ibm.red.business.dao.IFascicoloDAO;
import it.ibm.red.business.dao.IIterApprovativoDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.ITipologiaDocumentoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dao.IWorkflowDAO;
import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.CodaTrasformazioneParametriDTO;
import it.ibm.red.business.dto.ContributoDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DetailFatturaFepaDTO;
import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EventoLogDTO;
import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.MetadatoDinamicoDTO;
import it.ibm.red.business.dto.RegistrazioneAusiliariaDTO;
import it.ibm.red.business.dto.ResponsabileCopiaConformeDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.TemplateMetadatoValueDTO;
import it.ibm.red.business.dto.TemplateValueDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.VersionDTO;
import it.ibm.red.business.dto.filenet.EventLogDTO;
import it.ibm.red.business.dto.filenet.StepDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.DescrizioneTipologiaDocumentoEnum;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.PermessiAOOEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.StepTypeEnum;
import it.ibm.red.business.enums.TipoAllaccioEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.adobe.AdobeLCHelper;
import it.ibm.red.business.helper.adobe.IAdobeLCHelper;
import it.ibm.red.business.helper.adobe.dto.CountSignsOutputDTO;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.persistence.model.Workflow;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IASignSRV;
import it.ibm.red.business.service.IContributoSRV;
import it.ibm.red.business.service.IDocumentoRedSRV;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.IListaDocumentiSRV;
import it.ibm.red.business.service.IMetadatiDinamiciSRV;
import it.ibm.red.business.service.INodoSRV;
import it.ibm.red.business.service.INpsSRV;
import it.ibm.red.business.service.IRegistrazioniAusiliarieSRV;
import it.ibm.red.business.service.IRiattivaProcedimentoSRV;
import it.ibm.red.business.service.ITemplateDocUscitaSRV;
import it.ibm.red.business.service.ITipoFileSRV;
import it.ibm.red.business.service.ITipoProcedimentoSRV;
import it.ibm.red.business.service.ITipologiaDocumentoSRV;
import it.ibm.red.business.service.ITitolarioSRV;
import it.ibm.red.business.service.ITrasformazionePDFSRV;
import it.ibm.red.business.service.facade.IRegistroAusiliarioFacadeSRV;
import it.ibm.red.business.service.facade.IStoricoFacadeSRV;
import it.ibm.red.business.utils.DestinatariRedUtils;
import it.ibm.red.business.utils.DocumentoUtils;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.PermessiUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * Service gestione DocumentoRed.
 */
@Service
public class DocumentoRedSRV extends AbstractService implements IDocumentoRedSRV {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 6124447295274038883L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DocumentoRedSRV.class.getName());

	/**
	 * Dao.
	 */
	@Autowired
	private ITipologiaDocumentoDAO tipologiaDocumentoDAO;

	/**
	 * Servizio gestione contributi.
	 */
	@Autowired
	private IContributoSRV contributoSRV;

	/**
	 * SRV per il caricamento dello storico.
	 */
	@Autowired
	private IStoricoFacadeSRV storicoSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IFascicoloSRV fascicoloSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private ITipoProcedimentoSRV tipoProcedimentoSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IMetadatiDinamiciSRV metadatiDinamiciSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private INpsSRV npsSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private ITrasformazionePDFSRV trasformazionePDFSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private ITipoFileSRV tipoFileSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private ITitolarioSRV titolarioSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IListaDocumentiSRV listaDocumentiSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private ITemplateDocUscitaSRV templateDocUscitaSRV;

	/**
	 * Servizio gestione nodi.
	 */
	@Autowired
	private INodoSRV nodoSRV;

	/**
	 * Dao.
	 */
	@Autowired
	private IWorkflowDAO workflowDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private IIterApprovativoDAO iterApprovativoDAO;

	/**
	 * DAO per la gestione degli utenti.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;
	
	/**
	 * Servizio firma asincrona.
	 */
	@Autowired
	private IASignSRV aSignSRV;

	/**
	 * DAO gestione uffici.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * DAO gestione fascicoli.
	 */
	@Autowired
	private IFascicoloDAO fascicoloDAO;

	/**
	 * Servizio.
	 */
	@Autowired
	private ITipologiaDocumentoSRV tipologiaDocumentoSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IRegistrazioniAusiliarieSRV registrazioneAusiliarieSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IRegistroAusiliarioFacadeSRV registroAusiliarioSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private IAllaccioDAO allaccioDAO;

	/**
	 * Flag ricerca riservato.
	 */
	private boolean isRicercaRiservato;

	/**
	 * Servizio.
	 */
	@Autowired
	private IRiattivaProcedimentoSRV riattivaSRV;

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV#getDocumentDetail(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.String, java.lang.String,
	 *      boolean).
	 */
	@Override
	public DetailDocumentRedDTO getDocumentDetail(final String documentTitle, final UtenteDTO utente, final String wobNumberIN, final String documentClassName,
			final boolean isRicercaRiservato) {
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			return getDocumentDetail(documentTitle, utente, wobNumberIN, documentClassName, isRicercaRiservato, con);
		} catch (final SQLException e) {
			LOGGER.error("Errore nel recupero del dettaglio a causa di un problema nella connessione al DB", e);
			throw new RedException("Errore nel recupero del dettaglio", e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * @see it.ibm.red.business.service.IDocumentoRedSRV#getDocumentDetail(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.String, java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public DetailDocumentRedDTO getDocumentDetail(final String documentTitle, final UtenteDTO utente, final String wobNumberIN, final String documentClassName,
			final Connection con) {
		return getDocumentDetail(documentTitle, utente, wobNumberIN, documentClassName, false, con);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV#getDocumentDetail(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.String, java.lang.String).
	 */
	@Override
	public DetailDocumentRedDTO getDocumentDetail(final String documentTitle, final UtenteDTO utente, final String wobNumberIN, final String documentClassName) {
		return getDocumentDetail(documentTitle, utente, wobNumberIN, documentClassName, false);
	}

	/**
	 * @see it.ibm.red.business.service.IDocumentoRedSRV#getDocumentDetail(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.String, java.lang.String,
	 *      boolean, java.sql.Connection).
	 */
	@Override
	public DetailDocumentRedDTO getDocumentDetail(final String documentTitle, final UtenteDTO utente, final String wobNumberIN, String documentClassName,
			final boolean isRicercaRiservato, final Connection connection) {
		DetailDocumentRedDTO detailDTO = null;

		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fcehAdmin = null;
		IFilenetCEHelper fceh = null;

		Boolean assegnInSpedizione = false;
		Boolean wfAttivi = true;

		try {
			// vado su FileNet e attraverso il documentTitle recupero il documento
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			if (isRicercaRiservato) {
				final FilenetCredentialsDTO fcAdmin = new FilenetCredentialsDTO(utente.getFcDTO());
				fceh = FilenetCEHelperProxy.newInstance(fcAdmin, utente.getIdAoo());
			}

			if (StringUtils.isNullOrEmpty(documentClassName)) {
				LOGGER.info("####INFO#### Inizio calcola document class");
				final Integer idTipologiaDocumento = fceh.getTipoDocumentoByDocumentTitle(documentTitle);
				if (idTipologiaDocumento == null) {
					throw new RedException("Id tipo documento non trovato");
				}
				final TipologiaDocumentoDTO tipoDocumento = tipologiaDocumentoSRV.getById(idTipologiaDocumento, connection);
				documentClassName = tipoDocumento.getDocumentClass();
				LOGGER.info("####INFO#### Fine calcola document class");
			}
			LOGGER.info("####INFO#### Inizio getDocumentRedForDetail");
			final Document docFilenet = fceh.getDocumentRedForDetail(documentClassName, documentTitle);
			LOGGER.info("####INFO#### Fine getDocumentRedForDetail");

			if (docFilenet == null) {
				throw new RedException("Documento non presente");
			}

			fpeh = new FilenetPEHelper(utente.getFcDTO());
			if (isRicercaRiservato) {
				final FilenetCredentialsDTO fpehAdmin = new FilenetCredentialsDTO(utente.getFcDTO());
				fpeh = new FilenetPEHelper(fpehAdmin);
			}

			// Recupero il DTO dal documento filenet
			LOGGER.info("####INFO#### Inizio Trasformazione FROM_DOCUMENTO_TO_DETAIL_RED");
			detailDTO = TrasformCE.transform(docFilenet, TrasformerCEEnum.FROM_DOCUMENTO_TO_DETAIL_RED, connection);
			LOGGER.info("####INFO#### Fine Trasformazione FROM_DOCUMENTO_TO_DETAIL_RED");

			//Gestione ASIGN
			ASignItemDTO aSignItem = aSignSRV.getDetailInfo(detailDTO.getDocumentTitle());
			detailDTO.setaSignItem(aSignItem);
			
			// Recupero Protocollo Riferimento dal DocumentTitle
			final String protRif = detailDTO.getProtocolloRiferimento();
			String protToShow = "";
			if (protRif != null) {
				final Document docRif = fceh.getDocumentByDTandAOO(protRif, utente.getIdAoo());
				final Integer numeroProtocollo = (Integer) TrasformerCE.getMetadato(docRif, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
				final Integer annoProtocollo = (Integer) TrasformerCE.getMetadato(docRif, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
				protToShow = numeroProtocollo.toString() + "/" + annoProtocollo.toString();
				detailDTO.setProtRiferimentoToShow(protToShow);
			}

			final CategoriaDocumentoEnum categoriaDOC = CategoriaDocumentoEnum.getExactly(detailDTO.getIdCategoriaDocumento());
			if (CategoriaDocumentoEnum.CONTRIBUTO_ESTERNO.equals(categoriaDOC) || CategoriaDocumentoEnum.DOCUMENTO_USCITA.equals(categoriaDOC)
					|| CategoriaDocumentoEnum.MOZIONE.equals(categoriaDOC)) {
				LOGGER.info("####INFO#### Inizio get destinatari");
				final List<String> destinatariTo = new ArrayList<>();
				final List<String> destinatariCC = new ArrayList<>();
				final EmailDTO email = getMailByDocumentTitleAndIdAOObyAdmin(documentTitle, utente);
				if (email != null && email.getDestinatari() != null) {
					final String[] emailDestinatariTo = email.getDestinatari().split(";");
					for (final String emailDestTo : emailDestinatariTo) {
						destinatariTo.add(emailDestTo.trim());
					}
					detailDTO.setDestinatariTO(destinatariTo);
				}
				if (email != null && email.getDestinatariCC() != null) {
					final String[] emailDestinatariCC = email.getDestinatariCC().split(";");
					for (final String emailDestCC : emailDestinatariCC) {
						destinatariCC.add(emailDestCC.trim());
					}
					detailDTO.setDestinatariCC(destinatariCC);
				}
				LOGGER.info("####INFO#### Fine get destinatari");
			}

			LOGGER.info("####INFO#### Inizio gestione metadati dinamici");
			// recupero eventuali metadati dinamici START
			final List<MetadatoDinamicoDTO> list = metadatiDinamiciSRV.getMetadatyByClasseDocumentale(utente, documentClassName);

			if (list != null && !list.isEmpty()) {
				detailDTO.setMetadatiDinamici(metadatiDinamiciSRV.settaValoriMetadatiDinamici(docFilenet, list));
			}
			LOGGER.info("####INFO#### Fine gestione metadati dinamici");
			// recupero eventuali metadati dinamici END

			/**
			 * ASSEGNAZIONI START + eventuale recupero del wobNumber
			 ********************************************************/
			final Aoo aoo = detailDTO.getAoo();

			LOGGER.info("####INFO#### Inizio calcolo wf principale");

			/**
			 * CALCOLO DEL WORKFLOW PRINCIPALE - SERVE PER CAPIRE SE L'UTENTE PUO'
			 * MODIFICARE IL DOCUMENTO START
			 *****************************************************************/
			// Il workflow principale potrebbe non essere quello selezionato, se ad esempio
			// vengo da una coda il wobNumber mi viene passato
			// mentre in ricerca il wobNumber passatomi è null
			// Ho bisogno di andare sul roster per avere sia questa informazione che per
			// avere la lista di assegnatari del documento
			final VWRosterQuery query = fpeh.getRosterQueryWorkByIdDocumentoAndCodiceAOO(detailDTO.getDocumentTitle(), utente.getFcDTO().getIdClientAoo());
			detailDTO.setWobNumberPrincipale(getWobNumberWorkflowPrincipale(detailDTO, query, connection));
			query.resetFetch(); // resetto il puntatore per passarlo alla query delle assegnazioni
			if (StringUtils.isNullOrEmpty(wobNumberIN)) {
				// setto il principale come selezionato
				detailDTO.setWobNumberSelected(detailDTO.getWobNumberPrincipale());
			} else {
				detailDTO.setWobNumberSelected(wobNumberIN);
			}
			/**
			 * CALCOLO DEL WORKFLOW PRINCIPALE - SERVE PER CAPIRE SE L'UTENTE PUO'
			 * MODIFICARE IL DOCUMENTO END
			 *****************************************************************/

			LOGGER.info("####INFO#### Fine calcolo wf principale");

			LOGGER.info("####INFO#### Inizio calcolo recupero response raw");
			final String[] responses = fpeh.getResponses(query);
			detailDTO.setResponsesRaw(responses);
			query.resetFetch(); // resetto il puntatore per passarlo alla query delle assegnazioni
			LOGGER.info("####INFO#### Fine calcolo recupero response raw");

			LOGGER.info("####INFO#### Inizio calcolo wf attivi");
			/**
			 * Il documento potrebbe essere agli atti ma avere comunque delle assegnazioni.
			 * La variabile mi serve quindi a capire se esiste un assegnazione principale
			 * anche se non è assegnato all'utente loggato. Se esiste non deve cercare le
			 * assegnazioni all'interno dello storico
			 **/
			boolean esisteAssegnazionePrincipale = false;
			// per ogni assegnazione il metodo del PE gestisce solo gli id dell'ufficio e
			// dell'utente (se c'è)
			final List<AssegnazioneDTO> assegnazioniList = fpeh.getAssegnazioni(detailDTO.getDocumentTitle(), aoo.getCodiceAoo(), query);
			// boolean per wf
			if (assegnazioniList.isEmpty()) {
				wfAttivi = false;
			}
			LOGGER.info("####INFO#### Fine calcolo wf attivi");

			LOGGER.info("####INFO#### Inizio calcolo assegnazioni");
			detailDTO.setAssegnazioni(new ArrayList<>());
			if (assegnazioniList != null && !assegnazioniList.isEmpty()) {
				Utente ute = null;
				UtenteDTO inUtente = null;
				Nodo n = null;
				UfficioDTO inUfficio = null;

				// per ogni assegnazione recupero i dati del nodo ufficio e se ci sono i dati
				// dell'utente
				final Iterator<AssegnazioneDTO> itAssegnazioni = assegnazioniList.iterator();
				while (itAssegnazioni.hasNext()) {
					final AssegnazioneDTO a = itAssegnazioni.next();

					// Documento in uscita con Iter Automatico e allegato per Copia Conforme nella
					// coda "In Sospeso":
					// non si considera l'assegnazione per Copia Conforme (lato workflow, nella
					// D_EVENTI_CUSTOM l'IDTIPOASSEGNAZIONE è 0)
					// del workflow PRINCIPALE con IDNODO e IDUTENTE uguali a 0
					if (TipoAssegnazioneEnum.COPIA_CONFORME.equals(a.getTipoAssegnazione()) && a.getUfficio() != null && a.getUfficio().getId().equals(0L)
							&& a.getUtente() != null && a.getUtente().getId().equals(0L)) {
						itAssegnazioni.remove(); // Si rimuove dalla lista delle assegnazioni
					} else {

						if (a.getTipoAssegnazione().equals(TipoAssegnazioneEnum.SPEDIZIONE)) {
							// Boolean per assegnazione spedizione
							assegnInSpedizione = true;
						}

						n = nodoDAO.getNodo(a.getUfficio().getId(), connection);
						if (n != null) { // l'ufficio e' importante che ci sia

							inUfficio = new UfficioDTO(n.getIdNodo(), n.getDescrizione());

							if (a.getUtente().getId() != null && a.getUtente().getId() > 0) {
								ute = utenteDAO.getUtente(a.getUtente().getId(), connection);
								inUtente = new UtenteDTO(ute);
								inUtente.setIdUfficio(n.getIdNodo());
								inUtente.setUfficioDesc(n.getDescrizione());
								a.setDescrizioneAssegnatario(inUfficio.getDescrizione() + " - " + inUtente.getNome() + " " + inUtente.getCognome());
							} else {
								inUtente = null;
								a.setDescrizioneAssegnatario(inUfficio.getDescrizione());
							}

							a.setUfficio(inUfficio);
							a.setUtente(inUtente);

							if (StringUtils.isNullOrEmpty(detailDTO.getIterApprovativoSemaforo()) && a.getWobNumber().equals(detailDTO.getWobNumberPrincipale())) {
								detailDTO.setIterApprovativoSemaforo("Assegnazione per " + a.getTipoAssegnazione().getDescrizione());
							}

							if (!esisteAssegnazionePrincipale) {
								esisteAssegnazionePrincipale = (TipoAssegnazioneEnum.COMPETENZA.equals(a.getTipoAssegnazione())
										|| TipoAssegnazioneEnum.FIRMA.equals(a.getTipoAssegnazione()) || TipoAssegnazioneEnum.SIGLA.equals(a.getTipoAssegnazione())
										|| TipoAssegnazioneEnum.VISTO.equals(a.getTipoAssegnazione())
										|| TipoAssegnazioneEnum.RIFIUTO_ASSEGNAZIONE.equals(a.getTipoAssegnazione())
										|| TipoAssegnazioneEnum.RIFIUTO_FIRMA.equals(a.getTipoAssegnazione())
										|| TipoAssegnazioneEnum.RIFIUTO_SIGLA.equals(a.getTipoAssegnazione())
										|| TipoAssegnazioneEnum.RIFIUTO_VISTO.equals(a.getTipoAssegnazione())
										|| TipoAssegnazioneEnum.SPEDIZIONE.equals(a.getTipoAssegnazione())
										|| TipoAssegnazioneEnum.COPIA_CONFORME.equals(a.getTipoAssegnazione())
										|| TipoAssegnazioneEnum.FIRMA_MULTIPLA.equals(a.getTipoAssegnazione())
										|| TipoAssegnazioneEnum.RIFIUTO_FIRMA_MULTIPLA.equals(a.getTipoAssegnazione()));

								if (esisteAssegnazionePrincipale) {
									detailDTO.setRiservatoAbilitato(a.getUfficio() != null && utente.getIdUfficio().equals(a.getUfficio().getId()) && a.getUtente() != null
											&& utente.getId().equals(a.getUtente().getId()));
								}

							}
						}
					}
				}

				Collections.sort(assegnazioniList);
				detailDTO.setAssegnazioni(assegnazioniList);
			}

			LOGGER.info("####INFO#### Fine calcolo assegnazioni");

			detailDTO.setVisualizzaAnteprimBtn(wfAttivi && !assegnInSpedizione);
			/** ASSEGNAZIONI END **********************************************************/

			/**
			 * FASCICOLI E FALDONI START
			 ***************************************************************************/
			LOGGER.info("####INFO#### Inizio calcolo fascicoli e faldoni");
			final List<Document> fascicoliFilenet = fceh.getFascicoliByFoldersFiledIn(docFilenet.get_FoldersFiledIn(), aoo.getIdAoo().intValue());

			retrieveFascicoliAndSetProcedimentale(documentTitle, utente, detailDTO, fceh, connection, fascicoliFilenet);
			LOGGER.info("####INFO#### Fine calcolo fascicoli e faldoni");
			/**
			 * FASCICOLI E FALDONI END
			 *****************************************************************************/

			/**
			 * ALLEGATI START
			 ***************************************************************************/
			LOGGER.info("####INFO#### Inizio calcolo allegati");
			final DocumentSet dSet = fceh.getAllegati(detailDTO.getGuid(), false, false);
			List<AllegatoDTO> allegatiDTO = null;
			List<AllegatoDTO> allegatiNonSbustati = null;
			if (!dSet.isEmpty()) {
				allegatiDTO = new ArrayList<>();
				allegatiNonSbustati = new ArrayList<>();
				final Iterator<?> it = dSet.iterator();
				Document alleDocument = null;

				float sizeTotaleAllegatiInMB = 0L;

				final Map<ContextTrasformerCEEnum, Object> context = tipologiaDocumentoSRV.getContextTipologieDocumento(detailDTO.getIdCategoriaDocumento(), utente,
						connection);
				while (it.hasNext()) {
					alleDocument = (Document) it.next();

					final AllegatoDTO allegato = TrasformCE.transform(alleDocument, TrasformerCEEnum.FROM_DOCUMENT_TO_ALLEGATO_CONTEXT, context, connection);
					sizeTotaleAllegatiInMB = sizeTotaleAllegatiInMB + allegato.getContentSize();

					if (allegato.getFileNonSbustato()) {
						allegatiNonSbustati.add(allegato);
					} else {
						allegatiDTO.add(allegato);
					}

				}
				detailDTO.setAllegatiNonSbustati(allegatiNonSbustati);
				detailDTO.setAllegati(allegatiDTO);
				detailDTO.setSizeContent(detailDTO.getSizeContent() + sizeTotaleAllegatiInMB);
			}
			LOGGER.info("####INFO#### Fine calcolo allegati");
			/**
			 * ALLEGATI END
			 *****************************************************************************/

			/**
			 * CONTRIBUTI START
			 ***************************************************************************/
			LOGGER.info("####INFO#### Inizio calcolo contributi");
			final List<ContributoDTO> contributi = contributoSRV.getContributi(fceh, detailDTO.getDocumentTitle(), utente.getIdAoo());
			detailDTO.setContributi(contributi);
			LOGGER.info("####INFO#### Fine calcolo contributi");
			/**
			 * CONTRIBUTI END
			 *****************************************************************************/

			/**
			 * ALLACCI START
			 ***************************************************************************/
			LOGGER.info("####INFO#### Inizio calcolo allacci");
			RispostaAllaccioDTO allaccioPrincipale = null;
			if (detailDTO.getAllacci() != null && !detailDTO.getAllacci().isEmpty()) {
				DetailDocumentRedDTO detailAllaccioDTO = null;
				allaccioPrincipale = allaccioDAO.getAllaccioPrincipale(Integer.parseInt(documentTitle), connection);
				for (final RispostaAllaccioDTO r : detailDTO.getAllacci()) {
					// bisogna fare un metodo di filenet che prenda la versione light di un
					// documento
					// numeroDocumento, annoDocumento, numeroProtocollo, annoProtocollo
					fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()), utente.getIdAoo());
					final Document allaccioFilenet = fcehAdmin.getDocumentRedForDetailLIGHT(
							PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), r.getIdDocumentoAllacciato());

					detailAllaccioDTO = TrasformCE.transform(allaccioFilenet, TrasformerCEEnum.FROM_DOCUMENTO_TO_LIGHT_DETAIL_RED);
					if (detailAllaccioDTO != null) {
						r.setDocument(detailAllaccioDTO);
					}
				}
			}
			LOGGER.info("####INFO#### Fine calcolo allacci");
			/**
			 * ALLACCI END
			 *****************************************************************************/

			/**
			 * STORICO - APPROVAZIONI START
			 ***************************************************************************/
			LOGGER.info("####INFO#### Inizio calcolo storico");

			final List<StepDTO> steps = storicoSRV.getModelloStoricoVisuale(utente.getFcDTO(), detailDTO.getWobNumberSelected(), detailDTO.getDocumentTitle(),
					utente.getIdAoo(), detailDTO.getDateCreated(), isRicercaRiservato);

			if (steps != null) {
				final CategoriaDocumentoEnum categoria = CategoriaDocumentoEnum.getExactly(detailDTO.getIdCategoriaDocumento());

				if (checkWorkflowPrincipaleProcedimentoConcluso(steps, categoria, detailDTO.getDestinatari())) {
					final StepDTO stepProcedimentoConcluso = new StepDTO(StepTypeEnum.PROCEDIMENTO_CONCLUSO, new EventLogDTO("PROCEDIMENTO CONCLUSO"));
					steps.add(stepProcedimentoConcluso);
				} else {
					StepDTO stepCorrente = null;
					int index = 0;
					for (int i = steps.size() - 1; i >= 0; i--) {
						stepCorrente = steps.get(i);
						// Si individua lo step corrente, andando a ritroso dal più recente
						// ed escludendo gli step del workflow principale che iniziano flussi paralleli.
						// N.B. Gli eventuali step aggiuntivi inseriti nella timeline per gli iter
						// automatici sono già esclusi in quanto aggiunti nella lista degli step
						// successivamente alla chiamata a questo metodo.
						if (!StringUtils.isNullOrEmpty(stepCorrente.getHeader().getDataAssegnazione())
								&& !DocumentoUtils.isEventStartFlussoParalleloTimeline(stepCorrente.getHeader().getEventType())) {
							index = i;
							break;
						}
					}

					if (stepCorrente != null) {
						final StepDTO stepInPausa = new StepDTO(StepTypeEnum.IN_PAUSA, stepCorrente.getHeader(), stepCorrente.getLabel(), stepCorrente.getBody());
						steps.set(index, stepInPausa);
					}
				}

				detailDTO.setStoricoSteps(steps);
			}

			LOGGER.info("####INFO#### Fine calcolo storico");

			LOGGER.info("####INFO#### Inizio calcolo assegnazioni da storico");

			/**
			 * Se non esiste nessuna assegnazione significa che il documento non ha nessun
			 * workflow attivo e di conseguenza nessun assegnatario per competenza (oppure
			 * ha dei wkf attivi ma nessuno di questi è un assegnazione principale) . Si
			 * deve quindi prendere l'assegnatario andando sulla D_EVENTI_CUSTOM, prendo
			 * quindi la lista recuperata per visualizzare lo storico. L'assegnatario sarà
			 * il destinatario dell'evento estrapolato con le seguenti regole: Sse esiste un
			 * solo evento ed è di migrazione va preso quel destinatario; altrimenti se è un
			 * doc in entrata -> prendo l'ultimo evento di tipo ASSEGNAZIONE_COMPETENZA o
			 * RIASSEGNAZIONE o TRASFERIMENTO_COMPETENZA o MIGRAZIONE; se è un doc in uscita
			 * -> se l'iter è manuale prendo l'ultimo evento possibile diverso dall'evento
			 * creazione documento; se l'iter è automatico prendo l'ultimo evento
			 * ASSEGNAZIONE_COMPETENZA o ASSEGNAZIONE_FIRMA o ANNULLATO
			 **/
			if ((steps != null && !steps.isEmpty()) && !esisteAssegnazionePrincipale) {
				detailDTO.setAssegnazioni(new ArrayList<>());
				AssegnazioneDTO a = null;
				EventLogDTO e = null;
				if (steps.size() == 1 && (steps.get(0).getHeader().getEventType().equalsIgnoreCase(EventTypeEnum.MIGRAZIONE_PMEF.getValue())
						|| steps.get(0).getHeader().getEventType().equalsIgnoreCase(EventTypeEnum.MIGRAZIONE_PRGS.getValue()))) {
					a = eventLogDTO2AssegnazioneDTO(steps.get(0).getHeader(), connection);

				} else {
					if (CategoriaDocumentoEnum.get(detailDTO.getIdCategoriaDocumento()) == CategoriaDocumentoEnum.ENTRATA) {

						for (int i = steps.size() - 1; i >= 0; i--) {
							e = steps.get(i).getHeader();
							if (e.getEventType().equalsIgnoreCase(EventTypeEnum.ASSEGNAZIONE_COMPETENZA.getValue())
									|| e.getEventType().equalsIgnoreCase(EventTypeEnum.RIASSEGNAZIONE.getValue())
									|| e.getEventType().equalsIgnoreCase(EventTypeEnum.TRASFERIMENTO_COMPETENZA.getValue())
									|| e.getEventType().equalsIgnoreCase(EventTypeEnum.MIGRAZIONE_PMEF.getValue())
									|| e.getEventType().equalsIgnoreCase(EventTypeEnum.MIGRAZIONE_PRGS.getValue())) {
								a = eventLogDTO2AssegnazioneDTO(steps.get(i).getHeader(), connection);
								break;
							}
						}
					} else {
						Boolean isManuale = null;
						for (int i = 0; i < steps.size(); i++) {
							e = steps.get(i).getHeader();

							if (isManuale == null || (a != null && a.getWobNumber() != null && !StringUtils.isNullOrEmpty(e.getWorkFlowNumber())
									&& !a.getWobNumber().equals(e.getWorkFlowNumber()))) {
								isManuale = (e != null && e.getIdTipoassegnazione() != null && e.getIdTipoassegnazione().intValue() != TipoAssegnazioneEnum.STORICO.getId());
								a = null;
							}

							if (!isManuale) {
								if (e.getEventType().equalsIgnoreCase(EventTypeEnum.ASSEGNAZIONE_COMPETENZA.getValue())
										|| e.getEventType().equalsIgnoreCase(EventTypeEnum.ASSEGNAZIONE_FIRMA.getValue())
										|| e.getEventType().equalsIgnoreCase(EventTypeEnum.ANNULLATO.getValue())) {
									a = eventLogDTO2AssegnazioneDTO(steps.get(i).getHeader(), connection);
								}
							} else if (a == null && e.getEventType() != null && !e.getEventType().equalsIgnoreCase(EventTypeEnum.DOCUMENTO.getValue())) {
								a = eventLogDTO2AssegnazioneDTO(steps.get(i).getHeader(), connection);
							}
						}
					}
				}

				if (a != null) {
					detailDTO.getAssegnazioni().add(a);
				}
			}

			LOGGER.info("####INFO#### Fine calcolo assegnazioni da storico");
			/**
			 * STORICO - APPROVAZIONI END
			 *****************************************************************************/

			/**
			 * GESTIONE VERSIONI DEL DOCUMENTO PRINCIPALE E DEI SUOI ALLEGATI START
			 ********************************************/
			LOGGER.info("####INFO#### Inizio gestione versioni documento");
			// Si recupera la lista delle versioni del documento e dei suoi eventuali
			// allegati
			detailDTO.setVersioni(getVersioniDocumento(detailDTO.getGuid(), detailDTO.getIdAOO(), utente, isRicercaRiservato));

			if (detailDTO.getAllegati() != null && !detailDTO.getAllegati().isEmpty()) {
				for (final AllegatoDTO allegatoDTO : detailDTO.getAllegati()) {
					allegatoDTO.setVersions(
							getVersioniDocumentoAllegato(allegatoDTO.getGuid(), detailDTO.getIdAOO(), detailDTO.getIdCategoriaDocumento(), utente, isRicercaRiservato, detailDTO.getCodiceFlusso()));
				}
			}

			if (detailDTO.getAllegatiNonSbustati() != null && !detailDTO.getAllegatiNonSbustati().isEmpty()) {
				for (final AllegatoDTO allegatoDTO : detailDTO.getAllegatiNonSbustati()) {
					allegatoDTO.setVersions(
							getVersioniDocumentoAllegato(allegatoDTO.getGuid(), detailDTO.getIdAOO(), detailDTO.getIdCategoriaDocumento(), utente, isRicercaRiservato, detailDTO.getCodiceFlusso()));
				}
			}
			LOGGER.info("####INFO#### Fine gestione versioni documento");
			/**
			 * GESTIONE VERSIONI DEL DOCUMENTO PRINCIPALE E DEI SUOI ALLEGATI END
			 **********************************************/

			/** TEMPLATE DOC USCITA -> START **********************/
			LOGGER.info("####INFO#### Inizio gestione template");
			if (PermessiUtils.hasPermesso(utente.getPermessiAOO(), PermessiAOOEnum.TEMPLATE_DOC_USCITA)) {
				final Map<TemplateValueDTO, List<TemplateMetadatoValueDTO>> templateDocUscitaMappa = templateDocUscitaSRV.getValues(utente, documentTitle);

				if (MapUtils.isNotEmpty(templateDocUscitaMappa)) {
					detailDTO.setTemplateDocUscitaMap(templateDocUscitaMappa);
				}
			}
			LOGGER.info("####INFO#### Fine gestione template");
			/** TEMPLATE DOC USCITA -> END ************************/

			/** REGISTRO AUSILIARIO -> START **************************/
			final RegistrazioneAusiliariaDTO registroAusiliarioDocInfo = registrazioneAusiliarieSRV.getRegistrazioneAusiliaria(documentTitle, utente.getIdAoo().intValue(),
					connection);
			if (registroAusiliarioDocInfo != null) {
				detailDTO.setIdRegistroAusiliario(registroAusiliarioDocInfo.getRegistroAusiliario().getId());
				// configura metadati registro (obbligatorietà, visibilità, etc...)
				registroAusiliarioSRV.configureMetadatiRegistro(registroAusiliarioDocInfo.getRegistroAusiliario().getId(), utente,
						allaccioPrincipale.getIdDocumentoAllacciato(), registroAusiliarioDocInfo.getMetadatiRegistroAusiliario());
				detailDTO.setMetadatiRegistroAusiliario(registroAusiliarioDocInfo.getMetadatiRegistroAusiliario());
			}
			/** REGISTRO AUSILIARIO -> END ****************************/

		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero del dettaglio: " + e.getMessage(), e);
			detailDTO = null;
			throw new RedException("Errore nel recupero del dettaglio", e);
		} finally {
			logoff(fpeh);
			popSubject(fceh);
			popSubject(fcehAdmin);
		}

		return detailDTO;
	}

	private void retrieveFascicoliAndSetProcedimentale(final String documentTitle, final UtenteDTO utente, final DetailDocumentRedDTO detailDTO, final IFilenetCEHelper fceh,
			final Connection connection, final List<Document> fascicoliFilenet) {
		List<FascicoloDTO> fascicoliDTO;
		if (fascicoliFilenet != null && !fascicoliFilenet.isEmpty()) {

			fascicoliDTO = new ArrayList<>();
			FascicoloDTO fascicoloDTO = null;
			// setto la descrizione dell'indice di classificazione per il titolario del
			// fascicolo
			for (final Document docFascicolo : fascicoliFilenet) {
				fascicoloDTO = TrasformCE.transform(docFascicolo, TrasformerCEEnum.FROM_DOCUMENTO_TO_FASCICOLO, connection);

				/**
				 * Recupero i faldoni start
				 ****************************************************/
				if (!docFascicolo.get_FoldersFiledIn().isEmpty()) {
					// prendo i faldoni
					final Iterator<?> itFaldoni = docFascicolo.get_FoldersFiledIn().iterator();
					Document faldoneFilenet = null;
					String guidFaldone = null;
					FaldoneDTO faldoneDTO = null;
					Collection<FaldoneDTO> faldoni = null;

					while (itFaldoni.hasNext()) {
						guidFaldone = StringUtils.cleanGuidToString(((Containable) itFaldoni.next()).get_Id());
						faldoneFilenet = fceh.getFaldoneByFascicoloGuid(guidFaldone, utente.getIdAoo().intValue());
						if (faldoneFilenet != null) {
							if (faldoni == null) {
								faldoni = new ArrayList<>();
							}
							faldoneDTO = TrasformCE.transform(faldoneFilenet, TrasformerCEEnum.FROM_DOCUMENT_TO_FALDONE);
							if (faldoneDTO != null) {
								if (!StringUtils.isNullOrEmpty(faldoneDTO.getParentFaldone())) {
									final Document padreD = fceh.getFaldoneByDocumentTitle(faldoneDTO.getParentFaldone(), faldoneDTO.getIdAOO());
									if (padreD != null) {
										final String nomePadre = (String) TrasformerCE.getMetadato(padreD, PropertiesNameEnum.METADATO_FALDONE_NOMEFALDONE);
										faldoneDTO.setNomePadre(nomePadre);
										final String oggettoPadre = (String) TrasformerCE.getMetadato(padreD, PropertiesNameEnum.METADATO_FALDONE_OGGETTO);
										faldoneDTO.setOggettoPadre(oggettoPadre);
									}
								}

								faldoni.add(faldoneDTO);
							}
						}
					}
					fascicoloDTO.setFaldoni(faldoni);
				}
				/**
				 * Recupero i faldoni end
				 ******************************************************/
				fascicoliDTO.add(fascicoloDTO);
			}

			detailDTO.setFascicoli(fascicoliDTO);

			/**
			 * FASCICOLO PROCEDIMENTALE START
			 *****************************************************************************************/
			if (!fascicoliDTO.isEmpty()) {
				FascicoloDTO procedimentale;
				if (DescrizioneTipologiaDocumentoEnum.DICHIARAZIONE_SERVIZI_RESI.getDescrizione().equalsIgnoreCase(detailDTO.getDescTipologiaDocumento())) {
					procedimentale = fascicoloSRV.getFascicoloProcedimentaleDSR(fascicoliDTO);
				} else {
					procedimentale = fascicoloSRV.getFascicoloProcedimentale(documentTitle, utente.getIdAoo().intValue(), fascicoliDTO, connection);
				}
				setProcedimentale(detailDTO, procedimentale);
				// Mostro in testa sempre il fascicolo procedimentale
				if (fascicoliDTO != null && !fascicoliDTO.isEmpty()) {
					int position = 0;
					for (final FascicoloDTO fascicolo : fascicoliDTO) {
						if (fascicolo.getIdFascicolo().equals(procedimentale.getIdFascicolo())) {
							position = fascicoliDTO.indexOf(fascicolo);
						}
					}
					fascicoliDTO.remove(position);
					fascicoliDTO.add(0, procedimentale);
				}
			}
			/**
			 * FASCICOLO PROCEDIMENTALE END
			 *******************************************************************************************/
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV#setProcedimentale(it.ibm.red.business.dto.DetailDocumentRedDTO,
	 *      it.ibm.red.business.dto.FascicoloDTO).
	 */
	@Override
	public void setProcedimentale(final DetailDocumentRedDTO detailDTO, final FascicoloDTO procedimentale) {
		if (procedimentale != null) {
			List<FaldoneDTO> faldoniDelProcedimentale;
			detailDTO.setDescrizioneFascicoloProcedimentale(procedimentale.getDescrizione());
			detailDTO.setIdFascicoloProcedimentale(procedimentale.getIdFascicolo());
			detailDTO.setDescrizioneTitolarioFascicoloProcedimentale(procedimentale.getDescrizioneTitolario());

			final String prefixProcedimentale = StringUtils.getPrefixFascicoloProcedimentale(procedimentale.getOggetto());

			detailDTO.setPrefixFascicoloProcedimentale(prefixProcedimentale);
			detailDTO.setIndiceClassificazioneFascicoloProcedimentale(procedimentale.getIndiceClassificazione());
			faldoniDelProcedimentale = (List<FaldoneDTO>) procedimentale.getFaldoni();
			// setto nel dettaglio i faldoni del procedimentale (quelli del doc, nel tab
			// faldoni)
			detailDTO.setFaldoni(faldoniDelProcedimentale);
		}
	}

	/**
	 * A partire da un dettaglio di un documento recupera i suoi fascicoli e ne
	 * imposta il fascicolo procedimentatle.
	 * 
	 * In questo modo il dettaglio risulta nuovamente aggiornato
	 * 
	 * @param detailDocument
	 *            dettaglio del documento che si desidera aggiornare
	 * @param utente
	 *            utente che ha eseguito l'operazione
	 */
	@Override
	public void retrieveFascicoliAndSetProcedimentale(final DetailDocumentRedDTO detailDocument, final UtenteDTO utente) {
		IFilenetCEHelper fceh = null;
		Connection connection = null;
		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			connection = setupConnection(getDataSource().getConnection(), false);

			final Document docFilenet = fceh.getDocumentRedForDetail(detailDocument.getDocumentClass(), detailDocument.getDocumentTitle());
			final List<Document> fascicoliFilenet = fceh.getFascicoliByFoldersFiledIn(docFilenet.get_FoldersFiledIn(), utente.getIdAoo().intValue());

			retrieveFascicoliAndSetProcedimentale(detailDocument.getDocumentTitle(), utente, detailDocument, fceh, connection, fascicoliFilenet);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dei fascicoli e del fascicolo procedimentale del dettaglio: " + e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
			popSubject(fceh);
		}
	}

	/**
	 * Utilizzato n.
	 * 
	 * @param e
	 * @param connection
	 * @return
	 */
	private AssegnazioneDTO eventLogDTO2AssegnazioneDTO(final EventLogDTO e, final Connection connection) {
		AssegnazioneDTO a = null;
		try {
			UfficioDTO inUfficio = null;
			boolean isDestinatario = true;
			if (e.getIdNodoDestinatario() != null && e.getIdNodoDestinatario() > 0) {
				inUfficio = new UfficioDTO(e.getIdNodoDestinatario(), e.getNodoDestinatario());
			} else {
				// ci deve essere per forza il mittente
				inUfficio = new UfficioDTO(e.getIdNodoMittente(), e.getNodoMittente());
				isDestinatario = false;
			}
			
			final TipoAssegnazioneEnum t = (e.getIdTipoassegnazione() != null) ? TipoAssegnazioneEnum.get(e.getIdTipoassegnazione().intValue()) : null;
			final Date dateAssegnazione = getDataAssegnazione(e);

			a = new AssegnazioneDTO(null, // motivo assegnazione
					t, e.getDataAssegnazione(), dateAssegnazione, inUfficio.getDescrizione(), // descrizione assegnatario
					null, // utente
					inUfficio);

			UtenteDTO inUtente = null;
			if (isDestinatario && e.getIdUtenteDestinatario() != null && e.getIdUtenteDestinatario() > 0) {
				inUtente = new UtenteDTO(utenteDAO.getUtente(e.getIdUtenteDestinatario(), connection));
				inUtente.setIdUfficio(inUfficio.getId());
				inUtente.setUfficioDesc(inUfficio.getDescrizione());
				a.setDescrizioneAssegnatario(inUfficio.getDescrizione() + " - " + inUtente.getNome() + " " + inUtente.getCognome());
			} else if (!isDestinatario && e.getIdUtenteMittente() != null && e.getIdUtenteMittente() > 0) {
				inUtente = new UtenteDTO(utenteDAO.getUtente(e.getIdUtenteMittente(), connection));
				inUtente.setIdUfficio(inUfficio.getId());
				inUtente.setUfficioDesc(inUfficio.getDescrizione());
				a.setDescrizioneAssegnatario(inUfficio.getDescrizione() + " - " + inUtente.getNome() + " " + inUtente.getCognome());
			}

			a.setUtente(inUtente);
			a.setWobNumber(e.getWorkFlowNumber());

		} catch (final Exception e2) {
			LOGGER.error(e2.getMessage(), e2);
			throw new RedException();
		}

		return a;
	}

	/**
	 * Effettua il parsing della data assegnazione dell'evento e ne imposta il
	 * formato.
	 * 
	 * @param e
	 * @return data assegnazione
	 * @throws ParseException
	 */
	private static Date getDataAssegnazione(final EventLogDTO e) throws ParseException {
		Date dateAssegnazione;
		final String timpestampAsString = e.getDataAssegnazione();
		final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd-HH.mm.ss.");
		final SimpleDateFormat formatFepa = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
		try {
			dateAssegnazione = format.parse(timpestampAsString);
		} catch (final ParseException notAgoodParser) {
			dateAssegnazione = formatFepa.parse(timpestampAsString);
		}
		return dateAssegnazione;
	}

	/**
	 * Metodo per la conversione da EventoLogDTO a AssegnazioneDTO.
	 * 
	 * @param e
	 * @param connection
	 * @return AssegnazioneDTO
	 */
	private AssegnazioneDTO eventoLogDTO2AssegnazioneDTO(final EventoLogDTO e, final Connection connection) {
		AssegnazioneDTO a = null;

		try {
			UfficioDTO inUfficio = null;
			boolean isDestinatario = true;
			if (e.getIdUfficioDestinatario() != null && e.getIdUfficioDestinatario() > 0) {
				inUfficio = new UfficioDTO(e.getIdUfficioDestinatario(), nodoSRV.getNodo(e.getIdUfficioDestinatario()).getDescrizione());
			} else {
				// ci deve essere per forza il mittente
				inUfficio = new UfficioDTO(e.getIdUfficioMittente(), nodoSRV.getNodo(e.getIdUfficioMittente()).getDescrizione());
				isDestinatario = false;
			}
			final TipoAssegnazioneEnum t = TipoAssegnazioneEnum.get(e.getIdTipoAssegnazione());
			final Date dateAssegnazione = e.getDataAssegnazione();

			a = new AssegnazioneDTO(null, // motivo assegnazione
					t, e.getDataAssegnazione().toString(), dateAssegnazione, inUfficio.getDescrizione(), // descrizione assegnatario
					null, // utente
					inUfficio);

			UtenteDTO inUtente = null;
			if (isDestinatario && e.getIdUtenteDestinatario() != null && e.getIdUtenteDestinatario() > 0) {
				inUtente = new UtenteDTO(utenteDAO.getUtente(e.getIdUtenteDestinatario(), connection));
				inUtente.setIdUfficio(inUfficio.getId());
				inUtente.setUfficioDesc(inUfficio.getDescrizione());
				a.setDescrizioneAssegnatario(inUfficio.getDescrizione() + " - " + inUtente.getNome() + " " + inUtente.getCognome());
			} else if (!isDestinatario && e.getIdUtenteMittente() != null && e.getIdUtenteMittente() > 0) {
				inUtente = new UtenteDTO(utenteDAO.getUtente(e.getIdUtenteMittente(), connection));
				inUtente.setIdUfficio(inUfficio.getId());
				inUtente.setUfficioDesc(inUfficio.getDescrizione());
				a.setDescrizioneAssegnatario(inUfficio.getDescrizione() + " - " + inUtente.getNome() + " " + inUtente.getCognome());
			}

			a.setUtente(inUtente);
			a.setWobNumber(e.getWorkFlowNumber());

		} catch (final Exception e2) {
			LOGGER.error(e2.getMessage(), e2);
			throw new RedException();
		}

		return a;
	}

	/**
	 * Converte da EventoLogDTO a AssegnazioneDTO.
	 * 
	 * @param event
	 * @return AssegnazioneDTO
	 */
	@Override
	public AssegnazioneDTO eventoLogDTO2AssegnazioneDTO(final EventoLogDTO event) {
		Connection connection = null;
		try {
			connection = setupConnection(getFilenetDataSource().getConnection(), false);
			return eventoLogDTO2AssegnazioneDTO(event, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di conversione da EventoLogDTO a AssegnazioneDTO", e);
			throw new RedException("Errore in fase di conversione da EventoLogDTO a AssegnazioneDTO", e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Verifica se il workflow principale del documento ha raggiunto uno step tale
	 * da poter ritenere il procedimento concluso.
	 * 
	 * @return true se il procedimento è concluso, false altrimenti
	 *
	 * @param listaStepDocumento
	 * @param categoria
	 * @param destinatari
	 * @return
	 */
	private boolean checkWorkflowPrincipaleProcedimentoConcluso(final List<StepDTO> listaStepDocumento, final CategoriaDocumentoEnum categoria,
			final List<DestinatarioRedDTO> destinatari) {
		if (!CollectionUtils.isEmpty(listaStepDocumento)) {
			// EventType dell'ultimo evento (più recente)
			final String eventType = listaStepDocumento.get(listaStepDocumento.size() - 1).getHeader().getEventType();
			// Documento in ingresso
			if (categoria == CategoriaDocumentoEnum.DOCUMENTO_ENTRATA || categoria == CategoriaDocumentoEnum.ASSEGNAZIONE_INTERNA
					|| categoria == CategoriaDocumentoEnum.INTERNO) {
				// -> gli eventi che concludono il procedimento sono:
				// - MESSA AGLI ATTI
				// - PREPARAZIONE DELLA RISPOSTA
				// - PREDISPOSIZIONE RISPOSTA
				// - PREDISPOSIZIONE INOLTRO
				// - PREDISPOSIZIONE RESTITUZIONE
				if (EventTypeEnum.ATTI.getValue().equals(eventType) || EventTypeEnum.RISPOSTA.getValue().equals(eventType)
						|| EventTypeEnum.PREDISPOSIZIONE_RISPOSTA.getValue().equals(eventType) || EventTypeEnum.PREDISPOSIZIONE_INOLTRO.getValue().equals(eventType)
						|| EventTypeEnum.PREDISPOSIZIONE_RESTITUZIONE.getValue().equals(eventType) || EventTypeEnum.ANNULLATO.getValue().equals(eventType)) {
					return true;
				}
			} else { // Documento in uscita
				final boolean checkAllMezzoSpedizioneDestinatariElettronico = DestinatariRedUtils.checkAllMezzoSpedizioneDestinatariElettronico(destinatari);
				final boolean checkDestinatariSoloInterni = DestinatariRedUtils.checkDestinatariSoloInterni(destinatari);
				final int countDestinatariCartacei = DestinatariRedUtils.countDestinatariCartacei(destinatari);
				final int countDestinatariElettronici = DestinatariRedUtils.countDestinatariElettronici(destinatari);
				final int countDestinatariInterni = DestinatariRedUtils.countDestinatariInterni(destinatari);

				if (// Destinatari solo elettronici o elettronici e interni
					// -> l'evento che conclude il procedimento è "COMPLETAMENTO SPEDIZIONE
					// ELETTRONICA"
				(checkAllMezzoSpedizioneDestinatariElettronico && EventTypeEnum.COMPLETAMENTO_SPED_ELETTRONICA.getValue().equals(eventType))
						// Destinatari solo cartacei o cartacei e interni
						// -> l'evento che conclude il procedimento è "SPEDITO"
						|| (countDestinatariCartacei > 0 && countDestinatariElettronici == 0 && EventTypeEnum.SPEDITO.getValue().equals(eventType))
						// Destinatari solo interni -> gli eventi che concludono il procedimento sono
						// "FIRMATO" o "FIRMA AUTOGRAFA"
						|| (checkDestinatariSoloInterni && (EventTypeEnum.FIRMATO.getValue().equals(eventType) || EventTypeEnum.FIRMA_AUTOGRAFA.getValue().equals(eventType)))
						// Gli eventi che concludono il procedimento in seguito ad un annullamento sono:
						// "ANNULLATO", "VISTO_ANNULLATO", "OSSERVAZIONE_ANNULLATA",
						// "RICHIESTA_INTEGRAZIONE_ANNULLATA", "RELAZIONE_POSITIVA_ANNULLATA", "RELAZIONE_NEGATIVA_ANNULLATA"
						|| EventTypeEnum.ANNULLATO.getValue().equals(eventType) || EventTypeEnum.VISTO_ANNULLATO.getValue().equals(eventType)
						|| EventTypeEnum.OSSERVAZIONE_ANNULLATA.getValue().equals(eventType) || EventTypeEnum.RICHIESTA_INTEGRAZIONE_ANNULLATA.getValue().equals(eventType)
						|| EventTypeEnum.RELAZIONE_POSITIVA_ANNULLATA.getValue().equals(eventType) || EventTypeEnum.RELAZIONE_NEGATIVA_ANNULLATA.getValue().equals(eventType)) {
					return true;
				}

				// Destinatari con formato sia elettronico che cartaceo che interno.
				// Gli eventi che concludono il procedimento sono:
				if (countDestinatariElettronici > 0 && countDestinatariCartacei > 0 && countDestinatariInterni > 0) {
					// a) "COMPLETAMENTO SPEDIZIONE ELETTRONICA", per i destinatari in formato
					// elettronico
					// b) Invocazione response "SPEDITO" da parte dell’utente.
					// I due eventi sopracitati possono verificarsi in momenti diversi, quindi si
					// cerca in tutti gli step.
					boolean completamentoSpedizioneElettronica = false;
					boolean spedito = false;
					for (final StepDTO step : listaStepDocumento) {
						final String stepEventType = step.getHeader().getEventType();
						if (EventTypeEnum.COMPLETAMENTO_SPED_ELETTRONICA.getValue().equals(stepEventType)) {
							completamentoSpedizioneElettronica = true;
						} else if (EventTypeEnum.SPEDITO.getValue().equals(stepEventType)) {
							spedito = true;
						}
					}
					return completamentoSpedizioneElettronica && spedito;
				}
			}
		}

		return false;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV#getDetailFromCEDocumentoNSD(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      java.lang.String, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public DetailDocumentRedDTO getDetailFromCEDocumentoNSD(final IFilenetCEHelper fceh, final String documentTitle, final UtenteDTO utente) {
		try {
			final Document d = fceh.getDocumentRedForDetail(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY),
					documentTitle);
			return TrasformCE.transform(d, TrasformerCEEnum.FROM_DOCUMENTO_TO_DETAIL_RED);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero del documento: " + e.getMessage(), e);
		}

		return null;
	}

	private EmailDTO getMailByDocumentTitleAndIdAOO(final String documentTitle, final Long idAoo, final IFilenetCEHelper fceh) {
		EmailDTO email = null;

		try {
			final Document mailFilenet = fceh.fetchMailSpedizione(documentTitle, idAoo.intValue());

			if (mailFilenet == null) {
				return email;
			}

			email = TrasformCE.transform(mailFilenet, TrasformerCEEnum.FROM_DOCUMENTO_TO_MAIL_MASTER_WITH_CONTENT);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}

		return email;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV#getMailByDocumentTitleAndIdAOObyAdmin(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public EmailDTO getMailByDocumentTitleAndIdAOObyAdmin(final String documentTitle, final UtenteDTO utente) {
		IFilenetCEHelper fceh = null;
		Connection connection = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()), utente.getIdAoo());
			connection = getDataSource().getConnection();

			return getMailByDocumentTitleAndIdAOO(documentTitle, utente.getIdAoo(), fceh);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV#getAllegatiMail(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public List<DetailDocumentRedDTO> getAllegatiMail(final String guidMail, final UtenteDTO utente) {
		final List<DetailDocumentRedDTO> allegati = new ArrayList<>();
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final DocumentSet allegatiFN = fceh.getAllegati(guidMail, false, false);

			if (allegatiFN != null) {
				final Iterator<?> it = allegatiFN.iterator();
				while (it.hasNext()) {
					final Document a = (Document) it.next();
					allegati.add(TrasformCE.transform(a, TrasformerCEEnum.FROM_DOCUMENTO_TO_LIGHT_DETAIL_RED));
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}

		return allegati;
	}

	/**
	 * @see it.ibm.red.business.service.IDocumentoRedSRV#verificaProtocollo(int,
	 *      int, it.ibm.red.business.enums.TipoCategoriaEnum, java.lang.Long,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public Document verificaProtocollo(final int annoProtocollo, final int numeroProtocollo, final TipoCategoriaEnum tipoCategoria, final Long idAoo,
			final IFilenetCEHelper fceh, final Connection connection) {
		final List<TipologiaDocumentoDTO> list = tipologiaDocumentoDAO.getComboTipologieByAoo(idAoo, connection);

		final List<String> classiDocumentali = new ArrayList<>();
		if (list != null && !list.isEmpty()) {
			for (final TipologiaDocumentoDTO tipo : list) {
				if (!classiDocumentali.contains(tipo.getDocumentClass())) {
					classiDocumentali.add(tipo.getDocumentClass());
				}
			}
		}

		return fceh.verificaProtocollo(classiDocumentali, annoProtocollo, numeroProtocollo, null, tipoCategoria.getIdTipoCategoria().intValue());
	}

	/**
	 * Questo metodo restituisce {@code true} se il documento è modificabile
	 * dall'utente loggato. Visualizzazione del tasto registra:
	 * 
	 * Passo 1 - Cerco tra tutte le assegnazioni del documento quella principale e
	 * controllo che sia assegnata al mio ufficio (se è un assegnazione per ufficio)
	 * oppure se è assegnata a me e al mio ufficio (se è un assegnazione per
	 * utente): se non la trovo, il documento NON è modificabile.
	 * 
	 * Altrimenti, Passo 2 - il documento NON è modificabile se l'assegnazione
	 * principale NON è una di queste:
	 * <ul>
	 * 		<li>COMPETENZA</li>
	 * 		<li>FIRMA</li>
	 * 		<li>SIGLA</li>
	 * 		<li>FIRMA_MULTIPLA</li>
	 * 		<li>RIFIUTO_FIRMA</li>
	 * 		<li>RIFIUTO_SIGLA</li>
	 * 		<li>RIFIUTO_FIRMA_MULTIPLA</li>
	 * 		<li>COPIA_CONFORME</li>
	 * </ul>
	 */
	@Override
	public boolean isDocumentoModificabile(final DetailDocumentRedDTO detail, final UtenteDTO utente) {
		boolean isDocumentoModificabile = false;

		try {
			/** TROVO L'ASSEGNAZIONE PRINCIPALE ********************************/
			AssegnazioneDTO assegnazionePrincipale = null;
			for (final AssegnazioneDTO a : detail.getAssegnazioni()) {
				if (a != null && a.getUfficio() != null // l'ufficio non è presente per le DSR buggate: le DSR non sono modificabili.
						&& a.getUfficio().getId().equals(utente.getIdUfficio()) // l'ufficio c'è sempre
						&& (a.getUtente() == null || a.getUtente().getId().equals(utente.getId())) // se esiste l'utente
						&& (a.getWobNumber() != null && a.getWobNumber().equals(detail.getWobNumberPrincipale()))) { // controllo che sia l'assegnazione principale
					assegnazionePrincipale = a;
					break;
				}
			}

			// Se l'assegnazione principale NON è presente, il documento NON È MODIFICABILE.
			if (assegnazionePrincipale != null) {

				LOGGER.info("[CHECK DOC-CONTENT MODIFICABILE][" + detail.getDocumentTitle() + "] trovata assegnazione principale " + assegnazionePrincipale);

				// Se esiste una assegnazione principale e corrisponde tra queste è possibile
				// modificare il documento
				// altrimenti l'utente non può modificare il documento
				switch (assegnazionePrincipale.getTipoAssegnazione()) {
				case COMPETENZA:
				case FIRMA:
				case SIGLA:
				case FIRMA_MULTIPLA:
//				case VISTO:
				case RIFIUTO_ASSEGNAZIONE:
				case RIFIUTO_FIRMA:
				case RIFIUTO_SIGLA:
//				case RIFIUTO_VISTO:
				case RIFIUTO_FIRMA_MULTIPLA:
					isDocumentoModificabile = true;
					break;
				case COPIA_CONFORME:
					// Nel caso di allegati per Copia Conforme, il documento deve poter essere
					// modificato
					// in seguito all'eventuale rifiuto dell'attestazone di Copia Conforme da parte
					// del Responsabile per la Copia Conforme.
					if (!isInLibroFirmaResponsabileCopiaConforme(detail.getDocumentTitle(), detail.getAllegati(), detail.getResponsabileCopiaConforme(), utente)) {
						isDocumentoModificabile = true;
					}
					break;
				default:
					isDocumentoModificabile = false;
					break;
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}

		return isDocumentoModificabile;
	}

	/**
	 * Il metodo e' copiato da quello che fa RED e mi lascia varie perplessita'.
	 * Oltre a restituire il wob number del wkf principale imposta anche la
	 * DESCRIZIONE dell'iter (sia manuale che automatico) in modo da visualizzarlo
	 * correttamente nel dettaglio sintetico.
	 * 
	 * @param detail
	 * @param query
	 * @param connection
	 * @return
	 */
	private String getWobNumberWorkflowPrincipale(final DetailDocumentRedDTO detail, final VWRosterQuery query, final Connection connection) {
		String wobNumber = null;

		try {
			final String procEntrataGenerico = tipoProcedimentoSRV.getWorkflowSubjectProcessoGenericoEntrata(detail.getIdAOO().longValue());
			final String procUscitaGenerico = tipoProcedimentoSRV.getWorkflowSubjectProcessoGenericoUscita(detail.getIdAOO().longValue());

			final int idWorkflowStep = Integer.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.WF_PROCESSO_STEP_MAPPING_WFKEY));
			final Workflow wkfStep = workflowDAO.getById(connection, idWorkflowStep);

			VWWorkObject wo = null;
			while (query.hasNext()) {
				wo = (VWWorkObject) query.next();
				if ("FirmaCopiaConforme".equals(wo.getSubject()) || procEntrataGenerico.equals(wo.getSubject()) || procUscitaGenerico.equals(wo.getSubject())
						|| wkfStep.getDescription().equals(wo.getSubject())) {
					wobNumber = wo.getWorkObjectNumber();
					detail.setIdIterApprovativo(null); // Nessun iter oppure manuale
					detail.setIterApprovativoSemaforo(null);
					break;
				} else {
					final List<IterApprovativoDTO> iterApprovativi = iterApprovativoDAO.getAllByIdAOO(detail.getIdAOO(), connection);
					// Iter approvativi
					for (final IterApprovativoDTO iterApprovativoDTO : iterApprovativi) {
						if (iterApprovativoDTO.getWorkflowDescription().equals(wo.getSubject())) {
							wobNumber = wo.getWorkObjectNumber();
							detail.setIdIterApprovativo(iterApprovativoDTO.getIdIterApprovativo());
							detail.setIterApprovativoSemaforo(iterApprovativoDTO.getDescrizione());
							break;
						}
					}

					if (!StringUtils.isNullOrEmpty(wobNumber)) {
						break;
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}

		return wobNumber;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.ibm.red.business.service.IDocumentoRedSRV#getWobNumberWorkflowPrincipale
	 * (java.lang.Integer, java.lang.String, java.lang.String,
	 * it.ibm.red.business.helper.filenet.pe.FilenetPEHelper, java.sql.Connection)
	 */
	@Override
	public final String getWobNumberWorkflowPrincipale(final Integer idAOO, final String documentTitle, final String idClientAoo, final FilenetPEHelper fpeh,
			final Connection connection) {
		String wobNum = "";
		final DetailDocumentRedDTO detailDTO = new DetailDocumentRedDTO();
		detailDTO.setIdAOO(idAOO);
		final VWRosterQuery query = fpeh.getRosterQueryWorkByIdDocumentoAndCodiceAOO(documentTitle, idClientAoo);
		wobNum = getWobNumberWorkflowPrincipale(detailDTO, query, connection);
		query.resetFetch(); // resetto il puntatore
		return wobNum;
	}

	@Override
	public final List<VersionDTO> getVersioniDocumento(final String guid, final Integer idAoo, final UtenteDTO utente, final boolean isRicercaRiservato) {
		return getVersioniDocumentoAllegato(guid, idAoo, null, utente, isRicercaRiservato, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV#
	 * getVersioniDocumento (java.lang.String, java.lang.Integer,
	 * it.ibm.red.business.dto.UtenteDTO)
	 */
	@Override
	public final List<VersionDTO> getVersioniDocumento(final String guid, final Integer idAoo, final UtenteDTO utente) {
		return getVersioniDocumentoAllegato(guid, idAoo, null, utente, isRicercaRiservato, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV#
	 * getVersioniDocumentoAllegato (java.lang.String, java.lang.Integer,
	 * java.lang.Integer, it.ibm.red.business.dto.UtenteDTO)
	 */
	@Override
	public final List<VersionDTO> getVersioniDocumentoAllegato(final String guid, final Integer idAoo, final Integer idCategoriaDocPrincipale, final UtenteDTO utente,
			final boolean isRicercaRiservato, String codiceFlussoPrincipale) {
		List<VersionDTO> list = null;
		IFilenetCEHelper fceh = null;

		try {
			list = new ArrayList<>();
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			if (isRicercaRiservato) {
				final FilenetCredentialsDTO fcDTO = new FilenetCredentialsDTO(utente.getFcDTO());
				fceh = FilenetCEHelperProxy.newInstance(fcDTO, utente.getIdAoo());
			}
			final List<?> versionList = fceh.getDocumentVersionListByGuid(guid, idAoo);

			if (!CollectionUtils.isEmpty(versionList)) {
				final Iterator<?> itVersionList = versionList.iterator();

				VersionDTO v = null;
				int versionNumber = versionList.size();
				while (itVersionList.hasNext()) {
					final Document document = (Document) itVersionList.next();

					final EnumMap<ContextTrasformerCEEnum, Object> context = new EnumMap<>(ContextTrasformerCEEnum.class);
					context.put(ContextTrasformerCEEnum.NUM_VERSIONE_DOC, versionNumber);
					
					if (idCategoriaDocPrincipale != null) {
						context.put(ContextTrasformerCEEnum.ID_CATEGORIA_DOC_PRINCIPALE, idCategoriaDocPrincipale);
					}

					if(codiceFlussoPrincipale != null) {
						context.put(ContextTrasformerCEEnum.CODICE_FLUSSO_PRINCIPALE_METAKEY, codiceFlussoPrincipale);
					}
					
					v = TrasformCE.transform(document, TrasformerCEEnum.FROM_DOCUMENT_TO_RED_VERSION, context);
					v.setVersionNumber(versionNumber);
					v.setDocumentTitle(guid);

					list.add(v);
					versionNumber--;
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}

		return list;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV#
	 * eliminaDocumentoInterno (java.lang.String, int,
	 * it.ibm.red.business.dto.UtenteDTO)
	 */
	@Override
	public final EsitoOperazioneDTO eliminaDocumentoInterno(final String documentTitle, final int idFascicolo, final UtenteDTO utente) {
		final Integer idDocumento = Integer.parseInt(documentTitle);
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(idDocumento);
		Connection connection = null;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			// Si elimina il documento da FileNet
			fceh.eliminaDocumentoByDocumentTitle(documentTitle, utente.getIdAoo());

			// Si rimuove la relazione tra documento e fascicolo dal DB
			connection = setupConnection(getDataSource().getConnection(), true);

			final int eliminazioneOk = fascicoloDAO.deleteDocumentoFascicolo(idDocumento, idFascicolo, utente.getIdAoo(), connection);

			if (eliminazioneOk > 0) {
				esito.setEsito(true);
			} else {
				esito.setNote("Si è verificato un errore nell'eliminazione del documento interno.");
			}

			commitConnection(connection);

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore nell'eliminazione del documento interno: " + documentTitle, e);
			rollbackConnection(connection);
			esito.setNote("Si è verificato un errore nell'eliminazione del documento interno.");
			esito.setEsito(false);
		} finally {
			closeConnection(connection);
			popSubject(fceh);
		}

		return esito;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV#getFatturaDetail
	 * (java.lang.String, it.ibm.red.business.dto.UtenteDTO, java.lang.String,
	 * java.lang.String)
	 */
	@Override
	public final DetailFatturaFepaDTO getFatturaDetail(final String documentTitle, final UtenteDTO utente, final String wobNumberIN, final String inDocumentClassName) {
		DetailFatturaFepaDTO detailDTO = null;

		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		Connection connection = null;

		try {
			// vado su filenet e attraverso il documentTitle recupero il documento
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			String documentClassName = inDocumentClassName;
			if (StringUtils.isNullOrEmpty(documentClassName)) {
				documentClassName = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FATTURA_FEPA_CLASSNAME_FN_METAKEY);
			}
			final Document docFilenet = fceh.getDocumentRedForDetail(documentClassName, documentTitle);

			if (docFilenet == null) {
				throw new RedException("Il documento e' NULL");
			}

			connection = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			// Recupero il DTO dal documento filenet
			detailDTO = TrasformCE.transform(docFilenet, TrasformerCEEnum.FROM_DOCUMENT_TO_FATTURA, connection);

			// recupero eventuali metadati dinamici START
			final List<MetadatoDinamicoDTO> list = metadatiDinamiciSRV.getMetadatyByClasseDocumentale(utente, documentClassName);
			if (list != null && !list.isEmpty()) {
				detailDTO.setMetadatiDinamici(metadatiDinamiciSRV.settaValoriMetadatiDinamici(docFilenet, list));
			}
			// recupero eventuali metadati dinamici END

			/**
			 * ASSEGNAZIONI START + eventuale recupero del wobNumber
			 ********************************************************/
			final Aoo aoo = detailDTO.getAoo();
			// per ogni assegnazione il metodo del PE gestisce solo gli id dell'ufficio e
			// dell'utente(se c'è)

			detailDTO.setWobNumber(wobNumberIN);
			// Se provengo ad esempio da una ricerca e non ho il wobNumber del documento
			// devo trovare il wobNumber del workflow "principale"
			// Ho bisogno di andare sul roster per avere sia questa informazione che per
			// avere la lista di assegnatari del documento
			List<AssegnazioneDTO> assegnazioniList = null;

			assegnazioniList = fpeh.getAssegnazioni(detailDTO.getDocumentTitle(), aoo.getCodiceAoo(), utente.getFcDTO().getIdClientAoo());
			detailDTO.setAssegnazioni(new ArrayList<>());
			if (assegnazioniList != null && !assegnazioniList.isEmpty()) {

				Utente ute = null;
				UtenteDTO inUtente = null;
				Nodo n = null;
				UfficioDTO inUfficio = null;
				// per ogni assegnazione recupero i dati del nodo ufficio e se ci sono i dati
				// dell'utente
				for (final AssegnazioneDTO a : assegnazioniList) {

					n = nodoDAO.getNodo(a.getUfficio().getId(), connection);
					if (n == null) {
						continue; // l'ufficio e' importante che ci sia
					}
					inUfficio = new UfficioDTO(n.getIdNodo(), n.getDescrizione());

					if (a.getUtente().getId() != null && a.getUtente().getId() > 0) {
						ute = utenteDAO.getUtente(a.getUtente().getId(), connection);
						inUtente = new UtenteDTO(ute);
						inUtente.setIdUfficio(n.getIdNodo());
						inUtente.setUfficioDesc(n.getDescrizione());
						a.setDescrizioneAssegnatario(inUtente.getNome() + " " + inUtente.getCognome());
					} else {
						inUtente = null;
						a.setDescrizioneAssegnatario(inUfficio.getDescrizione());
					}

					a.setUfficio(inUfficio);
					a.setUtente(inUtente);
				}
				detailDTO.setAssegnazioni(assegnazioniList);
			}
			/** ASSEGNAZIONI END **********************************************************/

			/**
			 * FASCICOLI E FALDONI START
			 ***************************************************************************/
			List<FascicoloDTO> fascicoliDTO = null;
			final List<Document> fascicoliFilenet = fceh.getFascicoliByFoldersFiledIn(docFilenet.get_FoldersFiledIn(), aoo.getIdAoo().intValue());

			if (fascicoliFilenet != null && !fascicoliFilenet.isEmpty()) {
				final List<TitolarioDTO> titolari = titolarioSRV.getFigliRadice(utente.getIdAoo(), utente.getIdUfficio(), connection);
				fascicoliDTO = new ArrayList<>();
				FascicoloDTO fascicoloDTO = null;
				// setto la descrizione dell'indice di classificazione per il titolario del
				// fascicolo
				for (final Document docFascicolo : fascicoliFilenet) {
					fascicoloDTO = TrasformCE.transform(docFascicolo, TrasformerCEEnum.FROM_DOCUMENTO_TO_FASCICOLO);
					for (final TitolarioDTO tit : titolari) {
						if (tit.getIndiceClassificazione().equals(fascicoloDTO.getIndiceClassificazione())) {
							fascicoloDTO.setDescrizioneTitolario(tit.getDescrizione());
							break;
						}
					}

					/**
					 * Recupero i faldoni start
					 ****************************************************/
					if (!docFascicolo.get_FoldersFiledIn().isEmpty()) {
						// prendo i faldoni
						final Iterator<?> itFaldoni = docFascicolo.get_FoldersFiledIn().iterator();
						Document faldoneFilenet = null;
						String guidFaldone = null;
						FaldoneDTO faldoneDTO = null;
						Collection<FaldoneDTO> faldoni = null;

						while (itFaldoni.hasNext()) {
							guidFaldone = StringUtils.cleanGuidToString(((Containable) itFaldoni.next()).get_Id());
							faldoneFilenet = fceh.getFaldoneByFascicoloGuid(guidFaldone, detailDTO.getIdAOO());
							if (faldoneFilenet != null) {
								if (faldoni == null) {
									faldoni = new ArrayList<>();
								}
								faldoneDTO = TrasformCE.transform(faldoneFilenet, TrasformerCEEnum.FROM_DOCUMENT_TO_FALDONE);
								if (faldoneDTO != null) {
									if (!StringUtils.isNullOrEmpty(faldoneDTO.getParentFaldone())) {
										final Document padreD = fceh.getFaldoneByDocumentTitle(faldoneDTO.getParentFaldone(), faldoneDTO.getIdAOO());
										if (padreD != null) {
											final String nomePadre = (String) TrasformerCE.getMetadato(padreD, PropertiesNameEnum.METADATO_FALDONE_NOMEFALDONE);
											faldoneDTO.setNomePadre(nomePadre);
											final String oggettoPadre = (String) TrasformerCE.getMetadato(padreD, PropertiesNameEnum.METADATO_FALDONE_OGGETTO);
											faldoneDTO.setOggettoPadre(oggettoPadre);
										}
									}

									faldoni.add(faldoneDTO);
								}
							}
						}
						fascicoloDTO.setFaldoni(faldoni);
					}
					/**
					 * Recupero i faldoni end
					 ******************************************************/
					fascicoliDTO.add(fascicoloDTO);
				}

				detailDTO.setFascicoli(fascicoliDTO);

				/**
				 * FASCICOLO PROCEDIMENTALE START
				 *****************************************************************************************/
				List<FaldoneDTO> faldoniDelProcedimentale = null;

				if (!fascicoliDTO.isEmpty()) {
					final FascicoloDTO procedimentale = fascicoloSRV.getFascicoloProcedimentale(documentTitle, detailDTO.getIdAOO(), fascicoliDTO, connection);

					detailDTO.setDescrizioneFascicoloProcedimentale(procedimentale.getDescrizione());
					detailDTO.setIdFascicoloProcedimentale(procedimentale.getIdFascicolo());
					detailDTO.setDescrizioneTitolarioFascicoloProcedimentale(procedimentale.getDescrizioneTitolario());
					if (!StringUtils.isNullOrEmpty(procedimentale.getOggetto())) {
						final String[] arr = procedimentale.getOggetto().split("_");
						if (arr.length >= 3) {
							detailDTO.setPrefixFascicoloProcedimentale(arr[0] + "_" + arr[1] + "_");
						}
					}
					detailDTO.setIndiceClassificazioneFascicoloProcedimentale(procedimentale.getIndiceClassificazione());
					faldoniDelProcedimentale = (List<FaldoneDTO>) procedimentale.getFaldoni();
					// setto nel dettaglio i faldoni del procedimentale (quelli del doc, nel tab
					// faldoni)
					detailDTO.setFaldoni(faldoniDelProcedimentale);

				}
				/**
				 * FASCICOLO PROCEDIMENTALE END
				 *******************************************************************************************/
			}
			/**
			 * FASCICOLI E FALDONI END
			 *****************************************************************************/

			/**
			 * ALLEGATI START
			 ***************************************************************************/
			final DocumentSet dSet = fceh.getAllegati(detailDTO.getGuid(), false, false);
			List<AllegatoDTO> allegatiDTO = null;
			if (!dSet.isEmpty()) {
				allegatiDTO = new ArrayList<>();
				final Iterator<?> it = dSet.iterator();
				Document alleDocument = null;

				final Map<ContextTrasformerCEEnum, Object> context = tipologiaDocumentoSRV.getContextTipologieDocumento(detailDTO.getIdCategoriaDocumento(), utente,
						connection);

				while (it.hasNext()) {
					alleDocument = (Document) it.next();
					allegatiDTO.add(TrasformCE.transform(alleDocument, TrasformerCEEnum.FROM_DOCUMENT_TO_ALLEGATO_CONTEXT, context, connection));
				}

				detailDTO.setAllegati(allegatiDTO);
			}
			/**
			 * ALLEGATI END
			 *****************************************************************************/

			/**
			 * CONTRIBUTI START
			 ***************************************************************************/
			final List<ContributoDTO> contributi = contributoSRV.getContributi(fceh, detailDTO.getDocumentTitle(), utente.getIdAoo());
			detailDTO.setContributi(contributi);
			/**
			 * CONTRIBUTI END
			 *****************************************************************************/

			/**
			 * GESTIONE VERSIONI DEL DOCUMENTO PRINCIPALE E DEI SUOI ALLEGATI START
			 ********************************************/
			// Si recupera la lista delle versioni del documento e dei suoi eventuali
			// allegati
			detailDTO.setVersioni(getVersioniDocumento(detailDTO.getGuid(), detailDTO.getIdAOO(), utente));

			if (detailDTO.getAllegati() != null && !detailDTO.getAllegati().isEmpty()) {
				for (final AllegatoDTO allegatoDTO : detailDTO.getAllegati()) {
					allegatoDTO.setVersions(
							getVersioniDocumentoAllegato(allegatoDTO.getGuid(), detailDTO.getIdAOO(), detailDTO.getIdCategoriaDocumento(), utente, isRicercaRiservato, detailDTO.getCodiceFlusso()));
				}
			}
			/**
			 * GESTIONE VERSIONI DEL DOCUMENTO PRINCIPALE E DEI SUOI ALLEGATI END
			 **********************************************/
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero del dettaglio: " + e.getMessage(), e);
			detailDTO = null;
			throw new RedException(e);
		} finally {
			closeConnection(connection);
			logoff(fpeh);
			popSubject(fceh);
		}

		return detailDTO;
	}

	@Override
	public final Collection<EsitoOperazioneDTO> annullaDocumenti(final Collection<String> docTitlesDaAnnullare, final String motivoAnnullamento, final UtenteDTO utente) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();

		for (final String docTitleDaAnnullare : docTitlesDaAnnullare) {
			esiti.add(annullaDocumento(docTitleDaAnnullare, motivoAnnullamento, utente));
		}

		return esiti;
	}

	@Override
	public final EsitoOperazioneDTO annullaDocumentoOAnnullaProtocollo(final String docTitleDaAnnullare, final String motivoAnnullamento, final String provvedimentoAnnullato,
			final UtenteDTO utente) {
		LOGGER.info("annullaDocumentoOAnnnullaProtocollo");
		final Integer idDocumento = Integer.parseInt(docTitleDaAnnullare);
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(idDocumento);
		IFilenetCEHelper fceh = null;

		try {

			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			esito = annullaDocumentoOAnnullaProtocollo(docTitleDaAnnullare, motivoAnnullamento, provvedimentoAnnullato, utente, fceh);

		} catch (final Exception e) {
			LOGGER.error("Errore durante l'annullamento del procedimento: ", e);
			esito.setNote("Annullamento del procedimento non riuscito");
			esito.setEsito(false);
		} finally {
			popSubject(fceh);
		}

		return esito;
	}

	@Override
	public final EsitoOperazioneDTO annullaDocumentoOAnnullaProtocollo(final String docTitleDaAnnullare, final String motivoAnnullamento, final String provvedimentoAnnullato,
			final UtenteDTO utente, final IFilenetCEHelper fceh) {

		LOGGER.info("annullaDocumentoOAnnnullaProtocollo");
		final Integer idDocumento = Integer.parseInt(docTitleDaAnnullare);
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(idDocumento);

		try {

			final Document documentoDaAnnullare = recuperaDocumentoDaAnnullare(docTitleDaAnnullare, utente, fceh);

			if (documentoDaAnnullare != null) {
				final String idProtocollo = (String) TrasformerCE.getMetadato(documentoDaAnnullare, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY);
				final Integer numeroProtocollo = (Integer) TrasformerCE.getMetadato(documentoDaAnnullare, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
				final Integer annoProtocollo = (Integer) TrasformerCE.getMetadato(documentoDaAnnullare, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);

				if (TipologiaProtocollazioneEnum.isProtocolloNPS(utente.getIdTipoProtocollo()) && !StringUtils.isNullOrEmpty(idProtocollo)) {

					final String infoProtocollo = new StringBuilder().append(numeroProtocollo).append("/").append(annoProtocollo).append("_").append(utente.getCodiceAoo())
							.toString();

					npsSRV.annullaProtocollo(idProtocollo, infoProtocollo, motivoAnnullamento, provvedimentoAnnullato, utente, docTitleDaAnnullare);
				}

				esito = annullaDocumento(docTitleDaAnnullare, motivoAnnullamento, utente, documentoDaAnnullare, fceh);
			} else {
				esito.setNote("L'utente non ha i permessi per annullare il documento");
				esito.setEsito(false);
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante l'annullamento del procedimento: ", e);
			esito.setNote("Annullamento del procedimento non riuscito");
			esito.setEsito(false);
		}

		return esito;
	}

	private EsitoOperazioneDTO annullaDocumento(final String docTitleDaAnnullare, final String motivoAnnullamento, final UtenteDTO utente, final Document documentoDaAnnullare,
			final IFilenetCEHelper fceh) {
		final PropertiesProvider pp = PropertiesProvider.getIstance();
		final Integer idDocumento = Integer.parseInt(docTitleDaAnnullare);
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(idDocumento);
		FilenetPEHelper fpehAdmin = null;
		boolean documentoAnnullato = false;

		try {
			String motivoAnnullamentoDaInserire = motivoAnnullamento;
			if (StringUtils.isNullOrEmpty(motivoAnnullamento)) {
				motivoAnnullamentoDaInserire = "N/A";
			}

			final FilenetCredentialsDTO fcAdmin = new FilenetCredentialsDTO(utente.getFcDTO());
			fpehAdmin = new FilenetPEHelper(fcAdmin);

			// Si recuperano i workflow del documento...
			final List<VWWorkObject> workflows = fpehAdmin.getWorkflowsByIdDocumentoNodoUtente(idDocumento, 0L, 0L, utente.getFcDTO().getIdClientAoo());
			if (!CollectionUtils.isEmpty(workflows)) {
				// ...e si eliminano
				for (final VWWorkObject workflow : workflows) {
					fpehAdmin.deleteWF(workflow);
					LOGGER.info("annullaDocumento -> È stato eliminato il workflow: " + workflow.getWorkflowNumber() + " relativo al documento: " + idDocumento);
				}
			}

			final Date dataAnnullamento = (Date) TrasformerCE.getMetadato(documentoDaAnnullare, PropertiesNameEnum.METADATO_DOCUMENTO_DATA_ANNULLAMENTO);
			if (dataAnnullamento == null) {
				final Map<String, Object> metadati = new HashMap<>();
				metadati.put(pp.getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_MOTIVAZIONE_ANNULLAMENTO), motivoAnnullamentoDaInserire);
				metadati.put(pp.getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_DATA_ANNULLAMENTO), new Date());

				// Si procede all'annullamento del documento sul CE effettuando le seguenti
				// operazioni:
				// 1) Inserimento di una nuova versione del documento con il content che include
				// la stampigliatura di annullamento
				// 2) Inserimento dei metadati relativi all'annullamento del documento (Data
				// Annullamento e Motivazione Annullamento)
				if (ContentType.PDF.equals(documentoDaAnnullare.get_MimeType())) {
					final IAdobeLCHelper adobeh = new AdobeLCHelper();
					final InputStream contentAnnullato = adobeh.insertWatermark(FilenetCEHelper.getDocumentContentAsInputStream(documentoDaAnnullare), "ANNULLATO");
					final String fileName = (String) TrasformerCE.getMetadato(documentoDaAnnullare, PropertiesNameEnum.NOME_FILE_METAKEY);

					fceh.addVersion(documentoDaAnnullare, contentAnnullato, metadati, fileName, MediaType.PDF.toString(), utente.getIdAoo());
				} else {

					fceh.updateMetadati(documentoDaAnnullare, metadati);
				}

				documentoAnnullato = true;

				// Si avvia il workflow per l'annullamento del documento
				metadati.clear();
				final String nomeWorkflow = pp.getParameterByKey(PropertiesNameEnum.WF_PROCESSO_GESTIONE_PROCESSI_WFKEY);

				metadati.put(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY), idDocumento);
				metadati.put(pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY), 0);
				metadati.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), utente.getIdUfficio());
				metadati.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId());
				metadati.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), motivoAnnullamentoDaInserire);

				final String wobNumber = fpehAdmin.avviaWF(nomeWorkflow, metadati, idDocumento, utente.getFcDTO().getIdClientAoo(), false, "Annulla documento");

				LOGGER.info("annullaDocumento -> È stato avviato il workflow: " + wobNumber + " per l'annullamento del documento: " + idDocumento);
				esito.setEsito(true);

			} else {
				esito.setNote("Attenzione, documento già annullato.");
				esito.setEsito(false);
			}
		} catch (final Exception e) {
			LOGGER.error("annullaDocumento -> Si è verificato un errore durante l'annullamento del documento: " + idDocumento
					+ ". Si procede con il ripristino della versione precedente del documento.", e);
			if (documentoDaAnnullare != null && documentoAnnullato) {
				fceh.eliminaUltimaVersione(documentoDaAnnullare);
			}
			esito.setNote("Annullamento del documento non riuscito.");
			esito.setEsito(false);
		} finally {
			logoff(fpehAdmin);
		}

		LOGGER.info("annullaDocumento -> END. Documento: " + docTitleDaAnnullare);
		return esito;
	}

	@Override
	public final EsitoOperazioneDTO annullaDocumento(final String docTitleDaAnnullare, final String motivoAnnullamento, final UtenteDTO utente) {
		LOGGER.info("annullaDocumento -> START. Documento: " + docTitleDaAnnullare);
		final Integer idDocumento = Integer.parseInt(docTitleDaAnnullare);
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(idDocumento);
		IFilenetCEHelper fceh = null;
		Document documentoDaAnnullare = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			documentoDaAnnullare = recuperaDocumentoDaAnnullare(docTitleDaAnnullare, utente, fceh);

			annullaDocumento(docTitleDaAnnullare, motivoAnnullamento, utente, documentoDaAnnullare, fceh);
		} catch (final Exception e) {
			LOGGER.error("annullaDocumento -> Si è verificato un errore durante l'annullamento del documento: " + idDocumento
					+ ". Si procede con il ripristino della versione precedente del documento.", e);
		} finally {
			popSubject(fceh);
		}

		LOGGER.info("annullaDocumento -> END. Documento: " + docTitleDaAnnullare);
		return esito;
	}

	private Document recuperaDocumentoDaAnnullare(final String docTitleDaAnnullare, final UtenteDTO utente, final IFilenetCEHelper fceh) {
		final PropertiesProvider pp = PropertiesProvider.getIstance();

		Document documentoDaAnnullare;
		// Metadati del documento da recuperare dal CE
		final PropertyFilter pf = new PropertyFilter();
		final String metadatoDataAnnullamento = pp.getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_DATA_ANNULLAMENTO);
		final String metadatoNomeFile = pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY);
		pf.addIncludeProperty(new FilterElement(null, null, null, metadatoDataAnnullamento, null));
		pf.addIncludeProperty(new FilterElement(null, null, null, metadatoNomeFile, null));
		pf.addIncludeProperty(new FilterElement(null, null, null, PropertyNames.ID, null));
		pf.addIncludeProperty(new FilterElement(null, null, null, PropertyNames.CONTENT_ELEMENTS, null));
		pf.addIncludeProperty(new FilterElement(null, null, null, PropertyNames.IS_RESERVED, null));
		pf.addIncludeProperty(new FilterElement(null, null, null, PropertyNames.RESERVATION, null));
		pf.addIncludeProperty(new FilterElement(null, null, null, PropertyNames.NAME, null));
		pf.addIncludeProperty(new FilterElement(null, null, null, PropertyNames.IS_CURRENT_VERSION, null));
		pf.addIncludeProperty(new FilterElement(null, null, null, PropertyNames.VERSION_SERIES, null));
		pf.addIncludeProperty(new FilterElement(null, null, null, PropertyNames.MIME_TYPE, null));

		final String metadatoIdProtocollo = pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY);
		final String metadatoNumeroProtocollo = pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
		final String metadatoAnnoProtocollo = pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
		pf.addIncludeProperty(new FilterElement(null, null, null, metadatoIdProtocollo, null));
		pf.addIncludeProperty(new FilterElement(null, null, null, metadatoNumeroProtocollo, null));
		pf.addIncludeProperty(new FilterElement(null, null, null, metadatoAnnoProtocollo, null));

		final List<String> selectList = new ArrayList<>();
		selectList.add(metadatoDataAnnullamento);
		selectList.add(metadatoNomeFile);
		selectList.add(PropertyNames.ID);
		selectList.add(PropertyNames.CONTENT_ELEMENTS);
		selectList.add(PropertyNames.IS_RESERVED);
		selectList.add(PropertyNames.RESERVATION);
		selectList.add(PropertyNames.NAME);
		selectList.add(PropertyNames.IS_CURRENT_VERSION);
		selectList.add(PropertyNames.VERSION_SERIES);
		selectList.add(PropertyNames.MIME_TYPE);
		selectList.add(pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY));

		selectList.add(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY));

		selectList.add(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY));

		// Si recupera il documento dal CE
		documentoDaAnnullare = fceh.getDocumentByIdGestionale(docTitleDaAnnullare, selectList, pf, utente.getIdAoo().intValue(), null, null);
		return documentoDaAnnullare;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV#aggiornaContentDocumento(java.lang.String,
	 *      java.lang.Integer, byte[], java.lang.String, java.lang.String,
	 *      java.lang.Integer, java.util.List, java.util.Map, java.lang.Integer,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public EsitoOperazioneDTO aggiornaContentDocumento(final String documentTitle, final Integer idTipologiaDocumento, final byte[] content, final String mimeType,
			final String fileName, final Integer idIterApprovativo, final List<AssegnazioneDTO> assegnazioni,
			final Map<TemplateValueDTO, List<TemplateMetadatoValueDTO>> templateDocUscitaMap, final Integer idTipoAssegnazione, final UtenteDTO utente) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(documentTitle);
		IFilenetCEHelper fceh = null;
		Connection con = null;
		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			con = setupConnection(getDataSource().getConnection(), false);

			Integer tipoFirma = IterApprovativoDTO.ITER_FIRMA_MANUALE;
			if (idIterApprovativo != null && idIterApprovativo != 0) {
				final IterApprovativoDTO iterApprovativoDTO = iterApprovativoDAO.getIterApprovativoById(idIterApprovativo, con);
				if (iterApprovativoDTO.getTipoFirma() != null) {
					tipoFirma = iterApprovativoDTO.getTipoFirma();
				}
			}

			/**
			 * documentManagerSRV.checkUploadFilePrincipale è il metodo che si occupa di
			 * controllare la validità del documento che si sta inserendo ed è richiamato
			 * prima della chiamata a questo metodo.
			 */
			aggiornaContentDocumento(esito, documentTitle, content, mimeType, fileName, tipoFirma, assegnazioni, templateDocUscitaMap, idTipoAssegnazione, utente, fceh);
		} catch (final Exception e) {
			LOGGER.error("aggiornaContentDocumento -> Si è verificato un errore durante l'aggiornamento del content del documento.", e);
			esito.setNote("Si è verificato un errore durante l'aggiornamento del content del documento.");
		} finally {
			popSubject(fceh);
			closeConnection(con);
		}

		return esito;
	}

	private EsitoOperazioneDTO verificaContent(final String documentTitle, final Integer idTipologiaDocumento, final byte[] content, final String mimeType,
			final String fileName) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(documentTitle);
		Connection con = null;

		try (
			ByteArrayInputStream bis = new ByteArrayInputStream(content);
		) {
			if (!StringUtils.isNullOrEmpty(mimeType)) {
				// Si verifica che il file in input sia supportato
				final boolean isFileSupportato = tipoFileSRV.isSupportedFileName(fileName, tipoFileSRV.getAll());

				if (isFileSupportato) {
					con = setupConnection(getDataSource().getConnection(), false);
					final String documentClass = tipologiaDocumentoDAO.getClasseDocumentaleByIdTipologia(idTipologiaDocumento, con);

					// Se la tipologia documento corrisponde alla Dichiarazione Servizi Resi, si
					// verifica che il file rispetti i criteri relativi al content di una DSR
					if (PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DSR_CLASSNAME_FN_METAKEY).equals(documentClass)) {
						if (MediaType.PDF.toString().equals(mimeType)) {
							final IAdobeLCHelper adobeHelper = new AdobeLCHelper();
							final CountSignsOutputDTO countSigns = adobeHelper.countsSignedFields(bis);

							if (countSigns.getNumEmptySignFields() < 1) {
								esito.setNote("La Dichiarazione dei Servizi Resi deve contenere un campo firma vuoto.");
							} else {
								esito.setEsito(true);
							}
						} else {
							esito.setNote("Il documento inserito non è una Dichiarazione dei Servizi Resi.");
						}
					} else {
						esito.setEsito(true); // Altre verifiche inserite in RED 1.46.2
					}
				} else {
					esito.setNote("Estensione del file non permessa.");
				}
			} else {
				esito.setNote("Estensione del file non presente.");
			}
		} catch (final Exception e) {
			LOGGER.error("verificaContent -> Si è verificato un errore durante la verifica del documento inserito: " + documentTitle, e);
			esito.setNote("Si è verificato un errore durante la verifica del documento inserito.");
		} finally {
			closeConnection(con);
		}

		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV#verificaContent(java.lang.String,
	 *      java.lang.Integer, byte[], java.lang.String, java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public EsitoOperazioneDTO verificaContent(final String documentTitle, final Integer idTipologiaDocumento, final byte[] content, final String mimeType,
			final String fileName, final UtenteDTO utente) {
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(documentTitle);
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			esito = verificaContent(documentTitle, idTipologiaDocumento, content, mimeType, fileName);
		} catch (final Exception e) {
			LOGGER.error("verificaContent -> Si è verificato un errore durante la verifica del documento inserito.", e);
			esito.setNote("Si è verificato un errore durante la verifica del documento inserito.");
		} finally {
			popSubject(fceh);
		}

		return esito;
	}

	private void aggiornaContentDocumento(final EsitoOperazioneDTO esito, final String documentTitle, final byte[] content, final String mimeType, final String fileName,
			final Integer tipoFirma, final List<AssegnazioneDTO> assegnazioni, final Map<TemplateValueDTO, List<TemplateMetadatoValueDTO>> templateDocUscitaMap,
			final Integer idTipoAssegnazione, final UtenteDTO utente, final IFilenetCEHelper fceh) {
		final Document currentVersion = fceh.getDocumentByDTandAOO(documentTitle, utente.getIdAoo());

		final Map<String, Object> metadatiDoc = new HashMap<>();
		metadatiDoc.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.TRASFORMAZIONE_PDF_ERRORE_METAKEY), Varie.TRASFORMAZIONE_PDF_OK);
		metadatiDoc.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), fileName);

		// Si aggiunge la nuova versione del documento su FileNet con il content e i
		// metadati aggiornati
		fceh.addVersionCompoundDocument(currentVersion, new ByteArrayInputStream(content), mimeType, metadatiDoc, true, utente.getIdAoo());

		// Si preparano i parametri per la trasformazione PDF sincrona
		final String[] firmatariArray = DocumentoUtils.initArrayAssegnazioni(TipoAssegnazioneEnum.FIRMA, assegnazioni);

		final CodaTrasformazioneParametriDTO paramsTrasformazione = new CodaTrasformazioneParametriDTO();
		paramsTrasformazione.setIdDocumento(documentTitle);
		paramsTrasformazione.setIdNodo(utente.getIdUfficio());
		paramsTrasformazione.setIdUtente(utente.getId());
		paramsTrasformazione.setPrincipale(true);
		paramsTrasformazione.setFirmatariString(firmatariArray);
		paramsTrasformazione.setTipoFirma(tipoFirma);
		paramsTrasformazione.setAggiornaFirmaPDF(true);
		paramsTrasformazione.setIdTipoAssegnazione(idTipoAssegnazione);

		// ### Si esegue la trasformazione PDF sincrona
		trasformazionePDFSRV.doTransformation(paramsTrasformazione);

		// ### Se il content è stato generato da un template per documenti in uscita, si
		// aggiornano i metadati del template sul DB
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), true);

			templateDocUscitaSRV.aggiornaDatiDocumento(NumberUtils.toInt(documentTitle), templateDocUscitaMap, utente.getId(), con);

			commitConnection(con);

			// ### Aggiornamento del documento con esito positivo
			esito.setEsito(true);
			esito.setNote("Documento aggiornato con successo.");
		} catch (final SQLException e) {
			LOGGER.error(e);
			rollbackConnection(con);
			esito.setNote("Si è verificato un errore durante l'aggiornamento del documento");
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV#
	 *      getDocumentoDopoTrasformazione (java.lang.String, java.lang.Long,
	 *      it.ibm.red.business.dto.FilenetCredentialsDTO).
	 */
	@Override
	public DetailDocumentRedDTO getDocumentoDopoTrasformazione(final String documentTitle, final Long idAOO, final FilenetCredentialsDTO fcDto) {
		DetailDocumentRedDTO d = null;
		IFilenetCEHelper fceh = null;

		try {
			String nomeFile = Constants.EMPTY_STRING;
			String contentType = Constants.EMPTY_STRING;
			byte[] content = null;

			fceh = FilenetCEHelperProxy.newInstance(fcDto, idAOO);

			final Document doc = fceh.getFileInfoAndContentByDocumentTitle(documentTitle, idAOO);
			final ContentTransfer ct = FilenetCEHelper.getDocumentContentTransfer(doc);

			contentType = ct.get_ContentType();
			content = FileUtils.getByteFromInputStream(ct.accessContentStream());
			nomeFile = (String) TrasformerCE.getMetadato(doc, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY));

			d = new DetailDocumentRedDTO();
			d.setNomeFile(nomeFile);
			d.setContent(content);
			d.setMimeType(contentType);
			d.setDocumentTitle(documentTitle);
			d.setGuid(doc.get_Id().toString());
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}

		return d;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV#hasDocumentDetail(
	 *      java.lang.String).
	 */
	@Override
	public boolean hasDocumentDetail(final String tipologiaDocumento) {
		return StringUtils.isNullOrEmpty(tipologiaDocumento) || !org.apache.commons.lang3.StringUtils.endsWith(tipologiaDocumento, "_LIGHT");
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV#
	 *      isCompetenteDocumento(java.lang.Integer,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public boolean isCompetenteDocumento(final Integer idDocumento, final UtenteDTO utente) {
		FilenetPEHelper fpeh = null;

		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			final List<VWWorkObject> workflows = fpeh.getWorkflowsByIdDocumentoNodoUtente(idDocumento, utente.getIdUfficio(), utente.getId(),
					utente.getFcDTO().getIdClientAoo());

			if (!CollectionUtils.isEmpty(workflows)) {
				for (final VWWorkObject workflow : workflows) {
					final Integer tipoAssegnazioneWorkflow = (Integer) TrasformerPE.getMetadato(workflow,
							PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY));

					if (tipoAssegnazioneWorkflow != null && (tipoAssegnazioneWorkflow.equals(TipoAssegnazioneEnum.COMPETENZA.getId())
							|| tipoAssegnazioneWorkflow.equals(TipoAssegnazioneEnum.FIRMA.getId()) || tipoAssegnazioneWorkflow.equals(TipoAssegnazioneEnum.SIGLA.getId())
							|| tipoAssegnazioneWorkflow.equals(TipoAssegnazioneEnum.VISTO.getId())
							|| tipoAssegnazioneWorkflow.equals(TipoAssegnazioneEnum.RIFIUTO_ASSEGNAZIONE.getId())
							|| tipoAssegnazioneWorkflow.equals(TipoAssegnazioneEnum.RIFIUTO_FIRMA.getId())
							|| tipoAssegnazioneWorkflow.equals(TipoAssegnazioneEnum.RIFIUTO_SIGLA.getId())
							|| tipoAssegnazioneWorkflow.equals(TipoAssegnazioneEnum.RIFIUTO_VISTO.getId())
							|| tipoAssegnazioneWorkflow.equals(TipoAssegnazioneEnum.SPEDIZIONE.getId()))) {
						return true;
					}
				}
			}

			return false;
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di verifica della assegnazioni del documento " + idDocumento, e);
			throw new RedException(e);
		} finally {
			logoff(fpeh);
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV#
	 * isInLibroFirmaResponsabileCopiaConforme (java.lang.String, java.util.List,
	 * it.ibm.red.business.dto.ResponsabileCopiaConformeDTO,
	 * it.ibm.red.business.dto.UtenteDTO)
	 */
	@Override
	public final boolean isInLibroFirmaResponsabileCopiaConforme(final String documentTitle, final List<AllegatoDTO> allegati,
			final ResponsabileCopiaConformeDTO responsabileCopiaConforme, final UtenteDTO utente) {
		boolean isInLibroFirmaRespCopiaConforme = false;

		if (!StringUtils.isNullOrEmpty(documentTitle) && !CollectionUtils.isEmpty(allegati)) {
			Long idUfficioCopiaConforme = null;
			Long idUtenteCopiaConforme = null;

			if (responsabileCopiaConforme != null) {
				idUfficioCopiaConforme = responsabileCopiaConforme.getIdNodo();
				idUtenteCopiaConforme = responsabileCopiaConforme.getIdUtente();
			} else {
				for (final AllegatoDTO al : allegati) {
					if (Boolean.TRUE.equals(al.getCopiaConforme())) {
						idUfficioCopiaConforme = al.getIdUfficioCopiaConforme();
						idUtenteCopiaConforme = al.getIdUtenteCopiaConforme();
						break;
					}
				}
			}

			if (idUfficioCopiaConforme != null && idUtenteCopiaConforme != null) {
				isInLibroFirmaRespCopiaConforme = listaDocumentiSRV.isDocumentoInLibroFirmaUtente(documentTitle, idUfficioCopiaConforme, idUtenteCopiaConforme, utente);
			}
		}

		return isInLibroFirmaRespCopiaConforme;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.ibm.red.business.service.ws.IDocumentoWsSRV#getElencoVersioniDocumento
	 * (java.lang.String, java.lang.Long,
	 * it.ibm.red.business.helper.filenet.ce.IFilenetHelper)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public final List<Document> getElencoVersioniDocumento(final String documentTitle, final Long idAoo, final IFilenetCEHelper fceh) {
		return (List<Document>) fceh.getDocumentVersionList(documentTitle, idAoo.intValue());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.service.IDocumentoRedSRV#getVersioneDocumento
	 * (java.lang.String, java.lang.Integer, java.lang.Long,
	 * it.ibm.red.business.helper.filenet.ce.IFilenetHelper)
	 */
	@Override
	public final Document getVersioneDocumento(final String documentTitle, final Integer numeroVersione, final Long idAoo, final IFilenetCEHelper fceh) {
		Document documentoVersione = null;

		try {
			documentoVersione = fceh.getDocumentByDTandAOO(documentTitle, idAoo);

			if (documentoVersione != null) {
				if (numeroVersione > 0) {
					documentoVersione = fceh.getDocumentVersion(documentoVersione, numeroVersione);
				}
			} else {
				LOGGER.error("Documento con ID: " + documentTitle + " non trovato");
				throw new RedException("Documento con ID: " + documentTitle + " non trovato");
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero della versione: " + numeroVersione + " del documento con ID: " + documentTitle, e);
			throw new RedException("Errore durante il recupero della versione richiesta del documento. [" + e.getMessage() + "]", e);
		}

		return documentoVersione;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV#
	 * hasAllegatiCopiaConforme(java.util.List)
	 */
	@Override
	public final boolean hasAllegatiCopiaConforme(final List<AllegatoDTO> allegati) {
		boolean hasAllegatiCopiaConforme = false;

		if (!CollectionUtils.isEmpty(allegati)) {
			for (final AllegatoDTO al : allegati) {
				if (Boolean.TRUE.equals(al.getCopiaConforme())) {
					hasAllegatiCopiaConforme = true;
					break;
				}
			}
		}

		return hasAllegatiCopiaConforme;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV#riattivaDocIngressoAfterChiusuraRisposta(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public EsitoOperazioneDTO riattivaDocIngressoAfterChiusuraRisposta(final String docTitleDocUscita, final UtenteDTO utente) {
		Connection conn = null;
		EsitoOperazioneDTO output = null;
		IFilenetCEHelper fceh = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			
			//verifica se il document titla in input è un'uscita
			Document d = fceh.getDocumentByDTandAOO(docTitleDocUscita, utente.getIdAoo());
			final Integer idCategoriaDoc = (Integer) TrasformerCE.getMetadato(d, PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY);
			final boolean isEntrata = CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0].equals(idCategoriaDoc)
					|| CategoriaDocumentoEnum.ASSEGNAZIONE_INTERNA.getIds()[0].equals(idCategoriaDoc)
					|| CategoriaDocumentoEnum.INTERNO.getIds()[0].equals(idCategoriaDoc);
			
			if(!isEntrata) {
				final Collection<RispostaAllaccioDTO> listDocEntrataAllacciati = allaccioDAO.getDocumentiRispostaAllaccio(Integer.parseInt(docTitleDocUscita),
						utente.getIdAoo().intValue(), conn);
				if (listDocEntrataAllacciati != null && !listDocEntrataAllacciati.isEmpty()) {
					for (final RispostaAllaccioDTO docEntrataAllacciato : listDocEntrataAllacciati) {
						if (Boolean.TRUE.equals(docEntrataAllacciato.getDocumentoDaChiudere())
								|| TipoAllaccioEnum.RICHIESTA_INTEGRAZIONE.equals(docEntrataAllacciato.getTipoAllaccioEnum())) {
							
							output = riattivaSRV.riattiva(docEntrataAllacciato.getIdDocumentoAllacciato(), utente, "Riattivazione dopo annullamento/messa agli atti doc uscita", true);
							
						}
					}
				}
			}
		} catch (final Exception ex) {
			LOGGER.error("Errore nel recupero dell'allaccio");
			throw new RedException("Errore nel recupero dell'allaccio" + ex);
		} finally {
			closeConnection(conn);
			popSubject(fceh);
		}
		return output;
	}
}