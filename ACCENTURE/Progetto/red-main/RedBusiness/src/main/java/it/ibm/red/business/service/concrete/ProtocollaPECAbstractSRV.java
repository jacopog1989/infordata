package it.ibm.red.business.service.concrete;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.io.FileCleaningTracker;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.core.Document;
import com.filenet.api.exception.EngineRuntimeException;
import com.filenet.api.exception.ExceptionCode;
import com.filenet.api.property.Properties;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.Modalita;
import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.ProtocollazionePECConfigDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedParametriDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ProvenienzaSalvaDocumentoEnum;
import it.ibm.red.business.enums.StatoMailEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoSpedizioneEnum;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.xml.AbstractXmlHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.INpsSRV;
import it.ibm.red.business.service.IProtocollaPECSRV;
import it.ibm.red.business.service.IRubricaSRV;
import it.ibm.red.business.service.ISalvaDocumentoSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.utils.FileUtils;

/**
 * Abstract del service di protocollazione PEC.
 */
@Service
@Component
public abstract class ProtocollaPECAbstractSRV extends AbstractService implements IProtocollaPECSRV {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = 5072215933806523575L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ProtocollaPECAbstractSRV.class.getName());

	/**
	 * DAO.
	 */
	@Autowired
	private IAooDAO aooDAO;

	/**
	 * Service.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IRubricaSRV rubricaSRV;

	/**
	 * Service.
	 */
	@Autowired
	private INpsSRV npsSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ISalvaDocumentoSRV salvaDocumentoSRV;

	/**
	 * Factory.
	 */
	private transient DiskFileItemFactory diskFileItemFactory;
	
	/**
	 * Fornitore della properties dell'applicazione.
	 */
	private PropertiesProvider pp;

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
		diskFileItemFactory = new DiskFileItemFactory();
		diskFileItemFactory.setFileCleaningTracker(new FileCleaningTracker());
	}
	
	/**
	 * Restituisce la factory disk file item.
	 * @return Factory.
	 */
	protected DiskFileItemFactory getDiskFileItemFactory() {
		return diskFileItemFactory;
	}

	/**
	 * Restituisce il provider delle properties.
	 * 
	 * @return Properties Provider.
	 */
	protected PropertiesProvider getPP() {
		return pp;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IProtocollaPECFacadeSRV#protocollaPEC(it.ibm.red.business.dto.ProtocollazionePECConfigDTO).
	 */
	@Override
	public void protocollaPEC(final ProtocollazionePECConfigDTO config) {
		DetailDocumentRedDTO documento = null;
		String guidDocumento = null;
		UtenteDTO utenteProtocollatore = null;
		Aoo aoo = null;
		Connection con = null;
		IFilenetCEHelper fceh = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			// Si recupera l'utente protocollatore
			utenteProtocollatore = getUtenteProtocollatore(config, con);

			// Si recupera l'AOO
			aoo = aooDAO.getAoo(utenteProtocollatore.getIdAoo(), con);
			
			//se siamo in regime di emergenza, non procedere con al protocollazione
			if (aoo.getTipoProtocollo().getIdTipoProtocollo().equals(TipologiaProtocollazioneEnum.EMERGENZANPS.getId())) {
				LOGGER.warn("Non è possibile protocollare le PEC in regime di emergenza");
			}
			
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero dell'utente protocollatore.", e);
		} finally {
			closeConnection(con);
		}

		try {
			// Si inizializza accesso al content engine in modalità administrator
			final AooFilenet aooFilenet = aoo.getAooFilenet();
			fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(),
					aooFilenet.getStanzaJaas(), aooFilenet.getObjectStore(), aoo.getIdAoo());

			final Document email = fetchFirstMail(fceh, config.getCasellaPostale());
			AbstractXmlHelper helper = null;
			if (email == null) {
				LOGGER.info("Nessuna mail da protocollare per l'AOO: " + config.getIdAoo());
				return;
			} 
			
			try {
				helper = initXmlHelper(fceh, email);
				
				// vecchio metodo overridePpcbConfiguration
				final Integer idUfficioDestinatarioFromMail = getUfficioDestinatarioFromMail(email, helper);
				if (idUfficioDestinatarioFromMail != null) {
					config.setIdNodoDestinatario(idUfficioDestinatarioFromMail);
					LOGGER.info(" nuovo ufficio destinatario: " + idUfficioDestinatarioFromMail);
				}
			} catch (final Exception e) {
				LOGGER.error("Errore in fase di parsing della segnatura", e);
				markInErrore(fceh, email);
				LOGGER.info(" mail " + email.get_Id() + " registrata in stato di errore");
				return;
			}

			if (hasSupportedFileExtensionsAttachment(fceh, email, config.getAllowedFileExtensions())) {
				if (!markInElaborazione(fceh, email)) {
					LOGGER.info(
							" non si è riusciti a prendere in carico la mail con guid " + email.get_Id().toString());
					return;
				}

				documento = new DetailDocumentRedDTO();
				try {
					extractMailAttachments(fceh, email, utenteProtocollatore, documento, helper);
					completeMetadatiDocumentoPrincipale(config, documento);

					if (documento.getAllegati() != null) {
						for (final AllegatoDTO allegato : documento.getAllegati()) {
							completeMetadatiAllegato(allegato);
						}
					}

					final String idFascicolo = getIdFascicoloDaAssociare(email, fceh, helper);
					final Map<String, Object> metadatiFascicolo = getMetadatiFascicoloDaAssociare(email, helper);

					final SalvaDocumentoRedParametriDTO params = new SalvaDocumentoRedParametriDTO();
					params.setModalita(Modalita.MODALITA_INS);
					params.setInputMailGuid(email.get_Id().toString());
					params.setIdFascicoloSelezionato(idFascicolo);
					params.setIndiceClassificazioneFascicolo(config.getIndiceClassificazione());
					params.setMetadatiFascicolo(metadatiFascicolo);
					params.setIdUfficioDestinatarioWorkflow(config.getIdNodoDestinatario().longValue());
					params.setIdUtenteDestinatarioWorkflow((long) 0);
					params.setIdUfficioMittenteWorkflow(config.getIdNodoDestinatario().longValue());
					params.setIdUtenteMittenteWorkflow((long) 0);
					params.setNomeFaldoniSelezionati((config.getIdFaldone() != null) ? Arrays.asList("" + config.getIdFaldone()) : null);
					params.setProtocollazioneP7M(sbustaP7M());
					
					// Assegnazione per competenza
					final UfficioDTO ufficioAssegnatario = new UfficioDTO(config.getIdNodoDestinatario().longValue(), null);
					
					documento.setAssegnazioni(new ArrayList<>());
					documento.getAssegnazioni().add(new AssegnazioneDTO(null, TipoAssegnazioneEnum.COMPETENZA, null, null, null, null, ufficioAssegnatario));

					final EsitoSalvaDocumentoDTO esito = salvaDocumentoSRV.salvaDocumento(documento, params, utenteProtocollatore, ProvenienzaSalvaDocumentoEnum.PROCESSO_AUTOMATICO_INTERNO);

					if (!esito.isEsitoOk()) {
						LOGGER.warn("Si sono verificati i seguenti errori in fase di creazione del documnento da mail");
						for (final Enum<?> e : esito.getErrori()) {
							LOGGER.warn(e.toString());
						}

						throw new RedException("Errore in fase di creazione del documento");
					} else {

						guidDocumento = esito.getGuid();
						LOGGER.info("Nuovo documento registrato con guid " + guidDocumento);

					}

					if (aoo.getTipoProtocollo().getIdTipoProtocollo().equals(TipologiaProtocollazioneEnum.NPS.getId())) {
						final Document documentoProtocollato = fceh.getDocumentByGuid(guidDocumento);
						final String idDocumento = documentoProtocollato.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
						npsSRV.uploadContentToAsyncNps(documentoProtocollato, config.getIdNodoDestinatario(), fceh, true, aoo, idDocumento);
					}

				} catch (final Exception e) {
					LOGGER.error("Errore in fase di registrazione del nuovo documento", e);
					markInErrore(fceh, email);
					LOGGER.info(" mail " + email.get_Id() + " registrata in stato di errore");
					return;
				}

				markProtocollata(fceh, email, guidDocumento);

				try {
					eseguiAttivitaPostProtocollazione(fceh, email, guidDocumento, aoo, helper);
				} catch (final Exception e) {
					LOGGER.error("Errore in fase di elaborazione delle attività post protocollazione ", e);
					markInErrorePostProtocollazione(fceh, email);
				}
			} else {
				LOGGER.info(" allegati non consentiti per la mail con guid " + email.get_Id().toString());
				markInArrivoScartata(fceh, email);
			}
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * Metodo non implementato.
	 * 
	 * @return {@code false}.
	 */
	protected boolean sbustaP7M() {
		return false;
	}

	private UtenteDTO getUtenteProtocollatore(final ProtocollazionePECConfigDTO config, final Connection con) {
		UtenteDTO utenteProtocollatore = null;

		final Integer idUtenteProtocollatore = config.getIdUtenteProtocollatore();

		// Si ottiene l'utente
		utenteProtocollatore = utenteSRV.getById(idUtenteProtocollatore.longValue(), con);

		if (utenteProtocollatore == null) {
			final String msg = "Utente protocollatore con ID utente: " + idUtenteProtocollatore + " non valido.";
			LOGGER.error(msg);
			throw new RedException(msg);
		}

		return utenteProtocollatore;
	}

	/**
	 * @see it.ibm.red.business.service.IProtocollaPECSRV#initXmlHelper(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      com.filenet.api.core.Document).
	 */
	@Override
	public AbstractXmlHelper initXmlHelper(final IFilenetCEHelper fceh, final Document email) {
		// il flusso non prevede alcun allegato xml da parsare
		return null;
	}

	/**
	 * @see it.ibm.red.business.service.IProtocollaPECSRV#getUfficioDestinatarioFromMail(com.filenet.api.core.Document,
	 *      it.ibm.red.business.helper.xml.AbstractXmlHelper).
	 */
	@Override
	public Integer getUfficioDestinatarioFromMail(final Document email, final AbstractXmlHelper helper) {
		// l'ufficio destinatario non dipende dalla mail, ma solo dalla configurazione
		return null;
	}

	/**
	 * @see it.ibm.red.business.service.IProtocollaPECSRV#fetchFirstMail(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      java.lang.String).
	 */
	@Override
	public Document fetchFirstMail(final IFilenetCEHelper fceh, final String casellaPostale) {
		final String filter = "AND stato = " + StatoMailEnum.INARRIVO.getStatus();
		final String casellePostaliFolder = pp.getParameterByKey(PropertiesNameEnum.FOLDER_ELETTRONICI_FN_METAKEY)
				.split("\\|")[1];
		final String folder = "/" + casellePostaliFolder + "/" + casellaPostale + "/"
				+ pp.getParameterByKey(PropertiesNameEnum.FOLDER_MAIL_INBOX_FN_METAKEY);
		final String classeDocumentale = pp.getParameterByKey(PropertiesNameEnum.MAIL_CLASSNAME_FN_METAKEY);

		final DocumentSet mails = fceh.getMailsAsDocument(folder, classeDocumentale, filter, null, true, null);
		if (mails != null && !mails.isEmpty()) {
			return (Document) mails.iterator().next();
		}

		return null;
	}

	/**
	 * @see it.ibm.red.business.service.IProtocollaPECSRV#hasSupportedFileExtensionsAttachment(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      com.filenet.api.core.Document, java.util.List).
	 */
	@Override
	public boolean hasSupportedFileExtensionsAttachment(final IFilenetCEHelper fceh, final Document email,
			final List<String> allowedFileExtensions) {

		if (allowedFileExtensions == null || allowedFileExtensions.isEmpty() || allowedFileExtensions.contains("*")) {
			return true;
		}

		return fceh.hasSupportedFileExtensionsAttachment(email, allowedFileExtensions);
	}

	/**
	 * @see it.ibm.red.business.service.IProtocollaPECSRV#getIdFascicoloDaAssociare(com.filenet.api.core.Document,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      it.ibm.red.business.helper.xml.AbstractXmlHelper).
	 */
	@Override
	public String getIdFascicoloDaAssociare(final Document mail, final IFilenetCEHelper fceh, final AbstractXmlHelper helper) {
		// Il documento deve essere creato all'interno di un nuovo fascicolo
		return null;
	}

	/**
	 * @see it.ibm.red.business.service.IProtocollaPECSRV#getMetadatiFascicoloDaAssociare(com.filenet.api.core.Document,
	 *      it.ibm.red.business.helper.xml.AbstractXmlHelper).
	 */
	@Override
	public Map<String, Object> getMetadatiFascicoloDaAssociare(final Document mail, final AbstractXmlHelper helper) {
		// Non sono previsti metadati aggiuntivi per il nuovo fascicolo
		return null;
	}

	/**
	 * @see it.ibm.red.business.service.IProtocollaPECSRV#markInElaborazione(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      com.filenet.api.core.Document).
	 */
	@Override
	public boolean markInElaborazione(final IFilenetCEHelper fceh, final Document email) {
		return updateStatoMail(fceh, email, StatoMailEnum.IN_FASE_DI_PROTOCOLLAZIONE_AUTOMATICA.getStatus());
	}

	/**
	 * @see it.ibm.red.business.service.IProtocollaPECSRV#markProtocollata(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      com.filenet.api.core.Document, java.lang.String).
	 */
	@Override
	public boolean markProtocollata(final IFilenetCEHelper fceh, final Document email, final String guidDocumentoProtocollato) {
		
		@SuppressWarnings("unchecked")
		List<String> protocolliGenerati = (List<String>) TrasformerCE.getMetadato(email, pp.getParameterByKey(PropertiesNameEnum.PROTOCOLLI_GENERATI_MAIL_METAKEY));
		if (CollectionUtils.isEmpty(protocolliGenerati)) {
			protocolliGenerati = new ArrayList<>();
		}

		// Struttura metadato: "NUMERO_PROTOCOLLO/ANNO_PROTOCOLLO"
		final Document doc = fceh.getDocumentByGuid(guidDocumentoProtocollato, 
				new String[] {
						getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY),
						getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY)
				});
		
		final Integer numeroProtocollo = doc.getProperties().getInteger32Value(getPP().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY));
		final Integer annoProtocollo = doc.getProperties().getInteger32Value(getPP().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY));
		String protocollo = null;
		if (numeroProtocollo != null && numeroProtocollo > 0 && annoProtocollo != null && annoProtocollo > 0) {
			protocollo = numeroProtocollo + "/" + annoProtocollo;
			protocolliGenerati.add(protocollo);
		}
		
		return updateStatoMail(fceh, email, StatoMailEnum.PROTOCOLLATA.getStatus(), protocolliGenerati);
	}

	/**
	 * @see it.ibm.red.business.service.IProtocollaPECSRV#markInArrivoScartata(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      com.filenet.api.core.Document).
	 */
	@Override
	public void markInArrivoScartata(final IFilenetCEHelper fceh, final Document email) {
		updateStatoMail(fceh, email, StatoMailEnum.INARRIVO_NON_PROTOCOLLABILE_AUTOMATICAMENTE.getStatus());
	}

	/**
	 * @see it.ibm.red.business.service.IProtocollaPECSRV#markInErrorePostProtocollazione(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      com.filenet.api.core.Document).
	 */
	@Override
	public boolean markInErrorePostProtocollazione(final IFilenetCEHelper fceh, final Document email) {
		return updateStatoMail(fceh, email, StatoMailEnum.ERRORE_DI_POST_PROTOCOLLAZIONE.getStatus());
	}

	/**
	 * @see it.ibm.red.business.service.IProtocollaPECSRV#markInErrore(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      com.filenet.api.core.Document).
	 */
	@Override
	public boolean markInErrore(final IFilenetCEHelper fceh, final Document email) {
		return updateStatoMail(fceh, email, StatoMailEnum.ERRORE_DI_PROTOCOLLAZIONE.getStatus());
	}

	/**
	 * @see it.ibm.red.business.service.IProtocollaPECSRV#eseguiAttivitaPostProtocollazione(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      com.filenet.api.core.Document, java.lang.String,
	 *      it.ibm.red.business.persistence.model.Aoo,
	 *      it.ibm.red.business.helper.xml.AbstractXmlHelper).
	 */
	@Override
	public void eseguiAttivitaPostProtocollazione(final IFilenetCEHelper fceh, final Document email,
			final String guidDocumento, final Aoo aoo, final AbstractXmlHelper helper) {
		LOGGER.info("Non è prevista alcuna attività post protocollazione per il flusso");
	}

	private boolean updateStatoMail(final IFilenetCEHelper fceh, final Document email, final Integer newState) {
		return updateStatoMail(fceh, email, newState, null);
	}
	
	private boolean updateStatoMail(final IFilenetCEHelper fceh, final Document email, final Integer newState, final List<String> protocolliGenerati) {
		try {
			final Map<String, Object> metadati = new HashMap<>();
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.STATO_MAIL_METAKEY), newState);
			if (protocolliGenerati != null) {
				metadati.put(pp.getParameterByKey(PropertiesNameEnum.PROTOCOLLI_GENERATI_MAIL_METAKEY), protocolliGenerati);
			}
			fceh.updateMetadati(email, metadati);
			return true;
		} catch (final EngineRuntimeException ere) {
			if (ere.getExceptionCode().equals(ExceptionCode.E_OBJECT_MODIFIED)) {
				LOGGER.warn("Intercettata elaborazione concorrente", ere);
				return false;
			}
			throw ere;
		}
	}

	/**
	 * @param fceh
	 * @param mail
	 * @param inNomeAllegato
	 * @return
	 */
	protected FileItem extractAllegato(final IFilenetCEHelper fceh, final Document mail, final String inNomeAllegato) {

		String nomeAllegato = inNomeAllegato;
		final List<String> selectList = new ArrayList<>();
		selectList.add(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
		selectList.add(pp.getParameterByKey(PropertiesNameEnum.ID_FN_METAKEY));
		selectList.add(pp.getParameterByKey(PropertiesNameEnum.CONTENT_ELEMENTS_METAKEY));
		selectList.add(pp.getParameterByKey(PropertiesNameEnum.MIMETYPE_FN_METAKEY));

		String filter = null;
		if (nomeAllegato != null) {
			filter = " AND d." + pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY) + "='" + nomeAllegato
					+ "' ";
		}

		final DocumentSet documenti = fceh.getChildsByGuid(mail.get_Id(), selectList, null, 1, filter);

		if (documenti.isEmpty()) {
			LOGGER.info("No mail attachment found, mail id: " + mail.get_Id().toString());
			return null;
		}

		final Document doc = (Document) documenti.iterator().next();
		LOGGER.info("Found mail attachment " + doc.get_Id().toString());

		if (nomeAllegato == null) {
			nomeAllegato = doc.getProperties().getStringValue("DocumentTitle");
		}

		final FileItem fi = getDiskFileItemFactory().createItem(nomeAllegato, doc.get_MimeType(), false, nomeAllegato);
		try {
			IOUtils.copy(doc.accessContentStream(0), fi.getOutputStream());
		} catch (final IOException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}

		return fi;
	}

	/**
	 * Completa i metadati del documento principale.
	 * 
	 * @param config
	 * @param documento
	 * @return
	 */
	private void completeMetadatiDocumentoPrincipale(final ProtocollazionePECConfigDTO config,
			final DetailDocumentRedDTO documento) {

		String mittente = documento.getMittente();

		final Contatto contatto = rubricaSRV.retrieveContattoFromMail(mittente, config.getIdAoo());

		mittente = contatto.getContattoID() + "," + mittente;
		documento.setMittente(mittente);

		documento.setMittenteContatto(contatto);

		documento.setIdTipologiaDocumento(config.getIdTipologiaDocumento());
		documento.setIdTipologiaProcedimento(config.getIdTipologiaProcedimento());
		documento.setIdFormatoDocumento(FormatoDocumentoEnum.ELETTRONICO.getId().intValue());
		documento.setFormatoDocumentoEnum(FormatoDocumentoEnum.ELETTRONICO);
		documento.setIdCategoriaDocumento(CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0]);
		documento.setMezzoRicezione(TipoSpedizioneEnum.ELETTRONICO.getId());
		documento.setIdAOO(config.getIdAoo());
		documento.setIdUfficioCreatore(config.getIdNodoDestinatario());
		documento.setIdUtenteCreatore(0);
		documento.setDataArchiviazione(new Date());
		documento.setIndiceClassificazioneFascicoloProcedimentale(config.getIndiceClassificazione());
	}

	/**
	 * Completa i metadati dell'allegato.
	 * 
	 * @param allegato
	 */
	private static void completeMetadatiAllegato(final AllegatoDTO allegato) {
		allegato.setFormato(FormatoAllegatoEnum.ELETTRONICO.getId());
	}
	
	/**
	 * Estrae gli allegati mail.
	 * 
	 * @param fceh
	 * @param email
	 * @param utente
	 * @param documento
	 * @param casellaPecEnte
	 */
	protected void extractMailAttachments(final Document email, final DetailDocumentRedDTO documento, final String casellaPecEnte) {
		
		try {
			final Document xmlDocument = (Document) email.get_ChildDocuments().iterator().next();
			documento.setDocumentClass(getPP().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));
			documento.setMimeType(Constants.ContentType.TEXT);
			documento.setContent(FileUtils.toByteArrayHtml(xmlDocument.accessContentStream(0)));
			
			final Properties propMail = email.getProperties();
			
			documento.setNomeFile(Constants.Mail.TESTO_MAIL_HTML);
			documento.setOggetto(propMail.getStringValue(getPP().getParameterByKey(PropertiesNameEnum.OGGETTO_EMAIL_METAKEY)));
			documento.setMittente(casellaPecEnte);
			documento.setTipoProtocollo(Constants.Protocollo.TIPO_PROTOCOLLO_ENTRATA);
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di estrazione degli allegati per la mail " + email.get_Id().toString(), e);
			throw new RedException(e);
		}
		
	}
	
	/**
	 * Recupera l'idFascicoloFepa.
	 * 
	 * @param mail
	 * @param fceh
	 * @param idFascicoloFepa
	 * @return
	 */
	protected String getIdFascicoloDaAssociare(final Document mail, final IFilenetCEHelper fceh, final String idFascicoloFepa) {
		
		try {
			
			if (idFascicoloFepa == null || StringUtils.isEmpty(idFascicoloFepa)) {
				return null;
			}
			
			final Document fascicolo = fceh.getFascicoloByIdFascicoloFepa(idFascicoloFepa);
			if (fascicolo == null) {
				return null;
			} else {
				return fascicolo.getProperties().getStringValue(getPP().getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY));
			}
			
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero dell'id fascicolo dall'id fascicolo Fepa per la mail " + mail.get_Id().toString(), e);
			throw new RedException(e);
		}
		
	}
	
	/**
	 * Recupera i metadati del fascicolo.
	 * 
	 * @param mail
	 * @param idFascicoloFepa
	 * @return
	 */
	protected Map<String, Object> getMetadatiFascicoloDaAssociare(final Document mail, final String idFascicoloFepa) {
		
		try {
			
			if (idFascicoloFepa == null || StringUtils.isEmpty(idFascicoloFepa)) {
				return null;
			}
			
			final Map<String, Object> metadatiFascicolo = new HashMap<>();
			metadatiFascicolo.put(getPP().getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA), idFascicoloFepa);
			return metadatiFascicolo;
			
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero dell'id fascicolo dall'id fascicolo Fepa per la mail " + mail.get_Id().toString(), e);
			throw new RedException(e);
		}
		
	}
	
}