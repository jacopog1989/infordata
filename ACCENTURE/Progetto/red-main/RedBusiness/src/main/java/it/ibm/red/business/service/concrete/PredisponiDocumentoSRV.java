package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.core.Document;

import it.ibm.red.business.dao.IContattoDAO;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dao.ITipologiaDocumentoDAO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AnagraficaDipendentiComponentDTO;
import it.ibm.red.business.dto.AttachmentDTO;
import it.ibm.red.business.dto.CapitoloSpesaMetadatoDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoPredisponiDocumentoDTO;
import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.LookupTableDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.NamedStreamDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.FilenetStatoFascicoloEnum;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.PredisponiDocumentoMessageEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;
import it.ibm.red.business.enums.TipoAllaccioEnum;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.enums.TipoDocumentoModeEnum;
import it.ibm.red.business.enums.TipoMetadatoEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.PkHandler;
import it.ibm.red.business.persistence.model.TipoProcedimento;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAttoDecretoUCBSRV;
import it.ibm.red.business.service.IDetailFascicoloSRV;
import it.ibm.red.business.service.IDocumentoRedSRV;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.IPredisponiDocumentoSRV;
import it.ibm.red.business.service.ITipologiaDocumentoSRV;
import it.ibm.red.business.service.facade.IDocumentoFacadeSRV;
import it.ibm.red.business.utils.FileUtils;


/**
 * @author m.crescentini
 *
 */
@Service
public class PredisponiDocumentoSRV extends AbstractService implements IPredisponiDocumentoSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 3900865051861280007L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(PredisponiDocumentoSRV.class);
	
	/**
	 * Service.
	 */	
	@Autowired
	private IDocumentoRedSRV documentoRedSRV;

	/**
	 * Service.
	 */	
	@Autowired
	private IFascicoloSRV fascicoloSRV;
	
	/**
	 * Service.
	 */	
	@Autowired
	private ITipologiaDocumentoSRV tipologiaDocumentoSRV;

	/**
	 * Service.
	 */	
	@Autowired
	private IDocumentoFacadeSRV documentoSRV;
	
	/**
	 * DAO.
	 */	
	@Autowired
	private ITipologiaDocumentoDAO tipologiaDocumentoDAO;

	/**
	 * DAO.
	 */	
	@Autowired
	private ITipoProcedimentoDAO tipoProcedimentoDAO;
	
	/**
	 * Service.
	 */	
	@Autowired
	private IAttoDecretoUCBSRV attoDecretoUCBSRV;
	
	/**
	 * Service.
	 */	
	@Autowired
	private RubricaSRV rubricaSRV;
	
	/**
	 * DAO.
	 */	
	@Autowired
	private IContattoDAO contattoDAO;
	
	/**
	 * Service.
	 */	
	@Autowired
	private IDetailFascicoloSRV detailFascicoloSRV;
	
	/**
	 * @see it.ibm.red.business.service.facade.IPredisponiDocumentoFacadeSRV#trasformaInRisposteAllaccio(java.util.List).
	 */
	@Override
	public List<RispostaAllaccioDTO> trasformaInRisposteAllaccio(final List<MasterDocumentRedDTO> masters) {
		final List<RispostaAllaccioDTO> allacci = new ArrayList<>();
		
		if (!CollectionUtils.isEmpty(masters)) {
			RispostaAllaccioDTO allaccio = null;
			for (final MasterDocumentRedDTO m : masters) {
				allaccio = trasformaInRispostaAllaccio(m);
				
				allacci.add(allaccio);
			}
		}
		
		return allacci;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IPredisponiDocumentoFacadeSRV#trasformaInRispostaAllaccio(it.ibm.red.business.dto.MasterDocumentRedDTO).
	 */
	@Override
	public RispostaAllaccioDTO trasformaInRispostaAllaccio(final MasterDocumentRedDTO m) {
		return trasformaInRispostaAllaccio(m.getGuuid(), m.getDocumentTitle(), m.getNumeroDocumento(), m.getIdTipologiaDocumento(), m.getIdTipoProcedimento(),
				m.getNumeroProtocollo(), m.getAnnoProtocollo(), m.getDataProtocollazione(), m.getTipoProtocollo(), m.getClasseDocumentale(), m.getWobNumber());
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IPredisponiDocumentoFacadeSRV#predisponiUscita(
	 *      it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.enums.ResponsesRedEnum, java.util.List,
	 *      it.ibm.red.business.dto.RispostaAllaccioDTO,
	 *      it.ibm.red.business.dto.FileDTO, java.lang.Integer,
	 *      java.util.Collection).
	 */
	@Override
	public EsitoPredisponiDocumentoDTO predisponiUscita(final UtenteDTO utente, final ResponsesRedEnum responseSollecitata, List<RispostaAllaccioDTO> docsDaAllacciareAllaccioList, 
			final RispostaAllaccioDTO docDaRibaltareAllaccio, final FileDTO contentUscita, final Integer idRegistroAusiliario, final Collection<MetadatoDTO> metadatiRegistroAusiliarioList, 
			final Collection<MetadatoDTO> metadatiDaTemplate) {
		final EsitoPredisponiDocumentoDTO esito = new EsitoPredisponiDocumentoDTO();
		Connection con = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			
			final DetailDocumentRedDTO detailDocResponse = documentoRedSRV.getDocumentDetail(
					docDaRibaltareAllaccio.getIdDocumentoAllacciato(), utente, docDaRibaltareAllaccio.getDocument().getWobNumberSelected(), 
					docDaRibaltareAllaccio.getDocument().getDocumentClass(), con);
			
			final DetailDocumentRedDTO docPredisposto = new DetailDocumentRedDTO();
			docPredisposto.setIdCategoriaDocumento(CategoriaDocumentoEnum.DOCUMENTO_USCITA.getIds()[0]); // Uscita
			docPredisposto.setOggetto(detailDocResponse.getOggetto()); // Oggetto ribaltato
			docPredisposto.setMomentoProtocollazioneEnum(MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD); // Momento protocollazione: Standard per default
			
			// Content (se passato in input)
			if (contentUscita != null) {
				docPredisposto.setContent(contentUscita.getContent());
				docPredisposto.setNomeFile(contentUscita.getFileName());
				docPredisposto.setMimeType(contentUscita.getMimeType());
				docPredisposto.setContentModificabile(false);
			}
			
			// Dati del registro ausiliario (se passati in input)
			docPredisposto.setIdRegistroAusiliario(idRegistroAusiliario);
			docPredisposto.setMetadatiRegistroAusiliario(metadatiRegistroAusiliarioList);
			
			// Fascicolo procedimentale -> START
			if (StringUtils.isNotBlank(detailDocResponse.getIndiceClassificazioneFascicoloProcedimentale())
					&& !FilenetStatoFascicoloEnum.NON_CATALOGATO.getNome().equals(detailDocResponse.getIndiceClassificazioneFascicoloProcedimentale())) {
				docPredisposto.setIndiceClassificazioneFascicoloProcedimentale(detailDocResponse.getIndiceClassificazioneFascicoloProcedimentale());
			}
			docPredisposto.setDescrizioneFascicoloProcedimentale(detailDocResponse.getDescrizioneFascicoloProcedimentale());
			docPredisposto.setPrefixFascicoloProcedimentale(detailDocResponse.getPrefixFascicoloProcedimentale());
			docPredisposto.setIdFascicoloProcedimentale(detailDocResponse.getIdFascicoloProcedimentale());
			// Fascicolo procedimentale -> END
			
			// Allacci/Fascicoli -> START
			// Il documento da ribaltare deve essere sempre presente tra gli allacci del documento che si sta predisponendo
			boolean docAllaccioDaRibaltareTrovato = false;
			
			if (docsDaAllacciareAllaccioList != null) {
				for (final RispostaAllaccioDTO ra : docsDaAllacciareAllaccioList) {
					if (detailDocResponse.getDocumentTitle().equals(ra.getIdDocumentoAllacciato())) {
						docAllaccioDaRibaltareTrovato = true;
						break;
					}
				}
			} else {
				docsDaAllacciareAllaccioList = new ArrayList<>();
			}
			
			if (!docAllaccioDaRibaltareTrovato) {
				docsDaAllacciareAllaccioList.add(docDaRibaltareAllaccio);
			}
			
			// Se la response sollecitata è Predisponi Restituzione, agli allacci deve essere aggiunto il "protocollo riferimento" del documento che si sta ribaltando.
			// Il protocollo riferimento è il documento in entrata di cui si sta predisponendo la restituzione, mentre il documento che si sta ribaltando è quello 
			// in entrata di "Ritiro in autotutela".
			RispostaAllaccioDTO allaccioProtocolloRiferimento = null;
			if (ResponsesRedEnum.PREDISPONI_RESTITUZIONE.equals(responseSollecitata)) {
				allaccioProtocolloRiferimento = getAllaccioProtocolloRiferimento(detailDocResponse.getProtocolloRiferimento(), utente, con);
				docsDaAllacciareAllaccioList.add(allaccioProtocolloRiferimento);
			}
			
			final List<RispostaAllaccioDTO> orderedAllacci = new ArrayList<>();
			final List<FascicoloDTO> fascicoliUscita = new ArrayList<>();
			FascicoloDTO fascicoloProcedimentaleIngresso = null;
			List<FaldoneDTO> faldoniFascicoloProcedimentale = null;
			// Per ogni documento da allacciare, si carica il fascicolo procedimentale dell'allaccio per inserirlo tra i fascicoli del documento che si sta predisponendo
			for (final RispostaAllaccioDTO ra : docsDaAllacciareAllaccioList) {
				// Si verifica se l'allaccio è quello del documento da ribaltare
				if (detailDocResponse.getDocumentTitle().equals(ra.getIdDocumentoAllacciato())) {
					
					// L'allaccio del documento da ribaltare è selezionato come da chiudere e NON rimuovibile dalla lista degli allacci (disabled = true)
					ra.setDocumentoDaChiudere(true);
					ra.setDisabled(true);
					
					// Nel caso di Predisponi Restituzione, l'allaccio principale è quello del "protocollo riferimento" anziche' il documento da ribaltare (cfr. righe 214-216)
					if (!ResponsesRedEnum.PREDISPONI_RESTITUZIONE.equals(responseSollecitata)) {
						ra.setPrincipale(true); // Il documento da ribaltare diventa l'allaccio principale
					}
					
					// Se si tratta di uno dei Predisponi per UCB, la chiusura dell'allaccio del documento da ribaltare è obbligatoria (cfr. riga 227)
					if (ResponsesRedEnum.RISPONDI_INGRESSO.equals(responseSollecitata) || ResponsesRedEnum.INOLTRA_INGRESSO.equals(responseSollecitata)
							|| ResponsesRedEnum.PREDISPONI_VISTO.equals(responseSollecitata) || ResponsesRedEnum.PREDISPONI_OSSERVAZIONE.equals(responseSollecitata)
							|| ResponsesRedEnum.RICHIESTA_INTEGRAZIONI.equals(responseSollecitata) || ResponsesRedEnum.PREDISPONI_RESTITUZIONE.equals(responseSollecitata)
							|| ResponsesRedEnum.PREDISPONI_RELAZIONE_POSITIVA.equals(responseSollecitata) || ResponsesRedEnum.PREDISPONI_RELAZIONE_NEGATIVA.equals(responseSollecitata)) {
						ra.setChiusuraDisabled(true);
						if (ResponsesRedEnum.RICHIESTA_INTEGRAZIONI.equals(responseSollecitata)) {
							ra.setDocumentoDaChiudere(false);
						}
					}
					
					// Si prende il suo fascicolo procedimentale e lo si imposta nell'allaccio
					for (final FascicoloDTO f : detailDocResponse.getFascicoli()) {
						if (f.getIdFascicolo().equals(detailDocResponse.getIdFascicoloProcedimentale())) {
							ra.setFascicolo(f);
							fascicoliUscita.add(f);
							fascicoloProcedimentaleIngresso = f;
							break;
						}
					}
					
					orderedAllacci.add(0, ra); // Si inserisce come primo allaccio della lista
				
				// Altrimenti, il fascicolo dell'allaccio deve essere recuperato da FileNet
				} else {
					final FascicoloDTO f = fascicoloSRV.getFascicoloProcedimentale(ra.getIdDocumentoAllacciato(), detailDocResponse.getAoo().getIdAoo().intValue(), utente, con); 
					ra.setFascicolo(f);
					fascicoliUscita.add(f);
					fascicoloProcedimentaleIngresso = f;
					orderedAllacci.add(ra);
				}
				
				//prepopolamento eventuale faldone del fascicolo procedimentale dell'ingresso
				if (fascicoloProcedimentaleIngresso != null) {
					faldoniFascicoloProcedimentale = detailFascicoloSRV.getFaldoniByIdFascicolo(fascicoloProcedimentaleIngresso.getIdFascicolo(), utente.getIdAoo(), utente);
				}
				
				// Tipo allaccio
				TipoAllaccioEnum tipoAllaccio = null;
				switch (responseSollecitata) {
				case RISPONDI_INGRESSO:
					tipoAllaccio = TipoAllaccioEnum.RISPOSTA;
					break;
				case INOLTRA_INGRESSO:
					tipoAllaccio = TipoAllaccioEnum.INOLTRO;
					break;
				case PREDISPONI_VISTO:
					tipoAllaccio = TipoAllaccioEnum.VISTO;
					break;
				case PREDISPONI_OSSERVAZIONE:
					tipoAllaccio = TipoAllaccioEnum.OSSERVAZIONE;
					break;
				case RICHIESTA_INTEGRAZIONI:
					tipoAllaccio = TipoAllaccioEnum.RICHIESTA_INTEGRAZIONE;
					break;
				case PREDISPONI_RESTITUZIONE:
					tipoAllaccio = TipoAllaccioEnum.RESTITUZIONE;
					break;
				case PREDISPONI_RELAZIONE_POSITIVA:
					tipoAllaccio = TipoAllaccioEnum.RELAZIONE_POSITIVA;
					break;
				case PREDISPONI_RELAZIONE_NEGATIVA:
					tipoAllaccio = TipoAllaccioEnum.RELAZIONE_NEGATIVA;
					break;
				default:
					tipoAllaccio = TipoAllaccioEnum.RISPOSTA;
					break;
				}
				ra.setTipoAllaccioEnum(tipoAllaccio);
				ra.setIdTipoAllaccio(tipoAllaccio.getTipoAllaccioId());
			}
			
			docPredisposto.setAllacci(orderedAllacci); // Allacci
			docPredisposto.setFascicoli(fascicoliUscita); // Fascicoli
			docPredisposto.setFaldoni(faldoniFascicoloProcedimentale);
			// Allacci/Fascicoli -> END
			
			boolean isFlussoUCB = false;
			
			// ### Dati del documento che si sta predisponendo dipendenti dalla response sollecitata -> START
			switch (responseSollecitata) {
			case RISPONDI_INGRESSO:
				
				handleRispostaIngresso(utente, con, detailDocResponse, docPredisposto);
				break;
				
			case INOLTRA_INGRESSO:
				
				// Sotto categoria
				handleInoltroIngresso(utente, esito, con, detailDocResponse, docPredisposto);
				break;
				
			case PREDISPONI_VISTO:
				
				handlePredisposizioneVisto(utente, idRegistroAusiliario, metadatiDaTemplate, con, detailDocResponse, docPredisposto);
				break;
				
			case PREDISPONI_OSSERVAZIONE:
				
				handlePredisposizioneOsservazione(utente, idRegistroAusiliario, metadatiDaTemplate, con, detailDocResponse, docPredisposto);
				break;
				
			case RICHIESTA_INTEGRAZIONI:
				
				handleRichiestaIntegrazioni(utente, metadatiDaTemplate, con, detailDocResponse, docPredisposto);
				break;
			
			case PREDISPONI_RESTITUZIONE:
				
				handlePredisposizioneRestituzione(utente, metadatiDaTemplate, esito, con, detailDocResponse, docPredisposto, allaccioProtocolloRiferimento);
				break;
				
			case PREDISPONI_RELAZIONE_POSITIVA:
				
				handleRelazione(utente, metadatiDaTemplate, con, detailDocResponse, docPredisposto, SottoCategoriaDocumentoUscitaEnum.RELAZIONE_POSITIVA);
				break;
				
			case PREDISPONI_RELAZIONE_NEGATIVA:
				
				handleRelazione(utente, metadatiDaTemplate, con, detailDocResponse, docPredisposto, SottoCategoriaDocumentoUscitaEnum.RELAZIONE_NEGATIVA);
				break;
			
			default:
				// Destinatari (ribaltamento mittente -> destinatario)
				ribaltaMittenteInDestinatari(detailDocResponse, docPredisposto, isFlussoUCB);
				break;
			}
			// ### Dati del documento che si sta predisponendo dipendenti dalla response sollecitata -> END
			
			esito.setDocPredisposto(docPredisposto);
			esito.setEsito(true);
			
		} catch (final Exception e) {
			LOGGER.error("Errore nella predisposizione del documento in uscita", e);
			esito.setCodiceErrore(PredisponiDocumentoMessageEnum.ERRORE_GENERICO);
			esito.setNote(PredisponiDocumentoMessageEnum.ERRORE_GENERICO.getMessage());
		} finally {
			closeConnection(con);
		}
		
		LOGGER.info("predisponiUscita -> END");
		return esito;
	}

	/**
	 * Gestisce il ribaltamento delle informazioni dal documento in entrata e dal
	 * template della relazione positiva o della relazione negativa generata sul
	 * documento in fase di predisposizione.
	 * 
	 * @param utente
	 *            Utente in sessione.
	 * @param metadatiDaTemplate
	 *            Metadati recuperati dal template del visto predisposto.
	 * @param con
	 *            Connessione al database.
	 * @param detailDocResponse
	 *            Dettaglio del documento in entrata.
	 * @param docPredisposto
	 *            Documento in fase di predisposizione da aggiornare.
	 * @param categoria
	 *            Sottocategoria da gestire, può essere una tra:
	 *            <ul>
 *            			<li>RELAZIONE_POSITIVA</li>
	 *            		<li>RELAZIONE_NEGATIVA</li>
	 *            </ul>
	 */
	private void handleRelazione(final UtenteDTO utente, final Collection<MetadatoDTO> metadatiDaTemplate,
			Connection con, final DetailDocumentRedDTO detailDocResponse, final DetailDocumentRedDTO docPredisposto, SottoCategoriaDocumentoUscitaEnum categoria) {
		
		boolean isFlussoUCB = false;
		
		// Sotto categoria
		docPredisposto.setSottoCategoriaUscita(categoria);
		
		if (StringUtils.isNotBlank(detailDocResponse.getCodiceFlusso()) && utente.isUcb()) {
			isFlussoUCB = true;
		}
		
		// Destinatari (ribaltamento mittente -> destinatario)
		ribaltaMittenteInDestinatari(detailDocResponse, docPredisposto, isFlussoUCB);
		
		// Tipologia documento e tipo procedimento
		ribaltaTipologiaDocumentoTipoProcedimentoByDescsEntrata(docPredisposto, detailDocResponse.getDescTipologiaDocumento(), 
				detailDocResponse.getDescTipoProcedimento(), utente.getIdAoo(), con);
		
		// Metadati estesi
		ribaltaMetadatiEstesi(docPredisposto, detailDocResponse);
		
		// Si ribaltano i metadati estesi recuperati dal template
		ribaltaMetadatiDaTemplate(metadatiDaTemplate, docPredisposto.getMetadatiEstesi());
	}

	/**
	 * Gestisce il ribaltamento delle informazioni dal documento in entrata e dal
	 * template della restituzione generata sul documento in fase di
	 * predisposizione.
	 * 
	 * @param utente
	 *            Utente in sessione.
	 * @param metadatiDaTemplate
	 *            Metadati recuperati dal template del visto predisposto.
	 * @param esito
	 *            Esito predispozione del documento.
	 * @param con
	 *            Connessione al database.
	 * @param detailDocResponse
	 *            Dettaglio del documento in entrata.
	 * @param docPredisposto
	 *            Documento in fase di predisposizione da aggiornare.
	 * @param allaccioProtocolloRiferimento
	 *            Protocollo di riferimento a cui si riferisce la restituzione.
	 */
	private void handlePredisposizioneRestituzione(final UtenteDTO utente, final Collection<MetadatoDTO> metadatiDaTemplate, final EsitoPredisponiDocumentoDTO esito,
			Connection con, final DetailDocumentRedDTO detailDocResponse, final DetailDocumentRedDTO docPredisposto, RispostaAllaccioDTO allaccioProtocolloRiferimento) {
		
		boolean isFlussoUCB = false;
		
		// Sotto categoria
		docPredisposto.setSottoCategoriaUscita(SottoCategoriaDocumentoUscitaEnum.RESTITUZIONE);
		
		if (StringUtils.isNotBlank(allaccioProtocolloRiferimento.getDocument().getCodiceFlusso()) && utente.isUcb()) {
			isFlussoUCB = true;
		}
		
		
		// ###### N.B. Nel caso di Predisponi Restituzione, devono essere ribaltati i dati del protocollo riferimento
		// associato al documento di "Ritiro in autotutela" che si sta ribaltando!
		
		if (StringUtils.isNotBlank(allaccioProtocolloRiferimento.getDocument().getCodiceFlusso()) && utente.isUcb()) {
			isFlussoUCB = true;
		}
		
		// Destinatari (ribaltamento mittente -> destinatario)
		ribaltaMittenteInDestinatari(detailDocResponse, docPredisposto, isFlussoUCB);
		
		// Si ribaltano tipologia documento e tipo procedimento del protocollo riferimento
		ribaltaTipologiaDocumentoTipoProcedimentoByIdsEntrata(docPredisposto, allaccioProtocolloRiferimento.getDocument().getIdTipologiaDocumento(), 
				allaccioProtocolloRiferimento.getDocument().getIdTipologiaProcedimento(), utente.getIdAoo(), con);
		
		// Si ribaltano i metadati estesi  
		ribaltaMetadatiEstesi(docPredisposto, allaccioProtocolloRiferimento.getDocument());
		// Si ribaltano i metadati estesi recuperati dal template
		ribaltaMetadatiDaTemplate(metadatiDaTemplate, docPredisposto.getMetadatiEstesi());
		
		// Si ribaltano i content (principale e allegati) del protocollo riferimento come allegati del documento che si sta predisponendo
		ribaltaSuAllegati(esito, docPredisposto, allaccioProtocolloRiferimento.getDocument().getDocumentTitle(), 
				allaccioProtocolloRiferimento.getDocument().getOggetto(), 
				allaccioProtocolloRiferimento.getDocument().getAllegati(), utente, con, false);
	}

	/**
	 * Gestisce il ribaltamento delle informazioni dal documento in entrata e dal
	 * template della richiesta integrazione generata sul documento in fase di
	 * predisposizione.
	 * 
	 * @param utente
	 *            Utente in sessione.
	 * @param metadatiDaTemplate
	 *            Metadati recuperati dal template del visto predisposto.
	 * @param con
	 *            Connessione al database.
	 * @param detailDocResponse
	 *            Dettaglio del documento in entrata.
	 * @param docPredisposto
	 *            Documento in fase di predisposizione da aggiornare.
	 */
	private void handleRichiestaIntegrazioni(final UtenteDTO utente, final Collection<MetadatoDTO> metadatiDaTemplate,
			Connection con, final DetailDocumentRedDTO detailDocResponse, final DetailDocumentRedDTO docPredisposto) {

		boolean isFlussoUCB = false;
		
		// Sotto categoria
		docPredisposto.setSottoCategoriaUscita(SottoCategoriaDocumentoUscitaEnum.RICHIESTA_INTEGRAZIONI);
		
		if (StringUtils.isNotBlank(detailDocResponse.getCodiceFlusso()) && utente.isUcb()) {
			isFlussoUCB = true;
		}
		
		// Destinatari (ribaltamento mittente -> destinatario)
		ribaltaMittenteInDestinatari(detailDocResponse, docPredisposto, isFlussoUCB);
		
		// Tipologia documento e tipo procedimento
		impostaTipologiaDocumentoTipoProcedimentoByProperties(docPredisposto, 
				PropertiesNameEnum.ID_TIPOLOGIA_DOCUMENTO_USCITA_RICHIESTA_INTEGRAZIONI, utente.getCodiceAoo(), con);
		
		//carica metadati estesi
		docPredisposto.setMetadatiEstesi(tipologiaDocumentoSRV.caricaMetadati(docPredisposto.getIdTipologiaDocumento(), 
				docPredisposto.getIdTipologiaProcedimento(), null, utente.getIdAoo()));
		
		// Si ribaltano i metadati estesi recuperati dal template
		ribaltaMetadatiDaTemplate(metadatiDaTemplate, docPredisposto.getMetadatiEstesi());

		for (final MetadatoDTO metadatoDoc : docPredisposto.getMetadatiEstesi()) {
			if (PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NOME_TIPO_DOC_RIFERIMENTO_METADATO_ESTESO_NAME)
					.equals(metadatoDoc.getName())) {
				metadatoDoc.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
				metadatoDoc.setEditability(TipoDocumentoModeEnum.MAI);
				metadatoDoc.setSelectedValue(detailDocResponse.getDescTipologiaDocumento());
			}
			
			if (PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METADATO_ESTESO_NAME)
					.equals(metadatoDoc.getName())) {
				metadatoDoc.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
				metadatoDoc.setEditability(TipoDocumentoModeEnum.MAI);
				metadatoDoc.setSelectedValue(detailDocResponse.getAnnoProtocollo());
			}
			
			if (PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METADATO_ESTESO_NAME)
					.equals(metadatoDoc.getName())) {
				metadatoDoc.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
				metadatoDoc.setEditability(TipoDocumentoModeEnum.MAI);
				metadatoDoc.setSelectedValue(detailDocResponse.getNumeroProtocollo());
			}
		}
	}

	/**
	 * Gestisce il ribaltamento delle informazioni dal documento in entrata e dal
	 * template dell'osservazione generata sul documento in fase di predisposizione.
	 * 
	 * @param utente
	 *            Utente in sessione.
	 * @param idRegistroAusiliario
	 *            Identificativo del registro ausiliario.
	 * @param metadatiDaTemplate
	 *            Metadati recuperati dal template del visto predisposto.
	 * @param con
	 *            Connessione al database.
	 * @param detailDocResponse
	 *            Dettaglio del documento in entrata.
	 * @param docPredisposto
	 *            Documento in fase di predisposizione da aggiornare.
	 */
	private void handlePredisposizioneOsservazione(final UtenteDTO utente, final Integer idRegistroAusiliario,
			final Collection<MetadatoDTO> metadatiDaTemplate, Connection con,
			final DetailDocumentRedDTO detailDocResponse, final DetailDocumentRedDTO docPredisposto) {
		
		boolean isFlussoUCB = false;
		
		// Sotto categoria
		docPredisposto.setSottoCategoriaUscita(SottoCategoriaDocumentoUscitaEnum.OSSERVAZIONE);
		
		if (StringUtils.isNotBlank(detailDocResponse.getCodiceFlusso()) && utente.isUcb()) {
			isFlussoUCB = true;
		}
		
		
		// Destinatari (ribaltamento mittente -> destinatario)
		ribaltaMittenteInDestinatari(detailDocResponse, docPredisposto, isFlussoUCB);
		
		// Tipologia documento e tipo procedimento
		impostaTipologiaDocumentoTipoProcedimentoByPropertiesERegistroAusiliario(docPredisposto,
				PropertiesNameEnum.ID_TIPOLOGIA_DOCUMENTO_USCITA_PREDISPONI_OSSERVAZIONE, idRegistroAusiliario, utente.getCodiceAoo(), con);
		
		docPredisposto.setMetadatiEstesi(tipologiaDocumentoSRV.caricaMetadati(docPredisposto.getIdTipologiaDocumento(), 
				docPredisposto.getIdTipologiaProcedimento(), null, utente.getIdAoo()));
		
		// Si ribaltano i metadati estesi recuperati dal template
		ribaltaMetadatiDaTemplate(metadatiDaTemplate, docPredisposto.getMetadatiEstesi());
		
		for (final MetadatoDTO metadatoDoc : docPredisposto.getMetadatiEstesi()) {
			if (PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NOME_TIPO_DOC_RIFERIMENTO_METADATO_ESTESO_NAME)
					.equals(metadatoDoc.getName())) {
				metadatoDoc.setObligatoriness(TipoDocumentoModeEnum.SEMPRE);
				metadatoDoc.setEditability(TipoDocumentoModeEnum.MAI);
				metadatoDoc.setSelectedValue(detailDocResponse.getDescTipologiaDocumento());
			}
		}
	}

	/**
	 * Gestisce il ribaltamento delle informazioni dal documento in entrata e dal
	 * template del visto sul documento in fase di predisposizione.
	 * 
	 * @param utente
	 *            Utente in sessione.
	 * @param idRegistroAusiliario
	 *            Identificativo del registro ausiliario.
	 * @param metadatiDaTemplate
	 *            Metadati recuperati dal template del visto predisposto.
	 * @param con
	 *            Connessione al database.
	 * @param detailDocResponse
	 *            Dettaglio del documento in entrata.
	 * @param docPredisposto
	 *            Documento in fase di predisposizione da aggiornare.
	 */
	private void handlePredisposizioneVisto(final UtenteDTO utente, final Integer idRegistroAusiliario, final Collection<MetadatoDTO> metadatiDaTemplate,
			Connection con, final DetailDocumentRedDTO detailDocResponse, final DetailDocumentRedDTO docPredisposto) {
		boolean isFlussoUCB = false;

		// Sotto categoria
		docPredisposto.setSottoCategoriaUscita(SottoCategoriaDocumentoUscitaEnum.VISTO);
		
		if (StringUtils.isNotBlank(detailDocResponse.getCodiceFlusso()) && utente.isUcb()) {
			isFlussoUCB = true;
		}
		
		// Destinatari (ribaltamento mittente -> destinatario)
		ribaltaMittenteInDestinatari(detailDocResponse, docPredisposto, isFlussoUCB);
		
		// Tipologia documento e tipo procedimento
		ribaltaTipologiaDocumentoTipoProcedimentoByDescsEntrata(docPredisposto, detailDocResponse.getDescTipologiaDocumento(), 
				detailDocResponse.getDescTipoProcedimento(), utente.getIdAoo(), con);
		
		// Se il visto che prevede Corte dei Conti come destinatario (es. VISTO_EX_ART_6)
		final String idsString = PropertiesProvider.getIstance().getParameterByString(
				utente.getCodiceAoo() + "." + (PropertiesNameEnum.IDS_REGISTRI_DEST_CORTE_DEI_CONTI).getKey());
		if (idsString != null && !"".equals(idsString)) {
			final String[] ids = StringUtils.split(idsString, ";");
			if ((Arrays.asList(ids)).contains(idRegistroAusiliario.toString())) {
				// Aggiungo la corte dei conti come destinatario in CC
				final String idContattoCorteDeiConti = PropertiesProvider.getIstance().getParameterByString(
						utente.getCodiceAoo() + "." + PropertiesNameEnum.ID_CONTATTO_CORTE_DEI_CONTI.getKey());
				Contatto corteDeiConti = null;
				DestinatarioRedDTO destinazioneCC = null;
				
				if (!StringUtils.isEmpty(idContattoCorteDeiConti)) {
					corteDeiConti = rubricaSRV.getContattoByID(Long.parseLong(idContattoCorteDeiConti));
				}
				
				if (corteDeiConti != null) {
					destinazioneCC = new DestinatarioRedDTO(corteDeiConti.getContattoID(), MezzoSpedizioneEnum.ELETTRONICO, ModalitaDestinatarioEnum.CC);
					destinazioneCC.setContatto(corteDeiConti);
				}
				
				if (destinazioneCC != null) {
					if (!CollectionUtils.isEmpty(docPredisposto.getDestinatari())) {
						docPredisposto.getDestinatari().add(destinazioneCC);
					} else {
						docPredisposto.setDestinatari(new ArrayList<>());
						docPredisposto.getDestinatari().add(destinazioneCC);
					}
					
					if (!CollectionUtils.isEmpty(docPredisposto.getDestinatariCC())) {
						docPredisposto.getDestinatariCC().add(destinazioneCC.getContatto().getMail());
					} else {
						docPredisposto.setDestinatariCC(new ArrayList<>());
						docPredisposto.getDestinatariCC().add(destinazioneCC.getContatto().getMail());
					}
				}
			}
		}
		
		// Metadati estesi
		ribaltaMetadatiEstesi(docPredisposto, detailDocResponse);
		
		// Si ribaltano i metadati estesi recuperati dal template
		ribaltaMetadatiDaTemplate(metadatiDaTemplate, docPredisposto.getMetadatiEstesi());
	}

	/**
	 * Imposta la sottocategoria del documento e ne ribalta tipologia documento,
	 * tipologia procedimento e metadati estesi. Aggiorna i content (principale e
	 * allegati) del documento che si sta predisponendo impostandoli come allegati.
	 * Gestisce eventualemente un documento di tipo <em> ATTO DECRETO </em>.
	 * 
	 * @param utente
	 *            Utente in sessione.
	 * @param esito
	 *            Esito della predisposizione.
	 * @param con
	 *            Connessione al database.
	 * @param detailDocResponse
	 *            Dettaglio del documento sulla quale viene eseguita la response.
	 * @param docPredisposto
	 *            Dettaglio del documento che viene predisposto.
	 */
	private void handleInoltroIngresso(final UtenteDTO utente, final EsitoPredisponiDocumentoDTO esito, Connection con,
			final DetailDocumentRedDTO detailDocResponse, final DetailDocumentRedDTO docPredisposto) {
		docPredisposto.setSottoCategoriaUscita(SottoCategoriaDocumentoUscitaEnum.INOLTRO);
		
		// Tipologia documento e tipo procedimento
		ribaltaTipologiaDocumentoTipoProcedimentoByDescsEntrata(docPredisposto, detailDocResponse.getDescTipologiaDocumento(), 
				detailDocResponse.getDescTipoProcedimento(), utente.getIdAoo(), con);
		
		final boolean isAttoDecretoEntrataUCB = isAttoDecretoEntrataUCB(utente, detailDocResponse);
		
		setDestinatarioAttoDecreto(docPredisposto, con, isAttoDecretoEntrataUCB);
						
		// Metadati estesi
		ribaltaMetadatiEstesi(docPredisposto, detailDocResponse);
		
		// Si ribaltano i content (principale e allegati) come allegati del documento che si sta predisponendo
		ribaltaSuAllegati(esito, docPredisposto, detailDocResponse.getDocumentTitle(), detailDocResponse.getOggetto(), 
				detailDocResponse.getAllegati(), utente, con, isAttoDecretoEntrataUCB);
	}

	/**
	 * Imposta la sottocategoria del documento ribaltandone mittenti in destinatari
	 * e le informazioni associate alla tipologia documento, alla tipologia
	 * procedimentale e ai metadati estesi.
	 * 
	 * @param utente
	 *            Utente in sessione.
	 * @param con
	 *            Connessione al database.
	 * @param detailDocResponse
	 *            Dettaglio del documento.
	 * @param docPredisposto
	 *            Documento predisposto.
	 */
	private void handleRispostaIngresso(final UtenteDTO utente, Connection con,
			final DetailDocumentRedDTO detailDocResponse, final DetailDocumentRedDTO docPredisposto) {
		
		boolean isFlussoUCB = false;
		
		// Sotto categoria
		docPredisposto.setSottoCategoriaUscita(SottoCategoriaDocumentoUscitaEnum.RISPOSTA);
		
		if (StringUtils.isNotBlank(detailDocResponse.getCodiceFlusso()) && utente.isUcb()) {
			isFlussoUCB = true;
		}
		
		// Destinatari (ribaltamento mittente -> destinatario)
		ribaltaMittenteInDestinatari(detailDocResponse, docPredisposto, isFlussoUCB);
		
		// Tipologia documento e tipo procedimento
		ribaltaTipologiaDocumentoTipoProcedimentoByDescsEntrata(docPredisposto, detailDocResponse.getDescTipologiaDocumento(), 
				detailDocResponse.getDescTipoProcedimento(), utente.getIdAoo(), con);
		
		// Metadati estesi
		ribaltaMetadatiEstesi(docPredisposto, detailDocResponse);
	}
	
	private static boolean isAttoDecretoEntrataUCB(final UtenteDTO utente, final DetailDocumentRedDTO detailDocResponse) {
		// Recupero dell'id che caratterizza il tipo documento Atto decreto.
		final String tipoAttoDecretoUCBEntrata = PropertiesProvider.getIstance().getParameterByString(
				utente.getCodiceAoo() + "." + PropertiesNameEnum.ID_TIPOLOGIA_DOCUMENTO_ENTRATA_ATTO_DECRETO.getKey());

		return utente.isUcb() && tipoAttoDecretoUCBEntrata != null && tipoAttoDecretoUCBEntrata.equals(detailDocResponse.getIdTipologiaDocumento().toString());
	}


	/**
	 * @param guid
	 * @param documentTitle
	 * @param numeroDocumento
	 * @param idTipologiaDocumento
	 * @param numeroProtocollo
	 * @param annoProtocollo
	 * @param dataProtocollo
	 * @param tipoProtocollo
	 * @param classeDocumentale
	 * @param wobNumber
	 * @return
	 */
	private static RispostaAllaccioDTO trasformaInRispostaAllaccio(final String guid, final String documentTitle, final Integer numeroDocumento, final Integer idTipologiaDocumento, final Integer idTipologiaProcedimento, 
			final Integer numeroProtocollo, final Integer annoProtocollo, final Date dataProtocollo, final Integer tipoProtocollo, final String classeDocumentale, final String wobNumber) {
		final RispostaAllaccioDTO allaccio = new RispostaAllaccioDTO();
		
		if (numeroDocumento != null) {
			allaccio.setNumDocumento(numeroDocumento);
		}
		
		if (numeroProtocollo != null) {
			allaccio.setNumeroProtocollo(numeroProtocollo);
		} else {
			allaccio.setNumeroProtocollo(0);
		} 
		if (annoProtocollo != null) {
			allaccio.setAnnoProtocollo(annoProtocollo);
		}
		
		allaccio.setIdDocumentoAllacciato(documentTitle);
		allaccio.setVerificato(true);
		
		// Documento a cui fa riferimento l'allaccio -> START
		final DetailDocumentRedDTO d = new DetailDocumentRedDTO();
		d.setGuid(guid);
		d.setDocumentTitle(documentTitle);
		if (numeroProtocollo != null) {
			d.setNumeroProtocollo(numeroProtocollo);
		} else {
			d.setNumeroProtocollo(0);
		}
		if (annoProtocollo != null) {
			d.setAnnoProtocollo(annoProtocollo);
		}
		if (tipoProtocollo != null) {					
		 	d.setTipoProtocollo(tipoProtocollo);
		}
		if (dataProtocollo != null) {
			d.setDataProtocollo(dataProtocollo);
		}
		d.setDocumentClass(classeDocumentale);
		d.setWobNumberSelected(wobNumber);
		d.setIdTipologiaDocumento(idTipologiaDocumento);
		d.setIdTipologiaProcedimento(idTipologiaProcedimento);
		
		allaccio.setDocument(d);
		// Documento a cui fa riferimento l'allaccio -> END
		
		return allaccio;
	}

	/**
	 * @param detailDocResponse
	 * @param docPredisposto
	 */
	private void ribaltaMittenteInDestinatari(final DetailDocumentRedDTO detailDocResponse, final DetailDocumentRedDTO docPredisposto, final boolean isFlussoUCB) {
		docPredisposto.setDestinatari(new ArrayList<>());
		
		if (detailDocResponse.getMittenteContatto() != null) {
			final Contatto mittenteContatto = detailDocResponse.getMittenteContatto();
			
			MezzoSpedizioneEnum mezzoSpedizione = MezzoSpedizioneEnum.ELETTRONICO;
			mittenteContatto.setCampoEmailVisible(true); 
			if (!isFlussoUCB && StringUtils.isBlank(mittenteContatto.getMail()) && StringUtils.isBlank(mittenteContatto.getMailPec())) {
				mezzoSpedizione = MezzoSpedizioneEnum.CARTACEO;
				mittenteContatto.setCampoEmailVisible(false);
			}
			
			final DestinatarioRedDTO destRibaltato = new DestinatarioRedDTO(
					mezzoSpedizione.getId(), TipologiaDestinatarioEnum.ESTERNO.getTipologia(), ModalitaDestinatarioEnum.TO.name());
			
			if (StringUtils.isNotBlank(detailDocResponse.getMittenteMailProt())) {
				if (StringUtils.isNotBlank(mittenteContatto.getMailPec()) && detailDocResponse.getMittenteMailProt().equals(mittenteContatto.getMailPec())) {
					mittenteContatto.setMailSelected(mittenteContatto.getMailPec());
				} else if (StringUtils.isNotBlank(mittenteContatto.getMail()) && detailDocResponse.getMittenteMailProt().equals(mittenteContatto.getMail())) {
					mittenteContatto.setMailSelected(mittenteContatto.getMail());
				}
			} else {
				if (StringUtils.isNotBlank(mittenteContatto.getMailPec())) {
					mittenteContatto.setMailSelected(mittenteContatto.getMailPec());
				} else {
					mittenteContatto.setMailSelected(mittenteContatto.getMail());
				}
			}
			destRibaltato.setContatto(mittenteContatto);
			
			docPredisposto.getDestinatari().add(destRibaltato);
		}
	}
	
	/**
	 * @param docPredisposto
	 */
	private void setDestinatarioAttoDecreto(final DetailDocumentRedDTO docPredisposto, final Connection connection, final boolean isAttoDecretoEntrataUCB) {
		docPredisposto.setDestinatari(new ArrayList<>());
		
		if (isAttoDecretoEntrataUCB) {
			
			// Recupero l'id del contatto mail configurato.
			final Long idIndirizzoMailConfig = Long.valueOf(PropertiesProvider.getIstance().getParameterByString(PropertiesNameEnum.ID_CONTATTO_MAIL_ATTO_DECRETO_USCITA_UCB.getKey()));

			final Contatto preConfigContatto = contattoDAO.getContattoByID(idIndirizzoMailConfig, connection);
			preConfigContatto.setDisabilitaElemento(true);
			
			final Integer idMezzoSpedizione = MezzoSpedizioneEnum.ELETTRONICO.getId();
			final String tipoDest = TipologiaDestinatarioEnum.ESTERNO.getTipologia();
			final String modalitaDest = ModalitaDestinatarioEnum.TO.name();
			
			final DestinatarioRedDTO destRibaltato = new DestinatarioRedDTO(idMezzoSpedizione, tipoDest, modalitaDest);
			destRibaltato.setContatto(preConfigContatto);
			destRibaltato.setMezzoSpedizioneDisabled(true);
			destRibaltato.setNonModificabile(true);
			
			docPredisposto.getDestinatari().add(destRibaltato);
		}
	}
	
	/**
	 * @param esitoPredisponi
	 * @param docPredisposto
	 * @param documentTitleDocResponse
	 * @param oggettoDocResponse
	 * @param allegatiDaRibaltare
	 * @param utente
	 * @param con
	 * @param isAttoDecretoEntrataUCB
	 */
	private void ribaltaSuAllegati(final EsitoPredisponiDocumentoDTO esitoPredisponi, final DetailDocumentRedDTO docPredisposto, final String documentTitleDocResponse, 
			final String oggettoDocResponse, List<AllegatoDTO> allegatiDaRibaltare, final UtenteDTO utente, final Connection con,
			final boolean isAttoDecretoEntrataUCB) {
		final List<AllegatoDTO> allegati = new ArrayList<>();
		Collection<String> nomiFileAllegatiNonValidi = null;

		final List<TipologiaDocumentoDTO> tipiDocAttive = tipologiaDocumentoSRV.getComboTipologieByAooAndTipoCategoriaAttivi(TipoCategoriaEnum.USCITA, 
				utente.getIdAoo(), con);
				
		final FileDTO contentPrincipale = documentoSRV.getDocumentoContentPreview(utente.getFcDTO(), documentTitleDocResponse, false, utente.getIdAoo());

		// Si costruisce il primo allegato a partire dal principale del documento da ribaltare
		// Se il content principale è firmato, il suo formato deve essere 'Firmato Digitalmente'
		final boolean isP7M = FileUtils.isP7MFile(contentPrincipale.getMimeType());
		final FormatoAllegatoEnum formatoPrincipaleAsAllegato = 
				(isP7M || PdfHelper.getSignatureNumber(contentPrincipale.getContent()) > 0) ? FormatoAllegatoEnum.FIRMATO_DIGITALMENTE : FormatoAllegatoEnum.ELETTRONICO;

		final AllegatoDTO principaleAsAllegato = new AllegatoDTO(null, formatoPrincipaleAsAllegato.getId(), docPredisposto.getIdTipologiaDocumento(), null,
				oggettoDocResponse, null, false, false, null, null, contentPrincipale.getFileName(), contentPrincipale.getMimeType(), null, 
				contentPrincipale.getContent(), null, null, null, null, false, tipiDocAttive, false, true);
		// Si aggiunge il principale alla lista di allegati da ribaltare
		if (allegatiDaRibaltare == null) {
			allegatiDaRibaltare = new ArrayList<>();
		}
		allegatiDaRibaltare.add(principaleAsAllegato);
			
		// Se il documento che si sta ribaltando è un Atto Decreto UCB, vengono ribaltati solamente gli allegati che superano la validazione
		if (isAttoDecretoEntrataUCB) {
			
			nomiFileAllegatiNonValidi = attoDecretoUCBSRV.validateFilenames(
					allegatiDaRibaltare.stream().map(AllegatoDTO::getNomeFile).collect(Collectors.toList()));
			
			// Si imposta uno specifico messaggio da visualizzare al termine della predisposizione
			if (!CollectionUtils.isEmpty(nomiFileAllegatiNonValidi)) {
				esitoPredisponi.setCodiceErrore(PredisponiDocumentoMessageEnum.ATTO_DECRETO_ALLEGATI_NON_CONFORMI);
				esitoPredisponi.setNote(PredisponiDocumentoMessageEnum.ATTO_DECRETO_ALLEGATI_NON_CONFORMI.getMessage());
			}
			
		}
		
		final PkHandler pkHandler = utente.getSignerInfo().getPkHandlerVerifica();
		final String pUrlHandler = pkHandler.getHandler();
		final byte[] pSecurePin = pkHandler.getSecurePin();
		final boolean disableUseHostOnly = utente.getDisableUseHostOnly();
		NamedStreamDTO sbustato = null;
		FormatoAllegatoEnum formatoSbustatoAsAllegato = null;
		String mimeType = null;
		AllegatoDTO allegatoNew = null;
		for (final AllegatoDTO al : allegatiDaRibaltare) {
			if (CollectionUtils.isEmpty(nomiFileAllegatiNonValidi) || !nomiFileAllegatiNonValidi.contains(al.getNomeFile())) {
				//se non ha content (allegato del protocollo in ingresso), carica content
				if(al.getContent() == null || al.getContent().length == 0) {
					final AttachmentDTO contentAllegato = documentoSRV.getContentAllegato(al.getDocumentTitle(), utente.getFcDTO(), utente.getIdAoo());
					final boolean isP7MAllegato = FileUtils.isP7MFile(contentAllegato.getMimeType());
					final FormatoAllegatoEnum formato = 
							(isP7MAllegato || PdfHelper.getSignatureNumber(contentAllegato.getContent()) > 0) ? FormatoAllegatoEnum.FIRMATO_DIGITALMENTE : FormatoAllegatoEnum.ELETTRONICO;
					allegatoNew = new AllegatoDTO(null, formato.getId(), docPredisposto.getIdTipologiaDocumento(), null,
							oggettoDocResponse, null, false, false, null, null, al.getNomeFile(), contentAllegato.getMimeType(), 
							null, contentAllegato.getContent(), null, null, null, null, false, tipiDocAttive, false, true);
				} else {
					
					al.setDaPredisponi(true);
					allegatoNew = al;
				}
				
				//se atto decreto UCB, sbusta su P7M
				if (isAttoDecretoEntrataUCB && FileUtils.isP7MFile(al.getMimeType())) {
					
					sbustato = FileUtils.recursiveExtractP7MFile(al.getContent(), al.getNomeFile(), 
							pUrlHandler, pSecurePin, disableUseHostOnly);
					
					if (sbustato != null && sbustato.getContent() != null && sbustato.getName() != null) {
						
						formatoSbustatoAsAllegato = PdfHelper.getSignatureNumber(sbustato.getContent()) > 0 
								? FormatoAllegatoEnum.FIRMATO_DIGITALMENTE : FormatoAllegatoEnum.ELETTRONICO;
						
						mimeType = FileUtils.getMimeType(FilenameUtils.getExtension(sbustato.getName()));
						
						final AllegatoDTO sbustatoAsAllegato = new AllegatoDTO(null, formatoSbustatoAsAllegato.getId(), docPredisposto.getIdTipologiaDocumento(), null,
								oggettoDocResponse, null, false, false, null, null, sbustato.getName(), mimeType, null, 
								sbustato.getContent(), null, null, null, null, false, tipiDocAttive, false, true);
						
						allegati.add(sbustatoAsAllegato);
						allegatoNew.setFileNonSbustato(true);
						
					}
					
				}
				
				allegati.add(allegatoNew);
			}
		}
		
		docPredisposto.setAllegati(allegati);
	}

	/**
	 * @param docPredisposto
	 * @param descTipologiaDocumento
	 * @param descTipoProcedimento
	 * @param idAoo
	 * @param con
	 */
	private void ribaltaTipologiaDocumentoTipoProcedimentoByDescsEntrata(final DetailDocumentRedDTO docPredisposto, final String descTipologiaDocumento, final String descTipoProcedimento, 
			final Long idAoo, final Connection con) {
		// Si recupera la tipologia documento per l'uscita corrispondente a quella del documento che si sta ribaltando
		final Integer idTipologiaDocumentoDocPredisposto = tipologiaDocumentoDAO.getIdTipologiaDocumentoByDescrizioneAndTipoCategoria(
				descTipologiaDocumento, TipoCategoriaEnum.USCITA.getIdTipoCategoria(), idAoo, con);
		
		// Si recupera il tipo procedimento per l'uscita associato alla tipologia documento appena calcolata e al tipo procedimento del documento che si sta ribaltando
		// N.B. Si chiama intValue() anche se il risultato della query potrebbe essere NULL, perché se accadesse vorrebbe che c'è un errore di configurazione dei tipi procedimento
		// e non sarebbe comunque possibile procedere con la predisposizione.
		Integer idTipoProcedimentoDocPredisposto = null;
		try {
			idTipoProcedimentoDocPredisposto = tipoProcedimentoDAO.getIdTipoProcedimentoByDescrizioneAndTipologiaDocumento(descTipoProcedimento, idTipologiaDocumentoDocPredisposto, con).intValue();
			docPredisposto.setIdTipologiaDocumento(idTipologiaDocumentoDocPredisposto);
			docPredisposto.setIdTipologiaProcedimento(idTipoProcedimentoDocPredisposto);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il ribaltamento della tipologia procedimento associata alla tipologia documento con id: " + idTipologiaDocumentoDocPredisposto 
					+ ", con descrizione: '" + descTipologiaDocumento + "'", e);
			throw new RedException("Errore durante il ribaltamento della tipologia procedimento associata alla tipologia documento con id: " + idTipologiaDocumentoDocPredisposto 
					+ ", con descrizione: '" + descTipologiaDocumento + "'", e);
		}
	}
	
	/**
	 * Ribalta la tipologia documento e il tipo procedimento a partire dagli ID del documento in entrata.
	 * 
	 * @param docPredisposto
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @param idAoo
	 * @param con
	 */
	private void ribaltaTipologiaDocumentoTipoProcedimentoByIdsEntrata(final DetailDocumentRedDTO docPredisposto, final Integer idTipologiaDocumento, final Integer idTipoProcedimento, 
			final Long idAoo, final Connection con) {
		// Si recupera la tipologia documento il cui ID è passato in input
		final TipologiaDocumentoDTO tipologiaDocumento = tipologiaDocumentoDAO.getById(idTipologiaDocumento, con);
		
		// Si recupera il tipo procedimento il cui ID è passato in input
		final TipoProcedimento tipoProcedimento = tipoProcedimentoDAO.getTPbyId(con, idTipoProcedimento);
		
		ribaltaTipologiaDocumentoTipoProcedimentoByDescsEntrata(docPredisposto, tipologiaDocumento.getDescrizione(), tipoProcedimento.getDescrizione(), idAoo, con);
	}
	
	/**
	 * @param docPredisposto
	 * @param propertyTipologiaDocumento
	 * @param idRegistroAusiliario
	 * @param codiceAoo
	 * @param con
	 */
	private void impostaTipologiaDocumentoTipoProcedimentoByPropertiesERegistroAusiliario(final DetailDocumentRedDTO docPredisposto, 
			final PropertiesNameEnum propertyTipologiaDocumento, final int idRegistroAusiliario, final String codiceAoo, final Connection con) {
		// Si recupera la tipologia documento da properties DB
		final Integer idTipologiaDocumentoDocPredisposto = Integer.parseInt(
				PropertiesProvider.getIstance().getParameterByString(codiceAoo + "." + propertyTipologiaDocumento.getKey()));
		
		// Il tipo procedimento è quello associato alla tipologia documento e al registro ausiliario selezionato
		// N.B. Si chiama intValue() anche se il risultato della query potrebbe essere NULL, perché se accadesse vorrebbe dire che c'è un errore di configurazione dei tipi procedimento
		// e non sarebbe comunque possibile procedere con la predisposizione.
		final Integer idTipoProcedimentoDocPredisposto = tipoProcedimentoDAO.getIdTipoProcedimentoByTipologiaDocumentoAndRegistroAusiliario(idTipologiaDocumentoDocPredisposto, 
				idRegistroAusiliario, con).intValue();
		
		docPredisposto.setIdTipologiaDocumento(idTipologiaDocumentoDocPredisposto); // Tipologia documento
		docPredisposto.setIdTipologiaProcedimento(idTipoProcedimentoDocPredisposto); // Tipo procedimento
	}
	
	/**
	 * @param docPredisposto
	 * @param propertyTipologiaDocumento
	 * @param codiceAoo
	 * @param con
	 */
	private void impostaTipologiaDocumentoTipoProcedimentoByProperties(final DetailDocumentRedDTO docPredisposto, 
			final PropertiesNameEnum propertyTipologiaDocumento, final String codiceAoo, final Connection con) {
		// Si recupera la tipologia documento da properties DB
		final Integer idTipologiaDocumentoDocPredisposto = Integer.parseInt(
				PropertiesProvider.getIstance().getParameterByString(codiceAoo + "." + propertyTipologiaDocumento.getKey()));
		
		// Il tipo procedimento è quello associato alla tipologia documento e al registro ausiliario selezionato
		// N.B. Si chiama intValue() anche se il risultato della query potrebbe essere NULL, perché se accadesse vorrebbe dire che c'è un errore di configurazione dei tipi procedimento
		// e non sarebbe comunque possibile procedere con la predisposizione.
		final List<TipoProcedimento> tipiProcedimentoByTipologiaDocumento = tipoProcedimentoDAO.getTipiProcedimentoByTipologiaDocumento(con, idTipologiaDocumentoDocPredisposto);
		final Integer idTipoProcedimentoDocPredisposto = (int) tipiProcedimentoByTipologiaDocumento.get(0).getTipoProcedimentoId();
		
		docPredisposto.setIdTipologiaDocumento(idTipologiaDocumentoDocPredisposto); // Tipologia documento
		docPredisposto.setIdTipologiaProcedimento(idTipoProcedimentoDocPredisposto); // Tipo procedimento
	}

	/**
	 * @param docPredisposto
	 * @param detailDocDaRibaltare
	 */
	private static void ribaltaMetadatiEstesi(final DetailDocumentRedDTO docPredisposto, final DetailDocumentRedDTO detailDocDaRibaltare) {
		docPredisposto.setMetadatiEstesi(detailDocDaRibaltare.getMetadatiEstesi()); // Metadati estesi ribaltati
	}
	
	/**
	 * Restituisce un DTO di allaccio a partire dal protocollo riferimento.
	 * 
	 * @param protocolloRiferimento
	 * @param utente
	 * @param con
	 * @return
	 */
	private RispostaAllaccioDTO getAllaccioProtocolloRiferimento(final String protocolloRiferimento, final UtenteDTO utente, final Connection con) {
		RispostaAllaccioDTO allaccioProtRif = new RispostaAllaccioDTO();
		IFilenetCEHelper fceh = null;
		
		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			
			final Document protocolloRiferimentoFn = fceh.getDocumentByDTandAOO(protocolloRiferimento, utente.getIdAoo());
			
			final String guid = it.ibm.red.business.utils.StringUtils.cleanGuidToString(protocolloRiferimentoFn.get_Id());
			final String documentTitle = (String) TrasformerCE.getMetadato(protocolloRiferimentoFn, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
			final Integer numeroDocumento = (Integer) TrasformerCE.getMetadato(protocolloRiferimentoFn, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
			final Integer numeroProtocollo = (Integer)  TrasformerCE.getMetadato(protocolloRiferimentoFn, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
			final Integer annoProtocollo = (Integer) TrasformerCE.getMetadato(protocolloRiferimentoFn, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
			final Date dataProtocollo = (Date) TrasformerCE.getMetadato(protocolloRiferimentoFn, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY);
			final Integer tipoProtocollo = (Integer) TrasformerCE.getMetadato(protocolloRiferimentoFn, PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY);
			final Integer idTipologiaDocumento = (Integer) TrasformerCE.getMetadato(protocolloRiferimentoFn, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY);
			final Integer idTipologiaProcedimento = (Integer) TrasformerCE.getMetadato(protocolloRiferimentoFn, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY);
			
			allaccioProtRif = trasformaInRispostaAllaccio(guid, documentTitle, numeroDocumento, idTipologiaDocumento, idTipologiaProcedimento, 
					numeroProtocollo, annoProtocollo, dataProtocollo, tipoProtocollo, protocolloRiferimentoFn.getClassName(), null);
			
			// ### Dati aggiuntivi da impostare nell'allaccio che rappresenta il protocollo riferimento -> START
			// Allegati
			final Integer idCategoriaDocumento = (Integer) TrasformerCE.getMetadato(protocolloRiferimentoFn, PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY);
			
			final DocumentSet allegatiFn = fceh.getAllegati(guid, false, false);
			List<AllegatoDTO> allegati = null;
			if (allegatiFn != null && !allegatiFn.isEmpty()) {
				allegati = new ArrayList<>();
				Document allegatoFn = null;
				AllegatoDTO allegato = null;
				final Iterator<?> itAllegatiFn = allegatiFn.iterator();
				
				final Map<ContextTrasformerCEEnum, Object> context = tipologiaDocumentoSRV.getContextTipologieDocumento(idCategoriaDocumento, utente, con);
				while (itAllegatiFn.hasNext()) {
					allegatoFn = (Document) itAllegatiFn.next();
					allegato = TrasformCE.transform(allegatoFn, TrasformerCEEnum.FROM_DOCUMENT_TO_ALLEGATO_CONTEXT, context, con);
					
					allegati.add(allegato);
				}
				
				allaccioProtRif.getDocument().setAllegati(allegati);
			}
			
			// Oggetto
			final String oggetto = (String) TrasformerCE.getMetadato(protocolloRiferimentoFn, PropertiesNameEnum.OGGETTO_METAKEY);
			allaccioProtRif.getDocument().setOggetto(oggetto);
			
			// Tipo procedimento
			final Integer idTipoProcedimento = (Integer) TrasformerCE.getMetadato(protocolloRiferimentoFn, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY);
			allaccioProtRif.getDocument().setIdTipologiaProcedimento(idTipoProcedimento);
			
			// Metadati
			final Collection<MetadatoDTO> metadati = tipologiaDocumentoSRV.recuperaMetadatiEstesi(documentTitle, utente);
			allaccioProtRif.getDocument().setMetadatiEstesi(metadati);
			// ### Dati aggiuntivi da impostare nell'allaccio che rappresenta il protocollo riferimento -> END
			
			// L'allaccio che rappresenta il protocollo riferimento è quello principale
			allaccioProtRif.setPrincipale(true);
			// L'allaccio che rappresenta il protocollo riferimento è da chiudere
			allaccioProtRif.setDocumentoDaChiudere(true);
			// L'allaccio che rappresenta il protocollo riferimento non è modificabile da maschera
			allaccioProtRif.setChiusuraDisabled(true);
			allaccioProtRif.setDisabled(true);
			
		} finally {
			popSubject(fceh);
		}
		
		return allaccioProtRif;
	}
	
	private void ribaltaMetadatiDaTemplate(final Collection<MetadatoDTO> metadatiDaTemplate, final Collection<MetadatoDTO> metadatiUscita) {
		
		if (CollectionUtils.isEmpty(metadatiDaTemplate) || CollectionUtils.isEmpty(metadatiUscita)) {
			return;
		}
		
		// Popolo i metadati in uscita a partire dai metadati recuperati dal template
		for (final MetadatoDTO metadatoReg : metadatiDaTemplate) {
			for (final MetadatoDTO metadatoDoc : metadatiUscita) {
				if (metadatoDoc.getName().equals(metadatoReg.getName()) && metadatoDoc.getType().equals(metadatoReg.getType())) {
					
					final TipoMetadatoEnum type = metadatoDoc.getType();
					// Viene impostata l'editabilita' in base alla logica definita in fase di predisposizione template
					metadatoDoc.setEditability(metadatoReg.getEditability());
					switch (type) {
					
					case CAPITOLI_SELECTOR:
						
						((CapitoloSpesaMetadatoDTO) metadatoDoc).setCapitoloSelected(metadatoReg.getValue4AttrExt());
						break;
					case LOOKUP_TABLE:
						
						((LookupTableDTO) metadatoDoc).setLookupValueSelected(((LookupTableDTO) metadatoReg).getLookupValueSelected());
						((LookupTableDTO) metadatoDoc).setLookupTableSelector(((LookupTableDTO) metadatoReg).getLookupTableSelector());
						((LookupTableDTO) metadatoDoc).setLookupValueSelected(((LookupTableDTO) metadatoReg).getLookupValueSelected());
						break;
					case PERSONE_SELECTOR:
						
						((AnagraficaDipendentiComponentDTO) metadatoDoc).setSelectedValues(((AnagraficaDipendentiComponentDTO) metadatoReg).getSelectedValues());
						break;
					default:
						
						metadatoDoc.setSelectedValue(metadatoReg.getValue4AttrExt());
						break;
					}
				}
			}
		}
	}

}