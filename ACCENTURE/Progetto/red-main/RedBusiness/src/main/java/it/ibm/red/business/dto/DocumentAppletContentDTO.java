package it.ibm.red.business.dto;

/**
 * DTO applet documento.
 */
public class DocumentAppletContentDTO extends AbstractDTO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 6033877452641621665L;
	
	/**
	 * Nome del file.
	 */
	private String nomeFile;
	
	/**
	 * Tipo file.
	 */
	private String contentType;
	
	/**
	 * Content.
	 */
	private String content; 

	/**
	 * Costruttore del DTO.
	 * @param nomeFile
	 * @param contentType
	 * @param content
	 */
	public DocumentAppletContentDTO(final String nomeFile, final String contentType, final String content) {
		super();
		this.nomeFile = nomeFile;
		this.contentType = contentType;
		this.content = content;
	}

	/**
	 * Restituisce il nome del file.
	 * @return nome file
	 */
	public String getNomeFile() {
		return nomeFile;
	}

	/**
	 * Restituisce il contetType del file.
	 * @return contentType
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * Restituisce il content del file.
	 * @return content
	 */
	public String getContent() {
		return content;
	}
	
}