package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import it.ibm.red.business.dto.SecurityDTO.UtenteS;


/**
 * @author m.crescentini
 *
 * @param <E>
 */
public class ListSecurityDTO extends ArrayList<SecurityDTO> implements Serializable {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 984110461128619398L;

	/**
	 * Costruttore vuoto.
	 */
	public ListSecurityDTO() {
		super();
	}

	/**
	 * Costruttore di default.
	 * @param security
	 */
	public ListSecurityDTO(final Collection<? extends SecurityDTO> security) {
		super();
		this.addAll(security);
	}

	/**
	 * @see java.util.ArrayList#add(java.lang.Object).
	 */
	@Override
	public boolean add(final SecurityDTO object) {
		boolean added = false;

		if (!this.contains(object)) {
			super.add(object);
			added = true;
		}
		return added;
	}

	/**
	 * @see java.util.ArrayList#addAll(java.util.Collection).
	 */
	@Override
	public boolean addAll(final Collection<? extends SecurityDTO> collection) {
		boolean added = false;
		
		for (SecurityDTO security : collection) {
			added = this.add(security);
		}
		return added;
	}

	/**
	 * @see java.util.ArrayList#contains(java.lang.Object).
	 */
	@Override
	public boolean contains(final Object object) {
		SecurityDTO security = (SecurityDTO) object;
		boolean contains = false;
		
		
		for (SecurityDTO currSec : this) {
			final boolean entrambiNull = currSec.getUtente() == null && security.getUtente() == null;
			final boolean entrambiNotNull = currSec.getUtente() != null && security.getUtente() != null;
			
			if (currSec.getGruppo().getIdUfficio() == security.getGruppo().getIdUfficio() 
					&& (entrambiNull || (entrambiNotNull && currSec.getUtente().getIdUtente() == security.getUtente().getIdUtente()))) {
				contains = true;
			}
		}
		return contains;
	}

	/**
	 * Metodo che sostituisce null all'oggetto Utente se ha identificativo a zero.
	 */
	public void setNullableUtente() {
		for (int i = 0; i < size(); i++) {
			if (get(i).getUtente().getIdUtente() == 0) {
				get(i).setUtente(null);
			}
		}
	}

	/**
	 * Metodo che sostituisce un utente agli oggetti Utente null.
	 */
	public void setDeNullableUtente() {
		for (int i = 0; i < size(); i++) {
			if (get(i).getUtente() == null) {
				SecurityDTO security = new SecurityDTO();
				UtenteS utente = security.getUtente();
				utente.setIdUtente(0);
				get(i).setUtente(utente);
			}
		}
	}
}