package it.ibm.red.business.persistence.model;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import com.filenet.api.core.Document;

import it.ibm.red.business.dao.impl.TipoCalendario;
import it.ibm.red.business.dto.NotificaUtenteDTO;

/**
 * Model di un Evento.
 */
public class Evento extends NotificaUtenteDTO implements Serializable {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -4006864727043495682L;
	
	/**
	 * Messaggio.
	 */
	public static final String DESCRIZIONE_DOCUMENTO_SCADENZA = "Scadenze";

	/**
	 * Categoria di utenti e strutture che devono visualizzare l'evento: vale solo per NEWS e COMUNICAZIONI.
	 */
	private List<Long> idRuoliCategoriaTarget;

	/**
	 * Nodi.
	 */
	private List<Integer> idNodiTarget;

	/**
	 * Descrizione nodi.
	 */
	private List<String> descrizioneNodi;

	/**
	 * Evento.
	 */
	private int idEvento;

	/**
	 * Ente.
	 */
	private int idEnte;

	/**
	 * Aoo.
	 */
	private Long idAoo;

	/**
	 * Utente.
	 */
	private Long idUtente;

	/**
	 * Nodo.
	 */
	private Long idNodo;

	/**
	 * Ruolo.
	 */
	private Long idRuolo;

	/**
	 * Data inizio.
	 */
	private Date dataInizio;

	/**
	 * Data scadenza.
	 */
	private Date dataScadenza;

	/**
	 * Titolo.
	 */
	private String titolo;

	/**
	 * Contenuto.
	 */
	private String contenuto;

	/**
	 * Link esterno.
	 */
	private String linkEsterno;

	/**
	 * Colore.
	 */
	private String coloreContenuto;

	/**
	 * Flag locked.
	 */
	private boolean isLocked;

	/**
	 * Flag pubblico.
	 */
	private boolean isPubblico;

	/**
	 * Flag urgente.
	 */
	private int urgente;

	/**
	 * Data.
	 */
	private Date dataTime;

	/**
	 * Documento.
	 */
	private Document doc;

	/**
	 * Document title.
	 */
	private String documentTitle;

	/**
	 * Numero documento.
	 */
	private Integer numeroDocumento;

	/**
	 * Content allegato.
	 */
	private byte[] allegato;

	/**
	 * Nome allegato.
	 */
	private String nomeAllegato;

	/**
	 * Estensione allegato.
	 */
	private String estensioneAllegato;
	
	/**
	 * Mime type allegato.
	 */
	private String mimeTypeAllegato;

	/**
	 * Tpo calendario.
	 */
	private TipoCalendario tipoCalendario;
	
	/**
	 * Categoria documento.
	 */
	private Integer idCategoriaDocumento;

	/**
	 * Stato notifica.
	 */
	private Integer statoNotificaCalendario;
	
	/**
	 * Costruttore.
	 */
	public Evento() {
		super();
	}

	/**
	 * Costruttore.
	 * 
	 * @param idUtente
	 *            - id dell'utente
	 * @param idEvento
	 *            - id dell'evento
	 * @param dataInizio
	 *            - data di inizio
	 * @param dataScadenza
	 *            - data di scadenza
	 * @param contenuto
	 * @param coloreContenuto
	 *            - colore del contenuto
	 * @param isLocked
	 * @param isPubblico
	 * @param titolo
	 * @param linkEsterno
	 *            - link esterno
	 * @param allegato
	 * @param nomeAllegato
	 *            - nome dell'allegato
	 * @param estensioneAllegato
	 *            - estensione dell'allegato
	 * @param mimeTypeAllegato
	 * @param statoNotificaCalendario
	 *            - stato della notifica calendario
	 */
	public Evento(final Long idUtente, final int idEvento, final Date dataInizio, final Date dataScadenza, final String contenuto, final String coloreContenuto, final boolean isLocked, final boolean isPubblico, 
			final String titolo, final String linkEsterno, final byte[] allegato, final String nomeAllegato, final String estensioneAllegato, final String mimeTypeAllegato,
			final Integer statoNotificaCalendario) {
		this();
		this.idUtente = idUtente;
		this.idEvento = idEvento;
		this.dataInizio = dataInizio;
		this.dataScadenza = dataScadenza;
		this.contenuto = contenuto;
		this.coloreContenuto = coloreContenuto;
		this.isLocked = isLocked;
		this.titolo = titolo;
		this.linkEsterno = linkEsterno;
		this.isPubblico = isPubblico;
		this.allegato = allegato;
		this.nomeAllegato = nomeAllegato;
		this.estensioneAllegato = estensioneAllegato;
		this.mimeTypeAllegato = mimeTypeAllegato; 
		this.statoNotificaCalendario = statoNotificaCalendario;
	}

	/**
	 * Recupera l'id dell'evento.
	 * @return id dell'evento
	 */
	public int getIdEvento() {
		return idEvento;
	}

	/**
	 * Imposta l'id dell'evento.
	 * @param idEvento id dell'evento
	 */
	public void setIdEvento(final int idEvento) {
		this.idEvento = idEvento;
	}

	/**
	 * Recupera l'id dell'ente.
	 * @return id dell'ente
	 */
	public int getIdEnte() {
		return idEnte;
	}

	/**
	 * Imposta l'id dell'ente.
	 * @param idEnte id dell'ente
	 */
	public void setIdEnte(final int idEnte) {
		this.idEnte = idEnte;
	}

	/**
	 * Recupera l'id dell'Aoo.
	 * @return id dell'Aoo
	 */
	public Long getIdAoo() {
		return idAoo;
	}

	/**
	 * Imposta l'id dell'Aoo.
	 * @param idAoo id dell'Aoo
	 */
	public void setIdAoo(final Long idAoo) {
		this.idAoo = idAoo;
	}

	/**
	 * Recupera l'id dell'utente.
	 * @return id dell'utente
	 */
	public Long getIdUtente() {
		return idUtente;
	}

	/**
	 * Imposta l'id dell'utente.
	 * @param idUtente id dell'utente
	 */
	public void setIdUtente(final Long idUtente) {
		this.idUtente = idUtente;
	}

	/**
	 * Recupera l'id del nodo.
	 * @return id del nodo
	 */
	public Long getIdNodo() {
		return idNodo;
	}

	/**
	 * Imposta l'id del nodo.
	 * @param idNodo id del nodo
	 */
	public void setIdNodo(final Long idNodo) {
		this.idNodo = idNodo;
	}

	/**
	 * Recupera l'id del ruolo.
	 * @return id del ruolo
	 */
	public Long getIdRuolo() {
		return idRuolo;
	}

	/**
	 * Imposta l'id del ruolo.
	 * @param idRuolo id del ruolo
	 */
	public void setIdRuolo(final Long idRuolo) {
		this.idRuolo = idRuolo;
	}

	/**
	 * Recupera la data di inizio.
	 * @return data di inizio
	 */
	public Date getDataInizio() {
		return dataInizio;
	}

	/**
	 * Imposta la data di inizio.
	 * @param dataInizio data di inizio
	 */
	public void setDataInizio(final Date dataInizio) {
		this.dataInizio = dataInizio;
	}

	/**
	 * Recupera la data di scadenza.
	 * @return data di scadenza
	 */
	public Date getDataScadenza() {
		return dataScadenza;
	}

	/**
	 * Imposta la data di scadenza.
	 * @param dataScadenza data di scadenza
	 */
	public void setDataScadenza(final Date dataScadenza) {
		this.dataScadenza = dataScadenza;
	}

	/**
	 * Recupera il titolo.
	 * @return titolo
	 */
	public String getTitolo() {
		return titolo;
	}

	/**
	 * Imposta il titolo.
	 * 
	 * @param titolo titolo
	 */
	public void setTitolo(final String titolo) {
		this.titolo = titolo;
	}

	/**
	 * Recupera il contenuto.
	 * @return contenuto
	 */
	public String getContenuto() {
		return contenuto;
	}

	/**
	 * Imposta il contenuto.
	 * @param contenuto contenuto
	 */
	public void setContenuto(final String contenuto) {
		this.contenuto = contenuto;
	}

	/**
	 * Recupera il link esterno.
	 * @return link esterno
	 */
	public String getLinkEsterno() {
		return linkEsterno;
	}

	/**
	 * Imposta il link esterno.
	 * @param linkEsterno link esterno
	 */
	public void setLinkEsterno(final String linkEsterno) {
		this.linkEsterno = linkEsterno;
	}

	/**
	 * Recupera il colore del contenuto.
	 * @return colore del contenuto
	 */
	public String getColoreContenuto() {
		return coloreContenuto;
	}

	/**
	 * Imposta il colore del contenuto.
	 * @param coloreContenuto colore del contenuto
	 */
	public void setColoreContenuto(final String coloreContenuto) {
		this.coloreContenuto = coloreContenuto;
	}

	/**
	 * Recupera l'attributo isLocked.
	 * @return isLocked
	 */
	public boolean isLocked() {
		return isLocked;
	}

	/**
	 * Imposta l'attributo isLocked.
	 * @param isLocked
	 */
	public void setLocked(final boolean isLocked) {
		this.isLocked = isLocked;
	}

	/**
	 * Recupera l'attributo isPubblico.
	 * @return isPubblico
	 */
	public boolean isPubblico() {
		return isPubblico;
	}

	/**
	 * Imposta l'attributo isPubblico.
	 * @param isPubblico
	 */
	public void setPubblico(final boolean isPubblico) {
		this.isPubblico = isPubblico;
	}
    
	/**
	 * Recupera l'attributo urgente.
	 * @return urgente
	 */
	public int getUrgente() {
		return urgente;
	}

	/**
	 * Imposta l'attributo urgente.
	 * @param urgente
	 */
	public void setUrgente(final int urgente) {
		this.urgente = urgente;
	}

	/**
	 * Recupera l'attributo dataTime.
	 * @return dataTime
	 */
	public Date getDataTime() {
		return dataTime;
	}

	/**
	 * Imposta l'attributo dataTime.
	 * @param dataTime
	 */
	public void setDataTime(final Date dataTime) {
		this.dataTime = dataTime;
	}

	/**
	 * Recupera il documento.
	 * @return documento
	 */
	public Document getDoc() {
		return doc;
	}

	/**
	 * Imposta il documento.
	 * @param doc documento
	 */
	public void setDoc(final Document doc) {
		this.doc = doc;
	}

	/**
	 * Recupera il tipo di calendario.
	 * @return tipo di calendario
	 */
	public TipoCalendario getTipoCalendario() {
		return tipoCalendario;
	}

	/**
	 * Imposta il tipo di calendario.
	 * @param tipoCalendario tipo di calendario
	 */
	public void setTipoCalendario(final TipoCalendario tipoCalendario) {
		this.tipoCalendario = tipoCalendario;
	}

	/**
	 * Recupera l'allegato.
	 * @return allegato
	 */
	public byte[] getAllegato() {
		return allegato;
	}

	/**
	 * Imposta l'allegato.
	 * @param allegato
	 */
	public void setAllegato(final byte[] allegato) {
		this.allegato = allegato;
	}

	/**
	 * Recupera il nome dell'allegato.
	 * @return nome dell'allegato
	 */
	public String getNomeAllegato() {
		return nomeAllegato;
	}

	/**
	 * Imposta il nome dell'allegato.
	 * @param nomeAllegato nome dell'allegato
	 */
	public void setNomeAllegato(final String nomeAllegato) {
		this.nomeAllegato = nomeAllegato;
	}

	/**
	 * Recupera l'estensione dell'allegato.
	 * @return estensione dell'allegato
	 */
	public String getEstensioneAllegato() {
		return estensioneAllegato;
	}

	/**
	 * Imposta l'estensione dell'allegato.
	 * @param estensioneAllegato estensione dell'allegato
	 */
	public void setEstensioneAllegato(final String estensioneAllegato) {
		this.estensioneAllegato = estensioneAllegato;
	}
	
	/**
	 * Recupera gli id dei ruoli categoria target.
	 * @return lista degli id dei ruoli categoria target
	 */
	public List<Long> getIdRuoliCategoriaTarget() {
		return idRuoliCategoriaTarget;
	}

	/**
	 * Imposta gli id dei ruoli categoria target.
	 * @param idRuoliCategoriaTarget lista degli id dei ruoli categoria target
	 */
	public void setIdRuoliCategoriaTarget(final List<Long> idRuoliCategoriaTarget) {
		this.idRuoliCategoriaTarget = idRuoliCategoriaTarget;
	}

	/**
	 * Recupera gli id dei nodi target.
	 * @return lista degli id dei nodi target
	 */
	public List<Integer> getIdNodiTarget() {
		return idNodiTarget;
	}

	/**
	 * Imposta gli id dei nodi target.
	 * @param idNodiTarget lista degli id dei nodi target
	 */
	public void setIdNodiTarget(final List<Integer> idNodiTarget) {
		this.idNodiTarget = idNodiTarget;
	}
	
	/**
	 * Recupera mimeType allegato.
	 * @return mimeType allegato
	 */
	public String getMimeTypeAllegato() {
		return mimeTypeAllegato;
	}

	/**
	 * Imposta mimeType allegato.
	 * @param mimeTypeAllegato mimeType allegato.
	 */
	public void setMimeTypeAllegato(final String mimeTypeAllegato) {
		this.mimeTypeAllegato = mimeTypeAllegato;
	}

	/**
	 * Recupera il document title.
	 * @return document title
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Imposta il document title.
	 * @param documentTitle document title
	 */
	public void setDocumentTitle(final String documentTitle) {
		this.documentTitle = documentTitle;
	}

	/**
	 * Recupera il numero documento.
	 * @return numero documento
	 */
	public Integer getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * Imposta il numero documento.
	 * @param numeroDocumento numero documento
	 */
	public void setNumeroDocumento(final Integer numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * Recupera la descrizione dei nodi.
	 * @return lista delle descrizioni dei nodi
	 */
	public List<String> getDescrizioneNodi() {
		return descrizioneNodi;
	}

	/**
	 * Imposta la descrizione dei nodi.
	 * @param descrizioneNodi lista delle descrizioni dei nodi
	 */
	public void setDescrizioneNodi(final List<String> descrizioneNodi) {
		this.descrizioneNodi = descrizioneNodi;
	}

	/**
	 * Recupera l'id della categoria documento.
	 * @return id della categoria documento
	 */
	public Integer getIdCategoriaDocumento() {
		return idCategoriaDocumento;
	}

	/**
	 * Imposta l'id della categoria documento.
	 * @param idCategoriaDocumento id della categoria documento
	 */
	public void setIdCategoriaDocumento(final Integer idCategoriaDocumento) {
		this.idCategoriaDocumento = idCategoriaDocumento;
	}
	
	/**
	 * Recupera lo stato della notifica calendario.
	 * @return stato della notifica calendario
	 */
	public Integer getStatoNotificaCalendario() {
		return statoNotificaCalendario;
	}
	
	/**
	 * Imposta lo stato della notifica calendario.
	 * @param statoNotificaCalendario stato della notifica calendario
	 */
	public void setStatoNotificaCalendario(final Integer statoNotificaCalendario) {
		this.statoNotificaCalendario = statoNotificaCalendario;
	}

	/**
	 * @see it.ibm.red.business.dto.NotificaUtenteDTO#calcolaDaLeggere().
	 */
	@Override
	public void calcolaDaLeggere() {
		final LocalDate dataOdierna = LocalDate.now();
		final LocalDate dataInizioEvento = dataInizio != null ? Instant.ofEpochMilli(dataInizio.getTime()).atZone(ZoneId.systemDefault()).toLocalDate() : null;
		final LocalDate dataScadenzaEvento = dataScadenza != null ? Instant.ofEpochMilli(dataScadenza.getTime()).atZone(ZoneId.systemDefault()).toLocalDate() : null;
		
		if (dataInizioEvento != null && dataOdierna.compareTo(dataInizioEvento) >= 0 
				&& (dataScadenzaEvento == null || (dataOdierna.compareTo(dataScadenzaEvento) <= 0))) {
			setDaLeggere(true);
		}
	}
}
