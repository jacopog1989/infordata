package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.AssegnazioneAutomaticaCapSpesaDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * 
 * Interfaccia DAO per la gestione delle assegnazioni automatiche dei capitoli
 * di spesa.
 * 
 * @author mcrescentini
 *
 */
public interface IAssegnazioneAutomaticaCapSpesaDAO extends Serializable {
	
	/**
	 * Restituisce l'assegnazione per capitolo spesa se esistente, null altrimenti.
	 * 
	 * @param codiceCapitoloSpesa
	 * @param idAoo
	 * @param idUfficio
	 * @param connection
	 * @return assegnazione per capitolo spesa se esistente, null altrimenti
	 */
	AssegnazioneAutomaticaCapSpesaDTO get(String codiceCapitoloSpesa, Long idAoo, Long idUfficio, Connection connection);
	
	/**
	 * @param idAoo
	 * @param connection
	 * @return assegnazione per capitolo spesa se esistente, null altrimenti
	 */
	List<AssegnazioneAutomaticaCapSpesaDTO> getByIdAoo(Long idAoo, Connection connection);
	
	/**
	 * @param codiceCapitoloSpesa
	 * @param idAoo
	 * @param idUfficio
	 * @param connection
	 * @return Esito eliminazione.
	 */
	int elimina(String codiceCapitoloSpesa, Long idAoo, Long idUfficio, Connection connection);
	
	/**
	 * @param assegnazioniAutomatiche
	 * @param idAoo
	 * @param con
	 */
	void eliminaMultiple(List<AssegnazioneAutomaticaCapSpesaDTO> assegnazioniAutomatiche, Long idAoo, Connection con);
	
	/**
	 * @param idAoo
	 * @param connection
	 * @return Esito dell'eliminazione.
	 */
	int eliminaTutteByIdAoo(Long idAoo, Connection connection);
	
	/**
	 * @param codiceCapitoloSpesa
	 * @param idAoo
	 * @param idUfficio
	 * @param idUtente
	 * @param connection
	 */
	void inserisci(String codiceCapitoloSpesa, Long idAoo, Long idUfficio, Long idUtente, Connection connection);
	
	/**
	 * @param assegnazioniAutomatiche
	 * @param idAoo
	 * @param connection
	 */
	void inserisciMultiple(List<AssegnazioneAutomaticaCapSpesaDTO> assegnazioniAutomatiche, Long idAoo, Connection connection);

	/**
	 * Restituisce true o false in base alla correttezza della descrizione
	 * dell'ufficio.
	 * 
	 * @param idAoo
	 * @param descrizioneUfficio
	 * @param connection
	 * @return Esito della verifica.
	 */
	boolean verificaAssegnazioneAutomatica(Long idAoo, UtenteDTO utente, String descrizioneUfficio, Connection connection);
	
}
