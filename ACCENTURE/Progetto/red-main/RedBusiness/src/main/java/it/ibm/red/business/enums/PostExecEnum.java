/**
 * 
 */
package it.ibm.red.business.enums;

/**
 * Enum sui possibili eventi da richiamare dopo l'esecuzione di una response.
 * Tutti gli eventi sono associati ad un azione da svolgere e che coinvolge la coda principale o gli item lavorati o rimanenti nella coda.
 */
public enum PostExecEnum {

	/**
	 * Valore.
	 */
	DISABILITA_ITEM(1),
	
	/**
	 * Valore.
	 */
	RICARICA_CODA(2),
	
	/**
	 * Valore.
	 */
	AGGIORNA_ITEM(3),
	
	/**
	 * Valore.
	 */
	NESSUNA_AZIONE(4);
	
	/**
	 * Id.
	 */
	private Integer id;
	
	 /**
	  * Costruttore.
	  * @param inId
	  */
	 PostExecEnum(final Integer inId) {
		 id = inId;
	 }

	/**
	 * Restituisce l'id.
	 * @return id
	 */
	 public Integer getId() {
		return id;
	}
}
