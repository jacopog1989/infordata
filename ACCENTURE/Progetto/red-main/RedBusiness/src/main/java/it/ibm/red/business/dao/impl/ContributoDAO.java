package it.ibm.red.business.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.google.common.net.MediaType;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IContributoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.ContributoDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.enums.StatoContributoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contributo;
import it.ibm.red.business.persistence.model.Utente;

/** 
 * 
 * @author CPIERASC
 *
 *	Dao gestione contributi.
 */
@Repository
public class ContributoDAO extends AbstractDAO implements IContributoDAO {
	
	private static final String AND_C_STATO_NOT_EQUALS_3 = " AND c.STATO <> 3";

	/**
	 * Identificativo documento.
	 */
	private static final String ID_DOCUMENTO = "IDDOCUMENTO";

	/**
	 * Nome tabella - CONTRIBUTO ESTERNO.
	 */
	private static final String CONTRIBUTO_ESTERNO = "CONTRIBUTO_ESTERNO";
	
	/**
	 * Nome tabella - CONTRIBUTO.
	 */
	private static final String CONTRIBUTO = "CONTRIBUTO";
	
	/**
	 * Identificativo contributo.
	 */
	private static final String GUID_CONTRIBUTO = "GUIDCONTRIBUTO";

	/**
	 * Data contributo.
	 */
	private static final String DATA_CONTRIBUTO = "DATACONTRIBUTO";

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ContributoDAO.class.getName());
	
	/**
	 * Utente dao.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;
	
	/**
	 * Metodo per il recupero dei contributi interni di un documento.
	 * 
	 * @param documentTitle	identificativo del documento	
	 * @param connection	connessione al database
	 * @return				contributi
	 */
	@Override
	public final Collection<Contributo> getContributiInterni(final String documentTitle, final Long idAOO,  final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			final Collection<Contributo> output = new ArrayList<>();
			final String querySQL = "SELECT c.IDCONTRIBUTO, c.IDUTENTE, c.IDNODO, c.IDAOO, c.IDDOCUMENTO, c.NOTA, c.DATACONTRIBUTO, c.GUIDCONTRIBUTO, c.NOMEFILECONTRIBUTO FROM CONTRIBUTO c"
					  + " WHERE c.IDDOCUMENTO = " + sanitize(Integer.valueOf(documentTitle)) + AND_C_STATO_NOT_EQUALS_3 +  " AND c.IDAOO = " + idAOO;
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			while (rs.next()) {
				
				final Timestamp dataTimeTS = rs.getTimestamp(DATA_CONTRIBUTO);
				Date dataContributo = null;
				if (dataTimeTS != null) {
					dataContributo = new Date(dataTimeTS.getTime());
				}
				final Contributo contributo = new Contributo(rs.getLong("IDCONTRIBUTO"), dataContributo, rs.getLong("IDUTENTE"), rs.getLong("IDNODO"), 
						rs.getLong("IDAOO"), rs.getLong(ID_DOCUMENTO), rs.getString("NOMEFILECONTRIBUTO"), rs.getString("NOTA"), false, rs.getString(GUID_CONTRIBUTO));
				output.add(contributo);
			}
			return output;
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Gets the.
	 *
	 * @param id the id
	 * @param connection the connection
	 * @param flagEsterno the flag esterno
	 * @return the contributo DTO
	 */
	@Override
	public final ContributoDTO get(final Long id, final Connection connection, final Boolean flagEsterno) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ContributoDTO output = null;
			
			String table = CONTRIBUTO;
			if (Boolean.TRUE.equals(flagEsterno)) {
				table = CONTRIBUTO_ESTERNO;
			}
			String querySQL = "SELECT c.GUIDCONTRIBUTO, c.IDDOCUMENTO, c.NOTA FROM " + table + " c WHERE c.IDCONTRIBUTO = " + sanitize(id);
			if (Boolean.FALSE.equals(flagEsterno)) {
				querySQL += AND_C_STATO_NOT_EQUALS_3;
			}
			
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			
			if (rs.next()) {
				output = new ContributoDTO(id, rs.getInt(ID_DOCUMENTO), rs.getString(GUID_CONTRIBUTO), flagEsterno, rs.getString("nota"));
			}
			
			return output;
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Count contributi.
	 *
	 * @param documentTitle the document title
	 * @param connection the connection
	 * @return the long
	 */
	@Override
	public final Long countContributi(final String documentTitle, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long output = 0L;
		try {
			final String querySQL = "SELECT ((SELECT COUNT(*) FROM CONTRIBUTO where IDDOCUMENTO = " + sanitize(Integer.valueOf(documentTitle)) 
				+ " AND STATO <> 3) + (SELECT COUNT(*) FROM CONTRIBUTO_ESTERNO where IDDOCUMENTO = " + sanitize(Integer.valueOf(documentTitle)) + ")) AS totale FROM DUAL";
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			while (rs.next()) {
				output = rs.getLong("totale");
			}
			return output;
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}
	
	/**
	 * Gets the nota contributo esterno content.
	 *
	 * @param id the id
	 * @param connection the connection
	 * @param idDocumento the id documento
	 * @return the nota contributo esterno content
	 */
	@Override
	public final FileDTO getNotaContributoEsternoContent(final Long id, final Connection connection, final Integer idDocumento) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		FileDTO output = null;
		
		try {
			final String querySQL = "SELECT NOTA, DATACONTRIBUTO FROM CONTRIBUTO_ESTERNO c WHERE c.IDCONTRIBUTO = " + sanitize(id);		
			LOGGER.info("Query contributo esterno: " + querySQL);

			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			
			if (rs.next()) {
				final String nota = rs.getString("NOTA");
				LOGGER.info("Dati esterni presenti: " + nota);
				
				final Timestamp dataTimeTS = rs.getTimestamp(DATA_CONTRIBUTO);
				Date dataContributo = null;
				if (dataTimeTS != null) {
					dataContributo = new Date(dataTimeTS.getTime());
				}
				
				final byte[] content = PdfHelper.createContributo(idDocumento, nota, id, dataContributo, null, null);
				output = new FileDTO("nota.pdf", content, MediaType.PDF.toString());
			}
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return output;
	}
	
	
	/**
	 * Gets the nota contributo interno content.
	 *
	 * @param id the id
	 * @param connection the connection
	 * @param idDocumento the id documento
	 * @return the nota contributo interno content
	 */
	@Override
	public final FileDTO getNotaContributoInternoContent(final Long id, final Connection connection, final Integer idDocumento) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		FileDTO output = null;
		
		try {
			final String querySQL = "SELECT c.NOTA, c.IDUTENTE, c.DATACONTRIBUTO, c.NOMEFILECONTRIBUTO FROM CONTRIBUTO c WHERE c.IDCONTRIBUTO = " 
					+ sanitize(id) 	+ AND_C_STATO_NOT_EQUALS_3;		
			LOGGER.info("Query contributo interno: " + querySQL);
			ps = connection.prepareStatement(querySQL);
			
			rs = ps.executeQuery();
			
			if (rs.next()) {
				final String nota = rs.getString("NOTA");
				LOGGER.info("Dati esterni presenti: " + nota);
				
				final Timestamp dataTimeTS = rs.getTimestamp(DATA_CONTRIBUTO);
				Date dataContributo = null;
				if (dataTimeTS != null) {
					dataContributo = new Date(dataTimeTS.getTime());
				}
				
				final Long idUtente = rs.getLong("IDUTENTE");
				final Utente utente = utenteDAO.getUtente(idUtente, connection);
				
				final String nomeFileContributo = rs.getString("NOMEFILECONTRIBUTO");
				
				// Si procede alla generazione del PDF on-the-fly
				final byte[] content = PdfHelper.createContributo(idDocumento, nota, id, dataContributo, 
						utente.getNome() + " " + utente.getCognome() + "[" + utente.getUsername() + "]", nomeFileContributo);
				
				output = new FileDTO("nota.pdf", content, MediaType.PDF.toString());
			}
			
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return output;
	}
	

	/**
	 * Gets the contributi esterni.
	 *
	 * @param documentTitle the document title
	 * @param connection the connection
	 * @return the contributi esterni
	 */
	@Override
	public final Collection<Contributo> getContributiEsterni(final String documentTitle, final Long idAoo, final Connection connection) {
		final Collection<Contributo> output = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			final String querySQL = "SELECT c.IDCONTRIBUTO, c.IDDOCUMENTO, c.NOTA, c.DATACONTRIBUTO, c.PROTOCOLLO, c.MITTENTE, c.GUIDCONTRIBUTO, c.MODIFICABILE, c.VALIDITAFIRMA "
					+ "FROM CONTRIBUTO_ESTERNO c WHERE c.IDDOCUMENTO = " + sanitize(Integer.valueOf(documentTitle)) + "AND c.IDAOO = " + idAoo 
					+ " ORDER BY c.DATACONTRIBUTO";
			ps = connection.prepareStatement(querySQL);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				popolaContributoEsterno(output, rs, idAoo);
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore riscontrato durante il recupero dei contributi esterni", e);
			throw new RedException("Errore riscontrato durante il recupero dei contributi esterni", e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return output;
	}
	
	
	/**
	 * Recupera i contributi esterni dato il protocollo (documento di entrata dei contributi esterni).
	 *
	 * @param protocollo the protocollo
	 * @param connection the connection
	 * @return the contributi esterni
	 */
	@Override
	public final Collection<Contributo> getContributiEsterniByProtocollo(final String protocollo, final Long idAoo, final Connection connection) {
		final Collection<Contributo> output = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			int index = 1;
			
			final String querySQL = "SELECT c.IDCONTRIBUTO, c.IDDOCUMENTO, c.NOTA, c.DATACONTRIBUTO, c.PROTOCOLLO, c.MITTENTE, c.GUIDCONTRIBUTO, c.MODIFICABILE, c.VALIDITAFIRMA "
					+ "FROM CONTRIBUTO_ESTERNO c WHERE c.PROTOCOLLO = ? AND c.IDAOO = ? ORDER BY c.DATACONTRIBUTO";
			ps = connection.prepareStatement(querySQL);
			ps.setString(index++, protocollo);
			ps.setLong(index++, idAoo);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				popolaContributoEsterno(output, rs, idAoo);
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore riscontrato durante il recupero dei contributi esterni dal protocollo: " + protocollo, e);
			throw new RedException("Errore riscontrato durante il recupero dei contributi esterni dal protocollo: " + protocollo, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return output;
	}
	
	/**
	 * @see it.ibm.red.business.dao.IContributoDAO#updateStatoContributoByIdUfficio(java.lang.Long,
	 *      java.lang.Integer, java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public Integer updateStatoContributoByIdUfficio(final Long idNodo, final Integer idDocumento, final Integer stato, final Connection con) {
		Integer output = 0;
		PreparedStatement ps = null;
 
		try {
			int index = 1;
			
			ps = con.prepareStatement("UPDATE CONTRIBUTO SET STATO = ? WHERE IDDOCUMENTO = ? AND IDNODO IN (SELECT IDNODO FROM NODO U WHERE  IDNODOPADRE = ?)");
			ps.setInt(index++, stato);
			ps.setString(index++, idDocumento.toString());
			ps.setInt(index++, idNodo.intValue());
			
			output = ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'aggiornamento dello stato del contributo", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
		
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IContributoDAO#updateValiditaFirmaByGuid(java.lang.String,
	 *      int, java.sql.Connection).
	 */
	@Override
	public void updateValiditaFirmaByGuid(final String guidContributo, final int validitaFirma, final Connection con) {
		PreparedStatement ps = null;
		int index = 1;
		
		try {
			ps = con.prepareStatement("UPDATE contributo_esterno SET validitafirma = ? WHERE guidcontributo = ?");
			ps.setInt(index++, validitaFirma);
			ps.setString(index++, guidContributo);
			
			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'aggiornamento della validita della firma del contributo esterno con GUID: " + guidContributo, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}
	
	/**
	 * @see it.ibm.red.business.dao.IContributoDAO#insertContributoInterno(it.ibm.red.business.persistence.model.Contributo,
	 *      java.sql.Connection).
	 */
	@Override
	public long insertContributoInterno(final Contributo contributo, final Connection con) {
		CallableStatement cs = null;
		long identificativoContributo;
		
		try {
			int index = 0;
			cs = con.prepareCall("{CALL p_inscontributo(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");
			cs.setLong(++index, contributo.getIdUfficio()); 
			cs.setLong(++index, contributo.getIdUtente());
			cs.setLong(++index, contributo.getIdDocumento());
			cs.setString(++index, contributo.getNota());
			cs.setString(++index, contributo.getGuid());
			cs.setString(++index, contributo.getDataCdC());
			if (contributo.getFoglio() > -1) {
				cs.setInt(++index, contributo.getFoglio());
			} else {
				cs.setNull(++index, Types.INTEGER);
			}
			if (contributo.getRegistro() > -1) {
				cs.setInt(++index, contributo.getRegistro());
			} else {
				cs.setNull(++index, Types.INTEGER);
			}
			cs.setInt(++index, contributo.getStato());
			cs.setString(++index, contributo.getNomeFile());
			cs.registerOutParameter(++index, Types.INTEGER);
			cs.execute();
			
			identificativoContributo = cs.getLong(index);
		} catch (final SQLException e) {
			LOGGER.error("Si è verificato un errore durante l'inserimento del contributo interno per il documento: " + contributo.getIdDocumento(), e);
			throw new RedException("Si è verificato un rrrore durante l'inserimento del contributo interno", e);
		} finally {
			closeStatement(cs);
		}
		
		return identificativoContributo;
	}
	
	/**
	 * @see it.ibm.red.business.dao.IContributoDAO#insertContributoEsterno(it.ibm.red.business.persistence.model.Contributo,
	 *      java.sql.Connection).
	 */
	@Override
	public long insertContributoEsterno(final Contributo contributo, final Connection con) {
		CallableStatement cs = null;
		long idContributo;
		
		try {
			int index = 0;
			cs = con.prepareCall("{CALL p_inscontributo_esterno(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)}");
			cs.setLong(++index, contributo.getIdDocumento()); 
			cs.setString(++index, contributo.getProtocollo());
			cs.setString(++index, contributo.getMittente());
			cs.setString(++index, contributo.getGuid());
			cs.setString(++index, contributo.getNomeFile());
			cs.setString(++index, contributo.getNota());
			cs.setInt(++index, contributo.getStato());
			cs.setInt(++index, contributo.getModificabile());
			cs.setLong(++index, contributo.getIdAoo());
			cs.registerOutParameter(++index, Types.INTEGER);
			cs.execute();
			
			idContributo = cs.getLong(index);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'inserimento del contributo esterno per il documento: " + contributo.getIdDocumento(), e);
			throw new RedException("Si è verificato un rrrore durante l'inserimento del contributo esterno", e);
		} finally {
			closeStatement(cs);
		}
		
		return idContributo;
	}
	
	/**
	 * @see it.ibm.red.business.dao.IContributoDAO#updateGuidContributo(java.lang.Long,
	 *      java.lang.String, boolean, java.sql.Connection).
	 */
	@Override
	public Integer updateGuidContributo(final Long idContributo, final String guidContributo, final boolean isEsterno, final Connection con) {
		PreparedStatement ps = null;
		int result = 0;
		
		try {
			int index = 1;
			ps = con.prepareStatement("UPDATE contributo" + (isEsterno ? "_esterno" : "") + " SET guidcontributo = ? WHERE idcontributo = ?");
			ps.setString(index++, guidContributo);
			ps.setLong(index++, idContributo);
			
			result = ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'aggiornamento dello stato del contributo", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
		
		return result;
	}

	/**
	 * @see it.ibm.red.business.dao.IContributoDAO#updateStatoContributo(java.lang.Long,
	 *      java.lang.Integer, boolean, java.sql.Connection).
	 */
	@Override
	public final int updateStatoContributo(final Long idContributo, final Integer stato, final boolean isEsterno, final Connection con) {
		PreparedStatement ps = null;
		int result = 0;
		int index = 1;

		try {
			ps = con.prepareStatement("UPDATE contributo" + (isEsterno ? "_esterno" : "") + " SET stato = ? WHERE idcontributo = ?");
			ps.setInt(index++, stato);
			ps.setLong(index++, idContributo);
			
			result = ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'aggiornamento dello stato del contributo: " + idContributo, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
		
		return result;
	}
	
	
	@Override
	public final int deleteContributoEsterno(final Long idContributo, final Connection con) {
		PreparedStatement ps = null;
		int result = 0;
		
		try {
			ps = con.prepareStatement("DELETE FROM contributo_esterno WHERE idcontributo = ?");
			ps.setLong(1, idContributo);
			
			result = ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'eliminazione del contributo esterno con ID: " + idContributo, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
		
		return result;
	}
	
	
	@Override
	public final int updateContributoEsternoByProtocollo(final String protocollo, final Long idAoo, final String mittente, final String nota, final Connection con) {
		PreparedStatement ps = null;
		int result = 0;
		int index = 1;

		try {
			ps = con.prepareStatement("UPDATE contributo_esterno SET mittente = ?, nota = ? WHERE protocollo = ? AND idaoo = ?");
			ps.setString(index++, mittente);
			ps.setString(index++, nota);
			ps.setString(index++, protocollo);
			ps.setLong(index++, idAoo);
			
			result = ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'aggiornamento del contributo esterno dato il protocollo: " + protocollo + " dell'ID AOO: " + idAoo, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
		
		return result;
	}
	
	
	@Override
	public final int updateContributoInterno(final ContributoDTO contributo, final Long idUfficio, final Long idUtente, final Connection con) {
		PreparedStatement ps = null;
		int result = 0;
		int index = 1;

		try {
			ps = con.prepareStatement("UPDATE contributo SET idnodo = ?, idutente = ?, iddocumento = ?, datacontributo = SYSDATE, nota = ?, guidcontributo = ?"
					+ ", stato = ?, nomefilecontributo = ? WHERE idcontributo = ?");
			
			ps.setLong(index++, idUfficio);
			ps.setLong(index++, idUtente);
			ps.setInt(index++, contributo.getIdDocumento());
			ps.setString(index++, contributo.getNota());
			ps.setString(index++, contributo.getGuidContributo());
			ps.setInt(index++, StatoContributoEnum.MODIFICATO.getId());
			ps.setString(index++, contributo.getFilename());
			ps.setLong(index++, contributo.getIdContributo());

			result = ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'aggiornamento del contributo con ID: " + contributo.getIdContributo(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
		
		return result;
	}
	
	
	@Override
	public final int deleteContributiEsterniByProtocollo(final String protocollo, final Long idAoo, final Connection con) {
		PreparedStatement ps = null;
		int result = 0;
		
		try {
			ps = con.prepareStatement("DELETE FROM contributo_esterno WHERE protocollo = ?");
			ps.setString(1, protocollo);
			
			result = ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'eliminazione dei contributi esterni afferenti al protocollo: " + protocollo, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
		
		return result;
	}
	
	
	private static void popolaContributoEsterno(final Collection<Contributo> output, final ResultSet rs, final Long idAoo) throws SQLException {
		final Timestamp dataTimeTS = rs.getTimestamp(DATA_CONTRIBUTO);
		Date dataContributo = null;
		if (dataTimeTS != null) {
			dataContributo = new Date(dataTimeTS.getTime());
		}
		final Contributo contributo = new Contributo(rs.getLong("IDCONTRIBUTO"), dataContributo, rs.getLong(ID_DOCUMENTO), rs.getString("NOTA"), rs.getString("PROTOCOLLO"), true, 
				rs.getString(GUID_CONTRIBUTO), rs.getString("MITTENTE"), rs.getInt("MODIFICABILE"), rs.getInt("VALIDITAFIRMA"));
		contributo.setIdAoo(idAoo);
		output.add(contributo);
	}
	 
	@Override
	public final ContributoDTO getContributoSottoscrizioniByIDandAOO(final Long id, final Long idAoo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ContributoDTO output = null;
			 
			final String querySQL = "SELECT c.GUIDCONTRIBUTO, c.IDDOCUMENTO, c.NOTA FROM  CONTRIBUTO c WHERE c.IDCONTRIBUTO = " + sanitize(id) + " AND c.IDAOO = " + sanitize(idAoo);
		 
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			
			if (rs.next()) {
				output = new ContributoDTO(id, rs.getInt(ID_DOCUMENTO), rs.getString(GUID_CONTRIBUTO), false, rs.getString("nota"));
			}
			
			return output;
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

}