package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.DelegatoDTO;
import it.ibm.red.business.dto.ResponsabileCopiaConformeDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.persistence.model.UtenteFirma;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;

/**
 * The Interface IUtenteSRV.
 *
 * @author CPIERASC
 * 
 *         Interfaccia servizio gestione utente.
 */
public interface IUtenteSRV extends IUtenteFacadeSRV {

	/**
	 * Ottiene l'utente tramite l'id.
	 * @param idUtente
	 * @param connection
	 * @return utente
	 */
	UtenteDTO getById(Long idUtente, Connection connection);
	
	/**
	 * Ottiene l'utente tramite l'id dell'utente, del ruolo e dell'ufficio.
	 * @param idUtente
	 * @param idRuolo
	 * @param idNodo
	 * @param conn
	 * @return utente
	 */
	UtenteDTO getById(Long idUtente, Long idRuolo, Long idNodo, Connection conn);
	
	/**
	 * Ottiene i responsabili copia conforme.
	 * @param idAOO
	 * @return lista di responsabili copia conforme
	 */
	List<ResponsabileCopiaConformeDTO> getComboResponsabiliCopiaConforme(Long idAOO);
	
	/**
	 * Ottiene l'utente tramite l'utente e gli ids del ruolo e dell'ufficio.
	 * @param utente
	 * @param idRuolo
	 * @param idNodo
	 * @param connection
	 * @return utente
	 */
	UtenteDTO getByUtente(Utente utente, Long idRuolo, Long idNodo, Connection connection);

	/**
	 * Ottiene le deleghe per il Libro Firma.
	 * @param idNodo
	 * @param idAOO
	 * @return delegato
	 */
	DelegatoDTO getDelegatoLibroFirma(Long idNodo, Long idAOO);

	/**
	 * Ottiene l'utente tramite il codice fiscale.
	 * @param codiceFiscale
	 * @param con
	 * @return utente DTO
	 */
	UtenteDTO getByCodiceFiscale(String codiceFiscale, Connection con);

	/**
	 * Restituisce il glifo delegato.
	 * @param idUtente
	 * @param idUtenteDelegante
	 * @return UtenteFirma
	 */
	UtenteFirma getGlifoDelegato(Long idUtente, Long idUtenteDelegante);

}
