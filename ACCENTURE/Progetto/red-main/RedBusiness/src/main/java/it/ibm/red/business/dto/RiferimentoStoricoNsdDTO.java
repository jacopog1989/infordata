/**
 * 
 */
package it.ibm.red.business.dto;
 

/**
 * @author VINGENITO
 *
 */
public class RiferimentoStoricoNsdDTO extends AbstractDTO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 6717668339807970973L;
	

	/**
	 * Anno protocollo.
	 */
	private Integer annoProtocolloNsd;
	
	/**
	 * Oggetto protocollo.
	 */
	private String oggettoProtocolloNsd;
	
	/**
	 * Numero protocollo.
	 */
	private Integer numeroProtocolloNsd;	
	
	/**
	 * Flag verificato.
	 */
	private boolean verificato;
	
	/**
	 * Documento.
	 */
	private DetailDocumentRedDTO document;

	/**
	 * Identificativo documento principale.
	 */
	private String idDocumentoPrincipale;
	
	/**
	 * Flag disabilitato.
	 */
	private boolean disabled;
	
	/**
	 * Costruttore vuoto.
	 */
	public RiferimentoStoricoNsdDTO() {
		
	}
	
	/**
	 * Costruttore.
	 */
	public RiferimentoStoricoNsdDTO(final Integer inAnnoProtocolloNsd, final String inOggettoProtocolloNsd, final Integer inNumeroProtocolloNsd, final String inIdDocumentoPrincipale) {
		annoProtocolloNsd = inAnnoProtocolloNsd; 
		oggettoProtocolloNsd = inOggettoProtocolloNsd;
		numeroProtocolloNsd = inNumeroProtocolloNsd;
		idDocumentoPrincipale = inIdDocumentoPrincipale;
	}
	
	/**
	 * @return the annoProtocolloNsd
	 */
	public Integer getAnnoProtocolloNsd() {
		return annoProtocolloNsd;
	}

	/**
	 * @param annoProtocolloNsd the annoProtocolloNsd to set
	 */
	public void setAnnoProtocolloNsd(final Integer annoProtocolloNsd) {
		this.annoProtocolloNsd = annoProtocolloNsd;
	}

	/**
	 * @return the numeroProtocolloNsd
	 */
	public Integer getNumeroProtocolloNsd() {
		return numeroProtocolloNsd;
	}

	/**
	 * @param numeroProtocollo the numeroProtocollo to set
	 */
	public void setNumeroProtocolloNsd(final Integer numeroProtocolloNsd) {
		this.numeroProtocolloNsd = numeroProtocolloNsd;
	}

	/**
	 * @return the verificato
	 */
	public boolean getVerificato() {
		return verificato;
	}

	/**
	 * @param verificato the verificato to set
	 */
	public void setVerificato(final boolean verificato) {
		this.verificato = verificato;
	}

	/**
	 * @return the document
	 */
	public DetailDocumentRedDTO getDocument() {
		return document;
	}

	/**
	 * @param document the document to set
	 */
	public void setDocument(final DetailDocumentRedDTO document) {
		this.document = document;
	}
	
	/**
	 * @return the idDocumentoPrincipale
	 */
	public String getIdDocumentoPrincipale() {
		return idDocumentoPrincipale;
	}
	
	/**
	 * @param idDocumentoPrincipale the idDocumentoPrincipale to set
	 */
	public void setIdDocumentoPrincipale(final String idDocumentoPrincipale) {
		this.idDocumentoPrincipale = idDocumentoPrincipale;
	}
	
	/**
	 * @return the disabled
	 */
	public boolean getDisabled() {
		return disabled;
	}
	
	/**
	 * @param disabled the disabled to set
	 */
	public void setDisabled(final boolean disabled) {
		this.disabled = disabled;
	}
	
	/**
	 * @return the oggettoProtocolloNsd
	 */
	public String getOggettoProtocolloNsd() {
		return oggettoProtocolloNsd;
	}
	

	/**
	 * @param oggettoProtocolloNsd the oggettoProtocolloNsd to set
	 */
	public void setOggettoProtocolloNsd(final String oggettoProtocolloNsd) {
		this.oggettoProtocolloNsd = oggettoProtocolloNsd;
	}
}