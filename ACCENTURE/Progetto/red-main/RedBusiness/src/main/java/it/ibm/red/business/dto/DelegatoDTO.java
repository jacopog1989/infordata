/**
 * 
 */
package it.ibm.red.business.dto;

import java.util.Date;

/**
 * @author APerquoti
 *
 */
public class DelegatoDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -5685446876578206353L;

	/**
	 * Identificativo utente.
	 */
	private Long idUtente;

	/**
	 * Nome.
	 */
	private String nomeUtente;

	/**
	 * Cognome.
	 */
	private String cognomeUtente;

	/**
	 * Identificativo nodo.
	 */
	private Long idNodo;

	/**
	 * Descrizione nodo.
	 */
	private String descrizioneNodo;

	/**
	 * Data attivazione.
	 */
	private Date dataAttivazione;

	/**
	 * Data disattivazione.
	 */
	private Date dataDisattivazione;

	/**
	 * Motivo delega.
	 */
	private String motivoDelega;

	/**
	 * Costruttore completo del DTO.
	 * 
	 * @param idUtente
	 * @param nomeUtente
	 * @param cognomeUtente
	 * @param idNodo
	 * @param descrizioneNodo
	 * @param dataAttivazione
	 * @param dataDisattivazione
	 * @param motivoDelega
	 */
	public DelegatoDTO(final Long idUtente, final String nomeUtente, final String cognomeUtente, final Long idNodo, final String descrizioneNodo, final Date dataAttivazione,
			final Date dataDisattivazione, final String motivoDelega) {
		this.idUtente = idUtente;
		this.nomeUtente = nomeUtente;
		this.cognomeUtente = cognomeUtente;
		this.idNodo = idNodo;
		this.descrizioneNodo = descrizioneNodo;
		this.dataAttivazione = dataAttivazione;
		this.dataDisattivazione = dataDisattivazione;
		this.motivoDelega = motivoDelega;
	}

	/**
	 * @return the idUtente
	 */
	public Long getIdUtente() {
		return idUtente;
	}

	/**
	 * @param idUtente the idUtente to set
	 */
	public void setIdUtente(final Long idUtente) {
		this.idUtente = idUtente;
	}

	/**
	 * @return the nomeUtente
	 */
	public String getNomeUtente() {
		return nomeUtente;
	}

	/**
	 * @param nomeUtente the nomeUtente to set
	 */
	public void setNomeUtente(final String nomeUtente) {
		this.nomeUtente = nomeUtente;
	}

	/**
	 * @return the cognomeUtente
	 */
	public String getCognomeUtente() {
		return cognomeUtente;
	}

	/**
	 * @param cognomeUtente the cognomeUtente to set
	 */
	public void setCognomeUtente(final String cognomeUtente) {
		this.cognomeUtente = cognomeUtente;
	}

	/**
	 * @return the idNodo
	 */
	public Long getIdNodo() {
		return idNodo;
	}

	/**
	 * @param idNodo the idNodo to set
	 */
	public void setIdNodo(final Long idNodo) {
		this.idNodo = idNodo;
	}

	/**
	 * @return the descrizioneNodo
	 */
	public String getDescrizioneNodo() {
		return descrizioneNodo;
	}

	/**
	 * @param descrizioneNodo the descrizioneNodo to set
	 */
	public void setDescrizioneNodo(final String descrizioneNodo) {
		this.descrizioneNodo = descrizioneNodo;
	}

	/**
	 * @return the dataAttivazione
	 */
	public Date getDataAttivazione() {
		return dataAttivazione;
	}

	/**
	 * @param dataAttivazione the dataAttivazione to set
	 */
	public void setDataAttivazione(final Date dataAttivazione) {
		this.dataAttivazione = dataAttivazione;
	}

	/**
	 * @return the dataDisattivazione
	 */
	public Date getDataDisattivazione() {
		return dataDisattivazione;
	}

	/**
	 * @param dataDisattivazione the dataDisattivazione to set
	 */
	public void setDataDisattivazione(final Date dataDisattivazione) {
		this.dataDisattivazione = dataDisattivazione;
	}

	/**
	 * @return the motivoDelega
	 */
	public String getMotivoDelega() {
		return motivoDelega;
	}

	/**
	 * @param motivoDelega the motivoDelega to set
	 */
	public void setMotivoDelega(final String motivoDelega) {
		this.motivoDelega = motivoDelega;
	}

}
