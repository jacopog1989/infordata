package it.ibm.red.business.dao.impl;

import java.io.InputStream;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IEmailBckDAO;
import it.ibm.red.business.dto.EmailBckDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * The Class EmailBckDAO.
 *
 * @author a.dilegge
 * 
 * 	Dao per la gestione dei content delle mail in ingresso.
 */
@Repository
public class EmailBckDAO extends AbstractDAO implements IEmailBckDAO {
	
	/**
	 * SERIAL VERSION UID.
	 */
	private static final long serialVersionUID = -7104950624259449041L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(EmailBckDAO.class.getName());
	

	/**
	 * Gets the ente.
	 *
	 * @param idEnte the id ente
	 * @param connection the connection
	 * @return the ente
	 */ 
	@Override
	public final byte[] getBlobMailOriginale(final String messageIdClasseEmail, final String casellaPostale, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		byte[] byteArrayEmail = null;
		try {
			String querySQL = "SELECT IS_EMAIL FROM EMAILBCK WHERE MESSAGEIDCLASSEEMAIL = ?  AND CASELLA = ? AND NOTIFICA = 0";
			ps = connection.prepareStatement(querySQL);
			ps.setString(1, messageIdClasseEmail);
			ps.setString(2, casellaPostale);
			rs = ps.executeQuery();
			
			if (rs.next()) {
				Blob blobEmail = rs.getBlob("IS_EMAIL");
				if (blobEmail != null) {
					byteArrayEmail = blobEmail.getBytes(1, (int) blobEmail.length());
				}
			}
			
		} catch (Exception e) {
			LOGGER.error("Errore durante il recupero del content dell'email con ID: " + messageIdClasseEmail, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return byteArrayEmail;
	}

	
	@Override
	public final int saveEmail(final EmailBckDTO email, final Connection con) {
		CallableStatement cs = null;
		int result = 0;
	
		try {
			cs = con.prepareCall("call P_INSERTEMAILBCK (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			
			InputStream iS = email.getISEmail();
			cs.setString(1, email.getMessageId());  
			cs.setString(2, email.getOggetto()); 	
			if(iS!=null) {
				cs.setBinaryStream(3, iS, iS.available()); 
			}else {
				cs.setNull(3,java.sql.Types.VARBINARY); 
			}
			cs.setDate(4, email.getDataInvio() != null ? new java.sql.Date(email.getDataInvio().getTime()) : null);  
			cs.setDate(5, new java.sql.Date(email.getDataScarico().getTime())); 
			cs.setString(6, email.getMittente()); 
			cs.setInt(7, email.getIsNotifica()); 
			cs.setString(8, email.getCasella()); 
			cs.setInt(9, email.getIsPec());
			cs.setString(10, email.getMessageIdClasseEmail());
			
			cs.registerOutParameter(11, Types.INTEGER);
			cs.executeUpdate();
						
			result = cs.getInt(11);
			
		} catch (Exception e) {
			LOGGER.error("Non è stato possibile inserire il backup della mail con oggetto: " + email.getOggetto(), e);
			throw new RedException(e);
		} finally {
			closeStatement(cs);
		}
			
		return result;
	}
	
}
