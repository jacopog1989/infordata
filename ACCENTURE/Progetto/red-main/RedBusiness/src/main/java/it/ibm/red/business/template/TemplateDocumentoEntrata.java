package it.ibm.red.business.template;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.SalvaDocumentoRedDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.logger.REDLogger;

/**
 * Classe che definisce un template di un documento in entrata.
 */
public class TemplateDocumentoEntrata extends TemplateDocumento {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = 3802423226317864833L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TemplateDocumentoEntrata.class.getName());
	
	/**
	 * Stralcio di html.
	 */
	private static final String LABEL_TD = "</label></td>";

	/**
	 * Stralcio di html.
	 */
	private static final String TR = "</tr>";

	/**
	 * Costruttore vuoto.
	 */
	public TemplateDocumentoEntrata() {
		// Questo costruttore è lasciato intenzionalmente vuoto.
	}

	/**
	 * @see it.ibm.red.business.template.TemplateDocumento#getDettaglioDocumentoTable(it.ibm.red.business.dto.SalvaDocumentoRedDTO).
	 */
	@Override
	public StringBuilder getDettaglioDocumentoTable(final SalvaDocumentoRedDTO documento) {
		final StringBuilder dettaglioTable = new StringBuilder();

		try {
			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Formato documento in entrata: </label></td>");
			dettaglioTable.append(
					"<td colspan=\"5\"><label id=\"formatoDocumento\" name=\"formatoDocumento\" class=\"scripta\">"
							+ (documento.getFormatoDocumentoEnum() != null
									? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(documento.getFormatoDocumentoEnum().getDescrizione())
									: Constants.EMPTY_STRING)
							+ LABEL_TD);
			dettaglioTable.append(TR);

			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Tipo procedimento: </label></td>");
			dettaglioTable.append(
					"<td colspan=\"5\"><label id=\"tipoProcedimento\" name=\"tipoProcedimento\" class=\"scripta\">"
							+ (documento.getTipoProcedimento() != null
									? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(documento.getTipoProcedimento().getDescrizione())
									: Constants.EMPTY_STRING)
							+ LABEL_TD);
			dettaglioTable.append(TR);

			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Mittente: </label></td>");
			dettaglioTable.append("<td colspan=\"5\"><label id=\"mittente\" name=\"mittente\" class=\"scripta\">"
					+ (documento.getMittenteContatto() != null 
						? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(documento.getMittenteContatto().getAliasContatto()) : Constants.EMPTY_STRING)
					+ LABEL_TD);
			dettaglioTable.append(TR);

			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Mezzo di ricezione: </label></td>");
			dettaglioTable
					.append("<td colspan=\"5\"><label id=\"mezzoRicezione\" name=\"mezzoRicezione\" class=\"scripta\">"
							+ (documento.getMezzoRicezione() != null 
								? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(documento.getMezzoRicezione().getDescrizione()) : Constants.EMPTY_STRING)
							+ LABEL_TD);
			dettaglioTable.append(TR);

			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Riservato: </label></td>");
			dettaglioTable.append("<td colspan=\"5\"><label id=\"riservato\" name=\"riservato\" class=\"scripta\">"
					+ (Boolean.TRUE.equals(documento.getIsRiservato()) ? BooleanFlagEnum.SI.getDescription()
							: BooleanFlagEnum.NO.getDescription())
					+ LABEL_TD);
			dettaglioTable.append(TR);

			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Urgente: </label></td>");
			dettaglioTable.append("<td colspan=\"5\"><label id=\"urgente\" name=\"urgente\" class=\"scripta\">"
					+ (Boolean.TRUE.equals(documento.getIsUrgente()) ? BooleanFlagEnum.SI.getDescription()
							: BooleanFlagEnum.NO.getDescription())
					+ LABEL_TD);
			dettaglioTable.append(TR);

			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Da non protocollare: </label></td>");
			dettaglioTable.append(
					"<td colspan=\"5\"><label id=\"protocollazione\" name=\"protocollazione\" class=\"scripta\">"
							+ (Boolean.TRUE.equals(documento.getDaProtocollare()) ? BooleanFlagEnum.NO.getDescription()
									: BooleanFlagEnum.SI.getDescription())
							+ LABEL_TD);
			dettaglioTable.append(TR);

			dettaglioTable.append("<tr>");
			dettaglioTable.append(
					"<td><label class=\"scriptaLabelBold\">Protocollo mittente (data - anno / numero): </label></td>");
			dettaglioTable.append(
					"<td colspan=\"5\"><label id=\"protocolloMittente\" name=\"protocolloMittente\" class=\"scripta\">"
							+ (getProtocolloMittente(documento)) + LABEL_TD);
			dettaglioTable.append(TR);

			dettaglioTable.append("<tr>");
			dettaglioTable.append(
					"<td><label class=\"scriptaLabelBold\">In risposta al protocollo (anno / numero): </label></td>");
			dettaglioTable.append(
					"<td colspan=\"5\"><label id=\"protocolloRisposta\" name=\"protocolloRisposta\" class=\"scripta\">"
							+ (getProtocolloRisposta(documento)) + LABEL_TD);
			dettaglioTable.append(TR);

			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Barcode:</label></td>");
			dettaglioTable.append("<td colspan=\"5\"><label id=\"barcode\" name=\"barcode\" class=\"scripta\">"
					+ (StringUtils.isNotBlank(documento.getBarcode()) 
							? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(documento.getBarcode()) : Constants.EMPTY_STRING)
					+ LABEL_TD);
			dettaglioTable.append(TR);

			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td><label class=\"scriptaLabelBold\">Assegnatario per Competenza:</label></td>");
			dettaglioTable.append(
					"<td colspan=\"5\"><label id=\"assegnatarioCompetenza\" name=\"assegnatarioCompetenza\" class=\"scripta\">"
							+ (!CollectionUtils.isEmpty(documento.getAssegnazioni())
									? it.ibm.red.business.utils.StringUtils.replaceWithHtmlEntities(getAssegnatarioCompetenza(documento))
									: Constants.EMPTY_STRING)
							+ LABEL_TD);
			dettaglioTable.append(TR);

			dettaglioTable.append("<tr>");
			dettaglioTable.append("<td colspan=\"6\"><hr style=\"width: 100%\"></hr></td>");
			dettaglioTable.append(TR);
		} catch (final Exception e) {
			LOGGER.error("Errore nella creazione della sezione dettaglio documento per la creazione dell'html.", e);
		}

		return dettaglioTable;
	}

	private boolean protocolloMittenteValorizzato(final SalvaDocumentoRedDTO documento) {
		boolean protocolloValorizzato = false;

		if (documento.getDataProtocolloMittente() != null && documento.getAnnoProtocolloMittente() != null
				&& StringUtils.isNotBlank(documento.getProtocolloMittente())) {
			protocolloValorizzato = true;
		}

		return protocolloValorizzato;
	}

	private String getProtocolloMittente(final SalvaDocumentoRedDTO documento) {
		String protocollo = Constants.EMPTY_STRING;

		if (protocolloMittenteValorizzato(documento)) {
			protocollo = documento.getDataProtocolloMittente() + " - " + documento.getAnnoProtocolloMittente() + "/"
					+ documento.getProtocolloMittente();
		}

		return protocollo;
	}

	private static boolean protocolloRispostaValorizzato(final SalvaDocumentoRedDTO documento) {
		boolean protocolloValorizzato = false;

		if (documento.getAnnoProtocolloRisposta() != null && documento.getNumeroProtocolloRisposta() != null) {
			protocolloValorizzato = true;
		}

		return protocolloValorizzato;
	}

	private static String getProtocolloRisposta(final SalvaDocumentoRedDTO documento) {
		String protocollo = Constants.EMPTY_STRING;

		if (protocolloRispostaValorizzato(documento)) {
			protocollo = documento.getAnnoProtocolloRisposta() + "/" + documento.getNumeroProtocolloRisposta();
		}

		return protocollo;
	}
}