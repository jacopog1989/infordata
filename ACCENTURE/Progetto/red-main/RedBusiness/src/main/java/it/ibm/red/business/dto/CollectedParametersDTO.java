package it.ibm.red.business.dto;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import it.ibm.red.business.enums.SeverityValidationPluginEnum;

/**
 * DTO utilizzato per gestione dei placeholders per il popolamento di un template in fase di generazione di un documento per una registrazione ausiliaria.
 * */
public class CollectedParametersDTO extends AbstractDTO {

	private static final long serialVersionUID = -4301459807411419181L;
	
	/**
	 * Messaggio.
	 */
	private String message;
	
	/**
	 * Severità.
	 */
	private SeverityValidationPluginEnum severityMessage;
	
	/**
	 * Parametri.
	 */
	private HashMap<String, String> stringParams;

	/**
	 * Metadati.
	 */
	private Collection<MetadatoDTO> metadatiForDocUscita;

	/**
	 * Costruttore del DTO.
	 */
	public CollectedParametersDTO() {
		stringParams = new HashMap<>();
		this.severityMessage = SeverityValidationPluginEnum.NONE;
	}

	/**
	 * Restituisce il messaggio.
	 * @return message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Restituisce il severity message.
	 * @return severityMessage
	 */
	public SeverityValidationPluginEnum getSeverityMessage() {
		return severityMessage;
	}

	/**
	 * Restituisce i parametri per il popolamento del template.
	 * @return stringParams
	 */
	public Map<String, String> getStringParams() {
		return stringParams;
	}

	/**
	 * Restituisce la lista dei metadati popolati che saranno ribaltati sul documento in uscita.
	 * @return metadatiForDocUscita
	 */
	public Collection<MetadatoDTO> getMetadatiForDocUscita() {
		return metadatiForDocUscita;
	}

	/**
	 * Imposta la lista dei metadati da ribaltare sul documento in uscita.
	 * @param metadatiForDocUscita
	 */
	public void setMetadatiForDocUscita(final Collection<MetadatoDTO> metadatiForDocUscita) {
		this.metadatiForDocUscita = metadatiForDocUscita;
	}

	/**
	 * Aggiunge un parametro alla lista dei parametri del template.
	 * @param key
	 * @param value
	 */
	public void addStringParam(final String key, final String value) {
		if (!SeverityValidationPluginEnum.ERROR.equals(severityMessage)) {
			stringParams.put(key, value);
		}
	}

	/**
	 * Imposta un messaggio bloccante, un messaggio è bloccante quando la severity è Error.
	 * @param message
	 */
	public void setBlockingMessage(final String message) {
		this.severityMessage = SeverityValidationPluginEnum.ERROR;
		this.message = message;
		stringParams = new HashMap<>();
	}

	/**
	 * Imposta un messaggio che non sia bloccante.
	 * @param severityMessage
	 * @param message
	 */
	public void setNonBlockingMessage(final SeverityValidationPluginEnum severityMessage, final String message) {
		if (!SeverityValidationPluginEnum.ERROR.equals(severityMessage)) {
			this.severityMessage = SeverityValidationPluginEnum.WARNING;
			this.message = message;
		}
	}

}
