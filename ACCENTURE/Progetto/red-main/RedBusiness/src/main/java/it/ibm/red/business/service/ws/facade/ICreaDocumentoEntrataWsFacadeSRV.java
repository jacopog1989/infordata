package it.ibm.red.business.service.ws.facade;

import java.io.Serializable;

import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.webservice.model.documentservice.types.messages.RedCreazioneDocumentoEntrataType;


/**
 * The Interface ICreaDocumentoEntrataWsFacadeSRV.
 *
 * @author m.crescentini
 * 
 *         Facade per il servizio di creazione di documenti in entrata tramite web service.
 */
public interface ICreaDocumentoEntrataWsFacadeSRV extends Serializable {
	
	/**
	 * Servizio per la creazione di un documento in entrata per RED.
	 * 
	 * @param docEntrataInput
	 * @return
	 */
	EsitoSalvaDocumentoDTO creaDocumentoEntrataRed(RedWsClient client, RedCreazioneDocumentoEntrataType docEntrataInput);

}