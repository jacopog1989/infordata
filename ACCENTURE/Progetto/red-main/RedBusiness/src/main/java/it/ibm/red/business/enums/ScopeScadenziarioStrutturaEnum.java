package it.ibm.red.business.enums;

/**
 * Scope è inteso come livello di dettaglio per il count dei documenti scadenzati
 * 
 * @author a.difolca
 *
 */
public enum ScopeScadenziarioStrutturaEnum {
	
	/**
	 * Valore.
	 */
	SEGRETERIA, // Tutti i documenti con scadenza di tutti gli ispettorati con tipostruttura enum segreteria
	
	/**
	 * Valore.
	 */
	ISPETTORATO, // Tutti i documenti con scadenza di tutti gli uffici di un dato ispettorato
	
	/**
	 * Valore.
	 */
	UFFICIO; // Tutti i documenti con scadenza di un dato ufficio
}
