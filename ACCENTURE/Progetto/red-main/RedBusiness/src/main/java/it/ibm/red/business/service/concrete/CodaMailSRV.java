package it.ibm.red.business.service.concrete;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.Document;
import com.filenet.api.property.Properties;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.constants.Constants.Mail;
import it.ibm.red.business.constants.Constants.ModalitaSpedizioneMail;
import it.ibm.red.business.constants.Constants.Notifiche;
import it.ibm.red.business.constants.Constants.TipoDestinatario;
import it.ibm.red.business.constants.Constants.TipoSpedizione;
import it.ibm.red.business.constants.Constants.TipologiaDestinatariMittenti;
import it.ibm.red.business.constants.Constants.TipologiaInvio;
import it.ibm.red.business.dao.IContattoDAO;
import it.ibm.red.business.dao.IGestioneNotificheEmailDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.ITipoFileDAO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.DestinatarioCodaMailDTO;
import it.ibm.red.business.dto.DestinatarioDatiCertDTO;
import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.MessaggioEmailDTO;
import it.ibm.red.business.dto.NotificaEmailDTO;
import it.ibm.red.business.dto.NotificaInteroperabilitaEmailDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ErroriMailEnum;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.FilenetAnnotationEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.PostaEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.StatoCodaEmailEnum;
import it.ibm.red.business.enums.StatoNotificaContattoEnum;
import it.ibm.red.business.enums.TipoAzioneMailEnum;
import it.ibm.red.business.enums.TipoDestinatarioDatiCertEnum;
import it.ibm.red.business.enums.TipoNotificaInteropEnum;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.exception.FilenetException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.email.EmailHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.notifiche.NotificaHelper;
import it.ibm.red.business.helper.notifiche.states.AttesaAccettazioneState1;
import it.ibm.red.business.helper.notifiche.states.AttesaAvvenutaConsegnaState2;
import it.ibm.red.business.helper.notifiche.states.ChiusuraState3;
import it.ibm.red.business.helper.notifiche.states.ErroreState4;
import it.ibm.red.business.helper.notifiche.states.InizioState0;
import it.ibm.red.business.helper.notifiche.states.ReInvioState5;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.persistence.model.CodaEmail;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.NotificaContatto;
import it.ibm.red.business.persistence.model.TipoFile;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.ICasellePostaliSRV;
import it.ibm.red.business.service.ICodaMailSRV;
import it.ibm.red.business.service.IEventoLogSRV;
import it.ibm.red.business.service.INpsSRV;
import it.ibm.red.business.service.IRubricaSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.utils.DestinatariRedUtils;
import it.ibm.red.business.utils.EmailUtils;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.LogStyleNotNull;
import it.ibm.red.business.utils.StringUtils;

/**
 * Service che gestisce le funzionalità della coda mail.
 */
@Service
@Component
public class CodaMailSRV extends AbstractService implements ICodaMailSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 5072215933806523575L;

	/**
	 * Messaggio di errorre nel recupero notifiche per l'icona stato.
	 */
	private static final String MSG_ERROR_RECUPERO_NOTIFICHE = "Errore nel recupero delle notifiche email per icona stato.";
	
	/**
	 * Messaggio di errore sull'aggiornamento documento E-mail su Filenet.
	 */
	private static final String ERROR_UPDATE_DOCUMENT_MSG = "Impossibile aggiornare il documento E-mail su FileNet con il Message-ID in quanto il documento in precedenza non è stato creato con successo.";

	/**
	 * Messaggio di registrazione errore.
	 */
	private static final String REGISTRAZIONE_ERRORE_MSG = "Registrato l'errore dell'item ";

	/**
	 * Messaggio di sollevamento eccezione, occorre appendere il messaggio di
	 * errore.
	 */
	private static final String SOLLEVAMENTO_ECCEZIONE_MSG = "Eccezione sollevata: ";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(CodaMailSRV.class.getName());

	/**
	 * Stirnga documento.
	 */
	private static final String DOCUMENTO = "DOCUMENTO";

	/**
	 * Dao coda trasformazione.
	 */
	@Autowired
	private IGestioneNotificheEmailDAO gestioneNotificheEmailDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private ITipoFileDAO tipoFileDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private IContattoDAO contattoDAO;

	/**
	 * Servizio.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private ICasellePostaliSRV casellePostaliSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IEventoLogSRV eventoSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IAooSRV aooSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private INpsSRV npsSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private NotificaPostMailErroreSRV notificaSRV;

	/**
	 * Dao.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * Fornitore della properties dell'applicazione.
	 */
	private PropertiesProvider pp;
	/**
	 * Servizio.
	 */
	@Autowired
	private IRubricaSRV rubricaSRV;

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	@Override
	public final List<MessaggioEmailDTO> recuperaMailDaInviare(final int idAoo, final boolean isPostaInterna, final Integer numItemToSend) {
		final List<MessaggioEmailDTO> emailDaSpedire = new ArrayList<>();
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), true);

			final List<CodaEmail> items = gestioneNotificheEmailDAO.lockItemDaSpedire(idAoo, isPostaInterna, numItemToSend, connection);

			final List<Integer> idNotifiche = new ArrayList<>();

			MessaggioEmailDTO messaggio = null;
			for (final CodaEmail item : items) {
				idNotifiche.add(item.getIdNotifica());
				messaggio = (MessaggioEmailDTO) FileUtils.getObjectFromByteArray(item.getMessaggio());
				messaggio.setIdNotifica(item.getIdNotifica());
				messaggio.setTipoDestinatario(item.getTipoDestinatario());
				messaggio.setIdSpedizioneUnica(item.getIdSpedizioneUnica());
				messaggio.setTipoEvento(item.getTipoEvento());
				messaggio.setUltimoTentativo(item.getCountRetry().equals(item.getMaxRetry() - 1));
				messaggio.setIdDocumento(item.getIdDocumento());
				messaggio.setIdSpedizioneNps(item.getIdSpedizioneNps());

				messaggio.setTipoMittente(item.getTipoMittente());
				emailDaSpedire.add(messaggio);
			}

			gestioneNotificheEmailDAO.updateStatoRicevuta(idNotifiche, StatoCodaEmailEnum.ELABORAZIONE, connection);

			commitConnection(connection);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero degli item dal DB per la Aoo " + idAoo, e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}

		return emailDaSpedire;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICodaMailFacadeSRV#spedisciProtocolloNPSDaCoda(it.ibm.red.business.dto.MessaggioEmailDTO,
	 *      int).
	 */
	@Override
	public void spedisciProtocolloNPSDaCoda(final MessaggioEmailDTO msgNext, final int idAoo) {
		Connection connection = null;
		IFilenetCEHelper fceh = null;

		try {

			connection = setupConnection(getDataSource().getConnection(), false);
			// recupera l'AOO
			final Aoo aoo = aooSRV.recuperaAoo(idAoo, connection);

			// inizializza accesso a CE filenet
			final AooFilenet aooFilenet = aoo.getAooFilenet();
			fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getObjectStore(), aoo.getIdAoo());

			final Long idUtente = msgNext.getIdUtenteMittente();
			final Long idNodoMittente = msgNext.getIdNodoMittente();
			final String documentTitle = msgNext.getDocumentTitle();

			final UtenteDTO utente = utenteSRV.getById(idUtente, connection);
			final Nodo ufficio = nodoDAO.getNodo(idNodoMittente, connection);
			utente.setIdUfficio(idNodoMittente);
			utente.setCodiceUfficio(ufficio.getCodiceNodo());
			utente.setNodoDesc(ufficio.getDescrizione());
			utente.setAooDesc(aoo.getDescrizione());
			utente.setCodiceAoo(aoo.getCodiceAoo());

			final Document document = fceh.getDocumentByDTandAOO(documentTitle, (long) idAoo);
			final String idDocumento = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
			final String idProtocollo = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY);
			final String infoProtocollo = new StringBuilder()
					.append(document.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY))).append("/")
					.append(document.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY))).append("_")
					.append(aoo.getCodiceAoo()).toString();

			npsSRV.spedisciProtocolloUscitaAsync(utente, infoProtocollo, idProtocollo, null, msgNext.getOggetto(), msgNext.getTesto(), idDocumento, connection);

			registraInvio(msgNext, true);

		} catch (final Exception e) {
			// registra l'errore di invio sulla base dati per il messaggio
			registraErroreInvio(msgNext, SOLLEVAMENTO_ECCEZIONE_MSG + e.getMessage(), true);
			if (Boolean.TRUE.equals(msgNext.getUltimoTentativo())) {
				inviaNotificaErroreSpedizione(msgNext, idAoo, null, true, false);
			}
			LOGGER.error(REGISTRAZIONE_ERRORE_MSG + msgNext.getIdNotifica(), e);

		} finally {
			closeConnection(connection);
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ICodaMailSRV#gestisciInvioNPS(it.ibm.red.business.dto.MessaggioEmailDTO,
	 *      int, java.sql.Connection,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public void gestisciInvioNPS(final MessaggioEmailDTO codaEmailItem, final int idAoo, final Connection con, final IFilenetCEHelper fceh) {
		try {
			// gestisci la validazione/creazione della mail su filenet
			final ResponseSendMail response = manageMail(codaEmailItem, con, fceh);

			if (response.isInviato()) {
				managePostInvioMail(response, codaEmailItem, idAoo, fceh, con, false);
			} else if (Boolean.TRUE.equals(codaEmailItem.getUltimoTentativo())) {
				inviaNotificaErroreSpedizione(codaEmailItem, idAoo, response.getErroreInvio(), false, true);
			}

		} catch (final Exception e) {

			// registra l'errore di invio sulla base dati per il messaggio
			registraErroreInvio(codaEmailItem, SOLLEVAMENTO_ECCEZIONE_MSG + e.getMessage(), false);
			if (Boolean.TRUE.equals(codaEmailItem.getUltimoTentativo())) {
				inviaNotificaErroreSpedizione(codaEmailItem, idAoo, null, false, false);
			}

			LOGGER.error(REGISTRAZIONE_ERRORE_MSG + codaEmailItem.getIdNotifica(), e);

			throw new RedException("Errore in fase di gestione della spedizione " + codaEmailItem.getIdNotifica(), e);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICodaMailFacadeSRV#invioMailDaCoda(it.ibm.red.business.dto.MessaggioEmailDTO,
	 *      it.ibm.red.business.persistence.model.Aoo).
	 */
	@Override
	public void invioMailDaCoda(final MessaggioEmailDTO msgNext, final Aoo aoo) {
		IFilenetCEHelper fceh = null;
		final int idAoo = aoo.getIdAoo().intValue();

		try {
			// inizializza accesso a CE filenet
			final AooFilenet aooFilenet = aoo.getAooFilenet();
			fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getObjectStore(), aoo.getIdAoo());

			// recupera il tipo di ricevuta
			final String tipoRicevuta = aoo.getTipoRicevuta() != null ? aoo.getTipoRicevuta().getDescrizione() : "";

			// setta gli allegati al messaggio di posta elettronica
			setAllegati(msgNext, idAoo, fceh);

			LOGGER.info(" item " + msgNext.getIdNotifica() + ": allegati associati all'email");

			msgNext.setTipoRicevuta(tipoRicevuta);

			// invia la mail
			final ResponseSendMail response = sendMail(msgNext, idAoo, fceh);

			LOGGER.info(" sendMail: " + response);

			if (response.isInviato()) {
				managePostInvioMail(response, msgNext, idAoo, fceh, null, true);
			} else if (Boolean.TRUE.equals(msgNext.getUltimoTentativo())) {
				inviaNotificaErroreSpedizione(msgNext, idAoo, response.getErroreInvio(), false, true);
			}
		} catch (final Exception e) {

			// registra l'errore di invio sulla base dati per il messaggio
			registraErroreInvio(msgNext, SOLLEVAMENTO_ECCEZIONE_MSG + e.getMessage(), false);
			if (Boolean.TRUE.equals(msgNext.getUltimoTentativo())) {
				inviaNotificaErroreSpedizione(msgNext, idAoo, null, false, false);
			}
			LOGGER.error(REGISTRAZIONE_ERRORE_MSG + msgNext.getIdNotifica(), e);

		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ICodaMailSRV#managePostInvioMail(it.ibm.red.business.service.concrete.CodaMailSRV.ResponseSendMail,
	 *      it.ibm.red.business.dto.MessaggioEmailDTO, int,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      java.sql.Connection, boolean).
	 */
	@Override
	public void managePostInvioMail(final ResponseSendMail response, final MessaggioEmailDTO msgNext, final int idAoo, final IFilenetCEHelper fceh, final Connection con,
			final boolean gestisciEvento) {
		if (response != null) {
			msgNext.setGuid(response.getGuid());
			msgNext.setMsgID(response.getMsgid());
		}

		managePostInvioMail(msgNext, idAoo, fceh, con, gestisciEvento);

	}

	private void managePostInvioMail(final MessaggioEmailDTO msgNext, final int idAoo, final IFilenetCEHelper fceh, final Connection con, final boolean gestisciEvento) {
		creaAnnotation(msgNext, idAoo, fceh);

		LOGGER.info(" creata annotation per l'item " + msgNext.getIdNotifica());

		if (con == null) {
			registraInvio(msgNext, false);
		} else {
			registraInvio(msgNext, false, con);
		}

		LOGGER.info(" registato l'invio dell'item " + msgNext.getIdNotifica());

		aggiornaElencoTrasmissione(msgNext, idAoo, fceh);

		LOGGER.info(" aggiornato l'elenco trasmissione dell'item " + msgNext.getIdNotifica());

		if (gestisciEvento) {
			gestisciEvento(msgNext, idAoo);

			LOGGER.info(" gestito evento dell'item " + msgNext.getIdNotifica());
		}

	}

	private void inviaNotificaErroreSpedizione(final MessaggioEmailDTO msgNext, final int idAoo, final String inErroreInvio, final boolean spedisciNPS,
			final boolean registraErroreInvio) {
		notificaSRV.writeNotifiche(idAoo, msgNext);

		// registra l'errore di invio sulla base dati per il messaggio
		if (registraErroreInvio) {
			String erroreInvio = Constants.EMPTY_STRING;
			if (!StringUtils.isNullOrEmpty(inErroreInvio)) {
				erroreInvio = inErroreInvio;
			}
			registraErroreInvio(msgNext, erroreInvio, spedisciNPS);
			LOGGER.error(" registato l'errore dell'item " + msgNext.getIdNotifica() + ": " + erroreInvio);
		}

	}

	/**
	 * @see it.ibm.red.business.service.ICodaMailSRV#setAllegati(it.ibm.red.business.dto.MessaggioEmailDTO,
	 *      int, it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void setAllegati(final MessaggioEmailDTO msgNext, final int idAoo, final IFilenetCEHelper fceh) {
		if (!Boolean.TRUE.equals(msgNext.getIsNotifica())) {

			final List<AllegatoDTO> allegatiMail = new ArrayList<>();

			if (msgNext.getTipologiaInvioId() != TipologiaInvio.NUOVOMESSAGGIO) {
				// Allegati
				final DocumentSet allegati = fceh.getAllegati(msgNext.getGuid(), false, true, null, null,
						PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_PADRE_CLASSNAME_FN_METAKEY));

				if (msgNext.getAction() != null && msgNext.getAction().equals(TipoAzioneMailEnum.MAIL_PROTOCOLLA.getValue())) {
					// testo mail
					final AllegatoDTO testoMail = new AllegatoDTO();
					final Document mail = fceh.getDocumentByGuid(msgNext.getGuid());
					testoMail.setNomeFile("Testo_Mail.txt");
					testoMail.setContent(FilenetCEHelper.getDocumentContentAsByte(mail));
					testoMail.setMimeType(mail.get_MimeType());
					allegatiMail.add(testoMail);
				}

				if (msgNext.getHasAllegati() != null && msgNext.getHasAllegati() && allegati != null) {
					final Iterator<Document> it = allegati.iterator();

					while (it.hasNext()) {
						final Document allegatoFilenet = it.next();
						final AllegatoDTO allegato = new AllegatoDTO();
						allegato.setNomeFile(getNomeFileAllegato(allegatoFilenet));
						allegato.setContent(FilenetCEHelper.getDocumentContentAsByte(allegatoFilenet));
						allegato.setMimeType(allegatoFilenet.get_MimeType());
						allegato.setGuid(allegatoFilenet.get_Id().toString());
						allegatiMail.add(allegato);
					}
				}

			} else { // Nuovo messaggio (non di notifica)

				// La action impostata nel messaggio è "InoltraDaCoda"
				if (TipoAzioneMailEnum.MAIL_INOLTRA_DA_CODA.getValue().equalsIgnoreCase(msgNext.getAction())) {
					// Si recuperano da FileNet i documenti (principali e allegati) indicati nella
					// lista "allegatiDocTitle"
					final List<String> msgAllegatiDocTitle = msgNext.getAllegatiDocTitle();
					if (msgAllegatiDocTitle != null && !msgAllegatiDocTitle.isEmpty()) {

						for (int index = 0; index < msgAllegatiDocTitle.size(); index++) {
							Document doc = null;
							// La stringa contenente il Document Title è definita come:
							// "DOCUMENTO_XXXXX" oppure "ALLEGATO_XXXXX", dove XXXXX -> Document Title
							final String[] docTitlePrefisso = msgAllegatiDocTitle.get(index).split("_");
							if (DOCUMENTO.equals(docTitlePrefisso[0])) {
								doc = fceh.getDocumentByIdGestionale(docTitlePrefisso[1], null, null, null, null, null);
							} else {
								doc = fceh.getDocumentByIdGestionale(docTitlePrefisso[1], null, null, null, null,
										pp.getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY));
							}

							if (doc != null) {
								final AllegatoDTO allegatoMessaggio = new AllegatoDTO();
								allegatoMessaggio.setMimeType(doc.get_MimeType());
								allegatoMessaggio.setContent(FilenetCEHelper.getDocumentContentAsByte(doc));
								allegatoMessaggio.setNomeFile(getNomeFileAllegato(doc));
								allegatoMessaggio.setGuid(doc.get_Id().toString());
								allegatiMail.add(allegatoMessaggio);
							}
						}

					}
				} else {
					final Document doc = fceh.getDocumentByIdGestionale(msgNext.getDocumentTitle(), null, null, idAoo, null, null);

					final AllegatoDTO docPrincipale = new AllegatoDTO();
					docPrincipale.setMimeType(doc.get_MimeType());
					docPrincipale.setContent(FilenetCEHelper.getDocumentContentAsByte(doc));
					docPrincipale.setNomeFile(getNomeFileAllegato(doc));
					docPrincipale.setGuid(doc.get_Id().toString());
					allegatiMail.add(docPrincipale);

					final DocumentSet allegatiDoc = fceh.getAllegatiConContent(docPrincipale.getGuid());
					if (allegatiDoc != null) {

						final Iterator<Document> it = allegatiDoc.iterator();
						while (it.hasNext()) {
							final Document allegatoFilenet = it.next();

							final AllegatoDTO allegato = new AllegatoDTO();
							allegato.setNomeFile(getNomeFileAllegato(allegatoFilenet));
							allegato.setContent(FilenetCEHelper.getDocumentContentAsByte(allegatoFilenet));
							allegato.setMimeType(allegatoFilenet.get_MimeType());
							allegato.setGuid(allegatoFilenet.get_Id().toString());
							allegatiMail.add(allegato);
						}
					}
				}
			}

			// Si allegano i documenti recuperati al messaggio
			msgNext.setAllegatiMail(allegatiMail);
		}
	}

	private String getNomeFileAllegato(final Document allegatoFilenet) {
		String nomeFile = (String) TrasformerCE.getMetadato(allegatoFilenet, PropertiesNameEnum.NOME_FILE_METAKEY);
		if (StringUtils.isNullOrEmpty(nomeFile)) {
			nomeFile = (String) TrasformerCE.getMetadato(allegatoFilenet, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
		}
		return nomeFile;
	}

	/**
	 * @see it.ibm.red.business.service.ICodaMailSRV#sendMail(it.ibm.red.business.dto.MessaggioEmailDTO,
	 *      int, it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public ResponseSendMail sendMail(final MessaggioEmailDTO messaggio, final int idAoo, final IFilenetCEHelper fceh) {
		final ResponseSendMail response = new ResponseSendMail();

		final CasellaPostaDTO cp = casellePostaliSRV.getCasellaPostale(fceh, messaggio.getMittente());

		final Document message = validaESalvaMailDaInviare(response, messaggio, cp, null, fceh);

		if (!response.isInviato()) {
			return response;
		}

		final EmailHelper emh = new EmailHelper("CodaMailSRV", cp, messaggio);
		emh.sendEmail();

		aggiornaMessaggioPostInvio(response, messaggio, message, fceh);

		return response;
	}

	/**
	 * @see it.ibm.red.business.service.ICodaMailSRV#sendMail(it.ibm.red.business.dto.MessaggioEmailDTO,
	 *      int, it.ibm.red.business.dto.FilenetCredentialsDTO).
	 */
	@Override
	public ResponseSendMail sendMail(final MessaggioEmailDTO messaggio, final int idAoo, final FilenetCredentialsDTO fcDTO) {
		IFilenetCEHelper fceh = null;
		ResponseSendMail result = null;
		try {
			fceh = FilenetCEHelperProxy.newInstance(fcDTO, (long)idAoo);
			result = sendMail(messaggio, idAoo, fceh);
		} catch (Exception e) {
			LOGGER.warn(e);
		} finally {
			if (fceh != null) {
				fceh.popSubject();
			}
		}
		return result;
	}
	
	
	private Document validaESalvaMailDaInviare(final ResponseSendMail response, final MessaggioEmailDTO messaggio, final CasellaPostaDTO cp, final Connection conn, final IFilenetCEHelper fceh) {
		response.setInviato(true);

		final ErroriMailEnum validationError = valida(messaggio);
		if (validationError != null) {
			LOGGER.warn("Si è verificato un errore in fase di validazione del messaggio [" + messaggio + "]");
			response.setErroreInvio(validationError.getDescription());
			response.setInviato(false);
			return null;
		}

		if (cp == null) {
			LOGGER.warn("Si è verificato un errore durante il recupero della casella mail. [" + messaggio.getMittente() + "]");
			response.setErroreInvio(ErroriMailEnum.NOCASELLAPOSTALEONFILENET.getDescription());
			response.setInviato(false);
			return null;
		}

		LOGGER.info("CasellaPostale: " + cp);
		Document message = null;
		if (creaMessaggioSuFilenet(messaggio)) {
			message = salvaMailDaInviare(messaggio, cp.getIndirizzoEmail(), conn, fceh);
		}

		return message;
	}

	private Document salvaMailDaInviare(final MessaggioEmailDTO messaggio, final String indirizzoEmail, final Connection conn, final IFilenetCEHelper fceh) {
		Document message = null;

		Collection<TipoFile> tipiFile = null;
		Connection connection = conn;
		final boolean manageConn = (conn == null);

		try {
			if (manageConn) {
				connection = setupConnection(getDataSource().getConnection(), false);
			}

			tipiFile = tipoFileDAO.getAll(connection);

		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dei tipi file.", e);
		} finally {
			if (manageConn) {
				closeConnection(connection);
			}
		}

		// ### Creazione del messaggio su FileNet
		message = createMessaggio(messaggio, indirizzoEmail, tipiFile, fceh, messaggio.getIdAoo());

		if (message != null) {
			LOGGER.info("Messaggio di posta elettronica creato correttamente su FileNet.");
		} else {
			LOGGER.error("Errore durante la creazione del messaggio di posta elettronica su FileNet.");
		}

		return message;
	}

	private static boolean creaMessaggioSuFilenet(final MessaggioEmailDTO messaggio) {
		return messaggio.getLastMsg() == null || messaggio.getLastMsg() == 1 || messaggio.getIdSpedizioneUnica() != null;
	}

	private void aggiornaMessaggioPostInvio(final ResponseSendMail response, final MessaggioEmailDTO messaggio, final Document message, final IFilenetCEHelper fceh) {
		if (creaMessaggioSuFilenet(messaggio)) {
			if (message != null) {
				final Map<String, Object> metadati = new HashMap<>();
				metadati.put(pp.getParameterByKey(PropertiesNameEnum.MESSAGE_ID), messaggio.getMsgID());
				// Aggiornamento del documento E-mail creato su FileNet con il Message-ID dell
				// mail inviata
				fceh.updateMetadati(message, metadati);

				if (message.get_Id() != null) {
					response.setGuid(message.get_Id().toString());
				}
			} else {
				LOGGER.error(ERROR_UPDATE_DOCUMENT_MSG);
			}
		} else {
			response.setGuid(messaggio.getGuid());
		}

		response.setMsgid(messaggio.getMsgID());
	}

	private ResponseSendMail manageMail(final MessaggioEmailDTO messaggio, final Connection conn, final IFilenetCEHelper fceh) {
		final ResponseSendMail response = new ResponseSendMail();

		final CasellaPostaDTO cp = casellePostaliSRV.getCasellaPostale(fceh, messaggio.getMittente());

		final Document message = validaESalvaMailDaInviare(response, messaggio, cp, conn, fceh);

		if (!response.isInviato()) {
			return response;
		}

		aggiornaMessaggioPostInvio(response, messaggio, message, fceh);

		return response;
	}

	private Document createMessaggio(final MessaggioEmailDTO messaggio, final String indirizzoCasellaPostale, final Collection<TipoFile> tipiFile, final IFilenetCEHelper fceh,
			final Long idAoo) {
		LOGGER.info("Creazione messaggio [" + messaggio + "] per la casellapostale [" + indirizzoCasellaPostale + "]");
		Document message = null;

		try {
			FileDTO documentFile = null;
			final byte[] content = messaggio.getTesto().getBytes();
			documentFile = new FileDTO(ContentType.HTML, content, ContentType.HTML);

			if (messaggio.getTipologiaInvioId() == TipologiaInvio.NUOVOMESSAGGIO) {
				message = fceh.createMailAsDocument(messaggio, indirizzoCasellaPostale, documentFile, tipiFile);
			} else {
				message = fceh.getMailAsDocument(messaggio);

				// popola i metadati dell'oggetto mail da registrare su Filenet
				final Map<String, Object> metadati = fceh.populateMetadatiEmailInoltraRifiuta(messaggio, indirizzoCasellaPostale);

				if (message == null) {
					throw new FilenetException(ErroriMailEnum.NODOCUMENTPERMSGID.getDescription());
				}

				// aggiungi versione mail inoltrata
				InputStream is = null;
				is = new ByteArrayInputStream(documentFile.getContent());
				message = fceh.addVersion(message, is, metadati, documentFile.getFileName(), documentFile.getMimeType(), idAoo);
				is.close();
			}

			LOGGER.info("Email salvata.");
		} catch (final Exception e) {
			LOGGER.error(" Si è verificato un errore durante il salvataggio/recupero del messaggio su filenet. ", e);
		}

		return message;
	}

	/**
	 * Parametri di invio mail.
	 */
	public class ResponseSendMail {

		/**
		 * Flag inviato.
		 */
		private boolean inviato;

		/**
		 * Errore invio.
		 */
		private String erroreInvio;

		/**
		 * Guid.
		 */
		private String guid;

		/**
		 * Message id.
		 */
		private String msgid;

		/**
		 * Costruttore di default inner class.
		 */
		public ResponseSendMail() {
			// Metodo intenzionalmente vuoto.
		}

		/**
		 * Restituisce true se il messaggio è stato inviato, false altrimenti.
		 * 
		 * @return inviato
		 */
		public boolean isInviato() {
			return inviato;
		}

		/**
		 * Imposta il flag inviato.
		 * 
		 * @param inviato
		 */
		public void setInviato(final boolean inviato) {
			this.inviato = inviato;
		}

		/**
		 * Restituisce il messaggio di errore invio.
		 * 
		 * @return errore invio
		 */
		public String getErroreInvio() {
			return erroreInvio;
		}

		/**
		 * Imposta l'errore di invio.
		 * 
		 * @param erroreInvio
		 */
		public void setErroreInvio(final String erroreInvio) {
			this.erroreInvio = erroreInvio;
		}

		/**
		 * Imposta il guid.
		 * 
		 * @return guid
		 */
		public String getGuid() {
			return guid;
		}

		/**
		 * Restituisce il guid.
		 * 
		 * @param guid
		 */
		public void setGuid(final String guid) {
			this.guid = guid;
		}

		/**
		 * Restituisce l'id del messaggio.
		 * 
		 * @return msgid
		 */
		public String getMsgid() {
			return msgid;
		}

		/**
		 * Imposta l'id del messaggio.
		 * 
		 * @param msgid
		 */
		public void setMsgid(final String msgid) {
			this.msgid = msgid;
		}

		/**
		 * @see java.lang.Object#toString().
		 */
		@Override
		public String toString() {
			return ToStringBuilder.reflectionToString(this, new LogStyleNotNull());
		}

	}

	/**
	 * @see it.ibm.red.business.service.ICodaMailSRV#registraErroreInvio(it.ibm.red.business.dto.MessaggioEmailDTO,
	 *      java.lang.String, boolean).
	 */
	@Override
	public void registraErroreInvio(final MessaggioEmailDTO msgNext, final String erroreInvio, final boolean spedisciNPS) {
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), true);

			final Integer nuovoStatoRicevuta = StatoCodaEmailEnum.ERRORE.getStatus();

			// In fase di registrazione dell'avvenuta spedizione da parte di NPS
			// (spedisciNPS = false),
			// cioè se si sta elaborando una notifica, ricevuta da NPS, di avvenuta
			// spedizione per un certo destinatario,
			// deve essere aggiornato con le nuove informazioni il singolo record relativo a
			// quello specifico destinatario,
			// pertanto idSpedizioneNps viene impostato a null
			final Integer idSpedizioneNps = spedisciNPS ? msgNext.getIdSpedizioneNps() : null;

			final Integer upd = gestioneNotificheEmailDAO.aggiornaCodaMail(nuovoStatoRicevuta, FileUtils.serializeMessaggioEmail(msgNext), erroreInvio, new Date(),
					msgNext.getIdNotifica(), msgNext.getIdSpedizioneUnica(), msgNext.getIsSpedizioneUnica(), idSpedizioneNps, true, connection);

			if (upd != 1) {
				LOGGER.error("Errore aggiornamento stato ricevuta email inviata correttamente con ID notifica " + msgNext.getIdNotifica());
			}

			commitConnection(connection);
		} catch (final Exception e) {
			LOGGER.error("Errore nell'aggiornamento dello stato ricevuta email con ID notifica " + msgNext.getIdNotifica(), e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ICodaMailSRV#creaAnnotation(it.ibm.red.business.dto.MessaggioEmailDTO,
	 *      int, it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public void creaAnnotation(final MessaggioEmailDTO msgNext, final int idAoo, final IFilenetCEHelper ceh) {
		ceh.creaAnnotation(msgNext.getDocumentTitle(), msgNext.getMsgID(), FilenetAnnotationEnum.TEXT_MAIL_MESSAGE_ID, (long) idAoo);
	}

	/**
	 * @see it.ibm.red.business.service.ICodaMailSRV#registraInvio(it.ibm.red.business.dto.MessaggioEmailDTO,
	 *      boolean).
	 */
	@Override
	public void registraInvio(final MessaggioEmailDTO msgNext, final boolean spedisciNPS) {

		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), true);

			registraInvio(msgNext, spedisciNPS, connection);

			commitConnection(connection);
		} catch (final Exception e) {
			LOGGER.error("Errore nell'aggiornamento dello stato ricevuta email con idnotifica " + msgNext.getIdNotifica(), e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}

	}

	private void registraInvio(final MessaggioEmailDTO msgNext, final boolean spedisciNPS, final Connection connection) {
		Integer nuovoStatoRicevuta = getStatoRicevuta(msgNext, spedisciNPS);
		
		if(StatoCodaEmailEnum.CHIUSURA.getStatus().equals(nuovoStatoRicevuta) && !StringUtils.isNullOrEmpty(msgNext.getMittente())) {
			Integer tipoMittente = gestioneNotificheEmailDAO.getTipoMittenteCasellaPostale(msgNext.getMittente(),connection);
			if(tipoMittente!=null && tipoMittente == TipologiaDestinatariMittenti.TIPOLOGIA_DESTINATARI_MITTENTI_PEC) {
				nuovoStatoRicevuta = StatoCodaEmailEnum.ATTESA_ACCETTAZIONE.getStatus();
			}
		} 
		
		// In fase di registrazione dell'avvenuta spedizione da parte di NPS
		// (spedisciNPS = false),
		// cioè se si sta elaborando una notifica, ricevuta da NPS, di avvenuta
		// spedizione per un certo destinatario
		// deve essere aggiornato con le nuove informazioni il singolo record relativo a
		// quello specifico destinatario,
		// pertanto idSpedizioneNps viene impostato a null
		final Integer idSpedizioneNps = spedisciNPS ? msgNext.getIdSpedizioneNps() : null;

		final Integer upd = gestioneNotificheEmailDAO.aggiornaCodaMail(nuovoStatoRicevuta, FileUtils.serializeMessaggioEmail(msgNext), msgNext.getMsgID(), new Date(),
				msgNext.getIdNotifica(), msgNext.getIdSpedizioneUnica(), msgNext.getIsSpedizioneUnica(), idSpedizioneNps, !spedisciNPS, connection);

		if (upd < 1) {
			LOGGER.error("Errore aggiornamento stato ricevuta email inviata correttamente con idnotifica " + msgNext.getIdNotifica());
		}
	}

	private static Integer getStatoRicevuta(final MessaggioEmailDTO msgNext, final boolean spedisciNPS) {
		Integer nuovoStatoRicevuta = StatoCodaEmailEnum.ATTESA_SPEDIZIONE_NPS.getStatus();

		if (!spedisciNPS) {
			nuovoStatoRicevuta = StatoCodaEmailEnum.ATTESA_ACCETTAZIONE.getStatus();
			if (msgNext.getTipoDestinatario() == Constants.TipologiaDestinatariMittenti.TIPOLOGIA_DESTINATARI_MITTENTI_PEO
					|| (msgNext.getAction() != null && msgNext.getAction().equals(TipoAzioneMailEnum.MAIL_INOLTRA_DA_CODA.getValue()))) {
				nuovoStatoRicevuta = StatoCodaEmailEnum.CHIUSURA.getStatus();
			}
		}

		return nuovoStatoRicevuta;
	}

	/**
	 * @see it.ibm.red.business.service.ICodaMailSRV#aggiornaElencoTrasmissione(it.ibm.red.business.dto.MessaggioEmailDTO,
	 *      int, it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public void aggiornaElencoTrasmissione(final MessaggioEmailDTO msgNext, final int idAoo, final IFilenetCEHelper fceh) {

		if (msgNext.getTipologiaInvioId() == TipologiaInvio.NUOVOMESSAGGIO && (msgNext.getLastMsg() == 1 || msgNext.getIdSpedizioneUnica() != null)) {

			fceh.aggiornaElencoTrasmisione(msgNext.getDocumentTitle(), idAoo, msgNext.getDestinatari2());

			LOGGER.info("Aggiornato l'elenco trasmissione del document " + msgNext.getDocumentTitle());
		}

	}

	/**
	 * @see it.ibm.red.business.service.ICodaMailSRV#gestisciEvento(it.ibm.red.business.dto.MessaggioEmailDTO,
	 *      int).
	 */
	@Override
	public void gestisciEvento(final MessaggioEmailDTO msgNext, final int idAoo) {

		// Se TIPO_EVENTO è valorizzato, si inserisce un record nella D_EVENTI_CUSTOM
		// per tracciare l'evento
		if (msgNext.getTipoEvento() != null && msgNext.getTipoEvento() != 0 && (msgNext.getLastMsg() == 1 || msgNext.getIdSpedizioneUnica() != null)) {
			LOGGER.info("TipoEvento del Messaggio valorizzato, si procede con l'inserimento dell'evento : " + msgNext.getTipoEvento() + " per il documento: "
					+ msgNext.getDocumentTitle());

			Connection connection = null;
			try {
				connection = setupConnection(getFilenetDataSource().getConnection(), true);

				final StringBuilder motivazioneAssegnazione = new StringBuilder("");
				// Evento "INOLTRA_MAIL_DA_CODA_ATTIVA"
				if (msgNext.getAction().equals(TipoAzioneMailEnum.MAIL_INOLTRA_DA_CODA.getValue())
						&& Integer.parseInt(EventTypeEnum.INOLTRO_MAIL_DA_CODA_ATTIVA.getValue()) == msgNext.getTipoEvento()) {
					motivazioneAssegnazione.append("Inviato messaggio da casella e-mail: ").append(msgNext.getMittente()).append(". ID messaggio: ")
							.append(msgNext.getMsgID());
				}

				eventoSRV.inserisciEventoLog(Integer.parseInt(msgNext.getDocumentTitle()), msgNext.getIdNodoMittente(), msgNext.getIdUtenteMittente(), (long) 0, (long) 0,
						Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant()), EventTypeEnum.get(msgNext.getTipoEvento()),
						motivazioneAssegnazione.toString(), 0, (long) idAoo, connection);

				commitConnection(connection);

				LOGGER.info("Inserimento dell'evento : " + msgNext.getTipoEvento() + " per il documento: " + msgNext.getDocumentTitle());

			} catch (final Exception e) {
				LOGGER.error("Errore nell'inserimento dell'evento nello storico " + msgNext.getIdNotifica(), e);
				rollbackConnection(connection);
			} finally {
				closeConnection(connection);
			}
		}
	}

	/**
	 * @see it.ibm.red.business.service.ICodaMailSRV#creaMailEInserisciInCoda(java.lang.String,
	 *      java.lang.String, java.lang.String, java.util.List, java.util.List,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 *      java.lang.Integer, java.lang.Integer, java.lang.Boolean,
	 *      java.lang.Boolean, java.lang.Integer, java.lang.Integer,
	 *      it.ibm.red.business.dto.UtenteDTO, java.sql.Connection, boolean).
	 */
	@Override
	public void creaMailEInserisciInCoda(final String guid, final String casellaPostale, final String inMittente, final List<DestinatarioCodaMailDTO> destinatariToList,
			final List<DestinatarioCodaMailDTO> destinatariCcList, final String oggettoMail, final String testoMail, final String azioneMail, final String motivazioneMail,
			final Integer inTipologiaInvioId, final Integer inTipologiaMessaggioId, final Boolean isNotifica, final Boolean hasAllegati, final Integer modalitaSpedizione,
			final Integer tipoEvento, final UtenteDTO utente, final Connection connection, final boolean simulaInvioNPS) {
		LOGGER.info("creaMailEInserisciInCoda -> CREAZIONE E-MAIL E INSERIMENTO NELLA CODA SUL DB");
		IFilenetCEHelper fcehAdmin = null;

		try {
			final FilenetCredentialsDTO fcAdmin = new FilenetCredentialsDTO(utente.getFcDTO());
			fcehAdmin = FilenetCEHelperProxy.newInstance(fcAdmin, utente.getIdAoo());

			final Document doc = fcehAdmin.getDocument(fcehAdmin.idFromGuid(guid));
			final String messageId = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.MESSAGE_ID);
			final String documentTitle = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);

			String mittente = inMittente;
			if (casellaPostale != null) {
				mittente = casellaPostale;
			} else {
				mittente = mittente.split(";")[0];
			}

			final CasellaPostaDTO casellaPostaleRed = casellePostaliSRV.getCasellaPostale(fcehAdmin, mittente);

			Integer tipologiaMessaggioId = inTipologiaMessaggioId;
			Integer tipologiaInvioId = inTipologiaInvioId;
			if (casellaPostaleRed != null && casellaPostaleRed.getIndirizzoEmail().equals(mittente)) {
				if (tipologiaMessaggioId == null) {
					tipologiaMessaggioId = casellaPostaleRed.getTipologia();
				}
				if (tipologiaInvioId == null) {
					if (Constants.AzioneMail.INOLTRA.equalsIgnoreCase(azioneMail)) {
						tipologiaInvioId = TipologiaInvio.INOLTRA;
					} else {
						tipologiaInvioId = TipologiaInvio.RISPONDI;
					}
				}

				final List<String> destinatari = new ArrayList<>(destinatariToList.size() + destinatariCcList.size());
				int destinatariIndex = 0;

				final StringBuilder destinatariToString = new StringBuilder();
				for (final DestinatarioCodaMailDTO destinatarioTo : destinatariToList) {
					destinatari.add(destinatariIndex, destinatarioTo.getIdContatto() + "," + destinatarioTo.getIndirizzoMail() + ",2,E,TO");
					destinatariToString.append(destinatarioTo.getIndirizzoMail()).append(";");
					destinatariIndex++;
				}

				StringBuilder destinatariCcString = null;
				if (!CollectionUtils.isEmpty(destinatariCcList)) {
					destinatariCcString = new StringBuilder();
					for (final DestinatarioCodaMailDTO destinatarioCc : destinatariCcList) {
						destinatari.add(destinatariIndex, destinatarioCc.getIdContatto() + "," + destinatarioCc.getIndirizzoMail() + ",2,E,CC");
						destinatariCcString.append(destinatarioCc.getIndirizzoMail()).append(";");
						destinatariIndex++;
					}
				}

				final String destCCString = destinatariCcString != null ? destinatariCcString.toString() : null;
				final String destToString = destinatariToString.toString();

				creaMailEInserisciInCoda(documentTitle, guid, mittente, destinatari, destToString, destCCString, oggettoMail, testoMail, azioneMail, motivazioneMail,
						tipologiaInvioId, tipologiaMessaggioId, messageId, isNotifica, hasAllegati, modalitaSpedizione, tipoEvento, utente, connection, null, simulaInvioNPS);
			} else {
				LOGGER.error("Nessuna casella postale associata a: " + mittente);
			}
		} finally {
			popSubject(fcehAdmin);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ICodaMailSRV#creaMailEInserisciInCoda(java.lang.String,
	 *      java.lang.String, java.lang.String, java.util.Collection,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.Integer,
	 *      java.lang.Integer, java.lang.String, java.lang.Boolean,
	 *      java.lang.Boolean, java.lang.Integer, java.lang.Integer,
	 *      it.ibm.red.business.dto.UtenteDTO, java.sql.Connection,
	 *      java.lang.Boolean, boolean).
	 */
	@Override
	public void creaMailEInserisciInCoda(final String idDocumento, final String guid, final String mittente, final Collection<?> destinatari, final String destinatariTo,
			final String destinatariCc, final String oggettoMail, final String inTestoMail, final String azioneMail, final String motivazioneMail,
			final Integer tipologiaInvioId, final Integer tipologiaMessaggioId, final String messageId, final Boolean isNotifica, final Boolean hasAllegati,
			final Integer modalitaSpedizione, final Integer tipoEvento, final UtenteDTO utente, final Connection connection, final Boolean firmaDigitale,
			final boolean simulaInvioNPS) {

		creaMailEInserisciInCoda(idDocumento, guid, mittente, destinatari, destinatariTo, destinatariCc, oggettoMail, inTestoMail, azioneMail, motivazioneMail,
				tipologiaInvioId, tipologiaMessaggioId, messageId, isNotifica, hasAllegati, modalitaSpedizione, tipoEvento, utente, connection, firmaDigitale, null,
				simulaInvioNPS);
	}

	/**
	 * @see it.ibm.red.business.service.ICodaMailSRV#creaMailEInserisciInCoda(java.lang.String,
	 *      java.lang.String, java.lang.String, java.util.Collection,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.Integer,
	 *      java.lang.Integer, java.lang.String, java.lang.Boolean,
	 *      java.lang.Boolean, java.lang.Integer, java.lang.Integer,
	 *      it.ibm.red.business.dto.UtenteDTO, java.sql.Connection,
	 *      java.lang.Boolean, java.util.List, boolean).
	 */
	@Override
	public void creaMailEInserisciInCoda(final String idDocumento, final String guid, final String mittente, final Collection<?> inDestinatari, final String destinatariTo,
			final String destinatariCc, final String oggettoMail, final String inTestoMail, final String azioneMail, final String motivazioneMail,
			final Integer tipologiaInvioId, final Integer tipologiaMessaggioId, final String messageId, final Boolean isNotifica, final Boolean hasAllegati,
			final Integer modalitaSpedizione, final Integer tipoEvento, final UtenteDTO utente, final Connection connection, final Boolean firmaDigitale,
			final List<String> allegatiDocTitle, final boolean simulaInvioNPS) {

		creaMailEInserisciInCoda(idDocumento, guid, mittente, inDestinatari, destinatariTo, destinatariCc, oggettoMail, inTestoMail, azioneMail, motivazioneMail,
				tipologiaInvioId, tipologiaMessaggioId, messageId, isNotifica, hasAllegati, modalitaSpedizione, tipoEvento, utente, connection, firmaDigitale,
				allegatiDocTitle, false, simulaInvioNPS);
	}

	/**
	 * @see it.ibm.red.business.service.ICodaMailSRV#creaMailEInserisciInCoda(java.lang.String,
	 *      java.lang.String, java.lang.String, java.util.Collection,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.Integer,
	 *      java.lang.Integer, java.lang.String, java.lang.Boolean,
	 *      java.lang.Boolean, java.lang.Integer, java.lang.Integer,
	 *      it.ibm.red.business.dto.UtenteDTO, java.sql.Connection,
	 *      java.lang.Boolean, java.util.List, boolean, boolean).
	 */
	@Override
	public Long creaMailEInserisciInCoda(final String idDocumento, final String guid, final String mittente, final Collection<?> inDestinatari, final String destinatariTo,
			final String destinatariCc, final String oggettoMail, final String inTestoMail, final String azioneMail, final String motivazioneMail,
			final Integer tipologiaInvioId, final Integer tipologiaMessaggioId, final String messageId, final Boolean isNotifica, final Boolean hasAllegati,
			final Integer modalitaSpedizione, final Integer tipoEvento, final UtenteDTO utente, final Connection connection, final Boolean firmaDigitale,
			final List<String> allegatiDocTitle, final boolean iterManualeSpedizione, final boolean simulaInvioNPS) {
		LOGGER.info("creaMailEInserisciInCoda -> INIZIO CREAZIONE E-MAIL E INSERIMENTO NELLA CODA SUL DB");
		// destinatari è un array di stringhe del tipo: "IDCONTATTO,MAIL,2,E,TO" oppure
		// "IDCONTATTO,MAIL,2,E,CC"
		// destinatariTo e destinatariCc sono stringhe costituite dalla concatenazione
		// degli indirizzi mail dei destinatari
		// rispettivamente in TO e in CC, separati da ";"

		String testoMail = inTestoMail;
		testoMail = StringUtils.convertiTestoMailHtml(testoMail);

		// Si normalizza il metadato che contiene i destinatari del documento per
		// evitare errori comuni
		// durante lo split delle singole stringhe dei destinatari
		final List<String[]> destinatari = DestinatariRedUtils.normalizzaMetadatoDestinatariDocumento(inDestinatari);

		final MessaggioEmailDTO messaggio = new MessaggioEmailDTO(idDocumento, destinatariTo, destinatariCc, mittente, oggettoMail, testoMail, tipologiaInvioId,
				tipologiaMessaggioId, messageId, guid, isNotifica, hasAllegati, azioneMail, org.apache.commons.lang3.StringUtils.join(destinatari, "###;###"), motivazioneMail,
				allegatiDocTitle, utente.getIdUfficio(), utente.getId());

		final List<String[]> arrBoth = new ArrayList<>();
		if (destinatariTo != null && !destinatariTo.isEmpty()) {
			for (int z = 0; z < destinatari.size(); z++) {
				final String[] destinatario = destinatari.get(z);

				if (TipoDestinatario.ESTERNO.equalsIgnoreCase(destinatario[Mail.IE_MAIL_POSITION])
						&& ModalitaDestinatarioEnum.TO.name().equalsIgnoreCase(destinatario[Mail.CC_TO_MAIL_POSITION])
						&& TipoSpedizione.MEZZO_ELETTRONICO.equals(Integer.parseInt(destinatario[Mail.MEZZO_SPEDIZIONE_POSITION]))) {

					arrBoth.add(destinatario);
				}
			}
		}

		if (destinatariCc != null && !destinatariCc.isEmpty()) {
			for (int z = 0; z < destinatari.size(); z++) {
				final String[] destinatario = destinatari.get(z);

				if (TipoDestinatario.ESTERNO.equalsIgnoreCase(destinatario[Mail.IE_MAIL_POSITION])
						&& ModalitaDestinatarioEnum.CC.name().equalsIgnoreCase(destinatario[Mail.CC_TO_MAIL_POSITION])
						&& TipoSpedizione.MEZZO_ELETTRONICO.equals(Integer.parseInt(destinatario[Mail.MEZZO_SPEDIZIONE_POSITION]))) {

					arrBoth.add(destinatario);
				}
			}
		}

		// IN CASO DI FLUSSO RL VIENE SOVRASCRITTO LO STATO PER NON FAR INVIARE SUBITO
		// LA MAIL
		final Long idAooRL = Long.parseLong(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IDAOO_RL));
		int statoNotifica = Constants.Notifiche.STATO_RICEVUTA_NOTIFICA_EMAIL_INIZIO;
		if (firmaDigitale != null && firmaDigitale && utente.getIdAoo().equals(idAooRL)) {
			statoNotifica = Constants.Notifiche.STATO_RICEVUTA_NOTIFICA_EMAIL_ATTESA_SPEDIZIONE_UTENTE;
		}
		if (simulaInvioNPS) {
			statoNotifica = Constants.Notifiche.STATO_RICEVUTA_NOTIFICA_EMAIL_ATTESA_SPEDIZIONE_NPS;
		}

		final Aoo aoo = aooSRV.recuperaAoo(utente.getIdAoo().intValue(), connection);
		final boolean invioPostFirmaNps = firmaDigitale != null && PostaEnum.isInteroperabile(aoo.getPostaInteropEsterna());

		// Nel caso di iter manuale per spedizione setto lo stato a -1 per essere sicuro
		// che a trasformazione pdf invii il doc aggiornato
		if (iterManualeSpedizione) {
			statoNotifica = -1;
		}

		final Long idNotificaSped = inserisciMailInCoda(messaggio, idDocumento, tipologiaInvioId, arrBoth, mittente, statoNotifica, utente.getIdAoo().intValue(),
				modalitaSpedizione, tipoEvento, invioPostFirmaNps, connection);
		LOGGER.info("creaMailEInserisciInCoda -> FINE CREAZIONE E-MAIL E INSERIMENTO NELLA CODA SUL DB");
		return idNotificaSped;
	}

	/**
	 * Inserisce il messaggio da inviare sul DB.
	 * 
	 * @param messaggio          - DTO del messaggio da inviare.
	 * @param idDocumento        - Identificativo del documento.
	 * @param tipoMittente       - Identificativo del tipo di mittente
	 * @param inDestinatariList  - Lista degli identificativi dei destinatari.
	 * @param emailMittente      - Mittente dell'email.
	 * @param statoRicevuta      - Stato della ricevuta.
	 * @param idAoo              - Identificativo dell'AOO.
	 * @param modalitaSpedizione - Modalita di spedizione (unica o singola pre
	 *                           destinatario)
	 * @param tipoEvento         - Evento da gestire
	 * @param invioPostFirmaNps  - Indica se si tratta di un invio a valle di una
	 *                           firma che deve essere effettuato da NPS (posta
	 *                           interoperabile)
	 * 
	 * @param connection         - Connessione al DB.
	 */
	private Long inserisciMailInCoda(final MessaggioEmailDTO messaggio, final String idDocumento, final Integer tipoMittente, final List<String[]> inDestinatariList,
			final String emailMittente, final int statoRicevuta, final int idAoo, final Integer modalitaSpedizione, final Integer tipoEvento, final boolean invioPostFirmaNps,
			final Connection connection) {
		try {
			// Gestione modalità di spedizione "UNICA" (unica mail per tutti i destinatari)
			// e invio mail a valle di una firma
			long idNotificaSpedizione = 0;
			Long idNotifica = null;
			List<Long> idNotificaToUpdateList = null;
			if (((modalitaSpedizione != null && modalitaSpedizione == ModalitaSpedizioneMail.UNICA) || invioPostFirmaNps) && inDestinatariList.size() > 1) {
				idNotificaToUpdateList = new ArrayList<>();
			}

			for (int i = 0; i < inDestinatariList.size(); i++) {
				final String[] destSplit = inDestinatariList.get(i);

				final long idDestinatario = Long.parseLong(destSplit[0]);
				final NotificaEmailDTO notificaEmail = new NotificaEmailDTO(idDestinatario);
				if (idDestinatario > 0) {
					final Contatto contatto = contattoDAO.getContattoByID(idDestinatario, connection);

					// VERIFICO SE IL DESTINATARIO E' CARTACEO
					if (TipoSpedizione.MEZZO_CARTACEO.equals(Integer.parseInt(destSplit[2]))) {

						notificaEmail.setEmailDestinatario("null");
						notificaEmail.setTipoDestinatario(0);

					} else {

						// DESTINATARIO ELETTRONICO: VERIFICO SE PEC O PEO E SETTO LA MAIL
						final String[] emails = EmailHelper.getSelectedMail(contatto, messaggio.getDestinatari(), messaggio.getDestinatariCC());
						final String email = !StringUtils.isNullOrEmpty(emails[1]) ? emails[1] : emails[0];
						final int tipoDestinatario = !StringUtils.isNullOrEmpty(emails[1]) ? 2 : 1;

						notificaEmail.setEmailDestinatario(email);
						notificaEmail.setTipoDestinatario(tipoDestinatario);
						messaggio.setDestinatario(email);

					}
				} else {
					final String email = destSplit[1];
					notificaEmail.setEmailDestinatario(email);
					notificaEmail.setTipoDestinatario(1);
					messaggio.setDestinatario(email);
				}

				notificaEmail.setIdDocumento(idDocumento);
				notificaEmail.setStatoRicevuta(statoRicevuta);
				notificaEmail.setTipoMittente(tipoMittente);
				notificaEmail.setEmailMittente(emailMittente);
				notificaEmail.setTipoEvento(tipoEvento);

				if ((i + 1) == inDestinatariList.size()) {
					messaggio.setLastMsg(1);
				} else {
					messaggio.setLastMsg(0);
				}

				if (ModalitaDestinatarioEnum.CC.name().equalsIgnoreCase(destSplit[Constants.Mail.CC_TO_MAIL_POSITION])) {
					messaggio.setFlagToCc(2);
				} else {
					messaggio.setFlagToCc(1);
				}

				notificaEmail.setIdAoo(idAoo);

				final CodaEmail codaEmail = new CodaEmail(notificaEmail, FileUtils.serializeMessaggioEmail(messaggio), messaggio.getFlagToCc());

				// Inserimento della notifica email
				idNotifica = gestioneNotificheEmailDAO.insertNotificaEmail(codaEmail, connection);

				// Gestione modalità spedizione "UNICA" (unica mail per tutti i destinatari) e
				// invio mail a valle di una firma
				// nel caso in cui ci sia più di un destinatario
				if (((modalitaSpedizione != null && modalitaSpedizione == ModalitaSpedizioneMail.UNICA) || invioPostFirmaNps) && inDestinatariList.size() > 1) {
					idNotificaToUpdateList.add(idNotifica);
					if (i == 0) {
						idNotificaSpedizione = idNotifica;
					}
				}
			}

			if (inDestinatariList.size() > 1) {
				// Gestione modalità spedizione "UNICA" (unica mail per tutti i destinatari):
				// aggiornamento delle notifiche inserite
				if (modalitaSpedizione != null && modalitaSpedizione == ModalitaSpedizioneMail.UNICA) {
					gestioneNotificheEmailDAO.updateSpedizioneUnicaNotificaEmail(idNotificaToUpdateList, idNotificaSpedizione, connection);
				}

				// Gestione invio mail tramite NPS (posta interoperabile) a valle di una firma:
				// aggiornamento delle notifiche inserite
				if (invioPostFirmaNps) {
					gestioneNotificheEmailDAO.updateSpedizioneNps(idNotificaToUpdateList, idNotificaSpedizione, connection);
				}
			}
			return idNotifica;
		} catch (final Exception e) {
			LOGGER.error("Errore nell'invio della notifica " + e.getMessage(), e);
			return null;
		}
	}

	private static ErroriMailEnum valida(final MessaggioEmailDTO messaggio) {
		if (messaggio.getMittente() == null || messaggio.getMittente().trim().length() == 0) {
			return ErroriMailEnum.NOMITTENTE;
		} else {
			if (!controllaFormattazioneEmail(messaggio.getMittente())) {
				return ErroriMailEnum.EMAILMITTENTEFORMAT;
			}
		}
		if (messaggio.getDestinatari() == null || messaggio.getDestinatari().trim().length() == 0) {
			return ErroriMailEnum.NODESTINATARI;
		} else {
			final String[] dest = messaggio.getDestinatari().split(";");
			for (final String string : dest) {
				if (!string.trim().isEmpty() && !controllaFormattazioneEmail(string.trim())) {
					return ErroriMailEnum.EMAILDESTINATARIFORMAT;
				}
			}
		}

		if (messaggio.getOggetto() == null || messaggio.getOggetto().trim().length() == 0) {
			return ErroriMailEnum.NOOGGETTOEMAIL;
		}

		if (messaggio.getTipologiaMessaggioId() == null) {
			return ErroriMailEnum.NOOBJECTTIPOLOGIAMESSAGGIO;
		}

		if (messaggio.getTipologiaInvioId() == null) {
			return ErroriMailEnum.NOOBJECTTIPOLOGIAINVIO;
		}

		if (messaggio.getTipologiaMessaggioId() <= 0 || messaggio.getTipologiaMessaggioId() >= 4) {
			return ErroriMailEnum.TIPOLOGIARANGE;
		}

		if (messaggio.getTipologiaInvioId() < 0 || messaggio.getTipologiaInvioId() >= 5) {
			return ErroriMailEnum.TIPOLOGIARANGE;
		}

		if (messaggio.getTipologiaInvioId() != 1 && (messaggio.getMsgID() == null || "".equals(messaggio.getMsgID().trim()))) {
			return ErroriMailEnum.NOMESSAGEID;
		}

		return null;
	}

	private static boolean controllaFormattazioneEmail(final String email) {

		final Pattern p = Pattern.compile(".+@.+\\.[a-zA-Z]+");
		final Matcher m = p.matcher(email);
		return m.matches();

	}

	/**
	 * @see it.ibm.red.business.service.facade.ICodaMailFacadeSRV#checkAndUpdateNotificaMailStatus(it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper,
	 *      it.ibm.red.business.persistence.model.Aoo).
	 */
	@Override
	public void checkAndUpdateNotificaMailStatus(final IFilenetCEHelper fceh, final Aoo aoo) {
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			final List<CodaEmail> notificheEmailToCheck = gestioneNotificheEmailDAO
					.getNotificheEmailByNoStatoRicevuta(Arrays.asList(Notifiche.STATO_RICEVUTA_NOTIFICA_EMAIL_CHIUSURA, Notifiche.STATO_RICEVUTA_NOTIFICA_EMAIL_SPEDITO,
							Notifiche.STATO_RICEVUTA_NOTIFICA_EMAIL_ATTESA_SPEDIZIONE_UTENTE, Notifiche.STATO_RICEVUTA_NOTIFICA_EMAIL_INIZIO,
							Notifiche.STATO_RICEVUTA_NOTIFICA_EMAIL_ERRORE, Notifiche.STATO_RICEVUTA_NOTIFICA_EMAIL_ITER_SPEDIZIONE_DA_TRASFORMARE_PDF,
							Notifiche.STATO_RICEVUTA_NOTIFICA_EMAIL_ATTESA_SPEDIZIONE_NPS), aoo.getIdAoo().intValue(), connection);

			LOGGER.info(notificheEmailToCheck.size() + " notifiche da gestire per l'aoo " + aoo.getIdAoo());

			// BEGIN - crea una mappa che per ogni key associa la lista delle notifiche
			// appartenenti ad uno specifico documento
			final HashMap<String, List<CodaEmail>> notificheDocumento = new HashMap<>();

			notificheEmailToCheck.forEach(notificaEmail -> {
				if (notificaEmail != null) {
					if (notificheDocumento.get(notificaEmail.getIdDocumento()) != null) {
						notificheDocumento.get(notificaEmail.getIdDocumento()).add(notificaEmail);
					} else {
						final List<CodaEmail> neList = new ArrayList<>();
						neList.add(notificaEmail);
						notificheDocumento.put(notificaEmail.getIdDocumento(), neList);
					}
				}
			});
			// END - crea una mappa che per ogni key associa la lista delle notifiche
			// appartenenti ad uno specifico documento

			for (final Map.Entry<String, List<CodaEmail>> entry : notificheDocumento.entrySet()) {
				final List<CodaEmail> currNotificaEmailList = entry.getValue();
				if (currNotificaEmailList != null && !currNotificaEmailList.isEmpty()) {

					final String documentTitle = currNotificaEmailList.get(0).getIdDocumento();
					final String mittente = currNotificaEmailList.get(0).getEmailMittente();
					String path = null;
					final String rootCasellePostali = pp.getParameterByKey(PropertiesNameEnum.FOLDER_ELETTRONICI_FN_METAKEY).split("\\|")[1];
					final String cartellaNotifiche = pp.getParameterByKey(PropertiesNameEnum.FOLDER_MAIL_NOTIFICHE_FN_METAKEY);
					path = "/" + rootCasellePostali + "/" + mittente + "/" + cartellaNotifiche;

					LOGGER.info("recupero notifiche documento " + documentTitle + " in " + path);

					final Map<String, EmailDTO> notifiche = fceh.getNotifichePEC(documentTitle, mittente, path, aoo, false);

					updateStatoMailWaitingNotifiche(mittente, currNotificaEmailList, notifiche, false);
				}

			}
		} catch (final Exception e) {
			LOGGER.error("Errore generico in fase di riconciliazione delle notifiche.", e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICodaMailFacadeSRV#getNotifichePEC(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String, java.lang.String).
	 */
	@Override
	public Map<String, EmailDTO> getNotifichePEC(final UtenteDTO utente, final String documentTitle, final String mittente, final String path) {
		return getNotifichePEC(utente, documentTitle, mittente, path, false);

	}

	/**
	 * @see it.ibm.red.business.service.facade.ICodaMailFacadeSRV#getNotifichePECList(it.
	 *      ibm.red.business.dto.UtenteDTO, java.lang.String, java.lang.String,
	 *      java.lang.String).
	 */
	@Override
	public List<EmailDTO> getNotifichePECList(final UtenteDTO utente, final String documentTitle, final String mittente, final String path) {
		final Map<String, EmailDTO> map = getNotifichePEC(utente, documentTitle, mittente, path, true);
		return new ArrayList<>(map.values());
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICodaMailFacadeSRV#getNotifichePEC(it.ibm.
	 *      red.business.dto.UtenteDTO, java.lang.String, java.lang.String,
	 *      java.lang.String, boolean).
	 */
	@Override
	public Map<String, EmailDTO> getNotifichePEC(final UtenteDTO utente, final String documentTitle, final String mittente, final String path, final boolean isTabSpedizione) {
		Map<String, EmailDTO> map = null;
		IFilenetCEHelper fceh = null;

		try {
			final Aoo aoo = aooSRV.recuperaAoo(utente.getIdAoo().intValue());
			final AooFilenet aooFilenet = aoo.getAooFilenet();

			fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getObjectStore(), aoo.getIdAoo());

			map = fceh.getNotifichePEC(documentTitle, mittente, path, aoo, isTabSpedizione);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero delle notifiche PEC.", e);
			throw new RedException("Errore durante il recupero delle notifiche PEC", e);
		} finally {
			popSubject(fceh);
		}

		return map;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICodaMailFacadeSRV#
	 *      getNotificheInteroperabilita(java.lang.String, int, int,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public List<NotificaInteroperabilitaEmailDTO> getNotificheInteroperabilita(final String idProtocollo, final Integer numeroProtocollo, final Integer annoProtocollo,
			final UtenteDTO utente) {
		final List<NotificaInteroperabilitaEmailDTO> notificheInterop = new ArrayList<>();

		if (numeroProtocollo != null && annoProtocollo != null) {
			IFilenetCEHelper fceh = null;

			try {
				fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

				final DocumentSet notificheInteropFilenet = fceh.getNotificheInteroperabilitaByProtocollo(idProtocollo, numeroProtocollo, annoProtocollo);

				if (notificheInteropFilenet != null && !notificheInteropFilenet.isEmpty()) {
					NotificaInteroperabilitaEmailDTO notificaInterop;

					String tipoNotificaInterop;
					final Iterator<?> itNotificheInteropFilenet = notificheInteropFilenet.iterator();
					while (itNotificheInteropFilenet.hasNext()) {
						final Document notificaInteropFilenet = (Document) itNotificheInteropFilenet.next();

						notificaInterop = new NotificaInteroperabilitaEmailDTO();
						notificaInterop.setGuid(notificaInteropFilenet.get_Id().toString());
						notificaInterop.setMsgID((String) TrasformerCE.getMetadato(notificaInteropFilenet, PropertiesNameEnum.MESSAGE_ID));
						notificaInterop.setMittente((String) TrasformerCE.getMetadato(notificaInteropFilenet, PropertiesNameEnum.FROM_MAIL_METAKEY));
						notificaInterop.setOggetto((String) TrasformerCE.getMetadato(notificaInteropFilenet, PropertiesNameEnum.OGGETTO_EMAIL_METAKEY));
						notificaInterop.setDataRicezione((Date) TrasformerCE.getMetadato(notificaInteropFilenet, PropertiesNameEnum.DATA_RICEZIONE_MAIL_METAKEY));
						notificaInterop.setDestinatari((String) TrasformerCE.getMetadato(notificaInteropFilenet, PropertiesNameEnum.DESTINATARI_EMAIL_METAKEY));

						// Si ricava la tipologia di notifica interoperabile a partire dall'oggetto
						// della notifica stessa
						tipoNotificaInterop = Constants.EMPTY_STRING;
						if (!StringUtils.isNullOrEmpty(notificaInterop.getOggetto())) {
							for (final TipoNotificaInteropEnum tipoNotificaInteropEnum : TipoNotificaInteropEnum.values()) {
								if (notificaInterop.getOggetto().toLowerCase().contains(tipoNotificaInteropEnum.getDisplayName().toLowerCase())) {
									tipoNotificaInterop = tipoNotificaInteropEnum.getDisplayName();
									break;
								}
							}
						}
						notificaInterop.setTipoNotificaInterop(tipoNotificaInterop);

						handleNotificaText(notificaInterop, notificaInteropFilenet);

						notificheInterop.add(notificaInterop);
					}
				}
			} catch (final Exception e) {
				LOGGER.error("Si è verificato un errore durante il recupero delle notifiche di interoperabilità per il protocollo: " + numeroProtocollo + "/" + annoProtocollo
						+ (!StringUtils.isNullOrEmpty(idProtocollo) ? ", ID: " + idProtocollo : ""), e);
				throw new RedException("Errore durante il recupero delle notifiche di interoperabilità.", e);
			} finally {
				popSubject(fceh);
			}
		}

		return notificheInterop;
	}

	/**
	 * Gestisce il testo della notifica interoperabile.
	 * 
	 * @param notificaInterop
	 * @param notificaInteropFilenet
	 */
	private void handleNotificaText(final NotificaInteroperabilitaEmailDTO notificaInterop, final Document notificaInteropFilenet) {
		try {
			notificaInterop.setTesto(new String(FilenetCEHelper.getDocumentContentAsByte(notificaInteropFilenet)));
		} catch (final Exception e) {
			LOGGER.warn(e);
			notificaInterop.setTesto(null);
		}
	}

	/**
	 * Esegue l'aggiornamento dello stato degli item sulla coda email per le mail in
	 * attesa di notifica.
	 * 
	 * @param mittente          relativo al messaggio originale
	 * @param notificaEmailList contiene tutte le email in attesa di notifica per
	 *                          uno stesso documento/mittente
	 * @param notifiche         notifiche pervenute relative al messageID relativo
	 *                          al documentTitle delle email in attesa di notifica
	 * @param reInvio           indica se è stata richiesta un reinvio dell'email
	 * @param connection
	 * @return
	 */
	private void updateStatoMailWaitingNotifiche(final String mittente, final List<CodaEmail> notificaEmailList, final Map<String, EmailDTO> notifiche,
			final boolean reInvio) {

		final List<CodaEmail> notificaEmailToUpdateList = new ArrayList<>();
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			// BEGIN - calcolo dello stato successivo per ogni notificaEmail (destinatario
			// del documento)
			if (notificaEmailList != null) {

				for (final CodaEmail notificaEmail : notificaEmailList) {

					LOGGER.info("item " + notificaEmail.getIdNotifica() + " per il documento " + notificaEmail.getIdDocumento() + " con stato "
							+ notificaEmail.getStatoRicevuta());

					final NotificaHelper notificaHelper = new NotificaHelper(mittente);
					final int statoNotifica = notificaEmail.getStatoRicevuta();
					if (statoNotifica == 0) {
						notificaHelper.setGestioneMailState(new InizioState0());
					} else if (statoNotifica == 1) {
						notificaHelper.setGestioneMailState(new AttesaAccettazioneState1());
					} else if (statoNotifica == 2) {
						notificaHelper.setGestioneMailState(new AttesaAvvenutaConsegnaState2());
					} else if (statoNotifica == 3) {
						notificaHelper.setGestioneMailState(new ChiusuraState3());
					} else if (statoNotifica == 4) {
						notificaHelper.setGestioneMailState(new ErroreState4());
					} else if (statoNotifica == 5) {
						notificaHelper.setGestioneMailState(new ReInvioState5());
					}

					final CodaEmail notificaEmailToUpdate = notificaHelper.getNextNotificaEmailToUpdate(notificaEmail, notifiche, reInvio);
					if (notificaEmailToUpdate != null) {
						notificaEmailToUpdateList.add(notificaEmailToUpdate);
					}
				}
			}
			// END - calcolo dello stato successivo per ogni notificaEmail (destinatario del
			// documento)

			// BEGIN - aggiornamento degli stati in tabella
			for (final CodaEmail notifica : notificaEmailToUpdateList) {
				LOGGER.info("aggiornamento destinatario " + notifica.getIdDestinatario() + " per il documento " + notifica.getIdDocumento() + " e idmessaggio "
						+ notifica.getIdMessaggio() + " con stato " + notifica.getStatoRicevuta());
				gestioneNotificheEmailDAO.updateStatoNotificaEmail(notifica.getIdDocumento(), notifica.getIdMessaggio(), notifica.getIdDestinatario(),
						notifica.getStatoRicevuta(), connection);
			}
			// END - aggiornamento degli stati in tabella

			commitConnection(connection);

		} catch (final Exception e) {
			rollbackConnection(connection);
			LOGGER.error("Errore in fase di aggiornamento delle email in attesa di notifica ", e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICodaMailFacadeSRV#
	 *      chiudiDocumentiRiconciliati(it.ibm.red.business.helper.filenet.pe.
	 *      FilenetPEHelper, int).
	 */
	@Override
	public void chiudiDocumentiRiconciliati(final FilenetPEHelper fpeh, final IFilenetCEHelper fceh, final Aoo aoo) {
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			final int idAoo = aoo.getIdAoo().intValue();
			final String idDocuments = gestioneNotificheEmailDAO.getAndDeleteClosedDocument(idAoo, connection);
			if (idDocuments != null) {
				final String[] idDocumentsArray = idDocuments.split(";");
				final List<Long> ids = new ArrayList<>();
				for (final String id : idDocumentsArray) {
					addIfValid(ids, id);
				}

				// effettua lo scodamento dei documenti in stato 3

				final List<Long> idDocumentToRollback = fpeh.dispatchManualeFlussi(ids, Constants.SysResponse.RESPONSE_SYS_SPEDITO);
				if (!idDocumentToRollback.isEmpty()) {
					gestioneNotificheEmailDAO.rollbackGestioneNoticheBatch(idDocumentToRollback, connection);
				}

				// in caso di aoo con la posta gestita da RED, all’atto della riconciliazione
				// di destinatari elettronici per AOO, la spedizione (su workflow), dovrà
				// coincidere con l’invocazione di spedisciProtocollo di NPS
				if (PostaEnum.POSTA_INTERNA.getValue().intValue() == aoo.getPostaInteropEsterna()) {
					for (final Long id : ids) {
						if (idDocumentToRollback.contains(id)) {
							continue;
						}
						LOGGER.info("Aggiorna protocollo associato al documento " + id + " a spedito su NPS");
						spedisciNPS(aoo, id.toString(), fceh, connection, false);
					}
				}

			}
			commitConnection(connection);
		} catch (final SQLException e) {
			LOGGER.error("Errore durante la chiusura dei documenti ", e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Popola la collection <code> ids </code> se l'id è valido.
	 * 
	 * @param ids
	 * @param id
	 */
	private static void addIfValid(final List<Long> ids, final String id) {
		try {
			ids.add(Long.parseLong(id));
			LOGGER.info("chiudiDocumentiRiconciliati per il documento " + id);
		} catch (final NumberFormatException nfe) {
			LOGGER.warn(nfe);
			// skip document title che sicuramente non può stare sul PE
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICodaMailFacadeSRV#
	 *      chiudiDocumentiDestinatariMisti(it.ibm.red.business.helper.filenet.ce.
	 *      IFilenetHelper, it.ibm.red.business.helper.filenet.pe.FilenetPEHelper,
	 *      it.ibm.red.business.persistence.model.Aoo).
	 */
	@Override
	public void chiudiDocumentiDestinatariMisti(final IFilenetCEHelper fceh, final FilenetPEHelper fpeh, final Aoo aoo) {
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), true);

			// Prelevo tutti gli idDocumenti cartacei che non hanno lo stato chiuso
			final List<String> documentsListCartaceo = gestioneNotificheEmailDAO.getIdDocumentiNonChiusiDestOnlyCartacei(connection, aoo.getIdAoo().intValue());
			Long idDoc = null;
			for (final String idDocumento : documentsListCartaceo) {

				idDoc = tryParsingId(idDocumento);
				if (idDoc == null) {
					// Skip document title che sicuramente non può stare sul PE
					continue;
				}

				fpeh.dispatchManualeFlussi(Arrays.asList(idDoc), Constants.SysResponse.RESPONSE_SYS_SPEDITO);

				// in caso di aoo con la posta gestita da RED, all’atto della riconciliazione
				// di destinatari elettronici per AOO, la spedizione (su workflow), dovrà
				// coincidere con l’invocazione di spedisciProtocollo di NPS
				if (PostaEnum.POSTA_INTERNA.getValue().intValue() == aoo.getPostaInteropEsterna()) {
					LOGGER.info("Aggiorna protocollo associato al documento " + idDocumento + " a spedito su NPS");
					spedisciNPS(aoo, idDocumento, fceh, connection, true);
				}

			}

			commitConnection(connection);

		} catch (final SQLException e) {
			LOGGER.error("Si è verificato unerrore durante la chiusura dei documenti", e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}
	}
	
	/**
	 * Esegue il parsing dell'identificativo del documento: {@code idDocumento}. Se
	 * il parsing non va a buon fine vuol dire che il documento caratterizzato
	 * dall'identificativo per il quale viene tentato il parsing non può stare sul
	 * Content Engine di Filenet quindi deve essere ignorato.
	 * 
	 * @param idDocumento
	 *            Identificativo documento per il quale effettuare il parsing.
	 * @return Identificativo documento come Long o {@code null} se l'identificativo
	 *         {@code idDocumento} non è parsabile.
	 */
	private static Long tryParsingId(String idDocumento) {
		Long idDoc = null;
		try {
			idDoc = Long.parseLong(idDocumento);
			LOGGER.info("chiudiDocumentiDestinatariMisti per il documento " + idDocumento);
		} catch (final NumberFormatException nfe) {
			LOGGER.warn("Tentativo di parsing dell'id documento fallito.", nfe);
		}
		return idDoc;
	}

	private void spedisciNPS(final Aoo aoo, final String idDocumento, final IFilenetCEHelper fceh, final Connection connection, final boolean checkProtocolloSpedito) {
		if (TipologiaProtocollazioneEnum.isProtocolloNPS(aoo.getTipoProtocollo().getIdTipoProtocollo())) {

			final Document doc = fceh.getDocumentByIdGestionale(Constants.EMPTY_STRING + idDocumento, null, null, aoo.getIdAoo().intValue(), null, null);
			final Properties props = doc.getProperties();
			final String idProtocollo = props.getStringValue(pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY));

			// Spedizione protocollo in uscita
			if (idProtocollo != null) {

				boolean spedisci = true;
				if (checkProtocolloSpedito) {
					final Integer tipoProtocollo = props.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY));
					final ProtocolloNpsDTO dto = npsSRV.getDatiMinimiByIdProtocollo(aoo.getIdAoo().intValue(), idProtocollo, tipoProtocollo);
					if (dto.getStatoProtocollo() != null && "SPEDITO".equalsIgnoreCase(dto.getStatoProtocollo())) {
						spedisci = false;
					}
				}

				if (spedisci) {
					// infoProtocollo = NUMPROT/ANNOPROT_CODICEAOO
					final String infoProtocollo = new StringBuilder().append(props.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY)))
							.append("/").append(props.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY))).append("_")
							.append(aoo.getCodiceAoo()).toString();

					final UtenteDTO utente = utenteSRV.getAmministratoreAoo(aoo.getCodiceAoo());

					npsSRV.spedisciProtocolloUscitaAsync(utente, infoProtocollo, idProtocollo, new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()), null, null,
							idDocumento, connection);
				}

			}
		}
	}

	private static MessaggioEmailDTO fromCodaEmailToDTO(final CodaEmail item) {
		final MessaggioEmailDTO messaggio = (MessaggioEmailDTO) FileUtils.getObjectFromByteArray(item.getMessaggio());

		messaggio.setIdNotifica(item.getIdNotifica());
		messaggio.setTipoDestinatario(item.getTipoDestinatario());
		messaggio.setIdSpedizioneUnica(item.getIdSpedizioneUnica());
		messaggio.setTipoEvento(item.getTipoEvento());
		messaggio.setUltimoTentativo(item.getCountRetry().equals(item.getMaxRetry() - 1));
		messaggio.setIdDocumento(item.getIdDocumento());
		messaggio.setIdSpedizioneNps(item.getIdSpedizioneNps());

		return messaggio;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICodaMailFacadeSRV#checkEmailInviata(java.
	 *      lang.String, java.lang.String[]).
	 */
	@Override
	public boolean checkEmailInviata(final String idDocumento, final String[] destinatario) {
		Connection con = null;

		boolean emailInviata = true;

		if (destinatario != null) {
			try {
				con = setupConnection(getDataSource().getConnection(), false);
				for (int i = 0; i < destinatario.length; i++) {

					final NotificaEmailDTO notificaEmail = getNotifica(idDocumento, destinatario, con, i);

					if (notificaEmail != null
							&& ((notificaEmail.getTipoDestinatario() == Constants.TipologiaDestinatariMittenti.TIPOLOGIA_DESTINATARI_MITTENTI_PEO)
									|| (notificaEmail.getTipoDestinatario() == Constants.TipologiaDestinatariMittenti.TIPOLOGIA_DESTINATARI_MITTENTI_PEC))
							&& (notificaEmail.getStatoRicevuta() == 4) && (notificaEmail.getCountRetry().equals(notificaEmail.getMaxRetry()))
							&& (notificaEmail.getSpedizione() == 0)) {
						emailInviata = false;
						break;
					}
				}
			} catch (final Exception e) {
				LOGGER.error(e);
			} finally {
				closeConnection(con);
			}
		}

		return emailInviata;
	}

	/**
	 * @param idDocumento
	 * @param destinatario
	 * @param con
	 * @param i
	 * @return notifica
	 */
	private NotificaEmailDTO getNotifica(final String idDocumento, final String[] destinatario, final Connection con, final int i) {

		NotificaEmailDTO notificaEmail = null;
		try {
			notificaEmail = gestioneNotificheEmailDAO.getNotificheEmailByIdDocEmailDest(idDocumento, destinatario[i].trim(), con);
		} catch (final Exception e) {
			LOGGER.error(e);
		}
		return notificaEmail;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICodaMailFacadeSRV#reinviaMail(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 *      boolean).
	 */
	@Override
	public void reinviaMail(final UtenteDTO utente, final String idDocumento, final String mailGuid, final String indirizzoEmailDestinatarioMittente,
			final String indirizzoEmailDestinatario, final String oldEmailDestinatarioReinvia, final String numeroProtocollo, final String annoProtocollo,
			final String idProtocollo, final boolean clickMailTO) {
		// N.B. String oldEmailDestinatarioReinvia, String numeroProtocollo, String
		// annoProtocollo, Integer idProtocollo sono necessari solo per la parte NPS
		IFilenetCEHelper fceh = null;
		Connection con = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			con = setupConnection(getDataSource().getConnection(), true);

			NotificaEmailDTO notificaEmail = null;

			// Si controlla la validità dell'indirizzo e-mail tramite regex
			final Pattern p = Pattern.compile(EmailUtils.VALID_MAIL_ADDRESS);
			final Matcher m = p.matcher(indirizzoEmailDestinatario);
			if (!m.matches()) {
				// E-mail inserita non valida
				throw new RedException("Formato dell'indirizzo e-mail non valido, si prega di riprovare.");
			} else {
				LOGGER.info("REINVIA EMAIL -> START -> IdDocumento : " + idDocumento + " Indirizzo MailDestMittente : " + indirizzoEmailDestinatarioMittente);
				notificaEmail = gestioneNotificheEmailDAO.getNotificheEmailByIdDocEmailDest(idDocumento, indirizzoEmailDestinatarioMittente, con);
				LOGGER.info("REINVIA EMAIL -> END -> notificaEmail : " + notificaEmail);
				if (notificaEmail != null) {
					// 1. aggiornare il record su DB per (colonna "spedizione") valorizzandola ad 1
					// in modo da non far comparire più il tasto "REINVIA" e lo statoricevuta in
					// modo da far scodare il workflow
					LOGGER.info("updateSpeditoNotificaEmail START");
					if (gestioneNotificheEmailDAO.updateSpeditoNotificaEmail(notificaEmail.getIdNotifica(), 1, con) > 0) {
						LOGGER.info("updateSpeditoNotificaEmail END");
						final MessaggioEmailDTO mess = (MessaggioEmailDTO) FileUtils.getObjectFromByteArray(notificaEmail.getMessaggio());
						mess.setIdNotifica(notificaEmail.getIdNotifica());
						mess.setTipoDestinatario(notificaEmail.getTipoDestinatario());
						mess.setDestinatario(indirizzoEmailDestinatario);
						mess.setTipoEvento(notificaEmail.getTipoEvento());

						if (clickMailTO) {
							mess.setFlagToCc(1);
						} else {
							mess.setFlagToCc(2);
						}
 
						notificaEmail.setCountRetry(0);
						notificaEmail.setTipoEvento(null);
						notificaEmail.setEmailDestinatario(indirizzoEmailDestinatario);
						notificaEmail.setStatoRicevuta(Constants.Notifiche.STATO_RICEVUTA_NOTIFICA_EMAIL_INIZIO);
						notificaEmail.setIdMessaggio(null);

						// 2. aggiornare il documento di classe email aggiungendo un destinatario nel
						// metadato CC o TO in base al flag letto dalla notifica appena estratta dal DB
						final Document mail = fceh.getDocumentByGuid(mailGuid);

						String lstDestinatari = "";
						// alreadyExist serve per verificare se il destinatario è già contenuto nella
						// lista
						if (MessaggioEmailDTO.FLAG_TO.equals(mess.getFlagToCc())) {
							lstDestinatari = mail.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_EMAIL_METAKEY));
							lstDestinatari = lstDestinatari.concat(";" + indirizzoEmailDestinatario);
							mail.getProperties().putValue(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_EMAIL_METAKEY), lstDestinatari);
						} else {
							lstDestinatari = mail.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_CC_EMAIL_METAKEY));
							lstDestinatari = lstDestinatari.concat(";" + indirizzoEmailDestinatario);
							mail.getProperties().putValue(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_CC_EMAIL_METAKEY), lstDestinatari);
						}
						mail.save(RefreshMode.REFRESH);

						final CodaEmail codaEmail = new CodaEmail(notificaEmail, FileUtils.serializeMessaggioEmail(mess), mess.getFlagToCc());

						// 3. Inserire il nuovo record nella tabella
						gestioneNotificheEmailDAO.insertNotificaEmail(codaEmail, con);

						// Aggiunge il destinatario al protocollo NPS nel caso la mail inserita non sia
						// già nella lista
						// REINVIA_EMAIL_DESTINATARIO_NPS
						// Su RED va avanti anche se oldEmailDestinatarioReinvia è null ma si ferma
						// dopo, per evitare ci fermiamo prima.
						// Rispetto a RED abbiamo anche il controllo su numeroProtocollo e
						// annoProtocollo.
						if (TipologiaProtocollazioneEnum.isProtocolloNPS(utente.getIdTipoProtocollo())
								&& (utente.getTipoPosta() != null && PostaEnum.isInteroperabile(utente.getTipoPosta().getValue()))
								&& !StringUtils.isNullOrEmpty(oldEmailDestinatarioReinvia)) {

							if (StringUtils.isNullOrEmpty(numeroProtocollo) || StringUtils.isNullOrEmpty(annoProtocollo)) {
								throw new RedException("Informazioni sul protocollo assenti");
							}

							final String numeroAnnoProtocollo = numeroProtocollo + "/" + annoProtocollo + "_" + utente.getCodiceAoo();
							npsSRV.modificaDestinatarioUscitaAsync(numeroAnnoProtocollo, idProtocollo, oldEmailDestinatarioReinvia, indirizzoEmailDestinatario, utente,
									idDocumento, con);
						}
					} else {
						LOGGER.error("Aggiornamento dello spedito fallito per la notifica" + notificaEmail.getIdNotifica() + "necessario un intervento manuale!!");
						throw new RedException(
								"Aggiornamento dello spedito fallito per la notifica " + notificaEmail.getIdNotifica() + "; necessario un intervento manuale!!");
					}
				} else {
					LOGGER.error("Notifica pari a null");
					throw new RedException("Errore durante il reinvio");
				}

				commitConnection(con);
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'invio della mail", e);
			rollbackConnection(con);
			throw new RedException("Si è verificato un errore durante l'invio della mail", e);

		} finally {
			closeConnection(con);
			popSubject(fceh);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.service.ICodaMailSRV#updateTipoDestinatario(List,
	 * String, String, Connection)
	 */
	@Override
	public final void updateTipoDestinatario(final List<DestinatarioDatiCertDTO> destinatariDatiCert, final String messageId, final String inMittente, final Integer idAdminAoo, final Connection con) {
		try {
			if (!CollectionUtils.isEmpty(destinatariDatiCert) && !StringUtils.isNullOrEmpty(messageId)) {
				
				TipoDestinatarioDatiCertEnum tipoDestinatario = destinatariDatiCert.get(0).getTipoDestinatario();
				String mittente = inMittente.replace(";", "");
				 
				int inviaNotificaModifica = gestioneNotificheEmailDAO.updateTipoDestinatario(tipoDestinatario, messageId, mittente, con);
				if(inviaNotificaModifica>0 && idAdminAoo!=null && idAdminAoo!=0) {
					//Recupero id destinatario
					Long idDestinatario = gestioneNotificheEmailDAO.getIdDestinatarioFromGuid(messageId, mittente, con);
					Contatto contattoDaCorreggere = null;
					if(idDestinatario!=null) {
						contattoDaCorreggere = contattoDAO.getContattoSoloREDByID(idDestinatario, con);
					}
						
					if(contattoDaCorreggere!=null && (contattoDaCorreggere.getOnTheFly() == null || contattoDaCorreggere.getOnTheFly() != 1 )) {
						inviaNotificaCorrezioneDestinatario(idAdminAoo, contattoDaCorreggere);
					}
					
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'aggiornamento del tipo destinatario per i destinatari della mail con Message-ID: " + messageId + " e mittente: "
					+ inMittente, e);
			throw new RedException(e);
		}
	}

	private void inviaNotificaCorrezioneDestinatario(final Integer idAdminAoo,final Contatto contattoDaCorreggere){
		UtenteDTO utenteAdmin = utenteSRV.getById(idAdminAoo.longValue());
		if(utenteAdmin!=null) {
			Contatto contNuovo = new Contatto();
			contNuovo.copyFromContatto(contattoDaCorreggere);
			String mailPecTemp = "";
			String mailPeoTemp = "";
			
			//Inverto così da correggere eventuali mail pec in peo e viceversa
			//E dopo li risetto nell'oggetto da modificare
			mailPecTemp = contNuovo.getMail(); 
			mailPeoTemp = contNuovo.getMailPec();
			 
			contNuovo.setMailPec(mailPecTemp);
			contNuovo.setMail(mailPeoTemp);
			NotificaContatto notifica = new NotificaContatto();
			
			notifica.setNuovoContatto(contNuovo);
			notifica.setTipoNotifica(NotificaContatto.TIPO_NOTIFICA_RICHIESTA);
			notifica.setAlias(contNuovo.getAliasContatto());
			notifica.setCognome(contNuovo.getCognome());
			notifica.setNome(contNuovo.getNome());
			notifica.setDataOperazione(new Date());
			notifica.setIdContatto(contNuovo.getContattoID());

			notifica.setIdNodoModifica(utenteAdmin.getIdUfficio());
			notifica.setIdUtenteModifica(utenteAdmin.getId());
			notifica.setNote("Richiesta di modifica generata automaticamente");
			notifica.setTipologiaContatto(contNuovo.getTipoRubrica());
			notifica.setUtente(utenteAdmin.getUsername());
			notifica.setStato(StatoNotificaContattoEnum.IN_ATTESA);
			notifica.setIdAoo(utenteAdmin.getIdAoo());

			rubricaSRV.notificaModificaContatto(notifica, contattoDaCorreggere); 

		}
		
	}

	/**
	 * @see it.ibm.red.business.service.ICodaMailSRV#getItemByDocumentTitleAndEmailDestAndStato(java.lang.String,
	 *      java.lang.String, java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public MessaggioEmailDTO getItemByDocumentTitleAndEmailDestAndStato(final String idDocumento, final String destinatario, final Integer stato, final Connection conn) {
		MessaggioEmailDTO out = null;

		try {
			final CodaEmail item = gestioneNotificheEmailDAO.getNotificheEmailByIdDocEmailDestAndStato(idDocumento, destinatario, stato, conn);
			if (item != null) {
				out = fromCodaEmailToDTO(item);
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero dell'item relativo al documento " + idDocumento + " ed al destinatario : " + destinatario, e);
			throw new RedException(e);
		}

		return out;
	}

	/**
	 * @see it.ibm.red.business.service.ICodaMailSRV#updateNotifica(java.lang.Long).
	 */
	@Override
	public int updateNotifica(final Long idNotifica) {
		Connection conn = null;
		int output;
		try {
			conn = setupConnection(getDataSource().getConnection(), true);
			output = gestioneNotificheEmailDAO.updateNotificaSpedizione(idNotifica, conn);
			commitConnection(conn);
		} catch (final Exception ex) {
			LOGGER.error("Si è verificato un errore durante l'update dello stato della notifica", ex);
			rollbackConnection(conn);
			throw new RedException(ex);
		} finally {
			closeConnection(conn);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICodaMailFacadeSRV#inviaMessaggioPostaNps(it.ibm.red.business.dto.MessaggioEmailDTO,
	 *      it.ibm.red.business.persistence.model.Aoo).
	 */
	@Override
	public void inviaMessaggioPostaNps(final MessaggioEmailDTO msgNext, final Aoo aoo) {
		Connection connection = null;
		IFilenetCEHelper fceh = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			final AooFilenet aooFilenet = aoo.getAooFilenet();
			fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getObjectStore(), aoo.getIdAoo());

			setAllegati(msgNext, aoo.getIdAoo().intValue(), fceh);
			final List<String> idsDocumentiNps = new ArrayList<>();
			if (msgNext.getAllegatiMail() != null && !msgNext.getAllegatiMail().isEmpty()) {
				for (final AllegatoDTO allegatoMail : msgNext.getAllegatiMail()) {
					final String descrizione = "upload_" + allegatoMail.getNomeFile();
					final String idDocumentoNps = npsSRV.uploadDocToNps(aoo.getIdAoo().intValue(), /* firmato */allegatoMail.isDaFirmareBoolean(), allegatoMail.getNomeFile(),
							allegatoMail.getMimeType(), descrizione, new ByteArrayInputStream(allegatoMail.getContent()), /* dataDocumento */ allegatoMail.getDateCreated(),
							allegatoMail.getGuid());
					idsDocumentiNps.add(idDocumentoNps);
				}
			}
			final List<String> emailDestinatario = new ArrayList<>();
			emailDestinatario.add(msgNext.getDestinatario());

			String isPec = "no";
			if (msgNext.getTipoMittente() != null && msgNext.getTipoMittente() == 2) {
				isPec = "si";
			}

			String isUfficiale = "no";
			if (msgNext.getMittente().equals(aoo.getCasellaEmailUfficiale())) {
				isUfficiale = "si";
			}
			final boolean notificaSpedizione = true;

			boolean valida = true;
			String msgId = null;
			String erroreInvio = "Eccezione";
			final ErroriMailEnum validationError = valida(msgNext);
			final CasellaPostaDTO cp = casellePostaliSRV.getCasellaPostale(fceh, msgNext.getMittente());
			if (validationError != null) {
				LOGGER.warn("Si è verificato un errore in fase di validazione del messaggio [" + msgNext + "]");
				erroreInvio = validationError.getDescription();
				valida = false;
			}

			if (cp == null) {
				LOGGER.warn("Si è verificato un errore durante il recupero della casella mail. [" + msgNext.getMittente() + "]");
				erroreInvio = ErroriMailEnum.NOCASELLAPOSTALEONFILENET.getDescription();
				valida = false;
			}

			if (valida) {
				msgId = npsSRV.inviaMessaggioPosta(msgNext.getIdNotifica(), msgNext.getMittente(), msgNext.getOggetto(), msgNext.getTesto(), emailDestinatario,
						idsDocumentiNps, isPec, isUfficiale, aoo.getIdAoo().intValue(), notificaSpedizione, connection);
			}
			if (!StringUtils.isNullOrEmpty(msgId)) {
				msgNext.setMsgID(msgId);
				registraInvio(msgNext, true);
			} else {
				registraErroreInvio(msgNext, erroreInvio, false);
				if (Boolean.TRUE.equals(msgNext.getUltimoTentativo())) {
					inviaNotificaErroreSpedizione(msgNext, aoo.getIdAoo().intValue(), null, false, false);
				}
			}
		} catch (final Exception e) {
			// registra l'errore di invio sulla base dati per il messaggio
			registraErroreInvio(msgNext, SOLLEVAMENTO_ECCEZIONE_MSG + e.getMessage(), false);
			if (Boolean.TRUE.equals(msgNext.getUltimoTentativo())) {
				inviaNotificaErroreSpedizione(msgNext, aoo.getIdAoo().intValue(), null, false, false);
			}
			LOGGER.error("Errore durante l'invio del messaggio di posta su nps", e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
			popSubject(fceh);
		}

	}

	/**
	 * @see it.ibm.red.business.service.ICodaMailSRV#updateStatoAndManageEmailByCodiMessage(it.ibm.red.business.dto.MessaggioEmailDTO,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public void updateStatoAndManageEmailByCodiMessage(final MessaggioEmailDTO msgNext, final String msgId, final Connection conn) {
		IFilenetCEHelper fceh = null;
		try {
			final Aoo aoo = aooSRV.recuperaAoo(msgNext.getIdAoo().intValue(), conn);
			// Inizializza accesso a CE filenet
			final AooFilenet aooFilenet = aoo.getAooFilenet();
			fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getObjectStore(), aoo.getIdAoo());
			final CasellaPostaDTO cp = casellePostaliSRV.getCasellaPostale(fceh, msgNext.getMittente());
			if (cp == null) {
				throw new RedException(ErroriMailEnum.NOCASELLAPOSTALEONFILENET.getDescription());
			}

			// per gestire gli invii inviati senza msg id a seguito di un errore
			if (StringUtils.isNullOrEmpty(msgNext.getMsgID())) {
				msgNext.setMsgID(msgId);
			}
			final Document message = salvaMailDaInviare(msgNext, cp.getIndirizzoEmail(), null, fceh);

			creaMessaggioFileNet(msgNext, message, fceh);

			managePostInvioMail(msgNext, msgNext.getIdAoo().intValue(), fceh, null, true);
		} catch (final Exception ex) {
			LOGGER.error("Errore nell'aggiornamento dello stato della notifica di spedizione ", ex);
			throw new RedException("Errore nell'aggiornamento dello stato della notifica di spedizione ", ex);
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ICodaMailSRV#aggiornaMessaggioPostInvioWS(it.ibm.red.business.service.concrete.CodaMailSRV.ResponseSendMail,
	 *      it.ibm.red.business.dto.MessaggioEmailDTO,
	 *      com.filenet.api.core.Document,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public void aggiornaMessaggioPostInvioWS(final ResponseSendMail response, final MessaggioEmailDTO messaggio, final Document message, final IFilenetCEHelper fceh) {
		
		aggiornaMessaggioPostInvio(response, messaggio, message, fceh);
	}

	private void creaMessaggioFileNet(final MessaggioEmailDTO messaggio, final Document message, final IFilenetCEHelper fceh) {
		if (message != null) {
			final Map<String, Object> metadati = new HashMap<>();
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.MESSAGE_ID), messaggio.getMsgID());
			// Aggiornamento del documento E-mail creato su FileNet con il Message-ID dell
			// mail inviata
			fceh.updateMetadati(message, metadati);

			if (message.get_Id() != null) {
				messaggio.setGuid(message.get_Id().toString());
			}
		} else {
			LOGGER.error(ERROR_UPDATE_DOCUMENT_MSG);
		}
	}

	/**
	 * Recupera il prossimo item da lavorare in base al suo stato.
	 * @param statoRicevuta stato notifica mail
	 * @return next item
	 */
	@Override
	public List<NotificaEmailDTO> getNotificheByStatoRicevuta(final Long idAoo,final StatoCodaEmailEnum statoRicevuta,final Integer giorniNotificaMancataSpedizione) {
		Connection conn = null;
		List<NotificaEmailDTO> listaNotifiche = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), true); 
			listaNotifiche = gestioneNotificheEmailDAO.getNotificheByStatoRicevuta(idAoo, statoRicevuta.getStatus(),giorniNotificaMancataSpedizione, conn);
			commitConnection(conn);
		} catch (Exception e) {
			rollbackConnection(conn);
			LOGGER.error("Errore nel recupero delle notifiche email.", e);
			throw new RedException(e);
		} finally {
			closeConnection(conn);
		}
		return listaNotifiche;
	}
	
	
	/**
	 * Permette di aggiornare la data di processamento per il rispettivo DT nella gestione notifiche email 
	 * @param idAoo
	 * @param documetTitle
	 * @return boolean
	 */
	@Override
	public boolean upateDataProcessamentoByDT(final Long idAoo,final String documentTitle) {
		Connection conn = null;
		boolean output = false;
		try {
			conn = setupConnection(getDataSource().getConnection(), true); 
			output = gestioneNotificheEmailDAO.upateDataProcessamentoByDT(idAoo, documentTitle, conn);
			commitConnection(conn);
		} catch (Exception e) {
			rollbackConnection(conn);
			LOGGER.error("Errore nel recupero delle notifiche email.", e);
			throw new RedException(e);
		} finally {
			closeConnection(conn);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICodaMailFacadeSRV#getNotificheForIconaStato(java.lang.String,
	 *      java.lang.Long).
	 */
	@Override
	public Map<String, List<CodaEmail>> getNotificheForIconaStato(String documentTitle, Long idAoo) {
		Map<String, List<CodaEmail>> output = new HashMap<>();
		Connection conn = null;
		
		try {
			
			conn = setupConnection(getDataSource().getConnection(), false);
			
			Collection<CodaEmail> notifiche = gestioneNotificheEmailDAO.getNotificheEmailForIconaStato(documentTitle, idAoo, conn);
			for (CodaEmail n : notifiche) {
				
				List<CodaEmail> destNotifiche = output.get(n.getEmailDestinatario());
				if (destNotifiche == null) {
					destNotifiche = new ArrayList<>();
					destNotifiche.add(n);
				} else {
					destNotifiche.add(n);
				}
				
				output.put(n.getEmailDestinatario(), destNotifiche);
			}
			
		} catch (Exception e) {
			LOGGER.error(MSG_ERROR_RECUPERO_NOTIFICHE, e);
			throw new RedException(MSG_ERROR_RECUPERO_NOTIFICHE, e);
		} finally {
			closeConnection(conn);
		}
		
		return output;
	}
	
	@Override
	public Map<String, List<CodaEmail>> getNotificheForIconaStatoReinvia(String documentTitle, Long idAoo) {
		Map<String, List<CodaEmail>> output = new HashMap<>();
		Connection conn = null;
		
		try {
			
			conn = setupConnection(getDataSource().getConnection(), false);
			
			Collection<CodaEmail> notifiche = gestioneNotificheEmailDAO.getNotificheEmailForIconaStatoReinvia(documentTitle, idAoo, conn);
			for (CodaEmail n : notifiche) {
				
				List<CodaEmail> destNotifiche = output.get(n.getEmailDestinatario());
				if (destNotifiche == null) {
					destNotifiche = new ArrayList<>();
					destNotifiche.add(n);
				} else {
					destNotifiche.add(n);
				}
				
				output.put(n.getEmailDestinatario(), destNotifiche);
			}
			
		} catch (Exception e) {
			LOGGER.error(MSG_ERROR_RECUPERO_NOTIFICHE, e);
			throw new RedException(MSG_ERROR_RECUPERO_NOTIFICHE, e);
		} finally {
			closeConnection(conn);
		}
		
		return output;
	}

}