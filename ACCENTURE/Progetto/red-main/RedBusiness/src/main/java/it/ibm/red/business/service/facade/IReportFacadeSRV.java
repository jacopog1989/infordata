package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import it.ibm.red.business.dto.ParamsRicercaElencoDivisionaleDTO;
import it.ibm.red.business.dto.ReportDetailProtocolliPerCompetenzaDTO;
import it.ibm.red.business.dto.ReportDetailStatisticheUfficioUtenteDTO;
import it.ibm.red.business.dto.ReportDetailUfficioUtenteDTO;
import it.ibm.red.business.dto.ReportMasterStatisticheUfficioUtenteDTO;
import it.ibm.red.business.dto.ReportMasterUfficioUtenteDTO;
import it.ibm.red.business.dto.RisultatoRicercaElencoDivisionaleDTO;
import it.ibm.red.business.dto.UtenteRuoloReportDTO;
import it.ibm.red.business.enums.CodaDiLavoroReportEnum;
import it.ibm.red.business.enums.PeriodoReportEnum;
import it.ibm.red.business.enums.TipoOperazioneReportEnum;
import it.ibm.red.business.persistence.model.CodaDiLavoroReport;
import it.ibm.red.business.persistence.model.DettaglioReport;
import it.ibm.red.business.persistence.model.Ruolo;
import it.ibm.red.business.persistence.model.TipoOperazioneReport;

/**
 * The Interface IReportFacadeSRV.
 *
 * @author m.crescentini
 * 
 *         Facade del servizio per la gestione dei report.
 */
public interface IReportFacadeSRV extends Serializable {

	/**
	 * Ritorna i possibili intervalli temporali di interesse per la ricerca dei
	 * Report.
	 * 
	 * @return
	 */
	List<PeriodoReportEnum> getComboPeriodo();

	/**
	 * Ritrorna le eventuali configurazioni per le tipologie di report
	 * selezionabili.
	 * 
	 * Ciauscuna tipologia (livello ufficio/ispettorato) ha differenti code di
	 * lavorazione e tipo operazioni
	 * 
	 * @return
	 */
	List<DettaglioReport> getComboDettaglioReport(long idAoo);

	/**
	 * Ritorna la lista dei ruoli aplicativi.
	 * 
	 * @param idAoo
	 * @return
	 */
	List<Ruolo> getAllRuoliReport(Long idAoo);

//	/**
//	 * Ritorna una query che specifica il nome ufficio oppure l'utente insieme al numero dei documenti associatagli
//	 * 
//	 * @param reportForm un insieme di filtri slelezionati dall'utente
//	 * @return
//	 *  Qualora la ricerca sia per ufficio username e idUtente non sono valorizzati
//	 * 	
//	 */

	/**
	 * @param dataInizio
	 * @param dataFine
	 * @param uffici
	 * @param utenti
	 * @param tipoOperazioneEnum
	 * @param codaDiLavoroEnum
	 * @return
	 */
	List<ReportMasterUfficioUtenteDTO> ricercaMasterDocumenti(Long idAoo, Calendar dataInizio, Calendar dataFine, Long[] uffici, Long[] utenti,
			TipoOperazioneReportEnum tipoOperazioneEnum, CodaDiLavoroReportEnum codaDiLavoroEnum);

//	/**
//	 * Ritorna una query che specifica il nome ufficio oppure l'utente insieme al numero dei documenti associatagli
//	 * Caso specifico di ricercaMasterDocumenti
//	 * 
//	 * @param reportForm un insieme di filtri slelezionati dall'utente
//	 * 	tra questi filtri devono essere presenti le seguenti infomrazioni 
//	 * 	DettaglioReport deve essere codice "Livello Ufficio/Ispettorato"
//	 *  TipoOperazione deve essere "Utenti attivi con un dato ruolo"
//	 * @return
//	 */
	/**
	 * @param uffici
	 * @param ruoli
	 * @return
	 */
	List<UtenteRuoloReportDTO> ricercaMasterUtentiAttiviConUnDatoRuoloForUff(Long[] uffici, Long[] ruoli);

	/**
	 * Dato un singolo ufficio o utente ritorna la lista di docuemnti a lui
	 * associati filtrate per le opzioni considerate durante la ricerca.
	 * 
	 * @param master       ufficio o utente ricercato
	 * @param tipoRicerca  Tipologia operazione selezionata dall'utente
	 * @param codaDiLavoro l'utente può opzionalmente filtrare per le code di lavoro
	 * @return
	 */
	List<ReportDetailUfficioUtenteDTO> ricercaDetailDocumenti(Long idAoo, ReportMasterUfficioUtenteDTO master, TipoOperazioneReport tipoRicerca,
			CodaDiLavoroReport codaDiLavoro);

	/**
	 * Dato un singolo ufficio o utente ritorna la lista di docuemnti a lui
	 * associati filtrate per le opzioni considerate durante la ricerca.
	 * 
	 * @param reportForm utente selezionato dall'utente
	 * @return
	 */
	List<UtenteRuoloReportDTO> ricercaDetailUtentiAttiviConUnDatoRuoloForUff(UtenteRuoloReportDTO reportForm);

	/**
	 * Dato un elenco di strutture (ufficio o utente) e un range di date, recupera
	 * le statistiche dei documenti assegnati in ingresso e uscita
	 * 
	 * @param form
	 * @return
	 */
	List<ReportMasterStatisticheUfficioUtenteDTO> getStatisticheUfficiMaster(ReportMasterStatisticheUfficioUtenteDTO form);

	/**
	 * Data una struttura (ufficio o utente) e un range di date, recupera le
	 * statistiche dei documenti assegnati in ingresso e uscita
	 * 
	 * @param form
	 * @return
	 */
	List<ReportDetailStatisticheUfficioUtenteDTO> getStatisticheUfficiDetail(ReportDetailStatisticheUfficioUtenteDTO form);

	/**
	 * Estrae protocolli in entrata assegnati per competenza all'ufficio/utente (pervenuti, inevasi e lavorati).
	 * 
	 * @param idAoo
	 * @param dataInizio
	 * @param dataFine
	 * @param uffici
	 * @param utenti
	 * @param tipoRicerca
	 * @return lista dettagli report
	 */
	List<ReportDetailProtocolliPerCompetenzaDTO> ricercaDetailProtocolliPerCompetenza(Long idAoo, Calendar dataInizio,
			Calendar dataFine, Long[] uffici, Long[] utenti, TipoOperazioneReport tipoRicerca);

	/**
	 * Estrae i documenti assegnati (per competenza o conoscenza) alla data assegnazione, dall’ufficio dell’utente in sessione all’ufficio/utenti selezionati,
	 * coerentemente ai filtri di ricerca eventualmente valorizzati.
	 * 
	 * @param idAoo
	 * @param parametriElencoDivisionale
	 * @param uffici
	 * @param utenti
	 * @param tipoRicerca
	 * @param tipoAssegnazione
	 * @param numeroProtocolloDa
	 * @param idTipologiaDocumento
	 * @return lista dettagli report
	 */
	List<RisultatoRicercaElencoDivisionaleDTO> ricercaDetailElencoDivisionale(Long idAoo,
			ParamsRicercaElencoDivisionaleDTO parametriElencoDivisionale, Long[] uffici, Long[] utenti,
			TipoOperazioneReport tipoRicerca, String tipoAssegnazione, Integer numeroProtocolloDa,
			Integer idTipologiaDocumento);
}