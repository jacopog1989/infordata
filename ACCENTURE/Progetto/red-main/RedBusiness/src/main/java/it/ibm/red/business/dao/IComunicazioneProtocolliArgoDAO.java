package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;

import it.ibm.red.business.dto.ComunicazioneProtocolloArgoDTO;

/**
 *  The Class IComunicazioneProtocolliArgoDAO.
 * 
 * @author m.crescentini
 *
 *	Interfaccia del DAO per la gestione della coda per la comunicazione al sistema ARGO del protocollo staccato da RED.
 */
public interface IComunicazioneProtocolliArgoDAO extends Serializable {
	
	/**
	 * @param comunicazioneProtocollo
	 * @param con
	 * @return
	 */
	long inserisciComunicazioneProtocolloInCoda(ComunicazioneProtocolloArgoDTO comunicazioneProtocollo, Connection con);
	
	/**
	 * @param idAoo
	 * @param con
	 * @return
	 */
	ComunicazioneProtocolloArgoDTO getAndLockComunicazioneProtocollo(long idAoo, Connection con);
	
	/**
	 * @param idCoda
	 * @param stato
	 * @param con
	 * @return
	 */
	int aggiornaStatoComunicazioneProtocollo(Long idCoda, int stato, Connection con);

	/**
	 * @param idCoda
	 * @param stato
	 * @param errore
	 * @param con
	 * @return
	 */
	int aggiornaStatoComunicazioneProtocollo(Long idCoda, int stato, String errore, Connection con);
	
	
	/**
	 * @param idDocumento
	 * @param idAoo
	 * @param con
	 * @return
	 */
	boolean isComunicazionePresente(int idDocumento, Long idAoo, Connection con);
 
}
