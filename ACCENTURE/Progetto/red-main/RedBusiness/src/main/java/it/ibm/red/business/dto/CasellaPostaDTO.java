package it.ibm.red.business.dto;

/**
 * The Class CasellaPostaDTO.
 *
 * @author AndreaP
 * 
 * 	Data Transfer Object per Caselle di Posta.
 */
public class CasellaPostaDTO extends AbstractDTO implements Comparable<CasellaPostaDTO> {

	/**
	 * Serialization.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Guid.
	 */
	private String guid;

	/**
	 * Host invio mail.
	 */
	private String hostInvio;

	/**
	 * Host ricezione mail.
	 */
	private String hostRicezione;	

	/**
	 * Indirizzo mail.
	 */
	private String indirizzoEmail;

	/**
	 * Password invio.
	 */
	private String passwordInvio;

	/**
	 * Password ricezione.
	 */
	private String passwordRicezione;

	/**
	 * Porta di invio.
	 */
	private String portInvio;

	/**
	 * Porta di ricezione.
	 */
	private String portRicezione;

	/**
	 * PRoxy host.
	 */
	private String proxyHost;

	/**
	 * Proy port.
	 */
	private String proxyPort;

	/**
	 * Tipologia.
	 */
	private int tipologia;

	/**
	 * Username invio.
	 */
	private String usernameInvio;

	/**
	 * Username ricezione.
	 */
	private String usernameRicezione;

	/**
	 * Tipo server.
	 */
	private String serverInType;

	/**
	 * Autenticazione server.
	 */
	private int serverOutAutenticazione;

	/**
	 * Gestione white list.
	 */
	private String gestioneWhiteList;

	/**
	 * Flag abilita TLS12.
	 */
	private boolean abilitaTLS12;

	/**
	 * Ordinamento.
	 */
	private Integer ordinamento;

	/**
	 * Flag protocollazione automatica.
	 */
	private boolean flagProtocollazioneAutomatica;
	
	/**
	 * Flag che impedisce di scompattare gli archivi dell'email.
	 * Se il flag e' a true devono essere visualizzati e selezionabili gli allegati di primo livello non scompattati.
	 */
	private boolean mantieniAllegatiOriginali;
	
	/**
	 * Counter inbox mail.
	 */
	private Integer inboxCounter;

	/**
	 * Costruttore di default.
	 */
	public CasellaPostaDTO() {
		super();
	}

	/**
	 * Restituisce il server type.
	 * @return serverInType
	 */
	public String getServerInType() {
		return serverInType;
	}

	/**
	 * Imposta il server type.
	 * @param serverInType
	 */
	public void setServerInType(final String serverInType) {
		this.serverInType = serverInType;
	}

	/**
	 * Restituisce il proxy host.
	 * @return
	 */
	public String getProxyHost() {
		return proxyHost;
	}

	/**
	 * Imposta il proxy host.
	 * @param proxyHost
	 */
	public void setProxyHost(final String proxyHost) {
		this.proxyHost = proxyHost;
	}

	/**
	 * Restituisce la porta del proxy.
	 * @return proxyPort
	 */
	public String getProxyPort() {
		return proxyPort;
	}

	/**
	 * Imposta la porta del proxy.
	 * @param proxyPort
	 */
	public void setProxyPort(final String proxyPort) {
		this.proxyPort = proxyPort;
	}

	/**
	 * Restituisce l'host di invio.
	 * @return hostInvio
	 */
	public String getHostInvio() {
		return hostInvio;
	}

	/**
	 * Imposta l'host invio.
	 * @param hostInvio
	 */
	public void setHostInvio(final String hostInvio) {
		this.hostInvio = hostInvio;
	}

	/**
	 * Restituisce la porta di invio.
	 * @return
	 */
	public String getPortInvio() {
		return portInvio;
	}

	/**
	 * Imposta la porta di invio.
	 * @param portInvio
	 */
	public void setPortInvio(final String portInvio) {
		this.portInvio = portInvio;
	}

	/**
	 * Restituisce l'host di ricezione.
	 * @return hostRicezione
	 */
	public String getHostRicezione() {
		return hostRicezione;
	}

	/**
	 * Imposta l'host di ricezione.
	 * @param hostRicezione
	 */
	public void setHostRicezione(final String hostRicezione) {
		this.hostRicezione = hostRicezione;
	}

	/**
	 * Restituisce la porta di ricezione.
	 * @return portRicezione
	 */
	public String getPortRicezione() {
		return portRicezione;
	}

	/**
	 * Imposta la porta di ricezione.
	 * @param portRicezione
	 */
	public void setPortRicezione(final String portRicezione) {
		this.portRicezione = portRicezione;
	}

	/**
	 * Restituisce l'indirizzo email.
	 * @return indirizzoEmail
	 */
	public String getIndirizzoEmail() {
		return indirizzoEmail;
	}

	/**
	 * Imposta l'indirizzo email.
	 * @param indirizzoEmail
	 */
	public void setIndirizzoEmail(final String indirizzoEmail) {
		this.indirizzoEmail = indirizzoEmail;
	}

	/**
	 * Imposta la tipologia.
	 * @param tipologia
	 */
	public void setTipologia(final int tipologia) {
		this.tipologia = tipologia;
	}

	/**
	 * Restituisce la tipologia.
	 * @return tipologia
	 */
	public int getTipologia() {
		return tipologia;
	}

	/**
	 * Restituisce la password di invio.
	 * @return passwordInvio
	 */
	public String getPasswordInvio() {
		return passwordInvio;
	}

	/**
	 * Imposta la password di invio.
	 * @param passwordInvio
	 */
	public void setPasswordInvio(final String passwordInvio) {
		this.passwordInvio = passwordInvio;
	}

	/**
	 * Restituisce la password di ricezione.
	 * @return passwordRicezione
	 */
	public String getPasswordRicezione() {
		return passwordRicezione;
	}

	/**
	 * Imposta la password di ricezione.
	 * @param passwordRicezione
	 */
	public void setPasswordRicezione(final String passwordRicezione) {
		this.passwordRicezione = passwordRicezione;
	}

	/**
	 * Restituisce lo username di invio.
	 * @return usernameInvio
	 */
	public String getUsernameInvio() {
		return usernameInvio;
	}

	/**
	 * Imposta lo username di invio.
	 * @param usernameInvio
	 */
	public void setUsernameInvio(final String usernameInvio) {
		this.usernameInvio = usernameInvio;
	}

	/**
	 * Restituisce lo username di ricezione.
	 * @return
	 */
	public String getUsernameRicezione() {
		return usernameRicezione;
	}

	/**
	 * Imposta lo username di ricezione.
	 * @param usernameRicezione
	 */
	public void setUsernameRicezione(final String usernameRicezione) {
		this.usernameRicezione = usernameRicezione;
	}

	/**
	 * Restituisce il guid.
	 * @return guid
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * Imposta il guid.
	 *
	 * @param guid
	 */
	public void setGuid(final String guid) {
		this.guid = guid;
	}

	/**
	 * Getter serverOutAutenticazione.
	 * @return serverOutAutenticazione
	 */
	public int getServerOutAutenticazione() {
		return serverOutAutenticazione;
	}

	/**
	 * Setter serverOutAutenticazione.
	 * @param serverOutAutenticazione
	 */
	public void setServerOutAutenticazione(final int serverOutAutenticazione) {
		this.serverOutAutenticazione = serverOutAutenticazione;
	}

	/**
	 * Getter gestioneWhiteList.
	 * @return gestioneWhiteList
	 */
	public String getGestioneWhiteList() {
		return gestioneWhiteList;
	}

	/**
	 * Setter gestioneWhiteList.
	 * @param gestioneWhiteList
	 */
	public void setGestioneWhiteList(final String gestioneWhiteList) {
		this.gestioneWhiteList = gestioneWhiteList;
	}

	/**
	 * Restituisce il valore del flag abilitaTLS12.
	 * @return abilitaTLS12
	 */
	public boolean isAbilitaTLS12() {
		return abilitaTLS12;
	}

	/**
	 * Imposta il flag abilitaTLS12.
	 * @param abilitaTLS12
	 */
	public void setAbilitaTLS12(final boolean abilitaTLS12) {
		this.abilitaTLS12 = abilitaTLS12;
	}

	/**
	 * Restituisce l'integer che indica l'ordinamento.
	 * @return
	 */
	public Integer getOrdinamento() {
		return ordinamento;
	}

	/**
	 * Imposta l'ordinamento.
	 * @param ordinamento
	 */
	public void setOrdinamento(final Integer ordinamento) {
		this.ordinamento = ordinamento;
	}

	/**
	 * Setter.
	 * 
	 * @param inInboxCounter	contatore mail inbox
	 */
	public void setIboxCounter(final Integer inInboxCounter) {
		this.inboxCounter = inInboxCounter;
	}

	/**
	 * Getter.
	 * 
	 * @return	contatore mail inbox
	 */
	public Integer getInboxCounter() {
		return inboxCounter;
	}
	
	/**
	 * @return the flagProtocollazioneAutomatica
	 */
	public boolean isFlagProtocollazioneAutomatica() {
		return flagProtocollazioneAutomatica;
	}

	/**
	 * @param flagProtocollazioneAutomatica the flagProtocollazioneAutomatica to set
	 */
	public void setFlagProtocollazioneAutomatica(final boolean flagProtocollazioneAutomatica) {
		this.flagProtocollazioneAutomatica = flagProtocollazioneAutomatica;
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object).
	 */
	@Override
	public int compareTo(final CasellaPostaDTO o) {
		if (getOrdinamento() != null && o.getOrdinamento() != null) {
			//ordina sul campo ordinamento
			return getOrdinamento().compareTo(o.getOrdinamento());
		} else {
			//ordina sul campo email
			return getIndirizzoEmail().compareToIgnoreCase(o.getIndirizzoEmail());
		}
	}

	/**
	 * @return mantieniAllegatiOriginali
	 */
	public boolean isMantieniAllegatiOriginali() {
		return this.mantieniAllegatiOriginali;
	}

	/**
	 * Imposta il flag mantieniAllegatiOriginali.
	 * @param inMantieniAllegatiOriginali
	 */
	public void setMantieniAllegatiOriginali(final boolean inMantieniAllegatiOriginali) {
		this.mantieniAllegatiOriginali = inMantieniAllegatiOriginali;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((indirizzoEmail == null) ? 0 : indirizzoEmail.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CasellaPostaDTO other = (CasellaPostaDTO) obj;
		if (indirizzoEmail == null) {
			if (other.indirizzoEmail != null)
				return false;
		} else if (!indirizzoEmail.equals(other.indirizzoEmail))
			return false;
		return true;
	}
	
	
	
}
