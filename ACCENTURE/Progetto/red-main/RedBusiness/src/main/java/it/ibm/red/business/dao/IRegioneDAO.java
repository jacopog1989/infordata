package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.RegioneDTO;

/**
 * Interfaccia DAO gestione regioni.
 */
public interface IRegioneDAO extends Serializable {

	/**
	 * Ottiene tutte le regioni.
	 * @param connection
	 * @return lista di regioni
	 */
	List<RegioneDTO> getAll(Connection connection);
	
	/**
	 * Ottiene le regioni tramite query.
	 * @param conn
	 * @param query - testo inserito
	 * @return lista di regioni
	 */
	List<RegioneDTO> getRegioni(Connection conn, String query);
	
	/**
	 * Ottiene la regione tramite nome.
	 * @param nomeRegione
	 * @param connection
	 * @return regione
	 */
	RegioneDTO getRegioneByNome(String nomeRegione, Connection connection);
	
	/**
	 * Ottiene la regione tramite id.
	 * @param idRegione
	 * @param connection
	 * @return regione
	 */
	RegioneDTO get(Long idRegione, Connection connection);
	
}
