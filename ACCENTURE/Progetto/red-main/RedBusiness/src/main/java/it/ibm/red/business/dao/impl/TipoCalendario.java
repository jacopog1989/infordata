package it.ibm.red.business.dao.impl;

import java.io.Serializable;

import it.ibm.red.business.enums.EventoCalendarioEnum;

/**
 * Model tipo calendario.
 */
public class TipoCalendario implements Serializable {

	/**
	 * La costante serial Version UID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Tipologia.
	 */
	private int idTipo;

	/**
	 * Codice.
	 */
	private String codice;

	/**
	 * Descrizione.
	 */
	private String descrizione;

	/**
	 * Tipologia (enum).
	 */
	private EventoCalendarioEnum tipoEnum;

	/**
	 * Costruttore vuoto.
	 */
	public TipoCalendario() {
		super();
	}

	/**
	 * Costruttore di default.
	 * 
	 * @param idTipo
	 * @param codice
	 * @param descrizione
	 */
	public TipoCalendario(final int idTipo, final String codice, final String descrizione) {
		this(codice);
		this.idTipo = idTipo;
		this.descrizione = descrizione;
		this.tipoEnum = EventoCalendarioEnum.getItem(codice);
	}

	/**
	 * Costruttore.
	 * 
	 * @param codice
	 */
	public TipoCalendario(final String codice) {
		this();
		this.codice = codice;
		this.tipoEnum = EventoCalendarioEnum.getItem(codice);
	}

	/**
	 * @return codice
	 */
	public String getCodice() {
		return codice;
	}

	/**
	 * Imposta il codice.
	 * 
	 * @param codice
	 */
	public void setCodice(final String codice) {
		this.codice = codice;
	}

	/**
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Imposta la descrizione.
	 * 
	 * @param descrizione
	 */
	public void setDescrizione(final String descrizione) {
		this.descrizione = descrizione;
	}

	/**
	 * @return idTipo
	 */
	public int getIdTipo() {
		return idTipo;
	}

	/**
	 * Imposta l'id del tipo.
	 * 
	 * @param idTipo
	 */
	public void setIdTipo(final int idTipo) {
		this.idTipo = idTipo;
	}

	/**
	 * Restituisce il tipo di evento calendario.
	 * 
	 * @return tipoEnum
	 */
	public EventoCalendarioEnum getTipoEnum() {
		return tipoEnum;
	}

	/**
	 * Imposta il tipo di evento calendario.
	 * @param tipoEnum
	 */
	public void setTipoEnum(final EventoCalendarioEnum tipoEnum) {
		this.tipoEnum = tipoEnum;
	}
}
