package it.ibm.red.business.pcollector;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;

import org.springframework.util.CollectionUtils;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.AnagraficaDipendentiComponentDTO;
import it.ibm.red.business.dto.CapitoloSpesaMetadatoDTO;
import it.ibm.red.business.dto.CollectedParametersDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.LookupTableDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.SelectItemDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.SeverityValidationPluginEnum;
import it.ibm.red.business.enums.TipoDocumentoModeEnum;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IDocumentoRedSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * Classe abstract dei Parameter Collectros. I collectors gestiscono la raccolta
 * dei parametri per il popolamento dei template delle registrazioni ausiliarie.
 */
public abstract class AbstractParametersCollector implements ParametersCollector {
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AbstractParametersCollector.class);
	
	/**
	 * Servizio documenti.
	 */
	private IDocumentoRedSRV documentoSRV;
	
	/**
	 * Collector info.
	 */
	protected CollectedParametersDTO placeholdersCollector = new CollectedParametersDTO();
	
	/**
	 * Caratteri da rimuovere nella formattazione della descrizione dell'ufficio.
	 */
	private final String[] dirtyChars = {"-", "_"};
	/**
	 * Restituisce il valore del metadato sotto forma di
	 * <code> java.lang.String </code>.
	 * 
	 * @param m
	 *            metadato da cui recuperare il valore
	 * @return valore metadato sotto forma di stringa
	 */
	protected String getValue(final MetadatoDTO m) {
		String out = null;
		if (m instanceof LookupTableDTO) {
			final SelectItemDTO lookupSelect = ((LookupTableDTO) m).getLookupValueSelected();
			if (lookupSelect != null && lookupSelect.getValue() != null) {
				out = lookupSelect.getDescription();
			}
		} else if (m instanceof CapitoloSpesaMetadatoDTO) {
			out = ((CapitoloSpesaMetadatoDTO) m).getCapitoloSelected();
		} else if (m.getValue4AttrExt() != null) {
			out = m.getValue4AttrExt();
		} else {
			out = "";
		}
		
		return out;
	}

	/**
	 * Restituisce il valore del metadato il cui nome è: <code> nomeMetadato </code>
	 * recuperandolo dalla lista <code> listaMetadatiRegistro </code>. Restituisce
	 * sempre una <code> String </code> effettuando il
	 * {@link java.lang.String#toString()} nel modo più appropriato in base al tipo
	 * di metadato identificato da:
	 * {@link it.ibm.red.business.dto.MetadatoDTO#getType()}.
	 * 
	 * @param nomeMetadato
	 *            nome metadato per il quale occorre conoscere il valore
	 * @param listaMetadatiRegistro
	 *            lista dei metadati sul quale recuperare il valore del metadato
	 * @return valore acquisito dal metadato sotto forma di
	 *         <code> java.lang.String </code>
	 */
	protected String getValue(final String nomeMetadato, final Collection<MetadatoDTO> listaMetadatiRegistro) {
		String out = null;
		for (final MetadatoDTO m:listaMetadatiRegistro) {
			if (nomeMetadato.equalsIgnoreCase(m.getName())) {
				out = getValue(m);
				break;
			}
		}
		return out;
	}

	/**
	 * Restituisce il collector per la gestione della registrazione ausiliaria.
	 * 
	 * @see it.ibm.red.business.pcollector.ParametersCollector#collectParameters(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection, boolean)
	 */
	@Override
	public CollectedParametersDTO collectParameters(final UtenteDTO utente, final Collection<MetadatoDTO> listaMetadatiRegistro, final boolean checkConfiguration) {

		try {

			populatePlaceholdersCollector(listaMetadatiRegistro, checkConfiguration);

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di generazione del template.", e);
			placeholdersCollector.setBlockingMessage("Errore in fase di generazione del template.");
		}

		return placeholdersCollector;
	}

	/**
	 * Popola la collezione dei placeholder a partire dai dati presenti nella lista
	 * dei metadati. Gestisce errori bloccanti o non bloccanti a partire dalle
	 * informazioni sul metadato.
	 * 
	 * @param listaMetadatiRegistro
	 *            la lista dei valori dei metadati con cui popolare il template
	 * @param checkConfiguration
	 *            se <code> true </code> effettua una validazione degli stessi, se
	 *            <code> false </code> non effettua la validazione
	 */
	protected void populatePlaceholdersCollector(final Collection<MetadatoDTO> listaMetadatiRegistro, final boolean checkConfiguration) {
		
		// Gestisco eventuali warning o errori dei parametri
		if (!placeholdersCollector.getSeverityMessage().equals(SeverityValidationPluginEnum.ERROR)) {
			for (final MetadatoDTO m : listaMetadatiRegistro) {
				if (checkConfiguration && StringUtils.isNullOrEmpty(getValue(m.getName(), listaMetadatiRegistro))
						&& !TipoDocumentoModeEnum.MAI.equals(m.getVisibility())) {
					if (!TipoDocumentoModeEnum.MAI.equals(m.getObligatoriness())) {
						placeholdersCollector.setBlockingMessage("I campi obbligatori devono essere compilati!");
						break;
					} else {
						placeholdersCollector.addStringParam(m.getName(), "");
						if (placeholdersCollector.getSeverityMessage().equals(SeverityValidationPluginEnum.NONE)) {
							placeholdersCollector.setNonBlockingMessage(SeverityValidationPluginEnum.WARNING,
									"Alcuni campi non obbligatori non sono stati compilati");
						}
					}
				} else {
					placeholdersCollector.addStringParam(m.getName(), getValue(m.getName(), listaMetadatiRegistro));
				}
			}
		}
	}
	
	/**
	 * Prepopola i campi per i metadati che devono essere mostrati in maschera per i
	 * quali può essere già fornito un valore a partire dal documento in ingresso e
	 * da altre informazioni recuperabili da Evo.
	 * 
	 * @see it.ibm.red.business.pcollector.ParametersCollector#initParameters(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.util.Collection)
	 */
	@Override
	public void initParameters(final UtenteDTO utente, final String documentTitle, final Collection<MetadatoDTO> out) {
		initParameters(utente, documentTitle, out, true);
	}
	
	/**
	 * Prepopola i campi per i metadati che devono essere mostrati in maschera per i
	 * quali può essere già fornito un valore a partire dal documento in ingresso e
	 * da altre informazioni recuperabili da Evo.
	 * 
	 * @see it.ibm.red.business.pcollector.ParametersCollector#configureMetadati(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.util.Collection)
	 */
	@Override
	public void configureMetadati(final UtenteDTO utente, final String documentTitle, final Collection<MetadatoDTO> out) {
		initParameters(utente, documentTitle, out, false);
	}
	
	/**
	 * Inizializza i parametri per la corretta visualizzazione dei metadati in
	 * maschera.
	 * 
	 * @param utente
	 *            utente in sessione
	 * @param documentTitle
	 *            identificativo documento per cui viene predisposta la
	 *            registrazione ausiliaria
	 * @param out
	 *            metadati del registro ausiliario
	 * @param setDefaultValue
	 *            se <code> true </code> occorre impostare i valori di default sui
	 *            metadati, se <code> false </code> i valori dei metadati non
	 *            vengono reimpostati
	 */
	protected abstract void initParameters(UtenteDTO utente, String documentTitle, Collection<MetadatoDTO> out, boolean setDefaultValue);

	/**
	 * Inizializza il metadato associato alla data firma con il placeholder, @see
	 * Constants.Varie.PLACEHOLDER_DATE. Questo tipo di inizializzazione viene
	 * imposta sui parametri di tipo data che non devono essere modificati in fase
	 * di predisposizione del template, bensì nella rigenerazione del content
	 * associato alla registrazione ausiliaria.
	 * 
	 * @param metadato
	 *            Metadato da inizializzare.
	 * @param setDefaultValue
	 *            Se {@code true} ne modifica il valore, se {@code false} il valore
	 *            del metadato non viene modificato.
	 */
	protected void initializeDate(MetadatoDTO metadato, final boolean setDefaultValue) {
		try {
			if (setDefaultValue) {
				metadato.setSelectedValue((new SimpleDateFormat(DateUtils.DD_MM_YYYY)).parse(Constants.Varie.PLACEHOLDER_DATE));
			}
		} catch (ParseException e) {
			LOGGER.warn(e);
		}
	}
	
	/**
	 * Rimuove il metadato dalla maschera creazione.
	 * 
	 * @param metadato
	 *            Metadato da rimuovere logicamente.
	 * @param setDefaultValue
	 *            Se {@code true} azzera anche il valore selezionato, se
	 *            {@code false} non modifica il valore del metadato.
	 */
	protected void logicDelete (MetadatoDTO metadato, final boolean setDefaultValue) {
		metadato.setObligatoriness(TipoDocumentoModeEnum.MAI);
		metadato.setVisibility(TipoDocumentoModeEnum.MAI);
		metadato.setDisplayName("");
		
		if(setDefaultValue && StringUtils.isNullOrEmpty(metadato.getValue4AttrExt())) {
			resetValue(metadato);
		}
	}

	/**
	 * Inizializze un metadato di tipo <em> Capitolo Spesa </em>.
	 * 
	 * @param metadato
	 *            Metadato da inizializzare.
	 * @param displayName
	 *            Display name da impostare sul metadato.
	 * @param setDefaultValue
	 *            Se {@code true} inizializza il suo valore resettandolo, se
	 *            {@code false} non ne modifica il valore.
	 */
	protected void initNonMandatoryMetadato (MetadatoDTO metadato, final String displayName, final boolean setDefaultValue) {
		metadato.setDisplayName(displayName);
		metadato.setObligatoriness(TipoDocumentoModeEnum.MAI);
		if (setDefaultValue && StringUtils.isNullOrEmpty(metadato.getValue4AttrExt())) {
			resetValue(metadato);
		}
	}

	/**
	 * Resetta il valore del metadato in base alla tipologia dello stesso.
	 * 
	 * @param metadato
	 *            Metadato per il quale occorre resettare il valore.
	 */
	private static void resetValue(MetadatoDTO metadato) {
		switch (metadato.getType()) {
		case CAPITOLI_SELECTOR:
			((CapitoloSpesaMetadatoDTO)metadato).setCapitoloSelected("");
			break;
		case LOOKUP_TABLE:
			((LookupTableDTO)metadato).setLookupValueSelected(null);
			break;
		case PERSONE_SELECTOR:
			((AnagraficaDipendentiComponentDTO)metadato).setSelectedValues(null);
			break;
		case DATE:
		case INTEGER:
		case DOUBLE:
			metadato.setSelectedValue(null);
			break;
		default:
			metadato.setSelectedValue("");
		}
	}
	
	/**
	 * Recupera il destinatario per il prepopolamento della lista destinatari del
	 * template. Il destinatario recuperato è il mittente del documento in ingresso
	 * identificato dal {@code documentTitle}.
	 * 
	 * @param fceh
	 *            Filenet CE Helper.
	 * @param documentTitle
	 *            Identificativo del documento in ingresso dal quale recuperare il
	 *            mittente.
	 * @param utente
	 *            Utente in sessione.
	 * 
	 * @return Mittente del documento in ingresso.
	 */
	protected String recuperaDestinatario(final IFilenetCEHelper fceh, final String documentTitle, final UtenteDTO utente) {
		StringBuilder destinatario = new StringBuilder("");
		documentoSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoRedSRV.class);
		
		try {
			DetailDocumentRedDTO detailDTO = documentoSRV.getDetailFromCEDocumentoNSD(fceh, documentTitle, utente);
			destinatario.append(detailDTO.getMittenteContatto().getAliasContatto()).append("\n");
			
			if (!StringUtils.isNullOrEmpty(detailDTO.getMittenteContatto().getMailPec())) {
				destinatario.append(detailDTO.getMittenteContatto().getMailPec()).append("\n");
			} else {
				destinatario.append(detailDTO.getMittenteContatto().getMail()).append("\n");
			}
			
		} catch (Exception e) {
			LOGGER.warn("Errore durante il recupero dei destinatari del documento: " + documentTitle, e);
		} finally {
			fceh.popSubject();
		}
		return destinatario.toString();
	}
	
	/**
	 * Restituisce la descrizione dell'ufficio dell' {@code utente} formatta
	 * correttamente per i template. Rimuove eventuali riferimenti all'area
	 * organizzativa dalla descrizione dell'ufficio e tutti i caratteri definiti da
	 * {@link #dirtyChars}.
	 * 
	 * @param utente
	 *            Utente in sessione dal quale recuperare la descrizione
	 *            dell'ufficio.
	 * @return Descrizione ufficio formattata per i template.
	 */
	protected String recuperaDescrizioneUfficio(final UtenteDTO utente) {
		String descNodo = utente.getNodoDesc();
		
		try {
			
			// Vengono rimossi eventuali riferimenti all'area organizzativa
			String[] aooWords = utente.getCodiceAoo().split("_");
			if (!CollectionUtils.isEmpty(Arrays.asList(aooWords))) {
				for (String word : Arrays.asList(aooWords)) {
					descNodo = descNodo.replace(word, Constants.EMPTY_STRING);
				}
			}
			
			// Vengono rimossi tutti i caratteri non propri della descrizione dell'ufficio
			for (String literal : Arrays.asList(dirtyChars)) {
				descNodo = descNodo.replace(literal, Constants.EMPTY_STRING);
			}
			
			
		} catch (Exception e) {
			LOGGER.warn("Errore riscontrato nella formattazione delle descrizione dell'ufficio.", e);
		}
		
		return descNodo.trim();
	}
	
}
