package it.ibm.red.business.enums;

import it.ibm.red.business.utils.StringUtils;

/**
 * Enum per l'associazione ad un tipo mime di un icona per la gestione degli allegati delle mail.
 * 
 * @author CRISTIANOPierascenzi
 *
 */
public enum IconaExtensionEnum {

	/**
	 * Icona di default.
	 */
	DEFAULT("fa fa-file-o", ""),
	
	/**
	 * Icona documenti.
	 */
	DOC("fa fa-file-word-o", "DOC", "DOCX"),
	
	/**
	 * Icona html.
	 */
	HTML("fa fa-file-code-o", "HTML", "HTMLX", "HTM"),
	
	/**
	 * Icona mail.
	 */
	MAIL("fa fa-at", "EML"),
	
	/**
	 * Icona pdf.
	 */
	PDF("fa fa-file-pdf-o", "PDF"),
	
	/**
	 * Icona p7m.
	 */
	P7M(IconaExtensionEnum.FA_FA_FILE_ARCHIVE_O, "P7M"),
	
	/**
	 * Icona p7c.
	 */
	P7C(IconaExtensionEnum.FA_FA_FILE_ARCHIVE_O, "P7C"),
	
	/**
	 * Icona p7s.
	 */
	P7S(IconaExtensionEnum.FA_FA_FILE_ARCHIVE_O, "P7S"),
	
	/**
	 * Icona ppt.
	 */
	PPT("fa fa-file-powerpoint-o", "PPT", "PPTX"),
	
	/**
	 * Icona xls.
	 */
	XLS("fa fa-file-excel-o", "XLS", "XLSX"),
	
	/**
	 * Icona immagini.
	 */
	IMAGE("fa fa-file-image-o", "JPG", "JPEG", "GIF", "TIF", "TIFF", "PNG", "BMP"),
	
	/**
	 * Icona txt.
	 */
	TXT("fa fa-file-text-o", "TXT"),
	
	/**
	 * Icona xml.
	 */
	XML("fa fa-file-code-o", "XML");

	/**
	 * Estensioni.
	 */
	private String[] extensions;

	/**
	 * Path.
	 */
	private String path;
	
	private static final String FA_FA_FILE_ARCHIVE_O = "fa fa-file-archive-o";

	/**
	 * Costruttore.
	 * 
	 * @param inPath		path
	 * @param inExtensions	estensioni
	 */
	IconaExtensionEnum(final String inPath, final String...inExtensions) {
		path = inPath;
		extensions = inExtensions;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	estensioni
	 */
	public String[] getExtensions() {
		return extensions;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	path
	 */
	public String getPath() {
		return path;
	}

	/**
	 * Metodo per il recupero dell'enum.
	 * 
	 * @param filename	nome del file
	 * @return			enum
	 */
	public static IconaExtensionEnum get(final String filename) {
		IconaExtensionEnum output = IconaExtensionEnum.DEFAULT;
		
		if (!StringUtils.isNullOrEmpty(filename)) {
			String ext = StringUtils.getExtension(filename);
			
			for (IconaExtensionEnum cat : IconaExtensionEnum.values()) {
				for (String e : cat.getExtensions()) {
					if (e.equalsIgnoreCase(ext)) {
						output = cat;
					}
				}
			}
		}
		
		return output;
	}
}
