package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IValutaDAO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.ValutaMetadatiDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.IValutaSRV;

/**
 * Service che gestisce le funzionalità associate ai metadati di tipo valuta.
 * @author DarioVentimiglia
 */
@Service
public class ValutaSRV extends AbstractService implements IValutaSRV {

	/**
	 * serial version UID
	 */
	private static final long serialVersionUID = 6483974827957357408L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ValutaSRV.class.getName());
	
	/**
	 * DAO gestione valute.
	 */
	@Autowired
	private IValutaDAO valutaDAO;
	
	/**
	 * Restituisce una lista di triplette di metadati valuta
	 * a partire dalla lista di metadati filtrati precedentemente
	 * dalla lista di metadati estesi dei un documento.
	 * @return valutaMedatatiMap
	 */
	@Override
	public List<ValutaMetadatiDTO> buildListValutaMetadati(List<MetadatoDTO> metadatiValutaDoc) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			
			return valutaDAO.raggruppaValutaMetadati(metadatiValutaDoc, connection);
			
		} catch (SQLException e) {
			LOGGER.error("Errore nella connessione al DB", e);
			throw new RedException("Errore nel caricamento dei metadati estesi", e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IValutaFacadeSRV#getCambiValute(java.util.Date).
	 */
	@Override
	public Map<String, Double> getCambiValute(Date dataCreazione) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			
			return valutaDAO.getMetadatiConversione(dataCreazione, connection);
			
		} catch (SQLException e) {
			LOGGER.error("Errore nella connessione al DB", e);
			throw new RedException("Errore nel caricamento dei metadati estesi", e);
		} finally {
			closeConnection(connection);
		}
	}
}
