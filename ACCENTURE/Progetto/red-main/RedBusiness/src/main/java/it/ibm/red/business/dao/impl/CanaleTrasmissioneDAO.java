package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.ICanaleTrasmissioneDAO;
import it.ibm.red.business.dto.CanaleTrasmissioneDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * Si trova su DWH di filenet.
 * Ricordarsi di recuperare la connection dal corretto datasource
 *
 */
@Repository
public class CanaleTrasmissioneDAO extends AbstractDAO implements ICanaleTrasmissioneDAO {
	
	/**
	 * Costante serial versione UID.
	 */
	private static final long serialVersionUID = 3198825223550888286L;
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(CanaleTrasmissioneDAO.class.getName());
	
	/**
	 * @see it.ibm.red.business.dao.ICanaleTrasmissioneDAO#getAll(java.sql.Connection).
	 */
	@Override
	public List<CanaleTrasmissioneDTO> getAll(final Connection connection) {
		List<CanaleTrasmissioneDTO> all = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String querySQL = "select * from SE_CANALE_TRASMISSIONE";
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				int id = rs.getInt("ID");
				String nome = rs.getString("NOME");
				
				CanaleTrasmissioneDTO canale = new CanaleTrasmissioneDTO();
				canale.setId(id);
				canale.setNome(nome);
				
				all.add(canale);
			}
		} catch (Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return all;
	}
	
	/**
	 * @see it.ibm.red.business.dao.ICanaleTrasmissioneDAO#getById(java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public CanaleTrasmissioneDTO getById(final Integer id, final Connection connection) {
		CanaleTrasmissioneDTO canale = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			String querySQL = "select * from SE_CANALE_TRASMISSIONE where id = ?";
			ps = connection.prepareStatement(querySQL);
			
			int index = 1;
			ps.setInt(index++, id);
			
			rs = ps.executeQuery();
			
			if (rs.next()) {
				int idCurrent = rs.getInt("ID");
				String nome = rs.getString("NOME");
				
				canale = new CanaleTrasmissioneDTO();
				canale.setId(idCurrent);
				canale.setNome(nome);
			} else {
				throw new RedException("Non è stato possibile individuare un SE_CANALE_TRASMISSIONE a partire dall'id : " + id);
			}
			
			if (rs.next()) {
				throw new RedException("Il medesimo id ha recuperato due istanze di SE_CANALE_TRASMISSIONE");
			}
		} catch (Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return canale;
	}
}
