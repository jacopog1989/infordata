package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.core.Document;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.Ricerca;
import it.ibm.red.business.dao.IEventoLogDAO;
import it.ibm.red.business.dao.ILegislaturaDAO;
import it.ibm.red.business.dao.ILookupDAO;
import it.ibm.red.business.dao.IMezzoRicezioneDAO;
import it.ibm.red.business.dao.IRegistroAusiliarioDAO;
import it.ibm.red.business.dao.IRegistroRepertorioDAO;
import it.ibm.red.business.dao.IRicercaRedDAO;
import it.ibm.red.business.dao.ITipoAssegnazioneDAO;
import it.ibm.red.business.dao.ITipoEventoDAO;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dao.ITipologiaDocumentoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.AssegnatarioOrganigrammaDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DocumentoFascicoloDTO;
import it.ibm.red.business.dto.EventoLogDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.MezzoRicezioneDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocSalvataDTO;
import it.ibm.red.business.dto.ParamsRicercaEsitiDTO;
import it.ibm.red.business.dto.RegistroDTO;
import it.ibm.red.business.dto.RegistroRepertorioDTO;
import it.ibm.red.business.dto.RicercaAvanzataDocFormDTO;
import it.ibm.red.business.dto.SelectItemDTO;
import it.ibm.red.business.dto.TipoProcedimentoDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.ModalitaRicercaAvanzataTestoEnum;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.enums.TipoOperazionePermessoEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Legislatura;
import it.ibm.red.business.persistence.model.RicercaAvanzataSalvata;
import it.ibm.red.business.persistence.model.TipoEvento;
import it.ibm.red.business.persistence.model.TipoProcedimento;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IRicercaAvanzataDocSRV;
import it.ibm.red.business.service.IRicercaSRV;
import it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV;
import it.ibm.red.business.service.facade.ISecurityFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.PermessiUtils;

/**
 * The Class RicercaAvanzataDocSRV.
 *
 * @author m.crescentini
 * 
 *         Servizio per la gestione della ricerca avanzata dei documenti.
 */
@Service
public class RicercaAvanzataDocSRV extends AbstractService implements IRicercaAvanzataDocSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -2265842041200419369L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaAvanzataDocSRV.class.getName());

	/**
	 * Key uscita.
	 */
	private static final String USCITA = "uscita";

	/**
	 * Key tutto.
	 */
	private static final String TUTTO = "tutto";

	/**
	 * Key dell'id utente.
	 */
	private static final String TIPO_OPERAZIONE_UTENTE_SELECTED_ID_UTENTE = "tipoOperazioneUtenteSelected_idUtente";

	/**
	 * Key id nodo.
	 */
	private static final String TIPO_OPERAZIONE_UTENTE_SELECTED_ID_NODO = "tipoOperazioneUtenteSelected_idNodo";

	/**
	 * Key tipo operazione selezionata.
	 */
	private static final String TIPO_OPERAZIONE_SELECTED = "tipoOperazioneSelected";

	/**
	 * Key tipo assegnazione selezionata.
	 */
	private static final String TIPO_ASSEGNAZIONE_SELECTED = "tipoAssegnazioneSelected";

	/**
	 * Key id utente.
	 */
	private static final String RESPONSABILE_COPIA_CONFORME_SELECTED_ID_UTENTE = "responsabileCopiaConformeSelected_idUtente";

	/**
	 * Key responsabile copia conforme selezionato.
	 */
	private static final String RESPONSABILE_COPIA_CONFORME_SELECTED = "responsabileCopiaConformeSelected";

	/**
	 * Key numero RDP.
	 */
	private static final String NUMERO_RDP = "numeroRDP";

	/**
	 * Key numero documento.
	 */
	private static final String NUMERO_DOCUMENTO = "numeroDocumento";

	/**
	 * Key mezzo ricezione selezionato.
	 */
	private static final String MEZZO_RICEZIONE_SELECTED = "mezzoRicezioneSelected";

	/**
	 * Key legislatura selezionata.
	 */
	private static final String LEGISLATURA_SELECTED = "legislaturaSelected";

	/**
	 * Key interno.
	 */
	private static final String INTERNO = "interno";

	/**
	 * Key entrata.
	 */
	private static final String ENTRATA = "entrata";

	/**
	 * Key id utente assegnatario.
	 */
	private static final String ASSEGNATARIO_ID_UTENTE = "assegnatario_idUtente";

	/**
	 * Key id nodo assegnatario.
	 */
	private static final String ASSEGNATARIO_ID_NODO = "assegnatario_idNodo";

	/**
	 * Key descrizione utente.
	 */
	private static final String ASSEGNATARIO_DESCRIZIONE_UTENTE = "assegnatario_descrizioneUtente";

	/**
	 * Key riferimento protocollo mittente.
	 */
	private static final String RIF_PROTOCOLLO_MITTENTE = "rifProtocolloMittente";

	/**
	 * Key numero protocollo A.
	 */
	private static final String NUMERO_PROTOCOLLO_A = "numeroProtocolloA";

	/**
	 * Key numero protocollo DA.
	 */
	private static final String NUMERO_PROTOCOLLO_DA = "numeroProtocolloDa";

	/**
	 * Service.
	 */
	@Autowired
	private IRicercaSRV ricercaSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private ITipologiaDocumentoDAO tipologiaDocumentoDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private ITipoProcedimentoDAO tipoProcedimentoDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IEventoLogDAO eventoLogDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private ILegislaturaDAO legislaturaDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IMezzoRicezioneDAO mezzoRicezioneDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private ITipoAssegnazioneDAO tipoAssegnazioneDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private ITipoEventoDAO tipoEventoDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IRicercaRedDAO ricercaRedDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private ILookupDAO lookupDAO;

	/**
	 * Service.
	 */
	@Autowired
	private IOrganigrammaFacadeSRV orgSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ISecurityFacadeSRV securitySRV;

	/**
	 * DAO.
	 */
	@Autowired
	private IRegistroRepertorioDAO registroRepertorioDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IRegistroAusiliarioDAO registroAusiliarioDAO;

	/**
	 * Properties.
	 */
	private PropertiesProvider pp;

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	private Collection<MasterDocumentRedDTO> eseguiRicercaDocumenti(final ParamsRicercaAvanzataDocDTO paramsRicercaAvanzata, final UtenteDTO utente, final Connection con,
			final boolean orderbyInQuery, final Set<String> inIdDocumenti, final boolean setCap, final boolean ignoraAnnoCreazioneDocumento) {
		LOGGER.info("eseguiRicercaDocumenti -> START");
		Collection<MasterDocumentRedDTO> documentiRicerca = new ArrayList<>();
		final HashMap<String, Object> mappaValoriRicercaFilenet = new HashMap<>();
		Connection dwhCon = null;
		IFilenetCEHelper fceh = null;
		Connection connection = null;
		IFilenetCEHelper fcehAdmin = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			dwhCon = setupConnection(getFilenetDataSource().getConnection(), false);

			final Set<String> idDocumenti = new HashSet<>();
			if (!CollectionUtils.isEmpty(inIdDocumenti)) {
				idDocumenti.addAll(inIdDocumenti);
			}

			// Si convertono i parametri di ricerca in una mappa chiave -> valore
			convertiInParametriRicercaFilenet(mappaValoriRicercaFilenet, paramsRicercaAvanzata, ignoraAnnoCreazioneDocumento, utente.getIdAoo(), con);

			boolean cercaNelPE = false;
			if (mappaValoriRicercaFilenet.containsKey(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY))
					&& mappaValoriRicercaFilenet.containsKey(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY))
					&& mappaValoriRicercaFilenet.containsKey(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY))
					|| mappaValoriRicercaFilenet.containsKey(pp.getParameterByKey(PropertiesNameEnum.EVENTLOG_ID_TIPO_EVENTO))
					|| mappaValoriRicercaFilenet.containsKey(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY))
					|| mappaValoriRicercaFilenet.containsKey(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY))) {
				cercaNelPE = true;

				// ### RICERCA NEL PE
				// Si ricavano i documenti filtrati per assegnatario e/o tipo operazione: il
				// metodo cerca nel PE
				// attraverso gli eventi tracciati nella tabella D_EVENTI_CUSTOM del database
				// FileNet
				getDocumentiFromEventLog(idDocumenti, mappaValoriRicercaFilenet, paramsRicercaAvanzata, dwhCon);
			}

			// ### RICERCA NEL CE -> START
			// Se tra i parametri di ricerca viene specificato un fascicolo, si aggiungono
			// tutti i documenti del fascicolo
			if (mappaValoriRicercaFilenet.containsKey(pp.getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY))) {
				final Set<String> idDocumentiFascicolo = getDocumentiFascicoli(mappaValoriRicercaFilenet, utente.getIdAoo(), fceh);

				if (CollectionUtils.isEmpty(idDocumentiFascicolo)) {
					return documentiRicerca;
				} else {
					idDocumenti.addAll(idDocumentiFascicolo);
				}
			}

			// Si ricavano i documenti dal CE solo se:
			// a) sono stati trovati documenti nel PE
			// oppure
			// b) sono stati trovati documenti nel fascicolo specificato tra i parametri della ricerca
			// oppure
			// c) non si è cercato nel PE.
			DocumentSet documentiFilenet = null;
			if (!CollectionUtils.isEmpty(idDocumenti) || !cercaNelPE) {
				LOGGER.info("eseguiRicercaDocumenti (Avanzata) -> START");

				final boolean hasPermessoRiservato = PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.RICERCA_RISERVATO);
				final String campoRiservato = pp.getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY);

				final Integer riservato = (Integer) mappaValoriRicercaFilenet.get(campoRiservato);
				String acl = Constants.EMPTY_STRING;
				if (BooleanFlagEnum.SI.getIntValue().equals(riservato) && hasPermessoRiservato) {
					// Nodo Padre
					final List<AssegnazioneDTO> assList = new ArrayList<>();
					final NodoOrganigrammaDTO padre = new NodoOrganigrammaDTO();
					padre.setIdAOO(utente.getIdAoo());
					padre.setIdNodo(utente.getIdUfficio());
					padre.setCodiceAOO(utente.getCodiceAoo());
					padre.setUtentiVisible(true);
					padre.setDescrizioneNodo(utente.getNodoDesc());

					getOrganigrammaCompleto(assList, padre);

					fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()), utente.getIdAoo());
					acl = securitySRV.getACLDocRiservatoPerRicercaAvanzata(assList, fcehAdmin, connection);
				}

				documentiFilenet = fceh.ricercaAvanzataDocumenti(mappaValoriRicercaFilenet, idDocumenti, utente, orderbyInQuery, acl, setCap);

				if (documentiFilenet != null && !documentiFilenet.isEmpty()) {
					documentiRicerca = ricercaSRV.trasformToGenericDocContext(documentiFilenet, utente, con);
				}

				LOGGER.info("eseguiRicercaDocumenti (Avanzata) -> END");
			}

			// ### Se la ricerca è testuale, si deve innescare anche la ricerca negli
			// allegati
			if (paramsRicercaAvanzata.getModalitaRicercaTesto() != null
					&& mappaValoriRicercaFilenet.get(paramsRicercaAvanzata.getModalitaRicercaTesto().getValore()) != null) {
				LOGGER.info("eseguiRicercaDocumenti (Avanzata) Testuale -> START");

				Integer nMax = null;
				if (StringUtils.isNotBlank(pp.getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS))) {
					nMax = Integer.valueOf(pp.getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS));
				}
				final Collection<MasterDocumentRedDTO> allegatiRicercaTestuale = getAllegatiRicercaTestuale(mappaValoriRicercaFilenet,
						paramsRicercaAvanzata.getModalitaRicercaTesto(), nMax, documentiRicerca.size(), utente.getIdAoo(), fceh, con);

				if (!CollectionUtils.isEmpty(allegatiRicercaTestuale)) {
					documentiRicerca.addAll(allegatiRicercaTestuale);
				}

				LOGGER.info("eseguiRicercaDocumenti (Avanzata) Testuale -> END");
			}

			if (!orderbyInQuery) {
				final List<MasterDocumentRedDTO> docList = new ArrayList<>(documentiRicerca);
				// Implementa Comparable
				Collections.sort(docList, Collections.reverseOrder());
				documentiRicerca = docList;
			}

			LOGGER.info("Ricerca Avanzata Documenti Ordinati");

			// ### RICERCA NEL CE -> END
		} catch (final Exception e) {
			LOGGER.error("eseguiRicercaDocumenti -> Si è verificato un errore durante l'esecuzione della ricerca.", e);
			throw new RedException("Si è verificato un errore durante l'esecuzione della ricerca.", e);
		} finally {
			closeConnection(dwhCon);
			closeConnection(connection);
			popSubject(fceh);
			popSubject(fcehAdmin);
		}

		LOGGER.info("eseguiRicercaDocumenti -> END. Numero di documenti trovati: " + documentiRicerca.size());
		return documentiRicerca;
	}

	/**
	 * Esegue la ricerca avanzata dei documenti in base ai parametri definiti in
	 * <code> paramsRicercaAvanzata </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IRicercaAvanzataDocFacadeSRV#
	 *      eseguiRicercaDocumenti(
	 *      it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO,
	 *      it.ibm.red.business.dto.UtenteDTO, boolean).
	 * @param paramsRicercaAvanzata
	 *            parametri della ricerca
	 * @param utente
	 *            utente in sessione
	 * @param orderbyInQuery
	 *            flag di ordinamento risultati
	 * @param ignoraAnnoCreazioneDocumento
	 *            flag che permette di ignorare l'anno creazione documento nei
	 *            risultati
	 * @return collection dei master dei documenti recuperati
	 */
	@Override
	public Collection<MasterDocumentRedDTO> eseguiRicercaDocumenti(final ParamsRicercaAvanzataDocDTO paramsRicercaAvanzata, final UtenteDTO utente,
			final boolean orderbyInQuery, final boolean ignoraAnnoCreazioneDocumento) {
		Collection<MasterDocumentRedDTO> documentiRicerca;
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			documentiRicerca = eseguiRicercaDocumenti(paramsRicercaAvanzata, utente, con, orderbyInQuery, true, ignoraAnnoCreazioneDocumento);
		} catch (final SQLException e) {
			LOGGER.error("eseguiRicercaDocumenti -> Si è verificato un errore durante l'instaurazione della connessione verso il database.", e);
			throw new RedException("Si è verificato un errore durante l'instaurazione della connessione verso il database.", e);
		} finally {
			closeConnection(con);
		}

		return documentiRicerca;
	}

	/**
	 * Esegue la ricerca avanzata dei documenti in base ai parametri definiti in
	 * <code> paramsRicercaAvanzata </code>.
	 * 
	 * @param paramsRicercaAvanzata
	 *            parametri della ricerca
	 * @param utente
	 *            utente in sessione
	 * @param con
	 *            connession al database
	 * @param orderbyInQuery
	 *            flag di ordinamento risultati
	 * @param setCap
	 *            flag associato all'impostazione del capitolo spesa
	 * @param ignoraAnnoCreazioneDocumento
	 *            flag che permette di ignorare l'anno creazione documento nei
	 *            risultati
	 * @return collection dei master dei documenti recuperati
	 */
	@Override
	public Collection<MasterDocumentRedDTO> eseguiRicercaDocumenti(final ParamsRicercaAvanzataDocDTO paramsRicercaAvanzata, final UtenteDTO utente, final Connection con,
			final boolean orderbyInQuery, final boolean setCap, final boolean ignoraAnnoCreazioneDocumento) {
		return eseguiRicercaDocumenti(paramsRicercaAvanzata, utente, con, orderbyInQuery, null, setCap, ignoraAnnoCreazioneDocumento);
	}

	/**
	 * @param idDocumenti
	 * @param valoriRicercaFilenet
	 * @param dwhCon
	 */
	private void getDocumentiFromEventLog(final Set<String> idDocumenti, final HashMap<String, Object> valoriRicercaFilenet,final ParamsRicercaAvanzataDocDTO paramsRicerca, final Connection dwhCon) {
		LOGGER.info("getDocumentiFromEventLog -> START");
		final Map<String, Object> valoriRicercaPE = new HashMap<>();
		boolean eseguiRicerca = false;

		// Tipo Assegnazione, Utente Destinatario e Ufficio Destinatario
		final String keyIdUtenteDestinatario = pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY);
		final String keyIdUfficioDestinatario = pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY);
		final String keyIdTipoAssegnazione = pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY);
		final Long idUtenteDestinatario = (Long) valoriRicercaFilenet.get(keyIdUtenteDestinatario);
		final Long idUfficioDestinatario = (Long) valoriRicercaFilenet.get(keyIdUfficioDestinatario);
		final Integer idTipoAssegnazione = (Integer) valoriRicercaFilenet.get(keyIdTipoAssegnazione);

		if (idUtenteDestinatario != null && idUfficioDestinatario != null && idTipoAssegnazione != null) {
			valoriRicercaPE.put(keyIdUtenteDestinatario, idUtenteDestinatario);
			valoriRicercaPE.put(keyIdUfficioDestinatario, idUfficioDestinatario);
			valoriRicercaPE.put(keyIdTipoAssegnazione, idTipoAssegnazione);
			valoriRicercaFilenet.remove(keyIdUtenteDestinatario);
			valoriRicercaFilenet.remove(keyIdUfficioDestinatario);
			valoriRicercaFilenet.remove(keyIdTipoAssegnazione);

			eseguiRicerca = true;
		}

		// Tipo Operazione
		final String keyIdTipoOperazione = pp.getParameterByKey(PropertiesNameEnum.EVENTLOG_ID_TIPO_EVENTO);
		final Integer idTipoOperazione = (Integer) valoriRicercaFilenet.get(keyIdTipoOperazione);

		if (idTipoOperazione != null) {
			valoriRicercaPE.put(pp.getParameterByKey(PropertiesNameEnum.TIPO_EVENTO), String.valueOf(idTipoOperazione));
			valoriRicercaFilenet.remove(keyIdTipoOperazione);

			eseguiRicerca = true;
		}

		// Utente Mittente
		final String keyIdUtenteMittente = pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY);
		final Long idUtenteMittente = (Long) valoriRicercaFilenet.get(keyIdUtenteMittente);

		if (idUtenteMittente != null) {
			valoriRicercaPE.put(keyIdUtenteMittente, idUtenteMittente);
			valoriRicercaFilenet.remove(keyIdUtenteMittente);

			eseguiRicerca = true;
		}

		// Ufficio Mittente
		final String keyIdUfficioMittente = pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY);
		final Long idUfficioMittente = (Long) valoriRicercaFilenet.get(keyIdUfficioMittente);

		if (idUfficioMittente != null) {
			valoriRicercaPE.put(keyIdUfficioMittente, idUfficioMittente);
			valoriRicercaFilenet.remove(keyIdUfficioMittente);

			eseguiRicerca = true;
		}

		// Anno Documento
		final Integer annoDocumento = (Integer) valoriRicercaFilenet.get(pp.getParameterByKey(PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY));
		Date dataCreazioneDa = paramsRicerca.getDataCreazioneDa();
		Date dataCreazioneA = paramsRicerca.getDataCreazioneA();
		Date dataProtocolloDa = paramsRicerca.getDataProtocolloDa();
		Date dataProtocolloA = paramsRicerca.getDataProtocolloA();

		// data creazione => timestamp >= data creazione da
		if (dataCreazioneDa != null && dataCreazioneA != null) {

			valoriRicercaPE.put(pp.getParameterByKey(PropertiesNameEnum.TIMESTAMP_OPERAZIONE_DA), dataCreazioneDa);
			valoriRicercaPE.put(pp.getParameterByKey(PropertiesNameEnum.TIMESTAMP_OPERAZIONE_A), dataCreazioneA);

		} else if (dataProtocolloDa != null && dataProtocolloA != null) {

			valoriRicercaPE.put(pp.getParameterByKey(PropertiesNameEnum.TIMESTAMP_OPERAZIONE_DA), dataProtocolloDa);
			valoriRicercaPE.put(pp.getParameterByKey(PropertiesNameEnum.TIMESTAMP_OPERAZIONE_A), dataProtocolloA);

		}
		if (annoDocumento != null) {
			valoriRicercaPE.put(pp.getParameterByKey(PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY), annoDocumento);
		}

		if (eseguiRicerca) {
			try {
				final List<EventoLogDTO> eventLogList = eventoLogDAO.cercaEventiPerRicercaDocumenti(valoriRicercaPE, dwhCon);
				for (int i = 0; i < eventLogList.size(); i++) {
					idDocumenti.add(Constants.EMPTY_STRING + eventLogList.get(i).getIdDocumento());
				}
			} catch (final Exception e) {
				LOGGER.error("getDocumentiFromEventLog -> Si è verificato un errore durante la ricerca di eventi nello storico.", e);
			}
		}

		LOGGER.info("getDocumentiFromEventLog -> END");
	}

	/**
	 * @param assList
	 * @param nodo
	 * @return
	 */
	private List<NodoOrganigrammaDTO> getOrganigrammaCompleto(final List<AssegnazioneDTO> assList, final NodoOrganigrammaDTO nodo) {
		final UfficioDTO uff = new UfficioDTO(nodo.getIdNodo(), nodo.getDescrizioneNodo());
		final AssegnazioneDTO ass = new AssegnazioneDTO(null, TipoAssegnazioneEnum.COMPETENZA, null, null, null, null, uff);
		assList.add(ass);

		final List<NodoOrganigrammaDTO> livello = orgSRV.getFigliAlberoAssegnatarioPerAssegna(nodo.getIdNodo(), nodo);

		if (livello != null) {
			for (final NodoOrganigrammaDTO nodoSottolivello : livello) {
				final UfficioDTO ufficioSottolivello = new UfficioDTO(nodoSottolivello.getIdNodo(), nodoSottolivello.getDescrizioneNodo());
				if (nodoSottolivello.getIdUtente() != null) {
					UtenteDTO utenteSottolivello = null;
					utenteSottolivello = new UtenteDTO();
					utenteSottolivello.setId(nodoSottolivello.getIdUtente());
					utenteSottolivello.setIdUfficio(ufficioSottolivello.getId());
					utenteSottolivello.setUsername(nodoSottolivello.getUsernameUtente());
					try {
						final AssegnazioneDTO assSottoLivello = new AssegnazioneDTO(null, TipoAssegnazioneEnum.COMPETENZA, null, null, null, utenteSottolivello,
								ufficioSottolivello);
						assList.add(assSottoLivello);
					} catch (final Exception e) {
						LOGGER.error(e);
						e.getStackTrace();
					}
				} else {
					getOrganigrammaCompleto(assList, nodoSottolivello);
				}

			}
		}

		return livello;
	}

	/**
	 * @param mappaValoriRicerca
	 * @param idAoo
	 * @param fceh
	 * @param con
	 * @return
	 */
	private Set<String> getDocumentiFascicoli(final HashMap<String, Object> mappaValoriRicerca, final Long idAoo, final IFilenetCEHelper fceh) {
		LOGGER.info("getDocumentiFascicoli -> START");
		final Set<String> idDocumenti = new HashSet<>();

		try {
			final String keyNomeFascicolo = pp.getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY);
			final String keyTitolario = pp.getParameterByKey(PropertiesNameEnum.TITOLARIO_METAKEY);
			String nomeFascicolo = Constants.EMPTY_STRING;
			String titolario = Constants.EMPTY_STRING;

			if (mappaValoriRicerca.containsKey(keyNomeFascicolo)) {
				nomeFascicolo = (String) mappaValoriRicerca.get(keyNomeFascicolo);
				mappaValoriRicerca.remove(keyNomeFascicolo);
			}
			if (mappaValoriRicerca.containsKey(keyTitolario)) {
				titolario = (String) mappaValoriRicerca.get(keyTitolario);
				mappaValoriRicerca.remove(keyTitolario);
			}

			if (StringUtils.isNotBlank(nomeFascicolo) || StringUtils.isNotBlank(titolario)) {
				// Si recuperano i fascicoli che matchano le condizioni di ricerca
				final DocumentSet fascicoliDocumenti = fceh.getFascicoliPerRicercaDocumenti(nomeFascicolo, titolario, idAoo);

				if (!fascicoliDocumenti.isEmpty()) {
					final Iterator<?> itFascicoliDocumenti = fascicoliDocumenti.iterator();
					while (itFascicoliDocumenti.hasNext()) {
						final Document fascicolo = (Document) itFascicoliDocumenti.next();
						final Integer idFascicolo = Integer.parseInt((String) TrasformerCE.getMetadato(fascicolo, PropertiesNameEnum.NOME_FASCICOLO_METAKEY));

						// Per ciascun fascicolo, si recuperano i documenti in esso contenuti
						final Collection<DocumentoFascicoloDTO> documentiFascicolo = fceh.getOnlyDocumentiRedFascicolo(idFascicolo, idAoo);

						if (!CollectionUtils.isEmpty(documentiFascicolo)) {
							for (final DocumentoFascicoloDTO documentoFascicolo : documentiFascicolo) {
								if (StringUtils.isNotBlank(documentoFascicolo.getDocumentTitle())) {
									idDocumenti.add(documentoFascicolo.getDocumentTitle());
								}
							}
						}
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error("getDocumentiFascicoli -> Si è verificato un errore durante il recupero dei documenti associati ai fascicoli.", e);
		}

		LOGGER.info("getDocumentiFascicoli -> END");
		return idDocumenti;
	}

	/**
	 * @param mappaValoriRicercaFilenet
	 * @param modalitaRicercaTestuale
	 * @param inUtenteMaxNumeroElementi
	 * @param numeroRisultatiRicercaDoc
	 * @param idAoo
	 * @param fceh
	 * @param con
	 * @return
	 */
	private Collection<MasterDocumentRedDTO> getAllegatiRicercaTestuale(final HashMap<String, Object> mappaValoriRicercaFilenet,
			final ModalitaRicercaAvanzataTestoEnum modalitaRicercaTestuale, final Integer inUtenteMaxNumeroElementi, final int numeroRisultatiRicercaDoc, final Long idAoo,
			final IFilenetCEHelper fceh, final Connection con) {
		final Collection<MasterDocumentRedDTO> allegatiRicercaTestuale = new ArrayList<>();

		final DocumentSet allegatiFilenetRicercaTestuale = fceh.ricercaAllegatiFullText(mappaValoriRicercaFilenet, modalitaRicercaTestuale);

		if (allegatiFilenetRicercaTestuale != null && !allegatiFilenetRicercaTestuale.isEmpty()) {
			Integer utenteMaxNumeroElementi = inUtenteMaxNumeroElementi;
			if (utenteMaxNumeroElementi == null) {
				final String maxStr = pp.getParameterByKey(PropertiesNameEnum.RICERCA_FULLTEXT_MAX_RESULTS);
				if (StringUtils.isNotBlank(maxStr)) {
					utenteMaxNumeroElementi = Integer.valueOf(maxStr);
				} else {
					utenteMaxNumeroElementi = 100;
				}
			}

			int maxAllegati = utenteMaxNumeroElementi - numeroRisultatiRicercaDoc;

			final Map<ContextTrasformerCEEnum, Object> context = new EnumMap<>(ContextTrasformerCEEnum.class);

			final Map<Long, String> mapTipologiaDocumento = lookupDAO.getDescTipoDocumento(idAoo.intValue(), con);
			context.put(ContextTrasformerCEEnum.IDENTIFICATIVI_TIPO_DOC, mapTipologiaDocumento);

			final Map<Long, String> mapTipoProcedimento = tipoProcedimentoDAO.getAllDesc(con);
			context.put(ContextTrasformerCEEnum.IDENTIFICATIVI_TIPO_PROC, mapTipoProcedimento);

			final Iterator<?> itAllegatiFilenetFullText = allegatiFilenetRicercaTestuale.iterator();
			while (itAllegatiFilenetFullText.hasNext() && maxAllegati > 0) {
				final Document allegatoFilenetRicercaTestuale = (Document) itAllegatiFilenetFullText.next();

				// Si estraggono i dati del protocollo dal documento principale, per impostarli
				// sull'allegato
				final Iterator<?> itDocPrincipaliFilenet = allegatoFilenetRicercaTestuale.get_ParentDocuments().iterator();
				while (itDocPrincipaliFilenet.hasNext()) {
					final Document docPrincipaleFilenet = (Document) itDocPrincipaliFilenet.next();

					final Boolean isCurrentVersion = (Boolean) TrasformerCE.getMetadato(docPrincipaleFilenet, PropertyNames.IS_CURRENT_VERSION);
					if (Boolean.TRUE.equals(isCurrentVersion)) {

						final MasterDocumentRedDTO allegatoRicercaTestuale = TrasformCE.transform(allegatoFilenetRicercaTestuale, docPrincipaleFilenet,
								TrasformerCEEnum.FROM_DOCUMENTO_TO_ALLEGATO_RICERCA_AVANZATA_CONTEXT, context);

						allegatiRicercaTestuale.add(allegatoRicercaTestuale);
						maxAllegati--;
						break;
					}
				}

			}
		}

		return allegatiRicercaTestuale;
	}

	private void convertiInParametriRicercaFilenet(final HashMap<String, Object> paramsRicercaFilenet, final ParamsRicercaAvanzataDocDTO paramsRicerca,
			final boolean ignoraAnnoCreazioneDocumento, final Long idAoo, final Connection con) {
		LOGGER.info("convertiInParametriRicercaFilenet -> START");

		paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY), idAoo);

		// Classe documentale
		paramsRicercaFilenet.put(Ricerca.CLASSE_DOCUMENTALE_FWS, pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));

		// ID Documento (Numero Documento)
		if (paramsRicerca.getNumeroDocumento() != null) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY), paramsRicerca.getNumeroDocumento());
		}

		// Barcode
		if (StringUtils.isNotBlank(paramsRicerca.getBarcode())) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.BARCODE_METAKEY), paramsRicerca.getBarcode());
		}

		boolean inserireAnno = !ignoraAnnoCreazioneDocumento;
		// Data Creazione Da
		if (paramsRicerca.getDataCreazioneDa() != null) {
			paramsRicercaFilenet.put(Ricerca.DATA_CREAZIONE_DA, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataCreazioneDa(), true));
			inserireAnno = false;
		}

		// Data Creazione A
		if (paramsRicerca.getDataCreazioneA() != null) {
			paramsRicercaFilenet.put(Ricerca.DATA_CREAZIONE_A, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataCreazioneA(), false));
			inserireAnno = false;
		}

		// Anno documento
		if (inserireAnno) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY), paramsRicerca.getAnnoDocumento());
		}

		// Legislatura
		if (paramsRicerca.getNumeroLegislatura() != null) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_LEGISLATURA_METAKEY), paramsRicerca.getNumeroLegislatura());
		}

		// Numero RDP
		if (StringUtils.isNotBlank(paramsRicerca.getNumeroRDP())) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_RDP_METAKEY), paramsRicerca.getNumeroRDP());
		}

		// Tipologia Documento
		if (isComboSelected(paramsRicerca.getDescrizioneTipologiaDocumento())) {
			final List<Integer> idsTipologiaDocumento = tipologiaDocumentoDAO.getIdsByDescrizione(paramsRicerca.getDescrizioneTipologiaDocumento(), idAoo, con);
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY),
					idsTipologiaDocumento.stream().map(c -> Integer.toString(c)).collect(Collectors.joining(",")));
		}

		// Tipo Procedimento
		if (isComboSelected(paramsRicerca.getDescrizioneTipoProcedimento())) {
			final List<Integer> idsTipoProcedimento = tipoProcedimentoDAO.getIdsByDescrizione(paramsRicerca.getDescrizioneTipoProcedimento(), con);
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY),
					idsTipoProcedimento.stream().map(c -> Integer.toString(c)).collect(Collectors.joining(",")));
		}

		// Categoria
		String categorie = Constants.EMPTY_STRING;

		// Se metto tutto non devo fare filtri su categoria
		if (!paramsRicerca.isTutto()) {
			if (paramsRicerca.isEntrata()) {
				categorie += CategoriaDocumentoEnum.DOCUMENTO_ENTRATA + ",";
			}
			if (paramsRicerca.isInterno()) {
				categorie += CategoriaDocumentoEnum.ASSEGNAZIONE_INTERNA + ",";
			}
			if (paramsRicerca.isUscita()) {
				categorie += CategoriaDocumentoEnum.DOCUMENTO_USCITA + "," + CategoriaDocumentoEnum.MOZIONE + ",";
			}
		}
		if (categorie.length() > 0) {
			categorie = categorie.substring(0, categorie.length() - 1); // Si rimuove la virgola finale
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY), categorie);
		}

		// Oggetto
		if (StringUtils.isNotBlank(paramsRicerca.getOggetto())) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY), paramsRicerca.getOggetto().trim());
		}

		// Assegnatario
		if (paramsRicerca.getTipoAssegnazione() != null && (paramsRicerca.getIdUtenteResponsabileCopiaConforme() != null || paramsRicerca.getAssegnatario() != null)) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY), paramsRicerca.getTipoAssegnazione().getId());

			if (TipoAssegnazioneEnum.COPIA_CONFORME.equals(paramsRicerca.getTipoAssegnazione()) && paramsRicerca.getIdUtenteResponsabileCopiaConforme() != null) {
				paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY), paramsRicerca.getIdUtenteResponsabileCopiaConforme());
				paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY), paramsRicerca.getIdUfficioResponsabileCopiaConforme());
			} else if (paramsRicerca.getAssegnatario() != null) {
				paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY), paramsRicerca.getAssegnatario().getIdUtente());
				paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY), paramsRicerca.getAssegnatario().getIdNodo());
			}
		}

		// Nome Fascicolo
		if (StringUtils.isNotBlank(paramsRicerca.getNomeFascicolo())) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.NOME_FASCICOLO_METAKEY), paramsRicerca.getNomeFascicolo());
		}

		// Numero Protocollo Da
		if (paramsRicerca.getNumeroProtocolloDa() != null) {
			paramsRicercaFilenet.put(Ricerca.NUMERO_PROTOCOLLO_DA, paramsRicerca.getNumeroProtocolloDa());
		}

		// Numero Protocollo A
		if (paramsRicerca.getNumeroProtocolloA() != null) {
			paramsRicercaFilenet.put(Ricerca.NUMERO_PROTOCOLLO_A, paramsRicerca.getNumeroProtocolloA());
		}

		if (paramsRicerca.getDataProtocolloDa() != null || paramsRicerca.getDataProtocolloA() != null) {
			// Data Protocollo Da
			if (paramsRicerca.getDataProtocolloDa() != null) {
				paramsRicercaFilenet.put(Ricerca.DATA_PROTOCOLLO_DA, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataProtocolloDa(), true));
			}
			// Data Protocollo A
			if (paramsRicerca.getDataProtocolloA() != null) {
				paramsRicercaFilenet.put(Ricerca.DATA_PROTOCOLLO_A, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataProtocolloA(), false));
			}
		}

		// Numero Emergenza Protocollo Da
		if (paramsRicerca.getNumeroProtocolloEmergenzaDa() != null) {
			paramsRicercaFilenet.put(Ricerca.NUMERO_PROTOCOLLO_EMERGENZA_DA, paramsRicerca.getNumeroProtocolloEmergenzaDa());
		}

		// Numero Emergenza Protocollo A
		if (paramsRicerca.getNumeroProtocolloEmergenzaA() != null) {
			paramsRicercaFilenet.put(Ricerca.NUMERO_PROTOCOLLO_EMERGENZA_A, paramsRicerca.getNumeroProtocolloEmergenzaA());
		}

		if (paramsRicerca.getDataProtocolloEmergenzaDa() != null || paramsRicerca.getDataProtocolloEmergenzaA() != null) {
			// Data Protocollo Emergenza Da
			if (paramsRicerca.getDataProtocolloEmergenzaDa() != null) {
				paramsRicercaFilenet.put(Ricerca.DATA_PROTOCOLLO_EMERGENZA_DA, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataProtocolloEmergenzaDa(), true));
			}
			// Data Protocollo Emergenza A
			if (paramsRicerca.getDataProtocolloEmergenzaA() != null) {
				paramsRicercaFilenet.put(Ricerca.DATA_PROTOCOLLO_EMERGENZA_A, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataProtocolloEmergenzaA(), false));
			}
		}

		// Protocollo Mittente
		if (paramsRicerca.getRifProtocolloMittente() != null) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_MITTENTE_METAKEY), String.valueOf(paramsRicerca.getRifProtocolloMittente()));
		}

		// Data Scadenza Da
		if (paramsRicerca.getDataScadenzaDa() != null) {
			paramsRicercaFilenet.put(Ricerca.DATA_SCADENZA_DA, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataScadenzaDa(), true));
		}

		// Data Scadenza A
		if (paramsRicerca.getDataScadenzaA() != null) {
			paramsRicercaFilenet.put(Ricerca.DATA_SCADENZA_A, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataScadenzaA(), false));
		}

		// Mittente
		if (StringUtils.isNotBlank(paramsRicerca.getMittente())) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.MITTENTE_METAKEY), paramsRicerca.getMittente());
		}

		// Destinatario
		if (StringUtils.isNotBlank(paramsRicerca.getDestinatario())) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY), paramsRicerca.getDestinatario());
		}

		// Operazione
		if (paramsRicerca.getIdTipoOperazione() != null && paramsRicerca.getAssegnatarioTipoOperazione() != null) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.EVENTLOG_ID_TIPO_EVENTO), paramsRicerca.getIdTipoOperazione());

			if (paramsRicerca.getAssegnatarioTipoOperazione().getIdUtente() != null && paramsRicerca.getAssegnatarioTipoOperazione().getIdUtente() > 0) {
				paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), paramsRicerca.getAssegnatarioTipoOperazione().getIdUtente());
			} else {
				paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), paramsRicerca.getAssegnatarioTipoOperazione().getIdNodo());
			}
		}

		// Riservato
		if (paramsRicerca.isRiservato()) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY), BooleanFlagEnum.SI.getIntValue());
		}

		// Registro Riservato
		Integer registroRiservato = BooleanFlagEnum.NO.getIntValue();
		if (paramsRicerca.isRegistroRiservato()) {
			registroRiservato = BooleanFlagEnum.SI.getIntValue();
		}
		paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY), registroRiservato);

		// Annullati
		if (paramsRicerca.isAnnullati()) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_DATA_ANNULLAMENTO), "NOT NULL");
		}

		// Urgente
		if (paramsRicerca.isUrgente()) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.URGENTE_METAKEY), BooleanFlagEnum.SI.getIntValue());
		}

		// Note
		if (StringUtils.isNotBlank(paramsRicerca.getNote())) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.NOTE_METAKEY), paramsRicerca.getNote().trim());
		}

		// Ricerca Testo
		if (paramsRicerca.getModalitaRicercaTesto() != null && StringUtils.isNotBlank(paramsRicerca.getParoleRicercaTesto())) {
			// Ricerca per "Tutte le parole"
			if (ModalitaRicercaAvanzataTestoEnum.TUTTE_LE_PAROLE.equals(paramsRicerca.getModalitaRicercaTesto())) {
				paramsRicercaFilenet.put(ModalitaRicercaAvanzataTestoEnum.TUTTE_LE_PAROLE.getValore(), paramsRicerca.getParoleRicercaTesto().trim());
			} else if (ModalitaRicercaAvanzataTestoEnum.UNA_DELLE_PAROLE.equals(paramsRicerca.getModalitaRicercaTesto())) {
				// Ricerca per "Una delle parole"
				paramsRicercaFilenet.put(ModalitaRicercaAvanzataTestoEnum.UNA_DELLE_PAROLE.getValore(), paramsRicerca.getParoleRicercaTesto().trim());
			}
		}

		// Mezzo di ricezione
		if (paramsRicerca.getIdMezzoRicezione() != null && paramsRicerca.getIdMezzoRicezione().intValue() > 0) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_MEZZORICEZIONE), paramsRicerca.getIdMezzoRicezione());
		}
		// Tipo Registro Repertorio
		if (isComboSelected(paramsRicerca.getDescrizioneRegistroRepertorio())) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.REGISTRO_PROTOCOLLO_FN_METAKEY), paramsRicerca.getDescrizioneRegistroRepertorio());
		}

		LOGGER.info("convertiInParametriRicercaFilenet -> END");
	}

	private void convertiInParametriRicercaEsitiFilenet(final HashMap<String, Object> paramsRicercaFilenet, final ParamsRicercaEsitiDTO paramsRicerca) {
		LOGGER.info("convertiInParametriRicercaEsitiFilenet -> START");

		// Classe documentale
		paramsRicercaFilenet.put(Ricerca.CLASSE_DOCUMENTALE_FWS, pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));

		// Tipo Esito
		paramsRicercaFilenet.put(Ricerca.TIPO_ESITO_DOC, paramsRicerca.getTipoEsito().getId());

		// Data Creazione Da
		if (paramsRicerca.getDataCreazioneDa() != null) {
			paramsRicercaFilenet.put(Ricerca.DATA_CREAZIONE_DA, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataCreazioneDa(), true));
		}

		// Data Creazione A
		if (paramsRicerca.getDataCreazioneA() != null) {
			paramsRicercaFilenet.put(Ricerca.DATA_CREAZIONE_A, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataCreazioneA(), false));
		}

		// Numero Protocollo Da
		if (paramsRicerca.getNumeroProtocolloDa() != null) {
			paramsRicercaFilenet.put(Ricerca.NUMERO_PROTOCOLLO_DA, paramsRicerca.getNumeroProtocolloDa());
		}

		// Numero Protocollo A
		if (paramsRicerca.getNumeroProtocolloA() != null) {
			paramsRicercaFilenet.put(Ricerca.NUMERO_PROTOCOLLO_A, paramsRicerca.getNumeroProtocolloA());
		}

		LOGGER.info("convertiInParametriRicercaEsitiFilenet -> END");
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaAvanzataDocFacadeSRV#initAllCombo(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.RicercaAvanzataDocFormDTO, boolean).
	 */
	@Override
	public void initAllCombo(final UtenteDTO utente, final RicercaAvanzataDocFormDTO ricercaAvanzataForm, final boolean onlyUlterioriFiltri) {
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			if (ricercaAvanzataForm == null) {
				throw new RedException("Errore nel caricamento dei dati della maschera.");
			}

			if (!onlyUlterioriFiltri) {

				/**
				 * COMBO TIPO DOCUMENTO start
				 *****************************************************/
				ricercaAvanzataForm.setComboTipiDocumento(getListaTipiDocumento(utente.getIdAoo()));
				ricercaAvanzataForm.setTipoDocumentoSelected((String) ricercaAvanzataForm.getComboTipiDocumento().get(0).getValue());
				/**
				 * COMBO TIPO DOCUMENTO end
				 *******************************************************/

			} else {

				/**
				 * COMBO LEGISLATURA start
				 *******************************************************/
				ricercaAvanzataForm.setComboLegislatura(getListaLegislature(con));
				/**
				 * COMBO LEGISLATURA end
				 *********************************************************/

				/**
				 * TIPOLOGIA DOCUMENTO START
				 *****************************************************/
				ricercaAvanzataForm.setComboTipologieDocumento(getListaTipologieDocumento(con, utente.getIdAoo()));
				ricercaAvanzataForm.setComboTipiProcedimento(getTipiProcedimentoByTipologiaDocumento(null, utente.getIdAoo()));
				/**
				 * TIPOLOGIA DOCUMENTO END
				 *******************************************************/

				/**
				 * MEZZI DI RICEZIONE START
				 ******************************************************/
				ricercaAvanzataForm.setComboMezziRicezione(getListaMezziDiRicezione(con));
				/**
				 * MEZZI DI RICEZIONE END
				 ********************************************************/

				/**
				 * COMBO TIPO ASSEGNAZIONE START
				 *************************************************/
				ricercaAvanzataForm.setComboTipoAssegnazione(tipoAssegnazioneDAO.getTipiAssegnazioneManualeByIdAoo(utente.getIdAoo().intValue(), con));
				ricercaAvanzataForm.setComboResponsabileCopiaConforme(utenteDAO.getComboResponsabiliCopiaConforme(utente.getIdAoo(), con));
				ricercaAvanzataForm.setAssegnatarioOrganigrammaVisible(true);
				/**
				 * COMBO TIPO ASSEGNAZIONE END
				 ***************************************************/

				/** TIPO OPERAZIONI START ***************************************************/
				ricercaAvanzataForm.setComboTipiOperazione(getListaTipiOperazione(con, utente.getIdAoo()));
				/** TIPO OPERAZIONI END *****************************************************/

				/** REGISTRO REPERTORIO START VI RICERCA **/
				ricercaAvanzataForm.setComboRegistroRepertorio(getComboRegistroRepertorio(con, utente.getIdAoo()));
				/** REGISTRO REPERTORIO END **/
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

	}

	@Override
	public final void salvaRicercaAvanzata(final String nomeRicercaAvanzata, final ParamsRicercaAvanzataDocSalvataDTO paramsRicercaAvanzataSalvata, final UtenteDTO utente) {
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), true);

			final RicercaAvanzataSalvata ricercaAvanzataSalvata = new RicercaAvanzataSalvata();
			ricercaAvanzataSalvata.setNome(nomeRicercaAvanzata);
			ricercaAvanzataSalvata.setIdUtente(utente.getId());
			ricercaAvanzataSalvata.setIdUfficio(utente.getIdUfficio());
			ricercaAvanzataSalvata.setIdRuolo(utente.getIdRuolo());
			ricercaAvanzataSalvata.setDataCreazione(new Date());

			// Si procede all'inserimento della ricerca avanzata per l'utente
			final Integer idRicerca = ricercaRedDAO.insertRicercaAvanzataSalvata(ricercaAvanzataSalvata, con);

			// Se l'inserimento è andato a buon fine, si procede con l'inserimento dei
			// parametri di ricerca relativi alla ricerca avanzata appena salvata
			if (idRicerca != null) {
				final Map<String, String> mappaCampiValore = costruisciMappaPerRicercaCampoValore(paramsRicercaAvanzataSalvata);

				for (final Entry<String, String> entryCampoValore : mappaCampiValore.entrySet()) {
					ricercaRedDAO.insertRicercaCampoValore(idRicerca, entryCampoValore.getKey(), entryCampoValore.getValue(), con);
				}
			}

			commitConnection(con);
		} catch (final Exception e) {
			rollbackConnection(con);
			LOGGER.error("salvaRicercaAvanzata -> Si è verificato un errore durante il salvataggio della ricerca avanzata per documenti con nome: " + nomeRicercaAvanzata
					+ " da parte dell'utente: " + utente.getUsername(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
	}

	@Override
	public final List<RicercaAvanzataSalvata> getAllRicercheSalvateByUtente(final Long idUtente, final Long idUfficio, final Long idRuolo) {
		LOGGER.info("getAllRicercheSalvateByUtente -> START. ID utente: " + idUtente);
		Connection con = null;
		List<RicercaAvanzataSalvata> ricercheAvanzateSalvateUtente = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			if (idUtente != null && idUfficio != null || idRuolo != null) {
				ricercheAvanzateSalvateUtente = ricercaRedDAO.getAllRicercheSalvateByUtente(idUtente, idUfficio, idRuolo, con);
			}
		} catch (final Exception e) {
			LOGGER.error("getAllRicercheSalvateByUtente -> Si è verificato un errore durante il caricamento delle ricerche avanzata salvate per l'utente con ID: " + idUtente,
					e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

		LOGGER.info("getAllRicercheSalvateByUtente -> END. ID utente: " + idUtente);
		return ricercheAvanzateSalvateUtente;
	}

	@Override
	public final void eliminaRicercaAvanzataSalvata(final Integer idRicerca) {
		LOGGER.info("eliminaRicercaAvanzataSalvata -> START. ID ricerca: " + idRicerca);
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			ricercaRedDAO.eliminaRicercaAvanzataSalvata(idRicerca, con);
		} catch (final Exception e) {
			LOGGER.error("eliminaRicercaAvanzataSalvata -> Si è verificato un errore durante l'eliminazione della ricerca avanzata salvata con ID: " + idRicerca, e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

		LOGGER.info("eliminaRicercaAvanzataSalvata -> END. ID ricerca: " + idRicerca);
	}

	@Override
	public final ParamsRicercaAvanzataDocSalvataDTO getParamsRicercaAvanzataSalvata(final Integer idRicerca) {
		LOGGER.info("getParamsRicercaAvanzataSalvata -> START. ID ricerca: " + idRicerca);
		Connection con = null;
		ParamsRicercaAvanzataDocSalvataDTO paramsRicercaAvanzataDocSalvata = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final Map<String, String> mappaCampiValori = ricercaRedDAO.getCampiValoriByIdRicerca(idRicerca, con);
			paramsRicercaAvanzataDocSalvata = costruisciParamsRicercaSalvata(mappaCampiValori);

		} catch (final Exception e) {
			LOGGER.error("getParamsRicercaAvanzataSalvata -> Si è verificato un errore durante il recupero dei parametri della ricerca avanzata salvata con ID: " + idRicerca,
					e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

		LOGGER.info("getParamsRicercaAvanzataSalvata -> END. ID ricerca: " + idRicerca);
		return paramsRicercaAvanzataDocSalvata;
	}

	private ParamsRicercaAvanzataDocSalvataDTO costruisciParamsRicercaSalvata(final Map<String, String> mappaCampiValori) {
		final String tipoDocumento = mappaCampiValori.get("tipoDocumentoSelected");
		String descrAssegnatario = null;
		String assegnatarioCopiaConforme = null;
		String descrAssegnatarioOperazione = null;

		final ParamsRicercaAvanzataDocDTO paramsRicercaAvanzata = new ParamsRicercaAvanzataDocDTO();

		paramsRicercaAvanzata.setAnnoDocumento(NumberUtils.toInt(mappaCampiValori.get("annoDocumento")));

		paramsRicercaAvanzata.setDataProtocolloDa(DateUtils.parseDate(mappaCampiValori.get("dataProtocolloDa")));
		paramsRicercaAvanzata.setDataProtocolloA(DateUtils.parseDate(mappaCampiValori.get("dataProtocolloA")));

		if (StringUtils.isNotBlank(mappaCampiValori.get(LEGISLATURA_SELECTED))) {
			paramsRicercaAvanzata.setNumeroLegislatura(Integer.parseInt(mappaCampiValori.get(LEGISLATURA_SELECTED)));
		}

		paramsRicercaAvanzata.setBarcode(mappaCampiValori.get("barcode"));

		if (StringUtils.isNotBlank(mappaCampiValori.get(NUMERO_DOCUMENTO))) {
			paramsRicercaAvanzata.setNumeroDocumento(Integer.parseInt(mappaCampiValori.get(NUMERO_DOCUMENTO)));
		}

		if (StringUtils.isNotBlank(mappaCampiValori.get(NUMERO_RDP))) {
			paramsRicercaAvanzata.setNumeroRDP(mappaCampiValori.get(NUMERO_RDP));
		}

		paramsRicercaAvanzata.setDataCreazioneDa(DateUtils.parseDate(mappaCampiValori.get("dataCreazioneDa")));
		paramsRicercaAvanzata.setDataCreazioneA(DateUtils.parseDate(mappaCampiValori.get("dataCreazioneA")));

		paramsRicercaAvanzata.setDescrizioneTipologiaDocumento(mappaCampiValori.get("tipologiaDocumentoSelected"));
		paramsRicercaAvanzata.setDescrizioneTipoProcedimento(mappaCampiValori.get("tipoProcedimentoSelected"));

		paramsRicercaAvanzata.setRiservato(Boolean.parseBoolean(mappaCampiValori.get("riservato")));

		paramsRicercaAvanzata.setTutto(Boolean.parseBoolean(mappaCampiValori.get(TUTTO)));

		paramsRicercaAvanzata.setEntrata(Boolean.parseBoolean(mappaCampiValori.get(ENTRATA)));
		paramsRicercaAvanzata.setUscita(Boolean.parseBoolean(mappaCampiValori.get(USCITA)));
		paramsRicercaAvanzata.setInterno(Boolean.parseBoolean(mappaCampiValori.get(INTERNO)));

		paramsRicercaAvanzata.setUrgente(Boolean.parseBoolean(mappaCampiValori.get("urgente")));

		paramsRicercaAvanzata.setAnnullati(Boolean.parseBoolean(mappaCampiValori.get("annullati")));

		paramsRicercaAvanzata.setOggetto(mappaCampiValori.get("oggetto"));

		if (StringUtils.isNotBlank(mappaCampiValori.get(TIPO_ASSEGNAZIONE_SELECTED))) {
			paramsRicercaAvanzata.setTipoAssegnazione(TipoAssegnazioneEnum.get(Integer.parseInt(mappaCampiValori.get(TIPO_ASSEGNAZIONE_SELECTED))));

			if (StringUtils.isNotBlank(mappaCampiValori.get(ASSEGNATARIO_ID_NODO))) {
				final Long idUfficio = Long.parseLong(mappaCampiValori.get(ASSEGNATARIO_ID_NODO));
				descrAssegnatario = mappaCampiValori.get("assegnatario_descrizione");

				AssegnatarioOrganigrammaDTO assegnatarioTipoAssegnazione = null;
				if (StringUtils.isNotBlank(mappaCampiValori.get(ASSEGNATARIO_ID_UTENTE))) {
					assegnatarioTipoAssegnazione = new AssegnatarioOrganigrammaDTO(idUfficio, Long.parseLong(mappaCampiValori.get(ASSEGNATARIO_ID_UTENTE)));

					if (StringUtils.isNotBlank(mappaCampiValori.get(ASSEGNATARIO_DESCRIZIONE_UTENTE))) {
						descrAssegnatario += " - " + mappaCampiValori.get(ASSEGNATARIO_DESCRIZIONE_UTENTE);
					}
				} else {
					assegnatarioTipoAssegnazione = new AssegnatarioOrganigrammaDTO(idUfficio);
				}

				paramsRicercaAvanzata.setAssegnatario(assegnatarioTipoAssegnazione);
			}
		}

		paramsRicercaAvanzata.setDataScadenzaDa(DateUtils.parseDate(mappaCampiValori.get("dataScadenzaDa")));
		paramsRicercaAvanzata.setDataScadenzaA(DateUtils.parseDate(mappaCampiValori.get("dataScadenzaA")));

		paramsRicercaAvanzata.setMittente(mappaCampiValori.get("mittente"));

		paramsRicercaAvanzata.setNomeFascicolo(mappaCampiValori.get("nomeFascicolo"));

		if (StringUtils.isNotBlank(mappaCampiValori.get(NUMERO_PROTOCOLLO_DA))) {
			paramsRicercaAvanzata.setNumeroProtocolloDa(Integer.parseInt(mappaCampiValori.get(NUMERO_PROTOCOLLO_DA)));
		}

		if (StringUtils.isNotBlank(mappaCampiValori.get(NUMERO_PROTOCOLLO_A))) {
			paramsRicercaAvanzata.setNumeroProtocolloA(Integer.parseInt(mappaCampiValori.get(NUMERO_PROTOCOLLO_A)));
		}

		if (StringUtils.isNotBlank(mappaCampiValori.get(RIF_PROTOCOLLO_MITTENTE))) {
			paramsRicercaAvanzata.setRifProtocolloMittente(Integer.parseInt(mappaCampiValori.get(RIF_PROTOCOLLO_MITTENTE)));
		}

		if (StringUtils.isNotBlank(mappaCampiValori.get(TIPO_OPERAZIONE_SELECTED))) {
			paramsRicercaAvanzata.setIdTipoOperazione(Integer.parseInt(mappaCampiValori.get(TIPO_OPERAZIONE_SELECTED)));

			if (StringUtils.isNotBlank(mappaCampiValori.get(TIPO_OPERAZIONE_UTENTE_SELECTED_ID_NODO))) {
				final Long idUfficio = Long.parseLong(mappaCampiValori.get(TIPO_OPERAZIONE_UTENTE_SELECTED_ID_NODO));
				descrAssegnatarioOperazione = mappaCampiValori.get("tipoOperazioneUtenteSelected_descrizione");

				AssegnatarioOrganigrammaDTO assegnatarioTipoOperazione = null;
				if (StringUtils.isNotBlank(mappaCampiValori.get(TIPO_OPERAZIONE_UTENTE_SELECTED_ID_UTENTE))) {
					assegnatarioTipoOperazione = new AssegnatarioOrganigrammaDTO(idUfficio, Long.parseLong(mappaCampiValori.get(TIPO_OPERAZIONE_UTENTE_SELECTED_ID_UTENTE)));

					descrAssegnatarioOperazione += " - " + mappaCampiValori.get("tipoOperazioneUtenteSelected_descrizioneUtente");
				} else {
					assegnatarioTipoOperazione = new AssegnatarioOrganigrammaDTO(idUfficio);
				}

				paramsRicercaAvanzata.setAssegnatarioTipoOperazione(assegnatarioTipoOperazione);
			}
		}

		paramsRicercaAvanzata.setNote(mappaCampiValori.get("note"));

		paramsRicercaAvanzata.setModalitaRicercaTesto(ModalitaRicercaAvanzataTestoEnum.get(mappaCampiValori.get("ricercaTestoSelected")));

		if (paramsRicercaAvanzata.getModalitaRicercaTesto() != null) {
			if (ModalitaRicercaAvanzataTestoEnum.TUTTE_LE_PAROLE.equals(paramsRicercaAvanzata.getModalitaRicercaTesto())) {
				paramsRicercaAvanzata.setParoleRicercaTesto(mappaCampiValori.get("tutteLeSeguentiParole"));
			} else {
				paramsRicercaAvanzata.setParoleRicercaTesto(mappaCampiValori.get("unaDelleParole"));
			}
		}

		if (StringUtils.isNotBlank(mappaCampiValori.get(MEZZO_RICEZIONE_SELECTED))) {
			paramsRicercaAvanzata.setIdMezzoRicezione(Integer.parseInt(mappaCampiValori.get(MEZZO_RICEZIONE_SELECTED)));
		}

		paramsRicercaAvanzata.setDestinatario(mappaCampiValori.get("destinatario"));

		paramsRicercaAvanzata.setRegistroRiservato(Boolean.parseBoolean(mappaCampiValori.get("registroRiservato")));

		if (StringUtils.isNotBlank(mappaCampiValori.get(RESPONSABILE_COPIA_CONFORME_SELECTED))) {
			paramsRicercaAvanzata.setIdUfficioResponsabileCopiaConforme(Long.parseLong(mappaCampiValori.get(RESPONSABILE_COPIA_CONFORME_SELECTED)));

			if (StringUtils.isNotBlank(mappaCampiValori.get(RESPONSABILE_COPIA_CONFORME_SELECTED))) {
				assegnatarioCopiaConforme = mappaCampiValori.get(RESPONSABILE_COPIA_CONFORME_SELECTED);

				if (StringUtils.isNotBlank(mappaCampiValori.get(RESPONSABILE_COPIA_CONFORME_SELECTED_ID_UTENTE))) {
					paramsRicercaAvanzata.setIdUtenteResponsabileCopiaConforme(Long.parseLong(mappaCampiValori.get(RESPONSABILE_COPIA_CONFORME_SELECTED_ID_UTENTE)));

					assegnatarioCopiaConforme += " " + mappaCampiValori.get(RESPONSABILE_COPIA_CONFORME_SELECTED_ID_UTENTE);
				}
			}
		}

		return new ParamsRicercaAvanzataDocSalvataDTO(paramsRicercaAvanzata, tipoDocumento, descrAssegnatario, assegnatarioCopiaConforme, descrAssegnatarioOperazione);
	}

	private Map<String, String> costruisciMappaPerRicercaCampoValore(final ParamsRicercaAvanzataDocSalvataDTO paramsRicercaAvanzataSalvata) {
		final Map<String, String> mappaCampiValori = new HashMap<>();
		final SimpleDateFormat ddMMyyyy = new SimpleDateFormat(DateUtils.DD_MM_YYYY);

		final ParamsRicercaAvanzataDocDTO paramsRicercaAvanzata = paramsRicercaAvanzataSalvata.getParametri();

		if (paramsRicercaAvanzata.getAnnoDocumento() > 0) {
			mappaCampiValori.put("annoDocumento", String.valueOf(paramsRicercaAvanzata.getAnnoDocumento()));
		}

		if (paramsRicercaAvanzata.getDataProtocolloDa() != null) {
			mappaCampiValori.put("dataProtocolloDa", ddMMyyyy.format(paramsRicercaAvanzata.getDataProtocolloDa()));
		}

		if (paramsRicercaAvanzata.getDataProtocolloA() != null) {
			mappaCampiValori.put("dataProtocolloA", ddMMyyyy.format(paramsRicercaAvanzata.getDataProtocolloA()));
		}

		if (StringUtils.isNotBlank(paramsRicercaAvanzataSalvata.getTipoDocumento())) {
			mappaCampiValori.put("tipoDocumentoSelected", paramsRicercaAvanzataSalvata.getTipoDocumento());
		}

		if (paramsRicercaAvanzata.getNumeroLegislatura() != null) {
			mappaCampiValori.put(LEGISLATURA_SELECTED, String.valueOf(paramsRicercaAvanzata.getNumeroLegislatura()));
		}

		if (StringUtils.isNotBlank(paramsRicercaAvanzata.getBarcode())) {
			mappaCampiValori.put("barcode", paramsRicercaAvanzata.getBarcode());
		}

		if (paramsRicercaAvanzata.getNumeroDocumento() != null) {
			mappaCampiValori.put(NUMERO_DOCUMENTO, String.valueOf(paramsRicercaAvanzata.getNumeroDocumento()));
		}

		if (StringUtils.isNotBlank(paramsRicercaAvanzata.getNumeroRDP())) {
			mappaCampiValori.put(NUMERO_RDP, paramsRicercaAvanzata.getNumeroRDP());
		}

		if (paramsRicercaAvanzata.getDataCreazioneDa() != null) {
			mappaCampiValori.put("dataCreazioneDa", ddMMyyyy.format(paramsRicercaAvanzata.getDataCreazioneDa()));
		}

		if (paramsRicercaAvanzata.getDataCreazioneA() != null) {
			mappaCampiValori.put("dataCreazioneA", ddMMyyyy.format(paramsRicercaAvanzata.getDataCreazioneA()));
		}

		if (paramsRicercaAvanzata.getDescrizioneTipologiaDocumento() != null && !"-".equals(paramsRicercaAvanzata.getDescrizioneTipologiaDocumento())
				&& StringUtils.isNotBlank(paramsRicercaAvanzata.getDescrizioneTipologiaDocumento())) {
			mappaCampiValori.put("tipologiaDocumentoSelected", paramsRicercaAvanzata.getDescrizioneTipologiaDocumento());
		}

		if (paramsRicercaAvanzata.getDescrizioneTipoProcedimento() != null && !"-".equals(paramsRicercaAvanzata.getDescrizioneTipoProcedimento())
				&& StringUtils.isNotBlank(paramsRicercaAvanzata.getDescrizioneTipologiaDocumento())) {
			mappaCampiValori.put("tipoProcedimentoSelected", paramsRicercaAvanzata.getDescrizioneTipoProcedimento());
		}

		if (paramsRicercaAvanzata.isRiservato()) {
			mappaCampiValori.put("riservato", String.valueOf(paramsRicercaAvanzata.isRiservato()));
		}

		if (paramsRicercaAvanzata.isTutto()) {
			mappaCampiValori.put(TUTTO, String.valueOf(paramsRicercaAvanzata.isTutto()));
			mappaCampiValori.put(ENTRATA, String.valueOf(false));
			mappaCampiValori.put(USCITA, String.valueOf(false));
			mappaCampiValori.put(INTERNO, String.valueOf(false));
		} else {

			mappaCampiValori.put(TUTTO, String.valueOf(false));

			if (paramsRicercaAvanzata.isEntrata()) {
				mappaCampiValori.put(ENTRATA, String.valueOf(paramsRicercaAvanzata.isEntrata()));
			}

			if (paramsRicercaAvanzata.isUscita()) {
				mappaCampiValori.put(USCITA, String.valueOf(paramsRicercaAvanzata.isUscita()));
			}

			if (paramsRicercaAvanzata.isInterno()) {
				mappaCampiValori.put(INTERNO, String.valueOf(paramsRicercaAvanzata.isInterno()));
			}
		}

		if (paramsRicercaAvanzata.isUrgente()) {
			mappaCampiValori.put("urgente", String.valueOf(paramsRicercaAvanzata.isUrgente()));
		}

		if (paramsRicercaAvanzata.isAnnullati()) {
			mappaCampiValori.put("annullati", String.valueOf(paramsRicercaAvanzata.isAnnullati()));
		}

		if (StringUtils.isNotBlank((paramsRicercaAvanzata.getOggetto()))) {
			mappaCampiValori.put("oggetto", paramsRicercaAvanzata.getOggetto());
		}

		if (paramsRicercaAvanzata.getTipoAssegnazione() != null) {
			mappaCampiValori.put(TIPO_ASSEGNAZIONE_SELECTED, String.valueOf(paramsRicercaAvanzata.getTipoAssegnazione().getId()));

			if (paramsRicercaAvanzata.getAssegnatario() != null) {
				final AssegnatarioOrganigrammaDTO assegnatario = paramsRicercaAvanzata.getAssegnatario();
				mappaCampiValori.put(ASSEGNATARIO_ID_NODO, String.valueOf(assegnatario.getIdNodo()));

				if (assegnatario.getIdUtente() != null) {
					mappaCampiValori.put(ASSEGNATARIO_ID_UTENTE, String.valueOf(assegnatario.getIdUtente()));
				}

				if (StringUtils.isNotBlank(paramsRicercaAvanzataSalvata.getDescrAssegnatario())) {
					final String[] assegnatarioSplit = paramsRicercaAvanzataSalvata.getDescrAssegnatario().split(" - ", 1);
					mappaCampiValori.put("assegnatario_descrizione", assegnatarioSplit[0]);
					if (assegnatarioSplit.length > 1) {
						mappaCampiValori.put(ASSEGNATARIO_DESCRIZIONE_UTENTE, assegnatarioSplit[1]);
					}
				}
			}
		}

		if (paramsRicercaAvanzata.getDataScadenzaDa() != null) {
			mappaCampiValori.put("dataScadenzaDa", ddMMyyyy.format(paramsRicercaAvanzata.getDataScadenzaDa()));
		}

		if (paramsRicercaAvanzata.getDataScadenzaA() != null) {
			mappaCampiValori.put("dataScadenzaA", ddMMyyyy.format(paramsRicercaAvanzata.getDataScadenzaA()));
		}

		if (StringUtils.isNotBlank(paramsRicercaAvanzata.getMittente())) {
			mappaCampiValori.put("mittente", paramsRicercaAvanzata.getMittente());
		}

		if (StringUtils.isNotBlank(paramsRicercaAvanzata.getNomeFascicolo())) {
			mappaCampiValori.put("nomeFascicolo", paramsRicercaAvanzata.getNomeFascicolo());
		}

		if (paramsRicercaAvanzata.getNumeroProtocolloDa() != null) {
			mappaCampiValori.put(NUMERO_PROTOCOLLO_DA, String.valueOf(paramsRicercaAvanzata.getNumeroProtocolloDa()));
		}

		if (paramsRicercaAvanzata.getNumeroProtocolloA() != null) {
			mappaCampiValori.put(NUMERO_PROTOCOLLO_A, String.valueOf(paramsRicercaAvanzata.getNumeroProtocolloA()));
		}

		if (paramsRicercaAvanzata.getRifProtocolloMittente() != null) {
			mappaCampiValori.put(RIF_PROTOCOLLO_MITTENTE, String.valueOf(paramsRicercaAvanzata.getRifProtocolloMittente()));
		}

		if (paramsRicercaAvanzata.getIdTipoOperazione() != null && paramsRicercaAvanzata.getIdTipoOperazione() > 0) {
			mappaCampiValori.put(TIPO_OPERAZIONE_SELECTED, String.valueOf(paramsRicercaAvanzata.getIdTipoOperazione()));

			if (paramsRicercaAvanzata.getAssegnatarioTipoOperazione() != null) {
				final AssegnatarioOrganigrammaDTO assegnatarioTipoOperazione = paramsRicercaAvanzata.getAssegnatarioTipoOperazione();
				mappaCampiValori.put(TIPO_OPERAZIONE_UTENTE_SELECTED_ID_NODO, String.valueOf(assegnatarioTipoOperazione.getIdNodo()));

				if (assegnatarioTipoOperazione.getIdUtente() != null) {
					mappaCampiValori.put(TIPO_OPERAZIONE_UTENTE_SELECTED_ID_UTENTE, String.valueOf(assegnatarioTipoOperazione.getIdUtente()));
				}

				if (StringUtils.isNotBlank(paramsRicercaAvanzataSalvata.getDescrAssegnatarioOperazione())) {
					final String[] assegnatarioOperazioneSplit = paramsRicercaAvanzataSalvata.getDescrAssegnatarioOperazione().split(" - ");
					mappaCampiValori.put("tipoOperazioneUtenteSelected_descrizione", assegnatarioOperazioneSplit[0]);
					if (assegnatarioOperazioneSplit.length > 1) {
						mappaCampiValori.put("tipoOperazioneUtenteSelected_descrizioneUtente", assegnatarioOperazioneSplit[1]);
					}
				}
			}
		}

		if (StringUtils.isNotBlank(paramsRicercaAvanzata.getNote())) {
			mappaCampiValori.put("note", paramsRicercaAvanzata.getNote());
		}

		if (paramsRicercaAvanzata.getModalitaRicercaTesto() != null) {
			mappaCampiValori.put("ricercaTestoSelected", paramsRicercaAvanzata.getModalitaRicercaTesto().name());

			if (StringUtils.isNotBlank(paramsRicercaAvanzata.getParoleRicercaTesto())) {
				if (ModalitaRicercaAvanzataTestoEnum.TUTTE_LE_PAROLE.equals(paramsRicercaAvanzata.getModalitaRicercaTesto())) {
					mappaCampiValori.put("tutteLeSeguentiParole", paramsRicercaAvanzata.getParoleRicercaTesto());
				} else {
					mappaCampiValori.put("unaDelleParole", paramsRicercaAvanzata.getParoleRicercaTesto());
				}
			}
		}

		if (paramsRicercaAvanzata.getIdMezzoRicezione() != null && paramsRicercaAvanzata.getIdMezzoRicezione() > 0) {
			mappaCampiValori.put(MEZZO_RICEZIONE_SELECTED, String.valueOf(paramsRicercaAvanzata.getIdMezzoRicezione()));
		}

		if (StringUtils.isNotBlank(paramsRicercaAvanzata.getDestinatario())) {
			mappaCampiValori.put("destinatario", paramsRicercaAvanzata.getDestinatario());
		}

		if (paramsRicercaAvanzata.isRegistroRiservato()) {
			mappaCampiValori.put("registroRiservato", String.valueOf(paramsRicercaAvanzata.isRegistroRiservato()));
		}

		if (paramsRicercaAvanzata.getIdUfficioResponsabileCopiaConforme() != null) {
			mappaCampiValori.put(RESPONSABILE_COPIA_CONFORME_SELECTED, String.valueOf(paramsRicercaAvanzata.getIdUfficioResponsabileCopiaConforme()));

			if (paramsRicercaAvanzata.getIdUtenteResponsabileCopiaConforme() != null) {
				mappaCampiValori.put(RESPONSABILE_COPIA_CONFORME_SELECTED_ID_UTENTE, String.valueOf(paramsRicercaAvanzata.getIdUtenteResponsabileCopiaConforme()));
			}
		}

		return mappaCampiValori;
	}

	private List<SelectItemDTO> getListaTipiDocumento(final Long idAOO) {
		final List<SelectItemDTO> comboTipiDocumento = new ArrayList<>();
		comboTipiDocumento.add(new SelectItemDTO("RED", "Documento RED"));
		if (idAOO.intValue() == Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.IDAOO_RGS))) {
			comboTipiDocumento.add(new SelectItemDTO("PRELEX", "Documento Prelex"));
		}

		return comboTipiDocumento;
	}

	/**
	 * @param con puo' essere null. Se e' null crea la connessione al db al momento
	 *            e la richiude nel finally
	 * @return
	 */
	private List<Legislatura> getListaLegislature(final Connection inCon) {
		final List<Legislatura> comboLegislatura = new ArrayList<>();
		Connection con = inCon;
		final boolean closeConnection = (con == null);

		try {
			if (closeConnection) {
				con = setupConnection(getDataSource().getConnection(), false);
			}
			comboLegislatura.add(new Legislatura(0, "-", null, null)); // campo vuoto
			comboLegislatura.addAll(legislaturaDAO.getLegislature(con));

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			if (closeConnection) {
				closeConnection(con);
			}
		}

		return comboLegislatura;
	}

	@Override
	public final List<TipologiaDocumentoDTO> getListaTipologieDocumento(final Long idAoo) {
		List<TipologiaDocumentoDTO> list = new ArrayList<>();
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			list = getListaTipologieDocumento(con, idAoo);

		} catch (final SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

		return list;
	}

	/**
	 * @see it.ibm.red.business.service.IRicercaAvanzataDocSRV#getListaTipologieDocumento(java.sql.Connection,
	 *      java.lang.Long).
	 */
	@Override
	public List<TipologiaDocumentoDTO> getListaTipologieDocumento(final Connection inCon, final Long idAoo) {
		final List<TipologiaDocumentoDTO> list = new ArrayList<>();
		Connection con = inCon;
		final boolean closeConnection = (con == null);
		try {
			if (closeConnection) {
				con = setupConnection(getDataSource().getConnection(), false);
			}

			list.add(new TipologiaDocumentoDTO(null, null, null, "-", null, null, null, null, null, null, null, null, false, null, null));

			final List<TipologiaDocumentoDTO> tmplist = tipologiaDocumentoDAO.getComboTipologieByAoo(idAoo, con);
			final List<String> descrizioni = new ArrayList<>();

			for (final TipologiaDocumentoDTO tipoDoc : tmplist) {
				if (isEntrataUscita(tmplist, tipoDoc)) {
					// Memorizzo tutte le tipologie ENTRATA/USCITA prendendo solo quelle in ENTRATA
					if (TipoCategoriaEnum.ENTRATA.getIdTipoCategoria().equals(tipoDoc.getIdTipoCategoria())) {
						descrizioni.add(tipoDoc.getDescrizione());
						list.add(tipoDoc);
					}
				} else {
					descrizioni.add(tipoDoc.getDescrizione());
					list.add(tipoDoc);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			if (closeConnection) {
				closeConnection(con);
			}
		}

		return list;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaAvanzataDocFacadeSRV#getTipiProcedimentoByTipologiaDocumento(java.lang.String,
	 *      java.lang.Long).
	 */
	@Override
	public List<TipoProcedimentoDTO> getTipiProcedimentoByTipologiaDocumento(final String descrTipologiaDocumento, final Long idAOO) {
		final List<TipoProcedimentoDTO> list = new ArrayList<>();
		list.add(new TipoProcedimentoDTO(new TipoProcedimento(0L, "-"))); // mi serve purtroppo la voce vuota

		if (StringUtils.isBlank(descrTipologiaDocumento) || "-".equals(descrTipologiaDocumento.trim())) {
			return list;
		}

		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			final List<Integer> ids = tipologiaDocumentoDAO.getIdsByDescrizione(descrTipologiaDocumento, idAOO, connection);
			boolean toInsert = true;
			for (final Integer id : ids) {
				for (final TipoProcedimento t : tipoProcedimentoDAO.getTipiProcedimentoByTipologiaDocumento(connection, id)) {
					for (final TipoProcedimentoDTO tAlreadyIn : list) {
						if (tAlreadyIn.getDescrizione().equals(t.getDescrizione())) {
							toInsert = false;
							break;
						}
					}
					if (toInsert) {
						list.add(new TipoProcedimentoDTO(t));
					}
				}
			}
		} catch (final SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return list;
	}

	/**
	 * Restituisce il permesso associato al tipo operazione identificato dall'id:
	 * <code> idTipoOperazione </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IRicercaAvanzataDocFacadeSRV#
	 *      getPermessoByTipoOperazione(java.lang.Integer).
	 * @param idTipoOperazione
	 *            identificativo tipo operazione
	 * @return permesso associato al tipo operazione, @see PermessiEnum
	 */
	@Override
	public PermessiEnum getPermessoByTipoOperazione(final Integer idTipoOperazione) {
		final TipoOperazionePermessoEnum t = TipoOperazionePermessoEnum.getById(idTipoOperazione);
		PermessiEnum permesso = null;
		if (t != null) {
			switch (t) {
			case FIRMA:
				permesso = PermessiEnum.FIRMA;
				break;

			case SIGLA:
				permesso = PermessiEnum.SIGLA;
				break;

			case VISTO:
				permesso = PermessiEnum.VISTO;
				break;
			// AGGIUNGERE VENTUALI OPERAZIONI/PERMESSI DA RICERCARE
			default:
				permesso = null;
				break;
			}
		}
		return permesso;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaAvanzataDocFacadeSRV#getRegistriAusiliariByTipologieDocumento(java.util.Collection).
	 */
	@Override
	public Collection<RegistroDTO> getRegistriAusiliariByTipologieDocumento(final Collection<Integer> idsTipologieDocumento) {
		Collection<RegistroDTO> registri = new ArrayList<>();
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			registri = registroAusiliarioDAO.getRegistriByTipologieDocumento(connection, idsTipologieDocumento);

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei registri ausiliari.", e);
			throw new RedException("Errore durante il recupero dei registri ausiliari.", e);
		} finally {
			closeConnection(connection);
		}

		return registri;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaAvanzataDocFacadeSRV#ricercaEsitiUscita(it.ibm.red.business.dto.ParamsRicercaEsitiDTO,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public Collection<String> ricercaEsitiUscita(final ParamsRicercaEsitiDTO paramsRicerca, final IFilenetCEHelper fceh) {
		final Collection<String> output = new ArrayList<>();

		try {

			final HashMap<String, Object> paramsRicercaFilenet = new HashMap<>();
			convertiInParametriRicercaEsitiFilenet(paramsRicercaFilenet, paramsRicerca);

			DocumentSet documentiFilenet = null;
			LOGGER.info("eseguiRicercaEsiti (UCB) -> START");

			documentiFilenet = fceh.ricercaEsitiUscita(paramsRicercaFilenet);

			if (documentiFilenet != null && !documentiFilenet.isEmpty()) {
				final Iterator<?> itDsFileNet = documentiFilenet.iterator();

				while (itDsFileNet.hasNext()) {
					final Document document = (Document) itDsFileNet.next();
					final String documentTitle = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
					output.add(documentTitle);
				}

			}

			LOGGER.info("eseguiRicercaEsiti (UCB) -> END");

		} catch (final Exception e) {
			LOGGER.error("Errore durante la ricerca degli esiti/documenti uscita.", e);
			throw new RedException("Errore durante la ricerca degli esiti/documenti uscita.", e);
		}

		return output;
	}

	/**
	 * @param con
	 * @param idAoo
	 * @return
	 */
	private List<TipoEvento> getListaTipiOperazione(final Connection con, final Long idAoo) {
		final List<TipoEvento> list = new ArrayList<>();

		try {
			list.add(new TipoEvento(0, "-", 0L, 0L));
			list.addAll(tipoEventoDAO.getAllEntitaDocumento(idAoo, con));
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}

		return list;
	}

	/**
	 * @param inCon
	 * @return
	 */
	private List<MezzoRicezioneDTO> getListaMezziDiRicezione(final Connection inCon) {
		final List<MezzoRicezioneDTO> list = new ArrayList<>();
		Connection con = inCon;
		final boolean closeConnection = (con == null);

		try {
			if (closeConnection) {
				con = setupConnection(getDataSource().getConnection(), false);
			}

			list.add(new MezzoRicezioneDTO(0, "-", null));
			list.addAll(mezzoRicezioneDAO.getMezziRicezioneByIdFormato(FormatoDocumentoEnum.CARTACEO, con));

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			if (closeConnection) {
				closeConnection(con);
			}
		}

		return list;
	}

	private boolean isComboSelected(final String comboValue) {
		boolean isComboSelected = false;

		if (StringUtils.isNotBlank(comboValue) && !"-".equals(comboValue)) {
			isComboSelected = true;
		}

		return isComboSelected;
	}

	private List<RegistroRepertorioDTO> getComboRegistroRepertorio(final Connection conn, final Long idAoo) {
		final List<RegistroRepertorioDTO> list = new ArrayList<>();

		try {
			list.add(new RegistroRepertorioDTO(false, "-"));
			list.add(registroRepertorioDAO.getDenominazioneRegistroUfficiale(idAoo, conn));
			list.addAll(registroRepertorioDAO.getAllRegistroRepertorio(idAoo, conn));
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		}

		return list;
	}

	/**
	 * Metodo che consente di verificare il tipo categoria del tipo procedimento. Il
	 * metodo verifica il numero di occorrenze del procedimento in una lista, se è
	 * entrata e uscita esisterà due volte
	 * 
	 * @param documento La tipologia per cui occorre conoscere se è di tipo Entrata
	 *                  e Uscita
	 * @param tipi      Occorre la lista di tutti le tipologie perché una tipologia
	 *                  entrata e uscita è una tipologia che esiste in duplice
	 *                  copia, una gestisce l'entrata e una l'uscita.
	 */
	private static boolean isEntrataUscita(final Collection<TipologiaDocumentoDTO> tipi, final TipologiaDocumentoDTO documento) {
		final List<String> descrTipi = new ArrayList<>();

		for (final TipologiaDocumentoDTO p : tipi) {
			descrTipi.add(p.getDescrizione());
		}

		int numeroTipologieOmonime = 0;
		for (final String descr : descrTipi) {
			if (documento.getDescrizione().equals(descr)) {
				numeroTipologieOmonime++;
			}
		}

		return (numeroTipologieOmonime == 2);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaAvanzataDocFacadeSRV#getTipiProcedimentoByIdTipologiaDocumentoDescrProc(java.lang.String,
	 *      java.lang.Long, java.lang.Integer).
	 */
	@Override
	public TipoProcedimentoDTO getTipiProcedimentoByIdTipologiaDocumentoDescrProc(final String descrTipologiaProcedimento, final Long idAOO,
			final Integer idTipologiaDocumento) {
		TipoProcedimentoDTO tipoProcedimento = null;

		if (StringUtils.isBlank(descrTipologiaProcedimento) || "-".equals(descrTipologiaProcedimento.trim())) {
			return tipoProcedimento;
		}

		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			final List<TipoProcedimento> tipiProcedimentoByTipologiaDocumento = tipoProcedimentoDAO.getTipiProcedimentoByTipologiaDocumento(connection, idTipologiaDocumento);
			for (final TipoProcedimento t : tipiProcedimentoByTipologiaDocumento) {
				if (descrTipologiaProcedimento.equals(t.getDescrizione())) {
					tipoProcedimento = new TipoProcedimentoDTO(t);
					break;
				}
			}

		} catch (final SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return tipoProcedimento;
	}

}