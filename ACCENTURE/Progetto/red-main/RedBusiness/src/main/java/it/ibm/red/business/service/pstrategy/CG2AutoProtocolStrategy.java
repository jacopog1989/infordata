package it.ibm.red.business.service.pstrategy;

import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.MessaggioPostaNpsFlussoDTO;
import it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO;
import it.ibm.red.business.enums.TipoContestoProceduraleEnum;

/**
 * Strategy di protocollazione automatica flusso CG2.
 */
public class CG2AutoProtocolStrategy extends AUTAutoProtocolStrategy {

	/**
	 * @see it.ibm.red.business.service.pstrategy.AUTAutoProtocolStrategy#protocol(it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO,
	 *      java.lang.String).
	 */
	@Override
	public EsitoSalvaDocumentoDTO protocol(final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> richiestaElabMessagio, final String guidMail) {
		return protocol(richiestaElabMessagio, guidMail, TipoContestoProceduraleEnum.FLUSSO_CG2);
	}
	
}
