package it.ibm.red.business.helper.filenet.pe.trasform.impl;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.NotaDTO;
import it.ibm.red.business.dto.PEDocumentoDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.TipoOperazioneLibroFirmaEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.logger.REDLogger;

/**
 * The Class FromWFToDocumentoTrasformer.
 *
 * @author CPIERASC
 * 
 *         Oggetto per trasformare un workflow in un documento.
 */
public class FromWFToDocumentoTrasformer extends FromWFToGenericDocumentTrasformer<PEDocumentoDTO> {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FromWFToDocumentoTrasformer.class.getName());

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1705454340138423161L;

	/**
	 * Costruttore.
	 */
	public FromWFToDocumentoTrasformer() {
		super(TrasformerPEEnum.FROM_WF_TO_DOCUMENTO);
	}

	/**
	 * Costruttore.
	 * 
	 * @param inEnumKey
	 */
	protected FromWFToDocumentoTrasformer(final TrasformerPEEnum inEnumKey) {
		super(inEnumKey);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.pe.trasform.impl.FromWFToGenericDocumentTrasformer#getLogger().
	 */
	@Override
	protected REDLogger getLogger() {
		return LOGGER;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.pe.trasform.impl.FromWFToGenericDocumentTrasformer#createGeneric(filenet.vw.api.VWWorkObject,
	 *      java.lang.String,
	 *      it.ibm.red.business.enums.TipoOperazioneLibroFirmaEnum,
	 *      java.lang.String, it.ibm.red.business.enums.DocumentQueueEnum,
	 *      java.lang.Integer, java.lang.Integer, java.lang.Boolean,
	 *      java.lang.String, java.lang.String[], java.lang.Integer,
	 *      java.lang.String, java.lang.Integer, java.lang.Integer,
	 *      java.lang.String, java.lang.String[], java.lang.Integer,
	 *      java.lang.String, java.lang.Boolean, java.lang.Boolean,
	 *      java.lang.Boolean, java.lang.String, it.ibm.red.business.dto.NotaDTO,
	 *      java.lang.Integer, java.lang.Integer, int, boolean, java.lang.Boolean).
	 */
	@Override
	PEDocumentoDTO createGeneric(final VWWorkObject object, final String idDocumento,
			final TipoOperazioneLibroFirmaEnum tolf, final String wobNumber, final DocumentQueueEnum documentQueueEnum,
			final Integer idUtenteDestinatario, final Integer idNodoDestinatario, final Boolean bFirmaFig,
			final String motivazioneAssegnazione, final String[] responses, final Integer idFascicolo,
			final String codaCorrente, final Integer tipoAssegnazioneId, final Integer flagIterManuale,
			final String subject, final String[] elencoLibroFirma, final Integer count, final String dataCreazioneWF,
			final Boolean flagRenderizzatoBool, final Boolean firmaCopiaConforme, final Boolean urgente,
			final String stepName, final NotaDTO notaDTO, final Integer contributiRichiesti,
			final Integer contributiPervenuti, final int tipoFirma, final boolean annullaTemplate,
			final Boolean firmaAsincronaAvviata) {
		return new PEDocumentoDTO(idDocumento, tolf, wobNumber, documentQueueEnum, idUtenteDestinatario,
				idNodoDestinatario, bFirmaFig, motivazioneAssegnazione, responses, idFascicolo, codaCorrente,
				tipoAssegnazioneId, flagIterManuale, subject, elencoLibroFirma, count, dataCreazioneWF,
				flagRenderizzatoBool, firmaCopiaConforme, urgente, stepName, notaDTO, contributiRichiesti,
				contributiPervenuti, tipoFirma, annullaTemplate, firmaAsincronaAvviata);
	}

}
