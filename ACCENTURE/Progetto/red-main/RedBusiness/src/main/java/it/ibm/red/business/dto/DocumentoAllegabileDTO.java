package it.ibm.red.business.dto;

import java.math.BigDecimal;
import java.util.Date;

import javax.activation.DataHandler;

import it.ibm.red.business.enums.ValueMetadatoVerificaFirmaEnum;

/**
 * DTO di un documento allegabile.
 */
public class DocumentoAllegabileDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 3128854065101622334L;

	/**
	 * Enum che definisce il tipo di documento allegabile.
	 */
	public enum DocumentoAllegabileType {

		/**
		 * documento.
		 */
		DOCUMENTO,

		/**
		 * Allegato.
		 */
		ALLEGATO,

		/**
		 * Mail.
		 */
		EMAIL
	}

	/**
	 * Document title documento principale.
	 */
	private String documentTitleDocumentoPrincipale;

	/**
	 * Document title.
	 */
	private String documentTitle;

	/**
	 * Descrizione del protocollo e del numero del documento
	 * 
	 * numProt/annoProt - numeroDocumento
	 * 
	 * Tuttavia qualora manchi ad esempio il numero protocollo vedrai solamente
	 * l'anno. Se manca il numero documento puoi vedere solo il protocollo o l'anno.
	 * 
	 * Se non è possibile generarlo corrisponde a null.
	 */
	private String numProtId;

	/**
	 * Oggetto.
	 */
	private String oggetto;

	/**
	 * Sintesi oggetot.
	 */
	private String oggettoBreve;

	/**
	 * Nome del file.
	 */
	private String nomeFile;

	/**
	 * Sintesi nome file.
	 */
	private String nomeFileBreve;

	/**
	 * Content type.
	 */
	private String contentType;

	/**
	 * Handler file.
	 */
	private transient DataHandler contenuto;

	/**
	 * Dimensione.
	 */
	private BigDecimal dimensione;

	/**
	 * Flag tipo documento.
	 */
	private DocumentoAllegabileType tipo;

	/**
	 * Flag selezionato.
	 */
	private boolean selected;

	/**
	 * Flag disabilitato.
	 */
	private boolean disabled;

	/**
	 * Flag obbligatorio.
	 */
	private boolean mandatory;

	/**
	 * Fascicolo.
	 */
	private FascicoloDTO fascicolo;

	/**
	 * Tipologia documento.
	 */
	private Integer idTipologiaDocumento;

	/**
	 * Identificativo documento.
	 */
	private String guid;

	/**
	 * Data documento.
	 */
	private Date dataDocumento;

	/**
	 * Numero protocollo.
	 */
	private Integer numeroProtocollo;

	/**
	 * Data protocollo.
	 */
	private Date dataProtocollo;

	/**
	 * Anno protocollo.
	 */
	private Integer annoProtocollo;

	/**
	 * Numero documento.
	 */
	private Integer numeroDocumento;

	/**
	 * Anno documento.
	 */
	private Integer annoDocumento;

	/**
	 * Identificativo categoria.
	 */
	private Integer idCategoria;

	/**
	 * Valore verifica firma.
	 */
	private ValueMetadatoVerificaFirmaEnum valoreVerificaFirma;

	/**
	 * Flag allegato non sbustato.
	 */
	private boolean allegatoNonSbustato;

	/**
	 * Flag visualizza info verifica firma.
	 */
	private boolean visualizzaInfoVerificaFirma;

	/**
	 * Costruttore.
	 * 
	 * @param documentTitleDocumentoPrincipale
	 * @param documentTitle
	 * @param numProtId
	 * @param oggetto
	 * @param nomeFile
	 * @param contentType
	 * @param contenuto
	 * @param dimensione
	 * @param tipo
	 * @param fascicolo
	 * @param idTipologiaDocumento
	 * @param guid
	 * @param dataDocumento
	 * @param numeroProtocollo
	 * @param dataProtocollo
	 * @param annoProtocollo
	 * @param numeroDocumento
	 * @param annoDocumento
	 * @param idCategoria
	 * @param allegatoNonSbustato
	 * @param visualizzaInfoVerificaFirma
	 * @param valoreVerificaFirma
	 */
	public DocumentoAllegabileDTO(final String documentTitleDocumentoPrincipale, final String documentTitle, final String numProtId, final String oggetto,
			final String nomeFile, final String contentType, final DataHandler contenuto, final BigDecimal dimensione, final DocumentoAllegabileType tipo,
			final FascicoloDTO fascicolo, final Integer idTipologiaDocumento, final String guid, final Date dataDocumento, final Integer numeroProtocollo,
			final Date dataProtocollo, final Integer annoProtocollo, final Integer numeroDocumento, final Integer annoDocumento, final Integer idCategoria,
			final boolean allegatoNonSbustato, final boolean visualizzaInfoVerificaFirma, final ValueMetadatoVerificaFirmaEnum valoreVerificaFirma) {
		this(documentTitle, oggetto, nomeFile, contentType, contenuto, dimensione, tipo, guid, dataDocumento);
		this.documentTitleDocumentoPrincipale = documentTitleDocumentoPrincipale;
		this.numProtId = numProtId;
		this.fascicolo = fascicolo;
		this.idTipologiaDocumento = idTipologiaDocumento;
		this.numeroProtocollo = numeroProtocollo;
		this.dataProtocollo = dataProtocollo;
		this.annoProtocollo = annoProtocollo;
		this.numeroDocumento = numeroDocumento;
		this.annoDocumento = annoDocumento;
		this.idCategoria = idCategoria;
		this.allegatoNonSbustato = allegatoNonSbustato;
		this.setVisualizzaInfoVerificaFirma(visualizzaInfoVerificaFirma);
		this.valoreVerificaFirma = valoreVerificaFirma;
	}

	/**
	 * Costruttore.
	 * 
	 * @param documentTitle
	 * @param oggetto
	 * @param nomeFile
	 * @param contentType
	 * @param contenuto
	 * @param dimensione
	 * @param tipo
	 * @param guid
	 * @param dataDocumento
	 */
	public DocumentoAllegabileDTO(final String documentTitle, final String oggetto, final String nomeFile, final String contentType, final DataHandler contenuto,
			final BigDecimal dimensione, final DocumentoAllegabileType tipo, final String guid, final Date dataDocumento) {
		this.documentTitle = documentTitle;
		this.oggetto = oggetto;
		this.nomeFile = nomeFile;
		this.contentType = contentType;
		this.contenuto = contenuto;
		this.dimensione = dimensione;
		this.tipo = tipo;
		this.guid = guid;
		this.dataDocumento = dataDocumento;
	}

	/**
	 * Restituisce il documentTitle del documento principale.
	 * 
	 * @return documentTitle
	 */
	public String getDocumentTitleDocumentoPrincipale() {
		return documentTitleDocumentoPrincipale;
	}

	/**
	 * Imposta il documentTitle del documento principale.
	 * 
	 * @param documentTitleDocumentoPrincipale
	 */
	public void setDocumentTitleDocumentoPrincipale(final String documentTitleDocumentoPrincipale) {
		this.documentTitleDocumentoPrincipale = documentTitleDocumentoPrincipale;
	}

	/**
	 * Restituisce il documentTitle.
	 * 
	 * @return documentTitle
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Imposta il documentTitle.
	 * 
	 * @param documentTitle
	 */
	public void setDocumentTitle(final String documentTitle) {
		this.documentTitle = documentTitle;
	}

	/**
	 * Restituisce il numero del protocollo id.
	 * 
	 * @return numero protocollo id
	 */
	public String getNumProtId() {
		return numProtId;
	}

	/**
	 * Imposta il numero del protocollo id.
	 * 
	 * @param numProtId
	 */
	public void setNumProtId(final String numProtId) {
		this.numProtId = numProtId;
	}

	/**
	 * Restituisce l'oggetto.
	 * 
	 * @return oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * Imposta l'oggetto.
	 * 
	 * @param oggetto
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}

	/**
	 * Restituisce l'oggetto breve.
	 * 
	 * @return oggetto breve
	 */
	public String getOggettoBreve() {
		return oggettoBreve;
	}

	/**
	 * Imposta l'oggetto breve.
	 * 
	 * @param oggettoBreve
	 */
	public void setOggettoBreve(final String oggettoBreve) {
		this.oggettoBreve = oggettoBreve;
	}

	/**
	 * Restituisce il tipo dell'allegato.
	 * 
	 * @see DocumentoAllegabileType
	 * @return tipo
	 */
	public DocumentoAllegabileType getTipo() {
		return tipo;
	}

	/**
	 * Imposta il tipo dell'allegato.
	 * 
	 * @param tipo
	 */
	public void setTipo(final DocumentoAllegabileType tipo) {
		this.tipo = tipo;
	}

	/**
	 * Restituisce il nome file dell'allegato.
	 * 
	 * @return nome file allegato
	 */
	public String getNomeFile() {
		return nomeFile;
	}

	/**
	 * Imposta il nome file dell'allegato.
	 * 
	 * @param nomeFile
	 */
	public void setNomeFile(final String nomeFile) {
		this.nomeFile = nomeFile;
	}

	/**
	 * Restituisce il nome file breve.
	 * 
	 * @return nome breve file
	 */
	public String getNomeFileBreve() {
		return nomeFileBreve;
	}

	/**
	 * Imposta il nome file breve.
	 * 
	 * @param nomeFileBreve
	 */
	public void setNomeFileBreve(final String nomeFileBreve) {
		this.nomeFileBreve = nomeFileBreve;
	}

	/**
	 * Restituisce il content dell'allegato come DataHandler.
	 * 
	 * @return contenuto
	 */
	public DataHandler getContenuto() {
		return contenuto;
	}

	/**
	 * Imposta il contenuto dell'allegato.
	 * 
	 * @param contenuto
	 */
	public void setContenuto(final DataHandler contenuto) {
		this.contenuto = contenuto;
	}

	/**
	 * Restituisce il tipo del contenuto.
	 * 
	 * @return contentType
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * Imposta il tipo del contenuto del file.
	 * 
	 * @param contentType
	 */
	public void setContentType(final String contentType) {
		this.contentType = contentType;
	}

	/**
	 * Restituisce la dimensione dell'allegato come BigDecimal.
	 * 
	 * @return dimensione
	 */
	public BigDecimal getDimensione() {
		return dimensione;
	}

	/**
	 * Imposta la dimensione dell'allegato.
	 * 
	 * @param dimensione
	 */
	public void setDimensione(final BigDecimal dimensione) {
		this.dimensione = dimensione;
	}

	/**
	 * Restituisce true se selezionato, false altrimenti.
	 * 
	 * @return true se selezionato, false altrimenti
	 */
	public boolean isSelected() {
		return selected;
	}

	/**
	 * Imposta il flag: selected.
	 * 
	 * @param selected
	 */
	public void setSelected(final boolean selected) {
		this.selected = selected;
	}

	/**
	 * Restituisce true se disabilitato, false altrimenti.
	 * 
	 * @return true se disabilitato, false altrimenti
	 */
	public boolean isDisabled() {
		return disabled;
	}

	/**
	 * Imposta il flag: disabled.
	 * 
	 * @param disabled
	 */
	public void setDisabled(final boolean disabled) {
		this.disabled = disabled;
	}

	/**
	 * Restituisce true se obbligatorio, false altrimenti.
	 * 
	 * @return true se obbligatorio, false altrimenti
	 */
	public boolean isMandatory() {
		return mandatory;
	}

	/**
	 * Imposta l'obbligatorieta.
	 * 
	 * @param mandatory
	 */
	public void setMandatory(final boolean mandatory) {
		this.mandatory = mandatory;
	}

	/**
	 * Restituisce il fascicolo associato.
	 * 
	 * @return fascicolo
	 */
	public FascicoloDTO getFascicolo() {
		return fascicolo;
	}

	/**
	 * Imposta il fascicolo associato.
	 * 
	 * @param fascicolo
	 */
	public void setFascicolo(final FascicoloDTO fascicolo) {
		this.fascicolo = fascicolo;
	}

	/**
	 * Restituisce l'id della tipologia documento associata al documento di
	 * riferimento.
	 * 
	 * @return id della tipologia documento
	 */
	public Integer getIdTipologiaDocumento() {
		return idTipologiaDocumento;
	}

	/**
	 * Imposta l'id della tipologia documento associata al documento di riferimento.
	 * 
	 * @param idTipologiaDocumento
	 */
	public void setIdTipologiaDocumento(final Integer idTipologiaDocumento) {
		this.idTipologiaDocumento = idTipologiaDocumento;
	}

	/**
	 * Restituisce il guid.
	 * 
	 * @return guid
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * Imposta il guid.
	 * 
	 * @param guid
	 */
	public void setGuid(final String guid) {
		this.guid = guid;
	}

	/**
	 * Restituisce la data del documento.
	 * 
	 * @return data documento
	 */
	public Date getDataDocumento() {
		return dataDocumento;
	}

	/**
	 * Imposta la data del documento.
	 * 
	 * @param dataDocumento
	 */
	public void setDataDocumento(final Date dataDocumento) {
		this.dataDocumento = dataDocumento;
	}

	/**
	 * Restituisce il numero del protocollo.
	 * 
	 * @return numero protocollo
	 */
	public Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * Imposta il numero protocollo.
	 * 
	 * @param numeroProtocollo
	 */
	public void setNumeroProtocollo(final Integer numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}

	/**
	 * Restituisce la data del protocollo.
	 * 
	 * @return data protocollo
	 */
	public Date getDataProtocollo() {
		return dataProtocollo;
	}

	/**
	 * Imposta la data protocollo.
	 * 
	 * @param dataProtocollo
	 */
	public void setDataProtocollo(final Date dataProtocollo) {
		this.dataProtocollo = dataProtocollo;
	}

	/**
	 * Restituisce l'anno del protocollo.
	 * 
	 * @return anno protocollo
	 */
	public Integer getAnnoProtocollo() {
		return annoProtocollo;
	}

	/**
	 * Imposta l'anno del protocollo.
	 * 
	 * @param annoProtocollo
	 */
	public void setAnnoProtocollo(final Integer annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}

	/**
	 * Restituisce il numero del documento.
	 * 
	 * @return numero documento
	 */
	public Integer getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * Imposta il numero documento.
	 * 
	 * @param numeroDocumento
	 */
	public void setNumeroDocumento(final Integer numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * Restituisce l'anno del documento.
	 * 
	 * @return anno documento
	 */
	public Integer getAnnoDocumento() {
		return annoDocumento;
	}

	/**
	 * Imposta l'anno del documento.
	 * 
	 * @param annoDocumento
	 */
	public void setAnnoDocumento(final Integer annoDocumento) {
		this.annoDocumento = annoDocumento;
	}

	/**
	 * Restituisce l'id della categoria associata.
	 * 
	 * @return id categoria
	 */
	public Integer getIdCategoria() {
		return idCategoria;
	}

	/**
	 * Imposta l'id della categoria.
	 * 
	 * @param idCategoria
	 */
	public void setIdCategoria(final Integer idCategoria) {
		this.idCategoria = idCategoria;
	}

	/**
	 * Restituisce il valore di verifica firma.
	 * 
	 * @return ValueMetadatoVerificaFirmaEnum
	 */
	public ValueMetadatoVerificaFirmaEnum getValoreVerificaFirma() {
		return valoreVerificaFirma;
	}

	/**
	 * Imposta il valore di verifica firma.
	 * 
	 * @param valoreVerificaFirma
	 */
	public void setValoreVerificaFirma(final ValueMetadatoVerificaFirmaEnum valoreVerificaFirma) {
		this.valoreVerificaFirma = valoreVerificaFirma;
	}

	/**
	 * Restituisce true se l'allegato è non sbustato.
	 * 
	 * @return true se non sbustato, false altrimenti.
	 */
	public boolean getAllegatoNonSbustato() {
		return allegatoNonSbustato;
	}

	/**
	 * Imposta il flag: allegatoNonSbustato.
	 * 
	 * @param allegatoNonSbustato
	 */
	public void setAllegatoNonSbustato(final boolean allegatoNonSbustato) {
		this.allegatoNonSbustato = allegatoNonSbustato;
	}

	/**
	 * @return visualizzaInfoVerificaFirma
	 */
	public boolean getVisualizzaInfoVerificaFirma() {
		return visualizzaInfoVerificaFirma;
	}

	/**
	 * @param visualizzaInfoVerificaFirma
	 */
	public void setVisualizzaInfoVerificaFirma(final boolean visualizzaInfoVerificaFirma) {
		this.visualizzaInfoVerificaFirma = visualizzaInfoVerificaFirma;
	}

}