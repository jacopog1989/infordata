package it.ibm.red.business.service.asign.step;

/**
 * Enum context di firma.
 */
public enum ContextASignEnum {
	
	/**
	 * Doc filenet.
	 */
	DOC_FN_GUID_LIST,
	
	/**
	 * Doc interno - Guid.
	 */
	DOC_INTERNO_GUID_LIST,
	
	/**
	 * Doc interno - WobNumber.
	 */
	DOC_INTERNO_WOB_NUMBER_LIST
	
	;
	
}
