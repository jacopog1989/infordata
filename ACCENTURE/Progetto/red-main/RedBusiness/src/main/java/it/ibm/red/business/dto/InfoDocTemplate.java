package it.ibm.red.business.dto;

/**
 * @author SLac.
 *
 */
public class InfoDocTemplate extends AbstractDTO {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 2164860367953841369L;

	/**
	 * Label VUOTO.
	 */
	public static final String VUOTO = "[VUOTO]";
	
	/**
	 * Separatore destinatari.
	 */
	public static final String DESTINATARI_SEPARATOR = "%DEST_SEP%";
	
	/**
	 * Carattere separatore dei protocolli.
	 */
	public static final String PROTOCOLLI_SEPARATOR = ", ";
	
	/**
	 * Oggetto.
	 */
	private String oggetto;
	
	/**
	 * Destinatari.
	 */
	private String destinatari;
	
	/**
	 * Mittente.
	 */
	private String mittente;
	
	/**
	 * Protocollo mittente.
	 */
	private String protocolloMittente;
	
	/**
	 * @see java.lang.Object#toString().
	 */
	@Override
	public String toString() {

		final String separator = "|";

		return new StringBuilder(oggetto != null ? oggetto : VUOTO).append(separator)
				.append(destinatari != null ? destinatari : VUOTO).append(separator)
				.append(mittente != null ? mittente : VUOTO).append(separator)
				.append(protocolloMittente != null ? protocolloMittente : VUOTO).toString();
	}
	
	/**
	 * Recupera l'oggetto.
	 * @return oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}
	
	/**
	 * Imposta l'oggetto.
	 * @param oggetto
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}
	
	/**
	 * Recupera i destinatari.
	 * @return destinatari
	 */
	public String getDestinatari() {
		return destinatari;
	}
	
	/**
	 * Imposta i destinatari.
	 * @param destinatari
	 */
	public void setDestinatari(final String destinatari) {
		this.destinatari = destinatari;
	}
	
	/**
	 * Recupera il mittente.
	 * @return mittente
	 */
	public String getMittente() {
		return mittente;
	}
	
	/**
	 * Imposta il mittente.
	 * @param mittente
	 */
	public void setMittente(final String mittente) {
		this.mittente = mittente;
	}
	
	/**
	 * Recupera il protocollo mittente.
	 * @return protocollo mittente
	 */
	public String getProtocolloMittente() {
		return protocolloMittente;
	}
	
	/**
	 * Imposta il protocollo mittente.
	 * @param protocolloMittente protocollo mittente
	 */
	public void setProtocolloMittente(final String protocolloMittente) {
		this.protocolloMittente = protocolloMittente;
	}
}