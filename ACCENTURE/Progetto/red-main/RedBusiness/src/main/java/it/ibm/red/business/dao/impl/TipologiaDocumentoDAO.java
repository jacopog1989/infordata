package it.ibm.red.business.dao.impl;

import java.io.IOException;
import java.io.Reader;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.ITipologiaDocumentoDAO;
import it.ibm.red.business.dao.IValutaDAO;
import it.ibm.red.business.dto.AnagraficaDipendenteDTO;
import it.ibm.red.business.dto.AnagraficaDipendentiComponentDTO;
import it.ibm.red.business.dto.CapitoloSpesaMetadatoDTO;
import it.ibm.red.business.dto.DatiDocumentoContiConsuntiviDTO;
import it.ibm.red.business.dto.DatiNotificaContiConsuntiviDTO;
import it.ibm.red.business.dto.EstrattiContoUcbDTO;
import it.ibm.red.business.dto.KeyValueDTO;
import it.ibm.red.business.dto.LookupTableDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.ParamsRicercaContiConsuntiviDTO;
import it.ibm.red.business.dto.ParamsRicercaEstrattiContoDTO;
import it.ibm.red.business.dto.RisultatoRicercaContiConsuntiviDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.TipologiaRegistroNsdDTO;
import it.ibm.red.business.enums.ContiConsuntiviEnum;
import it.ibm.red.business.enums.ContoGiudizialeEnum;
import it.ibm.red.business.enums.EstrattiContoCCVTEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ReportQueryEnum;
import it.ibm.red.business.enums.StringMatchModeEnum;
import it.ibm.red.business.enums.TipoAllaccioEnum;
import it.ibm.red.business.enums.TipoMetadatoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.metadatiestesi.MetadatiEstesiHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.utils.StringUtils;

/**
 * Dao per la gestione delle tipologie di documento.
 *
 * @author mcrescentini
 */
@Repository
public class TipologiaDocumentoDAO extends AbstractDAO implements ITipologiaDocumentoDAO {
	
	/**
	 * La costante serial version UID.
	 */
	private static final long serialVersionUID = -2146662669658340538L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TipologiaDocumentoDAO.class.getName());
	
	/**
	 * Colonna Valore.
	 */
	private static final String VALORE = "VALORE";

	/**
	 * Colonna posizione.
	 */
	private static final String POSIZIONE = "POSIZIONE";

	/**
	 * Colonna Xml Metadati.
	 */
	private static final String MED_XML_METADATI = "MED_XML_METADATI";

	/**
	 * Colonna Id Documento per Metadati.
	 */
	private static final String MED_ID_DOCUMENTO = "MED_ID_DOCUMENTO";

	/**
	 * Etichetta.
	 */
	private static final String LABEL = "LABEL";

	/**
	 * Colonna Id DOcumento.
	 */
	private static final String ID_DOCUMENTO = "ID_DOCUMENTO";

	/**
	 * Colonna Id Documento Allacciato.
	 */
	private static final String IDDOCUMENTOALLACCIATO = "IDDOCUMENTOALLACCIATO";

	/**
	 * Label.
	 */
	private static final String DAL_DB = " dal DB.";

	/**
	 * Query parziale.
	 */
	private static final String WHERE_MED_MED_ID_AOO_AND_MED_MED_ID_TIPOLOGIA_DOC_AND_MED_MED_ID_TIPO_PROC = " WHERE med.MED_ID_AOO = ? AND med.MED_ID_TIPOLOGIA_DOC = ? AND med.MED_ID_TIPO_PROC = ?";

	/**
	 * Clausula order by Posizione.
	 */
	private static final String ORDER_BY_POSIZIONE = " ORDER BY POSIZIONE";

	/**
	 * FROM Metadati estesi documento.
	 */
	private static final String FROM_METADATI_ESTESI_DOCUMENTO_MED = " FROM METADATI_ESTESI_DOCUMENTO med";

	/**
	 * Label - And.
	 */
	private static final String AND_PAR_DX_AP = " AND (";

	/**
	 * Query parziale.
	 */
	private static final String TD_IDITERAPPROVATIVO_TD_FLAGVISTO_TD_CONTABILE = " td.iditerapprovativo, td.flagvisto, td.contabile, ";

	/**
	 * Query parziale.
	 */
	private static final String TD_IDTIPOCATEGORIA_TD_IDTIPOLOGIADOCUMENTOUSCITA = " td.idtipocategoria, td.idtipologiadocumentouscita, ";

	/**
	 * Mezzo creazione - MANUALE.
	 */
	private static final String MEZZOCREAZIONE_MANUALE = "MANUALE";
	
	/**
	 * Mezzo creazione - FLUSSO.
	 */
	private static final String MEZZOCREAZIONE_FLUSSO = "FLUSSO";
	
	/**
	 * Dao gestione valute.
	 */
	@Autowired
	private IValutaDAO valutaDAO;

	/**
	 * @see it.ibm.red.business.dao.ITipologiaDocumentoDAO#getClasseDocumentaleByIdTipologia(int,
	 *      java.sql.Connection).
	 */
	@Override
	public String getClasseDocumentaleByIdTipologia(final int idTipologiaDocumento, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String classeDocumentale = null;

		try {
			final String querySQL = "SELECT documentclass FROM classedocumentale CD, tipologiadocumento TD" + " WHERE TD.idclassedocumentale = CD.idclassedocumentale"
					+ " AND TD.idtipologiadocumento = ?";
			ps = connection.prepareStatement(querySQL);
			ps.setInt(1, idTipologiaDocumento);
			rs = ps.executeQuery();

			while (rs.next()) {
				classeDocumentale = rs.getString("documentclass");
			}
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero della classe documentale per la tipologia documento: " + idTipologiaDocumento, e);
		} finally {
			closeStatement(ps, rs);
		}

		return classeDocumentale;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipologiaDocumentoDAO#getComboTipologieByAooAndTipoCategoria(java.lang.Long,
	 *      java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public List<TipologiaDocumentoDTO> getComboTipologieByAooAndTipoCategoria(final Long idAoo, final Integer idTipoCategoria, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<TipologiaDocumentoDTO> result = null;

		try {
			final String querySQL = getComboTipByAooAndTipoCatQuery(idTipoCategoria);

			int index = 1;
			ps = connection.prepareStatement(querySQL);
			ps.setLong(index++, idAoo);
			if ((idTipoCategoria != null)) {
				ps.setInt(index++, idTipoCategoria);
			}
			rs = ps.executeQuery();

			final List<TipologiaDocumentoDTO> list = new ArrayList<>();
			TipologiaDocumentoDTO tipologiaGenerica = null;
			TipologiaDocumentoDTO item = null;

			while (rs.next()) {
				item = populate(rs);

				if (idTipoCategoria != null && "TIPOLOGIA GENERICA".equalsIgnoreCase(item.getDescrizione())) {
					tipologiaGenerica = item;
				} else {
					list.add(item);
				}
			}

			// Meccanismo per mettere tipologia generica per prima a prescindere
			// dall'ordinamento della query.
			// Lo faccio nel DAO per ottimizzare.
			result = new ArrayList<>();
			if (tipologiaGenerica != null) {
				result.add(tipologiaGenerica);
			}
			if (!list.isEmpty()) {
				result.addAll(list);
			}

		} catch (final SQLException e) {
			throw new RedException("Errore nel recupero delle tipologie documento: idAoo=" + idAoo + ", idTipoCategoria=" + idTipoCategoria, e);
		} finally {
			closeStatement(ps, rs);
		}

		return result;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipologiaDocumentoDAO#getComboTipologieByUtenteAndTipoCategoria(boolean,
	 *      boolean, boolean, java.lang.Long, java.lang.Long, java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public List<TipologiaDocumentoDTO> getComboTipologieByUtenteAndTipoCategoria(final boolean onlyAttivi, final boolean onlyDisattivi, final boolean escludiAutomatici,
			final Long idAoo, final Long idUfficio, final Integer idTipoCategoria, final Connection connection) {
		final List<TipologiaDocumentoDTO> output = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			final List<TipologiaDocumentoDTO> tmpList = new ArrayList<>();
			TipologiaDocumentoDTO itemTipologiaGenerica = null;
			TipologiaDocumentoDTO item = null;
			int index = 1;
			final String idNodoUtenteString = idUfficio + Constants.EMPTY_STRING;
			boolean aggiungi;
			boolean trovato;

			final String querySQL = "SELECT td.idtipologiadocumento, " + " LISTAGG(tn.idnodo, ',') WITHIN GROUP(ORDER BY tn.idnodo) AS nodi, td.idaoo, "
					+ " td.descrizione, td.dataattivazione, td.datadisattivazione, " + TD_IDTIPOCATEGORIA_TD_IDTIPOLOGIADOCUMENTOUSCITA
					+ TD_IDITERAPPROVATIVO_TD_FLAGVISTO_TD_CONTABILE + " td.idclassedocumentale, td.flagfirmacopiaconforme, td.ordinamento, "
					+ " cld.documentclass FROM tipologiadocumento td " + " LEFT OUTER JOIN tipologiadocumento_nodo tn ON tn.idtipologiadocumento = td.idtipologiadocumento "
					+ " INNER JOIN classedocumentale cld ON td.idclassedocumentale = cld.idclassedocumentale "
					+ ((escludiAutomatici) ? " LEFT OUTER JOIN tipologiadocumentoprocedimento tdp ON td.idtipologiadocumento = tdp.idtipologiadocumento " : "")
					+ " WHERE td.idaoo = ? "
					// Eventuale filtro per categoria (entrata/uscita)
					+ ((idTipoCategoria != null) ? " AND td.idtipocategoria = ? " : "")
					// Eventuale esclusione delle tipologie attive/disattive
					+ ((onlyAttivi) ? " AND td.datadisattivazione IS NULL " : (onlyDisattivi ? " AND td.datadisattivazione IS NOT NULL " : ""))
					// Eventuale esclusione delle tipologie associate ai tipi procedimento con tipo
					// gestione automatica (A)
					+ ((escludiAutomatici) ? " AND (tdp.tipogestione IS NULL OR tdp.tipogestione <> 'A') " : "")
					+ " GROUP BY td.idtipologiadocumento, td.idaoo, td.descrizione, " + " td.dataattivazione, td.datadisattivazione, "
					+ TD_IDTIPOCATEGORIA_TD_IDTIPOLOGIADOCUMENTOUSCITA + TD_IDITERAPPROVATIVO_TD_FLAGVISTO_TD_CONTABILE
					+ " td.idclassedocumentale, td.flagfirmacopiaconforme, " + " td.ordinamento, cld.documentclass " + " ORDER BY td.descrizione, td.ordinamento ASC";

			ps = connection.prepareStatement(querySQL);
			ps.setLong(index++, idAoo);
			if (idTipoCategoria != null) {
				ps.setInt(index++, idTipoCategoria);
			}

			rs = ps.executeQuery();

			while (rs.next()) {
				aggiungi = true;

				item = populate(rs);

				// Si filtra ulteriormente per la visibilita del nodo
				if (!StringUtils.isNullOrEmpty(item.getNodi())) {
					trovato = false;
					final String[] visNodi = item.getNodi().split(",");

					for (final String nodo : visNodi) {
						if (nodo.equals(idNodoUtenteString)) {
							trovato = true;
							break;
						}
					}

					aggiungi = trovato;
				}

				if (aggiungi) {
					if ("TIPOLOGIA GENERICA".equalsIgnoreCase(item.getDescrizione())) {
						itemTipologiaGenerica = item;
					} else {
						tmpList.add(item);
					}
				}
			}

			// Meccanismo per mettere tipologia generica per prima a prescindere
			// dall'ordinamento della query.
			// Lo faccio nel DAO per ottimizzare.
			if (itemTipologiaGenerica != null) {
				output.add(itemTipologiaGenerica);
			}

			if (!tmpList.isEmpty()) {
				output.addAll(tmpList);
			}
		} catch (final SQLException e) {
			throw new RedException("Errore nel recupero delle tipologie documento con idAoo: " + idAoo + ", idUfficio: " + idUfficio + ", idTipoCategoria: " + idTipoCategoria,
					e);
		} finally {
			closeStatement(ps, rs);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipologiaDocumentoDAO#getById(int,
	 *      java.sql.Connection).
	 */
	@Override
	public TipologiaDocumentoDTO getById(final int idTipologiaDocumento, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		TipologiaDocumentoDTO tipologiaDocumento = null;

		try {
			int index = 1;
			ps = connection.prepareStatement("SELECT td.idtipologiadocumento , LISTAGG(tn.idnodo, ',') WITHIN GROUP (ORDER BY tn.idnodo) AS nodi,"
					+ " td.idaoo, td.descrizione, td.dataattivazione, " + " td.datadisattivazione, td.idtipocategoria, td.idtipologiadocumentouscita, td.iditerapprovativo, "
					+ " td.flagvisto, td.contabile, td.idclassedocumentale, td.flagfirmacopiaconforme, td.ordinamento, cld.documentclass "
					+ " FROM tipologiadocumento td left outer join tipologiadocumento_nodo tn on tn.idtipologiadocumento = td.idtipologiadocumento "
					+ " inner join classedocumentale cld on td.idclassedocumentale = cld.idclassedocumentale " + " where td.idtipologiadocumento = ? "
					+ " GROUP BY td.idtipologiadocumento, td.idaoo, td.descrizione, td.dataattivazione, td.datadisattivazione, td.idtipocategoria, "
					+ " td.idtipologiadocumentouscita, td.iditerapprovativo, " + " td.flagvisto, td.contabile, td.idclassedocumentale, td.flagfirmacopiaconforme, "
					+ " td.ordinamento, cld.documentclass");
			ps.setInt(index++, idTipologiaDocumento);
			rs = ps.executeQuery();

			if (rs.next()) {
				tipologiaDocumento = populate(rs);
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero dei dati per TipologiaDocumento con idtipologiadocumento: " + idTipologiaDocumento + DAL_DB, e);
			throw new RedException("Errore durante il recupero dei dati perTipologiaDocumento con idtipologiadocumento: " + idTipologiaDocumento + DAL_DB);
		} finally {
			closeStatement(ps, rs);
		}

		return tipologiaDocumento;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipologiaDocumentoDAO#getComboTipologieByAoo(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public List<TipologiaDocumentoDTO> getComboTipologieByAoo(final Long idAoo, final Connection connection) {
		return getComboTipologieByAooAndTipoCategoria(idAoo, null, connection);
	}

	/**
	 * @see it.ibm.red.business.dao.ITipologiaDocumentoDAO#getIdsByDescrizione(java.lang.String,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<Integer> getIdsByDescrizione(final String descrizioneTipologiaDocumento, final Long idAoo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<Integer> result = new ArrayList<>();

		try {
			final String querySQL = " SELECT td.idtipologiadocumento FROM tipologiadocumento td WHERE LOWER(td.descrizione) = ? AND td.idaoo = ?";
			ps = connection.prepareStatement(querySQL);

			int index = 1;
			ps.setString(index++, descrizioneTipologiaDocumento.toLowerCase());
			ps.setLong(index++, idAoo);

			rs = ps.executeQuery();

			while (rs.next()) {
				result.add(rs.getInt("idtipologiadocumento"));
			}
		} catch (final SQLException e) {
			throw new RedException("Errore nel recupero delle tipologie documento con ID AOO: " + idAoo + " e descrizione: " + descrizioneTipologiaDocumento, e);
		} finally {
			closeStatement(ps, rs);
		}

		return result;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipologiaDocumentoDAO#getTipologiaByClasseDocumentaleAndAooAndidTipologiaDocumento(java.lang.String,
	 *      java.lang.Long, int, java.sql.Connection).
	 */
	@Override
	public List<TipologiaDocumentoDTO> getTipologiaByClasseDocumentaleAndAooAndidTipologiaDocumento(final String classeDocumentale, final Long idAoo,
			final int idTipologiaDocumento, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<TipologiaDocumentoDTO> tipologiaDocumentoList = new ArrayList<>();

		try {
			int index = 1;
			ps = connection.prepareStatement("SELECT td.idtipologiadocumento , LISTAGG(tn.idnodo, ',') WITHIN GROUP (ORDER BY tn.idnodo) AS nodi,"
					+ " td.idaoo, td.descrizione, td.dataattivazione, " + " td.datadisattivazione, td.idtipocategoria, td.idtipologiadocumentouscita, td.iditerapprovativo, "
					+ " td.flagvisto, td.contabile, td.idclassedocumentale, td.flagfirmacopiaconforme, td.ordinamento, cld.documentclass "
					+ " FROM tipologiadocumento td left outer join tipologiadocumento_nodo tn on tn.idtipologiadocumento = td.idtipologiadocumento "
					+ " inner join classedocumentale cld on td.idclassedocumentale = cld.idclassedocumentale "
					+ " where  cld.documentclass = ? and td.IDAOO = ? and td.idtipocategoria = ?"
					+ " GROUP BY td.idtipologiadocumento, td.idaoo, td.descrizione, td.dataattivazione, td.datadisattivazione, td.idtipocategoria, "
					+ " td.idtipologiadocumentouscita, td.iditerapprovativo, " + " td.flagvisto, td.contabile, td.idclassedocumentale, td.flagfirmacopiaconforme, "
					+ " td.ordinamento, cld.documentclass");
			ps.setString(index++, classeDocumentale);
			ps.setLong(index++, idAoo);
			ps.setInt(index++, idTipologiaDocumento);
			rs = ps.executeQuery();

			while (rs.next()) {
				final TipologiaDocumentoDTO tipologiaDocumento = populate(rs);
				tipologiaDocumentoList.add(tipologiaDocumento);
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero dei dati per TipologiaDocumento con idtipologiadocumento: " + idTipologiaDocumento + DAL_DB, e);
			throw new RedException("Errore durante il recupero dei dati perTipologiaDocumento con idtipologiadocumento: " + idTipologiaDocumento + DAL_DB);
		} finally {
			closeStatement(ps, rs);
		}

		return tipologiaDocumentoList;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipologiaDocumentoDAO#getTipologieDocumentiAggiuntivi(java.sql.Connection).
	 */
	@Override
	public List<TipologiaDocumentoDTO> getTipologieDocumentiAggiuntivi(final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<TipologiaDocumentoDTO> resultList = new ArrayList<>();
		try {
			ps = con.prepareStatement(
					"SELECT idtipologiadocaggiuntivo, descrizione FROM tipologiadocumentoaggiuntivo WHERE datadisattivazione is NULL ORDER BY idtipologiadocaggiuntivo ASC");
			rs = ps.executeQuery();
			while (rs.next()) {
				final TipologiaDocumentoDTO tipologiaDocumento = new TipologiaDocumentoDTO();
				tipologiaDocumento.setIdTipologiaDocAggiuntivo(rs.getString("idtipologiadocaggiuntivo"));
				tipologiaDocumento.setDescrizione(rs.getString("descrizione"));
				resultList.add(tipologiaDocumento);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei dati per TipologiaDocumentoAggiuntivo dal DB: " + e.getMessage(), e);
			throw new RedException("Errore durante il recupero dei dati per TipologiaDocumento dal DB");
		} finally {
			closeStatement(ps, rs);
		}

		return resultList;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipologiaDocumentoDAO#save(java.lang.String,
	 *      java.lang.String, long, int, java.sql.Connection).
	 */
	@Override
	public int save(final String nomeTipoDocumento, final String tipoGestione, final long idAoo, final int idTipoCategoria, final Connection connection) {
		PreparedStatement ps = null;
		final ResultSet rs = null;

		// 1. Ottenimento id tipologia da salvare e id classe documentale della
		// tipologia
		final int idTipo = getSequenceValueOfTipologiaDocumento(connection);

		// Calcolo dell'id della classe documentale
		final String classPrefix = getClasseDocumentalePrefix(connection, idAoo);
		final int idClasseDocumentale = getClasseDocumentale(connection, classPrefix);

		// 2. Salvo la tipologia documento
		int index = 1;
		final String queryInsert = "INSERT INTO TIPOLOGIADOCUMENTO (IDTIPOLOGIADOCUMENTO, IDAOO, DESCRIZIONE, DATAATTIVAZIONE, "
				+ "DATADISATTIVAZIONE, IDTIPOCATEGORIA, IDTIPOLOGIADOCUMENTOUSCITA, IDITERAPPROVATIVO, FLAGVISTO, "
				+ "CONTABILE, IDCLASSEDOCUMENTALE, FLAGFIRMACOPIACONFORME, ORDINAMENTO) VALUES (?,?,?,SYSDATE,?,?,?,?,?,?,?,?,?)";
		if (idTipo != 0) {
			try {
				ps = connection.prepareStatement(queryInsert);
				ps.setInt(index++, idTipo);
				ps.setLong(index++, idAoo);
				ps.setString(index++, nomeTipoDocumento);
				ps.setNull(index++, Types.DATE);
				ps.setInt(index++, idTipoCategoria);
				ps.setInt(index++, 0);
				ps.setInt(index++, 0);
				ps.setInt(index++, 0);
				ps.setInt(index++, 0);
				ps.setInt(index++, idClasseDocumentale);
				ps.setNull(index++, Types.INTEGER);
				ps.setString(index++, nomeTipoDocumento);

				ps.executeUpdate();
			} catch (final Exception e) {
				throw new RedException("Errore durante la creazione della tipologia di documento", e);
			} finally {
				closeStatement(ps);
				closeResultset(rs);
			}
		}

		return idTipo;

	}

	@Override
	public final HashMap<String, String> ricercaMetadatiEstesi(final Connection con, final Collection<MetadatoDTO> metadatiEstesi, final List<String> ids, final Long idAoo,
			final String descTipologiaDocumento, final String descTipoProcedimento) {
		final HashMap<String, String> output = new HashMap<>();
		final StringBuilder queryString = new StringBuilder();

		if (!CollectionUtils.isEmpty(metadatiEstesi)) {
			queryString.append("SELECT * FROM METADATI_ESTESI_DOCUMENTO WHERE MED_ID_AOO = ?"
					+ " AND MED_ID_TIPOLOGIA_DOC IN (SELECT IDTIPOLOGIADOCUMENTO FROM TIPOLOGIADOCUMENTO WHERE DESCRIZIONE = ?)"
					+ " AND MED_ID_TIPO_PROC IN (SELECT IDTIPOPROCEDIMENTO FROM TIPOPROCEDIMENTO WHERE DESCRIZIONE = ?)");

			// Se ci sono ID documento in input, considero solamente i metadati degli ID dei
			// documenti selezionati
			// nel rispetto della dimensione massima della IN ORACLE
			if (!CollectionUtils.isEmpty(ids)) {
				queryString.append(" AND MED_ID_DOCUMENTO IN (");
				for (int i = 0; i < Math.min(999, ids.size()); i++) {
					queryString.append(ids.get(i)).append(",");
				}

				queryString.delete(queryString.length() - 1, queryString.length());
				queryString.append(")");
			}

			// Considero i filtri sui metadati
			for (final MetadatoDTO metadato : metadatiEstesi) {

				String operator = "=";
				if (Boolean.TRUE.equals(metadato.getFlagRangeStart())) {
					operator = ">=";
				} else if (Boolean.TRUE.equals(metadato.getFlagRangeStop())) {
					operator = "<=";
				} else if (metadato.getType().equals(TipoMetadatoEnum.STRING) || metadato.getType().equals(TipoMetadatoEnum.TEXT_AREA)) {
					operator = "LIKE";
				}

				queryString.append(AND_PAR_DX_AP);

				final Boolean isMultiple = TipoMetadatoEnum.PERSONE_SELECTOR.equals(metadato.getType());

				for (final String value : getValueFromMetadato(metadato)) {
					String tmpValue = value; 
					if (metadato.getType().equals(TipoMetadatoEnum.STRING)) {
						if (metadato.getStringMatchMode().equals(StringMatchModeEnum.START_WITH)) {
							tmpValue = tmpValue + "%";
						} else if (metadato.getStringMatchMode().equals(StringMatchModeEnum.END_WITH)) {
							tmpValue = "%" + tmpValue;
						} else {
							tmpValue = "%" + tmpValue + "%";
						}
					}
					addCondition(isMultiple, queryString, metadato.getName(), operator, tmpValue);

				}
				queryString.delete(queryString.length() - 4, queryString.length());
				queryString.append(")");
			}

			// Eseguo la ricerca ottenendo gli identificativi dei documenti filtrati
			PreparedStatement ps = null;
			ResultSet rs = null;
			final String query = queryString.toString();
			LOGGER.info("Ricerca avanzata UCB per metadato: " + query);
			try {
				ps = con.prepareStatement(query);

				int index = 1;
				ps.setLong(index++, idAoo);
				ps.setString(index++, descTipologiaDocumento);
				ps.setString(index++, descTipoProcedimento);

				rs = ps.executeQuery();

				while (rs.next()) {
					output.put(rs.getInt(MED_ID_DOCUMENTO) + Constants.EMPTY_STRING, rs.getString(MED_XML_METADATI));
				}
			} catch (final Exception e) {
				throw new RedException("Errore durante la ricerca dei metadati estesi", e);
			} finally {
				closeStatement(ps, rs);
			}
		}
		return output;
	}

	private static void addCondition(final Boolean isMultiple, final StringBuilder queryString, final String name, final String operator, final String tmpValue) {
		if (!Boolean.TRUE.equals(isMultiple)) {
			queryString.append("EXTRACTVALUE(EXTRACT(xmltype(UPPER(MED_XML_METADATI)), '/METADATI/METADATO[@NAME=\"" + name.toUpperCase() + "\"]'),'/METADATO')")
					.append(operator + " " + sanitize(tmpValue.toUpperCase()) + " AND ");
		} else {
			queryString.append("EXTRACT(xmltype(UPPER(MED_XML_METADATI)), '/METADATI/METADATO[@NAME=\"" + name.toUpperCase() + "\"][@KEY=\"" + tmpValue.toUpperCase()
					+ "\"]') is not null AND ");
		} 
	}

	private static Collection<String> getValueFromMetadato(final MetadatoDTO metadato) {
		final Collection<String> out = new ArrayList<>();
		if (metadato instanceof LookupTableDTO) {
			final LookupTableDTO luDTO = (LookupTableDTO) metadato;
			if (luDTO.getLookupValueSelected() != null && luDTO.getLookupValueSelected().getValue() != null) {
				out.add(luDTO.getLookupValueSelected().getDescription());
			}
		} else if (metadato instanceof AnagraficaDipendentiComponentDTO) {
			final AnagraficaDipendentiComponentDTO adDTO = (AnagraficaDipendentiComponentDTO) metadato;
			final Collection<AnagraficaDipendenteDTO> anags = adDTO.getSelectedValues();
			for (final AnagraficaDipendenteDTO anag : anags) {
				out.add(anag.getId().toString());
			}
		} else if (metadato instanceof CapitoloSpesaMetadatoDTO) {
			final CapitoloSpesaMetadatoDTO csDTO = (CapitoloSpesaMetadatoDTO) metadato;
			if (csDTO.getCapitoloSelected() != null) {
				out.add(csDTO.getCapitoloSelected());
			}
		} else {
			out.add(metadato.getValue4AttrExt());
		}
		return out;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipologiaDocumentoDAO#cancellaMetadatiEstesi(java.sql.Connection,
	 *      java.lang.Integer, java.lang.Long).
	 */
	@Override
	public Date cancellaMetadatiEstesi(final Connection con, final Integer idDocumento, final Long idAoo) {
		PreparedStatement ps = null;
		Date dataCreazione = null;
		try {
			dataCreazione = recuperaDataCreazione(idDocumento, idAoo, con);
			
			ps = con.prepareStatement("DELETE METADATI_ESTESI_DOCUMENTO WHERE MED_ID_DOCUMENTO = ? and MED_ID_AOO = ?");

			ps.setInt(1, idDocumento);
			ps.setLong(2, idAoo);

			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante la cancellazione dei metadati estesi", e);
			throw new RedException("Errore durante la cancellazione dei metadati estesi", e);
		} finally {
			closeStatement(ps);
		}
		return dataCreazione;
	}

	private static Date recuperaDataCreazione(Integer idDocumento, Long idAoo, Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Date dataCreazione = null;
		try {
			ps = con.prepareStatement("SELECT MED_DATA_CREAZIONE_DOC FROM METADATI_ESTESI_DOCUMENTO WHERE MED_ID_DOCUMENTO = ? and MED_ID_AOO = ?");

			ps.setInt(1, idDocumento);
			ps.setLong(2, idAoo);

			rs = ps.executeQuery();
			if(rs.next()) {
				dataCreazione = rs.getDate("MED_DATA_CREAZIONE_DOC");
			}
		} catch (SQLException e) {
			LOGGER.error("Errore durante il recupero della data creazione del doc "+idDocumento, e);
			throw new RedException("Errore durante il recupero della data creazione del doc "+idDocumento, e);
		} finally {
			closeStatement(ps, rs);
		}
		return dataCreazione;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipologiaDocumentoDAO#salvaMetadatiEstesi(java.sql.Connection,
	 *      java.lang.String, java.lang.Integer, java.lang.Long, java.lang.Long,
	 *      java.lang.Long, java.util.Date).
	 */
	@Override
	public void salvaMetadatiEstesi(Connection con, String xmlMetadatiEstesi, Integer idDocumento, Long idAoo, 
			Long idTipologiaDocumento, Long idTipoProcedimento, Date dataCreazione) {
		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement("INSERT INTO METADATI_ESTESI_DOCUMENTO ( MED_ID_DOCUMENTO, MED_XML_METADATI,"
					+ " MED_ID_AOO, MED_ID_TIPOLOGIA_DOC, MED_ID_TIPO_PROC, MED_DATA_CREAZIONE_DOC ) VALUES (?, ?, ?, ?, ?, ?)");

			int index = 1;
			ps.setInt(index++, idDocumento);
			final Clob clob = con.createClob();
			clob.setString(1L, xmlMetadatiEstesi);
			ps.setClob(index++, clob);
			ps.setLong(index++, idAoo);
			ps.setLong(index++, idTipologiaDocumento);
			ps.setLong(index++, idTipoProcedimento);
			ps.setDate(index++, new java.sql.Date(dataCreazione.getTime()));

			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante la registrazione dei metadati estesi", e);
			throw new RedException("Errore durante la registrazione dei metadati estesi", e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ITipologiaDocumentoDAO#recuperaMetadatiEstesi(java.sql.Connection,
	 *      java.lang.Integer, java.lang.Long, java.lang.Long, java.lang.Long).
	 */
	@Override
	public String recuperaMetadatiEstesi(final Connection con, final Integer idDocumento, final Long idAoo, final Long idTipologiaDocumento, final Long idTipoProcedimento) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuilder sb = null;
		String result = null;

		try {
			final String querySQL = "SELECT MED_XML_METADATI FROM METADATI_ESTESI_DOCUMENTO"
					+ " WHERE MED_ID_DOCUMENTO = ? AND MED_ID_AOO = ? AND MED_ID_TIPOLOGIA_DOC = ? AND MED_ID_TIPO_PROC = ?";
			ps = con.prepareStatement(querySQL);

			int index = 1;
			ps.setInt(index++, idDocumento);
			ps.setLong(index++, idAoo);
			ps.setLong(index++, idTipologiaDocumento);
			ps.setLong(index++, idTipoProcedimento);

			rs = ps.executeQuery();
			Clob clob = null;
			sb = new StringBuilder();
			while (rs.next()) {
				clob = rs.getClob(MED_XML_METADATI);
				if (clob != null) {

					final Reader reader = clob.getCharacterStream();
					final char[] buffer = new char[(int) clob.length()];
					while (reader.read(buffer) != -1) {
						sb.append(buffer);
					}
				}
			}
		} catch (SQLException | IOException e) {
			throw new RedException("Errore nel recupero dei metadati estesi del documento " + idDocumento, e);
		} finally {
			result = sb != null ? sb.toString() : null;
			closeStatement(ps, rs);
		}

		return result;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipologiaDocumentoDAO#recuperaMetadatiEstesi(java.util.List, java.sql.Connection).
	 */
	@Override
	public Map<String, String> recuperaMetadatiEstesi(final List<String> idDocumenti, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final Map<String, String> result = new HashMap<>();

		try {
			final StringBuilder queryString = new StringBuilder().append("SELECT * FROM METADATI_ESTESI_DOCUMENTO WHERE ")
					.append(StringUtils.createInCondition(MED_ID_DOCUMENTO, idDocumenti.iterator(), false));

			ps = con.prepareStatement(queryString.toString());

			rs = ps.executeQuery();

			Clob clob = null;
			final StringBuilder sb = new StringBuilder();
			while (rs.next()) {
				clob = rs.getClob(MED_XML_METADATI);
				if (clob != null) {
					sb.setLength(0);
					final Reader reader = clob.getCharacterStream();
					final char[] buffer = new char[(int) clob.length()];
					while (reader.read(buffer) != -1) {
						sb.append(buffer);
					}

					result.put(rs.getInt(MED_ID_DOCUMENTO) + Constants.EMPTY_STRING, sb.toString());
				}
			}
		} catch (SQLException | IOException e) {
			throw new RedException("Errore nel recupero dei metadati estesi", e);
		} finally {
			closeStatement(ps, rs);
		}

		return result;
	}

	/**
	 * @param idTipoCategoria
	 * @return
	 */
	private static String getComboTipByAooAndTipoCatQuery(final Integer idTipoCategoria) {
		return "SELECT td.idtipologiadocumento, " + " LISTAGG(tn.idnodo, ',') WITHIN GROUP(ORDER BY tn.idnodo) AS nodi, td.idaoo, "
				+ " td.descrizione, td.dataattivazione, td.datadisattivazione, " + TD_IDTIPOCATEGORIA_TD_IDTIPOLOGIADOCUMENTOUSCITA
				+ TD_IDITERAPPROVATIVO_TD_FLAGVISTO_TD_CONTABILE + " td.idclassedocumentale, td.flagfirmacopiaconforme, td.ordinamento, "
				+ " cld.documentclass FROM tipologiadocumento td " + " LEFT OUTER JOIN tipologiadocumento_nodo tn ON tn.idtipologiadocumento = td.idtipologiadocumento "
				+ " INNER JOIN classedocumentale cld ON td.idclassedocumentale = cld.idclassedocumentale " + " WHERE td.idaoo = ? "
				+ ((idTipoCategoria != null) ? " AND td.idtipocategoria = ? " : "") + " GROUP BY td.idtipologiadocumento, td.idaoo, td.descrizione, "
				+ " td.dataattivazione, td.datadisattivazione, " + TD_IDTIPOCATEGORIA_TD_IDTIPOLOGIADOCUMENTOUSCITA
				+ TD_IDITERAPPROVATIVO_TD_FLAGVISTO_TD_CONTABILE + " td.idclassedocumentale, td.flagfirmacopiaconforme, " + " td.ordinamento, cld.documentclass "
				+ " ORDER BY td.descrizione, td.ordinamento ASC";
	}

	/**
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private static TipologiaDocumentoDTO populate(final ResultSet rs) throws SQLException {
		return new TipologiaDocumentoDTO(rs.getInt("idTipologiaDocumento"), rs.getString("nodi"), rs.getInt("idAoo"), rs.getString("descrizione"),
				rs.getDate("dataAttivazione"), rs.getDate("dataDisattivazione"), rs.getInt("idTipoCategoria"), rs.getInt("idTipologiaDocumentoUscita"),
				rs.getInt("idIterApprovativo"), (rs.getInt("flagVisto") == 1), rs.getInt("contabile"), rs.getInt("idClasseDocumentale"),
				(rs.getInt("flagFirmaCopiaConforme") == 1), rs.getString("ordinamento"), rs.getString("documentClass"));
	}

	/**
	 * @see it.ibm.red.business.dao.ITipologiaDocumentoDAO#getTipologiaRegistroNsd(java.sql.Connection, java.lang.Long).
	 */
	@Override
	public List<TipologiaRegistroNsdDTO> getTipologiaRegistroNsd(final Connection conn, final Long idAOO) {
		final List<TipologiaRegistroNsdDTO> tipoRegistroList = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM TIPOLOGIA_REGISTRO_NSD WHERE IDAOO=" + idAOO + ORDER_BY_POSIZIONE);
			ps = conn.prepareStatement(sb.toString());
			rs = ps.executeQuery();

			while (rs.next()) {
				final TipologiaRegistroNsdDTO tipoRegistro = new TipologiaRegistroNsdDTO(rs.getString(VALORE), rs.getString(LABEL), rs.getInt(POSIZIONE));
				tipoRegistroList.add(tipoRegistro);
			}
		} catch (final Exception ex) {
			LOGGER.error("Errore durante il recupero delle tipologie registro nsd :" + ex);
			throw new RedException("Errore durante il recupero delle tipologie registro nsd" + ex);
		} finally {
			closeStatement(ps, rs);
		}
		return tipoRegistroList;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipologiaDocumentoDAO#getTipologiaDocumentoNsd(java.lang.Long, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<TipologiaRegistroNsdDTO> getTipologiaDocumentoNsd(final Long idUfficio, final Long idUfficioPadre, final Connection conn) {
		List<TipologiaRegistroNsdDTO> tipoRegistroList = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM TIPOLOGIA_DOCUMENTO_NSD WHERE IDUFFICIO = " + idUfficio + ORDER_BY_POSIZIONE);
			ps = conn.prepareStatement(sb.toString());
			rs = ps.executeQuery();

			while (rs.next()) {
				final TipologiaRegistroNsdDTO tipoRegistro = new TipologiaRegistroNsdDTO(rs.getString(VALORE), rs.getString(LABEL), rs.getInt(POSIZIONE));
				tipoRegistroList.add(tipoRegistro);
			}

			// cerco per idufficio padre se non ne trovo per idufficio
			if (tipoRegistroList.isEmpty() && idUfficioPadre != null) {
				tipoRegistroList = new ArrayList<>();
				final StringBuilder sb1 = new StringBuilder();
				sb1.append("SELECT * FROM TIPOLOGIA_DOCUMENTO_NSD WHERE IDUFFICIOPADRE = " + idUfficioPadre + ORDER_BY_POSIZIONE);
				ps1 = conn.prepareStatement(sb1.toString());
				rs1 = ps1.executeQuery();

				while (rs1.next()) {
					final TipologiaRegistroNsdDTO tipoRegistro = new TipologiaRegistroNsdDTO(rs1.getString(VALORE), rs1.getString(LABEL), rs1.getInt(POSIZIONE));
					tipoRegistroList.add(tipoRegistro);
				}

			}
		} catch (final Exception ex) {
			LOGGER.error("Errore durante il recupero delle tipologie documento nsd :" + ex);
			throw new RedException("Errore durante il recupero delle tipologie documento nsd" + ex);
		} finally {
			closeStatement(ps, rs);
			closeStatement(ps1, rs1);
		}
		return tipoRegistroList;
	}

	/**
	 * Restituisce l'id della tipologia documento recuperato dalla descrizione:
	 * <code> nomeTipologiaDocumento </code> e dalla categoria
	 * <code> idCategoriaDocumento </code> dove la categoria indica entrata o
	 * uscita.
	 * 
	 * @see it.ibm.red.business.dao.ITipologiaDocumentoDAO#
	 *      getTipologiaDocumentoByDescrizioneAndCategoriaDocumento(
	 *      java.lang.String, java.lang.Integer, java.lang.Long,
	 *      java.sql.Connection).
	 * @param nomeTipologiaDocumento
	 *            descrizione della tipologia documento
	 * @param idCategoriaDocumento
	 *            id del tipo categoria, @see TipoCategoriaEnum.
	 * @param idAoo
	 *            identificativo AOO
	 * @return identificativo tipologia documento
	 */
	@Override
	public Integer getIdTipologiaDocumentoByDescrizioneAndTipoCategoria(final String nomeTipologiaDocumento, final Integer idCategoriaDocumento, final Long idAoo,
			final Connection connection) {
		Integer result = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = connection
					.prepareStatement("SELECT t.IDTIPOLOGIADOCUMENTO FROM TIPOLOGIADOCUMENTO t WHERE LOWER(t.DESCRIZIONE) = ? AND t.IDTIPOCATEGORIA = ? AND t.IDAOO = ?");

			int index = 1;
			ps.setString(index++, nomeTipologiaDocumento.toLowerCase());
			ps.setInt(index++, idCategoriaDocumento);
			ps.setLong(index++, idAoo);

			rs = ps.executeQuery();

			if (rs.next()) {
				result = rs.getInt("IDTIPOLOGIADOCUMENTO");
			}
		} catch (final SQLException e) {
			throw new RedException("Errore nel recupero della tipologia documento con nome: " + nomeTipologiaDocumento + " e categoria: " + idCategoriaDocumento, e);
		} finally {
			closeStatement(ps, rs);
		}

		return result;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipologiaDocumentoDAO#getDescTipologiaDocumentoById(java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public String getDescTipologiaDocumentoById(final Integer idTipologiaDocumento, final Connection connection) {
		String descrizioneTipologiaDocumento = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = connection.prepareStatement("SELECT t.DESCRIZIONE FROM TIPOLOGIADOCUMENTO t WHERE t.IDTIPOLOGIADOCUMENTO = ?");

			ps.setInt(1, idTipologiaDocumento);

			rs = ps.executeQuery();

			if (rs.next()) {
				descrizioneTipologiaDocumento = rs.getString("DESCRIZIONE");
			}
		} catch (final SQLException e) {
			throw new RedException("Errore nel recupero della tipologia documento con ID: " + idTipologiaDocumento, e);
		} finally {
			closeStatement(ps, rs);
		}

		return descrizioneTipologiaDocumento;
	}

	private static int getSequenceValueOfTipologiaDocumento(final Connection connection) {
		final String queryGetId = "SELECT SEQ_TIPOLOGIADOCUMENTO.NEXTVAL FROM DUAL";
		PreparedStatement ps = null;
		ResultSet rs = null;
		int idTipo = 0;
		try {
			ps = connection.prepareStatement(queryGetId);
			rs = ps.executeQuery();
			if (rs.next()) {
				idTipo = rs.getInt(1);
			}
		} catch (final Exception e1) {
			throw new RedException("Errore durante la creazione dell'id della tipologia di documento", e1);
		} finally {
			closeStatement(ps, rs);
		}

		return idTipo;
	}

	private static String getClasseDocumentalePrefix(final Connection connection, final Long idAoo) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String classPrefix = Constants.EMPTY_STRING;
		try {
			ps = connection.prepareStatement("SELECT CODICEAOO FROM AOO WHERE IDAOO = ?");
			ps.setLong(1, idAoo);
			rs = ps.executeQuery();
			if (rs.next()) {
				classPrefix = rs.getString("CODICEAOO");
			}
		} catch (final Exception docClassExc) {
			throw new RedException("Errore durante il recupero della classe documentale della tipologia di documento", docClassExc);
		} finally {
			closeStatement(ps, rs);
		}
		return classPrefix;
	}

	private static int getClasseDocumentale(final Connection connection, final String classPrefix) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int idClasseDocumentale = 1;
		int index = 1;
		try {
			final String query = "SELECT MAX(IDCLASSEDOCUMENTALE) AS IDCLASSEDOCUMENTALE FROM CLASSEDOCUMENTALE WHERE DOCUMENTCLASS IN (?, ?)";
			ps = connection.prepareStatement(query);
			ps.setString(index++, PropertiesProvider.getIstance().getParameterByKey((PropertiesNameEnum.DOCUMENTO_GENERICO_CLASSNAME_FN_METAKEY))); // NSD_DocumentoGenerico
			ps.setString(index++, classPrefix + "_DocumentoGenerico");

			rs = ps.executeQuery();
			if (rs.next()) {
				idClasseDocumentale = rs.getInt("IDCLASSEDOCUMENTALE");
			}
		} catch (final Exception idClassExc) {
			throw new RedException("Errore durante il recupero dell'id della classe documentale", idClassExc);
		} finally {
			closeStatement(ps, rs);
		}
		return idClasseDocumentale;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipologiaDocumentoDAO#ricercaEstrattiContoUcb(java.lang.Integer,
	 *      java.lang.Integer, java.lang.Integer,
	 *      it.ibm.red.business.dto.ParamsRicercaEstrattiContoDTO,
	 *      java.sql.Connection).
	 */
	@Override
	public Collection<EstrattiContoUcbDTO> ricercaEstrattiContoUcb(Integer idAoo, Integer tipoDoc, Integer tipoProc, ParamsRicercaEstrattiContoDTO estrattiContoUCB, Connection con) {
		Collection<EstrattiContoUcbDTO> report = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		final StringBuilder query = new StringBuilder();
		
		Integer esercizio = estrattiContoUCB.getEsercizio();
		Integer trimestre = estrattiContoUCB.getTrimestre();
		String sedeEstera = estrattiContoUCB.getSedeEstera();
		
		query.append("SELECT MED_XML_METADATI, MED_DATA_CREAZIONE_DOC FROM METADATI_ESTESI_DOCUMENTO WHERE MED_ID_AOO = ?")
			.append(" AND MED_ID_TIPOLOGIA_DOC = ? AND MED_ID_TIPO_PROC = ?");
		
		query.append(AND_PAR_DX_AP);
		final String uguale = "=";
		addCondition(false, query, EstrattiContoCCVTEnum.ESERCIZIO.getMetadatoName(), uguale, esercizio.toString());
		
		if(trimestre != null) {
			addCondition(false, query, EstrattiContoCCVTEnum.TRIMESTRE.getMetadatoName(), uguale, trimestre.toString());
		}
		if(!StringUtils.isNullOrEmpty(sedeEstera)) {
			addCondition(false, query, EstrattiContoCCVTEnum.DESCRIZIONE_SEDE_ESTERA.getMetadatoName(), uguale, sedeEstera);
		}
		query.delete(query.length() - 4, query.length());
		query.append(")");
		
		int index = 1;

		try {

			ps = con.prepareStatement(query.toString());
			ps.setInt(index++, idAoo);
			ps.setInt(index++, tipoDoc);
			ps.setInt(index++, tipoProc);
	
			rs = ps.executeQuery();
			
			while (rs.next()) {
				report.add(buildReport(MetadatiEstesiHelper.deserializeMetadatiEstesiPerRisultatiRicerca(
						rs.getString(MED_XML_METADATI)), rs.getDate("MED_DATA_CREAZIONE_DOC"), con));	
			}
		} catch (Exception e) {
			throw new RedException("Errore durante la ricerca degli estratti conto trimestrali CCVT", e);
		} finally {
			closeStatement(ps, rs);
		}
		return report;
	}

	private EstrattiContoUcbDTO buildReport(Map<String, String> metadatiMap, Date dataCreazione, Connection con) {
		EstrattiContoUcbDTO output = null;
		if(MapUtils.isNotEmpty(metadatiMap)) {
			
			Integer trimestre = null;
			Double val = stringToDouble(metadatiMap.get(EstrattiContoCCVTEnum.TRIMESTRE.getMetadatoName()));
			if(val != null) {
				trimestre = val.intValue();
			}
			Double saldoCCVT1 = stringToDouble(metadatiMap.get(EstrattiContoCCVTEnum.SALDO_CCVT_1T.getMetadatoName()));
			Double saldoCCVT2 = stringToDouble(metadatiMap.get(EstrattiContoCCVTEnum.SALDO_CCVT_2T.getMetadatoName()));
			Double saldoCCVT3 = stringToDouble(metadatiMap.get(EstrattiContoCCVTEnum.SALDO_CCVT_3T.getMetadatoName()));
			Double saldoCCVT4 = stringToDouble(metadatiMap.get(EstrattiContoCCVTEnum.SALDO_CCVT_4T.getMetadatoName()));
			String codiceValuta = metadatiMap.get(EstrattiContoCCVTEnum.VALUTA.getMetadatoName());
			Double cambio = valutaDAO.getMetadatiConversione(codiceValuta, dataCreazione, con).get(codiceValuta);
			
			output = new EstrattiContoUcbDTO(Integer.valueOf(metadatiMap.get(EstrattiContoCCVTEnum.ESERCIZIO.getMetadatoName())), trimestre, 
											metadatiMap.get(EstrattiContoCCVTEnum.CODICE_SEDE_ESTERA.getMetadatoName()), 
											metadatiMap.get(EstrattiContoCCVTEnum.DESCRIZIONE_SEDE_ESTERA.getMetadatoName()), 
											codiceValuta, cambio, saldoCCVT1, saldoCCVT2, saldoCCVT3, saldoCCVT4);
		}
		return output;
	} 
	
	/**
	 * Evita NullPointer/NumberFormat Exception
	 * in caso di stringa nulla o vuota.
	 * @param string
	 * @return n - valore Double di 'string'
	 */
	private static Double stringToDouble(String string) {
		Double n = null;
		if(!StringUtils.isNullOrEmpty(string)) {
			n = Double.valueOf(string);
		}
		return n;
	}
	
	/**
	 * Esegue la ricerca per metadati estesi dei conti consuntivi con mezzo creazione manuale.
	 * @param idAoo
	 * @param tipoDoc id tipologia conti consuntivi
	 * @param tipoProc id procedimento conti consuntivi
	 * @param contiConsuntiviUCB parametri della ricerca
	 * @param documentiList
	 * @param notificheList
	 * @param documentiNotificheMap
	 * @param con
	 * @return una map contenente i risultati della ricerca
	 */
	@Override
	public Map<Integer, RisultatoRicercaContiConsuntiviDTO> ricercaMetadatiContiConsuntivi (final Integer idAoo, final Integer tipoDoc, final Integer tipoProc, final ParamsRicercaContiConsuntiviDTO contiConsuntiviUCB,
			final List<Integer> documentiList, final List<Integer> notificheList, final Map<Integer, Integer> documentiNotificheMap, final Connection con) {
		final StringBuilder query = new StringBuilder();
		final Integer esercizio = contiConsuntiviUCB.getEsercizio();
		final String sedeEstera = contiConsuntiviUCB.getSedeEstera();
		final Map<Integer, RisultatoRicercaContiConsuntiviDTO> metadatiReportMap = new HashMap<>();
		
		if(tipoDoc!=null) {
			query.append("SELECT '" + MEZZOCREAZIONE_MANUALE + "' AS MEZZOCREAZIONE, med.MED_ID_DOCUMENTO AS ID_DOCUMENTO, med.MED_XML_METADATI AS XML_METADATI,")
				.append(" (SELECT a.IDDOCUMENTOALLACCIATO FROM ALLACCIO a WHERE med.MED_ID_DOCUMENTO = a.IDDOCUMENTO(+) AND a.IDTIPOALLACCIO = " + TipoAllaccioEnum.NOTIFICA.getTipoAllaccioId() + ") AS IDDOCUMENTOALLACCIATO")
				.append(FROM_METADATI_ESTESI_DOCUMENTO_MED)
				.append(WHERE_MED_MED_ID_AOO_AND_MED_MED_ID_TIPOLOGIA_DOC_AND_MED_MED_ID_TIPO_PROC);
			query.append(AND_PAR_DX_AP);
			final String uguale = "=";
			
			addCondition(false, query, ContiConsuntiviEnum.ESERCIZIO.getMetadatoName(), uguale, esercizio.toString());
			
			if(!StringUtils.isNullOrEmpty(sedeEstera)) {
				addCondition(false, query, ContiConsuntiviEnum.CODICE_SEDE_ESTERA.getMetadatoName(), uguale, sedeEstera);
			}
			
			query.delete(query.length() - 4, query.length());
			query.append(")");
			PreparedStatement ps = null;
			ResultSet rs = null;
			
			int index = 1;

			try {

				ps = con.prepareStatement(query.toString());
				ps.setInt(index++, idAoo);
				ps.setInt(index++, tipoDoc);
				ps.setInt(index++, tipoProc);
				
				rs = ps.executeQuery();
				
				while (rs.next()) {
					metadatiReportMap.put(rs.getInt(ID_DOCUMENTO), buildMetadatiReport(rs.getString("MEZZOCREAZIONE"),
							MetadatiEstesiHelper.deserializeMetadatiEstesiPerRisultatiRicerca(rs.getString("XML_METADATI"))));
					
					documentiList.add(rs.getInt(ID_DOCUMENTO));
					
					if (rs.getInt(IDDOCUMENTOALLACCIATO) != 0) {
						notificheList.add(rs.getInt(IDDOCUMENTOALLACCIATO));
						documentiNotificheMap.put(rs.getInt(ID_DOCUMENTO), rs.getInt(IDDOCUMENTOALLACCIATO));
					}
				}
			} catch (Exception e) {
				throw new RedException("Errore durante la ricerca dei metadati per elenco notifiche e conti consuntivi.", e);
			} finally {
				closeStatement(ps, rs);
			}
		}
		
		return metadatiReportMap;
	}
	
	/**
	 * Esegue la ricerca dei conti consuntivi sede estera da flusso.
	 * @param idAoo
	 * @param tipoDocFlusso
	 * @param tipoProcFlusso
	 * @param contiConsuntiviUCB
	 * @param documentiList
	 * @param notificheList
	 * @param documentiNotificheMap
	 * @param con
	 * @return una map contenente i risultati della ricerca
	 */
	@Override
	public Map<Integer, RisultatoRicercaContiConsuntiviDTO> ricercaMetadatiContiConsuntiviFlusso (final Integer idAoo, final Integer tipoDocFlusso, final Integer tipoProcFlusso, final ParamsRicercaContiConsuntiviDTO contiConsuntiviUCB,
			final List<Integer> documentiList, final List<Integer> notificheList, final Map<Integer, Integer> documentiNotificheMap, final Connection con) {
		
		final StringBuilder query = new StringBuilder();
		final Integer esercizio = contiConsuntiviUCB.getEsercizio();
		final String sedeEstera = contiConsuntiviUCB.getSedeEstera();
		final Map<Integer, RisultatoRicercaContiConsuntiviDTO> metadatiReportMap = new HashMap<>();

		if(tipoDocFlusso != null) {
			
			query.append("SELECT '" + MEZZOCREAZIONE_FLUSSO + "' AS MEZZOCREAZIONE, med.MED_ID_DOCUMENTO AS ID_DOCUMENTO, med.MED_XML_METADATI AS XML_METADATI,")
			.append(" (SELECT a.IDDOCUMENTOALLACCIATO FROM ALLACCIO a WHERE med.MED_ID_DOCUMENTO = a.IDDOCUMENTO(+) AND a.IDTIPOALLACCIO = " + TipoAllaccioEnum.NOTIFICA.getTipoAllaccioId() + ") AS IDDOCUMENTOALLACCIATO")
			.append(FROM_METADATI_ESTESI_DOCUMENTO_MED)
			.append(WHERE_MED_MED_ID_AOO_AND_MED_MED_ID_TIPOLOGIA_DOC_AND_MED_MED_ID_TIPO_PROC);
			query.append(AND_PAR_DX_AP);
			
			addCondition(false, query, ContiConsuntiviEnum.ESERCIZIO.getMetadatoName(), "=", esercizio.toString());
			
			if(!StringUtils.isNullOrEmpty(sedeEstera)) {
				addCondition(false, query, ContiConsuntiviEnum.ANAG_SEDI.getMetadatoName(), "like", sedeEstera+"%");
			}
			
			query.delete(query.length() - 4, query.length());
			query.append(")");
			
			PreparedStatement ps = null;
			ResultSet rs = null;
			
			try {
				ps = con.prepareStatement(query.toString());
				
				int index = 1;
				ps.setInt(index++, idAoo);
				ps.setInt(index++, tipoDocFlusso);
				ps.setInt(index++, tipoProcFlusso);
				
				rs = ps.executeQuery();
				
				while (rs.next()) {
					metadatiReportMap.put(rs.getInt(ID_DOCUMENTO), buildMetadatiReport(rs.getString("MEZZOCREAZIONE"), MetadatiEstesiHelper.deserializeMetadatiEstesiPerRisultatiRicerca(rs.getString("XML_METADATI"))));
					documentiList.add(rs.getInt(ID_DOCUMENTO));
					if (rs.getInt(IDDOCUMENTOALLACCIATO) != 0) {
						notificheList.add(rs.getInt(IDDOCUMENTOALLACCIATO));
						documentiNotificheMap.put(rs.getInt(ID_DOCUMENTO), rs.getInt(IDDOCUMENTOALLACCIATO));
					}
				}
			} catch (Exception e) {
				throw new RedException("Errore durante la ricerca dei metadati per elenco notifiche e conti consuntivi da flusso.", e);
			} finally {
				closeStatement(ps, rs);
			}
		}
		
		return metadatiReportMap;
		
	}
	
	
	/**
	 * Popola i metadati del report elenco notifiche e conti consuntivi per il singolo risultato.
	 * 
	 * @param metadatiMap
	 * @return risultato
	 * @throws ParseException
	 */
	private RisultatoRicercaContiConsuntiviDTO buildMetadatiReport(final String mezzoCreazione, final Map<String, String> metadatiMap) throws ParseException {
		final RisultatoRicercaContiConsuntiviDTO output = new RisultatoRicercaContiConsuntiviDTO();
		
		if (MapUtils.isNotEmpty(metadatiMap)) {
			
			String codiceSedeEstera = metadatiMap.get(ContiConsuntiviEnum.CODICE_SEDE_ESTERA.getMetadatoName());
			String descrizioneSedeEstera = metadatiMap.get(ContiConsuntiviEnum.DESCRIZIONE_SEDE_ESTERA.getMetadatoName());
			if(MEZZOCREAZIONE_FLUSSO.equals(mezzoCreazione)) {
				final String value = metadatiMap.get(ContiConsuntiviEnum.ANAG_SEDI.getMetadatoName());
				// Se il metadato ANAGSEDEST è valorizzato, le informazioni vengono suddivise in due campi differenti
				if(!StringUtils.isNullOrEmpty(value)) {
					final String[] values = value.split(" - ");
					codiceSedeEstera = values[0].replace("[", "").replace("]", "");
					descrizioneSedeEstera = values.length > 1 ? values[1] : "";
				}
			}
			
			final Integer esercizio = Integer.valueOf(metadatiMap.get(ContiConsuntiviEnum.ESERCIZIO.getMetadatoName()));
			final String programmaControlloUCB = metadatiMap.get(ContiConsuntiviEnum.PROGRAMMA_CONTROLLO_UCB.getMetadatoName());
			Date dataChiusura = null;
			if (!"".equals(metadatiMap.get(ContiConsuntiviEnum.DATA_CHIUSURA.getMetadatoName()))) {
				dataChiusura = new SimpleDateFormat("dd/MM/yyyy").parse(metadatiMap.get(ContiConsuntiviEnum.DATA_CHIUSURA.getMetadatoName()));
			}
			
			output.setCodiceSedEstera(codiceSedeEstera);
			output.setDescrizioneSedeEstera(descrizioneSedeEstera);
			output.setEsercizio(esercizio);
			if (!"".equals(programmaControlloUCB)) {
				output.setControlloUcb(programmaControlloUCB);
			}
			if (dataChiusura != null) {
				output.setDataChiusura(dataChiusura);
			}
		}
		
		return output;
	}
	
	/**
	 * Ricerca i dati dei documenti.
	 * 
	 * @param idAoo
	 * @param documentiList
	 * @param dwhCon
	 * @return mappa id documento dati del documento
	 */
	@Override
	public Map<Integer, DatiDocumentoContiConsuntiviDTO> ricercaDatiDocumenti(final Integer idAoo, final List<Integer> documentiList, final Connection dwhCon) {
		final Map<Integer, DatiDocumentoContiConsuntiviDTO> datiDocumentiMap = new HashMap<>();
		
		String query = Constants.EMPTY_STRING;
		query = getQuery(ReportQueryEnum.SQL_REPORT_ELENCO_NOTIFICHE_CONTI_CONSUNTIVI);
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			StringBuilder sb = new StringBuilder("");
			for (int i = 0; i < documentiList.size(); i++) {
				sb.append("?");
				if (i < documentiList.size() - 1) {
					sb.append(", ");
				}
			}
			query = query.replace("#IDDOCUMENTO", sb.toString());
			
			ps = dwhCon.prepareStatement(query);
	
			int index = 1;
			ps.setInt(index++, idAoo);
			ps.setInt(index++, idAoo);
			
			for (int i = 0; i < documentiList.size(); i++) {
				ps.setInt(index++, documentiList.get(i));
			}
			
			ps.setInt(index++, idAoo);
			ps.setInt(index++, idAoo);
			ps.setInt(index++, idAoo);
	
			rs = ps.executeQuery();
			
			while (rs.next()) {
				final DatiDocumentoContiConsuntiviDTO datiDocumento = new DatiDocumentoContiConsuntiviDTO();
				datiDocumento.setDataProtocollo(rs.getDate("dataProtocollo"));
				datiDocumento.setNumeroProtocollo(rs.getInt("numeroProtocollo"));
				datiDocumento.setUtenteAssegnatario(rs.getString("utenteAssegnatario"));
				datiDocumento.setUfficioAssegnatario(rs.getString("ufficioAssegnatario"));
				datiDocumento.setStato(rs.getString("stato"));
				datiDocumentiMap.put(rs.getInt("documentTitle"), datiDocumento);
			}
		} catch (Exception e) {
			throw new RedException("Errore durante la ricerca dei dati dei documenti", e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return datiDocumentiMap;
	}
	
	/**
	 * Restituisce la query da eseguire.
	 * 
	 * @param rqe
	 * @return query
	 */
	private static String getQuery(final ReportQueryEnum rqe) {
		return PropertiesProvider.getIstance().getReportQueries(rqe);
	}

	/**
	 * Ricerca i dati delle notifiche.
	 * 
	 * @param idAoo
	 * @param notificheList
	 * @param dwhCon
	 * @return mappa id notifica dati della notifica
	 */
	@Override
	public Map<Integer, DatiNotificaContiConsuntiviDTO> ricercaDatiNotifiche(final Integer idAoo, final List<Integer> notificheList, final Connection dwhCon) {
		final Map<Integer, DatiNotificaContiConsuntiviDTO> datiNotificheMap = new HashMap<>();
		
		final StringBuilder query = new StringBuilder();
		
		query.append("SELECT dv.numeroprotocollo AS numeroProtocollo, dv.dataprotocollo AS dataProtocollo, dv.documenttitle AS documentTitle")
			.append(" FROM v_docversion dv")
			.append(" WHERE dv.idaoo = ? AND dv.is_current = 1 AND dv.tipoprotocollo = 1 AND dv.documenttitle in (#IDNOTIFICA)");
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			String queryString = query.toString();
			StringBuilder sb = new StringBuilder("");
			for (int i = 0; i < notificheList.size(); i++) {
				sb.append("?");
				if (i < notificheList.size() - 1) {
					sb.append(", ");
				}
			}
			queryString = queryString.replace("#IDNOTIFICA", sb.toString());
			
			ps = dwhCon.prepareStatement(queryString);
	
			int index = 1;
			ps.setInt(index++, idAoo);
			for (int i = 0; i < notificheList.size(); i++) {
				ps.setInt(index++, notificheList.get(i));
			}
	
			rs = ps.executeQuery();
			
			while (rs.next()) {
				final DatiNotificaContiConsuntiviDTO datiNotifica = new DatiNotificaContiConsuntiviDTO();
				datiNotifica.setDataProtocollo(rs.getDate("dataProtocollo"));
				datiNotifica.setNumeroProtocollo(rs.getInt("numeroProtocollo"));
				datiNotificheMap.put(rs.getInt("documentTitle"), datiNotifica);
			}
		} catch (Exception e) {
			throw new RedException("Errore durante la ricerca dei dati delle notifiche", e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return datiNotificheMap;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipologiaDocumentoDAO#ricercaMetadatiContiGiudizialiUcb(java.lang.Integer,
	 *      java.lang.Integer, java.lang.Integer, java.lang.Integer,
	 *      java.lang.Integer, java.lang.String, java.lang.String, java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public List<String> ricercaMetadatiContiGiudizialiUcb(final Integer idAoo, final Integer tipoDoc, final Integer tipoProc,
			final Integer tipoDocFlusso, final Integer tipoProcFlusso, final String tipoCont, final String ageCont, final String esercizio,
			final Connection con) {
		
		final List<String> idDocumenti = new ArrayList<>();
		
		final StringBuilder query = new StringBuilder();
				
		boolean union = false;
		if(tipoDoc!=null) {
			query.append("SELECT med.MED_ID_DOCUMENTO AS ID_DOCUMENTO ")
				.append(FROM_METADATI_ESTESI_DOCUMENTO_MED)
				.append(WHERE_MED_MED_ID_AOO_AND_MED_MED_ID_TIPOLOGIA_DOC_AND_MED_MED_ID_TIPO_PROC);
			query.append(AND_PAR_DX_AP);
			final String uguale = "=";
			
			if(!StringUtils.isNullOrEmpty(tipoCont)) {
				addCondition(false, query, ContoGiudizialeEnum.TIPO_CONT.getMetadatoName(), uguale, tipoCont);
			}
			
			if(!StringUtils.isNullOrEmpty(ageCont)) {
				addCondition(false, query, ContoGiudizialeEnum.AGECONT.getMetadatoName(), uguale, ageCont);
			}
			
			if(!StringUtils.isNullOrEmpty(esercizio)) {
				addCondition(false, query, ContoGiudizialeEnum.ESERCIZIO.getMetadatoName(), uguale, esercizio);
			}
			
			query.delete(query.length() - 4, query.length());
			query.append(")");
			
			union = true;
		}
		
		if(tipoDocFlusso!=null) {
			
			if(union) {
				query.append(" UNION ");
			}
			
			query.append("SELECT med.MED_ID_DOCUMENTO AS ID_DOCUMENTO ")
				.append(FROM_METADATI_ESTESI_DOCUMENTO_MED)
				.append(WHERE_MED_MED_ID_AOO_AND_MED_MED_ID_TIPOLOGIA_DOC_AND_MED_MED_ID_TIPO_PROC);
			query.append(AND_PAR_DX_AP);
			final String uguale = "=";
			
			if(!StringUtils.isNullOrEmpty(tipoCont)) {
				addCondition(false, query, ContoGiudizialeEnum.TIPO_CONT.getMetadatoName(), uguale, tipoCont);
			}
			
			if(!StringUtils.isNullOrEmpty(ageCont)) {
				addCondition(false, query, ContoGiudizialeEnum.AGE_CONT.getMetadatoName(), uguale, ageCont);
			}
			
			if(!StringUtils.isNullOrEmpty(esercizio)) {
				addCondition(false, query, ContoGiudizialeEnum.ESERCIZIO.getMetadatoName(), uguale, esercizio);
			}
			
			query.delete(query.length() - 4, query.length());
			query.append(")");
			
		}
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = con.prepareStatement(query.toString());
	
			int index = 1;
			ps.setInt(index++, idAoo);
			
			if(tipoDoc!=null) {
				ps.setInt(index++, tipoDoc);
				ps.setInt(index++, tipoProc);
			}
			
			if(tipoDocFlusso!=null) {
				ps.setInt(index++, idAoo);
				ps.setInt(index++, tipoDocFlusso);
				ps.setInt(index++, tipoProcFlusso);
			}
			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				idDocumenti.add(rs.getString(ID_DOCUMENTO));
			}
		} catch (Exception e) {
			throw new RedException("Errore durante la ricerca dei metadati per elenco notifiche e conti consuntivi", e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return idDocumenti;
		
	}

	@Override
	public List<KeyValueDTO> getTipologieDocumentoNPS(Long idAoo, Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<KeyValueDTO> result = new ArrayList<>();

		try {
			final StringBuilder querySQL = new StringBuilder();
			querySQL.append("select * from ( "); 
			querySQL.append("select distinct n.npt_tipologia, td.descrizione || ' - '|| n.npt_tipologia "); 
			querySQL.append("  from nps_tipologia n, tipologiadocumento td ");
			querySQL.append(" where n.npt_id_aoo = ? ");
			querySQL.append("   and n.npt_id_tipo_doc = td.idtipologiadocumento ");
			querySQL.append("union ");
			querySQL.append("select distinct td.descrizione, td.descrizione || ' - '|| td.descrizione  ");
			querySQL.append("  from tipologiadocumento td, tipologiadocumentoprocedimento tdtp, tipoprocedimento tp ");
			querySQL.append(" where td.idaoo = ? ");
			querySQL.append("   and td.idtipologiadocumento = tdtp.idtipologiadocumento ");
			querySQL.append("   and tdtp.idtipoprocedimento = tp.idtipoprocedimento ");
			querySQL.append("   and not exists (select 1 from nps_tipologia n  ");
			querySQL.append("   where n.npt_id_aoo = td.idaoo ");
			querySQL.append("   and n.npt_id_tipo_doc = td.idtipologiadocumento  ");
			querySQL.append("   and n.npt_id_tipo_proc = tp.idtipoprocedimento ) ");
			querySQL.append(") t ");
			querySQL.append("order by 2 ");
			
			int index = 1;
			ps = con.prepareStatement(querySQL.toString());
			ps.setLong(index++, idAoo);
			ps.setLong(index++, idAoo);
			
			rs = ps.executeQuery();

			while (rs.next()) {
				result.add(new KeyValueDTO(rs.getString(1), rs.getString(2)));
			}

		} catch (final SQLException e) {
			throw new RedException("Errore nel recupero delle tipologie documento note ad NPS: idAoo=" + idAoo, e);
		} finally {
			closeStatement(ps, rs);
		}

		return result;
	}
}