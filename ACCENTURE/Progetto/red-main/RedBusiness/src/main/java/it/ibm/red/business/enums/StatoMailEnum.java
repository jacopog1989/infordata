package it.ibm.red.business.enums;

/**
 * Enum stati mail.
 * 
 * @author CRISTIANOPierascenzi
 *
 */
public enum StatoMailEnum {

	/**
	 * Archiviata definitivamente.
	 */
	ARCHIVIATA_DEFINITIVAMENTE(0, "Archiviata definitivamente"),

	/**
	 * In arrivo.
	 */
	INARRIVO(1, "In arrivo"),

	/**
	 * Eliminata.
	 */
	ELIMINATA(2, "Eliminata"),

	/**
	 * Importata.
	 */
	IMPORTATA(3, "Importata"),

	/**
	 * Notificata.
	 */
	NOTIFICATA(4, "Notificata"),

	/**
	 * Inoltrata.
	 */
	INOLTRATA(5, "Inoltrata"),

	/**
	 * Rifiutata.
	 */
	RIFIUTATA(6, "Rifiutata"),

	/**
	 * Archiviata.
	 */
	ARCHIVIATA(7, "Archiviata"),

	/**
	 * In fase di protocollazione manuale.
	 */
	IN_FASE_DI_PROTOCOLLAZIONE_MANUALE(9, "In fase di protocollazione manuale"),

	/**
	 * In fase di protocollazione automatica.
	 */
	IN_FASE_DI_PROTOCOLLAZIONE_AUTOMATICA(10, "In fase di protocollazione automatica"),

	/**
	 * Errore di protocollazione.
	 */
	ERRORE_DI_PROTOCOLLAZIONE(11, "Errore di protocollazione"),

	/**
	 * Protocollata.
	 */
	PROTOCOLLATA(12, "Protocollata"),

	/**
	 * In arrivo non protocollabile automaticamente.
	 */
	INARRIVO_NON_PROTOCOLLABILE_AUTOMATICAMENTE(13, "In arrivo, non protocollabile automaticamente"),

	/**
	 * Errore di post protocollazione.
	 */
	ERRORE_DI_POST_PROTOCOLLAZIONE(14, "Errore di post protocollazione"),
	
	/**
	 * Mail in uscita.
	 */
	INUSCITA(20, "In uscita"),
	
	/**
	 * Rifiutata automaticamente.
	 */
	RIFIUTATA_AUTOMATICAMENTE(21, "Rifiutata automaticamente"),
	
	/**
	 * Protocollata automaticamente.
	 */
	PROTOCOLLATA_AUTOMATICAMENTE(22, "Protocollata automaticamente");

	/**
	 * Stato.
	 */
	private Integer status;
	
	/**
	 * Descrizione.
	 */
	private String description;

	/**
	 * Costruttore.
	 * 
	 * @param inStatus		stato
	 * @param inDescription	descrizione
	 */
	StatoMailEnum(final Integer inStatus, final String inDescription) {
		status = inStatus;
		description = inDescription;
	}

	/**
	 * Getter.
	 * 
	 * @return	descrizione
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Getter.
	 * 
	 * @return	stato
	 */
	public Integer getStatus() {
		return status;
	}
	
	/**
	 * Restituisce lo status della mail come string.
	 * @return status mail come String
	 */
	public String getStatusString() {
		return status.toString();
	}

	/**
	 * Metodo per il recupero di un enum a partire dal valore associato.
	 * 
	 * @param value	valore
	 * @return		enum
	 */
	public static StatoMailEnum get(final Integer value) {
		StatoMailEnum output = null;
		for (StatoMailEnum sme:StatoMailEnum.values()) {
			if (sme.getStatus().equals(value)) {
				output = sme;
			}
		}
		return output;
	}
	
}
