package it.ibm.red.business.service.concrete;

import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import it.ibm.red.business.dto.ParamsRicercaElencoDivisionaleDTO;
import it.ibm.red.business.dto.RisultatoRicercaElencoDivisionaleDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.fop.FOPHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.IElencoDivisionaleFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.FileUtils;

/**
 * Service per la gestione della creazione dei report per l'elenco divisionale.
 * 
 * @author SimoneLungarella
 */
@Service
public class ElencoDivisionaleSRV extends XmlAbstractSRV implements IElencoDivisionaleFacadeSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -7072704959115825393L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ElencoDivisionaleSRV.class);
	
	/**
	 * Genera il pdf del report a partire dai parametri della ricerca e dei
	 * risultati.
	 * 
	 * @param paramsRicerca
	 *            parametri della ricerca
	 * @param results
	 *            elenco dei risultati della ricerca
	 * @return pdf generato
	 */
	@Override
	public byte[] generaPdfReport(ParamsRicercaElencoDivisionaleDTO paramsRicerca, List<RisultatoRicercaElencoDivisionaleDTO> results, String descrizioneAoo) {
		byte[] pdfFile = null;
		
		// Recupero dell'xslt specifico per la generazione del report
		byte[] xslt = FileUtils.getFileFromInternalResources("report/elencoDivisionale.xslt");
		try {
			// Trasformazione in pdf
			pdfFile = FOPHelper.transformToPdf(getCompleteXml(paramsRicerca, results, descrizioneAoo).getBytes(StandardCharsets.UTF_8), xslt);
		} catch (Exception ex) {
			LOGGER.error("Errore riscontrato durante la generazione del report dell'elenco divisionale.", ex);
			throw new RedException("Errore riscontrato durante la generazione del report dell'elenco divisionale.", ex);
		}
		return pdfFile;
	}

	/**
	 * Restituisce l'xml completo per la valorizzazione dei placeholders dell'xslt
	 * per la generazione del file pdf associato al report degli elenchi
	 * divisionali.
	 * 
	 * @param paramsRicerca
	 *            parametri della ricerca
	 * @param results
	 *            risultati della ricerca
	 * @param descrizioneAoo
	 *            descrizone dell'area organizzativa dell'utente in sessione
	 * @return xml completo
	 */
	private String getCompleteXml(ParamsRicercaElencoDivisionaleDTO paramsRicerca, List<RisultatoRicercaElencoDivisionaleDTO> results, String descrizioneAoo) {
		Document document = prepareParameters();
		addToDocument("desc_aoo", descrizioneAoo, document);
		addToDocument("giorno_stampa", new Date(), document);
		addToDocument("orario_stampa", new SimpleDateFormat(DateUtils.HH24_MM_SS).format(new Date()), document);
		addToDocument("numero_risultati", results.size(), document);
		addToDocument("timestamp_footer", new SimpleDateFormat(DateUtils.DD_MM_YYYY_HH_MM_SS).format(new Date()), document);
		
		createXmlFromParams(paramsRicerca, document);
		createXmlFromResults(results, document);
		return getXmlFromDocument(document);
	}

	/**
	 * Restituisce l'xml generato dai risultati della ricerca per il completamento
	 * del report degli elenchi divisionali.
	 * 
	 * @param results
	 *            risultat della ricerca
	 * @param document
	 *            documento xml da popolare
	 */
	private void createXmlFromResults(List<RisultatoRicercaElencoDivisionaleDTO> results, Document document) {
		Element risultati = document.createElement("risultati");
		document.getLastChild().appendChild(risultati);
		for (RisultatoRicercaElencoDivisionaleDTO risultato : results ) {
			Element elementoRisultato = document.createElement("risultato");
			addToElement("numero_data", risultato.getNumeroElenco() + "/" + risultato.getAnnoElenco(), document, elementoRisultato);
			addToElement("data_protocollo", risultato.getDataProtocollo(), document, elementoRisultato);
			addToElement("mittente", risultato.getDescrizioneMittente(), document, elementoRisultato);
			addToElement("classificazione", risultato.getDescrizioneTitolario(), document, elementoRisultato);
			addToElement("oggetto", risultato.getOggetto(), document, elementoRisultato);
			addToElement("tipologia_documento", risultato.getTipologiaDocumento(), document, elementoRisultato);
			addToElement("utente_assegnatario", risultato.getUtenteAssegnatario(), document, elementoRisultato);
			risultati.appendChild(elementoRisultato);
		}
	}

	/**
	 * Restituisce l'xml generato dai parametri di ricerca per il completamento del
	 * report degli elenchi divisionali.
	 * 
	 * @param paramsRicerca
	 *            parametri della ricerca
	 * @param document
	 *            documento da popolare con i parametri
	 */
	private void createXmlFromParams(ParamsRicercaElencoDivisionaleDTO paramsRicerca, Document document) {
		addToDocument("data_assegnazione", paramsRicerca.getDataAssegnazione(), document);
		addToDocument("tipologia_documento", paramsRicerca.getTipologiaDocumento(), document);
		addToDocument("ufficio_assegnante", paramsRicerca.getUfficioAssegnante().getDescrizione(), document);
		addToDocument("ufficio_assegnatario", paramsRicerca.getUfficioAssegnatario().getDescrizione(), document);
		
		StringBuilder xml = new StringBuilder("");
		if(!CollectionUtils.isEmpty(paramsRicerca.getUtentiAssegnatari())) {
			for (UtenteDTO utente : paramsRicerca.getUtentiAssegnatari()) {
				xml.append(utente.getNome()).append(" ").append(utente.getCognome()).append(", ");
			}
			xml = new StringBuilder(xml.substring(0, xml.length()-2));
		}
		
		addToDocument("utente_assegnatario", xml.toString(), document);
	}
}
