package it.ibm.red.business.dto;

import java.util.Date;

import fepa.types.v3.FascicoloBaseType;
import it.ibm.red.business.utils.DateUtils;

/**
 * DTO che definisce le informazioni su un Fascicolo FEPA.
 */
public class FascicoloFEPADTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -475496098147595220L;

	/**
	 * Data creazione.
	 */
	private Date dataCreazione;

	/**
	 * Data aggiornamento.
	 */
	private Date dataAggiornamento;

	/**
	 * Sistema produttore.
	 */
	private String sistemaProduttore;

	/**
	 * Descrizione.
	 */
	private String descrizione;

	/**
	 * Identificativo fascicolo.
	 */
	private String idFascicolo;

	/**
	 * Identificiativo fascicolo riferimneto.
	 */
	private String idFascicoloRiferimento;

	/**
	 * Tipologia fascicolo.
	 */
	private String tipoFascicolo;

	/**
	 * Descriaione tipologia fasciccolo.
	 */
	private String descrizioneTipoFascicolo;

	/**
	 * Stato fascicolo.
	 */
	private String statoFascicoloDocumentale;

	/**
	 * Costruttore del DTO.
	 * 
	 * @param fascicolo
	 */
	public FascicoloFEPADTO(final FascicoloBaseType fascicolo) {
		if (fascicolo != null) {
			dataCreazione = DateUtils.fromXmlGregCalToDate(fascicolo.getDataCreazione());
			dataAggiornamento = DateUtils.fromXmlGregCalToDate(fascicolo.getDataAggiornamento());
			sistemaProduttore = fascicolo.getSistemaProduttore();
			idFascicolo = fascicolo.getIdFascicolo();
			idFascicoloRiferimento = fascicolo.getIdFascicoloRiferimento();
			tipoFascicolo = fascicolo.getTipoFascicolo();
			statoFascicoloDocumentale = fascicolo.getStatoFascicoloDocumentale().toString();
			descrizione = fascicolo.getDescrizione();
			descrizioneTipoFascicolo = fascicolo.getDescrizioneTipoFascicolo();
		}
	}

	/**
	 * Imposta la data creazione del fascicolo.
	 * 
	 * @param dataCreazione
	 */
	public void setDataCreazione(final Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	/**
	 * Imposta la data di aggiornamento del fascicolo.
	 * 
	 * @param dataAggiornamento
	 */
	public void setDataAggiornamento(final Date dataAggiornamento) {
		this.dataAggiornamento = dataAggiornamento;
	}

	/**
	 * Imposta il sistema produttore.
	 * 
	 * @param sistemaProduttore
	 */
	public void setSistemaProduttore(final String sistemaProduttore) {
		this.sistemaProduttore = sistemaProduttore;
	}

	/**
	 * Restituisce la descrizione del fascicolo.
	 * 
	 * @param descrizione
	 */
	public void setDescrizione(final String descrizione) {
		this.descrizione = descrizione;
	}

	/**
	 * Imposta l'id del fascicolo.
	 * 
	 * @param idFascicolo
	 */
	public void setIdFascicolo(final String idFascicolo) {
		this.idFascicolo = idFascicolo;
	}

	/**
	 * Imposta il tipo del fascicolo.
	 * 
	 * @param tipoFascicolo
	 */
	public void setTipoFascicolo(final String tipoFascicolo) {
		this.tipoFascicolo = tipoFascicolo;
	}

	/**
	 * Imposta la descrizione del tipo fascicolo.
	 * 
	 * @param descrizioneTipoFascicolo
	 */
	public void setDescrizioneTipoFascicolo(final String descrizioneTipoFascicolo) {
		this.descrizioneTipoFascicolo = descrizioneTipoFascicolo;
	}

	/**
	 * Imposta lo stato del fascicolo documentale.
	 * 
	 * @param statoFascicoloDocumentale
	 */
	public void setStatoFascicoloDocumentale(final String statoFascicoloDocumentale) {
		this.statoFascicoloDocumentale = statoFascicoloDocumentale;
	}

	/**
	 * Restituisce la data creazione.
	 * 
	 * @return data creazione fascicolo
	 */
	public Date getDataCreazione() {
		return dataCreazione;
	}

	/**
	 * Restituisce la data di aggiornamento del fascicolo.
	 * 
	 * @return data aggiornamento
	 */
	public Date getDataAggiornamento() {
		return dataAggiornamento;
	}

	/**
	 * Restituisce il sistema produttore.
	 * 
	 * @return sistema produttore
	 */
	public String getSistemaProduttore() {
		return sistemaProduttore;
	}

	/**
	 * Restituisce la descrizione del fascicolo.
	 * 
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Restituisce l'id del fascicolo.
	 * 
	 * @return id fascicolo
	 */
	public String getIdFascicolo() {
		return idFascicolo;
	}

	/**
	 * Restituisce il tipo fascicolo.
	 * 
	 * @return tipo fascicolo
	 */
	public String getTipoFascicolo() {
		return tipoFascicolo;
	}

	/**
	 * Restituisce la descrizione del tipo fascicolo.
	 * 
	 * @return descrizione tipo fascicolo
	 */
	public String getDescrizioneTipoFascicolo() {
		return descrizioneTipoFascicolo;
	}

	/**
	 * Imposta lo stato del fascicolo documentale.
	 * 
	 * @return stato fascicolo documentale
	 */
	public String getStatoFascicoloDocumentale() {
		return statoFascicoloDocumentale;
	}

	/**
	 * Restituisce l'id del fascicolo di riferimento.
	 * 
	 * @return id fascicolo riferimento
	 */
	public String getIdFascicoloRiferimento() {
		return idFascicoloRiferimento;
	}

	/**
	 * Imposta l'id del fascicolo di riferimento.
	 * 
	 * @param idFascicoloRiferimento
	 */
	public void setIdFascicoloRiferimento(final String idFascicoloRiferimento) {
		this.idFascicoloRiferimento = idFascicoloRiferimento;
	}
}