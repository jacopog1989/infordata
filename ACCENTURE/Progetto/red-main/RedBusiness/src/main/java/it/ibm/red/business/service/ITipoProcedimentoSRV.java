package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.TipoProcedimentoDTO;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.persistence.model.TipoProcedimento;
import it.ibm.red.business.service.facade.ITipoProcedimentoFacadeSRV;

/**
 * The Interface ITipoProcedimentoSRV.
 *
 * @author CPIERASC
 * 
 *         Interfaccia per il servizio di gestione dei tipi procedimento.
 */
public interface ITipoProcedimentoSRV extends ITipoProcedimentoFacadeSRV {
	
	/**
	 * Ottiene il workflow subject del processo generico di uscita per tipi procedimento.
	 * @param tipiProcedimento
	 * @return workflow subject
	 */
	String getWorkflowSubjectProcessoGenericoUscita(Collection<TipoProcedimentoDTO> tipiProcedimento);

	/**
	 * Ottiene il workflow subject del processo generico di entrata per tipi procedimento.
	 * @param tipiProcedimento
	 * @return workflow subject
	 */
	String getWorkflowSubjectProcessoGenericoEntrata(Collection<TipoProcedimentoDTO> tipiProcedimento);
	
	/**
	 * Ottiene il workflow subject del processo generico di uscita per id dell'aoo.
	 * @param idAoo
	 * @return
	 */
	String getWorkflowSubjectProcessoGenericoUscita(Long idAoo);
	
	/**
	 * Ottiene il workflow subject del processo generico di entrata per id dell'aoo.
	 * @param idAoo
	 * @return
	 */
	String getWorkflowSubjectProcessoGenericoEntrata(Long idAoo);
	
	/**
	 * Ottiene il nome del workflow del processo generico di entrata per id dell'aoo.
	 * @param idAoo
	 * @param connection
	 * @return nome del workflow
	 */
	String getWorkflowNameProcessoGenericoEntrata(Long idAoo, Connection connection);
	
	/**
	 * Restituisce le tipologia procedimento configurate e valide per un'AOO
	 * identificate dall' <code> idAoo </code>.
	 * 
	 * @see it.ibm.red.business.service.ITipoProcedimentoSRV#getTipiProcedimentoByIdAoo(java.lang.Long,
	 *      java.sql.Connection).
	 * 
	 * @param idAoo
	 *            identificativo AOO
	 * @param connection
	 *            connessione al database
	 * @return collezione delle tipologia procedimento
	 */
	Collection<TipoProcedimentoDTO> getTipiProcedimentoByIdAoo(Long idAoo, Connection connection);

	/**
	 * Ottiene tutti i tipi procedimento.
	 * @param connection
	 * @return lista dei tipi procedimento
	 */
	Collection<TipoProcedimentoDTO> getAll(Connection connection);
	
	/**
	 * Restituisce il tipo procedimento identificato dall'
	 * <code> idProcedimento </code>.
	 * 
	 * @param idProcedimento
	 *            identificativo tipo procedimento
	 * @param connection
	 *            connessione al database
	 * @return tipo porcedimento recuperato se esistente, null altrimenti
	 */
	TipoProcedimentoDTO getTipoProcedimentoById(Connection connection, Integer idProcedimento);
	
	/**
	 * Restituisce una collection contenente tutti i tipi procedimento validi per
	 * un'AOO identificata dall' <code> idAoo </code> il cui tipo categoria è uguale
	 * a <code> tipoAcquisizione </code>. Quando la tipologia esiste per più di un
	 * tipo categoria questa viene considerata solo una volta.
	 * 
	 * @param idAoo
	 *            identificativo Area organizzativa
	 * @param tipoAcquisizione
	 *            tipo categoria specificato, @see TipoCategoriaEnum
	 * @return collezione dei tipi procedimento recuperati
	 */
	Collection<TipoProcedimentoDTO> getTipiProcedimentoByIdAooFiltered(Long idAoo, TipoCategoriaEnum tipoAcquisizione);
	
	/**
	 * Metodo che consente di recuperare tutti i tipi procedimento che rispettano i flag in entrata e uscita. 
	 * Equivale a getTipiProcedimentoByIdAooFiltered limitato per Entrata e Uscita
	 * 
	 * @param idAoo
	 * @param flagGenericoEntrata 	i flag sono in legati da una logica di OR esclusivo
	 * @param flagGenericoUscita
	 * */
	Collection<TipoProcedimentoDTO> getProcedimentoByFlags(Long idAoo, int flagGenericoEntrata, int flagGenericoUscita);
	
	/**
	 * Ottiene i tipi procedimento tramite i tipi documento.
	 * @param idTipologiaDocumento
	 * @param conn
	 * @return lista dei tipi procedimento
	 */
	List<TipoProcedimento> getTipiProcedimentoByTipologiaDocumento(Integer idTipologiaDocumento, Connection conn);
	
}