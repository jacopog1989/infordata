package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.filenet.api.core.Document;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.MasterDocumentoDTO;
import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoSpedizioneDocumentoEnum;
import it.ibm.red.business.enums.TrasformazionePDFInErroreEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.DestinatariRedUtils;

/**
 * Trasformer Documento to Master.
 */
public class ContextFromDocumentoToMasterTrasformer extends ContextTrasformerCE<MasterDocumentoDTO> {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -8314914097803022080L;

	/**
	 * LOGGER.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ContextFromDocumentoToMasterTrasformer.class.getName());

	/**
	 * Posizione tipo destinatari.
	 */
	private static final int TIPO_DEST_POSITION = 3;

	/**
	 * Costruttore.
	 */
	public ContextFromDocumentoToMasterTrasformer() {
		super(TrasformerCEEnum.FROM_DOCUMENTO_TO_MASTER_CONTEXT);
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public final MasterDocumentoDTO trasform(final Document document, final Map<ContextTrasformerCEEnum, Object> context) {
		try {
			final String dt = (String) getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
			final Integer numDoc = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
			final String ogg = (String) getMetadato(document, PropertiesNameEnum.OGGETTO_METAKEY);
			final Integer numeroProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
			final Integer annoProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
			final Integer idTipologiaDocumento = (Integer) getMetadato(document, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY);
			String tipologiaDocumento = null;
			if (idTipologiaDocumento != null) {
				//CONTEXT
				tipologiaDocumento = ((Map<Long, String>) context.get(ContextTrasformerCEEnum.IDENTIFICATIVI_TIPO_DOC)).get(idTipologiaDocumento.longValue());
			}
			final Integer attachment = document.get_CompoundDocumentState().getValue();
			final Boolean bAttachmentPresent = attachment > 0;
			
			//gestione del metadato trasformazionePDFInErrore
			final Integer trasfPdfErrore = (Integer) getMetadato(document, PropertiesNameEnum.TRASFORMAZIONE_PDF_ERRORE_METAKEY);
			Boolean bTrasfPdfErrore = false;
			Boolean bTrasfPdfWarning = false;
			if (trasfPdfErrore != null) {
				bTrasfPdfErrore = TrasformazionePDFInErroreEnum.getErrorCodes().contains(trasfPdfErrore);
				bTrasfPdfWarning = TrasformazionePDFInErroreEnum.getWarnCodes().contains(trasfPdfErrore);
			}
			//////////////////////////////////////////////////////////
			
			final Integer categoriaDocumentoId = (Integer) getMetadato(document, PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY);
			final Date dataCreazione = (Date) getMetadato(document, PropertiesNameEnum.DATA_CREAZIONE_METAKEY);
			final Date dataScadenza = (Date) getMetadato(document, PropertiesNameEnum.DATA_SCADENZA_METAKEY);
			final Integer urgenza = (Integer) getMetadato(document, PropertiesNameEnum.URGENTE_METAKEY);
			final Date dataProtocollo = (Date) getMetadato(document, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY);
			final Integer idCategoriaDocumento = (Integer) getMetadato(document, PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY); 
			final Integer idFormatoDocumento = (Integer) getMetadato(document, PropertiesNameEnum.ID_FORMATO_DOCUMENTO); 
			final Collection<String> inDestinatari = (Collection<String>) getMetadato(document, PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY);
			// Si normalizza il metadato per evitare errori comuni durante lo split delle singole stringhe dei destinatari
			final List<String[]> destinatariDocumento = DestinatariRedUtils.normalizzaMetadatoDestinatariDocumento(inDestinatari);
			final TipoSpedizioneDocumentoEnum tipoSpedizione = getTipoSpedizione(destinatariDocumento);
			final Boolean flagFirmaAutografaRM = (Boolean) getMetadato(document, PropertiesNameEnum.FLAG_FIRMA_AUTOGRAFA_RM_METAKEY);
			final Integer idMomentoProtocollazione = (Integer) getMetadato(document, PropertiesNameEnum.MOMENTO_PROTOCOLLAZIONE_ID_METAKEY);
			final Integer tipoProtocollo = (Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY);
			final Integer riservato = (Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.RISERVATO_METAKEY);
			Boolean flagRiservato = false;
			if (riservato != null && riservato > 0) {
				flagRiservato = true;
			}
			final Boolean hasContributi = ((Collection<String>) context.get(ContextTrasformerCEEnum.CHECK_CONTRIBUTI)).contains(dt);
			
			return new MasterDocumentoDTO(dt, numDoc, ogg, tipologiaDocumento, null, numeroProtocollo, annoProtocollo, null, 
					bAttachmentPresent, bTrasfPdfErrore, bTrasfPdfWarning, categoriaDocumentoId, dataScadenza, urgenza, dataCreazione, dataProtocollo, 
					tipoSpedizione, idCategoriaDocumento, idFormatoDocumento, flagFirmaAutografaRM, idMomentoProtocollazione, 
					tipoProtocollo, flagRiservato, hasContributi, null);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero di un metadato durante il trasfor del master del documento: ", e);
			return null;
		}
	}
	
	/**
	 * Recupero tipo spedizione.
	 * 
	 * @param inDestinatari	collezione destinarati
	 * @return				tipo spedizione
	 */
	public static TipoSpedizioneDocumentoEnum getTipoSpedizione(final List<String[]> inDestinatari) {
		TipoSpedizioneDocumentoEnum output;
		boolean bElettronico = false;
		boolean bCartaceo = false;
		boolean bInterni =  false;
		
		if (inDestinatari != null) {
			
			for (final String[] destSplit : inDestinatari) {
				Integer idTipoSpedizione;
				
				final String tipoDestinatario = destSplit[TIPO_DEST_POSITION];
				if (Constants.TipoDestinatario.ESTERNO.equalsIgnoreCase(tipoDestinatario)) {
					idTipoSpedizione = Integer.parseInt(destSplit[2]);
					if (Constants.TipoSpedizione.MEZZO_ELETTRONICO.equals(idTipoSpedizione)) {
						bElettronico = true;
					} else {
						bCartaceo = true;
					}
				} else {
					bInterni =  true;
				}
				
				if (bElettronico && bCartaceo) {
					break;
				}
			}
			
		}
		
		if ((bElettronico && bCartaceo) || (bInterni && (bElettronico || bCartaceo))) {
			output = TipoSpedizioneDocumentoEnum.MISTO;
		} else if (bElettronico) {
			output = TipoSpedizioneDocumentoEnum.ELETTRONICO;
		} else {
			output = TipoSpedizioneDocumentoEnum.CARTACEO;
		}
		
		return output;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.ContextTrasformerCE#trasform(com.filenet.api.core.Document,
	 *      java.sql.Connection, java.util.Map).
	 */
	@Override
	public MasterDocumentoDTO trasform(final Document document, final Connection connection, final Map<ContextTrasformerCEEnum, Object> context) {
		throw new RedException();
	}
}
