
package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.core.Document;

import filenet.vw.api.VWQueueQuery;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.IASignDAO;
import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.ASignMasterDTO;
import it.ibm.red.business.dto.ASignStepResultDTO;
import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.ProtocolloEmergenzaDocDTO;
import it.ibm.red.business.dto.RegistrazioneAusiliariaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.FunzionalitaEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.SignErrorEnum;
import it.ibm.red.business.enums.SignModeEnum;
import it.ibm.red.business.enums.SignStrategyEnum;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.business.enums.SignTypeEnum.SignTypeGroupEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TrasformazionePDFInErroreEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IASignSRV;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.ICasellePostaliSRV;
import it.ibm.red.business.service.IListaDocumentiSRV;
import it.ibm.red.business.service.IRegistrazioniAusiliarieSRV;
import it.ibm.red.business.service.ISignSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.service.asign.step.StatusEnum;
import it.ibm.red.business.service.asign.step.StepEnum;
import it.ibm.red.business.service.sign.strategy.SignStrategyFactory;
import it.ibm.red.business.utils.DateUtils;

/**
 * Service che gestisce il processo di firma asincrona.
 */
@Service
public class ASignSRV extends AbstractService implements IASignSRV {
	
	/**
	 * Serial version.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger per la gestione delle informazioni in console.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ASignSRV.class.getName());

	/**
	 * Messaggio di errore recupero document title dell'utente.
	 */
	private static final String ERRORE_DURANTE_RECUPERO_DT_PER_L_UTENTE = "Errore durante recupero dt per l'utente ";

	/**
	 * Messaggio di errore update info dell'item.
	 */
	private static final String ERRORE_DURANTE_UPDATE_INFO_ITEM = "Errore durante upfate info item";

	/**
	 * Messaggio di errore recupero dell'item.
	 */
	private static final String ERRORE_DURANTE_IL_RECUPERO_DELL_ITEM = "Errore durante il recupero dell'item";

	/**
	 * Service lista documenti.
	 */
	@Autowired
	private IListaDocumentiSRV listaDocumentiSRV;

	/**
	 * DAO gestione firma asincrona.
	 */
	@Autowired
	private IASignDAO aSignDAO;

	/**
	 * Service per la gestione dell'AOO.
	 */
	@Autowired
	private IAooSRV aooSRV;

	/**
	 * Service per la gestione delle registrazioni ausiliarie.
	 */
	@Autowired
	private IRegistrazioniAusiliarieSRV registrazioniAusiliarieSRV;

	/**
	 * Service per la gestione della firma.
	 */
	@Autowired
	private ISignSRV signSRV;

	/**
	 * Service per la gestione dell'utente.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;

	/**
	 * Service per la gestione delle caselle postali.
	 */
	@Autowired
	private ICasellePostaliSRV casellePostaliSRV;

	/**
	 * Restituisce il primo item resumable in base alla priorità degli items.
	 * 
	 * @return tutte le informazioni dell'item recuperato
	 */
	@Override
	public ASignItemDTO getFirstResumableItem() {
		ASignItemDTO out = null;
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			
			out = aSignDAO.getAndLockFirstResumableItem(connection);
			commitConnection(connection);
		} catch (Exception e) {
			rollbackConnection(connection);
			LOGGER.error("Errore durante il recupero del primo item resumabile", e);
			throw new RedException("Errore durante il recupero del primo item resumabile", e);
		} finally {
			closeConnection(connection);
		}
		return out;
	}

	/**
	 * Restituisce l'item identificato dall' <code> idItem </code>.
	 * 
	 * @param idItem
	 *            identificativo dell'item da recuperare
	 * @return tutte le informazioni dell'item identificato dal suo id
	 */
	@Override
	public ASignItemDTO getItem(Long idItem) {
		return getItem(idItem, true);
	}

	/**
	 * Restituisce le informazioni dell'item identificato dall'
	 * <code> idItem </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IASignFacadeSRV#getItem(java.lang.Long,
	 *      java.lang.Boolean)
	 */
	@Override
	public ASignItemDTO getItem(Long idItem, Boolean bLog) {
		ASignItemDTO out = null;
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			out = aSignDAO.getItem(connection, idItem, bLog);
		} catch (Exception e) {
			LOGGER.error(ERRORE_DURANTE_IL_RECUPERO_DELL_ITEM, e);
			throw new RedException(ERRORE_DURANTE_IL_RECUPERO_DELL_ITEM, e);
		} finally {
			closeConnection(connection);
		}
		return out;
	}

	/**
	 * Restituisce il primo item lavorabile.
	 * 
	 * @see it.ibm.red.business.service.IASignSRV#getFirstItem().
	 */
	@Override
	public Long getFirstItem() {
		Long idItemDaLavorare = null;
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), true);

			// Recupero item da lavorare
			idItemDaLavorare = aSignDAO.getAndLockFirstItem(connection);
			
			commitConnection(connection);
		} catch (Exception e) {
			rollbackConnection(connection);
			LOGGER.error("Errore durante il recupero dell'item da lavorare" + (idItemDaLavorare != null ? (": " + idItemDaLavorare) : ""), e);
			throw new RedException(ERRORE_DURANTE_IL_RECUPERO_DELL_ITEM, e);
		} finally {
			closeConnection(connection);
		}
		return idItemDaLavorare;
	}

	/**
	 * Esegue l'aggiornamento dello step, @see ASignStepResultDTO.
	 * 
	 * @see it.ibm.red.business.service.IASignSRV#updateStep(it.ibm.red.business.dto.ASignStepResultDTO).
	 */
	@Override
	public void updateStep(ASignStepResultDTO lastResult) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			aSignDAO.insertStepInfo(connection, lastResult);
		} catch (Exception e) {
			LOGGER.error(ERRORE_DURANTE_UPDATE_INFO_ITEM, e);
			throw new RedException(ERRORE_DURANTE_UPDATE_INFO_ITEM, e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Inserisce lo step finale sull'item identificato da <code> idItem </code>.
	 * @see it.ibm.red.business.service.facade.IASignFacadeSRV#insertStopStep(java.lang.Long)
	 */
	@Override
	public void insertStopStep(Long idItem) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			aSignDAO.insertStopStep(connection, idItem);
		} catch (Exception e) {
			LOGGER.error(ERRORE_DURANTE_UPDATE_INFO_ITEM, e);
			throw new RedException(ERRORE_DURANTE_UPDATE_INFO_ITEM, e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Consente di conoscere lo stato di protocollazione di un documento per
	 * verificare la necessità di esecuzione dello step di protocollazione.
	 * 
	 * @see it.ibm.red.business.service.IASignSRV#isProtocollato(java.lang.Long).
	 * @param idItem
	 *            identificativo dell'item per il quale occorre conoscere lo stato
	 *            di protocollazione del documento
	 * @return true se il documento associato risulta protocollato, false altrimenti
	 */
	@Override
	public Boolean isProtocollato(Long idItem) {
		Connection connection = null;
		Boolean out = false;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			out = aSignDAO.isProtocollato(connection, idItem);
		} catch (Exception e) {
			LOGGER.error("Errore durante verifica protocollazione item", e);
			throw new RedException("Errore durante verifica protocollazione item", e);
		} finally {
			closeConnection(connection);
		}
		return out;
	}
	
	/**
	 * Restituisce true se esiste una registrazione ausiliaria valida e completa su
	 * un documento associato all'item identificato dal parametro in ingresso.
	 * 
	 * @param item
	 *            item di firma asincrona per il quale occorre sapere se esiste una
	 *            registrazione ausiliaria
	 * @return true se esiste una registrazione ausiliaria, false altrimenti
	 */
	@Override
	public Boolean hasRegistrazioneAusiliaria(ASignItemDTO item) {
		Connection connection = null;
		boolean out = false;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			RegistrazioneAusiliariaDTO reg = registrazioniAusiliarieSRV.getRegistrazioneAusiliaria(item.getDocumentTitle(), item.getIdAoo().intValue(), connection);
			if(reg != null && reg.getNumeroRegistrazione() != null) {
				out = true;
			} else {
				out = false;
			}
			
		} catch (Exception e) {
			LOGGER.error("Errore durante verifica registro ausiliario per l'item: " + item.getId(), e);
			throw new RedException("Errore durante verifica registro ausiliario: " + item.getId(), e);
		} finally {
			closeConnection(connection);
		}
		return out;
		
	}

	/**
	 * Consente di inserire l'item di firma asincrona.
	 * @see it.ibm.red.business.service.IASignSRV#insertItem(java.lang.String,
	 *      java.lang.String, java.lang.Integer, java.lang.Integer,
	 *      java.lang.Integer, it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.enums.SignModeEnum,
	 *      it.ibm.red.business.enums.SignTypeEnum.SignTypeGroupEnum,
	 *      it.ibm.red.business.enums.DocumentQueueEnum, java.sql.Connection).
	 */
	@Override
	public Long insertItem(String wobNumber, String documentTitle, Integer numeroDocumento, Integer numeroProtocollo, Integer annoProtocollo, UtenteDTO utenteFirmatario, 
			SignModeEnum signMode, SignTypeGroupEnum signType, DocumentQueueEnum queue, Connection connection) {
		Long out = null;
		FilenetPEHelper fpeh = null;
		
		try {
			// Inserimento nella tabella CODA_PREPARAZIONE_SPEDIZIONE
			out = aSignDAO.insertItem(connection, documentTitle, wobNumber, numeroDocumento, numeroProtocollo, annoProtocollo, utenteFirmatario.getId(), utenteFirmatario.getIdUfficio(), 
					utenteFirmatario.getIdRuolo(), utenteFirmatario.getIdAoo(), signMode, signType, queue);
			
			fpeh = new FilenetPEHelper(utenteFirmatario.getFcDTO());
			
			VWWorkObject workflowPrincipale = fpeh.getWorkFlowByWob(wobNumber, false);
			if (workflowPrincipale != null) {
				Map<String, Object> metadatiWorkflow = new HashMap<>();
				updateFlag(fpeh, workflowPrincipale, metadatiWorkflow);
			}
		} catch (Exception e) {
			LOGGER.error("Errore durante inserimento item", e);
			throw new RedException("Errore durante inserimento item", e);
		} finally {
			logoff(fpeh);
		}
		
		return out;
	}
	
	/**
	 * Esegue l'aggiornamento del metadato flag_firmaAsincronaAvviata del workflow per gestire la visualizzazione del documento nella coda RED "Preparazione alla Spedizione"
	 * Se il metadato non esiste sul workflow, il processo di firma asincrona non viene interrotto, e viene posto a false il FLAG_RENDERIZZATO per fare in modo che il documento sparisca dalla coda di partenza.
	 * @param fpeh
	 * @param workflowPrincipale
	 * @param metadatiWorkflow
	 */
	private void updateFlag(FilenetPEHelper fpeh, VWWorkObject workflowPrincipale,
			Map<String, Object> metadatiWorkflow) {
		try {
			LOGGER.info("Update FLAG_FIRMA_ASINCRONA_AVVIATA");
			metadatiWorkflow.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FLAG_FIRMA_ASINCRONA_AVVIATA_WF_METAKEY), true);
			fpeh.updateWorkflow(workflowPrincipale, metadatiWorkflow);
			LOGGER.info("FLAG_FIRMA_ASINCRONA_AVVIATA impostato a true");
		} catch (Exception e) {
			LOGGER.error("Errore nell'update del metadato FLAG_FIRMA_ASINCRONA_AVVIATA", e);
			metadatiWorkflow.remove(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FLAG_FIRMA_ASINCRONA_AVVIATA_WF_METAKEY));
			metadatiWorkflow.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FLAG_RENDERIZZATO_METAKEY), BooleanFlagEnum.NO.getIntValue());
			fpeh.updateWorkflow(workflowPrincipale, metadatiWorkflow);
			LOGGER.info("FLAG_RENDERIZZATO impostato a false");
		}
	}

	/**
	 * Effettua l'aggiornamento dello status dell'item identificato da
	 * <code> idItem </code>.
	 * 
	 * @see it.ibm.red.business.service.IASignSRV#updateStatus(java.lang.Long,
	 *      it.ibm.red.business.service.asign.step.StatusEnum).
	 */
	@Override
	public void updateStatus(Long idItem, StatusEnum status) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			aSignDAO.updateStatusItem(connection, idItem, status);
		} catch (Exception e) {
			LOGGER.error("Errore durante aggiornamento stato item", e);
			throw new RedException("Errore durante aggiornamento stato item", e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Restituisce le informazioni sullo step associato all'item identificato dal
	 * <code> idItem </code>.
	 * 
	 * @see it.ibm.red.business.service.IASignSRV#getInfo(java.lang.Long,
	 *      it.ibm.red.business.service.asign.step.StepEnum)
	 */
	@Override
	public ASignStepResultDTO getInfo(Long idItem, StepEnum step) {
		Connection connection = null;
		ASignStepResultDTO out = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			out = aSignDAO.getInfo(connection, idItem, step);
		} catch (Exception e) {
			LOGGER.error("Errore durante recupero informazioni step ", e);
			throw new RedException("Errore durante recupero informazioni step ", e);
		} finally {
			closeConnection(connection);
		}
		return out;		
	}

	/**
	 * Restituisce le informazioni sugli step dell'item identificato dal
	 * <code> idItem </code>.
	 * 
	 * @see it.ibm.red.business.service.IASignSRV#getInfos(java.lang.Long)
	 */
	@Override
	public List<ASignStepResultDTO> getInfos(Long idItem) {
		Connection connection = null;
		List<ASignStepResultDTO> out = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			out = aSignDAO.getInfos(connection, idItem);
		} catch (Exception e) {
			LOGGER.error("Errore durante recupero informazioni item", e);
			throw new RedException("Errore durante recupero informazioni item", e);
		} finally {
			closeConnection(connection);
		}
		return out;		
	}

	/**
	 * Imposta il numero di retry per l' item identificato dall'
	 * <code> idItem </code>.
	 * 
	 * @see it.ibm.red.business.service.IASignSRV#setRetry(java.lang.Long,
	 *      java.lang.Integer, java.lang.Boolean)
	 */
	@Override
	public void setRetry(Long idItem, Integer n, Boolean bResetDate) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			aSignDAO.setRetry(connection, idItem, n, bResetDate);
		} catch (Exception e) {
			LOGGER.error("Errore durante aggiornamento numero retry", e);
			throw new RedException("Errore durante aggiornamento numero retry", e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Effettua il reset dei retry dell'item identificato dal <code> idItem </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IASignFacadeSRV#resetRetry(java.lang.Long).
	 * @param idItem
	 *            identificativo dell'item
	 * @return esito del reset
	 */
	@Override
	public EsitoOperazioneDTO resetRetry(Long idItem) {
		Connection connection = null;
		ASignItemDTO item = getItem(idItem);
		
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(item.getWobNumber());
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			if (StatusEnum.ERRORE.equals(item.getStatus())) {
				aSignDAO.setRetry(connection, idItem, 0, true);
				aSignDAO.updateStatusItem(connection, idItem, StatusEnum.RETRY);
				esito = new EsitoOperazioneDTO(null, null, item.getNumeroDocumento(), item.getWobNumber());
				esito.setNote("Operazione effettuata con successo.");
			} else {
				esito = new EsitoOperazioneDTO(null, null, item.getNumeroDocumento(), item.getWobNumber());
				esito.setNote("Il numero dei retry è già stato resettato.");
			}
		} catch (Exception e) {
			LOGGER.error("Errore durante il reset dei retry", e);
			throw new RedException("Errore durante il reset dei retry", e);
		} finally {
			closeConnection(connection);
		}
		
		return esito;
	}

	/**
	 * Restituisce gli item esistenti nella coda specificata da
	 * <code> queue </code>.
	 * 
	 * @see it.ibm.red.business.service.IASignSRV#getItems(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.enums.DocumentQueueEnum)
	 */
	@Override
	public List<ASignItemDTO> getItems(UtenteDTO utente, DocumentQueueEnum queue) {
		Connection connection = null;
		List<ASignItemDTO> out = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			
			Collection<String> dts = null;
			// Se l'utenza non è di GA, si recuperano i document title che sono nel Libro Firma dell'utente
			if (!utente.isGestioneApplicativa()) {
				dts = getDTCodaPreparazioneSpedizione(utente, queue, connection);
			}
			
			out = aSignDAO.getItemsFromDTS(connection, dts);
		} catch (Exception e) {
			LOGGER.error("Errore durante recupero items per l'utente " + utente.getUsername(), e);
			throw new RedException("Errore durante recupero items per l'utente " + utente.getUsername(), e);
		} finally {
			closeConnection(connection);
		}
		
		return out;		
	}

	/**
	 * Restituisce i document title dei documenti associati agli item presenti nella
	 * coda <code> queue </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IASignFacadeSRV#getItemsDT(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.enums.DocumentQueueEnum)
	 */
	@Override
	public Set<String> getItemsDT(UtenteDTO utente, DocumentQueueEnum queue) {
		Set<String> out = null;
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			
			// Se l'utenza non è di GA, si recuperano i document title che sono nel Libro Firma dell'utente con flag apposito
			if (!utente.isGestioneApplicativa()) {
				out = getDTCodaPreparazioneSpedizione(utente, queue, connection);
			} else {
				out = aSignDAO.getDTS(connection, queue, utente.getIdAoo());
			}
			return out;
		} catch (Exception e) {
			LOGGER.error(ERRORE_DURANTE_RECUPERO_DT_PER_L_UTENTE + utente.getUsername(), e);
			throw new RedException(ERRORE_DURANTE_RECUPERO_DT_PER_L_UTENTE + utente.getUsername(), e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Restituisce i document title dei documenti ancora non firmati e per i quali
	 * esiste un item di firma asincrona incompleto.
	 * 
	 * @see it.ibm.red.business.service.facade.IASignFacadeSRV#getIncompletedDTs(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.enums.DocumentQueueEnum)
	 */
	@Override
	public Set<String> getIncompletedDTs(UtenteDTO utente, DocumentQueueEnum queue) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			
			return aSignDAO.getIncompletedDTs(connection, queue, utente.getIdAoo());
		} catch (Exception e) {
			LOGGER.error(ERRORE_DURANTE_RECUPERO_DT_PER_L_UTENTE + utente.getUsername(), e);
			throw new RedException(ERRORE_DURANTE_RECUPERO_DT_PER_L_UTENTE + utente.getUsername(), e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Imposta la priorità per l'item definito dall' <code> idItem </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IASignFacadeSRV#setPriority(java.lang.Long,
	 *      java.lang.Integer)
	 */
	@Override
	public void setPriority(Long idItem, Integer priority) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			aSignDAO.setPriority(connection, idItem, priority);
		} catch (Exception e) {
			LOGGER.error("Errore durante l'impostazione della priority", e);
			throw new RedException("Errore l'impostazione della priority", e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Restituisce l'ultimo item di firma asincrona inserito per i quali esiste un
	 * protocollo ed è uguale al <code> numeroProtocollo </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IASignFacadeSRV#getMaxIdItem(java.lang.Integer,
	 *      java.lang.Integer)
	 */
	@Override
	public Long getMaxIdItem(Integer numeroProtocollo, Integer annoProtocollo) {
		Connection connection = null;
		Long out = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			out = aSignDAO.getMaxIdItem(connection, numeroProtocollo, annoProtocollo);
		} catch (Exception e) {
			LOGGER.error(ERRORE_DURANTE_IL_RECUPERO_DELL_ITEM, e);
			throw new RedException(ERRORE_DURANTE_IL_RECUPERO_DELL_ITEM, e);
		} finally {
			closeConnection(connection);
		}
		return out;		
	}

	/**
	 * Imposta la priorità al massimo per l'item associato ad un documento
	 * protocollato con numero: <code> numeroProtocollo </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IASignFacadeSRV#setMaxPriority(java.lang.Integer,
	 *      java.lang.Integer)
	 */
	@Override
	public void setMaxPriority(Integer numeroProtocollo, Integer annoProtocollo) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			aSignDAO.setMaxPriority(connection, numeroProtocollo, annoProtocollo);
		} catch (Exception e) {
			LOGGER.error(ERRORE_DURANTE_IL_RECUPERO_DELL_ITEM, e);
			throw new RedException(ERRORE_DURANTE_IL_RECUPERO_DELL_ITEM, e);
		} finally {
			closeConnection(connection);
		}
	}
	
	/**
	 * Imposta la priorità al massimo per l'item associato ad un documento
	 * identificato dal <code> dt </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IASignFacadeSRV#setMaxPriority(java.lang.String)
	 */
	@Override
	public void setMaxPriority(String dt) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			aSignDAO.setMaxPriority(connection, dt);
		} catch (Exception e) {
			LOGGER.error(ERRORE_DURANTE_IL_RECUPERO_DELL_ITEM, e);
			throw new RedException(ERRORE_DURANTE_IL_RECUPERO_DELL_ITEM, e);
		} finally {
			closeConnection(connection);
		}
	}
	
	/**
	 * Esegue la firma remota per i documenti identificati dai
	 * <code> wobNumbers </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IASignFacadeSRV#firmaRemota(java.util.Collection,
	 *      java.lang.String, java.lang.String, it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.enums.SignTypeEnum, boolean)
	 */
	@Override
	public Collection<EsitoOperazioneDTO> firmaRemota(Collection<String> wobNumbers, String pin, String otp, UtenteDTO utenteFirmatario, 
			SignTypeEnum signType, boolean firmaMultipla) {
		Aoo aoo = aooSRV.recuperaAoo(utenteFirmatario.getIdAoo().intValue());
		
		// Se l'AOO ha configurato il PK Handler per i Timbri, è possibile anche la firma invisibile del documento principale
		// (nel caso in cui tale documento non abbia campi firma vuoti)
		boolean principaleOnlyPAdESVisible = true;
		if (aoo.getPkHandlerTimbro() != null) {
			principaleOnlyPAdESVisible = false;
		}
		
		return SignStrategyFactory.firmaRemota(getSignTransactionId(utenteFirmatario, "Remota"), SignStrategyEnum.get(aoo.getFlagStrategiaFirma()), 
				wobNumbers, pin, otp, utenteFirmatario, signType, principaleOnlyPAdESVisible, firmaMultipla);
	}
	
	/**
	 * Restituisce il sign transaction id per legare le varie attività del processo di firma sul log.
	 * 
	 * @param utenteFirmatario
	 * @param tipologiaFirma
	 * @return
	 */
	@Override
	public String getSignTransactionId(final UtenteDTO utenteFirmatario, final String tipologiaFirma) {
		return "Firma" + tipologiaFirma + "_" + utenteFirmatario.getUsername() + "_" + DateUtils.dateToString(new Date(), "yyyyMMddHHmmss") + "_";
	}

	/**
	 * Esegue il processo di firma autografa per i documenti identificati dai
	 * <code> wobNumbers </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IASignFacadeSRV#firmaAutografa(java.util.Collection,
	 *      it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.ProtocolloEmergenzaDocDTO)
	 */
	@Override
	public Collection<EsitoOperazioneDTO> firmaAutografa(Collection<String> wobNumbers, UtenteDTO utenteFirmatario, 
			ProtocolloEmergenzaDocDTO protocolloEmergenza) {
		Aoo aoo = aooSRV.recuperaAoo(utenteFirmatario.getIdAoo().intValue());
		
		return SignStrategyFactory.firmaAutografa(getSignTransactionId(utenteFirmatario, "Autografa"), SignStrategyEnum.get(aoo.getFlagStrategiaFirma()), wobNumbers, utenteFirmatario, protocolloEmergenza);
	}

	/**
	 * Esegue le azioni precedenti alla firma locale per il documento identificato
	 * dal <code> wobNumber </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IASignFacadeSRV#preFirmaLocale(java.lang.String,java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.enums.SignTypeEnum, boolean)
	 */
	@Override
	public EsitoOperazioneDTO preFirmaLocale(String signTransactionId, String wobNumber, UtenteDTO utenteFirmatario, SignTypeEnum signType, boolean firmaMultipla) {
		Aoo aoo = aooSRV.recuperaAoo(utenteFirmatario.getIdAoo().intValue());
		
		return SignStrategyFactory.preFirmaLocale(signTransactionId, SignStrategyEnum.get(aoo.getFlagStrategiaFirma()), wobNumber, utenteFirmatario, signType, firmaMultipla);
	}

	/**
	 * Esegue le operazioni successive alla firma locale.
	 * 
	 * @see it.ibm.red.business.service.facade.IASignFacadeSRV#postFirmaLocale(java.lang.String, 
	 *      it.ibm.red.business.dto.EsitoOperazioneDTO,
	 *      it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.enums.SignTypeEnum, boolean)
	 */
	@Override
	public void postFirmaLocale(String signTransactionId, EsitoOperazioneDTO signOutcome, UtenteDTO utenteFirmatario, SignTypeEnum signType, boolean copiaConforme) {
		Aoo aoo = aooSRV.recuperaAoo(utenteFirmatario.getIdAoo().intValue());
		
		SignStrategyFactory.postFirmaLocale(signTransactionId, SignStrategyEnum.get(aoo.getFlagStrategiaFirma()), signOutcome, utenteFirmatario, signType, copiaConforme);
	}
	
	@Override 
	public Document findDocAggiornato(String documentTitle, UtenteDTO utente, boolean isPrincipale) {
		IFilenetCEHelper fceh = null;
		Document doc = null;
		try  {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			if(isPrincipale) {
				doc = fceh.getDocumentByDTandAOO(documentTitle, utente.getIdAoo());
			}else {
				doc = fceh.getAllegatoForDownload(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY), documentTitle);
			}
		
		} catch (Exception e) {
			LOGGER.error("Errore durante la procedura di retrieve documento aggiornato: " + documentTitle, e);
			throw new RedException("Errore durante la procedura di retrieve documento aggiornato");
		} finally {
			popSubject(fceh);
		}
		
		return doc;
	}

	/**
	 * Restituisce le informazioni sui master di firma asincrona dei documenti
	 * identificati dai <code> dts </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IASignFacadeSRV#getMasterInfo(java.util.Collection)
	 */
	@Override
	public Map<String, ASignMasterDTO> getMasterInfo(Collection<String> dts) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return aSignDAO.getMasterInfo(connection, dts);
		} catch (Exception e) {
			LOGGER.error("Errore durante il recupero degli items", e);
			throw new RedException("Errore durante il recupero degli items", e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Restituisce le informazioni sull'item di firma asincrona associato al documento identificato dal <code> dt </code>.
	 * @see it.ibm.red.business.service.facade.IASignFacadeSRV#getDetailInfo(java.lang.String)
	 */
	@Override
	public ASignItemDTO getDetailInfo(String dt) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return aSignDAO.getDetailInfo(connection, dt);
		} catch (Exception e) {
			LOGGER.error(ERRORE_DURANTE_IL_RECUPERO_DELL_ITEM, e);
			throw new RedException(ERRORE_DURANTE_IL_RECUPERO_DELL_ITEM, e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Restituisce true se esiste almeno un item associato al documento identificato
	 * dal <code> documentTitle </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IASignFacadeSRV#isPresent(java.lang.String)
	 */
	@Override
	public Boolean isPresent(String documentTitle) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return aSignDAO.isPresent(connection, documentTitle);
		} catch (Exception e) {
			LOGGER.error(ERRORE_DURANTE_IL_RECUPERO_DELL_ITEM, e);
			throw new RedException(ERRORE_DURANTE_IL_RECUPERO_DELL_ITEM, e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Restituisce il log associato allo step <code> step </code> e appartanente
	 * all'item identificato da <code> idItem </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IASignFacadeSRV#getLog(java.lang.Long,
	 *      it.ibm.red.business.service.asign.step.StepEnum)
	 */
	@Override
	public String getLog(Long idItem, StepEnum step) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return aSignDAO.getLog(connection, idItem, step);
		} catch (Exception e) {
			LOGGER.error("Errore durante il recupero del log", e);
			throw new RedException("Errore durante il recupero del log", e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Restituisce lo stato corrente dell'item associato al documento identificato
	 * dal <code> documentTitle </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IASignFacadeSRV#checkDocStatus(java.lang.String)
	 */
	@Override
	public StatusEnum checkDocStatus(String documentTitle) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return aSignDAO.checkDocStatus(connection, documentTitle);
		} catch (Exception e) {
			LOGGER.error("Errore durante il recupero dello stato del documento: " + documentTitle, e);
			throw new RedException("Errore durante il recupero dello stato del documento: " + documentTitle, e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Restituisce l'id dell'item associato al documento identificato dal
	 * <code> documentTitle </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IASignFacadeSRV#getIdItem(java.lang.String)
	 */
	@Override
	public Long getIdItem(String documentTitle) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return aSignDAO.getIdItem(connection, documentTitle);
		} catch (Exception e) {
			LOGGER.error("Errore durante il recupero dell'id item del documento: " + documentTitle, e);
			throw new RedException("Errore durante il recupero dell'id item del documento: " + documentTitle, e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Restituisce la strategia di firma utilizzata per il documento associato
	 * all'item identificato dall' <code> idItem </code>.
	 * 
	 * @see it.ibm.red.business.service.IASignSRV#getSignStrategy(java.lang.Long)
	 */
	@Override
	public SignStrategyEnum getSignStrategy(Long idItem) {
		Connection connection = null;
		Long idAOO = null;
		SignStrategyEnum strategy = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			idAOO = aSignDAO.getIdAoo(connection, idItem);
			strategy = aooSRV.getSignStrategy(idAOO);
		} catch (Exception e) {
			LOGGER.error("Errore durante il recupero del tipo strategia dell'item: " + idItem, e);
			throw new RedException("Errore durante il recupero del tipo strategia dell'item: " + idItem, e);
		} finally {
			closeConnection(connection);
		}
		return strategy;
	}

	/**
	 * Esegue la inser del documento identificato dal <code> wobNumber </code> nel
	 * buffer per la firma asincrona.
	 * 
	 * @see it.ibm.red.business.service.facade.IASignFacadeSRV#insertIntoSignedDocsBuffer(java.lang.String,
	 *      java.lang.String, byte[], boolean)
	 */
	@Override
	public void insertIntoSignedDocsBuffer(String wobNumber, String guid, byte[] contentFirmato, boolean allegato) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			aSignDAO.insertIntoSignedDocsBuffer(connection, wobNumber, guid, contentFirmato, allegato);
		} catch (Exception e) {
			LOGGER.error("Errore durante l'accodamento nel buffer del documento con GUID: " + guid + " per il workflow: " + wobNumber, e);
			throw new RedException("Errore durante l'accodamento nel buffer del documento con GUID: " + guid + " per il workflow: " + wobNumber, e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Rimuove il documento identificato dal <code> wobNumber </code> dal buffer di
	 * firma asincrona.
	 * 
	 * @see it.ibm.red.business.service.IASignSRV#removeFromSignedDocsBuffer(java.lang.String,
	 *      java.sql.Connection)
	 */
	@Override
	public void removeFromSignedDocsBuffer(String wobNumber, Connection connection) { 
		try {
			aSignDAO.removeFromSignedDocsBufferByWobNumber(connection, wobNumber);
		} catch (Exception e) {
			LOGGER.error("Errore durante l'eliminazione dal buffer dei documenti firmati per il workflow: " + wobNumber, e);
			throw new RedException("Errore durante l'eliminazione dal buffer dei documenti firmati per il workflow: " + wobNumber, e);
		}
	}

	/**
	 * Restituisce una mappa che associa ad ogni <code> wob number </code> l'item di
	 * riferimento per la quale il procedimento di firma asincrona è stato avviato.
	 * 
	 * @see it.ibm.red.business.service.IASignSRV#filterWorkflowForAsyncSignAlreadyStarted(java.util.Collection,
	 *      it.ibm.red.business.dto.UtenteDTO)
	 */
	@Override
	public Map<String, ASignItemDTO> filterWorkflowForAsyncSignAlreadyStarted(String signTransactionId, Collection<String> wobNumbersToSign, UtenteDTO utente) {
		Map<String, ASignItemDTO> wobNumberToAsignItemMap = new HashMap<>();
		Connection connection = null;
		FilenetPEHelper fpeh = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			VWWorkObject wobToSign;
			String documentTitle;
			
			for (String wobNumberToSign : wobNumbersToSign) {
				wobToSign = fpeh.getWorkFlowByWob(wobNumberToSign, true);
				LOGGER.info(signTransactionId + " recuperato il workflow " + wobNumberToSign);
				if (wobToSign != null) {
					Boolean firmaAsincronaAvviata = (Boolean) TrasformerPE.getMetadato(wobToSign, 
							PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FLAG_FIRMA_ASINCRONA_AVVIATA_WF_METAKEY));
					
					if (Boolean.TRUE.equals(firmaAsincronaAvviata)) {
						LOGGER.info(signTransactionId + " firma asincrona già avviata per " + wobNumberToSign);
						documentTitle = String.valueOf(TrasformerPE.getMetadato(wobToSign, 
								PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY)));
						
						wobNumberToAsignItemMap.put(wobNumberToSign, aSignDAO.getDetailInfo(connection, documentTitle));
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error(signTransactionId + "Errore durante il recupero della mappa wob number -> document title", e);
			throw new RedException("Errore durante il recupero della mappa wob number -> document title", e);
		} finally {
			closeConnection(connection);
			logoff(fpeh);
		}
		return wobNumberToAsignItemMap;
	}

	/**
	 * Restituisce la lista degli item per i quali il procedimento di firma
	 * asincrona non ha avuto un esito positivo.
	 * 
	 * @see it.ibm.red.business.service.IASignSRV#getItemsDTFailed(java.lang.Long)
	 */
	@Override
	public List<ASignItemDTO> getItemsDTFailed(Long idAoo) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return aSignDAO.getItemsDTFailed(connection, idAoo);
		} catch (Exception e) {
			LOGGER.error("Errore durante il recupero dei documenti in stato di errore che hanno raggiunto il numero massimo di retry", e);
			throw new RedException("Errore durante il recupero dei documenti in stato di errore che hanno raggiunto il numero massimo di retry", e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Gestisce i content firmati.
	 * 
	 * @see it.ibm.red.business.service.IASignSRV#gestisciContentsFirmati(java.util.Collection,
	 *      it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.enums.SignModeEnum,
	 *      it.ibm.red.business.enums.SignTypeEnum, boolean)
	 */
	@Override
	public void gestisciContentsFirmati(String signTransactionId, Collection<EsitoOperazioneDTO> signOutcomes, UtenteDTO utenteFirmatario, SignModeEnum signMode, SignTypeEnum signType,
			boolean copiaConforme) {
		if (!CollectionUtils.isEmpty(signOutcomes)) {
			Connection conBuffer = null;
			IFilenetCEHelper fceh = null;
			FilenetPEHelper fpeh = null;
			
			try {
				conBuffer = setupConnection(getDataSource().getConnection(), false);
				fceh = FilenetCEHelperProxy.newInstance(utenteFirmatario.getFcDTO(), utenteFirmatario.getIdAoo());
				fpeh = new FilenetPEHelper(utenteFirmatario.getFcDTO());
				
				// Per ogni content FIRMATO
				for (EsitoOperazioneDTO signOutcome : signOutcomes) {
					gestisciContentFirmato(signTransactionId, signOutcome, utenteFirmatario, signMode, signType, copiaConforme, fceh, fpeh, conBuffer);
				}
				
			} catch (Exception e) {
				LOGGER.error("Errore durante la gestione di: " + signOutcomes.size() + " content firmati per l'utente: " + utenteFirmatario.getUsername(), e);
			} finally {
				closeConnection(conBuffer);
				popSubject(fceh);
				logoff(fpeh);
			}
		}
	}

	/**
	 * Restituisce l'id dell'utente mittente del workflow associato al documento
	 * identificato dal <code> documentTitle </code>. L'utente mittente di
	 * riferimento non è obbligatoriamente l'utente mittente iniziale del documento.
	 * Ad es. se il documento passa per il corriere, l'utente che consente al
	 * documento di procedere risulterà come utente mittente.
	 * 
	 * @param documentTitle
	 *            identificativo del documento
	 * @param fcDTO
	 *            credenziali filenet per l'accesso a FileNet
	 * @return id utente mittente
	 */
	private Long getMittenteDoc(String documentTitle, FilenetCredentialsDTO fcDTO) {
		VWWorkObject workflow = null;
		FilenetPEHelper fpeh = null;
		Long idUtenteMittente = null;
		try {
			fpeh = new FilenetPEHelper(fcDTO);
			workflow = fpeh.getWorkflowPrincipale(documentTitle, fcDTO.getIdClientAoo());
			if(workflow != null) {
				idUtenteMittente = Long.valueOf((Integer)TrasformerPE.getMetadato(workflow, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY)));
			}
		} catch (Exception e) {
			LOGGER.error("Errore durante il recupero dell'utente mittente", e);
			throw new RedException("Errore durante il recupero dell'utente mittente", e);
		} finally {
			if(fpeh != null) {
				fpeh.logoff();
			}
		}
		return idUtenteMittente;
	}

	/**
	 * Restituisce true se l'item identificato da <code> idItem </code> è associato
	 * ad un iter di firma multipla.
	 * 
	 * @see it.ibm.red.business.service.IASignSRV#isIterFirmaMultipla(java.lang.Long)
	 */
	@Override
	public Boolean isIterFirmaMultipla(Long idItem) {
		Boolean firmaMultipla = null;
		FilenetPEHelper fpeh = null;
		Connection connection = null;
		
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			ASignItemDTO item = getItem(idItem);
			UtenteDTO utenteFirmatario = utenteSRV.getById(item.getIdFirmatario(), item.getIdRuoloFirmatario(), item.getIdUfficioFirmatario(), connection);
			
			fpeh = new FilenetPEHelper(utenteFirmatario.getFcDTO());
			VWWorkObject wob = fpeh.getWorkflowPrincipale(item.getDocumentTitle(), utenteFirmatario.getFcDTO().getIdClientAoo());
			firmaMultipla = Integer.valueOf(TipoAssegnazioneEnum.FIRMA_MULTIPLA.getId()).equals(TrasformerPE.getMetadato(wob, PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY));
		} catch (Exception e) {
			LOGGER.error("Errore durante il recupero del tipo firma dell'iter", e);
			throw new RedException("Errore durante il recupero del tipo firma dell'iter", e);
		} finally {
			closeConnection(connection);
			logoff(fpeh);
		}
		
		return firmaMultipla;
	}

	/**
	 * Aggiorna le informazioni sull'item identificato da <code> idItem </code>.
	 * 
	 * @see it.ibm.red.business.service.IASignSRV#saveAndOverrideItemData(java.sql.Connection,
	 *      java.lang.Long, java.util.Map)
	 */
	@Override
	public void saveAndOverrideItemData(Connection con, Long idItem, Map<String, String> dataToSave) {
		// Rimozione dei dati eventualmente già presenti per lo stesso item
		aSignDAO.deleteItemData(con, idItem);
		
		// Memorizzazione dei dati
		aSignDAO.saveItemData(con, idItem, dataToSave);
	}
	
	/**
	 * Gestisce i content firmati.
	 * 
	 * @param signTransactionId
	 *            transactionId dell'operazione di firma
	 * @param signOutcome
	 *            esito della firma per la quale occorre gestire il content
	 * @param utenteFirmatario
	 *            utente responsabile della firma del documento
	 * @param signMode
	 *            modalità della firma, @see SignMode
	 * @param signType
	 *            tipologia della firma, @see SignTypeEnum
	 * @param copiaConforme
	 *            flag associato alla copia conforme
	 * @param fceh
	 *            Helper per il content engine di Filenet
	 * @param fpeh
	 *            Helper per il PE di FileNet
	 * @param con
	 *            connessione al database
	 * @param conBuffer
	 *            connessione al database per il recupero del buffer
	 */
	private void gestisciContentFirmato(String signTransactionId, EsitoOperazioneDTO signOutcome, UtenteDTO utenteFirmatario, SignModeEnum signMode, SignTypeEnum signType,
			boolean copiaConforme, IFilenetCEHelper fceh, FilenetPEHelper fpeh, Connection conBuffer) {
		String documentTitle = null;
		Integer metadatoPdfErrorValue = null;
		Connection con = null;
		String logId = signTransactionId;
		try {
			if (signOutcome.isEsito()) {
				
				con = setupConnection(getDataSource().getConnection(), true);
				
				String wobNumber = signOutcome.getWobNumber();
				VWWorkObject workflow = fpeh.getWorkFlowByWob(wobNumber, true);
				documentTitle = String.valueOf(TrasformerPE.getMetadato(workflow, PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
				
				logId = logId + documentTitle;
				
				if (!copiaConforme) {
					
					LOGGER.info(logId + " non copia conforme");
					
					// 1) Si eseguono le operazioni da eseguire immediatamente dopo la firma per i documenti firmati afferenti a flussi che coinvolgono NPS
					signSRV.gestisciFlussiPerNPS(logId, signOutcome, workflow, documentTitle, utenteFirmatario, signType, SignModeEnum.AUTOGRAFA.equals(signMode), fceh, con, conBuffer);
					
					LOGGER.info(logId + " gestiti i flussi NPS");
					
					// 2) Si aggiornano i content su FileNet con le versioni firmate dei documenti.
					// Si usa una connessione apposita perché la rimozione dal buffer va eseguita in ogni caso
					signSRV.updateDocumentsVersionFromSignedDocsBuffer(logId, wobNumber, utenteFirmatario, signType, fceh, conBuffer);
					
					LOGGER.info(logId + " aggiornato filenet con le versioni firmate dei documenti");
					
					// 3) Si inserisce un item nella coda di firma asincrona.
					// La parte asincrona comincia sempre dal primo step (START), è compito degli step stessi 
					// capire se il lavoro è già stato eseguito in modalità sincrona
					Long idItem = insertItem(wobNumber, documentTitle, signOutcome.getIdDocumento(), signOutcome.getNumeroProtocollo(), signOutcome.getAnnoProtocollo(), utenteFirmatario,  
							signMode, SignTypeGroupEnum.fromTypeToGroup(signType), DocumentQueueEnum.get(workflow.getCurrentQueueName(), false), con);
	
					LOGGER.info(logId + "Inserito un nuovo item nella coda di firma asincrona per il documento: " + signOutcome.getDocumentTitle() 
					+ " [ID item: " + idItem + "]");
					
				} else { // Copia Conforme
					
					LOGGER.info(logId + " copia conforme");
					
					// 1) Si aggiornano i content su FileNet con le versioni firmate dei documenti.
					// Si usa una connessione apposita perché la rimozione dal buffer va eseguita in ogni caso
					signSRV.updateDocumentsVersionFromSignedDocsBuffer(logId, wobNumber, utenteFirmatario, signType, fceh, conBuffer);
					
					LOGGER.info(logId + " aggiornato filenet con le versioni firmate dei documenti");
					
					// 2) Si avanza il workflow sollecitando la response di attestazione della Copia Conforme
					String responseAttestaCopiaConforme = ResponsesRedEnum.ATTESTA_COPIA_CONFORME.getResponse();
					if (ArrayUtils.contains(workflow.getStepResponses(), responseAttestaCopiaConforme)) {
						fpeh.nextStep(workflow, null, responseAttestaCopiaConforme);
					}
					
					LOGGER.info(logId + " invocata la response " + responseAttestaCopiaConforme);
				}
				
				commitConnection(con);
				
				metadatoPdfErrorValue = Constants.Varie.TRASFORMAZIONE_PDF_OK;
			}
		} catch (Exception e) {
			LOGGER.warn(e);
			rollbackConnection(con);
			metadatoPdfErrorValue = TrasformazionePDFInErroreEnum.KO_GENERICO.getValue();
			signOutcome.setEsito(false);
			signOutcome.setCodiceErrore(SignErrorEnum.POST_FIRMA_CONTENTS_ASIGN_COD_ERROR);
			signOutcome.setNote(SignErrorEnum.POST_FIRMA_CONTENTS_ASIGN_COD_ERROR.getMessage());
		} finally {
			closeConnection(con);
			// Si aggiorna il metadato che indica lo stato del processo
			if (StringUtils.isNotBlank(documentTitle) && metadatoPdfErrorValue != null) {
				signSRV.aggiornaMetadatoTrasformazione(logId, documentTitle, utenteFirmatario, metadatoPdfErrorValue);
			}
		}
	}
	
	
	/**
	 * Restituisce un set di document title associati alla coda <code> queue </code>
	 * recuperandoli da FileNet.
	 * 
	 * @param utente
	 *            utente in sessione
	 * @param queue
	 *            coda di lavoro a cui fanno riferimento i documenti recuperati
	 * @param connection
	 *            connessione al database
	 * @return i document title recuperati
	 */
	private Set<String> getDTCodaPreparazioneSpedizione(UtenteDTO utente, DocumentQueueEnum queue, Connection connection) {
		Set<String> dts = new HashSet<>();
		VWQueueQuery queueQuery = listaDocumentiSRV.getQueueFilenet(queue, utente, null, connection);
		while (queueQuery.hasNext()) {
			dts.add(TrasformerPE.getMetadato((VWWorkObject) queueQuery.next(), PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY)).toString());
		}
		return dts;
	}
	
	/**
	 * Recupero destinatari per inoltro mail comunicazione errore. Le mail
	 * recuperate sono associati al mittente del documento in firma, dal firmatario
	 * e dall'utente GA configurato.
	 * 
	 * @param item
	 *            per il quale occorre recuperare i contatti email per la
	 *            comunicazione
	 * @return Lista delle mail dei destinatari
	 */
	@Override
	public List<String> recuperaEmailDestinatari(ASignItemDTO item) {
		List<String> destinatari = new ArrayList<>();
		IFilenetCEHelper fceh = null;
		try {
			UtenteDTO utenteFirmatario = utenteSRV.getById(item.getIdFirmatario());
			
			if(utenteFirmatario!= null) {
				fceh = FilenetCEHelperProxy.newInstance(utenteFirmatario.getFcDTO(), utenteFirmatario.getIdAoo());
			} else {
				throw new RedException("Impossibile recuperare l'utente firmatario per l'id " + item.getIdFirmatario());
			}
			
			// Utente Gestione Applicativa
			String codiceAoo = aooSRV.recuperaAoo(item.getIdAoo().intValue()).getCodiceAoo();
			String destinatarioGA = PropertiesProvider.getIstance().getParameterByString(codiceAoo + "." + PropertiesNameEnum.ASIGN_AOO_DESTINATARIO_COMUNICAZIONI.getKey()); 
			if(!StringUtils.isEmpty(destinatarioGA) && !"null".equals(destinatarioGA)) {
				destinatari.add(destinatarioGA);
			}
			
			List<CasellaPostaDTO> casellePostali = casellePostaliSRV.getCasellePostali(utenteFirmatario,  fceh, FunzionalitaEnum.MAIL_LEAF);
			if(!CollectionUtils.isEmpty(casellePostali)) {
				destinatari.add(casellePostali.get(0).getIndirizzoEmail());
			}
			
			// Utente Mittente
			Long idUtenteMittente = getMittenteDoc(item.getDocumentTitle(), utenteFirmatario.getFcDTO());
			if(idUtenteMittente != null) {
				UtenteDTO utenteInvioInFirma = utenteSRV.getById(idUtenteMittente);
				List<CasellaPostaDTO> casellePostaliUtenteInvio = casellePostaliSRV.getCasellePostali(utenteInvioInFirma,  fceh, FunzionalitaEnum.MAIL_LEAF);
				if(casellePostaliUtenteInvio != null && !destinatari.contains(casellePostaliUtenteInvio.get(0).getIndirizzoEmail())){
					destinatari.add(casellePostaliUtenteInvio.get(0).getIndirizzoEmail());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Errore riscontrato durante il recupero dei destinatari per la comunicazione di firme in errore. " + e);
			throw new RedException("Errore riscontrato durante il recupero dei destinatari per la comunicazione di firme in errore. ", e);
		} finally {
			if (fceh != null) {
				fceh.popSubject();
			}
		}
		return destinatari;
	}

	/**
	 * Imposta il flag <code> comunicato </code> sull'item identificato da
	 * <code> idItem </code>.
	 * 
	 * @see it.ibm.red.business.service.IASignSRV#setIsNotified(java.lang.Long,
	 *      boolean)
	 */
	@Override
	public void setIsNotified(Long idItem, boolean comunicato) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			aSignDAO.setIsNotified(idItem, comunicato, connection);
			
		} catch (Exception e) {
			LOGGER.error("Errore riscontrato durante la modifica del flag comunicato per l'item: " + idItem, e);
			throw new RedException("Errore riscontrato durante la modifica del flag comunicato per l'item: " + idItem, e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Restituisce l'id dell'item associato al documento identificato dall'
	 * <code> idDocumento </code>.
	 * 
	 * @see it.ibm.red.business.service.IASignSRV#getIdItemFromIdDocumento(java.lang.Long,
	 *      java.lang.Long)
	 */
	@Override
	public Long getIdItemFromIdDocumento(Long idDocumento, Long idAoo) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return aSignDAO.getIdItemFromIdDocumento(idDocumento, idAoo, connection);
			
		} catch (Exception e) {
			LOGGER.error("Errore riscontrato durante il recupero dell'id item associato al documento con numero: " + idDocumento, e);
			throw new RedException("Errore riscontrato durante il recupero dell'id item associato al documento con numero: " + idDocumento, e);
		} finally {
			closeConnection(connection);
		}
	}
	
	/**
	 * Recupera tutti gli item pronti per essere elaborati per la prima volta,
	 * quindi nello stato <code> StatusEnum.PRESO_IN_CARICO </code>. L'elaborazione
	 * degli stessi avviene tramite <code> play </code> definito dall'Orchestrator
	 * per la firma asincrona.
	 * 
	 * @param idAoo
	 *            identificativo dell'AOO
	 * @return lista degli id degli item per cui è possibile eseguire il play
	 */
	@Override
	public List<Long> getPlayableItems(Long idAoo) {
		List<Long> playables = new ArrayList<>();
		Connection connection = null;
		
		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			String codiceAoo = aooSRV.recuperaAoo(idAoo.intValue()).getCodiceAoo();
			
			// Recupero item da lavorare
			playables = aSignDAO.getAndLockAllPlayableItems(connection, idAoo, codiceAoo);
			commitConnection(connection);
		} catch (Exception e) {
			rollbackConnection(connection);
			LOGGER.error("Errore durante il recupero degli item da elaborare [PLAY]", e);
			throw new RedException("Errore durante il recupero degli item da elaborare [PLAY]", e);
		} finally {
			closeConnection(connection);
		}
		
		return playables;
	}
	
	/**
	 * Recupera tutti gli item nello stato: StatusEnum.RETRY. Un item nello stato
	 * retry deve essere processato dal metodo <code> retry </code>
	 * dell'Orchestrator di firma asincrona.
	 * 
	 * @param idAoo
	 *            identificativo dell'AOO
	 * @return lista degli item per i quali è necessario eseguire un retry, @see ASignItemDTO
	 */
	@Override
	public List<ASignItemDTO> getResumableItems(Long idAoo) {
		List<ASignItemDTO> resumables = new ArrayList<>();
		Connection connection = null;
		
		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			String codiceAoo = aooSRV.recuperaAoo(idAoo.intValue()).getCodiceAoo();

			// Recupero item da riavviare
			resumables = aSignDAO.getAndLockAllResumableItems(connection, idAoo, codiceAoo);
			commitConnection(connection);
		} catch (Exception e) {
			rollbackConnection(connection);
			LOGGER.error("Errore durante il recupero degli item da riavviare [RESUME]", e);
			throw new RedException("Errore durante il recupero degli item da riavviare [RESUME]", e);
		} finally {
			closeConnection(connection);
		}
		
		return resumables;
	}
	
	/**
	 * Restituisce true se il document title è associato ad un item in stato
	 * <code> StatusEnum.ELABORATO </code> e quindi con una firma già presente.
	 * Consente di verificare se procedere con la firma di un documento e comunicare
	 * l'avvenuta firma dello stesso.
	 * 
	 * @param documentTitle
	 *            identificativo del documento per la quale occorre conoscere lo
	 *            stato della firma
	 * @return true se il documento identificato dal <code> documentTitle </code>
	 *         risulta già firmato, false altrimenti
	 */
	@Override
	public boolean isAlreadySigned (String documentTitle) {
		boolean signed = false;
		
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			signed = aSignDAO.isSignedOnce(connection, documentTitle);
		} catch (Exception e) {
			LOGGER.error("Errore durante la verifica di esistenza di una firma sul documento: " + documentTitle, e);
			throw new RedException("Errore durante la verifica di esistenza di una firma sul documento: " + documentTitle, e);
		} finally {
			closeConnection(connection);
		}
		
		return signed;
	}
	
}

