package it.ibm.red.business.dto.filenet;

import it.ibm.red.business.dto.AbstractDTO;
import it.ibm.red.business.enums.FilenetTipoOperazioneEnum;

/**
 * DTO che definisce un documento fascicolazione RED.
 */
public class DocumentoFascicolazioneRedFnDTO extends AbstractDTO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -7672022621598371057L;
	
	/**
	 * Document title.
	 */
	private String documentTitle;
	
	/**
	 * Tipo operazione.
	 */
	private FilenetTipoOperazioneEnum tipoOperazione;
	
	/**
	 * Flag da validare.
	 */
	private boolean toValidate;
	
	/**
	 * Flag eredita security.
	 */
	private boolean ereditaSecurity;

	/**
	 * Restituisce il documentTitle.
	 * @return documentTitle
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Imposta il documentTitle.
	 * @param documentTitle
	 */
	public void setDocumentTitle(final String documentTitle) {
		this.documentTitle = documentTitle;
	}

	/**
	 * Restituisce il tipo operazione, @see FilenetTipoOperazioneEnum.
	 * @return tipo Operazione
	 */
	public FilenetTipoOperazioneEnum getTipoOperazione() {
		return tipoOperazione;
	}

	/**
	 * Imposta il tipo operazione.
	 * @param tipoOperazione
	 */
	public void setTipoOperazione(final FilenetTipoOperazioneEnum tipoOperazione) {
		this.tipoOperazione = tipoOperazione;
	}

	/**
	 * Restituisce true se da validare, false altrimenti.
	 * @return true se da validare, false altrimenti
	 */
	public boolean isToValidate() {
		return toValidate;
	}

	/**
	 * Imposta il flag: toValidate
	 * @param toValidate
	 */
	public void setToValidate(final boolean toValidate) {
		this.toValidate = toValidate;
	}

	/**
	 * @return ereditaSecurity
	 */
	public boolean isEreditaSecurity() {
		return ereditaSecurity;
	}

	/**
	 * @param ereditaSecurity
	 */
	public void setEreditaSecurity(final boolean ereditaSecurity) {
		this.ereditaSecurity = ereditaSecurity;
	}
}