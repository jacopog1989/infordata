package it.ibm.red.business.dto;

import it.ibm.red.business.enums.MailOperazioniEnum;

/**
 * Classe InoltroRifiutoAooConfDTO.
 *
 * @author adilegge
 * 
 */
public class ProtocollaMailAooConfDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3371763676540743239L;

	/**
	 * Identificativo aoo
	 */
	private Long idAoo;
	
	/**
	 * Operazione mail
	 */
	private MailOperazioniEnum operazione;
	
	/**
	 * Identificativo utente responsabile
	 */
	private Long idUtenteResponsabile;
	
	/**
	 * Identificativo nodo responsabile
	 */
	private Long idNodoResponsabile;
	
	/**
	 * Identificativo ruolo responsabile
	 */
	private Long idRuoloResponsabile;
	
	/**
	 * Indice classificazione
	 */
	private String indiceClassificazione;
	
	/**
	 * Guid template
	 */
	private String guidTemplate;
	
	/**
	 * Nome template
	 */
	private String nomeTemplate;
	
	/**
	 * Oggetto template
	 */
	private String oggettoTemplate;
	
	/**
	 * Costruttore protocolla mail aoo conf DTO.
	 *
	 * @param idAoo the id aoo
	 * @param operazione the operazione
	 * @param idUtenteResponsabile the id utente responsabile
	 * @param idNodoResponsabile the id nodo responsabile
	 * @param idRuoloResponsabile the id ruolo responsabile
	 * @param indiceClassificazione the indice classificazione
	 * @param guidTemplate the guid template
	 * @param nomeTemplate the nome template
	 * @param oggettoTemplate the oggetto template
	 */
	public ProtocollaMailAooConfDTO(final Long idAoo, final MailOperazioniEnum operazione, final Long idUtenteResponsabile,
			final Long idNodoResponsabile, final Long idRuoloResponsabile, final String indiceClassificazione, final String guidTemplate, 
			final String nomeTemplate, final String oggettoTemplate) {
		super();
		this.idAoo = idAoo;
		this.operazione = operazione;
		this.idUtenteResponsabile = idUtenteResponsabile;
		this.idNodoResponsabile = idNodoResponsabile;
		this.idRuoloResponsabile = idRuoloResponsabile;
		this.indiceClassificazione = indiceClassificazione;
		this.guidTemplate = guidTemplate;
		this.nomeTemplate = nomeTemplate;
		this.oggettoTemplate = oggettoTemplate;
	}

	/** 
	 * @return the id aoo
	 */
	public Long getIdAoo() {
		return idAoo;
	}

	/** 
	 * @param idAoo the new id aoo
	 */
	public void setIdAoo(final Long idAoo) {
		this.idAoo = idAoo;
	}

	/** 
	 * @return the operazione
	 */
	public MailOperazioniEnum getOperazione() {
		return operazione;
	}

	/** 
	 * @param operazione the new operazione
	 */
	public void setOperazione(final MailOperazioniEnum operazione) {
		this.operazione = operazione;
	}

	/** 
	 * @return the id utente responsabile
	 */
	public Long getIdUtenteResponsabile() {
		return idUtenteResponsabile;
	}

	/** 
	 * @param idUtenteResponsabile the new id utente responsabile
	 */
	public void setIdUtenteResponsabile(final Long idUtenteResponsabile) {
		this.idUtenteResponsabile = idUtenteResponsabile;
	}

	/** 
	 * @return the id nodo responsabile
	 */
	public Long getIdNodoResponsabile() {
		return idNodoResponsabile;
	}

	/** 
	 * @param idNodoResponsabile the new id nodo responsabile
	 */
	public void setIdNodoResponsabile(final Long idNodoResponsabile) {
		this.idNodoResponsabile = idNodoResponsabile;
	}
	
	/** 
	 * @return the id ruolo responsabile
	 */
	public Long getIdRuoloResponsabile() {
		return idRuoloResponsabile;
	}

	/** 
	 * @param idRuoloResponsabile the new id ruolo responsabile
	 */
	public void setIdRuoloResponsabile(final Long idRuoloResponsabile) {
		this.idRuoloResponsabile = idRuoloResponsabile;
	}

	/** 
	 * @return the indice classificazione
	 */
	public String getIndiceClassificazione() {
		return indiceClassificazione;
	}

	/** 
	 * @param indiceClassificazione the new indice classificazione
	 */
	public void setIndiceClassificazione(final String indiceClassificazione) {
		this.indiceClassificazione = indiceClassificazione;
	}

	/** 
	 * @return the guid template
	 */
	public String getGuidTemplate() {
		return guidTemplate;
	}

	/** 
	 * @param guidTemplate the new guid template
	 */
	public void setGuidTemplate(final String guidTemplate) {
		this.guidTemplate = guidTemplate;
	}
	
	/** 
	 * @return the nome template
	 */
	public String getNomeTemplate() {
		return nomeTemplate;
	}

	/** 
	 * @param nomeTemplate the new nome template
	 */
	public void setNomeTemplate(final String nomeTemplate) {
		this.nomeTemplate = nomeTemplate;
	}
	
	/** 
	 * @return the oggetto template
	 */
	public String getOggettoTemplate() {
		return oggettoTemplate;
	}

	/** 
	 * @param oggettoTemplate the new oggetto template
	 */
	public void setOggettoTemplate(final String oggettoTemplate) {
		this.oggettoTemplate = oggettoTemplate;
	}
	
}