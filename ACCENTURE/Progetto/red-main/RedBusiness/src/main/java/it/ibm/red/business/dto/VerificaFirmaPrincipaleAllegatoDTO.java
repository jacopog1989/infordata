package it.ibm.red.business.dto;

import it.ibm.red.business.enums.ValueMetadatoVerificaFirmaEnum;

/**
 * DTO che definisce le informazioni sulla verifica firma dell'allegato principale.
 */
public class VerificaFirmaPrincipaleAllegatoDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -1357500329546032860L;

	/**
	 * Document title.
	 */
	private String documentTitle;
	
	/**
	 * Flag principale.
	 */
	private boolean principale;
	
	/**
	 * Nome file.
	 */
	private String nomeFile;
	
	/**
	 * Verifica firma.
	 */
	private ValueMetadatoVerificaFirmaEnum valoreVerificaFirma;
	
	/**
	 * Flag firma valide documento principale/allegati.
	 */
	private boolean firmeValidePrincAllegato;
	
	/**
	 * Costruttore.
	 * @param nomeFile
	 * @param documentTitle
	 * @param principale
	 * @param valoreVerificaFirma
	 */
	public VerificaFirmaPrincipaleAllegatoDTO(final String nomeFile, final String documentTitle,
			final boolean principale, final ValueMetadatoVerificaFirmaEnum valoreVerificaFirma) {
		this.nomeFile = nomeFile;
		this.documentTitle = documentTitle;
		this.principale = principale;
		this.valoreVerificaFirma = valoreVerificaFirma;
	}
	
	/**
	 * Restituisce il document title.
	 * @return document title
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Imposta il document title.
	 * @param documentTitle
	 */
	public void setDocumentTitle(final String documentTitle) {
		this.documentTitle = documentTitle;
	}

	/**
	 * Restituisce il flag associato al principale.
	 * @return flag associato al principale
	 */
	public boolean isPrincipale() {
		return principale;
	}

	/**
	 * Imposta il flag associato al principale
	 * @param principale
	 */
	public void setPrincipale(final boolean principale) {
		this.principale = principale;
	}

	/**
	 * Restituisce il valore verifica firma.
	 * @return valore verifica firma
	 */
	public ValueMetadatoVerificaFirmaEnum getValoreVerificaFirma() {
		return valoreVerificaFirma;
	}

	/**
	 * Imposta il valore verifica firma.
	 * @param valoreVerificaFirma
	 */
	public void setValoreVerificaFirma(final ValueMetadatoVerificaFirmaEnum valoreVerificaFirma) {
		this.valoreVerificaFirma = valoreVerificaFirma;
	}
	
	/**
	 * Restituisce il nome del file.
	 * @return nome del file
	 */
	public String getNomeFile() {
		return nomeFile;
	}

	/**
	 * Imposta il nome del file.
	 * @param nomeFile
	 */
	public void setNomeFile(final String nomeFile) {
		this.nomeFile = nomeFile;
	}

	/**
	 * Restituisce il flag associato alla valdita firme principale allegato.
	 * @return flag associato alla valdita firme principale allegato
	 */
	public boolean isFirmeValidePrincAllegato() {
		return firmeValidePrincAllegato;
	}
	
	/**
	 * Imposta il flag associato alla valdita firme principale allegato.
	 * @param firmeValidePrincAllegato
	 */
	public void setFirmeValidePrincAllegato(final boolean firmeValidePrincAllegato) {
		this.firmeValidePrincAllegato = firmeValidePrincAllegato;
	}
	
}
