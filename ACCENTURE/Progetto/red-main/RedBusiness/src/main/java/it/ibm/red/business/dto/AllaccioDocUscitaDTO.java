/**
 * 
 */
package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author VINGENITO
 *
 */
public class AllaccioDocUscitaDTO implements Serializable {

	private static final long serialVersionUID = 1267584593274328726L;

	/**
	 * Document title uscita.
	 */
	private String docTitleUscita;

	/**
	 * Docuemnt title ingresso.
	 */
	private String docTitleIngresso;

	/**
	 * Lista document title ingresso.
	 */
	private List<String> docTitleIngressoList;

	/**
	 * Stato.
	 */
	private Integer stato;

	/**
	 * Messaggio d'errore.
	 */
	private String errorMsg;

	/**
	 * Data creazione.
	 */
	private Date dataCreazione;

	/**
	 * Data aggiornamento.
	 */
	private Date dataAggiornamento;

	/**
	 * Identitifcativo aoo.
	 */
	private Integer idAoo;
	
	/**
	 * Titolario.
	 */
	private String titolario;
	
	/**
	 * Costruttore.
	 */
	public AllaccioDocUscitaDTO() {
		
	}
	
	/**
	 * Costruttore.
	 */
	public AllaccioDocUscitaDTO(final String inDocTitleUscita, final String inDocTitleIngresso, final Integer inStato, final String inErrorMsg, final Date inDataCreazione, final Date inDataAggiornamento, final Integer inIdAoo) {
		docTitleUscita = inDocTitleUscita;
		docTitleIngresso = inDocTitleIngresso;
		stato = inStato;
		errorMsg = inErrorMsg;
		dataCreazione = inDataCreazione;
		dataAggiornamento = inDataAggiornamento;
		idAoo = inIdAoo;
	}

	/**
	 * @return the dataAggiornamento
	 */
	public final Date getDataAggiornamento() {
		return dataAggiornamento;
	}

	/**
	 * @param dataAggiornamento the dataAggiornamento to set
	 */
	public void setDataAggiornamento(final Date dataAggiornamento) {
		this.dataAggiornamento = dataAggiornamento;
	}

	/**
	 * @return the dataCreazione
	 */
	public final Date getDataCreazione() {
		return dataCreazione;
	}

	/**
	 * @param dataCreazione the dataCreazione to set
	 */
	public void setDataCreazione(final Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	/**
	 * @return the errorMsg
	 */
	public final String getErrorMsg() {
		return errorMsg;
	}

	/**
	 * @param errorMsg the errorMsg to set
	 */
	public void setErrorMsg(final String errorMsg) {
		this.errorMsg = errorMsg;
	}

	/**
	 * @return the stato
	 */
	public final Integer getStato() {
		return stato;
	}

	/**
	 * @param stato the stato to set
	 */
	public void setStato(final Integer stato) {
		this.stato = stato;
	}

	/**
	 * @return the docTitleIngresso
	 */
	public final String getDocTitleIngresso() {
		return docTitleIngresso;
	}

	/**
	 * @param docTitleIngresso the docTitleIngresso to set
	 */
	public void setDocTitleIngresso(final String docTitleIngresso) {
		this.docTitleIngresso = docTitleIngresso;
	}
	/**
	 * @return the docTitleIngressoList
	 */
	public final List<String> getDocTitleIngressoList() {
		return docTitleIngressoList;
	}

	/**
	 * @param docTitleIngressoList the docTitleIngressoList to set
	 */
	public void setDocTitleIngressoList(final List<String> docTitleIngressoList) {
		this.docTitleIngressoList = docTitleIngressoList;
	}

	/**
	 * @return the docTitleUscita
	 */
	public final String getDocTitleUscita() {
		return docTitleUscita;
	}

	/**
	 * @param docTitleUscita the docTitleUscita to set
	 */
	public void setDocTitleUscita(final String docTitleUscita) {
		this.docTitleUscita = docTitleUscita;
	}

	/**
	 * @return the idAoo
	 */
	public Integer getIdAoo() {
		return idAoo;
	}
	
	/**
	 * @param idAoo the idAoo to set
	 */
	public void setIdAoo(final Integer idAoo) {
		this.idAoo = idAoo;
	}
	
	/**
	 * @return the titolario
	 */
	public String getTitolario() {
		return titolario;
	}
	
	/**
	 * @param titolario the titolario to set
	 */
	public void setTitolario(final String titolario) {
		this.titolario = titolario;
	}
}
