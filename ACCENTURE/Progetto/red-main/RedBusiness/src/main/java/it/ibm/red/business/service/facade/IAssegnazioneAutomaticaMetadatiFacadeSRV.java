package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.AssegnazioneMetadatiDTO;

/**
 * Facade per la gestione del SRV delle assegnazioni automatiche dei metadati
 * 
 * @author SimoneLungarella
 */
public interface IAssegnazioneAutomaticaMetadatiFacadeSRV extends Serializable {
	/**
	 * Metodo che consente il recupero delle assegnazioni automatiche dei metadati
	 * per uno specifico AOO.
	 * 
	 * @param idAoo
	 *            identificativo dell'AOO per cui occorre recuperare le assegnazioni
	 * @return lista delle assegnazioni automatiche per metadati
	 */
	List<AssegnazioneMetadatiDTO> getByIdAoo(Long idAoo);

	/**
	 * Metodo che consente l'eliminazione di tutte le assegnazioni automatiche per
	 * metadati per l'AOO identificata dall' <code> idAoo </code>
	 * 
	 * @param idAoo
	 *            identificativo dell'AOO per cui eliminare le assegnazioni
	 */
	void deleteByIdAoo(Long idAoo);

	/**
	 * Metodo che consente il salvataggio di una assegnazione automatica dei
	 * metadati.
	 * 
	 * @param assegnazioneMetadato
	 *            DTO per la gestione delle informazioni relative all'assegnazione
	 * @param idAoo
	 *            identificativo dell'area organizzativa
	 */
	void registraAssegnazioneMetadato(AssegnazioneMetadatiDTO assegnazioneMetadato, Long idAoo);

	/**
	 * Metodo che consente l'eliminazione di una assegnazione automatica dei
	 * metadati tramite il suo identificativo.
	 * 
	 * @param idAssegnazione
	 *            identificativo dell'assegnazione da eliminare
	 */
	void deleteByIdAssegnazione(Integer idAssegnazione);

}
