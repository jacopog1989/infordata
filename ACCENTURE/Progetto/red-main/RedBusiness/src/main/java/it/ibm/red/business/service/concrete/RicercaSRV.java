/**
 * 
 */
package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.core.Document;
import com.google.common.collect.ImmutableMap;

import filenet.vw.api.VWQueueQuery;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dao.ICodeApplicativeDAO;
import it.ibm.red.business.dao.ILookupDAO;
import it.ibm.red.business.dao.IRegistroRepertorioDAO;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dto.DatiCodaApplicativaDTO;
import it.ibm.red.business.dto.DocumentQueueQueryObject;
import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.RecuperoCodeDTO;
import it.ibm.red.business.dto.RegistroRepertorioDTO;
import it.ibm.red.business.dto.RicercaResultDTO;
import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CampoRicercaEnum;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.QueueGroupEnum;
import it.ibm.red.business.enums.RicercaGenericaTypeEnum;
import it.ibm.red.business.enums.RicercaPerBusinessEntityEnum;
import it.ibm.red.business.enums.StatoLavorazioneEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.exception.SearchException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IListaDocumentiSRV;
import it.ibm.red.business.service.IRegistroRepertorioSRV;
import it.ibm.red.business.service.IRicercaSRV;
import it.ibm.red.business.service.ITitolarioSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.DestinatariRedUtils;
import it.ibm.red.business.utils.StringUtils;
import it.ibm.red.webservice.model.documentservice.types.ricerca.OperatoriRicerca;
import it.ibm.red.webservice.model.documentservice.types.ricerca.ParametriRicerca;
import it.ibm.red.webservice.model.documentservice.types.ricerca.TipoVersione;

/**
 * @author APerquoti
 *
 */
@Service
public class RicercaSRV extends AbstractService implements IRicercaSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 3714934865649351849L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaSRV.class.getName());
	
	/**
	 * Subject del Work Object.
	 */
	private static final String VW_WORK_OBJECT_GET_SUBJECT = "-------------->VWWorkObject.getSubject: ";

	/**
	 * Messaggio errore recupero code del documento. Occorre appendere il document
	 * title del documento a questo messaggio.
	 */
	private static final String ERROR_RECUPERO_CODE_MSG = "Errore durante il recupero delle code del documento con DocumentTitle: ( ";

	/**
	 * Service.
	 */
	@Autowired
	private IListaDocumentiSRV listaDocumentiSRV;

	/**
	 * DAO per la gestione delle Code Applicative.
	 */
	@Autowired
	private ICodeApplicativeDAO codeApplicativeDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private ILookupDAO lookupDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private ITipoProcedimentoDAO tipoProcedimentoDAO;

	/**
	 * Service.
	 */
	@Autowired
	private ITitolarioSRV titolarioSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private IRegistroRepertorioDAO registroRepertorioDAO;

	/**
	 * Service.
	 */
	@Autowired
	private IRegistroRepertorioSRV registroRepertorioSRV;

	/**
	 * Servizio firma asincrona.
	 */
	@Autowired
	private ASignSRV aSignSRV;
	
	/**
	 * Operatori.
	 */
	private static final Map<OperatoriRicerca, String> OPERATORI_RICERCA_WS = ImmutableMap.<OperatoriRicerca, String>builder().put(OperatoriRicerca.UGUALE, "=")
			.put(OperatoriRicerca.DIVERSO, "!=").put(OperatoriRicerca.MINORE, "<").put(OperatoriRicerca.MAGGIORE, ">").put(OperatoriRicerca.MINORE_UGUALE, "<=")
			.put(OperatoriRicerca.MAGGIORE_UGUALE, ">=").put(OperatoriRicerca.LIKE, "LIKE").build();

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaFacadeSRV#ricercaRapidaDocumenti
	 *      (java.lang.String, java.lang.Integer,
	 *      it.ibm.red.business.enums.RicercaGenericaTypeEnum,
	 *      it.ibm.red.business.enums.CategoriaDocumentoEnum,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Collection<MasterDocumentRedDTO> ricercaRapidaDocumenti(final String key, final Integer anno, final RicercaGenericaTypeEnum type,
			final CategoriaDocumentoEnum categoria, final UtenteDTO utente, final boolean onlyProtocollati) {
		Connection connection = null;
		IFilenetCEHelper fceh = null;

		try {
			// al contrario delle code la ricerca inizia con il recupero dei documenti dal
			// CE
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final DocumentSet documents = fceh.ricercaGenerica(key, anno, type, categoria, utente.getIdAoo(), false, onlyProtocollati);

			connection = setupConnection(getDataSource().getConnection(), false);

			return trasformToGenericDocContext(documents, utente, connection);
		} catch (final Exception e) {
			throw new RedException(e);
		} finally {
			closeConnection(connection);
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaFacadeSRV#ricercaRapidaDocumenti
	 *      (java.lang.String, java.lang.Integer,
	 *      it.ibm.red.business.enums.RicercaGenericaTypeEnum,
	 *      it.ibm.red.business.enums.CategoriaDocumentoEnum,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Collection<MasterDocumentRedDTO> ricercaRapidaDocumenti(final String key, final Integer anno, final RicercaGenericaTypeEnum type,
			final CategoriaDocumentoEnum categoria, final UtenteDTO utente) {
		return ricercaRapidaDocumenti(key, anno, type, null, utente, false);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaFacadeSRV#ricercaRapidaDocumenti
	 *      (java.lang.String, java.lang.Integer,
	 *      it.ibm.red.business.enums.RicercaGenericaTypeEnum,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Collection<MasterDocumentRedDTO> ricercaRapidaDocumenti(final String key, final Integer anno, final RicercaGenericaTypeEnum type, final UtenteDTO utente) {
		return ricercaRapidaDocumenti(key, anno, type, null, utente);
	}

	/**
	 * @see it.ibm.red.business.service.IRicercaSRV#trasformToGenericDocContext(com.
	 *      filenet.api.collection.DocumentSet, it.ibm.red.business.dto.UtenteDTO,
	 *      java.sql.Connection).
	 */
	@Override
	public Collection<MasterDocumentRedDTO> trasformToGenericDocContext(final DocumentSet documents, final UtenteDTO utente, final Connection connection) {
		final Map<ContextTrasformerCEEnum, Object> context = new EnumMap<>(ContextTrasformerCEEnum.class);

		final Map<Long, String> idsTipoDoc = lookupDAO.getDescTipoDocumento(utente.getIdAoo().intValue(), connection);
		context.put(ContextTrasformerCEEnum.IDENTIFICATIVI_TIPO_DOC, idsTipoDoc);

		final Map<Long, String> tps = tipoProcedimentoDAO.getAllDesc(connection);
		context.put(ContextTrasformerCEEnum.IDENTIFICATIVI_TIPO_PROC, tps);

		context.put(ContextTrasformerCEEnum.GESTISCI_RESPONSE, false);

		// ### Gestione Mittente / Destinatari -> START
		final Map<String, String> contattiMap = new HashMap<>();

		final Map<String, RegistroRepertorioDTO> registroRepertorioMap = new HashMap<>();

		Collection<String> dts = new ArrayList<>();
		if (documents != null && !documents.isEmpty()) {
			
			final Iterator<?> itDsFileNet = documents.iterator();
			
			final RegistroRepertorioDTO denominazioneRegistro = registroRepertorioDAO.getDenominazioneRegistroUfficiale(utente.getIdAoo(), connection);
			final Map<Long, List<RegistroRepertorioDTO>> registriRepertorioConfigurati = registroRepertorioSRV.getRegistriRepertorioConfigurati(utente.getIdAoo(), connection);
			
			while (itDsFileNet.hasNext()) {
				final Document document = (Document) itDsFileNet.next();
				final String mittente = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.MITTENTE_METAKEY);

				if (!StringUtils.isNullOrEmpty(mittente)) {
					final String[] mittenteSplit = mittente.split("\\,");

					if (mittenteSplit.length >= 2 && contattiMap.get(mittenteSplit[0]) == null) {
						contattiMap.put(mittenteSplit[0], mittenteSplit[1]);
					}
				} else {
					final Collection<?> destinatari = (Collection<?>) TrasformerCE.getMetadato(document, PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY);
					// Si normalizza il metadato per evitare errori comuni durante lo split delle
					// singole stringhe dei destinatari
					final List<String[]> destinatariDocumento = DestinatariRedUtils.normalizzaMetadatoDestinatariDocumento(destinatari);

					if (!CollectionUtils.isEmpty(destinatariDocumento)) {
						for (final String[] destSplit : destinatariDocumento) {

							if (destSplit.length >= 5) {
								final TipologiaDestinatarioEnum tde = TipologiaDestinatarioEnum.getByTipologia(destSplit[3]);
								if (TipologiaDestinatarioEnum.INTERNO == tde && "0".equals(destSplit[1])) {
									contattiMap.put(destSplit[0], destSplit[2]);
								} else if (TipologiaDestinatarioEnum.INTERNO == tde && contattiMap.get(destSplit[1]) == null) {
									contattiMap.put(destSplit[1], destSplit[2]);
								} else if (TipologiaDestinatarioEnum.ESTERNO == tde && contattiMap.get(destSplit[0]) == null) {
									contattiMap.put(destSplit[0], destSplit[1]);
								}
							}

						}
					}
				}

				// Gestione Registro Repertorio -> START VI
				final String registroRepertorio = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.REGISTRO_PROTOCOLLO_FN_METAKEY);
				final String documentTitle = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
				dts.add(documentTitle);
				if (registroRepertorio == null) {

					final Integer idTipoDocumento = (Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY);
					final Integer idTipoProcedimento = (Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY);

					Boolean isRegistroRepertorioPreProtocollo = Boolean.FALSE;
					if (idTipoDocumento != null && idTipoProcedimento != null) {
						isRegistroRepertorioPreProtocollo = registroRepertorioSRV.checkIsRegistroRepertorio(utente.getIdAoo(), idTipoDocumento.longValue(),
								idTipoProcedimento.longValue(), registriRepertorioConfigurati, connection);
					}

					registroRepertorioMap.put(documentTitle, new RegistroRepertorioDTO(isRegistroRepertorioPreProtocollo, null));

				} else {

					if (registroRepertorio != null && !registroRepertorio.equalsIgnoreCase(denominazioneRegistro.getDescrizioneRegistroRepertorio())) {
						registroRepertorioMap.put(documentTitle, new RegistroRepertorioDTO(true, registroRepertorio));
					} else {
						registroRepertorioMap.put(documentTitle, new RegistroRepertorioDTO(false, denominazioneRegistro.getDescrizioneRegistroRepertorio()));
					}

				}
				// Gestione Registro Repertorio -> END
			}
		}

		context.put(ContextTrasformerCEEnum.CONTATTI_DISPLAY_NAME, contattiMap);
		// ### Gestione Mittente / Destinatari -> END

		if (!registroRepertorioMap.isEmpty()) {
			context.put(ContextTrasformerCEEnum.REGISTRO_PROTOCOLLO, registroRepertorioMap);
		}

		context.put(ContextTrasformerCEEnum.IS_RICERCA, true);
		context.put(ContextTrasformerCEEnum.ASIGN_INFO, aSignSRV.getMasterInfo(dts));

		return TrasformCE.transform(documents, TrasformerCEEnum.FROM_DOCUMENTO_TO_GENERIC_DOC_CONTEXT, context);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaFacadeSRV#ricercaGenerica
	 *      (java.lang.String, java.lang.Integer,
	 *      it.ibm.red.business.enums.RicercaGenericaTypeEnum,
	 *      it.ibm.red.business.enums.RicercaPerBusinessEntityEnum,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Collection<MasterDocumentRedDTO> ricercaGenerica(final String key, final Integer anno, final RicercaGenericaTypeEnum type,
			final RicercaPerBusinessEntityEnum entity, final UtenteDTO utente) {
		try {
			final DocumentSet documents = ricercaGenericaFilenet(key, anno, type, entity, utente);

			return TrasformCE.transform(documents, TrasformerCEEnum.FROM_DOCUMENTO_TO_GENERIC_DOC);
		} catch (final Exception e) {
			throw new RedException(e);
		}
	}

	private DocumentSet ricercaGenericaFilenet(final String key, final Integer anno, final RicercaGenericaTypeEnum type, final RicercaPerBusinessEntityEnum entity,
			final UtenteDTO utente) throws SearchException {
		DocumentSet documents = null;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			documents = ricercaGenericaFilenet(key, anno, type, entity, utente, fceh);
		} finally {
			popSubject(fceh);
		}

		return documents;
	}

	private DocumentSet ricercaGenericaFilenet(final String key, final Integer anno, final RicercaGenericaTypeEnum type, final RicercaPerBusinessEntityEnum entity,
			final UtenteDTO utente, final IFilenetCEHelper fceh) throws SearchException {
		DocumentSet documents = null;

		// Considero solo i campi che sono stati valorizzati per la ricerca
		if (anno == null) {
			if (key == null) {
				throw new SearchException("Per continuare è necessario valorizzare i campi obbligatori.");
			}
		} else if (anno > Calendar.getInstance().get(Calendar.YEAR)) {
			throw new SearchException("L'anno inserito non risulta valido. [" + anno + " > " + Calendar.getInstance().get(Calendar.YEAR) + "]");
		}

		if (type == null) {
			throw new SearchException("Il tipo di ricerca non è stato valorizzato correttamente.");
		}

		if (entity == null) {
			throw new SearchException("L'Entità di ricerca non è stata valorizzata correttamente.");
		}

		try {
			String strAnno = null;
			if (anno != null) {
				strAnno = anno.toString();
			}

			switch (entity) {
			case DOCUMENTI:
				documents = fceh.ricercaGenerica(key, anno, type, utente.getIdAoo());
				break;

			case FASCICOLI:
				documents = fceh.ricercaGenericaFascicoli(key, type, utente.getIdAoo(), strAnno);
				break;

			case FALDONI:
				documents = eseguiRicercaGenerica(key, type, utente, fceh, strAnno);
				break;

			case DOCUMENTI_FASCICOLI:
				throw new IllegalArgumentException("Non è possibile eseguire una ricerca Generica Filenet per due entità documentali");

			default:
				throw new RedException("Tipo ricerca non previsto.");
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la ricerca generica dei " + entity.name() + ": " + e.getMessage(), e);
			throw new SearchException("Errore durante la ricerca generica dei " + entity.name() + ": " + e.getMessage());
		}

		return documents;
	}

	/**
	 * Esegue la ricerca generica dei Faldoni restituendone i risultati.
	 * 
	 * @param key
	 *            Chiave della ricerca.
	 * @param type
	 *            Type della ricerca.
	 * @param utente
	 *            Utente in sessione.
	 * @param fceh
	 *            Filenet Content Engine Helper.
	 * @param strAnno
	 *            Anno ricerca.
	 * @return Risultati della ricerca.
	 */
	private static DocumentSet eseguiRicercaGenerica(final String key, final RicercaGenericaTypeEnum type,
			final UtenteDTO utente, final IFilenetCEHelper fceh, String strAnno) {
		
		final Long idUtente = utente.getId();
		final Long idUfficioUtente = utente.getIdUfficio();
		final String utenteUsername = utente.getUsername();
		final Long idUfficioCorriereUtente = utente.getIdNodoCorriere();
		
		return fceh.ricercaGenericaFaldoni(key, type, utente.getIdAoo(), strAnno, idUfficioUtente, idUfficioCorriereUtente.intValue(), idUtente, utenteUsername,
				utente.getVisFaldoni(), false);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaFacadeSRV#ricercaGenericaFascicoli
	 *      (java.lang.String, java.lang.Integer,
	 *      it.ibm.red.business.enums.RicercaGenericaTypeEnum,
	 *      it.ibm.red.business.enums.RicercaPerBusinessEntityEnum,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Collection<FascicoloDTO> ricercaGenericaFascicoli(final String key, final Integer anno, final RicercaGenericaTypeEnum type,
			final RicercaPerBusinessEntityEnum entity, final UtenteDTO utente) {
		Collection<FascicoloDTO> fascicoliDTO = null;
		Connection connection = null;

		try {
			final DocumentSet documents = ricercaGenericaFilenet(key, anno, type, entity, utente);

			connection = setupConnection(getDataSource().getConnection(), false);

			fascicoliDTO = transformFascicoli(utente, connection, documents);
		} catch (final Exception e) {
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return fascicoliDTO;
	}

	private Collection<FascicoloDTO> transformFascicoli(final UtenteDTO utente, final Connection connection, final DocumentSet documents) {
		final Collection<FascicoloDTO> fascicoliDTO = TrasformCE.transform(documents, TrasformerCEEnum.FROM_DOCUMENTO_TO_FASCICOLO, connection);
		setDescTitolario(utente, connection, fascicoliDTO);
		return fascicoliDTO;
	}

	/**
	 * @see it.ibm.red.business.service.IRicercaSRV#setDescTitolario
	 *      (it.ibm.red.business.dto.UtenteDTO, java.sql.Connection,
	 *      java.util.Collection).
	 */
	@Override
	public void setDescTitolario(final UtenteDTO utente, final Connection connection, final Collection<FascicoloDTO> fascicoliDTO) {
		final List<TitolarioDTO> titolari = titolarioSRV.getFigliRadice(utente.getIdAoo(), utente.getIdUfficio(), connection);

		// Imposto la descrizine del titolario per ogni fascicolo
		for (final FascicoloDTO fas : fascicoliDTO) {
			for (final TitolarioDTO tit : titolari) {
				if (tit.getIndiceClassificazione().equals(fas.getIndiceClassificazione())) {
					fas.setDescrizioneTitolario(tit.getDescrizione());
					break;
				}
			}
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaFacadeSRV#ricercaGenericaFaldoni
	 *      (java.lang.String, java.lang.Integer,
	 *      it.ibm.red.business.enums.RicercaGenericaTypeEnum,
	 *      it.ibm.red.business.enums.RicercaPerBusinessEntityEnum,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Collection<FaldoneDTO> ricercaGenericaFaldoni(final String key, final Integer anno, final RicercaGenericaTypeEnum type, final RicercaPerBusinessEntityEnum entity,
			final UtenteDTO utente) {
		Collection<FaldoneDTO> documentiCE = null;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final DocumentSet documents = ricercaGenericaFilenet(key, anno, type, entity, utente, fceh);

			documentiCE = TrasformCE.transform(documents, TrasformerCEEnum.FROM_DOCUMENT_TO_FALDONE);

			if (documentiCE != null && !documentiCE.isEmpty()) {

				for (final FaldoneDTO figlio : documentiCE) {
					if (!StringUtils.isNullOrEmpty(figlio.getParentFaldone())) {
						// se ha un padre devo recuperare il nome del padre per avere la gerarchia
						final Document padreD = fceh.getFaldoneByDocumentTitle(figlio.getParentFaldone(), figlio.getIdAOO());
						if (padreD != null) {
							final String nomePadre = (String) TrasformerCE.getMetadato(padreD, PropertiesNameEnum.METADATO_FALDONE_NOMEFALDONE);
							figlio.setNomePadre(nomePadre);
							final String oggettoPadre = (String) TrasformerCE.getMetadato(padreD, PropertiesNameEnum.METADATO_FALDONE_OGGETTO);
							figlio.setOggettoPadre(oggettoPadre);
						}
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}

		return documentiCE;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaFacadeSRV#
	 *      getQueueNameFromDocumentTitle (it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String, java.lang.Integer, boolean).
	 */
	@Override
	public RecuperoCodeDTO getQueueNameFromDocumentTitle(final UtenteDTO utente, final String documentTitle, final String classeDocumentale,
			final Integer idCategoriaDocumento, final boolean fullQuery) {
		final Boolean isDocEntrata = idCategoriaDocumento == null ? null : CategoriaDocumentoEnum.ENTRATA == CategoriaDocumentoEnum.get(idCategoriaDocumento);

		if (fullQuery) {
			return getQueueNameFromDocumentTitleFull(utente, documentTitle, classeDocumentale, isDocEntrata);
		}

		return getQueueNameFromDocumentTitle(utente, documentTitle, classeDocumentale, isDocEntrata);
	}

	/**
	 * @see it.ibm.red.business.service.IRicercaSRV#getQueueNameFromDocumentTitle
	 *      (it.ibm.red.business.dto.UtenteDTO, java.lang.String, java.lang.String,
	 *      java.lang.Boolean).
	 */
	@Override
	public RecuperoCodeDTO getQueueNameFromDocumentTitle(final UtenteDTO utente, final String documentTitle, final String classeDocumentale, final Boolean isDocEntrata) {
		Collection<DatiCodaApplicativaDTO> dcaColl = new ArrayList<>();
		final Collection<DocumentQueueEnum> listaCode = new ArrayList<>();
		Collection<VWQueueQuery> queryWF = new ArrayList<>();
		final Collection<String> nomiQueuesNonCensite = new ArrayList<>();
		final RecuperoCodeDTO output = null;
		Connection connection = null;

		try {
			queryWF = listaDocumentiSRV.getQueueFNbyDocumentTitle(utente, documentTitle, classeDocumentale, isDocEntrata);
			connection = setupConnection(getDataSource().getConnection(), false);

			for (final VWQueueQuery qwf : queryWF) {
				while (qwf.hasNext()) {
					final VWWorkObject wo = (VWWorkObject) qwf.next();
					LOGGER.info(VW_WORK_OBJECT_GET_SUBJECT + wo.getSubject());

					Boolean inLavorazione = null;
					if (utente.isUcb()) {
						inLavorazione = TrasformerPE.getMetadato(wo, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IN_LAVORAZIONE_METAKEY)) != null
								&& ((Integer) TrasformerPE.getMetadato(wo, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IN_LAVORAZIONE_METAKEY))) == 1;
					}

					// scorro i risultati della query per verificare che ci sia un match con le Code
					// censite
					final DocumentQueueEnum dqe = DocumentQueueEnum.get(wo.getCurrentQueueName(), inLavorazione);
					// se dalla verifica non viene fuori un match positivo creo una raccolta con le
					// code non censite
					if (dqe.equals(DocumentQueueEnum.NON_CENSITO)) {
						nomiQueuesNonCensite.add(wo.getCurrentQueueName());
					}

					listaCode.add(dqe);
				}
			}

			// recupero le 'n' code applicative associate a questo id
			dcaColl = codeApplicativeDAO.getQueueName(documentTitle, utente.getId(), utente.getIdUfficio(), connection);

			for (final DatiCodaApplicativaDTO dcaDTO : dcaColl) {
				QueueGroupEnum group = QueueGroupEnum.UFFICIO;
				final StatoLavorazioneEnum sle = StatoLavorazioneEnum.get(dcaDTO.getIdStatoLav());

				// <------ Parti I --------->
				// parte che decodifica il gruppo
				if (dcaDTO.getIdUtente() != null && dcaDTO.getIdUtente() != 0) {
					group = QueueGroupEnum.UTENTE;
					if (StatoLavorazioneEnum.isOneOf(sle, StatoLavorazioneEnum.ATTI, StatoLavorazioneEnum.RISPOSTA, StatoLavorazioneEnum.MOZIONE)) {
						group = QueueGroupEnum.ARCHIVIO;
					}
				}
				// <------ Fine Parte I --------->

				listaCode.addAll(DocumentQueueEnum.getQueue(sle, group));
			}

			if (listaCode.isEmpty()) {
				listaCode.add(DocumentQueueEnum.NESSUNA_CODA);
			}

			return new RecuperoCodeDTO(listaCode, nomiQueuesNonCensite);
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_CODE_MSG + documentTitle + " ): " + e.getMessage(), e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.IRicercaSRV#getQueueNameFromDocumentTitle(it.ibm.
	 *      red.business.dto.UtenteDTO, java.lang.String).
	 */
	@Override
	public RecuperoCodeDTO getQueueNameFromDocumentTitle(final UtenteDTO utente, final String documentTitle) {
		Collection<DatiCodaApplicativaDTO> dcaColl = new ArrayList<>();
		final Collection<DocumentQueueEnum> listaCode = new ArrayList<>();
		Collection<VWQueueQuery> queryWF = new ArrayList<>();
		final Collection<String> nomiQueuesNonCensite = new ArrayList<>();
		final RecuperoCodeDTO output = null;
		Connection connection = null;

		try {
			queryWF = listaDocumentiSRV.getQueueFNbyDocumentTitle(utente, documentTitle, null, null);
			connection = setupConnection(getDataSource().getConnection(), false);

			for (final VWQueueQuery qwf : queryWF) {
				while (qwf.hasNext()) {
					final VWWorkObject wo = (VWWorkObject) qwf.next();
					LOGGER.info(VW_WORK_OBJECT_GET_SUBJECT + wo.getSubject());
					// scorro i risultati della query per verificare che ci sia un match con le Code
					// censite

					Boolean inLavorazione = null;
					if (utente.isUcb()) {
						inLavorazione = TrasformerPE.getMetadato(wo, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IN_LAVORAZIONE_METAKEY)) != null
								&& ((Integer) TrasformerPE.getMetadato(wo, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.IN_LAVORAZIONE_METAKEY))) == 1;
					}

					final DocumentQueueEnum dqe = DocumentQueueEnum.get(wo.getCurrentQueueName(), inLavorazione);
					// se dalla verifica non viene fuori un match positivo creo una raccolta con le
					// code non censite
					if (dqe.equals(DocumentQueueEnum.NON_CENSITO)) {
						nomiQueuesNonCensite.add(wo.getCurrentQueueName());
					}

					listaCode.add(dqe);
				}
			}

			// recupero le 'n' code applicative associate a questo id
			dcaColl = codeApplicativeDAO.getQueueName(documentTitle, utente.getId(), utente.getIdUfficio(), connection);

			for (final DatiCodaApplicativaDTO dcaDTO : dcaColl) {
				QueueGroupEnum group = QueueGroupEnum.UFFICIO;
				final StatoLavorazioneEnum sle = StatoLavorazioneEnum.get(dcaDTO.getIdStatoLav());

				// <------ Parti I --------->
				// parte che decodifica il gruppo
				if (dcaDTO.getIdUtente() != null && dcaDTO.getIdUtente() != 0) {
					group = QueueGroupEnum.UTENTE;
					if (StatoLavorazioneEnum.isOneOf(sle, StatoLavorazioneEnum.ATTI, StatoLavorazioneEnum.RISPOSTA, StatoLavorazioneEnum.MOZIONE)) {
						group = QueueGroupEnum.ARCHIVIO;
					}
				}
				// <------ Fine Parte I --------->

				listaCode.addAll(DocumentQueueEnum.getQueue(sle, group));
			}

			if (listaCode.isEmpty()) {
				listaCode.add(DocumentQueueEnum.NESSUNA_CODA);
			}

			return new RecuperoCodeDTO(listaCode, nomiQueuesNonCensite);
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_CODE_MSG + documentTitle + " ): " + e.getMessage(), e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}

		return output;
	}

	private RecuperoCodeDTO getQueueNameFromDocumentTitleFull(final UtenteDTO utente, final String documentTitle, final String classeDocumentale, final Boolean isDocEntrata) {
		Collection<DatiCodaApplicativaDTO> dcaColl = new ArrayList<>();
		final Collection<DocumentQueueEnum> listaCode = new ArrayList<>();
		Collection<DocumentQueueQueryObject> queryObjWF = new ArrayList<>();
		final Collection<String> nomiQueuesNonCensite = new ArrayList<>();
		final RecuperoCodeDTO output = null;
		Connection connection = null;

		try {
			queryObjWF = listaDocumentiSRV.getQueueFNbyDocumentTitleFull(utente, documentTitle, classeDocumentale, isDocEntrata);

			connection = setupConnection(getDataSource().getConnection(), false);

			for (final DocumentQueueQueryObject qwf : queryObjWF) {
				if (qwf.getQueueQueryObj() != null) {
					
					// Verifico se il documento che risulta in libro firma non sia invece afferente alla coda applicativa PREPARAZIONE_ALLA_SPEDIZIONE
					if(qwf.getQueueEnum().equals(DocumentQueueEnum.NSD) && utente.isGestioneApplicativa() && Boolean.TRUE.equals(aSignSRV.isPresent(documentTitle))) {
						listaCode.add(DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE);
					} else if(qwf.getQueueEnum().equals(DocumentQueueEnum.DD_DA_FIRMARE) && utente.isGestioneApplicativa() && Boolean.TRUE.equals(aSignSRV.isPresent(documentTitle))) {
						listaCode.add(DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE_FEPA);
					}
					while (qwf.getQueueQueryObj().hasNext()) {
						final VWWorkObject wo = (VWWorkObject) qwf.getQueueQueryObj().next();
						LOGGER.info(VW_WORK_OBJECT_GET_SUBJECT + wo.getSubject());
						// scorro i risultati della query per verificare che ci sia un match con le Code
						// censite
						final DocumentQueueEnum dqe = qwf.getQueueEnum();
						// se dalla verifica non viene fuori un match positivo creo una raccolta con le
						// code non censite
						if (dqe.equals(DocumentQueueEnum.NON_CENSITO)) {
							nomiQueuesNonCensite.add(wo.getCurrentQueueName());
						}

						listaCode.add(dqe);
					}
				} else if (qwf.getRosterQueryObj() != null) {
					while (qwf.getRosterQueryObj().hasNext()) {
						final VWWorkObject wo = (VWWorkObject) qwf.getRosterQueryObj().next();
						LOGGER.info(VW_WORK_OBJECT_GET_SUBJECT + wo.getSubject());
						// scorro i risultati della query per verificare che ci sia un match con le Code
						// censite
						final DocumentQueueEnum dqe = qwf.getQueueEnum();
						// se dalla verifica non viene fuori un match positivo creo una raccolta con le
						// code non censite
						if (dqe.equals(DocumentQueueEnum.NON_CENSITO)) {
							nomiQueuesNonCensite.add(wo.getCurrentQueueName());
						}

						listaCode.add(dqe);
					}
				}
			}

			// recupero le 'n' code applicative associate a questo id
			dcaColl = codeApplicativeDAO.getQueueName(documentTitle, utente.getId(), utente.getIdUfficio(), connection);

			for (final DatiCodaApplicativaDTO dcaDTO : dcaColl) {
				QueueGroupEnum group = QueueGroupEnum.UFFICIO;
				final StatoLavorazioneEnum sle = StatoLavorazioneEnum.get(dcaDTO.getIdStatoLav());

				// <------ Parti I --------->
				// parte che decodifica il gruppo
				if (dcaDTO.getIdUtente() != null && dcaDTO.getIdUtente() != 0) {
					group = QueueGroupEnum.UTENTE;
					if (StatoLavorazioneEnum.isOneOf(sle, StatoLavorazioneEnum.ATTI, StatoLavorazioneEnum.RISPOSTA, StatoLavorazioneEnum.MOZIONE)) {
						group = QueueGroupEnum.ARCHIVIO;
					}
				}
				// <------ Fine Parte I --------->

				listaCode.addAll(DocumentQueueEnum.getQueue(sle, group));
			}

			if (listaCode.isEmpty()) {
				listaCode.add(DocumentQueueEnum.NESSUNA_CODA);
			}

			return new RecuperoCodeDTO(listaCode, nomiQueuesNonCensite);
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_CODE_MSG + documentTitle + " ): " + e.getMessage(), e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaFacadeSRV#
	 *      retrieveRicercaGenericaModalita().
	 */
	@Override
	public Collection<RicercaGenericaTypeEnum> retrieveRicercaGenericaModalita() {
		return EnumSet.allOf(RicercaGenericaTypeEnum.class);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaFacadeSRV#ricercaRapidaUtente
	 *      (java.lang.String, java.lang.Integer,
	 *      it.ibm.red.business.enums.RicercaGenericaTypeEnum,
	 *      it.ibm.red.business.enums.RicercaPerBusinessEntityEnum, boolean,
	 *      it.ibm.red.business.dto.UtenteDTO, boolean).
	 */
	@Override
	public RicercaResultDTO ricercaRapidaUtente(final String key, final Integer anno, final RicercaGenericaTypeEnum type, final RicercaPerBusinessEntityEnum entity,
			final boolean searchInContent, final UtenteDTO utente, final boolean orderbyInQuery) {
		Collection<MasterDocumentRedDTO> documenti = null;
		Collection<FascicoloDTO> fascicoli = null;
		Connection connection = null;
		IFilenetCEHelper fceh = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			// al contrario delle code la ricerca inizia con il recupero dei documenti dal
			// CE
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			if (RicercaPerBusinessEntityEnum.DOCUMENTI.equals(entity) || RicercaPerBusinessEntityEnum.DOCUMENTI_FASCICOLI.equals(entity)) {
				LOGGER.info("Ricerca Generica Documenti Start");

				final DocumentSet documents = fceh.ricercaGenericaUtente(key, anno, type, utente, searchInContent, orderbyInQuery);
				documenti = trasformToGenericDocContext(documents, utente, connection);

				LOGGER.info("Ricerca Generica Documenti Trasformati");

				if (searchInContent) {
					final PropertiesProvider pp = PropertiesProvider.getIstance();
					Integer nMax = null;
					if (pp.getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS) != null) {
						nMax = Integer.valueOf(pp.getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS));
					}
					final Collection<MasterDocumentRedDTO> allegatiRicercaTestuale = getAllegatiRicercaRapida(key, type, nMax, documenti.size(), utente.getIdAoo(), anno, fceh,
							connection);

					if (!CollectionUtils.isEmpty(allegatiRicercaTestuale)) {
						documenti.addAll(allegatiRicercaTestuale);
					}
				}

				if (!orderbyInQuery) {
					final List<MasterDocumentRedDTO> docList = new ArrayList<>(documenti);
					// implementa Comparable
					Collections.sort(docList, Collections.reverseOrder());
					documenti = docList;
				}
				LOGGER.info("Ricerca Generica Documenti Ordinati");
			}

			if (RicercaPerBusinessEntityEnum.FASCICOLI.equals(entity) || RicercaPerBusinessEntityEnum.DOCUMENTI_FASCICOLI.equals(entity)) {
				LOGGER.info("Ricerca Generica Fascicoli Start");

				final DocumentSet documents = fceh.ricercaGenericaFascicoliUtente(key, type, anno, utente.getIdAoo(), orderbyInQuery);
				LOGGER.info("Ricerca Generica Fascicoli End");
				fascicoli = transformFascicoli(utente, connection, documents);

				LOGGER.info("Ricerca Generica Fascicoli Trasformati");

				if (!orderbyInQuery) {
					final List<FascicoloDTO> fasList = new ArrayList<>(fascicoli);
					// implementa Comparable
					Collections.sort(fasList, Collections.reverseOrder());
					fascicoli = fasList;

				}
				LOGGER.info("Ricerca Generica Fascicoli Ordinati");
				LOGGER.info("Ricerca Generica Fascicoli Record Restituiti: " + fascicoli.size());
			}
		} catch (final Exception e) {
			throw new RedException(e);
		} finally {
			closeConnection(connection);
			popSubject(fceh);
		}

		final PropertiesProvider pp = PropertiesProvider.getIstance();
		final RicercaResultDTO res = new RicercaResultDTO(fascicoli, documenti);
		res.setSoglia(Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS)));

		return res;
	}

	private Collection<MasterDocumentRedDTO> getAllegatiRicercaRapida(final String stringaRicerca, final RicercaGenericaTypeEnum modalitaRicercaTestuale,
			final Integer inUtenteMaxNumeroElementi, final int numeroRisultatiRicercaDoc, final Long idAoo, final Integer anno, final IFilenetCEHelper fceh,
			final Connection con) {
		final Collection<MasterDocumentRedDTO> allegatiRicercaTestuale = new ArrayList<>();

		final PropertiesProvider pp = PropertiesProvider.getIstance();
		final DocumentSet allegatiFilenetRicercaTestuale = fceh.ricercaAllegatiGenerica(stringaRicerca, anno, modalitaRicercaTestuale);

		if (allegatiFilenetRicercaTestuale != null && !allegatiFilenetRicercaTestuale.isEmpty()) {
			Integer utenteMaxNumeroElementi = inUtenteMaxNumeroElementi;
			if (utenteMaxNumeroElementi == null) {
				final String maxStr = pp.getParameterByKey(PropertiesNameEnum.RICERCA_FULLTEXT_MAX_RESULTS);
				if (maxStr != null) {
					utenteMaxNumeroElementi = Integer.valueOf(maxStr);
				} else {
					utenteMaxNumeroElementi = 100;
				}
			}

			int maxAllegati = utenteMaxNumeroElementi - numeroRisultatiRicercaDoc;

			final Map<ContextTrasformerCEEnum, Object> context = new EnumMap<>(ContextTrasformerCEEnum.class);

			final Map<Long, String> mapTipologiaDocumento = lookupDAO.getDescTipoDocumento(idAoo.intValue(), con);
			context.put(ContextTrasformerCEEnum.IDENTIFICATIVI_TIPO_DOC, mapTipologiaDocumento);

			final Map<Long, String> mapTipoProcedimento = tipoProcedimentoDAO.getAllDesc(con);
			context.put(ContextTrasformerCEEnum.IDENTIFICATIVI_TIPO_PROC, mapTipoProcedimento);

			final Iterator<?> itAllegatiFilenetFullText = allegatiFilenetRicercaTestuale.iterator();
			while (itAllegatiFilenetFullText.hasNext() && maxAllegati > 0) {
				final Document allegatoFilenetRicercaTestuale = (Document) itAllegatiFilenetFullText.next();

				// Si estraggono i dati del protocollo dal documento principale, per impostarli
				// sull'allegato
				final Iterator<?> itDocPrincipaliFilenet = allegatoFilenetRicercaTestuale.get_ParentDocuments().iterator();
				while (itDocPrincipaliFilenet.hasNext()) {
					final Document docPrincipaleFilenet = (Document) itDocPrincipaliFilenet.next();

					final Boolean isCurrentVersion = (Boolean) TrasformerCE.getMetadato(docPrincipaleFilenet, PropertyNames.IS_CURRENT_VERSION);
					if (Boolean.TRUE.equals(isCurrentVersion)) {

						final MasterDocumentRedDTO allegatoRicercaTestuale = TrasformCE.transform(allegatoFilenetRicercaTestuale, docPrincipaleFilenet,
								TrasformerCEEnum.FROM_DOCUMENTO_TO_ALLEGATO_RICERCA_AVANZATA_CONTEXT, context);

						allegatiRicercaTestuale.add(allegatoRicercaTestuale);
						maxAllegati--;
						break;
					}
				}

			}
		}
		return allegatiRicercaTestuale;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.service.IRicercaSRV#redRicercaDocumentiWs
	 * (java.lang.String, java.lang.Integer,
	 * it.ibm.red.business.enums.CampoRicercaEnum, boolean,
	 * it.ibm.red.business.dto.UtenteDTO)
	 */
	@Override
	public final DocumentSet ricercaDocumentiDaWs(final String key, final Integer anno, final String campoRicerca, final boolean withContent, final UtenteDTO utente,
			final IFilenetCEHelper fceh) {
		DocumentSet documentiRicercaFilenet = null;

		try {

			documentiRicercaFilenet = fceh.ricercaGenericaUtente(key, anno, RicercaGenericaTypeEnum.QUALSIASI, utente, CampoRicercaEnum.get(campoRicerca), false, false, false,
					withContent);

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la ricerca generica di documenti sollecitata tramite web service: " + e.getMessage(), e);
			throw new RedException("Errore durante la ricerca generica di documenti sollecitata tramite web service: " + e.getMessage(), e);
		}

		return documentiRicercaFilenet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.service.IRicercaSRV#ricercaFascicoliDaWs(java.lang.
	 * String, it.ibm.red.business.dto.UtenteDTO)
	 */
	@Override
	public final DocumentSet ricercaFascicoliDaWs(final String key, final UtenteDTO utente) {
		DocumentSet fascicoliFilenet = null;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			fascicoliFilenet = ricercaGenericaFilenet(key, null, RicercaGenericaTypeEnum.ESATTA, RicercaPerBusinessEntityEnum.FASCICOLI, utente, fceh);

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la ricerca dei fascicoli sollecitata tramite web service: " + e.getMessage(), e);
			throw new RedException("Errore durante la ricerca dei fascicoli sollecitata tramite web service: " + e.getMessage(), e);
		} finally {
			popSubject(fceh);
		}

		return fascicoliFilenet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.ibm.red.business.service.IRicercaSRV#ricercaFaldoniDaWs(java.lang.String,
	 * it.ibm.red.business.dto.UtenteDTO)
	 */
	@Override
	public final DocumentSet ricercaFaldoniDaWs(final String key, final UtenteDTO utente) {
		DocumentSet faldoniFilenet = null;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			faldoniFilenet = ricercaGenericaFilenet(key, null, RicercaGenericaTypeEnum.ESATTA, RicercaPerBusinessEntityEnum.FALDONI, utente, fceh);

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la ricerca dei faldoni sollecitata tramite web service: " + e.getMessage(), e);
			throw new RedException("Errore durante la ricerca dei faldoni sollecitata tramite web service: " + e.getMessage(), e);
		} finally {
			popSubject(fceh);
		}

		return faldoniFilenet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.service.IRicercaSRV#ricercaAvanzataDaWs
	 * (java.lang.String, java.util.List, java.lang.String, java.lang.String,
	 * java.lang.String,
	 * it.ibm.red.webservice.model.documentservice.types.ricerca.TipoVersione)
	 */
	@Override
	public final DocumentSet ricercaAvanzataDaWs(final String classeDocumentale, final List<ParametriRicerca> parametriRicerca, final String inWhereCondition,
			final String fullTextParamsAnd, final String fullTextParamsOr, final TipoVersione tipoVersione, final UtenteDTO utente) {
		DocumentSet documentiRicercaFilenet = null;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final StringBuilder whereCondition = new StringBuilder();

			// Costruzione clausola WHERE con i parametri di ricerca in input -> START
			if (StringUtils.isNullOrEmpty(inWhereCondition)) {
				if (!CollectionUtils.isEmpty(parametriRicerca)) {
					for (final ParametriRicerca paramRicerca : parametriRicerca) {
						// Chiave
						whereCondition.append(" ").append(paramRicerca.getChiave()).append(" ");

						// Operatore
						final OperatoriRicerca operatoreParametroRicerca = paramRicerca.getOperatoreRicerca();
						whereCondition.append(OPERATORI_RICERCA_WS.get(operatoreParametroRicerca));

						// Valore
						if (OperatoriRicerca.LIKE.equals(operatoreParametroRicerca)) {
							whereCondition.append(" '%").append(paramRicerca.getValore()).append("%' ");
						} else {

							final Object valueObject = fceh.getValueObjectClass(paramRicerca.getChiave(), paramRicerca.getValore(), null, classeDocumentale);

							if (valueObject instanceof String) {
								whereCondition.append(" '").append(paramRicerca.getValore()).append("' ");

							} else if (valueObject instanceof Date) {
								final Date dataValore = new SimpleDateFormat(DateUtils.YYYY_MM_DD).parse(paramRicerca.getValore());
								whereCondition.append(" ").append(DateUtils.buildFilenetDataForSearch(dataValore, false)).append(" ");

							} else {
								whereCondition.append(" ").append(paramRicerca.getValore()).append(" ");
							}
						}

						// Operatore logico (AND/OR) per la condizione successiva (...!!!)
						if (paramRicerca.getOperatoreLogico() != null) {
							whereCondition.append(" ").append(paramRicerca.getOperatoreLogico().value()).append(" ");
						}
					}
				}

				// Gestione parametro Tipo Versione
				if (TipoVersione.CURRENT.equals(tipoVersione)) {
					whereCondition.append("AND ").append(PropertyNames.IS_CURRENT_VERSION).append(" = TRUE ");
				} else if (TipoVersione.RELEASED.equals(tipoVersione)) {
					whereCondition.append("AND ").append(PropertyNames.IS_CURRENT_VERSION).append(" = FALSE ");
				}
			} else {
				whereCondition.append(inWhereCondition);
			}
			// Costruzione clausola WHERE con i parametri di ricerca in input -> END

			// Costruzione clausola per la ricerca FULL TEXT -> START
			final StringBuilder fullTextQuery = new StringBuilder();

			if (!StringUtils.isNullOrEmpty(fullTextParamsAnd)) {
				getFullTextCondition(fullTextQuery, "AND", fullTextParamsAnd);

				if (!StringUtils.isNullOrEmpty(fullTextParamsOr)) {
					fullTextQuery.append(" AND ");
				}
			}

			if (!StringUtils.isNullOrEmpty(fullTextParamsOr)) {
				getFullTextCondition(fullTextQuery, "OR", fullTextParamsOr);
			}
			// Costruzione clausola per la ricerca FULL TEXT -> END

			documentiRicercaFilenet = fceh.ricercaAvanzataDaWs(classeDocumentale, fullTextQuery.toString(), whereCondition.toString(), null);

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la ricerca avanzata sollecitata tramite web service: " + e.getMessage(), e);
			throw new RedException("Errore durante la ricerca avanzata sollecitata tramite web service: " + e.getMessage(), e);
		} finally {
			popSubject(fceh);
		}

		return documentiRicercaFilenet;
	}

	/**
	 * @param query
	 * @param operatore
	 * @param fullTextParams
	 */
	private static void getFullTextCondition(final StringBuilder query, final String operatore, final String fullTextParams) {
		query.append("(");

		final List<String> fullTextAndList = Arrays.asList(fullTextParams.split(" ")).stream().map(a -> it.ibm.red.business.utils.StringUtils.duplicaApici(a))
				.collect(Collectors.toList());

		query.append(String.join(" " + operatore + " ", fullTextAndList));

		query.append(")");
	}
}