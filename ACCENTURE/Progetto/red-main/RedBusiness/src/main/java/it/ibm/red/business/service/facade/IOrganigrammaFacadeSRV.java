package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.persistence.model.Nodo;

/**
 * Facade del servizio di gestione organigramma.
 */
public interface IOrganigrammaFacadeSRV extends Serializable {

	/**
	 * Albero assegnatario per competenza per il documento in uscita.
	 * 
	 * mostra anche gli utenti
	 * 
	 * @param idUfficioUtente
	 * @param idAOO
	 * @param nodoSelected
	 * @return
	 */
	List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerCompetenzaUscita(UtenteDTO utente, NodoOrganigrammaDTO nodeToOpen);

	/**
	 * Albero assegnatario per competenza per il documento in ingresso.
	 * 
	 * mostra solo gli uffici
	 * 
	 * @param idUfficioUtente
	 * @param idAOO
	 * @param nodoSelected
	 * @return
	 */
	List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerCompetenzaIngresso(UtenteDTO utente, NodoOrganigrammaDTO nodeToOpen);

	/**
	 * @param idUfficioUtente
	 * @param nodeToOpen
	 * @param primoLivello
	 * @return
	 */
	List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerConoscenza(Long idUfficioUtente, NodoOrganigrammaDTO nodeToOpen, boolean primoLivello);

	/**
	 * @param idUfficioUtente
	 * @param nodeToOpen
	 * @return
	 */
	List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerFirma(Long idUfficioUtente, NodoOrganigrammaDTO nodeToOpen);

	/**
	 * @param idUfficioUtente
	 * @param nodeToOpen
	 * @return
	 */
	List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerSigla(Long idUfficioUtente, NodoOrganigrammaDTO nodeToOpen);

	/**
	 * @param idUfficio
	 * @param idAOO
	 * @return
	 */
	List<UfficioDTO> getSottoNodi(Long idUfficio, Long idAOO);

	/**
	 * organigramma completo di tutti gli uffici e senza utenti.
	 * 
	 * @param idUfficioUtente
	 * @param nodeToOpen
	 * @param connection
	 * @return ritorna l'organigramma completo di tutti gli uffici e senza utenti
	 */
	List<NodoOrganigrammaDTO> getFigliAlberoCompletoNoUtentiFromRoot(Long idUfficioUtente, NodoOrganigrammaDTO nodeToOpen);

	/**
	 * organigramma completo di tutti gli uffici e senza utenti.
	 * 
	 * @param idUfficioUtente
	 * @param nodeToOpen
	 * @param connection
	 * @return ritorna l'organigramma completo di tutti gli uffici e senza utenti
	 */
	List<NodoOrganigrammaDTO> getFigliAlberoCompletoNoUtenti(Long idUfficioUtente, NodoOrganigrammaDTO nodeToOpen);

	/**
	 * Metodo per il recupero della lista di utenti utilizzata dall'organigramma
	 * della response 'Storna a Utente'.
	 * 
	 * @param idUfficioUtente
	 * @param nodeToOpen
	 * @return
	 */
	List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerStornaAUtente(Long idUfficioUtente, NodoOrganigrammaDTO nodeToOpen);

	/**
	 * Metodo per la creazione dell'organigramma della response 'Assegna'.
	 * 
	 * @param idUfficioUtente
	 * @param nodeToOpen
	 * @return
	 */
	List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerAssegna(Long idUfficioUtente, NodoOrganigrammaDTO nodeToOpen);

	/**
	 * L'albero dei figli di un nodo visualizzando gli utenti.
	 * 
	 * Senza filtrare tra uffici e utenti che appartengono allo stesso ufficio
	 * Filtrando rispetto ai permessi
	 * 
	 * @param idUfficioUtente ufficio dell'utente che esegue l'operazione
	 * @param nodeToOpen      nodo da cui partire con l'analisi
	 * @param permesso        filtro degli utenti rispetto credenziale
	 */
	List<NodoOrganigrammaDTO> getFigliAlberoUtentiAssegnatariFilteredByPermesso(Long idUfficioUtente, NodoOrganigrammaDTO nodeToOpen, ResponsesRedEnum response);

	/**
	 * Recupera i figli di nodeToOpen.
	 * 
	 * Mostra solo gli uffici, Solo l'ispettorato dell'utente può essere espanso
	 * 
	 * 
	 * @param idUfficioUtente
	 * @param nodeToOpen        Nodo da aprire
	 * @param filtraIspettorati
	 * @return
	 */
	List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerRichiestaVisto(Long idUfficioUtente, NodoOrganigrammaDTO nodeToOpen, boolean filtraIspettorati);

	/**
	 * @param idUfficioUtente
	 * @param nodeToOpen
	 * @return
	 */
	List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerRichiestaVisto(Long idUfficioUtente, NodoOrganigrammaDTO nodeToOpen);

	/**
	 * Recupera anche gli utenti, filtrati per permesso Visto.
	 * 
	 * (Parte dal nodo dell'ufficio dell'utente)
	 * 
	 * 
	 * @param idUfficioUtente identificativo dell'ufficio
	 * @param nodeToOpen      nodo del'ufficio da aprire
	 * @return
	 */
	List<NodoOrganigrammaDTO> getFigliUfficioAssegnatarioPerVisto(Long idUfficioUtente, NodoOrganigrammaDTO nodeToOpen);

	/**
	 * 
	 * @param idNodoSelezionato - id nodo di partenza per calcolare il sotto livello
	 * @param utente            -
	 * @return
	 */
	List<NodoOrganigrammaDTO> getOrgForScrivania(NodoOrganigrammaDTO nodoDiPartenza, Long idUfficioUtente);

	/**
	 * Metodo per il calcolo del primo livello per la selezione del Delegante Libro
	 * Firma.
	 * 
	 * @param idNodoSelezionato - id nodo di partenza per calcolare il sotto livello
	 * @return
	 */
	List<NodoOrganigrammaDTO> getTreeSelezioneDelegante(NodoOrganigrammaDTO nodoDiPartenza);

	/**
	 * Metodo per il recupero del dirigente per un dato Ufficio.
	 * 
	 * @param idNodoSelezionato - id nodo di partenza per calcolare il sotto livello
	 * @return
	 */
	NodoOrganigrammaDTO getDirigenteFromNodo(NodoOrganigrammaDTO nodoDiPartenza);

	/**
	 * Metodo per il recupero degli utenti per un dato Ufficio, potendo scegliere se
	 * includere il Dirigente oppure no.
	 * 
	 * @param nodoDiPartenza
	 * @param withDirigente
	 * @return
	 */
	List<NodoOrganigrammaDTO> getUtentiFromNodo(NodoOrganigrammaDTO nodoDiPartenza, boolean withDirigente);

	/**
	 * Albero utilizzato nella ricerca.
	 * 
	 * @param idUfficioUtente
	 * @param nodeToOpen
	 * @return
	 */
	List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioOperazione(Long idUfficioUtente, NodoOrganigrammaDTO nodeToOpen, PermessiEnum permesso);

	/**
	 * Albero per la gestione del tipo operazione in fase di ricerca avanzata
	 * 
	 * @param idUfficioUtente
	 * @param nodeToOpen
	 * @param permesso
	 * @param conUtenti
	 * @return
	 */
	List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioOperazione(Long idUfficioUtente, NodoOrganigrammaDTO nodeToOpen, PermessiEnum permesso, boolean conUtenti);

	/**
	 * @param idAoo
	 * @param idUfficio
	 * @return listaNodi
	 */
	List<Nodo> getAlberaturaBottomUp(Long idAoo, Long idUfficio);

	/**
	 * Recupera il nodo UCP a partire dal nodo passatogli.
	 * 
	 * @param idUfficioUtente
	 * @return
	 */
	NodoOrganigrammaDTO getNodoUCP(Long idUfficioUtente);

	/**
	 * @param isEntrata
	 * @param idUfficioUtente
	 * @param nodeToOpen
	 * @param indirizzoEmail
	 * @return
	 */
	List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerCompetenzaIngressoNuovo(Boolean isEntrata, Long idUfficioUtente, NodoOrganigrammaDTO nodeToOpen,
			String indirizzoEmail, boolean consideraDisattivi);

	/**
	 * @param isEntrata
	 * @param idUfficioUtente
	 * @param nodeToOpen
	 * @return
	 */
	List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerCompetenzaIngressoNuovoSenzaUtenti(Boolean isEntrata, Long idUfficioUtente, NodoOrganigrammaDTO nodeToOpen, boolean consideraDisattivi);

	/**
	 * @param isEntrata
	 * @param idUfficioUtente
	 * @param nodeToOpen
	 * @param indirizzoEmail
	 * @return NodoOrganigrammaDTO
	 */
	List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerCompetenzaIngressoNuovoConUtenti(Boolean isEntrata,
			Long idUfficioUtente, NodoOrganigrammaDTO nodeToOpen, String indirizzoEmail, boolean consideraDisattivi);

	/**
	 * Restituisce i nodi figli dell'albero degli assegnatari per competenza in uscita.
	 * @param utente
	 * @param nodeToOpen
	 * @param visualizzaUtenti
	 * @param connection
	 * @return NodoOrganigrammaDTO
	 */
	List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerCompetenzaUscita(UtenteDTO utente,
			NodoOrganigrammaDTO nodeToOpen, boolean visualizzaUtenti, Connection connection);

	/**
	 * Restituisce i nodi figli dell'albero degli assegnatari per competenza in uscita creando
	 * una connessione e chiudendola alla fine del recupero.
	 * @param utente
	 * @param nodoSelected
	 * @param visualizzaUtenti
	 * @return NodoOrganigrammaDTO
	 */
	List<NodoOrganigrammaDTO> getFigliAlberoAssegnatarioPerCompetenzaUscita(UtenteDTO utente,
			NodoOrganigrammaDTO nodoSelected, boolean visualizzaUtenti);

	/**
	 * OIrganigramma completo a partire dall'ufficio idUtente.
	 * 
	 * @param idUfficioUtente
	 * @param nodeToOpen
	 * @return
	 */

	List<NodoOrganigrammaDTO> getFigliAlberoCompletoUtenti(Long idUfficioUtente, boolean consideraDisattivi,
			NodoOrganigrammaDTO nodeToOpen);

	



}
