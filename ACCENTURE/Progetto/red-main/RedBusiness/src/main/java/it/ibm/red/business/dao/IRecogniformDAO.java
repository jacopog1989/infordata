package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.DocumentoRecogniformDTO;

/**
 * 
 * @author a.dilegge
 *
 *	Dao gestione coda recogniform.
 */
public interface IRecogniformDAO extends Serializable {
	
	/**
	 * Ottiene il barcode da elaborare nella coda Recogniform per l'aoo.
	 * @param idAoo
	 * @param con
	 * @return lista di documenti Recogniform
	 */
	List<DocumentoRecogniformDTO> getBarcodeDaElaborare(Integer idAoo, Connection con);
	
	/**
	 * Ottiene i documenti nella coda Recogniform per il barcode,
	 * @param barcode
	 * @param con
	 * @return lista di documenti Recogniform
	 */
	List<DocumentoRecogniformDTO> getDocumentoByBarcode(String barcode, Connection con);
	
	/**
	 * Aggiorna lo stato del barcode.
	 * @param stato
	 * @param barcode
	 * @param con
	 */
	void updateStatoDocumento(Integer stato, String barcode, Connection con);
	
	/**
	 * Ottiene gli allegati da elaborare nella coda Recogniform per il documento principale con barcode dato.
	 * @param barcodePrincipale
	 * @param con
	 * @return lista di documenti Recogniform
	 */
	List<DocumentoRecogniformDTO> getAllegatiDaPrincipale(String barcodePrincipale, Connection con);
	
	/**
	 * Ottiene gli stati.
	 * @param con
	 * @return mappa stato descrizione
	 */
	Map<Integer, String> getStati(Connection con);

}