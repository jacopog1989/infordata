package it.ibm.red.business.persistence.model;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The Class Aoo.
 *
 * @author CPIERASC
 * 
 *         Entità che mappa la tabella AOO.
 */
public class Aoo implements Serializable {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Informazioni filenet.
	 */
	private AooFilenet aooFilenet;

	/**
	 * Codice.
	 */
	private final String codiceAoo;

	/**
	 * Descrizione.
	 */
	private final String descrizione;

	/**
	 * Identificativo.
	 */
	private final Long idAoo;

	/**
	 * Identificativo ente.
	 */
	private final BigDecimal idEnte;

	/**
	 * Ente.
	 */
	private Ente ente;

	/**
	 * Tipo protocollo utilizzato.
	 */
	private TipoProtocollo tipoProtocollo;

	/**
	 * Salva PIN.
	 */
	private final boolean salvaPin;

	/**
	 * Salva ALTEZZAFOOTER.
	 */
	private Integer altezzaFooter;

	/**
	 * Salva SPAZIATURAFIRMA.
	 */
	private Integer spaziaturaFirma;

	/**
	 * Tipo ricevuta utilizzato.
	 */
	private TipoRicevuta tipoRicevuta;

	/**
	 * Parametri AOO.
	 */
	private final Long parametriAOO;

	/**
	 * Identificativo nodo radice.
	 */
	private final Long idNodoRadice;

	/**
	 * Id logo.
	 */
	private final Integer idLogo;

	/**
	 * Max size entrata.
	 */
	private final int maxSizeEntrata;

	/**
	 * Max size uscita.
	 */
	private final int maxSizeUscita;

	/**
	 * Identificativo configurazione firma.
	 */
	private final int configurazioneFirma;

	/**
	 * Identificativo pk handler per firma.
	 */
	private PkHandler pkHandlerFirma;

	/**
	 * Identificativo pk handler per verifica.
	 */
	private PkHandler pkHandlerVerifica;

	/**
	 * Identificativo pk handler per timbro.
	 */
	private PkHandler pkHandlerTimbro;

	/**
	 * Flag mentieni allegati originale.
	 */
	private final int mantieniAllegatiOriginale;

	/**
	 * Flag inoltro mail.
	 */
	private final int flagInoltraMail;

	/**
	 * Flag forza refresh code.
	 */
	private final int flagForzaRefreshCode;
	
	/**
	 * visibilità faldoni..
	 */
	private String visFaldoni;

	/**
	 * Signer timbro.
	 */
	private String signerTimbro;

	/**
	 * Pin timbro.
	 */
	private String pinTimbro;

	/**
	 * Posta interoperabile esterna.
	 */
	private final int postaInteropEsterna;

	/**
	 * Identificativo ruolo delegato libro firma.
	 */
	private final Long idRuoloDelegatoLibroFirma;

	/**
	 * Ordinamento mail aoo asc.
	 */
	private int ordinamentoMailAOOAsc;

	/**
	 * Ribalta titolario.
	 */
	private boolean ribaltaTitolario;

	/**
	 * Autocomplete rubrica.
	 */
	private boolean autocompleteRubricaCompleta;

	/**
	 * Response stampa etichette.
	 */
	private boolean stampaEtichetteResponse;

	/**
	 * Flag protocolla e mantieni.
	 */
	private boolean protocollaEMantieni;

	/**
	 * Flag download custom excel.
	 */
	private boolean downloadCustomExcel;

	/**
	 * Flag inoltra response.
	 */
	private Boolean inoltraResponse;

	/**
	 * Flag rifiuta response.
	 */
	private Boolean rifiutaResponse;

	/**
	 * Flag file non sbustato.
	 */
	private boolean fileNonSbustato;

	/**
	 * Flag show dialog aoo.
	 */
	private boolean showDialogAoo;

	/**
	 * Flag timbro uscita aoo.
	 */
	private boolean timbroUscitaAoo;

	/**
	 * Flag estendi visibilità.
	 */
	private boolean isEstendiVisibilita;

	/**
	 * Flag disable use host only.
	 */
	private boolean disableUseHostOnly;

	/**
	 * Flag show riferimento storico.
	 */
	private boolean showRiferimentoStorico;

	/**
	 * Flag show ricerca nsd.
	 */
	private boolean showRicercaNsd;

	/**
	 * Flag seleziona tutti visto.
	 */
	private boolean showSelezionaTuttiVisto;

	/**
	 * Flag stampiglia allegati visibile.
	 */
	private boolean stampigliaAllegatiVisible;
	
	/**
	 * Flag conf PDFA per handler.
	 */
	private boolean confPDFAPerHandler;

	/**
	 * Identificativo nodo assegnazione indiretta.
	 */
	private final Long idNodoAssegnazioneIndiretta;

	/**
	 * Flag UCB.
	 */
	private final int flagUCB;

	/**
	 * Flag check univocità mail.
	 */
	private boolean checkUnivocitaMail;

	/**
	 * Max numero giorni ritardo notifica rubrica.
	 */
	private int maxGiorniNotificheRubrica;

	/**
	 * Numero massimo di giorni notifica calendario.
	 */
	private int maxGiorniNotificheCalendario;

	/**
	 * Numero massimo giorni notifica sottoscrizioni.
	 */
	private int maxGiorniNotificheSottoscrizioni;

	/**
	 * Numero giorni eliminazione on the fly.
	 */
	private int numGiorniEliminazioneContOnTheFlyJob;

	/**
	 * Casella mail ufficiale.
	 */
	private String casellaEmailUfficiale;
	
	/**
	 * Numero massimo notifiche non lette.
	 */
	private int maxNotificheNonLette;
	
	/**
	 * Definisce la strategia di firma.
	 */
	private int flagStrategiaFirma;

	/**
	 * Numero di minuti di delay per il processamento dei record da parte del job di verifica firma.
	 */
	private Integer verificaFirmaMinutiDelay;
	
	/**
	 * Flag per visualizzare la tab degli allacci nps
	 */
	private boolean showAllaccioNps;
	/**
	 * Flag per download dai sistemi esterni
	 */
	private boolean downloadSistemiEsterni;
	
	/**
	 * Flag associato alla postilla.
	 */
	private Integer postillaAttiva;
	
	/**
	 * Flag che stabilisce la presenza della postilla solo sulla prima pagina.
	 */
	private Integer postillaSoloPag1;

	/**
	 * Giorni notifica mancata spedizione.
	 */
	private Integer giorniNotificaMancataSpedizione;
	
	
	/**
	 * Giorni notifica mancata spedizione cartacea.
	 */
	private Integer giorniNotificaMancataSpedizioneCartacea;
	
	/**
	 * Id admin aoo 
	 */
	private Integer idAdminAoo;
	
	
	/**
	 * Costruttore.
	 * 
	 * @param inIdAoo               identificativo aoo
	 * @param inIdEnte              identificativo ente
	 * @param inCodiceAoo           codice aoo
	 * @param inDescrizione         descrizione aoo
	 * @param inSalvaPin            salva Pin
	 * @param inAltezzaFooter
	 * @param inSpaziaturaFirma
	 * @param parametriAOO
	 * @param idNodoRadice
	 * @param inIdLogo
	 * @param inMaxSizeEntrata
	 * @param inMaxSizeUscita
	 * @param inConfigurazioneFirma
	 * @param mantieniAllegatiOriginale
	 * @param flagInoltraMail
	 * @param inVisFaldoni
	 * @param flagForzaRefreshCode
	 * @param inPostaInteropEsterna
	 * @param idRuoloDelegatoLibroFirma
	 * @param showDialogAoo
	 * @param timbroUscitaAoo
	 * @param isEstendiVisibilita
	 * @param disableUseHostOnly
	 * @param showRiferimentoStorico
	 * @param showRicercaNsd
	 * @param showSelezionaTuttiVisto
	 * @param idNodoAssegnazioneIndiretta
	 * @param flagUCB
	 * @param flagFirmaAsincrona
	 * @param postillaAttiva
	 * @param postillaSoloPag1
	 */
	public Aoo(final Long inIdAoo, final BigDecimal inIdEnte, final String inCodiceAoo, final String inDescrizione, final boolean inSalvaPin, final Integer inAltezzaFooter,
			final Integer inSpaziaturaFirma, final Long parametriAOO, final Long idNodoRadice, final Integer inIdLogo, final int inMaxSizeEntrata, final int inMaxSizeUscita,
			final int inConfigurazioneFirma, final int mantieniAllegatiOriginale, final int flagInoltraMail, final String inVisFaldoni, final int flagForzaRefreshCode, 
			final int inPostaInteropEsterna, final Long idRuoloDelegatoLibroFirma, final boolean showDialogAoo, final boolean timbroUscitaAoo, final boolean isEstendiVisibilita, 
			final boolean disableUseHostOnly, final boolean showRiferimentoStorico, final boolean showRicercaNsd, final boolean showSelezionaTuttiVisto, 
			final Long idNodoAssegnazioneIndiretta, final int flagUCB, final int flagFirmaAsincrona, final Integer postillaAttiva, final Integer postillaSoloPag1) {
		this.idAoo = inIdAoo;
		this.idEnte = inIdEnte;
		this.codiceAoo = inCodiceAoo;
		this.descrizione = inDescrizione;
		this.salvaPin = inSalvaPin;
		this.altezzaFooter = inAltezzaFooter;
		this.spaziaturaFirma = inSpaziaturaFirma;
		this.parametriAOO = parametriAOO;
		this.idNodoRadice = idNodoRadice;
		this.idLogo = inIdLogo;
		this.maxSizeEntrata = inMaxSizeEntrata;
		this.maxSizeUscita = inMaxSizeUscita;
		this.configurazioneFirma = inConfigurazioneFirma;
		this.mantieniAllegatiOriginale = mantieniAllegatiOriginale;
		this.flagInoltraMail = flagInoltraMail;
		this.visFaldoni = inVisFaldoni;
		this.flagForzaRefreshCode = flagForzaRefreshCode;
		this.postaInteropEsterna = inPostaInteropEsterna;
		this.idRuoloDelegatoLibroFirma = idRuoloDelegatoLibroFirma;
		this.showDialogAoo = showDialogAoo;
		this.timbroUscitaAoo = timbroUscitaAoo;
		this.isEstendiVisibilita = isEstendiVisibilita;
		this.disableUseHostOnly = disableUseHostOnly;
		this.showRiferimentoStorico = showRiferimentoStorico;
		this.showRicercaNsd = showRicercaNsd;
		this.showSelezionaTuttiVisto = showSelezionaTuttiVisto;
		this.idNodoAssegnazioneIndiretta = idNodoAssegnazioneIndiretta;
		this.flagUCB = flagUCB;
		this.flagStrategiaFirma = flagFirmaAsincrona;
		this.postillaAttiva = postillaAttiva;
		this.postillaSoloPag1 = postillaSoloPag1;
	}

	/**
	 * Setter aoo filenet.
	 * 
	 * @param inAooFilenet aoo filenet
	 */
	public final void setAooFilenet(final AooFilenet inAooFilenet) {
		this.aooFilenet = inAooFilenet;
	}

	/**
	 * Setter ente.
	 * 
	 * @param inEnte ente
	 */
	public final void setEnte(final Ente inEnte) {
		this.ente = inEnte;
	}

	/**
	 * Setter tipologia protocollo.
	 * 
	 * @param inTipoProtocollo tipo protocollo
	 */
	public final void setTipoProtocollo(final TipoProtocollo inTipoProtocollo) {
		this.tipoProtocollo = inTipoProtocollo;
	}

	/**
	 * Getter id ente.
	 * 
	 * @return id ente
	 */
	public final BigDecimal getIdEnte() {
		return idEnte;
	}

	/**
	 * Getter tipo protocollo.
	 * 
	 * @return tipo protocollo
	 */
	public final TipoProtocollo getTipoProtocollo() {
		return tipoProtocollo;
	}

	/**
	 * Getter aoo filenet.
	 * 
	 * @return aoo filenet
	 */
	public final AooFilenet getAooFilenet() {
		return aooFilenet;
	}

	/**
	 * Getter coice aoo.
	 * 
	 * @return codice aoo
	 */
	public final String getCodiceAoo() {
		return codiceAoo;
	}

	/**
	 * Getter descrizione.
	 * 
	 * @return descrizione
	 */
	public final String getDescrizione() {
		return descrizione;
	}

	/**
	 * Getter identificativo aoo.
	 * 
	 * @return identificativo aoo
	 */
	public final Long getIdAoo() {
		return idAoo;
	}

	/**
	 * Getter ente.
	 * 
	 * @return ente
	 */
	public final Ente getEnte() {
		return ente;
	}

	/**
	 * Getter SALVA PIN.
	 * 
	 * @return salvaPin
	 */
	public final boolean isSalvaPin() {
		return salvaPin;
	}

	/**
	 * @return altezzaFooter
	 */
	public Integer getAltezzaFooter() {
		return altezzaFooter;
	}

	/**
	 * @param inAltezzaFooter
	 */
	public void setAltezzaFooter(final Integer inAltezzaFooter) {
		this.altezzaFooter = inAltezzaFooter;
	}

	/**
	 * @return spaziaturaFirma
	 */
	public Integer getSpaziaturaFirma() {
		return spaziaturaFirma;
	}

	/**
	 * @param inSpaziaturaFirma
	 */
	public void setSpaziaturaFirma(final Integer inSpaziaturaFirma) {
		this.spaziaturaFirma = inSpaziaturaFirma;
	}

	/**
	 * @return tipoRicevuta
	 */
	public TipoRicevuta getTipoRicevuta() {
		return tipoRicevuta;
	}

	/**
	 * @param inTipoRicevuta
	 */
	public void setTipoRicevuta(final TipoRicevuta inTipoRicevuta) {
		this.tipoRicevuta = inTipoRicevuta;
	}

	/**
	 * @return parametriAOO
	 */
	public Long getParametriAOO() {
		return parametriAOO;
	}

	/**
	 * @return idNodoRadice
	 */
	public Long getIdNodoRadice() {
		return idNodoRadice;
	}

	/**
	 * @return idLogo
	 */
	public Integer getIdLogo() {
		return idLogo;
	}

	/**
	 * @return maxSizeEntrata
	 */
	public int getMaxSizeEntrata() {
		return maxSizeEntrata;
	}

	/**
	 * @return maxSizeUscita
	 */
	public int getMaxSizeUscita() {
		return maxSizeUscita;
	}

	/**
	 * @return configurazioneFirma
	 */
	public int getConfigurazioneFirma() {
		return configurazioneFirma;
	}

	/**
	 * @return pkHandlerFirma
	 */
	public PkHandler getPkHandlerFirma() {
		return pkHandlerFirma;
	}

	/**
	 * @param inPkHandlerFirma
	 */
	public void setPkHandlerFirma(final PkHandler inPkHandlerFirma) {
		this.pkHandlerFirma = inPkHandlerFirma;
	}

	/**
	 * @return pkHandlerVerifica
	 */
	public PkHandler getPkHandlerVerifica() {
		return pkHandlerVerifica;
	}

	/**
	 * @param inPkHandlerVerifica
	 */
	public void setPkHandlerVerifica(final PkHandler inPkHandlerVerifica) {
		this.pkHandlerVerifica = inPkHandlerVerifica;
	}

	/**
	 * @return mantieniAllegatiOriginale
	 */
	public int getMantieniAllegatiOriginale() {
		return mantieniAllegatiOriginale;
	}

	/**
	 * @return flagInoltraMail
	 */
	public int getFlagInoltraMail() {
		return flagInoltraMail;
	}

	/**
	 * @return flagForzaRefreshCode
	 */
	public int getFlagForzaRefreshCode() {
		return flagForzaRefreshCode;
	}

	/**
	 * @return the visFaldoni
	 */
	public final String getVisFaldoni() {
		return visFaldoni;
	}

	/**
	 * @param visFaldoni the visFaldoni to set
	 */
	public final void setVisFaldoni(final String visFaldoni) {
		this.visFaldoni = visFaldoni;
	}

	/**
	 * @return pkHandlerTimbro
	 */
	public PkHandler getPkHandlerTimbro() {
		return pkHandlerTimbro;
	}

	/**
	 * @param pkHandlerTimbro
	 */
	public void setPkHandlerTimbro(final PkHandler pkHandlerTimbro) {
		this.pkHandlerTimbro = pkHandlerTimbro;
	}

	/**
	 * @return signerTimbro
	 */
	public String getSignerTimbro() {
		return signerTimbro;
	}

	/**
	 * @param signerTimbro
	 */
	public void setSignerTimbro(final String signerTimbro) {
		this.signerTimbro = signerTimbro;
	}

	/**
	 * @return pinTimbro
	 */
	public String getPinTimbro() {
		return pinTimbro;
	}

	/**
	 * @param pinTimbro
	 */
	public void setPinTimbro(final String pinTimbro) {
		this.pinTimbro = pinTimbro;
	}

	/**
	 * @return postaInteropEsterna
	 */
	public int getPostaInteropEsterna() {
		return postaInteropEsterna;
	}

	/**
	 * @return idRuoloDelegatoLibroFirma
	 */
	public Long getIdRuoloDelegatoLibroFirma() {
		return idRuoloDelegatoLibroFirma;
	}

	/**
	 * @return ordinamentoMailAOOAsc
	 */
	public int getOrdinamentoMailAOOAsc() {
		return ordinamentoMailAOOAsc;
	}

	/**
	 * Imposta l'ordinamento delle mail aoo.
	 * 
	 * @param ordinamentoMailAooAsc
	 */
	public void setOrdinamentoMailAOOAsc(final Integer ordinamentoMailAooAsc) {
		this.ordinamentoMailAOOAsc = ordinamentoMailAooAsc;
	}

	/**
	 * @return ribaltaTitolario
	 */
	public boolean isRibaltaTitolario() {
		return ribaltaTitolario;
	}

	/**
	 * @param ribaltaTitolario
	 */
	public void setRibaltaTitolario(final boolean ribaltaTitolario) {
		this.ribaltaTitolario = ribaltaTitolario;
	}

	/**
	 * @return autocompleteRubricaCompleta
	 */
	public boolean isAutocompleteRubricaCompleta() {
		return autocompleteRubricaCompleta;
	}

	/**
	 * Imposta l'autocomplete riferito alla rubrica completa.
	 * 
	 * @param autocompleteRubricaCompleta
	 */
	public void setAutocompleteRubricaCompleta(final boolean autocompleteRubricaCompleta) {
		this.autocompleteRubricaCompleta = autocompleteRubricaCompleta;
	}

	/**
	 * @return stampaEtichetteResponse
	 */
	public boolean getStampaEtichetteResponse() {
		return stampaEtichetteResponse;
	}

	/**
	 * Imposta il parametro di stampa etichetta.
	 * 
	 * @param stampaEtichetteResponse
	 */
	public void setStampaEtichetteResponse(final boolean stampaEtichetteResponse) {
		this.stampaEtichetteResponse = stampaEtichetteResponse;
	}

	/**
	 * Restituisce true se la dialog deve essere visualizzata, false altrimenti.
	 * 
	 * @return showDialogAoo
	 */
	public boolean getShowDialog() {
		return showDialogAoo;
	}

	/**
	 * @param showDialog
	 */
	public void setShowDialog(final boolean showDialog) {
		this.showDialogAoo = showDialog;
	}

	/**
	 * @return timbroUscitaAoo
	 */
	public boolean getTimbroUscitaAoo() {
		return timbroUscitaAoo;
	}

	/**
	 * @param timbroUscitaAoo
	 */
	public void setTimbroUscitaAoo(final boolean timbroUscitaAoo) {
		this.timbroUscitaAoo = timbroUscitaAoo;
	}

	/**
	 * @return isEstendiVisibilita
	 */
	public boolean getIsEstendiVisibilita() {
		return isEstendiVisibilita;
	}

	/**
	 * @param isEstendiVisibilita
	 */
	public void setIsEstendiVisibilita(final boolean isEstendiVisibilita) {
		this.isEstendiVisibilita = isEstendiVisibilita;
	}

	/**
	 * @return disableUseHostOnly
	 */
	public boolean getDisableUseHostOnly() {
		return disableUseHostOnly;
	}

	/**
	 * @param disableUseHostOnly
	 */
	public void setDisableUseHostOnly(final boolean disableUseHostOnly) {
		this.disableUseHostOnly = disableUseHostOnly;
	}

	/**
	 * @return showRiferimentoStorico
	 */
	public boolean getShowRiferimentoStorico() {
		return showRiferimentoStorico;
	}

	/**
	 * @param showRiferimentoStorico
	 */
	public void setShowRiferimentoStorico(final boolean showRiferimentoStorico) {
		this.showRiferimentoStorico = showRiferimentoStorico;
	}

	/**
	 * @return showRicercaNsd
	 */
	public boolean getShowRicercaNsd() {
		return showRicercaNsd;
	}

	/**
	 * @param showRicercaNsd
	 */
	public void setShowRicercaNsd(final boolean showRicercaNsd) {
		this.showRicercaNsd = showRicercaNsd;
	}

	/**
	 * @return showSelezionaTuttiVisto
	 */
	public boolean isShowSelezionaTuttiVisto() {
		return showSelezionaTuttiVisto;
	}

	/**
	 * @param showSelezionaTuttiVisto
	 */
	public void setShowSelezionaTuttiVisto(final boolean showSelezionaTuttiVisto) {
		this.showSelezionaTuttiVisto = showSelezionaTuttiVisto;
	}

	/**
	 * @return stampigliaAllegatiVisible
	 */
	public boolean getStampigliaAllegatiVisible() {
		return stampigliaAllegatiVisible;
	}

	/**
	 * @param stampigliaAllegatiVisible
	 */
	public void setStampigliaAllegatiVisible(final boolean stampigliaAllegatiVisible) {
		this.stampigliaAllegatiVisible = stampigliaAllegatiVisible;
	}

	/**
	 * @return protocollaEMantieni
	 */
	public boolean getProtocollaEMantieni() {
		return protocollaEMantieni;
	}

	/**
	 * @param protocollaEMantieni
	 */
	public void setProtocollaEMantieni(final boolean protocollaEMantieni) {
		this.protocollaEMantieni = protocollaEMantieni;
	}

	/**
	 * @returndownloadCustomExcel
	 */
	public boolean getDownloadCustomExcel() {
		return downloadCustomExcel;
	}

	/**
	 * @param downloadCustomExcel
	 */
	public void setDownloadCustomExcel(final boolean downloadCustomExcel) {
		this.downloadCustomExcel = downloadCustomExcel;
	}

	/**
	 * @return inoltraResponse
	 */
	public Boolean getInoltraResponse() {
		return inoltraResponse;
	}

	/**
	 * @param inoltraResponse
	 */
	public void setInoltraResponse(final Boolean inoltraResponse) {
		this.inoltraResponse = inoltraResponse;
	}

	/**
	 * @return rifiutaResponse
	 */
	public Boolean getRifiutaResponse() {
		return rifiutaResponse;
	}

	/**
	 * @param rifiutaResponse
	 */
	public void setRifiutaResponse(final Boolean rifiutaResponse) {
		this.rifiutaResponse = rifiutaResponse;
	}

	/**
	 * @return true se il file non è sbustato, false altrimenti
	 */
	public boolean getFileNonSbustato() {
		return fileNonSbustato;
	}

	/**
	 * @param fileNonSbustato
	 */
	public void setFileNonSbustato(final boolean fileNonSbustato) {
		this.fileNonSbustato = fileNonSbustato;
	}

	/**
	 * @return confPDFAPerHandler
	 */
	public boolean isConfPDFAPerHandler() {
		return confPDFAPerHandler;
	}

	/**
	 * @param confPDFAPerHandler
	 */
	public void setConfPDFAPerHandler(final boolean confPDFAPerHandler) {
		this.confPDFAPerHandler = confPDFAPerHandler;
	}

	/**
	 * @return idNodoAssegnazioneIndiretta
	 */
	public Long getIdNodoAssegnazioneIndiretta() {
		return idNodoAssegnazioneIndiretta;
	}

	/**
	 * @return true se l'aoo è un UCB, false altrimenti
	 */
	public int getFlagUCB() {
		return flagUCB;
	}

	/**
	 * @return checkUnivocitaMail
	 */
	public boolean getCheckUnivocitaMail() {
		return checkUnivocitaMail;
	}

	/**
	 * @param checkUnivocitaMail
	 */
	public void setCheckUnivocitaMail(final boolean checkUnivocitaMail) {
		this.checkUnivocitaMail = checkUnivocitaMail;
	}
	/**
	 * @return maxGiorniNotificheRubrica
	 */
	public int getMaxGiorniNotificheRubrica() {
		return maxGiorniNotificheRubrica;
	}

	/**
	 * @param maxGiorniNotificheRubrica
	 */
	public void setMaxGiorniNotificheRubrica(final int maxGiorniNotificheRubrica) {
		this.maxGiorniNotificheRubrica = maxGiorniNotificheRubrica;
	}

	/**
	 * @return maxGiorniNotificheCalendario
	 */
	public int getMaxGiorniNotificheCalendario() {
		return maxGiorniNotificheCalendario;
	}

	/**
	 * @param maxGiorniNotificheCalendario
	 */
	public void setMaxGiorniNotificheCalendario(final int maxGiorniNotificheCalendario) {
		this.maxGiorniNotificheCalendario = maxGiorniNotificheCalendario;
	}

	/**
	 * @return maxGiorniNotificheSottoscrizioni
	 */
	public int getMaxGiorniNotificheSottoscrizioni() {
		return maxGiorniNotificheSottoscrizioni;
	}

	/**
	 * @param maxGiorniNotificheSottoscrizioni
	 */
	public void setMaxGiorniNotificheSottoscrizioni(final int maxGiorniNotificheSottoscrizioni) {
		this.maxGiorniNotificheSottoscrizioni = maxGiorniNotificheSottoscrizioni;
	}

	/**
	 * @return numGiorniEliminazioneContOnTheFlyJob
	 */
	public int getNumGiorniEliminazioneContOnTheFlyJob() {
		return numGiorniEliminazioneContOnTheFlyJob;
	}

	/**
	 * @param numGiorniEliminazioneContOnTheFlyJob
	 */
	public void setNumGiorniEliminazioneContOnTheFlyJob(final int numGiorniEliminazioneContOnTheFlyJob) {
		this.numGiorniEliminazioneContOnTheFlyJob = numGiorniEliminazioneContOnTheFlyJob;
	}

	/**
	 * @return casellaEmailUfficiale
	 */
	public String getCasellaEmailUfficiale() {
		return casellaEmailUfficiale;
	}

	/**
	 * @param casellaEmailUfficiale
	 */
	public void setCasellaEmailUfficiale(final String casellaEmailUfficiale) {
		this.casellaEmailUfficiale = casellaEmailUfficiale;
	}

	/**
	 * @return the maxNotificheNonLette
	 */
	public int getMaxNotificheNonLette() {
		return maxNotificheNonLette;
	}

	/**
	 * @param maxNotificheNonLette
	 */
	public void setMaxNotificheNonLette(final int maxNotificheNonLette) {
		this.maxNotificheNonLette = maxNotificheNonLette;
	}
	
	/**
	 * @return verificaFirmaMinutiDelay
	 */
	public Integer getVerificaFirmaMinutiDelay() {
		return verificaFirmaMinutiDelay;
	}
	
	/**
	 * @param verificaFirmaMinutiDelay
	 */
	public void setVerificaFirmaMinutiDelay(Integer verificaFirmaMinutiDelay) {
		this.verificaFirmaMinutiDelay = verificaFirmaMinutiDelay;
	}
	
	/**
	 * @return showAllaccioNps
	 */
	public boolean isShowAllaccioNps() {
		return showAllaccioNps;
	}

	/**
	 * @param showAllaccioNps
	 */
	public void setShowAllaccioNps(boolean showAllaccioNps) {
		this.showAllaccioNps = showAllaccioNps;
	}
	 
	
	/**
	 * @return downloadSistemiEsterni
	 */
	public boolean isDownloadSistemiEsterni() {
		return downloadSistemiEsterni;
	}

	/**
	 * @param downloadSistemiEsterni
	 */
	public void setDownloadSistemiEsterni(boolean downloadSistemiEsterni) {
		this.downloadSistemiEsterni = downloadSistemiEsterni;
	}
	
	/**
	 * @return the giorniNotificaMancataSpedizione
	 */
	public Integer getGiorniNotificaMancataSpedizione() {
		return giorniNotificaMancataSpedizione;
	}
	
	/**
	 * @param giorniNotificaMancataSpedizione
	 */
	public void setGiorniNotificaMancataSpedizione(Integer giorniNotificaMancataSpedizione) {
		this.giorniNotificaMancataSpedizione = giorniNotificaMancataSpedizione;
	}
	
	/**
	 * @return the idAdminAoo
	 */
	public Integer getIdAdminAoo() {
		return idAdminAoo;
	}
	
	/**
	 * @param idAdminAoo
	 */
	public void setIdAdminAoo(Integer idAdminAoo) {
		this.idAdminAoo = idAdminAoo;
	}

	/**
	 * Restituisce il numero di giorni di mancata spedizione notifica legati alla notifica.
	 * @return numero giorni notifica mancata spedizione cartacea
	 */
	public Integer getGiorniNotificaMancataSpedizioneCartacea() {
		return giorniNotificaMancataSpedizioneCartacea;
	}

	/**
	 * Imposta il numero di giorni di mancata spedizione notifica legati alla notifica.
	 * @param giorniNotificaMancataSpedizioneCartacea
	 */
	public void setGiorniNotificaMancataSpedizioneCartacea(Integer giorniNotificaMancataSpedizioneCartacea) {
		this.giorniNotificaMancataSpedizioneCartacea = giorniNotificaMancataSpedizioneCartacea;
	}

	/**
	 * @return flagStrategiaFirma
	 */
	public int getFlagStrategiaFirma() {
		return flagStrategiaFirma;
	}

	/**
	 * Restituisce il flag associato alla postilla attiva.
	 * @return flag postilla attiva
	 */
	public Integer getPostillaAttiva() {
		return postillaAttiva;
	}

	/**
	 * Imposta il flag associato alla postilla attiva.
	 * @param postillaAttiva
	 */
	public void setPostillaAttiva(Integer postillaAttiva) {
		this.postillaAttiva = postillaAttiva;
	}

	/**
	 * Restituisce il flag associato alla postilla solo prima pagina.
	 * @return il flag associato alla postilla solo prima pagina
	 */
	public Integer getPostillaSoloPag1() {
		return postillaSoloPag1;
	}

	/**
	 * Imposta il flag associato alla postilla solo prima pagina.
	 * @param postillaSoloPag1
	 */
	public void setPostillaSoloPag1(Integer postillaSoloPag1) {
		this.postillaSoloPag1 = postillaSoloPag1;
	}

}