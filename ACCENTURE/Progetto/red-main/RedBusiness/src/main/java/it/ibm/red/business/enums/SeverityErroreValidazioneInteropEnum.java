package it.ibm.red.business.enums;

/**
 * Enum tipi di errore di validazione interoperabilità.
 */
public enum SeverityErroreValidazioneInteropEnum {

	/**
	 * Valore.
	 */
	WARNING,
	
	/**
	 * Valore.
	 */
	ERROR;
	
}
