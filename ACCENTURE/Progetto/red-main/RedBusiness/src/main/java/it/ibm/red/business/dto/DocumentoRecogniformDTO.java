package it.ibm.red.business.dto;

import java.util.List;

import javax.activation.DataHandler;

/**
 * DTO documento recogniform.
 */
public class DocumentoRecogniformDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 7654902749787970528L;

	/**
	 * Flag PRINCIPALE.
	 */
	public static final int PRINCIPALEFLAG = 0;

	/**
	 * Flag ALLEGATO.
	 */
	public static final int ALLEGATOFLAG = 1;
	

	/**
	 * Identificativo Recogniform.
	 */
	private int idRecogniform;

	/**
	 * Oggetto.
	 */
	private String oggetto;

	/**
	 * Mittente.
	 */
	private String mittente;

	/**
	 * Protocollo mittente.
	 */
	private String protocolloMittente;

	/**
	 * Barcode.
	 */
	private String barcode;

	/**
	 * Gestore dati.
	 */
	private transient DataHandler dataHandler;

	/**
	 * Ufficio scansione.
	 */
	private String ufficioScanzione;

	/**
	 * Stato.
	 */
	private int stato;

	/**
	 * Allegati.
	 */
	private List<DocumentoRecogniformDTO> allegati;
	
	/**
	 * Nome del file.
	 */
	private String nomeFile;

	/**
	 * Mime type file.
	 */
	private String mimetype;

	/**
	 * Idenificativo ufficio.
	 */
	private int idnodo;

	/**
	 * Barcode documento principale.
	 */
	private String barcodeDocPrincipale;

	/**
	 * Restituisce la lista degli allegati.
	 * @return allegati
	 */
	public List<DocumentoRecogniformDTO> getAllegati() {
		return allegati;
	}

	/**
	 * Imposta la lista degli allegati.
	 * @param allegati
	 */
	public void setAllegati(final List<DocumentoRecogniformDTO> allegati) {
		this.allegati = allegati;
	}

	/**
	 * Restituisce l'id recogniform.
	 * @return
	 */
	public int getIdRecogniform() {
		return idRecogniform;
	}
	/**
	 * @param idRecogniform idRecogniform da impostare
	 */
	public void setIdRecogniform(final int idRecogniform) {
		this.idRecogniform = idRecogniform;
	}

	/**
	 * Restituisce l'oggetto.
	 * @return oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}
	/**
	 * @param oggetto oggetto da impostare
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}

	/**
	 * Imposta il mittente.
	 * @return mittente
	 */
	public String getMittente() {
		return mittente;
	}
	/**
	 * @param mittente mittente da impostare
	 */
	public void setMittente(final String mittente) {
		this.mittente = mittente;
	}

	/**
	 * Restituisce il protocollo mittente.
	 * @return protocollo mittente
	 */
	public String getProtocolloMittente() {
		return protocolloMittente;
	}
	/**
	 * @param protocolloMittente protocolloMittente da impostare
	 */
	public void setProtocolloMittente(final String protocolloMittente) {
		this.protocolloMittente = protocolloMittente;
	}

	/**
	 * Restituisce il barcode.
	 * @return barcode
	 */
	public String getBarcode() {
		return barcode;
	}
	/**
	 * @param barcode barcode da impostare
	 */
	public void setBarcode(final String barcode) {
		this.barcode = barcode;
	}

	/**
	 * Restituisce il dataHandler.
	 * @return dataHandler
	 */
	public DataHandler getDataHandler() {
		return dataHandler;
	}
	/**
	 * @param dataHandler dataHandler da impostare
	 */
	public void setDataHandler(final DataHandler dataHandler) {
		this.dataHandler = dataHandler;
	}

	/**
	 * Restituisce l'ufficio di scansione.
	 * @return ufficio scansione
	 */
	public String getUfficioScanzione() {
		return ufficioScanzione;
	}
	/**
	 * @param ufficioScanzione ufficioScanzione da impostare
	 */
	public void setUfficioScanzione(final String ufficioScanzione) {
		this.ufficioScanzione = ufficioScanzione;
	}

	/**
	 * Restituisce lo stato.
	 * @return stato
	 */
	public int getStato() {
		return stato;
	}
	/**
	 * @param stato stato da impostare
	 */
	public void setStato(final int stato) {
		this.stato = stato;
	}

	/**
	 * Restituisce il nome del file.
	 * @return nome file
	 */
	public String getNomeFile() {
		return nomeFile;
	}
	/**
	 * @param nomeFile nomeFile da impostare
	 */
	public void setNomeFile(final String nomeFile) {
		this.nomeFile = nomeFile;
	}

	/**
	 * Restituisce il mime type del file.
	 * @return
	 */
	public String getMimetype() {
		return mimetype;
	}
	/**
	 * @param mimetype mimetype da impostare
	 */
	public void setMimetype(final String mimetype) {
		this.mimetype = mimetype;
	}

	/**
	 * Restituisce l'id del nodo.
	 * @return id nodo
	 */
	public int getIdnodo() {
		return idnodo;
	}
	/**
	 * @param idnodo idnodo da impostare
	 */
	public void setIdnodo(final int idnodo) {
		this.idnodo = idnodo;
	}

	/**
	 * Restituisce il barcode del documento principale.
	 * @return barcode documento principale
	 */
	public String getBarcodeDocPrincipale() {
		return barcodeDocPrincipale;
	}
	/**
	 * @param barcodeDocPrincipale barcodeDocPrincipale da impostare
	 */
	public void setBarcodeDocPrincipale(final String barcodeDocPrincipale) {
		this.barcodeDocPrincipale = barcodeDocPrincipale;
	}
	
	/**
	 * @see java.lang.Object#toString().
	 */
	@Override
	public String toString() {
		return "DocumentoRecogniform [oggetto=" + oggetto
				 + ", mittente=" + mittente
				 + ", protocolloMittente="
				+ protocolloMittente
				+ ", barcode="
				+ barcode + ", " 
				 + ", dataHandler=" + dataHandler
				+ ", ufficioScanzione=" + ufficioScanzione + ", stato=" + stato
				+ "]";
	}
}
