package it.ibm.red.business.dto;

/**
 * DTO errore validazione.
 */
public class ErroreValidazioneDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -5874981949831935144L;

    /**
	 * codice.
	 */
	private Integer codice;
	
    /**
	 * descrizione.
	 */
	private final String descrizione;
	
    /**
	 * severity.
	 */
	private String severity;

	/**
	 * @param inDescrizione
	 * @param inSeverity
	 */
	public ErroreValidazioneDTO(final String inDescrizione, final String inSeverity) {
		descrizione = inDescrizione;
		severity = inSeverity;
	}
	
	/**
	 * @param inCodice
	 * @param inDescrizione
	 */
	public ErroreValidazioneDTO(final Integer inCodice, final String inDescrizione) {
		codice = inCodice;
		descrizione = inDescrizione;
	}
	
	/**
	 * @param inCodice
	 * @param inDescrizione
	 * @param inSeverity
	 */
	public ErroreValidazioneDTO(final Integer inCodice, final String inDescrizione, final String inSeverity) {
		codice = inCodice;
		descrizione = inDescrizione;
		severity = inSeverity;
	}
	
	/**
	 * @return
	 */
	public Integer getCodice() {
		return codice;
	}
	
	/**
	 * @return
	 */
	public String getDescrizione() {
		return descrizione;
	}
	
	/**
	 * @return
	 */
	public String getSeverity() {
		return severity;
	}
}
