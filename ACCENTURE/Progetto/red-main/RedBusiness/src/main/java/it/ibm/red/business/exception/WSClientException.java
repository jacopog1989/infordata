package it.ibm.red.business.exception;

/**
 * Exception generica client WS.
 */
public class WSClientException extends RedException {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Codice.
	 */
	private final int code;
	
	/**
	 * Costruttore.
	 * @param message
	 * @param code
	 */
	public WSClientException(final String message, final int code) {
		super(message);
		this.code = code;
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param e	eccezione
	 */
	public WSClientException(final Exception e) {
		super(e);
		this.code = -1;
	}

	/**
	 * Costruttore.
	 * 
	 * @param msg	messaggio
	 */
	public WSClientException(final String msg) {
		super(msg);
		this.code = -1;
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param msg	messaggio
	 * @param e		eccezione root
	 */
	public WSClientException(final String msg, final Exception e) {
		super(msg, e);
		this.code = -1;
	}

	/**
	 * Restituisce il codice errore.
	 * @return codice
	 */
	public int getCode() {
		return code;
	}
}
