package it.ibm.red.business.utils;

import org.apache.commons.lang3.SystemUtils;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Log style.
 */
public class LogStyleNotNull extends ToStringStyle {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @see ToStringStyle.MULTI_LINE_STYLE.
	 */
	public LogStyleNotNull() {
        super();
        this.setContentStart("[");
        this.setFieldSeparator(SystemUtils.LINE_SEPARATOR + "  ");
        this.setFieldSeparatorAtStart(true);
        this.setContentEnd(SystemUtils.LINE_SEPARATOR + "]");
    }

	/**
	 * Stampa solo valori non-nulli nella "toString".
	 */
	@Override
	public void append(final StringBuffer buffer, final String fieldName, final Object value, final Boolean fullDetail) {
		boolean valueNotNull = (value != null);
		boolean valueNotString = !(value instanceof String);

		if (valueNotNull && (valueNotString || !"".equals((((String) value).trim())))) {
    		super.append(buffer, fieldName, value, fullDetail);
	    } 
	}
}
