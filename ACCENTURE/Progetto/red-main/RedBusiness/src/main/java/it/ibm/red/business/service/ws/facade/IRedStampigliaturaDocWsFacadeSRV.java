package it.ibm.red.business.service.ws.facade;

import java.io.Serializable;

import it.ibm.red.business.dto.DocumentoWsDTO;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.webservice.model.documentservice.types.messages.RedInserisciApprovazioneType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedInserisciCampoFirmaType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedInserisciIdType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedInserisciPostillaType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedInserisciProtEntrataType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedInserisciProtUscitaType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedInserisciWatermarkType;

/**
 * The Interface IRedStampigliaturaDocWsFacadeSRV.
 *
 * @author m.crescentini
 * 
 *         Facade servizio stampigliatura documenti da web service.
 */
public interface IRedStampigliaturaDocWsFacadeSRV extends Serializable {
	
	/**
	 * Stampiglia il protocollo in entrata sul documento (file PDF).

	 * @param request
	 * @return
	 */
	DocumentoWsDTO redStampigliaProtocolloEntrata(RedWsClient client, RedInserisciProtEntrataType requestStampigliaProtEntrata);

	
	/**
	 * Stampiglia il protocollo in uscita sul documento (file PDF).
	 * 
	 * @param request
	 * @return
	 */
	DocumentoWsDTO redStampigliaProtocolloUscita(RedWsClient client, RedInserisciProtUscitaType requestStampigliaProtUscita);


	/**
	 * Inserisce un campo firma vuoto nel documento (file PDF).
	 * 
	 * @param client
	 * @param requestInserisciCampoFirma
	 * @return
	 */
	DocumentoWsDTO redInserisciCampoFirma(RedWsClient client, RedInserisciCampoFirmaType requestInserisciCampoFirma);


	/**
	 * Stampiglia l'ID del documento sul documento (file PDF).
	 * 
	 * @param client
	 * @param requestInserisciId
	 * @return
	 */
	DocumentoWsDTO redStampigliaIdDocumento(RedWsClient client, RedInserisciIdType requestInserisciId);


	/**
	 * Stampiglia la postilla sul documento (file PDF).
	 * 
	 * @param client
	 * @param requestInserisciPostilla
	 * @return
	 */
	DocumentoWsDTO redStampigliaPostilla(RedWsClient client, RedInserisciPostillaType requestInserisciPostilla);


	/**
	 * Stampiglia il testo come watermark (e.g. "ANNULLATO") sul documento (file PDF).
	 * 
	 * @param client
	 * @param request
	 * @return
	 */
	DocumentoWsDTO redStampigliaWatermark(RedWsClient client, RedInserisciWatermarkType request);


	/**
	 *  Stampiglia un'approvazione sul documento (file PDF).
	 * 
	 * @param client
	 * @param request
	 * @return
	 */
	DocumentoWsDTO redStampigliaApprovazione(RedWsClient client, RedInserisciApprovazioneType request);

}