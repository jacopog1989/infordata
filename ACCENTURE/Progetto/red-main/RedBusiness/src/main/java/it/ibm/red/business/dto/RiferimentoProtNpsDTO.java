package it.ibm.red.business.dto;

/**
 * @author VINGENITO
 *
 */
public class RiferimentoProtNpsDTO extends AbstractDTO{
	
	/**
	 * Serial version UID
	 */
	private static final long serialVersionUID = 6717668339807970973L;
	
	/**
	 * Identificativo Aoo
	 */
	private Long idAoo;
	
 
	/**
	 * Anno protocollo Nps
	 */
	private Integer annoProtocolloNps;
	
	/**
	 * Oggetto protocollo Nps
	 */
	private String oggettoProtocolloNps;
	
	/**
	 * Numero protocollo Nps
	 */
	private Integer numeroProtocolloNps;	
	
	/**
	 * Flag che indica se il documento è verificato
	 */
	private boolean verificato;
	   
	
	/**
	 * Flag che indica se dsisabilitare o meno i button
	 */
	private boolean disabled;

	/**
	 * Document Title uscita
	 */
	private String documentTitleUscita;
	
	/**
	 * Identificativo fascicolo procedimentale
	 */
	private String idFascProcedimentale;
	
	/**
	 * Identificativo documento per download
	 */
	private String idDocumentoDownload;
 
	/**
	 * Identificativo documento uscita tab fascicoli
	 */
	private Integer idDocumentoUscita;
	

	/**
	 * Costruttore
	 */
	public RiferimentoProtNpsDTO() {}
	
	/**
	 * Costruttore
	 */
	public RiferimentoProtNpsDTO(Integer annoProtocolloNps,Integer numeroProtocolloNps,String oggettoProtocolloNps,String idDocumentoDownload) {
		this.annoProtocolloNps = annoProtocolloNps; 
		this.numeroProtocolloNps = numeroProtocolloNps;
		this.oggettoProtocolloNps = oggettoProtocolloNps;
		this.idDocumentoDownload = idDocumentoDownload;
	}
	
	/**
	 * @return the annoProtocolloNps
	 */
	public Integer getAnnoProtocolloNps() {
		return annoProtocolloNps;
	}

	/**
	 * @param annoProtocolloNps the annoProtocolloNps to set
	 */
	public void setAnnoProtocolloNps(Integer annoProtocolloNps) {
		this.annoProtocolloNps = annoProtocolloNps;
	}

	/**
	 * @return the oggettoProtocolloNps
	 */
	public String getOggettoProtocolloNps() {
		return oggettoProtocolloNps;
	}

	/**
	 * @param oggettoProtocolloNps the oggettoProtocolloNps to set
	 */
	public void setOggettoProtocolloNps(String oggettoProtocolloNps) {
		this.oggettoProtocolloNps = oggettoProtocolloNps;
	}

	/**
	 * @return the numeroProtocolloNps
	 */
	public Integer getNumeroProtocolloNps() {
		return numeroProtocolloNps;
	}

	/**
	 * @param numeroProtocolloNps the numeroProtocolloNps to set
	 */
	public void setNumeroProtocolloNps(Integer numeroProtocolloNps) {
		this.numeroProtocolloNps = numeroProtocolloNps;
	}

	/**
	 * @return the verificato
	 */
	public boolean isVerificato() {
		return verificato;
	}

	/**
	 * @param verificato the verificato to set
	 */
	public void setVerificato(boolean verificato) {
		this.verificato = verificato;
	}
  
	/**
	 * @return the disabled
	 */
	public boolean isDisabled() {
		return disabled;
	}

	/**
	 * @param disabled the disabled to set
	 */
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
	 
	/**
	 * @return the annoProtocolloNps
	 */
	public Long getIdAoo() {
		return idAoo;
	}
	
	/**
	 * @param idAoo the idAoo to set
	 */
	public void setIdAoo(Long idAoo) {
		this.idAoo = idAoo;
	}
	
	/**
	 * @return the documentTitleUscita
	 */
	public String getDocumentTitleUscita() {
		return documentTitleUscita;	
	}
	
	/**
	 * @param documentTitleUscita the documentTitleUscita to set
	 */
	public void setDocumentTitleUscita(String documentTitleUscita) {
		this.documentTitleUscita = documentTitleUscita;
	}
	
	/**
	 * @return the idFascProcedimentale
	 */
	public String getIdFascProcedimentale() {
		return idFascProcedimentale;
	}

	/**
	 * @param idFascProcedimentale the idFascProcedimentale to set
	 */
	public void setIdFascProcedimentale(String idFascProcedimentale) {
		this.idFascProcedimentale = idFascProcedimentale;
	}
	
	/**
	 * @return the idDocumentoDownload
	 */
	public String getIdDocumentoDownload() {
		return idDocumentoDownload;
	}

	/**
	 * @param idDocumentoDownload the idDocumentoDownload to set
	 */
	public void setIdDocumentoDownload(String idDocumentoDownload) {
		this.idDocumentoDownload = idDocumentoDownload;
	}
	
	/**
	 * @return the idDocumentoUscita
	 */
	public Integer getIdDocumentoUscita() {
		return idDocumentoUscita;
	}

	/**
	 * @param idDocumentoUscita the idDocumentoUscita to set
	 */
	public void setIdDocumentoUscita(Integer idDocumentoUscita) {
		this.idDocumentoUscita = idDocumentoUscita;
	}
}