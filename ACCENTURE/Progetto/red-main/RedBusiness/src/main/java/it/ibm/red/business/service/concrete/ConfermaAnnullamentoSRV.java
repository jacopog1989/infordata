package it.ibm.red.business.service.concrete;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumMap;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.PEDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ContextTrasformerPEEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.TrasformPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.provider.ServiceTaskProvider.ServiceTask;
import it.ibm.red.business.service.IConfermaAnnullamentoSRV;
import it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV;

/**
 * Service che gestisce la conferma annullamento.
 */
@Service
@Component
public class ConfermaAnnullamentoSRV extends AbstractService implements IConfermaAnnullamentoSRV {

	/**
	 * Seriale.
	 */
	private static final long serialVersionUID = 8447337068295228612L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ConfermaAnnullamentoSRV.class.getName());
	
	/**
	 * Messaggio annullamento default.
	 */
	private static final String MOTIVAZIONE_ANNULLAMENTO_DEFAULT = "Richiesta annullamento";

	/**
	 * Messaggio annullamento default.
	 */
	private static final String PROVVEDIMENTO_ANNULLAMENTO_DEFAULT = "Richiesta annullamento";
	
	/**
	 * Service.
	 */
	@Autowired
	private IDocumentoRedFacadeSRV documentoRedSRV;
	
	/**
	 * Properties.
	 */
	private PropertiesProvider pp;

	/**
	 * Esegue tutta la logica da eseguire al post construct.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.service.facade.IConfermaAnnullamentoFacadeSRV#confermaAnnullamento(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	@ServiceTask(name = "confermaAnnullamento")
	public EsitoOperazioneDTO confermaAnnullamento(final UtenteDTO utente, final String wobNumber) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		try {
			
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			
			final VWWorkObject workObject = fpeh.getWorkFlowByWob(wobNumber, true);
			
			final EnumMap<ContextTrasformerPEEnum, Object> context = new EnumMap<>(ContextTrasformerPEEnum.class);
			context.put(ContextTrasformerPEEnum.UCB, utente.isUcb());
			PEDocumentoDTO wfDTO = TrasformPE.transform(workObject, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO_CONTEXT, context);	
			
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			
			final String[] parametriDocumento = new String[] { pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY), pp.getParameterByKey(PropertiesNameEnum.SOTTOCATEGORIA_DOCUMENTO_USCITA) };
			final Document doc = fceh.getDocumentByIdGestionale(wfDTO.getIdDocumento(), Arrays.asList(parametriDocumento), null, 
					utente.getIdAoo().intValue(), null, pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));
			
			final Integer numeroDocumento = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
			esito.setIdDocumento(numeroDocumento);
			
			String motivazione = MOTIVAZIONE_ANNULLAMENTO_DEFAULT;
			String provvedimento = PROVVEDIMENTO_ANNULLAMENTO_DEFAULT;
			final SottoCategoriaDocumentoUscitaEnum sottoCategoria = SottoCategoriaDocumentoUscitaEnum
					.get((Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.SOTTOCATEGORIA_DOCUMENTO_USCITA));
			if (sottoCategoria != null) {
				motivazione = motivazione + " " + sottoCategoria.getDescrizione();
				provvedimento = provvedimento + " " + sottoCategoria.getDescrizione();
			}
			//Annullamento documento in uscita (stesso servizio di annulla Procedimento da ricerca con motivazione di default, senza passare per maschera utente)
			documentoRedSRV.annullaDocumentoOAnnullaProtocollo(wfDTO.getIdDocumento(), motivazione, provvedimento, utente);
			
			//Riattivamento di tutte le entrate allacciate al documento chiuso
			documentoRedSRV.riattivaDocIngressoAfterChiusuraRisposta(wfDTO.getIdDocumento(), utente);
			
			esito.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
			esito.setEsito(true);
			
		} catch (final Exception e) {
			LOGGER.error("Errore durante la conferma dell'annullamento del documento con wobNumber: " + wobNumber, e);
			esito.setEsito(false);
			esito.setNote("Errore durante la conferma dell'annullamento del documento.");
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}
		
		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IConfermaAnnullamentoFacadeSRV#confermaAnnullamento(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection).
	 */
	@Override
	@ServiceTask(name = "confermaAnnullamento")
	public Collection<EsitoOperazioneDTO> confermaAnnullamento(final UtenteDTO utente, final Collection<String> wobNumber) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();
		
		for (final String w : wobNumber) {
			esiti.add(confermaAnnullamento(utente, w));
		}
		
		return esiti;
	}
	
}