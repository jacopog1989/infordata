package it.ibm.red.business.helper.signing;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Base64;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;

import org.apache.commons.io.IOUtils;
import org.springframework.util.CollectionUtils;

import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfAnnotation;
import com.itextpdf.text.pdf.PdfFormField;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfString;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.EsitoDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.SignerDTO;
import it.ibm.red.business.dto.SignerInfoDTO;
import it.ibm.red.business.dto.VerifyInfoDTO;
import it.ibm.red.business.enums.AccessPermissionsEnum;
import it.ibm.red.business.enums.EncodingEnum;
import it.ibm.red.business.enums.EsitoVerificaFirmaEnum;
import it.ibm.red.business.enums.FontFamilyEnum;
import it.ibm.red.business.enums.FontStyleEnum;
import it.ibm.red.business.enums.ModeEnum;
import it.ibm.red.business.enums.PositionEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.SignErrorEnum;
import it.ibm.red.business.enums.SignTypeEnum;
import it.ibm.red.business.enums.TextAlignEnum;
import it.ibm.red.business.enums.TipoDocumentoMockEnum;
import it.ibm.red.business.enums.ValueMetadatoVerificaFirmaEnum;
import it.ibm.red.business.exception.AdobeException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.exception.SignException;
import it.ibm.red.business.helper.pdf.DataManager;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IMockSRV;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.ServerUtils;
import it.ibm.red.business.utils.StringUtils;
import it.pkbox.client.Envelope;
import it.pkbox.client.InvalidEnvelopeException;
import it.pkbox.client.InvalidPinException;
import it.pkbox.client.InvalidSignerException;
import it.pkbox.client.PDFSignatureInfo;
import it.pkbox.client.PKBox;
import it.pkbox.client.PKBoxException;
import it.pkbox.client.SecurePINCache;
import it.pkbox.client.Utils;
import it.pkbox.client.VerifyInfoEx;
import it.pkbox.client.XMLEnvelope;
import it.pkbox.client.XMLVerifyInfoEx;

/**
 * The Class SignHelper.
 *
 * @author CPIERASC
 * 
 *         Classe per la gestione della firma remota e la verifica.
 */
public final class SignHelper implements Serializable {

	/**
	 * Tag New.
	 */
	private static final String NEW = "<new>";

	/**
	 * Immagine.
	 */
	private static final String TIMBRO_NUMERO_DOCUMENTO = "timbro_numero_documento.png";

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SignHelper.class.getName());

	/**
	 * Messaggio errore modifica immagine timbro per PDF-A.
	 */
	private static final String ERROR_MODIFICA_IMMAGINE_TIMBRO_MSG = "Non è stato possibile modificare l'immagine del timbro per PDF-A";

	/**
	 * Messaggio errore avvio transazione.
	 */
	private static final String ERRORE_AVVIO_TRANSAZIONE_FIRMA = "Errore durante l'avvio della transazione di firma multipla: ";

	/**
	 * Pkbox.
	 */
	private transient PKBox pkbox;

	/**
	 * Envelope.
	 */
	private transient Envelope envelope;
	
	/**
	 * Envelope per gestione dei servizi firma xml.
	 */
	private transient XMLEnvelope xmlEnvelope;

	/**
	 * Utils.
	 */
	private transient Utils utils;
	/**
	 * Handler url.
	 */
	private String handlerUrl;
	/**
	 * Pin cert.
	 */
	private byte[] securePINCert;

	/**
	 * Integer firma multipla.
	 */
	private Integer nDocsToSign;

	/**
	 * Informazioni firmatario.
	 */
	private String customerInfo = null;

	/**
	 * Signer.
	 */
	private String signer = null;

	/**
	 * Pin firmatario.
	 */
	private String pin = null;

	/**
	 * Signer pin (otp).
	 */
	private String signerPinOTP = null;

	/**
	 * Properties.
	 */
	private PropertiesProvider pp;

	/**
	 * Servizio.
	 */
	private IMockSRV mockSRV;

	/**
	 * Larghezza pagina pdf standard.
	 */
	private static final Float PDFSTANDARDPAGESIZE = (float) 595;

	/**
	 * Altezza pagina pdf standard.
	 */
	private static final Float PDFSTANDARDPAGEHEIGHT = (float) 842;

	/**
	 * Costruttore.
	 * 
	 * @param pUrlHandler handler url
	 */
	public SignHelper(final String pUrlHandler) {
		this(pUrlHandler, null, false);
	}

	/**
	 * Costruttore vuoto.
	 */
	public SignHelper() {
	}

	/**
	 * Costruttore.
	 * 
	 * @param pUrlHandler handler url
	 * @param pSecurePin  secure pin
	 */
	public SignHelper(final String pUrlHandler, final byte[] pSecurePin, final boolean disableUseHostOnly) {
		try {
			pkbox = new PKBox();
			pkbox.addServer(pUrlHandler, null, null, null);
			if (disableUseHostOnly) {
				SecurePINCache.setUseHostOnly(false);
			}
			handlerUrl = pUrlHandler;
			// Questo è solo per HTTPS
			if (pSecurePin != null) {
				pkbox.setSecurePINCert(pSecurePin);
				securePINCert = pSecurePin;
			}
			envelope = new Envelope(pkbox);
			xmlEnvelope = new XMLEnvelope(pkbox);
			
			utils = new Utils(pkbox);
			pp = PropertiesProvider.getIstance();
			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
					&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				mockSRV = ApplicationContextProvider.getApplicationContext().getBean(IMockSRV.class);
			}
		} catch (final Exception e) {
			LOGGER.error("SIGN HELPER SignHelper(): Errore durante la creazione dell'oggetto 'PKBox' con handler '" + pUrlHandler + "' :", e);
			throw new SignException(e);
		}
	}

	/**
	 * Metodo per l'avvio della transazione.
	 * 
	 * @param inCustomerInfo   informazioni firmatario
	 * @param inSigner         firmatario
	 * @param inPin            pin
	 * @param inSignerPinOTP   signer pin (otp)
	 * @param numberDocsToSign numero documenti da firmare
	 * @return esito firma
	 */
	public EsitoDTO startTransaction(final String inCustomerInfo, final String inSigner, final String inPin, final String inSignerPinOTP, final int numberDocsToSign) {
		EsitoDTO esito = null;
		try {
			nDocsToSign = numberDocsToSign;
			this.signerPinOTP = inSignerPinOTP;

			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
					&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[startTransaction] Non è possibile comunicare con PK in regime mock");
			} else {
				if (nDocsToSign > 1) {
					this.signerPinOTP = envelope.startTransaction(inCustomerInfo, inSigner, inPin, inSignerPinOTP, nDocsToSign);
				}
			}
			this.customerInfo = inCustomerInfo;
			this.signer = inSigner;
			this.pin = inPin;
			esito = new EsitoDTO();
			esito.setEsito(true);
			return esito;
		
		} catch (final InvalidPinException e) {
			
			LOGGER.error(ERRORE_AVVIO_TRANSAZIONE_FIRMA, e);
			esito = new EsitoDTO();
			esito.setCodiceErrore(SignErrorEnum.PKBOX_COD_ERROR);
			esito.setNote(SignErrorEnum.PKBOX_COD_ERROR.getMessage());
			if (e.getMessage() != null && e.getMessage().endsWith("Invalid OTP")) {
				esito.setCodiceErrore(SignErrorEnum.INVALID_OTP_FIRMA_COD_ERROR);
			}
			
		} catch (final InvalidSignerException e) {
		
			LOGGER.error(ERRORE_AVVIO_TRANSAZIONE_FIRMA, e);
			esito = new EsitoDTO();
			esito.setCodiceErrore(SignErrorEnum.PKBOX_CERTIFICATE_COD_ERROR);
			
		} catch (final PKBoxException e) {
			
			return gestisciPKException(e, true);
		
		} catch (final Exception e) {
			
			return gestisciPKException(e, false);
		
		}
		
		return esito;
	}
	
	/**
	 * Costruzione dell'esito a fronte di un errore di PK.
	 * 
	 * @param exc
	 * @param isPKBoxException
	 * @return
	 */
	private EsitoDTO gestisciPKException(final Exception exc, final boolean isPKBoxException) {
		
		LOGGER.error(ERRORE_AVVIO_TRANSAZIONE_FIRMA, exc);
		
		final EsitoDTO esito = new EsitoDTO();
		
		if(isPKBoxException) {
			final PKBoxException e = (PKBoxException)exc;
			if ((e.GetErrorCode() == PKBoxException.PKBOX_PARAM_INVALID_OTP) || (e.getMessage() != null && e.getMessage().endsWith("Invalid OTP")
					|| e.getMessage().contains("it.pkbox.client.PKBoxException: Invalid response status code: 500"))) {
				esito.setCodiceErrore(SignErrorEnum.INVALID_OTP_FIRMA_COD_ERROR);
				return esito;
			} else if ((e.GetErrorCode() == PKBoxException.PKBOX_PARAM_INVALID_PIN)
					|| (e.GetErrorCode() == PKBoxException.PKBOX_PARAM_CERTIFICATE_CHAIN_NOT_FOUND)) {
				esito.setCodiceErrore(SignErrorEnum.INVALID_PIN_FIRMA_COD_ERROR);
				return esito;
			} else if ((e.GetErrorCode() == PKBoxException.PKBOX_PARAM_PIN_LOCKED)) {
				esito.setCodiceErrore(SignErrorEnum.INVALID_PIN_LOCKED_FIRMA_COD_ERROR);
				return esito;
			}
		}
		
		if (exc.getMessage() != null && exc.getMessage().endsWith("the authentication token is expired")) {
			esito.setCodiceErrore(SignErrorEnum.PKBOX_NO_CERTIFICATE_FOUND_COD_ERROR);
			return esito;
		}
		
		esito.setCodiceErrore(SignErrorEnum.PKBOX_SIGN_COD_ERROR);
		return esito;
		
	}

	/**
	 * Oggetto utilizzato per gestire un nuovo campo firma.
	 */
	class Data {

		/**
		 * Nome del campo firma.
		 */
		private final String nome;

		/**
		 * Content del documento con campo firma associato.
		 */
		private final byte[] content;

		/**
		 * Costruttore.
		 * 
		 * @param inNome    nome
		 * @param inContent contenuto
		 */
		Data(final String inNome, final byte[] inContent) {
			super();
			this.nome = inNome;
			this.content = inContent;
		}

		/**
		 * Getter nome campo firma.
		 * 
		 * @return nome campo firma
		 */
		public String getNome() {
			return nome;
		}

		/**
		 * Content pdf con campo firma.
		 * 
		 * @return contente pdf con campo firma
		 */
		public byte[] getContent() {
			return content;
		}
	}

	/**
	 * Metodo per la firma multipla pades.
	 * 
	 * @param contents   contenuti da firmare
	 * @param imageFirma glifo
	 * @param campoFirma nome del campo firma
	 * @param signerName nome del firmatario
	 * @param inPosition posizione firma (nel caso di creazione di un nuovo campo
	 *                   firma)
	 * @param page       pagina firma (nel caso di creazione di un nuovo campo
	 *                   firma)
	 * @param x          ascissa firma (nel caso di creazione di un nuovo campo
	 *                   firma con posizione custom)
	 * @param y          ordinata firma (nel caso di creazione di un nuovo campo
	 *                   firma con posizione custom)
	 * @param reason     motivo firma
	 * @param location   location firma
	 * @param contact    contact info
	 * @param sigLayout  layout della firma
	 * @return contenuti firmati
	 */
	private LinkedHashMap<String, byte[]> multiSignPdf(final Map<String, byte[]> contents, final byte[] imageFirma, final String campoFirma, final String signerName,
			final PositionEnum inPosition, final Integer page, final Integer x, final Integer y, final String reason, final String location, final String contact,
			final SignatureLayout sigLayout) {
		try {
			final AccessPermissionsEnum ap = AccessPermissionsEnum.NO_RESTRICTION;
			PositionEnum position = inPosition;
			if (position == null) {
				position = PositionEnum.BOTTOM_RIGHT;
			}

			final Date today = new Date();
			if (contents != null && contents.keySet() != null) {
				Iterator<String> i = contents.keySet().iterator();

				final HashMap<String, String> docFieldNameMap = new HashMap<>();
				final int numeroDocs = contents.keySet().size();

				final byte[][] pkDigestReturn = new byte[numeroDocs][];
				int posArray = 0;
				while (i.hasNext()) {
					final String key = i.next();
					final byte[] content = contents.get(key);
					String fieldName = null;
					if (campoFirma == null) {
						final PDFSignatureInfo[] blankSign = envelope.pdfparse(content).getBlankSignatures();
						if (blankSign != null && blankSign.length > 0) {
							fieldName = blankSign[0].getName();
						}
					}
					docFieldNameMap.put(key, fieldName);

					final byte[] pkDigest = utils.pdfdigest(content, ap.getValue(), // accessPermissions
							signerName, // signerName
							fieldName, // fieldName
							sigLayout.toString(), // sigLayout
							reason, // reason
							location, // location
							contact, // contact
							today, // data
							imageFirma, // image
							page, // page
							position.getValue(), // position
							x, // x
							y, // y
							Constants.Firma.ALGORITHM_SHA_256, 2); // encoding DigestInfo BASE64 encoded
					pkDigestReturn[posArray++] = Base64.getDecoder().decode(pkDigest);
				}

				final byte[][] pkSignDigestReturn = envelope.multisigndigest(pkDigestReturn, // the array of digests to be
						// signed
						customerInfo, // customerInfo
						signer, // signer
						pin, // pin
						signerPinOTP, // signerPin
						2, // The output envelope encoding format (1= DER, 2=base64, 3=PEM)
						null // date (null is the current sysdate)
				);

				final byte[][] pkDigestReturnDecoded = new byte[numeroDocs][];
				for (posArray = 0; posArray < numeroDocs; posArray++) {
					pkDigestReturnDecoded[posArray] = Base64.getDecoder().decode(pkSignDigestReturn[posArray]);
				}
				if (pkDigestReturnDecoded.length != pkSignDigestReturn.length) {
					throw new SignException("L'output aspettato della firma non corrisponde a quello ottenuto.");
				}

				posArray = 0;
				i = contents.keySet().iterator();
				final LinkedHashMap<String, byte[]> signedContents = new LinkedHashMap<>();
				while (i.hasNext()) {
					final String key = i.next();
					final byte[] content = contents.get(key);
					final byte[] signedPdf = envelope.pdfmerge(content, // document
							pkDigestReturnDecoded[posArray], // envelope
							ap.getValue(), // accessPermissions
							signerName, // signerName
							docFieldNameMap.get(key), // fieldName
							sigLayout.toString(), // sigLayout
							reason, // reason
							location, // location
							contact, // contact
							today, // date
							imageFirma, // image
							page, // page
							position.getValue(), // position
							x, // x
							y // y
					);
					signedContents.put(key, signedPdf);
					posArray++;
				}
				return signedContents;
			} else {
				throw new SignException("Nessun content da firmare.");
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il processo di firma multipla: ", e);
			throw new SignException(e);
		}
	}

	/**
	 * Firma il contenuto in modalità cades con le credenziali fornite.
	 * 
	 * @param contentToSign contenuto da firmare In caso di firma remote questo è
	 *                      l'OTP, in caso di firma automatica è una credenziale
	 *                      statica definita in fase di creazione del certificato.
	 * @return contenuto firmato
	 */
	public byte[] cadesSign(final byte[] contentToSign) {
		final ModeEnum mode = ModeEnum.IMPLICIT_DATA_ATTACHED; // Se fai explicit il p7m creato non conterrà il document originale ma solo la
																// firma
		final EncodingEnum encoding = EncodingEnum.BASE64_ENCODE; // Come codificare la busta P7M
		final Date date = null; // sysdate
		try {

			byte[] output = null;
			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
					&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[cadesSign] Non è possibile comunicare con PK in regime mock");

				final FileDTO documento = mockSRV.getDocumento(TipoDocumentoMockEnum.PK_SIGNED_CADES, ServerUtils.getServerFullName());

				output = documento.getContent();

			} else {
				output = envelope.sign(contentToSign, signer, pin, signerPinOTP, mode.getValue(), encoding.getValue(), date);
			}

			nDocsToSign--;

			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
					&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[cadesSign] Non è possibile comunicare con PK in regime mock");
			} else {
				if (nDocsToSign >= 1) {
					this.signerPinOTP = envelope.continueTransaction(this.customerInfo, this.signer, this.pin, this.signerPinOTP);
				}
			}
			return output;
		} catch (final Exception e) {
			LOGGER.error("SIGN HELPER cadesSign(): ", e);
			throw new SignException(e);
		}
	}

	/**
	 * Getter handler url.
	 * 
	 * @return handler url
	 */
	public String getHandlerUrl() {
		return handlerUrl;
	}

	/**
	 * Getter pin cert.
	 * 
	 * @return pin cert
	 */
	public byte[] getSecurePINCert() {
		return securePINCert;
	}

	/**
	 * Rimuove un campo firma da un pdf.
	 * 
	 * @param content il pdf originale
	 * @param name    nome campo firma
	 * @param append  se impostato a true viene aggiunta una nuova versione al
	 *                documento in cui non è presente il campo firma, comunque esso
	 *                sarà presente nella versione originale, quindi verrà comunque
	 *                segnalato. Se impostato a false viene creata una nuova
	 *                versione del pdf senza il campo firma che quindi non verrà più
	 *                segnalato, ovviamente questo potrebbe invalidare firme
	 *                precedenti.
	 * @return il pdf modificato
	 */
	public static byte[] removeSignField(final byte[] content, final String name, final Boolean append) {
		final DataManager dm = new DataManager(content, append);
		dm.getStamper().getAcroFields().removeField(name);
		return dm.close();
	}

	/**
	 * 
	 * Rimuove il campo firma indicato e ne crea un'altro sulla base delle
	 * informazioni fornite (il campo firma che genera avrà le dimensioni del
	 * glifo).
	 * 
	 * @param contentTPDFoSign contenuto da firmare
	 * @param imageFirma       glifo
	 * @param page             pagina su cui apporre la firma
	 * @param x                ascissa posizione firma
	 * @param y                ordinata posizione firma
	 * @param campoFirma       nome del campo firma da rimuovere
	 * @param reason           motivazione firma
	 * @param sigLayout        layout firma
	 * @return contenuto firmato
	 */
	public byte[] padesMoveVisibleSign(final byte[] contentTPDFoSign, final byte[] imageFirma, final Integer page, final Integer x, final Integer y, final String campoFirma,
			final String reason, final SignatureLayout sigLayout) {
		final byte[] plainPdf = removeSignField(contentTPDFoSign, campoFirma, true);
		return padesPositionalVisibleSign(plainPdf, imageFirma, page, x, y, reason, sigLayout);
	}

	/**
	 * Esegue una pades visibile rapida (firma il primo campo firma che incontra, se
	 * non è presente ne crea uno all'ultima pagina in fondo destra e lo firma).
	 * 
	 * @param contentTPDFoSign contenuto da firmare
	 * @param imageFirma       glifo
	 * @param reason           motivo firma
	 * @param sigLayout        layout firma
	 * @return contenuto firmato
	 */
	public byte[] padesQuickVisibleSign(final byte[] contentTPDFoSign, final byte[] imageFirma, final String reason, final SignatureLayout sigLayout) {
		final Integer page = -1;
		final Integer x = 0;
		final Integer y = 0;
		final PositionEnum position = PositionEnum.BOTTOM_RIGHT;
		return padesSign(contentTPDFoSign, imageFirma, position, page, x, y, null, reason, sigLayout);
	}

	/**
	 * Metodo per la firma multipla pades visibile.
	 * 
	 * @param contents   contenuti da firmare
	 * @param imageFirma glifo
	 * @param signerName firmatario
	 * @param reason     motivo firma
	 * @param sigLayout  layout firma
	 * @return contenuti firmati
	 */
	public Map<String, byte[]> multiPadesQuickVisibleSign(final Map<String, byte[]> contents, final byte[] imageFirma, final String signerName, final String reason,
			final SignatureLayout sigLayout) {
		final Integer page = -1;
		final Integer x = 0;
		final Integer y = 0;
		final PositionEnum position = PositionEnum.BOTTOM_RIGHT;
		return multiSignPdf(contents, imageFirma, null, signerName, position, page, x, y, reason, null, null, sigLayout);
	}

	/**
	 * Esegue una pades su di un campo firma indicato.
	 * 
	 * @param contentTPDFoSign contenuto da firmare
	 * @param imageFirma       glifo
	 * @param campoFirma       campo firma su cui firmare
	 * @param reason           motivo firma
	 * @param sigLayout        layout firma
	 * @return contenuto firmato
	 */
	public byte[] padesSignOnField(final byte[] contentTPDFoSign, final byte[] imageFirma, final String campoFirma, final String reason, final SignatureLayout sigLayout) {
		final PositionEnum position = PositionEnum.CUSTOM;
		final Integer page = 0;
		final Integer x = 0;
		final Integer y = 0;
		return padesSign(contentTPDFoSign, imageFirma, position, page, x, y, campoFirma, reason, sigLayout);
	}

	/**
	 * Pades non visibile.
	 * 
	 * @param contentTPDFoSign contenuto da firmare
	 * @param reason           motivo firma
	 * @return contenuto firmato
	 */
	public byte[] padesInvisibleSign(final byte[] contentTPDFoSign, final String reason) {
		return padesSign(contentTPDFoSign, null, PositionEnum.CUSTOM, 0, 0, 0, null, reason, SignatureLayout.EMPTY);
	}

	/**
	 * Metodo per la firma pades multipla invisibile.
	 * 
	 * @param contents   contenuti da firmare
	 * @param reason     motivo della firma
	 * @param signerName nome del firmatario
	 * @return contenuti firmati
	 */
	public Map<String, byte[]> multiPadesInvisibleSign(final Map<String, byte[]> contents, final String reason, final String signerName) {
		return multiSignPdf(contents, null, null, signerName, PositionEnum.CUSTOM, 0, 0, 0, reason, null, null, SignatureLayout.EMPTY);
	}

	/**
	 * Esegue una firma pades che non impedisce alcuna operazione sul documento.
	 * 
	 * @param contentTPDFoSign contenuto del pdf da firmare
	 * @param imageFirma       glifo
	 * @param position         posizione della firma (per fornire una posizione
	 *                         custom selezionare CUSTOM)
	 * @param page             numero di pagina su cui apporre la firma
	 * @param x                ascissa posizione firma
	 * @param y                ordinata posizione firma
	 * @param campoFirma       nome del campo firma su cui apporre la firma
	 * @param reason           motivo della firma (visibile tra i dettagli della
	 *                         firma)
	 * @param sigLayout        formato della firma (eventualmente usa EMPTY)
	 * @return contenuto firmato
	 */
	private byte[] padesSign(final byte[] contentTPDFoSign, final byte[] imageFirma, final PositionEnum position, final Integer page, final Integer x, final Integer y,
			final String campoFirma, final String reason, final SignatureLayout sigLayout) {
		try {
			final String location = null;
			final String contact = null;
			final Date signingDate = null; // sysdate
			final AccessPermissionsEnum ap = AccessPermissionsEnum.NO_RESTRICTION;

			byte[] output = null;
			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
					&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[padesSign] Non è possibile comunicare con PK in regime mock");

				final FileDTO documento = mockSRV.getDocumento(TipoDocumentoMockEnum.PK_SIGNED_PADES, ServerUtils.getServerFullName());

				output = documento.getContent();

			} else {

				output = envelope.pdfsign(contentTPDFoSign, ap.getValue(), campoFirma, sigLayout.toString(), reason, location, contact, customerInfo, signer, pin,
						signerPinOTP, signingDate, imageFirma, page, position.getValue(), x, y);
			}

			nDocsToSign--;

			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
					&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[padesSign] Non è possibile comunicare con PK in regime mock");
			} else {
				if (nDocsToSign >= 1) {
					this.signerPinOTP = envelope.continueTransaction(this.customerInfo, this.signer, this.pin, this.signerPinOTP);
				}
			}

			return output;
		} catch (final Exception e) {
			LOGGER.error("Errore durante il processo di firma PAdES[URL=" + handlerUrl + ", ALIAS=" + signer + "]: " + e.getMessage(), e);
			throw new SignException(e);
		}
	}

	/**
	 * Crea un campo firma nella posizione indicata e con le dimensioni indicate.
	 * 
	 * @param contentTPDFoSign contenuto da manipolare
	 * @param page             numero di pagina su cui apporre la firma
	 * @param x                ascissa posizione firma
	 * @param y                ordinata posizione firma
	 * @param w                larghezza campo firma
	 * @param h                altezza campo firma
	 * @return contenuto manipolato
	 */
	private Data createCampoFirma(final byte[] contentTPDFoSign, final Integer page, final Integer x, final Integer y, final Integer w, final Integer h) {
		final DataManager dm = new DataManager(contentTPDFoSign);
		final PdfFormField sig = PdfFormField.createSignature(dm.getWriter());
		sig.setWidget(new Rectangle(x, y, x + (float) w, y + (float) h), null);
		sig.setFlags(PdfAnnotation.FLAGS_PRINT);
		sig.put(PdfName.DA, new PdfString("/Helv 0 Tf 0 g"));
		final String signName = "Signature" + new Date().getTime();
		sig.setFieldName(signName);
		sig.setPage(page);
		dm.getStamper().addAnnotation(sig, page);
		final byte[] b = dm.close();
		return new Data(signName, b);
	}

	/**
	 * Esegue una firma pades creando un nuovo campo firma delle dimensione
	 * indicate, nella posizione indicata.
	 * 
	 * @param contentTPDFoSign pdf da firmare
	 * @param imageFirma       glifo
	 * @param page             numero di pagina su cui apporre la firma
	 * @param x                ascissa posizione firma
	 * @param y                ordinata posizione firma
	 * @param reason           motivo della firma (visibile tra i dettagli della
	 *                         firma)
	 * @param sigLayout        formato della firma (eventualmente usa EMPTY)
	 * @return nuovo content con la firma apposta
	 */
	public byte[] padesPositionalVisibleSign(final byte[] contentTPDFoSign, final byte[] imageFirma, final Integer page, final Integer x, final Integer y, final String reason,
			final SignatureLayout sigLayout) {

		final Integer normalizedPage = getNormalizedPage(contentTPDFoSign, page);

		final DataManager dm = new DataManager(contentTPDFoSign);
		final Rectangle pagesize = dm.getReader().getPageSizeWithRotation(normalizedPage);
		final Integer nTop = Math.round(pagesize.getTop());
		dm.close();
		return padesSign(contentTPDFoSign, imageFirma, PositionEnum.CUSTOM, normalizedPage, x, nTop - y, null, reason, sigLayout);
	}

	/**
	 * Metodo per la normalizzazione del numero della pagina (non può superare
	 * l'ultima).
	 * 
	 * @param contentTPDFoSign content pdf (per recuperare numero ultima pagina)
	 * @param page             pagina richiesta
	 * @return pagina normalizzata
	 */
	private static Integer getNormalizedPage(final byte[] contentTPDFoSign, final Integer page) {
		Integer output = page;
		final Integer lastPage = PdfHelper.getNumberOfPages(contentTPDFoSign);
		if (page > lastPage) {
			output = lastPage;
		}
		return output;
	}

	/**
	 * Esegue una firma pades creando un nuovo campo firma delle dimensione
	 * indicate, nella posizione indicata.
	 * 
	 * @param contentTPDFoSign pdf da firmare
	 * @param imageFirma       glifo
	 * @param page             numero di pagina su cui apporre la firma
	 * @param x                ascissa posizione firma
	 * @param y                ordinata posizione firma
	 * @param w                larghezza campo firma
	 * @param h                altezza campo firma
	 * @param reason           motivo della firma (visibile tra i dettagli della
	 *                         firma)
	 * @param sigLayout        formato della firma (eventualmente usa EMPTY)
	 * @return nuovo content con la firma apposta
	 */
	public byte[] padesPositionalVisibleSign(final byte[] contentTPDFoSign, final byte[] imageFirma, final Integer page, final Integer x, final Integer y, final Integer w,
			final Integer h, final String reason, final SignatureLayout sigLayout) {
		final Integer normalizedPage = getNormalizedPage(contentTPDFoSign, page);

		final Data data = createCampoFirma(contentTPDFoSign, normalizedPage, x, y, w, h);
		return padesSignOnField(data.getContent(), imageFirma, data.getNome(), reason, sigLayout);
	}

	/**
	 * Gestisce la funzionalita di firma remota.
	 * 
	 * @param sinfo
	 * @param signType
	 * @param otp
	 * @param newPin
	 * @param contents
	 * @return contents firmato
	 */
	public Map<String, byte[]> remoteSign(final SignerInfoDTO sinfo, final SignTypeEnum signType, final String otp, final String newPin, final Map<String, byte[]> contents) {
		String reason = null;
		if (!SignTypeEnum.CADES.equals(signType)) {
			reason = sinfo.getReason();
		}
		String tmp = newPin;
		if (StringUtils.isNullOrEmpty(pin)) {
			tmp = sinfo.getPin();
		}
		SignatureLayout sigLayout = null;
		if (SignTypeEnum.PADES_VISIBLE.equals(signType)) {
			sigLayout = sinfo.getSigLayout();
		}

		return remoteSign(sinfo.getSigner(), sinfo.getCustomerinfo(), reason, sigLayout, sinfo.getImageFirma(), tmp, otp, signType, contents);
	}

	/**
	 * Gestisce la funzionalita di firma remota.
	 * 
	 * @param inSigner
	 * @param inCustomerInfo
	 * @param reason
	 * @param sigLayout
	 * @param inSignatureImage
	 * @param inPin
	 * @param inSignerPinOTP
	 * @param signType
	 * @param contents
	 * @return contents firmato
	 */
	public Map<String, byte[]> remoteSign(final String inSigner, final String inCustomerInfo, final String reason, final SignatureLayout sigLayout,
			final byte[] inSignatureImage, final String inPin, final String inSignerPinOTP, final SignTypeEnum signType, final Map<String, byte[]> contents) {

		final HashMap<String, byte[]> output = new HashMap<>();

		// Aggiornamento stato interno dell'oggetto: questi dati sono utilizzati dai
		// metodi interni di firma.
		signer = inSigner;
		pin = inPin;
		signerPinOTP = inSignerPinOTP;
		customerInfo = inCustomerInfo;
		nDocsToSign = contents.keySet().size();

		// Validazione input
		if (!SignTypeEnum.CADES.equals(signType) && !SignTypeEnum.PADES_VISIBLE.equals(signType) && !SignTypeEnum.PADES_INVISIBLE.equals(signType)) {
			throw new SignException("La modalità di firma seleziona non è ancora implementata [" + signType + "].");
		} else if (SignTypeEnum.PADES_VISIBLE.equals(signType) && (inSignatureImage == null || inSignatureImage.length == 0)) {
			throw new SignException("Non fornendo il glifo di firma non si può procedere con la PADES VISIBILE, invocare il metodo richiedendo la modalità PADES INVISIBILE.");
		} else if (SignTypeEnum.CADES.equals(signType) && (!StringUtils.isNullOrEmpty(reason))) {
			throw new SignException("Il campo reason non deve essere valorizzato in fase di firma CADES.");
		} else if (sigLayout != null && !SignTypeEnum.PADES_VISIBLE.equals(signType)) {
			throw new SignException("Il layout della firma può essere valorizzato solamente in caso di firma PADES visibile.");
		}

		// Il continue transaction è gestito nei metodi interni, va gestita
		// esplicitamente solamente la start transaction.
		if (nDocsToSign > 1) {
			try {
				signerPinOTP = envelope.startTransaction(customerInfo, signer, pin, signerPinOTP, nDocsToSign);
			} catch (final Exception e) {
				LOGGER.error(e);
				throw new SignException("Errore dutante l'avvio della transazione", e);
			}
		}

		// Firma dei documenti
		for (final Entry<String, byte[]> content : contents.entrySet()) {

			try {
				byte[] signedContent = null;
				if (SignTypeEnum.CADES.equals(signType)) {
					signedContent = cadesSign(content.getValue());
				} else if (SignTypeEnum.PADES_VISIBLE.equals(signType)) {
					signedContent = padesQuickVisibleSign(content.getValue(), inSignatureImage, reason, sigLayout);
				} else if (SignTypeEnum.PADES_INVISIBLE.equals(signType)) {
					signedContent = padesInvisibleSign(content.getValue(), reason);
				}
				output.put(content.getKey(), signedContent);
			} catch (final SignException e) {
				LOGGER.warn("Erroe di Firma per il content con ID:" + content.getKey(), e);
				output.put(content.getKey(), null);
			}
		}

		return output;
	}

	/**
	 * Il metodo sbusta il byte[] in input. Decodifica in base64 il documento in
	 * input in caso il primo sbustamento sollevi eccezioni.
	 * 
	 * @param documento deve essere un p7m. In caso contrario il metodo rilascia una
	 *                  RedException
	 * @return
	 */
	public byte[] extractP7M(final byte[] inDocumento) {
		byte[] ret = null;
		byte[] documento = inDocumento;
		try {
			ret = extract(envelope, documento);
		} catch (final Exception e) {
			LOGGER.warn(e);
			try {
				documento = Base64.getDecoder().decode(documento);
				ret = extract(envelope, documento);
			} catch (final Exception e1) {
				LOGGER.error("Errore durante il recupero del content del documento: " + e.getMessage(), e1);
				ret = null;
			}
		}
		return ret;
	}

	/**
	 * Il metodo verifica la firma del byte[] in input e restituisce il contenuto
	 * della busta
	 * 
	 * @param envelope
	 * @param documento deve essere un p7m
	 * @return
	 */
	private byte[] extract(final Envelope envelope, final byte[] documento) {
		byte [] content = new byte [0];
		
		try {

			if (!StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED)) && "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[extract] Non è possibile comunicare con PK in regime mock");

				final FileDTO doc = mockSRV.getDocumento(TipoDocumentoMockEnum.PK_EXTRACT_P7M, ServerUtils.getServerFullName());

				content = doc.getContent();

			} else {

				final ByteArrayOutputStream baos = new ByteArrayOutputStream();
				final VerifyInfoEx vi = envelope.verifyEx(null, 0, new ByteArrayInputStream(documento), documento.length, null, null, baos);

				if (vi != null) {
					final byte[] ret = baos.toByteArray();
					baos.close();
					content = ret;
				}
			}
		} catch (InvalidEnvelopeException | PKBoxException e) {
			throw new RedException("Errore durante la verifica della signature. ", e);
		} catch (final IOException e) {
			throw new RedException("Errore durante la chiusura del ByteArrayOutputStream: ", e);
		}
		
		return content;
	}

	/**
	 * Restituisce un DTO che contiene le informazioni di firma del documento in
	 * input.
	 * 
	 * @param contentFile
	 * @param isXades
	 * @return informazioni sulla firma
	 */
	public VerifyInfoDTO infoFirmaDocumento(final InputStream contentFile, boolean isXades) {
		VerifyInfoDTO verifyInfo = null;

		try {
			verifyInfo = infoFirmaDocumento(IOUtils.toByteArray(contentFile), isXades);
		} catch (final IOException e) {
			LOGGER.warn("Errore in fase di verifica della firma", e);
			verifyInfo = new VerifyInfoDTO(e);
		}

		return verifyInfo;
	}

	/**
	 * Restituisce un DTO che contiene le informazioni di firma del documento in
	 * input.
	 * 
	 * @param contentFile
	 * @param isXades true se occorre verificare una firma xades
	 * @return info sulla firma
	 */
	private VerifyInfoDTO infoFirmaDocumento(final byte[] contentFile, boolean isXades) {
		VerifyInfoDTO verifyInfo = null;

		try {
			if (!StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED)) && "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[infoFirmaDocumento] Non è possibile comunicare con PK in regime mock");
				final VerifyInfoEx vinfo = null;
				verifyInfo = new VerifyInfoDTO(vinfo);
				return verifyInfo;
			}
			
			if(isXades) {
				XMLVerifyInfoEx xmlInfo = xmlEnvelope.xmlverifyEx(null, null, contentFile, null, false, "");
				verifyInfo = new VerifyInfoDTO(xmlInfo);
			} else {
				VerifyInfoEx vinfo = envelope.verifyEx(null, contentFile, null, null);
				verifyInfo = new VerifyInfoDTO(vinfo);
			}

		} catch (final Exception e) {
			LOGGER.warn("Errore in fase di verifica della firma", e);
			verifyInfo = new VerifyInfoDTO(e);
		}

		LOGGER.info(verifyInfo.toString());
		return verifyInfo;
	}

	/**
	 * Verifica la validità della firma del documento in input.
	 * 
	 * @param contentFile
	 * @param isXades
	 * @return true se il file è firmato e la firma è valida
	 */
	public boolean isValidSignature(final InputStream contentFile, boolean isXades) {
		VerifyInfoDTO verifyInfo = infoFirmaDocumento(contentFile, isXades);
		return isValidSignature(verifyInfo);
	}

	/**
	 * @param verifyInfo
	 * @return
	 */
	private static boolean isValidSignature(final VerifyInfoDTO verifyInfo) {
		final Integer valueMetadato = verifyInfo.getMetadatoValiditaFirmaValue();
		return ValueMetadatoVerificaFirmaEnum.FIRMA_OK.getValue().equals(valueMetadato);
	}

	/**
	 * Verifica se il documento in input è firmato.
	 * 
	 * @param contentFile
	 * @param isXades
	 * @return true se il file è firmato
	 */
	public boolean hasSignatures(final InputStream contentFile, boolean isXades) {
		VerifyInfoDTO verifyInfo = infoFirmaDocumento(contentFile, isXades);
		return EsitoVerificaFirmaEnum.OK.equals(verifyInfo.getEsito());
	}

	/**
	 * Restituisce true se il content è firmato per la spedizione, false altrimenti.
	 * 
	 * @param contentFile
	 * @param signerAoo
	 * @return true se il content è firmato per la spedizione, false altrimenti
	 */
	public boolean hasSignaturesForSpedizione(final InputStream contentFile, final String signerAoo) {
		boolean esito = false;
		
		VerifyInfoDTO verifyInfo = infoFirmaDocumento(contentFile, false);

		esito = EsitoVerificaFirmaEnum.OK.equals(verifyInfo.getEsito());
		if (verifyInfo.getFirmatari() != null && !verifyInfo.getFirmatari().isEmpty()) {
			for (final SignerDTO signerDTO : verifyInfo.getFirmatari()) {
				if (!signerDTO.getFirmatario().equalsIgnoreCase(signerAoo)) {
					esito = true;
					break;
				}
				esito = false;
			}
		}

		return esito;
	}

	/**
	 * @param documento
	 * @return
	 */
	public byte[] extractP7MAndVerifySignature(final byte[] documento) {

		byte[] contentToReturn = new byte [0];
		try {

			if (!StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED)) && "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[extractP7MAndVerifySignature] Non è possibile comunicare con PK in regime mock");

				final FileDTO doc = mockSRV.getDocumento(TipoDocumentoMockEnum.PK_EXTRACT_P7M, ServerUtils.getServerFullName());

				contentToReturn = doc.getContent();

			} else {

				final ByteArrayOutputStream baos = new ByteArrayOutputStream();
				final VerifyInfoEx vi = envelope.verifyEx(null, 0, new ByteArrayInputStream(documento), documento.length, null, null, baos);

				if (vi != null) {
					final VerifyInfoDTO verifyInfo = new VerifyInfoDTO(vi);
					if (isValidSignature(verifyInfo)) {
						final byte[] ret = baos.toByteArray();
						baos.close();
						contentToReturn = ret;
					}
				}
			}

		} catch (InvalidEnvelopeException | PKBoxException e) {
			throw new RedException("Errore durante la verifica della signature. ", e);
		} catch (final IOException e) {
			throw new RedException("Errore durante la chiusura del ByteArrayOutputStream: ", e);
		}

		return contentToReturn;
	}
	
	/**
	 * @param content
	 * @param hash
	 * @param image
	 * @param aliasFirmatario
	 * @param campoFirma
	 * @param dataFirma
	 * @param reason
	 * @return
	 */
	public byte[] pdfmerge(final byte[] content, final byte[] hash, final byte[] image, final String aliasFirmatario, final String campoFirma, final Date dataFirma,
			final String reason) {
		byte[] output = new byte [0];

		try {

			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
					&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[pdfmerge] Non è possibile comunicare con PK in regime mock");
				return new byte [0];
			}

			final byte[] signedPdf = envelope.pdfmerge(content, hash, 0, aliasFirmatario, campoFirma, "<empty>", reason, null, null, dataFirma, image, -1, 9, 0, 0);
			output = signedPdf;

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di merge del pdf firmato", e);
		}

		return output;
	}

	/**
	 * @param document
	 * @param campoFirma
	 * @param image
	 * @param aliasFirmatario
	 * @param accessPermission
	 * @param date
	 * @param reason
	 * @return
	 */
	public byte[] createPdfDigest(final byte[] document, final String campoFirma, final byte[] image, final String aliasFirmatario, final int accessPermission,
			final Date date, final String reason) {
		byte[] pdfDigest = new byte[0];

		try {

			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
					&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[createPdfDigest] Non è possibile comunicare con PK in regime mock");
				return pdfDigest;
			}

			pdfDigest = utils.pdfdigest(
					// documento
					document,
					// accessPermissions
					accessPermission,
					// signer
					aliasFirmatario,
					// field name
					campoFirma,
					// field layout
					"<empty>",
					// reason
					reason,
					// location
					null,
					// contact
					null,
					// date (null is the current sysdate)
					date,
					// image
					image,
					// page
					-1,
					// position
					9,
					// x
					0,
					// y
					0,
					// The hash algorithm (1=MD2, 2=MD5, 3=SHA-1, 4=SHA-224, 5=SHA-256, 6=SHA-384,
					// 7=SHA-512, 8=RIPEMD-128, 9=RIPEMD-160) //fare costante
					5,
					// The output envelope encoding format (1= DER, 2=base64, 3= raw binary, 4=raw
					// base64)
					2);

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di creazione del pdf digest", e);
		}

		return pdfDigest;
	}

	/**
	 * @param inSigner
	 * @param inPin
	 * @param content
	 * @param segnatura
	 * @return
	 */
	public byte[] applicaTimbroProtocolloPrincipaleEntrata(final String inSigner, final String inPin, final byte[] content, final String segnatura, final boolean confPDFA) {
		return applicaTimbroProtocolloPrincipale(inSigner, inPin, content, segnatura, true, confPDFA);
	}

	/**
	 * @param inSigner
	 * @param inPin
	 * @param content
	 * @param segnatura
	 * @return
	 */
	public byte[] applicaTimbroProtocolloPrincipaleUscita(final String inSigner, final String inPin, final byte[] content, final String segnatura, final boolean confPDFA) {
		return applicaTimbroProtocolloPrincipale(inSigner, inPin, content, segnatura, false, confPDFA);
	}

	/**
	 * @param inSigner
	 * @param inPin
	 * @param content
	 * @param segnatura
	 * @param bFlagTop
	 * @return
	 */
	public byte[] applicaTimbroProtocolloAllegatoEntrata(final String inSigner, final String inPin, final byte[] content, final String segnatura, final boolean confPDFA) {
		return applicaTimbroProtocolloAllegato(inSigner, inPin, content, segnatura, true, confPDFA);
	}

	/**
	 * @param inSigner
	 * @param inPin
	 * @param content
	 * @param segnatura
	 * @param bFlagTop
	 * @return
	 */
	public byte[] applicaTimbroProtocolloAllegatoUscita(final String inSigner, final String inPin, final byte[] content, final String segnatura, final boolean confPDFA) {
		return applicaTimbroProtocolloAllegato(inSigner, inPin, content, segnatura, false, confPDFA);
	}

	/**
	 * @param inSigner
	 * @param inPin
	 * @param content
	 * @param segnatura
	 * @param bFlagEntrata
	 * @param bFlagTop
	 * @return
	 */
	private byte[] applicaTimbroProtocolloPrincipale(final String inSigner, final String inPin, final byte[] content, final String segnatura, final Boolean bFlagEntrata, final boolean confPDFA) {
		SignatureLayout sigLayout = null;

		if (confPDFA) {
			sigLayout = new SignatureLayout(segnatura, TextAlignEnum.CENTER, FontFamilyEnum.HELVETICA, 11, FontStyleEnum.NORMAL, 1);
		} else {
			sigLayout = new SignatureLayout(segnatura, TextAlignEnum.CENTER, FontFamilyEnum.TIMES_ROMAN, 11, FontStyleEnum.NORMAL, 1);
		}

		final byte[] imageFirma = FileUtils.getFile(SignHelper.class.getClassLoader(), "timbro_protocollo.png");

		return applicaTimbroProtocollo(inSigner, inPin, content, sigLayout, imageFirma, segnatura, bFlagEntrata, (float) 277, 11, confPDFA);
	}

	/**
	 * @param inSigner
	 * @param inPin
	 * @param content
	 * @param segnatura
	 * @param bFlagEntrata
	 * @param bFlagTop
	 * @return
	 */
	private byte[] applicaTimbroProtocolloAllegato(final String inSigner, final String inPin, final byte[] content, final String segnatura, final Boolean bFlagEntrata,
			final boolean confPDFA) {
		SignatureLayout sigLayout = null;
		if (confPDFA) {
			sigLayout = new SignatureLayout(segnatura, TextAlignEnum.CENTER, FontFamilyEnum.HELVETICA, 11, FontStyleEnum.NORMAL, 1);
		} else {
			sigLayout = new SignatureLayout(segnatura, TextAlignEnum.CENTER, FontFamilyEnum.TIMES_ROMAN, 11, FontStyleEnum.NORMAL, 1);
		}

		final byte[] imageFirma = FileUtils.getFile(SignHelper.class.getClassLoader(), "timbro_protocollo_allegato.png");

		return applicaTimbroProtocollo(inSigner, inPin, content, sigLayout, imageFirma, segnatura, bFlagEntrata, (float) 470, 11, confPDFA);
	}

	/**
	 * @param inSigner
	 * @param inPin
	 * @param content
	 * @param segnatura
	 * @param bFlagEntrata
	 * @param bFlagTop
	 * @param sigLayout
	 * @param imageFirma
	 * @param dimensioneTimbro
	 * @param dimensioneCarattere
	 * @param confPDFA
	 * @return
	 */
	private byte[] applicaTimbroProtocollo(final String inSigner, final String inPin, final byte[] content, final SignatureLayout sigLayout, byte[] imageFirma,
			final String segnatura, final Boolean bFlagEntrata, final Float dimensioneTimbro, final Integer dimensioneCarattere, final boolean confPDFA) {
		byte[] output = null;
		DataManager dm = null;

		try {
			output = content;
			dm = new DataManager(content);

			if (confPDFA) {
				imageFirma = recuperaImageFirma(imageFirma, dimensioneTimbro);
			}

			final PdfReader reader = dm.getReader();

			// Si seleziona la posizione (alto sinistra uscita, alto destra entrata)
			final PositionEnum position = PositionEnum.CUSTOM;
			// Il timbro per la stampigliatura del protocollo deve essere apposto solo sulla
			// prima pagina
			final int page = 1;
			final Rectangle pagesize = reader.getPageSizeWithRotation(page);
			final Integer nTop = Math.round(pagesize.getTop());
			final Integer nRight = Math.round(pagesize.getRight());
			Integer x = 0;
			final Integer y = nTop;
			if (Boolean.TRUE.equals(bFlagEntrata)) {
				x = nRight - 10;
			} else {
				x = 10;
			}

			final Rectangle pagesizeCurrentPage = reader.getPageSizeWithRotation(page);
			if ((dimensioneTimbro + 10) <= pagesizeCurrentPage.getWidth()) {
				// Implicitamente il metodo invocato non applicherà alcuna restrizione sul
				// content a valle della firma eseguita (AccessPermissionsEnum.NO_RESTRICTION)
				output = autoPadesSign(null, inSigner, inPin, output, imageFirma, position, page, x, y, NEW, segnatura, sigLayout);
			} else {
				final Float scalingPercentage = pagesizeCurrentPage.getWidth() / PDFSTANDARDPAGESIZE;
				final Integer imageWidth = Math.round(dimensioneTimbro * scalingPercentage);
				Integer characterSize = Math.round(dimensioneCarattere * scalingPercentage);
				characterSize = characterSize == 0 ? 1 : characterSize;
				SignatureLayout sigLayoutResized = null;
				if (confPDFA) {
					sigLayoutResized = new SignatureLayout(segnatura, TextAlignEnum.CENTER, FontFamilyEnum.HELVETICA, characterSize, FontStyleEnum.NORMAL, 1);
				} else {
					sigLayoutResized = new SignatureLayout(segnatura, TextAlignEnum.CENTER, FontFamilyEnum.TIMES_ROMAN, characterSize, FontStyleEnum.NORMAL, 1);
				}
				final byte[] imageFirmaResized = PdfHelper.scaleImg(imageFirma, imageWidth);
				// Implicitamente il metodo invocato non applicherà alcuna restrizione sul
				// content a valle della firma eseguita (AccessPermissionsEnum.NO_RESTRICTION)
				output = autoPadesSign(null, inSigner, inPin, output, imageFirmaResized, position, page, x, y, NEW, segnatura, sigLayoutResized);
			}

		} catch (final Exception e) {
			LOGGER.error(e);
			if (!StringUtils.isNullOrEmpty(e.getMessage()) && e.getMessage().contains("##PKBOXFAULT##")) {
				throw new AdobeException("##PKBOXFAULT## Si è verificato un errore durante la stampigliatura del numero di protocollo", e);
			} else {
				throw new RedException("Si è verificato un errore durante la stampigliatura del numero di protocollo", e);
			}

		} finally {
			if (dm != null) {
				dm.close();
			}
		}

		return output;
	}

	/**
	 * 
	 * @param dimensioneTimbro
	 * @param imageFirma
	 * @return byte array imageFirma
	 */
	private static byte[] recuperaImageFirma(final byte[] imageFirma, final Float dimensioneTimbro) {

		byte[] tmpImageFirma = imageFirma;
		final BufferedImage image = new BufferedImage(dimensioneTimbro.intValue(), 25, BufferedImage.TYPE_INT_RGB);
		try {
			final Graphics2D graphics = image.createGraphics();
			graphics.setColor(Color.WHITE);
			graphics.fillRect(0, 0, dimensioneTimbro.intValue(), 25);
			graphics.dispose();
			// image contains the bitmap
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(image, "png", baos);
			baos.flush();
			tmpImageFirma = baos.toByteArray();
			baos.close();
		} catch (final IOException e) {
			LOGGER.error(ERROR_MODIFICA_IMMAGINE_TIMBRO_MSG, e);
		}
		return tmpImageFirma;
	}

	/**
	 * @param inSigner
	 * @param inPin
	 * @param content
	 * @param numeroDocumento
	 * @param confPDFA
	 * @param descrizioneAoo
	 * @return
	 */
	public byte[] applicaTimbroNumeroDocumento(final String inSigner, final String inPin, final byte[] content, final String numeroDocumento, final boolean confPDFA,
			final String descrizioneAoo) {
		SignatureLayout sigLayout = null;
		final Integer dimensioneCarattere = 11;
		final Float dimensioneTimbro = (float) 102; // Dimensione dell'immagine di default
		byte[] imageFirma = FileUtils.getFile(SignHelper.class.getClassLoader(), TIMBRO_NUMERO_DOCUMENTO);
		final String testoTimbro = "ID: " + numeroDocumento;
		final String reasonTimbro = "Apposizione dell'ID " + numeroDocumento + " da parte dell'AOO \"" + descrizioneAoo + "\"";

		// Se esiste una firma con la stessa reason allora il timbro è già stato apposto
		// in precedenza (e.g. Firma Multipla), quindi si restituisce il content in
		// input
		try {
			VerifyInfoDTO verifyInfo = infoFirmaDocumento(content, false);
			
			if (!CollectionUtils.isEmpty(verifyInfo.getFirmatari())) {
				final Collection<SignerDTO> firme = verifyInfo.getFirmatari();
				for (final SignerDTO firma : firme) {
					if (reasonTimbro.equals(firma.getMotivo())) {
						LOGGER.info("Sul documento è già presente il timbro relativo al numero documento: " + numeroDocumento + " per l'AOO: " + descrizioneAoo);
						return content;
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la verifica di pre-esistenza del timbro relativo al numero documento, il timbro sarà apposto.", e);
		}

		if (confPDFA) {
			sigLayout = new SignatureLayout(testoTimbro, TextAlignEnum.CENTER, FontFamilyEnum.HELVETICA, dimensioneCarattere, FontStyleEnum.NORMAL, 1);
			imageFirma = recuperaImageFirma(imageFirma, dimensioneTimbro);
		} else {
			sigLayout = new SignatureLayout(testoTimbro, TextAlignEnum.CENTER, FontFamilyEnum.TIMES_ROMAN, dimensioneCarattere, FontStyleEnum.NORMAL, 1);
		}

		// Solo per la prima pagina
		final DataManager dm = new DataManager(content);
		final Rectangle pagesizeCurrentPage = dm.getReader().getPageSizeWithRotation(1);
		dm.close();
		final PositionEnum position = PositionEnum.CUSTOM;
		final Integer nTop = Math.round(pagesizeCurrentPage.getTop());
		final Integer x = 10;
		final Integer y = nTop;

		if ((dimensioneTimbro + 10) <= pagesizeCurrentPage.getWidth()) {
			// Implicitamente il metodo invocato non applicherà alcuna restrizione sul
			// content a valle della firma eseguita (AccessPermissionsEnum.NO_RESTRICTION)
			return autoPadesSign(null, inSigner, inPin, content, imageFirma, position, 0, x, y, NEW, reasonTimbro, sigLayout);
		} else {
			final Float scalingPercentage = pagesizeCurrentPage.getWidth() / PDFSTANDARDPAGESIZE;
			final Integer imageWidth = Math.round(dimensioneTimbro * scalingPercentage);
			Integer characterSize = Math.round(dimensioneCarattere * scalingPercentage);
			characterSize = characterSize == 0 ? 1 : characterSize;
			SignatureLayout sigLayoutResized = null;
			if (confPDFA) {
				sigLayoutResized = new SignatureLayout(testoTimbro, TextAlignEnum.CENTER, FontFamilyEnum.HELVETICA, characterSize, FontStyleEnum.NORMAL, 1);
			} else {
				sigLayoutResized = new SignatureLayout(testoTimbro, TextAlignEnum.CENTER, FontFamilyEnum.TIMES_ROMAN, characterSize, FontStyleEnum.NORMAL, 1);
			}
			final byte[] imageFirmaResized = PdfHelper.scaleImg(imageFirma, imageWidth);
			// Implicitamente il metodo invocato non applicherà alcuna restrizione sul
			// content a valle della firma eseguita (AccessPermissionsEnum.NO_RESTRICTION)
			return autoPadesSign(null, inSigner, inPin, content, imageFirmaResized, position, 0, x, y, NEW, reasonTimbro, sigLayoutResized);
		}
	}

	/**
	 * @param inSigner
	 * @param inPin
	 * @param content
	 * @param confPDFA
	 * @return
	 */
	public byte[] applicaTimbroPostilla(final String inSigner, final String inPin, final byte[] content, final boolean confPDFA) {
		return applicaTimbroPostilla(inSigner, inPin, content, confPDFA, false, null);
	}
	
	
	/**
	 * @param inSigner
	 * @param inPin
	 * @param content
	 * @param confPDFA
	 * @param onylFirstPage
	 * @return
	 */
	public byte[] applicaTimbroPostilla(final String inSigner, final String inPin, final byte[] content, final boolean confPDFA, final boolean onylFirstPage,  byte[] imageFirma) {
		
		Integer dimensioneCarattere = 9;
		// Dimensione del campo firma
		Integer cx = 11;	// Width
		Integer cy = 648; 	// Height
		SignatureLayout sigLayout = null;
		if (confPDFA) {
			sigLayout = new SignatureLayout(Constants.EMPTY_STRING, TextAlignEnum.LEFT, FontFamilyEnum.HELVETICA, dimensioneCarattere, FontStyleEnum.NORMAL, 1);
			if(imageFirma==null) {
				imageFirma = FileUtils.getFile(SignHelper.class.getClassLoader(), "postilla_no_transparency.jpg");
			}
		} else {
			sigLayout = new SignatureLayout(Constants.EMPTY_STRING, TextAlignEnum.LEFT, FontFamilyEnum.TIMES_ROMAN, dimensioneCarattere, FontStyleEnum.NORMAL, 1);
			if(imageFirma==null) {
				imageFirma = FileUtils.getFile(SignHelper.class.getClassLoader(), "postilla.png");
			}
		}
		
		// Coordinate della posizione
		Integer y = 97;
		Integer x = 7;
		
		return applicaTimbroPostilla(inSigner, inPin, content, confPDFA, imageFirma, sigLayout, x, y, cx, cy, onylFirstPage);
	}
	
	
	/**
	 * @param inSigner
	 * @param inPin
	 * @param content
	 * @param confPDFA
	 * @return
	 */
	public byte[] applicaTimbroPostillaCopiaConforme(String inSigner, String inPin, byte[] content, boolean confPDFA, byte [] imageFirma) {
		Integer dimensioneCarattere = 9;
		// Dimensione del campo firma
		Integer cx = 11;	// Width
		Integer cy = 648; 	// Height
		SignatureLayout sigLayout = null;
		if (confPDFA) {
			sigLayout = new SignatureLayout(Constants.EMPTY_STRING, TextAlignEnum.LEFT, FontFamilyEnum.HELVETICA, dimensioneCarattere, FontStyleEnum.NORMAL, 1);
			if(imageFirma==null) {
				imageFirma = FileUtils.getFile(SignHelper.class.getClassLoader(), "postilla_copia_conforme_no_transparency.jpg");
			}
		} else {
			sigLayout = new SignatureLayout(Constants.EMPTY_STRING, TextAlignEnum.LEFT, FontFamilyEnum.TIMES_ROMAN, dimensioneCarattere, FontStyleEnum.NORMAL, 1);
			if(imageFirma==null) {
				 imageFirma = FileUtils.getFile(SignHelper.class.getClassLoader(), "postilla_copia_conforme.png");
			}
		}
		
		// Coordinate della posizione
		Integer y = 97;
		Integer x = 7;
		
		return applicaTimbroPostilla(inSigner, inPin, content, confPDFA, imageFirma, sigLayout, x, y, cx, cy, false);
	}
	
	private byte[] applicaTimbroPostilla(final String inSigner, final String inPin, final byte[] content, final boolean confPDFA, final byte[] imageFirma, final SignatureLayout sigLayout, final Integer x, final Integer y,
			final Integer cx, final Integer cy, final boolean onlyFirstPage) {
		byte[] output = content;
		DataManager dm = null;
		Integer dimensioneCarattere = 9;

		try {
			dm = new DataManager(content);
			PdfReader reader = dm.getReader();
			int numeroPagine;
			if (onlyFirstPage) {
				numeroPagine = 1;
			} else {
				numeroPagine = reader.getNumberOfPages();
			}
			
			// Il timbro per la stampigliatura della postilla deve essere apposto su tutte le pagine
			for (int page = 1; page <= numeroPagine; page++) {	
				final Rectangle pagesizeCurrentPage = reader.getPageSizeWithRotation(page);
				if ((cy + 10) <= pagesizeCurrentPage.getHeight()) {
					// Implicitamente il metodo invocato non applicherà alcuna restrizione sul
					// content a valle della firma eseguita (AccessPermissionsEnum.NO_RESTRICTION)
					output = autoPadesSign(null, inSigner, inPin, output, imageFirma, PositionEnum.CUSTOM, page, x, y, NEW, null, sigLayout, cx, cy);
				} else {
					final Float scalingPercentage = pagesizeCurrentPage.getHeight() / PDFSTANDARDPAGEHEIGHT;
					final Integer newCy= Math.round(cy * scalingPercentage);
					Integer characterSize = Math.round(dimensioneCarattere * scalingPercentage);
					characterSize = characterSize == 0 ? 1 : characterSize;
					SignatureLayout sigLayoutResized = null;
					if (confPDFA) {
						sigLayoutResized = new SignatureLayout(Constants.EMPTY_STRING, TextAlignEnum.LEFT, FontFamilyEnum.HELVETICA, characterSize, FontStyleEnum.NORMAL, 1);
					} else {
						sigLayoutResized = new SignatureLayout(Constants.EMPTY_STRING, TextAlignEnum.LEFT, FontFamilyEnum.TIMES_ROMAN, characterSize, FontStyleEnum.NORMAL, 1);
					}

					// Implicitamente il metodo invocato non applicherà alcuna restrizione sul
					// content a valle della firma eseguita (AccessPermissionsEnum.NO_RESTRICTION)
					output = autoPadesSign(null, inSigner, inPin, output, imageFirma, PositionEnum.CUSTOM, page, x, y, NEW, null, sigLayoutResized, cx, newCy);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			if (!StringUtils.isNullOrEmpty(e.getMessage()) && e.getMessage().contains("##PKBOXFAULT##")) {
				throw new SignException("Si è verificato un errore durante la stampigliatura della postilla sul documento.", e);
			} else {
				throw new RedException("Si è verificato un errore durante la stampigliatura della postilla sul documento.", e);
			}

		} finally {
			if (dm != null) {
				dm.close();
			}
		}
		
		return output;
	}

	/**
	 * @param inSigner
	 * @param inPin
	 * @param content
	 * @param segnatura
	 * @param toRight
	 * @param confPDFA
	 * @return
	 */
	public byte[] applicaTimbroApprovazione(final String inSigner, final String inPin, final byte[] content, final String segnatura, final boolean toRight,
			final boolean confPDFA) {
		return applicaTimbroApprovazione(inSigner, inPin, content, segnatura, toRight, confPDFA, false);
	}
	
	
	/**
	 * @param inSigner
	 * @param inPin
	 * @param content
	 * @param segnatura
	 * @param toRight
	 * @param confPDFA
	 * @param onLastPage
	 * @return
	 */
	public byte[] applicaTimbroApprovazione(final String inSigner, final String inPin, final byte[] content, final String segnatura, final boolean toRight, final boolean confPDFA, 
			final boolean onLastPage) {
		TextAlignEnum alignment = TextAlignEnum.LEFT;
		if (toRight) {
			alignment = TextAlignEnum.RIGHT;
		}
		final Integer dimensioneCarattere = 8;
		SignatureLayout sigLayout = null;
		final Float dimensioneTimbro = (float) 277; // Dimensione dell'immagine di default
		byte[] imageFirma = FileUtils.getFile(SignHelper.class.getClassLoader(), "timbro_protocollo.png");

		if (confPDFA) {
			 sigLayout = new SignatureLayout(segnatura, alignment, FontFamilyEnum.HELVETICA, dimensioneCarattere, FontStyleEnum.ITALIC, 6579300);
			 final BufferedImage image = new BufferedImage(dimensioneTimbro.intValue(), 25,  BufferedImage.TYPE_INT_RGB);
			 try {
			    final Graphics2D graphics = image.createGraphics();
		    	graphics.setColor(Color.WHITE);
				graphics.fillRect(0, 0, dimensioneTimbro.intValue(), 25);
				graphics.dispose();
				// image contains the bitmap
				final ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ImageIO.write(image, "png", baos);
				baos.flush();
				imageFirma = baos.toByteArray();
				baos.close();
			} catch (IOException e) {
				LOGGER.error(ERROR_MODIFICA_IMMAGINE_TIMBRO_MSG, e);
			}
		} else {
			sigLayout = new SignatureLayout(segnatura, alignment, FontFamilyEnum.TIMES_ROMAN, dimensioneCarattere, FontStyleEnum.ITALIC, 6579300);
		}

		// Coordinate della posizione
		final Integer y = 32;
		Integer x = 12;
		if (toRight) {
			x = 284;
		}

		DataManager dm = new DataManager(content);
		PdfReader reader = dm.getReader();
		int paginaApprovazione;
		// Prima o ultima pagina
		if (onLastPage) {
			paginaApprovazione = reader.getNumberOfPages();
		} else {
			paginaApprovazione = 1;
		}
		
		final Rectangle pagesizeCurrentPage = dm.getReader().getPageSizeWithRotation(paginaApprovazione);
		dm.close();
		if ((dimensioneTimbro + 10) <= pagesizeCurrentPage.getWidth()) {
			// Implicitamente il metodo invocato non applicherà alcuna restrizione sul
			// content a valle della firma eseguita (AccessPermissionsEnum.NO_RESTRICTION)
			return autoPadesSign(null, inSigner, inPin, content, imageFirma, PositionEnum.CUSTOM, 0, x, y, NEW, null, sigLayout);
		} else {
			final Float scalingPercentage = pagesizeCurrentPage.getWidth() / PDFSTANDARDPAGEHEIGHT;
			final Integer imageWidth = Math.round(dimensioneTimbro * scalingPercentage);
			Integer characterSize = Math.round(dimensioneCarattere * scalingPercentage);
			characterSize = characterSize == 0 ? 1 : characterSize;
			SignatureLayout sigLayoutResized = null;
			if (confPDFA) {
				sigLayoutResized = new SignatureLayout(segnatura, alignment, FontFamilyEnum.HELVETICA, characterSize, FontStyleEnum.ITALIC, 6579300);
			} else {
				sigLayoutResized = new SignatureLayout(segnatura, alignment, FontFamilyEnum.TIMES_ROMAN, characterSize, FontStyleEnum.ITALIC, 6579300);
			}

			final byte[] imageFirmaResized = PdfHelper.scaleImg(imageFirma, imageWidth);
			// Implicitamente il metodo invocato non applicherà alcuna restrizione sul
			// content a valle della firma eseguita (AccessPermissionsEnum.NO_RESTRICTION).
			return autoPadesSign(null, inSigner, inPin, content, imageFirmaResized, PositionEnum.CUSTOM, 0, x, y, NEW, null, sigLayoutResized);
		}
	}

	/**
	 * @param inCustomerInfo
	 * @param inSigner
	 * @param inPin
	 * @param contentTPDFoSign
	 * @param imageFirma
	 * @param position
	 * @param page
	 * @param x
	 * @param y
	 * @param campoFirma
	 * @param reason
	 * @param sigLayout
	 * @return
	 */
	private byte[] autoPadesSign(final String inCustomerInfo, final String inSigner, final String inPin, final byte[] contentTPDFoSign, final byte[] imageFirma,
			final PositionEnum position, final Integer page, final Integer x, final Integer y, final String campoFirma, final String reason, final SignatureLayout sigLayout) {
		return autoPadesSign(inCustomerInfo, inSigner, inPin, contentTPDFoSign, imageFirma, position, page, x, y, campoFirma, reason, sigLayout, 0, 0);
	}

	/**
	 * @param inCustomerInfo
	 * @param inSigner
	 * @param inPin
	 * @param contentTPDFoSign
	 * @param imageFirma
	 * @param position
	 * @param page
	 * @param x
	 * @param y
	 * @param campoFirma
	 * @param reason
	 * @param sigLayout
	 * @param width
	 * @param height
	 * @return
	 */
	private byte[] autoPadesSign(final String inCustomerInfo, final String inSigner, final String inPin, final byte[] contentTPDFoSign, final byte[] imageFirma,
			final PositionEnum position, final Integer page, final Integer x, final Integer y, final String campoFirma, final String reason, final SignatureLayout sigLayout,
			final Integer width, final Integer height) {
		byte[] output = null;

		try {
			final String location = null;
			final String contact = null;
			final Date signingDate = null; // sysdate
			final AccessPermissionsEnum ap = AccessPermissionsEnum.NO_RESTRICTION;

			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
					&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[autoPadesSign] Non è possibile comunicare con PK in regime mock");

				final FileDTO documento = mockSRV.getDocumento(TipoDocumentoMockEnum.PK_SIGNED_TIMBRO, ServerUtils.getServerFullName());

				output = documento.getContent();

			} else {
				output = envelope.pdfsign(contentTPDFoSign, ap.getValue(), campoFirma, sigLayout.toString(), reason, location, contact, inCustomerInfo, inSigner, inPin, inPin,
						signingDate, imageFirma, page, position.getValue(), x, y, width, height);
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante il processo di firma PAdES[URL=" + handlerUrl + ", ALIAS=" + inSigner + "]: " + e.getMessage(), e);
			throw new SignException(e);
		}

		return output;
	}

	/**
	 * Restituisce il signer.
	 * 
	 * @return signer
	 */
	public String getSigner() {
		return signer;
	}
}