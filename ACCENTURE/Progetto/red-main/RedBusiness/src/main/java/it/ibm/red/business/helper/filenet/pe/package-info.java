/**
 * @author CPIERASC
 *
 *	Questo package conterrà l'helper per la gestione delle query da eseguire sul Process Engine. Mentre ogni accesso al
 *	database è confinato in uno specifico DAO, dato il numero esiguo di code da gestire, tutta la logica di recupero 
 *	di workflow è contenuta in questo helper.
 *
 */
package it.ibm.red.business.helper.filenet.pe;
