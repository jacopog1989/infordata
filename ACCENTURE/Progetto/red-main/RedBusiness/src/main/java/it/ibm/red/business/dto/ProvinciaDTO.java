package it.ibm.red.business.dto;

import java.io.Serializable;

/**
 * Classe ProvinciaDTO.
 */
public class ProvinciaDTO implements Serializable {
	
	/**
	 * Costante serial UID
	 */
	private static final long serialVersionUID = 6552634824187380241L;
	
	/**
	 * Identificativo provincia
	 */
	private String idProvincia;
	
	/**
	 * Denominazione provincia
	 */
	private String denominazione;
	
	/**
	 * Sigla provincia
	 */
	private String siglaProvincia;
	
	/**
	 * Soppressa
	 */
	private Integer soppressa;
	
	/**
	 * Identificativo regione
	 */
	private String idRegione;
	
	
	/** 
	 * @return the id provincia
	 */
	public String getIdProvincia() {
		return idProvincia;
	}
	
	/** 
	 * @param idProvincia the new id provincia
	 */
	public void setIdProvincia(final String idProvincia) {
		this.idProvincia = idProvincia;
	}
	
	/** 
	 * @return the denominazione
	 */
	public String getDenominazione() {
		return denominazione;
	}
	
	/** 
	 * @param denominazione the new denominazione
	 */
	public void setDenominazione(final String denominazione) {
		this.denominazione = denominazione;
	}
	
	/** 
	 * @return the sigla provincia
	 */
	public String getSiglaProvincia() {
		return siglaProvincia;
	}
	
	/** 
	 * @param siglaProvincia the new sigla provincia
	 */
	public void setSiglaProvincia(final String siglaProvincia) {
		this.siglaProvincia = siglaProvincia;
	}
	
	/** 
	 * @return the soppressa
	 */
	public Integer getSoppressa() {
		return soppressa;
	}
	
	/** 
	 * @param soppressa the new soppressa
	 */
	public void setSoppressa(final Integer soppressa) {
		this.soppressa = soppressa;
	}
	
	/** 
	 * @return the id regione
	 */
	public String getIdRegione() {
		return idRegione;
	}
	
	/** 
	 * @param idRegione the new id regione
	 */
	public void setIdRegione(final String idRegione) {
		this.idRegione = idRegione;
	}
	
	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "ProvinciaETY [idProvincia=" + idProvincia + ", denominazione=" + denominazione + ", siglaProvincia="
				+ siglaProvincia + ", soppressa=" + soppressa + ", idRegione=" + idRegione + "]";
	}
	

}
