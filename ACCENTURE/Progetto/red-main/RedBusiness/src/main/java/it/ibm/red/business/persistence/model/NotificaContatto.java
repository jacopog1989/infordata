package it.ibm.red.business.persistence.model;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import it.ibm.red.business.dto.NotificaUtenteDTO;
import it.ibm.red.business.enums.StatoNotificaContattoEnum;
import it.ibm.red.business.enums.TipoOperazioneEnum;

/**
 * Classe per la gestione delle notifiche contatto
 * 
 */
public class NotificaContatto extends NotificaUtenteDTO implements Serializable {
	

	/**
	 * Label BR.
	 */
	private static final String BR = "<br />";
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/** 
	 * The Constant TIPO_NOTIFICA_RICHIESTA. 
	 */
	public static final String TIPO_NOTIFICA_RICHIESTA = "richiesta";
	
	/** 
	 * The Constant TIPO_NOTIFICA_EVASA. 
	 */
	public static final String TIPO_NOTIFICA_EVASA = "evasa";
	
	/** 
	 * Identificativo notifica contatto. 
	 */
	private Integer idNotificaContatto;
	
	/** 
	 * Identificativo contatto. 
	 */
	private Long idContatto;
	
	/** 
	 * Identificativo nodo modifica. 
	 */
	private Long idNodoModifica;
	
	/** 
	 * Identificativo utente modifica. 
	 */
	private Long idUtenteModifica;
	
	/** 
	 * Data operazione notifica.
	 */
	private Date dataOperazione;
	
	/** 
	 * Utente. 
	 */
	private String utente;
	
	/** 
	 * Operazione effettuata. 
	 */
	private String operazioneEffettuata;
	
	/** 
	 * Alias dell'utente notifica. 
	 */
	private String alias;
	
	/** 
	 * Nome. 
	 */
	private String nome;
	
	/** 
	 * Cognome. 
	 */
	private String cognome;
	
	/** 
	 * Mail. 
	 */
	private String mail;
	
	/** 
	 * Mail Pec. 
	 */
	private String mailPec;
	
	/** 
	 * Tipo notifica. 
	 */
	private String tipoNotifica;
	
	/** 
	 * Vecchio valore notifica per la modifica. 
	 */
	private String vecchioValore;
	
	/** 
	 * Nuovo valore notifica. 
	 */
	private String nuovoValore;
	
	/** 
	 * Note. 
	 */
	private String note;
	
	/** 
	 * Tipologia contatto. 
	 */
	private String tipologiaContatto;
	
	/** 
	 * Motivo rifiuto. 
	 */
	private String motivoRifiuto;
	
	/** 
	 * Stato notifica contatto. 
	 */
	private StatoNotificaContattoEnum stato;
	
	/** 
	 * Identificativo Aoo. 
	 */
	private Long idAoo;
	
	/** 
	 * Valore nuovo contatto. 
	 */
	private Contatto nuovoContatto;
	
	/** 
	 * Tipo operazione di notifica. 
	 */
	private TipoOperazioneEnum operazioneEnum;
	
	/** 
	 * Stato notifica rubrica. 
	 */
	private Integer statoNotificaRubrica;
	
	/** 
	 * Parametro approvato con modifiche. 
	 */
	private boolean approvatoConModifiche;
	
	 
	/**
	 * Getter  stato notifica rubrica.
	 *
	 * @return the stato notifica rubrica
	 */
	public Integer getStatoNotificaRubrica() {
		return statoNotificaRubrica;
	}
	
	/**
	 * Setter  stato notifica rubrica.
	 *
	 * @param statoNotificaRubrica the new stato notifica rubrica
	 */
	public void setStatoNotificaRubrica(final Integer statoNotificaRubrica) {
		this.statoNotificaRubrica = statoNotificaRubrica;
	}	
	
	/**
	 * Getter  id utente modifica.
	 *
	 * @return the id utente modifica
	 */
	public Long getIdUtenteModifica() {
		return idUtenteModifica;
	}

	/**
	 * Setter  id utente modifica.
	 *
	 * @param idUtenteModifica the new id utente modifica
	 */
	public void setIdUtenteModifica(final Long idUtenteModifica) {
		this.idUtenteModifica = idUtenteModifica;
	}

	/**
	 * Getter tipo notifica.
	 *
	 * @return the tipo notifica
	 */
	public String getTipoNotifica() {
		return tipoNotifica;
	}

	/**
	 * Setter tipo notifica.
	 *
	 * @param tipoNotifica the new tipo notifica
	 */
	public void setTipoNotifica(final String tipoNotifica) {
		this.tipoNotifica = tipoNotifica;
	}

	/**
	 * Getter id notifica contatto.
	 *
	 * @return the id notifica contatto
	 */
	public int getIdNotificaContatto() {
		return idNotificaContatto;
	}
	
	/**
	 * Setter id notifica contatto.
	 *
	 * @param idNotificaContatto the new id notifica contatto
	 */
	public void setIdNotificaContatto(final int idNotificaContatto) {
		this.idNotificaContatto = idNotificaContatto;
	}
	
	/**
	 * Getter id contatto.
	 *
	 * @return the id contatto
	 */
	public Long getIdContatto() {
		return idContatto;
	}

	/**
	 * Setter id contatto.
	 *
	 * @param idContatto the new id contatto
	 */
	public void setIdContatto(final Long idContatto) {
		this.idContatto = idContatto;
	}
	
	/**
	 * Getter id nodo modifica.
	 *
	 * @return the id nodo modifica
	 */
	public Long getIdNodoModifica() {
		return idNodoModifica;
	}

	/**
	 * Setter id nodo modifica.
	 *
	 * @param idNodoModifica the new id nodo modifica
	 */
	public void setIdNodoModifica(final Long idNodoModifica) {
		this.idNodoModifica = idNodoModifica;
	}
	
	/**
	 * Getter alias.
	 *
	 * @return the alias
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * Setter alias.
	 *
	 * @param alias the new alias
	 */
	public void setAlias(final String alias) {
		this.alias = alias;
	}
	
	/**
	 * Getter utente.
	 *
	 * @return the utente
	 */
	public String getUtente() {
		return utente;
	}
	
	/**
	 * Getter operazione effettuata.
	 *
	 * @return the operazione effettuata
	 */
	public String getOperazioneEffettuata() {
		return operazioneEffettuata;
	}

	/**
	 * Setter operazione effettuata.
	 *
	 * @param operazioneEffettuata the new operazione effettuata
	 */
	public void setOperazioneEffettuata(final String operazioneEffettuata) {
		this.operazioneEffettuata = operazioneEffettuata;
		this.operazioneEnum = TipoOperazioneEnum.get(operazioneEffettuata);
		
	}

	/**
	 * Setter utente.
	 *
	 * @param utente the new utente
	 */
	public void setUtente(final String utente) {
		this.utente = utente;
	}
	
	/**
	 * Getter nome.
	 *
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}
	
	/**
	 * Setter nome.
	 *
	 * @param nome the new nome
	 */
	public void setNome(final String nome) {
		this.nome = nome;
	}
	
	/**
	 * Getter cognome.
	 *
	 * @return the cognome
	 */
	public String getCognome() {
		return cognome;
	}
	
	/**
	 * Setter cognome.
	 *
	 * @param cognome the new cognome
	 */
	public void setCognome(final String cognome) {
		this.cognome = cognome;
	}
	
	/**
	 * Getter mail.
	 *
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}
	
	/**
	 * Setter mail.
	 *
	 * @param mail the new mail
	 */
	public void setMail(final String mail) {
		this.mail = mail;
	}
	
	/**
	 * Getter mail pec.
	 *
	 * @return the mail pec
	 */
	public String getMailPec() {
		return mailPec;
	}
	
	/**
	 * Setter mail pec.
	 *
	 * @param mailPec the new mail pec
	 */
	public void setMailPec(final String mailPec) {
		this.mailPec = mailPec;
	}
	
	/**
	 * Getter data operazione.
	 *
	 * @return the data operazione
	 */
	public Date getDataOperazione() {
		return dataOperazione;
	}
	
	/**
	 * Setter data operazione.
	 *
	 * @param dataOperazione the new data operazione
	 */
	public void setDataOperazione(final Date dataOperazione) {
		this.dataOperazione = dataOperazione;
	}

	/**
	 * Getter vecchio valore.
	 *
	 * @return the vecchio valore
	 */
	public String getVecchioValore() {
		return vecchioValore;
	}

	/**
	 * Setter vecchio valore.
	 *
	 * @param vecchioValore the new vecchio valore
	 */
	public void setVecchioValore(final String vecchioValore) {
		this.vecchioValore = vecchioValore;
	}

	/**
	 * Getter nuovo valore.
	 *
	 * @return the nuovo valore
	 */
	public String getNuovoValore() {
		return nuovoValore;
	}

	/**
	 * Setter nuovo valore.
	 *
	 * @param nuovoValore the new nuovo valore
	 */
	public void setNuovoValore(final String nuovoValore) {
		this.nuovoValore = nuovoValore;
	}

	/**
	 * Getter note.
	 *
	 * @return the note
	 */
	public String getNote() {
		return note;
	}

	/**
	 * Setter note.
	 *
	 * @param note the new note
	 */
	public void setNote(final String note) {
		this.note = note;
	}

	/**
	 * Getter tipologia contatto.
	 *
	 * @return the tipologia contatto
	 */
	public String getTipologiaContatto() {
		return tipologiaContatto;
	}

	/**
	 * Setter tipologia contatto.
	 *
	 * @param tipologiaContatto the new tipologia contatto
	 */
	public void setTipologiaContatto(final String tipologiaContatto) {
		this.tipologiaContatto = tipologiaContatto;
	}
	
	/**
	 * Getter info notifica.
	 *
	 * @return the info notifica
	 */
	public String getInfoNotifica() {
		final StringBuilder info = new StringBuilder();
		
		if ("".equals(getAlias())) {
			info.append(" Alias: " + getAlias() + BR);
		}
		if ("".equals(getNome())) {
			info.append(" Nome: " + getNome() + BR);
		}
		if ("".equals(getCognome())) {
			info.append(" Cognome: " + getCognome() + BR);
		}
		if ("".equals(getMail())) {
			info.append(" PEO: " + getMail() + BR);
		}
		if ("".equals(getMailPec())) {
			info.append(" PEC: " + getMailPec() + BR);
		}
		
		return  info.toString();
	}

	/**
	 * Ritorna il fullname dato dai campi nome + cognome (Se diverso da null).
	 *
	 * @return the full name
	 */
	public String getFullName() {
		return nome + (cognome != null ? " " + cognome : "");
	}

	/**
	 * Getter del motivo rifiuto.
	 *
	 * @return the motivo rifiuto
	 */
	public String getMotivoRifiuto() {
		return motivoRifiuto;
	}

	/**
	 * Setter del motivo rifiuto.
	 *
	 * @param motivoRifiuto the new motivo rifiuto
	 */
	public void setMotivoRifiuto(final String motivoRifiuto) {
		this.motivoRifiuto = motivoRifiuto;
	}

	/**
	 * Getter stato.
	 *
	 * @return the stato
	 */
	public StatoNotificaContattoEnum getStato() {
		return stato;
	}

	/**
	 * Setter stato.
	 *
	 * @param stato the new stato
	 */
	public void setStato(final StatoNotificaContattoEnum stato) {
		this.stato = stato;
	}
	
	/**
	 * Getter  icona stato notifica.
	 *
	 * @return the icona stato notifica
	 */
	public String getIconaStatoNotifica() {
		String iconaNotifica = "img/richiesta_in_attesa.png";
		if (this.stato.equals(StatoNotificaContattoEnum.ESEGUITA)) {
			iconaNotifica = "img/richiesta_evasa.png";
		} else if (this.stato.equals(StatoNotificaContattoEnum.RIFIUTATA)) {
			iconaNotifica = "img/richiesta_rifiutata.png";
		}
		return iconaNotifica;
	}
	
	/**
	 * Getter tooltip stato notifica.
	 *
	 * @return the tooltip stato notifica
	 */
	public String getTooltipStatoNotifica() {
		String tooltipNotifica = "Richiesta in attesa";
		if (this.stato.equals(StatoNotificaContattoEnum.ESEGUITA)) {
			tooltipNotifica = "Richiesta evasa";
		} else if (this.stato.equals(StatoNotificaContattoEnum.RIFIUTATA)) {
			tooltipNotifica = "Richiesta rifiutata";
		}
		return tooltipNotifica;
	}
	
	/**
	 * Equals.
	 *
	 * @param other the other
	 * @return true, if successful
	 */
	@Override
	public boolean equals(final Object other) {
		final boolean result = false;
		if ((other instanceof NotificaContatto) && (this.getIdNotificaContatto() == ((NotificaContatto) other).getIdNotificaContatto())) {
			return true;
		}
        return result;
	}

	/**
	 * Hash code.
	 *
	 * @return the int
	 */
	@Override
	public int hashCode() {
		return super.hashCode();
	}
	
	/**
	 * Getter id aoo.
	 *
	 * @return the id aoo
	 */
	public Long getIdAoo() {
		return idAoo;
	}

	/**
	 * Setter id aoo.
	 *
	 * @param idAoo the new id aoo
	 */
	public void setIdAoo(final Long idAoo) {
		this.idAoo = idAoo;
	}

	/**
	 * Getter nuovo contatto.
	 *
	 * @return the nuovo contatto
	 */
	public Contatto getNuovoContatto() {
		return nuovoContatto;
	}
	
	/**
	 * Setter nuovo contatto.
	 *
	 * @param nuovoContatto the new nuovo contatto
	 */
	public void setNuovoContatto(final Contatto nuovoContatto) {
		this.nuovoContatto = nuovoContatto;
	}

	/**
	 * Getter operazione enum.
	 *
	 * @return the operazione enum
	 */
	public TipoOperazioneEnum getOperazioneEnum() {
		return operazioneEnum;
	}

	/**
	 * Setter operazione enum.
	 *
	 * @param operazioneEnum the new operazione enum
	 */
	public void setOperazioneEnum(final TipoOperazioneEnum operazioneEnum) {
		this.operazioneEnum = operazioneEnum;
	}
	
	/**
	 *Metodo che calcola le notifiche ancora da leggere.
	 */
	@Override
	public void calcolaDaLeggere() {
		final LocalDate dateOdierna = LocalDate.now();
		final LocalDate dateOperazione = dataOperazione != null ? Instant.ofEpochMilli(dataOperazione.getTime()).atZone(ZoneId.systemDefault()).toLocalDate() : null;
		
		if (dataOperazione != null && dateOdierna.compareTo(dateOperazione) == 0) {
			setDaLeggere(true);
		}
	}
	
	/**
	 * Getter approvato con modifiche.
	 *
	 * @return the approvato con modifiche
	 */
	public boolean getApprovatoConModifiche() {
		return approvatoConModifiche;
	}
	
	/**
	 * Setter approvato con modifiche.
	 *
	 * @param approvatoConModifiche the new approvato con modifiche
	 */
	public void setApprovatoConModifiche(final boolean approvatoConModifiche) {
		this.approvatoConModifiche = approvatoConModifiche;
	}
	 
}
