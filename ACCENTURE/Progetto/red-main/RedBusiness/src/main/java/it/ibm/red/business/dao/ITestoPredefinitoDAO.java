/**
 * 
 */
package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.TestoPredefinitoDTO;

/**
 * @author SLac
 *
 */
public interface ITestoPredefinitoDAO extends Serializable {

	/**
	 * Restituisce una lista di testi per tipo e AOO.
	 * 
	 * @param idTipoTesto
	 * @param idAOO
	 * @return
	 */
	List<TestoPredefinitoDTO> getTestiByTipoAndAOO(int idTipoTesto, Long idAOO, Connection connection);
	
}
