/**
 * 
 */
package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.Date;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.RegistroProtocolloDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.StringUtils;

/**
 * Trasformer Document FileNet -> Registro Protocollo DTO
 * 
 * @author m.crescentini
 */
public class FromDocumentToRegistroProtocolloTrasformer extends TrasformerCE<RegistroProtocolloDTO> {

	/**
	 * Serializzable
	 */
	private static final long serialVersionUID = -376051212611046568L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FromDocumentToRegistroProtocolloTrasformer.class.getName());
	
	/**
	 * Costruttore.
	 */
	public FromDocumentToRegistroProtocolloTrasformer() {
		super(TrasformerCEEnum.FROM_DOCUMENTO_TO_REGISTRO_PROTOCOLLO_PMEF);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE#trasform(com.filenet.api.core.Document, java.sql.Connection).
	 */
	@Override
	public RegistroProtocolloDTO trasform(final Document document, final Connection connection) {
		RegistroProtocolloDTO registroProtocollo = null;
		
		try {
			String guid = StringUtils.cleanGuidToString(document.get_Id());
			String documentTitle = (String) getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
			Date dataProtocollo = (Date) getMetadato(document, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY);
			Integer annoProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
			Date dataElencoTrasmissione = (Date) getMetadato(document, PropertiesNameEnum.DATA_ELENCO_TRASMISSIONE_METAKEY);
			String nomeFile = (String) getMetadato(document, PropertiesNameEnum.NOME_FILE_METAKEY);
			
			registroProtocollo = new RegistroProtocolloDTO(guid, documentTitle, dataProtocollo, annoProtocollo, dataElencoTrasmissione, nomeFile);
		} catch (Exception e) {
			LOGGER.error("Errore nel recupero di un metadato durante la trasformazione da Document a MasterDocumentRedDTO: ", e);
		}
		
		return registroProtocollo;
	}
}