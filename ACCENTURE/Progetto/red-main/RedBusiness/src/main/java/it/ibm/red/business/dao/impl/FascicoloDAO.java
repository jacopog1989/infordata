package it.ibm.red.business.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IFascicoloDAO;
import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * The Class FascicoloDAO.
 *
 * @author CPIERASC
 * 
 *         Dao per la gestione di un fascicolo.
 */
@Repository
public class FascicoloDAO extends AbstractDAO implements IFascicoloDAO {

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Messaggio di errore recupero indice classificazione. Occorre appendere l'id
	 * del fascicolo al messaggio.
	 */
	private static final String ERROR_RECUPERO_INDICE_CLASSIFICAZIONE = "Errore nel recupero dell'indice di classificazione per il fascicolo: ";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FascicoloDAO.class.getName());

	/**
	 * Gets the id fascicolo procedimentale.
	 *
	 * @param idDocumento the id documento
	 * @param idAoo       the id aoo
	 * @param connection  the connection
	 * @return the id fascicolo procedimentale
	 */
	@Override
	public final Integer getIdFascicoloProcedimentale(final String idDocumento, final Long idAoo, final Connection connection) {
		Integer output = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			final Long idDoc = Long.parseLong(idDocumento);
			String querySQL = "SELECT MIN(D.IDFASCICOLO) as ID FROM DOCUMENTOFASCICOLO D WHERE D.IDDOCUMENTO = " + sanitize(idDoc) + " AND D.IDAOO = " + sanitize(idAoo)
					+ " AND D.AUTOMATICO=1";
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			if (rs.next()) {
				output = rs.getInt("ID");
			} else {
				closeStatement(ps, rs);
				querySQL = "SELECT MIN(IDFASCICOLO) as ID FROM DOCUMENTOFASCICOLO WHERE IDDOCUMENTO = " + sanitize(idDoc) + " AND IDAOO = " + sanitize(idAoo);
				ps = connection.prepareStatement(querySQL);
				rs = ps.executeQuery();
				if (rs.next()) {
					output = rs.getInt("ID");
				}
			}
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero del fascicolo procedimentale per il documento con id=" + idDocumento, e);
		} finally {
			closeStatement(ps, rs);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IFascicoloDAO#insertDocumentoFascicolo(int, int,
	 *      int, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public int insertDocumentoFascicolo(final int idDocumento, final int idFascicolo, final int automatico, final Long idAoo, final Connection con) {
		PreparedStatement ps = null;
		int result = 0;

		try {
			int index = 1;
			ps = con.prepareStatement("INSERT INTO documentofascicolo (iddocumento, idfascicolo, automatico, idaoo) VALUES (?,?,?,?)");
			ps.setInt(index++, idDocumento);
			ps.setInt(index++, idFascicolo);
			ps.setInt(index++, automatico);
			ps.setLong(index++, idAoo);

			result = ps.executeUpdate();
		} catch (final Exception e) {
			LOGGER.error("Errore nell'inserimento dei dati nel DB, idDocumento: " + idDocumento + ", idFascicolo: " + idFascicolo + ", automatico: " + automatico, e);
			throw new RedException("Errore nell'inserimento dei dati nel DB, idDocumento: " + idDocumento + ", idFascicolo: " + idFascicolo + ", automatico: " + automatico,
					e);
		} finally {
			closeStatement(ps);
		}

		return result;
	}

	/**
	 * @see it.ibm.red.business.dao.IFascicoloDAO#insert(java.lang.String,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public int insert(final String indiceClassificazione, final Long idAoo, final Connection con) {
		CallableStatement cs = null;
		int idFascicolo = 0;

		try {
			int index = 0;
			cs = con.prepareCall("{call p_insfascicolo_maoo(?,?,?)}");
			cs.setString(++index, indiceClassificazione);
			cs.setLong(++index, idAoo);
			cs.registerOutParameter(++index, Types.INTEGER);
			cs.execute();
			idFascicolo = cs.getInt(index);
		} catch (final Exception e) {
			LOGGER.error("Errore nella creazione di un fascicolo su DB. ID AOO: " + idAoo, e);
			throw new RedException("Errore nella creazione di un fascicolo su DB. ID AOO: " + idAoo, e);
		} finally {
			closeStatement(cs);
		}

		return idFascicolo;
	}

	/**
	 * @see it.ibm.red.business.dao.IFascicoloDAO#updateDocumentoFascicolo(int, int,
	 *      int, int, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public int updateDocumentoFascicolo(final int idDocumento, final int idFascicoloNew, final int idFascicoloOld, final int automatico, final Long idAoo,
			final Connection con) {
		PreparedStatement ps = null;
		int result = 0;

		try {
			int index = 1;
			ps = con.prepareStatement(
					"UPDATE documentofascicolo SET iddocumento = ?, idfascicolo = ?, automatico = ?" + " WHERE iddocumento = ? AND idfascicolo = ? AND idaoo = ?");
			ps.setInt(index++, idDocumento);
			ps.setInt(index++, idFascicoloNew);
			ps.setInt(index++, automatico);
			ps.setInt(index++, idDocumento);
			ps.setInt(index++, idFascicoloOld);
			ps.setLong(index++, idAoo);

			result = ps.executeUpdate();
		} catch (final Exception e) {
			LOGGER.error("Errore nell'aggiornamento del fascicolo per il documento: " + idDocumento, e);
			throw new RedException("Errore nell'aggiornamento del fascicolo per il documento: " + idDocumento, e);
		} finally {
			closeStatement(ps);
		}

		return result;
	}

	/**
	 * @see it.ibm.red.business.dao.IFascicoloDAO#deleteDocumentoFascicolo(int, int,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public int deleteDocumentoFascicolo(final int idDocumento, final int idFascicolo, final Long idAoo, final Connection con) {
		PreparedStatement ps = null;
		int result = 0;

		try {
			int index = 1;
			ps = con.prepareStatement("DELETE FROM documentofascicolo WHERE iddocumento = ? AND idfascicolo = ? AND idaoo = ?");
			ps.setInt(index++, idDocumento);
			ps.setInt(index++, idFascicolo);
			ps.setLong(index++, idAoo);

			result = ps.executeUpdate();
		} catch (final Exception e) {
			LOGGER.error("Errore nell'eliminazione del documento: " + idDocumento + " dal fascicolo: " + idFascicolo, e);
			throw new RedException("Errore nell'eliminazione del documento: " + idDocumento + " dal fascicolo: " + idFascicolo, e);
		} finally {
			closeStatement(ps);
		}

		return result;
	}

	/**
	 * @see it.ibm.red.business.dao.IFascicoloDAO#getIndiceClassificazione(java.lang.String,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public String getIndiceClassificazione(final String idFascicolo, final Long idAoo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String result = Constants.EMPTY_STRING;

		try {
			ps = con.prepareStatement("SELECT INDICECLASSIFICAZIONE FROM fascicolo WHERE IDFASCICOLO = ? AND idaoo = ?");
			ps.setString(1, idFascicolo);
			ps.setLong(2, idAoo);

			rs = ps.executeQuery();
			if (rs.next()) {
				result = rs.getString("INDICECLASSIFICAZIONE");
			}
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_INDICE_CLASSIFICAZIONE + idFascicolo, e);
			throw new RedException(ERROR_RECUPERO_INDICE_CLASSIFICAZIONE + idFascicolo, e);
		} finally {
			closeStatement(ps, rs);
		}

		return result;
	}

	/**
	 * @see it.ibm.red.business.dao.IFascicoloDAO#getIndiceClassificazioneByIdFascicolo(java.lang.String,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public TitolarioDTO getIndiceClassificazioneByIdFascicolo(final String idFascicolo, final Long idAoo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		TitolarioDTO t = null;

		try {
			ps = con.prepareStatement("select t.indiceclassificazione, t.descrizione " + "  from titolario t, fascicolo f "
					+ " where t.indiceclassificazione = f.indiceclassificazione " + " and f.idfascicolo = ? " + " and f.idaoo = ? " + " and t.idaoo = ? ");
			ps.setString(1, idFascicolo);
			ps.setLong(2, idAoo);
			ps.setLong(3, idAoo);

			rs = ps.executeQuery();
			if (rs.next()) {
				t = new TitolarioDTO(rs.getString("INDICECLASSIFICAZIONE"), null, rs.getString("descrizione"), 0, idAoo, null, null, 0);
			}
		} catch (final SQLException e) {
			LOGGER.error(ERROR_RECUPERO_INDICE_CLASSIFICAZIONE + idFascicolo, e);
			throw new RedException(ERROR_RECUPERO_INDICE_CLASSIFICAZIONE + idFascicolo, e);
		} finally {
			closeStatement(ps, rs);
		}

		return t;
	}

	/**
	 * @see it.ibm.red.business.dao.IFascicoloDAO#aggiornaIndiceClassificazione(int,
	 *      java.lang.String, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public int aggiornaIndiceClassificazione(final int idFascicolo, final String indiceClassificazione, final Long idAoo, final Connection con) {
		PreparedStatement ps = null;
		int result = 0;

		try {
			int index = 1;
			ps = con.prepareStatement("UPDATE fascicolo SET indiceclassificazione = ? WHERE idfascicolo = ? AND idaoo = ?");
			ps.setString(index++, indiceClassificazione);
			ps.setInt(index++, idFascicolo);
			ps.setLong(index++, idAoo);

			result = ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore nell'aggiornamento dell'indice di classificazione per il fascicolo: " + idFascicolo, e);
			throw new RedException("Errore nell'aggiornamento dell'indice di classificazione per il fascicolo: " + idFascicolo, e);
		} finally {
			closeStatement(ps);
		}

		return result;
	}

	/**
	 * @see it.ibm.red.business.dao.IFascicoloDAO#isDocumentoAssociato(int, int,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public boolean isDocumentoAssociato(final int idDocumento, final int idFascicolo, final Long idAoo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean isAssociato = false;

		try {
			ps = con.prepareStatement("SELECT COUNT(*) count_associazioni FROM documentofascicolo WHERE iddocumento = ? AND idfascicolo = ? AND idaoo = ?");
			ps.setInt(1, idDocumento);
			ps.setInt(2, idFascicolo);
			ps.setLong(3, idAoo);

			rs = ps.executeQuery();

			if ((rs.next()) && (rs.getInt("count_associazioni") > 0)) {
				isAssociato = true;
			}
		} catch (final SQLException e) {
			LOGGER.error("Si è verificato un errore durante la verifica dell'associazione del fascicolo: " + idFascicolo + " al documento: " + idDocumento, e);
			throw new RedException("Si è verificato un errore durante la verifica dell'associazione del fascicolo: " + idFascicolo + " al documento: " + idDocumento, e);
		} finally {
			closeStatement(ps, rs);
		}

		return isAssociato;
	}

	/**
	 * @see it.ibm.red.business.dao.IFascicoloDAO#getFascicoliDocumento(int,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public Set<String> getFascicoliDocumento(final int idDocumento, final Long idAoo, final Connection con) {
		final Set<String> idFascicoli = new HashSet<>();

		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.prepareStatement("SELECT DISTINCT(idfascicolo) FROM documentofascicolo WHERE iddocumento = ? AND idaoo = ?");
			ps.setInt(1, idDocumento);
			ps.setLong(2, idAoo);
			rs = ps.executeQuery();

			while (rs.next()) {
				idFascicoli.add(String.valueOf(rs.getInt("IDFASCICOLO")));
			}
		} catch (final SQLException e) {
			LOGGER.error("Si è verificato un errore durante il recupero dei fascicoli associati al documento: " + idDocumento, e);
			throw new RedException("Si è verificato un errore durante il recupero dei fascicoli associati al documento: " + idDocumento, e);
		} finally {
			closeStatement(ps, rs);
		}

		return idFascicoli;
	}
}