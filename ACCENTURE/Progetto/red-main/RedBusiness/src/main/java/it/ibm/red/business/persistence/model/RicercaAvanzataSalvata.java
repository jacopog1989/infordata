package it.ibm.red.business.persistence.model;

import java.util.Date;

import it.ibm.red.business.dto.AbstractDTO;

/**
 * DTO di una Ricerva Avanzata Salvata che definisce i parametri di una ricerca per il recupero della stessa.
 */
public class RicercaAvanzataSalvata extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 8093157891528953294L;

	/**
	 * Identificativo.
	 */
	private Integer id;
	
	/**
	 * Utente.
	 */
	private Long idUtente;
	
	/**
	 * Ufficio.
	 */
	private Long idUfficio;
	
	/**
	 * Ruolo.
	 */
	private Long idRuolo;
	
	/**
	 * Nome.
	 */
	private String nome;
	
	/**
	 * Data creazione.
	 */
	private Date dataCreazione;
	
	/**
	 * Restituisce l'id.
	 * @return id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Imposta l'id.
	 * @param id
	 */
	public void setId(final int id) {
		this.id = id;
	}

	/**
	 * Restituisce l'id utente.
	 * @return id utente
	 */
	public Long getIdUtente() {
		return idUtente;
	}

	/**
	 * Imposta l'id dell'utente.
	 * @param idUtente
	 */
	public void setIdUtente(final Long idUtente) {
		this.idUtente = idUtente;
	}

	/**
	 * Restituisce il nome.
	 * @return nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Imposta il nome.
	 * @param nome
	 */
	public void setNome(final String nome) {
		this.nome = nome;
	}

	/**
	 * Restituisce la data di creazione.
	 * @return data creazione
	 */
	public Date getDataCreazione() {
		return dataCreazione;
	}

	/**
	 * Imposta la data di creazione.
	 * @param dataCreazione
	 */
	public void setDataCreazione(final Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	/**
	 * Restituisce l'id dell'ufficio.
	 * @return id ufficio
	 */
	public Long getIdUfficio() {
		return idUfficio;
	}

	/**
	 * Imposta l'id dell'ufficio.
	 * @param idUfficio
	 */
	public void setIdUfficio(final Long idUfficio) {
		this.idUfficio = idUfficio;
	}

	/**
	 * Restituisce l'id del ruolo.
	 * @return id ruolo
	 */
	public Long getIdRuolo() {
		return idRuolo;
	}

	/**
	 * Imposta l'id del ruolo.
	 * @param idRuolo
	 */
	public void setIdRuolo(final Long idRuolo) {
		this.idRuolo = idRuolo;
	}
}