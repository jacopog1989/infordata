package it.ibm.red.business.enums;

/**
 * Tipo documento mockato.
 */
public enum TipoDocumentoMockEnum {
	
	/**
	 * Valore.
	 */
	NPS, 
	
	/**
	 * Valore.
	 */
	PDF_ANNULLAMENTO, 
	
	/**
	 * Valore.
	 */
	PDF_APPROVAZIONI, 
	
	/**
	 * Valore.
	 */
	PDF_TRASFORMAZIONE_TIMBRO, 
	
	/**
	 * Valore.
	 */
	PDF_TRASFORMAZIONE_NO_TIMBRO, 
	
	/**
	 * Valore.
	 */
	PDF_PREDISPOSIZIONE_FIRMA_DIGITALE, 
	
	/**
	 * Valore.
	 */
	PDF_INSERT_POSTILLA, 
	
	/**
	 * Valore.
	 */
	PDF_INSERT_STAMPIGLIATURA, 
	
	/**
	 * Valore.
	 */
	PDF_POSTILLA_COPIA_CONFORME,
	
	/**
	 * Valore.
	 */
	PK_SIGNED_CADES, 
	
	/**
	 * Valore.
	 */
	PK_SIGNED_PADES, 
	
	/**
	 * Valore.
	 */
	PK_SIGNED_TIMBRO, 
	
	/**
	 * Valore.
	 */
	PK_EXTRACT_P7M;
	
}