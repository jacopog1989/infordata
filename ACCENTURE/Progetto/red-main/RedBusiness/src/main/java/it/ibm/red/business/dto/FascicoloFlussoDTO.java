package it.ibm.red.business.dto;

import java.util.Date;

import it.ibm.red.business.enums.FilenetStatoFascicoloEnum;

/**
 * DTO che definisce un fascicolo Flusso.
 */
public class FascicoloFlussoDTO extends AbstractDTO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 343342991395718619L;

	/**
	 * Identificativo documento.
	 */
	private Integer idDocumento;

	/**
	 * Numero pratica.
	 */
	private Integer numPratica;

	/**
	 * Descrizione.
	 */
	private String descrizione;

	/**
	 * Data apertura.
	 */
	private Date dataApertura;

	/**
	 * Data chisusura.
	 */
	private Date dataChiusura;

	/**
	 * Stato.
	 */
	private FilenetStatoFascicoloEnum stato;

	/**
	 * Data ultimo evento.
	 */
	private Date dataUltimoEvento;

	/**
	 * Descrizione ultimo evento.
	 */
	private String descrizioneUltimoEvento;
	
	/**
	 * Identificativo provvedimento.
	 */
	private String idProvvedimento;

	/**
	 * Numero protocollo mittente.
	 */
	private Integer numeroProtocolloMittente;

	/**
	 * Data protocollo mittente.
	 */
	private Date dataProtocolloMittente;

	/**
	 * Restituisce il numero pratica.
	 * @return numero pratica
	 */
	public Integer getIdDocumento() {
		return idDocumento;
	}
	
	/**
	 * Restituisce l'id del documento.
	 * @param idDocumento
	 */
	public void setIdDocumento(final Integer idDocumento) {
		this.idDocumento = idDocumento;
	}

	/**
	 * Restituisce il numero pratica.
	 * @return numero pratica
	 */
	public Integer getNumPratica() {
		return numPratica;
	}

	/**
	 * Imposta il numero pratica.
	 * @param numPratica
	 */
	public void setNumPratica(final Integer numPratica) {
		this.numPratica = numPratica;
	}

	/**
	 * Restituisce la descrizione.
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Imposta la descrizione.
	 * @param descrizione
	 */
	public void setDescrizione(final String descrizione) {
		this.descrizione = descrizione;
	}

	/**
	 * Restituisce la data di apertura.
	 * @return data apertura
	 */
	public Date getDataApertura() {
		return dataApertura;
	}

	/**
	 * Imposta la data di apertura.
	 * @param dataApertura
	 */
	public void setDataApertura(final Date dataApertura) {
		this.dataApertura = dataApertura;
	}

	/**
	 * Restituisce la data chiusura.
	 * @return data chiusura
	 */
	public Date getDataChiusura() {
		return dataChiusura;
	}

	/**
	 * Imposta la data di chiusura.
	 * @param dataChiusura
	 */
	public void setDataChiusura(final Date dataChiusura) {
		this.dataChiusura = dataChiusura;
	}

	/**
	 * Restituisce lo stato FilenetStatoFascicoloEnum.
	 * @return stato fascicolo
	 */
	public FilenetStatoFascicoloEnum getStato() {
		return stato;
	}

	/**
	 * Imposta lo stato del fascicolo.
	 * @param stato
	 */
	public void setStato(final FilenetStatoFascicoloEnum stato) {
		this.stato = stato;
	}

	/**
	 * Restituisce la data ultimo evento.
	 * @return data ultimo evento
	 */
	public Date getDataUltimoEvento() {
		return dataUltimoEvento;
	}

	/**
	 * Imposta la data dell'ultimo evento.
	 * @param dataUltimoEvento
	 */
	public void setDataUltimoEvento(final Date dataUltimoEvento) {
		this.dataUltimoEvento = dataUltimoEvento;
	}

	/**
	 * Restituisce la descrizione dell'ultimo evento.
	 * @return descrizione ultimo evento
	 */
	public String getDescrizioneUltimoEvento() {
		return descrizioneUltimoEvento;
	}

	/**
	 * Imposta la descrizione dell'ultimo evento.
	 * @param descrizioneUltimoEvento
	 */
	public void setDescrizioneUltimoEvento(final String descrizioneUltimoEvento) {
		this.descrizioneUltimoEvento = descrizioneUltimoEvento;
	}
	
	/**
	 * Restituisce l'id del provvedimento.
	 * @return id provvedimento
	 */
	public String getIdProvvedimento() {
		return idProvvedimento;
	}

	/**
	 * Imposta l'id del provvedimento.
	 * @param idProvvedimento
	 */
	public void setIdProvvedimento(final String idProvvedimento) {

		this.idProvvedimento = idProvvedimento;
	}

	/**
	 * Restituisce il numero protocollo mittente.
	 * @return numero protocollo mittente
	 */
	public Integer getNumeroProtocolloMittente() {
		return numeroProtocolloMittente;
	}

	/**
	 * Imposta il numero protocollo mittente.
	 * @param numeroProtocolloMittente
	 */
	public void setNumeroProtocolloMittente(final Integer numeroProtocolloMittente) {
		this.numeroProtocolloMittente = numeroProtocolloMittente;
	}

	/**
	 * Restituisce la data di protocollazione mittente.
	 * @return data protocollo mittente
	 */
	public Date getDataProtocolloMittente() {
		return dataProtocolloMittente;
	}

	/**
	 * Imposta la data protocollo mittente.
	 * @param dataProtocolloMittente
	 */
	public void setDataProtocolloMittente(final Date dataProtocolloMittente) {
		this.dataProtocolloMittente = dataProtocolloMittente;
	}
	
}
