/**
 * 
 */
package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IAllaccioDocUscitaDAO;
import it.ibm.red.business.dto.AllaccioDocUscitaDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * @author VINGENITO
 *
 */
@Repository
public class AllaccioDocUscitaDAO extends AbstractDAO implements IAllaccioDocUscitaDAO {

	/**
	 * Messaggio errore recupero documenti.
	 */
	private static final String RETRIEVE_DOCUMENT_ERROR_MSG = "Errore durante il recupero dei documenti ";

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 4594199191029422959L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AllaccioDocUscitaDAO.class.getName());

	/**
	 * @see it.ibm.red.business.dao.IAllaccioDocUscitaDAO#insertDocEstendiVisibilita(java.lang.String,
	 *      java.lang.String, java.lang.Integer, java.lang.String, java.util.Date,
	 *      java.util.Date, java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public int insertDocEstendiVisibilita(final String docTitleUscita, final String docTitleIngresso, final Integer stato, final String errorMsg, final Date dataCreazione,
			final Date dataAggiornamento, final Integer idAOO, final Connection conn) {
		PreparedStatement ps = null;

		try {
			ps = conn.prepareStatement(
					"INSERT INTO ALLACCIO_DOC_USCITA (DOCTITLEUSCITA, DOCTITLEINGRESSO,STATO,ERROR_MSG,  DATA_CREAZIONE,DATA_AGG, ID_AOO ) VALUES (?, ?, ?, ?, ?, ?, ?)");
			int index = 1;
			ps.setString(index++, docTitleUscita);
			ps.setString(index++, docTitleIngresso);
			ps.setInt(index++, stato);
			ps.setString(index++, errorMsg);
			ps.setDate(index++, new java.sql.Date(dataCreazione.getTime()));
			ps.setDate(index++, new java.sql.Date(dataAggiornamento.getTime()));
			ps.setInt(index++, idAOO);

			return ps.executeUpdate();

		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'inserimento del documento allacciato: " + docTitleUscita, e);
			throw new RedException("Errore durante l'inserimento del documento allacciato: " + docTitleUscita, e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IAllaccioDocUscitaDAO#getAllaccioDocUscitaByAoo(java.lang.String,
	 *      java.lang.Integer, java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public List<AllaccioDocUscitaDTO> getAllaccioDocUscitaByAoo(final String documentTitleUscita, final Integer stato, final Integer idAoo, final Connection conn) {
		final List<AllaccioDocUscitaDTO> allaccioDocUscitaList = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {

			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT * ");
			sb.append("FROM ALLACCIO_DOC_USCITA WHERE DOCTITLEUSCITA = " + documentTitleUscita);
			if (stato != null) {
				sb.append(" AND STATO = " + stato);
			}
			sb.append(" AND ALLACCIO_DOC_USCITA.ID_AOO=" + idAoo);

			ps = conn.prepareStatement(sb.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				final AllaccioDocUscitaDTO allaccioDocUscita = new AllaccioDocUscitaDTO(rs.getString("DOCTITLEUSCITA"), rs.getString("DOCTITLEINGRESSO"), rs.getInt("STATO"),
						rs.getString("ERROR_MSG"), rs.getDate("DATA_CREAZIONE"), rs.getDate("DATA_AGG"), rs.getInt("ID_AOO"));
				allaccioDocUscitaList.add(allaccioDocUscita);
			}

		} catch (final Exception e) {
			LOGGER.error(RETRIEVE_DOCUMENT_ERROR_MSG, e);
			throw new RedException(RETRIEVE_DOCUMENT_ERROR_MSG, e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
		return allaccioDocUscitaList;
	}

	/**
	 * @see it.ibm.red.business.dao.IAllaccioDocUscitaDAO#selectDocumentTileToProcess(java.lang.Integer,
	 *      java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public String selectDocumentTileToProcess(final Integer stato, final Integer idAoo, final Connection conn) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String docTitleUscita = "";
		try {
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT DOCTITLEUSCITA FROM ALLACCIO_DOC_USCITA WHERE STATO=" + stato + " and ROWNUM=1 and ID_AOO=" + idAoo + " FOR UPDATE SKIP LOCKED");
			ps = conn.prepareStatement(sb.toString());
			rs = ps.executeQuery();

			while (rs.next()) {
				docTitleUscita = rs.getString("DOCTITLEUSCITA");
			}

		} catch (final Exception e) {
			LOGGER.error(RETRIEVE_DOCUMENT_ERROR_MSG, e);
			throw new RedException(RETRIEVE_DOCUMENT_ERROR_MSG, e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);

		}
		return docTitleUscita;
	}

	/**
	 * @see it.ibm.red.business.dao.IAllaccioDocUscitaDAO#updateStato(java.lang.String,
	 *      java.lang.Integer, java.lang.String, java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public int updateStato(final String docTitleUscita, final Integer statoFinale, final String errorMsg, final Integer idAoo, final Connection conn) {
		return updateStato(docTitleUscita, null, statoFinale, null, errorMsg, idAoo, conn);
	}

	/**
	 * @see it.ibm.red.business.dao.IAllaccioDocUscitaDAO#updateStato(java.lang.String,
	 *      java.lang.String, java.lang.Integer, java.lang.String,
	 *      java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public int updateStato(final String docTitleUscita, final String docTitleIngresso, final Integer statoFinale, final String errorMsg, final Integer idAoo,
			final Connection conn) {
		return updateStato(docTitleUscita, docTitleIngresso, statoFinale, null, errorMsg, idAoo, conn);
	}

	/**
	 * @see it.ibm.red.business.dao.IAllaccioDocUscitaDAO#updateStato(java.lang.String,
	 *      java.lang.String, java.lang.Integer, java.lang.Integer,
	 *      java.lang.String, java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public int updateStato(final String docTitleUscita, final String docTitleIngresso, final Integer statoFinale, final Integer statoIniziale, final String errorMsg,
			final Integer idAoo, final Connection conn) {
		int output = 0;
		PreparedStatement ps = null;

		try {
			String s = "UPDATE ALLACCIO_DOC_USCITA SET STATO = ?, ERROR_MSG = ? WHERE doctitleuscita = ? AND ID_AOO=?";
			if (statoIniziale != null) {
				s += " AND STATO = " + sanitize(statoIniziale);
			}
			if (docTitleIngresso != null) {
				s += " AND DOCTITLEINGRESSO =" + sanitize(docTitleIngresso);
			}
			ps = conn.prepareStatement(s);

			int index = 1;
			ps.setInt(index++, statoFinale);
			ps.setString(index++, errorMsg);
			ps.setString(index++, docTitleUscita);
			ps.setInt(index++, idAoo);

			output = ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'aggiornamento dello stato", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IAllaccioDocUscitaDAO#insertRiferimentoStorico(java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.Integer, java.lang.String,
	 *      java.util.Date, java.util.Date, java.sql.Connection).
	 */
	@Override
	public int insertRiferimentoStorico(final String docTitleUscita, final String nProtocolloRifStorico, final String idDocPrincipale, final Integer stato,
			final String errorMsg, final Date dataCreazione, final Date dataAggiornamento, final Connection conn) {
		PreparedStatement ps = null;

		try {

			ps = conn.prepareStatement(
					"INSERT INTO ALLACCIO_RIFERIMENTO_STORICO (DOCTITLEUSCITA, NPROTOCOLLONSD,IDFILEPRINCIPALE,DATA_CREAZIONE,DATA_AGG,STATO,ERROR_MSG,ID_AOO) VALUES (?,?,?,?,?,?,?,?)");
			int index = 1;
			ps.setString(index++, docTitleUscita);
			ps.setString(index++, nProtocolloRifStorico);
			ps.setString(index++, idDocPrincipale);
			ps.setInt(index++, stato);
			ps.setString(index++, errorMsg);
			ps.setDate(index++, new java.sql.Date(dataCreazione.getTime()));
			ps.setDate(index++, new java.sql.Date(dataAggiornamento.getTime()));

			return ps.executeUpdate();

		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'inserimento del documento nella tabella del riferimento storico: " + docTitleUscita, e);
			throw new RedException("Errore durante l'inserimento del documento nella tabella del riferimento storico: " + docTitleUscita, e);
		} finally {
			closeStatement(ps);
		}
	}
	// END VI
}
