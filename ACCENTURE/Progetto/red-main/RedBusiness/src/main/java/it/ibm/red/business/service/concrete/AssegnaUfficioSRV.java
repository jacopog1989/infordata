package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dao.ICambiaUfficioDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IRuoloDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.SignerInfoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ConfigurazioneFirmaAooEnum;
import it.ibm.red.business.enums.PostaEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.NodoUtenteRuolo;
import it.ibm.red.business.persistence.model.Ruolo;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.provider.ServiceTaskProvider.ServiceTask;
import it.ibm.red.business.service.IAssegnaUfficioSRV;
import it.ibm.red.business.service.ISecurityCambioIterSRV;
import it.ibm.red.business.service.ISecuritySRV;
import it.ibm.red.business.utils.PermessiUtils;

/**
 * Service per la gestione delle assegnazioni ufficio.
 */
@Service
public class AssegnaUfficioSRV extends AbstractService implements IAssegnaUfficioSRV {

	/**
	 * Serial version UID.
	 */
	private static final long serialVersionUID = 1191732777548149077L;

	/**
	 * Messaggio d'errore.
	 */
	private static final String ERRORE_NEL_RICARICARE_I_NODI_DALL_UTENTE = "Errore nel ricaricare i nodi dall'utente";

	/**
	 * Servizio cambio iter.
	 */
	@Autowired
	private ISecurityCambioIterSRV securityCambioIterSRV;

	/**
	 * Servizio srcurity.
	 */
	@Autowired
	private ISecuritySRV securitySRV;

	/**
	 * Dao nodo.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * Dao cambio ufficio.
	 */
	@Autowired
	private ICambiaUfficioDAO cambiaUfficioDAO;

	/**
	 * Dao gestione ruolo.
	 */
	@Autowired
	private IRuoloDAO ruoloDAO;

	/**
	 * Dao gestione utente.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AssegnaUfficioSRV.class.getName());

	/**
	 * Consente di effettuare l'assegnamento ad Ufficio Centrale di Protocollo dei
	 * documenti identificati dai <code> wobNumbers </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAssegnaUfficioFacadeSRV#assegnaUCP(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection)
	 */
	@Override
	@ServiceTask(name = "assegnaUfficioUCP")
	public final Collection<EsitoOperazioneDTO> assegnaUCP(final UtenteDTO utente,
			final Collection<String> wobNumbers) {
		return assegna(utente, wobNumbers, StringUtils.EMPTY, true);

	}

	/**
	 * Consente di effettuare l'assegnamento ad Ufficio Centrale di Protocollo dei
	 * documenti identificati dai <code> wobNumbers </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAssegnaUfficioFacadeSRV#assegnaUCP(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String)
	 */
	@Override
	@ServiceTask(name = "assegnaUfficioUCP")
	public final EsitoOperazioneDTO assegnaUCP(final UtenteDTO utente, final String wobNumber) {
		return assegna(utente, wobNumber, StringUtils.EMPTY, true);
	}

	/**
	 * Consente di effettuare l'assegnamento ad Ufficio Proponente dei documenti
	 * identificati dai <code> wobNumbers </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAssegnaUfficioFacadeSRV#assegnaUfficioProponente(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection, java.lang.String)
	 */
	@Override
	public final Collection<EsitoOperazioneDTO> assegnaUfficioProponente(final UtenteDTO utente,
			final Collection<String> wobNumbers, final String motivazione) {
		return assegna(utente, wobNumbers, motivazione, false);
	}

	/**
	 * Consente di effettuare l'assegnamento ad Ufficio Proponente del documento
	 * identificato dal <code> wobNumber </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAssegnaUfficioFacadeSRV#assegnaUfficioProponente(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String)
	 */
	@Override
	public final EsitoOperazioneDTO assegnaUfficioProponente(final UtenteDTO utente, final String wobNumber,
			final String motivazione) {
		return assegna(utente, wobNumber, motivazione, false);
	}

	/**
	 * Effettua l'assegnamento.
	 * 
	 * @param utente
	 *            utente in sessione
	 * @param wobNumbers
	 *            identificativi documenti
	 * @param motivazione
	 *            motivazione dell'assegnamento
	 * @param isUCP
	 *            true se ufficio centrale di protocollo, false altrimenti
	 * @return collezione degli esiti per ogni assegnamento
	 */
	private Collection<EsitoOperazioneDTO> assegna(final UtenteDTO utente, final Collection<String> wobNumbers,
			final String motivazione, final boolean isUCP) {
		Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();
		for (String wobNumber : wobNumbers) {
			esiti.add(assegna(utente, wobNumber, motivazione, isUCP));
		}
		return esiti;
	}

	/**
	 * Effettua l'assegnamento.
	 * 
	 * @param utente
	 *            utente in sessione
	 * @param wobNumber
	 *            identificativo documento
	 * @param motivazione
	 *            motivazione assegnamento
	 * @param isUCP
	 *            true se ufficio centrale di protocollo, false altrimenti
	 * @return esito assegnamento
	 */
	private EsitoOperazioneDTO assegna(final UtenteDTO utente, final String wobNumber, final String motivazione,
			final boolean isUCP) {
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		Connection connection = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		Integer idNodo = null;
		Integer numeroDoc = 0;
		String response = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			VWWorkObject wo = fpeh.getWorkFlowByWob(wobNumber, true);
			String documentTitle = TrasformerPE.getMetadato(wo,
					PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY))
					.toString();

			String[] arrayAssegnazioni = new String[1];
			if (isUCP) {
				idNodo = nodoDAO.getNodoUCP(utente.getIdUfficio(), connection).intValue();
				// questa gestione viene fatta perchè all'interno del PE sono presenti due
				// response con lo stesso nome ma con lettere Maiuscole differenti
				response = fpeh.handleDuplicateResponses(wo, ResponsesRedEnum.ASSEGNA_UFFICIO_UCP);
			} else {
				idNodo = (Integer) TrasformerPE.getMetadato(wo,
						PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_NODO_START_WF_METAKEY));
				response = ResponsesRedEnum.INVIA_UFFICIO_PROPONENTE.getResponse();
			}
			arrayAssegnazioni[0] = idNodo + ",0";

			Document doc = fceh.getDocumentByIdGestionale(documentTitle, null, null, utente.getIdAoo().intValue(), null,
					null);
			numeroDoc = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
			ListSecurityDTO securities = securitySRV.getSecurityPerRiassegnazioneFascicolo(utente,
					documentTitle, arrayAssegnazioni, null, false, connection);
			securitySRV.modificaSecurityFascicoli(utente, securities, documentTitle);

			if (doc != null) {
				securitySRV.updateSecurity(utente, documentTitle, null, securities, true);

				Map<String, Object> map = new HashMap<>();
				map.put(PropertiesProvider.getIstance()
						.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), motivazione);
				map.put(PropertiesProvider.getIstance().getParameterByKey(
						PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId().intValue());
				map.put(PropertiesProvider.getIstance()
						.getParameterByKey(PropertiesNameEnum.DATA_ASSEGNAZIONE_WF_METAKEY),
						new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
				if (!isUCP) {
					map.put(PropertiesProvider.getIstance().getParameterByKey(
							PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), utente.getIdUfficio().intValue());
					map.put(PropertiesProvider.getIstance()
							.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY), idNodo);
				}

				if (response != null && fpeh.nextStep(wo, map, response)) {
					securityCambioIterSRV.aggiornaSecurityDocumentiAllacciati(documentTitle, securities, connection,
							arrayAssegnazioni, utente);
					securityCambioIterSRV.aggiornaSecurityContributiInseriti(documentTitle, connection,
							arrayAssegnazioni, utente);
				} else {
					throw new RedException("Errore nell'avanzamento del workflow.");
				}
			}
			esito.setIdDocumento(numeroDoc);
			esito.setEsito(true);
			esito.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
			commitConnection(connection);
			return esito;
		} catch (Exception e) {
			rollbackConnection(connection);
			LOGGER.error("Errore nella response di invio ad ufficio. ", e);
			esito.setEsito(false);
			esito.setIdDocumento(numeroDoc);
			esito.setNote("Errore nella response di invio ad ufficio. " + e.getMessage());
		} finally {
			closeConnection(connection);
			popSubject(fceh);
			logoff(fpeh);
		}

		return esito;
	}

	/**
	 * Restituisce i nodi associati all'utente identificato dall'
	 * <code> idUtente </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAssegnaUfficioFacadeSRV#getNodiFromIdUtenteandIdAOO(java.lang.Long,
	 *      java.lang.Long)
	 */
	@Override
	public List<Nodo> getNodiFromIdUtenteandIdAOO(final Long idUtente, final Long idAOO) {
		Connection connection = null;
		List<Nodo> listaNodi = new ArrayList<>();

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			List<Long> idNodi = cambiaUfficioDAO.getIdNodiFromIdUtenteandIdAOO(idUtente, idAOO, connection);
			for (Long nodoId : idNodi) {
				Nodo nodo = nodoDAO.getNodo(nodoId, connection);
				if (nodo.getDataDisattivazione() == null) {
					listaNodi.add(nodo);
				}
			}
			return listaNodi;
		} catch (SQLException e) {
			LOGGER.error(ERRORE_NEL_RICARICARE_I_NODI_DALL_UTENTE, e);
			throw new RedException(ERRORE_NEL_RICARICARE_I_NODI_DALL_UTENTE + e.getMessage());
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Restituisce i ruoli associati all'utente identificato dall'
	 * <code> idUtente </code> e dall' <code> idNodo </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAssegnaUfficioFacadeSRV#getRuoliFromIdNodoAndIdAOO(java.lang.Long,
	 *      java.lang.Long, java.lang.Long)
	 */
	@Override
	public List<Ruolo> getRuoliFromIdNodoAndIdAOO(final Long idUtente, final Long idNodo, final Long idAOO) {
		Connection connection = null;
		List<Ruolo> listaRuoli = new ArrayList<>();

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			List<Long> idRuoli = cambiaUfficioDAO.getIdRuoloFromIdUtenteAndIdNodoAndIdAOO(idUtente, idNodo.intValue(),
					idAOO, connection);
			for (Long ruoloId : idRuoli) {
				Ruolo ruolo = ruoloDAO.getRuolo(ruoloId, connection);
				if (ruolo.getDataDisattivazione() == null) {
					listaRuoli.add(ruolo);
				}
			}
			return listaRuoli;
		} catch (SQLException e) {
			LOGGER.error(ERRORE_NEL_RICARICARE_I_NODI_DALL_UTENTE, e);
			throw new RedException(ERRORE_NEL_RICARICARE_I_NODI_DALL_UTENTE + e.getMessage());
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Restituisce le AOO associate all'utente identificato dall'
	 * <code> idUtente </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAssegnaUfficioFacadeSRV#getAOOFromIdUtente(java.lang.Long)
	 */
	@Override
	public List<Aoo> getAOOFromIdUtente(final Long idUtente) {
		Connection connection = null;
		List<Aoo> listaAOO = new ArrayList<>();
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			listaAOO = cambiaUfficioDAO.getAOOfromIdUtente(idUtente, connection);
		} catch (SQLException e) {
			LOGGER.error(ERRORE_NEL_RICARICARE_I_NODI_DALL_UTENTE, e);
			throw new RedException(ERRORE_NEL_RICARICARE_I_NODI_DALL_UTENTE + e.getMessage());
		} finally {
			closeConnection(connection);
		}
		return listaAOO;
	}

	/**
	 * Effettua il popolamento dell'utente.
	 * 
	 * @see it.ibm.red.business.service.facade.IAssegnaUfficioFacadeSRV#popolaUtente(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.persistence.model.Nodo,
	 *      it.ibm.red.business.persistence.model.Ruolo)
	 */
	@Override
	public void popolaUtente(final UtenteDTO utente, final Nodo ufficioUtente, final Ruolo ruoloUtente) {

		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			NodoUtenteRuolo nodoUtenteRuolo = utenteDAO.getNodoUtenteRuolo(utente.getId(), ufficioUtente.getIdNodo(),
					ruoloUtente.getIdRuolo(), connection);
			popolaUtente(utente, ufficioUtente, ruoloUtente, nodoUtenteRuolo.getGestioneApplicativa() > 0);
		} catch (Exception e) {
			LOGGER.error("Errore nel recupero del nodo utente ruolo", e);
			throw new RedException("Errore nel recupero del nodo utente ruolo", e);
		} finally {
			closeConnection(connection);
		}

	}

	/**
	 * Popola tutti i parametri dell'utente in ingresso.
	 * 
	 * @param utente
	 *            utente in sessione
	 * @param ufficioUtente
	 *            ufficio dell'utente in sessione
	 * @param ruoloUtente
	 *            ruolo dell'utente in sessione
	 * @param gestioneApplicativa
	 *            true se l'utente è di gestione applicativa, false altrimenti
	 */
	public void popolaUtente(final UtenteDTO utente, final Nodo ufficioUtente, final Ruolo ruoloUtente,
			final boolean gestioneApplicativa) {
		if (ufficioUtente.getDirigente() != null) {
			utente.setDirigenteNodo(
					ufficioUtente.getDirigente().getCognome() + " " + ufficioUtente.getDirigente().getNome());
		}

		Aoo aooUfficio = ufficioUtente.getAoo();

		utente.setIdUfficio(ufficioUtente.getIdNodo()); // ID Ufficio
		utente.setCodiceUfficio(ufficioUtente.getCodiceNodo());
		utente.setUfficioDesc(getDescrizioneUfficioFromNodo(aooUfficio, ufficioUtente)); // Descrizione Ufficio
		utente.setNodoDesc(ufficioUtente.getDescrizione()); // Descrizione Nodo
		if (ufficioUtente.getFlagSegreteria() != null) {
			utente.setGiroVisti(ufficioUtente.getFlagSegreteria() > 0); // Giro Visti
		}
		utente.setFlagSegreteriaUfficio(ufficioUtente.getFlagSegreteria());
		utente.setIdTipoStrutturaUfficio(ufficioUtente.getIdTipoStruttura());

		utente.setPermessiAOO(aooUfficio.getParametriAOO());
		utente.setIdAoo(aooUfficio.getIdAoo()); // ID AOO
		utente.setAooDesc(aooUfficio.getDescrizione()); // Descrizione AOO
		utente.setCodiceAoo(aooUfficio.getCodiceAoo()); // Codice AOO
		utente.setIdLogoMefAoo(aooUfficio.getIdLogo()); // ID Logo MEF AOO
		utente.setIdNodoRadiceAOO(aooUfficio.getIdNodoRadice()); // ID Nodo Radice AOO
		utente.setDescrizioneEnte(aooUfficio.getEnte().getDescrizione()); // Descrizione Ente
		utente.setSavePinAoo(aooUfficio.isSalvaPin()); // Salva PIN AOO
		utente.setIdTipoProtocollo(aooUfficio.getTipoProtocollo().getIdTipoProtocollo()); // ID Tipo Protocollo
		utente.setMantieniFormatoOriginale(aooUfficio.getMantieniAllegatiOriginale()); // Mantienti allegati in formato
																						// originale
		utente.setFlagInoltraMail(aooUfficio.getFlagInoltraMail()); // flag inoltra mail
		utente.setFlagForzaRefreshCode(aooUfficio.getFlagForzaRefreshCode()); // flag forza refresh code
		utente.setTipoPosta(PostaEnum.getByValue(aooUfficio.getPostaInteropEsterna()));

		utente.setPermessoAooCorteDeiConti(PermessiUtils.isCorteDeiConti(aooUfficio.getParametriAOO())); // Permesso AOO
																											// Corte dei
																											// Conti
		utente.setPermessoAooFirmaCopiaConforme(PermessiUtils.hasCopiaConforme(aooUfficio.getParametriAOO())); // Permesso
																												// AOO
																												// Firma
																												// Copia
																												// Conforme
		utente.setIdRuoloDelegatoLibroFirma(aooUfficio.getIdRuoloDelegatoLibroFirma());
		utente.setOrdinamentoMailAOOAsc(aooUfficio.getOrdinamentoMailAOOAsc());
		utente.setRibaltaTitolario(aooUfficio.isRibaltaTitolario());
		utente.setAutocompleteRubricaCompleta(aooUfficio.isAutocompleteRubricaCompleta());
		utente.setStampaEtichetteResponse(aooUfficio.getStampaEtichetteResponse());
		utente.setProtocollaEMantieni(aooUfficio.getProtocollaEMantieni());
		utente.setDownloadCustomExcel(aooUfficio.getDownloadCustomExcel());
		utente.setInoltraResponse(aooUfficio.getInoltraResponse());
		utente.setRifiutaResponse(aooUfficio.getRifiutaResponse());
		utente.setFileNonSbustato(aooUfficio.getFileNonSbustato());
		utente.setStampigliaAllegatiVisible(aooUfficio.getStampigliaAllegatiVisible());
		utente.setIdNodoAssegnazioneIndiretta(aooUfficio.getIdNodoAssegnazioneIndiretta());
		utente.setUcb(aooUfficio.getFlagUCB() != 0);
		utente.setCheckUnivocitaMail(aooUfficio.getCheckUnivocitaMail());

		// Flag postilla
		utente.setPostillaAttiva(aooUfficio.getPostillaAttiva() != null && aooUfficio.getPostillaAttiva() != 0);
		utente.setPostillaSoloPag1(aooUfficio.getPostillaSoloPag1() != null && aooUfficio.getPostillaSoloPag1() != 0);

		utente.setIdUfficioPadre(ufficioUtente.getIdNodoPadre());
		utente.setMaxNotificheRubrica(aooUfficio.getMaxGiorniNotificheRubrica());
		utente.setMaxNotificheSottoscrizioni(aooUfficio.getMaxGiorniNotificheSottoscrizioni());
		utente.setMaxNotificheCalendario(aooUfficio.getMaxGiorniNotificheCalendario());

		utente.setMaxNotificheNonLette(aooUfficio.getMaxNotificheNonLette());

		// visibilità faldoni
		utente.setVisFaldoni(aooUfficio.getVisFaldoni());

		// Flag per DF
		utente.setShowDialogAoo(aooUfficio.getShowDialog());
		utente.setTimbroUscitaAoo(aooUfficio.getTimbroUscitaAoo());
		utente.setIsEstendiVisibilita(aooUfficio.getIsEstendiVisibilita());
		utente.setDisableUseHostOnly(aooUfficio.getDisableUseHostOnly());
		utente.setShowRiferimentoStorico(aooUfficio.getShowRiferimentoStorico());
		utente.setShowRicercaNsd(aooUfficio.getShowRicercaNsd());
		utente.setShowAllaccioNps(aooUfficio.isShowAllaccioNps());
		utente.setShowSelezionaTuttiVisto(aooUfficio.isShowSelezionaTuttiVisto());
		utente.setDownloadSistemiEsterni(aooUfficio.isDownloadSistemiEsterni());
		AooFilenet aooUfficioFilenet = aooUfficio.getAooFilenet();
		FilenetCredentialsDTO fcDTO = new FilenetCredentialsDTO(utente.getUsername().toUpperCase(),
				aooUfficioFilenet.getPassword(), aooUfficioFilenet.getUri(), aooUfficioFilenet.getStanzaJaas(),
				aooUfficioFilenet.getConnectionPoint(), aooUfficioFilenet.getObjectStore(),
				aooUfficioFilenet.getIdClientAoo());
		utente.setFcDTO(fcDTO);
		IFilenetCEHelper fceh = FilenetCEHelperProxy.newInstance(fcDTO, utente.getIdAoo());
		utente.setGruppiAD(fceh.getGruppiAD());
		popSubject(fceh);

		utente.setIdTipoNodo(ufficioUtente.getIdTipoNodo()); // ID Tipo Nodo
		utente.setIdNodoCorriere(ufficioUtente.getIdNodoCorriere()); // ID Nodo Corriere

		utente.setIdRuolo(ruoloUtente.getIdRuolo()); // ID Ruolo
		utente.setRuoloDesc(ruoloUtente.getNomeRuolo()); // Descrizione Ruolo
		utente.setPermessi(ruoloUtente.getPermessi().longValue()); // Permessi
		utente.setPermessoDSR(PermessiUtils.isDSR(ruoloUtente.getPermessi().longValue())); // Permesso DSR
		utente.setCorriere(PermessiUtils.isCorriere(ruoloUtente.getPermessi().longValue())); // Corriere
		utente.setLibroFirma(PermessiUtils.hasLibroFirma(ruoloUtente.getPermessi().longValue())); // Libro Firma
		utente.setTabScadenzario(PermessiUtils.isTabScadenzario(ruoloUtente.getPermessi().longValue())); // Tab
																											// Scadenzario
		if (PermessiUtils.isDelegato(ruoloUtente.getPermessi().longValue())) {
			utente.setIdUtenteDelegante(ufficioUtente.getDirigente().getIdUtente()); // ID Utente Delegante
		}

		utente.setMaxSizeEntrata(aooUfficio.getMaxSizeEntrata());
		utente.setMaxSizeUscita(aooUfficio.getMaxSizeUscita());

		if (utente.getSignerInfo() != null) {
			utente.getSignerInfo().setPkHandlerFirma(aooUfficio.getPkHandlerFirma());
			utente.getSignerInfo().setPkHandlerVerifica(aooUfficio.getPkHandlerVerifica());
		} else {
			utente.setSignerInfo(new SignerInfoDTO(aooUfficio.getPkHandlerFirma(), aooUfficio.getPkHandlerVerifica()));
		}

		utente.getSignerInfo()
				.setConfigurazioneFirma(ConfigurazioneFirmaAooEnum.getById(aooUfficio.getConfigurazioneFirma()));

		utente.setGestioneApplicativa(gestioneApplicativa);

	}

	/**
	 * Restituisce l'id del nodo corriere associato all'ufficio identificato dall'
	 * <code> idUfficio </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAssegnaUfficioFacadeSRV#getIdNodoCorriere(java.lang.Long)
	 */
	@Override
	public Long getIdNodoCorriere(final Long idUfficio) {
		Connection connection = null;
		Long idNodoCorriere = 0L;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			idNodoCorriere = nodoDAO.getNodoCorriereId(idUfficio, connection);
		} catch (SQLException e) {
			LOGGER.error("Errore nel recupero dell'ID Nodo Corriere per l'ufficio: " + idUfficio, e);
			throw new RedException("Errore nel recupero dell'ID Nodo Corriere per l'ufficio: " + idUfficio);
		} finally {
			closeConnection(connection);
		}

		return idNodoCorriere;
	}

	/**
	 * Restituisce il nodo identificato dall' <code> idNodo </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAssegnaUfficioFacadeSRV#getNodoById(java.lang.Long)
	 */
	@Override
	public Nodo getNodoById(final Long idNodo) {
		Nodo nodo = null;
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			nodo = nodoDAO.getNodo(idNodo, connection);
		} catch (Exception e) {
			LOGGER.error("Errore nel recupero dell'utente con idNodo =" + idNodo, e);
			throw new RedException("Errore durante il caricamento delle informazioni utente");
		} finally {
			closeConnection(connection);
		}
		return nodo;

	}

	/**
	 * Restituisce la descrizione dell'ufficio associato al <code> nodo </code>.
	 * 
	 * @see it.ibm.red.business.service.IAssegnaUfficioSRV#getDescrizioneUfficioFromNodo(it.ibm.red.business.persistence.model.Nodo)
	 */
	@Override
	public String getDescrizioneUfficioFromNodo(final Nodo nodo) {
		return getDescrizioneUfficioFromNodo(nodo.getAoo(), nodo);
	}

	/**
	 * Restituisce la descrizione dell'ufficio.
	 * 
	 * @param aoo
	 *            - Aoo del nodo.
	 * @param nodo
	 *            - Nodo dell'ufficio.
	 * @return Descrizione dell'ufficio.
	 */
	private static String getDescrizioneUfficioFromNodo(final Aoo aoo, final Nodo nodo) {
		String output = "";

		if (nodo != null) {
			output = "[" + aoo.getEnte().getDescrizione() + " - " + aoo.getCodiceAoo() + "] " + nodo.getDescrizione();
		}

		return output;
	}
}