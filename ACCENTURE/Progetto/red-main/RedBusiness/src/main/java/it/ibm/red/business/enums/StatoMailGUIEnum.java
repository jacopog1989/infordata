package it.ibm.red.business.enums;

/**
 * Enum stato mail visualizzato sulla GUI.
 * 
 * @author CRISTIANOPierascenzi
 *
 */
public enum StatoMailGUIEnum {

	/**
	 * Mail in arrivo.
	 */
	INARRIVO(1, "In arrivo", "0"),

	/**
	 * Mail eliminate.
	 */
	ELIMINATA(2, "Eliminata", "4"),

	/**
	 * Importata.
	 */
	IMPORTATA(3, "Importata", null),
	
	/**
	 * Mail inoltrate.
	 */
	INOLTRATA(5, "Inoltrata", "3"),
	
	/**
	 * Rifiutata.
	 */
	RIFIUTATA(6, "Rifiutata", null),

	/**
	 * Archiviata.
	 */
	ARCHIVIATA(7, "Archiviata", null),
	
	/**
	 * In fase di protocollazione manuale.
	 */
	IN_FASE_DI_PROTOCOLLAZIONE_MANUALE(9, "In fase di protocollazione manuale", null),

	/**
	 * In fase di protocollazione automatica.
	 */
	IN_FASE_DI_PROTOCOLLAZIONE_AUTOMATICA(10, "In fase di protocollazione automatica", null),
	
	/**
	 * In arrivo non protocollabile automaticamente.
	 */
	INARRIVO_NON_PROTOCOLLABILE_AUTOMATICAMENTE(13, "In arrivo, non protocollabile automaticamente", null),
	
	/**
	 * Mail in uscita.
	 */
	INUSCITA(20, "In uscita", "1"),
	
	/**
	 * Rifiutata automaticamente.
	 */
	RIFAUTO(21, "Rifiutata automaticamente", "2");
	
	
	/**
	 * Value.
	 */
	private Integer value;

	/**
	 * Label.
	 */
	private String label;
	
	/**
	 * Id item GUI.
	 */
	private String idGUI;

	/**
	 * Costruttore.
	 * 
	 * @param inValue	value
	 * @param inLabel	label
	 */
	StatoMailGUIEnum(final Integer inValue, final String inLabel, final String inIdGUI) {
		value = inValue;
		label = inLabel;
		idGUI = inIdGUI;
	}

	/**
	 * Getter.
	 * 
	 * @return	value
	 */
	public Integer getValue() {
		return value;
	}

	/**
	 * Getter.
	 * 
	 * @return	label
	 */
	public String getLabel() {
		return label;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	idGUI
	 */
	public String getIdGUI() {
		return idGUI;
	}

	/**
	 * Metodo per il recupero di un enum a partire da un valore.
	 * 
	 * @param value	valore
	 * @return		enum
	 */
	public static StatoMailGUIEnum get(final Integer value) {
		StatoMailGUIEnum output = null;
		for (StatoMailGUIEnum sme:StatoMailGUIEnum.values()) {
			if (sme.getValue().equals(value)) {
				output = sme;
			}
		}
		return output;
	}
	
}
