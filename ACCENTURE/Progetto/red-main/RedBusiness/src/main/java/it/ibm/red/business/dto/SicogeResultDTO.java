package it.ibm.red.business.dto;

import java.util.HashMap;
import java.util.Map;

/**
 * DTO che definisce i risultati SICOGE.
 */
public class SicogeResultDTO extends AbstractDTO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -5255637691724939631L;
	
	/**
	 * Risultato.
	 */
	private final Map<String, Map<String, Boolean>> result;
	
	/**
	 * Costruttore DTO.
	 */
	public SicogeResultDTO() {
		result = new HashMap<>();
	}
	
	/**
	 * Aggiunge il decreto da lavorare identificato dall'id alla lista dei risultati SICOGE.
	 * @param idDecretoDaLavorare
	 * @param idOP
	 * @param bOK
	 */
	public void add(final String idDecretoDaLavorare, final String idOP, final Boolean bOK) {
		result.computeIfAbsent(idDecretoDaLavorare, k -> new HashMap<>()).put(idOP, bOK);
	}

	/**
	 * Restituisce la lista dei risultati.
	 * @return risultati
	 */
	public Map<String, Map<String, Boolean>> getResult() {
		return result;
	}

}
