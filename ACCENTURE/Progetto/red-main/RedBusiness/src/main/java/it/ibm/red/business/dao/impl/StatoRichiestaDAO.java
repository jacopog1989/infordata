package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IStatoRichiestaDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.persistence.model.StatoRichiesta;

/**
 * The Class StatoRichiestaDAO.
 *
 * @author CPIERASC
 * 
 * 	Dao per la gestione dello stato della richiesta.
 */
@Repository
public class StatoRichiestaDAO extends AbstractDAO implements IStatoRichiestaDAO {

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Gets the all.
	 *
	 * @param connection the connection
	 * @return the all
	 */
	@Override
	public final Collection<StatoRichiesta> getAll(final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection<StatoRichiesta> stati = new ArrayList<>();
		try {
			ps = connection.prepareStatement("SELECT sr.* FROM StatoRichiesta sr");
			rs = ps.executeQuery();
			while (rs.next()) {
				stati.add(new StatoRichiesta(rs.getInt("IDSTATORICHIESTA"), rs.getString("DESCRIZIONE"), rs.getString("STEPNAME")));
			}
		} catch (SQLException e) {
			throw new RedException("Errore durante il recupero dei stati richiesta ", e);
		} finally {
			closeStatement(ps, rs);
		}
		return stati;
	}

}
