package it.ibm.red.business.dao.impl;

import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IPostaNpsDAO;
import it.ibm.red.business.dto.AssegnatarioInteropDTO;
import it.ibm.red.business.dto.MessaggioPostaNpsDTO;
import it.ibm.red.business.dto.MessaggioPostaNpsFlussoDTO;
import it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO;
import it.ibm.red.business.enums.StatoElabMessaggioPostaNpsEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.FileUtils;


/**
 *
 * DAO per la gestione dei messaggi di posta NPS.
 * 
 * @author m.crescentini
 */
@Repository
public class PostaNpsDAO extends AbstractDAO implements IPostaNpsDAO {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -716643970571625260L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(PostaNpsDAO.class.getName());
	
	/**
	 * @see it.ibm.red.business.dao.IPostaNpsDAO#inserisciRichiestaElabMessaggioInCoda(it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO,
	 *      java.sql.Connection).
	 */
	@Override
	public <T extends MessaggioPostaNpsDTO> long inserisciRichiestaElabMessaggioInCoda(final RichiestaElabMessaggioPostaNpsDTO<T> richiesta, final Connection con) {
		long idRichiesta = 0;
		PreparedStatement ps = null;
		final PreparedStatement blobPS = null;
		final ResultSet rs = null;
		
		try {
			String codiceFlusso = null;
			if (richiesta.getMessaggio() instanceof MessaggioPostaNpsFlussoDTO) {
				codiceFlusso = ((MessaggioPostaNpsFlussoDTO) richiesta.getMessaggio()).getDatiFlusso().getCodiceFlusso();
			}
			
			// Sequence per l'ID della richiesta da inserire
			idRichiesta = getNextId(con, "SEQ_CODA_NPS_POSTA_ING");
			
			ps = con.prepareStatement("INSERT INTO REDBATCH_CODA_NPS_POSTA_ING "
					+ "(IDCODA, MESSAGEIDNPS, MESSAGGIO, TIPOMESSAGGIO, STATO, IDAOO, IDTRACK, CODICE_FLUSSO, NUMEROPROTOCOLLO) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
			
			ps.setLong(1, idRichiesta);
			ps.setString(2, richiesta.getMessageIdNps());
			final Blob blobMessaggio = con.createBlob();
			blobMessaggio.setBytes(1, FileUtils.getByteObject(richiesta.getMessaggio()));
			ps.setBlob(3, blobMessaggio);
			ps.setString(4, richiesta.getMessaggio().getTipoIngresso().toString());
			ps.setInt(5, StatoElabMessaggioPostaNpsEnum.DA_ELABORARE.getId());
			ps.setLong(6, richiesta.getIdAoo());
			ps.setLong(7, richiesta.getIdTrack());
			ps.setString(8, codiceFlusso);
			if(richiesta.getMessaggio()!=null &&  richiesta.getMessaggio().getProtocollo()!=null) {
				ps.setLong(9, richiesta.getMessaggio().getProtocollo().getNumeroProtocollo());
			}else {
				ps.setNull(9, Types.INTEGER);
			}
			
			// Insert della richiesta
			final int result = ps.executeUpdate();
			
			if (result > 0) {
               LOGGER.info("Richiesta di elaborazione di un messaggio di posta NPS in ingresso inserita in coda. ID coda: " + idRichiesta);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException("Errore durante l'inserimento di un item nella coda di elaborazione dei messaggi di posta NPS: " + e.getMessage(), e);
		} finally {
			closeStatement(ps, rs);
			closeStatement(blobPS);
		}
		
		return idRichiesta;
	}

	/**
	 * @see it.ibm.red.business.dao.IPostaNpsDAO#getAndLockRichiestaElabMessaggio(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public <T extends MessaggioPostaNpsDTO> RichiestaElabMessaggioPostaNpsDTO<T> getAndLockRichiestaElabMessaggio(final Long idAoo, final Connection con) {
		RichiestaElabMessaggioPostaNpsDTO<T> richiestaElabMessaggio = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			ps = con.prepareStatement("SELECT * FROM REDBATCH_CODA_NPS_POSTA_ING WHERE IDAOO = ? AND STATO = ? ORDER BY DATAINSERIMENTO FOR UPDATE SKIP LOCKED");
			ps.setLong(index++, idAoo);
			ps.setLong(index++, StatoElabMessaggioPostaNpsEnum.DA_ELABORARE.getId());
			ps.setMaxRows(1);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				richiestaElabMessaggio = populateItem(rs);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException("Errore durante il recupero del prossimo item da lavorare dalla coda di elaborazione dei messaggi di posta NPS.", e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return richiestaElabMessaggio;
	}
	
	/**
	 * @see it.ibm.red.business.dao.IPostaNpsDAO#aggiornaStatoRichiestaElabMessaggioPostaNps(java.lang.Long,
	 *      int, java.sql.Connection).
	 */
	@Override
	public int aggiornaStatoRichiestaElabMessaggioPostaNps(final Long idCoda, final int stato, final Connection con) {
		return aggiornaStatoRichiestaElabMessaggioPostaNps(idCoda, stato, Constants.EMPTY_STRING, con);
	}
	
	/**
	 * @see it.ibm.red.business.dao.IPostaNpsDAO#aggiornaStatoRichiestaElabMessaggioPostaNps(java.lang.Long,
	 *      int, java.lang.String, java.sql.Connection).
	 */
	@Override
	public int aggiornaStatoRichiestaElabMessaggioPostaNps(final Long idCoda, final int stato, final String errore, final Connection con) {
		int result = 0;
		PreparedStatement ps = null;
		final ResultSet rs = null;
		
		try {
			ps = con.prepareStatement("UPDATE REDBATCH_CODA_NPS_POSTA_ING SET STATO = ?, ERRORE = ? WHERE IDCODA = ?");
			
			ps.setInt(1, stato);
			ps.setString(2, StringUtils.substring(errore, 0, 4000));
			ps.setLong(3, idCoda);
			
			result = ps.executeUpdate();
			
			LOGGER.info("Aggiornata ricezione messaggio " + idCoda + " con stato " + stato);
			
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'aggiornamento dell'item nella coda di elaborazione dei messaggi di posta NPS."
					+ " ID coda: " + idCoda, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return result;
	}
	
	/**
	 * @see it.ibm.red.business.dao.IPostaNpsDAO#checkAssegnatario(java.lang.Long,
	 *      java.lang.Long, java.lang.String, java.lang.String, java.sql.Connection)
	 * @param assegnatario
	 *            assegnatario da verificare
	 * @param con
	 *            connession al database
	 * @return true se verificato, false altrimenti
	 */
	@Override
	public boolean verificaPreAssegnatario(final AssegnatarioInteropDTO assegnatario, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean output = false;
		
		final StringBuilder query = new StringBuilder()
		.append("SELECT DISTINCT ute.idutente, ute.nome, ute.cognome, ute.username, ute.dataattivazione, ute.datadisattivazione, ute.email, ute.codfis, ")
		.append("ute.registroriservato, nod.idnodo, nod.idaoo, nod.descrizione ")
		.append("FROM utente ute, nodoutenteruolo nur, nodo no, ruolo ru, aoo ")
		.append("WHERE nur.idruolo = ru.idruolo AND nur.idutente = ute.idutente ")
		.append("AND nur.idnodo = no.idnodo ")
		.append("AND aoo.idaoo = no.idaoo ");
		
		if (assegnatario.getIdUfficio() != null) {
			query.append("AND no.idnodo = ").append(assegnatario.getIdUfficio()).append(" ");
		} else if (StringUtils.isNotBlank(assegnatario.getDescrizioneUfficio())) {
			query.append("AND LOWER(no.descrizione) LIKE '%").append(assegnatario.getDescrizioneUfficio().toLowerCase()).append("%' ");
		} else {
			// L'ufficio deve sempre essere presente
			throw new RedException("Ufficio non specificato nell'assegnatario comunicato nell'accordo di servizio interoperabile");
		}
		
		if (assegnatario.getIdUtente() != null) {
			query.append("AND ute.idutente = ").append(assegnatario.getIdUtente()).append(" ");
		} else if (StringUtils.isNotBlank(assegnatario.getDescrizioneUtente())) {
			final String[] descrizioneUtenteSplit = assegnatario.getDescrizioneUtente().split(",");
			
			query.append("AND (")
				.append("( LOWER(ute.cognome) = '").append(descrizioneUtenteSplit[0].trim().toLowerCase()).append("' ")
				.append(" OR LOWER(ute.cognome) = '").append(descrizioneUtenteSplit[1].trim().toLowerCase()).append("' ) ")
				.append("AND ")
				.append("( LOWER(ute.nome) = '").append(descrizioneUtenteSplit[0].trim().toLowerCase()).append("' ")
				.append(" OR LOWER(ute.nome) = '").append(descrizioneUtenteSplit[1].trim().toLowerCase()).append("' )")
				.append(") ");
		}

		query.append("AND nod.datadisattivazione IS NULL AND ru.datadisattivazione IS NULL ")
			.append("AND nur.datadisattivazione IS NULL AND ute.datadisattivazione IS NULL ")
			.append("ORDER BY ute.cognome");
		
		try {
			ps = con.prepareStatement(query.toString());
			rs = ps.executeQuery();
			
			if (rs.next()) {
				output = true;
			}
		} catch (final SQLException e) { 
			LOGGER.error(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IPostaNpsDAO#getDescrizioneById(it.ibm.red.business.dto.AssegnatarioInteropDTO,
	 *      java.sql.Connection).
	 */
	@Override
	public AssegnatarioInteropDTO getDescrizioneById(final AssegnatarioInteropDTO assegnatario, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		AssegnatarioInteropDTO descrPreassegnatario = null;
		
		final StringBuilder query = new StringBuilder()
		.append("SELECT DISTINCT ute.idutente, ute.nome, ute.cognome, ute.username, ute.dataattivazione, ute.datadisattivazione, ute.email, ute.codfis, ")
		.append("ute.registroriservato, nod.idnodo, nod.idaoo, nod.descrizione ")
		.append("FROM utente ute, nodoutenteruolo nur, nodo nod, ruolo ru, aoo ")
		.append("WHERE nur.idruolo = ru.idruolo AND nur.idutente = ute.idutente ")
		.append("AND nur.idnodo = nod.idnodo ")
		.append("AND aoo.idaoo = nod.idaoo ");
		
		if (assegnatario.getIdUfficio() != null) {
			query.append("AND nod.idnodo = ").append(assegnatario.getIdUfficio()).append(" ");
			if (assegnatario.getIdUtente() != null) {
				query.append("AND ute.idutente = ").append(assegnatario.getIdUtente()).append(" ");
			}
		}
		if (assegnatario.getDescrizioneUfficio() != null) {
			query.append("AND nod.descrizione = ").append(sanitize(assegnatario.getDescrizioneUfficio())).append(" ");
			if (assegnatario.getDescrizioneUtente() != null) { 
				String[] descrizioneSplit = null;
					if (assegnatario.getDescrizioneUtente().contains(",")) {
						descrizioneSplit = assegnatario.getDescrizioneUtente().split(",");
					} else {
						descrizioneSplit = assegnatario.getDescrizioneUtente().split(" ");
					} 
				query.append("AND (")
				.append("( LOWER(ute.cognome) = '").append(descrizioneSplit[0].trim().toLowerCase()).append("'")
				.append(" OR LOWER(ute.cognome) = '").append(descrizioneSplit[1].trim().toLowerCase()).append("')")
				.append("AND ")
				.append("( LOWER(ute.nome) = '").append(descrizioneSplit[0].trim().toLowerCase()).append("'")
				.append(" OR LOWER(ute.nome) = '").append(descrizioneSplit[1].trim().toLowerCase()).append("')")
				.append(") "); 
			}
		}
		query.append("AND nod.datadisattivazione IS NULL AND ru.datadisattivazione IS NULL ")
			.append("AND nur.datadisattivazione IS NULL AND ute.datadisattivazione IS NULL ")
			.append("ORDER BY ute.cognome");
		
		try {
			ps = con.prepareStatement(query.toString());
			rs = ps.executeQuery();
			
			if (rs.next()) {
				if ((assegnatario.getDescrizioneUfficio() != null && assegnatario.getDescrizioneUtente() == null) 
						|| assegnatario.getIdUfficio() != null && assegnatario.getIdUtente() == null) {
					descrPreassegnatario = new AssegnatarioInteropDTO(rs.getLong("IDNODO"), rs.getString("DESCRIZIONE"));
				} else {
					descrPreassegnatario = new AssegnatarioInteropDTO(rs.getLong("IDNODO"),
							rs.getLong("IDUTENTE"), rs.getString("DESCRIZIONE"), rs.getString("COGNOME") + " " + rs.getString("NOME"));
				}	
			}
		} catch (final SQLException e) { 
			LOGGER.error(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return descrPreassegnatario;
	}

	/**
	 * @param rs
	 * @return
	 * @throws SQLException 
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	private static <T extends MessaggioPostaNpsDTO> RichiestaElabMessaggioPostaNpsDTO<T> populateItem(final ResultSet rs) throws SQLException {
		T messaggio = null;
		
		final Blob blob = rs.getBlob("MESSAGGIO");
		if (blob != null) {
			messaggio = (T) FileUtils.getObjectFromByteArray(blob.getBytes(1, (int) blob.length()));
		}
		
		return new RichiestaElabMessaggioPostaNpsDTO<>(rs.getLong("IDCODA"), rs.getString("MESSAGEIDNPS"), messaggio, 
				StatoElabMessaggioPostaNpsEnum.get(rs.getInt("STATO")), rs.getLong("IDAOO"), rs.getLong("IDTRACK"));
	}
}