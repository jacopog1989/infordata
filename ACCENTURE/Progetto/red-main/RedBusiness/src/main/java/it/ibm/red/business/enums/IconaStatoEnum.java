package it.ibm.red.business.enums;

/**
 * The Enum IconaStatoEnum.
 *
 * @author CPIERASC
 * 
 *         Icona stato.
 */
public enum IconaStatoEnum {
	
	/**
	 * Icona visto.
	 */
	VISTO(1, "visto.png", IconaStatoEnum.VISTO_LABEL, IconaStatoEnum.VISTO_LABEL),
	
	/**
	 * Icona firma.
	 */
	FIRMA(2, "firma.png", "Firma", "Firma"),
	
	/**
	 * Icona firma autografa su red.
	 */
	FIRMA_AUTOGRAFA_SU_RED(3, "no_firma.png", "Firma autografa su Red", "Firma autografa su Red"),
	
	/**
	 * Icona sigla.
	 */
	SIGLA(4, "sigla.png", "Sigla", "Sigla"),
	
	/**
	 * Icona documento assegnazione interna.
	 */
	DOCUMENTO_ASSEGNAZIONE_INTERNA(5, "precensito.png", "Documento assegnazione interna", "Documento assegnazione interna"),
	
	/**
	 * Icona competenza.
	 */
	COMPETENZA(6, "competenza.png", "Competenza", "Competenza"),
	
	/**
	 * Icona conoscenza.
	 */
	CONOSCENZA(7, "conoscenza.png", "Conoscenza", "Conoscenza"),
	
	/**
	 * Icona contributo.
	 */
	CONTRIBUTO(8, "contributo.png", "Contributo", "Contributo"),
	
	/**
	 * Icona rifiuto assegnazione.
	 */
	RIFIUTO_ASSEGNAZIONE(9, IconaStatoEnum.RIFIUTO_PNG, "Rifiuto assegnazione", "Rifiuto assegnazione"),
	
	/**
	 * Icona sigla.
	 */
	RIFIUTO_SIGLA(10, IconaStatoEnum.RIFIUTO_PNG, "Rifiuto sigla", "Rifiuto sigla"),
	
	/**
	 * Icona visto.
	 */
	RIFIUTO_VISTO(11, IconaStatoEnum.RIFIUTO_PNG, "Rifiuto visto", "Rifiuto visto"),
	
	/**
	 * Icona firma.
	 */
	RIFIUTO_FIRMA(12, IconaStatoEnum.RIFIUTO_PNG, "Rifiuto firma", "Rifiuto firma"),
	
	/**
	 * Icona spedizione elettronica.
	 */
	SPEDIZIONE_ELETTRONICA(13, "sent_elettronici.png", IconaStatoEnum.SPEDIZIONE_ELETTRONICA_LABEL, IconaStatoEnum.SPEDIZIONE_ELETTRONICA_LABEL),
	
	/**
	 * Icona spedizione cartacea.
	 */
	SPEDIZIONE_CARTACEA(14, "sent.png", "Spedizione cartacea", "Spedizione cartacea"),
	
	/**
	 * Icona spedizione mista.
	 */
	SPEDIZIONE_MISTA(15, "sent_misti.png", IconaStatoEnum.SPEDIZIONE_MISTA_LABEL, IconaStatoEnum.SPEDIZIONE_MISTA_LABEL),
	
	/**
	 * Icona precensito.
	 */
	PRECENSITO(16, "precensito.png", "Precensito", "Precensito"),
	
	/**
	 * Icona documento in entrata chiuso senza risposta.
	 */
	DOCUMENTO_IN_ENTRATA_CHIUSO_SENZA_RISPOSTA(17, IconaStatoEnum.ENVELOPE_ICON_PNG, "Documento in entrata chiuso messo agli atti", "Documento in entrata chiuso messo agli atti"),
	
	/**
	 * Icona documento in entrata o uscita chiuso con risposta.
	 */
	DOCUMENTO_IN_ENTRATA_O_USCITA_CHIUSO_CON_RISPOSTA(18, IconaStatoEnum.ENVELOPE_ICON_PNG, "Documento in entrata o uscita chiuso con risposta", 
			"Documento in entrata o uscita chiuso con risposta"),
	
	/**
	 * Icona documento in uscita chiuso senza risposta.
	 */
	DOCUMENTO_IN_USCITA_CHIUSO_SENZA_RISPOSTA(19, IconaStatoEnum.ENVELOPE_ICON_PNG, "Documento in uscita chiuso in mozione", "Documento in uscita chiuso in mozione"),
	
	/**
	 * Icona sigla con alt e title "Visto".
	 */
	SIGLA_TITLE_VISTO(20, "sigla.png", IconaStatoEnum.VISTO_LABEL, IconaStatoEnum.VISTO_LABEL),
	
	/**
	 * Icona spedizione elettronica in errore.
	 */
	SPEDIZIONE_ELETTRONICA_ERRORE(21, "sent_elettronici_errore.png", IconaStatoEnum.SPEDIZIONE_ELETTRONICA_LABEL, IconaStatoEnum.SPEDIZIONE_ELETTRONICA_LABEL),
	
	/**
	 * Icona spedizione mista in errore.
	 */
	SPEDIZIONE_MISTA_ERRORE(22, "sent_misti_errore.png", IconaStatoEnum.SPEDIZIONE_MISTA_LABEL, IconaStatoEnum.SPEDIZIONE_MISTA_LABEL),
	
	/**
	 * Icona documento in attesa.
	 */
	DOCUMENTO_IN_ATTESA(23, "time.png", "Documento in attesa", "Documento in attesa"), 

	/**
	 * Icona doumento copia conforme.
	 */
	DOCUMENTO_IN_FIRMA_PER_COPIA_CONFORME(24, "copiaConforme.png", "Documento in firma per copia conforme", "Documento in firma per copia conforme"),
	
	/**
	 * Icona doumento copia conforme.
	 */
	COPIA_CONFORME(25, "copiaConforme.png", "Copia Conforme", "Copia Conforme"),
	
	/**
	 * Icona firma multipla.
	 */
	FIRMA_MULTIPLA(26, "firma_multipla.png", "Firma multipla", "Firma multipla"),
	
	/**
	 * Icona rifiuto firma multipla.
	 */
	RIFIUTO_FIRMA_MULTIPLA(27, IconaStatoEnum.RIFIUTO_PNG, "Rifiuto firma multipla", "Rifiuto firma multipla"),
	
	
	/**
	 * Icona spedizione elettronica in chiusa.
	 */
	SPEDIZIONE_ELETTRONICA_CHIUSA(28, "sent_elettronici_chiusa.png", IconaStatoEnum.SPEDIZIONE_ELETTRONICA_LABEL, IconaStatoEnum.SPEDIZIONE_ELETTRONICA_LABEL),
	
	/**
	 * Icona spedizione mista in chiusa.
	 */
	SPEDIZIONE_MISTA_CHIUSA(29, "sent_misti_chiusa.png", IconaStatoEnum.SPEDIZIONE_MISTA_LABEL, IconaStatoEnum.SPEDIZIONE_MISTA_LABEL),

	/**
	 * Icona spedizione elettronica in attesa_riconciliazione.
	 */
	SPEDIZIONE_ELETTRONICA_ATTESA_RICONCILIAZIONE(30, "sent_elettronici_attesa_riconciliazione.png", IconaStatoEnum.SPEDIZIONE_ELETTRONICA_LABEL, IconaStatoEnum.SPEDIZIONE_ELETTRONICA_LABEL),
	
	/**
	 * Icona spedizione mista in attesa_riconciliazione.
	 */
	SPEDIZIONE_MISTA_ATTESA_RICONCILIAZIONE(31, "sent_misti_attesa_riconciliazione.png", IconaStatoEnum.SPEDIZIONE_MISTA_LABEL, IconaStatoEnum.SPEDIZIONE_MISTA_LABEL),
	
	;
	
	private static final String VISTO_LABEL = "Visto";
	private static final String RIFIUTO_PNG = "rifiuto.png";
	private static final String SPEDIZIONE_ELETTRONICA_LABEL = "Spedizione elettronica";
	private static final String SPEDIZIONE_MISTA_LABEL = "Spedizione mista";
	private static final String ENVELOPE_ICON_PNG = "envelope-icon.png";
	 
	/**
	 * Valore.
	 */
	private Integer value;
	
	/**
	 * Nome file.
	 */
	private String filename;
	
	/**
	 * Testo alternativo.
	 */
	private String altText;
	
	/**
	 * Title.
	 */
	private String title;
	
	/**
	 * Costruttore.
	 * 
	 * @param inValue		valore
	 * @param inFilename	nome file
	 * @param inAltText		testo alternativo
	 * @param inTitle		title
	 */
	IconaStatoEnum(final Integer inValue, final String inFilename, final String inAltText, final String inTitle) {
		value = inValue;
		filename = inFilename;
		altText = inAltText;
		title = inTitle;
	}

	/**
	 * Getter.
	 * 
	 * @return	nome file
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * Getter.
	 * 
	 * @return	testo alternativo
	 */
	public String getAltText() {
		return altText;
	}

	/**
	 * Getter.
	 * 
	 * @return	title
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * Getter valore.
	 * 
	 * @return	valore
	 */
	public Integer getValue() {
		return value;
	}
}
