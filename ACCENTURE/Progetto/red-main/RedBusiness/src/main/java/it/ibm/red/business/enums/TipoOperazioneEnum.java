package it.ibm.red.business.enums;

import it.ibm.red.business.utils.StringUtils;

/**
 * Enum che definisce i tipi operazione.
 */
public enum TipoOperazioneEnum {
	
	/**
	 * Valore.
	 */
	MODIFICA_TIPO_CONTATTO("Modifica Tipo Contatto"),
	
	/**
	 * Valore.
	 */
	MODIFICA_NOME_CONTATTO("Modifica Ragione Sociale/Nome"),
	
	/**
	 * Valore.
	 */
	MODIFICA_COGNOME_CONTATTO("Modifica Cognome"),
	
	/**
	 * Valore.
	 */
	MODIFICA_DESCRIZIONE_ALIAS_CONTATTO("Modifica Descrizione/Alias"),
	
	/**
	 * Valore.
	 */
	MODIFICA_INDIRIZZO("Modifica Indirizzo"),
	
	/**
	 * Valore.
	 */
	MODIFICA_CAP_CONTATTO("Modifica Cap"),
	
	/**
	 * Valore.
	 */
	MODIFICA_TELEFONO_CONTATTO("Modifica Telefono"),
	
	/**
	 * Valore.
	 */
	MODIFICA_MAILPEO_CONTATTO("Modifica Mail"),
	
	/**
	 * Valore.
	 */
	MODIFICA_MAILPEC_CONTATTO("Modifica Mail PEC"),
	
	/**
	 * Valore.
	 */
	MODIFICA_FAX_CONTATTO("Modifica Fax"),
	
	/**
	 * Valore.
	 */
	MODIFICA_REGIONE_CONTATTO("Modifica Regione"),
	
	/**
	 * Valore.
	 */
	MODIFICA_PROVINCIA_CONTATTO("Modifica Provincia"),
	
	/**
	 * Valore.
	 */
	MODIFICA_COMUNE_CONTATTO("Modifica Comune"),
	
	/**
	 * Valore.
	 */
	ELIMINA_CONTATTO("Elimina Contatto"),
	
	/**
	 * Valore.
	 */
	CREAZIONE_AUTOMATICA_CONTATTO("Creazione Automatica Contatto"),
	
	/**
	 * Valore.
	 */
	MODIFICA_CELLULARE_CONTATTO("Modifica Cellulare"),
	
	/**
	 * Valore.
	 */
	RICHIESTA_CREAZIONE_CONTATTO("Creazione contatto"),
	
	/**
	 * Valore.
	 */
	MODIFICA_TITOLO_CONTATTO("Modifica titolo");
	
	
	/**
	 * Tipologia operazione.
	 */
	private String tipoOperazione;
	
	/**
	 * Costruttore.
	 * @param inTipoOperazione
	 */
	TipoOperazioneEnum(final String inTipoOperazione) {
		tipoOperazione = inTipoOperazione;
	}
	
	/**
	 * Restituisce il tipo operazione.
	 * @return tipo operazione
	 */
	public String getTipoOperazione() {
		return tipoOperazione;
	}
	
	/**
	 * Restituisce l'enum associato al tipo operazione in ingresso
	 * @param inTipoOperazione
	 * @return enum associato al tipo operazione
	 */
	public static TipoOperazioneEnum get(final String inTipoOperazione) {
		TipoOperazioneEnum output = null;
		for (TipoOperazioneEnum tipo : TipoOperazioneEnum.values()) {
			if (!StringUtils.isNullOrEmpty(tipo.getTipoOperazione())
					&& tipo.getTipoOperazione().equalsIgnoreCase(inTipoOperazione)) {
				output = tipo;
				break;
			}
		}
		return output;
	}
}