package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import filenet.vw.api.VWQueueQuery;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dao.ICodeApplicativeDAO;
import it.ibm.red.business.dao.IDocumentoDAO;
import it.ibm.red.business.dto.AssegnatarioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.SourceTypeEnum;
import it.ibm.red.business.enums.StatoLavorazioneEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.ICodeDocumentiSRV;
import it.ibm.red.business.service.IListaDocumentiSRV;

/**
 * Service che gestisce le funzionalità legate alle code documenti.
 */
@Service
@Component
public class CodeDocumentiSRV extends AbstractService implements ICodeDocumentiSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 5072215933806523575L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(CodeDocumentiSRV.class.getName());

	/**
	 * Servizio.
	 */
	@Autowired
	private IListaDocumentiSRV listaDocSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private ICodeApplicativeDAO codeApplicativeDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IDocumentoDAO documentoDAO;

	/**
	 * @see it.ibm.red.business.service.facade.ICodeDocumentiFacadeSRV#archiviaCodeApplicative().
	 */
	@Override
	public void archiviaCodeApplicative() {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			codeApplicativeDAO.archiviaCodeApplicative(connection);
			commitConnection(connection);
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di archiviazione delle code applicative", e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}

	}

	/**
	 * Recupera i document title dei documenti "in scadenza" (con la data scadenza
	 * valorizzata), suddivisi per coda. N.B. Sono prese in considerazione solo le
	 * code FileNet mostrate nella home page, accanto al Calendario.
	 * 
	 * @see it.ibm.red.business.service.facade.ICodeDocumentiFacadeSRV#getDocumentiInScadenzaCodeHomePage(it.ibm.red.business.dto.UtenteDTO)
	 */
	@Override
	public Map<String, List<String>> getDocumentiInScadenzaCodeHomePage(final UtenteDTO utente) {
		final Map<String, List<String>> output = new HashMap<>();
		Connection connection = null;
		FilenetPEHelper fpeh = null;

		try {
			VWQueueQuery queueQuery = null;
			VWWorkObject wo = null;
			List<String> documentTitlesInScadenza = null;
			final PropertiesProvider pp = PropertiesProvider.getIstance();

			// Si prendono in considerazione solo le seguenti code FileNet
			final List<DocumentQueueEnum> queuesScadenza = new ArrayList<>();
			if (utente.isUcb()) {
				queuesScadenza.add(DocumentQueueEnum.DA_LAVORARE_UCB);
				queuesScadenza.add(DocumentQueueEnum.IN_LAVORAZIONE_UCB);
			} else {
				queuesScadenza.add(DocumentQueueEnum.DA_LAVORARE);
			}

			queuesScadenza.add(DocumentQueueEnum.SOSPESO);
			queuesScadenza.add(DocumentQueueEnum.NSD);

			if (utente.getIdUfficio().equals(utente.getIdNodoAssegnazioneIndiretta())) {
				queuesScadenza.add(DocumentQueueEnum.CORRIERE_DIRETTO);
				queuesScadenza.add(DocumentQueueEnum.CORRIERE_INDIRETTO);
			} else {
				queuesScadenza.add(DocumentQueueEnum.CORRIERE);
			}

			connection = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			for (final DocumentQueueEnum queue : queuesScadenza) {
				if (queue.getType().equals(SourceTypeEnum.FILENET) && !"NSD_Roster".equals(queue.getName())) {
					documentTitlesInScadenza = new ArrayList<>();

					queueQuery = listaDocSRV.getQueueFilenetConScadenza(queue, utente, null, fpeh, connection);

					while (queueQuery.hasNext()) {
						wo = (VWWorkObject) queueQuery.next();

						final String documentTitleInScadenza = TrasformerPE.getMetadato(wo, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY)).toString();
						documentTitlesInScadenza.add(documentTitleInScadenza);
					}

					output.put(queue.getName(), documentTitlesInScadenza);
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero dei documenti in scadenza per le code mostrate nella home page.", e);
		} finally {
			closeConnection(connection);
			logoff(fpeh);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICodeDocumentiFacadeSRV#fromRecallToLavorate(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public void fromRecallToLavorate(final String wobNumber, final UtenteDTO utente) {
		Connection connection = null;
		FilenetPEHelper fpeh = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			final VWWorkObject wob = fpeh.getWorkFlowByWob(wobNumber, true);
			final Integer nIdDocumento = (Integer) TrasformerPE.getMetadato(wob,
					PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));

			if (nIdDocumento == null) {
				throw new RedException("ID documento non trovato per il documento da aggiornare.");
			}

			final String idDocumento = nIdDocumento.toString();

			codeApplicativeDAO.fromRecallToLavorate(idDocumento, connection);

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di aggiornamento del record dalla documentoutentestato ", e);
			throw new RedException("Errore in fase di aggiornamento del record dalla documentoutentestato " + e);
		} finally {
			closeConnection(connection);
			logoff(fpeh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ICodeDocumentiSRV#deleteRecallItem(java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public void deleteRecallItem(final Integer idDocumento, final Connection connection) {

		codeApplicativeDAO.deleteRecall(idDocumento, connection);

	}

	/**
	 * @see it.ibm.red.business.service.ICodeDocumentiSRV#checkChiusuraAllacci(java.lang.Integer,
	 *      java.util.List, java.sql.Connection).
	 */
	@Override
	public void checkChiusuraAllacci(final Integer idDocumento, final List<AssegnatarioDTO> assegnatari, final Connection connection) {
		codeApplicativeDAO.deleteAll(idDocumento, connection);
		for (final AssegnatarioDTO ass : assegnatari) {
			documentoDAO.inserisciStatoDocumentoLavorazione(idDocumento, ass.getIdUfficio(), 0L, StatoLavorazioneEnum.RISPOSTA, connection);
			if (ass.getIdUtente() != null && ass.getIdUtente() != 0L) {
				documentoDAO.inserisciStatoDocumentoLavorazione(idDocumento, ass.getIdUfficio(), ass.getIdUtente(), StatoLavorazioneEnum.RISPOSTA, connection);
			}
		}

	}

	/**
	 * @see it.ibm.red.business.service.ICodeDocumentiSRV#rimuoviDocumentoCodeApplicative(java.lang.String,
	 *      java.lang.Long, java.lang.Long).
	 */
	@Override
	public void rimuoviDocumentoCodeApplicative(final String documentTitle, final Long idUfficio, final Long idUtente) {
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			documentoDAO.rimuoviItemNodoUtente(Integer.parseInt(documentTitle), idUfficio, idUtente, con);
		} catch (final SQLException e) {
			LOGGER.error("Si è verificato un errore nella connessione verso il DB durante la cancellazione degli item relativi al nodo-utente per il workflow: ", e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ICodeDocumentiSRV#rimuoviDocumentoCodeApplicative(java.lang.String,
	 *      java.lang.Long).
	 */
	@Override
	public void rimuoviDocumentoCodeApplicative(final String documentTitle, final Long idUfficio) {
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			documentoDAO.rimuoviItemNodo(Integer.parseInt(documentTitle), idUfficio, con);
		} catch (final SQLException e) {
			LOGGER.error("Si è verificato un errore nella connessione verso il DB durante la cancellazione degli item relativi al nodo per il workflow: ", e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ICodeDocumentiSRV#aggiornaStatoDocumentoLavorazione(java.lang.String,
	 *      java.lang.Long, java.lang.Long,
	 *      it.ibm.red.business.enums.StatoLavorazioneEnum).
	 */
	@Override
	public void aggiornaStatoDocumentoLavorazione(final String documentTitle, final Long idUfficio, final Long idUtente, final StatoLavorazioneEnum statoLavorazione) {
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			documentoDAO.aggiornaStatoDocumentoLavorazione(Integer.parseInt(documentTitle), idUfficio, idUtente, statoLavorazione, con);
		} catch (final SQLException e) {
			LOGGER.error("Si è verificato un errore nella connessione verso il DB durante l'aggiornamento dello stato della lavorazione per il workflow: ", e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ICodeDocumentiSRV#annullaUtenteDocumento(java.lang.String,
	 *      java.lang.Long, java.lang.Long).
	 */
	@Override
	public void annullaUtenteDocumento(final String documentTitle, final Long idUfficio, final Long idUtente) {
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			documentoDAO.annullaUtenteDocumento(Integer.parseInt(documentTitle), idUfficio, idUtente, con);
		} catch (final SQLException e) {
			LOGGER.error("Si è verificato un errore nella connessione verso il DB durante l'aggiornamento dell'utente per il workflow: ", e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ICodeDocumentiSRV#hasItemNodo(java.lang.String,
	 *      java.lang.Long).
	 */
	@Override
	public boolean hasItemNodo(final String documentTitle, final Long idUfficio) {
		Connection con = null;
		boolean hasItemNodo = false;

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			hasItemNodo = documentoDAO.hasItemNodo(Integer.parseInt(documentTitle), idUfficio, con);
		} catch (final SQLException e) {
			LOGGER.error("Si è verificato un errore nella connessione verso il DB durante il conteggio degli item relativi al nodo per il workflow: ", e);
		} finally {
			closeConnection(con);
		}

		return hasItemNodo;
	}
}