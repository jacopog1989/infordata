package it.ibm.red.business.persistence.model;

import java.io.Serializable;

/**
 * Model dei Testi Default.
 */
public class TestiDefault implements Serializable {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -6623913382255499736L;

	/**
	 * Identificativo testo.
	 */
	private Integer idTesto;

	/**
	 * Nome del testo.
	 */
	private String nomeTesto;

	/**
	 * Titolo.
	 */
	private String titolo;

	/**
	 * Contenuto.
	 */
	private String contenuto;

	/**
	 * Costruttore vuoto.
	 */
	public TestiDefault() {
		super();
	}

	/**
	 * Costruttore di default.
	 * @param idTesto
	 * @param nomeTesto
	 * @param titolo
	 * @param contenuto
	 */
	public TestiDefault(final Integer idTesto, final String nomeTesto, final String titolo, final String contenuto) {
		this();
		this.idTesto = idTesto;
		this.nomeTesto = nomeTesto;
		this.titolo = titolo;
		this.contenuto = contenuto;
	}

	/**
	 * @return idTesto
	 */
	public Integer getIdTesto() {
		return idTesto;
	}

	/**
	 * Imposta l'id del testo.
	 * @param inIdTesto
	 */
	public void setIdTesto(final Integer inIdTesto) {
		this.idTesto = inIdTesto;
	}

	/**
	 * @return nomeTesto
	 */
	public String getNomeTesto() {
		return nomeTesto;
	}

	/**
	 * Imposta il nome del testo.
	 * @param inNomeTesto
	 */
	public void setNomeTesto(final String inNomeTesto) {
		this.nomeTesto = inNomeTesto;
	}

	/**
	 * @return titolo
	 */
	public String getTitolo() {
		return titolo;
	}

	/**
	 * Imposta il titolo del testo.
	 * @param inTitolo
	 */
	public void setTitolo(final String inTitolo) {
		this.titolo = inTitolo;
	}

	/**
	 * @return contenuto
	 */
	public String getContenuto() {
		return contenuto;
	}

	/**
	 * Imposta il contenuto del testo.
	 * @param inContenuto
	 */
	public void setContenuto(final String inContenuto) {
		this.contenuto = inContenuto;
	}
}
