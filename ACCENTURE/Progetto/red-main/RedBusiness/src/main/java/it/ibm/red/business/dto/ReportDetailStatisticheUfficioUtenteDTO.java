package it.ibm.red.business.dto;

import java.util.Date;

import it.ibm.red.business.enums.TargetNodoReportEnum;
import it.ibm.red.business.enums.TipoStatisticaUfficioReportEnum;

/**
 * DTO per il detail del report statistiche uffici.
 * 
 * @author a.dilegge
 *
 */
public class ReportDetailStatisticheUfficioUtenteDTO extends AbstractDTO implements Comparable<ReportDetailStatisticheUfficioUtenteDTO> {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Document title.
	 */
	private Integer documentTitle;

	/**
	 * Numero protcollo.
	 */
	private Integer numeroProtocollo;

	/**
	 * Data protocollo.
	 */
	private Date dataProtocollo;

	/**
	 * Numero documento.
	 */
	private Integer numeroDocumento;

	/**
	 * Anno documento.
	 */
	private Integer annoDocumento;

	/**
	 * Data creazione.
	 */
	private Date dataCreazione;

	/**
	 * Tipologia documento.
	 */
	private String tipologiaDocumento;

	/**
	 * Oggetto.
	 */
	private String oggetto;

	/**
	 * Ufficio assegnanate.
	 */
	private UfficioDTO ufficioAssegnante;

	/**
	 * Utente assegnante.
	 */
	private UtenteDTO utenteAssegnante;

	/**
	 * Ufficio assegnatario.
	 */
	private UfficioDTO ufficioAssegnatario;

	/**
	 * UTente assegnatario.
	 */
	private UtenteDTO utenteAssegnatario;

	/**
	 * Coda.
	 */
	private String coda;

	/**
	 * Data scadenza.
	 */
	private Date dataScadenza;
	

	/**
	 * Struttura.
	 */
	private NodoOrganigrammaDTO struttura;

	/**
	 * Tipo struttura.
	 */
	private TargetNodoReportEnum tipoStruttura;

	/**
	 * Anno.
	 */
	private String anno;

	/**
	 * Data da.
	 */
	private Date dataDa;

	/**
	 * Data a.
	 */
	private Date dataA;

	/**
	 * Tipo estrazione.
	 */
	private TipoStatisticaUfficioReportEnum tipoEstrazione;
	
	/**
	 * Costruttore di default.
	 */
	public ReportDetailStatisticheUfficioUtenteDTO() {
		super();
	}
	
	/**
	 * Costruttore.
	 * @param struttura
	 * @param tipoStruttura
	 * @param anno
	 * @param dataDa
	 * @param dataA
	 * @param tipoEstrazione
	 */
	public ReportDetailStatisticheUfficioUtenteDTO(final NodoOrganigrammaDTO struttura, final TargetNodoReportEnum tipoStruttura, final String anno, 
			final Date dataDa, final Date dataA, final TipoStatisticaUfficioReportEnum tipoEstrazione) {
		this();
		this.struttura = struttura;
		this.tipoStruttura = tipoStruttura;
		this.anno = anno;
		this.dataDa = dataDa;
		this.dataA = dataA;
		this.tipoEstrazione = tipoEstrazione;
	}

	/**
	 * Restituisce il documentTitle.
	 * @return documentTitle
	 */
	public Integer getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Imposta il documentTitle.
	 * @param documentTitle
	 */
	public void setDocumentTitle(final Integer documentTitle) {
		this.documentTitle = documentTitle;
	}

	/**
	 * Restituisce il numero del protocollo.
	 * @return numero protocollo
	 */
	public Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * Imposta il numero del protocollo.
	 * @param numeroProtocollo
	 */
	public void setNumeroProtocollo(final Integer numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}

	/**
	 * Restituisce la data di protocollo.
	 * @return data protocollo
	 */
	public Date getDataProtocollo() {
		return dataProtocollo;
	}

	/**
	 * Imposta la data di protocollo.
	 * @param dataProtocollo
	 */
	public void setDataProtocollo(final Date dataProtocollo) {
		this.dataProtocollo = dataProtocollo;
	}

	/**
	 * Restituisce il numero del documento.
	 * @return numero documento
	 */
	public Integer getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * Imposta il numero del documento.
	 * @param numeroDocumento
	 */
	public void setNumeroDocumento(final Integer numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * Restituisce l'anno del documento.
	 * @return anno documento
	 */
	public Integer getAnnoDocumento() {
		return annoDocumento;
	}

	/**
	 * Imposta l'anno del documento.
	 * @param annoDocumento
	 */
	public void setAnnoDocumento(final Integer annoDocumento) {
		this.annoDocumento = annoDocumento;
	}

	/**
	 * Restituisce la data di creazione.
	 * @return data creazione
	 */
	public Date getDataCreazione() {
		return dataCreazione;
	}

	/**
	 * Imposta la data di creazione.
	 * @param dataCreazione
	 */
	public void setDataCreazione(final Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	/**
	 * Restituisce la tipologia del documento.
	 * @return tipologia documento
	 */
	public String getTipologiaDocumento() {
		return tipologiaDocumento;
	}

	/**
	 * Imposta la tipologia del documento.
	 * @param tipologiaDocumento
	 */
	public void setTipologiaDocumento(final String tipologiaDocumento) {
		this.tipologiaDocumento = tipologiaDocumento;
	}

	/**
	 * Restituisce l'oggetto.
	 * @return oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * Imposta l'oggetto.
	 * @param oggetto
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}

	/**
	 * Restituisce l'ufficio assegnante.
	 * @return ufficio assegnante
	 */
	public UfficioDTO getUfficioAssegnante() {
		return ufficioAssegnante;
	}

	/**
	 * Imposta l'ufficio assegnante.
	 * @param ufficioAssegnante
	 */
	public void setUfficioAssegnante(final UfficioDTO ufficioAssegnante) {
		this.ufficioAssegnante = ufficioAssegnante;
	}

	/**
	 * Restituisce l'utente assegnante.
	 * @return utente assegnante
	 */
	public UtenteDTO getUtenteAssegnante() {
		return utenteAssegnante;
	}

	/**
	 * Imposta l'utente assegnante.
	 * @param utenteAssegnante
	 */
	public void setUtenteAssegnante(final UtenteDTO utenteAssegnante) {
		this.utenteAssegnante = utenteAssegnante;
	}

	/**
	 * Restituisce l'ufficio assegnante.
	 * @return ufficio assegnante
	 */
	public UfficioDTO getUfficioAssegnatario() {
		return ufficioAssegnatario;
	}

	/**
	 * Imposta l'ufficio assegnante.
	 * @param ufficioAssegnatario
	 */
	public void setUfficioAssegnatario(final UfficioDTO ufficioAssegnatario) {
		this.ufficioAssegnatario = ufficioAssegnatario;
	}

	/**
	 * Restituisce l'utente assegnatario.
	 * @return utente assegnatario
	 */
	public UtenteDTO getUtenteAssegnatario() {
		return utenteAssegnatario;
	}

	/**
	 * Imposta l'utente assegnatario.
	 * @param utenteAssegnatario
	 */
	public void setUtenteAssegnatario(final UtenteDTO utenteAssegnatario) {
		this.utenteAssegnatario = utenteAssegnatario;
	}

	/**
	 * Restituisce la coda.
	 * @return coda
	 */
	public String getCoda() {
		return coda;
	}

	/**
	 * Imposta la coda.
	 * @param coda
	 */
	public void setCoda(final String coda) {
		this.coda = coda;
	}

	/**
	 * Restituisce la dada di scadenza.
	 * @return data scadenza
	 */
	public Date getDataScadenza() {
		return dataScadenza;
	}

	/**
	 * Imposta la data di scadenza.
	 * @param dataScadenza
	 */
	public void setDataScadenza(final Date dataScadenza) {
		this.dataScadenza = dataScadenza;
	}

	/**
	 * Restituisce la struttura.
	 * @return struttura
	 */
	public NodoOrganigrammaDTO getStruttura() {
		return struttura;
	}

	/**
	 * Imposta la struttura.
	 * @param struttura
	 */
	public void setStruttura(final NodoOrganigrammaDTO struttura) {
		this.struttura = struttura;
	}

	/**
	 * Restituisce il tipo della struttura.
	 * @return tipo struttura
	 */
	public TargetNodoReportEnum getTipoStruttura() {
		return tipoStruttura;
	}

	/**
	 * Imposta il tipo della strutturua.
	 * @param tipoStruttura
	 */
	public void setTipoStruttura(final TargetNodoReportEnum tipoStruttura) {
		this.tipoStruttura = tipoStruttura;
	}

	/**
	 * Restituisce l'anno.
	 * @return anno
	 */
	public String getAnno() {
		return anno;
	}

	/**
	 * Imposta l'anno.
	 * @param anno
	 */
	public void setAnno(final String anno) {
		this.anno = anno;
	}

	/**
	 * Restituisce la data iniziale del range della data.
	 * @return data fine
	 */
	public Date getDataDa() {
		return dataDa;
	}

	/**
	 * Imposta la data iniziale del range della data.
	 * @param dataDa
	 */
	public void setDataDa(final Date dataDa) {
		this.dataDa = dataDa;
	}

	/**
	 * Restituisce la data finale del range della data.
	 * @return data finale
	 */
	public Date getDataA() {
		return dataA;
	}

	/**
	 * Imposta la data finale del range della data.
	 * @param dataA
	 */
	public void setDataA(final Date dataA) {
		this.dataA = dataA;
	}

	/**
	 * Restituisce il tipo estrazione.
	 * @return tipo estrazione
	 */
	public TipoStatisticaUfficioReportEnum getTipoEstrazione() {
		return tipoEstrazione;
	}

	/**
	 * Imposta il tipo estrazione.
	 * @param tipoEstrazione
	 */
	public void setTipoEstrazione(final TipoStatisticaUfficioReportEnum tipoEstrazione) {
		this.tipoEstrazione = tipoEstrazione;
	}

	/**
	 * @see java.lang.Object#hashCode().
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((documentTitle == null) ? 0 : documentTitle.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object).
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ReportDetailStatisticheUfficioUtenteDTO other = (ReportDetailStatisticheUfficioUtenteDTO) obj;
		if (documentTitle == null) {
			if (other.documentTitle != null) {
				return false;
			}
		} else if (!documentTitle.equals(other.documentTitle)) {
			return false;
		}
		return true;
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object).
	 */
	@Override
	public int compareTo(final ReportDetailStatisticheUfficioUtenteDTO o) {
		int comparison = 0;
		
		comparison = this.getDataCreazione().compareTo(o.getDataCreazione());
		

		if (comparison == 0) {
			comparison = this.numeroDocumento.compareTo(o.getNumeroDocumento());
		}

		return comparison;
	}
			
}
