package it.ibm.red.business.monitoring.attodecreto;

import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import it.ibm.red.business.logger.REDLogger;

/**
 * Handler dei WebServices dell'Atto Decreto.
 */
public class AttoDecretoWSClientHandler implements SOAPHandler<SOAPMessageContext> {
	
	/**
	 * Logger.
	 */
	private final REDLogger logger = REDLogger.getLogger(AttoDecretoWSClientHandler.class);

	/**
	 * Stringa associata alla request soap.
	 */
	public static final String SOAP_ENVELOPE_REQUEST = "SOAP_ENVELOPE_REQUEST";

	/**
	 * Stringa associata alla response soap.
	 */
	public static final String SOAP_ENVELOPE_RESPONSE = "SOAP_ENVELOPE_RESPONSE";

	/**
	 * @see javax.xml.ws.handler.Handler#handleMessage(javax.xml.ws.handler.MessageContext).
	 */
	@Override
	public boolean handleMessage(final SOAPMessageContext context) {
		try {
			final SOAPMessage soapMsg = context.getMessage();
	        final SOAPEnvelope soapEnv = soapMsg.getSOAPPart().getEnvelope();
	        final String soapEnvelopeString = soapEnv.toString();
			
			final Boolean isRequest = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
			if (Boolean.TRUE.equals(isRequest)) {
				context.put(SOAP_ENVELOPE_REQUEST, soapEnvelopeString);
				context.setScope(SOAP_ENVELOPE_REQUEST, MessageContext.Scope.APPLICATION);
			} else {
				context.put(SOAP_ENVELOPE_RESPONSE, soapEnvelopeString);
				context.setScope(SOAP_ENVELOPE_RESPONSE, MessageContext.Scope.APPLICATION);
			}
		} catch (final SOAPException e) {
			logger.error("Impossibile recuperare l'envelope", e);
		}
		
		return true;
	}

	/**
	 * @see javax.xml.ws.handler.Handler#handleFault(javax.xml.ws.handler.MessageContext).
	 */
	@Override
	public boolean handleFault(final SOAPMessageContext context) {
		return true;
	}

	/**
	 * @see javax.xml.ws.handler.Handler#close(javax.xml.ws.handler.MessageContext).
	 */
	@Override
	public void close(final MessageContext context) {
		// Metodo intenzionalmente vuoto.
	}

	/**
	 * @see javax.xml.ws.handler.soap.SOAPHandler#getHeaders().
	 */
	@Override
	public Set<QName> getHeaders() {
		return null;
	}

}
