package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.EstrattiContoUcbDTO;
import it.ibm.red.business.dto.FascicoloFlussoDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.ParamsRicercaContiConsuntiviDTO;
import it.ibm.red.business.dto.ParamsRicercaDocUCBAssegnanteAssegnatarioDTO;
import it.ibm.red.business.dto.ParamsRicercaEsitiDTO;
import it.ibm.red.business.dto.ParamsRicercaEstrattiContoDTO;
import it.ibm.red.business.dto.ParamsRicercaFlussiDTO;
import it.ibm.red.business.dto.RicercaAvanzataDocUcbFormDTO;
import it.ibm.red.business.dto.RisultatiRicercaUcbDTO;
import it.ibm.red.business.dto.RisultatoRicercaContiConsuntiviDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.StoricoDTO;

/**
 * The Interface IRicercaAvanzataDocUCBFacadeSRV.
 *
 * @author a.dilegge
 * 
 *         Facade del servizio per la gestione delle ricerche dei documenti custom per gli UCB.
 */
public interface IRicercaAvanzataDocUCBFacadeSRV extends Serializable {
	
	/**
	 * Gestisce la ricerca per assegnante/assegnatario
	 * 
	 * @param paramsRicerca
	 * @param utente
	 * @param orderbyInQuery
	 * @return
	 */
	RisultatiRicercaUcbDTO eseguiRicercaDocumentiAssegnanteAssegnatario(ParamsRicercaDocUCBAssegnanteAssegnatarioDTO paramsRicerca, UtenteDTO utente, boolean orderbyInQuery);
	
	/**
	 * Metodo utilizzato per popolare il form di ricerca Documenti.
	 * 
	 * @param utente
	 * @param ricercaAvanzataForm
	 */
	void initRicercaDoc(UtenteDTO utente, RicercaAvanzataDocUcbFormDTO ricercaAvanzataForm);

	/**
	 * Esegue la ricerca avanzata UCB.
	 * @param paramsRicercaFNet
	 * @param metadatiEstesi
	 * @param utente
	 * @return risultati della ricerca UCB
	 */
	RisultatiRicercaUcbDTO eseguiRicercaAvanzataUCB(ParamsRicercaAvanzataDocDTO paramsRicercaFNet, Collection<MetadatoDTO> metadatiEstesi, UtenteDTO utente);

	/**
	 * Esegue la ricerca degli esiti UCB.
	 * @param paramsRicerca
	 * @param utente
	 * @return risultati della ricerca UCB
	 */
	RisultatiRicercaUcbDTO eseguiRicercaEsitiUCB(ParamsRicercaEsitiDTO paramsRicerca, UtenteDTO utente);

	/**
	 * Esegue la ricerca dei flussi UCB.
	 * @param flussiUCB
	 * @param utente
	 * @return lista dei fascicoli del flusso
	 */
	List<FascicoloFlussoDTO> eseguiRicercaFlussiUCB(ParamsRicercaFlussiDTO flussiUCB, UtenteDTO utente);

	/**
	 * Ottiene lo storico del documento da flusso.
	 * @param idFascicolo
	 * @param utente
	 * @return storico
	 */
	Collection<StoricoDTO> getStoricoDocumentoFlusso(Integer idFascicolo, UtenteDTO utente);

	/**
	 * Esegue la ricerca tra i metadati per il monitoraggio
	 * degli estratti conto trimestrali CCVT.
	 * @param estrattiContoUCB
	 * @param utente
	 * @return risultati ricerca estratti conto trimestrali CCVT
	 */
	Collection<EstrattiContoUcbDTO> eseguiRicercaEstrattiContoUCB(ParamsRicercaEstrattiContoDTO estrattiContoUCB, UtenteDTO utente);

	/**
	 * Esegue la ricerca elenco notifiche e conti consuntivi.
	 * 
	 * @param contiConsuntiviUCB
	 * @param utente
	 * @return lista di risultati
	 */
	List<RisultatoRicercaContiConsuntiviDTO> eseguiRicercaContiConsuntiviUCB(ParamsRicercaContiConsuntiviDTO contiConsuntiviUCB, UtenteDTO utente);
}