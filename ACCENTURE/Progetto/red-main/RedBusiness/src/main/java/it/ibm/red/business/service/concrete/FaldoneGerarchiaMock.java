package it.ibm.red.business.service.concrete;

import java.util.Collection;
import java.util.Map;

import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.service.IFaldoneSRV;

/**
 * Gerarchia Faldone per MOCK.
 */
//@Service
public class FaldoneGerarchiaMock extends FaldoneSRV implements IFaldoneSRV {
	
	/**
	 * Srializzazione.
	 */
	private static final long serialVersionUID = 4003632926026562526L;

	/**
	 * Albero.
	 */
	private Map<String, Collection<FaldoneDTO>> treeTestMap;

	/**
	 * @see it.ibm.red.business.service.concrete.FaldoneSRV#getFaldoni(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.Integer, java.lang.String).
	 */
	@Override
	public Collection<FaldoneDTO> getFaldoni(final UtenteDTO utente, final Integer idUfficioSelezionato, final String parentName) {
		String key = idUfficioSelezionato.toString().concat(parentName);
		return treeTestMap.get(key);
	}
	
	/**
	 * Imposta il tree test.
	 * @param treeTestMap
	 */
	void setTreeTest(final Map<String, Collection<FaldoneDTO>> treeTestMap) {
		this.treeTestMap = treeTestMap;
	}
}
