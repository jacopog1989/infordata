package it.ibm.red.business.enums;

import it.ibm.red.business.utils.StringUtils;

/**
 * Enum che definisce il tipo di rubrica.
 */
public enum TipoRubricaEnum {
	
	/**
	 * Valore.
	 */
	RED("RED"),
	
	/**
	 * Valore.
	 */
	IPA("IPA"),
	
	/**
	 * Valore.
	 */
	MEF("MEF"),
	
	/**
	 * Valore.
	 */
	GRUPPO("GRUPPO"),
	
	/**
	 * Valore.
	 */
	INTERNO("INTERNO"),
	
	/**
	 * Valore.
	 */
	GRUPPOEMAIL("GRUPPOEMAIL");
	
	/**
	 * Tipo di rubrica.
	 */
	private String tipoRubrica;
	
	TipoRubricaEnum(final String inTipoRubrica) {
		this.tipoRubrica = inTipoRubrica;
	}
	
	/**
	 * Restituisce il tipo rubrica.
	 * @return tipo rubrica
	 */
	public String getTipoRubrica() {
		return tipoRubrica;
	}
	
	/**
	 * Restituisce l'enum associata al tipo rubrica.
	 * @param tipoRubrica
	 * @return enum associata al tipo rubrica
	 */
	public static TipoRubricaEnum get(final String tipoRubrica) {
		TipoRubricaEnum output = null;
		for (TipoRubricaEnum tipo : TipoRubricaEnum.values()) {
			if (!StringUtils.isNullOrEmpty(tipo.getTipoRubrica())
					&& tipo.getTipoRubrica().equalsIgnoreCase(tipoRubrica)) {
				output = tipo;
				break;
			}
		}
		return output;
	}
	

}
