package it.ibm.red.business.dto;

import java.util.Date;

import it.ibm.red.business.enums.DocumentQueueEnum;

/**
 * Oggetto usato nello scadenziario per tenere traccia dei documenti in scadenza.
 */
public class PEDocumentoScadenzatoDTO extends PEDocumentoAbsrtractDTO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4375267660400204737L;
	
	/**
	 * Data creazione
	 */
	private Date dataCreazione;
	
	/**
	 * Data di scadenza
	 */
	private Date dataScadenza;
	
	
	/**
	 * Costruttore PE documento scadenzato DTO.
	 *
	 * @param idDocumento the id documento
	 * @param wobNumber the wob number
	 * @param tipoAssegnazioneId the tipo assegnazione id
	 * @param codaCorrente the coda corrente
	 * @param queue the queue
	 * @param idNodoDestinatario the id nodo destinatario
	 * @param idUtenteDestinatario the id utente destinatario
	 * @param dataCreazione the data creazione
	 * @param inDatascadenza the in datascadenza
	 */
	public PEDocumentoScadenzatoDTO(final String idDocumento, final String wobNumber, final Integer tipoAssegnazioneId, final String codaCorrente,
			final DocumentQueueEnum queue, final Integer idNodoDestinatario, final Integer idUtenteDestinatario,
			final Date dataCreazione, final Date inDatascadenza) {
		super(idDocumento, wobNumber, tipoAssegnazioneId, codaCorrente, queue, idNodoDestinatario, idUtenteDestinatario);
		this.dataScadenza = inDatascadenza;
	}

	/** 
	 *
	 * @return the data scadenza
	 */
	public Date getDataScadenza() {
		return dataScadenza;
	}

	/** 
	 *
	 * @return the data creazione
	 */
	public Date getDataCreazione() {
		return dataCreazione;
	}
	
}
