package it.ibm.red.business.service;

import java.sql.Connection;
 
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.service.facade.ITimerRichiesteIntegrazioniFacadeSRV;

/**
 * Interfaccia del servizio che gestisce il timer per le richieste integrazioni.
 */
public interface ITimerRichiesteIntegrazioniSRV extends ITimerRichiesteIntegrazioniFacadeSRV {

	/**
	 * Registra la data di invio.
	 * @param idDocumento
	 * @param idAoo
	 * @param fceh
	 * @param con
	 */
	void registraDataInvio(String idDocumento, long idAoo, IFilenetCEHelper fceh, Connection con);

}
