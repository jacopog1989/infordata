package it.ibm.red.business.dao.impl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.IEnteDAO;
import it.ibm.red.business.dao.IPkHandlerDAO;
import it.ibm.red.business.dto.MappingOrgNsdDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.enums.SignStrategyEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.persistence.model.Ente;
import it.ibm.red.business.persistence.model.PkHandler;
import it.ibm.red.business.persistence.model.TipoProtocollo;
import it.ibm.red.business.persistence.model.TipoRicevuta;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class AooDAO.
 *
 * @author CPIERASC
 *
 *         Dao gestione aoo.
 */
@Repository
public class AooDAO extends AbstractDAO implements IAooDAO {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 2181888528596674584L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AooDAO.class.getName());

	/**
	 * DAO ente.
	 */
	@Autowired
	private IEnteDAO enteDAO;

	/**
	 * DAO handler PK.
	 */
	@Autowired
	private IPkHandlerDAO pkHandlerDAO;

	/**
	 * Restituisce l'aoo identificata dal {@code idAoo}.
	 *
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @param connection
	 *            Connessione al database.
	 * @return L'AOO recuperata.
	 */
	@Override
	public final Aoo getAoo(final Long idAoo, final Connection connection) {
		try {
			final String querySQL = "SELECT a.*, af.*, tp.IDTIPOPROTOCOLLO, tp.DESCRIZIONE as TPDESCRIZIONE, tr.ID_TIPO_RICEVUTA, tr.DESCR_TIPO_RICEVUTA as TRDESCRIZIONE"
							+ " FROM Aoo a"
							+ " LEFT JOIN Aoo_Filenet af ON a.idaoo = af.id_aoo"
							+ " LEFT JOIN TIPOPROTOCOLLO tp ON a.TIPOPROTOCOLLO = tp.IDTIPOPROTOCOLLO"
							+ " LEFT JOIN TIPORICEVUTAPEC tr ON a.ID_TIPO_RICEVUTA = tr.ID_TIPO_RICEVUTA"
							+ " WHERE a.idaoo = " + sanitize(idAoo);
			return getAooBySQL(querySQL, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dell'Aoo tramite id=" + idAoo, e);
			throw new RedException("Errore durante il recupero dell'Aoo tramite id=" + idAoo, e);
		}
	}

	/**
	 * Restituisce l'aoo identificata dal {@code codeAoo}.
	 *
	 * @param codeAoo
	 *            Identificativo dell'area organizzativa.
	 * @param connection
	 *            Connessione al database.
	 * @return L'AOO recuperata.
	 */
	@Override
	public final Aoo getAoo(final String codeAoo, final Connection connection) {
		try {
			final String querySQL = "SELECT a.*, af.*, tp.IDTIPOPROTOCOLLO, tp.DESCRIZIONE as TPDESCRIZIONE, tr.ID_TIPO_RICEVUTA, tr.DESCR_TIPO_RICEVUTA as TRDESCRIZIONE"
					+ " FROM Aoo a" + " LEFT JOIN Aoo_Filenet af ON a.idaoo = af.id_aoo" + " LEFT JOIN TIPOPROTOCOLLO tp ON a.TIPOPROTOCOLLO = tp.IDTIPOPROTOCOLLO"
					+ " LEFT JOIN TIPORICEVUTAPEC tr ON a.ID_TIPO_RICEVUTA = tr.ID_TIPO_RICEVUTA" + " WHERE a.codiceaoo = " + sanitize(codeAoo);
			return getAooBySQL(querySQL, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dell'Aoo tramite code=" + codeAoo, e);
			throw new RedException("Errore durante il recupero dell'Aoo tramite code=" + codeAoo, e);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IAooDAO#getAooFromNPS(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public final Aoo getAooFromNPS(final String codeAooNPS, final Connection connection) {
		Aoo out = null;
		final String querySQL = "select evo.codiceaoo from nps_configuration nps, AOO evo where nps.idaoo = evo.idaoo and nps.CODICE_AOO=?";
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(querySQL);
			ps.setString(1, codeAooNPS);
			rs = ps.executeQuery();
			if (rs.next()) {
				final String codeAoo = rs.getString(1);
				out = getAoo(codeAoo, connection);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante decodifica AOO NPS =" + codeAooNPS, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return out;
	}

	private Aoo getAooBySQL(final String querySQL, final Connection connection) throws SQLException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			if (rs.next()) {
				boolean salvaPin = false;
				if (rs.getInt("SALVA_PIN") == 1) {
					salvaPin = true;
				}
				boolean showDialogAoo = false;
				if (rs.getInt("SHOW_DIALOG") == 1) {
					showDialogAoo = true;
				}
				boolean timbroUscitaAoo = false;
				if (rs.getInt("TIMBRO_USCITA_AOO") == 1) {
					timbroUscitaAoo = true;
				}
				boolean isEstendiVisibilita = false;
				if (rs.getInt("IS_ESTENDI_VISIBILITA") == 1) {
					isEstendiVisibilita = true;
				}
				boolean disableUseHostOnly = false;
				if (rs.getInt("DISABLEUSEHOSTONLY") == 1) {
					disableUseHostOnly = true;
				}
				boolean showRiferimentoStorico = false;
				if (rs.getInt("SHOW_RIFERIMENTO_STORICO") == 1) {
					showRiferimentoStorico = true;
				}
				boolean showRicercaNsd = false;
				if (rs.getInt("SHOW_RICERCA_NSD") == 1) {
					showRicercaNsd = true;
				}
				boolean showSelezionaTuttiVisto = false;
				if (rs.getInt("SELEZIONA_TUTTI_VISTO") == 1) {
					showSelezionaTuttiVisto = true;
				}
				final Aoo aoo = new Aoo(rs.getLong("IDAOO"),
								  rs.getBigDecimal("IDENTE"),
								  rs.getString("CODICEAOO"),
								  rs.getString("DESCRIZIONE"),
								  salvaPin,
								  rs.getInt("ALTEZZAFOOTER"),
								  rs.getInt("SPAZIATURAFIRMA"),
								  rs.getLong("PARAMETRIAOO"),
								  rs.getLong("IDNODORADICE"),
								  rs.getInt("ID_LOGO"),
								  rs.getInt("MAX_SIZE_ENTRATA"),
								  rs.getInt("MAX_SIZE_USCITA"),
								  rs.getInt("CONF_FIRMA"),
								  rs.getInt("MANTIENI_ALLEGATI_ORIGINALI"),
								  rs.getInt("INOLTRA_MAIL"),
								  rs.getString("VIS_FALDONI"),
								  rs.getInt("FORZA_REFRESH_CODE"),
								  rs.getInt("POSTA_INTEROP_ESTERNA"),
								  rs.getLong("IDRUOLO_DELEGATO_LIBRO_FIRMA"),
								  showDialogAoo,
								  timbroUscitaAoo,
								  isEstendiVisibilita,
								  disableUseHostOnly,
								  showRiferimentoStorico,
								  showRicercaNsd,
								  showSelezionaTuttiVisto,
								  rs.getLong("ASSEGNAZIONEINDIRETTA"),
								  rs.getInt("UCB"),
								  rs.getInt("FLAG_STRATEGIA_FIRMA"),
								  rs.getInt("POSTILLA_ATTIVA"),
								  rs.getInt("POSTILLA_SOLO_PAG1"));
				final AooFilenet af = new AooFilenet(rs.getString("URI"), 
											  rs.getLong("PK_AOO_FILENET"), 
											  rs.getString("USERNAME"), 
											  rs.getString("PASSWORD"), 
											  rs.getString("OBJECT_STORE"),
											  rs.getString("ID_CLIENT_AOO"),
											  rs.getString("CONNECTION_POINT"),
											  rs.getString("STANZA_JAAS"),
											  aoo);
				aoo.setAooFilenet(af);
				final BigDecimal idTipoProtocollo = rs.getBigDecimal("IDTIPOPROTOCOLLO");
				if (idTipoProtocollo != null) {
					aoo.setTipoProtocollo(new TipoProtocollo(idTipoProtocollo.longValue(), rs.getString("TPDESCRIZIONE")));
				}
				final BigDecimal idTipoRicevuta = rs.getBigDecimal("ID_TIPO_RICEVUTA");
				if (idTipoRicevuta != null && idTipoProtocollo != null) {
					aoo.setTipoRicevuta(new TipoRicevuta(idTipoProtocollo.longValue(), rs.getString("TRDESCRIZIONE")));
				}
				if (aoo.getIdEnte() != null) {
					final Ente ente = enteDAO.getEnte(aoo.getIdEnte().intValue(), connection);
					aoo.setEnte(ente);
				}
				// Gestione handler PK per la firma
				final PkHandler pkHandlerFirma = pkHandlerDAO.getById(rs.getInt("PK_HANDLER_SIGN"), connection);
				aoo.setPkHandlerFirma(pkHandlerFirma);
				// Gestione handler PK per tutto ciò che non concerne la firma (verifica, etc.)
				final PkHandler pkHandlerVerifica = pkHandlerDAO.getById(rs.getInt("PK_HANDLER_VERIFY"), connection);
				aoo.setPkHandlerVerifica(pkHandlerVerifica);
				// Gestione handler PK timbro protocollo
				final Integer idPKHTimbro = rs.getInt("PK_HANDLER_TIMBRO");
				if (idPKHTimbro != null && idPKHTimbro != 0) {
					final PkHandler pkHandlerTimbro = pkHandlerDAO.getById(idPKHTimbro, connection);
					aoo.setPkHandlerTimbro(pkHandlerTimbro);
					aoo.setSignerTimbro(rs.getString("TIMBRO_SIGNER"));
					aoo.setPinTimbro(rs.getString("TIMBRO_PIN"));
				}
				//String poiché mantiene valore null
				final String orderByDataRicezioneAsc = rs.getString("ORDERBYDATARICEZIONEASC");
				if (orderByDataRicezioneAsc != null) {
					aoo.setOrdinamentoMailAOOAsc(Integer.parseInt(orderByDataRicezioneAsc));
				}

				aoo.setRibaltaTitolario(rs.getBoolean("SHOW_RIBALTA_TITOLARIO"));

				aoo.setAutocompleteRubricaCompleta(rs.getBoolean("SEARCH_WHOLE_RUBRICA"));

				boolean stampaEtichetteResponse = false;
				if (rs.getInt("SHOW_STAMPA_ETICHETTE") == 1) {
					stampaEtichetteResponse = true;
					aoo.setStampaEtichetteResponse(stampaEtichetteResponse);
				}

				boolean stampigliaAllegatiVisible = false;
				if (rs.getInt("STAMPIGLIA_ALLEGATI_VISIBLE") == 1) {
					stampigliaAllegatiVisible = true;
					aoo.setStampigliaAllegatiVisible(stampigliaAllegatiVisible);
				}
				boolean protocollaEMantieni = false;
				if (rs.getInt("PROTOCOLLA_E_MANTIENI") == 1) {
					protocollaEMantieni = true;
					aoo.setProtocollaEMantieni(protocollaEMantieni);
				}

				boolean downloadCustomExcel = false;
				if (rs.getInt("DOWNLOAD_CUSTOM_EXCEL") == 1) {
					downloadCustomExcel = true;
					aoo.setDownloadCustomExcel(downloadCustomExcel);
				}

				// String poiché mantiene valore null
				final String inoltraResponse = rs.getString("INOLTRA_RESPONSE");
				aoo.setInoltraResponse(inoltraResponse != null && "1".equals(inoltraResponse));

				// String poiché mantiene valore null
				final String rifiutaResponse = rs.getString("RIFIUTA_RESPONSE");
				aoo.setRifiutaResponse(rifiutaResponse != null && "1".equals(rifiutaResponse));

				boolean fileNonSbustato = false;
				if (rs.getInt("FILE_NON_SBUSTATO") == 1) {
					fileNonSbustato = true;
					aoo.setFileNonSbustato(fileNonSbustato);
				}

				if (rs.getInt("CONFPDFA") == 1) {
					aoo.setConfPDFAPerHandler(true);
				}

				boolean checkUnivocitaMail = false;
				if (rs.getInt("CHECK_UNIVOCITA_MAIL") == 1) {
					checkUnivocitaMail = true;
					aoo.setCheckUnivocitaMail(checkUnivocitaMail);
				}

				aoo.setMaxGiorniNotificheRubrica(rs.getInt("MAX_GIORNI_NOTIFICHE_RUBRICA"));
				aoo.setMaxGiorniNotificheSottoscrizioni(rs.getInt("MAX_GIORNI_NOTIFICHE_SOTT"));
				aoo.setMaxGiorniNotificheCalendario(rs.getInt("MAX_GIORNI_CALENDARIO"));

				aoo.setNumGiorniEliminazioneContOnTheFlyJob(rs.getInt("NUM_GIORNI_ELIM_CONT_ONTHEFLY"));

				aoo.setCasellaEmailUfficiale(rs.getString("MAILISTITUZIONALE1"));

				aoo.setMaxNotificheNonLette(rs.getInt("MAX_NOTIFICHE_NON_LETTE"));

				aoo.setVerificaFirmaMinutiDelay(rs.getInt("VERIFICAFIRMAMINUTIDELAY"));
				
				boolean showAllaccioNps = false;
				if(rs.getInt("SHOWALLACCIONPS") == 1) {
					showAllaccioNps = true;
				}
				aoo.setShowAllaccioNps(showAllaccioNps);
				
				boolean downloadSistemiEsterni = false;
				if(rs.getInt("DOWNLOAD_SISTEMI_ESTERNI") == 1) {
					downloadSistemiEsterni = true;
				}
				aoo.setDownloadSistemiEsterni(downloadSistemiEsterni);
				
				aoo.setGiorniNotificaMancataSpedizione(rs.getInt("GG_MANCATA_SPEDIZIONE"));
				
				aoo.setGiorniNotificaMancataSpedizioneCartacea(rs.getInt("GG_MANCATA_SPEDIZIONE_CARTACEA"));
				
				aoo.setIdAdminAoo(rs.getInt("IDADMINAOO"));
				
				return aoo;
			}
		} finally {
			closeStatement(ps, rs);
		}
		return null;
	}

	/**
	 * @see it.ibm.red.business.dao.IAooDAO#getVisFaldoni(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public String getVisFaldoni(final Long idAoo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String output = null;
		try {
			final String querySQL = "SELECT VIS_FALDONI FROM AOO A WHERE A.IDAOO = " + sanitize(idAoo);
			ps = con.prepareStatement(querySQL);
			rs = ps.executeQuery();
			if (rs.next()) {
				output = rs.getString("VIS_FALDONI");
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del parametro Vis Faldoni per Aoo =" + idAoo, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IAooDAO#getFlagSkipMotivazioneSigla(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean getFlagSkipMotivazioneSigla(final Long idAoo, final Connection con) {
		boolean output = false;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			final String querySQL = "SELECT DISABILITA_MOTIV_SIGLA FROM AOO A WHERE A.IDAOO = " + sanitize(idAoo);
			ps = con.prepareStatement(querySQL);

			rs = ps.executeQuery();

			if (rs.next()) {
				output = BooleanFlagEnum.SI.getIntValue().equals(rs.getInt("DISABILITA_MOTIV_SIGLA"));
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del flag di abilitazione per la motivazione dell'operazione di sigla." + idAoo, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IAooDAO#getMappingOrgNsd(java.lang.Long,
	 *      java.lang.Long, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public MappingOrgNsdDTO getMappingOrgNsd(final Long idUfficio, final Long idUtente, final Long idAoo, final Connection conn) {
		MappingOrgNsdDTO output = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			final String querySQL = "SELECT * FROM MAPPING_ORG_NSD WHERE ID_UFF_RED = " + sanitize(idUfficio) + "AND ID_UTE_RED = " + sanitize(idUtente) + " AND ID_AOO_RED = "
					+ sanitize(idAoo);
			ps = conn.prepareStatement(querySQL);

			rs = ps.executeQuery();

			if (rs.next()) {
				output = new MappingOrgNsdDTO(rs.getString("COD_AOO_NSD"), rs.getString("ID_AOO_NSD"), rs.getString("COD_UFF_NSD"), rs.getString("DESC_UFF_NSD"),
						rs.getString("COD_FISCALE_NSD"), rs.getString("NOME_UTE_NSD"), rs.getString("COGNOME_UTE__NSD"), rs.getString("ID_PARENT_NSD"),
						rs.getString("FUNZIONALITA_NSD"), rs.getString("ENTE_NSD"));
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del mapping" + idAoo, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IAooDAO#getMappingOrgWsNsd(java.lang.String,
	 *      java.lang.String, java.lang.String, java.sql.Connection).
	 */
	@Override
	public List<MappingOrgNsdDTO> getMappingOrgWsNsd(final String idAooNsd, final String codUffNsd, final String codFiscaleNsd, final Connection conn) {
		final List<MappingOrgNsdDTO> output = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM MAPPING_ORG_NSD WHERE ID_AOO_NSD = '" + idAooNsd + "' AND FUNZIONALITA_NSD = 'WS_SERVICE' ");

			if (!StringUtils.isNullOrEmpty(codUffNsd) && !codUffNsd.contains("?")) {
				sb.append(" AND COD_UFF_NSD = '" + codUffNsd + "'");
			}

			if (!StringUtils.isNullOrEmpty(codFiscaleNsd) && !codFiscaleNsd.contains("?")) {
				sb.append(" AND COD_FISCALE_NSD = '" + codFiscaleNsd + "'");
			}

			ps = conn.prepareStatement(sb.toString());
			rs = ps.executeQuery();

			while (rs.next()) {
				final MappingOrgNsdDTO result = new MappingOrgNsdDTO(rs.getString("COD_AOO_NSD"), rs.getString("ID_AOO_NSD"), rs.getString("COD_UFF_NSD"),
						rs.getString("DESC_UFF_NSD"), rs.getString("COD_FISCALE_NSD"), rs.getString("NOME_UTE_NSD"), rs.getString("COGNOME_UTE__NSD"),
						rs.getString("ID_PARENT_NSD"), rs.getString("FUNZIONALITA_NSD"), rs.getString("ENTE_NSD"), rs.getLong("ID_UFF_RED"), rs.getLong("ID_UTE_RED"),
						rs.getLong("ID_AOO_RED"));
				output.add(result);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del mapping di nsd chiamato dal ws" + idAooNsd, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IAooDAO#getIdNodoUfficioDiDefault(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public Long getIdNodoUfficioDiDefault(final Long idAoo, final Connection connection) {
		Long idNodoUfficio = 0L;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			final String querySQL = "SELECT IDNODOUFFICIODEFAULT FROM AOO WHERE IDAOO = ?";
			ps = connection.prepareStatement(querySQL);
			ps.setLong(1, idAoo);

			rs = ps.executeQuery();

			if (rs.next()) {
				idNodoUfficio = rs.getLong("IDNODOUFFICIODEFAULT");
			}
		} catch (final Exception e) {
			LOGGER.error("Errore riscontrato durante il recupero dell'id del nodo ufficio di default per l'AOO con id: " + idAoo, e);
			throw new RedException("Errore riscontrato durante il recupero dell'id del nodo ufficio di default per l'AOO con id: " + idAoo, e);
		} finally {
			closeStatement(ps, rs);
		}

		return idNodoUfficio;
	}
	
	/**
	 * @see it.ibm.red.business.dao.IAooDAO#getSignStrategy(java.sql.Connection,
	 *      java.lang.Long).
	 */
	@Override
	public SignStrategyEnum getSignStrategy(Connection connection, Long idAOO) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		SignStrategyEnum strategy = null;
		
		try {
			String querySQL = "SELECT FLAG_STRATEGIA_FIRMA FROM AOO WHERE IDAOO = ?";
			ps = connection.prepareStatement(querySQL);
			ps.setLong(1, idAOO);
			
			rs = ps.executeQuery();
			if(rs.next()) {
				int flagStrategy = rs.getInt("FLAG_STRATEGIA_FIRMA");
				if(flagStrategy == 0) {
					strategy = SignStrategyEnum.ASYNC_STRATEGY_B;
				} else {
					strategy = SignStrategyEnum.ASYNC_STRATEGY_A;
				}
			}
			
		} catch (Exception e) {
			LOGGER.error("Errore riscontrato durante il recupero del flag strategia firma per l'AOO con id: " + idAOO, e);
			throw new RedException("Errore riscontrato durante il recupero del flag strategia firma per l'AOO con id: " + idAOO, e);
		} finally {
			closeStatement(ps, rs);
		}
		return strategy;
	}
}
