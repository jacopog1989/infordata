package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IRicercaAvanzataDocUCBFacadeSRV;

/**
 * The Interface IRicercaAvanzataDocUCBSRV.
 *
 * @author a.dilegge
 * 
 *         Interfaccia del servizio per la gestione delle ricerche dei documenti custom per gli UCB.
 */
public interface IRicercaAvanzataDocUCBSRV extends IRicercaAvanzataDocUCBFacadeSRV {


}