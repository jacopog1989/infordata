package it.ibm.red.business.dto;

import it.ibm.red.business.enums.TipoOperazioneLibroFirmaEnum;

/**
 * The Class ContatoriLibroFirmaDTO.
 *
 * @author CPIERASC
 * 
 * 	DTO per i contatori dei documenti nel libro firma suddivisi per tipologia operazione.
 */
public class ContatoriLibroFirmaDTO extends AbstractDTO {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Numero documenti da siglare.
	 */
	private Integer numDaSiglare;
	
	/**
	 * Numero documenti da firmare.
	 */
	private Integer numDaFirmare;
	
	/**
	 * Numero documenti da vistare.
	 */
	private Integer numDaVistare;

	/**
	 * Costruttore.
	 */
	public ContatoriLibroFirmaDTO() {
		this(null, null, null);
	}

	/**
	 * Costruttore.
	 * 
	 * @param inNumDaSiglare	numero documenti da siglare
	 * @param inNumDaFirmare	numero documenti da firmare
	 * @param inNumDaVistare	numero documenti da vistare
	 */
	private ContatoriLibroFirmaDTO(final Integer inNumDaSiglare, final Integer inNumDaFirmare, final Integer inNumDaVistare) {
		this.numDaSiglare = inNumDaSiglare;
		this.numDaFirmare = inNumDaFirmare;
		this.numDaVistare = inNumDaVistare;
		
		if (numDaSiglare == null) {
			numDaSiglare = 0;
		}
		if (numDaFirmare == null) {
			numDaFirmare = 0;
		}
		if (numDaVistare == null) {
			numDaVistare = 0;
		}
	}
	
	/**
	 * Setter.
	 *
	 * @param numDaSiglare the new num da siglare
	 */
	public void setNumDaSiglare(final Integer numDaSiglare) {
		this.numDaSiglare = numDaSiglare;
	}

	/**
	 * Setter.
	 *
	 * @param numDaFirmare the new num da firmare
	 */
	public void setNumDaFirmare(final Integer numDaFirmare) {
		this.numDaFirmare = numDaFirmare;
	}

	/**
	 * Setter.
	 *
	 * @param numDaVistare the new num da vistare
	 */
	public void setNumDaVistare(final Integer numDaVistare) {
		this.numDaVistare = numDaVistare;
	}

	/**
	 * Getter .
	 * 
	 * @return	numero documenti da siglare
	 */
	public final Integer getNumDaSiglare() {
		return numDaSiglare;
	}
	
	/**
	 * Getter .
	 * 
	 * @return	numero documenti da firmare
	 */
	public final Integer getNumDaFirmare() {
		return numDaFirmare;
	}
	
	/**
	 * Getter .
	 * 
	 * @return	numero documenti da vistare
	 */
	public final Integer getNumDaVistare() {
		return numDaVistare;
	}

	/**
	 * Getter .
	 * 
	 * @return	numero documenti totale
	 */
	public final Integer getNumTotale() {
		return numDaSiglare + numDaFirmare + numDaVistare;
	}
	
	/**
	 * Metodo per aggiungere una unità all'opportuno contatore.
	 * 
	 * @param tipo	tipologia di operazione
	 */
	public final void add(final TipoOperazioneLibroFirmaEnum tipo) {
		if (TipoOperazioneLibroFirmaEnum.FIRMA.equals(tipo)) {
			numDaFirmare++;
		} else if (TipoOperazioneLibroFirmaEnum.SIGLA.equals(tipo)) {
			numDaSiglare++;
		} else if (TipoOperazioneLibroFirmaEnum.VISTA.equals(tipo)) {
			numDaVistare++;
		}
	}

	/**
	 * Metodo per sottrarre una unità all'opportuno contatore.
	 * 
	 * @param tipo	tipologia di operazione
	 */
	public final void sub(final TipoOperazioneLibroFirmaEnum tipo) {
		sub(tipo, 1);
	}

	/**
	 * Metodo per sottrarre una quantità generica all'opportuno contatore.
	 * 
	 * @param tipo	tipologia di operazione
	 * @param n		quantità
	 */
	public final void sub(final TipoOperazioneLibroFirmaEnum tipo, final int n) {
		if (TipoOperazioneLibroFirmaEnum.FIRMA.equals(tipo)) {
			numDaFirmare = Math.max(numDaFirmare - n, 0);
		} else if (TipoOperazioneLibroFirmaEnum.SIGLA.equals(tipo)) {
			numDaSiglare = Math.max(numDaSiglare - n, 0);
		} else if (TipoOperazioneLibroFirmaEnum.VISTA.equals(tipo)) {
			numDaVistare = Math.max(numDaVistare - n, 0);
		}
	}
	
}
