package it.ibm.red.business.service.ws.facade;

import java.io.Serializable;

import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.webservice.model.documentservice.types.messages.InserisciApprovazioneType;
import it.ibm.red.webservice.model.documentservice.types.messages.InserisciCampoFirmaType;
import it.ibm.red.webservice.model.documentservice.types.messages.InserisciIdType;
import it.ibm.red.webservice.model.documentservice.types.messages.InserisciPostillaType;
import it.ibm.red.webservice.model.documentservice.types.messages.InserisciProtEntrataType;
import it.ibm.red.webservice.model.documentservice.types.messages.InserisciProtUscitaType;
import it.ibm.red.webservice.model.documentservice.types.messages.InserisciWatermarkType;

/**
 * The Interface IStampigliaturaDocWsFacadeSRV.
 *
 * @author m.crescentini
 * 
 *         Facade servizio stampigliatura documenti da web service.
 */
public interface IStampigliaturaDocWsFacadeSRV extends Serializable {
	
	
	/**
	 * @param client
	 * @param request
	 * @return
	 */
	FileDTO stampigliaProtocolloEntrata(RedWsClient client, InserisciProtEntrataType request);
	
	
	/**
	 * @param client
	 * @param request
	 * @return
	 */
	FileDTO stampigliaProtocolloUscita(RedWsClient client, InserisciProtUscitaType request);

	
	/**
	 * @param client
	 * @param request
	 * @return
	 */
	FileDTO stampigliaIdDocumento(RedWsClient client, InserisciIdType request);

	
	/**
	 * @param client
	 * @param requestInserisciCampoFirma
	 * @return
	 */
	FileDTO inserisciCampoFirma(RedWsClient client, InserisciCampoFirmaType requestInserisciCampoFirma);

	
	/**
	 * @param client
	 * @param requestStampigliaPostilla
	 * @return
	 */
	FileDTO stampigliaPostilla(RedWsClient client, InserisciPostillaType requestStampigliaPostilla);


	/**
	 * @param client
	 * @param requestStampigliaWatermark
	 * @return
	 */
	FileDTO stampigliaWatermark(RedWsClient client, InserisciWatermarkType requestStampigliaWatermark);


	/**
	 * @param client
	 * @param requestStampigliaApprovazione
	 * @return
	 */
	FileDTO stampigliaApprovazione(RedWsClient client, InserisciApprovazioneType requestStampigliaApprovazione);
	
}