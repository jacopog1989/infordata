package it.ibm.red.business.dao.impl;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.ITipoFileDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.persistence.model.TipoFile;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class TipoFileDAO.
 *
 * @author adilegge
 * 
 * 	Dao per la gestione dei tipi file.
 */
@Repository
public class TipoFileDAO extends AbstractDAO implements ITipoFileDAO {

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Colonna - Convertibile.
	 */
	private static final String CONVERTIBILE = "CONVERTIBILE";

	/**
	 * Messaggio errore registrazione allegati su db.
	 */
	private static final String ERROR_INSERT_ALLEGATI_ON_DB_MSG = "Errore durante la scrittura dell'allegato sul database.";
	
	/**
	 * Messaggio errore cancellazione allegati su db.
	 */
	private static final String ERROR_DELETE_ALLEGATI_ON_DB_MSG = "Errore durante la cancellazione dell'allegato sul database.";
	
	/**
	 * Messaggio errore recupero allegati su db.
	 */
	private static final String ERROR_GET_ALLEGATI_ON_DB_MSG = "Errore durante il recupero dell'allegato sul database.";
	
	/**
	 * Messaggio errore normalizzazione del filename.
	 */
	private static final String ERROR_NORMALIZZAZIONE_NOME_FILE_MSG = "Errore durante la normalizzazione del nome del file.";
	
	/**
	 * Messaggio recupero tipi file per copia conforme.
	 */
	private static final String ERROR_RECUPERO_TIPI_FILE_CC_MSG = "Errore durante il recupero dei tipi file per copia conforme dal DB.";
	
	/**
	 * Messaggio errore recupero tipi file.
	 */
	private static final String ERROR_RECUPERO_TIPI_FILE_MSG = "Errore durante il recupero dei tipi file ";
	
	/**
	 * Gets the by id.
	 *
	 * @param connection the connection
	 * @return la lista di item
	 */
	@Override
	public final Collection<TipoFile> getAll(final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection<TipoFile> items = null;
		try {
			
			items = new ArrayList<>();
			
			final String querySQL = "SELECT tf.* FROM TIPOFILE tf ";
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				items.add(populateTipoFile(rs));
			}
			
			return items;
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero dei tipi file.", e);
		} finally {
			closeStatement(ps, rs);
		}
	}
	
	
	private static TipoFile populateTipoFile(final ResultSet rs) throws SQLException {
		return new TipoFile(rs.getInt("IDTIPOFILE"), rs.getString("ESTENSIONE"),
				rs.getString("MIMETYPE"), rs.getInt("COPIACONFORME"), rs.getInt("EVENTOALLEGATO"), rs.getInt(CONVERTIBILE));
	}

	/**
	 * @see it.ibm.red.business.dao.ITipoFileDAO#getByExtension(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public TipoFile getByExtension(final String ext, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		TipoFile item = null;
		int index = 1;
		try {
			final String querySQL = "SELECT tf.* FROM TIPOFILE tf WHERE tf.ESTENSIONE = ?";
			ps = connection.prepareStatement(querySQL);
			ps.setString(index, ext);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				item = populateTipoFile(rs);
			}
			
			return item;
		} catch (final SQLException e) {
			throw new RedException(ERROR_RECUPERO_TIPI_FILE_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ITipoFileDAO#isEstensioneConvertibile(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean isEstensioneConvertibile(final String ext, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			
			final String querySQL = "SELECT tf.* FROM TIPOFILE tf WHERE tf.ESTENSIONE = ?";
			ps = connection.prepareStatement(querySQL);
			ps.setString(index, ext);
			
			rs = ps.executeQuery();
			return isConvertibile(rs);
		} catch (final SQLException e) {
			throw new RedException(ERROR_RECUPERO_TIPI_FILE_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
	}


	/**
	 * Restituisce {@code true} se convertibile, {@code false} altrimenti.
	 * 
	 * @param rs
	 *            Result set dal quale recuperare l'informazione.
	 * @return {@code true} se convertibile, {@code false} altrimenti.
	 * @throws SQLException
	 */
	private static boolean isConvertibile(ResultSet rs) throws SQLException {
		int x = 0;
		boolean convertibile = false;
		while (rs.next()) {
			x++;
			convertibile = rs.getInt(CONVERTIBILE) == 1;
			if (rs.getInt(CONVERTIBILE) == 1) {
				break;
			}
		}
		if (x == 0) {
			//vuol dire che non ho trovato la riga a db quindi metto true
			convertibile = true;
		}
		return convertibile;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipoFileDAO#isMimeTypeConvertibile(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean isMimeTypeConvertibile(final String mimeType, final Connection connection) {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			
			final String querySQL = "SELECT tf.* FROM TIPOFILE tf WHERE tf.MIMETYPE = ?";
			ps = connection.prepareStatement(querySQL);
			ps.setString(index, mimeType);
			
			rs = ps.executeQuery();		
			return isConvertibile(rs);
		} catch (final SQLException e) {
			throw new RedException(ERROR_RECUPERO_TIPI_FILE_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
	}
	
	
	
	@Override
	public final Collection<TipoFile> getTipiFileCopiaConforme(final Connection con)  {
		return getTipiFileOnCondition(con, "copiaconforme");
	}

	@Override
	public final Collection<TipoFile> getTipiFileDocUscita(final Connection con)  {
		return getTipiFileOnCondition(con, "DOCUSCITA");
	}
	
	@Override
	public final Collection<TipoFile> getTipiFileEventoAllegato(final Connection con)  {
		return getTipiFileOnCondition(con, "EVENTOALLEGATO");
	}

	/**
	 * Restituisce le tipologia di File che rispettano la condizione
	 * {@code colum = 1}.
	 * 
	 * @param con
	 *            Connessione al database.
	 * @param column
	 *            Nome colonna.
	 * @return Collection di Tipo File.
	 */
	private static Collection<TipoFile> getTipiFileOnCondition(final Connection con, String column) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Collection<TipoFile> tipiFileCopiaConforme = null;
		
		try {
			tipiFileCopiaConforme = new ArrayList<>();
			
			ps = con.prepareStatement("SELECT * FROM TIPOFILE WHERE " + column + " = 1");
			rs = ps.executeQuery();
			
			while (rs.next()) {
				tipiFileCopiaConforme.add(populateTipoFile(rs));
			}
		} catch (final SQLException e) {
			throw new RedException(ERROR_RECUPERO_TIPI_FILE_CC_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return tipiFileCopiaConforme;
	}
	
	private static Boolean validateMN(final String mn, final byte[] content) {
		Boolean out = true;
		if (!StringUtils.isNullOrEmpty(mn)) {
			final Integer sizeMN = mn.length();
			if (content == null || content.length < sizeMN) {
				out = false;
			} else {
				final String contentMN = new String(content, 0, sizeMN);
				out = contentMN.equalsIgnoreCase(mn);
			}
		}
		return out;
	}
	
	@Override
	public final Boolean analizzaFile(final Connection con, final byte[] content, final String mimeType, final String filename) {
		Boolean out = false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			final String extension = FilenameUtils.getExtension(filename);
			ps = con.prepareStatement("SELECT MAGIC_NUMBER FROM TIPOFILE WHERE LOWER(MIMETYPE) = LOWER(?) AND ESTENSIONE = ?");
			ps.setString(1, mimeType);
			ps.setString(2, extension);
			rs = ps.executeQuery();
			if (rs.next()) {
				out = validateMN(rs.getString("MAGIC_NUMBER"), content);
			} else {
				out = true;
			}
		} catch (final SQLException e) {
			throw new RedException("Errore durante l'analisi del file.", e);
		} finally {
			closeStatement(ps, rs);
		}
		return out;
	}

	@Override
	public final String normalizzaNomeFile(final Connection con, final String filename) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String out = filename;
		try {
			ps = con.prepareStatement("SELECT OLD_VALUE, NEW_VALUE FROM SOSTITUZIONI_NOME_FILE");
			rs = ps.executeQuery();
			while (rs.next()) {
				final String oldValue = rs.getString("OLD_VALUE");
				String newValue = rs.getString("NEW_VALUE");
				if (newValue == null) {
					newValue = "";
				}
				out = out.replace(oldValue, newValue);
			}
		} catch (final SQLException e) {
			throw new RedException(ERROR_NORMALIZZAZIONE_NOME_FILE_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		return out;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipoFileDAO#insertRecordEmail(java.sql.Connection,
	 *      java.lang.Integer, java.lang.Integer, java.lang.Integer,
	 *      java.lang.Integer, java.lang.String, java.util.HashMap).
	 */
	@Override
	public void insertRecordEmail(final Connection con, final Integer idAOO, final Integer idUtente, final Integer idNodo, final Integer idRuolo, final String guidMail, final HashMap<String, byte[]> recordtoInsert) {
		PreparedStatement ps = null;
		final ResultSet rs = null;
		try {
		ps = con.prepareStatement("INSERT INTO CONTENT_PROTOCOLLAZIONE (IDAOO, IDUTENTE, IDNODO, IDRUOLO, GUIDMAIL, PATH, CONTENT, DATA) VALUES(?,?,?,?,?,?,?, SYSDATE) ");
		 for (final Entry<String, byte[]> keyValuePair : recordtoInsert.entrySet()) {
			ps.setInt(1, idAOO);
			ps.setInt(2, idUtente);
			ps.setInt(3, idNodo);
			ps.setInt(4, idRuolo);
			ps.setString(5, guidMail);
			ps.setString(6, keyValuePair.getKey());
			
			byte[] content = keyValuePair.getValue();
			if(content == null) {
				content = new byte[] {};
			}
			Blob blob = con.createBlob();
			blob.setBytes(1, content);
			ps.setBlob(7, blob);
			
			ps.addBatch();
		 }
		 ps.executeBatch();
		} catch (final SQLException e) {
			throw new RedException(ERROR_INSERT_ALLEGATI_ON_DB_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ITipoFileDAO#getContent(java.sql.Connection,
	 *      java.lang.Integer, java.lang.Integer, java.lang.Integer,
	 *      java.lang.Integer, java.lang.String).
	 */
	@Override
	public byte[] getContent(final Connection con, final Integer idAOO, final Integer idUtente, final Integer idNodo, final Integer idRuolo,  final String path) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		byte[] content = null;
		try {
			ps = con.prepareStatement("SELECT * FROM CONTENT_PROTOCOLLAZIONE WHERE idAOO=? AND idUtente=? AND idNODO=? and idRuolo=? and path=?");
			ps.setInt(1, idAOO);
			ps.setInt(2, idUtente);
			ps.setInt(3, idNodo);
			ps.setInt(4, idRuolo);
			ps.setString(5, path);
			rs = ps.executeQuery();
			if (rs.next()) {
				content = rs.getBytes("CONTENT");
			}
		} catch (final SQLException e) {
			throw new RedException(ERROR_GET_ALLEGATI_ON_DB_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		return content;
	}
	
	/**
	 * @see it.ibm.red.business.dao.ITipoFileDAO#deleteContents(java.sql.Connection,
	 *      java.lang.Integer, java.lang.Integer, java.lang.Integer,
	 *      java.lang.Integer, java.lang.String).
	 */
	@Override
	public void deleteContents(final Connection con, final Integer idAOO, final Integer idUtente, final Integer idNodo, final Integer idRuolo,  final String guidMail) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			//elimino i contenuti dei giorni precedenti e quello della mail attuale (In caso l'avessi già aperta)
			ps = con.prepareStatement("DELETE FROM CONTENT_PROTOCOLLAZIONE WHERE idAOO=? AND idUtente=? AND idNODO=? and idRuolo=? and (guidmail=? or DATA <> SYSDATE)");
			ps.setInt(1, idAOO);
			ps.setInt(2, idUtente);
			ps.setInt(3, idNodo);
			ps.setInt(4, idRuolo);
			ps.setString(5, guidMail);
			rs = ps.executeQuery();
		} catch (final SQLException e) {
			throw new RedException(ERROR_DELETE_ALLEGATI_ON_DB_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
	}
	
	
}