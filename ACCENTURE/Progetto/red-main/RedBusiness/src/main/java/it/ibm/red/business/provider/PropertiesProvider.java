package it.ibm.red.business.provider;

import java.io.InputStream;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.SortedSet;
import java.util.TreeSet;

import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ReportQueryEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.IPropertiesSRV;

/**
 * The Class PropertiesProvider.
 *
 * @author CPIERASC
 * 
 *         Oggetto per la gestione dei parametri (sono salvati su db e
 *         aggiornati ogni quanto di tempo).
 */
public final class PropertiesProvider implements Serializable {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Quanto di tempo in ms in fase di attesa di aggiornamento.
	 */
	private static final int WAITING_TIME = 100;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(PropertiesProvider.class.getName());

	/**
	 * Flag che indica la fase di aggiornamento in corso.
	 */
	private Boolean isUpdating;
	
	/**
	 * Data ultimo refresh.
	 */
	private Date lastRefreshTime;

	
	/**
	 * SRV Properties.
	 */
	private IPropertiesSRV propertiesSRV;
	
	/**
	 * Istanza (singleton).
	 */
	private static PropertiesProvider provider;

	/**
	 * Parametri.
	 */
	private Map<String, String> parameters;

	/**
	 * Report queries.
	 */
	private Properties reportQueries;

	/**
	 * Costruttore.
	 */
	private PropertiesProvider() {
		propertiesSRV = (IPropertiesSRV) ApplicationContextProvider.getApplicationContext().getBean("propertiesSRV");
		refreshProperties();
	}
	
	/**
	 * Getter istanza singleton.
	 * 
	 * @return	istanza
	 */
	public static PropertiesProvider getIstance() {
		if (provider == null) {
			provider = new PropertiesProvider();
		}
		return provider;
	}
	
	/**
	 * Recupero parametro a partire dalla sua chiave.
	 * 
	 * @param inPar	chiave parametro
	 * @return		valore parametro
	 */
	public String getParameterByKey(final PropertiesNameEnum inPar) {
		waitUpdating();
		return parameters.get(inPar.getKey());
	}
	
	/**
	 * Recupero parametro.
	 * 
	 * @param inParString	chiave parametro
	 * @return		valore parametro
	 */
	public String getParameterByString(final String inParString) {
		waitUpdating();
		return parameters.get(inParString);
	}
	
	/**
	 * Testa la presenza dell'inputValue nelle property di tipo "list". Una property di tipo list è una property ripetuta concatenando 
	 * la chiave radice con .n, dove n va da 1 al numero di valori associati a quella property.
	 *
	 * @param inPar the in par
	 * @param inputValue the input value
	 * @return true, if successful
	 */
	public boolean existsInPropertiesList(final PropertiesNameEnum inPar, final String inputValue) {
		waitUpdating();
		
		int i = 1;
		String value = parameters.get(inPar.getKey() + "." + i);
		do {
			
			if (value != null && value.equals(inputValue)) {
				return true;
			}
			
			value = parameters.get(inPar.getKey() + "." + (++i));
			
		} while (value != null);
		
		return false;
	}

	/**
	 * Metodo per attendere il termine dell'update.
	 */
	private void waitUpdating() {
		while (Boolean.TRUE.equals(isUpdating)) {
			try {
				Thread.sleep(WAITING_TIME);
			} catch (InterruptedException e) {
				LOGGER.error("Errore in fase di wait upadate.", e);
				Thread.currentThread().interrupt();
			}
		}
	}

	/**
	 * Aggiornamento parametri.
	 */
	public void refreshProperties() {
		try {
			isUpdating = true;
			lastRefreshTime = new Date();
			LOGGER.info("PROPERTIES PROVIDER refreshProperties(): Caricamento dei parametri di configurazione dal DB in corso...");
			parameters = propertiesSRV.getAll();
			LOGGER.info("PROPERTIES PROVIDER refreshProperties(): Caricamento dei parametri di configurazione dal DB effettuato.");
			if (parameters != null && parameters.size() > 0) {
				LOGGER.info("PROPERTIES PROVIDER refreshProperties(): Caricate i seguenti parametri:");
				for (Entry<String, String> entry: parameters.entrySet()) {
					LOGGER.info("PROPERTIES PROVIDER refreshProperties(): " + entry.getKey() + "=" + entry.getValue());
				}
			} else {
				LOGGER.warn("PROPERTIES PROVIDER refreshProperties(): Attenzione: non risulta essere stato caricato nessun parametro!");
			}
			
			reportQueries = new Properties();
			InputStream inputStream = PropertiesProvider.class.getClassLoader().getResourceAsStream("report/reportQuery.properties");
			if (inputStream != null) {
				reportQueries.load(inputStream);
			} 

		} catch (Exception e) {
			LOGGER.error("PROPERTIES PROVIDER refreshProperties(): Errore durante il refresh delle properties:", e);
		} finally {
			isUpdating = false;
		}
	}

	/**
	 * tags:.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		waitUpdating();
		StringBuilder sb = new StringBuilder("<html><head></head><body>");
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:SS");
		InetAddress localhost = null;
		try {
			localhost = InetAddress.getLocalHost();
		} catch (UnknownHostException e) {
			LOGGER.warn("Errore nel recupero dell'indirizzo locale della macchina: ", e);
		}
		sb.append("ULTIMO REFRESH: <" + sdf.format(lastRefreshTime) + "> - IPSERVER: " + localhost);
		if (parameters != null && parameters.size() > 0) {
			sb.append("<table border=\"1\">");
			sb.append("<tr>");
			sb.append("<th>Key</th>");
			sb.append("<th>Value</th> ");
			sb.append("</tr>");
			SortedSet<String> lhs = new TreeSet<>(parameters.keySet());
			for (String key : lhs) {
				sb.append("<tr>");
				sb.append("<td>" + key + "</td>");
				sb.append("<td>" + parameters.get(key) + "</td> ");
				sb.append("</tr>");
			}
			sb.append("</table></body></html>");
		} else {
			sb.append("NESSUN PARAMETRO CARICATO");
		}
		return sb.toString();
	}
	 
	/** 
	 * @return the last refresh time
	 */
	public Date getLastRefreshTime() {
		return lastRefreshTime;
	}
	
	/** 
	 * @param rqe the rqe
	 * @return the report queries
	 */
	public String getReportQueries(final ReportQueryEnum rqe) {
		return reportQueries.getProperty(rqe.getKey());
	}
}
