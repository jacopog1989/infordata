package it.ibm.red.business.service.concrete;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.DocumentSet;

import it.ibm.red.business.constants.Constants.Ricerca;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.RegistroProtocolloDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.nps.exceptions.BusinessDelegateRuntimeException;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.INpsSRV;
import it.ibm.red.business.service.IRicercaRegistroProtocolloSRV;
import it.ibm.red.business.utils.DateUtils;

/**
 * The Class RicercaRegistroProtocolloSRV.
 *
 * @author m.crescentini
 * 
 *         Servizio per la gestione della ricerca nel Registro Protocollo.
 */
@Service
@Component
public class RicercaRegistroProtocolloSRV extends AbstractService implements IRicercaRegistroProtocolloSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -6147767378111021038L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaRegistroProtocolloSRV.class.getName());

	/**
	 * Service.
	 */
	@Autowired
	private INpsSRV npsSRV;

	/**
	 * Properties.
	 */
	private PropertiesProvider pp;

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaRegistroProtocolloFacadeSRV#eseguiRicercaPerStampaRegistroProtocollo(it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Collection<RegistroProtocolloDTO> eseguiRicercaPerStampaRegistroProtocollo(final ParamsRicercaAvanzataDocDTO paramsRicercaStampaProtocollo,
			final UtenteDTO utente) {
		LOGGER.info("eseguiRicercaPerStampaRegistroProtocollo -> START");
		Collection<RegistroProtocolloDTO> documentiRicerca = new ArrayList<>();

		try {
			// NPS EMERGENZA
			if (TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(utente.getIdTipoProtocollo())) {
				LOGGER.warn("eseguiRicercaPerStampaRegistroProtocollo -> Impossibile eseguire la ricerca dei rigistri protocollo in regime di emergenza.");
			} else if (TipologiaProtocollazioneEnum.NPS.getId().equals(utente.getIdTipoProtocollo())) {
				// NPS
				documentiRicerca = npsSRV.ricercaStampaRegistro(paramsRicercaStampaProtocollo, utente);
				Collections.sort((List<RegistroProtocolloDTO>) documentiRicerca, new Comparator<RegistroProtocolloDTO>() {
					@Override
					public int compare(final RegistroProtocolloDTO reg1, final RegistroProtocolloDTO reg2) {
						Integer output = null;
						if (reg1.getDataProtocollo().before(reg2.getDataProtocollo())) {
							output = 1;
						} else if (reg1.getDataProtocollo().equals(reg2.getDataProtocollo())) {
							output = 0;
						} else {
							output = -1;
						}
						return output;
					}
				});

				// PMEF
			} else {
				ricercaRegistriProtocolloFilenet(documentiRicerca, paramsRicercaStampaProtocollo, utente);
			}
		} catch (final BusinessDelegateRuntimeException soap) {
			LOGGER.error("eseguiRicercaPerStampaRegistroProtocollo -> Si è verificato un errore nella chiamata verso NPS per l'esecuzione della ricerca.", soap);
			throw soap;
		} catch (final Exception e) {
			LOGGER.error("eseguiRicercaPerStampaRegistroProtocollo -> Si è verificato un errore durante l'esecuzione della ricerca.", e);
			throw new RedException(e);
		}

		LOGGER.info("eseguiRicercaPerStampaRegistroProtocollo -> END. Numero di documenti trovati: " + documentiRicerca.size());
		return documentiRicerca;
	}

	private void ricercaRegistriProtocolloFilenet(final Collection<RegistroProtocolloDTO> documentiRicerca, final ParamsRicercaAvanzataDocDTO paramsRicercaStampaProtocollo,
			final UtenteDTO utente) {
		IFilenetCEHelper fceh = null;

		try {
			final HashMap<String, Object> mappaValoriRicercaFilenet = new HashMap<>();

			// Si convertono i parametri di ricerca in una mappa chiave -> valore
			convertiInParametriRicercaFilenet(mappaValoriRicercaFilenet, paramsRicercaStampaProtocollo, utente.getIdAoo());

			// ### RICERCA NEL CE -> START
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final DocumentSet documentiFilenet = fceh.ricercaRegistroProtocollo(mappaValoriRicercaFilenet, utente);

			if (documentiFilenet != null && !documentiFilenet.isEmpty()) {
				documentiRicerca.addAll(TrasformCE.transform(documentiFilenet, TrasformerCEEnum.FROM_DOCUMENTO_TO_REGISTRO_PROTOCOLLO_PMEF));
			}
			// ### RICERCA NEL CE -> END
		} finally {
			popSubject(fceh);
		}
	}

	private void convertiInParametriRicercaFilenet(final HashMap<String, Object> paramsRicercaFilenet, final ParamsRicercaAvanzataDocDTO paramsRicerca, final Long idAoo) {
		LOGGER.info("convertiInParametriRicercaFilenet -> START");

		paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY), idAoo);

		// Data Creazione Da
		if (paramsRicerca.getDataCreazioneDa() != null) {
			paramsRicercaFilenet.put(Ricerca.DATA_CREAZIONE_DA, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataCreazioneDa(), true));
		}

		// Data Creazione A
		if (paramsRicerca.getDataCreazioneA() != null) {
			paramsRicercaFilenet.put(Ricerca.DATA_CREAZIONE_A, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataCreazioneA(), false));
		}

		LOGGER.info("convertiInParametriRicercaFilenet -> END");
	}
}