package it.ibm.red.business.dto;

import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import it.ibm.red.business.utils.LogStyleNotNull;

/**
 * The Class NotificaEmailDTO.
 *
 * @author CPIERASC
 * 
 * 	DTO per la notifica via mail.
 */
public class NotificaEmailDTO extends AbstractDTO {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -7763735214591856721L;
	
	/**
	 * Identificativo notifica.
	 */
	private Integer idNotifica;
	
	/**
	 * Identificativo documento.
	 */
	private String idDocumento;
	
	/**
	 * Identificativo messaggio.
	 */
	private String idMessaggio;
	
	/**
	 * Email mittente.
	 */
	private String emailMittente;
	
	/**
	 * Tipologia mittente.
	 */
	private Integer tipoMittente;
	
	/**
	 * Email destinatario.
	 */
	private String emailDestinatario;
	
	/**
	 * Tipologia destinatario.
	 */
	private Integer tipoDestinatario;
	
	/**
	 * Identificativo destinatario.
	 */
	private Long idDestinatario;
	
	/**
	 * Stato ricevuta.
	 */
	private Integer statoRicevuta;
	
	/**
	 * Identificativo aoo.
	 */
	private Integer idAoo;
	
	/**
	 * Contatore retry.
	 */
	private Integer countRetry;
	
	/**
	 * Numero massimo retry.
	 */
	private Integer maxRetry;
	
	/**
	 * Spedizione.
	 */
	private Integer spedizione;
	
	/**
	 * Messaggio.
	 */
	private byte[] messaggio;
	
	/**
	 * evento associato alla mail da inviare.
	 */
	private Integer tipoEvento;
	

	/**
	 * Data richiesta spedizione.
	 */
	private Date dataRichiestaSpedizione;
	
	/**
	 * Costruttore notifica email DTO.
	 */
	public NotificaEmailDTO() {
		super();
	}
	
	/**
	 * Costruttore notifica email DTO.
	 *
	 * @param idNotifica
	 *            the id notifica
	 * @param idDocumento
	 *            the id documento
	 * @param idMessaggio
	 *            the id messaggio
	 * @param emailMittente
	 *            the email mittente
	 * @param tipoMittente
	 *            the tipo mittente
	 * @param emailDestinatario
	 *            the email destinatario
	 * @param tipoDestinatario
	 *            the tipo destinatario
	 * @param idDestinatario
	 *            the id destinatario
	 * @param statoRicevuta
	 *            the stato ricevuta
	 * @param idAoo
	 *            the id aoo
	 * @param countRetry
	 *            the count retry
	 * @param maxRetry
	 *            the max retry
	 * @param spedizione
	 *            the spedizione
	 * @param messaggio
	 *            the messaggio
	 * @param tipoEvento
	 *            the tipo evento
	 */
	public NotificaEmailDTO(final Integer idNotifica, final String idDocumento, final String idMessaggio,
			final String emailMittente, final Integer tipoMittente, final String emailDestinatario,
			final Integer tipoDestinatario, final Long idDestinatario, final Integer statoRicevuta, final Integer idAoo, 
			final Integer countRetry, final Integer maxRetry, final Integer spedizione, 
			final byte[] messaggio, final Integer tipoEvento) {
		this();
		this.idNotifica = idNotifica;
		this.idDocumento = idDocumento;
		this.idMessaggio = idMessaggio;
		this.emailMittente = emailMittente;
		this.tipoMittente = tipoMittente;
		this.emailDestinatario = emailDestinatario;
		this.tipoDestinatario = tipoDestinatario;
		this.idDestinatario = idDestinatario;
		this.statoRicevuta = statoRicevuta;
		this.idAoo = idAoo;
		this.countRetry = countRetry;
		this.maxRetry = maxRetry;
		this.spedizione = spedizione;
		this.messaggio = messaggio;
		this.tipoEvento = tipoEvento;
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param inIdDestinatario	identificativo destinatario
	 */
	public NotificaEmailDTO(final Long inIdDestinatario) {
		idDestinatario = inIdDestinatario;
	}

	/**
	 * Getter.
	 * 
	 * @return	identificativo notifica
	 */
	public final Integer getIdNotifica() {
		return idNotifica;
	}

	/**
	 * Getter.
	 * 
	 * @return	identificativo documento
	 */
	public final String getIdDocumento() {
		return idDocumento;
	}

	/**
	 * Getter.
	 * 
	 * @return	identifcativo messaggio
	 */
	public final String getIdMessaggio() {
		return idMessaggio;
	}

	/**
	 * Getter.
	 * 
	 * @return	email mittente
	 */
	public final String getEmailMittente() {
		return emailMittente;
	}

	/**
	 * Getter.
	 * 
	 * @return	tipo mittente
	 */
	public final Integer getTipoMittente() {
		return tipoMittente;
	}

	/**
	 * Getter.
	 * 
	 * @return	email destinatario
	 */
	public final String getEmailDestinatario() {
		return emailDestinatario;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	tipologia destinatario
	 */
	public final Integer getTipoDestinatario() {
		return tipoDestinatario;
	}


	/**
	 * Getter.
	 * 
	 * @return	identificativo destinatario
	 */
	public final Long getIdDestinatario() {
		return idDestinatario;
	}

	/**
	 * Getter.
	 * 
	 * @return	stato ricevuta
	 */
	public final Integer getStatoRicevuta() {
		return statoRicevuta;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	identifcativo aoo
	 */
	public final Integer getIdAoo() {
		return idAoo;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	numero retry
	 */
	public final Integer getCountRetry() {
		return countRetry;
	}

	/**
	 * Getter.
	 * 
	 * @return	numero massimo retry
	 */
	public final Integer getMaxRetry() {
		return maxRetry;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	spedizione
	 */
	public final Integer getSpedizione() {
		return spedizione;
	}

	/**
	 * Getter.
	 * 
	 * @return	messaggio
	 */
	public final byte[] getMessaggio() {
		return messaggio;
	}
	
	/**
	 * Setter.
	 * 
	 * @param inEmailMittente	email mittente
	 */
	public final void setEmailMittente(final String inEmailMittente) {
		this.emailMittente = inEmailMittente;
	}

	/**
	 * Setter.
	 * 
	 * @param inTipoMittente	tipologia mittente
	 */
	public final void setTipoMittente(final Integer inTipoMittente) {
		this.tipoMittente = inTipoMittente;
	}

	/**
	 * Setter.
	 * 
	 * @param inEmailDestinatario	email destinatario
	 */
	public final void setEmailDestinatario(final String inEmailDestinatario) {
		this.emailDestinatario = inEmailDestinatario;
	}

	/**
	 * Setter.
	 * 
	 * @param inTipoDestinatario	tipologia destinatario
	 */
	public final void setTipoDestinatario(final Integer inTipoDestinatario) {
		this.tipoDestinatario = inTipoDestinatario;
	}

	/**
	 * Setter.
	 * 
	 * @param inIdDestinatario	identificativo destinatario
	 */
	public final void setIdDestinatario(final Long inIdDestinatario) {
		this.idDestinatario = inIdDestinatario;
	}

	/**
	 * Setter.
	 * 
	 * @param inStatoRicevuta	stato ricevuta
	 */
	public final void setStatoRicevuta(final Integer inStatoRicevuta) {
		this.statoRicevuta = inStatoRicevuta;
	}

	/**
	 * Setter.
	 * 
	 * @param inIdAoo	identificativo aoo
	 */
	public final void setIdAoo(final Integer inIdAoo) {
		this.idAoo = inIdAoo;
	}

	/**
	 * Setter.
	 * 
	 * @param inCountRetry	conteggio retry
	 */
	public final void setCountRetry(final Integer inCountRetry) {
		this.countRetry = inCountRetry;
	}
	
	/**
	 * Setter.
	 * 
	 * @param inMaxRetry	numero massimo retry
	 */
	public final void setMaxRetry(final Integer inMaxRetry) {
		this.maxRetry = inMaxRetry;
	}
	
	/**
	 * Setter.
	 * 
	 * @param inSpedizione	spedizione
	 */
	public final void setSpedizione(final Integer inSpedizione) {
		this.spedizione = inSpedizione;
	}
	
	/**
	 * Setter.
	 * 
	 * @param inMessaggio	messaggio
	 */
	public final void setMessaggio(final byte[] inMessaggio) {
		this.messaggio = inMessaggio;
	}

	/**
	 * Setter.
	 * 
	 * @param inIdNotifica	identificativo notifica
	 */
	public final void setIdNotifica(final Integer inIdNotifica) {
		this.idNotifica = inIdNotifica;
	}

	/**
	 * Setter.
	 * 
	 * @param inIdDocumento	identifcativo documento
	 */
	public final void setIdDocumento(final String inIdDocumento) {
		this.idDocumento = inIdDocumento;
	}

	/**
	 * Setter.
	 * 
	 * @param inIdMessaggio	identificativo messaggio
	 */
	public final void setIdMessaggio(final String inIdMessaggio) {
		this.idMessaggio = inIdMessaggio;
	}

	/**
	 * tags:.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, new LogStyleNotNull()); 
	}

	/** 
	 *
	 * @return the data richiesta spedizione
	 */
	public Date getDataRichiestaSpedizione() {
		return dataRichiestaSpedizione;
	}

	/** 
	 *
	 * @param dataRichiestaSpedizione the new data richiesta spedizione
	 */
	public void setDataRichiestaSpedizione(final Date dataRichiestaSpedizione) {
		this.dataRichiestaSpedizione = dataRichiestaSpedizione;
	}

	/** 
	 *
	 * @return the tipo evento
	 */
	public Integer getTipoEvento() {
		return tipoEvento;
	}

	/** 
	 *
	 * @param tipoEvento the new tipo evento
	 */
	public void setTipoEvento(final Integer tipoEvento) {
		this.tipoEvento = tipoEvento;
	}
}
