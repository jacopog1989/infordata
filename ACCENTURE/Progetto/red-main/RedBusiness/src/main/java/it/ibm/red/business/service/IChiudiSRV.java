package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IChiudiFacadeSRV;

/**
 * Interface del servizio chiusura.
 */
public interface IChiudiSRV extends IChiudiFacadeSRV {

}
