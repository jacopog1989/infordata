package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.Date;
import java.util.List;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.AssegnatarioOrganigrammaDTO;
import it.ibm.red.business.dto.ContributoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.service.facade.IContributoFacadeSRV;

/**
 * The Interface IContributoSRV.
 *
 * @author CPIERASC
 * 
 *         Interfaccia servizio lookup
 */
public interface IContributoSRV extends IContributoFacadeSRV {

	/**
	 * Inserisce il contributo interno per il documento.
	 * 
	 * @param idUtente
	 *            - id dell'utente
	 * @param idUfficio
	 *            - id dell'ufficio
	 * @param idAoo
	 *            - id dell'Aoo
	 * @param idDocumento
	 *            - id documento
	 * @param nota
	 * @param nomeFile
	 *            - nome del file
	 * @param dataCdC
	 * @param foglio
	 * @param registro
	 * @param stato
	 * @param con
	 * @return
	 */
	Long insertContributoInterno(Long idUtente, Long idUfficio, Long idAoo, Long idDocumento, String nota,
			String nomeFile, Date dataCdC, int foglio, int registro, int stato, Connection con);

	/**
	 * Recupera i contributi. ATTENZIONE!!! Se non è presente il file (guid null e
	 * nomeFile null) il content viene visualizzato con il documento di default
	 * 
	 * @param fceh
	 * @param documentTitle
	 * @param connection
	 * @return
	 */
	List<ContributoDTO> getContributi(IFilenetCEHelper fceh, String documentTitle, Long idAoo, Connection connection);

	/**
	 * @param workFlow
	 * @param idAoo
	 * @return
	 */
	Boolean isWorkflowPadrePerContributo(VWWorkObject workFlow, Long idAoo);

	/**
	 * 
	 * Recupera le assegnazioni per contributo dei documenti allacciati. Navigando
	 * sia il workflow corrispondente sia lo storico.
	 * 
	 * @param idDocumentoPrincipale
	 *            id del documento di cui ci interessano i documento allacciati.
	 * @param utente
	 *            utente che vuole eseguire l'operazione
	 * @param fpeh
	 *            gestore della connesione del pe.
	 * @param con
	 *            connessione con il server (per recuperare lo storico).
	 * @return Una lista di assegnazioni per i contributi.
	 * 
	 */
	List<AssegnatarioOrganigrammaDTO> getAssegnazioniContributiAllaccio(Integer idDocumentoPrincipale, UtenteDTO utente,
			FilenetPEHelper fpeh, Connection con);

	/**
	 * Recupera le assegnazioni per contributo del workflow.
	 * 
	 * @param wobNumber
	 *            Identificativo del workflow.
	 * @param fpeh
	 *            gestore della connesione del pe.
	 * @return Una lista di assegnazioni per i contributi.
	 */
	List<AssegnatarioOrganigrammaDTO> getAssegnazioniContributi(String wobNumber, FilenetPEHelper fpeh);

	/**
	 * Verifica se il documento in input ha un Contributo Esterno non concluso
	 * (senza risposta).
	 * 
	 * @param documentTitle
	 * @return
	 */
	boolean checkContributoEsternoNonConcluso(String documentTitle, Integer numeroDocumento, Integer annoDocumento,
			UtenteDTO utente, Connection con, boolean isSollecito);
}
