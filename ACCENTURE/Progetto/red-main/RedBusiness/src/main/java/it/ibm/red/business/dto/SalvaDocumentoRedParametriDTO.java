package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.enums.TipoAssegnazioneEnum;

/**
 * Struttura dati contenente i parametri per il salvataggio di un documento.
 * 
 * @author mcrescentini
 *
 */
public class SalvaDocumentoRedParametriDTO implements Serializable {
	
	private static final long serialVersionUID = -1877055211260748229L;
	
    /**
     * Modalità.
     */
	private String modalita;

    /**
     * Flag crea bozza.
     */
	private boolean creaBozza;
		
    /**
     * Flag content scannerizzato.
     */
	private boolean isContentScannerizzato;
	
    /**
     * Flag content variato.
     */
	private boolean isContentVariato;
	
    /**
     * Flag invia notifica protocollazione.
     */
	private boolean inviaNotificaProtocollazioneMail;
	
    /**
     * Id fascicolo selezionato.
     */
	private String idFascicoloSelezionato;
	
    /**
     * Lista nomi faldoni selezionati.
     */
	private List<String> nomeFaldoniSelezionati;
	
    /**
     * GUID della mail in input.
     */
	private String inputMailGuid;

	/* ** Attributi specifici per creazione diretta documento da processo automatico -> START ** */
    /**
     * Metadati fascicolo.
     */
	private transient Map<String, Object> metadatiFascicolo;
	
    /**
     * Indice classificazione fascicolo.
     */
	private String indiceClassificazioneFascicolo;
	
    /**
     * Tipo assegnazione fascicolo.
     */
	private TipoAssegnazioneEnum isAssegnazioneInternaWorkflow;
	
	/* **************** Metadati del workflow **************** */
    /**
     * Utente mittente wf.
     */
	private Long idUtenteMittenteWorkflow;

    /**
     * Ufficio mittente wf.
     */
	private Long idUfficioMittenteWorkflow;
	
    /**
     * Utente destinatario wf.
     */
	private Long idUtenteDestinatarioWorkflow;
	
    /**
     * Ufficio destinatario wf.
     */
	private Long idUfficioDestinatarioWorkflow;
	
    /**
     * Precedente id documento wf.
     */
	private String idDocumentoOldWorkflow;
	
    /**
     * Motivazione assegnazione wf.
     */
	private String motivazioneAssegnazioneWorkflow;
	

	/**
	 * Flag titolario modificato.
	 */
	private boolean isTitolarioChanged;
	/* ******************************************************* */
	/* ** Attributi specifici per creazione diretta documento da processo automatico -> END ** */
	
	/**
	 * Flag per forzare specifici controlli di validazione.
	 */
	private boolean forzaProtocollazioneMail;
	/**
	 * Flag per forzare specifici controlli di validazione. 
	 */
	private boolean forzaRispostaPubblica;
	/**
	 * Flag per forzare specifici controlli di validazione. 
	 */
	private boolean forzaAttoDecretoManuale;
	/**
	 * Flag per forzare specifici controlli di validazione. 
	 */
	private boolean forzaAssSpedizioneDestElettronici;
	/**
	 * Forza lo sbustamento in fase di trasformazione di un content P7M.
	 */
	private boolean protocollazioneP7M;
	
	/**
	 * Flag velina.
	 */
	private boolean velinaAsDocPrincipale;
	
	/**
	 * Flag che indica che il documento che si sta salvando è già presente sul sistema di protocollo.
	 */
	private boolean protocollazioneAutomatica;
	
	/**
	 * Flag content from NPS.
	 */
	private boolean contentFromNPS;
	
	/**
	 * GUID della seconda versione presente su NPS del documento principale.
	 */
	private String guidLastVersionNPSDocPrincipale;
	
	/**
	 * Restituisce la modalita.
	 * @return modalita
	 */
	public String getModalita() {
		return modalita;
	}

	/**
	 * Imposta la modalita.
	 * @param modalita
	 */
	public void setModalita(final String modalita) {
		this.modalita = modalita;
	}

	/**
	 * Restituisce il flag associato alla creaizone della bozza.
	 * @return crea bozza
	 */
	public boolean isCreaBozza() {
		return creaBozza;
	}

	/**
	 * Imposta il flag associato alla creaizone della bozza.
	 * @param creaBozza
	 */
	public void setCreaBozza(final boolean creaBozza) {
		this.creaBozza = creaBozza;
	}

	/**
	 * Restituisce true se il content ha subito variazione, false altrimenti.
	 * @return true se il content ha subito variazione, false altrimenti
	 */
	public boolean isContentVariato() {
		return isContentVariato;
	}

	/**
	 * Imposta il flag associato alla variazione del content.
	 * @param isContentVariato
	 */
	public void setContentVariato(final boolean isContentVariato) {
		this.isContentVariato = isContentVariato;
	}

	/**
	 * Restituisce true se il content è stato scannerizzato, false altrimenti.
	 * @return true se il content è stato scannerizzato, false altrimenti
	 */
	public boolean isContentScannerizzato() {
		return isContentScannerizzato;
	}

	/**
	 * Imposta il flag associato alla scansione del content.
	 * @param isContentScannerizzato
	 */
	public void setContentScannerizzato(final boolean isContentScannerizzato) {
		this.isContentScannerizzato = isContentScannerizzato;
	}

	/**
	 * @return inviaNotificaProtocollazioneMail
	 */
	public boolean isInviaNotificaProtocollazioneMail() {
		return inviaNotificaProtocollazioneMail;
	}

	/**
	 * @param inviaNotificaProtocollazioneMail
	 */
	public void setInviaNotificaProtocollazioneMail(final boolean inviaNotificaProtocollazioneMail) {
		this.inviaNotificaProtocollazioneMail = inviaNotificaProtocollazioneMail;
	}

	/**
	 * Restituisce il parametro associato alla forzatura della protocollazione mail.
	 * @return forzaProtocollazioneMail
	 */
	public boolean isForzaProtocollazioneMail() {
		return forzaProtocollazioneMail;
	}

	/**
	 * Imposta il parametro associato alla forzatura della protocollazione mail.
	 * @param forzaProtocollazioneMail
	 */
	public void setForzaProtocollazioneMail(final boolean forzaProtocollazioneMail) {
		this.forzaProtocollazioneMail = forzaProtocollazioneMail;
	}

	/**
	 * Restituisce il parametro associato alla forzatura della risposta pubblica.
	 * @return
	 */
	public boolean isForzaRispostaPubblica() {
		return forzaRispostaPubblica;
	}

	/**
	 * Imposta il parametro associato alla forzatura della risposta pubblica.
	 * @param forzaRispostaPubblica
	 */
	public void setForzaRispostaPubblica(final boolean forzaRispostaPubblica) {
		this.forzaRispostaPubblica = forzaRispostaPubblica;
	}

	/**
	 * Restituisce il parametro associato alla forzatura dell'atto decreto manuale.
	 * @return forzaAttoDecretoManuale
	 */
	public boolean isForzaAttoDecretoManuale() {
		return forzaAttoDecretoManuale;
	}

	/**
	 * Imposta il parametro associato alla forzatura dell'atto decreto manuale.
	 * @param forzaAttoDecretoManuale
	 */
	public void setForzaAttoDecretoManuale(final boolean forzaAttoDecretoManuale) {
		this.forzaAttoDecretoManuale = forzaAttoDecretoManuale;
	}

	/**
	 * Restituisce il parametro associato alla forzatura dell'AssSpedizioneDestElettronici.
	 * @return forzaAssSpedizioneDestElettronici
	 */
	public boolean isForzaAssSpedizioneDestElettronici() {
		return forzaAssSpedizioneDestElettronici;
	}

	/**
	 * Imposta il parametro associato alla forzatura dell'AssSpedizioneDestElettronici.
	 * @param forzaAssSpedizioneDestElettronici
	 */
	public void setForzaAssSpedizioneDestElettronici(final boolean forzaAssSpedizioneDestElettronici) {
		this.forzaAssSpedizioneDestElettronici = forzaAssSpedizioneDestElettronici;
	}

	/**
	 * Restituisce il guid della mail input.
	 * @return guid mail input
	 */
	public String getInputMailGuid() {
		return inputMailGuid;
	}

	/**
	 * Imposta il guid delal mail input.
	 * @param inputMailGuid
	 */
	public void setInputMailGuid(final String inputMailGuid) {
		this.inputMailGuid = inputMailGuid;
	}

	/**
	 * Restituisce l'id del fascicolo selezionato.
	 * @return id fascicolo selezionato
	 */
	public String getIdFascicoloSelezionato() {
		return idFascicoloSelezionato;
	}

	/**
	 * Imposta l'id del fascicolo selezionato.
	 * @param idFascicoloSelezionato
	 */
	public void setIdFascicoloSelezionato(final String idFascicoloSelezionato) {
		this.idFascicoloSelezionato = idFascicoloSelezionato;
	}

	/**
	 * Restituisce i metadati del fascicolo.
	 * @return metadati fascicolo
	 */
	public Map<String, Object> getMetadatiFascicolo() {
		return metadatiFascicolo;
	}

	/**
	 * Imposta i metadati del fascicolo.
	 * @param metadatiFascicolo
	 */
	public void setMetadatiFascicolo(final Map<String, Object> metadatiFascicolo) {
		this.metadatiFascicolo = metadatiFascicolo;
	}

	/**
	 * Restituisce l'indice di classificazione del fascicolo.
	 * @return indice classificazione fascicolo
	 */
	public String getIndiceClassificazioneFascicolo() {
		return indiceClassificazioneFascicolo;
	}

	/**
	 * Imposta l'indice di classificazione del fascicolo.
	 * @param indiceClassificazioneFascicolo
	 */
	public void setIndiceClassificazioneFascicolo(final String indiceClassificazioneFascicolo) {
		this.indiceClassificazioneFascicolo = indiceClassificazioneFascicolo;
	}

	/**
	 * Restituisce l'id dell'ufficio mittente del workflow.
	 * @return id dell'ufficio mittente del workflow
	 */
	public Long getIdUfficioMittenteWorkflow() {
		return idUfficioMittenteWorkflow;
	}

	/**
	 * Imposta l'id dell'ufficio mittente del workflow.
	 * @param idUfficioMittenteWorkflow
	 */
	public void setIdUfficioMittenteWorkflow(final Long idUfficioMittenteWorkflow) {
		this.idUfficioMittenteWorkflow = idUfficioMittenteWorkflow;
	}

	/**
	 * Restituisce l'id dell'utente mittente del workflow.
	 * @return id utente mittente del workflow
	 */
	public Long getIdUtenteMittenteWorkflow() {
		return idUtenteMittenteWorkflow;
	}

	/**
	 * Imposta l'id dell'utente mittente del workflow.
	 * @param idUtenteMittenteWorkflow
	 */
	public void setIdUtenteMittenteWorkflow(final Long idUtenteMittenteWorkflow) {
		this.idUtenteMittenteWorkflow = idUtenteMittenteWorkflow;
	}

	/**
	 * Restituisce l'id dell'ufficio destinatario del workflow.
	 * @return id ufficio destinatario workflow
	 */
	public Long getIdUfficioDestinatarioWorkflow() {
		return idUfficioDestinatarioWorkflow;
	}

	/**
	 * Imposta l'id dell'ufficio destinatario del workflow.
	 * @param idUfficioDestinatarioWorkflow
	 */
	public void setIdUfficioDestinatarioWorkflow(final Long idUfficioDestinatarioWorkflow) {
		this.idUfficioDestinatarioWorkflow = idUfficioDestinatarioWorkflow;
	}

	/**
	 * Restituisce l'id dell'utente destinatario del workflow.
	 * @return id utente destintario workflow
	 */
	public Long getIdUtenteDestinatarioWorkflow() {
		return idUtenteDestinatarioWorkflow;
	}

	/**
	 * Imposta l'id dell'utente destintario del workflow.
	 * @param idUtenteDestinatarioWorkflow
	 */
	public void setIdUtenteDestinatarioWorkflow(final Long idUtenteDestinatarioWorkflow) {
		this.idUtenteDestinatarioWorkflow = idUtenteDestinatarioWorkflow;
	}

	/**
	 * Restituisce true se assegnazione interna, false altrimenti.
	 * @return true se assegnazione interna, false altrimenti
	 */
	public TipoAssegnazioneEnum getIsAssegnazioneInternaWorkflow() {
		return isAssegnazioneInternaWorkflow;
	}

	/**
	 * Imposta il flag associato all'assegnazione interna.
	 * @param isAssegnazioneInternaWorkflow
	 */
	public void setIsAssegnazioneInternaWorkflow(final TipoAssegnazioneEnum isAssegnazioneInternaWorkflow) {
		this.isAssegnazioneInternaWorkflow = isAssegnazioneInternaWorkflow;
	}

	/**
	 * Restituisce l'id del documento old.
	 * @return id documento old
	 */
	public String getIdDocumentoOldWorkflow() {
		return idDocumentoOldWorkflow;
	}

	/**
	 * Imposta l'id del documento old.
	 * @param idDocumentoOldWorkflow
	 */
	public void setIdDocumentoOldWorkflow(final String idDocumentoOldWorkflow) {
		this.idDocumentoOldWorkflow = idDocumentoOldWorkflow;
	}

	/**
	 * Restituisce la motivazione dell'osservazione del workflow.
	 * @return motivazione assegnazione workflow
	 */
	public String getMotivazioneAssegnazioneWorkflow() {
		return motivazioneAssegnazioneWorkflow;
	}

	/**
	 * Imposta la motivazione dell'osservazione del workflow.
	 * @param motivazioneAssegnazioneWorkflow
	 */
	public void setMotivazioneAssegnazioneWorkflow(final String motivazioneAssegnazioneWorkflow) {
		this.motivazioneAssegnazioneWorkflow = motivazioneAssegnazioneWorkflow;
	}

	/**
	 * Restituisce true se il titolario ha subito variazioni, false altrimenti.
	 * @return true se il titolario ha subito variazioni, false altrimenti
	 */
	public boolean isTitolarioChanged() {
		return isTitolarioChanged;
	}
	
	/**
	 * Imposta il flag associato alla variazione del titolario.
	 * @param isTitolarioChanged
	 */
	public void setTitolarioChanged(final boolean isTitolarioChanged) {
		this.isTitolarioChanged = isTitolarioChanged;
	}

	/**
	 * Restituisce true se la protocollazione è P7M, false altrimenti.
	 * @return true se la protocollazione è P7M, false altrimenti
	 */
	public boolean isProtocollazioneP7M() {
		return protocollazioneP7M;
	}

	/**
	 * Imposta il flag associato alla protocollazione P7M.
	 * @param protocollazioneP7M
	 */
	public void setProtocollazioneP7M(final boolean protocollazioneP7M) {
		this.protocollazioneP7M = protocollazioneP7M;
	}

	/**
	 * Restituisce true se usare la velina come documento principale, false altrimenti.
	 * @return true se usare la velina come documento principale, false altrimenti
	 */
	public boolean isVelinaAsDocPrincipale() {
		return velinaAsDocPrincipale;
	}

	/**
	 * Imposta il flag associato alla velina del documento principale.
	 * @param velinaAsDocPrincipale
	 */
	public void setVelinaAsDocPrincipale(final boolean velinaAsDocPrincipale) {
		this.velinaAsDocPrincipale = velinaAsDocPrincipale;
	}
	
	/**
	 * Restituisce protocollazioneAutomatica.
	 * 
	 * @return the protocollazioneAutomatica
	 */
	public boolean isProtocollazioneAutomatica() {
		return protocollazioneAutomatica;
	}

	/**
	 * Imposta protocollazioneAutomatica.
	 * 
	 * @param protocollazioneAutomatica the protocollazioneAutomatica to set
	 */
	public void setProtocollazioneAutomatica(boolean protocollazioneAutomatica) {
		this.protocollazioneAutomatica = protocollazioneAutomatica;
	}

	/**
	 * Restituisce true se usare i content presenti su NPS, false altrimenti.
	 * 
	 * @return the contentFromNPS
	 */
	public boolean isContentFromNPS() {
		return contentFromNPS;
	}

	/**
	 * Imposta il flag per impostare i content del documento a partire da quelli presenti su NPS.
	 * 
	 * @param contentFromNPS the contentFromNPS to set
	 */
	public void setContentFromNPS(final boolean contentFromNPS) {
		this.contentFromNPS = contentFromNPS;
	}
	
	/**
	 * @return the guidLastVersionNPSDocPrincipale
	 */
	public String getGuidLastVersionNPSDocPrincipale() {
		return guidLastVersionNPSDocPrincipale;
	}

	/**
	 * @param guidLastVersionNPSDocPrincipale the guidLastVersionNPSDocPrincipale to set
	 */
	public void setGuidLastVersionNPSDocPrincipale(final String guidLastVersionNPSDocPrincipale) {
		this.guidLastVersionNPSDocPrincipale = guidLastVersionNPSDocPrincipale;
	}

	/**
	 * Restituisce i nomi dei faldoni selezionati.
	 * @return nome faldoni selezionati
	 */
	public List<String> getNomeFaldoniSelezionati() {
		return nomeFaldoniSelezionati;
	}

	/**
	 * Imposta i nomi dei faldoni selezionati.
	 * @param nomeFaldoniSelezionati
	 */
	public void setNomeFaldoniSelezionati(final List<String> nomeFaldoniSelezionati) {
		this.nomeFaldoniSelezionati = nomeFaldoniSelezionati;
	}
}