/**
 * 
 */
package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.RegistroRepertorioDTO;

/**
 * @author APerquoti
 *
 */
public interface IRegistroRepertorioDAO extends Serializable {
	
	/**
	 * Metodo per recuperare le occorrenze dei Registri di repertorio configurate per AOO
	 * 
	 * @param idAoo
	 * @param conn
	 * @return
	 */
	List<RegistroRepertorioDTO> getRegistriRepertorioByAoo(Long idAoo, Connection conn);

	/**
	 * Metodo per recuperare denominazione registro dalla tabella nps_configuration 
	 * @param idAoo
	 * @param conn
	 * @return
	 */
	RegistroRepertorioDTO getDenominazioneRegistroUfficiale(Long idAoo, Connection conn);

	
	/**
	 * Metodo per recuperare tipi registri per la ricerca avanzata
	 * @param idAoo
	 * @param conn
	 * @return
	 */
	List<RegistroRepertorioDTO> getAllRegistroRepertorio(Long idAoo, Connection conn);
	
	/**
	 * Metodo per recuperare descrizione dei registri per stampa etichette
	 * @param idAoo
	 * @param conn
	 * @return
	 */
	List<String> getDescrForShowEtichetteByAoo(Long idAoo, Connection conn);
}
