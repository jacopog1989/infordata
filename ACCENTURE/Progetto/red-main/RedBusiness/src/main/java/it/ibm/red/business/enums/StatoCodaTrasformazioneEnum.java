package it.ibm.red.business.enums;

/**
 * Enum stati coda trasformazione.
 * 
 * @author a.dilegge
 *
 */
public enum StatoCodaTrasformazioneEnum {
	
	/**
	 * Da elaborare.
	 */
	DA_ELABORARE(1, "Da elaborare"),

	/**
	 * Presa in carico.
	 */
	PRESA_IN_CARICO(2, "Presa in carico"),

	/**
	 * Lavorata.
	 */
	LAVORATA(3, "Lavorata"),

	/**
	 * Errore.
	 */
	ERRORE(4, "Errore"),

	/**
	 * Documento non trovato.
	 */
	DOCUMENTO_NON_TROVATO(5, "Documento non trovato");

	/**
	 * Stato.
	 */
	private Integer status;
	
	/**
	 * Descrizione.
	 */
	private String description;

	/**
	 * Costruttore.
	 * 
	 * @param inStatus		stato
	 * @param inDescription	descrizione
	 */
	StatoCodaTrasformazioneEnum(final Integer inStatus, final String inDescription) {
		status = inStatus;
		description = inDescription;
	}

	/**
	 * Getter.
	 * 
	 * @return	descrizione
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Getter.
	 * 
	 * @return	stato
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * Metodo per il recupero di un enum a partire dal valore associato.
	 * 
	 * @param value	valore
	 * @return		enum
	 */
	public static StatoCodaTrasformazioneEnum get(final Integer value) {
		StatoCodaTrasformazioneEnum output = null;
		for (StatoCodaTrasformazioneEnum sme:StatoCodaTrasformazioneEnum.values()) {
			if (sme.getStatus().equals(value)) {
				output = sme;
			}
		}
		return output;
	}
	
}
