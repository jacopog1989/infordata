/**
 * 
 */
package it.ibm.red.business.dto;

/**
 * @author APerquoti
 *
 */
public class FadRagioneriaDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -5601229994042670710L;

	/**
	 * Codice ragioneria.
	 */
	private String codiceRagioneria;
	
	/**
	 * Descrizione ragioneria.
	 */
	private String descrizioneRagioneria;

	/**
	 * Costruttore.
	 */
	public FadRagioneriaDTO() { }

	/**
	 * @param codiceRagioneria
	 * @param descrizioneRagioneria
	 */
	public FadRagioneriaDTO(final String codiceRagioneria, final String descrizioneRagioneria) {
		super();
		this.codiceRagioneria = codiceRagioneria;
		this.descrizioneRagioneria = descrizioneRagioneria;
	}

	/**
	 * @return the codiceRagioneria
	 */
	public final String getCodiceRagioneria() {
		return codiceRagioneria;
	}

	/**
	 * @return the descrizioneRagioneria
	 */
	public final String getDescrizioneRagioneria() {
		return descrizioneRagioneria;
	}
	
}
