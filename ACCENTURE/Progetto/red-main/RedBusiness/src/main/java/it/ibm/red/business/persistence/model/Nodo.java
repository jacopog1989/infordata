package it.ibm.red.business.persistence.model;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class Nodo.
 *
 * @author CPIERASC
 * 
 *         Entità che mappa la tabella NODO.
 */
public class Nodo implements Serializable, Comparable<Nodo> {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Aoo.
	 */
	private Aoo aoo;

	/**
	 * Descrizione.
	 */
	private String descrizione;

	/**
	 * Dirigente.
	 */
	private Utente dirigente;

	/**
	 * Identifcativo nodo.
	 */
	private Long idNodo;
	
	/**
	 * Codice nodo.
	 */
	private String codiceNodo;

	/**
	 * Data attivazione.
	 */
	private Date dataAttivazione;

	/**
	 * Data disattivazione.
	 */
	private Date dataDisattivazione;
	
	/**
	 * Data disattivazione.
	 */
	private Integer flagSegreteria;
	
	/**
	 * Identificativo nodo padre
	 */
	private Long idNodoPadre;
	
	/**
	 * Identificativo tipo nodo
	 */
	private Integer idTipoNodo;
	
	/**
	 * Identificativo nodo corriere
	 */
	private Long idNodoCorriere;
	
	/**
	 * Identificativo tipo struttura
	 */
	private Integer idTipoStruttura;

	/**
	 * Count ufficio
	 */
	private Integer countUfficio;

	/**
	 * Ufficio delegato
	 */
	private boolean ufficioDelegato;
	
	/**
	 * Ufficio libro firma fepa
	 */
	private boolean ufficioLibroFirmaFepa;
	
	/**
	 * Colore ufficio
	 */
	private String coloreUfficio;  

	/**
	 * Flag estendi visibilita
	 */
	private boolean isEstendiVisibilita;

	
	/**
	 * Costruttore.
	 *
	 * @param inIdNodo 			identifcativo nodo
	 * @param inIdNodoPadre the in id nodo padre
	 * @param inDataAttivazione 	data attivazione nodo
	 * @param inDataDisattivazione data disattivazione
	 * @param inAoo 				aoo
	 * @param inCodiceNodo the in codice nodo
	 * @param inDescrizione 		descrizione
	 * @param inDirigente 		dirigente
	 * @param inFlagSegreteria the in flag segreteria
	 */
	public Nodo(final Long inIdNodo, final Long inIdNodoPadre, final Date inDataAttivazione, final Date inDataDisattivazione, final Aoo inAoo, 
				final String inCodiceNodo, final String inDescrizione, final Utente inDirigente, final Integer inFlagSegreteria) {
		idNodo = inIdNodo;
		idNodoPadre = inIdNodoPadre;
		dataAttivazione = inDataAttivazione;
		dataDisattivazione = inDataDisattivazione;
		aoo = inAoo;
		codiceNodo = inCodiceNodo;
		descrizione = inDescrizione;
		dirigente = inDirigente;
		flagSegreteria = inFlagSegreteria;
	}

	/**
	 * Costruttore nodo.
	 *
	 * @param inIdNodo
	 *            the in id nodo
	 * @param inIdNodoPadre
	 *            the in id nodo padre
	 * @param inDataAttivazione
	 *            the in data attivazione
	 * @param inDataDisattivazione
	 *            the in data disattivazione
	 * @param inAoo
	 *            the in aoo
	 * @param inCodiceNodo
	 *            the in codice nodo
	 * @param inDescrizione
	 *            the in descrizione
	 * @param inDirigente
	 *            the in dirigente
	 * @param inFlagSegreteria
	 *            the in flag segreteria
	 * @param idTipoNodo
	 *            the id tipo nodo
	 * @param idNodoCorriere
	 *            the id nodo corriere
	 * @param inIdTipostruttura
	 *            the in id tipostruttura
	 * @param inIsEstendiVisibilita
	 *            the in is estendi visibilita
	 */
	public Nodo(final Long inIdNodo, final Long inIdNodoPadre, final Date inDataAttivazione, final Date inDataDisattivazione, final Aoo inAoo, final String inCodiceNodo,
			final String inDescrizione, final Utente inDirigente, final Integer inFlagSegreteria, final Integer idTipoNodo, final Long idNodoCorriere, final Integer inIdTipostruttura,
			final boolean inIsEstendiVisibilita) {
		this(inIdNodo, inIdNodoPadre, inDataAttivazione, inDataDisattivazione, inAoo, inCodiceNodo, inDescrizione, inDirigente, inFlagSegreteria);
		this.idTipoNodo = idTipoNodo;
		this.idNodoCorriere = idNodoCorriere;
		this.idTipoStruttura = inIdTipostruttura;
		this.isEstendiVisibilita = inIsEstendiVisibilita;
				
	}

	
	/**
	 * Costruttore copia.
	 * 
	 * @param nodoToCopy
	 *            Nodo da copiare.
	 */
	public Nodo(Nodo nodoToCopy) {
		super();
		this.aoo = nodoToCopy.getAoo();
		this.descrizione = nodoToCopy.getDescrizione();
		this.dirigente = nodoToCopy.getDirigente();
		this.idNodo = nodoToCopy.getIdNodo();
		this.codiceNodo = nodoToCopy.getCodiceNodo();
		this.dataAttivazione = nodoToCopy.getDataAttivazione();
		this.dataDisattivazione = nodoToCopy.getDataDisattivazione();
		this.flagSegreteria = nodoToCopy.getFlagSegreteria();
		this.idNodoPadre = nodoToCopy.getIdNodoPadre();
		this.idTipoNodo = nodoToCopy.getIdTipoNodo();
		this.idNodoCorriere = nodoToCopy.getIdNodoCorriere();
		this.idTipoStruttura = nodoToCopy.getIdTipoStruttura();
		this.countUfficio = nodoToCopy.getCountUfficio();
		this.ufficioDelegato = nodoToCopy.isUfficioDelegato();
		this.ufficioLibroFirmaFepa = nodoToCopy.isUfficioLibroFirmaFepa();
		this.coloreUfficio = nodoToCopy.getColoreUfficio();
		this.isEstendiVisibilita = nodoToCopy.getIsEstendiVisibilita();
	}

	/**
	 * Costruttore nodo.
	 */
	public Nodo() {
		super();
	}
	
	/**
	 * Getter.
	 * 
	 * @return	aoo
	 */
	public final Aoo getAoo() {
		return aoo;
	}

	/**
	 * Getter.
	 * 
	 * @return	descrizione
	 */
	public final String getDescrizione() {
		return descrizione;
	}
	 
	/**
	 * Setter del descrizione.
	 *
	 * @param descrizione the new descrizione
	 */
	public void setDescrizione(final String descrizione) {
		this.descrizione = descrizione;
	}
 
	/**
	 * Getter.
	 * 
	 * @return	dirigente
	 */
	public final Utente getDirigente() {
		return dirigente;
	}

	/**
	 * Getter.
	 * 
	 * @return	identificativo nodo
	 */
	public final Long getIdNodo() {
		return idNodo;
	}

	/**
	 * Getter.
	 * 
	 * @return	data attivazione
	 */
	public final Date getDataAttivazione() {
		return dataAttivazione;
	}

	/**
	 * Getter.
	 * 
	 * @return	data disattivazione
	 */
	public final Date getDataDisattivazione() {
		return dataDisattivazione;
	}

	/**
	 * Getter del flag segreteria.
	 *
	 * @return the flagSegreteria
	 */
	public final Integer getFlagSegreteria() {
		return flagSegreteria;
	}

	/**
	 * Setter del flag segreteria.
	 *
	 * @param flagSegreteria the flagSegreteria to set
	 */
	public final void setFlagSegreteria(final Integer flagSegreteria) {
		this.flagSegreteria = flagSegreteria;
	}

	/**
	 * Getter del id nodo padre.
	 *
	 * @return the idNodoPadre
	 */
	public Long getIdNodoPadre() {
		return idNodoPadre;
	}

	/**
	 * Setter del id nodo.
	 *
	 * @param idNodo the new id nodo
	 */
	public void setIdNodo(final Long idNodo) {
		this.idNodo = idNodo;
	}
	
	/**
	 * Getter del codice nodo.
	 *
	 * @return the codice nodo
	 */
	public String getCodiceNodo() {
		return codiceNodo;
	}

	/**
	 * Setter del codice nodo.
	 *
	 * @param codiceNodo the new codice nodo
	 */
	public void setCodiceNodo(final String codiceNodo) {
		this.codiceNodo = codiceNodo;
	}

	/**
	 * Getter del id tipo nodo.
	 *
	 * @return the id tipo nodo
	 */
	public Integer getIdTipoNodo() {
		return idTipoNodo;
	}

	/**
	 * Setter del id tipo nodo.
	 *
	 * @param idTipoNodo the new id tipo nodo
	 */
	public void setIdTipoNodo(final Integer idTipoNodo) {
		this.idTipoNodo = idTipoNodo;
	}

	/**
	 * Getter del id nodo corriere.
	 *
	 * @return the id nodo corriere
	 */
	public Long getIdNodoCorriere() {
		return idNodoCorriere;
	}

	/**
	 * Setter del id nodo corriere.
	 *
	 * @param idNodoCorriere the new id nodo corriere
	 */
	public void setIdNodoCorriere(final Long idNodoCorriere) {
		this.idNodoCorriere = idNodoCorriere;
	}

	/**
	 * Getter del id tipo struttura.
	 *
	 * @return the id tipo struttura
	 */
	public Integer getIdTipoStruttura() {
		return idTipoStruttura;
	}
	
	/**
	 * Getter del count ufficio.
	 *
	 * @return the count ufficio
	 */
	public Integer getCountUfficio() {
		if (countUfficio == null) {
			countUfficio = 0;
		}
		return countUfficio;
	}
	
	/**
	 * Setter del count ufficio.
	 *
	 * @param countUfficio the new count ufficio
	 */
	public void setCountUfficio(final Integer countUfficio) {
		this.countUfficio = countUfficio;
	}

	/**
	 * Checks if is ufficio delegato.
	 *
	 * @return true, if is ufficio delegato
	 */
	public boolean isUfficioDelegato() {
		return ufficioDelegato;
	}

	/**
	 * Setter del ufficio delegato.
	 *
	 * @param ufficioDelegato the new ufficio delegato
	 */
	public void setUfficioDelegato(final boolean ufficioDelegato) {
		this.ufficioDelegato = ufficioDelegato;
	}

	/**
	 * Checks if is ufficio libro firma fepa.
	 *
	 * @return true, if is ufficio libro firma fepa
	 */
	public boolean isUfficioLibroFirmaFepa() {
		return ufficioLibroFirmaFepa;
	}

	/**
	 * Setter del ufficio libro firma fepa.
	 *
	 * @param ufficioLibroFirmaFepa the new ufficio libro firma fepa
	 */
	public void setUfficioLibroFirmaFepa(final boolean ufficioLibroFirmaFepa) {
		this.ufficioLibroFirmaFepa = ufficioLibroFirmaFepa;
	}
	
	
	/**
	 * Compare to.
	 *
	 * @param o the o
	 * @return the int
	 */
	@Override
	public int compareTo(final Nodo o) {
		return this.countUfficio.compareTo(o.getCountUfficio());
	}

	/**
	 * Getter del colore ufficio.
	 *
	 * @return the colore ufficio
	 */
	public String getColoreUfficio() {
		return coloreUfficio;
	}
	
	/**
	 * Setter del colore ufficio.
	 *
	 * @param coloreUfficio the new colore ufficio
	 */
	public void setColoreUfficio(final String coloreUfficio) {
		this.coloreUfficio = coloreUfficio;
	}
	
	/**
	 * Getter del checks if is estendi visibilita.
	 *
	 * @return the checks if is estendi visibilita
	 */
	public boolean getIsEstendiVisibilita() {
		return isEstendiVisibilita;
	}
	
	/**
	 * Setter del checks if is estendi visibilita.
	 *
	 * @param isEstendiVisibilita the new checks if is estendi visibilita
	 */
	public void setIsEstendiVisibilita(final boolean isEstendiVisibilita) {
		this.isEstendiVisibilita = isEstendiVisibilita;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idNodo == null) ? 0 : idNodo.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Nodo other = (Nodo) obj;
		if (idNodo == null) {
			if (other.idNodo != null)
				return false;
		} else if (!idNodo.equals(other.idNodo))
			return false;
		return true;
	}

	
}