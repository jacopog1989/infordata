package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Date;
import java.util.List;

import it.ibm.red.business.dto.AllegatoOPDTO;
import it.ibm.red.business.dto.DecretoDirigenzialeDTO;
import it.ibm.red.business.dto.DetailDocumentoDTO;
import it.ibm.red.business.dto.DocumentoFascicoloFepaDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FadAmministrazioneDTO;
import it.ibm.red.business.dto.FadRagioneriaDTO;
import it.ibm.red.business.dto.FascicoloFEPAInfoDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.OrdineDiPagamentoDTO;
import it.ibm.red.business.dto.ProtocolloDTO;
import it.ibm.red.business.dto.RaccoltaFadDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.TrasmissioneDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Facade del servizio che gestisce FEPA.
 */
public interface IFepaFacadeSRV extends Serializable {

	/**
	 * @param fdaDTO
	 * @param processoManuale
	 * @param utente
	 * @return
	 */
	String creaRaccoltaFad(RaccoltaFadDTO fdaDTO, Boolean processoManuale, UtenteDTO utente);

	/**
	 * Invocazione WS per aggiornamento dti fascicolo Raccolta Provvisoria.
	 * 
	 * @param idFascicoloRaccoltaProvvisoria
	 * @param processoManuale
	 * @param utente
	 * @param idNodoDirigente
	 * @param descNodoDirigente
	 * @return
	 */
	Boolean updateCreatoreRaccoltaProvvisoria(RaccoltaFadDTO fdaDTO, String idFascicoloRaccoltaProvvisoria, Boolean processoManuale,
			UtenteDTO utente, String idNodoDirigente, String descNodoDirigente);

	/**
	 * Aggiunge Documenti al Fascicolo raccolta provvisoria.
	 * 
	 * @param fadDto
	 * @param utente
	 * @param processoManuale
	 * @param connection
	 */
	void addDocumentoFascicoloRaccoltaProvvisoria(RaccoltaFadDTO fadDto, UtenteDTO utente, Boolean processoManuale,
			Connection connection);

	/**
	 * @return
	 */
	List<FadAmministrazioneDTO> getComboAmministrazioni();

	/**
	 * @param codiceAmministrazione
	 * @return
	 */
	FadRagioneriaDTO findRagioneriaByCodiceAmministrazione(String codiceAmministrazione);

	/***************** DECRETO ********************/

	/**
	 * Associa il decreto al workflow.
	 * 
	 * Fascicola i vari elementi non fascicolati
	 * 
	 * 
	 * @param utente
	 * @param wobNumber
	 * @param documentTitleDaAggiungere
	 * @param documentTitleDaRimuovere
	 * @return
	 */
	EsitoOperazioneDTO associaDecreto(UtenteDTO utente, String wobNumber, List<String> wobNumbersDaAggiungere);

	/**
	 * Invia il decreto in firma.
	 * @param utente
	 * @param wobNumber
	 * @param idUfficioFirma
	 * @param idUtenteFirma
	 * @param oggetto
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO invioDecretoInFirma(UtenteDTO utente, String wobNumber, Long idUfficioFirma, Long idUtenteFirma,
			String oggetto);

	/**
	 * Associa il decreto al workflow.
	 * 
	 * Fascicola i vari elementi non fascicolati
	 * 
	 * 
	 * @param utente
	 * @param wobNumber
	 * @param documentTitleDaAggiungere
	 * @param documentTitleDaRimuovere
	 * @return
	 */
	EsitoOperazioneDTO modificaAssociazioni(UtenteDTO utente, String wobNumber, List<String> wobNumbersDaAggiungere,
			List<String> wobNumbersDaRimuovere);

	/**
	 * Ottiene i decreti da lavorare.
	 * @param selectedDSRWFNumber
	 * @param utente
	 * @return lista di decreti dirigenziali
	 */
	List<DecretoDirigenzialeDTO> getDecretiDaLavorare(String selectedDSRWFNumber, UtenteDTO utente);

	/**
	 * Ottiene gli ordini di pagamento.
	 * @param utente
	 * @param documentTitleDecreto
	 * @return lista degli ordini di pagamento
	 */
	List<OrdineDiPagamentoDTO> getOrdinePagamentoDecreto(UtenteDTO utente, String documentTitleDecreto);

	/**
	 * Ottiene gli allegati all'ordine di pagamento.
	 * @param utente
	 * @param guid
	 * @return lista degli allegati all'ordine di pagamento
	 */
	List<AllegatoOPDTO> getAllegatiOP(UtenteDTO utente, String guid);

	/**
	 * Eminina gli allegati all'ordine di pagamento.
	 * @param utente
	 * @param idDecreto
	 * @param guid
	 */
	void deleteAllegatiOP(UtenteDTO utente, String idDecreto, String guid);

	/**
	 * Seleziona gli allegati al documento aggiuntivo.
	 * @param utente
	 * @param idDocumentoAggiuntivo
	 * @param ordiniDiPagamento
	 */
	void selectOPAllegatiAlDocAggiuntivo(UtenteDTO utente, String idDocumentoAggiuntivo,
			List<OrdineDiPagamentoDTO> ordiniDiPagamento);

	/**
	 * Salva l'associazione all'allegato all'ordine di pagamento.
	 * @param utente
	 * @param ordiniDiPagamento
	 * @param guidAllegato
	 */
	void salvaAssociazioneAllegatoOP(UtenteDTO utente, List<OrdineDiPagamentoDTO> ordiniDiPagamento,
			String guidAllegato);

	/**
	 * Recupera riferimenti alla mail a pertire dal guid del documento.
	 * 
	 * @param id
	 *            il guid del documento
	 * @param utente
	 * @return la trasmissione se non trova la trasmissione ritorna null
	 */
	TrasmissioneDTO getTrasmissione(String id, UtenteDTO utente);

	/**
	 * Esegue la messa agli atti.
	 * @param wobNumber
	 * @param utente
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO mettiAgliAttiSysFineLavorazione(String wobNumber, UtenteDTO utente);

	/**
	 * Ottiene l'elenco dei decreti FEPA.
	 * @param wobNumber
	 * @param utente
	 * @return elenco dei decreti FEPA
	 */
	String[] ottieniElencoDecretiFepa(String wobNumber, UtenteDTO utente);

	/**
	 * Ottiene le tipologie di documenti aggiuntivi.
	 * @return lista delle tipologie di documenti
	 */
	List<TipologiaDocumentoDTO> getTipologieDocumentiAggiuntivi();

	/**
	 * Ottiene i fascicoli dell'ordine di pagamento.
	 * @param idFascicolo
	 * @return lista di documenti dei fascicoli
	 */
	List<DocumentoFascicoloFepaDTO> getFascicoliOP(String idFascicolo);

	/**
	 * Ottiene i fascicoli della fattura.
	 * @param idFascicolo
	 * @return lista dei documenti dei fascicoli
	 */
	List<DocumentoFascicoloFepaDTO> getFascicoliFattura(String idFascicolo);

	/**
	 * Ottiene i fascicoli del verbale di revisione.
	 * @param idFascicolo
	 * @return lista dei documenti dei fascicoli
	 */
	List<DocumentoFascicoloFepaDTO> getFascicoliVerbaleRevisione(String idFascicolo);

	/**
	 * Ottiene i fascicoli di bilancio enti.
	 * @param idFascicolo
	 * @return lista dei documenti dei fascicoli
	 */
	List<DocumentoFascicoloFepaDTO> getFascicoliBilancioEnti(String idFascicolo);

	/**
	 * Esegue il download del documento del fascicolo dell'ordine di pagamento.
	 * @param guidFascicolo
	 * @param guidDocumento
	 * @return file
	 */
	FileDTO downloadDocumentoFascicoloOP(String guidFascicolo, String guidDocumento);
	
	/**
	 * Esegue il download del documento del fascicolo della fattura.
	 * @param guidFascicolo
	 * @param guidDocumento
	 * @return file
	 */
	FileDTO downloadDocumentoFascicoloFattura(String guidFascicolo, String guidDocumento);
	
	/**
	 * Esegue il download del documento del verbale di revisione.
	 * @param guidFascicolo
	 * @param guidDocumento
	 * @return file
	 */
	FileDTO downloadDocumentoVerbaliRevisione(String guidFascicolo, String guidDocumento);

	/**
	 * Esegue il download del documento di bilancio enti.
	 * @param guidFascicolo
	 * @param guidDocumento
	 * @return file
	 */
	FileDTO downloadDocumentoBilancioEnti(String guidFascicolo, String guidDocumento);
	
	/**
	 * Esegue il download del documento SICOGE.
	 * @param guidFascicolo
	 * @param guidDocumento
	 * @param tipoFascicolo
	 * @param codiceAoo
	 * @param guidFascicoloRiferimento
	 * @return file
	 */
	FileDTO downloadDocumentoSicoge(String guidFascicolo, String guidDocumento, String tipoFascicolo, String codiceAoo, String guidFascicoloRiferimento);

	/**
	 * Associa il protocollo del verbale revisione.
	 * @param idFascicolo
	 * @param idDocumenti
	 * @param numeroProtocollo
	 * @param dataProtocollo
	 * @return true o false
	 */
	boolean associaProtocolloVerbaleRevisione(String idFascicolo, List<String> idDocumenti, String numeroProtocollo, Date dataProtocollo);

	/**
	 * Associa il protocollo di bilancio enti.
	 * @param idFascicolo
	 * @param idDocumenti
	 * @param numeroProtocollo
	 * @param dataProtocollo
	 * @return true o false
	 */
	boolean associaProtocolloBilancioEnti(String idFascicolo, List<String> idDocumenti, String numeroProtocollo, Date dataProtocollo);

	/**
	 * Mostra il fascicolo di bilancio enti.
	 * @param idTipologiaDocumento
	 * @return true o false
	 */
	boolean showBilEntiFasc(Integer idTipologiaDocumento);

	/**
	 * Mostra il fascicolo di revisione IGF.
	 * @param idTipologiaDocumento
	 * @return true o false
	 */
	boolean showRevIGFFasc(Integer idTipologiaDocumento);
	
	/**
	 * Esegue la messa agli atti bilancio enti.
	 * @param idFascicolo
	 * @return true o false
	 */
	boolean mettiAgliAttiBilancioEnti(String idFascicolo);
	
	/**
	 * Aggiunge il documento al fascicolo di bilancio enti.
	 * @param idFascicolo
	 * @param documentDetailDTO
	 * @param protocollo
	 * @param tipologiaProtocollo
	 * @param firmato
	 * @param idAoo
	 */
	void addDocumentoToFascicoloBilancioEnti(String idFascicolo, DetailDocumentoDTO documentDetailDTO, ProtocolloDTO protocollo, 
			String tipologiaProtocollo, boolean firmato, Integer idAoo);

	/**
	 * Invia DD e DSR firmati.
	 * @param idFascicolo
	 * @param wobDichiarazioneServiziResi
	 * @return
	 */
	int inviaDDDSRFirmati(String idFascicolo, List<String> wobDichiarazioneServiziResi);

	/**
	 * Invia DD.
	 * @param idDecreto
	 * @return
	 */
	int inviaDD(String idDecreto);

	/**
	 * Ottiene i fascicoli SICOGE.
	 * @param fullDetail
	 * @param idFascicolo
	 * @param tipoFascicoloFepaSicoge
	 * @param codiceAoo
	 * @param idFascicoloRiferimento
	 * @return fascicolo FEPA
	 */
	FascicoloFEPAInfoDTO getFascicoliSicoge(Boolean fullDetail, String idFascicolo, String tipoFascicoloFepaSicoge,
			String codiceAoo, String idFascicoloRiferimento);
	
}
