package it.ibm.red.business.enums;

import it.pkbox.client.Envelope;

/**
 * The Enum ModeEnum.
 *
 * @author CPIERASC
 * 
 *         Enumerato per specificare la modalità di firma CADES.
 */
public enum ModeEnum {
	/**
	 * P7M come busta contenente documento originale e firma.
	 */
	IMPLICIT_DATA_ATTACHED(Envelope.implicitMode), 
	/**
	 * P7M contenente la sola firma ma non il documento originale.
	 */
	EXPLICIT_DATA_DETACHAED(Envelope.explicitMode);
	
	/**
	 * Identificativo modalità firma CADES.
	 */
	private Integer value;
	
	/**
	 * Costruttore.
	 * 
	 * @param v	identificativo modalità firma CADES
	 */
	ModeEnum(final Integer v) {
		value = v;
	}
	
	/**
	 * Getter identificativo modalità firma CADES.
	 * 
	 * @return	identificativo modalità firma CADES
	 */
	public Integer getValue() {
		return value;
	}
}
