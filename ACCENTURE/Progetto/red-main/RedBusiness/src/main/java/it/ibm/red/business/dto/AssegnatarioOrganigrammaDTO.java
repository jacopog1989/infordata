package it.ibm.red.business.dto;

import java.io.Serializable;

/**
 * DTO che individua gli assegnatari selezionati da un organigramma.
 *
 * L'oggetto rappresenta il nodo di un organigramma e in particolare la selezione dell'utente di un nodo dell'organigramma
 *
 * L'oggetto è usato come input per assegnare le security e per le response
 */
public class AssegnatarioOrganigrammaDTO implements Serializable {

	private static final long serialVersionUID = 8410736447133025666L;

	/**
	 * Utente null.
	 */
	/**
	 * Utente null.
	 */
	private static final Long NULL_UTENTE = 0L;

	/**
	 * id dell'ufficio/ispettorato/segreteria.
	 */
	protected Long idNodo;
	/**
	 * identificativo dell'utente (l'utente appartiene all'ufficio identificato da idNodo).
	 */
	protected Long idUtente;

	/**
	 * Costruttore di classe.
	 * @param idNodo
	 * @param inIdUtente
	 */
	public AssegnatarioOrganigrammaDTO(final Long idNodo, final Long inIdUtente) {
		super();
		
		if (idNodo == null || idNodo <= NULL_UTENTE) {
			throw new IllegalArgumentException("Tentativo di definire un AssegnatarioOrganigrammaDTO con id non validi idNodo: " + idNodo + ", idUtente: " + idUtente);
		}
		Long idUsr = inIdUtente;
		if (idUsr == null) {
			idUsr = NULL_UTENTE;
		}
		this.idNodo = idNodo;
		this.idUtente = idUsr;
	}

	/**
	 * Costruttore di classe.
	 * @param inIdNodo
	 */
	public AssegnatarioOrganigrammaDTO(final Long inIdNodo) {
		this(inIdNodo, NULL_UTENTE);
	}

	/**
	 * Restituisce l'id del nodo.
	 * @return idNodo
	 */
	public Long getIdNodo() {
		return idNodo;
	}

	/**
	 * Imposta l'id del nodo.
	 * @param idNodo
	 */
	public void setIdNodo(final Long idNodo) {
		this.idNodo = idNodo;
	}

	/**
	 * Restituisce l'id utente.
	 * @return idUtente
	 */
	public Long getIdUtente() {
		return idUtente;
	}

	/**
	 * Imposta l'id utente.
	 * @param idUtente
	 */
	public void setIdUtente(final Long idUtente) {
		this.idUtente = idUtente;
	}

	/**
	 * Restituisce true se l'id utente è null, questo vuol dire che si tratta di un ufficio.
	 * Non garantisce che l'id ufficio sia valorizzato.
	 * @return not utente
	 */
	public boolean isUfficio() {
		return NULL_UTENTE.equals(idUtente); 
	}

	/**
	 * @see java.lang.Object#hashCode().
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idNodo == null) ? 0 : idNodo.hashCode());
		result = prime * result + ((idUtente == null) ? 0 : idUtente.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object).
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AssegnatarioOrganigrammaDTO other = (AssegnatarioOrganigrammaDTO) obj;
		if (idNodo == null) {
			if (other.idNodo != null) {
				return false;
			}
		} else if (!idNodo.equals(other.idNodo)) {
			return false;
		}
		if (idUtente == null) {
			if (other.idUtente != null) {
				return false;
			}
		} else if (!idUtente.equals(other.idUtente)) {
			return false;
		}
		return true;
	}
}
