package it.ibm.red.business.dao.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.activation.DataHandler;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IRecogniformDAO;
import it.ibm.red.business.dto.DocumentoRecogniformDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.FileUtils;

/**
 * DAO gestione form recogniform.
 */
@Repository
public class RecogniformDAO extends AbstractDAO implements IRecogniformDAO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -4649751002118496845L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RecogniformDAO.class);

	/**
	 * Query.
	 */
	private static final String SQL_ALLDOCUMENTBARCODE = "SELECT * FROM D_DOC_RECOGNIFORM WHERE BARCODE IS NOT NULL AND BARCODE_DOC_PRINCIPALE IS NULL AND STATO IN (1,5,6) AND IDNODO IN (SELECT IDNODO FROM NODO WHERE IDAOO = ?)";

	/**
	 * Query.
	 */
	private static final String SQL_GETDOCUMENTO_BY_BARCODE = "SELECT * FROM D_DOC_RECOGNIFORM WHERE BARCODE = ?";

	/**
	 * Query.
	 */
	private static final String SQL_GETALLEGATI_BY_BARCODE_PRINCIPALE = "SELECT * FROM D_DOC_RECOGNIFORM WHERE BARCODE_DOC_PRINCIPALE=? AND STATO IN (1,5,6)";

	/**
	 * Query.
	 */
	private static final String SQL_GETSTATI = "SELECT SEQU_K_LK_STATO, DESC_STATO FROM D_LK_STATO";
	
	/**
	 * @see it.ibm.red.business.dao.IRecogniformDAO#getBarcodeDaElaborare(java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public List<DocumentoRecogniformDTO> getBarcodeDaElaborare(final Integer idAoo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<DocumentoRecogniformDTO> documenti = new ArrayList<>();
		
		try {
			ps = con.prepareStatement(SQL_ALLDOCUMENTBARCODE);
			ps.setInt(1, idAoo);
			
			rs = ps.executeQuery();
			fetchMultiResult(rs, documenti);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei barcode da elaborare nella coda Recogniform per l'aoo " + idAoo, e);
		} finally {
			closeResultset(rs);
			closeStatement(ps);
		}
		
		return documenti;
	}	
	
	private static void fetchMultiResult(final ResultSet rs, final List<DocumentoRecogniformDTO> documenti) throws SQLException, IOException {
		DocumentoRecogniformDTO doc = null;
		while (rs.next()) {
			doc = new DocumentoRecogniformDTO();
			populateVO(doc, rs);
			documenti.add(doc);
		}
	}
	
	private static void populateVO(final DocumentoRecogniformDTO doc, final ResultSet rs) throws SQLException, IOException {
		doc.setIdRecogniform(rs.getInt("SEQU_K_DOC_RECOGNIFORM"));
		doc.setOggetto(rs.getString("OGGETTO"));		
		doc.setMittente(rs.getString("MITTENTE"));		
		doc.setProtocolloMittente(rs.getString("PROTOCOLLO_MITTENTE"));		
		doc.setBarcode(rs.getString("BARCODE"));		
		doc.setStato(rs.getInt("STATO"));		
		doc.setDataHandler(blobToHashMap(rs.getBlob("CONTENT_DOC")));		
		doc.setNomeFile(rs.getString("NOME_FILE"));
		doc.setMimetype(rs.getString("MIMETYPE"));
		doc.setIdnodo(rs.getInt("IDNODO"));
		doc.setBarcodeDocPrincipale(rs.getString("BARCODE_DOC_PRINCIPALE"));
	}
	
	private static DataHandler blobToHashMap(final Blob sqlBlob) throws IOException, SQLException {
		byte[] data = null;
		DataHandler dh = null;
		if (sqlBlob != null) {
			data = sqlBlob.getBytes(1, (int) sqlBlob.length());
			final InputStream in = new ByteArrayInputStream(data);
			dh = FileUtils.toDataHandler(in);
		}
		
		return dh;
	}

	/**
	 * @see it.ibm.red.business.dao.IRecogniformDAO#getDocumentoByBarcode(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public List<DocumentoRecogniformDTO> getDocumentoByBarcode(final String barcode, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<DocumentoRecogniformDTO> documenti = new ArrayList<>();
		
		try {
			ps = con.prepareStatement(SQL_GETDOCUMENTO_BY_BARCODE);
			ps.setString(1, barcode);
			
			rs = ps.executeQuery();
			fetchMultiResult(rs, documenti);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei documenti nella coda Recogniform per il barcode " + barcode, e);
			throw new RedException("Errore in fase di recupero dei documenti recogniform per il barcode " + barcode, e);
			
		} finally {
			closeResultset(rs);
			closeStatement(ps);
		}
		
		return documenti;
	}

	/**
	 * @see it.ibm.red.business.dao.IRecogniformDAO#updateStatoDocumento(java.lang.Integer,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public void updateStatoDocumento(final Integer stato, final String barcode, final Connection con) {
		PreparedStatement ps = null;
		try {
			final String updateQuery = "UPDATE D_DOC_RECOGNIFORM SET STATO = ? where (BARCODE = ? OR BARCODE_DOC_PRINCIPALE=?)";
			
			ps = con.prepareStatement(updateQuery);
			ps.setInt(1, stato);
			ps.setString(2, barcode);
			ps.setString(3, barcode);
			
			ps.executeUpdate();
		} catch (final SQLException e) {
			throw new RedException("Errore in fase di aggiornamento dello stato del barcode " + barcode, e);
		} finally {
			closeStatement(ps, null);
		}			
	}

	/**
	 * @see it.ibm.red.business.dao.IRecogniformDAO#getAllegatiDaPrincipale(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public List<DocumentoRecogniformDTO> getAllegatiDaPrincipale(final String barcodePrincipale, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<DocumentoRecogniformDTO> documenti = new ArrayList<>();
		
		try {
			ps = con.prepareStatement(SQL_GETALLEGATI_BY_BARCODE_PRINCIPALE);
			ps.setString(1, barcodePrincipale);
			
			rs = ps.executeQuery();	
			fetchMultiResult(rs, documenti);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero degli allegati da elaborare nella coda Recogniform per il documento principale con barcode " + barcodePrincipale, e);
		} finally {
			closeResultset(rs);
			closeStatement(ps);
		}
		
		return documenti;
	}

	/**
	 * @see it.ibm.red.business.dao.IRecogniformDAO#getStati(java.sql.Connection).
	 */
	@Override
	public Map<Integer, String> getStati(final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final Map<Integer, String> stati = new HashMap<>();
		
		try {
			ps = con.prepareStatement(SQL_GETSTATI);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				stati.put(rs.getInt("SEQU_K_LK_STATO"), rs.getString("DESC_STATO"));
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero degli stati ", e);
		} finally {
			closeResultset(rs);
			closeStatement(ps);
		}
		
		return stati;
	}
}
