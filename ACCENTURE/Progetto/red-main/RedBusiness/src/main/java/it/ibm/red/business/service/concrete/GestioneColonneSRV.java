package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IGestioneColonneDAO;
import it.ibm.red.business.dto.ColonneCodeDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.IGestioneColonneSRV;

/**
 * SRV per gestione delle colonne;
 *
 * @author VINGENITO
 */
@Service
public class GestioneColonneSRV extends AbstractService implements IGestioneColonneSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -355559510909028728L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(GestioneColonneSRV.class.getName());

	/**
	 * DAO.
	 */
	@Autowired
	private IGestioneColonneDAO gestioneColDAO;

	/**
	 * @see it.ibm.red.business.service.facade.IGestioneColonneFacadeSRV#salvaColonneScelte(java.lang.Long, java.util.List).
	 */
	@Override
	public void salvaColonneScelte(final Long idUtente, final List<ColonneCodeDTO> listColCodeDTO) {
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), true);
			
			gestioneColDAO.deleteColonneScelte(idUtente, conn);
			for (int i = 3; i < listColCodeDTO.size(); i++) { 
					gestioneColDAO.salvaColonneScelte(idUtente, listColCodeDTO.get(i).getColEnum().getDescrColonna(), listColCodeDTO.get(i).isVisibilitaColonna(), conn);
			}
			commitConnection(conn);
		} catch (final Exception ex) {
			rollbackConnection(conn);
			LOGGER.error("Errore durante il salvataggio delle colonne scelte " + ex);
			throw new RedException("Errore durante il salvataggio delle colonne scelte " + ex);
		} finally {
			closeConnection(conn);
		}

	}

	/**
	 * @see it.ibm.red.business.service.facade.IGestioneColonneFacadeSRV#getColonneScelte(java.lang.Long).
	 */
	@Override
	public List<ColonneCodeDTO> getColonneScelte(final Long idUtente) {
		Connection conn = null;
		List<ColonneCodeDTO> output = new ArrayList<>(); 
		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			output = gestioneColDAO.getColonneScelte(idUtente, conn); 
		} catch (final Exception ex) { 
			LOGGER.error("Errore durante il recupero delle colonne scelte " + ex);
			throw new RedException("Errore durante il recupero delle colonne scelte " + ex);
		} finally {
			closeConnection(conn);
		}
		return output;
	}
}