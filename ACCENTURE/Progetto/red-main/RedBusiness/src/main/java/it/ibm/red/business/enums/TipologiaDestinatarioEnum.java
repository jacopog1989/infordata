package it.ibm.red.business.enums;

/**
 * @author Slac
 *
 * interno(I) / esterno(E)
 * 
 */
public enum TipologiaDestinatarioEnum {

	/**
	 * Valore.
	 */
	INTERNO("I"),
	
	/**
	 * Valore.
	 */
	ESTERNO("E");
	
	/**
	 * Tipologia.
	 */
	private String tipologia;
	
	
	/**
	 * 
	 * @param tipologia
	 */
	TipologiaDestinatarioEnum(final String inTipologia) {
		this.tipologia = inTipologia;
	}

	/**
	 * 
	 * @return
	 */
	public String getTipologia() {
		return tipologia;
	}
	
	/**
	 * 
	 * @param tipologia
	 * @return
	 */
	public static TipologiaDestinatarioEnum getByTipologia(final String tipologia) {
		TipologiaDestinatarioEnum output = null;

		if (tipologia == null) {
			return output;
		}
		
		for (TipologiaDestinatarioEnum t:TipologiaDestinatarioEnum.values()) {
			if (t.getTipologia().equalsIgnoreCase(tipologia)) {
				output = t;
				break;
			}
		}
		return output;
	}
}