package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.ITipoAssegnazioneDAO;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.exception.RedException;

/**
 * @author SLac
 */
@Repository
public class TipoAssegnazioneDAO extends AbstractDAO implements ITipoAssegnazioneDAO {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 96349959427447552L;

	/**
	 * @see it.ibm.red.business.dao.ITipoAssegnazioneDAO#getTipiAssegnazioneManualeByIdAoo(java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public List<TipoAssegnazioneEnum> getTipiAssegnazioneManualeByIdAoo(final Integer idAOO, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<TipoAssegnazioneEnum> list = null;
		try {
			String querySQL = "SELECT t.idtipoassegnazione FROM tipoassegnazione t "
					+ " LEFT OUTER JOIN tipoassegnazione_aoo ta "
					+ "    ON t.idtipoassegnazione = ta.idtipoassegnazione "
					+ " WHERE (ta.idaoo IS NULL OR ta.idaoo = ?) " 
					+ "   AND t.itermanuale = 1"
					+ " ORDER BY t.idtipoassegnazione";
			
			
			ps = con.prepareStatement(querySQL);
			ps.setInt(1, idAOO);
			
			rs = ps.executeQuery();
			
			list = new ArrayList<>();
			TipoAssegnazioneEnum t = null;
			while (rs.next()) {
				t = TipoAssegnazioneEnum.get(rs.getInt("idTipoAssegnazione"));
				list.add(t);
			}
		} catch (SQLException e) {
			throw new RedException("Errore durante il recupero dei tipi assegnazione manuale per l'AOO: " + idAOO, e);
		} finally {
			closeStatement(ps, rs);
		}
		return list;
	}
}
