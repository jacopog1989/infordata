package it.ibm.red.business.utils;

import java.util.List;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.AccessoFunzionalitaEnum;
import it.ibm.red.business.enums.PermessiAOOEnum;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.enums.TipologiaNodoEnum;

/**
 * Classe PermessiUtils.
 */
public final class PermessiUtils {

	private PermessiUtils() {
	}
	
	/**
	 * Metodo generico che attraverso il long dei permessi restituisce true se il permesso è presente.
	 *
	 * @param permessi the permessi
	 * @param permesso the permesso
	 * @return the boolean
	 */
	public static boolean hasPermesso(final Long permessi, final PermessiEnum permesso) {
		return (permessi & permesso.getId()) > 0;
	}
	
	/**
	 * Data una lista di permessi controllo se c'e' il permesso cercato.
	 *
	 * @param permessi the permessi
	 * @param permesso the permesso
	 * @return the boolean
	 */
	public static boolean hasPermesso(final List<Long> permessi, final PermessiEnum permesso) {
		boolean hasPermesso = false;
		for (final Long p : permessi) {
			if ((p & permesso.getId()) > 0) {
				hasPermesso = true;
				break;
			}
		}
		return hasPermesso;
	}
	
	
	
	/**
	 * Metodo generico che attraverso il long dei permessi restituisce true se il permesso è presente.
	 *
	 * @param permessi the permessi
	 * @param permesso the permesso
	 * @return the boolean
	 */
	public static boolean hasPermesso(final Long permessi, final PermessiAOOEnum permesso) {
		return (permessi & permesso.getId()) > 0;
	}
	
	/**
	 * Data una lista di permessi controllo se c'e' il permesso cercato.
	 *
	 * @param permessi the permessi
	 * @param permesso the permesso
	 * @return the boolean
	 */
	public static boolean hasPermesso(final List<Long> permessi, final PermessiAOOEnum permesso) {
		boolean hasPermesso = false;
		for (final Long p : permessi) {
			if ((p & permesso.getId()) > 0) {
				hasPermesso = true;
				break;
			}
		}
		return hasPermesso;
	}
	
	/**
	 * Controlla se l'utente ha i permessi di amministratore
	 *
	 * @param permessi the permessi
	 * @return the boolean
	 */
	public static boolean isAmministratore(final Long permessi) {
		return hasPermesso(permessi, PermessiEnum.GESTIONE_AOO);
	}

	/**
	 * Controlla se l'utente ha i permessi di delegato
	 *
	 * @param permessi the permessi
	 * @return the boolean
	 */
	public static boolean isDelegato(final Long permessi) {
		return hasPermesso(permessi, PermessiEnum.DELEGA);
	}
	
	/**
	 * Controlla se l'utente ha i permessi di corriere
	 *
	 * @param permessi the permessi
	 * @return the boolean
	 */
	public static boolean isCorriere(final Long permessi) {
		return hasPermesso(permessi, PermessiEnum.CODA_CORRIERE);
	}
	
	/**
	 * Controlla se l'utente ha i permessi per accedere allo scadenzario
	 *
	 * @param permessi the permessi
	 * @return the boolean
	 */
	public static boolean isTabScadenzario(final Long permessi) {
		return hasPermesso(permessi, PermessiEnum.SCADENZARIO_TAB);
	}
	
	/**
	 * Controlla se l'utente ha i permessi per accedere al libro firma
	 *
	 * @param permessi the permessi
	 * @return the boolean
	 */
	public static boolean hasLibroFirma(final Long permessi) {
		final boolean bFirma = hasPermesso(permessi, PermessiEnum.FIRMA);
		final boolean bSigla = hasPermesso(permessi, PermessiEnum.SIGLA);
		final boolean bVista = hasPermesso(permessi, PermessiEnum.VISTO);
		return bFirma || bSigla || bVista;
	}
	
	/**
	 *Controlla se l'utente ha i permessi per accedere a dsr
	 *
	 * @param permessi the permessi
	 * @return the boolean
	 */
	public static boolean isDSR(final Long permessi) {
		return hasPermesso(permessi, PermessiEnum.DSR);
	}
	
	/**
	 * Controlla se l'utente ha i permessi di corte dei conti
	 *
	 * @param permessi the permessi
	 * @return the boolean
	 */
	public static boolean isCorteDeiConti(final Long permessi) {
		return hasPermesso(permessi, PermessiEnum.AOO_CORTE_DEI_CONTI);
	}
	
	/**
	 * Controlla se l'utente ha i permessi per riattivare il procedimento
	 *
	 * @param permessi the permessi
	 * @return the boolean
	 */
	public static boolean isRiattivaProcedimento(final Long permessi) {
		return hasPermesso(permessi, PermessiEnum.RIATTIVA_PROCEDIMENTO);
	}
	
	/**
	 * Controlla se l'utente ha i permessi di copia conforme
	 *
	 * @param permessi the permessi
	 * @return the boolean
	 */
	public static boolean hasCopiaConforme(final Long permessi) {
		return hasPermesso(permessi, PermessiEnum.AOO_FIRMA_COPIA_CONFORME);
	}
	
	/**
	 *Controlla se l'utente ha i permessi per firmare da mobile
	 *
	 * @param permessi the permessi
	 * @return the boolean
	 */
	public static boolean hasLibroFirmaMobile(final Long permessi) {
		return hasPermesso(permessi, PermessiEnum.LIBRO_FIRMA_MOBILE);
	}
	
	/**
	 * Controlla se l'utente ha i permessi di firma delegato
	 *
	 * @param permessi the permessi
	 * @return the boolean
	 */
	public static boolean hasLibroFirmaDelegato(final Long permessi) {
		return hasPermesso(permessi, PermessiEnum.DELEGA);
	}
	
	/**
	 * Metodo che controlla se un utente può accedere ad una data funzionalità
	 *
	 * @param accessoEnum the accesso enum
	 * @param utente the utente
	 * @return true, if successful
	 */
	public static boolean haAccessoAllaFunzionalita(final AccessoFunzionalitaEnum accessoEnum, final UtenteDTO utente) {
		return haAccessoAllaFunzionalita(accessoEnum, utente.getPermessi(), utente.getPermessiAOO(), utente.getIdTipoNodo());
	}
	
	/**
	 * Ha accesso alla funzionalita.
	 *
	 * @param accessoEnum the accesso enum
	 * @param permessi the permessi
	 * @param permessiAOO the permessi AOO
	 * @param idTipologiaNodoUtente the id tipologia nodo utente
	 * @return true, if successful
	 */
	public static boolean haAccessoAllaFunzionalita(final AccessoFunzionalitaEnum accessoEnum, final Long permessi, final Long permessiAOO, 
			final Integer idTipologiaNodoUtente) {
		boolean accede = true;
		
		if (accessoEnum == null) {
			return accede;
		}
		
		/**Permessi da utilizzare per gridMenu -- START**/
		if (accessoEnum.equals(AccessoFunzionalitaEnum.LIBROFIRMA)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.FIRMA);
		}
		if (accessoEnum.equals(AccessoFunzionalitaEnum.LIBROFIRMADELEGATO)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.DELEGA);
		}
		
		if (accessoEnum.equals(AccessoFunzionalitaEnum.MENUUFFICIO)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.CODA_CORRIERE);
		}
		if (accessoEnum.equals(AccessoFunzionalitaEnum.CORRIERE)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.CODA_CORRIERE);
		}
		if (accessoEnum.equals(AccessoFunzionalitaEnum.ASSEGNATE)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.CODA_CORRIERE);
		}
		if (accessoEnum.equals(AccessoFunzionalitaEnum.CHIUSE)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.CODA_CORRIERE);
		}
		if (accessoEnum.equals(AccessoFunzionalitaEnum.GIROVISTI)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.CODA_CORRIERE) && PermessiUtils.hasPermesso(permessiAOO, PermessiAOOEnum.AL_GIRO_VISTI);
		}
		if (accessoEnum.equals(AccessoFunzionalitaEnum.SPEDIZIONE)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.ACCESSO_CODA_IN_SPEDIZIONE) || PermessiUtils.hasPermesso(permessi, PermessiEnum.CODA_CORRIERE);
		}
		if (accessoEnum.equals(AccessoFunzionalitaEnum.MOZIONE)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.MOZIONE);
		}
		if (accessoEnum.equals(AccessoFunzionalitaEnum.MENUCREAZIONE)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.PROTOCOLLO_IN_ENTRATA) || PermessiUtils.hasPermesso(permessi, PermessiEnum.MOZIONE);
		}
		if (accessoEnum.equals(AccessoFunzionalitaEnum.DOCINGRESSO)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.PROTOCOLLO_IN_ENTRATA);
		}
		if (accessoEnum.equals(AccessoFunzionalitaEnum.DOCUSCITA)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.MOZIONE);
		}
		if (accessoEnum.equals(AccessoFunzionalitaEnum.RICERCASIGI)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.RICERCA_AUTORIZZAZIONE_ALLA_SPESA);
		}
		
		if (accessoEnum.equals(AccessoFunzionalitaEnum.RICERCAFEPA)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.PERMESSO_FATTURA_SOGEI) 
					|| PermessiUtils.hasPermesso(permessi, PermessiEnum.PERMESSO_FATTURA_NON_SOGEI) 
					|| PermessiUtils.hasPermesso(permessi, PermessiEnum.PERMESSO_FATTURA_MISTO) 
					|| PermessiUtils.hasPermesso(permessi, PermessiEnum.PERMESSO_DIRIGENTE_FEPA);
		}
		
		if (accessoEnum.equals(AccessoFunzionalitaEnum.RICERCAPROTOCOLLO)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.REGISTRO_PROTOCOLLO) 
					|| PermessiUtils.hasPermesso(permessi, PermessiEnum.RICERCA_NPS) 
					|| PermessiUtils.hasPermesso(permessi, PermessiEnum.RICERCA_REGISTRAZIONI_AUSILIARIE);
		}
		
		if (accessoEnum.equals(AccessoFunzionalitaEnum.RICERCANPS)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.RICERCA_NPS);
		}
		
		if (accessoEnum.equals(AccessoFunzionalitaEnum.STAMPAREGISTRO)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.REGISTRO_PROTOCOLLO);
		}
		
		if (accessoEnum.equals(AccessoFunzionalitaEnum.ORGANIGRAMMA)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.VISIBILITA_SCHEDA_ORGANIGRAMMA);
		}
		
		if (accessoEnum.equals(AccessoFunzionalitaEnum.MENUFEPA)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.PERMESSO_FATTURA_SOGEI) || PermessiUtils.hasPermesso(permessi, PermessiEnum.PERMESSO_FATTURA_NON_SOGEI) || PermessiUtils.hasPermesso(permessi, PermessiEnum.PERMESSO_DIRIGENTE_FEPA) || PermessiUtils.hasPermesso(permessi, PermessiEnum.PERMESSO_FATTURA_MISTO);
		}
		
		if (accessoEnum.equals(AccessoFunzionalitaEnum.FATTURE)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.PERMESSO_FATTURA_SOGEI);
		}
		
		if (accessoEnum.equals(AccessoFunzionalitaEnum.DECRETI)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.PERMESSO_FATTURA_SOGEI);
		}
		
		if (accessoEnum.equals(AccessoFunzionalitaEnum.DSR)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.PERMESSO_FATTURA_SOGEI);
		}
		
		if (accessoEnum.equals(AccessoFunzionalitaEnum.DECRETI_DIRIGENTE)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.PERMESSO_DIRIGENTE_FEPA);
		}
		
		if (accessoEnum.equals(AccessoFunzionalitaEnum.REPORT)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.REPORT);
		}
		
		if (AccessoFunzionalitaEnum.DECRETI_DECRETI_DIRIGENTE.equals(accessoEnum)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.PERMESSO_DIRIGENTE_FEPA) || PermessiUtils.hasPermesso(permessi, PermessiEnum.PERMESSO_FATTURA_SOGEI);
		}
		
		if (accessoEnum.equals(AccessoFunzionalitaEnum.PROTOCOLLA_DSR)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.DSR);
		}
		
		if (accessoEnum.equals(AccessoFunzionalitaEnum.SCADENZARIO)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.SCADENZARIO_TAB);
		}
		
		if (AccessoFunzionalitaEnum.MULTI_NOTE.equals(accessoEnum)) {
			accede = PermessiUtils.hasPermesso(permessiAOO, PermessiAOOEnum.MULTI_NOTE);
		}
		
		if (AccessoFunzionalitaEnum.CANCELLAZIONE_NOTE.equals(accessoEnum)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.CANCELLAZIONE_NOTE);
		}
		
		if (accessoEnum.equals(AccessoFunzionalitaEnum.MAIL)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.PROTOCOLLO_IN_ENTRATA) && TipologiaNodoEnum.GRUPPO.getId() != idTipologiaNodoUtente;
		}
		
		if (accessoEnum.equals(AccessoFunzionalitaEnum.DOCUMENTI_CARTACEI)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.PROTOCOLLO_IN_ENTRATA) && TipologiaNodoEnum.GRUPPO.getId() != idTipologiaNodoUtente;
		}
		
		if (accessoEnum.equals(AccessoFunzionalitaEnum.DOCUMENTI_CARTACEI_ACQUISITI_ELIMINATI)) {
			accede = PermessiUtils.hasPermesso(permessiAOO, PermessiAOOEnum.SCANSIONE_PRECEDENTE_ACQUISIZIONE);
		}
		
		if (accessoEnum.equals(AccessoFunzionalitaEnum.DASHBOARD)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.DASHBOARD);
		}

		if (accessoEnum.equals(AccessoFunzionalitaEnum.ASSEGNAZIONE_DELEGHE)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.ASSEGNAZIONE_DELEGHE);
		}
		
		if (accessoEnum.equals(AccessoFunzionalitaEnum.RICERCA_REGISTRAZIONI_AUSILIARIE)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.RICERCA_REGISTRAZIONI_AUSILIARIE);
		}
		
		if (accessoEnum.equals(AccessoFunzionalitaEnum.WIZARD_UCB)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.WIZARD_UCB);
		}
		
		if (accessoEnum.equals(AccessoFunzionalitaEnum.RICERCANPSSENZAACL)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.RICERCANPSSENZAACL);
		}
		
		if (accessoEnum.equals(AccessoFunzionalitaEnum.MODIFICA_METADATI_MINIMI)) {
			accede = PermessiUtils.hasPermesso(permessi, PermessiEnum.MODIFICA_METADATI_MINIMI);
		}
		/**Permessi da utilizzare per gridMenu -- END**/
		return accede;
	}
}
