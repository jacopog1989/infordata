package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Facade del servizio di gestione funzionalità di reinvio.
 */
public interface IReinviaFacadeSRV extends Serializable {

	/**
	 * Reinvia il documento.
	 * @param utente
	 * @param wobNumber
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO reinvia(UtenteDTO utente, String wobNumber);
	
	/**
	 * Reinvia i documenti in modo massivo.
	 * @param utente
	 * @param wobNumbers
	 * @return esiti dell'operazione
	 */
	Collection<EsitoOperazioneDTO> reinvia(UtenteDTO utente,  Collection<String> wobNumbers);
	
}