package it.ibm.red.business.dto;

import java.util.List;
import java.util.Map;

/**
 * The Class FaldoneWsDTO.
 *
 * @author m.crescentini
 * 
 * 	Classe utilizzata per modellare un faldone utilizzato nell'interfaccia web service.
 */
public class FaldoneWsDTO extends AbstractDTO {

	private static final long serialVersionUID = -5329528071764691309L;

	/**
	 * Nome faldone.
	 */
	private final String nomeFaldone;
	
	/**
	 * Oggetto.
	 */
	private final String oggetto;
	
	/**
	 * Descrizione.
	 */
	private final String descrizione;
	
	/**
	 * Parent folder.
	 */
	private final String parentFaldone;

	/**
	 * Classe documentale.
	 */
	private final String classeDocumentale;

	/**
	 * Metaati.
	 */
	private transient Map<String, Object> metadati;
	
	/**
	 * Security.
	 */
	private final List<SecurityWsDTO> security;
	

	/**
	 * @param nomeFaldone
	 * @param oggetto
	 * @param descrizione
	 * @param parentFaldone
	 * @param classeDocumentale
	 * @param metadati
	 * @param security
	 */
	public FaldoneWsDTO(final String nomeFaldone, final String oggetto, final String descrizione, final String parentFaldone, final String classeDocumentale,
			final Map<String, Object> metadati, final List<SecurityWsDTO> security) {
		super();
		this.nomeFaldone = nomeFaldone;
		this.oggetto = oggetto;
		this.descrizione = descrizione;
		this.parentFaldone = parentFaldone;
		this.classeDocumentale = classeDocumentale;
		this.metadati = metadati;
		this.security = security;
	}

	/**
	 * @return the nomeFaldone
	 */
	public String getNomeFaldone() {
		return nomeFaldone;
	}

	/**
	 * @return the oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}
	
	/**
	 * @return the descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * @return the parentFaldone
	 */
	public String getParentFaldone() {
		return parentFaldone;
	}

	/**
	 * @return the classeDocumentale
	 */
	public String getClasseDocumentale() {
		return classeDocumentale;
	}
	
	/**
	 * @return the metadati
	 */
	public Map<String, Object> getMetadati() {
		return metadati;
	}

	/**
	 * @return the security
	 */
	public List<SecurityWsDTO> getSecurity() {
		return security;
	}
	
}
