package it.ibm.red.business.service.ws.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.core.Document;

import it.ibm.red.business.dto.DocumentoWsDTO;
import it.ibm.red.business.dto.FaldoneWsDTO;
import it.ibm.red.business.dto.FascicoloWsDTO;
import it.ibm.red.business.dto.RisultatoRicercaWsDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IRicercaSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.service.concrete.AbstractService;
import it.ibm.red.business.service.ws.IDocumentoWsSRV;
import it.ibm.red.business.service.ws.IFaldoneWsSRV;
import it.ibm.red.business.service.ws.IFascicoloWsSRV;
import it.ibm.red.business.service.ws.IRicercaWsSRV;
import it.ibm.red.business.service.ws.auth.DocumentServiceAuthorizable;
import it.ibm.red.webservice.model.documentservice.dto.Servizio;
import it.ibm.red.webservice.model.documentservice.types.messages.RedRicercaAvanzataType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedRicercaDocumentiType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedRicercaFaldoniType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedRicercaFascicoliType;

/**
 * @author m.crescentini
 * 
 *         Servizio per la ricerca di documenti tramite web service.
 *
 */
@Service
public class RicercaWsSRV extends AbstractService implements IRicercaWsSRV {

	private static final long serialVersionUID = -3933447329746707313L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaWsSRV.class.getName());

	/**
	 * Servizio.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IRicercaSRV ricercaSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IDocumentoWsSRV documentoWsSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IFascicoloWsSRV fascicoloWsSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IFaldoneWsSRV faldoneWsSRV;

	/*
	 * (non-Javadoc)
	 * 
	 * @see it.ibm.red.business.service.ws.facade.IRicercaDocumentiWsFacadeSRV#
	 * ricercaDocumentiRed
	 * (it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedRicercaDocumentiType)
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_RICERCA_DOCUMENTI)
	public final RisultatoRicercaWsDTO redRicercaDocumenti(final RedWsClient client, final RedRicercaDocumentiType requestRicercaDoc) {
		RisultatoRicercaWsDTO risultatoRicerca = null;
		Connection con = null;
		IFilenetCEHelper fceh = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final UtenteDTO utente = utenteSRV.getById(requestRicercaDoc.getIdUtente().longValue(), con);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			// Si effettua la ricerca
			final DocumentSet documentiRicercaFilenet = ricercaSRV.ricercaDocumentiDaWs(requestRicercaDoc.getValueString(), requestRicercaDoc.getAnnoRicerca(),
					requestRicercaDoc.getCampoRicerca().name(), requestRicercaDoc.isWithContent(), utente, fceh);

			if (documentiRicercaFilenet != null && !documentiRicercaFilenet.isEmpty()) {
				// Si trasformano i documenti ottenuti
				final List<DocumentoWsDTO> risultatoRicercaDocumenti = documentoWsSRV.transformToDocumentiWsPerRicercaSemplice(documentiRicercaFilenet,
						Boolean.TRUE.equals(requestRicercaDoc.isWithContent()), utente.getIdAoo(), con);

				if (!CollectionUtils.isEmpty(risultatoRicercaDocumenti)) {
					risultatoRicerca = new RisultatoRicercaWsDTO();
					risultatoRicerca.setDocumenti(risultatoRicercaDocumenti);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}

		return risultatoRicerca;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.ibm.red.business.service.ws.facade.IRicercaWsFacadeSRV#redRicercaFascicoli
	 * (it.ibm.red.business.persistence.model.RedWsClient,
	 * it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedRicercaFascicoliType)
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_RICERCA_FASCICOLI)
	public final RisultatoRicercaWsDTO redRicercaFascicoli(final RedWsClient client, final RedRicercaFascicoliType requestRicercaFascicoli) {
		RisultatoRicercaWsDTO risultatoRicerca = null;
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final UtenteDTO utente = utenteSRV.getById(requestRicercaFascicoli.getIdUtente().longValue(), con);

			// Si effettua la ricerca
			final DocumentSet fascicoliRicercaFilenet = ricercaSRV.ricercaFascicoliDaWs(requestRicercaFascicoli.getValueString(), utente);

			if (fascicoliRicercaFilenet != null && !fascicoliRicercaFilenet.isEmpty()) {
				// Si trasformano i fascicoli ottenuti
				final List<FascicoloWsDTO> risultatoRicercaFascicoli = fascicoloWsSRV.transformToFascicoliWs(fascicoliRicercaFilenet, con);

				if (!CollectionUtils.isEmpty(risultatoRicercaFascicoli)) {
					risultatoRicerca = new RisultatoRicercaWsDTO();
					risultatoRicerca.setFascicoli(risultatoRicercaFascicoli);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

		return risultatoRicerca;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.ibm.red.business.service.ws.facade.IRicercaWsFacadeSRV#redRicercaFaldoni
	 * (it.ibm.red.business.persistence.model.RedWsClient,
	 * it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedRicercaFaldoniType)
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_RICERCA_FALDONI)
	public final RisultatoRicercaWsDTO redRicercaFaldoni(final RedWsClient client, final RedRicercaFaldoniType requestRicercaFaldoni) {
		RisultatoRicercaWsDTO risultatoRicerca = null;
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final UtenteDTO utente = utenteSRV.getById(requestRicercaFaldoni.getIdUtente().longValue(), con);

			// Si effettua la ricerca
			final DocumentSet faldoniRicercaFilenet = ricercaSRV.ricercaFaldoniDaWs(requestRicercaFaldoni.getValueString(), utente);

			if (faldoniRicercaFilenet != null && !faldoniRicercaFilenet.isEmpty()) {
				// Si trasformano i faldoni ottenuti
				final List<FaldoneWsDTO> risultatoRicercaFaldoni = faldoneWsSRV.transformToFaldoniWs(faldoniRicercaFilenet);

				if (!CollectionUtils.isEmpty(risultatoRicercaFaldoni)) {
					risultatoRicerca = new RisultatoRicercaWsDTO();
					risultatoRicerca.setFaldoni(risultatoRicercaFaldoni);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

		return risultatoRicerca;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * it.ibm.red.business.service.ws.facade.IRicercaWsFacadeSRV#redRicercaAvanzata
	 * (it.ibm.red.business.persistence.model.RedWsClient,
	 * it.ibm.red.webservice.model.documentservice.types.messages.
	 * RedRicercaAvanzataType)
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.RED_RICERCA_AVANZATA)
	public final RisultatoRicercaWsDTO redRicercaAvanzata(final RedWsClient client, final RedRicercaAvanzataType requestRicercaAv) {
		RisultatoRicercaWsDTO risultatoRicerca = null;
		IFilenetCEHelper fceh = null;
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			final PropertiesProvider pp = PropertiesProvider.getIstance();

			final UtenteDTO utente = utenteSRV.getById(requestRicercaAv.getIdUtente().longValue(), con);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			String classeDocumentale = requestRicercaAv.getClasseDocumentale();

			switch (requestRicercaAv.getEntitaRicerca()) {
			case FASCICOLO:
				classeDocumentale = pp.getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY);
				break;
			case EMAIL:
				classeDocumentale = pp.getParameterByKey(PropertiesNameEnum.MAIL_CLASSNAME_FN_METAKEY);
				break;
			default:
				break;
			}

			final DocumentSet risultatoRicercaFilenet = ricercaSRV.ricercaAvanzataDaWs(classeDocumentale, requestRicercaAv.getParametriRicerca(),
					requestRicercaAv.getWhereCondition(), requestRicercaAv.getFullTextAnd(), requestRicercaAv.getFullTextOr(), requestRicercaAv.getTipoVersione(), utente);

			if (risultatoRicercaFilenet != null && !risultatoRicercaFilenet.isEmpty()) {
				final List<DocumentoWsDTO> risultatoRicercaDocumenti = new ArrayList<>();
				final List<FascicoloWsDTO> risultatoRicercaFascicoli = new ArrayList<>();

				final Iterator<?> itRisultatoRicercaFilenet = risultatoRicercaFilenet.iterator();
				while (itRisultatoRicercaFilenet.hasNext()) {
					// Il DocumentSet "risultatoRicercaFilenet" recupera i soli ID dei documenti,
					// ora è necessario recuperare il documento per intero
					final Document documentoFilenet = fceh.getDocument(((Document) itRisultatoRicercaFilenet.next()).get_Id());

					// Estrazione dei soli metadati eventualmente richiesti
					Set<String> metadatiDaRecuperare = null;

					final List<String> metadatiRichiesti = requestRicercaAv.getMetadati();
					if (!CollectionUtils.isEmpty(metadatiRichiesti)) {
						metadatiDaRecuperare = new HashSet<>();
						metadatiRichiesti.stream().forEach(metadatiDaRecuperare::add);
					}

					switch (requestRicercaAv.getEntitaRicerca()) {
					case DOCUMENTO:
						// Documenti senza content e con metadati
						risultatoRicercaDocumenti
								.add(documentoWsSRV.transformToDocumentoWs(documentoFilenet, false, true, metadatiDaRecuperare, utente.getIdAoo(), fceh, con));
						break;
					case EMAIL:
						// Email con content e metadati
						risultatoRicercaDocumenti
								.add(documentoWsSRV.transformToDocumentoEmailWs(documentoFilenet, true, true, metadatiDaRecuperare, utente.getIdAoo(), fceh, con));
						break;
					case FASCICOLO:
						// Fascicoli con metadati
						risultatoRicercaFascicoli.add(fascicoloWsSRV.transformToFascicoloWs(documentoFilenet, true, metadatiDaRecuperare, true, fceh, con));
						break;
					default:
						throw new RedException("Entità di ricerca non gestita: " + requestRicercaAv.getEntitaRicerca());
					}

					risultatoRicerca = new RisultatoRicercaWsDTO(risultatoRicercaDocumenti, risultatoRicercaFascicoli);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}

		return risultatoRicerca;
	}
}