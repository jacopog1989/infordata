package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import it.ibm.red.business.dto.KeyValueDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.SelectItemDTO;
import it.ibm.red.business.dto.TipoProcedimentoDTO;
import it.ibm.red.business.dto.TipologiaDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.persistence.model.LookupTable;

/**
 * @author m.crescentini
 *
 */
public interface ITipologiaDocumentoFacadeSRV extends Serializable {

	/**
	 * Restituisce le tipologia documento attive della categoria identificata
	 * dall'Enum @see TipoCategoriaEnum: <code> tipoCategoria </code> valide per
	 * l'AOO identificata dall': <code> idAoo </code>.
	 * NB: Una tipologia documento risulta attiva quando non è stata superata la sua data di disabilitazione.
	 * 
	 * @see it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV#
	 *      getComboTipologieByAooAndTipoCategoriaAttivi(it.ibm.red.business.enums.
	 *      TipoCategoriaEnum, java.lang.Long).
	 * @param tipoCategoria
	 *            tipo categoria del documento
	 * @param idAoo
	 *            identificativo dell'Area organizzativa
	 * @return lista delle tipologia documento
	 */
	List<TipologiaDocumentoDTO> getComboTipologieByAooAndTipoCategoriaAttivi(TipoCategoriaEnum tipoCategoria,
			Long idAoo);

	/**
	 * Permette di recuperare le tipologie documento attive e non applicati i filtri
	 * di ricerca.
	 * 
	 * @param tipoCategoria
	 *            tipoCategoria.
	 * @param utente
	 *            utente con i permessi per le tipologie documentali.
	 * @return Una lista di Tipoologie documentali.
	 */
	List<TipologiaDocumentoDTO> getTipologieDocumentoByCategoriaDocumento(TipoCategoriaEnum tce, UtenteDTO utente);

	/**
	 * Metodo che permette di recuperare una tipologia di documento apartire da
	 * un'id.
	 * 
	 * @param idTipologiaDocumento
	 *            - id che rappresenta una tioplogia documento sul database.
	 * @return tipologiaDocumento - istanza che rappresenta una tipologia documento.
	 */
	TipologiaDocumentoDTO getById(Integer idTipologiaDocumento);

	/**
	 * Il SRV deve provvedere alla creazione dei metadati relativi ad una nuova
	 * tipologia da aggiungere e all'aggiunta della tipolgia stessa nella tabella TIPOLOGIADOCUMENTO
	 * 
	 * @param nomeTipoDocumento
	 * @param tipoGestione
	 * @param idAoo
	 * @param idTipoCategoria
	 * @param flagControlloPreventivo
	 * @param metadati
	 * @param registri
	 */
	void save(String nomeTipoDocumento, String tipoGestione, long idAoo, TipoCategoriaEnum tipoCategoria, Collection<TipoProcedimentoDTO> procedimenti);

	/**
	 * Restituisce i metadati validi per una coppia tipologia documento / tipologia
	 * procedimento. I metadati sono diversi in base alla date target.
	 * 
	 * @param idTipologiaDocumento
	 *            identificativo tipologia documento
	 * @param idTipoProcedimento
	 *            identificativo tipologia procedimento
	 * @param dateTarget
	 *            in creazione la date target dovrà essere concorrente alla
	 *            creazione, in fase di modifica occorre far riferimento alla data
	 *            di creazione del documento, mentre in fase di ricerca la date
	 *            target deve essere <code> null </code>.
	 * @param idAoo
	 *            identificativo dell'Area Organizzativa
	 * @return lista dei metadati recuperati
	 */
	List<MetadatoDTO> caricaMetadati(int idTipologiaDocumento, int idTipoProcedimento, Date dateTarget, long idAoo);
	
	/**
	 * Carica tutti i metadati per la visualizzazione in maschera, e.g. gestendo il flagRange.
	 * 
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @param forCreazione	indica con quale logica occorre recuperare i metadati (esiste una logica differente in base alla modifica e alla creazione)
	 * 						deve essere true solo se occorrono i metadati per la creazione di un documento
	 * 
	 * @param idAoo
	 * @param dateTarget 	può essere null solo in fase di ricerca perchÃ© non occorre ridurre lo scope dei metadati
	 * @return
	 */
	List<MetadatoDTO> caricaMetadatiEstesiPerGUI(int idTipologiaDocumento, int idTipoProcedimento, boolean forCreazione, long idAoo, Date dateTarget);
	
	/**
	 * @param idAoo
	 * @param codiceAoo
	 * @param tipoCategoriaDocumento
	 * @return
	 */
	Integer getTipologiaDocumentalePredefinita(Long idAoo, String codiceAoo, TipoCategoriaEnum tipoCategoriaDocumento);

	/**
	 * Restituisce l'id della tipologia documento associata al documento con
	 * descrizione: <code> descrizioneTipologiaDocumento </code> e il cui tipo
	 * categoria è uguale a <code> tce </code> @see TipoCategoriaEnum.
	 * 
	 * @see it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV#
	 *      getTipologiaDocumentoByNomeAndCategoriaDocumento( java.lang.String,
	 *      it.ibm.red.business.enums.TipoCategoriaEnum, java.lang.Long).
	 * @param descrizioneTipologiaDocumento
	 *            descrizione della tipologia documento
	 * @param tce
	 *            tipo categoria del documento
	 * @param idAoo
	 *            identificativo dell'Area Organizzativa
	 * @return identificativo tipologia documento
	 */
	Integer getIdTipologiaDocumentoByDescrizioneAndCategoria(String descrizioneTipologiaDocumento, TipoCategoriaEnum tce, Long idAoo);

	/**
	 * Carica tutti i valori di uno specifico selettore identificato dal nome:
	 * <code> selector </code>.
	 * 
	 * @param selector
	 *            Nome del selettore per il quale devono esserne recuperati i values
	 * @param dateTarget
	 *            in creazione la date target dovrà essere concorrente alla
	 *            creazione, in fase di modifica occorre far riferimento alla data
	 *            di creazione del documento, mentre in fase di ricerca la date
	 *            target deve essere <code> null </code>.
	 * @param forCreazione
	 *            flag che specifica fase di creazione
	 * @param idAoo
	 *            Se null vuol dire che il selettore non ha valenza per un solo AOO,
	 *            altrimenti indica l'identificativo dell'Area organizzativa
	 * @return lista dei values come @see SelectItemDTO
	 */
	List<SelectItemDTO> getValuesBySelector(String selector, Date dateTarget, Long idAoo);
	
	/**
	 * Salva le lookup table contenute nella lista in ingresso. Vengono utilizzati i model delle lookup table in quanto essi fanno riferimento 
	 * alla tabella LOOKUPTABLE
	 * 
	 * @param lookups la lista dei model LookupTable da salvare
	 * */
	void saveLookupTables(List<LookupTable> lookups);

	/**
	 * @param codiceAOO
	 * @return
	 */
	Integer getIdAOOByCodiceNPS(String codiceAOO);
	
	/**
	 * @param isIngresso
	 * @param codiceAOO
	 * @param nomeTipologia
	 * @return
	 */
	TipologiaDTO getTipologiaFromTipologiaNPS(Boolean isIngresso, Integer idAOO, String tipologiaNPS);

	/**
	 * Metodo che consente di recuperare i metadati estesi popolati di un documento dato il document title e l'utente
	 * @param documentTitle
	 * @param utente
	 * @return
	 */
	Collection<MetadatoDTO> recuperaMetadatiEstesi(String documentTitle, UtenteDTO utente);

	/**
	 * Metodo che consente di recuperare i metadati estesi popolati di un documento dato il document title, IFilenetHelper e idAoo dell'utente
	 * 
	 * */
	Collection<MetadatoDTO> recuperaMetadatiEstesi(String documentTitle, IFilenetCEHelper fceh, Long idAoo);
	
	/**
	 * @param idTipologiaDocumento
	 * @return
	 */
	String getDescTipologiaDocumentoById(Integer idTipologiaDocumento);

	/**
	 * Recupera l'elenco delle tipologie documentali note ad NPS.
	 * 
	 * @param utente
	 * @return
	 */
	List<KeyValueDTO> getTipologieDocumentoNPS(UtenteDTO utente);
	
}