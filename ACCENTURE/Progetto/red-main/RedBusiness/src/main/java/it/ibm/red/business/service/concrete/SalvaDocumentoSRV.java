package it.ibm.red.business.service.concrete;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.collection.AccessPermissionList;
import com.filenet.api.collection.DocumentSet;
import com.filenet.api.collection.IndependentObjectSet;
import com.filenet.api.constants.Cardinality;
import com.filenet.api.constants.PropertyNames;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.constants.TypeID;
import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.CustomObject;
import com.filenet.api.core.Document;
import com.filenet.api.property.FilterElement;
import com.filenet.api.property.PropertyFilter;
import com.google.common.net.MediaType;

import filenet.vw.api.VWAttachment;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.AzioneMail;
import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.constants.Constants.Modalita;
import it.ibm.red.business.constants.Constants.Protocollo;
import it.ibm.red.business.constants.Constants.TipoFascicolo;
import it.ibm.red.business.constants.Constants.TipoSpedizione;
import it.ibm.red.business.constants.Constants.TipologiaInvio;
import it.ibm.red.business.constants.Constants.TipologiaRiferimento;
import it.ibm.red.business.constants.Constants.Varie;
import it.ibm.red.business.dao.IAllaccioDAO;
import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.IApprovazioneDAO;
import it.ibm.red.business.dao.IDatiPredefinitiMozioneDAO;
import it.ibm.red.business.dao.IDocumentoDAO;
import it.ibm.red.business.dao.IFascicoloDAO;
import it.ibm.red.business.dao.IIterApprovativoDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.INpsConfigurationDAO;
import it.ibm.red.business.dao.IParametriProtocolloDAO;
import it.ibm.red.business.dao.IRiferimentoDAO;
import it.ibm.red.business.dao.IStampigliaturaSegnoGraficoDAO;
import it.ibm.red.business.dao.ITipoFileDAO;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dao.ITipologiaDocumentoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AnagraficaDipendentiComponentDTO;
import it.ibm.red.business.dto.ApprovazioneDTO;
import it.ibm.red.business.dto.AssegnatarioDTO;
import it.ibm.red.business.dto.AssegnatarioInteropDTO;
import it.ibm.red.business.dto.AssegnazioneAutomaticaCapSpesaDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.AssegnazioneWorkflowDTO;
import it.ibm.red.business.dto.CapitoloSpesaMetadatoDTO;
import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.CodaTrasformazioneParametriDTO;
import it.ibm.red.business.dto.ContributoDTO;
import it.ibm.red.business.dto.DelegatoDTO;
import it.ibm.red.business.dto.DestinatarioCodaMailDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.LookupTableDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.MetadatoDinamicoDTO;
import it.ibm.red.business.dto.NodoDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.NotaDTO;
import it.ibm.red.business.dto.NotificaRitiroAutotutelaDTO;
import it.ibm.red.business.dto.ProtocolloDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.RaccoltaFadDTO;
import it.ibm.red.business.dto.RiferimentoStoricoNsdDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedParametriDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedStatusDTO;
import it.ibm.red.business.dto.SequKDTO;
import it.ibm.red.business.dto.TipoProcedimentoDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.VerifyInfoDTO;
import it.ibm.red.business.dto.filenet.AnnotationDTO;
import it.ibm.red.business.dto.filenet.DocumentoRedFnDTO;
import it.ibm.red.business.dto.filenet.FascicoloRedFnDTO;
import it.ibm.red.business.enums.AccessoFunzionalitaEnum;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.enums.FilenetAnnotationEnum;
import it.ibm.red.business.enums.FilenetStatoFascicoloEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.IterApprovativoParzialeEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.PermessiAOOEnum;
import it.ibm.red.business.enums.PostaEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ProvenienzaSalvaDocumentoEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.SalvaDocumentoErroreEnum;
import it.ibm.red.business.enums.StatoLavorazioneEnum;
import it.ibm.red.business.enums.StatoMailEnum;
import it.ibm.red.business.enums.TipoAllaccioEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoContestoProceduraleEnum;
import it.ibm.red.business.enums.TipoMetadatoEnum;
import it.ibm.red.business.enums.TipoProtocolloEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.enums.TipologiaIndirizzoEmailEnum;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.enums.ValueMetadatoVerificaFirmaEnum;
import it.ibm.red.business.exception.FilenetException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.adobe.AdobeLCHelper;
import it.ibm.red.business.helper.adobe.IAdobeLCHelper;
import it.ibm.red.business.helper.adobe.dto.CountSignsOutputDTO;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.helper.html.HtmlFileHelper;
import it.ibm.red.business.helper.metadatiestesi.MetadatiEstesiHelper;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.helper.signing.SignHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.nps.builder.Builder;
import it.ibm.red.business.nps.dto.DocumentoNpsDTO;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.DatiPredefinitiMozione;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.NodoUtenteCoordinatore;
import it.ibm.red.business.persistence.model.NpsConfiguration;
import it.ibm.red.business.persistence.model.PkHandler;
import it.ibm.red.business.persistence.model.TipoProcedimento;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAllegatoSRV;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.IAssegnazioneAutomaticaCapSpesaSRV;
import it.ibm.red.business.service.IAssegnazioneAutomaticaSRV;
import it.ibm.red.business.service.ICasellePostaliSRV;
import it.ibm.red.business.service.ICodaMailSRV;
import it.ibm.red.business.service.IEventoLogSRV;
import it.ibm.red.business.service.IFaldoneSRV;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.IFepaSRV;
import it.ibm.red.business.service.IMailSRV;
import it.ibm.red.business.service.INotaSRV;
import it.ibm.red.business.service.INotificaRitiroAutotutelaSRV;
import it.ibm.red.business.service.INpsSRV;
import it.ibm.red.business.service.IPMefSRV;
import it.ibm.red.business.service.IProtocolliEmergenzaSRV;
import it.ibm.red.business.service.IRegistrazioniAusiliarieSRV;
import it.ibm.red.business.service.IRegistroRepertorioSRV;
import it.ibm.red.business.service.IRubricaSRV;
import it.ibm.red.business.service.ISalvaDocumentoSRV;
import it.ibm.red.business.service.ISecuritySRV;
import it.ibm.red.business.service.ITemplateDocUscitaSRV;
import it.ibm.red.business.service.ITipoProcedimentoSRV;
import it.ibm.red.business.service.ITipologiaDocumentoSRV;
import it.ibm.red.business.service.ITracciaDocumentiSRV;
import it.ibm.red.business.service.ITrasformazionePDFSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.service.IWorkflowSRV;
import it.ibm.red.business.service.facade.IAttoDecretoUCBFacadeSRV;
import it.ibm.red.business.service.facade.IOrganigrammaFacadeSRV;
import it.ibm.red.business.service.facade.ISignFacadeSRV;
import it.ibm.red.business.template.TemplateDocumento;
import it.ibm.red.business.template.TemplateDocumentoEntrata;
import it.ibm.red.business.template.TemplateDocumentoUscita;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.DestinatariRedUtils;
import it.ibm.red.business.utils.DocumentoUtils;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.PermessiUtils;
import it.ibm.red.business.validator.AllegatoValidator;
import it.ibm.red.business.validator.DataScadenzaDocumentoValidator;
import it.ibm.red.business.validator.DestinatarioRedValidator;

/**
 * Servizio per la gestione del salvataggio di un documento
 * (ingresso/uscita/protocollazione mail/etc.).
 *
 * @author m.crescentini
 */
@Service
public class SalvaDocumentoSRV extends AbstractService implements ISalvaDocumentoSRV {

	/**
	 * Messaggio errore protocollazione.
	 */
	private static final String GESTISCI_PROTOCOLLAZIONE_ERRORE_NELL_INSERIMENTO_DELLA_RICHIESTA_ASINCRONA_VERSO_NPS = "gestisciProtocollazione -> Attenzione! Errore nell'inserimento della richiesta asincrona verso NPS";

	/**
	 * Messaggio errore salvataggio contatto su Filenet.
	 */
	private static final String ERROR_SALVATAGGIO_CONTATTO_MSG = "Non è stato possibile salvare il contatto su FileNet";

	/**
	 * Messaggio.
	 */
	private static final String INSERITO_E_GIA_PRESENTE_NEL_SISTEMA = " inserito è già presente nel sistema.";

	/**
	 * Messaggio.
	 */
	private static final String SI_E_VERIFICATO_UN_ERRORE_DURANTE_LA_CREAZIONE_DEGLI_ALLEGATI_DEL_DOCUMENTO = "Si è verificato un errore durante la creazione "
			+ "degli allegati del documento.";

	/**
	 * Messaggio.
	 */
	private static final String FRECCIA = " ===> ";

	/**
	 * Messaggio.
	 */
	private static final String AGGIORNA_DOCUMENTO_SI_CHIAMA_IL_METODO_PER_L_AGGIORNAMENTO_DEI_DATI_AGGIORNAMENTO_METADATI_CE_PE_TRASFORMAZIONE_PDF_ETC = "aggiornaDocumento -> Si chiama il metodo per l'aggiornamento dei dati (aggiornamento metadati CE/PE, trasformazione PDF, etc.)";

	/**
	 * Messaggio.
	 */
	private static final String PER_IL_DOCUMENTO = " per il documento: ";

	/**
	 * Messaggio.
	 */
	private static final String DEL_DOCUMENTO = " del documento";

	/**
	 * Messaggio.
	 */
	private static final String AL_DOCUMENTO = " al documento: ";

	/**
	 * Lunghezza massima mittente.
	 */
	private static final int MAX_LENGTH_MITTENTE_METAKEY = 1333;

	private static final long serialVersionUID = -8865512104011215519L;

	/**
	 * Contatto OTF.
	 */
	private static final int CONTATTO_ON_THE_FLY = 1;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SalvaDocumentoSRV.class.getName());

	/**
	 * PRoperties.
	 */
	private PropertiesProvider pp;

	/**
	 * DAO.
	 */
	@Autowired
	private IFascicoloDAO fascicoloDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private ITipologiaDocumentoDAO tipologiaDocumentoDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IAllaccioDAO allaccioDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IDocumentoDAO documentoDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private INpsConfigurationDAO npsConfigurationDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IParametriProtocolloDAO parametriProtocolloDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IAooDAO aooDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IIterApprovativoDAO iterApprovativoDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private ITipoProcedimentoDAO tipoProcedimentoDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IRiferimentoDAO riferimentoDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IApprovazioneDAO approvazioneDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private ITipoFileDAO tipoFileDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IDatiPredefinitiMozioneDAO datiPredefinitiMozioneDAO;

	/**
	 * Service.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ISecuritySRV securitySRV;

	/**
	 * Service.
	 */
	@Autowired
	private IAllegatoSRV allegatoSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IFascicoloSRV fascicoloSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IEventoLogSRV eventoLogSRV;

	/**
	 * Service.
	 */
	@Autowired
	private INpsSRV npsSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IPMefSRV pMefSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IFaldoneSRV faldoneSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ITipoProcedimentoSRV tipoProcedimentoSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ITrasformazionePDFSRV trasformazionePDFSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ICodaMailSRV codaMailSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ITracciaDocumentiSRV tracciaDocumentiSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ITemplateDocUscitaSRV templateDocUscitaSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IFepaSRV fepaSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IWorkflowSRV workflowSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IMailSRV mailSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ICasellePostaliSRV casellePostaliSRV;

	/**
	 * Service.
	 */
	@Autowired
	private INotaSRV notaSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IProtocolliEmergenzaSRV protocolliEmergenzaSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IRegistroRepertorioSRV registroRepertorioSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IAssegnazioneAutomaticaCapSpesaSRV assegnazioneAutomaticaCapSpesaSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IAssegnazioneAutomaticaSRV assegnazioneAutomaticaSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IAooSRV aooSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ISignFacadeSRV signSRV;

	/**
	 * Service.
	 */
	@Autowired
	private DocumentManagerSRV documentManagerSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private IStampigliaturaSegnoGraficoDAO stampigliaturaSiglaDAO;

	/**
	 * Service.
	 */
	@Autowired
	private IRegistrazioniAusiliarieSRV templateRegistrazioneSRV;

	/**
	 * Service.
	 */
	@Autowired
	private INotificaRitiroAutotutelaSRV notificaRitiroAutotutelaSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IOrganigrammaFacadeSRV orgSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IAttoDecretoUCBFacadeSRV attoDecretoUCBSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ITipologiaDocumentoSRV tipologiaDocumentoSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IRubricaSRV rubricaSRV;

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	private void initService(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final SalvaDocumentoRedParametriDTO parametri,
			final UtenteDTO utente, final Connection con) {
		// ### INIT attributi generici -> START
		status.setHtmlFileHelper(new HtmlFileHelper());
		status.setIdAllegatiDaFirmare(new ArrayList<>());
		status.setIdAllegatiConFirmaVisibile(new ArrayList<>());
		status.setSecurityDoc(new ListSecurityDTO());
		status.setSecurityFascicolo(new ListSecurityDTO());
		status.setSecurityDocAllacciati(new ListSecurityDTO());
		status.setSecurityFascicoloDocAllacciati(new ListSecurityDTO());
		status.setAssegnazioniFascicolo(new ArrayList<>());
		status.setWorkflowList(new ArrayList<>());
		status.setAssegnazioniCompetenza(new String[0]);
		status.setAssegnazioniConoscenza(new String[0]);
		status.setAssegnazioniContributo(new String[0]);
		status.setAssegnazioniFirma(new String[0]);
		status.setAssegnazioniFirmaMultipla(new String[0]);
		status.setAssegnazioniSigla(new String[0]);
		status.setAssegnazioniVisto(new String[0]);
		status.setDocumentoInserito(false);
		status.setAggiornaOggettoProtocolloNPS(false);
		status.setAggiornaTipologiaDocumentoNPS(false);
		status.setFascicoloAutomatico(false);
		// ### INIT attributi generici -> END

		// ### INIT modalità  -> START
		final String modalita = parametri.getModalita();
		status.setModalitaInsert(Modalita.MODALITA_INS.equals(modalita));
		status.setModalitaUpdate(Modalita.MODALITA_UPD.equals(modalita));
		status.setModalitaUpdateIbrida(Modalita.MODALITA_UPD_IBRIDA.equals(modalita));
		status.setModalitaMailEntrata(Modalita.MODALITA_INS_MAIL.equals(modalita));
		status.setModalitaPostCens(Modalita.MODALITA_INS_POSTCENS.equals(modalita)); // Modalità  post scansione
		// ### INIT modalità  -> END

		// ### INIT proprietà  documento -> START
		status.setDocEntrata(docInput.getIdCategoriaDocumento().equals(CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0]));
		status.setDocInterno(docInput.getIdCategoriaDocumento().equals(CategoriaDocumentoEnum.INTERNO.getIds()[0]));
		status.setDocContributoEsterno(docInput.getIdCategoriaDocumento().equals(CategoriaDocumentoEnum.CONTRIBUTO_ESTERNO.getIds()[0]));
		status.setDocAssegnazioneInterna(docInput.getIdCategoriaDocumento().equals(CategoriaDocumentoEnum.ASSEGNAZIONE_INTERNA.getIds()[0]));
		status.setDocUscita(docInput.getIdCategoriaDocumento().equals(CategoriaDocumentoEnum.DOCUMENTO_USCITA.getIds()[0]));
		status.setMozione(docInput.getIdCategoriaDocumento().equals(CategoriaDocumentoEnum.MOZIONE.getIds()[0]));
		status.setBozza(isBozza(docInput));
		status.setPrecensito(FormatoDocumentoEnum.PRECENSITO.equals(docInput.getFormatoDocumentoEnum()));
		status.setIterManuale(Boolean.TRUE.equals(docInput.getIsIterManuale()));
		status.setIterApprovativo(docInput.getIterApprovativo());
		status.setDaProtocollare(Boolean.TRUE.equals(docInput.getDaProtocollare()));
		status.setInviaNotificaProtocollazioneMail(parametri.isInviaNotificaProtocollazioneMail());
		status.setContentScannerizzato(parametri.isContentScannerizzato());
		status.setContentVariato(parametri.isContentVariato());
		// ### INIT proprietà documento -> END

		// ### INIT altri attributi da parametri -> START

		status.setNomeFaldoniSelezionati(parametri.getNomeFaldoniSelezionati());
		status.setIdFascicoloSelezionato(parametri.getIdFascicoloSelezionato());
		status.setMetadatiFascicolo(parametri.getMetadatiFascicolo());
		status.setIndiceClassificazioneFascicolo(parametri.getIndiceClassificazioneFascicolo());
		status.setInputMailGuid(parametri.getInputMailGuid());
		status.setForzaProtocollazioneMail(parametri.isForzaProtocollazioneMail());
		status.setForzaRispostaPubblica(parametri.isForzaRispostaPubblica());
		status.setForzaAttoDecretoManuale(parametri.isForzaAttoDecretoManuale());
		status.setForzaAssSpedizioneDestElettronici(parametri.isForzaAssSpedizioneDestElettronici());
		status.setIdUtenteMittenteWorkflow(parametri.getIdUtenteMittenteWorkflow());
		status.setIdUfficioMittenteWorkflow(parametri.getIdUfficioMittenteWorkflow());
		status.setIdUtenteDestinatarioWorkflow(parametri.getIdUtenteDestinatarioWorkflow());
		status.setIdUfficioDestinatarioWorkflow(parametri.getIdUfficioDestinatarioWorkflow());
		status.setIsAssegnazioneInternaWorkflow(parametri.getIsAssegnazioneInternaWorkflow());
		status.setIdDocumentoOldWorkflow(parametri.getIdDocumentoOldWorkflow());
		status.setMotivazioneAssegnazioneWorkflow(parametri.getMotivazioneAssegnazioneWorkflow());
		status.setProtocollazioneAutomatica(parametri.isProtocollazioneAutomatica());
		status.setVelinaAsDocPrincipale(parametri.isVelinaAsDocPrincipale());
		status.setContentFromNPS(parametri.isContentFromNPS());
		status.setGuidLastVersionNPSDocPrincipale(parametri.getGuidLastVersionNPSDocPrincipale());
		// ### INIT altri attributi da parametri -> END

		// ### INIT assegnazioni
		caricaAssegnazioni(status, docInput);

		if (!status.isDocEntrata() && !status.isDocAssegnazioneInterna()) {
			// ### INIT stringhe destinatari
			caricaDestinatariStrings(status, docInput.getDestinatari(), utente.getIdAoo());
			// ### INIT iterPdf
			if (hasDocIterAutomatico(status)) {
				status.setIterPdf(status.getIterApprovativo());
			} else if (status.getAssegnazioniFirma().length > 0) {
				final Long idNodoSceltoInt = Long.parseLong(status.getAssegnazioniFirma()[0].split(",")[0]);
				final Long idUtenteSceltoInt = Long.parseLong(status.getAssegnazioniFirma()[0].split(",")[1]);

				final String ragioniere = iterApprovativoDAO.getFirmatario(idNodoSceltoInt, IterApprovativoDTO.LIVELLO_RAGIONIERE, con);
				final Long idUtenteRagInt = Long.parseLong(ragioniere.split(",")[1]);

				final String ispettore = iterApprovativoDAO.getFirmatario(idNodoSceltoInt, IterApprovativoDTO.LIVELLO_ISPETTORE, con);
				final Long idUtenteIspettInt = Long.parseLong(ispettore.split(",")[1]);

				final List<IterApprovativoDTO> iterApprovativi = iterApprovativoDAO.getAllByIdAOO(utente.getIdAoo().intValue(), con);
				for (final IterApprovativoDTO iter : iterApprovativi) {
					if ((idUtenteSceltoInt.equals(idUtenteIspettInt) && iter.isIterAutomaticoFirmaDirettoriIspettori())
							|| (idUtenteSceltoInt.equals(idUtenteRagInt) && iter.isIterAutomaticoFirmaRagioniere())) {
						status.setIterPdf(iter);
						break;
					}
				}
			}
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISalvaDocumentoFacadeSRV#salvaDocumento(it.ibm.red.business.dto.DetailDocumentRedDTO,
	 *      it.ibm.red.business.dto.SalvaDocumentoRedParametriDTO,
	 *      it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.enums.ProvenienzaSalvaDocumentoEnum).
	 */
	@Override
	public EsitoSalvaDocumentoDTO salvaDocumento(final DetailDocumentRedDTO detailDocInput, final SalvaDocumentoRedParametriDTO parametri, final UtenteDTO utente,
			final ProvenienzaSalvaDocumentoEnum provenienza) {
		LOGGER.info("salvaDocumento -> START. Utente: " + utente.getUsername() + ". Provenienza: " + provenienza.toString());
		Connection con = null;
		Connection dwhCon = null;
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;
		SalvaDocumentoRedDTO salvaDocInput = null;
		final SalvaDocumentoRedStatusDTO status = new SalvaDocumentoRedStatusDTO();
		final List<SalvaDocumentoErroreEnum> erroriValidazione = new ArrayList<>();
		final EsitoSalvaDocumentoDTO esito = new EsitoSalvaDocumentoDTO();

		try {
			con = setupConnection(getDataSource().getConnection(), true);
			if (ProvenienzaSalvaDocumentoEnum.GUI_RED.equals(provenienza) || ProvenienzaSalvaDocumentoEnum.INOLTRO_RIFIUTO_AUTOMATICO.equals(provenienza)
					|| ProvenienzaSalvaDocumentoEnum.WEB_SERVICE.equals(provenienza) || ProvenienzaSalvaDocumentoEnum.POSTA_NPS.equals(provenienza)) {
				dwhCon = setupConnection(getFilenetDataSource().getConnection(), true);
			}
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			status.setTitolarioChanged(parametri.isTitolarioChanged());
			// ### CONVERSIONE NEL DTO DEL SERVICE
			salvaDocInput = trasformaInSalvaDocumento(status, detailDocInput, parametri.getModalita(), utente, fceh, con);

			// ### INIZIALIZZAZIONE DEL SERVICE
			initService(status, salvaDocInput, parametri, utente, con);

			// impostazione della velina come documento principale
			if (parametri.isVelinaAsDocPrincipale()) {
				final SalvaDocumentoRedDTO documentoClone = (SalvaDocumentoRedDTO) DocumentoUtils.clonaObject(salvaDocInput);
				// Si crea il PDF della velina a partire dal documento in input
				final InputStream pdfInputStream = creaPdfPreview(status, documentoClone);
				salvaDocInput.setContent(IOUtils.toByteArray(pdfInputStream));
				salvaDocInput.setContentType(Constants.ContentType.PDF);
				String nomeFile = "documento_" + DateUtils.dateToString(new Date(), "yyyyMMddHHmmss") + ".pdf";
				if (StringUtils.isNotBlank(detailDocInput.getIdProtocollo())) {
					nomeFile = "protocollo_" + detailDocInput.getNumeroProtocollo() + "_" + detailDocInput.getAnnoProtocollo() + ".pdf";
				}
				salvaDocInput.setNomeFile(nomeFile);
			}
			
			// Se è una creazione di un documento in ingresso => calcola assegnazione
			// principale
			if (status.isDocEntrata() && !(status.isModalitaUpdate() || status.isModalitaUpdateIbrida())) {
				if (!calcolaAssegnazionePrincipale(esito, status, salvaDocInput, utente, con)) {
					if (!provenienza.isEseguiValidazione()) { // Se non deve essere eseguita la validazione successiva, aggiungi l'errore di
																// validazione
						erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_ING_ASSEGNATARIO_COMPETENZA_NON_PRESENTE);
					}
				} else {
					// Aggiorna detailDocInput se abbiamo utilizzato un assegnazione automatica o indiretta
					detailDocInput.setAssegnazioni(salvaDocInput.getAssegnazioni());
				}
			}

			if (provenienza.isEseguiValidazione()) {
				validaSalvaDocumento(erroriValidazione, status, salvaDocInput, utente, fceh, fpeh, con);
			}

			if (erroriValidazione.isEmpty()) {
				// ### SALVATAGGIO DEL DOCUMENTO
				switch (provenienza) {
				case PROCESSO_AUTOMATICO_INTERNO:
					inserisciDocDaProcessoAutomatico(status, salvaDocInput, utente, fceh, fpeh, con);
					break;
				case GUI_RED_FASCICOLO:
					inserisciDocInternoDaFascicolo(status, salvaDocInput, true, utente, fceh, con);
					break;
				case GUI_RED_FASCICOLO_SENZA_TRASFORMAZIONE:
					inserisciDocInternoDaFascicolo(status, salvaDocInput, false, utente, fceh, con);
					break;
				case FEPA_DSR:
					inserisciOAggiornaDichiarazioneServiziResi(status, salvaDocInput, utente, fceh, fpeh, con);
					break;
				case FEPA_FATTURA_DECRETO:
					inserisciDocFatturaODecreto(status, salvaDocInput, utente, fceh, fpeh, con);
					break;
				case IGEPA:
				case IGEPA_OPF:
					inserisciDocIgepa(status, salvaDocInput, utente, fceh, fpeh, con);
					break;
				case GUI_RED:
				case INOLTRO_RIFIUTO_AUTOMATICO:
				case WEB_SERVICE:
				case POSTA_NPS:
				default:
					inserisciOAggiornaDocumento(status, salvaDocInput, utente, fceh, fpeh, con, dwhCon);
				}

				// ### COMMIT DELLE TRANSAZIONI SUL DB "RED"
				commitConnection(con);

				// ### COMMIT DELLE (EVENTUALI) TRANSAZIONI SUL DB "DWH_RED"
				commitConnection(dwhCon);

				// ### IMPOSTAZIONE DELL'ESITO OK DELL'OPERAZIONE
				esito.setDocumentTitle(salvaDocInput.getDocumentTitle());
				esito.setGuid(salvaDocInput.getGuid());
				esito.setWobNumber(salvaDocInput.getWobNumber());
				esito.setNumeroDocumento(salvaDocInput.getNumeroDocumento());
				esito.setEsitoOk(true);
			} else {
				rollbackConnection(con);
				rollbackConnection(dwhCon);
				esito.getErrori().addAll(erroriValidazione);
			}
		} catch (final Exception e) {
			LOGGER.error("salvaDocumento -> Si è verificato un errore durante il salvataggio del documento.", e);
			// Errore generico, si dettaglia l'errore nelle note
			esito.getErrori().add(SalvaDocumentoErroreEnum.ERRORE_GENERICO);
			String noteEsitoKo = e.getMessage();
			if (e.getCause() != null && StringUtils.isNotBlank(e.getCause().getMessage())) {
				noteEsitoKo += ". CAUSA: " + e.getCause().getMessage();
			}
			if (StringUtils.isBlank(noteEsitoKo)) {
				noteEsitoKo = ExceptionUtils.getStackTrace(e);
			}
			esito.setNote(noteEsitoKo);

			// Si effettua il rollback delle transazioni
			rollbackConnection(con);
			rollbackConnection(dwhCon);
			LOGGER.info("salvaDocumento -> È stato effettuato il rollback delle modifiche eseguite sul DB.");

			// Si cancellano eventuali workflow avviati
			FilenetPEHelper fpehAdmin = null;
			try {
				if (!CollectionUtils.isEmpty(status.getWorkflowList())) {
					fpehAdmin = getFPEHAdmin(utente.getFcDTO());

					for (final String wobNumber : status.getWorkflowList()) {
						// Si procede con la cancellazione del singolo workflow
						fpehAdmin.chiudiWF(wobNumber);
					}
					LOGGER.info("salvaDocumento -> È stata eseguita la cancellazione da FileNet dei workflows avviati durante il salvataggio del documento.");
				}
			} catch (final Exception ex) {
				LOGGER.error("salvaDocumento -> Si è verificato un errore durante la cancellazione da FileNet dei workflow avviati" + " durante il salvataggio del documento.",
						ex);
			} finally {
				logoff(fpehAdmin);
			}
		} finally {
			closeConnection(con);
			closeConnection(dwhCon);
			logoff(fpeh);
			popSubject(fceh);
		}

		LOGGER.info("salvaDocumento -> END. ESITO SALVATAGGIO: " + esito.isEsitoOk() + " PER IL DOCUMENTO: " + salvaDocInput.getDocumentTitle());
		return esito;
	}

	/**
	 * @see it.ibm.red.business.service.ISalvaDocumentoSRV#salvaMetadatiEstesi(java.lang.Integer,
	 *      java.util.Collection, java.lang.Long, java.lang.Long, java.lang.Long,
	 *      java.util.Date, boolean, java.sql.Connection,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public void salvaMetadatiEstesi(final Integer idDocumento, final Collection<MetadatoDTO> metadatiEstesi, final Long idAoo, final Long idTipologiaDocumento,
			final Long idTipoProcedimento, Date inDataCreazione, final boolean isModalitaInsert, final Connection con, final IFilenetCEHelper fceh) {
		LOGGER.info("salvaMetadatiEstesi -> START");
		Date dataCreazione = inDataCreazione;
		if (!isModalitaInsert) {
			dataCreazione = tipologiaDocumentoDAO.cancellaMetadatiEstesi(con, idDocumento, idAoo);
			if(dataCreazione == null) {
				Document firstVersionDocument = fceh.getFirstVersionById(idDocumento.toString(), idAoo.intValue());
				dataCreazione = firstVersionDocument.get_DateCreated();
			}
		}

		if (!CollectionUtils.isEmpty(metadatiEstesi)) {
			LOGGER.info("salvaMetadatiEstesi -> Salvataggio dei metadati estesi " + FRECCIA + "START. Document Title: " + idDocumento);
			final String xml = MetadatiEstesiHelper.serializeMetadatiEstesi(metadatiEstesi);
			tipologiaDocumentoDAO.salvaMetadatiEstesi(con, xml, idDocumento, idAoo, idTipologiaDocumento, idTipoProcedimento, dataCreazione);
			LOGGER.info("salvaMetadatiEstesi -> Salvataggio dei metadati estesi " + FRECCIA + "END. Document Title: " + idDocumento);
		}
		LOGGER.info("salvaMetadatiEstesi -> END");
	}

	/**
	 * @param esitoCreazione
	 * @param status
	 * @param salvaDocInput
	 * @param utente
	 * @param con
	 * @return
	 */
	private boolean calcolaAssegnazionePrincipale(final EsitoSalvaDocumentoDTO esitoCreazione, final SalvaDocumentoRedStatusDTO status,
			final SalvaDocumentoRedDTO salvaDocInput, final UtenteDTO utente, final Connection con) {
		LOGGER.info("calcolaAssegnazionePrincipale -> START");
		boolean esito = false;

		List<AssegnazioneDTO> assegnazioni = salvaDocInput.getAssegnazioni();
		final AssegnazioneDTO assegnazioneManuale = (assegnazioni != null && !assegnazioni.isEmpty()
				&& TipoAssegnazioneEnum.COMPETENZA.equals(assegnazioni.get(0).getTipoAssegnazione())) ? assegnazioni.get(0) : null;

		LOGGER.info("calcolaAssegnazionePrincipale -> Assegnazione manuale " + assegnazioneManuale);

		AssegnazioneDTO assegnazioneAutomatica = null;
		final String codiceCapitolo = getCapitolo(salvaDocInput.getMetadatiEstesi());
		if (codiceCapitolo != null) {
			LOGGER.info("calcolaAssegnazionePrincipale -> Capitolo presente nei metadati " + codiceCapitolo);

			// cerca automatismo => popola assegnazioneAutomatica
			final AssegnazioneAutomaticaCapSpesaDTO ass = assegnazioneAutomaticaCapSpesaSRV.get(codiceCapitolo, utente.getIdAoo(), con);
			if (ass != null) {

				final Nodo ufficioAssegnazioneAutomatica = nodoDAO.getNodo(ass.getAssegnatario().getIdUfficio(), con);
				String descrizioneAssegnazioneAutomatica = ufficioAssegnazioneAutomatica.getDescrizione();
				UtenteDTO utenteAssegnazioneAutomatica = null;
				if (ass.getAssegnatario().getIdUtente() != null && ass.getAssegnatario().getIdUtente() != 0) {
					utenteAssegnazioneAutomatica = new UtenteDTO();
					utenteAssegnazioneAutomatica.setId(ass.getAssegnatario().getIdUtente());
					utenteAssegnazioneAutomatica.setIdUfficio(ass.getAssegnatario().getIdUfficio());
					descrizioneAssegnazioneAutomatica += " - " + ass.getAssegnatario().getDescrizioneUtente();
				}

				assegnazioneAutomatica = new AssegnazioneDTO(null, TipoAssegnazioneEnum.COMPETENZA, null, null, null, descrizioneAssegnazioneAutomatica,
						utenteAssegnazioneAutomatica, new UfficioDTO(ufficioAssegnazioneAutomatica.getIdNodo(), ufficioAssegnazioneAutomatica.getDescrizione()));
			}

			LOGGER.info("calcolaAssegnazionePrincipale -> assegnazione automatica " + assegnazioneAutomatica);
		} else {
			// Se non esiste un'automazione per capitolo di spesa, si verifica che ne esista
			// una per metadato
			final AssegnatarioDTO assegnatario = assegnazioneAutomaticaSRV.getAssegnatario(utente.getIdAoo(),
					salvaDocInput.getTipologiaDocumento().getIdTipologiaDocumento().longValue(), salvaDocInput.getTipoProcedimento().getTipoProcedimentoId(), "",
					salvaDocInput.getMetadatiEstesi());

			// Se esiste un assegnatario definito da un'assegnazione automatica su metadati
			// viene impostata come assegnazione principale
			if (assegnatario != null && assegnatario.getIdUfficio() != null) {
				assegnazioneAutomatica = assegnazioneAutomaticaSRV.getAssegnazioneFromAssegnatario(assegnatario);
			}
		}

		final boolean aooHasAssegnazioneIndiretta = utente.getIdNodoAssegnazioneIndiretta() != null && utente.getIdNodoAssegnazioneIndiretta() != 0;
		if (aooHasAssegnazioneIndiretta) {
			salvaDocInput.setAssegnazioneIndiretta(Boolean.FALSE); // inizializza il booleano
		}

		LOGGER.info("calcolaAssegnazionePrincipale -> aooHasAsegnazioneIndiretta " + aooHasAssegnazioneIndiretta);

		if (assegnazioneAutomatica != null) {
			// Se fra i metadati del documento esiste il capitolo ed il capitolo, per questa
			// aoo, è associato ad una struttura,
			// l'assegnazione è automatica verso questa struttura

			if (assegnazioni == null) {
				assegnazioni = new ArrayList<>();
				salvaDocInput.setAssegnazioni(assegnazioni);
			}

			if (assegnazioneManuale != null) {
				assegnazioni.remove(0);
			}
			assegnazioni.add(0, assegnazioneAutomatica);
			status.setAssegnazioniCompetenza(DocumentoUtils.initArrayAssegnazioni(TipoAssegnazioneEnum.COMPETENZA, assegnazioni));

			esitoCreazione.setAssegnazioneCompetenzaAutomatica(true);
			esito = true;
		} else if (assegnazioneManuale != null) {
			// altrimenti, se l'utente ha specificato un assegnatario per competenza,
			// l'assegnazione è confermata
			esito = true;
		} else if (aooHasAssegnazioneIndiretta) {
			// altrimenti, se l'aoo ha una struttura di assegnazione indiretta,
			// l'assegnazione è verso questa struttura

			final Nodo ufficioAssegnazioneIndiretta = nodoDAO.getNodo(utente.getIdNodoAssegnazioneIndiretta(), con);

			// popola assegnazione indiretta
			final AssegnazioneDTO assegnazioneIndiretta = new AssegnazioneDTO(null, TipoAssegnazioneEnum.COMPETENZA, null, null, null,
					ufficioAssegnazioneIndiretta.getDescrizione(), null,
					new UfficioDTO(ufficioAssegnazioneIndiretta.getIdNodo(), ufficioAssegnazioneIndiretta.getDescrizione()));

			LOGGER.info("calcolaAssegnazionePrincipale -> assegnazioneIndiretta " + assegnazioneIndiretta);

			if (assegnazioni == null) {
				assegnazioni = new ArrayList<>();
				salvaDocInput.setAssegnazioni(assegnazioni);
			}
			assegnazioni.add(0, assegnazioneIndiretta);
			status.setAssegnazioniCompetenza(DocumentoUtils.initArrayAssegnazioni(TipoAssegnazioneEnum.COMPETENZA, assegnazioni));

			salvaDocInput.setAssegnazioneIndiretta(Boolean.TRUE);
			salvaDocInput.setMotivoAssegnazione("Competenza per assegnazione automatica");

			esitoCreazione.setAssegnazioneCompetenzaIndiretta(true);
			esito = true;
		}

		LOGGER.info("calcolaAssegnazionePrincipale -> END " + esito);
		return esito;
	}

	/**
	 * Recupera il codice del capitolo di spesa fra i metadati se presente.
	 * 
	 * @param metadatiEstesiDocInput
	 * @return
	 */
	private String getCapitolo(final Collection<MetadatoDTO> metadatiEstesiDocInput) {
		String codiceCapitolo = null;

		if (!CollectionUtils.isEmpty(metadatiEstesiDocInput)) {
			for (final MetadatoDTO metadato : metadatiEstesiDocInput) {
				if (TipoMetadatoEnum.CAPITOLI_SELECTOR.equals(metadato.getType())) {
					codiceCapitolo = ((CapitoloSpesaMetadatoDTO) metadato).getCapitoloSelected();
					break;
				}
			}
		}

		return codiceCapitolo;
	}

	/**
	 * Validazione processo mail ATTO DECERTO in entrata/uscita.
	 * 
	 * @param status
	 * @param docInput
	 * @param utente
	 */
	private boolean validaProcessoAttoDecretoUCB(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente) {
		boolean isValidazioneOK = true;

		try {
			final String tipoAttoDecretoUCBUscita = PropertiesProvider.getIstance()
					.getParameterByString(utente.getCodiceAoo() + "." + PropertiesNameEnum.ID_TIPOLOGIA_DOCUMENTO_USCITA_ATTO_DECRETO.getKey());
			final String tipoAttoDecretoUCBEntrata = PropertiesProvider.getIstance()
					.getParameterByString(utente.getCodiceAoo() + "." + PropertiesNameEnum.ID_TIPOLOGIA_DOCUMENTO_ENTRATA_ATTO_DECRETO.getKey());

			if (utente.isUcb() && !CollectionUtils.isEmpty(docInput.getAllegati())
					&& (tipoAttoDecretoUCBUscita.equals(docInput.getTipologiaDocumento().getIdTipologiaDocumento().toString())
							|| tipoAttoDecretoUCBEntrata.equals(docInput.getTipologiaDocumento().getIdTipologiaDocumento().toString()))) {

				final Collection<String> filenames = new ArrayList<>();
				for (final AllegatoDTO a : docInput.getAllegati()) {
					filenames.add(a.getNomeFile());
				}

				// Validazione dei file allegati tramite verifica dell'estensione
				final Collection<String> filesNotValid = attoDecretoUCBSRV.validateFilenames(filenames);
				isValidazioneOK = filesNotValid.isEmpty();

				// Se il documento è in entrata e la validazione è K.O., si invia una mail di
				// notifica ma si continua comunque con la creazione
				if (status.isDocEntrata() && !isValidazioneOK) {
					isValidazioneOK = true; // Si continua con la creazione

					if (docInput.getProtocollazioneMailAccount() != null) {
						final byte[] content = docInput.getContent();
						final Date dataProtocollo = docInput.getDataProtocollo();
						final Integer numeroProtocollo = docInput.getNumeroProtocollo();
						final String casellaPostale = docInput.getProtocollazioneMailAccount();
						final String mittenteMessaggio = docInput.getMittenteContatto().getMail();
						final DestinatarioCodaMailDTO destinatario = new DestinatarioCodaMailDTO(docInput.getMittenteContatto().getMail(),
								docInput.getMittenteContatto().getContattoID());

						attoDecretoUCBSRV.comunicaErroreProtocollazione(utente, content, destinatario, casellaPostale, mittenteMessaggio, numeroProtocollo, dataProtocollo,
								filesNotValid);
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la validazione del Flusso ATTO DECRETO UCB.", e);
		}

		return isValidazioneOK;
	}

	/**
	 * @param status
	 * @param docInput
	 * @param utente
	 * @param inFceh
	 * @param inFpeh
	 * @param con
	 * @param dwhCon
	 */
	private void inserisciOAggiornaDocumento(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente,
			final IFilenetCEHelper inFceh, final FilenetPEHelper inFpeh, final Connection con, final Connection dwhCon) {
		final boolean isNodoUcp = nodoDAO.isNodoUcp(utente.getIdUfficio(), con);

		// Connessione admin poichè, in caso di utente UCP e documento in entrata, non
		// si avrebbero i permessi necessari
		// per creare l'annotation ed altri aggiornamenti sul documento (gestione del
		// riservato)
		final boolean adminConnection = status.isModalitaInsert() && status.isDocEntrata() && Boolean.TRUE.equals(docInput.getIsRiservato()) && isNodoUcp;

		IFilenetCEHelper fceh = inFceh;
		FilenetPEHelper fpeh = inFpeh;

		if (adminConnection) {
			logoff(fpeh);
			popSubject(fceh);
			fceh = getFCEHAdmin(utente.getFcDTO(), utente.getIdAoo());
			fpeh = getFPEHAdmin(utente.getFcDTO());
		}

		if (status.isModalitaUpdate() || status.isModalitaUpdateIbrida()) {
			aggiornaDocumento(status, docInput, utente, fceh, fpeh, con, dwhCon);
		} else {
			inserisciDocumento(status, docInput, utente, isNodoUcp, fceh, fpeh, con, dwhCon);
		}
	}

	/**
	 * @param status
	 * @param docInput
	 * @param utente
	 * @param fceh
	 * @param fpeh
	 * @param con
	 */
	private void inserisciOAggiornaDichiarazioneServiziResi(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente,
			final IFilenetCEHelper fceh, final FilenetPEHelper fpeh, final Connection con) {
		if (status.isModalitaUpdate() || status.isModalitaUpdateIbrida()) {
			aggiornaDocDichiarazioneServiziResi(status, docInput, utente, fceh, fpeh, con);
		} else {
			inserisciDocDichiarazioneServiziResi(status, docInput, utente, fceh, fpeh, con);
		}
	}

	/**
	 * @param status
	 * @param docInput
	 * @param utente
	 * @param isNodoUcp
	 * @param fceh
	 * @param fpeh
	 * @param con
	 * @param dwhCon
	 */
	private void inserisciDocumento(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente, final boolean isNodoUcp,
			final IFilenetCEHelper fceh, final FilenetPEHelper fpeh, final Connection con, final Connection dwhCon) {
		LOGGER.info("inserisciDocumento -> START");
		status.setCreazioneBozza(false);
		status.setDocDaSalvare(new DocumentoRedFnDTO());
		VWWorkObject wob = null;
		final Long idAoo = utente.getIdAoo();

		try {
			// ### RECUPERO DEL WOB (SE PRESENTE)
			if (StringUtils.isNotBlank(docInput.getWobNumber())) {
				wob = fpeh.getWorkFlowByWob(docInput.getWobNumber(), true);
			}

			// ### MAIL IN INGRESSO (PROTOCOLLAZIONE MANUALE) -> START
			// Se non si tratta di una protocollazione automatica...
			if (status.isModalitaMailEntrata() && !status.isProtocollazioneAutomatica()) {
				final Document mailFilenet = fceh.getDocument(fceh.idFromGuid(status.getInputMailGuid()));

				// ...si imposta lo stato della mail a "In fase di Protocollazione Manuale"
				aggiornaStatoEmail(status.getInputMailGuid(), mailFilenet, StatoMailEnum.IN_FASE_DI_PROTOCOLLAZIONE_MANUALE, fceh);

				// ...se la protocollazione manuale della mail è stata forzata, si aggiorna
				// l'utente protocollatore
				if (status.isForzaProtocollazioneMail()) {
					mailSRV.aggiornaUtenteProtocollatoreMail(mailFilenet, utente.getId(), fceh);
				}
			}
			// ### MAIL IN INGRESSO (PROTOCOLLAZIONE MANUALE) -> END

			// ### INSERIMENTO DEL DOCUMENTO NEL DB --- idDocumento/Document Title
			final int idDocumento = documentoDAO.insert(idAoo, docInput.getTipologiaDocumento().getIdTipologiaDocumento(),
					Boolean.TRUE.equals(docInput.getRegistroRiservato()) ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue(), con);
			// Impostazione del Document Title nel documento in input
			docInput.setDocumentTitle(String.valueOf(idDocumento));

			final Aoo aoo = aooSRV.recuperaAoo(utente.getIdAoo().intValue());

			// ### ALLACCI -> START --- Inserimento documenti da allacciare
			salvaDocumentiDaAllacciare(docInput.getAllacci(), idDocumento, idAoo, con);
			LOGGER.info("Allacci salvati per il documento: " + idDocumento);
			// ### ALLACCI -> END

			// ### SECURITY -> START
			impostaSecurityDocumento(status, docInput, idDocumento, utente, isNodoUcp, con);
			LOGGER.info("Security impostate per il documento: " + idDocumento);
			// ### SECURITY -> END

			// ### CONTENT DOCUMENTO -> START
			impostaContentDocumento(status, docInput);
			LOGGER.info("Content impostato per il documento: " + idDocumento);
			// ### CONTENT DOCUMENTO -> END

			// ### ALLEGATI -> START
			final List<AllegatoDTO> allegatiNuovoDoc = new ArrayList<>();
			if (!CollectionUtils.isEmpty(docInput.getAllegati())) {
				allegatiNuovoDoc.addAll(docInput.getAllegati());
			}
			if (!CollectionUtils.isEmpty(docInput.getAllegatiNonSbustati())) {
				allegatiNuovoDoc.addAll(docInput.getAllegatiNonSbustati());
			}
			if (!CollectionUtils.isEmpty(allegatiNuovoDoc)) {
				impostaAllegatiNuovoDocumento(status, allegatiNuovoDoc, idAoo, con, fceh);
				LOGGER.info("Allegati impostati per il documento: " + idDocumento);
			}
			// ### ALLEGATI -> END
			// CONTATTO_ON_THE_FLY - START
			if (docInput.getMittenteContatto() != null && docInput.getMittenteContatto().getContattoID() == null) {
				rubricaSRV.insertContattoOnTheFly(docInput.getMittenteContatto(), utente.getIdAoo());
			} else if (docInput.getMittenteContatto() != null && docInput.getMittenteContatto().getContattoID() != null) {
				if (docInput.getMittenteContatto().getOnTheFly() != null && docInput.getMittenteContatto().getOnTheFly() == 1) {
					gestisciContattoOnTheFly(docInput, utente);
				} else if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(docInput.getMittenteContatto().getAliasContattoProtocollazione())
						&& !docInput.getMittenteContatto().getAliasContattoProtocollazione().equals(docInput.getMittenteContatto().getAliasContatto())) {
					gestisciContattoOnTheFly(docInput, utente);
				}
			}

			// CONTATTO_ON_THE_FLY - END

			// ### INSERIMENTO DOCUMENTO SU FILENET -> START
			popolaESalvaDocumentoFilenet(status, idDocumento, docInput, utente, fceh, con);
			// ### INSERIMENTO DOCUMENTO SU FILENET -> END

			// ### CONTRIBUTO ESTERNO -> START
			// Gestione documento "Contributo Esterno": si sta inserendo un documento di
			// Contributo Esterno per un documento già presente
			if (status.isDocContributoEsterno()) {
				gestisciContributoEsterno(idDocumento, utente, wob, fceh, con, dwhCon);
			}
			// ### CONTRIBUTO ESTERNO -> END

			// ### ITER MANUALE DI SPEDIZIONE: INVIO DELLA MAIL AI DESTINATARI ELETTRONICI
			// -> START
			if (status.getIterManualeSpedizione() && aoo.getPkHandlerTimbro() != null) {
				final List<DestinatarioRedDTO> destinatariElettronici = new ArrayList<>();
				for (final DestinatarioRedDTO destinatario : docInput.getDestinatari()) {
					if (MezzoSpedizioneEnum.ELETTRONICO.equals(destinatario.getMezzoSpedizioneEnum())) {
						destinatariElettronici.add(destinatario);
					}
				}
				if (!CollectionUtils.isEmpty(destinatariElettronici)) {
					status.setIdNotificaSped(documentManagerSRV.inviaMailSpedizione(utente, docInput.getDocumentTitle(), docInput.getMailSpedizione().getMittente(),
							destinatariElettronici, docInput.getMailSpedizione().getOggetto(), docInput.getMailSpedizione().getTesto()));
					if (status.getIdNotificaSped() == null) {
						LOGGER.error("Errore durante la spedizione della mail");
					}
				}
			}
			// ### ITER MANUALE DI SPEDIZIONE: INVIO DELLA MAIL AI DESTINATARI ELETTRONICI
			// -> END

			// ######## OPERAZIONI DA ESEGUIRE IN SEGUITO AL SALVATAGGIO DEL DOCUMENTO SU
			// FILENET, SIA IN CREAZIONE SIA IN MODIFICA IBRIDA
			postSalvataggioDocumentoFilenet(status, wob, docInput, utente, fceh, fpeh, con, dwhCon);
			
			// Fascicolazione protocollo notifica se esiste una notifica associata al documento di flusso AUT
			fascicolaProtocolloNotifica(status, docInput, utente, fceh, con);
			
			// ### GESTIONE NOTIFICA PER DOCUMENTI DI TIPOLOGIA "RITIRO IN AUTOTUTELA"
			gestioneNotificaRitiroAutotutela(status, docInput, utente);

			// ### VALIDAZIONE DEL PROCESSO PER I DOCUMENTI IN ENTRATA DI TIPOLOGIA "ATTO
			// DECRETO" UCB
			if (status.isDocEntrata()) {
				validaProcessoAttoDecretoUCB(status, docInput, utente);
			}
			
			if(status.isContentFromNPS() && status.getGuidLastVersionNPSDocPrincipale() != null) {
				aggiungiSecondaVersioneDocumentoFromNPS(fceh, "" + idDocumento, utente, status.getGuidLastVersionNPSDocPrincipale());
			}

		} catch (final Exception e) {
			LOGGER.error("inserisciDocumento -> Si è verificato un errore durante l'inserimento di un nuovo documento da parte dell'utente: " + utente.getUsername(), e);
			rollbackInserimentoDocumento(status, e, "inserisciDocumento", utente);
		}

		LOGGER.info("inserisciDocumento -> END");
	}
	
	private void aggiungiSecondaVersioneDocumentoFromNPS(final IFilenetCEHelper fceh, final String idDocumento, final UtenteDTO utente, final String guidLastVersionNPSDocPrincipale) {
		final List<String> selectList = Arrays.asList(PropertyNames.ID, PropertyNames.IS_RESERVED,
				PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY));

		Document doc = fceh.getDocumentByIdGestionale(idDocumento, selectList, null, utente.getIdAoo().intValue(), null, null);
		
		//recupera la seconda versione del dontent su NPS
		DocumentoNpsDTO secondaVersioneDocPrincipale = npsSRV.downloadDocumento(utente.getIdAoo().intValue(), guidLastVersionNPSDocPrincipale, false);
		
		//aggiorna filenet con la seconda versione del documento
		final Map<String, Object> newProperty = new HashMap<>();
		newProperty.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.TRASFORMAZIONE_PDF_ERRORE_METAKEY), Constants.Varie.TRASFORMAZIONE_PDF_OK);
		newProperty.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), secondaVersioneDocPrincipale.getNomeFile());
		fceh.addVersion(doc, secondaVersioneDocPrincipale.getInputStream(), newProperty, secondaVersioneDocPrincipale.getNomeFile(), secondaVersioneDocPrincipale.getContentType(), utente.getIdAoo());
	}

	private void gestioneNotificaRitiroAutotutela(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente) {
		try {
			final String tipoIntegrazioneDati = PropertiesProvider.getIstance()
					.getParameterByString(utente.getCodiceAoo() + "." + PropertiesNameEnum.ID_TIPOLOGIA_DOCUMENTO_ENTRATA_RITIRO_IN_AUTOTUTELA.getKey());

			// Il documento deve essere in Entrata e di tipo RITIRO_AUTOTUTELA
			if (status.isDocEntrata() && tipoIntegrazioneDati != null && tipoIntegrazioneDati.equals(docInput.getTipologiaDocumento().getIdTipologiaDocumento().toString())) {

				AssegnazioneDTO assCompetenza = null;
				for (final AssegnazioneDTO ass : docInput.getAssegnazioni()) {
					if (TipoAssegnazioneEnum.COMPETENZA.equals(ass.getTipoAssegnazione())) {
						assCompetenza = ass;
						break;
					}
				}

				if (assCompetenza == null) {
					LOGGER.error("Assegnatario per competenza non definito.");
					throw new RedException("Assegnatario per competenza non definito.");
				}

				if (docInput.getProtocolloRiferimento() != null) {
//					 <-- invio notifica all' assegnatario per competenza. -->

					long idUtente = 0;
					if (assCompetenza.getUtente() != null) {
						idUtente = assCompetenza.getUtente().getId();
					}

					inserisciNotificaRitiroAutotutela(utente.getIdAoo().intValue(), Long.parseLong(docInput.getDocumentTitle()), assCompetenza.getUfficio().getId(), idUtente);

				} else {
//					 <-- Invio notifica al dirigente dell'ufficio dell'assegnatario per competenza. -->
//						 (e se configurato anche al delegato al libro firma).

					// Recupero del Dirigente.
					final NodoOrganigrammaDTO nodto = new NodoOrganigrammaDTO(null, assCompetenza.getUfficio().getId().intValue(), null);
					final NodoOrganigrammaDTO dirigente = orgSRV.getDirigenteFromNodo(nodto);

					// Inserimento notifica per il Dirigente.
					if (dirigente != null) {
						inserisciNotificaRitiroAutotutela(utente.getIdAoo().intValue(), Long.parseLong(docInput.getDocumentTitle()), assCompetenza.getUfficio().getId(),
								dirigente.getIdUtente());

						// Recupero del Delegato al LibroFirma.
						final DelegatoDTO delegato = utenteSRV.getDelegatoLibroFirma(dirigente.getIdNodo(), dirigente.getIdAOO());
						
						if (delegato != null) {
							// Inserimento notifica per il Delegato al LibroFirma.
							inserisciNotificaRitiroAutotutela(utente.getIdAoo().intValue(), Long.parseLong(docInput.getDocumentTitle()), delegato.getIdNodo(),
									delegato.getIdUtente());

						}
					}
				}

			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la gestione della notifica per il documento di tipo Ritiro in Autotutela", e);
			throw new RedException("Errore durante la gestione della notifica per il documento di tipo Ritiro in Autotutela", e);
		}
	}

	/**
	 * @param idAoo
	 * @param idDocumento
	 * @param idNodo
	 * @param idUtente
	 */
	private void inserisciNotificaRitiroAutotutela(final int idAoo, final long idDocumento, final long idNodo, final long idUtente) {
		final NotificaRitiroAutotutelaDTO item = new NotificaRitiroAutotutelaDTO(idDocumento, idNodo, idUtente);
		notificaRitiroAutotutelaSRV.writeNotifiche(idAoo, item);
	}

	private void gestisciContattoOnTheFly(final SalvaDocumentoRedDTO docInput, final UtenteDTO utente) {
		docInput.getMittenteContatto().setContattoID(null);
		if (TipologiaIndirizzoEmailEnum.PEC.equals(docInput.getMittenteContatto().getTipologiaEmail())
				&& !it.ibm.red.business.utils.StringUtils.isNullOrEmpty(docInput.getMittenteContatto().getMail())) {
			docInput.getMittenteContatto().setMail(null);
		} else if (TipologiaIndirizzoEmailEnum.PEO.equals(docInput.getMittenteContatto().getTipologiaEmail())
				&& !it.ibm.red.business.utils.StringUtils.isNullOrEmpty(docInput.getMittenteContatto().getMailPec())) {
			docInput.getMittenteContatto().setMailPec(null);
		}
		rubricaSRV.insertContattoOnTheFly(docInput.getMittenteContatto(), utente.getIdAoo());
	}

	private void rollbackInserimentoDocumento(final SalvaDocumentoRedStatusDTO status, final Exception e, final String chiamante, final UtenteDTO utente) {
		final IFilenetCEHelper fcehAdmin = getFCEHAdmin(utente.getFcDTO(), utente.getIdAoo());

		try {
			// ### CREAZIONE --- Se il documento è stato inserito ex-novo
			if (status.isDocumentoInserito() && !status.isDocumentoProtocollato()) {
				// Si elimina il documento su FileNet
				fcehAdmin.deleteVersion(fcehAdmin.idFromGuid(status.getDocDaSalvare().getGuid()));
				LOGGER.info(chiamante + " -> Cancellazione da FileNet del documento creato effettuata");

				// Si cancellano i fascicoli automatici su FileNet
				if (status.isFascicoloAutomatico() && !CollectionUtils.isEmpty(status.getAssegnazioniFascicolo())) {
					for (final AssegnazioneWorkflowDTO assegnazioneWorkflow : status.getAssegnazioniFascicolo()) {
						final FascicoloRedFnDTO fascicolo = assegnazioneWorkflow.getFascicolo();
						fcehAdmin.eliminaFascicoloSeVuoto(fascicolo.getIdFascicolo(), utente.getIdAoo());
					}
					LOGGER.info(chiamante + " -> Cancellazione da FileNet dei fascicoli automatici creati effettuata");
				}
			}

			// ### MAIL IN INGRESSO (PROTOCOLLAZIONE MANUALE) -> Si re-imposta lo stato
			// della mail a "In Arrivo"
			if (status.isModalitaMailEntrata() && !status.isProtocollazioneAutomatica() && !status.isDocumentoProtocollato()) {
				aggiornaStatoEmail(status.getInputMailGuid(), StatoMailEnum.INARRIVO, fcehAdmin);
				LOGGER.info(chiamante + " -> Ripristino su FileNet dello stato della mail da protocollare effettuato");
			}
		} catch (final Exception ex) {
			LOGGER.error(chiamante + " -> Si è verificato un errore durante il rollback delle operazioni eseguite su FileNet" + " per l'inserimento del documento", ex);
		}

		// Si rilancia l'eccezione per la gestione nel chiamante
		if (e instanceof RedException) {
			throw (RedException) e;
		} else {
			throw new RedException(e);
		}
	}

	private void popolaESalvaDocumentoFilenet(final SalvaDocumentoRedStatusDTO status, final int idDocumento, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente,
			final IFilenetCEHelper fceh, final Connection con) {
		final Map<String, Object> metadatiDocumento = caricaMetadatiDocumento(status, docInput, utente); // Caricamento dei Metadati dal documento in input
		popolaESalvaDocumentoFilenet(status, idDocumento, docInput, null, metadatiDocumento, utente.getIdAoo(), fceh, con);
	}

	private void popolaESalvaDocumentoFilenet(final SalvaDocumentoRedStatusDTO status, final int idDocumento, final SalvaDocumentoRedDTO docInput,
			final FascicoloRedFnDTO fascicoloRedFn, final Map<String, Object> metadatiDocumento, final Long idAoo, final IFilenetCEHelper fceh, final Connection con) {
		final DocumentoRedFnDTO docDaSalvare = status.getDocDaSalvare();

		// ### Costruzione dell'oggetto Documento FileNet (N.B. CONTENT, SECURITY e
		// ALLEGATI sono già stati eventualmente IMPOSTATI IN PRECEDENZA)
		docDaSalvare.setMetadati(metadatiDocumento); // Metadati
		docDaSalvare.setDocumentTitle(docInput.getDocumentTitle()); // Document Title
		docDaSalvare.setClasseDocumentale(getClasseDocumentaleDoc(status, docInput.getTipologiaDocumento().getIdTipologiaDocumento(), con)); // Classe Documentale

		Date dataInizio = new Date();
		// ### Inserimento su FileNet
		final Document docFilenet = creaDocumentoSuFilenet(status, fascicoloRedFn, fceh, idAoo);
		Date dataFine = new Date();
		LOGGER.info("popolaESalvaDocumentoFilenet -> È stato inserito su FileNet il nuovo documento: " + idDocumento);
		LOGGER.info("Timing popolaESalvaDocumentoFilenet -> creaDocumentoSuFilenet per il documento: " + idDocumento + FRECCIA + (dataFine.getTime() - dataInizio.getTime())
				+ "ms");

		// ### Impostazione del GUID
		final String guid = docFilenet.get_Id().toString();
		docInput.setGuid(guid);
		docDaSalvare.setGuid(guid);
		// ### Impostazione del Numero Documento
		final Integer numeroDocumento = (Integer) TrasformerCE.getMetadato(docFilenet, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
		docInput.setNumeroDocumento(numeroDocumento);

		// ### Se non è una bozza, si crea l'annotation contenente la versione corrente
		// (CURRENT_VERSION)
		if (!status.isCreazioneBozza()) {
			dataInizio = new Date();
			fceh.creaAnnotation(String.valueOf(idDocumento), docInput.getGuid(), FilenetAnnotationEnum.CURRENT_VERSION, idAoo);
			dataFine = new Date();
			LOGGER.info("popolaESalvaDocumentoFilenet -> È stata creata una Annotation con la versione corrente per il documento: " + idDocumento);
			LOGGER.info(
					"Timing popolaESalvaDocumentoFilenet -> creaAnnotation per il documento: " + idDocumento + FRECCIA + (dataFine.getTime() - dataInizio.getTime()) + "ms");
		}

		status.setDocumentoInserito(true);
	}

	private void postSalvataggioDocumentoFilenet(final SalvaDocumentoRedStatusDTO status, final VWWorkObject wob, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente,
			final IFilenetCEHelper fceh, final FilenetPEHelper fpeh, final Connection con, final Connection dwhCon) {
		final int idDocumento = NumberUtils.toInt(docInput.getDocumentTitle());
		LOGGER.info("postSalvataggioDocumentoFilenet -> START. Documento: " + idDocumento);
		Date dataInizio;
		Date dataFine;
		String documentoMailGuid = null;

		try {
			// ### GESTIONE MAIL TAB SPEDIZIONE (GESTIONE BOZZA MAIL DI SPEDIZIONE) -> START
			if (!status.isDocEntrata() && !status.isDocInterno() && !status.isDocAssegnazioneInterna()) {
				documentoMailGuid = creaEmailSpedizioneSuFilenet(status, docInput.getDocumentTitle(), docInput.getMailSpedizione(), utente, fceh, con);
				if (documentoMailGuid != null) {
					LOGGER.info("creaEmailSpedizioneSuFilenet -> Creazione e-mail per i destinatari elettronici completata");
				}
			}
			// ### GESTIONE MAIL TAB SPEDIZIONE (GESTIONE BOZZA MAIL DI SPEDIZIONE) -> END

			if (hasDocAssegnazioniIterManuale(status) || hasDocIterAutomatico(status) || status.isDocContributoEsterno()) {
				// ### PROTOCOLLAZIONE -> START
				gestisciProtocollazione(status, docInput, idDocumento, utente, fceh, con);
				// ### PROTOCOLLAZIONE -> END

				// ### FASCICOLAZIONE - INSERIMENTO DEI FASCICOLI -> START
				final List<FascicoloRedFnDTO> fascicoli = gestisciFascicolazione(status, idDocumento, docInput, utente, fceh, con);
				// ### FASCICOLAZIONE - INSERIMENTO DEI FASCICOLI -> END

				// ### FALDONATURA DEI FASCICOLI -> START
				// Faldonatura dei fascicoli su FileNet
				if (status.getNomeFaldoniSelezionati() != null) {
					for (final String faldone : status.getNomeFaldoniSelezionati()) {
						LOGGER.info("postSalvataggioDocumentoFilenet -> faldonaFascicoli: START. Documento: " + idDocumento);
						faldoneSRV.faldonaFascicoli(faldone, fascicoli, utente.getIdAoo(), fceh);
						LOGGER.info("postSalvataggioDocumentoFilenet -> faldonaFascicoli: END. Documento: " + idDocumento);
					}
				}
				// ### FALDONATURA DEI FASCICOLI -> END

				// ### AVVIO E GESTIONE DEL/I WORKFLOW SU FILENET -> START
				gestisciWorkflows(status, idDocumento, docInput, fascicoli, wob, utente, fceh, fpeh, con);
				LOGGER.info("postSalvataggioDocumentoFilenet -> Sono stati avviati " + status.getWorkflowList().size() + " workflow per il documento: " + idDocumento);
				// ### AVVIO E GESTIONE DEL/I WORKFLOW SU FILENET -> END

				// ### MODALITA POST-SCANSIONE (a.k.a. POSTCENS) -> START
				if (status.isModalitaPostCens() && docInput.getGuidDocumentiCartacei() != null) {
					final IFilenetCEHelper fcehAdmin = getFCEHAdmin(utente.getFcDTO(), utente.getIdAoo());

					// Eliminazione degli allegati cartacei (classe documentale NSD_POSTCENS)
					if (!CollectionUtils.isEmpty(docInput.getGuidDocumentiCartacei().getChildren())) {
						fcehAdmin.deleteAllegatiByGuid(docInput.getGuidDocumentiCartacei().getData(),
								docInput.getGuidDocumentiCartacei().getChildren().stream().map(NodoDTO::getData).collect(Collectors.toList()));
					}

					// Eliminazione del documento principale cartaceo (classe documentale
					// NSD_POSTCENS)
					fcehAdmin.eliminaVersioneDocumentoByGuid(docInput.getGuidDocumentiCartacei().getData());
				}
				// ### MODALITA POST-SCANSIONE (a.k.a. POSTCENS) -> END

				// ### MODALITA MAIL IN INGRESSO (PROTOCOLLAZIONE MANUALE/AUTOMATICA) -> START
				if (status.isModalitaMailEntrata()) {
					gestisciProtocollazioneEmail(status.getInputMailGuid(), idDocumento, status.getNumeroProtocollo(), status.getAnnoProtocollo(),
							docInput.getContentNoteDaMailProtocollaDTO(), docInput.getPreassegnatarioDaMailProtocollaInterop(), status.isInviaNotificaProtocollazioneMail(),
							status.isProtocollazioneAutomatica(), docInput.getProtocollaEMantieni(), utente, fceh, con);
				}
				// ### MODALITA MAIL IN INGRESSO (PROTOCOLLAZIONE MANUALE/AUTOMATICA) -> END
			}

			// ### TRASFORMAZIONE IN PDF -> START
			// Trasformazione in PDF se il documento ha content (che non è la velina di
			// protocollo) e:
			// a) è inviato in firma (iter automatico) o ha un'assegnazione per Competenza/Firma/Sigla/Visto/FirmaMultipla
			// oppure
			// b) è un documento in ingresso
			// oppure
			// c) si proviene dalla protocollazione e-mail.
			if (hasContent(docInput) && !status.isVelinaAsDocPrincipale() && !status.isContentFromNPS() && (status.isContentScannerizzato()
					|| (status.isDocEntrata() || status.isModalitaMailEntrata() || (hasDocAssegnazioniIterManuale(status) || hasDocIterAutomatico(status))))) {
				// Esecuzione della trasformazione PDF in modalità asincrona
				eseguiTrasformazionePDF(status, docInput, utente, ArrayUtils.addAll(status.getAssegnazioniFirma(), status.getAssegnazioniFirmaMultipla()),
						status.getIdAllegatiDaFirmare(), null, null, true, true, true, docInput.getTipoAssegnazioneEnum(), status.getIdAllegatiConFirmaVisibile(), con);
			}
			// ### TRASFORMAZIONE IN PDF -> END

			// ### GENERAZIONE VELINA (PREVIEW) E RELATIVA ANNOTATION -> START
			dataInizio = new Date();
			creaPreviewEImpostaAnnotationPreview(status, idDocumento, docInput, utente, fceh);
			dataFine = new Date();
			LOGGER.info("Timing creaPreviewEImpostaAnnotation per il documento: " + status.getDocDaSalvare().getDocumentTitle() + FRECCIA
					+ (dataFine.getTime() - dataInizio.getTime()) + "ms");
			// ### GENERAZIONE VELINA (PREVIEW) E RELATIVA ANNOTATION -> END

			// ### SOTTOSCRIZIONI (TRACCIA PROCEDIMENTO)
			gestisciTracciaProcedimento(docInput, utente, dwhCon);

			// ### TEMPLATE DOC USCITA
			gestisciTemplateDocUscita(docInput, utente.getId(), con);

			// ### METADATI ESTESI -> START
			salvaMetadatiEstesi(idDocumento, docInput, status.isModalitaInsert(), utente.getIdAoo(), con, fceh);
			// ### METADATI ESTESI -> END

			// ### METADATI REGISTRO AUSILIARIO -> START
			salvaMetadatiRegistroAusiliario(docInput, (status.isModalitaInsert() || status.isModalitaMailEntrata()), con);
			// ### METADATI REGISTRO AUSILIARIO -> END
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'esecuzione delle operazioni successive al salvataggio su FileNet del documento: " + idDocumento, e);

			// Se l'e-mail del tab Spedizione era stata inserita su FileNet, la si deve
			// rimuovere
			if (StringUtils.isNotBlank(documentoMailGuid)) {
				fceh.eliminaVersioneDocumentoByGuid(documentoMailGuid);
			}

			throw new RedException("Si è verificato un errore durante l'esecuzione delle operazioni successive al salvataggio su FileNet del documento: " + idDocumento, e);
		}

		LOGGER.info("postSalvataggioDocumentoFilenet -> END. Documento: " + idDocumento);
	}

	private void gestisciWorkflows(final SalvaDocumentoRedStatusDTO status, final int idDocumento, final SalvaDocumentoRedDTO docInput,
			final List<FascicoloRedFnDTO> fascicoli, final VWWorkObject wob, final UtenteDTO utente, final IFilenetCEHelper fceh, final FilenetPEHelper fpeh,
			final Connection con) {
		LOGGER.info("gestisciWorkflows -> START. Documento: " + idDocumento);
		final Map<String, Object> metadatiWorkflow = new HashMap<>();
		String wobNumber = null;

		// ### METADATI COMUNI -> START
		// Metadato ID Documento
		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY), idDocumento);

		// Metadato ID Nodo Mittente
		if (status.getIdUfficioMittenteWorkflow() != null) {
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), status.getIdUfficioMittenteWorkflow());
		} else {
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), utente.getIdUfficio());
		}

		// Metadato ID Utente Mittente
		if (status.getIdUtenteMittenteWorkflow() != null) {
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), status.getIdUtenteMittenteWorkflow());
		} else {
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId());
		}

		// Metadato Registro Riservato
		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY),
				Boolean.TRUE.equals(docInput.getRegistroRiservato()) ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue());

		// Metadato Urgente
		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.URGENTE_METAKEY),
				Boolean.TRUE.equals(docInput.getIsUrgente()) ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue());

		// Metadato Data Scadenza
		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY),
				docInput.getDataScadenza() != null ? DateUtils.setDateTo2359(docInput.getDataScadenza())
						: Date.from(DateUtils.NULL_DATE.atZone(ZoneId.systemDefault()).toInstant()));

		// Metadato Firma Digitale RGS
		final Long idUtenteRagGenDelloStato = utenteDAO.getIdRagioniereGeneraleDelloStato(con);
		boolean assegnatoPerFirmaRGS = false;
		if (!CollectionUtils.isEmpty(docInput.getAssegnazioni())) {
			for (final AssegnazioneDTO assegnazione : docInput.getAssegnazioni()) {

				if ((TipoAssegnazioneEnum.FIRMA.equals(docInput.getTipoAssegnazioneEnum()) || TipoAssegnazioneEnum.FIRMA_MULTIPLA.equals(docInput.getTipoAssegnazioneEnum()))
						&& assegnazione.getUtente() != null && assegnazione.getUtente().getId().equals(idUtenteRagGenDelloStato)) {
					assegnatoPerFirmaRGS = true;
					break;
				}

			}
		}
		// Se l'iter approvativo è "Firma Ragionere" oppure il documento è stato
		// assegnato al Ragioniere Generale per la firma,
		// si imposta il metadato "Firma Digitale RGS"
		if ((status.getIterApprovativo() != null && IterApprovativoDTO.ITER_FIRMA_RAGIONIERE.equals(status.getIterApprovativo().getIdIterApprovativo()))
				|| assegnatoPerFirmaRGS) {
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.FIRMA_DIG_RGS_WF_METAKEY),
					Boolean.TRUE.equals(docInput.getCheckFirmaDigitaleRGS()) ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue());
		}

		// Metadato Elenco Destinatari Interni
		if (status.getDestinatariInterniString() != null && status.getDestinatariInterniString().length > 0) {
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_DESTINATARI_INTERNI_METAKEY), status.getDestinatariInterniString());
		}

		// Metadato ID Coda Lavorazione (gestione per i documenti da inserire nella coda
		// IN_ACQUISIZIONE)
		if (status.isDocEntrata()) {
			if (StringUtils.isNotBlank(docInput.getBarcode()) && !hasContent(docInput)) {
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_CODA_LAVORAZIONE_FN_METAKEY), 1);
			} else {
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_CODA_LAVORAZIONE_FN_METAKEY), 0);
			}
		}

		if (!status.isPrecensito()) {
			// Metadati Copia Conforme (gestione Firma Copia Conforme)
			gestisciMetadatiWorkflowCopiaConforme(docInput, metadatiWorkflow);
		}
		// ### METADATI COMUNI -> END

		if (hasDocIterAutomatico(status)) {
			LOGGER.info("gestisciWorkflows -> Avvio Workflow con Iter Automatico per il documento: " + idDocumento);
			int idTipoFirma = 0;

			// Workflow Parziali
			String[] elencoLibroFirma = null;
			if (status.getIterApprovativo().getParziale().equals(IterApprovativoParzialeEnum.SIGLA_FIRMA.getIntValue())) {
				elencoLibroFirma = getElencoLibroFirma(status.getAssegnazioniSigla(), status.getAssegnazioniFirma(), TipoAssegnazioneEnum.SIGLA, TipoAssegnazioneEnum.FIRMA);
			} else if (status.getIterApprovativo().getParziale().equals(IterApprovativoParzialeEnum.FIRMA_SIGLA.getIntValue())) {
				elencoLibroFirma = getElencoLibroFirma(status.getAssegnazioniFirma(), status.getAssegnazioniSigla(), TipoAssegnazioneEnum.FIRMA, TipoAssegnazioneEnum.SIGLA);
			} else if (status.getIterApprovativo().getParziale().equals(IterApprovativoParzialeEnum.VISTA_FIRMA.getIntValue())) {
				elencoLibroFirma = getElencoLibroFirma(status.getAssegnazioniVisto(), status.getAssegnazioniFirma(), TipoAssegnazioneEnum.VISTO, TipoAssegnazioneEnum.FIRMA);
			} else if (status.getIterApprovativo().getParziale().equals(IterApprovativoParzialeEnum.FIRMA_VISTA.getIntValue())) {
				elencoLibroFirma = getElencoLibroFirma(status.getAssegnazioniFirma(), status.getAssegnazioniVisto(), TipoAssegnazioneEnum.FIRMA, TipoAssegnazioneEnum.VISTO);
			} else {
				// Iter automatico Firma Dirigente/Direttore/Ragioniere
				idTipoFirma = status.getIterApprovativo().getTipoFirma();
			}

			// Metadato ID Tipo Firma
			// Se Tipo Firma è valorizzato, si imposta come metadato del workflow
			if (idTipoFirma > 0) {
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_FIRMA_FN_METAKEY), idTipoFirma);
			}

			// Metadati Elenco Libro Firma e ID Tipo Assegnazione
			if (elencoLibroFirma != null && elencoLibroFirma.length > 0) {
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY), elencoLibroFirma[0].split(",")[2]);
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_LIBROFIRMA_METAKEY), elencoLibroFirma);
			}

			// Metadato Flag Renderizzato, se bozza
			if (status.isBozza()) {
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.FLAG_RENDERIZZATO_METAKEY), 1);
			}

			// Metadato ID Fascicolo
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY), fascicoli.get(0).getIdFascicolo());

			// Metadati ID ufficio coordinatore e ID utente coordinatore (gestione
			// dell'utente coordinatore se c'è la checkbox)
			if (status.isUtenteCoordinatore()) {
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UFFICIO_COORDINATORE_METAKEY), docInput.getIdUfficioCoordinatore());
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_COORDINATORE_METAKEY), docInput.getIdUtenteCoordinatore());
			}

			// ### AVVIO NUOVO WORKFLOW FILENET
			wobNumber = avviaNuovoWorkflow(status, null, docInput, utente, metadatiWorkflow, fpeh, con);
		} else {
			final boolean isAssegnazioneFirmaSiglaVisto = isAssegnazioneFirmaSiglaVisto(status);

			if (status.getIterManualeSpedizione() && status.isSpedisciMailDestElettronici()) {
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_SPEDIZIONE_METAKEY), 2L);
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.IS_DEST_CARTACEI_METAKEY), 0L);
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.SPED_CART_METAKEY), 0L);
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.SPED_ELE_METAKEY), 1L);
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.SPED_FORZATA_METAKEY), 0L);
			} else if (status.getIterManualeSpedizione()) {
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_SPEDIZIONE_METAKEY), 1L);
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.IS_DEST_CARTACEI_METAKEY), 1L);
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.SPED_CART_METAKEY), 1L);
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.SPED_ELE_METAKEY), 0L);
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.SPED_FORZATA_METAKEY), 1L);
			}

			// Impostazione dei metadati comuni alle seguenti casistiche
			if (isAssegnazioneFirmaSiglaVisto || !status.getAssegnazioniFascicolo().isEmpty() || status.isPrecensito() || status.isDocContributoEsterno()) {
				// Metadato Motivazione Assegnazione
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), docInput.getMotivoAssegnazione());

				// Metadato ID Formato Documento
				if (status.isDocEntrata()) {
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_FORMATO_DOCUMENTO), docInput.getFormatoDocumentoEnum().getId().intValue());
				}

				if (!status.isPrecensito()) {
					// Metadati Elenco Conoscenza, Elenco Conoscenza Storico, Elenco Contributi,
					// Elenco Contributi Storico
					aggiungiMetadatiAssegnazioniConoscenzaContributo(metadatiWorkflow, status.getAssegnazioniConoscenza(), status.getAssegnazioniContributo());

					// Metadato Item Assegnatari (elenco assegnazioni)
					final String[] elencoAssegnazioni = DocumentoUtils.compattaAssegnazioni(status.getAssegnazioniCompetenza(), status.getAssegnazioniConoscenza(),
							status.getAssegnazioniContributo(), status.getAssegnazioniFirma(), status.getAssegnazioniSigla(), status.getAssegnazioniVisto(),
							status.getAssegnazioniFirmaMultipla());
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ITEM_ASSEGNATARI_METAKEY), elencoAssegnazioni);
				}
			}

			if (isAssegnazioneFirmaSiglaVisto) {
				LOGGER.info("gestisciWorkflows -> Avvio workflow di un documento con assegnazioni Firma/Sigla/Visto/Firma Multipla per il documento: " + idDocumento);

				docInput.setTipoAssegnazioneEnum(status.getAssegnazioniFascicolo().get(0).getTipoAssegnazione());

				// Metadato Elenco Libro Firma
				final String[] elencoLibroFirma = new String[status.getAssegnazioniFascicolo().size()];
				for (int index = 0; index < status.getAssegnazioniFascicolo().size(); index++) {
					final AssegnazioneWorkflowDTO assegnazione = status.getAssegnazioniFascicolo().get(index);
					final String libroFirma = assegnazione.getIdUfficioAssegnatario() + "," + assegnazione.getIdUtenteAssegnatario();
					elencoLibroFirma[index] = libroFirma;
				}
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_LIBROFIRMA_METAKEY), elencoLibroFirma);

				// Metadato Flag Renderizzato, se bozza
				if (status.isBozza()) {
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.FLAG_RENDERIZZATO_METAKEY), 1);
				}

				// Metadato ID Fascicolo
				final FascicoloRedFnDTO fascicolo = !status.getAssegnazioniFascicolo().isEmpty() ? status.getAssegnazioniFascicolo().get(0).getFascicolo() : null;
				if (fascicolo != null) {
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY), fascicolo.getIdFascicolo());
				}

				// Metadati ID Tipo Assegnazione
				if (!status.isDocEntrata()) {
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY), docInput.getTipoAssegnazioneEnum().getId());
				}

				// ### AVVIO NUOVO WORKFLOW FILENET
				wobNumber = avviaNuovoWorkflow(status, null, docInput, utente, metadatiWorkflow, fpeh, con);
			} else if (!status.getAssegnazioniFascicolo().isEmpty()) {
				LOGGER.info("gestisciWorkflows -> Avvio workflow di un documento Standard Generico per il documento: " + idDocumento);

				for (final AssegnazioneWorkflowDTO assegnazioneWorkflow : status.getAssegnazioniFascicolo()) {
					final Long idUfficioDestinatario = assegnazioneWorkflow.getIdUfficioAssegnatario();
					final Long idUtenteDestinatario = assegnazioneWorkflow.getIdUtenteAssegnatario();
					final int idTipoAssegnazione = assegnazioneWorkflow.getTipoAssegnazione().getId();
					final FascicoloRedFnDTO fascicolo = assegnazioneWorkflow.getFascicolo();

					for (final AssegnazioneWorkflowDTO assegnazioneWorkflowRif : status.getAssegnazioniFascicolo()) {
						final FascicoloRedFnDTO fascicoloRif = assegnazioneWorkflowRif.getFascicolo();
						if (!fascicolo.getIdFascicolo().equals(fascicoloRif.getIdFascicolo())) {
							riferimentoDAO.insertRiferimentoFascicolo(NumberUtils.toInt(fascicolo.getIdFascicolo()), NumberUtils.toInt(fascicoloRif.getIdFascicolo()),
									TipologiaRiferimento.FASCICOLO, con);
						}
					}

					aggiungiMetadatiAssegnazione(status, metadatiWorkflow, idUfficioDestinatario, idUtenteDestinatario, idTipoAssegnazione);

					// Metadato ID Fascicolo
					if (fascicolo != null) {
						metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY), fascicolo.getIdFascicolo());
					}

					// Il boolean è diverso da null solo per doc in ingresso con AOO configurate per
					// l'assegnazione indiretta
					if (TipoAssegnazioneEnum.COMPETENZA.getId() == idTipoAssegnazione && utente.getIdNodoAssegnazioneIndiretta() != null
							&& utente.getIdNodoAssegnazioneIndiretta() != 0 && docInput.isAssegnazioneIndiretta() != null) {
						metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ASSEGNAZIONE_INDIRETTA_METAKEY),
								Boolean.TRUE.equals(docInput.isAssegnazioneIndiretta()) ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue());
					}
					if (utente.isUcb()) {
						metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.IN_LAVORAZIONE_METAKEY), BooleanFlagEnum.NO.getIntValue());
					}
					// ### AVVIO NUOVO WORKFLOW FILENET
					wobNumber = avviaNuovoWorkflow(status, null, docInput, utente, metadatiWorkflow, fpeh, con);

					// Si aggiornano le security del riservato
					if (Boolean.TRUE.equals(docInput.getIsRiservato())) {
						securitySRV.generateRiservatoSecurity(utente, String.valueOf(idDocumento), wobNumber, fceh, fpeh, con);
					}
				}
			} else if (status.isPrecensito()) {
				LOGGER.info("gestisciWorkflows -> Avvio workflow di un documento Precensito per il documento: " + idDocumento);

				if (status.getAssegnazioniCompetenza().length > 0) {
					String assegnazione = status.getAssegnazioniCompetenza()[0];
					if (assegnazione.endsWith(",")) {
						assegnazione += "0";
					}
					final String[] assegnatari = assegnazione.split(",");
					final Long idUfficioDestinatario = Long.parseLong(assegnatari[0]);
					final Long idUtenteDestinatario = Long.parseLong(assegnatari[1]);

					aggiungiMetadatiAssegnazione(status, metadatiWorkflow, idUfficioDestinatario, idUtenteDestinatario, TipoAssegnazioneEnum.COMPETENZA.getId());
				}

				// ### AVVIO NUOVO WORKFLOW FILENET
				wobNumber = avviaNuovoWorkflow(status, null, docInput, utente, metadatiWorkflow, fpeh, con);
			} else if (status.isDocContributoEsterno()) {
				LOGGER.info("gestisciWorkflows -> Avvio workflow di un documento Contributo Esterno per il documento: " + idDocumento);

				// Metadato Contributo Esterno
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.CONTRIBUTO_ESTERNO), docInput.getDestinatarioContributoEsterno() != null ? 1 : 0);

				// Metadato ID Fascicolo
				if (fascicoli.get(0) != null) {
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY), fascicoli.get(0).getIdFascicolo());
				}

				// Metadato ID Nodo Destinatario
				if (status.getIdUfficioDestinatarioWorkflow() != null) {
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY), status.getIdUfficioDestinatarioWorkflow());
				} else {
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY), utente.getIdUfficio());
				}
				// Metadato ID Utente Destinatario
				if (status.getIdUtenteDestinatarioWorkflow() != null) {
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY), status.getIdUtenteDestinatarioWorkflow());
				} else {
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY), utente.getId());
				}

				// Metadati ID Tipo Assegnazione
				if (!status.isDocEntrata()) {
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY), TipoAssegnazioneEnum.COMPETENZA.getId());
				}

				// ### AVVIO NUOVO WORKFLOW FILENET
				wobNumber = avviaNuovoWorkflow(status, null, docInput, utente, metadatiWorkflow, fpeh, con);
			}

		}

		docInput.setWobNumber(wobNumber);

		// ### AVANZAMENTO DEI WORKFLOW DI RISPOSTA -> START
		gestisciWorkflowsAllacci(status, idDocumento, docInput.getAllacci(), utente, fceh, fpeh, con);
		// ### AVANZAMENTO DEI WORKFLOW DI RISPOSTA -> END

		// ### CONTRIBUTO ESTERNO -> AVANZAMENTO DEL WORKFLOW CORRENTE (DOCUMENTO DI
		// PARTENZA) A "IN SOSPESO" -> START
		// Se si tratta di un Contributo Esterno, il workflow del documento di partenza
		// deve finire nella coda "IN SOSPESO"
		gestisciWorkflowContributoEsterno(status, wob, utente, fpeh);
		// ### CONTRIBUTO ESTERNO -> AVANZAMENTO DEL WORKFLOW CORRENTE (DOCUMENTO DI
		// PARTENZA) A "IN SOSPESO" -> END

		LOGGER.info("gestisciWorkflows -> END. Documento: " + idDocumento);
	}

	/**
	 * @param status
	 * @param idDocumento
	 * @param allacci
	 * @param utente
	 * @param fceh
	 * @param fpeh
	 * @param con
	 */
	private void gestisciWorkflowsAllacci(final SalvaDocumentoRedStatusDTO status, final int idDocumento, final List<RispostaAllaccioDTO> allacci, final UtenteDTO utente,
			final IFilenetCEHelper fceh, final FilenetPEHelper fpeh, final Connection con) {
		Map<String, Object> metadatiWorkflow;

		if (!CollectionUtils.isEmpty(allacci)) {
			// Si aggiornano le security di tutti i documenti allacciati
			securitySRV.aggiornaSecurityDocumentiAllacciati(utente, idDocumento, status.getSecurityDocAllacciati(), status.getSecurityFascicoloDocAllacciati(), fceh, con);

			for (final RispostaAllaccioDTO allaccio : allacci) {
				ResponsesRedEnum responseAllaccio = null;
				metadatiWorkflow = null;

				if (!utente.isUcb()) {
					// Caso standard per AOO NON UCB
					if (Boolean.TRUE.equals(allaccio.getDocumentoDaChiudere())) {
						responseAllaccio = ResponsesRedEnum.PREDISPONI_DOCUMENTO;
					}

				} else {
					// Se l'AOO è un UCB e l'allaccio:
					// 1) NON è il principale
					// 2) è da chiudere...
					if (!allaccio.isPrincipale() && Boolean.TRUE.equals(allaccio.getDocumentoDaChiudere())) {

						// Si avanza il workflow dell'allaccio con la response PREDISPONI_RESTITUZIONE
						// se l'allaccio è di tipo "Restituzione"...
						if (TipoAllaccioEnum.RESTITUZIONE.equals(allaccio.getTipoAllaccioEnum())) {
							responseAllaccio = ResponsesRedEnum.PREDISPONI_RESTITUZIONE;
							// ...altrimenti, si avanza con la response RISPONDI INGRESSO
						} else {
							responseAllaccio = ResponsesRedEnum.RISPONDI_INGRESSO;
						}

						// ...altrimenti, se l'allaccio è principale, è sempre da chiudere e si procede
						// a farlo con la response corrispondente al tipo di allaccio
					} else if (allaccio.isPrincipale()) {

						switch (allaccio.getTipoAllaccioEnum()) {
						case RISPOSTA:
							responseAllaccio = ResponsesRedEnum.RISPONDI_INGRESSO;
							break;
						case INOLTRO:
							responseAllaccio = ResponsesRedEnum.INOLTRA_INGRESSO;
							break;
						case VISTO:
							responseAllaccio = ResponsesRedEnum.PREDISPONI_VISTO;
							// Valorizzazione del metadato "In Lavorazione" per spostare il documento nella
							// coda omonima
							metadatiWorkflow = new HashMap<>();
							metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.IN_LAVORAZIONE_METAKEY), BooleanFlagEnum.SI.getIntValue());
							break;
						case OSSERVAZIONE:
							responseAllaccio = ResponsesRedEnum.PREDISPONI_OSSERVAZIONE;
							// Valorizzazione del metadato "In Lavorazione" per spostare il documento nella
							// coda omonima
							metadatiWorkflow = new HashMap<>();
							metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.IN_LAVORAZIONE_METAKEY), BooleanFlagEnum.SI.getIntValue());
							break;
						case RICHIESTA_INTEGRAZIONE:
							responseAllaccio = ResponsesRedEnum.RICHIESTA_INTEGRAZIONI;
							break;
						case RESTITUZIONE:
							responseAllaccio = ResponsesRedEnum.PREDISPONI_RESTITUZIONE;
							break;
						case RELAZIONE_POSITIVA:
		        			responseAllaccio = ResponsesRedEnum.PREDISPONI_RELAZIONE_POSITIVA;
		        			// Valorizzazione del metadato "In Lavorazione" per spostare il documento nella coda omonima
		        			metadatiWorkflow = new HashMap<>();
		        			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.IN_LAVORAZIONE_METAKEY), BooleanFlagEnum.SI.getIntValue());
		        			break;
						case RELAZIONE_NEGATIVA:
							responseAllaccio = ResponsesRedEnum.PREDISPONI_RELAZIONE_NEGATIVA;
		        			// Valorizzazione del metadato "In Lavorazione" per spostare il documento nella coda omonima
		        			metadatiWorkflow = new HashMap<>();
		        			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.IN_LAVORAZIONE_METAKEY), BooleanFlagEnum.SI.getIntValue());
		        			break;
						default:
							break;
						}
					}
				}

				if (responseAllaccio != null) {
					// Si ottengono i workflow dell'allaccio
					final int idDocAllacciato = Integer.parseInt(allaccio.getIdDocumentoAllacciato());
					final VWWorkObject workflowAllaccioCompetenza = fpeh.getWorkflowPrincipale("" + idDocAllacciato, utente.getFcDTO().getIdClientAoo());

					// Se è stato trovato un workflow di assegnazione per competenza, lo si avanza
					// con la response calcolata
					if (workflowAllaccioCompetenza != null) {
						// In questo caso, il workflow potrebbe trovarsi nella coda In Sospeso: va
						// quindi spostato in Da Lavorare
						// prima di poter sollecitare la response calcolata
						if (TipoAllaccioEnum.RESTITUZIONE.equals(allaccio.getTipoAllaccioEnum()) && allaccio.isPrincipale()
								&& DocumentQueueEnum.SOSPESO.getName().equals(workflowAllaccioCompetenza.getCurrentQueueName())) {
							fpeh.nextStep(workflowAllaccioCompetenza, null, ResponsesRedEnum.SPOSTA_IN_LAVORAZIONE.getResponse());
						}
						// ### AVANZAMENTO DEL WORKFLOW
						fpeh.nextStep(workflowAllaccioCompetenza, metadatiWorkflow, responseAllaccio.getResponse());
					}

				}
			}
		}
	}

	/**
	 * @param status
	 * @param wob
	 * @param utente
	 * @param fpeh
	 */
	private void gestisciWorkflowContributoEsterno(final SalvaDocumentoRedStatusDTO status, final VWWorkObject wob, final UtenteDTO utente, final FilenetPEHelper fpeh) {
		if (wob != null && status.isDocContributoEsterno()) {
			final Map<String, Object> metadatiWorkFlow = new HashMap<>();
			if (status.getIdUfficioMittenteWorkflow() != null) {
				metadatiWorkFlow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), status.getIdUfficioMittenteWorkflow());
			} else {
				metadatiWorkFlow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), utente.getIdUfficio());
			}
			if (status.getIdUtenteMittenteWorkflow() != null) {
				metadatiWorkFlow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), status.getIdUtenteMittenteWorkflow());
			} else {
				metadatiWorkFlow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId());
			}

			fpeh.nextStep(wob, metadatiWorkFlow, ResponsesRedEnum.METTI_IN_SOSPESO.getResponse());
		}
	}

	/**
	 * @param status
	 * @param docInput
	 * @param utente
	 * @param assegnazioniFirma
	 * @param idAllegatiDaFirmare
	 * @param tipoFirma
	 * @param fileRevisionNumber
	 * @param principale
	 * @param aggiornaFirmaPdf
	 * @param trasformazioneAsincrona
	 * @param tipoAssegnazione
	 * @param idAllegatiConFirmaVisibile
	 * @param con
	 */
	private void eseguiTrasformazionePDF(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente,
			final String[] assegnazioniFirma, final List<String> idAllegatiDaFirmare, final Integer tipoFirma, final Integer fileRevisionNumber, final boolean principale,
			final boolean aggiornaFirmaPdf, final boolean trasformazioneAsincrona, final TipoAssegnazioneEnum tipoAssegnazione, final List<String> idAllegatiConFirmaVisibile,
			final Connection con) {
		// Si esegue la trasformazione PDF asincrona senza la protocollazione P7M
		eseguiTrasformazionePDF(status, docInput, utente, assegnazioniFirma, idAllegatiDaFirmare, tipoFirma, fileRevisionNumber, principale, aggiornaFirmaPdf,
				trasformazioneAsincrona, false, tipoAssegnazione, idAllegatiConFirmaVisibile, con);
	}

	/**
	 * @param status
	 * @param docInput
	 * @param utente
	 * @param assegnazioniFirma
	 * @param idAllegatiDaFirmare
	 * @param tipoFirma
	 * @param fileRevisionNumber
	 * @param principale
	 * @param aggiornaFirmaPdf
	 * @param trasformazioneAsincrona
	 * @param protocollazioneP7M
	 * @param tipoAssegnazione
	 * @param con
	 */
	private void eseguiTrasformazionePDF(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente,
			final String[] assegnazioniFirma, final List<String> idAllegatiDaFirmare, final Integer tipoFirma, final Integer fileRevisionNumber, final boolean principale,
			final boolean aggiornaFirmaPdf, final boolean trasformazioneAsincrona, final boolean protocollazioneP7M, final TipoAssegnazioneEnum tipoAssegnazione,
			final List<String> idAllegatiConFirmaVisibile, final Connection con) {
		List<String> idAllegatiDaTrasformare = null;

		final List<DocumentoRedFnDTO> allegatiDoc = status.getDocDaSalvare().getAllegati();
		if (!CollectionUtils.isEmpty(allegatiDoc)) {
			idAllegatiDaTrasformare = new ArrayList<>(allegatiDoc.size());
			for (final DocumentoRedFnDTO allegato : allegatiDoc) {
				idAllegatiDaTrasformare.add(allegato.getDocumentTitle());
			}
		}

		eseguiTrasformazionePDF(status, docInput.getDocumentTitle(), utente, assegnazioniFirma, idAllegatiDaTrasformare, idAllegatiDaFirmare, tipoFirma, fileRevisionNumber,
				principale, aggiornaFirmaPdf, trasformazioneAsincrona, protocollazioneP7M, tipoAssegnazione, idAllegatiConFirmaVisibile, con);
	}

	/**
	 * @param status
	 * @param idDocumento
	 * @param utente
	 * @param assegnazioniFirma
	 * @param idAllegatiDaTrasformare
	 * @param idAllegatiDaFirmare
	 * @param tipoFirma
	 * @param fileRevisionNumber
	 * @param principale
	 * @param aggiornaFirmaPdf
	 * @param trasformazioneAsincrona
	 * @param tipoAssegnazione
	 * @param idAllegatiConFirmaVisibile
	 * @param con
	 */
	private void eseguiTrasformazionePDF(final SalvaDocumentoRedStatusDTO status, final String idDocumento, final UtenteDTO utente, final String[] assegnazioniFirma,
			final List<String> idAllegatiDaTrasformare, final List<String> idAllegatiDaFirmare, final Integer tipoFirma, final Integer fileRevisionNumber,
			final boolean principale, final boolean aggiornaFirmaPdf, final boolean trasformazioneAsincrona, final TipoAssegnazioneEnum tipoAssegnazione,
			final List<String> idAllegatiConFirmaVisibile, final Connection con) {
		eseguiTrasformazionePDF(status, idDocumento, utente, assegnazioniFirma, idAllegatiDaTrasformare, idAllegatiDaFirmare, tipoFirma, fileRevisionNumber, principale,
				aggiornaFirmaPdf, trasformazioneAsincrona, false, tipoAssegnazione, idAllegatiConFirmaVisibile, con);
	}

	/**
	 * @param status
	 * @param idDocumento
	 * @param utente
	 * @param assegnazioniFirma
	 * @param idAllegatiDaTrasformare
	 * @param idAllegatiDaFirmare
	 * @param tipoFirma
	 * @param fileRevisionNumber
	 * @param principale
	 * @param aggiornaFirmaPdf
	 * @param trasformazioneAsincrona
	 * @param protocollazioneP7M
	 * @param tipoAssegnazione
	 * @param idAllegatiConFirmaVisibile
	 * @param con
	 * @param firstVersionOnNPS 
	 */
	private void eseguiTrasformazionePDF(final SalvaDocumentoRedStatusDTO status, final String idDocumento, final UtenteDTO utente, final String[] assegnazioniFirma,
			final List<String> idAllegatiDaTrasformare, final List<String> idAllegatiDaFirmare, final Integer tipoFirma, final Integer fileRevisionNumber,
			final boolean principale, final boolean aggiornaFirmaPdf, final boolean trasformazioneAsincrona, final boolean protocollazioneP7M,
			final TipoAssegnazioneEnum tipoAssegnazione, final List<String> idAllegatiConFirmaVisibile, final Connection con) {
		LOGGER.info("eseguiTrasformazionePDF -> START. Documento: " + idDocumento);
		// Impostazione dei parametri per il job di Trasformazione PDF
		final CodaTrasformazioneParametriDTO parametri = new CodaTrasformazioneParametriDTO();
		parametri.setIdDocumento(idDocumento);
		parametri.setIdNodo(utente.getIdUfficio());
		parametri.setIdUtente(utente.getId());
		parametri.setFirmatariString(assegnazioniFirma);
		parametri.setPrincipale(principale);
		parametri.setIdCoda(null);
		parametri.setFileRevisionNumber(fileRevisionNumber);
		parametri.setProtocollazioneP7M(protocollazioneP7M);
		parametri.setFlagFirmaCopiaConforme(status.isFirmaCopiaConforme());
		parametri.setAggiornaFirmaPDF(aggiornaFirmaPdf);
		if (tipoFirma != null) {
			parametri.setTipoFirma(tipoFirma);
		} else if (status.getIterApprovativo() != null) {
			parametri.setTipoFirma(status.getIterApprovativo().getTipoFirma());
		} else {
			parametri.setTipoFirma(IterApprovativoDTO.ITER_FIRMA_MANUALE);
		}

		if (!CollectionUtils.isEmpty(idAllegatiDaTrasformare)) {
			parametri.setAllegati(true);
			parametri.setIdAllegati(idAllegatiDaTrasformare);
		} else {
			parametri.setAllegati(false);
			parametri.setIdAllegati(null);
		}

		if (!CollectionUtils.isEmpty(idAllegatiDaFirmare)) {
			parametri.setIdAllegatiDaFirmare(idAllegatiDaFirmare);
		}

		if (tipoAssegnazione != null) {
			parametri.setIdTipoAssegnazione(tipoAssegnazione.getId());
		}

		if (status.getIdNotificaSped() != null) {
			parametri.setIdNotificaSped(status.getIdNotificaSped());
		}

		if (!CollectionUtils.isEmpty(idAllegatiConFirmaVisibile)) {
			parametri.setIdAllegatiConFirmaVisibile(idAllegatiConFirmaVisibile);
		}
		
		parametri.setFirstVersionOnNPS(status.isProtocollazioneAutomatica());

		try {
			if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
					&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[avviaTrasformazionePDF] solo trasformazioni sincrone in regime mock");
				trasformazionePDFSRV.doTransformation(parametri);
			} else {
				// Trasformazione PDF asicrona
				// Si procede con l'inserimento nella coda di trasformazione PDF del DB

				if (trasformazioneAsincrona) {
					trasformazionePDFSRV.insertSchedule(utente, parametri, this.getClass().getSimpleName(), con);
					// Trasformazione sincrona
				} else {
					trasformazionePDFSRV.doTransformation(parametri);
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la conversione in PDF del documento: " + idDocumento, e);
		}
		LOGGER.info("eseguiTrasformazionePDF -> END. Documento: " + idDocumento);
	}

	/**
	 * Metodo per la generazione della velina del documento: crea la velina in PDF e
	 * la associa al documento tramite l'annotation TEXT_PREVIEW.
	 * 
	 * @param idDocumento
	 * @param docInput
	 * @param utente
	 * @param fceh
	 */
	private void creaPreviewEImpostaAnnotationPreview(final SalvaDocumentoRedStatusDTO status, final int idDocumento, final SalvaDocumentoRedDTO docInput,
			final UtenteDTO utente, final IFilenetCEHelper fceh) {
		LOGGER.info("creaPreviewEImpostaAnnotationPreview -> START. Documento: " + idDocumento);

		try {
			// Si controlla se il documento in input possiede la velina (annotation
			// TEXT_PREVIEW)
			final Date dataInizio = new Date();
			final boolean isVelinaPresente = fceh.isDocumentoConPreview(docInput.getDocumentTitle(), utente.getIdAoo());
			final Date dataFine = new Date();
			LOGGER.info("Timing creaPreviewEImpostaAnnotationPreview -> isDocumentoConPreview (velina) per il documento: " + docInput.getDocumentTitle() + FRECCIA
					+ (dataFine.getTime() - dataInizio.getTime()) + "ms");

			// Se:
			// a) si è in modalità di creazione e il documento in input è senza content
			// oppure
			// b) il documento non ha la velina (annotation TEXT_PREVIEW).
			if ((status.isModalitaInsert() && !hasContent(docInput)) || !isVelinaPresente) {
				final SalvaDocumentoRedDTO documentoClone = (SalvaDocumentoRedDTO) DocumentoUtils.clonaObject(docInput);

				// Si crea il PDF della velina a partire dal documento in input
				final InputStream pdfInputStream = creaPdfPreview(status, documentoClone);

				// Si crea l'annotation della velina (TEXT_PREVIEW) e la si lega al documento
				impostaAnnotationPreview(status, docInput.getDocumentTitle(), docInput.getGuid(), pdfInputStream, utente.getIdAoo(), fceh);
			}
		} catch (final Exception e) {
			LOGGER.error("creaPreviewEImpostaAnnotationPreview -> Errore nella clonazione dell'oggetto o nella generazione dell'HTML per il documento: "
					+ docInput.getDocumentTitle(), e);
		}

		LOGGER.info("creaPreviewEImpostaAnnotationPreview -> END. Documento: " + idDocumento);
	}

	private static void impostaAnnotationPreview(final SalvaDocumentoRedStatusDTO status, final String documentTitle, final String guid, final InputStream previewContent,
			final Long idAoo, final IFilenetCEHelper fceh) {
		final Date dataInizio = new Date();

		final boolean isCreazione = !(status.isModalitaUpdateIbrida() && !status.isBozza() && !status.isDocInterno());

		fceh.creaAnnotation(documentTitle, guid, FilenetAnnotationEnum.TEXT_PREVIEW, previewContent, isCreazione, idAoo);

		final Date dataFine = new Date();
		LOGGER.info("Timing impostaAnnotationPreview per il documento: " + documentTitle + FRECCIA + (dataFine.getTime() - dataInizio.getTime()) + "ms");
	}

	private InputStream creaPdfPreview(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO documentoClone) throws IOException {
		InputStream pdfPreview = null;
		Date dataInizio = new Date();

		// Si lasciano solo gli assegnatari per competenza (Competenza, Firma, Sigla e
		// Visto)
		if (!CollectionUtils.isEmpty(documentoClone.getAssegnazioni())) {
			final Iterator<AssegnazioneDTO> itAssegnazioni = documentoClone.getAssegnazioni().iterator();
			while (itAssegnazioni.hasNext()) {
				final AssegnazioneDTO assegnazione = itAssegnazioni.next();
				if (!(TipoAssegnazioneEnum.isIn(assegnazione.getTipoAssegnazione(), TipoAssegnazioneEnum.COMPETENZA, TipoAssegnazioneEnum.FIRMA, TipoAssegnazioneEnum.SIGLA,
						TipoAssegnazioneEnum.VISTO, TipoAssegnazioneEnum.SPEDIZIONE, TipoAssegnazioneEnum.FIRMA_MULTIPLA))) {
					itAssegnazioni.remove();
				}
			}
		}

		TemplateDocumento templateDocumento;
		if (status.isDocEntrata()) {
			templateDocumento = new TemplateDocumentoEntrata();
		} else {
			templateDocumento = new TemplateDocumentoUscita();
		}

		// Si genera il content HTML per il documento
		final ByteArrayInputStream htmlFile = status.getHtmlFileHelper().creaDocumentoHtml(templateDocumento, documentoClone);
		Date dataFine = new Date();
		LOGGER.info("creaPdfPreview -> creaDocumentoHtml (entrata/uscita). Timing per il documento: " + documentoClone.getDocumentTitle() + FRECCIA
				+ (dataFine.getTime() - dataInizio.getTime()) + "ms");

		// Si converte il contenuto dell'HTML in PDF
		dataInizio = new Date();
		final IAdobeLCHelper adobeh = new AdobeLCHelper();
		pdfPreview = adobeh.htmlToPdf(IOUtils.toByteArray(htmlFile)); // Conversione HTML -> PDF
		dataFine = new Date();
		LOGGER.info(
				"creaPdfPreview -> htmlToPdf. Timing per il documento: " + documentoClone.getDocumentTitle() + FRECCIA + (dataFine.getTime() - dataInizio.getTime()) + "ms");

		return pdfPreview;
	}

	private void caricaDestinatariStrings(final SalvaDocumentoRedStatusDTO status, final List<DestinatarioRedDTO> destinatari, final Long idAoo) {
		status.setDestinatariString(new String[0]);
		status.setDestinatariInterniString(new String[0]);

		if (!CollectionUtils.isEmpty(destinatari)) {
			final List<String> destinatariStringList = new ArrayList<>();
			final List<String> destinatariInterniStringList = new ArrayList<>();
			String destinatarioString;
			for (final DestinatarioRedDTO destinatario : destinatari) {
				// Destinatario INTERNO
				if (TipologiaDestinatarioEnum.INTERNO.equals(destinatario.getTipologiaDestinatarioEnum())) {
					destinatarioString = destinatario.getIdNodo() + "," + destinatario.getIdUtente() + "," + destinatario.getContatto().getAliasContatto() + ","
							+ TipologiaDestinatarioEnum.INTERNO.getTipologia() + "," + destinatario.getModalitaDestinatarioEnum().name();
					final String assDestinatarioString = destinatario.getIdNodo() + "," + destinatario.getIdUtente();
					destinatariInterniStringList.add(assDestinatarioString);
					// Destinatario ESTERNO
				} else {
					if (destinatario.getContatto() != null && destinatario.getContatto().getContattoID() == null) {
						// Creo il contatto
						final Contatto contattoNuovoDest = rubricaSRV.insertContattoOnTheFly(destinatario.getContatto(), idAoo) ; 
						destinatario.getContatto().setContattoID(contattoNuovoDest.getContattoID());
					} else if (destinatario.getContatto() != null && destinatario.getContatto().getContattoID() != null 
							&& (destinatario.getContatto().getOnTheFly() != null && destinatario.getContatto().getOnTheFly() == 1)) {
						destinatario.getContatto().setContattoID(null);
						final Contatto contattoNuovoDest = rubricaSRV.insertContattoOnTheFly(destinatario.getContatto(), idAoo);
						destinatario.getContatto().setContattoID(contattoNuovoDest.getContattoID());
					}
					destinatarioString = destinatario.getContatto().getContattoID() + "," + destinatario.getContatto().getAliasContatto() + ","
							+ destinatario.getMezzoSpedizioneEnum().getId() + "," + TipologiaDestinatarioEnum.ESTERNO.getTipologia() + ","
							+ destinatario.getModalitaDestinatarioEnum().name();
				}
				destinatariStringList.add(destinatarioString);
			}

			status.setDestinatariString(destinatariStringList.toArray(status.getDestinatariString()));
			status.setDestinatariInterniString(destinatariInterniStringList.toArray(status.getDestinatariInterniString()));
		}
	}

	private void caricaAssegnazioni(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput) {
		final List<AssegnazioneDTO> inputAssegnazioni = docInput.getAssegnazioni();

		status.setAssegnazioniCompetenza(DocumentoUtils.initArrayAssegnazioni(TipoAssegnazioneEnum.COMPETENZA, inputAssegnazioni));
		status.setAssegnazioniConoscenza(DocumentoUtils.initArrayAssegnazioni(TipoAssegnazioneEnum.CONOSCENZA, inputAssegnazioni));
		status.setAssegnazioniContributo(DocumentoUtils.initArrayAssegnazioni(TipoAssegnazioneEnum.CONTRIBUTO, inputAssegnazioni));

		if (!status.isDocEntrata()) {
			status.setAssegnazioniFirma(DocumentoUtils.initArrayAssegnazioni(TipoAssegnazioneEnum.FIRMA, inputAssegnazioni));
			status.setAssegnazioniSigla(DocumentoUtils.initArrayAssegnazioni(TipoAssegnazioneEnum.SIGLA, inputAssegnazioni));
			status.setAssegnazioniVisto(DocumentoUtils.initArrayAssegnazioni(TipoAssegnazioneEnum.VISTO, inputAssegnazioni));
			status.setAssegnazioniFirmaMultipla(DocumentoUtils.initArrayAssegnazioni(TipoAssegnazioneEnum.FIRMA_MULTIPLA, inputAssegnazioni));

			// Se l'iter è manuale, l'assegnazione per il responsabile di Copia Conforme si aggiunge a quelle per conoscenza
			if (status.isIterManuale()) {
				status.setAssegnazioniConoscenza(gestisciAssegnazionePerCopiaConforme(status, docInput.getAllegati(), TipoAssegnazioneEnum.CONOSCENZA));
			// Altrimenti, se l'iter è automatico, si aggiunge a quelle per firma
			} else {
				status.setAssegnazioniFirma(gestisciAssegnazionePerCopiaConforme(status, docInput.getAllegati(), TipoAssegnazioneEnum.FIRMA));
			}
		}
	}

	private String[] gestisciAssegnazionePerCopiaConforme(final SalvaDocumentoRedStatusDTO status, final List<AllegatoDTO> allegati, 
			final TipoAssegnazioneEnum tipoAssegnazione) {
		final String[] assegnazioniBase = TipoAssegnazioneEnum.FIRMA.equals(tipoAssegnazione) ? status.getAssegnazioniFirma() : status.getAssegnazioniConoscenza();
		final List<String> assegnazioniConCopiaConforme = new ArrayList<>(Arrays.asList(assegnazioniBase));

		if (!CollectionUtils.isEmpty(allegati)) {
			final Stream<String> streamAssegnazioni = Arrays.stream(assegnazioniBase);

			for (final AllegatoDTO allegato : allegati) {
				if (Boolean.TRUE.equals(allegato.getCopiaConforme())) {
					status.setFirmaCopiaConforme(true); // Indica che è stato inserito almeno un allegato flaggato per la Copia Conforme
					
					final String assegnazione = allegato.getIdUfficioCopiaConforme() + "," + allegato.getIdUtenteCopiaConforme();
					if (streamAssegnazioni.noneMatch(assegnazione::equals)) {
						assegnazioniConCopiaConforme.add(assegnazione);
					}
					break; // Il responsabile per la Copia Conforme è uguale per tutti gli allegati per i
							// quali è stata selezionata la Copia Conforme
				}
			}
		}

		final String[] assegnazioniArray = new String[assegnazioniConCopiaConforme.size()];

		return assegnazioniConCopiaConforme.toArray(assegnazioniArray);
	}

	private void impostaSecurityDocumento(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final int idDocumento, final UtenteDTO utente,
			final boolean isNodoUcp, final Connection con) {
		Collection<UtenteDTO> utentiRegistro = null;
		if (Boolean.TRUE.equals(docInput.getRegistroRiservato())) {
			final Collection<Utente> utentiRegistroRiservato = utenteDAO.getUtentiRegistroRiservato(utente.getIdAoo(), con);
			if (!utentiRegistroRiservato.isEmpty()) {
				utentiRegistro = new ArrayList<>(utentiRegistroRiservato.size());
				for (final Utente utenteRegistroRiservato : utentiRegistroRiservato) {
					utentiRegistro.add(utenteSRV.getByUsername(utenteRegistroRiservato.getUsername()));
				}
			}
		}

		final String assegnazioneCoordinatore = getAssegnazioneCoordinatore(docInput);
		final int idIter = getIdIter(docInput.getIterApprovativo());

		if (hasDocAssegnazioniIterManuale(status) || hasDocIterAutomatico(status) || status.isDocContributoEsterno()) {
			// Se ci sono assegnazioni, il documento viene inserito nella cartella
			// principale...
			status.getDocDaSalvare().setFolder(pp.getParameterByKey(PropertiesNameEnum.FOLDER_DOCUMENTI_CE_KEY));
			// Si ottengono le security del documento
			if (Boolean.TRUE.equals(docInput.getRegistroRiservato())) {
				status.setSecurityDoc(securitySRV.getSecurityPerRegistroGiornaliero(utentiRegistro, con));
			} else {
				final Date dataInizio = new Date();
				final Boolean aggiungiDelegato = aggiungiUtenteDelegato(status);
				// Security per il documento
				status.setSecurityDoc(securitySRV.getSecurityPerDocumento(String.valueOf(idDocumento), utente, status.getAssegnazioniCompetenza(),
						status.getAssegnazioniConoscenza(), status.getAssegnazioniContributo(), status.getAssegnazioniFirma(), status.getAssegnazioniSigla(),
						status.getAssegnazioniVisto(), status.getAssegnazioniFirmaMultipla(), utente.getIdUfficio(), utente.getId(), true, status.isDocEntrata(), idIter,
						docInput.getIsRiservato(), assegnazioneCoordinatore, aggiungiDelegato, isNodoUcp, null, con));

				// Security per i documenti allacciati
				status.setSecurityDocAllacciati(securitySRV.getSecurityDocumentoPerDocumentiAllacciati(String.valueOf(idDocumento), utente, status.getAssegnazioniCompetenza(),
						status.getAssegnazioniConoscenza(), status.getAssegnazioniContributo(), status.getAssegnazioniFirma(), status.getAssegnazioniSigla(),
						status.getAssegnazioniVisto(), status.getAssegnazioniFirmaMultipla(), utente.getIdUfficio(), utente.getId(), true, idIter, false,
						assegnazioneCoordinatore, aggiungiDelegato, null, con));

				status.setSecurityFascicoloDocAllacciati(securitySRV.getSecurityDocumentoPerDocumentiAllacciati(String.valueOf(idDocumento), utente,
						status.getAssegnazioniCompetenza(), status.getAssegnazioniConoscenza(), status.getAssegnazioniContributo(), status.getAssegnazioniFirma(),
						status.getAssegnazioniSigla(), status.getAssegnazioniVisto(), status.getAssegnazioniFirmaMultipla(), utente.getIdUfficio(), utente.getId(), true,
						idIter, false, assegnazioneCoordinatore, aggiungiDelegato, null, con));

				final Date dataFine = new Date();
				LOGGER.info("Timing SalvaDocumentoSRV -> securitySRV.getSecurityPerDocumento per il documento: " + idDocumento + FRECCIA
						+ (dataFine.getTime() - dataInizio.getTime()) + "ms");
			}
		} else {
			status.setCreazioneBozza(true);
			// ...altrimenti viene inserito nella cartella Bozze
			status.getDocDaSalvare().setFolder(pp.getParameterByKey(PropertiesNameEnum.FOLDER_BOZZE_CE_KEY));
			// Si ottengono le security del documento
			status.setSecurityDoc(securitySRV.getSecurityPerBozza(utente.getIdUfficio(), utente.getId(), true, con));
		}

		// Si impostano le security sul documento
		status.getDocDaSalvare().setSecurity(status.getSecurityDoc());
	}

	private static void impostaContentDocumento(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput) {
		final byte[] docContent = docInput.getContent();
		final String docContentType = docInput.getContentType();
		if (docContent != null) {
			try {
				// Si imposta il content
				if (status.isModalitaMailEntrata() && ContentType.HTML.equals(docContentType)) {
					LOGGER.info("Protocollazione della mail, il content del documento è il testo della Mail.");
					// Si ottiene il contenuto e si costruisce l'html
					final ByteArrayInputStream docContentMail = status.getHtmlFileHelper()
							.creaDocumentContentMailHTML(FileUtils.inToString(new ByteArrayInputStream(docContent), StandardCharsets.UTF_8.name()));
					status.getDocDaSalvare().setDataHandler(FileUtils.toDataHandler(docContentMail));
				} else {
					status.getDocDaSalvare().setDataHandler(FileUtils.toDataHandler(docContent));
				}

				// Si imposta il content type
				status.getDocDaSalvare().setContentType(docContentType);
			} catch (final IOException e) {
				LOGGER.error("Si è verificato un errore durante l'estrazione del content del documento.", e);
				throw new RedException("Si è verificato un errore durante l'estrazione del content del documento.", e);
			}
		}
	}

	private void impostaAllegatiNuovoDocumento(final SalvaDocumentoRedStatusDTO status, final List<AllegatoDTO> allegatiInput, final Long idAoo, final Connection con,
			final IFilenetCEHelper fceh) {
		LOGGER.info("impostaAllegatiNuovoDocumento -> START");
		// Si costruisce la lista di allegati da impostare nel documento
		List<DocumentoRedFnDTO> allegatiDocFilenet = null;
		try {
			allegatiDocFilenet = allegatoSRV.convertToAllegatiFilenet(allegatiInput, idAoo, con);
		} catch (final Exception e) {
			LOGGER.error(SI_E_VERIFICATO_UN_ERRORE_DURANTE_LA_CREAZIONE_DEGLI_ALLEGATI_DEL_DOCUMENTO, e);
			throw new RedException(SI_E_VERIFICATO_UN_ERRORE_DURANTE_LA_CREAZIONE_DEGLI_ALLEGATI_DEL_DOCUMENTO, e);
		}

		if (!CollectionUtils.isEmpty(allegatiDocFilenet)) {
			for (final DocumentoRedFnDTO allegatoDocFilenet : allegatiDocFilenet) {
				final Boolean isFormatoOriginale = (Boolean) allegatoDocFilenet.getMetadati().get(pp.getParameterByKey(PropertiesNameEnum.IS_FORMATO_ORIGINALE_METAKEY));
				final Integer idFormatoAllegato = (Integer) allegatoDocFilenet.getMetadati().get(pp.getParameterByKey(PropertiesNameEnum.FORMATO_ALLEGATO_ID_METAKEY));
				final Integer daFirmare = (Integer) allegatoDocFilenet.getMetadati().get(pp.getParameterByKey(PropertiesNameEnum.DA_FIRMARE_METAKEY));
				final String documentTitle = (String) allegatoDocFilenet.getMetadati().get(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
				final String barcode = (String) allegatoDocFilenet.getMetadati().get(pp.getParameterByKey(PropertiesNameEnum.BARCODE_METAKEY));
				final Boolean firmaVisibile = (Boolean) allegatoDocFilenet.getMetadati().get(pp.getParameterByKey(PropertiesNameEnum.FIRMA_VISIBILE_METAKEY));

				// Inserimento nella lista degli allegati da firmare
				if (Boolean.FALSE.equals(isFormatoOriginale) && (BooleanFlagEnum.SI.getIntValue().equals(daFirmare)
						|| (status.getIterPdf() != null && status.getIterPdf().isIterAutomatico()) || status.isIterManuale())) {
					status.getIdAllegatiDaFirmare().add(documentTitle);
					// Inserimento nella lista degli allegati da visualizzare col campo firma
					if (Boolean.TRUE.equals(firmaVisibile)) {
						status.getIdAllegatiConFirmaVisibile().add(documentTitle);
					}
				}

				// Si controlla il barcode se la tipologia è CARTACEO
				if (idFormatoAllegato.equals(TipoSpedizione.MEZZO_CARTACEO)) {
					final Date dateStart = new Date();
					final boolean barcodeEsistente = !StringUtils.isBlank(fceh.controllaBarcode(barcode));
					if (barcodeEsistente) {
						throw new RedException("Il barcode: " + barcode + INSERITO_E_GIA_PRESENTE_NEL_SISTEMA);
					}
					final Date dateEnd = new Date();
					LOGGER.info("Timing controllaBarcode -> " + (dateEnd.getTime() - dateStart.getTime()) + "ms");
				}
			}

			// Si impostano gli allegati nel documento da salvare
			status.getDocDaSalvare().setAllegati(allegatiDocFilenet);
		}
		LOGGER.info("impostaAllegatiNuovoDocumento -> END");
	}

	private void impostaAllegatiDocumento(final SalvaDocumentoRedStatusDTO status, final List<AllegatoDTO> allegatiInput, final Long idAoo, final Connection con) {
		LOGGER.info("impostaAllegatiDocumento -> START");
		// Si costruisce la lista di allegati da impostare nel documento
		List<DocumentoRedFnDTO> allegatiDocFilenet = null;

		try {
			allegatiDocFilenet = allegatoSRV.convertToAllegatiFilenet(allegatiInput, idAoo, con);
		} catch (final Exception e) {
			LOGGER.error(SI_E_VERIFICATO_UN_ERRORE_DURANTE_LA_CREAZIONE_DEGLI_ALLEGATI_DEL_DOCUMENTO, e);
			throw new RedException(e);
		}

		// Si impostano gli allegati nel documento da salvare
		if (!CollectionUtils.isEmpty(allegatiDocFilenet)) {
			status.getDocDaSalvare().setAllegati(allegatiDocFilenet);
		}
		LOGGER.info("impostaAllegatiDocumento -> END");
	}

	private void gestisciContributoEsterno(final int idDocumento, final UtenteDTO utente, final VWWorkObject wob, final IFilenetCEHelper fceh, final Connection con,
			final Connection dwhCon) {
		// Si inserisce il documento nel fascicolo del documento su cui è stato
		// richiesto il contributo (cioè il documento di origine)
		if (wob != null) {
			final Integer idFascicolo = (Integer) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY));
			fascicoloSRV.fascicola(String.valueOf(idFascicolo), idDocumento, TipoFascicolo.NON_AUTOMATICO, utente.getIdAoo(), fceh, con);
			LOGGER.info("Fascicolo: " + idFascicolo + " associato al documento: " + idDocumento);
		}

		// Si traccia l'evento di Richiesta Contributo
		eventoLogSRV.inserisciEventoLog(idDocumento, utente.getIdUfficio(), utente.getId(), 0L, 0L, Calendar.getInstance().getTime(), EventTypeEnum.RICHIEDI_CONTRIBUTO,
				"Richiesto contributo", 0, utente.getIdAoo(), dwhCon);
		LOGGER.info("Evento: " + EventTypeEnum.RICHIEDI_CONTRIBUTO + " inserito per il documento: " + idDocumento);
	}

	private Document creaDocumentoSuFilenet(final SalvaDocumentoRedStatusDTO status, final FascicoloRedFnDTO fascicoloRedFn, final IFilenetCEHelper fceh, final Long idAoo) {
		Document docFilenet = null;

		LOGGER.info("creaDocumentoSuFilenet -> Chiamata al servizio del IFilenetHelper per la creazione del documento in corso...");
		docFilenet = fceh.creaDocumento(status.getDocDaSalvare(), fascicoloRedFn, status.getInputMailGuid(), idAoo);
		LOGGER.info("creaDocumentoSuFilenet -> Chiamata al servizio del IFilenetHelper per la creazione del documento completata.");

		return docFilenet;
	}

	/**
	 * Trova i documenti da allacciare.
	 * @param allacci
	 * @param idDocumento
	 * @param idAoo
	 * @param connection
	 * @return lista dei documenti da allacciare
	 */
	private List<RispostaAllaccioDTO> trovaDocumentiDaAllacciare(List<RispostaAllaccioDTO> allacci, int idDocumento, Long idAoo, Connection connection) {
		List<RispostaAllaccioDTO> documentiAllacciati = allaccioDAO.getDocumentiRispostaAllaccio(idDocumento, idAoo.intValue(), connection);
		List<RispostaAllaccioDTO> documentiDaAllacciare = new ArrayList<>();
		
		if (!CollectionUtils.isEmpty(allacci)) {
			final Iterator<RispostaAllaccioDTO> itAllacci = allacci.iterator();
			while (itAllacci.hasNext()) {
				RispostaAllaccioDTO rispostaAllaccio = itAllacci.next();
				
				boolean isAllacciato = false;
				for (RispostaAllaccioDTO documentoAllacciato : documentiAllacciati) {
					if (rispostaAllaccio.getIdDocumentoAllacciato().equals(documentoAllacciato.getIdDocumentoAllacciato())) {
						isAllacciato = true;
						break;
					}
				}
				if (!isAllacciato) {
					documentiDaAllacciare.add(rispostaAllaccio);
				}
			}
		}
		
		return documentiDaAllacciare;
	}
	
	/**
	 * Salva i documenti da allacciare.
	 * @param allacci
	 * @param idDocumento
	 * @param idAoo
	 * @param connection
	 */
	private void salvaDocumentiDaAllacciare(List<RispostaAllaccioDTO> allacci, int idDocumento, Long idAoo, Connection connection) {
		if (!CollectionUtils.isEmpty(allacci)) {
			// Inserimento documenti da allacciare
			final Iterator<RispostaAllaccioDTO> itDocDaAllacciare = allacci.iterator();
			while (itDocDaAllacciare.hasNext()) {
				final RispostaAllaccioDTO rispostaAllaccio = itDocDaAllacciare.next();

				allaccioDAO.insert(idDocumento, idAoo, NumberUtils.toInt(rispostaAllaccio.getIdDocumentoAllacciato()), rispostaAllaccio.getIdTipoAllaccio(),
						rispostaAllaccio.getDocumentoDaChiudere(), rispostaAllaccio.isPrincipale(), connection);
			}
		}
	}

	private static void aggiungiAssegnazioniFascicolo(final SalvaDocumentoRedStatusDTO status, final String[] assegnazioni, final FascicoloRedFnDTO fascicolo,
			final TipoAssegnazioneEnum tipoAssegnazione) {
		for (final String assegnazione : assegnazioni) {
			aggiungiAssegnazioneFascicolo(status, assegnazione, fascicolo, tipoAssegnazione);
		}
	}

	private void aggiungiAssegnazioniFascicoloAutomatico(final SalvaDocumentoRedStatusDTO status, final String[] assegnazioni, final int idDocumento,
			final SalvaDocumentoRedDTO docInput, final UtenteDTO utente, final List<FascicoloRedFnDTO> fascicoliRedFn,
			final ListSecurityDTO securityFascicoloAutomatico, final TipoAssegnazioneEnum tipoAssegnazione, final IFilenetCEHelper fceh, final Connection con) {
		for (final String assegnazione : assegnazioni) {
			aggiungiAssegnazioneFascicoloAutomatico(status, assegnazione, idDocumento, docInput, utente, fascicoliRedFn, securityFascicoloAutomatico, tipoAssegnazione, fceh,
					con);
		}
	}

	private void aggiungiAssegnazioneFascicoloAutomatico(final SalvaDocumentoRedStatusDTO status, final String assegnazione, final int idDocumento,
			final SalvaDocumentoRedDTO docInput, final UtenteDTO utente, final List<FascicoloRedFnDTO> fascicoli,
			final ListSecurityDTO securityFascicoloAutomatico, final TipoAssegnazioneEnum tipoAssegnazione, final IFilenetCEHelper fceh, final Connection con) {
		if (!status.isPrecensito()) {
			final FascicoloRedFnDTO fascicoloAutomatico = fascicoloSRV.aggiungiFascicoloAutomatico(idDocumento, utente,
					docInput.getIndiceClassificazioneFascicoloProcedimentale(), docInput.getDescrizioneFascicoloProcedimentale(), status.getMetadatiFascicolo(),
					securityFascicoloAutomatico, fascicoli, fceh, con);

			aggiungiAssegnazioneFascicolo(status, assegnazione, fascicoloAutomatico, tipoAssegnazione);
		}
	}

	private static void aggiungiAssegnazioneFascicolo(final SalvaDocumentoRedStatusDTO status, final String assegnazione, final FascicoloRedFnDTO fascicolo,
			final TipoAssegnazioneEnum tipoAssegnazione) {
		if (fascicolo != null) {
			final AssegnazioneWorkflowDTO assegnazioneWorkflow = new AssegnazioneWorkflowDTO();
			assegnazioneWorkflow.setFascicolo(fascicolo);
			assegnazioneWorkflow.setIdUfficioAssegnatario(Long.parseLong(assegnazione.split(",")[0]));
			assegnazioneWorkflow.setIdUtenteAssegnatario(Long.parseLong(assegnazione.split(",")[1]));
			assegnazioneWorkflow.setTipoAssegnazione(tipoAssegnazione);

			status.getAssegnazioniFascicolo().add(assegnazioneWorkflow);
		}
	}

	private static void aggiungiAllAssegnazioniFascicolo(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final FascicoloRedFnDTO fascicolo) {
		if (!status.isDocEntrata() && status.isIterManuale() && TipoAssegnazioneEnum.SPEDIZIONE.equals(docInput.getTipoAssegnazioneEnum())) {
			aggiungiAssegnazioniFascicolo(status, status.getAssegnazioniCompetenza(), fascicolo, TipoAssegnazioneEnum.SPEDIZIONE);
		} else {
			aggiungiAssegnazioniFascicolo(status, status.getAssegnazioniCompetenza(), fascicolo, TipoAssegnazioneEnum.COMPETENZA);
		}
		aggiungiAssegnazioniFascicolo(status, status.getAssegnazioniFirma(), fascicolo, TipoAssegnazioneEnum.FIRMA);
		aggiungiAssegnazioniFascicolo(status, status.getAssegnazioniSigla(), fascicolo, TipoAssegnazioneEnum.SIGLA);
		aggiungiAssegnazioniFascicolo(status, status.getAssegnazioniVisto(), fascicolo, TipoAssegnazioneEnum.VISTO);
		aggiungiAssegnazioniFascicolo(status, status.getAssegnazioniVisto(), fascicolo, TipoAssegnazioneEnum.VISTO_MANUALE);
		aggiungiAssegnazioniFascicolo(status, status.getAssegnazioniFirmaMultipla(), fascicolo, TipoAssegnazioneEnum.FIRMA_MULTIPLA);
	}

	/**
	 * Gestione dell'e-mail del tab "Spedizione" della maschera di
	 * creazione/modifica.
	 * 
	 * @param status
	 * @param documentTitle
	 * @param emailSpedizione
	 * @param utente
	 * @param fceh
	 * @param con
	 * @return
	 */
	private String creaEmailSpedizioneSuFilenet(final SalvaDocumentoRedStatusDTO status, final String documentTitle, final EmailDTO emailSpedizione, final UtenteDTO utente,
			final IFilenetCEHelper fceh, final Connection con) {
		LOGGER.info("creaEmailSpedizioneSuFilenet -> START. Documento: " + documentTitle);
		String documentoMailGuid = null;
		final Long idAoo = utente.getIdAoo();
		IFilenetCEHelper fcehAdmin = null;

		try {
			fcehAdmin = getFCEHAdmin(utente.getFcDTO(), utente.getIdAoo());
			String emailGuid = null;

			// Si controlla se esiste già un'annotation (caso di bozza da salvare come
			// documento o documento che ha un destinatario elettronico)...
			final AnnotationDTO annotationTextMail = fcehAdmin.getAnnotationDocumentContent(documentTitle, FilenetAnnotationEnum.TEXT_MAIL, idAoo);
			if (annotationTextMail != null) {
				emailGuid = IOUtils.toString(annotationTextMail.getContent());
			}

			if (checkEmailSpedizioneModificata(emailGuid, emailSpedizione, fceh)) {
				// ...in caso positivo, si elimina il documento E-Mail (insieme alla relativa
				// annotation sul documento principale) da FileNet
				if (!status.isModalitaInsert()) {
					mailSRV.eliminaBozzaEmailDocumento(documentTitle, emailGuid, utente.getIdAoo(), fcehAdmin);
				}

				if (status.isSpedisciMailDestElettronici()) {
					checkEmailSpedizionePresente(true, emailSpedizione);

					final String documentTitleDocEmail = documentTitle + ".html";

					// Caricamento dei metadati
					final Map<String, Object> metadatiMail = caricaMetadatiMail(emailSpedizione, documentTitleDocEmail, TipologiaInvio.NUOVOMESSAGGIO);

					// Costruzione dell'oggetto Documento per il salvataggio della mail
					final DocumentoRedFnDTO mail = new DocumentoRedFnDTO();
					mail.setDocumentTitle(documentTitleDocEmail);
					mail.setClasseDocumentale(pp.getParameterByKey(PropertiesNameEnum.MAIL_CLASSNAME_FN_METAKEY));
					mail.setMetadati(metadatiMail);
					mail.setFolder(pp.getParameterByKey(PropertiesNameEnum.FOLDER_BOZZE_MAIL_CE_KEY));
					final byte[] testoEmail = emailSpedizione.getTesto().getBytes();
					mail.setDataHandler(FileUtils.toDataHandler(testoEmail, ContentType.HTML));
					mail.setContentType(ContentType.HTML);

					final List<DocumentoRedFnDTO> allegatiMail = getDocumentiTabSpedizione(documentTitle, emailSpedizione, idAoo, fceh, con);
					// DAG 2 - Allega alla bozza mail i documenti del fascicolo
					if (!allegatiMail.isEmpty()) {
						mail.setAllegati(allegatiMail);
					}

					// ### INSERIMENTO DELLA MAIL SU FILENET
					final Document documentoMailFilenet = fceh.creaDocumento(mail, null, null, utente.getIdAoo());
					if (documentoMailFilenet != null) {
						documentoMailGuid = documentoMailFilenet.get_Id().toString();
						LOGGER.info(
								"creaEmailSpedizioneSuFilenet -> Creazione su FileNet della mail (documento): " + documentTitle + " completata. GUID: " + documentoMailGuid);

						// ### INSERIMENTO DELL'ANNOTATION SU FILENET
						fceh.creaAnnotation(documentTitle, documentoMailGuid, FilenetAnnotationEnum.TEXT_MAIL, idAoo);
					}
				}
			} else {
				checkEmailSpedizionePresente(status.isSpedisciMailDestElettronici(), emailSpedizione);
			}
		} catch (final Exception e) {
			LOGGER.error("creaEmailSpedizioneSuFilenet -> Si è verificato un errore durante il processo di gestione dell'e-mail per i destinatari elettronici", e);
			throw new RedException("Si è verificato un errore durante il processo di gestione dell'e-mail per i destinatari elettronici");
		} finally {
			popSubject(fcehAdmin);
		}

		LOGGER.info("creaEmailSpedizioneSuFilenet -> END. Documento: " + documentTitle + ". GUID: " + documentoMailGuid);
		return documentoMailGuid;
	}

	/**
	 * @param spedisciMailDestinatariElettronici
	 * @param emailSpedizione
	 */
	private static void checkEmailSpedizionePresente(final boolean spedisciMailDestinatariElettronici, final EmailDTO emailSpedizione) {
		if (spedisciMailDestinatariElettronici && emailSpedizione == null) {
			LOGGER.error("creaEmailSpedizioneSuFilenet -> È prevista la creazione dell'e-mail per i destinatari elettronici inseriti, "
					+ "ma l'e-mail per la spedizione non è presente.");
			throw new RedException("È prevista la creazione dell'e-mail per i destinatari elettronici inseriti, " + "ma l'e-mail per la spedizione non è presente.");
		}
	}

	/**
	 * Carica i documenti selezionati del fascicolo nel tab spedizione come allegati
	 * alla mail.
	 * 
	 * @return
	 */
	private List<DocumentoRedFnDTO> getDocumentiTabSpedizione(final String idDocumento, final EmailDTO mailSpedizione, final Long idAoo, final IFilenetCEHelper fceh,
			final Connection con) {
		LOGGER.info("getDocumentiTabSpedizione -> START. Documento: " + idDocumento);
		// DAG 2 - Carica e allega alla mail i documenti del fascicolo
		final List<DocumentoRedFnDTO> allegatiList = new ArrayList<>();

		// N.B. Sono solo i selezionati nel tab Spedizione
		final List<String> allegatiTabSpedizione = mailSpedizione != null ? mailSpedizione.getIdAllegati() : null;

		if (!CollectionUtils.isEmpty(allegatiTabSpedizione)) {
			for (final String idAllegatoMail : allegatiTabSpedizione) {
				// Si recupera il documento da FileNet
				LOGGER.info("Recupero del documento : + " + idAllegatoMail + " da Filenet.");
				final Document contentDocument = fceh.getDocumentForDownload(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), idAllegatoMail);

				// Se ha il content, si conserva come allegato
				if (FilenetCEHelper.hasDocumentContentTransfer(contentDocument)) {
					final DocumentoRedFnDTO allegato = new DocumentoRedFnDTO();
					final String documentTitle = Constants.EMPTY_STRING + documentoDAO.getNextId(idAoo, con);

					final Map<String, Object> metadatiAllegatoList = new HashMap<>();
					metadatiAllegatoList.put(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), documentTitle);
					metadatiAllegatoList.put(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY),
							TrasformerCE.getMetadato(contentDocument, PropertiesNameEnum.NOME_FILE_METAKEY));
					allegato.setMetadati(metadatiAllegatoList);

					allegato.setDocumentTitle(documentTitle); // nomeFile allegato
					allegato.setClasseDocumentale(pp.getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY));

					final ContentTransfer content = FilenetCEHelper.getDocumentContentTransfer(contentDocument);
					try {
						allegato.setDataHandler(FileUtils.toDataHandler(content.accessContentStream()));
					} catch (final IOException e) {
						LOGGER.error("Si è verificato un errore nel recupero del content degli allegati del tab Spedizione.", e);
						throw new RedException("Si è verificato un errore nel recupero del content degli allegati del tab Spedizione.", e);
					}
					allegato.setContentType(content.get_ContentType());

					allegatiList.add(allegato);
				}
			}
		}

		LOGGER.info("getDocumentiTabSpedizione -> END. Documento: " + idDocumento);
		return allegatiList;
	}

	private Map<String, Object> caricaMetadatiMail(final EmailDTO mailSpedizione, final String documentTitleDocEmail, final int tipologiaInvio) {
		final Map<String, Object> metadatiList = new HashMap<>();

		// Metadati di base
		metadatiList.put(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), documentTitleDocEmail);

		// Metadati specifici dell'e-mail
		metadatiList.put(pp.getParameterByKey(PropertiesNameEnum.FROM_MAIL_METAKEY), mailSpedizione.getMittente());
		metadatiList.put(pp.getParameterByKey(PropertiesNameEnum.MITTENTE_MAIL_METAKEY), mailSpedizione.getMittente());
		if (StringUtils.isNotBlank(mailSpedizione.getDestinatari())) {
			metadatiList.put(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_EMAIL_METAKEY), mailSpedizione.getDestinatari().trim());
		}
		if (StringUtils.isNotBlank(mailSpedizione.getDestinatariCC())) {
			metadatiList.put(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_CC_EMAIL_METAKEY), mailSpedizione.getDestinatariCC().trim());
		}
		metadatiList.put(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_EMAIL_METAKEY), mailSpedizione.getOggetto());
		metadatiList.put(pp.getParameterByKey(PropertiesNameEnum.CASELLA_POSTALE_METAKEY), mailSpedizione.getMittente());
		metadatiList.put(pp.getParameterByKey(PropertiesNameEnum.TIPOLOGIA_INVIO_METAKEY), tipologiaInvio);

		return metadatiList;
	}

	/**
	 * @param status
	 * @param docInput
	 * @param idDocumento
	 * @param utente
	 * @param fceh
	 * @param con
	 */
	private void gestisciProtocollazione(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final Integer idDocumento, final UtenteDTO utente,
			final IFilenetCEHelper fceh, final Connection con) {
		// Si protocolla il documento se NON è precensito e:
		if (!status.isPrecensito()) {

			// 1) NON è già stato protocollato e:
			if (!isDocInputGiaProtocollato(docInput)) {
				if ((status.isDocEntrata() && status.isDaProtocollare()) // a) è un documento in entrata da protocollare (flag GUI "Da non
																			// protocollare"), oppure
						|| (!status.isDocEntrata() // b) NON è un documento in entrata e la protocollazione selezionata è "Non
													// Standard"
								&& MomentoProtocollazioneEnum.PROTOCOLLAZIONE_NON_STANDARD.equals(docInput.getMomentoProtocollazioneEnum()))) {
					// ###### PROTOCOLLAZIONE ######
					LOGGER.info("gestisciProtocollazione -> Documento da protocollare");
					protocollaDocumento(status, docInput, utente, fceh, con);
				}
				// ...altrimenti, se si tratta di un documento con dati di protocollo già
				// presenti in input:
			} else {
				LOGGER.info("gestisciProtocollazione -> Documento già protocollato");
				// 1) si aggiornano i metadati di protocollo del documento nel CE
				aggiornaMetadatiProtocolloCE(idDocumento, docInput, status, utente, fceh, con);

				// 2) se si tratta di una protocollazione automatica, si effettua una richiesta
				// asincrona verso NPS
				// per comunicare l'assegnatario per competenza del protocollo e cambiare lo
				// stato in IN_LAVORAZIONE
				if (TipologiaProtocollazioneEnum.isProtocolloNPS(utente.getIdTipoProtocollo()) && status.isProtocollazioneAutomatica() && status.isDocEntrata()) {
					final String numeroAnnoProtocollo = docInput.getNumeroProtocollo() + "/" + docInput.getAnnoProtocollo();
					final String[] assegnazioneCompetenza = status.getAssegnazioniCompetenza()[0].split(",");
					final Long idUfficioAssegnatario = Long.parseLong(assegnazioneCompetenza[0]);
					final Long idUtenteAssegnatario = Long.parseLong(assegnazioneCompetenza[1]);
					
					final boolean isNodoUcp = nodoDAO.isNodoUcp(utente.getIdUfficio(), con);
					final boolean escludiCreatore = status.isModalitaInsert() && status.isDocEntrata() && Boolean.TRUE.equals(docInput.getIsRiservato()) && isNodoUcp;
					
					final ListSecurityDTO securityList = securitySRV.getSecurityPerRiassegnazione(utente, docInput.getDocumentTitle(), idUtenteAssegnatario,
							idUfficioAssegnatario, getAssegnazioneCoordinatore(docInput), false, true, Boolean.TRUE.equals(docInput.getIsRiservato()), escludiCreatore, con);

					final int idNar = npsSRV.cambiaStatoProtEntrataAsync(docInput.getIdProtocollo(), numeroAnnoProtocollo, utente, Builder.IN_LAVORAZIONE,
							docInput.getDocumentTitle(), con);

					if (idNar <= 0) {
						LOGGER.warn(GESTISCI_PROTOCOLLAZIONE_ERRORE_NELL_INSERIMENTO_DELLA_RICHIESTA_ASINCRONA_VERSO_NPS
								+ " per la comunicazione del cambio stato del protocollo");
					} else if (npsSRV.aggiungiAssegnatarioPerRiassegnazioneAsync(idNar, numeroAnnoProtocollo, idUfficioAssegnatario, idUtenteAssegnatario,
							docInput.getIdProtocollo(), securityList, new Date(), utente, "si", docInput.getDocumentTitle(), con, fceh) == 0) {
						LOGGER.warn(GESTISCI_PROTOCOLLAZIONE_ERRORE_NELL_INSERIMENTO_DELLA_RICHIESTA_ASINCRONA_VERSO_NPS
								+ " per la comunicazione dell'assegnatario del protocollo");
					}
					
					//se il documento è riservato, aggiorna il flag di NPS
					if(docInput.getIsRiservato() != null && docInput.getIsRiservato()) {
						npsSRV.aggiornaRiservatezza(utente, numeroAnnoProtocollo, docInput.getIdProtocollo(), securityList, docInput.getIsRiservato(), docInput.getDocumentTitle());
					}

					TipoContestoProceduraleEnum tcp = TipoContestoProceduraleEnum.getEnumById(docInput.getCodiceFlusso());
					if(tcp != null && tcp.isAggiornaMetadatiPostProtAuto()) {
						boolean esitoAggiornamento = npsSRV.aggiornaDatiProtocollo(utente, numeroAnnoProtocollo, docInput.getIdProtocollo(), null,
								docInput.getTipologiaDocumento().getIdTipologiaDocumento(), (int) docInput.getTipoProcedimento().getTipoProcedimentoId(),
								docInput.getTipologiaDocumento().getDescrizione(), null, docInput.getDocumentTitle(), docInput.getMetadatiEstesi(), con);
						if(!esitoAggiornamento) {
							LOGGER.warn(GESTISCI_PROTOCOLLAZIONE_ERRORE_NELL_INSERIMENTO_DELLA_RICHIESTA_ASINCRONA_VERSO_NPS
									+ " per la comunicazione del tipo documento/procedimento e metadati estesi");
						}
					}
					
				}
			}

			if(docInput.getNumeroProtocollo()!=null && docInput.getAllacci()!=null && !docInput.getAllacci().isEmpty() && 
					(status.isDocEntrata() || 
							(MomentoProtocollazioneEnum.PROTOCOLLAZIONE_NON_STANDARD.equals(docInput.getMomentoProtocollazioneEnum()) && 
									status.getIterManualeSpedizione()))) { 
				String annoProtocollo = String.valueOf(docInput.getAnnoProtocollo());
				String numeroProtocollo = String.valueOf(docInput.getNumeroProtocollo());
				String numeroAnnoProtocollo = numeroProtocollo + "/" + annoProtocollo + "_" + utente.getCodiceAoo();
				String idProtocollo = docInput.getIdProtocollo();
				npsSRV.aggiungiAllacciAsync(utente, con, idProtocollo, docInput.getAllacci(), numeroAnnoProtocollo, !status.isDocEntrata(), docInput.getDocumentTitle());
			}
			
		}
	}

	/**
	 * Effettua la protocollazione del documento con NPS o PMEF e ne gestisce le
	 * operazioni successive.
	 * 
	 * @param status
	 * @param docInput
	 * @param utente
	 * @param fceh
	 * @param con
	 */
	private void protocollaDocumento(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente, final IFilenetCEHelper fceh,
			final Connection con) {
		Date dataFine = null;
		final Date dataInizio = new Date();

		final int idDocumento = NumberUtils.toInt(docInput.getDocumentTitle());
		Date dataProtocollo = null;

		// ### TIPOLOGIA PROTOCOLLAZIONE -> NPS o NPSEMERGENZA
		if (TipologiaProtocollazioneEnum.isProtocolloNPS(utente.getIdTipoProtocollo())) {
			LOGGER.info("Protocollazione NPS -> START. Documento: " + idDocumento);
			ProtocolloNpsDTO protocolloNpsDTO = null;

			if (TipologiaProtocollazioneEnum.NPS.getId().equals(utente.getIdTipoProtocollo())) {
				// ACL per security
				final String acl = npsSRV.getACLFromSecurity(status.getDocDaSalvare().getSecurity(), fceh);

				// Creazione protocollo sincrona NPS in entrata
				if (status.isDocEntrata()) {
					String idDocumentoMessaggioPostaNps = null;

					// ### MODALITA MAIL IN INGRESSO -> INTEROPERABILITA: NOTIFICA AL MITTENTE
					// DELL'AVVENUTA PROTOCOLLAZIONE DELLA MAIL IN ENTRATA -> START
					// Se si sta protocollando una mail in entrata, si recupera l'ID del documento
					// NPS corrispondente alla mail in entrata
					// per consentire a NPS di notificare al mittente della mail l'avvenuta
					// protocollazione e non interrompere il flusso di interoperabilità
					final Aoo aoo = aooDAO.getAoo(utente.getIdAoo(), con);
					if (status.isModalitaMailEntrata() && StringUtils.isNotBlank(status.getInputMailGuid()) && PostaEnum.isInteroperabile(aoo.getPostaInteropEsterna())) {

						IFilenetCEHelper fcehAdmin = null;
						try {
							fcehAdmin = getFCEHAdmin(utente.getFcDTO(), utente.getIdAoo());

							final Document emailFilenet = fcehAdmin.getDocument(fcehAdmin.idFromGuid(status.getInputMailGuid()));
							idDocumentoMessaggioPostaNps = (String) TrasformerCE.getMetadato(emailFilenet, PropertiesNameEnum.ID_MESSAGGIO_INTEROP_POSTA_NPS_MAIL_METAKEY);
						} finally {
							popSubject(fcehAdmin);
						}

					}
					// ### MODALITA MAIL IN INGRESSO -> INTEROPERABILITA: NOTIFICA AL MITTENTE
					// DELL'AVVENUTA PROTOCOLLAZIONE DELLA MAIL IN ENTRATA -> END

					protocolloNpsDTO = npsSRV.creaProtocolloEntrata(docInput, acl, utente, status.getAssegnazioniCompetenza(), status.getAssegnazioniConoscenza(),
							status.getAssegnazioniContributo(), status.getAssegnazioniFirma(), status.getAssegnazioniSigla(), status.getAssegnazioniVisto(),
							status.getAssegnazioniFirmaMultipla(), false, idDocumentoMessaggioPostaNps, con);
					LOGGER.info("Protocollazione NPS -> Creato protocollo in entrata per il documento: " + idDocumento);

					// Creazione protocollo sincrona NPS in uscita (protocollazione anticipata)
				} else if (MomentoProtocollazioneEnum.PROTOCOLLAZIONE_NON_STANDARD.equals(docInput.getMomentoProtocollazioneEnum())) {
					String indiceClassificazione = null;
					if (docInput.getFascicoloProcedimentale() != null && StringUtils.isNotBlank(docInput.getFascicoloProcedimentale().getIndiceClassificazione())) {
						indiceClassificazione = docInput.getFascicoloProcedimentale().getIndiceClassificazione();
					} else if (StringUtils.isNotBlank(docInput.getIndiceClassificazioneFascicoloProcedimentale())) {
						indiceClassificazione = docInput.getIndiceClassificazioneFascicoloProcedimentale();
					}

					protocolloNpsDTO = npsSRV.creaProtocolloUscita(docInput, indiceClassificazione, acl, utente, Builder.DA_SPEDIRE, con, fceh);

					if (docInput.getAllacci() != null && !docInput.getAllacci().isEmpty()) {
						for (final RispostaAllaccioDTO allaccio : docInput.getAllacci()) {
							if (Boolean.TRUE.equals(allaccio.getDocumentoDaChiudere()) && allaccio.getNumeroProtocollo() != null) {

								final String infoProtocollo = new StringBuilder().append(allaccio.getNumeroProtocollo()).append("/").append(allaccio.getAnnoProtocollo())
										.toString();

								final Document docIngresso = fceh.getDocumentByDTandAOO(allaccio.getIdDocumentoAllacciato(), utente.getIdAoo());
								final String idProtocollo = (String) TrasformerCE.getMetadato(docIngresso, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY);
								npsSRV.cambiaStatoProtEntrata(idProtocollo, infoProtocollo, utente, Builder.CHIUSO, String.valueOf(allaccio.getNumDocumento()), con);
							}
						}
					}
					LOGGER.info("Protocollazione NPS -> Creato protocollo in uscita per il documento: " + idDocumento);
				}

			} else {
				Connection conEmergenza = null;
				try {
					conEmergenza = setupConnection(getDataSource().getConnection(), true);

					protocolloNpsDTO = protocolliEmergenzaSRV.registraInfoEmergenza(docInput.getDocumentTitle(), utente.getIdAoo(), utente.getCodiceAoo(),
							docInput.getNumeroProtocolloEmergenza(), docInput.getAnnoProtocolloEmergenza(), docInput.getDataProtocolloEmergenza(),
							status.isDocEntrata() ? Protocollo.TIPO_PROTOCOLLO_ENTRATA : Protocollo.TIPO_PROTOCOLLO_USCITA, docInput.getOggetto(),
							docInput.getTipologiaDocumento().getDescrizione(), conEmergenza);

					commitConnection(conEmergenza);
				} catch (final Exception e) {
					LOGGER.error("Protocollazione NPS -> Si è verificato un errore durante la registrazione nel DB delle informazioni del protocollo di emergenza", e);
					rollbackConnection(conEmergenza);
				} finally {
					closeConnection(conEmergenza);
				}
			}

			if (protocolloNpsDTO == null) {
				LOGGER.error("Protocollazione NPS -> Si è verificato un errore durante la creazione del protocollo. Documento: " + idDocumento);
				throw new RedException("Si è verificato un errore durante la creazione del protocollo. Documento: " + idDocumento);
			} else {
				LOGGER.info("Protocollazione NPS -> EFFETTUATA. idProtocollo: " + protocolloNpsDTO.getIdProtocollo() + ", numero protocollo: "
						+ protocolloNpsDTO.getNumeroProtocollo() + ", anno: " + protocolloNpsDTO.getAnnoProtocollo());

				// ### UPLOAD ASINCRONO SU NPS -> START
				eseguiUploadToNps(status, protocolloNpsDTO, docInput, utente, fceh, con);
				// ### UPLOAD ASINCRONO SU NPS -> END

				dataProtocollo = new Date();
				status.setIdProtocollo(protocolloNpsDTO.getIdProtocollo());
				status.setNumeroProtocollo(protocolloNpsDTO.getNumeroProtocollo());
				status.setAnnoProtocollo(protocolloNpsDTO.getAnnoProtocollo());
				status.setDataProtocollo(DateUtils.formatDataByUSLocale(dataProtocollo.toString(), DateUtils.DD_MM_YYYY_HH_MM_SS));

				// Aggiornamento del documento con i dati del protocollo NPS
				fceh.aggiornaDocumentoProtocollo(utente.getId(), utente.getIdUfficio(), utente.getIdAoo(),
						status.isDocEntrata() ? Protocollo.TIPO_PROTOCOLLO_ENTRATA : Protocollo.TIPO_PROTOCOLLO_USCITA, status.getNumeroProtocollo(), status.getIdProtocollo(),
						protocolloNpsDTO.getOggetto(), protocolloNpsDTO.getDescrizioneTipologiaDocumento(), status.getAnnoProtocollo(), status.getDataProtocollo(),
						status.getDocDaSalvare().getDocumentTitle(), utente.getIdTipoProtocollo());
				LOGGER.info("Protocollazione NPS -> Eseguito aggiornamento dei dati del protocollo NPS per il documento: " + idDocumento);
			}

			dataFine = new Date();
			LOGGER.info("Protocollazione NPS -> Timing protocollazione per il documento: " + idDocumento + FRECCIA + (dataFine.getTime() - dataInizio.getTime()) + "ms");
			LOGGER.info("Protocollazione NPS -> END. Documento: " + idDocumento);
			// ### TIPOLOGIA PROTOCOLLAZIONE -> MEF
		} else if (TipologiaProtocollazioneEnum.MEF.getId().equals(utente.getIdTipoProtocollo())) {
			LOGGER.info("Protocollazione MEF -> START. Documento: " + idDocumento);
			ProtocolloDTO protocolloMef = null;

			// Si recuperano i parametri del protocollo MEF
			final SequKDTO parametriPMef = parametriProtocolloDAO.getParametriProtocolloByIdAoo(utente.getIdAoo(), con);

			// Si recupera la lista dei destinatari
			final boolean controlloIter = (status.isMozione() || status.isDocUscita());

			// isDocEntrata -> TIPO_PROTOCOLLO_ENTRATA
			if (status.isDocEntrata()) {
				protocolloMef = pMefSRV.creaProtocolloMEFEntrata(docInput.getMittenteContatto(), docInput.getOggetto(), parametriPMef, utente);
				// Altrimenti -> TIPO_PROTOCOLLO_USCITA
			} else {
				final List<String> destinatariProtocolloMef = pMefSRV.getListaDestinatari(utente, status.getAssegnazioniCompetenza(), status.getAssegnazioniConoscenza(),
						status.getAssegnazioniContributo(), status.getAssegnazioniFirma(), status.getAssegnazioniSigla(), status.getAssegnazioniVisto(),
						status.getAssegnazioniFirmaMultipla(), controlloIter, con);

				// N.B. Il primo destinatario è quello in conoscenza
				protocolloMef = pMefSRV.creaProtocolloMEFUscita(docInput.getOggetto(), parametriPMef, destinatariProtocolloMef.get(0), utente);
			}

			if (protocolloMef == null) {
				LOGGER.error("Protocollazione MEF -> Si è verificato un errore durante la creazione del protocollo. Documento: " + idDocumento);
				throw new RedException("Protocollazione MEF -> Si è verificato un errore durante la creazione del protocollo. Documento: " + idDocumento);
			} else {
				LOGGER.info("Protocollazione MEF -> EFFETTUATA. idProtocollo: " + protocolloMef.getIdProtocollo() + ", numero protocollo: "
						+ protocolloMef.getNumeroProtocollo() + ", anno: " + protocolloMef.getAnnoProtocollo());

				dataProtocollo = new Date();
				status.setIdProtocollo(String.valueOf(protocolloMef.getNumeroProtocollo())); // idProtocollo uguale a numeroProtocollo
				if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
						&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
					status.setIdProtocollo(String.valueOf(protocolloMef.getIdProtocollo()));
				}
				status.setNumeroProtocollo(protocolloMef.getNumeroProtocollo());
				status.setAnnoProtocollo(protocolloMef.getAnnoProtocollo());
				status.setDataProtocollo(DateUtils.formatDataByUSLocale(dataProtocollo.toString(), DateUtils.DD_MM_YYYY_HH_MM_SS));

				// Aggiornamento del documento con i dati del protocollo MEF
				fceh.aggiornaDocumentoProtocollo(utente.getId(), utente.getIdUfficio(), utente.getIdAoo(),
						status.isDocEntrata() ? Constants.Protocollo.TIPO_PROTOCOLLO_ENTRATA : Constants.Protocollo.TIPO_PROTOCOLLO_USCITA, status.getNumeroProtocollo(), null,
						null, null, status.getAnnoProtocollo(), status.getDataProtocollo(), status.getDocDaSalvare().getDocumentTitle(), utente.getIdTipoProtocollo());
				LOGGER.info("Protocollazione MEF -> Aggiornamento dei dati del protocollo MEF per il documento: " + idDocumento);
			}

			dataFine = new Date();
			LOGGER.info("Protocollazione MEF -> Timing protocollazione per il documento: " + idDocumento + FRECCIA + (dataFine.getTime() - dataInizio.getTime()) + "ms");
			LOGGER.info("Protocollazione MEF -> END. Documento: " + idDocumento);
		}

		if (StringUtils.isNotBlank(status.getIdProtocollo()) && StringUtils.isNotBlank(status.getAnnoProtocollo())) {
			docInput.setIdProtocollo(status.getIdProtocollo());
			docInput.setNumeroProtocollo(status.getNumeroProtocollo());
			docInput.setAnnoProtocollo(Integer.parseInt(status.getAnnoProtocollo()));
			docInput.setDataProtocollo(dataProtocollo);
			status.setDocumentoProtocollato(true);
		}
	}

	/**
	 * Esegue l'upload asincrono del documento principale e degli eventuali allegati
	 * verso NPS.
	 * 
	 * @param status
	 * @param protocolloNpsDTO
	 * @param docInput
	 * @param utente
	 * @param fceh
	 * @param con
	 */
	private void eseguiUploadToNps(final SalvaDocumentoRedStatusDTO status, final ProtocolloNpsDTO protocolloNpsDTO, final SalvaDocumentoRedDTO docInput,
			final UtenteDTO utente, final IFilenetCEHelper fceh, final Connection con) {
		final NpsConfiguration npsConfUtente = npsConfigurationDAO.getByIdAoo(utente.getIdAoo().intValue(), con);

		// N.B. La stringa infoProtocollo è costruita nel seguente modo:
		// NUMPROT/ANNOPROT_CODICEAOO
		final String infoProtocollo = new StringBuilder().append(protocolloNpsDTO.getNumeroProtocollo()).append("/").append(protocolloNpsDTO.getAnnoProtocollo()).append("_")
				.append(npsConfUtente.getCodiceAoo()).toString();

		// Si esegue l'upload asincrono del documento principale inserendo il relativo
		// record nel DB
		try {
			InputStream is = new ByteArrayInputStream(new byte[0]);
			if (status.getDocDaSalvare().getDataHandler() != null && status.getDocDaSalvare().getDataHandler().getInputStream() != null) {
				is = status.getDocDaSalvare().getDataHandler().getInputStream();
			}

			if (is.available() != 0) {

				final int idNarExecuted = npsSRV.uploadDocToAsyncNps(is, docInput.getGuid(), protocolloNpsDTO.getIdProtocollo(), status.getDocDaSalvare().getContentType(),
						docInput.getNomeFile(), docInput.getOggetto(), infoProtocollo, utente, docInput.getDocumentTitle(), con);

				npsSRV.associateDocumentoProtocolloToAsyncNps(idNarExecuted, docInput.getGuid(), protocolloNpsDTO.getIdProtocollo(), docInput.getNomeFile(),
						docInput.getOggetto(), utente, true, true, null, null, infoProtocollo, docInput.getDocumentTitle(), con);

			}

		} catch (final IOException e) {
			LOGGER.error("Protocollazione NPS -> Si è verificato un errore durante il recupero del content del documento.", e);
			throw new RedException("Protocollazione NPS -> Si è verificato un errore durante il recupero del content del documento.", e);
		}

		// Se il documento ha degli allegati, si gestiscono allo stesso modo anche
		// questi ultimi
		if (status.getDocDaSalvare().getAllegati() != null && !status.getDocDaSalvare().getAllegati().isEmpty()) {
			npsSRV.uploadAllegatiDocToAsyncNps(docInput.getGuid(), infoProtocollo, protocolloNpsDTO.getIdProtocollo(), utente, false, fceh, con);
		}
		LOGGER.info("Protocollazione NPS ->  Eseguito upload asincrono del documento principale e degli allegati per il documento: " + docInput.getDocumentTitle());
	}

	/**
	 * Gestisce la fascicolazione del documento.
	 * 
	 * @param status
	 * @param idDocumento
	 * @param docInput
	 * @param utente
	 * @param fceh
	 * @param con
	 * @return
	 */
	private List<FascicoloRedFnDTO> gestisciFascicolazione(final SalvaDocumentoRedStatusDTO status, final int idDocumento, final SalvaDocumentoRedDTO docInput,
			final UtenteDTO utente, final IFilenetCEHelper fceh, final Connection con) {
		LOGGER.info("gestisciFascicolazione -> START. Documento: " + idDocumento);
		final Date dataInizio = new Date();
		final List<FascicoloRedFnDTO> fascicoli = new ArrayList<>();
		ListSecurityDTO securityFascicoloAutomatico;
		final Long idAoo = utente.getIdAoo();
		final List<FascicoloDTO> inputFascicoli = docInput.getFascicoli();
		final List<RispostaAllaccioDTO> inputAllacci = docInput.getAllacci();
		boolean aggiornaClassificazioneFascicolo = false;
		boolean isDocInputEntrata = CategoriaDocumentoEnum.ENTRATA.equals(CategoriaDocumentoEnum.get(docInput.getIdCategoriaDocumento()));

		// FASCICOLAZIONE - Se il documento in input non ha specificati fascicoli e
		// allacci a cui deve essere associato
		if (CollectionUtils.isEmpty(inputFascicoli) && (CollectionUtils.isEmpty(inputAllacci) || (!CollectionUtils.isEmpty(inputAllacci) && isDocInputEntrata))) {
			// Deve essere creato il fascicolo automatico
			LOGGER.info("Fascicolazione - Nessun fascicolo passato in input: deve essere creato il fascicolo automatico");
			status.setFascicoloAutomatico(true);

			final int idIter = getIdIter(docInput.getIterApprovativo());
			final String assegnazioneCoordinatore = getAssegnazioneCoordinatore(docInput);
			final Boolean aggiungiDelegato = aggiungiUtenteDelegato(status);
			final boolean isNodoUcp = nodoDAO.isNodoUcp(utente.getIdUfficio(), con);

			// Security del documento per il fascicolo
			securityFascicoloAutomatico = securitySRV.getSecurityPerDocumento(String.valueOf(idDocumento), utente, status.getAssegnazioniCompetenza(),
					status.getAssegnazioniConoscenza(), status.getAssegnazioniContributo(), status.getAssegnazioniFirma(), status.getAssegnazioniSigla(),
					status.getAssegnazioniVisto(), status.getAssegnazioniFirmaMultipla(), utente.getIdUfficio(), utente.getId(), false, status.isDocEntrata(), idIter,
					docInput.getIsRiservato(), assegnazioneCoordinatore, aggiungiDelegato, isNodoUcp, status.getDestinatariInterniString(), con);

			// Security dei documenti allacciati per il fascicolo (IDUFFICIO MA PASSA
			// IDUTENTE ?????????)
			status.setSecurityFascicoloDocAllacciati(securitySRV.getSecurityDocumentoPerDocumentiAllacciati(String.valueOf(idDocumento), utente,
					status.getAssegnazioniCompetenza(), status.getAssegnazioniConoscenza(), status.getAssegnazioniContributo(), status.getAssegnazioniFirma(),
					status.getAssegnazioniSigla(), status.getAssegnazioniVisto(), status.getAssegnazioniFirmaMultipla(), utente.getId(), Long.valueOf(0), false, idIter,
					docInput.getIsRiservato(), assegnazioneCoordinatore, aggiungiDelegato, status.getDestinatariInterniString(), con));

			if (!status.isDocEntrata() && !status.isIterManuale()) {
				fascicoloSRV.aggiungiFascicoloAutomatico(idDocumento, utente, docInput.getIndiceClassificazioneFascicoloProcedimentale(),
						docInput.getDescrizioneFascicoloProcedimentale(), status.getMetadatiFascicolo(), securityFascicoloAutomatico, fascicoli, fceh, con);
			} else {
				if (status.getAssegnazioniCompetenza().length > 0) {
					for (int i = 0; i < status.getAssegnazioniCompetenza().length; i++) {
						String assegnazione = status.getAssegnazioniCompetenza()[i];
						if (assegnazione.endsWith(",")) {
							assegnazione += "0";
						}
						if (!status.isDocEntrata() && status.isIterManuale() && TipoAssegnazioneEnum.SPEDIZIONE.equals(docInput.getTipoAssegnazioneEnum())) {
							aggiungiAssegnazioneFascicoloAutomatico(status, assegnazione, idDocumento, docInput, utente, fascicoli, securityFascicoloAutomatico,
									TipoAssegnazioneEnum.SPEDIZIONE, fceh, con);
						} else if (TipoAssegnazioneEnum.COPIA_CONFORME.equals(docInput.getTipoAssegnazioneEnum())) {
							aggiungiAssegnazioneFascicoloAutomatico(status, assegnazione, idDocumento, docInput, utente, fascicoli, securityFascicoloAutomatico,
									TipoAssegnazioneEnum.COPIA_CONFORME, fceh, con);
						} else {
							aggiungiAssegnazioneFascicoloAutomatico(status, assegnazione, idDocumento, docInput, utente, fascicoli, securityFascicoloAutomatico,
									TipoAssegnazioneEnum.COMPETENZA, fceh, con);
						}
					}
				}
				if (status.getAssegnazioniFirma() != null && status.getAssegnazioniFirma().length > 0) {
					aggiungiAssegnazioniFascicoloAutomatico(status, status.getAssegnazioniFirma(), idDocumento, docInput, utente, fascicoli, securityFascicoloAutomatico,
							TipoAssegnazioneEnum.FIRMA, fceh, con);
				}
				if (status.getAssegnazioniSigla() != null && status.getAssegnazioniSigla().length > 0) {
					aggiungiAssegnazioniFascicoloAutomatico(status, status.getAssegnazioniSigla(), idDocumento, docInput, utente, fascicoli, securityFascicoloAutomatico,
							TipoAssegnazioneEnum.SIGLA, fceh, con);
				}
				if (status.getAssegnazioniVisto() != null && status.getAssegnazioniVisto().length > 0) {
					aggiungiAssegnazioniFascicoloAutomatico(status, status.getAssegnazioniVisto(), idDocumento, docInput, utente, fascicoli, securityFascicoloAutomatico,
							TipoAssegnazioneEnum.VISTO, fceh, con);
				}
				if (status.getAssegnazioniFirmaMultipla() != null && status.getAssegnazioniFirmaMultipla().length > 0) {
					aggiungiAssegnazioniFascicoloAutomatico(status, status.getAssegnazioniFirmaMultipla(), idDocumento, docInput, utente, fascicoli,
							securityFascicoloAutomatico, TipoAssegnazioneEnum.FIRMA_MULTIPLA, fceh, con);
				}
			}
		} else if (!CollectionUtils.isEmpty(inputAllacci) && !isDocInputEntrata) {
			LOGGER.info("Fascicolazione - Allacci -> START");
			boolean fascicoloCreato = false;
			final Set<String> checkFascicoliInseriti = new HashSet<>();
			for (final RispostaAllaccioDTO allaccio : inputAllacci) {
				final String idFascicoloAllaccio = allaccio.getFascicolo().getIdFascicolo();
				final int idFascicoloAllaccioInt = NumberUtils.toInt(idFascicoloAllaccio);

				// Verificata la presenza del fascicolo, si procede con l'applicazione
				// dell'indice di classificazione
				// solo nel caso in cui l'indice non risulti già applicato sul suddetto
				// fascicolo
				if (allaccio.getFascicolo() != null && StringUtils.isBlank(fascicoloDAO.getIndiceClassificazione(idFascicoloAllaccio, idAoo, con))) {
					// Si associa il fascicolo
					fascicoloSRV.associaATitolario(idFascicoloAllaccio, docInput.getIndiceClassificazioneFascicoloProcedimentale(), idAoo, fceh, con);
					LOGGER.info("Fascicolazione - Allacci -> Fascicolo: " + idFascicoloAllaccio + " associato al titolario: "
							+ DocumentoUtils.getTitolario(idAoo, docInput.getIndiceClassificazioneFascicoloProcedimentale()));
				}

				String idFascicoloRibaltato = null;
				if (!CollectionUtils.isEmpty(inputFascicoli)) {
					final FascicoloDTO fascicoloRibaltato = inputFascicoli.get(0);
					if (fascicoloRibaltato != null) {
						idFascicoloRibaltato = fascicoloRibaltato.getIdFascicolo();
					}
				}

				// Se il documento di risposta si trova già nel fascicolo del documento da
				// allacciare, allora non si esegue la insert
				if (!checkFascicoliInseriti.contains(idFascicoloAllaccio)) {
					checkFascicoliInseriti.add(idFascicoloAllaccio);
					// Inserimento dell'associazione tra documento e fascicolo sul DB.
					// Se il fascicolo dell'allacciato è quello ribaltato, lo si rende automatico
					if (idFascicoloRibaltato != null && idFascicoloRibaltato.equals(idFascicoloAllaccio)) {
						fascicoloDAO.insertDocumentoFascicolo(idDocumento, idFascicoloAllaccioInt, TipoFascicolo.AUTOMATICO, idAoo, con);
						LOGGER.info("Fascicolazione - Allacci -> Associato fascicolo AUTOMATICO: " + idFascicoloAllaccioInt + AL_DOCUMENTO + idDocumento);
					} else {
						fascicoloDAO.insertDocumentoFascicolo(idDocumento, idFascicoloAllaccioInt, TipoFascicolo.NON_AUTOMATICO, idAoo, con);
						LOGGER.info("Fascicolazione - Allacci -> Associato fascicolo NON AUTOMATICO: " + idFascicoloAllaccioInt + AL_DOCUMENTO + idDocumento);
					}
				}

				final FascicoloRedFnDTO fascicolo = new FascicoloRedFnDTO();
				fascicolo.setIdFascicolo(idFascicoloAllaccio);
				fascicoli.add(fascicolo);

				if (!fascicoloCreato) {
					// Si aggiungono le assegnazioni alla lista di assegnazioni del fascicolo
					aggiungiAllAssegnazioniFascicolo(status, docInput, fascicolo);

					// Si aggiungono le security del coordinatore e del delegato al coordinatore al
					// fascicolo dell'allacciato
					if (status.isUtenteCoordinatore()) {
						status.getSecurityFascicoloDocAllacciati()
								.addAll(securitySRV.getSecurityPuntuale(docInput.getIdUfficioCoordinatore(), docInput.getIdUtenteCoordinatore(), con));
						status.getSecurityFascicoloDocAllacciati().addAll(securitySRV.getSecurityUtenteDelegato(docInput.getIdUfficioCoordinatore(), con));
					}
					fascicoloCreato = true;
				}
			}
			LOGGER.info("Fascicolazione - Allacci -> END");
			// Fascicolazione - Fascicolo Procedimentale selezionato
		} else if (!CollectionUtils.isEmpty(inputFascicoli) && docInput.getFascicoloProcedimentale() != null) {
			LOGGER.info("Fascicolazione - Fascicolo Procedimentale selezionato -> START. Fascicolo: " + docInput.getFascicoloProcedimentale());

			aggiornaClassificazioneFascicolo = StringUtils.isNotBlank(docInput.getIndiceClassificazioneFascicoloProcedimentale())
					&& !docInput.getIndiceClassificazioneFascicoloProcedimentale().equals(docInput.getFascicoloProcedimentale().getIndiceClassificazione());
			LOGGER.info("Fascicolazione - aggiornaClassificazioneFascicolo -> " + aggiornaClassificazioneFascicolo);

			aggiungiFascicolo(status, idDocumento, docInput, docInput.getFascicoloProcedimentale().getIdFascicolo(), docInput.getFascicoloProcedimentale().getDescrizione(),
					(aggiornaClassificazioneFascicolo) ? docInput.getIndiceClassificazioneFascicoloProcedimentale()
							: docInput.getFascicoloProcedimentale().getIndiceClassificazione(),
					docInput.getFascicoloProcedimentale().getDataCreazione(), fascicoli, idAoo, con, true);

			LOGGER.info("Fascicolazione - Fascicolo Procedimentale selezionato -> END");
			// Fascicolazione - Tab "Fascicoli"
		} else if (!CollectionUtils.isEmpty(inputFascicoli)) {
			LOGGER.info("Fascicolazione - Tab Fascicoli -> START. Numero fascicoli: " + inputFascicoli.size());

			for (final FascicoloDTO inputFascicolo : inputFascicoli) {
				aggiungiFascicolo(status, idDocumento, docInput, inputFascicolo.getIdFascicolo(), inputFascicolo.getDescrizione(), inputFascicolo.getIndiceClassificazione(),
						inputFascicolo.getDataCreazione(), fascicoli, idAoo, con, false);
			}

			LOGGER.info("Fascicolazione - Tab Fascicoli -> END");
		}

		// ### FASCICOLAZIONE SU FILENET -> START
		// Fascicolo automatico appena creato
		if (!status.isPrecensito() && status.isFascicoloAutomatico()) {
			// Fascicolo automatico per iter automatico
			if (status.getAssegnazioniFascicolo().isEmpty()) {
				LOGGER.info("Fascicolazione - FileNet -> Fascicolo automatico per iter automatico: " + fascicoli.get(0).getIdFascicolo());
				fascicoloSRV.fascicolaSuFilenet(fascicoli.get(0).getIdFascicolo(), idDocumento, idAoo, fceh);
				// MULTICOMPETENZA - Fascicoli automatici (uno per ogni assegnazione per
				// competenza)
			} else {
				LOGGER.info("Fascicolazione - FileNet -> Fascicoli automatici (uno per ogni assegnazione per competenza)");
				for (final AssegnazioneWorkflowDTO assegnazioneWorkflow : status.getAssegnazioniFascicolo()) {
					fascicoloSRV.fascicolaSuFilenet(assegnazioneWorkflow.getFascicolo().getIdFascicolo(), idDocumento, idAoo, fceh);
				}
			}
			// Salvataggio di un documento già fascicolato proveniente da tab fascicolo
		} else if (!status.isDocInterno()) {
			LOGGER.info("Fascicolazione - FileNet -> Salvataggio di un documento già fascicolato proveniente da tab fascicolo");

			// Fascicoli del tab "Fascicoli" o fascicolo del documento in risposta (solo
			// documenti in uscita)
			final Set<String> checkFascicoliInseritiFilenet = new HashSet<>();
			for (int i = 0; i < fascicoli.size(); i++) {
				// Si procede con la riattivazione del fascicolo, se chiuso
				if (CollectionUtils.isEmpty(inputAllacci)) {
					final FascicoloDTO fascicolo = TrasformCE.transform(fceh.getFascicolo(fascicoli.get(i).getIdFascicolo(), idAoo.intValue()),
							TrasformerCEEnum.FROM_DOCUMENTO_TO_FASCICOLO);

					// Riattivazione del fascicolo
					if (FilenetStatoFascicoloEnum.CHIUSO.getNome().equalsIgnoreCase(fascicolo.getStato())) {
						fascicoloSRV.aggiornaStato(fascicoli.get(i).getIdFascicolo(), FilenetStatoFascicoloEnum.APERTO, idAoo, fceh);
					}
				}

				// Si controlla se si sta fascicolando il documento all'interno dello stesso
				// fascicolo
				if (!checkFascicoliInseritiFilenet.contains(fascicoli.get(i).getIdFascicolo())) {
					checkFascicoliInseritiFilenet.add(fascicoli.get(i).getIdFascicolo());
					fascicoloSRV.fascicolaSuFilenet(fascicoli.get(i).getIdFascicolo(), idDocumento, idAoo, fceh);

					if (aggiornaClassificazioneFascicolo) {
						final HashMap<String, Object> metadati = new HashMap<>();
						final String titolario = DocumentoUtils.getTitolario(idAoo, docInput.getIndiceClassificazioneFascicoloProcedimentale()).trim();
						metadati.put(pp.getParameterByKey(PropertiesNameEnum.TITOLARIO_METAKEY), titolario);

						fceh.updateFascicoloMetadati(fascicoli.get(i).getIdFascicolo(), idAoo, pp.getParameterByKey(PropertiesNameEnum.FASCICOLO_CLASSNAME_FN_METAKEY),
								metadati);
					}
				}
			}
		}

		// ### Chiusura di eventuali fascicoli in risposta diversi dal primo -> START
		if (!CollectionUtils.isEmpty(inputAllacci) && inputAllacci.size() > 1) {
			for (int i = 1; i < inputAllacci.size(); i++) {
				final RispostaAllaccioDTO documentoAllacciato = inputAllacci.get(i);
				// Si controlla se nella maschera di allaccio è stato selezionato come "da
				// chiudere" (documentoDaChiudere = TRUE)
				if (Boolean.TRUE.equals(documentoAllacciato.getDocumentoDaChiudere())) {
					fascicoloSRV.aggiornaStato(documentoAllacciato.getFascicolo().getIdFascicolo(), FilenetStatoFascicoloEnum.CHIUSO, idAoo, fceh);
				}
			}
		}
		// ### Chiusura di eventuali fascicoli in risposta diversi dal primo -> END
		// ### FASCICOLAZIONE SU FILENET -> END

		final Date dataFine = new Date();
		LOGGER.info("gestisciFascicolazione -> END. Documento: " + idDocumento + ". Timing ===> " + (dataFine.getTime() - dataInizio.getTime()) + "ms");
		return fascicoli;
	}

	private void aggiungiFascicolo(final SalvaDocumentoRedStatusDTO status, final int idDocumento, final SalvaDocumentoRedDTO docInput, final String idFascicolo,
			final String descrizioneFascicolo, final String indiceClassificazione, final Date dataCreazioneFascicolo, final List<FascicoloRedFnDTO> fascicoli,
			final Long idAoo, final Connection con, final boolean automatico) {
		final Calendar creazioneFascicoloCal = Calendar.getInstance();
		creazioneFascicoloCal.setTime(dataCreazioneFascicolo);
		final FascicoloRedFnDTO fascicolo = fascicoloSRV.costruisciFascicoloRedFn(idFascicolo, creazioneFascicoloCal.get(Calendar.YEAR), idFascicolo, descrizioneFascicolo,
				indiceClassificazione, idAoo);
		fascicoli.add(fascicolo);

		aggiungiAllAssegnazioniFascicolo(status, docInput, fascicolo);

		// Inserimento associazione documento <-> fascicolo su DB
		if (!status.isDocInterno()) {
			fascicoloDAO.insertDocumentoFascicolo(idDocumento, NumberUtils.toInt(idFascicolo), (automatico) ? TipoFascicolo.AUTOMATICO : TipoFascicolo.NON_AUTOMATICO, idAoo,
					con);
		}
	}

	private void aggiornaDocumento(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente, final IFilenetCEHelper fceh,
			final FilenetPEHelper fpeh, final Connection con, final Connection dwhCon) {
		VWWorkObject wob = null;
		if (StringUtils.isNotBlank(docInput.getWobNumber())) {
			wob = fpeh.getWorkFlowByWob(docInput.getWobNumber(), true);
		}

		// Si verifica se è necessario aggiornare i dati di protocollo NPS Oggetto e
		// Tipologia Documento
		verificaAggiornamentoDatiProtocolloNps(status, docInput.getDocumentTitle(), docInput.getOggetto(), docInput.getMetadatiEstesi(), docInput.getTipologiaDocumento(),
				utente.getIdAoo(), utente.getIdTipoProtocollo(), fceh);

		// Modalità di aggiornamento "ibrida"
		if (status.isModalitaUpdateIbrida()) {
			if (!status.isBozza() && !status.isDocInterno()) {
				// Protocollazione documento precensito
				protocollaPrecensito(status, docInput, utente, wob, fceh, fpeh);
			} else if (status.isBozza()) {
				// Trasformazione bozza in documento -> documento uscita/mozione
				aggiornaBozza(status, docInput, utente, fceh, con);
			} else if (status.isDocInterno()) {
				// Trasformazione documento interno -> documento uscita/mozione
				aggiornaDocumentoInterno(status, docInput, utente, fceh, fpeh, con);
			}
			// ######## Operazioni da eseguire dopo il salvataggio del documento su FileNet
			// (in questo caso dopo l'aggiornamento)
			// N.B. CHIAMATA A TUTTO CIÒ CHE SEGUE documentoInserito = true
			// (UpdateDocumentoEvent:1301)
			postSalvataggioDocumentoFilenet(status, wob, docInput, utente, fceh, fpeh, con, dwhCon);

			// Modalità di aggiornamento "pura"
		} else if (status.isModalitaUpdate()) {
			aggiornaDocumento(status, wob, docInput, utente, fceh, fpeh, con, dwhCon);
		}
	}

	private void verificaAggiornamentoDatiProtocolloNps(final SalvaDocumentoRedStatusDTO status, final String documentTitle, final String oggetto,
			final Collection<MetadatoDTO> metadati, final TipologiaDocumentoDTO tipologiaDocumento, final Long idAoo, final Long idTipoProtocollo,
			final IFilenetCEHelper fceh) {
		// Si esegue la verifica se il documento è già  presente e l'AOO dell'utente
		// utilizza NPS per la protocollazione
		if (StringUtils.isNotBlank(documentTitle) && TipologiaProtocollazioneEnum.isProtocolloNPS(idTipoProtocollo)) {

			final PropertyFilter pfDatiProtocolloNps = new PropertyFilter();
			pfDatiProtocolloNps.addIncludeProperty(new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY), null));
			pfDatiProtocolloNps.addIncludeProperty(new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.OGGETTO_PROTOCOLLO_NPS_METAKEY), null));
			pfDatiProtocolloNps
					.addIncludeProperty(new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.DESCRIZIONE_TIPOLOGIA_DOCUMENTO_NPS_METAKEY), null));

			final FilterElement[] feDatiProtocolloNps = pfDatiProtocolloNps.getIncludeProperties();
			final List<String> selectListDatiProtocolloNps = new ArrayList<>(feDatiProtocolloNps.length);
			for (final FilterElement filterElement : feDatiProtocolloNps) {
				selectListDatiProtocolloNps.add(filterElement.getValue());
			}

			// Si recuperano da FileNet i metadati relativi ai dati di protocollo NPS:
			// Oggetto e Tipologia Documento
			final Document currentDoc = fceh.getDocumentByIdGestionale(documentTitle, selectListDatiProtocolloNps, pfDatiProtocolloNps, idAoo.intValue(), null, null);
			Collection<MetadatoDTO> metadatiNPS = null;
			try {
				metadatiNPS = tipologiaDocumentoSRV.recuperaMetadatiEstesi(documentTitle, fceh, idAoo);
			} catch (final Exception e) {
				LOGGER.error("Errore durante il recupero dei metadati del documento: " + documentTitle, e);
				throw new RedException("Errore durante il recupero dei metadati del documento: " + documentTitle, e);
			}

			final Integer numeroProtocollo = (Integer) TrasformerCE.getMetadato(currentDoc, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);

			// Si esegue la verifica se il documento è già  protocollato
			if (numeroProtocollo != null && numeroProtocollo > 0) {
				final String oggettoProtocolloNps = (String) TrasformerCE.getMetadato(currentDoc, PropertiesNameEnum.OGGETTO_PROTOCOLLO_NPS_METAKEY);
				final String tipologiaDocumentoNps = (String) TrasformerCE.getMetadato(currentDoc, PropertiesNameEnum.DESCRIZIONE_TIPOLOGIA_DOCUMENTO_NPS_METAKEY);

				if (oggetto != null && !StringUtils.substring(oggetto, 0, 64).equals(oggettoProtocolloNps)) {
					status.setAggiornaOggettoProtocolloNPS(true);
				}

				if (tipologiaDocumento != null && !tipologiaDocumento.getDescrizione().equals(tipologiaDocumentoNps)) {
					status.setAggiornaTipologiaDocumentoNPS(true);
				}

				if (!CollectionUtils.isEmpty(metadati) && isMetadatiListChanged(metadati, metadatiNPS)) {
					status.setAggiornaMetadatiEstesiNPS(true);
				}
			}
		}
	}

	private void protocollaPrecensito(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente, final VWWorkObject wob,
			final IFilenetCEHelper fceh, final FilenetPEHelper fpeh) {
		final Integer idDocumento = (Integer) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));

		final Map<String, Object> metadati = caricaMetadatiDocumento(status, docInput, utente);

		// Si aggiornano i metadati
		fceh.updateMetadatiOnCurrentVersion(utente.getIdAoo(), idDocumento, metadati);

		// Si avanza il workflow con response 'Da Protocollare'
		final boolean esito = fpeh.nextStep(wob, null, ResponsesRedEnum.DA_PROTOCOLLARE.getResponse());
		if (!esito) {
			throw new FilenetException("Si è verificato un errore durante l'avanzamento del workflow per il documento: " + idDocumento);
		}
	}

	private void aggiornaBozza(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente, final IFilenetCEHelper fceh,
			final Connection con) {
		final String idDocumento = docInput.getDocumentTitle();
		LOGGER.info("aggiornaBozza -> START. Documento bozza: " + idDocumento);
		final Long idAoo = utente.getIdAoo();

		final Document documentoFilenet = fceh.getDocumentByIdGestionale(idDocumento, null, null, idAoo.intValue(), null, null);

		// Si caricano i metadati in input
		final Map<String, Object> metadatiDocumento = caricaMetadatiDocumento(status, docInput, utente);

		// ### AGGIORNAMENTO DEL DOCUMENTO - Si aggiorna il content solo se è cambiato
		if (status.isContentVariato()) {
			final InputStream contentIS = new ByteArrayInputStream(docInput.getContent());
			fceh.addVersion(documentoFilenet, contentIS, metadatiDocumento, idDocumento, docInput.getContentType(), idAoo);
		} else {
			fceh.addVersion(documentoFilenet, null, metadatiDocumento, idDocumento, null, utente.getIdAoo());
		}

		// ### CREAZIONE ANNOTATION
		final Date dataInizio = new Date();
		fceh.creaAnnotation(idDocumento, docInput.getGuid(), FilenetAnnotationEnum.CURRENT_VERSION, idAoo);
		final Date dataFine = new Date();
		LOGGER.info("aggiornaBozza -> Timing creaAnnotation per il documento: " + idDocumento + FRECCIA + (dataFine.getTime() - dataInizio.getTime()) + " ms");

		// ### MODIFICA DELLE SECURITY -> START
		final int idIter = getIdIter(docInput.getIterApprovativo());
		final Boolean aggiungiDelegato = aggiungiUtenteDelegato(status);

		// Si ottengono le security
		final ListSecurityDTO security = securitySRV.getSecurityDopoBozza(utente, idDocumento, status.getAssegnazioniCompetenza(),
				status.getAssegnazioniConoscenza(), status.getAssegnazioniContributo(), status.getAssegnazioniFirma(), status.getAssegnazioniSigla(),
				status.getAssegnazioniVisto(), status.getAssegnazioniFirmaMultipla(), true, idIter, Boolean.TRUE.equals(docInput.getIsRiservato()),
				getAssegnazioneCoordinatore(docInput), aggiungiDelegato, status.getDestinatariInterniString(), fceh, con);

		// Si aggiornano le security del documento
		securitySRV.aggiornaSecurityDocumento(idDocumento, idAoo, security, false, false, fceh);
		// ### MODIFICA DELLE SECURITY -> END

		status.setDocDaSalvare(new DocumentoRedFnDTO());
		status.getDocDaSalvare().setDocumentTitle(docInput.getDocumentTitle());
		status.getDocDaSalvare().setMetadati(metadatiDocumento);
		// Si sposta il documento da folder [Bozze] a folder [Folder NSD]
		status.getDocDaSalvare().setFolder(pp.getParameterByKey(PropertiesNameEnum.FOLDER_DOCUMENTI_CE_KEY));
		status.getDocDaSalvare().setSecurity(security);

		LOGGER.info("aggiornaBozza -> END. Documento bozza: " + idDocumento);
	}

	/**
	 * Nota bene: i workflow atomici/dinamici non sono ancora implementati, di
	 * seguito una parziale traduzione del codice di RED.
	 */
	private void aggiornaDocumentoInterno(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente, final IFilenetCEHelper fceh,
			final FilenetPEHelper fpeh, final Connection con) {
		final String idDocumento = docInput.getDocumentTitle();
		LOGGER.info("aggiornaDocumentoInterno -> START. Documento bozza: " + idDocumento);

		int idTipoFirma = 0;
		final boolean b = Math.random() > 1;
		if (b) {
			idTipoFirma = 1;
		}

		status.setDocDaSalvare(new DocumentoRedFnDTO());

		// Si recupera il documento da FileNet
		final Document documentoFilenet = fceh.getDocumentByIdGestionale(idDocumento, null, null, utente.getIdAoo().intValue(), null, null);

		// Si caricano i metadati in input
		final Map<String, Object> metadatiDocumento = caricaMetadatiDocumento(status, docInput, utente);

		// ### AGGIORNAMENTO DEL DOCUMENTO - Si aggiorna il content solo se è cambiato
		if (status.isContentVariato()) {
			final InputStream contentIS = new ByteArrayInputStream(docInput.getContent());
			fceh.addVersion(documentoFilenet, contentIS, metadatiDocumento, idDocumento, docInput.getContentType(), utente.getIdAoo());
		} else {
			fceh.addVersion(documentoFilenet, null, metadatiDocumento, idDocumento, null, utente.getIdAoo());
		}

		if (idTipoFirma > 0) {
			try {
				idTipoFirma = 0;
				String workflowName = null;
				String[] assegnazioni;
				ListSecurityDTO securityDoc;
				final String idFascicolo = docInput.getFascicoloProcedimentale().getIdFascicolo();
				final String[] elencoLibroFirma = new String[0];

				final Map<String, Object> metadatiWorkflow = new HashMap<>();
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY), Integer.parseInt(idDocumento));
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY), idFascicolo);
				if (status.getIdUfficioMittenteWorkflow() != null) {
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), status.getIdUfficioMittenteWorkflow());
				} else {
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), utente.getIdUfficio());
				}
				if (status.getIdUtenteMittenteWorkflow() != null) {
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), status.getIdUtenteMittenteWorkflow());
				} else {
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getIdUfficio());
				}

				// Iter automatico
				if (hasDocIterAutomatico(status)) {
					LOGGER.info("Avvio di un Workflow con Iter Automatico per il documento: " + idDocumento);
					assegnazioni = elencoLibroFirma;

					// Nome del workflow
					workflowName = status.getIterApprovativo().getWorkflowName();

					// Metadato Tipo Firma (se valorizzato)
					if (idTipoFirma > 0) {
						metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_FIRMA_FN_METAKEY), idTipoFirma);
					}

					// Metadati Elenco Libro Firma e ID Tipo Assegnazione
					if (elencoLibroFirma != null && elencoLibroFirma.length > 0) {
						metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY), elencoLibroFirma[0].split(",")[2]);
						metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_LIBROFIRMA_METAKEY), elencoLibroFirma);
					}
					// Iter manuale
				} else {
					LOGGER.info("Avvio di un Workflow con assegnazioni Iter Manuale per il documento: " + idDocumento);
					final String[] assegnazioniStepArray = new String[0];

					// Si includono i destinatari dello step come destinatari
					assegnazioni = getArrayAssegnazioniStep(assegnazioniStepArray);

					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ITEM_ASSEGNATARI_METAKEY), assegnazioniStepArray);

					// Nome del workflow
					workflowName = workflowSRV.getWorkflowNameProcessoStep(con);
				}

				// Si modificano/adeguano le security del fascicolo del documento
				securitySRV.modificaSecurityFascicoli(idDocumento, utente, assegnazioni, con);

				// Si modificano/adeguano le security del documento
				securityDoc = securitySRV.getSecurityPerRiassegnazioneDocumento(utente, idDocumento, assegnazioni, Boolean.TRUE.equals(docInput.getIsRiservato()), con);
				securitySRV.aggiornaSecurityDocumento(idDocumento, utente.getIdAoo(), securityDoc, true, false, fceh);

				// Gestione security aggiuntive (Allacci e Contributi interni) per Iter
				// Automatico
				if (hasDocIterAutomatico(status)) {
					// Si aggiornano le security di tutti i documenti allacciati (Allacci)
					securitySRV.aggiornaSecurityDocumentiAllacciati(utente, NumberUtils.toInt(idDocumento), securityDoc, assegnazioni, fceh, con);

					// Si aggiornano le security di tutti i Contributi inseriti (INTERNI)
					securitySRV.aggiornaSecurityContributiInterni(utente, idDocumento, assegnazioni, fceh, con);
				}

				// ### AVVIO DEL WORKFLOW
				avviaNuovoWorkflow(status, workflowName, docInput, utente, metadatiWorkflow, fpeh, con);

				// Si recupera la prima versione del documento
				final Document primaVersione = fceh.getDocumentVersion(documentoFilenet, 0);
				if (FilenetCEHelper.hasDocumentContentTransfer(primaVersione)) {
					final String documentTitle = (String) TrasformerCE.getMetadato(primaVersione, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
					final Integer fileRevisionNumber = fceh.getPreviousVersionWordRevisionNumber(primaVersione.get_Id().toString());

					List<String> idAllegatiDaFirmare = null;
					final DocumentSet allegati = fceh.getAllegati(primaVersione.get_Id().toString(), true, true);
					if (!allegati.isEmpty()) {
						idAllegatiDaFirmare = new ArrayList<>();
						final Iterator<?> itAllegati = allegati.iterator();
						while (itAllegati.hasNext()) {
							final String allegatoDocumentTitle = (String) TrasformerCE.getMetadato(((Document) itAllegati.next()), PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
							idAllegatiDaFirmare.add(allegatoDocumentTitle);
						}
					}
					Integer tipoFirmaTrasformazione = null;
					final List<IterApprovativoDTO> iterApprovativi = iterApprovativoDAO.getAllByIdAOO(utente.getIdAoo().intValue(), con);
					for (final IterApprovativoDTO iterApprovativo : iterApprovativi) {
						if (iterApprovativo.getWorkflowName().equals(workflowName)) {
							tipoFirmaTrasformazione = iterApprovativo.getIdIterApprovativo();
							break;
						}
					}

					// ### TRASFORMAZIONE PDF ASINCRONA
					eseguiTrasformazionePDF(status, documentTitle, utente, elencoLibroFirma, null, idAllegatiDaFirmare, tipoFirmaTrasformazione, fileRevisionNumber, true,
							true, true, docInput.getTipoAssegnazioneEnum(), null, con);
				}
			} catch (final Exception e) {
				LOGGER.error("Errore nell'avvio del procedimento", e);
				throw new RedException("Errore nell'avvio del procedimento", e);
			}
			// ### MODIFICA DELLE SECURITY DEL DOCUMENTO
		} else if (documentoFilenet != null) {
			final Boolean aggiungiDelegato = aggiungiUtenteDelegato(status);
			final String assegnazioneCoordinatore = getAssegnazioneCoordinatore(docInput);

			final ListSecurityDTO securityDoc = securitySRV.getSecurityDopoBozza(utente, idDocumento, status.getAssegnazioniCompetenza(),
					status.getAssegnazioniConoscenza(), status.getAssegnazioniContributo(), status.getAssegnazioniFirma(), status.getAssegnazioniSigla(),
					status.getAssegnazioniVisto(), status.getAssegnazioniFirmaMultipla(), true, getIdIter(status.getIterApprovativo()),
					Boolean.TRUE.equals(docInput.getIsRiservato()), assegnazioneCoordinatore, aggiungiDelegato, status.getDestinatariInterniString(), fceh, con);

			// Si modificano le security del documento
			securitySRV.aggiornaSecurityDocumento(idDocumento, utente.getIdAoo(), securityDoc, false, false, fceh);

			// Si impostano le security sul documento
			status.getDocDaSalvare().setSecurity(securityDoc);
		}

		status.getDocDaSalvare().setDocumentTitle(docInput.getDocumentTitle());
		status.getDocDaSalvare().setMetadati(metadatiDocumento);
		status.getDocDaSalvare().setFolder(pp.getParameterByKey(PropertiesNameEnum.FOLDER_DOCUMENTI_CE_KEY));

		LOGGER.info("aggiornaDocumentoInterno -> END. Documento: " + idDocumento);
	}

	private void aggiornaDocumento(final SalvaDocumentoRedStatusDTO status, VWWorkObject wob, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente,
			final IFilenetCEHelper fceh, final FilenetPEHelper fpeh, final Connection con, final Connection dwhCon) {
		final int idDocumento = NumberUtils.toInt(docInput.getDocumentTitle());
		LOGGER.info("aggiornaDocumento -> START. Documento: " + idDocumento);
		final boolean isRiservato = Boolean.TRUE.equals(docInput.getIsRiservato());
		final Long idAoo = utente.getIdAoo();

		boolean assegnazioneCompetenza = false;
		final boolean assegnazioneConoscenza = false;
		final boolean assegnazioneContributo = false;
		final boolean assConoscenzaAggiornata = false;
		final boolean assContributoAggiornata = false;
		final boolean assegnazioneAggiornata = false;

		final Long idUtenteNuovaAssegnazione = null;
		final Long idUfficioNuovaAssegnazione = null;
		final Map<String, Object> metadatiWorkflow = new HashMap<>();
		String motivazioneAssegnazione = null;
		ResponsesRedEnum workflowResponse = null;
		String documentoMailGuid = null;

		try {
			final Long idUfficioDestinatario = Long.valueOf((Integer) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY)));
			final Long idUtenteDestinatario = Long
					.valueOf((Integer) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY)));

			// ID Raccolta FAD
			boolean recuperaMetadatoRaccoltaFAD = false;
			final Integer tipologiaDocAttoDecreto = Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.ATTO_DECRETO_TIPO_DOC_VALUE));
			final Long tipoProcedimentoDocAttoDecretoManuale = Long.parseLong(pp.getParameterByKey(PropertiesNameEnum.ATTO_DECRETO_MANUALE_VALUE));
			if (tipologiaDocAttoDecreto.equals(docInput.getTipologiaDocumento().getIdTipologiaDocumento())
					&& tipoProcedimentoDocAttoDecretoManuale.equals(docInput.getTipoProcedimento().getTipoProcedimentoId())) {
				recuperaMetadatoRaccoltaFAD = true;
			}

			final DetailDocumentRedDTO oldDetailDoc = getOldDetailRedPerAggiornamento(docInput.getDocumentTitle(), utente.getIdAoo(), fceh, recuperaMetadatoRaccoltaFAD);
			if (oldDetailDoc == null) {
				throw new RedException("Impossibile recuperare la versione precedente del documento");
			}

			if (oldDetailDoc.getOggetto() != null && !oldDetailDoc.getOggetto().equals(docInput.getOggetto())) {
				eventoLogSRV.inserisciEventoLog(idDocumento, utente.getIdUfficio(), utente.getId(), 0L, 0L, Calendar.getInstance().getTime(), EventTypeEnum.MODIFICA_OGGETTO,
						motivazioneAssegnazione, 0, idAoo, dwhCon);
			}

			final String notaOldMetMinimi = oldDetailDoc.getNote() == null ? "" : oldDetailDoc.getNote();
			if (docInput.isModificaMetadatiMinimi() && docInput.getNote() != null && !docInput.getNote().equals(notaOldMetMinimi)) {
				eventoLogSRV.inserisciEventoLog(idDocumento, utente.getIdUfficio(), utente.getId(), 0L, 0L, Calendar.getInstance().getTime(), EventTypeEnum.MODIFICA_NOTA,
						motivazioneAssegnazione, 0, idAoo, dwhCon);
			}

			docInput.setIdProtocollo(oldDetailDoc.getIdProtocollo());
			docInput.setAnnoProtocollo(oldDetailDoc.getAnnoProtocollo());
			docInput.setDataProtocollo(oldDetailDoc.getDataProtocollo());
			docInput.setNumeroProtocollo(oldDetailDoc.getNumeroProtocollo());

			// Gestione mittente new filenet
			if (oldDetailDoc.getObjectIdContattoMittOnTheFly() != null) {
				docInput.setObjectIdContattoMittOnTheFly(oldDetailDoc.getObjectIdContattoMittOnTheFly());
			}
			// Gesione destinatari new filenet
			if (oldDetailDoc.getObjectIdContattoDestOnTheFly() != null) {
				docInput.setObjectIdContattoDestOnTheFly(oldDetailDoc.getObjectIdContattoDestOnTheFly());
			}

			// ### ALLACCI --- Individuazione documenti da allacciare
			List<RispostaAllaccioDTO> allAllacci = docInput.getAllacci();
			List<RispostaAllaccioDTO> nuoviAllacci = trovaDocumentiDaAllacciare(allAllacci, idDocumento, idAoo, con);
			
			// ### ALLACCI --- Inserimento documenti da allacciare
			salvaDocumentiDaAllacciare(nuoviAllacci, idDocumento, idAoo, con);
			LOGGER.info("Allacci salvati per il documento: " + idDocumento);
			
			// ### ALLACCI --- Invio nuovi allacci a NPS
			if (docInput.getNumeroProtocollo() != null && nuoviAllacci != null && !nuoviAllacci.isEmpty()) {
				String numeroProtocollo = String.valueOf(docInput.getNumeroProtocollo());
				String annoProtocollo = String.valueOf(docInput.getAnnoProtocollo());
				String numeroAnnoProtocollo = numeroProtocollo + "/" + annoProtocollo + "_" + utente.getCodiceAoo();
				
				npsSRV.aggiungiAllacciAsync(utente, con, docInput.getIdProtocollo(), nuoviAllacci, numeroAnnoProtocollo, false, docInput.getDocumentTitle());
			}
			
			// ### SOTTOSCRIZIONI --- Si gestisce il flag di tracciamento del procedimento
			gestisciTracciaProcedimento(docInput, utente, dwhCon);

			// ### TEMPLATE DOC USCITA
			gestisciTemplateDocUscita(docInput, utente.getId(), con);

			// ### METADATO URGENTE --- Si verifica l'aggiornamento del metadato Urgente
			if (!docInput.getIsUrgente().equals(oldDetailDoc.getUrgente())) {
				// Si aggiunge il metadato da modificare alla lista dei metadati
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.URGENTE_METAKEY),
						Boolean.TRUE.equals(docInput.getIsUrgente()) ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue());

				if (Boolean.TRUE.equals(docInput.getIsUrgente())) {
					motivazioneAssegnazione = "Abilitato indicatore <urgente>";
				} else {
					motivazioneAssegnazione = "Disabilitato indicatore <urgente>";
				}

				eventoLogSRV.inserisciEventoLog(idDocumento, utente.getIdUfficio(), utente.getId(), 0L, 0L, Calendar.getInstance().getTime(), EventTypeEnum.MODIFICA_URGENTE,
						motivazioneAssegnazione, 0, idAoo, dwhCon);
			}

			// ### METADATO DATA SCADENZA --- Si verifica l'aggiornamento del metadato Data
			// Scadenza
			if (oldDetailDoc.getDataScadenza() != docInput.getDataScadenza()) {
				// Si aggiunge il metadato da modificare alla lista dei metadati
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY),
						docInput.getDataScadenza() != null ? DateUtils.setDateTo2359(docInput.getDataScadenza())
								: Date.from(DateUtils.NULL_DATE.atZone(ZoneId.systemDefault()).toInstant()));

				if (docInput.getDataScadenza() != null) {
					motivazioneAssegnazione = "Inserita data di scadenza " + docInput.getDataScadenza();
				} else {
					motivazioneAssegnazione = "Eliminata data di scadenza";
				}

				eventoLogSRV.inserisciEventoLog(idDocumento, utente.getIdUfficio(), utente.getId(), 0L, 0L, Calendar.getInstance().getTime(),
						EventTypeEnum.MODIFICA_DATA_SCADENZA, motivazioneAssegnazione, 0, idAoo, dwhCon);
			}

			// ### WORKFLOW --- Si aggiornano i workflow
			if (!metadatiWorkflow.isEmpty()) {
				fpeh.updateWorkflow(idDocumento, metadatiWorkflow);
			}

			// ### GESTIONE SECURITY
			// Si controlla se l'utente è in competenza (competenza/firma/sigla/visto),
			// conoscenza o contributo
			// (Quindi si controlla se c'è un'assegnazione all'ufficio dell'utente oppure
			// proprio all'utente stesso)
			if (!CollectionUtils.isEmpty(docInput.getAssegnazioni())) {
				final List<AssegnazioneDTO> assegnazioni = docInput.getAssegnazioni();
				for (final AssegnazioneDTO assegnazione : assegnazioni) {
					// Si controlla se è assegnato all'utente (cioè al suo ufficio o proprio
					// all'utente stesso)
					if (assegnazione.getUfficio() != null && assegnazione.getUfficio().getId().equals(utente.getIdUfficio())
							&& ((assegnazione.getUtente() != null && assegnazione.getUtente().getId().equals(utente.getId()))
									|| (assegnazione.getUtente() != null && assegnazione.getUtente().getId() == 0) || assegnazione.getUtente() == null)) {

						final TipoAssegnazioneEnum tipoAssegnazione = assegnazione.getTipoAssegnazione();
						if (TipoAssegnazioneEnum.COMPETENZA.equals(tipoAssegnazione) || TipoAssegnazioneEnum.FIRMA.equals(tipoAssegnazione)
								|| TipoAssegnazioneEnum.SIGLA.equals(tipoAssegnazione) || TipoAssegnazioneEnum.VISTO.equals(tipoAssegnazione)
								|| TipoAssegnazioneEnum.RIFIUTO_ASSEGNAZIONE.equals(tipoAssegnazione) || TipoAssegnazioneEnum.RIFIUTO_FIRMA.equals(tipoAssegnazione)
								|| TipoAssegnazioneEnum.RIFIUTO_SIGLA.equals(tipoAssegnazione) || TipoAssegnazioneEnum.RIFIUTO_VISTO.equals(tipoAssegnazione)) {
							assegnazioneCompetenza = true;

							// Si controlla se il Livello Riservatezza è stato modificato. In caso positivo,
							// si aggiornano le security a livello Riservato.
							final boolean oldRiservato = oldDetailDoc.getRiservato() == 1 ? Boolean.TRUE : Boolean.FALSE;
							if (isRiservato != oldRiservato) {
								LOGGER.info("aggiornaDocumento -> Si aggiornano le security con nuovo livello riservatezza: " + docInput.getIsRiservato());

								final ListSecurityDTO security = securitySRV.getStoricoSecurity(NumberUtils.toInt(oldDetailDoc.getDocumentTitle()),
										status.isDocEntrata(), utente, docInput.getIsRiservato(), fceh, con);

								securitySRV.aggiornaEVerificaSecurityDocumento(docInput.getDocumentTitle(), utente.getIdAoo(), security, false, true, fceh, con);

								if (Boolean.FALSE.equals(docInput.getIsRiservato())) {
									securitySRV.modificaSecurityFascicoli(utente, security, docInput.getDocumentTitle());
								}

								// Si inserisce la modifica del Livello Riservatezza sullo storico
								eventoLogSRV.inserisciEventoLog(idDocumento, utente.getIdUfficio(), utente.getId(), 0L, 0L, Calendar.getInstance().getTime(),
										EventTypeEnum.MODIFICA_LIVELLO_RISERVATEZZA, null, 0, idAoo, dwhCon);
							
								status.setAggiornaRiservatezza(true);
							}

							break;
						}
					}
				}
			}

			// Si pulisce la mappa dei metadati del workflow
			metadatiWorkflow.clear();

			// ### GESTIONE COMPETENZA
			if (assegnazioneCompetenza && !assegnazioneConoscenza && !assegnazioneContributo) {
				final Integer currentTipologiaDocumentoId = docInput.getTipologiaDocumento().getIdTipologiaDocumento();
				boolean tipologiaDocumentoCambiata = false;
				if (!oldDetailDoc.getIdTipologiaDocumento().equals(currentTipologiaDocumentoId)) {
					tipologiaDocumentoCambiata = true;
				}

				// Documento in entrata
				if (status.isDocEntrata()) {
					final Long currentTipoProcedimentoId = docInput.getTipoProcedimento().getTipoProcedimentoId();
					boolean tipoProcedimentoCambiato = false;
					if (oldDetailDoc.getIdTipologiaProcedimento() != currentTipoProcedimentoId.intValue()) {
						tipoProcedimentoCambiato = true;
					}

					if (!tipologiaDocumentoCambiata && !tipoProcedimentoCambiato) {
						LOGGER.info("aggiornaDocumento -> Si chiama il metodo per l'aggiornamento dei dati del documento in entrata.");
						aggiornaDatiDocumento(status, docInput, utente, fceh, fpeh, con);
					} else {
						// È variata la tipologia del documento
						if (tipologiaDocumentoCambiata) {
							LOGGER.info("aggiornaDocumento -> Si chiama il metodo per la gestione del cambio della tipologia documento.");
							cambiaTipologiaDocumento(status, docInput, oldDetailDoc.getDocumentClass(), currentTipologiaDocumentoId, utente, fceh, con);

							// Si procede con l'aggiornamento del documento
							LOGGER.info(
									AGGIORNA_DOCUMENTO_SI_CHIAMA_IL_METODO_PER_L_AGGIORNAMENTO_DEI_DATI_AGGIORNAMENTO_METADATI_CE_PE_TRASFORMAZIONE_PDF_ETC + DEL_DOCUMENTO);
							aggiornaDatiDocumento(status, docInput, utente, fceh, fpeh, con);
						}
						// È variata la tipologia del procedimento -> si aggiorna il documento e si
						// gestisce la variazione del workflow
						if (tipoProcedimentoCambiato) {
							// Si procede con l'aggiornamento del documento, solo se non è già stato
							// eseguito durante il cambio della tipologia del documento
							if (!tipologiaDocumentoCambiata) {
								LOGGER.info(AGGIORNA_DOCUMENTO_SI_CHIAMA_IL_METODO_PER_L_AGGIORNAMENTO_DEI_DATI_AGGIORNAMENTO_METADATI_CE_PE_TRASFORMAZIONE_PDF_ETC
										+ DEL_DOCUMENTO);
								aggiornaDatiDocumento(status, docInput, utente, fceh, fpeh, con);
							}

							// Si procede con la modifica del workflow
							LOGGER.info("aggiornaDocumento -> Si chiama il metodo per l'aggiornamento del workflow.");
							wob = cambiaTipoProcedimento(status, docInput, oldDetailDoc.getIdTipologiaProcedimento(), wob, currentTipoProcedimentoId, idUfficioDestinatario,
									idUtenteDestinatario, utente, fpeh, con);
						}
					}
					// Documento in uscita o mozione
				} else {
					// Se non sono cambiate le combo di Tipologia Documento/Tipo Procedimento, si
					// esegue solo la modifica del documento
					if (!tipologiaDocumentoCambiata) {
						// Si procede con l'aggiornamento del documento in uscita
						LOGGER.info(AGGIORNA_DOCUMENTO_SI_CHIAMA_IL_METODO_PER_L_AGGIORNAMENTO_DEI_DATI_AGGIORNAMENTO_METADATI_CE_PE_TRASFORMAZIONE_PDF_ETC + DEL_DOCUMENTO);
						aggiornaDatiDocumento(status, docInput, utente, fceh, fpeh, con);
					} else {
						// È variata la tipologia documento, ma non la tipologia procedimento
						LOGGER.info("aggiornaDocumento -> Si chiama il metodo per la gestione del cambio della tipologia del documento");
						cambiaTipologiaDocumento(status, docInput, oldDetailDoc.getDocumentClass(), currentTipologiaDocumentoId, utente, fceh, con);

						// Si procede con l'aggiornamento del documento in uscita
						LOGGER.info(AGGIORNA_DOCUMENTO_SI_CHIAMA_IL_METODO_PER_L_AGGIORNAMENTO_DEI_DATI_AGGIORNAMENTO_METADATI_CE_PE_TRASFORMAZIONE_PDF_ETC + DEL_DOCUMENTO);
						aggiornaDatiDocumento(status, docInput, utente, fceh, fpeh, con);
					}
				}

				// ### MODIFICA EVENTUALE DEL FASCICOLO - Aggiunta di eventuali fascicoli o
				// spostamento in altro fascicolo
				final List<FascicoloDTO> inputFascicoli = docInput.getFascicoli();
				final List<FascicoloDTO> fascicoliAssociati = fceh.getFascicoliDocumento(oldDetailDoc.getDocumentTitle(), utente.getIdAoo().intValue());

				// Se il numero di fascicoli presenti nel documento in input è maggiore di
				// quelli attualmente associati su FileNet,
				// allora si fascicola il documento anche nei nuovi fascicoli (cioè i fascicoli
				// non presenti tra quelli associati)
				if (inputFascicoli.size() > fascicoliAssociati.size()) {
					LOGGER.info("aggiornaDocumento -> Numero di fascicoli in input maggiore del numero di fascicoli attualmente associati:"
							+ " si inserisce il documento nei restanti fascicoli. Documento: " + idDocumento);

					eseguiFascicolazione(fceh, con, idDocumento, idAoo, inputFascicoli, fascicoliAssociati);
				} else {
					LOGGER.info("aggiornaDocumento -> Si verifica se è necessario aggiornare il fascicolo procedimentale per il documento: " + idDocumento);
					// ### GESTIONE FASCICOLO PROCEDIMENTALE - Spostamento del documento se il
					// fascicolo è completamente cambiato
					// Si chiama il metodo di aggiornamento
					final FascicoloDTO oldFascicolo = fascicoloSRV.getFascicoloProcedimentale(oldDetailDoc.getDocumentTitle(), utente.getIdAoo().intValue(), utente);
					aggiornaFascicoloProcedimentale(docInput, inputFascicoli.get(0), oldFascicolo, utente, idAoo, fceh, fpeh, con);
				}
				// ### GESTIONE CONOSCENZA
			} else if (!assegnazioneCompetenza && assegnazioneConoscenza && !assegnazioneContributo) {
				// Gestione dell'assegnazione Conoscenza solo se l'assegnatario è un ufficio ed
				// è stata fatta una riassegnazione
				if (assConoscenzaAggiornata) {
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY), idUfficioNuovaAssegnazione);
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY), idUtenteNuovaAssegnazione);
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), docInput.getMotivoAssegnazione());
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY), TipoAssegnazioneEnum.CONOSCENZA.getId());

					workflowResponse = ResponsesRedEnum.ASSEGNA;
				} else {
					throw new RedException("È possibile modificare l'assegnazione solo verso un Ufficio e successivamente ad una riassegnazione.");
				}
				// ### GESTIONE CONTRIBUTO
				// Gestione dell'assegnazione Contributo solo se l'assegnatario è un ufficio ed
				// è stata fatta una riassegnazione
			} else if (!assegnazioneCompetenza && !assegnazioneConoscenza && assegnazioneContributo && assContributoAggiornata) {
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY), idUfficioNuovaAssegnazione);
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY), idUtenteNuovaAssegnazione);
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), docInput.getMotivoAssegnazione());
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY), TipoAssegnazioneEnum.CONTRIBUTO.getId());

				workflowResponse = ResponsesRedEnum.ASSEGNA;
			} else if (docInput.isModificaMetadatiMinimi()) {
				gestisciModificaMetadatiMinimi(docInput, utente, fceh, dwhCon);
			}

			// ### GESTIONE DEGLI ALLEGATI DEL DOCUMENTO
			gestisciModificaAllegati(status, docInput, utente, fceh, fpeh, con, dwhCon);

			// ### AVANZAMENTO DEL WORKFLOW
			avanzaWF(status, wob, docInput, utente, fceh, fpeh, con, idDocumento, isRiservato, assegnazioneAggiornata, metadatiWorkflow, workflowResponse);

			// ### GESTIONE APPROVAZIONI
			if (status.isModalitaUpdate() && !CollectionUtils.isEmpty(docInput.getApprovazioni())) {
				for (final ApprovazioneDTO approvazione : docInput.getApprovazioni()) {
					approvazioneDAO.aggiornaStampigliatura(approvazione.getStampigliaturaFirma(), approvazione.getIdApprovazione(), con);
				}
			}

			// ### GESTIONE MAIL SPEDIZIONE
			if (!status.isDocEntrata() && !status.isDocInterno() && !status.isDocAssegnazioneInterna()) {
				documentoMailGuid = creaEmailSpedizioneSuFilenet(status, docInput.getDocumentTitle(), docInput.getMailSpedizione(), utente, fceh, con);
				if (documentoMailGuid != null) {
					LOGGER.info("aggiornaDocumento -> Creazione mail per i destinatari elettronici completata.");
				}
			}

			// ### GENERAZIONE HTML E ANNOTATION
			creaPreviewEImpostaAnnotationPreview(status, idDocumento, docInput, utente, fceh);

			// ## GESTIONE FAD
			if (tipologiaDocAttoDecreto.equals(docInput.getTipologiaDocumento().getIdTipologiaDocumento())
					&& tipoProcedimentoDocAttoDecretoManuale.equals(docInput.getTipoProcedimento().getTipoProcedimentoId())) {
				gestisciRaccoltaProvvisoria(docInput, oldDetailDoc.getIdraccoltaFAD(), wob, utente, con);
			}

			// ### GESTIONE NPS - AGGIORNAMENTO OGGETTO PROTOCOLLO E TIPOLOGIA DOCUMENTO
			handleNPS(status, docInput, utente, con);

			// ### METADATI ESTESI -> START
			salvaMetadatiEstesi(idDocumento, docInput, status.isModalitaInsert(), utente.getIdAoo(), con, fceh);
			// ### METADATI ESTESI -> END

			// ### METADATI REGISTRO AUSILIARIO -> START
			salvaMetadatiRegistroAusiliario(docInput, status.isModalitaInsert(), con);
			// ### METADATI REGISTRO AUSILIARIO -> END
		} catch (final Exception e) {
			LOGGER.error("aggiornaDocumento -> Si è verificato un errore durante l'aggiornamento del documento: " + idDocumento, e);
			// Se la mail del tab Spedizione è stata inserita su FileNet, la si cancella
			if (StringUtils.isNotBlank(documentoMailGuid)) {
				fceh.eliminaVersioneDocumentoByGuid(documentoMailGuid);
			}

			throw new RedException("aggiornaDocumento -> Si è verificato un errore durante l'aggiornamento del documento: " + idDocumento, e);
		}

		LOGGER.info("aggiornaDocumento -> END. Documento: " + idDocumento);
	}

	private void handleNPS(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente, final Connection con) {
		final String infoProtocollo = new StringBuilder().append(docInput.getNumeroProtocollo()).append("/").append(docInput.getAnnoProtocollo()).append("_")
				.append(utente.getCodiceAoo()).toString();
		
		if (TipologiaProtocollazioneEnum.isProtocolloNPS(utente.getIdTipoProtocollo()) && docInput.getNumeroProtocollo() != null && docInput.getNumeroProtocollo() != 0
				&& (status.isAggiornaOggettoProtocolloNPS() || status.isAggiornaTipologiaDocumentoNPS() || status.isTitolarioChanged()
						|| status.isAggiornaMetadatiEstesiNPS())) {

			String titolario = null;
			if (Boolean.TRUE.equals(status.isTitolarioChanged())) {
				titolario = docInput.getIndiceClassificazioneFascicoloProcedimentale() + " - " + docInput.getDescrizioneIndiceClassificazioneFascicoloProcedimentale();
			}

			String oggetto = null;
			if (status.isAggiornaOggettoProtocolloNPS()) {
				oggetto = docInput.getOggetto();
			}

			String descrizioneTipologiaDocumento = null;
			if (status.isAggiornaTipologiaDocumentoNPS()) {
				descrizioneTipologiaDocumento = docInput.getTipologiaDocumento().getDescrizione();
			}

			Collection<MetadatoDTO> metadatiEstesi = null;
			if (status.isAggiornaMetadatiEstesiNPS()) {
				metadatiEstesi = docInput.getMetadatiEstesi();
			}

			npsSRV.aggiornaDatiProtocollo(utente, infoProtocollo, docInput.getIdProtocollo(), oggetto, docInput.getTipologiaDocumento().getIdTipologiaDocumento(),
					(int) docInput.getTipoProcedimento().getTipoProcedimentoId(), descrizioneTipologiaDocumento, titolario, docInput.getDocumentTitle(), metadatiEstesi, con);
		
		}
		
		if (status.isAggiornaRiservatezza()) {
			npsSRV.aggiornaRiservatezza(utente, infoProtocollo, docInput.getIdProtocollo(), status.getSecurityDoc(), docInput.getIsRiservato(), docInput.getDocumentTitle());
		}
	}

	private void avanzaWF(final SalvaDocumentoRedStatusDTO status, final VWWorkObject wob, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente, final IFilenetCEHelper fceh,
			final FilenetPEHelper fpeh, final Connection con, final int idDocumento, final boolean isRiservato, final boolean assegnazioneAggiornata,
			final Map<String, Object> metadatiWorkflow, final ResponsesRedEnum workflowResponse) {
		// Se è cambiata un'assegnazione, si deve avanzare il workflow
		if (assegnazioneAggiornata) {
			LOGGER.info("aggiornaDocumento -> Almeno un'assegnazione è cambiata, si procede con l'aggiornamento delle security e l'avanzamento del workflow.");

			// Si avanza il workflow anche se le security (securityFascicolo e securityDoc)
			// non sono valorizzate (SU NSD: DA RIVEDERE......CON RAFFAELE)
			if (status.getSecurityFascicolo() == null && status.getSecurityDoc() == null) {
				// Si avanza il workflow
				fpeh.nextStep(wob, metadatiWorkflow, workflowResponse.getResponse());
			} else {
				// Si modificano le security sul fascicolo
				final boolean modificaSecFascicolo = securitySRV.modificaSecurityFascicoli(utente, status.getSecurityFascicolo(), docInput.getDocumentTitle());
				if (modificaSecFascicolo) {
					// Si modificano le security sul documento
					securitySRV.aggiornaSecurityDocumento(docInput.getDocumentTitle(), utente.getIdAoo(), status.getSecurityDoc(), false, false, fceh);

					// Si avanza il workflow
					fpeh.nextStep(wob, metadatiWorkflow, workflowResponse.getResponse());
				}

				if (!CollectionUtils.isEmpty(docInput.getAllacci())) {
					LOGGER.info("aggiornaDocumento -> Si procede con l'aggiornamento delle security dei documenti allacciati.");
					securitySRV.aggiornaSecurityDocumentiAllacciati(utente, idDocumento, status.getSecurityDocAllacciati(), status.getSecurityFascicoloDocAllacciati(),
							fceh, con);
				}
			}
		} else {
			LOGGER.info("Nessuna assegnazione è stata cambiata, si procede con l'aggiornamento delle security. Riservato: " + isRiservato);

			// Si ottengono le security per il documento
			status.setSecurityDoc(securitySRV.getSecurityPerAggiornamento(utente, docInput.getDocumentTitle(), String.valueOf(utente.getId()),
					String.valueOf(utente.getIdUfficio()), isRiservato, con));

			// Si modificano delle security sul documento
			securitySRV.aggiornaSecurityDocumento(docInput.getDocumentTitle(), utente.getIdAoo(), status.getSecurityDoc(), false, false, fceh);
		}
	}

	/**
	 * @param fceh
	 * @param con
	 * @param idDocumento
	 * @param idAoo
	 * @param inputFascicoli
	 * @param fascicoliAssociati
	 */
	private void eseguiFascicolazione(final IFilenetCEHelper fceh, final Connection con, final int idDocumento, final Long idAoo, final List<FascicoloDTO> inputFascicoli,
			final List<FascicoloDTO> fascicoliAssociati) {
		try {
			for (final FascicoloDTO inputFascicolo : inputFascicoli) {
				boolean trovato = false;
				for (final FascicoloDTO fascicoloAssociato : fascicoliAssociati) {
					if (fascicoloAssociato.getIdFascicolo().equals(inputFascicolo.getIdFascicolo())) {
						trovato = true;
						break;
					}
				}
				if (!trovato) {
					fascicoloSRV.fascicola(inputFascicolo.getIdFascicolo(), idDocumento, TipoFascicolo.NON_AUTOMATICO, idAoo, fceh, con);
				}
			}
		} catch (final Exception e) {
			LOGGER.error("aggiornaDocumento -> Si è verificato un errore durante l'aggiornamento dei fascicoli per il documento: " + idDocumento, e);
			throw new RedException("aggiornaDocumento -> Si è verificato un errore durante l'aggiornamento dei fascicoli per il documento: " + idDocumento, e);
		}
	}

	private DetailDocumentRedDTO getOldDetailRedPerAggiornamento(final String documentTitle, final Long idAoo, final IFilenetCEHelper fceh,
			final boolean recuperaMetadatoRaccoltaFAD) {
		DetailDocumentRedDTO oldDetailDoc = null;

		if (StringUtils.isNotBlank(documentTitle) && idAoo != null) {
			final PropertyFilter pfOldDetailDoc = new PropertyFilter();
			pfOldDetailDoc.addIncludeProperty(new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), null));
			pfOldDetailDoc.addIncludeProperty(new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY), null));
			pfOldDetailDoc.addIncludeProperty(new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY), null));
			pfOldDetailDoc.addIncludeProperty(new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY), null));
			pfOldDetailDoc.addIncludeProperty(new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY), null));
			pfOldDetailDoc.addIncludeProperty(new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.URGENTE_METAKEY), null));
			pfOldDetailDoc.addIncludeProperty(new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY), null));
			pfOldDetailDoc.addIncludeProperty(new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY), null));
			pfOldDetailDoc.addIncludeProperty(new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY), null));
			pfOldDetailDoc.addIncludeProperty(new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY), null));
			if (recuperaMetadatoRaccoltaFAD) {
				pfOldDetailDoc.addIncludeProperty(new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.ID_RACCOLTA_FAD_METAKEY), null));
			}

			pfOldDetailDoc.addIncludeProperty(new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.OBJECTID_CONTATTO_MITT_METADATO), null));
			pfOldDetailDoc.addIncludeProperty(new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.OBJECTID_CONTATTO_DEST_METADATO), null));
			pfOldDetailDoc.addIncludeProperty(new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY), null));
			pfOldDetailDoc.addIncludeProperty(new FilterElement(null, null, null, pp.getParameterByKey(PropertiesNameEnum.NOTE_METAKEY), null));

			final FilterElement[] feOldDetailDoc = pfOldDetailDoc.getIncludeProperties();
			final List<String> selectListOldDetailDoc = new ArrayList<>(feOldDetailDoc.length);
			for (final FilterElement filterElement : feOldDetailDoc) {
				selectListOldDetailDoc.add(filterElement.getValue());
			}

			final Document oldDetailDocFilenet = fceh.getDocumentByIdGestionale(documentTitle, selectListOldDetailDoc, pfOldDetailDoc, idAoo.intValue(), null, null);

			if (oldDetailDocFilenet != null) {
				oldDetailDoc = TrasformCE.transform(oldDetailDocFilenet, TrasformerCEEnum.FROM_DOCUMENTO_TO_UPDATE_DETAIL_RED);
			}
		}

		return oldDetailDoc;
	}

	private void aggiornaFascicoloProcedimentale(final SalvaDocumentoRedDTO docInput, final FascicoloDTO newFascicoloProcedimentale,
			final FascicoloDTO oldFascicoloProcedimentale, final UtenteDTO utente, final Long idAoo, final IFilenetCEHelper fceh, final FilenetPEHelper fpeh,
			final Connection con) {
		try {
			fascicoloSRV.aggiornaFascicoloProcedimentale(docInput, newFascicoloProcedimentale, oldFascicoloProcedimentale, utente, idAoo, fceh, fpeh, con);
			// Aggiornamento del fascicolo procedimentale
			docInput.setFascicoloProcedimentale(newFascicoloProcedimentale);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'aggiornamento del fascicolo procedimentale.", e);
			throw new RedException(e);
		}
	}

	private void aggiornaDatiDocumento(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente, final IFilenetCEHelper fceh,
			final FilenetPEHelper fpeh, final Connection con) {
		final String idDocumento = docInput.getDocumentTitle();
		LOGGER.info("aggiornaDatiDocumento -> START. Documento: " + idDocumento);

		try {
			// ### Si aggiornano i metadati (CE) del documento
			final Map<String, Object> metadatiDoc = caricaMetadatiDocumento(status, docInput, utente);
			fceh.updateMetadatiOnCurrentVersion(utente.getIdAoo(), Integer.parseInt(idDocumento), metadatiDoc);

			// ### Se esistono destinatari interni, si aggiornano (sempre) i workflows
			if (status.getDestinatariInterniString() != null && status.getDestinatariInterniString().length > 0) {
				final Map<String, Object> metadatiWorkflow = new HashMap<>();
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_DESTINATARI_INTERNI_METAKEY), status.getDestinatariInterniString());

				// Si aggiornano i workflows del documento
				fpeh.updateWorkflow(Integer.parseInt(idDocumento), metadatiWorkflow);
			}

			// ### Se il documento è stato aggiornato ed inviato in firma/sigla/visto, si
			// deve procedere alla ri-trasformazione PDF
			final TipoAssegnazioneEnum currentTipoAssegnazione = docInput.getTipoAssegnazioneEnum();
			if (status.isContentScannerizzato() || (status.isContentVariato() && (TipoAssegnazioneEnum.COMPETENZA.equals(currentTipoAssegnazione)
					|| TipoAssegnazioneEnum.FIRMA.equals(currentTipoAssegnazione) || TipoAssegnazioneEnum.FIRMA_MULTIPLA.equals(currentTipoAssegnazione)
					|| TipoAssegnazioneEnum.SIGLA.equals(currentTipoAssegnazione) || TipoAssegnazioneEnum.VISTO.equals(currentTipoAssegnazione)
					|| TipoAssegnazioneEnum.RIFIUTO_FIRMA.equals(currentTipoAssegnazione) || TipoAssegnazioneEnum.RIFIUTO_FIRMA_MULTIPLA.equals(currentTipoAssegnazione)
					|| TipoAssegnazioneEnum.RIFIUTO_SIGLA.equals(currentTipoAssegnazione) || TipoAssegnazioneEnum.RIFIUTO_VISTO.equals(currentTipoAssegnazione)))) {
				// Si ottengono i firmatari
				final String[] assegnazioniFirmatariArray = DocumentoUtils.initArrayAssegnazioni(TipoAssegnazioneEnum.FIRMA, docInput.getAssegnazioni());
				final String[] assegnazioniFirmatariMultiplaArray = DocumentoUtils.initArrayAssegnazioni(TipoAssegnazioneEnum.FIRMA_MULTIPLA, docInput.getAssegnazioni());

				// Esecuzione della trasformazione PDF in modalità  sincrona
				eseguiTrasformazionePDF(status, docInput.getDocumentTitle(), utente, ArrayUtils.addAll(assegnazioniFirmatariArray, assegnazioniFirmatariMultiplaArray), null,
						null, null, null, true, true, true, docInput.getTipoAssegnazioneEnum(), null, con);
			}
		} catch (final Exception e) {
			LOGGER.error("aggiornaDatiDocumento -> Si è verificato un errore durante la modifica del documento: " + idDocumento, e);
			throw new RedException("aggiornaDatiDocumento -> Si è verificato un errore durante la modifica del documento: " + idDocumento, e);
		}

		LOGGER.info("aggiornaDatiDocumento -> END. Documento: " + idDocumento);
	}

	private void cambiaTipologiaDocumento(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final String oldClasseDocumentale,
			final int currentTipologiaDocumentoId, final UtenteDTO utente, final IFilenetCEHelper fceh, final Connection con) {
		final String idDocumento = docInput.getDocumentTitle();
		LOGGER.info("aggiornaTipologiaDocumento -> START. Documento: " + idDocumento);

		Document documentoFilenet = null;
		try {
			final String classeDocumentale = getClasseDocumentaleDoc(status, currentTipologiaDocumentoId, con);

			// ### AGGIORNAMENTO DELLA TIPOLOGIA DOCUMENTO SU FILENET --- Si aggiorna la
			// classe documentale se è diversa dalla precedente
			if (classeDocumentale != null && !classeDocumentale.equals(oldClasseDocumentale)) {
				// STEP 1 - Si ottengono i metadati
				final Map<String, Object> metadatiDocumento = caricaMetadatiAggiornamentoDocumento(status, docInput, utente);

				// Si controlla se bisogna inserire un content (QUI...?!)
				if (docInput.getContent() != null) {
					status.getDocDaSalvare().setDataHandler(FileUtils.toDataHandler(docInput.getContent(), docInput.getContentType()));
					status.getDocDaSalvare().setContentType(docInput.getContentType());
				}

				// STEP 2 - Si effettua l'aggiornamento della classe documentale
				documentoFilenet = fceh.cambiaClasseDocumentale(idDocumento, classeDocumentale, metadatiDocumento, null, utente.getIdAoo());
			}

			// ### AGGIORNAMENTO DELLA TIPOLOGIA DOCUMENTO SU DB
			documentoDAO.aggiornaTipologiaDocumento(NumberUtils.toInt(idDocumento), currentTipologiaDocumentoId, con);
		} catch (final Exception e) {
			LOGGER.error("aggiornaTipologiaDocumento -> Si è verificato un errore durante l'aggiornamento della tipologia del documento: "
					+ " si procede con il ripristino della versione precedente del documento. Documento: " + idDocumento, e);

			// Si ripristina la versione precedente del documento
			IFilenetCEHelper fcehAdmin = null;
			try {
				fcehAdmin = getFCEHAdmin(utente.getFcDTO(), utente.getIdAoo());
				fcehAdmin.eliminaUltimaVersione(documentoFilenet);
				LOGGER.info("aggiornaTipologiaDocumento -> è stato effettuato il ripristino della versione precedente del documento: " + idDocumento);
			} catch (final Exception ex) {
				LOGGER.error("aggiornaTipologiaDocumento -> Si è verificato un errore durante l'eliminazione da FileNet dell'ultima versione del documento: " + idDocumento
						+ ". Il ripristino della versione precedente non può essere eseguito.", ex);
			} finally {
				popSubject(fcehAdmin);
			}

			throw new RedException("aggiornaTipologiaDocumento -> Si è verificato un errore durante l'aggiornamento della tipologia del documento: " + idDocumento, e);
		}
		LOGGER.info("aggiornaTipologiaDocumento -> END. Documento: " + idDocumento);
	}

	private void gestisciModificaAllegati(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente, final IFilenetCEHelper fceh,
			final FilenetPEHelper fpeh, final Connection con, final Connection dwhCon) {
		final String idDocumento = docInput.getDocumentTitle();
		LOGGER.info("gestisciModificaAllegati -> START. Documento: " + idDocumento);
		List<DocumentoRedFnDTO> inputAllegatiRedFn;
		List<DocumentoRedFnDTO> allegatiDaAggiornare;
		List<DocumentoRedFnDTO> allegatiDaInserire;
		List<String> idAllegatiDaTrasformare;
		final Long idAoo = utente.getIdAoo();

		try {
			inputAllegatiRedFn = allegatoSRV.convertToAllegatiFilenet(docInput.getAllegati(), idAoo, con); // Lista degli allegati in input
		} catch (final Exception e) {
			LOGGER.error(SI_E_VERIFICATO_UN_ERRORE_DURANTE_LA_CREAZIONE_DEGLI_ALLEGATI_DEL_DOCUMENTO, e);
			throw new RedException(SI_E_VERIFICATO_UN_ERRORE_DURANTE_LA_CREAZIONE_DEGLI_ALLEGATI_DEL_DOCUMENTO, e);
		}

		// Si recupera il documento principale da FileNet
		Document docPrincipaleFilenet = null;
		if (StringUtils.isNotBlank(idDocumento)) {
			docPrincipaleFilenet = fceh.getDocumentByIdGestionale(idDocumento, null, null, idAoo.intValue(), null,
					getClasseDocumentaleDoc(status, docInput.getTipologiaDocumento().getIdTipologiaDocumento(), con));
		} else {
			docPrincipaleFilenet = fceh.getDocumentByGuid(docInput.getGuid());
		}

		List<String> placeholderUnici = null;
		if (!CollectionUtils.isEmpty(docInput.getAllegati())) {
			placeholderUnici = new ArrayList<>();
			for (final AllegatoDTO allegatoSigla : docInput.getAllegati()) {
				if (StringUtils.isNotBlank(allegatoSigla.getPlaceholderSigla())) {
					final String[] placeholderUnico = allegatoSigla.getPlaceholderSigla().split(Pattern.quote("||"));
					placeholderUnici.addAll(Arrays.asList(placeholderUnico));
				}
			}
		}

		String placeholderNuovo = Constants.EMPTY_STRING;
		if (!CollectionUtils.isEmpty(placeholderUnici)) {
			final Set<String> setPH = new LinkedHashSet<>(placeholderUnici);
			placeholderNuovo = StringUtils.join(setPH.toArray(), "||");
		}
		if (StringUtils.isNotBlank(placeholderNuovo)) {
			stampigliaturaSiglaDAO.updatePlaceholder(idDocumento, docInput.getIdUfficioCreatore(), docInput.getTipologiaDocumento().getIdTipologiaDocumento(),
					placeholderNuovo, con);
		} else {
			stampigliaturaSiglaDAO.deleteRowPlaceholderEmpty(idDocumento, docInput.getIdUfficioCreatore(), docInput.getIdUtenteCreatore(),
					docInput.getTipologiaDocumento().getIdTipologiaDocumento(), con);
		}

		// ### Gestione degli allegati eliminati -> START
		gestisciEliminazioneAllegati(docInput, inputAllegatiRedFn, docPrincipaleFilenet, utente, fceh, dwhCon);
		// ### Gestione degli allegati eliminati -> END

		// ### Gestione degli allegati inseriti/aggiornati
		if (inputAllegatiRedFn != null && !inputAllegatiRedFn.isEmpty()) {
			allegatiDaAggiornare = new ArrayList<>();
			allegatiDaInserire = new ArrayList<>();
			idAllegatiDaTrasformare = new ArrayList<>();

			for (final DocumentoRedFnDTO inputAllegatoRedFn : inputAllegatiRedFn) {
				final Boolean isFormatoOriginale = (Boolean) inputAllegatoRedFn.getMetadati().get(pp.getParameterByKey(PropertiesNameEnum.IS_FORMATO_ORIGINALE_METAKEY));
				final Integer idFormatoAllegato = (Integer) inputAllegatoRedFn.getMetadati().get(pp.getParameterByKey(PropertiesNameEnum.FORMATO_ALLEGATO_ID_METAKEY));
				final Integer daFirmare = (Integer) inputAllegatoRedFn.getMetadati().get(pp.getParameterByKey(PropertiesNameEnum.DA_FIRMARE_METAKEY));
				final String documentTitle = (String) inputAllegatoRedFn.getMetadati().get(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
				final String barcode = (String) inputAllegatoRedFn.getMetadati().get(pp.getParameterByKey(PropertiesNameEnum.BARCODE_METAKEY));
				final String guid = inputAllegatoRedFn.getGuid();
				final Boolean firmaVisibile = (Boolean) inputAllegatoRedFn.getMetadati().get(pp.getParameterByKey(PropertiesNameEnum.FIRMA_VISIBILE_METAKEY));

				// Inserimento nella lista degli allegati da firmare
				if (Boolean.FALSE.equals(isFormatoOriginale) && ((BooleanFlagEnum.SI.getIntValue().equals(daFirmare) && guid == null)
						|| ((status.getIterPdf() != null && status.getIterPdf().isIterAutomatico()) && guid == null))) {
					status.getIdAllegatiDaFirmare().add(documentTitle);
					// Inserimento nella lista degli allegati da visualizzare con campo firma
					if (Boolean.TRUE.equals(firmaVisibile)) {
						status.getIdAllegatiConFirmaVisibile().add(documentTitle);
					}
				}

				// Si controlla il barcode se la tipologia è CARTACEO
				if (TipoSpedizione.MEZZO_CARTACEO.equals(idFormatoAllegato)) {
					final Date dateStart = new Date();
					final boolean barcodeEsistente = !StringUtils.isBlank(fceh.controllaBarcode(barcode));
					if (barcodeEsistente) {
						throw new RedException("Il barcode: " + barcode + INSERITO_E_GIA_PRESENTE_NEL_SISTEMA);
					}
					final Date dateEnd = new Date();
					LOGGER.info("Timing gestisciModificaAllegati.controllaBarcode -> " + (dateEnd.getTime() - dateStart.getTime()) + "ms");
				}

				if (StringUtils.isNotBlank(guid)) {
					allegatiDaAggiornare.add(inputAllegatoRedFn);
				} else {
					allegatiDaInserire.add(inputAllegatoRedFn);
				}
			}

			// ### Gestione degli allegati da inserire (nuovi)
			if (!allegatiDaInserire.isEmpty()) {
				LOGGER.info("######INSERISCO ALLEGATI DOCUMENTO START #####");
				fceh.inserisciAllegati(docPrincipaleFilenet, allegatiDaInserire);
				LOGGER.info("######INSERISCO ALLEGATI DOCUMENTO END #####");
				for (final DocumentoRedFnDTO allegato : allegatiDaInserire) {
					idAllegatiDaTrasformare.add(allegato.getDocumentTitle());
				}
			}

			// ### Gestione degli allegati da aggiornare (esistenti)
			for (final DocumentoRedFnDTO allegatoDaAggiornare : allegatiDaAggiornare) {
				fceh.aggiornaAllegato(docPrincipaleFilenet, allegatoDaAggiornare, null, true, idAoo);

				if (status.getIdAllegatiDaFirmare().contains(allegatoDaAggiornare.getDocumentTitle())) {
					idAllegatiDaTrasformare.add(allegatoDaAggiornare.getDocumentTitle());
				}
			}

			// ### Si trasformano gli allegati
			if (!idAllegatiDaTrasformare.isEmpty()) {
				eseguiTrasformazionePDF(status, docInput.getDocumentTitle(), utente, ArrayUtils.addAll(status.getAssegnazioniFirma(), status.getAssegnazioniFirmaMultipla()),
						idAllegatiDaTrasformare, status.getIdAllegatiDaFirmare(), null, null, false, true, true, docInput.getTipoAssegnazioneEnum(),
						status.getIdAllegatiConFirmaVisibile(), con);
			}
		}

		// ### Si aggiornano i workflows con le eventuali informazioni relative alla
		// Copia Conforme
		aggiornaWorkflowsPerCopiaConforme(NumberUtils.toInt(idDocumento), docInput.getAllegati(), fpeh);

		// ### Si aggiorna il metadato relativo alla Firma PDF
		aggiornaMetadatoFirmaPDF(docPrincipaleFilenet, fceh);

		LOGGER.info("gestisciModificaAllegati -> END. Documento: " + idDocumento);
	}

	private void gestisciEliminazioneAllegati(final SalvaDocumentoRedDTO docInput, final List<DocumentoRedFnDTO> inputAllegatiRedFn, final Document docPrincipaleFilenet,
			final UtenteDTO utente, final IFilenetCEHelper fceh, final Connection dwhCon) {
		// Si recuperano gli allegati del documento attualemente presenti su FileNet
		final DocumentSet allegatiFilenet = fceh.getAllegatiByGuid(fceh.idFromGuid(docInput.getGuid()), Arrays.asList(PropertyNames.ID));

		// Se sono presenti allegati su FileNet, si controlla se sono stati eliminati
		if (allegatiFilenet != null && !allegatiFilenet.isEmpty()) {
			final List<String> guidAllegatiDaEliminare = new ArrayList<>();
			final Iterator<?> itAllegati = allegatiFilenet.iterator();
			while (itAllegati.hasNext()) {
				boolean trovato = false;
				final Document allegatoFilenet = (Document) itAllegati.next();
				final String allegatoFilenetGuidString = it.ibm.red.business.utils.StringUtils.cleanGuidToString(allegatoFilenet.get_Id());

				if (!CollectionUtils.isEmpty(inputAllegatiRedFn)) {
					for (final DocumentoRedFnDTO inputAllegatoRedFn : inputAllegatiRedFn) {
						if (inputAllegatoRedFn.getGuid() == null) {
							continue;
						} else if (allegatoFilenetGuidString.equals(inputAllegatoRedFn.getGuid())) {
							trovato = true;
							break;
						}
					}
				}

				if (!trovato) {
					guidAllegatiDaEliminare.add(allegatoFilenet.get_Id().toString());
				}
			}

			// Si rimuovono da FileNet gli allegati eliminati
			if (!CollectionUtils.isEmpty(guidAllegatiDaEliminare)) {
				IFilenetCEHelper admin = null;
				try {
					// Rimozione degli allegati da FileNet
					admin = getFCEHAdmin(utente.getFcDTO(), utente.getIdAoo());
					final List<Document> allegatiEliminati = admin.eliminaAllegatiByGuid(docPrincipaleFilenet, guidAllegatiDaEliminare);
					// Se ci sono allegati per Copia Conforme, se ne traccia la rimozione nello
					// storico
					tracciaEliminazioneAllegatiCopiaConforme(NumberUtils.toInt(docInput.getDocumentTitle()), utente, allegatiEliminati, dwhCon);
				} catch (final Exception e) {
					LOGGER.error("gestisciModificaAllegati -> Si è verificato un errore durante la rimozione da FileNet degli allegati da eliminare", e);
					throw new RedException(e);
				} finally {
					popSubject(admin);
				}
			}
		}
	}

	/**
	 * Aggiorna l'utente creatore della Raccolta Provvisoria (FAD) e aggiunge il
	 * documento a quest'ultima.
	 * 
	 * @param docInput
	 * @param utente
	 * @param wob
	 * @param con
	 */
	private void gestisciRaccoltaProvvisoria(final SalvaDocumentoRedDTO docInput, final String idRaccoltaFADDoc, final VWWorkObject wob, final UtenteDTO utente,
			final Connection con) {
		if (wob != null) {
			final RaccoltaFadDTO fadDTO = new RaccoltaFadDTO();
			fadDTO.setDocumentTitle(docInput.getDocumentTitle());
			fadDTO.setNumeroProtocollo(String.valueOf(docInput.getNumeroProtocollo()));
			fadDTO.setAnnoProtocollo(String.valueOf(docInput.getAnnoProtocollo()));
			fadDTO.setDataProtocolloFromManuale(docInput.getDataProtocollo());
			if (wob.getWorkflowNumber() != null) {
				fadDTO.setWobNumber(wob.getWorkflowNumber());
			}

			String idFascicoloRaccoltaProvvisoria = idRaccoltaFADDoc;
			if (idFascicoloRaccoltaProvvisoria != null) {

				fadDTO.setIdFascicoloRaccoltaProvvisoria(idFascicoloRaccoltaProvvisoria);

				// Si aggiorna l'utente creatore della Raccolta Provvisoria
				fepaSRV.updateCreatoreRaccoltaProvvisoria(fadDTO, idFascicoloRaccoltaProvvisoria, true, utente, null, null);
			} else {
				idFascicoloRaccoltaProvvisoria = docInput.getIdRaccoltaFAD();

				if (idFascicoloRaccoltaProvvisoria == null) {
					idFascicoloRaccoltaProvvisoria = (String) TrasformerPE.getMetadato(wob, pp.getParameterByKey(PropertiesNameEnum.ID_RACCOLTA_PROVVISORIA_METAKEY));
				}
			}

			fadDTO.setIdFascicoloRaccoltaProvvisoria(idFascicoloRaccoltaProvvisoria);

			// Si aggiunge il documento alla Raccolta Provvisoria
			fepaSRV.addDocumentoFascicoloRaccoltaProvvisoria(fadDTO, utente, true, con);
		}
	}

	/**
	 * Se l'allegato è per copia conforme ed è firmato, allora si traccia nello
	 * storico la cancellazione effettuata.
	 * 
	 * @param idDocumento
	 * @param utente
	 * @param allegatiEliminati
	 * @param dwhCon
	 */
	private void tracciaEliminazioneAllegatiCopiaConforme(final int idDocumento, final UtenteDTO utente, final List<Document> allegatiEliminati, final Connection dwhCon) {
		LOGGER.info("tracciaEliminazioneAllegatiCopiaConforme -> START. Documento: " + idDocumento);

		for (final Document allegatoEliminato : allegatiEliminati) {
			// Si verifica se l'allegato è per Copia Conforme
			final Boolean isCopiaConforme = (Boolean) TrasformerCE.getMetadato(allegatoEliminato, PropertiesNameEnum.IS_COPIA_CONFORME_METAKEY);
			if (Boolean.TRUE.equals(isCopiaConforme)) {
				// Si verifica la presenza della firma
				final PkHandler pkHandler = utente.getSignerInfo().getPkHandlerVerifica();
				final SignHelper signHelper = new SignHelper(pkHandler.getHandler(), pkHandler.getSecurePin(),utente.getDisableUseHostOnly());
				final VerifyInfoDTO verifyInfo = signHelper.infoFirmaDocumento(allegatoEliminato.accessContentStream(0), false);

				// Se l'allegato è per Copia Conforme e la firma è presente, si inserisce
				// l'evento
				if (ValueMetadatoVerificaFirmaEnum.FIRMA_OK.getValue().equals(verifyInfo.getMetadatoValiditaFirmaValue())) {
					eventoLogSRV.inserisciEventoLog(idDocumento, utente.getIdUfficio(), utente.getId(), 0L, 0L, Calendar.getInstance().getTime(),
							EventTypeEnum.ELIMINAZIONE_ALLEGATO_COPIA_CONFORME, "Allegato per copia conforme cancellato", 0, utente.getIdAoo(), dwhCon);
				}
			}
		}

		LOGGER.info("tracciaEliminazioneAllegatiCopiaConforme -> END. Documento: " + idDocumento);
	}

	private void aggiornaWorkflowsPerCopiaConforme(final int idDocumento, final List<AllegatoDTO> allegati, final FilenetPEHelper fpeh) {
		LOGGER.info("aggiornaWorkflowsPerCopiaConforme -> START. Documento: " + idDocumento);

		if (!CollectionUtils.isEmpty(allegati)) {
			final Map<String, Object> metadatiWorkflow = new HashMap<>();

			for (final AllegatoDTO allegato : allegati) {
				if (allegato.getIdUfficioCopiaConforme() != null && allegato.getIdUtenteCopiaConforme() != 0) {
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.FIRMA_COPIA_CONFORME_METAKEY), Boolean.TRUE.equals(allegato.getCopiaConforme()));
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.UFFICIO_COPIA_CONFORME_WF_METAKEY), allegato.getIdUfficioCopiaConforme());
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.UTENTE_COPIA_CONFORME_WF_METAKEY), allegato.getIdUtenteCopiaConforme());

					try {
						fpeh.updateWorkflow(idDocumento, metadatiWorkflow);
					} catch (final Exception e) {
						LOGGER.error("aggiornaWorkflowsPerCopiaConforme -> Si è verificato un errore durante l'aggiornamento dei workflows del documento: " + idDocumento, e);
						throw new RedException("Si è verificato un errore durante l'aggiornamento del documento: " + idDocumento, e);
					}
				}
			}
		}

		LOGGER.info("aggiornaWorkflowsPerCopiaConforme -> END. Documento: " + idDocumento);
	}

	private void aggiornaMetadatoFirmaPDF(final Document docFilenet, final IFilenetCEHelper fceh) {
		final Map<String, Object> metadatiDocumento = new HashMap<>(1);

		boolean firmaPDF = true;
		if (!MediaType.PDF.toString().equalsIgnoreCase(docFilenet.get_MimeType())) {
			firmaPDF = false;
		} else {
			final DocumentSet allegati = fceh.getAllegatiConContentDaFirmare(it.ibm.red.business.utils.StringUtils.cleanGuidToString(docFilenet.get_Id()));
			final Iterator<?> itAllegati = allegati.iterator();
			while (itAllegati.hasNext()) {
				final Document allegato = (Document) itAllegati.next();
				if (FilenetCEHelper.hasDocumentContentTransfer(allegato)) {
					final String contentTypeAllegato = FilenetCEHelper.getDocumentContentTransfer(allegato).get_ContentType();
					if (!MediaType.PDF.toString().equalsIgnoreCase(contentTypeAllegato)) {
						firmaPDF = false;
						break;
					}
				}
			}
		}

		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.FIRMA_PDF_METAKEY), firmaPDF);

		// Si aggiorna il metadato su FileNet
		fceh.updateMetadati(docFilenet, metadatiDocumento);
	}

	private Map<String, Object> caricaMetadatiAggiornamentoDocumento(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente) {
		final Map<String, Object> metadatiDocumento = new HashMap<>();

		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.UTENTE_CREATORE_ID_METAKEY), utente.getId());
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY), utente.getIdUfficio());
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_APPLICAZIONE_KEY_METAKEY),
				Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.ID_APPLICAZIONE_VALUE_METAKEY)));
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY), docInput.getIdCategoriaDocumento());
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY), docInput.getTipologiaDocumento().getIdTipologiaDocumento());
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY), docInput.getTipoProcedimento().getTipoProcedimentoId());
		if (docInput.getMomentoProtocollazioneEnum() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.MOMENTO_PROTOCOLLAZIONE_ID_METAKEY), docInput.getMomentoProtocollazioneEnum().getId());
		}
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY), docInput.getOggetto());
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_MITTENTE_METAKEY), docInput.getProtocolloMittente());
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_MITTENTE_METAKEY), docInput.getAnnoProtocolloMittente());
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_MITTENTE_METAKEY), docInput.getDataProtocolloMittente());
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_RISPOSTA_STRING_METAKEY), docInput.getNumeroProtocolloRisposta());
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_RISPOSTA_METAKEY), docInput.getAnnoProtocolloRisposta());
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.BARCODE_METAKEY),
				StringUtils.isNotBlank(docInput.getBarcode()) ? docInput.getBarcode() : Constants.EMPTY_STRING);
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_FORMATO_DOCUMENTO), docInput.getFormatoDocumentoEnum().getId());
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_RACCOMANDATA_METAKEY), docInput.getNumeroRaccomandata());
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NOTE_METAKEY), docInput.getNote());
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY), docInput.getDataScadenza());
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.URGENTE_METAKEY),
				Boolean.TRUE.equals(docInput.getIsUrgente()) ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue());

		if (status.getAssegnazioniCompetenza().length > 0) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ASSEGNATARI_COMPETENZA_METAKEY), status.getAssegnazioniCompetenza());
		}
		if (status.getAssegnazioniConoscenza().length > 0) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ASSEGNATARI_CONOSCENZA_METAKEY), status.getAssegnazioniConoscenza());
		}
		if (status.getAssegnazioniContributo().length > 0) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ASSEGNATARI_CONTRIBUTO_METAKEY), status.getAssegnazioniContributo());
		}
		if (status.getDestinatariString() != null && status.getDestinatariString().length > 0) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY), status.getDestinatariString());
		}

		// Metadati Dinamici
		if (!CollectionUtils.isEmpty(docInput.getMetadatiDinamici())) {
			metadatiDocumento.putAll(docInput.getMetadatiDinamici());
		}

		return metadatiDocumento;
	}

	private VWWorkObject cambiaTipoProcedimento(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final Integer oldTipoProcedimentoId,
			final VWWorkObject currentWob, final Long currentTipoProcedimentoId, final Long idUfficioDestinatario, final Long idUtenteDestinatario, final UtenteDTO utente,
			final FilenetPEHelper fpeh, final Connection con) {
		LOGGER.info("aggiornaTipoProcedimento -> START");
		VWWorkObject newWob = currentWob;

		// Startare un nuovo workflow solo se è cambiato effettivamente il nome (non
		// l'identificativo)
		final String newWorkflowName = tipoProcedimentoDAO.getWorkflowNameByIdTipoProcedimento(currentTipoProcedimentoId, con);
		final String oldWorkflowName = tipoProcedimentoDAO.getWorkflowNameByIdTipoProcedimento(oldTipoProcedimentoId, con);

		LOGGER.info("Workflow del procedimento precedente: " + oldTipoProcedimentoId + " selezionato: " + oldWorkflowName);
		LOGGER.info("Workflow del procedimento corrente: " + currentTipoProcedimentoId + " selezionato: " + newWorkflowName);

		// Se il nome del workflow è diverso startare un nuovo processo
		if (StringUtils.isNotBlank(newWorkflowName) && StringUtils.isNotBlank(oldWorkflowName) && !newWorkflowName.equals(oldWorkflowName)) {
			LOGGER.info("aggiornaTipoProcedimento -> è necessario startare un nuovo workflow: " + newWorkflowName);

			// STEP 1 - Si cancella il workflow corrente
			fpeh.deleteWF(currentWob);

			// STEP 2 - Il nuovo workflow deve avere:
			// - La competenza corrente o, se cambiata, la nuova
			// - Le vecchie assegnazioni per conoscenza e contributo
			// - Le nuove assegnazioni per conoscenza e contributo, se aggiunte.
			final Map<String, Object> metadatiWorkflow = new HashMap<>();
			// Metadato ID Ufficio Mittente
			if (status.getIdUfficioMittenteWorkflow() != null) {
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), status.getIdUfficioMittenteWorkflow());
			} else {
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), utente.getIdUfficio());
			}
			// Metadato ID Utente Mittente
			if (status.getIdUtenteMittenteWorkflow() != null) {
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), status.getIdUtenteMittenteWorkflow());
			} else {
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId());
			}
			// Metadato ID Ufficio Destinatario
			if (status.getIdUfficioDestinatarioWorkflow() != null) {
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY), status.getIdUfficioDestinatarioWorkflow());
			} else {
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY), idUfficioDestinatario);
			}
			// Metadato ID Utente Destinatario
			if (status.getIdUtenteDestinatarioWorkflow() != null) {
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY), status.getIdUtenteDestinatarioWorkflow());
			} else {
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY), idUtenteDestinatario);
			}

			// Metadato Registro Riservato
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY),
					Boolean.TRUE.equals(docInput.getRegistroRiservato()) ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue());

			// Metadato ID Documento
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY), docInput.getDocumentTitle());

			// Metadato ID Fascicolo
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY), docInput.getFascicoloProcedimentale().getIdFascicolo());

			// Metadato Destinatari Documento
			if (status.getDestinatariInterniString() != null && status.getDestinatariInterniString().length > 0) {
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY), status.getDestinatariInterniString());
			}

			// Metadati ID Tipo Assegnazione/ID Formato Documento
			if (!docInput.getIdCategoriaDocumento().equals(CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0])) {
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY), TipoAssegnazioneEnum.COMPETENZA.getId());
			} else {
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_FORMATO_DOCUMENTO), docInput.getFormatoDocumentoEnum().getId());
			}

			final List<AssegnazioneDTO> oldAssegnazioni = fpeh.getAssegnazioni(docInput.getDocumentTitle(), utente.getCodiceAoo(), utente.getFcDTO().getIdClientAoo());

			final String[] assegnazioniConoscenzaTotali = ArrayUtils.addAll(DocumentoUtils.initArrayAssegnazioni(TipoAssegnazioneEnum.CONOSCENZA, oldAssegnazioni),
					status.getAssegnazioniConoscenza());
			final String[] assegnazioniContributoTotali = ArrayUtils.addAll(DocumentoUtils.initArrayAssegnazioni(TipoAssegnazioneEnum.CONTRIBUTO, oldAssegnazioni),
					status.getAssegnazioniContributo());

			// Metadato Item Assegnatari
			final String[] elencoAssegnazioni = DocumentoUtils.compattaAssegnazioni(status.getAssegnazioniCompetenza(), assegnazioniConoscenzaTotali,
					assegnazioniContributoTotali, status.getAssegnazioniFirma(), status.getAssegnazioniSigla(), status.getAssegnazioniVisto(),
					status.getAssegnazioniFirmaMultipla());
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ITEM_ASSEGNATARI_METAKEY), elencoAssegnazioni);

			// Metadato Motivazione Assegnazione
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), docInput.getMotivoAssegnazione());

			// Metadati Elenco Conoscenza/Elenco Conoscenza Storico
			if (assegnazioniConoscenzaTotali != null) {
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_CONOSCENZA_METAKEY), assegnazioniConoscenzaTotali);
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_CONOSCENZA_STORICO_METAKEY), assegnazioniConoscenzaTotali);
			}

			// Metadati Elenco Contributi/Elenco Contributi Storico
			if (assegnazioniContributoTotali != null) {
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_CONTRIBUTI_METAKEY), assegnazioniContributoTotali);
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_CONTRIBUTI_STORICO_METAKEY), assegnazioniContributoTotali);
			}

			// ### AVVIO DEL NUOVO WORKFLOW
			final String wobNumber = avviaNuovoWorkflow(status, null, docInput, utente, metadatiWorkflow, fpeh, con);
			if (wobNumber != null) {
				newWob = fpeh.getWorkFlowByWob(wobNumber, true);
			}
		}

		LOGGER.info("aggiornaTipoProcedimento -> END");
		return newWob;
	}

	private Map<String, Object> caricaMetadatiDocumento(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente) {
		final Map<String, Object> metadatiDocumento = new HashMap<>();

		// Si inseriscono i metadati Document Title, Data Archiviazione e ID AOO
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), docInput.getDocumentTitle());
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DATA_ARCHIVIAZIONE_METAKEY), new Date());
		if (docInput.getIdAOO() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY), docInput.getIdAOO());
		} else {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY), utente.getIdAoo());
		}

		// Metadato Iter Approvativo
		if (status.getIterPdf() != null && status.getIterPdf().getIdIterApprovativo() > 0) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ITER_APPROVATIVO_METAKEY), status.getIterPdf().getIdIterApprovativo().toString());
		}

		// Metadato ID Categoria Documento
		if (status.isDocInterno()) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY), CategoriaDocumentoEnum.DOCUMENTO_USCITA.getIds()[0]);
		} else if (docInput.getIdCategoriaDocumento() != null && docInput.getIdCategoriaDocumento() > 0) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY), docInput.getIdCategoriaDocumento());
		}

		// Metadato ID Formato Documento
		if (docInput.getFormatoDocumentoEnum() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_FORMATO_DOCUMENTO), docInput.getFormatoDocumentoEnum().getId());
		}

		// Metadato ID Tipologia Documento
		if (!status.isPrecensito() && docInput.getTipologiaDocumento() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY), docInput.getTipologiaDocumento().getIdTipologiaDocumento());
		}

		// Metadato ID Tipo Procedimento
		if (docInput.getTipoProcedimento() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY), docInput.getTipoProcedimento().getTipoProcedimentoId());
		}

		// Metadato ID Momento Protocollazione
		if (docInput.getMomentoProtocollazioneEnum() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.MOMENTO_PROTOCOLLAZIONE_ID_METAKEY), docInput.getMomentoProtocollazioneEnum().getId());
		}

		// Metadato Urgente
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.URGENTE_METAKEY),
				Boolean.TRUE.equals(docInput.getIsUrgente()) ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue());

		// Metadato Riservato
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY),
				Boolean.TRUE.equals(docInput.getIsRiservato()) ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue());

		// Metadato Oggetto
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY), docInput.getOggetto());

		// Metadato Numero Protocollo Mittente
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_MITTENTE_METAKEY), docInput.getProtocolloMittente());

		// Metadato Anno Protocollo Mittente
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_MITTENTE_METAKEY), docInput.getAnnoProtocolloMittente());

		// Metadato Data Protocollo Mittente
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_MITTENTE_METAKEY), docInput.getDataProtocolloMittente());

		// Metadato Nome File (se esiste il content)
		if (docInput.getContent() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), docInput.getNomeFile());
		}

		// Metadato Barcode
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.BARCODE_METAKEY),
				StringUtils.isNotBlank(docInput.getBarcode()) ? docInput.getBarcode() : Constants.EMPTY_STRING);

		// Metadato Numero Raccomandata
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_RACCOMANDATA_METAKEY), docInput.getNumeroRaccomandata());

		// Metadato Note
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NOTE_METAKEY), docInput.getNote());

		// Metadato Data Scadenza
		if (docInput.getDataScadenza() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY), docInput.getDataScadenza());
		}

		// Metadato Registro Riservato
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY),
				Boolean.TRUE.equals(docInput.getRegistroRiservato()) ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue());

		// Metadato Destinatario Contributo Esterno
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DESTINATARIO_CONTRIBUTO_ESTERNO), docInput.getDestinatarioContributoEsterno());

		// Metadato Flag Corte Conti
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.FLAG_CORTE_CONTI_METAKEY),
				Boolean.TRUE.equals(docInput.getFlagCorteConti()) ? Boolean.TRUE : Boolean.FALSE);

		if (!status.isModalitaUpdate() && !status.isDocInterno()) {
			// Metadati da modificare solo in modalità inserimento
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.UTENTE_CREATORE_ID_METAKEY), utente.getId() != null ? utente.getId() : 0);
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY), utente.getIdUfficio());

			if (docInput.getIdUtenteCoordinatore() != null && docInput.getIdUtenteCoordinatore() > 0 && docInput.getIdUfficioCoordinatore() != null
					&& docInput.getIdUfficioCoordinatore() > 0) {
				metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_UFFICIO_COORDINATORE_METAKEY), docInput.getIdUfficioCoordinatore());
				metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_COORDINATORE_METAKEY), docInput.getIdUtenteCoordinatore());
			}
		}

		// Metadato ID Applicazione
		if (StringUtils.isNotBlank(pp.getParameterByKey(PropertiesNameEnum.ID_APPLICAZIONE_VALUE_METAKEY))) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_APPLICAZIONE_KEY_METAKEY),
					Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.ID_APPLICAZIONE_VALUE_METAKEY)));
		}

		if (status.isDocEntrata() || status.isDocAssegnazioneInterna()) {
			// Metadato Mezzo Ricezione
			if (docInput.getMezzoRicezione() != null) {
				metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_MEZZORICEZIONE), docInput.getMezzoRicezione().getIdMezzoRicezione());
			} else {
				metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_MEZZORICEZIONE), 0);
			}
			// Metadato Mittente
			try {
				gestisciMetadatoMittente(docInput, utente, metadatiDocumento);
			} catch (final Exception e) {
				LOGGER.error("Errore durante la gestione del metadatoMittente : " + e);
			}
		} else {
			// Si aggiornano numero RDP e numero legislatura
			if (docInput.getNumeroRDP() != null && docInput.getNumeroLegislatura() != null) {
				metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_RDP_METAKEY), docInput.getNumeroRDP());
				metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_LEGISLATURA_METAKEY), docInput.getNumeroLegislatura());
			} else if (status.isModalitaUpdate() && docInput.getNumeroRDP() == null
					&& pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_GENERICO_CLASSNAME_FN_METAKEY).equals(docInput.getTipologiaDocumento().getDocumentClass())) {
				metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_RDP_METAKEY), 0);
				metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_LEGISLATURA_METAKEY), 0);
			}
		}

		if (!status.isModalitaUpdate() && status.isDocContributoEsterno()) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.METADATO_NUMERO_MITTENTE_CONTRIBUTO), docInput.getNumeroMittenteContributo());
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.METADATO_ANNO_MITTENTE_CONTRIBUTO), docInput.getAnnoMittenteContributo());
		}

		// Adeguamento per documento interno proveniente da fascicoli
		if (status.getDestinatariString() != null && status.getDestinatariString().length > 0) {
			if (!it.ibm.red.business.utils.CollectionUtils.isEmptyOrNull(docInput.getDestinatari())) {
				final List<Contatto> contattiList = new ArrayList<>();
				for (final DestinatarioRedDTO destinatario : docInput.getDestinatari()) {
					contattiList.add(destinatario.getContatto());
				}
				final IFilenetCEHelper fceh = getIFilenetHelper(utente.getIdAoo().intValue());
				try {
					if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(docInput.getObjectIdContattoDestOnTheFly())) {
						cancellaVecchioContDaFilenet(docInput.getObjectIdContattoDestOnTheFly(), fceh);
					}
					final CustomObject listContatti = salvaContatto(docInput.getObjectIdContattoDestOnTheFly(), fceh, contattiList);
					metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.OBJECTID_CONTATTO_DEST_METADATO), listContatti.get_Id().toString());
				} catch (final Exception e) {
					LOGGER.error(ERROR_SALVATAGGIO_CONTATTO_MSG, e);
					throw e;
				} finally {
					popSubject(fceh);
				}
			} // END VI
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY), status.getDestinatariString());
		}
		if (status.getAssegnazioniCompetenza().length > 0) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ASSEGNATARI_COMPETENZA_METAKEY), status.getAssegnazioniCompetenza());
		}
		if (status.getAssegnazioniConoscenza().length > 0) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ASSEGNATARI_CONOSCENZA_METAKEY), status.getAssegnazioniConoscenza());
		}
		if (status.getAssegnazioniContributo().length > 0) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ASSEGNATARI_CONTRIBUTO_METAKEY), status.getAssegnazioniContributo());
		}

		// Metadato Numero Protocollo Risposta
		if (docInput.getNumeroProtocolloRisposta() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_RISPOSTA_STRING_METAKEY), docInput.getNumeroProtocolloRisposta());
		}

		// Metadato Anno Protocollo Risposta
		if (docInput.getAnnoProtocolloRisposta() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_RISPOSTA_METAKEY), docInput.getAnnoProtocolloRisposta());
		}

		// Metadato Num Doc Dest Int Uscita (documento con categoria "Assegnazione
		// Interna")
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NUM_DOC_DEST_INT_USCITA_METAKEY), docInput.getNumDocDestIntUscita());

		// NPS -> START
		// Oggetto Protocollo NPS
		// Si applica la substring() dato che il metadato sul CE è limitato a max 64
		// caratteri
		if (status.isAggiornaOggettoProtocolloNPS()) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_PROTOCOLLO_NPS_METAKEY), StringUtils.substring(docInput.getOggetto(), 0, 64));
		}
		// Descrizione Tipologia Documento NPS
		if (status.isAggiornaTipologiaDocumentoNPS()) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DESCRIZIONE_TIPOLOGIA_DOCUMENTO_NPS_METAKEY), docInput.getTipologiaDocumento().getDescrizione());
		}
		// NPS -> END

		// Metadati Dinamici
		if (!CollectionUtils.isEmpty(docInput.getMetadatiDinamici())) {
			metadatiDocumento.putAll(docInput.getMetadatiDinamici());
		}

		// Codice flusso per i documenti generati da flussi
		if (StringUtils.isNotBlank(docInput.getCodiceFlusso())) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY), docInput.getCodiceFlusso());
		}

		// decreto liquidazione per flusso SICOGE
		if (StringUtils.isNotBlank(docInput.getDecretoLiquidazioneSicoge())) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DECRETO_LIQUIDAZIONE_METAKEY), docInput.getDecretoLiquidazioneSicoge());
		}

		// Sotto categoria documento uscita
		if (docInput.getSottoCategoriaUscita() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.SOTTOCATEGORIA_DOCUMENTO_USCITA), docInput.getSottoCategoriaUscita().getId());
		}

		// Integrazione Dati
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.INTEGRAZIONE_DATI_METAKEY), docInput.isIntegrazioneDati());

		// Protocollo riferimento
		if (docInput.getProtocolloRiferimento() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.PROTOCOLLO_RIFERIMENTO_METAKEY), docInput.getProtocolloRiferimento());
		}

		// Identificatore processo
		if (StringUtils.isNotBlank(docInput.getIdentificatoreProcesso())) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.IDENTIFICATORE_PROCESSO_METAKEY), docInput.getIdentificatoreProcesso());
			LOGGER.info("caricaMetadatiDocumento -> Identificatore Processo: " + docInput.getIdentificatoreProcesso());
		}

		if (docInput.getDataScarico() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DATA_SCARICO_METAKEY), docInput.getDataScarico());
			LOGGER.info("caricaMetadatiDocumento -> Data Scarico/Ripristino: " + docInput.getDataScarico());
		}

		return metadatiDocumento;
	}

	private void gestisciMetadatoMittente(final SalvaDocumentoRedDTO docInput, final UtenteDTO utente, final Map<String, Object> metadatiDocumento) {
		final Contatto mittente = docInput.getMittenteContatto();
		if (mittente != null) {
			final IFilenetCEHelper fceh = getIFilenetHelper(utente.getIdAoo().intValue());
			try {
				String idContatto = null;
				if (mittente.getContattoID() != null) {
					idContatto = String.valueOf(mittente.getContattoID());
					final List<Contatto> contattiList = new ArrayList<>();
					contattiList.add(mittente);
					// Elimino il vecchio contatto da filenet se c'è
					if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(docInput.getObjectIdContattoMittOnTheFly())) {
						cancellaVecchioContDaFilenet(docInput.getObjectIdContattoMittOnTheFly(), fceh);
					}
					final String objectIdContattoOnTheFly = docInput.getObjectIdContattoMittOnTheFly() != null ? docInput.getObjectIdContattoMittOnTheFly() : null;
					final CustomObject listContatti = salvaContatto(objectIdContattoOnTheFly, fceh, contattiList);
					metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.OBJECTID_CONTATTO_MITT_METADATO), listContatti.get_Id().toString());
				} else {
					if (mittente.getOnTheFly().equals(CONTATTO_ON_THE_FLY)) {
						// Elimino il vecchio contatto da filenet se c'è
						cancellaVecchioContDaFilenet(docInput.getObjectIdContattoMittOnTheFly(), fceh);
						idContatto = gestisciSalvataggioMetadatoMittenteNew(docInput.getObjectIdContattoMittOnTheFly(), metadatiDocumento, docInput.getMittenteContatto(),
								utente.getIdAoo(), fceh);
					} else {
						idContatto = String.valueOf(mittente.getIdUtente());
					}
				}
				metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.MITTENTE_METAKEY),
						getMittenteMetaKey(idContatto, docInput.getMittenteContatto().getAliasContatto()));
			} catch (final Exception e) {
				LOGGER.error(ERROR_SALVATAGGIO_CONTATTO_MSG, e);
				throw e;
			} finally {
				popSubject(fceh);
			}
		}
	}

	private static Object getMittenteMetaKey(final String idContatto, final String aliasContatto) {
		String mittenteMetaKey = idContatto + "," + aliasContatto;

		if (mittenteMetaKey.length() > MAX_LENGTH_MITTENTE_METAKEY) {
			mittenteMetaKey = mittenteMetaKey.substring(0, MAX_LENGTH_MITTENTE_METAKEY);
		}

		return mittenteMetaKey;
	}

	private void gestisciProtocollazioneEmail(final String inputMailGuid, final int idDocumento, final int numeroProtocollo, final String annoProtocollo,
			final NotaDTO contentNoteDaMailProtocolla, final AssegnatarioInteropDTO preassegnatario, final boolean inviaNotifica, final boolean isProtocollazioneAutomatica,
			final boolean protocollaEMantieni, final UtenteDTO utente, final IFilenetCEHelper fceh, final Connection con) {
		LOGGER.info("gestisciProtocollazioneEmail -> START. Documento: " + idDocumento);

		// Creazione dell'annotation sul documento per mantenere il riferimento
		// all'e-mail in ingresso
		fceh.creaAnnotation(String.valueOf(idDocumento), inputMailGuid, FilenetAnnotationEnum.TEXT_MAIL, utente.getIdAoo());
		LOGGER.info("E-mail in ingresso (protocollazione manuale) -> è stata creata l'annotation " + FilenetAnnotationEnum.TEXT_MAIL + " su FileNet per il documento: "
				+ idDocumento);

		// Se il MULTI_NOTE è abilitato, si procede alla registrazione della nota e
		// delle relative informazioni
		if (PermessiUtils.haAccessoAllaFunzionalita(AccessoFunzionalitaEnum.MULTI_NOTE, utente) && contentNoteDaMailProtocolla != null) {
			notaSRV.registraNotaFromCodaMailDTO(utente, idDocumento, contentNoteDaMailProtocolla, preassegnatario, con);
		}

		// Si aggiornano i metadati dell'e-mail e si procede con l'eventuale invio
		// dell'e-mail di notifica
		gestisciMetadatiEmailProtocollata(inputMailGuid, idDocumento, numeroProtocollo, annoProtocollo, inviaNotifica, isProtocollazioneAutomatica, protocollaEMantieni,
				utente, fceh, con);
		LOGGER.info("gestisciProtocollazioneEmail -> END. Documento: " + idDocumento);
	}

	private void gestisciStatoEmailProtocollata(final String inputMailGuid, final int idDocumento, final int numeroProtocollo, final String annoProtocollo,
			final boolean inviaNotifica, final UtenteDTO utente, final Connection con) {
		LOGGER.info("gestisciStatoEmailProtocollata -> START. Documento: " + idDocumento);
		final IFilenetCEHelper fcehAdmin = getFCEHAdmin(utente.getFcDTO(), utente.getIdAoo());

		final Document emailFilenet = fcehAdmin.getDocument(fcehAdmin.idFromGuid(inputMailGuid));

		// Si calcola il nuovo stato e si procede all'eventuale invio dell'e-mail di
		// notifica
		final StatoMailEnum statoEmail = calcolaNuovoStatoEmailEdInviaNotifica(inputMailGuid, idDocumento, numeroProtocollo, annoProtocollo, emailFilenet, inviaNotifica,
				utente, fcehAdmin, con);

		// Si aggiorna lo stato dell'e-mail
		aggiornaStatoEmail(inputMailGuid, emailFilenet, statoEmail, fcehAdmin);
		LOGGER.info("gestisciStatoEmailProtocollata -> END. Documento: " + idDocumento);
	}

	/**
	 * Calcola il nuovo stato dell'e-mail in ingresso protocollata ed esegue
	 * l'eventuale invio dell'e-mail di notifica.
	 * 
	 * @param status
	 * @param idDocumento
	 * @param emailFilenet
	 * @param utente
	 * @param fcehAdmin
	 * @param con
	 * @return
	 */
	private StatoMailEnum calcolaNuovoStatoEmailEdInviaNotifica(final String inputMailGuid, final int idDocumento, final int numeroProtocollo, final String annoProtocollo,
			final Document emailFilenet, final boolean inviaNotifica, final UtenteDTO utente, final IFilenetCEHelper fcehAdmin, final Connection con) {
		StatoMailEnum statoEmail = StatoMailEnum.IMPORTATA;

		if (inviaNotifica) {
			statoEmail = StatoMailEnum.NOTIFICATA;
			// Invio dell'e-mail di notifica
			inviaEmailNotifica(inputMailGuid, idDocumento, numeroProtocollo, annoProtocollo, emailFilenet, utente, fcehAdmin, con);
		}

		return statoEmail;
	}

	private void inviaEmailNotifica(final String inputMailGuid, final int idDocumento, final int numeroProtocollo, final String annoProtocollo, final Document mailFilenet,
			final UtenteDTO utente, final IFilenetCEHelper fcehAdmin, final Connection con) {
		LOGGER.info("inviaEmailNotifica -> START. Documento: " + idDocumento);

		final String documentTitle = (String) TrasformerCE.getMetadato(mailFilenet, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
		final String messageId = (String) TrasformerCE.getMetadato(mailFilenet, PropertiesNameEnum.MESSAGE_ID);
		final String casellaPostale = (String) TrasformerCE.getMetadato(mailFilenet, PropertiesNameEnum.CASELLA_POSTALE_METAKEY);
		final String oggetto = (String) TrasformerCE.getMetadato(mailFilenet, PropertiesNameEnum.OGGETTO_EMAIL_METAKEY);
		final String mittente = (String) TrasformerCE.getMetadato(mailFilenet, PropertiesNameEnum.FROM_MAIL_METAKEY);
		final String destinatariCc = (String) TrasformerCE.getMetadato(mailFilenet, PropertiesNameEnum.DESTINATARI_CC_EMAIL_METAKEY);
		final Date data = (Date) TrasformerCE.getMetadato(mailFilenet, PropertiesNameEnum.DATA_RICEZIONE_MAIL_METAKEY);

		final CasellaPostaDTO casellaPostaleRed = casellePostaliSRV.getCasellaPostale(fcehAdmin, casellaPostale);
		if (casellaPostaleRed != null && casellaPostaleRed.getIndirizzoEmail().equals(casellaPostale)) {
			final StringBuilder testoMail = new StringBuilder().append("Buongiorno,\nla mail con oggetto '").append(oggetto).append("' Ricevuta in data '")
					.append(DateUtils.dateToString(data, null)).append("' ore '").append(DateUtils.dateToString(data, "HH.mm"))
					.append("' è stata protocollata con numero di protocollo: '").append("MEF - ").append(utente.getCodiceAoo()).append(" - ").append(utente.getAooDesc())
					.append(" - ")
					.append((TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(utente.getIdTipoProtocollo())) ? Constants.Protocollo.PROTOCOLLO_EMERGENZA_ACRONIMO + " "
							: Constants.EMPTY_STRING)
					.append(numeroProtocollo).append("/").append(annoProtocollo).append("' e inoltrata agli uffici competenti.\nCordiali saluti\n\n");

			// N.B. Il destinatario della mail di notifica è il mittente della mail in
			// input,
			// mentre il mittente della notifica è la casella postale che ha ricevuto la
			// mail
			final String destinatarioString = "0," + mittente + ",2,E,TO";

			codaMailSRV.creaMailEInserisciInCoda(documentTitle, inputMailGuid, casellaPostale, Arrays.asList(destinatarioString), mittente, destinatariCc, oggetto,
					testoMail.toString(), AzioneMail.PROTOCOLLA, null, TipologiaInvio.RISPONDI, casellaPostaleRed.getTipologia(), messageId, true, false, null, null, utente,
					con, null, false);

			LOGGER.info("E-mail di notifica inserita per il documento: " + idDocumento);
		} else {
			LOGGER.error("L'invio dell'e-mail di notifica non può essere effettuato in quanto non è stata trovata nessuna casella postale con nome: " + casellaPostale);
		}
		LOGGER.info("inviaEmailNotifica -> END. Documento: " + idDocumento);
	}

	private void aggiornaStatoEmail(final String inputMailGuid, final StatoMailEnum statoEmail, final IFilenetCEHelper fcehAdmin) {
		final Document mailFilenet = fcehAdmin.getDocument(fcehAdmin.idFromGuid(inputMailGuid));

		aggiornaStatoEmail(inputMailGuid, mailFilenet, statoEmail, fcehAdmin);
	}

	private void aggiornaStatoEmail(final String inputMailGuid, final Document mailFilenet, final StatoMailEnum statoEmail, final IFilenetCEHelper fceh) {
		final Map<String, Object> metadatiDaAggiornare = new HashMap<>();
		metadatiDaAggiornare.put(pp.getParameterByKey(PropertiesNameEnum.STATO_MAIL_METAKEY), statoEmail.getStatus());

		fceh.updateMetadati(mailFilenet, metadatiDaAggiornare);
		LOGGER.info("aggiornaStatoEmail -> END. GUID e-mail: " + inputMailGuid + ". Nuovo stato: " + statoEmail.name());
	}

	@SuppressWarnings("unchecked")
	private void gestisciMetadatiEmailProtocollata(final String inputMailGuid, final int idDocumento, final int numeroProtocollo, final String annoProtocollo,
			final boolean inviaNotifica, final boolean isProtocollazioneAutomatica, final boolean protocollaEMantieni, final UtenteDTO utente, final IFilenetCEHelper fceh,
			final Connection con) {
		final IFilenetCEHelper fcehAdmin = getFCEHAdmin(utente.getFcDTO(), utente.getIdAoo());
		final Document emailFilenet = fcehAdmin.getDocument(fcehAdmin.idFromGuid(inputMailGuid));

		// Calcolo del nuovo stato ed eventuale invio dell'e-mail di notifica
		StatoMailEnum statoEmail = null;
		if (!isProtocollazioneAutomatica) {
			statoEmail = calcolaNuovoStatoEmailEdInviaNotifica(inputMailGuid, idDocumento, numeroProtocollo, annoProtocollo, emailFilenet, inviaNotifica, utente, fcehAdmin,
					con);
		}

		// Aggiornamento dei protocolli generati dall'e-mail
		List<String> protocolliGenerati = (List<String>) TrasformerCE.getMetadato(emailFilenet, pp.getParameterByKey(PropertiesNameEnum.PROTOCOLLI_GENERATI_MAIL_METAKEY));
		if (CollectionUtils.isEmpty(protocolliGenerati)) {
			protocolliGenerati = new ArrayList<>();
		}

		// Struttura metadato: "NUMERO_PROTOCOLLO/ANNO_PROTOCOLLO"
		String protocollo = null;
		if (numeroProtocollo > 0 && StringUtils.isNotBlank(annoProtocollo)) {
			protocollo = numeroProtocollo + "/" + annoProtocollo;
			protocolliGenerati.add(protocollo);
		}

		final Map<String, Object> metadatiDaAggiornare = new HashMap<>();
		// Si aggiorna lo stato
		if (utente.getProtocollaEMantieni() && protocollaEMantieni) {
			metadatiDaAggiornare.put(pp.getParameterByKey(PropertiesNameEnum.STATO_MAIL_METAKEY), StatoMailEnum.INARRIVO.getStatus());
			metadatiDaAggiornare.put(pp.getParameterByKey(PropertiesNameEnum.PROTOCOLLI_GENERATI_MAIL_METAKEY), protocolliGenerati);
			metadatiDaAggiornare.put(pp.getParameterByKey(PropertiesNameEnum.PROTOCOLLA_E_MANTIENI_METAKEY), true);	
		} else {
			if (statoEmail != null) {
				metadatiDaAggiornare.put(pp.getParameterByKey(PropertiesNameEnum.STATO_MAIL_METAKEY), statoEmail.getStatus());
			}
			// Si aggiunge il documento appena creato ai protocolli generati a partire da
			// questa e-mail
			metadatiDaAggiornare.put(pp.getParameterByKey(PropertiesNameEnum.PROTOCOLLI_GENERATI_MAIL_METAKEY), protocolliGenerati);
			metadatiDaAggiornare.put(pp.getParameterByKey(PropertiesNameEnum.PROTOCOLLA_E_MANTIENI_METAKEY), false);	
		}

		// Si procede con l'aggiornamento dei metadati
		fceh.updateMetadati(emailFilenet, metadatiDaAggiornare);
		LOGGER.info("aggiornaMetadatiEmailProtocollata -> END. GUID e-mail: " + inputMailGuid);
	}

	private String avviaNuovoWorkflow(final SalvaDocumentoRedStatusDTO status, String nomeWorkflow, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente,
			final Map<String, Object> metadatiWorkflow, final FilenetPEHelper fpeh, final Connection con) {
		final int idDocumento = NumberUtils.toInt(docInput.getDocumentTitle());
		LOGGER.info("avviaNuovoWorkflow -> START. Documento: " + idDocumento);
		String wobNumber = null;

		final Aoo aoo = aooDAO.getAoo(utente.getIdAoo(), con);

		// Se non viene passato in input il nome del workflow da avviare, lo si ottiene
		// dal documento
		if (StringUtils.isBlank(nomeWorkflow)) {
			nomeWorkflow = getNomeWorkflow(status, docInput, con);
		}

		// ### AVVIO DEL NUOVO WORKFLOW FILENET
		if (StringUtils.isNotBlank(nomeWorkflow)) {
			wobNumber = fpeh.avviaWF(nomeWorkflow, metadatiWorkflow, idDocumento, aoo.getAooFilenet().getIdClientAoo(), false);
			if (wobNumber != null) {
				LOGGER.info("avviaNuovoWorkflow -> È stato avviato il workflow: " + nomeWorkflow + PER_IL_DOCUMENTO	+ idDocumento + ". WOB number: " + wobNumber);
				status.getWorkflowList().add(wobNumber);
			}
		} else {
			throw new RedException(
					"avviaNuovoWorkflow -> Documento: " + idDocumento + ". Il workflow non è stato avviato in quanto non si è riusciti a recuperare il suo nome.");
		}

		LOGGER.info("avviaNuovoWorkflow -> END. Documento: " + idDocumento);
		return wobNumber;
	}

	private String getNomeWorkflow(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final Connection con) {
		LOGGER.info(
				"getNomeWorkflow -> START. Selezione del workflow da startare in corso per il tipo procedimento: " + docInput.getTipoProcedimento().getTipoProcedimentoId());
		String workflowName = null;

		if (status.isDocEntrata() || status.isIterManuale() || status.isDocAssegnazioneInterna()) {
			workflowName = tipoProcedimentoDAO.getWorkflowNameByIdTipoProcedimento(docInput.getTipoProcedimento().getTipoProcedimentoId(), con);

			if (StringUtils.isNotBlank(workflowName)) {
				LOGGER.info("Selezionato il workflow: " + workflowName + " per il tipo procedimento di documento in entrata/iter manuale con ID: "
						+ docInput.getTipoProcedimento().getTipoProcedimentoId());
			} else {
				LOGGER.error("Si è verificato un errore durante il recupero del nome del workflow a partire dal tipo procedimento dal DB.");
				throw new RedException("Errore durante il recupero del nome del workflow a partire dal tipo procedimento");
			}
		} else if (status.getIterApprovativo() != null) {
			workflowName = status.getIterApprovativo().getWorkflowName();
			LOGGER.info("Selezionato il workflow: " + status.getIterApprovativo().getWorkflowName() + " per il tipo procedimento con iter automatico: "
					+ status.getIterApprovativo().getIdIterApprovativo());
		}

		if (workflowName == null) {
			final String error = "Il tipo procedimento: " + docInput.getTipoProcedimento().getDescrizione() + " impostato per il documento: " + docInput.getDocumentTitle()
					+ " non è presente nel DB.";
			LOGGER.error(error);
			throw new RedException(error);
		}

		LOGGER.info("getNomeWorkflow -> END.");
		return workflowName;
	}

	/**
	 * Metodo per la conversione del detail in input nel DTO utilizzato nel service.
	 * ###### N.B. In questo punto del codice lo status non è ancora inizializzato,
	 * quindi deve essere utilizzato solo in scrittura (set di alcuni suoi
	 * attributi).
	 * 
	 * @param status
	 * @param detailDocInput
	 * @param modalita
	 * @param utente
	 * @param fceh
	 * @param con
	 * @return
	 */
	private SalvaDocumentoRedDTO trasformaInSalvaDocumento(final SalvaDocumentoRedStatusDTO status, final DetailDocumentRedDTO detailDocInput, final String modalita,
			final UtenteDTO utente, final IFilenetCEHelper fceh, final Connection con) {
		LOGGER.info("trasformaInSalvaDocumento -> START. Utente: " + utente.getUsername());
		final SalvaDocumentoRedDTO salvaDocInput = new SalvaDocumentoRedDTO();

		final Integer inCategoriaDocumento = detailDocInput.getIdCategoriaDocumento();

		salvaDocInput.setDocumentTitle(detailDocInput.getDocumentTitle());
		salvaDocInput.setGuid(detailDocInput.getGuid());
		salvaDocInput.setWobNumber(detailDocInput.getWobNumberSelected());
		salvaDocInput.setFolder(detailDocInput.getFolder());
		salvaDocInput.setOggetto(detailDocInput.getOggetto());
		salvaDocInput.setFormatoDocumentoEnum(detailDocInput.getFormatoDocumentoEnum());
		salvaDocInput.setTipoAssegnazioneEnum(detailDocInput.getTipoAssegnazioneEnum());
		salvaDocInput.setContent(detailDocInput.getContent());
		salvaDocInput.setContentType(detailDocInput.getMimeType());
		salvaDocInput.setIdCategoriaDocumento(inCategoriaDocumento);
		if (detailDocInput.getIdAOO() != null) {
			salvaDocInput.setIdAOO(Long.valueOf(detailDocInput.getIdAOO()));
		} else {
			salvaDocInput.setIdAOO(utente.getIdAoo());
		}

		// Protocollo -> START
		salvaDocInput.setIdProtocollo(detailDocInput.getIdProtocollo());
		salvaDocInput.setNumeroProtocollo(detailDocInput.getNumeroProtocollo());
		salvaDocInput.setDataProtocollo(detailDocInput.getDataProtocollo());
		salvaDocInput.setAnnoProtocollo(detailDocInput.getAnnoProtocollo());
		salvaDocInput.setNumeroProtocolloEmergenza(detailDocInput.getNumeroProtocolloEmergenza());
		salvaDocInput.setDataProtocolloEmergenza(detailDocInput.getDataProtocolloEmergenza());
		salvaDocInput.setAnnoProtocolloEmergenza(detailDocInput.getAnnoProtocolloEmergenza());
		// Protocollo -> END

		// N.B. USATO SOLO IN CASO DI PROTOCOLLAZIONE MAIL
		salvaDocInput.setContentNoteDaMailProtocollaDTO(detailDocInput.getContentNoteDaMailProtocollaDTO());
		salvaDocInput.setPreassegnatarioDaMailProtocollaInterop(detailDocInput.getPreassegnatarioInteropDTO());
		salvaDocInput.setProtocollazioneMailAccount(detailDocInput.getProtocollazioneMailAccount());
		salvaDocInput.setDataScarico(detailDocInput.getDataScarico());

		// Fascicolo Procedimentale -> START
		final String idFascicoloProcedimentale = detailDocInput.getIdFascicoloProcedimentale();
		if (StringUtils.isNotBlank(idFascicoloProcedimentale)) {
			try {
				final FascicoloDTO fascicoloProcedimentale = TrasformCE.transform(fceh.getFascicolo(idFascicoloProcedimentale, utente.getIdAoo().intValue()),
						TrasformerCEEnum.FROM_DOCUMENTO_TO_FASCICOLO);
				salvaDocInput.setFascicoloProcedimentale(fascicoloProcedimentale);
			} catch (final Exception e) {
				LOGGER.error("trasformaInSalvaDocumento -> Si è verificato un errore durante il recupero del fascicolo procedimentale.", e);
				throw new RedException("trasformaInSalvaDocumento -> Si è verificato un errore durante il recupero del fascicolo procedimentale.", e);
			}
		}
		// Nome Fascicolo Procedimentale
		salvaDocInput.setDescrizioneFascicoloProcedimentale(detailDocInput.getDescrizioneFascicoloProcedimentale());

		// Indice Classificazione Fascicolo Procedimentale
		salvaDocInput.setIndiceClassificazioneFascicoloProcedimentale(detailDocInput.getIndiceClassificazioneFascicoloProcedimentale());

		salvaDocInput.setDescrizioneIndiceClassificazioneFascicoloProcedimentale(detailDocInput.getDescrizioneTitolarioFascicoloProcedimentale());
		// Fascicolo Procedimentale -> END

		// Assegnazioni
		final List<AssegnazioneDTO> inputAssegnazioni = detailDocInput.getAssegnazioni();
		if (!CollectionUtils.isEmpty(inputAssegnazioni)) {
			salvaDocInput.setAssegnazioni(inputAssegnazioni);
		}

		// Fascicoli
		final List<FascicoloDTO> inputFascicoli = detailDocInput.getFascicoli();
		if (!CollectionUtils.isEmpty(inputFascicoli)) {
			salvaDocInput.setFascicoli(inputFascicoli);
		}

		// Allegati
		final List<AllegatoDTO> inputAllegati = detailDocInput.getAllegati();
		if (!CollectionUtils.isEmpty(inputAllegati)) {
			salvaDocInput.setAllegati(inputAllegati);
		}

		// Allegati non sbustati
		final List<AllegatoDTO> inputAllegatiNonSbustati = detailDocInput.getAllegatiNonSbustati();
		if (!CollectionUtils.isEmpty(inputAllegatiNonSbustati)) {
			salvaDocInput.setAllegatiNonSbustati(inputAllegatiNonSbustati);
		}

		// Allacci
		final List<RispostaAllaccioDTO> inputAllacci = detailDocInput.getAllacci();
		if (!CollectionUtils.isEmpty(inputAllacci)) {
			salvaDocInput.setAllacci(inputAllacci);
		}

		// RIFERIMENTO STORICO
		final List<RiferimentoStoricoNsdDTO> inputRiferimentoStorico = detailDocInput.getAllaccioRifStorico();
		if (!CollectionUtils.isEmpty(inputRiferimentoStorico)) {
			salvaDocInput.setAllacciRifStorico(inputRiferimentoStorico);
		}

		// Contributi
		final List<ContributoDTO> inputContributi = detailDocInput.getContributi();
		if (!CollectionUtils.isEmpty(inputContributi)) {
			salvaDocInput.setContributi(inputContributi);
		}

		// Numero Mittente Contributo e Anno Mittente Contributo
		if (CategoriaDocumentoEnum.CONTRIBUTO_ESTERNO.getIds()[0].equals(detailDocInput.getIdCategoriaDocumento())) {
			salvaDocInput.setNumeroMittenteContributo(detailDocInput.getNumeroMittenteContributo());
			salvaDocInput.setAnnoMittenteContributo(detailDocInput.getAnnoMittenteContributo());
		} else {
			salvaDocInput.setNumeroMittenteContributo(-1);
			salvaDocInput.setAnnoMittenteContributo(-1);
		}

		if (CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0].equals(inCategoriaDocumento)
				|| CategoriaDocumentoEnum.ASSEGNAZIONE_INTERNA.getIds()[0].equals(inCategoriaDocumento) || detailDocInput.getMittenteContatto() != null) {
			// Mittente
			salvaDocInput.setMittenteContatto(detailDocInput.getMittenteContatto());
		} else {
			final List<DestinatarioRedDTO> inputDestinatari = detailDocInput.getDestinatari();
			final List<ApprovazioneDTO> inputApprovazioni = detailDocInput.getApprovazioni();

			// Approvazioni
			if (!CollectionUtils.isEmpty(inputApprovazioni)) {
				salvaDocInput.setApprovazioni(inputApprovazioni);
			}

			// Destinatari
			if (!CollectionUtils.isEmpty(inputDestinatari)) {
				// Se c'è almeno un destinatario esterno con mezzo di spedizione "Elettronico",
				// si imposta il flag per la spedizione della mail
				if (DestinatariRedUtils.checkOneDestinatarioElettronico(inputDestinatari)) {
					status.setSpedisciMailDestElettronici(true);
				}

				salvaDocInput.setDestinatari(detailDocInput.getDestinatari());
			}

			// Iter Approvativo
			if (detailDocInput.getIdIterApprovativo() != null) {
				final IterApprovativoDTO iterApprovativo = iterApprovativoDAO.getIterApprovativoById(detailDocInput.getIdIterApprovativo(), con);
				if (iterApprovativo != null) {
					salvaDocInput.setIterApprovativo(iterApprovativo);
				}
			}

			// Flag Iter Approvativo Manuale
			if (detailDocInput.getIdIterApprovativo() != null) {
				salvaDocInput.setIsIterManuale(IterApprovativoDTO.ITER_FIRMA_MANUALE.intValue() == detailDocInput.getIdIterApprovativo());
			}

			// Momento Protocollazione
			if (detailDocInput.getMomentoProtocollazioneEnum() != null) {
				salvaDocInput.setMomentoProtocollazioneEnum(detailDocInput.getMomentoProtocollazioneEnum());
			}

			// Mail Spedizione
			if (status.isSpedisciMailDestElettronici()) {
				salvaDocInput.setMailSpedizione(detailDocInput.getMailSpedizione());
			}
		}

		// Data Scadenza
		salvaDocInput.setDataScadenza(detailDocInput.getDataScadenza());

		// Check Firma Digitale RGS
		salvaDocInput.setCheckFirmaDigitaleRGS(detailDocInput.getCheckFirmaDigitaleRGS());

		// Tipologia Documento e Tipo Procedimento
		if (!FormatoDocumentoEnum.PRECENSITO.equals(detailDocInput.getFormatoDocumentoEnum())
				|| (FormatoDocumentoEnum.PRECENSITO.equals(detailDocInput.getFormatoDocumentoEnum()) && Modalita.MODALITA_UPD.equals(modalita))) {
			// Tipologia Documento
			if (detailDocInput.getIdTipologiaDocumento() != null) {
				final TipologiaDocumentoDTO tipologiaDoc = tipologiaDocumentoDAO.getById(detailDocInput.getIdTipologiaDocumento(), con);
				salvaDocInput.setTipologiaDocumento(tipologiaDoc);
			}

			// Tipo Procedimento
			if (detailDocInput.getIdTipologiaProcedimento() != null) {
				final TipoProcedimento tipoProcedimento = tipoProcedimentoDAO.getTPbyId(con, detailDocInput.getIdTipologiaProcedimento());
				salvaDocInput.setTipoProcedimento(new TipoProcedimentoDTO(tipoProcedimento));
			}
		}

		if (CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0].equals(inCategoriaDocumento)) {
			// Mezzo Ricezione
			if (detailDocInput.getMezzoRicezioneDTO() != null) {
				salvaDocInput.setMezzoRicezione(detailDocInput.getMezzoRicezioneDTO());
			}

			// Formato Documento
			salvaDocInput.setFormatoDocumentoEnum(detailDocInput.getFormatoDocumentoEnum());

			// Da Protocollare
			if (!Modalita.MODALITA_UPD.equals(modalita)) {
				salvaDocInput.setDaProtocollare(!Boolean.TRUE.equals(detailDocInput.getDaNonProtocollare()));
			}

			// Numero Protocollo Mittente
			salvaDocInput.setProtocolloMittente(detailDocInput.getProtocolloMittente());
			// Anno Protocollo Mittente
			salvaDocInput.setAnnoProtocolloMittente(detailDocInput.getAnnoProtocolloMittente());
			// Data Protocollo Mittente
			salvaDocInput.setDataProtocolloMittente(detailDocInput.getDataProtocolloMittente());

			// Numero Protocollo Risposta
			salvaDocInput.setNumeroProtocolloRisposta(detailDocInput.getNumeroProtocolloRisposta());
			// Anno Protocollo Risposta
			salvaDocInput.setAnnoProtocolloRisposta(detailDocInput.getAnnoProtocolloRisposta());
			// Data Protocollo Risposta
			salvaDocInput.setDataProtocolloRisposta(detailDocInput.getDataProtocolloRisposta());
			// Id Protocollo Risposta
			salvaDocInput.setIdProtocolloRisposta(detailDocInput.getIdProtocolloRisposta());

			// Barcode
			salvaDocInput.setBarcode(detailDocInput.getBarcode());

			// Numero Raccomandata
			salvaDocInput.setNumeroRaccomandata(detailDocInput.getNumeroRaccomandata());
		}

		// Firma con Copia Conforme
		// N.B. SEMBRA CHE NON SIA MAI USATO!
		if (salvaDocInput.getTipologiaDocumento() != null) {
			final Boolean checkFirmaConCopiaConforme = detailDocInput.getCheckFirmaConCopiaConforme();
			final Boolean firmaConCopiaConformeAttiva = salvaDocInput.getTipologiaDocumento().getFlagFirmaCopiaConforme();

			if (Boolean.TRUE.equals(checkFirmaConCopiaConforme) && Boolean.TRUE.equals(firmaConCopiaConformeAttiva)) {
				salvaDocInput.setFirmaConCopiaConforme(Boolean.TRUE);
			} else {
				salvaDocInput.setFirmaConCopiaConforme(Boolean.FALSE);
			}
		} else {
			salvaDocInput.setFirmaConCopiaConforme(Boolean.FALSE);
		}

		// Urgente
		salvaDocInput.setIsUrgente(detailDocInput.getUrgente());

		// Riservato
		salvaDocInput.setIsRiservato(BooleanFlagEnum.SI.getIntValue().equals(detailDocInput.getRiservato()) ? Boolean.TRUE : Boolean.FALSE);

		// Note
		salvaDocInput.setNote(detailDocInput.getNote());

		// Motivo Assegnazione
		salvaDocInput.setMotivoAssegnazione(detailDocInput.getMotivoAssegnazione());

		// Nome File
		salvaDocInput.setNomeFile(detailDocInput.getNomeFile());

		// Registro Riservato
		salvaDocInput.setRegistroRiservato(detailDocInput.getRegistroRiservato());

		// PRELEX (Numero RDP e Legislatura)
		if (salvaDocInput.getTipoProcedimento() != null && Varie.TIPO_PROCEDIMENTO_PRELEX.equalsIgnoreCase(salvaDocInput.getTipoProcedimento().getDescrizione())) {
			salvaDocInput.setNumeroRDP(detailDocInput.getNumeroRDP());
			salvaDocInput.setNumeroLegislatura(detailDocInput.getNumeroLegislatura());
		}

		// Registro Riservato
		final Aoo aoo = aooDAO.getAoo(utente.getIdAoo(), con);
		if (aoo != null && (utente.getRegistroRiservato() != null && utente.getRegistroRiservato())
				&& (aoo.getParametriAOO() & PermessiAOOEnum.GESTIONE_REGISTRI_RISERVATI.getId()) > 0) {
			salvaDocInput.setRegistroRiservato(detailDocInput.getRegistroRiservato());
		}

		// Flag Corte dei Conti
		salvaDocInput.setFlagCorteConti(detailDocInput.isFlagCorteConti());

		// Traccia Procedimento
		salvaDocInput.setTracciaProcedimento(detailDocInput.getTracciaProcedimento());

		// Template Doc Uscita
		salvaDocInput.setTemplateDocUscitaMap(detailDocInput.getTemplateDocUscitaMap());

		// ID Ufficio Coordinatore e ID Utente Coordinatore
		caricaCoordinatore(status, salvaDocInput, detailDocInput, utente, con);

		// ID Raccolta FAD
		salvaDocInput.setIdRaccoltaFAD(detailDocInput.getIdraccoltaFAD());

		// Responsabile Copia Conforme
		if (TipoAssegnazioneEnum.COPIA_CONFORME.equals(detailDocInput.getTipoAssegnazioneEnum())) {
			salvaDocInput.setResponsabileCopiaConforme(detailDocInput.getResponsabileCopiaConforme());
		}

		// Num Doc Dest Int Uscita
		salvaDocInput.setNumDocDestIntUscita(detailDocInput.getNumDocDestIntUscita());

		// Metadati Dinamici
		if (!CollectionUtils.isEmpty(detailDocInput.getMetadatiDinamici())) {
			final Map<String, Object> metadatiDinamiciRedFn = new HashMap<>();
			for (final MetadatoDinamicoDTO inputMetadatoDinamico : detailDocInput.getMetadatiDinamici()) {
				final Object valore = inputMetadatoDinamico.getValore();
				if (valore != null) {
					if (Cardinality.LIST_AS_INT == inputMetadatoDinamico.getCardinality()) {
						final List<Object> valoreMetadato = new ArrayList<>();
						final String valoreStringaGrezzo = (String) valore;
						// Si rimuovono le parentesi graffe che racchiudono i valori e si splittano i
						// singoli valori separati da ","
						if (StringUtils.isNotBlank(valoreStringaGrezzo)) {
							final String[] valoriMultipli = valoreStringaGrezzo.substring(1, valoreStringaGrezzo.length() - 1).split(",");
							for (final String singoloValore : valoriMultipli) {
								if (TypeID.LONG_AS_INT == inputMetadatoDinamico.getDataTypeValue()) {
									final Long singoloValoreNumerico = Long.parseLong(singoloValore);
									valoreMetadato.add(singoloValoreNumerico);
								} else {
									valoreMetadato.add(singoloValore);
								}
							}
							metadatiDinamiciRedFn.put(inputMetadatoDinamico.getChiave(), valoreMetadato);
						}
					} else if (valore instanceof String) {
						final String valoreStringaGrezzo = (String) valore;
						if (StringUtils.isNotBlank(valoreStringaGrezzo)) {
							if (TypeID.LONG_AS_INT == inputMetadatoDinamico.getDataTypeValue()) {
								metadatiDinamiciRedFn.put(inputMetadatoDinamico.getChiave(), Long.parseLong(valoreStringaGrezzo));
							} else {
								metadatiDinamiciRedFn.put(inputMetadatoDinamico.getChiave(), valore);
							}
						}
					} else {
						metadatiDinamiciRedFn.put(inputMetadatoDinamico.getChiave(), valore);
					}
				}
			}
			salvaDocInput.setMetadatiDinamici(metadatiDinamiciRedFn);
		}

		// Gestione documenti cartacei - Recogniform (Modalita POST-SCANSIONE a.k.a.
		// POSTCENS) -> START
		salvaDocInput.setGuidDocumentiCartacei(detailDocInput.getGuidCartaceiAcquisiti());
		// Gestione documenti cartacei - Recogniform (Modalita POST-SCANSIONE a.k.a.
		// POSTCENS) -> END

		// Codice flusso per i documenti generati da flussi
		if(!StringUtils.isEmpty(detailDocInput.getCodiceFlusso())){
			salvaDocInput.setCodiceFlusso(detailDocInput.getCodiceFlusso());
		} else if(isFlussoSipad(detailDocInput.getMittenteContatto())){
			salvaDocInput.setCodiceFlusso(Constants.Varie.CODICE_FLUSSO_SIPAD);
		} else {
			salvaDocInput.setCodiceFlusso(null);
		}
		
		// Decreto Liquidazione SICOGE
		salvaDocInput.setDecretoLiquidazioneSicoge(detailDocInput.getDecretoLiquidazioneSicoge());

		status.setIterManualeSpedizione(detailDocInput.getSpedizioneMail());

		salvaDocInput.setProtocollaEMantieni(detailDocInput.getProtocollaEMantieni());

		if (detailDocInput.getIdUfficioCreatore() != null) {
			salvaDocInput.setIdUfficioCreatore(detailDocInput.getIdUfficioCreatore().longValue());
		}

		if (detailDocInput.getIdUtenteCreatore() != null) {
			salvaDocInput.setIdUtenteCreatore(detailDocInput.getIdUtenteCreatore().longValue());
		}

		// Metadati estesi
		salvaDocInput.setMetadatiEstesi(detailDocInput.getMetadatiEstesi());

		// Sotto categoria uscita
		salvaDocInput.setSottoCategoriaUscita(detailDocInput.getSottoCategoriaUscita());

		// ID registro ausiliario
		salvaDocInput.setIdRegistroAusiliario(detailDocInput.getIdRegistroAusiliario());

		// Metadati del registro ausiliario
		salvaDocInput.setMetadatiRegistroAusiliario(detailDocInput.getMetadatiRegistroAusiliario());

		// Protocollo Riferimento
		salvaDocInput.setProtocolloRiferimento(detailDocInput.getProtocolloRiferimento());

		// Integrazione Dati
		salvaDocInput.setIntegrazioneDati(detailDocInput.getIntegrazioneDati());

		// Identificatore processo
		salvaDocInput.setIdentificatoreProcesso(detailDocInput.getIdentificatoreProcesso());

		// Permesso modifica metadati minimi
		salvaDocInput.setModificaMetadatiMinimi(detailDocInput.isModificaMetadatiMinimi());
		LOGGER.info("trasformaInSalvaDocumento -> END. Utente: " + utente.getUsername());
		return salvaDocInput;
	}

	private boolean isFlussoSipad(Contatto mittente) {
		// Verifica flusso SIPAD
		boolean isFlussoSipad = false;
		if(mittente != null) {
			try {
				String [] mittentiSipad = StringUtils.split(pp.getParameterByString(PropertiesNameEnum.FLUSSO_SIPAD_MITTENTI.getKey()), ";");
				List<String> mittenti = Arrays.asList(mittentiSipad);
				isFlussoSipad = mittenti.contains(mittente.getMail()) || mittenti.contains(mittente.getMailPec());
			} catch (Exception e ) {
				LOGGER.error("Errore durante il recupero dell'utente mittente del documento.", e);
				isFlussoSipad = false;
			}
		}
		return isFlussoSipad;
	}

	private boolean isBozza(final SalvaDocumentoRedDTO docInput) {
		boolean isBozza = false;

		if (docInput != null && docInput.getFolder() != null) {
			isBozza = docInput.getFolder().equals(pp.getParameterByKey(PropertiesNameEnum.FOLDER_BOZZE_CE_KEY).split("\\|")[2]);
		}

		return isBozza;
	}

	private String getClasseDocumentaleDoc(final SalvaDocumentoRedStatusDTO status, final int idTipologiaDocumento, final Connection con) {
		String classeDocumentaleDoc = null;

		if (status.isPrecensito() || idTipologiaDocumento == 0) {
			classeDocumentaleDoc = pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_GENERICO_CLASSNAME_FN_METAKEY);
		} else {
			classeDocumentaleDoc = tipologiaDocumentoDAO.getClasseDocumentaleByIdTipologia(idTipologiaDocumento, con);
		}

		return classeDocumentaleDoc;
	}

	private void caricaCoordinatore(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO salvaDocInput, final DetailDocumentRedDTO docInput,
			final UtenteDTO utente, final Connection con) {
		if (docInput.getIdUtenteCoordinatore() != null) {
			final NodoUtenteCoordinatore utCoordinatore = utenteDAO.getNodoUtenteCoordinatore(utente.getIdUfficio(), docInput.getIdUtenteCoordinatore(), con);
			if (utCoordinatore != null) {
				status.setUtenteCoordinatore(true);
				salvaDocInput.setIdUtenteCoordinatore(docInput.getIdUtenteCoordinatore());
				salvaDocInput.setIdUfficioCoordinatore(utCoordinatore.getIdUfficioCoordinatore());
			} else {
				status.setUtenteCoordinatore(false);
				LOGGER.error("Attenzione, è stato selezionato il coordinatore: " + docInput.getIdUtenteCoordinatore()
						+ " ma non è stata trovata l'associazione nel database con il nodo: " + utente.getIdUfficio());
			}
		}
	}

	private static String getAssegnazioneCoordinatore(final SalvaDocumentoRedDTO docInput) {
		String assegnazioneCoordinatore = null;

		final Long idNodoCoordinatore = docInput.getIdUfficioCoordinatore() != null && docInput.getIdUfficioCoordinatore() > 0 ? docInput.getIdUfficioCoordinatore() : null;
		final Long idUtenteCoordinatore = docInput.getIdUtenteCoordinatore() != null && docInput.getIdUtenteCoordinatore() > 0 ? docInput.getIdUtenteCoordinatore() : null;
		if (idNodoCoordinatore != null && idUtenteCoordinatore != null) {
			assegnazioneCoordinatore = idNodoCoordinatore + "," + idUtenteCoordinatore;
		}

		return assegnazioneCoordinatore;
	}

	private static int getIdIter(final IterApprovativoDTO iterApprovativo) {
		int idIter = 0;

		if (iterApprovativo != null && iterApprovativo.getIdIterApprovativo() != null && iterApprovativo.getParziale() == 0) {
			idIter = iterApprovativo.getIdIterApprovativo();
		}

		return idIter;
	}

	/**
	 * Metodo per capire se il documento da registrare ha content.
	 * 
	 * @param SalvaDocumentoRedDTO Documento in input
	 * @return
	 */
	private boolean hasContent(final SalvaDocumentoRedDTO docInput) {
		boolean content = false;

		if (docInput != null && docInput.getContent() != null && StringUtils.isNotBlank(docInput.getContentType()) && StringUtils.isNotBlank(docInput.getNomeFile())) {
			content = true;
		}

		return content;
	}

	private static String[] getElencoLibroFirma(final String[] assegnazioni1, final String[] assegnazioni2, final TipoAssegnazioneEnum tipoAssegnazione1,
			final TipoAssegnazioneEnum tipoAssegnazione2) {
		String[] result = null;
		final List<String> elencoLibroFirmaList = new ArrayList<>();
		String[] assegnazioneSplit = null;

		if (assegnazioni1 != null && assegnazioni1.length > 0) {
			for (final String assegnazione : assegnazioni1) {
				assegnazioneSplit = assegnazione.split(",");
				elencoLibroFirmaList.add(assegnazioneSplit[0] + "," + assegnazioneSplit[1] + "," + tipoAssegnazione1);
			}
		}
		if (assegnazioni2 != null && assegnazioni2.length > 0) {
			for (final String assegnazione : assegnazioni2) {
				assegnazioneSplit = assegnazione.split(",");
				elencoLibroFirmaList.add(assegnazioneSplit[0] + "," + assegnazioneSplit[1] + "," + tipoAssegnazione2);
			}
		}

		result = new String[elencoLibroFirmaList.size()];
		elencoLibroFirmaList.toArray(result);

		return result;
	}

	private void gestisciMetadatiWorkflowCopiaConforme(final SalvaDocumentoRedDTO docInput, final Map<String, Object> metadatiWorkflow) {
		// N.B. Segue la logica di RED_NSD, cfr. UpdateDocumentoEvent righe 1835 - 1838
		Long idUfficioCopiaConforme = 0L;
		Long idUtenteCopiaConforme = 0L;

		if (!CollectionUtils.isEmpty(docInput.getAllegati())) {
			for (final AllegatoDTO allegato : docInput.getAllegati()) {
				if (Boolean.TRUE.equals(allegato.getCopiaConforme())) {
					idUfficioCopiaConforme = allegato.getIdUfficioCopiaConforme();
					idUtenteCopiaConforme = allegato.getIdUtenteCopiaConforme();
					break; // Il Responsabile per la Copia Conforme è lo stesso per tutti gli allegati
				}
			}
		}

		if (idUfficioCopiaConforme != null && idUfficioCopiaConforme != 0) {
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.FIRMA_COPIA_CONFORME_METAKEY), true);
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.UFFICIO_COPIA_CONFORME_WF_METAKEY), idUfficioCopiaConforme);
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.UTENTE_COPIA_CONFORME_WF_METAKEY), idUtenteCopiaConforme);
		}
	}

	private static boolean isAssegnazioneFirmaSiglaVisto(final SalvaDocumentoRedStatusDTO status) {
		boolean result = false;
		for (final AssegnazioneWorkflowDTO assegnazione : status.getAssegnazioniFascicolo()) {
			final TipoAssegnazioneEnum tipoAssegnazione = assegnazione.getTipoAssegnazione();
			if (TipoAssegnazioneEnum.FIRMA.equals(tipoAssegnazione) || TipoAssegnazioneEnum.FIRMA_MULTIPLA.equals(tipoAssegnazione)
					|| TipoAssegnazioneEnum.SIGLA.equals(tipoAssegnazione) || TipoAssegnazioneEnum.RIFIUTO_FIRMA.equals(tipoAssegnazione)
					|| TipoAssegnazioneEnum.VISTO.equals(tipoAssegnazione) || TipoAssegnazioneEnum.RIFIUTO_SIGLA.equals(tipoAssegnazione)
					|| TipoAssegnazioneEnum.RIFIUTO_VISTO.equals(tipoAssegnazione)) {
				result = true;
				break;
			}
		}
		return result;
	}

	private void aggiungiMetadatiAssegnazione(final SalvaDocumentoRedStatusDTO status, final Map<String, Object> metadatiWorkflow, final Long idUfficioDestinatario,
			final Long idUtenteDestinatario, final int idTipoAssegnazione) {
		// Metadato ID Nodo Destinatario
		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY), idUfficioDestinatario);
		// Metadato ID Utente Destinatario
		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY), idUtenteDestinatario);

		// Metadati ID Tipo Assegnazione
		if (!status.isDocEntrata()) {
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY), idTipoAssegnazione);
		}
	}

	private void aggiungiMetadatiAssegnazioniConoscenzaContributo(final Map<String, Object> metadatiWorkflow, final String[] assegnazioniConoscenza,
			final String[] assegnazioniContributo) {
		if (assegnazioniConoscenza.length > 0) {
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_CONOSCENZA_METAKEY), assegnazioniConoscenza);
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_CONOSCENZA_STORICO_METAKEY), assegnazioniConoscenza);
		}
		if (assegnazioniContributo.length > 0) {
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_CONTRIBUTI_METAKEY), assegnazioniContributo);
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_CONTRIBUTI_STORICO_METAKEY), assegnazioniContributo);
		}
	}

	private static Boolean aggiungiUtenteDelegato(final SalvaDocumentoRedStatusDTO status) {
		Boolean aggiungiUtenteDelegato = false;

		if (status.getAssegnazioniFirma().length > 0 || status.getAssegnazioniSigla().length > 0 || status.getAssegnazioniVisto().length > 0 || !status.isIterManuale()) {
			aggiungiUtenteDelegato = true;
		}

		return aggiungiUtenteDelegato;
	}

	private static String[] getArrayAssegnazioniStep(final String[] arrayAssegnazioniStep) {
		String[] arrayAssegnazioni = new String[0];
		final List<String> valoriAssegnazioni = new ArrayList<>();
		String currentStep;

		if (arrayAssegnazioniStep != null && arrayAssegnazioniStep.length > 0) {
			for (int i = 0; i < arrayAssegnazioniStep.length; i++) {
				final String[] splitted = arrayAssegnazioniStep[i].split(",");
				currentStep = splitted[0] + "," + splitted[1];
				valoriAssegnazioni.add(currentStep);
			}
		}

		arrayAssegnazioni = valoriAssegnazioni.toArray(arrayAssegnazioni);
		return arrayAssegnazioni;
	}

	private static void convertiDestinatariInCartacei(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput) {
		for (final DestinatarioRedDTO destinatario : docInput.getDestinatari()) {
			if (MezzoSpedizioneEnum.ELETTRONICO.equals(destinatario.getMezzoSpedizioneEnum())) {
				destinatario.setMezzoSpedizioneEnum(MezzoSpedizioneEnum.CARTACEO);
			}
		}
		// Si disabilita l'invio della mail del tab Spedizione
		status.setSpedisciMailDestElettronici(false);
		docInput.setMailSpedizione(null);
	}

	private void gestisciTracciaProcedimento(final SalvaDocumentoRedDTO docInput, final UtenteDTO utente, final Connection dwhCon) {
		final int idDocumento = NumberUtils.toInt(docInput.getDocumentTitle());
		EsitoOperazioneDTO esitoTracciaProcedimento = new EsitoOperazioneDTO(null, null, idDocumento, null);

		final boolean isDocumentoTracciato = tracciaDocumentiSRV.isDocumentoTracciato(utente, idDocumento, dwhCon);

		if (Boolean.TRUE.equals(docInput.getTracciaProcedimento()) && !isDocumentoTracciato) {
			esitoTracciaProcedimento = tracciaDocumentiSRV.tracciaDocumento(utente.getIdUfficio(), utente.getId(), utente.getIdRuolo(), idDocumento, dwhCon);
		} else if (!Boolean.TRUE.equals(docInput.getTracciaProcedimento()) && isDocumentoTracciato) {
			esitoTracciaProcedimento = tracciaDocumentiSRV.rimuoviDocumento(utente.getIdUfficio(), utente.getId(), utente.getIdRuolo(), idDocumento, dwhCon);
		}

		if (Boolean.FALSE.equals(esitoTracciaProcedimento.getFlagEsito())) {
			throw new RedException(esitoTracciaProcedimento.getNote());
		}
	}

	private void gestisciTemplateDocUscita(final SalvaDocumentoRedDTO docInput, final Long idUtenteCreatore, final Connection con) {
		final int idDocumento = NumberUtils.toInt(docInput.getDocumentTitle());
		templateDocUscitaSRV.aggiornaDatiDocumento(idDocumento, docInput.getTemplateDocUscitaMap(), idUtenteCreatore, con);
	}

	/**
	 * Metodo per l'inserimento di un nuovo documento: - per assegnazione interna a
	 * valle del processo di firma (quindi un nuovo documento in ingresso per i
	 * destinatari del documento firmato); - da parte dei batch di protocollazione
	 * PEC (FAD, PCC, etc.).
	 * 
	 * @param docInput  Documento di input.
	 * @param utente    Utente destinatario.
	 * @param fcehAdmin
	 * @param fpehAdmin
	 * @param con
	 */
	private void inserisciDocDaProcessoAutomatico(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente,
			final IFilenetCEHelper fcehAdmin, final FilenetPEHelper fpehAdmin, final Connection con) {
		LOGGER.info("inserisciDocDaProcessoAutomatico -> START");
		int idDocumento = 0;

		try {
			final Long idAoo = utente.getIdAoo();
			status.setDocDaSalvare(new DocumentoRedFnDTO());
			final Map<String, Object> metadatiDocumento = new HashMap<>();
			final Map<String, Object> metadatiWorkflow = new HashMap<>();

			// ### INSERIMENTO DEL DOCUMENTO NEL DB --- idDocumento/Document Title
			idDocumento = documentoDAO.insert(idAoo, docInput.getTipologiaDocumento().getIdTipologiaDocumento(), 0, con);
			// Impostazione del Document Title nel documento in input
			docInput.setDocumentTitle(String.valueOf(idDocumento));
			LOGGER.info("inserisciDocDaProcessoAutomatico -> ID del nuovo documento: " + idDocumento);

			// Si sta creando un documento per assegnazione interna a valle della firma di
			// un documento con soli destinatari interni e:
			// - Si inserisce il documento che è stato firmato nei metadati del workflow
			// come allegato dello stesso
			// - Si recupera il GUID della mail dal documento che è stato firmato (prima
			// versione).
			if (status.isDocAssegnazioneInterna()) {
				final Document vecchioDocFn = fcehAdmin.getDocumentByGuid(docInput.getGuid()); // Il GUID del documento in input è quello del documento che è stato firmato
				final VWAttachment vecchioDocFnAllegatoWorkflow = fpehAdmin.convertiInAllegatoWorkflow(vecchioDocFn);
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.DOC_OLD_ALLEGATO_WF_METAKEY), vecchioDocFnAllegatoWorkflow);

				final AnnotationDTO mailAnnotation = fcehAdmin.getAnnotationDocumentContent(
						(String) TrasformerCE.getMetadato(vecchioDocFn, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), FilenetAnnotationEnum.TEXT_MAIL, idAoo);
				if (mailAnnotation != null) {
					status.setInputMailGuid(IOUtils.toString(mailAnnotation.getContent()));
				}
			}

			// ### SECURITY -> START
			final List<Nodo> organigramma = nodoDAO.getAlberaturaBottomUp(idAoo, utente.getIdUfficio(), con);

			// Si aggiunge l'alberatura dell'ufficio destinatario
			// (idUfficioDestinatarioWorkflow) alle security se NON coincide con l'ufficio
			// dell'utente
			// (protocollatore nel caso di provenienza dai batch o firmatario nel caso di
			// documento per assegnazione interna a valle della firma)
			if (status.getIdUfficioDestinatarioWorkflow() != null && !status.getIdUfficioDestinatarioWorkflow().equals(utente.getIdUfficio())) {
				organigramma.addAll(nodoDAO.getAlberaturaBottomUp(idAoo, status.getIdUfficioDestinatarioWorkflow(), con));
			}

			status.setSecurityDoc(securitySRV.getSecurityByGruppo(organigramma));
			status.getDocDaSalvare().setSecurity(status.getSecurityDoc());
			// ### SECURITY -> END

			// ### CONTENT (DOCUMENTO PRINCIPALE)
			impostaContentDocumento(status, docInput);

			// ### ALLEGATI
			impostaAllegatiDocumento(status, docInput.getAllegati(), idAoo, con);

			// ### CREAZIONE DEL FASCICOLO -> START
			FascicoloRedFnDTO fascicolo = null;
			// Fascicolo passato in input
			if (StringUtils.isNotBlank(status.getIdFascicoloSelezionato())) {
				fascicolo = new FascicoloRedFnDTO();
				fascicolo.setIdFascicolo(status.getIdFascicoloSelezionato());
				// Creazione di un nuovo fascicolo nel DB
			} else {
				final int idFascicolo = fascicoloDAO.insert(status.getIndiceClassificazioneFascicolo(), idAoo, con);

				// Costruzione dell'oggetto Fascicolo da inserire su FileNet: si considerano
				// eventuali metadati aggiuntivi passati in input
				final String descrizioneFascicolo = costruisciDescrizioneFascicolo(docInput);
				if (!CollectionUtils.isEmpty(status.getMetadatiFascicolo())) {
					fascicolo = fascicoloSRV.costruisciFascicoloRedFn(String.valueOf(idFascicolo), status.getMetadatiFascicolo(), Calendar.getInstance().get(Calendar.YEAR),
							String.valueOf(idFascicolo), descrizioneFascicolo, status.getIndiceClassificazioneFascicolo(), idAoo);
				} else {
					fascicolo = fascicoloSRV.costruisciFascicoloRedFn(String.valueOf(idFascicolo), Calendar.getInstance().get(Calendar.YEAR), String.valueOf(idFascicolo),
							descrizioneFascicolo, status.getIndiceClassificazioneFascicolo(), idAoo);
				}
				// Impostazione delle security
				fascicolo.setSecurity(status.getDocDaSalvare().getSecurity());
			}
			// ### CREAZIONE DEL FASCICOLO -> END

			// ### INSERIMENTO DELL'ASSOCIAZIONE TRA DOCUMENTO E FASCICOLO
			fascicoloDAO.insertDocumentoFascicolo(idDocumento, NumberUtils.toInt(fascicolo.getIdFascicolo()), TipoFascicolo.AUTOMATICO, idAoo, con);

			// ### INSERIMENTO DOCUMENTO CON FASCICOLAZIONE CONTESTUALE ED INSERIMENTO
			// ANNOTATION MAIL SU FILENET -> START
			metadatiDocumento.putAll(caricaMetadatiDocumento(status, docInput, utente)); // Si aggiungono i Metadati ricavati dal documento in input

			// Si richiama il metodo "popolaESalvaDocumentoFilenet", che esegue le seguenti
			// operazioni:
			// - Crea il nuovo documento su FileNet con i dati in input (security, allegati, etc.)
			// - Aggiunge al nuovo documento l'annotation per collegarlo alla mail
			// - Crea il fascicolo con i dati in input (solo se non presente su FileNet,
			// i.e. l'attributo "classeDocumentale" del fascicolo in input è specificato)
			// - Fascicola il nuovo documento nel fascicolo appena creato
			// - Aggiunge al nuovo documento l'annotation CURRENT_VERSION.
			popolaESalvaDocumentoFilenet(status, idDocumento, docInput, fascicolo, metadatiDocumento, idAoo, fcehAdmin, con);

			// ### METADATI ESTESI ### -> START
			salvaMetadatiEstesi(idDocumento, docInput, status.isModalitaInsert(), utente.getIdAoo(), con, fcehAdmin);
			// ### METADATI ESTESI ### -> END

			// ### METADATI REGISTRO AUSILIARIO -> START
			salvaMetadatiRegistroAusiliario(docInput, status.isModalitaInsert(), con);
			// ### METADATI REGISTRO AUSILIARIO -> END

			LOGGER.info("inserisciDocDaProcessoAutomatico -> Inserimento su FileNet eseguito per il documento: " + idDocumento + ". GUID: " + docInput.getGuid());
			// ###### N.B. Da questo momento in poi, docInput.getGuid() restituisce il GUID
			// del nuovo documento appena creato!
			// ### INSERIMENTO DOCUMENTO CON FASCICOLAZIONE CONTESTUALE ED INSERIMENTO
			// ANNOTATION MAIL SU FILENET -> END

			// ### PROTOCOLLAZIONE -> START
			// Se non si sta creando un documento per assegnazione interna, si effettua la
			// protocollazione
			if (!status.isDocAssegnazioneInterna()) {
				protocollaDocumento(status, docInput, utente, fcehAdmin, con);
			}
			// ### PROTOCOLLAZIONE -> END

			// ### FALDONATURA -> START
			if (status.getNomeFaldoniSelezionati() != null) {
				for (final String faldone : status.getNomeFaldoniSelezionati()) {
					LOGGER.info("postSalvataggioDocumentoFilenet -> faldonaFascicoli: START. Documento: " + idDocumento);
					faldoneSRV.faldonaFascicolo(faldone, fascicolo.getIdFascicolo(), idAoo, fcehAdmin);
					LOGGER.info("postSalvataggioDocumentoFilenet -> faldonaFascicoli: END. Documento: " + idDocumento);
				}
			}
			// ### FALDONATURA -> END

			// ### WORKFLOW -> START
			// ### Se si tratta di un documento per assegnazione interna, si inseriscono
			// alcuni metadati di workflow aggiuntivi
			if (status.isDocAssegnazioneInterna()) {
				final Document nuovoDocFn = fcehAdmin.getDocumentByGuid(docInput.getGuid());
				final VWAttachment nuovoDocFnAllegatoWorkflow = fpehAdmin.convertiInAllegatoWorkflow(nuovoDocFn);
				// Si inserisce il nuovo documento come allegato del workflow
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.DOC_NEW_ALLEGATO_WF_METAKEY), nuovoDocFnAllegatoWorkflow);
				// Altri metadati aggiuntivi: isAssegnazioneInterna, idDocumentoOld e
				// motivazioneAssegnazione
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.IS_ASSEGNAZIONE_INTERNA_WF_METAKEY), status.getIsAssegnazioneInternaWorkflow().getId());
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_OLD_WF_METAKEY), status.getIdDocumentoOldWorkflow());
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), status.getMotivazioneAssegnazioneWorkflow());
			}

			// Metadati ID Documento e ID Fascicolo del workflow
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY), docInput.getDocumentTitle());
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY), Integer.parseInt(fascicolo.getIdFascicolo()));
			// Metadati ID Utente Mittente / ID Ufficio Destinatario / ID Utente
			// Destinatario / ID Ufficio Destinatario del workflow
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), status.getIdUtenteMittenteWorkflow());
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), status.getIdUfficioMittenteWorkflow());
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY), status.getIdUtenteDestinatarioWorkflow());
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY), status.getIdUfficioDestinatarioWorkflow());

			// Si avvia il nuovo workflow su FileNet
			avviaNuovoWorkflow(status, null, docInput, utente, metadatiWorkflow, fpehAdmin, con);
			// ### WORKFLOW -> END

			// ### TRASFORMAZIONE PDF ASINCRONA -> START
			// N.B. Solo se non si tratta di un documento per assegnazione interna
			if (!status.isDocAssegnazioneInterna()) {
				eseguiTrasformazionePDF(status, docInput, utente, null, null, null, null, true, false, true, status.isProtocollazioneP7M(), docInput.getTipoAssegnazioneEnum(),
						null, con);
				LOGGER.info("inserisciDocDaProcessoAutomatico -> Trasformazione PDF eseguita per il documento: " + idDocumento);
			}
			// ### TRASFORMAZIONE PDF ASINCRONA -> END
		} catch (final Exception e) {
			LOGGER.error("inserisciDocDaProcessoAutomatico -> Si è verificato un errore durante la creazione del documento: " + idDocumento, e);
			throw new RedException(e);
		}

		LOGGER.info("inserisciDocDaProcessoAutomatico -> END");
	}

	private void inserisciDocInternoDaFascicolo(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final boolean eseguiTrasformazionePDF,
			final UtenteDTO utente, final IFilenetCEHelper fceh, final Connection con) {
		LOGGER.info("inserisciDocInternoDaFascicolo -> START");
		LOGGER.info("inserisciDocInternoDaFascicolo -> eseguiTrasformazionePDF: " + eseguiTrasformazionePDF);

		try {
			status.setDocDaSalvare(new DocumentoRedFnDTO());

			// ### INSERIMENTO DEL DOCUMENTO NEL DB --- idDocumento/Document Title
			final int idDocumento = documentoDAO.insert(utente.getIdAoo(), docInput.getTipologiaDocumento().getIdTipologiaDocumento(), 0, con);
			// Impostazione del Document Title nel documento in input
			docInput.setDocumentTitle(String.valueOf(idDocumento));
			LOGGER.info("creaDocumentoInternoDaFascicolo -> ID del nuovo documento interno: " + idDocumento);

			if (idDocumento > 0) {
				final Map<String, Object> metadatiDocumento = new HashMap<>();

				// ### METADATI
				caricaMetadatiDocInternoDaFascicolo(docInput, metadatiDocumento, utente);

				// ### SECURITY -> START
				final ListSecurityDTO securityBase = securitySRV
						.getSecurityByGruppo(nodoDAO.getAlberaturaBottomUp(utente.getIdAoo(), utente.getIdUfficio(), con));
				final ListSecurityDTO securityFascicolo = securitySRV.getSecurityPerFascicolazione(status.getIdFascicoloSelezionato(), utente.getIdAoo(), fceh);

				securityBase.addAll(securityFascicolo);

				status.setSecurityDoc(securityBase);
				status.getDocDaSalvare().setSecurity(status.getSecurityDoc());
				// ### SECURITY -> END

				// ### CONTENT
				impostaContentDocumento(status, docInput);

				// ### CREAZIONE DEL NUOVO DOCUMENTO INTERNO SU FILENET
				popolaESalvaDocumentoFilenet(status, idDocumento, docInput, null, metadatiDocumento, utente.getIdAoo(), fceh, con);

				// ### FASCICOLAZIONE (FILENET E DB)
				fascicoloSRV.fascicola(status.getIdFascicoloSelezionato(), idDocumento, TipoFascicolo.NON_AUTOMATICO, utente.getIdAoo(), fceh, con);
				LOGGER.info("inserisciDocInternoDaFascicolo -> Il nuovo documento interno: " + idDocumento + " è stato fascicolato nel fascicolo: "
						+ status.getIdFascicoloSelezionato());

				// ### TRASFORMAZIONE PDF
				if (eseguiTrasformazionePDF) {
					final Integer fileRevisionNumber = fceh.getPreviousVersionWordRevisionNumber(docInput.getGuid());
					eseguiTrasformazionePDF(status, docInput, utente, null, null, null, fileRevisionNumber, true, true, true, docInput.getTipoAssegnazioneEnum(), null, con);
				}
			}
		} catch (final Exception e) {
			LOGGER.error("inserisciDocInternoDaFascicolo -> Si è verificato un errore durante la creazione del nuovo documento interno dal fascicolo: "
					+ status.getIdFascicoloSelezionato(), e);
			throw new RedException(e);
		}

		LOGGER.info("inserisciDocInternoDaFascicolo -> END");
	}

	private void caricaMetadatiDocInternoDaFascicolo(final SalvaDocumentoRedDTO docInput, final Map<String, Object> metadatiDocumento, final UtenteDTO utente) {
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY), docInput.getTipologiaDocumento().getIdTipologiaDocumento());
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY), CategoriaDocumentoEnum.INTERNO.getIds()[0]);
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.UTENTE_CREATORE_ID_METAKEY), utente.getId());
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY), utente.getIdUfficio());
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY), docInput.getOggetto());
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), docInput.getNomeFile());
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_FORMATO_DOCUMENTO), FormatoDocumentoEnum.ELETTRONICO.getId());
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), docInput.getDocumentTitle());
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DATA_ARCHIVIAZIONE_METAKEY), new Date());
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY), utente.getIdAoo());
	}

	private void inserisciDocDichiarazioneServiziResi(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente,
			final IFilenetCEHelper fceh, final FilenetPEHelper fpeh, final Connection con) {
		LOGGER.info("inserisciDocDichiarazioneServiziResi -> START");
		try {
			status.setDocDaSalvare(new DocumentoRedFnDTO());

			// ### INSERIMENTO DEL DOCUMENTO NEL DB --- idDocumento/Document Title
			final int idDocumento = documentoDAO.insert(utente.getIdAoo(), docInput.getTipologiaDocumento().getIdTipologiaDocumento(), 0, con);

			// Impostazione del Document Title nel documento in input
			docInput.setDocumentTitle(String.valueOf(idDocumento));
			LOGGER.info("inserisciDocDichiarazioneServiziResi -> ID del documento DSR: " + idDocumento);

			if (idDocumento > 0) {
				final Map<String, Object> metadatiDocumento = new HashMap<>();

				// ### SECURITY -> START
				// ### SECURITY DOCUMENTO PRINCIPALE/ALLEGATI -> START
				final boolean isNodoUcp = nodoDAO.isNodoUcp(utente.getIdUfficio(), con);

				// Si calcolano le security base per il documento (e gli eventuali allegati)
				final ListSecurityDTO securityDoc = securitySRV.getSecurityPerDocumento(docInput.getDocumentTitle(), utente, status.getAssegnazioniCompetenza(),
						null, null, null, null, null, null, utente.getIdUfficio(), utente.getId(), true, status.isDocEntrata(), 0, false, null, false, isNodoUcp, null, con);

				// Se si tratta di un documento DSR in entrata (Response "Protocolla DSR"),
				// si inseriscono nel documento le security dei destinatari della DSR,
				// in modo da permettere loro la visualizzazione del documento a valle della
				// predisposizione del documento DSR in uscita
				if (status.isDocEntrata()) {
					String[] assegnazioniDestinatariDSR = null;

					final List<UtenteDTO> destinatariDSR = utenteDAO.getUtentiByDescNodoByDescPermesso(pp.getParameterByKey(PropertiesNameEnum.FEPA_UFFICIO_DESTINATARI),
							pp.getParameterByKey(PropertiesNameEnum.FEPA_PERMESSI_DESTINATARI), con);
					if (!CollectionUtils.isEmpty(destinatariDSR)) {
						assegnazioniDestinatariDSR = new String[destinatariDSR.size()];
						for (int index = 0; index < destinatariDSR.size(); index++) {
							final UtenteDTO destinatarioDSR = destinatariDSR.get(index);
							assegnazioniDestinatariDSR[index] = destinatarioDSR.getIdUfficio() + "," + destinatarioDSR.getId();
						}
					}

					final ListSecurityDTO securityDocDestinatari = securitySRV.getSecurityPerDocumento(docInput.getDocumentTitle(), utente,
							assegnazioniDestinatariDSR, null, null, null, null, null, null, utente.getIdUfficio(), utente.getId(), true, status.isDocEntrata(), 0, false, null,
							false, isNodoUcp, null, con);
					securityDoc.addAll(securityDocDestinatari);

					// Se si tratta di un documento DSR in uscita (Response "Predisponi
					// Dichiarazione"),
					// si aggiungono le security dell'assegnatario per firma
				} else if (status.isDocUscita()) {
					final ListSecurityDTO securityDocAssegnatarioFirma = securitySRV.getSecurityPerDocumento(docInput.getDocumentTitle(), utente,
							status.getAssegnazioniFirma(), null, null, null, null, null, null, utente.getIdUfficio(), utente.getId(), true, status.isDocEntrata(), 0, false,
							null, false, isNodoUcp, null, con);

					securityDoc.addAll(securityDocAssegnatarioFirma);
				}

				status.setSecurityDoc(securityDoc);
				status.getDocDaSalvare().setSecurity(securityDoc);
				// ### SECURITY DOCUMENTO PRINCIPALE/ALLEGATI -> END

				// ### SECURITY FASCICOLO -> START
				// Si calcolano le security base per il fascicolo
				final ListSecurityDTO securityFascicolo = securitySRV.getSecurityPerDocumento(docInput.getDocumentTitle(), utente,
						status.getAssegnazioniCompetenza(), null, null, null, null, null, null, utente.getIdUfficio(), utente.getId(), false, status.isDocEntrata(), 0, false,
						null, false, isNodoUcp, null, con);

				// Se si tratta di un documento DSR in uscita (Response "Predisponi
				// Dichiarazione"),
				// si aggiungono le security dell'assegnatario per firma
				if (status.isDocUscita()) {
					final ListSecurityDTO securityFascicoloAssegnatarioFirma = securitySRV.getSecurityPerDocumento(docInput.getDocumentTitle(), utente,
							status.getAssegnazioniFirma(), null, null, null, null, null, null, utente.getIdUfficio(), utente.getId(), true, status.isDocEntrata(), 0, false,
							null, false, isNodoUcp, null, con);

					securityDoc.addAll(securityFascicoloAssegnatarioFirma);
				}
				// ### SECURITY FASCICOLO -> END
				LOGGER.info("inserisciDocDichiarazioneServiziResi -> Security per documento principale/allegati/fascicolo calcolate per il documento: " + idDocumento);
				// ### SECURITY -> END

				// ### CONTENT (DOCUMENTO PRINCIPALE)
				impostaContentDocumento(status, docInput);

				// ### ALLEGATI
				impostaAllegatiDocumento(status, docInput.getAllegati(), utente.getIdAoo(), con);

				// ### CREAZIONE DEL FASCICOLO -> START
				FascicoloRedFnDTO fascicolo = null;
				// Fascicolo passato in input
				if (!CollectionUtils.isEmpty(docInput.getFascicoli()) && docInput.getFascicoloProcedimentale() == null) {
					final FascicoloDTO inputFascicolo = docInput.getFascicoli().get(0);
					LOGGER.info("inserisciDocDichiarazioneServiziResi -> Fascicolo passato in input: " + inputFascicolo.getIdFascicolo());

					fascicolo = new FascicoloRedFnDTO();
					fascicolo.setIdFascicolo(inputFascicolo.getIdFascicolo());
					// Creazione del fascicolo prevista solo nel caso di documento DSR in entrata
					// (Response "Protocolla DSR")
				} else if (status.isDocEntrata()) {
					LOGGER.info("inserisciDocDichiarazioneServiziResi -> Nessun fascicolo passato in input, se ne crea uno nuovo.");
					// Creazione di un nuovo fascicolo sul DB
					final int idFascicolo = fascicoloDAO.insert(docInput.getIndiceClassificazioneFascicoloProcedimentale(), utente.getIdAoo(), con);

					// Costruzione dell'oggetto Fascicolo da inserire su FileNet
					final String descrizioneFascicolo = costruisciDescrizioneFascicolo(docInput);
					fascicolo = fascicoloSRV.costruisciFascicoloRedFn(String.valueOf(idFascicolo), Calendar.getInstance().get(Calendar.YEAR), String.valueOf(idFascicolo),
							descrizioneFascicolo, docInput.getIndiceClassificazioneFascicoloProcedimentale(), utente.getIdAoo());
				}
				// Impostazione delle security per il fascicolo
				if (fascicolo != null) {
					fascicolo.setSecurity(securityFascicolo);
				}
				// ### CREAZIONE DEL FASCICOLO -> END

				// ### METADATI
				caricaMetadatiDocDichiarazioneServiziResi(docInput, status, metadatiDocumento, utente);
				LOGGER.info("inserisciDocDichiarazioneServiziResi -> Caricamento dei metadati completato per il documento: " + idDocumento);

				// ### SALVATAGGIO DEL DOCUMENTO E DEL FASCICOLO SU FILENET
				popolaESalvaDocumentoFilenet(status, idDocumento, docInput, fascicolo, metadatiDocumento, utente.getIdAoo(), fceh, con);

				// ### PROTOCOLLAZIONE - Solo nel caso di documento DSR in entrata (Response
				// "Protocolla DSR")
				if (status.isDocEntrata()) {
					protocollaDocumento(status, docInput, utente, fceh, con);
				}

				// ### PREVIEW -> START
				final InputStream previewContent = PdfHelper.getPDFPreview(docInput.getContent());
				impostaAnnotationPreview(status, docInput.getDocumentTitle(), docInput.getGuid(), previewContent, utente.getIdAoo(), fceh);
				// ### PREVIEW -> END

				// ### WORKFLOW -> START
				// Si caricano i metadati del workflow
				Integer idFascicolo = null;
				if (fascicolo != null) {
					idFascicolo = Integer.parseInt(fascicolo.getIdFascicolo());
				}
				final Map<String, Object> metadatiWorkflow = caricaMetadatiWorkflowDichiarazioneServiziResi(status, docInput, idFascicolo, utente);

				// Si ottiene il nome del nuovo workflow da avviare
				final String nomeWorkflow = tipoProcedimentoDAO.getWorkflowNameByIdTipoProcedimento(docInput.getTipoProcedimento().getTipoProcedimentoId(), con);

				// Si avvia il nuovo workflow su FileNet
				avviaNuovoWorkflow(status, nomeWorkflow, docInput, utente, metadatiWorkflow, fpeh, con);

				// Nel caso di DSR in uscita, quindi di inserimento del documento DSR
				// sollecitato dalla response "Predisponi Dichiarazione",
				// si avanza il workflow della DSR in entrata con questa response
				if (status.isDocUscita() && !CollectionUtils.isEmpty(docInput.getAllacci())) {
					final RispostaAllaccioDTO allaccio = docInput.getAllacci().get(0);

					// Si recupera il workflow dell'allaccio (DSR in entrata)
					final List<VWWorkObject> workflowsAllaccio = fpeh.getWorkflowsByIdDocumentoNodoUtente(Integer.valueOf(allaccio.getIdDocumentoAllacciato()),
							utente.getIdUfficio(), utente.getId(), utente.getFcDTO().getIdClientAoo());

					if (!CollectionUtils.isEmpty(workflowsAllaccio)) {
						// Si avanza il workflow
						final boolean avanzamentoWorkflowOk = fpeh.nextStep(workflowsAllaccio.get(0).getWorkflowNumber(), null,
								ResponsesRedEnum.PREDISPONI_DICHIARAZIONE.getResponse());
						if (avanzamentoWorkflowOk) {
							LOGGER.info("inserisciDocDichiarazioneServiziResi -> Il workflow: " + status.getWorkflowList().get(0) + " è stato avanzato con la response: "
									+ ResponsesRedEnum.PREDISPONI_DICHIARAZIONE.name());
						}
					}
				}
				// ### WORKFLOW -> END

				// ### MODALITA MAIL IN INGRESSO (RESPONSE "PROTOCOLLA DSR") -> START
				if (status.isModalitaMailEntrata()) {
					// Si aggiorna lo stato dell'e-mail
					gestisciStatoEmailProtocollata(status.getInputMailGuid(), idDocumento, status.getNumeroProtocollo(), status.getAnnoProtocollo(),
							status.isInviaNotificaProtocollazioneMail(), utente, con);
				}
				// ### MODALITA MAIL IN INGRESSO (RESPONSE "PROTOCOLLA DSR") -> END

				// ### AGGIORNAMENTO DELLE SECURITY PER DOCUMENTO PRINCIPALE/ALLEGATI E
				// FASCICOLO -> START
				// Si recuperano le security di default (filtrate) della Classe Documentale
				// "Dichiarazione Servizi Resi"
				final AccessPermissionList securityDSRClass = fceh.getDefaultInstancePermissions(docInput.getTipologiaDocumento().getDocumentClass());

				// Si aggiornano le security del documento principale e degli eventuali allegati
				securitySRV.aggiornaSecurityDocumento(docInput.getDocumentTitle(), utente.getIdAoo(), securityDSRClass, true, false, fceh);

				if (fascicolo != null) {
					// Si aggiornano le security del fascicolo
					fceh.updateFascicoloSecurity(fascicolo.getIdFascicolo(), utente.getIdAoo().intValue(), securityDSRClass);
				}
				// ### AGGIORNAMENTO DELLE SECURITY PER DOCUMENTO PRINCIPALE/ALLEGATI E
				// FASCICOLO -> END
			}
		} catch (final Exception e) {
			LOGGER.error("inserisciDocDichiarazioneServiziResi -> Si è verificato un errore durante la creazione della DSR: " + e.getMessage(), e);
			rollbackInserimentoDocumento(status, e, "inserisciDocDichiarazioneServiziResi", utente);
		}

		LOGGER.info("inserisciDocDichiarazioneServiziResi -> END");
	}

	private void caricaMetadatiDocDichiarazioneServiziResi(final SalvaDocumentoRedDTO docInput, final SalvaDocumentoRedStatusDTO status,
			final Map<String, Object> metadatiDocumento, final UtenteDTO utente) {
		// Metadato Document Title
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), docInput.getDocumentTitle());

		// Metadato ID AOO
		if (docInput.getIdAOO() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY), docInput.getIdAOO());
		} else {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY), utente.getIdAoo());
		}

		// Metadato Tipo Protocollo
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY), TipoProtocolloEnum.ENTRATA.getId());

		// Metadati Numero Protocollo Risposta e Anno Protocollo Risposta
		if (!CollectionUtils.isEmpty(docInput.getAllacci())) {
			final RispostaAllaccioDTO allaccioRisposta = docInput.getAllacci().get(0);

			if (allaccioRisposta.getNumeroProtocollo() > 0) {
				metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_RISPOSTA_STRING_METAKEY), String.valueOf(allaccioRisposta.getNumeroProtocollo()));
			}
			if (allaccioRisposta.getAnnoProtocollo() > 0) {
				metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_RISPOSTA_METAKEY), allaccioRisposta.getAnnoProtocollo());
			}

		}
		if (docInput.getNumeroProtocolloRisposta() != null && !docInput.getNumeroProtocolloRisposta().isEmpty()) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_RISPOSTA_STRING_METAKEY), docInput.getNumeroProtocolloRisposta());
		}
		if (docInput.getAnnoProtocolloRisposta() != null && docInput.getAnnoProtocolloRisposta() > 0) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_RISPOSTA_METAKEY), docInput.getAnnoProtocolloRisposta());
		}

		// Metadato Oggetto
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY), docInput.getOggetto());

		// Metadato Riservato
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY),
				Boolean.TRUE.equals(docInput.getIsRiservato()) ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue());

		// Metadato Nome File (se esiste il content)
		if (docInput.getContent() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), docInput.getNomeFile());
		}

		// Metadato ID Applicazione
		if (StringUtils.isNotBlank(pp.getParameterByKey(PropertiesNameEnum.ID_APPLICAZIONE_VALUE_METAKEY))) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_APPLICAZIONE_KEY_METAKEY),
					Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.ID_APPLICAZIONE_VALUE_METAKEY)));
		}

		// Metadato ID Utente Protocollatore
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_PROTOCOLLATORE_METAKEY), utente.getId());

		// Metadato ID Gruppo Protocollatore
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_GRUPPO_PROTOCOLLATORE_METAKEY), utente.getIdUfficio());

		// Metadato Mittente
		if (docInput.getMittenteContatto() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.MITTENTE_METAKEY),
					getMittenteMetaKey(docInput.getMittenteContatto().getContattoID().toString(), docInput.getMittenteContatto().getAliasContatto()));
		}

		// Metadato Data Archiviazione
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DATA_ARCHIVIAZIONE_METAKEY), new Date());

		// Nel caso di DSR in uscita (inserimento sollecitato dalla response "Predisponi
		// Dichiarazione", si aggiunge il metadato Destinatari Documento
		// (per la DSR sono solo destinatari interni)
		if (!CollectionUtils.isEmpty(docInput.getDestinatari())) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY), status.getDestinatariString());
		}

		// Metadato ID Utente Creatore
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.UTENTE_CREATORE_ID_METAKEY), utente.getId() != null ? utente.getId() : 0);

		// Metadato ID Ufficio Creatore
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY), utente.getIdUfficio());

		// Metadato ID Tipologia Documento
		if (docInput.getTipologiaDocumento() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY), docInput.getTipologiaDocumento().getIdTipologiaDocumento());
		}

		// Metadato ID Tipo Procedimento
		if (docInput.getTipoProcedimento() != null && docInput.getTipoProcedimento().getTipoProcedimentoId() > 0) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY), docInput.getTipoProcedimento().getTipoProcedimentoId());
		}

		// Metadato ID Categoria Documento
		if (docInput.getIdCategoriaDocumento() != null && docInput.getIdCategoriaDocumento() > 0) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY), docInput.getIdCategoriaDocumento());
		}

		// Metadato ID Formato Documento
		if (docInput.getFormatoDocumentoEnum() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_FORMATO_DOCUMENTO), docInput.getFormatoDocumentoEnum().getId());
		}

		// Metadato Mezzo Ricezione
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_MEZZORICEZIONE),
				docInput.getMezzoRicezione() != null ? docInput.getMezzoRicezione().getIdMezzoRicezione() : 0);

		// Metadato Registro Riservato
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY),
				Boolean.TRUE.equals(docInput.getRegistroRiservato()) ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue());

		// Metadato Urgente
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.URGENTE_METAKEY),
				Boolean.TRUE.equals(docInput.getIsUrgente()) ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue());

		// Metadato Flag Corte Conti
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.FLAG_CORTE_CONTI_METAKEY),
				Boolean.TRUE.equals(docInput.getFlagCorteConti()) ? Boolean.TRUE : Boolean.FALSE);

		// Metadato Assegnatari Competenza
		if (status.getAssegnazioniCompetenza().length > 0) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ASSEGNATARI_COMPETENZA_METAKEY), status.getAssegnazioniCompetenza());
		}
		
		// Metadato ID Momento Protocollazione
		if (docInput.getMomentoProtocollazioneEnum() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.MOMENTO_PROTOCOLLAZIONE_ID_METAKEY), docInput.getMomentoProtocollazioneEnum().getId());
		}else {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.MOMENTO_PROTOCOLLAZIONE_ID_METAKEY), MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD.getId());
		}
	}

	private Map<String, Object> caricaMetadatiWorkflowDichiarazioneServiziResi(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput,
			final Integer idFascicolo, final UtenteDTO utente) {
		final Map<String, Object> metadatiWorkflow = new HashMap<>();

		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_CLIENT_METAKEY), pp.getParameterByKey(PropertiesNameEnum.WS_IDCLIENT));
		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY), docInput.getDocumentTitle());
		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY), idFascicolo);

		// Il destinatario è sempre memorizzato nel formato idNodo,idUtente. In caso sia
		// presente solo il nodo, l'idUtente è pari a 0.
		final String[] destinatario = status.getAssegnazioniCompetenza()[0].split(",");
		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY), destinatario[0]);
		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY), destinatario[1]);

		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId());
		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), utente.getIdUfficio());

		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.FLAG_RENDERIZZATO_METAKEY), 1);

		final String[] assegnatari = new String[1];
		assegnatari[0] = status.getAssegnazioniCompetenza()[0] + ",1";
		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ITEM_ASSEGNATARI_METAKEY), assegnatari);

		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY), 1);

		// Nel caso di DSR in entrata, si aggiunge il metadato ID Formato Documento
		if (status.isDocEntrata()) {
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_FORMATO_DOCUMENTO), docInput.getFormatoDocumentoEnum().getId());
			// Nel caso di DSR in uscita (inserimento sollecitato dalla response "Predisponi
			// Dichiarazione"), si aggiunge il metadato Elenco Destinatari Interni
		} else if (status.isDocUscita()) {
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_DESTINATARI_INTERNI_METAKEY), status.getDestinatariInterniString());
		}

		return metadatiWorkflow;
	}

	private void aggiornaDocDichiarazioneServiziResi(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente,
			final IFilenetCEHelper fceh, final FilenetPEHelper fpeh, final Connection con) {
		final String idDocumento = docInput.getDocumentTitle();
		LOGGER.info("aggiornaDocDichiarazioneServiziResi -> START. Documento DSR: " + idDocumento);

		try {
			final Map<String, Object> metadatiDocumento = new HashMap<>();
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY), docInput.getOggetto());

			Document documentoFilenet = fceh.getDocumentByDTandAOO(idDocumento, utente.getIdAoo());

			// ### AGGIORNAMENTO DEL CONTENT SU FILENET
			// Il content viene aggiornato solo se è cambiato rispetto a quello attuale e se
			// il nuovo content è una DSR (PDF con una firma digitale e un campo firma
			// vuoto)
			if (status.isContentVariato()) {
				final IAdobeLCHelper adobeh = new AdobeLCHelper();
				final InputStream contentIS = new ByteArrayInputStream(docInput.getContent());
				final CountSignsOutputDTO countSigns = adobeh.countsSignedFields(contentIS);

				final boolean isDocDSR = !(countSigns.getNumSignFields() < 2 || countSigns.getNumSigns() < 1 && countSigns.getNumEmptySignFields() < 1);
				if (isDocDSR) {
					metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), docInput.getNomeFile());
					final InputStream previewContent = PdfHelper.getPDFPreview(docInput.getContent());

					// Si aggiornano il content e i metadati su FileNet
					documentoFilenet = fceh.addVersion(documentoFilenet, contentIS, previewContent, metadatiDocumento, docInput.getNomeFile(), docInput.getContentType(),
							utente.getIdAoo());
				}
				// ### AGGIORNAMENTO DEI METADATI SU FILENET
			} else {
				fceh.updateMetadati(documentoFilenet, metadatiDocumento);
			}

			// ### AGGIORNAMENTO DEL FASCICOLO PROCEDIMENTALE
			// N.B. Se il documento viene spostato in un altro fascicolo, il metodo provvede
			// anche all'aggiornamento dei metadati relativi al fascicolo per tutti workflow
			// del documento
			aggiornaFascicoloProcedimentale(docInput, docInput.getFascicoli().get(0), docInput.getFascicoli().get(0), utente, utente.getIdAoo(), fceh, fpeh, con);

			// ### GESTIONE DEGLI ALLEGATI (AGGIORNAMENTO/ELIMINAZIONE)
			gestisciModificaAllegatiDichiarazioneServiziResi(docInput, idDocumento, documentoFilenet, utente, fceh, con);
		} catch (final Exception e) {
			LOGGER.error("aggiornaDocDichiarazioneServiziResi -> Si è verificato un errore durante l'aggiornamento della DSR. Documento: " + idDocumento, e);
			throw new RedException(e);
		}

		LOGGER.info("aggiornaDocDichiarazioneServiziResi -> END. Documento DSR: " + idDocumento);
	}

	private void gestisciModificaAllegatiDichiarazioneServiziResi(final SalvaDocumentoRedDTO docInput, final String idDocumento, final Document docPrincipaleFilenet,
			final UtenteDTO utente, final IFilenetCEHelper fceh, final Connection con) {
		LOGGER.info("gestisciModificaAllegatiDichiarazioneServiziResi -> START. Documento: " + idDocumento);
		List<DocumentoRedFnDTO> inputAllegatiRedFn;
		List<DocumentoRedFnDTO> allegatiDaAggiornare;
		List<DocumentoRedFnDTO> allegatiDaInserire;
		final Long idAoo = utente.getIdAoo();

		try {
			inputAllegatiRedFn = allegatoSRV.convertToAllegatiFilenet(docInput.getAllegati(), idAoo, con); // Lista
			// degli
			// allegati
			// in input
		} catch (final Exception e) {
			LOGGER.error("gestisciModificaAllegatiDichiarazioneServiziResi -> Si è verificato un errore durante la creazione degli allegati del documento.", e);
			throw new RedException("gestisciModificaAllegatiDichiarazioneServiziResi -> Si è verificato un errore durante la creazione degli allegati del documento.", e);
		}

		// ### Gestione degli allegati eliminati -> START
		gestisciEliminazioneAllegati(docInput, inputAllegatiRedFn, docPrincipaleFilenet, utente, fceh, null);
		// ### Gestione degli allegati eliminati -> END

		// ### Gestione degli allegati inseriti/aggiornati
		if (inputAllegatiRedFn != null && !inputAllegatiRedFn.isEmpty()) {
			allegatiDaAggiornare = new ArrayList<>();
			allegatiDaInserire = new ArrayList<>();

			for (final DocumentoRedFnDTO inputAllegatoRedFn : inputAllegatiRedFn) {
				if (inputAllegatoRedFn.getGuid() != null) {
					allegatiDaAggiornare.add(inputAllegatoRedFn);
				} else {
					allegatiDaInserire.add(inputAllegatoRedFn);
				}
			}

			// ### Gestione degli allegati da inserire (nuovi)
			if (!allegatiDaInserire.isEmpty()) {
				fceh.inserisciAllegati(docPrincipaleFilenet, allegatiDaInserire);
			}

			// ### Gestione degli allegati da aggiornare (esistenti)
			for (final DocumentoRedFnDTO allegatoDaAggiornare : allegatiDaAggiornare) {
				fceh.aggiornaAllegato(docPrincipaleFilenet, allegatoDaAggiornare, null, true, utente.getIdAoo());
			}
		}
		LOGGER.info("gestisciModificaAllegatiDichiarazioneServiziResi -> END. Documento: " + idDocumento);
	}

	private void inserisciDocFatturaODecreto(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente, final IFilenetCEHelper fceh,
			final FilenetPEHelper fpeh, final Connection con) {
		LOGGER.info("inserisciDocFatturaODecreto -> START");

		try {
			final Long idAoo = utente.getIdAoo();
			final boolean isDecretoDirigenzialeFepa = pp.getParameterByKey(PropertiesNameEnum.DECRETO_DIRIGENZIALE_FEPA_CLASSNAME_FN_METAKEY)
					.equals(docInput.getTipologiaDocumento().getDocumentClass());
			status.setDocDaSalvare(new DocumentoRedFnDTO());
			final Map<String, Object> metadatiDocumento = new HashMap<>();

			// ### INSERIMENTO DEL DOCUMENTO NEL DB --- idDocumento/Document Title
			final int idDocumento = documentoDAO.insert(idAoo, docInput.getTipologiaDocumento().getIdTipologiaDocumento(),
					Boolean.TRUE.equals(docInput.getRegistroRiservato()) ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue(), con);
			// Impostazione del Document Title nel documento in input
			docInput.setDocumentTitle(String.valueOf(idDocumento));

			// ### SECURITY -> START
			final boolean isNodoUcp = nodoDAO.isNodoUcp(utente.getIdUfficio(), con);
			impostaSecurityDocumento(status, docInput, idDocumento, utente, isNodoUcp, con);
			LOGGER.info("inserisciDocFatturaODecreto -> Security impostate per il documento: " + idDocumento);
			// ### SECURITY -> END

			// ### CONTENT DOCUMENTO -> START
			impostaContentDocumento(status, docInput);
			LOGGER.info("inserisciDocFatturaODecreto -> Content impostato per il documento: " + idDocumento);
			// ### CONTENT DOCUMENTO -> END

			// ### METADATI
			caricaMetadatiDocFatturaODecreto(status, docInput, metadatiDocumento, utente);
			LOGGER.info("inserisciDocFatturaODecreto -> Caricamento dei metadati completato per il documento: " + idDocumento);

			// ### SALVATAGGIO DEL DOCUMENTO E DEL FASCICOLO SU FILENET
			popolaESalvaDocumentoFilenet(status, idDocumento, docInput, null, metadatiDocumento, idAoo, fceh, con);

			// ### OPERAZIONI DA ESEGUIRE IN SEGUITO AL SALVATAGGIO DEL DOCUMENTO SU FILENET
			// -> START
			// ### FASCICOLAZIONE - INSERIMENTO DEI FASCICOLI -> START
			status.setFascicoloAutomatico(true);

			// Security del documento per il fascicolo
			final ListSecurityDTO securityFascicoloAutomatico = securitySRV.getSecurityPerDocumento(String.valueOf(idDocumento), utente,
					status.getAssegnazioniCompetenza(), status.getAssegnazioniConoscenza(), status.getAssegnazioniContributo(), status.getAssegnazioniFirma(),
					status.getAssegnazioniSigla(), status.getAssegnazioniVisto(), status.getAssegnazioniFirmaMultipla(), utente.getIdUfficio(), 0L, false,
					status.isDocEntrata(), 0, docInput.getIsRiservato(), null, false, isNodoUcp, null, con);

			// CREAZIONE DEL FASCICOLO AUTOMATICO -> START
			// Si aggiunge il metadato ID Fascicolo FEPA
			status.setMetadatiFascicolo(new HashMap<>());
			status.getMetadatiFascicolo().put(pp.getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA),
					docInput.getMetadatiDinamici().get(pp.getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA)));

			// ### Si crea il fascicolo automatico
			// N.B. Esegue anche la fascicolazione del documento sul DB
			aggiungiAssegnazioniFascicoloAutomatico(status, status.getAssegnazioniCompetenza(), idDocumento, docInput, utente, null, securityFascicoloAutomatico,
					TipoAssegnazioneEnum.COMPETENZA, fceh, con);

			if (!CollectionUtils.isEmpty(status.getAssegnazioniFascicolo())) {
				final String idFascicolo = status.getAssegnazioniFascicolo().get(0).getFascicolo().getIdFascicolo();

				// Si procede alla fascicolazione anche su FileNet
				fascicoloSRV.fascicolaSuFilenet(idFascicolo, idDocumento, idAoo, fceh);
				// CREAZIONE DEL FASCICOLO AUTOMATICO -> END

				// ### Si inseriscono le Fatture FEPA nel fascicolo automatico appena creato,
				// cioè nel fascicolo del nuovo documento Decreto Dirigenziale (DD)
				if (isDecretoDirigenzialeFepa) {
					final List<?> idFatturaFepaList = (List<?>) docInput.getMetadatiDinamici().get(pp.getParameterByKey(PropertiesNameEnum.ELENCO_FATTURE_FEPA));
					if (!CollectionUtils.isEmpty(idFatturaFepaList)) {
						for (final Object idFatturaFepaObj : idFatturaFepaList) {
							final String documentTitleFatturaFepa = fceh
									.getIdDocumentoByMetadatoFepa(pp.getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA), (String) idFatturaFepaObj);
							// Fascicolazione su FileNet della Fattura FEPA nel fascicolo del Decreto
							// Dirigenziale
							fascicoloSRV.fascicolaSuFilenet(idFascicolo, Integer.parseInt(documentTitleFatturaFepa), utente.getIdAoo(), fceh);
						}
					}
				}
			}
			// ### FASCICOLAZIONE - INSERIMENTO DEI FASCICOLI -> END

			// ### AVVIO DEL WORKFLOW SU FILENET -> START
			final Map<String, Object> metadatiWorkflow = new HashMap<>();
			// Si caricano i metadati del nuovo workflow
			caricaMetadatiWorkflowFatturaODecreto(status, docInput, metadatiWorkflow, utente, isDecretoDirigenzialeFepa);

			// Si ottiene il nome del nuovo workflow da avviare
			final String nomeWorkflow = tipoProcedimentoDAO.getWorkflowNameByIdTipoProcedimento(docInput.getTipoProcedimento().getTipoProcedimentoId(), con);

			// Si avvia il nuovo workflow
			avviaNuovoWorkflow(status, nomeWorkflow, docInput, utente, metadatiWorkflow, fpeh, con);
			LOGGER.info("inserisciDocFatturaODecreto -> Sono stati avviati " + status.getWorkflowList().size() + " workflow per il documento: " + idDocumento);
			// ### AVVIO DEL WORKFLOW SU FILENET -> END

			// ### TRASFORMAZIONE PDF
			if (isDecretoDirigenzialeFepa) {
				// Esecuzione della trasformazione PDF in modalità asincrona
				eseguiTrasformazionePDF(status, docInput, utente, status.getAssegnazioniCompetenza(), null, null, null, true, true, true, docInput.getTipoAssegnazioneEnum(),
						null, con);
			}
			// ### OPERAZIONI DA ESEGUIRE IN SEGUITO AL SALVATAGGIO DEL DOCUMENTO SU FILENET
			// -> END
		} catch (final Exception e) {
			LOGGER.error("inserisciDocFatturaODecreto -> Si è verificato un errore durante la creazione del documento: " + e.getMessage(), e);
			rollbackInserimentoDocumento(status, e, "inserisciDocFatturaODecreto", utente);
		}

		LOGGER.info("inserisciDocFatturaODecreto -> END");
	}

	private void caricaMetadatiDocFatturaODecreto(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final Map<String, Object> metadatiDocumento,
			final UtenteDTO utente) {
		// Si inseriscono i metadati Document Title, Data Archiviazione e ID AOO
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), docInput.getDocumentTitle());
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DATA_ARCHIVIAZIONE_METAKEY), new Date());
		if (docInput.getIdAOO() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY), docInput.getIdAOO());
		} else {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY), utente.getIdAoo());
		}

		// Metadato Iter Approvativo
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ITER_APPROVATIVO_METAKEY), "");

		// Metadato Numero Protocollo
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY), docInput.getNumeroProtocollo());

		// Metadato Anno Protocollo
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY), docInput.getAnnoProtocollo());

		// Metadato Data Protocollo
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY), docInput.getDataProtocollo());

		// Metadato ID Utente Creatore
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.UTENTE_CREATORE_ID_METAKEY), utente.getId() != null ? utente.getId() : 0);

		// Metadato ID Ufficio Creatore
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY), utente.getIdUfficio());

		// Metadato Nome File (se esiste il content)
		if (docInput.getContent() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), docInput.getNomeFile());
		}

		// Metadato ID Applicazione
		if (StringUtils.isNotBlank(pp.getParameterByKey(PropertiesNameEnum.ID_APPLICAZIONE_VALUE_METAKEY))) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_APPLICAZIONE_KEY_METAKEY),
					Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.ID_APPLICAZIONE_VALUE_METAKEY)));
		}

		// Metadato ID Categoria Documento
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY), docInput.getIdCategoriaDocumento());

		// Metadato ID Tipologia Documento
		if (docInput.getTipologiaDocumento() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY), docInput.getTipologiaDocumento().getIdTipologiaDocumento());
		}

		// Metadato Mezzo Ricezione
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_MEZZORICEZIONE), 0);

		// Metadato Mittente
		if (docInput.getMittenteContatto() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.MITTENTE_METAKEY),
					getMittenteMetaKey(docInput.getMittenteContatto().getContattoID().toString(), docInput.getMittenteContatto().getAliasContatto()));
		}

		// Metadato ID Tipo Procedimento
		if (docInput.getTipoProcedimento() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY), docInput.getTipoProcedimento().getTipoProcedimentoId());
		}

		// Metadato ID Momento Protocollazione
		if (docInput.getMomentoProtocollazioneEnum() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.MOMENTO_PROTOCOLLAZIONE_ID_METAKEY), docInput.getMomentoProtocollazioneEnum().getId());
		}else {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.MOMENTO_PROTOCOLLAZIONE_ID_METAKEY), MomentoProtocollazioneEnum.PROTOCOLLAZIONE_STANDARD.getId());
		}

		// Metadato Riservato
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY),
				Boolean.TRUE.equals(docInput.getIsRiservato()) ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue());

		// Metadato Oggetto
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY), docInput.getOggetto());

		// Metadato Numero Protocollo Mittente
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_MITTENTE_METAKEY), docInput.getProtocolloMittente());

		// Metadato Anno Protocollo Mittente
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_MITTENTE_METAKEY), docInput.getAnnoProtocolloMittente());

		// Metadato Data Protocollo Mittente
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_MITTENTE_METAKEY), docInput.getDataProtocolloMittente());

		// Metadato Urgente
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.URGENTE_METAKEY),
				Boolean.TRUE.equals(docInput.getIsUrgente()) ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue());

		// Metadato Assegnatari Competenza
		if (status.getAssegnazioniCompetenza().length > 0) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ASSEGNATARI_COMPETENZA_METAKEY), status.getAssegnazioniCompetenza());
		}

		// Metadato Numero Protocollo Risposta
		if (docInput.getNumeroProtocolloRisposta() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_RISPOSTA_STRING_METAKEY), docInput.getNumeroProtocolloRisposta());
		}

		// Metadato Anno Protocollo Risposta
		if (docInput.getAnnoProtocolloRisposta() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_RISPOSTA_METAKEY), docInput.getAnnoProtocolloRisposta());
		}

		// Metadato Barcode
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.BARCODE_METAKEY),
				StringUtils.isNotBlank(docInput.getBarcode()) ? docInput.getBarcode() : Constants.EMPTY_STRING);

		// Metadato ID Formato Documento
		if (docInput.getFormatoDocumentoEnum() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_FORMATO_DOCUMENTO), docInput.getFormatoDocumentoEnum().getId());
		}

		// Metadato Numero Raccomandata
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_RACCOMANDATA_METAKEY), docInput.getNumeroRaccomandata());

		// Metadato Note
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NOTE_METAKEY), docInput.getNote());

		// Metadato Data Scadenza
		if (docInput.getDataScadenza() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY), docInput.getDataScadenza());
		}

		// Metadato Registro Riservato
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY),
				Boolean.TRUE.equals(docInput.getRegistroRiservato()) ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue());

		// Metadato Destinatario Contributo Esterno
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DESTINATARIO_CONTRIBUTO_ESTERNO), docInput.getDestinatarioContributoEsterno());

		// Metadato Flag Corte Conti
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.FLAG_CORTE_CONTI_METAKEY),
				Boolean.TRUE.equals(docInput.getFlagCorteConti()) ? Boolean.TRUE : Boolean.FALSE);

		// Metadati Dinamici
		if (!CollectionUtils.isEmpty(docInput.getMetadatiDinamici())) {
			metadatiDocumento.putAll(docInput.getMetadatiDinamici());
		}
	}

	private void caricaMetadatiWorkflowFatturaODecreto(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput,
			final Map<String, Object> metadatiWorkflow, final UtenteDTO utente, final boolean isDecretoDirigenzialeFepa) {
		// Metadato ID Documento
		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY), Integer.parseInt(docInput.getDocumentTitle()));

		// Metadato ID Nodo Mittente
		if (status.getIdUfficioMittenteWorkflow() != null) {
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), status.getIdUfficioMittenteWorkflow());
		} else {
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), utente.getIdUfficio());
		}

		// Metadato ID Utente Mittente
		if (status.getIdUtenteMittenteWorkflow() != null) {
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), status.getIdUtenteMittenteWorkflow());
		} else {
			metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId());
		}

		// Metadato ID Coda Lavorazione
		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_CODA_LAVORAZIONE_FN_METAKEY), BooleanFlagEnum.NO.getIntValue());

		// Metadato ID Fascicolo
		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY), status.getAssegnazioniFascicolo().get(0).getFascicolo().getIdFascicolo());

		// N.B. Il destinatario del workflow è l'assegnatario per competenza del
		// documento (Decreto Dirigenziale o Fattura FEPA)
		final String[] assegnazioneCompetenzaSplit = status.getAssegnazioniCompetenza()[0].split(",");
		// Metadato ID Nodo Destinatario
		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY), Long.valueOf(assegnazioneCompetenzaSplit[0]));

		// Metadato ID Utente Destinatario
		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY), Long.valueOf(assegnazioneCompetenzaSplit[1]));

		if (!CollectionUtils.isEmpty(docInput.getMetadatiDinamici())) {
			// Metadato Elenco Fatture Fepa nel caso di Decreto Dirigenziale FEPA
			if (isDecretoDirigenzialeFepa) {
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ELENCO_FATTURE_FEPA),
						docInput.getMetadatiDinamici().get(pp.getParameterByKey(PropertiesNameEnum.ELENCO_FATTURE_FEPA)));
				// Metadato ID Fattura FEPA nel caso di Fattura FEPA
			} else {
				metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA),
						docInput.getMetadatiDinamici().get(pp.getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA)));
			}
		}
	}

	private void inserisciDocIgepa(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente, final IFilenetCEHelper fceh,
			final FilenetPEHelper fpeh, final Connection con) {
		LOGGER.info("inserisciDocIgepa -> START");
		try {
			final Long idAoo = utente.getIdAoo();
			status.setDocDaSalvare(new DocumentoRedFnDTO());

			final boolean isOPF = pp.getParameterByKey(PropertiesNameEnum.IGEPA_OPF_CLASSNAME_FN_METAKEY).equals(docInput.getTipologiaDocumento().getDocumentClass());
			LOGGER.info("inserisciDocIgepa -> È un Ordine di Pagamento Fondi (OPF) IGEPA : " + isOPF);

			// ### INSERIMENTO DEL DOCUMENTO NEL DB --- idDocumento/Document Title
			final int idDocumento = documentoDAO.insert(idAoo, docInput.getTipologiaDocumento().getIdTipologiaDocumento(), 0, con);
			// Impostazione del Document Title nel documento in input
			docInput.setDocumentTitle(String.valueOf(idDocumento));
			LOGGER.info("inserisciDocIgepa -> ID del documento: " + idDocumento);

			if (idDocumento > 0) {
				final Map<String, Object> metadatiDocumento = new HashMap<>();

				// ### CONTENT (DOCUMENTO PRINCIPALE)
				impostaContentDocumento(status, docInput);

				// ### ALLEGATI (solo se NON è un OPF)
				if (!isOPF) {
					impostaAllegatiDocumento(status, docInput.getAllegati(), utente.getIdAoo(), con);
				}

				// ### METADATI DOCUMENTO
				if (isOPF) {
					caricaMetadatiDocOPFIgepa(status, docInput, metadatiDocumento, utente);
				} else {
					caricaMetadatiDocIgepa(status, docInput, metadatiDocumento, utente);
				}
				LOGGER.info("inserisciDocIgepa -> Caricamento dei metadati completato per il documento: " + idDocumento);

				// ### SECURITY
				final boolean isNodoUcp = nodoDAO.isNodoUcp(utente.getIdUfficio(), con);
				impostaSecurityDocumento(status, docInput, idDocumento, utente, isNodoUcp, con);

				// ### SALVATAGGIO DEL DOCUMENTO SU FILENET
				popolaESalvaDocumentoFilenet(status, idDocumento, docInput, null, metadatiDocumento, idAoo, fceh, con);

				// ### FASCICOLAZIONE -> START
				String idFascicolo = null;

				if (isOPF) {
					// ### Si crea il fascicolo automatico
					inserisciFascicoloAutomaticoCompetenza(status, docInput, utente, fceh, con);

					if (!CollectionUtils.isEmpty(status.getAssegnazioniFascicolo())) {
						idFascicolo = status.getAssegnazioniFascicolo().get(0).getFascicolo().getIdFascicolo();

						// Si fascicola il nuovo documento documento anche su FileNet
						fascicoloSRV.fascicolaSuFilenet(idFascicolo, idDocumento, idAoo, fceh);

						// ### Si faldona il fascicolo nell'apposito faldone degli Ordini di Pagamento
						// Fondi (OPF) IGEPA
						final String oggettoFaldoneOPFIgepa = "NUM. CONTO "
								+ metadatiDocumento.get(pp.getParameterByKey(PropertiesNameEnum.PROVVEDIMENTO_NUMERO_CONTO_METAKEY)) + "/"
								+ metadatiDocumento.get(pp.getParameterByKey(PropertiesNameEnum.IGEPA_OPF_SEZIONE_METAKEY));
						final String annoFaldoneOPFIgepa = "ANNO " + metadatiDocumento.get(pp.getParameterByKey(PropertiesNameEnum.IGEPA_OPF_ANNO_RICHIESTA_METAKEY));

						final String nomeFaldoneOPFIgepa = faldoneSRV.getFaldoneOPFIgepa(oggettoFaldoneOPFIgepa, annoFaldoneOPFIgepa, utente);

						faldoneSRV.faldonaFascicolo(nomeFaldoneOPFIgepa, idFascicolo, idAoo, fceh);
						LOGGER.info("inserisciDocIgepa -> Il fascicolo : " + idFascicolo + " è stato faldonato nel faldone degli OPF IGEPA: " + nomeFaldoneOPFIgepa);
					}
				} else {
					final String oggettoFascicolo = "_" + docInput.getMetadatiDinamici().get(pp.getParameterByKey(PropertiesNameEnum.IGEPA_ANNO_METAKEY)) + "_"
							+ docInput.getDescrizioneFascicoloProcedimentale();

					// Si cerca su FileNet un fascicolo con l'oggetto calcolato
					final Document fascicoloFilenet = fceh.getFascicoloByOggetto(oggettoFascicolo, utente.getGruppiAD());

					// Fascicolo esistente su Filenet
					if (fascicoloFilenet != null) {
						LOGGER.info("inserisciDocIgepa -> Fascicolo con oggetto: " + oggettoFascicolo + " recuperato da FileNet.");
						idFascicolo = String.valueOf(TrasformerCE.getMetadato(fascicoloFilenet, PropertiesNameEnum.NOME_FASCICOLO_METAKEY));

						// Si fascicola sia sul DB sia su FileNet il nuovo documento nel fascicolo già
						// esistente
						fascicoloSRV.fascicola(idFascicolo, idDocumento, TipoFascicolo.AUTOMATICO, idAoo, fceh, con);
						// Creazione di un nuovo fascicolo (automatico)
					} else {
						LOGGER.info("inserisciDocIgepa -> Fascicolo con oggetto: " + oggettoFascicolo + " NON trovato su FileNet."
								+ " Si procede alla creazione di un nuovo fascicolo.");
						// ### Si crea il fascicolo automatico
						inserisciFascicoloAutomaticoCompetenza(status, docInput, utente, fceh, con);

						if (!CollectionUtils.isEmpty(status.getAssegnazioniFascicolo())) {
							idFascicolo = status.getAssegnazioniFascicolo().get(0).getFascicolo().getIdFascicolo();

							// Si fascicola il nuovo documento documento anche su FileNet
							fascicoloSRV.fascicolaSuFilenet(idFascicolo, idDocumento, idAoo, fceh);

							// ### Si faldona il nuovo fascicolo automatico nell'apposito faldone IGEPA
							final String nomeFaldoneIgepa = pp.getParameterByKey(PropertiesNameEnum.IGEPA_FALDONE);
							faldoneSRV.faldonaFascicolo(nomeFaldoneIgepa, idFascicolo, idAoo, fceh);
							LOGGER.info("inserisciDocIgepa -> Il fascicolo : " + idFascicolo + " è stato faldonato nel faldone IGEPA: " + nomeFaldoneIgepa);
						}
					}
				}
				// ### FASCICOLAZIONE -> END

				// ### Si aggiorna lo stato lavorazione del documento mettendolo agli atti
				documentoDAO.inserisciStatoDocumentoLavorazione(idDocumento, utente.getIdUfficio(), utente.getId(), StatoLavorazioneEnum.ATTI, con);

				// ### GESTIONE WORKFLOW -> START
				final Map<String, Object> metadatiWorkflow = new HashMap<>();

				if (isOPF) {
					// Si ottiene il nome del workflow
					final String nomeWorkflow = tipoProcedimentoSRV.getWorkflowNameProcessoGenericoEntrata(utente.getIdAoo(), con);

					// Si caricano i metadati del workflow
					caricaMetadatiWorkflowIgepaOPF(status, docInput, idFascicolo, metadatiWorkflow, utente);

					// ### Si avvia il nuovo workflow (è necessario committare la transazione
					// affinché il workflow si avvii correttamente)
					con.commit();
					avviaNuovoWorkflow(status, nomeWorkflow, docInput, utente, metadatiWorkflow, fpeh, con);
					LOGGER.info("inserisciDocIgepa -> È stato avviato il workflow: " + status.getWorkflowList().get(0) + PER_IL_DOCUMENTO + idDocumento);
				} else {
					// Vengono startate due istanze dello stesso workflow per registrare sullo
					// storico la creazione e la messa agli atti del documento.

					// Si ottiene il nome del workflow
					final String nomeWorkflow = pp.getParameterByKey(PropertiesNameEnum.WF_PROCESSO_CHIUSURA_STORICO_WFKEY);

					// Si caricano i metadati del workflow
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY), idDocumento);
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), utente.getIdUfficio());
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId());
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY), utente.getIdUfficio());
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY), utente.getId());
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.EVENT_TYPE_EXP_WF_METAKEY), "1");

					// ### Si avvia il primo workflow
					avviaNuovoWorkflow(status, nomeWorkflow, docInput, utente, metadatiWorkflow, fpeh, con);
					LOGGER.info("inserisciDocIGEPA -> È stato avviato il primo workflow: " + status.getWorkflowList().get(0) + PER_IL_DOCUMENTO + idDocumento);

					// Si rimuovono i metadati ID Nodo Destinatario Destinatario e ID Utente
					// Destinatario
					metadatiWorkflow.remove(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY));
					metadatiWorkflow.remove(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY));
					// Si sovrascrive il metadato "eventTypeExp"
					metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.EVENT_TYPE_EXP_WF_METAKEY), "9");

					// ### Si avvia il secondo workflow
					avviaNuovoWorkflow(status, nomeWorkflow, docInput, utente, metadatiWorkflow, fpeh, con);
					LOGGER.info("inserisciDocIgepa -> È stato avviato il secondo workflow: " + status.getWorkflowList().get(1) + PER_IL_DOCUMENTO + idDocumento);
				}
				// ### GESTIONE WORKFLOW -> END

			}
		} catch (final Exception e) {
			LOGGER.error("inserisciDocIgepa -> Si è verificato un errore durante la creazione del documento.", e);
			rollbackInserimentoDocumento(status, e, "inserisciDocIgepa", utente);
		}

		LOGGER.info("inserisciDocIgepa -> END");
	}

	private void caricaMetadatiDocIgepa(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final Map<String, Object> metadatiDocumento,
			final UtenteDTO utente) {
		// Metadato Document Title
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), docInput.getDocumentTitle());

		// Metadato ID AOO
		if (docInput.getIdAOO() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY), docInput.getIdAOO());
		} else {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY), utente.getIdAoo());
		}

		// Metadato Data Archiviazione
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DATA_ARCHIVIAZIONE_METAKEY), new Date());

		// Metadato Anno Protocollo
		if (docInput.getAnnoProtocollo() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY), docInput.getAnnoProtocollo());
		}

		// Metadato Data Protocollo
		if (docInput.getDataProtocollo() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY), docInput.getDataProtocollo());
		}

		// Metadato Numero Protocollo
		if (docInput.getNumeroProtocollo() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY), docInput.getNumeroProtocollo());
		}

		// Metadato ID Utente Creatore
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.UTENTE_CREATORE_ID_METAKEY), utente.getId() != null ? utente.getId() : 0);

		// Metadato ID Ufficio Creatore
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY), utente.getIdUfficio());

		// Metadato Nome File (se esiste il content)
		if (docInput.getContent() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), docInput.getNomeFile());
		}

		// Metadato IGEPA Anno
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.IGEPA_ANNO_METAKEY),
				docInput.getMetadatiDinamici().get(pp.getParameterByKey(PropertiesNameEnum.IGEPA_ANNO_METAKEY)));

		// Metadato IGEPA Codice Ente IGF
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.IGEPA_CODICE_ENTE_IGF_METAKEY),
				docInput.getMetadatiDinamici().get(pp.getParameterByKey(PropertiesNameEnum.IGEPA_CODICE_ENTE_IGF_METAKEY)));

		// Metadato IGEPA Tipologia Ente
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.IGEPA_TIPOLOGIA_ENTE_METAKEY),
				docInput.getMetadatiDinamici().get(pp.getParameterByKey(PropertiesNameEnum.IGEPA_TIPOLOGIA_ENTE_METAKEY)));

		// Metadato IGEPA Descrizione Ente
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.IGEPA_DESCRIZIONE_ENTE_METAKEY),
				docInput.getMetadatiDinamici().get(pp.getParameterByKey(PropertiesNameEnum.IGEPA_DESCRIZIONE_ENTE_METAKEY)));

		// Metadato ID Applicazione
		if (StringUtils.isNotBlank(pp.getParameterByKey(PropertiesNameEnum.ID_APPLICAZIONE_VALUE_METAKEY))) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_APPLICAZIONE_KEY_METAKEY),
					Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.ID_APPLICAZIONE_VALUE_METAKEY)));
		}

		// Metadato ID Categoria Documento
		if (docInput.getIdCategoriaDocumento() != null && docInput.getIdCategoriaDocumento() > 0) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY), docInput.getIdCategoriaDocumento());
		}

		// Metadato ID Tipologia Documento
		if (docInput.getTipologiaDocumento() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY), docInput.getTipologiaDocumento().getIdTipologiaDocumento());
		}

		// Metadato Mezzo Ricezione
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_MEZZORICEZIONE), 0);

		// Metadato Mittente
		if (docInput.getMittenteContatto() != null) {
			String idContatto = "";

			final IFilenetCEHelper fceh = getIFilenetHelper(utente.getIdAoo().intValue());
			try {
				if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(docInput.getMittenteContatto().getContattoID().toString())) {
					idContatto = docInput.getMittenteContatto().getContattoID().toString();
				}

				if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(idContatto)) {
					idContatto = gestisciSalvataggioMetadatoMittenteNew(docInput.getObjectIdContattoMittOnTheFly(), metadatiDocumento, docInput.getMittenteContatto(),
							utente.getIdAoo(), fceh);
				}
			} catch (final Exception e) {
				LOGGER.error(ERROR_SALVATAGGIO_CONTATTO_MSG, e);
				throw e;
			} finally {
				popSubject(fceh);
			}

			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.MITTENTE_METAKEY),
					getMittenteMetaKey(idContatto, docInput.getMittenteContatto().getAliasContatto()));
		}

		// Metadato ID Tipo Procedimento
		if (docInput.getTipoProcedimento() != null && docInput.getTipoProcedimento().getTipoProcedimentoId() > 0) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY), docInput.getTipoProcedimento().getTipoProcedimentoId());
		}

		// Metadato ID Momento Protocollazione
		if (docInput.getMomentoProtocollazioneEnum() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.MOMENTO_PROTOCOLLAZIONE_ID_METAKEY), docInput.getMomentoProtocollazioneEnum().getId());
		}

		// Metadato Riservato
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY),
				Boolean.TRUE.equals(docInput.getIsRiservato()) ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue());

		// Metadato Oggetto
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY), docInput.getOggetto());

		// Metadato Numero Protocollo Mittente
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_MITTENTE_METAKEY), docInput.getProtocolloMittente());

		// Metadato Anno Protocollo Mittente
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_MITTENTE_METAKEY), docInput.getAnnoProtocolloMittente());

		// Metadato Data Protocollo Mittente
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_MITTENTE_METAKEY), docInput.getDataProtocolloMittente());

		// Metadato Urgente
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.URGENTE_METAKEY),
				Boolean.TRUE.equals(docInput.getIsUrgente()) ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue());

		// Metadato Assegnatari Competenza
		if (status.getAssegnazioniCompetenza().length > 0) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ASSEGNATARI_COMPETENZA_METAKEY), status.getAssegnazioniCompetenza());
		}

		// Metadato Numero Protocollo Risposta
		if (docInput.getNumeroProtocolloRisposta() != null && !docInput.getNumeroProtocolloRisposta().isEmpty()) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_RISPOSTA_STRING_METAKEY), docInput.getNumeroProtocolloRisposta());
		}

		// Metadato Anno Protocollo Risposta
		if (docInput.getAnnoProtocolloRisposta() != null && docInput.getAnnoProtocolloRisposta() > 0) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_RISPOSTA_METAKEY), docInput.getAnnoProtocolloRisposta());
		}

		// Metadato Barcode
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.BARCODE_METAKEY),
				StringUtils.isNotBlank(docInput.getBarcode()) ? docInput.getBarcode() : Constants.EMPTY_STRING);

		// Metadato ID Formato Documento
		if (docInput.getFormatoDocumentoEnum() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_FORMATO_DOCUMENTO), docInput.getFormatoDocumentoEnum().getId());
		}

		// Metadato Numero Raccomandata
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_RACCOMANDATA_METAKEY), docInput.getNumeroRaccomandata());

		// Metadato Note
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NOTE_METAKEY), docInput.getNote());

		// Metadato Data Scadenza
		if (docInput.getDataScadenza() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DATA_SCADENZA_METAKEY), docInput.getDataScadenza());
		}

		// Metadato Registro Riservato
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY),
				Boolean.TRUE.equals(docInput.getRegistroRiservato()) ? BooleanFlagEnum.SI.getIntValue() : BooleanFlagEnum.NO.getIntValue());

	}

	private void inserisciFascicoloAutomaticoCompetenza(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente,
			final IFilenetCEHelper fceh, final Connection con) {
		// Si imposta il flag per il fascicolo automatico
		status.setFascicoloAutomatico(true);

		// Si calcolano le security del nuovo fascicolo automatico
		final boolean isNodoUcp = nodoDAO.isNodoUcp(utente.getIdUfficio(), con);

		final ListSecurityDTO securityFascicoloAutomatico = securitySRV.getSecurityPerDocumento(docInput.getDocumentTitle(), utente,
				status.getAssegnazioniCompetenza(), null, null, null, null, null, null, utente.getIdUfficio(), 0L, false, true, 0, false, null, false, isNodoUcp, null, con);

		// ### Si crea il fascicolo automatico
		// N.B. Esegue anche la fascicolazione del documento sul DB
		aggiungiAssegnazioniFascicoloAutomatico(status, status.getAssegnazioniCompetenza(), Integer.parseInt(docInput.getDocumentTitle()), docInput, utente, null,
				securityFascicoloAutomatico, TipoAssegnazioneEnum.COMPETENZA, fceh, con);
	}

	private void caricaMetadatiDocOPFIgepa(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final Map<String, Object> metadatiDocumento,
			final UtenteDTO utente) {
		// Metadato Document Title
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), docInput.getDocumentTitle());

		// Metadato ID AOO
		if (docInput.getIdAOO() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY), docInput.getIdAOO());
		} else {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY), utente.getIdAoo());
		}

		// Metadato Data Archiviazione
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DATA_ARCHIVIAZIONE_METAKEY), new Date());

		// Metadato Numero Conto OPF
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.PROVVEDIMENTO_NUMERO_CONTO_METAKEY),
				docInput.getMetadatiDinamici().get(pp.getParameterByKey(PropertiesNameEnum.PROVVEDIMENTO_NUMERO_CONTO_METAKEY)));

		// Metadato Anno Richiesta OPF
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.IGEPA_OPF_ANNO_RICHIESTA_METAKEY),
				docInput.getMetadatiDinamici().get(pp.getParameterByKey(PropertiesNameEnum.IGEPA_OPF_ANNO_RICHIESTA_METAKEY)));

		// Metadato Numero Richiesta OPF
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.IGEPA_OPF_NUMERO_RICHIESTA_METAKEY),
				docInput.getMetadatiDinamici().get(pp.getParameterByKey(PropertiesNameEnum.IGEPA_OPF_NUMERO_RICHIESTA_METAKEY)));

		// Metadato Sezione OPF
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.IGEPA_OPF_SEZIONE_METAKEY),
				docInput.getMetadatiDinamici().get(pp.getParameterByKey(PropertiesNameEnum.IGEPA_OPF_SEZIONE_METAKEY)));

		// Metadato Data Protocollo
		if (docInput.getDataProtocollo() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY), docInput.getDataProtocollo());
		}

		// Metadato Numero Protocollo
		if (docInput.getNumeroProtocollo() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY), docInput.getNumeroProtocollo());
		}

		// Metadato Oggetto
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY), docInput.getOggetto());

		// Metadato ID Tipologia Documento
		if (docInput.getTipologiaDocumento() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY), docInput.getTipologiaDocumento().getIdTipologiaDocumento());
		}

		// Metadato ID Tipo Procedimento
		if (docInput.getTipoProcedimento() != null && docInput.getTipoProcedimento().getTipoProcedimentoId() > 0) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY), docInput.getTipoProcedimento().getTipoProcedimentoId());
		}

		// Metadato Anno Protocollo
		if (docInput.getAnnoProtocollo() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY), docInput.getAnnoProtocollo());
		}

		// Metadato Mittente
		if (docInput.getMittenteContatto() != null) {
			String idContatto = "";
			final IFilenetCEHelper fceh = getIFilenetHelper(utente.getIdAoo().intValue());
			try {
				if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(docInput.getMittenteContatto().getContattoID().toString())) {
					idContatto = docInput.getMittenteContatto().getContattoID().toString();
				}
				if (it.ibm.red.business.utils.StringUtils.isNullOrEmpty(idContatto)) {
					idContatto = gestisciSalvataggioMetadatoMittenteNew(docInput.getObjectIdContattoMittOnTheFly(), metadatiDocumento, docInput.getMittenteContatto(),
							utente.getIdAoo(), fceh);
				}
				metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.MITTENTE_METAKEY),
						getMittenteMetaKey(idContatto, docInput.getMittenteContatto().getAliasContatto()));
			} catch (final Exception e) {
				LOGGER.error(ERROR_SALVATAGGIO_CONTATTO_MSG, e);
				throw e;
			} finally {
				popSubject(fceh);
			}
		}

		// Metadato Nome File (se esiste il content)
		if (docInput.getContent() != null) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), docInput.getNomeFile());
		}

		// Metadato Assegnatari Competenza
		if (status.getAssegnazioniCompetenza().length > 0) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ASSEGNATARI_COMPETENZA_METAKEY), status.getAssegnazioniCompetenza());
		}

		// Metadato ID Applicazione
		if (StringUtils.isNotBlank(pp.getParameterByKey(PropertiesNameEnum.ID_APPLICAZIONE_VALUE_METAKEY))) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_APPLICAZIONE_KEY_METAKEY),
					Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.ID_APPLICAZIONE_VALUE_METAKEY)));
		}

		// Metadato Mezzo Ricezione
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_MEZZORICEZIONE), 0);

		// Metadato Registro Riservato
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.REGISTRO_RISERVATO_METAKEY), 0);

		// Metadato Urgente
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.URGENTE_METAKEY), 0);

		// Metadato Flag Corte Conti
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.FLAG_CORTE_CONTI_METAKEY), false);

		// Metadato ID Categoria Documento
		if (docInput.getIdCategoriaDocumento() != null && docInput.getIdCategoriaDocumento() > 0) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY), docInput.getIdCategoriaDocumento());
		}

		// Metadato ID Formato Documento
		if (docInput.getIdCategoriaDocumento() != null && docInput.getIdCategoriaDocumento() > 0) {
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.ID_FORMATO_DOCUMENTO), docInput.getFormatoDocumentoEnum().getId());
		}

		// Metadato ID Utente Creatore
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.UTENTE_CREATORE_ID_METAKEY), utente.getId() != null ? utente.getId() : 0);

		// Metadato ID Ufficio Creatore
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY), utente.getIdUfficio());
	}

	private void caricaMetadatiWorkflowIgepaOPF(final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final String idFascicolo,
			final Map<String, Object> metadatiWorkflow, final UtenteDTO utente) {
		// Metadato ID Documento
		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY), Integer.parseInt(docInput.getDocumentTitle()));

		// Metadato ID Nodo Mittente
		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_MITTENTE_WF_METAKEY), utente.getIdUfficio());

		// Metadato ID Utente Mittente
		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId());

		// Metadato ID Coda Lavorazione
		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_CODA_LAVORAZIONE_FN_METAKEY), 0);

		// Metadato ID Nodo Destinatario
		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY), status.getIdUfficioDestinatarioWorkflow());

		// Metadato ID Tipo Assegnazione
		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY), TipoAssegnazioneEnum.COMPETENZA.getId());

		// Metadato ID Fascicolo
		metadatiWorkflow.put(pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY), idFascicolo);
	}

	private boolean checkEmailSpedizioneModificata(final String emailGuid, final EmailDTO currentEmailSpedizione, final IFilenetCEHelper fceh) throws IOException {
		boolean isEmailSpedizioneModificata = false;

		if (StringUtils.isNotBlank(emailGuid)) {
			final Document oldEmailSpedizione = fceh.getDocumentByGuid(emailGuid);

			String oldDestinatari = null;
			String oldDestinatariCC = null;
			String oldOggetto = null;
			String oldTesto = null;
			String oldMittente = null;

			if (oldEmailSpedizione != null) {
				oldDestinatari = (String) TrasformerCE.getMetadato(oldEmailSpedizione, PropertiesNameEnum.DESTINATARI_EMAIL_METAKEY);
				oldDestinatariCC = (String) TrasformerCE.getMetadato(oldEmailSpedizione, PropertiesNameEnum.DESTINATARI_CC_EMAIL_METAKEY);
				oldOggetto = (String) TrasformerCE.getMetadato(oldEmailSpedizione, PropertiesNameEnum.OGGETTO_EMAIL_METAKEY);
				oldMittente = (String) TrasformerCE.getMetadato(oldEmailSpedizione, PropertiesNameEnum.MITTENTE_MAIL_METAKEY);
				oldTesto = IOUtils.toString(FilenetCEHelper.getDocumentContentAsInputStream(oldEmailSpedizione));
			}

			// Si controllano i dati dell'e-mail
			if ((currentEmailSpedizione == null && oldEmailSpedizione != null) || (currentEmailSpedizione != null && oldEmailSpedizione == null)
					|| (currentEmailSpedizione != null
							&& (!currentEmailSpedizione.getOggetto().equals(oldOggetto) || !currentEmailSpedizione.getDestinatari().equals(oldDestinatari)
									|| ((currentEmailSpedizione.getDestinatariCC() != null && !currentEmailSpedizione.getDestinatariCC().equals(oldDestinatariCC))
											|| (currentEmailSpedizione.getDestinatariCC() == null && oldDestinatariCC != null))
									|| !currentEmailSpedizione.getTesto().equals(oldTesto) || !currentEmailSpedizione.getMittente().equals(oldMittente)))) {
				isEmailSpedizioneModificata = true;
				// Si controlla la lista dei documenti allegati
			} else if (currentEmailSpedizione != null) {
				final List<String> currentEmailSpedizioneAllegati = currentEmailSpedizione.getIdAllegati();
				List<String> oldEmailSpedizioneAllegati = null;

				// Si recuperano i Document Title degli allegati dell'e-mail precedente
				final DocumentSet oldEmailSpedizioneAllegatiDoc = fceh.getAllegati(it.ibm.red.business.utils.StringUtils.cleanGuidToString(oldEmailSpedizione.get_Id()), false,
						false);

				if (oldEmailSpedizioneAllegatiDoc != null && !oldEmailSpedizioneAllegatiDoc.isEmpty()) {
					oldEmailSpedizioneAllegati = new ArrayList<>();
					final Iterator<?> itOldEmailSpedizioneAllegatiDoc = oldEmailSpedizioneAllegatiDoc.iterator();
					while (itOldEmailSpedizioneAllegatiDoc.hasNext()) {
						oldEmailSpedizioneAllegati
								.add((String) TrasformerCE.getMetadato((Document) itOldEmailSpedizioneAllegatiDoc.next(), PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
					}
				}

				if ((currentEmailSpedizioneAllegati != null && oldEmailSpedizioneAllegati == null)
						|| (currentEmailSpedizioneAllegati == null && oldEmailSpedizioneAllegati != null)
						|| (currentEmailSpedizioneAllegati != null && currentEmailSpedizioneAllegati.size() != oldEmailSpedizioneAllegati.size())) {
					isEmailSpedizioneModificata = true;
				} else if (currentEmailSpedizioneAllegati != null) {
					final List<String> currentMailSpedizioneAllegatiSorted = new ArrayList<>(currentEmailSpedizioneAllegati);
					final List<String> oldMailSpedizioneAllegatiSorted = new ArrayList<>(oldEmailSpedizioneAllegati);

					Collections.sort(currentMailSpedizioneAllegatiSorted);
					Collections.sort(oldMailSpedizioneAllegatiSorted);

					if (!currentMailSpedizioneAllegatiSorted.equals(oldMailSpedizioneAllegatiSorted)) {
						isEmailSpedizioneModificata = true;
					}
				}
			}
		} else if (currentEmailSpedizione != null) {
			// Se l'e-mail è presente nel documento in input, ma l'annotation su FileNet non
			// è presente,
			// l'e-mail si considera modificata rispetto alla versione precedente
			isEmailSpedizioneModificata = true;
		}

		LOGGER.info("checkEmailSpedizioneModificata " + FRECCIA + isEmailSpedizioneModificata);
		return isEmailSpedizioneModificata;
	}

	/**
	 * Aggiorna i metadati del protocollo del documento nel CE. N.B. Questo metodo
	 * viene usato nel caso in cui il documento in input che si sta salvando sia già
	 * stato protocollato "esternamente" a RED Evo.
	 * 
	 * @param idDocumento
	 * @param docInput
	 * @param status
	 * @param utente
	 * @param fceh
	 * @param con
	 */
	private void aggiornaMetadatiProtocolloCE(final int idDocumento, final SalvaDocumentoRedDTO docInput, final SalvaDocumentoRedStatusDTO status, final UtenteDTO utente,
			final IFilenetCEHelper fceh, final Connection con) {
		// Si verifica che non esista già un altro documento con lo stesso protocollo
		// (numero e anno) in input,
		// per la categoria (entrata/uscita) del documento che si sta creando
		final Document docConStessoProtocollo = fceh.verificaProtocollo(
				Arrays.asList(getClasseDocumentaleDoc(status, docInput.getTipologiaDocumento().getIdTipologiaDocumento(), con)), docInput.getAnnoProtocollo(),
				docInput.getNumeroProtocollo(), docInput.getIdProtocollo(), status.isDocEntrata() ? TipoProtocolloEnum.ENTRATA.getId() : TipoProtocolloEnum.USCITA.getId());

		// Se non esiste, si procede con l'aggiornamento
		if (docConStessoProtocollo == null) {
			status.setNumeroProtocollo(docInput.getNumeroProtocollo());
			status.setAnnoProtocollo(docInput.getAnnoProtocollo().toString());
			status.setDataProtocollo(DateUtils.formatDataByUSLocale(docInput.getDataProtocollo().toString(), DateUtils.DD_MM_YYYY_HH_MM_SS));

			// Se il protocollo è stato staccato da NPS, si compilano le informazioni
			// aggiuntive proprie di NPS
			String idProtocollo = null;
			String oggettoProtocolloNps = null;
			String descrizioneTipologiaDocumentoNps = null;
			if (TipologiaProtocollazioneEnum.isProtocolloNPS(utente.getIdTipoProtocollo())) {
				idProtocollo = docInput.getIdProtocollo();
				oggettoProtocolloNps = docInput.getOggetto();
				descrizioneTipologiaDocumentoNps = docInput.getTipologiaDocumento().getDescrizione();
				status.setIdProtocollo(idProtocollo);
			}

			fceh.aggiornaDocumentoProtocollo(utente.getId(), utente.getIdUfficio(), utente.getIdAoo(),
					status.isDocEntrata() ? Constants.Protocollo.TIPO_PROTOCOLLO_ENTRATA : Constants.Protocollo.TIPO_PROTOCOLLO_USCITA, status.getNumeroProtocollo(),
					idProtocollo, oggettoProtocolloNps, descrizioneTipologiaDocumentoNps, status.getAnnoProtocollo(), status.getDataProtocollo(),
					status.getDocDaSalvare().getDocumentTitle(), utente.getIdTipoProtocollo());

			LOGGER.info("aggiornaMetadatiProtocolloCE -> Aggiornamento dei dati del protocollo nel CE eseguito correttamente per il documento: " + idDocumento);
		} else {
			LOGGER.error("aggiornaMetadatiProtocolloCE -> Esiste già un altro documento con lo stesso protocollo del documento che si sta creando." + " ID protocollo: "
					+ docInput.getIdProtocollo() + ". Numero protocollo: " + docInput.getNumeroProtocollo() + ", anno protocollo: " + docInput.getAnnoProtocollo());
			throw new RedException("Esiste già un altro documento con lo stesso protocollo del documento che si sta creando");
		}
	}

	private String costruisciDescrizioneFascicolo(final SalvaDocumentoRedDTO docInput) {
		String descrizioneFascicolo = null;

		if (StringUtils.isNotBlank(docInput.getOggetto())) {
			descrizioneFascicolo = docInput.getTipologiaDocumento().getDescrizione() + " - " + docInput.getOggetto();
		}

		return descrizioneFascicolo;
	}

	private static boolean hasDocIterAutomatico(final SalvaDocumentoRedStatusDTO status) {
		return status.getIterApprovativo() != null && !status.isIterManuale();
	}

	private static boolean hasDocAssegnazioniIterManuale(final SalvaDocumentoRedStatusDTO status) {
		return (status.getAssegnazioniCompetenza() != null && status.getAssegnazioniCompetenza().length > 0)
				|| (status.getAssegnazioniConoscenza() != null && status.getAssegnazioniConoscenza().length > 0)
				|| (status.getAssegnazioniContributo() != null && status.getAssegnazioniContributo().length > 0)
				|| (status.getAssegnazioniFirma() != null && status.getAssegnazioniFirma().length > 0)
				|| (status.getAssegnazioniSigla() != null && status.getAssegnazioniSigla().length > 0)
				|| (status.getAssegnazioniVisto() != null && status.getAssegnazioniVisto().length > 0)
				|| (status.getAssegnazioniFirmaMultipla() != null && status.getAssegnazioniFirmaMultipla().length > 0);
	}

	private static boolean isDocInputGiaProtocollato(final SalvaDocumentoRedDTO docInput) {
		return (docInput.getNumeroProtocollo() != null && docInput.getAnnoProtocollo() != null && docInput.getDataProtocollo() != null);
	}

	private void salvaMetadatiEstesi(final Integer idDocumento, final SalvaDocumentoRedDTO docInput, final boolean isModalitaInsert, 
			final Long idAoo, final Connection con, final IFilenetCEHelper fceh) {
		Date dataCreazione = null;
		if(isModalitaInsert) {
			dataCreazione = new Date();
		}
		salvaMetadatiEstesi(idDocumento, docInput.getMetadatiEstesi(), idAoo, Long.valueOf(docInput.getTipologiaDocumento().getIdTipologiaDocumento()),
				docInput.getTipoProcedimento().getTipoProcedimentoId(), dataCreazione, isModalitaInsert, con, fceh);
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISalvaDocumentoFacadeSRV#verificaFirmaIterSpedizione(java.lang.String,
	 *      byte[], it.ibm.red.business.dto.UtenteDTO, boolean, java.lang.String).
	 */
	@Override
	public boolean verificaFirmaIterSpedizione(final String nomeFile, final byte[] contentFile, final UtenteDTO utente, final boolean destCartacei,
			final String signerTimbro) {
		return verificaFirmaIterSpedizione(nomeFile, contentFile, utente, destCartacei, signerTimbro, null);
	}

	/**
	 * @see it.ibm.red.business.service.facade.ISalvaDocumentoFacadeSRV#verificaFirmaIterSpedizione(java.lang.String,
	 *      byte[], it.ibm.red.business.dto.UtenteDTO, boolean, java.lang.String,
	 *      com.google.common.net.MediaType).
	 */
	@Override
	public boolean verificaFirmaIterSpedizione(final String nomeFile, final byte[] contentFile, final UtenteDTO utente, final boolean destCartacei, final String signerTimbro,
			final MediaType mediaType) {
		boolean isValido = true;

		boolean isSignedPdf = false;
		boolean isSignedP7m = false;

		if (MediaType.PDF.equals(mediaType)) {
			isSignedPdf = true;
		} else {
			final String[] extSplit = nomeFile.split("\\.");
			final String ext = extSplit[extSplit.length - 1];
			if (extSplit.length == 0) {
				isValido = false;
				return isValido;
			}

			isSignedPdf = "pdf".equalsIgnoreCase(ext);
			isSignedP7m = "p7m".equalsIgnoreCase(ext);
		}
		final ByteArrayInputStream bisContent = new ByteArrayInputStream(contentFile);

		if (!destCartacei && isSignedPdf) {
			isSignedPdf = signSRV.hasSignaturesForSpedizione(bisContent, utente, signerTimbro);
		} else if (!destCartacei && !isSignedP7m) {
			return false;
		} else if (!isSignedP7m) {
			if (isSignedPdf) {
				final IAdobeLCHelper adobeHelper = new AdobeLCHelper();
				isSignedPdf = FileUtils.hasEmptySignFields(bisContent, adobeHelper);
				if (!isSignedPdf) {
					bisContent.reset();
					isSignedPdf = signSRV.hasSignatures(bisContent, utente);
				}
			} else {
				return true;
			}

		}

		if (!destCartacei && isSignedP7m) {
			isSignedP7m = signSRV.hasSignatures(bisContent, utente);
		}

		if (!destCartacei) {
			if (!isSignedPdf && !isSignedP7m) {
				isValido = false;
				return isValido;
			}
		} else {
			if (isSignedPdf || isSignedP7m) {
				isValido = false;
				return isValido;
			}
		}
		return isValido;
	}

	private void salvaMetadatiRegistroAusiliario(final SalvaDocumentoRedDTO docInput, final boolean isModalitaInsert, final Connection con) {
		LOGGER.info("salvaMetadatiRegistroAusiliario -> START");
		if (!isModalitaInsert) {
			templateRegistrazioneSRV.deleteRegistrazioneAusiliaria(docInput.getDocumentTitle(), docInput.getIdAOO().intValue(), con);
			LOGGER.info("salvaMetadatiRegistroAusiliario -> Effettuata l'eliminazione dei metadati del registro ausiliario da sovrascrivere per il documento: "
					+ docInput.getDocumentTitle());
		}

		if (docInput.getIdRegistroAusiliario() != null && !CollectionUtils.isEmpty(docInput.getMetadatiRegistroAusiliario())) {
			templateRegistrazioneSRV.saveRegistrazioneAusiliaria(docInput.getDocumentTitle(), docInput.getIdAOO().intValue(), docInput.getIdRegistroAusiliario(),
					docInput.getMetadatiRegistroAusiliario(), con);
			LOGGER.info(
					"salvaMetadatiRegistroAusiliario -> Effettuato il salvatagaggio dei metadati del registro ausiliario per il documento: " + docInput.getDocumentTitle());
		}
		LOGGER.info("salvaMetadatiRegistroAusiliario -> END");
	}

	private static IFilenetCEHelper getFCEHAdmin(final FilenetCredentialsDTO fcUtente, final Long idAoo) {
		// Connessione al CE con le credenziali dell'amministratore
		return FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(fcUtente), idAoo);
	}

	private static FilenetPEHelper getFPEHAdmin(final FilenetCredentialsDTO fcUtente) {
		// Connessione al PE con le credenziali dell'amministratore
		return new FilenetPEHelper(new FilenetCredentialsDTO(fcUtente));
	}

	/**
	 * Consente di verificare se i metadati estesi hanno subito modifiche in fase di
	 * modifica
	 * 
	 * @param metadati
	 * @param metadatiNps
	 * 
	 */
	private boolean isMetadatiListChanged(final Collection<MetadatoDTO> metadati, final Collection<MetadatoDTO> metadatiNps) {
		for (final MetadatoDTO metadato : metadati) {
			for (final MetadatoDTO metadatoNps : metadatiNps) {
				if (metadato.getName().equals(metadatoNps.getName()) && metadato.getType().equals(metadatoNps.getType())) {

					if (TipoMetadatoEnum.CAPITOLI_SELECTOR.equals(metadatoNps.getType())
							&& ((CapitoloSpesaMetadatoDTO) metadato).getCapitoloSelected() != null
							&& !((CapitoloSpesaMetadatoDTO) metadato).getCapitoloSelected().equals(((CapitoloSpesaMetadatoDTO) metadatoNps).getCapitoloSelected())) {
						return true;
					}

					if (TipoMetadatoEnum.PERSONE_SELECTOR.equals(metadatoNps.getType())
							&& ((AnagraficaDipendentiComponentDTO) metadato).getValue4AttrExt() != null
							&& !((AnagraficaDipendentiComponentDTO) metadato).getValue4AttrExt().equals(((AnagraficaDipendentiComponentDTO) metadatoNps).getValue4AttrExt())) {
						return true;
					}

					if (TipoMetadatoEnum.LOOKUP_TABLE.equals(metadatoNps.getType())
							&& ((LookupTableDTO) metadato).getLookupValueSelected() != null
							&& !((LookupTableDTO) metadato).getLookupValueSelected().equals(((LookupTableDTO) metadatoNps).getLookupValueSelected())) {
						return true;
					}

					if (!StringUtils.isEmpty(metadato.getValue4AttrExt()) && !metadato.getValue4AttrExt().equals(metadatoNps.getValue4AttrExt())) {
						return true;
					}
				}
			}
		}

		return false;
	}

	private static CustomObject salvaContatto(final String objectIdCont, final IFilenetCEHelper fceh, final List<Contatto> contatti) {
		return fceh.salvaContatto(objectIdCont, contatti);
	}

	private IFilenetCEHelper getIFilenetHelper(final Integer idAoo) {
		final Aoo aoo = aooSRV.recuperaAoo(idAoo);
		final AooFilenet aooFilenet = aoo.getAooFilenet();
		return FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
				aooFilenet.getObjectStore(), aoo.getIdAoo());
	}

	private String gestisciSalvataggioMetadatoMittenteNew(final String objectIdContattoOnTheFly, final Map<String, Object> metadatiDocumento, final Contatto mittenteNew,
			final Long idAoo, final IFilenetCEHelper fceh) {
		final List<Contatto> contattiList = new ArrayList<>();
		contattiList.add(rubricaSRV.insertContattoOnTheFly(mittenteNew, idAoo));
		final CustomObject listContatti = salvaContatto(objectIdContattoOnTheFly, fceh, contattiList);
		metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.OBJECTID_CONTATTO_MITT_METADATO), listContatti.get_Id().toString());
		return contattiList.get(0).getContattoID().toString();
	}

	private void cancellaVecchioContDaFilenet(final String objectIdContattoOnTheFly, final IFilenetCEHelper fceh) {
		final IndependentObjectSet contattiDaEliminare = fceh.fetchContatti(objectIdContattoOnTheFly);
		final Iterator<?> iter = contattiDaEliminare.iterator();
		while (iter.hasNext()) {
			final CustomObject object = (CustomObject) iter.next();
			object.delete();
			object.save(RefreshMode.NO_REFRESH);
		}
	}

	private void gestisciModificaMetadatiMinimi(final SalvaDocumentoRedDTO docInput, final UtenteDTO utente, final IFilenetCEHelper fceh, final Connection dwhCon) {
		final Map<String, Object> metadatiDocumento = new HashMap<>();
		try {
			final String idDocumento = docInput.getDocumentTitle();
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.NOTE_METAKEY), docInput.getNote());
			metadatiDocumento.put(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY), docInput.getOggetto());
			fceh.updateMetadatiOnCurrentVersion(utente.getIdAoo(), Integer.parseInt(idDocumento), metadatiDocumento);

			final Aoo aoo = aooSRV.recuperaAoo(utente.getIdAoo().intValue());
			final List<Contatto> listContatti = rubricaSRV.fetchContattiById(docInput.getObjectIdContattoMittOnTheFly(), aoo);
			if (listContatti != null && !listContatti.isEmpty()) {
				final Contatto oldContatto = listContatti.get(0);
				if (!oldContatto.getContattoID().equals(docInput.getMittenteContatto().getContattoID())
						|| (oldContatto.getMailSelected() != null && !oldContatto.getMailSelected().equals(docInput.getMittenteContatto().getMailSelected()))) {
					if (TipologiaIndirizzoEmailEnum.PEC.equals(docInput.getMittenteContatto().getTipologiaEmail())) {
						docInput.getMittenteContatto().setMailPec(docInput.getMittenteContatto().getMailSelected());
					} else {
						docInput.getMittenteContatto().setMail(docInput.getMittenteContatto().getMailSelected());
					}
					gestisciMetadatoMittente(docInput, utente, metadatiDocumento);
					eventoLogSRV.inserisciEventoLog(Integer.parseInt(idDocumento), utente.getIdUfficio(), utente.getId(), 0L, 0L, Calendar.getInstance().getTime(),
							EventTypeEnum.MODIFICA_MITTENTE, "", 0, utente.getIdAoo(), dwhCon);
				}
			}
		} catch (final Exception ex) {
			LOGGER.error("Errore durante gestione modifica metadati minimi : " + ex);
			throw new RedException("Errore durante gestione modifica metadati minimi : ");
		}
	}

	private List<SalvaDocumentoErroreEnum> validaSalvaDocumento(final List<SalvaDocumentoErroreEnum> erroriValidazione, final SalvaDocumentoRedStatusDTO status,
			final SalvaDocumentoRedDTO docInput, final UtenteDTO utente, final IFilenetCEHelper fceh, final FilenetPEHelper fpeh, final Connection con) {
		LOGGER.info("validaSalvaDocumento -> START");
		final List<AssegnazioneDTO> assegnazioni = docInput.getAssegnazioni();

		// ### Validazione degli attributi che sono sempre obbligatori -> START
		// Tipologia Documento
		if (docInput.getTipologiaDocumento() == null || docInput.getTipologiaDocumento().getIdTipologiaDocumento() <= 0) {
			erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_TIPOLOGIA_DOC_NON_PRESENTE);
		}

		// Tipo Procedimento
		if (docInput.getTipoProcedimento() == null || docInput.getTipoProcedimento().getTipoProcedimentoId() <= 0) {
			erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_TIPO_PROCEDIMENTO_NON_PRESENTE);
		}
		// Oggetto
		if (StringUtils.isBlank(docInput.getOggetto())) {
			erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_OGGETTO_NON_PRESENTE);
		}

		// Si termina immediatamente la validazione
		if (!erroriValidazione.isEmpty()) {
			return erroriValidazione;
		}
		// Validazione degli attributi sempre obbligatori -> END

		// ### Validazione per la protocollazione manuale di mail in ingresso
		if (status.isModalitaMailEntrata()) {
			handleProtocollatore(erroriValidazione, status, utente, fceh);

			// Si termina immediatamente la validazione
			if (!erroriValidazione.isEmpty()) {
				return erroriValidazione;
			}
		}

		// ### Si controlla se è una risposta pubblica ad un allaccio riservato
		if (!status.isForzaRispostaPubblica() && !CollectionUtils.isEmpty(docInput.getAllacci()) && Boolean.FALSE.equals(docInput.getIsRiservato())) {
			for (final RispostaAllaccioDTO allaccio : docInput.getAllacci()) {
				if (allaccio.isRiservato()) {
					erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_PUBBLICO_ALLACCIO_RISERVATO);
					// Si termina immediatamente la validazione
					return erroriValidazione;
				}
			}
		}

		// ### Validazione per Tipologia Documento "Dichiarazione Servizi Resi"
		if (Varie.TIPOLOGIA_DOC_DICHIARAZIONE_SERVIZI_RESI.equalsIgnoreCase(docInput.getTipologiaDocumento().getDescrizione())) {
			erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_TIPOLOGIA_DOC_DSR);
			// Si termina immediatamente la validazione
			return erroriValidazione;
		}

		// ### Validazione per l'attributo ID Raccolta FAD
		if (!status.isForzaAttoDecretoManuale()) {
			handleDecreto(erroriValidazione, docInput);

			// Si termina immediatamente la validazione
			if (!erroriValidazione.isEmpty()) {
				return erroriValidazione;
			}
		}

		if (!status.isDocEntrata() && !status.isDocInterno() && !status.isDocAssegnazioneInterna()) {
			final List<DestinatarioRedDTO> inputDestinatari = docInput.getDestinatari();

			// ### Validazione dei DESTINATARI
			final DestinatarioRedValidator destinatarioRedValidator = new DestinatarioRedValidator();
			erroriValidazione.addAll(destinatarioRedValidator.validaTutti(inputDestinatari));

			// ### Se il Tipo Assegnazione è "SPEDIZIONE", si devono gestire i casi in cui
			// sia rispettivamente presente
			// almeno un destinatario interno oppure uno elettronico
			if (TipoAssegnazioneEnum.SPEDIZIONE.equals(docInput.getTipoAssegnazioneEnum())) {
				handleSpedizione(erroriValidazione, status, docInput, utente, inputDestinatari);

				// Si termina immediatamente la validazione
				if (!erroriValidazione.isEmpty()) {
					return erroriValidazione;
				}
			}

			Integer idIterApprovativo = null;
			if (status.isIterManuale()) {
				idIterApprovativo = IterApprovativoDTO.ITER_FIRMA_MANUALE;
			} else if (status.getIterApprovativo() != null) {
				idIterApprovativo = status.getIterApprovativo().getIdIterApprovativo();
			}

			// ### Se i dati predefiniti mozione sono valorizzati, deve essere presente
			// almeno un destinatario esterno con mezzo di spedizione "ELETTRONICO"
			if (idIterApprovativo != null && !status.isSpedisciMailDestElettronici() && utente.getIdUfficio() != null && utente.getIdUfficio() > 0) {
				// Si controlla se esiste una email predefinita per i dati inseriti
				final DatiPredefinitiMozione datiPredefinitiMozione = datiPredefinitiMozioneDAO.getDatiPredefinitiMozione(
						docInput.getTipologiaDocumento().getIdTipologiaDocumento(), docInput.getTipoProcedimento().getTipoProcedimentoId(), utente.getIdUfficio(),
						idIterApprovativo, con);

				if (datiPredefinitiMozione != null) {
					erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_DATI_PREDEFINITI_MOZIONE_DESTINATARIO_ELETTRONICO);
					// Si termina immediatamente la validazione
					return erroriValidazione;
				}
			}

			// ### Validazione per Flusso RGS (iter Firma Ragionere Generale dello Stato): i
			// mezzi di spedizione dei destinatari esterni devono essere omogenei
			if (IterApprovativoDTO.ITER_FIRMA_RAGIONIERE.intValue() == idIterApprovativo.intValue()) {
				final boolean mezziSpedOmogeneiDestinatariEsterni = DestinatariRedUtils.checkMezziSpedOmogeneiDestinatariEsterni(docInput.getDestinatari());
				if (!mezziSpedOmogeneiDestinatariEsterni) {
					erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_FLUSSO_RGS_DESTINATARI_MISTI);
					// Si termina immediatamente la validazione
					return erroriValidazione;
				}
			}

			// ### Se il flag "Firma Digitale RGS" è selezionato, i destinatari esterni
			// devono avere mezzo di spedizione "ELETTRONICO"
			if (Boolean.TRUE.equals(docInput.getCheckFirmaDigitaleRGS())) {
				final boolean allDestinatariEsterniElettronici = DestinatariRedUtils.checkAllMezzoSpedizioneDestinatariElettronico(inputDestinatari);
				if (!allDestinatariEsterniElettronici) {
					erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_CHECK_FIRMA_RGS_DESTINATARI_CARTACEI);
					// Si termina immediatamente la validazione
					return erroriValidazione;
				}
			}

			// ### Se il documento è un Contributo Esterno, i destinatari esterni devono
			// avere mezzo di spedizione "ELETTRONICO"
			if (status.isDocContributoEsterno()) {
				final boolean allDestinatariEsterniElettronici = DestinatariRedUtils.checkAllMezzoSpedizioneDestinatariElettronico(inputDestinatari);
				if (!allDestinatariEsterniElettronici) {
					erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_CONTRIBUTO_ESTERNO_DESTINATARI_CARTACEI);
					// Si termina immediatamente la validazione
					return erroriValidazione;
				}
			}

			// ### Validazione delle APPROVAZIONI
			if (!CollectionUtils.isEmpty(docInput.getApprovazioni())) {
				int numStampigliature = 0;
				for (final ApprovazioneDTO approvazione : docInput.getApprovazioni()) {
					if (approvazione.getStampigliaturaFirma() != 0) {
						numStampigliature++;
					}
				}
				if (numStampigliature > 2) {
					erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_APPROVAZIONI_NUM_STAMPIGLIATURE);
				}
			}

			// ### Se si sta inserendo un nuovo documento non in ingresso, il content deve
			// essere presente
			if (status.isModalitaInsert() && docInput.getContent() == null) {
				erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_CONTENT_NON_PRESENTE);
			}
			// ### Se non si tratta di un documento in ingresso e il Tipo Procedimento è
			// PRELEX, il Numero RDP deve essere presente
			if (Varie.TIPO_PROCEDIMENTO_PRELEX.equalsIgnoreCase(docInput.getTipoProcedimento().getDescrizione()) && docInput.getNumeroRDP() == null) {
				erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_NUMERO_RDP_NON_PRESENTE);
			}

			// ### Validazione per i Registri di Repertorio (RR)
			// Se la tipologia documento specificata è configurata per i RR e si tratta di
			// un'assegnazione per firma, NON devono essere presenti destinatari esterni
			final boolean isRegistroRepertorio = registroRepertorioSRV.checkIsRegistroRepertorio(utente.getIdAoo(),
					docInput.getTipologiaDocumento().getIdTipologiaDocumento().longValue(), docInput.getTipoProcedimento().getTipoProcedimentoId(), null, con);

			if (isRegistroRepertorio // Tipologia documento configurata come Registro Repertorio
					&& ((TipoAssegnazioneEnum.FIRMA.equals(docInput.getTipoAssegnazioneEnum())
							|| TipoAssegnazioneEnum.FIRMA_MULTIPLA.equals(docInput.getTipoAssegnazioneEnum())) || !status.isIterManuale()) // Assegnazione manuale per
																																			// Firma/Firma Multipla oppure Iter
																																			// Automatico
					&& !DestinatariRedUtils.checkDestinatariSoloInterni(inputDestinatari)) { // Almeno un destinatario esterno
				erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_REGISTRO_REPERTORIO_DESTINATARI_ESTERNI);
			}

			// ### Validazione del processo per i documenti in uscita di tipologia "Atto
			// Decreto" UCB
			if (!validaProcessoAttoDecretoUCB(status, docInput, utente)) {
				erroriValidazione.add(SalvaDocumentoErroreEnum.ATTO_DECRETO_ALLEGATI_NON_VALIDI);
			}
		} else {

			handleIngresso(erroriValidazione, status, docInput, fceh, assegnazioni);
		}

		// ### Validazione della DATA SCADENZA
		String wobNumberDocSelezionato = null;
		if (status.isModalitaUpdate()) {
			wobNumberDocSelezionato = docInput.getWobNumber();
		}
		final DataScadenzaDocumentoValidator dataScadenzaDocumentoRedValidator = new DataScadenzaDocumentoValidator(wobNumberDocSelezionato, fpeh);
		erroriValidazione.addAll(dataScadenzaDocumentoRedValidator.valida(docInput.getDataScadenza()));

		// ### Se il formato del documento NON è cartaceo, il nome del file del
		// documento principale deve essere presente
		if (!FormatoDocumentoEnum.CARTACEO.equals(docInput.getFormatoDocumentoEnum()) && StringUtils.isBlank(docInput.getNomeFile())) {
			erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_NOME_FILE_NON_PRESENTE);
		}

		// ### Se si tratta di un documento in ingresso o di un'assegnazione interna, il
		// mittente deve essere presente
		if (status.isDocEntrata() || status.isDocAssegnazioneInterna()) {
			LOGGER.info((docInput.getMittenteContatto() != null ? docInput.getMittenteContatto().toString() : "Mittente vuoto") + " - Tipo documento: "
					+ (status.isDocEntrata() ? "Entrata" : "Assegnazione Interna"));

			if (docInput.getMittenteContatto() == null) {
				erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_MITTENTE_NON_PRESENTE);
			}
			// ### Se non si tratta di un documento in ingresso o di un'assegnazione
			// interna, l'indice di classificazione deve essere presente
		} else if (StringUtils.isBlank(docInput.getIndiceClassificazioneFascicoloProcedimentale())
				|| FilenetStatoFascicoloEnum.NON_CATALOGATO.getNome().equals(docInput.getIndiceClassificazioneFascicoloProcedimentale())) {
			erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_INDICE_CLASSIFICAZIONE_NON_PRESENTE);
		}

		// ### Validazione degli ALLEGATI
		final String extensionFilePrincipale = FilenameUtils.getExtension(docInput.getNomeFile());
		final AllegatoValidator allegatoValidator = new AllegatoValidator(docInput.getResponsabileCopiaConforme(), tipoFileDAO, extensionFilePrincipale, con);
		erroriValidazione.addAll(allegatoValidator.validaTutti(docInput.getAllegati()));
		erroriValidazione.addAll(allegatoValidator.validaTutti(docInput.getAllegatiNonSbustati()));

		// ### Validazione numero protocollo emergenza
		if (TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(utente.getIdTipoProtocollo()) && !status.isPrecensito()
				&& ((status.isDocEntrata() && status.isDaProtocollare())
						|| (!status.isDocEntrata() && MomentoProtocollazioneEnum.PROTOCOLLAZIONE_NON_STANDARD.equals(docInput.getMomentoProtocollazioneEnum())))) {

			if (docInput.getNumeroProtocolloEmergenza() == null || docInput.getNumeroProtocolloEmergenza() == 0) {
				erroriValidazione.add(SalvaDocumentoErroreEnum.ERRORE_PROTOCOLLO_EMERGENZA_MANCANTE);
			} else {
				final boolean isPresent = protocolliEmergenzaSRV.isProtocolloEmergenzaPresent(docInput.getNumeroProtocolloEmergenza(), docInput.getAnnoProtocolloEmergenza(),
						utente.getIdAoo(), con);
				if (isPresent) {
					erroriValidazione.add(SalvaDocumentoErroreEnum.ERRORE_PROTOCOLLO_EMERGENZA_DUPLICATO);
				}
			}
		}

		if (!CollectionUtils.isEmpty(erroriValidazione)) {
			LOGGER.error("validaSalvaDocumento -> Si sono verificati " + erroriValidazione.size() + " errori di validazione durante il salvataggio del documento.");
		}

		LOGGER.info("validaSalvaDocumento -> END");
		return erroriValidazione;
	}

	private void handleDecreto(final List<SalvaDocumentoErroreEnum> erroriValidazione, final SalvaDocumentoRedDTO docInput) {
		// Se il Tipo Procedimento è "Atto Decreto Manuale", ma non è stato creato un
		// codice di raccolta provvisoria
		// tramite il pulsante "Crea Raccolta", si deve avvisare l'utente
		final Integer tipologiaDocAttoDecreto = Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.ATTO_DECRETO_TIPO_DOC_VALUE));
		final Long tipoProcedimentoDocAttoDecretoManuale = Long.parseLong(pp.getParameterByKey(PropertiesNameEnum.ATTO_DECRETO_MANUALE_VALUE));
		if (tipologiaDocAttoDecreto != null && tipologiaDocAttoDecreto.equals(docInput.getTipologiaDocumento().getIdTipologiaDocumento())
				&& tipoProcedimentoDocAttoDecretoManuale.equals(docInput.getTipoProcedimento().getTipoProcedimentoId())) {
			if (StringUtils.isBlank(docInput.getIdRaccoltaFAD())) {
				erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_ID_RACCOLTA_FAD_NON_PRESENTE);
			}
			// Se il Tipo Procedimento non è "Atto Decreto Manuale", ma è presente l'ID
			// Raccolta FAD, si deve avvisare l'utente
		} else if (StringUtils.isNotBlank(docInput.getIdRaccoltaFAD())) {
			erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_ID_RACCOLTA_FAD_PRESENTE);
		}
	}

	private void handleProtocollatore(final List<SalvaDocumentoErroreEnum> erroriValidazione, final SalvaDocumentoRedStatusDTO status, final UtenteDTO utente,
			final IFilenetCEHelper fceh) {
		final String[] metadati = new String[] { pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_PROTOCOLLATORE_METAKEY),
				pp.getParameterByKey(PropertiesNameEnum.STATO_MAIL_METAKEY) };
		final Document mail = fceh.getDocumentByGuid(status.getInputMailGuid(), metadati);
		final Long currentIdUtenteProtocollatore = TrasformerCE.getMetadato(mail, PropertiesNameEnum.ID_UTENTE_PROTOCOLLATORE_METAKEY) != null
				? Long.valueOf((Integer) TrasformerCE.getMetadato(mail, PropertiesNameEnum.ID_UTENTE_PROTOCOLLATORE_METAKEY))
				: null;
		final Integer currentStatoMail = (Integer) TrasformerCE.getMetadato(mail, PropertiesNameEnum.STATO_MAIL_METAKEY);

		if (currentIdUtenteProtocollatore != null && !utente.getId().equals(currentIdUtenteProtocollatore)) {
			if (StatoMailEnum.IN_FASE_DI_PROTOCOLLAZIONE_MANUALE.getStatus().equals(currentStatoMail)) {
				erroriValidazione.add(SalvaDocumentoErroreEnum.MAIL_GIA_IN_PROTOCOLLAZIONE_MANUALE);
			} else if (!status.isForzaProtocollazioneMail()) {
				erroriValidazione.add(SalvaDocumentoErroreEnum.MAIL_GIA_PRESA_IN_CARICO);
			}
		}
	}

	private void handleIngresso(final List<SalvaDocumentoErroreEnum> erroriValidazione, final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput,
			final IFilenetCEHelper fceh, final List<AssegnazioneDTO> assegnazioni) {
		// ### Se il documento e in ingresso e il formato del documento e cartaceo, si
		// controlla il barcode
		if (!status.isModalitaUpdateIbrida() && FormatoDocumentoEnum.CARTACEO.equals(docInput.getFormatoDocumentoEnum())
				&& StringUtils.isNotBlank(docInput.getBarcode())) {
			final boolean barcodeEsistente = !StringUtils.isBlank(fceh.controllaBarcode(docInput.getBarcode()));
			if (barcodeEsistente) {
				LOGGER.error("Il barcode " + docInput.getBarcode() + INSERITO_E_GIA_PRESENTE_NEL_SISTEMA);
				erroriValidazione.add(SalvaDocumentoErroreEnum.BARCODE_NON_VALIDO);
			}
		}

		if (status.isIterManuale()) {
			// ### Se l'iter è manuale, il tipo di assegnazione deve essere presente
			if (docInput.getTipoAssegnazioneEnum() == null) {
				erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_TIPO_ASSEGNAZIONE_NON_PRESENTE);
				// ### Se il tipo di assegnazione è per Copia Conforme, il Responsabile Copia
				// Conforme deve essere presente
			} else if (TipoAssegnazioneEnum.COPIA_CONFORME.equals(docInput.getTipoAssegnazioneEnum()) && docInput.getResponsabileCopiaConforme() == null) {
				erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_RESP_COPIA_CONFORME_NON_PRESENTE);
			}
		}

		// ### Validazione delle ASSEGNAZIONI
		if (!CollectionUtils.isEmpty(assegnazioni)) {
			// ### Se il documento è in ingresso, deve essere presente almeno
			// un'assegnazione per Competenza
			AssegnazioneDTO assegnazioneCompetenza = null;
			for (final AssegnazioneDTO assegnazione : assegnazioni) {
				if (TipoAssegnazioneEnum.COMPETENZA.equals(assegnazione.getTipoAssegnazione())) {
					assegnazioneCompetenza = assegnazione;
					break;
				}
			}
			if (assegnazioneCompetenza == null) {
				erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_ING_ASSEGNATARIO_COMPETENZA_NON_PRESENTE);
			} else {
				
				//in caso di flusso SICOGE e documento riservato (derivante da capitolo di spesa riservato)
				//consentire la creazione del documento solo le l'assegnazione per competenza è a livello utente
				boolean isLivelloUtente = assegnazioneCompetenza.getUtente() != null;
				boolean isFlussoSicoge = TipoContestoProceduraleEnum.SICOGE.equals(TipoContestoProceduraleEnum.getEnumById(docInput.getCodiceFlusso()));  
				boolean isRiservato = docInput.getIsRiservato();
				if(isRiservato && isFlussoSicoge && !isLivelloUtente) {
					erroriValidazione.add(SalvaDocumentoErroreEnum.MANCATA_ASSEGNAZIONE_UTENTE_CAPITOLO_SPESA_RISERVATO);
				}
				
			}

			// ### Se il documento è in ingresso e siamo in fase di creazione, si verifica
			// se ci sono assegnazioni duplicate
			if ((status.isModalitaInsert() || status.isModalitaMailEntrata()) && assegnazioni.size() > 1) {
				boolean assegnazioniDuplicate = false;
				int index = 0;
				int compareIndex = 0;
				for (index = 0; index < assegnazioni.size(); index++) {
					final AssegnazioneDTO assegnazioneDaValidare = assegnazioni.get(index);
					final Integer idUfficioDaValidare = assegnazioneDaValidare.getUfficio() != null ? assegnazioneDaValidare.getUfficio().getId().intValue() : null;
					final Integer idUtenteDaValidare = assegnazioneDaValidare.getUtente() != null ? assegnazioneDaValidare.getUtente().getId().intValue() : null;
					for (compareIndex = (index + 1); compareIndex < assegnazioni.size(); compareIndex++) {
						final AssegnazioneDTO assegnazione = assegnazioni.get(compareIndex);
						final Integer idUfficio = assegnazione.getUfficio() != null ? assegnazione.getUfficio().getId().intValue() : null;
						final Integer idUtente = assegnazione.getUtente() != null ? assegnazione.getUtente().getId().intValue() : null;
						if (assegnazione.getTipoAssegnazione().equals(assegnazioneDaValidare.getTipoAssegnazione())
								&& ((idUfficioDaValidare != null && idUfficioDaValidare.equals(idUfficio)) || (idUfficioDaValidare == null && idUfficio == null))
								&& ((idUtenteDaValidare != null && idUtenteDaValidare.equals(idUtente)) || (idUtenteDaValidare == null && idUtente == null))) {
							erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_ASSEGNAZIONE_DUPLICATA);
							assegnazioniDuplicate = true;
							break;
						}
					}

					if (assegnazioniDuplicate) {
						break;
					}
				}
			}
		} else if (!(status.isDocEntrata() || status.isDocInterno() || status.isDocAssegnazioneInterna() || status.isDocContributoEsterno())
				&& !hasDocAssegnazioniIterManuale(status) && !hasDocIterAutomatico(status)) {
			erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_ASSEGNAZIONI_NON_PRESENTI);
		}
	}

	private void handleSpedizione(final List<SalvaDocumentoErroreEnum> erroriValidazione, final SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput,
			final UtenteDTO utente, final List<DestinatarioRedDTO> inputDestinatari) {
		final Aoo aoo = aooSRV.recuperaAoo(utente.getIdAoo().intValue());
		// Trovato destinatario interno
		if (DestinatariRedUtils.checkOneDestinatarioInterno(inputDestinatari)) {
			erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_ASSEGNAZIONE_SPEDIZIONE_DESTINATARIO_INTERNO);
			// Trovato destinatario elettronico
		} else if (DestinatariRedUtils.checkOneDestinatarioElettronico(inputDestinatari) && aoo.getPkHandlerTimbro() == null) {
			// Se il controllo è forzato, si convertono i destinatari in cartacei
			if (status.isForzaAssSpedizioneDestElettronici()) {
				convertiDestinatariInCartacei(status, docInput);
				// Altrimenti, si mostra un messaggio di conferma dell'operazione per forzarla
			} else {
				erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_ASSEGNAZIONE_SPEDIZIONE_DESTINATARIO_ELETTRONICO);
			}
		} else if (DestinatariRedUtils.checkOneDestinatarioElettronico(inputDestinatari) && aoo.getPkHandlerTimbro() != null) {
			final boolean isValido = verificaFirmaIterSpedizione(docInput.getNomeFile(), docInput.getContent(), utente, false, aoo.getSignerTimbro());
			if (!isValido) {
				erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_NON_FIRMATO_ITER_SPEDIZIONE);
			}
		} else if (!DestinatariRedUtils.checkOneDestinatarioElettronico(inputDestinatari)) {
			final boolean isValido = verificaFirmaIterSpedizione(docInput.getNomeFile(), docInput.getContent(), utente, true, aoo.getSignerTimbro());
			if (!isValido) {
				erroriValidazione.add(SalvaDocumentoErroreEnum.DOC_FIRMATO_ITER_SPEDIZIONE);
			}
		}
	}

	/**
	 * Gestisce la fascicolazione della notifica eventualmente allacciata al documento principale.
	 * @param status status del salvataggio documento per il recupero del fascicolo automatico
	 * @param docInput documento sul cui fascicolo deve essere copiata la notifica
	 * @param utente utente in sessione
	 * @param fceh FilenetCEHelper
	 * @param connection
	 */
	private void fascicolaProtocolloNotifica(SalvaDocumentoRedStatusDTO status, final SalvaDocumentoRedDTO docInput, final UtenteDTO utente, final IFilenetCEHelper fceh, Connection connection) {
		
		if(TipoContestoProceduraleEnum.FLUSSO_AUT.toString().equals(docInput.getCodiceFlusso()) 
				&& !CollectionUtils.isEmpty(docInput.getAllacci()) && TipoAllaccioEnum.NOTIFICA.equals(docInput.getAllacci().get(0).getTipoAllaccioEnum())) {
		
			try {
				String idFascicolo = status.getAssegnazioniFascicolo().get(0).getFascicolo().getIdFascicolo();
				fascicoloSRV.fascicola(idFascicolo, Integer.parseInt(docInput.getAllacci().get(0).getIdDocumentoAllacciato()), TipoFascicolo.NON_AUTOMATICO, utente.getIdAoo(), fceh, connection);
			} catch (Exception ex) {
				LOGGER.error("Errore riscontrato durante la fascicolazione del protocollo notifica.", ex);
				throw new RedException("Errore riscontrato durante la fascicolazione del protocollo notifica.", ex);
			}
		}
	}

	
}