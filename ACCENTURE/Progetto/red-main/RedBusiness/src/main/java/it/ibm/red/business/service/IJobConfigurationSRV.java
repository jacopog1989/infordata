package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IJobConfigurationFacadeSRV;

/**
 * The Interface IJobSRV.
 *
 * @author a.dilegge
 * 
 *         Servizio per la gestione dei job quartz.
 */
public interface IJobConfigurationSRV extends IJobConfigurationFacadeSRV {
	
}
