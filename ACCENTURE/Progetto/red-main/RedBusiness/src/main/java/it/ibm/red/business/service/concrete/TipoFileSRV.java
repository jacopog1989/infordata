package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.ITipoFileDAO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.TipoFile;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.ITipoFileSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * Service che si occupa di gestire la tipologia di un file.
 */
@Service
public class TipoFileSRV extends AbstractService implements ITipoFileSRV {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = 417846461689650339L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TipoFileSRV.class.getName());

	/**
	 * DAO.
	 */
	@Autowired
	private ITipoFileDAO tipoFileDAO;

	/**
	 * @see it.ibm.red.business.service.ITipoFileSRV#getAll().
	 */
	@Override
	public Collection<TipoFile> getAll() {
		Collection<TipoFile> tipoFiles = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			tipoFiles = tipoFileDAO.getAll(connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei tipi di file", e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return tipoFiles;
	}

	/**
	 * @see it.ibm.red.business.service.ITipoFileSRV#isSupportedFileName(java.lang.String,
	 *      java.util.Collection).
	 */
	@Override
	public boolean isSupportedFileName(final String fileName, final Collection<TipoFile> allTipoDati) {
		final Collection<String> extensions = new ArrayList<>();
		for (final TipoFile tf : allTipoDati) {
			extensions.add(tf.getEstensione());
		}
		return FilenameUtils.isExtension(fileName, extensions);
	}

	/**
	 * @see it.ibm.red.business.service.ITipoFileSRV#getTipoFileFromFileName(java.lang.String,
	 *      java.util.Collection).
	 */
	@Override
	public TipoFile getTipoFileFromFileName(final String fileName, final Collection<TipoFile> allTipoDati) {
		for (final TipoFile tf : allTipoDati) {
			if (FilenameUtils.isExtension(fileName.toLowerCase(), tf.getEstensione().toLowerCase())) {
				return tf;
			}
		}
		return null;
	}

	/**
	 * @see it.ibm.red.business.service.ITipoFileSRV#isConvertibile(java.lang.String,
	 *      java.util.Collection).
	 */
	@Override
	public boolean isConvertibile(final String fileName, final Collection<TipoFile> allTipoDati) {
		boolean flagConversione = false;
		if(!StringUtils.isNullOrEmpty(fileName)) {
			final TipoFile tfExtension = getTipoFileFromFileName(fileName, allTipoDati);
	
			if(tfExtension!=null) {
				for (TipoFile tf: allTipoDati) {
					if (tf.getFlagConvertibile().equals(1) && tf.getEstensione().equals(tfExtension.getEstensione())){
						flagConversione = true;
						break;
					}
				}
			}
		}

		return flagConversione;
	}

	/**
	 * @see it.ibm.red.business.service.ITipoFileSRV#normalizzaNomeFile(java.lang.String).
	 */
	@Override
	public final String normalizzaNomeFile(final String filename) {
		String out = filename;

		final String flag = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FLAG_NORMALIZZAZIONE_NOME_FILE);

		if ("S".equalsIgnoreCase(flag)) {
			Connection connection = null;
			try {
				connection = setupConnection(getDataSource().getConnection(), false);
				out = tipoFileDAO.normalizzaNomeFile(connection, filename);
			} catch (final Exception e) {
				LOGGER.error("Errore durante la normalizzazione del nome del file.", e);
				throw new RedException(e);
			} finally {
				closeConnection(connection);
			}
		}

		return out;
	}

	/**
	 * @see it.ibm.red.business.service.ITipoFileSRV#verificaFile(byte[],
	 *      java.lang.String, java.lang.String).
	 */
	@Override
	public void verificaFile(final byte[] content, final String mimeType, final String filename) {
		if (Boolean.FALSE.equals(analizzaFile(content, mimeType, filename))) {
			throw new RedException("Magic number, nome del file e mime type non conformi.");
		}
	}

	/**
	 * @see it.ibm.red.business.service.ITipoFileSRV#analizzaFile(byte[], java.lang.String, java.lang.String).
	 */
	@Override
	public Boolean analizzaFile(final byte[] content, final String mimeType, final String filename) {
		Boolean out = true;
		final String flag = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FLAG_VERIFICA_CONTENT_FILE);

		if ("S".equalsIgnoreCase(flag)) {
			Connection connection = null;
			try {
				connection = setupConnection(getDataSource().getConnection(), false);
				out = tipoFileDAO.analizzaFile(connection, content, mimeType, filename);
			} catch (final Exception e) {
				LOGGER.error("Errore l'analisi del nome del file.", e);
				throw new RedException(e);
			} finally {
				closeConnection(connection);
			}
		}

		return out;
	}
}