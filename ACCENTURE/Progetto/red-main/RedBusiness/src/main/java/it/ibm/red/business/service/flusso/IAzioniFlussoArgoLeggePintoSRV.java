package it.ibm.red.business.service.flusso;

import it.ibm.red.business.service.flusso.facade.IAzioniFlussoArgoLeggePintoFacadeSRV;

/**
 * Interfaccia del servizio che gestisce le azioni flusso ARGO - Legge Pinto.
 */
public interface IAzioniFlussoArgoLeggePintoSRV extends IAzioniFlussoArgoLeggePintoFacadeSRV {

}
