package it.ibm.red.business.persistence.model;

import java.io.Serializable;

/**
 * Model di una coda di lavoro per report.
 */
public class CodaDiLavoroReport implements Serializable {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Identiicativo coda.
	 */
	private int idCoda;
	
	/**
	 * Codice.
	 */
	private String codice;
	
	/**
	 * Descrizione.
	 */
	private String descrizione;
	
	/**
	 * identificativo dettaglio report.
	 */
	private int idDettaglioReport;

	/**
	 * Restituisce l'id della coda.
	 * @return idCoda
	 */
	public int getIdCoda() {
		return idCoda;
	}

	/**
	 * Imposta l'id della coda.
	 * @param idCoda
	 */
	public void setIdCoda(final int idCoda) {
		this.idCoda = idCoda;
	}

	/**
	 * Restituisce il codice.
	 * @return codice
	 */
	public String getCodice() {
		return codice;
	}

	/**
	 * Imposta il codice.
	 * @param codice
	 */
	public void setCodice(final String codice) {
		this.codice = codice;
	}

	/**
	 * Restituisce la descrizione.
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Imposta la descrizione.
	 * @param descrizione
	 */
	public void setDescrizione(final String descrizione) {
		this.descrizione = descrizione;
	}

	/**
	 * Restituisce l'id del dettaglio report.
	 * @return idDettaglioReport
	 */
	public int getIdDettaglioReport() {
		return idDettaglioReport;
	}

	/**
	 * Imposta l'id del dettaglio report.
	 * @param idDettaglioReport
	 */
	public void setIdDettaglioReport(final int idDettaglioReport) {
		this.idDettaglioReport = idDettaglioReport;
	}
}