/**
 * 
 */
package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.ProtocollaMailAooConfDTO;
import it.ibm.red.business.dto.ProtocollaMailCodaDTO;
import it.ibm.red.business.dto.DestinatarioEsternoDTO;
import it.ibm.red.business.dto.TestoPredefinitoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.MailOperazioniEnum;
import it.ibm.red.business.enums.TestoPredefinitoEnum;

/**
 * @author adilegge
 *
 */
public interface IProtocollaMailFacadeSRV extends Serializable {
	
	/**
	 * Ottiene le configurazioni per l'aoo.
	 * @param idAoo
	 * @param operazione
	 * @return ProtocollaMailAooConfDTO
	 */
	ProtocollaMailAooConfDTO getConfAoo(Long idAoo, MailOperazioniEnum operazione);
	
	/**
	 * Prende in carico il primo item in coda per l'aoo.
	 * @param idAoo
	 * @return ProtocollaMailCodaDTO
	 */
	ProtocollaMailCodaDTO getFirstItemToProcess(Long idAoo);
	
	/**
	 * Elabora l'item.
	 * @param item
	 * @param conf
	 */
	void processItem(ProtocollaMailCodaDTO item, ProtocollaMailAooConfDTO conf);
	
	/**
	 * Inserisce la mail in coda.
	 * @param utente
	 * @param operazione
	 * @param guidMail
	 * @param casella
	 * @param idContattoMittente
	 * @param testoTemplate
	 * @param contattiDestinatari
	 */
	void inserisciInCoda(UtenteDTO utente, MailOperazioniEnum operazione, String guidMail, String casella, Long idContattoMittente, 
			String testoTemplate, List<DestinatarioEsternoDTO> contattiDestinatari);
	
	/**
	 * Ottiene i testi predefiniti.
	 * @param utente
	 * @param tipoTesto
	 * @return lista di testi predefiniti
	 */
	List<TestoPredefinitoDTO> getTestiPredefiniti(UtenteDTO utente, TestoPredefinitoEnum tipoTesto);
	
	/**
	 * Controlla l'esistenza di items al processo.
	 * @param guidMail
	 * @param utente
	 * @return true o false
	 */
	boolean existItemsToProcess(String guidMail, UtenteDTO utente);
	
}
