package it.ibm.red.business.dto;

import java.util.Collection;

/**
 * DTO modifica iter.
 * 
 * @author CRISTIANOPierascenzi
 *
 */
public class ModificaIterPEDTO extends AbstractDTO {

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificativo documento.
	 */
	private Integer idDocumento;

	/**
	 * Identificativo fascicolo.
	 */
	private Integer idFascicolo;

	/**
	 * Lista destinatari interni.
	 */
	private Collection<String> destinatariInterni;

	/**
	 * Lista conoscenze.
	 */
	private Collection<String> conoscenze;

	/**
	 * Lista contributi.
	 */
	private Collection<String> contributi;
	
	/**
	 * Identificativo assegnazione.
	 */
	private Integer idTipoAssegnazione;
	

	/**
	 * Costruttore.
	 * 
	 * @param inIdDocumento			identificativo documento
	 * @param inIdFascicolo			identificativo fascicolo
	 * @param inDestinatariInterni	lista destinatari interni
	 * @param inConoscenze			lista conoscenze
	 * @param inContributi			lista contributi
	 */
	public ModificaIterPEDTO(final Integer inIdDocumento, final Integer inIdFascicolo, final Collection<String> inDestinatariInterni,
			final Collection<String> inConoscenze, final Collection<String> inContributi, final Integer inIdTipoAssegnazione) {
		super();
		this.idDocumento = inIdDocumento;
		this.idFascicolo = inIdFascicolo;
		this.destinatariInterni = inDestinatariInterni;
		this.conoscenze = inConoscenze;
		this.contributi = inContributi;
		this.idTipoAssegnazione = inIdTipoAssegnazione;
	}

	/**
	 * Getter id documento.
	 * 
	 * @return	id documento
	 */
	public Integer getIdDocumento() {
		return idDocumento;
	}

	/**
	 * Getter id fascicolo.
	 * 
	 * @return	id fascicolo
	 */
	public Integer getIdFascicolo() {
		return idFascicolo;
	}

	/**
	 * Getter.
	 * 
	 * @return	destinatari interni
	 */
	public Collection<String> getDestinatariInterni() {
		return destinatariInterni;
	}

	/**
	 * Getter.
	 * 
	 * @return	conoscenze
	 */
	public Collection<String> getConoscenze() {
		return conoscenze;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	contributi
	 */
	public Collection<String> getContributi() {
		return contributi;
	}

	/**
	 * Getter.
	 * 
	 * @return the idTipoAssegnazione
	 */
	public Integer getIdTipoAssegnazione() {
		return idTipoAssegnazione;
	}

}
