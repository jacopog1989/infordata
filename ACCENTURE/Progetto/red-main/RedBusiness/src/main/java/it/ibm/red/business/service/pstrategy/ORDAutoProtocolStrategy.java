package it.ibm.red.business.service.pstrategy;

import java.io.IOException;
import java.util.List;

import org.apache.pdfbox.io.IOUtils;

import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.MessaggioPostaNpsFlussoDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO;
import it.ibm.red.business.dto.TipologiaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipoContestoProceduraleEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;

/**
 * Strategy di protocollazione automatica flusso Ordinario.
 */
public class ORDAutoProtocolStrategy extends AbstractAutoProtocolStrategy<MessaggioPostaNpsFlussoDTO> {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ORDAutoProtocolStrategy.class.getName());

	/**
	 * @see it.ibm.red.business.service.pstrategy.IAutoProtocolStrategy#protocol(it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO,
	 *      java.lang.String).
	 */
	@Override
	public EsitoSalvaDocumentoDTO protocol(final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> richiestaElabMessagio, final String guidFilenetMessaggio) {
		return protocol(richiestaElabMessagio, guidFilenetMessaggio, TipoContestoProceduraleEnum.ORD);
	}

	/**
	 * Gestisce la protocollazione.
	 * 
	 * @param richiestaElabMessagio
	 * @param guidFilenetMessaggio
	 * @param tcp
	 * @return Esito protocollazione.
	 */
	protected EsitoSalvaDocumentoDTO protocol(final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> richiestaElabMessagio, final String guidFilenetMessaggio, final TipoContestoProceduraleEnum tcp) {
		LOGGER.info("Avvio protocollazione automatica per " + tcp.getId());
		LOGGER.info("ID AOO: " + richiestaElabMessagio.getIdAoo());

		final ProtocolloNpsDTO protocollo = getDettagliProtocollo(richiestaElabMessagio.getIdAoo().intValue(), richiestaElabMessagio.getMessaggio().getProtocollo());

		byte[] content = null;
		try {
			content = IOUtils.toByteArray(protocollo.getDocumentoPrincipale().getInputStream());
			LOGGER.info("content size: " + (content != null ? content.length : 0));
		} catch (final IOException e) {
			LOGGER.error("Errore in fase di recupero content del documento principale.", e);
			throw new RedException("Errore in fase di recupero content del documento principale.", e);
		}
		final String fileName = protocollo.getDocumentoPrincipale().getNomeFile();
		LOGGER.info("fileName: " + fileName);
		final String mType = protocollo.getDocumentoPrincipale().getContentType();
		LOGGER.info("mType: " + mType);

		final TipologiaDTO tipologia = getTipologiaFromTipologiaNPS(richiestaElabMessagio.getIdAoo(), richiestaElabMessagio.getMessaggio().getDatiFlusso().getMetadatiEstesi());
		LOGGER.info("tipologia: " + tipologia);
		final Integer idTipologiaDocumento = tipologia.getIdTipoDocumento();
		final Integer idTipologiaProcedimento = tipologia.getIdTipoProcedimento();
		final String descTipoDoc = getDescTipoDocFromID(idTipologiaDocumento);
		LOGGER.info("descTipoDoc: " + descTipoDoc);
		final String oggetto = protocollo.getOggetto();
		LOGGER.info("oggetto: " + oggetto);
		final String indiceClassificazioneFascicoloProcedimentale = protocollo.getDescrizioneTitolario();
		final String descrizioneFascicoloProcedimentale = descTipoDoc + " - " + oggetto;
		LOGGER.info("indiceClassificazioneFascicoloProcedimentale: " + indiceClassificazioneFascicoloProcedimentale);
		final String idProcesso = richiestaElabMessagio.getMessaggio().getDatiFlusso().getIdentificatoreProcesso();
		LOGGER.info("idProcesso: " + idProcesso);

		final List<AssegnazioneDTO> assegnazioni = getAssegnazioni(protocollo);
		final UtenteDTO utenteCreatore = getUtenteProtocollatore(protocollo);
		LOGGER.info("utenteCreatore: " + utenteCreatore);
		final String mail = (richiestaElabMessagio.getMessaggio() != null && richiestaElabMessagio.getMessaggio().getMittente() != null) 
				? richiestaElabMessagio.getMessaggio().getMittente().getMail() : null;
		final Contatto contattoMittente = getMittente(protocollo.getMittente(), utenteCreatore, mail);
		LOGGER.info("contattoMittente: " + contattoMittente);
		final Integer idMittenteContatto = contattoMittente.getContattoID().intValue();
		
		final String protocolloRiferimento = null;
		final FascicoloDTO fascicoloSelezionato = null;
				
		final EsitoSalvaDocumentoDTO esito = creaIngresso(tcp, oggetto, utenteCreatore, idMittenteContatto, idTipologiaDocumento, 
				idTipologiaProcedimento, assegnazioni, protocolloRiferimento, content, fileName, 
				mType, fascicoloSelezionato, idProcesso, guidFilenetMessaggio, indiceClassificazioneFascicoloProcedimentale, 
				descrizioneFascicoloProcedimentale, protocollo, null, richiestaElabMessagio.getMessaggio().getDatiFlusso(), false, false);

		LOGGER.info("Fine protocollazione automatica per " + tcp.getId());
		return esito;
	}
	
}
