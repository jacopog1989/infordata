/**
 * 
 */
package it.ibm.red.business.service.ws;

import it.ibm.red.business.service.ws.facade.IWorkFlowWsFacadeSRV;

/**
 * @author APerquoti
 *
 */
public interface IWorkFlowWsSRV extends IWorkFlowWsFacadeSRV {

}
