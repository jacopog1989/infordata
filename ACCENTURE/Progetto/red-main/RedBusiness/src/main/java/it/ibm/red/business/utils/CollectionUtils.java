package it.ibm.red.business.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Classe di utility per le collection.
 */
public final class CollectionUtils {

	/**
	 * Costruttore vuoto.
	 */
	private CollectionUtils() {
		// Costruttore vuoto.
	}

	/**
	 * Restituisce una collection che comprende tutti gli oggetti di entrambe le collection in ingresso.
	 * @param list1
	 * @param list2
	 * @return Collection creata dall'union delle due collection
	 */
	public static <T> Collection<T> union(final Collection<T> list1, final Collection<T> list2) {
        Set<T> set = new HashSet<>();

        set.addAll(list1);
        set.addAll(list2);

        return new ArrayList<>(set);
    }

	/**
	 * Restituisce una collection composta dagli elementi in comune alle due collection in ingresso.
	 * @param list1
	 * @param list2
	 * @return Collection intersection delle due collection in ingresso
	 */
    public static <T> Collection<T> intersection(final Collection<T> list1, final Collection<T> list2) {
        List<T> list = new ArrayList<>();

        for (T t : list1) {
            if (list2.contains(t)) {
                list.add(t);
            }
        }

        return list;
    }
    
    /**
     * @param <T>
     * @param list
     * @return true se la lista è vuota o null
     */
    public static <T> boolean isEmptyOrNull(final Collection<T> list) {
    	return list == null || list.isEmpty();
    }

}
