package it.ibm.red.business.helper.filenet.pe.trasform.impl;

import java.util.Date;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dao.impl.TipoCalendario;
import it.ibm.red.business.dto.EventoDTO;
import it.ibm.red.business.enums.EventoCalendarioEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;

/**
 * Trasformer WF ad Evento.
 */
public class FromWFToEvento extends TrasformerPE<EventoDTO> {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1705454340138423161L;

	/**
	 * Label.
	 */
	private static final String SCADENZE = "Scadenze";

	/**
	 * Costruttore.
	 */
	public FromWFToEvento() {
		super(TrasformerPEEnum.FROM_WF_TO_EVENTO);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE#trasform(filenet.vw.api.VWWorkObject).
	 */
	@Override
	public EventoDTO trasform(final VWWorkObject object) {
		
		TipoCalendario tipo = new TipoCalendario();
		tipo.setTipoEnum(EventoCalendarioEnum.SCADENZA);
		Date dataScadenza = (Date) getMetadato(object, PropertiesNameEnum.DATA_SCADENZA_METAKEY);
		Date dataInizio = (Date) getMetadato(object, PropertiesNameEnum.DATA_SCADENZA_METAKEY);
		String documentiTitle = getMetadato(object, PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY).toString();
		
		return new EventoDTO(0, dataInizio, dataScadenza, SCADENZE, EventoCalendarioEnum.SCADENZA, null, null, documentiTitle, null, null, null, null, null, null, null);
	}

}
