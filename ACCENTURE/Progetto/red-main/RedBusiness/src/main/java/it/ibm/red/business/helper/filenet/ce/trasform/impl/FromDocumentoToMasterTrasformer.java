package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.filenet.api.core.Document;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.IContributoDAO;
import it.ibm.red.business.dao.ILookupDAO;
import it.ibm.red.business.dto.MasterDocumentoDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoSpedizioneDocumentoEnum;
import it.ibm.red.business.enums.TrasformazionePDFInErroreEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.utils.DestinatariRedUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class FromDocumentoToMasterTrasformer.
 *
 * @author CPIERASC
 * 
 *         Trasformer documento.
 */
public class FromDocumentoToMasterTrasformer extends TrasformerCE<MasterDocumentoDTO> {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -5272643610748848810L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FromDocumentoToMasterTrasformer.class.getName());

	/**
	 * Posizione tipo destinatari.
	 */
	private static final int TIPO_DEST_POSITION = 3;

	/**
	 * Dimensione massima soggetto.
	 */
	private static final int MAX_ABSTRACT_SIZE = 100;

	/**
	 * Dao lookup.
	 */
	private final ILookupDAO lookupDAO;

	/**
	 * Dao gestione contributi.
	 */
	private final IContributoDAO contributoDAO;

	/**
	 * Costruttore.
	 */
	public FromDocumentoToMasterTrasformer() {
		super(TrasformerCEEnum.FROM_DOCUMENTO_TO_MASTER);
		lookupDAO = ApplicationContextProvider.getApplicationContext().getBean(ILookupDAO.class);
		contributoDAO = ApplicationContextProvider.getApplicationContext().getBean(IContributoDAO.class);
	}

	/**
	 * Trasform.
	 *
	 * @param document
	 *            the document
	 * @param connection
	 *            the connection
	 * @return the master documento DTO
	 */
	@Override
	public final MasterDocumentoDTO trasform(final Document document, final Connection connection) {
		try {
			final String dt = (String) getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
			final Integer numDoc = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
			final String ogg = StringUtils.getTextAbstract((String) getMetadato(document, PropertiesNameEnum.OGGETTO_METAKEY), MAX_ABSTRACT_SIZE);
			final Integer numeroProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
			final Integer annoProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
			final Integer idTipologiaDocumento = (Integer) getMetadato(document, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY);
			String tipologiaDocumento = null;
			if (idTipologiaDocumento != null) {
				tipologiaDocumento = lookupDAO.getDescTipoDocumento(idTipologiaDocumento.longValue(), connection);
			}
			final Integer attachment = document.get_CompoundDocumentState().getValue();
			final Boolean bAttachmentPresent = attachment > 0;
			
			//gestione del metadato trasformazionePDFInErrore
			final Integer trasfPdfErrore = (Integer) getMetadato(document, PropertiesNameEnum.TRASFORMAZIONE_PDF_ERRORE_METAKEY);
			Boolean bTrasfPdfErrore = false;
			Boolean bTrasfPdfWarning = false;
			if (trasfPdfErrore != null) {
				bTrasfPdfErrore = TrasformazionePDFInErroreEnum.getErrorCodes().contains(trasfPdfErrore);
				bTrasfPdfWarning = TrasformazionePDFInErroreEnum.getWarnCodes().contains(trasfPdfErrore);
			}
			
			//gestione del metadato firmaPDF
			Boolean flagFirmaPDF = (Boolean) getMetadato(document, PropertiesNameEnum.FIRMA_PDF_METAKEY);
			if (flagFirmaPDF == null) {
				flagFirmaPDF = true;
			}
			
			final Integer categoriaDocumentoId = (Integer) getMetadato(document, PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY);
			final Date dataCreazione = (Date) getMetadato(document, PropertiesNameEnum.DATA_CREAZIONE_METAKEY);
			final Date dataScadenza = (Date) getMetadato(document, PropertiesNameEnum.DATA_SCADENZA_METAKEY);
			final Integer urgenza = (Integer) getMetadato(document, PropertiesNameEnum.URGENTE_METAKEY);
			final Date dataProtocollo = (Date) getMetadato(document, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY);
			final Integer idCategoriaDocumento = (Integer) getMetadato(document, PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY); 
			final Integer idFormatoDocumento = (Integer) getMetadato(document, PropertiesNameEnum.ID_FORMATO_DOCUMENTO); 
			final Collection<?> inDestinatari = (Collection<?>) getMetadato(document, PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY);
			// Si normalizza il metadato per evitare errori comuni durante lo split delle singole stringhe dei destinatari
			final List<String[]> destinatariDocumento = DestinatariRedUtils.normalizzaMetadatoDestinatariDocumento(inDestinatari);
			final TipoSpedizioneDocumentoEnum tipoSpedizione = getTipoSpedizione(destinatariDocumento);
			final Boolean flagFirmaAutografaRM = (Boolean) getMetadato(document, PropertiesNameEnum.FLAG_FIRMA_AUTOGRAFA_RM_METAKEY);
			final Integer idMomentoProtocollazione = (Integer) getMetadato(document, PropertiesNameEnum.MOMENTO_PROTOCOLLAZIONE_ID_METAKEY);
			final Integer tipoProtocollo = (Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY);
			final Integer riservato = (Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.RISERVATO_METAKEY);
			Boolean flagRiservato = false;
			if (riservato != null && riservato > 0) {
				flagRiservato = true;
			}
			final Long numContributi = contributoDAO.countContributi(dt, connection);
			
			return new MasterDocumentoDTO(dt, numDoc, ogg, tipologiaDocumento, null, numeroProtocollo, annoProtocollo, null, 
					bAttachmentPresent, bTrasfPdfErrore, bTrasfPdfWarning, categoriaDocumentoId, dataScadenza, urgenza, dataCreazione, dataProtocollo, 
					tipoSpedizione, idCategoriaDocumento, idFormatoDocumento, flagFirmaAutografaRM, idMomentoProtocollazione, 
					tipoProtocollo, flagRiservato, numContributi, flagFirmaPDF);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero di un metadato durante il trasfor del master del documento: ", e);
			return null;
		}
	}
	
	/**
	 * Recupero tipo spedizione.
	 * 
	 * @param inDestinatari	collezione destinarati
	 * @return				tipo spedizione
	 */
	public static TipoSpedizioneDocumentoEnum getTipoSpedizione(final List<String[]> inDestinatari) {
		TipoSpedizioneDocumentoEnum output;
		boolean bElettronico = false;
		boolean bCartaceo = false;
		boolean bInterni =  false;
		
		if (inDestinatari != null) {
			for (final String[] destSplit : inDestinatari) {
				Integer idTipoSpedizione;
				
				final String tipoDestinatario = destSplit[TIPO_DEST_POSITION];
				if (Constants.TipoDestinatario.ESTERNO.equalsIgnoreCase(tipoDestinatario)) {
					idTipoSpedizione = Integer.parseInt(destSplit[2]);
					if (Constants.TipoSpedizione.MEZZO_ELETTRONICO.equals(idTipoSpedizione)) {
						bElettronico = true;
					} else {
						bCartaceo = true;
					}
				} else {
					bInterni =  true;
				}
				
				if (bElettronico && bCartaceo) {
					break;
				}
				
			}
		}
		if ((bElettronico && bCartaceo) || (bInterni && (bElettronico || bCartaceo))) {
			output = TipoSpedizioneDocumentoEnum.MISTO;
		} else if (bElettronico) {
			output = TipoSpedizioneDocumentoEnum.ELETTRONICO;
		} else {
			output = TipoSpedizioneDocumentoEnum.CARTACEO;
		}
		return output;
	}
	
}
