package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;

import com.filenet.api.collection.AccessPermissionList;
import com.filenet.api.collection.Integer32List;
import com.filenet.api.collection.StringList;
import com.filenet.api.constants.SecurityPrincipalType;
import com.filenet.api.core.Document;
import com.filenet.api.security.AccessPermission;
import com.filenet.apiimpl.property.PropertyImpl;

import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.FascicoloWsDTO;
import it.ibm.red.business.dto.SecurityWsDTO;
import it.ibm.red.business.dto.SecurityWsDTO.GruppoWs;
import it.ibm.red.business.dto.SecurityWsDTO.UtenteWs;
import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class ContextFromDocumentoToFascicoloWsTrasformer.
 *
 * @author m.crescentini
 * 
 *         Trasformer fascicolo WS.
 */
public class ContextFromDocumentoToFascicoloWsTrasformer extends ContextTrasformerCE<FascicoloWsDTO> {

	private static final long serialVersionUID = -5550530174350382835L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ContextFromDocumentoToFascicoloWsTrasformer.class.getName());

	/**
	 * Nodo dao.
	 */
	private final INodoDAO nodoDAO;

	/**
	 * Utente dao.
	 */
	private final IUtenteDAO utenteDAO;
	
	
	/**
	 * Costruttore.
	 */
	public ContextFromDocumentoToFascicoloWsTrasformer() {
		super(TrasformerCEEnum.FROM_DOCUMENT_TO_FASCICOLO_WS);
		nodoDAO = ApplicationContextProvider.getApplicationContext().getBean(INodoDAO.class);
		utenteDAO = ApplicationContextProvider.getApplicationContext().getBean(IUtenteDAO.class);
	}

	
	/**
	 * Trasform.
	 *
	 * @param document
	 *            the document
	 * @param connection
	 *            the connection
	 * @return the DocumentoWs DTO
	 */
	@SuppressWarnings("unchecked")
	@Override
	public final FascicoloWsDTO trasform(final Document document, final Connection connection, final Map<ContextTrasformerCEEnum, Object> context) {
		FascicoloWsDTO fascicoloWs = null;
		
		try {
			final String idFascicolo = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.NOME_FASCICOLO_METAKEY);
			final String oggetto = (String) TrasformerCE.getMetadato(document, PropertiesNameEnum.OGGETTO_METAKEY);
			String titolario = (String) getMetadato(document, PropertiesNameEnum.TITOLARIO_METAKEY);
			if (!StringUtils.isNullOrEmpty(titolario)) {
				final String[] splitted = titolario.split("_");
				if (splitted.length > 1) {
					titolario = splitted[1];
				}
			}
			
			// Metadati (se richiesti)
			final boolean withMetadati = Boolean.TRUE.equals(context.get(ContextTrasformerCEEnum.RECUPERA_METADATI));
			
			Map<String, Object> metadati = null;
			if (withMetadati) {
				metadati = new HashMap<>();
				final Set<String> classDefinitionPropertyList = (Set<String>) context.get(ContextTrasformerCEEnum.CLASS_DEFINITION_PROPERTIES);
				
				final Iterator<?> itMetadati = document.getProperties().iterator();
				while (itMetadati.hasNext()) {
					final PropertyImpl property = (PropertyImpl) itMetadati.next();
					
					if (classDefinitionPropertyList.contains(property.getKey())) {
						final Object valoreMetadato = property.getObjectValue();
						
						if (valoreMetadato != null) {
							if (valoreMetadato instanceof StringList || valoreMetadato instanceof Integer32List) {
								final List<String> valore = new ArrayList<>();
								
								for (final Object valoreSingolo : ((StringList) valoreMetadato)) {
									valore.add(String.valueOf(valoreSingolo));
								}
								
								if (!CollectionUtils.isEmpty(valore)) {
									metadati.put((String) property.getKey(), valore);
								}
							} else {
								final String valore = String.valueOf(valoreMetadato);
								
								if (!StringUtils.isNullOrEmpty(valore)) {
									metadati.put((String) property.getKey(), valore);
								}
							}
						}
					}
				}
			}
			
			// Security (se richieste)
			final boolean withSecurity = Boolean.TRUE.equals(context.get(ContextTrasformerCEEnum.RECUPERA_SECURITY));
			
			List<SecurityWsDTO> securityList = null;
			if (withSecurity) {
				final AccessPermissionList acl = document.get_Permissions();
				if (!CollectionUtils.isEmpty(acl)) {
					securityList = new ArrayList<>();
					
					final Iterator<?> itAcl = acl.iterator();
					while (itAcl.hasNext()) {
						final SecurityWsDTO security = new SecurityWsDTO();
						
						final AccessPermission perm = (AccessPermission) itAcl.next();
						final String accessPermissionName = perm.get_GranteeName();
						
						if (!"#AUTHENTICATED-USERS".equals(accessPermissionName) && !"#CREATOR-OWNER".equals(accessPermissionName)
								&& !SecurityPrincipalType.UNKNOWN.equals(perm.get_GranteeType())) {
							
							final String ufficioOUtente = perm.get_GranteeName().split("@")[0];
							
							// Gruppo (Ufficio)
							if (SecurityPrincipalType.GROUP.equals(perm.get_GranteeType())) {
								
								final String[] groupPrefixes = PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.GROUP_SECURITY_PREFIX).split("\\|");
								
								// groupPrefixes[0] = "red" (ufficio)
								// groupPrefixes[1] = "VIRred" (corriere)
								final String idGruppo = ufficioOUtente.toLowerCase().replace(groupPrefixes[1].toLowerCase(), "").replace(groupPrefixes[0].toLowerCase(), "");
								
								final Nodo nodo = nodoDAO.getNodo(Long.valueOf(idGruppo), connection);
								
								if (nodo != null) {
									final GruppoWs gruppoSecurity = security.getGruppo();
									gruppoSecurity.setIdUfficio(nodo.getIdNodo().intValue());
									gruppoSecurity.setIdSiap(-1);
									gruppoSecurity.setDescrizione(nodo.getDescrizione());
									if (nodo.getIdNodoPadre() != null) {
										gruppoSecurity.setIdUfficioPadre(nodo.getIdNodoPadre().intValue());
									}
									
									security.setGruppo(gruppoSecurity);
								}
							// Utente	
							} else {
								final Utente utente = utenteDAO.getByUsername(ufficioOUtente, connection);
															
								if (utente != null) {
									final UtenteWs utenteSecurity = security.getUtente();
									utenteSecurity.setIdUtente(utente.getIdUtente().intValue());
									utenteSecurity.setIdSiap(-1);
									utenteSecurity.setUsername(utente.getUsername());
									utenteSecurity.setNome(utente.getNome());
									utenteSecurity.setCognome(utente.getCognome());
									utenteSecurity.setEmail(utente.getEmail());
									utenteSecurity.setDataDisattivazione(utente.getDataDisattivazione());
									
									security.setUtente(utenteSecurity);
								}
							}
							
							securityList.add(security);
						}
					}
				}
			}
			
			fascicoloWs = new FascicoloWsDTO(idFascicolo, oggetto, titolario, document.getClassName(), metadati, securityList);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero di un metadato durante la trasformazione da documento FileNet a DocumentoWsDTO", e);
		}
		
		return fascicoloWs;
	}
	
	
	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.ContextTrasformerCE#trasform(com.filenet.api.core.Document,
	 *      java.util.Map).
	 */
	@Override
	public FascicoloWsDTO trasform(final Document document, final Map<ContextTrasformerCEEnum, Object> context) {
		throw new RedException();
	}
	
}
