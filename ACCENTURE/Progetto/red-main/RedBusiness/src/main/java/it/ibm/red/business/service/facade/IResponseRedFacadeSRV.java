/**
 * 
 */
package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.ResponsesDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * @author APerquoti
 *
 */
public interface IResponseRedFacadeSRV extends Serializable {

	/**
	 * Pulizia delle responses per i masters selezionati.
	 * @param masters
	 * @param u
	 * @param onlyMulti
	 * @return responses
	 */
	Collection<ResponsesDTO> refineResponses(List<MasterDocumentRedDTO> masters, UtenteDTO u, Boolean onlyMulti);
	
	/**
	 * Pulizia delle responses per il master selezionato.
	 * @param m
	 * @param u
	 * @param onlyMulti
	 * @return responses
	 */
	ResponsesDTO refineReponsesSingle(MasterDocumentRedDTO m, UtenteDTO u, Boolean onlyMulti);

	/**
	 * Controlla se si tratta di firma multipla.
	 * @param u
	 * @return true o false
	 */
	boolean hasLibroFirmaMultipla(UtenteDTO u);
}
