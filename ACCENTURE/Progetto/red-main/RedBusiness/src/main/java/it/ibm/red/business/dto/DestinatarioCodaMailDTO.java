package it.ibm.red.business.dto;

/**
 * DTO che definisce un destinatario in coda mail.
 */
public class DestinatarioCodaMailDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 2564041372277603463L;

	/**
	 * Indirizzo mail.
	 */
	private String indirizzoMail;
	
	/**
	 * Identificativo contatto.
	 */
	private Long idContatto;

	/**
	 * Costruttore del DTO, imposta indirizzo Email del contatto e id del contatto.
	 * @param indirizzoMail
	 * @param idContatto
	 */
	public DestinatarioCodaMailDTO(final String indirizzoMail, final Long idContatto) {
		super();
		this.indirizzoMail = indirizzoMail;
		this.idContatto = idContatto;
	}

	/**
	 * Costruttore vuoto.
	 */
	public DestinatarioCodaMailDTO() {
		super();
	}

	/**
	 * Restituisce l'indirizzo Email del contatto.
	 * @return indirizzoMail come String
	 */
	public String getIndirizzoMail() {
		return indirizzoMail;
	}

	/**
	 * Retituisce l'id del contatto.
	 * @return idContatto come Long
	 */
	public Long getIdContatto() {
		return idContatto;
	}

	/**
	 * Imposta l'indirizzo Email associato al contatto.
	 * @param indirizzoMail
	 */
	public void setIndirizzoMail(final String indirizzoMail) {
		this.indirizzoMail = indirizzoMail;
	}

	/**
	 * Imposta l'id del contatto.
	 * @param idContatto
	 */
	public void setIdContatto(final Long idContatto) {
		this.idContatto = idContatto;
	}
}