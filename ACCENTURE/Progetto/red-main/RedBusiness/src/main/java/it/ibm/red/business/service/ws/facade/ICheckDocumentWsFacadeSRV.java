/**
 * 
 */
package it.ibm.red.business.service.ws.facade;

import java.io.Serializable;

import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.webservice.model.documentservice.types.messages.RedCheckInType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedCheckOutType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedUndoCheckOutType;

/**
 * @author APerquoti
 * 
 *  Facade Check service da web service.
 *
 */
public interface ICheckDocumentWsFacadeSRV extends Serializable {

	/**
	 * Esegue il checkout del documento.
	 * @param client
	 * @param request
	 */
	void checkOut(RedWsClient client, RedCheckOutType request);
	
	/**
	 * Esegue la cancellazione del checkout del documento.
	 * @param client
	 * @param request
	 */
	void undoCheckout(RedWsClient client, RedUndoCheckOutType request);
	
	/**
	 * Esegue il checkin del documento.
	 * @param client
	 * @param request
	 */
	void checkIn(RedWsClient client, RedCheckInType request);
	
}
