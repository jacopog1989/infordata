package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.ITestoPredefinitoDAO;
import it.ibm.red.business.dto.TestoPredefinitoDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * @author SLac
 * 
 *         Tabella NSD.TESTOPREDEFINITO
 */
@Repository
public class TestoPredefinitoDAO extends AbstractDAO implements ITestoPredefinitoDAO {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 6189738971186915814L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TestoPredefinitoDAO.class);

	/**
	 * @see it.ibm.red.business.dao.ITestoPredefinitoDAO#getTestiByTipoAndAOO(int,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<TestoPredefinitoDTO> getTestiByTipoAndAOO(final int idTipoTesto, final Long idAOO,
			final Connection connection) {

		PreparedStatement ps = null;
		ResultSet rs = null;
		List<TestoPredefinitoDTO> list = null;
		try {
			String query = "SELECT t.* " 
					+ "  FROM TESTOPREDEFINITO t " 
					+ " WHERE t.TIPOTESTO = ? "
					+ "   and t.idaoo = ? " 
					+ " ORDER BY idtestopredefinito";

			int index = 1;
			ps = connection.prepareStatement(query);
			ps.setInt(index++, idTipoTesto);
			ps.setLong(index++, idAOO);
			rs = ps.executeQuery();

			list = new ArrayList<>();
			while (rs.next()) {
				list.add(new TestoPredefinitoDTO(rs.getLong("IDTESTOPREDEFINITO"), rs.getString("OGGETTO"),
						rs.getString("CORPOTESTO"), rs.getString("DESCRIZIONE"), rs.getLong("IDAOO"),
						rs.getInt("TIPOTESTO")));
			}
		} catch (SQLException e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e.getMessage(), e);
		} finally {
			closeStatement(ps, rs);
		}

		return list;
	}

}
