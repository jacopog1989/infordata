/**
 * 
 */
package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author VINGENITO
 *
 */
public class AllaccioRiferimentoStoricoDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3093619622953446995L;

	/**
	 * Document title uscita.
	 */
	private String docTitleUscita;

	/**
	 * Id documento uscita.
	 */
	private Integer idDocUscita;
	
	/**
	 * Numero protocollo.
	 */
	private String nProtocolloNsd;

	/**
	 * Lista protooclli.
	 */
	private List<Integer> nProtocolloNsdList;

	/**
	 * Identificativo del file principale.
	 */
	private String idFilePrincipale;

	/**
	 * Oggetto.
	 */
	private String oggetto;
	
	/**
	 * Anno protocollo.
	 */
	private Integer annoProtocollo;
	
	/**
	 * Data creazione.
	 */
	private Date dataCreazione;

	/**
	 * Data aggiornamento.
	 */
	private Date dataAggiornamento;
   
	/**
	 * Identificativo fascicolo procediumentale.
	 */
	private String idFascProcedimentale;
	
	/**
	 * Descrizione fascicolo procedimentale.
	 */
	private String descFascProcedimentale;
	
	/**
	 * Identificativo aoo.
	 */
	private Long idAoo;

	/**
	 * Costruttore di default.
	 */
	public AllaccioRiferimentoStoricoDTO() {
		
	}

	/**
	 * Costruttore completo.
	 * @param inDocTitleUscita
	 * @param inNProtocolloNsd
	 * @param inIdFilePrincipale
	 * @param inDataCreazione
	 * @param inDataAggiornamento
	 * @param inIdFascicoloProc
	 * @param inDescFascProc
	 * @param inIdAoo
	 * @param inOggetto
	 * @param inAnnoProtocollo
	 */
	public AllaccioRiferimentoStoricoDTO(final String inDocTitleUscita, final String inNProtocolloNsd, final String inIdFilePrincipale,
			final Date inDataCreazione, final Date inDataAggiornamento, final String inIdFascicoloProc, final String inDescFascProc, final Long inIdAoo,
			final String inOggetto, final Integer inAnnoProtocollo) {
		
		docTitleUscita = inDocTitleUscita;
		nProtocolloNsd = inNProtocolloNsd;
		idFilePrincipale = inIdFilePrincipale;
		dataCreazione = inDataCreazione;
		dataAggiornamento = inDataAggiornamento; 
		idFascProcedimentale = inIdFascicoloProc;
		descFascProcedimentale = inDescFascProc;
		idAoo = inIdAoo;
		oggetto = inOggetto;
		annoProtocollo = inAnnoProtocollo;
	}
	
	/**
	 * @return the docTitleUscita
	 */
	public String getDocTitleUscita() {
		return docTitleUscita;
	}

	/**
	 * @param docTitleUscita the docTitleUscita to set
	 */
	public void setDocTitleUscita(final String docTitleUscita) {
		this.docTitleUscita = docTitleUscita;
	}

	/**
	 * @return the nProtocolloNsd
	 */
	public String getnProtocolloNsd() {
		return nProtocolloNsd;
	}

	/**
	 * @param nProtocolloNsd the nProtocolloNsd to set
	 */
	public void setnProtocolloNsd(final String nProtocolloNsd) {
		this.nProtocolloNsd = nProtocolloNsd;
	}

	/**
	 * @return the idFilePrincipale
	 */
	public String getIdFilePrincipale() {
		return idFilePrincipale;
	}

	/**
	 * @param idFilePrincipale the idFilePrincipale to set
	 */
	public void setIdFilePrincipale(final String idFilePrincipale) {
		this.idFilePrincipale = idFilePrincipale;
	}

	/**
	 * @return the dataCreazione
	 */
	public Date getDataCreazione() {
		return dataCreazione;
	}

	/**
	 * @param dataCreazione the dataCreazione to set
	 */
	public void setDataCreazione(final Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	/**
	 * @return the dataAggiornamento
	 */
	public Date getDataAggiornamento() {
		return dataAggiornamento;
	}

	/**
	 * @param dataAggiornamento the dataAggiornamento to set
	 */
	public void setDataAggiornamento(final Date dataAggiornamento) {
		this.dataAggiornamento = dataAggiornamento;
	}

	/**
	 * Restituisce l'id del fascicolo procedimentale.
	 * @return id fascicolo procedimentale
	 */
	public String getIdFascProcedimentale() {
		return idFascProcedimentale;
	}

	/**
	 * Imposta l'id del fascicolo procedimentale.
	 * @param idFascProcedimentale
	 */
	public void setIdFascProcedimentale(final String idFascProcedimentale) {
		this.idFascProcedimentale = idFascProcedimentale;
	}

	/**
	 * Restituisce la descrizione del fascicolo procedimentale.
	 * @return descrizione fascicolo procedimentale
	 */
	public String getDescFascProcedimentale() {
		return descFascProcedimentale;
	}

	/**
	 * Imposta la descrizione del fascicolo procedimentale.
	 * @param descFascProcedimentale
	 */
	public void setDescFascProcedimentale(final String descFascProcedimentale) {
		this.descFascProcedimentale = descFascProcedimentale;
	}
	/**
	 * @return the idAoo
	 */
	public Long getIdAoo() {
		return idAoo;
	}

	/**
	 * @param idAoo the idAoo to set
	 */
	public void setIdAoo(final Long idAoo) {
		this.idAoo = idAoo;
	}

	/**
	 * @return the nProtocolloNsdList
	 */
	public List<Integer> getNProtocolloNsdList() {
		return nProtocolloNsdList;
	} 
	
	/**
	 * @param nProtocolloNsdList the nProtocolloNsdList to set
	 */
	public void setNProtocolloNsdList(final List<Integer> nProtocolloNsdList) {
		this.nProtocolloNsdList = nProtocolloNsdList;
	}

	/**
	 * Restituisce l'oggetto.
	 * @return oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * Imposta l'oggetto.
	 * @param oggetto
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}

	/**
	 * Restituisce l'anno associato al protocollo.
	 * @return anno protocollo
	 */
	public Integer getAnnoProtocollo() {
		return annoProtocollo;
	}

	/**
	 * Imposta l'anno associato al protocollo.
	 * @param annoProtocollo
	 */
	public void setAnnoProtocollo(final Integer annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}

	/**
	 * Restituisce l'id del documento in uscita.
	 * @return id documento uscita
	 */
	public Integer getIdDocUscita() {
		return idDocUscita;
	}

	/**
	 * Imposta l'id del documento in uscita.
	 * @param idDocUscita
	 */
	public void setIdDocUscita(final Integer idDocUscita) {
		this.idDocUscita = idDocUscita;
	}
}
