package it.ibm.red.business.enums;

/**
 * The Enum SignTypeEnum.
 *
 * @author CPIERASC
 * 
 *         Enum per le tipologie di firma.
 */
public enum SignTypeEnum {	

	/**
	 * Firma cades.
	 */
	CADES,

	/**
	 * Firma pades invisibile.
	 */
	PADES_INVISIBLE,

	/**
	 * Firma pades visibile.
	 */
	PADES_VISIBLE,

	/**
	 * Firma pades posizionale.
	 */
	PADES_POSITONAL;



	/**
	 * The Enum P7M.
	 *
	 * @author CPIERASC
	 * 
	 *         Enum per esplicitare le tipologie di firma ammesse su di un
	 *         contenuto P7M.
	 */
	public enum P7M {

		/**
		 * Firma cades.
		 */
		CADES;
	}

	/**
	 * The Enum PDF.
	 *
	 * @author CPIERASC
	 * 
	 *         Enum per esplicitare le tipologie di firma ammesse su di un
	 *         contenuto PDF.
	 */
	public enum PDF {

		/**
		 * Firma pades invisibile.
		 */
		PADES_INVISIBLE, 

		/**
		 * Firma pades visibile.
		 */
		PADES_VISIBLE, 

		/**
		 * Firma pades posizionale.
		 */
		PADES_POSITONAL;
	}

	/**
	 * Definisce la tipologia di firma.
	 */
	public enum SignTypeGroupEnum {

		/**
		 * Firma PADES.
		 */
		PADES(0),

		/**
		 * Firma CADES.
		 */
		CADES(1),

		/**
		 * Firma Autografa.
		 */
		NON_DIGITALE(2);

		/**
		 * Id tipo firma.
		 */
		private Integer id;

		/**
		 * Costruttore.
		 * @param id
		 */
		private SignTypeGroupEnum(Integer id) {
			this.id = id;
		}

		/**
		 * Restituisce l'id del tipo firma.
		 * @return id tipo firma
		 */
		public Integer getId() {
			return id;
		}

		/**
		 * @param id
		 * @return
		 */
		public static SignTypeGroupEnum get(Integer id) {
			SignTypeGroupEnum out = null;

			for (SignTypeGroupEnum g : SignTypeGroupEnum.values()) {
				if (g.getId().equals(id)) {
					out = g;
					break;
				}
			}

			return out;
		}


		/**
		 * @param signType
		 * @return
		 */
		public static SignTypeGroupEnum fromTypeToGroup(SignTypeEnum signType) {
			SignTypeGroupEnum out = SignTypeGroupEnum.NON_DIGITALE;

			if (signType != null) {
				if (SignTypeEnum.CADES.equals(signType)) {
					out = SignTypeGroupEnum.CADES;
				} else {
					out = SignTypeGroupEnum.PADES;
				}
			}

			return out;
		}
	}
}
