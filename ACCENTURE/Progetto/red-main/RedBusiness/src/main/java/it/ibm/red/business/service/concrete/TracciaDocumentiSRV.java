package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.core.Document;
import com.filenet.api.property.Properties;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.ISottoscrizioniDAO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EventoSottoscrizioneDTO;
import it.ibm.red.business.dto.IdGenericoDTO;
import it.ibm.red.business.dto.ProcedimentoTracciatoDTO;
import it.ibm.red.business.dto.TipoProcedimentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.provider.ServiceTaskProvider.ServiceTask;
import it.ibm.red.business.service.ITipoProcedimentoSRV;
import it.ibm.red.business.service.ITracciaDocumentiSRV;

/**
 * Service che gestisce la traccia documenti.
 */
@Service
@Component
public class TracciaDocumentiSRV extends AbstractService implements ITracciaDocumentiSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 8447337068295228612L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TracciaDocumentiSRV.class.getName());

	/**
	 * DAO.
	 */
	@Autowired
	private ISottoscrizioniDAO sottoscrizioniDAO;

	/**
	 * Servizio.
	 */
	@Autowired
	private ITipoProcedimentoSRV tipoProcedimentoSRV;

	/**
	 * Properties.
	 */
	private PropertiesProvider pp;

	/**
	 * Post construct del service.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * Indica se l'utente in input ha sottoscrizioni con tracciamento singolo.
	 * 
	 * @param account
	 * @return true se account ha sottoscrizioni con tracciamento singolo
	 */
	public boolean hasTracciamentiSingoli(final Long idUfficio, final Long idUtente, final Long idRuolo) {
		Connection con = null;
		List<EventoSottoscrizioneDTO> listaEventiSottoscritti = null;
		try {
			con = getFilenetDataSource().getConnection();
			listaEventiSottoscritti = sottoscrizioniDAO.getSottoscrizioniListByUser(con, idUtente, idUfficio, idRuolo);

			for (final EventoSottoscrizioneDTO sottoscrizione : listaEventiSottoscritti) {
				if (sottoscrizione.getTracciamento() == Constants.Sottoscrizioni.TRACCIAMENTO_SINGOLO) {
					return true;
				}
			}

		} catch (final Exception e) {
			LOGGER.error("SottoscrizioniSRV.hasTracciamentiSingoli: errore generico.", e);
		} finally {
			closeConnection(con);
		}

		return false;
	}

	/**
	 * Indica se l'utente ha già tracciato il documento in input.
	 * 
	 * @param account
	 * @param idDocumento
	 * @return
	 */
	@Override
	public boolean isDocumentoTracciato(final UtenteDTO utente, final int idDocumento) {
		Connection con = null;
		boolean isTracciato = false;
		try {

			con = getFilenetDataSource().getConnection();
			isTracciato = isDocumentoTracciato(utente, idDocumento, con);

		} catch (final Exception e) {
			LOGGER.error("SottoscrizioniSRV.isDocumentoTracciato: errore generico.", e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

		return isTracciato;
	}

	/**
	 * Indica se l'utente ha già tracciato il documento in input.
	 * 
	 * @param utente      Utente
	 * @param idDocumento ID Documento
	 * @param con         Connessione
	 * @return
	 */
	@Override
	public boolean isDocumentoTracciato(final UtenteDTO utente, final int idDocumento, final Connection con) {
		boolean isTracciato = false;

		try {
			isTracciato = sottoscrizioniDAO.isDocTracciato(con, utente.getId(), utente.getIdUfficio(), utente.getIdRuolo(), idDocumento);
		} catch (final Exception e) {
			LOGGER.error("SottoscrizioniSRV.isDocumentoTracciato: errore generico.", e);
			throw new RedException(e);
		}

		return isTracciato;
	}

	@Override
	public final Collection<EsitoOperazioneDTO> tracciaDocumenti(final Long idUfficio, final Long idUtente, final Long idRuolo, final Collection<Integer> idDocumenti) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();

		for (final int idDocumento : idDocumenti) {
			esiti.add(tracciaDocumento(idUfficio, idUtente, idRuolo, idDocumento));
		}

		return esiti;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITracciaDocumentiFacadeSRV#tracciaDocumento(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection).
	 */
	@Override
	@ServiceTask(name = "tracciaProcedimento")
	public Collection<EsitoOperazioneDTO> tracciaDocumento(final UtenteDTO utente, final Collection<String> wobNumber) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();

		for (final String w : wobNumber) {
			esiti.add(tracciaDocumento(utente, w));
		}

		return esiti;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITracciaDocumentiFacadeSRV#tracciaDocumento(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	@ServiceTask(name = "tracciaProcedimento")
	public final EsitoOperazioneDTO tracciaDocumento(final UtenteDTO utente, final String wobNumber) {
		EsitoOperazioneDTO esitoTp = new EsitoOperazioneDTO(wobNumber);

		try {
			final IdGenericoDTO infoDoc = findDocumentTitleAndNumeroDocumento(utente, wobNumber);

			esitoTp = tracciaDocumento(utente.getIdUfficio(), utente.getId(), utente.getIdRuolo(), infoDoc.getDocumentTitle());
			esitoTp.setIdDocumento(infoDoc.getNumeroDocumento());
			esitoTp.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
		} catch (final Exception e) {
			LOGGER.error("SottoscrizioniSRV.tracciaDocumento: impossibile recuperare l'ID del documento a partire dal wobNumber: " + wobNumber, e);
			esitoTp.setEsito(false);
			esitoTp.setNote("Errore durante il tracciamento del documento.");
		}

		return esitoTp;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITracciaDocumentiFacadeSRV#rimuoviDocumento(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.Collection).
	 */
	@Override
	@ServiceTask(name = "noTracciaProcedimento")
	public Collection<EsitoOperazioneDTO> rimuoviDocumento(final UtenteDTO utente, final Collection<String> wobNumber) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();

		for (final String w : wobNumber) {
			esiti.add(rimuoviDocumento(utente, w));
		}

		return esiti;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITracciaDocumentiFacadeSRV#rimuoviDocumento(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	@ServiceTask(name = "noTracciaProcedimento")
	public final EsitoOperazioneDTO rimuoviDocumento(final UtenteDTO utente, final String wobNumber) {
		EsitoOperazioneDTO esitoTp = new EsitoOperazioneDTO(wobNumber);

		try {
			final IdGenericoDTO esito = findDocumentTitleAndNumeroDocumento(utente, wobNumber);

			esitoTp = rimuoviDocumento(utente.getIdUfficio(), utente.getId(), utente.getIdRuolo(), esito.getDocumentTitle());
			esitoTp.setIdDocumento(esito.getNumeroDocumento());
			esitoTp.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
		} catch (final Exception e) {
			LOGGER.error("SottoscrizioniSRV.tracciaDocumento: impossibile recuperare l'ID del documento a partire dal wobNumber: " + wobNumber, e);
			esitoTp.setEsito(false);
			esitoTp.setNote("Errore durante il tracciamento del documento.");
		}

		return esitoTp;
	}

	/**
	 * Ritorna per semplicita l'esitoDocumento che mi serve da passare eventualmente
	 * all'utente se va in errore.
	 * 
	 * Quando va in errore EsitoOperazioneDTO ha numeroDocumento in idDocument
	 * 
	 * Quando non va in errore ha documentTitle in idDocument
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	private IdGenericoDTO findDocumentTitleAndNumeroDocumento(final UtenteDTO utente, final String wobNumber) {
		Integer documentTitleInteger = null;
		final IdGenericoDTO esito = new IdGenericoDTO();
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;

		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			final VWWorkObject workObject = fpeh.getWorkFlowByWob(wobNumber, true);
			documentTitleInteger = (Integer) TrasformerPE.getMetadato(workObject, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
			if (documentTitleInteger == null) {
				throw new RedException("id del documento non valorizzato");
			} else {
				esito.setDocumentTitle(documentTitleInteger);
			}

			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final String[] parametriDocumento = new String[] { pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY) };
			final Document doc = fceh.getDocumentByIdGestionale(documentTitleInteger.toString(), Arrays.asList(parametriDocumento), null, utente.getIdAoo().intValue(), null,
					pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));

			final Integer numeroDocumento = (Integer) TrasformerCE.getMetadato(doc, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);

			esito.setNumeroDocumento(numeroDocumento);
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}

		return esito;
	}

	/**
	 * Associa il documento in input a tutti gli eventi sottoscritti dall'utente in
	 * input.
	 * 
	 * @param account
	 * @param idDocumento
	 * @return true se l'operazione è andata a buona fine, false altrimenti
	 */
	@Override
	public EsitoOperazioneDTO tracciaDocumento(final Long idUfficio, final Long idUtente, final Long idRuolo, final int idDocumento) {
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(null, null, idDocumento, null);
		Connection con = null;

		try {
			con = setupConnection(getFilenetDataSource().getConnection(), true);
			esito = tracciaDocumento(idUfficio, idUtente, idRuolo, idDocumento, con);
			if (!esito.isEsito()) {
				throw new RedException("SottoscrizioniSRV.tracciaDocumento: errore generico.");
			}

			commitConnection(con);
		} catch (final Exception e) {
			rollbackConnection(con);
			LOGGER.error("SottoscrizioniSRV.tracciaDocumento: errore generico.", e);
			esito.setEsito(false);
			esito.setNote("Errore durante il tracciamento del documento. ");
		} finally {
			closeConnection(con);
		}

		return esito;
	}

	/**
	 * Associa il documento in input a tutti gli eventi sottoscritti dall'utente in
	 * input.
	 * 
	 * @param account
	 * @param idDocumento
	 * @return true se l'operazione è andata a buona fine, false altrimenti
	 */
	@Override
	public EsitoOperazioneDTO tracciaDocumento(final Long idUfficio, final Long idUtente, final Long idRuolo, final int idDocumento, final Connection con) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(null, null, idDocumento, null);

		try {
			sottoscrizioniDAO.aggiungiDoc(con, idUtente, idUfficio, idRuolo, idDocumento);
		} catch (final Exception e) {
			LOGGER.error("tracciaDocumento -> Si è verificato un errore nell'attivazione del tracciamento del documento: " + idDocumento, e);
			esito.setEsito(false);
			esito.setNote("Errore durante l'attivazione del tracciamento del documento.");
		}

		return esito;
	}

	@Override
	public final Collection<EsitoOperazioneDTO> rimuoviDocumenti(final Long idUfficio, final Long idUtente, final Long idRuolo, final Collection<Integer> idDocumenti) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();
		for (final int idDocumento : idDocumenti) {
			esiti.add(rimuoviDocumento(idUfficio, idUtente, idRuolo, idDocumento));
		}
		return esiti;
	}

	/**
	 * Elimina l'associazione fra il documento in input e tutti gli eventi
	 * sottoscritti dall'utente in input.
	 * 
	 * @param account
	 * @param idDocumento
	 * @return true se l'operazione è andata a buona fine, false altrimenti
	 */
	@Override
	public EsitoOperazioneDTO rimuoviDocumento(final Long idUfficio, final Long idUtente, final Long idRuolo, final int idDocumento) {
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(null, null, idDocumento, null);
		Connection con = null;
		try {

			con = setupConnection(getFilenetDataSource().getConnection(), true);
			esito = rimuoviDocumento(idUfficio, idUtente, idRuolo, idDocumento, con);
			if (!esito.isEsito()) {
				throw new RedException(esito.getNote());
			}
			commitConnection(con);

		} catch (final Exception e) {
			LOGGER.error(e);
			rollbackConnection(con);
		} finally {
			closeConnection(con);
		}

		return esito;
	}

	/**
	 * Elimina l'associazione fra il documento in input e tutti gli eventi
	 * sottoscritti dall'utente in input.
	 * 
	 * @return true se l'operazione è andata a buona fine, false altrimenti
	 */
	@Override
	public EsitoOperazioneDTO rimuoviDocumento(final Long idUfficio, final Long idUtente, final Long idRuolo, final int idDocumento, final Connection con) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(null, null, idDocumento, null);

		try {
			sottoscrizioniDAO.rimuoviDoc(idUtente, idUfficio, idRuolo, idDocumento, con);
		} catch (final SQLException e) {
			LOGGER.error("rimuoviDocumento -> Si è verificato un errore nell'attivazione del tracciamento del documento: " + idDocumento, e);
			esito.setEsito(false);
			esito.setNote("Errore durante la disattivazione del tracciamento del documento.");
		}

		return esito;
	}

	/**
	 * Restituisce i documenti tracciati dall'utente per l'evento in inpiut.
	 * 
	 * @param account
	 * @param idEvento
	 * @return
	 */
	public List<Integer> getDocumentiTracciatiByEvento(final Long idUfficio, final Long idUtente, final Long idRuolo, final Integer idEvento) {
		Connection con = null;
		List<Integer> idDocList = null;
		try {
			con = getFilenetDataSource().getConnection();
			idDocList = sottoscrizioniDAO.getDocumentiTracciati(con, idUtente, idUfficio, idRuolo, idEvento);

		} catch (final Exception e) {
			LOGGER.error("SottoscrizioniSRV.getDocumentiTracciatiByEvento: errore generico.", e);
		} finally {
			closeConnection(con);
		}

		return idDocList;
	}

	/**
	 * Restituisce i documenti tracciati dall'utente.
	 * 
	 * @param account
	 * @param idEvento
	 * @return
	 */
	public List<Integer> getDocumentiTracciati(final Long idUfficio, final Long idUtente, final Long idRuolo) {
		Connection con = null;
		List<Integer> idDocList = null;
		try {
			con = getFilenetDataSource().getConnection();
			idDocList = sottoscrizioniDAO.getDocumentiTracciati(con, idUtente, idUfficio, idRuolo);
		} catch (final Exception e) {
			LOGGER.error("SottoscrizioniSRV.getDocumentiTracciati: errore generico.", e);
		} finally {
			closeConnection(con);
		}

		return idDocList;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITracciaDocumentiFacadeSRV#hasTracciamentiSingoli(it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public boolean hasTracciamentiSingoli(final UtenteDTO utente) {
		Connection connection = null;
		Collection<EventoSottoscrizioneDTO> listaEventiSottoscritti = null;

		try {
			connection = setupConnection(getFilenetDataSource().getConnection(), false);

			listaEventiSottoscritti = sottoscrizioniDAO.getSottoscrizioniListByUser(connection, utente.getId(), utente.getIdUfficio(), utente.getIdRuolo());

			for (final EventoSottoscrizioneDTO es : listaEventiSottoscritti) {
				if (es.getTracciamento() == Constants.Sottoscrizioni.TRACCIAMENTO_SINGOLO) {
					return true;
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero delle Sottoscrizioni ", e);
			rollbackConnection(connection);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return false;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITracciaDocumentiFacadeSRV#loadProcedimentiTracciati(it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Collection<ProcedimentoTracciatoDTO> loadProcedimentiTracciati(final UtenteDTO utente) {
		final Collection<ProcedimentoTracciatoDTO> listaProcedimentiTracciati = new ArrayList<>();
		IFilenetCEHelper fceh = null;

		try {
			// Si recuperano gli id dei documenti tracciati dall'utente
			final List<Integer> idDocTracciati = getDocumentiTracciati(utente.getIdUfficio(), utente.getId(), utente.getIdRuolo());

			if (idDocTracciati != null && !idDocTracciati.isEmpty()) {
				final StringBuilder whereCondition = new StringBuilder();
				whereCondition.append("d.").append(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY)).append(" IN (");
				for (int i = 0; i < idDocTracciati.size(); i++) {
					whereCondition.append("'").append(idDocTracciati.get(i)).append("'");
					if (i < (idDocTracciati.size() - 1)) {
						whereCondition.append(", ");
					}
				}
				whereCondition.append(")");

				fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

				final DocumentSet docs = fceh.getDocuments(utente.getIdAoo().intValue(), whereCondition.toString(),
						PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), true, false);
				if (docs != null) {
					final Iterator<?> it = docs.iterator();
					while (it.hasNext()) {
						listaProcedimentiTracciati.add(buildProcedimentoTracciatoDTO((Document) it.next()));
					}
				}
			}
		} finally {
			popSubject(fceh);
		}

		return listaProcedimentiTracciati;
	}

	private ProcedimentoTracciatoDTO buildProcedimentoTracciatoDTO(final Document filenetDocument) {
		final ProcedimentoTracciatoDTO procedimentoTracciato = new ProcedimentoTracciatoDTO();
		// Properties del documento Filenet
		final Properties prop = filenetDocument.getProperties();

		// ID documento (DocumentTitle)
		if (prop.isPropertyPresent(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY))) {
			procedimentoTracciato.setIdDocumento(Integer.parseInt(prop.getStringValue(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY))));
		}

		// Numero documento
		if (prop.isPropertyPresent(pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY))) {
			procedimentoTracciato.setNumeroDocumento(prop.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY)).toString());
		}

		// Oggetto
		procedimentoTracciato.setOggetto(prop.getStringValue(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY)));

		// Tipologia procedimento
		if (prop.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY)) != null) {
			final TipoProcedimentoDTO tipoProcedimentoDTO = tipoProcedimentoSRV
					.getTipoProcedimentoById(prop.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY)));
			procedimentoTracciato.setTipoProcedimento(tipoProcedimentoDTO.getDescrizione());
		}
		if (prop.isPropertyPresent(pp.getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY))) {
			procedimentoTracciato.setCategoriaDocumentoId(prop.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY)));
		}

		if (prop.isPropertyPresent(pp.getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY))) {
			procedimentoTracciato.setTipologiaDocumento(prop.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY)));
		}
		if (prop.isPropertyPresent(pp.getParameterByKey(PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY))) {
			procedimentoTracciato.setIdUfficioCreatore(prop.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY)));
		}
		if (prop.isPropertyPresent(pp.getParameterByKey(PropertiesNameEnum.ITER_APPROVATIVO_SEMAFORO_METAKEY))) {
			final String iterApp = prop.getStringValue(pp.getParameterByKey(PropertiesNameEnum.ITER_APPROVATIVO_SEMAFORO_METAKEY));
			procedimentoTracciato.setTipoFirma(-1);
			if (iterApp != null) {
				final Integer iterAppInt = Integer.parseInt(iterApp);
				procedimentoTracciato.setTipoFirma(iterAppInt);
			}

		}
		return procedimentoTracciato;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITracciaDocumentiFacadeSRV#hasProcedimentiTracciati(it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public boolean hasProcedimentiTracciati(final UtenteDTO utente) {
		final List<Integer> idDocTracciati = getDocumentiTracciati(utente.getIdUfficio(), utente.getId(), utente.getIdRuolo());
		return idDocTracciati != null && !idDocTracciati.isEmpty();
	}

}