/**
 * 
 */
package it.ibm.red.business.dto;

import java.util.Map;

import it.ibm.red.business.enums.DocumentQueueEnum;

/**
 * @author APerquoti
 *
 */
public class CountFepaDTO extends AbstractDTO {

		private static final long serialVersionUID = 4689233327050153209L;

	/**
	 * Numero fatture da lavorare.
	 */
	private Integer daLavorareFatt;
	/**
	 * Numero fatture in lavorazione.
	 */
	private int lavorazioneFatt;

	/**
	 * Numero decredi da lavorare.
	 */
	private int daLavorareDD;

	/**
	 * Numero decreti in firma.
	 */
	private int firmaDD;

	/**
	 * Numero decreti da firmare.
	 */
	private int daFirmareDD;

	/**
	 * Numero decreti firmati.
	 */
	private int firmatiDD;

	/**
	 * Numero dichiarazione servizi resi.
	 */
	private int dsr;
	
	/**
	 * Preparazione spedizione.
	 */
	private Integer preparazioneSpedizione;

	/**
	 * Costruttore di default.
	 */
	public CountFepaDTO() {
		super();
	}

	/**
	 * Costruttore DTO CountFepa che inizializza i parametri a partire da una Map.
	 * @param map dei parametri
	 */
	public CountFepaDTO(final Map<DocumentQueueEnum, Integer> map) {
		super();
		daLavorareFatt = map.get(DocumentQueueEnum.FATTURE_DA_LAVORARE);
		lavorazioneFatt = map.get(DocumentQueueEnum.FATTURE_IN_LAVORAZIONE);
		daLavorareDD = map.get(DocumentQueueEnum.DD_DA_LAVORARE);
		firmaDD = map.get(DocumentQueueEnum.DD_IN_FIRMA);
		daFirmareDD = map.get(DocumentQueueEnum.DD_DA_FIRMARE);
		firmatiDD = map.get(DocumentQueueEnum.DD_FIRMATI);
		dsr = map.get(DocumentQueueEnum.DSR);
		preparazioneSpedizione = map.get(DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE_FEPA);
	}

	/**
	 * Restituisce il numero di documenti presenti in una coda, gestisce solo le code FEPA.
	 * @param queue
	 * @return contatore coda in ingresso
	 */
	public final Integer count(final DocumentQueueEnum queue) {
		Integer total = null;
		
		if (DocumentQueueEnum.FATTURE_DA_LAVORARE.equals(queue)) {
			total = daLavorareFatt;
		} else if (DocumentQueueEnum.FATTURE_IN_LAVORAZIONE.equals(queue)) {
			total = lavorazioneFatt;
		} else if (DocumentQueueEnum.DD_DA_LAVORARE.equals(queue)) {
			total = daLavorareDD;
		} else if (DocumentQueueEnum.DD_IN_FIRMA.equals(queue)) {
			total = firmaDD;
		} else if (DocumentQueueEnum.DD_DA_FIRMARE.equals(queue)) {
			total = daFirmareDD;
		} else if (DocumentQueueEnum.DD_FIRMATI.equals(queue)) {
			total = firmatiDD;
		} else if (DocumentQueueEnum.DSR.equals(queue)) {
			total = dsr;
		} else if (DocumentQueueEnum.PREPARAZIONE_ALLA_SPEDIZIONE_FEPA.equals(queue)) {
			total = preparazioneSpedizione;
		}
		
		return total;
	}

	/**
	 * @return the daLavorareFatt
	 */
	public Integer getDaLavorareFatt() {
		return daLavorareFatt;
	}

	/**
	 * @return the lavorazioneFatt
	 */
	public Integer getLavorazioneFatt() {
		return lavorazioneFatt;
	}

	/**
	 * @return the daLavorareDD
	 */
	public Integer getDaLavorareDD() {
		return daLavorareDD;
	}

	/**
	 * @return the firmaDD
	 */
	public Integer getFirmaDD() {
		return firmaDD;
	}

	/**
	 * @return the daFirmareDD
	 */
	public Integer getDaFirmareDD() {
		return daFirmareDD;
	}

	/**
	 * @return the firmatiDD
	 */
	public Integer getFirmatiDD() {
		return firmatiDD;
	}

	/**
	 * @return the dsr
	 */
	public Integer getDsr() {
		return dsr;
	}

	/**
	 * @return the preparazioneSpedizione
	 */
	public Integer getPreparazioneSpedizione() {
		return preparazioneSpedizione;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "CountFepaDTO [daLavorareFatt=" + daLavorareFatt + ", lavorazioneFatt=" + lavorazioneFatt
				+ ", daLavorareDD=" + daLavorareDD + ", firmaDD=" + firmaDD + ", daFirmareDD=" + daFirmareDD
				+ ", firmatiDD=" + firmatiDD + ", dsr=" + dsr + ", preparazioneSpedizione=" + preparazioneSpedizione
				+ "]";
	}
	
}
