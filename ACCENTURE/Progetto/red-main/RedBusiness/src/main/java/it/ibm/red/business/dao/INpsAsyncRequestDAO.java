package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.nps.dto.AsyncRequestDTO;

/**
 * 
 * @author CPIERASC
 *
 *         Interfaccia del DAO per la gestione delle richieste asincrone verso
 *         NPS.
 */
@SuppressWarnings("rawtypes")
public interface INpsAsyncRequestDAO extends Serializable {

	/**
	 * Metodo per l'inserimento di una richiesta asincrona.
	 * 
	 * @param asyncRequest
	 *            dati richiesta
	 * @param connection
	 *            connessione
	 * @return
	 */

	int insert(AsyncRequestDTO asyncRequest, Connection connection);

	/**
	 * Metodo per il recupero delle prime richieste asincrone da lavorare in coda.
	 * Il metodo locka le richieste estratta modificandone lo stato.
	 * 
	 * @param idAoo
	 * @param partition
	 * @param priority
	 * @param connection
	 * @return
	 */
	List<AsyncRequestDTO> getAsyncRequests(int idAoo, int partition, int priority, Connection connection);
	
	/**
	 * Metodo che aggiorna le richiesta in input con stato IN_LAVORAZIONE.
	 * 
	 * @param richieste
	 * @param connection
	 * @return
	 */
	int presaInCaricoAsyncRequests(List<AsyncRequestDTO> richieste, Connection connection);

	/**
	 * Metodo che aggiorna la richiesta in input.
	 * 
	 * @param numeroAnnoProtocollo
	 * @param id_nar
	 * @param stato
	 * @param eccezione
	 * @param count_retry
	 * @param connection
	 * @return
	 */
	int updateStatoAsyncRequest(String numeroAnnoProtocollo, int idNar, int stato, String eccezione, int countRetry,
			Connection connection);

	/**
	 * Data un AOO restituisce la partizione relativa
	 * 
	 * @param idAoo
	 * @param connection
	 * @return
	 */
	int getPartition(int idAoo, Connection connection);

	/**
	 * Recupera gli item associati al protocollo di emergenza in input, non ancora associati al protocollo ufficiale. I dati sono estratti in ordine di id crescente
	 * 
	 * @param infoProtocollo
	 * @param connection
	 * @return
	 */
	List<AsyncRequestDTO> recuperaItemProtocolliEmergenza(String infoProtocollo, Connection connection);

	/**
	 * Aggiorna le informazioni relative al protocollo ufficiale: numeroannoprotocollo e blob input
	 * @param item
	 * @param connection
	 */
	int aggiornaProtocolloUfficiale(AsyncRequestDTO item, Connection connection);
	
	/**
	 * Controlla se le attività precedenti all'idNar in input sono tutte complete per il protocollo in input.
	 * 
	 * @param numeroAnnoProtocollo
	 * @param idNar
	 * @param connection
	 * 
	 * @return true, se sono complete, false altrimenti
	 */
	boolean isActivitiesComplete(String numeroAnnoProtocollo, int idNar, Connection connection);
	
	
	/**
	 * Recupera l'ID della NAR più recente per il protocollo in input.
	 * 
	 * @param infoProtocollo
	 * @param isEmergenzaNotUpdated
	 * @param connection
	 * @return
	 */
	int getMaxIdNarByProtocollo(String infoProtocollo, boolean isEmergenzaNotUpdated, Connection connection);
	
	/**
	 * Restituisce true se è presente un item nella tabella asincrona di Nps associata ad un document title e ad un nome di un metodo.
	 * @param documentTitle identificativo del documento, deve essere non null
	 * @param methodName
	 * @param connection
	 * @return true se esiste un item associato, false altrimenti
	 */
	boolean isActivityPresent(final String documentTitle, final String methodName, final Connection connection);

}
