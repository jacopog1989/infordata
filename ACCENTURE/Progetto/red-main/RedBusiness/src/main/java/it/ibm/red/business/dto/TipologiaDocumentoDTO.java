package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Simone Lacarpia
 *
 *	Sul DB tabella: TIPOLOGIADOCUMENTO
 */
public class TipologiaDocumentoDTO implements Serializable {

	private static final long serialVersionUID = 5254573131142343829L;
	
	/**
	 * 
	 */
	private Integer idTipologiaDocumento;
	
	/**
	 * LISTAGG(tn.idnodo, ',') WITHIN GROUP(ORDER BY tn.idnodo) AS nodi
	 */
	private String nodi;
	
	/**
	 * 
	 */
	private Integer idAoo;
	
	/**
	 * 
	 */
	private String descrizione;
	
	/**
	 * 
	 */
	private Date dataAttivazione;
	
	/**
	 * 
	 */
	private Date dataDisattivazione;
	
	/**
	 * 
	 */
	private Integer idTipoCategoria;
	
	/**
	 * 
	 */
	private Integer idTipologiaDocumentoUscita;
	
	/**
	 * 
	 */
	private Integer idIterApprovativo;
	
	/**
	 * 
	 */
	private Boolean flagVisto;
	
	/**
	 * 
	 */
	private Integer contabile;
	
	/**
	 * 
	 */
	private Integer idClasseDocumentale;
	
	/**
	 * 
	 */
	private Boolean flagFirmaCopiaConforme;
	
	/**
	 * 
	 */
	private String ordinamento;
	
	/**
	 * Classe documentale 
	 */
	private String documentClass;
	
	/**
	 * Id per doc Aggiuntivo allegato OP non è un intero	
	 */
	private String idTipologiaDocAggiuntivo;
	

	/**
	 * Costruttore
	 * 
	 * @param idTipologiaDocumento
	 * @param nodi
	 * @param idAoo
	 * @param descrizione
	 * @param dataAttivazione
	 * @param dataDisattivazione
	 * @param idTipoCategoria
	 * @param idTipologiaDocumentoUscita
	 * @param idIterApprovativo
	 * @param flagVisto
	 * @param contabile
	 * @param idClasseDocumentale
	 * @param flagFirmaCopiaConforme
	 * @param ordinamento
	 * @param documentClass
	 */
	public TipologiaDocumentoDTO(final Integer idTipologiaDocumento, final String nodi, final Integer idAoo, 
			final String descrizione, final Date dataAttivazione, final Date dataDisattivazione, final Integer idTipoCategoria,
			final Integer idTipologiaDocumentoUscita, final Integer idIterApprovativo, final Boolean flagVisto,
			final Integer contabile, final Integer idClasseDocumentale, final Boolean flagFirmaCopiaConforme,
			final String ordinamento, final String documentClass) {
		this.idTipologiaDocumento = idTipologiaDocumento; 
		this.nodi = nodi;
		this.idAoo = idAoo;
		this.descrizione = descrizione;  
		this.dataAttivazione = dataAttivazione;
		this.dataDisattivazione = dataDisattivazione;  
		this.idTipoCategoria = idTipoCategoria; 
		this.idTipologiaDocumentoUscita = idTipologiaDocumentoUscita;  
		this.idIterApprovativo = idIterApprovativo;  
		this.flagVisto = flagVisto; 
		this.contabile = contabile;  
		this.idClasseDocumentale = idClasseDocumentale;  
		this.flagFirmaCopiaConforme = flagFirmaCopiaConforme; 
		this.ordinamento = ordinamento;  
		this.documentClass = documentClass;
	}
	
	/**
	 * Costruttore vuoto.
	 */
	public TipologiaDocumentoDTO() {
	}

	/**
	 * Restituisce l'id della tipologia documento.
	 * @return id tipologia documento
	 */
	public Integer getIdTipologiaDocumento() {
		return idTipologiaDocumento;
	}

	/**
	 * Restituisce i nodi.
	 * @return nodi
	 */
	public String getNodi() {
		return nodi;
	}

	/**
	 * Restituisce l'id dell'Aoo.
	 * @return id Aoo
	 */
	public Integer getIdAoo() {
		return idAoo;
	}

	/**
	 * Restituisce la descrizione della tipologia documento.
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Restituisce la data attivazione della tipologia documento.
	 * @return data attivazione
	 */
	public Date getDataAttivazione() {
		return dataAttivazione;
	}

	/**
	 * Restituisce la data di disattivazione della tipologia documento.
	 * @return data disattivazione
	 */
	public Date getDataDisattivazione() {
		return dataDisattivazione;
	}

	/**
	 * Restituisce l'id del tipo categoria della tipologia documento.
	 * @return id tipo categoria
	 */
	public Integer getIdTipoCategoria() {
		return idTipoCategoria;
	}

	/**
	 * Restituisce l'id della tipologia documento uscita.
	 * @return id tipologia documento uscita
	 */
	public Integer getIdTipologiaDocumentoUscita() {
		return idTipologiaDocumentoUscita;
	}

	/**
	 * Restituisce l'id dell'iter approvativo.
	 * @return id iter approvativo
	 */
	public Integer getIdIterApprovativo() {
		return idIterApprovativo;
	}

	/**
	 * Restituisce il flag Visto.
	 * @return flag visto
	 */
	public Boolean getFlagVisto() {
		return flagVisto;
	}

	/**
	 * Restituisce il contabile.
	 * @return contabile
	 */
	public Integer getContabile() {
		return contabile;
	}

	/**
	 * Restituisce l'id della classe documentale.
	 * @return id classe documentale
	 */
	public Integer getIdClasseDocumentale() {
		return idClasseDocumentale;
	}

	/**
	 * Restituisce il flag associato al flag copia conforme.
	 * @return flag copia conforme
	 */
	public Boolean getFlagFirmaCopiaConforme() {
		return flagFirmaCopiaConforme;
	}

	/**
	 * Restituisce l'ordinamento.
	 * @return ordinamento
	 */
	public String getOrdinamento() {
		return ordinamento;
	}

	/**
	 * Restituisce la classe documentale.
	 * @return classe documentale
	 */
	public String getDocumentClass() {
		return documentClass;
	}

	/**
	 * Imposta l'id della tipologia documento.
	 * @param inIdTipologiaDocumento
	 */
	public void setIdTipologiaDocumento(final Integer inIdTipologiaDocumento) {
		this.idTipologiaDocumento = inIdTipologiaDocumento;
	}

	/**
	 * Restituisce la descrizione.
	 * @param inDescrizione
	 */
	public void setDescrizione(final String inDescrizione) {
		this.descrizione = inDescrizione;
	}
	
	/**
	 * Restituisce l'id della tipologia documento aggiuntivo.
	 * @return id tipologia documento aggiuntivo
	 */
	public String getIdTipologiaDocAggiuntivo() {
		return idTipologiaDocAggiuntivo;
	}

	/**
	 * Imposta l'id della tipologia documento aggiuntivo.
	 * @param inIdTipologiaDocAggiuntivo
	 */
	public void setIdTipologiaDocAggiuntivo(final String inIdTipologiaDocAggiuntivo) {
		this.idTipologiaDocAggiuntivo = inIdTipologiaDocAggiuntivo;
	}

}
