package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.ASignMasterDTO;
import it.ibm.red.business.dto.ASignStepResultDTO;
import it.ibm.red.business.dto.DocumentoDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.SignModeEnum;
import it.ibm.red.business.enums.SignTypeEnum.SignTypeGroupEnum;
import it.ibm.red.business.service.asign.step.StatusEnum;
import it.ibm.red.business.service.asign.step.StepEnum;

/**
 * DAO gestione firma asincrona.
 */
public interface IASignDAO extends Serializable {

	/**
	 * Restituisce il primo item lavorabile in base alla priorità degli item. Il
	 * metodo effettua un lock sulla tabella finché l'item selezionato come
	 * lavorabile non viene rimosso logicamente dagli item selezionabili, garantendo
	 * l'accesso ad un item ad un singolo processo.
	 * 
	 * @param connection
	 *            connessione al database
	 * @return id item da lavorare
	 */
	Long getAndLockFirstItem(Connection connection);

	/**
	 * Effettua la insert sul database delle informazioni su un step.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param lastResult
	 *            informazioni da inserire
	 */
	void insertStepInfo(Connection connection, ASignStepResultDTO lastResult);

	/**
	 * Restituisce <code> true </code> se il documento associato all'item
	 * identificato dall' <code> idItem </code> è protocollato, altrimenti
	 * restituisce <code> false </code>.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idItem
	 *            identificativo dell'item
	 * @return <code> true </code> se il documento è protocollato,
	 *         <code> false </code> altrimenti
	 */
	Boolean isProtocollato(Connection connection, Long idItem);

	/**
	 * Restituisce le informazioni sullo step del tipo specificato da {@code step} e
	 * associato all'item identificato dal {@code idItem}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idItem
	 *            identificativo dell'item
	 * @param step
	 *            step specifico per il quale occorrono le informazioni
	 * @return informazioni sullo step
	 */
	ASignStepResultDTO getInfo(Connection connection, Long idItem, StepEnum step);

	/**
	 * Restituisce le informazioni su tutti gli step raggiunti dall'item
	 * identificato dall' {@code idItem}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idItem
	 *            identificativo dell'item
	 * @return informazioni sugli step
	 */
	List<ASignStepResultDTO> getInfos(Connection connection, Long idItem);

	/**
	 * Aggiorna lo status dell'item identificato dall' {@code idItem} inserendo il
	 * nuovo {@code status}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idItem
	 *            identificativo dell'item
	 * @param status
	 *            status da inserire
	 */
	void updateStatusItem(Connection connection, Long idItem, StatusEnum status);

	/**
	 * Recupera il primo item <em> resumable </em> scegliendo quello a priorità
	 * maggiore tra tutti gli item <em> resumable </em.
	 * 
	 * @param connection
	 *            connessione al database
	 * @return item in stato @see {@link StatusEnum#RETRY}.
	 */
	ASignItemDTO getAndLockFirstResumableItem(Connection connection);

	/**
	 * Imposta il numero di tentativi associati all'item identificato dall'
	 * {@code idItem}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idItem
	 *            identificativo dell'item
	 * @param n
	 *            numero di tentativi da impostare
	 * @param bResetDate
	 *            se {@code true} effettua un reset della data di ultimo tentativo,
	 *            se {@code false} la data di ultimo tentativo viene aggiornata con
	 *            la data di inserimento numero tentativi
	 */
	void setRetry(Connection connection, Long idItem, Integer n, Boolean bResetDate);

	/**
	 * Effettua la insert dell'item di firma asincrona associato al documento
	 * identificato dal {@code documentTitle}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param documentTitle
	 *            identificativo del documento
	 * @param wobNumber
	 *            wobnumber del documento
	 * @param numeroDocumento
	 *            numero documento
	 * @param numeroProtocollo
	 *            numero protocollo se già protocollato, {@code null} altrimenti
	 * @param annoProtocollo
	 *            anno protocollo se già protocollato, {@code null} altrimenti
	 * @param idUtenteFirmatario
	 *            identificativo dell'utente responsabile per la firma
	 * @param idUfficioUtenteFirmatario
	 *            identificativo dell'ufficio dell'utente firmatario
	 * @param idRuoloUtenteFirmatario
	 *            id del ruolo dell'utente firmatario
	 * @param idAOO
	 *            identificativo dell'area organizzativa
	 * @param signMode
	 *            modalità di firma, @see SignMode
	 * @param signType
	 *            tipologia di firma, @see SignTypeEnum
	 * @param queue
	 *            coda di lavorazione alla quale è associato l'item di firma (Libro
	 *            Firma o Libro Firma FEPA)
	 * @return id dell'item inserito
	 */
	Long insertItem(Connection connection, String documentTitle, String wobNumber, Integer numeroDocumento, Integer numeroProtocollo, Integer annoProtocollo, 
			Long idUtenteFirmatario, Long idUfficioUtenteFirmatario, Long idRuoloUtenteFirmatario, Long idAOO, SignModeEnum signMode, 
			SignTypeGroupEnum signType, DocumentQueueEnum queue);
	
	/**
	 * Imposta la priorità di lavorazione dell'item identificato dall'
	 * {@code idItem}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idItem
	 *            identificativo dell'item
	 * @param priority
	 *            priorità da impostare
	 */
	void setPriority(Connection connection, Long idItem, Integer priority);

	/**
	 * Imposta la priorità al massimo sull'item associato al documento identificato
	 * dal {@code numeroProtocollo} e {@code annoProtocollo}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param numeroProtocollo
	 *            numero protocollo del documento associato all'item
	 * @param annoProtocollo
	 *            anno protocollo del documento associato all'item
	 */
	void setMaxPriority(Connection connection, Integer numeroProtocollo, Integer annoProtocollo);

	/**
	 * Restituisce l'id massimo tra gli item associati al documento caratterizzato
	 * dal {@code numeroProtocollo} e {@code annoProtocollo}.
	 * 
	 * @param con
	 *            connessione al database
	 * @param numeroProtocollo
	 *            numero protocollo del documento
	 * @param annoProtocollo
	 *            anno protocollo del documento
	 * @return identificativo item recuperato
	 */
	Long getMaxIdItem(Connection con, Integer numeroProtocollo, Integer annoProtocollo);

	/**
	 * Restituisce gli item di firm asincrona associati ai documenti identificati
	 * dai {@code dts}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param dts
	 *            identificativi dei documenti
	 * @return lista degli item di firma asincrona
	 */
	List<ASignItemDTO> getItemsFromDTS(Connection connection, Collection<String> dts);

	/**
	 * Restituisce set di document title afferenti ad una specifica coda.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param queue
	 *            NSD, DD_DA_FIRMARE (FEPA) o {@code null} se occorrono tutti i
	 *            document title senza distinzione di coda
	 * @param idAoo
	 *            identificativo dell'area organizzativa
	 * @return lista dei document title dei documenti
	 */
	Set<String> getDTS(Connection connection, DocumentQueueEnum queue, Long idAoo);

	/**
	 * Restituisce le informazioni sugli item di firma asincrona per il popolamento
	 * dei master dei documenti identificati dai {@code dts}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param dts
	 *            identificativi dei documenti
	 * @return mappa che associa ai document title i dettagli sui master
	 */
	Map<String, ASignMasterDTO> getMasterInfo(Connection connection, Collection<String> dts);

	/**
	 * Restituisce le informazioni sul documento identificato dal {@code dt}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param dt
	 *            identificativo del documento
	 * @return informazioni sull'item di firma asincrona
	 */
	ASignItemDTO getDetailInfo(Connection connection, String dt);

	/**
	 * Restituisce {@code true} se l'item è presente nella tabella degli item di
	 * firma asincrona, {@code false} altrimenti.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param documentTitle
	 *            identificativo del documento
	 * @return {@code true} se presente in tabella <em> PREPARAZIONE_ALLA_SPEDIZIONE </em>,
	 *         {@code false} altrimenti
	 */
	Boolean isPresent(Connection connection, String documentTitle);

	/**
	 * Restituisce le informazioni sull'item identificato dall' {@code idItem}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idItem
	 *            identificativo dell'item
	 * @param bLog
	 *            se {@code true} consente il recupero del log
	 * @return informazioni sull'item
	 */
	ASignItemDTO getItem(Connection connection, Long idItem, Boolean bLog);

	/**
	 * Restituisce il log dello step associato all'item identificato dall'
	 * {@code idItem}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idItem
	 *            identificativo dell'item
	 * @param step
	 *            step per il quale occorre recuperare il log
	 * @return log associato allo step
	 */
	String getLog(Connection connection, Long idItem, StepEnum step);

	/**
	 * Esegue l'aggiornamento delle informazioni sul protocollo del documento
	 * associato all'item identificato dall' {@code idItem}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idItem
	 *            identificativo dell'item
	 * @param numeroProtocollo
	 *            numero protocollo da impostare
	 * @param annoProtocollo
	 *            anno protocollo da impostare
	 */
	void updateProtocolloItem(Connection connection, Long idItem, Integer numeroProtocollo, Integer annoProtocollo);
	
	/**
	 * Recupera stato raggiunto da un documento identificato dal
	 * {@code documentTitle} e lo restituisce come @see StatusEnum.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param documentTitle
	 *            identificativo del documento
	 * @return ultimo status raggiunto dall'item associato al documento identificato
	 *         dal {@code documentTitle}
	 */
	StatusEnum checkDocStatus(Connection connection, String documentTitle);
	
	/**
	 * Recupera id dell'item associato al documento identificato dal
	 * {@code documentTitle}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param documentTitle
	 *            identificativo del documento
	 * @return identificativo dell'item
	 */
	Long getIdItem(Connection connection, String documentTitle);

	/**
	 * Imposta la priorità al massimo per l'item associato al documento identificato
	 * dal {@code dt}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param dt
	 *            identificativo del documento
	 */
	void setMaxPriority(Connection connection, String dt);
	
	/**
	 * Restituisce l'identificativo dell'area organizzativa di riferimento dell'item
	 * identificato dall' {@code idItem}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idItem
	 *            identificativo dell'item
	 * @return identificativo dell'area organizzativa
	 */
	Long getIdAoo(Connection connection, Long idItem);

	/**
	 * Inserisce lo step {@link StepEnum#STOP} per l'item identificato dall'
	 * {@code idItem}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idItem
	 *            identificativo dell'item
	 * @return identificativo dello step inserito
	 */
	Long insertStopStep(Connection connection, Long idItem);

	/**
	 * Restituisce un set di document title costituito da tutti gli item che si
	 * trovano nello stato {@link StatusEnum#ELABORATO}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param queue
	 *            coda di lavorazione in cui ricercare (Libro Firma o Libro Firma
	 *            Fepa)
	 * @return set di identificativi documenti elaborati
	 */
	Set<String> getDTSCompleted(Connection connection, DocumentQueueEnum queue);

	/**
	 * Restituisce una lista degli item in errore quindi nello
	 * {@link StatusEnum#ERRORE}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idAoo
	 *            identificativo dell'area organizzativa
	 * @return lista degli item in errore
	 */
	List<ASignItemDTO> getItemsDTFailed(Connection connection, Long idAoo);
	
	/**
	 * @param connection
	 *            connessione al database
	 * @param wobNumber
	 *            wobnumber documento
	 * @param guid
	 *            guid documento
	 * @param contentFirmato
	 *            byte array del content firmato
	 * @param flagAllegato
	 *            se {@code true} il documento è un allegato, se {@code false} il
	 *            documento non è un allegato
	 */
	void insertIntoSignedDocsBuffer(Connection connection, String wobNumber, String guid, byte[] contentFirmato, Boolean flagAllegato);
	
	/**
	 * Restituisce i documenti firmati associati al {@code wobNumber}.
	 * 
	 * @param connection
	 *            connessioen al database
	 * @param wobNumber
	 *            identificativo documento
	 * @return lista documenti
	 */
	Collection<DocumentoDTO> getFromSignedDocsBuffer(Connection connection, String wobNumber);

	/**
	 * Rimuove l'item associato al documento identificato dal {@code wobNumber}
	 * dalla tabella del buffer di firma asincrona.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param wobNumber
	 *            wob number del documento da rimuovere
	 * @param guid
	 *            guid del documento da rimuovere
	 */
	void removeDocFromSignedDocsBuffer(Connection connection, String wobNumber, String guid);
	
	/**
	 * Rimuove l'item associato al documento identificato dal {@code wobNumber}
	 * dalla tabella del buffer di firma asincrona.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param wobNumber
	 *            identificativo del documento
	 */
	void removeFromSignedDocsBufferByWobNumber(Connection connection, String wobNumber);

	/**
	 * Aggiorna il content princiapale dell'item associato al documento identificato
	 * dal {@code wobNumber} dalla tabella del buffer di firma asincrona.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param contentPrincipale
	 *            connessione da impostare
	 * @param wobNumber
	 *            identificativo del documento da aggiornare
	 */
	void updateContentPrincipaleForSignedDocsBuffer(Connection connection, byte[] contentPrincipale, String wobNumber);

	/**
	 * Memorizza informazioni di firma asincrone aggiuntive associate all'item
	 * identificato dall' {@code idItem}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idItem
	 *            identificativo dell'item
	 * @param dataToSave
	 *            informazioni da rendere persistenti sulla base dati
	 */
	void saveItemData(Connection connection, Long idItem, Map<String, String> dataToSave);

	/**
	 * Restituisce le informazioni aggiuntive presenti sulla base dati associate
	 * all'item identificato dall' {@code idItem}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idItem
	 *            identificativo dell'item
	 * @return mappa che contiene le informazioni
	 */
	Map<String, String> getItemData(Connection connection, Long idItem);

	/**
	 * Elimina le informazioni aggiuntive associate all'item identificato dall'
	 * {@code cpsId}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param cpsId
	 *            identificativo dell'item da cui eliminare le informazioni
	 *            aggiuntive
	 */
	void deleteItemData(Connection connection, Long cpsId);

	/**
	 * Imposta il flag CPS_FLAG_COMUNICATO sull'item identificato dall'
	 * {@code idItem}.
	 * 
	 * @param idItem
	 *            identificativo item da aggiornare
	 * @param comunicato
	 *            flag da impostare
	 * @param connection
	 *            connessione al database
	 */
	void setIsNotified(Long idItem, boolean comunicato, Connection connection);

	/**
	 * Restituisce l'id dell'item di firma associato all'id del documento in
	 * ingresso. Un documento può essere associato a piu di un item (in caso di
	 * firma multipla), in questo caso solo un item sarà ancora in lavorazione e lo
	 * stato risulterà diverso da {@link StatusEnum#ELABORATO}, quello sarà l'item
	 * restituito.
	 * 
	 * @param idDocumento
	 *            identificativo del documento
	 * @param idAoo
	 *            identificativo dell'area organizzativa
	 * @param connection
	 *            connessione al database
	 * @return item ancora in lavorazione associato al documento identificato
	 *         dall'id
	 */
	Long getIdItemFromIdDocumento(Long idDocumento, Long idAoo, Connection connection);
	
	/**
	 * Recupera tutti i documenti per i quali esiste un item associato che non sia
	 * ancora stato lavorato e portato fino allo {@link StatusEnum#ELABORATO}.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param queue
	 *            coda di lavorazione nella quale si trovano <em> logicamente </em>
	 *            i documenti
	 * @param idAoo
	 *            identificativo dell'area organizzativa
	 * @return lista dei document title recuperati
	 */
	Set<String> getIncompletedDTs(Connection connection, DocumentQueueEnum queue, Long idAoo);
	
	/**
	 * Recupera tutti gli item in {@link StatusEnum#PRESO_IN_CARICO} lockando la
	 * tabella e inizializzando il timer di timeout per garantire la non lavorazione
	 * dello stesso item da un altro processo.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idAoo
	 *            identificativo dell'area organizzativa
	 * @param codiceAoo
	 *            codice dell'area organizzativa
	 * @return lista degli id degli item <em> playable </em>
	 */
	List<Long> getAndLockAllPlayableItems(Connection connection, Long idAoo, String codiceAoo);
	
	/**
	 * Recupera tutti gli item in {@link StatusEnum#RETRY} lockando la tabella e
	 * inizializzando il timer di timeout per garantire la non lavorazione dello
	 * stesso item da un altro processo.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idAoo
	 *            identificativo dell'area organizzativa
	 * @param codiceAoo
	 *            codice dell'area organizzativa
	 * @return lista degli item <em> resumable </em>
	 */
	List<ASignItemDTO> getAndLockAllResumableItems(Connection connection, Long idAoo, String codiceAoo);

	/**
	 * Restituisce {@code true} se il documento identificato dal
	 * {@code documentTitle} è associato ad almeno un item nello
	 * {@link StatusEnum#ELABORATO}, altrimenti restituisce {@code false}.
	 * 
	 * @param connection
	 *            connessione la database
	 * @param documentTitle
	 *            identificativo del documento
	 * @return {@code true} se il documento è firmato, {@code false} altrimenti
	 */
	boolean isSignedOnce (Connection connection, String documentTitle);
}
