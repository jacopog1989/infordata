/**
 * 
 */
package it.ibm.red.business.service;


import java.util.List;

import it.ibm.red.business.dto.AllaccioRiferimentoStoricoDTO;
import it.ibm.red.business.service.facade.IRiferimentoStoricoFacadeSRV;

/**
 * @author VINGENITO
 *
 */
public interface IRiferimentoStoricoSRV extends IRiferimentoStoricoFacadeSRV {

	/**
	 * Ottiene il riferimento storico tramite aoo.
	 * @param docTitle
	 * @param idAoo
	 * @param idFascicoloProcedimentale
	 * @return lista di allacci
	 */
	 List<AllaccioRiferimentoStoricoDTO> getRiferimentoStoricoByAoo(String docTitle, Long idAoo, String idFascicoloProcedimentale);

}
