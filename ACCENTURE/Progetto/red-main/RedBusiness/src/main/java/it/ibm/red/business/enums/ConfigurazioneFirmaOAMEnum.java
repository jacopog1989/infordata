package it.ibm.red.business.enums;

/**
 * Enum configurazione firma OAM.
 */
public enum ConfigurazioneFirmaOAMEnum {
	
	/**
	 * Valore.
	 */
	DISABILITATA("", "Utilizzare le configurazioni dell'AOO", ConfigurazioneFirmaAooEnum.DISABILITATA),
	
	/**
	 * Valore.
	 */
	SOLO_LOCALE("1", "Utilizzare solo la firma locale", ConfigurazioneFirmaAooEnum.SOLO_LOCALE),
	
	/**
	 * Valore.
	 */
	SOLO_REMOTA("2", "Utilizzare solo la firma remota", ConfigurazioneFirmaAooEnum.SOLO_REMOTA),
	
	/**
	 * Valore.
	 */
	LOCALE_REMOTA("3", "Utilizzare sia la firma locale che la firma remota", ConfigurazioneFirmaAooEnum.LOCALE_REMOTA);
	

	/**
	 * Codice.
	 */
	private String codice;
	
	/**
	 * Descrizione.
	 */
	private String descrizione;
	
	/**
	 * Configurazione firma aoo.
	 */
	private ConfigurazioneFirmaAooEnum aooConfig;

	/**
	 * Costruttore di classe.
	 * @param code
	 * @param description
	 * @param aooConfig
	 */
	ConfigurazioneFirmaOAMEnum(final String code, final String description, final ConfigurazioneFirmaAooEnum aooConfig) {
		this.codice = code;
		this.descrizione = description;
		this.aooConfig = aooConfig;
	}

	
	/**
	 * @return codice
	 */
	public String getCodice() {
		return codice;
	}


	/**
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}


	/**
	 * @return aooConfig
	 */
	public ConfigurazioneFirmaAooEnum getAooConfig() {
		return aooConfig;
	}

	/**
	 * Restituisce l'enum associata al codice.
	 * @param inCodice
	 * @return ConfigurazioneFirmaOAMEnum
	 */
	public static ConfigurazioneFirmaOAMEnum getByCodice(final String inCodice) {
		ConfigurazioneFirmaOAMEnum output = null;
		
		for (final ConfigurazioneFirmaOAMEnum sso: ConfigurazioneFirmaOAMEnum.values()) {
			if (sso.getCodice().equals(inCodice)) {
				output = sso;
				break;
			}
		}
		
		return output;
	}
}