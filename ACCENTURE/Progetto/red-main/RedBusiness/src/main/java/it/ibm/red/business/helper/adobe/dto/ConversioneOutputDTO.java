package it.ibm.red.business.helper.adobe.dto;

import org.apache.commons.fileupload.FileItem;

/**
 * DTO che definisce un output di conversione.
 */
public class ConversioneOutputDTO {

    /**
     * Documento.
     */
	private FileItem documento;
	
    /**
     * Preview.
     */
	private FileItem preview;

	/**
	 * Costruttore vuoto.
	 */
	public ConversioneOutputDTO() {
		// Costruttore di default.
	}

	/**
	 * Restituisce il FileItem che definisce il documento.
	 * @return documento
	 */
	public FileItem getDocumento() {
		return documento;
	}

	/**
	 * Imposta il documento come FileItem.
	 * @param documento
	 */
	public void setDocumento(final FileItem documento) {
		this.documento = documento;
	}

	/**
	 * Restituisce la preview come FileItem.
	 * @return preview
	 */
	public FileItem getPreview() {
		return preview;
	}

	/**
	 * Imposta la preview.
	 * @param preview
	 */
	public void setPreview(final FileItem preview) {
		this.preview = preview;
	}
	
}