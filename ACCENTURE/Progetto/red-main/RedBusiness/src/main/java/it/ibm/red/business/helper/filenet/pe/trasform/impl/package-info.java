/**
 * @author CPIERASC
 *
 *	Questo package conterrà tutti i trasformer implementati per trasformare un workflow in un oggetto java: attraverso
 *	una factory si nasconderà il costruttore di tale trasformazione, in pratica il client si limiterà a fornire un
 *	workflow ed a selezionare una specifica trasformazione tramite un enum, il sistema in automatico applicherà la
 *	trasformazione indicata dall'enum e restituirà l'oggetto trasformato.
 *
 */
package it.ibm.red.business.helper.filenet.pe.trasform.impl;
