/**
 * 
 */
package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.INextStepFacadeSRV;

/**
 * @author APerquoti
 *
 */
public interface INextStepSRV extends INextStepFacadeSRV {

}
