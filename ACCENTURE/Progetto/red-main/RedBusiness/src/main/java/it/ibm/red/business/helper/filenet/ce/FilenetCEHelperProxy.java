package it.ibm.red.business.helper.filenet.ce;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider; 

/**
 * Proxy Filenet CE.
 */
public final class FilenetCEHelperProxy implements InvocationHandler {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FilenetCEHelperProxy.class);


	/**
	 * Filenet CE.
	 */
	private final IFilenetCEHelper fcehInterface;

	/**
	 * Flag log enabled.
	 */
	private final boolean doLog;

	private FilenetCEHelperProxy(final IFilenetCEHelper fcehInterface) {
		this.fcehInterface = fcehInterface;
		this.doLog = !"true".equals(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DISABLE_LOG_PROXY_FILENET));
	}

	/**
	 * Restituisce un'istanza del FilenetCEHelper.
	 * @param dto
	 * @param idAoo
	 * @return istanza di FilenetCEHelper
	 */
	public static IFilenetCEHelper newInstance(final FilenetCredentialsDTO dto, final Long idAoo) {
		final IFilenetCEHelper fceh = new FilenetCEHelper(dto);
		boolean doLog = !"true".equals(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DISABLE_LOG_PROXY_FILENET));
		
		if (doLog) {
			LOGGER.info("Recupero dell'instanza Filenet per l'AOO con identificativo: " + idAoo);
		}
		
		LOGGER.info("Recupero ");
		return (IFilenetCEHelper) java.lang.reflect.Proxy.newProxyInstance(
				fceh.getClass().getClassLoader(), fceh.getClass().getInterfaces(), new FilenetCEHelperProxy(fceh));
	}

	/**
	 * Restituisce un'istanza del FilenetCEHelper.
	 * @param userName
	 * @param password
	 * @param urlCE
	 * @param stanzaJAAS
	 * @param objectStore
	 * @param idAoo
	 * @return istanza di FilenetCEHelper
	 */
	public static IFilenetCEHelper newInstance(final String userName, final String password, final String urlCE, final String stanzaJAAS,
			final String objectStore, final Long idAoo) {
		
		boolean doLog = !"true".equals(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DISABLE_LOG_PROXY_FILENET));
		
		if (doLog) {
			LOGGER.info("Recupero dell'instanza Filenet per l'AOO con identificativo: " + idAoo);
		}
		
		final IFilenetCEHelper fceh = new FilenetCEHelper(userName, password, urlCE, stanzaJAAS, objectStore);
		return (IFilenetCEHelper) java.lang.reflect.Proxy.newProxyInstance(
				fceh.getClass().getClassLoader(), fceh.getClass().getInterfaces(), new FilenetCEHelperProxy(fceh));
	}

	@Override
	public Object invoke(final Object proxy, final Method m, final Object[] args) throws Throwable {
		Object result = null;
		Throwable targetException = null;
		try {
			
			if (doLog) {
				LOGGER.info("Inizio chiamata proxy Filenet: " + m.getName());
			}
			result = m.invoke(fcehInterface, args);
			if (doLog) {
				LOGGER.info("Fine chiamata proxy Filenet: " + m.getName());
			}

		} catch (final InvocationTargetException e) {
			LOGGER.error("Errore in fase di invocazione del metodo nel proxy", e);
			targetException = e.getTargetException();
			throw targetException;
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di invocazione del metodo nel proxy", e);
			targetException = new RedException("Errore in fase di invocazione del metodo", e);
			throw targetException;
		} 
		return result;
	}

}
