package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IEventoLogDAO;
import it.ibm.red.business.dto.EventoLogDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.IEventoLogSRV;

/**
 * The Class EventoLogSRV.
 *
 * @author m.crescentini
 * 
 *         Servizio per il tracciamento degli eventi (EventoLog).
 */
@Service
@Component
public class EventoLogSRV extends AbstractService implements IEventoLogSRV {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(EventoLogSRV.class.getName());
	
	/**
	 * LITERAL inizio errore.
	 */
	private static final String ERRORE_IN_FASE_DI_LITERAL = "Errore in fase di ";

	/**
	 * LITERAL riferimento documento.
	 */
	private static final String DEL_DOCUMENTO_LITERAL = " del documento ";

	/**
	 * DAO.
	 */
	@Autowired
	private IEventoLogDAO eventoLogDAO;
	
	/**
	 * @see it.ibm.red.business.service.IEventoLogSRV#inserisciEventoLog(int,
	 *      java.lang.Long, java.lang.Long, java.lang.Long, java.lang.Long,
	 *      java.util.Date, it.ibm.red.business.enums.EventTypeEnum,
	 *      java.lang.String, int, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public void inserisciEventoLog(final int idDocumento, final Long idUfficioMittente, final Long idUtenteMittente, final Long idUfficioDestinatario, final Long idUtenteDestinatario,
			final Date dataAssegnazione, final EventTypeEnum eventType,	final String motivazioneAssegnazione, final int idTipoAssegnazione, final Long idAoo, final Connection connection) {
		LOGGER.info("inserisciEventoLog -> START. Documento: " + idDocumento + ", evento: " + eventType);
		
		final EventoLogDTO eventoLog = new EventoLogDTO(idDocumento, idUfficioMittente, idUtenteMittente, idUfficioDestinatario, idUtenteDestinatario, dataAssegnazione, 
				eventType.getValue(), motivazioneAssegnazione, idTipoAssegnazione);

		eventoLogDAO.inserisciEvento(eventoLog, idAoo, connection);
		
		LOGGER.info("inserisciEventoLog -> END. Documento: " + idDocumento + ", evento: " + eventType);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IEventoLogFacadeSRV#inserisciEventoLog(int,
	 *      java.lang.Long, java.lang.Long, java.lang.Long, java.lang.Long,
	 *      java.util.Date, it.ibm.red.business.enums.EventTypeEnum,
	 *      java.lang.String, int, java.lang.Long).
	 */
	@Override
	public void inserisciEventoLog(final int idDocumento, final Long idUfficioMittente, final Long idUtenteMittente, final Long idUfficioDestinatario, final Long idUtenteDestinatario,
			final Date dataAssegnazione, final EventTypeEnum eventType,	final String motivazioneAssegnazione, final int idTipoAssegnazione, final Long idAoo) {
		Connection connection = null;
		
		try {
			connection = setupConnection(getFilenetDataSource().getConnection(), false);	
			inserisciEventoLog(idDocumento, idUfficioMittente,idUtenteMittente, idUfficioDestinatario, idUtenteDestinatario, dataAssegnazione, eventType, motivazioneAssegnazione, idTipoAssegnazione, idAoo, connection);
		} catch (Exception e) {
			LOGGER.error(ERRORE_IN_FASE_DI_LITERAL + eventType + DEL_DOCUMENTO_LITERAL + idDocumento, e);
			throw new RedException(ERRORE_IN_FASE_DI_LITERAL + eventType + DEL_DOCUMENTO_LITERAL + idDocumento, e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IEventoLogFacadeSRV#writeSpostaCopiaEventLog(it.ibm.red.business.dto.UtenteDTO,
	 *      int, it.ibm.red.business.enums.EventTypeEnum, java.lang.Integer,
	 *      java.lang.Integer).
	 */
	@Override
	public void writeSpostaCopiaEventLog(final UtenteDTO utente, final int idDocumento, final EventTypeEnum eventType,
			final Integer idFascicoloProvenienza, final Integer idFascicoloTarget) {
		Connection connection = null;
		
		try {
			connection = setupConnection(getFilenetDataSource().getConnection(), false);
			final String azione = (eventType.equals(EventTypeEnum.COPIA_IN_ALTRO_FASCICOLO)) ? "copiato" : "spostato";
			final String motivazione = "Documento " + azione + " dal fascicolo " + idFascicoloProvenienza + " al fascicolo " + idFascicoloTarget;
			
			inserisciEventoLog(idDocumento, utente.getIdUfficio(), utente.getId(), (long) 0, (long) 0, new Date(), eventType, motivazione, 0, utente.getIdAoo(), connection);
		} catch (final Exception e) {
			LOGGER.error(ERRORE_IN_FASE_DI_LITERAL + eventType + DEL_DOCUMENTO_LITERAL + idDocumento, e);
			throw new RedException(ERRORE_IN_FASE_DI_LITERAL + eventType + DEL_DOCUMENTO_LITERAL + idDocumento, e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IEventoLogFacadeSRV#writePredisposizioneRispostaAutomaticaEventLog(java.lang.Long,
	 *      java.lang.Long, int, java.lang.String, java.lang.Long).
	 */
	@Override
	public void writePredisposizioneRispostaAutomaticaEventLog(final Long idUfficioMittente, final Long idUtenteMittente,
			final int idDocumento, final String motivazione, final Long idAoo) {
		Connection connection = null;
		final EventTypeEnum eventType = EventTypeEnum.PREDISPOSIZIONE_RISPOSTA;
		try {
			connection = setupConnection(getFilenetDataSource().getConnection(), false);
			inserisciEventoLog(idDocumento, idUfficioMittente, idUtenteMittente, (long) 0, (long) 0, new Date(), eventType, motivazione, 0, idAoo, connection);
		} catch (final Exception e) {
			LOGGER.error(ERRORE_IN_FASE_DI_LITERAL + eventType + DEL_DOCUMENTO_LITERAL + idDocumento, e);
			throw new RedException(ERRORE_IN_FASE_DI_LITERAL + eventType + DEL_DOCUMENTO_LITERAL + idDocumento, e);
		} finally {
			closeConnection(connection);
		}
		
	}
	
	
	/**
	 * Recupera dallo storico l'ultimo evento di chiusura del documento in ingresso
	 * (messa agli atti o preparazione alla risposta).
	 * 
	 * @param idDocumento
	 * @param idAoo
	 * @return ultimo evento di chiusura
	 */
	@Override
	public EventoLogDTO getLastChiusuraIngressoEvent(final int idDocumento, final int idAoo) {
		Connection connection = null;
		try {
			connection = setupConnection(getFilenetDataSource().getConnection(), false);
			return eventoLogDAO.getLastChiusuraIngressoEvent(connection, idDocumento, idAoo);
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero dell'ultimo evento di chiusura del documento " + idDocumento, e);
			throw new RedException("Errore in fase di recupero dell'ultimo evento di chiusura del documento " + idDocumento, e);
		} finally {
			closeConnection(connection);
		}
	}
	
	/**
	 * @see it.ibm.red.business.service.IEventoLogSRV#getIdUfficioCreatore(java.util.List,
	 *      java.lang.Long).
	 */
	@Override
	public final HashMap<String,Integer> getIdUfficioCreatore(final List<String> documentTitle, final Long idAoo) {
		Connection connFilenet = null;
		try {	
			connFilenet = setupConnection(getFilenetDataSource().getConnection(), false);
			return eventoLogDAO.getIdUfficioCreatore(documentTitle, idAoo, connFilenet);
		} catch (Exception e) {
			rollbackConnection(connFilenet);
			LOGGER.error("Errore nel recupero dell'ufficio creatore" + documentTitle, e);
			throw new RedException(e);
		} finally {
			closeConnection(connFilenet);
		}
	}
}