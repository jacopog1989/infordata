/**
 * @author CPIERASC
 *
 *	In questo package avremo le implementazioni dei Data Access Object: gli oggetti JAVA utilizzati per l'accesso alle
 *	tabelle del RDBMS; da notare che ciascun DAO accedere ad una sola tabella, lasciando ai servizi il compito di comporre
 *	tra loro le varie entità del model per costruire artefatti più articolati.
 *	
 */
package it.ibm.red.business.dao.impl;
