package it.ibm.red.business.dto;

import it.ibm.red.business.enums.FormatoAllegatoEnum;

/**
 * The Class PlaceholderInfoDTO.
 */
public class PlaceholderInfoDTO extends AbstractDTO {

	 
	private static final long serialVersionUID = 3178236883860732586L;

	
	/**
	 * Mantieni formato originale
	 */
	private Boolean mantieniFormatoOriginale;
	
	/**
	 * Flag da firmare 
	 */
	private Boolean daFirmare;
	
	/**
	 * Formato selezione allegato
	 */
	private FormatoAllegatoEnum formatoSelected;
	
	/**
	 * Flag formato originale
	 */
	private Boolean formatoOriginale;
	
	/**
	 * Flag firma visibile
	 */
	private Boolean firmaVisibile;
	
	/**
	 * Flag stampigliatura sigla allegato
	 */
	private Boolean stampigliaturaSiglaAllegato;
	
	/**
	 * Guid allegato
	 */
	private String guidAllegato;
	
	/**
	 * Flag file principale firmato
	 */
	private Boolean filePrincipaleFirmato;
	

	/**
	 * Costruttore placeholder info DTO.
	 */
	public PlaceholderInfoDTO() {
		// Metodo intenzionalmente vuoto.
	}

	/** 
	 * @return the mantieni formato originale
	 */
	public Boolean getMantieniFormatoOriginale() {
		return mantieniFormatoOriginale;
	}

	/** 
	 * @param mantieniFormatoOriginale the new mantieni formato originale
	 */
	public void setMantieniFormatoOriginale(final Boolean mantieniFormatoOriginale) {
		this.mantieniFormatoOriginale = mantieniFormatoOriginale;
	}

	/** 
	 * @return the da firmare
	 */
	public Boolean getDaFirmare() {
		return daFirmare;
	}

	/** 
	 * @param daFirmare the new da firmare
	 */
	public void setDaFirmare(final Boolean daFirmare) {
		this.daFirmare = daFirmare;
	}

	/** 
	 * @return the formato selected
	 */
	public FormatoAllegatoEnum getFormatoSelected() {
		return formatoSelected;
	}

	/** 
	 * @param formatoSelected the new formato selected
	 */
	public void setFormatoSelected(final FormatoAllegatoEnum formatoSelected) {
		this.formatoSelected = formatoSelected;
	}

	/** 
	 * @return the formato originale
	 */
	public Boolean getFormatoOriginale() {
		return formatoOriginale;
	}

	/** 
	 * @param formatoOriginale the new formato originale
	 */
	public void setFormatoOriginale(final Boolean formatoOriginale) {
		this.formatoOriginale = formatoOriginale;
	}
	
	/** 
	 * @return the firma visibile
	 */
	public Boolean getFirmaVisibile() {
		return firmaVisibile;
	}

	/** 
	 * @param firmaVisibile the new firma visibile
	 */
	public void setFirmaVisibile(final Boolean firmaVisibile) {
		this.firmaVisibile = firmaVisibile;
	}

	/** 
	 * @return the stampigliatura sigla allegato
	 */
	public boolean getStampigliaturaSiglaAllegato() {
		return stampigliaturaSiglaAllegato;
	}
	
	/** 
	 * @param stampigliaturaSiglaAllegato the new stampigliatura sigla allegato
	 */
	public void setStampigliaturaSiglaAllegato(final Boolean stampigliaturaSiglaAllegato) {
		this.stampigliaturaSiglaAllegato = stampigliaturaSiglaAllegato;
	}
	
	/** 
	 * @return the guid allegato
	 */
	public String getGuidAllegato() {
		return guidAllegato;
	}
	
	/** 
	 * @param guidAllegato the new guid allegato
	 */
	public void setGuidAllegato(final String guidAllegato) {
		this.guidAllegato = guidAllegato;
	}
	 
	/** 
	 * @return the file principale firmato
	 */
	public Boolean getFilePrincipaleFirmato() {
		return filePrincipaleFirmato;
	}

	/** 
	 * @param filePrincipaleFirmato the new file principale firmato
	 */
	public void setFilePrincipaleFirmato(final Boolean filePrincipaleFirmato) {
		this.filePrincipaleFirmato = filePrincipaleFirmato;
	}
}
