/**
 * 
 */
package it.ibm.red.business.dto;

import java.io.Serializable;

import it.ibm.red.business.enums.StatoElabMessaggioPostaNpsEnum;

/**
 * 
 * @author CristianoPierascenzi
 *
 * @param <T>	Tipo generico della ricchiesta
 */
public class RichiestaElabMessaggioPostaNpsDTO<T extends MessaggioPostaNpsDTO> extends AbstractDTO implements Serializable {

	private static final long serialVersionUID = 2838876558765672125L;

	/**
	 * Coda.
	 */
	private Long idCoda;
	
	/**
	 * Message id.
	 */
	private String messageIdNps;
	
	/**
	 * Messaggio.
	 */
	private T messaggio;
		
	/**
	 * Stato.
	 */
	private StatoElabMessaggioPostaNpsEnum stato;
	
	/**
	 * Aoo.
	 */
	private Long idAoo;
	
	/**
	 * Identificativo tracciamento.
	 */
	private Long idTrack;
	
	
	/**
	 * @param messaggio
	 * @param stato
	 * @param idAoo
	 * @param idTrack
	 */
	public RichiestaElabMessaggioPostaNpsDTO(final String messageIdNps, final T messaggio, final StatoElabMessaggioPostaNpsEnum stato, final Long idAoo, 
			final Long idTrack) {
		this(null, messageIdNps, messaggio, stato, idAoo, idTrack);
	}
	
	
	/**
	 * @param idCoda
	 * @param messaggio
	 * @param stato
	 * @param idAoo
	 * @param idTrack
	 */
	public RichiestaElabMessaggioPostaNpsDTO(final Long idCoda, final String messageIdNps, final T messaggio, final StatoElabMessaggioPostaNpsEnum stato, 
			final Long idAoo, final Long idTrack) {
		super();
		this.idCoda = idCoda;
		this.messageIdNps = messageIdNps;
		this.messaggio = messaggio;
		this.stato = stato;
		this.idAoo = idAoo;
		this.idTrack = idTrack;
	}
	

	/**
	 * @return the idRichiesta
	 */
	public Long getIdCoda() {
		return idCoda;
	}
	

	/**
	 * @return the messageIdNps
	 */
	public String getMessageIdNps() {
		return messageIdNps;
	}


	/**
	 * @return the messaggio
	 */
	public T getMessaggio() {
		return messaggio;
	}


	/**
	 * @return the stato
	 */
	public StatoElabMessaggioPostaNpsEnum getStato() {
		return stato;
	}


	/**
	 * @return the idAoo
	 */
	public Long getIdAoo() {
		return idAoo;
	}
	
	/**
	 * @return the idTrack
	 */
	public Long getIdTrack() {
		return idTrack;
	}
	
}
