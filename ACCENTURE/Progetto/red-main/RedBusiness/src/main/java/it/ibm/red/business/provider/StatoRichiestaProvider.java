package it.ibm.red.business.provider;

import java.util.Collection;
import java.util.HashMap;

import it.ibm.red.business.dto.StatoRichiestaDTO;
import it.ibm.red.business.service.IStatoRichiestaSRV;

/**
 * The Class StatoRichiestaProvider.
 *
 * @author CPIERASC
 * 
 *         Provider stato richiesta.
 */
public final class StatoRichiestaProvider extends HashMap<String, StatoRichiestaDTO> {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -3354265367886819484L;
	
	/**
	 * Istanza (singleton).
	 */
	private static StatoRichiestaProvider provider;
	
	/**
	 * SRV StatoRichiesta.
	 */
	private IStatoRichiestaSRV statoRichiestaSRV;
	
	/**
	 * Costruttore.
	 */
	private StatoRichiestaProvider() {
		statoRichiestaSRV = (IStatoRichiestaSRV) ApplicationContextProvider.getApplicationContext().getBean("statoRichiestaSRV");

		Collection<StatoRichiestaDTO> stati = statoRichiestaSRV.getAll();
		if (stati != null) {
			for (StatoRichiestaDTO sr : stati) {
				super.put(sr.getStepName(), sr);
			}
		}
	}
	
	/**
	 * Getter istanza singleton.
	 * 
	 * @return	istanza
	 */
	public static StatoRichiestaProvider getIstance() {
		if (provider == null) {
			provider = new StatoRichiestaProvider();
		}
		return provider;
	}
	
	/**
	 * Metodo per il recupero dello stato della richiesta dal nome dello step.
	 * 
	 * @param stepName	nome dello step
	 * @return			stato della richiesta
	 */
	public StatoRichiestaDTO getStatoRichiestaByStepName(final String stepName) {
		return super.get(stepName);
	}
	
	/**
	 * Equals.
	 *
	 * @param object
	 *            the object
	 * @return true, if successful
	 */
	@Override
	public boolean equals(final Object object) {
		return super.equals(object);
	}
	
	/**
	 * 
	 * tags:
	 * @return
	 */
	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
