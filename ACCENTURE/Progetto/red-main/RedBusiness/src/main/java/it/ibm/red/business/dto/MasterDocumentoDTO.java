package it.ibm.red.business.dto;

import java.util.Date;

import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.IconaStatoEnum;
import it.ibm.red.business.enums.StatoDocumentoFuoriLibroFirmaEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoOperazioneLibroFirmaEnum;
import it.ibm.red.business.enums.TipoSpedizioneDocumentoEnum;

/**
 * The Class MasterDocumentoDTO.
 *
 * @author CPIERASC
 * 
 *         Data Transfer Object per i dati sintetici di un documento.
 */
public class MasterDocumentoDTO extends AbstractDTO {

	/**
	 * Serializzable.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Flag urgente.
	 */
	private Boolean urgente;

	/**
	 * Icona che rappresenta lo stato del documento.
	 */
	private IconaStatoEnum iconaStato;

	/**
	 * Anno protocollo.
	 */
	private final Integer annoProtocollo;

	/**
	 * Flag allegati presenti.
	 */
	private final Boolean bAttachmentPresent;

	/**
	 * Flag trasformazione in errore.
	 */
	private final Boolean bTrasfPdfErrore;

	/**
	 * Flag trasformazione in warning.
	 */
	private final Boolean bTrasfPdfWarning;

	/**
	 * Document title.
	 */
	private final String documentTitle;

	/**
	 * Flag abilita firma.
	 */
	private Boolean flagEnableFirma;

	/**
	 * Flag abilita firma autografa.
	 */
	private Boolean flagEnableFirmaAutografa;

	/**
	 * Flag abilita rifiuto.
	 */
	private Boolean flagEnableRifiuta;

	/**
	 * Flag abilita sigla.
	 */
	private Boolean flagEnableSigla;

	/**
	 * Flag abilita visto.
	 */
	private Boolean flagEnableVista;

	/**
	 * Flag Procedi da Corriere.
	 */
	private Boolean flagEnableProcediCorriere;

	/**
	 * Flag firma digitale.
	 */
	private Boolean flagFirmaDig;

	/**
	 * Identificativo utente destinatario.
	 */
	private Integer idUtenteDestinatario;

	/**
	 * Motivazione assegnazione.
	 */
	private String motivazioneAssegnazione;

	/**
	 * Numero documento.
	 */
	private final Integer numeroDocumento;

	/**
	 * Numero protocollo.
	 */
	private final Integer numeroProtocollo;

	/**
	 * Oggetto.
	 */
	private final String oggetto;

	/**
	 * Operazione.
	 */
	private TipoOperazioneLibroFirmaEnum operazione;

	/**
	 * Coda.
	 */
	private DocumentQueueEnum queue;

	/**
	 * Tipologia documento.
	 */
	private final String tipologiaDocumento;

	/**
	 * Wob number.
	 */
	private String wobNumber;

	/**
	 * Identificativo categoria documewnto.
	 */
	private final Integer categoriaDocumentoId;

	/**
	 * Flag firma autografa Red.
	 */
	private Boolean flagFirmaAutografaRM;

	/**
	 * Data protocollazione.
	 */
	private final Date dataProtocollazione;

	/**
	 * Data creazione.
	 */
	private final Date dataCreazione;

	/**
	 * Data scadenza.
	 */
	private final Date dataScadenza;

	/**
	 * Urgenza.
	 */
	private final Integer urgenza;

	/**
	 * Stato.
	 */
	private StatoDocumentoFuoriLibroFirmaEnum stato;

	/**
	 * Tipo spedizione.
	 */
	private final TipoSpedizioneDocumentoEnum tipoSpedizione;

	/**
	 * Identificativo categoria documento.
	 */
	private final Integer idCategoriaDocumento;

	/**
	 * Identificativo formato documento.
	 */
	private final Integer idFormatoDocumento;

	/**
	 * Flag renderizzato.
	 */
	private Boolean flagRenderizzato;

	/**
	 * Identificativo momento protocollazione.
	 */
	private final Integer idMomentoProtocollazione;

	/**
	 * Tipologia protocollo.
	 */
	private final Integer tipoProtocollo;

	/**
	 * Flag riservato.
	 */
	private final Boolean flagRiservato;

	/**
	 * Numero contributi.
	 */
	private Long numContributi;

	/**
	 * Flag contributi.
	 */
	private Boolean hasContributi;

	/**
	 * Flag firma copia conforme.
	 */
	private Boolean firmaCopiaConforme;

	/**
	 * Tipologia assegnazione.
	 */
	private TipoAssegnazioneEnum tipoAssegnazione;

	/**
	 * Flag firma PDF.
	 */
	private Boolean flagFirmaPDF;

	/**
	 * Costruttore.
	 */
	public MasterDocumentoDTO() {
		super();
		tipoSpedizione = null;
		idCategoriaDocumento = null;
		idFormatoDocumento = null;
		dataCreazione = null;
		documentTitle = null;
		numeroDocumento = null;
		oggetto = null;
		tipologiaDocumento = null;
		operazione = null;
		numeroProtocollo = null;
		annoProtocollo = null;
		wobNumber = null;
		bAttachmentPresent = null;
		bTrasfPdfErrore = null;
		bTrasfPdfWarning = null;
		categoriaDocumentoId = null;
		flagEnableFirma = false;
		flagEnableFirmaAutografa = false;
		flagEnableRifiuta = false;
		flagEnableVista = false;
		flagEnableSigla = false;
		flagEnableProcediCorriere = false;
		dataScadenza = null;
		urgenza = null;
		dataProtocollazione = null;
		flagFirmaAutografaRM = null;
		iconaStato = null;
		idMomentoProtocollazione = null;
		tipoProtocollo = null;
		flagRiservato = null;
		numContributi = null;
	}

	/**
	 * Costruttore.
	 *
	 * @param inDocumentTitle
	 *            document title
	 * @param inNumeroDocumento
	 *            numero documento
	 * @param inOggetto
	 *            oggetto
	 * @param inTipologiaDocumento
	 *            tipologia documento
	 * @param inOperazione
	 *            operazione
	 * @param inNumeroProtocollo
	 *            numero protocollo
	 * @param inAnnoProtocollo
	 *            anno protocollo
	 * @param inWobNumber
	 *            wob number
	 * @param inAttachmentPresent
	 *            flag allegati presenti
	 * @param inTrasfPdfErrore
	 *            flag trasformazione in errore
	 * @param inCategoriaDocumentoId
	 *            identificativo categoria documento
	 * @param inDataScadenza
	 *            data scadenza
	 * @param inUrgenza
	 *            flag urgenza
	 * @param inDataCreazione
	 *            data creazione
	 * @param inDataProtocollazione
	 *            data protocollazione
	 * @param inTipoSpedizione
	 *            tipologia spedizione
	 * @param inIdCategoriaDocumento
	 *            identificativo categoria documento
	 * @param inIdFormatoDocumento
	 *            identificativo formato documento
	 * @param inFlagFirmaAutografaRM
	 *            flag firma autografa red mobile
	 * @param inIdMomentoProtocollazione
	 *            identificativo momento protocollazione
	 * @param inTipoProtocollo
	 *            tipo protocollo
	 * @param inFlagRiservato
	 *            flag riservato
	 * @param inNumContributi
	 *            numero contributi
	 * @param inFlagFirmaPDF
	 *            the in flag firma PDF
	 */
	public MasterDocumentoDTO(final String inDocumentTitle, final Integer inNumeroDocumento, final String inOggetto, final String inTipologiaDocumento,
			final TipoOperazioneLibroFirmaEnum inOperazione, final Integer inNumeroProtocollo, final Integer inAnnoProtocollo, final String inWobNumber,
			final Boolean inAttachmentPresent, final Boolean inTrasfPdfErrore, final Boolean inTrasfPdfWarning, final Integer inCategoriaDocumentoId,
			final Date inDataScadenza, final Integer inUrgenza, final Date inDataCreazione, final Date inDataProtocollazione,
			final TipoSpedizioneDocumentoEnum inTipoSpedizione, final Integer inIdCategoriaDocumento, final Integer inIdFormatoDocumento, final Boolean inFlagFirmaAutografaRM,
			final Integer inIdMomentoProtocollazione, final Integer inTipoProtocollo, final Boolean inFlagRiservato, final Long inNumContributi,
			final Boolean inFlagFirmaPDF) {
		super();
		tipoSpedizione = inTipoSpedizione;
		idCategoriaDocumento = inIdCategoriaDocumento;
		idFormatoDocumento = inIdFormatoDocumento;
		dataCreazione = inDataCreazione;
		documentTitle = inDocumentTitle;
		numeroDocumento = inNumeroDocumento;
		oggetto = inOggetto;
		tipologiaDocumento = inTipologiaDocumento;
		operazione = inOperazione;
		numeroProtocollo = inNumeroProtocollo;
		annoProtocollo = inAnnoProtocollo;
		wobNumber = inWobNumber;
		bAttachmentPresent = inAttachmentPresent;
		bTrasfPdfErrore = inTrasfPdfErrore;
		bTrasfPdfWarning = inTrasfPdfWarning;
		categoriaDocumentoId = inCategoriaDocumentoId;
		flagEnableFirma = false;
		flagEnableFirmaAutografa = false;
		flagEnableRifiuta = false;
		flagEnableVista = false;
		flagEnableSigla = false;
		flagEnableProcediCorriere = false;
		dataScadenza = inDataScadenza;
		urgenza = inUrgenza;
		dataProtocollazione = inDataProtocollazione;
		flagFirmaAutografaRM = inFlagFirmaAutografaRM;
		idMomentoProtocollazione = inIdMomentoProtocollazione;
		tipoProtocollo = inTipoProtocollo;
		flagRiservato = inFlagRiservato;
		numContributi = inNumContributi;
		flagFirmaPDF = inFlagFirmaPDF;
	}

	/**
	 * Costruttore.
	 * 
	 * @param inDocumentTitle
	 * @param inNumeroDocumento
	 * @param inOggetto
	 * @param inTipologiaDocumento
	 * @param inOperazione
	 * @param inNumeroProtocollo
	 * @param inAnnoProtocollo
	 * @param inWobNumber
	 * @param inAttachmentPresent
	 * @param inTrasfPdfErrore
	 * @param inTrasfPdfWarning
	 * @param inCategoriaDocumentoId
	 * @param inDataScadenza
	 * @param inUrgenza
	 * @param inDataCreazione
	 * @param inDataProtocollazione
	 * @param inTipoSpedizione
	 * @param inIdCategoriaDocumento
	 * @param inIdFormatoDocumento
	 * @param inFlagFirmaAutografaRM
	 * @param inIdMomentoProtocollazione
	 * @param inTipoProtocollo
	 * @param inFlagRiservato
	 * @param inHasContributi
	 * @param inFlagFirmaPDF
	 */
	public MasterDocumentoDTO(final String inDocumentTitle, final Integer inNumeroDocumento, final String inOggetto, final String inTipologiaDocumento,
			final TipoOperazioneLibroFirmaEnum inOperazione, final Integer inNumeroProtocollo, final Integer inAnnoProtocollo, final String inWobNumber,
			final Boolean inAttachmentPresent, final Boolean inTrasfPdfErrore, final Boolean inTrasfPdfWarning, final Integer inCategoriaDocumentoId,
			final Date inDataScadenza, final Integer inUrgenza, final Date inDataCreazione, final Date inDataProtocollazione,
			final TipoSpedizioneDocumentoEnum inTipoSpedizione, final Integer inIdCategoriaDocumento, final Integer inIdFormatoDocumento, final Boolean inFlagFirmaAutografaRM,
			final Integer inIdMomentoProtocollazione, final Integer inTipoProtocollo, final Boolean inFlagRiservato, final Boolean inHasContributi,
			final Boolean inFlagFirmaPDF) {
		super();
		tipoSpedizione = inTipoSpedizione;
		idCategoriaDocumento = inIdCategoriaDocumento;
		idFormatoDocumento = inIdFormatoDocumento;
		dataCreazione = inDataCreazione;
		documentTitle = inDocumentTitle;
		numeroDocumento = inNumeroDocumento;
		oggetto = inOggetto;
		tipologiaDocumento = inTipologiaDocumento;
		operazione = inOperazione;
		numeroProtocollo = inNumeroProtocollo;
		annoProtocollo = inAnnoProtocollo;
		wobNumber = inWobNumber;
		bAttachmentPresent = inAttachmentPresent;
		bTrasfPdfErrore = inTrasfPdfErrore;
		bTrasfPdfWarning = inTrasfPdfWarning;
		categoriaDocumentoId = inCategoriaDocumentoId;
		flagEnableFirma = false;
		flagEnableFirmaAutografa = false;
		flagEnableRifiuta = false;
		flagEnableVista = false;
		flagEnableSigla = false;
		flagEnableProcediCorriere = false;
		dataScadenza = inDataScadenza;
		urgenza = inUrgenza;
		dataProtocollazione = inDataProtocollazione;
		flagFirmaAutografaRM = inFlagFirmaAutografaRM;
		idMomentoProtocollazione = inIdMomentoProtocollazione;
		tipoProtocollo = inTipoProtocollo;
		flagRiservato = inFlagRiservato;
		hasContributi = inHasContributi;
		flagFirmaPDF = inFlagFirmaPDF;
	}

	/**
	 * Getter.
	 * 
	 * @return flag urgente
	 */
	public Boolean getUrgente() {
		return urgente;
	}

	/**
	 * Setter.
	 * 
	 * @param inUrgente flag urgente
	 */
	public void setUrgente(final Boolean inUrgente) {
		this.urgente = inUrgente;
	}

	/**
	 * Gets the flag firma PDF.
	 *
	 * @return the flag firma PDF
	 */
	public Boolean getFlagFirmaPDF() {
		return flagFirmaPDF;
	}

	/**
	 * Setter.
	 * 
	 * @param inFirmaCopiaConforme flag firma copia conforme
	 */
	public final void setFirmaCopiaConforme(final Boolean inFirmaCopiaConforme) {
		this.firmaCopiaConforme = inFirmaCopiaConforme;
	}

	/**
	 * Getter.
	 * 
	 * @return flag firma copia conforme
	 */
	public final Boolean getFirmaCopiaConforme() {
		return firmaCopiaConforme;
	}

	/**
	 * Getter numero contributi.
	 * 
	 * @return numero contributi
	 */
	public final Long getNumContributi() {
		return numContributi;
	}

	/**
	 * Getter.
	 * 
	 * @return test presenza contributi
	 */
	public final Boolean getHasContributi() {
		Boolean output = hasContributi;
		if (output == null) {
			output = (numContributi > 0);
		}
		return output;
	}

	/**
	 * Getter.
	 * 
	 * @return flag riservato
	 */
	public final Boolean getFlagRiservato() {
		return flagRiservato;
	}

	/**
	 * Ritorna il clone.
	 * 
	 * @return Clone
	 */
	public final MasterDocumentoDTO getClone() {
		return new MasterDocumentoDTO(this.documentTitle, this.numeroDocumento, this.oggetto, this.tipologiaDocumento, this.operazione, this.numeroProtocollo,
				this.annoProtocollo, this.wobNumber, this.bAttachmentPresent, this.bTrasfPdfErrore, this.bTrasfPdfWarning, this.categoriaDocumentoId, this.dataScadenza,
				this.urgenza, this.dataCreazione, this.dataProtocollazione, this.tipoSpedizione, this.idCategoriaDocumento, this.idFormatoDocumento, this.flagFirmaAutografaRM,
				this.idMomentoProtocollazione, this.tipoProtocollo, this.flagRiservato, this.numContributi, this.flagFirmaPDF);
	}

	/**
	 * Getter Flag renderizzato.
	 * 
	 * @return flag renderizzato autografa Red
	 */
	public final Boolean getFlagRenderizzato() {
		return flagRenderizzato;
	}

	/**
	 * Setter Flag renderizzato.
	 * 
	 * @param inFlagRenderizzato - Flag renderizzato
	 */
	public final void setFlagRenderizzato(final Boolean inFlagRenderizzato) {
		this.flagRenderizzato = inFlagRenderizzato;
	}

	/**
	 * Getter.
	 * 
	 * @return flag firma autografa Red
	 */
	public final Boolean getFlagFirmaAutografaRM() {
		return flagFirmaAutografaRM;
	}

	/**
	 * Getter.
	 * 
	 * @return data scadenza
	 */
	public final Date getDataScadenza() {
		return dataScadenza;
	}

	/**
	 * Getter.
	 * 
	 * @return urgenza
	 */
	public final Integer getUrgenza() {
		return urgenza;
	}

	/**
	 * Getter.
	 * 
	 * @return data creazione
	 */
	public final Date getDataCreazione() {
		return dataCreazione;
	}

	/**
	 * Setter.
	 * 
	 * @param inStato stato
	 */
	public final void setStato(final StatoDocumentoFuoriLibroFirmaEnum inStato) {
		this.stato = inStato;
	}

	/**
	 * Getter.
	 * 
	 * @return stato
	 */
	public final StatoDocumentoFuoriLibroFirmaEnum getStato() {
		return stato;
	}

	/**
	 * Getter.
	 * 
	 * @return tipo spedizione
	 */
	public final TipoSpedizioneDocumentoEnum getTipoSpedizione() {
		return tipoSpedizione;
	}

	/**
	 * Getter.
	 * 
	 * @return id categoria documento
	 */
	public final Integer getIdCategoriaDocumento() {
		return idCategoriaDocumento;
	}

	/**
	 * Getter.
	 * 
	 * @return id formato documento
	 */
	public final Integer getIdFormatoDocumento() {
		return idFormatoDocumento;
	}

	/**
	 * Getter.
	 * 
	 * @return data protocollazione
	 */
	public final Date getDataProtocollazione() {
		return dataProtocollazione;
	}

	/**
	 * Getter.
	 * 
	 * @return id categoria documento
	 */
	public final Integer getCategoriaDocumentoId() {
		return categoriaDocumentoId;
	}

	/**
	 * Getter.
	 * 
	 * @return anno protocollo
	 */
	public final Integer getAnnoProtocollo() {
		return annoProtocollo;
	}

	/**
	 * Getter.
	 * 
	 * @return flag allegati presenti
	 */
	public final Boolean getbAttachmentPresent() {
		return bAttachmentPresent;
	}

	/**
	 * Getter.
	 * 
	 * @return flag trasformazione pdf in errore
	 */
	public final Boolean getbTrasfPdfErrore() {
		return bTrasfPdfErrore;
	}

	/**
	 * Getter.
	 * 
	 * @return flag trasformazione pdf in warning
	 */
	public final Boolean getbTrasfPdfWarning() {
		return bTrasfPdfWarning;
	}

	/**
	 * Getter.
	 * 
	 * @return document title
	 */
	public final String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Getter.
	 * 
	 * @return flag abilita firma
	 */
	public final Boolean getFlagEnableFirma() {
		return flagEnableFirma;
	}

	/**
	 * Getter.
	 * 
	 * @return flag abilita firma autografa
	 */
	public final Boolean getFlagEnableFirmaAutografa() {
		return flagEnableFirmaAutografa;
	}

	/**
	 * Getter.
	 * 
	 * @return flag abilita rifiuto
	 */
	public final Boolean getFlagEnableRifiuta() {
		return flagEnableRifiuta;
	}

	/**
	 * Getter.
	 * 
	 * @return flag abilita sigla
	 */
	public final Boolean getFlagEnableSigla() {
		return flagEnableSigla;
	}

	/**
	 * Getter.
	 * 
	 * @return flag abilita visto
	 */
	public final Boolean getFlagEnableVista() {
		return flagEnableVista;
	}

	/**
	 * Getter.
	 * 
	 * @return flag bilita firma digitale
	 */
	public final Boolean getFlagFirmaDig() {
		return flagFirmaDig;
	}

	/**
	 * Getter.
	 * 
	 * @return identificativo utente destinatario
	 */
	public final Integer getIdUtenteDestinatario() {
		return idUtenteDestinatario;
	}

	/**
	 * Getter.
	 * 
	 * @return motivazione assegnazione
	 */
	public final String getMotivazioneAssegnazione() {
		return motivazioneAssegnazione;
	}

	/**
	 * Getter.
	 * 
	 * @return numero documento
	 */
	public final Integer getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * Getter.
	 * 
	 * @return numero protocollo
	 */
	public final Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * Getter.
	 * 
	 * @return oggetto
	 */
	public final String getOggetto() {
		return oggetto;
	}

	/**
	 * Getter.
	 * 
	 * @return operazione
	 */
	public final TipoOperazioneLibroFirmaEnum getOperazione() {
		return operazione;
	}

	/**
	 * Getter.
	 * 
	 * @return coda
	 */
	public final DocumentQueueEnum getQueue() {
		return queue;
	}

	/**
	 * Getter.
	 * 
	 * @return tipologia documento
	 */
	public final String getTipologiaDocumento() {
		return tipologiaDocumento;
	}

	/**
	 * Getter.
	 * 
	 * @return wob number
	 */
	public final String getWobNumber() {
		return wobNumber;
	}

	/**
	 * Setter.
	 * 
	 * @param inFlagEnableFirma flag abilita firma
	 */
	public final void setFlagEnableFirma(final Boolean inFlagEnableFirma) {
		this.flagEnableFirma = inFlagEnableFirma;
	}

	/**
	 * Setter.
	 * 
	 * @param inFlagEnableFirmaAutografa flag abilita firma digitale
	 */
	public final void setFlagEnableFirmaAutografa(final Boolean inFlagEnableFirmaAutografa) {
		this.flagEnableFirmaAutografa = inFlagEnableFirmaAutografa;
	}

	/**
	 * Setter.
	 * 
	 * @param inFlagEnableRifiuta flag abilita rifiuto
	 */
	public final void setFlagEnableRifiuta(final Boolean inFlagEnableRifiuta) {
		this.flagEnableRifiuta = inFlagEnableRifiuta;
	}

	/**
	 * Setter.
	 * 
	 * @param inFlagEnableSigla flag abilita sigla
	 */
	public final void setFlagEnableSigla(final Boolean inFlagEnableSigla) {
		this.flagEnableSigla = inFlagEnableSigla;
	}

	/**
	 * Setter.
	 * 
	 * @param inFlagEnableVista flag abilita vista
	 */
	public final void setFlagEnableVista(final Boolean inFlagEnableVista) {
		this.flagEnableVista = inFlagEnableVista;
	}

	/**
	 * Setter.
	 * 
	 * @param inFlagFirmaDig flag abilita firma digitale
	 */
	public final void setFlagFirmaDig(final Boolean inFlagFirmaDig) {
		this.flagFirmaDig = inFlagFirmaDig;
	}

	/**
	 * Setter.
	 * 
	 * @param inIdUtenteDestinatario identificativo utente destinatario
	 */
	public final void setIdUtenteDestinatario(final Integer inIdUtenteDestinatario) {
		idUtenteDestinatario = inIdUtenteDestinatario;
	}

	/**
	 * Setter.
	 * 
	 * @param inMotivazioneAssegnazione motivazione assegnazione
	 */
	public final void setMotivazioneAssegnazione(final String inMotivazioneAssegnazione) {
		this.motivazioneAssegnazione = inMotivazioneAssegnazione;
	}

	/**
	 * Setter.
	 * 
	 * @param inOperazione operazione
	 */
	public final void setOperazione(final TipoOperazioneLibroFirmaEnum inOperazione) {
		this.operazione = inOperazione;
	}

	/**
	 * Setter.
	 * 
	 * @param inQueue coda
	 */
	public final void setQueue(final DocumentQueueEnum inQueue) {
		this.queue = inQueue;
	}

	/**
	 * Setter.
	 * 
	 * @param inWobNumber wob number
	 */
	public final void setWobNumber(final String inWobNumber) {
		this.wobNumber = inWobNumber;
	}

	/**
	 * Setter.
	 *
	 * @param inFlagFirmaAutografaRM the new flag firma autografa RM
	 */
	public final void setFlagFirmaAutografaRM(final Boolean inFlagFirmaAutografaRM) {
		this.flagFirmaAutografaRM = inFlagFirmaAutografaRM;
	}

	/**
	 * Getter icona stato.
	 * 
	 * @return icona stato
	 */
	public final IconaStatoEnum getIconaStato() {
		return iconaStato;
	}

	/**
	 * 
	 * tags.
	 * 
	 * @return
	 */
	@Override
	public final int hashCode() {
		final int prime = 31;
		int result = 1;
		int dcHashCode = 0;
		if (documentTitle != null) {
			dcHashCode = documentTitle.hashCode();
		}
		int dcWobNumber = 0;
		if (wobNumber != null) {
			dcWobNumber = wobNumber.hashCode();
		}
		result = prime * result + dcHashCode;
		result = prime * result + dcWobNumber;
		return result;
	}

	/**
	 * Metodo per testare se una proprietà è null per entrambi gli oggetti o, se
	 * diversa da null, se assume il medesimo valore.
	 * 
	 * @param my    oggetto a
	 * @param other oggetto b
	 * @return esito
	 */
	private static Boolean nullOrEqual(final Object my, final Object other) {
		Boolean output = false;
		if ((my == null && other == null) || (my != null && my.equals(other))) {
			output = true;
		}
		return output;
	}

	/**
	 * Equals.
	 *
	 * @param obj the obj
	 * @return true, if successful
	 */
	@Override
	public final boolean equals(final Object obj) {
		Boolean output = false;
		if (obj instanceof MasterDocumentoDTO) {
			final MasterDocumentoDTO other = (MasterDocumentoDTO) obj;
			output = nullOrEqual(documentTitle, other.getDocumentTitle()) && nullOrEqual(wobNumber, other.getWobNumber());
		}
		return output;
	}

	/**
	 * Getter id momento protocollazione.
	 * 
	 * @return id momento protocollazione
	 */
	public final Integer getIdMomentoProtocollazione() {
		return idMomentoProtocollazione;
	}

	/**
	 * Getter tipo protocollo.
	 * 
	 * @return tipo protocollo
	 */
	public final Integer getTipoProtocollo() {
		return tipoProtocollo;
	}

	/**
	 * Setter icona stato.
	 * 
	 * @param inIconaStato icona stato
	 */
	public final void setIconaStato(final IconaStatoEnum inIconaStato) {
		this.iconaStato = inIconaStato;
	}

	/**
	 * Setter.
	 * 
	 * @param inTipoAssegnazione tipo assegnazione
	 */
	public final void setTipoAssegnazione(final TipoAssegnazioneEnum inTipoAssegnazione) {
		tipoAssegnazione = inTipoAssegnazione;
	}

	/**
	 * Getter.
	 * 
	 * @return tipo assegnazione
	 */
	public final TipoAssegnazioneEnum getTipoAssegnazione() {
		return tipoAssegnazione;
	}

	/**
	 * Gets the flag enable procedi corriere.
	 *
	 * @return the flagEnableProcediCorriere
	 */
	public Boolean getFlagEnableProcediCorriere() {
		return flagEnableProcediCorriere;
	}

	/**
	 * Sets the flag enable procedi corriere.
	 *
	 * @param inFlagEnableProcediCorriere the flagEnableProcediCorriere to set
	 */
	public void setFlagEnableProcediCorriere(final Boolean inFlagEnableProcediCorriere) {
		this.flagEnableProcediCorriere = inFlagEnableProcediCorriere;
	}
}
