package it.ibm.red.business.enums;

/**
 * The Enum TextAlignEnum.
 *
 * @author CPIERASC
 * 
 *         Enum per l'allineamento del testo in fase di stampigliatura delle
 *         informazioni del destinatario sul glifo.
 */
public enum TextAlignEnum {
	
	/**
	 * Allineamento a sinistra.
	 */
	LEFT(0),
	
	/**
	 * Allineamento centrale.
	 */
	CENTER(1),
	
	/**
	 * Allineamento a destra.
	 */
	RIGHT(2),
	
	/**
	 * Allineamento giustificato.
	 */
	JUSTIFIED(3);

	/**
	 * Identificativo allineamento.
	 */
	private Integer value;

	/**
	 * Costruttore.
	 * 
	 * @param inValue	identificativo
	 */
	TextAlignEnum(final Integer inValue) {
		this.value = inValue;
	}
	
	/**
	 * Getter identificativo.
	 * 
	 * @return	identificativo
	 */
	public Integer getValue() {
		return value;
	}

}
