/**
 * 
 */
package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.Collection;
import java.util.List;

import com.filenet.api.collection.DocumentSet;

import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.RecuperoCodeDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.service.facade.IRicercaFacadeSRV;
import it.ibm.red.webservice.model.documentservice.types.ricerca.ParametriRicerca;
import it.ibm.red.webservice.model.documentservice.types.ricerca.TipoVersione;

/**
 * @author APerquoti
 *
 */
public interface IRicercaSRV extends IRicercaFacadeSRV {

	/**
	 * @param utente
	 * @param documentTitle
	 * @param classeDocumentale
	 * @param isDocEntrata
	 * @return
	 */
	RecuperoCodeDTO getQueueNameFromDocumentTitle(UtenteDTO utente, String documentTitle, String classeDocumentale,	Boolean isDocEntrata);

	/**
	 * @param utente
	 * @param documentTitle
	 * @return
	 */
	RecuperoCodeDTO getQueueNameFromDocumentTitle(UtenteDTO utente, String documentTitle);

	/**
	 * @param utente
	 * @param connection
	 * @param fascicoliDTO
	 */
	void setDescTitolario(UtenteDTO utente, Connection connection, Collection<FascicoloDTO> fascicoliDTO);

	/**
	 * @param documents
	 * @param utente
	 * @param connection
	 * @return
	 */
	Collection<MasterDocumentRedDTO> trasformToGenericDocContext(DocumentSet documents, UtenteDTO utente, Connection connection);
	
	/**
	 * @param key
	 * @param anno
	 * @param campoRicerca
	 * @param withContent
	 * @param utente
	 * @param fceh
	 * @return
	 */
	DocumentSet ricercaDocumentiDaWs(String key, Integer anno, String campoRicerca, boolean withContent, UtenteDTO utente, IFilenetCEHelper fceh);

	/**
	 * @param key
	 * @param utente
	 * @return
	 */
	DocumentSet ricercaFascicoliDaWs(String key, UtenteDTO utente);
	
	/**
	 * @param key
	 * @param utente
	 * @return
	 */
	DocumentSet ricercaFaldoniDaWs(String key, UtenteDTO utente);
	
	/**
	 * @param classeDocumentale
	 * @param parametriRicerca
	 * @param whereCondition
	 * @param fullTextAnd
	 * @param fullTextOr
	 * @param tipoVersione
	 * @param utente
	 * @return
	 */
	DocumentSet ricercaAvanzataDaWs(String classeDocumentale, List<ParametriRicerca> parametriRicerca, String whereCondition, 
			String fullTextAnd, String fullTextOr, TipoVersione tipoVersione, UtenteDTO utente);
	
}
