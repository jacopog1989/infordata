package it.ibm.red.business.dto;

/**
 * DTO Rds Siebel - Tipologia.
 */
public class RdsSiebelTipologiaDTO extends AbstractDTO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -1851563040291197467L;
	
	/**
	 * Tipologia.
	 */
	private Integer idTipologia;

    /**
     * Descrizione tipologia.
     */
	private String descrizioneTipologia;
	
	/**
	 * Costruttore di default.
	 */
	public RdsSiebelTipologiaDTO() {
		super();
	}

	/**
	 * Costruttore del DTO.
	 * @param idTipologia
	 * @param descrizioneTipologia
	 */
	public RdsSiebelTipologiaDTO(final Integer idTipologia, final String descrizioneTipologia) {
		super();
		this.idTipologia = idTipologia;
		this.descrizioneTipologia = descrizioneTipologia;
	}

	/**
	 * Restituisce l'id della tipologia.
	 * @return id tipologia
	 */
	public Integer getIdTipologia() {
		return idTipologia;
	}

	/**
	 * Imposta l'id della tipologia.
	 * @param idTipologia
	 */
	public void setIdTipologia(final Integer idTipologia) {
		this.idTipologia = idTipologia;
	}

	/**
	 * Restituisce la descrizione della tipologia.
	 * @return descrizione tipologia
	 */
	public String getDescrizioneTipologia() {
		return descrizioneTipologia;
	}

	/**
	 * Imposta la descrizione della tipologia.
	 * @param descrizioneTipologia
	 */
	public void setDescrizioneTipologia(final String descrizioneTipologia) {
		this.descrizioneTipologia = descrizioneTipologia;
	}

	/**
	 * @see java.lang.Object#hashCode().
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idTipologia == null) ? 0 : idTipologia.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object).
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final RdsSiebelTipologiaDTO other = (RdsSiebelTipologiaDTO) obj;
		if (idTipologia == null) { 
			if (other.idTipologia != null) {
				return false;
			}
		} else if (!idTipologia.equals(other.idTipologia)) {
			return false;
		}
		return true;
	}
	
	
}