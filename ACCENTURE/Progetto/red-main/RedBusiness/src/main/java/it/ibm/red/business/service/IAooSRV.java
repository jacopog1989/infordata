package it.ibm.red.business.service;

import java.sql.Connection;

import it.ibm.red.business.enums.SignStrategyEnum;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.service.facade.IAooFacadeSRV;

/**
 * Interface del service che gestisce l'AOO.
 */
public interface IAooSRV extends IAooFacadeSRV {
	
	/**
	 * Il metodo recupera l'Aoo in input.
	 * 
	 * @param idAoo
	 * @param conn
	 * @return
	 */
	Aoo recuperaAoo(int idAoo, Connection conn);
	
	/**
	 * Il metodo recupera l'Aoo in input.
	 * 
	 * @param codiceAoo
	 * @param conn
	 * @return
	 */
	Aoo recuperaAoo(String codiceAoo, Connection conn);
	
	/**
	 * Recupero flag strategia firma dell'aoo
	 * 
	 * @param idAOO
	 * */
	SignStrategyEnum getSignStrategy(Long idAOO);
}
