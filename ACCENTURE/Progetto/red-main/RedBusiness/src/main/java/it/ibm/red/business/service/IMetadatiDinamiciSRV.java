package it.ibm.red.business.service;

import java.util.Collection;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.MetadatoDinamicoDTO;
import it.ibm.red.business.service.facade.IMetadatiDinamiciFacadeSRV;

/**
 * Interface del servizio che gestisce i metadati dinamici.
 */
public interface IMetadatiDinamiciSRV extends IMetadatiDinamiciFacadeSRV {

	/**
	 * Imposta i valori dei metadati dinamici.
	 * @param docFilenet
	 * @param list
	 * @return metadati dinamici
	 */
	Collection<MetadatoDinamicoDTO> settaValoriMetadatiDinamici(Document docFilenet, Collection<MetadatoDinamicoDTO> list);
}
