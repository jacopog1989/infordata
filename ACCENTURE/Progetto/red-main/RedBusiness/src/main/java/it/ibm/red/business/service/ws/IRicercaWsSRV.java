package it.ibm.red.business.service.ws;

import it.ibm.red.business.service.ws.facade.IRicercaWsFacadeSRV;


/**
 * Servizio per la ricerca (generica e avanzata) tramite web service.
 * 
 * @author m.crescentini
 *
 */
public interface IRicercaWsSRV extends IRicercaWsFacadeSRV {

	
}