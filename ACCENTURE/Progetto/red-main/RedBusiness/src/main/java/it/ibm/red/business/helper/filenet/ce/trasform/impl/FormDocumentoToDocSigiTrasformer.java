/**
 * 
 */
package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.Date;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.MasterSigiDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.logger.REDLogger;

/**
 * @author APerquoti
 *
 */
public class FormDocumentoToDocSigiTrasformer extends TrasformerCE<MasterSigiDTO> {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 2332797161964281405L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FormDocumentoToDocSigiTrasformer.class.getName());

	/**
	 * Costruttore.
	 */
	public FormDocumentoToDocSigiTrasformer() {
		super(TrasformerCEEnum.FROM_DOCUMENTO_TO_DOC_SIGI);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE#trasform(com.filenet.api.core.Document, java.sql.Connection).
	 */
	@Override
	public MasterSigiDTO trasform(final Document document, final Connection connection) {
		try {
			
			String idDocumento = (String) getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
			String oggetto = (String) getMetadato(document, PropertiesNameEnum.OGGETTO_METAKEY);
			Integer numeroProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
			Integer annoProtocollo = (Integer) getMetadato(document, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
			String motivoAnnullato = (String) getMetadato(document, PropertiesNameEnum.METADATO_DOCUMENTO_MOTIVAZIONE_ANNULLAMENTO);
			Date dataAnnullato = (Date) getMetadato(document, PropertiesNameEnum.METADATO_DOCUMENTO_DATA_ANNULLAMENTO);
			
			return new MasterSigiDTO(idDocumento, oggetto, numeroProtocollo, annoProtocollo, motivoAnnullato, dataAnnullato);

		} catch (Exception e) {
			LOGGER.error("Errore nel recupero di un metadato durante la trasformazione da Document a MasterSigiDTO : ", e);
			return null;
		}
	}

}
