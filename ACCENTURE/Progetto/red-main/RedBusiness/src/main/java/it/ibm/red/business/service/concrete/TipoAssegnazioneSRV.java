package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.ITipoAssegnazioneDAO;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.ITipoAssegnazioneSRV;

/**
 * @author APerquoti
 */
@Service
public class TipoAssegnazioneSRV extends AbstractService implements ITipoAssegnazioneSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -2554608364604760010L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TipoAssegnazioneSRV.class.getName());
	
	/**
	 * Dao per la gestione dei tipi assegnazione.
	 */
	@Autowired
	private ITipoAssegnazioneDAO tipoAssegnazioneDAO;

	/**
	 * @see it.ibm.red.business.service.facade.ITipoAssegnazioneFacadeSRV#getAllByIdAooIterManuale(java.lang.Integer).
	 */
	@Override
	public List<TipoAssegnazioneEnum> getAllByIdAooIterManuale(final Integer idAoo) {
		List<TipoAssegnazioneEnum> output = new ArrayList<>();
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			
			output = tipoAssegnazioneDAO.getTipiAssegnazioneManualeByIdAoo(idAoo, connection);
			
			// In linea con la logica di NSD si rimuove questa assegnazione in quanto non prevista nel cambio iter
			output.remove(TipoAssegnazioneEnum.SPEDIZIONE);
		} catch (Exception e) {
			LOGGER.error("Si è verificato un rrrore durante il recupero tipi assegnazione", e);
		} finally {
			closeConnection(connection);
		}
		return output;
	}
}
