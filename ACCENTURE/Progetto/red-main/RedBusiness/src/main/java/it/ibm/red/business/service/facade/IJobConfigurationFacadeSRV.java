package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.JobConfigurationDTO;

/**
 * The Interface IJobFacadeSRV.
 *
 * @author a.dilegge
 * 
 *         Facade del servizio di gestione dei job quartz.
 */
public interface IJobConfigurationFacadeSRV extends Serializable {
	
	/**
	 * Metodo per il recupero di tutti i job configurati in base dati.
	 * 
	 * @return	job recuperati
	 */
	List<JobConfigurationDTO> getAll();
	
}
