package it.ibm.red.business.service;

import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.service.facade.IRecogniformFacadeSRV;

/**
 * Interfaccia del servizio di gestione recogniform.
 */
public interface IRecogniformSRV extends IRecogniformFacadeSRV {

	/**
	 * Conta i documenti di classe POST_CENS e suoi allegati
	 * 
	 * @param fceh
	 * @param flagRegistroRiservato
	 * @param eliminati => solo quelli con stato = 1
	 * @return
	 */
	Integer getCountDocumentiCartacei(IFilenetCEHelper fceh, boolean flagRegistroRiservato, boolean eliminati);
	
}
