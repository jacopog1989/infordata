package it.ibm.red.business.service.concrete;
  
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IGestioneNotificheEmailDAO;
import it.ibm.red.business.dto.EventoSottoscrizioneDTO; 
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.service.INotificaMancataSpedizioneCartMistaSRV;
import it.ibm.red.business.service.ISottoscrizioniSRV;
import it.ibm.red.business.service.IStoricoSRV; 

/**
 * Service che viene utilizzato per l'invio della notifica ad un utente
 * sottoscritto nel caso di mancata spedizione mista o cartacea.
 * 
 * @author VINGENITO
 */
@Service
public class NotificaMancataSpedizioneCartMistaSRV extends NotificaWriteAbstractSRV implements INotificaMancataSpedizioneCartMistaSRV {

	/**
	 * Serial Version Costante
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * LOGGER
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NotificaMancataSpedizioneCartMistaSRV.class.getName());
	
	/**
	 * Service.
	 */
	@Autowired
	private IGestioneNotificheEmailDAO gestNotificheEmailDAO;
	 
	/**
	 * Service.
	 */
	@Autowired
	private ISottoscrizioniSRV sottoscrizioniSRV;
	
	/**
	 * Service.
	 */
	@Autowired
	private IStoricoSRV storicoSRV;

	/**
	 * @see it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV#getSottoscrizioni(int).
	 */
	@Override
	protected List<EventoSottoscrizioneDTO> getSottoscrizioni(int idAoo) {
		Connection con = null;
		List<EventoSottoscrizioneDTO> eventi = null;
		try {
			Integer[] idEventi = new Integer[]{
					Integer.parseInt(EventTypeEnum.NOTIFICA_MANCATA_SPEDIZIONE.getValue())};
		
			con = setupConnection(getFilenetDataSource().getConnection(), false);
			eventi = sottoscrizioniSRV.getEventiSottoscrittiByListaEventi(idAoo, idEventi, con);
			
		} catch (Exception e) {
			LOGGER.error("Errore in fase recupero delle sottoscrizioni per l'evento di mancata spedizione per l'aoo " + idAoo, e);
		} finally {
			closeConnection(con);
		}
		return eventi;
	}

	/**
	 * @see it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV#getDataDocumenti(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.persistence.model.Aoo, int).
	 */
	@Override
	protected List<InfoDocumenti> getDataDocumenti(UtenteDTO utente, Aoo aoo, int idEvento) {
		throw new RedException("Metodo non previsto per il servizio ");
	}

	/**
	 * @see it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV#getDataDocumenti(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.persistence.model.Aoo, int, java.lang.Object).
	 */
	@Override
	protected List<InfoDocumenti> getDataDocumenti(UtenteDTO utente, Aoo aoo, int idEvento, Object objectForGetDataDocument) {
		List<InfoDocumenti> documenti = new ArrayList<>();
 		try { 
 			String docTitle = (String) objectForGetDataDocument; 
 			
 			if(docTitle!=null) {
 				boolean inviaNotifica = storicoSRV.isDocLavoratoDaUfficio(utente.getIdAoo(),docTitle, utente.getIdUfficio());
 	 			if(inviaNotifica) {
 		 			InfoDocumenti info = new InfoDocumenti();
 		 			info.setIdDocumento(Integer.parseInt(docTitle));
 		 			documenti.add(info);
 	 			}
 			}
 		} catch (Exception e) {
			LOGGER.error("Errore ", e);
		}
		return documenti;
	}
	
	/**
	 * @see it.ibm.red.business.service.INotificaMancataSpedizioneCartMistaSRV#getNotificaMancataSpedCartMista(it.ibm.red.business.persistence.model.Aoo).
	 */
	@Override
	public void getNotificaMancataSpedCartMista(final Aoo aoo) {
		FilenetPEHelper fpeh = null; 
		Connection conn = null;
		
		try {
			conn = setupConnection(getDataSource().getConnection(), true); 
			
			AooFilenet aooFilenet = aoo.getAooFilenet();
			//inizializza accesso al process engine in modalità administrator
			fpeh = new FilenetPEHelper(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), 
					aooFilenet.getStanzaJaas(), aooFilenet.getConnectionPoint()); 
			   
			//Creo la query sul pe per prendere i doc in spedizione  
			Date dataCreazioneMinoreDi = null;
			if(aoo.getGiorniNotificaMancataSpedizioneCartacea()!=null && aoo.getGiorniNotificaMancataSpedizioneCartacea().intValue()!=0 ) {
				Date currentDate = new Date();
				Calendar c = Calendar.getInstance();
				c.setTime(currentDate);
				c.add(Calendar.DATE, -aoo.getGiorniNotificaMancataSpedizioneCartacea().intValue());
				dataCreazioneMinoreDi = c.getTime();
			}
		
			List<String> idDocRespSpediti = fpeh.fetchIdDocSpedizioneSpeditoJob(aooFilenet.getIdClientAoo(),dataCreazioneMinoreDi); 
			if(idDocRespSpediti!=null && !idDocRespSpediti.isEmpty()) {
				//Prima leggo dalla nuova tabella per vedere se ho record già processati andati in errore
				List<String> listaDaInviare = gestNotificheEmailDAO.getRecordGiaProcessati(idDocRespSpediti ,conn);
				//Poi accedo alla gestione notifiche email
				listaDaInviare = gestNotificheEmailDAO.getStatoRicevutaDiversoDa3(listaDaInviare, conn);
				if(listaDaInviare!=null && !listaDaInviare.isEmpty()) {
					for(String notifica : listaDaInviare) {
						processaNotifica(aoo, conn, notifica); 
					} 
					gestNotificheEmailDAO.deleteRecordProcessatiDaUnaSettimana(aoo.getIdAoo(), conn);
				}
			}  
			
			commitConnection(conn);
			
		} catch(Exception ex) {
			rollbackConnection(conn);
			LOGGER.error("Errore durante il recupero dei documenti spediti cartacei o misti con stato mancata spedizione : "+ex);
			throw new RedException("Errore durante il recupero dei documenti spediti cartacei o misti con stato mancata spedizione : "+ex);
		} finally { 
			closeConnection(conn); 
			logoff(fpeh); 
		}
	}

	/**
	 * @param aoo
	 * @param conn
	 * @param notifica
	 */
	private void processaNotifica(final Aoo aoo, Connection conn, String notifica) {
		try {
			writeNotifiche(aoo.getIdAoo().intValue(), notifica);
			//Stato 1 significa che il record è stato processato
			gestNotificheEmailDAO.deleteByIdAooAndDT(aoo.getIdAoo(), notifica, conn);
			gestNotificheEmailDAO.insertRecordProcessati(aoo.getIdAoo(), notifica, 1, conn); 
		} catch(Exception ex) {
			//Stato 0 significa che il record non è stato processato perchè in errore
			gestNotificheEmailDAO.deleteByIdAooAndDT(aoo.getIdAoo(), notifica, conn);
			gestNotificheEmailDAO.insertRecordProcessati(aoo.getIdAoo(), notifica, 0, conn);
			LOGGER.error("Errore durante il processamento della notifica", ex);
		}
	}
}
