package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Date;

import it.ibm.red.business.dto.EventoLogDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.EventTypeEnum;

/**
 * The Interface IEventoLogFacadeSRV.
 *
 * @author mcrescentini
 * 
 *         Facade EventoLog service.
 */
public interface IEventoLogFacadeSRV extends Serializable {
	
	/**
	 * Sposta/copia l'evento.
	 * @param utente
	 * @param idDocumento
	 * @param eventType
	 * @param idFascicoloProvenienza
	 * @param idFascicoloTarget
	 */
	void writeSpostaCopiaEventLog(UtenteDTO utente, int idDocumento, EventTypeEnum eventType, Integer idFascicoloProvenienza, Integer idFascicoloTarget);
	
	/**
	 * Predispone l'evento di risposta automatica.
	 * @param idUfficioMittente
	 * @param idUtenteMittente
	 * @param idDocumento
	 * @param motivazione
	 * @param idAoo
	 */
	void writePredisposizioneRispostaAutomaticaEventLog(Long idUfficioMittente, Long idUtenteMittente, int idDocumento, String motivazione, Long idAoo);

	/**
	 * Recupera dallo storico l'ultimo evento di chiusura del documento in ingresso (messa agli atti o preparazione alla risposta).
	 * 
	 * @param idDocumento
	 * @param idAoo
	 * @return ultimo evento di chiusura
	 */
	EventoLogDTO getLastChiusuraIngressoEvent(int idDocumento, int idAoo);

	/**
	 * Effettua l'inserimento dell'evento nel log.
	 * @param idDocumento
	 * @param idUfficioMittente
	 * @param idUtenteMittente
	 * @param idUfficioDestinatario
	 * @param idUtenteDestinatario
	 * @param dataAssegnazione
	 * @param eventType
	 * @param motivazioneAssegnazione
	 * @param idTipoAssegnazione
	 * @param idAoo
	 */
	void inserisciEventoLog(int idDocumento, Long idUfficioMittente, Long idUtenteMittente, Long idUfficioDestinatario,
			Long idUtenteDestinatario, Date dataAssegnazione, EventTypeEnum eventType, String motivazioneAssegnazione,
			int idTipoAssegnazione, Long idAoo);

}
