package it.ibm.red.business.dto;

import java.util.Date;
import java.util.List;

import it.ibm.red.business.enums.EventoCalendarioEnum;
import it.ibm.red.business.utils.DateUtils;

/**
 * DTO che definisce l'evento di ricerca.
 */
public class SearchEventiDTO {
	
	/**
	 * Data inizio.
	 */
	private Date dataInizio;
	
	/**
	 * Data fine.
	 */
	private Date dataFine;
	
	/**
	 * Lista tipi evento.
	 */
	private List<EventoCalendarioEnum> tipiEvento;
	
	/**
	 * Descrizione.
	 */
	private String descrizioneLike;
	
	/**
	 * Numero massimo risultati.
	 */
	private int maxResults; 
	
	/**
	 * Data inizio notifiche non lette.
	 */
	private Date dataInizioNotificheNonLette;
	
	/**
	 * Data fine notifiche non lette.
	 */
	private Date dataFineNotificheNonLette;
	
	/**
	 * Restituisce la data inizio della ricerca.
	 * @return data inizio
	 */
	public Date getDataInizio() {
		Date output = null;
		if (dataInizio != null) {
			output = DateUtils.dropTimeInfo(dataInizio);
		}
		return output;
	}
	
	/**
	 * Imposta la data inizio della ricerca.
	 * @param dataInizio
	 */
	public void setDataInizio(final Date dataInizio) {
		this.dataInizio = dataInizio;
	}
	
	/**
	 * Restituisce la data finale della ricerca.
	 * @return data fine
	 */
	public Date getDataFine() {
		return DateUtils.setDateTo2359(dataFine);
	}
	
	/**
	 * Imposta la data finale della ricerca.
	 * @param dataFine
	 */
	public void setDataFine(final Date dataFine) {
		this.dataFine = dataFine;
	}
	
	/**
	 * Restituisce la lista dei tipi di evento.
	 * @return lista tipi evento
	 */
	public List<EventoCalendarioEnum> getTipiEvento() {
		return tipiEvento;
	}
	
	/**
	 * Imposta la lista dei tipi di evento.
	 * @param tipiEvento
	 */
	public void setTipiEvento(final List<EventoCalendarioEnum> tipiEvento) {
		this.tipiEvento = tipiEvento;
	}
	
	/**
	 * Restituisce la descrizione da inserire nella clausula LIKE di SQL.
	 * @return descrizione da ricercare
	 */
	public String getDescrizioneLike() {
		return descrizioneLike;
	}
	
	/**
	 * Imposta la descrizione da inserire nella clausula LIKE di SQL.
	 * @param descrizioneLike
	 */
	public void setDescrizioneLike(final String descrizioneLike) {
		this.descrizioneLike = descrizioneLike;
	}
	
	/**
	 * Restituisce il numero massimo dei risultati.
	 * @return numero massimo risultati
	 */
	public int getMaxResults() {
		return maxResults;
	}
	
	/**
	 * Imposta il numero massimo dei risultati.
	 * @param maxResults
	 */
	public void setMaxResults(final int maxResults) {
		this.maxResults = maxResults;
	}

	/**
	 * Restituisce la data inizio come String, rimuovendo l'orario.
	 * @return data inizio come String
	 */
	public String getDataInizioString() {
		return DateUtils.dateToString(dataInizio, true);
	}
	
	/**
	 * Imposta la data fine come String, rimuovendo l'orario.
	 * @return data fine come String
	 */
	public String getDataFineString() {
		return DateUtils.dateToString(dataFine, true);
	}
	
	/**
	 * Restituisce la data inizio delle notifiche non lette.
	 * @return data inizio notifiche da leggere
	 */
	public Date getDataInizioNotificheNonLette() {
		return dataInizioNotificheNonLette;
	}

	/**
	 * Imposta la data inizio delle notifiche non lette.
	 * @param dataInizioNotificheNonLette
	 */
	public void setDataInizioNotificheNonLette(final Date dataInizioNotificheNonLette) {
		this.dataInizioNotificheNonLette = dataInizioNotificheNonLette;
	}

	/**
	 * Restituisce la data finale delle notifiche non lette.
	 * @return data fine notifiche da leggere
	 */
	public Date getDataFineNotificheNonLette() {
		return dataFineNotificheNonLette;
	}

	/**
	 * Imposta la data finale delle notifiche non lette.
	 * @param dataFineNotificheNonLette
	 */
	public void setDataFineNotificheNonLette(final Date dataFineNotificheNonLette) {
		this.dataFineNotificheNonLette = dataFineNotificheNonLette;
	}
}