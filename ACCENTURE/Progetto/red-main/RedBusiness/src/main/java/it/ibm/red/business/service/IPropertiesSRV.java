package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IPropertiesFacadeSRV;

/**
 * The Interface IPropertiesSRV.
 *
 * @author CPIERASC
 * 
 *         Servizio per la gestione delle property.
 */
public interface IPropertiesSRV extends IPropertiesFacadeSRV {
	
}
