package it.ibm.red.business.service.facade;

import java.io.Serializable;

/**
 * The Interface IEmailBckFacadeSRV.
 *
 * @author mcrescentini
 * 
 * Facade EmailBck service.
 */
public interface IEmailBckFacadeSRV extends Serializable {
	
	
}
