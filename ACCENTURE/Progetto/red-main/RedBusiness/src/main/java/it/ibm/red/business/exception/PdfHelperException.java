package it.ibm.red.business.exception;

/**
 * The Class PdfHelperException.
 *
 * @author CPIERASC
 * 
 *         Eccezione generata in fase di gestione del pdf.
 */
public class PdfHelperException extends RuntimeException {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Costruttore.
	 * 	
	 * @param msg	messaggio
	 */
	public PdfHelperException(final String msg) {
		super(msg);
	}
	
	/**
	 * Costruttore.
	 * 	
	 * @param e	eccezione
	 */
	public PdfHelperException(final Exception e) {
		super(e);
	}
}
