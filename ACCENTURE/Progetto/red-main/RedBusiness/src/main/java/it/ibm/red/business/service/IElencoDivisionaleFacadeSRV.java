package it.ibm.red.business.service;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.ParamsRicercaElencoDivisionaleDTO;
import it.ibm.red.business.dto.RisultatoRicercaElencoDivisionaleDTO;

/**
 * Facade del service @see ElencoDivisionaleSRV.
 * 
 * @author SimoneLungarella
 */
public interface IElencoDivisionaleFacadeSRV extends Serializable {

	/**
	 * Restituisce il byte array per la generazione del pdf che permette la
	 * visualizzazione del report generato dai parametri di ricerca in ingresso.
	 * 
	 * @param paramsRicerca
	 *            parametri della ricerca
	 * @param results
	 *            risultati della ricerca
	 * @param descrizioneAoo
	 *            descrizione dell'AOO dell'utente che ha generato il report
	 * @return byte array del pdf generato, null se la trasformazione non ha avuto
	 *         esito positivo
	 */
	byte[] generaPdfReport(ParamsRicercaElencoDivisionaleDTO paramsRicerca, List<RisultatoRicercaElencoDivisionaleDTO> results, String descrizioneAoo);
}
