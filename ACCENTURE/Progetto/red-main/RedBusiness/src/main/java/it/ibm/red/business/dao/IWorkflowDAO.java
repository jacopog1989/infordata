package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;

import it.ibm.red.business.dto.TipoProcedimentoDTO;
import it.ibm.red.business.persistence.model.Workflow;

/**
 * 
 * @author m.crescentini
 *
 *	Interfaccia dao per la gestione dei workflow su DB.
 */
public interface IWorkflowDAO extends Serializable {
	
	/**
	 * Recupero dei workflow.
	 * 
	 * @param connection	connessione al db
	 * @return				lista dei workflow
	 */
	Collection<Workflow> getAll(Connection connection);

	/**
	 * Ottiene il workflow tramite l'id.
	 * @param connection
	 * @param idWorkflow
	 * @return workflow
	 */
	Workflow getById(Connection connection, int idWorkflow);
	
	/**
	 * Ottiene il workflow dal procedimento.
	 * @param procedimento
	 * @param connection
	 * @return id del workflow
	 */
	int getWorkflowId(TipoProcedimentoDTO procedimento, Connection connection);
}
