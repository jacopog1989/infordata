package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IASignOrchestratorFacade;

/**
 * Interfaccia orchestrator di firma asincrona.
 */
public interface IASignOrchestrator extends IASignOrchestratorFacade {
}
