package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV;

/**
 * Interfaccia del servizio che gestisce le tipologia documenti.
 */
public interface ITipologiaDocumentoSRV extends ITipologiaDocumentoFacadeSRV {

	/**
	 * Restituisce la lista delle Tipologie Documento ATTIVE, filtrate per tipo categoria e dati utente.
	 * 
	 * @param idAoo
	 *            id aoo per cui filtrare.
	 * 
	 * @param idUfficio
	 *            Corrisponde all'idNodo.
	 * 
	 * @param idTipoCategoria
	 *            Si differenzia in entrata o in uscita.
	 * 
	 * @param connection
	 *            Connessione al db.
	 * @return
	 */
	List<TipologiaDocumentoDTO> getComboTipologieDocumentoAttive(Long idAoo, Long idUfficio, Integer idTipoCategoria, Connection connection);
	
	/**
	 * Restituisce la lista delle Tipologie Documento DISATTIVE, filtrate per tipo categoria e dati utente.
	 * 
	 * @param idAoo
	 *            id aoo per cui filtrare.
	 * 
	 * @param idUfficio
	 *            Corrisponde all'idNodo.
	 * 
	 * @param idTipoCategoria
	 *            Si differenzia in entrata o in uscita.
	 * 
	 * @param connection
	 *            Connessione al db.
	 * @return
	 */
	List<TipologiaDocumentoDTO> getComboTipologieDocumentoDisattive(Long idAoo, Long idUfficio, Integer idTipoCategoria, Connection connection);

	/**
	 * Recupera l'insieme delle tipologie documento filtrate per i parametri in ingresso.
	 * 
	 * Una tipologia documento è attiva quando la sua data di disattivazione è
	 * antecedente alla data odierna. Le date nulle vengono ignorate.
	 * 
	 * @param idAoo
	 *            id aoo per cui filtrare.
	 * @param idUfficio
	 *            Corrisponde all'idNodo.
	 * @param idTipoCategoria
	 *            Si differenzia in entrata o in uscita.
	 * @param idTipologiaDocumento
	 *            Se non è null allora viene conservato nella selezione l'idTipologiaDocumento
	 * @param connection
	 *            Connessione al db.
	 * @return Una lista filtrata di tipologie documenti attive.
	 */
	List<TipologiaDocumentoDTO> getComboTipologieByUtenteAndTipoCategoriaAndTipologiaSelezionataNoAutomatici(Long idAoo, Long idUfficio,
			Integer idTipoCategoria, Integer idTipologiaDocumento, Connection connection);

	/**
	 * @param idTipologiaDocumento
	 * @param connection
	 * @return
	 */
	TipologiaDocumentoDTO getById(Integer idTipologiaDocumento, Connection connection);

	/**
	 * Seleziona la tipologia di default dalle properties o, se non valorizzato,
	 * seleziona la prima della lista delle tipologie.
	 * 
	 * @param idAoo
	 * @param codiceAoo
	 * @param tipoCategoriaDocumento
	 * @param con
	 * @return
	 */
	Integer getTipologiaDocumentalePredefinita(Long idAoo, String codiceAoo, TipoCategoriaEnum tipoCategoriaDocumento, Connection con);

	/**
	 * Recupera i tipi documento attivi e disattivi e li restituisce all'interno della mappa in output.
	 * 
	 * @param idCategoriaDocumento
	 * @param utente
	 * @param connection
	 * @return
	 */
	Map<ContextTrasformerCEEnum, Object> getContextTipologieDocumento(Integer idCategoriaDocumento,
			UtenteDTO utente, Connection connection);

	/**
	 * @param tipoCategoria
	 * @param idAoo
	 * @param connection
	 * @return
	 */
	List<TipologiaDocumentoDTO> getComboTipologieByAooAndTipoCategoriaAttivi(TipoCategoriaEnum tipoCategoria, Long idAoo, Connection connection);

}