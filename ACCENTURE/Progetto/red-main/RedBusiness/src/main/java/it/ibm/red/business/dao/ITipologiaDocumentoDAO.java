package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.DatiDocumentoContiConsuntiviDTO;
import it.ibm.red.business.dto.DatiNotificaContiConsuntiviDTO;
import it.ibm.red.business.dto.EstrattiContoUcbDTO;
import it.ibm.red.business.dto.KeyValueDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.ParamsRicercaContiConsuntiviDTO;
import it.ibm.red.business.dto.ParamsRicercaEstrattiContoDTO;
import it.ibm.red.business.dto.RisultatoRicercaContiConsuntiviDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.TipologiaRegistroNsdDTO;

/**
 * 
 * @author CPIERASC
 *
 *         Interfaccia dao tipo procedimento.
 */
public interface ITipologiaDocumentoDAO extends Serializable {

	/**
	 * Ottiene la tipologia documento tramite l'id.
	 * @param idTipologiaDocumento
	 * @param connection
	 * @return tipologia documento
	 */
	TipologiaDocumentoDTO getById(int idTipologiaDocumento, Connection connection);

	/**
	 * @param idTipologiaDocumento
	 * @param connection
	 * @return
	 */
	String getClasseDocumentaleByIdTipologia(int idTipologiaDocumento, Connection connection);

	/**
	 * @param idAoo
	 * @param idTipoCategoria
	 * @param connection
	 * @return
	 */
	List<TipologiaDocumentoDTO> getComboTipologieByAooAndTipoCategoria(Long idAoo, Integer idTipoCategoria,
			Connection connection);

	/**
	 * @param idAoo
	 * @param connection
	 * @return
	 */
	List<TipologiaDocumentoDTO> getComboTipologieByAoo(Long idAoo, Connection connection);

	/**
	 * @param descrizioneTipologiaDocumento
	 * @param idAoo
	 * @param connection
	 * @return
	 */
	List<Integer> getIdsByDescrizione(String descrizioneTipologiaDocumento, Long idAoo, Connection connection);

	/**
	 * @param classeDocumentale
	 * @param idAoo
	 * @param idTipologiaDocumento
	 * @param connection
	 * @return
	 */
	List<TipologiaDocumentoDTO> getTipologiaByClasseDocumentaleAndAooAndidTipologiaDocumento(String classeDocumentale,
			Long idAoo, int idTipologiaDocumento, Connection connection);

	/**
	 * @param con
	 * @return
	 */
	List<TipologiaDocumentoDTO> getTipologieDocumentiAggiuntivi(Connection con);

	/**
	 * @param onlyAttivi
	 * @param onlyDisattivi
	 * @param escludiAutomatici
	 * @param idAoo
	 * @param idUfficio
	 * @param idTipoCategoria
	 * @param connection
	 * @return
	 */
	List<TipologiaDocumentoDTO> getComboTipologieByUtenteAndTipoCategoria(boolean onlyAttivi, boolean onlyDisattivi, 
			boolean escludiAutomatici, Long idAoo, Long idUfficio, Integer idTipoCategoria, Connection connection);

	/**
	 * @param nomeTipoDocumento
	 * @param tipoGestione
	 * @param idAoo
	 * @param idTipoCategoria
	 * @param procedimenti
	 * @param connection
	 */
	int save(String nomeTipoDocumento, String tipoGestione, long idAoo, int idTipoCategoria, Connection connection);

	/**
	 * @param con
	 * @param metadatiEstesi
	 * @param ids
	 * @param idAoo
	 * @param descTipologiaDocumento
	 * @param descTipoProcedimento
	 * @return
	 */
	HashMap<String, String> ricercaMetadatiEstesi(Connection con, Collection<MetadatoDTO> metadatiEstesi, List<String> ids, 
			Long idAoo, String descTipologiaDocumento, String descTipoProcedimento);
	
	/**
	 * @param con
	 * @param idDocumento
	 * @param idAoo
	 * @return dataCreazione
	 */
	Date cancellaMetadatiEstesi(Connection con, Integer idDocumento, Long idAoo);
	
	/**
	 * @param con
	 * @param xmlMetadatiEstesi
	 * @param idDocumento
	 * @param idAoo
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @param dataCreazione
	 */
	void salvaMetadatiEstesi(Connection con, String xmlMetadatiEstesi, Integer idDocumento, Long idAoo,
			Long idTipologiaDocumento, Long idTipoProcedimento, Date dataCreazione);
	
	/**
	 * @param con
	 * @param idDocumento
	 * @param idAoo
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @return
	 */
	String recuperaMetadatiEstesi(Connection con, Integer idDocumento, Long idAoo, Long idTipologiaDocumento, Long idTipoProcedimento);

	/**
	 * @param idDocumenti
	 * @param con
	 * @return
	 */
	Map<String, String> recuperaMetadatiEstesi(List<String> idDocumenti, Connection con);
	
	/**
	 * @param idUfficio
	 * @param idUfficioPadre
	 * @param conn
	 * @return
	 */
	List<TipologiaRegistroNsdDTO> getTipologiaDocumentoNsd(Long idUfficio, Long idUfficioPadre, Connection conn);
	
	/**
	 * Restituisce l'id della tipologia documento recuperato dalla descrizione:
	 * <code> nomeTipologiaDocumento </code> e dalla categoria
	 * <code> idCategoriaDocumento </code> dove la categoria indica entrata o
	 * uscita.
	 * 
	 * @param nomeTipologiaDocumento
	 *            descrizione della tipologia documento
	 * @param idCategoriaDocumento
	 *            id del tipo categoria, @see TipoCategoriaEnum.
	 * @param idAoo
	 *            identificativo AOO
	 * @return identificativo tipologia documento
	 */
	Integer getIdTipologiaDocumentoByDescrizioneAndTipoCategoria(String nomeTipologiaDocumento, Integer idCategoriaDocumento, Long idAoo, Connection con);

	/**
	 * @param idTipologiaDocumento
	 * @param connection
	 * @return
	 */
	String getDescTipologiaDocumentoById(Integer idTipologiaDocumento, Connection connection);
	
	/**
	 * @param conn
	 * @param idAOO
	 * @return
	 */
	List<TipologiaRegistroNsdDTO> getTipologiaRegistroNsd(Connection conn, Long idAOO);

	/**
	 * Esegue la ricerca degli estratti conto per l'area organizzativa UCB
	 * identificata dall' {@code idAoo}.
	 * 
	 * @param tipoDoc
	 *            identificativo della tipologia del documento da ricercare
	 * @param tipoProc
	 *            identificativo della tipologia del procedimento da ricercare
	 * @param estrattiContoUCB
	 *            i parametri della ricerca
	 * @param con
	 *            connessione al database
	 * @return i risultati della ricerca
	 */
	Collection<EstrattiContoUcbDTO> ricercaEstrattiContoUcb(Integer idAoo, Integer tipoDoc, Integer tipoProc, ParamsRicercaEstrattiContoDTO estrattiContoUCB, Connection con);

	/**
	 * Metodo che esegue la ricerca per metadati dei documenti di tipo Conti
	 * consuntivi Sede Estera.
	 * 
	 * @param idAoo
	 *            identificativo dell'area organizzativa
	 * @param tipoDoc
	 *            identificativo della tipologia documento
	 * @param tipoProc
	 *            identificativo della tipologia procedimento
	 * @param contiConsuntiviUCB
	 *            parametri della ricerca
	 * @param documentiList
	 * @param notificheList
	 * @param documentiNotificheMap
	 * @param connection
	 *            connessione al database
	 * @return risultati della ricerca
	 */
	Map<Integer, RisultatoRicercaContiConsuntiviDTO> ricercaMetadatiContiConsuntivi (Integer idAoo, Integer tipoDoc, Integer tipoProc, ParamsRicercaContiConsuntiviDTO contiConsuntiviUCB,
			List<Integer> documentiList, List<Integer> notificheList, Map<Integer, Integer> documentiNotificheMap, Connection connection);
	
	/**
	 * Metodo che esegue la ricerca per metadati dei documenti di tipo Conti
	 * consuntivi Sede Estera Da Flusso.
	 * 
	 * @param idAoo
	 *            identificativo dell'area organizzativa
	 * @param tipoDocFlusso
	 *            identificativo tipologia documento da flusso
	 * @param tipoProcFlusso
	 *            identificativo tipologia procedimento da flusso
	 * @param contiConsuntiviUCB
	 *            parametri della ricerca
	 * @param documentiList
	 * @param notificheList
	 * @param documentiNotificheMap
	 * @param connection
	 *            connessione al database
	 * @return risultati della ricerca
	 */
	Map<Integer, RisultatoRicercaContiConsuntiviDTO> ricercaMetadatiContiConsuntiviFlusso (Integer idAoo, Integer tipoDocFlusso, Integer tipoProcFlusso, ParamsRicercaContiConsuntiviDTO contiConsuntiviUCB,
			List<Integer> documentiList, List<Integer> notificheList, Map<Integer, Integer> documentiNotificheMap, Connection connection);
	
	/**
	 * Ricerca i dati dei documenti.
	 * 
	 * @param idAoo
	 * @param documentiList
	 * @param dwhCon
	 * @return mappa id documento dati del documento
	 */
	Map<Integer, DatiDocumentoContiConsuntiviDTO> ricercaDatiDocumenti(Integer idAoo, List<Integer> documentiList, Connection dwhCon);

	/**
	 * Ricerca i dati delle notifiche.
	 * 
	 * @param idAoo
	 * @param notificheList
	 * @param dwhCon
	 * @return mappa id notifica dati della notifica
	 */
	Map<Integer, DatiNotificaContiConsuntiviDTO> ricercaDatiNotifiche(Integer idAoo, List<Integer> notificheList, Connection dwhCon);
	
	/**
	 * Ricerca i metadati per conti giudiziali.
	 * 
	 * @param idAoo
	 * @param tipoDoc
	 * @param tipoProc
	 * @param tipoDocFlusso
	 * @param tipoProcFlusso
	 * @param tipoCont
	 * @param ageCont
	 * @param esercizio
	 * @param con
	 * @return
	 */
	List<String> ricercaMetadatiContiGiudizialiUcb(Integer idAoo, Integer tipoDoc, Integer tipoProc, Integer tipoDocFlusso, Integer tipoProcFlusso, 
			String tipoCont, String ageCont, String esercizio, Connection con);

	/**
	 * Recupera le tipologie documento note ad NPS.
	 * 
	 * @param idAoo
	 * @param con
	 * @return
	 */
	List<KeyValueDTO> getTipologieDocumentoNPS(Long idAoo, Connection con);
}