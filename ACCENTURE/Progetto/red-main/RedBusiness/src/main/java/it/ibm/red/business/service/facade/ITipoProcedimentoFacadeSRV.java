package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.TipoProcedimentoDTO;
import it.ibm.red.business.persistence.model.TipoProcedimento;

/**
 * The Interface ITipoProcedimentoFacadeSRV.
 *
 * @author CPIERASC
 * 
 *         Facade per la gestione dei tipi procedimento.
 */
public interface ITipoProcedimentoFacadeSRV extends Serializable {
	
	/**
	 * Recupero di tutti i tipi procedimento.
	 * 
	 * @return	lista tipi procedimento
	 */
	Collection<TipoProcedimentoDTO> getAll();
	
	/**
	 * Restituisce il tipo procedimento identificato dall'
	 * <code> idProcedimento </code>.
	 * 
	 * @param idProcedimento
	 *            identificativo tipo procedimento
	 * @return tipo porcedimento recuperato se esistente, null altrimenti
	 */
	TipoProcedimentoDTO getTipoProcedimentoById(Integer idProcedimento);

	/**
	 * Restituisce le tipologie dei procedimenti associati ad una tipologia
	 * documento identificata dall' <code> idTipologiaDocumento </code>.
	 * 
	 * @param idTipologiaDocumento
	 *            identificativo tipologia documento
	 * @return lista delle tipologia procedimento recuperate
	 */
	List<TipoProcedimento> getTipiProcedimentoByTipologiaDocumento(Integer idTipologiaDocumento);

	/**
	 * Esegue il salvataggio sulla base dati di una tipologia procedimento.
	 * 
	 * @param procedimento
	 *            caratteristiche del procedimento da salvare
	 * @return identificativo del tipo procedimento salvato
	 */
	Long save(TipoProcedimentoDTO procedimento);
	
	/**
	 * @param idAoo
	 * @return
	 */
	Collection<TipoProcedimentoDTO> getTipiProcedimentoByIdAoo(Long idAoo);
	
	/**
	 * Restituisce l'identificativo del tipo procedimento associato al procedimento
	 * caratterizzate dalla descrizione definita da <code> descrizione </code> e
	 * associato alla tipologia documento definita da
	 * <code> idTipologiaDocumento </code>.
	 * 
	 * @param descrizione
	 *            descrizione tipo procedimento
	 * @param idTipologiaDocumento
	 *            identificativo univoco tipologia documento
	 * @return identificativo tipo procedimento recuperato se esistente,
	 *         <code> null </code> altrimenti
	 */
	Long getIdTipoProcedimentoByDescrizioneAndTipologiaDocumento(String descrizione, Integer idTipologiaDocumento);

	/**
	 * Ottiene la descrizione del tipo procedimento tramite l'id.
	 * @param idTipoProcedimento
	 * @return descrizione del tipo procedimento
	 */
	String getDescTipoProcedimentoById(Integer idTipoProcedimento);

}
