package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class SecurityWsDTO.
 *
 * @author m.crescentini
 * 
 * 	Data Transfer Object per le security FileNet utilizzate dai web service di RED Evo.
 */
public class SecurityWsDTO extends AbstractDTO {

	private static final long serialVersionUID = 4411102729767151607L;

	/**
	 * Oggetto gruppo.
	 */
	private GruppoWs gruppo;

	/**
	 * Oggetto utente.
	 */
	private UtenteWs utente;

	/**
	 * Costruttore.
	 */
	public SecurityWsDTO() {
		this.utente = new UtenteWs();
		this.gruppo = new GruppoWs();
	}

	/**
	 * Getter.
	 * 
	 * @return	gruppo
	 */
	public final GruppoWs getGruppo() {
		return gruppo;
	}

	/**
	 * Setter.
	 * 
	 * @param inGruppo	gruppo
	 */
	public final void setGruppo(final GruppoWs inGruppo) {
		this.gruppo = inGruppo;
	}

	/**
	 * Getter utente.
	 * 	
	 * @return	utente
	 */
	public final UtenteWs getUtente() {
		return utente;
	}

	/**
	 * Setter.
	 * 
	 * @param inUtente	utente
	 */
	public final void setUtente(final UtenteWs inUtente) {
		this.utente = inUtente;
	}
	
	/**
	 * The Class GruppoWs.
	 *
	 * @author m.crescentini
	 * 
	 * 	DTO gruppo.
	 */
	public class GruppoWs implements Serializable {

		/**
		 * The Constant serialVersionUID.
		 */
		private static final long serialVersionUID = 1L;
		
		/**
		 * Identificativo ufficio.
		 */
		private int idUfficio;

		/**
		 * Identificativo siap.
		 */
		private int idSiap;
		
		/**
		 * Descrizione.
		 */
		private String descrizione;
		
		/**
		 * Identificativo ufficio padre.
		 */
		private int idUfficioPadre;
		
		
		/**
		 * Costruttore.
		 */
		public GruppoWs() {
			//Metodo intenzionalmente lasciato vuoto.
		}

		/**
		 * Getter.
		 * 
		 * @return	identificativo ufficio
		 */
		public final int getIdUfficio() {
			return idUfficio;
		}

		/**
		 * Setter.
		 * 
		 * @param inIdUfficio	identificativo ufficio
		 */
		public final void setIdUfficio(final int inIdUfficio) {
			this.idUfficio = inIdUfficio;
		}

		/**
		 * Getter.
		 * 
		 * @return	identificativo siap
		 */
		public final int getIdSiap() {
			return idSiap;
		}

		/**
		 * Setter.
		 * 
		 * @param inIdSiap	identificativo siap
		 */
		public final void setIdSiap(final int inIdSiap) {
			this.idSiap = inIdSiap;
		}

		/**
		 * @return the descrizione
		 */
		public String getDescrizione() {
			return descrizione;
		}

		/**
		 * @param descrizione the descrizione to set
		 */
		public void setDescrizione(final String descrizione) {
			this.descrizione = descrizione;
		}

		/**
		 * @return the idUfficioPadre
		 */
		public int getIdUfficioPadre() {
			return idUfficioPadre;
		}

		/**
		 * @param idUfficioPadre the idUfficioPadre to set
		 */
		public void setIdUfficioPadre(final int idUfficioPadre) {
			this.idUfficioPadre = idUfficioPadre;
		}
		
	}
	
	/**
	 * The Class UtenteWs.
	 *
	 * @author m.crescentini
	 * 
	 * 	DTO utente.
	 */
	public class UtenteWs implements Serializable {

		/**
		 * The Constant serialVersionUID.
		 */
		private static final long serialVersionUID = 1L;
		
		/**
		 * Identificativo utente.
		 */
		private int idUtente;
		
		/**
		 * Username.
		 */
		private String username;
		
		/**
		 * Identificativo siap.
		 */
		private int idSiap;
		
		/**
		 * Codice fiscale.
		 */
		private String codFis;
		
		/**
		 * Cognome.
		 */
		private String cognome;
		
		/**
		 * Nome.
		 */
		private String nome;
		
		/**
		 * Data disattivazione.
		 */
		private Date dataDisattivazione;
		
		/**
		 * E-mail
		 */
		private String email;
		

		/**
		 * Costruttore.
		 */
		public UtenteWs() {
			//Metodo intenzionalmente lasciato vuoto.
		}

		/**
		 * Getter.
		 * 
		 * @return	username
		 */
		public final String getUsername() {
			return username;
		}

		/**
		 * Setter.
		 * 
		 * @param inUsername	username
		 */
		public final void setUsername(final String inUsername) {
			this.username = inUsername;
		}

		/**
		 * Getter identificativo utente.
		 * 
		 * @return	identificativo utente
		 */
		public final int getIdUtente() {
			return idUtente;
		}
		
		/**
		 * Setter.
		 * 
		 * @param inIdUtente	identificativo utente
		 */
		public final void setIdUtente(final int inIdUtente) {
			this.idUtente = inIdUtente;
		}

		/**
		 * @return the idSiap
		 */
		public int getIdSiap() {
			return idSiap;
		}

		/**
		 * @param idSiap the idSiap to set
		 */
		public void setIdSiap(final int idSiap) {
			this.idSiap = idSiap;
		}

		/**
		 * @return the codFis
		 */
		public String getCodFis() {
			return codFis;
		}

		/**
		 * @param codFis the codFis to set
		 */
		public void setCodFis(final String codFis) {
			this.codFis = codFis;
		}

		/**
		 * @return the cognome
		 */
		public String getCognome() {
			return cognome;
		}

		/**
		 * @param cognome the cognome to set
		 */
		public void setCognome(final String cognome) {
			this.cognome = cognome;
		}

		/**
		 * @return the nome
		 */
		public String getNome() {
			return nome;
		}

		/**
		 * @param nome the nome to set
		 */
		public void setNome(final String nome) {
			this.nome = nome;
		}

		/**
		 * @return the dataDisattivazione
		 */
		public Date getDataDisattivazione() {
			return dataDisattivazione;
		}

		/**
		 * @param dataDisattivazione the dataDisattivazione to set
		 */
		public void setDataDisattivazione(final Date dataDisattivazione) {
			this.dataDisattivazione = dataDisattivazione;
		}

		/**
		 * @return the email
		 */
		public String getEmail() {
			return email;
		}

		/**
		 * @param email the email to set
		 */
		public void setEmail(final String email) {
			this.email = email;
		}
		
	}

}
