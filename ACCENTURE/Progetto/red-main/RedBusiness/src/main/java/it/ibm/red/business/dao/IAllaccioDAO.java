package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.enums.TipoAllaccioEnum;

/**
 * 
 * Dao allaccio.
 * 
 * @author m.crescentini
 *
 */
public interface IAllaccioDAO extends Serializable {

	/**
	 * Inserisce l'allaccio.
	 * 
	 * @param idDocumento
	 *            - id del documento
	 * @param idAoo
	 *            - id dell'Aoo
	 * @param idDocumentoAllacciato
	 *            - id del documento allacciato
	 * @param idTipoAllaccio
	 *            - id del tipo di allaccio
	 * @param documentoDaChiudere
	 *            - documento da chiudere
	 * @param principale
	 * @param connection
	 * @return Esito dell'update.
	 */
	int insert(int idDocumento, Long idAoo, Integer idDocumentoAllacciato, Integer idTipoAllaccio,
			Boolean documentoDaChiudere, boolean principale, Connection connection);

	/**
	 * Recupera tutti gli allacci a partire dal documento in uscita
	 * 
	 * @param idDocumento
	 * @param idAoo
	 * @param con
	 * @return Allacci associati al documento identificato dal {@code idDocumento}.
	 */
	List<RispostaAllaccioDTO> getDocumentiRispostaAllaccio(int idDocumento, int idAoo, Connection con);

	/**
	 * Recupera tutti gli allacci a partire dal documento in ingresso
	 * 
	 * @param idDocumento
	 * @param con
	 * @return Allacci associati al documento identificato dal {@code idDocumento}.
	 */
	Collection<RispostaAllaccioDTO> getDocumentiRispostaAllaccioUscita(int idDocumento, Connection con);

	/**
	 * Metodo per verificare se un documento ha allacci.
	 * 
	 * @param documentTitle
	 *            document title del documento
	 * @param idAoo
	 *            identificativo aoo
	 * @param connection
	 *            connessione
	 * @return numero di allacci
	 */
	Boolean hasAllacci(String documentTitle, Long idAoo, Connection connection);

	/**
	 * Chiude l'allaccio principale per il documento.
	 * 
	 * @param idDocumento
	 *            - id del documento
	 * @param idAoo
	 *            - id dell'Aoo
	 * @param connection
	 * @return Esito dell'update.
	 */
	int chiudiAllaccioPrincipale(int idDocumento, Long idAoo, Connection connection);

	/**
	 * Ottiene gli allacci partendo da un set di DocumentTitle.
	 * 
	 * @param docsUscita
	 *            - documenti in uscita
	 * @param idAoo
	 *            - id dell'Aoo
	 * @param connection
	 * @return Allacci principali recuperati.
	 */
	Collection<String> getAllacciPrincipaliByDocumentTitle(Collection<String> docsUscita, Long idAoo,
			Connection connection);

	/**
	 * Recupera l'ultimo allaccio principale di tipo richiesta integrazioni
	 * associato al documento in ingresso in input.
	 * 
	 * @param idDocIngresso
	 * @param connection
	 * @return Ultima richiesta integrazioni se esiste, {@code null} altrimenti.
	 */
	RispostaAllaccioDTO getLastRichiestaIntegrazioni(int idDocIngresso, Connection connection);

	/**
	 * Recupera l'ultimo allaccio principale di tipo visto o osservazione associato
	 * al documento in ingresso in input.
	 * 
	 * @param idDocIngresso
	 * @param connection
	 * @return Risposta allaccio recuperata.
	 */
	RispostaAllaccioDTO getLastRichiestaVistoOsservazione(int idDocIngresso, Connection connection);

	/**
	 * Recupera l'allaccio principale associato al documento in uscita in input.
	 * 
	 * @param idDocUscita
	 * @param connection
	 * @return Risposta allaccio recuperata.
	 */
	RispostaAllaccioDTO getAllaccioPrincipale(int idDocUscita, Connection connection);

	/**
	 * Restituisce la lista degli allacci identificati da un document title e dalla
	 * tipologia della notifica.
	 * 
	 * @param documentTitle
	 *            Identificativo del documento.
	 * @param tipoAllaccio
	 *            Tipologia dell'allaccio da recuperare.
	 * @param connection
	 *            Connessione al database.
	 * @return Lista di RispostaAllaccio associate al documento.
	 */
	List<RispostaAllaccioDTO> getAllacciOfType(String documentTitle, TipoAllaccioEnum tipoAllaccio,
			Connection connection);
}
