package it.ibm.red.business.persistence.model;

import java.io.Serializable;

/**
 * The Class Ente.
 *
 * @author CPIERASC
 * 
 *         Entità che mappa la tabella ENTE.
 */
public class Ente implements Serializable {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Identificativo ente.
	 */
	private int idEnte;

	/**
	 * Descrizione.
	 */
	private String descrizione;
	
	/**
	 * Costruttore.
	 */
	public Ente() {
		super();
	}

	/**
	 * Costruttore.
	 * 
	 * @param inIdEnte				identificativo ente
	 * @param inDescrizione			descrizione
	 */
	public Ente(final int inIdEnte, final String inDescrizione) {
		idEnte = inIdEnte;
		descrizione = inDescrizione;
	}

	/**
	 * Getter.
	 * 	
	 * @return	identificativo ente.
	 */
	public int getIdEnte() {
		return idEnte;
	}

	/**
	 * Getter descrizione.
	 * 
	 * @return	descrizione
	 */
	public final String getDescrizione() {
		return descrizione;
	}

}
