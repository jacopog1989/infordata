package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.TitolarioDTO;

/**
 * @author SLac
 *
 */
public interface ITitolarioDAO extends Serializable {

	/**
	 * @param idAOO
	 * @param idNodo
	 * @param connection
	 * @return
	 */
	List<TitolarioDTO> getFigliRadice(Long idAOO, Long idNodo, Connection connection);
	
	/**
	 * @param idAOO
	 * @param indice
	 * @param connection
	 * @return
	 */
	List<TitolarioDTO> getFigliByIndiceNonDisattivati(Long idAOO, String indice, Connection connection);
	
	
	/**
	 * @param idAOO
	 * @param indice
	 * @param connection
	 * @return
	 */
	TitolarioDTO getNodoByIndice(Long idAOO, String indice, Connection connection);
	
	/**
	 * Chiamata nell'autocomplete.
	 * 
	 * @param idAOO
	 * @param indicePartenza
	 * @param parola
	 * @param connection
	 * @return
	 */
	List<TitolarioDTO> getFigliNodoTitolarioAutocomplete(Long idAOO, String indicePartenza, String parola, Connection connection);

	/**
	 * Ottiene i figli radice non disattivati.
	 * @param idAOO
	 * @param idNodo
	 * @param connection
	 * @return lista di titolari
	 */
	List<TitolarioDTO> getFigliRadiceNonDisattivati(Long idAOO, Long idNodo, Connection connection);

	/**
	 * Ottiene i figli non disattivati tramite l'indice.
	 * @param idAOO
	 * @param indice
	 * @param connection
	 * @return lista di titolari
	 */
	List<TitolarioDTO> getFigliByIndice(Long idAOO, String indice, Connection connection);
	
}
