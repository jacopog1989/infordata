package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.ProtocolloDTO;
import it.ibm.red.business.dto.RegistrazioneAusiliariaNPSDTO;
import it.ibm.red.business.dto.RegistroDTO;
import it.ibm.red.business.enums.TipoProtocolloEnum;
import it.ibm.red.business.service.facade.IMockFacadeSRV;

/**
 * Interfaccia del servizio di mock.
 */
public interface IMockSRV extends IMockFacadeSRV {

	/**
	 * Stacca il nuovo protocollo mock.
	 * @param codiceAoo
	 * @param tipoProtocollo
	 * @param conn
	 * @return protocollo mock
	 */
	ProtocolloDTO getNewProtocollo(String codiceAoo, TipoProtocolloEnum tipoProtocollo, Connection conn);
	
	/**
	 * Ottiene il protocollo mock.
	 * @param idProtocollo
	 * @param conn
	 * @return protocollo mock
	 */
	ProtocolloDTO getProtocollo(String idProtocollo, Connection conn);
	
	/**
	 * Ottiene le registrazioni ausiliarie.
	 * @return lista di registrazioni ausiliarie NPS
	 */
	List<RegistrazioneAusiliariaNPSDTO> getRegistrazioniAusiliarie();

	/**
	 * Ottiene la registrazione ausiliaria.
	 * @param codiceAoo
	 * @param registro
	 * @return registrazione ausiliaria NPS
	 */
	RegistrazioneAusiliariaNPSDTO getRegistrazioneAusiliaria(String codiceAoo, RegistroDTO registro);

}
