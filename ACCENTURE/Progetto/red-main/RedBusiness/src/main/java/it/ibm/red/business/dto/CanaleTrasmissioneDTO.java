package it.ibm.red.business.dto;

import java.io.Serializable;

/**
 * DTO per la gestione dei canale di trasmissione.
 */
public class CanaleTrasmissioneDTO implements Serializable {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -1234346772122574811L;

	/**
	 * ID associato a TUTTI.
	 */
	public static final int ID_TUTTI = 0;

	/**
	 * ID associato a EMAIL.
	 */
	public static final int ID_EMAIL = 1;

	/**
	 * ID associato a RED.
	 */
	public static final int ID_RED = 2;
	

	/**
	 * Idnetificativo.
	 */
	private int id;

	/**
	 * Nome.
	 */
	private String nome;

	/**
	 * @return
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(final int inId) {
		this.id = inId;
	}


	/**
	 * @return
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome
	 */
	public void setNome(final String nome) {
		this.nome = nome;
	}

	/**
	 * @see java.lang.Object#toString().
	 */
	@Override
	public String toString() {
		return "CanaleTrasmissioneDTO [id=" + id + ", " + (nome != null ? "nome=" + nome : "") + "]";
	}
	
	/**
	 * @see java.lang.Object#hashCode().
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		
		return result;
	}
	
	/**
	 * @see java.lang.Object#equals(java.lang.Object).
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof CanaleTrasmissioneDTO)) {
			return false;
		}
		CanaleTrasmissioneDTO other = (CanaleTrasmissioneDTO) obj;
		if (id != other.id) {
			return false;
		}
		if (nome == null) {
			if (other.nome != null) {
				return false;
			}
		} else if (!nome.equals(other.nome)) {
			return false;
		}
		
		return true;
	}
}