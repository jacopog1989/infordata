package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.MetadatoDinamicoDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Facade del servizio che gestisce i metadati dinamici.
 */
public interface IMetadatiDinamiciFacadeSRV extends Serializable {
	
	/**
	 * @param utente
	 * @param classeDocumentale
	 * @return
	 */
	List<MetadatoDinamicoDTO> getMetadatyByClasseDocumentale(UtenteDTO utente, String classeDocumentale);
}