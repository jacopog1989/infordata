package it.ibm.red.business.exception;

import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;

/**
 * The Class NoTrasformerException.
 *
 * @author CPIERASC
 * 
 *         Eccezione per l'assenza del trasformer.
 */
public class NoTrasformerException extends RuntimeException {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Costruttore.
	 * 
	 * @param enumKey	enum PE
	 */
	public NoTrasformerException(final TrasformerPEEnum enumKey) {
		super("Trasformer non trovato per la chiave: " + enumKey);
	}

	/**
	 * Costruttore.
	 * 
	 * @param enumKey enum CE
	 */
	public NoTrasformerException(final TrasformerCEEnum enumKey) {
		super("Trasformer non trovato per la chiave: " + enumKey);
	}

}
