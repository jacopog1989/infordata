package it.ibm.red.business.service;

import java.sql.Connection;

import it.ibm.red.business.service.facade.ISiebelFacadeSRV;

/**
 * Interfaccia del servizio di gestione Siebel.
 */
public interface ISiebelSRV extends ISiebelFacadeSRV {

	/**
	 * Viene restituito l'idContattoSiebel associato all'ufficio in input se l'ufficio è abilitato su Siebel
	 * 
	 * @param idNodo
	 * @param connection
	 * @return
	 */
	String retrieveIdContattoSiebel(Long idNodo, Connection connection);
	
	/**
	 * Versione che recupera la connessione precedentemente aperta.
	 * @param idDocumento
	 * @param conn
	 * @return
	 */
	boolean hasRds(String idDocumento, Long idAOO, Connection conn);
	
}
