package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import it.ibm.red.business.dao.IAllaccioDAO;
import it.ibm.red.business.dao.IContributoDAO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.SecurityDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.ISecurityCambioIterSRV;
import it.ibm.red.business.service.ISecuritySRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class SecurityCambioIterSRV.
 *
 * @author CPIERASC
 * 
 *         Servizio gestione security.
 */
@Service
public class SecurityCambioIterSRV extends AbstractService implements ISecurityCambioIterSRV {

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SecurityCambioIterSRV.class.getName());

	/**
	 * Dao gestione contributo.
	 */
	@Autowired
	private IContributoDAO contributoDAO;

	/**
	 * Dao.
	 */
	@Autowired
	private IAllaccioDAO allaccioDAO;

	/**
	 * Servizio gestione sicurezza.
	 */
	@Autowired
	private ISecuritySRV securitySRV;

	/**
	 * Gets the assegnazione utente coordinatore.
	 *
	 * @param assegnazioneCoordinatore the assegnazione coordinatore
	 * @return the assegnazione utente coordinatore
	 */
	@Override
	public final String[] getAssegnazioneUtenteCoordinatore(final String assegnazioneCoordinatore) {
		final String[] assegnazioniArray = new String[1];
		assegnazioniArray[0] = assegnazioneCoordinatore;
		return assegnazioniArray;
	}

	/**
	 * Gets the security per riassegnazione.
	 *
	 * @param utente            the utente
	 * @param idDocumento       the id documento
	 * @param assegnazioniArray the assegnazioni array
	 * @param isRiservato       the is riservato
	 * @param con               the con
	 * @param coordinatore      the coordinatore
	 * @return the security per riassegnazione
	 */
	@Override
	public final ListSecurityDTO getSecurityPerRiassegnazione(final UtenteDTO utente, final String idDocumento, final String[] assegnazioniArray,
			final boolean isRiservato, final Connection con, final String coordinatore) {

		// ottieni lo storico security sul documento
		final ListSecurityDTO riassegnazioniSecurity = securitySRV.getStoricoSecurity(utente, idDocumento, true, false, isRiservato, false, null, con);

		// ##############################################################
		// Aggiungi la gerarchia delle nuove assegnazioni
		// Ottieni la lista delle gerarchie delle security
		final ListSecurityDTO securityDestinatario = securitySRV.getSecurityAssegnazioni(assegnazioniArray, true, false, isRiservato, con);
		riassegnazioniSecurity.addAll(securityDestinatario);
		// ##############################################################
		if (!StringUtils.isNullOrEmpty(coordinatore)) {
			final ListSecurityDTO securityDestinatarioCoordinatore = securitySRV.getSecurityAssegnazioni(getAssegnazioneUtenteCoordinatore(coordinatore), true, false,
					isRiservato, con);
			riassegnazioniSecurity.addAll(securityDestinatarioCoordinatore);
		}
		// ##############################################################

		for (final SecurityDTO sec : riassegnazioniSecurity) {
			if (sec.getUtente().getIdUtente() == 0) {
				sec.setUtente(null);
			}
		}

		return riassegnazioniSecurity;
	}

	/**
	 * Aggiorna le security sui contributi collegati al documento.
	 *
	 * @param idDocumentoPrincipale the id documento principale
	 * @param con                   the con
	 * @param assegnazioni          the assegnazioni
	 * @param utente                the utente
	 */
	@Override
	public void aggiornaSecurityContributiInseriti(final String idDocumentoPrincipale, final Connection con, final String[] assegnazioni, final UtenteDTO utente) {
		LOGGER.info("Aggiorna le security dei contributi collegati per il documento: [" + idDocumentoPrincipale + "].");
		final Collection<it.ibm.red.business.persistence.model.Contributo> contributi = contributoDAO.getContributiInterni(idDocumentoPrincipale + "", utente.getIdAoo(), con);

		if (contributi != null && !contributi.isEmpty()) {
			// Aggiornamento delle security dei contributi collegati effettuato.
			// loop su tutti i documenti allacciati
			for (final it.ibm.red.business.persistence.model.Contributo contributo : contributi) {
				// controlla se è stato inserito su filenet
				if (!StringUtils.isNullOrEmpty(contributo.getGuid())) {
					LOGGER.info("Adeguamento e modifica delle security del Documento: [" + contributo.getGuid() + "]");
					// Modifica delle security sul documento ##############################
					final Collection<SecurityDTO> securities = getSecurityPerRiassegnazione(utente, idDocumentoPrincipale, assegnazioni, false, con, null);
					securitySRV.updateSecurity(utente, idDocumentoPrincipale, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FOLDER_DOCUMENTI_CE_KEY),
							new ListSecurityDTO(securities), true);
				}
			}
		}
	}

	/**
	 * aggiorna security documenti allacciati.
	 *
	 * @param idDocumentoPrincipale the id documento principale
	 * @param securities            the securities
	 * @param con                   the con
	 * @param assegnazioni          the assegnazioni
	 * @param utente                the utente
	 */
	@Override
	public final void aggiornaSecurityDocumentiAllacciati(final String idDocumentoPrincipale, final ListSecurityDTO securities, final Connection con,
			final String[] assegnazioni, final UtenteDTO utente) {
		IFilenetCEHelper fcehAdmin = null;

		try {
			// Ottieni tutti i documenti allacciati
			final Collection<RispostaAllaccioDTO> documentiDaAllacciare = allaccioDAO.getDocumentiRispostaAllaccio(Integer.parseInt(idDocumentoPrincipale),
					utente.getIdAoo().intValue(), con);

			if (documentiDaAllacciare != null && !documentiDaAllacciare.isEmpty()) {
				fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()), utente.getIdAoo());

				for (final RispostaAllaccioDTO rispostaAllaccio : documentiDaAllacciare) {
					final Document documento = fcehAdmin.getDocumentByDTandAOO(rispostaAllaccio.getIdDocumentoAllacciato(), utente.getIdAoo());

					final Integer riservato = (Integer) TrasformerCE.getMetadato(documento, PropertiesNameEnum.RISERVATO_METAKEY);
					final boolean isRiservato = riservato != null && riservato > 0;
					if (!isRiservato) {
						LOGGER.info("Adeguamento e modifica delle security del fascicolo per il Documento: [" + rispostaAllaccio.getIdDocumentoAllacciato() + "]");
						// Modifica le security del fascicolo #####################################
						securitySRV.modificaSecurityFascicoli(rispostaAllaccio.getIdDocumentoAllacciato() + "", utente, assegnazioni, null, false, con);

						// ########################################################################
						// Se modifica security fascicolo ok, modifica security documento
						// Modifica delle security sul documento ##############################
						securitySRV.updateSecurity(utente, rispostaAllaccio.getIdDocumentoAllacciato(),
								PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FOLDER_DOCUMENTI_CE_KEY), securities, true);
						// ####################################################################
					}
				}
			}
		} finally {
			popSubject(fcehAdmin);
		}
	}
}