package it.ibm.red.business.enums;

/**
 * The Enum TipoSpedizioneDocumentoEnum.
 *
 * @author CPIERASC
 * 
 *         Enum per le possibili tipologie di spedizione di un documento
 */
public enum TipoSpedizioneDocumentoEnum {

	/**
	 * Spedizione cartacea.
	 */
	CARTACEO,
	
	/**
	 * Spedizione elettronica.
	 */
	ELETTRONICO,
	
	/**
	 * Spedizione mista.
	 */
	MISTO;

}
