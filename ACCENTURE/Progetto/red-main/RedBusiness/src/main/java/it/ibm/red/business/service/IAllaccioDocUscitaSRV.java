/**
 * 
 */
package it.ibm.red.business.service;

import java.util.List;

import it.ibm.red.business.dto.AggiornaSecurityNpsDTO;
import it.ibm.red.business.dto.AllaccioDocUscitaDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.SecurityDTO;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.service.facade.IAllacciaDocUscitaFacadeSRV;

/**
 * @author VINGENITO
 *
 */
public interface IAllaccioDocUscitaSRV extends IAllacciaDocUscitaFacadeSRV {

	/**
	 * Aggiorna le security del documento in uscita.
	 * 
	 * @param docTitleUscita
	 *            - document title in uscita
	 * @param docTitleIngresso
	 *            - document title in ingresso
	 * @param securityDTOList
	 *            - lista delle security
	 * @param aoo
	 *            identificativo area organizzativa
	 */
	void aggiornaSecurityDocUscita(String docTitleUscita, String docTitleIngresso,
			ListSecurityDTO securityDTOList, Aoo aoo);

	/**
	 * Ricerca i documenti allacciati in uscita.
	 * 
	 * @param idAoo
	 *            - id dell'Aoo
	 * @param stato
	 *            identificativo stato
	 * @return lista dei documenti allacciati in uscita
	 */
	List<AllaccioDocUscitaDTO> getAllaccioDocUscitaByAoo(int idAoo, Integer stato);

	/**
	 * Ottiene le security per il documento in ingresso.
	 * 
	 * @param aclDocUscita
	 *            acl del documento in uscita
	 * @param docAllaccioIngrsso
	 *            - documento in ingresso
	 * @param aoo
	 *            identificiativo area organizzativa
	 * @param securityDTOList
	 *            - lista delle security
	 * @param idUtenteDaTracciare
	 *            - id dell'utente da tracciare
	 * @return lista delle security
	 */
	List<SecurityDTO> getSecurityByDocIngresso(String aclDocUscita, String docAllaccioIngrsso, Aoo aoo,
			List<SecurityDTO> securityDTOList, List<Integer> idUtenteDaTracciare);

	/**
	 * Ottiene le acl per il documento in uscita.
	 * 
	 * @param docTitleUscita
	 *            - document title in uscita
	 * @param securityDTOList
	 *            - lista delle security
	 * @param aoo
	 *            identificativo area organizzativa
	 * @param aggiornaSecurityDTO
	 *            dto per l'aggiornamento delle security
	 * @return
	 */
	String getAclDocUscitaAndSetSecurity(String docTitleUscita, List<SecurityDTO> securityDTOList, Aoo aoo,
			AggiornaSecurityNpsDTO aggiornaSecurityDTO);

	/**
	 * Aggiorna le security del documento.
	 * 
	 * @param aoo
	 *            identificativo dell'area organizzativa
	 * @param securityDTOList
	 *            - lista delle security
	 * @param aclDocUscita
	 *            acl del documento in uscita
	 * @param securityDocIngresso
	 *            - lista delle security per il doc in ingresso
	 * @param docAllaccio
	 *            - documento in uscita
	 * @param idUtenteDaTracciare
	 *            - id dell'utente da tracciare
	 */
	void aggiornaSecurityDocumento(Aoo aoo, List<SecurityDTO> securityDTOList, String aclDocUscita,
			ListSecurityDTO securityDocIngresso, AllaccioDocUscitaDTO docAllaccio,
			List<Integer> idUtenteDaTracciare);

	/**
	 * Traccia il procedimento.
	 * 
	 * @param idAoo
	 *            identificativo dell'area organizzativa - id dell'Aoo
	 * @param idUtente
	 *            - id dell'utente
	 * @param idDocumento
	 *            - id documento
	 */
	void tracciaProcedimentoAllaccioDoc(Integer idAoo, Integer idUtente, Integer idDocumento);

	/**
	 * Aggiorna lo stato.
	 * 
	 * @param docTitleUscita
	 *            - document title in uscita
	 * @param statoFinale
	 *            - stato finale
	 * @param idAoo
	 *            - id dell'Aoo
	 * @param errorMsg
	 *            - messaggio di errore
	 */
	void updateStato(String docTitleUscita, Integer statoFinale, Integer idAoo, String errorMsg);

	/**
	 * Aggiorna lo stato.
	 * 
	 * @param docTitleUscita
	 *            - document title in uscita
	 * @param docTitleIngresso
	 *            - document title in ingresso
	 * @param statoFinale
	 *            - stato finale
	 * @param idAoo
	 *            - id dell'Aoo
	 * @param errorMsg
	 *            - messaggio di errore
	 */
	void updateStato(String docTitleUscita, String docTitleIngresso, Integer statoFinale, Integer idAoo,
			String errorMsg);

	/**
	 * Aggiorna lo stato.
	 * 
	 * @param docTitleUscita
	 *            - document title in uscita
	 * @param docTitleIngresso
	 *            - document title in ingresso
	 * @param statoFinale
	 *            - stato finale
	 * @param idAoo
	 *            - id dell'Aoo
	 * @param statoIniziale
	 *            - stato iniziale
	 * @param errorMsg
	 *            - messaggio di errore
	 */
	void updateStato(String docTitleUscita, String docTitleIngresso, Integer statoFinale, Integer idAoo,
			Integer statoIniziale, String errorMsg);
}
