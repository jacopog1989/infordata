package it.ibm.red.business.dto;

import java.util.Collection;
import java.util.Date;

import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.enums.TipoControlloEnum;
import it.ibm.red.business.enums.TipoGestioneEnum;
import it.ibm.red.business.persistence.model.TipoProcedimento;

/**
 * The Class TipoProcedimentoDTO.
 *
 * @author CPIERASC
 * 
 *         DTO tipo procedimento.
 */
public class TipoProcedimentoDTO extends AbstractDTO {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Data attivazione.
	 */
	private Date dataAttivazione;

	/**
	 * Data disattivazione.
	 */
	private Date datadisattivazione;

	/**
	 * Descrizione.
	 */
	private String descrizione;

	/**
	 * Flag generico entrato.
	 */
	private int flagGenericoEntrata;

	/**
	 * Flag generico uscita.
	 */
	private int flagGenericoUscita;

	/**
	 * Identificativo aoo.
	 */
	private int idAoo;

	/**
	 * Identificativo tipo procedimento.
	 */
	private long tipoProcedimentoId;

	/**
	 * Identificativo workflow.
	 */
	private Long workflowId;

	/**
	 * Subject workflow.
	 */
	private String workflowSubject;
	/**
	 * Tipologia di gestione della coppia. MANUALE, IBRIDA o AUTOMATICA
	 */
	private TipoGestioneEnum tipoGestione;

	/**
	 * Tipologia di controllo della coppia. PREVENTIVO, SUCCESSIVO o CONCOMITANTE
	 */
	private TipoControlloEnum tipoControllo;

	/**
	 * Metadi associati al procedimento/documento
	 */
	private Collection<MetadatoDTO> metadati;

	/**
	 * Registri associati al procedimento/documento
	 */
	private Collection<RegistroDTO> registri;

	/**
	 * Intero rappresentante i giorni per il quale il procedimento puoi esistere in
	 * coda spedizioni
	 */
	private int minutiScadenzaSospeso;

	/**
	 * Flag rappresentante la proprietà CONTABILE
	 */
	private boolean flagContabile;

	/**
	 * Costruttore.
	 * 
	 * @param tipoProcedimento model
	 */
	public TipoProcedimentoDTO(final TipoProcedimento tipoProcedimento) {
		if (tipoProcedimento != null) {
			this.tipoProcedimentoId = tipoProcedimento.getTipoProcedimentoId();
			this.descrizione = tipoProcedimento.getDescrizione();
			this.dataAttivazione = tipoProcedimento.getDataAttivazione();
			this.datadisattivazione = tipoProcedimento.getDatadisattivazione();
			this.workflowId = tipoProcedimento.getWorkFlowId();
			this.idAoo = tipoProcedimento.getIdAoo();
			this.flagGenericoEntrata = tipoProcedimento.getFlagGenericoEntrata();
			this.flagGenericoUscita = tipoProcedimento.getFlagGenericoUscita();
			this.workflowSubject = tipoProcedimento.getWorkflowSubject();
		}
	}

	/**
	 * Costruttore vuoto.
	 */
	public TipoProcedimentoDTO() {
	}

	/**
	 * Costruttore.
	 * 
	 * @param descrizione
	 */
	public TipoProcedimentoDTO(final String descrizione) {
		setDescrizione(descrizione);
	}

	/**
	 * Costruttore copia.
	 * 
	 * @param other
	 */
	public TipoProcedimentoDTO(final TipoProcedimentoDTO other) {
		this.setDescrizione(other.getDescrizione());
		this.setFlagGenericoEntrata(other.getFlagGenericoEntrata());
		this.setFlagGenericoUscita(other.getFlagGenericoUscita());
		this.setTipoControllo(other.getTipoControllo());
		this.setTipoGestione(other.getTipoGestione());
		this.setIdAoo(other.getIdAoo());
		if (other.getWorkflowId() != null) {
			this.setWorkflowId(other.getWorkflowId());
		}
		this.setMetadati(other.getMetadati());
		this.setRegistri(other.getRegistri());
		this.setMinutiScadenzaSospeso(other.getMinutiScadenzaSospeso());
		this.setFlagContabile(other.isFlagContabile());
	}

	/**
	 * Imposta i parametri associati alla categoria. Valorizza
	 * {@link #flagGenericoEntrata} e {@link #flagGenericoUscita} opportunamente.
	 * 
	 * @param categoria
	 */
	public void setParametriCategoria(final TipoCategoriaEnum categoria) {
		if (categoria.equals(TipoCategoriaEnum.ENTRATA)) {
			setFlagGenericoEntrata(1);
			setFlagGenericoUscita(0);
		} else if (categoria.equals(TipoCategoriaEnum.USCITA)) {
			setFlagGenericoEntrata(0);
			setFlagGenericoUscita(1);
		} else if (categoria.equals(TipoCategoriaEnum.ENTRATA_ED_USCITA)) {
			setFlagGenericoEntrata(1);
			setFlagGenericoUscita(1);
		}
	}

	/**
	 * Getter.
	 * 
	 * @return data attivazione
	 */
	public final Date getDataAttivazione() {
		return dataAttivazione;
	}

	/**
	 * Getter.
	 * 
	 * @return data disattivazione
	 */
	public final Date getDatadisattivazione() {
		return datadisattivazione;
	}

	/**
	 * Getter.
	 * 
	 * @return descrizione
	 */
	public final String getDescrizione() {
		return descrizione;
	}

	/**
	 * Getter.
	 * 
	 * @return flag generico entrata
	 */
	public final int getFlagGenericoEntrata() {
		return flagGenericoEntrata;
	}

	/**
	 * Restituisce il flag generico entrata.
	 * 
	 * @param flagGenericoEntrata
	 */
	public final void setFlagGenericoEntrata(final int flagGenericoEntrata) {
		this.flagGenericoEntrata = flagGenericoEntrata;
	}

	/**
	 * Getter.
	 * 
	 * @return flag generico uscita
	 */
	public final int getFlagGenericoUscita() {
		return flagGenericoUscita;
	}

	/**
	 * Restituisce il flag generico uscita.
	 * 
	 * @param flagGenericoUscita
	 */
	public final void setFlagGenericoUscita(final int flagGenericoUscita) {
		this.flagGenericoUscita = flagGenericoUscita;
	}

	/**
	 * Getter.
	 * 
	 * @return identificativo aoo
	 */
	public final int getIdAoo() {
		return idAoo;
	}

	/**
	 * Restituisce l'id aoo.
	 * 
	 * @param idAoo
	 */
	public final void setIdAoo(final int idAoo) {
		this.idAoo = idAoo;
	}

	/**
	 * Getter.
	 * 
	 * @return identificativo tipo procedimento
	 */
	public final long getTipoProcedimentoId() {
		return tipoProcedimentoId;
	}

	/**
	 * Getter.
	 * 
	 * @return identificativo workflow
	 */
	public final Long getWorkflowId() {
		return workflowId;
	}

	/**
	 * Imposta l'id dell'workflow.
	 * 
	 * @param workflowId
	 */
	public void setWorkflowId(final Long workflowId) {
		this.workflowId = workflowId;
	}

	/**
	 * Getter.
	 * 
	 * @return subject workflow
	 */
	public final String getWorkflowSubject() {
		return workflowSubject;
	}

	/**
	 * Restituisce la descrizione.
	 * 
	 * @param descrizione
	 */
	public void setDescrizione(final String descrizione) {
		this.descrizione = descrizione;
	}

	/**
	 * Imposta l'id del tipo procedimento.
	 * 
	 * @param tipoProcedimentoId
	 */
	public void setTipoProcedimentoId(final long tipoProcedimentoId) {
		this.tipoProcedimentoId = tipoProcedimentoId;
	}

	/**
	 * Restituisce il tipo gestione.
	 * 
	 * @return tipo gestione
	 */
	public TipoGestioneEnum getTipoGestione() {
		return tipoGestione;
	}

	/**
	 * Imposta il tipo gestione.
	 * 
	 * @param tipoGestione
	 */
	public void setTipoGestione(final TipoGestioneEnum tipoGestione) {
		this.tipoGestione = tipoGestione;
	}

	/**
	 * Restituisce il tipo controllo.
	 * 
	 * @return tipo controllo
	 */
	public TipoControlloEnum getTipoControllo() {
		return tipoControllo;
	}

	/**
	 * Imposta il tipo controllo.
	 * 
	 * @param tipoControllo
	 */
	public void setTipoControllo(final TipoControlloEnum tipoControllo) {
		this.tipoControllo = tipoControllo;
	}

	/**
	 * Restituisce la lista dei metadati.
	 * 
	 * @return metadati
	 */
	public Collection<MetadatoDTO> getMetadati() {
		return metadati;
	}

	/**
	 * Imposta la lista dei metadati.
	 * 
	 * @param metadati
	 */
	public void setMetadati(final Collection<MetadatoDTO> metadati) {
		this.metadati = metadati;
	}

	/**
	 * Restituisce la lista dei registri.
	 * 
	 * @return registri
	 */
	public Collection<RegistroDTO> getRegistri() {
		return registri;
	}

	/**
	 * Imposta la lista dei registri.
	 * 
	 * @param registri
	 */
	public void setRegistri(final Collection<RegistroDTO> registri) {
		this.registri = registri;
	}

	/**
	 * Restituisce i minuti di scadenza in coda Sopsesi.
	 * 
	 * @return minuti scadenza
	 */
	public int getMinutiScadenzaSospeso() {
		return minutiScadenzaSospeso;
	}

	/**
	 * Imposta i minuti di scadenza in coda Sopsesi.
	 * 
	 * @param minutiScadenzaSospeso
	 */
	public void setMinutiScadenzaSospeso(final int minutiScadenzaSospeso) {
		this.minutiScadenzaSospeso = minutiScadenzaSospeso;
	}

	/**
	 * Restituisce il flag associato al contabile.
	 * 
	 * @return flag contabile
	 */
	public boolean isFlagContabile() {
		return flagContabile;
	}

	/**
	 * Imposta il flag associato al contabile.
	 * 
	 * @param flagContabile
	 */
	public void setFlagContabile(final boolean flagContabile) {
		this.flagContabile = flagContabile;
	}

}