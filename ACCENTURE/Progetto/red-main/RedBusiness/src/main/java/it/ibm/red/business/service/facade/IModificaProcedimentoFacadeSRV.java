package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.AssegnatarioOrganigrammaDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;

/**
 * Servizio modifica procedimento.
 * 
 * @author CRISTIANOPierascenzi
 *
 */
public interface IModificaProcedimentoFacadeSRV extends Serializable {

	/**
	 * Metodo per il cambio iter manuale.
	 * 
	 * @param tipoAssegnazione
	 * @param bFlagUrgente
	 * @param utente
	 * @param wobNumber
	 * @param idNodoDestinatarioNew
	 * @param idUtenteDestinatarioNew
	 * @return
	 */
	EsitoOperazioneDTO gestioneCambioIterManuale(TipoAssegnazioneEnum tipoAssegnazione, Boolean bFlagUrgente, UtenteDTO utente, String wobNumber, Long idNodoDestinatarioNew,
			Long idUtenteDestinatarioNew);

	/**
	 * Metodo per il cambio iter automatico.
	 * 
	 * @param iterSelezionato      nuovo iter
	 * @param flagCheckFirmaDigRGS flag check firma digitale rgs
	 * @param bFlagUrgente         flag urgente
	 * @param idUtenteCoordinatore identificativo utente coordinatore
	 * @param utente               utente
	 * @param wobNumber            identificativo workflow
	 * @return esito operazione
	 */
	EsitoOperazioneDTO gestioneCambioIterAutomatico(Integer tipoAssegnazioneSelected, Integer iterSelezionato, Boolean flagCheckFirmaDigRGS, Boolean bFlagUrgente,
			Integer idUtenteCoordinatore, UtenteDTO utente, String wobNumber);

	/**
	 * Metodo per l'inserimento di nuovi uffici/utenti in conoscenza.
	 * 
	 * @param wobNumber                   Identificativo del workflow
	 * @param nuoveAssegnazioniConoscenza Lista delle nuove assegnazioni per
	 *                                    conoscenza da aggiungere alle esistenti
	 * @param utente                      Utente
	 * @return Esito operazione
	 */
	EsitoOperazioneDTO mettiInConoscenza(String wobNumber, List<AssegnatarioOrganigrammaDTO> nuoveAssegnazioniConoscenza, String motivazioneAssegnazione, UtenteDTO utente);

	/**
	 * Metodo per l'inserimento di nuovi uffici/utenti in conoscenza.
	 * 
	 * @param wobNumber
	 * @param inputAssegnazioniConoscenza
	 * @param motivazioneAssegnazione
	 * @param utente
	 * @param annoProtocollo
	 * @param numProtocollo
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO mettiInConoscenza(String wobNumber, List<AssegnatarioOrganigrammaDTO> inputAssegnazioniConoscenza, String motivazioneAssegnazione, UtenteDTO utente,
			Integer annoProtocollo, Integer numProtocollo);
}