/**
 * 
 */
package it.ibm.red.business.dto;

import java.util.ArrayList;
import java.util.Collection;

import it.ibm.red.business.enums.ResponsesRedEnum;

/**
 * @author APerquoti
 *
 */
public class ResponsesDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 7941767403595014966L;
	
	/**
	 * Collection contenente le response applicative e filenet che risultano essere gestite
	 */
	private Collection<ResponsesRedEnum> responsesEnum;
	
	/**
	 * responses che non risultano essere gestite ma che mostreremo a video come riscontro 
	 */
	private Collection<String> responseIssues;
	
	/**
	 * Costruttore.
	 */
	public ResponsesDTO() {
		super();
		responsesEnum = new ArrayList<>();
		responseIssues = new ArrayList<>();
	}

	/**
	 * @return the responsesEnum
	 */
	public final Collection<ResponsesRedEnum> getResponsesEnum() {
		return responsesEnum;
	}

	/**
	 * @param responsesEnum the responsesEnum to set
	 */
	public final void setResponsesEnum(final Collection<ResponsesRedEnum> responsesEnum) {
		this.responsesEnum = responsesEnum;
	}

	/**
	 * @return the responseIssues
	 */
	public final Collection<String> getResponseIssues() {
		return responseIssues;
	}

	/**
	 * @param responseIssues the responseIssues to set
	 */
	public final void setResponseIssues(final Collection<String> responseIssues) {
		this.responseIssues = responseIssues;
	}
	
	


}
