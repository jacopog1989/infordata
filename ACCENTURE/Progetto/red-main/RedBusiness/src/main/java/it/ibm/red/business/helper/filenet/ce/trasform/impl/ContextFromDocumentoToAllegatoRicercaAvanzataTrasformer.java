/**
 * 
 */
package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.util.CollectionUtils;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.enums.ValueMetadatoVerificaFirmaEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.DestinatariRedUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * @author m.crescentini
 * 
 *         Trasformer dai Document FileNet allegato e principale ad Allegato
 *         della ricerca avanzata.
 *
 */
public class ContextFromDocumentoToAllegatoRicercaAvanzataTrasformer extends ContextAllegatoTrasformerCE<MasterDocumentRedDTO> {

	/**
	 * UID.
	 */
	private static final long serialVersionUID = 6198986337394115883L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ContextFromDocumentoToAllegatoRicercaAvanzataTrasformer.class.getName());

	/**
	 * Trasformer context - FROM_DOCUMENTO_TO_ALLEGATO_RICERCA_AVANZATA_CONTEXT.
	 * 
	 * @see TrasformerCEEnum
	 */
	public ContextFromDocumentoToAllegatoRicercaAvanzataTrasformer() {
		super(TrasformerCEEnum.FROM_DOCUMENTO_TO_ALLEGATO_RICERCA_AVANZATA_CONTEXT);
	}

	/**
	 * @param allegatoFileNet
	 * @param principaleFilenet
	 * @param context
	 * @return MasterDocumentRedDTO
	 */
	@SuppressWarnings("unchecked")
	@Override
	public MasterDocumentRedDTO trasform(final Document allegatoFileNet, final Document principaleFilenet, final Map<ContextTrasformerCEEnum, Object> context) {
		MasterDocumentRedDTO allegatoDocumento = null;

		try {
			// ### Metadati estratti dall'allegato -> START
			// Document Title dell'allegato
			final String documentTitleAllegato = (String) getMetadato(allegatoFileNet, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);

			// Nome file
			final String nomeFile = (String) getMetadato(allegatoFileNet, PropertiesNameEnum.NOME_FILE_METAKEY);
			// ### Metadati estratti dall'allegato -> END

			// ### Metadati estratti dal documento principale -> START
			// Classe Documentale (si deve prendere quella del documento principale per
			// caricare correttamente il dettaglio)
			final String classeDocumentale = principaleFilenet.getClassName();

			// GUID
			final String guid = StringUtils.cleanGuidToString(principaleFilenet.get_Id());

			// Document Title
			final String documentTitle = (String) getMetadato(principaleFilenet, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);

			// Anno Documento
			final Integer annoDocumento = (Integer) getMetadato(principaleFilenet, PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY);

			// Numero Documento
			final Integer numeroDocumento = (Integer) getMetadato(principaleFilenet, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);

			// Anno Protocollo
			final Integer annoProtocollo = (Integer) getMetadato(principaleFilenet, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);

			// Numero Protocollo
			final Integer numeroProtocollo = (Integer) getMetadato(principaleFilenet, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);

			// Tipo Protocollo
			final Integer tipoProtocollo = (Integer) getMetadato(principaleFilenet, PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY);

			// Anno Protocollo Emergenza
			final Integer annoProtEmergenza = (Integer) getMetadato(principaleFilenet, PropertiesNameEnum.ANNO_PROTOCOLLO_EMERGENZA_METAKEY);

			// Numero Protocollo Emergenza
			final Integer numeroProtEmergenza = (Integer) getMetadato(principaleFilenet, PropertiesNameEnum.NUMERO_PROTOCOLLO_EMERGENZA_METAKEY);

			// Oggetto
			final String oggetto = (String) getMetadato(principaleFilenet, PropertiesNameEnum.OGGETTO_METAKEY);

			// Data Creazione
			final Date dataCreazione = (Date) getMetadato(principaleFilenet, PropertiesNameEnum.DATA_CREAZIONE_METAKEY);

			// Data Scadenza
			final Date dataScadenza = (Date) getMetadato(principaleFilenet, PropertiesNameEnum.DATA_SCADENZA_METAKEY);

			// Tipologia Documento
			String tipologiaDocumento = null;

			final Integer idTipologiaDocumento = (Integer) getMetadato(principaleFilenet, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY);
			if (idTipologiaDocumento != null) {
				tipologiaDocumento = ((Map<Long, String>) context.get(ContextTrasformerCEEnum.IDENTIFICATIVI_TIPO_DOC)).get(idTipologiaDocumento.longValue());
			}

			// Tipo Procedimento
			String tipoProcedimento = null;

			final Integer idTipoProcedimento = (Integer) getMetadato(principaleFilenet, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY);
			if (idTipoProcedimento != null) {
				tipoProcedimento = ((Map<Long, String>) context.get(ContextTrasformerCEEnum.IDENTIFICATIVI_TIPO_PROC)).get(idTipoProcedimento.longValue());
			}

			// Mittente/Destinatari
			String mittente = null;
			String destinatari = null;

			final String metadatoMittente = (String) getMetadato(principaleFilenet, PropertiesNameEnum.MITTENTE_METAKEY);

			final Collection<?> metadatoDestinatari = (Collection<?>) getMetadato(principaleFilenet, PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY);
			// Si normalizza il metadato per evitare errori comuni durante lo split delle
			// singole stringhe dei destinatari
			final List<String[]> destinatariDocumento = DestinatariRedUtils.normalizzaMetadatoDestinatariDocumento(metadatoDestinatari);

			// Mittente
			if (!StringUtils.isNullOrEmpty(metadatoMittente)) {
				mittente = metadatoMittente.split("\\,")[1];
				// Destinatari
			} else if (!CollectionUtils.isEmpty(destinatariDocumento)) {
				final StringBuilder strDestinatari = new StringBuilder();

				for (final String[] destSplit : destinatariDocumento) {
					if (destSplit.length >= 5) {
						final TipologiaDestinatarioEnum tde = TipologiaDestinatarioEnum.getByTipologia(destSplit[3]);

						String alias = null;
						if (TipologiaDestinatarioEnum.INTERNO.equals(tde)) {
							alias = destSplit[2];
						} else if (TipologiaDestinatarioEnum.ESTERNO.equals(tde)) {
							alias = destSplit[1];
						}

						if (!StringUtils.isNullOrEmpty(alias)) {
							strDestinatari.append(alias).append(", ");
						}
					}
				}

				if (strDestinatari != null && strDestinatari.length() >= 2) {
					destinatari = strDestinatari.substring(0, strDestinatari.length() - 2);
				}
			}

			final String inIdProtocollo = (String) getMetadato(principaleFilenet, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY);

			final Integer idCategoriaDocumento = (Integer) getMetadato(principaleFilenet, PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY);

			final SottoCategoriaDocumentoUscitaEnum sottoCategoriaDocumentoUscita = SottoCategoriaDocumentoUscitaEnum
					.get((Integer) getMetadato(principaleFilenet, PropertiesNameEnum.SOTTOCATEGORIA_DOCUMENTO_USCITA));

			final String decretoLiquidazione = (String) getMetadato(principaleFilenet, PropertiesNameEnum.DECRETO_LIQUIDAZIONE_METAKEY);

			// ### Metadati estratti dal documento principale -> END

			final Boolean firmeValideAll = (Boolean) getMetadato(principaleFilenet, PropertiesNameEnum.VERIFICA_FIRMA_DOC_ALLEGATO); // Vale solo per gli allegati
			final Integer metadatoValiditaFirma = (Integer) getMetadato(principaleFilenet, PropertiesNameEnum.METADATO_DOCUMENTO_VALIDITA_FIRMA);
			Boolean firmaValidaPrinc = null;
			if(metadatoValiditaFirma!=null) {
				if(ValueMetadatoVerificaFirmaEnum.FIRMA_OK.equals(ValueMetadatoVerificaFirmaEnum.get(metadatoValiditaFirma))) {
					firmaValidaPrinc = true;
				}else if(ValueMetadatoVerificaFirmaEnum.FIRMA_KO.equals(ValueMetadatoVerificaFirmaEnum.get(metadatoValiditaFirma))){
					firmaValidaPrinc = false;
				}
				
			}
			
			allegatoDocumento = new MasterDocumentRedDTO(documentTitle, numeroDocumento, oggetto, tipologiaDocumento, numeroProtocollo, annoProtocollo, null, null, null, 
					dataScadenza, null, dataCreazione, idCategoriaDocumento, null, tipoProtocollo, null, null, null, tipoProcedimento, null, annoDocumento, guid, null, null, 
					classeDocumentale, null, null, nomeFile, mittente, destinatari, false, inIdProtocollo, numeroProtEmergenza, annoProtEmergenza,
					null, null, null, null, idTipologiaDocumento, idTipoProcedimento, sottoCategoriaDocumentoUscita, null, false, null,firmaValidaPrinc,firmeValideAll, decretoLiquidazione, 
					metadatoValiditaFirma, null, null, destinatariDocumento, null);
			
			allegatoDocumento.setAllegatoDocumentTitle(documentTitleAllegato); // Si aggiunge il document title dell'allegato
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero di un metadato durante la trasformazione da Document a MasterDocumentRedDTO : ", e);
		}

		return allegatoDocumento;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.ContextTrasformerCE#trasform(com.filenet.api.core.Document,
	 *      java.util.Map).
	 */
	@Override
	public MasterDocumentRedDTO trasform(final Document document, final Map<ContextTrasformerCEEnum, Object> context) {
		throw new RedException();
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.ContextAllegatoTrasformerCE#trasform(com.filenet.api.core.Document,
	 *      java.sql.Connection, java.util.Map).
	 */
	@Override
	public MasterDocumentRedDTO trasform(final Document document, final Connection connection, final Map<ContextTrasformerCEEnum, Object> context) {
		throw new RedException();
	}
}
