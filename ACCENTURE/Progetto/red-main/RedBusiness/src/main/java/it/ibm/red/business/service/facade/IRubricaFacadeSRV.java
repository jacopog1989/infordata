package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.CasellaPostaDTO;
import it.ibm.red.business.dto.RicercaRubricaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.StatoNotificaContattoEnum;
import it.ibm.red.business.enums.TipoRubricaEnum;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.NotificaContatto;

/**
 * Facade del servizio di gestione rubrica.
 */
public interface IRubricaFacadeSRV extends Serializable {

	/**
	 * Ottiene i contatti preferiti.
	 * 
	 * @param idUfficio
	 * @param idAOO
	 * @return lista di contatti
	 */
	List<Contatto> getPreferiti(Long idUfficio, Long idAOO);

	/**
	 * Rimuove il contatto dai preferiti.
	 * 
	 * @param idUfficio
	 * @param contattoID
	 * @param tipoRubrica
	 */
	void rimuoviPreferito(Long idUfficio, Long contattoID, TipoRubricaEnum tipoRubrica);

	/**
	 * Ricerca la lista di contatti.
	 * 
	 * @param idUfficio
	 * @param ricercaContattoObj
	 * @return lista di contatti
	 */
	List<Contatto> ricerca(Long idUfficio, RicercaRubricaDTO ricercaContattoObj);

	/**
	 * Inserisce il contatto.
	 * 
	 * @param idUfficio
	 * @param inserisciContattoItem
	 * @param checkUnivocitaMail
	 * @return id del contatto
	 */
	Long inserisci(Long idUfficio, Contatto inserisciContattoItem, boolean checkUnivocitaMail);

	/**
	 * Ottiene il contatto tramite l'id.
	 * 
	 * @param idContatto
	 * @return contatto
	 */
	Contatto getContattoByID(Long idContatto);

	/**
	 * Ottiene il contatto tramite l'id considerando o no i gruppi.
	 * 
	 * @param consideraGruppi
	 * @param idContatto
	 * @return contatto
	 */
	Contatto getContattoByID(boolean consideraGruppi, Long idContatto);

	/**
	 * Crea la notifica di modifica contatto.
	 * 
	 * @param notifica
	 * @param contattoVecchio
	 */
	void notificaModificaContatto(NotificaContatto notifica, Contatto contattoVecchio);

	/**
	 * Crea la notifica di eliminazione contatto.
	 * 
	 * @param notifica
	 */
	void notificaEliminazioneContatto(NotificaContatto notifica);

	/**
	 * Ottiene le notifiche contatto.
	 * 
	 * @param idContatto
	 * @param statoNotifica
	 * @return lista di notifiche contatto
	 */
	List<NotificaContatto> getNotifiche(Long idContatto, String statoNotifica);

	/**
	 * Ottiene le strutture di I livello.
	 * 
	 * @return lista delle strutture di I livello
	 */
	List<String> getStruttureILivello();

	/**
	 * Ottiene le strutture di II livello a partire dal I livello.
	 * 
	 * @param idStrutturaILivello
	 * @return lista delle strutture di II livello
	 */
	List<String> getStruttureIILivello(String idStrutturaILivello);

	/**
	 * Ottiene le strutture di III livello a partire dal II livello.
	 * 
	 * @param idStrutturaIILivello
	 * @return lista delle strutture di III livello
	 */
	List<String> getStruttureIIILivello(String idStrutturaIILivello);

	/**
	 * Crea il gruppo.
	 * 
	 * @param gruppo
	 * @param contattiSelezionati
	 * @param idUfficio
	 * @param idUtente
	 */
	void creaGruppo(Contatto gruppo, Collection<Contatto> contattiSelezionati, Long idUfficio, Long idUtente);

	/**
	 * Ottiene il contatto dalla notifica.
	 * 
	 * @param notifica
	 * @return contatto
	 */
	Contatto getContattoFromNotifica(NotificaContatto notifica);

	/**
	 * Modifica il contatto.
	 * 
	 * @param contatto
	 * @param notifica
	 * @param checkUnivocitaMail
	 */
	void modifica(Contatto contatto, NotificaContatto notifica, boolean checkUnivocitaMail);

	/**
	 * Elimina il contatto.
	 * 
	 * @param idContatto
	 * @param notifica
	 * @param altreNotificheCollegate
	 */
	void elimina(Long idContatto, NotificaContatto notifica, List<NotificaContatto> altreNotificheCollegate);

	/**
	 * Aggiorna lo stato della notifica.
	 * 
	 * @param notifica
	 * @param statoNotifica
	 * @param motivoRifiuto
	 */
	void updateStatoNotifica(NotificaContatto notifica, StatoNotificaContattoEnum statoNotifica, String motivoRifiuto);

	/**
	 * Ottiene i contatti di tutti i gruppi.
	 * 
	 * @param idUfficio
	 * @return lista di contatti
	 */
	List<Contatto> getAllGruppi(Long idUfficio);

	/**
	 * Modifica il gruppo.
	 * 
	 * @param gruppo
	 * @param contattiSelezionati
	 * @param contattidaRimuovere
	 * @param idUfficio
	 */
	void modificaGruppo(Contatto gruppo, List<Contatto> contattiSelezionati, List<Contatto> contattidaRimuovere, Long idUfficio);

	/**
	 * Elimina il gruppo.
	 * 
	 * @param gruppo
	 * @param idUfficio
	 */
	void eliminaGruppo(Contatto gruppo, Long idUfficio);

	/**
	 * Ottiene i contatti IPA figli.
	 * 
	 * @param contattoIpa
	 * @return lista di contatti
	 */
	List<Contatto> getContattiChildIPA(Contatto contattoIpa);

	/**
	 * Ottiene i contatti della rubrica mail per alias.
	 * 
	 * @param casellaPostale
	 * @param aliasDescrizione
	 * @return lista di contatti
	 */
	List<Contatto> getContattiRubricaMailPerAlias(String casellaPostale, String aliasDescrizione);

	/**
	 * Ottiene i contatti preferiti per alias.
	 * 
	 * @param aliasDescrizione
	 * @param idUfficio
	 * @param idAOO
	 * @return lista di contatti
	 */
	List<Contatto> getContattiPreferitiPerAlias(String aliasDescrizione, Long idUfficio, Long idAOO);

	/**
	 * Ottiene i contatti tramite l'email.
	 * 
	 * @param casellaPostale
	 * @param aliasDescrizione
	 * @return lista di contatti
	 */
	List<Contatto> getContattiByEmail(String casellaPostale, String aliasDescrizione);

	/**
	 * Ottiene i contatti dalle caselle di posta e dal nome.
	 * 
	 * @param casellePostali
	 * @param nomeContatto
	 * @return lista di contatti
	 */
	List<Contatto> getContattiCasellePostaliFromName(List<CasellaPostaDTO> casellePostali, String nomeContatto);

	/**
	 * Elimina il contatto email.
	 * 
	 * @param idContatto
	 * @return
	 */
	int eliminaContattoMail(long idContatto);

	/**
	 * Aggiorna il contatto casella postale.
	 * 
	 * @param contatto
	 * @param casellePostali
	 * @return
	 */
	long updateContattoCasellaPostale(Contatto contatto, List<CasellaPostaDTO> casellePostali);

	/**
	 * Aggiunge il contatto nella casella postale.
	 * 
	 * @param c
	 * @param casellePostali
	 * @return id del contatto
	 */
	long inserisciContattoCasellaPostale(Contatto c, List<CasellaPostaDTO> casellePostali);

	/**
	 * Inserisce il gruppo email nella casella postale.
	 * 
	 * @param gruppoEmail
	 * @param casellePostali
	 * @param contattiDaAssociare
	 * @return id del gruppo
	 */
	int inserisciGruppoEmail(Contatto gruppoEmail, List<CasellaPostaDTO> casellePostali, List<Contatto> contattiDaAssociare);

	/**
	 * Ottiene i contatti associati al gruppo email.
	 * 
	 * @param gruppoEmail
	 * @return lista di contatti
	 */
	Collection<Contatto> getContattiAssociati(Contatto gruppoEmail);

	/**
	 * Ottiene la lista dei gruppi email tramite il nome della casella postale.
	 * 
	 * @param casellePostali
	 * @param nome
	 * @return lista di contatti
	 */
	List<Contatto> getListaGruppoEmailByNomeECasellaPostale(List<CasellaPostaDTO> casellePostali, String nome);

	/**
	 * Ottiene la lista delle caselle postali tramite l'id del contatto.
	 * 
	 * @param idContatto
	 * @return emails
	 */
	Collection<String> getListaCasellaPostaleGruppoEmail(long idContatto);

	/**
	 * Elimina il gruppo email.
	 * 
	 * @param idGruppoEmail
	 * @return
	 */
	int eliminaGruppoEmail(Long idGruppoEmail);

	/**
	 * Aggiorna il gruppo email.
	 * 
	 * @param gruppoEmail
	 * @param casellePostali
	 * @param contattiDaAssociare
	 * @return
	 */
	int updateGruppoEmail(Contatto gruppoEmail, List<CasellaPostaDTO> casellePostali, List<Contatto> contattiDaAssociare);

	/**
	 * Ottiene i contatti dalla mail.
	 * 
	 * @param mailMittente
	 * @param idAoo
	 * @param idUfficio
	 * @return lista di contatti
	 */
	List<Contatto> getContattoFromMail(String mailMittente, int idAoo, Long idUfficio);

	/**
	 * Esporta i contatti.
	 * 
	 * @param listaContatti
	 * @param idAOO
	 * @return lista di contatti
	 */
	List<Contatto> esportaContatti(List<Contatto> listaContatti, Long idAOO);

	/**
	 * Ottiene il gruppo email tramite l'id.
	 * 
	 * @param idGruppo
	 * @return contatto
	 */
	Contatto getGruppoEmailByID(Long idGruppo);

	/**
	 * Conta il numero di notifiche contatti.
	 * 
	 * @param idAoo
	 * @param numGiorni
	 * @return numero di notifiche contatti
	 */
	int getCountListaNotificheRichieste(Long idAoo, int numGiorni);

	/**
	 * Ottiene i contatti preferiti.
	 * 
	 * @param idUfficio
	 * @param idAOO
	 * @param consideraGruppi
	 * @return lista di contatti
	 */
	List<Contatto> getPreferiti(Long idUfficio, Long idAOO, boolean consideraGruppi);

	/**
	 * Ottiene il contatto dalla mail mittente.
	 * 
	 * @param mailMittente
	 * @return contatto
	 */
	Contatto getContattoFromMailInterop(String mailMittente);

	/**
	 * Ricerca i contatti.
	 * 
	 * @param idUfficio
	 * @param ricercaContattoObj
	 * @param acceptNullValues
	 * @param connection
	 * @return lista di contatti
	 */
	List<Contatto> ricerca(Long idUfficio, RicercaRubricaDTO ricercaContattoObj, boolean acceptNullValues, Connection connection);

	/**
	 * Ricerca i contatti.
	 * 
	 * @param idUfficio
	 * @param ricercaContattoObj
	 * @param acceptNullValues
	 * @return lista di contatti
	 */
	List<Contatto> ricerca(Long idUfficio, RicercaRubricaDTO ricercaContattoObj, boolean acceptNullValues);

	/**
	 * Ricerca i contatti per campo singolo.
	 * 
	 * @param value
	 * @param idUfficio
	 * @param idAOO
	 * @param ricercaRED
	 * @param ricercaIPA
	 * @param ricercaMEF
	 * @param ricercaGruppo
	 * @param topN
	 * @return lista di contatti
	 */
	List<Contatto> ricercaCampoSingolo(String value, Long idUfficio, Integer idAOO, boolean ricercaRED, boolean ricercaIPA, boolean ricercaMEF, boolean ricercaGruppo,
			String topN);

	/**
	 * Ottiene il padre del contatto IPA.
	 * 
	 * @param contattoIPA
	 * @return contatto padre
	 */
	Contatto getPadreContattoIPA(Contatto contattoIPA);

	/**
	 * Crea la notifica di creazione contatto.
	 * 
	 * @param notifica
	 */
	void notificaCreazioneContatto(NotificaContatto notifica);

	/**
	 * Inserisce il contatto per la richiesta di creazione.
	 * 
	 * @param contatto
	 * @param dataDisattivazionePresente
	 * @return id del contatto
	 */
	Long inserisciContattoPerRichiestaCreazione(Contatto contatto, boolean dataDisattivazionePresente);

	/**
	 * Rimuove il contatto dalla richiesta di creazione.
	 * 
	 * @param idContatto
	 */
	void removeContattoRichiestaCreazione(Long idContatto);

	/**
	 * Modifica il contatto.
	 * 
	 * @param contatto
	 * @param notifica
	 * @param isApprovaRichiestaCreazione
	 * @param checkUnivocitaMail
	 */
	void modifica(Contatto contatto, NotificaContatto notifica, boolean isApprovaRichiestaCreazione, boolean checkUnivocitaMail);

	/**
	 * Ottiene le notifiche contatto.
	 * 
	 * @param idNodo
	 * @param numGiorni
	 * @param numRow
	 * @param utente
	 * @return notifiche contatto
	 */
	List<NotificaContatto> getNotifiche(Long idNodo, int numGiorni, int numRow, String utente);

	/**
	 * Controlla se un contatto con la stessa email è già presente all'interno del
	 * sistema per l'aoo.
	 * 
	 * @param contattoToCheck
	 * @param isInModifica
	 */
	void checkMailPerContattoGiaPresentePerAoo(Contatto contattoToCheck, boolean isInModifica);

	/**
	 * Rimuove i contatti dal gruppo.
	 * 
	 * @param gruppo
	 * @param contattiDaRimuovere
	 */
	void rimuoviDAGruppo(Contatto gruppo, List<Contatto> contattiDaRimuovere);

	/**
	 * Controlla se il contatto è già nei gruppi.
	 * 
	 * @param idgruppoToCheck
	 * @param idContattoToCheck
	 */
	void contattoGiaInGruppi(Collection<Contatto> idgruppoToCheck, Long idContattoToCheck);

	/**
	 * Aggiunge il contatto ai preferiti.
	 * 
	 * @param idUfficio
	 * @param contattoID
	 * @param tipoRubrica
	 * @param idUtente
	 */
	void aggiungiAiPreferiti(Long idUfficio, Long contattoID, TipoRubricaEnum tipoRubrica, Long idUtente);

	/**
	 * Inserisce il contatto nel flusso automatico.
	 * 
	 * @param inContatto
	 * @param utenteCreatore
	 * @return contatto
	 */
	Contatto insertContattoFlussoAutomatico(Contatto inContatto, UtenteDTO utenteCreatore);
	
	/**
	 * Esegue l'update dello stato della notifica nella tabella
	 * stato_notifica_rubrica.
	 * 
	 * @param idUtente
	 * @param idAoo
	 * @param idNotificaRubrica
	 * @return stato aggiornato
	 */
	Integer updateStatoNotificaRubrica(Long idUtente, Long idAoo, Integer idNotificaRubrica);

	/**
	 * Ottiene le notifiche di amministratore.
	 * 
	 * @param idNodo
	 * @param idAoo
	 * @param tipoNotifica
	 * @param numGiorni
	 * @param numRow
	 * @param idUtente
	 * @return lista di notifiche contatto
	 */
	List<NotificaContatto> getNotificheAdmin(Long idNodo, Long idAoo, String tipoNotifica, int numGiorni, Integer numRow, Long idUtente);
	
	/**
	 * Ricerca i contatti per campo singolo.
	 * 
	 * @param value
	 * @param idUfficio
	 * @param idAOO
	 * @param ricercaRED
	 * @param ricercaIPA
	 * @param ricercaMEF
	 * @param ricercaGruppo
	 * @param topN
	 * @param contattiDaEscludere
	 * @param idUfficioGruppo
	 * @param mostraPrimaPreferitiUfficio
	 * @return lista di contatti
	 */
	List<Contatto> ricercaCampoSingolo(String value, Long idUfficio, Integer idAOO, boolean ricercaRED, boolean ricercaIPA, boolean ricercaMEF, boolean ricercaGruppo,
			String topN, ArrayList<Long> contattiDaEscludere, Long idUfficioGruppo, boolean mostraPrimaPreferitiUfficio);
}
