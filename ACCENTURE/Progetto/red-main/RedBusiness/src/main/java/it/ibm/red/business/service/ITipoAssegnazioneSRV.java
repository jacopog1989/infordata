/**
 * 
 */
package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.ITipoAssegnazioneFacadeSRV;

/**
 * @author APerquoti
 *
 */
public interface ITipoAssegnazioneSRV extends ITipoAssegnazioneFacadeSRV {

}
