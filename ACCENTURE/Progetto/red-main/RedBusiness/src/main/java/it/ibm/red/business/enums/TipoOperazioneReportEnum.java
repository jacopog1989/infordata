package it.ibm.red.business.enums;

/**
 * Enum che definisce i tipi operazione dei report che possono essere generati.
 */
public enum TipoOperazioneReportEnum {
	
	/**
	 * Valore.
	 */
	UFF_PROT_ENTRATA_ASSEGNATI,
	
	/**
	 * Valore.
	 */
	UTE_PROT_ENTRATA_ASSEGNATI,
	
	/**
	 * Valore.
	 */
	UFF_DOC_USCITA_GENERATI,
	
	/**
	 * Valore.
	 */
	UTE_DOC_USCITA_GENERATI,
	
	/**
	 * Valore.
	 */
	UTE_DOC_FIRMATI,
	
	/**
	 * Valore.
	 */
	UFF_DOC_FIRMATI, 
	
	/**
	 * Valore.
	 */
	UTE_DOC_SIGLATI,
	
	/**
	 * Valore.
	 */
	UTE_DOC_VISTATI,
	
	/**
	 * Valore.
	 */
	UTE_DOC_AGLI_ATTI,
	
	/**
	 * Valore.
	 */
	UTE_DOC_CON_CONTRIBUTO,
	
	/**
	 * Valore.
	 */
	UTE_DOC_IN_CODA_LAVORO,
	
	/**
	 * Valore.
	 */
	UFF_DOC_CODA_LAVORO,
	
	/**
	 * Valore.
	 */
	UFF_UTENTI_ATTIVI_RUOLO,
	
	/**
	 * Valore.
	 */
	UFF_STAT_UFF,
	
	/**
	 * Valore.
	 */
	UTE_STAT_UFF,
	
	/**
	 * Valore.
	 */
	UFF_PROT_ENTR_GENERATI,
	
	/**
	 * Valore.
	 */
	UTE_PROT_PERVENUTI_COMPETENZA,
	
	/**
	 * Valore.
	 */
	UTE_PROT_INEVASI_COMPETENZA,
	
	/**
	 * Valore.
	 */
	UTE_PROT_LAVORATI_COMPETENZA,
	
	/**
	 * Valore.
	 */
	UFF_ELENCO_DIVISIONALE,
	
	/**
	 * Valore.
	 */
	UTE_ELENCO_DIVISIONALE;
	
	/**
	 * Restituisce l'enum associato al codice.
	 * @param codiceTipoOperazioneReport
	 * @return enum associato
	 */
	public static TipoOperazioneReportEnum get(final String codiceTipoOperazioneReport) {
		TipoOperazioneReportEnum output = null;
		
		 for (TipoOperazioneReportEnum tipoOperazioneReport : TipoOperazioneReportEnum.values()) {
			 if (tipoOperazioneReport.name().equals(codiceTipoOperazioneReport)) {
				 output = tipoOperazioneReport;
				 break;
			 }
		 }
		 
		 return output;
	}
	
	/**
	 * Restituisce true se tipo operazione associata al codice è report statistiche uffici.
	 * @param codiceTipoOperazioneReport
	 * @return true se statistiche uffici, false altrimenti
	 */
	public static boolean isStatisticheUffici(final String codiceTipoOperazioneReport) {
		
		return TipoOperazioneReportEnum.UFF_STAT_UFF.name().equals(codiceTipoOperazioneReport) 
				|| TipoOperazioneReportEnum.UTE_STAT_UFF.name().equals(codiceTipoOperazioneReport);
	}
	
	/**
	 * Restituisce true se tipo operazione associato al codice è report "Protocolli pervenuti per competenza", 
	 * "Protocolli inevasi per competenza" o "Protocolli lavorati per competenza".
	 * 
	 * @param codiceTipoOperazioneReport
	 * @return true se protocolli per competenza, false altrimenti
	 */
	public static boolean isProtocolliPerCompetenza(final String codiceTipoOperazioneReport) {
		return TipoOperazioneReportEnum.UTE_PROT_PERVENUTI_COMPETENZA.name().equals(codiceTipoOperazioneReport) 
				|| TipoOperazioneReportEnum.UTE_PROT_INEVASI_COMPETENZA.name().equals(codiceTipoOperazioneReport)
					|| TipoOperazioneReportEnum.UTE_PROT_LAVORATI_COMPETENZA.name().equals(codiceTipoOperazioneReport);
	}
	
	/**
	 * Restituisce true se tipo operazione associato al codice è report "Elenco divisionale".
	 *
	 * @param codiceTipoOperazioneReport
	 * @return true se elenco divisionale, false altrimenti
	 */
	public static boolean isElencoDivisionale(final String codiceTipoOperazioneReport) {
		return TipoOperazioneReportEnum.UFF_ELENCO_DIVISIONALE.name().equals(codiceTipoOperazioneReport) 
				|| TipoOperazioneReportEnum.UTE_ELENCO_DIVISIONALE.name().equals(codiceTipoOperazioneReport);
	}
	
}