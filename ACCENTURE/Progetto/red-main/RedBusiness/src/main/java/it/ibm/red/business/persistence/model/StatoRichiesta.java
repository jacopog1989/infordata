package it.ibm.red.business.persistence.model;

import java.io.Serializable;

/**
 * The Class StatoRichiesta.
 *
 * @author CPIERASC
 * 
 *         Entità che mappa la tabella STATORICHIESTA.
 */
public class StatoRichiesta implements Serializable {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Identificativo.
	 */
	private int statoRichiestaID;

	/**
	 * Descrizione.
	 */
	private String descrizione;

	/**
	 * Stepname.
	 */
	private String stepName;
	
	/**
	 * Costruttore.
	 * 
	 * @param inStatoRichiestaID	identificativo stato richiesta
	 * @param inDescrizione			descrizione stato richiesta
	 * @param inStepName			stepname
	 */
	public StatoRichiesta(final int inStatoRichiestaID, final String inDescrizione, final String inStepName) {
		super();
		this.statoRichiestaID = inStatoRichiestaID;
		this.descrizione = inDescrizione;
		this.stepName = inStepName;
	}


	/**
	 * Getter identificativo.
	 * 
	 * @return	identificativo
	 */
	public final int getStatoRichiestaID() {
		return statoRichiestaID;
	}

	/**
	 * Getter descrizione.
	 * 
	 * @return	descrizione
	 */
	public final String getDescrizione() {
		return descrizione;
	}

	/**
	 * Getter step name.
	 * 
	 * @return	step name
	 */
	public final String getStepName() {
		return stepName;
	}

}
