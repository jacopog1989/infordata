/**
 * 
 */
package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.AllaccioRiferimentoStoricoDTO;
import it.ibm.red.business.dto.RiferimentoProtNpsDTO;

/**
 * @author VINGENITO
 *
 */
public interface IRiferimentoStoricoFacadeSRV extends Serializable {

	/**
	 * Inserisce il riferimento storico.
	 * 
	 * @param allaccioRifStoricoDTO
	 */
	void inserisciRiferimentoStorico(List<AllaccioRiferimentoStoricoDTO> allaccioRifStoricoDTO);

	/**
	 * Cancella il riferimento storico.
	 * 
	 * @param documentTitle
	 * @param idAoo
	 */
	void deleteRiferimentoStoricoModifica(String documentTitle, Long idAoo);

	/**
	 * Elimina il riferimento allaccio NPS dal documento identificato dal <code> documentTitle </code>.
	 * @param documentTitle
	 * @param idAoo
	 */
	void deleteRiferimentoAllaccioNpsModifica(String documentTitle, Long idAoo);

	/**
	 * Restituisce i riferimenti NPS associati ad un documento identificato dai parametri in input.
	 * @param docTitle
	 * @param idAoo
	 * @param idFascicoloProcedimentale
	 * @return RiferimentoProtNpsDTO se esistente, null altrimenti
	 */
	List<RiferimentoProtNpsDTO> getRiferimentoNpsByAoo(String docTitle, Long idAoo, String idFascicoloProcedimentale);

	/**
	 * Effettua la insert del Riferimento Allaccio NPS.
	 * @param idAoo
	 * @param docTitleUscita
	 * @param allacciRiferimentoNps
	 * @param idFascicoloProc
	 */
	void insertRifAllaccioNps(Long idAoo, String docTitleUscita, List<RiferimentoProtNpsDTO> allacciRiferimentoNps,String idFascicoloProc);

}
