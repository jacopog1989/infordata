/**
 * 
 */
package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IRiferimentoStoricoDAO;
import it.ibm.red.business.dto.AllaccioRiferimentoStoricoDTO;
import it.ibm.red.business.dto.RiferimentoProtNpsDTO;
import it.ibm.red.business.dto.RiferimentoStoricoNsdDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * @author VINGENITO
 *
 */
@Repository
public class RiferimentoStoricoDAO extends AbstractDAO implements IRiferimentoStoricoDAO {

	private static final String ERRORE_DURANTE_IL_RECUPERO_DEI_DOCUMENTI = "Errore durante il recupero dei documenti ";

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -5855691807247793004L;

	/**
	 * Messaggio errore recupero documenti.
	 */
	private static final String ERROR_RECUPERO_DOCUMENTI_MSG = ERRORE_DURANTE_IL_RECUPERO_DEI_DOCUMENTI;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RiferimentoStoricoDAO.class.getName());

	/**
	 * @see it.ibm.red.business.dao.IRiferimentoStoricoDAO#insertRiferimentoStorico(java.lang.String,
	 *      java.lang.Integer, java.lang.String, java.util.Date, java.util.Date,
	 *      java.lang.String, java.lang.String, java.lang.Long, java.lang.String,
	 *      java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public int insertRiferimentoStorico(final String docTitleUscita, final Integer nProtocolloNsd, final String idFilePrincipale, final Date dataCreazione, final Date dataAgg,
			final String idFascicoloProc, final String descrFascicoloProc, final Long idAoo, final String oggettoProtNsd, final Integer annoProtNsd,
			final Connection connection) {
		PreparedStatement ps = null;
		try {
			int index = 1;

			ps = connection.prepareStatement(
					"INSERT INTO ALLACCIO_RIFERIMENTO_STORICO (DOCTITLEUSCITA, NPROTOCOLLONSD, IDFILEPRINCIPALE, DATA_CREAZIONE , DATA_AGG, ID_FASCICOLO_PROCEDIMENTALE, DESC_FASC_PROCEDIMENTALE, ID_AOO,OGGETTO_PROT_NSD,ANNO_PROT_NSD)"
							+ " VALUES (?,?,?,?,?,?,?,?,?,?)");
			ps.setString(index++, docTitleUscita);
			ps.setInt(index++, nProtocolloNsd);
			ps.setString(index++, idFilePrincipale);
			ps.setDate(index++, new java.sql.Date(dataCreazione.getTime()));
			ps.setDate(index++, new java.sql.Date(dataAgg.getTime()));
			ps.setString(index++, idFascicoloProc);
			ps.setString(index++, descrFascicoloProc);
			ps.setLong(index++, idAoo);
			ps.setString(index++, oggettoProtNsd);
			ps.setInt(index++, annoProtNsd);
			return ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'inserimento dell'allaccio: " + docTitleUscita, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IRiferimentoStoricoDAO#getRiferimentoStoricoByAoo(java.lang.String,
	 *      java.lang.Long, java.lang.String, java.sql.Connection).
	 */
	@Override
	public List<AllaccioRiferimentoStoricoDTO> getRiferimentoStoricoByAoo(final String docTitle, final Long idAoo, final String idFascProcedimentale, final Connection conn) {
		final List<AllaccioRiferimentoStoricoDTO> allaccioRifStoricoListDTO = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM ALLACCIO_RIFERIMENTO_STORICO WHERE DOCTITLEUSCITA=" + docTitle + " AND ID_AOO=" + idAoo + " AND ID_FASCICOLO_PROCEDIMENTALE = "
					+ idFascProcedimentale);

			ps = conn.prepareStatement(sb.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				final AllaccioRiferimentoStoricoDTO allaccioRifStoricoDTO = new AllaccioRiferimentoStoricoDTO(rs.getString("DOCTITLEUSCITA"), rs.getString("NPROTOCOLLONSD"),
						rs.getString("IDFILEPRINCIPALE"), rs.getDate("DATA_CREAZIONE"), rs.getDate("DATA_AGG"), rs.getString("ID_FASCICOLO_PROCEDIMENTALE"),
						rs.getString("DESC_FASC_PROCEDIMENTALE"), rs.getLong("ID_AOO"), rs.getString("OGGETTO_PROT_NSD"), rs.getInt("ANNO_PROT_NSD"));
				allaccioRifStoricoListDTO.add(allaccioRifStoricoDTO);
			}

		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_DOCUMENTI_MSG, e);
			throw new RedException(ERROR_RECUPERO_DOCUMENTI_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		return allaccioRifStoricoListDTO;
	}

	/**
	 * @see it.ibm.red.business.dao.IRiferimentoStoricoDAO#getRiferimentoStorico(java.lang.String,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<RiferimentoStoricoNsdDTO> getRiferimentoStorico(final String docTitle, final Long idAoo, final Connection conn) {
		final List<RiferimentoStoricoNsdDTO> rifStoricoListDTO = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM ALLACCIO_RIFERIMENTO_STORICO WHERE DOCTITLEUSCITA=" + docTitle + " and ID_AOO=" + idAoo + "");

			ps = conn.prepareStatement(sb.toString());
			rs = ps.executeQuery();
			while (rs.next()) {
				final RiferimentoStoricoNsdDTO rifStoricoDTO = new RiferimentoStoricoNsdDTO(rs.getInt("ANNO_PROT_NSD"), rs.getString("OGGETTO_PROT_NSD"),
						rs.getInt("NPROTOCOLLONSD"), rs.getString("IDFILEPRINCIPALE"));
				rifStoricoListDTO.add(rifStoricoDTO);
			}

		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_DOCUMENTI_MSG, e);
			throw new RedException(ERROR_RECUPERO_DOCUMENTI_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		return rifStoricoListDTO;
	}

	/**
	 * @see it.ibm.red.business.dao.IRiferimentoStoricoDAO#deleteRiferimentoStoricoModifica(java.lang.String,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public int deleteRiferimentoStoricoModifica(final String docTitleUscita, final Long idAoo, final Connection connection) {
		PreparedStatement ps = null;
		final String query = "DELETE FROM ALLACCIO_RIFERIMENTO_STORICO WHERE DOCTITLEUSCITA = ? AND ID_AOO = ? ";
		try {
			int index = 1;
			ps = connection.prepareStatement(query);
			ps.setString(index++, docTitleUscita);
			ps.setLong(index++, idAoo);
			return ps.executeUpdate();
		} catch (final Exception e) {
			LOGGER.error("Errore durante la cancellazione dell'allaccio: " + docTitleUscita, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IRiferimentoStoricoDAO#updateIdFascicoloProcedimentaleSposta(java.lang.String,
	 *      java.lang.String, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public int updateIdFascicoloProcedimentaleSposta(final String idFascicoloProcedimentale, final String docTitleUscita, final Long idAoo, final Connection conn) {
		PreparedStatement ps = null;
		int output = 0;
		try {
			ps = conn.prepareStatement("UPDATE ALLACCIO_RIFERIMENTO_STORICO SET ID_FASCICOLO_PROCEDIMENTALE = ? WHERE DOCTITLEUSCITA = ? and ID_AOO = ? ");

			int index = 1;
			ps.setString(index++, idFascicoloProcedimentale);
			ps.setString(index++, docTitleUscita);
			ps.setLong(index++, idAoo);

			output = ps.executeUpdate();
		} catch (final Exception ex) {
			LOGGER.error("Errore durante l'update dell'id del fascicolo procedimentale: " + docTitleUscita, ex);
			throw new RedException(ex);
		} finally {
			closeStatement(ps);
		}
		return output;

	}

	/**
	 * @see it.ibm.red.business.dao.IRiferimentoStoricoDAO#insertRifAllaccioNps(java.lang.Long,
	 *      java.lang.Integer, java.lang.Integer, java.util.Date, java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean insertRifAllaccioNps(final Long idAoo,final Integer annoProt,final Integer nProtocollo,final Date dataCreazione,final String docTitleUscita,
			final String idFascicoloProcedimentale,final String oggetto,final String idDocumentoDownload,final Connection connection) {
		PreparedStatement ps = null;
		boolean output = false;
		try {
			int index = 1;

			StringBuilder sb = new StringBuilder();
			sb.append("INSERT INTO ALLACCIO_RIFERIMENTO_NPS(IDAOO, ANNOPROT, NPROT, DATACREAZIONE ,DOCTITLEUSCITA,IDFASCICOLOPROCEDIMENTALE,OGGETTO,IDDOCUMENTODOWNLOAD) ");
			sb.append("VALUES (?,?,?,?,?,?,?,?)");
			
			ps = connection.prepareStatement(sb.toString());
			
			ps.setInt(index++, idAoo.intValue());
			ps.setInt(index++, annoProt);
			ps.setInt(index++, nProtocollo);
			ps.setDate(index++, new java.sql.Date(dataCreazione.getTime()));
			ps.setString(index++, docTitleUscita); 
			ps.setString(index++, idFascicoloProcedimentale); 
			ps.setString(index++, oggetto); 
			ps.setString(index++, idDocumentoDownload);
			
			output = ps.executeUpdate()>0;
		} catch (SQLException e) {
			LOGGER.error("Errore durante l'inserimento dell'allaccio: " + docTitleUscita,e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IRiferimentoStoricoDAO#deleteRiferimentoAllaccioNpsModifica(java.lang.String,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public boolean deleteRiferimentoAllaccioNpsModifica(final String docTitleUscita,final Long idAoo,final Connection connection) {
		PreparedStatement ps = null;
		boolean output = false;
		try {
			int index = 1;
			StringBuilder sb = new StringBuilder();
			sb.append("DELETE FROM ALLACCIO_RIFERIMENTO_NPS WHERE DOCTITLEUSCITA = ? AND IDAOO = ? ");
			ps = connection.prepareStatement(sb.toString());
			ps.setString(index++,docTitleUscita); 
			ps.setLong(index++,idAoo); 
			output = ps.executeUpdate()>0;
		}catch (Exception e) {
			LOGGER.error("Errore durante la cancellazione dell'allaccio: " + docTitleUscita,e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IRiferimentoStoricoDAO#getRiferimentoAllaccioNps(java.lang.String,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<RiferimentoProtNpsDTO> getRiferimentoAllaccioNps(final String docTitle,final Long idAoo,final Connection conn) {
 		List<RiferimentoProtNpsDTO> rifStoricoNpsDTO = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			StringBuilder sb = new StringBuilder(); 
			sb.append("SELECT * FROM ALLACCIO_RIFERIMENTO_NPS WHERE DOCTITLEUSCITA="+docTitle+" and IDAOO="+idAoo+""); 

			ps = conn.prepareStatement(sb.toString());
			rs = ps.executeQuery();
			while(rs.next()) {
				RiferimentoProtNpsDTO rifStoricoNps = new RiferimentoProtNpsDTO(rs.getInt("ANNOPROT"),rs.getInt("NPROT"),rs.getString("OGGETTO"),rs.getString("IDDOCUMENTODOWNLOAD"));
				rifStoricoNpsDTO.add(rifStoricoNps); 
			} 
		}
		catch (Exception e) {
			LOGGER.error(ERROR_RECUPERO_DOCUMENTI_MSG,e);
			throw new RedException(ERROR_RECUPERO_DOCUMENTI_MSG, e);
		}
		finally {
			closeStatement(ps, rs);
		}
		return rifStoricoNpsDTO;
	}

	/**
	 * @see it.ibm.red.business.dao.IRiferimentoStoricoDAO#getRiferimentoNpsByAoo(java.lang.String,
	 *      java.lang.Long, java.lang.String, java.sql.Connection).
	 */
	@Override
	public List<RiferimentoProtNpsDTO> getRiferimentoNpsByAoo(final String docTitle,final Long idAoo,final String idFascProcedimentale,final Connection conn) {
 		List<RiferimentoProtNpsDTO> allaccioRifNpsListDTO = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			int index = 1;
			StringBuilder sb = new StringBuilder(); 
			sb.append("SELECT * FROM ALLACCIO_RIFERIMENTO_NPS ");
			sb.append("WHERE DOCTITLEUSCITA = ? AND IDAOO = ? AND IDFASCICOLOPROCEDIMENTALE = ? "); 

			ps = conn.prepareStatement(sb.toString());
			
			ps.setString(index++, docTitle);
			ps.setInt(index++, idAoo.intValue());
			ps.setString(index++, idFascProcedimentale);
			rs = ps.executeQuery();
			while(rs.next()) {
				RiferimentoProtNpsDTO rifNpsDTO = new RiferimentoProtNpsDTO();
				rifNpsDTO.setNumeroProtocolloNps(rs.getInt("NPROT"));
				rifNpsDTO.setAnnoProtocolloNps(rs.getInt("ANNOPROT"));
				rifNpsDTO.setOggettoProtocolloNps(rs.getString("OGGETTO"));
				rifNpsDTO.setIdDocumentoDownload(rs.getString("IDDOCUMENTODOWNLOAD"));
				allaccioRifNpsListDTO.add(rifNpsDTO); 
			} 
		} catch (Exception e) {
			LOGGER.error(ERROR_RECUPERO_DOCUMENTI_MSG,e);
			throw new RedException(ERRORE_DURANTE_IL_RECUPERO_DEI_DOCUMENTI, e);
		} finally {
			closeStatement(ps, rs);
		}
		return allaccioRifNpsListDTO;
	}
}
