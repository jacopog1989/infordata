/**
 * 
 */
package it.ibm.red.business.dto;

/**
 * @author APerquoti
 *
 */
public class StatoRicercaDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7434650068604976868L;
	
	/**
	 * Contenuto del form della ricerca appena effettuata.
	 */
	private transient Object formRicerca;

	/**
	 * Risultati della ricerca appena effettuata.
	 */
	private transient Object risultatiRicerca;
	
	/**
	 * Costruttore.
	 * 
	 * @param inFormRicerca
	 * @param inRisultatiRicerca
	 */
	public StatoRicercaDTO(final Object inFormRicerca, final Object inRisultatiRicerca) {
		this.formRicerca = inFormRicerca;
		this.risultatiRicerca = inRisultatiRicerca;
	}

	/**
	 * @return the formRicerca
	 */
	public final Object getFormRicerca() {
		return formRicerca;
	}

	/**
	 * @return the risultatiRicerca
	 */
	public final Object getRisultatiRicerca() {
		return risultatiRicerca;
	}
	
}
