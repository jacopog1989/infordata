package it.ibm.red.business.enums;

import npstypes.v1.it.gov.mef.AllTipoRegistroAusiliarioType;

/**
 * The Enum TipoAllaccioEnum.
 *
 * @author CPIERASC
 *
 * Enum per la tipologia di allaccio.
 */
public enum TipoAllaccioEnum {

	/**
	 * Valore.
	 */
	RISPOSTA(1, null, null),

	/**
	 * Valore.
	 */
	INOLTRO(2, null, null),

	/**
	 * Valore.
	 */
	VISTO(3, AllTipoRegistroAusiliarioType.VISTO, AUTMessageIDEnum.VISTO),

	/**
	 * Valore.
	 */
	OSSERVAZIONE(4, AllTipoRegistroAusiliarioType.OSSERVAZIONE, AUTMessageIDEnum.OSSERVAZIONE),

	/**
	 * Valore.
	 */
	RICHIESTA_INTEGRAZIONE(5, AllTipoRegistroAusiliarioType.RICHIESTA_INTEGRAZIONE, AUTMessageIDEnum.RICHIESTA_INTEGRAZIONE),

	/**
	 * Valore.
	 */
	RESTITUZIONE(6, AllTipoRegistroAusiliarioType.RESTITUZIONE, AUTMessageIDEnum.RESTITUZIONE),
	
	/**
	 * Valore.
	 */
	RELAZIONE_POSITIVA(7, AllTipoRegistroAusiliarioType.VISTO, AUTMessageIDEnum.VISTO),
	
	/**
	 * Valore.
	 */
	RELAZIONE_NEGATIVA(8, AllTipoRegistroAusiliarioType.OSSERVAZIONE, AUTMessageIDEnum.OSSERVAZIONE),
	
	/**
	 * Valore.
	 */
	ENTRATA_ENTRATA(-1, null, null),
	
	/**
	 * Valore.
	 */
	ENTRATA_USCITA(-2, null, null),
	
	/**
	 * Valore.
	 */
	NOTIFICA(-3, null, null);

	/**
	 * Identificativo associato.
	 */
	private int tipoAllaccioId;

	/**
	 * Tipo di registro sul sistema di protocollo NPS
	 */
	private AllTipoRegistroAusiliarioType tipoRegistro;

	/**
	 * ID Messaggio per Flusso AUT
	 */
	private AUTMessageIDEnum messageIdFlussoAUT; 

	/**
	 * Costruttore.
	 * 
	 * @param id	identificativo associato
	 */
	TipoAllaccioEnum(final int id, final AllTipoRegistroAusiliarioType tipoRegistro, final AUTMessageIDEnum messageIdFlussoAUT) {
		tipoAllaccioId = id;
		this.tipoRegistro = tipoRegistro;
		this.messageIdFlussoAUT = messageIdFlussoAUT;
	}

	/**
	 * Getter identificativo associato.
	 * @return	identificativo associato
	 */
	public int getTipoAllaccioId() {
		return tipoAllaccioId;
	}

	/**
	 * Metodo per il recupero dell'enum a partire dall'identificativo associato.
	 *
	 * @param idTipoAllaccio	identificativo associato
	 * @return					enum associato all'identificativo
	 */
	public static TipoAllaccioEnum get(final Integer idTipoAllaccio) {
		TipoAllaccioEnum output = null;
		if (idTipoAllaccio != null) {
			for (TipoAllaccioEnum t:TipoAllaccioEnum.values()) {
				if (t.getTipoAllaccioId() == idTipoAllaccio) {
					output = t;
				}
			}
		}
		return output;
	}

	/**
	 * @return tipoRegistro
	 */
	public AllTipoRegistroAusiliarioType getTipoRegistro() {
		return tipoRegistro;
	}

	/**
	 * @return messageIdFlussoAUT
	 */
	public AUTMessageIDEnum getMessageIdFlussoAUT() {
		return messageIdFlussoAUT;
	}
}