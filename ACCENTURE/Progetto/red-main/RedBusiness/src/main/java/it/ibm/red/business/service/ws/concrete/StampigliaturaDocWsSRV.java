package it.ibm.red.business.service.ws.concrete;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.constants.Constants.Firma;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.adobe.AdobeLCHelper;
import it.ibm.red.business.helper.adobe.IAdobeLCHelper;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.signing.SignHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.PkHandler;
import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.concrete.AbstractService;
import it.ibm.red.business.service.ws.IStampigliaturaDocWsSRV;
import it.ibm.red.business.service.ws.auth.DocumentServiceAuthorizable;
import it.ibm.red.webservice.model.documentservice.dto.Servizio;
import it.ibm.red.webservice.model.documentservice.types.documentale.Posizione;
import it.ibm.red.webservice.model.documentservice.types.messages.BaseContentRequestType;
import it.ibm.red.webservice.model.documentservice.types.messages.InserisciApprovazioneType;
import it.ibm.red.webservice.model.documentservice.types.messages.InserisciCampoFirmaType;
import it.ibm.red.webservice.model.documentservice.types.messages.InserisciIdType;
import it.ibm.red.webservice.model.documentservice.types.messages.InserisciPostillaType;
import it.ibm.red.webservice.model.documentservice.types.messages.InserisciProtEntrataType;
import it.ibm.red.webservice.model.documentservice.types.messages.InserisciProtType;
import it.ibm.red.webservice.model.documentservice.types.messages.InserisciProtUscitaType;
import it.ibm.red.webservice.model.documentservice.types.messages.InserisciWatermarkType;

/**
 * Servizio per la stampigliatura dei documenti (protocollo entrata/uscita,
 * numero documento, postilla, campo firma) tramite i processi di Adobe
 * LiveCycle.
 * 
 * @author m.crescentini
 */
@Service
public class StampigliaturaDocWsSRV extends AbstractService implements IStampigliaturaDocWsSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -2802901963270876312L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(StampigliaturaDocWsSRV.class.getName());

	/**
	 * Servizio.
	 */
	@Autowired
	private IAooSRV aooSRV;

	/**
	 * @see it.ibm.red.business.service.ws.facade.IStampigliaturaDocWsFacadeSRV#
	 *      inserisciStampigliaturaProtEntrata
	 *      (it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedInserisciProtEntrataType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.INSERISCI_PROT_ENTRATA)
	public final FileDTO stampigliaProtocolloEntrata(final RedWsClient client, final InserisciProtEntrataType requestStampigliaProtEntrata) {
		return stampigliaDocumento(client, requestStampigliaProtEntrata, "la stampigliatura del protocollo in entrata");
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IStampigliaturaDocWsFacadeSRV#
	 *      stampigliaProtocolloUscita
	 *      (it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedInserisciProtUscitaType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.INSERISCI_PROT_USCITA)
	public final FileDTO stampigliaProtocolloUscita(final RedWsClient client, final InserisciProtUscitaType requestStampigliaProtUscita) {
		return stampigliaDocumento(client, requestStampigliaProtUscita, "la stampigliatura del protocollo in uscita");
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IStampigliaturaDocWsFacadeSRV#
	 *      inserisciCampoFirma (it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedInserisciCampoFirmaType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.INSERISCI_CAMPO_FIRMA)
	public final FileDTO inserisciCampoFirma(final RedWsClient client, final InserisciCampoFirmaType requestInserisciCampoFirma) {
		return stampigliaDocumento(client, requestInserisciCampoFirma, "l'inserimento del campo firma");
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IStampigliaturaDocWsFacadeSRV#
	 *      stampigliaIdDocumento
	 *      (it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedInserisciIdType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.INSERISCI_ID)
	public final FileDTO stampigliaIdDocumento(final RedWsClient client, final InserisciIdType requestStampigliaIdDocumento) {
		return stampigliaDocumento(client, requestStampigliaIdDocumento, "la stampigliatura dell'ID documento");
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IStampigliaturaDocWsFacadeSRV#
	 *      stampigliaPostilla (it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedInserisciPostillaType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.INSERISCI_POSTILLA)
	public final FileDTO stampigliaPostilla(final RedWsClient client, final InserisciPostillaType requestStampigliaPostilla) {
		return stampigliaDocumento(client, requestStampigliaPostilla, "la stampigliatura della postilla");
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IStampigliaturaDocWsFacadeSRV#
	 *      stampigliaWatermark (it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedInserisciWatermarkType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.INSERISCI_WATERMARK)
	public FileDTO stampigliaWatermark(final RedWsClient client, final InserisciWatermarkType requestStampigliaWatermark) {
		return stampigliaDocumento(client, requestStampigliaWatermark, "la stampigliatura del watermark");
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IStampigliaturaDocWsFacadeSRV#
	 *      stampigliaApprovazione
	 *      (it.ibm.red.business.persistence.model.RedWsClient,
	 *      it.ibm.red.webservice.model.documentservice.types.messages.
	 *      RedInserisciApprovazioneType).
	 */
	@Override
	@DocumentServiceAuthorizable(servizio = Servizio.INSERISCI_APPROVAZIONE)
	public FileDTO stampigliaApprovazione(final RedWsClient client, final InserisciApprovazioneType requestStampigliaApprovazione) {
		return stampigliaDocumento(client, requestStampigliaApprovazione, "la stampigliatura dell'approvazione");
	}

	private FileDTO stampigliaDocumento(final RedWsClient client, final BaseContentRequestType requestStampigliatura, final String operazione) {
		FileDTO fileWs = null;
		Connection con = null;
		final IFilenetCEHelper fceh = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			byte[] contentStampigliato = null;

			// Protocollo entrata
			if (requestStampigliatura instanceof InserisciProtEntrataType) {

				contentStampigliato = stampigliaProtocollo(requestStampigliatura.getContent(), requestStampigliatura.getContentType(), requestStampigliatura.getNomeFile(),
						((InserisciProtType) requestStampigliatura).getProtocolloStampigliatura(), true);

				// Protocollo uscita
			} else if (requestStampigliatura instanceof InserisciProtUscitaType) {

				contentStampigliato = stampigliaProtocollo(((InserisciProtUscitaType) requestStampigliatura).getIdAooRed(), requestStampigliatura.getContent(),
						requestStampigliatura.getContentType(), requestStampigliatura.getNomeFile(), ((InserisciProtType) requestStampigliatura).getProtocolloStampigliatura(),
						false);

				// Campo firma
			} else if (requestStampigliatura instanceof InserisciCampoFirmaType) {

				contentStampigliato = inserisciCampoFirma(requestStampigliatura.getContent(), requestStampigliatura.getContentType(), requestStampigliatura.getNomeFile(),
						((InserisciCampoFirmaType) requestStampigliatura).getFirmatari(), ((InserisciCampoFirmaType) requestStampigliatura).getAltezzaFooter(),
						((InserisciCampoFirmaType) requestStampigliatura).getSpaziaturaFirma());

				// ID (numero) documento
			} else if (requestStampigliatura instanceof InserisciIdType) {

				contentStampigliato = stampigliaIdDocumento(requestStampigliatura.getContent(), requestStampigliatura.getContentType(), requestStampigliatura.getNomeFile(),
						((InserisciIdType) requestStampigliatura).getIdDocumentoStampigliatura());

				// Postilla
			} else if (requestStampigliatura instanceof InserisciPostillaType) {

				contentStampigliato = stampigliaPostilla(((InserisciPostillaType) requestStampigliatura).getIdAooRed(), requestStampigliatura.getContent(),
						requestStampigliatura.getContentType(), requestStampigliatura.getNomeFile(), ((InserisciPostillaType) requestStampigliatura).getPostilla());

				// Approvazione
			} else if (requestStampigliatura instanceof InserisciApprovazioneType) {
			
				contentStampigliato = stampigliaApprovazione(requestStampigliatura.getContent(), requestStampigliatura.getContentType(), 
						((InserisciApprovazioneType) requestStampigliatura).getTesto(), 
						((InserisciApprovazioneType) requestStampigliatura).getPosizione());
			
			// Watermark
			} else if (requestStampigliatura instanceof InserisciWatermarkType) {

				contentStampigliato = stampigliaWatermark(requestStampigliatura.getContent(), ((InserisciWatermarkType) requestStampigliatura).getTesto());

			}

			fileWs = new FileDTO(requestStampigliatura.getNomeFile(), contentStampigliato, ContentType.PDF);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante " + operazione + ": " + e.getMessage(), e);
			throw new RedException("Errore durante " + operazione + ": " + e.getMessage(), e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}

		return fileWs;
	}

	/**
	 * @see it.ibm.red.business.service.ws.IStampigliaturaDocWsSRV#stampigliaProtocollo
	 *      (byte[], java.lang.String, java.lang.String, java.lang.String, boolean).
	 */
	@Override
	public byte[] stampigliaProtocollo(final byte[] content, final String contentType, final String nomeFile, final String protocolloStampigliatura, final boolean isEntrata) {

		return stampigliaProtocollo(null, content, contentType, nomeFile, protocolloStampigliatura, isEntrata);
	}

	/**
	 * @see it.ibm.red.business.service.ws.IStampigliaturaDocWsSRV#stampigliaProtocollo(java.lang.Integer,
	 *      byte[], java.lang.String, java.lang.String, java.lang.String, boolean).
	 */
	@Override
	public byte[] stampigliaProtocollo(final Integer idAOORED, final byte[] content, final String contentType, final String nomeFile, final String protocolloStampigliatura,
			final boolean isEntrata) {
		final IAdobeLCHelper adobeh = new AdobeLCHelper();
		Connection con = null;
		PkHandler pkHandlerTimbro = null;
		byte[] returnContent = null;

		if (idAOORED != null) {

			try {
				con = setupConnection(getDataSource().getConnection(), false);
				final Aoo aoo = aooSRV.recuperaAoo(idAOORED, con);
				pkHandlerTimbro = aoo.getPkHandlerTimbro();

				if (pkHandlerTimbro != null) {
					final SignHelper sh = new SignHelper(pkHandlerTimbro.getHandler(), pkHandlerTimbro.getSecurePin(), aoo.getDisableUseHostOnly());

					if (isEntrata) {
						returnContent = sh.applicaTimbroProtocolloPrincipaleEntrata(aoo.getSignerTimbro(), aoo.getPinTimbro(), content, protocolloStampigliatura, aoo.isConfPDFAPerHandler());
					} else {
						returnContent = sh.applicaTimbroProtocolloPrincipaleUscita(aoo.getSignerTimbro(), aoo.getPinTimbro(), content, protocolloStampigliatura, aoo.isConfPDFAPerHandler());
					}
				}
			} catch (final Exception e) {
				LOGGER.error("Si è verificato un errore durante la stampigliatura del protocollo: " + e.getMessage(), e);
				throw new RedException("Errore durante durante la stampigliatura del protocollo: " + e.getMessage(), e);
			} finally {
				closeConnection(con);

			}

		} else {

			returnContent = adobeh.insertProtocollo(content, contentType, nomeFile, protocolloStampigliatura);
		}

		return returnContent;
	}

	/**
	 * @see it.ibm.red.business.service.ws.IStampigliaturaDocWsSRV#stampigliaPostilla
	 *      (byte[], java.lang.String, java.lang.String, java.lang.String).
	 */
	@Override
	public byte[] stampigliaPostilla(final byte[] content, final String contentType, final String nomeFile, final String inPostilla) {

		return stampigliaPostilla(null, content, contentType, nomeFile, inPostilla);
	}

	/**
	 * @see it.ibm.red.business.service.ws.IStampigliaturaDocWsSRV#stampigliaPostilla(java.lang.Integer,
	 *      byte[], java.lang.String, java.lang.String, java.lang.String).
	 */
	@Override
	public byte[] stampigliaPostilla(final Integer idAOORED, final byte[] content, final String contentType, final String nomeFile, final String inPostilla) {

		String postilla = inPostilla;
		if (StringUtils.isBlank(postilla)) {
			postilla = Firma.TESTO_POSTILLA_STANDARD;
		}
		final IAdobeLCHelper adobeh = new AdobeLCHelper();
		Connection con = null;
		PkHandler pkHandlerTimbro = null;
		byte[] returnContent = null;

		if (idAOORED != null) {

			try {
				con = setupConnection(getDataSource().getConnection(), false);
				final Aoo aoo = aooSRV.recuperaAoo(idAOORED, con);
				pkHandlerTimbro = aoo.getPkHandlerTimbro();

				if (pkHandlerTimbro != null) {
					final SignHelper sh = new SignHelper(pkHandlerTimbro.getHandler(), pkHandlerTimbro.getSecurePin(), aoo.getDisableUseHostOnly());

					returnContent = sh.applicaTimbroPostilla(aoo.getSignerTimbro(), aoo.getPinTimbro(), content, aoo.isConfPDFAPerHandler());
				}
			} catch (final Exception e) {
				LOGGER.error("Si è verificato un errore durante la stampigliatura del protocollo: " + e.getMessage(), e);
				throw new RedException("Errore durante durante la stampigliatura del protocollo: " + e.getMessage(), e);
			} finally {
				closeConnection(con);
			}

		} else {
			returnContent = adobeh.insertPostilla(content, contentType, nomeFile, postilla);
		}

		return returnContent;
	}

	/**
	 * @see it.ibm.red.business.service.ws.IStampigliaturaDocWsSRV#stampigliaIdDocumento
	 *      (byte[], java.lang.String, java.lang.String, java.lang.String).
	 */
	@Override
	public byte[] stampigliaIdDocumento(final byte[] content, final String contentType, final String nomeFile, final String inIdDocumentoStampigliatura) {
		final IAdobeLCHelper adobeh = new AdobeLCHelper();

		Integer idDocumentoStampigliatura = null;
		if (NumberUtils.isCreatable(inIdDocumentoStampigliatura)) {
			idDocumentoStampigliatura = NumberUtils.createInteger(inIdDocumentoStampigliatura);
		} else {
			throw new RedException("Errore durante la conversione dell'ID documento in input in un numero");
		}

		return adobeh.insertIdDocumento(content, contentType, nomeFile, idDocumentoStampigliatura);
	}

	/**
	 * @see it.ibm.red.business.service.ws.IStampigliaturaDocWsSRV#inserisciCampoFirma
	 *      (byte[], java.lang.String, java.lang.String, java.util.List,
	 *      java.lang.Integer, java.lang.Integer).
	 */
	@Override
	public byte[] inserisciCampoFirma(final byte[] content, final String contentType, final String nomeFile, final List<Integer> firmatari, final Integer altezzaFooter,
			final Integer spaziaturaFirma) {
		final IAdobeLCHelper adobeh = new AdobeLCHelper();

		return adobeh.insertCampoFirma(content, contentType, nomeFile, firmatari, altezzaFooter, spaziaturaFirma);
	}

	/**
	 * @param content
	 * @param contentType
	 * @param testoApprovazione
	 * @param posizione
	 * @return
	 */
	@Override
	public byte[] stampigliaApprovazione(final byte[] content, final String contentType, final String testoApprovazione, final Posizione posizione) {
		final IAdobeLCHelper adobeh = new AdobeLCHelper();
		
		// Gestione della posizione dell'approvazione all'interno del documento
		boolean toRight = false;
		if (Posizione.RIGHT.equals(posizione)) {
			toRight = true;
		}

		return adobeh.insertApprovazione(content, contentType, testoApprovazione, toRight);
	}

	/**
	 * @param requestStampigliatura
	 * @return
	 */
	@Override
	public byte[] stampigliaWatermark(final byte[] content, final String testoWatermark) {
		final IAdobeLCHelper adobeh = new AdobeLCHelper();

		return adobeh.insertWatermark(content, testoWatermark);
	}
}