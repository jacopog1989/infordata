package it.ibm.red.business.dto;
  
import java.util.Map;

import it.ibm.red.business.persistence.model.UtentePreferenze;

/**
 * The Class PreferenzeDTO.
 */
public class PreferenzeDTO extends AbstractDTO {

	private static final long serialVersionUID = -537658913961126760L;

	/**
	 * Delay autocomplete
	 */
	private Integer autocompleteDelay;
	
	/**
	 * Identificativo tema red
	 */
	private Integer idTheme;
	
	/**
	 * Size text
	 */
	private Integer textSize;
	
	/**
	 * Nome tema
	 */
	private String theme;
	
	/**
	 * Homepage
	 */
	private String homepage;
	
	/**
	 * Pagina preferita
	 */
	private String paginaPreferita;
	
	/**
	 * Menu tiles
	 */
	private Map<String, Boolean> menuTiles;
//	<- Flag Gestione menu ->
	/**
	 * Scrivania gruppo
	 */
	private boolean groupScrivania;
	
	/**
	 * Gruppo archivio
	 */
	private boolean groupArchivio;
	
	/**
	 * Gruppo ufficio
	 */
	private boolean groupUfficio;
	
	/**
	 * Gruppo mail
	 */
	private boolean groupMail;
	
	/**
	 * Gruppo fatture elettronica
	 */
	private boolean groupFatElettronica;
	
	/**
	 * Gruppo organigramma
	 */
	private boolean groupOrganigrama;
	
	/**
	 * Flag abilita pdf nativo
	 */
	private boolean nativeReaderPDFEnable;
	
	/**
	 * Dettaglio full abilitato
	 */
	private boolean fullDetailEnable = true;
	
	//FLAG GESTIONE ERRORE 500
	/**
	 * Codifica base 64
	 */
	private boolean base64;
	
	/**
	 * Codifica url encoding
	 */
	private boolean urlEncoding;
	
	/**
	 * Nessun cryp
	 */
	private boolean noCrypt;
	
	/**
	 *Flag firma remota come prima firma
	 */
	private boolean firmaRemotaFirst;   
//	<- Flag Gestione menu ->

	
	/**
	 * Costruttore preferenze DTO.
	 */
	public PreferenzeDTO() {
		super();
	}
	
	/**
	 * Costruttore preferenze DTO.
	 *
	 * @param up the up
	 */
	public PreferenzeDTO(final UtentePreferenze up) {
		super();
		
		if (up.getAutocompleteDelay() == 0) {
			this.autocompleteDelay = 600;
		} else {
			this.autocompleteDelay = up.getAutocompleteDelay();
		}
		
		if (up.getTextSize() == 0) {
			this.textSize = 2;
		} else {
			this.textSize = up.getTextSize();
		}
		
		this.idTheme = up.getIdTema();
		this.homepage = up.getStartPage();
		this.paginaPreferita = up.getPaginaPreferita();
		
		this.groupScrivania = up.isGroupScrivania();
		this.groupArchivio = up.isGroupArchivio();
		this.groupUfficio = up.isGroupUfficio();
		this.groupMail = up.isGroupMail();
		this.groupFatElettronica = up.isGroupFatEletronica();
		this.groupOrganigrama = up.isGroupOrganigramma();

		nativeReaderPDFEnable = up.isNativeReaderPDFenable();
		fullDetailEnable = true;
 
		this.base64 = up.getBase64();
		this.urlEncoding = up.getUrlEncoding();
		this.noCrypt = up.getNoCrypt();
		
		this.firmaRemotaFirst = up.isFirmaRemotaFirst();
	}

	/** 
	 *
	 * @return true, if is native reader PDF enable
	 */
	public boolean isNativeReaderPDFEnable() {
		return nativeReaderPDFEnable;
	}

	/** 
	 *
	 * @param nativeReaderPDFEnable the new native reader PDF enable
	 */
	public void setNativeReaderPDFEnable(final boolean nativeReaderPDFEnable) {
		this.nativeReaderPDFEnable = nativeReaderPDFEnable;
	}

	/**  
	 * @return true, if is full detail enable
	 */
	public boolean isFullDetailEnable() {
		return fullDetailEnable;
	}

	/** 
	 * @param fullDetailEnable the new full detail enable
	 */
	public void setFullDetailEnable(final boolean fullDetailEnable) {
		this.fullDetailEnable = fullDetailEnable;
	}
	
	/** 
	 * @return the autocomplete delay
	 */
	public Integer getAutocompleteDelay() {
		return autocompleteDelay;
	}
	
	/** 
	 * @param autocompleteDelay the new autocomplete delay
	 */
	public void setAutocompleteDelay(final Integer autocompleteDelay) {
		this.autocompleteDelay = autocompleteDelay;
	}
	
	/** 
	 * @return the text size
	 */
	public Integer getTextSize() {
		return textSize;
	}
	
	/** 
	 * @param textSize the new text size
	 */
	public void setTextSize(final Integer textSize) {
		this.textSize = textSize;
	}
	
	/** 
	 * @return the theme
	 */
	public String getTheme() {
		return theme;
	}
	
	/** 
	 * @param theme the new theme
	 */
	public void setTheme(final String theme) {
		this.theme = theme;
	}
	
	/** 
	 * @return the id theme
	 */
	public final Integer getIdTheme() {
		return idTheme;
	}
	
	/** 
	 * @param idTheme the new id theme
	 */
	public final void setIdTheme(final Integer idTheme) {
		this.idTheme = idTheme;
	}
	
	/** 
	 * @return the homepage
	 */
	public String getHomepage() {
		return homepage;
	}
	
	/** 
	 * @param homepage the new homepage
	 */
	public void setHomepage(final String homepage) {
		this.homepage = homepage;
	}
	
	/** 
	 * @return the pagina preferita
	 */
	public final String getPaginaPreferita() {
		return paginaPreferita;
	}
	
	/** 
	 * @param paginaPreferita the new pagina preferita
	 */
	public final void setPaginaPreferita(final String paginaPreferita) {
		this.paginaPreferita = paginaPreferita;
	}
	
	/** 
	 * @return the menu tiles
	 */
	public Map<String, Boolean> getMenuTiles() {
		return menuTiles;
	}
	
	/** 
	 * @param menuTiles the menu tiles
	 */
	public void setMenuTiles(final Map<String, Boolean> menuTiles) {
		this.menuTiles = menuTiles;
	}

	/** 
	 * @return true, if is group scrivania
	 */
	public final boolean isGroupScrivania() {
		return groupScrivania;
	}
	
	/** 
	 * @param groupScrivania the new group scrivania
	 */
	public final void setGroupScrivania(final boolean groupScrivania) {
		this.groupScrivania = groupScrivania;
	}
	
	/** 
	 * @return true, if is group archivio
	 */
	public final boolean isGroupArchivio() {
		return groupArchivio;
	}
	
	/** 
	 * @param groupArchivio the new group archivio
	 */
	public final void setGroupArchivio(final boolean groupArchivio) {
		this.groupArchivio = groupArchivio;
	}
	
	/** 
	 * @return true, if is group ufficio
	 */
	public final boolean isGroupUfficio() {
		return groupUfficio;
	}
	
	/**  
	 * @param groupUfficio the new group ufficio
	 */
	public final void setGroupUfficio(final boolean groupUfficio) {
		this.groupUfficio = groupUfficio;
	}
	
	/** 
	 * @return true, if is group mail
	 */
	public final boolean isGroupMail() {
		return groupMail;
	}
	
	/** 
	 * @param groupMail the new group mail
	 */
	public final void setGroupMail(final boolean groupMail) {
		this.groupMail = groupMail;
	}
	
	/** 
	 * @return true, if is group fat elettronica
	 */
	public final boolean isGroupFatElettronica() {
		return groupFatElettronica;
	}
	
	/** 
	 * @param groupFatElettronica the new group fat elettronica
	 */
	public final void setGroupFatElettronica(final boolean groupFatElettronica) {
		this.groupFatElettronica = groupFatElettronica;
	}
	
	/** 
	 * @return true, if is group organigrama
	 */
	public final boolean isGroupOrganigrama() {
		return groupOrganigrama;
	}
	
	/** 
	 * @param groupOrganigrama the new group organigrama
	 */
	public final void setGroupOrganigrama(final boolean groupOrganigrama) {
		this.groupOrganigrama = groupOrganigrama;
	}
 
	/** 
	 * @return the base 64
	 */
	public boolean getBase64() {
		return base64;
	}
	
	/** 
	 * @param base64 the new base 64
	 */
	public void setBase64(final boolean base64) {
		this.base64 = base64;
	}
	
	/** 
	 * @return the url encoding
	 */
	public boolean getUrlEncoding() {
		return urlEncoding;
	}
	
	/** 
	 * @param urlEncoding the new url encoding
	 */
	public void setUrlEncoding(final boolean urlEncoding) {
		this.urlEncoding = urlEncoding;
	}
	
	/** 
	 * @return the no crypt
	 */
	public boolean getNoCrypt() {
		return noCrypt;
	}
	
	/** 
	 * @param noCrypt the new no crypt
	 */
	public void setNoCrypt(final boolean noCrypt) {
		this.noCrypt = noCrypt;
	}
	
	/** 
	 * @return true, if is firma remota first
	 */
	public boolean isFirmaRemotaFirst() {
		return firmaRemotaFirst;
	}
	
	/** 
	 * @param firmaRemotaFirst the new firma remota first
	 */
	public void setFirmaRemotaFirst(final boolean firmaRemotaFirst) {
		this.firmaRemotaFirst = firmaRemotaFirst;
	}
	 
}
