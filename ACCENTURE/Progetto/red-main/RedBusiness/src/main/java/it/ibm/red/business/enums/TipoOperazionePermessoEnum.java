package it.ibm.red.business.enums;

/**
 * Enum che definisce i permessi sui tipi operazione.
 */
public enum TipoOperazionePermessoEnum {
	
	/**
	 * Valore.
	 */
	CREAZIONE_DOCUMENTO(1001, 1068, 1024),
	
	/**
	 * Valore.
	 */
	COMPETENZA(1004, 1011),
	
	/**
	 * Valore.
	 */
	CONOSCENZA(1005),
	
	/**
	 * Valore.
	 */
	CONTRIBUTO(1093, 1081, 1083, 1018, 1082, 1019, 1074, 1092, 1069, 1091),
	
	/**
	 * Valore.
	 */
	FIRMA(1006, 3005, 1014, 1085, 1025),
	
	/**
	 * Valore.
	 */
	SIGLA(1007,	3018, 1015),
	
	/**
	 * Valore.
	 */
	VISTO(1061, 1076, 1016, 1062),
	
	/**
	 * Valore.
	 */
	SPEDIZIONE(1067, 1084, 1085, 1088, 1020),
	
	/**
	 * Valore.
	 */
	COPIA_CONFORME(1094, 1103, 1102);
	
	
	/**
	 * Identificativo.
	 */
	private Integer[] id;
	
	/**
	 * Costruttore.
	 * @param id
	 */
	TipoOperazionePermessoEnum(final Integer...id) {
		this.id = id;
	}
	
	/**
	 * Restituisce l'enum associato all'Id.
	 * @param id
	 * @return enum associato all'id
	 */
	public static TipoOperazionePermessoEnum getById(final Integer id) {
		
		TipoOperazionePermessoEnum t = null;
		if (id == null) {
			return t;
		}
		for (TipoOperazionePermessoEnum t1 : TipoOperazionePermessoEnum.values()) {
			for (Integer t1Id : t1.id) {
				if (t1Id.intValue() == id.intValue()) {
					t = t1;
					break;
				}
			}
		}
		
		return t;
	}

	/**
	 * Restituisce la lista degli id.
	 * @return id
	 */
	public Integer[] getId() {
		return id;
	}
}
