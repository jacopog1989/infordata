package it.ibm.red.business.dao.impl;

import java.io.OutputStream;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.ICalendarioDAO;
import it.ibm.red.business.dto.RuoloDTO;
import it.ibm.red.business.dto.SearchEventiDTO;
import it.ibm.red.business.enums.EventoCalendarioEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Calendario;
import it.ibm.red.business.persistence.model.Evento;
import it.ibm.red.business.persistence.model.TestiDefault;

/**
 * Dao per la gestione del calendario.
 */
@Repository
public class CalendarioDAO extends AbstractDAO implements ICalendarioDAO {

	/**
	 * Serializzazione.
	 */
	private static final long serialVersionUID = -1523661835729618448L;
	
	/**
	 * Colonna Id categoria.
	 */
	private static final String IDCATEGORIA = "idcategoria";

	/**
	 * Colonna Descrizione.
	 */
	private static final String DESCRIZIONE = "descrizione";

	/**
	 * Colonna Codice.
	 */
	private static final String CODICE = "codice";

	/**
	 * Colonna Id tipo.
	 */
	private static final String ID_TIPO = "id_tipo";

	/**
	 * Colonna Titolo.
	 */
	private static final String TITOLO = "titolo";

	/**
	 * Colonna Contenuto.
	 */
	private static final String CONTENUTO = "contenuto";

	/**
	 * Colonna Id evento.
	 */
	private static final String IDEVENTO = "idevento";

	/**
	 * Codice uguale placeholder.
	 */
	private static final String TC_CODICE_UGUALE_PLACEHOLDER = " tc.codice = ? ";

	/**
	 * Label - And.
	 */
	private static final String AND_PAR_DX_AP = " and ( ";

	/**
	 * Query parziale.
	 */
	private static final String AND_UPPER_C_TITOLO_LIKE_UPPER = " and (upper(c.titolo) like upper('%'||?||'%')) ";

	/**
	 * Query parziale.
	 */
	private static final String TC_ID_TIPO_1_AND_CHECKVISIBILITAEVENTI_C_IDNODO_C_IDRUOLO_0 = " (TC.ID_TIPO = 1 AND CHECKVISIBILITAEVENTI(C.IDNODO, C.IDRUOLO, ?, ?, ?)>0))";

	/**
	 * Query parziale.
	 */
	private static final String AND_C_IDENTE_AND_C_IDUTENTE_OR_TC_ID_TIPO_1_AND_CHECKVISIBILITANEWS_C_IDEVENTO_0_OR = "AND C.IDENTE = ? AND (C.IDUTENTE = ? OR (TC.ID_TIPO <> 1 AND CHECKVISIBILITANEWS(C.IDEVENTO, ?, ?)>0) OR ";

	/**
	 * Query parziale.
	 */
	private static final String WHERE_C_TIPOCALENDARIO_ID_TC_ID_TIPO_AND_C_IDAOO = "WHERE C.TIPOCALENDARIO_ID = TC.ID_TIPO AND C.IDAOO = ? ";

	/**
	 * Query parziale.
	 */
	private static final String JOIN_TIPOCALENDARIO_TC_ON_C_TIPOCALENDARIO_ID_TC_ID_TIPO = "JOIN TIPOCALENDARIO TC ON C.TIPOCALENDARIO_ID = TC.ID_TIPO ";

	/**
	 * Query parziale.
	 */
	private static final String FROM_CALENDARIO_C_LEFT_OUTER_JOIN_STATO_NOTIFICA_CALENDARIO_SNC_ON_C_IDEVENTO_SNC_IDEVENTO = "FROM CALENDARIO C LEFT OUTER JOIN STATO_NOTIFICA_CALENDARIO SNC ON C.IDEVENTO = SNC.IDEVENTO ";

	/**
	 * Id utente uguale placeholder.
	 */
	private static final String AND_SNC_IDUTENTE = " AND SNC.IDUTENTE = ? ";

	/**
	 * Colonna Allegato.
	 */
	private static final String ALLEGATO = "allegato";

	/**
	 * Messaggio errore aggiornamento evento calendario, occorre appendere l'id dell'evento al messaggio.
	 */
	private static final String ERROR_AGGIORNAMENTO_EVENTO_MSG = "Errore durante l'aggiornamento dell'evento del calendario tramite ID = ";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(CalendarioDAO.class);
	
	/**
	 * Insert nodi target.
	 */
	private static final String SQL_INSERT_NODI_TARGET = "INSERT INTO calendario_nodo_target ( idevento, idnodo ) VALUES (? , ?)";
	
	/**
	 * Insert ruoli.
	 */
	private static final String SQL_INSERT_RUOLI_TARGET = "INSERT INTO calendario_ruolocat_target ( idevento, idcategoria ) VALUES (? , ?)";
	
	/**
	 * Recupero ruoli.
	 */
	private static final String SQL_GET_RUOLI_TARGET = "SELECT * FROM calendario_ruolocat_target WHERE idevento = ?";
	
	/**
	 * Recupero nodi.
	 */
	private static final String SQL_GET_NODI_TARGET = "SELECT * FROM calendario_nodo_target WHERE idevento = ?";
	
	/**
	 * Recupero testi di default.
	 */
	private static final String SQL_GET_TESTI_DEFAULT = "SELECT * FROM calendario_testi_default";
	
	/**
	 * Insert.
	 */
	private static final String SQL_INSERT = "call P_INSERTEVENTOCALENDARIO(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
	
	/**
	 * Delete.
	 */
	private static final String SQL_DELETE = "DELETE FROM calendario WHERE idevento = ?";
	
	/**
	 * Update.
	 */
	private static final String SQL_UPDATE = "UPDATE calendario SET titolo = ?, contenuto = ?, link_esterno = ?, "
											  + "datainizio = ?, datascadenza = ?, datatime = ?, allegato = ?," 
											  + " nome_allegato = ?, estensione_allegato = ? WHERE idevento = ?";
	
	/**
	 * Recupero categorie e ruoli.
	 */
	private static final String SQL_GET_CATEGORIERUOLI = "SELECT * FROM TIPOCATEGORIARUOLO WHERE AOO= ?";
	
	/**
	 * Recupero tipologie.
	 */
	private static final String SQL_GETTIPOLOGIE = "SELECT * FROM tipocalendario";
	
	/**
	 * Delete ruoli.
	 */
	private static final String SQL_DELETE_RUOLI_TARGET = "DELETE FROM calendario_ruolocat_target WHERE idevento = ?";
	
	/**
	 * Delete nodi.
	 */
	private static final String SQL_DELETE_NODI_TARGET = "DELETE FROM calendario_nodo_target WHERE idevento = ?";

	/**
	 * Recupero allegato.
	 */
	private static final String SQL_GET_ALLEGATO = "SELECT allegato FROM calendario c WHERE idevento = ?";

	/**
	 * @see it.ibm.red.business.dao.ICalendarioDAO#getAllegatoById(java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public byte[] getAllegatoById(final Integer idEvento, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		byte[] allegato = null;
		try {
			ps = con.prepareStatement(SQL_GET_ALLEGATO);
			
			int index = 1;
			ps.setInt(index++, idEvento.intValue());
			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				
				final Blob allegatoBlob = rs.getBlob(ALLEGATO);
				if (allegatoBlob != null) {
					allegato = allegatoBlob.getBytes(1, (int) allegatoBlob.length());
				}
			}
			
			return allegato;
			
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero allegato dell'evento id --> " + idEvento, e);
		} finally {
			closeStatement(ps, rs);
		}
		
	}

	/**
	 * @see it.ibm.red.business.dao.ICalendarioDAO#getEventi(it.ibm.red.business.dto.SearchEventiDTO,
	 *      java.lang.Long, java.lang.Long, java.lang.Long, java.lang.Long,
	 *      java.lang.Integer, boolean, java.sql.Connection).
	 */
	@Override
	public List<Evento> getEventi(final SearchEventiDTO searchBean, final Long idUtente, final Long idUfficio, final Long idRuolo, final Long idAoo,
			final Integer idEnte, final boolean withContent, final Connection con) {
		final List<Evento> list = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT C.*, TC.*,SNC.STATO, (SELECT TF.MIMETYPE FROM TIPOFILE TF WHERE TF.ESTENSIONE = C.ESTENSIONE_ALLEGATO AND ROWNUM < 2) AS MIMETYPE_ALLEGATO "); 
			sb.append(queryEventiCommon());
			 
			if (searchBean.getDataInizio() != null) {
				sb.append(" and c.datascadenza >= ? ");
			}
			
			if (searchBean.getDataFine() != null) {
				sb.append(" and c.datainizio <= ? ");
			}
			
			sb.append(andDescrizioneAndTipiEvento(searchBean));

			sb.append(" ORDER BY c.datascadenza DESC, c.datainizio desc ");

			ps = con.prepareStatement(sb.toString());
			
			int index = 1;
			index = setParametersPreparedStatement(idUtente, idUfficio, idRuolo, idAoo, idEnte, ps, index);
			
			if (searchBean.getDataInizio() != null) {
				ps.setTimestamp(index++, new Timestamp((searchBean.getDataInizio()).getTime()));
			}
			
			if (searchBean.getDataFine() != null) {
				ps.setTimestamp(index++, new Timestamp((searchBean.getDataFine()).getTime()));
			}
			
			if (searchBean.getDescrizioneLike() != null && !"".equals(searchBean.getDescrizioneLike())) {
				ps.setString(index++, searchBean.getDescrizioneLike());
			}
			
			if (searchBean.getTipiEvento() != null && !searchBean.getTipiEvento().isEmpty()) {
				for (final EventoCalendarioEnum tipo: searchBean.getTipiEvento()) {
					ps.setString(index++, tipo.getCodice());
				}
			}
			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				list.add(handleResultSet(withContent, rs, con));
			}

			return list;

		} catch (final SQLException e) {
			LOGGER.error("Errore nel recupero degli eventi Calendario.", e);
		} finally {
			closeStatement(ps, rs);
		}

		return list;
	}

	/**
	 * @param sb
	 */
	private static String queryEventiCommon() {
		StringBuilder sb = new StringBuilder();
		sb.append(FROM_CALENDARIO_C_LEFT_OUTER_JOIN_STATO_NOTIFICA_CALENDARIO_SNC_ON_C_IDEVENTO_SNC_IDEVENTO);
		sb.append(AND_SNC_IDUTENTE);
		sb.append(JOIN_TIPOCALENDARIO_TC_ON_C_TIPOCALENDARIO_ID_TC_ID_TIPO);
		sb.append(WHERE_C_TIPOCALENDARIO_ID_TC_ID_TIPO_AND_C_IDAOO);
		sb.append(AND_C_IDENTE_AND_C_IDUTENTE_OR_TC_ID_TIPO_1_AND_CHECKVISIBILITANEWS_C_IDEVENTO_0_OR);
		sb.append(TC_ID_TIPO_1_AND_CHECKVISIBILITAEVENTI_C_IDNODO_C_IDRUOLO_0);
		return sb.toString();
	}
	
	/**
	 * @see it.ibm.red.business.dao.ICalendarioDAO#getEventiHomepage(it.ibm.red.business.dto.SearchEventiDTO,
	 *      java.lang.Long, java.lang.Long, java.lang.Long, java.lang.Long,
	 *      java.lang.Integer, boolean, java.sql.Connection).
	 */
	@Override
	public List<Evento> getEventiHomepage(final SearchEventiDTO searchBean, final Long idUtente, final Long idUfficio, final Long idRuolo, final Long idAoo,
			final Integer idEnte, final boolean withContent, final Connection con) {
		final List<Evento> list = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM (SELECT C.*, TC.*,SNC.STATO, (SELECT TF.MIMETYPE FROM TIPOFILE TF WHERE TF.ESTENSIONE = C.ESTENSIONE_ALLEGATO) AS MIMETYPE_ALLEGATO ");
			sb.append(queryEventiCommon());
			sb.append(" AND (SNC.stato IS NULL OR SNC.STATO = 0)");
			
			andDateNotificheNonLetteAndDescrizioneAndTipiEvento(searchBean, sb);

			sb.append("UNION ALL ");
			sb.append("SELECT C.*, TC.*,SNC.STATO, (SELECT TF.MIMETYPE FROM TIPOFILE TF WHERE TF.ESTENSIONE = C.ESTENSIONE_ALLEGATO) AS MIMETYPE_ALLEGATO "); 
			sb.append(queryEventiCommon());
			sb.append(" AND (SNC.stato =1 AND c.datascadenza >= ?  and c.datainizio <= ? )");
			 
			sb.append(andDescrizioneAndTipiEvento(searchBean));
			
			if (searchBean.getMaxResults() > 0) {
				sb.append(" AND ROWNUM < ?"); 	
			}
			
			sb.append(" )RISULTATI");
			sb.append(" ORDER BY RISULTATI.datascadenza DESC, RISULTATI.datainizio desc ");

			ps = con.prepareStatement(sb.toString());
			
			int index = 1;
			index = setParametersPreparedStatement(idUtente, idUfficio, idRuolo, idAoo, idEnte, ps, index);
			
			index = setDateNotificheNonLetteAndTipiEventoPs(searchBean, ps, index);
			
			index = setParametersPreparedStatement(idUtente, idUfficio, idRuolo, idAoo, idEnte, ps, index);
			
			ps.setTimestamp(index++, new Timestamp((searchBean.getDataInizio()).getTime()));
			ps.setTimestamp(index++, new Timestamp((searchBean.getDataFine()).getTime()));
			
			if (searchBean.getTipiEvento() != null && !searchBean.getTipiEvento().isEmpty()) {
				for (final EventoCalendarioEnum tipo: searchBean.getTipiEvento()) {
					ps.setString(index++, tipo.getCodice());
				}
			}
			if (searchBean.getMaxResults() > 0) {
				ps.setInt(index++, searchBean.getMaxResults());	
			}

			rs = ps.executeQuery();
			
			while (rs.next()) {
				list.add(handleResultSet(withContent, rs, con));
			}
			return list;

		} catch (final SQLException e) {
			LOGGER.error("Errore nel recupero degli eventi Calendario.", e);
		} finally {
			closeStatement(ps, rs);
		}
		return list;
	}

	/**
	 * @param searchBean
	 * @param sb
	 */
	private static void andDateNotificheNonLetteAndDescrizioneAndTipiEvento(final SearchEventiDTO searchBean,
			final StringBuilder sb) {
		if (searchBean.getDataInizioNotificheNonLette() != null && searchBean.getDataFineNotificheNonLette() != null) {
			sb.append(" AND c.datascadenza >= ?  and c.datainizio <= ?");	
		}
		
		sb.append(andDescrizioneAndTipiEvento(searchBean));
	}

	/**
	 * @param searchBean
	 * @param ps
	 * @param index
	 * @return
	 * @throws SQLException
	 */
	private static int setDateNotificheNonLetteAndTipiEventoPs(final SearchEventiDTO searchBean, PreparedStatement ps,
			int index) throws SQLException {
		if (searchBean.getDataInizioNotificheNonLette() != null && searchBean.getDataFineNotificheNonLette() != null) {
			ps.setTimestamp(index++, new Timestamp((searchBean.getDataInizioNotificheNonLette()).getTime()));
			ps.setTimestamp(index++, new Timestamp((searchBean.getDataFineNotificheNonLette()).getTime()));
		}
		 
		if (searchBean.getTipiEvento() != null && !searchBean.getTipiEvento().isEmpty()) {
			for (final EventoCalendarioEnum tipo: searchBean.getTipiEvento()) {
				ps.setString(index++, tipo.getCodice());
			}
		}
		return index;
	}

	/**
	 * @param idUtente
	 * @param idUfficio
	 * @param idRuolo
	 * @param idAoo
	 * @param idEnte
	 * @param ps
	 * @param index
	 * @return
	 * @throws SQLException
	 */
	private static int setParametersPreparedStatement(final Long idUtente, final Long idUfficio, final Long idRuolo,
			final Long idAoo, final Integer idEnte, PreparedStatement ps, int index) throws SQLException {
		ps.setLong(index++, idUtente);
		ps.setLong(index++, idAoo);
		ps.setInt(index++, idEnte);
		ps.setLong(index++, idUtente);
		ps.setLong(index++, idUfficio);
		ps.setLong(index++, idRuolo);
		ps.setLong(index++, idUfficio);
		ps.setLong(index++, idRuolo);
		ps.setLong(index++, idUtente);
		return index;
	}

	/**
	 * @param searchBean
	 * @param sb
	 * @return 
	 */
	private static String andDescrizioneAndTipiEvento(final SearchEventiDTO searchBean) {
		StringBuilder sb = new StringBuilder();
		if (searchBean.getDescrizioneLike() != null && !"".equals(searchBean.getDescrizioneLike())) {
			sb.append(AND_UPPER_C_TITOLO_LIKE_UPPER);
		}
		
		if (searchBean.getTipiEvento() != null && !searchBean.getTipiEvento().isEmpty()) {
			sb.append(AND_PAR_DX_AP);
			for (int i = 0; i < searchBean.getTipiEvento().size(); i++) {
				sb.append(TC_CODICE_UGUALE_PLACEHOLDER);
				if (i < (searchBean.getTipiEvento().size() - 1)) {
					sb.append(" or ");
				}
			}
			sb.append(" ) ");
		}
		return sb.toString();
	}

	/**
	 * Crea l'Evento a partire dai risultati ottenuti dal ResultSet {@code rs}.
	 * 
	 * @param withContent
	 *            Se {@code true} recupera il blob dell'allegato e lo salva
	 *            nell'evento, se {@code false} imposta il content dell'allegato a
	 *            {@code null}.
	 * @param rs
	 *            ResultSet da cui recuperare l'evento.
	 * @param con
	 *            Connessione al database.
	 * @return Evento completo con le informazioni recuperate dal ResultSet
	 * @throws SQLException
	 */
	private static Evento handleResultSet(final boolean withContent, final ResultSet rs, final Connection con) throws SQLException {
		
		ResultSet rsruoli = null;
		ResultSet rsnodi = null;
		Evento evento = null;
		TipoCalendario tipoCalendario = null;
		List<Long> idRuoliTarget;
		List<Integer> idNodiTarget;
		
		byte[] allegato = null;
		
		if (withContent) {
			final Blob allegatoBlob = rs.getBlob(ALLEGATO);
			if (allegatoBlob != null) {
				allegato = allegatoBlob.getBytes(1, (int) allegatoBlob.length());
			}
		}
		
		evento = new Evento(
				rs.getLong("idutente"), 
				rs.getInt(IDEVENTO), 
				rs.getDate("datainizio"), 
				rs.getDate("datascadenza"), 
				rs.getString(CONTENUTO), 
				rs.getString("colore"), 
				rs.getInt("locked") == 1, 
				rs.getInt("flagpubblico") == 1,
				rs.getString(TITOLO),
				rs.getString("link_esterno"),
				allegato,
				rs.getString("nome_allegato"),
				rs.getString("estensione_allegato"),
				rs.getString("mimetype_allegato"),
				rs.getInt("stato")
		);
		
		tipoCalendario = new TipoCalendario(rs.getInt(ID_TIPO),
				rs.getString(CODICE),
				rs.getString(DESCRIZIONE)
		);
		
		evento.setTipoCalendario(tipoCalendario);
		
		try (PreparedStatement psruoli = con.prepareStatement(SQL_GET_RUOLI_TARGET)) {
			idRuoliTarget = new ArrayList<>();
			psruoli.setInt(1, rs.getInt(IDEVENTO));
			rsruoli = psruoli.executeQuery();
			while (rsruoli.next()) {
				idRuoliTarget.add(rsruoli.getLong(IDCATEGORIA));
			}
			evento.setIdRuoliCategoriaTarget(idRuoliTarget);
		} finally {
			closeResultset(rsruoli);
		}
		
		try (PreparedStatement psnodi = con.prepareStatement(SQL_GET_NODI_TARGET)) {
			idNodiTarget = new ArrayList<>();
			psnodi.setInt(1, rs.getInt(IDEVENTO));
			rsnodi = psnodi.executeQuery();
			while (rsnodi.next()) {
				idNodiTarget.add(rsnodi.getInt("idnodo"));
			}
			evento.setIdNodiTarget(idNodiTarget);
		} finally {
			closeResultset(rsnodi);
		}
		
		return evento;
	} 
	
	
	/**
	 * Find by.
	 *
	 * @param inIdUtente the in id utente
	 * @param inIdNodo the in id nodo
	 * @param inIdRuolo the in id ruolo
	 * @param connection the connection
	 * @return the collection
	 */
	@Override
	public final Collection<Calendario> findBy(final Long inIdUtente, final Long inIdNodo, final Long inIdRuolo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<Calendario> output = new ArrayList<>();
		try {
			ps = connection.prepareStatement("SELECT c.* FROM CALENDARIO c WHERE c.dataScadenza > ? AND CHECKVISIBILITAEVENTI(c.idNodo, c.idRuolo, ?, ?, ?)>0");

			int index = 1;
			final Calendar today = Calendar.getInstance();
			today.set(Calendar.DAY_OF_MONTH, 1);
			today.set(Calendar.HOUR_OF_DAY, 0);
			today.set(Calendar.MINUTE, 0);
			ps.setDate(index++, new java.sql.Date(today.getTime().getTime()));
			ps.setLong(index++, inIdNodo.longValue());
			ps.setLong(index++, inIdRuolo.longValue());
			ps.setLong(index++, inIdUtente.longValue());
			
			rs = ps.executeQuery();
			while (rs.next()) {
				final Calendario calendario = createCalendario(rs);
				output.add(calendario);
			}
			return output;
		} catch (final Exception e) {
			throw new RedException("Errore durante il recupero degli eventi del calendario tramite " + "idUtente=" + inIdUtente + " idNodo=" + inIdNodo, e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ICalendarioDAO#insertEvento(it.ibm.red.business.persistence.model.Evento,
	 *      java.sql.Connection).
	 */
	@Override
	public int insertEvento(final Evento evento, final Connection con) {
		
		final Calendar it = Calendar.getInstance();
		final java.util.Date now = it.getTime();
		CallableStatement cs = null;
		try {
			int index = 1;
			cs = con.prepareCall(SQL_INSERT);
			cs.setLong(index++, evento.getIdUtente());
			if (evento.getIdRuolo() != 0) {
				cs.setLong(index++, evento.getIdRuolo());
			} else {
				cs.setNull(index++, Types.INTEGER);
			}
			cs.setLong(index++, evento.getIdNodo());
			if (evento.getDataInizio() != null) {
				cs.setTimestamp(index++, new Timestamp(evento.getDataInizio().getTime()));
			} else {
				cs.setNull(index++, Types.DATE);
			}
			if (evento.getDataScadenza() != null) {
				cs.setTimestamp(index++, new Timestamp(evento.getDataScadenza().getTime()));
			} else {
				cs.setNull(index++, Types.DATE);
			}
			cs.setString(index++, evento.getTitolo());
			cs.setString(index++, evento.getContenuto());
			cs.setString(index++, evento.getTipoCalendario().getTipoEnum().getStyleClass());

			cs.setInt(index++, evento.isLocked() ? 1 : 0);
			cs.setInt(index++, evento.isPubblico() ? 1 : 0);
			cs.setTimestamp(index++, new Timestamp(now.getTime()));
			if (evento.getIdEnte() != 0) {
				cs.setInt(index++, evento.getIdEnte());
			} else {
				cs.setNull(index++, Types.INTEGER);
			}
			if (evento.getIdAoo() != 0) {
				cs.setLong(index++, evento.getIdAoo());
			} else {
				cs.setNull(index++, Types.INTEGER);
			}
			cs.setInt(index++, evento.getUrgente());
			cs.setInt(index++, evento.getTipoCalendario().getIdTipo());
			if (evento.getLinkEsterno() != null) {
				cs.setString(index++, evento.getLinkEsterno());
			} else {
				cs.setNull(index++, Types.VARCHAR);
			}
			// Gestione allegato (colonne "ALLEGATO" e "NOME_ALLEGATO")
			if (evento.getAllegato() != null) {
				
				cs.setBlob(index++, getAllegatoContent(evento, con));
				cs.setString(index++, evento.getNomeAllegato());
				cs.setString(index++, evento.getEstensioneAllegato());
			} else {
				cs.setNull(index++, Types.BLOB);
				cs.setNull(index++, Types.VARCHAR);
				cs.setNull(index++, Types.VARCHAR);
			}
			cs.registerOutParameter(index, Types.INTEGER);
			LOGGER.info("Execute Procedure p_inserteventocalendario");
			cs.execute();
			return cs.getInt(index);
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inserimento dell'evento nel calendario", e);
			throw new RedException("Errore durante l'inserimento dell'evento nel calendario");
		} finally {
			closeStatement(cs);
		}
	}
	
	/**
	 * @see it.ibm.red.business.dao.ICalendarioDAO#aggiornaEvento(it.ibm.red.business.persistence.model.Evento,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean aggiornaEvento(final Evento evento, final Connection con) {
		PreparedStatement ps = null;
		boolean actionOk = false;
		try {
			ps = con.prepareStatement(SQL_UPDATE);
			int index = 1;
			
			ps.setString(index++, evento.getTitolo());
			
			ps.setString(index++, evento.getContenuto());
			
			if (evento.getLinkEsterno() != null) {
				ps.setString(index++, evento.getLinkEsterno());
			} else {
				ps.setNull(index++, Types.VARCHAR);
			}
						
			if (evento.getDataInizio() != null) {
				ps.setTimestamp(index++, new Timestamp(evento.getDataInizio().getTime()));
			} else {
				ps.setNull(index++, Types.TIMESTAMP);
			}
			
			if (evento.getDataScadenza() != null) {
				ps.setTimestamp(index++, new Timestamp(evento.getDataScadenza().getTime()));
			} else {
				ps.setNull(index++, Types.TIMESTAMP);
			}
						
			ps.setTimestamp(index++, new Timestamp((new Date()).getTime()));
			
			// Gestione allegato (colonne "ALLEGATO" e "NOME_ALLEGATO")
			if (evento.getAllegato() != null) {
				
				ps.setBlob(index++, getAllegatoContent(evento, con));
				ps.setString(index++, evento.getNomeAllegato());
				ps.setString(index++, evento.getEstensioneAllegato());
			} else {
				ps.setNull(index++, Types.BLOB);
				ps.setNull(index++, Types.VARCHAR);
				ps.setNull(index++, Types.VARCHAR);
			}
			
			ps.setInt(index++, evento.getIdEvento());
			ps.executeUpdate();
			actionOk = true;
		} catch (final Exception e) {
			LOGGER.error(ERROR_AGGIORNAMENTO_EVENTO_MSG + evento.getIdEvento(), e);
			throw new RedException(ERROR_AGGIORNAMENTO_EVENTO_MSG + evento.getIdEvento(), e);
		} finally {
			closeStatement(ps);
		}
		return actionOk;
	}

	/**
	 * Recupera il blob dell'allegato.
	 * @param evento
	 * @param con
	 * @return Blob allegato
	 * @throws RedException
	 */
	private static Blob getAllegatoContent(final Evento evento, final Connection con) {
		Blob blobAllegato = null;
		try {
			blobAllegato = con.createBlob();
			final OutputStream writer = blobAllegato.setBinaryStream(1);
			writer.write(evento.getAllegato());
			writer.flush();
			writer.close();
		} catch (final Exception e) {
			e.getStackTrace();
			LOGGER.error(ERROR_AGGIORNAMENTO_EVENTO_MSG + evento.getIdEvento(), e);
			throw new RedException(ERROR_AGGIORNAMENTO_EVENTO_MSG + evento.getIdEvento(), e);
		}
		return blobAllegato;
	}

	/**
	 * @see it.ibm.red.business.dao.ICalendarioDAO#cancellaEvento(int,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean cancellaEvento(final int idEvento, final Connection con) {
		PreparedStatement ps = null;
		boolean actionOk = false;
		try {
			ps = con.prepareStatement(SQL_DELETE);
			ps.setInt(1, idEvento);
			
			ps.executeUpdate();
			actionOk = true;
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'eliminazione dell'evento del calendario tramite ID = " + idEvento, e);
			throw new RedException("Errore durante l'eliminazione dell'evento del calendario tramite ID = " + idEvento, e);
		} finally {

			closeStatement(ps);

		}
		return actionOk;
	}
	
	
	/**
	 * Find by id.
	 *
	 * @param id the id
	 * @param connection the connection
	 * @return the calendario
	 */
	@Override
	public final Calendario findById(final Integer id, final Connection connection) {
		Calendario output = null;
		if (id != null) {
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				ps = connection.prepareStatement("SELECT c.* FROM Calendario c WHERE c.IDEVENTO = " + sanitize(id));
				rs = ps.executeQuery();
				if (rs.next()) {
					output = createCalendario(rs);
				}
			} catch (final SQLException e) {
				throw new RedException("Errore durante il recupero dell'evento del calendario tramite ID=" + id, e);
			} finally {
				closeStatement(ps, rs);
			}
		}
		return output;
	}

	/**
	 * Creazione di un Calendario a partire dal ResultSet.
	 * 
	 * @param rs			Resultset di partenza
	 * @return				Calendario
	 * @throws SQLException	Eccezione generata in fase di creazione del Calendario
	 */
	private static Calendario createCalendario(final ResultSet rs) throws SQLException {
		final String contenuto = rs.getString(CONTENUTO);
		final Timestamp dataInizioTS = rs.getTimestamp("dataInizio");
		final Date dataInizio = getDateFromTS(dataInizioTS);
		final Timestamp dataScadenzaTS = rs.getTimestamp("dataScadenza");
		final Date dataScadenza = getDateFromTS(dataScadenzaTS);
		final Timestamp dataTimeTS = rs.getTimestamp("dataTime");
		final Date dataTime = getDateFromTS(dataTimeTS);
		final Integer idEvento = rs.getInt("idEvento");
		final String titolo = rs.getString(TITOLO);
		final Integer urgente = rs.getInt("urgente");
		return new Calendario(idEvento, dataInizio, dataScadenza, titolo, contenuto, dataTime, urgente);
	}

	/**
	 * Metodo per il recupero della data a partire da un timestamp.
	 * 	
	 * @param dataInizioTS	timestamp
	 * @return				data
	 */
	private static Date getDateFromTS(final Timestamp dataInizioTS) {
		Date dataInizio = null;
		if (dataInizioTS != null) {
			dataInizio = new Date(dataInizioTS.getTime());
		}
		return dataInizio;
	}

	/**
	 * @see it.ibm.red.business.dao.ICalendarioDAO#getAllTestiDefault(java.sql.Connection).
	 */
	@Override
	public List<TestiDefault> getAllTestiDefault(final Connection con) {
		final List<TestiDefault> list = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.prepareStatement(SQL_GET_TESTI_DEFAULT);
			rs = ps.executeQuery();
			TestiDefault td = null;
			while (rs.next()) {
				td = new TestiDefault(rs.getInt("idtesto"),
						rs.getString("nometesto"),
						rs.getString(TITOLO),
						rs.getString(CONTENUTO)
				);
				list.add(td);
			}

			return list;

		} catch (final SQLException e) {
			throw new RedException("Errore nel recupero dei testi di default", e);
		} finally {
			closeResultset(rs);
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ICalendarioDAO#getCategorieRuoli(java.sql.Connection,
	 *      java.lang.Long).
	 */
	@Override
	public List<RuoloDTO> getCategorieRuoli(final Connection connection, final Long idAoo) {
		final List<RuoloDTO> ruoli = new ArrayList<>();
		ResultSet rs = null;
		PreparedStatement ps = null;
		int index = 1;
		try {
			ps = connection.prepareStatement(SQL_GET_CATEGORIERUOLI);
			ps.setLong(index++, idAoo);
			rs = ps.executeQuery();
			while (rs.next()) {
				ruoli.add(new RuoloDTO(rs.getLong(IDCATEGORIA), rs.getString("nomecategoria")));
			}
		} catch (final SQLException e) {
			throw new RedException("Errore nel recupero dei ruoli", e);
			
		} finally {
			closeStatement(ps, rs);
		}
		
		
		return ruoli;
		
	}

	/**
	 * @see it.ibm.red.business.dao.ICalendarioDAO#getAllTipologie(java.sql.Connection).
	 */
	@Override
	public List<TipoCalendario> getAllTipologie(final Connection con) {
		
		final List<TipoCalendario> list = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.prepareStatement(SQL_GETTIPOLOGIE);
			rs = ps.executeQuery();
			TipoCalendario tc = null;
			while (rs.next()) {
				tc = new TipoCalendario(rs.getInt(ID_TIPO),
						rs.getString(CODICE),
						rs.getString(DESCRIZIONE)
				);
				list.add(tc);
			}

			return list;

		} catch (final SQLException e) {
			throw new RedException("Errore nel recupero delle tipologie di eventi di calendario", e);
		} finally {
			closeResultset(rs);
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ICalendarioDAO#insertRuoliTarget(int,
	 *      java.util.List, java.sql.Connection).
	 */
	@Override
	public void insertRuoliTarget(final int idEvento, final List<Long> idRuoliCategoriaTarget, final Connection con) {
		
		PreparedStatement ps = null;

		try {
			
			if (idRuoliCategoriaTarget != null && !idRuoliCategoriaTarget.isEmpty()) {
			
				ps = con.prepareStatement(SQL_INSERT_RUOLI_TARGET);
				ps.setInt(1, idEvento);
				
				for (final Long idCategoria: idRuoliCategoriaTarget) {
					ps.setLong(2, idCategoria);
					ps.executeUpdate();
				}
				
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inserimento dei Ruoli nell'evento con ID: " + idEvento, e);
			throw new RedException("Errore durante l'inserimento dei ruoli target per l'evento " + idEvento, e);
		} finally {

			closeStatement(ps);

		}
		
	}

	/**
	 * @see it.ibm.red.business.dao.ICalendarioDAO#insertNodiTarget(int,
	 *      java.util.List, java.sql.Connection).
	 */
	@Override
	public void insertNodiTarget(final int idEvento, final List<Integer> idNodiTarget, final Connection con) {
		
		PreparedStatement ps = null;

		try {
			
			if (idNodiTarget != null && !idNodiTarget.isEmpty()) {
			
				ps = con.prepareStatement(SQL_INSERT_NODI_TARGET);
				ps.setInt(1, idEvento);
				
				for (final Integer idNodo: idNodiTarget) {
					ps.setInt(2, idNodo);
					ps.executeUpdate();
				}
				
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inserimento dei nodi target per l'evento " + idEvento, e);
			throw new RedException("Errore durante l'inserimento dei nodi target per l'evento " + idEvento, e);
		} finally {
			closeStatement(ps);
		}
		
	}
	
	/**
	 * @see it.ibm.red.business.dao.ICalendarioDAO#deleteRuoliTarget(int,
	 *      java.sql.Connection).
	 */
	@Override
	public void deleteRuoliTarget(final int idEvento, final Connection con) {
		PreparedStatement ps = null;

		try {
			ps = con.prepareStatement(SQL_DELETE_RUOLI_TARGET);
			ps.setInt(1, idEvento);

			ps.executeUpdate();
		} catch (final SQLException e) {
			throw new RedException("Errore durante la cancellazione dei ruoli target per l'evento " + idEvento, e);
		} finally {
			closeStatement(ps);
		}
		
	}

	/**
	 * @see it.ibm.red.business.dao.ICalendarioDAO#deleteNodiTarget(int,
	 *      java.sql.Connection).
	 */
	@Override
	public void deleteNodiTarget(final int idEvento, final Connection con) {
		PreparedStatement ps = null;

		try {
			ps = con.prepareStatement(SQL_DELETE_NODI_TARGET);
			ps.setInt(1, idEvento);

			ps.executeUpdate();
		} catch (final SQLException e) {
			throw new RedException("Errore durante la cancellazione dei nodi target per l'evento " + idEvento, e);
		} finally {
			closeStatement(ps);
		}
		
	}

	/**
	 * @see it.ibm.red.business.dao.ICalendarioDAO#getCountEventiHomepage(it.ibm.red.business.dto.SearchEventiDTO,
	 *      java.lang.Long, java.lang.Long, java.lang.Long, java.lang.Long,
	 *      java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public Integer getCountEventiHomepage(final SearchEventiDTO searchBean, final Long idUtente, final Long idUfficio, 
			final Long idRuolo, final Long idAoo, final Integer idEnte, final Connection con) {
		Integer contatoreOutput = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT count(*) as counter "); 
			sb.append(queryEventiCommon());
			sb.append(" AND (SNC.stato IS NULL OR SNC.STATO = 0)");
			
			andDateNotificheNonLetteAndDescrizioneAndTipiEvento(searchBean, sb);

			ps = con.prepareStatement(sb.toString());

			int index = 1;
			index = setParametersPreparedStatement(idUtente, idUfficio, idRuolo, idAoo, idEnte, ps, index);

			index = setDateNotificheNonLetteAndTipiEventoPs(searchBean, ps, index);

			rs = ps.executeQuery(); 
			if (rs.next()) {
				contatoreOutput = rs.getInt("counter");
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore nel recupero della count degli eventi Calendario.", e);
		} finally {
			closeStatement(ps, rs);
		}
		return contatoreOutput;
	}
}
