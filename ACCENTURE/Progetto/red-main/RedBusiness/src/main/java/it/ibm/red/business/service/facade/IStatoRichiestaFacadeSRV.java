package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.dto.StatoRichiestaDTO;

/**
 * The Interface IStatoRichiestaFacadeSRV.
 *
 * @author CPIERASC
 * 
 *         Facade gestione stato richiesta.
 */
public interface IStatoRichiestaFacadeSRV extends Serializable {
	
	/**
	 * Metodo per il recupero di tutti gli stato richiesta.
	 * 
	 * @return	lista stato richiesta
	 */
	Collection<StatoRichiestaDTO> getAll();

}
