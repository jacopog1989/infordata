package it.ibm.red.business.dto;

/**
 * DTO di un documento.
 */
public class DocumentoDTO extends AbstractDTO {

	/**
	 * La costante serial version UID.
	 */
	private static final long serialVersionUID = -7846646523523596176L;

	/**
	 * Wob number documento.
	 */
	private String wobNumber;
	
	/**
	 * GUID documento.
	 */
	private String guid;
	
	/**
	 * Document title documento.
	 */
	private String documentTitle;
	
	/**
	 * Content documento.
	 */
	private byte[] content;
	
	/**
	 * Flag allegato.
	 */
	private boolean flagAllegato;
	

	/**
	 * @param wobNumber
	 * @param guid
	 * @param documentTitle
	 * @param content
	 * @param flagAllegato
	 */
	public DocumentoDTO(String wobNumber, String guid, String documentTitle, byte[] content, boolean flagAllegato) {
		super();
		this.wobNumber = wobNumber;
		this.guid = guid;
		this.documentTitle = documentTitle;
		this.content = content;
		this.flagAllegato = flagAllegato;
	}

	/**
	 * @return the wobNumber
	 */
	public String getWobNumber() {
		return wobNumber;
	}

	/**
	 * @return the guid
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * @return the documentTitle
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * @return the content
	 */
	public byte[] getContent() {
		return content;
	}

	/**
	 * @return the flagAllegato
	 */
	public boolean isFlagAllegato() {
		return flagAllegato;
	}
	
}

