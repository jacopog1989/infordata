package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Facade del servizio che gestisce gli ATTI.
 */
public interface IAttiFacadeSRV extends Serializable {
	
	/**
	 * Metodo per la messa agli atti non asincrona.
	 * 
	 * @param wobNumber
	 *            identificativo del documento da mettere agli atti
	 * @param utente
	 *            utente in sessione
	 * @param motivazione
	 *            motivo della messa agli atti
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO mettiAgliAtti(String wobNumber, UtenteDTO utente, String motivazione);
	
	/**
	 * Metodo per la messa agli atti.
	 * 
	 * @param wobNumber
	 *            identificativo del documento da mettere agli atti
	 * @param utente
	 *            utente in sessione
	 * @param motivazione
	 *            motivo della messa agli atti del documento
	 * @param async
	 *            se <code> true </code> viene messo agli atti in maniera asincrona,
	 *            se <code> false </code> viene messo agli atti in maniera sincrona
	 * @param statoNPS
	 *            stato NPS, @see it.ibm.red.business.nps.builder.Builder
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO mettiAgliAtti(String wobNumber, UtenteDTO utente, String motivazione, boolean async, String statoNPS);

	/**
	 * Metodo per la messa agli atti massiva.
	 * 
	 * @param wobNumbers
	 *            identificativi dei documenti da mettere agli atti
	 * @param utente
	 *            utente in sessione
	 * @param motivazione
	 *            motivo della messa agli atti
	 * @return esiti delle operazioni
	 */
	Collection<EsitoOperazioneDTO> mettiAgliAtti(Collection<String> wobNumbers, UtenteDTO utente, String motivazione);

}