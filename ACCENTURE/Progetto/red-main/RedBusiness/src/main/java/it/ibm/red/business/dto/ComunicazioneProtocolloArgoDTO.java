/**
 * 
 */
package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * Rappresenta una comunicazione al sistema ARGO del protocollo staccato da RED.
 * 
 * Contiene tutte le informazioni relative al protocollo di cui necessita il sistema ARGO 
 * per gestire la protocollazione di un documento creato e protocollato da RED su richiesta di ARGO.
 * 
 * @author m.crescentini
 *
 */
public class ComunicazioneProtocolloArgoDTO extends AbstractDTO implements Serializable {

	private static final long serialVersionUID = 8976510236025429522L;

	/**
	 * Identificativo coda.
	 */
	private final Long idCoda;
	
	/**
	 * Identificativo documento.
	 */
	private final int idDocumento;
	
	/**
	 * Identificativo aoo.
	 */
	private final Long idAoo;
	
	/**
	 * Numero protocollo.
	 */
	private final int numeroProtocollo;
	
	/**
	 * Anno protocollo.
	 */
	private final int annoProtocollo;
	
	/**
	 * Data registrazione protocollo.
	 */
	private final Date dataRegistrazioneProtocollo;
	
	/**
	 * Identificativo protocollo.
	 */
	private final String idProtocollo;
	
	/**
	 * Registro protocollo.
	 */
	private final String registroProtocollo;
	
	
	
	/**
	 * @param idDocumento
	 * @param idAoo
	 * @param numeroProtocollo
	 * @param annoProtocollo
	 * @param dataRegistrazioneProtocollo
	 * @param idProtocollo
	 * @param registroProtocollo
	 */
	public ComunicazioneProtocolloArgoDTO(final int idDocumento, final Long idAoo, final int numeroProtocollo, final int annoProtocollo,
			final Date dataRegistrazioneProtocollo, final String idProtocollo, final String registroProtocollo) {
		this(null, idDocumento, idAoo, numeroProtocollo, annoProtocollo, dataRegistrazioneProtocollo, idProtocollo, registroProtocollo);
	}


	/**
	 * @param idCoda
	 * @param idDocumento
	 * @param idAoo
	 * @param numeroProtocollo
	 * @param annoProtocollo
	 * @param dataRegistrazioneProtocollo
	 * @param idProtocollo
	 * @param registroProtocollo
	 */
	public ComunicazioneProtocolloArgoDTO(final Long idCoda, final int idDocumento, final Long idAoo, final int numeroProtocollo,
			final int annoProtocollo, final Date dataRegistrazioneProtocollo, final String idProtocollo, final String registroProtocollo) {
		super();
		this.idCoda = idCoda;
		this.idDocumento = idDocumento;
		this.idAoo = idAoo;
		this.numeroProtocollo = numeroProtocollo;
		this.annoProtocollo = annoProtocollo;
		this.dataRegistrazioneProtocollo = dataRegistrazioneProtocollo;
		this.idProtocollo = idProtocollo;
		this.registroProtocollo = registroProtocollo;
	}


	/**
	 * @return the idCoda
	 */
	public Long getIdCoda() {
		return idCoda;
	}


	/**
	 * @return the documentTitle
	 */
	public int getIdDocumento() {
		return idDocumento;
	}

	
	/**
	 * @return the idAoo
	 */
	public Long getIdAoo() {
		return idAoo;
	}

	
	/**
	 * @return the numeroProtocollo
	 */
	public int getNumeroProtocollo() {
		return numeroProtocollo;
	}


	/**
	 * @return the annoProtocollo
	 */
	public int getAnnoProtocollo() {
		return annoProtocollo;
	}


	/**
	 * @return the idProtocollo
	 */
	public String getIdProtocollo() {
		return idProtocollo;
	}


	/**
	 * @return the dataRegistrazioneProtocollo
	 */
	public Date getDataRegistrazioneProtocollo() {
		return dataRegistrazioneProtocollo;
	}


	/**
	 * @return the registroProtocollo
	 */
	public String getRegistroProtocollo() {
		return registroProtocollo;
	}

}
