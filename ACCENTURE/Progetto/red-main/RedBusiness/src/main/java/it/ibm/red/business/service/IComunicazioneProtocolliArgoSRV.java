package it.ibm.red.business.service;

import java.sql.Connection;

import it.ibm.red.business.dto.ComunicazioneProtocolloArgoDTO;
import it.ibm.red.business.service.facade.IComunicazioneProtocolliArgoFacadeSRV;

/**
 * Interface del servizio di comunicazione protocolli ARGO.
 */
public interface IComunicazioneProtocolliArgoSRV extends IComunicazioneProtocolliArgoFacadeSRV {

	/**
	 * @param comunicazioneProtocolloArgo
	 * @param con
	 * @return
	 */
	Long accodaComunicazioneProtocollo(ComunicazioneProtocolloArgoDTO comunicazioneProtocollo, Connection con);

}
