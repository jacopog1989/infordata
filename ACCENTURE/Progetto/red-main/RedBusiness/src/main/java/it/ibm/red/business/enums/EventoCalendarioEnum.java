package it.ibm.red.business.enums;

/**
 * Enum che definisce gli eventi calendario.
 */
public enum EventoCalendarioEnum {

	/**
	 * Valore.
	 */
	COMUNICAZIONE("C", "Comunicazione", "colore_comunicazione", "COMUNICAZIONE", 2),

	/**
	 * Valore.
	 */
	SCADENZA("D", "Doc Scadenza", "colore_scadenza", "DOCUMENTO_IN_SCADENZA", 4),

	/**
	 * Valore.
	 */
	UTENTE("U", "Evento Utente", "colore_utente", "EVENTO_UTENTE", 1), 

	/**
	 * Valore.
	 */
	NEWS("N", "News", "colore_news", "NEWS", 3);

	/**
	 * Valore.
	 */
	private String value;

	/**
	 * Descrizione.
	 */
	private String description;

	/**
	 * Codice.
	 */
	private String codice;

	/**
	 * Classe CSS.
	 */
	private String styleClass;

	/**
	 * Tipologia.
	 */
	private Integer idTipoCal;
	
	EventoCalendarioEnum(final String value, final String description, final String styleClass, final String codice, final Integer idTipoCal) {
		this.value = value;
		this.description = description;
		this.codice = codice;
		this.styleClass = styleClass;
		this.idTipoCal = idTipoCal;
	}

	/**
	 * Ottiene l'item dal codice.
	 * @param codice
	 * @return l'evento del calendario se corrispondono i codici, null in caso contrario
	 */
	public static EventoCalendarioEnum getItem(final String codice) {
		for (EventoCalendarioEnum t: EventoCalendarioEnum.values()) {
			if (t.getCodice().equals(codice)) {
				return t;
			}
		}
		return null;
	}
	
	/**
	 * Ottiene l'item dalla descrizione.
	 * @param descrizione
	 * @return l'evento del calendario se corrispondono le descrizioni, null in caso contrario
	 */
	public static EventoCalendarioEnum getItemFromDescription(final String descrizione) {
		for (EventoCalendarioEnum t: EventoCalendarioEnum.values()) {
			if (t.getDescription().equals(descrizione)) {
				return t;
			}
		}
		return null;
	}
	
	/**
	 * Ottiene l'item da styleClass.
	 * @param styleClass
	 * @return l'evento del calendario se corrispondono gli styleClass, null in caso contrario
	 */
	public static EventoCalendarioEnum getItemFromStyle(final String styleClass) {
		for (EventoCalendarioEnum t: EventoCalendarioEnum.values()) {
			if (t.getStyleClass().equals(styleClass)) {
				return t;
			}
		}
		return null;
	}
	
	/**
	 * Recupera il valore.
	 * @return valore
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Imposta il valore.
	 * 
	 * @param value valore
	 */
	protected void setValue(final String value) {
		this.value = value;
	}

	/**
	 * Recupera la descrizione.
	 * @return descrizione
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Recupera il codice.
	 * @return codice
	 */
	public String getCodice() {
		return codice;
	}
	
	/**
	 * Recupera styleClass.
	 * @return styleClass
	 */
	public String getStyleClass() {
		return styleClass;
	}

	/**
	 * Imposta styleClass.
	 * @param styleClass
	 */
	protected void setStyleClass(final String styleClass) {
		this.styleClass = styleClass;
	}

	/**
	 * Recupera l'id del tipo calendario.
	 * @return id del tipo calendario
	 */
	public Integer getIdTipoCal() {
		return idTipoCal;
	}

	/**
	 * Imposta l'id del tipo calendario.
	 * @param inIdTipoCal id del tipo calendario
	 */
	protected void setIdTipoCal(final Integer inIdTipoCal) {
		idTipoCal = inIdTipoCal;
	}
	
}
