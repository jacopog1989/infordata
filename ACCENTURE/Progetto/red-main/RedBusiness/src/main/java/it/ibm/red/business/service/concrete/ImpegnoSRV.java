package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import it.ibm.red.business.logger.REDLogger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.ICalendarioDAO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.ImpegnoDTO;
import it.ibm.red.business.dto.MasterDocumentoDTO;
import it.ibm.red.business.enums.TipoAzioneEnum;
import it.ibm.red.business.persistence.model.Calendario;
import it.ibm.red.business.service.IDocumentoSRV;
import it.ibm.red.business.service.facade.IImpegnoFacadeSRV;
import it.ibm.red.business.utils.DateUtils;

/**
 * The Class ImpegnoSRV.
 *
 * @author CPIERASC
 * 
 *         Servizio gestione impegni.
 */
@Service
@Component
public class ImpegnoSRV extends AbstractService implements IImpegnoFacadeSRV {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ImpegnoSRV.class.getName());
	
	/**
	 * Servizio gestione documenti.
	 */
	@Autowired
	private IDocumentoSRV documentoSRV;
	
	
	/**
	 * Dao gestione calendario.
	 */
	@Autowired
	private ICalendarioDAO calendarioDAO;
	
	/**
	 * Metodo per recuperare gli impegni in scadenza.
	 *
	 * @param fcDTO
	 *            the fc DTO
	 * @param idUtente
	 *            the id utente
	 * @param idUfficio
	 *            the id ufficio
	 * @param idRuolo
	 *            the id ruolo
	 * @param idAoo
	 *            the id aoo
	 * @param filter
	 *            the filter
	 * @return the impegni in scadenza
	 */
	@Override
	public final Collection<ImpegnoDTO> getImpegniInScadenza(final FilenetCredentialsDTO fcDTO, final Long idUtente, final Long idUfficio, 
			final Long idRuolo, final Long idAoo, final TipoAzioneEnum filter) {
		Collection<ImpegnoDTO> output = new ArrayList<>();
		Connection connection =  null;
		try {
			//Connessione
			connection = setupConnection(getDataSource().getConnection(), false);
			if (!TipoAzioneEnum.EVENTO.equals(filter)) {
				Collection<MasterDocumentoDTO> documenti = documentoSRV.getDocumenti(fcDTO, idUtente, idUfficio, idAoo, idRuolo, null, false);
				for (MasterDocumentoDTO documento : documenti) {
					if (documento.getDataScadenza() != null) {
						output.add(new ImpegnoDTO(documento));
					}
				}
			}
			//se il filtro è diverso dalla scadenza
			if (!TipoAzioneEnum.SCADENZA_DOCUMENTO.equals(filter)) {
				Collection<Calendario> eventi = calendarioDAO.findBy(idUtente, idUfficio, idRuolo, connection);
				for (Calendario evento : eventi) {
					output.add(new ImpegnoDTO(evento));
				}
			}
		} catch (Exception e) {
			LOGGER.error("Errore nel recupero degli impegni: " + e.getMessage(), e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}		
		return output;
	}

	/**
	 * recupero degli impegni in scadenza passando una data specifica.
	 *
	 * @param fcDTO
	 *            the fc DTO
	 * @param idUtente
	 *            the id utente
	 * @param idUfficio
	 *            the id ufficio
	 * @param idRuolo
	 *            the id ruolo
	 * @param idAoo
	 *            the id aoo
	 * @param date
	 *            the date
	 * @param filter
	 *            the filter
	 * @return the impegni in scadenza
	 */
	@Override
	public final Collection<ImpegnoDTO> getImpegniInScadenza(final FilenetCredentialsDTO fcDTO, final Long idUtente, final Long idUfficio, 
			final Long idRuolo, final Long idAoo, final Date date, final TipoAzioneEnum filter) {
		Date cleanDate = DateUtils.dropTimeInfo(date);
		Collection<ImpegnoDTO> output = new ArrayList<>();
		Collection<ImpegnoDTO> impegni = getImpegniInScadenza(fcDTO, idUtente, idUfficio, idRuolo, idAoo, filter);
		for (ImpegnoDTO impegno:impegni) {
			//recupero data scadenza dell'impegno
			Date cleanDateImpegno = DateUtils.dropTimeInfo(impegno.getDataInizio());
			//comparazione con il parametro data
			if (cleanDate.equals(cleanDateImpegno)) {
				output.add(impegno);
			}
		}
		return output;
	}
	
	/**
	 * Metodo per il recupero dei documenti in scadenza.
	 *
	 * @param fcDTO
	 *            the fc DTO
	 * @param idUtente
	 *            the id utente
	 * @param idUfficio
	 *            the id ufficio
	 * @param idRuolo
	 *            the id ruolo
	 * @param idAoo
	 *            the id aoo
	 * @param date
	 *            the date
	 * @return the documenti in scadenza
	 */
	@Override
	public final Collection<MasterDocumentoDTO> getDocumentiInScadenza(final FilenetCredentialsDTO fcDTO, final Long idUtente, final Long idUfficio, 
			final Long idRuolo, final Long idAoo, final Date date) {
		Date cleanDate = DateUtils.dropTimeInfo(date);
		Collection<MasterDocumentoDTO> output = new ArrayList<>();
		Collection<MasterDocumentoDTO> documenti = documentoSRV.getDocumenti(fcDTO, idUtente, idUfficio, idAoo, idRuolo, null, false);
		for (MasterDocumentoDTO documento:documenti) {
			if (documento.getDataScadenza() != null) {
				Date cleanDataScadenzaDoc = DateUtils.dropTimeInfo(documento.getDataScadenza());
				if (cleanDate.equals(cleanDataScadenzaDoc)) {
					output.add(documento);
				}
			}
		}
		return output;
	}
	
	/**
	 * Metodo per il recupero degli eventi di un calendario.
	 *
	 * @param fcDTO
	 *            the fc DTO
	 * @param idUtente
	 *            the id utente
	 * @param idUfficio
	 *            the id ufficio
	 * @param idRuolo
	 *            the id ruolo
	 * @param id
	 *            the id
	 * @return the calendario by id
	 */
	@Override
	public final ImpegnoDTO getCalendarioById(final FilenetCredentialsDTO fcDTO, final Long idUtente, final Long idUfficio, final Long idRuolo, final Integer id) {
		ImpegnoDTO output = null;
		Connection connection =  null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			Calendario calendario = calendarioDAO.findById(id, connection);
			output = new ImpegnoDTO(calendario);
		} catch (Exception e) {
			LOGGER.error("Errore nel recupero impegno con id " + id + ": " + e.getMessage(), e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}		
		return output;
	}

}
