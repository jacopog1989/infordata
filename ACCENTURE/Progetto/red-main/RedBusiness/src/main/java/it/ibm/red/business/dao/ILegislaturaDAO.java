package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.persistence.model.Legislatura;

/**
 * 
 * @author m.crescentini
 *
 *	Dao Legislatura.
 */
public interface ILegislaturaDAO extends Serializable {
	
	/**
	 * Ottiene la legislatura dal numero legislatura.
	 * @param idLegislatura
	 * @param connection
	 * @return legislatura
	 */
	Legislatura getLegislaturaByNumero(int idLegislatura, Connection connection);
	
	/**
	 * Ottiene la legislatura corrente.
	 * @param connection
	 * @return legislatura
	 */
	Legislatura getLegislaturaCorrente(Connection connection);
	
	/**
	 * Ottiene le legislature.
	 * @param connection
	 * @return lista delle legislature
	 */
	List<Legislatura> getLegislature(Connection connection);
}
