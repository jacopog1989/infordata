/**
 * @author CPIERASC
 *
 *	Interfaccia a grana fine dei servizi: qui vengono riportate le interfacce che listano i servizi che devono essere
 *	implementati da un servizio concreto, ma che non devono essere esposti al client.
 *
 */
package it.ibm.red.business.service;
