package it.ibm.red.business.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.usermodel.Workbook;

import it.ibm.red.business.logger.REDLogger;

/**
 * XlsUtils.
 *
 * @author Vingenito
 * 
 *         Utility per la gestione dei file xls
 */
public final class XlsUtils {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(XlsUtils.class.getName());

	/**
	 * Costruttore.
	 */
	private XlsUtils() {
	}

	/**
	 * Metodo per settare lo stile delle celle.
	 * 
	 * @param Workbook      workbook
	 * @param Boolean       bold
	 * @param Integer       height
	 * @param IndexedColors color
	 * @return CellStyle
	 */
	public static CellStyle createCellStyle(final Workbook workbook, final Boolean bold, final Integer height, final IndexedColors colorText,
			final IndexedColors backGroundColor, final boolean border, final boolean alignCenter) {
		final Font font = workbook.createFont();
		font.setBold(bold);
		font.setFontHeightInPoints(height.shortValue());
		font.setColor(colorText.getIndex());
		final CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(font);
		if (border) {
			headerCellStyle.setBorderBottom(BorderStyle.THIN);
			headerCellStyle.setBottomBorderColor(IndexedColors.BLACK.getIndex());
			headerCellStyle.setBorderLeft(BorderStyle.THIN);
			headerCellStyle.setLeftBorderColor(IndexedColors.BLACK.getIndex());
			headerCellStyle.setBorderRight(BorderStyle.THIN);
			headerCellStyle.setRightBorderColor(IndexedColors.BLACK.getIndex());
			headerCellStyle.setBorderTop(BorderStyle.THIN);
			headerCellStyle.setTopBorderColor(IndexedColors.BLACK.getIndex());
		}
		if (backGroundColor != null) {
			headerCellStyle.setFillForegroundColor(backGroundColor.getIndex());
			headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		}
		if (alignCenter) {
			headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
			headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
		}
		return headerCellStyle;
	}

	/**
	 * Metodo per scrviere le righe dell'excel.
	 * 
	 * @param Sheet     sheet
	 * @param Integer   nRow
	 * @param Integer   nStartCol
	 * @param CellStyle style
	 * @param Object    ... values
	 * @return
	 */
	public static void writeRow(final Sheet sheet, final Integer nRow, final Integer nStartCol, final CellStyle style, final Object... values) {
		final Row headerRow = sheet.createRow(nRow);
		Integer i = nStartCol;
		for (final Object value : values) {
			final Cell cell = headerRow.createCell(i);
			if (value instanceof Date) {
				cell.setCellValue((Date) value);
			} else if (value instanceof Number) {
				cell.setCellValue(((Number) value).doubleValue());
			} else {
				cell.setCellValue(value.toString());
			}
			cell.setCellStyle(style);
			i++;
		}
	}

	/**
	 * Metodo per l'autosize delle colonne.
	 * 
	 * @param Workbook workbook
	 * @return
	 */
	public static void autoSizeColumns(final Workbook workbook) {
		final int numberOfSheets = workbook.getNumberOfSheets();
		for (int i = 0; i < numberOfSheets; i++) {
			final Sheet sheet = workbook.getSheetAt(i);
			if (sheet.getPhysicalNumberOfRows() > 0) {
				final Row row = sheet.getRow(sheet.getFirstRowNum());
				final Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					final Cell cell = cellIterator.next();
					final int columnIndex = cell.getColumnIndex();
					sheet.autoSizeColumn(columnIndex);
				}
			}
		}
	}

	/**
	 * Restituisce il byte array associato al workbook.
	 * 
	 * @param workbook
	 * @return byte array workbook
	 */
	public static byte[] getByte(final Workbook workbook) {
		byte[] output = null;
		try {
			final ByteArrayOutputStream baos = new ByteArrayOutputStream();
			workbook.write(baos);
			output = baos.toByteArray();
			baos.close();
			workbook.close();
		} catch (final IOException e) {
			LOGGER.error(e);
		}
		return output;
	}

	/**
	 * @param sheet
	 * @param style
	 * @param row
	 * @param nomeColonna
	 */
	public static void writeHeader(final Sheet sheet, final CellStyle style, final int row, final String... nomeColonna) {
		final Row headerRow = sheet.createRow(row);
		Cell headerCell = null;
		for (int i = 0; i < nomeColonna.length; i++) {
			headerCell = headerRow.createCell(i);
			headerCell.setCellValue(nomeColonna[i]);
			headerCell.setCellStyle(style);
		}
	}

	private static String capitalize(final String s) {
		if (s.length() == 0) {
			return s;
		}
		return s.substring(0, 1).toUpperCase() + s.substring(1);
	}

	/**
	 * @param sheet
	 * @param data
	 * @param fieldNames
	 * @param nomeHeader
	 * @param rowCount
	 * @param styleHeader
	 */
	public static <T> void writeExcel(final Sheet sheet, final List<T> data, final List<String> fieldNames, final List<String> nomeHeader, int rowCount,
			final CellStyle styleHeader) {
		try {
			final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			int columnCount = 0;
			Row row = sheet.createRow(rowCount++);
			for (final String fieldName : nomeHeader) {
				final Cell cell = row.createCell(columnCount++);
				cell.setCellValue(fieldName);
				cell.setCellStyle(styleHeader);
			}
			final Class<? extends Object> classz = data.get(0).getClass();
			for (final T t : data) {
				row = sheet.createRow(rowCount++);
				columnCount = 0;
				writeBodyXls(fieldNames, sdf, columnCount, row, classz, t);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
		}
	}

	private static <T> void writeBodyXls(final List<String> fieldNames, final SimpleDateFormat sdf, int columnCount, final Row row, final Class<? extends Object> classz,
			final T t) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
		for (final String fieldName : fieldNames) {
			final Cell cell = row.createCell(columnCount);
			Method method = null;
			try {
				method = classz.getMethod("get" + capitalize(fieldName));
			} catch (final NoSuchMethodException nme) {
				LOGGER.warn(nme);
				method = classz.getMethod("get" + fieldName);
			}
			final Object value = method.invoke(t, (Object[]) null);
			if (value != null) {
				if (value instanceof String) {
					cell.setCellValue((String) value);
				} else if (value instanceof Long) {
					cell.setCellValue((Long) value);
				} else if (value instanceof Integer) {
					cell.setCellValue((Integer) value);
				} else if (value instanceof Double) {
					cell.setCellValue((Double) value);
				} else if (value instanceof Date) {
					cell.setCellValue(sdf.format((Date) value));
				}
			}
			columnCount++;
		}
	}

}
