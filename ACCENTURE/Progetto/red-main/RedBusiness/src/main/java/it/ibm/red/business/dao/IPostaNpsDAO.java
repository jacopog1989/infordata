package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;

import it.ibm.red.business.dto.AssegnatarioInteropDTO;
import it.ibm.red.business.dto.MessaggioPostaNpsDTO;
import it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO;

/**
 *  The Class IPostaNpsDAO.
 * 
 * @author m.crescentini
 *
 *	Interfaccia del DAO per la gestione della posta in ingresso da NPS
 */
public interface IPostaNpsDAO extends Serializable {
	
	/**
	 * @param messaggio
	 * @param con
	 * @return
	 */
	<T extends MessaggioPostaNpsDTO> long inserisciRichiestaElabMessaggioInCoda(RichiestaElabMessaggioPostaNpsDTO<T> richiesta, Connection conn);
	
	/**
	 * @param con
	 * @return
	 */
	<T extends MessaggioPostaNpsDTO> RichiestaElabMessaggioPostaNpsDTO<T> getAndLockRichiestaElabMessaggio(Long idAoo, Connection con);
	
	/**
	 * @param idRichiesta
	 * @param stato
	 * @param con
	 * @return
	 */
	int aggiornaStatoRichiestaElabMessaggioPostaNps(Long idRichiesta, int stato, Connection con);

	/**
	 * @param idRichiesta
	 * @param stato
	 * @param errore
	 * @param con
	 * @return
	 */
	int aggiornaStatoRichiestaElabMessaggioPostaNps(Long idRichiesta, int stato, String errore, Connection con);
	
	
	/**
	 * Verifica se l'assegnatario indicato è presente nel DB di RED.
	 * 
	 * @param assegnatario
	 * @param con
	 * @return
	 */
	boolean verificaPreAssegnatario(AssegnatarioInteropDTO assegnatario, Connection con);
	
	/**
	 * Restituisce la descrizione dell'ufficio e dell'utente a partire dagli id 
	 * Utile per il tooltip nella coda mail
	 * @param assegnatario
	 * @param con
	 * @return
	 */
	AssegnatarioInteropDTO getDescrizioneById(AssegnatarioInteropDTO assegnatario, Connection con);
 
}
