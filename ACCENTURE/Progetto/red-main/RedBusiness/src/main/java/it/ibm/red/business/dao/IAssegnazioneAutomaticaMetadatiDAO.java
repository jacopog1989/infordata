package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.AssegnazioneMetadatiDTO;

/**
 * DAO che gestisce le assegnazioni automatiche dei metadati esistenti nella
 * tabella ASSEGNAZIONE_METADATI.
 * 
 * @author SimoneLungarella
 */
public interface IAssegnazioneAutomaticaMetadatiDAO extends Serializable {

	/**
	 * Metodo per il recupero delle assegnazioni automatiche dell'area organizzativa
	 * identificata dall' {@code idAoo}.
	 * 
	 * @param idAoo
	 *            Identificativo dell'area organizzativa di cui si vogliono
	 *            recuperare le assegnazioni automatiche.
	 * @param connection
	 *            Connessione al database.
	 * 
	 * @return La lista dei dto delle assegnazioni automatiche dei metadati.
	 */
	List<AssegnazioneMetadatiDTO> getByIdAoo(Connection connection, Long idAoo);

	/**
	 * Metodo per la cancellazione di tutte le assegnazioni automatiche dei metadati
	 * per l'area organizzativa identificata dall' {@code idAoo}.
	 * 
	 * @param idAoo
	 *            Identificativo dell'are organizzativa per cui si vogliono
	 *            rimuovere le assegnazioni automatiche.
	 * @param connection
	 *            Connessione al database.
	 */
	void deleteByIdAoo(Connection connection, Long idAoo);

	/**
	 * Metodo per effettuare il salvataggio di una assegnazione automatica dei
	 * metadati.
	 * 
	 * @param assegnazioneMetadato
	 *            DTO per la memorizzazione delle informazioni da salvare in
	 *            database.
	 * @param idAoo
	 *            Identificativo dell'area organizzativa a cui è associata
	 *            l'assegnazione automatica.
	 * @param connection
	 *            Connessione al database.
	 */
	void registraAssegnazioneMetadato(Connection connection, AssegnazioneMetadatiDTO assegnazioneMetadato, Long idAoo);

	/**
	 * Metodo che consente la cancellazione di un'assegnazione automatica
	 * identificata dall' {@code idAssegnazione}.
	 * 
	 * @param idAssegnazione
	 *            Identificativo dell'assegnazione automatica da eliminare.
	 * @param connection
	 *            Connessione al database.
	 */
	void deleteByIdAssegnazione(Connection connection, Integer idAssegnazione);

	/**
	 * Metodo che permette di recuperare le assegnazione automatiche di una coppia
	 * documento - procedimento identificate dall' {@code idTipoDocumento} e dall'
	 * {@code idTipoProcedimento}.
	 * 
	 * @param connection
	 *            Connessione al database.
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @param idTipoDocumento
	 *            Identificativo della tipologia documento.
	 * @param idTipoProcedimento
	 *            Identificativo della tipologia procedimento.
	 * @return Lista delle assegnazioni automatiche recuperate.
	 */
	List<AssegnazioneMetadatiDTO> getAssegnazioniMetadati(Connection connection, Long idAoo, Long idTipoDocumento,
			Long idTipoProcedimento);

	/**
	 * Ottiene le assegnazioni automatiche per il tipo documento identificato dall'
	 * {@code idTipoDocumento}.
	 * 
	 * @param idAoo
	 *            Identificativo dell'area organizzativa per cui è valida
	 *            l'assegnazione automatica.
	 * @param idTipoDocumento
	 *            Identificativo del tipo documento dell'assegnazione automatica.
	 * @param connection
	 *            Connessione al database.
	 * @return Lista delle assegnazioni esistenti recuperate.
	 */
	List<AssegnazioneMetadatiDTO> getAssegnazioniByTipoDoc(Long idAoo, Long idTipoDocumento, Connection connection);

}
