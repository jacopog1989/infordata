package it.ibm.red.business.service.concrete;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.xml.datatype.DatatypeConfigurationException;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.Document;
import com.filenet.apiimpl.core.SubSetImpl;
import com.google.common.net.MediaType;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.Protocollo;
import it.ibm.red.business.dao.IContattoDAO;
import it.ibm.red.business.dao.IGestioneNotificheEmailDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.INpsAsyncRequestDAO;
import it.ibm.red.business.dao.INpsConfigurationDAO;
import it.ibm.red.business.dao.IRegistroAusiliarioDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.DestinatarioDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DetailDocumentoDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.MessaggioEmailDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.ParamsRicercaProtocolloDTO;
import it.ibm.red.business.dto.ParamsRicercaRegistrazioniAusiliarieDTO;
import it.ibm.red.business.dto.ProtocolloDTO;
import it.ibm.red.business.dto.ProtocolloEmergenzaDocDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.RegistrazioneAusiliariaNPSDTO;
import it.ibm.red.business.dto.RegistroDTO;
import it.ibm.red.business.dto.RegistroProtocolloDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedDTO;
import it.ibm.red.business.dto.SecurityDTO;
import it.ibm.red.business.dto.TipologiaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.enums.PostaEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoAllaccioEnum;
import it.ibm.red.business.enums.TipoDocumentoMockEnum;
import it.ibm.red.business.enums.TipoMetadatoEnum;
import it.ibm.red.business.enums.TipoProtocolloEnum;
import it.ibm.red.business.enums.TipoRegistroAusiliarioEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.email.EmailHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.nps.BusinessDelegate;
import it.ibm.red.business.nps.NPSAsynchCaller;
import it.ibm.red.business.nps.builder.Builder;
import it.ibm.red.business.nps.builder.Director;
import it.ibm.red.business.nps.dto.AllaccioDTO;
import it.ibm.red.business.nps.dto.AllegatoNpsDTO;
import it.ibm.red.business.nps.dto.AssegnanteDTO;
import it.ibm.red.business.nps.dto.AssegnatariDTO;
import it.ibm.red.business.nps.dto.AssegnatarioDTO;
import it.ibm.red.business.nps.dto.AsyncRequestDTO;
import it.ibm.red.business.nps.dto.DocumentoNpsDTO;
import it.ibm.red.business.nps.dto.PersonaFisicaDTO;
import it.ibm.red.business.nps.dto.StoricoDTO;
import it.ibm.red.business.nps.exceptions.BusinessDelegateRuntimeException;
import it.ibm.red.business.nps.exceptions.ProtocolloGialloException;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.NodoUtenteRuolo;
import it.ibm.red.business.persistence.model.NpsAzioniConfig;
import it.ibm.red.business.persistence.model.NpsConfiguration;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.provider.WebServiceClientProvider;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.IMockSRV;
import it.ibm.red.business.service.INpsSRV;
import it.ibm.red.business.service.IProtocolliEmergenzaSRV;
import it.ibm.red.business.service.ITipologiaDocumentoSRV;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.PermessiUtils;
import it.ibm.red.business.utils.ServerUtils;
import it.ibm.red.business.utils.StringUtils;
import npsmessages.v1.it.gov.mef.BaseServiceResponseType;
import npsmessages.v1.it.gov.mef.EsitoType;
import npsmessages.v1.it.gov.mef.RichiestaAggiungiAllaccioProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaAggiungiAssegnatarioProtocolloEntrataType;
import npsmessages.v1.it.gov.mef.RichiestaAggiungiDocumentiProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaAggiungiDocumentiProtocolloType.Documenti;
import npsmessages.v1.it.gov.mef.RichiestaAnnullamentoProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaCambiaStatoProtocolloEntrataType;
import npsmessages.v1.it.gov.mef.RichiestaCambiaStatoProtocolloUscitaType;
import npsmessages.v1.it.gov.mef.RichiestaCollegaRegistrazioneAusiliariaProtocolloUscitaType;
import npsmessages.v1.it.gov.mef.RichiestaModificaSpedizioneDestinatarioProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaSpedisciProtocolloUscitaType;
import npsmessages.v1.it.gov.mef.RichiestaUpdateDatiProtocolloType;
import npsmessages.v1.it.gov.mef.RichiestaUploadDocumentoType;
import npsmessages.v1.it.gov.mef.RispostaAnnullamentoProtocolloType;
import npsmessages.v1.it.gov.mef.RispostaCreateProtocolloEntrataType;
import npsmessages.v1.it.gov.mef.RispostaCreateProtocolloUscitaType;
import npsmessages.v1.it.gov.mef.RispostaCreateRegistrazioneAusiliariaType;
import npsmessages.v1.it.gov.mef.RispostaGetProtocolloEntrataType;
import npsmessages.v1.it.gov.mef.RispostaGetProtocolloUscitaType;
import npsmessages.v1.it.gov.mef.RispostaInviaAttoType;
import npsmessages.v1.it.gov.mef.RispostaInviaMessaggioPostaType;
import npsmessages.v1.it.gov.mef.RispostaReportProtocolloType;
import npsmessages.v1.it.gov.mef.RispostaReportRegistroGiornalieroType;
import npsmessages.v1.it.gov.mef.RispostaReportRegistroGiornalieroType.Report;
import npsmessages.v1.it.gov.mef.RispostaRicercaProtocolloType;
import npsmessages.v1.it.gov.mef.RispostaRicercaRegistrazioneAusiliariaType;
import npsmessages.v1.it.gov.mef.RispostaUpdateDatiProtocolloType;
import npsmessages.v1.it.gov.mef.RispostaUploadDocumentoType;
import npsmessages.v1.it.gov.mef.ServiceErrorType;
import npstypes.v1.it.gov.mef.AllChiaveEsternaType;
import npstypes.v1.it.gov.mef.AllTipoRegistroAusiliarioType;
import npstypes.v1.it.gov.mef.AnaEnteEsternoType;
import npstypes.v1.it.gov.mef.AnaPersonaFisicaType;
import npstypes.v1.it.gov.mef.AnaPersonaGiuridicaType;
import npstypes.v1.it.gov.mef.DocTipoOperazioneType;
import npstypes.v1.it.gov.mef.OrgOrganigrammaType;
import npstypes.v1.it.gov.mef.ProtAllaccioProtocolloType;
import npstypes.v1.it.gov.mef.ProtAnnullamentoType;
import npstypes.v1.it.gov.mef.ProtAssegnatarioType;
import npstypes.v1.it.gov.mef.ProtAttributiEstesiType;
import npstypes.v1.it.gov.mef.ProtDestinatarioType;
import npstypes.v1.it.gov.mef.ProtDocumentoProtocollatoType;
import npstypes.v1.it.gov.mef.ProtMetadatiType;
import npstypes.v1.it.gov.mef.ProtMetadatoAssociatoType;
import npstypes.v1.it.gov.mef.ProtMittenteType;
import npstypes.v1.it.gov.mef.ProtProtocolloBaseResponseType;
import npstypes.v1.it.gov.mef.ProtProtocolloDatiMinimiType;
import npstypes.v1.it.gov.mef.ProtProtocolloEntrataResponseType;
import npstypes.v1.it.gov.mef.ProtRegistroOperazioniProtocolloType;
import npstypes.v1.it.gov.mef.ProtRicezioneType;
import npstypes.v1.it.gov.mef.ProtRisultatoRicercaProtocolloType;
import npstypes.v1.it.gov.mef.ProtTipoDettaglioEstrazioneType;
import npstypes.v1.it.gov.mef.ProtTipoProtocolloType;
import npstypes.v1.it.gov.mef.ProtTipologiaDocumentoType;
import npstypes.v1.it.gov.mef.RegRegistrazioneAusiliariaType;
import npstypes.v1.it.gov.mef.WkfDestinatarioFlussoType;
import npstypesestesi.v1.it.gov.mef.MetaDatoAssociato;
import npstypesestesi.v1.it.gov.mef.TIPOLOGIA;

/**
 * Servizio per gestire l'integrazione con NPS.
 *
 * @author CPIERASC
 */
@SuppressWarnings({ "rawtypes", "static-access" })
@Service
public class NpsSRV extends AbstractService implements INpsSRV {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -7580997820663651227L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NpsSRV.class.getName());
	
	/**
	 * Label Su NPS.
	 */
	private static final String SU_NPS = " su NPS";

	/**
	 * Label NPS FAULT.
	 */
	private static final String NPSFAULT = "##NPSFAULT##";

	/**
	 * Messaggio errore generato se il documento non ha almeno un destinatario
	 * esterno.
	 */
	private static final String ERROR_DESTINATARIO_ESTERNO_ASSENTE_MSG = "Per protocollare in uscita, il documento deve avere almeno un destinatario esterno.";

	/**
	 * Messaggio di esito negativo da parte di NPS.
	 */
	private static final String NPS_ESITO_NEGATIVO_MSG = "Il servizio di protocollazione NPS ha restituito esito negativo";

	/**
	 * Messaggio di errore upload documento.
	 */
	private static final String ERROR_UPLOAD_DOCUMENTO_MSG = "Errore in fase di upload del documento ";

	/**
	 * Messaggio errore creazione protocollo uscita da NPS.
	 */
	private static final String ERROR_CREAZIONE_PROTOCOLLO_USCITA_MSG = "Errore durante la creazione del protocollo in uscita da NPS ";

	/**
	 * Cartaceo.
	 */
	private static final String CARTACEO = "CARTACEO";

	/**
	 * Tipo report.
	 */
	private static final String TIPO_REPORT = "ETICHETTA";

	/**
	 * Fully qualified name classe Documento.
	 */
	private static final String COM_CAPGEMINI_NSD_NPS_BUSINESS_DELEGATE = "it.ibm.red.business.nps.BusinessDelegate$Documento";

	/**
	 * Dao contatto.
	 */
	@Autowired
	private IContattoDAO contattoDAO;

	/**
	 * Dao utente.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;

	/**
	 * Dao nodo.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * Dao gestione configurazione nps.
	 */
	@Autowired
	private INpsConfigurationDAO npsConfigurationDAO;

	/**
	 * Dao gestione richieste asincrone NPS.
	 */
	@Autowired
	private INpsAsyncRequestDAO npsAsyncRequestDAO;

	/**
	 * Servizio.
	 */
	@Autowired
	private ITipologiaDocumentoSRV tipologiaDocumentoSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IProtocolliEmergenzaSRV protocolloEmergenzaDocSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IUtenteFacadeSRV utenteSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IAooSRV aooSRV;

	/**
	 * Dao registri ausiliari.
	 */
	@Autowired
	private IRegistroAusiliarioDAO registroAusiliarioDAO;

	/**
	 * Servizio.
	 */
	@Autowired
	private IMockSRV mockSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private IGestioneNotificheEmailDAO gestioneNotificheEmailDAO;

	/**
	 * Fornitore della properties dell'applicazione.
	 */
	private PropertiesProvider pp;
	
	/**
	 * Metodo upload.
	 */
	private static final String UPLOAD_DOCUMENTO_METHOD_NAME = "upload";

	/**
	 * Metodo spedisci protocollo uscita.
	 */
	private static final String SPEDISCI_PROTOCOLLO_USCITA_METHOD_NAME = "spedisciProtocolloUscita";

	/**
	 * Metodo di aggiornamento del protocollo a completo.
	 */
	private static final String UPDATE_PROTOCOLLO_COMPLETO_METHOD_NAME = "updateProtocolloCompleto";

	/**
	 * Metodo aggiungi.
	 */
	private static final String AGGIUNGI_DOCUMENTI_PROTOCOLLO_METHOD_NAME = "aggiungi";

	/**
	 * Metodo per la modifica di un destinatario di un protocollo.
	 */
	private static final String MODIFICA_DESTINATARIO_PROTOCOLLO_METHOD_NAME = "modifica";

	/**
	 * Metodo per la modifica dello stato di un protocollo in entrata.
	 */
	private static final String CAMBIA_STATO_PROTOCOLLO_ENTRATA_METHOD_NAME = "cambiaStatoProtEntrata";

	/**
	 * Metodo per la modifica dello stato di un protocollo in uscita.
	 */
	private static final String CAMBIA_STATO_PROTOCOLLO_USCITA_METHOD_NAME = "cambiaStatoProtUscita";

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.service.INpsSRV#associateDocumentoProtocolloToAsyncNps(int,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO, boolean, boolean, java.lang.String,
	 *      npstypes.v1.it.gov.mef.DocTipoOperazioneType, java.lang.String,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public final void associateDocumentoProtocolloToAsyncNps(final int idNar, final String guid, final String protocolloGuid, final String nomeFile, final String descrizione,
			final UtenteDTO utente, final boolean isPrincipale, final boolean daInviare, final String idDocumentoOperazione, final DocTipoOperazioneType tipoOperazioneEnum,
			final String numeroAnnoProtocollo, final String idDocumento, final Connection connection) {
		associateDocumentoProtocolloToAsyncNps(true, idNar, guid, protocolloGuid, nomeFile, 
				descrizione, utente, isPrincipale, daInviare, idDocumentoOperazione, tipoOperazioneEnum, 
				numeroAnnoProtocollo, idDocumento, connection);
	}
	
	/**
	 * @see it.ibm.red.business.service.INpsSRV#associateDocumentoProtocolloToNps(int,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO, boolean, boolean, java.lang.String,
	 *      npstypes.v1.it.gov.mef.DocTipoOperazioneType, java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public final void associateDocumentoProtocolloToNps(final int idNar, final String guid, final String protocolloGuid, final String nomeFile, final String descrizione,
			final UtenteDTO utente, final boolean isPrincipale, final boolean daInviare, final String idDocumentoOperazione, final DocTipoOperazioneType tipoOperazioneEnum,
			final String idDocumento, final Connection connection) {
		associateDocumentoProtocolloToAsyncNps(false, idNar, guid, protocolloGuid, nomeFile, 
				descrizione, utente, isPrincipale, daInviare, idDocumentoOperazione, tipoOperazioneEnum, 
				null, idDocumento, connection);
	}

	/**
	 * Associa il documento al protocollo nps.
	 * 
	 * @param async
	 *            flag associato al tipo di aggiornamento
	 * @param idNar
	 *            id nar
	 * @param guid
	 *            guid documento
	 * @param protocolloGuid
	 *            guid protocollo
	 * @param nomeFile
	 *            nome file documento
	 * @param descrizione
	 *            descrizione documento
	 * @param utente
	 *            utente
	 * @param isPrincipale
	 *            flag <em> principale </em>
	 * @param daInviare
	 *            flag <em> da inviare </em>
	 * @param idDocumentoOperazione
	 *            id documento operazione
	 * @param tipoOperazioneEnum
	 *            tipo operazione
	 * @param numeroAnnoProtocollo
	 *            numero e anno protocollo
	 * @param idDocumento
	 *            id documento
	 * @param connection
	 *            connessione al database
	 */
	private final void associateDocumentoProtocolloToAsyncNps(final boolean async, final int idNar, final String guid, final String protocolloGuid, final String nomeFile, final String descrizione,
			final UtenteDTO utente, final boolean isPrincipale, final boolean daInviare, final String idDocumentoOperazione, final DocTipoOperazioneType tipoOperazioneEnum,
			final String numeroAnnoProtocollo, final String idDocumento, final Connection connection) {
		final Documenti[] documentiNps = new Documenti[1];
		documentiNps[0] = Builder.buildDocumentoToAddToNps(guid, nomeFile, descrizione, isPrincipale, daInviare, idDocumentoOperazione, tipoOperazioneEnum);

		final OrgOrganigrammaType operatoreOrg = creaOperatoreNps(utente.getIdUfficio(), utente.getId(), null, false, connection);

		final RichiestaAggiungiDocumentiProtocolloType input = Director.constructAggiungiDocumentiProtocolloInput(true, protocolloGuid, operatoreOrg, documentiNps);
		
		if(async) {
			final AsyncRequestDTO request = costructRequest(idNar, input, new Date(), npsAsyncRequestDAO.getPartition(utente.getIdAoo().intValue(), connection), 1, 1,
					AGGIUNGI_DOCUMENTI_PROTOCOLLO_METHOD_NAME, numeroAnnoProtocollo, BusinessDelegate.Documento.class,
					protocolloEmergenzaDocSRV.isEmergenzaNotUpdated(protocolloGuid, connection), idDocumento);
	
			insertRequest(request, connection);
		} else {
			BusinessDelegate.Documento.aggiungi(utente.getIdAoo().intValue(), UUID.randomUUID().toString(), input );
		}
	}
	
	
	@Override
	public final int uploadDocToAsyncNps(final InputStream contentIS, final String guid, final String guidProtocollo, final String contentType,
			final String nomefile, final String descrizione, final String numeroAnnoProtocollo, final UtenteDTO utente, final String idDocumento, 
			final Connection connection) {
		return uploadDocToAsyncNps(contentIS, guid, guidProtocollo, contentType, nomefile, descrizione, numeroAnnoProtocollo, utente, idDocumento, false, connection);
	}
	

	@Override
	public final int uploadDocToAsyncNps(final InputStream contentIS, final String guid, final String guidProtocollo, final String contentType,
			final String nomefile, final String descrizione, final String numeroAnnoProtocollo, final UtenteDTO utente, final String idDocumento, 
			final boolean firmato, final Connection connection) {
		int idNar = 0;

		DocumentoNpsDTO documentoNps = null;
		try {
			byte[] content = null;
			byte[] hash = null;
			if (contentIS != null) {
				content = IOUtils.toByteArray(contentIS);
				hash = DigestUtils.sha1Hex(content).getBytes();
			}

			documentoNps = Builder.buildDocumentoToUploadToNps(guid, contentType, nomefile, descrizione, content, hash);
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'upload asincrono su NPS del documento con GUID: " + guid, e);
			throw new RedException(e);
		}

		if (documentoNps != null) {
			idNar = uploadDocToAsyncNps(documentoNps, guidProtocollo, numeroAnnoProtocollo, utente, idDocumento, firmato, connection);
		}

		return idNar;
	}

	/**
	 * Upload del documento su NPS tramite l'inserimento di una richiesta asincrona.
	 *
	 * @param documentoNps
	 * @param guidProtocollo
	 * @param numeroAnnoProtocollo
	 * @param utente
	 * @param idDocumento
	 * @param firmato
	 * @param connection
	 * @return
	 */
	private final int uploadDocToAsyncNps(final DocumentoNpsDTO documentoNps, final String guidProtocollo, final String numeroAnnoProtocollo,
			final UtenteDTO utente, final String idDocumento, final boolean firmato, final Connection connection) {
		int idNar = 0;

		try {
			final String chiaveEsterna = documentoNps.getGuid();
			final String descrizioneDocNps = "upload_" + documentoNps.getDescrizione();
			final String nomeFile = documentoNps.getNomeFile();
			final RichiestaUploadDocumentoType input = Director.constructUploadDocumentoInput(true, chiaveEsterna, true,
					documentoNps.getInputStream(), descrizioneDocNps, nomeFile, firmato, documentoNps.getHash(),
					StringUtils.cleanGuidToString(documentoNps.getGuid()), null, documentoNps.getContentType(), null,
					null, null, null, null, null, null, null, null, null);

			final AsyncRequestDTO request = costructRequest(input, new Date(),
					npsAsyncRequestDAO.getPartition(utente.getIdAoo().intValue(), connection), 1, 1, UPLOAD_DOCUMENTO_METHOD_NAME,
					numeroAnnoProtocollo, protocolloEmergenzaDocSRV.isEmergenzaNotUpdated(guidProtocollo, connection), idDocumento);

			// Si inserisce la richiesta asincrona
			idNar = insertRequest(request, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'upload asincrono su NPS del documento con GUID: " + documentoNps.getGuid(), e);
			throw new RedException(e);
		}

		return idNar;
	}
	
	/**
	 * @see it.ibm.red.business.service.INpsSRV#uploadDocToNps(java.lang.Integer,
	 *      boolean, java.lang.String, java.lang.String, java.lang.String,
	 *      java.io.InputStream, java.util.Date, java.lang.String).
	 */
	@Override
	public String uploadDocToNps(final Integer idAoo, final boolean firmato, final String fileName, final String mimetype, final String descrizione, final InputStream content,
			final Date dataDocumento, final String guidDocumento) throws IOException {
		String idDocumentoNps = null;

		try {
			final byte[] contentBA = IOUtils.toByteArray(content);
			final byte[] hash = DigestUtils.sha1Hex(contentBA).getBytes();
			final Long length = (long) contentBA.length;

			idDocumentoNps = StringUtils.cleanGuidToString(guidDocumento);

			final RispostaUploadDocumentoType response = BusinessDelegate.Documento.upload(idAoo, firmato, fileName, null, true, mimetype, descrizione,
					new ByteArrayInputStream(contentBA), hash, null, null, null, dataDocumento, null, null, null, true, guidDocumento, idDocumentoNps, null, length, null);

			if (EsitoType.OK.equals(response.getEsito())) {
				idDocumentoNps = response.getDatiDocumento().getIdDocumento();
			} else {
				final boolean actualError = isActualError(response);
				
				if (!actualError) {
					LOGGER.info("uploadDocToNps -> Documento " + fileName + " con ID " + idDocumentoNps + " gia' presente su NPS.");
				} else {
					logErrorResponse(response, "uploadDocToNps");
					throw new RedException(ERROR_UPLOAD_DOCUMENTO_MSG + fileName + SU_NPS);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(ERROR_UPLOAD_DOCUMENTO_MSG + fileName + SU_NPS, e);
			if (!StringUtils.isNullOrEmpty(e.getMessage()) && e.getMessage().contains(NPSFAULT)) {
				throw new ProtocolloGialloException(ERROR_UPLOAD_DOCUMENTO_MSG + fileName + SU_NPS, e);
			} else {
				throw new RedException(ERROR_UPLOAD_DOCUMENTO_MSG + fileName + SU_NPS, e);
			}
		}

		return idDocumentoNps;
	}
	
	/**
	 * Verifica, laddove sono presenti errori nella response di NPS, se le casistiche di errore di NPS possono essere bypassate.
	 * 
	 * @param risposta
	 * @return
	 */
	private boolean isActualError(BaseServiceResponseType risposta) {
		boolean actualError = false;
		if(risposta != null && risposta.getErrorList() != null && !risposta.getErrorList().isEmpty()) {
			for(ServiceErrorType e: risposta.getErrorList()) {
				Integer codice = e.getErrorCode();
				String descrizione = e.getErrorMessageString();
				
				if(!pp.existsInPropertiesList(PropertiesNameEnum.NPS_WARNING_CODICI, codice.toString()) &&
						!pp.existsInPropertiesList(PropertiesNameEnum.NPS_WARNING_DESCRIZIONI, descrizione)) {
					actualError = true;
					break;
				}
				
			}
		}
		return actualError;
	}

	/**
	 * Metodo per costruire richiesta.
	 * 
	 * @param input         xml object
	 * @param dataCreazione data creazione
	 * @param partition     partizione
	 * @param priority      priorità
	 * @param stato         stato
	 * @param methodName    nome del metodo
	 * @return richiesta
	 */
	@SuppressWarnings("unchecked")
	private static AsyncRequestDTO costructRequest(final Object input, final Date dataCreazione, final int partition, final int priority, final int stato,
			final String methodName, final String numeroAnnoProtocollo, final boolean isEmergenza, final String idDocumento) {
		final AsyncRequestDTO asyncRequestDTO = new AsyncRequestDTO();
		asyncRequestDTO.setInput(input);
		asyncRequestDTO.setXml(input);
		asyncRequestDTO.setDataCreazione(dataCreazione);
		asyncRequestDTO.setPartition(partition);
		asyncRequestDTO.setPriority(priority);
		asyncRequestDTO.setStato(stato);
		asyncRequestDTO.setClassName(COM_CAPGEMINI_NSD_NPS_BUSINESS_DELEGATE);
		asyncRequestDTO.setMethodName(methodName);
		if (!isEmergenza) {
			asyncRequestDTO.setNumeroAnnoProtocollo(numeroAnnoProtocollo);
		} else {
			asyncRequestDTO.setNumeroAnnoProtocolloEmergenza(numeroAnnoProtocollo);
		}
		asyncRequestDTO.setClassNameInput(input.getClass().getName());
		asyncRequestDTO.setIdDocumento(idDocumento);
		return asyncRequestDTO;
	}

	/**
	 * Metodo per costruire richiesta.
	 * 
	 * @param id_nar        id_nar
	 * @param input         xml object
	 * @param dataCreazione data creazione
	 * @param partition     partizione
	 * @param priority      priorità
	 * @param stato         stato
	 * @param methodName    nome del metodo
	 * @return richiesta
	 */
	@SuppressWarnings("unchecked")
	private static AsyncRequestDTO costructRequest(final int idNar, final Object input, final Date dataCreazione, final int partition, final int priority, final int stato,
			final String methodName, final String numeroAnnoProtocollo, final Class className, final boolean isEmergenza, final String idDocumento) {
		final AsyncRequestDTO asyncRequestDTO = new AsyncRequestDTO();
		asyncRequestDTO.setInput(input);
		asyncRequestDTO.setXml(input);
		asyncRequestDTO.setDataCreazione(dataCreazione);
		asyncRequestDTO.setPartition(partition);
		asyncRequestDTO.setPriority(priority);
		asyncRequestDTO.setId_nar_execute_required(idNar);
		asyncRequestDTO.setStato(stato);
		asyncRequestDTO.setClassName(className.getName());
		asyncRequestDTO.setMethodName(methodName);
		if (!isEmergenza) {
			asyncRequestDTO.setNumeroAnnoProtocollo(numeroAnnoProtocollo);
		} else {
			asyncRequestDTO.setNumeroAnnoProtocolloEmergenza(numeroAnnoProtocollo);
		}
		asyncRequestDTO.setClassNameInput(input.getClass().getName());
		asyncRequestDTO.setIdDocumento(idDocumento);
		return asyncRequestDTO;
	}

	/**
	 * Metodo per inserire una richiesta.
	 * 
	 * @param request    richiesta
	 * @param connection connessione db
	 */
	private int insertRequest(final AsyncRequestDTO request, final Connection connection) {
		return npsAsyncRequestDAO.insert(request, connection);
	}

	@Override
	public final void uploadAllegatiDocToAsyncNps(final String guidDocumentoPrincipale, final String infoProtocollo, final String guidProtocollo, final UtenteDTO utente,
			final boolean isCollegato, final IFilenetCEHelper fceh, final Connection con) {
		DocumentSet allegatiTemp = null;

		try {
			allegatiTemp = fceh.getAllegati(guidDocumentoPrincipale, false, false);
			DocumentoNpsDTO allegatoNsdNps;

			if (allegatiTemp != null && !allegatiTemp.isEmpty()) {

				final Map<ContextTrasformerCEEnum, Object> context = tipologiaDocumentoSRV.getContextTipologieDocumento(CategoriaDocumentoEnum.USCITA.getIds()[0], utente,
						con);

				final Iterator<?> allegatiIt = allegatiTemp.iterator();
				while (allegatiIt.hasNext()) {
					final Document allegatoFilenet = (Document) allegatiIt.next();
					final AllegatoDTO allegato = TrasformCE.transform(allegatoFilenet, TrasformerCEEnum.FROM_DOCUMENT_TO_ALLEGATO_CONTEXT, context, con);
					InputStream isAllegato = null;
					byte[] contentAllegato = null;
					byte[] hashAllegato = null;
					if (allegato.getFormatoSelected() != FormatoAllegatoEnum.CARTACEO && allegato.getFormatoSelected() != FormatoAllegatoEnum.NON_SPECIFICATO) {
						isAllegato = FilenetCEHelper.getDocumentContentAsInputStream(allegatoFilenet);
						if (isAllegato != null) {
							contentAllegato = IOUtils.toByteArray(isAllegato);
							hashAllegato = DigestUtils.sha1Hex(contentAllegato).getBytes();
						}
					} else {
						// Non uploadare verso NPS se l'allegato è cartaceo
						continue;
					}

					// Si esegue l'upload asincrono inserendo il relativo record nel DB
					allegatoNsdNps = Builder.buildDocumentoToUploadToNps(allegato.getGuid(), allegato.getMimeType(),
							allegato.getNomeFile(), allegato.getOggetto(), contentAllegato, hashAllegato);
					final int idNar = uploadDocToAsyncNps(allegatoNsdNps, guidProtocollo, infoProtocollo, utente, allegato.getDocumentTitle(), false, con);

					if (idNar > 0) {
						// Se collegato, si recupera il guid della versione principale per associarlo
						DocTipoOperazioneType tipoOperazioneEnum = null;
						String idDocumentoOperazione = null;
						int numVersions = 1;
						if (allegatoFilenet.get_VersionSeries() != null && allegatoFilenet.get_VersionSeries().get_Versions() != null) {
							numVersions = ((SubSetImpl) allegatoFilenet.get_VersionSeries().get_Versions()).getList().size();
						}
						if (isCollegato && numVersions > 1) {
							final Document firstVersion = fceh.getFirstVersionByGuid(allegato.getGuid());
							tipoOperazioneEnum = DocTipoOperazioneType.COLLEGA_DOCUMENTO;
							idDocumentoOperazione = firstVersion.get_Id().toString();
						}

						// Si inserisce aggiungi_documento/allegato asincrono sul DB
						associateDocumentoProtocolloToAsyncNps(idNar, allegatoNsdNps.getGuid(), guidProtocollo, allegatoNsdNps.getNomeFile(), allegatoNsdNps.getDescrizione(),
								utente, false, true, idDocumentoOperazione, tipoOperazioneEnum, infoProtocollo, allegato.getDocumentTitle(), con);
					}
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'upload asincrono su NPS degli allegati. GUID documento principale: " + guidDocumentoPrincipale, e);
			throw new RedException(e);
		}
	}

	/**
	 * creazione protocollo in uscita.
	 *
	 * @param documentDetail   the document detail
	 * @param acl              the acl
	 * @param utenteFirmatario the utente firmatario
	 * @param statoProtocollo  the stato protocollo
	 * @param connection       the connection
	 * @return the protocollo nps DTO
	 */
	@Override
	public final ProtocolloNpsDTO creaProtocolloUscita(final DetailDocumentoDTO documentDetail, final String acl, final UtenteDTO utenteFirmatario, 
			final String statoProtocollo, final Connection connection, final IFilenetCEHelper fceh) {
		ProtocolloNpsDTO protocollo = null;
		try {

			if (!StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED)) && "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[creaProtocolloUscita] Non è possibile comunicare con NPS in regime mock");

				final ProtocolloDTO prot = mockSRV.getNewProtocollo(utenteFirmatario.getCodiceAoo(), TipoProtocolloEnum.USCITA);
				return trasform(prot);
			}

			// ### DESTINATARI
			// Recupero IDs dei destinatari esterni e verifica se è un Registro di
			// Repertorio
			if (!documentDetail.isHasDestinatariEsterni() && StringUtils.isNullOrEmpty(documentDetail.getDescRegistroRepertorio())) {
				throw new RedException(ERROR_DESTINATARIO_ESTERNO_ASSENTE_MSG);
			}

			final ProtDestinatarioType[] destinatariNPS = getDestinatariNPS(documentDetail.getDestinatariDocumento(), null, utenteFirmatario.getIdAoo().intValue(),
					documentDetail.getDocumentTitle(), utenteFirmatario.getTipoPosta(), connection, utenteFirmatario);

			String indiceFascicoloNps = Constants.EMPTY_STRING;
			String descrIndiceFascicoloNps = null;
			if (documentDetail.getIndiceFascicolo() != null) {
				indiceFascicoloNps = documentDetail.getIndiceFascicolo();
				descrIndiceFascicoloNps = indiceFascicoloNps + " - " + documentDetail.getDescrizioneIndiceFascicolo();
			}

			final String tipologiaDocumento = getTipologiaDocumentoNPS(documentDetail.getIdTipologiaDocumento().intValue(), documentDetail.getIdTipoProcedimento().intValue(),
					documentDetail.getTipologiaDocumento(), utenteFirmatario.getIdAoo().intValue(), connection);
			
			final boolean isRiservato = documentDetail.getFlagRiservato() != null && documentDetail.getFlagRiservato();
			
			protocollo = creaProtocolloUscita(destinatariNPS, acl, Long.valueOf(documentDetail.getIdUfficioCreatore()), Long.valueOf(documentDetail.getIdUtenteCreatore()),
					utenteFirmatario, documentDetail.getOggetto(), tipologiaDocumento, indiceFascicoloNps, descrIndiceFascicoloNps, statoProtocollo,
					documentDetail.getDescRegistroRepertorio(), documentDetail.getMetadatiEstesi(), isRiservato, connection);

			if (protocollo == null  || protocollo.getNumeroProtocollo() == 0 || StringUtils.isNullOrEmpty(protocollo.getIdProtocollo()) 
					|| StringUtils.isNullOrEmpty(protocollo.getAnnoProtocollo()) || "null".equalsIgnoreCase(protocollo.getAnnoProtocollo())) {
				throw new RedException(NPS_ESITO_NEGATIVO_MSG);
			}

			return protocollo;
		} catch (final Exception e) {
			LOGGER.error(ERROR_CREAZIONE_PROTOCOLLO_USCITA_MSG, e);
			if (!StringUtils.isNullOrEmpty(e.getMessage()) && e.getMessage().contains(NPSFAULT)) {
				throw new ProtocolloGialloException(e);
			} else {
				throw new RedException(e);
			}
		}
	}

	@Override
	public final ProtocolloNpsDTO creaProtocolloUscita(final SalvaDocumentoRedDTO documento, final String indiceFascicolo, final String acl, 
			final UtenteDTO utente, final String statoProtocollo, final Connection connection, final IFilenetCEHelper fceh) {
		ProtocolloNpsDTO protocollo = null;

		try {
			
			if (!StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED)) && 
					"true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[creaProtocolloUscita] Non è possibile comunicare con NPS in regime mock");

				final ProtocolloDTO prot = mockSRV.getNewProtocollo(utente.getCodiceAoo(), TipoProtocolloEnum.USCITA);
				return trasform(prot);
			}

			// ### DESTINATARI
			final ProtDestinatarioType[] destinatariNPS = getDestinatariNPS(null, documento.getDestinatari(), 
					utente.getIdAoo().intValue(), documento.getDocumentTitle(), utente.getTipoPosta(), connection, utente);

			String indiceFascicoloNps = Constants.EMPTY_STRING;
			String descrIndiceFascicoloNps = null;
			if (indiceFascicolo != null) {
				indiceFascicoloNps = indiceFascicolo;
				descrIndiceFascicoloNps = indiceFascicoloNps + " - " + documento.getDescrizioneIndiceClassificazioneFascicoloProcedimentale();
			}

			final String tipologiaDocumento = getTipologiaDocumentoNPS(documento.getTipologiaDocumento().getIdTipologiaDocumento(),
					(int) documento.getTipoProcedimento().getTipoProcedimentoId(), documento.getTipologiaDocumento().getDescrizione(), utente.getIdAoo().intValue(),
					connection);
			
			final boolean isRiservato = documento.getIsRiservato() != null && documento.getIsRiservato();
			
			protocollo = creaProtocolloUscita(destinatariNPS, acl, utente.getIdUfficio(), utente.getId(), utente, documento.getOggetto(), tipologiaDocumento,
					indiceFascicoloNps, descrIndiceFascicoloNps, statoProtocollo, null, documento.getMetadatiEstesi(), isRiservato, connection);

			if (protocollo == null || protocollo.getNumeroProtocollo() == 0 || StringUtils.isNullOrEmpty(protocollo.getIdProtocollo()) 
					|| StringUtils.isNullOrEmpty(protocollo.getAnnoProtocollo()) || "null".equalsIgnoreCase(protocollo.getAnnoProtocollo())) {
				throw new RedException("Il servizio chiamato ha restituito esito negativo");
			}

			return protocollo;
		} catch (final Exception e) {
			LOGGER.error(ERROR_CREAZIONE_PROTOCOLLO_USCITA_MSG, e);
			throw new RedException(e);
		}
	}
	
	private ProtDestinatarioType[] getDestinatariNPS(final List<DestinatarioDTO> destinatari, final List<DestinatarioRedDTO> destinatariRed, final Integer idAoo, final String idDocumento, 
			final PostaEnum tipoPosta, final Connection connection, final UtenteDTO utente) {
		String destinatariTo = null;
		String destinatariCC = null;
		String indirizzoCasellaMittente = null;
		boolean competenzaImpostata = false;
		ProtDestinatarioType destInternoNPS;

		String uNome = null;
		String uCognome = null;
		String uCodiceFiscale = null;
		String uNodoDesc = null;
		String uCodiceNodo = null;
		IFilenetCEHelper fcehAdmin = null;

		try {

			fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()), utente.getIdAoo());
			final NpsConfiguration npsConf = npsConfigurationDAO.getByIdAoo(idAoo, connection);

			final List<ProtDestinatarioType> destinatariNPS = new ArrayList<>();
			if (destinatari != null) {
				Contatto c = null;
				for (final DestinatarioDTO dest : destinatari) {
					if (Constants.TipoDestinatario.ESTERNO.equalsIgnoreCase(dest.getTipo())) {
						boolean perCompetenza = false;
						boolean isPEC = false;
						String indirizzoEmail = null;

						c = contattoDAO.getContattoByID(dest.getId(), connection);

						final MezzoSpedizioneEnum mezzoSpedizione = MezzoSpedizioneEnum.getById(dest.getTipoSpedizione());

						if (!MezzoSpedizioneEnum.CARTACEO.equals(mezzoSpedizione)) {

							if (StringUtils.isNullOrEmpty(destinatariTo)) {
								// Recupera destinatari attraverso la mail spedizione
								final Document mail = fcehAdmin.fetchMailSpedizione(idDocumento, idAoo);
								destinatariTo = mail.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_EMAIL_METAKEY));
								destinatariCC = mail.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_CC_EMAIL_METAKEY));
								indirizzoCasellaMittente = mail.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.FROM_MAIL_METAKEY));
							}

							final String[] emails = EmailHelper.getSelectedMail(c, destinatariTo, destinatariCC);
							isPEC = !StringUtils.isNullOrEmpty(emails[1]);
							indirizzoEmail = isPEC ? emails[1] : emails[0];

						}

						// Si imposta per competenza il primo destinatario in TO
						if (!competenzaImpostata && !Boolean.TRUE.equals(dest.getIsCC())) {
							perCompetenza = true;
							competenzaImpostata = true;
						}

						destinatariNPS.add(getDestinatarioNPS(mezzoSpedizione, c.getTipoPersona(), c.getNome(), c.getCognome(), c.getCodiceFiscalePIva(), c.getAliasContatto(),
								indirizzoEmail, isPEC, c.getContattoID(), tipoPosta, npsConf.getCodiceAoo(), npsConf.getDenominazioneAoo(), indirizzoCasellaMittente,
								perCompetenza));

					} else {

						if (dest.getId() != null && dest.getId() > 0) {
							final NodoUtenteRuolo nur = utenteDAO.getUtenteUfficioByIdUtenteAndIdNodo(dest.getId(), dest.getIdUfficio(), connection);
							uNome = nur.getUtente().getNome();
							uCognome = nur.getUtente().getCognome();
							uCodiceFiscale = nur.getUtente().getCodiceFiscale();
							uNodoDesc = nur.getNodo().getDescrizione();
							uCodiceNodo = nur.getNodo().getCodiceNodo();
						} else {
							final Nodo nodo = nodoDAO.getNodo(dest.getIdUfficio(), connection);
							uNodoDesc = nodo.getDescrizione();
							uCodiceNodo = nodo.getCodiceNodo();
						}

						try {
							destInternoNPS = Builder.buildDestinatarioEnte(npsConf.getCodiceAmministrazione(), npsConf.getCodiceAoo(), uCodiceNodo, uNodoDesc, uNome, uCognome,
									uCodiceFiscale);
							destInternoNPS.setSpedizione(Builder.buildSpedizioneCartaceo(Builder.DA_SPEDIRE, null));

							// Si imposta per competenza il primo destinatario in TO
							if (!competenzaImpostata) {
								destInternoNPS.setPerConoscenza(Builder.NO);
								competenzaImpostata = true;
							} else {
								destInternoNPS.setPerConoscenza(Builder.SI);
							}

						} catch (final DatatypeConfigurationException e) {
							throw new RedException(e);
						}

						destinatariNPS.add(destInternoNPS);

					}

				}
			}

			if (destinatariRed != null) {
				Contatto c = null;
				for (final DestinatarioRedDTO dest : destinatariRed) {
					if (TipologiaDestinatarioEnum.ESTERNO.equals(dest.getTipologiaDestinatarioEnum())) {
						boolean perCompetenza = false;
						boolean isPEC = false;
						String indirizzoEmail = null;

						c = dest.getContatto();

						final MezzoSpedizioneEnum mezzoSpedizione = dest.getMezzoSpedizioneEnum();

						if (!MezzoSpedizioneEnum.CARTACEO.equals(mezzoSpedizione)) {

							if (StringUtils.isNullOrEmpty(destinatariTo)) {
								// Recupera destinatari attraverso la mail spedizione
								final Document mail = fcehAdmin.fetchMailSpedizione(idDocumento, idAoo);
								destinatariTo = mail.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_EMAIL_METAKEY));
								destinatariCC = mail.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_CC_EMAIL_METAKEY));
								indirizzoCasellaMittente = mail.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.FROM_MAIL_METAKEY));
							}

							final String[] emails = EmailHelper.getSelectedMail(c, destinatariTo, destinatariCC);
							isPEC = !StringUtils.isNullOrEmpty(emails[1]);
							indirizzoEmail = isPEC ? emails[1] : emails[0];

						}

						// Si imposta per competenza il primo destinatario in TO
						if (!competenzaImpostata && ModalitaDestinatarioEnum.TO.equals(dest.getModalitaDestinatarioEnum())) {
							perCompetenza = true;
							competenzaImpostata = true;
						}

						destinatariNPS.add(getDestinatarioNPS(mezzoSpedizione, c.getTipoPersona(), c.getNome(), c.getCognome(), c.getCodiceFiscalePIva(), c.getAliasContatto(),
								indirizzoEmail, isPEC, c.getContattoID(), tipoPosta, npsConf.getCodiceAoo(), npsConf.getDenominazioneAoo(), indirizzoCasellaMittente,
								perCompetenza));

					}
				}
			}

			final ProtDestinatarioType[] destinatariNPSArray = new ProtDestinatarioType[destinatariNPS.size()];
			return destinatariNPS.toArray(destinatariNPSArray);

		} finally {
			popSubject(fcehAdmin);
		}
	}

	/**
	 * Restituisce il destinatario NPS
	 * 
	 * @param mezzoSpedizione
	 *            mezzo spedizione
	 * @param tipoPersona
	 *            tipo persona
	 * @param nome
	 *            nome destinatario
	 * @param cognome
	 *            cognome destinatario
	 * @param codiceFiscalePIva
	 *            codice fiscale o partita iva destinatario
	 * @param aliasContatto
	 *            alias del contatto destinatario
	 * @param indirizzoEmail
	 *            indirizzo mail
	 * @param isPEC
	 *            flag associato all'indirizzo mail
	 * @param idContatto
	 *            id contatto
	 * @param tipoPosta
	 *            tipo posta
	 * @param codiceAoo
	 *            codice area organizzativa
	 * @param denominazioneAoo
	 *            denominazione area organizzativa
	 * @param indirizzoCasellaMittente
	 *            indirizzo casella mittente
	 * @param perCompetenza
	 *            flag <em> per competenza </em>
	 * @return ProtDestinatarioType
	 */
	@Override
	public ProtDestinatarioType getDestinatarioNPS(final MezzoSpedizioneEnum mezzoSpedizione, final String tipoPersona, final String nome, final String cognome,
			final String codiceFiscalePIva, final String aliasContatto, final String indirizzoEmail, final boolean isPEC, final Long idContatto, final PostaEnum tipoPosta,
			final String codiceAoo, final String denominazioneAoo, final String indirizzoCasellaMittente, final boolean perCompetenza) {

		ProtDestinatarioType destinatario = null;

		final String perConoscenza = perCompetenza ? Builder.NO : Builder.SI;

		if ("F".equals(tipoPersona)) {
			if (StringUtils.isNullOrEmpty(nome) || StringUtils.isNullOrEmpty(cognome)) {
				throw new RedException("Il contatto per la protocollazione NPS con ID: " + idContatto + " non ha valorizzato i campi 'Nome' e 'Cognome'");
			}
			destinatario = Builder.buildDestinatarioPF(perConoscenza, nome, cognome, codiceFiscalePIva, Builder.SMTP, indirizzoEmail);
		} else {
			String denominazione = nome;
			if (StringUtils.isNullOrEmpty(denominazione)) {
				denominazione = aliasContatto;
			}

			destinatario = Builder.buildDestinatarioPG(perConoscenza, denominazione, Builder.SMTP, indirizzoEmail);
		}

		try {
			if (MezzoSpedizioneEnum.CARTACEO.equals(mezzoSpedizione) || PostaEnum.POSTA_INTERNA.equals(tipoPosta)) {
				destinatario.setSpedizione(Builder.buildSpedizioneCartaceo(Builder.DA_SPEDIRE, null));
			} else {
				destinatario.setSpedizione(Builder.buildSpedizioneElettronico(Builder.DA_SPEDIRE, null, indirizzoCasellaMittente, isPEC, codiceAoo, denominazioneAoo));
			}
		} catch (final DatatypeConfigurationException e) {
			throw new RedException(e);
		}

		return destinatario;
	}
	
	/**
	 * @see it.ibm.red.business.service.INpsSRV#creaProtocolloUscita(npstypes.v1.it.gov.mef.ProtDestinatarioType[],
	 *      java.lang.String, java.lang.Long, java.lang.Long,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.String, java.lang.Integer,
	 *      java.lang.Integer, java.lang.String, java.lang.String, java.lang.String,
	 *      java.lang.String, java.lang.String, java.util.Collection, boolean,
	 *      java.sql.Connection).
	 */
	@Override
	public ProtocolloNpsDTO creaProtocolloUscita(final ProtDestinatarioType[] destinatariArray, final String acl, final Long idUfficioCreatore, final Long idUtenteCreatore,
			final UtenteDTO utenteFirmatario, final String oggetto, final Integer idTipologiaDocumento, final Integer idTipoProcedimento, final String tipologiaDocumentoDesc,
			final String indiceFascicolo, final String descrIndiceFascicolo, final String statoProtocollo, final String descRegistroProtocollo, 
			final Collection<MetadatoDTO> metadatiEstesi, final boolean isRiservato, final Connection connection) {
		final String tipologiaDocumento = getTipologiaDocumentoNPS(idTipologiaDocumento, idTipoProcedimento, tipologiaDocumentoDesc, utenteFirmatario.getIdAoo().intValue(), connection);
		return creaProtocolloUscita(destinatariArray, acl, idUfficioCreatore, idUtenteCreatore, utenteFirmatario, oggetto, tipologiaDocumento, 
				indiceFascicolo, descrIndiceFascicolo, statoProtocollo, descRegistroProtocollo, metadatiEstesi, isRiservato, connection);
	}

	/**
	 * Crea il protocollo uscita.
	 * 
	 * @param destinatariArray
	 *            lista dei destinatari
	 * @param acl
	 *            acl protocollo
	 * @param idUfficioCreatore
	 *            id ufficio del creatore
	 * @param idUtenteCreatore
	 *            id utente creatore
	 * @param utenteFirmatario
	 *            utente firmatario
	 * @param oggetto
	 *            oggetto del protocollo
	 * @param tipologiaDocumentoDesc
	 *            descrizione tipologia documento
	 * @param indiceFascicolo
	 *            indice fascicolo del protocollo
	 * @param descrIndiceFascicolo
	 *            descrizione indice fascicolo
	 * @param statoProtocollo
	 *            stato protocollo
	 * @param descRegistroProtocollo
	 *            descrizione registro protocollo
	 * @param metadatiEstesi
	 *            metadati estesi
	 * @param isRiservato
	 *            flag riservato
	 * @param connection
	 *            connessione al database
	 * @return Protocollo Nps creato
	 */
	private ProtocolloNpsDTO creaProtocolloUscita(final ProtDestinatarioType[] destinatariArray, final String acl, final Long idUfficioCreatore, final Long idUtenteCreatore,
			final UtenteDTO utenteFirmatario, final String oggetto, final String tipologiaDocumentoDesc, final String indiceFascicolo, final String descrIndiceFascicolo,
			final String statoProtocollo, final String descRegistroProtocollo, final Collection<MetadatoDTO> metadatiEstesi, final boolean isRiservato, final Connection connection) {

		ProtocolloNpsDTO protocollo = null;

		if ((destinatariArray == null || destinatariArray.length == 0) && StringUtils.isNullOrEmpty(descRegistroProtocollo)) {
			throw new RedException(ERROR_DESTINATARIO_ESTERNO_ASSENTE_MSG);
		}

		// MITTENTE -- ufficio/utente che ha creato il documento
		ProtMittenteType mittente = null;
		if (idUtenteCreatore != null && idUtenteCreatore > 0) {
			final NodoUtenteRuolo nur = utenteDAO.getUtenteUfficioByIdUtenteAndIdNodo(idUtenteCreatore, idUfficioCreatore, connection);
			final NpsConfiguration npsConfMittente = npsConfigurationDAO.getByIdAoo(nur.getNodo().getAoo().getIdAoo().intValue(), connection);
			final String muNome = nur.getUtente().getNome();
			final String muCognome = nur.getUtente().getCognome();
			final String muCF = nur.getUtente().getCodiceFiscale();
			final String muCodiceAmm = npsConfMittente.getCodiceAmministrazione();
			final String muCodiceAoo = npsConfMittente.getCodiceAoo();
			final String muCodiceNodo = nur.getNodo().getCodiceNodo();
			final String muNodoDesc = nur.getNodo().getDescrizione();
			mittente = Builder.buildMittenteUscitaPF(null, null, muNome, muCognome, muCF, muCodiceAmm, muCodiceAoo, muCodiceNodo, muNodoDesc);
		} else {
			final Nodo nodo = nodoDAO.getNodo(idUfficioCreatore, connection);
			if (nodo != null) {
				final NpsConfiguration npsConfMittente = npsConfigurationDAO.getByIdAoo(nodo.getAoo().getIdAoo().intValue(), connection);
				final String muCodiceAmministrazione = npsConfMittente.getCodiceAmministrazione();
				final String codiceAoo = npsConfMittente.getCodiceAoo();
				final String codiceNodo = nodo.getCodiceNodo();
				final String descrizioneNodo = nodo.getDescrizione();
				mittente = Builder.buildMittenteEnte(null, null, muCodiceAmministrazione, codiceAoo, codiceNodo, descrizioneNodo);
			}
		}

		// PROTOCOLLATORE -- chi firma/account
		final Utente utente = utenteDAO.getUtente(utenteFirmatario.getId(), connection);
		final NpsConfiguration npsConf = npsConfigurationDAO.getByIdAoo(utenteFirmatario.getIdAoo().intValue(), connection);

		// Vengono predisposti due campi perchè sul db la tabella 'NpsConfiguration'
		// rappresenta questi dati con due colonne differenti.
		String descRegistroUfficiale = npsConf.getRegistroUfficiale();
		String denominazioneRegistro = npsConf.getDenominazioneRegistro();
		if (descRegistroProtocollo != null) {
			// Nel caso del Registro Repertorio questi due parametri condivideranno lo stesso valore.
			descRegistroUfficiale = descRegistroProtocollo;
			denominazioneRegistro = descRegistroProtocollo;
		}

		final OrgOrganigrammaType protocollatoreOrg = Builder.buildOrganigrammaPf(String.valueOf(utente.getIdUtente()), utente.getCognome(), utente.getNome(),
				utente.getCodiceFiscale(), npsConf.getCodiceAmministrazione(), npsConf.getDenominazioneAmministrazione(), npsConf.getCodiceAoo(),
				npsConf.getDenominazioneAoo(), utenteFirmatario.getCodiceUfficio(), utenteFirmatario.getNodoDesc(), String.valueOf(utenteFirmatario.getIdUfficio()));

		final ProtMetadatiType metadatiAssociati = createMetadatiAssociati(metadatiEstesi);
		
		protocollo = creaProtocolloUscitaPf(utenteFirmatario.getIdAoo().intValue(), null, // dataRegistrazione
				null, // numeroRegistrazione
				descRegistroUfficiale, // codiceRegistro in tabella registro in chiave con la tabella aoo
				npsConf.getDenominazioneAmministrazione(), // denominazioneAmministrazione
				denominazioneRegistro, // denominazioneRegistro
				npsConf.getDenominazioneAoo(), // denominazioneAOO
				npsConf.getCodiceAmministrazione(), // codiceAmministrazione - verificare se ente
				npsConf.getCodiceAoo(), // codiceAoo
				ProtTipoProtocolloType.USCITA, // tipoProtocollo
				null, destinatariArray, // cDestinatari
				acl, protocollatoreOrg, // OperatoreDTO
				isRiservato, // isRiservato
				oggetto, // oggetto
				statoProtocollo, // statoProtocollo
				null, // idProtocollo
				indiceFascicolo, // codiceVoceTitolario
				descrIndiceFascicolo, // descrizioneVoceTitolario da verificare
				tipologiaDocumentoDesc, // descrizioneTipologia
				tipologiaDocumentoDesc, // codiceTipologia
				false, // risposta
				null, // oggettoRisposta
				null, // dataRegistrazioneRisposta
				null, // tipoProtocolloRisposta
				null, // idProtocolloRisposta
				null, // numeroRegistrazioneRisposta
				mittente, // mittente
				metadatiAssociati); // metadatiAssociati

		return protocollo;
	}

	/**
	 * Crea protocollo in uscita.
	 * 
	 * @param dataRegistrazione
	 *            data registrazione
	 * @param numeroRegistrazione
	 *            numero registrazione
	 * @param codiceRegistro
	 *            codice registro
	 * @param denominazioneAmministrazione
	 *            denominazione amministrazione
	 * @param denominazioneRegistro
	 *            denominazione registro
	 * @param denominazioneAOO
	 *            denominazione aoo
	 * @param codiceAmministrazione
	 *            codice amministrazione
	 * @param codiceAoo
	 *            codice aoo
	 * @param tipoProtocollo
	 *            tipo protocollo
	 * @param cAllacciati
	 *            collezione allacciati
	 * @param cDestinatari
	 *            collezione destinatari
	 * @param acl
	 *            acl
	 * @param protocollatoreOrg
	 *            protocollatore
	 * @param isRiservato
	 *            flag riservato
	 * @param oggetto
	 *            oggetto
	 * @param stato
	 *            stato
	 * @param idProtocollo
	 *            identificativo protocollo
	 * @param codiceVoceTitolario
	 *            codice voce titolario
	 * @param descrizioneVoceTitolario
	 *            descrizione voce titolario
	 * @param descrizioneTipologia
	 *            descrizione tipologia
	 * @param codiceTipologia
	 *            codice tipologia
	 * @param risposta
	 *            risposta
	 * @param oggettoRisposta
	 *            oggetto risposta
	 * @param dataRegistrazioneRisposta
	 *            data registrazione risposta
	 * @param tipoProtocolloRisposta
	 *            tipo protocollo risposta
	 * @param idProtocolloRisposta
	 *            identificativo protocollo risposta
	 * @param numeroRegistrazioneRisposta
	 *            numero registrazione risposta
	 * @param mittente
	 *            mittente
	 * @param metadatiAssociati
	 * @return protocollo uscita
	 */
	private static ProtocolloNpsDTO creaProtocolloUscitaPf(final Integer idAoo, final Date dataRegistrazione, final Integer numeroRegistrazione, final String codiceRegistro,
			final String denominazioneAmministrazione, final String denominazioneRegistro, final String denominazioneAOO, final String codiceAmministrazione,
			final String codiceAoo, final ProtTipoProtocolloType tipoProtocollo, final ProtAllaccioProtocolloType[] cAllacciati, final ProtDestinatarioType[] cDestinatari,
			final String acl, final OrgOrganigrammaType protocollatoreOrg, final Boolean isRiservato, final String oggetto, final String stato, final String idProtocollo,
			final String codiceVoceTitolario, final String descrizioneVoceTitolario, final String descrizioneTipologia, final String codiceTipologia, final boolean risposta,
			final String oggettoRisposta, final String dataRegistrazioneRisposta, final ProtTipoProtocolloType tipoProtocolloRisposta, final String idProtocolloRisposta,
			final String numeroRegistrazioneRisposta, final ProtMittenteType mittente, final ProtMetadatiType metadatiAssociati) {

		ProtocolloNpsDTO protocollo = null;

		initBusinessDelegate();
		
		final RispostaCreateProtocolloUscitaType rispostaProtocollo = BusinessDelegate.Protocollo.createUscitaPF(idAoo, dataRegistrazione, numeroRegistrazione,
				codiceRegistro, denominazioneAmministrazione, denominazioneRegistro, codiceAmministrazione, denominazioneAOO, codiceAoo, tipoProtocollo, cAllacciati,
				cDestinatari, idProtocollo, isRiservato, stato, oggetto, acl, protocollatoreOrg, codiceVoceTitolario, descrizioneVoceTitolario, descrizioneTipologia,
				codiceTipologia, risposta, oggettoRisposta, dataRegistrazioneRisposta, tipoProtocolloRisposta, idProtocolloRisposta, numeroRegistrazioneRisposta, mittente,
				metadatiAssociati);

		if (rispostaProtocollo != null && rispostaProtocollo.getEsito() != null && EsitoType.OK.equals(rispostaProtocollo.getEsito())) {
			protocollo = new ProtocolloNpsDTO();

			final ProtProtocolloDatiMinimiType datiProtocollo = rispostaProtocollo.getDatiProtocollo();
			// Descrizione tipologia documento
			final ProtTipologiaDocumentoType tipDoc = datiProtocollo.getTipologiaDocumento();
			if (tipDoc != null && tipDoc.getCodice() != null) {
				final String descrizioneTipologiaDocumento = tipDoc.getCodice();
				protocollo.setDescrizioneTipologiaDocumento(descrizioneTipologiaDocumento);
			}

			final Date dataProtocollazione = datiProtocollo.getDataRegistrazione().toGregorianCalendar().getTime();
			protocollo.setDataProtocollo(dataProtocollazione);
			final Calendar cal = Calendar.getInstance();
			cal.setTime(dataProtocollazione);
			final int annoProtocollazione = cal.get(Calendar.YEAR);
			protocollo.setAnnoProtocollo(String.valueOf(annoProtocollazione));
			protocollo.setIdProtocollo(datiProtocollo.getIdProtocollo());
			protocollo.setNumeroProtocollo(Integer.parseInt(datiProtocollo.getNumeroRegistrazione()));
			protocollo.setOggetto(datiProtocollo.getOggetto());
			if (datiProtocollo.getTipologiaDocumento() != null) {
				protocollo.setDescrizioneTipologiaDocumento(datiProtocollo.getTipologiaDocumento().getDescrizione().getValue());
			}
			if (datiProtocollo.getRegistro() != null) {
				protocollo.setCodiceRegistro(datiProtocollo.getRegistro().getCodice().getCodice());
			}

		} else {

			logErrorResponse(rispostaProtocollo, "creazione del protocollo in uscita");

		}

		return protocollo;
	}

	/**
	 * Metodo per creazione oggetto organigramma.
	 * 
	 * @param utente
	 *            dati utente
	 * @param connection
	 *            connessione db
	 * @return oggetto organigramma
	 */
	private OrgOrganigrammaType getOrganigrammaType(final UtenteDTO utente, final Connection connection) {
		final NpsConfiguration npsConf = npsConfigurationDAO.getByIdAoo(utente.getIdAoo().intValue(), connection);
		return Builder.buildOrganigrammaPf(String.valueOf(utente.getId()), utente.getCognome(), utente.getNome(), utente.getCodFiscale(), npsConf.getCodiceAmministrazione(),
				npsConf.getDenominazioneAmministrazione(), npsConf.getCodiceAoo(), npsConf.getDenominazioneAoo(), utente.getCodiceUfficio(), utente.getNodoDesc(),
				String.valueOf(utente.getIdUfficio()));
	}

	/**
	 * aggiungi allacci.
	 *
	 * @param utente
	 *            the utente
	 * @param connection
	 *            the connection
	 * @param idProtocollo
	 *            the id protocollo
	 * @param allacci
	 *            the allacci
	 * @param numeroAnnoProtocollo
	 *            the numero anno protocollo
	 * @param isRisposta
	 *            the is risposta
	 */
	@Override
	public final void aggiungiAllacciAsync(final UtenteDTO utente, final Connection connection,	final String idProtocollo, final Collection<RispostaAllaccioDTO> allacci, 
			final String numeroAnnoProtocollo, final boolean isRisposta, final String idDocumento) {
		IFilenetCEHelper fcehAdmin = null;

		try {
			if (allacci != null && !allacci.isEmpty()) {
				final FilenetCredentialsDTO fcAdmin = new FilenetCredentialsDTO(utente.getFcDTO());
				fcehAdmin = FilenetCEHelperProxy.newInstance(fcAdmin, utente.getIdAoo());

				final Collection<RichiestaAggiungiAllaccioProtocolloType.Allaccio> allacciNps = new ArrayList<>();
				final List<String> props = Arrays.asList(pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY));
				Document documentAllaccio;
				String idProtocolloAllaccio;
				String statoNPS;

				for (final RispostaAllaccioDTO allaccio : allacci) {
					// Recupero l'ID protocollo del documento allacciato
					documentAllaccio = fcehAdmin.getDocumentByIdGestionale(allaccio.getIdDocumentoAllacciato(), props, null, utente.getIdAoo().intValue(), null, null);
					idProtocolloAllaccio = (String) TrasformerCE.getMetadato(documentAllaccio, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY);
					if (!StringUtils.isNullOrEmpty(idProtocolloAllaccio)) { // Si inviano a NPS solo gli allacci con protocollo
						allaccio.setIdProtocollo(idProtocolloAllaccio);

						statoNPS = (allaccio.getDocumentoDaChiudere() != null && allaccio.getDocumentoDaChiudere()) ? Builder.CHIUSO : null;
						allacciNps.add(Builder.buildAllaccio(isRisposta, statoNPS, allaccio.getIdProtocollo()));
					}
				}

				final OrgOrganigrammaType operatore = getOrganigrammaType(utente, connection);
				final RichiestaAggiungiAllaccioProtocolloType input = Director.constructAggiungiAllaccioProtocolloInput(operatore, idProtocollo,
						allacciNps.toArray(new RichiestaAggiungiAllaccioProtocolloType.Allaccio[] {}));

				final int partition = npsAsyncRequestDAO.getPartition(utente.getIdAoo().intValue(), connection);
				final int priority = 1;
				final int stato = 1;

				final AsyncRequestDTO<RichiestaAggiungiAllaccioProtocolloType> asyncRequestDTO = new AsyncRequestDTO<>();
				asyncRequestDTO.setInput(input);
				asyncRequestDTO.setXml(input);
				asyncRequestDTO.setDataCreazione(new Date());
				asyncRequestDTO.setPartition(partition);
				asyncRequestDTO.setPriority(priority);
				asyncRequestDTO.setStato(stato);
				asyncRequestDTO.setClassName(BusinessDelegate.Allaccio.class.getName());
				asyncRequestDTO.setMethodName("aggiungiAllacci");
				asyncRequestDTO.setId_nar_execute_required(0);
				if (!protocolloEmergenzaDocSRV.isEmergenzaNotUpdated(idProtocollo, connection)) {
					asyncRequestDTO.setNumeroAnnoProtocollo(numeroAnnoProtocollo);
				} else {
					asyncRequestDTO.setNumeroAnnoProtocolloEmergenza(numeroAnnoProtocollo);
				}
				asyncRequestDTO.setClassNameInput(RichiestaAggiungiAllaccioProtocolloType.class.getName());
				asyncRequestDTO.setIdDocumento(idDocumento);
				npsAsyncRequestDAO.insert(asyncRequestDTO, connection);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'aggiunta degli allacci", e);
			// RED non lancia eccezione vedi saveAsyncReq_AggiungiAllacci
			// NPSProxyAsyncService
		} finally {
			popSubject(fcehAdmin);
		}
	}

	/**
	 * @see it.ibm.red.business.service.INpsSRV#creaProtocolloEntrata(it.ibm.red.business.dto.SalvaDocumentoRedDTO,
	 *      java.lang.String, it.ibm.red.business.dto.UtenteDTO, java.lang.String[],
	 *      java.lang.String[], java.lang.String[], java.lang.String[],
	 *      java.lang.String[], java.lang.String[], java.lang.String[], boolean,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public ProtocolloNpsDTO creaProtocolloEntrata(final SalvaDocumentoRedDTO docInput, final String acl, final UtenteDTO utente, final String[] assegnazioniCompetenza,
			final String[] assegnazioniConoscenza, final String[] assegnazioniContributo, final String[] assegnazioniFirma, final String[] assegnazioniSigla,
			final String[] assegnazioniVisto, final String[] assegnazioniFirmaMultipla, final boolean controlloIter, final String idDocumentoMessaggioPostaNps,
			final Connection connection) {
		LOGGER.info("creaProtocolloEntrata -> START. Documento: " + docInput.getDocumentTitle());

		try {

			if (!StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED)) && "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[creaProtocolloEntrata] Non è possibile comunicare con NPS in regime mock");

				final ProtocolloDTO protocollo = mockSRV.getNewProtocollo(utente.getCodiceAoo(), TipoProtocolloEnum.ENTRATA);
				return trasform(protocollo);
			}

			// Entrata: assegnante = protocollatore | destinatario/assegnatario = competente
			// | mittente = mittente red
			final String tipo = Builder.SMTP;
			ProtocolloNpsDTO protocollo = null;

			// ### PROTOCOLLATORE
			final OrgOrganigrammaType protocollatoreOrg = getOrganigrammaType(utente, connection);

			// ### DESTINATARIO E ASSEGNATARI
			final List<ProtDestinatarioType> destinatariNps = new ArrayList<>();
			final List<ProtAssegnatarioType> assegnatariNps = new ArrayList<>();
			final Date currentDate = new Date();
			if (assegnazioniCompetenza != null && assegnazioniCompetenza.length > 0) {
				boolean isDestinatarioCompetenza = false;
				for (int index = 0; index < assegnazioniCompetenza.length; index++) {
					// Il destinatario è sempre il primo assegnatario per competenza (obbligatorio)
					if (index == 0) {
						isDestinatarioCompetenza = true;
					}
					aggiungiAssegnatario(assegnatariNps, destinatariNps, assegnazioniCompetenza[index], protocollatoreOrg, currentDate, isDestinatarioCompetenza, connection);
				}
			}

			if (assegnazioniConoscenza != null && assegnazioniConoscenza.length > 0) {
				aggiungiAssegnatari(assegnatariNps, destinatariNps, assegnazioniConoscenza, protocollatoreOrg, currentDate, false, connection);
			}

			if (assegnazioniContributo != null && assegnazioniContributo.length > 0) {
				aggiungiAssegnatari(assegnatariNps, destinatariNps, assegnazioniContributo, protocollatoreOrg, currentDate, false, connection);
			}

			if (assegnazioniFirma != null && assegnazioniFirma.length > 0) {
				aggiungiAssegnatari(assegnatariNps, destinatariNps, assegnazioniFirma, protocollatoreOrg, currentDate, false, connection);
			}

			if (assegnazioniSigla != null && assegnazioniSigla.length > 0) {
				aggiungiAssegnatari(assegnatariNps, destinatariNps, assegnazioniSigla, protocollatoreOrg, currentDate, false, connection);
			}

			if (assegnazioniVisto != null && assegnazioniVisto.length > 0) {
				aggiungiAssegnatari(assegnatariNps, destinatariNps, assegnazioniVisto, protocollatoreOrg, currentDate, false, connection);
			}

			if (assegnazioniFirmaMultipla != null && assegnazioniFirmaMultipla.length > 0) {
				aggiungiAssegnatari(assegnatariNps, destinatariNps, assegnazioniFirmaMultipla, protocollatoreOrg, currentDate, false, connection);
			}

			if (controlloIter && utente != null && (assegnazioniCompetenza.length == 0 && assegnazioniConoscenza.length == 0 && assegnazioniContributo.length == 0
					&& assegnazioniFirma.length == 0 && assegnazioniSigla.length == 0 && assegnazioniVisto.length == 0 && assegnazioniFirmaMultipla.length == 0)) {
				final Nodo nodoUtente = nodoDAO.getNodo(utente.getIdUfficio(), connection);
				final UtenteDTO dirigenteUfficioUtente = new UtenteDTO(utenteDAO.getUtente(nodoUtente.getDirigente().getIdUtente(), connection));

				if (dirigenteUfficioUtente != null) {
					final String assegnazioneDirigente = dirigenteUfficioUtente.getIdUfficio() + "," + dirigenteUfficioUtente.getId();
					aggiungiAssegnatario(assegnatariNps, destinatariNps, assegnazioneDirigente, protocollatoreOrg, currentDate, false, connection);
				}
			}

			final ProtAssegnatarioType[] assegnatariNpsArray = new ProtAssegnatarioType[assegnatariNps.size()];
			assegnatariNps.toArray(assegnatariNpsArray);
			final ProtDestinatarioType[] destinatariNpsArray = new ProtDestinatarioType[destinatariNps.size()];
			destinatariNps.toArray(destinatariNpsArray);

			// ### DOCUMENTI ALLACCIATI (ALLACCI)
			ProtAllaccioProtocolloType[] allacciNps = null;
			if (docInput.getIdProtocolloRisposta() != null) {
				allacciNps = new ProtAllaccioProtocolloType[1];
				final ProtAllaccioProtocolloType allaccioNps = Builder.buildRisposta(ProtTipoProtocolloType.USCITA, false, null,
						Constants.EMPTY_STRING + docInput.getNumeroProtocolloRisposta(), null, docInput.getDataProtocolloRisposta());

				allacciNps[0] = allaccioNps;
			}

			// ### MEZZI RICEZIONE (CARTACEO - ELETTRONICO - PEC - PEO - ALTRO)
			String mezzoRicezione = null;
			if (docInput.getMezzoRicezione() != null) {
				final String mezzoRicezioneInputDescrizione = docInput.getMezzoRicezione().getDescrizione();
				if ("Posta elettronica da PEC".equalsIgnoreCase(mezzoRicezioneInputDescrizione)) {
					mezzoRicezione = "PEC";
				} else if ("Posta elettronica da PEO".equalsIgnoreCase(mezzoRicezioneInputDescrizione)) {
					mezzoRicezione = "PEO";
				} else {
					mezzoRicezione = CARTACEO;
				}
			} else {
				mezzoRicezione = CARTACEO;
			}

			// ### MITTENTE
			ProtMittenteType mittenteNps = null;
			final Contatto mittente = docInput.getMittenteContatto();
			if (mittente != null) {
				final String mail = !StringUtils.isNullOrEmpty(mittente.getMailPec()) ? mittente.getMailPec() : mittente.getMail();
				if ("F".equals(mittente.getTipoPersona())) {
					mittenteNps = Builder.buildMittenteEntrataPF(mezzoRicezione, mezzoRicezione, tipo, CARTACEO, mittente.getNome(), mittente.getCognome(),
							mittente.getCodiceFiscalePIva());
				} else {
					String denominazione = mittente.getNome();
					if (StringUtils.isNullOrEmpty(denominazione)) {
						denominazione = mittente.getAliasContatto();
					}

					mittenteNps = Builder.buildMittentePG(mezzoRicezione, mezzoRicezione, tipo, mail, denominazione);
				}
			}

			// ### CREAZIONE
			final NpsConfiguration npsConfUtente = npsConfigurationDAO.getByIdAoo(utente.getIdAoo().intValue(), connection);

			String indiceFascicoloNps = Constants.EMPTY_STRING;
			String descrIndiceFascicoloNps = Constants.EMPTY_STRING;
			if (docInput.getIndiceClassificazioneFascicoloProcedimentale() != null) {
				indiceFascicoloNps = docInput.getIndiceClassificazioneFascicoloProcedimentale();
				descrIndiceFascicoloNps = indiceFascicoloNps + " - " + docInput.getDescrizioneIndiceClassificazioneFascicoloProcedimentale();
			}

			final String tipologiaDocumento = getTipologiaDocumentoNPS(docInput.getTipologiaDocumento().getIdTipologiaDocumento(),
					(int) docInput.getTipoProcedimento().getTipoProcedimentoId(), docInput.getTipologiaDocumento().getDescrizione(), utente.getIdAoo().intValue(), connection);

			final ProtMetadatiType metadatiAssociati = createMetadatiAssociati(docInput.getMetadatiEstesi());
			
			final boolean isRiservato = docInput.getIsRiservato() != null && docInput.getIsRiservato();

			protocollo = creaProtocolloEntrataPf(utente.getIdAoo().intValue(), /* dataRegistrazione */ null, /* numeroRegistrazione */ null,
					/* codiceRegistro in tabella registro in chiave con la tabella aoo */ npsConfUtente.getRegistroUfficiale(),
					/* denominazioneAmministrazione */ npsConfUtente.getDenominazioneAmministrazione(),
					/* denominazioneRegistro */ npsConfUtente.getDenominazioneRegistro(),
					/* denominazioneAOO */ npsConfUtente.getDenominazioneAoo(),
					/* tipoProtocollo */ ProtTipoProtocolloType.ENTRATA, /* cAllacciati */ allacciNps,
					/* cAssegnatari */ assegnatariNpsArray, /* cDestinatari */ destinatariNpsArray, acl,
					/* OperatoreDTO */ protocollatoreOrg, /* isRiservato */ isRiservato, /* oggetto */ docInput.getOggetto(),
					/* stato */ Builder.IN_LAVORAZIONE, /* idProtocollo */ null,
					/* codiceVoceTitolario */ indiceFascicoloNps,
					/* descrizioneVoceTitolario */ descrIndiceFascicoloNps,
					/* descrizioneTipologia */ tipologiaDocumento,
					/* codiceTipologia */ tipologiaDocumento,
					npsConfUtente.getCodiceAmministrazione(), /* note */ null, tipo, npsConfUtente.getCodiceAoo(), /* value */ null, 
					mittenteNps, metadatiAssociati, idDocumentoMessaggioPostaNps);

			if (protocollo == null || protocollo.getNumeroProtocollo() == 0 || StringUtils.isNullOrEmpty(protocollo.getIdProtocollo())
					|| StringUtils.isNullOrEmpty(protocollo.getAnnoProtocollo()) || "null".equalsIgnoreCase(protocollo.getAnnoProtocollo())) {
				LOGGER.error("Errore durante la creazione del protocollo in entrata da NPS. Documento: " + docInput.getDocumentTitle());
			}

			LOGGER.info("creaProtocolloEntrata -> END. Documento: " + docInput.getDocumentTitle());
			return protocollo;

		} catch (final DatatypeConfigurationException e) {
			LOGGER.error("Errore in fase di parsing di una data", e);
			throw new RedException(e);
		}
	}

	/**
	 * Crea protocollo entrata.
	 * 
	 * @param idAoo
	 *            identificativo area organizzativa
	 * @param dataRegistrazione
	 *            data registrazione
	 * @param numeroRegistrazione
	 *            numero registazione
	 * @param codiceRegistro
	 *            codice registro
	 * @param denominazioneAmministrazione
	 *            denominazione amministrazione
	 * @param denominazioneRegistro
	 *            denominazione registro
	 * @param denominazioneAOO
	 *            denominazione area organizzativa
	 * @param tipoProtocollo
	 *            tipo protocollo
	 * @param cAllacciati
	 *            documenti allacciati
	 * @param cAssegnatari
	 *            assegnatari protocollo
	 * @param cDestinatari
	 *            destinatari protocollo
	 * @param acl
	 *            acl protocollo
	 * @param protocollatoreOrg
	 *            protocollo ORG
	 * @param isRiservato
	 *            flag riservato
	 * @param oggetto
	 *            oggetto protocollo
	 * @param stato
	 *            stato protocollo
	 * @param idProtocollo
	 *            identificativo protocollo
	 * @param codiceVoceTitolario
	 *            codice della voce titolario
	 * @param descrizioneVoceTitolario
	 *            descrizione voce titolario
	 * @param descrizioneTipologia
	 *            descrizione tipologia
	 * @param codiceTipologia
	 *            codice tipologia
	 * @param codiceAmministrazione
	 *            codice amministrazione
	 * @param note
	 *            note protocollo
	 * @param tipo
	 *            tipo protocollo
	 * @param codiceAOO
	 *            codice area organizzativa
	 * @param value
	 *            valore
	 * @param mittente
	 *            mittente protocollo
	 * @param metadatiAssociati
	 *            metadati associati del protocollo
	 * @param idDocumentoMessaggioPostaNps
	 *            identificativi del messaggio posta NPS
	 * @return protocollo nps
	 */
	private static ProtocolloNpsDTO creaProtocolloEntrataPf(final Integer idAoo, final Date dataRegistrazione, final Integer numeroRegistrazione, final String codiceRegistro,
			final String denominazioneAmministrazione, final String denominazioneRegistro, final String denominazioneAOO, final ProtTipoProtocolloType tipoProtocollo,
			final ProtAllaccioProtocolloType[] cAllacciati, final ProtAssegnatarioType[] cAssegnatari, final ProtDestinatarioType[] cDestinatari, final String acl,
			final OrgOrganigrammaType protocollatoreOrg, final Boolean isRiservato, final String oggetto, final String stato, final String idProtocollo,
			final String codiceVoceTitolario, final String descrizioneVoceTitolario, final String descrizioneTipologia, final String codiceTipologia,
			final String codiceAmministrazione, final String note, final String tipo, final String codiceAOO, final String value, final ProtMittenteType mittente,
			final ProtMetadatiType metadatiAssociati, final String idDocumentoMessaggioPostaNps) {
		LOGGER.info("creaProtocolloEntrataPf -> START");
		ProtocolloNpsDTO protocolloNps = null;

		initBusinessDelegate();
		
		final RispostaCreateProtocolloEntrataType rispostaProtocollo = BusinessDelegate.Protocollo.createEntrataPF(idAoo, dataRegistrazione, // Date dataRegistrazione
				numeroRegistrazione, // Integer numeroRegistrazione
				codiceRegistro, // in tabella registro in chiave con la tabella aoo
				denominazioneAmministrazione, // denominazioneAmministrazione
				denominazioneRegistro, // denominazioneRegistro
				denominazioneAOO, // denominazioneAOO
				tipoProtocollo, // tipoProtocollo Entrata
				cAllacciati, // Collection<ProtAssegnatarioType> cAllacciati idallacciati
				cAssegnatari, // cAssegnatari
				cDestinatari, // cDestinatari
				acl, // acl
				protocollatoreOrg, // OperatoreDTO protocollatore
				isRiservato, // isRiservato
				oggetto, // oggetto
				stato, // stato Prot_protocolloBaseRequestTypeStato IN LAVORAZIONE
				idProtocollo, // idProtocollo
				codiceVoceTitolario, // codiceVoceTitolario
				descrizioneVoceTitolario, // descrizioneVoceTitolario
				descrizioneTipologia, // descrizioneTipologia
				codiceTipologia, // codiceTipologia
				codiceAmministrazione, // codiceAmministrazione
				note, // note verificare
				tipo, // tipo indice telematico mail contatto
				codiceAOO, // codiceAoo
				value, // value
				mittente /* PersonaFisicaDTO pf */, metadatiAssociati, // ProtMetadatoAssociatoType
				idDocumentoMessaggioPostaNps); // ID Documento del messaggio di posta NPS che si sta protocollando

		if (rispostaProtocollo != null && rispostaProtocollo.getEsito() != null && EsitoType.OK.equals(rispostaProtocollo.getEsito())) {
			protocolloNps = new ProtocolloNpsDTO();
			final Date dataProtocollazione = rispostaProtocollo.getDatiProtocollo().getDataRegistrazione().toGregorianCalendar().getTime();
			protocolloNps.setDataProtocollo(dataProtocollazione);
			final Calendar cal = Calendar.getInstance();
			cal.setTime(dataProtocollazione);
			final int annoProtocollazione = cal.get(Calendar.YEAR);
			protocolloNps.setAnnoProtocollo(String.valueOf(annoProtocollazione));
			protocolloNps.setIdProtocollo(rispostaProtocollo.getDatiProtocollo().getIdProtocollo());
			protocolloNps.setNumeroProtocollo(Integer.parseInt(rispostaProtocollo.getDatiProtocollo().getNumeroRegistrazione()));
			protocolloNps.setOggetto(rispostaProtocollo.getDatiProtocollo().getOggetto());
			if (rispostaProtocollo.getDatiProtocollo().getTipologiaDocumento() != null) {
				protocolloNps.setDescrizioneTipologiaDocumento(rispostaProtocollo.getDatiProtocollo().getTipologiaDocumento().getCodice());
			}
		} else {

			logErrorResponse(rispostaProtocollo, "creazione del protocollo in entrata");

		}

		LOGGER.info("creaProtocolloEntrataPf -> END");
		return protocolloNps;
	}

	private void aggiungiAssegnatari(final List<ProtAssegnatarioType> assegnazioniNps, final List<ProtDestinatarioType> destinatariNps, final String[] assegnazioni,
			final OrgOrganigrammaType protocollatoreOrg, final Date currentDate, final boolean isDestinatarioCompetenza, final Connection con)
			throws DatatypeConfigurationException {
		if (assegnazioni != null && assegnazioni.length > 0) {
			for (final String assegnazione : assegnazioni) {
				aggiungiAssegnatario(assegnazioniNps, destinatariNps, assegnazione, protocollatoreOrg, currentDate, isDestinatarioCompetenza, con);
			}
		}
	}

	private void aggiungiAssegnatario(final List<ProtAssegnatarioType> assegnazioniNps, final List<ProtDestinatarioType> destinatariNps, final String assegnazione,
			final OrgOrganigrammaType protocollatoreOrg, final Date currentDate, final boolean isDestinatarioCompetenza, final Connection con)
			throws DatatypeConfigurationException {
		// Si crea l'assegnatario
		final OrgOrganigrammaType assegnatarioOrg = creaOperatoreNps(assegnazione, destinatariNps, isDestinatarioCompetenza, con);

		// Si crea l'assegnazione
		final ProtAssegnatarioType assegnazioneNps = Builder.buildProtAssegnatarioType(assegnatarioOrg, protocollatoreOrg, currentDate, isDestinatarioCompetenza);

		// Si inserisce nella lista di assegnazioni NPS
		assegnazioniNps.add(assegnazioneNps);
	}

	private OrgOrganigrammaType creaOperatoreNps(final String assegnazione, final List<ProtDestinatarioType> destinatariNps, final boolean isDestinatarioCompetenza,
			final Connection connection) {
		Long idUfficio;
		Long idUtente;
		final String[] assegnazioneSplit = assegnazione.split(",");
		idUfficio = Long.parseLong(assegnazioneSplit[0]);
		idUtente = Long.parseLong(assegnazioneSplit[1]);

		return creaOperatoreNps(idUfficio, idUtente, destinatariNps, isDestinatarioCompetenza, connection);
	}

	private OrgOrganigrammaType creaOperatoreNps(final Long idUfficio, final Long idUtente, final List<ProtDestinatarioType> destinatariNps,
			final boolean isDestinatarioCompetenza, final Connection connection) {
		OrgOrganigrammaType operatoreOrg = null;

		if (idUtente > 0) { // Utente
			final NodoUtenteRuolo nur = utenteDAO.getUtenteUfficioByIdUtenteAndIdNodo(idUtente, idUfficio, connection);
			final NpsConfiguration npsConfDestinatario = npsConfigurationDAO.getByIdAoo(nur.getNodo().getAoo().getIdAoo().intValue(), connection);
			final String uNome = nur.getUtente().getNome();
			final String uCognome = nur.getUtente().getCognome();
			final String uCF = nur.getUtente().getCodiceFiscale();
			final String uCodiceAmm = npsConfDestinatario.getCodiceAmministrazione();
			final String uDenominazioneAmm = npsConfDestinatario.getDenominazioneAmministrazione();
			final String uCodiceAoo = npsConfDestinatario.getCodiceAoo();
			final String uDenominazioneAoo = npsConfDestinatario.getDenominazioneAoo();
			final String uIdNodo = String.valueOf(nur.getNodo().getIdNodo());
			final String uNodoDesc = nur.getNodo().getDescrizione();
			final String uCodiceNodo = nur.getNodo().getCodiceNodo();

			operatoreOrg = Builder.buildOrganigrammaPf(String.valueOf(idUtente), uCognome, uNome, uCF, uCodiceAmm, uDenominazioneAmm, uCodiceAoo, uDenominazioneAoo,
					uCodiceNodo, uNodoDesc, uIdNodo);

			if (destinatariNps != null && isDestinatarioCompetenza) {
				final String uEmail = nur.getUtente().getEmail();
				final ProtDestinatarioType destinatario = Builder.buildDestinatarioPF(Builder.NO, uNome, uCognome, uCF, Builder.SMTP, uEmail);
				destinatariNps.add(destinatario);
			}
		} else { // Ufficio
			final Nodo nodo = nodoDAO.getNodo(idUfficio, connection);
			final NpsConfiguration npsConfDestinatario = npsConfigurationDAO.getByIdAoo(nodo.getAoo().getIdAoo().intValue(), connection);
			final String uCodiceAmm = npsConfDestinatario.getCodiceAmministrazione();
			final String uDenominazioneAmm = npsConfDestinatario.getDenominazioneAmministrazione();
			final String uCodiceAoo = npsConfDestinatario.getCodiceAoo();
			final String uDenominazioneAoo = npsConfDestinatario.getDenominazioneAoo();
			final String uNodoDesc = nodo.getDescrizione();
			final String uCodiceNodo = nodo.getCodiceNodo();

			operatoreOrg = Builder.buildOrganigrammaEnte(uCodiceAmm, uDenominazioneAmm, uCodiceAoo, uDenominazioneAoo, uCodiceNodo, uNodoDesc, String.valueOf(idUfficio));

			if (destinatariNps != null && isDestinatarioCompetenza) {
				final ProtDestinatarioType destinatario = Builder.buildDestinatarioEnte(uCodiceAmm, uCodiceAoo, uCodiceNodo, uNodoDesc);
				destinatariNps.add(destinatario);
			}
		}

		return operatoreOrg;
	}

	/**
	 * @see it.ibm.red.business.service.INpsSRV#getACLFromSecurity(it.ibm.red.business.dto.ListSecurityDTO,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public String getACLFromSecurity(final ListSecurityDTO securityList, final IFilenetCEHelper fceh) {
		LOGGER.info("getACLFromSecurity -> START");
		final StringBuilder acl = new StringBuilder("");

		if (securityList != null && !securityList.isEmpty()) {
			try {
				for (final SecurityDTO security : securityList) {
					acl.append(fceh.getADGroupNameFormId(String.valueOf(security.getGruppo().getIdUfficio())) + ",");
				}
				if (acl.length() > 0) {
					acl.setLength(acl.length() - 1);
					LOGGER.info("getACLFromSecurity -> ACL recuperate.");
				}
			} catch (final Exception e) {
				LOGGER.error("Errore durante il recupero delle ACL.", e);
			}
		}

		LOGGER.info("getACLFromSecurity -> END");
		return acl.toString();
	}

	/**
	 * Invia i content del documento principale e degli allegati ad NPS.
	 * 
	 * @param guid
	 * @param mimeType
	 * @param fileName
	 * @param oggetto
	 * @param idProtocollo
	 * @param infoProtocollo
	 * @param utente
	 * @param is
	 * @param fceh
	 * @param allegati
	 */
	private void uploadContentToNps(final String guid, final String mimeType, final String fileName, final String oggetto, final String idProtocollo,
			final String infoProtocollo, final UtenteDTO utente, final InputStream is, final IFilenetCEHelper fceh, final boolean allegati, final String idDocumento) {

		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), true);

			final int idNar = uploadDocToAsyncNps(is, guid, idProtocollo, mimeType, fileName, oggetto, infoProtocollo, utente, idDocumento, connection);

			associateDocumentoProtocolloToAsyncNps(idNar, guid, idProtocollo, fileName, oggetto, utente, true, true, guid, null, infoProtocollo, idDocumento, connection);

			if (allegati) {
				uploadAllegatiDocToAsyncNps(guid, infoProtocollo, idProtocollo, utente, false, fceh, connection);
			}

			commitConnection(connection);

		} catch (final Exception e) {
			LOGGER.error("Errore nell'invio a NPS del guid " + guid, e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.INpsFacadeSRV#uploadContentToAsyncNps(com.filenet.api.core.Document,
	 *      java.lang.Integer,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper, boolean,
	 *      it.ibm.red.business.persistence.model.Aoo, java.lang.String).
	 */
	@Override
	public void uploadContentToAsyncNps(final Document documento, final Integer idNodoDestinatario, final IFilenetCEHelper fceh, final boolean allegati, final Aoo aoo,
			final String idDocumento) {

		final String guid = documento.get_Id().toString();
		final String mimeType = documento.get_MimeType();
		final String fileName = documento.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY));
		final String oggetto = documento.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY));

		final String idProtocollo = documento.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY));
		String infoProtocollo = null;
		if (idProtocollo != null) {
			infoProtocollo = documento.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY)) + "/"
					+ documento.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY)) + "_" + aoo.getCodiceAoo();

			final UtenteDTO utente = new UtenteDTO();
			utente.setIdUfficio((long) idNodoDestinatario);
			utente.setId((long) 0);
			utente.setIdAoo(aoo.getIdAoo());

			final InputStream is = ((ContentTransfer) documento.get_ContentElements().get(0)).accessContentStream();

			uploadContentToNps(guid, mimeType, fileName, oggetto, idProtocollo, infoProtocollo, utente, is, fceh, allegati, idDocumento);
		}

	}

	/**
	 * Esegue una doppia richiesta asincrona, collegata l'una all'altra: un
	 * aggiornamento del protocollo a COMPLETO e una richiesta di spedizione che,
	 * oltre a spedire le comunicazioni verso i destinatari elettronici, aggiorna lo
	 * stato del protocollo a "spedito" e la relativa data di spedizione.
	 * 
	 * @param utente
	 * @param idProtocollo
	 * @param infoProtocollo
	 * @param dataSpedizione
	 * @param oggettoMessaggio
	 * @param corpoMessaggio
	 * @param idDocumento
	 * @param connection
	 * @return
	 */
	@Override
	public int spedisciProtocolloUscitaAsync(final UtenteDTO utente, final String infoProtocollo, final String idProtocollo, final String dataSpedizione,
			final String oggettoMessaggio, final String corpoMessaggio, final String idDocumento, final Connection connection) {

		try {
			final NpsConfiguration npsConfMittente = npsConfigurationDAO.getByIdAoo(utente.getIdAoo().intValue(), connection);

			final OrgOrganigrammaType operatore = Builder.buildOrganigrammaPf("" + utente.getId(), utente.getCognome(), utente.getNome(), utente.getCodFiscale(),
					npsConfMittente.getCodiceAmministrazione(), npsConfMittente.getDenominazioneAmministrazione(), npsConfMittente.getCodiceAoo(),
					npsConfMittente.getDenominazioneAoo(), utente.getCodiceUfficio(), utente.getNodoDesc(), "" + utente.getIdUfficio());

			final int partition = npsAsyncRequestDAO.getPartition(utente.getIdAoo().intValue(), connection);

			final boolean isEmergenzaNotUpdated = protocolloEmergenzaDocSRV.isEmergenzaNotUpdated(idProtocollo, connection);

			final int idNarExecuteRequired = aggiornaProtocolloCompletoAsync(partition, isEmergenzaNotUpdated, operatore, infoProtocollo, idProtocollo, idDocumento,
					connection);

			final RichiestaSpedisciProtocolloUscitaType input = Director.constructSpedisciProtocolloUscita(operatore, idProtocollo, dataSpedizione, oggettoMessaggio,
					corpoMessaggio);

			final AsyncRequestDTO request = costructRequest(idNarExecuteRequired, input, new Date(), partition, 1, 1, SPEDISCI_PROTOCOLLO_USCITA_METHOD_NAME, infoProtocollo,
					BusinessDelegate.Protocollo.class, isEmergenzaNotUpdated, idDocumento);

			return insertRequest(request, connection);

		} catch (final Exception e) {
			LOGGER.error("Errore durante la spedizione protocollo uscita asincrona su NPS del documento con id protocollo: " + idProtocollo, e);
			throw new RedException(e);
		}

	}

	/**
	 * @see it.ibm.red.business.service.facade.INpsFacadeSRV#aggiornaDatiProtocollo(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.Integer,
	 *      java.lang.Integer, java.lang.String, java.lang.String, java.lang.String,
	 *      java.util.Collection).
	 */
	@Override
	public boolean aggiornaDatiProtocollo(final UtenteDTO utente, final String infoProtocollo, final String idProtocollo, final String oggetto,
			final Integer idTipologiaDocumento, final Integer idTipoProcedimento, final String descrizioneTipologiaDocumento, final String indiceClassificazione,
			final String idDocumento, final Collection<MetadatoDTO> metadatiEstesi) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			return aggiornaDatiProtocollo(utente, infoProtocollo, idProtocollo, oggetto, idTipologiaDocumento, idTipoProcedimento, descrizioneTipologiaDocumento,
					indiceClassificazione, idDocumento, metadatiEstesi, connection);

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di aggiornamento dati protocollo su NPS del documento " + idDocumento, e);
			throw new RedException("Errore in fase di aggiornamento dati protocollo su NPS del documento " + idDocumento, e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.INpsSRV#aggiornaDatiProtocollo(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.Integer,
	 *      java.lang.Integer, java.lang.String, java.lang.String, java.lang.String,
	 *      java.util.Collection, java.sql.Connection).
	 */
	@Override
	public boolean aggiornaDatiProtocollo(final UtenteDTO utente, final String infoProtocollo, final String idProtocollo, final String oggetto,
			final Integer idTipologiaDocumento, final Integer idTipoProcedimento, final String descrizioneTipologiaDocumento, final String indiceClassificazione,
			final String idDocumento, final Collection<MetadatoDTO> metadatiEstesi, final Connection con) {
		final String tipoDocumento = getTipologiaDocumentoNPS(idTipologiaDocumento, idTipoProcedimento, descrizioneTipologiaDocumento, utente.getIdAoo().intValue(), con);
		return aggiornaDatiProtocollo(utente, infoProtocollo, idProtocollo, oggetto, tipoDocumento, indiceClassificazione, con, null, null, idDocumento, metadatiEstesi);
	}

	/**
	 * @see it.ibm.red.business.service.INpsSRV#aggiornaTitolarioProtocollo(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean aggiornaTitolarioProtocollo(final UtenteDTO utente, final String infoProtocollo, final String idProtocollo, final String indiceClassificazione,
			final String idDocumento, final Connection con) {
		return aggiornaDatiProtocollo(utente, infoProtocollo, idProtocollo, null, null, indiceClassificazione, con, null, null, idDocumento, null);
	}

	/**
	 * @see it.ibm.red.business.service.INpsSRV#aggiornaAcl(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.String).
	 */
	@Override
	public boolean aggiornaAcl(final UtenteDTO utente, final String infoProtocollo, final String idProtocollo, final String acl, final String idDocumento) {
		Connection conn = null;
		boolean aggiornamentoAcl = false;
		try {
			conn = setupConnection(getDataSource().getConnection(), true);

			aggiornamentoAcl = aggiornaDatiProtocollo(utente, infoProtocollo, idProtocollo, null, null, null, conn, acl, null, idDocumento, null);

			commitConnection(conn);
		} catch (final Exception ex) {
			rollbackConnection(conn);
			LOGGER.error("Errore durante l'aggiornamento delle acl su nps " + idProtocollo, ex);
			throw new RedException(ex);
		} finally {
			closeConnection(conn);
		}

		return aggiornamentoAcl;
	}

	private boolean aggiornaDatiProtocollo(final UtenteDTO utente, final String infoProtocollo, final String idProtocollo, final String oggetto,
			final String descrizioneTipologiaDocumento, final String indiceClassificazione, final Connection con, final String acl, final Boolean isRiservato, 
			final String idDocumento, final Collection<MetadatoDTO> metadati) {
		LOGGER.info("aggiornaDatiProtocollo -> START");
		boolean esitoOk = false;

		ProtMetadatiType metadatiAssociati = null;
		if (!CollectionUtils.isEmpty(metadati)) {

			metadatiAssociati = createMetadatiAssociati(metadati);

		}

		if (TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(utente.getIdTipoProtocollo())) {

			aggiornaDatiProtocolloAsync(utente, infoProtocollo, idProtocollo, oggetto, descrizioneTipologiaDocumento, indiceClassificazione, false, metadatiAssociati, acl,
					isRiservato, idDocumento, con);

		} else {

			// Se siamo mock, non procedere con il colloquio con NPS
			if (!StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED)) && "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[aggiornaDatiProtocollo] Non è possibile comunicare con NPS in regime mock");
				return true;
			}

			final NpsConfiguration npsConfUtente = npsConfigurationDAO.getByIdAoo(utente.getIdAoo().intValue(), con);

			final OrgOrganigrammaType operatore = Builder.buildOrganigrammaPf(String.valueOf(utente.getId()), utente.getCognome(), utente.getNome(), utente.getCodFiscale(),
					npsConfUtente.getCodiceAmministrazione(), npsConfUtente.getDenominazioneAmministrazione(), npsConfUtente.getCodiceAoo(),
					npsConfUtente.getDenominazioneAoo(), utente.getCodiceUfficio(), utente.getNodoDesc(), String.valueOf(utente.getIdUfficio()));

			initBusinessDelegate();
			
			final RispostaUpdateDatiProtocolloType update = BusinessDelegate.Protocollo.updateDatiProtocollo(utente.getIdAoo().intValue(), operatore, idProtocollo,
					oggetto, descrizioneTipologiaDocumento, indiceClassificazione, false, acl, isRiservato, metadatiAssociati);

			if (update != null && EsitoType.OK.equals(update.getEsito())) {
				esitoOk = true;
			} else {
				logErrorResponse(update, "aggiornamento dei dati del protocollo " + idProtocollo);
			}

		}

		LOGGER.info("aggiornaDatiProtocollo -> END");
		return esitoOk;
	}

	private ProtMetadatiType createMetadatiAssociati(final Collection<MetadatoDTO> metadatiEstesi) {

		if (!CollectionUtils.isEmpty(metadatiEstesi)) {
			final List<ProtMetadatoAssociatoType> metadatiNPS = new ArrayList<>();

			ProtMetadatoAssociatoType metadatoNPS = null;

			for (final MetadatoDTO metadato : metadatiEstesi) {
				metadatoNPS = Builder.createProtMetadato(metadato.getName(), metadato.getValue4AttrExt());
				metadatiNPS.add(metadatoNPS);
			}

			return Builder.createProtMetadatiType(metadatiNPS);
		} else {
			return null;
		}
	}

	private boolean aggiornaDatiProtocolloAsync(final UtenteDTO utente, final String infoProtocollo, final String idProtocollo, final String oggetto,
			final String descrizioneTipologiaDocumento, final String indiceClassificazione, final boolean isCompleto, final ProtMetadatiType metadatiAssociati,
			final String acl, final Boolean isRiservato, final String idDocumento, final Connection con) {
		int output = 0;
		final int partition = npsAsyncRequestDAO.getPartition(utente.getIdAoo().intValue(), con);
		final int priority = 1;
		final int stato = 1;

		try {

			final NpsConfiguration npsConfUtente = npsConfigurationDAO.getByIdAoo(utente.getIdAoo().intValue(), con);

			final OrgOrganigrammaType operatore = Builder.buildOrganigrammaPf(String.valueOf(utente.getId()), utente.getCognome(), utente.getNome(), utente.getCodFiscale(),
					npsConfUtente.getCodiceAmministrazione(), npsConfUtente.getDenominazioneAmministrazione(), npsConfUtente.getCodiceAoo(),
					npsConfUtente.getDenominazioneAoo(), utente.getCodiceUfficio(), utente.getNodoDesc(), String.valueOf(utente.getIdUfficio()));

			final RichiestaUpdateDatiProtocolloType input = Director.constructUpdateDatiProtocolloInput(operatore, idProtocollo, oggetto, descrizioneTipologiaDocumento,
					indiceClassificazione, isCompleto, metadatiAssociati, acl, isRiservato, new Date());

			final AsyncRequestDTO asyncRequestDTO = costructRequest(0, input, new Date(), partition, priority, stato, "updateDatiProtocollo", infoProtocollo,
					BusinessDelegate.Protocollo.class, protocolloEmergenzaDocSRV.isEmergenzaNotUpdated(idProtocollo, con), idDocumento);
			output = insertRequest(asyncRequestDTO, con);

		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inserimento su DB della request updateDatiProtocollo", e);
		}
		return output > 0;
	}

	/**
	 * La richiesta verso NPS non è altro che un aggiornaDatiProtocollo. Il
	 * BusinessDelegate espone un'altra firma (updateProtocolloCompleto) per
	 * permettere al nostro metodo service di controllare che siano effettivamente
	 * complete tutte le attività asincrone associate allo stesso protocollo.
	 * 
	 * @param partition
	 * @param isEmergenzaNotUpdated
	 * @param operatore
	 * @param infoProtocollo
	 * @param idProtocollo
	 * @param con
	 * @return
	 */
	private int aggiornaProtocolloCompletoAsync(final int partition, final boolean isEmergenzaNotUpdated, final OrgOrganigrammaType operatore, final String infoProtocollo,
			final String idProtocollo, final String idDocumento, final Connection con) {
		int idNar = 0;
		final int priority = 1;
		final int stato = 1;

		try {
			final RichiestaUpdateDatiProtocolloType input = Director.constructUpdateDatiProtocolloInput(operatore, idProtocollo, null, null, null, true, null, null,
					null, new Date());

			final int idNarExecuteRequired = npsAsyncRequestDAO.getMaxIdNarByProtocollo(infoProtocollo, isEmergenzaNotUpdated, con);

			final AsyncRequestDTO asyncRequestDTO = costructRequest(idNarExecuteRequired, input, new Date(), partition, priority, stato,
					UPDATE_PROTOCOLLO_COMPLETO_METHOD_NAME, infoProtocollo, BusinessDelegate.Protocollo.class, isEmergenzaNotUpdated, idDocumento);

			idNar = insertRequest(asyncRequestDTO, con);

		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inserimento su DB della request updateProtocolloCompleto", e);
		}

		return idNar;
	}

	/**
	 * @see it.ibm.red.business.service.INpsSRV#aggiungiAssegnatarioPerRiassegnazioneAsync(int,
	 *      java.lang.String, java.lang.Long, java.lang.Long, java.lang.String,
	 *      it.ibm.red.business.dto.ListSecurityDTO, java.util.Date,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.String, java.lang.String,
	 *      java.sql.Connection,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@SuppressWarnings("unchecked")
	@Override
	public int aggiungiAssegnatarioPerRiassegnazioneAsync(final int idNar, String numeroAnnoProtocollo, final Long idNodoNew, final Long idUtenteNew,
			final String idProtocollo, final ListSecurityDTO securityList, final Date dataAssegnazione, final UtenteDTO utente, final String perCompetenza,
			final String idDocumento, final Connection con, final IFilenetCEHelper fceh) {
		int output = 0;
		final int partition = npsAsyncRequestDAO.getPartition(utente.getIdAoo().intValue(), con);
		final int priority = 1;
		final int stato = 1;

		try {
			final String acl = getACLFromSecurity(securityList, fceh);
			final NpsConfiguration npsConf = npsConfigurationDAO.getByIdAoo(utente.getIdAoo().intValue(), con);

			final OrgOrganigrammaType assegnante = Builder.buildOrganigrammaPf(String.valueOf(utente.getId()), utente.getCognome(), utente.getNome(), utente.getCodFiscale(),
					npsConf.getCodiceAmministrazione(), npsConf.getDenominazioneAmministrazione(), npsConf.getCodiceAoo(), npsConf.getDenominazioneAoo(),
					utente.getCodiceUfficio(), utente.getNodoDesc(), String.valueOf(utente.getIdUfficio()));

			final OrgOrganigrammaType assegnatarioOrg = creaOperatoreNps(idNodoNew, idUtenteNew, null, false, con);

			numeroAnnoProtocollo = numeroAnnoProtocollo.concat("_" + npsConf.getCodiceAoo());
			final RichiestaAggiungiAssegnatarioProtocolloEntrataType input = Director.constructAggiungiAssegnatarioProtocolloEntrataInput(idProtocollo, acl, dataAssegnazione,
					perCompetenza, assegnante, assegnatarioOrg);

			final AsyncRequestDTO<RichiestaAggiungiAssegnatarioProtocolloEntrataType> asyncRequestDTO = costructRequest(idNar, input, new Date(), partition, priority, stato,
					Constants.NpsConstants.AGGIUNGI_ASSEGNATARIO_METHOD_NAME, numeroAnnoProtocollo, BusinessDelegate.Assegnatario.class,
					protocolloEmergenzaDocSRV.isEmergenzaNotUpdated(idProtocollo, con), idDocumento);
			output = insertRequest(asyncRequestDTO, con);

		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inserimento su DB della request aggiungiAssegnatario", e);
		}
		return output;
	}

	/**
	 * Cambia stato protocollo in entrata.
	 * 
	 * @param guidProtocollo
	 * @param utente
	 * @param stato
	 * @param connection
	 * @return
	 */
	@Override
	public boolean cambiaStatoProtEntrata(final String guidProtocollo, final String numeroAnnoProtocollo, final UtenteDTO utente, final String stato, final String idDocumento,
			final Connection con) {

		boolean result = false;

		try {

			// se siamo mock, non procedere con il colloquio con NPS
			if (!StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED)) && "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[cambiaStatoProtEntrata] Non è possibile comunicare con NPS in regime mock");
				return true;
			}

			result = cambiaStatoProtEntrataAsync(guidProtocollo, numeroAnnoProtocollo, utente, stato, idDocumento, con) > 0;

			if (!result) {
				LOGGER.error("Errore nel cambio stato del protocollo in entrata " + guidProtocollo);
			}

		} catch (final Exception e) {
			LOGGER.error("cambiaStatoProtEntrata -> Si è verificato un errore durante il cambio stato del protocollo da parte dell'utente: " + utente.getUsername(), e);
			throw new RedException(e);
		}

		return result;
	}

	/**
	 * Cambia stato protocollo in entrata.
	 * 
	 * @param guidProtocollo
	 * @param utente
	 * @param stato
	 * @param connection
	 * @return
	 */
	@Override
	public int cambiaStatoProtEntrataAsync(final String guidProtocollo, String infoProtocollo, final UtenteDTO utente, final String stato, final String idDocumento,
			final Connection con) {

		try {
			final NpsConfiguration npsConf = npsConfigurationDAO.getByIdAoo(utente.getIdAoo().intValue(), con);
			infoProtocollo = infoProtocollo.concat("_" + npsConf.getCodiceAoo());
			final OrgOrganigrammaType operatore = Builder.buildOrganigrammaPf(String.valueOf(utente.getId()), utente.getCognome(), utente.getNome(), utente.getCodFiscale(),
					npsConf.getCodiceAmministrazione(), npsConf.getDenominazioneAmministrazione(), npsConf.getCodiceAoo(), npsConf.getDenominazioneAoo(),
					utente.getCodiceUfficio(), utente.getNodoDesc(), String.valueOf(utente.getIdUfficio()));

			final RichiestaCambiaStatoProtocolloEntrataType input = Director.constructCambiaStatoProtocolloEntrataInput(operatore, guidProtocollo, stato, new Date());

			final AsyncRequestDTO asyncRequestDTO = costructRequest(0, input, new Date(), npsAsyncRequestDAO.getPartition(utente.getIdAoo().intValue(), con), 1, 1,
					CAMBIA_STATO_PROTOCOLLO_ENTRATA_METHOD_NAME, infoProtocollo, BusinessDelegate.Protocollo.class,
					protocolloEmergenzaDocSRV.isEmergenzaNotUpdated(guidProtocollo, con), idDocumento);

			return insertRequest(asyncRequestDTO, con);

		} catch (final Exception e) {
			LOGGER.error("cambiaStatoProtEntrataAsync -> Si è verificato un errore durante il cambio stato del protocollo da parte dell'utente: " + utente.getUsername(), e);
			throw new RedException(e);
		}

	}

	/**
	 * @param idProtocollo
	 * @param infoProtocollo
	 * @param utente
	 * @param stato
	 * @param con
	 * @return
	 */
	@Override
	public int cambiaStatoProtUscita(final String idProtocollo, final String infoProtocollo, final UtenteDTO utente, final String stato, final String idDocumento,
			final Connection con) {
		final NpsConfiguration npsConf = npsConfigurationDAO.getByIdAoo(utente.getIdAoo().intValue(), con);

		final OrgOrganigrammaType operatore = Builder.buildOrganigrammaPf(String.valueOf(utente.getId()), utente.getCognome(), utente.getNome(), utente.getCodFiscale(),
				npsConf.getCodiceAmministrazione(), npsConf.getDenominazioneAmministrazione(), npsConf.getCodiceAoo(), npsConf.getDenominazioneAoo(),
				utente.getCodiceUfficio(), utente.getNodoDesc(), String.valueOf(utente.getIdUfficio()));

		final int aooPartition = npsAsyncRequestDAO.getPartition(utente.getIdAoo().intValue(), con);

		final boolean isEmergenzaNotUpdated = protocolloEmergenzaDocSRV.isEmergenzaNotUpdated(idProtocollo, con);

		return cambiaStatoProtUscitaAsync(infoProtocollo, idProtocollo, stato, operatore, aooPartition, isEmergenzaNotUpdated, idDocumento, con);
	}

	/**
	 * @param infoProtocollo
	 * @param idProtocollo
	 * @param con
	 * @param operatore
	 * @param aooPartition
	 * @param isEmergenzaNotUpdated
	 * @return
	 */
	private int cambiaStatoProtUscitaAsync(final String infoProtocollo, final String idProtocollo, final String stato, final OrgOrganigrammaType operatore,
			final int aooPartition, final boolean isEmergenzaNotUpdated, final String idDocumento, final Connection con) {
		try {
			// Si costruisce la request per il cambio di stato del protocollo
			final RichiestaCambiaStatoProtocolloUscitaType inputCambiaStatoProtocollo = Director.constructCambiaStatoProtocolloUscitaInput(operatore, idProtocollo, stato,
					new Date());

			return insertAsyncRequest(infoProtocollo, inputCambiaStatoProtocollo, new Date(), aooPartition, 1, 1, BusinessDelegate.Protocollo.class.getName(),
					CAMBIA_STATO_PROTOCOLLO_USCITA_METHOD_NAME, 0, isEmergenzaNotUpdated, idDocumento, con);

		} catch (final Exception e) {
			LOGGER.error("cambiaStatoProtUscitaAsync -> ", e);
			throw new RedException(e);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.INpsFacadeSRV#workNextAsyncRequests(int).
	 */
	@Override
	public void workNextAsyncRequests(final int idAoo) {

		final List<AsyncRequestDTO> richieste = recuperaRichieste(idAoo);
		for (final AsyncRequestDTO request : richieste) {
			Object obj = null;
			Connection connection = null;
			try {
				String error = null;
				boolean invia = true;
				
				execAsyncRequestNPS(idAoo, request, obj, connection, error, invia);
				
			} catch (final Exception e) {
				LOGGER.error("Errore nella lavorazione richiesta " + request.getId_nar(), e);
				throw new RedException(e);
			}
		}
	}

	private void execAsyncRequestNPS(final int idAoo, final AsyncRequestDTO request, Object obj, Connection connection,
			String error, boolean invia) throws SQLException {
		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			// se siamo mock, non procedere con il colloquio con NPS
			if (!StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
					&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[workNextAsyncRequest] Non è possibile comunicare con NPS in regime mock");
				npsAsyncRequestDAO.updateStatoAsyncRequest(request.getNumeroAnnoProtocollo(), request.getId_nar(), Constants.NpsConstants.STATO_LAVORATA, null, 0,
						connection);
				return;
			}
			// inizializza NPS se non inizializzato
			WebServiceClientProvider.getIstance().checkInitializationBdNPS();

			if (!StringUtils.isNullOrEmpty(request.getNumeroAnnoProtocolloEmergenza())) {
				final NpsAzioniConfig azioneConfig = npsConfigurationDAO.getAzioneConfig(idAoo, request.getMethodName(), request.getClassName(), connection);
				invia = azioneConfig.isInviaPostEmergenza();
			}
			if (invia) {

				obj = executeAsyncRequest(idAoo, connection, request);
			}

		} catch (final Exception e) {
			LOGGER.error("Errore nella lavorazione richiesta " + request.getId_nar(), e);
			error = e.getMessage();
			if (error == null) {
				error = e.getClass().getCanonicalName();
			}
		} finally {
			closeConnection(connection);
		}
		// Infine si gestisce l'errore eventualmente presente.
		handleError(request, obj, error, invia);
	}

	/**
	 * Gestisce l'errore.
	 * 
	 * @param request
	 * @param obj
	 * @param error
	 * @param invia
	 * @throws SQLException 
	 */
	private void handleError(final AsyncRequestDTO request, final Object obj, final String error, final boolean invia) throws SQLException {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), true);
	
			if (error != null) {
				// mette in errore lo stato della request
				npsAsyncRequestDAO.updateStatoAsyncRequest(request.getNumeroAnnoProtocollo(), request.getId_nar(), Constants.NpsConstants.STATO_IN_ERRORE, error, 1, connection);
			} else {
	
				if (invia) {
					// aggiorna lo stato della request ad inviata
					updateStatoRequest(obj, request.getNumeroAnnoProtocollo(), request.getId_nar(), connection);
				} else {
					// aggiorna lo stato della request a skippata
					npsAsyncRequestDAO.updateStatoAsyncRequest(request.getNumeroAnnoProtocollo(), request.getId_nar(), Constants.NpsConstants.STATO_SKIPPATA_POST_EMERGENZA, "", 0,
							connection);
				}
	
			}
	
			commitConnection(connection);
		}catch (Exception e) {
			rollbackConnection(connection);
			throw e;
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @param idAoo
	 * @param connection
	 * @param request
	 * @return obj
	 */
	private Object executeAsyncRequest(final int idAoo, final Connection connection, final AsyncRequestDTO request) {
		Object obj;
		// in caso il metodo processato sia l'aggiornamento a completo del protocollo
		if (UPDATE_PROTOCOLLO_COMPLETO_METHOD_NAME.equals(request.getMethodName()) ||
				CAMBIA_STATO_PROTOCOLLO_ENTRATA_METHOD_NAME.equals(request.getMethodName()) ||
				CAMBIA_STATO_PROTOCOLLO_USCITA_METHOD_NAME.equals(request.getMethodName())) {
			// controlla che tutte le attività asincrone relative al protocollo siano
			// terminate
			final boolean activitiesComplete = npsAsyncRequestDAO.isActivitiesComplete(request.getNumeroAnnoProtocollo(), request.getId_nar(), connection);

			// in caso negativo, rilancia eccezione con messaggio parlante
			if (!activitiesComplete) {
				throw new RedException("Non tutte le attività asincrone sul protocollo si sono concluse con esito positivo");
			}
		}

		// esegue la request asincrona
		obj = NPSAsynchCaller.execute(idAoo, request.getClassName(), request.getMethodName(), UUID.randomUUID().toString(), request.getInput());

		if (obj instanceof BaseServiceResponseType) {
			final BaseServiceResponseType response = (BaseServiceResponseType) obj;
			if (!EsitoType.OK.equals(response.getEsito())) {

				logErrorResponse(response, "invocazione asincrona di " + request.getClassName() + "." + request.getMethodName());

			}
		}
		return obj;
	}

	/**
	 * @param idAoo
	 * @return richieste da lavorare
	 */
	private List<AsyncRequestDTO> recuperaRichieste(final int idAoo) {
		Connection connection = null;
		List<AsyncRequestDTO> richieste = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			final int partition = npsAsyncRequestDAO.getPartition(idAoo, connection);
			richieste = npsAsyncRequestDAO.getAsyncRequests(idAoo, partition, 1, connection);
			commitConnection(connection);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero della prima richiesta in coda per l'AOO " + idAoo, e);
			rollbackConnection(connection);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
		return richieste;
	}

	/**
	 * @see it.ibm.red.business.service.facade.INpsFacadeSRV#ricercaProtocolli(it.ibm.red.business.dto.ParamsRicercaProtocolloDTO,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public List<ProtocolloNpsDTO> ricercaProtocolli(final ParamsRicercaProtocolloDTO paramsRicercaProtocollo,final UtenteDTO utente) {
		return ricercaProtocolli(paramsRicercaProtocollo,utente,false);
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.INpsFacadeSRV#ricercaProtocolli(it.ibm.red.business.dto.ParamsRicercaProtocolloDTO,
	 *      it.ibm.red.business.dto.UtenteDTO, boolean).
	 */
	@Override
	public List<ProtocolloNpsDTO> ricercaProtocolli(final ParamsRicercaProtocolloDTO paramsRicercaProtocollo, final UtenteDTO utente, final boolean fromFascicolo) {
		LOGGER.info("ricercaProtocolli -> START");
		final List<ProtocolloNpsDTO> protocolliTrovati = new ArrayList<>();
		Connection con = null;

		try {
			final Short numeroRisultati = Short.valueOf(pp.getParameterByKey(PropertiesNameEnum.NPS_MAX_RISULTATI_RICERCA_PROTOCOLLI));

			// se siamo mock, non procedere con il colloquio con NPS
			if (!StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED)) && "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[ricercaProtocolli] Non è possibile comunicare con NPS in regime mock"); 
				return protocolliTrovati;
			}

			con = setupConnection(getDataSource().getConnection(), false);

			// Si recupera la configurazione NPS per l'AOO
			final NpsConfiguration npsConfigurationAoo = npsConfigurationDAO.getByIdAoo(utente.getIdAoo().intValue(), con);

			// ACL
			// Prima ricerca con il nodo corriere dell'utente
			String acl = null;
			
			boolean isAmministratore = false; 
			boolean ricercaConAdmin = false;
			if (fromFascicolo || (PermessiUtils.isAmministratore(utente.getPermessi()) && PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.RICERCA_NPS)) || 
					(PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.RICERCANPSSENZAACL) && PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.RICERCA_NPS))) {
				acl = null;
				isAmministratore = true;
				ricercaConAdmin = true;
			} else if (PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.RICERCA_NPS)){
				acl = String.valueOf(utente.getIdNodoCorriere()); 
				ricercaConAdmin = false;
			}

			// Anno protocollo
			Short annoProtocollo = null;
			if (paramsRicercaProtocollo.getDataProtocolloA() == null && paramsRicercaProtocollo.getDataProtocolloDa() == null
					&& paramsRicercaProtocollo.getAnnoProtocollo() != null) {
				annoProtocollo = paramsRicercaProtocollo.getAnnoProtocollo().shortValue();
			}

			// Tipo protocollo
			ProtTipoProtocolloType tipoProtocollo = null;

			if (paramsRicercaProtocollo.getTipoProtocollo() != null
					&& ProtTipoProtocolloType.ENTRATA.toString().equals(paramsRicercaProtocollo.getTipoProtocollo().toString())) {
				tipoProtocollo = ProtTipoProtocolloType.ENTRATA;
			} else if (paramsRicercaProtocollo.getTipoProtocollo() != null
					&& ProtTipoProtocolloType.USCITA.toString().equals(paramsRicercaProtocollo.getTipoProtocollo().toString())) {
				tipoProtocollo = ProtTipoProtocolloType.USCITA;
			}

			// Tipologia documento
			String tipologiaDocumento = null;
			if (paramsRicercaProtocollo.getTipologiaDocumento() != null) {
				tipologiaDocumento = paramsRicercaProtocollo.getTipologiaDocumento();
			}

			// Tipo dettaglio estrazione
			final ProtTipoDettaglioEstrazioneType[] elencoTipoDettaglioEstrazione = new ProtTipoDettaglioEstrazioneType[2];
			elencoTipoDettaglioEstrazione[0] = ProtTipoDettaglioEstrazioneType.DATI_MINIMI_ESTESI;
			if(!ricercaConAdmin && utente.isDownloadSistemiEsterni()) {
				elencoTipoDettaglioEstrazione[1] = ProtTipoDettaglioEstrazioneType.ACL_ESTESE;				
			}

			// ### ESECUZIONE DELLA PRIMA RICERCA
			boolean isAnnullato = false;
			if (paramsRicercaProtocollo.getFlagAnnullato()) {
				isAnnullato = true;
			}

			final Set<ProtocolloNpsDTO> protocolliTrovatiSet = eseguiRicerca(utente.getIdAoo().intValue(), numeroRisultati, acl, annoProtocollo,
					paramsRicercaProtocollo, npsConfigurationAoo, tipologiaDocumento, tipoProtocollo,
					elencoTipoDettaglioEstrazione,isAnnullato,ricercaConAdmin);

			// ACL
			// Seconda ricerca con l'ufficio dell'utente
			acl = null; 
			if (PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.RICERCA_NPS)) {
				acl = String.valueOf(utente.getIdUfficio()); 
			}

			// ### ESECUZIONE DELLA SECONDA RICERCA
			if (!isAmministratore) {
				protocolliTrovatiSet.addAll(eseguiRicerca(utente.getIdAoo().intValue(), numeroRisultati, acl, annoProtocollo, paramsRicercaProtocollo, npsConfigurationAoo,
						tipologiaDocumento, tipoProtocollo, elencoTipoDettaglioEstrazione, isAnnullato,
						ricercaConAdmin));
			}
			protocolliTrovati.addAll(protocolliTrovatiSet);

			if (protocolliTrovati.size() > numeroRisultati) {
				for (int i = protocolliTrovati.size() - 1; i >= numeroRisultati; i--) {
					protocolliTrovati.remove(i);
				}
			}

			Collections.sort(protocolliTrovati, (final ProtocolloNpsDTO prot1, final ProtocolloNpsDTO prot2) -> {
				Integer output = null;
				if (prot1.getDataProtocollo().before(prot2.getDataProtocollo())) {
					output = 1;
				} else if (prot1.getDataProtocollo().equals(prot2.getDataProtocollo())) {
					output = 0;
				} else {
					output = -1;
				}
				return output;
			});

		} catch (final Exception e) {
			LOGGER.error("ricercaProtocolli -> Si è verificato un errore durante la ricerca di protocolli da parte dell'utente: " + utente.getUsername(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

		LOGGER.info("ricercaProtocolli -> END");
		return protocolliTrovati;
	}

	private Set<ProtocolloNpsDTO> eseguiRicerca(final Integer idAoo, final Short numeroRisultati, final String acl, final Short annoProtocollo,
			final ParamsRicercaProtocolloDTO paramsRicercaProtocollo, final NpsConfiguration npsConfigurationAoo, final String tipologiaDocumento,
			final ProtTipoProtocolloType tipoProtocollo, final ProtTipoDettaglioEstrazioneType[] elencoTipoDettaglioEstrazione, final boolean isAnnullato,
			final boolean ricercaConAdmin) {
			
		final Set<ProtocolloNpsDTO> protocolliTrovatiSet = new HashSet<>();

		initBusinessDelegate();
		
		final RispostaRicercaProtocolloType rispostaRicercaProtocollo = BusinessDelegate.Protocollo.ricerca(idAoo, numeroRisultati, acl, annoProtocollo,
				/* isAnnullato */ isAnnullato, /* isCompleto */false, /* isRiservato */false, paramsRicercaProtocollo.getOggetto(), /* sistemaProduttore */ null,
				paramsRicercaProtocollo.getStato(), tipoProtocollo, /* statoSpedizione */ null, /* descrizioneTipologiaDocumentale */ tipologiaDocumento,
				/* codiceTipologiaDocumentale */ tipologiaDocumento, /* descrizioneVoceTitolario */ null, /* codiceVoceTitolario */ null,
				/* numeroRegistrazioneA */ paramsRicercaProtocollo.getNumeroProtocolloA(), /* numeroRegistrazioneDa */ paramsRicercaProtocollo.getNumeroProtocolloDa(),
				/* codiceRegistro */ npsConfigurationAoo.getRegistroUfficiale(), /* dataAnnullamentoA */ null, /* dataAnnullamentoDa */ null,
				/* dataRegistrazioneDa */ paramsRicercaProtocollo.getDataProtocolloDa(), /* codiceAmministrazione */ npsConfigurationAoo.getCodiceAmministrazione(),
				/* idUtente */ null, /* denominazioneAmministrazione */ npsConfigurationAoo.getDenominazioneAmministrazione(), /* nome */ null, /* cognome */ null,
				/* denominazioneRegistro */ npsConfigurationAoo.getDenominazioneRegistro(), /* dataRegistrazioneA */ paramsRicercaProtocollo.getDataProtocolloA(),
				/* codiceAmministrazioneAssegnante */ null, /* nomeAssegnante */null, /* idOperatoreAssegnante */ null, /* cognomeAssegnante */ null,
				/* codiceAOOAssegnante */ null, /* denominazioneAOOAssegnante */ null, /* denominazioneAmministrazioneAssegnante */ null, /* codiceUOAssegnante */ null,
				/* denominazioneUOAssegnante */ null, /* cognomeAssegnatario */ null, /* idOperatoreAssegnatario */ null, /* nomeAssegnatario */ null,
				/* codiceAmministrazioneAssegnatario */ null, /* denominazioneAOOAssegnatario */ null, /* codiceUOAssegnatario */ null, /* codiceAOOAssegnatario */ null,
				/* denominazioneUOAssegnatario */ null, /* denominazioneAmministrazioneAssegnatario */ null, /* codiceMezzoRicezione, */ null,
				/* numeroRegistrazioneMittenteA */ null, /* dataRicezioneA */ null, /* descrizioneMezzoRicezione */ null, /* dataRicezioneDa */ null,
				/* numeroRegistrazioneMittenteDa */null, /* dataProtocolloMittenteA */ null, /* dataProtocolloMittenteDa */ null, /* dataSpedizioneDa */ null,
				/* codiceAOOPostaElettronica */ npsConfigurationAoo.getCodiceAoo(), /* denominazioneUOPostaElettronica */null, "denominazioneAOOPostaElettronica",
				/* dataSpedizioneA */ null, /* destinatarioSpedizione */ paramsRicercaProtocollo.getDestinatario(), /* displayNameEmailSpedizione */ null,
				/* codiceUOPostaElettronica */ null, /* descrizioneMezzoSpedizioneCartaceo */ null, /* codiceMezzoSpedizioneCartaceo */ null, /* emailSpedizione */ null,
				paramsRicercaProtocollo.getMittente(), elencoTipoDettaglioEstrazione, null, null,
				/* ricercaConAdmin */ricercaConAdmin);

		if (rispostaRicercaProtocollo != null && EsitoType.OK.equals(rispostaRicercaProtocollo.getEsito())) {
			List<ProtRisultatoRicercaProtocolloType> risultatiRicercaProtocolli = rispostaRicercaProtocollo.getDatiRicerca();
			
			if (!CollectionUtils.isEmpty(risultatiRicercaProtocolli)) {
				for (ProtRisultatoRicercaProtocolloType risultatoRicercaProtocolli : risultatiRicercaProtocolli) {
					ProtocolloNpsDTO protocolloTrovato = null;
					if(risultatoRicercaProtocolli.getBase()!=null) {
						protocolloTrovato = fillBaseProt(risultatoRicercaProtocolli);
					} else if(risultatoRicercaProtocolli.getEntrata()!=null) {
						protocolloTrovato = fillEntrataProt(risultatoRicercaProtocolli);
					} else if(risultatoRicercaProtocolli.getUscita()!=null) {
						protocolloTrovato = fillUscitaProt(risultatoRicercaProtocolli);
					}
					if(protocolloTrovato!=null) {
						protocolliTrovatiSet.add(protocolloTrovato);
					}
				}
			}
		} else { 
			logErrorResponse(rispostaRicercaProtocollo, "ricerca dati di protocollo"); 
		}

		return protocolliTrovatiSet;
	}

	private void updateStatoRequest(final Object obj, final String numeroAnnoProtocollo, final int idNar, final Connection connection) {
		if (obj != null) {
			BaseServiceResponseType risposta = null;
			if (obj instanceof BaseServiceResponseType) {
				risposta = ((BaseServiceResponseType) obj);
			}

			final boolean actualError = isActualError(risposta);

			if (actualError) {
				// in caso di errore
				npsAsyncRequestDAO.updateStatoAsyncRequest(numeroAnnoProtocollo, idNar, Constants.NpsConstants.STATO_IN_ERRORE,
						risposta.getErrorList().get(0).getErrorMessageString(), 1, connection);
			} else {
				// in caso di esito positivo
				npsAsyncRequestDAO.updateStatoAsyncRequest(numeroAnnoProtocollo, idNar, Constants.NpsConstants.STATO_LAVORATA, null, 0, connection);
			}
		}
	}

	/**
	 * 
	 * @param idProtocollo          Corrisponde al medatadato.idprotocollo di un
	 *                              document
	 * @param motivoAnnullamento    stringa inserita dall'utente
	 * @param provvedimentoAnnulato stringa inserita dall'utente
	 * @param utente
	 * 
	 * @throw RedException se qualcosa va storto
	 */
	@Override
	public void annullaProtocollo(final String idProtocollo, final String infoProtocollo, final String motivoAnnullamento, final String provvedimentoAnnulato,
			final UtenteDTO utente, final String idDocumento) {
		LOGGER.info("annullaProtocollo -> START");
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			// Si recupera la configurazione NPS per l'AOO
			final NpsConfiguration npsConfigurationAoo = npsConfigurationDAO.getByIdAoo(utente.getIdAoo().intValue(), con);

			final OrgOrganigrammaType annullatore = Builder.buildOrganigrammaPf(utente.getId().toString(), utente.getCognome(), utente.getNome(), utente.getCodFiscale(),
					npsConfigurationAoo.getCodiceAmministrazione(), npsConfigurationAoo.getDenominazioneAmministrazione(), npsConfigurationAoo.getCodiceAoo(),
					npsConfigurationAoo.getDenominazioneAoo(), utente.getCodiceUfficio(), utente.getNodoDesc(), utente.getIdUfficio().toString());

			if (TipologiaProtocollazioneEnum.EMERGENZANPS.getId().equals(utente.getIdTipoProtocollo())) {

				final RichiestaAnnullamentoProtocolloType input = Director.constructAnnullamentoProtocolloInput(idProtocollo, motivoAnnullamento, new Date(),
						provvedimentoAnnulato, annullatore, new Date());

				final AsyncRequestDTO asyncRequestDTO = costructRequest(0, input, new Date(), npsAsyncRequestDAO.getPartition(utente.getIdAoo().intValue(), con), 1, 1,
						"annullamento", infoProtocollo, BusinessDelegate.Protocollo.class, protocolloEmergenzaDocSRV.isEmergenzaNotUpdated(idProtocollo, con), idDocumento);

				insertRequest(asyncRequestDTO, con);

			} else {

				// se siamo mock, non procedere con il colloquio con NPS
				if (!StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
						&& "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
					LOGGER.warn("[annullaProtocollo] Non è possibile comunicare con NPS in regime mock");
					return;
				}

				initBusinessDelegate();
				
				final RispostaAnnullamentoProtocolloType result = BusinessDelegate.Protocollo.annullamento(utente.getIdAoo().intValue(), idProtocollo, motivoAnnullamento,
						new Date(), provvedimentoAnnulato, annullatore);
				if (result.getEsito() != EsitoType.OK) {
					logErrorResponse(result, "annullamento del protocollo");
					throw new RedException("Errore in fase di annullameno del protocollo" + idProtocollo);
				}
			}
		} catch (final Exception e) {
			LOGGER.error("annullaProtocollo -> Si è verificato un errore durante l'annullamento dei protocolli da parte dell'utente: " + utente.getUsername(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

		LOGGER.info("annullaProtocollo -> END");
	}

	/**
	 * Recupera il dettaglio di un protocollo.
	 * 
	 * @param protocolloNps
	 * @return
	 */
	@Override
	public final ProtocolloNpsDTO getDettagliProtocollo(final ProtocolloNpsDTO protocolloNps, final Integer idAoo,final UtenteDTO utente,final boolean aperturaDaFascicolo) {
		return getDettagliProtocollo(idAoo, protocolloNps, false, utente,aperturaDaFascicolo);
	}

	@Override
	public final ProtocolloNpsDTO getDettagliProtocollo(final Integer idAoo, final ProtocolloNpsDTO protocolloNps, final boolean perProtocollazioneAutomatica,
			final UtenteDTO utente,final boolean aperturaDaFascicolo) {
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			return getDettagliProtocollo(protocolloNps, perProtocollazioneAutomatica, idAoo, utente, aperturaDaFascicolo, con);
		} catch (final Exception e) {
			LOGGER.error("getDettagliProtocollo ", e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

	}

	@Override
	public final ProtocolloNpsDTO getDettagliProtocollo(final ProtocolloNpsDTO protocolloNps, final boolean perProtocollazioneAutomatica, final Integer idAoo,
			final UtenteDTO utente,final boolean aperturaDaFascicolo, final Connection connection) {
		LOGGER.info("getDettagliProtocollo -> START. ID del protocollo NPS: " + protocolloNps.getIdProtocollo() + ". Per protocollazione automatica: "
				+ perProtocollazioneAutomatica);

		try {
			final ProtTipoDettaglioEstrazioneType[] elencoTipoDettaglioEstrazione = new ProtTipoDettaglioEstrazioneType[8];
			final ProtTipoDettaglioEstrazioneType allegatiParam = ProtTipoDettaglioEstrazioneType.ALLEGATI;
			final ProtTipoDettaglioEstrazioneType allaccioParam = ProtTipoDettaglioEstrazioneType.ALLACCIO;
			final ProtTipoDettaglioEstrazioneType storicoParam = ProtTipoDettaglioEstrazioneType.STORICO_OPERAZIONI;
			final ProtTipoDettaglioEstrazioneType datiMinimiParam = ProtTipoDettaglioEstrazioneType.DATI_MINIMI_ESTESI;
			final ProtTipoDettaglioEstrazioneType allegatiOperazioniParam = ProtTipoDettaglioEstrazioneType.ALLEGATI_OPERAZIONI;
			final ProtTipoDettaglioEstrazioneType assegnatariParam = ProtTipoDettaglioEstrazioneType.ASSEGNATARI;
			final ProtTipoDettaglioEstrazioneType metadatiParam = ProtTipoDettaglioEstrazioneType.METADATI_TIPOLOGIA_DOCUMENTALE;
			final ProtTipoDettaglioEstrazioneType aclEstese = ProtTipoDettaglioEstrazioneType.ACL_ESTESE;
			elencoTipoDettaglioEstrazione[0] = allegatiParam;
			elencoTipoDettaglioEstrazione[1] = allaccioParam;
			elencoTipoDettaglioEstrazione[2] = storicoParam;
			elencoTipoDettaglioEstrazione[3] = datiMinimiParam;
			elencoTipoDettaglioEstrazione[4] = allegatiOperazioniParam;
			elencoTipoDettaglioEstrazione[5] = assegnatariParam;
			elencoTipoDettaglioEstrazione[6] = metadatiParam;
 
			if(utente!=null && PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.RICERCA_NPS) && utente.isDownloadSistemiEsterni()) {
				elencoTipoDettaglioEstrazione[7] = aclEstese;
			} 
  
			final NpsConfiguration npsConfigurationAoo = npsConfigurationDAO.getByIdAoo(idAoo, connection);

			if (ProtTipoProtocolloType.ENTRATA.toString().equalsIgnoreCase(protocolloNps.getTipoProtocollo())) {
				RispostaGetProtocolloEntrataType risposta = null;
				if (StringUtils.isNullOrEmpty(protocolloNps.getIdProtocollo())) {
					risposta = BusinessDelegate.Protocollo.getEntrata(idAoo, protocolloNps.getDataProtocollo(), null, protocolloNps.getNumeroProtocollo(),
							npsConfigurationAoo.getRegistroUfficiale(), npsConfigurationAoo.getCodiceAoo(), npsConfigurationAoo.getCodiceAmministrazione(),
							npsConfigurationAoo.getDenominazioneRegistro(), npsConfigurationAoo.getDenominazioneAmministrazione(),
							npsConfigurationAoo.getDenominazioneAmministrazione(), elencoTipoDettaglioEstrazione);
				} else {
					risposta = BusinessDelegate.Protocollo.getEntrata(idAoo, protocolloNps.getIdProtocollo(), elencoTipoDettaglioEstrazione);
				}

				if (risposta != null && EsitoType.OK.equals(risposta.getEsito())) {
					popolaConDettagliProtocollo(idAoo, protocolloNps, risposta.getDatiProtocollo(), perProtocollazioneAutomatica);
				} else {
					logErrorResponse(risposta, "recupero delle informazioni di dettaglio del protocollo in entrata " + protocolloNps.getIdProtocollo());
				}
			} else if (ProtTipoProtocolloType.USCITA.toString().equalsIgnoreCase(protocolloNps.getTipoProtocollo())) {
				RispostaGetProtocolloUscitaType risposta = null;
				if (StringUtils.isNullOrEmpty(protocolloNps.getIdProtocollo())) {
					risposta = BusinessDelegate.Protocollo.getUscita(idAoo, protocolloNps.getDataProtocollo(), null, protocolloNps.getNumeroProtocollo(),
							npsConfigurationAoo.getRegistroUfficiale(), npsConfigurationAoo.getCodiceAoo(), npsConfigurationAoo.getCodiceAmministrazione(),
							npsConfigurationAoo.getDenominazioneRegistro(), npsConfigurationAoo.getDenominazioneAmministrazione(),
							npsConfigurationAoo.getDenominazioneAmministrazione(), elencoTipoDettaglioEstrazione);
				} else {
					risposta = BusinessDelegate.Protocollo.getUscita(idAoo, protocolloNps.getIdProtocollo(), elencoTipoDettaglioEstrazione);
				}

				if (risposta != null && EsitoType.OK.equals(risposta.getEsito())) {
					popolaConDettagliProtocollo(idAoo, protocolloNps, risposta.getDatiProtocollo(), perProtocollazioneAutomatica);
				} else {
					logErrorResponse(risposta, "recupero delle informazioni di dettaglio del protocollo in uscita " + protocolloNps.getIdProtocollo());
				}
			}
		} catch (final Exception e) {
			LOGGER.error("getDettagliProtocollo -> Si è verificato un errore durante il recupero dei dettagli del protocollo NPS con ID: " + protocolloNps.getIdProtocollo(),
					e);
			throw new RedException(e);
		}

		LOGGER.info("getDettagliProtocollo -> END");
		return protocolloNps;
	}

	private void popolaConDettagliProtocollo(final Integer idAoo, final ProtocolloNpsDTO inProtocolloNps, final ProtProtocolloBaseResponseType datiProtocollo,
			final boolean perProtocollazioneAutomatica) {
		// Protocollo
		if (datiProtocollo.getIdentificatoreProtocollo() != null) {

			inProtocolloNps.setIdProtocollo(datiProtocollo.getIdProtocollo());
			inProtocolloNps.setNumeroProtocollo(datiProtocollo.getIdentificatoreProtocollo().getNumeroRegistrazione());
			inProtocolloNps.setDataProtocollo(datiProtocollo.getIdentificatoreProtocollo().getDataRegistrazione().toGregorianCalendar().getTime());

			final Calendar cal = Calendar.getInstance();
			cal.setTime(inProtocolloNps.getDataProtocollo());
			final int annoProtocollo = cal.get(Calendar.YEAR);
			inProtocolloNps.setAnnoProtocollo("" + annoProtocollo);

			inProtocolloNps.setTipoProtocollo(datiProtocollo.getIdentificatoreProtocollo().getTipoProtocollo().toString());
			inProtocolloNps.setStatoProtocollo(datiProtocollo.getStato());

			inProtocolloNps.setCodiceAoo(datiProtocollo.getIdentificatoreProtocollo().getRegistro().getAoo().getCodiceAOO());
		}

		// Oggetto
		inProtocolloNps.setOggetto(datiProtocollo.getOggetto());

		// Descrizione tipologia documento
		final ProtAttributiEstesiType tipologiaDocumento = datiProtocollo.getTipologiaDocumento();
		if (tipologiaDocumento.getTipologia() != null && tipologiaDocumento.getTipologia().getCodice() != null) {
			inProtocolloNps.setDescrizioneTipologiaDocumento(tipologiaDocumento.getTipologia().getCodice());

			// Metadati estesi della tipologia documento
			if (perProtocollazioneAutomatica && tipologiaDocumento.getMetadatiAssociati() != null && tipologiaDocumento.getMetadatiAssociati().getValue() != null) {
				final List<ProtMetadatoAssociatoType> metadatiEstesiTipologiaDocProt = tipologiaDocumento.getMetadatiAssociati().getValue().getMetadatoAssociato();

				if (!CollectionUtils.isEmpty(metadatiEstesiTipologiaDocProt)) {
					final List<MetadatoDTO> metadatiEstesiTipologiaDoc = new ArrayList<>();

					MetadatoDTO metadatoEsteso = null;
					for (final ProtMetadatoAssociatoType metadatoEstesoTipologiaDocProt : metadatiEstesiTipologiaDocProt) {
						metadatoEsteso = new MetadatoDTO(metadatoEstesoTipologiaDocProt.getCodice(), TipoMetadatoEnum.get(metadatoEstesoTipologiaDocProt.getTipo()),
								metadatoEstesoTipologiaDocProt.getValore());

						metadatiEstesiTipologiaDoc.add(metadatoEsteso);
					}

					inProtocolloNps.setMetadatiEstesiTipologiaDoc(metadatiEstesiTipologiaDoc);
				}
			}
		}

		// Descrizione titolario
		if (datiProtocollo.getVoceTitolario() != null) {
			inProtocolloNps.setCodiceTitolario(datiProtocollo.getVoceTitolario().getCodice());
			inProtocolloNps.setDescrizioneTitolario(datiProtocollo.getVoceTitolario().getDescrizione());
		}

		// Mittente
		final ProtMittenteType mittente = datiProtocollo.getMittente();
		if (mittente != null) {
			Contatto contattoMittente = null;
			String mittenteString;

			if (mittente.getPersonaFisica() != null) {

				final AnaPersonaFisicaType mittentePF = mittente.getPersonaFisica();
				mittenteString = mittentePF.getNome() + " " + mittentePF.getCognome() + ";";
				contattoMittente = new Contatto(mittentePF.getNome(), mittentePF.getCognome(), "F", mittentePF.getCodiceFiscale(), null, null,
						mittentePF.getNome() + " " + mittentePF.getCognome(), 1, null);

			} else if (mittente.getPersonaGiuridica() != null) {

				final AnaPersonaGiuridicaType mittentePG = mittente.getPersonaGiuridica();
				mittenteString = mittentePG.getDenominazione() + ";";
				contattoMittente = new Contatto(mittentePG.getDenominazione(), null, "G", null, null, null, mittentePG.getDenominazione(), 1, null);

			} else {

				final AnaEnteEsternoType mittenteEnte = mittente.getEnte();
				String descMittente;
				if (!StringUtils.isNullOrEmpty(mittenteEnte.getDenominazioneUO())) {
					mittenteString = mittenteEnte.getDenominazioneUO() + ";";
					descMittente = mittenteEnte.getDenominazioneUO();
				} else {
					mittenteString = mittenteEnte.getCodiceUO() + ";";
					descMittente = mittenteEnte.getCodiceUO();
				}
				contattoMittente = new Contatto(descMittente, null, "G", null, null, null, descMittente, 1, null);

			}

			inProtocolloNps.setMittenteString(mittenteString);
			inProtocolloNps.setMittente(contattoMittente);
			
			if(mittente.getRicezione() != null) {
				ProtRicezioneType protocolloMittente = mittente.getRicezione();
				if(protocolloMittente.getDataProtocolloMittente()!=null) {
					inProtocolloNps.setDataProtocolloMittente(protocolloMittente.getDataProtocolloMittente().toGregorianCalendar().getTime());
					inProtocolloNps.setNumeroProtocolloMittente(protocolloMittente.getNumeroProtocolloMittente());
				}
			}
		}

		// Destinatari
		final List<ProtDestinatarioType> destinatariProt = datiProtocollo.getDestinatari();
		if (!CollectionUtils.isEmpty(destinatariProt)) {
			final StringBuilder destinatarioString = new StringBuilder("");
			final List<Contatto> contattiDestinatari = new ArrayList<>();
			Contatto contattoDestinatario = null;
			for (final ProtDestinatarioType destinatarioProt : destinatariProt) {
				if (destinatarioProt.getPersonaFisica() != null) {
					destinatarioString.append(destinatarioProt.getPersonaFisica().getNome()).append(" ").append(destinatarioProt.getPersonaFisica().getCognome()).append(";");

					contattoDestinatario = new Contatto(destinatarioProt.getPersonaFisica().getNome(), destinatarioProt.getPersonaFisica().getCognome(), "F",
							destinatarioProt.getPersonaFisica().getCodiceFiscale(), null, null,
							destinatarioProt.getPersonaFisica().getNome() + " " + destinatarioProt.getPersonaFisica().getCognome(), 1, null);
				} else if (destinatarioProt.getPersonaGiuridica() != null) {
					destinatarioString.append(destinatarioProt.getPersonaGiuridica().getDenominazione()).append(";");
					contattoDestinatario = new Contatto(destinatarioProt.getPersonaGiuridica().getDenominazione(), null, "G", null, null, null,
							destinatarioProt.getPersonaGiuridica().getDenominazione(), 1, null);
				} else {
					String descDestinatario = null;
					if (!StringUtils.isNullOrEmpty(destinatarioProt.getEnte().getDenominazioneUO())) {
						descDestinatario = destinatarioProt.getEnte().getDenominazioneUO();
					} else {
						descDestinatario = destinatarioProt.getEnte().getCodiceUO();
					}
					destinatarioString.append(descDestinatario).append(";");
					contattoDestinatario = new Contatto(descDestinatario, null, "G", null, null, null, descDestinatario, 1, null);
				}
				contattiDestinatari.add(contattoDestinatario);
			}
			inProtocolloNps.setDestinatari(contattiDestinatari);
			inProtocolloNps.setDestinatariString(destinatarioString.toString());
		}

		if (datiProtocollo.getSistemaProduttore() != null) {
			inProtocolloNps.setSistemaProduttore(datiProtocollo.getSistemaProduttore());
		}
		// Allacci associati al protocollo
		final List<ProtAllaccioProtocolloType> allacciProt = datiProtocollo.getAllacciati();
		if (!CollectionUtils.isEmpty(allacciProt)) {
			final ArrayList<AllaccioDTO> allacci = new ArrayList<>();

			for (final ProtAllaccioProtocolloType allaccioProt : allacciProt) {
				final AllaccioDTO allaccio = new AllaccioDTO();
				allaccio.setIdProtocollo(allaccioProt.getIdProtocollo().getValue());
				allaccio.setStato(allaccioProt.getStato());
				allaccio.setRisposta(allaccioProt.isRisposta());
				allaccio.setDataRegistrazione(allaccioProt.getDataRegistrazione().toGregorianCalendar().getTime());
				allaccio.setOggetto(allaccioProt.getOggetto());
				allaccio.setAnnoProtocollo(String.valueOf(allaccioProt.getDataRegistrazione().getYear()));
				allaccio.setNumeroProtocollo(allaccioProt.getNumeroRegistrazione());
				allacci.add(allaccio);
			}

			inProtocolloNps.setAllacciList(allacci);
		}

		// Allegati associati al protocollo
		final List<ProtDocumentoProtocollatoType> allegatiProt = datiProtocollo.getAllegati();
		if (!CollectionUtils.isEmpty(allegatiProt)) {
			final ArrayList<AllegatoNpsDTO> allegati = new ArrayList<>();

			for (final ProtDocumentoProtocollatoType allegatoProt : allegatiProt) {
				
				final AllegatoNpsDTO allegato = new AllegatoNpsDTO();
				allegato.setGuid(allegatoProt.getIdDocumento());
				allegato.setNomeFile(allegatoProt.getFileName());
				allegato.setOggetto(allegatoProt.getDescrizione());
				allegato.setDateCreated(allegatoProt.getDataCreazione().toGregorianCalendar().getTime());
				if (allegatoProt.getOperazione() != null && allegatoProt.getOperazione().getDocumentoOperazione() != null) {
					allegato.setGuidLastVersion(allegatoProt.getOperazione().getDocumentoOperazione().getIdDocumento());
				}				
				
				DocumentoNpsDTO allegatoNPS = null;
				if (perProtocollazioneAutomatica) {
					allegatoNPS = downloadDocumento(idAoo, allegatoProt.getIdDocumento(), false);
					if (allegatoNPS == null) {
						throw new RedException("Errore nel download dell'allegato associato al protocollo: " + datiProtocollo.getIdProtocollo());
					}
					allegato.setInputStream(allegatoNPS.getInputStream());
					allegato.setContentType(allegatoNPS.getContentType());
				} 				
				
				allegati.add(allegato);
			}

			inProtocolloNps.setAllegatiList(allegati);
		}

		// Documento principale associato al protocollo
		final ProtDocumentoProtocollatoType documentoPrincipaleProt = datiProtocollo.getDocumentoPrincipale();
		if (documentoPrincipaleProt != null) {
			DocumentoNpsDTO documentoPrincipale = null;
			if (perProtocollazioneAutomatica) {
				documentoPrincipale = downloadDocumento(idAoo, documentoPrincipaleProt.getIdDocumento(),false);
				if (documentoPrincipale == null) {
					throw new RedException("Errore nel download del documento principale associato al protocollo: " + datiProtocollo.getIdProtocollo());
				}
			} else {
				documentoPrincipale = new DocumentoNpsDTO();
				documentoPrincipale.setGuid(documentoPrincipaleProt.getIdDocumento());
				documentoPrincipale.setNomeFile(documentoPrincipaleProt.getFileName());
			}
			documentoPrincipale.setOggetto(datiProtocollo.getOggetto());
			documentoPrincipale.setDataCreazione(documentoPrincipaleProt.getDataCreazione().toGregorianCalendar().getTime());
			if (documentoPrincipaleProt.getOperazione() != null && documentoPrincipaleProt.getOperazione().getDocumentoOperazione() != null) {
				documentoPrincipale.setGuidLastVersion(documentoPrincipaleProt.getOperazione().getDocumentoOperazione().getIdDocumento());
			}

			inProtocolloNps.setDocumentoPrincipale(documentoPrincipale);
		}

		// Storico delle operazioni associate al protocollo
		final List<ProtRegistroOperazioniProtocolloType> storicoListProt = datiProtocollo.getStoricoOperazioni();
		if (!CollectionUtils.isEmpty(storicoListProt)) {
			final ArrayList<StoricoDTO> storicoList = new ArrayList<>();

			for (final ProtRegistroOperazioniProtocolloType storicoProt : storicoListProt) {
				final StoricoDTO storico = new StoricoDTO();

				storico.setDescrizione(storicoProt.getDescrizione());
				storico.setTipoOperazione(storicoProt.getTipoOperazioneProtocollo());
				storico.setOperatore(storicoProt.getOperatore());
				storico.setDataOperazione(storicoProt.getDataOperazioneProtocollo().toGregorianCalendar().getTime());

				storicoList.add(storico);
			}

			inProtocolloNps.setStoricoList(storicoList);
		}

		// Dati documento annullato
		if (datiProtocollo.isIsAnnullato() && datiProtocollo.getDatiAnnullamento() != null) {
			final ProtAnnullamentoType datiAnnullamento = datiProtocollo.getDatiAnnullamento();
			inProtocolloNps.setUtenteAnnullatore(datiAnnullamento.getAnnullatore().getUtente().getNome() + " " + datiAnnullamento.getAnnullatore().getUtente().getCognome());
			inProtocolloNps.setMotivoAnnullamento(datiAnnullamento.getMotivoAnnullamento());
			inProtocolloNps.setProvvedimentoAnnullamento(datiAnnullamento.getProvvAnnullamento());
			inProtocolloNps.setDataAnnullamento(datiAnnullamento.getDataAnnullamento().toGregorianCalendar().getTime());
		}

		// Assegnatari (solo per protocollo in entrata)
		if (datiProtocollo instanceof ProtProtocolloEntrataResponseType) {
			final ProtProtocolloEntrataResponseType datiProtocolloEntrata = (ProtProtocolloEntrataResponseType) datiProtocollo;

			if (!CollectionUtils.isEmpty(datiProtocolloEntrata.getAssegnatari())) {
				final ArrayList<AssegnatariDTO> listAssegnatari = new ArrayList<>();

				for (final ProtAssegnatarioType assegnatariProt : datiProtocolloEntrata.getAssegnatari()) {
					final AssegnatariDTO assegnatari = new AssegnatariDTO();

					if (assegnatariProt.getAssegnatario() != null) {
						final AssegnatarioDTO assegnatario = new AssegnatarioDTO();

						assegnatario.setCodiceAoo(assegnatariProt.getAssegnatario().getUnitaOrganizzativa().getAoo().getCodiceAOO());
						assegnatario.setDescrizioneNodo(assegnatariProt.getAssegnatario().getUnitaOrganizzativa().getDenominazione());

						if (assegnatariProt.getAssegnatario().getUtente() != null) {
							assegnatario.setIsUtente(true);
							assegnatario.setNome(assegnatariProt.getAssegnatario().getUtente().getNome());
							assegnatario.setCognome(assegnatariProt.getAssegnatario().getUtente().getCognome());
							assegnatario.setCodiceFiscale(assegnatariProt.getAssegnatario().getUtente().getCodiceFiscale());
						} else {
							assegnatario.setIsUtente(false);
						}

						assegnatari.setAssegnatario(assegnatario);
					}
					if (assegnatariProt.getAssegnante() != null) {
						final AssegnanteDTO assegnante = new AssegnanteDTO();

						assegnante.setCodiceAoo(assegnatariProt.getAssegnante().getUnitaOrganizzativa().getAoo().getCodiceAOO());
						assegnante.setDescrizioneNodo(assegnatariProt.getAssegnante().getUnitaOrganizzativa().getDenominazione());

						if (assegnatariProt.getAssegnante().getUtente() != null) {
							assegnante.setIsUtente(true);
							assegnante.setNome(assegnatariProt.getAssegnante().getUtente().getNome());
							assegnante.setCognome(assegnatariProt.getAssegnante().getUtente().getCognome());
							assegnante.setCodiceFiscale(assegnatariProt.getAssegnante().getUtente().getCodiceFiscale());
						} else {
							assegnante.setIsUtente(false);
						}

						assegnatari.setAssegnante(assegnante);
					}
					assegnatari.setCompetenza(assegnatariProt.getPerCompetenza());
					assegnatari.setDataAssegnazione(assegnatariProt.getDataAssegnazione().toGregorianCalendar().getTime());

					listAssegnatari.add(assegnatari);
				}

				inProtocolloNps.setAssegnatariList(listAssegnatari);
			}
		}

		// Utente protocollatore
		if (datiProtocollo.getProtocollatore() != null) {
			final OrgOrganigrammaType protocollatoreProt = datiProtocollo.getProtocollatore();

			final PersonaFisicaDTO protocollatore = new PersonaFisicaDTO();
			protocollatore.setNomePF(protocollatoreProt.getUtente().getNome());
			protocollatore.setCognomePF(protocollatoreProt.getUtente().getCognome());
			protocollatore.setCodiceFiscalePF(protocollatoreProt.getUtente().getCodiceFiscale());

			inProtocolloNps.setProtocollatore(protocollatore);

			if (protocollatoreProt.getChiaveEsterna() != null) {
				inProtocolloNps.setChiaveEsternaProtocollatore(protocollatoreProt.getChiaveEsterna().getValue());
			}
		}
		
	}

	/**
	 * @see it.ibm.red.business.service.facade.INpsFacadeSRV#downloadDocumento(java.lang.Integer,
	 *      java.lang.String, boolean).
	 */
	@Override
	public final DocumentoNpsDTO downloadDocumento(final Integer idAoo, final String guid,final boolean accessoAdmin) {
		DocumentoNpsDTO documento = null;

		try {
			// Se siamo mock, non procedere con il colloquio con NPS
			if (!StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED)) && "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[downloadDocumento] Non è possibile comunicare con NPS in regime mock");
				final FileDTO doc = mockSRV.getDocumento(TipoDocumentoMockEnum.NPS, ServerUtils.getServerFullName());
				documento = new DocumentoNpsDTO();
				documento.setContentType(doc.getMimeType());
				documento.setNomeFile(doc.getFileName());
				documento.setInputStream(new ByteArrayInputStream(doc.getContent()));
				return documento;
			}

			if (!StringUtils.isNullOrEmpty(guid)) {
				documento = BusinessDelegate.Documento.downloadDocumento(idAoo, guid,accessoAdmin);
			}

		} catch (final Exception e) {
			LOGGER.error("downloadDocumento -> Si è verificato un errore durante il download del documento da NPS.", e);
			if (!StringUtils.isNullOrEmpty(e.getMessage()) && e.getMessage().contains(NPSFAULT)) {
				throw new ProtocolloGialloException(e);
			} else {
				throw new RedException(e);
			}
		}

		return documento;
	}

	/**
	 * @see it.ibm.red.business.service.INpsSRV#modificaDestinatarioUscitaAsync(java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public int modificaDestinatarioUscitaAsync(final String infoProtocollo, final String idProtocollo, final String oldEmailDestinatario, final String newEmailDestinatario,
			final UtenteDTO utente, final String idDocumento, final Connection con) throws DatatypeConfigurationException {
		// Commit O Rollback dal chiamante
		int result = 0;

		final OrgOrganigrammaType operatore = creaOperatoreNps(utente.getIdUfficio(), utente.getId(), null, false, con);

		// Recupero il destinatario
		final ProtDestinatarioType destinatario = getDestinatarioProtocolloUscita(utente.getIdAoo().intValue(), idProtocollo, oldEmailDestinatario);
		// Recupero la chiave esterna per sostituire la mail
		if (destinatario != null) {
			String chiaveDestinatario = null;
			AllChiaveEsternaType chiaveEsterna = null;
			if (destinatario.getPersonaFisica() != null) {
				chiaveEsterna = destinatario.getPersonaFisica().getChiaveEsterna();
			} else if (destinatario.getPersonaGiuridica() != null) {
				chiaveEsterna = destinatario.getPersonaGiuridica().getChiaveEsterna();
			} else if (destinatario.getEnte() != null) {
				chiaveEsterna = destinatario.getEnte().getChiaveEsterna();
			}

			if (chiaveEsterna != null) {
				chiaveDestinatario = chiaveEsterna.getValue();

				final int aooPartition = npsAsyncRequestDAO.getPartition(utente.getIdAoo().intValue(), con);
				final boolean isEmergenzaNotUpdated = protocolloEmergenzaDocSRV.isEmergenzaNotUpdated(idProtocollo, con);

				// Si richiede il cambio di stato del protocollo a "DA SPEDIRE"
				final int idNarExecuteRequired = cambiaStatoProtUscitaAsync(infoProtocollo, idProtocollo, Builder.DA_SPEDIRE, operatore, aooPartition, isEmergenzaNotUpdated,
						idDocumento, con);

				// Si richiede la modifica del destinatario
				final RichiestaModificaSpedizioneDestinatarioProtocolloType inputModificaSpedizioneDestinatario = Director
						.constructModificaDestinatarioProtocolloInput(operatore, idProtocollo, Builder.DA_SPEDIRE, null, chiaveDestinatario, newEmailDestinatario);

				result = insertAsyncRequest(infoProtocollo, inputModificaSpedizioneDestinatario, new Date(), aooPartition, 1, 1, BusinessDelegate.Destinatario.class.getName(),
						MODIFICA_DESTINATARIO_PROTOCOLLO_METHOD_NAME, idNarExecuteRequired, isEmergenzaNotUpdated, idDocumento, con);
			}
		}

		return result;
	}

	private static ProtDestinatarioType getDestinatarioProtocolloUscita(final Integer idAoo, final String idProtocollo, final String oldEmailDestinatarioReinvia) {
		final ProtTipoDettaglioEstrazioneType[] elencoTipoDettEstra = new ProtTipoDettaglioEstrazioneType[1];
		elencoTipoDettEstra[0] = ProtTipoDettaglioEstrazioneType.DATI_MINIMI;

		final RispostaGetProtocolloUscitaType risposta = BusinessDelegate.Protocollo.getUscita(idAoo, idProtocollo, elencoTipoDettEstra);

		if (risposta != null && risposta.getDatiProtocollo() != null) {
			final List<ProtDestinatarioType> destinatariArray = risposta.getDatiProtocollo().getDestinatari();
			if (destinatariArray != null && oldEmailDestinatarioReinvia != null) {

				for (final ProtDestinatarioType protDestinatarioType : destinatariArray) {
					if (protDestinatarioType.getIndirizzoTelematico() != null
							&& oldEmailDestinatarioReinvia.equalsIgnoreCase(protDestinatarioType.getIndirizzoTelematico().getValue())) {
						return protDestinatarioType;
					}
				}

			}
		}

		return null;
	}

	private <T> int insertAsyncRequest(final String infoProtocollo, final T input, final Date date, final int partition, final int priority, final int stato,
			final String className, final String methodName, final int idNarExecuteRequired, final boolean isEmergenza, final String idDocumento, final Connection con) {
		int idNar = 0;

		final AsyncRequestDTO<T> asyncRequestDTO = new AsyncRequestDTO<>();
		asyncRequestDTO.setInput(input);
		asyncRequestDTO.setXml(input);
		asyncRequestDTO.setDataCreazione(date);
		asyncRequestDTO.setPartition(partition);
		asyncRequestDTO.setPriority(priority);
		asyncRequestDTO.setStato(stato);
		asyncRequestDTO.setClassName(className);
		asyncRequestDTO.setMethodName(methodName);
		asyncRequestDTO.setId_nar_execute_required(idNarExecuteRequired);
		if (isEmergenza) {
			asyncRequestDTO.setNumeroAnnoProtocolloEmergenza(infoProtocollo);
		} else {
			asyncRequestDTO.setNumeroAnnoProtocollo(infoProtocollo);
		}
		asyncRequestDTO.setClassNameInput(input.getClass().getName());
		asyncRequestDTO.setIdDocumento(idDocumento);
		idNar = npsAsyncRequestDAO.insert(asyncRequestDTO, con);

		return idNar;
	}

	/**
	 * @see it.ibm.red.business.service.facade.INpsFacadeSRV#ricercaStampaRegistro(it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Collection<RegistroProtocolloDTO> ricercaStampaRegistro(final ParamsRicercaAvanzataDocDTO paramsRicercaRegistro, final UtenteDTO utente) {
		final Collection<RegistroProtocolloDTO> registriProtocollo = new ArrayList<>();

		// se siamo mock, non procedere con il colloquio con NPS
		if (!StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED)) && "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
			LOGGER.warn("[ricercaStampaRegistro] Non è possibile comunicare con NPS in regime mock");
			return registriProtocollo;
		}

		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			// Si recupera la configurazione NPS per l'AOO
			final NpsConfiguration npsConfigurationAoo = npsConfigurationDAO.getByIdAoo(utente.getIdAoo().intValue(), con);

			final OrgOrganigrammaType operatore = Builder.buildOrganigrammaPf(String.valueOf(utente.getId()), utente.getCognome(), utente.getNome(), utente.getCodFiscale(),
					npsConfigurationAoo.getCodiceAmministrazione(), npsConfigurationAoo.getDenominazioneAmministrazione(), npsConfigurationAoo.getCodiceAoo(),
					npsConfigurationAoo.getDenominazioneAoo(), utente.getCodiceUfficio(), utente.getNodoDesc(), utente.getIdUfficio().toString());

			final RispostaReportRegistroGiornalieroType registroGiornaliero = BusinessDelegate.Report.registroGiornaliero(utente.getIdAoo().intValue(),
					paramsRicercaRegistro.getDataCreazioneDa(), paramsRicercaRegistro.getDataCreazioneA(), operatore, npsConfigurationAoo.getRegistroUfficiale(),
					npsConfigurationAoo.getDenominazioneRegistro());

			if (registroGiornaliero != null && EsitoType.OK.equals(registroGiornaliero.getEsito())) {
				final List<Report> listaReportRegistriProtocollo = registroGiornaliero.getReport();

				if (!CollectionUtils.isEmpty(listaReportRegistriProtocollo)) {
					for (final Report reportRegistriProtocollo : listaReportRegistriProtocollo) {
						final Calendar dataRegistro = reportRegistriProtocollo.getData().toGregorianCalendar();
						final String dataNomeFile = DateUtils.dateToString(dataRegistro.getTime(), "yyyyMMdd");

						final RegistroProtocolloDTO registroProtocollo = new RegistroProtocolloDTO(reportRegistriProtocollo.getContent(), MediaType.PDF.toString(),
								dataRegistro.getTime(), dataRegistro.get(Calendar.YEAR), dataRegistro.getTime(), "registroGiornaliero_" + dataNomeFile + ".pdf");

						registriProtocollo.add(registroProtocollo);
					}
				}
			} else {

				logErrorResponse(registroGiornaliero, "ricerca del registro di protocollo");

			}
		} catch (final BusinessDelegateRuntimeException soap) {
			LOGGER.error("Errore durante la chaimata al SOAP del registro protocollo da NPS.", soap);
			throw soap;
		} catch (final Exception e) {
			LOGGER.error("Errore durante il caricamento del registro protocollo da NPS.", e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

		return registriProtocollo;
	}

	/**
	 * @see it.ibm.red.business.service.facade.INpsFacadeSRV#getDatiMinimiByIdProtocollo(java.lang.Integer,
	 *      java.lang.String, java.lang.Integer).
	 */
	@Override
	public ProtocolloNpsDTO getDatiMinimiByIdProtocollo(final Integer idAoo, final String idProtocollo, final Integer tipoProtocollo) {
		if (!StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED)) && "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
			LOGGER.warn("[getDatiMinimiByIdProtocollo] Non è possibile comunicare con NPS in regime mock");

			final ProtocolloDTO protocollo = mockSRV.getProtocollo(idProtocollo);
			return trasform(protocollo);

		}

		final ProtTipoDettaglioEstrazioneType[] elencoTipoDettEstra = new ProtTipoDettaglioEstrazioneType[1];
		elencoTipoDettEstra[0] = ProtTipoDettaglioEstrazioneType.DATI_MINIMI;

		ProtProtocolloBaseResponseType datiProtocollo = null;

		if (tipoProtocollo.equals(Protocollo.TIPO_PROTOCOLLO_USCITA)) {
			final RispostaGetProtocolloUscitaType risposta = BusinessDelegate.Protocollo.getUscita(idAoo, idProtocollo, elencoTipoDettEstra);
			if (risposta != null && EsitoType.OK.equals(risposta.getEsito())) {
				datiProtocollo = risposta.getDatiProtocollo();
			} else {

				logErrorResponse(risposta, "recupero dei dati minimi del protocollo in uscita " + idProtocollo);

			}
		} else {
			final RispostaGetProtocolloEntrataType risposta = BusinessDelegate.Protocollo.getEntrata(idAoo, idProtocollo, elencoTipoDettEstra);
			if (risposta != null && EsitoType.OK.equals(risposta.getEsito())) {
				datiProtocollo = risposta.getDatiProtocollo();
			} else {

				logErrorResponse(risposta, "recupero dei dati minimi del protocollo in entrata " + idProtocollo);

			}
		}

		ProtocolloNpsDTO protocolloNps = null;
		if (datiProtocollo != null) {
			protocolloNps = new ProtocolloNpsDTO();
			popolaConDettagliProtocollo(idAoo, protocolloNps, datiProtocollo, false);
		}

		return protocolloNps;
	}

	/**
	 * @see it.ibm.red.business.service.INpsSRV#aggiornaItemPostEmergenza(it.ibm.red.business.dto.ProtocolloEmergenzaDocDTO,
	 *      java.lang.String, java.sql.Connection).
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void aggiornaItemPostEmergenza(final ProtocolloEmergenzaDocDTO dto, final String codiceAoo, final Connection connection) {

		final String infoProtocolloEmergenza = dto.getNumeroProtocolloEmergenza() + "/" + dto.getAnnoProtocolloEmergenza() + "_" + codiceAoo;
		final String infoProtocollo = dto.getNumeroProtocollo() + "/" + dto.getAnnoProtocollo() + "_" + codiceAoo;

		// recupera gli item da riconciliare
		final List<AsyncRequestDTO> items = npsAsyncRequestDAO.recuperaItemProtocolliEmergenza(infoProtocolloEmergenza, connection);

		for (final AsyncRequestDTO item : items) {

			item.setNumeroAnnoProtocollo(infoProtocollo);

			Object genericObj = item.getInput();
			genericObj = aggiornaInputConInfoUfficiali(genericObj, dto.getIdProtocollo());
			item.setInput(genericObj);

			final int result = npsAsyncRequestDAO.aggiornaProtocolloUfficiale(item, connection);
			if (result == 1) {
				LOGGER.info("Richiesta asincrona " + item.getId_nar() + " ( prot: " + item.getNumeroAnnoProtocollo() + " )  ( protemergenza: "
						+ item.getNumeroAnnoProtocolloEmergenza() + " ) aggiornata");
			} else {
				LOGGER.warn("Richiesta asincrona " + item.getId_nar() + " ( prot: " + item.getNumeroAnnoProtocollo() + " )  ( protemergenza: "
						+ item.getNumeroAnnoProtocolloEmergenza() + " ) non aggiornata");
			}
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.INpsFacadeSRV#stampaEtichette(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public byte[] stampaEtichette(final String documentTitle, final UtenteDTO utente) {
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;
		RispostaReportProtocolloType result = null;
		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final Document d = fceh.getDocumentByDTandAOO(documentTitle, utente.getIdAoo());
//			final Document d = fceh.getDocumentByDTandAOO(("" + fpeh.getMetadato(wobNumber, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY))),
//					(utente.getIdAoo()));
			if (d.getProperties().isPropertyPresent(pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY))
					&& d.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY)) != null) {

				final String idProtocollo = d.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY));
				final String tipoReport = TIPO_REPORT;
				result = BusinessDelegate.Protocollo.stampaEtichette(utente.getIdAoo().intValue(), idProtocollo, tipoReport);

				if (result.getEsito() != EsitoType.OK) {
					logErrorResponse(result, "stampa etichette");
					throw new RedException("Errore in fase di stampa delle etichette" + idProtocollo);
				}
			}
		} catch (final Exception ex) {
			LOGGER.error("Errore durante la stampa etichette da NPS.", ex);
			throw new RedException(ex);
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}

		if (result == null) {
			LOGGER.error("RispostaReportProtocolloType non inizializzata prima della chiamata ad un metodo della classe.");
			throw new RedException("RispostaReportProtocolloType non inizializzato prima della chiamata ad un metodo della classe.");
		}

		return result.getContent();
	}

	/**
	 * @see it.ibm.red.business.service.facade.INpsFacadeSRV#
	 *      ricercaRegistrazioniAusiliarie(
	 *      it.ibm.red.business.dto.ParamsRicercaRegistrazioneAusiliariaDTO,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public List<RegistrazioneAusiliariaNPSDTO> ricercaRegistrazioniAusiliarie(final ParamsRicercaRegistrazioniAusiliarieDTO paramsRicerca, final UtenteDTO utente) {
		LOGGER.info("ricercaRegistrazioniAusiliarie -> START");
		List<RegistrazioneAusiliariaNPSDTO> regAusiliarieOut = new ArrayList<>();
		Connection con = null;

		try {
			final Short numeroRisultati = Short.valueOf(pp.getParameterByKey(PropertiesNameEnum.NPS_MAX_RISULTATI_RICERCA_PROTOCOLLI));

			// Se siamo mock, non procedere con il colloquio con NPS
			if (!StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))
					&& Boolean.TRUE.toString().equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[ricercaRegistrazioniAusiliarie] Non è possibile comunicare con NPS in regime mock");
				regAusiliarieOut = mockSRV.getRegistrazioniAusiliarie();
				return regAusiliarieOut;
			}

			con = setupConnection(getDataSource().getConnection(), false);

			// Si recupera la configurazione NPS per l'AOO
			final NpsConfiguration npsConfigurationAoo = npsConfigurationDAO.getByIdAoo(utente.getIdAoo().intValue(), con);

			String tipologiaDocumento = null;
			if (!StringUtils.isNullOrEmpty(paramsRicerca.getCodiceTipologiaDocumento()) && !"-".equals(paramsRicerca.getCodiceTipologiaDocumento())) {
				tipologiaDocumento = paramsRicerca.getCodiceTipologiaDocumento();
			}

			String oggetto = null;
			if (!StringUtils.isNullOrEmpty(paramsRicerca.getOggetto())) {
				oggetto = paramsRicerca.getOggetto();
			}

			// ### ACL: prima ricerca con il nodo corriere dell'utente
			String acl = null;

			boolean isAmministratore = false;
			if (PermessiUtils.isAmministratore(utente.getPermessi()) && PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.RICERCA_NPS)) {
				acl = null;
				isAmministratore = true;
			} else if (PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.RICERCA_NPS)) {
				acl = String.valueOf(utente.getIdNodoCorriere());
			}

			final RegistroDTO registro = registroAusiliarioDAO.getRegistro(con, paramsRicerca.getCodiceRegistro());
			final TipoRegistroAusiliarioEnum tipoRegistro = TipoRegistroAusiliarioEnum.get(registro.getIdTipo());

			// ### ESECUZIONE DELLA PRIMA RICERCA
			final Set<RegistrazioneAusiliariaNPSDTO> regAusiliarieTrovateSet = eseguiRicercaRegistrazioniAusiliarie(utente.getNodoDesc(), utente.getCodiceUfficio(),
					String.valueOf(utente.getIdUfficio()), paramsRicerca.getAnno(), registro.getNome(), registro.getNome(), tipoRegistro.getTipoRegistroNPS(), oggetto,
					tipologiaDocumento, paramsRicerca.getNumeroRegistrazioneDa(), paramsRicerca.getNumeroRegistrazioneA(), paramsRicerca.getDataRegistrazioneDa(),
					paramsRicerca.getDataRegistrazioneA(), paramsRicerca.getDataAnnullamentoDa(), paramsRicerca.getDataAnnullamentoA(), paramsRicerca.isAnnullato(), acl,
					npsConfigurationAoo, utente.getId(), utente.getNome(), utente.getCognome(), utente.getIdAoo(), numeroRisultati);

			// ### ACL: seconda ricerca con l'ufficio dell'utente
			acl = null;
			if (PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.RICERCA_NPS)) {
				acl = String.valueOf(utente.getIdUfficio());
			}

			// ### ESECUZIONE DELLA SECONDA RICERCA
			if (!isAmministratore) {
				regAusiliarieTrovateSet.addAll(eseguiRicercaRegistrazioniAusiliarie(utente.getNodoDesc(), utente.getCodiceUfficio(), String.valueOf(utente.getIdUfficio()),
						paramsRicerca.getAnno(), registro.getNome(), registro.getNome(), tipoRegistro.getTipoRegistroNPS(), oggetto, tipologiaDocumento,
						paramsRicerca.getNumeroRegistrazioneDa(), paramsRicerca.getNumeroRegistrazioneA(), paramsRicerca.getDataRegistrazioneDa(),
						paramsRicerca.getDataRegistrazioneDa(), paramsRicerca.getDataAnnullamentoDa(), paramsRicerca.getDataAnnullamentoA(), paramsRicerca.isAnnullato(), acl,
						npsConfigurationAoo, utente.getId(), utente.getNome(), utente.getCognome(), utente.getIdAoo(), numeroRisultati));
			}
			regAusiliarieOut.addAll(regAusiliarieTrovateSet);

			// Rimozione dei risultati in eccesso rispetto al numero massimo di risultati
			// per la ricerca
			if (regAusiliarieOut.size() > numeroRisultati) {
				for (int i = regAusiliarieOut.size() - 1; i >= numeroRisultati; i--) {
					regAusiliarieOut.remove(i);
				}
			}

			// Ordinamento per data registrazione
			regAusiliarieOut.sort((final RegistrazioneAusiliariaNPSDTO reg1, final RegistrazioneAusiliariaNPSDTO reg2) -> reg2.getDataRegistrazione()
					.compareTo(reg1.getDataRegistrazione()));

		} catch (final Exception e) {
			LOGGER.error("ricercaRegistrazioniAusiliarie -> Si è verificato un errore durante la ricerca delle registrazioni ausiliarie da parte dell'utente: "
					+ utente.getUsername(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

		LOGGER.info("ricercaRegistrazioniAusiliarie -> END");
		return regAusiliarieOut;
	}

	/**
	 * Esegue la ricerca delle registrazioni ausiliarie
	 * 
	 * @param ufficioUtenteFirmatarioDesc
	 *            descrizione ufficio dell'utente firmatario
	 * @param ufficioUtenteFirmatarioCodice
	 *            codice dell'ufficio dell'utente firmatario
	 * @param ufficioUtenteFirmatarioID
	 *            ID dell'ufficio dell'utente firmatario
	 * @param anno
	 *            anno di ricerca
	 * @param codiceRegistro
	 *            codice registro da ricercare
	 * @param descrizioneRegistro
	 *            descrizione registro da ricercare
	 * @param tipoRegistro
	 *            tipo registro da ricerca
	 * @param oggetto
	 *            oggetto dei documenti da ricercare
	 * @param tipologiaDocumento
	 *            tipologia documento del documento ricercato
	 * @param numeroRegistrazioneDa
	 *            numero registrazione iniziale
	 * @param numeroRegistrazioneA
	 *            numero registrazione finale
	 * @param dataRegistrazioneDa
	 *            data registrazione iniziale
	 * @param dataRegistrazioneA
	 *            data registrazione finale
	 * @param dataAnnullamentoDa
	 *            data annullamento iniziale
	 * @param dataAnnullamentoA
	 *            data annullamento finale
	 * @param annullato
	 *            flag annullato
	 * @param acl
	 *            acl del documento
	 * @param npsConfigurationAoo
	 *            configurazioni nps dell'area organizzativa
	 * @param idUtente
	 *            identificativo dell'utente
	 * @param nomeUtente
	 *            nome utente
	 * @param cognomeUtente
	 *            cognome utente
	 * @param idAoo
	 *            identificativo dell'area organizzativa
	 * @param numeroRisultati
	 *            numero dei risultati
	 * @return elenco delle registrazioni recuperate dalla ricerca
	 */
	private Set<RegistrazioneAusiliariaNPSDTO> eseguiRicercaRegistrazioniAusiliarie(final String ufficioUtenteFirmatarioDesc, final String ufficioUtenteFirmatarioCodice,
			final String ufficioUtenteFirmatarioID, final short anno, final String codiceRegistro, final String descrizioneRegistro,
			final AllTipoRegistroAusiliarioType tipoRegistro, final String oggetto, final String tipologiaDocumento, final Integer numeroRegistrazioneDa,
			final Integer numeroRegistrazioneA, final Date dataRegistrazioneDa, final Date dataRegistrazioneA, final Date dataAnnullamentoDa, final Date dataAnnullamentoA,
			final boolean annullato, final String acl, final NpsConfiguration npsConfigurationAoo, final Long idUtente, final String nomeUtente, final String cognomeUtente,
			final Long idAoo, final Short numeroRisultati) {
		final Set<RegistrazioneAusiliariaNPSDTO> regAusiliarieTrovateSet = new HashSet<>();

		initBusinessDelegate();
		
		final RispostaRicercaRegistrazioneAusiliariaType rispostaRicercaRegistrazioniAusiliarie = BusinessDelegate.RegistrazioneAusiliaria.ricercaRegistrazioneAusiliaria(
				idAoo.intValue(), anno, codiceRegistro, descrizioneRegistro, tipoRegistro, numeroRegistrazioneDa, numeroRegistrazioneA, dataRegistrazioneDa,
				dataRegistrazioneA, dataAnnullamentoDa, dataAnnullamentoA, tipologiaDocumento, annullato, acl, null, idUtente != null ? String.valueOf(idUtente) : null,
				nomeUtente, cognomeUtente, npsConfigurationAoo.getCodiceAmministrazione(), npsConfigurationAoo.getDenominazioneAmministrazione(), ufficioUtenteFirmatarioDesc,
				ufficioUtenteFirmatarioCodice, npsConfigurationAoo.getDenominazioneAoo(), npsConfigurationAoo.getCodiceAoo(), numeroRisultati, ufficioUtenteFirmatarioID,
				oggetto);

		if (rispostaRicercaRegistrazioniAusiliarie != null && EsitoType.OK.equals(rispostaRicercaRegistrazioniAusiliarie.getEsito())) {
			final List<RegRegistrazioneAusiliariaType> risultatiRicercaRegAusiliarie = rispostaRicercaRegistrazioniAusiliarie.getDatiRicerca();

			if (!CollectionUtils.isEmpty(risultatiRicercaRegAusiliarie)) {
				for (final RegRegistrazioneAusiliariaType risultatoRicercaRegAusiliarie : risultatiRicercaRegAusiliarie) {
					final RegistrazioneAusiliariaNPSDTO registrazioneAusiliaria = new RegistrazioneAusiliariaNPSDTO();

					registrazioneAusiliaria.setId(risultatoRicercaRegAusiliarie.getIdRegistrazioneAusiliaria());
					registrazioneAusiliaria.setNumeroRegistrazione(risultatoRicercaRegAusiliarie.getNumeroRegistrazione());
					if (risultatoRicercaRegAusiliarie.getRegistro() != null) {
						registrazioneAusiliaria.setCodiceRegistro(risultatoRicercaRegAusiliarie.getRegistro().getCodice().getCodice());
					}
					if (risultatoRicercaRegAusiliarie.getDataRegistrazione() != null) {
						registrazioneAusiliaria.setDataRegistrazione(risultatoRicercaRegAusiliarie.getDataRegistrazione().getValue().toGregorianCalendar().getTime());
					}
					if (risultatoRicercaRegAusiliarie.getDataAnnullamento() != null) {
						registrazioneAusiliaria.setDataAnnullamento(risultatoRicercaRegAusiliarie.getDataAnnullamento().toGregorianCalendar().getTime());
						registrazioneAusiliaria.setMotivoAnnullamento(risultatoRicercaRegAusiliarie.getMotivoAnnullamento());
					}
					if (risultatoRicercaRegAusiliarie.getDocumentoPrincipale() != null) {
						registrazioneAusiliaria.setGuidDocumento(risultatoRicercaRegAusiliarie.getDocumentoPrincipale().getIdDocumento());
					}
					registrazioneAusiliaria.setOggetto(risultatoRicercaRegAusiliarie.getOggetto());
					if (risultatoRicercaRegAusiliarie.getTipoDocumento() != null && risultatoRicercaRegAusiliarie.getTipoDocumento().getTipologia().getDescrizione() != null) {
						registrazioneAusiliaria.setDescrizioneTipologiaDocumento(risultatoRicercaRegAusiliarie.getTipoDocumento().getTipologia().getDescrizione().getValue());
					}
					registrazioneAusiliaria.setIdAoo(idAoo);

					regAusiliarieTrovateSet.add(registrazioneAusiliaria);
				}
			}
		} else {
			logErrorResponse(rispostaRicercaRegistrazioniAusiliarie, "ricerca registrazioni ausiliarie");
		}

		return regAusiliarieTrovateSet;
	}

	private static Object aggiornaInputConInfoUfficiali(Object genericObj, final String idProtocollo) {

		if (genericObj instanceof RichiestaAggiungiDocumentiProtocolloType) {
			final RichiestaAggiungiDocumentiProtocolloType obj = (RichiestaAggiungiDocumentiProtocolloType) genericObj;
			obj.setProtocollo(Builder.buildIdentificativoProtocollo(idProtocollo));
		}

		if (genericObj instanceof RichiestaAggiungiAllaccioProtocolloType) {
			final RichiestaAggiungiAllaccioProtocolloType obj = (RichiestaAggiungiAllaccioProtocolloType) genericObj;
			obj.setIdentificativoProtocollo(Builder.buildIdentificativoProtocollo(idProtocollo));
		}

		if (genericObj instanceof RichiestaSpedisciProtocolloUscitaType) {
			final RichiestaSpedisciProtocolloUscitaType obj = (RichiestaSpedisciProtocolloUscitaType) genericObj;
			obj.setProtocollo(Builder.buildIdentificativoProtocollo(idProtocollo));
		}

		if (genericObj instanceof RichiestaUpdateDatiProtocolloType) {
			final RichiestaUpdateDatiProtocolloType obj = (RichiestaUpdateDatiProtocolloType) genericObj;
			obj.setIdentificativoProtocollo(Builder.buildIdentificativoProtocollo(idProtocollo));
		}

		if (genericObj instanceof RichiestaAggiungiAssegnatarioProtocolloEntrataType) {
			final RichiestaAggiungiAssegnatarioProtocolloEntrataType obj = (RichiestaAggiungiAssegnatarioProtocolloEntrataType) genericObj;
			obj.setProtocollo(Builder.buildIdentificativoProtocollo(idProtocollo));
		}

		if (genericObj instanceof RichiestaCambiaStatoProtocolloEntrataType) {
			final RichiestaCambiaStatoProtocolloEntrataType obj = (RichiestaCambiaStatoProtocolloEntrataType) genericObj;
			obj.setProtocollo(Builder.buildIdentificativoProtocollo(idProtocollo));
		}

		if (genericObj instanceof RichiestaAnnullamentoProtocolloType) {
			final RichiestaAnnullamentoProtocolloType obj = (RichiestaAnnullamentoProtocolloType) genericObj;
			obj.setIdentificativoProtocollo(Builder.buildIdentificativoProtocollo(idProtocollo));
		}

		if (genericObj instanceof RichiestaModificaSpedizioneDestinatarioProtocolloType) {
			final RichiestaModificaSpedizioneDestinatarioProtocolloType obj = (RichiestaModificaSpedizioneDestinatarioProtocolloType) genericObj;
			obj.setProtocollo(Builder.buildIdentificativoProtocollo(idProtocollo));
		}

		if (genericObj instanceof RichiestaUploadDocumentoType) {
			// do nothing
		}
		return genericObj;
	}

	private static void logErrorResponse(final BaseServiceResponseType response, final String methodName) {
		LOGGER.error("Errore durante la procedura di " + methodName + " tramite servizi NPS");
		if (response != null && response.getErrorList() != null && !response.getErrorList().isEmpty()) {
			for (final ServiceErrorType set : response.getErrorList()) {
				LOGGER.error("Errore NPS: " + set.getErrorMessageString());
			}
		}
	}

	private static ProtocolloNpsDTO trasform(final ProtocolloDTO protocollo) {
		ProtocolloNpsDTO protocolloNPS = null;
		if (protocollo != null) {
			protocolloNPS = new ProtocolloNpsDTO();
			protocolloNPS.setIdProtocollo(protocollo.getIdProtocollo());
			protocolloNPS.setDataProtocollo(protocollo.getDataProtocollo());
			protocolloNPS.setNumeroProtocollo(protocollo.getNumeroProtocollo());
			protocolloNPS.setAnnoProtocollo(protocollo.getAnnoProtocollo());
		}

		return protocolloNPS;
	}

	private static void initBusinessDelegate() {
		WebServiceClientProvider.getIstance().checkInitializationBdNPS();
	}

	/**
	 * @see it.ibm.red.business.service.INpsSRV#creaRegistrazioneAusiliaria(it.ibm.red.business.dto.DetailDocumentoDTO,
	 *      java.lang.String, it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.RegistroDTO,
	 *      npstypes.v1.it.gov.mef.AllTipoRegistroAusiliarioType, java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public RegistrazioneAusiliariaNPSDTO creaRegistrazioneAusiliaria(final DetailDocumentoDTO documentDetail, final String acl, final UtenteDTO utenteFirmatario,
			final RegistroDTO registro, final AllTipoRegistroAusiliarioType tipoRegistro, final String idProtocolloIngresso, final Connection connection) {
		RegistrazioneAusiliariaNPSDTO registrazione = null;
		try {

			if (!StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED)) && "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[creaRegistrazioneAusiliaria] Non è possibile comunicare con NPS in regime mock");

				return mockSRV.getRegistrazioneAusiliaria(utenteFirmatario.getCodiceAoo(), registro);
			}

			final NpsConfiguration npsConf = npsConfigurationDAO.getByIdAoo(utenteFirmatario.getIdAoo().intValue(), connection);

			initBusinessDelegate();
			
			final RispostaCreateRegistrazioneAusiliariaType rispostaProtocollo = BusinessDelegate.RegistrazioneAusiliaria.createRegistrazioneAusiliaria(
					utenteFirmatario.getIdAoo().intValue(), acl, documentDetail.getOggetto(), utenteFirmatario.getId().toString(), utenteFirmatario.getNome(),
					utenteFirmatario.getCognome(), npsConf.getCodiceAmministrazione(), npsConf.getDenominazioneAmministrazione(), utenteFirmatario.getNodoDesc(),
					utenteFirmatario.getCodiceUfficio(), npsConf.getDenominazioneAoo(), npsConf.getCodiceAoo(), registro.getNome(), registro.getNome(), tipoRegistro,
					documentDetail.getTipologiaDocumento(), documentDetail.getTipologiaDocumento(), idProtocolloIngresso, String.valueOf(utenteFirmatario.getIdUfficio()),
					utenteFirmatario.getCodFiscale());

			if (rispostaProtocollo != null && rispostaProtocollo.getEsito() != null && EsitoType.OK.equals(rispostaProtocollo.getEsito())) {

				registrazione = new RegistrazioneAusiliariaNPSDTO();
				registrazione.setDataRegistrazione(rispostaProtocollo.getDatiRegistrazione().getDataRegistrazione().toGregorianCalendar().getTime());
				registrazione.setId(rispostaProtocollo.getDatiRegistrazione().getIdRegistrazioneAusiliaria());
				registrazione.setNumeroRegistrazione(rispostaProtocollo.getDatiRegistrazione().getNumeroRegistrazione());

			} else {
				logErrorResponse(rispostaProtocollo, "creazione registrazione ausiliaria");
			}

			if (registrazione == null) {
				throw new RedException(NPS_ESITO_NEGATIVO_MSG);
			}

			return registrazione;

		} catch (final Exception e) {
			LOGGER.error(ERROR_CREAZIONE_PROTOCOLLO_USCITA_MSG, e);
			if (!StringUtils.isNullOrEmpty(e.getMessage()) && e.getMessage().contains(NPSFAULT)) {
				throw new ProtocolloGialloException(e);
			} else {
				throw new RedException(e);
			}
		}
	}

	/**
	 * @see it.ibm.red.business.service.INpsSRV#collegaRegAuxProtUscita(it.ibm.red.business.dto.UtenteDTO,
	 *      java.sql.Connection, java.lang.String, java.lang.String,
	 *      java.lang.String, java.lang.String).
	 */
	@Override
	public void collegaRegAuxProtUscita(final UtenteDTO utente, final Connection connection, final String idProtocollo, final String idRegAux,
			final String numeroAnnoProtocollo, final String idDocumento) {

		try {

			final NpsConfiguration npsConf = npsConfigurationDAO.getByIdAoo(utente.getIdAoo().intValue(), connection);

			final RichiestaCollegaRegistrazioneAusiliariaProtocolloUscitaType input = Director.constructRichiestaCollegaRegistrazioneAusiliariaProtocolloUscitaType(
					utente.getId().toString(), utente.getNome(), utente.getCognome(), npsConf.getCodiceAmministrazione(), npsConf.getDenominazioneAmministrazione(),
					utente.getNodoDesc(), utente.getCodiceUfficio(), npsConf.getDenominazioneAoo(), npsConf.getCodiceAoo(), idRegAux, idProtocollo,
					String.valueOf(utente.getIdUfficio()), utente.getCodFiscale());

			final int partition = npsAsyncRequestDAO.getPartition(utente.getIdAoo().intValue(), connection);
			final int priority = 1;
			final int stato = 1;

			final AsyncRequestDTO<RichiestaCollegaRegistrazioneAusiliariaProtocolloUscitaType> asyncRequestDTO = new AsyncRequestDTO<>();
			asyncRequestDTO.setInput(input);
			asyncRequestDTO.setXml(input);
			asyncRequestDTO.setDataCreazione(new Date());
			asyncRequestDTO.setPartition(partition);
			asyncRequestDTO.setPriority(priority);
			asyncRequestDTO.setStato(stato);
			asyncRequestDTO.setClassName(BusinessDelegate.RegistrazioneAusiliaria.class.getName());
			asyncRequestDTO.setMethodName("collegaRegistrazioneAusiliariaProtocolloUscita");
			asyncRequestDTO.setId_nar_execute_required(0);
			if (!protocolloEmergenzaDocSRV.isEmergenzaNotUpdated(idProtocollo, connection)) {
				asyncRequestDTO.setNumeroAnnoProtocollo(numeroAnnoProtocollo);
			} else {
				asyncRequestDTO.setNumeroAnnoProtocolloEmergenza(numeroAnnoProtocollo);
			}
			asyncRequestDTO.setClassNameInput(RichiestaCollegaRegistrazioneAusiliariaProtocolloUscitaType.class.getName());
			asyncRequestDTO.setIdDocumento(idDocumento);
			npsAsyncRequestDAO.insert(asyncRequestDTO, connection);

		} catch (final Exception e) {
			LOGGER.error("Errore durante l'associazione fra la registrazione ausiliaria ed il protocollo in uscita", e);
			throw new RedException(e);
		}

	}

	@Override
	public final ProtocolloNpsDTO inviaAtto(final String codiceFlusso, final DetailDocumentoDTO documentDetail, final String acl, final TipoAllaccioEnum tipoAllaccio,
			final String identificatoreProcessoInput, final UtenteDTO utenteFirmatario, final String guidDocPrincipale, final String[] guidAllegati,
			final Connection connection) {
		ProtocolloNpsDTO protocollo = null;
		IFilenetCEHelper fcehAdmin = null;
		try {

			if (!StringUtils.isNullOrEmpty(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED)) && "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[inviaAtto] Non è possibile comunicare con NPS in regime mock");

				final ProtocolloDTO prot = mockSRV.getNewProtocollo(utenteFirmatario.getCodiceAoo(), TipoProtocolloEnum.USCITA);
				return trasform(prot);

			}

			// ### DESTINATARI
			// Recupero IDs dei destinatari esterni e verifica se è un Registro di
			// Repertorio
			if (!documentDetail.isHasDestinatariEsterni() && StringUtils.isNullOrEmpty(documentDetail.getDescRegistroRepertorio())) {
				throw new RedException(ERROR_DESTINATARIO_ESTERNO_ASSENTE_MSG);
			}

			fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utenteFirmatario.getFcDTO()), utenteFirmatario.getIdAoo());
			final Document mail = fcehAdmin.fetchMailSpedizione(documentDetail.getDocumentTitle(), utenteFirmatario.getIdAoo().intValue());

			String oggettoMessaggio = null;
			String corpoMessaggio = null;
			if (mail != null) {
				oggettoMessaggio = mail.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_EMAIL_METAKEY));

				ContentTransfer ct;
				if (FilenetCEHelper.hasDocumentContentTransfer(mail)) {
					ct = FilenetCEHelper.getDocumentContentTransfer(mail);
				} else {
					throw new RedException("Errore durante il recupero del content della mail con idDocumento: " + documentDetail.getDocumentTitle());
				}

				if (ct != null) {
					corpoMessaggio = new String(FileUtils.toByteArrayHtml(mail.accessContentStream(0)), StandardCharsets.UTF_8.name());
				}
			}

			final WkfDestinatarioFlussoType[] destinatariNPS = getDestinatariFlussoAUTNPS(documentDetail.getDestinatariDocumento(), null,
					utenteFirmatario.getIdAoo().intValue(), connection, mail);

			final NpsConfiguration npsConf = npsConfigurationDAO.getByIdAoo(utenteFirmatario.getIdAoo().intValue(), connection);

			final String tipologiaDocumento = getTipologiaDocumentoNPS(documentDetail.getIdTipologiaDocumento().intValue(), documentDetail.getIdTipoProcedimento().intValue(),
					documentDetail.getTipologiaDocumento(), utenteFirmatario.getIdAoo().intValue(), connection);

			final List<MetaDatoAssociato> metadatiNPS = getMetadatiNPS(documentDetail.getMetadatiEstesi());
			final TIPOLOGIA metadatiEstesi = Builder.createMetadatiEstesiFlusso(tipologiaDocumento, metadatiNPS);
			final ProtMetadatiType metadatiAssociati = createMetadatiAssociati(documentDetail.getMetadatiEstesi());

			String indiceFascicoloNps = Constants.EMPTY_STRING;
			String descrIndiceFascicoloNps = null;
			if (documentDetail.getIndiceFascicolo() != null) {
				indiceFascicoloNps = documentDetail.getIndiceFascicolo();
				descrIndiceFascicoloNps = indiceFascicoloNps + " - " + documentDetail.getDescrizioneIndiceFascicolo();
			}

			Integer identificativoMessaggio = null;
			String identificatoreProcesso = null;
			if (Constants.Varie.CODICE_FLUSSO_ATTO_DECRETO.equals(codiceFlusso)) {
				identificativoMessaggio = Constants.Varie.ID_MESSAGGIO_ATTO_DECRETO;
				identificatoreProcesso = Constants.Varie.CODICE_FLUSSO_ATTO_DECRETO + "_" + documentDetail.getDocumentTitle();
			} else {
				identificativoMessaggio = tipoAllaccio.getMessageIdFlussoAUT().getId();
				identificatoreProcesso = identificatoreProcessoInput;
			}
			
			initBusinessDelegate();
			
			final RispostaInviaAttoType rispostaInviaAtto = BusinessDelegate.FlussoAUT.inviaAtto(utenteFirmatario.getIdAoo().intValue(), acl, destinatariNPS, 
					codiceFlusso, identificatoreProcesso, "" + identificativoMessaggio, 
					utenteFirmatario.getId().toString(), utenteFirmatario.getNome(), utenteFirmatario.getCognome(), npsConf.getCodiceAmministrazione(), 
					npsConf.getDenominazioneAmministrazione(), utenteFirmatario.getNodoDesc(), utenteFirmatario.getCodiceUfficio(), npsConf.getDenominazioneAoo(), npsConf.getCodiceAoo(), 
					documentDetail.getOggetto(), metadatiEstesi, guidDocPrincipale, guidAllegati, indiceFascicoloNps, descrIndiceFascicoloNps, tipologiaDocumento, tipologiaDocumento,
					metadatiAssociati, null, String.valueOf(utenteFirmatario.getIdUfficio()), utenteFirmatario.getCodFiscale(), oggettoMessaggio, corpoMessaggio);
					
			if (rispostaInviaAtto != null && rispostaInviaAtto.getEsito() != null
					&& EsitoType.OK.equals(rispostaInviaAtto.getEsito())) {
				protocollo = new ProtocolloNpsDTO();

				final ProtProtocolloDatiMinimiType datiProtocollo = rispostaInviaAtto.getDatiProtocollo();
				// Descrizione tipologia documento
				final ProtTipologiaDocumentoType tipDoc = datiProtocollo.getTipologiaDocumento();
				if (tipDoc != null && tipDoc.getCodice() != null) {
					final String descrizioneTipologiaDocumento = tipDoc.getCodice();
					protocollo.setDescrizioneTipologiaDocumento(descrizioneTipologiaDocumento);
				}

				final Date dataProtocollazione = datiProtocollo.getDataRegistrazione().toGregorianCalendar().getTime();
				protocollo.setDataProtocollo(dataProtocollazione);
				final Calendar cal = Calendar.getInstance();
				cal.setTime(dataProtocollazione);
				final int annoProtocollazione = cal.get(Calendar.YEAR);
				protocollo.setAnnoProtocollo(String.valueOf(annoProtocollazione));
				protocollo.setIdProtocollo(datiProtocollo.getIdProtocollo());
				protocollo.setNumeroProtocollo(Integer.parseInt(datiProtocollo.getNumeroRegistrazione()));
				protocollo.setOggetto(datiProtocollo.getOggetto());
				if (datiProtocollo.getTipologiaDocumento() != null) {
					protocollo.setDescrizioneTipologiaDocumento(datiProtocollo.getTipologiaDocumento().getDescrizione().getValue());
				}
				if (datiProtocollo.getRegistro() != null) {
					protocollo.setCodiceRegistro(datiProtocollo.getRegistro().getCodice().getCodice());
				}

				protocollo.setIdDocumento(rispostaInviaAtto.getChiaveDocumento());

			} else {

				logErrorResponse(rispostaInviaAtto, "invia atto");

			}

			if (protocollo == null) {
				throw new RedException(NPS_ESITO_NEGATIVO_MSG);
			}

			return protocollo;
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'invio atto NPS", e);
			if (!StringUtils.isNullOrEmpty(e.getMessage()) && e.getMessage().contains(NPSFAULT)) {
				throw new ProtocolloGialloException(e);
			} else {
				throw new RedException(e);
			}
		} finally {
			popSubject(fcehAdmin);
		}
	}

	private static List<MetaDatoAssociato> getMetadatiNPS(final Collection<MetadatoDTO> metadatiEstesi) {
		final List<MetaDatoAssociato> metadatiNPS = new ArrayList<>();

		if (metadatiEstesi != null) {
			MetaDatoAssociato metadatoNPS = null;
			String value = "";
			for (final MetadatoDTO metadato : metadatiEstesi) {
				value = metadato.getValue4AttrExt();
				if (value == null) {
					value = "";
				}
				metadatoNPS = Builder.createMetadato(metadato.getName(), value);
				metadatiNPS.add(metadatoNPS);
			}
		}

		return metadatiNPS;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.INpsFacadeSRV#getTipologiaDocumentoNPS(java.lang.Integer,
	 *      java.lang.Integer, java.lang.String, java.lang.Integer).
	 */
	@Override
	public String getTipologiaDocumentoNPS(final Integer idTipologiaDocumento, final Integer idTipoProcedimento, final String tipologiaDocumentoDesc, final Integer idAoo) {
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			return getTipologiaDocumentoNPS(idTipologiaDocumento, idTipoProcedimento, tipologiaDocumentoDesc, idAoo, con);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero della tipologia documento di NPS", e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
	}
	
	private String getTipologiaDocumentoNPS(final Integer idTipologiaDocumento, final Integer idTipoProcedimento, final String tipologiaDocumentoDesc, final Integer idAoo,
			final Connection connection) {

		if (idTipologiaDocumento == null || idTipologiaDocumento == 0 || idTipoProcedimento == null || idTipoProcedimento == 0) {
			return tipologiaDocumentoDesc;
		}

		final TipologiaDTO tipologiaDTO = new TipologiaDTO();
		tipologiaDTO.setIdTipoDocumento(idTipologiaDocumento);
		tipologiaDTO.setIdTipoProcedimento(idTipoProcedimento);
		String tipologiaDocumento = npsConfigurationDAO.getTipologia(idAoo, tipologiaDTO, connection);
		if (StringUtils.isNullOrEmpty(tipologiaDocumento)) {
			tipologiaDocumento = tipologiaDocumentoDesc;
		}
		return tipologiaDocumento;
	}

	private WkfDestinatarioFlussoType[] getDestinatariFlussoAUTNPS(final List<DestinatarioDTO> destinatari, final List<DestinatarioRedDTO> destinatariRed, final Integer idAoo,
			final Connection connection, final Document mail) {
		String destinatariTo = null;
		String destinatariCC = null;
		boolean competenzaImpostata = false;
		WkfDestinatarioFlussoType destInternoNPS;

		String uNome = null;
		String uCognome = null;
		String uCodiceFiscale = null;
		String uNodoDesc = null;
		String uCodiceNodo = null;

		final NpsConfiguration npsConf = npsConfigurationDAO.getByIdAoo(idAoo, connection);

		final List<WkfDestinatarioFlussoType> destinatariNPS = new ArrayList<>();
		if (destinatari != null) {
			Contatto c = null;
			for (final DestinatarioDTO dest : destinatari) {
				if (Constants.TipoDestinatario.ESTERNO.equalsIgnoreCase(dest.getTipo())) {
					boolean perCompetenza = false;
					boolean isPEC = false;
					String indirizzoEmail = null;

					c = contattoDAO.getContattoByID(dest.getId(), connection);

					final MezzoSpedizioneEnum mezzoSpedizione = MezzoSpedizioneEnum.getById(dest.getTipoSpedizione());

					if (!MezzoSpedizioneEnum.CARTACEO.equals(mezzoSpedizione)) {

						if (StringUtils.isNullOrEmpty(destinatariTo)) {
							// Recupera destinatari attraverso la mail spedizione
							destinatariTo = mail.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_EMAIL_METAKEY));
							destinatariCC = mail.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_CC_EMAIL_METAKEY));
						}

						final String[] emails = EmailHelper.getSelectedMail(c, destinatariTo, destinatariCC);
						isPEC = !StringUtils.isNullOrEmpty(emails[1]);
						indirizzoEmail = isPEC ? emails[1] : emails[0];

					}

					// Si imposta per competenza il primo destinatario in TO
					if (!competenzaImpostata && !Boolean.TRUE.equals(dest.getIsCC())) {
						perCompetenza = true;
						competenzaImpostata = true;
					}

					destinatariNPS.add(getDestinatarioFlussoAUTNPS(c.getTipoPersona(), c.getNome(), c.getCognome(), c.getCodiceFiscalePIva(),
							c.getAliasContatto(), indirizzoEmail, c.getContattoID(), perCompetenza));

				} else {

					if (dest.getId() != null && dest.getId() > 0) {
						final NodoUtenteRuolo nur = utenteDAO.getUtenteUfficioByIdUtenteAndIdNodo(dest.getId(), dest.getIdUfficio(), connection);
						uNome = nur.getUtente().getNome();
						uCognome = nur.getUtente().getCognome();
						uCodiceFiscale = nur.getUtente().getCodiceFiscale();
						uNodoDesc = nur.getNodo().getDescrizione();
						uCodiceNodo = nur.getNodo().getCodiceNodo();
					} else {
						final Nodo nodo = nodoDAO.getNodo(dest.getIdUfficio(), connection);
						uNodoDesc = nodo.getDescrizione();
						uCodiceNodo = nodo.getCodiceNodo();
					}

					destInternoNPS = Builder.buildDestinatarioEnteFlussoAUT(npsConf.getCodiceAmministrazione(), npsConf.getCodiceAoo(), uCodiceNodo, uNodoDesc, uNome,
							uCognome, uCodiceFiscale);

					// Si imposta per competenza il primo destinatario in TO
					if (!competenzaImpostata) {
						destInternoNPS.setPerConoscenza(Builder.NO);
						competenzaImpostata = true;
					} else {
						destInternoNPS.setPerConoscenza(Builder.SI);
					}

					destinatariNPS.add(destInternoNPS);

				}

			}
		}

		if (destinatariRed != null) {
			Contatto c = null;
			for (final DestinatarioRedDTO dest : destinatariRed) {
				if (TipologiaDestinatarioEnum.ESTERNO.equals(dest.getTipologiaDestinatarioEnum())) {
					boolean perCompetenza = false;
					boolean isPEC = false;
					String indirizzoEmail = null;

					c = dest.getContatto();

					final MezzoSpedizioneEnum mezzoSpedizione = dest.getMezzoSpedizioneEnum();

					if (!MezzoSpedizioneEnum.CARTACEO.equals(mezzoSpedizione)) {

						if (StringUtils.isNullOrEmpty(destinatariTo)) {
							// Recupera destinatari attraverso la mail spedizione
							destinatariTo = mail.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_EMAIL_METAKEY));
							destinatariCC = mail.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.DESTINATARI_CC_EMAIL_METAKEY));
						}

						final String[] emails = EmailHelper.getSelectedMail(c, destinatariTo, destinatariCC);
						isPEC = !StringUtils.isNullOrEmpty(emails[1]);
						indirizzoEmail = isPEC ? emails[1] : emails[0];

					}

					// Si imposta per competenza il primo destinatario in TO
					if (!competenzaImpostata && ModalitaDestinatarioEnum.TO.equals(dest.getModalitaDestinatarioEnum())) {
						perCompetenza = true;
						competenzaImpostata = true;
					}

					destinatariNPS.add(getDestinatarioFlussoAUTNPS(c.getTipoPersona(), c.getNome(), c.getCognome(), c.getCodiceFiscalePIva(),
							c.getAliasContatto(), indirizzoEmail, c.getContattoID(), perCompetenza));
				}
			}
		}

		final WkfDestinatarioFlussoType[] destinatariNPSArray = new WkfDestinatarioFlussoType[destinatariNPS.size()];
		return destinatariNPS.toArray(destinatariNPSArray);
	}

	private static WkfDestinatarioFlussoType getDestinatarioFlussoAUTNPS(final String tipoPersona, final String nome, final String cognome,
			final String codiceFiscalePIva, final String aliasContatto, final String indirizzoEmail, final Long idContatto, final boolean perCompetenza) {

		WkfDestinatarioFlussoType destinatario = null;

		final String perConoscenza = perCompetenza ? Builder.NO : Builder.SI;

		if ("F".equals(tipoPersona)) {
			if (StringUtils.isNullOrEmpty(nome) || StringUtils.isNullOrEmpty(cognome)) {
				throw new RedException("Il contatto per la protocollazione NPS con ID: " + idContatto + " non ha valorizzato i campi 'Nome' e 'Cognome'");
			}

			destinatario = Builder.buildDestinatarioFlussoAUTPF(perConoscenza, nome, cognome, codiceFiscalePIva, Builder.SMTP, indirizzoEmail);
		} else {
			String denominazione = nome;
			if (StringUtils.isNullOrEmpty(denominazione)) {
				denominazione = aliasContatto;
			}

			destinatario = Builder.buildDestinatarioFlussoAUTPG(perConoscenza, denominazione, Builder.SMTP, indirizzoEmail);
		}

		return destinatario;
	}

	/**
	 * @see it.ibm.red.business.service.facade.INpsFacadeSRV#getUtenteProtocollatore(it.ibm.red.business.dto.ProtocolloNpsDTO,
	 *      boolean).
	 */
	@Override
	public UtenteDTO getUtenteProtocollatore(final ProtocolloNpsDTO protocolloNps, final boolean onlyChiaveEsterna) {

		Long idUtente = null;
		Long idNodo = null;
		final String chiaveEsternaProtocollatore = protocolloNps.getChiaveEsternaProtocollatore();
		if (!StringUtils.isNullOrEmpty(chiaveEsternaProtocollatore)) {
			final String[] splitted = org.apache.commons.lang3.StringUtils.split(chiaveEsternaProtocollatore, ".");
			try {
				if (splitted.length == 2) {
					idUtente = Long.parseLong(splitted[1]);
					idNodo = Long.parseLong(splitted[0]);
				}
			} catch (final Exception e) {
				LOGGER.warn(e);
				idUtente = null;
				idNodo = null;
			}
		}

		UtenteDTO utenteReturn = null;
		if (idUtente != null && idUtente != 0L && idNodo != null && idNodo != 0L) {
			LOGGER.info("getUtenteProtocollatore -> chiave esterna del protocollatore: " + chiaveEsternaProtocollatore);
			utenteReturn = utenteSRV.getById(idUtente, null, idNodo);
			if (utenteReturn == null) {
				utenteReturn = utenteSRV.getById(idUtente, null, null);
			}
		}

		if (utenteReturn != null) {
			return utenteReturn;
		} else if (!onlyChiaveEsterna && protocolloNps.getProtocollatore() != null && !StringUtils.isNullOrEmpty(protocolloNps.getProtocollatore().getCodiceFiscalePF())) {
			LOGGER.info("getUtenteProtocollatore -> Codice fiscale del protocollatore: " + protocolloNps.getProtocollatore().getCodiceFiscalePF());
			return utenteSRV.getByCodiceFiscale(protocolloNps.getProtocollatore().getCodiceFiscalePF());
		} else if (!onlyChiaveEsterna && !StringUtils.isNullOrEmpty(protocolloNps.getCodiceAoo())) {
			LOGGER.info("getUtenteProtocollatore -> Codice AOO: " + protocolloNps.getCodiceAoo());
			final Integer idAoo = tipologiaDocumentoSRV.getIdAOOByCodiceNPS(protocolloNps.getCodiceAoo());
			final Aoo aoo = aooSRV.recuperaAoo(idAoo);
			final String usernameProtocollatore = PropertiesProvider.getIstance()
					.getParameterByString(aoo.getCodiceAoo() + "." + PropertiesNameEnum.USERNAME_PROTOCOLLATORE_NPS.getKey());
			final String ufficioProtocollatore = PropertiesProvider.getIstance()
					.getParameterByString(aoo.getCodiceAoo() + "." + PropertiesNameEnum.NODO_PROTOCOLLATORE_NPS.getKey());
			LOGGER.info("getUtenteProtocollatore -> Username del protocollatore: " + usernameProtocollatore);
			LOGGER.info("getUfficioProtocollatore -> Ufficio del protocollatore: " + ufficioProtocollatore);
			if (!StringUtils.isNullOrEmpty(ufficioProtocollatore)) {
				try {
					idNodo = Long.parseLong(ufficioProtocollatore);

					final UtenteDTO utenteDTO = utenteSRV.getByUsername(usernameProtocollatore, null, idNodo);
					if (utenteDTO != null) {
						return utenteDTO;
					}

					LOGGER.info("getUfficioProtocollatore -> Ufficio del protocollatore configurato in maniera errata (utente non configurato per l'ufficio): "
							+ ufficioProtocollatore);
				} catch (final NumberFormatException e) {
					LOGGER.info("getUfficioProtocollatore -> Ufficio del protocollatore configurato in maniera errata (valore errato): " + ufficioProtocollatore);
				}
			}
			return utenteSRV.getByUsername(usernameProtocollatore);
		} else {
			throw new RedException("getUtenteProtocollatore ->  Errore nel recupero dell'utente protocollatore a partire dal codice fiscale: "
					+ "informazione NON presente nel protocollo NPS in input");
		}
	}

	/**
	 * @see it.ibm.red.business.service.INpsSRV#inviaMessaggioPosta(java.lang.Integer,
	 *      java.lang.String, java.lang.String, java.lang.String, java.util.List,
	 *      java.util.List, java.lang.String, java.lang.String, int,
	 *      java.lang.Boolean, java.sql.Connection).
	 */
	@Override
	public String inviaMessaggioPosta(final Integer idNotifica, final String mailMittente, final String testoMessaggio, final String corpoMessaggio,
			final List<String> indirizzoMailDest, final List<String> idDocumentiNps, final String isUfficiale, final String isPec, final int idAoo,
			final Boolean notificaSpedizione, final Connection connection) {
		String msgId = null;
		try {
			final NpsConfiguration npsConf = npsConfigurationDAO.getByIdAoo(idAoo, connection);

			final RispostaInviaMessaggioPostaType responseNps = BusinessDelegate.Posta.inviaMessaggioPosta(idAoo, /* codAoo */npsConf.getCodiceAoo(), mailMittente,
					/* isPec */isPec, /* ufficiale */isUfficiale, /* messaggio */testoMessaggio, /* corpoMessaggio */ corpoMessaggio,
					/* List<String> indirizzoMailDest */indirizzoMailDest, /* List<String> allegatiGuid */ idDocumentiNps, /* notificaSpedzione */notificaSpedizione);

			if (EsitoType.OK.equals(responseNps.getEsito())) {
				msgId = responseNps.getMessageId();
				gestioneNotificheEmailDAO.updateCodiMessageGestioneNotificheEmail(responseNps.getMessageId(), idNotifica.longValue(), connection);
			} else {
				if(!CollectionUtils.isEmpty(responseNps.getErrorList())) { 
					for(ServiceErrorType error : responseNps.getErrorList()) { 
						if(error.getErrorMessageString()!=null) {
							LOGGER.error("Errore durante l'invio del messaggio di posta su nps" + error.getErrorMessageString());
						}
					}
					
				} 
			}
		} catch (final Exception ex) {
			LOGGER.error("Errore durante l'invio del messaggio di posta su nps", ex);
			throw new RedException(ex);
		}

		return msgId;
	}

	/**
	 * @see it.ibm.red.business.service.INpsSRV#getAll().
	 */
	@Override
	public List<NpsConfiguration> getAll() {
		List<NpsConfiguration> output = new ArrayList<>();
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), true);

			output = npsConfigurationDAO.getAll(connection);

			commitConnection(connection);

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di lettura configurazioni NPS", e);
			rollbackConnection(connection);
		} finally {
			closeConnection(connection);
		}
		return output;
	}
	
	private static ProtocolloNpsDTO fillBaseProt(final ProtRisultatoRicercaProtocolloType risultatoRicercaProtocolli) {
		ProtocolloNpsDTO protocolloTrovato = new ProtocolloNpsDTO();
		protocolloTrovato.setOggetto(risultatoRicercaProtocolli.getBase().getOggetto());
		protocolloTrovato.setIdProtocollo(risultatoRicercaProtocolli.getBase().getIdProtocollo());
		protocolloTrovato.setNumeroProtocollo(Integer.parseInt(risultatoRicercaProtocolli.getBase().getNumeroRegistrazione()));
		protocolloTrovato.setDataProtocollo(risultatoRicercaProtocolli.getBase().getDataRegistrazione().toGregorianCalendar().getTime());
		protocolloTrovato.setTipoProtocollo(risultatoRicercaProtocolli.getBase().getTipoProtocollo().toString());
		protocolloTrovato.setAnnoProtocollo(
				String.valueOf(risultatoRicercaProtocolli.getBase().getDataRegistrazione().getYear()));
		protocolloTrovato.setNumeroProtocolloAnnoProtocollo(
				protocolloTrovato.getNumeroProtocollo() + " / " + protocolloTrovato.getAnnoProtocollo());
		protocolloTrovato.setDestinatariString(risultatoRicercaProtocolli.getBase().getDestinatario());
		protocolloTrovato.setMittenteString(risultatoRicercaProtocolli.getBase().getMittente());
		protocolloTrovato.setStatoProtocollo(risultatoRicercaProtocolli.getBase().getStato().replace("_", " ").trim());
		protocolloTrovato.setEntrataProtocollo(ProtTipoProtocolloType.ENTRATA.toString().equalsIgnoreCase(protocolloTrovato.getTipoProtocollo()));
		protocolloTrovato.setSistemaProduttore(risultatoRicercaProtocolli.getBase().getSistemaProduttore());
		return protocolloTrovato;
	}
	
	private static ProtocolloNpsDTO fillEntrataProt(ProtRisultatoRicercaProtocolloType risultatoRicercaProtocolli) {
		ProtocolloNpsDTO protocolloTrovato = new ProtocolloNpsDTO();
		protocolloTrovato.setOggetto(risultatoRicercaProtocolli.getEntrata().getOggetto());
		protocolloTrovato.setIdProtocollo(risultatoRicercaProtocolli.getEntrata().getIdProtocollo());
		protocolloTrovato.setNumeroProtocollo(risultatoRicercaProtocolli.getEntrata().getIdentificatoreProtocollo().getNumeroRegistrazione());
		protocolloTrovato.setDataProtocollo(risultatoRicercaProtocolli.getEntrata().getIdentificatoreProtocollo().getDataRegistrazione().toGregorianCalendar().getTime());
		protocolloTrovato.setTipoProtocollo(risultatoRicercaProtocolli.getEntrata().getIdentificatoreProtocollo().getTipoProtocollo().toString());
		protocolloTrovato.setAnnoProtocollo(
				String.valueOf(risultatoRicercaProtocolli.getEntrata().getIdentificatoreProtocollo().getDataRegistrazione().getYear()));
		protocolloTrovato.setNumeroProtocolloAnnoProtocollo(
				protocolloTrovato.getNumeroProtocollo() + " / " + protocolloTrovato.getAnnoProtocollo());
		
		if(risultatoRicercaProtocolli.getEntrata().getDestinatari().get(0).getPersonaFisica()!=null) {
			String nome = risultatoRicercaProtocolli.getEntrata().getDestinatari().get(0).getPersonaFisica().getNome();
			String cognome = risultatoRicercaProtocolli.getEntrata().getDestinatari().get(0).getPersonaFisica().getCognome();
			protocolloTrovato.setDestinatariString(nome + " " + cognome);
		} else if(risultatoRicercaProtocolli.getEntrata().getDestinatari().get(0).getPersonaGiuridica()!=null) {
			String denominazione = risultatoRicercaProtocolli.getEntrata().getDestinatari().get(0).getPersonaGiuridica().getDenominazione();
			protocolloTrovato.setDestinatariString(denominazione);
		}
		
		if(risultatoRicercaProtocolli.getEntrata().getMittente().getPersonaFisica()!=null) {
			String nome = risultatoRicercaProtocolli.getEntrata().getMittente().getPersonaFisica().getNome();
			String cognome = risultatoRicercaProtocolli.getEntrata().getMittente().getPersonaFisica().getCognome();
			protocolloTrovato.setMittenteString(nome + " " + cognome);
		} else if(risultatoRicercaProtocolli.getEntrata().getMittente().getPersonaGiuridica()!=null) {
			String denominazione = risultatoRicercaProtocolli.getEntrata().getMittente().getPersonaGiuridica().getDenominazione();
			protocolloTrovato.setMittenteString(denominazione);
		}
		 
		protocolloTrovato.setStatoProtocollo(risultatoRicercaProtocolli.getEntrata().getStato().replace("_", " ").trim());
		protocolloTrovato.setEntrataProtocollo(true);
		protocolloTrovato.setSistemaProduttore(risultatoRicercaProtocolli.getEntrata().getSistemaProduttore());
		return protocolloTrovato;
	}
	
	private static ProtocolloNpsDTO fillUscitaProt(ProtRisultatoRicercaProtocolloType risultatoRicercaProtocolli) {
		ProtocolloNpsDTO protocolloTrovato = new ProtocolloNpsDTO();
		protocolloTrovato.setOggetto(risultatoRicercaProtocolli.getUscita().getOggetto());
		protocolloTrovato.setIdProtocollo(risultatoRicercaProtocolli.getUscita().getIdProtocollo());
		protocolloTrovato.setNumeroProtocollo(risultatoRicercaProtocolli.getUscita().getIdentificatoreProtocollo().getNumeroRegistrazione());
		protocolloTrovato.setDataProtocollo(risultatoRicercaProtocolli.getUscita().getIdentificatoreProtocollo().getDataRegistrazione().toGregorianCalendar().getTime());
		protocolloTrovato.setTipoProtocollo(risultatoRicercaProtocolli.getUscita().getIdentificatoreProtocollo().getTipoProtocollo().toString());
		protocolloTrovato.setAnnoProtocollo(
				String.valueOf(risultatoRicercaProtocolli.getUscita().getIdentificatoreProtocollo().getDataRegistrazione().getYear()));
		protocolloTrovato.setNumeroProtocolloAnnoProtocollo(
				protocolloTrovato.getNumeroProtocollo() + " / " + protocolloTrovato.getAnnoProtocollo());
		
		if(risultatoRicercaProtocolli.getUscita().getDestinatari().get(0).getPersonaFisica()!=null) {
			String nome = risultatoRicercaProtocolli.getUscita().getDestinatari().get(0).getPersonaFisica().getNome();
			String cognome = risultatoRicercaProtocolli.getUscita().getDestinatari().get(0).getPersonaFisica().getCognome();
			protocolloTrovato.setDestinatariString(nome + " " + cognome);
		} else if(risultatoRicercaProtocolli.getUscita().getDestinatari().get(0).getPersonaGiuridica()!=null) {
			String denominazione = risultatoRicercaProtocolli.getUscita().getDestinatari().get(0).getPersonaGiuridica().getDenominazione();
			protocolloTrovato.setDestinatariString(denominazione);
		}
		
		if(risultatoRicercaProtocolli.getUscita().getMittente().getPersonaFisica()!=null) {
			String nome = risultatoRicercaProtocolli.getUscita().getMittente().getPersonaFisica().getNome();
			String cognome = risultatoRicercaProtocolli.getUscita().getMittente().getPersonaFisica().getCognome();
			protocolloTrovato.setMittenteString(nome + " " + cognome);
		} else if(risultatoRicercaProtocolli.getUscita().getMittente().getPersonaGiuridica()!=null) {
			String denominazione = risultatoRicercaProtocolli.getUscita().getMittente().getPersonaGiuridica().getDenominazione();
			protocolloTrovato.setMittenteString(denominazione);
		}
		 
		protocolloTrovato.setStatoProtocollo(risultatoRicercaProtocolli.getUscita().getStato().replace("_", " ").trim());
		protocolloTrovato.setEntrataProtocollo(false);
		protocolloTrovato.setSistemaProduttore(risultatoRicercaProtocolli.getUscita().getSistemaProduttore());
		return protocolloTrovato;
	}
	
	/**
	 * @param idProtocollo
	 * @param infoProtocollo
	 * @param utente
	 * @param stato
	 * @param con
	 * @return
	 */
	@Override
	public int cambiaStatoProtUscitaToSpedito(Aoo aoo, MessaggioEmailDTO msgNext) {
		Connection conn = null;
		int out = -1;
		try {
			conn = setupConnection(getDataSource().getConnection(), true); 

			// inizializza accesso a CE filenet
			AooFilenet aooFilenet = aoo.getAooFilenet();
			IFilenetCEHelper fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(), aooFilenet.getObjectStore(),aoo.getIdAoo());
			Document document = fceh.getDocumentByDTandAOO(msgNext.getDocumentTitle(), aoo.getIdAoo());
			String idProtocollo = (String)TrasformerCE.getMetadato(document, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY);
			if(!StringUtils.isNullOrEmpty(idProtocollo)) {
				String idDocumento = (String)TrasformerCE.getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
				   
				UtenteDTO utente = utenteSRV.getById(msgNext.getIdUtenteMittente());
				Nodo ufficio = nodoDAO.getNodo(msgNext.getIdNodoMittente(), conn);
				utente.setIdUfficio(msgNext.getIdNodoMittente());
				utente.setCodiceUfficio(ufficio.getCodiceNodo());
				utente.setNodoDesc(ufficio.getDescrizione());
				utente.setAooDesc(aoo.getDescrizione());
				utente.setCodiceAoo(aoo.getCodiceAoo());
				 
				String infoProtocollo = new StringBuilder()
						.append(document.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY))).append("/")
						.append(document.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY)))
						.append("_").append(aoo.getCodiceAoo()).toString();
				 
				out = cambiaStatoProtUscita(idProtocollo, infoProtocollo, utente, Builder.SPEDITO, idDocumento, conn);
			}
			
			commitConnection(conn);
		} catch (Exception e) {
			LOGGER.error("Errore in fase di cambio stato prot uscita spedito ", e);
			rollbackConnection(conn);
		} finally {
			closeConnection(conn);
		}
		return out;
	
	}

	/**
	 * @see it.ibm.red.business.service.INpsSRV#aggiornaRiservatezza(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String,
	 *      it.ibm.red.business.dto.ListSecurityDTO, boolean, java.lang.String).
	 */
	@Override
	public boolean aggiornaRiservatezza(final UtenteDTO utente, final String infoProtocollo, final String idProtocollo, 
			final ListSecurityDTO securityList, final boolean isRiservato, final String idDocumento) {
		Connection conn = null;
		boolean aggiornamentoAcl = false;
		IFilenetCEHelper fceh = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), true);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			
			String acl = getACLFromSecurity(securityList, fceh);
			aggiornamentoAcl = aggiornaDatiProtocollo(utente, infoProtocollo, idProtocollo, null, null, null, conn, acl, isRiservato, idDocumento, null);

			commitConnection(conn);
		} catch (final Exception ex) {
			rollbackConnection(conn);
			LOGGER.error("Errore durante l'aggiornamento delle acl su nps " + idProtocollo, ex);
			throw new RedException(ex);
		} finally {
			if (fceh != null) {
				fceh.popSubject();
			}
			closeConnection(conn);
		}

		return aggiornamentoAcl;
	}
}