package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;


/**
 * The Interface ISecurityFacadeSRV.
 *
 * @author CPIERASC
 * 
 *         Facade servizio gestione security filenet.
 */
public interface ISecurityFacadeSRV extends Serializable {
	
	/**
	 * Modifica security fascicolo.
	 * 
	 * @param utente		credenziali utente
	 * @param securities	security
	 * @param idDocumento	identificativo documento
	 * @return				risultato operazione
	 */
	boolean modificaSecurityFascicoli(UtenteDTO utente, ListSecurityDTO securities, String idDocumento);

	/**
	 * Metodo per aggiornamento security.
	 * 
	 * @param utente		credenziali utente
	 * @param idDocumento	identificativo documento
	 * @param folderName	folder name
	 * @param securities	security
	 * @param allVersion	flag tutte le versioni
	 */
	void updateSecurity(UtenteDTO utente, String idDocumento, String folderName, ListSecurityDTO securities, Boolean allVersion);

	/**
	 * Ottiene le assegnazioni degli utenti delegati.
	 * @param assegnazione
	 * @param connection
	 * @return lista di assegnazioni
	 */
	List<String> getAssegnazioneUtentiDelegati(String assegnazione, Connection connection);

	/**
	 * Ottiene le securities delle assegnazioni.
	 * @param assegnazioniArray
	 * @param isDocumento
	 * @param addUfficio
	 * @param isRiservato
	 * @param connection
	 * @return lista delle securities
	 */
	ListSecurityDTO getSecurityAssegnazioni(String[] assegnazioniArray, boolean isDocumento, boolean addUfficio, boolean isRiservato, Connection connection);

	/**
	 * Ottiene le assegnazioni degli utenti delegati.
	 * @param assegnazioniPerDelegato
	 * @param connection
	 * @return assegnazioni
	 */
	String[] getAssegnazioniUtentiDelegati(String[] assegnazioniPerDelegato, Connection connection);

	/**
	 * @param assList
	 * @param fceh
	 * @param connection
	 * @return
	 */
	String getACLDocRiservatoPerRicercaAvanzata(List<AssegnazioneDTO> assList, IFilenetCEHelper fceh,
			Connection connection);
}
