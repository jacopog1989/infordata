package it.ibm.red.business.persistence.model;

import java.io.Serializable;

/**
 * The Class NpsConfiguration.
 *
 * @author CPIERASC
 * 
 *         Entità che mappa la tabella NPS_CONFIGURATION.
 */
public class NpsConfiguration implements Serializable {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Identificativo aoo.
	 */
	private final int idAoo;

	/**
	 * Codice aoo.
	 */
	private final String codiceAoo;

	/**
	 * Denominazione aoo.
	 */
	private final String denominazioneAoo;

	/**
	 * Codice amministrazione.
	 */
	private final String codiceAmministrazione;

	/**
	 * Denominazione amministrazione.
	 */
	private final String denominazioneAmministrazione;

	/**
	 * Registro ufficiale.
	 */
	private final String registroUfficiale;

	/**
	 * Denominazione registro.
	 */
	private final String  denominazioneRegistro;

	/**
	 * Utente
	 */
	private String utente;

	/**
	 * Client
	 */
	private String client;

	/**
	 * Password
	 */
	private String password;

	/**
	 * Servizio
	 */
	private String servizio;

	/**
	 * actor
	 */
	private String actor;

	/**
	 * Codice Applicazione.
	 */
	private String applicazione;
	
	/**
	 * utenteAdmin
	 */
	private String utenteAdmin;
	
	/**
	 * passwordAdmin
	 */
	private String passwordAdmin;
	
	/**
	 * servizioAdmin
	 */
	private String servizioAdmin;
	
	/**
	 * Costruttore.
	 * 
	 * @param inIdAoo
	 *            identificativo aoo
	 * @param inCodiceAoo
	 *            codice aoo
	 * @param inDenominazioneAoo
	 *            denominazione aoo
	 * @param inCodiceAmministrazione
	 *            codice amministrazione
	 * @param inDenominazioneAmministrazione
	 *            denominazione amministrazione
	 * @param inRegistroUfficiale
	 *            registro ufficiale
	 * @param inDenominazioneRegistro
	 *            denominazione registro
	 * @param inUtente
	 *            utente
	 * @param inClient
	 *            client
	 * @param inPassword
	 *            password
	 * @param inServizio
	 *            servizio
	 * @param inActor
	 *            actor
	 * @param inApplicazione
	 *            inApplicazione
	 */
	public NpsConfiguration(final int inIdAoo, final String inCodiceAoo, final String inDenominazioneAoo, final String inCodiceAmministrazione, 
			final String inDenominazioneAmministrazione, final String inRegistroUfficiale, final String inDenominazioneRegistro, 
			final String inUtente, final String inClient, final String inPassword, final String inServizio, final String inActor, final String inApplicazione,
			final String inUtenteAdmin,final String inPasswordAdmin,final String inServizioAdmin) {
		super();
		this.idAoo = inIdAoo;
		this.codiceAoo = inCodiceAoo;
		this.denominazioneAoo = inDenominazioneAoo;
		this.codiceAmministrazione = inCodiceAmministrazione;
		this.denominazioneAmministrazione = inDenominazioneAmministrazione;
		this.registroUfficiale = inRegistroUfficiale;
		this.denominazioneRegistro = inDenominazioneRegistro;
		this.utente = inUtente;
		this.client = inClient;
		this.password = inPassword;
		this.servizio = inServizio;
		this.actor = inActor;
		this.applicazione = inApplicazione;
		this.utenteAdmin = inUtenteAdmin;
		this.passwordAdmin = inPasswordAdmin;
		this.servizioAdmin = inServizioAdmin;
	}
	
	/**
	 * Getter id aoo.
	 * 
	 * @return	id aoo
	 */
	public final int getIdAoo() {
		return idAoo;
	}

	/**
	 * Getter codice aoo.
	 * 
	 * @return	codice aoo
	 */
	public final String getCodiceAoo() {
		return codiceAoo;
	}

	/**
	 * Getter denominazione.
	 * 
	 * @return	denominazione
	 */
	public final String getDenominazioneAoo() {
		return denominazioneAoo;
	}

	/**
	 * Getter codice amministrazione.
	 * 
	 * @return	codice amministrazione
	 */
	public final String getCodiceAmministrazione() {
		return codiceAmministrazione;
	}

	/**
	 * Getter denominiazione amministrazione.
	 * 
	 * @return	denominiazione amministrazione
	 */
	public final String getDenominazioneAmministrazione() {
		return denominazioneAmministrazione;
	}

	/**
	 * Getter registro ufficiale.
	 * 
	 * @return	registro ufficiale
	 */
	public final String getRegistroUfficiale() {
		return registroUfficiale;
	}

	/**
	 * Getter denominazione registro.
	 * 
	 * @return	denominazione registro
	 */
	public final String getDenominazioneRegistro() {
		return denominazioneRegistro;
	}

	/**
	 * Getter utente.
	 *
	 * @return the utente
	 */
	public String getUtente() {
		return utente;
	}

	/**
	 * Setter utente.
	 *
	 * @param utente the new utente
	 */
	public void setUtente(final String utente) {
		this.utente = utente;
	}

	/**
	 * Getter client.
	 *
	 * @return the client
	 */
	public String getClient() {
		return client;
	}

	/**
	 * Setter client.
	 *
	 * @param client the new client
	 */
	public void setClient(final String client) {
		this.client = client;
	}

	/**
	 * Getter password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Setter password.
	 *
	 * @param password the new password
	 */
	public void setPassword(final String password) {
		this.password = password;
	}

	/**
	 * Getter servizio.
	 *
	 * @return the servizio
	 */
	public String getServizio() {
		return servizio;
	}

	/**
	 * Setter servizio.
	 *
	 * @param servizio the new servizio
	 */
	public void setServizio(final String servizio) {
		this.servizio = servizio;
	}

	/**
	 * Getter actor.
	 *
	 * @return the actor
	 */
	public String getActor() {
		return actor;
	}

	/**
	 * Setter actor.
	 *
	 * @param actor the new actor
	 */
	public void setActor(final String actor) {
		this.actor = actor;
	}

	/**
	 * Getter applicazione.
	 *
	 * @return the applicazione
	 */
	public String getApplicazione() {
		return applicazione;
	}

	/**
	 * Setter applicazione.
	 *
	 * @param applicazione the new applicazione
	 */
	public void setApplicazione(final String applicazione) {
		this.applicazione = applicazione;
	}

	/**
	 * Getter utenteAdmin.
	 *
	 * @return the utenteAdmin
	 */
	public String getUtenteAdmin() {
		return utenteAdmin;
	}

	/**
	 * Setter utenteAdmin.
	 *
	 * @param utenteAdmin the new utenteAdmin
	 */
	public void setUtenteAdmin(String utenteAdmin) {
		this.utenteAdmin = utenteAdmin;
	}

	/**
	 * Getter passwordAdmin.
	 *
	 * @return the passwordAdmin
	 */
	public String getPasswordAdmin() {
		return passwordAdmin;
	}

	/**
	 * Setter passwordAdmin.
	 *
	 * @param passwordAdmin the new passwordAdmin
	 */
	public void setPasswordAdmin(String passwordAdmin) {
		this.passwordAdmin = passwordAdmin;
	}

	/**
	 * Getter servizioAdmin.
	 *
	 * @return the servizioAdmin
	 */
	public String getServizioAdmin() {
		return servizioAdmin;
	}

	/**
	 * Setter servizioAdmin.
	 *
	 * @param servizioAdmin the new servizioAdmin
	 */
	public void setServizioAdmin(String servizioAdmin) {
		this.servizioAdmin = servizioAdmin;
	}
	
	
}
