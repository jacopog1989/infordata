package it.ibm.red.business.helper.timeline;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.faces.context.FacesContext;

import flexjson.JSONSerializer;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.ApprovazioneDTO;
import it.ibm.red.business.dto.StepComponentDTO;
import it.ibm.red.business.dto.filenet.EventLogDTO;
import it.ibm.red.business.dto.filenet.StepDTO;
import it.ibm.red.business.enums.StepTypeEnum;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class TimelineHelper.
 *
 * @author CPIERASC
 * 
 *         Classe per la gestione della firma remota e la verifica.
 */
public final class TimelineHelper implements Serializable {
	
	private static final String RIFIUTATO_CLASS = "rifiutato-class";

	/**
	 * Tag iniziale immagine.
	 */
	private static final String START_IMG_TAG_STORICO = "<img class='img-storico-class' src='";

	/**
	 * Tag finale immagine.
	 */
	private static final String END_IMG_TAG_STORICO = "'/>";

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Step storico.
	 */
	private transient List<StepComponentDTO> data;
	
	/**
	 * Step approvazioni.
	 */
	private transient List<StepComponentDTO> dataApprovazioni;
	
	/**
	 * Per RED gli url alle icone devono essere preceduti da '../' altrimenti non si vede nessuna immagine .
	 */
	private String requestContextPath;
	
	
	/**
	 * Costruttore.
	 *
	 * @param steps
	 *            step storico visuale
	 * @param descrizioneIter
	 *            descrizione iter
	 * @param inStepsApprovazioni
	 *            lista step storico
	 */
	public TimelineHelper(final List<StepDTO> steps, final String descrizioneIter, final List<ApprovazioneDTO> inStepsApprovazioni) {
		data = new ArrayList<>();
		requestContextPath = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
		
        if (!steps.isEmpty()) {
        	String descIter = Constants.EMPTY_STRING;
        	if (!StringUtils.isNullOrEmpty(descrizioneIter)) {
        		if (descrizioneIter.toUpperCase().contains("AVVIO PROCEDIMENTO")) {
        			descIter = descrizioneIter.toUpperCase();
        		} else {
        			descIter = "AVVIO PROCEDIMENTO " + descrizioneIter.toUpperCase();
				}
        	}
        	addStep(new StepDTO(StepTypeEnum.INIZIO, new EventLogDTO(descIter)));
        }
		
		for (StepDTO step : steps) {
			addStep(step);
		}
		
		updateApprovazioni(inStepsApprovazioni);
	}
	
	
	/**
	 * Metodo per aggiungere allo storico uno step.
	 * 
	 * @param step	step da aggiungere
	 */
	private void addStep(final StepDTO step) {
		String picto = null;
		String cssClass = null;
		
		boolean rifiutoContributo = false;
		boolean eliminaContributo = "ELIMINAZIONE CONTRIBUTO".equalsIgnoreCase(step.getHeader().getDescrizione());

		String shortContent = "<table class='table-content'><tr><th>" + step.getHeader().toStringEventLog(false) + "</th></tr>";
		StringBuilder fullContent = new StringBuilder(shortContent);
	 
		for (EventLogDTO row : step.getBody()) {
			fullContent.append("<tr><td>" + row.toStringEventLog(true) + "</td></tr>");
			// Anche se l'operazione è completata, il box di richiesta contributo dev'essere rosso se questa viene rifiutata.
			// Questo booleano serve a indicare questo nell'istruzione che assegna la classe del colore.
			rifiutoContributo = "RICHIESTA CONTRIBUTO RIFIUTATA".equalsIgnoreCase(row.getDescrizione());
		} 	
		
		fullContent.append("</table>"); 
		String label = step.getLabel();
		String showMore = "";
		String showLess = "";
		String position = "left";
		if (!step.getBody().isEmpty()) {
			position = "right";
			showMore = "<img src='" + requestContextPath + "/resources/images/white_add.png' class='showMore img-show' onclick='tlShowMore(this)'/>";
			showLess = "<img src='" + requestContextPath + "/resources/images/white_minus.png' class='showLess img-show' onclick='tlShowLess(this)' />";
		}

//		<table style="width:100%"><tbody><tr><th> UCP - RGS, Renzo Asci RICHIESTA CONTRIBUTO UCP - RGS Irene Pellegrini</th></tr><tr><td>24/04/2019 10:44:59 UCP - RGS, Renzo Asci RICHIESTA CONTRIBUTO UCP - RGS Irene Pellegrini</td></tr><tr><td>24/04/2019 10:44:59 UCP - RGS, Renzo Asci RICHIESTA CONTRIBUTO UCP - RGS Irene Pellegrini</td></tr></tbody></table>
		
		if (StepTypeEnum.COMPLETATO.equals(step.getStepType())) {
			picto = START_IMG_TAG_STORICO + requestContextPath + "/resources/images/storico_completato.png" + END_IMG_TAG_STORICO;
			
			if (rifiutoContributo || eliminaContributo) {
				// L'operazione è completata, ma va mostrata di colore rosso
				cssClass = RIFIUTATO_CLASS;
				
				if (eliminaContributo) {
					position = "right";
				}
			} else {
				cssClass = "completato-class";
			} 
		} else if (StepTypeEnum.IN_CORSO.equals(step.getStepType())) {
			picto = START_IMG_TAG_STORICO + requestContextPath +  "/resources/images/storico_in_corso.png" + END_IMG_TAG_STORICO;
			cssClass = "in-corso-class";
		} else if (StepTypeEnum.RIFIUTATO.equals(step.getStepType())) {
			picto = START_IMG_TAG_STORICO + requestContextPath + "/resources/images/storico_rifiutato.png" + END_IMG_TAG_STORICO;
			cssClass = RIFIUTATO_CLASS;
		} else if (StepTypeEnum.IN_PAUSA.equals(step.getStepType())) {
			picto = START_IMG_TAG_STORICO + requestContextPath + "/resources/images/storico_in_pausa.png" + END_IMG_TAG_STORICO;
			cssClass = "in-pausa-class";
		} else if (StepTypeEnum.PROCEDIMENTO_CONCLUSO.equals(step.getStepType())) {
			cssClass = "procedimento-concluso-class";
		} else if (StepTypeEnum.DA_COMPLETARE.equals(step.getStepType())) {
			cssClass = "da-completare-class";
		} else if (StepTypeEnum.INIZIO.equals(step.getStepType())) {
			cssClass = "inizio-class";
		}
		StepComponentDTO stepComponent = new StepComponentDTO("smallItem", label, shortContent, fullContent.toString(), showMore, showLess, picto, cssClass, position);
		data.add(stepComponent);
	}
	
	
	/**
	 * Aggiornamento lista step approvazioni.
	 * 	
	 * @param stepsApprovazioni	lista step approvazioni
	 */
	private void updateApprovazioni(final List<ApprovazioneDTO> stepsApprovazioni) {
		dataApprovazioni = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String pictoOk = START_IMG_TAG_STORICO + requestContextPath + "/resources/images/storico_completato.png" + END_IMG_TAG_STORICO;
		String cssClassOk = "completato-class";
		String pictoKo = START_IMG_TAG_STORICO + requestContextPath + "/resources/images/storico_rifiutato.png" + END_IMG_TAG_STORICO;
		String cssClassKo = RIFIUTATO_CLASS;
		String pictoCondizionato = START_IMG_TAG_STORICO + requestContextPath + "/resources/images/storico_in_corso.png" + END_IMG_TAG_STORICO;
		String cssClassCondizionato = "in-corso-class";

		for (ApprovazioneDTO step:stepsApprovazioni) {
			String label = "<div class='ui-widget-content myLabel'>" + sdf.format(step.getDataApprovazione()) + "</div>";
			String content = step.getDescApprovazione() + " [" + step.getDescUfficio() + "/" + step.getDescUtente() + "]";
			String picto = pictoOk;
			String cssClass = cssClassOk;
			if (step.getDescApprovazione().toLowerCase().contains("negativo")) {
				picto = pictoKo;
				cssClass = cssClassKo;
			} else if (step.getDescApprovazione().toLowerCase().contains("condizionato")) {
				picto = pictoCondizionato;
				cssClass = cssClassCondizionato;
			}
			dataApprovazioni.add(new StepComponentDTO("smallItem", label, content, null, null, null, picto, cssClass, null));
		}
	}

	
	/**
	 * Metodo per la trasformazione in json dell'array di step.
	 * 
	 * @return	stringa
	 */
	public String getJSON() {
		return new JSONSerializer().serialize(data.toArray());
	}
	

	/**
	 * Getter.
	 * 
	 * @return	json per la rappresentazione grafica delle approvazioni
	 */
	public String getApprovazioniJSON() {
		return new JSONSerializer().serialize(dataApprovazioni.toArray());
	}
}