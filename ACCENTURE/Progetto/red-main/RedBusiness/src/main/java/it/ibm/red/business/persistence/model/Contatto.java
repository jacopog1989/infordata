package it.ibm.red.business.persistence.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import it.ibm.red.business.dto.ComuneDTO;
import it.ibm.red.business.dto.ProvinciaDTO;
import it.ibm.red.business.dto.RegioneDTO;
import it.ibm.red.business.enums.TipoRubricaEnum;
import it.ibm.red.business.enums.TipologiaIndirizzoEmailEnum;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class Contatto.
 *
 * @author CPIERASC
 * 
 *         Entità che mappa la tabella CONTATTO.
 */
public class Contatto implements Serializable {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -2241660861946314854L;

	/**
	 * Codice fiscale / Partita iva.
	 */
	private  String codiceFiscalePIva;


	/**
	 * Cognome.
	 */
	private  String cognome;

	/**
	 * Identificativo contatto.
	 */
	private  Long contattoID;

	/**
	 * Nome.
	 */
	private  String nome;

	/**
	 * Tipo persona.
	 */
	private  String tipoPersona;

	/**
	 * Mail.
	 */
	private  String mail;

	/**
	 * Pec.
	 */
	private  String mailPec;

	/**
	 * Alias.
	 */
	private String aliasContatto;

	/**
	 * Cap.
	 */
	private String cap;

	/**
	 * Cellulare.
	 */
	private String cellulare;

	/**
	 * Contatto casella postale.
	 */
	private Long contattocasellapostale;

	/**
	 * Data attivazione.
	 */
	private Date dataattivazione;

	/**
	 * Data creazione.
	 */
	private Date datacreazione;

	/**
	 * Data disattivazione.
	 */
	private Date datadisattivazione;

	/**
	 * Fax.
	 */
	private String fax;

	/**
	 * Identificativo AOO.
	 */
	private Long idAOO;

	/**
	 * Sul DB è un VARCHAR.
	 */
	private String idComuneIstat;

	/**
	 * Sul DB è un VARCHAR.
	 */
	private String idProvinciaIstat;

	/**
	 * Sul DB è un VARCHAR.
	 */
	private String idRegioneIstat;

	/**
	 * Indirizzo.
	 */
	private String indirizzo;

	/**
	 * Ipa.
	 */
	private Integer ipa;

	/**
	 * Flag pubblico.
	 */
	private Integer pubblico;

	/**
	 * Riferimento.
	 */
	private String riferimento;

	/**
	 * Telefono.
	 */
	private String telefono;

	/**
	 * Tipologia rubrica.
	 */
	private String tipoRubrica;

	/**
	 * Flag preferito.
	 */
	private Boolean isPreferito;

	/**
	 * Regione.
	 */
	private String regione;

	/**
	 * Provincia.
	 */
	private String provincia;

	/**
	 * Comune.
	 */
	private String comune;

	/**
	 * Necessario per utilizzare i contatti negli input text multipli in to e cc.
	 */
	private String idDataTable;

	/**
	 * Comune.
	 */
	private ComuneDTO comuneObj;

	/**
	 * Regione.
	 */
	private RegioneDTO regioneObj;

	/**
	 * Provincia.
	 */
	private ProvinciaDTO provinciaObj;

	/**
	 * Mail selezionata.
	 */
	private String mailSelected;
	
	/**
	 * Identificativo nodo.
	 */
	private Long idNodo;
	
	/**
	 * Identificarivo utente.
	 */
	private Long idUtente;
	
	/**
	 * Alias.
	 */
	private String aliasStringforEmail;

	/**
	 * Booleano per indicare la selezione o meno dell'oggetto quando usato per la presentation.
	 */
	private Boolean selected;
	
	/**
	 * Booleano per indicare se si vuole disabilitare o meno l'elemento che contiene le informazioni del contatto.
	 */
	private Boolean disabilitaElemento = false;

	/**
	 * Tipo rubrica.
	 */
	private TipoRubricaEnum tipoRubricaEnum;
	
	/**
	 * Motivo esportazione.
	 */
	private String motivoEsportazione;
	
	/**
	 * Collection gruppi contato.
	 */
	private Collection<Contatto> contattiContenutiInGruppo;
	
	/**
	 * Identificativo.
	 */
	private String identificativo;

	/**
	 * Flag add to table.
	 */
	private Boolean isAddTable;
	 
	/**
	 * Flag on the fly.
	 */
	private Integer onTheFly;
	 
	/**
	 * Flag campo email visibile.
	 */
	private boolean campoEmailVisible;
	
	/**
	 * Identificativo gruppo selezionato.
	 */
	private Long idGruppoSelected;
	
	/**
	 * Nome gruppo selezionato.
	 */
	private String nomeGruppoSelected;
	
	/**
	 * Tipologia email.
	 */
	private TipologiaIndirizzoEmailEnum tipologiaEmail;
	
	/**
	 * Alias contatto.
	 */
	private String aliasContattoProtocollazione;

	/**
	 * Utente aggiunto contatto preferiti.
	 */
	private String utenteCheHaAggiuntoAiPreferiti;
	
	/**
	 * Creatore gruppo.
	 */
	private String gruppoCreatoDa;
	
	/**
	 * Data aggiunta preferiti.
	 */
	private Date dataDiAggiuntaAiPreferiti;
	
	/**
	 * Data creazione gruppo.
	 */
	private Date dataCreazioneGruppo;
	
	/**
	 * Flag dialog.
	 */
	private boolean fromDialogCreazioneModifica;

	/**
	 * Titolo.
	 */
	private String titolo;
	
	/**
	 * Flag cancella tipologia persona.
	 */
	private boolean sbiancaTipologiaPersona;
	
	/**
	 * Nodo preferit.
	 */
	private Long nodoPreferiti;

	/**
	 * Flag delete gruppi non ufficio.
	 */
	private boolean disabiitaDeleteGruppiNonUff;
	   
	
	/**
	 * Costruttore vuoto.
	 */
	public Contatto() { }

	/**
	 * Costruttore.
	 * 
	 * @param inContattoID		identificativo contatto
	 * @param inNome			nome
	 * @param inCognome			cognome
	 * @param inTipoPersona		tipo persona
	 * @param inCodiceFiscale	codice fiscale
	 * @param inMail			mail
	 * @param inMailPec			pec
	 */
	public Contatto(final Long inContattoID, final String inNome, final String inCognome, final String inTipoPersona, final String inCodiceFiscale, 
			final String inMail, final String inMailPec, final String inAliasContatto) {
		this.contattoID = inContattoID;
		this.nome = inNome;
		this.cognome = inCognome;
		this.tipoPersona = inTipoPersona;
		this.codiceFiscalePIva = inCodiceFiscale;
		this.mail = inMail;
		this.mailPec = inMailPec;
		this.aliasContatto = inAliasContatto;
	}
	
	
	/**
	 * Costruttore per nuovo contatto.
	 * 
	 * @param inNome
	 * @param inCognome
	 * @param inTipoPersona
	 * @param inCodiceFiscale
	 * @param inMail
	 * @param inMailPec
	 * @param inAliasContatto
	 * @param inPubblico
	 * @param inIdAoo
	 */
	public Contatto(final String inNome, final String inCognome, final String inTipoPersona, final String inCodiceFiscale, 
			final String inMail, final String inMailPec, final String inAliasContatto, final int inPubblico, final Long inIdAoo) {
		this.nome = inNome;
		this.cognome = inCognome;
		this.tipoPersona = inTipoPersona;
		this.codiceFiscalePIva = inCodiceFiscale;
		this.mail = inMail;
		this.mailPec = inMailPec;
		this.aliasContatto = inAliasContatto;
		this.pubblico = inPubblico;
		this.idAOO = inIdAoo;
		this.datacreazione = new Date();
	}

	/**
	 * Esegue una copia tra contatti.
	 * @param contatto
	 */
	public void copyFromContatto(final Contatto contatto) {
		this.codiceFiscalePIva = contatto.codiceFiscalePIva;
		this.cognome = contatto.cognome;
		this.contattoID = contatto.contattoID;
		this.nome = contatto.nome;
		this.tipoPersona = contatto.tipoPersona;
		this.mail = contatto.mail;
		this.mailPec = contatto.mailPec;
		this.aliasContatto = contatto.aliasContatto;
		this.cap = contatto.cap;
		this.cellulare = contatto.cellulare;
		this.contattocasellapostale = contatto.contattocasellapostale;
		this.dataattivazione = contatto.dataattivazione;
		this.datacreazione =   contatto.datacreazione;
		this.datadisattivazione = contatto.datadisattivazione;
		this.fax = contatto.fax;
		this.idAOO = contatto.idAOO;
		this.idComuneIstat = contatto.idComuneIstat;
		this.idProvinciaIstat = contatto.idProvinciaIstat;
		this.idRegioneIstat = contatto.idRegioneIstat;
		this.indirizzo = contatto.indirizzo;
		this.ipa = contatto.ipa;
		this.pubblico = contatto.pubblico;
		this.riferimento = contatto.riferimento;
		this.telefono = contatto.telefono;
		this.tipoRubrica = contatto.tipoRubrica;
		this.isPreferito = contatto.isPreferito;
		this.regione = contatto.regione;
		this.provincia = contatto.provincia;
		this.tipoRubricaEnum = contatto.tipoRubricaEnum;
		this.comune = contatto.comune;
		if (contatto.comuneObj != null) {
			final ComuneDTO copiaComune = new ComuneDTO();
			this.comuneObj = copiaComune;
			copiaComune.setIdComune(contatto.comuneObj.getIdComune());
			copiaComune.setDenominazione(contatto.comuneObj.getDenominazione());
		}
		if (contatto.regioneObj != null) {
			final RegioneDTO copiaRegione = new RegioneDTO();
			this.regioneObj = copiaRegione;
			copiaRegione.setIdRegione(contatto.regioneObj.getIdRegione());
			copiaRegione.setDenominazione(contatto.regioneObj.getDenominazione());
		}
		if (contatto.provinciaObj != null) {
			final ProvinciaDTO copiaProvincia = new ProvinciaDTO();
			this.provinciaObj = copiaProvincia;
			copiaProvincia.setIdProvincia(contatto.provinciaObj.getIdProvincia());
			copiaProvincia.setDenominazione(contatto.provinciaObj.getDenominazione());
		}
		
		if(contatto.titolo!=null) {
			this.titolo = contatto.titolo;
		} 
	}

	/**
	 * Restituisce l'indice che indica la posizione nella lista del contatto se esistente, se non presente restituisce -1.
	 * @param listaContatti
	 * @return la posizione se presente nella lista, -1 altrimenti
	 */
	public int isContattoInGruppo(final List<Contatto> listaContatti) {
		int index = -1;
			final int size = listaContatti.size();
			for (int i = 0; i < size; i++) {
				final Contatto contattoGruppo = listaContatti.get(i);
				if (contattoGruppo.getContattoID().equals(this.getContattoID())) {
					index = i;
					break;	
				}	
			}
			return index;
	}

	/**
	 * Getter.
	 * 
	 * @return	mail
	 */
	public final String getMail() {
		return mail;
	}

	/**
	 * Getter.
	 * 
	 * @return	pec
	 */
	public final String getMailPec() {
		return mailPec;
	}

	/**
	 * Getter.
	 * 
	 * @return	codice fiscale / partita iva
	 */
	public final String getCodiceFiscalePIva() {
		return codiceFiscalePIva;
	}

	/**
	 * Getter.
	 * 
	 * @return	cognome
	 */
	public final String getCognome() { 
		if("G".equals(tipoPersona)) {
			cognome = null;
			titolo = null;
		}
		return cognome;
	}

	/**
	 * Getter.
	 * 
	 * @return	identificativo contatto
	 */
	public final Long getContattoID() {
		return contattoID;
	}

	/**
	 * Getter.
	 * 
	 * @return	nome
	 */
	public final String getNome() {
		return nome;
	}

	/**
	 * Getter.
	 * 
	 * @return	tipo persona
	 */
	public final String getTipoPersona() {
		return tipoPersona;
	}

	/**
	 * Getter.
	 * 
	 * @return the aliasContatto
	 */
	public String getAliasContatto() {
		return aliasContatto;
	}


	/**
	 * @return the cAP
	 */
	public String getCAP() {
		return cap;
	}


	/**
	 * @param cAP the cAP to set
	 */
	public void setCAP(final String inCAP) {
		cap = inCAP;
	}


	/**
	 * @return the cellulare
	 */
	public String getCellulare() {
		return cellulare;
	}


	/**
	 * @param cellulare the cellulare to set
	 */
	public void setCellulare(final String cellulare) {
		this.cellulare = cellulare;
	}


	/**
	 * @return the contattocasellapostale
	 */
	public Long getContattocasellapostale() {
		return contattocasellapostale;
	}


	/**
	 * @param contattocasellapostale the contattocasellapostale to set
	 */
	public void setContattocasellapostale(final Long contattocasellapostale) {
		this.contattocasellapostale = contattocasellapostale;
	}


	/**
	 * @return the dataattivazione
	 */
	public Date getDataattivazione() {
		return dataattivazione;
	}


	/**
	 * @param dataattivazione the dataattivazione to set
	 */
	public void setDataattivazione(final Date dataattivazione) {
		this.dataattivazione = dataattivazione;
	}


	/**
	 * @return the datacreazione
	 */
	public Date getDatacreazione() {
		return datacreazione;
	}


	/**
	 * @param datacreazione the datacreazione to set
	 */
	public void setDatacreazione(final Date datacreazione) {
		this.datacreazione = datacreazione;
	}


	/**
	 * @return the datadisattivazione
	 */
	public Date getDatadisattivazione() {
		return datadisattivazione;
	}


	/**
	 * @param datadisattivazione the datadisattivazione to set
	 */
	public void setDatadisattivazione(final Date datadisattivazione) {
		this.datadisattivazione = datadisattivazione;
	}


	/**
	 * @return the fax
	 */
	public String getFax() {
		return fax;
	}


	/**
	 * @param fax the fax to set
	 */
	public void setFax(final String fax) {
		this.fax = fax;
	}


	/**
	 * @return the idAOO
	 */
	public Long getIdAOO() {
		return idAOO;
	}


	/**
	 * @param idAOO the idAOO to set
	 */
	public void setIdAOO(final Long idAOO) {
		this.idAOO = idAOO;
	}


	/**
	 * @return the idComuneIstat
	 */
	public String getIdComuneIstat() {
		return idComuneIstat;
	}


	/**
	 * @param idComuneIstat the idComuneIstat to set
	 */
	public void setIdComuneIstat(final String idComuneIstat) {
		this.idComuneIstat = idComuneIstat;
	}


	/**
	 * @return the idProvinciaIstat
	 */
	public String getIdProvinciaIstat() {
		return idProvinciaIstat;
	}


	/**
	 * @param idProvinciaIstat the idProvinciaIstat to set
	 */
	public void setIdProvinciaIstat(final String idProvinciaIstat) {
		this.idProvinciaIstat = idProvinciaIstat;
	}


	/**
	 * @return the idRegioneIstat
	 */
	public String getIdRegioneIstat() {
		return idRegioneIstat;
	}


	/**
	 * @param idRegioneIstat the idRegioneIstat to set
	 */
	public void setIdRegioneIstat(final String idRegioneIstat) {
		this.idRegioneIstat = idRegioneIstat;
	}


	/**
	 * @return the indirizzo
	 */
	public String getIndirizzo() {
		return indirizzo;
	}


	/**
	 * @param indirizzo the indirizzo to set
	 */
	public void setIndirizzo(final String indirizzo) {
		this.indirizzo = indirizzo;
	}


	/**
	 * @return the iPA
	 */
	public Integer getIPA() {
		return ipa;
	}


	/**
	 * @param iPA the iPA to set
	 */
	public void setIPA(final Integer inIPA) {
		ipa = inIPA;
	}


	/**
	 * @return the pubblico
	 */
	public Integer getPubblico() {
		return pubblico;
	}


	/**
	 * @param pubblico the pubblico to set
	 */
	public void setPubblico(final Integer pubblico) {
		this.pubblico = pubblico;
	}


	/**
	 * @return the riferimento
	 */
	public String getRiferimento() {
		return riferimento;
	}


	/**
	 * @param riferimento the riferimento to set
	 */
	public void setRiferimento(final String riferimento) {
		this.riferimento = riferimento;
	}


	/**
	 * @return the telefono
	 */
	public String getTelefono() {
		return telefono;
	}


	/**
	 * @param telefono the telefono to set
	 */
	public void setTelefono(final String telefono) {
		this.telefono = telefono;
	}
	/**
	 * @return the tipoRubrica
	 */
	public String getTipoRubrica() {
		return tipoRubrica;
	}

	/**
	 * @param tipoRubrica the tipoRubrica to set
	 */
	public void setTipoRubrica(final String tipoRubrica) {
		this.tipoRubrica = tipoRubrica;
		this.tipoRubricaEnum = TipoRubricaEnum.get(tipoRubrica);
	}


	/**
	 * @param codiceFiscalePIva the codiceFiscalePIva to set
	 */
	public void setCodiceFiscalePIva(final String codiceFiscalePIva) {
		this.codiceFiscalePIva = codiceFiscalePIva;
	}

	/**
	 * @param cognome the cognome to set
	 */
	public void setCognome(final String cognome) {
		this.cognome = cognome;
	}

	/**
	 * @param contattoID the contattoID to set
	 */
	public void setContattoID(final Long contattoID) {
		this.contattoID = contattoID;
	}

	/**
	 * @param nome the nome to set
	 */
	public void setNome(final String nome) {
		this.nome = nome;
	}

	/**
	 * @param tipoPersona the tipoPersona to set
	 */
	public void setTipoPersona(final String tipoPersona) {
		this.tipoPersona = tipoPersona; 
	}

	/**
	 * @param mail the mail to set
	 */
	public void setMail(final String mail) {
		this.mail = mail;
	}

	/**
	 * @param mailPec the mailPec to set
	 */
	public void setMailPec(final String mailPec) {
		this.mailPec = mailPec;
	}

	/**
	 * @param aliasContatto the aliasContatto to set
	 */
	public void setAliasContatto(final String aliasContatto) {
		this.aliasContatto = aliasContatto;
	}

	/**
	 * @return the isPreferito
	 */
	public Boolean getIsPreferito() {
		return isPreferito;
	}

	/**
	 * @param isPreferito the isPreferito to set
	 */
	public void setIsPreferito(final Boolean isPreferito) {
		this.isPreferito = isPreferito;
	}


	/**
	 * @return the regione
	 */
	public String getRegione() {
		return regione;
	}

	/**
	 * @param regione the regione to set
	 */
	public void setRegione(final String regione) {
		this.regione = regione;
	}

	/**
	 * @return the provincia
	 */
	public String getProvincia() {
		return provincia;
	}

	/**
	 * @param provincia the provincia to set
	 */
	public void setProvincia(final String provincia) {
		this.provincia = provincia;
	}

	/**
	 * @return the comune
	 */
	public String getComune() {
		return comune;
	}

	/**
	 * @param comune the comune to set
	 */
	public void setComune(final String comune) {
		this.comune = comune;
	}

	/**
	 * Restituisce l'id del datatable.
	 * @return idDataTable
	 */
	public String getIdDataTable() {
		return idDataTable;
	}

	/**
	 * Imposta l'id del datatable.
	 * @param idDataTable
	 */
	public void setIdDataTable(final String idDataTable) {
		this.idDataTable = idDataTable;
	}

	/**
	 * @return the comuneObj
	 */
	public ComuneDTO getComuneObj() {
		return comuneObj;
	}

	/**
	 * @param comuneObj the comuneObj to set
	 */
	public void setComuneObj(final ComuneDTO comuneObj) {
		this.comuneObj = comuneObj;
	}

	/**
	 * @return the regioneObj
	 */
	public RegioneDTO getRegioneObj() {
		return regioneObj;
	}

	/**
	 * @param regioneObj the regioneObj to set
	 */
	public void setRegioneObj(final RegioneDTO regioneObj) {
		this.regioneObj = regioneObj;
	}

	/**
	 * @return the provinciaObj
	 */
	public ProvinciaDTO getProvinciaObj() {
		return provinciaObj;
	}

	/**
	 * @param provinciaObj the provinciaObj to set
	 */
	public void setProvinciaObj(final ProvinciaDTO provinciaObj) {
		this.provinciaObj = provinciaObj;
	}

	/**
	 * Restituisce true se selezionato.
	 * @return selected
	 */
	public Boolean getSelected() {
		return selected;
	}

	/**
	 * Setter booleano selezione.
	 * 
	 * @param inSelected	booleano selezione
	 */
	public final void setSelected(final Boolean inSelected) {
		this.selected = inSelected;
	}

	/**
	 * Getter mailSelected.
	 * @return mailSelected
	 */
	public String getMailSelected() {
		return mailSelected;
	}

	/**
	 * @param mailSelected
	 */
	public void setMailSelected(final String mailSelected) {
		this.mailSelected = mailSelected;
	}

	/**
	 * Restituisce l'enum che indica il tipo rubrica, associato al contatto.
	 * @return tipoRubricaEnum
	 */
	public TipoRubricaEnum getTipoRubricaEnum() {
		return tipoRubricaEnum;
	}

	/**
	 * Imposta l'enum che indica il tipo rubrica.
	 * @param tipoRubricaEnum
	 */
	public void setTipoRubricaEnum(final TipoRubricaEnum tipoRubricaEnum) {
		this.tipoRubricaEnum = tipoRubricaEnum;
	}

	/**
	 * @return disabilitaElemento
	 */
	public Boolean getDisabilitaElemento() {
		return disabilitaElemento;
	}

	/**
	 * @param disabilitaElemento
	 */
	public void setDisabilitaElemento(final Boolean disabilitaElemento) {
		this.disabilitaElemento = disabilitaElemento;
	}

	/**
	 * Restituisce l'id del nodo associato al contatto.
	 * @return idNodo
	 */
	public Long getIdNodo() {
		return idNodo;
	}

	/**
	 * Imposta l'id del nodo.
	 * @param idNodo
	 */
	public void setIdNodo(final Long idNodo) {
		this.idNodo = idNodo;
	}

	/**
	 * Restituisce l'id dell'utente associato al contatto.
	 * @return idUtente
	 */
	public Long getIdUtente() {
		return idUtente;
	}

	/**
	 * Imposta l'id dell'utente associato al contatto.
	 * @param idUtente
	 */
	public void setIdUtente(final Long idUtente) {
		this.idUtente = idUtente;
	}

	/**
	 * Restituisce l'alias per le email.
	 * @return aliasStringForEmail
	 */
	public String getAliasStringforEmail() {
		return aliasStringforEmail;
	}

	/**
	 * @param aliasStringforEmail
	 */
	public void setAliasStringforEmail(final String aliasStringforEmail) {
		this.aliasStringforEmail = aliasStringforEmail;
	}

	/**
	 * Restituisce il motivo di esportazione.
	 * @return motivoEsportazione
	 */
	public String getMotivoEsportazione() {
		return motivoEsportazione;
	}

	/**
	 * Imposta il motivo di esportazione.
	 * @param motivoEsportazione
	 */
	public void setMotivoEsportazione(final String motivoEsportazione) {
		this.motivoEsportazione = motivoEsportazione;
	}

	/**
	 * Restituisce i contatti appertenenenti al gruppo.
	 * @return contattiContenutiInGruppo
	 */
	public Collection<Contatto> getContattiContenutiInGruppo() {
		return contattiContenutiInGruppo;
	}

	/**
	 * @param contattiContenutiInGruppo
	 */
	public void setContattiContenutiInGruppo(final Collection<Contatto> contattiContenutiInGruppo) {
		this.contattiContenutiInGruppo = contattiContenutiInGruppo;
	}

	/**
	 * Restituisce l'identificativo del contatto.
	 * @return identificativo
	 */
	public String getIdentificativo() {
		return identificativo;
	}

	/**
	 * Imposta l'identificativo.
	 * @param identificativo
	 */
	public void setIdentificativo(final String identificativo) {
		this.identificativo = identificativo;
	}

	/**
	 * Restituisce la denominazione formatta se i valori non sono null, altrimenti restituisce "-".
	 * @return denominazione
	 */
	public String getDenominazione() {
		String denominazione = "-";
		if (!StringUtils.isNullOrEmpty(this.cognome) && !StringUtils.isNullOrEmpty(this.nome)) {
			denominazione  = this.cognome + " "  + this.nome;
		} else if (StringUtils.isNullOrEmpty(this.cognome) && !StringUtils.isNullOrEmpty(this.nome)) {
			denominazione = this.nome;
		} else if (StringUtils.isNullOrEmpty(this.nome) && !StringUtils.isNullOrEmpty(this.cognome)) {
			denominazione = this.cognome;
		}
		return denominazione;
	}

	/**
	 * @return isAddTable
	 */
	public Boolean getIsAddTable() {
		return isAddTable;
	}

	/**
	 * @param isAddTable
	 */
	public void setIsAddTable(final Boolean isAddTable) {
		this.isAddTable = isAddTable;
	}

	/**
	 * Restituisce la tipologia della mail.
	 * @return tipologiaEmail
	 */
	public TipologiaIndirizzoEmailEnum getTipologiaEmail() {
		return tipologiaEmail;
	}

	/**
	 * Imposta la tipologia della mail.
	 * @param tipologiaEmail
	 */
	public void setTipologiaEmail(final TipologiaIndirizzoEmailEnum tipologiaEmail) {
		this.tipologiaEmail = tipologiaEmail;
	}

	/**
	 * @return onTheFly
	 */
	public Integer getOnTheFly() {
		if(onTheFly==null) {
			return 0;
		}
		return onTheFly;
	}

	/**
	 * Imposta il flag onTheFly.
	 * @param onTheFly
	 */
	public void setOnTheFly(final Integer onTheFly) {
		this.onTheFly = onTheFly;
	}

	/**
	 * @see java.lang.Object#toString().
	 */
	@Override
	public String toString() {
		return "Contatto [codiceFiscalePIva=" + codiceFiscalePIva + ", cognome=" + cognome + ", contattoID="
				+ contattoID + ", nome=" + nome + ", tipoPersona=" + tipoPersona + ", mail=" + mail + ", mailPec="
				+ mailPec + ", aliasContatto=" + aliasContatto + ", CAP=" + cap + ", cellulare=" + cellulare
				+ ", contattocasellapostale=" + contattocasellapostale + ", dataattivazione=" + dataattivazione
				+ ", datacreazione=" + datacreazione + ", datadisattivazione=" + datadisattivazione + ", fax=" + fax
				+ ", idAOO=" + idAOO + ", idComuneIstat=" + idComuneIstat + ", idProvinciaIstat=" + idProvinciaIstat
				+ ", idRegioneIstat=" + idRegioneIstat + ", indirizzo=" + indirizzo + ", IPA=" + ipa + ", pubblico="
				+ pubblico + ", riferimento=" + riferimento + ", telefono=" + telefono + ", tipoRubrica=" + tipoRubrica
				+ ", isPreferito=" + isPreferito + ", regione=" + regione + ", provincia=" + provincia + ", comune="
				+ comune + ", idDataTable=" + idDataTable + ", aliasStringforEmail=" + aliasStringforEmail + ", comuneObj=" + comuneObj
				+ ", regioneObj=" + regioneObj + ", provinciaObj=" + provinciaObj + ", mailSelected=" + mailSelected
				+ ", idNodo=" + idNodo + ", idUtente=" + idUtente + ", selected=" + selected + ", disabilitaElemento="
				+ disabilitaElemento + ", tipoRubricaEnum=" + tipoRubricaEnum + "]";
	}

	/**
	 * @return campoEmailVisible
	 */
	public boolean getCampoEmailVisible() {
		return campoEmailVisible;
	}

	/**
	 * Imposta il flag campo email visibile.
	 * @param campoEmailVisible
	 */
	public void setCampoEmailVisible(final boolean campoEmailVisible) {
		this.campoEmailVisible = campoEmailVisible;
	}

	/**
	 * Restituisce l'id del gruppo selezionato.
	 * @return idGruppoSelected
	 */
	public Long getIdGruppoSelected() {
		return idGruppoSelected;
	}

	/**
	 * Imposta l'id del gruppo selezionato.
	 * @param idGruppoSelected
	 */
	public void setIdGruppoSelected(final Long idGruppoSelected) {
		this.idGruppoSelected = idGruppoSelected;
	}

	/**
	 * Restituisce il nome del gruppo.
	 * @return nomeGruppoSelected
	 */
	public String getNomeGruppoSelected() {
		return nomeGruppoSelected;
	}

	/**
	 * @param nomeGruppoSelected
	 */
	public void setNomeGruppoSelected(final String nomeGruppoSelected) {
		this.nomeGruppoSelected = nomeGruppoSelected;
	}

	/**
	 * Restituisce true se l'obj in ingresso è uguale a this.
	 * @param obj
	 * @return
	 */
	public boolean checkUguale(final Object obj) {
		boolean equals = true;
		if (obj != null && getClass().equals(obj.getClass())) {
			final Contatto contatto = (Contatto) obj;
			if ((contatto.getNome() != null && !contatto.getNome().equals(nome)) 
					|| (contatto.getCognome() != null && !contatto.getCognome().equals(cognome)) 
					|| (contatto.getAliasContatto() != null && !contatto.getAliasContatto().equals(aliasContatto)) 
					|| (contatto.getIndirizzo() != null && !contatto.getIndirizzo().equals(indirizzo)) 
					|| (contatto.getCAP() != null && !contatto.getCAP().equals(cap)) 
					|| (contatto.getCellulare() != null && !contatto.getCellulare().equals(cellulare)) 
					|| (contatto.getMail() != null && !contatto.getMail().equals(mail)) 
					|| (contatto.getMailPec() != null && !contatto.getMailPec().equals(mailPec)) 
					|| (contatto.getTelefono() != null && !contatto.getTelefono().equals(telefono)) 
					|| (contatto.getFax() != null && !contatto.getFax().equals(fax)) 
					|| (contatto.getRegione() != null && !contatto.getRegione().equals(regione)) 
					|| (contatto.getProvincia() != null && !contatto.getProvincia().equals(provincia)) 
					|| (contatto.getComune() != null && !contatto.getComune().equals(comune))) {
				equals = false;
			}
		}
		return equals;
	}

	/**
	 * Restituisce l'alias del contatto di protocollazione.
	 * @return aliasContattoProtocollazione
	 */
	public String getAliasContattoProtocollazione() {
		return aliasContattoProtocollazione;
	}

	/**
	 * Imposta l'alisa del contatto di protocollazione.
	 * @param aliasContattoProtocollazione
	 */
	public void setAliasContattoProtocollazione(final String aliasContattoProtocollazione) {
		this.aliasContattoProtocollazione = aliasContattoProtocollazione;
	}

	/**
	 * Restituisce la data di aggiunta ai preferiti.
	 * @return dataDiAggiuntaAiPreferiti
	 */
	public Date getDataDiAggiuntaAiPreferiti() {
		return dataDiAggiuntaAiPreferiti;
	}

	/**
	 * Imposta la data di aggiunta ai preferiti.
	 * @param dataDiAggiuntaAiPreferiti
	 */
	public void setDataDiAggiuntaAiPreferiti(final Date dataDiAggiuntaAiPreferiti) {
		this.dataDiAggiuntaAiPreferiti = dataDiAggiuntaAiPreferiti;
	}

	/**
	 * @return utenteCheHaAggiuntoAiPreferiti
	 */
	public String getUtenteCheHaAggiuntoAiPreferiti() {
		return utenteCheHaAggiuntoAiPreferiti;
	}

	/**
	 * @param utenteCheHaAggiuntoAiPreferiti
	 */
	public void setUtenteCheHaAggiuntoAiPreferiti(final String utenteCheHaAggiuntoAiPreferiti) {
		this.utenteCheHaAggiuntoAiPreferiti = utenteCheHaAggiuntoAiPreferiti;
	}

	/**
	 * Restituisce la data di creazione gruppo.
	 * @return dataCreazioneGruppo
	 */
	public Date getDataCreazioneGruppo() {
		return dataCreazioneGruppo;
	}

	/**
	 * Imposta la data di creazione gruppo.
	 * @param dataCreazioneGruppo
	 */
	public void setDataCreazioneGruppo(final Date dataCreazioneGruppo) {
		this.dataCreazioneGruppo = dataCreazioneGruppo;
	}

	/**
	 * @return gruppoCreatoDa
	 */
	public String getGruppoCreatoDa() {
		return gruppoCreatoDa;
	}

	/**
	 * @param gruppoCreatoDa
	 */
	public void setGruppoCreatoDa(final String gruppoCreatoDa) {
		this.gruppoCreatoDa = gruppoCreatoDa;
	}

	/**
	 * Getter fromDialogCreazioneModifica.
	 * @return fromDialogCreazioneModifica
	 */
	public boolean getFromDialogCreazioneModifica() {
		return fromDialogCreazioneModifica;
	}

	/**
	 * Setter fromDialogCreazioneModifica.
	 * @param fromDialogCreazioneModifica
	 */
	public void setFromDialogCreazioneModifica(final boolean fromDialogCreazioneModifica) {
		this.fromDialogCreazioneModifica = fromDialogCreazioneModifica;
	}

	/**
	 * Restituisce il titolo.
	 * @return titolo
	 */
	public String getTitolo() {
		return titolo;
	}

	/**
	 * Imposta il titolo.
	 * @param titolo
	 */
	public void setTitolo(final String titolo) {
		this.titolo = titolo;
	}

	/**
	 * Restituisce il flag associato allo sbiancamento della tipologia persona.
	 * @return flag sbianca tipologia persona
	 */
	public boolean isSbiancaTipologiaPersona() {
		return sbiancaTipologiaPersona;
	}

	/**
	 * Imposta il flag associato allo sbiancamento della tipologia persona.
	 * @param sbiancaTipologiaPersona
	 */
	public void setSbiancaTipologiaPersona(final boolean sbiancaTipologiaPersona) {
		this.sbiancaTipologiaPersona = sbiancaTipologiaPersona;
	}

	/**
	 * Restituisce l'id del nodo preferito.
	 * @return id nodo preferito
	 */
	public Long getNodoPreferiti() {
		return nodoPreferiti;
	}
	
	/**
	 * Imposta l'id del nodo preferito.
	 * @param nodoPreferiti
	 */
	public void setNodoPreferiti(Long nodoPreferiti) {
		this.nodoPreferiti = nodoPreferiti;
	}

	/**
	 * Restituisce il flag associato all'eliminazione dei gruppi non ufficio.
	 * @return flag eliminazione gruppi non uff
	 */
	public boolean isDisabiitaDeleteGruppiNonUff() {
		return disabiitaDeleteGruppiNonUff;
	}
	
	/**
	 * Imposta il flag associato all'eliminazione dei gruppi non ufficio.
	 * @param disabiitaDeleteGruppiNonUff
	 */
	public void setDisabiitaDeleteGruppiNonUff(boolean disabiitaDeleteGruppiNonUff) {
		this.disabiitaDeleteGruppiNonUff = disabiitaDeleteGruppiNonUff;
	}
}