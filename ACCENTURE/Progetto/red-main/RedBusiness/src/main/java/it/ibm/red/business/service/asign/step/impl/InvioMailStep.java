package it.ibm.red.business.service.asign.step.impl;

import java.sql.Connection;
import java.util.Map;

import org.springframework.stereotype.Service;

import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.ASignStepResultDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.asign.step.ASignStepAbstract;
import it.ibm.red.business.service.asign.step.ContextASignEnum;
import it.ibm.red.business.service.asign.step.StepEnum;

/**
 * Step che gestisce l'invio mail - firma asincrona.
 */
@Service
public class InvioMailStep extends ASignStepAbstract {

	/**
	 * Serial version.
	 */
	private static final long serialVersionUID = -7553486970039206304L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(InvioMailStep.class.getName());

	/**
	 * Esegue l'invio della mail per un item di firma asincrona.
	 * @param in
	 * @param item
	 * @param context
	 * @return risultato dell'invio mail
	 */
	@Override
	protected ASignStepResultDTO run(Connection con, ASignItemDTO item, Map<ContextASignEnum, Object> context) {
		LOGGER.info("run -> START [ID item: " + item.getId() + "]");
		ASignStepResultDTO esitoEsecuzioneStep = new ASignStepResultDTO(item.getId(), getStep());
	
		// Recupero l'utente firmatario
		UtenteDTO utenteFirmatario = getUtenteFirmatario(item, con);
		
		// ### INVIO MAIL ###
		EsitoOperazioneDTO esitoInvioMail = signSRV.invioMail(utenteFirmatario, item, con);
		
		if (esitoInvioMail.isEsito()) {
			esitoEsecuzioneStep.setStatus(true);
			esitoEsecuzioneStep.appendMessage("[OK] Invio mail eseguito");
		} else {
			throw new RedException("[KO] Errore nello step di invio mail: " + esitoInvioMail.getNote() + ". [Codice errore: " + esitoInvioMail.getCodiceErrore() + "]");
		}
		
		LOGGER.info("run -> END [ID item: " + item.getId() + "]");
		return esitoEsecuzioneStep;
	}

	/**
	 * Restituisce lo @see StepEnum a cui fa riferimento questa classe.
	 * @return StepEnum
	 */
	@Override
	protected StepEnum getStep() {
		return StepEnum.INVIO_MAIL;
	}

	/**
	 * @see it.ibm.red.business.service.asign.step.ASignStepAbstract#getErrorMessage().
	 */
	@Override
	protected String getErrorMessage() {
		return "[KO] Errore imprevisto nello step di invio mail";
	}
	
}
