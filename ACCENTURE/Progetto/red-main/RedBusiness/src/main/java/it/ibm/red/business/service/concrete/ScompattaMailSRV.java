package it.ibm.red.business.service.concrete;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.mail.MessagingException;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.core.Document;

import it.ibm.red.business.dao.ITipoFileDAO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.DetailEmailDTO;
import it.ibm.red.business.dto.HierarchicalFileWrapperDTO;
import it.ibm.red.business.dto.MailAttachmentDTO;
import it.ibm.red.business.dto.NamedStreamDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.PkHandler;
import it.ibm.red.business.persistence.model.TipoFile;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IMailSRV;
import it.ibm.red.business.service.IScompattaMailSRV;
import it.ibm.red.business.service.ITipoFileSRV;
import it.ibm.red.business.service.facade.ISignFacadeSRV;
import it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV;
import it.ibm.red.business.utils.EmailUtils;
import it.ibm.red.business.utils.FileUtils;

/**
 * Vista la crescente complessita' delle funzioni per gestire il content degli
 * allegati ho preparato un servizio a parte che gestisce lo scompattamento
 * delle mail.
 */
@Service
public class ScompattaMailSRV extends AbstractService implements IScompattaMailSRV {

	private static final String IMPOSSIBILE_ESTRARRE_IL_FILE_MSG = "Impossibile estrarre il file msg";

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 7482802714113868744L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ScompattaMailSRV.class.getName());

	/**
	 * Messaggio di errore trasformazione HierarchicalFileWrapperDTO - AllegatoDTO.
	 */
	private static final String ERROR_TRASFORMAZIONE_MSG = "Errore durante la trasformazione da hierarchicalfilewrapper a AllegatoDTO";

	/**
	 * Indice.
	 */
	private static final Integer DEFAULT_TEXT_INDEX = -1;

	/**
	 * Service.
	 */
	@Autowired
	private ITipoFileSRV tipoFileSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ISignFacadeSRV signSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IMailSRV mailSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ITipologiaDocumentoFacadeSRV tipologiaDocumentoSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private ITipoFileDAO tipoFileDAO;

	/**
	 * @see it.ibm.red.business.service.facade.IScompattaMailFacadeSRV#scompattaMail(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, boolean).
	 */
	@Override
	public List<HierarchicalFileWrapperDTO> scompattaMail(final UtenteDTO utente, final String guid, final boolean mantieniAllegatiOriginali) {
		List<HierarchicalFileWrapperDTO> scompattati = null;
		Connection con = null;
		IFilenetCEHelper fceh = null;

		try {
			con = setupConnection(getDataSource().getConnection(), true);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final HashMap<String, byte[]> pathContentMailScompattata = new HashMap<>();
			final PkHandler pkHandler = utente.getSignerInfo().getPkHandlerVerifica();

			tipoFileDAO.deleteContents(con, utente.getIdAoo().intValue(), utente.getId().intValue(), utente.getIdUfficio().intValue(), utente.getIdRuolo().intValue(), guid);

			// Recupero documenti FN
			final Document doc = fceh.getDocumentByGuid(guid);

			// Converto Documenti FN in DTO
			final DetailEmailDTO email = TrasformCE.transform(doc, TrasformerCEEnum.FROM_DOCUMENTO_TO_MAIL_DETAIL);

			if (!mailSRV.isProtocollabile(email.getStatoMail())) {
				throw new RedException("L'email non è protocollabile");
			}

			scompattati = new ArrayList<>();
			final Collection<TipoFile> allTipoDati = tipoFileSRV.getAll();

			// aggiungo il testo dell'email principale viene considerato come un allegato
			String testoEmail = email.getTesto();
			if (testoEmail == null) {
				testoEmail = StringUtils.EMPTY;
			}
			// START VI
			final NamedStreamDTO contentTextNodePdf = EmailUtils.initializeTextNodePdf();
			final byte[] contentVelina = mailSRV.creaFileVelinaEmail(email);
			final HierarchicalFileWrapperDTO textNodePdf = recursiveScompattaMail(utente, DEFAULT_TEXT_INDEX, contentTextNodePdf.getName(), contentVelina, allTipoDati, guid,
					pkHandler, pathContentMailScompattata);
			scompattati.add(textNodePdf);

			final NamedStreamDTO contentTextNode = EmailUtils.initializeTextNode();
			final HierarchicalFileWrapperDTO textNode = recursiveScompattaMail(utente, DEFAULT_TEXT_INDEX, contentTextNode.getName(), testoEmail.getBytes(), allTipoDati, guid,
					pkHandler, pathContentMailScompattata);
			scompattati.add(textNode);

			// END VI

			int indexMailAllegati = 0;
			final Collection<MailAttachmentDTO> allegati = createAllegatiListWithContentsOrNot(doc.get_ChildDocuments());
			
			int dimensioneMax=Integer.parseInt(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.MAX_SIZE_SPACCHETTAMENTO_MAIL_MB));
			boolean inMantieniAllegatiOriginali = mantieniAllegatiOriginali;
			if(dimensioneMax >0) {
				int byteSize = 0;
				for (MailAttachmentDTO allegato : allegati) {
					if(allegato.getContent()!=null) {
						byteSize+=allegato.getContent().length;
					}
				}
				int mbSize = byteSize/1048576;
				if(!inMantieniAllegatiOriginali && mbSize> dimensioneMax){
					inMantieniAllegatiOriginali = true;
				}
			}
			for (final MailAttachmentDTO allegato : allegati) {
				// Viene scritto il GUID poi verra scritto il nome del figlio.
				final String pathOfDirectSon = StringUtils.join(guid, HierarchicalFileWrapperDTO.FATHER_SON_SEPARATOR, indexMailAllegati,
						HierarchicalFileWrapperDTO.BRANCH_NAME_SEPARATOR, allegato.getId());
				HierarchicalFileWrapperDTO hfwDto;
				if (inMantieniAllegatiOriginali) {
					hfwDto = createHierachicalWrapper(indexMailAllegati, allegato.getDocumentTitle(), allegato.getContent(), allTipoDati, pathOfDirectSon);
					hfwDto.setContentSize(allegato.getContent().length);
					hfwDto.setFirmato(isFirmato(hfwDto.getFile(), utente));
					hfwDto = verificaEinizializzaFoglia(hfwDto, allTipoDati);

					if (hfwDto == null) {
						throw new RedException("Errore nella verifica e l'inizializzazione della foglia.");
					}
					pathContentMailScompattata.put(hfwDto.getNodeId(), allegato.getContent());
				} else {
					try {
						hfwDto = recursiveScompattaMail(utente, indexMailAllegati, allegato.getDocumentTitle(), allegato.getContent(), allTipoDati, pathOfDirectSon, pkHandler,
								pathContentMailScompattata);
					} catch (final Exception e) {
						if ((e.getCause() != null) && ("encrypted ZIP entry not supported".equals(e.getCause().getMessage()))
								|| ("Cannot read encrypted content from unknown archive without a password.".equals(e.getCause().getMessage()))) {

							hfwDto = createHierachicalWrapper(indexMailAllegati, allegato.getDocumentTitle(), null, allTipoDati, pathOfDirectSon);
							hfwDto.setIsProtected(true);
							hfwDto = verificaEinizializzaFoglia(hfwDto, allTipoDati);
							hfwDto.setPrincipalSelectable(false);
							pathContentMailScompattata.put(hfwDto.getNodeId(), allegato.getContent());
						} else {
							throw e;
						}
					}
				}

				// Se ritorna null oppure non è una foglia e non ha figli non è valido
				if (hfwDto != null && hfwDto.isValid()) {
					scompattati.add(hfwDto);
				}
				indexMailAllegati++;
			}
			tipoFileDAO.insertRecordEmail(con, utente.getIdAoo().intValue(), utente.getId().intValue(), utente.getIdUfficio().intValue(), utente.getIdRuolo().intValue(), guid,
					pathContentMailScompattata);

			commitConnection(con);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante lo scompattamento dell'e-mail.", e);
			rollbackConnection(con);
			throw new RedException(e);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}

		return scompattati;
	}

	/**
	 * Acquisisce gli allegati dell'email, il contenuto in byte degli allegati è
	 * acquisito solo se l'allegato dovrà essere ulteriormente scompattato. Per
	 * questo motivo il metodo si trova nel service
	 * 
	 * @param documentSet
	 * @return
	 */
	private Collection<MailAttachmentDTO> createAllegatiListWithContentsOrNot(final DocumentSet documentSet) {
		final Collection<MailAttachmentDTO> allegatoList = new ArrayList<>();
		final Iterator<?> it = documentSet.iterator();
		while (it.hasNext()) {
			final Document d = (Document) it.next();
			MailAttachmentDTO mailAttachment = TrasformCE.transform(d, TrasformerCEEnum.FROM_DOCUMENTO_TO_ALLEGATO_CONTENT_MAIL);
			allegatoList.add(mailAttachment);
		}
		return allegatoList;
	}

	/**
	 * Ritorna un nodo dell'albero degli allegati dell'email Ciascun nodo ha un
	 * figlio chiamato file che contiene il nome dell'allegato Dell'allegato ci
	 * interessa esclusivamente il nome tentiamo di evitare in ogni modo di
	 * acquisire byte[] che non ci interessano
	 * 
	 * L'algoritmo appica le seguenti regole: Gli allegati che non hanno un titolo
	 * con una estensione valida sono ignorati. L'attributo del path è univoco solo
	 * se non esistono due figli dello stesso nodo con lo stesso nome, questa
	 * ipotesi è ragionevole Eml scompatta ricorsivamente gli eml e i loro allegati
	 * Msg scompatta ricorsivamente gli msg e i loro allegati Considera il text body
	 * dell'email come un allegato e gli fornisce un nome file fittizio in formato
	 * html
	 * 
	 * 
	 * p7m sbustati ricorsivamente viene verificata la correttezza della firma zip
	 * sbustati il contenuto del file zip non viene ulteriormente sbustato
	 * 
	 * Gli allegati estratti che non hanno le estensioni p7m, zip, eml, msg vengono
	 * confrontati con le estensioni consentite e quindi eventualmente scartati o
	 * aggiunti come foglia dell'albero. I nodi che non posseggono foglie valide
	 * vengono scartati.
	 * 
	 * @param fileName    nome del file da scompattare
	 * @param content     contenuto in byte dell'allegato da analizzare
	 * @param allTipoDati contiene le estensioni che rendono valide le foglie
	 *                    dell'albero
	 * @param path        contiene il path del file nell'albero
	 * @param pkHandler   utility per recuperare il pk.
	 * @param index       identificativo del nodo rispetto ai suoi fratelli nella
	 *                    struttra ad albero degli allegati.
	 * 
	 */
	public HierarchicalFileWrapperDTO recursiveScompattaMail(final UtenteDTO utente, final int index, final String fileName, final byte[] content,
			final Collection<TipoFile> allTipoDati, final String path, final PkHandler pkHandler, final Map<String, byte[]> pathContentMailScompattata) {
		// Inizializzo il padre
		HierarchicalFileWrapperDTO hfwDto = createHierachicalWrapper(index, fileName, content, allTipoDati, path);
		hfwDto.setFirmato(isFirmato(hfwDto.getFile(), utente));
		if (hfwDto.getFile().getContent() != null) {
			pathContentMailScompattata.put(hfwDto.getNodeId(), hfwDto.getFile().getContent());
		}

		final List<HierarchicalFileWrapperDTO> extractedSons = hfwDto.getSons();

		List<NamedStreamDTO> toExtractSons = new ArrayList<>();
		final String newPath = hfwDto.getNodeId();

		final String extension = FilenameUtils.getExtension(fileName);
		if ("eml".equalsIgnoreCase(extension)) { // scompatta ricorsivamente l'email e i suoi allegati
			try {
				hfwDto.setArchivio(true);
				toExtractSons = EmailUtils.extractBodyTextAndAttachmentsFromEml(content);
			} catch (IOException | MessagingException me) {
				LOGGER.error(me);
				throw new RedException("Impossibile estrarre il file eml", me);
			}
		} else if ("msg".equalsIgnoreCase(extension)) { // scompatta ricorsivamente l'email e i suoi allegati
			try {
				hfwDto.setArchivio(true);
				toExtractSons = EmailUtils.extractBodyTextAndAttachmentsFromMsg(content);				
			} catch (final Exception me) {
				if(me.getMessage()!=null && me.getMessage().contains("##EXTRACTFAILED##")) {
					toExtractSons = gestisciEstrazioneMsg(content);
				} else {
					LOGGER.error(me);
					throw new RedException(IMPOSSIBILE_ESTRARRE_IL_FILE_MSG, me);
				} 
			}
		} else if ("p7m".equalsIgnoreCase(extension)) { // Scompatta recorsivamente il p7m fintanto che non trova qualcosa che non sia
														// p7m
			hfwDto.setArchivio(true);
			final NamedStreamDTO extracted = FileUtils.recursiveExtractP7MFile(content, fileName, pkHandler.getHandler(), pkHandler.getSecurePin(),
					utente.getDisableUseHostOnly());
			toExtractSons.add(extracted);
		} else if ("zip".equalsIgnoreCase(extension)) { // caso base zip non deve essere scompattato ricorsivamente
			try {
				hfwDto.setArchivio(true);
				final List<NamedStreamDTO> entryList = FileUtils.extractZipEntryName(content);
				if (CollectionUtils.isNotEmpty(entryList)) {
					int i = 0;
					for (final NamedStreamDTO entryZip : entryList) {
						final TipoFile entryTipoFile = tipoFileSRV.getTipoFileFromFileName(entryZip.getName(), allTipoDati);
						entryZip.setTipoFile(entryTipoFile);
						if (entryTipoFile != null) { // = tipoFileSRV.isSupportedFileName(fileName, allTipoDati)
							final HierarchicalFileWrapperDTO entryWrapper = new HierarchicalFileWrapperDTO();
							entryWrapper.setFile(entryZip);
							entryWrapper.setAttachmentSelectable(true);
							entryWrapper.setPrincipalSelectable(true);
							entryWrapper.setNodeId(newPath, i);
							if (entryWrapper.getFile().getContent() != null) {
								pathContentMailScompattata.put(entryWrapper.getNodeId(), entryWrapper.getFile().getContent());
								entryWrapper.setFirmato(isFirmato(entryWrapper.getFile(),utente));	
							}
							extractedSons.add(entryWrapper);
						}
						i++;
						hfwDto.getFile().setContent(null);
					}
				}
			} catch (final IOException e) {
				LOGGER.error(e);
				throw new RedException("Impossibile scompattare il file zip", e);
			}

		} else { // caso base file non scompattabile
			hfwDto = verificaEinizializzaFoglia(hfwDto, allTipoDati);
		}

		if (CollectionUtils.isNotEmpty(toExtractSons)) { // caso ricorsivo comune a tutti i rami ricorsivi
			int indiceRamo = 0;
			boolean sonsNULL = true;
			for (final NamedStreamDTO sonStream : toExtractSons) {
				final HierarchicalFileWrapperDTO son = recursiveScompattaMail(utente, indiceRamo, sonStream.getName(), sonStream.getContent(), allTipoDati, newPath, pkHandler,
						pathContentMailScompattata);
				if (son != null && son.isValid()) {
					hfwDto.getSons().add(son);
					sonsNULL = false;
				}
				indiceRamo++;
			}

			if (sonsNULL) {
				hfwDto.setAttachmentSelectable(true);
				hfwDto.setPrincipalSelectable(true);
				// In questo modo non viene caricato come file non sbustato--> vale come singolo
				// file
				hfwDto.setArchivio(false);

			} else {
				hfwDto.getFile().setContent(null);
				// Elimino i byte del padre perché i figli portano tutte le informazioni
				// necessarie.getFile().setContent(null)
				// Elimino i byte del padre perché i figli portano tutte le informazioni
				// necessarie
			}
		}

		return hfwDto;
	}


	private List<NamedStreamDTO> gestisciEstrazioneMsg(final byte[] content) {
		List<NamedStreamDTO> toExtractSons  = new ArrayList<>();
		try {
			toExtractSons = EmailUtils.extractBodyTextAndAttachmentsFromEml(content);
		}  catch (IOException  | MessagingException me1) {
			LOGGER.error(me1);
			throw new RedException(IMPOSSIBILE_ESTRARRE_IL_FILE_MSG, me1);
		}
		return toExtractSons;
	}

 

	/**
	 * Restituisce true se il file in ingresso risulta firmato, false altrimenti.
	 * 
	 * @param file   - documento su cui controllare la firma
	 * @param utente
	 * @return true se il file in ingresso risulta firmato, false altrimenti
	 */
	public boolean isFirmato(final NamedStreamDTO file, final UtenteDTO utente) {
		Boolean firmato = false;
		if(file != null && file.getContent() != null) {
			if(file.getFileName()!=null && (file.getFileName().toLowerCase().endsWith(".p7m") || file.getFileName().toLowerCase().endsWith(".p7s"))){
					return true;
			}else if(file.getFileName()!=null && file.getFileName().toLowerCase().endsWith(".pdf")){	
				try {
					final InputStream stream = new ByteArrayInputStream(file.getContent());
					firmato = signSRV.hasSignatures(stream, utente);
				} catch (final Exception e) {
					LOGGER.error("Errore nel calcolo della/e firma/e del doc " + file.getFileName(), e);
				}
			}
		}
		return firmato;
	}

	/**
	 * Se la foglia rappresente una tipologia di file non supportata dal sistema
	 * ritorno null. Altrimenti ritorno il file correttamente valorizzato, affinche
	 * sia una foglia.
	 * 
	 * @param hfwDto
	 *            Nodo da verificare e trasformare in forglia.
	 * @param allTipoDati
	 *            Lista di tipoDati supportati dal sistema.
	 * @return
	 */
	private static HierarchicalFileWrapperDTO verificaEinizializzaFoglia(final HierarchicalFileWrapperDTO hfwDto, final Collection<TipoFile> allTipoDati) {
		if (!allTipoDati.contains(hfwDto.getFile().getTipoFile())) {
			return null;
		} else {
			hfwDto.setAttachmentSelectable(true);
			hfwDto.setPrincipalSelectable(true);
			hfwDto.getFile().setContent(null);
		}
		return hfwDto;
	}

	/**
	 * Inizializza un hierarchicalFileWrapper.
	 * 
	 * @see {@link HierarchicalFileWrapperDTO}
	 * 
	 * @param index
	 *            identificativo del nodo tra i figli del padre.
	 * @param fileName
	 *            nome del file contenuto nel nodo.
	 * @param content
	 *            contenuto del byte del file
	 * @param allTipoDati
	 *            Una lista di tipoDati recuperati dal db necessari a individuare il
	 *            tipodati di riferimento del nodo.
	 * @param path
	 *            Il path del nodo a partire dalla radice. Corrisponde all'attributo
	 *            nodoId dello hierarchicalFileWrapper
	 * @return
	 * 
	 * 		Una istanza foglia dello {@link HierarchicalFileWrapperDTO}.
	 */
	private HierarchicalFileWrapperDTO createHierachicalWrapper(final int index, final String fileName, final byte[] content, final Collection<TipoFile> allTipoDati,
			final String path) {
		final NamedStreamDTO file = new NamedStreamDTO();
		file.setName(fileName);
		file.setContent(content);

		// "StringUtils.lowerCase" serve per la visualizzazione dell'anteprima anche
		// quando il file è .PDF
		final TipoFile tipoFile = tipoFileSRV.getTipoFileFromFileName(StringUtils.lowerCase(fileName), allTipoDati);

		file.setTipoFile(tipoFile);
		final HierarchicalFileWrapperDTO hfwDto = new HierarchicalFileWrapperDTO();
		hfwDto.setFile(file);
		hfwDto.setSons(new ArrayList<>());
		hfwDto.setNodeId(path, index);
		return hfwDto;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IScompattaMailFacadeSRV#getContentutoDiscompattataMail(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String).
	 */
	@Override
	public byte[] getContentutoDiscompattataMail(final UtenteDTO utente, final String path) {
		byte[] contentsArray = null;
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			contentsArray = tipoFileDAO.getContent(con, utente.getIdAoo().intValue(), utente.getId().intValue(), utente.getIdUfficio().intValue(),
					utente.getIdRuolo().intValue(), path);

		} catch (final Exception e) {
			final String msg = "Scompatta mail non riuscito: non sono riuscito a scompattare gli allegati per recuperare i loro content.";
			LOGGER.error(msg, e);
			throw new RedException(msg);
		} finally {
			closeConnection(con);
		}

		return contentsArray;
	}

	/**
	 * Estrae il contenuto dell'allegato dell'email leggendone il suo nodeId
	 * 
	 * Il nodeId è un identificativo che permette di individuare l'allegato e
	 * corrisponde al path dell'allegato, come se ogni allegato fosse una directory.
	 * Fin dove è possibile gli allegati sono individuati dall'id di FileNet, poi
	 * successivamente vengono usati i loro documentTitle/fileName.
	 * 
	 * @param utente
	 * @param documentTitle
	 *            nome del nodo da scompattareRicorsivamente
	 * @param content
	 *            rappresentazione in byte dell'allegato
	 * @param pathStack
	 *            contiene i nodi del path successivi a documentTitle
	 * @param pkHandler
	 *            handler PK per l'estrazione dei file P7M
	 * @return
	 */
	public byte[] recursiveGetContentsScompattaMail(final UtenteDTO utente, final String documentTitle, final byte[] content,
			final Deque<HierarchicalFileWrapperDTO> pathStack, final PkHandler pkHandler) {
		if (pathStack.isEmpty()) {
			return content;
		} else {
			List<NamedStreamDTO> toExtractSons = new ArrayList<>();
			final HierarchicalFileWrapperDTO actualPathDTO = pathStack.pop(); // prendo il nome della entry
			final String actualPath = actualPathDTO.getFile().getName();
			Integer actualIndex = actualPathDTO.getIdentificativoRamo();

			final String extension = FilenameUtils.getExtension(documentTitle);

			if ("eml".equalsIgnoreCase(extension)) { // scompatta ricorsivamente l'email e i suoi allegati
				try {
					toExtractSons = EmailUtils.extractBodyTextAndAttachmentsFromEml(content);
				} catch (IOException | MessagingException me) {
					LOGGER.error(me);
					throw new RedException("Impossibile estrarre il file eml", me);
				}
			} else if ("msg".equalsIgnoreCase(extension)) { // scompatta ricorsivamente l'email e i suoi allegati
				try {
					toExtractSons = EmailUtils.extractBodyTextAndAttachmentsFromMsg(content);
				} catch (final IOException me) {
					LOGGER.error(me);
					throw new RedException(IMPOSSIBILE_ESTRARRE_IL_FILE_MSG, me);
				}
			} else if ("p7m".equalsIgnoreCase(extension)) { // Scompatta recorsivamente il p7m fintanto che non trova qualcosa che non sia
															// p7m
				final NamedStreamDTO extracted = FileUtils.recursiveExtractP7MFile(content, documentTitle, pkHandler.getHandler(), pkHandler.getSecurePin(),
						utente.getDisableUseHostOnly());
				toExtractSons.add(extracted);
				actualIndex = 0;
			} else if ("zip".equalsIgnoreCase(extension)) { // a questo punto il path dovrebbe essere vuoto
				try {
					final byte[] contentExtracted = FileUtils.extractZipEntryByName(content, actualPath);
					final NamedStreamDTO extracted = new NamedStreamDTO();
					extracted.setName(actualPath);
					extracted.setContent(contentExtracted);
					toExtractSons.add(extracted);
					actualIndex = 0;
				} catch (final IOException e) {
					LOGGER.error(e);
					throw new RedException("Impossibile scompattare il file zip " + actualPath, e);
				}
			}

			if (actualIndex >= toExtractSons.size()) {
				throw new RedException("L'indice dell'allegato da scompattare e' superiore ai risultati trovati");
			}

			final NamedStreamDTO inIndex = toExtractSons.get(actualIndex);
			if (inIndex.getName().equals(actualPath)) {
				return recursiveGetContentsScompattaMail(utente, inIndex.getName(), inIndex.getContent(), pathStack, pkHandler);
			} else {
				final String messaggio = "Attenzione l'indice restituito dallo scompatto mail e l'albero individuato dal getContenuto scompattaMail non sono coerenti nome file:"
						+ inIndex.getName() + ", actualPath= " + actualPath + ", actualindex= " + actualIndex;
				throw new RedException(messaggio);
			}
		}
	}

	/**
	 * @param path
	 * @return Deque di HierarchicalFileWrapperDTO
	 */
	public Deque<HierarchicalFileWrapperDTO> parseHierarchicalFileWrapperDTONodeId(final String path) {
		// Questo split usa dei caratteri specifici piuttosto che una regular
		// expression.
		final String[] splittedPath = StringUtils.split(path, HierarchicalFileWrapperDTO.FATHER_SON_SEPARATOR);

		final Deque<HierarchicalFileWrapperDTO> stack = new ArrayDeque<>();
		final List<String> splitPathList = Arrays.asList(splittedPath);

		final List<HierarchicalFileWrapperDTO> splitFileList = splitPathList.stream().map(p -> fromSplittedPath(p)).collect(Collectors.toList());

		Collections.reverse(splitFileList);
		stack.addAll(splitFileList);

		return stack;
	}

	private HierarchicalFileWrapperDTO fromSplittedPath(final String splittedPath) {
		final String[] indexAndNome = StringUtils.split(splittedPath, HierarchicalFileWrapperDTO.BRANCH_NAME_SEPARATOR, 2);

		final Integer index = indexAndNome.length > 1 ? Integer.parseInt(indexAndNome[0]) : null;
		final String nome = indexAndNome.length > 1 ? indexAndNome[1] : indexAndNome[0];

		final NamedStreamDTO named = new NamedStreamDTO();
		named.setName(nome);
		final HierarchicalFileWrapperDTO hFwDTO = new HierarchicalFileWrapperDTO();
		hFwDTO.setFile(named);
		hFwDTO.setNodeId(null, index);
		return hFwDTO;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IScompattaMailFacadeSRV#transformToAllegatoDTO(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.List).
	 */
	@Override
	public List<AllegatoDTO> transformToAllegatoDTO(final UtenteDTO utente, final List<HierarchicalFileWrapperDTO> fileList) {
		try {
			return transformToAllegatoDTO(utente, fileList, false, null);
		} catch (final Exception e) {
			LOGGER.error(ERROR_TRASFORMAZIONE_MSG, e);
			throw new RedException(e);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IScompattaMailFacadeSRV#transformToAllegatoDTO(it.ibm.red.business.dto.UtenteDTO,
	 *      java.util.List, boolean, java.lang.Integer).
	 */
	@Override
	public List<AllegatoDTO> transformToAllegatoDTO(final UtenteDTO utente, final List<HierarchicalFileWrapperDTO> fileList, final boolean inMantieniFormatoOriginale,
			final Integer idTipologiaDocumentale) {
		try {
			final List<AllegatoDTO> allegatiSelezionatiDati = new ArrayList<>();
			if (CollectionUtils.isEmpty(fileList)) {
				return allegatiSelezionatiDati;
			}

			final List<TipologiaDocumentoDTO> tipiDocAttive = tipologiaDocumentoSRV.getComboTipologieByAooAndTipoCategoriaAttivi(TipoCategoriaEnum.ENTRATA, utente.getIdAoo());

			for (final HierarchicalFileWrapperDTO selected : fileList) {

				Boolean mantieniFormatoOriginale = inMantieniFormatoOriginale;
				Boolean checkMantieniFOdisable = false;

				String nomeFile = selected.getFile().getFileName();
				// qualora il nome contenga anche un persorso lo taglio, deve corrispondere al
				// nome reale dell'allegato.
				nomeFile = FilenameUtils.getName(nomeFile);

				final String mimeType = selected.getFile().getTipoFile().getMimeType();
				// Possibili miglioramenti di prestazione chiamando una sola volta filenet per
				// recuperare tutti gli allegati.
				final byte[] contenuto = getContentutoDiscompattataMail(utente, selected.getNodeId());

				if (utente.getMantieniFormatoOriginale() != 1 && selected.getIsProtected() != null && Boolean.TRUE.equals(selected.getIsProtected())) {
					mantieniFormatoOriginale = true;
					checkMantieniFOdisable = true;
				}
				FormatoAllegatoEnum formatoall = FormatoAllegatoEnum.ELETTRONICO;

				final boolean hassignature = selected.isFirmato();
				if (hassignature) {
					formatoall = FormatoAllegatoEnum.FIRMATO_DIGITALMENTE;
				}

				final boolean allegatoNonSbustato = selected.getFileNonSbustato();
				if (contenuto != null && contenuto.length > 0) {
					final AllegatoDTO alleg = new AllegatoDTO(null, formatoall.getId(), idTipologiaDocumentale, null, null, null, mantieniFormatoOriginale,
							checkMantieniFOdisable, null, null, nomeFile, mimeType, null, contenuto, null, null, null, null, true, tipiDocAttive, allegatoNonSbustato, false);
					alleg.setNodeIdFromHierarchicalFile(selected.getNodeId());

					allegatiSelezionatiDati.add(alleg);
				}
			}

			return allegatiSelezionatiDati;
		} catch (final Exception e) {
			LOGGER.error(ERROR_TRASFORMAZIONE_MSG, e);
			throw new RedException(e);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IScompattaMailFacadeSRV#populateAllegatoDTOInterop(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.AllegatoDTO,
	 *      it.ibm.red.business.dto.DetailEmailDTO, boolean, boolean,
	 *      java.lang.Integer, java.util.List).
	 */
	@Override
	public AllegatoDTO populateAllegatoDTOInterop(final UtenteDTO utente, final AllegatoDTO allegatoDTO, final DetailEmailDTO mailSelected,
			final boolean mantieniFormatoOriginale, final boolean disabilitaMantieniFormatoOriginale, final Integer idTipologiaDocumentale,
			final List<TipologiaDocumentoDTO> tipiDocAttive) {
		try {
			AllegatoDTO allegatoReturn = null;

			String nomeFile = allegatoDTO.getNomeFile();
			// qualora il nome contenga anche un persorso lo taglio, deve corrispondere al
			// nome reale dell'allegato.
			nomeFile = FilenameUtils.getName(nomeFile);

			final String mimeType = allegatoDTO.getMimeType();
			byte[] contenuto = allegatoDTO.getContent();

			if (contenuto == null || contenuto.length == 0) {
				// Possibili miglioramenti di prestazione chiamando una sola volta filenet per
				// recuperare tutti gli allegati.
				final String path = mailSelected.getGuid() + "|" + allegatoDTO.getGuid() + "|" + allegatoDTO.getNomeFile();
				contenuto = getContentutoDiscompattataMail(utente, path);
			}

			if (contenuto != null && contenuto.length > 0) {
				allegatoReturn = new AllegatoDTO(null, FormatoAllegatoEnum.ELETTRONICO.getId(), idTipologiaDocumentale, null, null, null, mantieniFormatoOriginale,
						disabilitaMantieniFormatoOriginale, null, null, nomeFile, mimeType, null, contenuto, null, null, null, null, true, tipiDocAttive, false, false);
			}

			return allegatoReturn;
		} catch (final Exception e) {
			LOGGER.error(ERROR_TRASFORMAZIONE_MSG, e);
			throw new RedException(e);
		}
	}
}
