package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IComunicazioneProtocolliArgoDAO;
import it.ibm.red.business.dto.ComunicazioneProtocolloArgoDTO;
import it.ibm.red.business.enums.StatoComunicazioneProtocolloArgoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;


/**
 * DAO per la gestione delle comunicazioni verso il sistema ARGO di avvenuta
 * protocollazione da parte di RED.
 *
 * @author m.crescentini
 */
@Repository
public class ComunicazioneProtocolliArgoDAO extends AbstractDAO implements IComunicazioneProtocolliArgoDAO {
	
	/**
	 * Serial version UID.
	 */
	private static final long serialVersionUID = -716643970571625260L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ComunicazioneProtocolliArgoDAO.class.getName());
	
	
	/**
	 * @see it.ibm.red.business.dao.IProtocollazioneArgoDAO#inserisciComunicazioneProtocolloInCoda(it.ibm.red.business.dto.ComunicazioneProtocolloArgoDTO,
	 *      java.sql.Connection).
	 * @param comunicazioneProtocolloArgo
	 *            comunicazione Argo da inserire
	 * @param con
	 *            connession al database
	 * @return esito dell'update SQL
	 */
	@Override
	public long inserisciComunicazioneProtocolloInCoda(final ComunicazioneProtocolloArgoDTO comunicazioneProtocolloArgo, final Connection con) {
		long idCoda = 0;
		PreparedStatement ps = null;
		final ResultSet rs = null;
		
		try {
			idCoda = getNextId(con, "SEQ_CODA_PROT_ARGO");
			
			ps = con.prepareStatement("INSERT INTO REDBATCH_CODA_PROT_ARGO "
					+ "(IDCODA, IDDOCUMENTO, NUMEROPROTOCOLLO, ANNOPROTOCOLLO, REGISTROPROTOCOLLO, DATAREGISTRAZIONEPROTOCOLLO, IDPROTOCOLLO, IDAOO, STATO) "
					+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)");
			
			ps.setLong(1, idCoda);
			ps.setInt(2, comunicazioneProtocolloArgo.getIdDocumento());
			ps.setInt(3, comunicazioneProtocolloArgo.getNumeroProtocollo());
			ps.setInt(4, comunicazioneProtocolloArgo.getAnnoProtocollo());
			ps.setString(5, comunicazioneProtocolloArgo.getRegistroProtocollo());
			ps.setTimestamp(6, new Timestamp(comunicazioneProtocolloArgo.getDataRegistrazioneProtocollo().getTime()));
			ps.setString(7, comunicazioneProtocolloArgo.getIdProtocollo());
			ps.setLong(8, comunicazioneProtocolloArgo.getIdAoo());
			ps.setInt(9, StatoComunicazioneProtocolloArgoEnum.DA_ELABORARE.getId());
			
			final int result = ps.executeUpdate();
			
			if (result > 0) {
               LOGGER.info("Richiesta di comunicazione al sistema ARGO del protocollo con ID: " + comunicazioneProtocolloArgo.getIdProtocollo() 
               	+ " inserita in coda per il documento: " + comunicazioneProtocolloArgo.getIdDocumento() + ". ID coda: " + idCoda);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException("Errore durante l'inserimento di un item nella coda delle comunicazioni di protocollo al sistema ARGO. ID documento: " 
					+ comunicazioneProtocolloArgo.getIdDocumento() + ", ID protocollo: " + comunicazioneProtocolloArgo.getIdProtocollo(), e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return idCoda;
	}

	
	/**
	 * @see it.ibm.red.business.dao.IProtocollazioneArgoDAO#getAndLockComunicazioneProtocollo(java.lang.Long,
	 *      java.sql.Connection).
	 * @param idAoo
	 *            identificativo dell'AOO
	 * @param con
	 *            connession al database
	 * @return comunicazione ottenuta
	 */
	@Override
	public ComunicazioneProtocolloArgoDTO getAndLockComunicazioneProtocollo(final long idAoo, final Connection con) {
		ComunicazioneProtocolloArgoDTO comunicazioneProtocollo = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			ps = con.prepareStatement("SELECT * FROM REDBATCH_CODA_PROT_ARGO WHERE IDAOO = ? AND STATO = ? ORDER BY DATAINSERIMENTO FOR UPDATE SKIP LOCKED");
			
			ps.setLong(index++, idAoo);
			ps.setLong(index++, StatoComunicazioneProtocolloArgoEnum.DA_ELABORARE.getId());
			ps.setMaxRows(1);

			rs = ps.executeQuery();
			if (rs.next()) {
				comunicazioneProtocollo = populateItem(rs);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del prossimo item da lavorare dalla coda delle comunicazioni di protocollo al sistema ARGO", e);
			throw new RedException("Errore durante il recupero del prossimo item da lavorare dalla coda delle comunicazioni di protocollo al sistema ARGO: " + e.getMessage(), e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return comunicazioneProtocollo;
	}
	
	
	/**
	 * @see it.ibm.red.business.dao.IProtocollazioneArgoDAO#aggiornaStatoComunicazioneProtocollo(java.lang.Long,
	 *      int, java.sql.Connection).
	 * @param idCoda
	 *            id della coda
	 * @param stato
	 *            stato da inserire
	 * @param con
	 *            connessione al database
	 * @return esito dell'aggiornamento SQL
	 */
	@Override
	public int aggiornaStatoComunicazioneProtocollo(final Long idCoda, final int stato, final Connection con) {
		return aggiornaStatoComunicazioneProtocollo(idCoda, stato, Constants.EMPTY_STRING, con);
	}
	
	
	/**
	 * @see it.ibm.red.business.dao.IProtocollazioneArgoDAO#aggiornaStatoComunicazioneProtocollo(java.lang.Long,
	 *      int, java.lang.String, java.sql.Connection).
	 * @param idCoda
	 *            identificativo coda
	 * @param stato
	 *            stato da inserire
	 * @param errore
	 *            errore da impostare, inserire "" se non si è verificato un errore
	 * @param con
	 *            connessione al database
	 * @return esito dell'update SQL
	 */
	@Override
	public int aggiornaStatoComunicazioneProtocollo(final Long idCoda, final int stato, final String errore, final Connection con) {
		int result = 0;
		PreparedStatement ps = null;
		final ResultSet rs = null;
		
		try {
			ps = con.prepareStatement("UPDATE REDBATCH_CODA_PROT_ARGO SET STATO = ?, ERRORE = ?, DATALAVORAZIONE = SYSDATE WHERE IDCODA = ?");
			
			ps.setInt(1, stato);
			ps.setString(2, errore);
			ps.setLong(3, idCoda);
			
			result = ps.executeUpdate();
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'aggiornamento dell'item nella coda delle comunicazioni di protocollo al sistema ARGO."
					+ " ID coda: " + idCoda, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return result;
	}
	
	
	/**
	 * Restituisce true se è presente la comunicazione, false altrimenti.
	 * @param idDocumento
	 * @param idAoo
	 * @param con
	 */
	public boolean isComunicazionePresente(int idDocumento, Long idAoo, Connection con) {
		boolean presente = false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = con.prepareStatement("SELECT * FROM REDBATCH_CODA_PROT_ARGO WHERE IDDOCUMENTO = ? AND IDAOO = ?");
			
			ps.setInt(1, idDocumento);
			ps.setLong(2, idAoo);
			
			rs = ps.executeQuery();
			
			if (rs.next()) {
				presente = true;
			}
		} catch (Exception e) {
			LOGGER.error("Errore durante la ricerca di un item nella coda per l'ID documento: " + idDocumento + " e l'AOO: " + idAoo, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return presente;
	}
	
	/**
	 * @param rs
	 * @return
	 * @throws SQLException 
	 */
	private static ComunicazioneProtocolloArgoDTO populateItem(final ResultSet rs) throws SQLException {
		return new ComunicazioneProtocolloArgoDTO(rs.getLong("IDCODA"), rs.getInt("IDDOCUMENTO"), rs.getLong("IDAOO"), rs.getInt("NUMEROPROTOCOLLO"),
				rs.getInt("ANNOPROTOCOLLO"), rs.getDate("DATAREGISTRAZIONEPROTOCOLLO"), rs.getString("IDPROTOCOLLO"), rs.getString("REGISTROPROTOCOLLO"));
	}
}