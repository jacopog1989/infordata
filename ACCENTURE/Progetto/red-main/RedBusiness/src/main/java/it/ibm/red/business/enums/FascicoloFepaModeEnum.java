package it.ibm.red.business.enums;

/**
 * Enum modalità fascicoli fepa.
 */
public enum FascicoloFepaModeEnum {
	
	/**
	 * Valore.
	 */
	OFF("O", ""),
	
	/**
	 * Valore.
	 */
	SICOGE("S", "Visualizza Fascicolo SICOGE"),
	
	/**
	 * Valore.
	 */
	BILANCIO_ENTI("B", "Visualizza Fascicolo Bilancio Enti"),
	
	/**
	 * Valore.
	 */
	REVISORI("R", "Visualizza Fascicolo Revisori");

	/**
	 * Codice.
	 */
	private String code;

	/**
	 * Valore.
	 */
	private String btnValue;

	FascicoloFepaModeEnum(final String code, final String btnValue) {
		this.code = code;
		this.btnValue = btnValue;
	}

	/**
	 * Restituisce il value dell'enum.
	 * @return value
	 */
	public String getBtnValue() {
		return btnValue;
	}

	/**
	 * Restituisce il codice dell'enum.
	 * @return code
	 */
	public String getCode() {
		return code;
	}

}