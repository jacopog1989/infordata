package it.ibm.red.business.enums;

/**
 * The Enum TipoAzioneEnum.
 *
 * @author CPIERASC
 * 
 *         Enum per le azioni rappresentate sul calendario.
 */
public enum ErroriAttrExtEnum {

	/**
	 * Valore.
	 */
	ERRORE_RECUPERO_AOO(1, "Errore in fase di recupero dell'area organizzativa omogenea di riferimento."), // Errore configurazione

	/**
	 * Valore.
	 */
	ERRORE_RECUPERO_NOME_TIPOLOGIA(2, "Errore in fase di recupero del nome della tipologia di documento di riferimento."), // Errore configurazione

	/**
	 * Valore.
	 */
	ERRORE_CONFIGURAZIONE_NOME_TIPOLOGIA(3, "Errore in fase di recupero dei metadati associati alla tipologia di documento di riferimento."), // Errore configurazione

	/**
	 * Valore.
	 */
	VALORE_NON_VALIDO(4, "Sono stati forniti dei valori non conformi alle aspettative per i seguenti metadati: <<METADATI>>"), // Il valore che hai fornito per un metadato non
																																// va bene

	/**
	 * Valore.
	 */
	METADATO_SCONOSCIUTO(5, "I seguenti metadati non sono attesi per la tipologia di documento di riferimento: <<METADATI>>"), // Mi hai dato un metadato di troppo

	/**
	 * Valore.
	 */
	VALORE_NON_PRESENTE(6, "Non è stato fornito un valore per i seguenti metadati obbligatori per la tipologia di documento di riferimento: <<METADATI>>"), // Mi aspetto un
																																							// metadato e non
																																							// me lo dai

	/**
	 * Valore.
	 */
	ID_PROCESSO_ESISTENTE(7, "Identificativo del processo utilizzato in precedenza."), // Nel flusso si avvia un nuovo processo utilizzando un id preesistente

	/**
	 * Valore.
	 */
	ID_PROCESSO_NON_ESISTENTE(8, "Identificativo del processo sconosciuto."), // Nel flusso si lavora un processo ritenuto preesistente, ma che in realtà non
																				// è presente

	/**
	 * Valore.
	 */
	STATO_DOCUMENTO_NON_CONFORME(9, "Lo stato del documento non è conforme alle aspettative."), // Nel flusso si richiede di eseguire una operazione su di un documento in uno
																								// stato non compatibile

	/**
	 * Valore.
	 */
	ID_MESSAGGIO_NON_CONFORME(10, "Identificativo messaggio non conforme per il flusso."), // Nel flusso ci sono degli ID di messaggi normati (101, 102, etc.), e quello
																							// fornito non è conforme

	/**
	 * Valore.
	 */
	ID_FASCICOLO_FEPA_NON_PRESENTE(11, "Identificativo del fascicolo FEPA non presente"),

	/**
	 * Valore.
	 */
	CAPITOLO_NON_PRESENTE_IN_ANAGRAFICA(12, "Capitolo non presente in anagrafica"),

	/**
	 * Valore.
	 */
	CODICE_FLUSSO_NON_PRESENTE(13, "Codice Flusso non presente"),

	/**
	 * Valore.
	 */
	GENERICO(99, "Errore generico"); // Errore generico

	/**
	 * Valore.
	 */
	private Integer value;

	/**
	 * Descrizione.
	 */
	private String description;

	/**
	 * Costruttore.
	 * 
	 * @param inValue       valore
	 * @param inDescription descrizione
	 */
	ErroriAttrExtEnum(final Integer inValue, final String inDescription) {
		this.value = inValue;
		this.description = inDescription;
	}

	/**
	 * Getter descrizione.
	 * 
	 * @return descrizione
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Getter valore.
	 * 
	 * @return valore
	 */
	public Integer getValue() {
		return value;
	}

}
