package it.ibm.red.business.service.concrete;

import java.sql.Connection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IRuoloDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.Ruolo;
import it.ibm.red.business.service.ILookupSRV;

/**
 * The Class LookupSRV.
 *
 * @author CPIERASC
 * 
 *         Servizio gestione lookup.
 */
@Service
@Component
public class LookupSRV extends AbstractService implements ILookupSRV {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(LookupSRV.class.getName());
	
	/**
	 * Dao gestione nodo.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * Dao gestione ruolo.
	 */
	@Autowired
	private IRuoloDAO ruoloDAO;
	
	/**
	 * Metodo per il recupero dell'uffico partendo da un id.
	 *
	 * @param id
	 *            the id
	 * @return the ufficio
	 */
	@Override
	public final Nodo getUfficio(final Long id) {
		Connection connection =  null;
		
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			
			return getUfficio(id, connection);
		} catch (Exception e) {
			LOGGER.error(e);
		} finally {
			closeConnection(connection);
		}
		
		return null;
	}
	
	/**
	 * Metodo per il recupero dell'uffico partendo da un id.
	 *
	 * @param id
	 *            the id
	 * @return the ufficio
	 * @throws Exception 
	 */
	@Override
	public final Nodo getUfficio(final Long id, final Connection inConnection) {
		Connection connection = inConnection;
		boolean myConn = true;
		if (connection != null) {
			myConn = false;
		}
		
		try {
			if (myConn) {
				connection = setupConnection(getDataSource().getConnection(), false);
			}
			
			return nodoDAO.getNodo(id, connection);
		
		} catch (Exception e) {
			LOGGER.error(e);
			if (myConn) {
				rollbackConnection(connection);
			} else {
				throw new RedException(e);
			}
		} finally {
			if (myConn) {
				closeConnection(connection);
			}
		}
		
		return null;
	}
	
	/**
	 * Metodo per il recupero di un ruolo partendo da un id.
	 *
	 * @param id
	 *            the id
	 * @return the ruolo
	 */
	@Override
	public final Ruolo getRuolo(final Long id) {
		Connection connection =  null;
		
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			
			return ruoloDAO.getRuolo(id, connection);
		} catch (Exception e) {
			LOGGER.error(e);
		} finally {
			closeConnection(connection);
		}
		
		return null;
	}

}
