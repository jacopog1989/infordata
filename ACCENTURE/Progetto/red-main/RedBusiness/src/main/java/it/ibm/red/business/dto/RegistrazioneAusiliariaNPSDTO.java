package it.ibm.red.business.dto;

import java.util.Date;

/**
 * DTO che definisce le caratteristiche di una registrazione ausiliaria NPS.
 */
public class RegistrazioneAusiliariaNPSDTO extends AbstractDTO {

	/**
	 * La costante serial Version UID.
	 */
	private static final long serialVersionUID = 5594470442836101825L;

	/**
	 * Id.
	 */
	private String id;
	
	/**
	 * Numero registrazione.
	 */
	private Integer numeroRegistrazione;
	
	/**
	 * Codice registro.
	 */
	private String codiceRegistro;
	
	/**
	 * Guid documento.
	 */
	private String guidDocumento;
	
	/**
	 * Data registrazione.
	 */
	private Date dataRegistrazione;
	
	/**
	 * Oggetto.
	 */
	private String oggetto;
	
	/**
	 * Data annullamento.
	 */
	private Date dataAnnullamento;
	
	/**
	 * Motivo annullamento.
	 */
	private String motivoAnnullamento;
	
	/**
	 * Descrizione tipologia documento.
	 */
	private String descrizioneTipologiaDocumento;
	
	/**
	 * Aoo.
	 */
	private Long idAoo;
	
	/**
	 * Flag annullato.
	 */
	private Boolean annullato;
	
	
	/**
	 * Costruttore vuoto.
	 */
	public RegistrazioneAusiliariaNPSDTO() {
		super();
	}

	/**
	 * Restituisce l'id della registrazione ausiliaria.
	 * 
	 * @return id della registrazione
	 */
	public String getId() {
		return id;
	}

	/**
	 * Imposta l'id della registrazione ausiliaria.
	 * @param id id della registrazione ausiliaria da impostare
	 */
	public void setId(final String id) {
		this.id = id;
	}

	/**
	 * Restituisce il numero della registrazione ausiliaria.
	 * 
	 * @return numero registrazione ausiliaria
	 */
	public Integer getNumeroRegistrazione() {
		return numeroRegistrazione;
	}

	/**
	 * Imposta il numero della registrazione ausiliaria.
	 * 
	 * @param numeroRegistrazione
	 *            il numero della registrazione ausiliaria da impostare
	 */
	public void setNumeroRegistrazione(final Integer numeroRegistrazione) {
		this.numeroRegistrazione = numeroRegistrazione;
	}

	/**
	 * Restituisce il codice del registro ausiliario associato alla registrazione
	 * ausiliaria.
	 * 
	 * @return codice registro ausiliario
	 */
	public String getCodiceRegistro() {
		return codiceRegistro;
	}

	/**
	 * Imposta il codice del registro ausiliario.
	 * 
	 * @param codiceRegistro
	 *            codice registro ausiliario da impostare
	 */
	public void setCodiceRegistro(final String codiceRegistro) {
		this.codiceRegistro = codiceRegistro;
	}

	/**
	 * Restituisce il guid del documento al quale è associata la registrazione
	 * ausiliaria.
	 * 
	 * @return guid del documento
	 */
	public String getGuidDocumento() {
		return guidDocumento;
	}

	/**
	 * Imposta il guid del documento al quale è associata la registrazione
	 * ausiliaria.
	 * 
	 * @param guidDocumento
	 *            guid documento da impostare
	 */
	public void setGuidDocumento(final String guidDocumento) {
		this.guidDocumento = guidDocumento;
	}

	/**
	 * Restituisce la motivazione di annullamento.
	 * 
	 * @return motivo annullamento
	 */
	public String getMotivoAnnullamento() {
		return motivoAnnullamento;
	}

	/**
	 * Imposta la motivazione dell'annullamento.
	 * 
	 * @param motivoAnnullamento
	 *            motivazione annullamento da impostare
	 */
	public void setMotivoAnnullamento(final String motivoAnnullamento) {
		this.motivoAnnullamento = motivoAnnullamento;
	}

	/**
	 * Restituisce la data della registrazione ausiliaria.
	 * 
	 * @return data registrazione ausiliaria
	 */
	public Date getDataRegistrazione() {
		return dataRegistrazione;
	}

	/**
	 * Imposta la data della registrazione ausiliaria.
	 * 
	 * @param dataRegistrazione
	 *            data registrazione ausiliaria
	 */
	public void setDataRegistrazione(final Date dataRegistrazione) {
		this.dataRegistrazione = dataRegistrazione;
	}

	/**
	 * Restituisce la data di annullamento della registrazione ausiliaria.
	 * 
	 * @return data annullamento
	 */
	public Date getDataAnnullamento() {
		return dataAnnullamento;
	}

	/**
	 * Imposta la data di annullamento della registrazione ausiliaria.
	 * 
	 * @param dataAnnullamento
	 *            data annullamento da impostare
	 */
	public void setDataAnnullamento(final Date dataAnnullamento) {
		this.dataAnnullamento = dataAnnullamento;
	}

	/**
	 * Restituisce l'oggetto della registrazione ausiliario.
	 * 
	 * @return oggetto registrazione ausiliaria
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * Imposta l'oggetto della registrazione ausiliaria.
	 * 
	 * @param oggetto
	 *            oggetto da impostare
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}

	/**
	 * Restituisce la descrizione della tipologia documento del documento su cui
	 * viene predisposta la registrazione ausiliaria.
	 * 
	 * @return la descrizione della tipologia del documento
	 */
	public String getDescrizioneTipologiaDocumento() {
		return descrizioneTipologiaDocumento;
	}

	/**
	 * Imposta la descrizione della tipologia documento.
	 * 
	 * @param descrizioneTipologiaDocumento
	 *            la descrizione della tipologia da impostare
	 */
	public void setDescrizioneTipologiaDocumento(final String descrizioneTipologiaDocumento) {
		this.descrizioneTipologiaDocumento = descrizioneTipologiaDocumento;
	}

	/**
	 * Restituisce l'id dell'area organizzativa.
	 * 
	 * @return identificativo AOO
	 */
	public Long getIdAoo() {
		return idAoo;
	}

	/**
	 * Imposta l'id dell'area organizzativa.
	 * 
	 * @param idAoo
	 *            identificativo dell'AOO da impostare
	 */
	public void setIdAoo(final Long idAoo) {
		this.idAoo = idAoo;
	}

	/**
	 * Restituisce <code> true </code> se la registrazione è annullata,
	 * <code> false </code> altrimenti.
	 * 
	 * @return <code> true </code> se la registrazione è annullata,
	 *         <code> false </code> altrimenti
	 */
	public Boolean getAnnullato() {
		return annullato;
	}

	/**
	 * Imposta il flag che definisce l'annullamento della registrazione ausiliaria.
	 * 
	 * @param annullato
	 *            il flag da impostare
	 */
	public void setAnnullato(final Boolean annullato) {
		this.annullato = annullato;
	}

	/**
	 * Hashcode per la registrazione ausiliaria.
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	/**
	 * Equals della registrazione ausiliaria.
	 * 
	 * @param obj
	 *            registrazione ausiliaria da confrontare
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final RegistrazioneAusiliariaNPSDTO other = (RegistrazioneAusiliariaNPSDTO) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
	
	
}
