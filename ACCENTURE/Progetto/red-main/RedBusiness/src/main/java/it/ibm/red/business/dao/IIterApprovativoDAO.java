package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.IterApprovativoDTO;

/**
 * 
 * @author CRISTIANOPierascenzi
 *
 */
public interface IIterApprovativoDAO extends Serializable {

	/**
	 * Recupero nome workflow.
	 * 
	 * @param idIterApprovativo	identificativo iter approvativo
	 * @param con				connessione
	 * @return					nome del workflow
	 */
	String getWFName(int idIterApprovativo, Connection con);

	/**
	 * Restituisce il firmatario relativo all'iter di firma in input per l'ufficio in input.
	 * @param idNodo
	 * @param tipoFirma
	 * @param connection
	 * @return
	 */
	String getFirmatario(Long idNodo, Integer tipoFirma, Connection connection);
	
	/**
	 * Ottiene il tipo firma dell'iter approvativo.
	 * @param idAoo
	 * @param wfName
	 * @param con
	 * @return tipo firma
	 */
	Integer getTipoFirma(Integer idAoo, String wfName, Connection con);
	
	/**
	 * Ottiene tutti gli iter approvativi tramite l'id dell'aoo.
	 * @param idAoo
	 * @param con
	 * @return lista degli iter approvativi
	 */
	List<IterApprovativoDTO> getAllByIdAOO(Integer idAoo, Connection con);
	
	/**
	 * Ottiene l'iter approvativo tramite l'id.
	 * @param id
	 * @param con
	 * @return iter approvativo
	 */
	IterApprovativoDTO getIterApprovativoById(Integer id, Connection con);
	
	/**
	 * Ottiene il nome generico del workflow.
	 * @param idAoo
	 * @param con
	 * @return nome generico del workflow
	 */
	String getWFNameGenerico(long idAoo, Connection con);
}