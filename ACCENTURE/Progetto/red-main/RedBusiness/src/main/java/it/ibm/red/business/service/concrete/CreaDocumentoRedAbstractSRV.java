package it.ibm.red.business.service.concrete;

import java.sql.Connection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedParametriDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ProvenienzaSalvaDocumentoEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.service.ISalvaDocumentoSRV;
import it.ibm.red.business.service.IUtenteSRV;

/**
 * @author m.crescentini
 * 
 * Servizio per la creazione di un documento.
 *
 */
@Service
public abstract class CreaDocumentoRedAbstractSRV extends AbstractService {

	private static final long serialVersionUID = 8607035748938103289L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(CreaDocumentoRedAbstractSRV.class.getName());
	
	/**
	 * Service.
	 */
	@Autowired
	private ISalvaDocumentoSRV salvaDocumentoSRV;
	
	/**
	 * Service.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;
	
	/**
	 * DAO.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;


	/**
	 * @return
	 */
	protected abstract ProvenienzaSalvaDocumentoEnum getProvenienza();
	

	/**
	 * @param documento
	 * @param parametri
	 * @param idUtente
	 * @param idRuolo
	 * @param idUfficio
	 * @param con
	 * @return
	 */
	protected EsitoSalvaDocumentoDTO creaDocumentoRed(final DetailDocumentRedDTO documento, final SalvaDocumentoRedParametriDTO parametri, 
			final Long idUtente, final Long idRuolo, final Long idUfficio, final Connection con) {
		// Recupero dell'utente
		final UtenteDTO utente = getUtente(idUtente, idRuolo, idUfficio, con);
		
		return creaDocumentoRed(documento, parametri, utente);
	}
	

	/**
	 * @param documento
	 * @param parametri
	 * @param utente
	 * @return
	 */
	protected EsitoSalvaDocumentoDTO creaDocumentoRed(final DetailDocumentRedDTO documento, final SalvaDocumentoRedParametriDTO parametri, final UtenteDTO utente) {
		// Recupero del chiamante
		final ProvenienzaSalvaDocumentoEnum provenienza = getProvenienza();
		LOGGER.info("creaDocumentoRed -> Provenienza: " + provenienza);
		
		return salvaDocumentoSRV.salvaDocumento(documento, parametri, utente, provenienza);
	}


	/**
	 * @param idUtente
	 * @param idRuolo
	 * @param idNodo
	 * @param connection
	 * @return
	 */
	protected UtenteDTO getUtente(final Long idUtente, final Long idRuolo, final Long idNodo, final Connection connection) {
		final Utente utenteBean = utenteDAO.getUtente(idUtente, connection);
		
		return utenteSRV.getByUtente(utenteBean, idRuolo, idNodo, connection);
	}
	
}