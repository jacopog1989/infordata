package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Facade del servizio che gestisce l'eliminazione dei precensiti.
 */
public interface IEliminaPrecensitiFacadeSRV extends Serializable {
	
	/**
	 * Elimina il documento precensito.
	 * @param utente
	 * @param wobNumber
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO eliminaPrecensito(UtenteDTO utente, String wobNumber);
	
	/**
	 * Elimina i documenti precensiti.
	 * @param utente
	 * @param wobNumbers
	 * @return esiti dell'operazione
	 */
	Collection<EsitoOperazioneDTO> eliminaPrecensiti(UtenteDTO utente, Collection<String> wobNumbers);

}
