package it.ibm.red.business.helper.notifiche.states;

import java.util.Map;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.enums.StatoCodaEmailEnum;
import it.ibm.red.business.helper.notifiche.IGestioneMailState;
import it.ibm.red.business.helper.notifiche.NotificaHelper;
import it.ibm.red.business.persistence.model.CodaEmail;

/**
 * Stato 5 - ReInvio.
 */
public class ReInvioState5 implements IGestioneMailState {

	/**
	 * @see it.ibm.red.business.helper.notifiche.IGestioneMailState#getNextNotificaEmailToUpdate(it.ibm.red.business.helper.notifiche.NotificaHelper,
	 *      it.ibm.red.business.persistence.model.CodaEmail, java.util.Map,
	 *      boolean).
	 */
	@Override
	public CodaEmail getNextNotificaEmailToUpdate(final NotificaHelper nh, final CodaEmail notificaEmail, final Map<String, EmailDTO> notifiche, final boolean reInvio) {
		CodaEmail result = notificaEmail;
		
		IGestioneMailState nextState = new ReInvioState5();
		int nextStatoRicevuta =  notificaEmail.getStatoRicevuta();
			
		int tipoMittente = notificaEmail.getTipoMittente();
		
		if (((tipoMittente == Constants.TipologiaDestinatariMittenti.TIPOLOGIA_DESTINATARI_MITTENTI_PEC)
				 && reInvio)) {
			nextState = new InizioState0();
			nextStatoRicevuta = StatoCodaEmailEnum.DA_ELABORARE.getStatus();
		}
		
		nh.setGestioneMailState(nextState);
		result.setStatoRicevuta(nextStatoRicevuta);
		
		return result;
	}

}
