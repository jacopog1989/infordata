package it.ibm.red.business.service;

/**
 * Interface del servizio di gestione dei conti consuntivi.
 * 
 * @author SimoneLungarella
 */
public interface IContiConsuntiviSRV extends IContiConsuntiviFacadeSRV {

}
