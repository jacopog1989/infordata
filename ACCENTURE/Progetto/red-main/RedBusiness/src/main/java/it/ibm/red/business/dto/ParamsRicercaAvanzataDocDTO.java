package it.ibm.red.business.dto;

import java.util.Calendar;
import java.util.Date;

import it.ibm.red.business.enums.ModalitaRicercaAvanzataTestoEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;


/**
 * DTO per la ricerca avanzata dei documenti.
 * 
 * @author m.crescentini
 *
 */
public class ParamsRicercaAvanzataDocDTO extends AbstractDTO {

	/**
	 * Serial version UID.
	 */
	private static final long serialVersionUID = -7536538789913565390L;

	/**
	 * Anno documento 
	 */
	private Integer annoDocumento;
	
	/**
	 * Flag per disabilitare anno documento 
	 */
	private boolean annoDocumentoDisabled;
	
	/**
	 * Numero documento 
	 */
	private Integer numeroDocumento;
	
	/**
	 * Barcode 
	 */
	private String barcode;
	
	/**
	 * Data creazione da 
	 */
	private Date dataCreazioneDa;
	
	/**
	 * Data creazione a 
	 */
	private Date dataCreazioneA;
	
	// Prelex
	/**
	 * Numero legislatura 
	 */
	private Integer numeroLegislatura;
	
	/**
	 * Numero RDP 
	 */
	private String numeroRDP;
	
	/***** PROPRIETA BASE -> START *****/
	// Categoria
	/**
	 * Flag entrata 
	 */
	private boolean entrata;
	
	/**
	 * Flag interno 
	 */
	private boolean interno;
	
	/**
	 * Flag uscita 
	 */
	private boolean uscita;
	
	/**
	 * Flag tutto 
	 */
	private boolean tutto;
	
	// Livello di riservatezza
	/**
	 * Flag riservato 
	 */
	private boolean riservato;
	
	/**
	 * Flag registro riservato 
	 */
	private boolean registroRiservato;
	
	// Altro
	/**
	 * Flag urgente 
	 */
	private boolean urgente;
	
	/**
	 * Flag annullati 
	 */
	private boolean annullati;
	
	/**
	 * Oggetto 
	 */
	private String oggetto;
	
	/**
	 * Descrizione titolario 
	 */
	private String descrizioneTitolario;
	
	/**
	 * Descrizione ufficio competenza 
	 */
	private String descrUfficioComp;
	
	/**
	 * Descrizione ufficio assegnazione 
	 */
	private String descrUfficioAss;
	
	/**
	 * Descrizione ufficio firma 
	 */
	private String descrUfficioFirm;
	
	/**
	 * Descrizione assegnatario 
	 */
	private String descrAssegnatario;
	
	/**
	 * Utente Protocollatore 
	 */
	private String protocollatore;
	
	/**
	 * Nome fascicolo 
	 */
	private String nomeFascicolo;
	
	/**
	 * Descrizione tipologia documento 
	 */
	private String descrizioneTipologiaDocumento;
	
	/**
	 * Descrizione tipo procedimento 
	 */
	private String descrizioneTipoProcedimento;
	
	/**
	 * Mittente 
	 */
	private String mittente;
	
	/**
	 * Destinatario 
	 */
	private String destinatario;
	
	/**
	 * Identificativo mezzo ricezione 
	 */
	private Integer idMezzoRicezione;
	/***** PROPRIETA BASE -> END *****/
	
	/*** PROPRIETA PROTOCOLLO -> START ***/
	
	/**
	 * Numero protocollo da 
	 */
	private Integer numeroProtocolloDa;
	
	/**
	 * Numero protocollo a 
	 */
	private Integer numeroProtocolloA;

	/**
	 * Numero protocollo emergenza da 
	 */
	private Integer numeroProtocolloEmergenzaDa;
	
	/**
	 * Numero protocollo emergenza a 
	 */
	private Integer numeroProtocolloEmergenzaA;

	/**
	 * Anno protocollo da 
	 */
	private Integer annoProtocolloDa;
	
	/**
	 * Anno protocollo a 
	 */
	private Integer annoProtocolloA;
	
	/**
	 * Data protocollo da 
	 */
	private Date dataProtocolloDa;
	
	/**
	 * Data protocollo a 
	 */
	private Date dataProtocolloA;
	
	/**
	 * Data protocollo emergenza da 
	 */
	private Date dataProtocolloEmergenzaDa;
	
	/**
	 * Data protocollo emergenza a 
	 */
	private Date dataProtocolloEmergenzaA;
	
	/**
	 * Riferimento protocollo mittente 
	 */
	private Integer rifProtocolloMittente;
	/*** PROPRIETA PROTOCOLLO -> END ***/

	/***** PROPRIETA AVANZATE -> START *****/
	
	/**
	 * Note 
	 */
	private String note;
	
	/**
	 * Data scadenza da 
	 */
	private Date dataScadenzaDa;
	
	/**
	 * Data scadenza a 
	 */
	private Date dataScadenzaA;
	
	/**
	 * Tipo assegnazione 
	 */
	private TipoAssegnazioneEnum tipoAssegnazione;
	
	/**
	 * Assegnatario 
	 */
	private AssegnatarioOrganigrammaDTO assegnatario;
	
	/**
	 * Identificativo ufficio responsabile copia conforme 
	 */
	private Long idUfficioResponsabileCopiaConforme;
	
	/**
	 * Identificativo utente responsabile copia conforme 
	 */
	private Long idUtenteResponsabileCopiaConforme;
	
	/**
	 * Modalita ricerca testo 
	 */
	private ModalitaRicercaAvanzataTestoEnum modalitaRicercaTesto;
	
	/**
	 * Parole ricerca testo 
	 */
	private String paroleRicercaTesto;
	
	/**
	 * Identificativo tipo operazine 
	 */
	private Integer idTipoOperazione;
	
	/**
	 * Assegnatario tipo operazione 
	 */
	private AssegnatarioOrganigrammaDTO assegnatarioTipoOperazione;
	
	/**
	 * Descrizione assegnatario operazione 
	 */
	private String descrAssegnatarioOperazione;
	/***** PROPRIETA AVANZATE -> END *****/

	/**
	 * Descrizione registro repertorio 
	 */
	private String descrizioneRegistroRepertorio;
	
	
	/**
	 * Costruttore params ricerca avanzata doc DTO.
	 */
	public ParamsRicercaAvanzataDocDTO() {
		this.annoDocumento = Calendar.getInstance().get(Calendar.YEAR);
	}
	
	/**
	 * Costruttore copia.
	 * 
	 * @param paramsToCopy
	 *            DTO da copiare.
	 */
	public ParamsRicercaAvanzataDocDTO(ParamsRicercaAvanzataDocDTO paramsToCopy) {
		super();
		this.annoDocumento = paramsToCopy.getAnnoDocumento();
		this.annoDocumentoDisabled = paramsToCopy.isAnnoDocumentoDisabled();
		this.numeroDocumento = paramsToCopy.getNumeroDocumento();
		this.barcode = paramsToCopy.getBarcode();
		this.dataCreazioneDa = paramsToCopy.getDataCreazioneDa();
		this.dataCreazioneA = paramsToCopy.getDataCreazioneA();
		this.numeroLegislatura = paramsToCopy.getNumeroLegislatura();
		this.numeroRDP = paramsToCopy.getNumeroRDP();
		this.entrata = paramsToCopy.isEntrata();
		this.interno = paramsToCopy.isInterno();
		this.uscita = paramsToCopy.isUscita();
		this.tutto = paramsToCopy.isTutto();
		this.riservato = paramsToCopy.isRiservato();
		this.registroRiservato = paramsToCopy.isRegistroRiservato();
		this.urgente = paramsToCopy.isUrgente();
		this.annullati = paramsToCopy.isAnnullati();
		this.oggetto = paramsToCopy.getOggetto();
		this.descrizioneTitolario = paramsToCopy.getDescrizioneTitolario();
		this.descrUfficioComp = paramsToCopy.getDescrUfficioComp();
		this.descrUfficioAss = paramsToCopy.getDescrUfficioAss();
		this.descrUfficioFirm = paramsToCopy.getDescrUfficioFirm();
		this.descrAssegnatario = paramsToCopy.getDescrAssegnatario();
		this.protocollatore = paramsToCopy.getProtocollatore();
		this.nomeFascicolo = paramsToCopy.getNomeFascicolo();
		this.descrizioneTipologiaDocumento = paramsToCopy.getDescrizioneTipologiaDocumento();
		this.descrizioneTipoProcedimento = paramsToCopy.getDescrizioneTipoProcedimento();
		this.mittente = paramsToCopy.getMittente();
		this.destinatario = paramsToCopy.getDestinatario();
		this.idMezzoRicezione = paramsToCopy.getIdMezzoRicezione();
		this.numeroProtocolloDa = paramsToCopy.getNumeroProtocolloDa();
		this.numeroProtocolloA = paramsToCopy.getNumeroProtocolloA();
		this.numeroProtocolloEmergenzaDa = paramsToCopy.getNumeroProtocolloEmergenzaDa();
		this.numeroProtocolloEmergenzaA = paramsToCopy.getNumeroProtocolloEmergenzaA();
		this.annoProtocolloDa = paramsToCopy.getAnnoProtocolloDa();
		this.annoProtocolloA = paramsToCopy.getAnnoProtocolloA();
		this.dataProtocolloDa = paramsToCopy.getDataProtocolloDa();
		this.dataProtocolloA = paramsToCopy.getDataProtocolloA();
		this.dataProtocolloEmergenzaDa = paramsToCopy.getDataProtocolloEmergenzaDa();
		this.dataProtocolloEmergenzaA = paramsToCopy.getDataProtocolloEmergenzaA();
		this.rifProtocolloMittente = paramsToCopy.getRifProtocolloMittente();
		this.note = paramsToCopy.getNote();
		this.dataScadenzaDa = paramsToCopy.getDataScadenzaDa();
		this.dataScadenzaA = paramsToCopy.getDataScadenzaA();
		this.tipoAssegnazione = paramsToCopy.getTipoAssegnazione();
		this.assegnatario = paramsToCopy.getAssegnatario();
		this.idUfficioResponsabileCopiaConforme = paramsToCopy.getIdUfficioResponsabileCopiaConforme();
		this.idUtenteResponsabileCopiaConforme = paramsToCopy.getIdUtenteResponsabileCopiaConforme();
		this.modalitaRicercaTesto = paramsToCopy.getModalitaRicercaTesto();
		this.paroleRicercaTesto = paramsToCopy.getParoleRicercaTesto();
		this.idTipoOperazione = paramsToCopy.getIdTipoOperazione();
		this.assegnatarioTipoOperazione = paramsToCopy.getAssegnatarioTipoOperazione();
		this.descrAssegnatarioOperazione = paramsToCopy.getDescrAssegnatarioOperazione();
		this.descrizioneRegistroRepertorio = paramsToCopy.getDescrizioneRegistroRepertorio();
	}

	/** 
	 *
	 * @return the descr ufficio comp
	 */
	public String getDescrUfficioComp() {
	    return descrUfficioComp;
	}

	/** 
	 *
	 * @param descrUfficioComp the new descr ufficio comp
	 */
	public void setDescrUfficioComp(final String descrUfficioComp) {
	    this.descrUfficioComp = descrUfficioComp;
	}

	/** 
	 *
	 * @return the descr ufficio ass
	 */
	public String getDescrUfficioAss() {
	    return descrUfficioAss;
	}

	/** 
	 *
	 * @param descrUfficioAss the new descr ufficio ass
	 */
	public void setDescrUfficioAss(final String descrUfficioAss) {
	    this.descrUfficioAss = descrUfficioAss;
	}

	/** 
	 *
	 * @return the descr ufficio firm
	 */
	public String getDescrUfficioFirm() {
	    return descrUfficioFirm;
	}

	/** 
	 *
	 * @param descrUfficioFirm the new descr ufficio firm
	 */
	public void setDescrUfficioFirm(final String descrUfficioFirm) {
	    this.descrUfficioFirm = descrUfficioFirm;
	}

	/** 
	 *
	 * @return the protocollatore
	 */
	public String getProtocollatore() {
	    return protocollatore;
	}

	/** 
	 *
	 * @param protocollatore the new protocollatore
	 */
	public void setProtocollatore(final String protocollatore) {
	    this.protocollatore = protocollatore;
	}

	/** 
	 *
	 * @return the descrizione titolario
	 */
	public String getDescrizioneTitolario() {
	    return descrizioneTitolario;
	}

	/** 
	 *
	 * @param descrizioneTitolario the new descrizione titolario
	 */
	public void setDescrizioneTitolario(final String descrizioneTitolario) {
	    this.descrizioneTitolario = descrizioneTitolario;
	}

	
	/** 
	 *
	 * @return the descr ufficio
	 */
	public String getDescrUfficio() {
	    return descrUfficioComp;
	}

	/** 
	 *
	 * @param descrUfficio the new descr ufficio
	 */
	public void setDescrUfficio(final String descrUfficio) {
	    this.descrUfficioComp = descrUfficio;
	}

	/** 
	 *
	 * @return the annoDocumento
	 */
	public Integer getAnnoDocumento() {
		return annoDocumento;
	}
	
	/** 
	 *
	 * @param annoDocumento the annoDocumento to set
	 */
	public void setAnnoDocumento(final Integer annoDocumento) {
		this.annoDocumento = annoDocumento;
	}
	
	/** 
	 *
	 * @return the numeroDocumento
	 */
	public Integer getNumeroDocumento() {
		return numeroDocumento;
	}
	
	/** 
	 *
	 * @param numeroDocumento the numeroDocumento to set
	 */
	public void setNumeroDocumento(final Integer numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}
	
	/** 
	 *
	 * @return the barcode
	 */
	public String getBarcode() {
		return barcode;
	}
	
	/** 
	 *
	 * @param barcode the barcode to set
	 */
	public void setBarcode(final String barcode) {
		this.barcode = barcode;
	}
	
	/** 
	 *
	 * @return the dataCreazioneDa
	 */
	public Date getDataCreazioneDa() {
		return dataCreazioneDa;
	}
	
	/** 
	 *
	 * @param dataCreazioneDa the dataCreazioneDa to set
	 */
	public void setDataCreazioneDa(final Date dataCreazioneDa) {
		this.dataCreazioneDa = dataCreazioneDa;
	}
	
	/** 
	 *
	 * @return the dataCreazioneA
	 */
	public Date getDataCreazioneA() {
		return dataCreazioneA;
	}
	
	/** 
	 *
	 * @param dataCreazioneA the dataCreazioneA to set
	 */
	public void setDataCreazioneA(final Date dataCreazioneA) {
		this.dataCreazioneA = dataCreazioneA;
	}
	
	/** 
	 *
	 * @return the numeroLegislatura
	 */
	public Integer getNumeroLegislatura() {
		return numeroLegislatura;
	}
	
	/** 
	 *
	 * @param numeroLegislatura the numeroLegislatura to set
	 */
	public void setNumeroLegislatura(final Integer numeroLegislatura) {
		this.numeroLegislatura = numeroLegislatura;
	}
	
	/** 
	 *
	 * @return the numeroRDP
	 */
	public String getNumeroRDP() {
		return numeroRDP;
	}
	
	/** 
	 *
	 * @param numeroRDP the numeroRDP to set
	 */
	public void setNumeroRDP(final String numeroRDP) {
		this.numeroRDP = numeroRDP;
	}
	
	/** 
	 *
	 * @return the entrata
	 */
	public boolean isEntrata() {
		return entrata;
	}
	
	/** 
	 *
	 * @param entrata the entrata to set
	 */
	public void setEntrata(final boolean entrata) {
		this.entrata = entrata;
	}
	
	/** 
	 *
	 * @return the interno
	 */
	public boolean isInterno() {
		return interno;
	}
	
	/** 
	 *
	 * @param interno the interno to set
	 */
	public void setInterno(final boolean interno) {
		this.interno = interno;
	}
	
	/** 
	 *
	 * @return the uscita
	 */
	public boolean isUscita() {
		return uscita;
	}
	
	/** 
	 *
	 * @param uscita the uscita to set
	 */
	public void setUscita(final boolean uscita) {
		this.uscita = uscita;
	}
	
	/**
	 * Checks if is riservato.
	 *
	 * @return the riservato
	 */
	public boolean isRiservato() {
		return riservato;
	}
	
	/** 
	 *
	 * @param riservato the riservato to set
	 */
	public void setRiservato(final boolean riservato) {
		this.riservato = riservato;
	}
	
	/** 
	 *
	 * @return the registroRiservato
	 */
	public boolean isRegistroRiservato() {
		return registroRiservato;
	}
	
	/** 
	 *
	 * @param registroRiservato the registroRiservato to set
	 */
	public void setRegistroRiservato(final boolean registroRiservato) {
		this.registroRiservato = registroRiservato;
	}
	
	/** 
	 *
	 * @return the urgente
	 */
	public boolean isUrgente() {
		return urgente;
	}
	
	/** 
	 *
	 * @param urgente the urgente to set
	 */
	public void setUrgente(final boolean urgente) {
		this.urgente = urgente;
	}
	
	/** 
	 *
	 * @return the annullati
	 */
	public boolean isAnnullati() {
		return annullati;
	}
	
	/** 
	 *
	 * @param annullati the annullati to set
	 */
	public void setAnnullati(final boolean annullati) {
		this.annullati = annullati;
	}
	
	/** 
	 *
	 * @return the oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}
	
	/** 
	 *
	 * @param oggetto the oggetto to set
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}
	
	/** 
	 *
	 * @return the nomeFascicolo
	 */
	public String getNomeFascicolo() {
		return nomeFascicolo;
	}
	
	/** 
	 *
	 * @param nomeFascicolo the nomeFascicolo to set
	 */
	public void setNomeFascicolo(final String nomeFascicolo) {
		this.nomeFascicolo = nomeFascicolo;
	}
	
	/** 
	 *
	 * @return the descrizioneTipologiaDocumento
	 */
	public String getDescrizioneTipologiaDocumento() {
		return descrizioneTipologiaDocumento;
	}

	/** 
	 *
	 * @param descrizioneTipologiaDocumento the descrizioneTipologiaDocumento to set
	 */
	public void setDescrizioneTipologiaDocumento(final String descrizioneTipologiaDocumento) {
		this.descrizioneTipologiaDocumento = descrizioneTipologiaDocumento;
	}

	/** 
	 *
	 * @return the descrizioneTipoProcedimento
	 */
	public String getDescrizioneTipoProcedimento() {
		return descrizioneTipoProcedimento;
	}

	/** 
	 *
	 * @param descrizioneTipoProcedimento the descrizioneTipoProcedimento to set
	 */
	public void setDescrizioneTipoProcedimento(final String descrizioneTipoProcedimento) {
		this.descrizioneTipoProcedimento = descrizioneTipoProcedimento;
	}

	/** 
	 *
	 * @return the mittente
	 */
	public String getMittente() {
		return mittente;
	}
	
	/** 
	 *
	 * @param mittente the mittente to set
	 */
	public void setMittente(final String mittente) {
		this.mittente = mittente;
	}
	
	/** 
	 *
	 * @return the destinatario
	 */
	public String getDestinatario() {
		return destinatario;
	}
	
	/** 
	 *
	 * @param destinatario the destinatario to set
	 */
	public void setDestinatario(final String destinatario) {
		this.destinatario = destinatario;
	}
	
	/** 
	 *
	 * @return the idMezzoRicezione
	 */
	public Integer getIdMezzoRicezione() {
		return idMezzoRicezione;
	}
	
	/** 
	 *
	 * @param idMezzoRicezione the idMezzoRicezione to set
	 */
	public void setIdMezzoRicezione(final Integer idMezzoRicezione) {
		this.idMezzoRicezione = idMezzoRicezione;
	}
	
	/** 
	 *
	 * @return the numeroProtocolloDa
	 */
	public Integer getNumeroProtocolloDa() {
		return numeroProtocolloDa;
	}
	
	/** 
	 *
	 * @param numeroProtocolloDa the numeroProtocolloDa to set
	 */
	public void setNumeroProtocolloDa(final Integer numeroProtocolloDa) {
		this.numeroProtocolloDa = numeroProtocolloDa;
	}
	
	/** 
	 *
	 * @return the numeroProtocolloA
	 */
	public Integer getNumeroProtocolloA() {
		return numeroProtocolloA;
	}
	
	/** 
	 *
	 * @param numeroProtocolloA the numeroProtocolloA to set
	 */
	public void setNumeroProtocolloA(final Integer numeroProtocolloA) {
		this.numeroProtocolloA = numeroProtocolloA;
	}
	
	/** 
	 *
	 * @return the dataProtocolloDa
	 */
	public Date getDataProtocolloDa() {
		return dataProtocolloDa;
	}
	
	/** 
	 *
	 * @param dataProtocolloDa the dataProtocolloDa to set
	 */
	public void setDataProtocolloDa(final Date dataProtocolloDa) {
		this.dataProtocolloDa = dataProtocolloDa;
	}
	
	/** 
	 *
	 * @return the dataProtocolloA
	 */
	public Date getDataProtocolloA() {
		return dataProtocolloA;
	}
	
	/** 
	 *
	 * @param dataProtocolloA the dataProtocolloA to set
	 */
	public void setDataProtocolloA(final Date dataProtocolloA) {
		this.dataProtocolloA = dataProtocolloA;
	}
	
	/** 
	 *
	 * @return the rifProtocolloMittente
	 */
	public Integer getRifProtocolloMittente() {
		return rifProtocolloMittente;
	}
	
	/** 
	 *
	 * @param rifProtocolloMittente the rifProtocolloMittente to set
	 */
	public void setRifProtocolloMittente(final Integer rifProtocolloMittente) {
		this.rifProtocolloMittente = rifProtocolloMittente;
	}
	
	/** 
	 *
	 * @return the note
	 */
	public String getNote() {
		return note;
	}
	
	/** 
	 *
	 * @param note the note to set
	 */
	public void setNote(final String note) {
		this.note = note;
	}
	
	/** 
	 *
	 * @return the dataScadenzaDa
	 */
	public Date getDataScadenzaDa() {
		return dataScadenzaDa;
	}
	
	/** 
	 *
	 * @param dataScadenzaDa the dataScadenzaDa to set
	 */
	public void setDataScadenzaDa(final Date dataScadenzaDa) {
		this.dataScadenzaDa = dataScadenzaDa;
	}
	
	/** 
	 *
	 * @return the dataScadenzaA
	 */
	public Date getDataScadenzaA() {
		return dataScadenzaA;
	}
	
	/** 
	 *
	 * @param dataScadenzaA the dataScadenzaA to set
	 */
	public void setDataScadenzaA(final Date dataScadenzaA) {
		this.dataScadenzaA = dataScadenzaA;
	}
	
	/** 
	 *
	 * @return the tipoAssegnazione
	 */
	public TipoAssegnazioneEnum getTipoAssegnazione() {
		return tipoAssegnazione;
	}
	
	/** 
	 *
	 * @param tipoAssegnazione the tipoAssegnazione to set
	 */
	public void setTipoAssegnazione(final TipoAssegnazioneEnum tipoAssegnazione) {
		this.tipoAssegnazione = tipoAssegnazione;
	}
	
	/** 
	 *
	 * @return the assegnatario
	 */
	public AssegnatarioOrganigrammaDTO getAssegnatario() {
		return assegnatario;
	}
	
	/** 
	 *
	 * @param assegnatario the assegnatario to set
	 */
	public void setAssegnatario(final AssegnatarioOrganigrammaDTO assegnatario) {
		this.assegnatario = assegnatario;
	}
	
	/** 
	 *
	 * @return the idUfficioResponsabileCopiaConforme
	 */
	public Long getIdUfficioResponsabileCopiaConforme() {
		return idUfficioResponsabileCopiaConforme;
	}
	
	/** 
	 *
	 * @param idUfficioResponsabileCopiaConforme the idUfficioResponsabileCopiaConforme to set
	 */
	public void setIdUfficioResponsabileCopiaConforme(final Long idUfficioResponsabileCopiaConforme) {
		this.idUfficioResponsabileCopiaConforme = idUfficioResponsabileCopiaConforme;
	}
	
	/** 
	 *
	 * @return the idUtenteResponsabileCopiaConforme
	 */
	public Long getIdUtenteResponsabileCopiaConforme() {
		return idUtenteResponsabileCopiaConforme;
	}

	/** 
	 *
	 * @param idUtenteResponsabileCopiaConforme the idUtenteResponsabileCopiaConforme to set
	 */
	public void setIdUtenteResponsabileCopiaConforme(final Long idUtenteResponsabileCopiaConforme) {
		this.idUtenteResponsabileCopiaConforme = idUtenteResponsabileCopiaConforme;
	}
	
	/** 
	 *
	 * @return the modalitaRicercaTesto
	 */
	public ModalitaRicercaAvanzataTestoEnum getModalitaRicercaTesto() {
		return modalitaRicercaTesto;
	}

	/** 
	 *
	 * @param modalitaRicercaTesto the modalitaRicercaTesto to set
	 */
	public void setModalitaRicercaTesto(final ModalitaRicercaAvanzataTestoEnum modalitaRicercaTesto) {
		this.modalitaRicercaTesto = modalitaRicercaTesto;
	}

	/** 
	 *
	 * @return the paroleRicercaTesto
	 */
	public String getParoleRicercaTesto() {
		return paroleRicercaTesto;
	}
	
	/** 
	 *
	 * @param paroleRicercaTesto the paroleRicercaTesto to set
	 */
	public void setParoleRicercaTesto(final String paroleRicercaTesto) {
		this.paroleRicercaTesto = paroleRicercaTesto;
	}
	
	/** 
	 *
	 * @return the idTipoOperazione
	 */
	public Integer getIdTipoOperazione() {
		return idTipoOperazione;
	}
	
	/** 
	 *
	 * @param idTipoOperazione the idTipoOperazione to set
	 */
	public void setIdTipoOperazione(final Integer idTipoOperazione) {
		this.idTipoOperazione = idTipoOperazione;
	}
	
	/** 
	 *
	 * @return the assegnatarioTipoOperazione
	 */
	public AssegnatarioOrganigrammaDTO getAssegnatarioTipoOperazione() {
		return assegnatarioTipoOperazione;
	}
	
	/** 
	 *
	 * @param assegnatarioTipoOperazione the assegnatarioTipoOperazione to set
	 */
	public void setAssegnatarioTipoOperazione(final AssegnatarioOrganigrammaDTO assegnatarioTipoOperazione) {
		this.assegnatarioTipoOperazione = assegnatarioTipoOperazione;
	}
	
	/** 
	 *
	 * @return the annoProtocolloDa
	 */
	public final Integer getAnnoProtocolloDa() {
		return annoProtocolloDa;
	}

	/** 
	 *
	 * @param annoProtocolloDa the annoProtocolloDa to set
	 */
	public final void setAnnoProtocolloDa(final Integer annoProtocolloDa) {
		this.annoProtocolloDa = annoProtocolloDa;
	}

	/** 
	 *
	 * @return the annoProtocolloA
	 */
	public final Integer getAnnoProtocolloA() {
		return annoProtocolloA;
	}

	/** 
	 *
	 * @param annoProtocolloA the annoProtocolloA to set
	 */
	public final void setAnnoProtocolloA(final Integer annoProtocolloA) {
		this.annoProtocolloA = annoProtocolloA;
	}

	/** 
	 *
	 * @return the descrAssegnatario
	 */
	public final String getDescrAssegnatario() {
		return descrAssegnatario;
	}

	/** 
	 *
	 * @param descrAssegnatario the descrAssegnatario to set
	 */
	public final void setDescrAssegnatario(final String descrAssegnatario) {
		this.descrAssegnatario = descrAssegnatario;
	}
	
	/** 
	 *
	 * @return the descrAssegnatarioOperazione
	 */
	public final String getDescrAssegnatarioOperazione() {
		return descrAssegnatarioOperazione;
	}

	/** 
	 *
	 * @param descrAssegnatarioOperazione the descrAssegnatarioOperazione to set
	 */
	public final void setDescrAssegnatarioOperazione(final String descrAssegnatarioOperazione) {
		this.descrAssegnatarioOperazione = descrAssegnatarioOperazione;
	}

	/** 
	 *
	 * @return true, if is anno documento disabled
	 */
	public boolean isAnnoDocumentoDisabled() {
		return annoDocumentoDisabled;
	}

	/** 
	 *
	 * @param annoDocumentoDisabled the new anno documento disabled
	 */
	public void setAnnoDocumentoDisabled(final boolean annoDocumentoDisabled) {
		this.annoDocumentoDisabled = annoDocumentoDisabled;
	}

	/** 
	 *
	 * @return true, if is tutto
	 */
	public boolean isTutto() {
		return tutto;
	}

	/** 
	 *
	 * @param tutto the new tutto
	 */
	public void setTutto(final boolean tutto) {
		this.tutto = tutto;
	}

	/** 
	 *
	 * @return the numero protocollo emergenza da
	 */
	public Integer getNumeroProtocolloEmergenzaDa() {
		return numeroProtocolloEmergenzaDa;
	}

	/** 
	 *
	 * @param numeroProtocolloEmergenzaDa the new numero protocollo emergenza da
	 */
	public void setNumeroProtocolloEmergenzaDa(final Integer numeroProtocolloEmergenzaDa) {
		this.numeroProtocolloEmergenzaDa = numeroProtocolloEmergenzaDa;
	}

	/** 
	 *
	 * @return the numero protocollo emergenza A
	 */
	public Integer getNumeroProtocolloEmergenzaA() {
		return numeroProtocolloEmergenzaA;
	}

	/** 
	 *
	 * @param numeroProtocolloEmergenzaA the new numero protocollo emergenza A
	 */
	public void setNumeroProtocolloEmergenzaA(final Integer numeroProtocolloEmergenzaA) {
		this.numeroProtocolloEmergenzaA = numeroProtocolloEmergenzaA;
	}

	/** 
	 *
	 * @return the data protocollo emergenza da
	 */
	public Date getDataProtocolloEmergenzaDa() {
		return dataProtocolloEmergenzaDa;
	}

	/** 
	 *
	 * @param dataProtocolloEmergenzaDa the new data protocollo emergenza da
	 */
	public void setDataProtocolloEmergenzaDa(final Date dataProtocolloEmergenzaDa) {
		this.dataProtocolloEmergenzaDa = dataProtocolloEmergenzaDa;
	}

	/** 
	 *
	 * @return the data protocollo emergenza A
	 */
	public Date getDataProtocolloEmergenzaA() {
		return dataProtocolloEmergenzaA;
	}

	/** 
	 *
	 * @param dataProtocolloEmergenzaA the new data protocollo emergenza A
	 */
	public void setDataProtocolloEmergenzaA(final Date dataProtocolloEmergenzaA) {
		this.dataProtocolloEmergenzaA = dataProtocolloEmergenzaA;
	}
	
	/** 
	 *
	 * @return the descrizione registro repertorio
	 */ 
	public String getDescrizioneRegistroRepertorio() {
		return descrizioneRegistroRepertorio;
	}
	
	/** 
	 *
	 * @param descrizioneRegistroRepertorio the new descrizione registro repertorio
	 */
	public void setDescrizioneRegistroRepertorio(final String descrizioneRegistroRepertorio) {
		this.descrizioneRegistroRepertorio = descrizioneRegistroRepertorio;
	} 
}