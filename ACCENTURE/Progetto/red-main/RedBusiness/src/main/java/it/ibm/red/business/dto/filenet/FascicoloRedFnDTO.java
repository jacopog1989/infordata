package it.ibm.red.business.dto.filenet;

import java.util.Map;

import it.ibm.red.business.dto.AbstractDTO;
import it.ibm.red.business.dto.ListSecurityDTO;

/**
 * DTO che definisce un fascicolo Red FN.
 */
public class FascicoloRedFnDTO extends AbstractDTO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 8252874066347577367L;

	/**
	 * Identificativo fascicolo.
	 */
	private String idFascicolo;
	
	/**
	 * Classe documentale.
	 */
	private String classeDocumentale;

	/**
	 * Metadati.
	 */
	private transient Map<String, Object> metadati;

	/**
	 * Security.
	 */
	private ListSecurityDTO security;
	
	/**
	 * Restituisce l'id del fascicolo.
	 * @return id fascicolo
	 */
	public String getIdFascicolo() {
		return idFascicolo;
	}

	/**
	 * Imposta l'id del fascicolo.
	 * @param idFascicolo
	 */
	public void setIdFascicolo(final String idFascicolo) {
		this.idFascicolo = idFascicolo;
	}

	/**
	 * Restituisce la classe documentale.
	 * @return classe documentale
	 */
	public String getClasseDocumentale() {
		return classeDocumentale;
	}

	/**
	 * Imposta la classe documentale.
	 * @param classeDocumentale
	 */
	public void setClasseDocumentale(final String classeDocumentale) {
		this.classeDocumentale = classeDocumentale;
	}

	/**
	 * Restituisce la Map dei metadati.
	 * @return metadati
	 */
	public Map<String, Object> getMetadati() {
		return metadati;
	}

	/**
	 * Imposta la Map dei metadati.
	 * @param metadati
	 */
	public void setMetadati(final Map<String, Object> metadati) {
		this.metadati = metadati;
	}

	/**
	 * Restituisce le security.
	 * @return security
	 */
	public ListSecurityDTO getSecurity() {
		return security;
	}

	/**
	 * Imposta le security.
	 * @param security
	 */
	public void setSecurity(final ListSecurityDTO security) {
		this.security = security;
	}
}