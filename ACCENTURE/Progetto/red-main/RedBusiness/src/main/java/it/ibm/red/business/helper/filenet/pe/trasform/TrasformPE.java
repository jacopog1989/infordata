package it.ibm.red.business.helper.filenet.pe.trasform;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import filenet.vw.api.VWException;
import filenet.vw.api.VWQueueQuery;
import filenet.vw.api.VWRosterQuery;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.enums.ContextTrasformerPEEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.exception.FilenetException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.ContextTrasformerPE;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;

/**
 * The Class TrasformPE.
 *
 * @author CPIERASC
 * 
 *         Classe per la trasformazione degli oggetti PE.
 */
@SuppressWarnings({"rawtypes", "unchecked"})
public abstract class TrasformPE {
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TrasformPE.class.getName());
	
	/**
	 * Costruttore.
	 */
	private TrasformPE() {
	}
	
	/**
	 * Metodo per la trasformazione dei risultato provenienti da una query sul PE.
	 * 
	 * @param <T>	tipologia degli oggetti trasformati
	 * @param query	query eseguita sul PE
	 * @param te	tipologia di trasformazione da adottare
	 * @return		collezione degli oggetti trasformati
	 */
	public static <T> Collection<T> transform(final VWQueueQuery query, final TrasformerPEEnum te, final Integer cap) {
		TrasformerPE trasformer = TrasformerPEFactory.getInstance().getTrasformer(te);
		return trasformer.trasform(query, cap);
	}
	
	/**
	 * Metodo per trasformazione di un WorkObject in un una entità.
	 * 
	 * @param <T>	tipologia degli oggetti trasformati
	 * @param wob	workobject da trasformare
	 * @param te	tipologia di trasformazione da eseguire
	 * @return		oggetto trasformato
	 */
	public static <T> T transform(final VWWorkObject wob, final TrasformerPEEnum te) {
		TrasformerPE trasformer = TrasformerPEFactory.getInstance().getTrasformer(te);
		try {
			return (T) trasformer.trasform(wob);
		} catch (Exception e) {
			LOGGER.error("Errore durante il recupero delle informazioni dal workflow. ", e);
			throw new FilenetException(e);
		}
	}
	
	
	/**
	 * Metodo per trasformazione di un WorkObject in un una entità.
	 * 
	 * @param <T>	tipologia degli oggetti trasformati
	 * @param wob	workobject da trasformare
	 * @param te	tipologia di trasformazione da eseguire
	 * @return		oggetto trasformato
	 */
	public static <T> T transform(final VWWorkObject wob, final TrasformerPEEnum te, final Map<ContextTrasformerPEEnum, Object> context) {
		ContextTrasformerPE trasformer = (ContextTrasformerPE) TrasformerPEFactory.getInstance().getTrasformer(te);
		
		try {
			return (T) trasformer.trasform(wob, context);
		} catch (Exception e) {
			LOGGER.error("Errore durante il recupero delle informazioni dal workflow. ", e);
			throw new FilenetException(e);
		}
	}
	
	/**
	 * Esegue la logica di trasformazione.
	 * @param wobs
	 * @param te
	 * @return output
	 */
	public static <T> Collection<T> transform(final Collection<VWWorkObject> wobs, final TrasformerPEEnum te) {
		Collection output = new ArrayList<>();
		for (VWWorkObject wob : wobs) {
			
			output.add(transform(wob, te));
		}
		
		return output;
	}
	
	/**
	 * Esegue la logica di trasformazione.
	 * @param wobs
	 * @param te
	 * @param context
	 * @return output
	 */
	public static <T> Collection<T> transform(final Collection<VWWorkObject> wobs, final TrasformerPEEnum te, final Map<ContextTrasformerPEEnum, Object> context) {
		Collection output = new ArrayList<>();
		
		for (VWWorkObject wob : wobs) {
			output.add(transform(wob, te, context));
		}
		
		return output;
	}
	
	/**
	 * Metodo per la trasformazione dei risultato provenienti da una query sul roster del PE.
	 * 
	 * @param <T>	tipologia degli oggetti trasformati
	 * @param query	query eseguita sul roster del PE
	 * @param te	tipologia di trasformazione da adottare
	 * @return		collezione degli oggetti trasformati
	 */
	public static <T> Collection<T> transform(final VWRosterQuery query, final TrasformerPEEnum te) {
		Collection<T> collection = null;
		if (query != null) {
			collection = new ArrayList<>();
			try {
				while (query.hasNext()) {
					VWWorkObject obj = (VWWorkObject) query.next();
					collection.add((T) transform(obj, te));
				}
			} catch (VWException e) {
				LOGGER.error("Errore durante il recupero delle informazioni dal worfkflow: " + e.getMessage(), e);
				throw new RedException("Errore durante il recupero delle informazioni dal worfkflow. ", e);
			}
		}
		return collection;
	}
	
}