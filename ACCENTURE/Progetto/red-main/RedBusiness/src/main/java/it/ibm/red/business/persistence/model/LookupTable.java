package it.ibm.red.business.persistence.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author APerquoti
 */
public class LookupTable implements Serializable {

	private static final long serialVersionUID = 4564654203803404977L;

	/**
	 * Identificativo.
	 */
	private final long idLookup;
	
	/**
	 * Selettore.
	 */
	private final String selettore;

	/**
	 * Display name.
	 */
	private final String displayName;

	/**
	 * Valore.
	 */
	private final String value;

	/**
	 * Data attivsazione.
	 */
	private final Date dataAttivazione;

	/**
	 * Data disattivazione.
	 */
	private final Date dataDisattivazione;

	/**
	 * Id aoo.
	 */
	private final long idAoo;

    /**
     * Costruttore di default.
     * @param inIdLookup
     * @param inSelettore
     * @param inDisplayName
     * @param inValue
     * @param inDataAttivazione
     * @param inDataDisattivazione
     * @param inIdAoo
     */
	public LookupTable(final long inIdLookup, final String inSelettore, final String inDisplayName, final String inValue, 
			final Date inDataAttivazione, final Date inDataDisattivazione, final long inIdAoo) {
		this.idLookup = inIdLookup;
		this.selettore = inSelettore;
		this.displayName = inDisplayName;
		this.value = inValue;
		this.dataAttivazione = inDataAttivazione;
		this.dataDisattivazione = inDataDisattivazione;
		this.idAoo = inIdAoo;
	}

	/**
	 * @return idLookup
	 */
	public long getIdLookup() {
		return idLookup;
	}

	/**
	 * @return selettore
	 */
	public String getSelettore() {
		return selettore;
	}

	/**
	 * @return displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @return value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @return dataAttivazione
	 */
	public Date getDataAttivazione() {
		return dataAttivazione;
	}

	/**
	 * @return dataDisattivazione
	 */
	public Date getDataDisattivazione() {
		return dataDisattivazione;
	}

	/**
	 * @return idAoo
	 */
	public long getIdAoo() {
		return idAoo;
	}
}
