package it.ibm.red.business.service.facade;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.List;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AllegatoOPDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.VersionDTO;
import it.ibm.red.business.dto.filenet.DocumentoRedFnDTO;

/**
 * Interfaccia del service che gestisce gli allegati.
 */
public interface IAllegatoFacadeSRV extends Serializable {

	/**
	 * Converte in allegati filenet.
	 * 
	 * @param allegatiInput
	 *            - allegati in input
	 * @param idAoo
	 *            - id dell'Aoo
	 * @return lista di documenti allegati filenet
	 * @throws IOException
	 */
	List<DocumentoRedFnDTO> convertToAllegatiFilenet(List<AllegatoDTO> allegatiInput, Long idAoo) throws IOException;

	/**
	 * Inserisce un allegato alla richiesta OPF IGEPA.
	 * 
	 * @param richiestaIgepaFilenet
	 *            document che definisce la richiesta Igepa
	 * @param allegatoIgepa
	 *            allegato al documento Igepa
	 * @param utente
	 *            utente in sessione
	 */
	void allegaARichiestaOPFIgepa(Document richiestaIgepaFilenet, AllegatoDTO allegatoIgepa, UtenteDTO utente);

	/**
	 * Inserisce un allegato all'ordine di pagamento.
	 * 
	 * @param decreto
	 *            decreto a cui allegare un documento
	 * @param allegato
	 *            documento da allegare
	 * @param utente
	 *            utente in sessione
	 */
	void allegaAdOrdineDiPagamento(DetailDocumentRedDTO decreto, AllegatoOPDTO allegato, UtenteDTO utente);

	/**
	 * Inserisce un allegato alla fattura.
	 * 
	 * @param idDocumento
	 *            identificativo documento a cui allegare un altro documento
	 * @param allegato
	 *            documento da allegare alla fattura
	 * @param utente
	 *            utente in sessione
	 */
	void allegaAFattura(String idDocumento, AllegatoDTO allegato, UtenteDTO utente);

	/**
	 * Elimina un allegato FEPA.
	 * 
	 * @param idDocumento
	 *            identificativo documento dal quale rimuovere gli allegati
	 * @param utente
	 *            utente in sessione
	 */
	void eliminaAllegatiFepa(String idDocumento, UtenteDTO utente);

	/**
	 * Recupera lo stream dati dell'allegato inviato. Viene considerata la versione
	 * più recente disponibile.
	 * 
	 * @param allegato
	 *            allegato di cui si richiede lo stream dati
	 * @param utente
	 *            richeidente dello stream dati
	 * @return lo stream dati dell'allegato considerato
	 */
	InputStream getStreamByGuid(AllegatoDTO allegato, UtenteDTO utente);

	/**
	 * Dato una versone crea un VersionDTO che ne è l'esatta copia
	 * 
	 * @param currentVersion
	 *            una versione che si vuole copiare
	 * @return copia di currentVersion
	 */
	VersionDTO copyVersion(VersionDTO currentVersion);

	/**
	 * Permette di trasformare un documento allegabile in un allegato. Alcuni dei
	 * paarametri hanno dei valori di default, decisi dalla funzionalità-
	 * 
	 * @param allegabileList
	 *            lista di allegabili da convertire.
	 * @param tipoDocAttivi
	 *            lista delle tipologie di documenti attive.
	 * @return una lista di allegatiDTO convertiti, da salvare.
	 */
	List<AllegatoDTO> transformFrom(List<DocumentoAllegabileDTO> allegabileList,
			List<TipologiaDocumentoDTO> tipoDocAttivi);
}