/**
 * 
 */
package it.ibm.red.business.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.ICodeApplicativeDAO;
import it.ibm.red.business.dto.DatiCodaApplicativaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.StatoLavorazioneEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * @author APerquoti
 * 
 *         Dao gestione Code Applicative
 *
 */
@Repository
public class CodeApplicativeDAO extends AbstractDAO implements ICodeApplicativeDAO {

	private static final String AND_DUS_IDUTENTE = " AND dus.idutente = ? ";

	private static final String AND_DUS_IDNODO = " AND dus.idnodo = ? ";

	private static final String WHERE_DUS_IDSTATOLAVORAZIONE_SLA_IDSTATOLAVORAZIONE = " WHERE dus.idstatolavorazione = sla.idstatolavorazione ";

	/**
	 * Serializable.
	 */
	private static final long serialVersionUID = -4431901659295204271L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(CodeApplicativeDAO.class.getName());

	@Override
	public final Set<String> getCodeAppl(final Long idNodoUtente, final Long idUtente, final Collection<Long> idsStatoLavorazione, final Integer numElements,
			final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final Long[] idsl = idsStatoLavorazione.toArray(new Long[0]);
		final Set<String> idsDocument = new HashSet<>();
		final StringBuilder sb = new StringBuilder();
		final StringBuilder sbQuery = new StringBuilder();
		if (idsl != null && (idsl.length > 0)) {
			if (idsl.length > 1) {
				sb.append("dus.idstatolavorazione IN ( ");
				for (int i = 0; i < idsl.length; i++) {
					sb.append(idsl[i]);
					if (i < (idsl.length - 1)) {
						sb.append(" , ");
					}
				}
				sb.append(" ) ");
			} else {
				sb.append("dus.idstatolavorazione = " + idsl[0]);
			}
		}

		try {

			final String nestQuery = "SELECT dus.iddocumento as IDDOCUMENTO, dus.DATA_STATO as DATA_STATO "
					+ "FROM documentoutentestato dus, statolavorazione sla, documento doc, tipologiadocumento tdo, classedocumentale cld "
					+ WHERE_DUS_IDSTATOLAVORAZIONE_SLA_IDSTATOLAVORAZIONE + "AND doc.iddocumento = dus.iddocumento "
					+ "AND cld.idclassedocumentale = tdo.idclassedocumentale " + "AND doc.idtipologiadocumento = tdo.idtipologiadocumento " + AND_DUS_IDNODO
					+ AND_DUS_IDUTENTE + "AND " + sb.toString() + "AND doc.registroriservato = 0 " + "UNION "
					+ "SELECT ras.idrichiesta as IDDOCUMENTO, dus.DATA_STATO as DATA_STATO "
					+ "FROM documentoutentestato dus, statolavorazione sla, richiesta ras, tipologiarichiesta tri " + WHERE_DUS_IDSTATOLAVORAZIONE_SLA_IDSTATOLAVORAZIONE
					+ "AND ras.idrichiesta = dus.iddocumento " + "AND ras.idtipologiarichiesta = tri.idtipologiarichiesta " + AND_DUS_IDNODO + AND_DUS_IDUTENTE
					+ "AND " + sb.toString();

			// seconda query nidificata per appliccare l'ordinamento prima del 'rownum'
			final String secondaryQuery = "SELECT * " + "FROM (" + nestQuery + ")  ORDER BY DATA_STATO DESC";

			String primaryQuery = "SELECT * " + "FROM (" + secondaryQuery + ") ";

			// aggiungo il limite dei risultati se richiesto
			if (numElements != null && numElements > 0) {
				sbQuery.append(primaryQuery);
				sbQuery.append("WHERE rownum <= ?");
				primaryQuery = sbQuery.toString();
			}

			ps = connection.prepareStatement(primaryQuery);
			int n = 1;
			// prima Select
			ps.setLong(n++, idNodoUtente);
			ps.setLong(n++, idUtente);
			ps.setLong(n++, idNodoUtente);
			ps.setLong(n++, idUtente);

			if (numElements != null && numElements > 0) {
				ps.setInt(n++, numElements);
			}

			rs = ps.executeQuery();

			while (rs.next()) {
				idsDocument.add(rs.getString("IDDOCUMENTO"));
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero di una Coda Applicativa", e);
			throw new RedException("Errore durante il recupero di una Coda Applicativa", e);
		} finally {
			closeStatement(ps, rs);
		}

		return idsDocument;
	}

	/**
	 * @see it.ibm.red.business.dao.ICodeApplicativeDAO#getQueueName(java.lang.String,
	 *      java.lang.Long, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public Collection<DatiCodaApplicativaDTO> getQueueName(final String documentTitle, final Long idUtente, final Long idNodo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final Collection<DatiCodaApplicativaDTO> output = new ArrayList<>();

		try {
			final String querySQL = "SELECT dus.idstatolavorazione, dus.idutente " + "FROM documentoutentestato dus, documento doc "
					+ "WHERE doc.iddocumento = dus.iddocumento " + "AND dus.iddocumento = ? AND dus.idnodo = ? AND dus.idutente IN ( 0, ?) "
					+ "AND doc.registroriservato = 0 ";

			ps = connection.prepareStatement(querySQL);
			int n = 1;
			ps.setString(n++, documentTitle);
			ps.setLong(n++, idNodo);
			ps.setLong(n++, idUtente);

			rs = ps.executeQuery();
			while (rs.next()) {
				output.add(new DatiCodaApplicativaDTO(rs.getLong("IDSTATOLAVORAZIONE"), rs.getLong("IDUTENTE")));
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del Nome di una Coda Applicativa", e);
			throw new RedException("Errore durante il recupero del Nome di una Coda Applicativa");
		} finally {
			closeStatement(ps, rs);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.ICodeApplicativeDAO#archiviaCodeApplicative(java.sql.Connection).
	 */
	@Override
	public void archiviaCodeApplicative(final Connection connection) {
		CallableStatement cs = null;
		try {
			cs = connection.prepareCall("{ call P_JOB_GESTIONECODE() }");
			cs.execute();
		} catch (final SQLException e) {
			throw new RedException("Errore durante l'archiviazione delle code applicative", e);
		} finally {
			closeStatement(cs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ICodeApplicativeDAO#countCodeAppl(java.lang.Long,
	 *      java.lang.Long, java.util.Collection, java.sql.Connection).
	 */
	@Override
	public Integer countCodeAppl(final Long idNodoUtente, final Long idUtente, final Collection<Long> idsStatoLavorazione, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final Long[] idsl = idsStatoLavorazione.toArray(new Long[0]);
		Integer output = 0;
		final StringBuilder sb = new StringBuilder();
		if (idsl != null && (idsl.length > 0)) {
			if (idsl.length > 1) {
				sb.append("dus.idstatolavorazione IN ( ");
				for(int i = 0; i < idsl.length; i++) {
					sb.append(idsl[i]);
					if (i < (idsl.length-1)) {
						sb.append(" , ");
					}
				}
				sb.append(" ) ");
			} else {
				sb.append("dus.idstatolavorazione = " + idsl[0]);
			}
		}

		try {

			final String query = "SELECT COUNT(iddocumento) AS NUMERO_DOCUMENTI " + "FROM( " + "SELECT doc.iddocumento "
					+ "FROM documentoutentestato dus, statolavorazione sla, documento doc, tipologiadocumento tdo, classedocumentale cld "
					+ WHERE_DUS_IDSTATOLAVORAZIONE_SLA_IDSTATOLAVORAZIONE
					+ "AND doc.iddocumento = dus.iddocumento "
					+ "AND cld.idclassedocumentale = tdo.idclassedocumentale "
					+ "AND doc.idtipologiadocumento = tdo.idtipologiadocumento " + AND_DUS_IDNODO
					+ AND_DUS_IDUTENTE + (StringUtils.isEmpty(sb.toString()) ? " " : ("AND " + sb.toString())) + "AND doc.registroriservato = 0" 
					+ " UNION "
					+ "SELECT ras.idrichiesta as iddocumento "
					+ "FROM documentoutentestato dus, statolavorazione sla, richiesta ras, tipologiarichiesta tri "
					+ WHERE_DUS_IDSTATOLAVORAZIONE_SLA_IDSTATOLAVORAZIONE
					+ "AND ras.idrichiesta = dus.iddocumento "
					+ "AND ras.idtipologiarichiesta = tri.idtipologiarichiesta " + AND_DUS_IDNODO
					+ AND_DUS_IDUTENTE + (StringUtils.isEmpty(sb.toString()) ? " " : "AND " + sb.toString()) + " )";

			ps = connection.prepareStatement(query);
			int n = 1;
			// prima Select
			ps.setLong(n++, idNodoUtente);
			ps.setLong(n++, idUtente);
			ps.setLong(n++, idNodoUtente);
			ps.setLong(n++, idUtente);

			rs = ps.executeQuery();

			while (rs.next()) {
				output = rs.getInt(1);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la count di una Coda Applicativa", e);
			throw new RedException("Errore durante la count di una Coda Applicativa", e);
		} finally {
			closeStatement(ps, rs);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.ICodeApplicativeDAO#checkDocumentTitleRecall(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.Long, boolean,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean checkDocumentTitleRecall(final String documentTitle, final UtenteDTO utente, final Long idStatoLavorazione, final boolean isAzioniMassive,
			final Connection connection) {
		boolean isPresente = false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = null;

		try {
			query = "SELECT dus.iddocumento," + "dus.idnodo," + "dus.idstatolavorazione " + "FROM documentoutentestato dus "
					+ "WHERE dus.iddocumento = ? AND dus.idnodo = ? AND dus.idutente = ? AND dus.idstatolavorazione = ? ";

			ps = connection.prepareStatement(query);
			ps.setString(1, documentTitle);
			ps.setLong(2, utente.getIdUfficio());
			ps.setLong(3, utente.getId());
			ps.setLong(4, idStatoLavorazione);
			rs = ps.executeQuery();

			while (rs.next()) {
				isPresente = true;
				return isPresente;
			}
		} catch (final Exception ex) {
			LOGGER.error("Errore durante il check recall della coda in Approvazione", ex);
			throw new RedException("Errore durante il check recall della coda in Approvazione", ex);

		} finally {
			closeStatement(ps, rs);
		}
		return isPresente;
	}

	/**
	 * @see it.ibm.red.business.dao.ICodeApplicativeDAO#fromRecallToLavorate(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public void fromRecallToLavorate(final String idDocumento, final Connection connection) {
		PreparedStatement ps = null;
		int index = 1;
		
		try {
			ps = connection.prepareStatement("UPDATE documentoutentestato SET idstatolavorazione = ? WHERE iddocumento = ? AND idstatolavorazione = ?");
			
			ps.setLong(index++, StatoLavorazioneEnum.LAVORATE.getId());
			ps.setLong(index++, Long.parseLong(idDocumento));
			ps.setLong(index++, StatoLavorazioneEnum.RICHIAMA_DOCUMENTI_STATO.getId());

			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'aggiornamento del record dalla tabella documentoutentestato con ID: " + idDocumento, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ICodeApplicativeDAO#deleteRecall(java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public void deleteRecall(final Integer idDocumento, final Connection connection) {
		PreparedStatement ps = null;
		int index = 1;
		try {
			ps = connection.prepareStatement("DELETE FROM documentoutentestato WHERE iddocumento = ? AND idstatolavorazione = ?");
			
			ps.setLong(index++, idDocumento);
			ps.setLong(index++, StatoLavorazioneEnum.RICHIAMA_DOCUMENTI_STATO.getId());
			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'eliminazione del record recall dalla tabella documentoutentestato con ID: " + idDocumento, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ICodeApplicativeDAO#deleteAll(java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public void deleteAll(final Integer idDocumento, final Connection connection) {
		PreparedStatement ps = null;

		try {
			ps = connection.prepareStatement("DELETE FROM documentoutentestato WHERE iddocumento = ? ");
			ps.setLong(1, idDocumento);

			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'eliminazione dei record dalla tabella documentoutentestato con ID: " + idDocumento, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

}
