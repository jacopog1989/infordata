package it.ibm.red.business.service.facade;

import java.io.Serializable;

import it.ibm.red.business.dto.AssegnatarioDTO;
import it.ibm.red.business.dto.EsitoDTO;
import it.ibm.red.business.dto.MappingOrgNsdDTO;
import it.ibm.red.business.persistence.model.Aoo;

/**
 * Facade del servizio che gestisce l'AOO.
 */
public interface IAooFacadeSRV extends Serializable {

	/**
	 * Il metodo recupera l'Aoo in input.
	 * 
	 * @param idAoo
	 *            identificativo dell'area organizzativa da recuperare
	 * @return informazioni sull'area organizzativa recuperata
	 */
	Aoo recuperaAoo(int idAoo);

	/**
	 * Metodo per verificare che il Ruolo di Delegato a Lìbro firma sia configurato
	 * correttamente.
	 * 
	 * @param idAoo
	 *            identificativo area organizzativa
	 * @return esito ruolo delegato al libro firma
	 */
	EsitoDTO checkRuoloDelegatoLiboFirma(Long idAoo);

	/**
	 * Il metodo recupera l'Aoo in input.
	 * 
	 * @param codiceAoo
	 *            codice che identifica l'area organizzativa
	 * @return informazione sull'area organizzativa recuperata
	 */
	Aoo recuperaAoo(String codiceAoo);

	/**
	 * Restituisce true se la dialog di inserimento della motivazione per
	 * l'operazone di Sigla NON va mostrata, false altrimenti.
	 * 
	 * @param idAoo
	 *            identificativo dell'area organizzativa
	 * @return true se la dialog di inserimento della motivazione per l'operazone di
	 *         Sigla NON va mostrata, false altrimenti
	 */
	boolean checkSkipMotivazioneSigla(Long idAoo);

	/**
	 * @param idUfficio
	 *            identificativo ufficio
	 * @param idUtente
	 *            identificativo utente
	 * @param idAoo
	 *            identificativo area organizzativa
	 * @return mapping organigramma nsd
	 */
	MappingOrgNsdDTO getMappingOrgNsd(Long idUfficio, Long idUtente, Long idAoo);

	/**
	 * Metodo che consente di recuperare l'assegnatario di default di una specifica
	 * AOO.
	 * 
	 * @param idAoo
	 *            identificativo dell'area organizzativa
	 * @return assegnatario di default
	 */
	AssegnatarioDTO recuperaAssegnatarioDiDefault(Long idAoo);
	
	/**
	 * Il metodo recupera l'Aoo a partire dal codice aoo di NPS.
	 * 
	 * @param codiceAooNPS
	 * @return
	 */
	Aoo recuperaAooFromNPS(String codiceAooNPS);
}