package it.ibm.red.business.service.concrete;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.core.Document;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.IDocumentoDAO;
import it.ibm.red.business.dao.IFascicoloDAO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AllegatoOPDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.DocumentoAllegabileDTO.DocumentoAllegabileType;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedParametriDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.VersionDTO;
import it.ibm.red.business.dto.filenet.DocumentoRedFnDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ProvenienzaSalvaDocumentoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAllegatoSRV;
import it.ibm.red.business.service.IDocumentManagerSRV;
import it.ibm.red.business.service.ISalvaDocumentoSRV;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * Service per la gestione degli allegati.
 */
@Service
@Component
public class AllegatoSRV extends AbstractService implements IAllegatoSRV {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = 5072215933806523575L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AllegatoSRV.class.getName());

	/**
	 * Provider property.
	 */
	private PropertiesProvider pp;

	/**
	 * DAO documenti.
	 */
	@Autowired
	private IDocumentoDAO documentoDAO;

	/**
	 * Servizio salva documento.
	 */
	@Autowired
	private ISalvaDocumentoSRV salvaDocSRV;

	/**
	 * Dao fascicolo.
	 */
	@Autowired
	private IFascicoloDAO fascicoloDAO;

	/**
	 * Servizio gestione documento.
	 */
	@Autowired
	private IDocumentManagerSRV documentManagerSRV;

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * Convert gli allegati in input <code> allegatiInput </code> in allegati
	 * FileNet.
	 * 
	 * @see it.ibm.red.business.service.facade.IAllegatoFacadeSRV#convertToAllegatiFilenet(java.util.List,
	 *      java.lang.Long)
	 */
	@Override
	public List<DocumentoRedFnDTO> convertToAllegatiFilenet(final List<AllegatoDTO> allegatiInput, final Long idAoo) throws IOException {
		Connection con = null;
		List<DocumentoRedFnDTO> allegatiDocFilenet = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			allegatiDocFilenet = convertToAllegatiFilenet(allegatiInput, idAoo, con);
		} catch (final SQLException e) {
			LOGGER.error("convertToAllegatiDocFilenet -> Impossibile creare una nuova connessione al DB: " + e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

		return allegatiDocFilenet;
	}

	/**
	 * Convert gli allegati in input <code> allegatiInput </code> in allegati
	 * FileNet.
	 * 
	 * @see it.ibm.red.business.service.IAllegatoSRV#convertToAllegatiFilenet(java.util.List,
	 *      java.lang.Long, java.sql.Connection)
	 */
	@Override
	public List<DocumentoRedFnDTO> convertToAllegatiFilenet(final List<AllegatoDTO> allegatiInput, final Long idAoo, final Connection con) throws IOException {
		LOGGER.info("convertToAllegatiDocFilenet -> START");
		List<DocumentoRedFnDTO> allegatiDocFilenet = null;

		if (!CollectionUtils.isEmpty(allegatiInput)) {
			allegatiDocFilenet = new ArrayList<>(allegatiInput.size());
			for (final AllegatoDTO allegatoInput : allegatiInput) {
				String idUfficioCopiaConforme = "0";
				String idUtenteCopiaConforme = "0";

				final String guid = allegatoInput.getGuid();
				String documentTitle = allegatoInput.getDocumentTitle();
				final Integer idFormatoAllegato = allegatoInput.getFormato();
				final String barcodeAllegato = allegatoInput.getBarcode();
				final String oggettoAllegato = allegatoInput.getOggetto();
				final String nomeFileAllegato = allegatoInput.getNomeFile();
				final byte[] content = allegatoInput.getContent();
				final String contentType = allegatoInput.getMimeType();
				final Integer idTipologiaDocumento = allegatoInput.getIdTipologiaDocumento();
				final Integer daFirmare = allegatoInput.getDaFirmare();
				final String posizione = allegatoInput.getPosizione();
				final boolean isCopiaConforme = Boolean.TRUE.equals(allegatoInput.getCopiaConforme());
				if (isCopiaConforme) {
					idUfficioCopiaConforme = String.valueOf(allegatoInput.getIdUfficioCopiaConforme());
					idUtenteCopiaConforme = String.valueOf(allegatoInput.getIdUtenteCopiaConforme());
				}
				final boolean isFormatoOriginale = Boolean.TRUE.equals(allegatoInput.getFormatoOriginale());
				final boolean isTimbroUscita = Boolean.TRUE.equals(allegatoInput.isTimbroUscitaAoo());
				final boolean firmaVisibile = Boolean.TRUE.equals(allegatoInput.getFirmaVisibile());

				final Map<String, Object> metadatiAllegato = new HashMap<>();

				// Se il GUID è uguale a null, si tratta di un nuovo allegato
				if (guid == null) {
					documentTitle = Constants.EMPTY_STRING + documentoDAO.getNextId(idAoo, con);
				}

				// Info Allacci
				final String idFascicoloProvenienza = allegatoInput.getIdFascicoloProvenienza();
				final String descrizioneFascicoloProvenienza = allegatoInput.getDescrizioneFascicoloProvenienza();
				final String documentTitleDocumentoProvenienza = allegatoInput.getDocumentTitleDocumentoProvenienza();
				final String descrizioneDocumentoProvenienza = allegatoInput.getDescrizioneDocumentoProvenienza();
				final String documentTitleAllegatoProvenienza = allegatoInput.getDocumentTitleAllegatoProvenienza();
				final String nomeFileAllegatoProvenienza = allegatoInput.getNomeFileAllegatoProvenienza();
				final boolean stampigliaturaSigla = allegatoInput.getStampigliaturaSigla();
				metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.STAMPIGLIATURA_SIGLA_ALLEGATO), stampigliaturaSigla);

				final boolean fileNonSbustato = allegatoInput.getFileNonSbustato();
				metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.ALLEGATO_NON_SBUSTATO), fileNonSbustato);

				metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.FORMATO_ALLEGATO_ID_METAKEY), idFormatoAllegato);
				metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), nomeFileAllegato);
				metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY), oggettoAllegato);
				metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.BARCODE_METAKEY), barcodeAllegato);
				metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY), idTipologiaDocumento);
				metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.DA_FIRMARE_METAKEY), daFirmare);
				metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), documentTitle);
				metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.POSIZIONE_METAKEY), posizione);
				metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.ID_UFFICIO_COPIA_CONFORME_METAKEY), idUfficioCopiaConforme);
				metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_COPIA_CONFORME_METAKEY), idUtenteCopiaConforme);
				metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.IS_COPIA_CONFORME_METAKEY), isCopiaConforme);
				metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.IS_FORMATO_ORIGINALE_METAKEY), isFormatoOriginale);
				metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.IS_TIMBRO_USCITA_METAKEY), isTimbroUscita);
				metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.FIRMA_VISIBILE_METAKEY), firmaVisibile);

				// Info Allacci
				if (!StringUtils.isNullOrEmpty(idFascicoloProvenienza)) {
					metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.ALLEGATO_IDFASCICOLOPROVENIENZA_METAKEY), idFascicoloProvenienza);
					metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.ALLEGATO_DESCRIZIONEFASCICOLOPROVENIENZA_METAKEY), descrizioneFascicoloProvenienza);
					metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.ALLEGATO_DOCUMENTTITLEDOCUMENTOPROVENIENZA_METAKEY), documentTitleDocumentoProvenienza);
					metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.ALLEGATO_DESCRIZIONEDOCUMENTOPROVENIENZA_METAKEY), descrizioneDocumentoProvenienza);
					if (!StringUtils.isNullOrEmpty(documentTitleAllegatoProvenienza)) {
						metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.ALLEGATO_DOCUMENTTITLEALLEGATOPROVENIENZA_METAKEY), documentTitleAllegatoProvenienza);
						metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.ALLEGATO_NOMEFILEALLEGATOPROVENIENZA_METAKEY), nomeFileAllegatoProvenienza);
					}
				}

				final DocumentoRedFnDTO allegatoFilenet = new DocumentoRedFnDTO();
				allegatoFilenet.setDocumentTitle(documentTitle);
				allegatoFilenet.setGuid(guid);
				allegatoFilenet.setClasseDocumentale(pp.getParameterByKey(PropertiesNameEnum.ALLEGATO_CLASSNAME_FN_METAKEY));
				allegatoFilenet.setMetadati(metadatiAllegato);
				if (content != null && guid == null) {
					allegatoFilenet.setDataHandler(FileUtils.toDataHandler(content));
					allegatoFilenet.setContentType(contentType);
				}

				// Inserimento nella lista degli allegati
				allegatiDocFilenet.add(allegatoFilenet);
			}
		}

		LOGGER.info("convertToAllegatiFilenet -> END");
		return allegatiDocFilenet;
	}

	/**
	 * Allega l'allegato specificiato dal parametro: <code> allegato </code> alla
	 * richiesta OPF Igepa.
	 * 
	 * @see it.ibm.red.business.service.facade.IAllegatoFacadeSRV#allegaARichiestaOPFIgepa(com.filenet.api.core.Document,
	 *      it.ibm.red.business.dto.AllegatoDTO, it.ibm.red.business.dto.UtenteDTO)
	 */
	@Override
	public void allegaARichiestaOPFIgepa(final Document richiestaOPFIgepaFilenet, final AllegatoDTO allegato, final UtenteDTO utente) {
		LOGGER.info("inserisciAllegatoIgepa -> START");
		final IFilenetCEHelper fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
		final List<AllegatoDTO> allegati = new ArrayList<>();
		allegati.add(allegato);

		try {
			final List<DocumentoRedFnDTO> allegatiIgepaDocFilenet = convertToAllegatiFilenet(allegati, utente.getIdAoo());

			// Si inserisce l'allegato IGEPA su FileNet, come allegato della richiesta OPF
			// IGEPA
			fceh.inserisciAllegati(richiestaOPFIgepaFilenet, allegatiIgepaDocFilenet);
		} catch (final Exception e) {
			LOGGER.error("inserisciAllegatoIgepa -> Si è verificato un errore durante l'inserimento di un allegato con nome file: " + allegato.getNomeFile()
					+ " alla richiesta OPF IGEPA con GUID: " + richiestaOPFIgepaFilenet.get_Id(), e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}

		LOGGER.info("inserisciAllegatoIgepa -> END");
	}

	/**
	 * Allega alla fattura identificata dall' <code> idDocumento </code>, il
	 * documento specificato da: <code> allegato </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAllegatoFacadeSRV#allegaAFattura(java.lang.String,
	 *      it.ibm.red.business.dto.AllegatoDTO, it.ibm.red.business.dto.UtenteDTO)
	 */
	@Override
	public void allegaAFattura(final String idDocumento, final AllegatoDTO allegato, final UtenteDTO utente) {
		IFilenetCEHelper fceh = null;
		final List<AllegatoDTO> allegati = new ArrayList<>();
		allegati.add(allegato);
		Connection con = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			con = setupConnection(getDataSource().getConnection(), false);

			final DetailDocumentRedDTO detailDocInput = new DetailDocumentRedDTO();
			// Come NSD
			detailDocInput.setIdTipologiaDocumento(22); // Allegato
			detailDocInput.setOggetto(allegato.getOggetto());
			detailDocInput.setNomeFile(allegato.getNomeFile());
			detailDocInput.setContent(allegato.getContent());
			detailDocInput.setMimeType(allegato.getMimeType());
			detailDocInput.setIdAOO(Integer.parseInt("" + utente.getIdAoo()));
			detailDocInput.setIdCategoriaDocumento(CategoriaDocumentoEnum.INTERNO.getIds()[0]);
			detailDocInput.setIdUtenteCreatore(utente.getId().intValue());
			detailDocInput.setIdUfficioCreatore(utente.getIdUfficio().intValue());
			detailDocInput.setFormatoDocumentoEnum(FormatoDocumentoEnum.ELETTRONICO);

			final SalvaDocumentoRedParametriDTO parametri = new SalvaDocumentoRedParametriDTO();
			parametri.setIdFascicoloSelezionato(fascicoloDAO.getIdFascicoloProcedimentale(idDocumento, utente.getIdAoo(), con).toString());

			final EsitoSalvaDocumentoDTO esito = salvaDocSRV.salvaDocumento(detailDocInput, parametri, utente, ProvenienzaSalvaDocumentoEnum.GUI_RED_FASCICOLO);

			final Document doc = fceh.getDocumentByIdGestionale(esito.getDocumentTitle(), null, null, utente.getIdAoo().intValue(), "", "");

			final Map<String, Object> metadati = new HashMap<>();

			// Lo risetto a 0, red gestisce 0 come Allegato
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY), 0);

			fceh.setPropertiesAndSaveDocument(doc, metadati, true);
		} catch (final Exception e) {
			LOGGER.error("inserisciAllegatoFattura -> Si è verificato un errore durante l'inserimento di un allegato con nome file: " + allegato.getNomeFile(), e);
			throw new RedException("Errore durante l'inserimento dell'allegato");
		} finally {
			popSubject(fceh);
			closeConnection(con);
		}
	}

	/**
	 * Elimina gli allegati Fepa associati al documento identificato dall'
	 * <code> idDocumento </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAllegatoFacadeSRV#eliminaAllegatiFepa(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO)
	 */
	@Override
	public void eliminaAllegatiFepa(final String idDocumento, final UtenteDTO utente) {
		IFilenetCEHelper fceh = null;
		Connection con = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			con = setupConnection(getDataSource().getConnection(), false);

			final String idFascicolo = fascicoloDAO.getIdFascicoloProcedimentale(idDocumento, utente.getIdAoo(), con).toString();
			fascicoloDAO.deleteDocumentoFascicolo(Integer.parseInt(idDocumento), Integer.parseInt(idFascicolo), utente.getIdAoo(), con);
			/**
			 * Maurizio. cambiata la funzione di cancellazione. eliminaDocumentoBozza,
			 * permette di "nascondere la versione 1 del documento". Commento da vecchio
			 * red, sul EVO elimina corrisponde a eliminaDocumentoBozza
			 */
			fceh.eliminaDocumentoByDocumentTitle(idDocumento, utente.getIdAoo());
		} catch (final Exception e) {
			LOGGER.error("EliminaAllegatoFattura -> Si è verificato un errore durante l'eliminazione di un allegato con documentTitle: " + idDocumento, e);
			throw new RedException("Errore durante l'eliminazione dell'allegato");
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}
	}

	/**
	 * Allega un documento all'ordine di pagamento identificato dal
	 * <code> decreto </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAllegatoFacadeSRV#allegaAdOrdineDiPagamento(it.ibm.red.business.dto.DetailDocumentRedDTO,
	 *      it.ibm.red.business.dto.AllegatoOPDTO,
	 *      it.ibm.red.business.dto.UtenteDTO)
	 */
	@Override
	public void allegaAdOrdineDiPagamento(final DetailDocumentRedDTO decreto, final AllegatoOPDTO allegato, final UtenteDTO utente) {
		LOGGER.info("inserisciAllegatoIgepa -> START");
		final IFilenetCEHelper fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

		try {
			final DocumentoRedFnDTO allegatoOP = convertToAllegatoFilenetPerAssociazioneOP(allegato, utente.getIdAoo());

			final Document documentDecreto = fceh.getDocumentByIdGestionale(decreto.getDocumentTitle(), null, null, Integer.parseInt("" + utente.getIdAoo()), null, null);

			fceh.inserisciAllegatoOP(documentDecreto, allegatoOP);
		} catch (final Exception e) {
			LOGGER.error("allegaAdOrdineDiPagamento -> Si è verificato un errore durante l'inserimento di un allegato con nome file: " + allegato.getNomeDocumento(), e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
		}

		LOGGER.info("allegaAdOrdineDiPagamento -> END");
	}

	/**
	 * Converte l'allegato in input in allegato FileNet.
	 * 
	 * @param allegatoInput
	 *            documento da allegare
	 * @param idAoo
	 *            identificativo dell'AOO
	 * @return documento filenet
	 */
	private DocumentoRedFnDTO convertToAllegatoFilenetPerAssociazioneOP(final AllegatoOPDTO allegatoInput, final Long idAoo) {
		Connection con = null;
		DocumentoRedFnDTO allegatoDocFilenet = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			allegatoDocFilenet = loadDocumentiAllegatoOp(allegatoInput, idAoo, con);
		} catch (final SQLException e) {
			LOGGER.error("convertToAllegatiDocFilenet -> Impossibile creare una nuova connessione al DB: " + e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

		return allegatoDocFilenet;
	}

	/**
	 * Carica i documenti allegati dal fascicolo OP.
	 * 
	 * @param allegatoInput
	 * @param idAoo
	 *            identificativo area organizzativa
	 * @param con
	 *            connessione al database
	 * @return documento filenet
	 */
	private DocumentoRedFnDTO loadDocumentiAllegatoOp(final AllegatoOPDTO allegatoInput, final Long idAoo, final Connection con) {
		final String guid = allegatoInput.getGuid();
		String documentTitle = allegatoInput.getIdDocumentoAllegatoOp();
		final String oggettoAllegato = allegatoInput.getOggetto();
		final String nomeFileAllegato = allegatoInput.getNomeDocumento();
		final byte[] mediaAllegato = allegatoInput.getDataHandler();
		final String contentType = allegatoInput.getContentType();

		final Map<String, Object> metadatiAllegato = new HashMap<>();

		// Se il GUID è uguale a null, si tratta di un nuovo allegato
		if (guid == null) {
			documentTitle = Constants.EMPTY_STRING + documentoDAO.getNextId(idAoo, con);
		}

		metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), nomeFileAllegato);
		metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY), oggettoAllegato);

		metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY), documentTitle);

		metadatiAllegato.put(pp.getParameterByKey(PropertiesNameEnum.FEPA_CODICE_TIPO_DOCUMENTO_METAKEY), allegatoInput.getTipologiaDocumento());

		final DocumentoRedFnDTO allegatoFilenet = new DocumentoRedFnDTO();
		allegatoFilenet.setDocumentTitle(documentTitle);
		allegatoFilenet.setFolder(pp.getParameterByKey(PropertiesNameEnum.FOLDER_DOCUMENTI_CE_KEY));
		allegatoFilenet.setGuid(guid);
		allegatoFilenet.setClasseDocumentale(pp.getParameterByKey(PropertiesNameEnum.ALLEGATO_NSD_OP_FN_METAKEY));
		allegatoFilenet.setMetadati(metadatiAllegato);
		if (mediaAllegato != null && guid == null) {
			allegatoFilenet.setDataHandler(FileUtils.toDataHandler(mediaAllegato));
			allegatoFilenet.setContentType(contentType);
		}

		return allegatoFilenet;
	}

	/**
	 * Restituisce lo stream associato al content del documento idenficato dal guid
	 * recuperato all'interno dell' <code> allegato </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAllegatoFacadeSRV#getStreamByGuid(it.ibm.red.business.dto.AllegatoDTO,
	 *      it.ibm.red.business.dto.UtenteDTO)
	 */
	@Override
	public InputStream getStreamByGuid(final AllegatoDTO allegato, final UtenteDTO utente) {
		String guid = allegato.getGuid();
		if (!CollectionUtils.isEmpty(allegato.getVersions())) {
			final List<VersionDTO> versionList = allegato.getVersions();
			final VersionDTO version = versionList.get(0);
			guid = version.getGuid();
		}
		return documentManagerSRV.getStreamDocumentoByGuid(guid, utente);
	}

	/**
	 * Estrae gli allegati minimi restituendoli.
	 * 
	 * @see it.ibm.red.business.service.IAllegatoSRV#extractMinimalAllegati(com.filenet.api.core.Document)
	 */
	@Override
	public List<AllegatoDTO> extractMinimalAllegati(final Document docMail) {
		final List<AllegatoDTO> allegatiList = new ArrayList<>();
		final Iterator<?> docIter = docMail.get_ChildDocuments().iterator();
		while (docIter.hasNext()) {
			final Document docAttach = (Document) docIter.next();
			final AllegatoDTO allegato = new AllegatoDTO();
			// A.D.F. prima veniva settato nel documentId,
			// siccome dovrebbe essere impostato il documentDetail nel documenId l'ho
			// modificato in guid
			allegato.setGuid(docAttach.get_Id().toString());
			allegato.setNomeFile(docAttach.get_Name());
			allegato.setMimeType(docAttach.get_MimeType());
			allegatiList.add(allegato);
		}
		return allegatiList;
	}

	/**
	 * Effettua la copia della versione.
	 * 
	 * @see it.ibm.red.business.service.facade.IAllegatoFacadeSRV#copyVersion(it.ibm.red.business.dto.VersionDTO)
	 */
	@Override
	public VersionDTO copyVersion(final VersionDTO currentVersion) {
		final VersionDTO copyCurrentVersion = new VersionDTO(currentVersion.getGuid(), currentVersion.getDateCheckedIn(), currentVersion.getMimeType(),
				currentVersion.isCurrentVersion(), // currentVersion
				currentVersion.isFrozenVersion(), //
				currentVersion.getClassName(), currentVersion.getFileName(), currentVersion.getValoreVerificaFirma(), currentVersion.isVisualizzaInfoVerificaFirma(),
				currentVersion.getCreatore());
		copyCurrentVersion.setVersionNumber(currentVersion.getVersionNumber());
		return copyCurrentVersion;
	}

	/**
	 * Effettua la trasformazione da documento allegabile in allegato.
	 * 
	 * @see it.ibm.red.business.service.facade.IAllegatoFacadeSRV#transformFrom(java.util.List,
	 *      java.util.List)
	 */
	@Override
	public List<AllegatoDTO> transformFrom(final List<DocumentoAllegabileDTO> allegabileList, final List<TipologiaDocumentoDTO> tipoDocAttive) {
		final List<AllegatoDTO> allegatoList = new ArrayList<>();
		try {

			for (final DocumentoAllegabileDTO allegabile : allegabileList) {
				final boolean istipoAllegato = DocumentoAllegabileType.ALLEGATO.equals(allegabile.getTipo());

				if (allegabile.getFascicolo() == null) {
					throw new IllegalArgumentException("Il documento allegato DTO con guid: " + allegabile.getGuid() + " e documentTitle: " + allegabile.getDocumentTitle()
							+ "non dispone di fascicolo.");
				}
				// numeroFascicolo_annoFascicolo
				final List<String> descrizioneDocumentoList = Arrays.asList(allegabile.getFascicolo().getNomeDocumento(), allegabile.getFascicolo().getAnno());
				final String descrizioneFascicolo = StringUtils.nullIgnoreJoin("_", descrizioneDocumentoList);

				// numeroProtocollo/annoProtocollo - numeroDoumento
				final List<Integer> descrizioneFascicoloIntList = Arrays.asList(allegabile.getNumeroProtocollo(), allegabile.getAnnoDocumento());
				String descrizioneDocumento = StringUtils.nullIgnoreJoin("/", descrizioneFascicoloIntList);
				final List<Object> descrizioneFascicoloAndDocList = Arrays.asList(descrizioneDocumento, allegabile.getNumeroDocumento());
				descrizioneDocumento = StringUtils.nullIgnoreJoin(" - ", descrizioneFascicoloAndDocList);

				final AllegatoDTO allegato = new AllegatoDTO(null, // documentTitle
						FormatoAllegatoEnum.ELETTRONICO.getId(), // formato in questi casi e'sempre elettronico.
						null, // idTipologiadocumento
						null, // TipologiaDocumento
						null, // l'oggetto dell'allegato deeve essere vuoto perche' non c'entra molto.
						null, // barcode
						true, // formatoOriginale
						0, // daFirmare
						false, // copiaConforme
						null, // guid
						allegabile.getNomeFile(), // nomeFile
						FileUtils.getMimeType(FilenameUtils.getExtension(allegabile.getNomeFile())), // mymetype
						new Date(), // dateCreated
						null, // idUfficioCopiaConforme,
						null, // idUtenteCopiaConforme,
						allegabile.getFascicolo().getIdFascicolo(), // idFascicoloProvenienza,
						descrizioneFascicolo, // descrizioneFascicoloProvenienza,
						allegabile.getDocumentTitleDocumentoPrincipale(), // documentTitleDocumentoProvenienza,
						descrizioneDocumento, // descrizioneDocumentoProvenienza,
						istipoAllegato ? allegabile.getDocumentTitle() : null, // documentTitleAllegatoProvenienza,
						istipoAllegato ? allegabile.getNomeFile() : null, // nomeFileAllegatoProvenienza)
						tipoDocAttive, false, false, null, false, null, false, null);

				if (allegabile.getContenuto() != null) {
					allegato.setContent(IOUtils.toByteArray(allegabile.getContenuto().getInputStream()));
					final String sizeMBRound = String.format("%.2f", (float) allegato.getContent().length).replace(",", ".");
					allegato.setContentSize(Float.parseFloat(sizeMBRound));
				}

				allegato.setCheckDaFirmareVisible(true);
				allegato.setCheckMantieniFormatoOriginaleVisible(true);
				allegato.setCheckMantieniFormatoOriginaleDisable(true);

				allegato.setCheckTimbroUscitaAooDisabled(true);

				allegato.setDocumentTitleDaAllacci(allegabile.getDocumentTitle());

				allegatoList.add(allegato);
			}
		} catch (final Exception e) {
			LOGGER.error("Errori durante la trasformazione da documento allegabile a allegato", e);
			throw new RedException(e);
		}
		return allegatoList;
	}
}