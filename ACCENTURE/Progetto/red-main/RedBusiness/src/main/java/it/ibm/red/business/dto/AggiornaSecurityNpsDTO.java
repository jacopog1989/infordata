package it.ibm.red.business.dto;

import java.io.Serializable;

/**
 * @author VINGENITO
 *
 */
public class AggiornaSecurityNpsDTO implements Serializable {

	 
	private static final long serialVersionUID = 7587725455102596736L;
	
	/**
	 * Documento in uscita.
	 */
	private String aclDocUscita;
	
	/**
	 * Identificativo protocollo.
	 */
	private String idProtocollo;
	  
	/**
	 * Flag aggiorna acl.
	 */
	private boolean aggiornaAclNps;
	
	/**
	 * Informazione protocollo.
	 */
	private String infoProtocollo;
	
	/**
	 * Utente.
	 */
	private UtenteDTO utente;
	
	/**
	 * Anno protocollo.
	 */
	private Integer annoProtocollo;

	/**
	 * Imposta l'anno del protocollo.
	 * @param annoProtocollo
	 */
	public void setAnnoProtocollo(final Integer annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}

	/**
	 * Restituisce l'anno in cui è stato creato il protocollo.
	 * @return anno protocollo
	 */
	public Integer getAnnoProtocollo() {
		return annoProtocollo;
	}

	/**
	 * Imposta l'utente.
	 * @param utente
	 */
	public void setUtente(final UtenteDTO utente) {
		this.utente = utente;
	}

	/**
	 * Restituisce le informazioni sull'utente con un DTO.
	 * @return utenteDTO
	 */
	public UtenteDTO getUtente() {
		return utente;
	}

	/**
	 * Restituisce informazioni sul protocollo sotto forma di String.
	 * @return info protocollo
	 */
	public String getInfoProtocollo() {
		return infoProtocollo;
	}

	/**
	 * Imposta le informazioni sul protocollo.
	 * @param infoProtocollo
	 */
	public void setInfoProtocollo(final String infoProtocollo) {
		this.infoProtocollo = infoProtocollo;
	}

	/**
	 * Restituisce l'id del protocollo.
	 * @return id protocollo
	 */
	public String getIdProtocollo() {
		return idProtocollo;
	}
	/**
	 * Imposta l'id del protocollo.
	 * @param idProtocollo
	 */
	public void setIdProtocollo(final String idProtocollo) {
		this.idProtocollo = idProtocollo;
	}

	/**
	 * Restituisce il valore di aggiornaAclNps.
	 * @return aggiornaAclNps
	 */
	public boolean getAggiornaAclNps() {
		return aggiornaAclNps;
	}

	/**
	 * Imposta il valore di aggiornaAclNps.
	 * @param aggiornaAclNps
	 */
	public void setAggiornaAclNps(final boolean aggiornaAclNps) {
		this.aggiornaAclNps = aggiornaAclNps;
	}

	/**
	 * Restituisce le acl del documento in uscita.
	 * @return aclDocUscita
	 */
	public String getAclDocUscita() {
		return aclDocUscita;
	}

	/**
	 * Imposta le acl del documento in uscita.
	 * @param aclDocUscita
	 */
	public void setAclDocUscita(final String aclDocUscita) {
		this.aclDocUscita = aclDocUscita;
	}
}
