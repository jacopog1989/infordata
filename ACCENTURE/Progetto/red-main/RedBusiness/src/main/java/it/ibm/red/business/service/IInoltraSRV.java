package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IInoltraFacadeSRV;

/**
 * Interface del servizio di inoltro.
 */
public interface IInoltraSRV extends IInoltraFacadeSRV {

}
