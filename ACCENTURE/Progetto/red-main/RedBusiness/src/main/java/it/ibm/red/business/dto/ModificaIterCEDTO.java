package it.ibm.red.business.dto;

import java.util.Date;
import java.util.List;

/**
 * 
 * Struttura DTO per modifica iter.
 * 
 * @author CRISTIANOPierascenzi
 *
 */
public class ModificaIterCEDTO extends AbstractDTO {

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificativo categoria documento.
	 */
	private Integer idCategoriaDocumento;

	/**
	 * Identificativo formato documento.
	 */
	private Integer idFormatoDocumento;
	
	/**
	 * Identificativo tipo procedimento.
	 */
	private Integer idTipoProcedimento;
	
	/**
	 * Registro riservato.
	 */
	private Integer registroRiservato;
	
	/**
	 * Destinatari contributo esterno.
	 */
	private String destinatariContributoEsterno;
	
	/**
	 * Flag riservato.
	 */
	private Boolean isRiservato;
	
	/**
	 * Data scadenza.
	 */
	private Date dataScadenza;
	
	/**
	 * Lista destinatari.
	 */
	private List<String[]> destinatari;
	
	/**
	 * Identificativo protocollo.
	 */
	private String idProtocollo;

	/**
	 * Anno protocollo.
	 */
	private Integer annoProtocollo;

	/**
	 * Data protocollo.
	 */
	private Date dataProtocollo;
	
	/**
	 * guid del documento
	 */
	private String guid;
	
	/**
	 * guid del documento
	 */
	private Integer numDoc;

	/**
	 * Costruttore.
	 * 
	 * @param inIdCategoriaDocumento
	 *            identificativo categoria documento
	 * @param inIdFormatoDocumento
	 *            identificativo formato documento
	 * @param inIdTipoProcedimento
	 *            identificativo tipo procedimento
	 * @param inRegistroRiservato
	 *            flag registro riservato
	 * @param inDestinatariContributoEsterno
	 *            destinatari contributo esterno
	 * @param inIsRiservato
	 *            flag riservato
	 * @param inDataScadenza
	 *            data scadenza
	 * @param inDestinatari
	 *            lista destinatari
	 * @param inIdProtocollo
	 *            identificativo protocollo
	 * @param inAnnoProtocollo
	 *            anno protocollo
	 * @param inDataProtocollo
	 *            data protocollo
	 */
	public ModificaIterCEDTO(final Integer inIdCategoriaDocumento, final Integer inIdFormatoDocumento, final Integer inIdTipoProcedimento,
			final Integer inRegistroRiservato, final String inDestinatariContributoEsterno, final Boolean inIsRiservato, 
			final Date inDataScadenza, final List<String[]> inDestinatari, 
			final String inIdProtocollo, final Integer inAnnoProtocollo, final Date inDataProtocollo, final String inGuid, final Integer inNumDoc) {
		super();
		this.idCategoriaDocumento = inIdCategoriaDocumento;
		this.idFormatoDocumento = inIdFormatoDocumento;
		this.idTipoProcedimento = inIdTipoProcedimento;
		this.registroRiservato = inRegistroRiservato;
		this.destinatariContributoEsterno = inDestinatariContributoEsterno;
		this.isRiservato = inIsRiservato;
		this.dataScadenza = inDataScadenza;
		this.destinatari = inDestinatari;
		this.idProtocollo = inIdProtocollo;
		this.annoProtocollo = inAnnoProtocollo;
		this.dataProtocollo = inDataProtocollo;
		this.guid = inGuid;
		this.numDoc = inNumDoc;
	}

	/**
	 * Getter.
	 * 
	 * @return	identificativo protocollo
	 */
	public final String getIdProtocollo() {
		return idProtocollo;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	anno protocollo
	 */
	public final Integer getAnnoProtocollo() {
		return annoProtocollo;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	data protocollo
	 */
	public final Date getDataProtocollo() {
		return dataProtocollo;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	lista destinatari
	 */
	public final List<String[]> getDestinatari() {
		return destinatari;
	}
	
	/**
	 * Setter.
	 * 
	 * @param inDestinatari	lista destinatari
	 */
	public final void setDestinatari(final List<String[]> inDestinatari) {
		this.destinatari = inDestinatari;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	identificativo categoria documento
	 */
	public final Integer getIdCategoriaDocumento() {
		return idCategoriaDocumento;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	identificativo formato documento
	 */
	public final Integer getIdFormatoDocumento() {
		return idFormatoDocumento;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	identificativo tipo procedimento
	 */
	public final Integer getIdTipoProcedimento() {
		return idTipoProcedimento;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	flag registro riservato
	 */
	public final Integer getRegistroRiservato() {
		return registroRiservato;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	destinatari contributo esterno
	 */
	public final String getDestinatariContributoEsterno() {
		return destinatariContributoEsterno;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	flag riservato
	 */
	public final Boolean getIsRiservato() {
		return isRiservato;
	}

	/**
	 * Getter.
	 * 
	 * @return	data scadenza
	 */
	public final Date getDataScadenza() {
		return dataScadenza;
	}

	/**
	 * @return the guid
	 */
	public final String getGuid() {
		return guid;
	}

	/**
	 * @return the numDoc
	 */
	public final Integer getNumDoc() {
		return numDoc;
	}
	
	
	
	
}
