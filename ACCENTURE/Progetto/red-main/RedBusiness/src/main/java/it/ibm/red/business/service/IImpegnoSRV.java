package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IImpegnoFacadeSRV;

/**
 * Interface del servizio gestione impegno.
 */
public interface IImpegnoSRV extends IImpegnoFacadeSRV {

}
