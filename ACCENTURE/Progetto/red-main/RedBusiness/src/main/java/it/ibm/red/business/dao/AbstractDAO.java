package it.ibm.red.business.dao;

import java.io.OutputStream;
import java.io.Serializable;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.utils.StringUtils;

/**
 * 
 * @author CPIERASC
 *
 *	DAO astratto.
 */
public abstract class AbstractDAO implements Serializable {

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Costante che identifica il valore NULL.
	 */
	protected static final String NULL = "NULL";
	
	/**
	 * Label applicazione.
	 */
	protected static final String APPLICAZIONE_DB = "REDEVO";

	/**
	 * Metodo per rendere testo eventuali comandi sql presenti in una stringa.
	 * 
	 * @param strToSanitize	stringa da normalizzare
	 * @return				stringa normalizzata
	 */
	public static final String sanitize(final String strToSanitize) {
		String output = null;
		if (!StringUtils.isNullOrEmpty(strToSanitize)) {
			output = " '" + strToSanitize.replace("'", "''") + "' ";
		}
		return output;
	}
	
	/**
	 * Metodo per rendere sicuri i parametri Long forniti in input ad una query. 
	 * 
	 * @param value	valore
	 * @return		valore
	 */
	public static final String sanitize(final Long value) {
		String output = "";
		if (value != null) {
			output = value.toString();
		}
		return output;
	}

	/**
	 * Gestisce eventuali nomi di colonne null, sostituendole con "".
	 * 
	 * @param colName la label da rendere Null Safe.
	 * @return String mai Null.
	 * */
	public static final String sanitizeCol(final String colName) {
		String output = "";
		if (colName != null) {
			output = colName;
		}
		return output;
	}
	
	
	/**
	 * Metodo per rendere sicuri i parametri Integer forniti in input ad una query. 
	 * 
	 * @param value	valore
	 * @return		valore
	 */
	public static final String sanitize(final Integer value) {
		String output = "";
		if (value != null) {
			output = value.toString();
		}
		return output;
	}
	
	/**
	 * Metodo per rendere compliance un valore booleano in una istruzione SQL.
	 * 
	 * @param value valore
	 * @return      valore
	 */
	public static final String fromBooleanToChar(final boolean value) {
		String output = "";	
		if (value) {
			output = "'1'";
		} else {
			output = "'0'";
		}
		return output;
	}
	
	/**
	 * Metodo per la chiusura del prepare statement.
	 * 
	 * @param ps	prepare statment da chiudere
	 */
	public static final void closeStatement(final Statement ps) {
		closeStatement(ps, null);
	}
	
	
	/**
	 * Metodo per la chiusura del prepare statement e del result set.
	 * 
	 * @param ps	prepare statement
	 * @param rs	result set
	 */
	public static final void closeStatement(final Statement ps, final ResultSet rs) {
		if (rs != null) {
			try {
				if (!rs.isClosed()) {
					rs.close();
				}
			} catch (SQLException e) {
				throw new RedException(e);
			}
		}
		if (ps != null) {
			try {
				if (!ps.isClosed()) {
					ps.close();
				}
			} catch (SQLException e) {
				throw new RedException(e);
			}
		}
	}

	/**
	 * Close callable statement.
	 * 
	 * @param cs	callable statement.
	 */
	protected void close(final CallableStatement cs) {
		try {
			if (!cs.isClosed()) {
				cs.close();
			}
		} catch (SQLException e) {
			throw new RedException(e);
		}
	}
	
	/**
	 * Crea il Blob dal byte[] in input.
	 * 
	 * @param connection
	 * @param byteArray
	 * @return Blob recuperato.
	 */
	protected Blob getBlob(final Connection connection, final byte[] byteArray) {
		try {
			Blob blob = connection.createBlob();
			
			OutputStream writer = blob.setBinaryStream(1);
			writer.write(byteArray);
			writer.flush();
			writer.close();
			
			return blob;
		} catch (Exception e) {
			throw new RedException(e);
		}
	}
	
	/**
	 * Gestisce la chiusura del ResultSet.
	 * 
	 * @param rs ResultSet da chiudere
	 * */
	public static void closeResultset(final ResultSet rs) {
		if (rs != null) {
			try {
				rs.close();
			} catch (SQLException e) {
				throw new RedException(e);
			}
		}
	}

	/**
	 * Restituisce il <em> next val </em> dalla sequence con nome
	 * {@code sequenceName}.
	 * 
	 * @param con
	 *            Connessione al database.
	 * @param sequenceName
	 *            Nome della sequence.
	 * @return Id recuperato dalla sequence.
	 */
	protected static long getNextId(final Connection con, final String sequenceName) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		long nextId = 0;

		try {
			ps = con.prepareStatement("SELECT " + sequenceName + ".NEXTVAL FROM DUAL");
			rs = ps.executeQuery();

			if (rs.next()) {
				nextId = rs.getLong(1);
			}
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero della sequence con nome " + sequenceName, e);
		} finally {
			closeStatement(ps, rs);
		}

		return nextId;
	}
	
}
