package it.ibm.red.business.service;

import java.util.List;

import it.ibm.red.business.persistence.model.DEventiCustom;

import it.ibm.red.business.service.facade.IStoricoFacadeSRV;

/**
 * Interfaccia del servizio che gestisce lo storico.
 */
public interface IStoricoSRV extends IStoricoFacadeSRV {

	/**
	 * Recupera il model filenet che gestisce lo storico del documento.
	 * 
	 * @param idDocumento
	 *  Id del documento da cui recuperare lo storico.
	 * @param idAoo
	 *  Serve come rafforzativo ma può essere tranquillamente annullato.
	 * @return
	 */
	List<DEventiCustom> getStoricoDocumento(Integer idDocumento, Integer idAoo);
	 
	/**
	 * Verifica se il documento è lavorato da ufficio.
	 * @param idAoo
	 * @param documentTitle
	 * @param idUfficio
	 * @return true o false
	 */
	boolean isDocLavoratoDaUfficio(Long idAoo, String documentTitle, Long idUfficio);
	
}
