package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IGestioneColonneFacadeSRV;

/**
 * The Class IGestioneColonneSRV.
 *
 * @author VINGENITO
 * 
 *         SRV per gestione delle colonne;
 */
public interface IGestioneColonneSRV extends IGestioneColonneFacadeSRV {
	 
	
	
}