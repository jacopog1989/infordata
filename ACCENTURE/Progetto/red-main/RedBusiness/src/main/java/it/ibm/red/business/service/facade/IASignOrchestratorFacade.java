/**
 * 
 */
package it.ibm.red.business.service.facade;

import java.io.Serializable;

import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.service.asign.step.StepEnum; 

/**
 * Facade dell'orchestrator di firma asincrona.
 */
public interface IASignOrchestratorFacade extends Serializable {

	/**
	 * Esegue l'orchestrazione di firma del primo item disponibile.
	 */
	Long play();
	
	/**
	 * Esegue l'orchestrazione di firma per l'item dato dall'id
	 * @param idItem
	 * 
	 * */
	Long play(Long idItem);
	
	/**
	 * Esegue l'orchestrazione di firma del primo item recuperato (deve essere in stato retry, e deve essere compatibile col numero massimo di retry
	 * e con il quanto di tempo minimo che deve separare due retry consecutivi).
	 */
	Long resume();
	
	/**
	 * Esegue l'orchestrazione di firma dell'item.
	 * @param item
	 * 
	 */
	Long resume(ASignItemDTO item);

	/**
	 * Esegue il {@link #play()} sull'item identificato dal <code> idItem </code>
	 * simulando un crash durante l'esecuzione dello step identificato da <code> crashStep </code>.
	 * @see StepEnum.
	 * @param idItem
	 * @param crashStep
	 * @return id dell'item
	 */
	Long playSimulatingCrash(Long idItem, StepEnum crashStep);
	
}
