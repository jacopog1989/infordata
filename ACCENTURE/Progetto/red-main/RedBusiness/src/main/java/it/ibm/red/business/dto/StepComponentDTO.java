package it.ibm.red.business.dto;

/**
 * The Class StepComponentDTO.
 *
 * @author CPIERASC
 * 
 * 	Step component dto.
 */
public class StepComponentDTO {

	/**
	 * Tipo.
	 */
	private String type;
	
	/**
	 * Label.
	 */
	private String label;
	
	/**
	 * Contenuto master.
	 */
	private String shortContent;

	/**
	 * Contenuto detail.
	 */
	private String fullContent;

	/**
	 * Show detail.
	 */
	private String showMore;

	/**
	 * Show master.
	 */
	private String showLess;
	
	/**
	 * Picture.
	 */
	private String picto;

	/**
	 * Css class master/detail.
	 */
	private String cssClass;
	
	/**
	 * Posizione.
	 */
	private String position;
	

	/**
	 * Costruttore.
	 * 
	 * @param inType			tipo
	 * @param inLabel			label
	 * @param inShortContent	contenuto master
	 * @param inFullContent		contenuto detail
	 * @param inShowMore		testo show detail
	 * @param inShowLess		testo show master
	 * @param inPicto			picto
	 * @param inCssClass		css class
	 */
	public StepComponentDTO(final String inType, final String inLabel, final String inShortContent, final String inFullContent, final String inShowMore,
			final String inShowLess, final String inPicto, final String inCssClass, final String inPosition) {
		super();
		this.type = inType;
		this.label = inLabel;
		this.shortContent = inShortContent;
		this.fullContent = inFullContent;
		this.showMore = inShowMore;
		this.showLess = inShowLess;
		this.picto = inPicto;
		this.cssClass = inCssClass;
		this.position = inPosition;
	}

	/**
	 * Getter.
	 * 
	 * @return	tipo
	 */
	public final String getType() {
		return type;
	}

	/**
	 * Getter.
	 * 
	 * @return	label
	 */
	public final String getLabel() {
		return label;
	}

	/**
	 * Getter.
	 * 
	 * @return	content master
	 */
	public final String getShortContent() {
		return shortContent;
	}

	/**
	 * Getter.
	 * 
	 * @return	content detail
	 */
	public final String getFullContent() {
		return fullContent;
	}

	/**
	 * Getter.
	 * 
	 * @return	show detail
	 */
	public final String getShowMore() {
		return showMore;
	}

	/**
	 * Getter.
	 * 
	 * @return	show master
	 */
	public final String getShowLess() {
		return showLess;
	}

	/**
	 * Getter.
	 * 
	 * @return	picto
	 */
	public final String getPicto() {
		return picto;
	}

	/**
	 * Getter.
	 * 
	 * @return	css class
	 */
	public final String getCssClass() {
		return cssClass;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	position
	 */
	public final String getPosition() {
		return position;
	}
}
