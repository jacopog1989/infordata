package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dto.TipoProcedimentoDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.TipoProcedimento;

/**
 * The Class TipoProcedimentoDAO.
 *
 * @author CPIERASC
 * 
 *         Dao per la gestione dei tipi procedimento.
 */
@Repository
public class TipoProcedimentoDAO extends AbstractDAO implements ITipoProcedimentoDAO {

	private static final String IDTIPOPROCEDIMENTO = "IDTIPOPROCEDIMENTO";

	private static final String DESCRIZIONE = "DESCRIZIONE";

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TipoProcedimentoDAO.class.getName());
	
	/**
	 * Messaggio errore recupero tipi procedimento.
	 */
	private static final String ERROR_RECUPERO_TIPI_PROCEDIMENTI_MSG = "Errore durante il recupero dei tipi di procedimenti ";
	
	/**
	 * Gets the all.
	 *
	 * @param connection
	 *            the connection
	 * @return the all
	 */
	@Override
	public final Collection<TipoProcedimento> getAll(final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final Collection<TipoProcedimento> tipi = new ArrayList<>();
		try {
			final String querySQL = "SELECT tp.*, wf.workflowdescription, wf.flaggenericoentrata, wf.flaggenericouscita, tpa.idaoo "
					+ "FROM tipoprocedimento tp, workflow wf, tipoprocedimentoaoo tpa "
					+ "WHERE tp.idtipoprocedimento = tpa.idtipoprocedimento AND tp.idworkflow = wf.idworkflow";
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			while (rs.next()) {
				tipi.add(new TipoProcedimento(rs.getLong(IDTIPOPROCEDIMENTO), rs.getString(DESCRIZIONE),
						rs.getDate("DATAATTIVAZIONE"), rs.getDate("DATADISATTIVAZIONE"), rs.getLong("IDWORKFLOW"),
						rs.getString("WORKFLOWDESCRIPTION"), rs.getInt("IDAOO"), rs.getInt("FLAGGENERICOENTRATA"),
						rs.getInt("FLAGGENERICOUSCITA")));
			}
		} catch (final SQLException e) {
			throw new RedException(ERROR_RECUPERO_TIPI_PROCEDIMENTI_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		return tipi;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipoProcedimentoDAO#getTPbyId(java.sql.Connection,
	 *      java.lang.Integer).
	 */
	@Override
	public TipoProcedimento getTPbyId(final Connection connection, final Integer idProcedimento) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		TipoProcedimento tipo = null;
		try {
			final String querySQL = "SELECT tp.* FROM tipoprocedimento tp WHERE tp.idtipoprocedimento = ?";
			ps = connection.prepareStatement(querySQL);
			ps.setInt(1, idProcedimento);
			rs = ps.executeQuery();
			if (rs.next()) {
				tipo = new TipoProcedimento(rs.getLong(IDTIPOPROCEDIMENTO), rs.getString(DESCRIZIONE));
			}
		} catch (final SQLException e) {
			throw new RedException(ERROR_RECUPERO_TIPI_PROCEDIMENTI_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		return tipo;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipoProcedimentoDAO#getAllDesc(java.sql.Connection).
	 */
	@Override
	public Map<Long, String> getAllDesc(final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final Map<Long, String> output = new HashMap<>();
		try {
			final String querySQL = "SELECT IDTIPOPROCEDIMENTO,DESCRIZIONE FROM tipoprocedimento tp ";
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			while (rs.next()) {
				output.put(rs.getLong(IDTIPOPROCEDIMENTO), rs.getString(DESCRIZIONE));
			}
		} catch (final SQLException e) {
			throw new RedException(ERROR_RECUPERO_TIPI_PROCEDIMENTI_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipoProcedimentoDAO#getTPCompletobyId(java.sql.Connection,
	 *      java.lang.Integer).
	 */
	@Override
	public TipoProcedimento getTPCompletobyId(final Connection connection, final Integer idProcedimento) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		TipoProcedimento tipo = null;
		try {
			final String querySQL = "SELECT tp.*, "
					+ "       wf.workflowdescription, " 
					+ "       wf.flaggenericoentrata, "
					+ "       wf.flaggenericouscita, " 
					+ "       tpa.idaoo "
					+ "  FROM tipoprocedimento tp, workflow wf, tipoprocedimentoaoo tpa "
					+ " WHERE tp.idtipoprocedimento = tpa.idtipoprocedimento " 
					+ "   AND tp.idworkflow = wf.idworkflow "
					+ "   and tp.idtipoprocedimento = ?";
			ps = connection.prepareStatement(querySQL);
			ps.setInt(1, idProcedimento);
			rs = ps.executeQuery();
			if (rs.next()) {
				tipo = new TipoProcedimento(rs.getLong(IDTIPOPROCEDIMENTO), rs.getString(DESCRIZIONE),
						rs.getDate("DATAATTIVAZIONE"), rs.getDate("DATADISATTIVAZIONE"), rs.getLong("IDWORKFLOW"),
						rs.getString("WORKFLOWDESCRIPTION"), rs.getInt("IDAOO"), rs.getInt("FLAGGENERICOENTRATA"),
						rs.getInt("FLAGGENERICOUSCITA"));
			}
		} catch (final SQLException e) {
			throw new RedException(ERROR_RECUPERO_TIPI_PROCEDIMENTI_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		return tipo;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipoProcedimentoDAO#getTipiProcedimentoByTipologiaDocumento(java.sql.Connection,
	 *      java.lang.Integer).
	 */
	@Override
	public List<TipoProcedimento> getTipiProcedimentoByTipologiaDocumento(final Connection connection, final Integer idTipologiaDocumento) {
		List<TipoProcedimento> list = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			final String querySQL = "SELECT tp.idtipoprocedimento, tp.descrizione, tdp.tipogestione "
					+ " FROM tipologiadocumento tpd, tipologiadocumentoprocedimento tdp, tipoprocedimento tp "
					+ " WHERE tpd.idtipologiadocumento = tdp.idtipologiadocumento "
					+ " AND tdp.idtipoprocedimento = tp.idtipoprocedimento "
					+ " AND tdp.idtipologiadocumento = ? "
					+ " ORDER BY tp.descrizione";
			ps = connection.prepareStatement(querySQL);
			ps.setInt(1, idTipologiaDocumento);
			rs = ps.executeQuery();

			list = new ArrayList<>();
			while (rs.next()) {
				list.add(new TipoProcedimento(rs.getLong(IDTIPOPROCEDIMENTO), rs.getString(DESCRIZIONE), rs.getString("TIPOGESTIONE")));
			}
		} catch (final SQLException e) {
			throw new RedException(ERROR_RECUPERO_TIPI_PROCEDIMENTI_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return list;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipoProcedimentoDAO#getWorkflowNameByIdTipoProcedimento(long,
	 *      java.sql.Connection).
	 */
	@Override
	public String getWorkflowNameByIdTipoProcedimento(final long idTipoProcedimento, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String workflowName = null;

		try {
			final String querySQL = "SELECT wf.workflowname FROM tipoprocedimento tp, workflow wf WHERE tp.idworkflow = wf.idworkflow AND tp.idtipoprocedimento = ?";
			ps = con.prepareStatement(querySQL);
			ps.setLong(1, idTipoProcedimento);
			rs = ps.executeQuery();

			while (rs.next()) {
				workflowName = rs.getString("WORKFLOWNAME");
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero del nome del workflow per il tipo procedimento: " + idTipoProcedimento, e);
			throw new RedException("Errore durante il recupero del nome del workflow per il tipo procedimento: " + idTipoProcedimento, e);
		} finally {
			closeStatement(ps, rs);
		}

		return workflowName;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipoProcedimentoDAO#getIdsByDescrizione(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public List<Integer> getIdsByDescrizione(final String descrizioneTipoProcedimento, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<Integer> result = new ArrayList<>();

		try {
			final String querySQL = " SELECT td.idtipoprocedimento FROM tipoprocedimento td WHERE UPPER(td.descrizione) = ?";
			ps = connection.prepareStatement(querySQL);

			int index = 1;
			ps.setString(index++, descrizioneTipoProcedimento.toUpperCase());

			rs = ps.executeQuery();

			while (rs.next()) {
				result.add(rs.getInt("idtipoprocedimento"));
			}
		} catch (final SQLException e) {
			throw new RedException(
					"Errore nel recupero dei tipi procedimento con descrizione: " + descrizioneTipoProcedimento, e);
		} finally {
			closeStatement(ps, rs);
		}

		return result;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipoProcedimentoDAO#checkStampigliaturaSiglaVisible(java.lang.Long,
	 *      java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public boolean checkStampigliaturaSiglaVisible(final Long idUffIstruttore, final Integer idTipologiaDoc, final Connection conn) {
		boolean output = false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT * FROM GLIFO_UTENTE WHERE ID_UFF_ISTRUTTORE = ? AND ID_TIPO_DOC = ?");
			 
			ps = conn.prepareStatement(sb.toString());  
			ps.setLong(index++, idUffIstruttore);
			ps.setInt(index++, idTipologiaDoc);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				output = true;
			}
		} catch (final Exception ex) {
			LOGGER.error("Errore nel recupero della stampigliatura di sigla : " + ex);
			throw new RedException("Errore nel recupero della stampigliatura di sigla : " + ex);
		} finally {
			closeStatement(ps, rs);
		}
		
		return output;
	} 
	
	
	/**
	 * Esegue il salvataggio del procedimento effettuandone la insert sul database.
	 * 
	 * @see it.ibm.red.business.dao.ITipoProcedimentoDAO#save(it.ibm.red.business.dto.TipoProcedimentoDTO,
	 *      java.sql.Connection).
	 * @param procedimento
	 *            procedimento da salvare sulla base dati
	 * @param connection
	 *            connessione al database
	 * @return id del procedimento generato dalla sequence:
	 *         <code> SEQ_TIPOLOGIAPROCEDIMENTO </code>
	 */
	@Override
	public Long save(final TipoProcedimentoDTO procedimento, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		// Recuper id tipologia procedimento
		Long idProcedimento = 0L;
		final String queryGetId = "SELECT SEQ_TIPOLOGIAPROCEDIMENTO.NEXTVAL FROM DUAL";
		try {
			ps = connection.prepareStatement(queryGetId);
			rs = ps.executeQuery();
			if (rs.next()) {
				idProcedimento = rs.getLong(1);
			}
		} catch (final Exception e) {
			throw new RedException("Errore durante la creazione dell'id della tipologia procedimento", e);
		} finally {
			closeStatement(ps, rs);
		}
		try {
			final String querySQL = "INSERT INTO TIPOPROCEDIMENTO (IDTIPOPROCEDIMENTO, DESCRIZIONE, DATAATTIVAZIONE, DATADISATTIVAZIONE, IDWORKFLOW) VALUES (?, ?, SYSDATE, ?, ?)";
			int index = 1;
			ps = connection.prepareStatement(querySQL);
			ps.setLong(index++, idProcedimento);
			ps.setString(index++, procedimento.getDescrizione());
			ps.setNull(index++, Types.DATE);
			ps.setLong(index, procedimento.getWorkflowId());
			
			ps.executeUpdate();
		
		} catch (final Exception e) {
			throw new RedException(
					"Errore durante il salvataggio del tipo procedimento con descrizione: " + procedimento.getDescrizione(), e);
		} finally {
			closeStatement(ps);
		}
		return idProcedimento;
	}
	

	/**
	 * Restituisce l'id della tipologia procedimento associata alla descrizione:
	 * <code> descrizione </code> e associata alla tipologia documento identificata
	 * da <code> idTipologiaDocumento </code>.
	 * 
	 * @see it.ibm.red.business.dao.ITipoProcedimentoDAO#getTipoProcedimentoByDescrizioneAndTipologiaDocumento(java.lang.String,
	 *      java.lang.Integer, java.sql.Connection).
	 * @param descrizione
	 *            descrizione tipo procedimento
	 * @param idTipologiaDocumento
	 *            identificativo tipologia documento
	 * @param con
	 *            connessione al database
	 * @return identificativo tipo procedimento
	 */
	@Override
	public Long getIdTipoProcedimentoByDescrizioneAndTipologiaDocumento(final String descrizione, final Integer idTipologiaDocumento, final Connection con) {
		Long output = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			final String querySQL = "SELECT tp.IDTIPOPROCEDIMENTO FROM tipoprocedimento tp, tipologiadocumentoprocedimento tdp " 
					+ "WHERE tdp.idtipoprocedimento = tp.idtipoprocedimento AND LOWER(tp.descrizione) = ? AND tdp.idtipologiadocumento = ?";
			ps = con.prepareStatement(querySQL);
			
			ps.setString(1, descrizione.toLowerCase());
			ps.setInt(2, idTipologiaDocumento);
			
			rs = ps.executeQuery();

			if (rs.next()) {
				output = rs.getLong(IDTIPOPROCEDIMENTO);
			}
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero del tipo procedimento: " + descrizione + " associato alla tipologia documento: " + idTipologiaDocumento, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipoProcedimentoDAO#getIdTipoProcedimentoByTipologiaDocumentoAndRegistroAusiliario(int,
	 *      int, java.sql.Connection).
	 */
	@Override
	public Long getIdTipoProcedimentoByTipologiaDocumentoAndRegistroAusiliario(final int idTipologiaDocumento, final int idRegistroAusiliario, final Connection con) {
		Long output = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			ps = con.prepareStatement("SELECT p.IDPROCEDIMENTO FROM PROCEDIMENTO_REG_AUS p, TIPOPROCEDIMENTO tipoproc WHERE p.REA_ID = ? AND p.IDTIPOLOGIADOCUMENTO = ? "
					+ "AND p.IDPROCEDIMENTO = tipoproc.IDTIPOPROCEDIMENTO AND (tipoproc.DATADISATTIVAZIONE IS NULL OR tipoproc.DATADISATTIVAZIONE > SYSDATE)");
			
			ps.setInt(1, idRegistroAusiliario);
			ps.setInt(2, idTipologiaDocumento);
			
			rs = ps.executeQuery();

			if (rs.next()) {
				output = rs.getLong("IDPROCEDIMENTO");
			}
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero del tipo procedimento associato alla tipologia documento: " + idTipologiaDocumento
					+ " e al registro ausiliario: " + idRegistroAusiliario, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.ITipoProcedimentoDAO#saveCrossProcedimentoDocumento(java.sql.Connection,
	 *      java.lang.Long, int, it.ibm.red.business.dto.TipoProcedimentoDTO).
	 */
	@Override
	public void saveCrossProcedimentoDocumento(final Connection connection, final Long idProcedimento, final int idDocumento, final TipoProcedimentoDTO procedimento) {
		// Salvataggio procedimento nella cross tra documento e procedimento - TIPOLOGIADOCUMENTOPROCEDIMENTO
		PreparedStatement ps = null;
		int index = 1;
		final String queryCrossDocProc = "INSERT INTO TIPOLOGIADOCUMENTOPROCEDIMENTO(IDTIPOPROCEDIMENTO, IDTIPOLOGIADOCUMENTO, DATAATTIVAZIONE, DATADISATTIVAZIONE, TIPOCONTROLLO, TIPOGESTIONE, MINUTISCADENZASOSPESO, CONTROLLOCONTABILE) VALUES(?,?,SYSDATE,?,?,?,?,?)";
		try {
			ps = connection.prepareStatement(queryCrossDocProc);
			ps.setLong(index++, idProcedimento);
			ps.setInt(index++, idDocumento);
			ps.setNull(index++, Types.DATE);
			ps.setString(index++, procedimento.getTipoControllo().getCode());
			ps.setString(index++, procedimento.getTipoGestione().getCode());
			ps.setInt(index++, procedimento.getMinutiScadenzaSospeso());
			ps.setBoolean(index, procedimento.isFlagContabile());
			
			ps.executeUpdate();
		} catch (final Exception e) {
			throw new RedException("Errore durante il salvataggio della relazione tra tipologia documento e tipologia procedimento", e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ITipoProcedimentoDAO#saveCrossProcedimentoAOO(java.lang.Long,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public void saveCrossProcedimentoAOO(final Long idProcedimento, final Long idAoo, final Connection connection) {
		PreparedStatement ps = null;
		final String query = "INSERT INTO TIPOPROCEDIMENTOAOO (IDTIPOPROCEDIMENTO, IDAOO) VALUES (?, ?)";
		try {
			ps = connection.prepareStatement(query);
			
			ps.setLong(1, idProcedimento);
			ps.setLong(2, idAoo);
			
			ps.executeUpdate();
		} catch (final Exception e) {
			LOGGER.error("Errore riscontrato nel salvataggio della relazione procedimento: " + idProcedimento + " - AOO: " + idAoo, e);
			throw new RedException(e.getMessage(), e);
		} finally {
			closeStatement(ps);
		}
	}
	
	
}