package it.ibm.red.business.dao.impl;

import java.io.Reader;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.Date;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IRegistrazioniAusiliarieDAO;
import it.ibm.red.business.dto.RegistrazioneAusiliariaDTO;
import it.ibm.red.business.dto.RegistroDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * DAO per il recupero delle informazioni associate alle registrazioni
 * ausiliarie.
 * 
 * @author SimoneLungarella
 */
@Repository
public class RegistrazioniAusiliarieDAO extends AbstractDAO implements IRegistrazioniAusiliarieDAO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -692340705768871582L;
	
	/**
	 * Logger per la gestione degli errori in console.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RegistrazioniAusiliarieDAO.class.getName());

	/**
	 * Messaggio di errore riscontrato nel salvataggio dei valori dei metadati.
	 */
	private static final String ERROR_SALVATAGGIO_VALORI_METADATI_MSG = "Errore durante il salvataggio dei valori dei metadati";

	/**
	 * @see it.ibm.red.business.dao.IRegistrazioniAusiliarieDAO#getRegistrazioneAusiliaria(java.sql.Connection,
	 *      java.lang.String, int).
	 */
	@Override
	public RegistrazioneAusiliariaDTO getRegistrazioneAusiliaria(final Connection connection, final String documentTitle, final int idAoo) {
		RegistrazioneAusiliariaDTO registroAusiliarioDocInfo = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		StringBuilder sb = null;
		
		try {
			final String sql = "SELECT RE.*, REG.REA_ID, REG.REA_NOME "
					+ "  FROM REGISTRAZIONI_AUSILIARIE RE, REGISTRO_AUSILIARIO REG"
					+ " WHERE RE.RAU_REA_ID = REG.REA_ID"
					+ "   AND DOCUMENT_TITLE = ?"
					+ "   AND IDAOO = ?";
			ps = connection.prepareStatement(sql);
			ps.setString(1, documentTitle);
			ps.setInt(2, idAoo);
			
			rs = ps.executeQuery();
			
			if (rs.next()) {
				registroAusiliarioDocInfo = new RegistrazioneAusiliariaDTO();
				registroAusiliarioDocInfo.setRegistroAusiliario(new RegistroDTO(rs.getInt("REA_ID"), rs.getString("REA_NOME")));
				
				// Recupero metadati serializzati in CLOB
				Clob clob = null;
				clob = rs.getClob("RAU_XML_METADATI");
				if (clob != null) {
					sb = new StringBuilder();
					final Reader reader = clob.getCharacterStream();
					final char[] buffer = new char[(int) clob.length()];
					while (reader.read(buffer) != -1) {
						sb.append(buffer);
					}
				}
				
				registroAusiliarioDocInfo.setClobMetadatiEstesi(sb);
				registroAusiliarioDocInfo.setDataRegistrazione(rs.getDate("DATA_REGISTRAZIONE"));
				registroAusiliarioDocInfo.setNumeroRegistrazione(rs.getInt("NUMERO_REGISTRAZIONE"));
				registroAusiliarioDocInfo.setIdRegistrazione(rs.getString("ID_REGISTRAZIONE"));
				registroAusiliarioDocInfo.setDocumentoDefinitivo((rs.getInt("DOCUMENTO_DEFINITIVO") != 0));
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero delle informazioni del registro ausiliario per il documento: " + documentTitle, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return registroAusiliarioDocInfo;
	}
	
	/**
	 * @see it.ibm.red.business.dao.IRegistrazioniAusiliarieDAO#saveRegistrazioneAusiliaria(java.sql.Connection,
	 *      java.lang.String, int, java.lang.String, int).
	 */
	@Override
	public void saveRegistrazioneAusiliaria(final Connection connection, final String documentTitle, final int idRegistro, final String xml, final int idAoo) {
		PreparedStatement ps = null;

		try {
			int index = 1;
			ps = connection.prepareStatement("INSERT INTO REGISTRAZIONI_AUSILIARIE(RAU_ID, RAU_XML_METADATI, RAU_REA_ID, DOCUMENT_TITLE, NUMERO_REGISTRAZIONE, DATA_REGISTRAZIONE, ID_REGISTRAZIONE, IDAOO) VALUES (SEQ_REGISTRAZIONI_AUSILIARIE.NEXTVAL,?,?,?,?,?,?,?)");

			ps.setString(index++, xml);
			ps.setInt(index++, idRegistro);
			ps.setString(index++, documentTitle);
			
			// Impostazione di Data Registrazione Numero Registrazione e ID Registrazione a null perché nel salvataggio iniziale dei metadati non
			// è possibile conoscere queste tre informazioni
			ps.setNull(index++, Types.INTEGER);
			ps.setNull(index++, Types.DATE);
			ps.setNull(index++, Types.VARCHAR);
			
			ps.setInt(index++, idAoo);
			
			ps.executeUpdate();
		} catch (final Exception e) {
			LOGGER.error(ERROR_SALVATAGGIO_VALORI_METADATI_MSG, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IRegistrazioniAusiliarieDAO#deleteRegistrazioneAusiliaria(java.lang.String,
	 *      int, java.sql.Connection).
	 */
	@Override
	public void deleteRegistrazioneAusiliaria(final String documentTitle, final int idAoo, final Connection con) {
		PreparedStatement ps = null;
		
		try {
			ps = con.prepareStatement("DELETE REGISTRAZIONI_AUSILIARIE WHERE DOCUMENT_TITLE = ? AND IDAOO = ?");
			
			ps.setString(1, documentTitle);
			ps.setInt(2, idAoo);
			
			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante la cancellazione dei metadati del registro ausiliario associati al documento: " + documentTitle, e);
			throw new RedException("Errore durante la cancellazione dei metadati del registro ausiliario associati al documento: " + documentTitle, e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IRegistrazioniAusiliarieDAO#updateRegistrazioneAusiliaria(java.sql.Connection,
	 *      java.lang.String, int, java.lang.String, java.util.Date,
	 *      java.lang.Integer, java.lang.String, boolean).
	 */
	@Override
	public void updateRegistrazioneAusiliaria(final Connection connection, final String documentTitle, final int idAoo, final String xml,
			final Date dataRegistrazione, final Integer numeroRegistrazione, final String idRegistrazione, final boolean documentoDefinitivo) {
		PreparedStatement ps = null;

		try {
			int index = 1;
			ps = connection.prepareStatement("UPDATE REGISTRAZIONI_AUSILIARIE SET RAU_XML_METADATI = ?, NUMERO_REGISTRAZIONE = ?, DATA_REGISTRAZIONE = ?, ID_REGISTRAZIONE = ?, DOCUMENTO_DEFINITIVO = ? WHERE DOCUMENT_TITLE = ? AND IDAOO = ?");

			ps.setString(index++, xml);
			ps.setInt(index++, numeroRegistrazione);
			ps.setTimestamp(index++, new Timestamp(dataRegistrazione.getTime()));
			ps.setString(index++, idRegistrazione);
			ps.setInt(index++, documentoDefinitivo ? 1 : 0);
			
			ps.setString(index++, documentTitle);
			ps.setInt(index++, idAoo);
			
			ps.executeUpdate();
		} catch (final Exception e) {
			LOGGER.error(ERROR_SALVATAGGIO_VALORI_METADATI_MSG, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IRegistrazioniAusiliarieDAO#updateParzialeRegistrazioneAusiliaria(java.sql.Connection,
	 *      java.lang.String, java.lang.Long, java.lang.Integer, java.lang.String,
	 *      java.util.Date).
	 */
	@Override
	public void updateParzialeRegistrazioneAusiliaria(final Connection connection, final String documentTitle, final Long idAoo, final Integer numeroRegistrazione, final String idRegistrazione, final Date dataRegistrazione) {
		PreparedStatement ps = null;
		final String updateQuery = "UPDATE REGISTRAZIONI_AUSILIARIE SET NUMERO_REGISTRAZIONE = ?, ID_REGISTRAZIONE = ?, DATA_REGISTRAZIONE = ? WHERE DOCUMENT_TITLE = ? AND IDAOO = ?";
		
		try {
			int index = 1;
			ps = connection.prepareStatement(updateQuery);
			
			ps.setInt(index++, numeroRegistrazione);
			ps.setString(index++, idRegistrazione);
			ps.setTimestamp(index++, new Timestamp(dataRegistrazione.getTime()));
			ps.setString(index++, documentTitle);
			ps.setLong(index++, idAoo);
			
			ps.executeUpdate();
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'update parziale della registrazione ausiliaria associata al documento: " + documentTitle, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}
	
	/**
	 * @see it.ibm.red.business.dao.IRegistrazioniAusiliarieDAO#updateDatiRegistrazioneAusiliaria(java.sql.Connection,
	 *      java.lang.String, int, java.lang.String, boolean).
	 */
	@Override
	public void updateDatiRegistrazioneAusiliaria(final Connection connection, final String documentTitle, final int idAoo, final String xml, final boolean documentoDefinitivo) {
		PreparedStatement ps = null;

		try {
			int index = 1;
			ps = connection.prepareStatement("UPDATE REGISTRAZIONI_AUSILIARIE SET RAU_XML_METADATI = ?, DOCUMENTO_DEFINITIVO = ? WHERE DOCUMENT_TITLE = ? AND IDAOO = ?");

			ps.setString(index++, xml);
			ps.setInt(index++, documentoDefinitivo ? 1 : 0);
			
			ps.setString(index++, documentTitle);
			ps.setInt(index++, idAoo);
			
			ps.executeUpdate();
		} catch (final Exception e) {
			LOGGER.error(ERROR_SALVATAGGIO_VALORI_METADATI_MSG, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}
	
}