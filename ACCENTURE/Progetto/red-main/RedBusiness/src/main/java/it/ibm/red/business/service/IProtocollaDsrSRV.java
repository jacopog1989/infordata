package it.ibm.red.business.service;

import java.sql.Connection;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.service.facade.IProtocollaDsrFacadeSRV;

/**
 * Interfaccia del servizio di protocollazione DSR.
 */
public interface IProtocollaDsrSRV extends IProtocollaDsrFacadeSRV {
	
	/**
	 * Permette di inizializzare il dettaglio del documento per la protocollazione Dsr.
	 * @param detailDocument
	 * @param utente
	 * @return dettaglio del documento
	 */
	DetailDocumentRedDTO initializeDetailDocumentForProtocollazioneDsr(DetailDocumentRedDTO detailDocument,
			UtenteDTO utente);

	/**
	 * Consente di verificare se il file è protocollabile Un file protocollabile è
	 * anche valido per la predisposizione del documento
	 * 
	 * Per essere un dsr valido alla protocollazione deve rispettare le seguenti
	 * condizioni:
	 * 
	 * Almeno due campi firma Il primo campo firma valorizzato
	 * 
	 * @param byteArray
	 * @return true se il content è valido
	 */
	boolean validateAdobeProtocollazioneDsr(byte[] byteArray);

	/**
	 * Recupera tipologiaDocumento e idTipoProcedimento per il dsr in base
	 * all'idCategoriaDocumento e l'idAoo dell'utente
	 * 
	 * idCategoriaDocumento, idTipologiaDocuemnto e idTipoProcedimento sono settati
	 * in detailDocument
	 * 
	 * @param detailDocument
	 * @param idCategoriaDocumento
	 * @param utente
	 * @param conn
	 */
	void inizializzaTipoProcedimentoAndDocumento(DetailDocumentRedDTO detailDocument, Integer idCategoriaDocumento,
			UtenteDTO utente, Connection conn);
}
