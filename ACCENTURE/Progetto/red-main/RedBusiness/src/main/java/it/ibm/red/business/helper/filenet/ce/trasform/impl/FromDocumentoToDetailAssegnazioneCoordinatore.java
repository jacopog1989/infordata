package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.DetailAssegnazioneDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;

/**
 * Trasformer dal Documento al Dettaglio Assegnazione Coordinatore.
 */
public class FromDocumentoToDetailAssegnazioneCoordinatore extends FromDocumentoToDetailAssegnazione {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -4271889600235483916L;

	/**
	 * Costruttore.
	 */
	public FromDocumentoToDetailAssegnazioneCoordinatore() {
		super(TrasformerCEEnum.FROM_DOCUMENT_TO_DETAIL_ASSEGNAZIONE_COORDINATORE);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.FromDocumentoToDetailAssegnazione#addFurtherInformation(com.filenet.api.core.Document,
	 *      it.ibm.red.business.dto.DetailAssegnazioneDTO).
	 */
	@Override
	protected void addFurtherInformation(final Document document, final DetailAssegnazioneDTO assegnazione) {
		final Integer coordinatoreIdUfficio = (Integer) getMetadato(document, PropertiesNameEnum.ID_UFFICIO_COORDINATORE_METAKEY);
		final Integer coordinatoreIdUtente = (Integer) getMetadato(document, PropertiesNameEnum.ID_UTENTE_COORDINATORE_METAKEY);

		assegnazione.setCoordinatore(coordinatoreIdUfficio != null ? coordinatoreIdUfficio.toString() : null,
				coordinatoreIdUtente != null ? coordinatoreIdUtente.toString() : null);
	}

}
