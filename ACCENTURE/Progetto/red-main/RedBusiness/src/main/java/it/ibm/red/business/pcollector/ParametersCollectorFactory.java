package it.ibm.red.business.pcollector;

import it.ibm.red.business.logger.REDLogger;

/**
 * Classe factory per la creazione di ParametersCollector.
 */
public final class ParametersCollectorFactory {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ParametersCollectorFactory.class.getName());

	private ParametersCollectorFactory() {
	}

	/**
	 * Restituisce una classe Parameters Collector sulla base del .class e il full qualified name in input.
	 * @param fqn - **.**.package.nomeclasse
	 * @param cls - bytecode
	 * @return instanza di ParametersCollector, se cls != null; {@link DummyParametersCollector}, altrimenti.
	 */
	public static ParametersCollector getParametersCollector(final String fqn, final byte[] cls) {
		ParametersCollector out = null;
		if (cls == null) {
			out = new DummyParametersCollector();
		} else {
			final ClassLoader loader = new ByteClassLoader(ParametersCollector.class.getClassLoader(), fqn, cls);
			try {
				@SuppressWarnings("unchecked")
				final
				Class<ParametersCollector> c = (Class<ParametersCollector>) loader.loadClass(fqn);
				out = c.newInstance();
			} catch (final Exception e) {
				LOGGER.error(e);
			}
		}
		return out;
	}
}
