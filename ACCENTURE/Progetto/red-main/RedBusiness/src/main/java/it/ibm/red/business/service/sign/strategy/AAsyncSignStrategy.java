package it.ibm.red.business.service.sign.strategy;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.stereotype.Service;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Questo service gestisce tutte le funzionalita strettamente collegate alla strategia di firma A. @see SignStrategyEnum.
 * La strategia B differisce dalla strategia A in quanto la protocollazione avviene in maniera sincrona.
 */
@Service
public class AAsyncSignStrategy extends AbstractSignStrategy {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 6808049712198578108L;
	
	/**
	 * Esegue le operazioni precedenti alla firma dei contents identificati dai
	 * wobnumbers: <code> wobNumbersToProcess </code>.
	 * 
	 * @param signTransactionId
	 *            transactionId dell'operazione di firma
	 * @param wobNumbersToProcess
	 *            identificativi documenti da firmare
	 * @param utenteFirmatario
	 *            utente responsabile della firma
	 * @return collection degli esiti associati alle firme
	 */
	@Override
	protected final Collection<EsitoOperazioneDTO> operazioniPreFirmaContents(final String signTransactionId, final Collection<String> wobNumbers, final UtenteDTO utenteFirmatario) {
		Collection<EsitoOperazioneDTO> signOutcomes = new ArrayList<>();
		Collection<EsitoOperazioneDTO> stepOutcomes;
		Collection<String> wobNumbersToProcess = wobNumbers;
		
		// Questa strategia esegue in modalità sincrona la protocollazione, la gestione della registrazione ausiliaria e la firma dei content,
		// quindi la parte asincrona inizierà dallo step di stampigliatura
		
		// ### PROTOCOLLAZIONE
		stepOutcomes = signSRV.protocollazione(signTransactionId, wobNumbersToProcess, utenteFirmatario);
		
		// ### REGISTRAZIONE AUSILIARIA
		wobNumbersToProcess = getWobNumbersForNextStep(signOutcomes, stepOutcomes);
		stepOutcomes = signSRV.registrazioneAusiliaria(signTransactionId, wobNumbersToProcess, utenteFirmatario);
		
		signOutcomes.addAll(stepOutcomes);
		return signOutcomes;
	}
	
}
