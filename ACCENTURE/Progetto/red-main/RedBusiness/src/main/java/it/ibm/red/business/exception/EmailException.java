package it.ibm.red.business.exception;

/**
 * The Class RedException.
 *
 * @author CPIERASC
 * 
 *         Eccezione generica dell'applicativo.
 */
public class EmailException extends RuntimeException {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Costruttore.
	 * 
	 * @param e	eccezione
	 */
	public EmailException(final Exception e) {
		super(e);
	}

	/**
	 * Costruttore.
	 * 
	 * @param msg	messaggio
	 */
	public EmailException(final String msg) {
		super(msg);
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param msg	messaggio
	 * @param e		eccezione root
	 */
	public EmailException(final String msg, final Exception e) {
		super(msg, e);
	}
	
}
