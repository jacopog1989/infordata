package it.ibm.red.business.service.facade;

import java.io.InputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DocumentoAllegabileDTO;
import it.ibm.red.business.dto.DocumentoFascicoloDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.MasterFascicoloDTO;
import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;

/**
 * The Interface IFascicoloFacadeSRV.
 *
 * @author CPIERASC
 * 
 *         Facade fascicolo service.
 */
/**
 * @author m.crescentini
 *
 */
public interface IFascicoloFacadeSRV extends Serializable {

	/**
	 * Ottiene i documenti del fascicolo.
	 * 
	 * @param idfascicolo
	 * @param utente
	 * @return documenti del fascicolo
	 */
	Collection<DocumentoFascicoloDTO> getDocumentiFascicolo(Integer idfascicolo, UtenteDTO utente);

	/**
	 * Solo documenti RED (si escludono il "documento fascicolo" e i contributi).
	 * 
	 * @param idfascicolo
	 * @param utente
	 * @return
	 */
	Collection<DocumentoFascicoloDTO> getOnlyDocumentiRedFascicolo(Integer idfascicolo, UtenteDTO utente);

	/**
	 * Ottiene i documenti tramite l'id del fascicolo padre.
	 * 
	 * @param idfascicolo
	 * @param utente
	 * @return lista dei dettagli dei documenti
	 */
	List<DetailDocumentRedDTO> getDocumentiByIdFascicoloPadre(Integer idfascicolo, UtenteDTO utente);

	/**
	 * Ottiene i fascicoli non classificati.
	 * 
	 * @param utente
	 * @return fascicoli master
	 */
	Collection<MasterFascicoloDTO> getFascicoliNonClassificati(UtenteDTO utente);

	/**
	 * Conta i fascicoli non classificati.
	 * 
	 * @param utente
	 * @return numero di fascicoli non classificati
	 */
	Integer getCountFascicoliNonClassificati(UtenteDTO utente);

	/**
	 * Recupero il fascicolo documentale avendo solo il document§Title del
	 * documento.
	 * 
	 * @param documentTitle
	 * @param idAoo
	 * @param utente
	 * @return
	 */
	FascicoloDTO getFascicoloProcedimentale(String documentTitle, Integer idAoo, UtenteDTO utente);

	/**
	 * @param idFascicolo
	 * @param idAoo
	 * @return
	 */
	String getIndiceClassificazione(String idFascicolo, long idAoo);

	/**
	 * @param idFascicolo
	 * @param idAoo
	 * @return
	 */
	TitolarioDTO getIndiceClassificazioneByIdFascicolo(String idFascicolo, Long idAoo);

	/**
	 * @param idDocumento
	 * @param idFascicolo
	 * @param utente
	 * @return
	 */
	boolean documentoGiaPresenteInFascicolo(String idDocumento, String idFascicolo, UtenteDTO utente);

	/**
	 * @param idDocumento
	 * @param newFascicoloId
	 * @param currentFascicoloId
	 * @param idAoo
	 * @param utente
	 * @param idProtocollo
	 * @param indiceClassificazioneFascicoloProcedimentale
	 * @param oggetto
	 * @param tipoDocumento
	 * @return
	 */
	boolean spostaDocumento(String idDocumento, String newFascicoloId, String currentFascicoloId, Long idAoo, UtenteDTO utente, String idProtocollo,
			String indiceClassificazioneFascicoloProcedimentale);

	/**
	 * @param idDocumento
	 * @param newFascicoloId
	 * @param currentFascicoloId
	 * @param idAoo
	 * @param utente
	 * @return
	 */
	boolean copiaDocumento(String idDocumento, String newFascicoloId, String currentFascicoloId, Long idAoo, UtenteDTO utente);

	/**
	 * @param nomeFascicolo
	 * @param utente
	 * @return
	 */
	FascicoloDTO getFascicoloByNomeFascicolo(String nomeFascicolo, UtenteDTO utente);

	/**
	 * Serve a recuperare la lista dei fascicoli dato il percorso assoluto del
	 * faldone che li contiene.
	 * 
	 * IMPORTANTE: la connessione viene fatta con l'utente passatogli e non con
	 * l'utente admin (a differenza di FaldoneSRV.getFascicoliFaldoneDS(...))
	 * 
	 * @param faldoneAbsolutePath
	 * @param utente
	 * @return
	 */
	List<FascicoloDTO> getFascicoliByFaldoneAbsolutePath(String faldoneAbsolutePath, UtenteDTO utente);

	/**
	 * Restituisce i fascicoli dato l'indice di classificazione.
	 * 
	 * @param utente
	 * @param indiceDiClassificazione
	 * @return
	 */
	Collection<MasterFascicoloDTO> getFascicoliByIndiceDiClassificazione(UtenteDTO utente, String indiceDiClassificazione);

	/**
	 * Apre un fascicolo chiuso.
	 * 
	 * @param idFascicolo
	 * @param idAoo
	 * @param utente
	 */
	void apriFascicolo(String idFascicolo, Long idAoo, UtenteDTO utente);

	/**
	 * Restituisce il contenuto del fascicolo sotto forma di ZipInputStream
	 * 
	 * @param idFascicolo
	 * @param utente
	 * @return
	 */
	InputStream getZipFascicoloContents(String idFascicolo, UtenteDTO utente);

	/**
	 * Recupero il fascicolo documentale avendo a partire dal wobnumber del
	 * documento.
	 * 
	 * @param wobNumber
	 * @param utente
	 * @return
	 */
	FascicoloDTO getFascicoloProcedimentale(String wobNumber, UtenteDTO utente);

	/**
	 * Recupera i documenti del fascicolo in input
	 * 
	 * @param wobNumber   il wobnumber relativo al documento principale da mettere
	 *                    in testa alla lista dei documenti
	 * @param utente
	 * @param fascicolo
	 * @param withContent
	 * @return
	 */
	List<DocumentoAllegabileDTO> getDocumentiAllegabili(String wobNumber, UtenteDTO utente, FascicoloDTO fascicolo, boolean withContent);

	/**
	 * Recupera i documenti dei fascicoli procedimentali relativi ai documenti in
	 * input, escludendo fra i content idDocumentoToExclude e valorizzando come
	 * selected i content in allegatiDaAllacciToSelect
	 * 
	 * @param utente
	 * @param idDocs
	 * @param idDocumentoToExclude
	 * @param allegatiDaAllacciToSelect
	 * @param withContent
	 * @return
	 */
	Map<FascicoloDTO, List<DocumentoAllegabileDTO>> getDocumentiAllegabiliMap(UtenteDTO utente, List<Integer> idDocs, Integer idDocumentoToExclude,
			List<AllegatoDTO> allegatiDaAllacciToSelect, boolean withContent);

	/**
	 * Carichi da Filenet i content selezionati
	 * 
	 * @param utente
	 * @param allegatiSelezionati
	 */
	void loadContent4DocumentiAllegabili(UtenteDTO utente, List<DocumentoAllegabileDTO> allegatiSelezionati);

	/**
	 * @param utente
	 * @param idFascicolo
	 * @param indiceClassificazione
	 * @param indiceClassificazioneDescrizione
	 */
	void associaATitolarioAggiornaNPS(UtenteDTO utente, String idFascicolo, String indiceClassificazione, String indiceClassificazioneDescrizione);

	/**
	 * @param idFascicolo
	 * @param indiceClassificazione
	 * @param idAoo
	 * @param fceh
	 * @param con
	 */
	void associaATitolario(String idFascicolo, String indiceClassificazione, Long idAoo, IFilenetCEHelper fceh, Connection con);

	/**
	 * @param utente
	 * @param idFascicolo
	 * @param indiceClassificazione
	 */
	void associaATitolario(UtenteDTO utente, String idFascicolo, String indiceClassificazione);

	/**
	 * 
	 * @param utente
	 * @param numProt
	 * @param annoProt
	 * @return
	 */
	Map<FascicoloDTO, List<DocumentoAllegabileDTO>> getDocsProtRiferimento(UtenteDTO utente, Integer numProt, Integer annoProt, boolean isForIntegrazioneDati);

	/**
	 * Chiude il fascicolo.
	 * 
	 * @param idFascicolo
	 * @param idAoo
	 * @param utente
	 */
	void chiudiFascicolo(String idFascicolo, Long idAoo, UtenteDTO utente);

	/**
	 * Restituisce true se il fascicolo identificato da <code> idfascicolo </code> è associato ad un flusso.
	 * @param idfascicolo
	 * @param utente
	 * @return true se il fascicolo è di un flusso, false altrimenti
	 */
	boolean verificaFascicoloFlusso(String idfascicolo, UtenteDTO utente);

}