package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dto.EventoSottoscrizioneDTO;
import it.ibm.red.business.dto.TimerRichiesteIntegrazioniDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.service.INotificaMessaInLavorazioneAutomaticaSRV;
import it.ibm.red.business.service.ISottoscrizioniSRV;

/**
 * Service notifica messa in lavorazione automatica.
 */
@Service
@Component
public class NotificaMessaInLavorazioneAutomaticaSRV extends NotificaWriteAbstractSRV implements INotificaMessaInLavorazioneAutomaticaSRV {
	
	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -3579005827950582917L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NotificaMessaInLavorazioneAutomaticaSRV.class.getName());

	/**
	 * Service.
	 */
	@Autowired
	private ISottoscrizioniSRV sottoscrizioniSRV;

	/**
	 * @see it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV#getSottoscrizioni(int).
	 */
	@Override
	protected List<EventoSottoscrizioneDTO> getSottoscrizioni(final int idAoo) {
		Connection con = null;
		List<EventoSottoscrizioneDTO> eventi = null;
		
		try {
			final Integer[] idEventi = new Integer[]{ Integer.parseInt(EventTypeEnum.SPOSTATO_IN_LAVORAZIONE_AUTOMATICO.getValue()) };
		
			con = setupConnection(getFilenetDataSource().getConnection(), false);
			eventi = sottoscrizioniSRV.getEventiSottoscrittiByListaEventi(idAoo, idEventi, con);			
		
		} catch (final Exception e) {
			LOGGER.error("Errore in fase recupero delle sottoscrizioni per l'evento per l'aoo " + idAoo, e);
		} finally {
			closeConnection(con);
		}
		
		return eventi;
	}

	/**
	 * @see it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV#getDataDocumenti(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.persistence.model.Aoo, int).
	 */
	@Override
	protected List<InfoDocumenti> getDataDocumenti(final UtenteDTO utente, final Aoo aoo, final int idEvento) {
		throw new RedException("Metodo non previsto per il servizio ");
	}

	/**
	 * @see it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV#getDataDocumenti(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.persistence.model.Aoo, int, java.lang.Object).
	 */
	@Override
	protected List<InfoDocumenti> getDataDocumenti(final UtenteDTO utente, final Aoo aoo, final int idEvento, final Object objectForGetDataDocument) {
 		
		final List<InfoDocumenti> documenti = new ArrayList<>();
 		try {
 			
 			final TimerRichiesteIntegrazioniDTO timer = (TimerRichiesteIntegrazioniDTO) objectForGetDataDocument;
			
 			if (timer.getIdUtente() == utente.getId().longValue() && timer.getIdNodo() == utente.getIdUfficio().longValue()) {
	 			final InfoDocumenti info = new InfoDocumenti();
	 			info.setIdDocumento(Integer.valueOf((int) timer.getIdDocumento()));
	 			documenti.add(info);
 			}
 		} catch (final Exception e) {
			LOGGER.error("Errore in fase recupero info timer", e);
		}
		return documenti;
	}

}
