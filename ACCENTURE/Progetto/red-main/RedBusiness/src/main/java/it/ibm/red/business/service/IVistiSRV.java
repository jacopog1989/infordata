/**
 * 
 */
package it.ibm.red.business.service;

import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.service.facade.IVistiFacadeSRV;

/**
 * The Interface IVistiSRV.
 * 
 * @author adilegge
 * 
 * 			Interfaccia del servizio di gestione dei visti.
 *
 */
public interface IVistiSRV extends IVistiFacadeSRV {

	/**
	 * Metodo per lo storno dal giro visti per id documento.
	 * @param documentoId
	 * @param fpeh
	 */
	void stornaDalGiroVistiByIdDocumento(Integer documentoId, FilenetPEHelper fpeh, final UtenteDTO utente);
}