package it.ibm.red.business.dto;

import java.util.Date;
import java.util.List;

import it.ibm.red.business.enums.MailOperazioniEnum;
import it.ibm.red.business.enums.ProtocollaMailStatiEnum;

/**
 * The Class InoltroRifiutoCodaDTO.
 *
 * @author adilegge
 * 
 */
public class ProtocollaMailCodaDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7587457073538520671L;
	
	/**
	 * Identificativo coda
	 */
	private Long idCoda;
	
	/**
	 * Guid mail
	 */
	private String guidMail;
	
	/**
	 * Identificativo aoo
	 */
	private Long idAoo;
	
	/**
	 * Operazione mail
	 */
	private MailOperazioniEnum operazione;
	
	/**
	 * identificativo utente operazione
	 */
	private Long idUtenteOperazione;
	
	/**
	 * Identificativo ruolo operazione
	 */
	private Long idRuoloOperazione;
	
	/**
	 * Identificativo nodo operazione
	 */
	private Long idNodoOperazione;
	
	/**
	 *  Identificativo mittente
	 */
	private Long idContattoMittente;
	
	/**
	 *  Testo template
	 */
	private String testoTemplate;
	
	/**
	 * Flag mantieni allegati originali 
	 */
	private boolean mantieniAllegatiOriginali;
	
	/**
	 * Casella mail
	 */
	private String casella;
	
	/**
	 * Stato protocollazione
	 */
	private ProtocollaMailStatiEnum stato;
	
	/**
	 * Descrizione errore
	 */
	private String descrizioneErrore;
	
	/**
	 * Data ultima operazione
	 */
	private Date dataUltimaOperazione;
	
	/**
	 * Identificativo documento entrata
	 */
	private String idDocumentoEntrata;
	
	/**
	 * Wob number entrata
	 */
	private String wobNumberEntrata;
	
	/**
	 * Identificativo documento uscita
	 */
	private String idDocumentoUscita;
	
	/**
	 * Wob number uscita
	 */
	private String wobNumberUscita;
	
	/**
	 * Contatti destinatari
	 */
	private List<DestinatarioEsternoDTO> contattiDestinatari;
	
	/**
	 * Costruttore protocolla mail coda DTO.
	 *
	 * @param idCoda the id coda
	 * @param guidMail the guid mail
	 * @param idAoo the id aoo
	 * @param operazione the operazione
	 * @param idUtenteOperazione the id utente operazione
	 * @param idRuoloOperazione the id ruolo operazione
	 * @param idNodoOperazione the id nodo operazione
	 * @param idContattoMittente the id contatto mittente
	 * @param testoTemplate the testo template
	 * @param mantieniAllegatiOriginali the mantieni allegati originali
	 * @param casella the casella
	 * @param stato the stato
	 * @param descrizioneErrore the descrizione errore
	 * @param dataUltimaOperazione the data ultima operazione
	 * @param idDocumentoEntrata the id documento entrata
	 * @param wobNumberEntrata the wob number entrata
	 * @param idDocumentoUscita the id documento uscita
	 * @param wobNumberUscita the wob number uscita
	 */
	public ProtocollaMailCodaDTO(final Long idCoda, final String guidMail, final Long idAoo, final MailOperazioniEnum operazione, final Long idUtenteOperazione, final Long idRuoloOperazione, final Long idNodoOperazione, 
			final Long idContattoMittente, final String testoTemplate, final boolean mantieniAllegatiOriginali, final String casella, final ProtocollaMailStatiEnum stato, final String descrizioneErrore, 
			final Date dataUltimaOperazione, final String idDocumentoEntrata, final String wobNumberEntrata, final String idDocumentoUscita, final String wobNumberUscita) {
		super();
		this.idCoda = idCoda;
		this.guidMail = guidMail;
		this.idAoo = idAoo;
		this.operazione = operazione;
		this.idUtenteOperazione = idUtenteOperazione;
		this.idRuoloOperazione = idRuoloOperazione;
		this.idNodoOperazione = idNodoOperazione;
		this.idContattoMittente = idContattoMittente;
		this.testoTemplate = testoTemplate;
		this.mantieniAllegatiOriginali = mantieniAllegatiOriginali;
		this.casella = casella;
		this.stato = stato;
		this.descrizioneErrore = descrizioneErrore;
		this.dataUltimaOperazione = dataUltimaOperazione;
		this.idDocumentoEntrata = idDocumentoEntrata;
		this.wobNumberEntrata = wobNumberEntrata;
		this.idDocumentoUscita = idDocumentoUscita;
		this.wobNumberUscita = wobNumberUscita;
	}
	
	/**
	 * Costruttore protocolla mail coda DTO.
	 *
	 * @param guidMail the guid mail
	 * @param idAoo the id aoo
	 * @param operazione the operazione
	 * @param idUtenteOperazione the id utente operazione
	 * @param idRuoloOperazione the id ruolo operazione
	 * @param idNodoOperazione the id nodo operazione
	 * @param idContattoMittente the id contatto mittente
	 * @param testoTemplate the testo template
	 * @param mantieniAllegatiOriginali the mantieni allegati originali
	 * @param casella the casella
	 * @param stato the stato
	 * @param contattiDestinatari the contatti destinatari
	 */
	public ProtocollaMailCodaDTO(final String guidMail, final Long idAoo, final MailOperazioniEnum operazione, final Long idUtenteOperazione, final Long idRuoloOperazione, final Long idNodoOperazione, 
			final Long idContattoMittente, final String testoTemplate, final boolean mantieniAllegatiOriginali, final String casella, final ProtocollaMailStatiEnum stato, 
			final List<DestinatarioEsternoDTO> contattiDestinatari) {
		super();
		this.guidMail = guidMail;
		this.idAoo = idAoo;
		this.operazione = operazione;
		this.idUtenteOperazione = idUtenteOperazione;
		this.idRuoloOperazione = idRuoloOperazione;
		this.idNodoOperazione = idNodoOperazione;
		this.idContattoMittente = idContattoMittente;
		this.testoTemplate = testoTemplate;
		this.mantieniAllegatiOriginali = mantieniAllegatiOriginali;
		this.casella = casella;
		this.stato = stato;
		this.contattiDestinatari = contattiDestinatari;
	}

	/** 
	 * @return the id coda
	 */
	public Long getIdCoda() {
		return idCoda;
	}

	/** 
	 * @param idCoda the new id coda
	 */
	public void setIdCoda(final Long idCoda) {
		this.idCoda = idCoda;
	}

	/** 
	 * @return the guid mail
	 */
	public String getGuidMail() {
		return guidMail;
	}

	/** 
	 * @param guidMail the new guid mail
	 */
	public void setGuidMail(final String guidMail) {
		this.guidMail = guidMail;
	}

	/** 
	 * @return the id aoo
	 */
	public Long getIdAoo() {
		return idAoo;
	}

	/** 
	 * @param idAoo the new id aoo
	 */
	public void setIdAoo(final Long idAoo) {
		this.idAoo = idAoo;
	}

	/** 
	 * @return the operazione
	 */
	public MailOperazioniEnum getOperazione() {
		return operazione;
	}

	/** 
	 * @param operazione the new operazione
	 */
	public void setOperazione(final MailOperazioniEnum operazione) {
		this.operazione = operazione;
	}
	
	/** 
	 * @return the id utente operazione
	 */
	public Long getIdUtenteOperazione() {
		return idUtenteOperazione;
	}

	/** 
	 * @param idUtenteOperazione the new id utente operazione
	 */
	public void setIdUtenteOperazione(final Long idUtenteOperazione) {
		this.idUtenteOperazione = idUtenteOperazione;
	}
	
	/** 
	 * @return the id ruolo operazione
	 */
	public Long getIdRuoloOperazione() {
		return idRuoloOperazione;
	}

	/** 
	 * @param idRuoloOperazione the new id ruolo operazione
	 */
	public void setIdRuoloOperazione(final Long idRuoloOperazione) {
		this.idRuoloOperazione = idRuoloOperazione;
	}

	/** 
	 * @return the id nodo operazione
	 */
	public Long getIdNodoOperazione() {
		return idNodoOperazione;
	}

	/** 
	 * @param idNodoOperazione the new id nodo operazione
	 */
	public void setIdNodoOperazione(final Long idNodoOperazione) {
		this.idNodoOperazione = idNodoOperazione;
	}

	/** 
	 * @return the id contatto mittente
	 */
	public Long getIdContattoMittente() {
		return idContattoMittente;
	}

	/** 
	 * @param idContattoMittente the new id contatto mittente
	 */
	public void setIdContattoMittente(final Long idContattoMittente) {
		this.idContattoMittente = idContattoMittente;
	}

	/** 
	 * @return the testo template
	 */
	public String getTestoTemplate() {
		return testoTemplate;
	}

	/** 
	 * @param testoTemplate the new testo template
	 */
	public void setTestoTemplate(final String testoTemplate) {
		this.testoTemplate = testoTemplate;
	}
	
	/** 
	 * @return true, if is mantieni allegati originali
	 */
	public boolean isMantieniAllegatiOriginali() {
		return mantieniAllegatiOriginali;
	}

	/** 
	 * @param mantieniAllegatiOriginali the new mantieni allegati originali
	 */
	public void setMantieniAllegatiOriginali(final boolean mantieniAllegatiOriginali) {
		this.mantieniAllegatiOriginali = mantieniAllegatiOriginali;
	}
	
	/** 
	 * @return the casella
	 */
	public String getCasella() {
		return casella;
	}

	/** 
	 * @param casella the new casella
	 */
	public void setCasella(final String casella) {
		this.casella = casella;
	}

	/** 
	 * @return the stato
	 */
	public ProtocollaMailStatiEnum getStato() {
		return stato;
	}

	/** 
	 * @param stato the new stato
	 */
	public void setStato(final ProtocollaMailStatiEnum stato) {
		this.stato = stato;
	}
	
	/** 
	 * @return the descrizione errore
	 */
	public String getDescrizioneErrore() {
		return descrizioneErrore;
	}

	/** 
	 * @param descrizioneErrore the new descrizione errore
	 */
	public void setDescrizioneErrore(final String descrizioneErrore) {
		this.descrizioneErrore = descrizioneErrore;
	}

	/** 
	 * @return the data ultima operazione
	 */
	public Date getDataUltimaOperazione() {
		return dataUltimaOperazione;
	}

	/** 
	 * @param dataUltimaOperazione the new data ultima operazione
	 */
	public void setDataUltimaOperazione(final Date dataUltimaOperazione) {
		this.dataUltimaOperazione = dataUltimaOperazione;
	}

	/** 
	 * @return the id documento entrata
	 */
	public String getIdDocumentoEntrata() {
		return idDocumentoEntrata;
	}

	/** 
	 * @param idDocumentoEntrata the new id documento entrata
	 */
	public void setIdDocumentoEntrata(final String idDocumentoEntrata) {
		this.idDocumentoEntrata = idDocumentoEntrata;
	}

	/** 
	 * @return the wob number entrata
	 */
	public String getWobNumberEntrata() {
		return wobNumberEntrata;
	}

	/** 
	 * @param wobNumberEntrata the new wob number entrata
	 */
	public void setWobNumberEntrata(final String wobNumberEntrata) {
		this.wobNumberEntrata = wobNumberEntrata;
	}

	/** 
	 * @return the id documento uscita
	 */
	public String getIdDocumentoUscita() {
		return idDocumentoUscita;
	}

	/** 
	 * @param idDocumentoUscita the new id documento uscita
	 */
	public void setIdDocumentoUscita(final String idDocumentoUscita) {
		this.idDocumentoUscita = idDocumentoUscita;
	}

	/** 
	 * @return the wob number uscita
	 */
	public String getWobNumberUscita() {
		return wobNumberUscita;
	}

	/** 
	 * @param wobNumberUscita the new wob number uscita
	 */
	public void setWobNumberUscita(final String wobNumberUscita) {
		this.wobNumberUscita = wobNumberUscita;
	}
	
	/** 
	 * @return the contatti destinatari
	 */
	public List<DestinatarioEsternoDTO> getContattiDestinatari() {
		return contattiDestinatari;
	}

	/** 
	 * @param contattiDestinatari the new contatti destinatari
	 */
	public void setContattiDestinatari(final List<DestinatarioEsternoDTO> contattiDestinatari) {
		this.contattiDestinatari = contattiDestinatari;
	}
	
}