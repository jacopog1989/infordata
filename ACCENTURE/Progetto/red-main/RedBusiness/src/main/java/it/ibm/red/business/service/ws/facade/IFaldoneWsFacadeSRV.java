package it.ibm.red.business.service.ws.facade;

import java.io.Serializable;

import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.webservice.model.documentservice.types.messages.RedAssociaFascicoloFaldoneType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedCreaFaldoneType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedEliminaFaldoneType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedRimuoviAssociazioneFascicoloFaldoneType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedSpostaFascicoloFaldoneType;

/**
 * The Interface IFaldoneWsFacadeSRV.
 *
 * @author a.dilegge
 * 
 *         Facade faldone service da web service.
 */
public interface IFaldoneWsFacadeSRV extends Serializable {

	/**
	 * Elimina il faldone.
	 * @param client
	 * @param request
	 */
	void redEliminaFaldone(RedWsClient client, RedEliminaFaldoneType request);

	/**
	 * Crea il faldone.
	 * @param client
	 * @param request
	 * @return
	 */
	String redCreaFaldone(RedWsClient client, RedCreaFaldoneType request);

	/**
	 * Associa il fascicolo al faldone.
	 * @param client
	 * @param request
	 */
	void redAssociaFascicoloFaldone(RedWsClient client, RedAssociaFascicoloFaldoneType request);

	/**
	 * Rimuove l'associazione del fascicolo al faldone.
	 * @param client
	 * @param request
	 */
	void redRimuoviAssociazioneFascicoloFaldone(RedWsClient client, RedRimuoviAssociazioneFascicoloFaldoneType request);

	/**
	 * Sposta il fascicolo di faldone.
	 * @param client
	 * @param request
	 */
	void redSpostaFascicoloFaldone(RedWsClient client, RedSpostaFascicoloFaldoneType request);

}