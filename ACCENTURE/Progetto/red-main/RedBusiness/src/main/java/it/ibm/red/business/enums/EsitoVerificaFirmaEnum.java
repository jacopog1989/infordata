package it.ibm.red.business.enums;

/**
 * Enum che definisce l'esito della verifica firma.
 */
public enum EsitoVerificaFirmaEnum {
	
	/**
	 * Ok.
	 */
	OK,
	/**
	 * in questo caso PK ha sollevato un'eccezione generica, salvata in <code>genericException</code>.
	 */
	ERRORE_GENERIC,

	/**
	 * Errore conversione.
	 */
	ERRORE_CONVERSIONE_CONTENT,

	/**
	 * Errore connessione PK.
	 */
	ERRORE_CONNESSIONE_PK,
	
	/**
	 * in questo caso PK ha sollevato <code>PKBoxException</code> ed è popolato <code>errorCodePK</code>.
	 */
	ERRORE_GENERICO_PK,
	
	/**
	 * info restituita da PK.
	 */
	ERRORE_NO_SIGNATURE,

	/**
	 * Errore generico envelope.
	 */
	ERRORE_ENVELOPE,
	/**
	 * nessun oggetto restituito da PK o numero di Signer = 0.
	 */
	ERRORE_NO_SIGNER,

	/**
	 * Errore generico compatibilità.
	 */
	ERRORE_GLOBAL_COMPLIANCE;
}