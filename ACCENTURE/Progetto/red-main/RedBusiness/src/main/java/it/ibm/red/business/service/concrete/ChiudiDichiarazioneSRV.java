package it.ibm.red.business.service.concrete;

import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.ChiudiDichiarazioneDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IChiudiDichiarazioneSRV;

/**
 * Service chiusura dichiarazione.
 */
@Service
public class ChiudiDichiarazioneSRV extends AbstractService implements IChiudiDichiarazioneSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -5901571540569112676L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ChiudiDichiarazioneSRV.class.getName());
	
	/**
	 * @see it.ibm.red.business.service.facade.IChiudiDichiarazioneFacadeSRV#chiudiDichiarazione(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public EsitoOperazioneDTO chiudiDichiarazione(final String wobNumber, final UtenteDTO utente) {
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		Integer numeroDocumento = null;
		
		try {
			PropertiesProvider pp = PropertiesProvider.getIstance();
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			
			// recuperare il workflow
			VWWorkObject workObject = fpeh.getWorkFlowByWob(wobNumber, true);
			Integer idDocumento = (Integer) TrasformerPE.getMetadato(workObject, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
			numeroDocumento = fceh.getNumDocByDocumentTitle(idDocumento.toString());
			
			// Si avanza il workflow con la response "Chiudi Senza Associazione"
			fpeh.nextStep(workObject, null, ResponsesRedEnum.CHIUDI_SENZA_ASSOCIAZIONE.getResponse());
			
			esito.setEsito(true);
			esito.setIdDocumento(numeroDocumento);
			esito.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
		} catch (Exception e) {
			LOGGER.error("chiudiDichiarazione -> Si è verificato un errore: avanzamento del workflow non riuscito.", e);
			esito.setNote("Attenzione: avanzamento del workflow non riuscito.");
			esito.setIdDocumento(numeroDocumento);
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}
		
		return esito;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IChiudiDichiarazioneFacadeSRV#getMetadati(it.ibm.red.business.dto.FilenetCredentialsDTO,
	 *      java.lang.String, long).
	 */
	@Override
	public ChiudiDichiarazioneDTO getMetadati(final FilenetCredentialsDTO fnDTO, final String documentTitle, final long idAOO) {
		ChiudiDichiarazioneDTO result = new ChiudiDichiarazioneDTO();
		IFilenetCEHelper fceh = null;
		
		try {
			fceh = FilenetCEHelperProxy.newInstance(fnDTO, idAOO);
			
			Document d = fceh.getDocumentByDTandAOO(documentTitle, idAOO);
			
			result.setNomeFile(d.getProperties().getStringValue("nomeFile"));
			result.setDateCreated(d.get_DateCreated());
		} catch (Exception e) {
			LOGGER.error(e);
		} finally {
			popSubject(fceh);
		}
		
		return result;
	}
}