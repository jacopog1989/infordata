package it.ibm.red.business.enums;

/**
 * Enum errori validazione.
 */
public enum SeverityValidationPluginEnum {
	
	/**
	 * Valore.
	 */
	WARNING,
	
	/**
	 * Valore.
	 */
	ERROR,

	/**
	 * Valore.
	 */
	NONE;
}
