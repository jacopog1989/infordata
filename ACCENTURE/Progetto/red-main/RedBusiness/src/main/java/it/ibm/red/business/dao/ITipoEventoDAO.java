package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.persistence.model.TipoEvento;

/**
 * 
 * @author CPIERASC
 *
 *	Interfaccia dao per la gestione dei tipi evento.
 */
public interface ITipoEventoDAO extends Serializable {
	
	/**
	 * Recupero tipo evento per id.
	 * 
	 * @param id			identificativo tipo evento
	 * @param connection	connessione al db
	 * @return				tipo evento
	 */
	TipoEvento getById(int id, Connection connection);
	
	/**
	 * Recupera la combo per la ricerca avanzata red, tipo operazioni
	 * @param idAoo
	 * @param connection
	 * @return
	 */
	List<TipoEvento> getAllEntitaDocumento(Long idAoo, Connection connection);

}
