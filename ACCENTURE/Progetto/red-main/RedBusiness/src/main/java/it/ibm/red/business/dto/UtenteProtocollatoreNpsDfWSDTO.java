package it.ibm.red.business.dto;

/**
 * DTO che definisce un utente protocollatore NPS.
 */
public class UtenteProtocollatoreNpsDfWSDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -6367873647195914153L;


    /**
     * Codice amministrazione.
     */
	private String codiceAmministrazione;
	
    /**
     * Denominazione amministrazione.
     */
	private String denominazioneAmministrazione;
	
    /**
     * Aoo.
     */
	private String codAoo;
	
    /**
     * Denominazione aoo.
     */
	private String denominazioneAoo;
	
    /**
     * Codice uo.
     */
	private String codUO;
	
    /**
     * Denominazione uo.
     */
	private String denominazioneUO;
	
    /**
     * Nome.
     */
	private String nome;
	
    /**
     * Cognome.
     */
	private String cognome;
	
    /**
     * Chiave esterna operatore.
     */
	private String chiaveEsternaOperatore;
	
	/**
	 * @return the codiceAmministrazione
	 */
	public String getCodiceAmministrazione() {
		return codiceAmministrazione;
	}
	/**
	 * @param codiceAmministrazione the codiceAmministrazione to set
	 */
	public void setCodiceAmministrazione(final String codiceAmministrazione) {
		this.codiceAmministrazione = codiceAmministrazione;
	}
	/**
	 * @return the denominazioneAmministrazione
	 */
	public String getDenominazioneAmministrazione() {
		return denominazioneAmministrazione;
	}
	/**
	 * @param denominazioneAmministrazione the denominazioneAmministrazione to set
	 */
	public void setDenominazioneAmministrazione(final String denominazioneAmministrazione) {
		this.denominazioneAmministrazione = denominazioneAmministrazione;
	}
	 
	/**
	 * @return the codAoo
	 */
	public String getCodAoo() {
		return codAoo;
	}
	/**
	 * @param codAoo the codAoo to set
	 */
	public void setCodAoo(final String codAoo) {
		this.codAoo = codAoo;
	}
	/**
	 * @return the denominazioneAoo
	 */
	public String getDenominazioneAoo() {
		return denominazioneAoo;
	}
	/**
	 * @param denominazioneAoo the denominazioneAoo to set
	 */
	public void setDenominazioneAoo(final String denominazioneAoo) {
		this.denominazioneAoo = denominazioneAoo;
	}
	/**
	 * @return the codUO
	 */
	public String getCodUO() {
		return codUO;
	}
	/**
	 * @param codUO the codUO to set
	 */
	public void setCodUO(final String codUO) {
		this.codUO = codUO;
	}
	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}
	/**
	 * @param nome the nome to set
	 */
	public void setNome(final String nome) {
		this.nome = nome;
	}
	/**
	 * @return the cognome
	 */
	public String getCognome() {
		return cognome;
	}
	/**
	 * @param cognome the cognome to set
	 */
	public void setCognome(final String cognome) {
		this.cognome = cognome;
	}
	/**
	 * @return the chiaveEsternaOperatore
	 */
	public String getChiaveEsternaOperatore() {
		return chiaveEsternaOperatore;
	}
	/**
	 * @param chiaveEsternaOperatore the chiaveEsternaOperatore to set
	 */
	public void setChiaveEsternaOperatore(final String chiaveEsternaOperatore) {
		this.chiaveEsternaOperatore = chiaveEsternaOperatore;
	}

	/**
	 * Restituisce la denominazione UO.
	 * @return denominazione UO
	 */
	public String getDenominazioneUO() {
		return denominazioneUO;
	}
	
	/**
	 * Imposta la denominazione UO.
	 * @param denominazioneUO
	 */
	public void setDenominazioneUO(final String denominazioneUO) {
		this.denominazioneUO = denominazioneUO;
	}
	
}
