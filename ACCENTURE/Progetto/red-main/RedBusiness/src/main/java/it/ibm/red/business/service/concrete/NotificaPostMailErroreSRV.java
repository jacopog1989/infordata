package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dto.EventoSottoscrizioneDTO;
import it.ibm.red.business.dto.MessaggioEmailDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.service.ISottoscrizioniSRV;
import it.ibm.red.business.service.IStoricoSRV;

/**
 * Service notifica post mail errore.
 */
@Service
public class NotificaPostMailErroreSRV extends NotificaWriteAbstractSRV implements INotificaPostMailErroreSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 5072215933806523575L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NotificaPostMailErroreSRV.class.getName());

	/**
	 * Service.
	 */
	@Autowired
	private ISottoscrizioniSRV sottoscrizioniSRV;
	
	/**
	 * Service.
	 */
	@Autowired
	private IStoricoSRV storicoSRV;
	
	/**
	 * @see it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV#getSottoscrizioni(int).
	 */
	@Override
	protected List<EventoSottoscrizioneDTO> getSottoscrizioni(final int idAoo) {
	
		Connection con = null;
		List<EventoSottoscrizioneDTO> eventi = null;
		try {
			
			final Integer[] idEventi = new Integer[]{ Integer.parseInt(EventTypeEnum.ERRORE_SPEDIZIONE_MAIL.getValue()) };
		
			con = setupConnection(getFilenetDataSource().getConnection(), false);
			eventi = sottoscrizioniSRV.getEventiSottoscrittiByListaEventi(idAoo, idEventi, con);			
		
		} catch (final Exception e) {
		
			LOGGER.error("Errore in fase recupero delle sottoscrizioni per gli eventi EVENTTYPE_MODIFICATO_DOPO_VISTO-EVENTTYPE_MODIFICATO_DOPO_SIGLA per l'aoo " + idAoo, e);
		
		} finally {
			closeConnection(con);
		}
		
		return eventi;
	}
	
	/**
	 * @see it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV#getDataDocumenti(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.persistence.model.Aoo, int, java.lang.Object).
	 */
 	@Override
 	protected List<InfoDocumenti> getDataDocumenti(final UtenteDTO utente, final Aoo aoo, final int idEvento, final Object objectForGetDataDocument) {
 		
 		final List<InfoDocumenti> documenti = new ArrayList<>();
 		
  		
 		try {
 			
 			final MessaggioEmailDTO messaggio = (MessaggioEmailDTO)objectForGetDataDocument;
 			
 			final boolean inviaNotifica = storicoSRV.isDocLavoratoDaUfficio(utente.getIdAoo(), messaggio.getIdDocumento(), utente.getIdUfficio());
 			if (inviaNotifica) {
	 			final InfoDocumenti info = new InfoDocumenti();
	 			info.setIdDocumento(Integer.parseInt(messaggio.getIdDocumento()));
	 			documenti.add(info);
 			}
 			
 		} catch (final Exception e) {
			LOGGER.error("Errore in fase recupero dei documenti siglati e vistati dall'utente/ufficio (" + utente.getId() + "," + utente.getIdUfficio() + ") l'aoo " + aoo.getIdAoo(), e);
		
		} 
		
		return documenti;
		
 	}
 	
 	/**
	 * @see it.ibm.red.business.service.concrete.NotificaWriteAbstractSRV#getDataDocumenti(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.persistence.model.Aoo, int).
	 */
 	@Override
	protected List<InfoDocumenti> getDataDocumenti(final UtenteDTO utente, final Aoo aoo, final int idEvento) {
		throw new RedException("Metodo non previsto per il servizio ");
	}
	
}