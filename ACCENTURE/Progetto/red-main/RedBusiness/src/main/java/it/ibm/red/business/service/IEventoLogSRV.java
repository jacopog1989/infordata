package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.service.facade.IEventoLogFacadeSRV;

/**
 * The Interface IEventoLogSRV.
 *
 * @author mcrescentini
 * 
 *         Interfaccia servizio per il tracciamento degli eventi (EventoLog).
 */
public interface IEventoLogSRV extends IEventoLogFacadeSRV {
	
	/**
	 * Inserisce l'evento.
	 * @param idDocumento
	 * @param idUfficioMittente
	 * @param idUtenteMittente
	 * @param idUfficioDestinatario
	 * @param idUtenteDestinatario
	 * @param dataAssegnazione
	 * @param eventType
	 * @param motivazioneAssegnazione
	 * @param idTipoAssegnazione
	 * @param idAoo
	 * @param connection
	 */
	void inserisciEventoLog(int idDocumento, Long idUfficioMittente, Long idUtenteMittente, Long idUfficioDestinatario, Long idUtenteDestinatario, 
		Date dataAssegnazione, EventTypeEnum eventType, String motivazioneAssegnazione, int idTipoAssegnazione, Long idAoo, Connection connection);

	
	/**
	 * Recupera la mappa doc title,id ufficio creatore dalla deventicustom.
	 * @param documentTitle
	 * @param idAoo
	 * @return idufficioCreatore
	 */ 
	HashMap<String, Integer> getIdUfficioCreatore(List<String> documentTitle, Long idAoo);
	
}