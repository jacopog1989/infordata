package it.ibm.red.business.helper.filenet.pe.trasform.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import filenet.vw.api.VWException;
import filenet.vw.api.VWQueueQuery;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.exception.FilenetException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;

/**
 * The Class TrasformerPE.
 *
 * @author CPIERASC
 * 
 *         Oggetto generico per trasformazione di un elemento del PE.
 * @param <T>
 *            Tipo generico che rappresenta l'elemento destinazione della
 *            trasformazione (la sorgente sarà di tipo VWWorkObject).
 */
public abstract class TrasformerPE<T> implements Serializable {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TrasformerPE.class.getName());
	
	/**
	 * Enum che identifica la trasformazione da eseguire.
	 */
	private final TrasformerPEEnum enumKey;

	/**
	 * Costruttore.
	 * 
	 * @param inEnumKey	trasformazione da eseguire
	 */
	protected TrasformerPE(final TrasformerPEEnum inEnumKey) {
		enumKey = inEnumKey;
	}

	/**
	 * Metodo per la trasformazione di un VWWorkObject in un oggetto target.
	 * 
	 * @param object	VWWorkObject da trasformare
	 * @return			oggetto trasformato
	 */
	public abstract T trasform(VWWorkObject object);
	
	/**
	 * Metodo per la trasformazione di un una collezione di VWWorkObject in una collezione di oggetti destinazione.
	 * 	
	 * @param query	VWQueueQuery che rappresenta l'insieme dei VWWorkObject da trasformare
	 * @return		l'insieme degli oggetti trasformati
	 */
	public final Collection<T> trasform(final VWQueueQuery query) {
		return trasform(query, null);
	}

	/**
	 * Esegue la logica di trasformazione.
	 * @param query
	 * @param cap
	 * @return oggetti trasformati.
	 */
	public final Collection<T> trasform(final VWQueueQuery query, final Integer cap) {
		try {
			final Collection<T> output = new ArrayList<>();
			Integer index = 0;
			while (query.hasNext()) {
				final VWWorkObject wo = (VWWorkObject) query.next();
				output.add(trasform(wo));
				index++;
				if ((cap != null && cap > 0) && (index >= cap)) {
					break;
				}
			}
			return output;
		} catch (final Exception e) {
			LOGGER.error("Errore durante la trasformazione del WF: ", e);
			throw new FilenetException(e);
		}
	}

	/**
	 * Metodo per recuperare da un VWWorkObject uno specifico campo sotto forma di stringa.
	 * 
	 * @param object		oggetto sorgente
	 * @param fieldName		campo
	 * @return				valore del campo nell'oggetto
	 * @throws VWException	eccezione generata in fase di estrazione del campo
	 */
	protected final String getDataFieldValue(final VWWorkObject object, final String fieldName) {
		return object.getDataField(fieldName).getStringValue();
	}

	/**
	 * Metodo per recuperare da un VWWorkObject uno specifico campo sotto forma
	 * di oggetto.
	 *
	 * @param object
	 *            oggetto sorgente
	 * @param nomeMetadato
	 *            campo
	 * @return valore del campo nell'oggetto
	 */
	public static final Object getMetadato(final VWWorkObject object, final String nomeMetadato) {
		Object output = null;
		try {
			output = object.getFieldValue(nomeMetadato);
		} catch (final VWException e) {
			LOGGER.info("[PE]" + e);
			LOGGER.info("[PE] PROPRIETA' '" + nomeMetadato + "' NON TROVATA");
		}
		return output;
	}
	
	/**
	 * Restituisce il metadato con VWException.
	 * @param object
	 * @param nomeMetadato
	 * @return metadato con VWException
	 * @throws VWException
	 */
	public static final Object getMetadatoWithVWException(final VWWorkObject object, final String nomeMetadato) throws VWException {
		return object.getFieldValue(nomeMetadato);
	}
	
	/**
	 * Metodo per il recupero di un metadato sottoforma di oggetto.
	 * 
	 * @param document		documento sorgente
	 * @param propertyEnum	enum della proprietà da estrapolare
	 * @return				prorpietà sottoforma di oggetto
	 */
	public static final Object getMetadato(final VWWorkObject object, final PropertiesNameEnum propertyEnum) {
		return getMetadato(object, PropertiesProvider.getIstance().getParameterByKey(propertyEnum));
	}
	
	/**
	 * Getter.
	 * 
	 * @return	enum
	 */
	public final TrasformerPEEnum getEnumKey() {
		return enumKey;
	}
}
