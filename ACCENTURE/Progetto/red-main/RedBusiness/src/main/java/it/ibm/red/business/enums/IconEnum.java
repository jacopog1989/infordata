package it.ibm.red.business.enums;

/**
 * The Enum IconEnum.
 *
 * @author CPIERASC
 * 
 *         Enum delle icone utilizzabili nei messaggi all'interno di un pdf.
 */
public enum IconEnum {

	/**
	 * Commento.
	 */
	COMMENT("Comment"), 

	/**
	 * Chiave.
	 */
	KEY("Key"), 

	/**
	 * Nota.
	 */
	NOTE("Note"), 

	/**
	 * Help.
	 */
	HELP("Help"), 

	/**
	 * Nuovo paragrafo.
	 */
	NEWPARAGRAPH("NewParagraph"), 

	/**
	 * Paragrafo.
	 */
	PARAGRAPH("Paragraph"), 

	/**
	 * Insert.
	 */
	INSERT("Insert");

	/**
	 * Nome dell'icona.
	 */
	private String iconName; 

	/**
	 * Costruttore.
	 * 
	 * @param inIconName	descrizione icona
	 */
	IconEnum(final String inIconName) {
		iconName = inIconName;
	}

	/**
	 * Getter descrizione icona.
	 * 
	 * @return	descrizione icona
	 */
	public String getIconName() {
		return iconName;
	}

}
