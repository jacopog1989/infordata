package it.ibm.red.business.constants;

/**
 * The Class Constants.
 *
 * @author CPIERASC
 * 
 * 	Classe delle costanti.
 */
public final class Constants {
	
	/**
	 * EMPTY_STRING
	 */
	public static final String EMPTY_STRING = "";
	
	/**
	 * ACCESS_EXTERNAL_DTD.
	 */
	public static final String ACCESS_EXTERNAL_DTD = "http://javax.xml.XMLConstants/property/accessExternalDTD";
	
	/**
	 * ACCESS_EXTERNAL_SCHEMA.
	 */
	public static final String ACCESS_EXTERNAL_SCHEMA = "http://javax.xml.XMLConstants/property/accessExternalSchema";
	
	/**
	 * ACCESS_EXTERNAL_STYLESHEET.
	 */
	public static final String ACCESS_EXTERNAL_STYLESHEET = "http://javax.xml.XMLConstants/property/accessExternalStylesheet";
	
	/**
	 * Classe per Aoo.
	 * 
	 * @author APerquoti
	 *
	 */
	public static final class Aoo {
		
		/**
		 * Visibilità Totale.
		 */
		public static final String VIS_FALDONI_TOTALE = "T";
		
		/**
		 * Visibilità Ristretta.
		 */
		public static final String VIS_FALDONI_RISTRETTA = "R";

		private Aoo() {
			
		}

	}
	
	/**
	 * Costanti sign servlet.
	 */
	public static final class SignServlet {		
		/**
		 * Nome del parametro che specifica la action da eseguire.
		 */
		public static final String ACTION = "action";

		/**
		 * Nome del parametro che specifica la pagina in cui è presente l'applet.
		 */
		public static final String SOURCE = "source";

		/**
		 * Nome del parametro che specifica a quale delegato inviare la get/posts.
		 */
		public static final String DELEGATE = "delegate";

		/**
		 * Nome del parametro che specifica l'identificativo del documento da firmare.
		 */
		public static final String DOC_ID_SIGNING = "docIDSigning";

		/**
		 * Costruttore.
		 */
		private SignServlet() {
			
		}
	}

	/**
	 * Classe per storico.
	 * 
	 * @author CRISTIANOPierascenzi
	 *
	 */
	public static final class Storico {
		
		/**
		 * Start DATE in Long.
		 */
		public static final Long START_DATE = 1484312400000L;

		private Storico() {
			
		}
	}
	
	/**
	 * The Class BooleanFlag.
	 *
	 * @author cpaoluzi
	 * 
	 * 	Constanti booleane Integer.
	 */
	public static final class BooleanFlag {

		/**
		 * FLAG FALSE.
		 */
		public static final int FALSE = 0;
		/**
		 * FLAG TRUE.
		 */
		public static final int TRUE = 1;

		/**
		 * Costruttore.
		 */
		protected BooleanFlag() {
		}
	}

	/**
	 * Costanti error code.
	 */
	public static final class ErrorCode {
		
		/**
		 * Success.
		 */
		public static final int SUCCESS = 0;
		/**
		 * Errore generico RED_WS.
		 */
		public static final int GENERIC_RED_WS_ERROR = 10000;
		/**
		 * Errore: NO_DATA_INTO_REQUEST_ERROR.
		 */
		public static final int NO_DATA_INTO_REQUEST_ERROR = 10001;
		/**
		 * Errore: METADATA_IS_NOT_VALUED_PROPERLY_ERROR.
		 */
		public static final int METADATA_IS_NOT_VALUED_PROPERLY_ERROR = 10002;
		/**
		 * Errore: FEPA_FASCICLE_VALIDATION_ERROR.
		 */
		public static final int FEPA_FASCICLE_VALIDATION_ERROR = 10003;
		/**
		 * Errore: DOES_NOT_EXISTS_ANY_ANNULABLE_DD_WORKFLOW_ERROR.
		 */
		public static final int DOES_NOT_EXISTS_ANY_ANNULABLE_DD_WORKFLOW_ERROR = 10004;
		/**
		 * Errore: DD_WORKFLOW_NOT_ANNULLABLE_ERROR.
		 */
		public static final int DD_WORKFLOW_NOT_ANNULLABLE_ERROR = 10005;
		/**
		 * Errore: FATTURA_ELETTRONICA_NOT_EXISTS_ERROR.
		 */
		public static final int FATTURA_ELETTRONICA_NOT_EXISTS_ERROR = 10006;
		/**
		 * Errore: FASCICOLO_FEPA_NOT_FOUND_ERROR.
		 */
		public static final int FASCICOLO_FEPA_NOT_FOUND_ERROR = 10007;
		/**
		 * Errore: GENERIC_FWS_ERROR.
		 */
		public static final int GENERIC_FWS_ERROR = 10008;
		/**
		 * Errore: DOCUMENT_NOT_ASSIGNED.
		 */
		public static final int DOCUMENT_NOT_ASSIGNED = 10009;
		/**
		 * Errore: IDFASCICOLOFEPA_ASSOCIATO_AD_ALTRO_UTENTE.
		 */
		public static final int IDFASCICOLOFEPA_ASSOCIATO_AD_ALTRO_UTENTE = 100010;
		/**
		 * Errore: IDFASCICOLO_FEPA_GIA_ASSEGNATO.
		 */
		public static final int IDFASCICOLO_FEPA_GIA_ASSEGNATO = 100011;
		/**
		 * Errore: FATTURA_GIA_PRESENTE.
		 */
		public static final int FATTURA_GIA_PRESENTE = 100012;
		/**
		 * Errore: ID_FASCICOLO_FEPA_NON_PRESENTE.
		 */
		public static final int ID_FASCICOLO_FEPA_NON_PRESENTE = 100013;
		/**
		 * Errore: UTENTE_CEDENTE_O_DESTINATARIO_NON_DEFINITI.
		 */
		public static final int UTENTE_CEDENTE_O_DESTINATARIO_NON_DEFINITI = 100014;
		/**
		 * Errore: RIASSEGNA_FATTURA_GENERIC_ERROR.
		 */
		public static final int RIASSEGNA_FATTURA_GENERIC_ERROR = 100015;
		/**
		 * Errore: UFFICIO_MITTENTE_DESTINATARIO_DIFFERENTI.
		 */
		public static final int UFFICIO_MITTENTE_DESTINATARIO_DIFFERENTI = 100016;
		/**
		 * Errore: FATTURA_GIA_LAVORATA.
		 */
		public static final int FATTURA_GIA_LAVORATA = 100017;
		/**
		 * Errore: FATTURA_GIA_ASSOCIATA_AD_UN_DECRETO.
		 */
		public static final int FATTURA_GIA_ASSOCIATA_AD_UN_DECRETO = 100018;
		/**
		 * Errore: FATTURA_NON_PRESENTE_SU_RED.
		 */
		public static final int FATTURA_NON_PRESENTE_SU_RED = 100019;
		/**
		 * Errore: ID_DECRETO_DIRIGENZIALE_NON_PRESENTE.
		 */
		public static final int ID_DECRETO_DIRIGENZIALE_NON_PRESENTE = 100020;
		/**
		 * Errore: NESSUNA_FATTURA_DA_AGGIUNGERE_O_RIMUOVERE_DAL_DD.
		 */
		public static final int NESSUNA_FATTURA_DA_AGGIUNGERE_O_RIMUOVERE_DAL_DD = 100021;
		/**
		 * Errore: CREDENZIALI_UTENTE_NON_DEFINITE.
		 */
		public static final int CREDENZIALI_UTENTE_NON_DEFINITE = 100022;
		/**
		 * Errore: ID_GRUPPO_NON_DEFINITO.
		 */
		public static final int ID_GRUPPO_NON_DEFINITO = 100023;
		/**
		 * Errore: ID_UTENTE_NON_DEFINITO.
		 */
		public static final int ID_UTENTE_NON_DEFINITO = 100024;
		/**
		 * Errore: DD_NON_TROVATO.
		 */
		public static final int DD_NON_TROVATO = 100025;
		/**
		 * Errore: DD_GIA_ASSOCIATO_AD_UN_DSR.
		 */
		public static final int DD_GIA_ASSOCIATO_AD_UN_DSR = 100026;
		/**
		 * Errore: DD_NON_CONTENENTE_TUTTE_LE_FATTURE_DA_RIMUOVERE.
		 */
		public static final int DD_NON_CONTENENTE_TUTTE_LE_FATTURE_DA_RIMUOVERE = 100027;
		/**
		 * Errore: FATTURA_NON_TROVATA.
		 */
		public static final int FATTURA_NON_TROVATA = 100028;
		/**
		 * Errore: FATTURA_GIA_ASSOCIATA_ALL_UTENTE.
		 */
		public static final int FATTURA_GIA_ASSOCIATA_ALL_UTENTE = 100029;
		/**
		 * Errore: GENERIC_FILENET_PE_ERROR.
		 */
		public static final int GENERIC_FILENET_PE_ERROR = 100030;
		/**
		 * Errore: NESSUNA_FATTURA_PRESENTE_NEL_FASCICOLO.
		 */
		public static final int NESSUNA_FATTURA_PRESENTE_NEL_FASCICOLO = 100031;
		/**
		 * Errore: GENERIC_FILENET_CE_ERROR.
		 */
		public static final int GENERIC_FILENET_CE_ERROR = 100032;
		/**
		 * Errore: MODIFICA_FASCICOLO_NO_OPERATION.
		 */
		public static final int MODIFICA_FASCICOLO_NO_OPERATION = 100033;
		/**
		 * Errore: DECRETO_DIRIGENZIALE_SENZA_DOCUMENTO_ALLEGATO.
		 */
		public static final int DECRETO_DIRIGENZIALE_SENZA_DOCUMENTO_ALLEGATO = 100034;
		
		private ErrorCode() {
			
		}
	}
	
	/**
	 * The Class Adobe.
	 *
	 * @author CPIERASC
	 * 
	 * 	Constanti Adobe.
	 */
	public static final class Adobe {

		/**
		 * Buffer size.
		 */
		public static final int ADOBE_BUFFER_SIZE = 2048;
		
		/**
		 * Nome del processo di inserimento Stampigliatura - FWS.
		 */
		public static final String PROCESS_NAME_INSERT_STAMPIGLIATURA_FWS = "FWS/insertStampigliaturaFws";
		
		/**
		 * Nome del processo per la predisposizione alla conversione.
		 */
		public static final String PROCESS_NAME_PREDISPOSIZIONE_DOCUMENTONSD = "WFP2/predisposizioneDocumentoNsd";
		
		/**
		 * Nome del processo per il conteggio dei campi firma.
		 */
		public static final String PROCESS_NAME_COUNT_SIGNED_FIELDS = "WFP10/countsSignedFields";
		
		/**
		 * Nome del processo per la predisposizione alla firma.
		 */
		public static final String PROCESS_NAME_PREDISPOSIZIONE_FIRMA = "WFP2/predisposizioneFirmaDigitale";
		
		/**
		 * Nome del processo inserimento postilla.
		 */
		public static final String PROCESS_NAME_POSTILLA = "FWS/insertPostillaFws";
		
		/**
		 * Nome del processo inserimento postilla copia conforme.
		 */
		public static final String PROCESS_NAME_POSTILLA_COPIA_CONFORME = "WFP2/insertStampigliaturaFws";
		
		/**
		 * Properties per invocare il processo di stampa approvazioni.
		 */
		public static final String PROCESS_NAME_STAMPA_APPROVAZIONI_DEFAULT = "WFP2/approvazioni/GeneratePdfApprovazioni";
		
		/**
		 * Properties per invocare il processo di conversione in PDF/A.
		 */
		public static final String PROCESS_NAME_CONVERT_PDF_A = "FWS/convertToPDFFws";
		
		/**
		 * Properties per invocare il processo di inserimento di un campo firma.
		 */
		public static final String PROCESS_NAME_INSERISCI_CAMPO_FIRMA = "FWS/prepareForSignFws";
		
		/**
		 * Nome del processo per la ricerca dei tag firmatari.
		 */
		public static final String FIND_TAG_PROCESS_NAME = "WFP/findTag";
		
		/**
		 * CONVERSIONE_OUTDOC.
		 */
		public static final String CONVERSIONE_OUTDOC = "outDoc";
		
		/**
		 * CONVERSIONE_PREVIEWDOC.
		 */
		public static final String CONVERSIONE_PREVIEWDOC = "previewDoc";
		
		/**
		 * NUM_SIGN_FIELDS.
		 */
		public static final String NUM_SIGN_FIELDS = "numSignFields";
		
		/**
		 * NUM_SIGN.
		 */
		public static final String NUM_SIGN = "numSignatures";
		
		/**
		 * NUM_EMPTY_SIGN_FIELDS.
		 */
		public static final String NUM_EMPTY_SIGN_FIELDS = "numEmptySignFields";
		
		/**
		 * TAG_FIRMATARIO.
		 */
		public static final String TAG_FIRMATARIO = "Firmatario1";
		
		/**
		 * OUTPDF_TAG_PRESENT.
		 */
		public static final String OUTPDF_TAG_PRESENT = "pageTag";
		
		/**
		 * Costruttore.
		 */
		protected Adobe() {
		}
	}
	
	/**
	 * The Class Firma.
	 *
	 * @author CPIERASC
	 * 
	 * 	Costanti usate in fase di firma.
	 */
	public static final class Firma {
		
		/**
		 * Algoritmo utilizzato in fase di firma.
		 */
		public static final int ALGORITHM_SHA_256 = 5;
		
		/**
		 * Timebox otp (è di 5').
		 */
		public static final int TOKEN_EXP_WARN_TIME = 300000; //300000 = 5'
		
		/**
		 * Testo postilla standard.
		 */
		public static final String TESTO_POSTILLA_STANDARD = 
				"La riproduzione su supporto cartaceo del seguente documento costituisce una copia del documento firmato digitalmente " 
						+ "e conservato presso il MEF ai sensi della normativa vigente";
		
		/**
		 * Testo postilla copia conforme.
		 */
		public static final String TESTO_POSTILLA_COPIA_CONFORME = "Firmato digitalmente per copia conforme all'originale analogico esistente "
				+ "presso gli uffici del Ministero dell'Economia e delle Finanze.";

		/**
		 * Costruttore.
		 */
		protected Firma() {
		}
	}
	
	/**
	 * The Class Protocollo.
	 *
	 * @author CPIERASC
	 * 
	 * 	Costanti protocollo.
	 */
	public static final class Protocollo {

		/**
		 * Tipo protocollo non specificato.
		 */
		public static final int TIPO_PROTOCOLLO_NESSUNO = 0;
		
		/**
		 * Protocollo entrata.
		 */
		public static final int TIPO_PROTOCOLLO_ENTRATA = 1;
		
		/**
		 * Protocollo uscita.
		 */
		public static final int TIPO_PROTOCOLLO_USCITA = 2;
		
		/**
		 * Protocollo Ingresso utilizzato da FEPA nella creazione della raccolta FAD.
		 */
		public static final String TIPOLOGIA_PROTOCOLLO_INGRESSO = "INGRESSO";
		
		/**
		 * Protocollo Uscita utilizzato da FEPA nella creazione della raccolta FAD.
		 */
		public static final String TIPOLOGIA_PROTOCOLLO_USCITA = "USCITA";
		
		/**
		 * Acronimo Protocolla Emergenza.
		 */
		public static final String PROTOCOLLO_EMERGENZA_ACRONIMO = "RE";
		
		/**
		 * Acronimo Protocolla Repertorio.
		 */
		public static final String PROTOCOLLO_REPERTORIO_ACRONIMO = "RR";
		
		/**
		 * Costruttore.
		 */
		protected Protocollo() {
		}
	}
	
	/**
	 * The Class Mail.
	 *
	 * @author CPIERASC
	 * 
	 * 	Costanti gestione mail.
	 */
	public static final class Mail {
		
		/**
		 * The Constant WORK_PARZIALE_MAIL_POSITION.
		 */
		public static final int WORK_PARZIALE_MAIL_POSITION = 3;
		
		/**
		 * The Constant CC_TO_MAIL_POSITION.
		 */
		public static final int CC_TO_MAIL_POSITION = 4;
		
		/**
		 * The Constant IE_MAIL_POSITION.
		 */
		public static final int IE_MAIL_POSITION = 3;
		
		/**
		 * The Constant MEZZO_SPEDIZIONE_POSITION (solo destinatari esterni).
		 */
		public static final int MEZZO_SPEDIZIONE_POSITION = 2;
		
		/**
		 * The Constant TESTO_MAIL_HTML.
		 */
		public static final String TESTO_MAIL_HTML = "TestoMail.html";		
		
		/**
		 * Costruttore.
		 */
		protected Mail() {
		}
	}
	
	/**
	 * The Class StatoRichiesta.
	 *
	 * @author CPIERASC
	 * 
	 * 	Stato della richiesta.
	 */
	public static final class StatoRichiesta {
		
		/**
		 * Identificativo rifiuto.
		 */
		public static final Long ID_RIFIUTATA = 13L;

		/**
		 * Identificativo evasione.
		 */
		public static final Long ID_EVASA = 14L;
		
		/**
		 * Costruttore.
		 */
		protected StatoRichiesta() {
		}
	}
	
	/**
	 * The Class TipoSpedizione.
	 *
	 * @author CPIERASC
	 * 
	 * 	Tipo spedizione.
	 */
	public static final class TipoSpedizione {
		
		/**
		 * Identificativo mezzo non specificato.
		 */
		public static final Integer MEZZO_NON_SPECIFICATO = 0;

		/**
		 * Identificativo mezzo cartaceo.
		 */
		public static final Integer MEZZO_CARTACEO = 1;

		/**
		 * Identificativo mezzo elettronico.
		 */
		public static final Integer MEZZO_ELETTRONICO = 2;
		
		/**
		 * Costruttore.
		 */
		protected TipoSpedizione() {
		}
	}
	
	/**
	 * The Class TipoDestinatario.
	 *
	 * @author CPIERASC
	 * 
	 * 	Tipologia destinatario (interno/esterno).
	 */
	public static final class TipoDestinatario {
		
		/**
		 * Destinatario interno.
		 */
		public static final String INTERNO = "I";

		/**
		 * Destinatario esterno.
		 */
		public static final String ESTERNO = "E";

		/**
		 * Costruttore.
		 */
		protected TipoDestinatario() {
		}
	}
	

	/**
	 * The Class Notifiche.
	 *
	 * @author CPIERASC
	 * 
	 * 	Notifiche.
	 */
	public static final class Notifiche {
		/**
		 * Chiusura fascicolo.
		 */
		public static final int CHIUSURA_FASCICOLO = 1026;

		/**
		 * Riattivazione fascicolo.
		 */
		public static final int RIATTIVAZIONE_FASCICOLO = 1027;

		/**
		 * Associa fascicolo.
		 */
		public static final int ASSOCIA_FASCICOLO = 1028;

		/**
		 * Disassocia fascicolo.
		 */
		public static final int DISASSOCIA_FASCICOLO = 1029;

		/**
		 * Errore invio documento firma.
		 */
		public static final int ERRORE_INVIO_DOCUMENTO_FIRMA = 1030;

		/**
		 * Errore invio mail.
		 */
		public static final int ERRORE_INVIO_MAIL = 1031;

		/**
		 * Errore consegna mail.
		 */
		public static final int ERRORE_CONSEGNA_MAIL = 1032;

		/**
		 * Protocollazione mail da batch.
		 */
		public static final int PROTOCOLLAZIONE_MAIL_DA_BATCH = 1042;

		/**
		 * Avviso di non accettazione.
		 */
		public static final int STATO_RICEVUTA_NOTIFICA_EMAIL_CHIUSURA = 3;
		
		/**
		 * Avviso di non accettazione.
		 */
		public static final int STATO_RICEVUTA_NOTIFICA_EMAIL_ATTESA_SPEDIZIONE_UTENTE = 8;
		
		/**
		 * Stato ricevuta notifica email, in attesa di spedizione NPS.
		 */
		public static final int STATO_RICEVUTA_NOTIFICA_EMAIL_ATTESA_SPEDIZIONE_NPS = 9;
		
		/**
		 * Avviso di non accettazione.
		 */
		public static final int TIPOLOGIA_DESTINATARI_MITTENTI_CARTACEI = 0;
		
		/**
		 * Avviso di non accettazione.
		 */
		public static final int TIPOLOGIA_DESTINATARI_MITTENTI_MISTI = 2;
		
		/**
		 * Stato notifica email ricevuta spedita.
		 */
		public static final int STATO_RICEVUTA_NOTIFICA_EMAIL_SPEDITO = 7;
		
		/**
		 * Stato notifica email ricevuta reinviata.
		 */
		public static final int STATO_RICEVUTA_NOTIFICA_EMAIL_REINVIO = 5;
		
		/**
		 * Stato notifica email ricevuta errore.
		 */
		public static final int STATO_RICEVUTA_NOTIFICA_EMAIL_ERRORE = 4;

		/**
		 * stato ricevuta notifica email inizio.
		 */
		public static final int STATO_RICEVUTA_NOTIFICA_EMAIL_INIZIO = 0;
		/**
		 * stato ricevuta notifica email iter spedizione da trasformare pdf.
		 */
		public static final int STATO_RICEVUTA_NOTIFICA_EMAIL_ITER_SPEDIZIONE_DA_TRASFORMARE_PDF = -1;
		/**
		 * stato ricevuta notifica email attesa accettazione.
		 */
		public static final int STATO_RICEVUTA_NOTIFICA_EMAIL_ATTESA_ACCETTAZIONE = 1;
		/**
		 * stato ricevuta notifica email attesa avvenuta consegna.
		 */
		public static final int STATO_RICEVUTA_NOTIFICA_EMAIL_ATTESA_AVVENUTA_CONSEGNA = 2;
		/**
		 * stato ricevuta notifica email elaborazione.
		 */
		public static final int STATO_RICEVUTA_NOTIFICA_EMAIL_ELABORAZIONE = 6;

		
		/**
		 * Costruttore.
		 */
		protected Notifiche() {
		}
		
	}


	/**
	 * 	
	 * @author CPIERASC
	 *
	 *	Messaggi.
	 */
	public static final class Messaggi {
		
		/**
		 * Errore ricerca.
		 */
		public static final String ERRORE_RICERCA = "Si è verificato un errore durante la ricerca: ";

		/**
		 * Costruttore.
		 */
		protected Messaggi() {
		}
		
	}
	
	
	/**
	 * The Class Varie.
	 *
	 * @author CPIERASC
	 * 
	 * 	Varie.
	 */
	public static final class Varie {

		/**
		 * Indice di split per il calcolo del tipo di spedizione su di un destinatario.
		 */
		public static final int INDICE_SPLIT_TIPO_SPEDIZIONE = 3;
		
		/**
		 * Cambio iter MEV CAVALLO.
		 */
		public static final int INDEX_BEFORE_MEV_CAVALLO = 4;		
		/**
		 * Suffisso eccezione OTP non valido.
		 */
		public static final String INVALID_OTP_SUFFIX = "Invalid OTP";
		
		/**
		 * OTO non valido.
		 */
		public static final String OTP_NON_VALIDO = "L'OTP inserito non è valido!";
		
		/**
		 * Comando aggiornaDTViewe.
		 */
		public static final String DT_VIEWER_COMMAND = "#{LibroBean.aggiornaDTViewer}";
		
		/**
		 * Logout.
		 */
		public static final String LOGOUT_TITLE = "Logout";
		
		/**
		 * Identificativo categoria assegnazione interna.
		 */
		public static final int CATEGORIA_ASSEGNAZIONE_INTERNA = 7;
		
		/**
		 * Codice errore per trasformazione PDF.
		 */
		public static final int TRASFORMAZIONE_PDF_IN_ERRORE = 1;
		
		/**
		 * Codice ok per trasformazione PDF.
		 */
		public static final int TRASFORMAZIONE_PDF_OK = 0;
		
		/**
		 * Tipo trasformazione "TRASFORMATA" per la coda trasformazione PDF.
		 */
		public static final int TRASFORMATA = 1;
		
		/**
		 * Tipo trasformazione "TRASFORMATAIMPL" per la coda trasformazione PDF.
		 */
		public static final int TRASFORMATAIMPL = 2;
		
		/**
		 * Prefisso AOO FileNet.
		 */
		public static final String AOO_PREFIX = "/AOO_";
		
		/**
		 * ELENCOTRASMISSIONEELETTRONICO.
		 */
		public static final String ELENCOTRASMISSIONEELETTRONICO = "elettronico";
		
		/**
		 * Descrizione Tipo Procedimento PRELEX.
		 */
		public static final String TIPO_PROCEDIMENTO_PRELEX = "PRELEX";
		
		/**
		 * Stato del Fascicolo utilizzato da fepa nella crazione della raccolta FAD.
		 */
		public static final String STATO_FASCICOLO_FEPA_INSERITO = "INSERITO";
		
		/**
		 * Stato del Fascicolo utilizzato da fepa nella crazione della raccolta FAD.
		 */
		public static final String STATO_FASCICOLO_FEPA_APERTO = "APERTO";
		
		/**
		 * Label Tipologia processo manuale .
		 */
		public static final String TIPOLOGIA_PROC_PROCESSO_MANUALE_ATTO_DECRETO_LABEL = "Processo manuale ATTO DECRETO";
		
		/**
		 * Label Tipologia processo automatico .
		 */
		public static final String TIPOLOGIA_PROC_PROCESSO_AUTOMATICO_ATTO_DECRETO_LABEL = "Processo automatico ATTO DECRETO";
		
		/**
		 * Tipologia Documento "Dichiarazione Servizi Resi".
		 */
		public static final String TIPOLOGIA_DOC_DICHIARAZIONE_SERVIZI_RESI = "DICHIARAZIONE SERVIZI RESI";
		
		/**
		 * Limite elementi clausula IN query SQL.
		 */
		public static final int MAXLENGTH_SQL_QUERY_IN = 999;
		
		/**
		 * Label Tipologia documento FAD principale.
		 */
		public static final String TIPO_DOCUMENTO_FAD_PRINCIPALE = "RP01";
		
		/**
		 *  Label Tipologia documento FAD allegato.
		 */
		public static final String TIPO_DOCUMENTO_FAD_ALLEGATO = "RP02";
		
		/**
		 *  Label Tipologia documento FAD allegato ReportProtocolli.
		 */
		public static final String TIPO_DOCUMENTO_FAD_REPORT_PROTOCOLLI = "RP03";
		
		/**
		 *  Label Tipologia documento BE risposta.
		 */
		public static final String TIPO_DOCUMENTO_BILANCIO_ENTI_RISPOSTA = "BE02";
		
		/**
		 *  Label Tipologia documento generico FEPA .
		 */
		public static final String TIPO_DOCUMENTO_GENERICO_FEPA = "F008";
		
		/**
		 *  Label Tipologia documento lettera servizi resi FEPA .
		 */
		public static final String TIPO_DOCUMENTO_LETTERA_SERVIZI_RESI_FEPA = "G003";
		
		/**
		 *  Label Tipologia documento documentazione allegata FEPA .
		 */
		public static final String TIPO_DOCUMENTO_DOCUMENTAZIONE_ALLEGATA_FEPA = "G002";
		
		/**
		 *  Label Tipologia documento decreto liquidazione FEPA .
		 */
		public static final String TIPO_DOCUMENTO_DECRETO_LIQUIDAZIONE_FEPA = "G001";
				
		/**
		 * Codice errore associato all'errore: Content documento già esistente.
		 */
		public static final String SKIP_FAULT_CODE_DOCUMENT_CONTENT_ALREADY_EXIST = "1058_DOCUMENT_CONTENT_ALREADY_EXIST";
		
		/**
		 * ReportProtocolli file name.
		 */
		public static final String REPORT_PROTOCOLLI_PDF_FILENAME = "ReportProtocolli.pdf";
		
		/**
		 * ReportProtocolli name.
		 */
		public static final String REPORT_PROTOCOLLI_NAME = "ReportProtocolli";
		
		/**
		 * ReportProtocolli file descr.
		 */
		public static final String REPORT_PROTOCOLLI_PDF_DESCR = "Report Protocolli";
		
		/**
		 * Segnatura ATTO DECRETO file name.
		 */
		public static final String SEGNATURA_FILENAME = "Segnatura.xml";
		/**
		 * FAD EMAIL ORIGINALE prefisso file name.
		 */
		public static final String FAD_EMAIL_ORIGINALE_PREFIX_FILENAME = "emailOriginaleProtocollo";
		/**
		 * FAD EMAIL ORIGINALE descrizione prefisso.
		 */
		public static final String FAD_EMAIL_ORIGINALE_DESCR_PREFIX = "Email Originale Protocollo ";
		/**
		 * Tipo documento FAD EMAIL ORIGINALE.
		 */
		public static final String TIPO_DOCUMENTO_FAD_EMAIL_ORIGINALE = "RP04";
		/**
		 * Contributo esterno uscita id - chiave suffisso.
		 */
		public static final String CONTRIBUTO_ESTERNO_USCITA_ID_KEY_SUFFIX = "contributoEsterno.uscita.idTipologiaDocumento";
		/**
		 * Contributo esterno entrata id - chiave suffisso.
		 */
		public static final String CONTRIBUTO_ESTERNO_ENTRATA_ID_KEY_SUFFIX = "contributoEsterno.entrata.idTipologiaDocumento";
		/**
		 * Placeholder data.
		 */
		public static final String PLACEHOLDER_DATE = "31/12/9999";
		/**
		 * Placeholder numero.
		 */
		public static final String PLACEHOLDER_NUMBER = "0";
		/**
		 * Placeholder stringa.
		 */
		public static final String PLACEHOLDER_STRING = "abc";
		/**
		 * Codice flusso ATTO DECRETO.
		 */
		public static final String CODICE_FLUSSO_ATTO_DECRETO = "ATTO_DECRETO_UCB";
		
		/**
		 * Codice flusso SIPAD.
		 */
		public static final String CODICE_FLUSSO_SIPAD = "FLUSSO_SIPAD";
		
		/**
		 * SICOGE.
		 */
		public static final String SICOGE = "SICOGE";
		
		/**
		 * ID messaggio ATTO DECRETO.
		 */
		public static final Integer ID_MESSAGGIO_ATTO_DECRETO = 2001;
		/**
		 * Codice integrazione dati.
		 */
		public static final String CODICE_INTEGRAZIONE_DATI = "INT";
		/**
		 * Descrizione integrazione dati.
		 */
		public static final String DESCRIZIONE_INTEGRAZIONE_DATI = "Integrazione Dati";
		
		/**
		 * Costruttore.
		 */
		protected Varie() {
		}
	}

	/**
	 * The Class PageComponentId.
	 *
	 * @author CPIERASC
	 * 
	 * 	Id componenti.
	 */
	public static final class PageComponentId {
		
		/**
		 * FULLSCREEN_DIALOG_DETAIL.
		 * Identificativo dialog visualizzazione detail pdf fullscreen.
		 */
		public static final String FULLSCREEN_DIALOG_DETAIL = "idFullScreenPDFDetail,idFullScreenPDFDetailForm";

		/**
		 * Identificativo dialog visualizzazione drilldown pdf fullscreen.
		 */
		public static final String FULLSCREEN_DIALOG_DRILLDOWN = "idFullScreenPDFDrilldown,idFullScreenPDFDrilldownForm";
		
		/**
		 * PANEL VIEWER SMARTPHONE ID.
		 */
		public static final String PANEL_VIEWER = "panelViewer";
		
		/**
		 * Central.
		 */
		public static final String CENTRAL = "central";
		
		/**
		 * Form center.
		 */
		public static final String FORM_CENTER = "formCenter";
		
		/**
		 * Panel form center.
		 */
		public static final String PANEL_FORM_CENTER = "formCenter:panelFormCenter";
		
		/**
		 * Form center selected label.
		 */
		public static final String FORM_CENTER_SELECTED_LABEL = "formCenter:selectedLabel";
		
		/**
		 * Central selected label.
		 */
		public static final String CENTRAL_SELECTED_LABEL = "central:selectedLabel";
		
		/**
		 * Detail panel.
		 */
		public static final String DETAIL_PANEL = "detailPanel";
		
		/**
		 * Costruttore.
		 */
		protected PageComponentId() {
		}
	}

	/**
	 * The Class Icon.
	 *
	 * @author CPIERASC
	 * 
	 * 	Icone.
	 */
	public static final class Icon {

		
		/**
		 * P7M.
		 */
		public static final String P7M = "fa fa-file";
		
		/**
		 * PDF.
		 */
		public static final String PDF = "fa fa-file-pdf-o";

		/**
		 * Folder.
		 */
		public static final String FOLDER = "fa fa-folder-open";

		/**
		 * EMPTY(cartaceo).
		 */
		public static final String EMPTY = "fa fa-inbox";

		/**
		 * Costruttore.
		 */
		protected Icon() {
		}
	
	}
	
	/**
	 * The Class RequestParameter.
	 *
	 * @author CPIERASC
	 * 
	 * 	Request parameter.
	 */
	public static final class RequestParameter {
		
		/**
		 * Data scadenza.
		 */
		public static final String DATA_SCADENZA = "dataScadenza";
		
		/**
		 * Mail link del dettaglio del documento da visualizzare sul libro firma.
		 */
		public static final String MAIL_LINK = "mailLink";

		/**
		 * Standalone value.
		 */
		public static final String STANDALONE_ON_VALUE = "on";

		/**
		 * Standalone parameter.
		 */
		public static final String STANDALONE_KEY = "standalone";
		
		/**
		 * bDrilldown.
		 */
		public static final String B_DRILLDOWN = "bDrilldown";
		
		/**
		 * Document title FILE SELECTED.
		 */
		public static final String DOCUMENT_TITLE = "documentTitle";
		
		/**
		 * Document name FILE SELECTED.
		 */
		public static final String DOCUMENT_NAME_FILE_SELECTED = "documentNameFileSelected";
		
		/**
		 * Document mimeType FILE SELECTED.
		 */
		public static final String DOCUMENT_MIMETYPE_FILE_SELECTED = "documentMimetypeFileSelected";
		
		/**
		 * Identificativo ufficio.
		 */
		public static final String ID_UFFICIO = "idUfficio";
		
		/**
		 * Identificativo ruolo.
		 */
		public static final String ID_RUOLO = "idRuolo";
		
		/**
		 * Username.
		 */
		public static final String USERNAME = "redevo_username";
		
		/**
		 * Username.
		 */
		public static final String USERNAME_MOBILE = "redmobile_username";
		
		/**
		 * Chiave dell'header per la gestione dell'handler di firma per AOO.
		 */
		public static final String FIRMA_HANDLER_HEADER_KEY = "ENV_FR";
		
		/**
		 * Chiave dell'header per la gestione della tipologia di firma per AOO.
		 */
		public static final String FIRMA_TIPO_HEADER_KEY = "DISP";
		
		/**
		 * Non utilizzato.
		 */
		public static final String HEADER_ENV_FA_KEY = "ENV_FA";
		
		

		/**
		 * Costruttore.
		 */
		protected RequestParameter() {
		}
		
		/**
		 * Costanti per il recupero dei parametri necessari per la visualizzazione del documento della mail.
		 * 
		 * @author CPAOLUZI
		 *
		 */
		public static final class MailLink {

			/**
			 * MAIL LINK REQUEST_PARAMETERS_SEPARATOR.
			 */
			public static final String MAIL_LINK_REQUEST_PARAMETERS_SEPARATOR = "&";
			/**
			 * MAIL LINK PARAMETER_SEPARATOR.
			 */
			public static final String MAIL_LINK_PARAMETER_SEPARATOR = "=";
			/**
			 * MAIL LINK PARAMETERS SIZE.
			 */
			public static final Integer MAIL_LINK_PARAMETER_SIZE = 4;
			/**
			 * MAIL LINK DOCUMENT TITLE PARAM NAME.
			 */
			public static final String MAIL_LINK_DOCUMENT_TITLE_PARAM_NAME = "documentTitle";
			/**
			 * MAIL LINK ID NODO PARAM NAME.
			 */
			public static final String MAIL_LINK_ID_NODO_NAME = "idNodo";
			/**
			 * MAIL LINK ID RUOLO PARAM NAME.
			 */
			public static final String MAIL_LINK_ID_RUOLO_PARAM_NAME = "idRuolo";
			/**
			 * MAIL LINK USERNAME PARAM NAME.
			 */
			public static final String MAIL_LINK_USERNAME_PARAM_NAME = "username";

			/**
			 * Costruttore.
			 */
			protected MailLink() {
			}
			
		} 
	}
	
	/**
	 * The Class ManagedBeanName.
	 *
	 * @author CPIERASC
	 * 
	 * 	Managed bean name.
	 */
	public static final class ManagedBeanName {
				
		/**
		 * Libro.
		 */
		public static final String LIBRO_BEAN = "LibroBean";

		/**
		 * Session.
		 */
		public static final String SESSION_BEAN = "SessionBean";
		
		/**
		 * Scheduler.
		 */
		public static final String SCHEDULER_BEAN = "SchedulerBean";
		
		/**
		 * Ricerca.
		 */
		public static final String RICERCA_BEAN = "RicercaBean";
		
		/**
		 * Firma Rapida.
		 */
		public static final String FIRMA_RAPIDA_BEAN = "FirmaRapidaBean";
		
		/**
		 * Mail.
		 */
		public static final String MAIL_BEAN = "MailBean";
		
		
		/**
		 *  DocumentManagerBean.
		 */
		public static final String DOCUMENT_MANAGER_BEAN = "DocumentManagerBean";
		
		
		/**
		 *  FascicoloManagerBean.
		 */
		public static final String FASCICOLO_FATTURA_MANAGER_BEAN = "FascicoloFatturaManagerBean";
		
		
		
		/**
		 * Costruttore.
		 */
		protected ManagedBeanName() {
		}

	}
	
	/**
	 * The Class SessionObject.
	 *
	 * @author CPIERASC
	 * 
	 * 	Oggetti di sessione.
	 */
	public static final class SessionObject {
		
		/**
		 * Document IDS.
		 */
		public static final String DOCUMENT_IDS = "DOCUMENT_IDS";
		/**
		 * Mails.
		 */
		public static final String MAILS = "MAILS";
		
		/**
		 * Costruttore.
		 */
		protected SessionObject() {
		}

	}
	
	/**
	 * The Class PEProperty.
	 *
	 * @author CPIERASC
	 * 
	 * 	Proprietà del PE.
	 */
	public static final class PEProperty {
				
		/**
		 * F_WorkObjectName.
		 */
		public static final String VW_WORK_OBJECTNAME = "F_WorkObjectName";

		/**
		 * F_Tag.
		 */
		public static final String VW_TAG = "F_Tag";

		/**
		 * F_WorkObjectNumber.
		 */
		public static final String VW_WOB_NUM = "F_WorkObjectNumber";

		/**
		 * F_WorkClass.
		 */
		public static final String VW_WORK_CLASS_NAME = "F_WorkClass";

		/**
		 * F_WPClassName.
		 */
		public static final String VW_WORK_PERFORMER_CLASS_NAME = "F_WPClassName";

		/**
		 * F_Operation.
		 */
		public static final String VW_OPERATION_NAME = "F_Operation";

		/**
		 * F_ISheetName.
		 */
		public static final String VW_INSTRUCTION_SHEET_NAME = "F_ISheetName";

		/**
		 * F_LockedUserName.
		 */
		public static final String VW_LOCKED_USER_NAME = "F_LockedUserName";

		/**
		 * F_WorkOrderId.
		 */
		public static final String VW_WORK_ORDER_ID = "F_WorkOrderId";

		/**
		 * F_CurrentQueue.
		 */
		public static final String VW_CURRENT_QUEUE = "F_CurrentQueue";

		/**
		 * F_WorkSpaceId.
		 */
		public static final String VW_WORK_SPACE_ID = "F_WorkSpaceId";

		/**
		 * F_WorkClassId.
		 */
		public static final String VW_WORK_CLASS_ID = "F_WorkClassId";

		/**
		 * F_WobNum.
		 */
		public static final String F_WOB_NUM = "F_WobNum";

		/**
		 * F_Locked.
		 */
		public static final String F_LOCKED = "F_Locked";

		/**
		 * F_ServerId.
		 */
		public static final String F_SERVER_ID = "F_ServerId";

		/**
		 * F_LockMachine.
		 */
		public static final String F_LOCKED_MACHINE_ID = "F_LockMachine";

		/**
		 * F_EnqueueTime.
		 */
		public static final String F_ENQUEUE_TIME = "F_EnqueueTime";

		/**
		 * F_Responses.
		 */
		public static final String F_RESPONSES = "F_Responses";

		/**
		 * F_LastErrorNumber.
		 */
		public static final String F_LAST_ERROR_NUMBER = "F_LastErrorNumber";

		/**
		 * F_LastErrorText.
		 */
		public static final String F_LAST_ERROR_TEXT = "F_LastErrorText";

		/**
		 * F_WsStatus.
		 */
		public static final String F_WS_STATUS = "F_WsStatus";

		/**
		 * F_Comment.
		 */
		public static final String F_COMMENT = "F_Comment";

		/**
		 * F_Response.
		 */
		public static final String F_RESPONSE = "F_Response";

		/**
		 * F_Subject.
		 */
		public static final String F_SUBJECT = "F_Subject";

		/**
		 * F_CreateTime.
		 */
		public static final String F_CREATE_TIME = "F_CreateTime";

		/**
		 * F_WorkflowName.
		 */
		public static final String F_WORKFLOW_NAME = "F_WorkflowName";

		/**
		 * F_RosterName.
		 */
		public static final String IW_ROSTER_NAME = "F_RosterName";

		/**
		 * F_StepReceived.
		 */
		public static final String IW_DATE_RECEIVED = "F_StepReceived";

		/**
		 * F_Originator.
		 */
		public static final String IW_ORIGINATOR = "F_Originator";

		/**
		 * F_Trackers.
		 */
		public static final String IW_TRACKERS = "F_Trackers";

		/**
		 * F_StartTime.
		 */
		public static final String IW_LAUNCHDATE = "F_StartTime";

		/**
		 * F_ParticipantName.
		 */
		public static final String IW_PARTICIPANTNAME = "F_ParticipantName";

		/**
		 * F_Deadline.
		 */
		public static final String IW_DEADLINE = "F_Deadline";

		/**
		 * F_Reminder.
		 */
		public static final String IW_REMINDER = "F_Reminder";

		/**
		 * F_Overdue.
		 */
		public static final String IW_OVERDUE = "F_Overdue";

		/**
		 * F_TrackerStatus.
		 */
		public static final String IW_IS_TRACKER = "F_TrackerStatus";

		/**
		 * F_WorkFlowNumber.
		 */
		public static final String IW_WORKFLOW_NUMBER = "F_WorkFlowNumber";

		/**
		 * F_OperationId.
		 */
		public static final String IW_OPERATION_ID = "F_OperationId";

		/**
		 * F_OperationType.
		 */
		public static final String IW_OPERATION_TYPE = "F_OperationType";

		/**
		 * F_UIFlag.
		 */
		public static final String IW_UI_FLAG = "F_UIFlag";

		/**
		 * F_StepName.
		 */
		public static final String IW_STEP_NAME = "F_StepName";

		/**
		 * F_StepProcId.
		 */
		public static final String IW_STEP_PROC_ID = "F_StepProcId";

		/**
		 * F_SourceDoc.
		 */
		public static final String IW_SOURCE_DOC = "F_SourceDoc";

		/**
		 * F_StepDescription.
		 */
		public static final String IW_STEP_DESCRIPTION = "F_StepDescription";

		/**
		 * F_StepResponses.
		 */
		public static final String IW_STEP_RESPONSES = "F_StepResponses";

		/**
		 * F_Class.
		 */
		public static final String VW_CLASS = "F_Class";
		
		/**
		 * GIRO_VISTI_ISPETTORATO.
		 */
		public static final String GIRO_VISTI_ISPETTORATO = "Giro Visti Ispettorato";

		/**
		 * Costruttore.
		 */
		protected PEProperty() {
		}

	}
	
	/**
	 * Costanti per le code.
	 * 
	 * @author APerquoti
	 *
	 */
	public static final class QueueConstant {
		
		/**
		 * Destinatario.
		 */
		public static final String INDEX_NAME_DESTINATARIO = "Destinatario";
		/**
		 * Step Destinatario.
		 */
		public static final String INDEX_NAME_DESTINATARIO_STEP = "DestinatarioStep";
		
		private QueueConstant() {
		}
	}
	
	/**
	 * Modalita di salvataggio del documento.
	 * 
	 * @author m.crescentini
	 *
	 */
	public static final class Modalita {
		
		/**
		 * Modalita - label.
		 */
		public static final String MODALITA_LABEL = "modalita";
		/**
		 * Modalita - insert.
		 */
		public static final String MODALITA_INS = "modalitainsert";
		/**
		 * Modalita - update.
		 */
		public static final String MODALITA_UPD = "modalitaupdate";
		/**
		 * Modalita - post censimento.
		 */
		public static final String MODALITA_INS_POSTCENS = "modalitainsertpostcensimento";
		/**
		 * Modalita - read only.
		 */
		public static final String MODALITA_READONLY = "modalitareadonly";
		/**
		 * Modalita - insert mail.
		 */
		public static final String MODALITA_INS_MAIL = "modalitainsertmail";
		/**
		 * Modalita - update ibrida.
		 */
		public static final String MODALITA_UPD_IBRIDA = "modalitaupdateibrida";
		
		/**
		 * Costruttore.
		 */
		protected Modalita() {
		}
	}
	
	
	/**
	 * Content type documenti.
	 * 
	 * @author m.crescentini
	 *
	 */
	public static final class ContentType {
		
		/**
		 * Content type - HTML.
		 */
		public static final String HTML = "text/html";
		/**
		 * Content type - MIMETYPE_ENC.
		 */
		public static final String MIMETYPE_ENC = "application/octet-stream";
		/**
		 * Content type - PDF.
		 */
		public static final String PDF = "application/pdf";
		/**
		 * Content type - XML.
		 */
		public static final String XML = "application/xml";
		
		/**
		 * Content type - XML.
		 */
		public static final String TEXT_XML = "text/xml";
		
		/**
		 * Content type - TEXT.
		 */
		public static final String TEXT = "text/plain";
		/**
		 * Content type - DOCX.
		 */
		public static final String DOCX = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
		/**
		 * Content type - DOC.
		 */
		public static final String DOC = "application/msword";
		/**
		 * Content type - RTF.
		 */
		public static final String RTF = "application/rtf";
		/**
		 * Content type - P7M_X_MIME.
		 */
		public static final String P7M_X_MIME = "application/x-pkcs7-mime";
		/**
		 * Content type - P7M_MIME.
		 */
		public static final String P7M_MIME = "application/pkcs7-mime";
		/**
		 * Content type - P7M.
		 */
		public static final String P7M = "application/pkcs7";
		/**
		 * Content type - P7M_PKNET_MANAGER.
		 */
		public static final String P7M_PKNET_MANAGER = "Application/PkNet Manager";
		/**
		 * Content type - ZIP_MIME.
		 */
		public static final String ZIP_MIME = "application/zip";
		/**
		 * Content type - ZIP_7Z_MIME.
		 */
		public static final String ZIP7Z_MIME = "application/x-7z-compressed";
		
		/**
		 * Formato XLS.
		 */
		public static final String XLS = "application/vnd.ms-excel";
		/**
		 * Content type - XLSX.
		 */
		public static final String XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
		/**
		 * Content type - GIF.
		 */
		public static final String GIF = "image/gif";
		/**
		 * Content type - JPG.
		 */
		public static final String JPG = "image/jpeg";
		/**
		 * Content type - TIFF.
		 */
		public static final String TIFF = "image/tiff";
		/**
		 * Content type - EML.
		 */
		public static final String EML = "message/rfc822";
		/**
		 * Content type - MSG.
		 */
		public static final String MSG = "application/vnd.ms-outlook";
		/**
		 * Content type - BMP.
		 */
		public static final String BMP = "image/bmp";
		/**
		 * Content type - PNG.
		 */
		public static final String PNG = "image/png";

		/**
		 * Costruttore.
		 */
		protected ContentType() {	
		}
	}
	
	
	/**
	 * Tipologia fascicolo (automatico/non automatico).
	 * 
	 * @author m.crescentini
	 *
	 */
	public static final class TipoFascicolo {
		
		/**
		 * Tipo fascicolo - Automatico.
		 */
		public static final int AUTOMATICO = 1;
		/**
		 * Tipo fascicolo - Non Automatico.
		 */
		public static final int NON_AUTOMATICO = 0;
		
		protected TipoFascicolo() {
		}
	}
	
	/**
	 * Costanti sottoscrizioni.
	 */
	public static final class Sottoscrizioni {
		
		/**
		 * Sottoscrizioni - id sottoscrizione.
		 */
		public static final String EVENTO_FIELD_IDSOTTOSCRIZIONE = "IDSOTTOSCRIZIONE";
		/**
		 * Sottoscrizioni - id evento.
		 */
		public static final String EVENTO_FIELD_IDEVENTO = "IDEVENTO";
		/**
		 * Sottoscrizioni - descrizione evento.
		 */
		public static final String EVENTO_FIELD_DESCRIZIONEEVENTO = "DESCRIZIONEEVENTO";
		/**
		 * Sottoscrizioni - dettaglio evento.
		 */
		public static final String EVENTO_FIELD_DETTAGLIOEVENTO = "DETTAGLIOEVENTO";
		/**
		 * Sottoscrizioni - tipologia evento.
		 */
		public static final String EVENTO_FIELD_TIPOLOGIAEVENTO = "TIPOLOGIAEVENTO";
		/**
		 * Sottoscrizioni - descrizione categoria.
		 */
		public static final String EVENTO_FIELD_DESCRIZIONECATEGORIA = "DESCRIZIONECATEGORIA";
		/**
		 * Sottoscrizioni - id AOO.
		 */
		public static final String EVENTO_FIELD_IDAOO = "IDAOO";
		/**
		 * Sottoscrizioni - id Nodo.
		 */
		public static final String EVENTO_FIELD_IDNODO = "IDNODO";
		/**
		 * Sottoscrizioni - id Ruolo.
		 */
		public static final String EVENTO_FIELD_IDRUOLO = "IDRUOLO";
		/**
		 * Sottoscrizioni - id Utente.
		 */
		public static final String EVENTO_FIELD_IDUTENTE = "IDUTENTE";
		/**
		 * Sottoscrizioni - scadenza_da.
		 */
		public static final String EVENTO_FIELD_SCADENZA_DA = "SCADENZADA";
		/**
		 * Sottoscrizioni - scadenza_a.
		 */
		public static final String EVENTO_FIELD_SCADENZA_A = "SCADENZAA";
		/**
		 * Sottoscrizioni - tracciamento.
		 */
		public static final String EVENTO_FIELD_TRACCIAMENTO = "TRACCIAMENTO";
		/**
		 * Sottoscrizioni - id canale trasmissione.
		 */
		public static final String EVENTO_FIELD_IDCANALETRASMISSIONE = "IDCANALETRASMISSIONE";
		/**
		 * Sottoscrizioni - nome canale trasmissione.
		 */
		public static final String CANALE_TRASMISSIONE_FIELD_NOME = "NOME";
		/**
		 * Sottoscrizioni - tracciamento Tutti.
		 */
		public static final int TRACCIAMENTO_TUTTI = 0;
		/**
		 * Sottoscrizioni - tracciamento Singolo.
		 */
		public static final int TRACCIAMENTO_SINGOLO = 1;
		/**
		 * Sottoscrizioni - assegnazioni ricevute.
		 */
		public static final int ASSEGNAZIONI_RICEVUTE = 1;
		/**
		 * Sottoscrizioni - procedimenti avviati.
		 */
		public static final int PROCEDIMENTI_AVVIATI = 2;
		/**
		 * Sottoscrizioni - procedimenti in scadenza.
		 */
		public static final int PROCEDIMENTI_IN_SCADENZA = 3;
		
		protected Sottoscrizioni() {
		}
	}
	
	/**
	 * Tipologia invio mail.
	 * 
	 * @author m.crescentini
	 *
	 */
	public static final class TipologiaInvio {
		
		/**
		 * Nuovo messaggio.
		 */
		public static final int NUOVOMESSAGGIO = 1;
		/**
		 * Inoltra.
		 */
		public static final int INOLTRA = 2;
		/**
		 * Rispondi.
		 */
		public static final int RISPONDI = 3;
		/**
		 * Rispondi a tutti.
		 */
		public static final int RISPONDIATUTTI = 3;
		
		protected TipologiaInvio() {
		}
	}
	
	/**
	 * Tipologia mail.
	 * 
	 * @author a.dilegge
	 *
	 */
	public static final class TipologiaMessaggio {
		
		/**
		 * Email.
		 */
		public static final int TIPO_MSG_EMAIL = 1;
		/**
		 * Posta Elettronica certificata.
		 */
		public static final int TIPO_MSG_PEC = 2;
		/**
		 * Fax.
		 */
		public static final int TIPO_MSG_FAX = 3;
		
		protected TipologiaMessaggio() {
		}
	}
	
	/**
	 * Tipologia destinatari mittenti elettronici.
	 * 
	 * @author a.dilegge
	 *
	 */
	public static final class TipologiaDestinatariMittenti {
		
		/**
		 * PEO.
		 */
		public static final int TIPOLOGIA_DESTINATARI_MITTENTI_PEO = 1;
		/**
		 * PEC.
		 */
		public static final int TIPOLOGIA_DESTINATARI_MITTENTI_PEC = 2;
		/**
		 * Fax.
		 */
		public static final int TIPOLOGIA_DESTINATARI_MITTENTI_FAX = 3;
		
		protected TipologiaDestinatariMittenti() {
		}
	}
	
	/**
	 * Response di sistema.
	 * 
	 * @author a.dilegge
	 *
	 */
	public static final class SysResponse {
		
		/**
		 *  RESPONSE CON CHIAMATA DIRETTA A PROCESSO FILENET TRAMITE CODICE.
		 */
		public static final String RESPONSE_SYS_SPEDITO = "SYS_spedito";
		
		protected SysResponse() {
		}
	}
	
	/**
	 * Tipologia riferimenti per la tabella Riferimento.
	 * 
	 * @author mcrescentini
	 *
	 */
	public static final class TipologiaRiferimento {
		
		/**
		 * Riferimento Documento.
		 */
		public static final int DOCUMENTO = 1;
		/**
		 * Riferimento Fascicolo.
		 */
		public static final int FASCICOLO = 2;
		/**
		 * Riferimento Faldone.
		 */
		public static final int FALDONE = 3;
		
		protected TipologiaRiferimento() {
		}
	}
	
	/**
	 * Costanti per gli identificativi dei ruoli.
	 * 
	 * @author m.crescentini
	 *
	 */
	public static final class Ruolo {
		
		/**
		 * Ruolo DIRIGENTE.
		 */
		public static final int DIRIGENTE = 2;
		/**
		 * Ruolo ISPETTORE.
		 */
		public static final int ISPETTORE = 3;
		
		protected Ruolo() {
		}
	}
	
	
	/**
	 * Costanti per le azioni sulle mail.
	 * 
	 * @author m.crescentini
	 *
	 */
	public static final class AzioneMail {
		
		/**
		 * Azione: Protocolla.
		 */
		public static final String PROTOCOLLA = "Protocolla";
		/**
		 * Azione: Protocolla Dsr.
		 */
		public static final String PROTOCOLLA_DSR = "Protocolla Dsr";
		/**
		 * Azione: Rifiuta.
		 */
		public static final String RIFIUTA = "Rifiuta";
		/**
		 * Azione: Inoltra.
		 */
		public static final String INOLTRA = "Inoltra";
		/**
		 * Azione: Elimina.
		 */
		public static final String ELIMINA = "Elimina";
		/**
		 * Azione: Ripristina.
		 */
		public static final String RIPRISTINA = "Ripristina";
		/**
		 * Azione: InoltraDaCoda.
		 */
		public static final String INOLTRA_DA_CODA = "InoltraDaCoda";
		/**
		 * Azione: Verifica.
		 */
		public static final String VERIFICA_FIRMA = "Verifica Firma";
		/**
		 * Azione: Inserisci nota.
		 */
		public static final String INSERISCI_NOTA = "Inserisci nota";
		
		private AzioneMail() {	
		}
	}
	
	/**
	 * Costanti per le modalità di spedizione delle mail:
	 * - SINGOLA: singola mail per ciascun destinatario.
	 * - UNICA: unica mail per tutti i destinatari.
	 * 
	 * @author m.crescentini
	 *
	 */
	public static final class ModalitaSpedizioneMail {
		
		/**
		 * Modalita spedizione Singola.
		 */
		public static final int SINGOLA = 1;
		/**
		 * Modalita spedizione Unica.
		 */
		public static final int UNICA = 2;
		
		private ModalitaSpedizioneMail() {
		}
	}
	
	/**
	 * Costanti tipologia casella postale.
	 */
	public static final class TipologiaCasellaPostale {
		
		/**
		 * Tipologia casella postale PEO.
		 */
		public static final int PEO = 1;
		/**
		 * Tipologia casella postale PEC.
		 */
		public static final int PEC = 2;
		
		private TipologiaCasellaPostale() {
		}
	}
	
	/**
	 * 
	 * @author APerquoti
	 *
	 */
	public static final class NpsConstants {

		/**
		 * Metodo: upload documento.
		 */
		public static final String UPLOAD_DOCUMENTO_METHOD_NAME = "upload";
		
		/**
		 * Metodo: aggiungi assegnatario.
		 */
		public static final String AGGIUNGI_ASSEGNATARIO_METHOD_NAME = "aggiungiAssegnatario";
		/**
		 * Stato: Skip post emergenza.
		 */
		public static final int STATO_SKIPPATA_POST_EMERGENZA = -1;
		/**
		 * Stato: Da lavorare.
		 */
		public static final int STATO_DA_LAVORARE = 1;
		/**
		 * Stato: In lavorazione.
		 */
		public static final int STATO_IN_LAVORAZIONE = 2;
		/**
		 * Stato: Lavorata.
		 */
		public static final int STATO_LAVORATA = 3;
		/**
		 * Stato: In errore.
		 */
		public static final int STATO_IN_ERRORE = 4;

		protected NpsConstants() {
		}
		
	}
	
	/**
	 * @author SLac 
	 * 
	 * Per metadati dinamici
	 */
	public static final class FilenetConstants {
		
		/**
		 * Lunghezza massima tipo Int Filenet.
		 */
		public static final int MAXLENGTH_INT_FILENET = 11;
		/**
		 * Lunghezza massima tipo String Filenet.
		 */
		public static final int MAXLENGTH_STRING_FILENET = 3800;
		
		private FilenetConstants() {
		}
	}
	
	/**
	 * Mappa chiavi Red.
	 */
	public static final class MapRedKey {
		
		/**
		 * Utente destinatario.
		 */
		public static final String UTENTE_DESTINATARIO_KEY = "UTENTE_DESTINATARIO";
		/**
		 * Ufficio destinatario.
		 */
		public static final String UFFICIO_DESTINATARIO_KEY = "UFFICIO_DESTINATARIO";
		/**
		 * Utente cedente.
		 */
		public static final String UTENTE_CEDENTE_KEY = "UTENTE_CEDENTE";
		/**
		 * Ufficio cedente.
		 */
		public static final String UFFICIO_CEDENTE_KEY = "UFFICIO_CEDENTE";
		/**
		 * AOO cedente.
		 */
		public static final String AOO_CEDENTE_KEY = "AOO_CEDENTE";
		/**
		 * Motivo assegnazione.
		 */
		public static final String MOTIVO_ASSEGNAZIONE_KEY = "MOTIVO_ASSEGNAZIONE";
		/**
		 * Numero fattura SIGI.
		 */
		public static final String NUMERO_FATTURA_SIGI = "NUMERO_FATTURA_SIGI";
		/**
		 * Anno fattura SIGI.
		 */
		public static final String ANNO_FATTURA_SIGI = "ANNO_FATTURA_SIGI";
		/**
		 * Nome File FEPA.
		 */
		public static final String NOME_FILE_FEPA = "nomeFileFepa";
		/**
		 * Id Fascicolo FEPA.
		 */
		public static final String ID_FASCICOLO_FEPA = "IdFascicoloFEPA";
		/**
		 * Numero protocollo FEPA.
		 */
		public static final String NUM_PROTOCOLLO_FEPA = "numProtocolloFepa";
		/**
		 * Data protocollo FEPA.
		 */
		public static final String DATA_PROTOCOLLO_FEPA = "dataProtocolloFepa";
		/**
		 * Oggetto protocollo FEPA.
		 */
		public static final String OGGETTO_PROTOCOLLO_FEPA = "oggettoProtocolloFepa";
		/**
		 * Stato documento FEPA.
		 */
		public static final String STATO_DOCUMENTO_FEPA = "statoDocumentoFepa";
		/**
		 * Anno protocollo FEPA.
		 */
		public static final String ANNO_PROTOCOLLO_FEPA = "annoProtocolloFepa";
		/**
		 * Mittente.
		 */
		public static final String MITTENTE = "fepa.mittente";
		/**
		 * Tipo decreto - label.
		 */
		public static final String TIPO_DECRETO = "tipologiaDecreto";
		/**
		 * Tipo decreto - netto.
		 */
		public static final String TIPO_DECRETO_NETTO = "NETTO";
		/**
		 * Tuoi decreto - IVA.
		 */
		public static final String TIPO_DECRETO_IVA = "IVA";
		/**
		 * Fattura assegnata.
		 */
		public static final String FATTURA_ASSEGNATA = "FatturaAssegnata";
		
		private MapRedKey() {
		}
	}
	
	/**
	 * @author m.crescentini 
	 * 
	 * Motivi assegnazione response
	 */
	public static final class MotivoAssegnazione {
		
		/**
		 * Motivo valida contributi.
		 */
		public static final String MOTIVO_VALIDA_CONTRIBUTI = "Contributi Validati";
		/**
		 * Motivo inserimento contributi..
		 */
		public static final String MOTIVO_INSERISCI_CONTRIBUTO = "Inserimento contributo";
		
		private MotivoAssegnazione() {
		}
	}
	
	/**
	 * Costanti FEPA.
	 */
	public static final class Fepa {
		
		/**
		 * Prefisso Oggetto protocollazione FEPA.
		 */
		public static final String OGGETTO_PROTOCOLLAZIONE_DSR_PREFIX = "DICHIARAZIONE SERVIZI RESI - ";
		
		private Fepa() {
		}
	}
	
	/**
	 * Costanti ricerca.
	 */
	public static final class Ricerca {
		
		/**
		 * CLASSE_DOCUMENTALE_FWS.
		 */
		public static final String CLASSE_DOCUMENTALE_FWS = "classedocumentalefws";
		/**
		 * DATA_CREAZIONE_DA.
		 */
		public static final String DATA_CREAZIONE_DA = "datacreazioneda";
		/**
		 * DATA_CREAZIONE_A.
		 */
		public static final String DATA_CREAZIONE_A = "datacreazionea";
		/**
		 * DATA_PROTOCOLLO_DA.
		 */
		public static final String DATA_PROTOCOLLO_DA = "dataprotocolloda";
		/**
		 * DATA_PROTOCOLLO_A.
		 */
		public static final String DATA_PROTOCOLLO_A = "dataprotocolloa";
		/**
		 * DATA_SCADENZA_DA.
		 */
		public static final String DATA_SCADENZA_DA = "datascadenzada";
		/**
		 * DATA_SCADENZA_A.
		 */
		public static final String DATA_SCADENZA_A = "datascadenzaa";
		/**
		 * NUMERO_PROTOCOLLO_DA.
		 */
		public static final String NUMERO_PROTOCOLLO_DA = "numeroprotocolloda";
		/**
		 * NUMERO_PROTOCOLLO_A.
		 */
		public static final String NUMERO_PROTOCOLLO_A = "numeroprotocolloa";
		/**
		 * TIPO_RICERCA_LISTA.
		 */
		public static final String TIPO_RICERCA_LISTA = "tiporicercalist";
		/**
		 * DATA_CHIUSURA_DA.
		 */
		public static final String DATA_CHIUSURA_DA = "datachiusurada";
		/**
		 * DATA_CHIUSURA_A.
		 */
		public static final String DATA_CHIUSURA_A = "datachiusuraa";
		/**
		 * NUMERO_PROTOCOLLO_EMERGENZA_DA.
		 */
		public static final String NUMERO_PROTOCOLLO_EMERGENZA_DA = "numeroprotocolloemergenzada";
		/**
		 * NUMERO_PROTOCOLLO_EMERGENZA_A.
		 */
		public static final String NUMERO_PROTOCOLLO_EMERGENZA_A = "numeroprotocolloemergenzaa";
		/**
		 * DATA_PROTOCOLLO_EMERGENZA_DA.
		 */
		public static final String DATA_PROTOCOLLO_EMERGENZA_DA = "dataprotocolloemergenzada";
		/**
		 * DATA_PROTOCOLLO_EMERGENZA_A.
		 */
		public static final String DATA_PROTOCOLLO_EMERGENZA_A = "dataprotocolloemergenzaa";
		/**
		 * TIPO_CAMPO_RICERCA_NUMERICO.
		 */
		public static final String TIPO_CAMPO_RICERCA_NUMERICO = "NUMERICO";
		/**
		 * TIPO_CAMPO_RICERCA_TESTO_ESATTO.
		 */
		public static final String TIPO_CAMPO_RICERCA_TESTO_ESATTO = "TESTO_ESATTO";
		/**
		 * TIPO_CAMPO_RICERCA_TESTO_LIKE.
		 */
		public static final String TIPO_CAMPO_RICERCA_TESTO_LIKE = "TESTO_LIKE";
		/**
		 * STATO_DOC_DA_FIRMARE.
		 */
		public static final String STATO_DOC_DA_FIRMARE = "Da firmare";
		/**
		 * STATO_DOC_PROTOCOLLATO_FIRMATO.
		 */
		public static final String STATO_DOC_PROTOCOLLATO_FIRMATO = "Protocollato e firmato";
		/**
		 * STATO_DOC_SPEDITO.
		 */
		public static final String STATO_DOC_SPEDITO = "Spedito";
		/**
		 * STATO_DOC_SPEDITO_ERRORE.
		 */
		public static final String STATO_DOC_SPEDITO_ERRORE = "Spedito con errore";
		/**
		 * STATO_DOC_ANNULLATO.
		 */
		public static final String STATO_DOC_ANNULLATO = "Annullato";
		/**
		 * STATO_DOC_RIFIUTATO.
		 */
		public static final String STATO_DOC_RIFIUTATO = "Rifiutato";
		/**
		 * TIPO_ESITO_DOC.
		 */
		public static final String TIPO_ESITO_DOC = "tipoesitodocumento";
		/**
		 * KEY_RICERCA_UCB_DOC.
		 */
		public static final String KEY_RICERCA_UCB_DOC = "Documenti"; // Devono riflettere il nome del tab della propria ricerca
		/**
		 * KEY_RICERCA_UCB_ASS.
		 */
		public static final String KEY_RICERCA_UCB_ASS = "Assegnante/Assegnatario"; // Devono riflettere il nome del tab della propria ricerca
		/**
		 * KEY_RICERCA_UCB_ESITI.
		 */
		public static final String KEY_RICERCA_UCB_ESITI = "Esiti"; // Devono riflettere il nome del tab della propria ricerca
		/**
		 * KEY_RICERCA_ESTRATTI_CONTO_CCVT.
		 */
		public static final String KEY_RICERCA_ESTRATTI_CONTO_CCVT = "Estratti Conto Trimestrali CCVT";
		
		private Ricerca() {
		}
	}
	
	/**
	 * @author APerquoti
	 */
	public static final class StatoConservazione {

		/**
		 * Da conservare.
		 */
		public static final int DA_CONSERVARE = 1;
		/**
		 * Conservato.
		 */
		public static final int CONSERVATO = 2;
		/**
		 * In errore.
		 */
		public static final int IN_ERRORE = 3;

		protected StatoConservazione() {
		}
	}

	/**
	 * 
	 * @author APerquoti
	 *
	 */
	public static final class Contatto {
		/**
		 * Contatto RED.
		 */
		public static final String CONTATTO_RED = "RED";
		/**
		 * Contatto MEF.
		 */
		public static final String CONTATTO_MEF = "MEF";
		/**
		 * Contatto IPA.
		 */
		public static final String CONTATTO_IPA = "IPA";
		/**
		 * Contatto GRUPPO.
		 */
		public static final String CONTATTO_GRUPPO = "GRUPPO";
		/**
		 * Contatto Amministrazione.
		 */
		public static final int CONTATTO_AMM = 1;
		/**
		 * Contatto AOO.
		 */
		public static final int CONTATTO_AOO = 2;
		/**
		 * Contatto OU.
		 */
		public static final int CONTATTO_OU = 3;
		/**
		 * Contatto Amministrazione - Label.
		 */
		public static final String CONTATTO_AMM_STRING = "AMM";
		/**
		 * Contatto AOO - Label.
		 */
		public static final String CONTATTO_AOO_STRING = "AOO";
		/**
		 * Contatto OU - Label.
		 */
		public static final String CONTATTO_OU_STRING = "OU";
		/**
		 * Contatto Ufficio MEF.
		 */
		public static final int CONTATTO_UFFICIO_MEF = 4;
		/**
		 * Contatto Utente MEF.
		 */
		public static final int CONTATTO_UTENTE_MEF = 5;
		/**
		 * Contatto Utente MEF - Label.
		 */
		public static final String CONTATTO_UTENTE_MEF_STRING = "UTENTE";
		/**
		 * Contatto Ufficio MEF - Label.
		 */
		public static final String CONTATTO_UFFICIO_MEF_STRING = "UFFICIO";
		
		protected Contatto() {
		}
		
	}
	
	/**
	 * Costruttore.
	 */
	protected Constants() {
	}
}