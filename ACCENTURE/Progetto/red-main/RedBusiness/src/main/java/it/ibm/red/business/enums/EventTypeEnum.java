package it.ibm.red.business.enums;

/**
 * The Enum EventTypeEnum.
 *
 * @author CPIERASC
 * 
 *         Enum eventi.
 */
public enum EventTypeEnum {
	
	/**
	 * Documento.
	 */
	DOCUMENTO("1001"),

	/**
	 * Fascicolo.
	 */
	FASCICOLO("1002"),

	/**
	 * Faldone.
	 */
	FALDONE("1003"),

	/**
	 * Assegnazione per competenza.
	 */
	ASSEGNAZIONE_COMPETENZA("1004"),

	/**
	 * Assegnazione per conoscenza.
	 */
	ASSEGNAZIONE_CONOSCENZA("1005"),

	/**
	 * Assegnazione per firma.
	 */
	ASSEGNAZIONE_FIRMA("1006"),

	/**
	 * Assegnazione per sigla.
	 */
	ASSEGNAZIONE_SIGLA("1007"),

	/**
	 * Assegnazione per visto.
	 */
	ASSEGNAZIONE_VISTO("1008"),

	/**
	 * Atti.
	 */
	ATTI("1009"),

	/**
	 * Risposta/Predisponi Documento.
	 */
	RISPOSTA("1010"),

	/**
	 * Trasferimento per competenza.
	 */
	TRASFERIMENTO_COMPETENZA("1011"),

	/**
	 * Rifiuto.
	 */
	RIFIUTO("1012"),

	/**
	 * Riassegnazione.
	 */
	RIASSEGNAZIONE("1013"),

	/**
	 * Firmato.
	 */
	FIRMATO("1014"),

	/**
	 * Siglato.
	 */
	SIGLATO("1015"),

	/**
	 * Vistato.
	 */
	VISTATO("1016"),

	/**
	 * Presa visione.
	 */
	PRESA_VISIONE("1017"),

	/**
	 * Inserimento contributo.
	 */
	INSERIMENTO_CONTRIBUTO("1018"),

	/**
	 * Richiesta contributo.
	 */
	RICHIESTA_CONTRIBUTO("1019"),
	
	/**
	 * Spedito.
	 */
	SPEDITO("1020"),

	/**
	 * Firma autografa.
	 */
	FIRMA_AUTOGRAFA("1025"),
	
	/**
	 * Assegnazione diretta all'utente.
	 */
	ASSEGNAZIONE_DIRETTA_A_UTENTE("1036"),
	
	/**
	 * Richiesta visto.
	 */
	RICHIESTA_VISTO("1061"),
	
	/**
	 * Assegnazione per Firma Copia Conforme.
	 */
	ASSEGNAZIONE_PER_FIRMA_COPIA_CONFORME("1063"),

	/**
	 * Assegnazione spedizione.
	 */
	ASSEGNAZIONE_SPEDIZIONE("1067"),
	
	/**
	 * Annullato.
	 */
	ANNULLATO("1068"),
	
	/**
	 * Completamento spedizione elettronica.
	 */
	COMPLETAMENTO_SPED_ELETTRONICA("1084"),

	/**
	 * Firmato e spedito.
	 */
	FIRMATO_SPEDITO("1085"),

	/**
	 * Modifica flag urgente.
	 */
	MODIFICA_URGENTE("1089"),
	
	/**
	 * Modifica data scadenza.
	 */
	MODIFICA_DATA_SCADENZA("1090"),
	
	/**
	 * Sollecito contributo.
	 */
	SOLLECITO_CONTRIBUTO("1091"),

	/**
	 * Richiedi contributo.
	 */
	RICHIEDI_CONTRIBUTO("1092"),

	/**
	 * Collega contributo.
	 */
	COLLEGA_CONTRIBUTO("1093"),
	
	/**
	 * Eliminazione allegato copia conforme.
	 */
	ELIMINAZIONE_ALLEGATO_COPIA_CONFORME("1094"),

	/**
	 * Modifica livello riservatezza.
	 */
	MODIFICA_LIVELLO_RISERVATEZZA("1095"),
	
	/**
	 * Firmato per firma multipla.
	 */
	FIRMATO_FIRMA_MULTIPLA("1105"),
	
	/**
	 * Predisposizione risposta.
	 */
	PREDISPOSIZIONE_RISPOSTA("1108"),

	/**
	 * Predisposizione inoltro.
	 */
	PREDISPOSIZIONE_INOLTRO("1109"),
	
	/**
	 * Predisposizione visto.
	 */
	PREDISPOSIZIONE_VISTO("1111"),
	
	/**
	 * Predisposizione osservazione.
	 */
	PREDISPOSIZIONE_OSSERVAZIONE("1112"),
	
	/**
	 * Ricihesta integrazioni.
	 */
	RICHIESTA_INTEGRAZIONI("1113"),
	
	/**
	 * Predisposizione restituzione.
	 */
	PREDISPOSIZIONE_RESTITUZIONE("1114"),
	
	/**
	 * Predisposizione relazione positiva.
	 */
	PREDISPOSIZIONE_RELAZIONE_POSITIVA("1115"),
	
	/**
	 * Predisposizione relazione negativa.
	 */
	PREDISPOSIZIONE_RELAZIONE_NEGATIVA("1116"),

	/**
	 * Migrazione PMEF.
	 */
	MIGRAZIONE_PMEF("3001"),
	
	/**
	 * Errore spedizione.
	 */
	ERRORE_SPEDIZIONE_MAIL("9990"),
	
	/**
	 * Modifica dopo visto.
	 */
	MODIFICATO_DOPO_VISTO("9994"),
	
	/**
	 * Modiifca dopo sigla.
	 */
	MODIFICATO_DOPO_SIGLA("9995"),
	
	/**
	 * Inoltro mail da coda attiva.
	 */
	INOLTRO_MAIL_DA_CODA_ATTIVA("9996"),
	
	/**
	 * Copia in fascicolo.
	 */
	COPIA_IN_ALTRO_FASCICOLO("9997"),
	
	/**
	 * Sposta in fascicolo.
	 */
	SPOSTA_IN_ALTRO_FASCICOLO("9998"),
 
	/**
	 * Elimina contributo.
	 */
	ELIMINAZIONE_CONTRIBUTO("1083"),
	
	/**
	 * Avvenuta approvazione.
	 */
	AVVENUTA_APPROVAZIONE("1110"),

	/**
	 * Rifiuto firma.
	 */
	RIFIUTO_FIRMA("9989"),

	/**
	 * Osservazione annullata.
	 */
	OSSERVAZIONE_ANNULLATA("9980"),

	/**
	 * Richiesta integrazione annullata.
	 */
	RICHIESTA_INTEGRAZIONE_ANNULLATA("9981"),

	/**
	 * Annullamento visto.
	 */
	VISTO_ANNULLATO("9982"),
	
	/**
	 * Relazione positiva annullata.
	 */
	RELAZIONE_POSITIVA_ANNULLATA("9984"),
	
	/**
	 * Relazione negativa annullata.
	 */
	RELAZIONE_NEGATIVA_ANNULLATA("9985"),
	
	/**
	 * Spostamento in lavorazione automatico.
	 */
	SPOSTATO_IN_LAVORAZIONE_AUTOMATICO("9000"),
	
	/**
	 * Ricezione titiro autotutela.
	 */
	RICEZIONE_RITIRO_IN_AUTOTUTELA("9100"),
	
	/**
	 * Riconciliazione integrazione dati.
	 */
	RICONCILIAZIONE_INTEGRAZIONE_DATI("9983"),

	/**
	 * Ricezione integrazione dati.
	 */
	RICEZIONE_INTEGRAZIONE_DATI("9101"),
	
	/**
	 * Migrazione PRGS.
	 */
	MIGRAZIONE_PRGS("3020"),
	
	/**
	 * MOdifica oggetto.
	 */
	MODIFICA_OGGETTO("3021"),
	
	/**
	 * Modifica mittente.
	 */
	MODIFICA_MITTENTE("3022"),
	
	/**
	 * Modifica nota.
	 */
	MODIFICA_NOTA("3023"), 
	
	/**
	 * Notifica mancata spedizione NPS.
	 */
	NOTIFICA_MANCATA_SPEDIZIONE("3024");
	

	
	/**
	 * Valore.
	 */
	private String value;
	
	/**
	 * Costruttore.
	 * 
	 * @param inValue	valore
	 */
	EventTypeEnum(final String inValue) {
		value = inValue;
	}

	/**
	 * Metodo per testare se una stringa è uno dei valori associati agli eventi forniti in input.
	 * 
	 * @param eventType	stringa
	 * @param events	lista eventi
	 * @return			risultato test
	 */
	public static Boolean isIn(final String eventType, final EventTypeEnum... events) {
		Boolean output = false;
		if (events != null) {
			for (EventTypeEnum event:events) {
				if (event.getValue().equals(eventType)) {
					output = true;
					break;
				}
			}
		}
		return output;
	}

	/**
	 * Getter valore.
	 * 
	 * @return	valore
	 */
	public String getValue() {
		return value;
	}
	
	/**
	 * Restituisce un intero per la stringa.
	 * @return intero per la stringa
	 */
	public Integer getIntValue() {
		return Integer.parseInt(value);
	}
	
	/**
	 * Metodo per il recupero di un enum a partire dal suo valore caratteristico.
	 * 
	 * @param value	valore
	 * @return		enum associata al valore
	 */
	public static EventTypeEnum get(final Integer value) {
		for (EventTypeEnum cat:EventTypeEnum.values()) {
			if (value.equals(cat.getIntValue())) {
				return cat;
			}
		}
		return null;
	}
}
