package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IRifiutaSpedizioneFacadeSRV;

/**
 * Interfaccia del servizio di gestione rifiuto spedizione.
 */
public interface IRifiutaSpedizioneSRV extends IRifiutaSpedizioneFacadeSRV {

}
