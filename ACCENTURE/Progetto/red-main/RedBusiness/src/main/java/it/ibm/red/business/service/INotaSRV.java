package it.ibm.red.business.service;

import java.sql.Connection;

import it.ibm.red.business.dto.AssegnatarioInteropDTO;
import it.ibm.red.business.dto.NotaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.service.facade.INotaFacadeSRV;

/**
 * The Interface INotaSRV.
 *
 * @author FMANTUANO
 * 
 *         Servizi gestione della nota sul workflow.
 */
public interface INotaSRV extends INotaFacadeSRV {
	/**
	 * Salvataggio della nota ribaltata dalla coda mail.
	 * 
	 * @param utente							utente che esegue l'operazione.
	 * @param idDocumento						documentitle del documento a cui è associata la nota.
	 * @param contentNoteDaMailProtocolla		viene chiamato nel documento contentNote.
	 * @param con								connessione proveniente da altre fonti.
	 */
	void registraNotaFromCodaMail(UtenteDTO utente, int idDocumento, String contentNoteDaMailProtocolla, Connection con);
	 
	/**
	 * Registra la nota dalla coda mail.
	 * @param utente
	 * @param idDocumento
	 * @param contentNoteDaMailProtocolla
	 * @param preassegnatario
	 * @param con
	 */
	void registraNotaFromCodaMailDTO(UtenteDTO utente, int idDocumento, NotaDTO contentNoteDaMailProtocolla, AssegnatarioInteropDTO preassegnatario, Connection con);

}
