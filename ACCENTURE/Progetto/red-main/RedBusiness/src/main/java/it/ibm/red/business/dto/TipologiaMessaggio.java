package it.ibm.red.business.dto;

import java.io.Serializable;

/**
 * DTO che definisce la tipologia messaggio.
 */
public class TipologiaMessaggio implements Serializable  {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -6919991897412322342L;

	/**
	 * Tipolgia messaggio.
	 */
	private Integer idTipologiaMessaggio;
	
	/**
	 * Restituisce l'id tipologia messaggio.
	 * @return id tipologia messaggio
	 */
	public Integer getIdTipologiaMessaggio() {
		return idTipologiaMessaggio;
	}

	/**
	 * Imposta l'id della tipologia messaggio.
	 * @param idTipologiaMessaggio
	 */
	public void setIdTipologiaMessaggio(final Integer idTipologiaMessaggio) {
		this.idTipologiaMessaggio = idTipologiaMessaggio;
	}
	
}
