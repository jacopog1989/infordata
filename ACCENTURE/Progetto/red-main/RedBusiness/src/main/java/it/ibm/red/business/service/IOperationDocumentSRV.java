package it.ibm.red.business.service;

import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.service.facade.IOperationDocumentFacadeSRV;

/**
 * Interfaccia dei metodi delle operazioni di sigla/vista/rifiuta.
 * 
 * @author CPAOLUZI
 *
 */
public interface IOperationDocumentSRV extends IOperationDocumentFacadeSRV {

	/**
	 * Metodo per lo storno workflow per contributo.
	 * @param currentWobNumber
	 * @param idDocumento
	 * @param fpeh
	 * @param idAoo
	 */
	void stornaWorkflowPerContributo(String currentWobNumber, String idDocumento, FilenetPEHelper fpeh, Long idAoo);

}
