package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.Date;

import com.filenet.api.core.ComponentRelationship;
import com.filenet.api.core.Document;

import it.ibm.red.business.dto.MasterDocCartaceoRedDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class FromDocumentoToMasterCartaceoTrasformer.
 *
 * @author m.crescentini
 * 
 *         Trasformer documento cartaceo.
 */
public class FromDocumentoToMasterCartaceoTrasformer extends TrasformerCE<MasterDocCartaceoRedDTO> {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -5272643610748848810L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FromDocumentoToMasterCartaceoTrasformer.class.getName());


	/**
	 * Costruttore.
	 */
	public FromDocumentoToMasterCartaceoTrasformer() {
		super(TrasformerCEEnum.FROM_DOCUMENTO_TO_MASTER_CARTACEO);
	}


	/**
	 * Trasform.
	 *
	 * @param document
	 *            the document
	 * @return the master documento DTO
	 */
	@Override
	public final MasterDocCartaceoRedDTO trasform(final Document document, final Connection connection) {
		try {
			String documentTitle = (String) getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
			Date dataCreazione = (Date) getMetadato(document, PropertiesNameEnum.DATA_CREAZIONE_METAKEY);
			String barcode = (String) getMetadato(document, PropertiesNameEnum.BARCODE_METAKEY);
			String barcodePrincipale = (String) getMetadato(document, PropertiesNameEnum.BARCODE_PRINCIPALE_METAKEY);
			String statoRecogniform = (String) getMetadato(document, PropertiesNameEnum.STATO_RECOGNIFORM_METAKEY);
			
			// Se si tratta un documento con barcode principale, quindi di un allegato, si recupera il GUID del documento principale
			String guidPrincipale = null;
			if (!StringUtils.isNullOrEmpty(barcodePrincipale) && document.get_ParentRelationships() != null && !document.get_ParentRelationships().isEmpty()) {
				guidPrincipale = ((ComponentRelationship) document.get_ParentRelationships().iterator().next()).get_ParentComponent().get_Id().toString();
			}
			
			return new MasterDocCartaceoRedDTO(documentTitle, document.get_Id().toString(), dataCreazione, barcode, barcodePrincipale, statoRecogniform, 
					document.get_MimeType(), guidPrincipale);
		} catch (Exception e) {
			LOGGER.error("Errore nel recupero di un metadato durante la trasformazione del master del documento cartaceo: " + document.get_Id(), e);
			return null;
		}
	}
	
}
