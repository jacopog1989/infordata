package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IFirmaRapidaFacadeSRV;

/**
 * Interfaccia per l'implementazione dei servizi di firma.
 * 
 * @author APerquoti
 *
 */
public interface IFirmaRapidaSRV extends IFirmaRapidaFacadeSRV {

}
