package it.ibm.red.business.dto;

import java.util.Date;

/**
 * Classe ParamsRicercaFascicoliPregressiDfDTO.
 *
 * @author VINGENITO
 */
public class ParamsRicercaFascicoliPregressiDfDTO extends AbstractDTO {

	private static final long serialVersionUID = -6709118116367270434L;

	/**
	 * Nome fascicolo 
	 */
	private String nomeFascicolo;

	/**
	 * Descrizione fascicolo 
	 */
	private String descrizioneFascicolo;

	/**
	 * Responsabile fascicolo 
	 */
	private String responsabileFascicolo;

	/**
	 * Stato fascicolo 
	 */
	private String statoFascicolo;

	/**
	 * Codice titolorario fascicolo 
	 */
	private String codiceTitolarioFascicolo;

	/**
	 * Numero fascicolo 
	 */
	private Integer numeroFascicolo;

	/**
	 * Data apertura da 
	 */
	private Date dataAperturaDa;

	/**
	 * Data apertura A 
	 */
	private Date dataAperturaA;
	
	/**
	 * Data chiusura da 
	 */
	private Date dataChiusuraDa;

	/**
	 * Data chiusura a 
	 */
	private Date dataChiusuraA;
	
	/**
	 * Data scadenza da 
	 */
	private Date dataScadenzaDa;

	/**
	 * Data scadenza a 
	 */
	private Date dataScadenzaA;
	
	/**
	 * Numero prtocollo fascicolo 
	 */
	private Integer numeroProtocolloFascicolo;

	/**
	 * Anno protocollo Fascicolo 
	 */
	private Integer annoProtocolloFascicolo;
	
	/**
	 * Id registro fascicolo 
	 */
	private String idRegistroFascicolo;

	/**
	 * Ufficio proprietario fascicolo 
	 */
	private String ufficioProprietarioFascicolo;

	

	/** 
	 *
	 * @return the nomeFascicolo
	 */
	public String getNomeFascicolo() {
		return nomeFascicolo;
	}

	/** 
	 *
	 * @param nomeFascicolo the nomeFascicolo to set
	 */
	public void setNomeFascicolo(final String nomeFascicolo) {
		this.nomeFascicolo = nomeFascicolo;
	}

	/** 
	 *
	 * @return the descrizioneFascicolo
	 */
	public String getDescrizioneFascicolo() {
		return descrizioneFascicolo;
	}

	/** 
	 *
	 * @param descrizioneFascicolo the descrizioneFascicolo to set
	 */
	public void setDescrizioneFascicolo(final String descrizioneFascicolo) {
		this.descrizioneFascicolo = descrizioneFascicolo;
	}

	/** 
	 *
	 * @return the responsabileFascicolo
	 */
	public String getResponsabileFascicolo() {
		return responsabileFascicolo;
	}

	/** 
	 *
	 * @param responsabileFascicolo the responsabileFascicolo to set
	 */
	public void setResponsabileFascicolo(final String responsabileFascicolo) {
		this.responsabileFascicolo = responsabileFascicolo;
	}

	/** 
	 *
	 * @return the statoFascicolo
	 */
	public String getStatoFascicolo() {
		return statoFascicolo;
	}

	/** 
	 *
	 * @param statoFascicolo the statoFascicolo to set
	 */
	public void setStatoFascicolo(final String statoFascicolo) {
		this.statoFascicolo = statoFascicolo;
	}

	/** 
	 *
	 * @return the codiceTitolarioFascicolo
	 */
	public String getCodiceTitolarioFascicolo() {
		return codiceTitolarioFascicolo;
	}

	/** 
	 *
	 * @param codiceTitolarioFascicolo the codiceTitolarioFascicolo to set
	 */
	public void setCodiceTitolarioFascicolo(final String codiceTitolarioFascicolo) {
		this.codiceTitolarioFascicolo = codiceTitolarioFascicolo;
	}

	/** 
	 *
	 * @return the numeroFascicolo
	 */
	public Integer getNumeroFascicolo() {
		return numeroFascicolo;
	}

	/** 
	 *
	 * @param numeroFascicolo the numeroFascicolo to set
	 */
	public void setNumeroFascicolo(final Integer numeroFascicolo) {
		this.numeroFascicolo = numeroFascicolo;
	}

	/** 
	 *
	 * @return the numeroProtocolloFascicolo
	 */
	public Integer getNumeroProtocolloFascicolo() {
		return numeroProtocolloFascicolo;
	}

	/** 
	 *
	 * @param numeroProtocolloFascicolo the numeroProtocolloFascicolo to set
	 */
	public void setNumeroProtocolloFascicolo(final Integer numeroProtocolloFascicolo) {
		this.numeroProtocolloFascicolo = numeroProtocolloFascicolo;
	}

	/** 
	 *
	 * @return the ufficioProprietarioFascicolo
	 */
	public String getUfficioProprietarioFascicolo() {
		return ufficioProprietarioFascicolo;
	}

	/** 
	 *
	 * @param ufficioProprietarioFascicolo the ufficioProprietarioFascicolo to set
	 */
	public void setUfficioProprietarioFascicolo(final String ufficioProprietarioFascicolo) {
		this.ufficioProprietarioFascicolo = ufficioProprietarioFascicolo;
	}

	/** 
	 *
	 * @return the annoProtocolloFascicolo
	 */
	public Integer getAnnoProtocolloFascicolo() {
		return annoProtocolloFascicolo;
	}

	/** 
	 *
	 * @param annoProtocolloFascicolo the annoProtocolloFascicolo to set
	 */
	public void setAnnoProtocolloFascicolo(final Integer annoProtocolloFascicolo) {
		this.annoProtocolloFascicolo = annoProtocolloFascicolo;
	}

	/** 
	 *
	 * @return the idRegistroFascicolo
	 */
	public String getIdRegistroFascicolo() {
		return idRegistroFascicolo;
	}

	/** 
	 *
	 * @param idRegistroFascicolo the registroFascicolo to set
	 */
	public void setIdRegistroFascicolo(final String idRegistroFascicolo) {
		this.idRegistroFascicolo = idRegistroFascicolo;
	}

	/** 
	 *
	 * @return the dataAperturaDa
	 */
	public Date getDataAperturaDa() {
		return dataAperturaDa;
	}

	/** 
	 *
	 * @param dataAperturaDa the dataAperturaDa to set
	 */
	public void setDataAperturaDa(final Date dataAperturaDa) {
		this.dataAperturaDa = dataAperturaDa;
	}

	/** 
	 *
	 * @return the dataAperturaA
	 */
	public Date getDataAperturaA() {
		return dataAperturaA;
	}

	/** 
	 *
	 * @param dataAperturaA the dataAperturaA to set
	 */
	public void setDataAperturaA(final Date dataAperturaA) {
		this.dataAperturaA = dataAperturaA;
	}

	/** 
	 *
	 * @return the dataChiusuraDa
	 */
	public Date getDataChiusuraDa() {
		return dataChiusuraDa;
	}

	/** 
	 *
	 * @param dataChiusuraDa the dataChiusuraDa to set
	 */
	public void setDataChiusuraDa(final Date dataChiusuraDa) {
		this.dataChiusuraDa = dataChiusuraDa;
	}

	/** 
	 *
	 * @return the dataChiusuraA
	 */
	public Date getDataChiusuraA() {
		return dataChiusuraA;
	}

	/** 
	 *
	 * @param dataChiusuraA the dataChiusuraA to set
	 */
	public void setDataChiusuraA(final Date dataChiusuraA) {
		this.dataChiusuraA = dataChiusuraA;
	}

	/** 
	 *
	 * @return the dataScadenzaDa
	 */
	public Date getDataScadenzaDa() {
		return dataScadenzaDa;
	}

	/** 
	 *
	 * @param dataScadenzaDa the dataScadenzaDa to set
	 */
	public void setDataScadenzaDa(final Date dataScadenzaDa) {
		this.dataScadenzaDa = dataScadenzaDa;
	}

	/** 
	 *
	 * @return the dataScadenzaA
	 */
	public Date getDataScadenzaA() {
		return dataScadenzaA;
	}

	/** 
	 *
	 * @param dataScadenzaA the dataScadenzaA to set
	 */
	public void setdataScadenzaA(final Date dataScadenzaA) {
		this.dataScadenzaA = dataScadenzaA;
	}

}
