package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IAnagraficaDipendenteDAO;
import it.ibm.red.business.dto.AnagraficaDipendenteDTO;
import it.ibm.red.business.dto.IndirizzoDTO;
import it.ibm.red.business.dto.RicercaAnagDipendentiDTO;
import it.ibm.red.business.dto.SelectItemDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.StringUtils;

/**
 * DAO per la gestione delle anagrafiche dipendenti.
 */
@Repository
public class AnagraficaDipendenteDAO extends AbstractDAO implements IAnagraficaDipendenteDAO {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -1024279590929293340L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AnagraficaDipendenteDAO.class.getName());
	
	/**
	 * Colonna TOPONIMO.
	 */
	private static final String IND_TOPONIMO = "IND_TOPONIMO";

	/**
	 * Colonna Telefono.
	 */
	private static final String IND_TELEFONO = "IND_TELEFONO";

	/**
	 * Colonna Note.
	 */
	private static final String IND_NOTE = "IND_NOTE";

	/**
	 * Colonna ID Provincia ISTAT.
	 */
	private static final String IND_IDPROVINCIAISTAT = "IND_IDPROVINCIAISTAT";

	/**
	 * Colonna Id.
	 */
	private static final String IND_ID = "IND_ID";

	/**
	 * Colonna Fax.
	 */
	private static final String IND_FAX = "IND_FAX";

	/**
	 * Colonna Codice DUG.
	 */
	private static final String IND_DUG_CODICE = "IND_DUG_CODICE";

	/**
	 * Colonna Comune.
	 */
	private static final String IND_COMUNE = "IND_COMUNE";

	/**
	 * Colonna Civico
	 */
	private static final String IND_CIVICO = "IND_CIVICO";

	/**
	 * Colonna CAP.
	 */
	private static final String IND_CAP = "IND_CAP";

	/**
	 * Label - And ufficio mittente.
	 */
	private static final String AND_UFFICIO_MITTENTE = "AND_UFFICIO_MITTENTE";

	/**
	 * Label - And Nome.
	 */
	private static final String AND_NOME = "AND_NOME";

	/**
	 * Label - And Id provincia ISTAT.
	 */
	private static final String AND_IDPROVINCIAISTAT = "AND_IDPROVINCIAISTAT";

	/**
	 * Label - And Id.
	 */
	private static final String AND_ID = "AND_ID";

	/**
	 * Label - And Flag Pensione.
	 */
	private static final String AND_FLAG_PENSIONE = "AND_FLAG_PENSIONE";

	/**
	 * Label - And Flag maschio.
	 */
	private static final String AND_FLAG_MASCHIO = "AND_FLAG_MASCHIO";

	/**
	 * Label - And Flag celibe.
	 */
	private static final String AND_FLAG_CELIBE = "AND_FLAG_CELIBE";

	/**
	 * Label - And Data nascita.
	 */
	private static final String AND_DATA_NASCITA = "AND_DATA_NASCITA";

	/**
	 * Label - And comune nascita.
	 */
	private static final String AND_COMUNE_NASCITA = "AND_COMUNE_NASCITA";

	/**
	 * Label - And Cognome.
	 */
	private static final String AND_COGNOME = "AND_COGNOME";

	/**
	 * Label - And Codice fiscale.
	 */
	private static final String AND_CODICE_FISCALE = "AND_CODICE_FISCALE";

	/**
	 * Label - And codice.
	 */
	private static final String AND_CODICE = "AND_CODICE";

	/**
	 * Messaggio di errore modifica anagrafica.
	 */
	private static final String MODIFICA_ANAGRAFICA_ERROR_MESSAGE = "Errore durante la modifica dell'anagrafica.";

	/**
	 * Messaggio di errore salvataggio anagrafica.
	 */
	private static final String SALVATAGGIO_ANAGRAFICA_ERROR_MESSAGE = "Errore durante il salvataggio dell'anagrafica.";

	/**
	 * @see it.ibm.red.business.dao.IAnagraficaDipendenteDAO#modifica(java.sql.Connection,
	 *      it.ibm.red.business.dto.AnagraficaDipendenteDTO).
	 */
	@Override
	public void modifica(final Connection con, final AnagraficaDipendenteDTO newAnagrafica) {
		PreparedStatement prepStat = null;
		try {

			final SimpleUpdateBuilder sub = new SimpleUpdateBuilder("ANAGRAFICA_DIPENDENTI");
			
			sub.addParams(AND_CODICE, newAnagrafica.getCodice());
			sub.addParams(AND_NOME, newAnagrafica.getNome());
			sub.addParams(AND_COGNOME, newAnagrafica.getCognome());
			sub.addParams(AND_CODICE_FISCALE, newAnagrafica.getCodiceFiscale());
			sub.addParams(AND_IDPROVINCIAISTAT, newAnagrafica.getCodiceProvinciaNascita());
			sub.addParams(AND_COMUNE_NASCITA, newAnagrafica.getComuneNascita());
			sub.addParams(AND_UFFICIO_MITTENTE, newAnagrafica.getUfficioMittente());
			sub.addParams(AND_FLAG_CELIBE, newAnagrafica.getFlagCelibe());
			sub.addParams(AND_FLAG_MASCHIO, newAnagrafica.getFlagMaschio());
			sub.addParams(AND_FLAG_PENSIONE, newAnagrafica.getFlagPensione());
			sub.addParams(AND_DATA_NASCITA, newAnagrafica.getDataNascita());

			prepStat = sub.update(con, AND_ID, newAnagrafica.getId());

		} catch (final Exception e) {
			LOGGER.error(MODIFICA_ANAGRAFICA_ERROR_MESSAGE, e);
			throw new RedException(MODIFICA_ANAGRAFICA_ERROR_MESSAGE, e);
		} finally {
			closeStatement(prepStat);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IAnagraficaDipendenteDAO#salva(java.sql.Connection,
	 *      long, it.ibm.red.business.dto.AnagraficaDipendenteDTO).
	 */
	@Override
	public AnagraficaDipendenteDTO salva(final Connection con, final long idAoo, final AnagraficaDipendenteDTO newAnagrafica) {

		try {

			final SimpleInsertBuilder sib = new SimpleInsertBuilder("ANAGRAFICA_DIPENDENTI");
			
			final String seqName = "SEQ_ANAGRAFICA_DIPENDENTI";
			sib.addSequence(AND_ID, seqName);
			
			sib.addParams(AND_CODICE, newAnagrafica.getCodice());
			sib.addParams(AND_NOME, newAnagrafica.getNome());
			sib.addParams(AND_COGNOME, newAnagrafica.getCognome());
			sib.addParams(AND_CODICE_FISCALE, newAnagrafica.getCodiceFiscale());
			sib.addParams(AND_IDPROVINCIAISTAT, newAnagrafica.getCodiceProvinciaNascita());
			sib.addParams(AND_COMUNE_NASCITA, newAnagrafica.getComuneNascita());
			sib.addParams(AND_UFFICIO_MITTENTE, newAnagrafica.getUfficioMittente());
			sib.addParams(AND_FLAG_CELIBE, newAnagrafica.getFlagCelibe());
			sib.addParams(AND_FLAG_MASCHIO, newAnagrafica.getFlagMaschio());
			sib.addParams(AND_FLAG_PENSIONE, newAnagrafica.getFlagPensione());
			sib.addParams(AND_DATA_NASCITA, newAnagrafica.getDataNascita());
			sib.addParams("ID_AOO", idAoo);
			
			sib.insert(con);
			
			newAnagrafica.setId(sib.getSequencesValue().get(AND_ID));

		} catch (final Exception e) {
			LOGGER.error(SALVATAGGIO_ANAGRAFICA_ERROR_MESSAGE, e);
			throw new RedException(SALVATAGGIO_ANAGRAFICA_ERROR_MESSAGE, e);
		} 
		return newAnagrafica;
	}

	/**
	 * @see it.ibm.red.business.dao.IAnagraficaDipendenteDAO#ricerca(java.sql.Connection,
	 *      long, it.ibm.red.business.dto.RicercaAnagDipendentiDTO).
	 */
	@Override
	public Collection<AnagraficaDipendenteDTO> ricerca(final Connection con, final long idAoo, final RicercaAnagDipendentiDTO ricercaDTO) {
		PreparedStatement prepStat = null;
		ResultSet rs = null;
		final Collection<AnagraficaDipendenteDTO> output = new ArrayList<>();
		try {
			final StringBuilder sb = new StringBuilder("SELECT * FROM ANAGRAFICA_DIPENDENTI WHERE 1=1 ");

			if (!StringUtils.isNullOrEmpty(ricercaDTO.getCodiceFiscale())) {
				sb.append(" and upper(AND_CODICE_FISCALE) like upper('%'||?||'%') ");
			}
			if (!StringUtils.isNullOrEmpty(ricercaDTO.getCognome())) {
				sb.append(" and upper(AND_COGNOME) like upper('%'||?||'%') ");
			}
			if (!StringUtils.isNullOrEmpty(ricercaDTO.getNome())) {
				sb.append(" and upper(AND_NOME) like upper('%'||?||'%') ");
			}

			sb.append(" and ID_AOO = ? ");
			
			prepStat = con.prepareStatement(sb.toString());

			int index = 1;
			if (!StringUtils.isNullOrEmpty(ricercaDTO.getCodiceFiscale())) {
				prepStat.setString(index, ricercaDTO.getCodiceFiscale());
				index++;
			}
			if (!StringUtils.isNullOrEmpty(ricercaDTO.getCognome())) {
				prepStat.setString(index, ricercaDTO.getCognome());
				index++;
			}
			if (!StringUtils.isNullOrEmpty(ricercaDTO.getNome())) {
				prepStat.setString(index, ricercaDTO.getNome());
				index++;
			}
			prepStat.setLong(index, idAoo);
			
			rs = prepStat.executeQuery();
			while (rs.next()) {
				final AnagraficaDipendenteDTO anag = createAnagrafica(rs);
				output.add(anag);
			}
			
		} catch (final Exception e) {
			LOGGER.error("Errore durante la ricerca delle anagrafiche.", e);
			throw new RedException("Errore durante la ricerca delle anagrafiche.", e);
		} finally {
			closeStatement(prepStat, rs);
		}
		return output;
	}

	private static AnagraficaDipendenteDTO createAnagrafica(final ResultSet rs) throws SQLException {
		final AnagraficaDipendenteDTO anag = new AnagraficaDipendenteDTO();
		anag.setId(rs.getInt(AND_ID));
		anag.setCodice(rs.getString(AND_CODICE));
		anag.setCodiceFiscale(rs.getString(AND_CODICE_FISCALE));
		anag.setNome(rs.getString(AND_NOME));
		anag.setCognome(rs.getString(AND_COGNOME));
		anag.setCodiceProvinciaNascita(rs.getString(AND_IDPROVINCIAISTAT));
		anag.setComuneNascita(rs.getString(AND_COMUNE_NASCITA));
		anag.setDataNascita(rs.getDate(AND_DATA_NASCITA));
		anag.setFlagCelibe(getFlag(rs.getString(AND_FLAG_CELIBE)));
		anag.setFlagMaschio(getFlag(rs.getString(AND_FLAG_MASCHIO)));
		anag.setFlagPensione(getFlag(rs.getString(AND_FLAG_PENSIONE)));
		anag.setUfficioMittente(rs.getString(AND_UFFICIO_MITTENTE));
		return anag;
	}
	
	private static Boolean getFlag(final String flag) {
		Boolean out = false;
		if (flag != null && flag.toUpperCase().contentEquals("S")) {
			out = true;
		}
		return out;
	}

	/**
	 * @see it.ibm.red.business.dao.IAnagraficaDipendenteDAO#salvaIndirizzo(java.sql.Connection,
	 *      java.lang.Integer, it.ibm.red.business.dto.IndirizzoDTO).
	 */
	@Override
	public IndirizzoDTO salvaIndirizzo(final Connection con, final Integer idAnagraficaDipendente, final IndirizzoDTO newIndirizzo) {
		try {

			final SimpleInsertBuilder sib = new SimpleInsertBuilder("INDIRIZZO_DIPENDENTI");
			
			final String seqName = "SEQ_INDIRIZZO_DIPENDENTI";
			sib.addSequence(IND_ID, seqName);
			
			sib.addParams(IND_TOPONIMO, newIndirizzo.getToponimo());
			sib.addParams(IND_CIVICO, newIndirizzo.getCivico());
			sib.addParams(IND_CAP, newIndirizzo.getCap());
			sib.addParams(IND_IDPROVINCIAISTAT, newIndirizzo.getCodiceProvincia());
			sib.addParams(IND_COMUNE, newIndirizzo.getComune());
			sib.addParams(IND_TELEFONO, newIndirizzo.getTelefono());
			sib.addParams(IND_FAX, newIndirizzo.getFax());
			sib.addParams(IND_NOTE, newIndirizzo.getNote());
			sib.addParams(IND_DUG_CODICE, newIndirizzo.getCodiceDug());
			sib.addParams("IND_AND_ID", idAnagraficaDipendente);
			
			sib.insert(con);
			
			newIndirizzo.setId(sib.getSequencesValue().get(IND_ID));

		} catch (final Exception e) {
			LOGGER.error(SALVATAGGIO_ANAGRAFICA_ERROR_MESSAGE, e);
			throw new RedException(SALVATAGGIO_ANAGRAFICA_ERROR_MESSAGE, e);
		}
		return newIndirizzo;	
	}

	/**
	 * @see it.ibm.red.business.dao.IAnagraficaDipendenteDAO#modificaIndirizzo(java.sql.Connection,
	 *      java.lang.Integer, it.ibm.red.business.dto.IndirizzoDTO).
	 */
	@Override
	public void modificaIndirizzo(final Connection con, final Integer idAnagraficaDipendente, final IndirizzoDTO newIndirizzo) {
		PreparedStatement prepStat = null;
		try {

			final SimpleUpdateBuilder sub = new SimpleUpdateBuilder("INDIRIZZO_DIPENDENTI");
			
			sub.addParams(IND_TOPONIMO, newIndirizzo.getToponimo());
			sub.addParams(IND_CIVICO, newIndirizzo.getCivico());
			sub.addParams(IND_CAP, newIndirizzo.getCap());
			sub.addParams(IND_IDPROVINCIAISTAT, newIndirizzo.getCodiceProvincia());
			sub.addParams(IND_COMUNE, newIndirizzo.getComune());
			sub.addParams(IND_TELEFONO, newIndirizzo.getTelefono());
			sub.addParams(IND_FAX, newIndirizzo.getFax());
			sub.addParams(IND_NOTE, newIndirizzo.getNote());
			sub.addParams(IND_DUG_CODICE, newIndirizzo.getCodiceDug());
			sub.addParams("IND_AND_ID", idAnagraficaDipendente);
			
			prepStat = sub.update(con, IND_ID, newIndirizzo.getId());

		} catch (final Exception e) {
			LOGGER.error(MODIFICA_ANAGRAFICA_ERROR_MESSAGE, e);
			throw new RedException(MODIFICA_ANAGRAFICA_ERROR_MESSAGE, e);
		} finally {
			closeStatement(prepStat);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IAnagraficaDipendenteDAO#getDug(java.sql.Connection).
	 */
	@Override
	public Collection<SelectItemDTO> getDug(final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final String sql = "SELECT DUG_CODICE, DUG_DESCRIZIONE FROM DEN_URB_GENERICA ";
		final Collection<SelectItemDTO> out = new ArrayList<>();
		try {
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			while (rs.next()) {
				out.add(new SelectItemDTO(rs.getString("DUG_CODICE"), rs.getString("DUG_DESCRIZIONE")));
			}
		} catch (final SQLException e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return out;
	}

	/**
	 * @see it.ibm.red.business.dao.IAnagraficaDipendenteDAO#getIndirizzi(java.sql.Connection,
	 *      java.lang.Integer).
	 */
	@Override
	public Collection<IndirizzoDTO> getIndirizzi(final Connection con, final Integer idAnagrafica) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final String sql = "SELECT * FROM INDIRIZZO_DIPENDENTI INNER JOIN DEN_URB_GENERICA ON IND_DUG_CODICE = DUG_CODICE WHERE IND_AND_ID = ?";
		final Collection<IndirizzoDTO> out = new ArrayList<>();
		try {
			ps = con.prepareStatement(sql);
			ps.setInt(1, idAnagrafica);
			rs = ps.executeQuery();
			while (rs.next()) {
				final IndirizzoDTO indirizzo = new IndirizzoDTO();
				indirizzo.setCap(rs.getString(IND_CAP));
				indirizzo.setCivico(rs.getString(IND_CIVICO));
				indirizzo.setCodiceDug(rs.getString(IND_DUG_CODICE));
				indirizzo.setDescrizioneDug(rs.getString("DUG_DESCRIZIONE"));
				indirizzo.setCodiceProvincia(rs.getString(IND_IDPROVINCIAISTAT));
				indirizzo.setComune(rs.getString(IND_COMUNE));
				indirizzo.setFax(rs.getString(IND_FAX));
				indirizzo.setId(rs.getInt(IND_ID));
				indirizzo.setNote(rs.getString(IND_NOTE));
				indirizzo.setTelefono(rs.getString(IND_TELEFONO));
				indirizzo.setToponimo(rs.getString(IND_TOPONIMO));
				out.add(indirizzo);
			}
		} catch (final SQLException e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		return out;
	}

	/**
	 * @see it.ibm.red.business.dao.IAnagraficaDipendenteDAO#removeIndirizzo(java.sql.Connection,
	 *      java.lang.Integer).
	 */
	@Override
	public void removeIndirizzo(final Connection con, final Integer idIndirizzo) {
		PreparedStatement ps = null;
		final ResultSet rs = null;
		final String sql = "DELETE FROM INDIRIZZO_DIPENDENTI WHERE IND_ID = ?";
		try {
			ps = con.prepareStatement(sql);
			ps.setInt(1, idIndirizzo);
			ps.executeUpdate();
		} catch (final SQLException e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IAnagraficaDipendenteDAO#remove(java.sql.Connection,
	 *      java.lang.Integer).
	 */
	@Override
	public void remove(final Connection con, final Integer id) {
		PreparedStatement ps = null;
		final ResultSet rs = null;
		final String sql = "DELETE FROM ANAGRAFICA_DIPENDENTI WHERE AND_ID = ?";
		try {
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			ps.executeUpdate();
		} catch (final SQLException e) {
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IAnagraficaDipendenteDAO#getByPks(java.sql.Connection,
	 *      java.util.Collection).
	 */
	@Override
	public Collection<AnagraficaDipendenteDTO> getByPks(final Connection connection, final Collection<Integer> pks) {
		final Collection<AnagraficaDipendenteDTO> out = new ArrayList<>();
		PreparedStatement prepStat = null;
		ResultSet rs = null;
		try {
			final StringBuilder sb = new StringBuilder("SELECT * FROM ANAGRAFICA_DIPENDENTI WHERE AND_ID IN ( ");
			for (final Integer pk:pks) {
				sb.append(pk);
				sb.append(",");
			}
			String query = StringUtils.cutLast(sb.toString());
			query += " )";
			prepStat = connection.prepareStatement(query);
			rs = prepStat.executeQuery();
			while (rs.next()) {
				out.add(createAnagrafica(rs));
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero delle anagrafiche.", e);
			throw new RedException("Errore durante il recupero delle anagrafiche.", e);
		} finally {
			closeStatement(prepStat, rs);
		}
		return out;
	}

}