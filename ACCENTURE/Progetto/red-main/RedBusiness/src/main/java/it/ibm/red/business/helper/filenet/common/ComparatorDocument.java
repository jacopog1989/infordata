package it.ibm.red.business.helper.filenet.common;

import java.util.Comparator;

import com.filenet.api.core.Document;

/**
 * The Class ComparatorDocument.
 *
 * @author CPIERASC
 * 
 *         Comparator dei documenti basato sulle versioni.
 */
public class ComparatorDocument implements Comparator<Document> {

	/**
	 * Compare.
	 *
	 * @param o1
	 *            the o 1
	 * @param o2
	 *            the o 2
	 * @return the int
	 */
	@Override
	public final int compare(final Document o1, final Document o2) {
		int majorVersionO1 = o1.get_MajorVersionNumber().intValue();
		int majorVersionO2 = o2.get_MajorVersionNumber().intValue();
		if (majorVersionO1 > majorVersionO2) {
			return 1;
		} else if (majorVersionO1 < majorVersionO2) {
			return -1;
		} else {
			return 0;
		}
	}

}
