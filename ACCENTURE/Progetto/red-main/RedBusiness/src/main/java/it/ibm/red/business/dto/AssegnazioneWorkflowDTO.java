package it.ibm.red.business.dto;

import it.ibm.red.business.dto.filenet.FascicoloRedFnDTO;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;

/**
 * DTO assegnazione workflow.
 */
public class AssegnazioneWorkflowDTO extends AbstractDTO {
	
	/**
	 * La costante serial Version UID.
	 */
	private static final long serialVersionUID = 5409011184793133416L;

	/**
	 * Identificativo ufficio assegnatario.
	 */
	private Long idUfficioAssegnatario;
	
	/**
	 * Identificativo utente assegnatario.
	 */
	private Long idUtenteAssegnatario;
	
	/**
	 * Tipologia assegnazione.
	 */
	private TipoAssegnazioneEnum tipoAssegnazione;
	
	/**
	 * Fascicolo.
	 */
	private FascicoloRedFnDTO fascicolo;

	/**
	 * Restituisce l'id dell'ufficio assegnatario.
	 * @return idUfficioAssegnatario
	 */
	public Long getIdUfficioAssegnatario() {
		return idUfficioAssegnatario;
	}

	/**
	 * Imposta id ufficio assegnatario.
	 * @param idUfficioAssegnatario
	 */
	public void setIdUfficioAssegnatario(final Long idUfficioAssegnatario) {
		this.idUfficioAssegnatario = idUfficioAssegnatario;
	}

	/**
	 * Restituisce l'id dell'utente assegnatario.
	 * @return idUtenteAssegnatario
	 */
	public Long getIdUtenteAssegnatario() {
		return idUtenteAssegnatario;
	}

	/**
	 * Imposta l'id dell'utente assegnatario.
	 * @param idUtenteAssegnatario
	 */
	public void setIdUtenteAssegnatario(final Long idUtenteAssegnatario) {
		this.idUtenteAssegnatario = idUtenteAssegnatario;
	}

	/**
	 * Restituisce il tipo assegnazione: TipoAssegnazioneEnum.
	 * @return tipoAssegnazione.
	 */
	public TipoAssegnazioneEnum getTipoAssegnazione() {
		return tipoAssegnazione;
	}

	/**
	 * Imposta il tipo assegnazione.
	 * @param tipoAssegnazione
	 */
	public void setTipoAssegnazione(final TipoAssegnazioneEnum tipoAssegnazione) {
		this.tipoAssegnazione = tipoAssegnazione;
	}

	/**
	 * Restituisce il fascicolo come FascicoloRedFnDTO.
	 * @return fascicolo
	 */
	public FascicoloRedFnDTO getFascicolo() {
		return fascicolo;
	}

	/**
	 * Imposta il fascicolo.
	 * @param fascicolo
	 */
	public void setFascicolo(final FascicoloRedFnDTO fascicolo) {
		this.fascicolo = fascicolo;
	}
}