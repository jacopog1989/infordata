package it.ibm.red.business.dto;

import java.io.Serializable;
import java.lang.reflect.Method;

/**
 * DTO che definisce un service task.
 */
public class ServiceTaskDTO implements Serializable {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 7185531893935918882L;

	/**
	 * Metodo Servizio singolo.
	 */
	private transient Method singleService;

	/**
	 * Metodo Servizio multiplo.
	 */
	private transient Method multiService;

	/**
	 * Classe di riferimento.
	 */
	private final Class<?> clsSrv;

	/**
	 * Costruttore.
	 * 
	 * @param clsSrv
	 */
	public ServiceTaskDTO(final Class<?> clsSrv) {
		super();
		this.clsSrv = clsSrv;
	}

	/**
	 * Restituisce il single service.
	 * 
	 * @return single service
	 */
	public Method getSingleService() {
		return singleService;
	}

	/**
	 * Imposta il single service.
	 * 
	 * @param singleService
	 */
	public void setSingleService(final Method singleService) {
		this.singleService = singleService;
	}

	/**
	 * Restituisce il multi service.
	 * 
	 * @return multi service
	 */
	public Method getMultiService() {
		return multiService;
	}

	/**
	 * Imposta il multi service.
	 * 
	 * @param multiService
	 */
	public void setMultiService(final Method multiService) {
		this.multiService = multiService;
	}

	/**
	 * @return clsSrv
	 */
	public Class<?> getClsSrv() {
		return clsSrv;
	}
}