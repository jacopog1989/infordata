package it.ibm.red.business.enums;

/**
 * The Enum TipoAzioneEnum.
 *
 * @author CPIERASC
 * 
 *         Enum per le azioni rappresentate sul calendario.
 */
public enum TipoAzioneEnum {	

	/**
	 * Scadenza documento.
	 */
	SCADENZA_DOCUMENTO(0, "Documento"),
	
	/**
	 * Evento.
	 */
	EVENTO(1, "Evento"),
	
	/**
	 * Tutti.
	 */
	ALL(2, "Tutti");

	/**
	 * Valore.
	 */
	private Integer value;

	/**
	 * Descrizione.
	 */
	private String description;

	/**
	 * Costruttore.
	 * 
	 * @param inValue		valore
	 * @param inDescription	descrizione
	 */
	TipoAzioneEnum(final int inValue, final String inDescription) {
		this.value = inValue;
		this.description = inDescription;
	}

	/**
	 * Getter descrizione.
	 * 
	 * @return	descrizione
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Getter valore.
	 * 
	 * @return	valore
	 */
	public Integer getValue() {
		return value;
	}

}
