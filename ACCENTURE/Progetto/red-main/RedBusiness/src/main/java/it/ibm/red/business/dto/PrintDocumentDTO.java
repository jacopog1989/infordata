package it.ibm.red.business.dto;

import com.itextpdf.text.Rectangle;

/**
 * Classe PrintDocumentDTO.
 */
public class PrintDocumentDTO extends AbstractDTO {

	 
	private static final long serialVersionUID = 6093817335383986954L;

	/**
	 * Content 
	 */
	private byte[] content;
	 	
	/**
	 * Posizione 
	 */
	private transient Rectangle position;
	
	/** 
	 * @return the content
	 */
	public byte[] getContent() {
		return content;
	}
	
	/** 
	 * @param content the new content
	 */
	public void setContent(final byte[] content) {
		this.content = content;
	}
	
	/** 
	 * @return the position
	 */
	public Rectangle getPosition() {
		return position;
	}
	
	/** 
	 * @param position the new position
	 */
	public void setPosition(final Rectangle position) {
		this.position = position;
	}
	 
}
