package it.ibm.red.business.dto;

/**
 * DTO che definisce la tipologia di un documento.
 */
public class TipologiaDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -2933716129708479931L;

	/**
	 * Id tipo documento.
	 */
	private Integer idTipoDocumento;

	/**
	 * Id tipo procedimento.
	 */
	private Integer idTipoProcedimento;

	/**
	 * Restituisce l'id del tipo documento.
	 * @return id tipo documento
	 */
	public Integer getIdTipoDocumento() {
		return idTipoDocumento;
	}
	
	/**
	 * Imposta l'id del tipo documento.
	 * @param idTipoDocumento
	 */
	public void setIdTipoDocumento(final Integer idTipoDocumento) {
		this.idTipoDocumento = idTipoDocumento;
	}
	
	/**
	 * Restituisce l'id tipo procedimento.
	 * @return id tipo procedimento
	 */
	public Integer getIdTipoProcedimento() {
		return idTipoProcedimento;
	}
	
	/**
	 * Imposta l'id del tipo procedimento.
	 * @param idTipoProcedimento
	 */
	public void setIdTipoProcedimento(final Integer idTipoProcedimento) {
		this.idTipoProcedimento = idTipoProcedimento;
	}

}
