package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IREDServiceFacadeSRV;

/**
 * Interfaccia del servizio REDService.
 */
public interface IREDServiceSRV extends IREDServiceFacadeSRV {

	
}
