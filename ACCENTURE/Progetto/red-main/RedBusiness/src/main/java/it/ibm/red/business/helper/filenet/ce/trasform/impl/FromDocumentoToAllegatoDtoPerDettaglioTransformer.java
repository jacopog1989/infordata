/**
 * 
 */
package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.filenet.api.core.Document;

import it.ibm.red.business.dao.ITipologiaDocumentoDAO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.enums.ValueMetadatoVerificaFirmaEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.utils.StringUtils;

/**
 * @author APerquoti
 *
 */
public class FromDocumentoToAllegatoDtoPerDettaglioTransformer extends ContextAllegatoTrasformerCE<AllegatoDTO> {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -8662018343327308079L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FromDocumentoToAllegatoDtoPerDettaglioTransformer.class);

	/**
	 * Dao per recuperare la tipologia di documento.
	 */
	private final ITipologiaDocumentoDAO tipologiaDocumentoDAO;

	/**
	 * Costruttore.
	 */
	public FromDocumentoToAllegatoDtoPerDettaglioTransformer() {
		super(TrasformerCEEnum.FROM_DOCUMENT_TO_ALLEGATO_CONTEXT);
		tipologiaDocumentoDAO = ApplicationContextProvider.getApplicationContext().getBean(ITipologiaDocumentoDAO.class);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.ContextAllegatoTrasformerCE#trasform(com.filenet.api.core.Document,
	 *      java.sql.Connection, java.util.Map).
	 */
	@SuppressWarnings("unchecked")
	@Override
	public final AllegatoDTO trasform(final Document document, final Connection connection, final Map<ContextTrasformerCEEnum, Object> context) {
		AllegatoDTO a = null;

		try {
			final String documentTitle = (String) getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
			final Integer formato = (Integer) getMetadato(document, PropertiesNameEnum.FORMATO_ALLEGATO_ID_METAKEY);
			final Integer idTipologiaDocumento = (Integer) getMetadato(document, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY);
			TipologiaDocumentoDTO tipologiaDTO = null;
			String tipologia = null;
			final Collection<TipologiaDocumentoDTO> listaTipoDoc = new ArrayList<>();

			final List<TipologiaDocumentoDTO> listTipoDocAttive = (List<TipologiaDocumentoDTO>) context.get(ContextTrasformerCEEnum.TIPOLOGIE_DOCUMENTO_ATTIVE);
			final Map<Integer, TipologiaDocumentoDTO> mapTipoDocDisattive = (Map<Integer, TipologiaDocumentoDTO>) context
					.get(ContextTrasformerCEEnum.TIPOLOGIE_DOCUMENTO_NON_ATTIVE);

			if (listTipoDocAttive != null) {
				listaTipoDoc.addAll(listTipoDocAttive);
			}

			if (idTipologiaDocumento != null && idTipologiaDocumento.intValue() > 0) {
				tipologiaDTO = tipologiaDocumentoDAO.getById(idTipologiaDocumento, connection);
				tipologia = tipologiaDTO.getDescrizione();

				if (mapTipoDocDisattive != null && mapTipoDocDisattive.containsKey(idTipologiaDocumento)) {
					listaTipoDoc.add(mapTipoDocDisattive.get(idTipologiaDocumento));
				}

			}

			final String oggetto = (String) getMetadato(document, PropertiesNameEnum.OGGETTO_METAKEY);
			final String barcode = (String) getMetadato(document, PropertiesNameEnum.BARCODE_METAKEY);
			final Boolean formatoOriginale = (Boolean) getMetadato(document, PropertiesNameEnum.IS_FORMATO_ORIGINALE_METAKEY);
			final Integer daFirmare = (Integer) getMetadato(document, PropertiesNameEnum.DA_FIRMARE_METAKEY);
			final String nomeFile = (String) getMetadato(document, PropertiesNameEnum.NOME_FILE_METAKEY);
			final String mimeType = document.get_MimeType();
			final String guid = StringUtils.cleanGuidToString(document.get_Id());
			final Date dateCreated = document.get_DateCreated();
			final Boolean copiaConforme = (Boolean) getMetadato(document, PropertiesNameEnum.IS_COPIA_CONFORME_METAKEY);
			Long idUfficioCopiaConforme = null;
			Long idUtenteCopiaConforme = null;
			final String idUfficioCopiaConformeStr = (String) getMetadato(document, PropertiesNameEnum.ID_UFFICIO_COPIA_CONFORME_METAKEY);
			if (idUfficioCopiaConformeStr != null) {
				idUfficioCopiaConforme = Long.parseLong(idUfficioCopiaConformeStr);
			}
			final String idUtenteCopiaConformeStr = (String) getMetadato(document, PropertiesNameEnum.ID_UTENTE_COPIA_CONFORME_METAKEY);
			if (idUtenteCopiaConformeStr != null) {
				idUtenteCopiaConforme = Long.parseLong(idUtenteCopiaConformeStr);
			}

			// infoAllacci
			final String idFascicoloProvenienza = (String) getMetadato(document, PropertiesNameEnum.ALLEGATO_IDFASCICOLOPROVENIENZA_METAKEY);
			final String descrizioneFascicoloProvenienza = (String) getMetadato(document, PropertiesNameEnum.ALLEGATO_DESCRIZIONEFASCICOLOPROVENIENZA_METAKEY);
			final String documentTitleDocumentoProvenienza = (String) getMetadato(document, PropertiesNameEnum.ALLEGATO_DOCUMENTTITLEDOCUMENTOPROVENIENZA_METAKEY);
			final String descrizioneDocumentoProvenienza = (String) getMetadato(document, PropertiesNameEnum.ALLEGATO_DESCRIZIONEDOCUMENTOPROVENIENZA_METAKEY);
			final String documentTitleAllegatoProvenienza = (String) getMetadato(document, PropertiesNameEnum.ALLEGATO_DOCUMENTTITLEALLEGATOPROVENIENZA_METAKEY);
			final String nomeFileAllegatoProvenienza = (String) getMetadato(document, PropertiesNameEnum.ALLEGATO_NOMEFILEALLEGATOPROVENIENZA_METAKEY);

			final Boolean fileNonSbustatoBool = (Boolean) getMetadato(document, PropertiesNameEnum.ALLEGATO_NON_SBUSTATO);
			boolean fileNonSbustato = false;
			if (fileNonSbustatoBool != null) {
				fileNonSbustato = fileNonSbustatoBool;
			}

			final Boolean stampigliaSiglaBool = (Boolean) getMetadato(document, PropertiesNameEnum.STAMPIGLIATURA_SIGLA_ALLEGATO);
			boolean stampigliaSigla = false;
			if (stampigliaSiglaBool != null) {
				stampigliaSigla = stampigliaSiglaBool;
			}
			final String placeholderSigla = (String) getMetadato(document, PropertiesNameEnum.PLACEHOLDER_SIGLA_ALLEGATO);

			final Boolean timbroUscitaBool = (Boolean) getMetadato(document, PropertiesNameEnum.IS_TIMBRO_USCITA_METAKEY);
			boolean timbroUscita = false;
			if (timbroUscitaBool != null) {
				timbroUscita = timbroUscitaBool;
			}

			Float contentSize = null;
			if (document.get_ContentSize() != null) {
				final float size = document.get_ContentSize().floatValue();
				final float sizeInMb = ((size / 1024) / 1024);
				contentSize = sizeInMb;
			}

			final Boolean firmaVisibile = (Boolean) getMetadato(document, PropertiesNameEnum.FIRMA_VISIBILE_METAKEY);

			final Integer metadatoDocumentoValiditaFirma = (Integer) getMetadato(document, PropertiesNameEnum.METADATO_DOCUMENTO_VALIDITA_FIRMA);
			ValueMetadatoVerificaFirmaEnum valueMetadatoEnum = null;
			if (metadatoDocumentoValiditaFirma != null) {
				valueMetadatoEnum = ValueMetadatoVerificaFirmaEnum.get(metadatoDocumentoValiditaFirma);
			}

			a = new AllegatoDTO(documentTitle, formato, idTipologiaDocumento, tipologia, oggetto, barcode, formatoOriginale, daFirmare, copiaConforme, guid, nomeFile,
					mimeType, dateCreated, idUfficioCopiaConforme, idUtenteCopiaConforme, idFascicoloProvenienza, descrizioneFascicoloProvenienza,
					documentTitleDocumentoProvenienza, descrizioneDocumentoProvenienza, documentTitleAllegatoProvenienza, nomeFileAllegatoProvenienza, listaTipoDoc,
					fileNonSbustato, stampigliaSigla, placeholderSigla, timbroUscita, contentSize, firmaVisibile, valueMetadatoEnum);

		} catch (final Exception e) {
			LOGGER.error("Errore nella trasformazione dell'allegato: " + e.getMessage(), e);
			a = null;
		}

		return a;
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.ContextAllegatoTrasformerCE#trasform(com.filenet.api.core.Document,
	 *      com.filenet.api.core.Document, java.util.Map).
	 */
	@Override
	public AllegatoDTO trasform(final Document allegato, final Document principale, final Map<ContextTrasformerCEEnum, Object> context) {
		throw new RedException();
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.ContextTrasformerCE#trasform(com.filenet.api.core.Document,
	 *      java.util.Map).
	 */
	@Override
	public AllegatoDTO trasform(final Document document, final Map<ContextTrasformerCEEnum, Object> context) {
		throw new RedException();
	}

}
