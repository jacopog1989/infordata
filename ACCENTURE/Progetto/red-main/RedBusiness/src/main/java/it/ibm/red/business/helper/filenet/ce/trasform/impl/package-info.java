/**
 * @author CPIERASC
 *
 *	In questo package avremo le implementazioni delle trasformazioni di documenti del content engine in oggetti java.
 *	In pratica si fanno corrispondere ai metadati della classe documentale proprietà di un oggetto java: al programmatore
 *	il compito di esplicitare la logica di trasformazione (ovviamente disaccoppiando dal resto del codice la logica di
 *	trasformazione se ne facilita il riuso).
 *	
 */
package it.ibm.red.business.helper.filenet.ce.trasform.impl;
