package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.HashMap;

import it.ibm.red.business.persistence.model.Nodo;

/**
 * Facade del servizio che gestisce i nodi.
 */
public interface INodoFacadeSRV extends Serializable {

	/**
	 * Ottiene il nodo dall'id dell'ufficio.
	 * 
	 * @param idUfficio
	 * @return nodo
	 */
	Nodo getNodo(Long idUfficio);

	/**
	 * Otteniamo la mappa di id nodo descrizione per idAoo
	 * @param idAoo 
	 * @return map
	 */
	HashMap<Integer, String> getDescrizioneByIdNodo(Integer idAoo);

}
