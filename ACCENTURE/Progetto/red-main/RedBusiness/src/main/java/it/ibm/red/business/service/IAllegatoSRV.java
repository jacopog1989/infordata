package it.ibm.red.business.service;

import java.io.IOException;
import java.sql.Connection;
import java.util.List;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.filenet.DocumentoRedFnDTO;
import it.ibm.red.business.service.facade.IAllegatoFacadeSRV;

/**
 * Interfaccia del service gestione allegati.
 */
public interface IAllegatoSRV extends IAllegatoFacadeSRV {
	
	/**
	 * Converte in allegati filenet.
	 * @param allegatiInput - allegati in input
	 * @param idAoo - id dell'Aoo
	 * @param connection
	 * @return lista di documenti allegati filenet
	 * @throws IOException
	 */
	List<DocumentoRedFnDTO> convertToAllegatiFilenet(List<AllegatoDTO> allegatiInput, Long idAoo, Connection connection) throws IOException;

	/**
	 * Dato un documento estratto da fileNet ne recupera gli allegati e li wrappa in un oggetto AllegatoDTO 
	 * 
	 * Ciascun allegatoDTO conterrà le informazioni minime necessarie ad essere scaricato.
	 * 
	 * mimeType
	 * Nome file
	 * guid
	 * 
	 * @param docMail Un document recuperato da fileNet
	 * @return
	 *  Qualora il documento non abbia allegato ritorna una lista vuota
	 */
	List<AllegatoDTO> extractMinimalAllegati(Document docMail);
}
