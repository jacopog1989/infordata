package it.ibm.red.business.service.concrete;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.core.Document;

import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.constants.Constants.TipologiaCasellaPostale;
import it.ibm.red.business.constants.Constants.TipologiaInvio;
import it.ibm.red.business.constants.Constants.TipologiaMessaggio;
import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.ITipoFileDAO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.ErroreValidazioneDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.MessaggioEmailDTO;
import it.ibm.red.business.dto.MessaggioPostaNpsDTO;
import it.ibm.red.business.dto.NamedStreamDTO;
import it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO;
import it.ibm.red.business.dto.SalvaMessaggioPostaNpsContestoDTO;
import it.ibm.red.business.dto.SegnaturaMessaggioInteropDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.SeverityErroreValidazioneInteropEnum;
import it.ibm.red.business.enums.StatoMailEnum;
import it.ibm.red.business.enums.TipoMessaggioPostaNpsIngressoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.nps.dto.DocumentoNpsDTO;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IEmailBckSRV;
import it.ibm.red.business.service.INpsSRV;
import it.ibm.red.business.service.ISalvaMessaggioPostaNpsSRV;
import it.ibm.red.business.utils.EmailUtils;

/**
 * Abstract del Service che gestisce il salvataggio di un messaggio Posta NPS.
 * @param <T>
 */
@Service
@Component
public abstract class SalvaMessaggioPostaNpsAbstractSRV<T extends MessaggioPostaNpsDTO> extends AbstractService implements ISalvaMessaggioPostaNpsSRV<T> {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = 5498213810766741216L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SalvaMessaggioPostaNpsAbstractSRV.class.getName());

	/**
	 * Massima dimensione oggetto.
	 */
	private static final int OGGETTO_MAX_CHARS = 1200;

	/**
	 * Service.
	 */
	@Autowired
	private IEmailBckSRV emailBckSRV;

	/**
	 * Service.
	 */
	@Autowired
	private INpsSRV npsSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private IAooDAO aooDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private ITipoFileDAO tipoFileDAO;

	/**
	 * Properties.
	 */
	private PropertiesProvider pp;

	/**
	 * Inizializza il service.
	 */
	@PostConstruct
	public void initService() {
		pp = PropertiesProvider.getIstance();
	}

	@Override
	public final String salvaMessaggio(final RichiestaElabMessaggioPostaNpsDTO<T> richiesta, final Connection con) {
		String guidMessaggio = null;
		IFilenetCEHelper fceh = null;
		InputStream isMessaggioPosta = null;
		try {
			final Aoo aoo = aooDAO.getAoo(richiesta.getIdAoo(), con);
			final AooFilenet aooFilenet = aoo.getAooFilenet();

			fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getObjectStore(), aoo.getIdAoo());

			final T messaggioPostaNps = richiesta.getMessaggio();

			if (messaggioPostaNps != null) {
				// Si scarica il content del messaggio di posta da NPS
				final DocumentoNpsDTO contentMessaggioPostaNps = npsSRV.downloadDocumento(aoo.getIdAoo().intValue(), messaggioPostaNps.getIdDocumento(), false);

				isMessaggioPosta = contentMessaggioPostaNps.getInputStream();

				if (contentMessaggioPostaNps != null && isMessaggioPosta != null) {

					// ### Estrazione del messaggio di posta originale da processare
					final MimeMessage messaggioPostaOriginaleMime = getMessaggioPostaOriginale(messaggioPostaNps, isMessaggioPosta, fceh);

					if (messaggioPostaOriginaleMime != null) {
						final List<NamedStreamDTO> contenutiMessaggioPosta = EmailUtils.extractBodyTextAndAttachmentsFromMimeMessage(messaggioPostaOriginaleMime);

						// Si crea il contesto, da cui i service specifici ricavano le informazioni
						// necessarie al recupero dei dati di loro interesse
						final SalvaMessaggioPostaNpsContestoDTO<T> contesto = new SalvaMessaggioPostaNpsContestoDTO<>(richiesta, messaggioPostaOriginaleMime,
								contenutiMessaggioPosta);

						final String mailCasellaPostale = messaggioPostaNps.getCasellaDestinataria().getMail();

						// Si recupera il Message-ID
						final String messageId = getMessageId(contesto);

						// Si recupera il mittente
						final String mittente = getMittente(contesto);

						// Si verifica se il messaggio è già presente su FileNet
						if (!isMessaggioDuplicato(fceh, messageId, mailCasellaPostale)) {

							// Si recupera l'oggetto
							String oggetto = messaggioPostaOriginaleMime.getSubject();
							// Se è più lungo di OGGETTO_MAX_CHARS caratteri, lo si tronca
							if (oggetto != null && oggetto.length() >= OGGETTO_MAX_CHARS) {
								LOGGER.info("Oggetto mail troppo lungo: " + oggetto.length() + " caratteri. La dimensione massima consentita è di " + OGGETTO_MAX_CHARS
										+ " caratteri, si rende necessario un troncamento.");
								oggetto = oggetto.substring(0, OGGETTO_MAX_CHARS);
							}

							// Si recuperano le mail dei destinatari (TO/CC), sotto forma di stringhe
							// separate da ';'
							final String mailDestinatariStringTo = getMailDestinatariStringFromlist(getDestinatariTo(contesto));
							final String mailDestinatariStringCc = getMailDestinatariStringFromlist(getDestinatariCc(contesto));

							// Si aggiorna la tipologia dei destinatari (NON PEC/PEC a.k.a.
							// esterno/certificato)
							aggiornaTipologiaDestinatariMail(messageId, messaggioPostaNps, mailDestinatariStringTo, aoo.getIdAdminAoo(), con);

							// Si recupera la data di invio del messaggio ricevuto, che sarà impostata come
							// data di ricezione
							Date dataRicezione;
							if (messaggioPostaNps.getData() != null) {
								dataRicezione = Date.from(messaggioPostaNps.getData().atZone(ZoneId.systemDefault()).toInstant());
							} else if (messaggioPostaOriginaleMime.getSentDate() != null) {
								dataRicezione = messaggioPostaOriginaleMime.getSentDate();
							} else {
								dataRicezione = new Date();
							}

							// ### Gestione del testo del messaggio e degli allegati (mail) -> START
							FileDTO testoMessaggioFile = null;
							List<AllegatoDTO> allegatiMessaggio = null;

							if (!CollectionUtils.isEmpty(contenutiMessaggioPosta)) {
								allegatiMessaggio = new ArrayList<>();
								AllegatoDTO allegato;

								for (final NamedStreamDTO contenutoMessaggio : contenutiMessaggioPosta) {
									if (!EmailUtils.NOME_NODO_TESTO_MAIL.equals(contenutoMessaggio.getName())) {
										allegato = new AllegatoDTO();
										allegato.setNomeFile(contenutoMessaggio.getFileName());
										allegato.setContent(contenutoMessaggio.getContent());

										allegatiMessaggio.add(allegato);
									} else {
										if (testoMessaggioFile == null || (contenutoMessaggio.getContent() != null && contenutoMessaggio.getContent().length != 0)) {
											testoMessaggioFile = new FileDTO("testo.html", contenutoMessaggio.getContent(), ContentType.HTML);
										}
									}
								}
							}
							// ### Gestione del testo del messaggio e degli allegati (mail) -> START

							// ### Si costruisce il DTO del messaggio di posta NPS da memorizzare su FileNet
							final MessaggioEmailDTO messaggioPostaNpsRedFilenet = costruisciMessaggioPostaNpsRedFilenet(mailDestinatariStringTo, mailDestinatariStringCc,
									mittente, oggetto, messageId, allegatiMessaggio, dataRicezione, messaggioPostaNps);

							// ### Si procede alla memorizzazione del messaggio di posta NPS su FileNet
							final Document messaggioPostaNpsFilenet = fceh.createMailAsDocument(messaggioPostaNpsRedFilenet, mailCasellaPostale, testoMessaggioFile,
									tipoFileDAO.getAll(con));

							guidMessaggio = messaggioPostaNpsFilenet.get_Id().toString(); // GUID del documento FileNet che rappresenta il messaggio di posta NPS

							// ### Gestione della spedizione a seguito di Notifica PEC
							gestisciSpedizione(messageId, messaggioPostaNps, fceh);

							// ### Si esegue il backup del messaggio memorizzandolo nel DB
							salvaBackupMessaggio(messaggioPostaOriginaleMime, messageId, mailCasellaPostale, mittente, con);

						} else {
							throw new RedException("Il messaggio è già presente nel folder FileNet della casella postale, pertanto non è stato elaborato."
									+ " Casella postale: " + mailCasellaPostale + ", Message-ID: " + messageId);
						}

					} else {
						throw new RedException("Errore nell'estrazione del messaggio di posta originale dal mesaggio di posta NPS");
					}
				} else {
					throw new RedException("Errore nel download del contenuto del messaggio di posta NPS");
				}
			} else if (messaggioPostaNps == null) {
				throw new RedException("Errore durante l'estrazione del messaggio di posta NPS dalla richiesta di elaborazione. ID coda: " + richiesta.getIdCoda());
			}
		} catch (final RedException e) {
			LOGGER.error("Si è verificato un errore nel salvataggio del messaggio di posta NPS su FileNet. ID coda: " + richiesta.getIdCoda(), e);
			throw e;
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore nel salvataggio del messaggio di posta NPS su FileNet. ID coda: " + richiesta.getIdCoda(), e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
			closeStream(isMessaggioPosta);
		}

		return guidMessaggio;
	}

	/**
	 * @param isMessaggioPosta
	 */
	private static void closeStream(final InputStream isMessaggioPosta) {
		if (isMessaggioPosta != null) {
			try {
				isMessaggioPosta.close();
			} catch (final IOException e) {
				throw new RedException(e);
			}
		}
	}

	/**
	 * Recupera il messaggio di posta originale da processare. Estrae l'EML del
	 * messaggio originale allegato (postacert.eml) al messaggio di posta recuperato
	 * da NPS (busta di trasporto) se: - la casella destinataria è una PEC, e - il
	 * messaggio di posta NON è considerato da RED come una notifica PEC, e - è
	 * presente almeno un allegato con MIME-Type di tipo "message/*" (* è il
	 * carattere jolly)
	 * 
	 * @param isContentMessaggioPosta
	 * @return MimeMessage
	 */
	private MimeMessage getMessaggioPostaOriginale(final T messaggioPosta, final InputStream isContentMessaggioPosta, final IFilenetCEHelper fceh) {
		MimeMessage messaggioPostaOriginale = null;

		try {
			messaggioPostaOriginale = EmailUtils.getMimeMessageFromInputStream(isContentMessaggioPosta);

			// Il valore della property RETRIEVE_EML_BOOL_INTEROP_POSTA_NPS indica se
			// dobbiamo estrarre o meno
			// l'EML del messaggio originale che è allegato al messaggio di posta (busta di
			// trasporto) recuperato su NPS
			// TRUE -> si procede all'estrazione dell'EML
			if (Boolean.TRUE.equals(Boolean.valueOf(pp.getParameterByKey(PropertiesNameEnum.RETRIEVE_EML_BOOL_INTEROP_POSTA_NPS)))) {

				final Integer tipologiaCasellaPostale = fceh.getTipoCasellaPostaleFromNome(messaggioPosta.getCasellaDestinataria().getMail());

				// Se la casella postale destinataria è PEC e il messaggio non è considerato da
				// RED come una notifica PEC
				if (tipologiaCasellaPostale != null && tipologiaCasellaPostale == TipologiaCasellaPostale.PEC && !EmailUtils.isNotificaPecRed(messaggioPosta.getOggetto())
						&& messaggioPostaOriginale.isMimeType("multipart/*")) {

					final MimeBodyPart contentMessaggioPostaOriginale = getContentPostaOriginale(messaggioPostaOriginale);
					messaggioPostaOriginale = getMsgPostaOriginale(messaggioPostaOriginale, contentMessaggioPostaOriginale);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException("Si è verificato un errore durante l'estrazione del messaggio di posta originale", e);
		}

		return messaggioPostaOriginale;
	}

	/**
	 * @param messaggioPostaOriginale
	 * @param contentMessaggioPostaOriginale
	 * @return messaggio posta originale
	 * @throws MessagingException
	 * @throws IOException
	 */
	private MimeMessage getMsgPostaOriginale(MimeMessage messaggioPostaOriginale, final MimeBodyPart contentMessaggioPostaOriginale) throws MessagingException, IOException {
		try (InputStream isMessaggioPosta = contentMessaggioPostaOriginale.getInputStream()) {

			messaggioPostaOriginale = EmailUtils.getMimeMessageFromInputStream(isMessaggioPosta);
		} catch (final IOException e) {
			if (e.getMessage() != null && "No MimeBodyPart content".equals(e.getMessage())) {
				LOGGER.warn("Messaggio di posta PEO arrivato su casella PEC", e);
			} else {
				throw e;
			}
		}
		return messaggioPostaOriginale;
	}

	/**
	 * 
	 * @param messaggioPostaOriginale
	 * @return content posta originale
	 * @throws MessagingException
	 * @throws IOException
	 */
	private MimeBodyPart getContentPostaOriginale(final MimeMessage messaggioPostaOriginale) throws MessagingException, IOException {
		MimeBodyPart contentMessaggioPostaOriginale = null;
		try {

			contentMessaggioPostaOriginale = EmailUtils.estraiEmlMessaggioOriginaleDaBustaDiTrasporto(messaggioPostaOriginale);
		} catch (final IOException e) {
			if (e.getMessage() != null && "No MimeBodyPart content".equals(e.getMessage())) {
				LOGGER.warn("Messaggio di posta PEO arrivato su casella PEC", e);
			} else {
				throw e;
			}
		}
		return contentMessaggioPostaOriginale;
	}

	/**
	 * @param mailDestinatariStringTo
	 * @param mailDestinatariStringCc
	 * @param mittente
	 * @param oggetto
	 * @param messageId
	 * @param allegatiMessaggio
	 * @param dataRicezione
	 * @param messaggioPostaNps
	 * @return
	 */
	protected MessaggioEmailDTO costruisciMessaggioPostaNpsRedFilenet(final String mailDestinatariStringTo, final String mailDestinatariStringCc, final String mittente,
			final String oggetto, final String messageId, final List<AllegatoDTO> allegatiMessaggio, final Date dataRicezione, final T messaggioPostaNps) {
		// ### Dati della validazione della Segnatura di Interoperabilità -> START
		List<String> erroriValidazioneInterop = null;
		List<String> warningValidazioneInterop = null;

		final SegnaturaMessaggioInteropDTO segnatura = messaggioPostaNps.getSegnatura();
		if (segnatura != null) {
			// Errori e warning di validazione della Segnatura di Interoperabilità
			erroriValidazioneInterop = new ArrayList<>();
			warningValidazioneInterop = new ArrayList<>();

			for (final ErroreValidazioneDTO erroreValidazioneInterop : segnatura.getErroriValidazione()) {

				if (SeverityErroreValidazioneInteropEnum.WARNING.name().equalsIgnoreCase(erroreValidazioneInterop.getSeverity())) {
					warningValidazioneInterop.add(erroreValidazioneInterop.getDescrizione());
				} else if (SeverityErroreValidazioneInteropEnum.ERROR.name().equalsIgnoreCase(erroreValidazioneInterop.getSeverity())) {
					erroriValidazioneInterop.add(erroreValidazioneInterop.getDescrizione());
				}
			}
		}
		// ### Dati della validazione della Segnatura di Interoperabilità -> END

		// ### Logica per il calcolo dello stato del nuovo messaggio di posta
		final StatoMailEnum stato = calcolaStatoMail(messaggioPostaNps.getTipoIngresso());

		return new MessaggioEmailDTO(mailDestinatariStringTo, mailDestinatariStringCc, mittente, oggetto, TipologiaInvio.NUOVOMESSAGGIO, TipologiaMessaggio.TIPO_MSG_EMAIL,
				messageId, !CollectionUtils.isEmpty(allegatiMessaggio), allegatiMessaggio, dataRicezione, getFolderDestinazioneMessaggio(messaggioPostaNps.getTipoIngresso()),
				messaggioPostaNps.getIdMessaggio(), segnatura, erroriValidazioneInterop, warningValidazioneInterop, segnatura != null ? segnatura.getEsitoValidazione() : null,
				stato);
	}

	/**
	 * @param messageId
	 * @param messaggioPosta
	 * @param mittente
	 * @param idAdminAoo
	 * @param con
	 */
	protected void aggiornaTipologiaDestinatariMail(final String messageId, final T messaggioPosta, final String mittente, final Integer idAdminAoo, final Connection con) {
		// Metodo hook che viene sovrascritto SOLO in caso di Notifica PEC
	}

	/**
	 * Gestisce la spedizione in caso di ricezione di Notifica PEC di: - avvenuta
	 * consegna (avvenuta-consegna) - mancata consegna (preavviso-errore-consegna) -
	 * mancata consegna per virus (rilevazione-virus)
	 * 
	 * (Cioè in caso di elemento "consegna" del Daticert presente)
	 * 
	 * @param fceh
	 * @param messaggioPosta
	 * @param datiCert
	 */
	protected void gestisciSpedizione(final String messageId, final T messaggioPosta, final IFilenetCEHelper fceh) {
		// Metodo hook che viene sovrascritto SOLO in caso di Notifica PEC
	}

	/**
	 * @param contesto
	 * @return
	 */
	protected abstract String getMessageId(SalvaMessaggioPostaNpsContestoDTO<T> contesto);

	/**
	 * @param contesto
	 * @return
	 */
	protected abstract String getMittente(SalvaMessaggioPostaNpsContestoDTO<T> contesto);

	/**
	 * @param tipoIngresso
	 * @return
	 */
	protected abstract String getFolderDestinazioneMessaggio(TipoMessaggioPostaNpsIngressoEnum tipoIngresso);

	/**
	 * @param fceh
	 * @param messageId
	 * @param mailCasellaPostale
	 * @return
	 */
	protected boolean isMessaggioDuplicato(final IFilenetCEHelper fceh, final String messageId, final String mailCasellaPostale) {
		return fceh.checkEmailDuplicata(messageId, mailCasellaPostale, null);
	}

	/**
	 * @return
	 */
	protected boolean isNotificaInterop() {
		return false;
	}

	/**
	 * @return
	 */
	protected boolean isNotificaPec() {
		return false;
	}

	/**
	 * @return
	 */
	protected boolean isPec() {
		return false;
	}

	/**
	 * @param mailDestinatari
	 * @return
	 */
	private String getMailDestinatariStringFromlist(final List<String> mailDestinatari) {
		final StringBuilder mailDestinatariStringBuilder = new StringBuilder();

		if (!CollectionUtils.isEmpty(mailDestinatari)) {
			for (final String mailDestinatario : mailDestinatari) {
				mailDestinatariStringBuilder.append(mailDestinatario).append(";");
			}
		}

		return mailDestinatariStringBuilder.toString();
	}

	/**
	 * @param contesto
	 * @return
	 */
	private List<String> getDestinatariTo(final SalvaMessaggioPostaNpsContestoDTO<T> contesto) {
		return getDestinatari(contesto, Message.RecipientType.TO);
	}

	/**
	 * @param contesto
	 * @return
	 */
	private List<String> getDestinatariCc(final SalvaMessaggioPostaNpsContestoDTO<T> contesto) {
		return getDestinatari(contesto, Message.RecipientType.CC);
	}

	/**
	 * @param contesto
	 * @param tipoDestinatari
	 * @return
	 */
	private List<String> getDestinatari(final SalvaMessaggioPostaNpsContestoDTO<T> contesto, final RecipientType tipoDestinatari) {
		List<String> destinatari = null;

		try {
			final Address[] indirizzi = contesto.getMessaggioMime().getRecipients(tipoDestinatari);

			if (indirizzi != null && indirizzi.length > 0) {
				destinatari = EmailUtils.getIndirizziMailDaAddresses(indirizzi);
			}
		} catch (final MessagingException me) {
			LOGGER.error("SalvaMessaggioPEOInteropSRV.getDestinatariTo -> Errore durante il recupero dei destinatari in " + tipoDestinatari
					+ " per la richiesta di elaborazione: " + contesto.getRichiesta().getIdCoda(), me);
		}

		return destinatari;
	}

	/**
	 * @param messaggioMime
	 * @param messageId
	 * @param indirizzoMailCasella
	 * @param mittente
	 * @param con
	 */
	private void salvaBackupMessaggio(final MimeMessage messaggioMime, final String messageId, final String indirizzoMailCasella, final String mittente,
			final Connection con) {
		try {
			emailBckSRV.salvaEmailBck(messaggioMime, messageId, indirizzoMailCasella, mittente, isPec(), isNotificaPec(), con);
		} catch (final Exception e) {
			LOGGER.error("Errore nel salvataggio del backup del messaggio con Message-ID: " + messageId, e);
			throw new RedException("Errore nel salvataggio del backup del messaggio con Message-ID: " + messageId, e);
		}
	}

	/**
	 * Calcola lo stato con cui la mail deve essere memorizzata su FileNet.
	 * 
	 * @param tipoMessaggioIngresso
	 * @return
	 */
	private static StatoMailEnum calcolaStatoMail(final TipoMessaggioPostaNpsIngressoEnum tipoMessaggioIngresso) {
		StatoMailEnum stato = StatoMailEnum.INARRIVO;

		if (TipoMessaggioPostaNpsIngressoEnum.NOTIFICA_ERRORE_PROT_AUTO.equals(tipoMessaggioIngresso)) {
			stato = StatoMailEnum.RIFIUTATA_AUTOMATICAMENTE;
		} else if (tipoMessaggioIngresso != null && tipoMessaggioIngresso.isProtocollazioneAutomatica()) {
			stato = StatoMailEnum.PROTOCOLLATA_AUTOMATICAMENTE;
		}

		return stato;
	}

	/**
	 * @return the pp
	 */
	public PropertiesProvider getPp() {
		return pp;
	}

}