package it.ibm.red.business.dto.filenet;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import it.ibm.red.business.enums.EventTypeEnum;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class EventLogDTO.
 *
 * @author CPIERASC
 * 
 *         DTO per mappare un event log.
 */
public class EventLogDTO implements Serializable {

	/**
	 * Serializzazione.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Event type.
	 */
	private StringBuilder eventType;

	/**
	 * Lista event type.
	 */
	private Collection<String> eventTypeList;

	/**
	 * Workflow number.
	 */
	private String workFlowNumber;

	/**
	 * Data assegnazione.
	 */
	private Date dataAssegnazione;

	/**
	 * Tipo.
	 */
	private String tipo;

	/**
	 * Descrizione.
	 */
	private final String descrizione;

	/**
	 * Data chiusura.
	 */
	private Date dataChiusura;

	/**
	 * Nodo mittente.
	 */
	private String nodoMittente;

	/**
	 * Nodo destinatario.
	 */
	private String nodoDestinatario;

	/**
	 * Nome mittente.
	 */
	private String nomeMitt;

	/**
	 * Cognome mittente.
	 */
	private String cognomeMitt;

	/**
	 * Nome destinatario.
	 */
	private String nomeDest;

	/**
	 * Cognome destinatario.
	 */
	private String cognomeDest;

	/**
	 * Flag event type checked.
	 */
	private boolean eventTypeCheched;

	/**
	 * 
	 */
	private Long idUtenteMittente;
	/**
	 * 
	 */
	private Long idUtenteDestinatario;

	/**
	 * 
	 */
	private Long idNodoMittente;
	/**
	 * 
	 */
	private Long idNodoDestinatario;
	/**
	 * 
	 */
	private Long idTipoassegnazione;

	/**
	 * Costruttore.
	 * 
	 * @param inDescrizione descrizione
	 * @param eventTypes    lista tipi eventi
	 */
	public EventLogDTO(final String inDescrizione, final EventTypeEnum... eventTypes) {
		eventTypeList = new ArrayList<>();
		eventType = new StringBuilder("");
		if (eventTypes != null && eventTypes.length > 0) {
			for (final EventTypeEnum evt : eventTypes) {
				eventType.append(evt.getValue() + ";");
				eventTypeList.add(evt.getValue());
			}
			if (!StringUtils.isNullOrEmpty(eventType.toString())) {
				eventType = new StringBuilder(StringUtils.cutLast(eventType.toString()));
			}
		}
		descrizione = inDescrizione;
	}

	/**
	 * Costruttore.
	 * 
	 * @param inTipoEvento
	 *            tipo evento
	 * @param inDataAssegnazione
	 *            data assegnazione
	 * @param inIdworkflow
	 *            identificativo wf
	 * @param inTipo
	 *            tipo
	 * @param inDescrizione
	 *            descrizione
	 * @param inDatachiusura
	 *            data chiusura
	 * @param inNodoMittente
	 *            nodo mittente descrizione
	 * @param inNodoDestinatario
	 *            nodo destinatario descrizione
	 * @param inNomeMitt
	 *            nome mittente
	 * @param inCognomeMitt
	 *            cognome mittente
	 * @param inNomeDest
	 *            nome destinatario
	 * @param inCognomeDest
	 *            cognome destinatario
	 * @param inIdUtenteMittente
	 * @param inIdUtenteDestinatario
	 * @param inIdNodoMittente
	 * @param inIdNodoDestinatario
	 */
	public EventLogDTO(final String inTipoEvento, final Timestamp inDataAssegnazione, final String inIdworkflow, final String inTipo, final String inDescrizione,
			final Timestamp inDatachiusura, final String inNodoMittente, final String inNodoDestinatario, final String inNomeMitt, final String inCognomeMitt,
			final String inNomeDest, final String inCognomeDest, final Long inIdUtenteMittente, final Long inIdUtenteDestinatario, final Long inIdNodoMittente,
			final Long inIdNodoDestinatario, final Long inIdTipoAssegnazione) {

		eventType = new StringBuilder(inTipoEvento);
		dataAssegnazione = inDataAssegnazione;
		workFlowNumber = inIdworkflow;
		tipo = inTipo;
		descrizione = inDescrizione;
		dataChiusura = inDatachiusura;
		nodoMittente = inNodoMittente;
		nodoDestinatario = inNodoDestinatario;
		nomeMitt = inNomeMitt;
		cognomeMitt = inCognomeMitt;
		nomeDest = inNomeDest;
		cognomeDest = inCognomeDest;
		idUtenteMittente = inIdUtenteMittente;
		idUtenteDestinatario = inIdUtenteDestinatario;
		idNodoMittente = inIdNodoMittente;
		idNodoDestinatario = inIdNodoDestinatario;
		idTipoassegnazione = inIdTipoAssegnazione;
	}

	/**
	 * Getter.
	 * 
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Getter.
	 * 
	 * @return nodo mittente
	 */
	public String getNodoMittente() {
		return nodoMittente;
	}

	/**
	 * Getter.
	 * 
	 * @return event type checked
	 */
	public final boolean isEventTypeCheched() {
		return eventTypeCheched;
	}

	/**
	 * Getter.
	 * 
	 * @return tipo evento
	 */
	public final String getEventType() {
		String output = "";
		if (eventType != null) {
			output = eventType.toString();
		}
		return output;
	}

	/**
	 * Getter.
	 * 
	 * @return workflow number
	 */
	public final String getWorkFlowNumber() {
		return workFlowNumber;
	}

	/**
	 * Getter.
	 * 
	 * @return event type list
	 */
	public final Collection<String> getEventTypeList() {
		return eventTypeList;
	}

	/**
	 * Getter.
	 * 
	 * @return name
	 */
	public final String getTipo() {
		return tipo;
	}

	/**
	 * Getter.
	 * 
	 * @return data assegnazione
	 */
	public final String getDataAssegnazione() {
		String output = "";
		if (dataAssegnazione != null) {
			output = new SimpleDateFormat("dd/MM/yy HH:mm:ss").format(dataAssegnazione);
		}
		return output;
	}

	/**
	 * Getter.
	 * 
	 * @return data chiusura
	 */
	public final String getDataChiusura() {
		String output = "";
		if (dataChiusura != null) {
			output = new SimpleDateFormat("dd/MM/yy HH:mm:ss").format(dataChiusura);
		}
		return output;
	}

	/**
	 * Setter.
	 * 
	 * @param inEventTypeCheched event type checked
	 */
	public final void setEventTypeCheched(final boolean inEventTypeCheched) {
		this.eventTypeCheched = inEventTypeCheched;
	}

	/**
	 * Metodo che verifica se una stringa è null, se non lo è la restituisce,
	 * altrimenti ritorna una stringa vuota.
	 * 
	 * @param prop valore della proprietà
	 * @return valore della proprietà verificata
	 */
	private static String getProperty(final String prop) {
		return getProperty(prop, null);
	}

	/**
	 * Metodo che verifica se una stringa è null, se non lo è la restituisce
	 * fornendo eventualmente un prefisso, altrimenti ritorna una stringa vuota.
	 *
	 * @param prop   valore della proprietà
	 * @param suffix suffisso
	 * @return valore della proprietà verificata
	 */
	private static String getProperty(final String prop, final String suffix) {
		String output = "";
		if (prop != null) {
			output += prop;
			if (!StringUtils.isNullOrEmpty(suffix)) {
				output += suffix;
			}
		}
		return output;
	}

	/**
	 * Metodo per la conversione in stringa.
	 * 
	 * @param withDate booleano che indica se fornire anche la data assegnazione.
	 * @return stringa
	 */
	public final String toStringEventLog(final boolean withDate) {
		String output = "";
		if (withDate && dataAssegnazione != null) {
			output += getDataAssegnazione() + " ";
		}
		output += getProperty(nodoMittente, ",") + " " + getProperty(nomeMitt) + " " + getProperty(cognomeMitt) + " " + getProperty(descrizione) + " "
				+ getProperty(nodoDestinatario) + " " + getProperty(nomeDest) + " " + getProperty(cognomeDest);
		return output;
	}

	/**
	 * Recupera il nodo destinatario.
	 * 
	 * @return nodo destinatario
	 */
	public String getNodoDestinatario() {
		return nodoDestinatario;
	}

	/**
	 * Recupera il nome del mittente.
	 * 
	 * @return nome del mittente
	 */
	public String getNomeMitt() {
		return nomeMitt;
	}

	/**
	 * Recupera il cognome del mittente.
	 * 
	 * @return cognome del mittente
	 */
	public String getCognomeMitt() {
		return cognomeMitt;
	}

	/**
	 * Recupera il nome del destinatario.
	 * 
	 * @return nome del destinatario
	 */
	public String getNomeDest() {
		return nomeDest;
	}

	/**
	 * Recupera il cognome del destinatario.
	 * 
	 * @return cognome del destinatario
	 */
	public String getCognomeDest() {
		return cognomeDest;
	}

	/**
	 * Recupera l'id dell'utente mittente.
	 * 
	 * @return id dell'utente mittente
	 */
	public Long getIdUtenteMittente() {
		return idUtenteMittente;
	}

	/**
	 * Recupera l'id dell'utente destinatario.
	 * 
	 * @return id dell'utente destinatario
	 */
	public Long getIdUtenteDestinatario() {
		return idUtenteDestinatario;
	}

	/**
	 * Recupera l'id del nodo mittente.
	 * 
	 * @return id del nodo mittente
	 */
	public Long getIdNodoMittente() {
		return idNodoMittente;
	}

	/**
	 * Recupera l'id del nodo destinatario.
	 * 
	 * @return id del nodo destinatario
	 */
	public Long getIdNodoDestinatario() {
		return idNodoDestinatario;
	}

	/**
	 * Recupera l'id del tipo di assegnazione.
	 * 
	 * @return id del tipo di assegnazione
	 */
	public Long getIdTipoassegnazione() {
		return idTipoassegnazione;
	}
}
