package it.ibm.red.business.service.concrete;

import java.io.ByteArrayInputStream;
import java.math.RoundingMode;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.google.common.math.DoubleMath;

import it.ibm.red.business.dao.IAssegnazioneAutomaticaCapSpesaDAO;
import it.ibm.red.business.dto.AssegnatarioDTO;
import it.ibm.red.business.dto.AssegnazioneAutomaticaCapSpesaDTO;
import it.ibm.red.business.dto.CapitoloSpesaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAssegnazioneAutomaticaCapSpesaSRV;
import it.ibm.red.business.service.ICapitoloSpesaSRV;
import it.ibm.red.business.service.IUtenteSRV;

/**
 * The Class AssegnazioneAutomaticaSRV.
 *
 * @author mcrescentini
 * 
 *         Servizio gestione assegnazioni automatiche.
 */
@Service
@Component
public class AssegnazioneAutomaticaCapSpesaSRV extends AbstractService implements IAssegnazioneAutomaticaCapSpesaSRV {

	private static final long serialVersionUID = -5049519354032869746L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AssegnazioneAutomaticaCapSpesaSRV.class.getName());
	
	/**
	 * DAO assegnazione automatica.
	 */
	@Autowired
	private IAssegnazioneAutomaticaCapSpesaDAO assegnazioneAutomaticaDAO;
	
	/**
	 * Service capitolo di spesa.
	 */
	@Autowired
	private ICapitoloSpesaSRV capitoloSpesaSRV;
	
	/**
	 * Service utente.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;
	/* (non-Javadoc)
	 * @see it.ibm.red.business.service.IAssegnazioneAutomaticaSRV#get(java.lang.String, java.lang.Long, java.sql.Connection)
	 */
	@Override
	public final AssegnazioneAutomaticaCapSpesaDTO get(final String codiceCapitoloSpesa, final Long idAoo, final Connection connection) {
		try {
			
			return assegnazioneAutomaticaDAO.get(codiceCapitoloSpesa, idAoo, null, connection);
			
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero dell'asegnazione automatica per il capitolo " + codiceCapitoloSpesa 
					+ " e l'AOO: " + idAoo, e);
			throw new RedException("Errore durante il recupero delle assegnazioni automatiche");
		} 
	}
	
	
	/* (non-Javadoc)
	 * @see it.ibm.red.business.service.facade.IAssegnazioneAutomaticaFacadeSRV#getByIdAoo(java.lang.Long)
	 */
	@Override
	public final List<AssegnazioneAutomaticaCapSpesaDTO> getByIdAoo(final Long idAoo) {
		final List<AssegnazioneAutomaticaCapSpesaDTO> assegnazioniAutomatiche = new ArrayList<>();
		Connection con = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			
			assegnazioniAutomatiche.addAll(assegnazioneAutomaticaDAO.getByIdAoo(idAoo, con));
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero delle assegnazioni automatiche per l'AOO: " + idAoo, e);
			throw new RedException("Errore durante il recupero delle assegnazioni automatiche");
		} finally {
			closeConnection(con);
		}
		
		return assegnazioniAutomatiche;
	}


	/* (non-Javadoc)
	 * @see it.ibm.red.business.service.facade.IAssegnazioneAutomaticaFacadeSRV#elimina(
	 * it.ibm.red.business.dto.AssegnazioneAutomaticaDTO, java.lang.Long)
	 */
	@Override
	public final void elimina(final AssegnazioneAutomaticaCapSpesaDTO assegnazioneAutomatica, final Long idAoo) {
		Connection con = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			
			assegnazioneAutomaticaDAO.elimina(
					assegnazioneAutomatica.getCapitoloSpesa().getCodice(), idAoo, 
					assegnazioneAutomatica.getAssegnatario().getIdUfficio(), con);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'eliminazione di un'assegnazione automatica per l'AOO: " + idAoo, e);
			throw new RedException("Errore durante l'eliminazione dell'assegnazione automatica");
		} finally {
			closeConnection(con);
		}
	}
	

	/* (non-Javadoc)
	 * @see it.ibm.red.business.service.facade.IAssegnazioneAutomaticaFacadeSRV#eliminaTutte(java.lang.Long)
	 */
	@Override
	public final void eliminaTutte(final Long idAoo) {
		Connection con = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			
			assegnazioneAutomaticaDAO.eliminaTutteByIdAoo(idAoo, con);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'eliminazione massiva di assegnazioni automatiche per l'AOO: " + idAoo, e);
			throw new RedException("Errore durante l'eliminazione massiva delle assegnazioni automatiche");
		} finally {
			closeConnection(con);
		}
	}
	
	
	@Override
	public final void inserisci(final String codiceCapitoloSpesa, final Long idAoo, final Long idUfficio, final Long idUtente) {
		Connection con = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), true);
			
			// Si elimina l'eventuale assegnazione automatica preesistente per la tripletta CAPITOLO - AOO - UFFICIO
			assegnazioneAutomaticaDAO.elimina(codiceCapitoloSpesa, idAoo, idUfficio, con);
			
			// Si inserisce la nuova assegnazione automatica
			assegnazioneAutomaticaDAO.inserisci(codiceCapitoloSpesa, idAoo, idUfficio, idUtente, con);
			
			commitConnection(con);
		} catch (final Exception e) {
			rollbackConnection(con);
			LOGGER.error("Si è verificato un errore durante l'inserimento di un'assegnazione automatica per l'AOO: " + idAoo, e);
			throw new RedException("Errore durante l'inserimento dell'assegnazione automatica");
		} finally {
			closeConnection(con);
		}
	}


	/* (non-Javadoc)
	 * @see it.ibm.red.business.service.facade.IAssegnazioneAutomaticaFacadeSRV#inserisciMultiple(java.util.List, java.lang.Long)
	 */
	@Override
	public final void inserisciMultiple(final List<AssegnazioneAutomaticaCapSpesaDTO> assegnazioniAutomatiche, final Long idAoo) {
		if (!CollectionUtils.isEmpty(assegnazioniAutomatiche)) {
			Connection con = null;
			
			try {
				con = setupConnection(getDataSource().getConnection(), true);
				
				// Si inseriscono/aggiornano i capitoli di spesa
				capitoloSpesaSRV.inserisciMultipli(
						assegnazioniAutomatiche.stream().map(assAuto -> assAuto.getCapitoloSpesa()).collect(Collectors.toList()), 
						idAoo, con);
				
				// Si eliminano tutte le assegnazioni automatiche preesistenti per la tripletta CAPITOLO SPESA - AOO - UFFICIO
				assegnazioneAutomaticaDAO.eliminaMultiple(assegnazioniAutomatiche, idAoo, con);
				
				// Si procede con l'inserimento massivo delle assegnazioni automatiche
				assegnazioneAutomaticaDAO.inserisciMultiple(assegnazioniAutomatiche, idAoo, con);
				
				commitConnection(con);
			} catch (final Exception e) {
				rollbackConnection(con);
				LOGGER.error("Si è verificato un errore durante l'inserimento massivo di assegnazioni automatiche per l'AOO: "
						+ idAoo, e);
				throw new RedException("Errore durante l'inserimento massivo delle assegnazioni automatiche");
			} finally {
				closeConnection(con);
			}
		}
	}
	
	
	/* (non-Javadoc)
	 * @see it.ibm.red.business.service.facade.IAssegnazioneAutomaticaFacadeSRV#estraiEValidaAssegnazioneAutomaticheDaExcel(
	 * byte[], java.lang.String, java.lang.Long)
	 */
	@Override
	public final List<AssegnazioneAutomaticaCapSpesaDTO> estraiEValidaAssegnazioneAutomaticheDaExcel(final byte[] file, 
			final String primaCellaStr, final Long idAoo) {
		final List<AssegnazioneAutomaticaCapSpesaDTO> assegnazioniAutomaticheOutput = new ArrayList<>();
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			
			// Si estraggono le assegnazioni automatiche dal file Excel:
			// durante l'estrazione viene effettuata una prima validazione sull'esistenza dei dati minimi obbligatori
			// (codice capitolo spesa e ufficio assegnatario)
			final List<AssegnazioneAutomaticaCapSpesaDTO> assegnazioniAutomaticheEstratte = estraiAssegnazioneAutomaticheDaExcel(file, primaCellaStr);
			
			if (!CollectionUtils.isEmpty(assegnazioniAutomaticheEstratte)) {
				// Le assegnazioni che NON superano questo primo controllo vengono inserite nella lista in output,
				// che deve contenere tutte le assegnazioni estratte (valide e non)
				AssegnazioneAutomaticaCapSpesaDTO assegnazioneAutomatica = null;
				final Iterator<AssegnazioneAutomaticaCapSpesaDTO> itAssegnazioniAutomaticheEstratte = assegnazioniAutomaticheEstratte.iterator();
				while (itAssegnazioniAutomaticheEstratte.hasNext()) {
					assegnazioneAutomatica = itAssegnazioniAutomaticheEstratte.next();
					
					if (!assegnazioneAutomatica.isValida()) {
						assegnazioniAutomaticheOutput.add(assegnazioneAutomatica);
						itAssegnazioniAutomaticheEstratte.remove();
					}
				}
				
				// Le assegnazioni che sono risultate valide a questo primo controllo vengono sottoposte a una seconda validazione,
				// che verifica la validità di ciascuna assegnazione attraverso un confronto con i dati presenti nel DB.
				// Infine, vengono aggiunte anch'esse alla lista di assegnazoni automatiche restituita in output
				for (final AssegnazioneAutomaticaCapSpesaDTO assAuto : assegnazioniAutomaticheEstratte) {
					UtenteDTO utente = null;
					if (assAuto.getAssegnatario().getIdUtente() != null) {
						utente = utenteSRV.getById(assAuto.getAssegnatario().getIdUtente());
					}
					
					// Se non esiste un utente con l'id inserito, viene eliminato e l'assegnazione viene considerata a livello ufficio
					if (utente == null) {
						assAuto.getAssegnatario().setIdUtente(null);
					}
					
					final boolean isAssegnazioneAutomaticaValida = assegnazioneAutomaticaDAO.verificaAssegnazioneAutomatica(idAoo, utente,
							assAuto.getAssegnatario().getDescrizioneUfficio(), connection);
					
					assAuto.setValida(isAssegnazioneAutomaticaValida);
					assegnazioniAutomaticheOutput.add(assAuto);
				}
				
				// Ordinamento
				assegnazioniAutomaticheOutput.sort(Comparator.comparing(AssegnazioneAutomaticaCapSpesaDTO::isValida));
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante l'estrazione e la verifica delle assegnazioni automatiche presenti nel file", e);
			throw new RedException("Errore durante l'estrazione e la verifica delle assegnazioni automatiche presenti nel file");
		} finally {
			closeConnection(connection);
		}
		
		return assegnazioniAutomaticheOutput;
	}
	
	
	/**
	 * @param file
	 * @param primaCellaStr
	 * @return
	 */
	private List<AssegnazioneAutomaticaCapSpesaDTO> estraiAssegnazioneAutomaticheDaExcel(final byte[] file, final String primaCellaStr) {
		final List<AssegnazioneAutomaticaCapSpesaDTO> assegnazioniAutomatiche = new ArrayList<>();
		
		try (Workbook workbook = new HSSFWorkbook(new ByteArrayInputStream(file))) {
			
			// Primo foglio
			final Sheet foglio = workbook.getSheetAt(0);
			
			// Si imposta la cella di partenza
			CellAddress primaCella = null;
			if (StringUtils.isNotBlank(primaCellaStr)) {
				primaCella = new CellAddress(primaCellaStr);
			// Se non è presente, si recupera la properties dal DB
			} else if (StringUtils.isNotBlank(
					PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ASSEGNAZIONI_AUTOMATICHE_XLS_CELLA_INIZIO))) {
				primaCella = new CellAddress(
						PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ASSEGNAZIONI_AUTOMATICHE_XLS_CELLA_INIZIO));
			// Se non è presente, si parte dalla prima cella del foglio (riga 0, colonna 0)
			} else {
				primaCella = new CellAddress(0, 0);
			}
			
			// 			COLONNA 1       |				COLONNA 2				|            COLONNA 3
			// CODICE CAPITOLO SPESA 	|  DESCRIZIONE CAPITOLO SPESA UFFICIO  	|  ANNO FINANZIARIO CAPITOLO SPESA 
			//  		COLONNA 4      	|				COLONNA 5				|            COLONNA 6
			// ID UFFICIO ASSEGNATARIO 	| 		ID UTENTE ASSEGNATARIO			| 		FLAG SENTENZA ASSOCIATA
			final int rigaInizioIndex = primaCella.getRow();
			final int rigaFineIndex = foglio.getLastRowNum();
			final int colonnaInizioIndex = primaCella.getColumn();
			
			AssegnazioneAutomaticaCapSpesaDTO assegnazioneAutomatica;
			CapitoloSpesaDTO capitoloSpesa;
			String codiceCapitoloSpesa;
			String descrizioneCapitoloSpesa;
			Integer annoCapitoloSpesa;
			String descrizioneUfficioAssegnatario;
			Integer idUtenteAssegnatario;
			Boolean flagSentenzaAssociata;
			boolean isAssegnazioneValida;
			Cell cella;
			Row riga;
			for (int indiceRiga = rigaInizioIndex; indiceRiga <= rigaFineIndex; indiceRiga++) {
				assegnazioneAutomatica = new AssegnazioneAutomaticaCapSpesaDTO();
				assegnazioneAutomatica.setAssegnatario(new AssegnatarioDTO());
				capitoloSpesa = null;
				codiceCapitoloSpesa = null;
				descrizioneCapitoloSpesa = null;
				annoCapitoloSpesa = null;
				descrizioneUfficioAssegnatario = null;
				idUtenteAssegnatario = null;
				flagSentenzaAssociata = null;
				isAssegnazioneValida = false;
				
				riga = foglio.getRow(indiceRiga);
				
				cella = riga.getCell(colonnaInizioIndex);
				if (cella != null && CellType.STRING.equals(cella.getCellTypeEnum()) && StringUtils.isNotBlank(cella.getStringCellValue())) {
					codiceCapitoloSpesa = cella.getStringCellValue();
				}
				
				cella = riga.getCell(colonnaInizioIndex + 1);
				if (cella != null && CellType.STRING.equals(cella.getCellTypeEnum()) && StringUtils.isNotBlank(cella.getStringCellValue())) {
					descrizioneCapitoloSpesa = cella.getStringCellValue();
				}

				cella = riga.getCell(colonnaInizioIndex + 2);
				if (cella != null && CellType.NUMERIC.equals(cella.getCellTypeEnum()) && DoubleMath.isMathematicalInteger(cella.getNumericCellValue())) {
					annoCapitoloSpesa = DoubleMath.roundToInt(cella.getNumericCellValue(), RoundingMode.UNNECESSARY);
				}
				
				cella = riga.getCell(colonnaInizioIndex + 3);
				if (cella != null && CellType.STRING.equals(cella.getCellTypeEnum()) && StringUtils.isNotBlank(cella.getStringCellValue())) {
					descrizioneUfficioAssegnatario = cella.getStringCellValue();
					assegnazioneAutomatica.getAssegnatario().setDescrizioneUfficio(descrizioneUfficioAssegnatario);
				}
				
				cella = riga.getCell(colonnaInizioIndex + 4);
				if (cella != null && CellType.NUMERIC.equals(cella.getCellTypeEnum()) && DoubleMath.isMathematicalInteger(cella.getNumericCellValue())) {
					idUtenteAssegnatario = DoubleMath.roundToInt(cella.getNumericCellValue(), RoundingMode.UNNECESSARY);
					assegnazioneAutomatica.getAssegnatario().setIdUtente(idUtenteAssegnatario.longValue());
				}
				
				cella = riga.getCell(colonnaInizioIndex + 5);
				if (cella != null && CellType.NUMERIC.equals(cella.getCellTypeEnum()) && DoubleMath.isMathematicalInteger(cella.getNumericCellValue())) {
					flagSentenzaAssociata = BooleanFlagEnum.SI.getIntValue().equals(
							DoubleMath.roundToInt(cella.getNumericCellValue(), RoundingMode.UNNECESSARY));
				}
				
				capitoloSpesa = new CapitoloSpesaDTO(codiceCapitoloSpesa, descrizioneCapitoloSpesa, annoCapitoloSpesa, flagSentenzaAssociata);
				assegnazioneAutomatica.setCapitoloSpesa(capitoloSpesa);
				
				if (StringUtils.isNotBlank(codiceCapitoloSpesa) && StringUtils.isNotBlank(descrizioneCapitoloSpesa) && annoCapitoloSpesa != null
						&& flagSentenzaAssociata != null && StringUtils.isNotBlank(descrizioneUfficioAssegnatario)) {
					isAssegnazioneValida = true;
				}
				assegnazioneAutomatica.setValida(isAssegnazioneValida);
				
				assegnazioniAutomatiche.add(assegnazioneAutomatica);
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la lettura del file Excel contenente le assegnazioni automatiche", e);
			throw new RedException("Errore durante la lettura del file contenente le assegnazioni automatiche");
		}
		
		return assegnazioniAutomatiche;
	}
}