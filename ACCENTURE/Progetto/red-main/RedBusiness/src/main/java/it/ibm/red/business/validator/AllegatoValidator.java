package it.ibm.red.business.validator;

import java.sql.Connection;
import java.util.Collection;
import java.util.List;

import org.springframework.util.CollectionUtils;

import it.ibm.red.business.dao.ITipoFileDAO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.ResponsabileCopiaConformeDTO;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.SalvaDocumentoErroreEnum;
import it.ibm.red.business.persistence.model.TipoFile;

/**
 * Classe Validator per gli Allegati.
 */
public class AllegatoValidator extends AbstractValidator<AllegatoDTO, SalvaDocumentoErroreEnum> {

	/**
	 * Responsabile copia conforme.
	 */
	private final ResponsabileCopiaConformeDTO responsabileCopiaConforme;

	/**
	 * Tipo file aoo.
	 */
	private final ITipoFileDAO tipoFileDAO;

	/**
	 * Connessione.
	 */
	private final Connection connection;

	/**
	 * Estensione file principale.
	 */
	private String extensionFilePrincipale;

	/**
	 * Costruttore di classe.
	 * 
	 * @param responsabileCopiaConforme
	 * @param tipoFileDAO
	 * @param extensionFilePrincipale
	 * @param connection
	 */
	public AllegatoValidator(final ResponsabileCopiaConformeDTO responsabileCopiaConforme, final ITipoFileDAO tipoFileDAO, final String extensionFilePrincipale,
			final Connection connection) {
		super();
		this.responsabileCopiaConforme = responsabileCopiaConforme;
		this.tipoFileDAO = tipoFileDAO;
		this.connection = connection;
		this.extensionFilePrincipale = extensionFilePrincipale;
	}

	/**
	 * @see it.ibm.red.business.validator.AbstractValidator#validaTutti(java.util.Collection).
	 */
	@Override
	public List<SalvaDocumentoErroreEnum> validaTutti(final Collection<AllegatoDTO> allegati) {
		getErrori().clear(); // Si puliscono gli errori

		boolean isAllegatoCopiaConformePresente = false;
		Long idUfficioCopiaConforme = null;
		Long idUtenteCopiaConforme = null;

		if (!CollectionUtils.isEmpty(allegati)) {
			boolean isMantieniFormatoAndDaFirmareAndNotPdf = false;
			boolean isStampigliaSiglaChecked = false;
			boolean erroreStampGiaUsato = false;

			for (final AllegatoDTO allegato : allegati) {

				if (FormatoAllegatoEnum.ELETTRONICO.equals(allegato.getFormatoSelected()) || FormatoAllegatoEnum.FIRMATO_DIGITALMENTE.equals(allegato.getFormatoSelected())) {
					// capisco se c'è almeno un allegato non pdf che sia da firmare e da mantenere
					// in formato originale
					if (Boolean.TRUE.equals(allegato.getFormatoOriginale()) && Boolean.TRUE.equals(allegato.isDaFirmareBoolean())
							&& !("pdf".equalsIgnoreCase(allegato.getMimeType()) || "application/pdf".equalsIgnoreCase(allegato.getMimeType()))) {
						isMantieniFormatoAndDaFirmareAndNotPdf = true;
					}
					// capisco se c'è almeno un allegato per il quale è richiesto il segno grafico
					if (allegato.getStampigliaturaSigla()) {
						isStampigliaSiglaChecked = true;
					}

					// Controlla se le condizioni sulla stampigliatura del segno grafico sono valide
					// in caso diamo un errore
					if (isStampigliaSiglaChecked && isMantieniFormatoAndDaFirmareAndNotPdf && !erroreStampGiaUsato) {
						getErrori().add(SalvaDocumentoErroreEnum.PDF_MANTIENI_FORMATO_ERROR_SIGLA_ALLEGATO);
						erroreStampGiaUsato = true;
					}
					// Nei nuovi allegati, il content dell'allegato deve essere presente
					if (allegato.getGuid() == null && allegato.getContent() == null) {
						getErrori().add(SalvaDocumentoErroreEnum.ALLEGATO_CONTENT_NON_PRESENTE);
					}

					// La tipologia del documento allegato deve essere presente
					if (allegato.getIdTipologiaDocumento() == null || allegato.getIdTipologiaDocumento() < 0) {
						getErrori().add(SalvaDocumentoErroreEnum.ALLEGATO_TIPOLOGIA_DOC_NON_PRESENTE);
					}

					// Se l'allegato è per Copia Conforme
					if (Boolean.TRUE.equals(allegato.getCopiaConforme())) {
						isAllegatoCopiaConformePresente = true;
						// Il responsabile per la Copia Conforme deve essere lo stesso per tutti gli
						// allegati
						if (idUfficioCopiaConforme == null || idUtenteCopiaConforme == null) {
							idUfficioCopiaConforme = allegato.getIdUfficioCopiaConforme();
							idUtenteCopiaConforme = allegato.getIdUtenteCopiaConforme();
						} else if (!idUfficioCopiaConforme.equals(allegato.getIdUfficioCopiaConforme())
								|| !idUtenteCopiaConforme.equals(allegato.getIdUtenteCopiaConforme())) {
							getErrori().add(SalvaDocumentoErroreEnum.ALLEGATO_RESP_COPIA_CONFORME_NON_VALIDO);
						}

						// Il Content Type dell'allegato deve essere tra quelli ammessi per la Copia
						// Conforme
						if (!isContentTypeCopiaConforme(allegato.getMimeType())) {
							getErrori().add(SalvaDocumentoErroreEnum.ALLEGATO_MIMETYPE_COPIA_CONFORME_NON_VALIDO);
						}

					}

				}
				if (allegato.isMantieniFormatoOriginale() && allegato.getStampigliaturaSigla()) {
					getErrori().add(SalvaDocumentoErroreEnum.STAMPIGLIATURA_SIGLA_ALLEGATO);
				}

				if (extensionFilePrincipale != null && ("p7m".equalsIgnoreCase(extensionFilePrincipale) || "p7s".equalsIgnoreCase(extensionFilePrincipale))
						&& allegato.getStampigliaturaSigla()) {
					getErrori().add(SalvaDocumentoErroreEnum.P7M_ERROR_SIGLA_ALLEGATO);
				}

				if (Boolean.TRUE.equals(allegato.getFirmaVisibile()) && !FormatoAllegatoEnum.ELETTRONICO.equals(allegato.getFormatoSelected())) {
					getErrori().add(SalvaDocumentoErroreEnum.FIRMA_VISIBILE_FORMATO_ELETTRONICO_FIRMATO_DIGITALMENTE);
				}

				if (Boolean.TRUE.equals(allegato.getFirmaVisibile()) && allegato.isMantieniFormatoOriginale()) {
					getErrori().add(SalvaDocumentoErroreEnum.FIRMA_VISIBILE_MANTIENI_FORMATO_ORIGINALE);
				}
			}

			extensionFilePrincipale = null;

			if (responsabileCopiaConforme != null) {
				if (!isAllegatoCopiaConformePresente) {
					getErrori().add(SalvaDocumentoErroreEnum.ALLEGATO_COPIA_CONFORME_NON_PRESENTE);
				} else {
					final Long idUfficioRespCopiaConforme = responsabileCopiaConforme.getIdNodo();
					final Long idUtenteRespCopiaConforme = responsabileCopiaConforme.getIdUtente();

					if (!idUfficioRespCopiaConforme.equals(idUfficioCopiaConforme) || !idUtenteRespCopiaConforme.equals(idUtenteCopiaConforme)) {
						getErrori().add(SalvaDocumentoErroreEnum.DOC_RESP_COPIA_CONFORME_NEGLI_ALLEGATI);
					}
				}
			}
		}

		return getErrori();
	}

	/**
	 * @see it.ibm.red.business.validator.AbstractValidator#valida(java.lang.Object).
	 */
	@Override
	public List<SalvaDocumentoErroreEnum> valida(final AllegatoDTO allegato) {
		getErrori().clear(); // Si puliscono gli errori

		if (FormatoAllegatoEnum.ELETTRONICO.equals(allegato.getFormatoSelected()) || FormatoAllegatoEnum.FIRMATO_DIGITALMENTE.equals(allegato.getFormatoSelected())) {

			// Il content dell'allegato deve essere presente
			if (allegato.getContent() == null) {
				getErrori().add(SalvaDocumentoErroreEnum.ALLEGATO_CONTENT_NON_PRESENTE);
			}

			// La tipologia del documento allegato deve essere presente
			if (allegato.getIdTipologiaDocumento() == null || allegato.getIdTipologiaDocumento() < 0) {
				getErrori().add(SalvaDocumentoErroreEnum.ALLEGATO_TIPOLOGIA_DOC_NON_PRESENTE);
			}

			if (Boolean.TRUE.equals(allegato.getCopiaConforme()) && !isContentTypeCopiaConforme(allegato.getMimeType())) {
				// Se l'allegato è per Copia Conforme
				// Il Content Type dell'allegato deve essere tra quelli ammessi per la Copia
				// Conforme
				getErrori().add(SalvaDocumentoErroreEnum.ALLEGATO_MIMETYPE_COPIA_CONFORME_NON_VALIDO);
			}
		}

		return getErrori();
	}

	private boolean isContentTypeCopiaConforme(final String contentType) {
		boolean contentTypeCopiaConforme = false;

		final Collection<TipoFile> tipiFileCopiaConforme = tipoFileDAO.getTipiFileCopiaConforme(connection);
		if (!CollectionUtils.isEmpty(tipiFileCopiaConforme)) {
			for (final TipoFile tipoFileCopiaConforme : tipiFileCopiaConforme) {
				if (tipoFileCopiaConforme.getMimeType().equalsIgnoreCase(contentType)) {
					contentTypeCopiaConforme = true;
					break;
				}
			}
		}

		return contentTypeCopiaConforme;
	}
}