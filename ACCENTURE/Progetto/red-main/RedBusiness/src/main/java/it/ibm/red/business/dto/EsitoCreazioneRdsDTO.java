package it.ibm.red.business.dto;

import java.util.Date;

/**
 * DTO esito creazioen Rds.
 */
public class EsitoCreazioneRdsDTO extends EsitoOperazioneDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Data apertura RDS.
	 */
	private Date dataAperturaRds;

	/**
	 * Numero RDS.
	 */
	private String numeroRds;

	/**
	 * Costruttore.
	 * 
	 * @param wobNumber
	 * @param esito
	 * @param descrizioneEsito descrizione dell'esito
	 * @param dataAperturaRds  data di apertura Rds
	 * @param numeroRds        numero Rds
	 */
	public EsitoCreazioneRdsDTO(final String wobNumber, final boolean esito, final String descrizioneEsito, final Date dataAperturaRds, final String numeroRds) {
		this(wobNumber, esito, descrizioneEsito);
		this.dataAperturaRds = dataAperturaRds;
		this.numeroRds = numeroRds;
	}

	/**
	 * Costruttore.
	 * 
	 * @param wobNumber
	 * @param esito
	 * @param descrizioneEsito descrizione dell'esito
	 */
	public EsitoCreazioneRdsDTO(final String wobNumber, final boolean esito, final String descrizioneEsito) {
		super(wobNumber, esito, descrizioneEsito);
	}

	/**
	 * Recupera la data di apertura Rds.
	 * 
	 * @return data di apertura Rds
	 */
	public Date getDataAperturaRds() {
		return dataAperturaRds;
	}

	/**
	 * Recupera il numero Rds.
	 * 
	 * @return numero Rds
	 */
	public String getNumeroRds() {
		return numeroRds;
	}

	/**
	 * @see it.ibm.red.business.dto.EsitoDTO#getNote().
	 */
	@Override
	public String getNote() {
		if (isEsito() && getNumeroRds() != null) {
			return "RdS " + getNumeroRds() + " correttamente aperta su Siebel. Il documento è stato spostato nella coda in sospeso dell'utente.";
		} else {
			return super.getNote();
		}
	}

}