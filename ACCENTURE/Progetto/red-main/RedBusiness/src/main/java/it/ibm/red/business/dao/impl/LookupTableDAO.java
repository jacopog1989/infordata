/**
 * 
 */
package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.ILookupTableDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.LookupTable;

/**
 * @author APerquoti
 *
 */
@Repository
public class LookupTableDAO extends AbstractDAO implements ILookupTableDAO {

	private static final String DISPLAYNAME = "DISPLAYNAME";

	private static final String SELETTORE = "SELETTORE";

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 7328155522195248283L;

	/**
	 * Messaggio errore salvataggio lookup table.
	 */
	private static final String ERROR_SALVATAGGIO_LOOKUPTABLE_MSG = "Errore in fase di salvataggio della lookuptable ";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(LookupTableDAO.class.getName());

	/**
	 * Restituisce la lista dei selettori validi per un'Area organizzativa.
	 * 
	 * @see it.ibm.red.business.dao.ILookupTableDAO#getSelectorByIdAoo(long,
	 *      java.sql.Connection).
	 * @param idAoo
	 *            identificativo AOO
	 * @param con
	 *            connession al database
	 * @return lista dei selettori
	 */
	@Override
	public Collection<String> getSelectorByIdAoo(final long idAoo, final Connection con) {
		final Collection<String> output = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "";

		try {

			query = "SELECT lt.SELETTORE FROM LOOKUPTABLE lt WHERE (LT.DATADISATTIVAZIONE IS NULL OR DATADISATTIVAZIONE > SYSDATE) AND LT.IDAOO = ? GROUP BY lt.SELETTORE ORDER BY lt.SELETTORE";

			ps = con.prepareStatement(query);
			final int index = 1;
			ps.setLong(index, idAoo);

			rs = ps.executeQuery();

			while (rs.next()) {
				final String selettore = rs.getString(SELETTORE);
				output.add(selettore);
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei Selettori attivi per AOO con id <" + idAoo + ">", e);
			throw new RedException("Errore durante il recupero dei Selettori attivi per AOO con id <" + idAoo + ">", e);
		} finally {
			closeStatement(ps, rs);
		}

		return output;
	}

	/**
	 * Restituisce i valori validi per un selettore identificato dal nome univoco
	 * <code> selector </code>.
	 * 
	 * @see it.ibm.red.business.dao.ILookupTableDAO#getValuesBySelector(java.lang.String,
	 *      java.util.Date, boolean, java.sql.Connection).
	 * @param selector
	 *            nome selettore
	 * @param dateTarget
	 *            data di riferimento dell'utilizzo valore
	 * @param idAoo
	 *            identificativo AOO
	 * @param con
	 *            connession al database
	 * @return lista dei selettori recuperati
	 */
	@Override
	public Collection<LookupTable> getValuesBySelector(final String selector, final Date dateTarget, final Long idAoo, final Connection con) {
		final Collection<LookupTable> output = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		final StringBuilder query = new StringBuilder();
		
		try {
			query.append("SELECT lt.* ").append("FROM LOOKUPTABLE lt ").append("WHERE lt.SELETTORE = ? ");

			if (idAoo != null) {
				query.append("AND lt.IDAOO = ? ");
			} else {
				query.append("AND lt.IDAOO IS NULL ");
			}

			if (dateTarget != null) {
				query.append("AND TRUNC(lt.DATAATTIVAZIONE) <= ? AND (lt.DATADISATTIVAZIONE IS NULL OR lt.DATADISATTIVAZIONE > ?) ");
			}

			query.append("ORDER BY lt.DISPLAYNAME");

			ps = con.prepareStatement(query.toString());

			int index = 1;
			ps.setString(index++, selector);

			if (idAoo != null) {
				ps.setLong(index++, idAoo);
			}

			if (dateTarget != null) {
				ps.setDate(index++, new java.sql.Date(dateTarget.getTime()));
				ps.setDate(index, new java.sql.Date(dateTarget.getTime()));
			}

			rs = ps.executeQuery();

			while (rs.next()) {
				output.add(populate(rs));
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei valori definiti per il selettore <" + selector + ">", e);
			throw new RedException("Errore durante il recupero dei valori definiti per il selettore <" + selector + ">", e);
		} finally {
			closeStatement(ps, rs);
		}

		return output;
	}

	/**
	 * Recupera descrizione del selettore identificato dalla <code> key </code>.
	 * 
	 * @see it.ibm.red.business.dao.ILookupTableDAO#getDescByPk(java.lang.Integer,
	 *      java.sql.Connection).
	 * @param key
	 *            chiave selettore
	 * @param con
	 *            connession al database
	 * @return Display name del selettore
	 */
	@Override
	public String getDescByPk(final Integer key, final Connection con) {
		String out = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {

			final String query = "SELECT DISPLAYNAME FROM LOOKUPTABLE WHERE ID = ?";
			ps = con.prepareStatement(query);
			ps.setInt(1, key);
			rs = ps.executeQuery();

			if (rs.next()) {
				out = rs.getString(DISPLAYNAME);
			}

		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero della descrizione del valore della lookuptable.", e);
			throw new RedException("Errore in fase di recupero della descrizione del valore della lookuptable", e);
		} finally {
			closeStatement(ps, rs);
		}

		return out;
	}

	/**
	 * @see it.ibm.red.business.dao.ILookupTableDAO#getLookupTables(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public Collection<LookupTable> getLookupTables(final Long idAoo, final Connection connection) {
		final Collection<LookupTable> lookupTables = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			final String query = "SELECT * FROM LOOKUPTABLE WHERE IDAOO = ? OR IDAOO IS NULL";
			ps = connection.prepareStatement(query);
			ps.setLong(1, idAoo);
			rs = ps.executeQuery();

			while (rs.next()) {
				final LookupTable lookupTable = new LookupTable(rs.getLong("ID"), rs.getString(SELETTORE), rs.getString(DISPLAYNAME), rs.getString("VALUE"),
						rs.getDate("DATAATTIVAZIONE"), rs.getDate("DATADISATTIVAZIONE"), rs.getLong("IDAOO"));
				lookupTables.add(lookupTable);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero della descrizione del valore della lookuptable.", e);
			throw new RedException("Errore in fase di recupero della descrizione del valore della lookuptable", e);
		} finally {
			closeStatement(ps, rs);
		}

		return lookupTables;
	}

	/**
	 * @see it.ibm.red.business.dao.ILookupTableDAO#getIdBySelectorAndValueAndAoo(java.lang.String,
	 *      java.lang.String, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public LookupTable getIdBySelectorAndValueAndAoo(final String selettore, final String valore, final Long idAoo, final Connection con) {
		LookupTable lookupTableValue = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = con.prepareStatement("SELECT lt.* FROM LOOKUPTABLE lt WHERE lt.SELETTORE = ? AND lt.VALUE = ? AND lt.IDAOO = ?");
			ps.setString(1, selettore);
			ps.setString(2, valore);
			ps.setLong(3, idAoo);

			rs = ps.executeQuery();

			if (rs.next()) {
				lookupTableValue = populate(rs);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero dell'ID per il valore: " + valore + " della LOOKUP TABLE con selettore: " + selettore + " per l'AOO: " + idAoo, e);
			throw new RedException("Errore in fase di recupero dell'ID per il valore: " + valore + " della LOOKUP TABLE con selettore: " + selettore + " per l'AOO: " + idAoo,
					e);
		} finally {
			closeStatement(ps, rs);
		}

		return lookupTableValue;
	}

	/**
	 * @see it.ibm.red.business.dao.ILookupTableDAO#saveLookupTable(it.ibm.red.business.persistence.model.LookupTable,
	 *      java.sql.Connection).
	 */
	@Override
	public void saveLookupTable(final LookupTable lookupTable, final Connection connection) {
		PreparedStatement ps = null;
		int index = 1;
		try {
			ps = connection
					.prepareStatement("INSERT INTO LOOKUPTABLE(ID, SELETTORE, DISPLAYNAME, VALUE, DATAATTIVAZIONE, DATADISATTIVAZIONE, IDAOO) VALUES(?, ?, ?, ?, ?, ?, ?)");

			ps.setLong(index++, getNextValFromSequence(connection));
			ps.setString(index++, lookupTable.getSelettore());
			ps.setString(index++, lookupTable.getDisplayName());
			ps.setString(index++, lookupTable.getValue());
			ps.setDate(index++, new java.sql.Date(lookupTable.getDataAttivazione().getTime()));

			if (lookupTable.getDataDisattivazione() != null) {
				ps.setDate(index++, new java.sql.Date(lookupTable.getDataDisattivazione().getTime()));
			} else {
				ps.setNull(index++, Types.DATE);
			}

			ps.setLong(index, lookupTable.getIdAoo());

			ps.executeUpdate();
		} catch (final Exception e) {
			LOGGER.error(ERROR_SALVATAGGIO_LOOKUPTABLE_MSG, e);
			throw new RedException(ERROR_SALVATAGGIO_LOOKUPTABLE_MSG, e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ILookupTableDAO#getNextValFromSequence(java.sql.Connection).
	 */
	@Override
	public long getNextValFromSequence(final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		long nextVal = 0L;

		try {
			ps = connection.prepareStatement("SELECT SEQ_LOOKUPTABLE.NEXTVAL FROM DUAL");
			rs = ps.executeQuery();
			if (rs.next()) {
				nextVal = rs.getLong(1);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore riscontrato durante il sollecitamento della sequence: SEQ_LOOKUPTABLE", e);
			throw new RedException(ERROR_SALVATAGGIO_LOOKUPTABLE_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}

		return nextVal;
	}

	private static LookupTable populate(final ResultSet rs) throws SQLException {
		return new LookupTable(rs.getLong("ID"), rs.getString(SELETTORE), rs.getString(DISPLAYNAME), rs.getString("VALUE"), rs.getDate("DATAATTIVAZIONE"),
				rs.getDate("DATADISATTIVAZIONE"), rs.getLong("IDAOO"));
	}

}
