package it.ibm.red.business.persistence.model;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class Contributo.
 *
 * @author CPIERASC
 * 
 *         Entità che mappa la tabella ContributoInterno e ContributoEsterno.
 */
public class Contributo implements Serializable {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Identificativo contributo.
	 */
	private Long idContributo;

	/**
	 * Data contributo.
	 */
	private final Date dataContributo;

	/**
	 * Identificativo utente.
	 */
	private Long idUtente;

	/**
	 * Identificativo utente.
	 */
	private Long idUfficio;

	/**
	 * Identificativo aoo.
	 */
	private Long idAoo;

	/**
	 * Document title del documento a cui è associato il contributo.
	 */
	private final Long idDocumento;

	/**
	 * Nota.
	 */
	private String nota;

	/**
	 * Flag esterno.
	 */
	private Boolean flagEsterno;

	/**
	 * Protocollo.
	 */
	private String protocollo;

	/**
	 * Identificativo filenet del documento che rappresenta il contributo (se il contributo è interno è il document title, altrimenti è il guid...); 
	 * se questo valore è null allora non è presente un documento ma il contributo è rappresentato dalla nota.
	 */
	private String guid;
	

    /**
     * Stato.
     */
	private Integer stato;
	

    /**
     * Nome file.
     */
	private String nomeFile;
	
    /**
     * Data CdC.
     */
	private String dataCdC;
	
    /**
     * Foglio.
     */
	private int foglio;
	
    /**
     * Registro.
     */
	private int registro;
	
    /**
     * Mittente.
     */
	private String mittente;
	
    /**
     * Flag modificabile.
     */
	private Integer modificabile;
	
    /**
     * Flag validità firma.
     */
	private Integer validitaFirma;
	

	/**
	 * Costruttore contributo interno.
	 * 
	 * @param dataContributo
	 * @param idUtente
	 * @param idUfficio
	 * @param idAoo
	 * @param idDocumento
	 * @param nota
	 * @param nomeFile
	 * @param protocollo
	 * @param guid
	 * @param dataCdC
	 * @param foglio
	 * @param registro
	 * @param stato
	 */
	public Contributo(final Date dataContributo, final Long idUtente, final Long idUfficio, final Long idAoo, final Long idDocumento, final String nota, 
			final String nomeFile, final String protocollo, final String guid, final String dataCdC, final int foglio, final int registro, final Integer stato) {
		super();
		this.dataContributo = dataContributo;
		this.idUtente = idUtente;
		this.idUfficio = idUfficio;
		this.idAoo = idAoo;
		this.idDocumento = idDocumento;
		this.nota = nota;
		this.nomeFile = nomeFile;
		this.protocollo = protocollo;
		this.guid = guid;
		this.dataCdC = dataCdC;
		this.foglio = foglio;
		this.registro = registro;
		this.stato = stato;
	}
	
	
	/**
	 * Costruttore contributo esterno.
	 * 
	 * @param dataContributo
	 * @param idUtente
	 * @param idUfficio
	 * @param idAoo
	 * @param idDocumento
	 * @param nota
	 * @param nomeFile
	 * @param protocollo
	 * @param guid
	 * @param stato
	 * @param modificabile
	 */
	public Contributo(final Date dataContributo, final Long idAoo, final Long idDocumento, final String mittente, final String nota, final String nomeFile, 
			final String protocollo, final String guid, final Integer stato, final Integer modificabile, final Integer validitaFirma) {
		super();
		this.dataContributo = dataContributo;
		this.idAoo = idAoo;
		this.idDocumento = idDocumento;
		this.mittente = mittente;
		this.nota = nota;
		this.nomeFile = nomeFile;
		this.protocollo = protocollo;
		this.guid = guid;
		this.stato = stato;
		this.modificabile = modificabile;
		this.validitaFirma = validitaFirma;
	}
	

	/**
	 * Costruttore.
	 * 
	 * @param inIdContributo	identificativo contributo
	 * @param inDataContributo	data contributo
	 * @param inIdUtente		identificativo utente
	 * @param inIdUfficio		identificativo ufficio
	 * @param inIdAoo			identificativo aoo
	 * @param inIdDocumento		identificativo documento
	 * @param inNota			nota
	 * @param inFlagEsterno		flag esterno
	 * @param inGuid			guid
	 */
	public Contributo(final Long inIdContributo, final Date inDataContributo, final Long inIdUtente, final Long inIdUfficio, final Long inIdAoo, final Long inIdDocumento, 
			final String nomeFile, final String inNota, final Boolean inFlagEsterno, final String inGuid) {
		super();
		this.dataContributo = inDataContributo;
		this.idUtente = inIdUtente;
		this.idUfficio = inIdUfficio;
		this.idAoo = inIdAoo;
		this.idDocumento = inIdDocumento;
		this.nomeFile = nomeFile;
		this.nota = inNota;
		this.flagEsterno = inFlagEsterno;
		this.idContributo = inIdContributo;
		this.guid = inGuid;
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param inIdContributo	identificativo contributo
	 * @param inDataContributo	data contributo
	 * @param inIdDocumento		identificativo documento
	 * @param inNota			nota
	 * @param inProtocollo		protocollo
	 * @param inFlagEsterno		flag esterno
	 * @param inGuid			guid
	 */
	public Contributo(final Long inIdContributo, final Date inDataContributo, final Long inIdDocumento, final String inNota, 
			final String inProtocollo, final Boolean inFlagEsterno, final String inGuid, final String inMittente, final Integer inModificabile, 
			final Integer inValiditaFirma) {
		super();
		this.idContributo = inIdContributo;
		this.dataContributo = inDataContributo;
		this.idDocumento = inIdDocumento;
		this.nota = inNota;
		this.protocollo = inProtocollo;
		this.flagEsterno = inFlagEsterno;
		this.guid = inGuid;
		this.mittente = inMittente;
		this.modificabile = inModificabile;
		this.validitaFirma = inValiditaFirma;
	}

	/**
	 * Getter.
	 * 
	 * @return	protocollo
	 */
	public final String getProtocollo() {
		return protocollo;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	identificativo contributo
	 */
	public final Long getIdContributo() {
		return idContributo;
	}

	/**
	 * Getter.
	 * 
	 * @return	data contributo
	 */
	public final Date getDataContributo() {
		return dataContributo;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	identificativo utente
	 */
	public final Long getIdUtente() {
		return idUtente;
	}

	/**
	 * Getter.
	 * 
	 * @return	identificativo ufficio
	 */
	public final Long getIdUfficio() {
		return idUfficio;
	}

	/**
	 * Getter.
	 * 
	 * @return	identificativo aoo
	 */
	public final Long getIdAoo() {
		return idAoo;
	}

	/**
	 * Imposta l'id AOO.
	 * @param idAoo
	 */
	public void setIdAoo(final Long idAoo) {
		this.idAoo = idAoo;
	}

	/**
	 * Getter.
	 * 
	 * @return	identificativo documento
	 */
	public final Long getIdDocumento() {
		return idDocumento;
	}

	/**
	 * Getter.
	 * 
	 * @return	nota
	 */
	public final String getNota() {
		return nota;
	}

	/**
	 * Setter.
	 * 
	 * @param inNota	nota
	 */
	public final void setNota(final String inNota) {
		this.nota = inNota;
	}

	/**
	 * Getter.
	 * 
	 * @return	flag esterno
	 */
	public final Boolean getFlagEsterno() {
		return flagEsterno;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	guid
	 */
	public final String getGuid() {
		return guid;
	}

	/**
	 * Imposta il GUID.
	 * @param guid
	 */
	public final void setGuid(final String guid) {
		this.guid = guid;
	}

	/**
	 * Getter.
	 * 
	 * @return	dataCdC
	 */
	public String getDataCdC() {
		return dataCdC;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	foglio
	 */
	public int getFoglio() {
		return foglio;
	}

	/**
	 * Getter.
	 * 
	 * @return	registro
	 */
	public int getRegistro() {
		return registro;
	}

	/**
	 * Getter.
	 * 
	 * @return	stato
	 */
	public Integer getStato() {
		return stato;
	}

	/**
	 * Getter.
	 * 
	 * @return	nome file
	 */
	public String getNomeFile() {
		return nomeFile;
	}

	/**
	 * @return the mittente
	 */
	public String getMittente() {
		return mittente;
	}


	/**
	 * @return the modificabile
	 */
	public Integer getModificabile() {
		return modificabile;
	}

	
	/**
	 * @return the validitaFirma
	 */
	public Integer getValiditaFirma() {
		return validitaFirma;
	}
}