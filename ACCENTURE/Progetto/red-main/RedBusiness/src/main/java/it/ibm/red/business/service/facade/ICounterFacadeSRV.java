/**
 * 
 */
package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.PerformanceDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.persistence.model.Nodo;

/**
 * @author APerquoti
 *
 */
public interface ICounterFacadeSRV extends Serializable {

	/**
	 * Metodo per il calcolo dei contatori data una casella postale.
	 * 
	 * @param nomeCasella
	 * @param fcDTO
	 * @return
	 */
	Map<String, Integer> getCounterMenuMail(String nomeCasella, FilenetCredentialsDTO fcDTO, Long idAoo);

	/**
	 * Metodo per il calcolo dei contatori del menu Attività dato un utente.
	 * 
	 * @param utente
	 * @return
	 */
	Map<Integer, Integer> getCounterMenuAttivita(UtenteDTO utente);

	/**
	 * Metodo per il calcolo dei contatori del menu FEPA dato un utente.
	 * 
	 * @param utente
	 * @return
	 */
	Map<DocumentQueueEnum, Integer> getCounterMenuFepa(UtenteDTO utente);

	/**
	 * Metodo per il calcolo dei contatori per le code ufficio per organigramma.
	 * 
	 * @param idNodo
	 * @param isUCB
	 * @return
	 */
	Map<String, Integer> countCodeUfficioOrg(Long idNodo, FilenetCredentialsDTO fcDto, boolean isUCB);

	/**
	 * Metodo per il calcolo dei contatori per le code Utente per Organigramma.
	 * 
	 * @param idNodo
	 * @param idUtenteSessione
	 * @param listaUtenti
	 * @param fcDto
	 * @return
	 */
	Map<Long, Integer> countCodeUtentiOrg(Long idNodo, Long idUtenteSessione, List<NodoOrganigrammaDTO> listaUtenti, FilenetCredentialsDTO fcDto,
			boolean isUtenteSessioneGestioneApplicativa);

	/**
	 * Metodo per contare il totale degli elementi per i sotto uffici e per gli
	 * eventuali figli.
	 * 
	 * @param sottoUffici
	 * @param fcDto
	 * @return
	 */
	Map<Long, Integer> countTotSottoUffici(List<NodoOrganigrammaDTO> sottoUffici, UtenteDTO utente);

	/**
	 * Conta i sottouffici nell'organigramma.
	 * 
	 * @param listUffici
	 * @param utente
	 * @param inApprovazioneDirigente
	 * @return mappa ufficio contatore
	 */
	Map<String, Integer> countTotSottoUfficiWidget(List<Nodo> listUffici, UtenteDTO utente, boolean inApprovazioneDirigente);

	/**
	 * Conta i sottouffici nell'organigramma.
	 * 
	 * @param listUffici
	 * @param utente
	 * @param inApprovazioneDirigente
	 * @return mappa ufficio contatore
	 */
	Map<Integer, Integer> countTotSottoUfficiWidgetStruttura(List<Nodo> listUffici, UtenteDTO utente, boolean inApprovazioneDirigente);

	/**
	 * Misura le performance.
	 * 
	 * @param username
	 * @param isFull
	 * @return risultato della misurazione
	 */
	PerformanceDTO measure(String username, Boolean isFull);

}