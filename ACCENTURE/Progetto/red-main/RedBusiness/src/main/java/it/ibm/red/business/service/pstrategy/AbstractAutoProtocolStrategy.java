package it.ibm.red.business.service.pstrategy;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.filenet.api.core.Document;

import it.ibm.red.business.constants.Constants.Modalita;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AnagraficaDipendenteDTO;
import it.ibm.red.business.dto.AnagraficaDipendentiComponentDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.CapitoloSpesaMetadatoDTO;
import it.ibm.red.business.dto.DatiFlussoDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.EventoLogDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.LookupTableDTO;
import it.ibm.red.business.dto.MessaggioPostaNpsDTO;
import it.ibm.red.business.dto.MessaggioPostaNpsFlussoDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.NotificaRitiroAutotutelaDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.RicercaAnagDipendentiDTO;
import it.ibm.red.business.dto.RichiestaElabMessaggioPostaNpsDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedParametriDTO;
import it.ibm.red.business.dto.SelectItemDTO;
import it.ibm.red.business.dto.TipologiaDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.FilenetStatoFascicoloEnum;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ProvenienzaSalvaDocumentoEnum;
import it.ibm.red.business.enums.RiservatezzaEnum;
import it.ibm.red.business.enums.SalvaDocumentoErroreEnum;
import it.ibm.red.business.enums.TipoAllaccioEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoContestoProceduraleEnum;
import it.ibm.red.business.enums.TipoMetadatoEnum;
import it.ibm.red.business.enums.TipoProtocolloEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.metadatiestesi.AnagraficaDipendentiHelper;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.nps.dto.AllegatoNpsDTO;
import it.ibm.red.business.nps.dto.AssegnatariDTO;
import it.ibm.red.business.nps.dto.AssegnatarioDTO;
import it.ibm.red.business.nps.dto.DocumentoNpsDTO;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.TipoProcedimento;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.IDocumentManagerSRV;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.ILookupTableSRV;
import it.ibm.red.business.service.INotificaNpsSRV;
import it.ibm.red.business.service.INotificaRicezioneIntegrazioneDatiSRV;
import it.ibm.red.business.service.INotificaRitiroAutotutelaSRV;
import it.ibm.red.business.service.ISalvaDocumentoSRV;
import it.ibm.red.business.service.concrete.NotificaRicezioneIntegrazioneDatiDTO;
import it.ibm.red.business.service.facade.IAnagraficaDipendentiFacadeSRV;
import it.ibm.red.business.service.facade.IDocumentoRedFacadeSRV;
import it.ibm.red.business.service.facade.IEventoLogFacadeSRV;
import it.ibm.red.business.service.facade.IIntegrazioneDatiFacadeSRV;
import it.ibm.red.business.service.facade.INodoFacadeSRV;
import it.ibm.red.business.service.facade.INpsFacadeSRV;
import it.ibm.red.business.service.facade.IProtocollaFlussoFacadeSRV;
import it.ibm.red.business.service.facade.IRubricaFacadeSRV;
import it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV;
import it.ibm.red.business.service.facade.IUtenteFacadeSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.FileUtils;
import it.ibm.red.webservice.model.interfaccianps.npstypesestesi.MetaDatoAssociato;
import it.ibm.red.webservice.model.interfaccianps.npstypesestesi.TIPOLOGIA;

/**
 * Abstract del servizio di gestione protocollazione automatica.
 * @param <T>
 */
@Component
public abstract class AbstractAutoProtocolStrategy<T extends MessaggioPostaNpsDTO> implements IAutoProtocolStrategy<T> {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AbstractAutoProtocolStrategy.class.getName());

	/**
	 * Label costante associata alla mancanza del numero di protocollo.
	 */
	private static final String MISSING_PROTOCOLLO = "[numero protocollo non identificato]";
	
	/**
	 * Label costante associata alla mancanza del document title.
	 */
	private static final String MISSING_DT = "[document title non identificato]";
	
	/**
	 * Servizio utente.
	 */
	private final IUtenteFacadeSRV utenteSRV = ApplicationContextProvider.getApplicationContext().getBean(IUtenteFacadeSRV.class);

	/**
	 * Servizio rubrica.
	 */
	private final IRubricaFacadeSRV rubricaSRV = ApplicationContextProvider.getApplicationContext().getBean(IRubricaFacadeSRV.class);

	/**
	 * Servizio tipologia documento.
	 */
	private final ITipologiaDocumentoFacadeSRV tipologiaDocumentoSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipologiaDocumentoFacadeSRV.class);

	/**
	 * Servizio protocllazione flusso.
	 */
	private final IProtocollaFlussoFacadeSRV protocollaFlussoSRV = ApplicationContextProvider.getApplicationContext().getBean(IProtocollaFlussoFacadeSRV.class);

	/**
	 * Servizio anagrafica dipendenti.
	 */
	private final IAnagraficaDipendentiFacadeSRV anagraficaDipendentiSRV = ApplicationContextProvider.getApplicationContext().getBean(IAnagraficaDipendentiFacadeSRV.class);

	/**
	 * Servizio gestione tipi documento.
	 */
	private final ITipologiaDocumentoFacadeSRV tipoDocSRV = ApplicationContextProvider.getApplicationContext().getBean(ITipologiaDocumentoFacadeSRV.class);

	/**
	 * Servizio salvataggio documento.
	 */
	private final ISalvaDocumentoSRV salvaDocSRV = ApplicationContextProvider.getApplicationContext().getBean(ISalvaDocumentoSRV.class);

	/**
	 * Servizio gestione aoo.
	 */
	private final IAooSRV aooSRV = ApplicationContextProvider.getApplicationContext().getBean(IAooSRV.class);

	/**
	 * Servizio gestione documento.
	 */
	private final IDocumentManagerSRV documentManagerSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentManagerSRV.class);

	/**
	 * Servizio fascicolo.
	 */
	private final IFascicoloSRV fascicoloSRV = ApplicationContextProvider.getApplicationContext().getBean(IFascicoloSRV.class);

	/**
	 * Servizio notifica ricezione integrazione dati.
	 */
	private final INotificaRicezioneIntegrazioneDatiSRV notificaRicezioneIntegrazioneDatiSRV = ApplicationContextProvider.getApplicationContext()
			.getBean(INotificaRicezioneIntegrazioneDatiSRV.class);

	/**
	 * Servizio notifica ritiro in autotutela.
	 */
	private final INotificaRitiroAutotutelaSRV notificaRitiroAutotutelaSRV = ApplicationContextProvider.getApplicationContext().getBean(INotificaRitiroAutotutelaSRV.class);

	/**
	 * Servizio gestione nodi.
	 */
	private final INodoFacadeSRV nodoSRV = ApplicationContextProvider.getApplicationContext().getBean(INodoFacadeSRV.class);

	/**
	 * Servizio integrazione dati.
	 */
	private final IIntegrazioneDatiFacadeSRV integrazioneDatiSRV = ApplicationContextProvider.getApplicationContext().getBean(IIntegrazioneDatiFacadeSRV.class);

	/**
	 * Servizio nps.
	 */
	private final INpsFacadeSRV npsSRV = ApplicationContextProvider.getApplicationContext().getBean(INpsFacadeSRV.class);

	/**
	 * Servizio evento log.
	 */
	private final IEventoLogFacadeSRV eventoLogSRV = ApplicationContextProvider.getApplicationContext().getBean(IEventoLogFacadeSRV.class);

	/**
	 * Servizio documento Red.
	 */
	private final IDocumentoRedFacadeSRV documentoRedSRV = ApplicationContextProvider.getApplicationContext().getBean(IDocumentoRedFacadeSRV.class);

	/**
	 * Servizio di gestione lookup tables.
	 */
	private final ILookupTableSRV lookupSRV = ApplicationContextProvider.getApplicationContext().getBean(ILookupTableSRV.class);

	/**
	 * Servizio utilizzato per il recupero del protocollo di notifica da NPS.
	 */
	private final INotificaNpsSRV notificaNpsSRV = ApplicationContextProvider.getApplicationContext().getBean(INotificaNpsSRV.class);
	
	/**
	 * @param protocolloMessaggioPosta
	 * @return
	 */
	protected ProtocolloNpsDTO getDettagliProtocollo(final Integer idAoo, final ProtocolloNpsDTO protocolloMessaggioPosta) {
		return npsSRV.getDettagliProtocollo(idAoo, protocolloMessaggioPosta, true, null, false);
	}

	/**
	 * @param mittenteProtocollo
	 * @param utenteProtocollatore
	 * @param con
	 * @return
	 */
	protected Contatto getMittente(final Contatto mittenteProtocollo, final UtenteDTO utenteProtocollatore, final String mail) {
		if (mittenteProtocollo != null) {
			mittenteProtocollo.setMailPec(mail);
			return rubricaSRV.insertContattoFlussoAutomatico(mittenteProtocollo, utenteProtocollatore);
		} else {
			throw new RedException("getMittente -> Errore: mittente NON presente nel protocollo NPS in input");
		}
	}

	/**
	 * Recupera l'utente protocollatore. In caso il documento di rifertimento esista e sia riservato, l'utente protocollatore sarà 
	 * l'assegnatario del documento che si sta creando o, se non esiste, l'amministratore di sistema per superare eventuali 
	 * problemi di security sul documento iniziale del processo.
	 * 
	 * @param protocolloNps
	 * @param protocolloRiferimento
	 * @param assegnazioni
	 * @param con
	 * @return
	 */
	private UtenteDTO getUtenteProtocollatore(final ProtocolloNpsDTO protocolloNps, final String protocolloRiferimento, final List<AssegnazioneDTO> assegnazioni) {
		UtenteDTO protocollatore = npsSRV.getUtenteProtocollatore(protocolloNps, false);
		if (protocollatore == null) {
			throw new RedException("getUtenteProtocollatore -> Errore: impossibile recuperare l'utente protocollatore");
		} else {
			
			if(StringUtils.isNotBlank(protocolloRiferimento) && assegnazioni != null && !assegnazioni.isEmpty()) {
				final Aoo aoo = aooSRV.recuperaAoo(protocollatore.getIdAoo().intValue());
				final AooFilenet aooFilenet = aoo.getAooFilenet();
				final boolean riservato = isProtocolloRiservato(protocolloRiferimento, aooFilenet);
				if(riservato) {
					AssegnazioneDTO assegnazioneDTO = assegnazioni.get(0);
					if(assegnazioneDTO.getUfficio() != null && assegnazioneDTO.getUtente() != null) {
						//recupera l'utente in quell'ufficio
						protocollatore = utenteSRV.getById(assegnazioneDTO.getUtente().getId(), null, assegnazioneDTO.getUfficio().getId());
					} else {
						//protocolla con p8admin
						protocollatore = utenteSRV.getByUsername(FilenetCredentialsDTO.P8_ADMIN);
					}
				}
			}
			
			return protocollatore;
		}
	}
	
	/**
	 * @param protocolloNps
	 * @param con
	 * @return
	 */
	protected UtenteDTO getUtenteProtocollatore(final ProtocolloNpsDTO protocolloNps) {
		return getUtenteProtocollatore(protocolloNps, null, null);
	}

	/**
	 * @param idAOO
	 * @param tipologiaNPS
	 * @return
	 */
	protected TipologiaDTO getTipologiaFromTipologiaNPS(final Long idAOO, final TIPOLOGIA tipologiaNPS) {
		return getTipologia(idAOO, tipologiaNPS.getNOME());
	}

	/**
	 * @param idAOO
	 * @param protocolloNPS
	 * @return
	 */
	protected TipologiaDTO getTipologiaFromProtocolloNPS(final Long idAOO, final ProtocolloNpsDTO protocolloNPS) {
		return getTipologia(idAOO, protocolloNPS.getDescrizioneTipologiaDocumento());
	}

	/**
	 * Restituisce la tipologia identificata dalla descrizione
	 * {@code nomeTipologiaNPS}.
	 * 
	 * @param idAOO
	 *            identificativo dell'area organizzativa
	 * @param nomeTipologiaNPS
	 *            nome della tipologia NPS
	 * @return tipologia descritta dal {@code nomeTipologiaNPS}
	 */
	protected TipologiaDTO getTipologia(final Long idAOO, final String nomeTipologiaNPS) {
		LOGGER.info("ID AOO: " + idAOO);
		LOGGER.info("Nome Tipologia: " + nomeTipologiaNPS);
		return tipoDocSRV.getTipologiaFromTipologiaNPS(true, idAOO.intValue(), nomeTipologiaNPS);
	}

	/**
	 * Recupera e restituisce i metadati estesi della {@code tipologia}
	 * formattandoli per l'interfaccia grafica.
	 * 
	 * @param idAoo
	 *            identificativo area organizzativa
	 * @param tipologia
	 *            descrizione tipologia
	 * @return lista dei metadati estesi recuperati
	 */
	protected List<MetadatoDTO> caricaMetadatiEstesiPerGUI(final Long idAoo, final TipologiaDTO tipologia) {
		return tipoDocSRV.caricaMetadatiEstesiPerGUI(tipologia.getIdTipoDocumento(), tipologia.getIdTipoProcedimento(), false, idAoo, null);
	}

	/**
	 * @param idTipoDoc
	 * @return
	 */
	protected String getDescTipoDocFromID(final Integer idTipoDoc) {
		return tipologiaDocumentoSRV.getDescTipologiaDocumentoById(idTipoDoc);
	}

	/**
	 * @param protocolloNps
	 */
	private List<AllegatoDTO> getAllegati(final Integer idAoo, final ProtocolloNpsDTO protocolloNps, final String codiceAoo) {
		List<AllegatoDTO> allegati = null;

		try {
			if (!CollectionUtils.isEmpty(protocolloNps.getAllegatiList())) {

				final Integer idTipologiaDocumento = Integer.parseInt(PropertiesProvider.getIstance().getParameterByString(codiceAoo + ".tipologia.generica.default"));

				final List<AllegatoNpsDTO> allegatiProtocollo = protocolloNps.getAllegatiList();
				allegati = new ArrayList<>();

				AllegatoDTO allegato = null;
				DocumentoNpsDTO contentAllegato = null;
				for (final AllegatoNpsDTO alleg : allegatiProtocollo) {
					// Si scarica il content da NPS
					contentAllegato = npsSRV.downloadDocumento(idAoo, alleg.getGuid(),false);

					final byte[] contentAllegatoByte = IOUtils.toByteArray(contentAllegato.getInputStream());
					// Se l'allegato è firmato, il suo formato deve essere 'Firmato Digitalmente'
					final FormatoAllegatoEnum formatoAllegato = FileUtils.isP7MFile(contentAllegato.getContentType()) || PdfHelper.getSignatureNumber(contentAllegatoByte) > 0 ? 
							FormatoAllegatoEnum.FIRMATO_DIGITALMENTE : FormatoAllegatoEnum.ELETTRONICO;

					allegato = new AllegatoDTO(alleg.getNomeFile(), contentAllegatoByte, contentAllegato.getContentType(), formatoAllegato, alleg.getOggetto(), null, null,
							idTipologiaDocumento);

					allegati.add(allegato);
				}
			}
		} catch (final IOException e) {
			LOGGER.error("Errore nel recupero del content di un allegato dal protocollo NPS.", e);
			throw new RedException("Errore nel recupero del content di un allegato dal protocollo NPS.", e);
		}

		return allegati;
	}

	/**
	 * @param document
	 * @param protocolloNps
	 */
	private static void impostaProtocollo(final DetailDocumentRedDTO document, final ProtocolloNpsDTO protocolloNps) {
		document.setIdProtocollo(protocolloNps.getIdProtocollo());
		document.setNumeroProtocollo(protocolloNps.getNumeroProtocollo());
		document.setAnnoProtocollo(Integer.parseInt(protocolloNps.getAnnoProtocollo()));
		document.setDataProtocollo(protocolloNps.getDataProtocollo());
		document.setTipoProtocollo(TipoProtocolloEnum.getByName(protocolloNps.getTipoProtocollo()).getId());
	}
	
	/**
	 * @param document
	 * @param protocolloNps
	 */
	private void impostaProtocolloMittente(final DetailDocumentRedDTO document, final ProtocolloNpsDTO protocolloNps) {
		document.setProtocolloMittente(protocolloNps.getNumeroProtocolloMittente());
		document.setDataProtocolloMittente(protocolloNps.getDataProtocolloMittente());
		if(protocolloNps.getDataProtocolloMittente()!=null) {
			document.setAnnoProtocolloMittente(DateUtils.getYearFromDate(protocolloNps.getDataProtocolloMittente()));
		}
	}

	/**
	 * Restituisce la lista di metadati valorizzati per la creazione del protocollo.
	 * 
	 * @param metadatiEstesiTipologiaDocProt
	 *            metadati estesi associati alla coppia tipo documento / tipo
	 *            procedimento
	 * @param idTipologiaDocumento
	 *            id della tipologia documento
	 * @param idTipoProcedimento
	 *            id della tipologia procedimento
	 * @param idAoo
	 *            id dell'AOO dell'utente in sessione
	 * @param document
	 * @param codiceAOO
	 *            codice dell'AOO dell'utente in sessione
	 * @return lista dei metadati completi di valore
	 */
	private List<MetadatoDTO> getMetadatiEstesi(final List<MetaDatoAssociato> metadatiEstesiTipologiaDocProt, final Integer idTipologiaDocumento,
			final Integer idTipoProcedimento, final Long idAoo, final DetailDocumentRedDTO document, final String codiceAOO) {
		List<MetadatoDTO> metadatiEstesi = null;
		if (!CollectionUtils.isEmpty(metadatiEstesiTipologiaDocProt)) {
			LOGGER.info("getMetadatiEstesi ->  Metadati Estesi: " + metadatiEstesiTipologiaDocProt.size());
			// Caricamento dei metadati estesi associati alla tipologia documento e al tipo
			// procedimento dal DB
			final List<MetadatoDTO> metadatiEstesiDB = tipologiaDocumentoSRV.caricaMetadati(idTipologiaDocumento, idTipoProcedimento, new Date(), idAoo);

			if (!CollectionUtils.isEmpty(metadatiEstesiDB)) {
				LOGGER.info("getMetadatiEstesi -> Esistono metadati estesi sulla base dati");
				metadatiEstesi = new ArrayList<>();

				// Mappa nome metadato -> valore
				final Map<String, String> mapMetadatoValoreProt = metadatiEstesiTipologiaDocProt.stream().collect(Collectors.toMap(m -> m.getCodice(), m -> m.getValore()));

				// Si valorizzano i metadati estesi recuperato dal DB per la coppia tipologia
				// documento/tipo procedimento
				// con il valore presente nel protocollo NPS per quel metadato
				for (final MetadatoDTO metadatoEstesoDB : metadatiEstesiDB) {
					// Valore del metadato esteso, presente nel protocollo
					final String valoreMetadatoProt = mapMetadatoValoreProt.get(metadatoEstesoDB.getName());

					if (valoreMetadatoProt != null) {
						if (metadatoEstesoDB instanceof LookupTableDTO) {

							boolean compareWithLike = lookupSRV.hasToCompareWithLike(metadatoEstesoDB.getId(), codiceAOO);
							
							for (final SelectItemDTO lookupValue : ((LookupTableDTO) metadatoEstesoDB).getLookupValues()) {
								if ((compareWithLike && lookupValue.getDescription().startsWith(valoreMetadatoProt))
										|| (!compareWithLike && lookupValue.getDescription().equals(valoreMetadatoProt))) {
									
									((LookupTableDTO) metadatoEstesoDB).setLookupValueSelected(lookupValue);
									break;
								}
							}

						} else if (metadatoEstesoDB instanceof CapitoloSpesaMetadatoDTO) {

							((CapitoloSpesaMetadatoDTO) metadatoEstesoDB).setCapitoloSelected(valoreMetadatoProt);

						} else if (metadatoEstesoDB instanceof AnagraficaDipendentiComponentDTO) {

							final List<AnagraficaDipendenteDTO> listaDipendenti = AnagraficaDipendentiHelper.deserializeAnagraficaDipendente(valoreMetadatoProt);
							for (final AnagraficaDipendenteDTO anag : listaDipendenti) {
								final RicercaAnagDipendentiDTO ricercaAnDipDTO = new RicercaAnagDipendentiDTO();
								ricercaAnDipDTO.setCodiceFiscale(anag.getCodiceFiscale());
								final Collection<AnagraficaDipendenteDTO> anagraficaDipendente = anagraficaDipendentiSRV.ricerca(idAoo, ricercaAnDipDTO);
								// se non esiste inserisco l'anagrafica
								if (CollectionUtils.isEmpty(anagraficaDipendente)) {
									anagraficaDipendentiSRV.salva(idAoo, anag);
								} else {
									// altrimenti ne recupero l'id
									anag.setId(anagraficaDipendente.iterator().next().getId());
								}
							}

							((AnagraficaDipendentiComponentDTO) metadatoEstesoDB).setSelectedValues(listaDipendenti);

						} else {

							if (TipoMetadatoEnum.DATE.equals(metadatoEstesoDB.getType())) {
								if (!it.ibm.red.business.utils.StringUtils.isNullOrEmpty(valoreMetadatoProt)) {
									final SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.DD_MM_YYYY);
									try {
										metadatoEstesoDB.setSelectedValue(sdf.parse(valoreMetadatoProt));
									} catch (final ParseException e) {
										throw new RedException("Errore in fase di parsing del metadato " + metadatoEstesoDB.getName(), e);
									}
								} else {
									metadatoEstesoDB.setSelectedValue(valoreMetadatoProt);
								}
							} else {
								metadatoEstesoDB.setSelectedValue(valoreMetadatoProt);
							}

						}

						// ribaltamento metadato esteso SICOGE_DECRETO_LIQUIDAZIONE_METADATO_ESTESO_NAME
						// in decretoLiquidazioneSicoge
						if (metadatoEstesoDB.getName()
								.equals(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.SICOGE_DECRETO_LIQUIDAZIONE_METADATO_ESTESO_NAME))) {
							document.setDecretoLiquidazioneSicoge(valoreMetadatoProt);
						}

					}

					metadatiEstesi.add(metadatoEstesoDB);
				}
			} else {
				final String errore = "Il protocollo contiene metadati estesi per una tipologia documento a cui su RED NON sono associati metadati estesi. Tipologia: "
						+ idTipologiaDocumento;
				LOGGER.error(errore);
				throw new RedException(errore);
			}
		}

		return metadatiEstesi;
	}

	/**
	 * @param protocolloNps
	 * @return
	 */
	protected List<AssegnazioneDTO> getAssegnazioni(final ProtocolloNpsDTO protocolloNps) {
		List<AssegnazioneDTO> assegnazioni = null;

		if (!CollectionUtils.isEmpty(protocolloNps.getAssegnatariList())) {
			assegnazioni = new ArrayList<>();
			AssegnazioneDTO assegnazione = null;

			final List<AssegnatariDTO> assegnazioniProtocollo = protocolloNps.getAssegnatariList();
			for (final AssegnatariDTO assegnazioneProtocollo : assegnazioniProtocollo) {
				final AssegnatarioDTO assegnatario = assegnazioneProtocollo.getAssegnatario();
				if (assegnatario != null) {

					// Ufficio
					final Integer idAoo = tipologiaDocumentoSRV.getIdAOOByCodiceNPS(protocolloNps.getCodiceAoo());
					final Aoo aoo = aooSRV.recuperaAoo(idAoo);
					LOGGER.info("assegnatario.getDescrizioneNodo(): " + assegnatario.getDescrizioneNodo());
					LOGGER.info("aoo.getCodiceAoo(): " + aoo.getCodiceAoo());
					final Long idUfficioAssegnatario = protocollaFlussoSRV.getIdNodoByDescAndCodiceAoo(assegnatario.getDescrizioneNodo(), aoo.getCodiceAoo());
					final UfficioDTO ufficioAssegnatario = new UfficioDTO(idUfficioAssegnatario, assegnatario.getDescrizioneNodo());

					// Utente
					UtenteDTO utenteAssegnatario = null;
					if (Boolean.TRUE.equals(assegnatario.getIsUtente())) {
						utenteAssegnatario = new UtenteDTO();
						LOGGER.info("assegnatario.getCodiceFiscale(): " + assegnatario.getCodiceFiscale());
						LOGGER.info("assegnatario.getNome(): " + assegnatario.getNome());
						LOGGER.info("assegnatario.getCognome(): " + assegnatario.getCognome());
						final Long idUtente = utenteSRV.getIdByCodiceFiscale(assegnatario.getCodiceFiscale());
						if (idUtente != null && idUtente != 0) {
							utenteAssegnatario.setId(idUtente);
							utenteAssegnatario.setIdUfficio(idUfficioAssegnatario);
							utenteAssegnatario.setNome(assegnatario.getNome());
							utenteAssegnatario.setCognome(assegnatario.getCognome());
						} else {
							utenteAssegnatario = null;
						}
					}

					final TipoAssegnazioneEnum tipoAssegnazione = BooleanFlagEnum.SI.getDescription().equalsIgnoreCase(assegnazioneProtocollo.getCompetenza())
							? TipoAssegnazioneEnum.COMPETENZA
							: TipoAssegnazioneEnum.CONOSCENZA;

					LOGGER.info("tipoAssegnazione: " + tipoAssegnazione);

					assegnazione = new AssegnazioneDTO(null, tipoAssegnazione, null, assegnazioneProtocollo.getDataAssegnazione(), null, utenteAssegnatario,
							ufficioAssegnatario);

					assegnazioni.add(assegnazione);
				}
			}
		}

		return assegnazioni;
	}

	/**
	 * Metodo base per la creazione di un documento in ingresso.
	 * @param tcp
	 * @param oggetto
	 * @param inUtenteCreatore
	 * @param idMittenteContatto
	 * @param idTipologiaDocumento
	 * @param idTipologiaProcedimento
	 * @param assegnazioni
	 * @param protocolloRiferimento
	 * @param content
	 * @param fileName
	 * @param mType
	 * @param fascicoloSelezionato
	 * @param idProcesso
	 * @param guidMail
	 * @param indiceClassificazioneFascicoloProcedimentale
	 * @param descrizioneFascicoloProcedimentale
	 * @param protocolloNps
	 * @param protocolloNotifica se diverso da null la notifica viene fascicolata col documento creato
	 * @param datiFlusso
	 * @param isIntegrazioneDati
	 * @param capitoloRiservato
	 * @return EsitoSalvaDocumentoDTO - esito della creazione
	 */
	protected EsitoSalvaDocumentoDTO creaIngresso(final TipoContestoProceduraleEnum tcp, final String oggetto, final UtenteDTO inUtenteCreatore,
			final Integer idMittenteContatto, final Integer idTipologiaDocumento, final Integer idTipologiaProcedimento, final List<AssegnazioneDTO> assegnazioni,
			final String protocolloRiferimento, final byte[] content, final String fileName, final String mType, final FascicoloDTO fascicoloSelezionato,
			final String idProcesso, final String guidMail, final String indiceClassificazioneFascicoloProcedimentale, final String descrizioneFascicoloProcedimentale,
			final ProtocolloNpsDTO protocolloNps, final ProtocolloNpsDTO protocolloNotifica, final DatiFlussoDTO datiFlusso, final boolean isIntegrazioneDati, final boolean capitoloRiservato) {
		final SalvaDocumentoRedParametriDTO parametri = new SalvaDocumentoRedParametriDTO();
		parametri.setProtocollazioneAutomatica(true);
		parametri.setContentVariato(true); // Essendo nuovo il content è variato

		if (StringUtils.isNotBlank(guidMail)) {
			LOGGER.info("creaIngresso -> GUID mail: " + guidMail);
			parametri.setInputMailGuid(guidMail);
			parametri.setModalita(Modalita.MODALITA_INS_MAIL);
		} else {
			LOGGER.info("creaIngresso -> Nessuna messaggio di posta in input");
			parametri.setModalita(Modalita.MODALITA_INS);
		}

		final DetailDocumentRedDTO document = new DetailDocumentRedDTO();

		document.setIdCategoriaDocumento(CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0]); // Documento in entrata
		document.setFormatoDocumentoEnum(FormatoDocumentoEnum.ELETTRONICO);
		document.setRiservato(RiservatezzaEnum.PUBBLICO.getId()); // Non riservato di default
		if(capitoloRiservato && tcp != null && tcp.equals(TipoContestoProceduraleEnum.SICOGE)) {
			document.setRiservato(RiservatezzaEnum.RISERVATO.getId());
		}
		document.setMezzoRicezione(MezzoSpedizioneEnum.ELETTRONICO.getId());

		// Content -> START
		document.setContent(content);
		document.setNomeFile(fileName);
		document.setMimeType(mType);
		// Content -> END

		document.setIntegrazioneDati(isIntegrazioneDati);
		document.setProtocolloRiferimento(protocolloRiferimento);
		document.setOggetto(oggetto);
		document.setIdTipologiaDocumento(idTipologiaDocumento);
		document.setIdTipologiaProcedimento(idTipologiaProcedimento);
		document.setAssegnazioni(assegnazioni);

		// Impostazione dell'allaccio notifica se esiste un protocollo notifica tra gli allacci.
		if(protocolloNotifica != null) {
			List<RispostaAllaccioDTO> allacci = new ArrayList<>();
			RispostaAllaccioDTO allaccioNotifica = new RispostaAllaccioDTO(null, protocolloNotifica.getIdDocumento(), TipoAllaccioEnum.NOTIFICA.getTipoAllaccioId(), 0, true);
			allacci.add(allaccioNotifica);
			document.setAllacci(allacci);
		}
		
		final Contatto contatto = rubricaSRV.getContattoByID(idMittenteContatto.longValue());
		LOGGER.info("creaIngresso -> Contatto: " + contatto);
		document.setMittenteContatto(contatto);

		document.setIdentificatoreProcesso(idProcesso);
		if (tcp != null) {
			document.setCodiceFlusso(tcp.getId());
		}

		// Fascicolo -> START
		// Inserimento in un fascicolo esistente
		if (fascicoloSelezionato != null && StringUtils.isNotBlank(fascicoloSelezionato.getIdFascicolo())) {
			LOGGER.info("creaIngresso -> ID fascicolo selezionato: " + fascicoloSelezionato.getIdFascicolo());
			document.setIdFascicoloProcedimentale(fascicoloSelezionato.getIdFascicolo());

			document.setFascicoli(new ArrayList<>());
			document.getFascicoli().add(fascicoloSelezionato);
			// Inserimento in un nuovo fascicolo
		} else {
			document.setIndiceClassificazioneFascicoloProcedimentale(indiceClassificazioneFascicoloProcedimentale);
			document.setDescrizioneFascicoloProcedimentale(descrizioneFascicoloProcedimentale);

			// Gestione fascicolo FEPA per il nuovo fascicolo da creare
			if (datiFlusso != null && StringUtils.isNotBlank(datiFlusso.getIdFascicoloFepa())) {
				final Map<String, Object> metadatiFascicolo = new HashMap<>();
				metadatiFascicolo.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA),
						datiFlusso.getIdFascicoloFepa());
				metadatiFascicolo.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_TIPO_FASCICOLO_FEPA_METAKEY),
						datiFlusso.getTipoFascicoloFepa());

				parametri.setMetadatiFascicolo(metadatiFascicolo);
			}

		}
		// Fascicolo -> END

		// Metadati estesi
		if (datiFlusso != null && datiFlusso.getMetadatiEstesi() != null) {
			document.setMetadatiEstesi(getMetadatiEstesi(datiFlusso.getMetadatiEstesi().getMetadatoAssociato(), idTipologiaDocumento, idTipologiaProcedimento,
					inUtenteCreatore.getIdAoo(), document, inUtenteCreatore.getCodiceAoo()));
		}

		// Protocollo
		impostaProtocollo(document, protocolloNps);
		
		// Protocollo Mittente
		impostaProtocolloMittente(document, protocolloNps);

		// Allegati
		document.setAllegati(getAllegati(inUtenteCreatore.getIdAoo().intValue(), protocolloNps, inUtenteCreatore.getCodiceAoo()));
		
		//se il documento di riferimento è riservato, l'utenteCreatore deve essere l'ultimo competente del documento di riferimento
		UtenteDTO utenteCreatore = inUtenteCreatore;
		if(StringUtils.isNotBlank(protocolloRiferimento) && assegnazioni != null && !assegnazioni.isEmpty()) {
			final Aoo aoo = aooSRV.recuperaAoo(inUtenteCreatore.getIdAoo().intValue());
			final AooFilenet aooFilenet = aoo.getAooFilenet();
			final boolean riservato = isProtocolloRiservato(protocolloRiferimento, aooFilenet);
			if(riservato) {
				AssegnazioneDTO assegnazioneDTO = assegnazioni.get(0);
				if(assegnazioneDTO.getUfficio() != null && assegnazioneDTO.getUtente() != null) {
					//recupera l'utente in quell'ufficio
					utenteCreatore = utenteSRV.getById(assegnazioneDTO.getUtente().getId(), null, assegnazioneDTO.getUfficio().getId());
				} else {
					//protocolla con p8admin
					utenteCreatore = utenteSRV.getByUsername(FilenetCredentialsDTO.P8_ADMIN);
				}
			}
		}

		return salvaDocSRV.salvaDocumento(document, parametri, utenteCreatore, ProvenienzaSalvaDocumentoEnum.POSTA_NPS);
	}

	/**
	 * @param richiestaElabMessagio
	 * @return
	 */
	public String getCodiceAOO(final RichiestaElabMessaggioPostaNpsDTO<? extends MessaggioPostaNpsDTO> richiestaElabMessagio) {
		final Integer idAOO = richiestaElabMessagio.getIdAoo().intValue();
		LOGGER.info("idAoo richiestaelaborazone: " + idAOO);
		return aooSRV.recuperaAoo(idAOO).getCodiceAoo();
	}

	/**
	 * Integrazione spontanea dei dati: messaggio 103 (FLUSSO AUT/CG2) o 1002 (SICOGE).
	 * 
	 * @param richiestaElabMessagio
	 * @param guidMail
	 * @param tcp
	 * @param statoNPS
	 * @param isProtocolloRifChiuso
	 * @return esito del salvataggio del documento
	 */
	protected EsitoSalvaDocumentoDTO integraDatiSpontanei(final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> richiestaElabMessagio, final String guidMail,
			final TipoContestoProceduraleEnum tcp, final String statoNPS, final boolean isProtocolloRifChiuso, final boolean capitoloRiservato) {
		final Integer idAOO = richiestaElabMessagio.getIdAoo().intValue();
		final Aoo aoo = aooSRV.recuperaAoo(idAOO);
		final AooFilenet aooFilenet = aoo.getAooFilenet();
		final String codiceAOO = aoo.getCodiceAoo();

		// Recupero del protocollo completo da NPS con tutte le informazioni
		final ProtocolloNpsDTO protocollo = getDettagliProtocollo(idAOO, richiestaElabMessagio.getMessaggio().getProtocollo());

		// Recupero tipo documento e tipo procedimento
		final TipologiaDTO tipologia = getTipologiaFromTipologiaNPS(richiestaElabMessagio.getIdAoo(),
				richiestaElabMessagio.getMessaggio().getDatiFlusso().getMetadatiEstesi());
		final Integer idTipoDoc = tipologia.getIdTipoDocumento();
		final Integer idTipoProc = tipologia.getIdTipoProcedimento();
		
		final String idProcesso = richiestaElabMessagio.getMessaggio().getDatiFlusso().getIdentificatoreProcesso();
		LOGGER.info("Identificatore Processo: " + idProcesso);
		
		final String protocolloRiferimento = getDocumentTitle(aooFilenet, tcp, idProcesso);
		LOGGER.info("Protocollo Riferimento: " + protocolloRiferimento);
		
		final List<AssegnazioneDTO> assegnazioni = getAssegnazioni(isProtocolloRifChiuso, aooFilenet, codiceAOO, idAOO, protocolloRiferimento);

		final UtenteDTO utenteCreatore = getUtenteProtocollatore(protocollo, protocolloRiferimento, assegnazioni);

		final EsitoSalvaDocumentoDTO esitoCreazione = alimentaProcesso(richiestaElabMessagio, protocollo, guidMail, idTipoDoc, idTipoProc, idAOO,
				true, tcp, capitoloRiservato, utenteCreatore, protocolloRiferimento, assegnazioni);

		if (esitoCreazione.isEsitoOk()) {
			final String documentTitleNotifica = esitoCreazione.getDocumentTitle();

			if (!isProtocolloRifChiuso) {
				final AssegnazioneDTO assegnazioneComp = getAssegnazioneCompetenza(aooFilenet, codiceAOO, documentTitleNotifica);

				// Invio della notifica di ricezione dell'integrazione dati spontanea
				LOGGER.info("integraDatiSpontanei -> Invio della notifica di ricezione dell'integrazione dati spontanea");
				inviaNotificaAssegnatarioIntegrazioneDati(documentTitleNotifica, assegnazioneComp);
			} else {
				final EventoLogDTO lastChiusuraIngressoEvent = eventoLogSRV.getLastChiusuraIngressoEvent(Integer.parseInt(protocolloRiferimento), idAOO);

				// Invio della notifica di ricezione dell'integrazione dati spontanea workflow
				// non attivo
				LOGGER.info("integraDatiSpontanei -> Invio della notifica di ricezione dell'integrazione dati spontanea workflow non attivo");
				inviaNotificaDestinatarioIntegrazioneDati(documentTitleNotifica, lastChiusuraIngressoEvent);
			}

			// Validazione dell'integrazione dati spontanea
			LOGGER.info("integraDatiSpontanei -> Validazione dell'integrazione dati spontanea");
			final EsitoOperazioneDTO esitoValidazioneIntegrazioneDati = integrazioneDatiSRV.validaIntegrazioneDati(utenteCreatore, esitoCreazione.getWobNumber(), false, true,
					statoNPS);

			if (!esitoValidazioneIntegrazioneDati.isEsito()) {
				esitoCreazione.setEsitoOk(false);
				esitoCreazione.getErrori().add(SalvaDocumentoErroreEnum.ERRORE_GENERICO);
				esitoCreazione.setNote("Errore nell'operazione di validazione dell'integrazione dati: " + esitoValidazioneIntegrazioneDati.getNote());
			}
		}

		return esitoCreazione;
	}

	private void aggiornaTipoDocumentoNPS(final UtenteDTO utente, final ProtocolloNpsDTO protocollo, final String idDocumento, final Integer idTipologiaDocumento,
			final Integer idTipoProcedimento) {
		final String infoProtocollo = new StringBuilder().append(protocollo.getNumeroProtocollo()).append("/").append(protocollo.getAnnoProtocollo()).append("_")
				.append(utente.getCodiceAoo()).toString();

		final Collection<MetadatoDTO> metadatiEstesi = tipoDocSRV.caricaMetadatiEstesiPerGUI(idTipologiaDocumento, idTipoProcedimento, false, utente.getIdAoo(),
				protocollo.getDataProtocollo());

		npsSRV.aggiornaDatiProtocollo(utente, infoProtocollo, protocollo.getIdProtocollo(), null, idTipologiaDocumento, idTipoProcedimento,
				getDescTipoDocFromID(idTipologiaDocumento), null, idDocumento, metadatiEstesi);
	}

	/**
	 * Integrazione dati: messaggio 102 (FLUSSO AUT/CG2) o 1002 (SICOGE). Oltre alla
	 * creazione dell'ingresso si procede con la validazione dell'integrazione
	 * appena ricevuta.
	 * 
	 * @param richiestaElabMessagio
	 * @param guidMail
	 * @param tcp
	 * @return
	 */
	protected EsitoSalvaDocumentoDTO riconciliaIntegrazioneDati(final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> richiestaElabMessagio,
			final String guidMail, final TipoContestoProceduraleEnum tcp, final boolean capitoloRiservato) {
		final Integer idAOO = richiestaElabMessagio.getIdAoo().intValue();
		final Aoo aoo = aooSRV.recuperaAoo(idAOO);
		final AooFilenet aooFilenet = aoo.getAooFilenet();
		final String codiceAOO = aoo.getCodiceAoo();

		// Recupero del protocollo completo da NPS con tutte le informazioni
		final ProtocolloNpsDTO protocollo = getDettagliProtocollo(idAOO, richiestaElabMessagio.getMessaggio().getProtocollo());

		// Recupero tipo documento e tipo procedimento
		final TipologiaDTO tipologia = getTipologiaFromTipologiaNPS(richiestaElabMessagio.getIdAoo(),
				richiestaElabMessagio.getMessaggio().getDatiFlusso().getMetadatiEstesi());
		final Integer idTipoDoc = tipologia.getIdTipoDocumento();
		final Integer idTipoProc = tipologia.getIdTipoProcedimento();
		
		final String idProcesso = richiestaElabMessagio.getMessaggio().getDatiFlusso().getIdentificatoreProcesso();
		
		final String protocolloRiferimento = getDocumentTitle(aooFilenet, tcp, idProcesso);
		
		final List<AssegnazioneDTO> assegnazioni = getAssegnazioni(false, aooFilenet, codiceAOO, idAOO, protocolloRiferimento);

		final UtenteDTO utenteCreatore = getUtenteProtocollatore(protocollo, protocolloRiferimento, assegnazioni);

		final EsitoSalvaDocumentoDTO esitoCreazione = alimentaProcesso(richiestaElabMessagio, protocollo, guidMail, idTipoDoc, idTipoProc, 
				idAOO, true, tcp, capitoloRiservato, utenteCreatore, protocolloRiferimento, assegnazioni);

		if (esitoCreazione.isEsitoOk()) {

			final String documentTitleNotifica = esitoCreazione.getDocumentTitle();

			final AssegnazioneDTO assegnazioneComp = getAssegnazioneCompetenza(aooFilenet, codiceAOO, protocolloRiferimento);

			// Invio della notifica di avvenuta ricezione dell'integrazione dati
			LOGGER.info("riconciliaIntegrazioneDati -> Invio della notifica di avvenuta ricezione dell'integrazione dati");
			inviaNotificaAssegnatarioIntegrazioneDati(documentTitleNotifica, assegnazioneComp);

			// Validazione dell'integrazione dati ricevuta
			LOGGER.info("riconciliaIntegrazioneDati -> Validazione dell'integrazione dati");
			final EsitoOperazioneDTO esitoValidazioneIntegrazioneDati = integrazioneDatiSRV.validaIntegrazioneDati(utenteCreatore, esitoCreazione.getWobNumber());

			if (!esitoValidazioneIntegrazioneDati.isEsito()) {
				esitoCreazione.setEsitoOk(false);
				esitoCreazione.getErrori().add(SalvaDocumentoErroreEnum.ERRORE_GENERICO);
				esitoCreazione.setNote("Errore nell'operazione di validazione dell'integrazione dati: " + esitoValidazioneIntegrazioneDati.getNote());
			}
		}

		return esitoCreazione;
	}

	/**
	 * Invio notifica assegnatario per competenza del documento. In caso di
	 * assegnazione ad ufficio, notifica il dirigente dell'ufficio.
	 * 
	 * @param documentTitleNotifica
	 * @param assegnazioneComp
	 */
	protected void inviaNotificaAssegnatarioIntegrazioneDati(final String documentTitleNotifica, final AssegnazioneDTO assegnazioneComp) {
		final Long idUtenteAssegnatarioComp = assegnazioneComp.getUtente().getId();
		final Long idUfficioAssegnatarioComp = assegnazioneComp.getUtente().getIdUfficio();
		final Nodo ufficioAssegnatarioComp = nodoSRV.getNodo(assegnazioneComp.getUfficio().getId());
		final Aoo aooAssegnatarioComp = ufficioAssegnatarioComp.getAoo();

		scriviNotificaIntegrazioneDati(idUtenteAssegnatarioComp, idUfficioAssegnatarioComp, ufficioAssegnatarioComp, aooAssegnatarioComp, documentTitleNotifica);
	}

	/**
	 * Invia la notifica al destinatario dell'integrazione dati (l'esecutore
	 * dell'evento di chiusura del documento).
	 * 
	 * @param documentTitleNotifica
	 * @param lastChiusuraIngressoEvent
	 */
	protected void inviaNotificaDestinatarioIntegrazioneDati(final String documentTitleNotifica, final EventoLogDTO lastChiusuraIngressoEvent) {
		final Long idUtenteDestinatario = lastChiusuraIngressoEvent.getIdUtenteMittente();
		final Long idUfficioDestinatario = lastChiusuraIngressoEvent.getIdUfficioMittente();
		final Nodo ufficioDestinatario = nodoSRV.getNodo(idUfficioDestinatario);
		final Aoo aooDestinatario = ufficioDestinatario.getAoo();

		scriviNotificaIntegrazioneDati(idUtenteDestinatario, idUfficioDestinatario, ufficioDestinatario, aooDestinatario, documentTitleNotifica);
	}

	/**
	 * Notifica l'ingresso di un'integrazione dati.
	 * 
	 * @param idUtente
	 * @param idUfficio
	 * @param ufficio
	 * @param aoo
	 * @param documentTitleNotifica
	 */
	private void scriviNotificaIntegrazioneDati(final Long idUtente, final Long idUfficio, final Nodo ufficio, final Aoo aoo, final String documentTitleNotifica) {
		if (idUtente != null && !Long.valueOf(0).equals(idUtente)) {
			final NotificaRicezioneIntegrazioneDatiDTO notifica = new NotificaRicezioneIntegrazioneDatiDTO(documentTitleNotifica, idUfficio, idUtente);
			notificaRicezioneIntegrazioneDatiSRV.writeNotifiche(aoo.getIdAoo().intValue(), notifica);
		} else {
			if (ufficio.getDirigente() != null) {
				final UtenteDTO dirigente = new UtenteDTO(ufficio.getDirigente());

				final NotificaRicezioneIntegrazioneDatiDTO notifica = new NotificaRicezioneIntegrazioneDatiDTO(documentTitleNotifica, idUfficio, dirigente.getId());
				notificaRicezioneIntegrazioneDatiSRV.writeNotifiche(aoo.getIdAoo().intValue(), notifica);
			} else {
				LOGGER.warn("Impossibile inviare la notifica di ingresso integrazione dati: dirigente non configurato per l'ufficio " + idUfficio + " dell'AOO "
						+ aoo.getCodiceAoo());
			}
		}
	}

	/**
	 * Crea un nuovo processo utilizzando la request: {@code richiestaElabMessagio}.
	 * 
	 * @param richiestaElabMessagio
	 *            request elaborazione messaggio
	 * @param guidMail
	 *            guid della mail
	 * @param tcp
	 *            tipologia del contesto procedurale che definisce il flusso
	 * @return esito della creazione
	 */
	protected EsitoSalvaDocumentoDTO creaNuovoProcesso(final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> richiestaElabMessagio, final String guidMail,
			final TipoContestoProceduraleEnum tcp) {
		return creaNuovoProcesso(richiestaElabMessagio, guidMail, tcp, null, false);
	}

	/**
	 * Crea un nuovo processo utilizzando la request: {@code richiestaElabMessagio}.
	 * 
	 * @param richiestaElabMessagio
	 *            request elaborazione messaggio
	 * @param guidMail
	 *            guid della mail
	 * @param tcp
	 *            tipologia del contesto procedurale che definisce il flusso
	 * @param fascicoloSelezionato
	 *            fascicolo selezionato
	 * @param capitoloRiservato
	 *            flag associato al capitolo riservato
	 * @return esito della creazione
	 */
	protected EsitoSalvaDocumentoDTO creaNuovoProcesso(final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> richiestaElabMessagio, final String guidMail,
			final TipoContestoProceduraleEnum tcp, final FascicoloDTO fascicoloSelezionato, final boolean capitoloRiservato) {
		final ProtocolloNpsDTO protocollo = getDettagliProtocollo(richiestaElabMessagio.getIdAoo().intValue(), richiestaElabMessagio.getMessaggio().getProtocollo());
		final ProtocolloNpsDTO protocolloNotifica = getDettagliProtocolloNotifica(richiestaElabMessagio);
		final TipologiaDTO tipologia = getTipologiaFromTipologiaNPS(richiestaElabMessagio.getIdAoo(),
				richiestaElabMessagio.getMessaggio().getDatiFlusso().getMetadatiEstesi());
		final Integer idTipoDoc = tipologia.getIdTipoDocumento();
		final Integer idTipoProc = tipologia.getIdTipoProcedimento();

		final String oggetto = protocollo.getOggetto(); // Recupero oggetto

		final String idProcesso = richiestaElabMessagio.getMessaggio().getDatiFlusso().getIdentificatoreProcesso();
		LOGGER.info("creaNuovoProcesso -> Identificatore Processo: " + idProcesso);

		final UtenteDTO utenteCreatore = getUtenteProtocollatore(protocollo);

		final String mail = (richiestaElabMessagio.getMessaggio() != null && richiestaElabMessagio.getMessaggio().getMittente() != null)
				? richiestaElabMessagio.getMessaggio().getMittente().getMail()
				: null;
		final Contatto contattoMittente = getMittente(protocollo.getMittente(), utenteCreatore, mail);
		final Integer idMittenteContatto = contattoMittente.getContattoID().intValue();

		final List<AssegnazioneDTO> assegnazioni = new ArrayList<>();

		final String indiceClassificazioneFascicoloProcedimentale = !FilenetStatoFascicoloEnum.NON_CATALOGATO.getNome().equalsIgnoreCase(protocollo.getDescrizioneTitolario())
				? protocollo.getDescrizioneTitolario()
				: null;
		final String descTipoDoc = getDescTipoDocFromID(idTipoDoc);
		final String descrizioneFascicoloProcedimentale = descTipoDoc + " - " + oggetto;

		byte[] content = null;
		try {
			content = IOUtils.toByteArray(protocollo.getDocumentoPrincipale().getInputStream());
		} catch (final IOException e) {
			LOGGER.error("creaNuovoProcesso -> Errore in fase di recupero content del documento principale.", e);
			throw new RedException("Errore in fase di recupero content del documento principale.", e);
		}
		final String fileName = protocollo.getDocumentoPrincipale().getNomeFile();
		final String mType = protocollo.getDocumentoPrincipale().getContentType();

		final String protocolloRiferimento = null;

		return creaIngresso(tcp, oggetto, utenteCreatore, idMittenteContatto, idTipoDoc, idTipoProc, assegnazioni, protocolloRiferimento, content, fileName, mType,
				fascicoloSelezionato, idProcesso, guidMail, indiceClassificazioneFascicoloProcedimentale, descrizioneFascicoloProcedimentale, protocollo, protocolloNotifica,
				richiestaElabMessagio.getMessaggio().getDatiFlusso(), false, capitoloRiservato);
	}

	/**
	 * Metodo per la creazione di un messaggio in ingresso per messaggi non
	 * iniziali.
	 * 
	 * @param richiestaElabMessagio
	 * @param protocollo
	 * @param guidMail
	 * @param idTipoDoc
	 * @param idTipoProc
	 * @param idAOO
	 * @param codiceAOO
	 * @param isIntegrazioneDati
	 * @param tcp
	 * @param isProtocolloRifChiuso
	 * @return esito del salvataggio del documento
	 */
	private EsitoSalvaDocumentoDTO alimentaProcesso(final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> richiestaElabMessagio,
			final ProtocolloNpsDTO protocollo, final String guidMail, final Integer idTipoDoc, final Integer idTipoProc, final Integer idAOO, 
			final boolean isIntegrazioneDati, final TipoContestoProceduraleEnum tcp, final boolean capitoloRiservato,
			final UtenteDTO utenteCreatore, final String protocolloRiferimento, final List<AssegnazioneDTO> assegnazioni) {
		
		final Aoo aoo = aooSRV.recuperaAoo(idAOO);
		final AooFilenet aooFilenet = aoo.getAooFilenet();
		
		final String mail = (richiestaElabMessagio.getMessaggio() != null && richiestaElabMessagio.getMessaggio().getMittente() != null)
				? richiestaElabMessagio.getMessaggio().getMittente().getMail()
				: null;
		final Contatto contattoMittente = getMittente(protocollo.getMittente(), utenteCreatore, mail);

		final String oggetto = protocollo.getOggetto(); // Recupero oggetto

		final Integer idMittenteContatto = contattoMittente.getContattoID().intValue();

		final FascicoloDTO fascicoloSelezionato = getFascicoloProcedimentale(idAOO, protocolloRiferimento, utenteCreatore, aooFilenet);

		byte[] content = null;
		try {
			content = IOUtils.toByteArray(protocollo.getDocumentoPrincipale().getInputStream());
		} catch (final IOException e) {
			LOGGER.error("Errore in fase di recupero content del documento principale.", e);
			throw new RedException("alimentaProcesso -> Errore in fase di recupero content del documento principale.", e);
		}
		final String fileName = protocollo.getDocumentoPrincipale().getNomeFile();
		final String mType = protocollo.getDocumentoPrincipale().getContentType();

		return creaIngresso(tcp, oggetto, utenteCreatore, idMittenteContatto, idTipoDoc, idTipoProc, assegnazioni, protocolloRiferimento, content, fileName, mType,
				fascicoloSelezionato, null, guidMail, null, null, protocollo, null, richiestaElabMessagio.getMessaggio().getDatiFlusso(), isIntegrazioneDati, capitoloRiservato);
	}
	
	private List<AssegnazioneDTO> getAssegnazioni(final boolean isProtocolloRifChiuso, final AooFilenet aooFilenet, final String codiceAOO, final Integer idAOO, final String protocolloRiferimento){
		final List<AssegnazioneDTO> assegnazioni = new ArrayList<>();

		if (!isProtocolloRifChiuso) {
			final AssegnazioneDTO assegnazioneComp = getAssegnazioneCompetenza(aooFilenet, codiceAOO, protocolloRiferimento);
			assegnazioni.add(assegnazioneComp);
		} else {
			final EventoLogDTO lastChiusuraIngressoEvent = eventoLogSRV.getLastChiusuraIngressoEvent(Integer.parseInt(protocolloRiferimento), idAOO);
			final AssegnazioneDTO respChiusuraProtocolloRif = documentoRedSRV.eventoLogDTO2AssegnazioneDTO(lastChiusuraIngressoEvent);
			respChiusuraProtocolloRif.setTipoAssegnazione(TipoAssegnazioneEnum.COMPETENZA);

			assegnazioni.add(respChiusuraProtocolloRif);
		}
		
		return assegnazioni;
	}

	/**
	 * Recupera il fascicolo procedimentale del documento identificato da
	 * documentTitle.
	 * 
	 * @param idAOO
	 * @param documentTitle
	 * @param utenteCreatore
	 * @return
	 */
	private FascicoloDTO getFascicoloProcedimentale(final Integer idAOO, final String documentTitle, final UtenteDTO utenteCreatore, final AooFilenet aooFilenet) {
		return fascicoloSRV.getFascicoloProcedimentale(documentTitle, idAOO, utenteCreatore, aooFilenet);
	}

	/**
	 * Recupera l'assegnazione per competenza del documento identificato da
	 * documentTitle.
	 * 
	 * @param utenteCreatore
	 * @param codiceAOO
	 * @param documentTitle
	 * @return
	 */
	private static AssegnazioneDTO getAssegnazioneCompetenza(final AooFilenet aooFilenet, final String codiceAOO, final String documentTitle) {
		AssegnazioneDTO assegnazioneComp = null;
		FilenetPEHelper fpeh = null;

		try {
			fpeh = new FilenetPEHelper(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(),
					aooFilenet.getStanzaJaas(), aooFilenet.getConnectionPoint());

			final List<AssegnazioneDTO> assegnazioni = fpeh.getAssegnazioni(documentTitle, codiceAOO, aooFilenet.getIdClientAoo());
			for (final AssegnazioneDTO assegnazione : assegnazioni) {
				if (TipoAssegnazioneEnum.COMPETENZA.equals(assegnazione.getTipoAssegnazione())) {
					assegnazioneComp = assegnazione;
					break;
				}
			}
		} finally {
			if (fpeh != null) {
				fpeh.logoff();
			}
		}

		return assegnazioneComp;
	}

	/**
	 * Recupera il documentTitle del documento associato ad idProcesso.
	 * 
	 * @param utenteCreatore
	 * @param tcp
	 * @param idProcesso
	 * @return
	 */
	protected String getDocumentTitle(final AooFilenet aooFilenet, final TipoContestoProceduraleEnum tcp, final String idProcesso) {
		return protocollaFlussoSRV.getDocumentTitleByIdProcesso(aooFilenet, tcp, idProcesso);
	}

	/**
	 * Recupera, se esiste, il workflow number del workflow nella coda SOSPESO a cui
	 * afferisce il documentTitle.
	 * 
	 * @param documentTitle
	 * @param aooFilenet
	 * @return
	 */
	protected String getWobNumberSospeso(final String documentTitle, final AooFilenet aooFilenet) {
		return protocollaFlussoSRV.getWobNumberSospeso(documentTitle, aooFilenet);
	}

	/**
	 * @param documentTitle
	 * @param aooFilenet
	 * @return
	 */
	protected String getWorkflowPrincipale(final String documentTitle, final AooFilenet aooFilenet) {
		return protocollaFlussoSRV.getQueueNameWorkflowPrincipale(documentTitle, aooFilenet);
	}

	/**
	 * Recupera il primo tipo procedimento del tipo documento fornito in input.
	 * 
	 * @param idTipoDocRitiroAutotutela
	 * @return
	 */
	private Integer getFirstTipoProc(final Integer idTipoDocRitiroAutotutela) {
		final List<TipoProcedimento> tipiProcedimento = documentManagerSRV.getTipiProcedimentoByTipologiaDocumento(idTipoDocRitiroAutotutela, null);
		final TipoProcedimento tipoProc = tipiProcedimento.get(0);
		return (int) tipoProc.getTipoProcedimentoId();
	}

	/**
	 * Ritiro di un documento (messaggio 104).
	 * 
	 * @param richiestaElabMessagio
	 * @param guidMail
	 * @param idAOO
	 * @param codiceAOO
	 * @return
	 */
	protected EsitoSalvaDocumentoDTO ritiraDocumento(final RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> richiestaElabMessagio, final String guidMail, final TipoContestoProceduraleEnum tcp) {
		final Integer idAOO = richiestaElabMessagio.getIdAoo().intValue();
		final Aoo aoo = aooSRV.recuperaAoo(idAOO);
		final AooFilenet aooFilenet = aoo.getAooFilenet();
		final String codiceAOO = aoo.getCodiceAoo();

		// Recupero del protocollo completo da NPS con tutte le informazioni
		final ProtocolloNpsDTO protocollo = getDettagliProtocollo(idAOO, richiestaElabMessagio.getMessaggio().getProtocollo());

		// Recupero tipo documento e tipo procedimento
		final Integer idTipoDocRitiroAutotutela = getIdTipoDocRitiroAutotutela(codiceAOO);
		final Integer idTipoProcRitiroAutotutela = getFirstTipoProc(idTipoDocRitiroAutotutela);
		
		final String idProcesso = richiestaElabMessagio.getMessaggio().getDatiFlusso().getIdentificatoreProcesso();
		
		final String protocolloRiferimento = getDocumentTitle(aooFilenet, tcp, idProcesso);
		
		final List<AssegnazioneDTO> assegnazioni = getAssegnazioni(false, aooFilenet, codiceAOO, idAOO, protocolloRiferimento);

		final UtenteDTO utenteCreatore = getUtenteProtocollatore(protocollo, protocolloRiferimento, assegnazioni);
		
		final EsitoSalvaDocumentoDTO esitoCreazione = alimentaProcesso(richiestaElabMessagio, protocollo, guidMail, idTipoDocRitiroAutotutela, idTipoProcRitiroAutotutela,
				idAOO, false, tcp, false, utenteCreatore, protocolloRiferimento, assegnazioni);

		if (esitoCreazione.isEsitoOk()) {
			
			final String documentTitleNotifica = esitoCreazione.getDocumentTitle();

			LOGGER.info("ritiraDocumento -> Comunica ad NPS il cambio tipo documento");
			aggiornaTipoDocumentoNPS(utenteCreatore, protocollo, documentTitleNotifica, idTipoDocRitiroAutotutela, idTipoProcRitiroAutotutela);

			final AssegnazioneDTO assegnazioneComp = getAssegnazioneCompetenza(aooFilenet, codiceAOO, documentTitleNotifica);
			
			if(assegnazioneComp != null) {
				if (assegnazioneComp.getUtente() != null) {
	
					final UtenteDTO assegnatario = assegnazioneComp.getUtente();
					assegnatario.setIdAoo(idAOO.longValue());
	
					final NotificaRitiroAutotutelaDTO notifica = new NotificaRitiroAutotutelaDTO(Long.parseLong(documentTitleNotifica), assegnatario.getIdUfficio(),
							assegnatario.getId());
					notificaRitiroAutotutelaSRV.writeNotifiche(assegnatario.getIdAoo().intValue(), notifica);
				} else if (assegnazioneComp.getUfficio() != null) {
					// invia notifica a dirigente ufficio assegnatario per competenza
					final Nodo nodoCompetenza = nodoSRV.getNodo(assegnazioneComp.getUfficio().getId());
					
					if (nodoCompetenza != null) {
						final UtenteDTO dirigente = new UtenteDTO(nodoCompetenza.getDirigente());
						final NotificaRitiroAutotutelaDTO notifica = new NotificaRitiroAutotutelaDTO(
								Long.parseLong(documentTitleNotifica), nodoCompetenza.getIdNodo(), dirigente.getId());
						notificaRitiroAutotutelaSRV.writeNotifiche(dirigente.getIdAoo().intValue(), notifica);
					}
	
				}
			}
		}

		return esitoCreazione;
	}

	/**
	 * Recupera l'id del tipo documento ritiro in autotutela.
	 * 
	 * @param codiceAOO
	 * @return
	 */
	private static Integer getIdTipoDocRitiroAutotutela(final String codiceAOO) {
		final String tipoRitiroAutotutela = PropertiesProvider.getIstance()
				.getParameterByString(codiceAOO + "." + PropertiesNameEnum.ID_TIPOLOGIA_DOCUMENTO_ENTRATA_RITIRO_IN_AUTOTUTELA.getKey());
		return Integer.valueOf(tipoRitiroAutotutela);

	}

	/**
	 * Recupera i dettagli del protocollo associato alla notifica.
	 * @param richiestaElabMessaggio informazioni della request
	 * @return informazioni sul protocollo notifica
	 */
	private ProtocolloNpsDTO getDettagliProtocolloNotifica(RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> richiestaElabMessaggio) {
		IFilenetCEHelper fceh = null;
		ProtocolloNpsDTO protocolloNotifica = null;
		Integer annoProtocollo = null;
		Integer numeroProtocollo = null;
		int tipoProtocollo = 0;
		String idProtocollo = null;
		
		try {
			final AooFilenet aooFilenet = aooSRV.recuperaAoo(richiestaElabMessaggio.getIdAoo().intValue()).getAooFilenet();
			fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getObjectStore(), richiestaElabMessaggio.getIdAoo());
			
			if(richiestaElabMessaggio.getMessaggio().getProtocolloNotifica()!= null) {
				if(richiestaElabMessaggio.getMessaggio().getProtocolloNotifica().getTipoProtocollo() != null) {
					tipoProtocollo = TipoProtocolloEnum.getByName(richiestaElabMessaggio.getMessaggio().getProtocolloNotifica().getTipoProtocollo()).getId();
				}
				if(richiestaElabMessaggio.getMessaggio().getProtocolloNotifica().getAnnoProtocollo() != null) {
					annoProtocollo = Integer.parseInt(richiestaElabMessaggio.getMessaggio().getProtocolloNotifica().getAnnoProtocollo());
				}
				numeroProtocollo = richiestaElabMessaggio.getMessaggio().getProtocolloNotifica().getNumeroProtocollo();
				idProtocollo = richiestaElabMessaggio.getMessaggio().getProtocolloNotifica().getIdProtocollo();
			}
			
			protocolloNotifica = recuperaProtocolloNotifica(richiestaElabMessaggio, fceh, tipoProtocollo, annoProtocollo, numeroProtocollo, idProtocollo);
			
		} catch (Exception ex) {
			LOGGER.error("Errore durante il recupero del protocollo notifica con id: " + (numeroProtocollo != null ? numeroProtocollo.toString() : MISSING_PROTOCOLLO), ex);
			throw new RedException("Errore durante il recupero del protocollo notifica con id: " + (numeroProtocollo != null ? numeroProtocollo.toString() : MISSING_PROTOCOLLO), ex);
		} finally {
			if(fceh != null) {
				fceh.popSubject();
				
			}
		}
		
		return protocolloNotifica;
	}

	/**
	 * Restituisce ProtocolloNpsDTO recuperato da NPS se esistente. Restituisce null se il protocollo non esiste.
	 * @param richiestaElabMessaggio
	 * @param fceh
	 * @param tipoProtocollo
	 * @param annoProtocollo
	 * @param numeroProtocollo
	 * @param idProtocollo
	 * @return ProtocolloNpsDTO se esistente, null altrimenti
	 */
	private ProtocolloNpsDTO recuperaProtocolloNotifica(RichiestaElabMessaggioPostaNpsDTO<MessaggioPostaNpsFlussoDTO> richiestaElabMessaggio, IFilenetCEHelper fceh,
			int tipoProtocollo, Integer annoProtocollo, Integer numeroProtocollo, String idProtocollo) {
		
		Document documentNotifica = null;
		ProtocolloNpsDTO protocolloNotifica = null;
		try {
			documentNotifica = notificaNpsSRV.getDocFileNetByProtocollo(idProtocollo, numeroProtocollo, annoProtocollo, tipoProtocollo, fceh, richiestaElabMessaggio.getIdAoo().intValue());
		} catch (Exception e) {
			LOGGER.warn("Nessun protocollo notifica associato al protocollo: " + (numeroProtocollo != null ? numeroProtocollo.toString() : MISSING_PROTOCOLLO), e);
		}
		
		
		// Se il protocollo esiste
		if (documentNotifica != null) {
			protocolloNotifica = new ProtocolloNpsDTO();
			try {
				protocolloNotifica.setIdDocumento((String)TrasformerCE.getMetadato(documentNotifica, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
				protocolloNotifica.setIdProtocollo((String)TrasformerCE.getMetadato(documentNotifica, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY));
				protocolloNotifica.setAnnoProtocollo(TrasformerCE.getMetadato(documentNotifica, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY).toString());
				protocolloNotifica.setDataProtocollo((Date)TrasformerCE.getMetadato(documentNotifica, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY));
				
			} catch (Exception ex) {
				LOGGER.error("Errore riscontrato durante la trasformazione del document in ProtocolloNpsDTO.", ex);
				throw new RedException("Errore riscontrato durante la trasformazione del document in ProtocolloNpsDTO.", ex);
			}
		}
		return protocolloNotifica;
	}
	
	/**
	 * Indica se il documento in input è un documento riservato.
	 * 
	 * @param documentTitle
	 * @param aooFilenet
	 * @return
	 */
	private boolean isProtocolloRiservato(final String documentTitle, final AooFilenet aooFilenet) {
		IFilenetCEHelper fceh = null;
				
		try {
			fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getObjectStore(), aooFilenet.getAoo().getIdAoo());
			
			Document document = fceh.getDocumentByDTandAOO(documentTitle, aooFilenet.getAoo().getIdAoo());
			
			final Integer riservato = (Integer) TrasformerCE.getMetadato(document, PropertiesNameEnum.RISERVATO_METAKEY);
			boolean flagRiservato = false;
			if (riservato != null && riservato > 0) {
				flagRiservato = true;
			}
			
			return flagRiservato;
			
		} catch (Exception ex) {
			LOGGER.error("Errore durante il recupero del della riservatezza del documento: " + (documentTitle != null ? documentTitle : MISSING_DT), ex);
			throw new RedException("Errore durante il recupero del della riservatezza del documento: " + (documentTitle != null ? documentTitle : MISSING_DT), ex);
		} finally {
			if(fceh != null) {
				fceh.popSubject();
			}
		}
		
	}
	
}
