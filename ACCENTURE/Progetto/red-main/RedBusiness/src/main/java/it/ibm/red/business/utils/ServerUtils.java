package it.ibm.red.business.utils;

import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;

import it.ibm.red.business.logger.REDLogger;

/**
 * The Class ServerUtils.
 *
 * @author a.dilegge
 * 
 *         Utility per la gestione delle informazioni del server.
 */
public final class ServerUtils {
	
	/**
	 * Logger.
	 */
	private static REDLogger logger = REDLogger.getLogger(ServerUtils.class);	

	/**
	 * Costruttore.
	 */
	private ServerUtils() {
	}

	/**
	 * Metodo che restituisce il ServerName WebSphere su cui gira l'applicativo enterprise.
	 * In caso non si riesca ad evincere il nome del server, si recupera l'IP address.
	 * 
	 * @return	il nome o l'indirizzo IP del server
	 */
	public static String getServerFullName() {
		String serverName = null;
		
		try {
			Class<?> c = Class.forName("com.ibm.websphere.runtime.ServerName");
			Method m = c.getMethod("getFullName", new Class<?>[0]);
			Object o = m.invoke(null, new Object[0]);
			serverName = o.toString();
		} catch (ClassNotFoundException ex) {
			logger.warn("Errore in fase di recupero del nome del server ", ex);
			serverName = getHostName();
		} catch (Exception ex) {
			logger.warn("Errore in fase di recupero del nome del server ", ex);
			serverName = getHostName();
		}
		
		return serverName;
	}
	
	private static String getHostName() {
		try {
			return InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			logger.warn("Errore nel recupero del nome della macchina: ", e);
			return "unknown";
		}
	}

}
