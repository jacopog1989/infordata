package it.ibm.red.business.dto;

import java.util.Date;

/**
 * @author SimoneLungarella
 * DTO per la gestione dei risultati della ricerca elenco notifica e conti consuntivi.
 */
public class RisultatoRicercaContiConsuntiviDTO extends AbstractDTO{

	/**
	 * La costante serial version UID.
	 */
	private static final long serialVersionUID = -5769778397195318960L;

	/**
	 * Codice sede estera associata al risultato.
	 */
	private String codiceSedEstera;
	
	/**
	 * Descrizione della sede estera associata al {@link #codiceSedEstera}.
	 */
	private String descrizioneSedeEstera;
	
	/**
	 * Anno di riferimento.
	 */
	private int esercizio;
	
	/**
	 * Data Prot. RGS della notifica.
	 */
	private Date dataNotifica;
	
	/**
	 * Numero Prot. RGS della notifica.
	 */
	private Integer numeroNotifica;
	
	/**
	 * Data Prot. RGS del documento.
	 */
	private Date dataDocumento;
	
	/**
	 * Numero Prot. RGS del documento.
	 */
	private Integer numeroDocumento;
	
	/**
	 * Descrizione dell'assegnatario.
	 */
	private String descrizioneAssegnatario;
	
	/**
	 * Stato.
	 */
	private String stato;
	
	/**
	 * Programma controllo UCB.
	 */
	private String controlloUcb;
	
	/**
	 * Data chiusura.
	 */
	private Date dataChiusura;

	/**
	 * Costruttore vuoto del DTO.
	 */
	public RisultatoRicercaContiConsuntiviDTO() {
		super();
	}

	/**
	 * Restituisce il codice della sede estera.
	 * @return codice sede estera
	 */
	public String getCodiceSedEstera() {
		return codiceSedEstera;
	}

	/**
	 * Imposta il codice della sede estera.
	 * @param codiceSedEstera
	 */
	public void setCodiceSedEstera(String codiceSedEstera) {
		this.codiceSedEstera = codiceSedEstera;
	}

	/**
	 * Restituisce la descrizione della sede estera.
	 * @return descrizione sede estera
	 */
	public String getDescrizioneSedeEstera() {
		return descrizioneSedeEstera;
	}

	/**
	 * Imposta la descrizione della sede estera.
	 * @param descrizioneSedeEstera
	 */
	public void setDescrizioneSedeEstera(String descrizioneSedeEstera) {
		this.descrizioneSedeEstera = descrizioneSedeEstera;
	}

	/**
	 * Restituisce l'anno dell'esercizio.
	 * @return anno esercizio
	 */
	public int getEsercizio() {
		return esercizio;
	}

	/**
	 * Imposta l'anno dell'esercizio.
	 * @param esercizio
	 */
	public void setEsercizio(int esercizio) {
		this.esercizio = esercizio;
	}

	/**
	 * Restituisce la data di protocollazione della notifica.
	 * @return data Prot. RGS Notifica
	 */
	public Date getDataNotifica() {
		return dataNotifica;
	}

	/**
	 * Imposta la data di protocollazione della notifica.
	 * @param dataNotifica
	 */
	public void setDataNotifica(Date dataNotifica) {
		this.dataNotifica = dataNotifica;
	}

	/**
	 * Restituisce il numero della notifica.
	 * @return numero notifica
	 */
	public Integer getNumeroNotifica() {
		return numeroNotifica;
	}

	/**
	 * Imposta il numero della notifica.
	 * @param numeroNotifica
	 */
	public void setNumeroNotifica(Integer numeroNotifica) {
		this.numeroNotifica = numeroNotifica;
	}

	/**
	 * Restituisce la data di protocollazione del documento.
	 * @return data Prot. RGS docuemnto
	 */
	public Date getDataDocumento() {
		return dataDocumento;
	}

	/**
	 * Imposta la data di protocollazione del documento.
	 * @param dataDocumento
	 */
	public void setDataDocumento(Date dataDocumento) {
		this.dataDocumento = dataDocumento;
	}

	/**
	 * Restituisce il numero del documento.
	 * @return numero documento
	 */
	public Integer getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * Imposta il numero del documento.
	 * @param numeroDocumento
	 */
	public void setNumeroDocumento(Integer numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * Restituisce la descrizione dell'assegnatario.
	 * @return descrizione assegnatario
	 */
	public String getDescrizioneAssegnatario() {
		return descrizioneAssegnatario;
	}

	/**
	 * Imposta la descrizione dell'assegnatario.
	 * @param descrizioneAssegnatario
	 */
	public void setDescrizioneAssegnatario(String descrizioneAssegnatario) {
		this.descrizioneAssegnatario = descrizioneAssegnatario;
	}

	/**
	 * Restituisce lo stato.
	 * @return stato
	 */
	public String getStato() {
		return stato;
	}

	/**
	 * Imposta lo stato.
	 * @param stato
	 */
	public void setStato(String stato) {
		this.stato = stato;
	}

	/**
	 * Restituisce il programma controllo UCB.
	 * @return programma controllo ucb
	 */
	public String getControlloUcb() {
		return controlloUcb;
	}

	/**
	 * Imposta il programma controllo UCB.
	 * @param controlloUcb
	 */
	public void setControlloUcb(String controlloUcb) {
		this.controlloUcb = controlloUcb;
	}

	/**
	 * Restituisce la data di chiusura.
	 * @return data chiusura
	 */
	public Date getDataChiusura() {
		return dataChiusura;
	}

	/**
	 * Imposta la data di chiusura.
	 * @param dataChiusura
	 */
	public void setDataChiusura(Date dataChiusura) {
		this.dataChiusura = dataChiusura;
	}
}
