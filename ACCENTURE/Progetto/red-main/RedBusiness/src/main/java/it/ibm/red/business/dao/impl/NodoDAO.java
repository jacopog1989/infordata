package it.ibm.red.business.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.utils.StringUtils;

/**
 * Dao per la gestione dei nodi.
 *
 * @author CPIERASC
 */
@Repository
public class NodoDAO extends AbstractDAO implements INodoDAO {

	private static final String IDNODOPADRE = "IDNODOPADRE";

	private static final String DESCRIZIONE = "DESCRIZIONE";

	private static final String CODICENODO = "CODICENODO";

	private static final String IDNODO = "IDNODO";

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = -7648697982856381781L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NodoDAO.class.getName());

	/**
	 * Dao per la gestione delle aoo.
	 */
	@Autowired
	private IAooDAO aooDAO;

	/**
	 * Dao per la gestione degli utenti.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;

	/**
	 * Gets the rag gen dello stato.
	 *
	 * @param con
	 *            the con
	 * @return the rag gen dello stato
	 */
	@Override
	public final Integer getRagGenDelloStato(final Connection con) {
		Integer output = null;
		CallableStatement cs = null;
		try {
			int index = 1;
			cs = con.prepareCall("{call PKG_NSD_PE_RGS.getRagGenDelloStato(?,?)}");
			cs.registerOutParameter(index++, Types.INTEGER);
			cs.registerOutParameter(index++, Types.INTEGER);
			cs.execute();
			output = cs.getInt(2);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del ragionere generale dello stato", e);
			throw new RedException("Errore durante il recupero del ragionere generale dello stato", e);
		} finally {
			close(cs);
		}
		return output;
	}

	/**
	 * Gets the firmatario iter approvativo.
	 *
	 * @param idUfficioUtente
	 *            the id ufficio utente
	 * @param idTipoFirma
	 *            the id tipo firma
	 * @param con
	 *            the con
	 * @return the firmatario iter approvativo
	 */
	@Override
	public final String getFirmatarioIterApprovativo(final Long idUfficioUtente, final Integer idTipoFirma, final Connection con) {
		String output = null;
		CallableStatement cs = null;
		try {
			int index = 0;
			cs = con.prepareCall("{call PKG_NSD_PE_RGS.getfirmatario(?,?,?,?)}");
			cs.setInt(++index, idUfficioUtente.intValue());
			cs.setInt(++index, idTipoFirma);
			cs.registerOutParameter(++index, Types.INTEGER);
			cs.registerOutParameter(++index, Types.INTEGER);
			cs.execute();
			output = cs.getInt(index - 1) + "," + cs.getInt(index);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del firmatario dell'iter approvativo", e);
			throw new RedException("Errore durante il recupero del firmatario dell'iter approvativo", e);
		} finally {
			close(cs);
		}
		return output;
	}

	/**
	 * Gets the nodo.
	 *
	 * @param idNodo
	 *            the id nodo
	 * @param connection
	 *            the connection
	 * @return the nodo
	 */
	@Override
	public final Nodo getNodo(final Long idNodo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			final String querySQL = "SELECT n.* FROM Nodo n WHERE n.idnodo=" + sanitize(idNodo);
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			if (rs.next()) {
				final Long idNodoDB = rs.getLong(IDNODO);
				final Date dataAttivazione = rs.getDate("DATAATTIVAZIONE");
				final Date dataDisattivazione = rs.getDate("DATADISATTIVAZIONE");
				final String codiceNodo = rs.getString(CODICENODO);
				final String descrizione = rs.getString(DESCRIZIONE);
				final Long idAoo = rs.getLong("IDAOO");
				final Long idUtenteDirigente = rs.getLong("IDUTENTEDIRIGENTE");
				final Integer flagSegreteria = rs.getInt("FLAGSEGRETERIA");
				final Long idNodoPadre = rs.getLong(IDNODOPADRE);
				final Integer idTipoNodo = rs.getInt("IDTIPONODO");
				final Long idNodoCorriere = rs.getLong("IDNODOCORRIERE");
				final Integer idTipoStruttura = rs.getInt("IDTIPOSTRUTTURA");
				final boolean isEstendiVisibilita = rs.getInt("IS_ESTENDI_VISIBILITA") == 1;

				closeStatement(ps, rs);
				ps = null;
				rs = null;

				final Aoo aoo = aooDAO.getAoo(idAoo, connection);
				final Utente utenteDirigente = utenteDAO.getUtente(idUtenteDirigente, connection);
				return new Nodo(idNodoDB, idNodoPadre, dataAttivazione, dataDisattivazione, aoo, codiceNodo, descrizione, utenteDirigente, flagSegreteria, idTipoNodo,
						idNodoCorriere, idTipoStruttura, isEstendiVisibilita);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del Nodo tramite id=" + idNodo, e);
			throw new RedException("Errore durante il recupero del Nodo tramite id=" + idNodo, e);
		} finally {
			closeStatement(ps, rs);
		}
		return null;
	}

	/**
	 * Gets the nodo corriere id.
	 *
	 * @param idNodo
	 *            the id nodo
	 * @param connection
	 *            the connection
	 * @return the nodo corriere id
	 */
	@Override
	public final Long getNodoCorriereId(final Long idNodo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long idNodoPadre = 0L;
		try {
			int index = 1;
			ps = connection.prepareStatement("SELECT idnodocorriere FROM nodo WHERE idnodo = ?");
			ps.setLong(index++, idNodo);
			rs = ps.executeQuery();
			if (rs.next()) {
				idNodoPadre = rs.getLong("IDNODOCORRIERE");
			}
			return idNodoPadre;
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero del Nodo corriere tramite id=" + idNodo, e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Gets the alberatura ufficio.
	 *
	 * @param idNodo
	 *            the id nodo
	 * @param connection
	 *            the connection
	 * @return the alberatura ufficio
	 */
	@Override
	public final List<Long> getAlberaturaUfficio(final Long idNodo, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<Long> nodi = new ArrayList<>();

		try {
			int index = 1;
			ps = connection.prepareStatement("SELECT * FROM nodo CONNECT BY PRIOR idnodopadre = idnodo START WITH idnodo = ? ORDER SIBLINGS BY ordinamento");
			ps.setLong(index++, idNodo);

			rs = ps.executeQuery();
			while (rs.next()) {
				nodi.add(rs.getLong(IDNODO));
			}
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero dell'alberatura dell'ufficio tramite id=" + idNodo, e);
		} finally {
			closeStatement(ps, rs);
		}

		return nodi;
	}

	/**
	 * @see it.ibm.red.business.dao.INodoDAO#getCorriereSegreteriaFromSottoNodo(int,
	 *      java.sql.Connection).
	 */
	@Override
	public Nodo getCorriereSegreteriaFromSottoNodo(final int idNodo, final Connection con) {
		Nodo nodo = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.prepareStatement("SELECT * FROM nodo WHERE idtipostruttura = 4 AND flagsegreteria = 1 CONNECT BY PRIOR idnodopadre = idnodo START WITH idnodo = ?");
			ps.setInt(1, idNodo);
			rs = ps.executeQuery();
			if (rs.next()) {
				final Long idNodoDB = rs.getLong(IDNODO);
				final Date dataAttivazione = rs.getDate("DATAATTIVAZIONE");
				final Date dataDisattivazione = rs.getDate("DATADISATTIVAZIONE");
				final String codiceNodo = rs.getString(CODICENODO);
				final String descrizione = rs.getString(DESCRIZIONE);
				final Long idAoo = rs.getLong("IDAOO");
				final Long idUtenteDirigente = rs.getLong("IDUTENTEDIRIGENTE");
				final Integer flagSegreteria = rs.getInt("FLAGSEGRETERIA");
				final Long idNodoPadre = rs.getLong(IDNODOPADRE);
				closeStatement(ps, rs);
				ps = null;
				rs = null;

				final Aoo aoo = aooDAO.getAoo(idAoo, con);
				final Utente utenteDirigente = utenteDAO.getUtente(idUtenteDirigente, con);
				nodo = new Nodo(idNodoDB, idNodoPadre, dataAttivazione, dataDisattivazione, aoo, codiceNodo, descrizione, utenteDirigente, flagSegreteria);
			}
			return nodo;
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dei dati nodo con idnodo=" + idNodo + " dal DB: " + e.getMessage(), e);
			throw new RedException("Errore nel recupero dei dati nodo con idnodo=" + idNodo + " dal DB.");
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Gets the id nodo coordinatore.
	 *
	 * @param idUfficioMittente
	 *            the id ufficio mittente
	 * @param idUtenteCoordinatore
	 *            the id utente coordinatore
	 * @param connection
	 *            the connection
	 * @return the id nodo coordinatore
	 */
	@Override
	public final Integer getIdNodoCoordinatore(final Long idUfficioMittente, final Integer idUtenteCoordinatore, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			int index = 1;
			ps = connection.prepareStatement("SELECT idnodocoordinatore FROM NODOUTENTECOORDINATORE WHERE idnodomittente = ? AND idutentecoordinatore = ?");
			ps.setInt(index++, idUfficioMittente.intValue());
			ps.setInt(index++, idUtenteCoordinatore.intValue());
			rs = ps.executeQuery();
			rs.next();
			return rs.getInt("idnodocoordinatore");
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero dell'id del nodo coordinatore", e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.INodoDAO#getNodoUCP(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public Long getNodoUCP(final Long idNodo, final Connection connection) {
		int idUfficioUCP = 0;
		CallableStatement cs = null;
		try {
			int index = 0;
			cs = connection.prepareCall("{call PKG_NSD_PE_RGS.getUfficioUCP(?,?,?)}");
			cs.setInt(++index, idNodo.intValue());
			cs.registerOutParameter(++index, Types.INTEGER);
			cs.registerOutParameter(++index, Types.INTEGER);
			cs.execute();
			idUfficioUCP = cs.getInt(index - 1);
			return (long) idUfficioUCP;
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero del nodo UCP per l'ufficio " + idNodo, e);
		} finally {
			closeStatement(cs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.INodoDAO#isNodoUcp(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public Boolean isNodoUcp(final Long idNodo, final Connection con) {
		boolean isNodoUcp = false;
		try {
			final Long nodoUcp = getNodoUCP(idNodo, con);
			if (idNodo.equals(nodoUcp)) {
				isNodoUcp = true;
			}
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dei documenti dal PE: " + e.getMessage(), e);
		}
		return isNodoUcp;
	}

	/**
	 * @see it.ibm.red.business.dao.INodoDAO#getAlberaturaBottomUp(java.lang.Long,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<Nodo> getAlberaturaBottomUp(final Long idAoo, final Long idUfficio, final Connection connection) {
		final List<Nodo> nodi = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			final StringBuilder query = new StringBuilder();
			query.append("SELECT idnodo, idnodopadre, codicenodo, descrizione FROM nodo WHERE nodo.idaoo = ?")
					.append(" AND (nodo.idnodo NOT IN (select aoo.idnodoradice from aoo where aoo.idaoo = ?))").append(" CONNECT BY PRIOR idnodopadre = idnodo")
					.append(" START WITH idnodo = ?").append(" AND (datadisattivazione IS NULL or datadisattivazione > SYSDATE)");
			int index = 1;
			ps = connection.prepareStatement(query.toString());
			ps.setLong(index++, idAoo);
			ps.setLong(index++, idAoo);
			ps.setLong(index++, idUfficio);

			rs = ps.executeQuery();
			while (rs.next()) {
				final Nodo nodo = new Nodo(rs.getLong(IDNODO), rs.getLong(IDNODOPADRE), null, null, null, rs.getString(CODICENODO), rs.getString(DESCRIZIONE), null,
						null);
				nodi.add(nodo);
			}
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero dell'alberatura bottom-up", e);
		} finally {
			closeStatement(ps, rs);
		}

		return nodi;
	}

	/**
	 * @see it.ibm.red.business.dao.INodoDAO#getSegreteriaDirezione(java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public Integer getSegreteriaDirezione(final Integer idNodo, final Connection con) {
		CallableStatement cs = null;
		try {
			int iddirezione = 0;
			cs = con.prepareCall("{call PKG_NSD_PE_RGS.GETSEGRETERIADIREZIONE(?,?)}");
			cs.setInt(1, idNodo);
			cs.registerOutParameter(2, Types.NUMERIC);
			cs.execute();
			iddirezione = cs.getInt(2);
			return iddirezione;
		} catch (final SQLException e) {
			LOGGER.error("Errore nella chiamata a NSD.PKG_NSD_PE_RGS.GETSEGRETERIADIREZIONE per idNodo:" + idNodo, e);
			throw new RedException("Errore nella chiamata a NSD.PKG_NSD_PE_RGS.GETSEGRETERIADIREZIONE per idNodo:" + idNodo, e);
		} finally {
			closeStatement(cs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.INodoDAO#getNewIdFromTabMigrazione(long,
	 *      java.sql.Connection).
	 */
	@Override
	public UfficioDTO getNewIdFromTabMigrazione(final long idNodo, final Connection con) {

		UfficioDTO nodoPostMigrazione = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.prepareStatement("SELECT id_uff_new,desc_uff_new from t_migrazione_ufficio where id_uff_old = ?");
			ps.setLong(1, idNodo);
			rs = ps.executeQuery();
			if (rs.next()) {
				nodoPostMigrazione = new UfficioDTO(rs.getLong("id_uff_new"), rs.getString("desc_uff_new"));
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei dati del nodo: idNodo=" + idNodo + " dal DB. " + e.getMessage(), e);
			throw new RedException("Errore durante il recupero dei dati del nodo idNodo=" + idNodo);
		} finally {
			closeStatement(ps, rs);
		}
		return nodoPostMigrazione;
	}

	/**
	 * @see it.ibm.red.business.dao.INodoDAO#getUfficioUCR(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public UfficioDTO getUfficioUCR(final Long idAOO, final Connection con) {

		UfficioDTO item = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			ps = con.prepareStatement("select n.idNodo, n.descrizione from nodo n where upper(descrizione) = upper(?) and idaoo = ? and idnodocorriere is not null");
			
			ps.setString(index++, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DESC_UFF_UCRA));
			ps.setLong(index++, idAOO);
			rs = ps.executeQuery();
			if (rs.next()) {
				item = new UfficioDTO(rs.getLong("idNodo"), rs.getString("descrizione"));
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return item;
	}

	/**
	 * @see it.ibm.red.business.dao.INodoDAO#getSottoNodiIspettorato(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public Map<UfficioDTO, List<UfficioDTO>> getSottoNodiIspettorato(final Long idAOO, final Connection con) {
		final Map<UfficioDTO, List<UfficioDTO>> ufficio2son = new HashMap<>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = con.prepareStatement("select idnodopadre,idnodo,descrizione,ordinamento from nodo "
					+ " where idnodopadre in (select idnodo from nodo where idtipostruttura = 4 and idaoo = ? and datadisattivazione is null) "
					+ " and datadisattivazione is null and idnodocorriere is not null " + " union all " + " select idnodo,idnodo,descrizione,ordinamento from nodo "
					+ " where idtipostruttura = 4 and idaoo = ? and datadisattivazione is null " + " order by 1, 4 ");
			ps.setLong(1, idAOO);
			ps.setLong(2, idAOO);
			rs = ps.executeQuery();
			while (rs.next()) {
				final Long idNodoPadre = rs.getLong("idnodopadre");
				final Long idNodo = rs.getLong("idnodo");
				final String descrizione = rs.getString("descrizione");

				final UfficioDTO itemPadre = new UfficioDTO(idNodoPadre, descrizione);
				final UfficioDTO itemFiglio = new UfficioDTO(idNodo, descrizione);

				if (itemPadre.equals(itemFiglio)) {
					List<UfficioDTO> sons = ufficio2son.remove(itemPadre);
					if (sons == null) {
						sons = new ArrayList<>();
					}
					ufficio2son.put(itemPadre, sons);
				} else {
					List<UfficioDTO> sons = ufficio2son.get(itemPadre);
					if (CollectionUtils.isEmpty(sons)) {
						sons = new ArrayList<>();
						ufficio2son.put(itemPadre, sons);
					}
					sons.add(itemFiglio);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return ufficio2son;
	}

	/**
	 * @see it.ibm.red.business.dao.INodoDAO#getFlagAssegnazioneDiretta(java.lang.Long,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public Boolean getFlagAssegnazioneDiretta(final Long idNodo, final String indirizzoEmail, final Connection con) {
		Boolean output = false;

		final String queryAssDirUfficio = "select NVL(FLAG_ASSEGNAZIONE_DIRETTA, '0') as FLAG_ASSEGNAZIONE_DIRETTA from nodo n where n.idnodo = ?";

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = con.prepareStatement(queryAssDirUfficio);
			ps.setLong(1, idNodo);
			rs = ps.executeQuery();
			if (rs.next()) {
				final String flagAssegnazioneDiretta = rs.getString("FLAG_ASSEGNAZIONE_DIRETTA");
				output = BooleanFlagEnum.SI.getIntValueInString().equals(flagAssegnazioneDiretta);
				if (Boolean.TRUE.equals(output) && !StringUtils.isNullOrEmpty(indirizzoEmail)) {
					output = getFlagAssegnazioneDirettaAccount(idNodo, indirizzoEmail, con);
				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero assegnazione diretta per ufficio.", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return output;
	}

	private static Boolean getFlagAssegnazioneDirettaAccount(final Long idNodo, final String indirizzoEmail, final Connection con) {
		Boolean output = false;

		final String queryAssDirAccount = "select 1 from ASSEGNAZIONE_DIRETTA a where a.ASD_IDNODO = ? and lower(a.ASD_ACCOUNT) = ?";

		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = con.prepareStatement(queryAssDirAccount);
			ps.setLong(1, idNodo);
			ps.setString(2, indirizzoEmail);
			rs = ps.executeQuery();
			output = rs.next();
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di recupero assegnazione diretta per account.", e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.INodoDAO#preselezionaRibaltaTitolario(java.lang.Long,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public boolean preselezionaRibaltaTitolario(final Long idUfficio, final Long idAoo, final Connection conn) {
		boolean preseleziona = false;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int index = 1;
		try {
			final String query = "SELECT * FROM PRESELEZIONA_RIBALTA_TITOLARIO WHERE ID_UFFICIO = ? AND ID_AOO = ?";
			ps = conn.prepareStatement(query);
			ps.setLong(index++, idUfficio);
			ps.setLong(index++, idAoo);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				preseleziona = true;
			}
		} catch (final Exception ex) {
			LOGGER.error("Errore durante il recupero dei dati dalla tabella per la preselezione del ribalta titolario :" + ex);
			throw new RedException(ex);
		} finally {
			closeStatement(ps, rs);
		}

		return preseleziona;
	}

	/**
	 * @see it.ibm.red.business.dao.INodoDAO#getIdNodoByDescAndCodiceAoo(java.lang.String,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public final Long getIdNodoByDescAndCodiceAoo(final String descrizioneNodo, final String codiceAoo, final Connection connection) {
		Long output = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = connection.prepareStatement("SELECT n.IDNODO FROM NODO n, AOO a WHERE LOWER(n.DESCRIZIONE) = ?" + " AND n.IDAOO = a.IDAOO AND LOWER(a.CODICEAOO) = ?");
			ps.setString(1, descrizioneNodo.toLowerCase());
			ps.setString(2, codiceAoo.toLowerCase());

			rs = ps.executeQuery();

			if (rs.next()) {
				output = rs.getLong(IDNODO);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del nodo con descrizione: " + descrizioneNodo + " afferente all'AOO: " + codiceAoo, e);
			throw new RedException("Errore durante il recupero del nodo con descrizione: " + descrizioneNodo + " afferente all'AOO: " + codiceAoo, e);
		} finally {
			closeStatement(ps, rs);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.INodoDAO#getDescrizioneByIdNodo(java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public HashMap<Integer, String> getDescrizioneByIdNodo(final Integer idAoo,final Connection conn) {
		HashMap<Integer, String> mapIdNodoDescrizione = new HashMap<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			int index = 1;
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT IDNODO,DESCRIZIONE FROM NODO WHERE IDAOO = ? ");
			
			
			ps = conn.prepareStatement(sb.toString());
			ps.setInt(index++, idAoo);
			rs = ps.executeQuery();
			
			while(rs.next()) {
				mapIdNodoDescrizione.put(rs.getInt(IDNODO), rs.getString(DESCRIZIONE));
			}
			 
		} catch(Exception ex) {
			LOGGER.error("Errore nel recupero della mappa di id nodo descrizione nodo : "+ex);
			throw new RedException("Errore nel recupero della mappa di id nodo descrizione nodo : "+ex);
		} finally {
			closeStatement(ps, rs);
		}
		return mapIdNodoDescrizione;
	}
	
	/**
	 * @see it.ibm.red.business.dao.INodoDAO#getIdNodiByAoo(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public final List<Long> getIdNodiByAoo(final Long idAoo, final Connection connection) {
		List<Long> output = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			int index = 1;
			StringBuilder sb = new StringBuilder();
			sb.append("select * from nodo n join aoo a ");
			sb.append("on n.idaoo = a.idaoo ");
			sb.append("where a.idaoo = ?");
		
			ps = connection.prepareStatement(sb.toString()); 
					
			ps.setInt(index++, idAoo.intValue());
			rs = ps.executeQuery();
			
			output = new ArrayList<>();
			while (rs.next()) {
				output.add(rs.getLong(IDNODO));
			}
		} catch (Exception e) {
			LOGGER.error("Errore durante il recupero dei nodi per aoo: ",e);
			throw new RedException("Errore durante il recupero dei nodi per aoo: ",e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return output;
	}
}