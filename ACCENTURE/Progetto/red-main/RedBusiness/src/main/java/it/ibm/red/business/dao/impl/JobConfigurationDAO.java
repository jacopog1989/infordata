package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IJobConfigurationDAO;
import it.ibm.red.business.dto.JobConfigurationDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.exception.RedException;

/**
 * The Class JobConfigurationDAO.
 *
 * @author a.dilegge
 * 
 * 	Dao gestione job.
 */
@Repository
public class JobConfigurationDAO extends AbstractDAO implements IJobConfigurationDAO {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Gets the all.
	 *
	 * @param connection the connection
	 * @return the all
	 */
	@Override
	public final List<JobConfigurationDTO> getAll(final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<JobConfigurationDTO> jobs = new ArrayList<>();
		try {
			String querySQL = "SELECT * FROM REDBATCH_JOBCONFIGURATION";
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			while (rs.next()) {
				jobs.add(
						new JobConfigurationDTO(
								rs.getInt("RBJ_IDJOBCONFIGURATION"), 
								rs.getString("RBJ_CLASSNAME"), 
								rs.getString("RBJ_METHODNAME"),
								rs.getInt("RBJ_IDAOO"), 
								rs.getString("RBJ_OPERATION"), 
								rs.getInt("RBJ_JOBSTATE") == BooleanFlagEnum.SI.getIntValue(), 
								rs.getString("RBJ_CRONSCHEDULE"), 
								rs.getInt("RBJ_TRIGGERNUMBER"), 
								rs.getString("RBJ_JOBDESCRIPTION"), 
								rs.getString("RBJ_JOBERROR"), 
								rs.getString("RBJ_SERVERNAME"))
						);
				
			}
		} catch (SQLException e) {
			throw new RedException("Errore durante il recupero dei job ", e);
		} finally {
			closeStatement(ps, rs);
		}
		return jobs;
	}

}