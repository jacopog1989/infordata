package it.ibm.red.business.service;

import java.sql.Connection;
import java.util.Collection;

import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.service.facade.IMasterPaginatiFacadeSRV;

/**
 * Interface del servizio che gestisce i master paginati.
 */
public interface IMasterPaginatiSRV extends IMasterPaginatiFacadeSRV {

	/**
	 * Permette di valorizzare il flag siebel per una lista di master.
	 * @param mastersCE
	 * @param connection
	 */
	void addSiebelFlag(Collection<MasterDocumentRedDTO> mastersCE, Long idAOO, Connection connection);

}