package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import it.ibm.red.business.dto.RdsSiebelDTO;
import it.ibm.red.business.dto.RdsSiebelDescrizioneDTO;
import it.ibm.red.business.dto.RdsSiebelGruppoDTO;
import it.ibm.red.business.dto.RdsSiebelTipologiaDTO;

/**
 * 
 * @author adilegge
 *
 *	Interfaccia del dao per la gestione delle RdS Siebel
 */
public interface ISiebelDAO extends Serializable {
	
	/**
	 * Recupera l'id contatto Siebel associato all'ufficio in input, in caso sia presente e abilitato 
	 * 
	 * @param idNodo
	 * @param con
	 * @return
	 * @throws DAOException
	 */
	String getIdContattoSiebel(Integer idNodo, Connection con);
	
	/**
	 * Verifica che siano presenti Rds aperte per il documento in input.
	 * 
	 * @param idDocumento
	 * @param con
	 * @return
	 * @throws DAOException
	 */
	boolean hasRdsAperte(String idDocumento, Long idAOO, Connection con);
	
	/**
	 * Recupera le Rds associate al documento in input
	 * 
	 * @param idDocumento
	 * @param con
	 * @return
	 * @throws DAOException
	 */
	List<RdsSiebelDTO> getAllRds(String idDocumento, Long idAOO, Connection con);

	/**
	 * Inserisce una RdS sul database
	 * 
	 * @param rds
	 * @param con
	 * @throws DAOException
	 */
	void inserisciRdS(RdsSiebelDTO rds, Connection con);
	
	/**
	 * Recupera la Rds identificata dall'id in input
	 * 
	 * @param idRdsSiebel
	 * @param con
	 * @return
	 * @throws DAOException
	 */
	RdsSiebelDTO getRds(String idRdsSiebel, Connection con);

	/**
	 * Aggiorna informazioni della RdS
	 * 
	 * @param rdsSiebel
	 * @param connection
	 */
	void aggiornaRds(RdsSiebelDTO rdsSiebel, Connection connection);

	/**
	 * Lista di rds siebel con alcune descrizioni utili alla rappresentazione delle siebel in una tabella di visualizzazione.
	 * 
	 * @param idDocumento	Id del documento del quale interessano le rds.
	 * @param con			connesione con il database applicativo. 
	 * @return				UNa lista di rds con informazioni accessorie riguardo l'utente e il nodo di apertura.
	 */
	List<RdsSiebelDescrizioneDTO> getAllRdsDescrizione(String idDocumento, Long idAOO, Connection con);
	/**
	 * Verifica se i documenti in input hanno RdS associate
	 * 
	 * @param ids
	 * @param connection
	 * @return
	 */
	Map<String, Boolean> hasRds(Collection<String> ids, Long idAOO,  Connection connection);

	/**
	 * @param con
	 * @return
	 */
	Map<RdsSiebelTipologiaDTO, List<RdsSiebelGruppoDTO>> getMappaTipiGruppiSiebel(Connection con);
}
