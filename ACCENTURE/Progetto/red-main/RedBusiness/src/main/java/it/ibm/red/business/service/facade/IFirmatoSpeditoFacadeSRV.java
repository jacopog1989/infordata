package it.ibm.red.business.service.facade;

import java.io.Serializable;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.ProtocolloEmergenzaDocDTO;
import it.ibm.red.business.dto.UtenteDTO;

public interface IFirmatoSpeditoFacadeSRV extends Serializable {
	
	/**
	 * metodo specifico che gestisce la response 'Firmato e Spedito'.
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	EsitoOperazioneDTO firmatoESpedito(UtenteDTO utente, String wobNumber, ProtocolloEmergenzaDocDTO protocolloEmergenza);
	
	/**
	 * metodo specifico che gestisce la response 'Firmato'.
	 * 
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	EsitoOperazioneDTO firmato(UtenteDTO utente, String wobNumber, ProtocolloEmergenzaDocDTO protocolloEmergenza);
	
}
