package it.ibm.red.business.enums;

/**
 * Enum associata alla tipologia di firma.
 */
public enum SignModeEnum {
	
	/**
	 * Firma remota.
	 */
	REMOTA(0),

	/**
	 * Firma locale.
	 */
	LOCALE(1),
	
	/**
	 * Firma autografa.
	 */
	AUTOGRAFA(2);

	/**
	 * Id tipo firma.
	 */
	private Integer id;

	/**
	 * Costruttore.
	 * @param id
	 */
	private SignModeEnum(Integer id) {
		this.id = id;
	}
	
	/**
	 * Restituisce l'id del tipo firma.
	 * @return id
	 */
	public Integer getId() {
		return id;
	}
	
	/**
	 * Restituisce l'enum associata all'id passato come parametro.
	 * @param id
	 * @return tipo firma
	 */
	public static SignModeEnum get(Integer id) {
		SignModeEnum out = null;
		
		for (SignModeEnum m : SignModeEnum.values()) {
			if (m.getId().equals(id)) {
				out = m;
				break;
			}
		}
		
		return out;
	}

}
