package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.core.Document;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.Ricerca;
import it.ibm.red.business.dao.IAllaccioDAO;
import it.ibm.red.business.dao.IEventoLogDAO;
import it.ibm.red.business.dao.IMetadatoDAO;
import it.ibm.red.business.dao.ITipologiaDocumentoDAO;
import it.ibm.red.business.dto.AnagraficaDipendentiComponentDTO;
import it.ibm.red.business.dto.DatiDocumentoContiConsuntiviDTO;
import it.ibm.red.business.dto.DatiNotificaContiConsuntiviDTO;
import it.ibm.red.business.dto.EstrattiContoUcbDTO;
import it.ibm.red.business.dto.EventoLogDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FascicoloFlussoDTO;
import it.ibm.red.business.dto.LookupTableDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.ParamsRicercaContiConsuntiviDTO;
import it.ibm.red.business.dto.ParamsRicercaDocUCBAssegnanteAssegnatarioDTO;
import it.ibm.red.business.dto.ParamsRicercaEsitiDTO;
import it.ibm.red.business.dto.ParamsRicercaEstrattiContoDTO;
import it.ibm.red.business.dto.ParamsRicercaFlussiDTO;
import it.ibm.red.business.dto.RicercaAvanzataDocUcbFormDTO;
import it.ibm.red.business.dto.RisultatiRicercaUcbDTO;
import it.ibm.red.business.dto.RisultatoRicercaContiConsuntiviDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.StoricoDTO;
import it.ibm.red.business.enums.FilenetStatoFascicoloEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.RicercaAssegnanteAssegnatarioEnum;
import it.ibm.red.business.enums.TipoContestoProceduraleEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.metadatiestesi.MetadatiEstesiHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.IRicercaAvanzataDocSRV;
import it.ibm.red.business.service.IRicercaAvanzataDocUCBSRV;
import it.ibm.red.business.service.IRicercaSRV;
import it.ibm.red.business.service.IStoricoSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class RicercaAvanzataDocUCBSRV.
 *
 * @author a.dilegge
 * 
 *         Servizio per la gestione delle ricerche dei documenti custom per gli
 *         UCB.
 */
@Service
public class RicercaAvanzataDocUCBSRV extends AbstractService implements IRicercaAvanzataDocUCBSRV {

	private static final String LIKE = " LIKE ";

	private static final String AND = " AND ";

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -2265842041200419369L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaAvanzataDocUCBSRV.class.getName());

	/**
	 * Service.
	 */
	@Autowired
	private IStoricoSRV storicoSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private ITipologiaDocumentoDAO tipologiaDocumentoDAO;

	/**
	 * Service.
	 */
	@Autowired
	private IRicercaSRV ricercaSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IFascicoloSRV fascicoloSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IRicercaAvanzataDocSRV ricercaAvanzataDoc;

	/**
	 * DAO.
	 */
	@Autowired
	private IEventoLogDAO eventoLogDAO;

	/**
	 * DAO gestione metadati estesi.
	 */
	@Autowired
	private IMetadatoDAO metadatoDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IAllaccioDAO allaccioDAO;

	/**
	 * PRoperties.
	 */
	private PropertiesProvider pp;

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaAvanzataDocUCBFacadeSRV#eseguiRicercaDocumentiAssegnanteAssegnatario(it.ibm.red.business.dto.ParamsRicercaDocUCBAssegnanteAssegnatarioDTO,
	 *      it.ibm.red.business.dto.UtenteDTO, boolean).
	 */
	@Override
	public RisultatiRicercaUcbDTO eseguiRicercaDocumentiAssegnanteAssegnatario(final ParamsRicercaDocUCBAssegnanteAssegnatarioDTO paramsRicerca, final UtenteDTO utente,
			final boolean orderbyInQuery) {
		LOGGER.info("eseguiRicercaDocumentiAssegnanteAssegnatario -> START");
		RisultatiRicercaUcbDTO output = null;
		Collection<MasterDocumentRedDTO> documentiRicerca = new ArrayList<>();
		final HashMap<String, Object> mappaValoriRicercaFilenet = new HashMap<>();
		Connection con = null;
		Connection dwhCon = null;
		IFilenetCEHelper fceh = null;

		try {
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			dwhCon = setupConnection(getFilenetDataSource().getConnection(), false);
			con = setupConnection(getDataSource().getConnection(), false);
			final Set<String> idDocumenti = new HashSet<>();

			// Si convertono i parametri di ricerca in una mappa chiave -> valore
			convertiInParametriRicercaFilenet(mappaValoriRicercaFilenet, paramsRicerca, utente.getIdAoo());

			// recupera gli iddocumento dallo storico
			getDocumentiFromStorico(idDocumenti, paramsRicerca, utente, dwhCon);

			// recupera i documenti da Filenet per recuperare le informazioni e applicare le
			// acl
			DocumentSet documentiFilenet = null;
			if (!CollectionUtils.isEmpty(idDocumenti)) {
				final String acl = "";

				documentiFilenet = fceh.ricercaAvanzataDocumenti(mappaValoriRicercaFilenet, idDocumenti, utente, orderbyInQuery, acl, false);

				if (documentiFilenet != null && !documentiFilenet.isEmpty()) {
					documentiRicerca = ricercaSRV.trasformToGenericDocContext(documentiFilenet, utente, con);
				}
			}

			if (!orderbyInQuery) {
				final List<MasterDocumentRedDTO> docList = new ArrayList<>(documentiRicerca);
				// Implementa Comparable
				Collections.sort(docList, Collections.reverseOrder());
				documentiRicerca = docList;
			}

			// Risultati omogenei/eterogei
			final boolean risultatiOmogenei = isRisultatiOmogenei(documentiRicerca);
			Collection<MetadatoDTO> metadatiOut = new ArrayList<>();

			// Metadati flagOut
			if (!CollectionUtils.isEmpty(documentiRicerca) && risultatiOmogenei) {
				final MasterDocumentRedDTO docRicerca = documentiRicerca.iterator().next();
				final Integer idTipologiaDocumento = docRicerca.getIdTipologiaDocumento();
				final Integer idTipoProcedimento = docRicerca.getIdTipoProcedimento();

				// Metadati che devono essere visualizzati in ricerca
				metadatiOut = metadatoDAO.caricaMetadatiOut(idTipologiaDocumento, idTipoProcedimento, con);

				// Mappa Document Title documento -> XML metadati estesi documento
				final Map<String, String> metadatiDocumenti = tipologiaDocumentoDAO
						.recuperaMetadatiEstesi(documentiRicerca.stream().map(MasterDocumentRedDTO::getDocumentTitle).collect(Collectors.toList()), con);

				// Si impostano i metadati estesi per ogni documento
				documentiRicerca.forEach(
						doc -> doc.setMetadatiEstesi(MetadatiEstesiHelper.deserializeMetadatiEstesiPerRisultatiRicerca(metadatiDocumenti.get(doc.getDocumentTitle()))));
			}

			output = new RisultatiRicercaUcbDTO(documentiRicerca, risultatiOmogenei, metadatiOut, Constants.Ricerca.KEY_RICERCA_UCB_ASS);

		} catch (final Exception e) {
			LOGGER.error("eseguiRicercaDocumentiAssegnanteAssegnatario -> Si è verificato un errore durante l'esecuzione della ricerca.", e);
			throw new RedException("Si è verificato un errore durante l'esecuzione della ricerca.", e);
		} finally {
			closeConnection(dwhCon);
			closeConnection(con);
			popSubject(fceh);
		}

		LOGGER.info("eseguiRicercaDocumentiAssegnanteAssegnatario -> END. Numero di documenti trovati: " + documentiRicerca.size());
		return output;
	}

	private void getDocumentiFromStorico(final Set<String> idDocumenti, final ParamsRicercaDocUCBAssegnanteAssegnatarioDTO paramsRicerca, final UtenteDTO utente,
			final Connection dwhCon) {

		LOGGER.info("getDocumentiFromEventLog -> START");
		final Map<String, Object> valoriRicerca = new HashMap<>();

		// idaoo
		valoriRicerca.put("idaoo", utente.getIdAoo());

		// destinatario
		if (RicercaAssegnanteAssegnatarioEnum.ASSEGNATARIO.equals(paramsRicerca.getTipoRicerca())) {
			valoriRicerca.put("idnododestinatario", paramsRicerca.getAssegnatario().getIdNodo());
			if (paramsRicerca.getAssegnatario().getIdUtente() != null && paramsRicerca.getAssegnatario().getIdUtente() != 0) {
				valoriRicerca.put("idutentedestinatario", paramsRicerca.getAssegnatario().getIdUtente());
			}
		}

		// mittente
		if (RicercaAssegnanteAssegnatarioEnum.ASSEGNANTE.equals(paramsRicerca.getTipoRicerca())) {
			valoriRicerca.put("idnodomittente", paramsRicerca.getAssegnatario().getIdNodo());
			if (paramsRicerca.getAssegnatario().getIdUtente() != null && paramsRicerca.getAssegnatario().getIdUtente() != 0) {
				valoriRicerca.put("idutentemittente", paramsRicerca.getAssegnatario().getIdUtente());
			}
		}

		// data creazione => timestamp >= data creazione da
		if ((paramsRicerca.getDataCreazioneDa() != null && paramsRicerca.getDataCreazioneA() != null)) {
			valoriRicerca.put(pp.getParameterByKey(PropertiesNameEnum.TIMESTAMP_OPERAZIONE_DA), paramsRicerca.getDataCreazioneDa());
			valoriRicerca.put(pp.getParameterByKey(PropertiesNameEnum.TIMESTAMP_OPERAZIONE_A), paramsRicerca.getDataCreazioneA());
		}

		try {
			final List<EventoLogDTO> eventLogList = eventoLogDAO.cercaEventiPerRicercaDocumenti(valoriRicerca, dwhCon);
			for (int i = 0; i < eventLogList.size(); i++) {
				idDocumenti.add(Constants.EMPTY_STRING + eventLogList.get(i).getIdDocumento());
			}
		} catch (final Exception e) {
			LOGGER.error("getDocumentiFromEventLog -> Si è verificato un errore durante la ricerca di eventi nello storico.", e);
		}

		LOGGER.info("getDocumentiFromEventLog -> END");

	}

	private void convertiInParametriRicercaFilenet(final HashMap<String, Object> paramsRicercaFilenet, final ParamsRicercaDocUCBAssegnanteAssegnatarioDTO paramsRicerca,
			final Long idAoo) {
		LOGGER.info("convertiInParametriRicercaFilenet -> START");

		paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY), idAoo);

		// Classe documentale
		paramsRicercaFilenet.put(Ricerca.CLASSE_DOCUMENTALE_FWS, pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));

		// Data Creazione Da
		if (paramsRicerca.getDataCreazioneDa() != null) {
			paramsRicercaFilenet.put(Ricerca.DATA_CREAZIONE_DA, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataCreazioneDa(), true));
		}

		// Data Creazione A
		if (paramsRicerca.getDataCreazioneA() != null) {
			paramsRicercaFilenet.put(Ricerca.DATA_CREAZIONE_A, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataCreazioneA(), false));
		}

		// Anno Protocollo
		if (paramsRicerca.getAnnoProtocollo() != null) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY), paramsRicerca.getAnnoProtocollo());
		}

		// Numero Protocollo Da
		if (paramsRicerca.getNumeroProtocolloDa() != null) {
			paramsRicercaFilenet.put(Ricerca.NUMERO_PROTOCOLLO_DA, paramsRicerca.getNumeroProtocolloDa());
		}

		// Numero Protocollo A
		if (paramsRicerca.getNumeroProtocolloA() != null) {
			paramsRicercaFilenet.put(Ricerca.NUMERO_PROTOCOLLO_A, paramsRicerca.getNumeroProtocolloA());
		}

		// Data Scadenza Da
		if (paramsRicerca.getDataScadenzaDa() != null) {
			paramsRicercaFilenet.put(Ricerca.DATA_SCADENZA_DA, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataScadenzaDa(), true));
		}

		// Data Scadenza A
		if (paramsRicerca.getDataScadenzaA() != null) {
			paramsRicercaFilenet.put(Ricerca.DATA_SCADENZA_A, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataScadenzaA(), false));
		}

		LOGGER.info("convertiInParametriRicercaFilenet -> END");
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaAvanzataDocUCBFacadeSRV#initRicercaDoc(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.RicercaAvanzataDocUcbFormDTO).
	 */
	@Override
	public void initRicercaDoc(final UtenteDTO utente, final RicercaAvanzataDocUcbFormDTO ricercaAvanzataForm) {
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			if (ricercaAvanzataForm == null) {
				throw new RedException("Errore nel caricamento dei dati della maschera.");
			}

			/**
			 * TIPOLOGIA DOCUMENTO START
			 *****************************************************/
			ricercaAvanzataForm.setComboTipologieDocumento(ricercaAvanzataDoc.getListaTipologieDocumento(con, utente.getIdAoo()));
			ricercaAvanzataForm.setComboTipiProcedimento(ricercaAvanzataDoc.getTipiProcedimentoByTipologiaDocumento(null, utente.getIdAoo()));
			/**
			 * TIPOLOGIA DOCUMENTO END
			 *******************************************************/

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaAvanzataDocUCBFacadeSRV#eseguiRicercaAvanzataUCB(it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO,
	 *      java.util.Collection, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public RisultatiRicercaUcbDTO eseguiRicercaAvanzataUCB(final ParamsRicercaAvanzataDocDTO paramsRicercaFNet, final Collection<MetadatoDTO> metadatiEstesi,
			final UtenteDTO utente) {
		RisultatiRicercaUcbDTO output = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			final Collection<MasterDocumentRedDTO> resultsFiltered = new ArrayList<>();
			Collection<MetadatoDTO> metadatiOut = new ArrayList<>();
			boolean isRisultatiOmogenei = false;

//			<-- Filtro metadati estesi -->
			HashMap<String, String> idsFiltered = new HashMap<>();
			Collection<MetadatoDTO> metadatiDaCercare = null;
			boolean ricercaInMetadatiEstesi = false;
			if (!CollectionUtils.isEmpty(metadatiEstesi)) {
				metadatiDaCercare = new ArrayList<>();

				for (final MetadatoDTO metadato : metadatiEstesi) {
					if (Boolean.TRUE.equals(metadatoSelected(metadato))) {
						metadatiDaCercare.add(metadato);

						ricercaInMetadatiEstesi = true;
					}
				}

			}

			// ricerca filenet
//			<-- Ricerca Filenet -->
			final Collection<MasterDocumentRedDTO> resultRicercaFn = ricercaAvanzataDoc.eseguiRicercaDocumenti(paramsRicercaFNet, utente, connection, false,
					!ricercaInMetadatiEstesi, false);

			// ricerca metadati estesi
			if (ricercaInMetadatiEstesi) {

				// calcola ids
				final List<String> ids = new ArrayList<>();
				final HashMap<String, MasterDocumentRedDTO> resultsCached = new HashMap<>();
				for (final MasterDocumentRedDTO master : resultRicercaFn) {
					resultsCached.put(master.getDocumentTitle(), master);
					ids.add(master.getDocumentTitle());
				}

				idsFiltered = ricercaInMetadatiEstesi(metadatiDaCercare, ids, utente.getIdAoo(), paramsRicercaFNet.getDescrizioneTipologiaDocumento(),
						paramsRicercaFNet.getDescrizioneTipoProcedimento(), connection);

				if (!CollectionUtils.isEmpty(idsFiltered)) {
					for (final Entry<String, String> entry : idsFiltered.entrySet()) {
						final MasterDocumentRedDTO master = resultsCached.get(entry.getKey());
						final String xmlMetadatiEstesi = entry.getValue();

						master.setMetadatiEstesi(MetadatiEstesiHelper.deserializeMetadatiEstesiPerRisultatiRicerca(xmlMetadatiEstesi));
						resultsFiltered.add(master);
					}
				}

			} else {
				resultsFiltered.addAll(resultRicercaFn);
			}

//			<-- Risultati omogenei/eterogei -->
			isRisultatiOmogenei = isRisultatiOmogenei(resultsFiltered);

//			<-- Metadati flagOut -->
			// Se ho già a disposizione i metadati estesi (perchè sono stati caricati e
			// forniti dalla UI)
			// allora li utilizzo per estrapolare quelli con il flagout.
			// (do per scontato che se ho ricevuto una lista di metadati allora sia stata
			// selezionata
			// una coppia tipoDocumento/procedimento nella view e di conseguenza i risultati
			// saranno omogenei).
			if (!CollectionUtils.isEmpty(metadatiEstesi)) {
				for (final MetadatoDTO metadato : metadatiEstesi) {
					if (Boolean.TRUE.equals(metadato.getFlagOut()) && !Boolean.TRUE.equals(metadato.getFlagRangeStop())) {
						metadatiOut.add(metadato);
					}
				}
			} else if ((metadatiEstesi == null || metadatiEstesi.isEmpty()) && isRisultatiOmogenei && !resultsFiltered.isEmpty()) {
				// Se nella UI non è stata caricata nessuna lista di metadati estesi ma comunque
				// i risultati risultano omogenei
				// allora va definita una lista di metadati estesi con il flagout abilitato.
				final MasterDocumentRedDTO docRicerca = resultsFiltered.iterator().next();
				final Integer idTipologiaDocumento = docRicerca.getIdTipologiaDocumento();
				final Integer idTipoProcedimento = docRicerca.getIdTipoProcedimento();

				// Metadati che devono essere visualizzati in ricerca
				metadatiOut = metadatoDAO.caricaMetadatiOut(idTipologiaDocumento, idTipoProcedimento, connection);

				// Mappa Document Title documento -> XML metadati estesi documento
				final List<String> idDocumenti = resultsFiltered.stream().map(MasterDocumentRedDTO::getDocumentTitle).collect(Collectors.toList());
				final Map<String, String> metadatiDocumenti = tipologiaDocumentoDAO.recuperaMetadatiEstesi(idDocumenti, connection);

				// Si impostano i metadati estesi per ogni documento
				resultsFiltered.forEach(
						doc -> doc.setMetadatiEstesi(MetadatiEstesiHelper.deserializeMetadatiEstesiPerRisultatiRicerca(metadatiDocumenti.get(doc.getDocumentTitle()))));
			}

			output = new RisultatiRicercaUcbDTO(resultsFiltered, isRisultatiOmogenei, metadatiOut, Constants.Ricerca.KEY_RICERCA_UCB_DOC);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaAvanzataDocUCBFacadeSRV#eseguiRicercaEsitiUCB(it.ibm.red.business.dto.ParamsRicercaEsitiDTO,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public RisultatiRicercaUcbDTO eseguiRicercaEsitiUCB(final ParamsRicercaEsitiDTO paramsRicerca, final UtenteDTO utente) {
		LOGGER.info(new Date().toString());
		RisultatiRicercaUcbDTO output = null;
		IFilenetCEHelper fceh = null;
		Connection connection = null;

		try {

			connection = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			// Recupero Documenti in Uscita che soddisfano i parametri di riceca valorizzati
			// dall'utente.
			final Collection<String> idsDocUscita = ricercaAvanzataDoc.ricercaEsitiUscita(paramsRicerca, fceh);

			// Proiezione dei documentTitle per recuperare gli allacci nati dai documenti in
			// uscita.
			Collection<String> docsAllaccioEntrata = new ArrayList<>();
			if (!idsDocUscita.isEmpty()) {
				docsAllaccioEntrata = allaccioDAO.getAllacciPrincipaliByDocumentTitle(idsDocUscita, utente.getIdAoo(), connection);
			}

			// Recupero delle entità Documento che rappresentano gli Allacci Entrata.
			Collection<MasterDocumentRedDTO> resultsFiltered = new ArrayList<>();
			if (!docsAllaccioEntrata.isEmpty()) {
				final DocumentSet documentiFilenet = fceh.getDocumentsForMasters(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), docsAllaccioEntrata);

				if (documentiFilenet != null && !documentiFilenet.isEmpty()) {
					resultsFiltered = ricercaSRV.trasformToGenericDocContext(documentiFilenet, utente, connection);
				}

			}

			output = new RisultatiRicercaUcbDTO(resultsFiltered, false, null, Constants.Ricerca.KEY_RICERCA_UCB_ESITI);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);

		}

		return output;
	}

	/**
	 * @param metadatiEstesi
	 * @param ids
	 * @param idAoo
	 * @param descTipologiaDocumento
	 * @param descTipoProcedimento
	 * @param connection
	 * @return
	 */
	private HashMap<String, String> ricercaInMetadatiEstesi(final Collection<MetadatoDTO> metadatiEstesi, final List<String> ids, final Long idAoo,
			final String descTipologiaDocumento, final String descTipoProcedimento, final Connection connection) {
		HashMap<String, String> idsFiltered = new HashMap<>();

		try {
			idsFiltered = tipologiaDocumentoDAO.ricercaMetadatiEstesi(connection, metadatiEstesi, ids, idAoo, descTipologiaDocumento, descTipoProcedimento);
		} catch (final Exception e) {
			LOGGER.error("ricercaInMetadatiEstesi -> Errore nella ricerca per metadati estesi: " + e.getMessage(), e);
			throw new RedException(e);
		}

		return idsFiltered;
	}

	/**
	 * Calcola se i documenti restituiti dalla ricerca sono omogenei ai fini dei
	 * metadati estesi da visualizzare, cioè se hanno tutti la stessa coppia
	 * tipologia documento/tipo procedimento.
	 * 
	 * @param documentiRisultatiRicerca
	 * @return
	 */
	private boolean isRisultatiOmogenei(final Collection<MasterDocumentRedDTO> documentiRisultatiRicerca) {
		boolean risultatiOmogenei = true;

		if (!CollectionUtils.isEmpty(documentiRisultatiRicerca)) {
			final Iterator<MasterDocumentRedDTO> itDocumentiRisultatiRicerca = documentiRisultatiRicerca.iterator();

			MasterDocumentRedDTO docRicerca = itDocumentiRisultatiRicerca.next();
			final Integer idTipologiaDocumento = docRicerca.getIdTipologiaDocumento();
			final Integer idTipoProcedimento = docRicerca.getIdTipoProcedimento();

			while (itDocumentiRisultatiRicerca.hasNext()) {
				docRicerca = itDocumentiRisultatiRicerca.next();

				if (!idTipologiaDocumento.equals(docRicerca.getIdTipologiaDocumento()) || !idTipoProcedimento.equals(docRicerca.getIdTipoProcedimento())) {
					risultatiOmogenei = false;
					break;
				}
			}
		}

		return risultatiOmogenei;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaAvanzataDocUCBFacadeSRV#eseguiRicercaFlussiUCB(it.ibm.red.business.dto.ParamsRicercaFlussiDTO,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public List<FascicoloFlussoDTO> eseguiRicercaFlussiUCB(final ParamsRicercaFlussiDTO flussiUCB, final UtenteDTO utente) {
		final List<FascicoloFlussoDTO> out = new ArrayList<>();
		Connection con = null;
		Connection dwhCon = null;
		IFilenetCEHelper fceh = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			boolean isRicercaCG2WithParams = false;
			List<String> idDocumentiFlussoCG2 = null;
			if ((TipoContestoProceduraleEnum.FLUSSO_CG2.equals(flussiUCB.getFlusso())) && (!StringUtils.isNullOrEmpty(flussiUCB.getTipoCont())
							|| !StringUtils.isNullOrEmpty(flussiUCB.getAgeCont()) || !StringUtils.isNullOrEmpty(flussiUCB.getEsercizio()))) {

				isRicercaCG2WithParams = true;
				final String coppiaTipiString = pp.getParameterByString(
						utente.getCodiceAoo() + "." + PropertiesNameEnum.CONTO_GIUDIZIALE.getKey());
				final String coppiaTipiStringFlusso = pp.getParameterByString(
						utente.getCodiceAoo() + "." + PropertiesNameEnum.CONTO_GIUDIZIALE_FLUSSO.getKey());

				if (StringUtils.isNullOrEmpty(coppiaTipiString) && StringUtils.isNullOrEmpty(coppiaTipiStringFlusso)) {
					throw new RedException("Mancata configurazione dei tipi documento relativi ai conti giudiziali.");
				}

				Integer tipoDoc = null;
				Integer tipoProc = null;
				if (!StringUtils.isNullOrEmpty(coppiaTipiString)) {
					final String[] coppiaTipi = coppiaTipiString.split(",");
					tipoDoc = Integer.valueOf(coppiaTipi[0]);
					tipoProc = Integer.valueOf(coppiaTipi[1]);
				}

				Integer tipoDocFlusso = null;
				Integer tipoProcFlusso = null;
				if (!StringUtils.isNullOrEmpty(coppiaTipiStringFlusso)) {
					final String[] coppiaTipi = coppiaTipiStringFlusso.split(",");
					tipoDocFlusso = Integer.valueOf(coppiaTipi[0]);
					tipoProcFlusso = Integer.valueOf(coppiaTipi[1]);
				}

				idDocumentiFlussoCG2 = tipologiaDocumentoDAO.ricercaMetadatiContiGiudizialiUcb(
						utente.getIdAoo().intValue(), tipoDoc, tipoProc, tipoDocFlusso, tipoProcFlusso,
						flussiUCB.getTipoCont(), flussiUCB.getAgeCont(), flussiUCB.getEsercizio(), con);

			}
				
			
			if (!isRicercaCG2WithParams || !CollectionUtils.isEmpty(idDocumentiFlussoCG2)) {
				
				final Map<String, FascicoloFlussoDTO> docs = cercaDocumentiByParamsRicerca(utente.getIdAoo().intValue(), fceh, flussiUCB.getFlusso(),
						flussiUCB.getIdProvvedimento(), flussiUCB.getNumProtMittente(), flussiUCB.getDataProtMittente(), flussiUCB.getDataCreazioneDa(),
						flussiUCB.getDataCreazioneA(), flussiUCB.getOrdinePagamento(), flussiUCB.getCamicia(), flussiUCB.getCapitolo(), 
						flussiUCB.getDecretoLiquidazione(), idDocumentiFlussoCG2);
				
				dwhCon = setupConnection(getFilenetDataSource().getConnection(), false);
				
				eventoLogDAO.filtraDocumentiByDataEvento(docs, flussiUCB.getAmbitoTemporale().calcolaData(), new Date(), dwhCon);
				
				// Uso l'iteratore poiché un for each sui valori di docs
				// potrebbe sollevare una ConcurrentModificationException
				final Iterator<FascicoloFlussoDTO> docsIterator = docs.values().iterator();
				while (docsIterator.hasNext()) {
					final FascicoloFlussoDTO f = docsIterator.next();
					if (f.getDataUltimoEvento() == null) {
						
						// elimino i documenti per i quali non è stato trovato un evento nell'arco
						// temporare scelto
						docsIterator.remove(); // remove nell'iterazione
						docs.remove(String.valueOf(f.getIdDocumento())); // remove entry della mappa
						
					} else {
						
						// altrimenti, popolo il DTO con le info del fascicolo procedimentale
						final FascicoloDTO fasc = fascicoloSRV.getFascicoloProcedimentale(String.valueOf(f.getIdDocumento()), utente.getIdAoo().intValue(), fceh, con);
						
						// Se non è stato utilizzato il filtro per descrizione va considerato il
						// fascicolo a prescindere.
						boolean matchDescFasc = true;
						if (!StringUtils.isNullOrEmpty(flussiUCB.getDescrizioneFascicolo())) {
							
							matchDescFasc = true;
							if ((fasc != null && !StringUtils.isNullOrEmpty(fasc.getDescrizione())) && fasc.getDescrizione().contains(flussiUCB.getDescrizioneFascicolo())) {
								// Se il filtro viene valorizzato e matcha con il fascicolo trovato si considera
								// il fascicolo.
								matchDescFasc = true;
							} else if ((fasc != null && !StringUtils.isNullOrEmpty(fasc.getDescrizione()))
									&& !fasc.getDescrizione().contains(flussiUCB.getDescrizioneFascicolo())) {
								// Se il filtro viene valorizzato e non matcha con il fascicolo trovato non si
								// considera il fascicolo.
								matchDescFasc = false;
							}
							
						}
						
						if (fasc != null && matchDescFasc) {
							f.setNumPratica(Integer.parseInt(fasc.getIdFascicolo()));
							f.setDataApertura(fasc.getDataCreazione());
							f.setDataChiusura(fasc.getDataTerminazione());
							f.setDescrizione(fasc.getDescrizione());
							// posso usare il valueOf?
							switch (fasc.getStato().toUpperCase()) {
							case "APERTO":
								f.setStato(FilenetStatoFascicoloEnum.APERTO);
								break;
							case "CHIUSO":
								f.setStato(FilenetStatoFascicoloEnum.CHIUSO);
								break;
							default:
								f.setStato(FilenetStatoFascicoloEnum.NON_CATALOGATO);
							}
						}
						out.add(f);
					}
				}
			}

		} catch (final Exception e) {
			LOGGER.error("eseguiRicercaFlussiUCB -> Si è verificato un errore durante l'esecuzione della ricerca.", e);
			throw new RedException("eseguiRicercaFlussiUCB -> Si è verificato un errore durante l'esecuzione della ricerca.", e);
		} finally {
			closeConnection(con);
			closeConnection(dwhCon);
			popSubject(fceh);
		}
		return out;
	}

	/**
	 * Ricerca i documenti dai parametri di ricerca.
	 * 
	 * @param idAoo
	 *            identificativo area organizzativa
	 * @param fceh
	 *            filenet helper per content engine
	 * @param flusso
	 *            flusso specificato
	 * @param idProvvedimento
	 *            identificativo provvedimento
	 * @param numProtMittente
	 *            numero del protocollo mittente
	 * @param dataProtMittente
	 *            data protocollo mittente
	 * @param dataCreazioneDa
	 *            data creazione iniziale
	 * @param dataCreazioneA
	 *            data creazione finale
	 * @param ordinePagamento
	 *            ordine pagamento
	 * @param camicia
	 *            camicia del documento
	 * @param capitolo
	 *            capitolo
	 * @param decretoLiquidazione
	 *            decreto liquidazione
	 * @param idDocumenti
	 *            identificativo documenti
	 * @return fascicoli recuperati della ricerca
	 */
	private Map<String, FascicoloFlussoDTO> cercaDocumentiByParamsRicerca(final int idAoo, final IFilenetCEHelper fceh, final TipoContestoProceduraleEnum flusso,
			final Integer idProvvedimento, final Integer numProtMittente, final Date dataProtMittente, final Date dataCreazioneDa, final Date dataCreazioneA,
			final String ordinePagamento, final String camicia, final String capitolo, final String decretoLiquidazione, final List<String> idDocumenti) {
		boolean boolAnd = false;
		Map<String, FascicoloFlussoDTO> docs = null;
		final StringBuilder whereCondition = new StringBuilder();

		if (idProvvedimento != null) {
			whereCondition.append("d.");
			whereCondition.append(pp.getParameterByKey(PropertiesNameEnum.IDENTIFICATORE_PROCESSO_METAKEY)).append(" = ").append(idProvvedimento);
			boolAnd = true;
		}

		if (numProtMittente != null) {
			if (boolAnd) {
				whereCondition.append(AND);
			}
			whereCondition.append("d.");
			whereCondition.append(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_MITTENTE_METAKEY)).append(" = ").append(numProtMittente);
			boolAnd = true;
		}

		if (dataProtMittente != null) {
			final Date dataProtMit = new java.sql.Date(dataProtMittente.getTime());
			if (boolAnd) {
				whereCondition.append(AND);
			}
			whereCondition.append("d.");
			whereCondition.append(pp.getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_MITTENTE_METAKEY)).append(" = ").append(dataProtMit);
			boolAnd = true;
		}

		if (dataCreazioneDa != null) {
			final Date dataCreazDa = new java.sql.Date(dataCreazioneDa.getTime());
			if (boolAnd) {
				whereCondition.append(AND);
			}
			whereCondition.append("d.").append(pp.getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY)).append(" >= ").append(dataCreazDa);
			boolAnd = true;
		}

		if (dataCreazioneA != null) {
			if (boolAnd) {
				whereCondition.append(AND);
			}
			final Date dataCreazA = new java.sql.Date(dataCreazioneA.getTime());
			whereCondition.append("d.").append(pp.getParameterByKey(PropertiesNameEnum.DATA_CREAZIONE_METAKEY)).append(" <= ").append(dataCreazA);
			boolAnd = true;
		}

		if (flusso != null) {
			if (boolAnd) {
				whereCondition.append(AND);
			}
			whereCondition.append("d.").append(pp.getParameterByKey(PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY)).append(" = '").append(flusso.getId() + "'");
			boolAnd = true;
		}

		if (!StringUtils.isNullOrEmpty(ordinePagamento)) {
			if (boolAnd) {
				whereCondition.append(AND);
			}
			whereCondition.append("d.").append(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY)).append(LIKE).append("'%").append(ordinePagamento).append("%'");
			boolAnd = true;
		}

		if (!StringUtils.isNullOrEmpty(camicia)) {
			if (boolAnd) {
				whereCondition.append(AND);
			}
			whereCondition.append("d.").append(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY)).append(LIKE).append("'%").append(camicia).append("%'");
			boolAnd = true;
		}

		if (!StringUtils.isNullOrEmpty(capitolo)) {
			if (boolAnd) {
				whereCondition.append(AND);
			}
			whereCondition.append("d.").append(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY)).append(LIKE).append("'%").append(capitolo).append("%'");
			boolAnd = true;
		}

		if (!StringUtils.isNullOrEmpty(decretoLiquidazione)) {
			if (boolAnd) {
				whereCondition.append(AND);
			}
			whereCondition.append("d.").append(pp.getParameterByKey(PropertiesNameEnum.DECRETO_LIQUIDAZIONE_METAKEY)).append(LIKE).append("'%").append(decretoLiquidazione)
					.append("%'");
			boolAnd = true;
		}
		
		
		if(!it.ibm.red.business.utils.CollectionUtils.isEmptyOrNull(idDocumenti)) {
			if (boolAnd) {
				whereCondition.append(AND);
			}
			whereCondition.append("d.").append(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY)).append(" IN (");
			for (int i = 0; i < idDocumenti.size(); i++) {
				whereCondition.append("'").append(idDocumenti.get(i)).append("'");
				if (i < (idDocumenti.size() - 1)) {
					whereCondition.append(", ");
				}
			}
			whereCondition.append(")");
		}
		
		final DocumentSet ds = fceh.getDocuments(idAoo, whereCondition.toString(), pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), true, true);
		docs = new HashMap<>();

		if (ds != null) {
			@SuppressWarnings("rawtypes")
			final Iterator it = ds.iterator();
			while (it.hasNext()) {

				final Document doc = (Document) it.next();
				final String documentTitle = doc.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
				final String identificatoreProcesso = doc.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.IDENTIFICATORE_PROCESSO_METAKEY));

				final FascicoloFlussoDTO ff = new FascicoloFlussoDTO();
				ff.setIdDocumento(Integer.parseInt(documentTitle));
				ff.setIdProvvedimento(identificatoreProcesso);

				final String numProtMit = doc.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_MITTENTE_METAKEY));
				if (numProtMit != null) {
					ff.setNumeroProtocolloMittente(Integer.valueOf(numProtMit));
				}

				final Date dataProtMit = doc.getProperties().getDateTimeValue(pp.getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_MITTENTE_METAKEY));
				ff.setDataProtocolloMittente(dataProtMit);

				docs.put(documentTitle, ff);
			}
		}

		return docs;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaAvanzataDocUCBFacadeSRV#getStoricoDocumentoFlusso(java.lang.Integer,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Collection<StoricoDTO> getStoricoDocumentoFlusso(final Integer idDocumento, final UtenteDTO utente) {
		return storicoSRV.getStorico(idDocumento, utente.getIdAoo().intValue());
	}

	private static Boolean metadatoSelected(final MetadatoDTO metadato) {
		Boolean out = true;

		if (metadato instanceof LookupTableDTO) {
			final LookupTableDTO luDTO = (LookupTableDTO) metadato;
			if (luDTO.getLookupValueSelected() == null || luDTO.getLookupValueSelected().getValue() == null) {
				out = false;
			}
		} else if (metadato instanceof AnagraficaDipendentiComponentDTO) {
			final AnagraficaDipendentiComponentDTO adDTO = (AnagraficaDipendentiComponentDTO) metadato;
			if (adDTO.getSelectedValues() == null || adDTO.getSelectedValues().isEmpty()) {
				out = false;
			}
		} else if ((metadato.getSelectedValue() == null) || (metadato.getSelectedValue() instanceof String && ((String) metadato.getSelectedValue()).isEmpty())) {
			out = false;
		}

		return out;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaAvanzataDocUCBFacadeSRV#eseguiRicercaEstrattiContoUCB(it.ibm.red.business.dto.ParamsRicercaEstrattiContoDTO,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Collection<EstrattiContoUcbDTO> eseguiRicercaEstrattiContoUCB(ParamsRicercaEstrattiContoDTO estrattiContoUCB, UtenteDTO utente) {
		String coppiaTipiString = pp.getParameterByString(utente.getCodiceAoo()+"."+PropertiesNameEnum.ESTRATTO_CONTO_TRIMESTRALE_CCVT.getKey());
		String [] coppiaTipi = coppiaTipiString.split(",");
		Integer tipoDoc = Integer.valueOf(coppiaTipi[0]);
		Integer tipoProc = Integer.valueOf(coppiaTipi[1]);
		
		Collection<EstrattiContoUcbDTO> report = new ArrayList<>();
		Connection con = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			report = tipologiaDocumentoDAO.ricercaEstrattiContoUcb(utente.getIdAoo().intValue(), tipoDoc, tipoProc, estrattiContoUCB, con);
		} catch (final Exception e) {
			LOGGER.error("eseguiRicercaEstrattiContoUCB -> Si è verificato un errore durante l'esecuzione della ricerca.", e);
		} finally {
			closeConnection(con);
		}
		LOGGER.info("eseguiRicercaEstrattiContoUCB -> END. Numero di documenti trovati: " + report.size());
		return report;
	}
	
	/**
	 * Esegue la ricerca elenco notifiche e conti consuntivi.
	 * 
	 * @param contiConsuntiviUCB
	 * @param utente
	 * @return lista di risultati
	 */
	@Override
	public List<RisultatoRicercaContiConsuntiviDTO> eseguiRicercaContiConsuntiviUCB(final ParamsRicercaContiConsuntiviDTO contiConsuntiviUCB, final UtenteDTO utente) {
		final String coppiaTipiString = pp.getParameterByString(utente.getCodiceAoo()+"."+PropertiesNameEnum.CONTO_CONSUNTIVO_SEDE_ESTERA.getKey());
		final String coppiaTipiStringFlusso = pp.getParameterByString(utente.getCodiceAoo()+"."+PropertiesNameEnum.CONTO_CONSUNTIVO_SEDE_ESTERA_FLUSSO.getKey());
		
		if(StringUtils.isNullOrEmpty(coppiaTipiString) && StringUtils.isNullOrEmpty(coppiaTipiStringFlusso)) {
			throw new RedException("Mancata configurazione dei tipo documento.");
		}
		
		Integer tipoDoc = null;
		Integer tipoProc = null;
		if(!StringUtils.isNullOrEmpty(coppiaTipiString)) {
			final String [] coppiaTipi = coppiaTipiString.split(",");
			tipoDoc = Integer.valueOf(coppiaTipi[0]);
			tipoProc = Integer.valueOf(coppiaTipi[1]);
		}
		
		Integer tipoDocFlusso = null;
		Integer tipoProcFlusso = null;
		if(!StringUtils.isNullOrEmpty(coppiaTipiStringFlusso)) {
			final String [] coppiaTipi = coppiaTipiStringFlusso.split(",");
			tipoDocFlusso = Integer.valueOf(coppiaTipi[0]);
			tipoProcFlusso = Integer.valueOf(coppiaTipi[1]);
		}
		
		List<RisultatoRicercaContiConsuntiviDTO> report = new ArrayList<>();
		Map<Integer, RisultatoRicercaContiConsuntiviDTO> reportMap = new HashMap<>();
		Map<Integer, DatiDocumentoContiConsuntiviDTO> datiDocumentiMap = new HashMap<>();
		Map<Integer, DatiNotificaContiConsuntiviDTO> datiNotificheMap = new HashMap<>();
		
		final List<Integer> documentiList = new ArrayList<>();
		final List<Integer> notificheList = new ArrayList<>();
		final Map<Integer, Integer> documentiNotificheMap = new HashMap<>();
		Connection con = null;
		Connection dwhCon = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			dwhCon = setupConnection(getFilenetDataSource().getConnection(), false);
			reportMap = tipologiaDocumentoDAO.ricercaMetadatiContiConsuntivi(utente.getIdAoo().intValue(), tipoDoc, tipoProc, contiConsuntiviUCB, 
					documentiList, notificheList, documentiNotificheMap, con);
			reportMap.putAll(tipologiaDocumentoDAO.ricercaMetadatiContiConsuntiviFlusso(utente.getIdAoo().intValue(), tipoDocFlusso, tipoProcFlusso, contiConsuntiviUCB, 
					documentiList, notificheList, documentiNotificheMap, con));	
			
			if (!documentiList.isEmpty()) {
				datiDocumentiMap = tipologiaDocumentoDAO.ricercaDatiDocumenti(utente.getIdAoo().intValue(), documentiList, dwhCon);
				if (!notificheList.isEmpty()) {
					datiNotificheMap = tipologiaDocumentoDAO.ricercaDatiNotifiche(utente.getIdAoo().intValue(), notificheList, dwhCon);
				}
			}
			
			for (final Entry<Integer, RisultatoRicercaContiConsuntiviDTO> entry : reportMap.entrySet()) {
				
				final Integer idDocumento = entry.getKey();
				final RisultatoRicercaContiConsuntiviDTO risultatoRicerca = entry.getValue();
								
				final Date dataProtocolloDocumento = datiDocumentiMap.get(idDocumento).getDataProtocollo();
				final Integer numeroProtocolloDocumento = datiDocumentiMap.get(idDocumento).getNumeroProtocollo();
				final String assegnatario = datiDocumentiMap.get(idDocumento).getUtenteAssegnatario();
				final String stato = datiDocumentiMap.get(idDocumento).getStato();
				
				risultatoRicerca.setDataDocumento(dataProtocolloDocumento);
				risultatoRicerca.setNumeroDocumento(numeroProtocolloDocumento);
				risultatoRicerca.setDescrizioneAssegnatario(assegnatario);
				risultatoRicerca.setStato(stato);
				
				if (documentiNotificheMap.containsKey(idDocumento)) {
					final Integer idNotifica = documentiNotificheMap.get(idDocumento);
					if (datiNotificheMap.get(idNotifica) != null) {
						final Date dataProtocolloNotifica = datiNotificheMap.get(idNotifica).getDataProtocollo();
						final Integer numeroProtocolloNotifica = datiNotificheMap.get(idNotifica).getNumeroProtocollo();
						
						risultatoRicerca.setDataNotifica(dataProtocolloNotifica);
						risultatoRicerca.setNumeroNotifica(numeroProtocolloNotifica);
					}
				}
				
				report.add(risultatoRicerca);
			}
		} catch (final Exception e) {
			LOGGER.error("eseguiRicercaContiConsuntiviUCB -> Si è verificato un errore durante l'esecuzione della ricerca.", e);
		} finally {
			closeConnection(con);
			closeConnection(dwhCon);
		}
		LOGGER.info("eseguiRicercaContiConsuntiviUCB -> END. Numero di documenti trovati: " + report.size());
		
		return report;
	}
}