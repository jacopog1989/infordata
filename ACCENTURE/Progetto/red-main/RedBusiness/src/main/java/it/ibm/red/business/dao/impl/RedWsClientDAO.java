package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IRedWsClientDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.RedWsClient;

/**
 * The Class RedWsClientDAO.
 *
 * @author m.crescentini
 * 
 * 	Dao per la gestione client web service Red EVO.
 */
@Repository
public class RedWsClientDAO extends AbstractDAO implements IRedWsClientDAO {
	
	private static final long serialVersionUID = -5242046798234561104L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RedWsClientDAO.class.getName());

	
	/**
	 * Gets the RedWsClient.
	 *
	 * @param idClient	 the id client
	 * @param connection the connection
	 * @return the RedWsClient
	 */
	@Override
	public final RedWsClient getById(final String idClient, final Connection con) {
		RedWsClient redWsClient = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			final String querySQL = "SELECT r.* FROM REDWS_CLIENT r WHERE IDCLIENT = ?";
			ps = con.prepareStatement(querySQL);
			ps.setString(1, idClient);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				redWsClient = populate(rs);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero del client web service con ID: " + idClient, e);
			throw new RedException("Errore durante il recupero del client web service con ID: " + idClient, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		final List<Long> nodiCompetenti = new ArrayList<>();
		final Set<Long> aooCompetenti = new HashSet<>();
		try {
			final String querySQL = "SELECT n.IDNODO, n.IDAOO FROM REDWS_CLIENT_NODO r, nodo n WHERE r.IDNODO = n.IDNODO AND IDCLIENT = ?";
			ps = con.prepareStatement(querySQL);
			ps.setString(1, idClient);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				nodiCompetenti.add(rs.getLong("IDNODO"));
				aooCompetenti.add(rs.getLong("IDAOO"));
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei nodi competenti del client web service con ID: " + idClient, e);
			throw new RedException("Errore durante il recupero dei nodi competenti del client web service con ID: " + idClient, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		if (redWsClient == null) {
			LOGGER.error("RedWSClient non inizializzato prima della chiamata ad un metodo della classe.");
			throw new RedException("RedWSClient non inizializzato prima della chiamata ad un metodo della classe.");
		}
		
		redWsClient.setNodiCompetenti(nodiCompetenti);
		redWsClient.setAooCompetenti(aooCompetenti);
		
		final List<String> serviziAbilitati = new ArrayList<>();
		try {
			final String querySQL = "SELECT S.DESCRSERVIZIO FROM REDWS_CLIENT_SERVIZIO r, REDWS_SERVIZIO s WHERE r.IDSERVIZIO = s.IDSERVIZIO AND r.IDCLIENT = ?";
			ps = con.prepareStatement(querySQL);
			ps.setString(1,  idClient);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				serviziAbilitati.add(rs.getString("DESCRSERVIZIO"));
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei servizi abilitati per il client web service con ID: " + idClient, e);
			throw new RedException("Errore durante il recupero dei servizi abilitati per il client web service con ID: " + idClient, e);
		} finally {
			closeStatement(ps, rs);
		}
		redWsClient.setServiziAbilitati(serviziAbilitati);
		
		return redWsClient;
	}

	/**
	 * @param rs
	 * @return
	 * @throws SQLException 
	 */
	private static RedWsClient populate(final ResultSet rs) throws SQLException {
		return new RedWsClient(rs.getString("IDCLIENT"), rs.getString("PWDCLIENT"));
	}
}
