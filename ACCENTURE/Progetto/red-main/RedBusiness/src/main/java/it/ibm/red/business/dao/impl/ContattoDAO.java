package it.ibm.red.business.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IContattoDAO;
import it.ibm.red.business.dto.ComuneDTO;
import it.ibm.red.business.dto.ProvinciaDTO;
import it.ibm.red.business.dto.RegioneDTO;
import it.ibm.red.business.dto.RicercaRubricaDTO;
import it.ibm.red.business.enums.TipoRubricaEnum;
import it.ibm.red.business.enums.TipologiaIndirizzoEmailEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.utils.StringUtils;

/**
 * Dao per la gestione dei contatti.
 *
 * @author CPIERASC
 */
@Repository
public class ContattoDAO extends AbstractDAO implements IContattoDAO {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -8765283686481290919L;
	
	/**
	 * Query parziale.
	 */
	private static final String NULL_AS_FAX_NULL_AS_IDREGIONEISTAT_NULL_AS_IDPROVINCIAISTAT_NULL_AS_IDCOMUNEISTAT_NULL_AS_DENOMINAZIONEPROVINCIA = " NULL AS FAX,  NULL AS IDREGIONEISTAT,  NULL AS IDPROVINCIAISTAT,  NULL AS IDCOMUNEISTAT,  NULL AS DENOMINAZIONEPROVINCIA, ";
	
	/**
	 * Colonna tipo persona.
	 */

	private static final String TIPOPERSONA_COLUMN = "tipopersona";

	/**
	 * Id contatto letto - label.
	 */
	private static final String ID_CONTATTO_LETTO = "idContatto letto: ";

	/**
	 * Tipo mail IPA.
	 */
	private static final String TIPO_MAIL1_IPA = "tipo_mail1_ipa";

	/**
	 * Mail 1 IPA.
	 */
	private static final String MAIL1_IPA = "mail1_ipa";

	/**
	 * Tipo contatto IPA.
	 */
	private static final String TIPO_CONTATTO_IPA = "tipo_contatto_ipa";

	/**
	 * Des AOO IPA.
	 */
	private static final String DES_AOO_IPA = "des_aoo_ipa";

	/**
	 * Des Amm IPA.
	 */
	private static final String DES_AMM_IPA = "des_amm_ipa";

	/**
	 * Cognome - label.
	 */
	private static final String COGNOME_COLUMN = "cognome";

	/**
	 * Alias gruppo email - label.
	 */
	private static final String ALIASGRUPPOEMAIL = "aliasgruppoemail";

	/**
	 * Colonna id contatto.
	 */
	private static final String IDCONTATTO_COLUMN = "idcontatto";

	/**
	 * Alias contatto - label.
	 */
	private static final String ALIAS_CONTATTO = "aliasContatto";

	/**
	 * Insert gruppo contatto.
	 */
	private static final String INSERT_INTO_GRUPPOCONTATTO_IDGRUPPO_IDCONTATTORED_IDCONTATTOMEF_IDCONTATTOIPA_TIPOLOGIACONTATTO_ISUTENTECANCELLATO_VALUES = "INSERT INTO GRUPPOCONTATTO (IDGRUPPO, IDCONTATTORED, IDCONTATTOMEF, IDCONTATTOIPA, TIPOLOGIACONTATTO , ISUTENTECANCELLATO) VALUES (";

	/**
	 * Colonna ID nodo.
	 */
	private static final String IDNODO = "IDNODO";

	/**
	 * Colonna denominazione regione.
	 */
	private static final String DENOMINAZIONEREGIONE = "DENOMINAZIONEREGIONE";

	/**
	 * Colonna denominazione provincia.
	 */
	private static final String DENOMINAZIONEPROVINCIA = "DENOMINAZIONEPROVINCIA";

	/**
	 * Colonna denominazione comune.
	 */
	private static final String DENOMINAZIONECOMUNE = "DENOMINAZIONECOMUNE";

	/**
	 * Colonna Titolo.
	 */
	private static final String TITOLO = "TITOLO";

	/**
	 * Colonna data aggiunta.
	 */
	private static final String DATA_AGGIUNTA = "DATA_AGGIUNTA";

	/**
	 * Colonna Id provincia ISTAT.
	 */
	private static final String IDPROVINCIAISTAT = "IDPROVINCIAISTAT";

	/**
	 * Colonna Id comune ISTAT.
	 */
	private static final String IDCOMUNEISTAT = "IDCOMUNEISTAT";

	/**
	 * Colonna Id regione ISTAT.
	 */
	private static final String IDREGIONEISTAT = "IDREGIONEISTAT";

	/**
	 * Colonna Id Aoo.
	 */
	private static final String IDAOO = "IDAOO";

	/**
	 * Colonna Cellulare.
	 */
	private static final String CELLULARE = "CELLULARE";

	/**
	 * Colonna Telefono.
	 */
	private static final String TELEFONO = "TELEFONO";

	/**
	 * Colonna Inidirizzo.
	 */
	private static final String INDIRIZZO = "INDIRIZZO";

	/**
	 * Colonna Riferimento.
	 */
	private static final String RIFERIMENTO = "RIFERIMENTO";

	/**
	 * Colonna Tipologia Contatto.
	 */
	private static final String TIPOLOGIACONTATTO = "TIPOLOGIACONTATTO";

	/**
	 * Colonna Alias Contatto.
	 */
	private static final String ALIASCONTATTO = "ALIASCONTATTO";

	/**
	 * Colonna Mail PEC.
	 */
	private static final String MAILPEC = "MAILPEC";

	/**
	 * Colonna Codice Fiscale.
	 */
	private static final String CODICEFISCALE = "CODICEFISCALE";

	/**
	 * Colonna Tipo persona.
	 */
	private static final String TIPOPERSONA = "TIPOPERSONA";

	/**
	 * Colonna Cognome.
	 */
	private static final String COGNOME = "COGNOME";

	/**
	 * Colonna Id Contatto.
	 */
	private static final String IDCONTATTO = "IDCONTATTO";

	/**
	 * Placeholder finale.
	 */
	private static final String VIRGOLA_PLACEHOLDER = ", ? )";

	/**
	 * Upper MAIL Like.
	 */
	private static final String OR_UPPER_MAIL_LIKE = " OR UPPER(MAIL) LIKE ";

	/**
	 * Query parziale.
	 */
	private static final String FROM_GRUPPO_LEFT_JOIN_GRUPPOCONTATTO_ON_GRUPPOCONTATTO_IDGRUPPO_GRUPPO_IDGRUPPO = " FROM GRUPPO LEFT JOIN GRUPPOCONTATTO on GRUPPOCONTATTO.IDGRUPPO = GRUPPO.IDGRUPPO";

	/**
	 * Query parziale.
	 */
	private static final String NULL_AS_TIPOPERSONA_NULL_AS_CODICEFISCALE_NULL_AS_MAIL_NULL_AS_MAILPEC_GRUPPO_DESCRIZIONE_AS_ALIASCONTATTO = " NULL AS TIPOPERSONA,  NULL AS CODICEFISCALE,  NULL AS MAIL,  NULL AS MAILPEC,  GRUPPO.DESCRIZIONE AS ALIASCONTATTO";

	/**
	 * Query parziale.
	 */
	private static final String LEFT_JOIN_CONTATTO_IPA_ON_CONTATTO_IPA_IDCONTATTO_GRUPPOCONTATTO_IDCONTATTOIPA = " LEFT JOIN CONTATTO_IPA on CONTATTO_IPA.IDCONTATTO = GRUPPOCONTATTO.IDCONTATTOIPA";

	/**
	 * Query parziale.
	 */
	private static final String LEFT_JOIN_CONTATTO_ON_CONTATTO_IDCONTATTO_GRUPPOCONTATTO_IDCONTATTORED = " LEFT JOIN CONTATTO on CONTATTO.IDCONTATTO = GRUPPOCONTATTO.IDCONTATTORED";

	/**
	 * Query parziale.
	 */
	private static final String G_ALIASCONTATTO_NULL_AS_IDAOO_GRUPPO_AS_TIPOLOGIACONTATTO_NULL_AS_INDIRIZZO_NULL_AS_CAP_NULL_AS_TELEFONO = " G.ALIASCONTATTO,  NULL AS IDAOO,  'GRUPPO' AS TIPOLOGIACONTATTO, NULL AS INDIRIZZO,  NULL AS CAP,  NULL AS TELEFONO, ";

	/**
	 * Query parziale.
	 */
	private static final String SELECT_G_IDCONTATTO_G_NOME_G_COGNOME_G_TIPOPERSONA_G_CODICEFISCALE_G_MAIL_G_MAILPEC = "SELECT G.IDCONTATTO,  G.NOME,  G.COGNOME,  G.TIPOPERSONA,  G.CODICEFISCALE,  G.MAIL,  G.MAILPEC, ";

	/**
	 * Union keyword.
	 */
	private static final String UNION = " UNION ";

	/**
	 * Query parziale.
	 */
	private static final String NULL_AS_CODICEFISCALE_DECODE_TIPO_MAIL1_ALTRO_MAIL1_NULL_AS_MAIL_DECODE_TIPO_MAIL1_PEC_MAIL1_NULL_AS_MAILPEC = " NULL AS CODICEFISCALE,  DECODE(TIPO_MAIL1, 'altro', MAIL1, DECODE(TIPO_MAIL1, 'Altro', MAIL1, NULL)) AS MAIL,  DECODE(TIPO_MAIL1, 'pec', MAIL1, DECODE(TIPO_MAIL1, 'Pec', MAIL1, NULL)) AS MAILPEC, ";

	/**
	 * Query parziale.
	 */
	private static final String CASE_WHEN_DES_AOO_IS_NOT_NULL_AND_TIPO_CONTATTO_3_THEN_CONCAT_CONCAT_CONCAT_DES_AMM_CONCAT_DES_AOO = " CASE WHEN (DES_AOO IS NOT NULL AND TIPO_CONTATTO = 3 ) THEN CONCAT('[',CONCAT(CONCAT(DES_AMM,CONCAT(' - ',DES_AOO)),']')) ";

	/**
	 * Query parziale.
	 */
	private static final String DECODE_TIPO_MAIL1_ALTRO_MAIL1_NULL_AS_MAIL_DECODE_TIPO_MAIL1_PEC_MAIL1_NULL_AS_MAILPEC_DESCRIZIONE_AS_ALIASCONTATTO = " DECODE(TIPO_MAIL1, 'altro', MAIL1, DECODE(TIPO_MAIL1, 'Altro', MAIL1, NULL)) AS MAIL,  DECODE(TIPO_MAIL1, 'pec', MAIL1, DECODE(TIPO_MAIL1, 'Pec', MAIL1, NULL)) AS MAILPEC, DESCRIZIONE AS ALIASCONTATTO, ";

	/**
	 * Query parziale.
	 */
	private static final String DECODE_TIPO_CONTATTO_1_AMM_DECODE_TIPO_CONTATTO_2_AOO_OU_AS_TIPOPERSONA_NULL_AS_CODICEFISCALE = " DECODE(TIPO_CONTATTO, 1 , 'AMM',  DECODE(TIPO_CONTATTO, 2 , 'AOO', 'OU') )  AS TIPOPERSONA,  NULL AS CODICEFISCALE, ";

	/**
	 * Query parziale.
	 */
	private static final String NULL_AS_IDREGIONEISTAT_NULL_AS_IDPROVINCIAISTAT_NULL_AS_IDCOMUNEISTAT_NULL_AS_CELLULARE_CIPA_RIFERIMENTO = " NULL AS IDREGIONEISTAT,  NULL AS IDPROVINCIAISTAT,  NULL AS IDCOMUNEISTAT,  NULL AS CELLULARE,  cipa.RIFERIMENTO, ";

	/**
	 * Query parziale.
	 */
	private static final String C_ALIASCONTATTO_C_IDAOO_RED_AS_TIPOLOGIACONTATTO_C_INDIRIZZO_C_CAP_C_TELEFONO_C_FAX = " c.ALIASCONTATTO,  c.IDAOO,  'RED' AS TIPOLOGIACONTATTO,  C.INDIRIZZO,  C.CAP,  C.TELEFONO,  C.FAX, ";

	/**
	 * Query parziale.
	 */
	private static final String SELECT_C_IDCONTATTO_C_NOME_C_COGNOME_C_TIPOPERSONA_C_CODICEFISCALE_C_MAIL_C_MAILPEC = "SELECT c.IDCONTATTO,  c.NOME,  c.COGNOME,  c.TIPOPERSONA,  c.CODICEFISCALE,  c.MAIL,  c.MAILPEC, ";

	/**
	 * Where ROWNUM.
	 */
	private static final String WHERE_ROWNUM = " ) WHERE rownum <= ";

	/**
	 * SELECT ALL FROM.
	 */
	private static final String SELECT_FROM = "SELECT * FROM ( ";

	/**
	 * Query parziale.
	 */
	private static final String CONTATTO_IPA_DESCRIZIONE_AS_DESCRIZIONE_IPA_CONTATTO_IPA_REGIONE_AS_REGIONE_IPA = " CONTATTO_IPA.descrizione as descrizione_ipa, CONTATTO_IPA.regione as regione_ipa, ";

	/**
	 * Query parziale.
	 */
	private static final String CONTATTO_IPA_PROVINCIA_AS_PROVINCIA_IPA_CONTATTO_IPA_COMUNE_AS_COMUNE_IPA = " CONTATTO_IPA.provincia as provincia_ipa, CONTATTO_IPA.comune as comune_ipa, ";

	/**
	 * Query parziale.
	 */
	private static final String CONTATTO_IPA_DES_AOO_AS_DES_AOO_IPA_CONTATTO_IPA_DES_AMM_AS_DES_AMM_IPA_FROM_CONTATTO_IPA_INNER_JOIN = " CONTATTO_IPA.DES_AOO as des_aoo_ipa, CONTATTO_IPA.DES_AMM as des_amm_ipa FROM CONTATTO_IPA  INNER JOIN ";

	/**
	 * Query parziale.
	 */
	private static final String SELECT_FROM_CONTATTO_IPA_WHERE_IDCONTATTO = " (Select * from CONTATTO_IPA where IDCONTATTO=";

	/**
	 * Label AND.
	 */
	private static final String AND = " AND ";

	/**
	 * Messaggio di errore tipo rubrica non previsto.
	 */
	private static final String ERROR_TIPO_IMPREVISTO_MSG = "Tipo rubrica non previsto.";

	/**
	 * Messaggio errore modifica contatto.
	 */
	private static final String ERROR_MODIFICA_CONTATTO_MSG = "Errore in fase di modifica del contatto nella base dati";

	/**
	 * Messaggio errore generico ricerca.
	 */
	private static final String GENERIC_ERROR_RICERCA = "Errore durante la ricerca";

	/**
	 * Messaggio errore cancellazione da gruppo.
	 */
	private static final String ERROR_CANCELLAZIONE_DA_GRUPPO_MSG = "Errore durante la cancellazione del gruppo";

	/**
	 * Messaggio errore inserimento contatto.
	 */
	private static final String ERROR_INSERIMENTO_CONTATTO_MSG = "Errore durante l'inserimento del contatto casella postale nel DB";

	/**
	 * Messaggio errore eliminazione contatto.
	 */
	private static final String ERROR_ELIMINAZIONE_ONTHEFLY_MSG = "Errore durante l'eliminazione del contatto dal gruppo on the fly";

	/**
	 * Messaggio errore recupero.
	 */
	private static final String GENERIC_ERROR_RECUPERO_MSG = "Errore durante il recupero";

	/**
	 * Messaggio errore recupero progressivo.
	 */
	private static final String ERROR_RECUPERO_PROGRESSIVO_MSG = "Errore durante il recupero progressivo. ";

	/**
	 * Messaggio errore recupero contatto con denominazione.
	 */
	private static final String ERROR_RECUPERO_CONTATTO_CON_DENOMINAZIONE_MSG = "Errore durante il recupero del contatto con denominazione: '";

	/**
	 * Messaggio errore recupero contatto.
	 */
	private static final String ERROR_RECUPERO_CONTATTO_DA_GRUPPO_MSG = "Errore durante il recupero del contatto all'interno del Gruppo";

	/**
	 * Label "effettuato".
	 */
	private static final String EFFETTUATO_LITERAL = " effettuato. ";

	/**
	 * Label gruppo per la gestione dei messaggi loggati.
	 */
	private static final String GRUPPO_LABEL = " / gruppo ";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(ContattoDAO.class.getName());

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#getContattiByIDs(java.util.List,
	 *      java.sql.Connection).
	 */
	@Override
	public final Collection<Contatto> getContattiByIDs(final List<Long> contattiIDs, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final Collection<Contatto> contatti = new ArrayList<>();
		try {
			final String strContatti = StringUtils.fromLongListToCommaString(contattiIDs);
			// i sanitize sono stati levati perché non funzionano in una lista di stringhe,
			// es: '124, 127' -> non è un numero/i valido/i
			final String querySQL = "SELECT c.IDCONTATTO, c.NOME, c.COGNOME, c.TIPOPERSONA, c.CODICEFISCALE, c.MAIL, c.MAILPEC, c.ALIASCONTATTO "
					+ " FROM CONTATTO c WHERE c.idcontatto IN(" + strContatti + ")"
					+ " UNION SELECT cipa.IDCONTATTO, cipa.DESCRIZIONE AS NOME, NULL AS COGNOME, 'G' AS TIPOPERSONA, NULL AS CODICEFISCALE, "
					+ " DECODE(cipa.TIPO_MAIL1, 'altro', cipa.MAIL1, DECODE(cipa.TIPO_MAIL1, 'Altro', cipa.MAIL1, NULL)) AS MAIL,  DECODE(cipa.TIPO_MAIL1, 'pec', cipa.MAIL1, DECODE(cipa.TIPO_MAIL1, 'Pec', cipa.MAIL1, NULL)) AS MAILPEC, "
					+ " NULL AS ALIASCONTATTO FROM contatto_ipa cipa WHERE cipa.idcontatto IN( " + strContatti + ") AND cipa.DATADISATTIVAZIONE IS NULL "
					+ " UNION SELECT cmef.IDCONTATTO, cmef.NOME, cmef.COGNOME, 'F' AS TIPOPERSONA, NULL AS CODICEFISCALE, "
					+ " DECODE(cmef.TIPO_MAIL1, 'altro', cmef.MAIL1, DECODE(cmef.TIPO_MAIL1, 'Altro', cmef.MAIL1, NULL)) AS MAIL,  DECODE(cmef.TIPO_MAIL1, 'pec', cmef.MAIL1, DECODE(cmef.TIPO_MAIL1, 'Pec', cmef.MAIL1, NULL)) AS MAILPEC, "
					+ " NULL AS ALIASCONTATTO FROM contatto_mef cmef WHERE cmef.idcontatto IN( " + strContatti + ") AND CMEF.DATADISATTIVAZIONE IS NULL ";
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			while (rs.next()) {
				final Contatto contatto = new Contatto(rs.getLong(IDCONTATTO), rs.getString("NOME"), rs.getString(COGNOME), rs.getString(TIPOPERSONA),
						rs.getString(CODICEFISCALE), rs.getString("MAIL"), rs.getString(MAILPEC), rs.getString(ALIASCONTATTO));
				contatti.add(contatto);
			}
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero dei contatti ", e);
		} finally {
			closeStatement(ps, rs);
		}
		return contatti;
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#getContattoByID(boolean,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public final Contatto getContattoByID(final boolean isConsideraGruppi, final Long contattoID, final Connection connection) {
		return getContattoByIDGruppo(isConsideraGruppi, contattoID, connection);
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#getContattoByID(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public final Contatto getContattoByID(final Long contattoID, final Connection connection) {
		return getContattoByIDGruppo(false, contattoID, connection);
	} 
	
	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#getContattoSoloREDByID(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public final Contatto getContattoSoloREDByID(final Long contattoID,final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Contatto contatto = null;
		try {
			String sanitizeContattoID = sanitize(contattoID);
			String query = "SELECT c.IDCONTATTO,  c.NOME,  c.COGNOME,  c.TIPOPERSONA, "
					+ " c.CODICEFISCALE,  c.MAIL,  c.MAILPEC,  c.ALIASCONTATTO, "
					+ " c.IDAOO,  'RED' AS TIPOLOGIACONTATTO,  C.INDIRIZZO,  C.CAP, "
					+ " C.TELEFONO,  C.FAX, "
					+ " C.IDREGIONEISTAT, "
					+ " C.IDPROVINCIAISTAT, "
					+ " C.IDCOMUNEISTAT, "
					+ " C.CELLULARE, "
					+ " NULL AS RIFERIMENTO,"
					+ " ONTHEFLY "
					+ " FROM contatto c " 
					+ " WHERE c.idcontatto = "
					+ sanitizeContattoID + " " 
					;
				 
			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();
			if (rs.next()) {
				contatto = populateContattoGruppo(rs);
				
			}
			return contatto;
		} catch (SQLException e) {
			throw new RedException("Errore durante il recupero del contatto con ID=" + contattoID, e);
		} finally {
			closeStatement(ps, rs);
		}
	}
	
	/**
	 * Recupera il contatto identificato dal {@code contattoID}.
	 *
	 * @param isConsideraGruppi
	 *            se {@code true} considera i contatti gruppo, se {@code false} non
	 *            li considera.
	 * @param contattoID
	 *            Identificativo del contatto da recuperare.
	 * @param connection
	 *            Connessione al database.
	 * @return Contatto recuperato.
	 */
	private static Contatto getContattoByIDGruppo(final boolean isConsideraGruppi, final Long contattoID, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Contatto contatto = null;
		try {
			final String sanitizeContattoID = sanitize(contattoID);
			String query = SELECT_C_IDCONTATTO_C_NOME_C_COGNOME_C_TIPOPERSONA_C_CODICEFISCALE_C_MAIL_C_MAILPEC
					+ C_ALIASCONTATTO_C_IDAOO_RED_AS_TIPOLOGIACONTATTO_C_INDIRIZZO_C_CAP_C_TELEFONO_C_FAX
					+ " C.IDREGIONEISTAT,  C.IDPROVINCIAISTAT,  C.IDCOMUNEISTAT,  C.CELLULARE,  NULL AS RIFERIMENTO, ONTHEFLY  FROM contatto c "
					+ " WHERE c.idcontatto = " + sanitizeContattoID + " UNION SELECT cipa.IDCONTATTO,  cipa.NOME,  cipa.COGNOME,  cipa.TIPOPERSONA, "
					+ " cipa.CODICEFISCALE,  cipa.MAIL,  cipa.MAILPEC,  CONCAT(CONCAT(cipa.ALIASCONTATTO,' '), cipa.RIFERIMENTO) AS ALIASCONTATTO, "
					+ " NULL AS IDAOO,  'IPA' AS TIPOLOGIACONTATTO,  cipa.INDIRIZZO,  cipa.CAP AS CAP,  NULL AS TELEFONO,  NULL AS FAX, "
					+ NULL_AS_IDREGIONEISTAT_NULL_AS_IDPROVINCIAISTAT_NULL_AS_IDCOMUNEISTAT_NULL_AS_CELLULARE_CIPA_RIFERIMENTO
					+ " NULL AS ONTHEFLY  FROM (SELECT * FROM  (SELECT IDCONTATTO,  NULL AS NOME,  NULL AS COGNOME, "
					+ DECODE_TIPO_CONTATTO_1_AMM_DECODE_TIPO_CONTATTO_2_AOO_OU_AS_TIPOPERSONA_NULL_AS_CODICEFISCALE
					+ DECODE_TIPO_MAIL1_ALTRO_MAIL1_NULL_AS_MAIL_DECODE_TIPO_MAIL1_PEC_MAIL1_NULL_AS_MAILPEC_DESCRIZIONE_AS_ALIASCONTATTO
					+ CASE_WHEN_DES_AOO_IS_NOT_NULL_AND_TIPO_CONTATTO_3_THEN_CONCAT_CONCAT_CONCAT_DES_AMM_CONCAT_DES_AOO
					+ " ELSE CONCAT('[',CONCAT(DES_AMM,']')) END  AS RIFERIMENTO,  INDIRIZZO, CAP, DATADISATTIVAZIONE FROM contatto_ipa) cipaDecoded "
					+ " WHERE cipaDecoded.idcontatto = " + sanitizeContattoID + " )cipa  UNION SELECT cmef.IDCONTATTO,  cmef.NOME,  cmef.COGNOME, "
					+ " cmef.TIPOPERSONA,  cmef.CODICEFISCALE,  cmef.MAIL,  cmef.MAILPEC,  cmef.ALIASCONTATTO,  NULL AS IDAOO, "
					+ " 'MEF' AS TIPOLOGIACONTATTO,  NULL AS INDIRIZZO,  NULL AS CAP,  TELEFONO,  NULL AS FAX,  NULL AS IDREGIONEISTAT, "
					+ " NULL AS IDPROVINCIAISTAT,  NULL AS IDCOMUNEISTAT,  NULL AS RIFERIMENTO,  NULL AS CELLULARE,  NULL AS ONTHEFLY "
					+ " FROM ( SELECT IDCONTATTO,  NOME,  COGNOME,  DECODE(TIPO_CONTATTO, 4 , 'UFFICIO', 'UTENTE') AS TIPOPERSONA, "
					+ NULL_AS_CODICEFISCALE_DECODE_TIPO_MAIL1_ALTRO_MAIL1_NULL_AS_MAIL_DECODE_TIPO_MAIL1_PEC_MAIL1_NULL_AS_MAILPEC
					+ " TELEFONO,  DECODE(des_ute, '', des_uff, des_ute) AS ALIASCONTATTO   FROM contatto_mef  WHERE idcontatto = " + sanitizeContattoID
					+ " )cmef ";

			if (isConsideraGruppi) {
				if (!query.isEmpty()) {
					query += UNION;
				}
				query += SELECT_G_IDCONTATTO_G_NOME_G_COGNOME_G_TIPOPERSONA_G_CODICEFISCALE_G_MAIL_G_MAILPEC
						+ G_ALIASCONTATTO_NULL_AS_IDAOO_GRUPPO_AS_TIPOLOGIACONTATTO_NULL_AS_INDIRIZZO_NULL_AS_CAP_NULL_AS_TELEFONO
						+ " NULL AS FAX,  NULL AS IDREGIONEISTAT,  NULL AS IDPROVINCIAISTAT,  NULL AS IDCOMUNEISTAT,  NULL AS CELLULARE, "
						+ " NULL AS RIFERIMENTO,  NULL AS ONTHEFLY ";

				query += " FROM   (SELECT DISTINCT GRUPPO.IDGRUPPO AS IDCONTATTO, GRUPPO.NOMEGRUPPO AS NOME, NULL AS COGNOME,  NULL AS TIPOPERSONA, "
						+ " NULL AS CODICEFISCALE,  NULL AS MAIL,  NULL AS MAILPEC,  GRUPPO.DESCRIZIONE AS ALIASCONTATTO FROM GRUPPO"
						+ " LEFT JOIN GRUPPOCONTATTO on GRUPPOCONTATTO.IDGRUPPO = GRUPPO.IDGRUPPO"
						+ LEFT_JOIN_CONTATTO_ON_CONTATTO_IDCONTATTO_GRUPPOCONTATTO_IDCONTATTORED
						+ LEFT_JOIN_CONTATTO_IPA_ON_CONTATTO_IPA_IDCONTATTO_GRUPPOCONTATTO_IDCONTATTOIPA
						+ " LEFT JOIN CONTATTO_MEF on CONTATTO_MEF.IDCONTATTO = GRUPPOCONTATTO.IDCONTATTOMEF WHERE GRUPPO.ISCANCELLATO = 0 ) G"
						+ " WHERE idcontatto = " + sanitizeContattoID;

			}

			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();
			if (rs.next()) {
				
				contatto = populateContattoGruppo(rs);

			}
			return contatto;
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero del contatto con ID=" + contattoID, e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Restituisce un contatto generato e popolato a partire dalle informazioni
	 * recuperate dal ResulSet.
	 * 
	 * @param rs
	 *            ResultSet dal cui recuperare le informazioni.
	 * @return Contatto generato.
	 * @throws SQLException
	 */
	private static Contatto populateContattoGruppo(ResultSet rs) throws SQLException {
		
		Contatto contatto = new Contatto(rs.getLong(IDCONTATTO), rs.getString("NOME"), rs.getString(COGNOME), rs.getString(TIPOPERSONA), rs.getString(CODICEFISCALE),
				rs.getString("MAIL"), rs.getString(MAILPEC), rs.getString(ALIASCONTATTO));

		contatto.setTipoRubrica(rs.getString(TIPOLOGIACONTATTO));
		contatto.setRiferimento(rs.getString(RIFERIMENTO));
		contatto.setIndirizzo(rs.getString(INDIRIZZO));
		contatto.setTelefono(rs.getString(TELEFONO));
		contatto.setCellulare(rs.getString(CELLULARE));
		
		populateLocRef(rs, contatto);
		
		contatto.setMailSelected(contatto.getMailPec());
		contatto.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEC);
		
		if (StringUtils.isNullOrEmpty(contatto.getMailPec())) {
			contatto.setMailSelected(contatto.getMail());
			contatto.setTipologiaEmail(TipologiaIndirizzoEmailEnum.PEO);
		}
		contatto.setOnTheFly(rs.getInt("ONTHEFLY"));
		return contatto;
	}

	/**
	 * Aggiorna il {@code contatto} con alcune informazioni ulteriori recuperandole
	 * dal ResultSet.
	 * 
	 * @param rs
	 *            ResultSet dal quale vengono recuperate le informazioni.
	 * @param contatto
	 *            Contatto da aggiornare.
	 * @throws SQLException
	 */
	private static void populateLocRef(ResultSet rs, Contatto contatto) throws SQLException {
		
		contatto.setIdAOO(rs.getLong(IDAOO));
		contatto.setCAP(rs.getString("CAP"));
		contatto.setFax(rs.getString("FAX"));
		contatto.setIdRegioneIstat(rs.getString(IDREGIONEISTAT));
		contatto.setIdComuneIstat(rs.getString(IDCOMUNEISTAT));
		contatto.setIdProvinciaIstat(rs.getString(IDPROVINCIAISTAT));
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#getContattiPreferiti(java.lang.Long,
	 *      java.lang.Long, boolean, java.sql.Connection).
	 */
	@Override
	public List<Contatto> getContattiPreferiti(final Long idUfficio, final Long idAOO, final boolean consideraGruppi, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			final String sanitizeIdUfficio = sanitize(idUfficio);
			final String sanitizeIdAOO = sanitize(idAOO);
			String query = "SELECT c.IDCONTATTO,  c.NOME,  c.COGNOME,  c.TIPOPERSONA, "
					+ " c.CODICEFISCALE,  c.MAIL,  c.MAILPEC,  c.ALIASCONTATTO, "
					+ " p.TIPOLOGIACONTATTO, "+ "c.INDIRIZZO, "+ "c.TELEFONO,"
					+ " c.CAP, c.CELLULARE, c.FAX, p.idutente, p.data_aggiunta , u.username,c.TITOLO,'1' as priorita,"
					+ " NULL AS NODOPREFERITI "
					+ "  FROM contatto c, preferiti p left outer join utente u on p.idutente = u.idutente"
					+ " WHERE c.ONTHEFLY=0 AND c.idcontatto =  p.IdContattoRED  AND p.tipologiacontatto='RED'  AND p.idnodo="
					+ sanitizeIdUfficio + " AND c.IdAOO=" + sanitizeIdAOO + " "
					+ " AND c.pubblico=1 AND c.datadisattivazione is null UNION SELECT cipa.IDCONTATTO, "
					+ " cipa.DESCRIZIONE AS NOME,  NULL AS COGNOME, "
					+ " DECODE(TIPO_CONTATTO, 1 , 'AMM',  DECODE(TIPO_CONTATTO, 2 , 'AOO', 'OU') )  AS TIPOPERSONA, "
					+ " NULL AS CODICEFISCALE, "
					+ " DECODE(cipa.TIPO_MAIL1, 'altro', cipa.MAIL1, DECODE(cipa.TIPO_MAIL1, 'Altro', cipa.MAIL1, NULL)) AS MAIL, "
					+ " DECODE(cipa.TIPO_MAIL1, 'pec', MAIL1, DECODE(cipa.TIPO_MAIL1, 'Pec', MAIL1, NULL)) AS MAILPEC, "
					+ " cipa.des_amm AS ALIASCONTATTO,  pcipa.TIPOLOGIACONTATTO, "
					+ "   cipa.INDIRIZZO, "+ "null AS TELEFONO," 
					+ "   cipa.CAP, null AS CELLULARE, null AS FAX, pcipa.idutente, pcipa.data_aggiunta, ucipa.username,null as titolo,'3' as priorita,"
					+ "  NULL AS NODOPREFERITI "
					+ "  FROM contatto_ipa cipa, preferiti pcipa left outer join utente ucipa on pcipa.idutente = ucipa.idutente WHERE cipa.idcontatto = pcipa.idcontattoipa AND cipa.DATADISATTIVAZIONE IS NULL "
					+ " AND pcipa.tipologiacontatto='IPA'  AND pcipa.idnodo=" + sanitizeIdUfficio + "UNION "
					+ " SELECT cmef.IDCONTATTO,  cmef.NOME,  cmef.COGNOME, "
					+ " DECODE(TIPO_CONTATTO, 4 , 'UFFICIO', 'UTENTE') AS TIPOPERSONA, "
					+ " NULL AS CODICEFISCALE, "
					+ " DECODE(cmef.TIPO_MAIL1, 'altro', cmef.MAIL1, DECODE(cmef.TIPO_MAIL1, 'Altro', cmef.MAIL1, NULL)) AS MAIL, "
					+ " DECODE(cmef.TIPO_MAIL1, 'pec', MAIL1, DECODE(cmef.TIPO_MAIL1, 'Pec', MAIL1, NULL)) AS MAILPEC, "
					+ " DECODE(cmef.des_ute, '', cmef.des_uff , cmef.des_ute) AS ALIASCONTATTO, "
					+ " pcmef.TIPOLOGIACONTATTO, "+ "null AS INDIRIZZO, "+ "cmef.TELEFONO,"
					+ " null AS CAP, null AS CELLULARE, null AS FAX, pcmef.idutente,pcmef.data_aggiunta,ucmef.username,null as titolo,'2' as priorita,"
					+ " NULL AS NODOPREFERITI "
					+ "  FROM contatto_mef cmef, preferiti pcmef left outer join utente ucmef on pcmef.idutente = ucmef.idutente"
					+ " WHERE cmef.idcontatto = pcmef.idcontattomef  AND pcmef.tipologiacontatto='MEF' AND CMEF.DATADISATTIVAZIONE IS NULL "
					+ " AND pcmef.idnodo=" + sanitizeIdUfficio;

			if (consideraGruppi) { 
				query += " UNION  SELECT G.IDGRUPPO AS IDCONTATTO,  G.NomeGruppo AS NOME,"
						+ " NULL AS COGNOME,  NULL AS TIPOPERSONA,  NULL AS CODICEFISCALE, "
						+ " NULL AS MAIL,  NULL AS MAILPEC,  G.Descrizione AS ALIASCONTATTO, "
						+ " 'GRUPPO' AS TIPOLOGIACONTATTO, "+ "null AS INDIRIZZO, "+ "null AS TELEFONO,"  
						+ " null AS CAP, null AS CELLULARE, null AS FAX,G.IDUTENTE, G.data_creazione as data_aggiunta, U.USERNAME,null as titolo,'0' as priorita,"
						+ "  g.idnodo  AS NODOPREFERITI"
						+ " FROM GRUPPO G LEFT OUTER JOIN UTENTE U ON G.IDUTENTE = U.IDUTENTE"
						+ " LEFT OUTER JOIN preferiti PG ON G.idgruppo = PG.idgruppo"
						+ " WHERE G.ISCANCELLATO = 0 AND (G.idnodo=" + sanitizeIdUfficio+ " OR "+ "( PG.tipologiacontatto='GRUPPO' AND PG.idnodo=" +sanitizeIdUfficio+" ) ) "
						+ " ORDER BY priorita asc";
				
			}
			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();
			return loadContatti(rs);
		} catch (final Exception e) {
			throw new RedException("Errore durante il recupero dei preferiti", e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	private static List<Contatto> loadContatti(final ResultSet rs) throws SQLException {
		final List<Contatto> output = new ArrayList<>();
		while (rs.next()) {
			final Contatto contatto = new Contatto(rs.getLong(IDCONTATTO), rs.getString("NOME"), rs.getString(COGNOME), rs.getString(TIPOPERSONA),
					rs.getString(CODICEFISCALE), rs.getString("MAIL"), rs.getString(MAILPEC), rs.getString(ALIASCONTATTO));

			contatto.setTipoRubrica(rs.getString(TIPOLOGIACONTATTO));
			contatto.setIndirizzo(rs.getString(INDIRIZZO));
			contatto.setTelefono(rs.getString(TELEFONO));
			contatto.setCAP(rs.getString("CAP"));
			contatto.setCellulare(rs.getString(CELLULARE));
			contatto.setFax(rs.getString("FAX"));

			contatto.setMailSelected(contatto.getMailPec());
			if (StringUtils.isNullOrEmpty(contatto.getMailPec())) {
				contatto.setMailSelected(contatto.getMail());
			}

			if ("GRUPPO".equals(rs.getString(TIPOLOGIACONTATTO))) {
				contatto.setGruppoCreatoDa(rs.getString("USERNAME"));
				contatto.setDataCreazioneGruppo(rs.getDate(DATA_AGGIUNTA));
			} else {
				contatto.setUtenteCheHaAggiuntoAiPreferiti(rs.getString("USERNAME"));
				contatto.setDataDiAggiuntaAiPreferiti(rs.getDate(DATA_AGGIUNTA));
			}

			contatto.setTitolo(rs.getString(TITOLO));
			contatto.setNodoPreferiti(rs.getLong("NODOPREFERITI"));
			output.add(contatto);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#getContattiPreferitiPerAlias(java.lang.String,
	 *      java.lang.Long, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<Contatto> getContattiPreferitiPerAlias(final String aliasDescrizione, final Long idUfficio, final Long idAOO, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			final String sanitizeIdUfficio = sanitize(idUfficio);
			final String sanitizeIdAOO = sanitize(idAOO);
			final String sanitizeAlias = sanitize("%" + (aliasDescrizione.toUpperCase()) + "%");
			final String query = SELECT_C_IDCONTATTO_C_NOME_C_COGNOME_C_TIPOPERSONA_C_CODICEFISCALE_C_MAIL_C_MAILPEC
					+ " c.ALIASCONTATTO,  p.TIPOLOGIACONTATTO, c.INDIRIZZO, c.TELEFONO, c.CAP, c.CELLULARE, c.FAX  FROM contatto c, preferiti p "
					+ " WHERE c.ONTHEFLY=0 and c.idcontatto =  p.IdContattoRED  AND p.tipologiacontatto='RED'  AND p.idnodo=" + sanitizeIdUfficio + "  AND c.IdAOO="
					+ sanitizeIdAOO + "  AND upper(c.ALIASCONTATTO) like " + sanitizeAlias + "  AND c.pubblico=1 AND c.datadisattivazione is null UNION "
					+ "SELECT cipa.IDCONTATTO,  cipa.DESCRIZIONE AS NOME,  NULL AS COGNOME, "
					+ DECODE_TIPO_CONTATTO_1_AMM_DECODE_TIPO_CONTATTO_2_AOO_OU_AS_TIPOPERSONA_NULL_AS_CODICEFISCALE
					+ " DECODE(cipa.TIPO_MAIL1, 'altro', cipa.MAIL1, DECODE(cipa.TIPO_MAIL1, 'Altro', cipa.MAIL1, NULL)) AS MAIL,  DECODE(cipa.TIPO_MAIL1, 'pec', cipa.MAIL1, DECODE(cipa.TIPO_MAIL1, 'Pec', MAIL1, NULL)) AS MAILPEC, "
					+ " cipa.des_amm AS ALIASCONTATTO,  pcipa.TIPOLOGIACONTATTO,  cipa.INDIRIZZO, null AS TELEFONO,"
					+ " cipa.CAP, null AS CELLULARE, null AS FAX  FROM contatto_ipa cipa, preferiti pcipa  WHERE cipa.idcontatto = pcipa.idcontattoipa AND cipa.DATADISATTIVAZIONE IS NULL "
					+ " AND upper(cipa.des_amm) like " + sanitizeAlias + "  AND pcipa.tipologiacontatto='IPA'  AND pcipa.idnodo=" + sanitizeIdUfficio + "UNION "
					+ "SELECT cmef.IDCONTATTO,  cmef.NOME,  cmef.COGNOME,  DECODE(TIPO_CONTATTO, 4 , 'UFFICIO', 'UTENTE') AS TIPOPERSONA, "
					+ " NULL AS CODICEFISCALE,  DECODE(cmef.TIPO_MAIL1, 'altro', cmef.MAIL1, DECODE(cmef.TIPO_MAIL1, 'Altro', cmef.MAIL1, NULL)) AS MAIL, "
					+ " DECODE(cmef.TIPO_MAIL1, 'pec', cmef.MAIL1, DECODE(cmef.TIPO_MAIL1, 'Pec', cmef.MAIL1, NULL)) AS MAILPEC,  DECODE(cmef.des_ute, '', cmef.des_uff , cmef.des_ute) AS ALIASCONTATTO, "
					+ " pcmef.TIPOLOGIACONTATTO, null AS INDIRIZZO, pcmef.TELEFONO null AS CAP, null AS CELLULARE, null AS FAX"
					+ "  FROM contatto_mef cmef, preferiti pcmef  WHERE cmef.idcontatto = pcmef.idcontattomef "
					+ " AND upper(DECODE(cmef.des_ute, '', cmef.des_ute, cmef.des_uff)) like " + sanitizeAlias + "  AND pcmef.tipologiacontatto='MEF' AND cmef.DATADISATTIVAZIONE IS NULL "
					+ " AND pcmef.idnodo=" + sanitizeIdUfficio;

			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();

			return loadContatti(rs);
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException("Errore durante il recupero dei preferiti", e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * Gestisce la rimozione di un contatto dalla lista dei preferiti.
	 * 
	 * @param idUfficio
	 * @param contattoID
	 * @param tipoRubrica
	 * @param connection
	 */
	@Override
	public void rimuoviPreferito(final Long idUfficio, final Long contattoID, final TipoRubricaEnum tipoRubrica, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			if (contattoID == null) {
				throw new RedException("Nessun preferito selezionato");
			}
			final String sanitizeContattoId = sanitize(contattoID);
			final String sanitizeIdUfficio = sanitize(idUfficio);
			String query = "DELETE FROM PREFERITI WHERE IDNODO=" + sanitizeIdUfficio;
			switch (tipoRubrica) {
			case RED:
				query += " AND IDCONTATTORED=" + sanitizeContattoId;
				break;
			case MEF:
				query += " AND IDCONTATTOMEF=" + sanitizeContattoId;
				break;
			case IPA:
				query += " AND IDCONTATTOIPA=" + sanitizeContattoId;
				break;
			case GRUPPO:
				query += " AND IDGRUPPO=" + sanitizeContattoId;
				break;
			default:
				throw new RedException(ERROR_TIPO_IMPREVISTO_MSG);
			}

			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();

		} catch (final Exception e) {
			throw new RedException("Errore durante la rimozione del contatto/gruppo dai preferiti", e);
		} finally {
			closeStatement(ps, rs);
		}

	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#aggiungiAiPreferiti(java.lang.Long,
	 *      java.lang.Long, it.ibm.red.business.enums.TipoRubricaEnum,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public void aggiungiAiPreferiti(final Long idUfficio, final Long contattoID, final TipoRubricaEnum tipoRubrica, final Long idUtente, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean update = false;
		try {
			if (contattoID == null) {
				throw new RedException("Nessun contatto selezionato");
			}
			final String sanitizeContattoId = sanitize(contattoID);
			final String sanitizeIdUfficio = sanitize(idUfficio);
			String query = "INSERT INTO PREFERITI (IDNODO, IDCONTATTORED,IDCONTATTOIPA, IDCONTATTOMEF, IDGRUPPO, TIPOLOGIACONTATTO,IDUTENTE,DATA_AGGIUNTA) VALUES("
					+ sanitizeIdUfficio + ", ";

			switch (tipoRubrica) {
			case RED:
				if (!contattoGiaInPreferiti(idUfficio, contattoID, "IDCONTATTORED", connection)) {
					update = true;
					query += sanitizeContattoId + ", NULL, NULL, NULL,'RED'," + idUtente + VIRGOLA_PLACEHOLDER;
				}
				break;
			case MEF:
				if (!contattoGiaInPreferiti(idUfficio, contattoID, "IDCONTATTOMEF", connection)) {
					update = true;
					query += "NULL, NULL, " + sanitizeContattoId + ", NULL, 'MEF'," + idUtente + VIRGOLA_PLACEHOLDER;
				}
				break;
			case IPA:
				if (!contattoGiaInPreferiti(idUfficio, contattoID, "IDCONTATTOIPA", connection)) {
					update = true;
					query += "NULL, " + sanitizeContattoId + ", NULL, NULL, 'IPA'," + idUtente + VIRGOLA_PLACEHOLDER;
				}
				break;
			case GRUPPO:
				if (!contattoGiaInPreferiti(idUfficio, contattoID, "IDGRUPPO", connection)) {
					update = true;
					query += "NULL, NULL, NULL, " + sanitizeContattoId + ", 'GRUPPO'," + idUtente + VIRGOLA_PLACEHOLDER;
				}
				break;
			default:
				throw new RedException(ERROR_TIPO_IMPREVISTO_MSG);
			}

			if (update) {
				ps = connection.prepareStatement(query);
				ps.setDate(1, new java.sql.Date(new Date().getTime()));
				rs = ps.executeQuery();
			}

		} catch (final Exception e) {
			throw new RedException("Errore durante l'aggiunte del contatto/gruppo ai preferiti", e);
		} finally {
			closeStatement(ps, rs);
		}

	}

	private static void aggiungiAGruppo(final Contatto gruppo, final Collection<Contatto> contattiSelezionati, final Connection connection) {

		final String idGruppo = sanitize(gruppo.getContattoID());
		PreparedStatement ps = null;
		final List<Long> contattiProcessati = new ArrayList<>();
		ResultSet rs = null;
		try {
			String query = "";
			for (final Contatto selContatto : contattiSelezionati) {
				boolean addContatto = false;
				if (contattiProcessati.contains(selContatto.getContattoID())) {
					continue;
				}
				contattiProcessati.add(selContatto.getContattoID());
				final String sanitizeContattoId = sanitize(selContatto.getContattoID());
				switch (selContatto.getTipoRubricaEnum()) {
				case RED:
					if (!contattoGiaInGruppo(gruppo.getContattoID(), selContatto.getContattoID(), "IDCONTATTORED", connection)) {
						addContatto = true;
						query = INSERT_INTO_GRUPPOCONTATTO_IDGRUPPO_IDCONTATTORED_IDCONTATTOMEF_IDCONTATTOIPA_TIPOLOGIACONTATTO_ISUTENTECANCELLATO_VALUES + idGruppo + ", " + sanitizeContattoId + ", NULL, NULL,'RED', 0)";
					}
					break;
				case MEF:
					if (!contattoGiaInGruppo(gruppo.getContattoID(), selContatto.getContattoID(), "IDCONTATTOMEF", connection)) {
						addContatto = true;
						query = INSERT_INTO_GRUPPOCONTATTO_IDGRUPPO_IDCONTATTORED_IDCONTATTOMEF_IDCONTATTOIPA_TIPOLOGIACONTATTO_ISUTENTECANCELLATO_VALUES + idGruppo + ", NULL, " + sanitizeContattoId + ", NULL, 'MEF', 0)";
					}
					break;
				case IPA:
					if (!contattoGiaInGruppo(gruppo.getContattoID(), selContatto.getContattoID(), "IDCONTATTOIPA", connection)) {
						addContatto = true;
						query = INSERT_INTO_GRUPPOCONTATTO_IDGRUPPO_IDCONTATTORED_IDCONTATTOMEF_IDCONTATTOIPA_TIPOLOGIACONTATTO_ISUTENTECANCELLATO_VALUES + idGruppo + ", NULL,  NULL," + sanitizeContattoId + ", 'IPA', 0)";
					}
					break;
				default:
					throw new RedException(ERROR_TIPO_IMPREVISTO_MSG);
				}
				if (addContatto) {
					ps = connection.prepareStatement(query);
					rs = ps.executeQuery();
					closeStatement(ps, rs);
				}
			}
		} catch (final Exception e) {
			throw new RedException("Errore durante l'aggiunta al gruppo", e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	private static boolean contattoGiaInPreferiti(final Long idUfficioToCheck, final Long idContattoToCheck, final String columnToCheck, final Connection con) {
		
		String query = "SELECT * FROM PREFERITI WHERE IDNODO = " + sanitize(idUfficioToCheck) + AND + /* non mettere sanitize su columnToCheck */
					sanitizeCol(columnToCheck) + "= " + sanitize(idContattoToCheck);
		
		return executeQueryContattoEsistente(con, query);
	}

	private static boolean contattoGiaInGruppo(final Long idgruppoToCheck, final Long idContattoToCheck, final String columnToCheck, final Connection con) {
		
		String query = "SELECT * FROM GRUPPOCONTATTO WHERE IDGRUPPO = " + sanitize(idgruppoToCheck) + AND + /* non mettere sanitize su columnToCheck */
				sanitizeCol(columnToCheck) + "= " + sanitize(idContattoToCheck) + " AND ISUTENTECANCELLATO = 0";
		
		return executeQueryContattoEsistente(con, query);
	}

	/**
	 * @param con
	 * @param query
	 * @return
	 */
	private static boolean executeQueryContattoEsistente(final Connection con, String query) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean contattoGiaEsistente = false;
		try {
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			if (rs.next()) {
				contattoGiaEsistente = true;
			}
			return contattoGiaEsistente;
		} catch (final SQLException e) {
			LOGGER.error(ERROR_RECUPERO_CONTATTO_DA_GRUPPO_MSG, e);
			throw new RedException(ERROR_RECUPERO_CONTATTO_DA_GRUPPO_MSG);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#getPadreContattoIPA(it.ibm.red.business.persistence.model.Contatto,
	 *      java.sql.Connection).
	 */
	@Override
	public Contatto getPadreContattoIPA(final Contatto contattoIpa, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Contatto contatto = null;
		final StringBuilder query = new StringBuilder();
		try {

			query.append("SELECT * FROM(SELECT  CONTATTO_IPA.IDCONTATTO as idcontatto_ipa,  CONTATTO_IPA.cap as cap_ipa,  "
					+ " CONTATTO_IPA.indirizzo as indirizzo_ipa,  CONTATTO_IPA.tipo_contatto as tipo_contatto_ipa, "
					+ " CONTATTO_IPA.mail1 as mail1_ipa, CONTATTO_IPA.tipo_mail1 as tipo_mail1_ipa, "
					+ CONTATTO_IPA_DESCRIZIONE_AS_DESCRIZIONE_IPA_CONTATTO_IPA_REGIONE_AS_REGIONE_IPA
					+ CONTATTO_IPA_PROVINCIA_AS_PROVINCIA_IPA_CONTATTO_IPA_COMUNE_AS_COMUNE_IPA
					+ CONTATTO_IPA_DES_AOO_AS_DES_AOO_IPA_CONTATTO_IPA_DES_AMM_AS_DES_AMM_IPA_FROM_CONTATTO_IPA_INNER_JOIN
					+ SELECT_FROM_CONTATTO_IPA_WHERE_IDCONTATTO + contattoIpa.getContattoID() + ") contattoSel  ON  contattoSel.tipo_contatto=3  AND "
					+ " CONTATTO_IPA.COD_ou IS NOT NULL  AND  contattoSel.COD_ou_padre IS NOT NULL  AND  contattoSel.COD_ou_padre = CONTATTO_IPA.COD_ou "
					+ " AND  contattoSel.COD_AMM IS NOT NULL  AND  contattoSel.COD_AMM = CONTATTO_IPA.COD_AMM  UNION  SELECT "
					+ " CONTATTO_IPA.IDCONTATTO as idcontatto_ipa,  CONTATTO_IPA.cap as cap_ipa,   CONTATTO_IPA.indirizzo as indirizzo_ipa, "
					+ " CONTATTO_IPA.tipo_contatto as tipo_contatto_ipa,  CONTATTO_IPA.mail1 as mail1_ipa, CONTATTO_IPA.tipo_mail1 as tipo_mail1_ipa, "
					+ CONTATTO_IPA_DESCRIZIONE_AS_DESCRIZIONE_IPA_CONTATTO_IPA_REGIONE_AS_REGIONE_IPA
					+ CONTATTO_IPA_PROVINCIA_AS_PROVINCIA_IPA_CONTATTO_IPA_COMUNE_AS_COMUNE_IPA
					+ CONTATTO_IPA_DES_AOO_AS_DES_AOO_IPA_CONTATTO_IPA_DES_AMM_AS_DES_AMM_IPA_FROM_CONTATTO_IPA_INNER_JOIN
					+ SELECT_FROM_CONTATTO_IPA_WHERE_IDCONTATTO + contattoIpa.getContattoID() + ") contattoSel  ON  contattoSel.tipo_contatto=3  AND "
					+ " CONTATTO_IPA.tipo_contatto=2  AND  contattoSel.COD_AMM = CONTATTO_IPA.COD_AMM  UNION  SELECT "
					+ " CONTATTO_IPA.IDCONTATTO as idcontatto_ipa,  CONTATTO_IPA.cap as cap_ipa,  CONTATTO_IPA.indirizzo as indirizzo_ipa, "
					+ " CONTATTO_IPA.tipo_contatto as tipo_contatto_ipa,  CONTATTO_IPA.mail1 as mail1_ipa, CONTATTO_IPA.tipo_mail1 as tipo_mail1_ipa, "
					+ CONTATTO_IPA_DESCRIZIONE_AS_DESCRIZIONE_IPA_CONTATTO_IPA_REGIONE_AS_REGIONE_IPA
					+ CONTATTO_IPA_PROVINCIA_AS_PROVINCIA_IPA_CONTATTO_IPA_COMUNE_AS_COMUNE_IPA
					+ CONTATTO_IPA_DES_AOO_AS_DES_AOO_IPA_CONTATTO_IPA_DES_AMM_AS_DES_AMM_IPA_FROM_CONTATTO_IPA_INNER_JOIN
					+ SELECT_FROM_CONTATTO_IPA_WHERE_IDCONTATTO + contattoIpa.getContattoID() + ") contattoSel  ON  contattoSel.tipo_contatto=2  AND "
					+ " CONTATTO_IPA.tipo_contatto=1  AND  contattoSel.COD_AMM = CONTATTO_IPA.COD_AMM ) order by tipo_contatto_ipa desc");

			LOGGER.info("Ricerca contatto rubricaIPA: " + query.toString());

			ps = con.prepareStatement(query.toString());
			rs = ps.executeQuery();

			if (rs.next()) {
				contatto = new Contatto();
				contatto.setTipoRubrica("IPA");
				populateIPA(contatto, rs);

			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei contatti dal DB.", e);
			throw new RedException("Errore durante il recupero dei contatti dal DB. ");
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
		return contatto;
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#rimuoviDAGruppo(it.ibm.red.business.persistence.model.Contatto,
	 *      java.util.List, java.sql.Connection).
	 */
	@Override
	public void rimuoviDAGruppo(final Contatto gruppo, final List<Contatto> contattiDaRimuovere, final Connection connection) {

		final String idGruppo = sanitize(gruppo.getContattoID());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {

			for (final Contatto contatto : contattiDaRimuovere) {
				final String idContatto = sanitize(contatto.getContattoID());
				final String query = "DELETE FROM GRUPPOCONTATTO WHERE IDGRUPPO = " + idGruppo + " AND (IDCONTATTORED = " + idContatto + " OR IDCONTATTOMEF = " + idContatto
						+ " OR IDCONTATTOIPA = " + idContatto + " )";
				ps = connection.prepareStatement(query);
				rs = ps.executeQuery();
				closeStatement(ps, rs);
			}

		} catch (final Exception e) {
			throw new RedException("Errore durante la rimozione dal gruppo", e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	private static boolean checkGruppoEsistente(final String nomedelGruppo, final Long idUfficio, final Long idGruppo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean gruppoGiaEsistente = false;
		try {
			final String idUfficioSanitized = sanitize(idUfficio);
			String query = "SELECT * FROM GRUPPO WHERE  IDNODO = " + idUfficioSanitized + " AND UPPER(NOMEGRUPPO) = UPPER(" + sanitize(nomedelGruppo)
					+ ") AND ISCANCELLATO = 0";
			if (idGruppo != null) {
				query += " AND IDGRUPPO <>" + sanitize(idGruppo) + " ";
			}
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			if (rs.next()) {
				gruppoGiaEsistente = true;
			}
			return gruppoGiaEsistente;
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero del gruppo con nomeGruppo= " + nomedelGruppo, e);
			throw new RedException("Errore durante il recupero del gruppo con nomeGruppo: " + nomedelGruppo);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#creaGruppo(it.ibm.red.business.persistence.model.Contatto,
	 *      java.util.Collection, java.lang.Long, java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public void creaGruppo(final Contatto gruppo, final Collection<Contatto> contattiSelezionati, final Long idUfficio, final Long idUtente, final Connection connection) {

		final String nomeGruppo = sanitize(gruppo.getNome());
		String aliasGruppo = sanitize(gruppo.getAliasContatto());
		if (StringUtils.isNullOrEmpty(aliasGruppo)) {
			aliasGruppo = nomeGruppo;
		}
		final String idUfficioSanitized = sanitize(idUfficio);
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {

			if (checkGruppoEsistente(gruppo.getNome(), idUfficio, null, connection)) {
				throw new RedException("Esiste già un gruppo con lo stesso nome per quest'ufficio");
			}
			final String query = "INSERT INTO GRUPPO (IDGRUPPO, IDNODO, DESCRIZIONE, NOMEGRUPPO, ISCANCELLATO,IDUTENTE,DATA_CREAZIONE) VALUES (SEQ_GRUPPO.NEXTVAL,"
					+ idUfficioSanitized + ", " + aliasGruppo + ", " + nomeGruppo + ", 0 , " + idUtente + ",?)";
			ps = connection.prepareStatement(query, new String[] { "IDGRUPPO" });
			ps.setDate(1, new java.sql.Date(new java.util.Date().getTime()));
			ps.executeQuery();

			rs = ps.getGeneratedKeys();
			if (rs != null && rs.next()) {
				gruppo.setContattoID(rs.getLong(1));
			}

			// Aggiungo i contatti al gruppo appena creato
			aggiungiAGruppo(gruppo, contattiSelezionati, connection);
		} catch (final Exception e) {
			throw new RedException("Errore durante la creazione del gruppo", e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#eliminaGruppo(it.ibm.red.business.persistence.model.Contatto,
	 *      java.sql.Connection).
	 */
	@Override
	public void eliminaGruppo(final Contatto gruppo, final Connection connection) {
		final String idGruppo = sanitize(gruppo.getContattoID());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {

			final String query = "UPDATE GRUPPO SET ISCANCELLATO = 1 WHERE IDGRUPPO =" + sanitize(idGruppo);
			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();
			ps.close();
		} catch (final Exception e) {
			throw new RedException("Errore durante l'eliminazione del gruppo", e);
		} finally {
			closeStatement(ps, rs);
		}

	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#modificaGruppo(it.ibm.red.business.persistence.model.Contatto,
	 *      java.util.List, java.util.List, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public void modificaGruppo(final Contatto gruppo, final List<Contatto> contattiSelezionati, final List<Contatto> contattidaRimuovere, final Long idUfficio,
			final Connection connection) {

		final String nomeGruppo = sanitize(gruppo.getNome());
		final String aliasGruppo = sanitize(gruppo.getAliasContatto());
		final String idGruppo = sanitize(gruppo.getContattoID());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {

			if (checkGruppoEsistente(gruppo.getNome(), idUfficio, gruppo.getContattoID(), connection)) {
				throw new RedException("Esiste già un gruppo con lo stesso nome per quest'ufficio");
			}
			final String query = "UPDATE GRUPPO SET DESCRIZIONE = " + aliasGruppo + ", NOMEGRUPPO= " + nomeGruppo + " WHERE IDGRUPPO= " + idGruppo;
			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();

			// Rimuovo i contatti dal gruppo appena modificato
			rimuoviDAGruppo(gruppo, contattidaRimuovere, connection);
			// Aggiungo i contatti al gruppo appena modificato
			// IMPORTANTE: l'aggiunta deve avvenire sempre dopo la rimozione
			// in questo modo nel caso in cui un utente rimuova e poi aggiunga lo stesso
			// contatto non avvertirà alcun cambiamento
			aggiungiAGruppo(gruppo, contattiSelezionati, connection);
		} catch (final Exception e) {
			throw new RedException("Errore durante la modifica del gruppo", e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#ricerca(java.lang.Long,
	 *      it.ibm.red.business.dto.RicercaRubricaDTO, java.sql.Connection).
	 */
	@Override
	public List<Contatto> ricerca(final Long idUfficio, final RicercaRubricaDTO ricercaContattoObj, final Connection connection) {
		return ricerca(idUfficio, ricercaContattoObj, false, true, connection);
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#ricerca(java.lang.Long,
	 *      it.ibm.red.business.dto.RicercaRubricaDTO, boolean,
	 *      java.sql.Connection).
	 */
	@Override
	public List<Contatto> ricerca(final Long idUfficio, final RicercaRubricaDTO ricercaContattoObj, final boolean acceptNullValues, final Connection connection) {
		return ricerca(idUfficio, ricercaContattoObj, acceptNullValues, true, connection);
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#ricerca(java.lang.Long,
	 *      it.ibm.red.business.dto.RicercaRubricaDTO, boolean, boolean,
	 *      java.sql.Connection).
	 */
	@Override
	public List<Contatto> ricerca(final Long idUfficio, final RicercaRubricaDTO ricercaContattoObj, final boolean acceptNullValues, final boolean pubblico,
			final Connection connection) {
		final List<Contatto> listaRicerca = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			// Inizializzazione variabili interne a partire dal DTO di Ricerca -- START
			final String nome = ricercaContattoObj.getNome();
			final String cognome = ricercaContattoObj.getCognome();
			final String alias = ricercaContattoObj.getAliasDescrizione();
			final String mail = ricercaContattoObj.getMail();
			final Integer idAoo = ricercaContattoObj.getIdAoo();
			final Boolean ricercaRED = ricercaContattoObj.getRicercaRED();
			final Boolean ricercaIPA = ricercaContattoObj.getRicercaIPA();
			final Boolean ricercaMEF = ricercaContattoObj.getRicercaMEF();
			final Boolean ricercaGruppo = ricercaContattoObj.getRicercaGruppo();
			final Boolean isUfficio = ricercaContattoObj.getIsUfficio();
			final String struLivelloI = ricercaContattoObj.getStruLivelloI();
			final String struLivelloII = ricercaContattoObj.getStruLivelloII();
			final String struLivelloIII = ricercaContattoObj.getStruLivelloIII();
			final Long idGruppo = ricercaContattoObj.getIdGruppo();
			
			final boolean checkNominativo = StringUtils.isNullOrEmpty(nome) && StringUtils.isNullOrEmpty(cognome);
			final boolean checkUtente = checkNominativo && StringUtils.isNullOrEmpty(mail) && StringUtils.isNullOrEmpty(alias);
			final boolean checkStrutture = StringUtils.isNullOrEmpty(struLivelloI) && StringUtils.isNullOrEmpty(struLivelloII) && StringUtils.isNullOrEmpty(struLivelloIII);
			final boolean checkNull = !acceptNullValues && idGruppo == null;
			
			// Inizializzazione variabili interne a partire dal DTO di Ricerca -- END
			if (checkNull && checkUtente && checkStrutture){
				throw new RedException("Nessun filtro inserito");
			}

			String andConditionMail = "";
			String andConditionNome = "";
			String andConditionCognome = "";
			String andConditionAlias = "";
			String andConditionAoo = "";
			String andConditionMailForGruppo = "";

			String andConditionIsUfficio = "";
			String andConditionstruLivelloI = "";
			String andConditionstruLivelloII = "";
			String andConditionstruLivelloIII = "";

			if (mail != null && !mail.trim().isEmpty()) {
				andConditionMail = " AND (" + creaAndContitionRubrica(MAILPEC, mail, false) + " OR " + creaAndContitionRubrica("MAIL", mail, false) + " ) ";

				andConditionMailForGruppo = " AND ( ( ( " + creaAndContitionRubrica("MAIL", mail, false) + " OR " + creaAndContitionRubrica(MAILPEC, mail, false)
						+ " ) AND CONTATTO.DATADISATTIVAZIONE is null) OR (" + creaAndContitionRubrica("CONTATTO_IPA.MAIL1", mail, false) + " AND CONTATTO_IPA.DATADISATTIVAZIONE IS NULL)  OR ("
						+ creaAndContitionRubrica("CONTATTO_MEF.MAIL1", mail, false) + " AND CONTATTO_MEF.DATADISATTIVAZIONE IS NULL) ) ";
			}
			if (alias != null && !alias.trim().isEmpty()) {
				andConditionAlias = creaAndContitionRubrica(ALIASCONTATTO, alias, true);
			}
			if (nome != null && !nome.trim().isEmpty()) {
				andConditionNome = creaAndContitionRubrica("NOME", nome, true);
			}
			if (cognome != null && !cognome.trim().isEmpty()) {
				andConditionCognome = creaAndContitionRubrica(COGNOME, cognome, true);
			}
			if (idAoo != null) {
				final String sanitizeIdAoo = sanitize(idAoo);
				andConditionAoo = " AND IDAOO = " + sanitizeIdAoo + " ";
			}

			final String nomePerContattoMEF = " DECODE(des_ute, '', des_uff, des_ute)";
			// come su Nsd
			if (isUfficio != null) {
				if (isUfficio) {
					// Ufficio MEF
					andConditionIsUfficio = " AND (contatto_mef.TIPO_CONTATTO = 4 ) ";
				} else {
					// Contatto MEF
					andConditionIsUfficio = " AND (contatto_mef.TIPO_CONTATTO = 5 )";
				}
			}

			if (struLivelloI != null && !struLivelloI.trim().isEmpty()) {
				final String sanitizeStruLivI = sanitize(struLivelloI);
				andConditionstruLivelloI = " AND UPPER (contatto_mef.LIVELLO1) = UPPER(" + sanitizeStruLivI + ") ";
			}

			if (struLivelloII != null && !struLivelloII.trim().isEmpty()) {
				final String sanitizeStruLivII = sanitize(struLivelloII);
				andConditionstruLivelloII = " AND UPPER (contatto_mef.LIVELLO2) = UPPER(" + sanitizeStruLivII + ") ";
			}

			if (struLivelloIII != null && !struLivelloIII.trim().isEmpty()) {
				final String sanitizeStruLivIII = sanitize(struLivelloIII);
				andConditionstruLivelloIII = " AND UPPER (contatto_mef.LIVELLO3) = UPPER(" + sanitizeStruLivIII + ") ";
			}

			String sanitizeIdUfficio = "";
			if (idUfficio != null) {
				sanitizeIdUfficio = sanitize(idUfficio);
			}

			String sanitizeIdGruppo = "";
			// Necessario per ricercare i contatti in base al gruppo
			if (idGruppo != null) {
				sanitizeIdGruppo = sanitize(idGruppo);
			}
			String query = "";
			// RED
			if (Boolean.TRUE.equals(ricercaRED)) {
				query += SELECT_C_IDCONTATTO_C_NOME_C_COGNOME_C_TIPOPERSONA_C_CODICEFISCALE_C_MAIL_C_MAILPEC
						+ C_ALIASCONTATTO_C_IDAOO_RED_AS_TIPOLOGIACONTATTO_C_INDIRIZZO_C_CAP_C_TELEFONO_C_FAX
						+ " C.IDREGIONEISTAT,  C.IDPROVINCIAISTAT,  C.IDCOMUNEISTAT,  PRO.DENOMINAZIONE AS DENOMINAZIONEPROVINCIA, "
						+ " COM.DENOMINAZIONE AS DENOMINAZIONECOMUNE,  R.DENOMINAZIONE AS DENOMINAZIONEREGIONE,  C.CELLULARE,  NULL AS RIFERIMENTO";

				if (idUfficio != null) {
					query += " , p.IDNODO ";
				}

				query += " FROM (SELECT * FROM CONTATTO WHERE " + (pubblico ? "pubblico = 1" : "1 = 1") + " AND ONTHEFLY=0 AND datadisattivazione IS NULL " + andConditionMail
						+ andConditionNome + andConditionCognome + andConditionAlias + andConditionAoo + " )c ";

				query += " LEFT JOIN REGIONE R  ON R.IDREGIONEISTAT = C.IDREGIONEISTAT  LEFT JOIN PROVINCIA PRO  ON PRO.IDPROVINCIAISTAT = C.IDPROVINCIAISTAT"
						+ " LEFT JOIN COMUNE COM  ON COM.IDCOMUNEISTAT = C.IDCOMUNEISTAT";

				if (idUfficio != null) {
					query += " LEFT JOIN preferiti p  ON  c.idcontatto =  p.IdContattoRED  AND p.tipologiacontatto = 'RED'  AND p.idnodo = "
							+ sanitizeIdUfficio;
				}

				if (idGruppo != null) {
					query += " INNER JOIN GRUPPOCONTATTO GCRED  ON  c.idcontatto = GCRED.IdContattoRED  AND GCRED.tipologiacontatto = 'RED' "
							+ " AND GCRED.IDGRUPPO = " + sanitizeIdGruppo;
				}

			}
			// IPA
			if (Boolean.TRUE.equals(ricercaIPA)) {
				if (!query.isEmpty()) {
					query += UNION;
				}
				query += "SELECT cipa.IDCONTATTO,  cipa.NOME,  cipa.COGNOME,  cipa.TIPOPERSONA,  cipa.CODICEFISCALE,  cipa.MAIL,  cipa.MAILPEC, "
						+ " CONCAT(CONCAT(cipa.ALIASCONTATTO,' '), cipa.RIFERIMENTO) AS ALIASCONTATTO,  NULL AS IDAOO,  'IPA' AS TIPOLOGIACONTATTO, "
						+ " cipa.INDIRIZZO,  NULL AS CAP,  NULL AS TELEFONO,  NULL AS FAX,  NULL AS IDREGIONEISTAT,  NULL AS IDPROVINCIAISTAT, "
						+ " NULL AS IDCOMUNEISTAT,  NULL AS DENOMINAZIONEPROVINCIA,  NULL AS DENOMINAZIONECOMUNE,  NULL AS DENOMINAZIONEREGIONE, "
						+ " NULL AS CELLULARE,  cipa.RIFERIMENTO ";

				if (idUfficio != null) {
					query += " , pcipa.IDNODO ";
				}

				query += " FROM (SELECT * FROM  (SELECT IDCONTATTO,  NULL AS NOME,  NULL AS COGNOME, "
						+ DECODE_TIPO_CONTATTO_1_AMM_DECODE_TIPO_CONTATTO_2_AOO_OU_AS_TIPOPERSONA_NULL_AS_CODICEFISCALE
						+ DECODE_TIPO_MAIL1_ALTRO_MAIL1_NULL_AS_MAIL_DECODE_TIPO_MAIL1_PEC_MAIL1_NULL_AS_MAILPEC_DESCRIZIONE_AS_ALIASCONTATTO
						+ CASE_WHEN_DES_AOO_IS_NOT_NULL_AND_TIPO_CONTATTO_3_THEN_CONCAT_CONCAT_CONCAT_DES_AMM_CONCAT_DES_AOO
						+ " ELSE CONCAT('[',CONCAT(DES_AMM,']')) END  AS RIFERIMENTO,  INDIRIZZO, DATADISATTIVAZIONE FROM contatto_ipa) cipaDecoded WHERE 1=1 " + andConditionMail
						+ andConditionNome + andConditionCognome + andConditionAlias + " )cipa ";

				if (idUfficio != null) {
					query += " LEFT JOIN preferiti pcipa  ON  cipa.idcontatto =  pcipa.IdContattoIPA  AND pcipa.tipologiacontatto='IPA'  AND pcipa.idnodo="
							+ sanitizeIdUfficio;
				}

				if (idGruppo != null) {
					query += " INNER JOIN GRUPPOCONTATTO GCIPA  ON  cipa.idcontatto =  GCIPA.IdContattoIPA  AND GCIPA.tipologiacontatto='IPA' "
							+ " AND GCIPA.IDGRUPPO=" + sanitizeIdGruppo;
				}
			}
			// MEF
			if (Boolean.TRUE.equals(ricercaMEF)) {
				if (!query.isEmpty()) {
					query += UNION;
				}

				query += "SELECT cmef.IDCONTATTO,  cmef.NOME,  cmef.COGNOME,  cmef.TIPOPERSONA,  cmef.CODICEFISCALE,  cmef.MAIL,  cmef.MAILPEC, "
						+ " cmef.ALIASCONTATTO,  NULL AS IDAOO,  'MEF' AS TIPOLOGIACONTATTO,  NULL AS INDIRIZZO,  NULL AS CAP,  TELEFONO, "
						+ NULL_AS_FAX_NULL_AS_IDREGIONEISTAT_NULL_AS_IDPROVINCIAISTAT_NULL_AS_IDCOMUNEISTAT_NULL_AS_DENOMINAZIONEPROVINCIA
						+ " NULL AS DENOMINAZIONECOMUNE,  NULL AS DENOMINAZIONEREGIONE,  NULL AS RIFERIMENTO,  NULL AS CELLULARE ";

				if (idUfficio != null) {
					query += ", pcmef.IDNODO ";
				}

				query += " FROM (SELECT * FROM  (SELECT IDCONTATTO,    NOME,    COGNOME,  DECODE(TIPO_CONTATTO, 4 , 'UFFICIO', 'UTENTE') AS TIPOPERSONA, "
						+ NULL_AS_CODICEFISCALE_DECODE_TIPO_MAIL1_ALTRO_MAIL1_NULL_AS_MAIL_DECODE_TIPO_MAIL1_PEC_MAIL1_NULL_AS_MAILPEC
						+ "TELEFONO, " + nomePerContattoMEF + " AS ALIASCONTATTO, DATADISATTIVAZIONE FROM contatto_mef WHERE 1=1 " + andConditionIsUfficio + andConditionstruLivelloI
						+ andConditionstruLivelloII + andConditionstruLivelloIII + " ) cmefDecoded WHERE 1=1 " + andConditionMail + andConditionNome + andConditionCognome
						+ andConditionAlias + " AND cmefDecoded.DATADISATTIVAZIONE IS NULL )cmef ";

				if (idUfficio != null) {
					query += " LEFT JOIN preferiti pcmef  ON  cmef.idcontatto =  pcmef.IDCONTATTOMEF  AND pcmef.tipologiacontatto='MEF'  AND pcmef.idnodo="
							+ sanitizeIdUfficio;
				}

				if (idGruppo != null) {
					query += " INNER JOIN GRUPPOCONTATTO GCMEF  ON  cmef.idcontatto =  GCMEF.IDCONTATTOMEF  AND GCMEF.tipologiacontatto='MEF' "
							+ " AND GCMEF.IDGRUPPO=" + sanitizeIdGruppo;
				}

			}
			// Gruppo
			if (Boolean.TRUE.equals(ricercaGruppo)) {
				if (!query.isEmpty()) {
					query += UNION;
				}

				query += SELECT_G_IDCONTATTO_G_NOME_G_COGNOME_G_TIPOPERSONA_G_CODICEFISCALE_G_MAIL_G_MAILPEC
						+ G_ALIASCONTATTO_NULL_AS_IDAOO_GRUPPO_AS_TIPOLOGIACONTATTO_NULL_AS_INDIRIZZO_NULL_AS_CAP_NULL_AS_TELEFONO
						+ NULL_AS_FAX_NULL_AS_IDREGIONEISTAT_NULL_AS_IDPROVINCIAISTAT_NULL_AS_IDCOMUNEISTAT_NULL_AS_DENOMINAZIONEPROVINCIA
						+ " NULL AS DENOMINAZIONECOMUNE,  NULL AS DENOMINAZIONEREGIONE,  NULL AS RIFERIMENTO,  NULL AS CELLULARE ";

				String andConditionNodo = "";

				if (idUfficio != null) {
					query += ", PG.IDNODO ";
					andConditionNodo += " AND GRUPPO.IDNODO =" + sanitizeIdUfficio + " ";
				}

				query += " FROM  (SELECT * FROM  (SELECT DISTINCT GRUPPO.IDGRUPPO AS IDCONTATTO, GRUPPO.NOMEGRUPPO AS NOME, NULL AS COGNOME, "
						+ NULL_AS_TIPOPERSONA_NULL_AS_CODICEFISCALE_NULL_AS_MAIL_NULL_AS_MAILPEC_GRUPPO_DESCRIZIONE_AS_ALIASCONTATTO
						+ FROM_GRUPPO_LEFT_JOIN_GRUPPOCONTATTO_ON_GRUPPOCONTATTO_IDGRUPPO_GRUPPO_IDGRUPPO
						+ LEFT_JOIN_CONTATTO_ON_CONTATTO_IDCONTATTO_GRUPPOCONTATTO_IDCONTATTORED
						+ LEFT_JOIN_CONTATTO_IPA_ON_CONTATTO_IPA_IDCONTATTO_GRUPPOCONTATTO_IDCONTATTOIPA
						+ " LEFT JOIN CONTATTO_MEF on CONTATTO_MEF.IDCONTATTO = GRUPPOCONTATTO.IDCONTATTOMEF WHERE GRUPPO.ISCANCELLATO = 0" + andConditionMailForGruppo
						+ andConditionNodo + " ) GruppoASContatto WHERE 1=1 " + andConditionNome + andConditionAlias + " ) G";

				if (idUfficio != null) {
					query += " LEFT JOIN preferiti PG  ON  G.idcontatto =  PG.IDGRUPPO  AND PG.tipologiacontatto='GRUPPO'  AND PG.idnodo=" + sanitizeIdUfficio;
				}

			}

			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				final Contatto contatto = populateContattoComplete(idUfficio, rs);
				listaRicerca.add(contatto);
			}

		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(GENERIC_ERROR_RICERCA, e);
		} finally {
			closeStatement(ps, rs);
		}

		return listaRicerca;

	}

	/**
	 * Popola il contatto con la maggior parte delle informazioni esistenti su un
	 * contatto.
	 * 
	 * @param idUfficio
	 *            Identificativo dell'ufficio.
	 * @param rs
	 *            ResultSet dal quale vengono recuperate le informazioni sul
	 *            contatto da popolare.
	 * @return Contatto popolato.
	 * @throws SQLException
	 */
	private static Contatto populateContattoComplete(final Long idUfficio, ResultSet rs) throws SQLException {
		
		final Contatto contatto = populateContattoBasic(rs);
		populateLocRef(rs, contatto);
		populateLocationContatto(rs, contatto);
		
		// se non ho idnodo evidentemente non sono andato in join con preferiti quindi
		// setto il flag a false
		if (idUfficio != null) {
			contatto.setIsPreferito(rs.getString(IDNODO) != null);
		}
		
		return contatto;
	}
	

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#ricercaCampoSingolo(java.lang.String,
	 *      java.lang.Long, java.lang.Integer, boolean, boolean, boolean, boolean,
	 *      java.lang.String, java.util.ArrayList, java.lang.Long, boolean,
	 *      java.sql.Connection).
	 */
	@Override
	public List<Contatto> ricercaCampoSingolo(final String value, final Long idUfficio, final Integer idAOO, final boolean ricercaRED, final boolean ricercaIPA,
			final boolean ricercaMEF, final boolean ricercaGruppo, final String topN, final ArrayList<Long> contattiDaEscludere, final Long idUfficioGruppo,
			final boolean mostraPrimaPreferitiUfficio, final Connection connection) {
		final List<Contatto> listaRicerca = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			// Inizializzazione variabili interne a partire dal DTO di Ricerca -- START
			final String nome = value;
			final String cognome = value;
			final String alias = value;
			final String mail = value;
			final Integer idAoo = idAOO;

			String orConditionMail = "";
			String orConditionNome = "";
			String orConditionCognome = "";
			String orConditionAlias = "";
			String andConditionAoo = "";
			String orConditionNomeForGruppo = "";

			if (mail != null && !mail.trim().isEmpty()) {
				orConditionMail = " (" + creaORConditionRubrica(MAILPEC, mail, false) + " OR " + creaORConditionRubrica("MAIL", mail, false) + " ) ";

			}
			if (alias != null && !alias.trim().isEmpty()) {
				orConditionAlias = creaORConditionRubrica(ALIASCONTATTO, alias, true);
			}
			if (nome != null && !nome.trim().isEmpty()) {
				orConditionNome = creaORConditionRubrica("NOME", nome, true);
				orConditionNomeForGruppo = creaORConditionRubrica("NOME", nome, false);
			}
			if (cognome != null && !cognome.trim().isEmpty()) {
				orConditionCognome = creaORConditionRubrica(COGNOME, cognome, true);
			}
			if (idAoo != null) {
				final String sanitizeIdAoo = sanitize(idAoo);
				andConditionAoo = " AND IDAOO = " + sanitizeIdAoo + " ";
			}

			final String nomePerContattoMEF = " DECODE(des_ute, '', des_uff, des_ute)";
			// come su Nsd

			String sanitizeIdUfficio = "";
			if (idUfficio != null) {
				sanitizeIdUfficio = sanitize(idUfficio);
			}

			String sanitizeIdUfficioGruppo = "";
			if (idUfficioGruppo != null) {
				sanitizeIdUfficioGruppo = sanitize(idUfficioGruppo);
			}
			String query = "Select * FROM (Select * FROM (";

			// RED
			if (ricercaRED) {
				query += SELECT_C_IDCONTATTO_C_NOME_C_COGNOME_C_TIPOPERSONA_C_CODICEFISCALE_C_MAIL_C_MAILPEC
						+ C_ALIASCONTATTO_C_IDAOO_RED_AS_TIPOLOGIACONTATTO_C_INDIRIZZO_C_CAP_C_TELEFONO_C_FAX
						+ " C.IDREGIONEISTAT,  C.IDPROVINCIAISTAT,  C.IDCOMUNEISTAT,  PRO.DENOMINAZIONE AS DENOMINAZIONEPROVINCIA, "
						+ " COM.DENOMINAZIONE AS DENOMINAZIONECOMUNE,  R.DENOMINAZIONE AS DENOMINAZIONEREGIONE,  C.CELLULARE,  NULL AS RIFERIMENTO ,"
						+ " C.TITOLO ,  '1' AS PRIORITA ";

				if (idUfficio != null) {
					query += " , p.IDNODO,P.IDUTENTE ,P.DATA_AGGIUNTA ,U.NOME AS UTENTECHEHAAGGIUNTO_NOME ,U.COGNOME AS UTENTECHEHAAGGIUNTO_COGNOME ";
				}

				query += "  FROM (SELECT * FROM CONTATTO WHERE pubblico=1 AND ONTHEFLY=0 AND datadisattivazione is null  " + andConditionAoo + " AND (" + orConditionMail + orConditionNome
						+ orConditionCognome + orConditionAlias + " ) )c ";

				query += " LEFT JOIN REGIONE R  ON R.IDREGIONEISTAT=C.IDREGIONEISTAT  LEFT JOIN PROVINCIA PRO  ON PRO.IDPROVINCIAISTAT=C.IDPROVINCIAISTAT"
						+ " LEFT JOIN COMUNE COM  ON COM.IDCOMUNEISTAT=C.IDCOMUNEISTAT";

				if (idUfficio != null) {
					query += " LEFT JOIN preferiti p  ON  c.idcontatto =  p.IdContattoRED  AND p.tipologiacontatto='RED'  AND p.idnodo=" + sanitizeIdUfficio
							+ " LEFT OUTER JOIN UTENTE U ON p.idutente = u.idutente";
				}
				
		
			}

			// IPA
			if (ricercaIPA) {
				if (!query.isEmpty() && ricercaRED) {
					query += UNION;
				}

				query += "SELECT cipa.IDCONTATTO,  cipa.NOME,  cipa.COGNOME,  cipa.TIPOPERSONA,  cipa.CODICEFISCALE,  cipa.MAIL,  cipa.MAILPEC, "
						+ " CONCAT(CONCAT(cipa.ALIASCONTATTO,' '), cipa.RIFERIMENTO) AS ALIASCONTATTO,  NULL AS IDAOO,  'IPA' AS TIPOLOGIACONTATTO, "
						+ " cipa.INDIRIZZO,  NULL AS CAP,  NULL AS TELEFONO,  NULL AS FAX,  NULL AS IDREGIONEISTAT,  NULL AS IDPROVINCIAISTAT, "
						+ " NULL AS IDCOMUNEISTAT,  NULL AS DENOMINAZIONEPROVINCIA,  NULL AS DENOMINAZIONECOMUNE,  NULL AS DENOMINAZIONEREGIONE, "
						+ " NULL AS CELLULARE,  cipa.RIFERIMENTO , NULL AS TITOLO,  '3' AS PRIORITA";

				if (idUfficio != null) {
					query += " , pcipa.IDNODO,NULL AS IDUTENTE,NULL AS DATA_AGGIUNTA ,NULL AS UTENTECHEHAAGGIUNTO_NOME ,NULL AS UTENTECHEHAAGGIUNTO_COGNOME ";
				}

				query += " FROM (SELECT * FROM  (SELECT IDCONTATTO,  NULL AS NOME,  NULL AS COGNOME, "
						+ DECODE_TIPO_CONTATTO_1_AMM_DECODE_TIPO_CONTATTO_2_AOO_OU_AS_TIPOPERSONA_NULL_AS_CODICEFISCALE
						+ DECODE_TIPO_MAIL1_ALTRO_MAIL1_NULL_AS_MAIL_DECODE_TIPO_MAIL1_PEC_MAIL1_NULL_AS_MAILPEC_DESCRIZIONE_AS_ALIASCONTATTO
						+ CASE_WHEN_DES_AOO_IS_NOT_NULL_AND_TIPO_CONTATTO_3_THEN_CONCAT_CONCAT_CONCAT_DES_AMM_CONCAT_DES_AOO
						+ " ELSE CONCAT('[',CONCAT(DES_AMM,']')) END  AS RIFERIMENTO,  INDIRIZZO, DATADISATTIVAZIONE  FROM contatto_ipa) cipaDecoded WHERE 1=1 AND ( "
						+ orConditionMail + orConditionNome + orConditionCognome + orConditionAlias + " AND cipaDecoded.DATADISATTIVAZIONE IS NULL ) )cipa ";

				if (idUfficio != null) {
					query += " LEFT JOIN preferiti pcipa  ON  cipa.idcontatto =  pcipa.IdContattoIPA  AND pcipa.tipologiacontatto='IPA'  AND pcipa.idnodo="
							+ sanitizeIdUfficio;
				}

			}
			if (ricercaMEF) {
				if (!query.isEmpty() && (ricercaRED || ricercaIPA)) {
					query += UNION;
				}
				query += "SELECT cmef.IDCONTATTO,  cmef.NOME,  cmef.COGNOME,  cmef.TIPOPERSONA,  cmef.CODICEFISCALE,  cmef.MAIL,  cmef.MAILPEC, "
						+ " cmef.ALIASCONTATTO,  NULL AS IDAOO,  'MEF' AS TIPOLOGIACONTATTO,  NULL AS INDIRIZZO,  NULL AS CAP,  TELEFONO, "
						+ NULL_AS_FAX_NULL_AS_IDREGIONEISTAT_NULL_AS_IDPROVINCIAISTAT_NULL_AS_IDCOMUNEISTAT_NULL_AS_DENOMINAZIONEPROVINCIA
						+ " NULL AS DENOMINAZIONECOMUNE,  NULL AS DENOMINAZIONEREGIONE,  NULL AS RIFERIMENTO,  NULL AS CELLULARE , NULL AS TITOLO, "
						+ " '2' AS PRIORITA ";

				if (idUfficio != null) {
					query += ", pcmef.IDNODO,NULL AS IDUTENTE,pcmef.DATA_AGGIUNTA ,U.NOME AS UTENTECHEHAAGGIUNTO_NOME ,U.COGNOME AS UTENTECHEHAAGGIUNTO_COGNOME ";
				}

				query += " FROM (SELECT * FROM  (SELECT IDCONTATTO,    NOME,    COGNOME,  DECODE(TIPO_CONTATTO, 4 , 'UFFICIO', 'UTENTE') AS TIPOPERSONA, "
						+ NULL_AS_CODICEFISCALE_DECODE_TIPO_MAIL1_ALTRO_MAIL1_NULL_AS_MAIL_DECODE_TIPO_MAIL1_PEC_MAIL1_NULL_AS_MAILPEC
						+ " TELEFONO, " + nomePerContattoMEF + " AS ALIASCONTATTO, DATADISATTIVAZIONE FROM contatto_mef ) cmefDecoded WHERE 1=1 AND (" + orConditionMail
						+ orConditionNome + orConditionCognome + orConditionAlias + " AND cmefDecoded.DATADISATTIVAZIONE IS NULL ))cmef ";

				if (idUfficio != null) {
					query += " LEFT JOIN preferiti pcmef  ON  cmef.idcontatto =  pcmef.IDCONTATTOMEF  AND pcmef.tipologiacontatto='MEF'  AND pcmef.idnodo="
							+ sanitizeIdUfficio + " LEFT OUTER JOIN UTENTE U ON pcmef.idutente = u.idutente ";
				}

			}

			// Gruppo
			if (ricercaGruppo) {
				if (!query.isEmpty() && (ricercaRED || ricercaIPA || ricercaMEF)) {
					query += UNION;
				}

				query += SELECT_G_IDCONTATTO_G_NOME_G_COGNOME_G_TIPOPERSONA_G_CODICEFISCALE_G_MAIL_G_MAILPEC
						+ G_ALIASCONTATTO_NULL_AS_IDAOO_GRUPPO_AS_TIPOLOGIACONTATTO_NULL_AS_INDIRIZZO_NULL_AS_CAP_NULL_AS_TELEFONO
						+ NULL_AS_FAX_NULL_AS_IDREGIONEISTAT_NULL_AS_IDPROVINCIAISTAT_NULL_AS_IDCOMUNEISTAT_NULL_AS_DENOMINAZIONEPROVINCIA
						+ " NULL AS DENOMINAZIONECOMUNE,  NULL AS DENOMINAZIONEREGIONE,  NULL AS RIFERIMENTO,  NULL AS CELLULARE , NULL AS TITOLO, "
						+ " '0' AS PRIORITA ";

				String andConditionNodo = "";

				if (idUfficio != null) {
					query += ", PG.IDNODO ,NULL AS IDUTENTE ,NULL AS DATA_AGGIUNTA, NULL AS UTENTECHEHAAGGIUNTO_NOME ,NULL AS UTENTECHEHAAGGIUNTO_COGNOME ";
					andConditionNodo += " AND GRUPPO.IDNODO =" + sanitizeIdUfficio + " ";
				}

				query += " FROM  (SELECT * FROM  (SELECT DISTINCT GRUPPO.IDGRUPPO AS IDCONTATTO, GRUPPO.NOMEGRUPPO AS NOME, NULL AS COGNOME, "
						+ NULL_AS_TIPOPERSONA_NULL_AS_CODICEFISCALE_NULL_AS_MAIL_NULL_AS_MAILPEC_GRUPPO_DESCRIZIONE_AS_ALIASCONTATTO
						+ FROM_GRUPPO_LEFT_JOIN_GRUPPOCONTATTO_ON_GRUPPOCONTATTO_IDGRUPPO_GRUPPO_IDGRUPPO
						+ LEFT_JOIN_CONTATTO_ON_CONTATTO_IDCONTATTO_GRUPPOCONTATTO_IDCONTATTORED
						+ LEFT_JOIN_CONTATTO_IPA_ON_CONTATTO_IPA_IDCONTATTO_GRUPPOCONTATTO_IDCONTATTOIPA
						+ " LEFT JOIN CONTATTO_MEF on CONTATTO_MEF.IDCONTATTO = GRUPPOCONTATTO.IDCONTATTOMEF WHERE GRUPPO.ISCANCELLATO = 0 " + andConditionNodo
						+ "AND GRUPPO.IDNODO = " + sanitizeIdUfficioGruppo + " ) GruppoASContatto WHERE 1=1 AND (" + orConditionNomeForGruppo + orConditionAlias
						+ " )) G";

				if (idUfficio != null) {
					query += " LEFT JOIN preferiti PG  ON  G.idcontatto =  PG.IDGRUPPO  AND PG.tipologiacontatto='GRUPPO'  AND PG.idnodo=" + sanitizeIdUfficio;
				}

			}

			if (idUfficio == null || !mostraPrimaPreferitiUfficio) {
				query += " )tabdaOrdinare order by priorita asc) tabellaCont ";
			} else {
				query += " )tabdaOrdinare order by idnodo asc, priorita asc) tabellaCont ";
			}

			if (!StringUtils.isNullOrEmpty(topN)) {
				query += " where ROWNUM <=" + topN;

				if (contattiDaEscludere != null) {
					query += " AND NOT " + StringUtils.createInCondition(IDCONTATTO, contattiDaEscludere.iterator(), false);

				}
			}

			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				
				final Contatto contatto = populateContattoComplete(idUfficioGruppo, rs);
				
				if (idUfficio != null) {
					if (!StringUtils.isNullOrEmpty(rs.getString("UTENTECHEHAAGGIUNTO_NOME")) && !StringUtils.isNullOrEmpty(rs.getString("UTENTECHEHAAGGIUNTO_COGNOME"))) {
						contatto.setUtenteCheHaAggiuntoAiPreferiti(rs.getString("UTENTECHEHAAGGIUNTO_NOME") + " " + rs.getString("UTENTECHEHAAGGIUNTO_COGNOME"));
					}
					contatto.setDataDiAggiuntaAiPreferiti(rs.getDate(DATA_AGGIUNTA));
				}

				contatto.setTitolo(rs.getString(TITOLO));
				listaRicerca.add(contatto);
			}

		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(GENERIC_ERROR_RICERCA, e);
		} finally {
			closeStatement(ps, rs);
		}

		return listaRicerca;

	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#getAllGruppi(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public List<Contatto> getAllGruppi(final Long idUfficio, final Connection connection) {
		final List<Contatto> listaRicerca = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {

			String query = "";
			final String sanitizeIdUfficio = sanitize(idUfficio);
			if (!query.isEmpty()) {
				query += UNION;
			}

			query += "SELECT G.IDCONTATTO,  G.NOME,  G.COGNOME,  G.TIPOPERSONA,  G.CODICEFISCALE,  G.MAIL,  G.MAILPEC,  G.ALIASCONTATTO, "
					+ " 'GRUPPO' AS TIPOLOGIACONTATTO  FROM  (SELECT DISTINCT GRUPPO.IDGRUPPO AS IDCONTATTO, GRUPPO.NOMEGRUPPO AS NOME, NULL AS COGNOME, "
					+ NULL_AS_TIPOPERSONA_NULL_AS_CODICEFISCALE_NULL_AS_MAIL_NULL_AS_MAILPEC_GRUPPO_DESCRIZIONE_AS_ALIASCONTATTO
					+ FROM_GRUPPO_LEFT_JOIN_GRUPPOCONTATTO_ON_GRUPPOCONTATTO_IDGRUPPO_GRUPPO_IDGRUPPO
					+ LEFT_JOIN_CONTATTO_ON_CONTATTO_IDCONTATTO_GRUPPOCONTATTO_IDCONTATTORED
					+ LEFT_JOIN_CONTATTO_IPA_ON_CONTATTO_IPA_IDCONTATTO_GRUPPOCONTATTO_IDCONTATTOIPA
					+ " LEFT JOIN CONTATTO_MEF on CONTATTO_MEF.IDCONTATTO = GRUPPOCONTATTO.IDCONTATTOMEF WHERE GRUPPO.ISCANCELLATO = 0  AND GRUPPO.IDNODO = "
					+ sanitizeIdUfficio + " ) G";

			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				final Contatto contatto = new Contatto(rs.getLong(IDCONTATTO), rs.getString("NOME"), rs.getString(COGNOME), rs.getString(TIPOPERSONA),
						rs.getString(CODICEFISCALE), rs.getString("MAIL"), rs.getString(MAILPEC), rs.getString(ALIASCONTATTO));
				contatto.setTipoRubrica(rs.getString(TIPOLOGIACONTATTO));
				listaRicerca.add(contatto);
			}

		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(GENERIC_ERROR_RICERCA, e);
		} finally {
			closeStatement(ps, rs);
		}

		return listaRicerca;

	}

	private static String creaAndContitionRubrica(final String nomeCampoDB, final String stringaFiltro, final boolean startingWithAnd) {
		final StringBuilder andCondition = new StringBuilder("");

		if (stringaFiltro != null && !stringaFiltro.trim().isEmpty()) {
			final String[] stringaFiltroArray = stringaFiltro.trim().split(" ");
			for (int x = 0; x < stringaFiltroArray.length; x++) {
				final String primaParentesi = x == 0 ? "(" : "";
				final String ultimaParentesi = x == (stringaFiltroArray.length - 1) ? ")" : "";
				final String firstCondition = (!startingWithAnd && x == 0) ? "" : AND;
				final String sanitizeString = sanitize("%" + stringaFiltroArray[x].toUpperCase() + "%");
				andCondition.append(" ").append(firstCondition).append(" ").append(primaParentesi).append(" UPPER(").append(nomeCampoDB).append(") LIKE ")
						.append(sanitizeString).append(ultimaParentesi).append(" ");
			}
		}

		return andCondition.toString();
	}

	private static String creaORConditionRubrica(final String nomeCampoDB, final String stringaFiltro, final boolean startingWithOR) {
		final StringBuilder andCondition = new StringBuilder("");

		if (stringaFiltro != null && !stringaFiltro.trim().isEmpty()) {
			final String[] stringaFiltroArray = stringaFiltro.trim().split(" ");
			for (int x = 0; x < stringaFiltroArray.length; x++) {
				String primaParentesi = "";
				if (x == 0) {
					primaParentesi = "(";
				}

				String ultimaParentesi = "";
				if (x == stringaFiltroArray.length - 1) {
					ultimaParentesi = ")";
				}

				String firstCondition = "";
				if (startingWithOR && x == 0) {
					firstCondition = "OR";
				}

				if (x > 0) {
					firstCondition = "AND";
				}

				final String sanitizeString = sanitize("%" + stringaFiltroArray[x].toUpperCase() + "%");
				andCondition.append(" ").append(firstCondition).append(" ").append(primaParentesi).append(" UPPER(").append(nomeCampoDB).append(") LIKE ")
						.append(sanitizeString).append(ultimaParentesi).append(" ");
			}
		}

		return andCondition.toString();
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#ricercaContattoPerModificaByID(long,
	 *      java.sql.Connection).
	 */
	@Override
	public Contatto ricercaContattoPerModificaByID(final long idContatto, final Connection connection) {
		Contatto contatto = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {

			String query = "";
			final String sanitizeIdContatto = sanitize(idContatto);
			query += "SELECT c.IDCONTATTO,  c.NOME,  c.COGNOME,  c.TIPOPERSONA,  c.CODICEFISCALE,  c.MAIL,  c.MAILPEC,  c.ALIASCONTATTO, "
					+ " c.IDAOO,  'RED' AS TIPOLOGIACONTATTO,  C.INDIRIZZO,  C.CAP,  C.TELEFONO,  C.FAX,  C.IDREGIONEISTAT, "
					+ " C.IDPROVINCIAISTAT,  C.IDCOMUNEISTAT,  p.IDNODO,  PRO.DENOMINAZIONE AS DENOMINAZIONEPROVINCIA, "
					+ " COM.DENOMINAZIONE AS DENOMINAZIONECOMUNE,  R.DENOMINAZIONE AS DENOMINAZIONEREGIONE,  C.CELLULARE, C.TITOLO";

			// MODIFICATA QUERY TOLTA AND CONDITION DATA DISATTIVAZIONE
			query += "  FROM (SELECT * FROM CONTATTO WHERE pubblico=1 and IDCONTATTO=" + sanitizeIdContatto + " )c LEFT JOIN preferiti p "
					+ " ON  c.idcontatto =  p.IdContattoRED  AND p.tipologiacontatto='RED'  LEFT JOIN REGIONE R  ON R.IDREGIONEISTAT=C.IDREGIONEISTAT "
					+ " LEFT JOIN PROVINCIA PRO  ON PRO.IDPROVINCIAISTAT=C.IDPROVINCIAISTAT LEFT JOIN COMUNE COM  ON COM.IDCOMUNEISTAT=C.IDCOMUNEISTAT";

			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();
			rs.next();

			contatto = new Contatto(rs.getLong(IDCONTATTO), rs.getString("NOME"), rs.getString(COGNOME), rs.getString(TIPOPERSONA), rs.getString(CODICEFISCALE),
					rs.getString("MAIL"), rs.getString(MAILPEC), rs.getString(ALIASCONTATTO));

			contatto.setTipoRubrica(rs.getString(TIPOLOGIACONTATTO));
			populateLocationContatto(rs, contatto);
			contatto.setIsPreferito(rs.getString(IDNODO) != null);
			populateLocRef(rs, contatto);

			contatto.setIndirizzo(rs.getString(INDIRIZZO));
			contatto.setTelefono(rs.getString(TELEFONO));
			contatto.setCellulare(rs.getString(CELLULARE));
			contatto.setTitolo(rs.getString(TITOLO));
		
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(GENERIC_ERROR_RICERCA, e);
		} finally {
			closeStatement(ps, rs);
		}

		return contatto;

	}

	/**
	 * Aggiorna il {@code contatto} con le informazioni associate alla regione, al
	 * comune e alla provincia recuperandole dal ResulSet.
	 * 
	 * @param rs
	 *            ResultSet dal quale recuperare le informazioni sul contatto.
	 * @param contatto
	 *            Contatto da aggiornare.
	 * @throws SQLException
	 */
	private static void populateLocationContatto(ResultSet rs, Contatto contatto) throws SQLException {
		final ComuneDTO comuneObj = new ComuneDTO();
		final ProvinciaDTO provinciaObj = new ProvinciaDTO();
		final RegioneDTO regioneObj = new RegioneDTO();

		contatto.setComuneObj(comuneObj);
		contatto.setProvinciaObj(provinciaObj);
		contatto.setRegioneObj(regioneObj);

		comuneObj.setIdComune(rs.getString(IDCOMUNEISTAT));
		comuneObj.setDenominazione(rs.getString(DENOMINAZIONECOMUNE));
		comuneObj.setIdProvincia(rs.getString(IDPROVINCIAISTAT));
		provinciaObj.setIdProvincia(rs.getString(IDPROVINCIAISTAT));
		provinciaObj.setIdRegione(rs.getString(IDREGIONEISTAT));
		provinciaObj.setDenominazione(rs.getString(DENOMINAZIONEPROVINCIA));
		regioneObj.setIdRegione(rs.getString(IDREGIONEISTAT));
		regioneObj.setDenominazione(rs.getString(DENOMINAZIONEREGIONE));
	}

	private static void setStringNullable(final CallableStatement cs, final Integer index, final String value, final Boolean flagDeleteSpecChar) throws SQLException {
		try {
			String newValue = value.trim();
			if (Boolean.TRUE.equals(flagDeleteSpecChar)) {
				newValue = StringUtils.deleteSpecialCharacterForSpace(newValue, false);
			}
			cs.setString(index, newValue);
		} catch (final Exception ex) {
			LOGGER.error(ex);
			cs.setNull(index, Types.VARCHAR);
		}
	}

	private static void setIntNullable(final CallableStatement cs, final Integer index, final Integer value) throws SQLException {
		try {
			cs.setInt(index, value);
		} catch (final Exception ex) {
			LOGGER.error(ex);
			cs.setNull(index, Types.INTEGER);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#checkEsistenzaContattoRED(it.ibm.red.business.persistence.model.Contatto,
	 *      java.sql.Connection).
	 */
	@Override
	public boolean checkEsistenzaContattoRED(final Contatto contattoToCheck, final Connection connection) {
		boolean esiste = false;
		esiste = esisteGiaContattoRED(contattoToCheck, connection);
		return esiste;
	}

	private static String getAndCondition(final String key, final Object value) {
		String output = null;
		if (value == null || StringUtils.isNullOrEmpty(value.toString().trim())) {
			output = AND + key + " IS NULL";
		} else {
			final String v = value.toString();
			output = AND + key + " = " + sanitize(v.trim());
		}
		return output;
	}

	private static boolean esisteGiaContattoRED(final Contatto contattoToCheck, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean esiste = false;
		try {

			if (contattoToCheck.getOnTheFly() != null && contattoToCheck.getOnTheFly() == 1) {
				return esiste;
			}

			final String andConditionNome = getAndCondition("NOME", contattoToCheck.getNome());
			final String andConditionCognome = getAndCondition(COGNOME, contattoToCheck.getCognome());
			final String andConditionAlias = getAndCondition(ALIASCONTATTO, contattoToCheck.getAliasContatto());
			final String andConditionPEO = getAndCondition("MAIL", contattoToCheck.getMail());
			final String andConditionPEC = getAndCondition(MAILPEC, contattoToCheck.getMailPec());
			final String andConditionTipoContatto = getAndCondition(TIPOPERSONA, contattoToCheck.getTipoPersona());
			final String andConditionIndirizzo = getAndCondition(INDIRIZZO, contattoToCheck.getIndirizzo());
			final String andConditionTelefono = getAndCondition(TELEFONO, contattoToCheck.getTelefono());
			final String andConditionFax = getAndCondition("FAX", contattoToCheck.getFax());
			final String andConditionCAP = getAndCondition("CAP", contattoToCheck.getCAP());
			final String andConditionRegione = getAndCondition(IDREGIONEISTAT, contattoToCheck.getIdRegioneIstat());
			final String andConditionProvincia = getAndCondition(IDPROVINCIAISTAT, contattoToCheck.getIdProvinciaIstat());
			final String andConditionComune = getAndCondition(IDCOMUNEISTAT, contattoToCheck.getIdComuneIstat());
			final String andConditionAOO = getAndCondition(IDAOO, contattoToCheck.getIdAOO());
			final String andConditionCellulare = getAndCondition(CELLULARE, contattoToCheck.getCellulare());

			String notContitionIdContatto = "";

			if (contattoToCheck.getContattoID() != null) {
				notContitionIdContatto = " AND IDCONTATTO NOT IN (" + contattoToCheck.getContattoID() + ") ";
			}

			final String querySQL = "SELECT * FROM CONTATTO WHERE ONTHEFLY=0 AND PUBBLICO=1 AND DATADISATTIVAZIONE IS NULL " + andConditionNome + andConditionCognome + andConditionAlias
					+ andConditionPEO + andConditionPEC + andConditionTipoContatto + andConditionIndirizzo + andConditionTelefono + andConditionFax + andConditionCAP
					+ andConditionRegione + andConditionProvincia + andConditionComune + andConditionAOO + andConditionCellulare + notContitionIdContatto;

			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();

			if (rs.next()) {
				esiste = true;
			}
		} catch (final Exception e) {
			LOGGER.error("errore durante la ricerca di un duplicato in Rubrica", e);
			throw new RedException("errore durante la ricerca di un duplicato in Rubrica", e);
		} finally {
			closeStatement(ps, rs);
		}
		return esiste;
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#checkMailPerContattoGiaPresentePerAoo(it.ibm.red.business.persistence.model.Contatto,
	 *      java.sql.Connection, boolean).
	 */
	@Override
	public boolean checkMailPerContattoGiaPresentePerAoo(final Contatto contattoToCheck, final Connection connection, final boolean isInModifica) {
		boolean esiste = false;
		if (!StringUtils.isNullOrEmpty(contattoToCheck.getMail()) || !StringUtils.isNullOrEmpty(contattoToCheck.getMailPec())) {
			esiste = mailPerContattoGiaPresentePerAoo(contattoToCheck, connection, isInModifica);
		}
		return esiste;
	}

	private static boolean mailPerContattoGiaPresentePerAoo(final Contatto contattoToCheck, final Connection connection, final boolean isInModifica) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean esiste = false;
		try {
			String conditionPEO = "";
			String conditionPEC = "";

			// Controllo che le due mail non siano uguali altrimenti dò subito il messaggio
			// d'errore
			if (!StringUtils.isNullOrEmpty(contattoToCheck.getMail()) && !StringUtils.isNullOrEmpty(contattoToCheck.getMailPec())) {
				if (contattoToCheck.getMail().equalsIgnoreCase(contattoToCheck.getMailPec())) {
					esiste = true;
					return esiste;
				} else {
					conditionPEO = " AND ((MAIL = '" + contattoToCheck.getMail() + "' OR MAIL = '" + contattoToCheck.getMailPec() + "')";
					conditionPEC = " OR (MAILPEC = '" + contattoToCheck.getMail() + "' OR MAILPEC = '" + contattoToCheck.getMailPec() + "'))";

				}
			} else {
				if (!StringUtils.isNullOrEmpty(contattoToCheck.getMail())) {
					conditionPEO = " AND (MAIL = '" + contattoToCheck.getMail() + "'";
					conditionPEC = " OR MAILPEC = '" + contattoToCheck.getMail() + "')";
				}

				if (!StringUtils.isNullOrEmpty(contattoToCheck.getMailPec())) {
					conditionPEO = " AND (MAILPEC = '" + contattoToCheck.getMailPec() + "'";
					conditionPEC = " OR MAIL = '" + contattoToCheck.getMailPec() + "')";
				}
			}

			String notContitionIdContatto = "";

			if (isInModifica) {
				notContitionIdContatto = " AND IDCONTATTO NOT IN (" + contattoToCheck.getContattoID() + ") ";
			}

			final StringBuilder sb = new StringBuilder();
			sb.append("SELECT IDCONTATTO,MAIL,MAILPEC,PUBBLICO,DATADISATTIVAZIONE,IDAOO FROM CONTATTO WHERE ONTHEFLY=0 AND PUBBLICO=1 AND DATADISATTIVAZIONE IS NULL AND IDAOO = "
					+ contattoToCheck.getIdAOO() + conditionPEO + conditionPEC + notContitionIdContatto);
			sb.append(UNION);
			sb.append("SELECT IDCONTATTO,MAIL1 AS MAIL,NULL AS MAILPEC,NULL AS PUBBLICO,NULL AS DATADISATTIVAZIONE,NULL AS IDAOO FROM CONTATTO_MEF WHERE DATADISATTIVAZIONE IS NULL AND ");
			sb.append("(MAIL1 = '" + contattoToCheck.getMail() + "' OR MAIL1 = '" + contattoToCheck.getMailPec() + "')");

			ps = connection.prepareStatement(sb.toString());
			rs = ps.executeQuery();

			if (rs.next()) {
				esiste = true;
			}
		} catch (final Exception ex) {
			LOGGER.error("Errore durante la ricerca di una mail duplicata in Rubrica", ex);
			throw new RedException("Errore durante la ricerca di una mail duplicata in Rubrica", ex);
		} finally {
			closeStatement(ps, rs);
		}
		return esiste;
	}

	/**
	 * Inserimento nuovo contatto, idUfficio necessario se si vuole aggiungere ai
	 * preferiti.
	 * 
	 * @param idUfficio
	 * @param inserisciContattoItem
	 * @param connection
	 * @return id del contatto inserito
	 */
	@Override
	public Long inserisci(final Long idUfficio, final Contatto inserisciContattoItem, final Connection connection) {
		Long contattoId = null;

		try {
			contattoId = insertContatto(inserisciContattoItem, connection);
			inserisciContattoItem.setContattoID(contattoId);

			if (Boolean.TRUE.equals(inserisciContattoItem.getIsPreferito()) && idUfficio != null) {
				aggiungiAiPreferiti(idUfficio, inserisciContattoItem.getContattoID(), inserisciContattoItem.getTipoRubricaEnum(), null, connection);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di creazione del contatto nella base dati", e);
			throw new RedException("Errore in fase di creazione del contatto nella base dati");
		}

		return contattoId;
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#modifica(it.ibm.red.business.persistence.model.Contatto,
	 *      java.sql.Connection).
	 */
	@Override
	public void modifica(final Contatto contatto, final Connection connection) {
		try {
			modifica(contatto, connection, false);
		} catch (final Exception e) {
			LOGGER.error(ERROR_MODIFICA_CONTATTO_MSG, e);
			throw new RedException(ERROR_MODIFICA_CONTATTO_MSG, e);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#modifica(it.ibm.red.business.persistence.model.Contatto,
	 *      java.sql.Connection, boolean).
	 */
	@Override
	public void modifica(final Contatto contatto, final Connection connection, final boolean isApprovaRichiestaCreazione) {
		try {
			updateContatto(contatto, connection, isApprovaRichiestaCreazione);
		} catch (final Exception e) {
			LOGGER.error(ERROR_MODIFICA_CONTATTO_MSG, e);
			throw new RedException(ERROR_MODIFICA_CONTATTO_MSG, e);
		}
	}

	/**
	 * Esegue un update sul Contatto.
	 * 
	 * @param contatto
	 * @param con
	 */
	@Override
	public int updateContatto(final Contatto contatto, final Connection con) {
		return updateContatto(contatto, con, false);
	}

	/**
	 * Esegue un update sul Contatto passato come parametro.
	 * 
	 * @param contatto
	 * @param con
	 * @param isApprovaRichiestaCreazione
	 * @return int che rappresenta il risultato dell'update SQL
	 */
	public int updateContatto(final Contatto contatto, final Connection con, final boolean isApprovaRichiestaCreazione) {
		PreparedStatement ps = null;
		final ResultSet rs = null;
		int updateContatto = 0;

		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append(
					"UPDATE CONTATTO SET riferimento = ?, indirizzo = ?, cap = ?, telefono = ?, cellulare = ?, mail = ?, fax = ?, idcomuneistat = ?, idaoo = ?, mailpec = ?, codicefiscale = ?, idprovinciaistat = ?, idregioneistat = ?, tipopersona = ?, nome = ?, cognome = ?, aliascontatto = ?, DATA_MODIFICA = ?,titolo = ? ");
			if (isApprovaRichiestaCreazione) {
				sb.append(" ,DATADISATTIVAZIONE = ?");
				sb.append(" ,pubblico = ?");
				sb.append(" ,onthefly = ?");
			}
			sb.append(" WHERE idcontatto = ?");

			ps = con.prepareStatement(sb.toString());

			putStringInStatement(ps, index++, contatto.getRiferimento(), true, false);
			putStringInStatement(ps, index++, contatto.getIndirizzo(), true, false);
			putStringInStatement(ps, index++, contatto.getCAP(), true, true);
			putStringInStatement(ps, index++, contatto.getTelefono(), true, true);
			putStringInStatement(ps, index++, contatto.getCellulare(), true, true);
			putStringInStatement(ps, index++, contatto.getMail(), true, true);
			putStringInStatement(ps, index++, contatto.getFax(), true, true);
			putStringInStatement(ps, index++, contatto.getIdComuneIstat(), false, null);
			putIntegerInStatement(ps, index++, contatto.getIdAOO().intValue());
			putStringInStatement(ps, index++, contatto.getMailPec(), true, true);
			putStringInStatement(ps, index++, contatto.getCodiceFiscalePIva(), true, true);
			putStringInStatement(ps, index++, contatto.getIdProvinciaIstat(), false, null);
			putStringInStatement(ps, index++, contatto.getIdRegioneIstat(), false, null);
			putStringInStatement(ps, index++, contatto.getTipoPersona(), false, null);
			putStringInStatement(ps, index++, contatto.getNome(), false, null);
			putStringInStatement(ps, index++, contatto.getCognome(), false, null);
			putStringInStatement(ps, index++, contatto.getAliasContatto(), true, false);
			putSysdateInStatement(ps, index++);
			putStringInStatement(ps, index++, contatto.getTitolo(), false, null);

			if (isApprovaRichiestaCreazione) {
				setNullAsDate(ps, index++);
				putIntegerInStatement(ps,index++,1);
				putIntegerInStatement(ps,index++,0);
			}

			ps.setLong(index++, contatto.getContattoID());

			updateContatto = ps.executeUpdate();

		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'aggiornamento del contatto", e);
			throw new RedException("Errore durante l'aggiornamento del contatto", e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
		return updateContatto;
	}

	/**
	 * Imposta la data nello statement come null.
	 * 
	 * @param ps
	 * @param index
	 * @throws SQLException
	 */
	private static void setNullAsDate(final PreparedStatement ps, int index) throws SQLException {
		try {
			ps.setNull(index++, Types.DATE);
		} catch (final Exception ex) {
			LOGGER.error(ex);
			ps.setNull(index - 1, Types.VARCHAR);
		}
	}

	/**
	 * Valorizza un parametro di tipo varchar nel prepared statement.
	 * 
	 * @param ps
	 * @param index
	 * @param value
	 * @param handleSpecialChar se true viene gestito il valore prima di valorizzare
	 *                          il parametro
	 * @param handleApix        se true vengono sostituiti i singoli apici con i
	 *                          doppi apici
	 * @throws SQLException
	 */
	private static void putStringInStatement(final PreparedStatement ps, final int index, final String value, final boolean handleSpecialChar, final Boolean handleApix)
			throws SQLException {
		try {
			
			if(value == null) {
				ps.setNull(index, Types.VARCHAR);
			} else {
				final String trimValue = value.trim();
				ps.setString(index, handleSpecialChar ? StringUtils.deleteSpecialCharacterForSpace(trimValue, handleApix) : trimValue);
			}
			
		} catch (final Exception ex) {
			LOGGER.error(ex);
			ps.setNull(index, Types.VARCHAR);
		}
	}

	/**
	 * Valorizza un parametro di tipo int nel prepared statement.
	 * 
	 * @param ps
	 * @param index
	 * @param value
	 * @throws SQLException
	 */
	private static void putIntegerInStatement(final PreparedStatement ps, int index, final Integer value) throws SQLException {
		try {
			ps.setInt(index++, value);
		} catch (final Exception ex) {
			LOGGER.error(ex);
			ps.setNull(index - 1, Types.INTEGER);
		}
	}

	/**
	 * Valorizza un parametro di tipo Date con il valore della data attuale.
	 * 
	 * @param ps
	 * @param index
	 * @throws SQLException
	 */
	private static void putSysdateInStatement(final PreparedStatement ps, int index) throws SQLException {
		try {
			ps.setDate(index++, new java.sql.Date(new java.util.Date().getTime()));
		} catch (final Exception ex) {
			LOGGER.error(ex);
			ps.setNull(index - 1, Types.VARCHAR);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#eliminaContatto(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public int eliminaContatto(final Long idContatto, final Connection con) {
		PreparedStatement psContatto = null;
		PreparedStatement psPreferiti = null;
		PreparedStatement psGruppo = null;
		int result = 0;
		try {
			psPreferiti = con.prepareStatement("DELETE FROM preferiti WHERE idcontattoRED = ?");
			psPreferiti.setLong(1, idContatto);
			result = psPreferiti.executeUpdate();

			psGruppo = con.prepareStatement("UPDATE GRUPPOCONTATTO SET ISUTENTECANCELLATO = 1 WHERE (IDCONTATTORED = ? OR IDCONTATTOMEF = ? OR IDCONTATTOIPA = ?)");
			psGruppo.setLong(1, idContatto);
			psGruppo.setLong(2, idContatto);
			psGruppo.setLong(3, idContatto);
			result = psGruppo.executeUpdate();

			psContatto = con.prepareStatement("UPDATE CONTATTO SET datadisattivazione = ? WHERE idcontatto = ?");
			psContatto.setDate(1, new java.sql.Date((new Date()).getTime()));
			psContatto.setLong(2, idContatto);
			result = psContatto.executeUpdate();

		} catch (final SQLException e) {
			LOGGER.error("Errore durante la cancellazione del  contatto " + idContatto, e);
			throw new RedException("Errore durante la cancellazione del  contatto", e);
		} finally {
			closeStatement(psContatto);
			closeStatement(psPreferiti);
			closeStatement(psGruppo);

		}
		return result;
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#caricaIlivello(java.sql.Connection).
	 */
	@Override
	public List<String> caricaIlivello(final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<String> primoLivelloList = new ArrayList<>();
		final StringBuilder query = new StringBuilder();
		try {
			query.append("SELECT DISTINCT livello1, id1 FROM contatto_gerarchia ORDER BY(livello1)");

			LOGGER.info("Ricerca Struttura I livello contatti MEF: " + query.toString());

			ps = con.prepareStatement(query.toString());
			rs = ps.executeQuery();

			primoLivelloList.add("");
			while (rs.next()) {
				if (rs.getString("livello1") != null) {
					primoLivelloList.add(rs.getString("livello1"));
				}
			}

			return primoLivelloList;

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero della Struttura I livello contatti MEF dal DB.", e);
			throw new RedException("Errore durante il recupero della Struttura I livello contatti MEF dal DB. ");
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}

	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#caricaIIlivello(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public List<String> caricaIIlivello(final String idComboILivello, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<String> secondoLivelloList = new ArrayList<>();
		final StringBuilder query = new StringBuilder();
		try {

			query.append("SELECT DISTINCT livello2, id2 FROM contatto_gerarchia WHERE UPPER (livello1) like UPPER('" + idComboILivello + "') ORDER BY(livello2) ");

			LOGGER.info("Ricerca Struttura II livello contatti MEF: " + query.toString());

			ps = con.prepareStatement(query.toString());
			rs = ps.executeQuery();

			secondoLivelloList.add("");
			while (rs.next()) {
				if (rs.getString("livello2") != null) {
					secondoLivelloList.add(rs.getString("livello2"));
				}
			}

			return secondoLivelloList;

		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero della Struttura II livello contatti MEF dal DB.", e);
			throw new RedException("Errore durante il recupero della Struttura II livello contatti MEF dal DB. ");
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}

	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#caricaIIIlivello(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public List<String> caricaIIIlivello(final String idcomboIILivello, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<String> terzoLivelloList = new ArrayList<>();
		final StringBuilder query = new StringBuilder();
		try {

			query.append("SELECT DISTINCT livello3, id3 FROM contatto_gerarchia WHERE UPPER(livello2) like UPPER('" + idcomboIILivello + "') ORDER BY(livello3)");

			LOGGER.info("Ricerca Struttura III livello contatti MEF: " + query.toString());

			ps = con.prepareStatement(query.toString());
			rs = ps.executeQuery();

			terzoLivelloList.add("");
			while (rs.next()) {
				if (rs.getString("livello3") != null) {
					terzoLivelloList.add(rs.getString("livello3"));
				}
			}
			return terzoLivelloList;

		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero della Struttura III livello contatti MEF dal DB.", e);
			throw new RedException("Errore durante il recupero della Struttura III livello contatti MEF dal DB. ");
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#ricercaFigliContattoIPA(it.ibm.red.business.persistence.model.Contatto,
	 *      java.sql.Connection).
	 */
	@Override
	public List<Contatto> ricercaFigliContattoIPA(final Contatto contattoIpa, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final ArrayList<Contatto> contatti = new ArrayList<>();
		final StringBuilder query = new StringBuilder();
		try {

			query.append(
					"SELECT CONTATTO_IPA.IDCONTATTO as idcontatto_ipa, CONTATTO_IPA.cap as cap_ipa,  CONTATTO_IPA.indirizzo as indirizzo_ipa, CONTATTO_IPA.tipo_contatto as tipo_contatto_ipa, CONTATTO_IPA.mail1 as mail1_ipa, CONTATTO_IPA.tipo_mail1 as tipo_mail1_ipa, CONTATTO_IPA.descrizione as descrizione_ipa, CONTATTO_IPA.regione as regione_ipa, CONTATTO_IPA.provincia as provincia_ipa, CONTATTO_IPA.comune as comune_ipa, CONTATTO_IPA.DES_AOO as des_aoo_ipa, CONTATTO_IPA.DES_AMM as des_amm_ipa "
							+ "FROM CONTATTO_IPA WHERE ");

			query.append(" CONTATTO_IPA.IDCONTATTO is not null AND contatto_IPA.DATADISATTIVAZIONE IS NULL ");

			if (!contattoIpa.getTipoPersona().isEmpty() && "AMM".equalsIgnoreCase(contattoIpa.getTipoPersona())) {
				query.append(" AND UPPER(CONTATTO_IPA.TIPO_CONTATTO) <> " + 1);
				query.append(" AND CONTATTO_IPA.COD_amm=(Select COD_amm from CONTATTO_IPA where IDCONTATTO=" + contattoIpa.getContattoID() + ")");
			} else if (!contattoIpa.getTipoPersona().isEmpty() && "AOO".equalsIgnoreCase(contattoIpa.getTipoPersona())) {
				query.append(" AND TIPO_CONTATTO = " + 3);
				query.append(" AND CONTATTO_IPA.COD_aoo=(Select COD_aoo from CONTATTO_IPA where IDCONTATTO=" + contattoIpa.getContattoID() + ")");
				query.append(" AND CONTATTO_IPA.COD_amm=(Select COD_amm from CONTATTO_IPA where IDCONTATTO=" + contattoIpa.getContattoID() + ")");
			} else if (!contattoIpa.getTipoPersona().isEmpty() && "OU".equalsIgnoreCase(contattoIpa.getTipoPersona())) {
				query.append(" AND CONTATTO_IPA.COD_ou_padre=(Select COD_ou from CONTATTO_IPA where IDCONTATTO=" + contattoIpa.getContattoID() + ")");
			} else {
				return contatti;
			}

			LOGGER.info("Ricerca contatto rubricaIPA: " + query.toString());

			ps = con.prepareStatement(query.toString());
			rs = ps.executeQuery();
			Contatto contatto = null;
			while (rs.next()) {
				contatto = new Contatto();
				contatto.setTipoRubrica("IPA");
				populateIPA(contatto, rs);
				contatti.add(contatto);
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei contatti dal DB.", e);
			throw new RedException("Errore durante il recupero dei contatti dal DB. ");
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
		return contatti;
	}

	private static void populateIPA(final Contatto contatto, final ResultSet rs) throws SQLException {
		contatto.setContattoID(rs.getLong("idcontatto_ipa"));
		contatto.setCAP(rs.getString("cap_ipa"));
		contatto.setIndirizzo(rs.getString("indirizzo_ipa"));
		if (rs.getString(TIPO_MAIL1_IPA) != null) {
			if ("pec".equalsIgnoreCase(rs.getString(TIPO_MAIL1_IPA))) {
				contatto.setMailPec(rs.getString(MAIL1_IPA));
			} else {
				contatto.setMail(rs.getString(MAIL1_IPA));
			}
		}
		contatto.setAliasContatto(rs.getString("descrizione_ipa"));
		if (rs.getString(DES_AOO_IPA) != null && (rs.getInt(TIPO_CONTATTO_IPA) == 3)) {
			contatto.setRiferimento(" [" + rs.getString(DES_AMM_IPA) + " - " + rs.getString(DES_AOO_IPA) + "]");
		} else {
			contatto.setRiferimento(" [" + rs.getString(DES_AMM_IPA) + "]");
		}
		if (rs.getInt(TIPO_CONTATTO_IPA) == 1) {
			contatto.setTipoPersona("AMM");
		} else if (rs.getInt(TIPO_CONTATTO_IPA) == 2) {
			contatto.setTipoPersona("AOO");
		} else if (rs.getInt(TIPO_CONTATTO_IPA) == 3) {
			contatto.setTipoPersona("OU");
		}

	}

	/**
	 * Aggiungi Contatti a casella postale.
	 * 
	 * @param contatto
	 *            il contatto da aggiungere
	 * @param con
	 * @return id del contatto aggiunto
	 */
	@Override
	public long inserisciContattoCasellaPostale(final Contatto contatto, final Connection con) {
		CallableStatement cs = null;
		long idContatto = 0;

		try {

			cs = con.prepareCall("{call P_INSERTCONTATTOCASELLAPOSTALE(?,?,?,?,?,?,?,?,?)}");
			cs.setString(1, contatto.getNome());
			cs.setString(2, contatto.getCognome());
			cs.setString(3, contatto.getTipoPersona());
			cs.setDate(4, new java.sql.Date(new java.util.Date().getTime()));
			cs.setString(5, contatto.getMail());
			cs.setLong(6, contatto.getIdAOO());
			cs.setString(7, contatto.getAliasContatto());
			cs.setLong(8, contatto.getContattocasellapostale());
			cs.registerOutParameter(9, Types.INTEGER);
			cs.execute();
			idContatto = cs.getInt(9);
			LOGGER.info("contatto " + idContatto + " inserito per la casella di posta");

		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inserimento del contatto casella postale nel DB: ", e);
			throw new RedException(ERROR_INSERIMENTO_CONTATTO_MSG);
		} finally {
			closeStatement(cs);
		}
		return idContatto;
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#updateContattoCasellaPostale(it.ibm.red.business.persistence.model.Contatto,
	 *      java.sql.Connection).
	 */
	@Override
	public long updateContattoCasellaPostale(final Contatto contatto, final Connection con) {
		PreparedStatement ps = null;
		int update = 0;
		try {
			ps = con.prepareStatement(
					"update contatto set nome = ? ,aliascontatto = ?,  cognome = ?, tipopersona = ?, mail = ?,datacreazione = ?,DATA_MODIFICA = ?  where idcontatto = ?");
			ps.setString(1, contatto.getNome());
			ps.setString(2, contatto.getAliasContatto());
			ps.setString(3, contatto.getCognome());
			ps.setString(4, contatto.getTipoPersona());
			ps.setString(5, contatto.getMail());
			ps.setDate(6, new java.sql.Date(new java.util.Date().getTime()));
			ps.setDate(7, new java.sql.Date(new java.util.Date().getTime()));
			ps.setLong(8, contatto.getContattoID());
			update = ps.executeUpdate();
			LOGGER.info("contatto aggiornato per la casella di posta:  " + contatto.getNome());

		} catch (final Exception e) {
			LOGGER.error("Errore durante l'aggiornamento del contatto casella postale nel DB: ", e);
			throw new RedException(ERROR_INSERIMENTO_CONTATTO_MSG);
		} finally {
			closeStatement(ps);
		}
		return update;
	}

	/**
	 * Associa contatto a casella postale.
	 * 
	 * @param idContatto
	 * @param casellaPostale
	 * @param con
	 * @return risultato della query SQL
	 */
	@Override
	public int associaContattoACasellaPostale(final long idContatto, final String casellaPostale, final Connection con) {
		PreparedStatement ps = null;
		int insert = 0;
		try {
			ps = con.prepareStatement("insert into contattocasellapostale (CASELLAPOSTALE, IDCONTATTO) VALUES (?, ?)");
			ps.setString(1, casellaPostale);
			ps.setLong(2, idContatto);
			insert = ps.executeUpdate();
			LOGGER.info("associazione contatto " + idContatto + " / alla casella postale " + casellaPostale + EFFETTUATO_LITERAL);
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'associazione contatto " + idContatto + " / alla casella postale " + casellaPostale + "", e);
			throw new RedException("Errore durante l'associazione contatto " + idContatto + " alla casella postale " + casellaPostale + "");
		} finally {
			closeStatement(ps);
		}
		return insert;
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#getListaGruppoEmailByAlias(java.lang.String,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public List<Contatto> getListaGruppoEmailByAlias(final String casellaPostale, final String nomeGruppo, final Connection con) {
		final List<Contatto> gruppoEmails = new ArrayList<>();
		Contatto gruppoEmail = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			final String query = "select ge.idgruppo, ge.aliasgruppoemail, ge.descrizione from gruppoemailcasellapostale gecp "
					+ " inner join gruppoemail ge on gecp.idgruppo = ge.idgruppo where gecp.indirizzoemail = " + sanitize(casellaPostale)
					+ " and  UPPER(ge.aliasgruppoemail) like " + sanitize("%" + nomeGruppo.toUpperCase() + "%");
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				gruppoEmail = new Contatto();
				gruppoEmail.setTipoRubrica("GRUPPOEMAIL");
				gruppoEmail.setContattoID(rs.getLong("idgruppo"));
				gruppoEmail.setAliasContatto("GRUPPO: " + rs.getString(ALIASGRUPPOEMAIL));
				gruppoEmail.setAliasStringforEmail("GRUPPO: " + rs.getString(ALIASGRUPPOEMAIL));
				gruppoEmail.setIdentificativo("GRUPPOEMAIL:" + gruppoEmail.getContattoID());
				gruppoEmail.setContattiContenutiInGruppo(getListaContattiGruppoEmail(gruppoEmail.getContattoID(), con));
				gruppoEmails.add(gruppoEmail);
			}
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_PROGRESSIVO_MSG, e);
			throw new RedException("errore durante il recupero dei gruppi", e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
		return gruppoEmails;
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#getContattiMailFromAlias(java.lang.String,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public List<Contatto> getContattiMailFromAlias(final String casellaPostale, final String nomeContatto, final Connection con) {
	
		final String strNomeContatto = sanitize("%" + nomeContatto.toUpperCase() + "%");
		final String query = "SELECT c.idcontatto, c.nome, c.cognome, c.tipopersona, c.mail,c.aliascontatto FROM contattocasellapostale cp "
				+ "INNER JOIN contatto c ON c.idcontatto = cp.idcontatto WHERE cp.casellapostale = " + sanitize(casellaPostale)
				+ "  and c.CONTATTOCASELLAPOSTALE = 1 AND (UPPER(c.nome) like " + strNomeContatto + " or  UPPER(c.mail) like " + strNomeContatto
				+ " or UPPER(c.aliascontatto) like " + strNomeContatto + ")";
		
		return executeQueryContattiBy(true, query, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#getContattiByEmail(java.lang.String,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public List<Contatto> getContattiByEmail(final String casellaPostale, final String emailContatto, final Connection con) {
		
		final String query = "SELECT c.idcontatto, c.nome, c.cognome, c.tipopersona, c.mail,c.aliascontatto FROM contattocasellapostale cp "
				+ "INNER JOIN contatto c ON c.idcontatto = cp.idcontatto WHERE cp.casellapostale = " + sanitize(casellaPostale)
				+ " and c.CONTATTOCASELLAPOSTALE = 1 AND c.mail = " + sanitize(emailContatto);
	
		return executeQueryContattiBy(false, query, con);
	}

	/**
	 * @param con
	 * @param query
	 * @return
	 */
	private static List<Contatto> executeQueryContattiBy(boolean byAlias, final String query, final Connection con) {
		final List<Contatto> listaContatti = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {	
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			Contatto contatto = null;
			while (rs.next()) {
				contatto = populateContattoBase(rs);
				// sovrascrivo l'alias con il nuovo valore
				contatto.setAliasContatto(getInfoContattoAsString(contatto));
				contatto.setTipoRubrica("RED");
				if(byAlias) {
					contatto.setIdentificativo("CONTATTO:" + contatto.getContattoID());
				}
				listaContatti.add(contatto);
				LOGGER.info(ID_CONTATTO_LETTO + rs.getLong(IDCONTATTO_COLUMN));
			}
			
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_PROGRESSIVO_MSG, e);
			throw new RedException("errore durante il recupero dei contatti", e);
		} finally {
			closeStatement(ps, rs);
		}

		return listaContatti;
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#getContattoByAlias(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public Contatto getContattoByAlias(final String alias, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Contatto contatto = null;
		try {
			int index = 1;
			ps = con.prepareStatement(" SELECT CONTATTO.idcontatto as idcontatto_red, CONTATTO.cap as cap_red, "
					+ "CONTATTO.indirizzo as indirizzo_red, CONTATTO.mail as mail_red, CONTATTO.mailPec as mailPec_red, CONTATTO.nome as nome_red, "
					+ "CONTATTO.cognome as cognome_red, CONTATTO.tipopersona as tipopersona_red, CONTATTO.cellulare as cellulare_red, CONTATTO.telefono as telefono_red, "
					+ "CONTATTO.pubblico as pubblico_red, CONTATTO.DATAATTIVAZIONE as dataattivazione_red, "
					+ "CONTATTO.DATADISATTIVAZIONE as datadisattivazione_red, CONTATTO.riferimento as riferimento_red, "
					+ "CONTATTO.fax as fax_red, CONTATTO.codicefiscale as codicefiscale_red, "
					+ "CONTATTO.aliascontatto as aliascontatto_red, CONTATTO.idcomuneistat as idcomuneistat_red, CONTATTO.idprovinciaistat as idprovinciaistat_red, CONTATTO.idregioneistat as idregioneistat_red "
					+ "FROM contatto WHERE ONTHEFLY=0 AND aliascontatto = ?");
			ps.setString(index++, alias);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				contatto = popolaContattoRed(rs);
			}
		} catch (final SQLException e) {
			LOGGER.error(ERROR_RECUPERO_CONTATTO_CON_DENOMINAZIONE_MSG + alias, e);
			throw new RedException(ERROR_RECUPERO_CONTATTO_CON_DENOMINAZIONE_MSG + alias + "'");
		} finally {
			closeStatement(ps, rs);
		}
		return contatto;
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#getContattoByDenominazione(java.lang.String,
	 *      java.sql.Connection).
	 */
	@Override
	public Contatto getContattoByDenominazione(final String denominazione, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Contatto contatto = null;
		try {
			int index = 1;
			ps = con.prepareStatement(
					" SELECT CONTATTO.idcontatto as idcontatto_red, CONTATTO.cap as cap_red, CONTATTO.indirizzo as indirizzo_red, CONTATTO.mail as mail_red, "
							+ "CONTATTO.mailPec as mailPec_red, CONTATTO.nome as nome_red, CONTATTO.cognome as cognome_red, CONTATTO.tipopersona as tipopersona_red, "
							+ "CONTATTO.cellulare as cellulare_red, CONTATTO.telefono as telefono_red, CONTATTO.pubblico as pubblico_red, CONTATTO.DATAATTIVAZIONE as dataattivazione_red, "
							+ "CONTATTO.DATADISATTIVAZIONE as datadisattivazione_red, CONTATTO.riferimento as riferimento_red, CONTATTO.fax as fax_red, CONTATTO.codicefiscale as codicefiscale_red, "
							+ "CONTATTO.aliascontatto as aliascontatto_red, CONTATTO.idcomuneistat as idcomuneistat_red, CONTATTO.idprovinciaistat as idprovinciaistat_red, CONTATTO.idregioneistat as idregioneistat_red "
							+ "FROM contatto WHERE ONTHEFLY=0 AND nome = ? AND CONTATTOCASELLAPOSTALE = 0");
			ps.setString(index++, denominazione);
			
			rs = ps.executeQuery();
			if (rs.next()) {
				contatto = popolaContattoRed(rs);
			}
		} catch (final SQLException e) {
			LOGGER.error(ERROR_RECUPERO_CONTATTO_CON_DENOMINAZIONE_MSG + denominazione, e);
			throw new RedException(ERROR_RECUPERO_CONTATTO_CON_DENOMINAZIONE_MSG + denominazione + "'");
		} finally {
			closeStatement(ps, rs);
		}

		return contatto;
	}

	/**
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private static Contatto popolaContattoRed(ResultSet rs) throws SQLException {
		Contatto contatto;
		contatto = new Contatto(rs.getLong("IDCONTATTO_RED"), rs.getString("NOME_RED"), rs.getString("COGNOME_RED"), rs.getString("TIPOPERSONA_RED"),
				rs.getString("CODICEFISCALE_RED"), rs.getString("MAIL_RED"), rs.getString("MAILPEC_RED"), rs.getString("ALIASCONTATTO_RED"));

		contatto.setIdRegioneIstat(rs.getString("IDREGIONEISTAT_RED"));
		contatto.setIdProvinciaIstat(rs.getString("IDPROVINCIAISTAT_RED"));
		contatto.setIdComuneIstat(rs.getString("IDCOMUNEISTAT_RED"));
		contatto.setFax(rs.getString("FAX_RED"));
		contatto.setIndirizzo(rs.getString("INDIRIZZO_RED"));
		contatto.setTelefono(rs.getString("TELEFONO_RED"));
		contatto.setCellulare(rs.getString("CELLULARE_RED"));
		contatto.setPubblico(rs.getInt("PUBBLICO_RED"));
		contatto.setDataattivazione(rs.getDate("DATAATTIVAZIONE_RED"));
		contatto.setDatadisattivazione(rs.getDate("DATADISATTIVAZIONE_RED"));
		contatto.setRiferimento(rs.getString("RIFERIMENTO_RED"));
		contatto.setTipoRubrica("RED");
		return contatto;
	}

	private static String getInfoContattoAsString(final Contatto contatto) {
		String valoreContatto;
		valoreContatto = "";

		if (contatto.getAliasContatto() != null) {
			valoreContatto = contatto.getAliasContatto();
		}
		if (contatto.getCognome() != null) {
			valoreContatto = valoreContatto + " " + contatto.getCognome();
		}
		if (contatto.getNome() != null) {
			valoreContatto = valoreContatto + " " + contatto.getNome();
		}
		valoreContatto = valoreContatto + " [ " + contatto.getMail() + " ]";
		return valoreContatto;
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#inserisciGruppoEmail(it.ibm.red.business.persistence.model.Contatto,
	 *      java.sql.Connection).
	 */
	@Override
	public int inserisciGruppoEmail(final Contatto gruppoEmail, final Connection con) {
		CallableStatement cs = null;
		int insertId = 0;
		try {

			cs = con.prepareCall("{call P_INSERTGRUPPOEMAIL(?,?,?)}");
			cs.setString(1, gruppoEmail.getNome());
			cs.setString(2, gruppoEmail.getAliasContatto());
			cs.registerOutParameter(3, Types.INTEGER);
			cs.execute();
			insertId = cs.getInt(3);
			LOGGER.info("gruppo " + gruppoEmail.getNome() + "inserito");
		} catch (final Exception e) {
			LOGGER.error(ERROR_INSERIMENTO_CONTATTO_MSG, e);
			throw new RedException("Errore durante l'inserimento del gruppo");
		} finally {
			closeStatement(cs);
		}
		return insertId;
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#updateGruppoEmail(it.ibm.red.business.persistence.model.Contatto,
	 *      java.sql.Connection).
	 */
	@Override
	public int updateGruppoEmail(final Contatto gruppoEmail, final Connection con) {
		PreparedStatement ps = null;
		int update = 0;
		try {
			ps = con.prepareStatement("update gruppoemail set aliasgruppoemail = ? ,descrizione = ? where idgruppo = ?");
			ps.setString(1, gruppoEmail.getNome());
			ps.setString(2, gruppoEmail.getAliasContatto());
			ps.setLong(3, gruppoEmail.getContattoID());
			update = ps.executeUpdate();
			LOGGER.info("gruppoEmail aggiornato per la casella di posta:  " + gruppoEmail.getNome());
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'aggiornamento del gruppo sul DB", e);
			throw new RedException("Errore durante la modifica del gruppo");
		} finally {
			closeStatement(ps);
		}
		return update;
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#associaContattoGruppoMail(long,
	 *      long, java.sql.Connection).
	 */
	@Override
	public int associaContattoGruppoMail(final long idGruppoEmail, final long idContatto, final Connection con) {
		PreparedStatement ps = null;
		int insert = 0;
		try {
			ps = con.prepareStatement("insert into gruppoemailcontatto (IDGRUPPO, IDCONTATTO) values (?, ?)");
			ps.setLong(1, idGruppoEmail);
			ps.setLong(2, idContatto);
			insert = ps.executeUpdate();
			LOGGER.info("associazione contatto " + idContatto + GRUPPO_LABEL + idGruppoEmail + EFFETTUATO_LITERAL);
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'aggiornamento del contatto casella postale nel DB: " + e.getMessage(), e);
			throw new RedException("Errore durante l'inserimento dell'associazione del contatto al gruppo");
		} finally {
			closeStatement(ps);
		}
		return insert;
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#associaCasellaPostaleGruppo(long,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public int associaCasellaPostaleGruppo(final long idGruppoEmail, final String indirizzoEmail, final Connection con) {
		PreparedStatement ps = null;
		int insert = 0;
		try {
			ps = con.prepareStatement("insert into gruppoemailcasellapostale (idgruppo,indirizzoemail) values (?,?)");
			ps.setLong(1, idGruppoEmail);
			ps.setString(2, indirizzoEmail);
			insert = ps.executeUpdate();
			LOGGER.info("associazione casella postale " + indirizzoEmail + GRUPPO_LABEL + idGruppoEmail + EFFETTUATO_LITERAL);
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'associazione casella postale " + indirizzoEmail + GRUPPO_LABEL + idGruppoEmail + "", e);
			throw new RedException("Errore durante l'inserimento dell'associazione dell'indirizzo mail al gruppo");
		} finally {
			closeStatement(ps);
		}
		return insert;
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#eliminaAssociazioneContattoGruppo(long,
	 *      long, java.sql.Connection).
	 */
	@Override
	public int eliminaAssociazioneContattoGruppo(final long idGruppoEmail, final long idContatto, final Connection con) {
		PreparedStatement ps = null;
		int result = 0;
		try {
			ps = con.prepareStatement("delete from gruppoemailcontatto where idGruppo = ? and idcontatto = ?");
			ps.setLong(1, idGruppoEmail);
			ps.setLong(2, idContatto);
			result = ps.executeUpdate();
		} catch (final Exception e) {
			LOGGER.error("Errore durante la cancellazione dell'associazione contatto " + idContatto + " con il gruppo " + idGruppoEmail, e);
			throw new RedException("Errore durante la cancellazione dell'associazione del contatto al gruppo");
		} finally {
			closeStatement(ps);

		}
		return result;
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#eliminaAssociazioneCasellaPostaleGruppo(long,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public int eliminaAssociazioneCasellaPostaleGruppo(final long idGruppoEmail, final String indirizzoEmail, final Connection con) {
		PreparedStatement ps = null;
		int result = 0;
		try {
			ps = con.prepareStatement("delete from gruppoemailcasellapostale where idGruppo = ? and indirizzoemail = ?");
			ps.setLong(1, idGruppoEmail);
			ps.setString(2, indirizzoEmail);
			result = ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante la cancellazione dell'associazione casella postale " + indirizzoEmail + " con il gruppo " + idGruppoEmail, e);
			throw new RedException("Errore durante la cancellazione dell'associazione del gruppo alla casella postale");
		} finally {
			closeStatement(ps);
		}
		return result;
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#getListaGruppoEmailByNomeEmail(java.lang.String,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public List<Contatto> getListaGruppoEmailByNomeEmail(final String indirizzoEmail, final String inNomeGruppo, final Connection con) {
		final List<Contatto> gruppoEmails = new ArrayList<>();
		Contatto gruppoEmail = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String nomeGruppo = inNomeGruppo;
		try {
			ps = con.prepareStatement("select ge.idgruppo, ge.aliasgruppoemail, ge.descrizione   from gruppoemailcasellapostale gecp  inner join gruppoemail ge "
					+ "    on gecp.idgruppo = ge.idgruppo  where gecp.indirizzoemail = ?    and lower(ge.aliasgruppoemail) like ?");
			nomeGruppo = "%" + nomeGruppo + "%";
			ps.setString(1, indirizzoEmail);
			ps.setString(2, nomeGruppo);
			rs = ps.executeQuery();
			while (rs.next()) {
				gruppoEmail = new Contatto();
				gruppoEmail.setContattoID(rs.getLong("idgruppo"));
				gruppoEmail.setNome(rs.getString(ALIASGRUPPOEMAIL));
				gruppoEmail.setAliasContatto(rs.getString("descrizione"));
				gruppoEmail.setTipoRubrica("GRUPPOEMAIL");
				gruppoEmail.setTipoRubricaEnum(TipoRubricaEnum.GRUPPOEMAIL);
				gruppoEmails.add(gruppoEmail);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero progressivo.", e);
			throw new RedException(GENERIC_ERROR_RECUPERO_MSG);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
		return gruppoEmails;
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#getListaContattiGruppoEmail(long,
	 *      java.sql.Connection).
	 */
	@Override
	public Collection<Contatto> getListaContattiGruppoEmail(final long idGruppo, final Connection con) {
		final Collection<Contatto> contatti = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.prepareStatement("SELECT c.idcontatto, c.nome, c.cognome, c.tipopersona, c.mail, c.aliascontatto FROM gruppoemailcontatto gec "
					+ " INNER JOIN contatto c on c.idcontatto = gec.idcontatto WHERE gec.idgruppo = ?");
			ps.setLong(1, idGruppo);
			rs = ps.executeQuery();
			Contatto contatto = null;
			while (rs.next()) {
				contatto = populateContattoBase(rs);
				contatto.setAliasStringforEmail(getInfoContattoAsString(contatto));
				contatti.add(contatto);
				LOGGER.info(ID_CONTATTO_LETTO + rs.getInt(IDCONTATTO_COLUMN));
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero progressivo.", e);
			throw new RedException(GENERIC_ERROR_RECUPERO_MSG);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
		return contatti;
	}
	
	/**
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private static Contatto populateContattoBase(ResultSet rs) throws SQLException {
		Contatto contatto = new Contatto();
		contatto.setContattoID(rs.getLong(IDCONTATTO_COLUMN));
		contatto.setNome(rs.getString("nome"));
		contatto.setCognome(rs.getString(COGNOME_COLUMN));
		contatto.setTipoPersona(rs.getString(TIPOPERSONA_COLUMN));
		contatto.setMail(rs.getString("mail"));
		contatto.setAliasContatto(rs.getString(ALIAS_CONTATTO));
		return contatto;
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#getListaCasellaPostaleGruppoEmail(long,
	 *      java.sql.Connection).
	 */
	@Override
	public Collection<String> getListaCasellaPostaleGruppoEmail(final long idGruppo, final Connection con) {
		final Collection<String> listaEmail = new ArrayList<>();
		String email = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.prepareStatement("select indirizzoemail from gruppoemailcasellapostale gecp where gecp.idgruppo = ?");
			ps.setLong(1, idGruppo);
			rs = ps.executeQuery();
			while (rs.next()) {
				email = rs.getString("indirizzoemail");
				LOGGER.info("email letta: " + email);
				listaEmail.add(email);
			}
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_PROGRESSIVO_MSG, e);
			throw new RedException(GENERIC_ERROR_RECUPERO_MSG);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}
		return listaEmail;
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#getGruppoEmailById(long,
	 *      java.sql.Connection).
	 */
	@Override
	public Contatto getGruppoEmailById(final long idGruppo, final Connection con) {
		Contatto gruppoEmail = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.prepareStatement("select descrizione, aliasgruppoemail from gruppoemail where idgruppo = ?");
			ps.setLong(1, idGruppo);
			rs = ps.executeQuery();
			if (rs.next()) {
				gruppoEmail = new Contatto();
				gruppoEmail.setContattoID(idGruppo);
				gruppoEmail.setNome(rs.getString(ALIASGRUPPOEMAIL));
				gruppoEmail.setAliasContatto(rs.getString(ALIASGRUPPOEMAIL));
				gruppoEmail.setTipoRubricaEnum(TipoRubricaEnum.GRUPPOEMAIL);
				gruppoEmail.setContattiContenutiInGruppo(getListaContattiGruppoEmail(gruppoEmail.getContattoID(), con));
				LOGGER.info("gruppoEmail letto: " + idGruppo);
			}
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_PROGRESSIVO_MSG, e);
			throw new RedException(GENERIC_ERROR_RECUPERO_MSG);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}

		return gruppoEmail;
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#eliminaGruppoEmail(long,
	 *      java.sql.Connection).
	 */
	@Override
	public int eliminaGruppoEmail(final long idGruppoEmail, final Connection con) {
		PreparedStatement ps = null;
		int result = 0;
		try {
			ps = con.prepareStatement("delete from gruppoemail where idgruppo = ?");
			ps.setLong(1, idGruppoEmail);
			result = ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante la cancellazione del gruppo " + idGruppoEmail, e);
			throw new RedException(ERROR_CANCELLAZIONE_DA_GRUPPO_MSG);
		} finally {
			closeStatement(ps);

		}
		return result;

	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#eliminaAssociazioneContattoCasellaPostale(long,
	 *      java.sql.Connection).
	 */
	@Override
	public int eliminaAssociazioneContattoCasellaPostale(final long idContatto, final Connection con) {
		
		final String tabella = "contattocasellapostale";
		
		return gestioneEliminaContatto(true, idContatto, tabella, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#eliminaContattoEmail(long,
	 *      java.sql.Connection).
	 */
	@Override
	public int eliminaContattoEmail(final long idContatto, final Connection con) {
		
		final String tabella = "contatto";
		
		return gestioneEliminaContatto(true, idContatto, tabella, con);
	}
	
	/**
	 * Elimina il contatto della tabella e gestisce il messaggio di errore.
	 * @param idContatto
	 * @param tabella
	 * @param con
	 * @return result
	 */
	private static int gestioneEliminaContatto(final boolean isAssociazione, final long idContatto, final String tabella, final Connection con) {
		PreparedStatement ps = null;
		int result = 0;
		try {
			ps = con.prepareStatement("delete from " + tabella + " where idcontatto = ?");
			ps.setLong(1, idContatto);
			result = ps.executeUpdate();
		} catch (final Exception e) {
			String dellAssociazione = " ";
			if(isAssociazione) {
				dellAssociazione = " dell'associazione ";
			}
			LOGGER.error("Errore durante la cancellazione" + dellAssociazione + "del contatto " + idContatto, e);
			throw new RedException("Errore durante la cancellazione" + dellAssociazione + "del contatto alle caselle postali");
		} finally {
			closeStatement(ps);
		}
		return result;
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#eliminaAssociazioneContattiGruppoEmail(long,
	 *      java.sql.Connection).
	 */
	@Override
	public int eliminaAssociazioneContattiGruppoEmail(final long idGruppo, final Connection con) {
		// SQL_DELETE_ASSOCIAZIONE_CONTATTI_GRUPPO
		final String tabella = "gruppoemailcontatto";
		
		return gestisciEliminaContattoGruppo(idGruppo, tabella, con);
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#eliminaAssociazioneCaselleGruppoEmail(long,
	 *      java.sql.Connection).
	 */
	@Override
	public int eliminaAssociazioneCaselleGruppoEmail(final long idGruppo, final Connection con) {
		// SQL_DELETE_ASSOCIAZIONE_CONTATTI_GRUPPO
		final String tabella = "gruppoemailcasellapostale";
		
		return gestisciEliminaContattoGruppo(idGruppo, tabella, con);
	}

	/**
	 * Elimina il gruppo della tabella in input.
	 * @param idGruppo
	 * @param con
	 * @param tabella
	 * @return result
	 */
	private static int gestisciEliminaContattoGruppo(final long idGruppo, final String tabella, final Connection con) {
		PreparedStatement ps = null;
		int result = 0;
		try {
			ps = con.prepareStatement("delete from " + tabella + " where idgruppo = ?");
			ps.setLong(1, idGruppo);
			result = ps.executeUpdate();
		} catch (final Exception e) {
			LOGGER.error("Errore durante la cancellazione del gruppo " + idGruppo, e);
			throw new RedException(ERROR_CANCELLAZIONE_DA_GRUPPO_MSG);
		} finally {
			closeStatement(ps);
		}
		return result;
	}

	/**
	 * Esegue una ricerca di un contatto a partire da mailMittente.
	 * 
	 * @param mailMittente
	 * @param connection
	 * @return Contatto trovato
	 */
	@Override
	public Contatto ricercaRedInteroperabilita(final String mailMittente, final Connection connection) {
		String andConditionMail = "";
		String query = "";
		
		// ricerca red
		if (mailMittente != null && !mailMittente.trim().isEmpty()) {
			final String sanitizeMail = sanitize("%" + mailMittente.toUpperCase() + "%");
			andConditionMail = " AND (UPPER(MAILPEC) LIKE " + sanitizeMail + OR_UPPER_MAIL_LIKE + sanitizeMail + ") ";
		}

		query += "SELECT c.IDCONTATTO, c.NOME, c.COGNOME, c.TIPOPERSONA, c.CODICEFISCALE, c.MAIL, c.MAILPEC, c.ALIASCONTATTO, "
				+ "NULL AS RIFERIMENTO, c.TELEFONO, c.INDIRIZZO, 'RED' AS TIPOLOGIACONTATTO, c.CELLULARE ";

		query += "FROM CONTATTO c FULL OUTER JOIN preferiti p  ON c.idcontatto = p.IdContattoRED and p.tipologiacontatto = 'RED' ";
		query += "WHERE ONTHEFLY=0 AND pubblico=1 AND datadisattivazione is null " + andConditionMail;
		query += "ORDER BY p.IdContattoRED, C.data_modifica desc";
		
		return ricercaInteroperabilita(connection, query);
	}

	/**
	 * Esegue una ricerca di un contatto a partire da mailMittente.
	 * 
	 * @param mailMittente
	 * @param connection
	 * @return Contatto trovato
	 */
	@Override
	public Contatto ricercaIPAInteroperabilita(final String mailMittente, final Connection connection) {
		String andConditionMail = "";
		String query = "";
		
		if (mailMittente != null && !mailMittente.trim().isEmpty()) {
			final String sanitizeMail = sanitize("%" + mailMittente.toUpperCase() + "%");
			andConditionMail = "(UPPER(MAILPEC) LIKE " + sanitizeMail + OR_UPPER_MAIL_LIKE + sanitizeMail + ") ";
		}

		query += "SELECT cipa.IDCONTATTO,cipa.NOME,cipa.COGNOME, cipa.TIPOPERSONA, cipa.CODICEFISCALE, cipa.MAIL, cipa.MAILPEC, "
				+ " CONCAT(CONCAT(cipa.ALIASCONTATTO,' '), cipa.RIFERIMENTO) AS ALIASCONTATTO, cipa.RIFERIMENTO, NULL AS TELEFONO, cipa.INDIRIZZO, "
				+ "'IPA' AS TIPOLOGIACONTATTO, NULL AS CELLULARE ";

		query += "FROM (SELECT * FROM (SELECT IDCONTATTO, NULL AS NOME, NULL AS COGNOME, DECODE(TIPO_CONTATTO, 1 ,'AMM',DECODE(TIPO_CONTATTO, 2 ,'AOO', 'OU'))"
					+ "AS TIPOPERSONA, NULL AS CODICEFISCALE, DECODE(TIPO_MAIL1, 'altro', MAIL1, DECODE(TIPO_MAIL1, 'Altro', MAIL1, NULL)) AS MAIL, "
					+ "DECODE(TIPO_MAIL1, 'pec', MAIL1, DECODE(TIPO_MAIL1, 'Pec', MAIL1, NULL)) AS MAILPEC, DESCRIZIONE AS ALIASCONTATTO, CASE WHEN (DES_AOO IS NOT NULL AND TIPO_CONTATTO = 3 )"
					+ "THEN CONCAT('[',CONCAT(CONCAT(DES_AMM,CONCAT(' - ',DES_AOO)),']'))" + "ELSE CONCAT('[',CONCAT(DES_AMM,']')) END  AS RIFERIMENTO, INDIRIZZO, DATADISATTIVAZIONE "
					+ "FROM contatto_ipa) cipaDecoded WHERE 1=1 AND cipaDecoded.DATADISATTIVAZIONE IS NULL )cipa FULL OUTER JOIN preferiti pcipa "
					+ "ON cipa.idcontatto = pcipa.IdContattoIPA AND pcipa.tipologiacontatto='IPA' ";

			query += "WHERE " + andConditionMail + "ORDER BY pcipa.IdContattoIPA";

		return ricercaInteroperabilita(connection, query);
	}

	/**
	 * Esegue una ricerca di un contatto a partire da mailMittente.
	 * 
	 * @param mailMittente
	 * @param connection
	 * @return Contatto trovato
	 */
	@Override
	public Contatto ricercaMEFInteroperabilita(final String mailMittente, final Connection connection) {
		String andConditionMail = "";
		String query = "";

		if (mailMittente != null && !mailMittente.trim().isEmpty()) {
			final String sanitizeMail = sanitize("%" + mailMittente.toUpperCase() + "%");
			andConditionMail = "(UPPER(MAILPEC) LIKE " + sanitizeMail + OR_UPPER_MAIL_LIKE + sanitizeMail + ") ";
		}

		query += "SELECT cmef.IDCONTATTO ,cmef.NOME , cmef.COGNOME ,cmef.TIPOPERSONA , cmef.CODICEFISCALE, cmef.MAIL, cmef.MAILPEC, "
				+ "cmef.ALIASCONTATTO, NULL AS IDAOO, 'MEF' AS TIPOLOGIACONTATTO, NULL AS INDIRIZZO, NULL AS CAP, TELEFONO, NULL AS FAX,"
				+ "NULL AS IDREGIONEISTAT, NULL AS IDPROVINCIAISTAT, NULL AS IDCOMUNEISTAT, NULL AS DENOMINAZIONEPROVINCIA,NULL AS DENOMINAZIONECOMUNE, "
				+ "NULL AS DENOMINAZIONEREGIONE, NULL AS RIFERIMENTO, NULL AS CELLULARE ";

		query += "FROM(SELECT * FROM(SELECT IDCONTATTO,NOME, COGNOME,DECODE(TIPO_CONTATTO, 4 , 'UFFICIO', 'UTENTE') AS TIPOPERSONA,"
				+ "NULL AS CODICEFISCALE,DECODE(TIPO_MAIL1, 'altro', MAIL1, DECODE(TIPO_MAIL1, 'Altro', MAIL1, NULL)) AS MAIL,DECODE(TIPO_MAIL1, 'pec', MAIL1, DECODE(TIPO_MAIL1, 'Pec', MAIL1, NULL)) AS MAILPEC,"
				+ "TELEFONO, des_ute AS ALIASCONTATTO, DATADISATTIVAZIONE  FROM contatto_mef WHERE 1=1)cmefDecoded WHERE 1=1 AND cmefDecoded.DATADISATTIVAZIONE IS NULL )cmef "
				+ "FULL OUTER JOIN preferiti pmef ON cmef.idcontatto = pmef.IdContattoIPA AND pmef.tipologiacontatto='MEF'";

		query += "WHERE " + andConditionMail + "ORDER BY pmef.IdContattoMef";

		return ricercaInteroperabilita(connection, query);
	}
	
	/**
	 * @param connection
	 * @param query
	 * @param contatto
	 * @return
	 */
	private static Contatto ricercaInteroperabilita(final Connection connection, String query) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Contatto contatto = null;
		try {
			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();

			if (rs.next()) {
				contatto = populateContattoBasic(rs);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore durante la ricerca del contatto" + e);
			throw new RedException("Errore durante la ricerca del contatto");
		} finally {
			closeStatement(ps, rs);
		}
		return contatto;
	}

	/**
	 * Restituisce un contatto generato e popolato a partire dalle informazioni
	 * recuperate dal ResulSet. Vengono impostate solo le informazioni più comuni di
	 * un Contatto.
	 * 
	 * @param rs
	 *            ResultSet dal cui recuperare le informazioni.
	 * @return Contatto generato.
	 * @throws SQLException
	 */
	private static Contatto populateContattoBasic(ResultSet rs) throws SQLException {
		Contatto contatto = new Contatto(rs.getLong(IDCONTATTO), rs.getString("NOME"), rs.getString(COGNOME), rs.getString(TIPOPERSONA), rs.getString(CODICEFISCALE),
				rs.getString("MAIL"), rs.getString(MAILPEC), rs.getString(ALIASCONTATTO));

		contatto.setTipoRubrica(rs.getString(TIPOLOGIACONTATTO));
		contatto.setRiferimento(rs.getString(RIFERIMENTO));
		contatto.setIndirizzo(rs.getString(INDIRIZZO));
		contatto.setTelefono(rs.getString(TELEFONO));
		contatto.setCellulare(rs.getString(CELLULARE));
		contatto.setMailSelected(contatto.getMailPec());
		
		if (StringUtils.isNullOrEmpty(contatto.getMailPec())) {
			contatto.setMailSelected(contatto.getMail());
		}
		return contatto;
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#getContattiByTipoRubrica(java.lang.String,
	 *      java.lang.String, java.lang.String, java.lang.Integer,
	 *      java.lang.Integer, it.ibm.red.business.enums.TipoRubricaEnum, int,
	 *      java.sql.Connection).
	 */
	@Override
	public List<Contatto> getContattiByTipoRubrica(final String descrizione, final String mail, final String tipoPersona, final Integer idAoo, final Integer idContatto,
			final TipoRubricaEnum tipoRubrica, final int limit, final Connection con) {
		final List<Contatto> output = new ArrayList<>();
		StringBuilder querybuilder = new StringBuilder();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			if (TipoRubricaEnum.RED.equals(tipoRubrica)) {
				querybuilder = createQueryRubricaRED(descrizione, mail, tipoPersona, idAoo, idContatto, limit);
			} else if (TipoRubricaEnum.IPA.equals(tipoRubrica)) {
				querybuilder = createQueryRubricaIPA(descrizione, mail, idContatto, limit);
			} else if (TipoRubricaEnum.MEF.equals(tipoRubrica)) {
				querybuilder = createQueryRubricaMEF(descrizione, mail, idContatto, limit);
			}

			ps = con.prepareStatement(querybuilder.toString());
			rs = ps.executeQuery();

			Contatto contatto = null;
			while (rs.next()) {
				contatto = new Contatto();
				contatto.setTipoRubrica(tipoRubrica.getTipoRubrica());
				popolaContatto(contatto, rs);
				output.add(contatto);
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante la ricerca contatti sulla rubrica di tipo: " + tipoRubrica.getTipoRubrica(), e);
			throw new RedException("Errore durante la ricerca contatti sulla rubrica di tipo: " + tipoRubrica.getTipoRubrica(), e);
		} finally {
			closeStatement(ps, rs);
		}

		return output;
	}

	private static StringBuilder createQueryRubricaRED(final String descrizione, final String mail, final String tipoPersona, final Integer idAoo, final Integer idContatto,
			final int limit) {
		final StringBuilder querybuilder = new StringBuilder();

		try {

			querybuilder.append("SELECT CONTATTO.idcontatto as idcontatto_red, CONTATTO.cap as cap_red, CONTATTO.indirizzo as indirizzo_red, CONTATTO.mail as mail_red, "
					+ " CONTATTO.mailPec as mailPec_red, CONTATTO.nome as nome_red, CONTATTO.cognome as cognome_red, CONTATTO.tipopersona as tipopersona_red,"
					+ " CONTATTO.cellulare as cellulare_red, CONTATTO.telefono as telefono_red,"
					+ " CONTATTO.pubblico as pubblico_red, CONTATTO.DATAATTIVAZIONE as dataattivazione_red,"
					+ " CONTATTO.DATADISATTIVAZIONE as datadisattivazione_red, CONTATTO.riferimento as riferimento_red,"
					+ " CONTATTO.fax as fax_red, CONTATTO.codicefiscale as codicefiscale_red,"
					+ " CONTATTO.aliascontatto as aliascontatto_red, CONTATTO.idcomuneistat as idcomuneistat_red,"
					+ " CONTATTO.idprovinciaistat as idprovinciaistat_red, CONTATTO.idregioneistat as idregioneistat_red ");

			querybuilder.append("FROM CONTATTO ");
			querybuilder.append("WHERE CONTATTO.dataDisattivazione is null ");
			querybuilder.append("AND CONTATTO.pubblico = 1 ");
			querybuilder.append("AND CONTATTO.ONTHEFLY = 0 ");

//			<-- Gestione aliascontatto -->
			if (descrizione != null && !descrizione.isEmpty()) {
				final String descClear = StringUtils.deleteSpecialCharacterForSpace(descrizione);
				if (!descClear.isEmpty()) {
					final StringTokenizer descTokenizer = new StringTokenizer(descClear);
					while (descTokenizer.hasMoreTokens()) {
						querybuilder.append(" AND UPPER(aliascontatto) like UPPER('%" + descTokenizer.nextToken() + "%') ");
					}
				}
			}
//			<-- Gestione mail or mailpec -->
			if (mail != null && !mail.isEmpty()) {
				final String mailClear = StringUtils.deleteSpecialCharacterForSpace(mail);
				if (!mailClear.isEmpty()) {
					final StringTokenizer mailTokenizer = new StringTokenizer(mailClear);
					while (mailTokenizer.hasMoreTokens()) {
						final String token = mailTokenizer.nextToken();
						querybuilder.append(" AND (UPPER(mail) like UPPER('%" + token + "%') or UPPER(mailpec) like UPPER('%" + token + "%'))");
					}
				}
			}
//			<-- Gestione tipopersona -->
			if (!tipoPersona.isEmpty()) {
				querybuilder.append(" AND UPPER(tipopersona) like UPPER('" + tipoPersona + "') ");
			}
//			<-- Gestione idAoo -->			
			if (idAoo != null && idAoo > 0) {
				querybuilder.append(" AND CONTATTO.idaoo = " + idAoo);
			}
//			<-- Gestione idAoo -->			
			if (idContatto != null && idContatto > 0) {
				querybuilder.append(" AND CONTATTO.idaoo = " + idContatto);
			}
			querybuilder.append(" ORDER BY CONTATTO.aliascontatto");

			querybuilder.append(gestioneLimiteRisultati(limit));

			LOGGER.info("Ricerca contatti rubricaRED: " + querybuilder.toString());

		} catch (final Exception e) {
			LOGGER.error("Errore durante la costruzione della query per la ricerca sulla rubricaRED. ", e);
			throw new RedException("Errore durante la costruzione della query per la ricerca sulla rubricaRED. ", e);
		}

		return querybuilder;
	}

	private static StringBuilder createQueryRubricaIPA(final String descrizione, final String mail, final Integer idContatto, final int limit) {
		final StringBuilder querybuilder = new StringBuilder();

		try {
			querybuilder.append("SELECT CONTATTO_IPA.IDCONTATTO as idcontatto_ipa,  CONTATTO_IPA.cap as cap_ipa,   CONTATTO_IPA.indirizzo as indirizzo_ipa, "
					+ " CONTATTO_IPA.tipo_contatto as tipo_contatto_ipa,  CONTATTO_IPA.mail1 as mail1_ipa,  CONTATTO_IPA.tipo_mail1 as tipo_mail1_ipa, "
					+ " CONTATTO_IPA.descrizione as descrizione_ipa,  CONTATTO_IPA.regione as regione_ipa,  CONTATTO_IPA.provincia as provincia_ipa, "
					+ " CONTATTO_IPA.comune as comune_ipa,  CONTATTO_IPA.DES_AOO as des_aoo_ipa,  CONTATTO_IPA.DES_AMM as des_amm_ipa ");

			querybuilder.append(handleColumnsByTipoRubrica(descrizione, mail, idContatto, limit, TipoRubricaEnum.IPA.getTipoRubrica()));

		} catch (final Exception e) {
			LOGGER.error("Errore durante la costruzione della query per la ricerca sulla rubricaIPA. ", e);
			throw new RedException("Errore durante la costruzione della query per la ricerca sulla rubricaIPA. ", e);
		}
		return querybuilder;
	}

	private static StringBuilder createQueryRubricaMEF(final String descrizione, final String mail, final Integer idContatto, final int limit) {
		final StringBuilder querybuilder = new StringBuilder();

		try {
			querybuilder.append("SELECT CONTATTO_MEF.IDCONTATTO as idcontatto_mef,  CONTATTO_MEF.tipo_contatto as tipo_contatto_mef, "
					+ " CONTATTO_MEF.mail1 as mail1_mef,  CONTATTO_MEF.tipo_mail1 as tipo_mail1_mef,  CONTATTO_MEF.descrizione as descrizione_mef, "
					+ " CONTATTO_MEF.livello1 as livello1_mef,  CONTATTO_MEF.livello2 as livello2_mef,  CONTATTO_MEF.livello3 as livello3_mef,"
					+ " CONTATTO_MEF.nome as nome_mef,CONTATTO_MEF.cognome as cognome_mef, CONTATTO_MEF.telefono as telefono_mef ");

			querybuilder.append(handleColumnsByTipoRubrica(descrizione, mail, idContatto, limit, TipoRubricaEnum.MEF.getTipoRubrica()));

		} catch (final Exception e) {
			LOGGER.error("Errore durante la costruzione della query per la ricerca sulla rubricaMEF. ", e);
			throw new RedException("Errore durante la costruzione della query per la ricerca sulla rubricaMEF. ", e);
		}
		return querybuilder;
	}
	
	private static String handleColumnsByTipoRubrica(final String descrizione, final String mail, final Integer idContatto, final int limit, String tipoRubrica) {
		final StringBuilder querybuilder = new StringBuilder(" FROM CONTATTO_" + tipoRubrica + " WHERE CONTATTO_" + tipoRubrica + ".IDCONTATTO is not null AND contatto_" + tipoRubrica + ".DATADISATTIVAZIONE IS NULL ");

//		<-- Gestione DESCRIZIONE -->
		if (descrizione != null && !descrizione.isEmpty()) {
			final String descClear = StringUtils.deleteSpecialCharacterForSpace(descrizione);
			if (!descClear.isEmpty()) {
				final StringTokenizer descTokenizer = new StringTokenizer(descClear);
				while (descTokenizer.hasMoreTokens()) {
					querybuilder.append(" AND UPPER (CONTATTO_" + tipoRubrica + ".DESCRIZIONE) like UPPER('%" + descTokenizer.nextToken() + "%') ");
				}
			}
		}
//		<-- Gestione mail or mailpec -->
		if (mail != null && !mail.isEmpty()) {
			final String mailClear = StringUtils.deleteSpecialCharacterForSpace(mail);
			if (!mailClear.isEmpty()) {
				final StringTokenizer mailTokenizer = new StringTokenizer(mailClear);
				while (mailTokenizer.hasMoreTokens()) {
					querybuilder.append(" AND UPPER (CONTATTO_" + tipoRubrica + ".MAIL1) like UPPER('%" + mailTokenizer.nextToken() + "%') ");
				}
			}
		}
		
		if (idContatto != null && idContatto > 0) {
			querybuilder.append(" AND CONTATTO_" + tipoRubrica + ".IDCONTATTO = " + idContatto);
		}
		querybuilder.append(" ORDER BY CONTATTO_" + tipoRubrica + ".DESCRIZIONE ");

		querybuilder.append(gestioneLimiteRisultati(limit));

		String query = querybuilder.toString();
		LOGGER.info("Ricerca contatti rubrica" + tipoRubrica + ": " + query);
		return query;
	}

	/**
	 * @param limit
	 * @param querybuilder
	 */
	private static String gestioneLimiteRisultati(final int limit) {
		final StringBuilder querybuilder = new StringBuilder();
		
		if (limit > 0) {
			querybuilder.insert(0, SELECT_FROM);
			querybuilder.append(WHERE_ROWNUM).append(limit);
		}
		return querybuilder.toString();
	}

	private static void popolaContatto(final Contatto contatto, final ResultSet rs) {

		try {

			if (TipoRubricaEnum.RED.equals(contatto.getTipoRubricaEnum())) {

				contatto.setContattoID(rs.getLong("idcontatto_red"));
				contatto.setCAP(rs.getString("cap_red"));
				contatto.setIndirizzo(StringUtils.unescapeString(rs.getString("indirizzo_red")));
				contatto.setMail(StringUtils.unescapeString(rs.getString("mail_red")));
				contatto.setMailPec(StringUtils.unescapeString(rs.getString("aliascontatto_red")));
				contatto.setAliasContatto(StringUtils.unescapeString(rs.getString("mailPec_red")));
				contatto.setNome(StringUtils.unescapeString(rs.getString("nome_red")));
				contatto.setCognome(StringUtils.unescapeString(rs.getString("cognome_red")));
				contatto.setTipoPersona(StringUtils.unescapeString(rs.getString("tipopersona_red")));
				contatto.setCellulare(rs.getString("cellulare_red"));
				contatto.setTelefono(rs.getString("telefono_red"));
				contatto.setPubblico(rs.getInt("pubblico_red"));
				contatto.setDataattivazione(rs.getTimestamp("dataattivazione_red"));
				contatto.setDatadisattivazione(rs.getTimestamp("datadisattivazione_red"));
				contatto.setRiferimento(StringUtils.unescapeString(rs.getString("riferimento_red")));
				contatto.setFax(StringUtils.unescapeString(rs.getString("fax_red")));
				contatto.setCodiceFiscalePIva(StringUtils.unescapeString(rs.getString("codicefiscale_red")));
				contatto.setIdComuneIstat(rs.getString("idcomuneistat_red"));
				contatto.setIdRegioneIstat(rs.getString("idregioneistat_red"));
				contatto.setIdProvinciaIstat(rs.getString("idprovinciaistat_red"));

			} else if (TipoRubricaEnum.IPA.equals(contatto.getTipoRubricaEnum())) {

				contatto.setContattoID(rs.getLong("idcontatto_ipa"));
				contatto.setCAP(rs.getString("cap_ipa"));
				contatto.setIndirizzo(StringUtils.unescapeString(rs.getString("indirizzo_ipa")));
				contatto.setRegione(StringUtils.unescapeString(rs.getString("regione_ipa")));
				contatto.setProvincia(StringUtils.unescapeString(rs.getString("provincia_ipa")));
				contatto.setComune(StringUtils.unescapeString(rs.getString("comune_ipa")));

				final String tipoMail = rs.getString(TIPO_MAIL1_IPA);
				if (tipoMail != null) {
					if ("pec".equalsIgnoreCase(tipoMail)) {
						contatto.setMailPec(StringUtils.unescapeString(rs.getString(MAIL1_IPA)));
					} else {
						contatto.setMail(StringUtils.unescapeString(rs.getString(MAIL1_IPA)));
					}
				}

				final String desAooIpa = StringUtils.unescapeString(rs.getString(DES_AOO_IPA));
				final String desAmmIpa = StringUtils.unescapeString(rs.getString(DES_AMM_IPA));
				final String aliasIpa = StringUtils.unescapeString(rs.getString("descrizione_ipa"));
				final int tipoContatto = rs.getInt(TIPO_CONTATTO_IPA);
				if (desAooIpa != null && (tipoContatto == Constants.Contatto.CONTATTO_OU)) {
					contatto.setRiferimento(" [" + desAmmIpa + " - " + desAooIpa + "]");
					contatto.setAliasContatto(aliasIpa + contatto.getRiferimento());
				} else {
					contatto.setRiferimento(" [" + desAmmIpa + "]");
					contatto.setAliasContatto(aliasIpa + contatto.getRiferimento());
				}

				if (tipoContatto == Constants.Contatto.CONTATTO_AMM) {
					contatto.setTipoPersona(Constants.Contatto.CONTATTO_AMM_STRING);
				} else if (tipoContatto == Constants.Contatto.CONTATTO_AOO) {
					contatto.setTipoPersona(Constants.Contatto.CONTATTO_AOO_STRING);
				} else if (tipoContatto == Constants.Contatto.CONTATTO_OU) {
					contatto.setTipoPersona(Constants.Contatto.CONTATTO_OU_STRING);
				}

			} else if (TipoRubricaEnum.MEF.equals(contatto.getTipoRubricaEnum())) {

				contatto.setContattoID(rs.getLong("idcontatto_mef"));
				contatto.setNome(StringUtils.unescapeString(rs.getString("nome_mef")));
				contatto.setCognome(StringUtils.unescapeString(rs.getString("cognome_mef")));
				contatto.setAliasContatto(StringUtils.unescapeString(rs.getString("descrizione_mef")));
				contatto.setTelefono(rs.getString("telefono_mef"));

				final String tipoMail = rs.getString("tipo_mail1_mef");
				if (tipoMail != null) {
					if ("pec".equalsIgnoreCase(tipoMail)) {
						contatto.setMailPec(StringUtils.unescapeString(rs.getString("mail1_mef")));
					} else {
						contatto.setMail(StringUtils.unescapeString(rs.getString("mail1_mef")));
					}
				}

				final int tipoContatto = rs.getInt("tipo_contatto_mef");
				if (tipoContatto == Constants.Contatto.CONTATTO_UFFICIO_MEF) {
					contatto.setTipoPersona(Constants.Contatto.CONTATTO_UFFICIO_MEF_STRING);
					contatto.setRiferimento("");
				} else if (tipoContatto == Constants.Contatto.CONTATTO_UTENTE_MEF) {
					contatto.setTipoPersona(Constants.Contatto.CONTATTO_UTENTE_MEF_STRING);
					contatto.setRiferimento(rs.getString("livello3_mef"));
				}

			} else if (TipoRubricaEnum.GRUPPO.equals(contatto.getTipoRubricaEnum())) {

				contatto.setContattoID(rs.getLong("idgruppo_grup"));
				contatto.setAliasContatto(StringUtils.unescapeString(rs.getString("descrizione_grup")));
				contatto.setNome(StringUtils.unescapeString(rs.getString("nomegruppo_grup")));
				contatto.setTipoPersona(TipoRubricaEnum.GRUPPO.getTipoRubrica());
				contatto.setRiferimento("");

			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante la gestione del ResultSet. ", e);
			throw new RedException("Errore durante la gestione del ResultSet. ", e);
		}

	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#insertContatto(it.ibm.red.business.persistence.model.Contatto,
	 *      java.sql.Connection).
	 */
	@Override
	public Long insertContatto(final Contatto contatto, final Connection con) {
		CallableStatement cs = null;
		try {
			int index = 1;
			cs = con.prepareCall("{call P_INSERTCONTATTO_NEW(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");

			setStringNullable(cs, index++, contatto.getNome(), false);
			setStringNullable(cs, index++, contatto.getCognome(), false);
			cs.setInt(index++, contatto.getPubblico());
			setStringNullable(cs, index++, contatto.getTipoPersona(), false);
			cs.setDate(index++, new java.sql.Date(contatto.getDatacreazione().getTime()));
			setStringNullable(cs, index++, contatto.getRiferimento(), true);
			setStringNullable(cs, index++, contatto.getIndirizzo(), true);

			setStringNullable(cs, index++, contatto.getCAP(), true);
			setStringNullable(cs, index++, contatto.getTelefono(), true);
			setStringNullable(cs, index++, contatto.getCellulare(), true);
			setStringNullable(cs, index++, contatto.getMail(), true);
			setStringNullable(cs, index++, contatto.getFax(), true);

			setStringNullable(cs, index++, contatto.getIdComuneIstat(), false);
			setIntNullable(cs, index++, contatto.getIPA());
			setIntNullable(cs, index++, contatto.getIdAOO().intValue());
			setStringNullable(cs, index++, contatto.getMailPec(), true);
			setStringNullable(cs, index++, contatto.getCodiceFiscalePIva(), true);
			setStringNullable(cs, index++, contatto.getAliasContatto(), false);
			setStringNullable(cs, index++, contatto.getIdProvinciaIstat(), false);
			setStringNullable(cs, index++, contatto.getIdRegioneIstat(), false);
			setIntNullable(cs, index++, contatto.getOnTheFly());
			if (contatto.getDatadisattivazione() != null) {
				cs.setDate(index++, new java.sql.Date(contatto.getDatadisattivazione().getTime()));
			} else {
				cs.setDate(index++, null);
			}
			setStringNullable(cs, index++, contatto.getTitolo(), true);
			cs.registerOutParameter(index, Types.INTEGER);

			cs.execute();
			return cs.getLong(index);
		} catch (final SQLException e) {
			LOGGER.error("Errore in fase di inserimento del contatto sulla base dati ", e);
			throw new RedException("Errore in fase di inserimento del contatto", e);
		} finally {
			closeStatement(cs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#removeContattoRichiestaCreazione(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public void removeContattoRichiestaCreazione(final Long idContatto, final Connection conn) {
		PreparedStatement ps = null;
		try {
			final int index = 1;
			ps = conn.prepareStatement("DELETE FROM CONTATTO WHERE idcontatto = ?");
			ps.setLong(index, idContatto);

			ps.executeUpdate();
		} catch (final SQLException e) {
			LOGGER.error("Errore durante l'eliminazione del record dalla tabella contatto " + e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#contattoGiaInGruppi(java.lang.Long,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public Boolean contattoGiaInGruppi(final Long idgruppoToCheck, final Long idContattoToCheck, final Connection con) {

		String query = "SELECT * FROM GRUPPOCONTATTO WHERE IDGRUPPO = " + idgruppoToCheck + " AND ( IDCONTATTORED = " + sanitize(idContattoToCheck)
					+ " OR IDCONTATTOMEF = " + sanitize(idContattoToCheck) + " OR IDCONTATTOIPA = " + sanitize(idContattoToCheck) + ") AND ISUTENTECANCELLATO = 0";
		
		return executeQueryContattoEsistente(con, query);
	}

	/**
	 * @see it.ibm.red.business.dao.IContattoDAO#eliminaContattiOnTheFlyRangeTemporale(int,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public void eliminaContattiOnTheFlyRangeTemporale(final int numGiorniDelete, final Long idAoo, final Connection conn) {
		PreparedStatement ps = null;
		try {
			int index = 1;
			eliminaContattiOnTheFlyRangeTemporaleDaGruppi(numGiorniDelete, idAoo, conn);
			eliminaContattiOnTheFlyRangeTemporaleDaPreferiti(numGiorniDelete, idAoo, conn);

			final StringBuilder sb = new StringBuilder();
			sb.append("DELETE FROM CONTATTO ");
			sb.append("WHERE ((DATADISATTIVAZIONE + ? < SYSDATE) OR (ONTHEFLY = 1 AND DATACREAZIONE + ? < SYSDATE)) AND NON_ELIMINABILE <> 1 and IDAOO = ?");
			ps = conn.prepareStatement(sb.toString());
			ps.setInt(index++, numGiorniDelete);
			ps.setInt(index++, numGiorniDelete);
			ps.setInt(index++, idAoo.intValue());
			ps.executeUpdate();

		} catch (final SQLException ex) {
			LOGGER.error(ERROR_RECUPERO_CONTATTO_DA_GRUPPO_MSG, ex);
			throw new RedException(ERROR_RECUPERO_CONTATTO_DA_GRUPPO_MSG);
		} finally {
			closeStatement(ps);
		}

	}

	private static void eliminaContattiOnTheFlyRangeTemporaleDaGruppi(final int numGiorniDelete, final Long idAoo, final Connection conn) {
		eliminaContattiOnTheFlyByRangeTemp("GRUPPOCONTATTO", numGiorniDelete, idAoo, conn);
	}

	private static void eliminaContattiOnTheFlyRangeTemporaleDaPreferiti(final int numGiorniDelete, final Long idAoo, final Connection conn) {
		eliminaContattiOnTheFlyByRangeTemp("PREFERITI", numGiorniDelete, idAoo, conn);
	}

	private static void eliminaContattiOnTheFlyByRangeTemp(final String tabella, final int numGiorniDelete, final Long idAoo,
			final Connection conn) {
		PreparedStatement ps = null;
		try {
			int index = 1;
			final StringBuilder sb = new StringBuilder();
			sb.append("DELETE FROM "+ tabella +" WHERE IDCONTATTORED IN(");
			sb.append("SELECT IDCONTATTO FROM CONTATTO ");
			sb.append("where ((DATADISATTIVAZIONE + ? > SYSDATE) OR (ONTHEFLY = 1 AND DATACREAZIONE + ? > SYSDATE)) ");
			sb.append("and IDAOO = ? AND NON_ELIMINABILE <> 1) ");

			ps = conn.prepareStatement(sb.toString());
			ps.setInt(index++, numGiorniDelete);
			ps.setInt(index++, numGiorniDelete);
			ps.setInt(index++, idAoo.intValue());
			ps.executeUpdate();
		} catch (final Exception ex) {
			LOGGER.error(ERROR_ELIMINAZIONE_ONTHEFLY_MSG, ex);
			throw new RedException(ERROR_ELIMINAZIONE_ONTHEFLY_MSG);
		} finally {
			closeStatement(ps);
		}
	}
}
