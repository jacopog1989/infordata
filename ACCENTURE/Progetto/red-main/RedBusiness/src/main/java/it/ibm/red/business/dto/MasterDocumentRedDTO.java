package it.ibm.red.business.dto;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.IconaFlussoEnum;
import it.ibm.red.business.enums.IconaStatoEnum;
import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;
import it.ibm.red.business.enums.StatoDocumentoFuoriLibroFirmaEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoOperazioneLibroFirmaEnum;
import it.ibm.red.business.enums.TipoSpedizioneDocumentoEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.DateUtils;

/**
 * DTO che definisce le caratteristiche di un master di un Documento RED.
 */
public class MasterDocumentRedDTO extends AbstractDTO implements Comparable<MasterDocumentRedDTO> {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 4729912919798479406L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(MasterDocumentRedDTO.class.getName());
	
	/**
	 * Flasg disabilitato.
	 */
	private Boolean flagDisabilitato;

	/**
	 * Guid.
	 */
	private String guuid;

	/**
	 * Classe documentale.
	 */
	private String classeDocumentale;

	/**
	 * id auto generato all'occorrenza per rendere univoco un elemento in tabella.
	 */
	private String idTable;

	/**
	 * Flag urgente.
	 */
	private Boolean urgente;

	/**
	 * Icona che rappresenta lo stato del documento.
	 */
	private IconaStatoEnum iconaStato;

	/**
	 * Anno protocollo.
	 */
	private Integer annoProtocollo;

	/**
	 * Flag allegati presenti.
	 */
	private Boolean bAttachmentPresent;

	/**
	 * Flag con contributi.
	 */
	private Boolean hasContributi;

	/**
	 * Flag trasformazione in errore.
	 */
	private Boolean bTrasfPdfErrore;

	/**
	 * Flag trasformazione pdf in warning (errore che permette un nuovo tentativo
	 * senza passare dalla GA).
	 */
	private Boolean bTrasfPdfWarning;

	/**
	 * Document title.
	 */
	private String documentTitle;

	/**
	 * Flag abilita firma.
	 */
	private Boolean flagEnableFirma;

	/**
	 * Flag abilita firma autografa.
	 */
	private Boolean flagEnableFirmaAutografa;

	/**
	 * Flag abilita rifiuto.
	 */
	private Boolean flagEnableRifiuta;

	/**
	 * Flag abilita sigla.
	 */
	private Boolean flagEnableSigla;

	/**
	 * Flag abilita visto.
	 */
	private Boolean flagEnableVista;

	/**
	 * Flag Procedi da Corriere.
	 */
	private Boolean flagEnableProcediCorriere;

	/**
	 * Flag firma digitale.
	 */
	private Boolean flagFirmaDig;

	/**
	 * Flag per evidenziare la presenza di documenti siebel.
	 */
	private boolean flagSiebel;

	/**
	 * Identificativo utente destinatario.
	 */
	private Integer idUtenteDestinatario;

	/**
	 * Identificativo ufficio destinatario.
	 */
	private Integer idUfficioDestinatario;

	/**
	 * Motivazione assegnazione.
	 */
	private String motivazioneAssegnazione;

	/**
	 * Numero documento.
	 */
	private Integer numeroDocumento;

	/**
	 * Numero protocollo.
	 */
	private Integer numeroProtocollo;

	/**
	 * Oggetto.
	 */
	private String oggetto;

	/**
	 * Operazione.
	 */
	private TipoOperazioneLibroFirmaEnum operazione;

	/**
	 * Coda.
	 */
	private DocumentQueueEnum queue;

	/**
	 * Tipologia documento.
	 */
	private String tipologiaDocumento;

	/**
	 * Wob number.
	 */
	private String wobNumber;

	/**
	 * Flag firma autografa Red.
	 */
	private Boolean flagFirmaAutografaRM;

	/**
	 * Data protocollazione.
	 */
	private Date dataProtocollazione;

	/**
	 * Data creazione.
	 */
	private Date dataCreazione;

	/**
	 * Data scadenza.
	 */
	private Date dataScadenza;

	/**
	 * Urgenza.
	 */
	private Integer urgenza;

	/**
	 * Stato.
	 */
	private StatoDocumentoFuoriLibroFirmaEnum stato;

	/**
	 * Tipo spedizione.
	 */
	private TipoSpedizioneDocumentoEnum tipoSpedizione;

	/**
	 * Tipo Procedimento.
	 */
	private String tipoProcedimento;

	/**
	 * Identificativo categoria documento.
	 */
	private Integer idCategoriaDocumento;

	/**
	 * Identificativo formato documento.
	 */
	private Integer idFormatoDocumento;

	/**
	 * Flag renderizzato.
	 */
	private Boolean flagRenderizzato;

	/**
	 * Identificativo momento protocollazione.
	 */
	private Integer idMomentoProtocollazione;

	/**
	 * Tipologia protocollo.
	 */
	private Integer tipoProtocollo;

	/**
	 * Flag riservato.
	 */
	private Boolean flagRiservato;

	/**
	 * Numero contributi.
	 */
	private Long numContributi;

	/**
	 * Flag firma copia conforme.
	 */
	private Boolean firmaCopiaConforme;

	/**
	 * Tipologia assegnazione.
	 */
	private TipoAssegnazioneEnum tipoAssegnazione;

	/**
	 * Flag firma PDF.
	 */
	private Boolean flagFirmaPDF;

	/**
	 * 
	 */
	private Integer annoDocumento;

	/**
	 * 
	 */
	private ResponsesDTO responses;

	/**
	 * 
	 */
	private String[] responsesRaw;

	/**
	 * 
	 */
	private String stepName;

	/**
	 * 
	 */
	private NotaDTO nota;

	/**
	 * 
	 */
	private String errorMessage;

	/**
	 * 
	 */

	private String warningMessage;

	/**
	 * 
	 */
	private String nomeFile;

	/**
	 * 
	 */
	private Integer contributiRichiesti;

	/**
	 * 
	 */
	private Integer contributiPervenuti;

	/**
	 * 
	 */
	private String mittente;

	/**
	 * Alias dei destinatari concatenati e separati da ", ".
	 */
	private String destinatari;
	
	/**
	 * Destinatari non lavorati.
	 */
	private List<String[]> destinatariRaw;

	/**
	 * 
	 */
	private String assegnatario;

	/**
	 * Data assegnazione.
	 */
	private Date dataAss;

	/**
	 * Identificativo ufficio creatore.
	 */
	private Integer idUfficioCreatore;

	/**
	 * Serve per gestire la ricerca avanzata full-text, che prevede la presenza in
	 * un unico DTO di dati relativi al documento principale e all'allegato.
	 */
	private String allegatoDocumentTitle;

	/**
	 * Flag che indica se il documento non ha content (e.g. documento cartaceo da
	 * riconciliare)
	 */
	private boolean flagNoContent;

	/**
	 * Informazioni sull'item di firma asincrona. Le informazioni sono associate solo al master.
	 */
	private ASignMasterDTO info;
	
	/**
	 * Dati protocollo Emergenza.
	 */
	private Integer numeroProtEmergenza;

	/**
	 * Anno protocollo emergenza.
	 */
	private Integer annoProtEmergenza;

	/**
	 * Data protocollo emergenza.
	 */
	private Date dataProtEmergenza;

	/**
	 * Oggetto protocollo NPS.
	 */
	private String oggettoProtocolloNPS;

	/**
	 * Serve per aggiornare il titolario su NPS.
	 */
	private String idProtocollo;

	/**
	 * Flag modalità registrazione repertorio.
	 */
	private Boolean isModalitaRegistroRepertorio;

	/**
	 * Descrizione registro repertorio.
	 */
	private String descrizioneRegistroRepertorio;

	/**
	 * Codice del flusso (per i documenti generati da flussi).
	 */
	private String codiceFlusso;

	/**
	 * Icona del flusso (per i documenti generati da flussi).
	 */
	private IconaFlussoEnum iconaFlusso;

	/**
	 * Flag stampa etichette.
	 */
	private boolean stampaEtichette;

	/**
	 * Primo livello.
	 */
	private String primoLivello;

	/**
	 * Secondo livello.
	 */
	private String secondoLivello;

	/**
	 * Tipo assegnazione descrizione.
	 */
	private String tipoAssegnazioneDescr;

	/**
	 * Numero protocollo identificativo protocollo.
	 */
	private String numProtIdProtAnnoExcel;

	/**
	 * Flag selezione.
	 */
	private boolean flagSelezione;

	/**
	 * Placeholder sigla.
	 */
	private String placeholderSigla;

	/**
	 * Tipologia firma.
	 */
	private int tipoFirma;

	/**
	 * Icona stampigliatura segno grafico.
	 */
	private Map<String, String> iconaStampigliaturaSegnoGrafico;

	/**
	 * Set icone.
	 */
	private Set<String> iconaKeySet;
	/**
	 * Rappresentazione dei Metadati estesi per la GUI dei risultati ricerca.
	 */
	private Map<String, String> metadatiEstesi;

	/**
	 * Id tipologia documento.
	 */
	private Integer idTipologiaDocumento;

	/**
	 * Id tipologia procedimento.
	 */
	private Integer idTipoProcedimento;

	/**
	 * Sotto categoria uscita documento.
	 */
	private SottoCategoriaDocumentoUscitaEnum sottoCategoriaDocumentoUscita;

	/**
	 * Flag annulla template.
	 */
	private boolean annullaTemplate;

	/**
	 * Protocollo riferimento.
	 */
	private String protocolloRiferimento;

	/**
	 * Flag integrazione dati.
	 */
	private boolean integrazioneDati;

	/**
	 * Flag firma valida principale.
	 */
	private Boolean firmaValidaPrincipale;

	/**
	 * Flag firma valida allegati.
	 */
	private Boolean firmaValidaAllegati;

	/**
	 * Flag documento lockato.
	 */
	private boolean docLocked;

	/**
	 * Username utente che ha lockato.
	 */
	private String usernameLocked;

	/**
	 * Decreto liquidazione.
	 */
	private String decretoLiquidazione;

	
	/**
	 * Validità firma.
	 */
	private Integer metadatoValiditaFirmaPrincipale;
	
	/**
	 * Versione da verificare.
	 */
	private Integer version;
	
	/**
	 * Ufficio Creatore.
	 */
	private Integer uffCreatore;
	
	/**
	 * Descrizione Ufficio Creatore.
	 */
	private String descUffCreatore;
	
	
	/**
	 * Data Creazione Workflof.
	 */
	private Date dataCreazioneWF;
	
	/**
	 * Costruttore.
	 */
	public MasterDocumentRedDTO() {
		idCategoriaDocumento = null;
		tipoSpedizione = null;
		dataCreazione = null;
		idFormatoDocumento = null;
		numeroDocumento = null;
		documentTitle = null;
		tipologiaDocumento = null;
		oggetto = null;
		numeroProtocollo = null;
		operazione = null;
		wobNumber = null;
		annoProtocollo = null;
		bTrasfPdfErrore = null;
		bTrasfPdfWarning = null;
		bAttachmentPresent = null;
		flagEnableFirma = false;
		flagEnableFirmaAutografa = false;
		flagEnableRifiuta = false;
		flagEnableVista = false;
		flagEnableSigla = false;
		flagEnableProcediCorriere = false;
		dataScadenza = null;
		urgenza = null;
		flagFirmaAutografaRM = null;
		iconaStato = null;
		idMomentoProtocollazione = null;
		tipoProtocollo = null;
		flagRiservato = null;
		numContributi = null;
		hasContributi = null;
		dataProtocollazione = null;
		flagSiebel = false;
		flagNoContent = false;
		flagDisabilitato = false;
		setFlagSelezione(true);
		sottoCategoriaDocumentoUscita = null;
		setAnnullaTemplate(false);
		protocolloRiferimento = null;
		integrazioneDati = false;
		decretoLiquidazione = null;
		this.firmaValidaPrincipale = false;
		this.firmaValidaAllegati =  false;
		this.metadatoValiditaFirmaPrincipale = null;
	}

	/**
	 * Costruttore.
	 * 
	 * @param inDocumentTitle
	 * @param inNumeroDocumento
	 * @param inOggetto
	 * @param inTipologiaDocumento
	 * @param inNumeroProtocollo
	 * @param inAnnoProtocollo
	 * @param inAttachmentPresent
	 * @param inTrasfPdfErrore
	 * @param inTrasfPdfWarning
	 * @param inDataScadenza
	 * @param inUrgenza
	 * @param inDataCreazione
	 * @param inIdCategoriaDocumento
	 * @param inFlagFirmaAutografaRM
	 * @param inTipoProtocollo
	 * @param inFlagRiservato
	 * @param inNumContributi
	 * @param inFlagFirmaPDF
	 * @param inTipoProcedimento
	 * @param inTipoSpedizione
	 * @param inAnnoDocumento
	 * @param inGuuid
	 * @param inIdFormatoDocumento
	 * @param inDataProtocollazione
	 * @param inClasseDocumentale
	 * @param errorMessage
	 * @param warningMessage
	 * @param inNomeFile
	 * @param sottoCategoriaDocumentoUscita
	 * @param protocolloRiferimento
	 * @param integrazioneDati
	 * @param idUfficioCreatore
	 * @param inDecretoLiquidazione
	 */
	public MasterDocumentRedDTO(final String inDocumentTitle, final Integer inNumeroDocumento, final String inOggetto, final String inTipologiaDocumento,
			final Integer inNumeroProtocollo, final Integer inAnnoProtocollo, final Boolean inAttachmentPresent, final Boolean inTrasfPdfErrore,
			final Boolean inTrasfPdfWarning, final Date inDataScadenza, final Integer inUrgenza, final Date inDataCreazione, final Integer inIdCategoriaDocumento,
			final Boolean inFlagFirmaAutografaRM, final Integer inTipoProtocollo, final Boolean inFlagRiservato, final Long inNumContributi, final Boolean inFlagFirmaPDF,
			final String inTipoProcedimento, final TipoSpedizioneDocumentoEnum inTipoSpedizione, final Integer inAnnoDocumento, final String inGuuid,
			final Integer inIdFormatoDocumento, final Date inDataProtocollazione, final String inClasseDocumentale, final String errorMessage, final String warningMessage,
			final String inNomeFile, final SottoCategoriaDocumentoUscitaEnum sottoCategoriaDocumentoUscita, final String protocolloRiferimento, final boolean integrazioneDati,
			final Integer idUfficioCreatore, final String inDecretoLiquidazione) {
		super();
		idTable = UUID.randomUUID().toString();
		idFormatoDocumento = inIdFormatoDocumento;
		idCategoriaDocumento = inIdCategoriaDocumento;
		documentTitle = inDocumentTitle;
		dataCreazione = inDataCreazione;
		oggetto = inOggetto;
		numeroDocumento = inNumeroDocumento;
		numeroProtocollo = inNumeroProtocollo;
		tipologiaDocumento = inTipologiaDocumento;
		bAttachmentPresent = inAttachmentPresent;
		annoProtocollo = inAnnoProtocollo;
		bTrasfPdfWarning = inTrasfPdfWarning;
		dataScadenza = inDataScadenza;
		bTrasfPdfErrore = inTrasfPdfErrore;
		urgenza = inUrgenza;
		tipoProtocollo = inTipoProtocollo;
		flagFirmaAutografaRM = inFlagFirmaAutografaRM;
		numContributi = inNumContributi;
		flagRiservato = inFlagRiservato;
		hasContributi = false;
		if (numContributi != null) {
			hasContributi = numContributi > 0;
		}
		flagFirmaPDF = inFlagFirmaPDF;
		tipoSpedizione = inTipoSpedizione;
		tipoProcedimento = inTipoProcedimento;
		guuid = inGuuid;
		annoDocumento = inAnnoDocumento;
		classeDocumentale = inClasseDocumentale;
		dataProtocollazione = inDataProtocollazione;
		nomeFile = inNomeFile;

		// Flag che verranno abilitati più avanti
		flagEnableFirma = false;
		flagEnableFirmaAutografa = false;
		flagEnableRifiuta = false;
		flagEnableVista = false;
		flagEnableSigla = false;
		flagEnableProcediCorriere = false;
		flagSiebel = false;

		this.errorMessage = errorMessage;
		this.warningMessage = warningMessage;
		flagDisabilitato = false;
		setFlagSelezione(true);
		this.sottoCategoriaDocumentoUscita = sottoCategoriaDocumentoUscita;
		this.protocolloRiferimento = protocolloRiferimento;
		this.integrazioneDati = integrazioneDati;
		this.idUfficioCreatore = idUfficioCreatore;
		this.decretoLiquidazione = inDecretoLiquidazione;
	}

	/**
	 * Costruttore.
	 * 
	 * @param inDocumentTitle
	 * @param inNumeroDocumento
	 * @param inOggetto
	 * @param inTipologiaDocumento
	 * @param inNumeroProtocollo
	 * @param inAnnoProtocollo
	 * @param inAttachmentPresent
	 * @param inTrasfPdfErrore
	 * @param inTrasfPdfWarning
	 * @param inDataScadenza
	 * @param inUrgenza
	 * @param inDataCreazione
	 * @param inIdCategoriaDocumento
	 * @param inFlagFirmaAutografaRM
	 * @param inTipoProtocollo
	 * @param inFlagRiservato
	 * @param inHasContributi
	 * @param inFlagFirmaPDF
	 * @param inTipoProcedimento
	 * @param inTipoSpedizione
	 * @param inAnnoDocumento
	 * @param inGuuid
	 * @param inIdFormatoDocumento
	 * @param inDataProtocollazione
	 * @param inClasseDocumentale
	 * @param inErrorMessage
	 * @param inWarningMessage
	 * @param inNomeFile
	 * @param inMittente
	 * @param inDestinatari
	 * @param inFlagNoContent
	 * @param inIdProtocollo
	 * @param inNumeroProtEmergenza
	 * @param inAnnoProtEmergenza
	 * @param inIsModalitaRegistroRepertorio
	 * @param inDescrizioneRegistroRepertorio
	 * @param inCodiceFlusso
	 * @param inPlaceholderSigla
	 * @param inIdTipologiaDocumento
	 * @param inIdTipoProcedimento
	 * @param sottoCategoriaDocumentoUscita
	 * @param protocolloRiferimento
	 * @param integrazioneDati
	 * @param idUfficioCreatore
	 * @param firmaValidePrinc
	 * @param firmaValideAllegati
	 * @param inDecretoLiquidazione
	 */
	public MasterDocumentRedDTO(final String inDocumentTitle, final Integer inNumeroDocumento, final String inOggetto, final String inTipologiaDocumento,
			final Integer inNumeroProtocollo, final Integer inAnnoProtocollo, final Boolean inAttachmentPresent, final Boolean inTrasfPdfErrore,
			final Boolean inTrasfPdfWarning, final Date inDataScadenza, final Integer inUrgenza, final Date inDataCreazione, final Integer inIdCategoriaDocumento,
			final Boolean inFlagFirmaAutografaRM, final Integer inTipoProtocollo, final Boolean inFlagRiservato, final Boolean inHasContributi, final Boolean inFlagFirmaPDF,
			final String inTipoProcedimento, final TipoSpedizioneDocumentoEnum inTipoSpedizione, final Integer inAnnoDocumento, final String inGuuid,
			final Integer inIdFormatoDocumento, final Date inDataProtocollazione, final String inClasseDocumentale, final String inErrorMessage, final String inWarningMessage,
			final String inNomeFile, final String inMittente, final String inDestinatari, final boolean inFlagNoContent, final String inIdProtocollo,
			final Integer inNumeroProtEmergenza, final Integer inAnnoProtEmergenza, final Boolean inIsModalitaRegistroRepertorio, final String inDescrizioneRegistroRepertorio,
			final String inCodiceFlusso, final String inPlaceholderSigla, final Integer inIdTipologiaDocumento, final Integer inIdTipoProcedimento,
			final SottoCategoriaDocumentoUscitaEnum sottoCategoriaDocumentoUscita, final String protocolloRiferimento, final boolean integrazioneDati, 
			final Integer idUfficioCreatore, final Boolean firmaValidePrinc, final Boolean firmaValideAllegati, final String inDecretoLiquidazione,
			final Integer inMetadatoValiditaFirmaPrincipale, final Integer inVersion, final Integer inIdUffCreatore, final List<String[]> inDestinatariRaw,
			final ASignMasterDTO inInfo) {
		super();
		idTable = UUID.randomUUID().toString();
		idCategoriaDocumento = inIdCategoriaDocumento;
		idFormatoDocumento = inIdFormatoDocumento;
		dataCreazione = inDataCreazione;
		documentTitle = inDocumentTitle;
		numeroDocumento = inNumeroDocumento;
		oggetto = inOggetto;
		tipologiaDocumento = inTipologiaDocumento;
		numeroProtocollo = inNumeroProtocollo;
		annoProtocollo = inAnnoProtocollo;
		bAttachmentPresent = inAttachmentPresent;
		bTrasfPdfErrore = inTrasfPdfErrore;
		bTrasfPdfWarning = inTrasfPdfWarning;
		dataScadenza = inDataScadenza;
		urgenza = inUrgenza;
		flagFirmaAutografaRM = inFlagFirmaAutografaRM;
		tipoProtocollo = inTipoProtocollo;
		flagRiservato = inFlagRiservato;
		hasContributi = inHasContributi;
		flagFirmaPDF = inFlagFirmaPDF;
		tipoProcedimento = inTipoProcedimento;
		tipoSpedizione = inTipoSpedizione;
		annoDocumento = inAnnoDocumento;
		guuid = inGuuid;
		classeDocumentale = inClasseDocumentale;

		nomeFile = inNomeFile;

		dataProtocollazione = inDataProtocollazione;

		// Flag che verranno abilitati più avanti
		flagEnableFirma = false;
		flagEnableFirmaAutografa = false;
		flagEnableRifiuta = false;
		flagEnableVista = false;
		flagEnableSigla = false;
		flagEnableProcediCorriere = false;
		flagSiebel = false;

		errorMessage = inErrorMessage;
		warningMessage = inWarningMessage;

		mittente = inMittente;
		destinatari = inDestinatari;

		flagNoContent = inFlagNoContent;
		flagDisabilitato = false;

		idProtocollo = inIdProtocollo;

		numeroProtEmergenza = inNumeroProtEmergenza;
		annoProtEmergenza = inAnnoProtEmergenza;

		isModalitaRegistroRepertorio = inIsModalitaRegistroRepertorio;
		descrizioneRegistroRepertorio = inDescrizioneRegistroRepertorio;

		codiceFlusso = inCodiceFlusso;

		setFlagSelezione(true);
		placeholderSigla = inPlaceholderSigla;

		idTipologiaDocumento = inIdTipologiaDocumento;
		idTipoProcedimento = inIdTipoProcedimento;

		this.sottoCategoriaDocumentoUscita = sottoCategoriaDocumentoUscita;
		this.protocolloRiferimento = protocolloRiferimento;
		this.integrazioneDati = integrazioneDati;
		
		this.idUfficioCreatore = idUfficioCreatore;
		this.decretoLiquidazione = inDecretoLiquidazione;
		 
		this.firmaValidaPrincipale = firmaValidePrinc;
		this.firmaValidaAllegati = firmaValideAllegati;
		this.metadatoValiditaFirmaPrincipale = inMetadatoValiditaFirmaPrincipale;
		this.version = inVersion;
		this.uffCreatore = inIdUffCreatore;
		this.destinatariRaw = inDestinatariRaw;
		
		this.info = inInfo;
	}

	/**
	 * Costruttore utilizzato per il clone del master
	 */
	public MasterDocumentRedDTO(final MasterDocumentRedDTO m) {
		super();
		this.nomeFile = m.getNomeFile();
		this.urgente = m.getUrgente();
		this.iconaStato = m.getIconaStato();
		this.annoProtocollo = m.getAnnoProtocollo();
		this.bAttachmentPresent = m.getbAttachmentPresent();
		this.bTrasfPdfErrore = m.getbTrasfPdfErrore();
		this.bTrasfPdfWarning = m.getbTrasfPdfWarning();
		this.documentTitle = m.getDocumentTitle();
		this.flagEnableFirma = m.getFlagEnableFirma();
		this.flagEnableFirmaAutografa = m.getFlagEnableFirmaAutografa();
		this.flagEnableRifiuta = m.getFlagEnableRifiuta();
		this.flagEnableSigla = m.getFlagEnableSigla();
		this.flagEnableVista = m.getFlagEnableVista();
		this.flagEnableProcediCorriere = m.getFlagEnableProcediCorriere();
		this.flagFirmaDig = m.getFlagFirmaDig();
		this.idUtenteDestinatario = m.getIdUtenteDestinatario();
		this.idUfficioDestinatario = m.getIdUfficioDestinatario();
		this.motivazioneAssegnazione = m.getMotivazioneAssegnazione();
		this.numeroDocumento = m.getNumeroDocumento();
		this.numeroProtocollo = m.getNumeroProtocollo();
		this.oggetto = m.getOggetto();
		this.operazione = m.getOperazione();
		this.queue = m.getQueue();
		this.tipologiaDocumento = m.getTipologiaDocumento();
		this.wobNumber = m.getWobNumber();
		this.flagFirmaAutografaRM = m.getFlagFirmaAutografaRM();
		this.dataProtocollazione = m.getDataProtocollazione();
		this.dataCreazione = m.getDataCreazione();
		this.dataScadenza = m.getDataScadenza();
		this.urgenza = m.getUrgenza();
		this.stato = m.getStato();
		this.tipoSpedizione = m.getTipoSpedizione();
		this.tipoProcedimento = m.getTipoProcedimento();
		this.idCategoriaDocumento = m.getIdCategoriaDocumento();
		this.idFormatoDocumento = m.getIdFormatoDocumento();
		this.flagRenderizzato = m.getFlagRenderizzato();
		this.idMomentoProtocollazione = m.getIdMomentoProtocollazione();
		this.tipoProtocollo = m.getTipoProtocollo();
		this.flagRiservato = m.getFlagRiservato();
		this.numContributi = m.getNumContributi();
		this.hasContributi = m.getHasContributi();
		this.firmaCopiaConforme = m.getFirmaCopiaConforme();
		this.tipoAssegnazione = m.getTipoAssegnazione();
		this.flagFirmaPDF = m.getFlagFirmaPDF();
		this.annoDocumento = m.getAnnoDocumento();
		this.guuid = m.getGuuid();
		this.classeDocumentale = m.getClasseDocumentale();
		this.errorMessage = m.getErrorMessage();
		this.warningMessage = m.getWarningMessage();
		this.mittente = m.getMittente();
		this.destinatari = m.getDestinatari();
		this.assegnatario = m.getAssegnatario();
		this.flagSiebel = m.isFlagSiebel();
		this.flagNoContent = m.isFlagNoContent();
		this.flagDisabilitato = m.getFlagDisabilitato();
		this.flagSelezione = m.isFlagSelezione();
		this.idProtocollo = m.getIdProtocollo();
		this.annoProtEmergenza = m.getAnnoProtEmergenza();
		this.numeroProtEmergenza = m.getNumeroProtEmergenza();
		this.dataProtEmergenza = m.getDataProtEmergenza();
		this.isModalitaRegistroRepertorio = m.getIsModalitaRegistroRepertorio();
		this.descrizioneRegistroRepertorio = m.getDescrizioneRegistroRepertorio();
		this.codiceFlusso = m.getCodiceFlusso();
		this.placeholderSigla = m.getPlaceholderSigla();

		this.idTipologiaDocumento = m.getIdTipologiaDocumento();
		this.idTipoProcedimento = m.getIdTipoProcedimento();
		this.sottoCategoriaDocumentoUscita = m.getSottoCategoriaDocumentoUscita();
		this.annullaTemplate = m.isAnnullaTemplate();
		this.protocolloRiferimento = m.getProtocolloRiferimento();
		this.integrazioneDati = m.getIntegrazioneDati();
		this.idUfficioCreatore = m.getIdUfficioCreatore();
		this.decretoLiquidazione = m.getDecretoLiquidazione();
		
		this.docLocked = m.isDocLocked();
		this.usernameLocked = m.getUsernameLocked();
		this.firmaValidaPrincipale = m.getFirmaValidaPrincipale();
		this.firmaValidaAllegati = m.getFirmaValidaAllegati();
		this.metadatoValiditaFirmaPrincipale=m.getMetadatoValiditaFirmaPrincipale();
		this.version = m.getVersion();
		this.uffCreatore = m.getUffCreatore();
		this.destinatariRaw= m.getDestinatariRaw();
	}
	
	/**
	 * Costruttore per response di predisposizione.
	 * 
	 * @param inDetail
	 */
	public MasterDocumentRedDTO(final DetailDocumentRedDTO inDetail) {
		this.documentTitle = inDetail.getDocumentTitle();
		this.guuid = inDetail.getGuid();
		this.numeroDocumento = inDetail.getNumeroDocumento();
		this.numeroProtocollo = inDetail.getNumeroProtocollo();
		this.annoProtocollo = inDetail.getAnnoProtocollo();
		this.dataProtocollazione = inDetail.getDataProtocollo();
		this.tipoProtocollo = inDetail.getTipoProtocollo();
		this.wobNumber = inDetail.getWobNumberSelected();
		this.classeDocumentale = inDetail.getDocumentClass();
		this.idTipologiaDocumento = inDetail.getIdTipologiaDocumento();
		this.idTipoProcedimento = inDetail.getIdTipologiaProcedimento();
	}
	
	/**
	 * Restituisce le info dell'item per il popolamento del dettaglio master.
	 * @return master item firma asincrona
	 */
	public ASignMasterDTO getInfo() {
		return info;
	}

	/**
	 * Imposta le info dell'item per il popolamento del dettaglio master.
	 * @param info
	 */
	public void setInfo(ASignMasterDTO info) {
		this.info = info;
	}

	/**
	 * Metodo utilzzato per merge dati PE --> CE
	 * 
	 * @param wf
	 */
	public final void fill(final PEDocumentoDTO wf) {
		this.setUrgente(wf.getUrgente());
		this.setOperazione(wf.getOperazione());
		this.setWobNumber(wf.getWobNumber());
		this.setResponsesRaw(wf.getResponses());
		this.setStepName(wf.getStepName());
		this.setNota(wf.getNotaDTO());
		if (wf.getDataScadenza() != null) {
			final LocalDateTime wfDataScadenza = wf.getDataScadenza().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
			if (DateUtils.NULL_DATE.equals(wfDataScadenza)) {
				this.setDataScadenza(null);
			} else {
				this.setDataScadenza(wf.getDataScadenza());
			}
		} else {
			this.setDataScadenza(null);
		}
		this.setQueue(wf.getQueue());
		this.setIdUtenteDestinatario(wf.getIdUtenteDestinatario());
		this.setIdUfficioDestinatario(wf.getIdNodoDestinatario());
		this.setFlagFirmaDig(wf.getFlagFirmaDig());
		this.setMotivazioneAssegnazione(wf.getMotivazioneAssegnazione());
		this.setTipoAssegnazione(TipoAssegnazioneEnum.get(wf.getTipoAssegnazioneId()));
		if (this.tipoAssegnazione != null) {
			this.tipoAssegnazioneDescr = this.tipoAssegnazione.getDescrizione();
		}
		this.setContributiRichiesti(wf.getContributiRichiesti());
		this.setContributiPervenuti(wf.getContributiPervenuti());
		this.setDataAss(wf.getDataAss());
		if (Boolean.FALSE.equals(this.getbTrasfPdfErrore())) {
			this.setFlagEnableFirma(wf.getFlagEnableFirma());
			this.setFlagEnableFirmaAutografa(wf.getFlagEnableFirmaAutografa());
			this.setFlagEnableRifiuta(wf.getFlagEnableRifiuta());
			this.setFlagEnableSigla(wf.getFlagEnableSigla());
			this.setFlagEnableVista(wf.getFlagEnableVista());
		}
		this.setAssegnatario(wf.getAssegnatario());
		flagDisabilitato = false;
		flagSelezione = true;

		this.setTipoFirma(wf.getTipoFirma());
		this.setAnnullaTemplate(wf.isAnnullaTemplate());
		
		if(wf.getDataCreazioneWF()!=null) {
			try {
				DateFormat format = new SimpleDateFormat("dd/MM/yy");
				this.dataCreazioneWF = format.parse(wf.getDataCreazioneWF());
			} catch (Exception e) {
				LOGGER.warn(e);
				this.dataCreazioneWF = null;
			}
		}
	}

	/**
	 * @return
	 */
	public Date getDataAss() {
		return dataAss;
	}

	/**
	 * @param inDataAss
	 */
	private void setDataAss(final Date inDataAss) {
		dataAss = inDataAss;
	}

	/**
	 * @return the urgente
	 */
	public final Boolean getUrgente() {
		return urgente;
	}

	/**
	 * @param urgente the urgente to set
	 */
	public final void setUrgente(final Boolean urgente) {
		this.urgente = urgente;
	}

	/**
	 * @return the iconaStato
	 */
	public final IconaStatoEnum getIconaStato() {
		return iconaStato;
	}

	/**
	 * @param iconaStato the iconaStato to set
	 */
	public final void setIconaStato(final IconaStatoEnum iconaStato) {
		this.iconaStato = iconaStato;
	}

	/**
	 * @return the annoProtocollo
	 */
	public final Integer getAnnoProtocollo() {
		return annoProtocollo;
	}

	/**
	 * @param annoProtocollo the annoProtocollo to set
	 */
	public final void setAnnoProtocollo(final Integer annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}

	/**
	 * @return the bAttachmentPresent
	 */
	public final Boolean getbAttachmentPresent() {
		return bAttachmentPresent;
	}

	/**
	 * @param bAttachmentPresent the bAttachmentPresent to set
	 */
	public final void setbAttachmentPresent(final Boolean bAttachmentPresent) {
		this.bAttachmentPresent = bAttachmentPresent;
	}

	/**
	 * @return the bTrasfPdfErrore
	 */
	public final Boolean getbTrasfPdfErrore() {
		return bTrasfPdfErrore;
	}

	/**
	 * @return the bTrasfPdfWarning
	 */
	public final Boolean getbTrasfPdfWarning() {
		return bTrasfPdfWarning;
	}

	/**
	 * @param bTrasfPdfErrore the bTrasfPdfErrore to set
	 */
	public final void setbTrasfPdfErrore(final Boolean bTrasfPdfErrore) {
		this.bTrasfPdfErrore = bTrasfPdfErrore;
	}

	/**
	 * @param bTrasfPdfWarning the bTrasfPdfWarning to set
	 */
	public final void setbTrasfPdfWarning(final Boolean bTrasfPdfWarning) {
		this.bTrasfPdfWarning = bTrasfPdfWarning;
	}

	/**
	 * @return the documentTitle
	 */
	public final String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * @param documentTitle the documentTitle to set
	 */
	public final void setDocumentTitle(final String documentTitle) {
		this.documentTitle = documentTitle;
	}

	/**
	 * @return the flagEnableFirma
	 */
	public final Boolean getFlagEnableFirma() {
		return flagEnableFirma;
	}

	/**
	 * @param flagEnableFirma the flagEnableFirma to set
	 */
	public final void setFlagEnableFirma(final Boolean flagEnableFirma) {
		this.flagEnableFirma = flagEnableFirma;
	}

	/**
	 * @return the flagEnableFirmaAutografa
	 */
	public final Boolean getFlagEnableFirmaAutografa() {
		return flagEnableFirmaAutografa;
	}

	/**
	 * @param flagEnableFirmaAutografa the flagEnableFirmaAutografa to set
	 */
	public final void setFlagEnableFirmaAutografa(final Boolean flagEnableFirmaAutografa) {
		this.flagEnableFirmaAutografa = flagEnableFirmaAutografa;
	}

	/**
	 * @return the flagEnableRifiuta
	 */
	public final Boolean getFlagEnableRifiuta() {
		return flagEnableRifiuta;
	}

	/**
	 * @param flagEnableRifiuta the flagEnableRifiuta to set
	 */
	public final void setFlagEnableRifiuta(final Boolean flagEnableRifiuta) {
		this.flagEnableRifiuta = flagEnableRifiuta;
	}

	/**
	 * @return the flagEnableSigla
	 */
	public final Boolean getFlagEnableSigla() {
		return flagEnableSigla;
	}

	/**
	 * @param flagEnableSigla the flagEnableSigla to set
	 */
	public final void setFlagEnableSigla(final Boolean flagEnableSigla) {
		this.flagEnableSigla = flagEnableSigla;
	}

	/**
	 * @return the flagEnableVista
	 */
	public final Boolean getFlagEnableVista() {
		return flagEnableVista;
	}

	/**
	 * @param flagEnableVista the flagEnableVista to set
	 */
	public final void setFlagEnableVista(final Boolean flagEnableVista) {
		this.flagEnableVista = flagEnableVista;
	}

	/**
	 * @return the flagEnableProcediCorriere
	 */
	public final Boolean getFlagEnableProcediCorriere() {
		return flagEnableProcediCorriere;
	}

	/**
	 * @param flagEnableProcediCorriere the flagEnableProcediCorriere to set
	 */
	public final void setFlagEnableProcediCorriere(final Boolean flagEnableProcediCorriere) {
		this.flagEnableProcediCorriere = flagEnableProcediCorriere;
	}

	/**
	 * @return the flagFirmaDig
	 */
	public final Boolean getFlagFirmaDig() {
		return flagFirmaDig;
	}

	/**
	 * @param flagFirmaDig the flagFirmaDig to set
	 */
	public final void setFlagFirmaDig(final Boolean flagFirmaDig) {
		this.flagFirmaDig = flagFirmaDig;
	}

	/**
	 * @return the idUtenteDestinatario
	 */
	public final Integer getIdUtenteDestinatario() {
		return idUtenteDestinatario;
	}

	/**
	 * @param idUtenteDestinatario the idUtenteDestinatario to set
	 */
	public final void setIdUtenteDestinatario(final Integer idUtenteDestinatario) {
		this.idUtenteDestinatario = idUtenteDestinatario;
	}

	/**
	 * Restituisce l'id dell'ufficio destinatario.
	 * 
	 * @return id ufficio destinatario
	 */
	public Integer getIdUfficioDestinatario() {
		return idUfficioDestinatario;
	}

	/**
	 * Imposta l'id dell'ufficio destintario.
	 * 
	 * @param idUfficioDestinatario
	 */
	public void setIdUfficioDestinatario(final Integer idUfficioDestinatario) {
		this.idUfficioDestinatario = idUfficioDestinatario;
	}

	/**
	 * @return the motivazioneAssegnazione
	 */
	public final String getMotivazioneAssegnazione() {
		return motivazioneAssegnazione;
	}

	/**
	 * @param motivazioneAssegnazione the motivazioneAssegnazione to set
	 */
	public final void setMotivazioneAssegnazione(final String motivazioneAssegnazione) {
		this.motivazioneAssegnazione = motivazioneAssegnazione;
	}

	/**
	 * @return the numeroDocumento
	 */
	public final Integer getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * @param numeroDocumento the numeroDocumento to set
	 */
	public final void setNumeroDocumento(final Integer numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * @return the numeroProtocollo
	 */
	public final Integer getNumeroProtocollo() {
		return numeroProtocollo;
	}

	/**
	 * @param numeroProtocollo the numeroProtocollo to set
	 */
	public final void setNumeroProtocollo(final Integer numeroProtocollo) {
		this.numeroProtocollo = numeroProtocollo;
	}

	/**
	 * @return the oggetto
	 */
	public final String getOggetto() {
		return oggetto;
	}

	/**
	 * @param oggetto the oggetto to set
	 */
	public final void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}

	/**
	 * @return the operazione
	 */
	public final TipoOperazioneLibroFirmaEnum getOperazione() {
		return operazione;
	}

	/**
	 * @param operazione the operazione to set
	 */
	public final void setOperazione(final TipoOperazioneLibroFirmaEnum operazione) {
		this.operazione = operazione;
	}

	/**
	 * @return the queue
	 */
	public final DocumentQueueEnum getQueue() {
		return queue;
	}

	/**
	 * @param queue the queue to set
	 */
	public final void setQueue(final DocumentQueueEnum queue) {
		this.queue = queue;
	}

	/**
	 * @return the tipologiaDocumento
	 */
	public final String getTipologiaDocumento() {
		return tipologiaDocumento;
	}

	/**
	 * @param tipologiaDocumento the tipologiaDocumento to set
	 */
	public final void setTipologiaDocumento(final String tipologiaDocumento) {
		this.tipologiaDocumento = tipologiaDocumento;
	}

	/**
	 * @return the wobNumber
	 */
	public final String getWobNumber() {
		return wobNumber;
	}

	/**
	 * @param wobNumber the wobNumber to set
	 */
	public final void setWobNumber(final String wobNumber) {
		this.wobNumber = wobNumber;
	}

	/**
	 * @return the flagFirmaAutografaRM
	 */
	public final Boolean getFlagFirmaAutografaRM() {
		return flagFirmaAutografaRM;
	}

	/**
	 * @param flagFirmaAutografaRM the flagFirmaAutografaRM to set
	 */
	public final void setFlagFirmaAutografaRM(final Boolean flagFirmaAutografaRM) {
		this.flagFirmaAutografaRM = flagFirmaAutografaRM;
	}

	/**
	 * @return the dataCreazione
	 */
	public final Date getDataCreazione() {
		return dataCreazione;
	}

	/**
	 * @param dataCreazione the dataCreazione to set
	 */
	public final void setDataCreazione(final Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	/**
	 * @return the dataScadenza
	 */
	public final Date getDataScadenza() {
		return dataScadenza;
	}

	/**
	 * @param dataScadenza the dataScadenza to set
	 */
	public final void setDataScadenza(final Date dataScadenza) {
		this.dataScadenza = dataScadenza;
	}

	/**
	 * @return the urgenza
	 */
	public final Integer getUrgenza() {
		return urgenza;
	}

	/**
	 * @param urgenza the urgenza to set
	 */
	public final void setUrgenza(final Integer urgenza) {
		this.urgenza = urgenza;
	}

	/**
	 * @return the stato
	 */
	public final StatoDocumentoFuoriLibroFirmaEnum getStato() {
		return stato;
	}

	/**
	 * @param stato the stato to set
	 */
	public final void setStato(final StatoDocumentoFuoriLibroFirmaEnum stato) {
		this.stato = stato;
	}

	/**
	 * @return the tipoSpedizione
	 */
	public final TipoSpedizioneDocumentoEnum getTipoSpedizione() {
		return tipoSpedizione;
	}

	/**
	 * @param tipoSpedizione the tipoSpedizione to set
	 */
	public final void setTipoSpedizione(final TipoSpedizioneDocumentoEnum tipoSpedizione) {
		this.tipoSpedizione = tipoSpedizione;
	}

	/**
	 * @return the idCategoriaDocumento
	 */
	public final Integer getIdCategoriaDocumento() {
		return idCategoriaDocumento;
	}

	/**
	 * @param idCategoriaDocumento the idCategoriaDocumento to set
	 */
	public final void setIdCategoriaDocumento(final Integer idCategoriaDocumento) {
		this.idCategoriaDocumento = idCategoriaDocumento;
	}

	/**
	 * @return the idFormatoDocumento
	 */
	public final Integer getIdFormatoDocumento() {
		return idFormatoDocumento;
	}

	/**
	 * @param idFormatoDocumento the idFormatoDocumento to set
	 */
	public final void setIdFormatoDocumento(final Integer idFormatoDocumento) {
		this.idFormatoDocumento = idFormatoDocumento;
	}

	/**
	 * @return the flagRenderizzato
	 */
	public final Boolean getFlagRenderizzato() {
		return flagRenderizzato;
	}

	/**
	 * @param flagRenderizzato the flagRenderizzato to set
	 */
	public final void setFlagRenderizzato(final Boolean flagRenderizzato) {
		this.flagRenderizzato = flagRenderizzato;
	}

	/**
	 * @return the idMomentoProtocollazione
	 */
	public final Integer getIdMomentoProtocollazione() {
		return idMomentoProtocollazione;
	}

	/**
	 * @param idMomentoProtocollazione the idMomentoProtocollazione to set
	 */
	public final void setIdMomentoProtocollazione(final Integer idMomentoProtocollazione) {
		this.idMomentoProtocollazione = idMomentoProtocollazione;
	}

	/**
	 * @return the tipoProtocollo
	 */
	public final Integer getTipoProtocollo() {
		return tipoProtocollo;
	}

	/**
	 * @param tipoProtocollo the tipoProtocollo to set
	 */
	public final void setTipoProtocollo(final Integer tipoProtocollo) {
		this.tipoProtocollo = tipoProtocollo;
	}

	/**
	 * @return the flagRiservato
	 */
	public final Boolean getFlagRiservato() {
		return flagRiservato;
	}

	/**
	 * @param flagRiservato the flagRiservato to set
	 */
	public final void setFlagRiservato(final Boolean flagRiservato) {
		this.flagRiservato = flagRiservato;
	}

	/**
	 * @return the numContributi
	 */
	public final Long getNumContributi() {
		return numContributi;
	}

	/**
	 * Getter.
	 * 
	 * @return test presenza contributi
	 */
	public final Boolean getHasContributi() {
		return hasContributi;
	}

	/**
	 * @return the firmaCopiaConforme
	 */
	public final Boolean getFirmaCopiaConforme() {
		return firmaCopiaConforme;
	}

	/**
	 * @param firmaCopiaConforme the firmaCopiaConforme to set
	 */
	public final void setFirmaCopiaConforme(final Boolean firmaCopiaConforme) {
		this.firmaCopiaConforme = firmaCopiaConforme;
	}

	/**
	 * @return the tipoAssegnazione
	 */
	public final TipoAssegnazioneEnum getTipoAssegnazione() {
		return tipoAssegnazione;
	}

	/**
	 * @param tipoAssegnazione the tipoAssegnazione to set
	 */
	public final void setTipoAssegnazione(final TipoAssegnazioneEnum tipoAssegnazione) {
		this.tipoAssegnazione = tipoAssegnazione;
	}

	/**
	 * @return the flagFirmaPDF
	 */
	public final Boolean getFlagFirmaPDF() {
		return flagFirmaPDF;
	}

	/**
	 * @param flagFirmaPDF the flagFirmaPDF to set
	 */
	public final void setFlagFirmaPDF(final Boolean flagFirmaPDF) {
		this.flagFirmaPDF = flagFirmaPDF;
	}

	/**
	 * @return the dataProtocollazione
	 */
	public final Date getDataProtocollazione() {
		return dataProtocollazione;
	}

	/**
	 * @return the tipoProcedimento
	 */
	public String getTipoProcedimento() {
		return tipoProcedimento;
	}

	/**
	 * @param tipoProcedimento the tipoProcedimento to set
	 */
	public void setTipoProcedimento(final String tipoProcedimento) {
		this.tipoProcedimento = tipoProcedimento;
	}

	/**
	 * @return the idTable
	 */
	public String getIdTable() {
		return idTable;
	}

	/**
	 * @param idTable the idTable to set
	 */
	public void setIdTable(final String idTable) {
		this.idTable = idTable;
	}

	/**
	 * @return the responses
	 */
	public ResponsesDTO getResponses() {
		return responses;
	}

	/**
	 * @param responses the responses to set
	 */
	public void setResponses(final ResponsesDTO responses) {
		this.responses = responses;
	}

	/**
	 * @return the annoDocumento
	 */
	public Integer getAnnoDocumento() {
		return annoDocumento;
	}

	/**
	 * @param annoDocumento the annoDocumento to set
	 */
	public void setAnnoDocumento(final Integer annoDocumento) {
		this.annoDocumento = annoDocumento;
	}

	/**
	 * @return the guuid
	 */
	public String getGuuid() {
		return guuid;
	}

	/**
	 * @return the classeDocumentale
	 */
	public String getClasseDocumentale() {
		return classeDocumentale;
	}

	/**
	 * @param guuid the guuid to set
	 */
	public void setGuuid(final String guuid) {
		this.guuid = guuid;
	}

	/**
	 * @param classeDocumentale the classeDocumentale to set
	 */
	public void setClasseDocumentale(final String classeDocumentale) {
		this.classeDocumentale = classeDocumentale;
	}

	/**
	 * @return the stepName
	 */
	public String getStepName() {
		return stepName;
	}

	/**
	 * @param stepName the stepName to set
	 */
	public void setStepName(final String stepName) {
		this.stepName = stepName;
	}

	/**
	 * @return the responsesRaw
	 */
	public String[] getResponsesRaw() {
		return responsesRaw;
	}

	/**
	 * @param responsesRaw the responsesRaw to set
	 */
	public void setResponsesRaw(final String[] responsesRaw) {
		this.responsesRaw = responsesRaw;
	}

	/**
	 * @return the nota
	 */
	public NotaDTO getNota() {
		return nota;
	}

	/**
	 * @param nota the nota to set
	 */
	public void setNota(final NotaDTO nota) {
		this.nota = nota;
	}

	/**
	 * Restituisce il messaggio di errore.
	 * 
	 * @return messaggio di errore
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * Imposta il messaggio di errore.
	 * 
	 * @param errorMessage
	 */
	public void setErrorMessage(final String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * Restituisce il messaggio di warning.
	 * 
	 * @return messaggio di warning
	 */
	public String getWarningMessage() {
		return warningMessage;
	}

	/**
	 * Imposta il messaggio di warning.
	 * 
	 * @param warningMessage
	 */
	public void setWarningMessage(final String warningMessage) {
		this.warningMessage = warningMessage;
	}

	/**
	 * Restituisce il nome del file.
	 * 
	 * @return nome file
	 */
	public String getNomeFile() {
		return nomeFile;
	}

	/**
	 * @return the contributiRichiesti
	 */
	public final Integer getContributiRichiesti() {
		return contributiRichiesti;
	}

	/**
	 * @param contributiRichiesti the contributiRichiesti to set
	 */
	public final void setContributiRichiesti(final Integer contributiRichiesti) {
		this.contributiRichiesti = contributiRichiesti;
	}

	/**
	 * @return the contributiPervenuti
	 */
	public final Integer getContributiPervenuti() {
		return contributiPervenuti;
	}

	/**
	 * @param contributiPervenuti the contributiPervenuti to set
	 */
	public final void setContributiPervenuti(final Integer contributiPervenuti) {
		this.contributiPervenuti = contributiPervenuti;
	}

	/**
	 * Restituisce true se il documento è entrata, false altrimenti.
	 * 
	 * @return true se il documento è entrata, false altrimenti
	 */
	public Boolean getFlagEntrata() {
		return (idCategoriaDocumento == 1 || idCategoriaDocumento == 7);
	}

	/**
	 * Restituisce il char che identifica la tipologia della categoria del
	 * documento.
	 * 
	 * @return char che suggerisce il tipo categoria
	 */
	public String getCharTipoCategoriaDoc() {
		String output = null;
		if (CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0].equals(idCategoriaDocumento)) {
			output = getCharSottoCategoriaEntrata();
		} else if (CategoriaDocumentoEnum.ASSEGNAZIONE_INTERNA.getIds()[0].equals(idCategoriaDocumento)) {
			// ASSEGNAZIONE_INTERNA firmo con destinatario interno per ogni firmatario
			// documento in entrata con tale valore
			output = "I";
		} else if (CategoriaDocumentoEnum.INTERNO.getIds()[0].equals(idCategoriaDocumento)) {
			// INTERNO documento allegato al fascicolo (alias DOCUMENTO ISTRUTTORIA)
			output = "I";
		} else {
			// sia per documenti in uscita che doc in uscita non protocollati
			// (annoprotocollo == null)
			output = getCharSottoCategoriaUscita();
		}
		return output;
	}

	private String getCharSottoCategoriaEntrata() {
		String output = "E";
		if (integrazioneDati) {
			output += " (" + Constants.Varie.CODICE_INTEGRAZIONE_DATI + ")";
		}
		return output;
	}

	private String getCharSottoCategoriaUscita() {
		String output = "U";
		if (sottoCategoriaDocumentoUscita != null && sottoCategoriaDocumentoUscita.getId() != 0) {
			output += " (" + sottoCategoriaDocumentoUscita.getCodice() + ")";
		}
		return output;
	}

	/**
	 * Restituisce la label per il title della tipologia della categoria del
	 * documento.
	 * 
	 * @return label tipo categoria associata all' {@link #idCategoriaDocumento}
	 */
	public String getTitleTipoCategoriaDoc() {
		String output = null;
		if (CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0].equals(idCategoriaDocumento)) {
			output = "Documento in entrata";
		} else if (CategoriaDocumentoEnum.ASSEGNAZIONE_INTERNA.getIds()[0].equals(idCategoriaDocumento)) {
			// ASSEGNAZIONE_INTERNA firmo con destinatario interno per ogni firmatario
			// documento in entrata con tale valore
			output = "Documento assegnazione interna";
		} else if (CategoriaDocumentoEnum.INTERNO.getIds()[0].equals(idCategoriaDocumento)) {
			// INTERNO documento allegato al fascicolo (alias DOCUMENTO ISTRUTTORIA)
			output = "Documento interno";
		} else {
			if (annoProtocollo == null) {
				output = "Documento in uscita non ancora protocollato";
			} else {
				output = "Documento in uscita";
			}
		}
		return output;
	}

	/**
	 * @return the mittente
	 */
	public String getMittente() {
		return mittente;
	}

	/**
	 * @return the destinatari
	 */
	public String getDestinatari() {
		return destinatari;
	}

	/**
	 * @return the assegnatario
	 */
	public String getAssegnatario() {
		return assegnatario;
	}

	/**
	 * @param assegnatario the assegnatario to set
	 */
	public void setAssegnatario(final String assegnatario) {
		this.assegnatario = assegnatario;
	}

	/**
	 * @param mittente the mittente to set
	 */
	public void setMittente(final String mittente) {
		this.mittente = mittente;
	}

	/**
	 * @return the allegatoDocumentTitle
	 */
	public String getAllegatoDocumentTitle() {
		return allegatoDocumentTitle;
	}

	/**
	 * @param allegatoDocumentTitle the allegatoDocumentTitle to set
	 */
	public void setAllegatoDocumentTitle(final String allegatoDocumentTitle) {
		this.allegatoDocumentTitle = allegatoDocumentTitle;
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object).
	 */
	@Override
	public int compareTo(final MasterDocumentRedDTO o) {
		int comparison = 0;

		comparison = this.getDataCreazione().compareTo(o.getDataCreazione());

		if (comparison == 0) {
			comparison = this.numeroDocumento.compareTo(o.getNumeroDocumento());
		}

		return comparison;

	}

	/**
	 * Restituisce true se Siebel, false altrimenti.
	 * 
	 * @return true se Siebel, false altrimenti
	 */
	public boolean isFlagSiebel() {
		return flagSiebel;
	}

	/**
	 * Imposta il flag associato a Siebel.
	 * 
	 * @param flagSiebel
	 */
	public void setFlagSiebel(final boolean flagSiebel) {
		this.flagSiebel = flagSiebel;
	}

	/**
	 * @return falgNoContent
	 */
	public boolean isFlagNoContent() {
		return flagNoContent;
	}

	/**
	 * Imposta il flag disabilitato.
	 * 
	 * @param bFlagDisabilitato
	 */
	public void setFlagDisabilitato(final Boolean bFlagDisabilitato) {
		flagDisabilitato = bFlagDisabilitato;
	}

	/**
	 * Restituisce true se disabilitato, false altrimenti.
	 * 
	 * @return true se disabilitato, false altrimenti
	 */
	public Boolean getFlagDisabilitato() {
		return flagDisabilitato;
	}

	/**
	 * Imposta il flag associato ai contributi.
	 * 
	 * @param hasContributi
	 */
	public void setHasContributi(final Boolean hasContributi) {
		this.hasContributi = hasContributi;
	}

	/**
	 * Imposta il numero dei contributi.
	 * 
	 * @param numContributi
	 */
	public void setNumContributi(final Long numContributi) {
		this.numContributi = numContributi;
	}

	/**
	 * Imposta il nome del file.
	 * 
	 * @param nomeFile
	 */
	public void setNomeFile(final String nomeFile) {
		this.nomeFile = nomeFile;
	}

	/**
	 * Imposta la lista dei destinatari.
	 * 
	 * @param destinatari
	 */
	public void setDestinatari(final String destinatari) {
		this.destinatari = destinatari;
	}

	/**
	 * Imposta il flag: noContent.
	 * 
	 * @param flagNoContent
	 */
	public void setFlagNoContent(final boolean flagNoContent) {
		this.flagNoContent = flagNoContent;
	}

	/**
	 * Imposta la data di protocollazione.
	 * 
	 * @param dataProtocollazione
	 */
	public void setDataProtocollazione(final Date dataProtocollazione) {
		this.dataProtocollazione = dataProtocollazione;
	}

	/**
	 * Restituisce il numero protocollo emergenza.
	 * 
	 * @return numero protocollo emergenza
	 */
	public Integer getNumeroProtEmergenza() {
		return numeroProtEmergenza;
	}

	/**
	 * Imposta il numero protocollo emergenza.
	 * 
	 * @param numeroProtEmergenza
	 */
	public void setNumeroProtEmergenza(final Integer numeroProtEmergenza) {
		this.numeroProtEmergenza = numeroProtEmergenza;
	}

	/**
	 * Restituisce l'anno del protocollo emergenza.
	 * 
	 * @return anno del protocollo emergenza
	 */
	public Integer getAnnoProtEmergenza() {
		return annoProtEmergenza;
	}

	/**
	 * Imposta l'anno del protocollo emergenza.
	 * 
	 * @param annoProtEmergenza
	 */
	public void setAnnoProtEmergenza(final Integer annoProtEmergenza) {
		this.annoProtEmergenza = annoProtEmergenza;
	}

	/**
	 * Restituisce la data del protocollo emergenza.
	 * 
	 * @return data protocollo emergenza
	 */
	public Date getDataProtEmergenza() {
		return dataProtEmergenza;
	}

	/**
	 * Imposta la data protocollo emergenza.
	 * 
	 * @param dataProtEmergenza
	 */
	public void setDataProtEmergenza(final Date dataProtEmergenza) {
		this.dataProtEmergenza = dataProtEmergenza;
	}

	/**
	 * @see java.lang.Object#hashCode().
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((guuid == null) ? 0 : guuid.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object).
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}

		final MasterDocumentRedDTO other = (MasterDocumentRedDTO) obj;
		if (guuid == null) {
			if (other.guuid != null) {
				return false;
			}
		} else if (!guuid.equals(other.guuid)) {
			return false;
		}
		return true;
	}

	/**
	 * Restituisce true se in modalita registro repertorio, false altrimenti.
	 * 
	 * @return true se in modalita registro repertorio, false altrimenti
	 */
	public Boolean getIsModalitaRegistroRepertorio() {
		return isModalitaRegistroRepertorio;
	}

	/**
	 * Imposta il flag: modalita registro repertorio.
	 * 
	 * @param isModalitaRegistroRepertorio
	 */
	public void setIsModalitaRegistroRepertorio(final Boolean isModalitaRegistroRepertorio) {
		this.isModalitaRegistroRepertorio = isModalitaRegistroRepertorio;
	}

	/**
	 * Restituisce la descrizione del registro repertorio.
	 * 
	 * @return descrizione del registro repertorio
	 */
	public String getDescrizioneRegistroRepertorio() {
		return descrizioneRegistroRepertorio;
	}

	/**
	 * Imposta la descrizione del registro repertorio.
	 * 
	 * @param descrizioneRegistroRepertorio
	 */
	public void setDescrizioneRegistroRepertorio(final String descrizioneRegistroRepertorio) {
		this.descrizioneRegistroRepertorio = descrizioneRegistroRepertorio;
	}

	/**
	 * Restituisce l'id del protocollo.
	 * 
	 * @return id protocollo
	 */
	public String getIdProtocollo() {
		return idProtocollo;
	}

	/**
	 * Imposta l'id del protocollo.
	 * 
	 * @param idProtocollo
	 */
	public void setIdProtocollo(final String idProtocollo) {
		this.idProtocollo = idProtocollo;
	}

	/**
	 * Restituisce l'oggetto protocollo Nps.
	 * 
	 * @return oggetto protocollo NPS
	 */
	public String getOggettoProtocolloNPS() {
		return oggettoProtocolloNPS;
	}

	/**
	 * Imposta l'oggetto protocollo Nps.
	 * 
	 * @param oggettoProtocolloNPS
	 */
	public void setOggettoProtocolloNPS(final String oggettoProtocolloNPS) {
		this.oggettoProtocolloNPS = oggettoProtocolloNPS;
	}

	/**
	 * Restituisce il codice flusso.
	 * 
	 * @return codice flusso
	 */
	public String getCodiceFlusso() {
		return codiceFlusso;
	}

	/**
	 * Restituisce l'icona flusso.
	 * 
	 * @return icona flusso
	 */
	public IconaFlussoEnum getIconaFlusso() {
		return iconaFlusso;
	}

	/**
	 * Imposta l'icona flusso.
	 * 
	 * @param iconaFlusso
	 */
	public void setIconaFlusso(final IconaFlussoEnum iconaFlusso) {
		this.iconaFlusso = iconaFlusso;
	}

	/**
	 * Restituisce true se abilitata la stampa etichette, false altrimenti.
	 * 
	 * @return true se abilitata la stampa etichette, false altrimenti
	 */
	public boolean isStampaEtichette() {
		return stampaEtichette;
	}

	/**
	 * Imposta il flag: stampaEtichette.
	 * 
	 * @param stampaEtichette
	 */
	public void setStampaEtichette(final boolean stampaEtichette) {
		this.stampaEtichette = stampaEtichette;
	}

	/**
	 * Restituisce il primo livelo.
	 * 
	 * @return primo livello
	 */
	public String getPrimoLivello() {
		return primoLivello;
	}

	/**
	 * Imposta il primo livello.
	 * 
	 * @param primoLivello
	 */
	public void setPrimoLivello(final String primoLivello) {
		this.primoLivello = primoLivello;
	}

	/**
	 * Restituisce il secondo livello.
	 * 
	 * @return secondo livello
	 */
	public String getSecondoLivello() {
		return secondoLivello;
	}

	/**
	 * Imposta il secondo livello.
	 * 
	 * @param secondoLivello
	 */
	public void setSecondoLivello(final String secondoLivello) {
		this.secondoLivello = secondoLivello;
	}

	/**
	 * Restituisce la descrizione del tipo assegnazione.
	 * 
	 * @return descrizione tipo assegnazione
	 */
	public String getTipoAssegnazioneDescr() {
		return tipoAssegnazioneDescr;
	}

	/**
	 * Imposta la descrizione del tipo assegnazione.
	 * 
	 * @param tipoAssegnazioneDescr
	 */
	public void setTipoAssegnazioneDescr(final String tipoAssegnazioneDescr) {
		this.tipoAssegnazioneDescr = tipoAssegnazioneDescr;
	}

	/**
	 * Restituisce il numero, id e anno protocollo per Excel.
	 * 
	 * @return numProtIdProtAnnoExcel
	 */
	public String getNumProtIdProtAnnoExcel() {
		return numProtIdProtAnnoExcel;
	}

	/**
	 * @param numProtIdProtAnnoExcel
	 */
	public void setNumProtIdProtAnnoExcel(final String numProtIdProtAnnoExcel) {
		this.numProtIdProtAnnoExcel = numProtIdProtAnnoExcel;
	}

	/**
	 * Restituisce true se abilitata la selezione, false altrimenti.
	 * 
	 * @return true se abilitata la selezione, false altrimenti
	 */
	public boolean isFlagSelezione() {
		return flagSelezione;
	}

	/**
	 * @param flagSelezione
	 */
	public void setFlagSelezione(final boolean flagSelezione) {
		this.flagSelezione = flagSelezione;
	}

	/**
	 * Restituisce il placeholder della sigla.
	 * 
	 * @return placeholder sigla
	 */
	public String getPlaceholderSigla() {
		return placeholderSigla;
	}

	/**
	 * Imposta il placeholder sigla.
	 * 
	 * @param placeholderSigla
	 */
	public void setPlaceholderSigla(final String placeholderSigla) {
		this.placeholderSigla = placeholderSigla;
	}

	/**
	 * Restituisce il tipo firma.
	 * 
	 * @return tipo firma
	 */
	public int getTipoFirma() {
		return tipoFirma;
	}

	/**
	 * Imposta il tipo firma.
	 * 
	 * @param tipoFirma
	 */
	public void setTipoFirma(final int tipoFirma) {
		this.tipoFirma = tipoFirma;
	}

	/**
	 * Restituisce le icone per la stampigliatura del segno grafico.
	 * 
	 * @return icone stampigliatura segni grafici
	 */
	public Map<String, String> getIconaStampigliaturaSegnoGrafico() {
		return iconaStampigliaturaSegnoGrafico;
	}

	/**
	 * Impsota le icone per la stampigliatura dei segni grafici
	 * 
	 * @param iconaStampigliaturaSegnoGrafico
	 */
	public void setIconaStampigliaturaSegnoGrafico(final Map<String, String> iconaStampigliaturaSegnoGrafico) {
		this.iconaStampigliaturaSegnoGrafico = iconaStampigliaturaSegnoGrafico;
	}

	/**
	 * Restituisce il keyset delle icone.
	 * 
	 * @return keyset icone
	 */
	public Set<String> getIconaKeySet() {
		return iconaKeySet;
	}

	/**
	 * Imposta il keyset dell icone.
	 * 
	 * @param iconaKeySet
	 */
	public void setIconaKeySet(final Set<String> iconaKeySet) {
		this.iconaKeySet = iconaKeySet;
	}

	/**
	 * Restituisce la lista degli attributi estesi.
	 * 
	 * @return metadati estesi
	 */
	public Map<String, String> getMetadatiEstesi() {
		return metadatiEstesi;
	}

	/**
	 * Imposta la lista degli attributi estesi.
	 * 
	 * @param metadatiEstesi
	 */
	public void setMetadatiEstesi(final Map<String, String> metadatiEstesi) {
		this.metadatiEstesi = metadatiEstesi;
	}

	/**
	 * Restituisce l'id associato alla tipologia del documento.
	 * 
	 * @return id tipologia documento
	 */
	public Integer getIdTipologiaDocumento() {
		return idTipologiaDocumento;
	}

	/**
	 * Imposta l'id associato alla tipologia del procedimento.
	 * 
	 * @return id tipo procedimento
	 */
	public Integer getIdTipoProcedimento() {
		return idTipoProcedimento;
	}

	/**
	 * Restituisce la sottocategoria del documento in uscita.
	 * 
	 * @return sottocategoria documento
	 */
	public SottoCategoriaDocumentoUscitaEnum getSottoCategoriaDocumentoUscita() {
		return sottoCategoriaDocumentoUscita;
	}

	/**
	 * Imposta la sottocategoria del documento in uscita.
	 * 
	 * @param sottoCategoriaUscita
	 */
	public void setSottoCategoriaDocumentoUscita(final SottoCategoriaDocumentoUscitaEnum sottoCategoriaUscita) {
		this.sottoCategoriaDocumentoUscita = sottoCategoriaUscita;
	}

	/**
	 * @return annullaTemplate
	 */
	public boolean isAnnullaTemplate() {
		return annullaTemplate;
	}

	/**
	 * @param annullaTemplate
	 */
	public void setAnnullaTemplate(final boolean annullaTemplate) {
		this.annullaTemplate = annullaTemplate;
	}

	/**
	 * Restituisce il protocollo di riferimento.
	 * 
	 * @return protocollo di riferimento
	 */
	public String getProtocolloRiferimento() {
		return protocolloRiferimento;
	}

	/**
	 * Imposta il protocollo di riferimento.
	 * 
	 * @param protocolloRiferimento
	 */
	public void setProtocolloRiferimento(final String protocolloRiferimento) {
		this.protocolloRiferimento = protocolloRiferimento;
	}

	/**
	 * Restituisce true se è un'integrazione dati.
	 * 
	 * @return true se è un'integrazione dati, false altrimenti
	 */
	public boolean getIntegrazioneDati() {
		return integrazioneDati;
	}

	/**
	 * @param integrazioneDati
	 */
	public void setIntegrazioneDati(final boolean integrazioneDati) {
		this.integrazioneDati = integrazioneDati;
	}

	/**
	 * Imposta l'id della tipologia documento.
	 * 
	 * @param idTipologiaDocumento
	 */
	public void setIdTipologiaDocumento(final Integer idTipologiaDocumento) {
		this.idTipologiaDocumento = idTipologiaDocumento;
	}

	/**
	 * Imposta l'id del tipo procedimento.
	 * 
	 * @param idTipoProcedimento
	 */
	public void setIdTipoProcedimento(final Integer idTipoProcedimento) {
		this.idTipoProcedimento = idTipoProcedimento;
	}

	/**
	 * Imposta l'id dell'ufficio creatore.
	 * 
	 * @param idUfficioCreatore
	 */
	public void setIdUfficioCreatore(final Integer idUfficioCreatore) {
		this.idUfficioCreatore = idUfficioCreatore;
	}

	/**
	 * Restituisce l'id dell'ufficio creatore.
	 * 
	 * @return id ufficio creatore
	 */
	public Integer getIdUfficioCreatore() {
		return idUfficioCreatore;
	}

	/**
	 * Restituisce lo username dell'utente bloccato.
	 * 
	 * @return username utente bloccato
	 */
	public String getUsernameLocked() {
		return usernameLocked;
	}

	/**
	 * Imposta lo username dell'utente bloccato.
	 * 
	 * @param usernameLocked
	 */
	public void setUsernameLocked(final String usernameLocked) {
		this.usernameLocked = usernameLocked;
	}

	/**
	 * Restituisce true se il documento risulta bloccato, false altrimenti.
	 * 
	 * @return true se il documento risulta bloccato, false altrimenti
	 */
	public boolean isDocLocked() {
		return docLocked;
	}

	/**
	 * Imposta il flag associato al documento che suggerisce lo stato dello stesso:
	 * bloccato o sbloccato.
	 * 
	 * @param docLocked
	 */
	public void setDocLocked(final boolean docLocked) {
		this.docLocked = docLocked;
	}

	/**
	 * Restituisce il decreto liquidazione.
	 * 
	 * @return decreto liquidazione
	 */
	public String getDecretoLiquidazione() {
		return decretoLiquidazione;
	}

	/**
	 * Imposta il decreto liquidazione.
	 * 
	 * @param decretoLiquidazione
	 */
	public void setDecretoLiquidazione(final String decretoLiquidazione) {
		this.decretoLiquidazione = decretoLiquidazione;
	}

	/**
	 * Restituisce true se le firme sugli allegati sono valide, false altrimenti.
	 * 
	 * @return true se le firme sugli allegati sono valide, false altrimenti
	 */
	public Boolean getFirmaValidaAllegati() {
		return firmaValidaAllegati;
	}

	/**
	 * Imposta il flag associato alla validita delle firme sugli allegati.
	 * 
	 * @param firmaValidaAllegati
	 */
	public void setFirmaValidaAllegati(final Boolean firmaValidaAllegati) {
		this.firmaValidaAllegati = firmaValidaAllegati;
	}

	/**
	 * Restituisce true se la firma principale risulta valida, false altrimenti.
	 * 
	 * @return true se la firma principale risulta valida, false altrimenti
	 */
	public Boolean getFirmaValidaPrincipale() {
		return firmaValidaPrincipale;
	}

	/**
	 * @param firmaValidaPrincipale.
	 */
	public void setFirmaValidaPrincipale(final Boolean firmaValidaPrincipale) {
		this.firmaValidaPrincipale = firmaValidaPrincipale;
	}
	
	/**
	 * Restituisce l'ufficio creatore
	 * @return uffCreatore
	 */
	public Integer getUffCreatore() {
		return uffCreatore;
	}
	
	/**
	 * @param uffCreatore.
	 */
	public void setUffCreatore(Integer uffCreatore) {
		this.uffCreatore = uffCreatore;
	}
	
	
	/**
	 * Restituisce la descrizione dell'ufficio creatore.
	 * @return descUffCreatore
	 */
	public String getDescUffCreatore() {
		return descUffCreatore;
	}
	
	/**
	 * @param descUffCreatore.
	 */
	public void setDescUffCreatore(String descUffCreatore) {
		this.descUffCreatore = descUffCreatore;
	}

	/**
	 * Restituisce il metadato che indica la validita della firma principale.
	 * @return metadato validita firma principale
	 */
	public Integer getMetadatoValiditaFirmaPrincipale() {
		return metadatoValiditaFirmaPrincipale;
	}

	/**
	 * Imposta il metadato che indica la validita della firma principale.
	 * @param metadatoValiditaFirmaPrincipale
	 */
	public void setMetadatoValiditaFirmaPrincipale(Integer metadatoValiditaFirmaPrincipale) {
		this.metadatoValiditaFirmaPrincipale = metadatoValiditaFirmaPrincipale;
	}

	/**
	 * Versione da verificare get.
	 * @param versioneDaVerificare
	 */
	public Integer getVersion(){
		return version;
	}
	
	/**
	 * Versione da verificare set.
	 * @param version
	 */
	public void setVersion(Integer version) {
		this.version = version;
	}
	
	/**
	 * @return the destinatariRaw.
	 */
	public List<String[]> getDestinatariRaw() {
		return destinatariRaw;
	}

	/**
	 * @param destinatariRaw the destinatariRaw to set.
	 */
	public void setDestinatariRaw(List<String[]> destinatariRaw) {
		this.destinatariRaw = destinatariRaw;
	}
	/**
	 * @return the dataCreazioneWF.
	 */
	public Date getDataCreazioneWF() {
		return dataCreazioneWF;
	}
	
}