package it.ibm.red.business.enums;

/**
 * The Enum RicercaAssegnanteAssegnatarioEnum.
 *
 * @author a.dilegge
 * 
 *         Enum per la tipologia di ricerca assegnante/assegnatario.
 */
public enum RicercaAssegnanteAssegnatarioEnum {

	/**
	 * Valore.
	 */
	ASSEGNANTE, 
	
	/**
	 * Valore.
	 */
	ASSEGNATARIO;

}
