package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.TemplateDTO;
import it.ibm.red.business.dto.TemplateMetadatoDTO;
import it.ibm.red.business.dto.TemplateMetadatoValueDTO;
import it.ibm.red.business.dto.TemplateMetadatoVersion;
import it.ibm.red.business.dto.TemplateValueDTO;

/**
 * Interfaccia DAO template.
 */
public interface ITemplateDAO extends Serializable {

	/**
	 * Ottiene tutti i template.
	 * @param connection
	 * @param idAOO
	 * @param applet
	 * @return lista dei template
	 */
	List<TemplateDTO> getAll(Connection connection, Long idAOO, boolean applet);
	
	/**
	 * Ottiene i metadati del template.
	 * @param connection
	 * @param idTemplate
	 * @return lista dei metadati del template
	 */
	List<TemplateMetadatoDTO> getMetadati(Connection connection, String idTemplate);
	
	/**
	 * Ottiene il valore del template.
	 * @param connection
	 * @param idDocumento
	 * @return valore del template
	 */
	TemplateValueDTO getTemplateValue(Connection connection, String idDocumento);

	/**
	 * Ottiene i valori dei metadati.
	 * @param connection
	 * @param idDocumento
	 * @param idTemplate
	 * @return lista dei valori dei metadati del template
	 */
	List<TemplateMetadatoValueDTO> getMetadatiValues(Connection connection, String idDocumento, String idTemplate);
	
	/**
	 * Cancella il valore del template.
	 * @param con
	 * @param idDocumento
	 */
	void deleteTemplateValue(Connection con, String idDocumento);
	
	/**
	 * Inserisce il valore del template.
	 * @param con
	 * @param template
	 * @param values
	 */
	void insertTemplateValue(Connection con, TemplateValueDTO template, List<TemplateMetadatoValueDTO> values);
	
	/**
	 * Ottiene le versioni del documento.
	 * @param idDocumento
	 * @param conn
	 * @return lista delle versioni dei metadati del template
	 */
	List<TemplateMetadatoVersion> getVersioniDocumento(Integer idDocumento, Connection conn);
	
	/**
	 * Ottiene la versione del documento.
	 * @param con
	 * @param idDocumento
	 * @return versione
	 */
	Integer getVersionTemplate(Connection con, String idDocumento);
	
	/**
	 * Inserisce il nuovo valore del template.
	 * @param con
	 * @param template
	 * @param values
	 * @param versione
	 * @param idUtenteCreatore
	 */
	void insertTemplateValueNew(Connection con, TemplateValueDTO template, List<TemplateMetadatoValueDTO> values, Integer versione, Long idUtenteCreatore);
	
	/**
	 * Ottiene i valori dei metadati.
	 * @param idTemplate
	 * @param idDocumento
	 * @param versione
	 * @param connection
	 * @return lista dei valori dei metadati del template
	 */
	List<TemplateMetadatoValueDTO> getValueMetadato(String idTemplate, Integer idDocumento, Integer versione, Connection connection);
	
}
