/**
 * 
 */
package it.ibm.red.business.dao.impl;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IRegistroAusiliarioDAO;
import it.ibm.red.business.dto.AnagraficaDipendentiComponentDTO;
import it.ibm.red.business.dto.CapitoloSpesaMetadatoDTO;
import it.ibm.red.business.dto.LookupTableDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.RegistroDTO;
import it.ibm.red.business.enums.TipoDocumentoModeEnum;
import it.ibm.red.business.enums.TipoMetadatoEnum;
import it.ibm.red.business.enums.TipoRegistroAusiliarioEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.pcollector.ParametersCollector;
import it.ibm.red.business.pcollector.ParametersCollectorFactory;
import it.ibm.red.business.utils.StringUtils;

/**
 * DAO per la gestione dei registri ausiliari.
 * @author APerquoti
 * @author SimoneLungarella
 */
@Repository
public class RegistroAusiliarioDAO extends AbstractDAO implements IRegistroAusiliarioDAO {

	private static final String TRA_DESCRIZIONE = "TRA_DESCRIZIONE";

	private static final String REA_ID = "REA_ID";

	private static final String REA_NOME = "REA_NOME";

	private static final String TRA_ID = "TRA_ID";

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 6637041279931515871L;
	
	/**
	 * Messaggio errore caricamento registri.
	 */
	private static final String ERROR_CARICAMENTO_REGISTRI_MSG = "Errore durante il caricamento dei registri";
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RegistroAusiliarioDAO.class);

	/**
	 * Restituisce tutti i registri configurati.
	 * 
	 * @param connection
	 *            connessione al database
	 * @return lista dei registri recuperati
	 */
	@Override
	public Collection<RegistroDTO> getRegistri(final Connection connection) {
		final Collection<RegistroDTO> registri = new ArrayList<>();
		RegistroDTO registro;
		PreparedStatement ps = null;
		ResultSet rs = null;
		final String query = "SELECT * FROM REGISTRO_AUSILIARIO INNER JOIN TIPO_REGISTRO_AUSILIARIO ON REA_TRA_ID = TRA_ID";
		
		try {
			ps = connection.prepareStatement(query);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				registro = new RegistroDTO(rs.getInt(REA_ID), rs.getString(REA_NOME), rs.getInt(TRA_ID), rs.getString(TRA_DESCRIZIONE));
				registri.add(registro);
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(ERROR_CARICAMENTO_REGISTRI_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return registri;
	}


	/**
	 * Restituisce i registri del tipo specificato dal parametro <code> tipo </code>
	 * e che sia valido per la coppia tipologia documento / tipologia procedimento.
	 * 
	 * @see it.ibm.red.business.dao.IRegistroAusiliarioDAO#getRegistri(java.sql.Connection,
	 *      it.ibm.red.business.enums.TipoRegistroAusiliarioEnum,
	 *      java.lang.Integer).
	 * @param connection
	 *            connessione al database
	 * @param tipo
	 *            tipo registro da recuperare
	 * @param idTipologiaDocumento
	 *            identificativo tipologia documento
	 * @param idTipoProcedimento
	 *            identificativo tipologia procedimento
	 * @return lista dei registri del tipo specificato validi per il documento
	 */
	@Override
	public Collection<RegistroDTO> getRegistri(final Connection connection, final TipoRegistroAusiliarioEnum tipo, final Integer idTipologiaDocumento, final Integer idTipoProcedimento) {
		final Collection<RegistroDTO> registri = new ArrayList<>();
		RegistroDTO registro;
		PreparedStatement ps = null;
		ResultSet rs = null;
		final String query = "select r.REA_ID, r.REA_NOME, r.REA_DISPLAYNAME, tr.TRA_ID, tr.TRA_DESCRIZIONE from REGISTRO_AUSILIARIO r INNER JOIN TIPO_REGISTRO_AUSILIARIO tr ON tr.TRA_ID = r.REA_TRA_ID"
				+ " INNER JOIN PROCEDIMENTO_REG_AUS t ON t.REA_ID = r.REA_ID where tr.TRA_ID = ? AND t.IDTIPOLOGIADOCUMENTO = ? AND t.IDPROCEDIMENTO = ?";

		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, tipo.getCode());
			ps.setInt(2, idTipologiaDocumento);
			ps.setInt(3, idTipoProcedimento);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				registro = new RegistroDTO(rs.getInt(REA_ID), rs.getString(REA_NOME), rs.getInt(TRA_ID), rs.getString(TRA_DESCRIZIONE));
				registro.setDisplayName(rs.getString("REA_DISPLAYNAME"));
				registri.add(registro);
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(ERROR_CARICAMENTO_REGISTRI_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return registri;
	}
	
	/**
	 * Restituisce il template xslt associato al registro identificato dall'id:
	 * <code> idRegistro </code>.
	 * 
	 * @see it.ibm.red.business.dao.IRegistroAusiliarioDAO#createPdf(java.sql.Connection,
	 *      java.lang.Integer)
	 * @param connection
	 *            connession al database
	 * @param idRegistro
	 *            identificativo registro ausiliario
	 * @return byte array del template xslt
	 */
	@Override
	public byte[] getXSLT(final Connection connection, final Integer idRegistro) {
		byte[] xslt = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		final String query = "SELECT REA_XSLT FROM REGISTRO_AUSILIARIO WHERE REA_ID = ? ";

		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, idRegistro);
			rs = ps.executeQuery();
			
			if (rs.next()) {
				xslt = rs.getString("REA_XSLT").getBytes();
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(ERROR_CARICAMENTO_REGISTRI_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}

		return xslt;
	}

	/**
	 * Restituisce la classe @see ParametersCollector che definisce il collector per
	 * la creazione del template associato al registro ausiliario identificato dall:
	 * <code> idRegistro </code>.
	 * 
	 * @param connection
	 *            connession al database
	 * @param idRegistro
	 *            identificativo registro
	 * @return collector dei parametri per il popolamento del template ottenuto con
	 *         {@link #getXSLT(Connection, Integer)}
	 */
	@Override
	public ParametersCollector getCollector(final Connection connection, final Integer idRegistro) {
		byte[] byteCodeCollector = null;
		String fqnCollector = null;

		PreparedStatement ps = null;
		ResultSet rs = null;
		final String query = "SELECT REA_COLLECTOR_FQN, REA_COLLECTOR FROM REGISTRO_AUSILIARIO WHERE REA_ID = ?";
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, idRegistro);
			rs = ps.executeQuery();

			if (rs.next()) {

				final Blob collectorBlob = rs.getBlob("REA_COLLECTOR");
				if (collectorBlob != null) {
					byteCodeCollector = collectorBlob.getBytes(1, (int) collectorBlob.length());
				}
				fqnCollector = rs.getString("REA_COLLECTOR_FQN");
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException("Errore durante il caricamento della classe java per il registro ausiliario selezionato.", e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}

		return ParametersCollectorFactory.getParametersCollector(fqnCollector, byteCodeCollector);
	}

	/**
	 * Restituisce i metadati specifici del registro ausiliario identificato dall:
	 * <code> idRegistro </code>.
	 * 
	 * @param connection
	 *            connession al database
	 * @param idRegistro
	 *            identificativo registro ausiliario
	 * @return lista dei metadati estesi propri del template associato al registro
	 *         ausiliario
	 */
	@Override
	public Collection<MetadatoDTO> getMetadatiRegistro(final Connection connection, final Integer idRegistro) {
		final Collection<MetadatoDTO> out = new ArrayList<>();
		MetadatoDTO metadato = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		final String query = "SELECT * FROM METADATO_REGISTRO_AUSILIARIO WHERE MRA_REA_ID = ?";
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, idRegistro);
			rs = ps.executeQuery();

			while (rs.next()) {

				final TipoMetadatoEnum tipo = TipoMetadatoEnum.get(rs.getString("MRA_TIPO"));

				if (TipoMetadatoEnum.CAPITOLI_SELECTOR.equals(tipo)) {
					metadato = new CapitoloSpesaMetadatoDTO();

				} else if (TipoMetadatoEnum.LOOKUP_TABLE.equals(tipo)) {
					metadato = new LookupTableDTO();
				} else if (TipoMetadatoEnum.PERSONE_SELECTOR.equals(tipo)) {
					metadato = new AnagraficaDipendentiComponentDTO();
				} else {
					metadato = new MetadatoDTO();
					metadato.setDimension(rs.getInt("MRA_DIMENSIONE"));
				}

				metadato.setName(rs.getString("MRA_NOME"));
				metadato.setDisplayName(rs.getString("MRA_DISPLAYNAME"));
				metadato.setObligatoriness(TipoDocumentoModeEnum.get(rs.getString("OBLIGATORINESS")));
				metadato.setEditability(TipoDocumentoModeEnum.get(rs.getString("EDITABILITY")));
				metadato.setVisibility(TipoDocumentoModeEnum.get(rs.getString("VISIBILITY")));
				metadato.setType(tipo);

				out.add(metadato);
			}

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
		} finally {
			closeStatement(ps, rs);
		}
		return out;
	}
	
	
	/**
	 * Restituisce i registri ausiliari validi sui documento identificati dalla
	 * tipologia documento.
	 * 
	 * @see it.ibm.red.business.dao.IRegistroAusiliarioDAO#getRegistriByTipologieDocumento(java.sql.Connection,
	 *      java.util.List).
	 * @param connection
	 *            connession al database
	 * @param idsTipologieDocumento
	 *            id delle tipologia documento
	 * @return lista dei registri ausiliari
	 */
	@Override
	public Collection<RegistroDTO> getRegistriByTipologieDocumento(final Connection connection, final Collection<Integer> idsTipologieDocumento) {
		final Collection<RegistroDTO> registri = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = connection.prepareStatement("SELECT DISTINCT rea.REA_ID, rea.REA_NOME FROM REGISTRO_AUSILIARIO rea INNER JOIN PROCEDIMENTO_REG_AUS pra ON rea.rea_id = pra.rea_id"
					+ " INNER JOIN TIPOLOGIADOCUMENTOPROCEDIMENTO tdp ON tdp.idtipologiadocumento = pra.idtipologiadocumento"
					+ " WHERE " + StringUtils.createInCondition("tdp.idtipologiadocumento", idsTipologieDocumento.iterator(), false)
					+ " AND pra.idprocedimento = tdp.idtipoprocedimento");
			
			rs = ps.executeQuery();
			
			RegistroDTO registro;
			while (rs.next()) {
				registro = new RegistroDTO(rs.getInt(REA_ID), rs.getString(REA_NOME), null, null);
				
				registri.add(registro);
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(ERROR_CARICAMENTO_REGISTRI_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return registri;
	}

	/**
	 * Esegue il salvataggio della relazion tra procedimento identificato dal
	 * <code> idProcedimento </code>, dal documento identificato dal
	 * <code> idDocumento </code> e dal registro ausiliario identificato dal
	 * <code> idRegistro </code>.
	 * 
	 * @param connection
	 *            connession al database
	 * @param idProcedimento
	 *            identificativo tipo procedimento
	 * @param idDocumento
	 *            identificativo tipologia documento
	 * @param idRegistro
	 *            identificativo registro ausiliario
	 */
	@Override
	public void saveCrossProcedimentoRegistro(final Connection connection, final Long idProcedimento, final int idDocumento, final int idRegistro) {
		// Salvataggio della relazione tra registro e tipologia procedimento - PROCEDIMENTO_REG_AUS
		PreparedStatement ps = null;
		int index = 1;
		final String queryCrossRegistro = "INSERT INTO PROCEDIMENTO_REG_AUS(IDPROCEDIMENTO,IDTIPOLOGIADOCUMENTO, REA_ID) VALUES(?,?,?)";
		try {
			ps = connection.prepareStatement(queryCrossRegistro);
			ps.setLong(index++, idProcedimento);
			ps.setInt(index++, idDocumento);
			ps.setInt(index, idRegistro);
			ps.executeUpdate();
		} catch (final Exception e) {
			throw new RedException("Errore durante il salvataggio della relazione tra registro e tipologia procedimento", e);
		} finally {
			closeStatement(ps);
		}
			
	}

	/**
	 * Restituisce il registro identificato dal codice:
	 * <code> codiceRegistro </code>.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param codiceRegistro
	 *            codice registro ausiliario
	 * @return registro ausiliario
	 */
	@Override
	public RegistroDTO getRegistro(final Connection connection, final String codiceRegistro) {
		RegistroDTO registro = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		final String query = "SELECT * FROM REGISTRO_AUSILIARIO INNER JOIN TIPO_REGISTRO_AUSILIARIO ON REA_TRA_ID = TRA_ID WHERE REA_NOME = ?";
		
		try {
			ps = connection.prepareStatement(query);
			ps.setString(1, codiceRegistro);
			rs = ps.executeQuery();
			
			if (rs.next()) {
				registro = new RegistroDTO(rs.getInt(REA_ID), rs.getString(REA_NOME), rs.getInt(TRA_ID), rs.getString(TRA_DESCRIZIONE));
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException("Errore durante il caricamento del registro", e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return registro;
	}

	/**
	 * Restituisce il registro identificato dall' identificativo univoco:
	 * <code> idRegistro </code>.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idRegistro
	 *            identificativo registro ausiliario
	 * @return registro ausiliario
	 */
	@Override
	public RegistroDTO getRegistro(final Connection connection, final Integer idRegistro) {
		RegistroDTO registro = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		final String query = "SELECT * FROM REGISTRO_AUSILIARIO INNER JOIN TIPO_REGISTRO_AUSILIARIO ON REA_TRA_ID = TRA_ID WHERE REA_ID = ?";
		
		try {
			ps = connection.prepareStatement(query);
			ps.setInt(1, idRegistro);
			rs = ps.executeQuery();
			
			if (rs.next()) {
				registro = new RegistroDTO(rs.getInt(REA_ID), rs.getString(REA_NOME), rs.getInt(TRA_ID), rs.getString(TRA_DESCRIZIONE));
			}
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException("Errore durante il caricamento del registro", e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return registro;
	}
	
}
