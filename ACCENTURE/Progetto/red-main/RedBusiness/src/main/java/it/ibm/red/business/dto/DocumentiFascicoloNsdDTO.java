/**
 * 
 */
package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.List;

/**
 * @author VINGENITO
 *
 */
public class DocumentiFascicoloNsdDTO extends AbstractDTO implements Serializable {

	
	private static final long serialVersionUID = -5542630008125478362L;
	
	/**
	 * Identificativo.
	 */
	private String iChronicleId;
	
	/**
	 * Id folder.
	 */
	private String idFolderId;
	
	/**
	 * Lista cartelle.
	 */
	private List<DocumentiFascicoloNsdDTO> listaCartelle;
	
	/**
	 * Lista documenti.
	 */
	private List<DocumentiFascicoloNsdDTO> listaDocumenti;
	 
	/**
	 * Nome cartella.
	 */
	private String nomeCartella;

	/**
	 * Classe documentale.
	 */
	private String classeDocumentale;
	
	/**
	 * Descrizione.
	 */
	private String descrizione;
	
	/**
	 * Nome.
	 */
	private String nome;
	
	/**
	 * Segnatura.
	 */
	private String segnatura;
	
	/**
	 * Flag fascicolo.
	 */
	private boolean fascicolo;

	/**
	 * @return iChronicleId
	 */
	public String getiChronicleId() {
		return iChronicleId;
	}

	/**
	 * @param iChronicleId
	 */
	public void setiChronicleId(final String iChronicleId) {
		this.iChronicleId = iChronicleId;
	}

	/**
	 * Restituisce l'id della folder.
	 * @return idFolderId
	 */
	public String getIdFolderId() {
		return idFolderId;
	}

	/**
	 * Imposta l'id.
	 * @param idFolderId
	 */
	public void setIdFolderId(final String idFolderId) {
		this.idFolderId = idFolderId;
	}

	/**
	 * Restituisce la lista delle cartelle.
	 * @return List di DocumentiFascicoloNsdDTO
	 */
	public List<DocumentiFascicoloNsdDTO> getListaCartelle() {
		return listaCartelle;
	}

	/**
	 * Imposta la lista delle cartelle.
	 * @param listaCartelle
	 */
	public void setListaCartelle(final List<DocumentiFascicoloNsdDTO> listaCartelle) {
		this.listaCartelle = listaCartelle;
	}

	/**
	 * Restituisce la lista dei documenti.
	 * @return lista dei documenti
	 */
	public List<DocumentiFascicoloNsdDTO> getListaDocumenti() {
		return listaDocumenti;
	}

	/**
	 * Imposta la lista dei documenti.
	 * @param listaDocumenti
	 */
	public void setListaDocumenti(final List<DocumentiFascicoloNsdDTO> listaDocumenti) {
		this.listaDocumenti = listaDocumenti;
	}

	/**
	 * Restituisce il nome della cartella.
	 * @return nome cartella
	 */
	public String getNomeCartella() {
		return nomeCartella;
	}

	/**
	 * Imposta il nome della cartella.
	 * @param nomeCartella
	 */
	public void setNomeCartella(final String nomeCartella) {
		this.nomeCartella = nomeCartella;
	}

	/**
	 * Restituisce la classe documentale.
	 * @return classe documentale
	 */
	public String getClasseDocumentale() {
		return classeDocumentale;
	}

	/**
	 * Imposta la classe documentale.
	 * @param classeDocumentale
	 */
	public void setClasseDocumentale(final String classeDocumentale) {
		this.classeDocumentale = classeDocumentale;
	}

	/**
	 * Restituisce la descrizione.
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Imposta la descrizione.
	 * @param descrizione
	 */
	public void setDescrizione(final String descrizione) {
		this.descrizione = descrizione;
	}

	/**
	 * Restituisce il nome.
	 * @return nome fascicolo Nsd
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Imposta il nome.
	 * @param nome
	 */
	public void setNome(final String nome) {
		this.nome = nome;
	}

	/**
	 * Restituisce la segnatura.
	 * @return segnatura
	 */
	public String getSegnatura() {
		return segnatura;
	}

	/**
	 * Imposta la segnatura.
	 * @param segnatura
	 */
	public void setSegnatura(final String segnatura) {
		this.segnatura = segnatura;
	}

	/**
	 * Restituisce true se fascicolo, false altrimenti.
	 * @return
	 */
	public boolean isFascicolo() {
		return fascicolo;
	}

	/**
	 * Imposta il flag: fascicolo.
	 * @param fascicolo
	 */
	public void setFascicolo(final boolean fascicolo) {
		this.fascicolo = fascicolo;
	}

}
