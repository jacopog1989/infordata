package it.ibm.red.business.service.sign.strategy;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.stereotype.Service;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Questo service gestisce tutte le funzionalita strettamente collegate alla strategia di firma B. @see SignStrategyEnum.
 * La strategia A differisce dalla strategia B in quanto la protocollazione avviene in maniera asincrona.
 */
@Service
public class BAsyncSignStrategy extends AbstractSignStrategy {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 3938228451930686715L;

	/**
	 * Esegue le operazioni precedenti alla firma dei contents identificati dai
	 * wobnumbers: <code> wobNumbersToProcess </code>.
	 * 
	 * @param signTransactionId
	 *            transactionId dell'operazione di firma
	 * @param wobNumbersToProcess
	 *            identificativi documenti da firmare
	 * @param utenteFirmatario
	 *            utente responsabile della firma
	 * @return collection degli esiti associati alle firme
	 */
	@Override
	protected final Collection<EsitoOperazioneDTO> operazioniPreFirmaContents(final String signTransactionId, final Collection<String> wobNumbers, final UtenteDTO utenteFirmatario) {
		// Questa strategia esegue in modalità asincrona tutti gli step successivi alla firma dei content, quindi non ci sono operazioni propedeutiche alla firma dei content
		Collection<EsitoOperazioneDTO> signOutcomes = new ArrayList<>();
		
		EsitoOperazioneDTO esitoOk;
		for (String wobNumber : wobNumbers) {
			esitoOk = new EsitoOperazioneDTO(wobNumber);
			esitoOk.setEsito(true);
			signOutcomes.add(esitoOk);
		}
		
		return signOutcomes;
	}
	
}
