/**
 * 
 */
package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.FadAmministrazioneDTO;
import it.ibm.red.business.dto.FadRagioneriaDTO;


/**
 * @author APerquoti
 *
 */
public interface IFadDAO  extends Serializable {
	
	/**
	 * 
	 * @param con
	 * @param codiceRagioneria
	 * @return
	 */
	FadRagioneriaDTO getRagioneriaByCodiceRagioneria(String codiceRagioneria, Connection con);
	
	/**
	 * 
	 * @param con
	 * @param codiceAmministrazione
	 * @return
	 */
	FadAmministrazioneDTO getAmministrazioneByCodiceAmministrazione(String codiceAmministrazione, Connection con);

	/**
	 * @param con
	 * @return
	 */
	List<FadAmministrazioneDTO> getAmministrazioni(Connection con);
	
	/**
	 * @param codiceAmministrazione
	 * @param con
	 * @return
	 */
	FadRagioneriaDTO findRagioneriaByCodiceAmministrazione(String codiceAmministrazione, Connection con);

}
