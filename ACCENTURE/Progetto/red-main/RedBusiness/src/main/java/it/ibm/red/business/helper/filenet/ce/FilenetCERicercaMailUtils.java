package it.ibm.red.business.helper.filenet.ce;
 
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.StatoMailEnum;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.utils.DateUtils;

/**
 * Utility FilenetCE ricerca.
 */
public class FilenetCERicercaMailUtils {
	
	private static final String AND = " AND ";

	/**
	 * Costruttore da non utilizzare.
	 */
	private FilenetCERicercaMailUtils() {
		throw new UnsupportedOperationException("Le classi statiche non possono essere istanziate"); 
	}
	
	/**
	 * Genera la lista di filtri a partire dall'input ritorna la stringa in cui i filtri sono separati da AND.
	 * 
	 * @param registroRicservato
	 *  Boolean per la scelta dei registri riservati.
	 * @param searchString
	 *  stringa ricercata nell'oggetto e nel mittente dell'email case unsensitive.
	 * @param dataDa
	 *  Data dalla quale sono ricercati le mail.
	 * @param dataA
	 *  Data fino alla quale sono ricercati le mail.
	 * @param alias
	 *  alias della tabella.
	 * @param oggettoRicerca
	 *  Oggetto dell'email
	 * @param mittenteRicerca
	 *  Mittente dell'email
	 * @param numeroProtocolloRicerca
	 *  Numero protocollo generato dalla protocollazione dell'email
	 * @param pp
	 *  Oggetto da cui vengono recuperate le properities
	 * @return
	 * 	Ritorna una lista di condizioni separate in AND
	 */
	public static String generateSearchFilter(final boolean registroRicservato,final Date dataDa, final Date dataA, final List<StatoMailEnum> selectedStatList, 
			final String alias, final String oggettoRicerca, final String mittenteRicerca, final String numeroProtocolloRicerca, final PropertiesProvider pp) {
		final List<String> filter = new ArrayList<>();
	  
		if (StringUtils.isNotBlank(oggettoRicerca)) {
			final String str = it.ibm.red.business.utils.StringUtils.duplicaApici(oggettoRicerca);
			filter.add(StringUtils.join(" (UPPER(",alias(alias), pp.getParameterByKey(PropertiesNameEnum.OGGETTO_EMAIL_METAKEY), ") LIKE UPPER('%", str, "%'))"));
		}
		
		if (StringUtils.isNotBlank(mittenteRicerca)) {
			final String str = it.ibm.red.business.utils.StringUtils.duplicaApici(mittenteRicerca);
			filter.add(StringUtils.join("(UPPER(", alias(alias), "[", pp.getParameterByKey(PropertiesNameEnum.FROM_MAIL_METAKEY), "]) LIKE UPPER('%", str, "%'))"));
		}
		
		if (StringUtils.isNotBlank(numeroProtocolloRicerca)) {
			final String str = it.ibm.red.business.utils.StringUtils.duplicaApici(numeroProtocolloRicerca);
			filter.add(StringUtils.join("(UPPER(", alias(alias), "[", pp.getParameterByKey(PropertiesNameEnum.PROTOCOLLI_GENERATI_MAIL_METAKEY), "]) LIKE UPPER('%", str, "%'))"));
		} 
		
		return simpleSearchFilter(registroRicservato, dataDa, dataA, selectedStatList, alias, pp, filter);
	}
	
	/**
	 * Genera il search filter con numeroProtocolloRicerca = null.
	 * Chiama {@link #generateSearchFilter(boolean, Date, Date, List, String, String, String, String, PropertiesProvider)}.
	 * @param registroRicservato
	 * @param dataDa
	 * @param dataA
	 * @param selectedStatList
	 * @param alias
	 * @param oggettoRicerca
	 * @param mittenteRicerca
	 * @param pp
	 * @return searchFilter
	 */
	public static String generateSearchFilter(final boolean registroRicservato,final Date dataDa, final Date dataA, final List<StatoMailEnum> selectedStatList, 
			final String alias, final String oggettoRicerca, final String mittenteRicerca, final PropertiesProvider pp) {
		return generateSearchFilter(registroRicservato, dataDa, dataA, selectedStatList, alias, oggettoRicerca, mittenteRicerca, null, pp);
	}
	
	/**
	 * Genera la lista di filtri a partire dall'input ritorna la stringa in cui i filtri sono separati da AND.
	 * 
	 * @param registroRicservato
	 *  Boolean per la scelta dei registri riservati.
	 * @param searchString
	 *  Stringa da inserire nel full text.
	 * @param dataDa
	 *  Data dalla quale sono ricercati le mail.
	 * @param dataA
	 *  Data fino alla quale sono ricercati le mail.
	 * @param alias
	 *  alias della tabella.
	 * @param pp
	 *  Oggetto da cui vengono recuperate le properities
	 * @return
	 * 	Ritorna una lista di condizioni separate in AND
	 */ 
	public static String generateFullTextSearchFilter(final boolean registroRicservato, final String oggettoRicerca,final String mittenteRicerca, 
			final Date dataDa, final Date dataA, final List<StatoMailEnum> selectedStatList, final String numeroProtocolloRicerca, final String alias, final PropertiesProvider pp) {
		final List<String> filter = new ArrayList<>();
		 
		String fullTextContains = "";  
		
		//FULL TEXT PER OGGETTO
		if (StringUtils.isNotBlank(oggettoRicerca)) {
			final String[] tokens = oggettoRicerca.split(" ");
			List<String> tokenList = Arrays.asList(tokens);
			
			tokenList = tokenList.stream()
			.map(it.ibm.red.business.utils.StringUtils::duplicaApici)
			.collect(Collectors.toList());
			
			final String oggettoRicercaCondition = String.join(AND, tokenList); 
			fullTextContains = oggettoRicercaCondition;
		}
		//FULL TEXT PER MITTENTE
		if (StringUtils.isNotBlank(mittenteRicerca)) {
			final String[] tokens = mittenteRicerca.split(" ");
			List<String> tokenList = Arrays.asList(tokens);
			
			tokenList = tokenList.stream()
			.map(it.ibm.red.business.utils.StringUtils::duplicaApici)
			.collect(Collectors.toList());
			
			final String mittenteRicercaCondition = String.join(AND, tokenList);
			if (StringUtils.isNotBlank(fullTextContains)) {
				fullTextContains += AND + mittenteRicercaCondition;
			} else {
				fullTextContains = mittenteRicercaCondition;
			}
		}
		
		//FULL TEXT PER NUMERO PROTOCOLLO
		if (StringUtils.isNotBlank(numeroProtocolloRicerca)) {
			final String[] tokens = numeroProtocolloRicerca.split(" ");
			List<String> tokenList = Arrays.asList(tokens);
			
			tokenList = tokenList.stream()
			.map(it.ibm.red.business.utils.StringUtils::duplicaApici)
			.collect(Collectors.toList());
			
			final String mittenteRicercaCondition = String.join(AND, tokenList);
			if (StringUtils.isNotBlank(fullTextContains)) {
				fullTextContains += AND + mittenteRicercaCondition;
			} else {
				fullTextContains = mittenteRicercaCondition;
			}
		}
		 
		final String fullTextCondition = StringUtils.join(" CONTAINS(", alias(alias), "*, '", String.join(AND, fullTextContains),"')");
		filter.add(fullTextCondition);
		return simpleSearchFilter(registroRicservato, dataDa, dataA, selectedStatList, alias, pp, filter);
	}

	private static String simpleSearchFilter(final boolean registroRiservato, final Date dataDa, final Date dataA, final List<StatoMailEnum> selectedStatList,
			final String alias, final PropertiesProvider pp, List<String> filter) {
		generateRegistroRiservatoFilter(registroRiservato, filter);
		generateDateFilter(dataDa, dataA, alias, pp, filter);
		if (selectedStatList !=null && !selectedStatList.isEmpty()) {
			filter.add(FilenetCERicercaMailUtils.generateFilterByStato(selectedStatList, alias, pp));
		}
		
		filter = filter.stream()
		.filter(StringUtils::isNotBlank)
		.collect(Collectors.toList());
		
		return String.join(AND, filter);
	}

	private static void generateRegistroRiservatoFilter(final boolean registroRicservato, final List<String> filter) {
		// ( d.registroRiservato is null OR d.registroRiservato = 1 oppure 0 )
		final StringBuilder condition = new StringBuilder();
		condition.append("( d.registroRiservato is null OR d.registroRiservato = ");
		if (registroRicservato) {
			condition.append("1");	
		} else {
			condition.append("0");
		}
		condition.append(" )");
		filter.add(condition.toString());
	}

	private static void generateDateFilter(final Date dataDa, final Date dataA, final String alias,	final PropertiesProvider pp, final List<String> filter) {
		if (dataDa != null || dataA != null) {

			if (dataDa != null) {
				final String dataDaText = DateUtils.buildFilenetDataForSearch(dataDa, true);
				filter.add(StringUtils.join(
					" (",
						"(", alias(alias), pp.getParameterByKey(PropertiesNameEnum.DATA_INVIO_MAIL_METAKEY), " IS NULL AND ",
						alias(alias), pp.getParameterByKey(PropertiesNameEnum.DATA_RICEZIONE_MAIL_METAKEY), " >= ", dataDaText, ") ",
						"OR (d.", pp.getParameterByKey(PropertiesNameEnum.DATA_INVIO_MAIL_METAKEY), " IS NOT NULL AND ",
						alias(alias), pp.getParameterByKey(PropertiesNameEnum.DATA_INVIO_MAIL_METAKEY), " >= ", dataDaText, ")",
					 ")")
				);
			}
			
			if (dataA != null) {
				final String dataAText = DateUtils.buildFilenetDataForSearch(dataA, false);
				filter.add(StringUtils.join(" ((", alias(alias), pp.getParameterByKey(PropertiesNameEnum.DATA_INVIO_MAIL_METAKEY), " IS NULL AND ",
					alias(alias), pp.getParameterByKey(PropertiesNameEnum.DATA_RICEZIONE_MAIL_METAKEY), " <= ", dataAText, ") ",
					" OR (", alias(alias), pp.getParameterByKey(PropertiesNameEnum.DATA_INVIO_MAIL_METAKEY), " IS NOT NULL AND ",
					alias(alias), pp.getParameterByKey(PropertiesNameEnum.DATA_INVIO_MAIL_METAKEY), " <= ", dataAText, "))"
				));
			}
		}
	}
	
	private static String alias(final String alias) {
		return StringUtils.isNotBlank(alias) ? StringUtils.join(" ", alias,".") : "";
	}
	
	/**
	 * Genera la condizione per cui ricerca tutte le email contenute nell'array validState.
	 * @param validState
	 *  lista degli stati da porre in OR
	 * @return
	 *  la condizione ' ( stati in OR )'
	 */
	public static String generateFilterByStato(final List<StatoMailEnum> validState, final String alias, final PropertiesProvider pp) {
		final StringBuilder res = new StringBuilder();
		res.append(" (");
		
		final List<String> stateCondition = validState.stream()
			.map(state -> StringUtils.join(alias(alias), pp.getParameterByKey(PropertiesNameEnum.STATO_MAIL_METAKEY), " = ", state.getStatus().toString()))
			.collect(Collectors.toList());
		res.append(String.join(" OR ", stateCondition));
		
		res.append(") ");
		
		return res.toString();
	}
	
	/**
	 * Genera la lista di colonne separate da virgole come nel select.
	 * 
	 * @param alias
	 *            alias da anteporre alle colonne.
	 * @param columns
	 *            Lista di colonne se nullo viene usato l'asterisco.
	 * @return alias.column1 , alias.column2 , ... oppure alias.*
	 */
	public static String generateProjection(final String alias, final List<String> columns) {
		if (CollectionUtils.isEmpty(columns)) {
			return StringUtils.join(alias(alias), "*");
		}
		
		List<String> withAlias = columns;
		if (StringUtils.isNotBlank(alias)) {
			withAlias = columns.stream()
				.map(c -> StringUtils.join(alias(alias), c))
				.collect(Collectors.toList());
		}
		
		return String.join(", ", withAlias);
	}
	
	/**
	 * @param onlyFirst
	 * @param projection
	 * @param path
	 * @param documentClass
	 * @param alias
	 * @return query
	 */
	public static String generateBody(final boolean onlyFirst, final String projection, final String path, final String documentClass, final String alias) {
		final StringBuilder queryString = new StringBuilder();
		String projectionFirst = "";
		if (onlyFirst) {
            projectionFirst = "TOP 1";
        } else {
            projectionFirst = "TOP " + PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS);
        }

		queryString.append("SELECT " + projectionFirst + " " + projection);
		queryString.append(" FROM");
		queryString.append(" " + documentClass + " " + alias + " INNER JOIN");
		queryString.append(" ReferentialContainmentRelationship r ON " + alias + ".This = r.Head");
		queryString.append(" WHERE");
		queryString.append(" r.Tail = OBJECT('" + path + "') ");
		
		return queryString.toString();
	}
	
	/**
	 * @param onlyFirst
	 * @param projection
	 * @param pathRecenti
	 * @param pathArchiviate
	 * @param documentClass
	 * @param alias
	 * @return query di ricerca
	 */
	public static String generateBodyRicerca(final boolean onlyFirst, final String projection, final String pathRecenti, final String pathArchiviate, 
			final String documentClass, final String alias) {
		final StringBuilder queryString = new StringBuilder();
		
		String projectionFirst = "";
		if (onlyFirst) {
            projectionFirst = "TOP 1";
        } else {
            projectionFirst = "TOP " + PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.RICERCA_MAX_RESULTS);
        }

		queryString.append("SELECT " + projectionFirst + " " + projection);
		queryString.append(" FROM");
		queryString.append(" " + documentClass + " " + alias + " INNER JOIN");
		queryString.append(" ReferentialContainmentRelationship r ON " + alias + ".This = r.Head");
		queryString.append(" WHERE (");
		
		List<String> filter = new ArrayList<>();
		if (!StringUtils.isEmpty(pathRecenti)) {
			filter.add("r.Tail = OBJECT('" + pathRecenti + "')");
		}
		if (!StringUtils.isEmpty(pathArchiviate)) {
			filter.add("r.Tail = OBJECT('" + pathArchiviate + "')");
		}
		
		filter = filter.stream()
		.filter(StringUtils::isNotBlank)
		.collect(Collectors.toList());
		
		queryString.append(String.join(" OR ", filter));
		queryString.append(")");
		
		return queryString.toString();
	}  
}
