package it.ibm.red.business.dao.impl;

import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IPkHandlerDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.PkHandler;

/**
 * Dao per la gestione degli handler PK.
 *
 * @author m.crescentini
 */
@Repository
public class PkHanlderDAO extends AbstractDAO implements IPkHandlerDAO {
	
	private static final String HANDLER = "HANDLER";

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -4098659861630393783L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(PkHanlderDAO.class.getName());
	
	/**
	 * @see it.ibm.red.business.dao.IPkHandlerDAO#getById(int, java.sql.Connection).
	 */
	@Override
	public final PkHandler getById(final int id, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		PkHandler pkHandler = null;
		
		try {
			String sql = " SELECT p.* FROM pk_handler p WHERE p.id = ?";
			ps = con.prepareStatement(sql);
			ps.setInt(1, id);
			
			rs = ps.executeQuery();
			
			if (rs.next()) {
				pkHandler = new PkHandler(rs.getInt("ID"), rs.getString(HANDLER), rs.getBytes("SECURE_PIN"));
			}
		} catch (SQLException e) {
			LOGGER.error("Errore nel recupero dell'handler PK con ID: " + id, e);
			throw new RedException("Errore nel recupero dell'handler PK con ID: " + id, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return pkHandler;
	}
	
	
	/* (non-Javadoc)
	 * @see it.ibm.red.business.dao.IPkHandlerDAO#getByHandler(java.lang.String, java.sql.Connection)
	 */
	@Override
	public final PkHandler getByHandler(final String url, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		PkHandler pkHandler = null;
		
		try {
			String sql = "SELECT * FROM pk_handler WHERE handler = ?";
			ps = con.prepareStatement(sql);
			ps.setString(1, url);
			
			rs = ps.executeQuery();
			
			if (rs.next()) {
				pkHandler = new PkHandler();
				populate(pkHandler, rs);
			}
		} catch (SQLException e) {
			LOGGER.error("Errore nel recupero dell'handler PK con URL: " + url, e);
			throw new RedException("Errore nel recupero dell'handler PK con URL: " + url, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return pkHandler;
	}
	

	/**
	 * @see it.ibm.red.business.dao.IPkHandlerDAO#getAllHandlers(java.sql.Connection).
	 */
	@Override
	public List<String> getAllHandlers(final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<String> handlerList = new ArrayList<>();
		
		try {
			String sql = "SELECT DISTINCT handler FROM pk_handler";
			ps = con.prepareStatement(sql);
			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				handlerList.add(rs.getString(HANDLER));
			}
		} catch (SQLException e) {
			LOGGER.error("Errore nel recupero degli handler PK.", e);
			throw new RedException("Errore nel recupero degli handler PK.", e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return handlerList;
	}
	
	
	/**
	 * Popola una singola entity.
	 * 
	 * @param pkHandler
	 *            Nuova entity da popolare
	 * @param rs
	 *            ResultSet
	 * @throws SQLException
	 */
	private static void populate(final PkHandler pkHandler, final ResultSet rs) throws SQLException {
		pkHandler.setId(rs.getInt("ID"));
		pkHandler.setHandler(rs.getString(HANDLER));
		Blob pkSecurePinBlob = rs.getBlob("SECURE_PIN");
		if (pkSecurePinBlob != null) {
			byte[] pkSecurePin = pkSecurePinBlob.getBytes(1, (int) pkSecurePinBlob.length());
			pkHandler.setSecurePin(pkSecurePin);
		}
	}
}