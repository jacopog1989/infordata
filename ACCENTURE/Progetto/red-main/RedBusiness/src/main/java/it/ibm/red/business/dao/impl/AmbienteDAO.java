package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import it.ibm.red.business.dao.IAmbienteDAO;
import it.ibm.red.business.logger.REDLogger;

/**
 * DAO ambiente.
 */
public class AmbienteDAO implements IAmbienteDAO {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AmbienteDAO.class);

	/**
	 * Select recupero ambiente.
	 */
	private static final String SQL_GET_AMBIENTE = "SELECT * FROM AMBIENTE WHERE ROWNUM=1";

	/**
	 * @see it.ibm.red.business.dao.IAmbienteDAO#getAmbiente(java.sql.Connection).
	 */
	@Override
	public String getAmbiente(final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		String ambiente = "";
		try {
			ps = con.prepareStatement(SQL_GET_AMBIENTE);

			rs = ps.executeQuery();
			if (rs.next()) {
				ambiente = rs.getString("TIPO");
			}
			return ambiente;
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero del tipo di ambiente." + e.getMessage(), e);
		} finally {
			if (rs != null) {
				try {
					rs.close();
				} catch (final SQLException e) {
					LOGGER.error("Errore nella chiusura del ResultSet.", e);
				}
			}
			
			if (ps != null) {
				try {
					ps.close();
				} catch (final SQLException e) {
					LOGGER.error("Errore nella chiusura del PreparedStatement.", e);
				}
			}

		}
		return ambiente;
	}	
}
