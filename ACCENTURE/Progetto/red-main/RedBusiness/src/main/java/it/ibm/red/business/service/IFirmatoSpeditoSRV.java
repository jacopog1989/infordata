package it.ibm.red.business.service;

import it.ibm.red.business.service.facade.IFirmatoSpeditoFacadeSRV;

/**
 * Interface del servizio di gestione firmati spediti.
 */
public interface IFirmatoSpeditoSRV extends IFirmatoSpeditoFacadeSRV {

}
