package it.ibm.red.business.service.asign.step.impl;

import java.sql.Connection;
import java.util.Map;

import org.springframework.stereotype.Service;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.ASignStepResultDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.asign.step.ASignStepAbstract;
import it.ibm.red.business.service.asign.step.ContextASignEnum;
import it.ibm.red.business.service.asign.step.StepEnum;

/**
 * Step di rigenerazione content registrazione ausiliaria - firma asincrona.
 * 
 * @author SimoneLungarella
 */
@Service
public class RegAuxRigenerazioneContentStep extends ASignStepAbstract {

	/**
	 * Serial version.
	 */
	private static final long serialVersionUID = -1459996120714620349L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RegAuxRigenerazioneContentStep.class.getName());
	
	/**
	 * Esegue la rigenerazione del content per un item di firma asincrona soggetto a registrazione ausiliaria.
	 * @param in
	 * @param item
	 * @param context
	 * @return risultato dell'esecuzione dello step
	 */
	@Override
	protected ASignStepResultDTO run(Connection con, ASignItemDTO item, Map<ContextASignEnum, Object> context) {
		Long idItem = item.getId();
		LOGGER.info("run -> START [ID item: " + idItem + "]");
		ASignStepResultDTO out = new ASignStepResultDTO(idItem, getStep());
		
		// Si recupera l'utente firmatario
		UtenteDTO utenteFirmatario = getUtenteFirmatario(item, con);
		
		// Si recupera il workflow
		VWWorkObject wob = getWorkflow(item, out, utenteFirmatario);
		
		if (wob != null) {
			// ### REGISTRAZIONE AUSILIARIA - FASE RIGENERAZIONE CONTENT ###
			EsitoOperazioneDTO esitoRegAus = signSRV.rigeneraContentRegAux(aSignSRV.getSignTransactionId(utenteFirmatario, "Batch"), wob, utenteFirmatario, con);
			
			if (esitoRegAus.isEsito()) {
				out.setStatus(true);
				out.appendMessage("[OK] Rigenerazione content registrazione ausiliaria eseguito");
			} else {
				throw new RedException("[KO] Errore nello step di rigenerazione content registrazione ausiliaria: " + esitoRegAus.getNote() 
					+ ". [Codice errore: " + esitoRegAus.getCodiceErrore() + "]");
			}
		}
		
		LOGGER.info("run -> END [ID item: " + idItem + "]");
		return out;
	}

	/**
	 * Restituisce lo @see StepEnum associato alla classe.
	 * 
	 * @return StepEnum
	 */
	@Override
	protected StepEnum getStep() {
		return StepEnum.REG_AUX_RIGENERAZIONE_CONTENT;
	}

	/**
	 * @see it.ibm.red.business.service.asign.step.ASignStepAbstract#getErrorMessage().
	 */
	@Override
	protected String getErrorMessage() {
		return "[KO] Errore imprevisto nello step di rigenerazione content registrazione ausiliaria";
	}
}
