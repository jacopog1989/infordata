package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dto.AssegnatarioOrganigrammaWFAttivoDTO;
import it.ibm.red.business.dto.DocumentoFascicoloDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.dto.filenet.StoricoDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.enums.FilenetStatoFascicoloEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.assegnazioni.AssegnazioniEntrataHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.nps.builder.Builder;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAttiSRV;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.INpsSRV;
import it.ibm.red.business.service.ISecurityCambioIterSRV;
import it.ibm.red.business.service.ISecuritySRV;
import it.ibm.red.business.service.IStoricoSRV;
import it.ibm.red.business.service.facade.IFepaFacadeSRV;

/**
 * Service gestione ATTI.
 */
@Service
public class AttiSRV extends AbstractService implements IAttiSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -8020677603411323955L;

	/**
	 * Messaggio di errore messa agli atti.
	 */
	private static final String ERROR_MESSA_AGLI_ATTI_MSG = "Si è verificato un errore nella messa agli atti.";
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(AttiSRV.class.getName());
	
	/**
	 * Property provider.
	 */
	private PropertiesProvider pp;
	
	/**
	 * Service gestione security.
	 */
	@Autowired
	private ISecuritySRV securitySRV;
	
	/**
	 * Service cambio iter.
	 */
	@Autowired
	private ISecurityCambioIterSRV securityCambioIterSRV;
	
	/**
	 * Service gestione fascicolo.
	 */
	@Autowired
	private IFascicoloSRV fascicoloSRV;
	
	/**
	 * Service fepa.
	 */
	@Autowired
	private IFepaFacadeSRV fepaSRV;
	
	/**
	 * Service nps.
	 */
	@Autowired
	private INpsSRV npsSRV;
	
	/**
	 * Service storico.
	 */
	@Autowired
	private IStoricoSRV storicoSRV;
	
	/**
	 * Service code documenti.
	 */
	@Autowired
	private CodeDocumentiSRV codeDocSRV;

	/**
	 * Definisce le azioni da eseguire nel post construct.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}
	
	/**
	 * Consente di effettuare la messa agli atti dei documenti identificati dai
	 * <code> wobNumbers </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAttiFacadeSRV#mettiAgliAtti(java.util.Collection,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.String)
	 */
	@Override
	public final Collection<EsitoOperazioneDTO> mettiAgliAtti(final Collection<String> wobNumbers, final UtenteDTO utente, final String motivazione) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();
		
		for (final String wobNumber : wobNumbers) {
			esiti.add(mettiAgliAtti(wobNumber, utente, motivazione));
		}
		
		return esiti;
	}
	
	/**
	 * Effettua la messa agli atti del documento identificato dal
	 * <code> wobNumber </code>.
	 * 
	 * @param wobNumber
	 *            identificativo documento
	 * @param utente
	 *            utente in sessione
	 * @param motivazione
	 *            motivo di messa agli atti
	 * @param aggiornaNPS
	 *            se <code> true </code> aggiorna NPS, se <code> false </code> non
	 *            aggiorna NPS
	 * @param async
	 *            se <code> true </code> effettua la messa agli atti in maniera
	 *            asincrona, se <code> false </code> effettua la messa agli atti in
	 *            maniera sincrona
	 * @param statoNPS
	 *            stato NPS, @see it.ibm.red.business.nps.builder.Builder
	 * @param fceh
	 *            FileNet Helper per l'accesso al CE
	 * @param fpeh
	 *            FileNet Helper per l'accesso al PE
	 * @param con
	 *            connessione al database
	 * @return esito della messa agli atti
	 */
	private EsitoOperazioneDTO mettiAgliAtti(final String wobNumber, final UtenteDTO utente, final String motivazione, final boolean aggiornaNPS, 
			final boolean async, final String statoNPS, final IFilenetCEHelper fceh, final FilenetPEHelper fpeh, final Connection con) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		Integer numDoc = null;

		try {
			final VWWorkObject wo = fpeh.getWorkFlowByWob(wobNumber, true);
			final String documentTitle = TrasformerPE.getMetadato(wo, PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY).toString();
			final Document d = fceh.getDocumentByIdGestionale(documentTitle, null, null, utente.getIdAoo().intValue(), null, null);
			
			numDoc = (Integer) TrasformerCE.getMetadato(d, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
			final boolean isRiservato = ((Integer) TrasformerCE.getMetadato(d, PropertiesNameEnum.RISERVATO_METAKEY)).equals(BooleanFlagEnum.SI.getIntValue());
			final ListSecurityDTO securities = securitySRV.getSecurityUtentePerUfficio(utente, documentTitle, utente.getIdUfficio().toString(), null, null, 
					isRiservato);
			
			if (!securitySRV.modificaSecurityFascicoli(utente, securities, documentTitle)) {
				throw new RedException("Errore durante l'aggiornamento delle security dei fascicoli in cui è presente il documento: " + documentTitle);
			}
			securitySRV.updateSecurity(utente, documentTitle, null, securities, true);
			
			final HashMap<String, Object> map = new HashMap<>();
			map.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId());
			map.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), motivazione);
			
			// calcolo assegnazioni
			final AssegnazioniEntrataHelper assegnazioniEntrata = new AssegnazioniEntrataHelper();
			assegnazioniEntrata.calcolaAssegnazioni((List<StoricoDTO>) storicoSRV.getStorico(Integer.parseInt(documentTitle), utente.getIdAoo().intValue()), fpeh);
			
			if (!fpeh.nextStep(wo, map, ResponsesRedEnum.ATTI.getResponse())) {
				throw new RedException("Errore durante l'avanzamento del workflow per la messa agli atti del documento: " + documentTitle);
			} else {
				final String[] arrayAssegnazioni = new String[1];
				arrayAssegnazioni[0] = utente.getIdUfficio() + ",0";
				
				securityCambioIterSRV.aggiornaSecurityDocumentiAllacciati(documentTitle, securities, con, arrayAssegnazioni, utente);
				securityCambioIterSRV.aggiornaSecurityContributiInseriti(documentTitle, con, arrayAssegnazioni, utente);
				
				final Integer idFascicolo = (Integer) wo.getFieldValue(pp.getParameterByKey(PropertiesNameEnum.ID_FASCICOLO_WF_METAKEY));
				final Collection<DocumentoFascicoloDTO> documentiFascicolo = fascicoloSRV.getOnlyDocumentiRedFascicolo(idFascicolo, utente);
				final Integer idTipologiaDocumento = (Integer) TrasformerCE.getMetadato(d, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY);
						
				if (documentiFascicolo != null && documentiFascicolo.size() == 1) {
					fascicoloSRV.aggiornaStato(String.valueOf(idFascicolo), FilenetStatoFascicoloEnum.CHIUSO, utente.getIdAoo(), fceh);
				}
				
				bonificaCodeApplicative(assegnazioniEntrata, documentTitle);
				
				if (idTipologiaDocumento.equals(Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.ID_TIPOLOGIA_BILANCIO_ENTI_ENTRATA))) 
						|| idTipologiaDocumento.equals(Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.TIPO_DOC_BILANCIO_ENTI_USCITA)))) {
					final Document dFascicolo = fceh.getFascicolo(Constants.EMPTY_STRING + idFascicolo, utente.getIdAoo().intValue());
					final FascicoloDTO fascicoloDTO = TrasformCE.transform(dFascicolo, TrasformerCEEnum.FROM_DOCUMENTO_TO_FASCICOLO);
					
					if (fascicoloDTO != null && fascicoloDTO.getIdFascicoloFEPA() != null && !fascicoloDTO.getIdFascicoloFEPA().equals(Constants.EMPTY_STRING)) {
						fepaSRV.mettiAgliAttiBilancioEnti(fascicoloDTO.getIdFascicoloFEPA());
					}
				}
			
				// cambio stato protocollo NPS
				if (aggiornaNPS && TipologiaProtocollazioneEnum.isProtocolloNPS(utente.getIdTipoProtocollo())) {
					final String idProtocollo = (String) TrasformerCE.getMetadato(d, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY);
					if (idProtocollo != null) {
						
						final Integer numeroProtocollo = (Integer) TrasformerCE.getMetadato(d, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
						final Integer annoProtocollo = (Integer) TrasformerCE.getMetadato(d, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
						final String numeroAnnoProtocollo = numeroProtocollo + "/" + annoProtocollo;
						
						if (async) {
							npsSRV.cambiaStatoProtEntrataAsync(idProtocollo, numeroAnnoProtocollo, utente, statoNPS, documentTitle, con);
						} else {
							npsSRV.cambiaStatoProtEntrata(idProtocollo, numeroAnnoProtocollo, utente, statoNPS, documentTitle, con);
						}
						
					}
				}
				
				esito.setEsito(true);
				esito.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
				esito.setDocumentTitle(documentTitle);
			}
			
		} catch (final Exception e) {
			LOGGER.error(ERROR_MESSA_AGLI_ATTI_MSG, e);
			esito.setNote(ERROR_MESSA_AGLI_ATTI_MSG);
		} finally {
			esito.setIdDocumento(numDoc);
		}
		
		return esito;
	}
	
	/**
	 * Effettua la bonifica delle code applicative.
	 * 
	 * @param assegnazioniEntrata
	 * @param documentTitle
	 */
	private void bonificaCodeApplicative(final AssegnazioniEntrataHelper assegnazioniEntrata, final String documentTitle) {
		boolean isNodo = false;
		boolean isNodoUtente = false;
		
		// cancellazione del documento dalle code applicative degli uffici/utenti assegnatari per conoscenza
		for (final AssegnatarioOrganigrammaWFAttivoDTO assConoscenza : assegnazioniEntrata.getAssegnatariConoscenza()) {
			isNodo = false;
			isNodoUtente = false;
			if (!assConoscenza.isWfAttivo()) {
				for (final AssegnatarioOrganigrammaWFAttivoDTO assCompetenza : assegnazioniEntrata.getAssegnatariCompetenza()) {
					if (assCompetenza.getIdNodo().intValue() == assConoscenza.getIdNodo().intValue()) {
						isNodo = true;
						if (!assConoscenza.isUfficio() && assCompetenza.getIdUtente().intValue() == assConoscenza.getIdUtente().intValue()) {
							isNodoUtente = true;
						}
					}
				}
				if (!isNodo) {
					codeDocSRV.rimuoviDocumentoCodeApplicative(documentTitle, assConoscenza.getIdNodo());
				} else if (!isNodoUtente && !assConoscenza.isUfficio()) {
					codeDocSRV.rimuoviDocumentoCodeApplicative(documentTitle, assConoscenza.getIdNodo(), assConoscenza.getIdUtente());
				}
			}
		}
		
		// cancellazione del documento dalle code applicative degli uffici/utenti assegnatari per contributo attivi al momento della messa agli atti
		// presenza del documento nelle code applicative (chiuse ufficio) degli uffici/utenti assegnatari per contributo chiusi al momento della messa agli atti
		for (final AssegnatarioOrganigrammaWFAttivoDTO assContributo : assegnazioniEntrata.getAssegnatariContributo()) {
			isNodo = false;
			isNodoUtente = false;
			for (final AssegnatarioOrganigrammaWFAttivoDTO assCompetenza : assegnazioniEntrata.getAssegnatariCompetenza()) {
				if (assCompetenza.getIdNodo().intValue() == assContributo.getIdNodo().intValue()) {
					isNodo = true;
					if (!assContributo.isUfficio() && assCompetenza.getIdUtente().intValue() == assContributo.getIdUtente().intValue()) {
						isNodoUtente = true;
					}
				}
			}
			if (assContributo.isWfAttivo()) {
				boolean rimuovi = true;
				for (final AssegnatarioOrganigrammaWFAttivoDTO assContributoNew : assegnazioniEntrata.getAssegnatariContributo()) {
					if (assContributoNew.getIdNodo().equals(assContributo.getIdNodo()) && assContributoNew.getIdUtente().equals(assContributo.getIdUtente()) && !assContributoNew.isWfAttivo()) {
						rimuovi = false;
						break;
					}
				}
				if (rimuovi) {
					if (!isNodo) {
						codeDocSRV.rimuoviDocumentoCodeApplicative(documentTitle, assContributo.getIdNodo());
					} else if (!isNodoUtente && !assContributo.isUfficio()) {
						codeDocSRV.rimuoviDocumentoCodeApplicative(documentTitle, assContributo.getIdNodo(), assContributo.getIdUtente());
					}
				}
			} else {
				if (!assContributo.isUfficio() && !isNodoUtente) {
					
					final boolean isItemNodo = codeDocSRV.hasItemNodo(documentTitle, assContributo.getIdNodo());
					if (isItemNodo) {
						codeDocSRV.rimuoviDocumentoCodeApplicative(documentTitle, assContributo.getIdNodo(), assContributo.getIdUtente());
					} else {
						codeDocSRV.annullaUtenteDocumento(documentTitle, assContributo.getIdNodo(), assContributo.getIdUtente());
					}
				}
			}
		}
	}
	
	
	/**
	 * Effettua la messa agli atti del documento identificato dal
	 * <code> wobNumber </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.IAttiFacadeSRV#mettiAgliAtti(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.String)
	 */
	@Override
	public EsitoOperazioneDTO mettiAgliAtti(final String wobNumber, final UtenteDTO utente, final String motivazione) {
		return mettiAgliAtti(wobNumber, utente, motivazione, false, Builder.AGLI_ATTI);
	}
	
	/**
	 * Effettua la messa agli atti del documento identificato dal <code> wobNumber </code>.
	 * @see it.ibm.red.business.service.facade.IAttiFacadeSRV#mettiAgliAtti(java.lang.String, it.ibm.red.business.dto.UtenteDTO, java.lang.String, boolean, java.lang.String)
	 */
	@Override
	public EsitoOperazioneDTO mettiAgliAtti(final String wobNumber, final UtenteDTO utente, final String motivazione, final boolean async, final String statoNPS) {
		EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		Connection con = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			
			esito = mettiAgliAtti(wobNumber, utente, motivazione, true, async, statoNPS, fceh, fpeh, con);

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore nella connessione verso il DB durante la messa agli atti.", e);
			esito.setNote(ERROR_MESSA_AGLI_ATTI_MSG);
		} finally {
			closeConnection(con);
			popSubject(fceh);
			logoff(fpeh);
		}
		
		return esito;
	}
	
}
