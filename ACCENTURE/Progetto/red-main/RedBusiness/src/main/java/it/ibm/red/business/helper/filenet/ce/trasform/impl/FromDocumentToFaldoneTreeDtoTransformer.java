package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.Date;

import com.filenet.api.core.Document;

import it.ibm.red.business.dto.FaldoneDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;

/**
 * Trasformer Documet to FadloneTreeDTO.
 */
public class FromDocumentToFaldoneTreeDtoTransformer extends TrasformerCE<FaldoneDTO> {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Costruttore.
	 */
	public FromDocumentToFaldoneTreeDtoTransformer() {
		super(TrasformerCEEnum.FROM_DOCUMENT_TO_FALDONE_TREE);
	}

	/**
	 * Esegue la trasfomrazione del document.
	 * @param document
	 * @param connection
	 * @return FaldoneDTO
	 */
	@Override
	public FaldoneDTO trasform(final Document document, final Connection connection) {
		String documentTitle = (String) getMetadato(document, PropertiesNameEnum.METADATO_FALDONE_DOCUMENTTITLE);
		String nomeFaldone = (String) getMetadato(document, PropertiesNameEnum.METADATO_FALDONE_NOMEFALDONE);
		String parentFaldone = (String) getMetadato(document, PropertiesNameEnum.METADATO_FALDONE_PARENTFALDONE);
		String oggetto = (String) getMetadato(document, PropertiesNameEnum.METADATO_FALDONE_OGGETTO);
		String descrizioneFaldone = (String) getMetadato(document, PropertiesNameEnum.METADATO_FALDONE_DESCRIZIONEFALDONE);
		Date dateCreated = document.get_DateCreated();
		
		FaldoneDTO output = null;
		
		if (document.get_SecurityFolder() != null) {
			output = new FaldoneDTO(null, null, documentTitle, null, dateCreated, null, nomeFaldone, parentFaldone, descrizioneFaldone, null, null, oggetto,
					document.get_SecurityFolder().get_PathName());
		}
		
		return output;
	}
}
