package it.ibm.red.business.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IFaldoneDAO;
import it.ibm.red.business.exception.RedException;

/**
 * Dao per la gestione di un fascicolo.
 *
 * @author CPIERASC
 * 
 */
@Repository
public class FaldoneDAO extends AbstractDAO implements IFaldoneDAO {

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @see it.ibm.red.business.dao.IFaldoneDAO#getNextIdFaldone(java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public Integer getNextIdFaldone(final Integer idAoo, final Connection con) {
		CallableStatement cs = null;
		try {
			cs = con.prepareCall("{? = call CALL_SEQ_FALDONE_BY_IDAOO(?)}");
			cs.registerOutParameter(1, Types.INTEGER);
			cs.setInt(2, idAoo);
			cs.execute();
			return cs.getInt(1);
		} catch (SQLException e) {
			throw new RedException("Errore durante il recupero della sequence del faldone. " + e);
		} finally {
			closeStatement(cs);
		}
	}
}