package it.ibm.red.business.enums;

/**
 * Tipologia del documento corrisponde alla descrizione dell'oggetto presente sul db PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY.
 * 
 */
public enum DescrizioneTipologiaDocumentoEnum {
	
    /**
     * Dichiarazione servizi resi.
     */
	DICHIARAZIONE_SERVIZI_RESI("DICHIARAZIONE SERVIZI RESI"),

	/**
     * Fattura Fepa.
     */
	FATTURA_FEPA("FATTURA FEPA"),

	/**
	 * Decreto dirigenziale Fepa.
	 */
	DECRETO_DIRIGENZIALE_FEPA("DECRETO DIRIGENZIALE FEPA");
	
    /**
     * Descrizione.
     */
	private String descrizione;

	/**
	 * Costruttore dell'Enum.
	 * @param inDescrizione
	 */
	DescrizioneTipologiaDocumentoEnum(final String inDescrizione) {
		this.descrizione = inDescrizione;
	}

	/**
	 * Restituisce la descrizione associata all'Enum.
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}
	
}
