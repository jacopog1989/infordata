package it.ibm.red.business.dto;

/**
 * DTO Rds Siebel Gruppo.
 */
public class RdsSiebelGruppoDTO extends AbstractDTO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -1851563040291197467L;
	
	/**
	 * Gruppo.
	 */
	private String codiceGruppo;

	/**
	 * descrizione.
	 */
	private String descrizioneGruppo;
	
	/**
	 * Costruttore di default.
	 */
	public RdsSiebelGruppoDTO() {
		super();
	}

	/**
	 * Costruttore del DTO.
	 * @param codiceGruppo
	 * @param descrizioneGruppo
	 */
	public RdsSiebelGruppoDTO(final String codiceGruppo, final String descrizioneGruppo) {
		super();
		this.codiceGruppo = codiceGruppo;
		this.descrizioneGruppo = descrizioneGruppo;
	}

	/**
	 * Restituisce il codice del gruppo.
	 * @return codice gruppo
	 */
	public String getCodiceGruppo() {
		return codiceGruppo;
	}

	/**
	 * Imposta il codice del gruppo.
	 * @param codiceGruppo
	 */
	public void setCodiceGruppo(final String codiceGruppo) {
		this.codiceGruppo = codiceGruppo;
	}

	/**
	 * Restituisce la descrizione del gruppo.
	 * @return descrizione gruppo
	 */
	public String getDescrizioneGruppo() {
		return descrizioneGruppo;
	}

	/**
	 * Imposta la descrizione del gruppo.
	 * @param descrizioneGruppo
	 */
	public void setDescrizioneGruppo(final String descrizioneGruppo) {
		this.descrizioneGruppo = descrizioneGruppo;
	}

}