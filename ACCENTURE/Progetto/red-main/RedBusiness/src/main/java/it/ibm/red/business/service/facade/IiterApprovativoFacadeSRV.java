/**
 * 
 */
package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * @author APerquoti
 *
 */
public interface IiterApprovativoFacadeSRV extends Serializable {
	
	/**
	 * 
	 * @param idAoo
	 * @return
	 */
	List<IterApprovativoDTO> getAllByIdAoo(Integer idAoo);
	
	
	
	/**
	 * @param idAoo
	 * @param documentTitle
	 * @param utente
	 * @return
	 */
	List<IterApprovativoDTO> getAllByDocument(String documentTitle, UtenteDTO utente);

}
