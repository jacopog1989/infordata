package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.service.concrete.MasterPageIterator;

/**
 * Facade del servizio che gestisce i master paginati.
 */
public interface IMasterPaginatiFacadeSRV extends Serializable {

	/**
	 * Recupera una nuova pagina di masters.
	 * @param utente
	 * @param pagedResult
	 * @return documenti master
	 */
	Collection<MasterDocumentRedDTO> refineMasters(UtenteDTO utente, MasterPageIterator pagedResult);

	/**
	 * Ottiene l'oggetto in grado di gestire la paginazione per le code di un utente.
	 * @param queue
	 * @param documentTitlesToFilterBy
	 * @param utente
	 * @return paginator
	 */
	MasterPageIterator getMastersRaw(DocumentQueueEnum queue, Collection<String> documentTitlesToFilterBy, UtenteDTO utente);

	/**
	 * Recupera una nuova pagina di masters per le code dell'organigramma.
	 * @param utente
	 * @param nodoOrganigramma
	 * @param pagedResult
	 * @return documenti master
	 */
	Collection<MasterDocumentRedDTO> refineMastersOrganigramma(UtenteDTO utente, NodoOrganigrammaDTO nodoOrganigramma, MasterPageIterator pagedResult);

	/**
	 * Ottiene l'oggetto in grado di gestire la paginazione della scrivania di uno dei nodi dell'organigramma acceduto da un altro utente.
	 * @param utente
	 * @param nodoOrganigramma
	 * @param queues
	 * @return paginator
	 */
	MasterPageIterator getMastersRawUtenteOrganigramma(UtenteDTO utente, NodoOrganigrammaDTO nodoOrganigramma, DocumentQueueEnum... queues);
	
	/**
	 * Ottiene i documenti dal CE senza paginazione filtrandoli.
	 * @param filterString
	 * @param pagedResult
	 * @param utente
	 * @return documenti master
	 */
	Collection<MasterDocumentRedDTO> getFilteredMasters(String filterString, MasterPageIterator pagedResult, UtenteDTO utente);
}
