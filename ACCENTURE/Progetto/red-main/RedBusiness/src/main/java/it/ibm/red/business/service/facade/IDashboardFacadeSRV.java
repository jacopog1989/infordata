package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.persistence.model.Widget;

/**
 * Facade del servizio di gestione dashboard.
 */
public interface IDashboardFacadeSRV extends Serializable {

	/**
     * 
     * @param wdgId    
     * @param utenteId 
     * @return        
     *  
     */
	void salvaPreferenzeWdg(List<Long> wdgId, Long utenteId, Long ruoloUtente);
	
	/**
     * 
     * @param wdgId   
     * @param utenteId 
     * @return           
     * 
     */
	void rimuoviPreferenze(List<Long> wdgId, Long utenteId);
	
	/**
	 * 
     * @param utenteId  
     * @param utenteRuolo
     * @return          
     */
	List<Widget> getWidgetPreferenze(Long utenteId, Long utenteRuolo);
	
	/**
     * 
     * @param idRuolo   
     * @return         
     */
	List<Widget> getWidgetRuolo(Long idRuolo);		
}
