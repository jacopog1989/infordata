package it.ibm.red.business.dto;

import java.util.List;

import it.ibm.red.business.enums.TipoAssegnazioneEnum;

/**
 * DTO che raccoglie le informazioni del form di ricerca avazata dei fascicoli.
 */
public class RicercaAvanzataFascicoliFormDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 2728405170027850101L;

	/**
	 * Lista tipologie documento.
	 */
	private List<TipologiaDocumentoDTO> comboTipologieDocumento;
	
	/**
	 * Lista tipi procedimento.
	 */
	private List<TipoProcedimentoDTO> comboTipiProcedimento;
	
	/**
	 * Lista tipo assegnazione.
	 */
	private List<TipoAssegnazioneEnum> comboTipoAssegnazione;
	
	/* GET & SET ******************************************************************************************/

	/**
	 * Restituisce la lista delle tipologie documento per il popolamento della combobox.
	 * @return lista tipologie documento
	 */
	public List<TipologiaDocumentoDTO> getComboTipologieDocumento() {
		return comboTipologieDocumento;
	}

	/**
	 * Imposta la lista delle tipologie documento per il popolamento della combobox.
	 * @param comboTipologieDocumento
	 */
	public void setComboTipologieDocumento(final List<TipologiaDocumentoDTO> comboTipologieDocumento) {
		this.comboTipologieDocumento = comboTipologieDocumento;
	}

	/**
	 * Restituisce la lista dei tipi di procedimento per il popolamento della combobox.
	 * @return lista dei tipi di procedimento
	 */
	public List<TipoProcedimentoDTO> getComboTipiProcedimento() {
		return comboTipiProcedimento;
	}

	/**
	 * Imposta la lista dei tipi di procedimento per il popolamento della combobox.
	 * @param comboTipiProcedimento
	 */
	public void setComboTipiProcedimento(final List<TipoProcedimentoDTO> comboTipiProcedimento) {
		this.comboTipiProcedimento = comboTipiProcedimento;
	}

	/**
	 * Restituisce la lista delle tipologie di assegnazione per il popolamento della combobox associata.
	 * @return lista delle tipologie di assegnazione
	 */
	public List<TipoAssegnazioneEnum> getComboTipoAssegnazione() {
		return comboTipoAssegnazione;
	}

	/**
	 * Imposta la lista delle tipologie di assegnazione per il popolamento della combobox associata.
	 * @param comboTipoAssegnazione
	 */
	public void setComboTipoAssegnazione(final List<TipoAssegnazioneEnum> comboTipoAssegnazione) {
		this.comboTipoAssegnazione = comboTipoAssegnazione;
	}

}
