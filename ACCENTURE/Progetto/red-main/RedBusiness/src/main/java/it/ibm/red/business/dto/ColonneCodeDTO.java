package it.ibm.red.business.dto;

import it.ibm.red.business.enums.ColonneEnum;

/**
* @author  VINGENITO
*/
public class ColonneCodeDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Colonna di riferimento.
	 */
	private ColonneEnum colEnum;

	/**
	 * Flag visibilità colonna.
	 */
	private boolean visibilitaColonna;

	/**
	 * Flag render colonna.
	 */
	private boolean renderColonna;

	/**
	 * Costruttore del DTO.
	 * @param colEnum
	 * @param visibilitaColonna
	 */
	public ColonneCodeDTO(final ColonneEnum colEnum, final boolean visibilitaColonna) {
		this.colEnum = colEnum;
		this.visibilitaColonna = visibilitaColonna;
	}

	/**
	 * Restituisce l'Enum associata alle colonne.
	 * @return colEnum
	 */
	public ColonneEnum getColEnum() {
		return colEnum;
	}

	/**
	 * Imposta l'Enum associata.
	 * @param colEnum
	 */
	public void setColEnum(final ColonneEnum colEnum) {
		this.colEnum = colEnum;
	}

	/**
	 * Restituisce la visibilità della colonna.
	 * @return visibilitaColonna
	 */
	public boolean isVisibilitaColonna() {
		return visibilitaColonna;
	}

	/**
	 * Imposta la visibilità della colonna.
	 * @param visibilitaColonna
	 */
	public void setVisibilitaColonna(final boolean visibilitaColonna) {
		this.visibilitaColonna = visibilitaColonna;
	}

	/**
	 * Getter renderColonna.
	 * @return remderColonna
	 */
	public boolean isRenderColonna() {
		return renderColonna;
	}

	/**
	 * Imposta il flag renderColonna.
	 * @param renderColonna
	 */
	public void setRenderColonna(final boolean renderColonna) {
		this.renderColonna = renderColonna;
	}


}
