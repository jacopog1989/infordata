/**
 * 
 */
package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.enums.FunzionalitaEnum;

/**
 * @author Slac
 *
 */
public interface ICasellaPostaleDAO extends Serializable {
	
	/**
	 * Restituisce TRUE se la casella postale è abilitata alla funzionalità
	 * passatagli.
	 * 
	 * @param casellaPostale
	 * @param idNodo
	 * @param f
	 * @param con
	 * @return Esito dell'abilitazione.
	 */
	boolean getAbilitazioniCasellaPostale(String casellaPostale, Long idNodo, FunzionalitaEnum f, Connection con);

	/**
	 * Restituisce l'ordinamento se la casella postale è abilitata alla funzionalità
	 * passatagli.
	 * 
	 * @param casellaPostale
	 * @param idNodo
	 * @param f
	 * @param con
	 * @return l'ordinamento in caso la casella sia abilitata, -1 altrimenti
	 */
	int getOrdinamentoCPAbilitata(String casellaPostale, Long idNodo, FunzionalitaEnum f, Connection con);

	/**
	 * Restituisce le response associate alla casella, per il nodo in input.
	 * 
	 * @param casellaPostale
	 * @param idNodo
	 * @param connection
	 * @return Lista recuperata.
	 */
	List<Integer> getCasellaPostaleNodoResponse(String casellaPostale, Long idNodo, Connection connection);

	/**
	 * Restituisce la capienza della casella postale.
	 * @param casellaPostale - casella postale
	 * @param connection
	 * @return dimensione massima
	 */
	Integer getMaxSizeContentCasellaPostale(String casellaPostale, Connection connection);
	
}
