package it.ibm.red.business.service.facade;

import java.io.Serializable;

/**
 * Facade del servizio di verifica firma.
 */
public interface IVerificaFirmaFacadeSRV extends Serializable {

}
