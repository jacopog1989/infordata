package it.ibm.red.business.service.concrete;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.filenet.api.constants.AutoUniqueName;
import com.filenet.api.constants.Cardinality;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.constants.ReservationType;
import com.filenet.api.core.Document;
import com.filenet.api.core.Folder;
import com.filenet.api.core.ReferentialContainmentRelationship;
import com.filenet.api.util.Id;

import fepa.header.v3.EsitoType;
import fepa.header.v3.ServiceErrorType;
import fepa.messages.v3.RispostaDownloadDocumentoFascicolo;
import fepa.messages.v3.RispostaGetFascicolo;
import fepa.services.v3.InterfacciaFascicoli;
import fepa.types.v3.DocumentoContentType;
import fepa.types.v3.DocumentoFascicoloType;
import fepa.types.v3.FascicoloType;
import fepa.types.v3.RichiestaDownloadDocumentoFascicoloType;
import fepa.types.v3.RichiestaGetFascicoloType;
import fepa.types.v3.TipoDettaglioEstrazioneFascicoloType;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.ErrorCode;
import it.ibm.red.business.constants.Constants.MapRedKey;
import it.ibm.red.business.constants.Constants.Modalita;
import it.ibm.red.business.dao.IContattoDAO;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dao.ITipologiaDocumentoDAO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.DocumentoFascicoloContabileDTO;
import it.ibm.red.business.dto.DocumentoFascicoloDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.MetadatoDinamicoDTO;
import it.ibm.red.business.dto.PEDocumentoDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedParametriDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.FormatoDocumentoEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ProvenienzaSalvaDocumentoEnum;
import it.ibm.red.business.enums.TipoAccessoFascicoloFepaEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.exception.RedWSException;
import it.ibm.red.business.helper.adobe.AdobeLCHelper;
import it.ibm.red.business.helper.adobe.dto.CountSignsOutputDTO;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.REDServiceTrasformer;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.TipoProcedimento;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.provider.WebServiceClientProvider;
import it.ibm.red.business.service.IDocumentoRedSRV;
import it.ibm.red.business.service.IDocumentoSRV;
import it.ibm.red.business.service.IREDServiceSRV;
import it.ibm.red.business.service.ISalvaDocumentoSRV;
import it.ibm.red.business.service.ISecurityCambioIterSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.webservice.model.redservice.header.v1.CredenzialiUtenteRed;
import it.ibm.red.webservice.model.redservice.messages.v1.GetDocumentiFascicoloResponse;
import it.ibm.red.webservice.model.redservice.messages.v1.GetVersioneDocumentoResponse;
import it.ibm.red.webservice.model.redservice.types.v1.DocumentoRed;
import it.ibm.red.webservice.model.redservice.types.v1.FascicoloRed;
import it.ibm.red.webservice.model.redservice.types.v1.IdentificativoUnitaDocumentaleFEPATypeRed;
import it.ibm.red.webservice.model.redservice.types.v1.MapRed;
import it.ibm.red.webservice.model.redservice.types.v1.MetadatoRed;
import it.ibm.red.webservice.model.redservice.types.v1.ObjectFactory;
import it.ibm.red.webservice.model.redservice.types.v1.RispostaDocRed;
import it.ibm.red.webservice.model.redservice.types.v1.RispostaElencoVersioniDocRed;

/**
 * RED service.
 */
@Service
@Component
public class REDServiceSRV extends AbstractService implements IREDServiceSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 207276981906528822L;

	/**
	 * Label errore fattura.
	 */
	private static final String LABEL_ERROR_FATTURA = "ERROR: Fattura [";

	/**
	 * Messaggio errore call FEPA.
	 */
	private static final String ERROR_CALL_FEPA_MSG = "ERROR CALL: FEPA.getFascicolo. MESSAGE: ";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(REDServiceSRV.class);

	/**
	 * Label.
	 */
	private static final String TIPO_DECRETO_NETTO = "NETTO";

	/**
	 * Label.
	 */
	private static final String TIPO_DECRETO_IVA = "IVA";

	/**
	 * Service.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IDocumentoSRV documentoSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ISecurityCambioIterSRV securityCambioIterSRV;

	/**
	 * Service.
	 */
	@Autowired
	private IDocumentoRedSRV documentoRedSRV;

	/**
	 * Service.
	 */
	@Autowired
	private ISalvaDocumentoSRV salvaDocSrv;

	/**
	 * DAO.
	 */
	@Autowired
	private ITipoProcedimentoDAO tipoProcedimentoDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private ITipologiaDocumentoDAO tipologiaDocumentoDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IContattoDAO contattoDAO;

	/**
	 * Post construct del service.
	 */
	@PostConstruct
	public void postCostruct() {
		// Non deve eseguire alcuna azione nel post construct.
	}

	/**
	 * @see it.ibm.red.business.service.facade.IREDServiceFacadeSRV#riassegnaFatturaFepa(java.lang.String,
	 *      java.lang.String, java.lang.String,
	 *      it.ibm.red.business.helper.filenet.pe.FilenetPEHelper,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper).
	 */
	@Override
	public String riassegnaFatturaFepa(final String idFascicoloFepa, final String utenteCedente, final String destinatario, final FilenetPEHelper peh,
			final IFilenetCEHelper ceh) {
		String idDoc = null;
		Connection con = null;
		PropertiesProvider pp = PropertiesProvider.getIstance();
		try {
			con = setupConnection(getDataSource().getConnection(), true);
			final UtenteDTO utente = utenteSRV.getById(Long.parseLong(utenteCedente));

			final String idNodo = utente.getIdUfficio().toString();

			idDoc = ceh.getIdDocumentoByMetadatoFepa(pp.getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA), idFascicoloFepa);
			if (idDoc == null) {
				throw new RedWSException("ERROR: Fattura non presente su RED", ErrorCode.FATTURA_NON_PRESENTE_SU_RED);
			}

			final VWWorkObject workflow = peh.getWorkflowPrincipale(idDoc, utente.getFcDTO().getIdClientAoo());

			LOGGER.info("BEGIN validazione workflow");

			if (workflow == null) {
				LOGGER.error("ERROR: ERROR: Fattura già lavorata");
				throw new RedWSException("ERROR: Fattura già lavorata", ErrorCode.FATTURA_GIA_LAVORATA);
			}
			// Quando la fattura viene associata ad un decreto, viene valorizzato il campo
			// WobParent per avere il riferimento al decreto.
			final String wobParent = (String) peh.getMetadato(workflow.getWorkObjectNumber(), pp.getParameterByKey(PropertiesNameEnum.WF_WOBPARENT));
			if (wobParent != null && !"".equals(wobParent)) {
				LOGGER.error("ERROR: Fattura già associata ad un decreto");
				throw new RedWSException("ERROR: Fattura già associata ad un decreto", ErrorCode.FATTURA_GIA_ASSOCIATA_AD_UN_DECRETO);
			}

			LOGGER.info("END validazione workflow");

			final String[] arrayAssegnazioni = { idNodo + "," + utente.getId().toString() };

			securityCambioIterSRV.aggiornaSecurityContributiInseriti(idDoc, con, arrayAssegnazioni, utente);

			LOGGER.info("Avanzamento del Workflow: [" + workflow.getWorkObjectNumber() + "]");

			final Map<String, Object> metadati = new HashMap<>();
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY), idNodo);
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY), destinatario);
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.MOTIVAZIONE_ASSEGNAZIONE_WF_METAKEY), "");
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), "" + utente.getId());

			if (!peh.nextStep(workflow, metadati, "SYS_Storna a utente")) {
				throw new RedWSException("Errore nell'avanzamento del WF per riassegnazione fattura.");

			}
			commitConnection(con);
		} catch (final Exception e) {
			LOGGER.error("Errore durante la riassegnazione fattura : ", e);
			rollbackConnection(con);
			throw new RedWSException(e);
		} finally {
			closeConnection(con);
		}
		return idDoc;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IREDServiceFacadeSRV#getVersioneDocumento(java.lang.String,
	 *      java.lang.String, int).
	 */
	@Override
	public GetVersioneDocumentoResponse getVersioneDocumento(final String idUtente, final String idDocumento, final int versione) {
		GetVersioneDocumentoResponse getVersioneDocumentoResponse;
		IFilenetCEHelper fceh = null;

		try {
			final UtenteDTO utente = utenteSRV.getById(Long.parseLong(idUtente));

			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final Document doc = documentoRedSRV.getVersioneDocumento(idDocumento, versione, utente.getIdAoo(), fceh);

			final AdobeLCHelper adobe = new AdobeLCHelper();
			final CountSignsOutputDTO countsSignedFields = adobe.countsSignedFields(FilenetCEHelper.getDocumentContentAsInputStream(doc));
			final boolean firmato = countsSignedFields.getNumSigns() > 0;

			final MetadatoRed met = new MetadatoRed();
			final ObjectFactory of = new ObjectFactory();
			if (firmato) {
				// statoDocumentoFepa enum?
				met.setKey(of.createMetadatoRedKey("statoDocumentoFepa"));
				met.setValue(of.createMetadatoRedValue("FIRMATO"));
			} else {
				met.setKey(of.createMetadatoRedKey("statoDocumentoFepa"));
				met.setValue(of.createMetadatoRedValue("DAFIRMARE"));
			}

			getVersioneDocumentoResponse = REDServiceTrasformer.getGetVersioneDocumentoResponseDocumentFromDocument(doc, fceh);

			getVersioneDocumentoResponse.getReturn().getValue().getVersioneDocumento().getValue().getClasseDocumentale().getValue().getMetadato().add(met);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero della versione richiesta del documento", e);
			throw new RedException("Errore nel recupero della versione richiesta del documento");
		} finally {
			popSubject(fceh);
		}

		return getVersioneDocumentoResponse;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IREDServiceFacadeSRV#modificaMetadati(java.lang.String,
	 *      java.lang.String, java.util.List).
	 */
	@Override
	public void modificaMetadati(final String idUtente, final String idDocumento, final List<MetadatoRed> metadati) {
		IFilenetCEHelper fceh = null;

		try {
			final UtenteDTO utente = utenteSRV.getById(Long.parseLong(idUtente));
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final Map<String, Object> metadatiMap = new HashMap<>();

			for (final MetadatoRed i : metadati) {
				if (i.getValue() != null) {
					metadatiMap.put(i.getKey().getValue(), i.getValue().getValue());
				} else {
					metadatiMap.put(i.getKey().getValue(), i.getMultiValues());
				}
			}

			fceh.updateMetadati(fceh.getDocumentByDTandAOO(idDocumento, utente.getIdAoo()), metadatiMap);
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IREDServiceFacadeSRV#checkOut(java.lang.String,
	 *      java.lang.String).
	 */
	@Override
	public void checkOut(final String idDocumento, final String idUtente) {
		IFilenetCEHelper fceh = null;

		try {
			final UtenteDTO utente = utenteSRV.getById(Long.parseLong(idUtente));
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final Document doc = fceh.getDocumentByDTandAOO(idDocumento, utente.getIdAoo());
			FilenetCEHelper.checkout(doc);
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IREDServiceFacadeSRV#elencoVersioniDocumento(java.lang.String,
	 *      java.lang.String).
	 */
	@Override
	public RispostaElencoVersioniDocRed elencoVersioniDocumento(final String idUtente, final String idDocumento) {
		IFilenetCEHelper fceh = null;

		try {
			final UtenteDTO utente = utenteSRV.getById(Long.parseLong(idUtente));
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final RispostaElencoVersioniDocRed rispostaElencoVersioniDoc = new RispostaElencoVersioniDocRed();

			final List<Document> versioni = documentoRedSRV.getElencoVersioniDocumento(idDocumento, utente.getIdAoo(), fceh);

			for (final Document d : versioni) {
				rispostaElencoVersioniDoc.getVersioniDocumento().add(REDServiceTrasformer.getDocumentoRedFromDocument(d, fceh).getValue());
			}

			return rispostaElencoVersioniDoc;
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IREDServiceFacadeSRV#getDocumentiFascicolo(java.lang.String,
	 *      java.lang.String).
	 */
	@Override
	public GetDocumentiFascicoloResponse getDocumentiFascicolo(final String idUtente, final String idFascicoloFEPA) {
		final GetDocumentiFascicoloResponse getDocumentiFascicoloResponse = new GetDocumentiFascicoloResponse();
		IFilenetCEHelper fceh = null;

		try {
			final UtenteDTO utente = utenteSRV.getById(Long.parseLong(idUtente));
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final ObjectFactory of = new ObjectFactory();
			final Iterator<DocumentoFascicoloDTO> i = fceh.getOnlyDocumentiRedFascicolo(Integer.parseInt(idFascicoloFEPA), utente.getIdAoo()).iterator();
			while (i.hasNext()) {
				final DocumentoFascicoloDTO f = i.next();
				final Document d = fceh.getDocumentByDTandAOO(f.getDocumentTitle(), f.getAoo().getIdAoo());
				getDocumentiFascicoloResponse.getDocumenti().add(REDServiceTrasformer.getDocumentoRedFromDocument(d, fceh).getValue());
			}
			final FascicoloRed fascicolo = new FascicoloRed();
			fascicolo.setIdFascicolo(of.createFascicoloRedIdFascicolo(idFascicoloFEPA));
			getDocumentiFascicoloResponse.getFascicoli().add(fascicolo);
		} finally {
			popSubject(fceh);
		}

		return getDocumentiFascicoloResponse;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IREDServiceFacadeSRV#inserimentoFascicoloDecreto(it.ibm.red.webservice.model.redservice.header.v1.CredenzialiUtenteRed,
	 *      it.ibm.red.webservice.model.redservice.types.v1.MapRed,
	 *      it.ibm.red.webservice.model.redservice.types.v1.DocumentoRed,
	 *      java.util.List).
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public String inserimentoFascicoloDecreto(final CredenzialiUtenteRed credenziali, final MapRed metadati, final DocumentoRed documento,
			final List<IdentificativoUnitaDocumentaleFEPATypeRed> fascicoli) {
		String idDocRitornato;
		Connection connection = null;
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;
		PropertiesProvider pp = PropertiesProvider.getIstance();
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			final UtenteDTO utente = utenteSRV.getById(Long.parseLong(credenziali.getIdUtente().getValue()));
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			LOGGER.info("BEGIN REQUEST AssegnaFascicoloFatturaRequest");
			LOGGER.info("CredenzialiUtente.idAoo: " + utente.getIdAoo());
			LOGGER.info("CredenzialiUtente.idGruppo: " + utente.getIdUfficio());
			LOGGER.info("CredenzialiUtente.idUtente: " + utente.getId());

			String idDocumento = null;
			String idFascicolo = null;
			if (documento != null) {
				if (documento.getIdDocumento() != null) {
					idDocumento = documento.getIdDocumento().getValue();
				}
				if (documento.getIdFascicolo() != null) {
					idFascicolo = documento.getIdFascicolo().getValue();
				}
			}

			final List<MetadatoRed> metadata = documento.getClasseDocumentale().getValue().getMetadato();
			final HashMap<String, Object> map = new HashMap<>();
			for (final MetadatoRed m : metadata) {
				if (m.getValue() != null) {
					map.put(m.getKey().getValue(), m.getValue().getValue());
				} else {
					map.put(m.getKey().getValue(), m.getMultiValues());
				}
			}
			LOGGER.info("METADATI: " + map.toString());

			final String oggettoProtocollo = (String) map.get(MapRedKey.OGGETTO_PROTOCOLLO_FEPA);
			if ((oggettoProtocollo != null) && (oggettoProtocollo.length() == 0)) {
				LOGGER.error("OGGETTO_PROTOCOLLO_FEPA errata valorizzazione");
				throw new RedWSException(
						"ERROR: si è verificato un errore applicativo in InserimentoFascicoloDecretoService. MESSAGE: Eccezione per i metadati del protocollo: OGGETTO_PROTOCOLLO_FEPA errata valorizzazione",
						ErrorCode.METADATA_IS_NOT_VALUED_PROPERLY_ERROR);
			}

			Integer idTipologiaDocumento = Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.TIPO_DECRETO_NETTO_ID));
			if (map.containsKey(MapRedKey.TIPO_DECRETO)) {
				LOGGER.info("Tipo Decreto :" + map.get(MapRedKey.TIPO_DECRETO));
				if (TIPO_DECRETO_NETTO.equals(map.get(MapRedKey.TIPO_DECRETO))) {
					idTipologiaDocumento = Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.TIPO_DECRETO_NETTO_ID));
				} else if (TIPO_DECRETO_IVA.equals(map.get(MapRedKey.TIPO_DECRETO))) {
					idTipologiaDocumento = Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.TIPO_DECRETO_IVA_ID));
				}
			} else {
				LOGGER.warn("TIPO DECRETO NON SPECIFICATO: default NETTO");
				idTipologiaDocumento = Integer.parseInt(pp.getParameterByKey(PropertiesNameEnum.TIPO_DECRETO_NETTO_ID));
			}

			LOGGER.info("idDocumento: " + (idDocumento != null ? idDocumento : "NON PRESENTE"));
			LOGGER.info("idFascicolo: " + (idFascicolo != null ? idFascicolo : "NON PRESENTE"));

			// Aggiunta metadati

			final String nomeDocumento = (String) map.get(MapRedKey.NOME_FILE_FEPA);
			if ((nomeDocumento != null) && (nomeDocumento.length() == 0)) {
				LOGGER.error("NOME_FILE_FEPA errata valorizzazione");
				throw new RedWSException(
						"ERROR: si è verificato un errore applicativo in InserimentoFascicoloDecretoService. MESSAGE: Eccezione per i metadati del protocollo: NOME_FILE_FEPA errata valorizzazione",
						ErrorCode.METADATA_IS_NOT_VALUED_PROPERLY_ERROR);
			}

			final String numeroProtocolloStr = (String) map.get(MapRedKey.NUM_PROTOCOLLO_FEPA);
			if ((numeroProtocolloStr != null) && (numeroProtocolloStr.length() == 0)) {
				LOGGER.error("NUM_PROTOCOLLO_FEPA errata valorizzazione");
				throw new RedWSException(
						"ERROR: si è verificato un errore applicativo in InserimentoFascicoloDecretoService. MESSAGE: Eccezione per i metadati del protocollo: NUM_PROTOCOLLO_FEPA errata valorizzazione",
						ErrorCode.METADATA_IS_NOT_VALUED_PROPERLY_ERROR);
			}

			final String dataProtocollo = (String) map.get(MapRedKey.DATA_PROTOCOLLO_FEPA);
			if ((dataProtocollo != null) && (dataProtocollo.length() == 0)) {
				LOGGER.error("DATA_PROTOCOLLO_FEPA errata valorizzazione");
				throw new RedWSException(
						"ERROR: si è verificato un errore applicativo in InserimentoFascicoloDecretoService. MESSAGE: Eccezione per i metadati del protocollo: DATA_PROTOCOLLO_FEPA errata valorizzazione",
						ErrorCode.METADATA_IS_NOT_VALUED_PROPERLY_ERROR);
			}

			List<String> fascicoliFepa = null;
			if (fascicoli != null && !fascicoli.isEmpty()) {
				fascicoliFepa = new ArrayList();
				for (final IdentificativoUnitaDocumentaleFEPATypeRed id : fascicoli) {
					if (id != null && id.getID() != null && id.getID().getID() != null) {
						fascicoliFepa.add(id.getID().getID());
					}
				}
			}

			final byte[] content = documento.getDataHandler().getValue();
			if (content == null || (content != null && content.length == 0)) {
				LOGGER.error("Non è stato allegato alcun documento al decreto dirigenziale.");
				throw new RedWSException(
						"ERROR: si è verificato un errore applicativo in InserimentoFascicoloDecretoService. MESSAGE: Non è stato allegato alcun documento al decreto dirigenziale.",
						ErrorCode.DECRETO_DIRIGENZIALE_SENZA_DOCUMENTO_ALLEGATO);
			}

			final String utenteDest = credenziali.getIdUtente().getValue();
			final String nododest = credenziali.getIdGruppo().toString();

			// nel validaAllFascicoloFepa verifico per ogni fascicolo se:
			// 1) l'idfascicolo esiste
			// 2) se è associato all'utente mappato in credenziali utente
			// 3) se non è assegnato ad un altro decreto

			LOGGER.info("BEGIN validazione fascicolo FEPA");
			for (final String fascicolo : fascicoliFepa) {
				LOGGER.info("Validazione fascicolo FEPA: " + fascicolo);
				final String iddocumento = fceh.getIdDocumentoByMetadatoFepa(pp.getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA), fascicolo);
				LOGGER.info("Trovata Fattura: " + iddocumento);
				if ((iddocumento != null) && (iddocumento.length() > 0)) {
					final VWWorkObject workF = fpeh.getWorkflowPrincipale(iddocumento, utente.getIdAoo().toString());

					if ((workF != null)) {
						boolean assegnato = false;
						if ((workF.getFieldValue(MapRedKey.FATTURA_ASSEGNATA) != null) && ("true".equals(workF.getFieldValue(MapRedKey.FATTURA_ASSEGNATA)))) {
							assegnato = true;
						}
						final String idnodoDest = (String) workF.getFieldValue(pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY));
						final String idutenteDest = (String) workF.getFieldValue(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY));
						if (assegnato) {
							closeConnection(connection);
							LOGGER.info("idFascicoloFepa già assegnato");
							throw new RedWSException("idFascicoloFepa già assegnato", ErrorCode.IDFASCICOLO_FEPA_GIA_ASSEGNATO);
						} else if ((!idnodoDest.equals(nododest)) && (!idutenteDest.equalsIgnoreCase(utenteDest))) {
							closeConnection(connection);
							LOGGER.info("idFascicoloFepa già associato ad un altro utente");
							throw new RedWSException("IdFascicoloFepa già associato ad un altro utente", ErrorCode.IDFASCICOLOFEPA_ASSOCIATO_AD_ALTRO_UTENTE);
						}
					}
				} else {
					closeConnection(connection);
					LOGGER.info("idFascicoloFepa non trovato");
					throw new RedWSException("ERROR: idFascicoloFepa non trovato", ErrorCode.FASCICOLO_FEPA_NOT_FOUND_ERROR);
				}
			}

			LOGGER.info("END validazione fascicolo FEPA");
			LOGGER.info("BEGIN inserimento decreto dirigenziale");

			final SalvaDocumentoRedParametriDTO parametri = new SalvaDocumentoRedParametriDTO();
			parametri.setModalita(Constants.Modalita.MODALITA_INS);
			parametri.setIdUfficioMittenteWorkflow(Long.parseLong(nododest));
			parametri.setIdUtenteMittenteWorkflow(Long.parseLong(utenteDest));

			final TipologiaDocumentoDTO tipoDoc = tipologiaDocumentoDAO.getById(idTipologiaDocumento, connection);
			final DetailDocumentRedDTO dto = getDetailDocumentRedDTO(tipoDoc, metadati, oggettoProtocollo, nomeDocumento, utente, nododest, fascicoliFepa, dataProtocollo,
					idDocumento, idFascicolo, content, documento.getContentType().getValue(), false, numeroProtocolloStr, connection);

			idDocRitornato = salvaDocSrv.salvaDocumento(dto, parametri, utente, ProvenienzaSalvaDocumentoEnum.FEPA_FATTURA_DECRETO).getDocumentTitle();

			LOGGER.info("END inserimento decreto dirigenziale");
			if (idDocRitornato == null) {
				LOGGER.error("DOCUMENTO DIRIGENZIALE NON INSERITO");
			} else {
				LOGGER.info("Id decreto dirigenziale: " + idDocRitornato);
			}
		} catch (final RedWSException e) {
			throw e;
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedWSException(e);
		} finally {
			closeConnection(connection);
			logoff(fpeh);
			popSubject(fceh);
		}

		return idDocRitornato;
	}

	private String assegnaFascicoloFattura(final CredenzialiUtenteRed credenziali, final String idFascicoloFEPA, final MapRed mapRed,
			final DocumentoFascicoloContabileDTO inDocumentoFattura, final FileDTO inFileFepa) {
		Connection connection = null;
		String idDocRitornato = null;
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;

		try {
			String utenteCedente = null;
			String utenteDestinatario = null;
			String ufficioDestinatario = null;
			String ufficioMittente = null;

			if (mapRed != null && mapRed.getElemento() != null && !mapRed.getElemento().isEmpty()) {
				utenteCedente = getValueByKeyFromMapRed(mapRed, MapRedKey.UTENTE_CEDENTE_KEY);
				utenteDestinatario = getValueByKeyFromMapRed(mapRed, MapRedKey.UTENTE_DESTINATARIO_KEY);
				ufficioDestinatario = getValueByKeyFromMapRed(mapRed, MapRedKey.UFFICIO_DESTINATARIO_KEY);
			}

			if (credenziali != null) {
				ufficioMittente = credenziali.getIdGruppo() + "";
			}

			if (StringUtils.isNotBlank(utenteCedente) && StringUtils.isNotBlank(utenteDestinatario)) {
				connection = setupConnection(getDataSource().getConnection(), false);
				final UtenteDTO utente = utenteSRV.getById(Long.parseLong(utenteDestinatario));
				fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
				fpeh = new FilenetPEHelper(utente.getFcDTO());

				if (utenteCedente != null && utenteCedente.equals(utenteDestinatario)) {

					LOGGER.info("BEGIN REQUEST AssegnaFascicoloFatturaRequest");
					LOGGER.info("Id facicolo FEPA: " + idFascicoloFEPA);
					LOGGER.info("Validazione del fascicolo fepa");
					final String idControllo = fceh.getIdDocumentoByMetadatoFepa(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA), idFascicoloFEPA);
					if (idControllo != null) {
						throw new RedWSException("ERROR: Fattura già presente in RED", ErrorCode.FATTURA_GIA_PRESENTE);
					}

					final fepa.header.v3.AccessoApplicativo accesso = WebServiceClientProvider.getIstance()
							.getAccessoInterfacciaFepa3Client(TipoAccessoFascicoloFepaEnum.DOCUMENTO_CONTABILE.getTipoAccesso());
					final InterfacciaFascicoli fepa = WebServiceClientProvider.getIstance().getInterfacciaFepa3Client();

					DocumentoFascicoloContabileDTO documentoFattura = inDocumentoFattura;
					if (documentoFattura == null) {
						documentoFattura = getDocumentoFatturaFromFEPA(fepa, accesso, idFascicoloFEPA);
					}

					if (documentoFattura == null || documentoFattura.getIdDocumento() == null) {
						LOGGER.error("ERROR: Il fascicolo non contiene nessuna fattura");
						throw new RedWSException("ERROR: Il fascicolo non contiene nessuna fattura", ErrorCode.NESSUNA_FATTURA_PRESENTE_NEL_FASCICOLO);
					}

					FileDTO fileFepa = inFileFepa;
					if (fileFepa == null) {
						fileFepa = getFileFromFEPA(fepa, accesso, documentoFattura.getIdDocumento(), idFascicoloFEPA);
					}

					final int idAoo = credenziali.getIdAoo();

					final String nododest = getValueByKeyFromMapRed(mapRed, MapRedKey.UFFICIO_DESTINATARIO_KEY);
					final String utenteDest = getValueByKeyFromMapRed(mapRed, MapRedKey.UTENTE_DESTINATARIO_KEY);

					final SalvaDocumentoRedParametriDTO parametri = new SalvaDocumentoRedParametriDTO();
					parametri.setModalita(Modalita.MODALITA_INS);
					parametri.setIdUfficioMittenteWorkflow(Long.parseLong(nododest));
					parametri.setIdUtenteMittenteWorkflow(Long.parseLong(utenteDest));

					final List<TipologiaDocumentoDTO> tipologieDoc = tipologiaDocumentoDAO.getComboTipologieByAoo((long) idAoo, connection);
					TipologiaDocumentoDTO tipologiaFattura = null;
					for (final TipologiaDocumentoDTO tipologia : tipologieDoc) {
						if (tipologia.getDocumentClass().equals(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FATTURA_FEPA_CLASSNAME_FN_METAKEY))) {
							tipologiaFattura = tipologia;
						}
					}
					final DetailDocumentRedDTO dto = getDetailDocumentRedDTO(tipologiaFattura, mapRed, documentoFattura.getDescrizione(), fileFepa.getFileName(), utente,
							nododest, null, null, documentoFattura.getIdDocumento(), idFascicoloFEPA, fileFepa.getContent(), fileFepa.getMimeType(), true, null, connection);

					idDocRitornato = salvaDocSrv.salvaDocumento(dto, parametri, utente, ProvenienzaSalvaDocumentoEnum.FEPA_FATTURA_DECRETO).getDocumentTitle();

					LOGGER.info("END REQUEST AssegnaFascicoloFatturaRequest");

					if (idDocRitornato == null) {
						throw new RedWSException("ERROR: Documento non assegnato", ErrorCode.DOCUMENT_NOT_ASSIGNED);
					} else {
						LOGGER.info("Id decreto dirigenziale: " + idDocRitornato);
					}
				} else {
					if (ufficioMittente != null && ufficioDestinatario != null && ufficioMittente.equals(ufficioDestinatario)) {
						idDocRitornato = riassegnaFatturaFepa(idFascicoloFEPA, utenteCedente, utenteDestinatario, fpeh, fceh);
					} else {
						throw new RedWSException("ERROR: impossibilie riassegnare la fattura. MESSAGE: Il mittente ed il destinatario appartengono a due uffici differenti",
								ErrorCode.UFFICIO_MITTENTE_DESTINATARIO_DIFFERENTI);
					}
				}
			} else {
				throw new RedWSException("ERROR: utente cedente o utente destinatario non definiti.", ErrorCode.UTENTE_CEDENTE_O_DESTINATARIO_NON_DEFINITI);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedWSException(e);
		} finally {
			closeConnection(connection);
			logoff(fpeh);
			popSubject(fceh);
		}

		return idDocRitornato;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IREDServiceFacadeSRV#assegnaFascicoloFattura(it.ibm.red.webservice.model.redservice.header.v1.CredenzialiUtenteRed,
	 *      java.lang.String,
	 *      it.ibm.red.webservice.model.redservice.types.v1.MapRed).
	 */
	@Override
	public String assegnaFascicoloFattura(final CredenzialiUtenteRed credenziali, final String idFascicoloFEPA, final MapRed mapRed) {
		return assegnaFascicoloFattura(credenziali, idFascicoloFEPA, mapRed, null, null);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IREDServiceFacadeSRV#assegnaFascicoloFatturaWithoutFepa(it.ibm.red.webservice.model.redservice.header.v1.CredenzialiUtenteRed,
	 *      java.lang.String,
	 *      it.ibm.red.webservice.model.redservice.types.v1.MapRed,
	 *      java.lang.String, java.lang.String, java.lang.String, byte[],
	 *      java.lang.String).
	 */
	@Override
	public String assegnaFascicoloFatturaWithoutFepa(final CredenzialiUtenteRed credenziali, final String idFascicoloFEPA, final MapRed mapRed, final String idDocumento,
			final String descrizione, final String inFileName, final byte[] inContent, final String inMimeType) {
		final DocumentoFascicoloContabileDTO inDocumentoFattura = new DocumentoFascicoloContabileDTO(idDocumento, descrizione);
		final FileDTO inFileFepa = new FileDTO(inFileName, inContent, inMimeType);
		return assegnaFascicoloFattura(credenziali, idFascicoloFEPA, mapRed, inDocumentoFattura, inFileFepa);
	}

	private static DocumentoFascicoloContabileDTO getDocumentoFatturaFromFEPA(final InterfacciaFascicoli fepa, final fepa.header.v3.AccessoApplicativo accesso,
			final String idFascicoloFEPA) throws fepa.services.v3.GenericFault, fepa.services.v3.SecurityFault {
		LOGGER.info("BEGIN CALL: FEPA.getFascicolo: IdFascicolo: " + idFascicoloFEPA);

		DocumentoFascicoloContabileDTO output = null;
		DocumentoFascicoloType responseFepa = null;

		final RichiestaGetFascicoloType richiesta = new RichiestaGetFascicoloType();
		richiesta.setIdFascicolo(idFascicoloFEPA);
		richiesta.getTipoEstrazione().add(TipoDettaglioEstrazioneFascicoloType.DOCUMENTI);
		richiesta.getTipoEstrazione().add(TipoDettaglioEstrazioneFascicoloType.METADATI_DOCUMENTO);
		richiesta.setTipoFascicolo(TipoAccessoFascicoloFepaEnum.DOCUMENTO_CONTABILE.getTipoFascicolo());

		final RispostaGetFascicolo response = fepa.getFascicolo(accesso, richiesta);

		if (response != null && !"OK".equals(response.getEsito().toString())) {
			LOGGER.info("END CALL: FEPA.getFascicolo");
			if (response.getErrorList() == null) {
				LOGGER.error(ERROR_CALL_FEPA_MSG + response.getEsito());
				throw new RedWSException(response.getEsito().toString(), -1);
			}
			final ServiceErrorType msgError = response.getErrorList().get(0);
			if (msgError != null) {
				LOGGER.error(ERROR_CALL_FEPA_MSG + msgError);
				throw new RedWSException(msgError.getErrorMessageString(), msgError.getErrorCode());
			}
			LOGGER.error(ERROR_CALL_FEPA_MSG + response.getEsito());
			throw new RedWSException(response.getEsito().toString(), -1);
		}

		if (response != null && response.getDettaglioFascicolo() != null) {

			final FascicoloType dettaglioFascicolo = response.getDettaglioFascicolo();
			final List<DocumentoFascicoloType> documenti = dettaglioFascicolo.getDocumenti();

			if (documenti != null && !documenti.isEmpty()) {
				for (int i = 0; i < documenti.size(); i++) {
					final DocumentoFascicoloType documento = documenti.get(i);
					if ("F001".equals(documento.getTipoDocumento().getCodice())) {
						responseFepa = documento;
					}
				}
			}
		}
		if (responseFepa != null) {
			output = new DocumentoFascicoloContabileDTO(responseFepa.getIdDocumento(), responseFepa.getDescrizione());
		}
		return output;
	}

	private static FileDTO getFileFromFEPA(final InterfacciaFascicoli fepa, final fepa.header.v3.AccessoApplicativo accesso, final String idDocumento, final String idFascicoloFEPA)
			throws fepa.services.v3.GenericFault, fepa.services.v3.SecurityFault {
		LOGGER.info("BEGIN CALL: FEPA.downloadFile. IdDocumeto: " + idDocumento);
		final RichiestaDownloadDocumentoFascicoloType richiestaDownload = new RichiestaDownloadDocumentoFascicoloType();
		richiestaDownload.setIdDocumento(idDocumento);
		richiestaDownload.setIdFascicolo(idFascicoloFEPA);
		richiestaDownload.setTipoFascicolo(TipoAccessoFascicoloFepaEnum.DOCUMENTO_CONTABILE.getTipoFascicolo());
		final RispostaDownloadDocumentoFascicolo downloadFileContentResponse = fepa.downloadDocumentoFascicolo(accesso, richiestaDownload);
		LOGGER.info("END CALL: FEPA.downloadFile");

		if (downloadFileContentResponse == null) {
			throw new RedWSException("File non presente");
		}

		if (!downloadFileContentResponse.getEsito().equals(EsitoType.OK)) {
			final ServiceErrorType msgError = downloadFileContentResponse.getErrorList().get(0);
			LOGGER.error("ERROR  FEPA.downloadFile. MESSAGE: " + msgError);
			throw new RedWSException(msgError.getErrorMessageString(), msgError.getErrorCode());
		}

		final DocumentoContentType doc = downloadFileContentResponse.getDocumentoContent();
		final byte[] documentContent = doc.getContent();
		final String contentType = doc.getMimeType();
		String filename = null;
		if (documentContent != null) {
			filename = doc.getFileName();
		}
		return new FileDTO(filename, documentContent, contentType);
	}

	/**
	 * @param tipologiaDocumento
	 * @param metadati
	 * @param oggettoProtocollo
	 * @param nomeDocumento
	 * @param utente
	 * @param nododest
	 * @param fascicoliFepa
	 * @param dataProtocollo
	 * @param idDocumento
	 * @param idFascicolo
	 * @param content
	 * @param contentType
	 * @param isFattura
	 * @param numeroProtocollo
	 * @param connection
	 * @return
	 */
	private DetailDocumentRedDTO getDetailDocumentRedDTO(final TipologiaDocumentoDTO tipologiaDocumento, final MapRed metadati, final String oggettoProtocollo,
			final String nomeDocumento, final UtenteDTO utente, final String nododest, final List<String> fascicoliFepa, final String dataProtocollo, final String idDocumento,
			final String idFascicolo, final byte[] content, final String contentType, final boolean isFattura, final String numeroProtocollo, final Connection connection) {
		final DetailDocumentRedDTO dto = new DetailDocumentRedDTO();
		PropertiesProvider pp = PropertiesProvider.getIstance();
		final TipoProcedimento tipologiaProcedimento = tipoProcedimentoDAO.getTipiProcedimentoByTipologiaDocumento(connection, tipologiaDocumento.getIdTipologiaDocumento()).get(0);
		dto.setIdTipologiaDocumento(tipologiaDocumento.getIdTipologiaDocumento());
		dto.setAssegnazioni(new ArrayList<>());
		final Date now = new Date();
		dto.getAssegnazioni().add(new AssegnazioneDTO(getValueByKeyFromMapRed(metadati, MapRedKey.MOTIVO_ASSEGNAZIONE_KEY), TipoAssegnazioneEnum.COMPETENZA,
				DateUtils.dateToString(now, null), now, null, utente, new UfficioDTO(Long.parseLong(nododest), null)));
		if (isFattura) {
			dto.setIdCategoriaDocumento(CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0]);
		} else {
			dto.setIdCategoriaDocumento(CategoriaDocumentoEnum.DOCUMENTO_USCITA.getIds()[0]);
		}
		dto.setFormatoDocumentoEnum(FormatoDocumentoEnum.ELETTRONICO);
		dto.setIdFormatoDocumento(FormatoDocumentoEnum.ELETTRONICO.getId().intValue());
		dto.setOggetto(oggettoProtocollo);
		dto.setIdTipologiaProcedimento((int) tipologiaProcedimento.getTipoProcedimentoId());
		dto.setIdAOO(utente.getIdAoo().intValue());
		dto.setDataArchiviazione(new Date());
		dto.setMetadatiDinamici(new ArrayList<>());

		if (fascicoliFepa != null && !fascicoliFepa.isEmpty()) {
			final MetadatoDinamicoDTO elencoFatture = new MetadatoDinamicoDTO(pp.getParameterByKey(PropertiesNameEnum.ELENCO_FATTURE_FEPA),
					fascicoliFepa != null ? fascicoliFepa.stream().collect(Collectors.joining(",", "{", "}")) : null);
			elencoFatture.setCardinality(Cardinality.LIST_AS_INT);
			dto.getMetadatiDinamici().add(elencoFatture);
		}

		final MetadatoDinamicoDTO fascicoloFepa = new MetadatoDinamicoDTO(pp.getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA), idFascicolo);
		dto.getMetadatiDinamici().add(fascicoloFepa);
		final MetadatoDinamicoDTO documentoFepa = new MetadatoDinamicoDTO(pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_FEPA), idDocumento);
		dto.getMetadatiDinamici().add(documentoFepa);
		dto.setMittenteContatto(contattoDAO.getContattoByDenominazione(pp.getParameterByKey(PropertiesNameEnum.FEPA_MITTENTE), connection));
		dto.setNomeFile(nomeDocumento);
		dto.setDescrizioneFascicoloProcedimentale(tipologiaDocumento.getDescrizione() + " - " + oggettoProtocollo);
		if (numeroProtocollo == null) {
			try {
				dto.setNumeroProtocollo(Integer.parseInt(getValueByKeyFromMapRed(metadati, MapRedKey.NUMERO_FATTURA_SIGI)));
			} catch (final Exception e) {
				LOGGER.warn("ID Fattura SIGI non numerico o non presente.", e);
				dto.setNumeroProtocollo(null);
			}
		} else {
			dto.setNumeroProtocollo(Integer.parseInt(numeroProtocollo));
		}
		dto.setMotivoAssegnazione(getValueByKeyFromMapRed(metadati, MapRedKey.MOTIVO_ASSEGNAZIONE_KEY));
		if (dataProtocollo == null) {
			try {
				dto.setAnnoProtocollo(Integer.parseInt(getValueByKeyFromMapRed(metadati, MapRedKey.ANNO_FATTURA_SIGI)));
			} catch (final Exception e) {
				LOGGER.warn("Anno Fattura SIGI non numerico o non presente.", e);
				dto.setAnnoProtocollo(null);
			}
		} else {
			dto.setAnnoProtocollo(getAnno(dataProtocollo));
			dto.setDataProtocollo(DateUtils.parseDate(dataProtocollo));
		}
		dto.setMimeType(contentType);
		dto.setContent(content);
		return dto;
	}

	private static int getAnno(final String dataProtocollo) {
		// esempio gg/mm/yyyy
		int annoN = -1;
		try {
			final String[] componenti = dataProtocollo.split("/");
			final String anno = componenti[2];
			annoN = Integer.parseInt(anno);
		} catch (final Exception e) {
			LOGGER.warn(e);
			annoN = 0;
		}
		return annoN;
	}

	private static String getValueByKeyFromMapRed(final MapRed mapRed, final String key) {
		String value = null;
		if (mapRed != null && mapRed.getElemento() != null && !mapRed.getElemento().isEmpty()) {
			for (int i = 0; i < mapRed.getElemento().size(); i++) {
				if ((mapRed.getElemento().get(i) != null && mapRed.getElemento().get(i).getKey() != null) && (mapRed.getElemento().get(i).getKey().equals(key))) {
					value = mapRed.getElemento().get(i).getValore();
				}
			}
		}
		return value;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IREDServiceFacadeSRV#annullaDocumento(java.lang.String,
	 *      java.lang.String, java.lang.String).
	 */
	@Override
	public void annullaDocumento(final String idUtente, final String idDocumento, final String motivoAnnullamento) {
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;

		try {
			final UtenteDTO utente = utenteSRV.getById(Long.parseLong(idUtente));
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			fpeh = new FilenetPEHelper(utente.getFcDTO());

			final Document docAnnullato = fceh.getDocumentByDTandAOO(idDocumento, utente.getIdAoo());

			// Si verifica che sia possibile annullare il decreto

			final VWWorkObject wo = fpeh.getWorkflowPrincipale(idDocumento, null);

			if (wo == null) {
				throw new RedWSException("ERROR: Non esiste un singolo workflow da annullare: o non sono presenti workflow o ne esistono più di uno.",
						ErrorCode.DOES_NOT_EXISTS_ANY_ANNULABLE_DD_WORKFLOW_ERROR);
			}
			if (((String[]) TrasformerPE.getMetadato(wo, "wobDichiarazioneServiziResi"))[0].length() != 0) {
				throw new RedWSException("ERROR: Il Workflow non è annullabile.", ErrorCode.DD_WORKFLOW_NOT_ANNULLABLE_ERROR);
			}

			documentoSRV.annullaDocumento(docAnnullato, motivoAnnullamento, fpeh, fceh, wo, utente.getIdAoo());
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			popSubject(fceh);
			logoff(fpeh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IREDServiceFacadeSRV#checkIn(java.lang.String,
	 *      java.lang.String, java.util.Map, java.util.List, byte[]).
	 */
	@Override
	public RispostaDocRed checkIn(final String documentTitle, final String idUtente, final Map<String, Object> metadatiMap, final List<DocumentoRed> allegati, final byte[] content) throws IOException {
		final RispostaDocRed risposta = new RispostaDocRed();
		IFilenetCEHelper fceh = null;

		try (
			InputStream contentIS = new ByteArrayInputStream(content);
		){
			final ObjectFactory of = new ObjectFactory();
			final UtenteDTO utente = utenteSRV.getById(Long.parseLong(idUtente));
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			Document d = fceh.getDocumentByDTandAOO(documentTitle, utente.getIdAoo());
			d = (Document) d.get_Reservation();
			fceh.checkin(d, contentIS, null, null, metadatiMap, null, URLConnection.guessContentTypeFromStream(contentIS), utente.getIdAoo());
			
			if (allegati != null && !allegati.isEmpty()) {
				throw new RedWSException(
						"FWS al check out esegue il checkout del solo documento principale, al checkin del documento principale può operare anche sugli allegati. Non implementato!!");
			}

			risposta.setEsito(true);
			risposta.setGuid(of.createRispostaRedGuid(d.get_Id().toString()));
			risposta.setIdVersione(d.get_MajorVersionNumber());
		} finally {
			popSubject(fceh);
		}

		return risposta;
	}

	/**
	 * @see it.ibm.red.business.service.facade.IREDServiceFacadeSRV#undoCheckOut(java.lang.String,
	 *      java.lang.String).
	 */
	@Override
	public final void undoCheckOut(final String documentTitle, final String idUtente) {
		IFilenetCEHelper fceh = null;

		try {

			final UtenteDTO utente = utenteSRV.getById(Long.parseLong(idUtente));
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			final Document docToUndoCheckout = fceh.getDocumentByDTandAOO(documentTitle, utente.getIdAoo());

			// Si controlla prima che il documento sia in stato di Checkout.
			if (fceh.isCheckOut(docToUndoCheckout)) {
				fceh.cancelCheckout(docToUndoCheckout);
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante il tentativo di cancellazione del checkout del Documento con documentTitle -->: " + documentTitle + " " + e.getMessage(), e);
			throw new RedException("Errore durante il tentativo di cancellazione del checkout del Documento con documentTitle -->: " + documentTitle + " " + e.getMessage(),
					e);
		} finally {
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.IREDServiceFacadeSRV#modificaDecreto(java.lang.String,
	 *      java.lang.String, java.lang.String, java.util.List, java.util.List,
	 *      byte[], java.lang.String).
	 */
	@Override
	public final void modificaDecreto(final String idNodo, final String idUtente, final String idDocumentoDecreto, final List<String> fattureDaAggiungere,
			final List<String> fattureDaRimuovere, final byte[] content, final String contentType) {
		LOGGER.info("[enter] modificaDecreto idNodo: [" + idNodo + "], idDocumentoDecreto: [" + idDocumentoDecreto + "], fattureDaAggiungere : [" + fattureDaAggiungere
				+ "] , fattureDaRimuovere : [" + fattureDaRimuovere + "]");
		final List<String> docId2Add = new ArrayList<>();
		final List<String> docId2Remove = new ArrayList<>();
		String queryFilter = Constants.EMPTY_STRING;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		PropertiesProvider pp = PropertiesProvider.getIstance();
		try {
			final UtenteDTO utente = utenteSRV.getById(Long.parseLong(idUtente));
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			queryFilter = "idDocumento = " + idDocumentoDecreto;
			LOGGER.info("ricerca del workflow del decreto. [" + queryFilter + "]");
			List<PEDocumentoDTO> workflows = (List<PEDocumentoDTO>) fpeh.getWorkflowByFilter("NSD_Roster", null, queryFilter);
			if (workflows.isEmpty()) {
				throw new RedWSException("ERROR: Decreto non trovato in RED: idDocumentoDecreto = " + idDocumentoDecreto, ErrorCode.DD_NON_TROVATO);
			}

			final VWWorkObject wob = fpeh.getWorkFlowByWob(workflows.get(0).getWobNumber(), true);
			if (!"".equals(((String[]) wob.getFieldValue("wobDichiarazioneServiziResi"))[0])) {
				throw new RedWSException("ERROR: Decreto già associato ad un DSR: idDocumentoDecreto = " + idDocumentoDecreto, ErrorCode.DD_GIA_ASSOCIATO_AD_UN_DSR);
			}

			if (fattureDaRimuovere != null && !fattureDaRimuovere.isEmpty()) {
				LOGGER.info("Verifica che tutte le fatture da rimuovere siano realmente associate al decreto.");
				final List<String> fattureAssociate = Arrays.asList((String[]) wob.getFieldValue(pp.getParameterByKey(PropertiesNameEnum.ELENCO_FATTURE_FEPA)));
				if (!fattureAssociate.containsAll(fattureDaRimuovere)) {
					throw new RedWSException("ERROR: Il decreto non contiene tutte le fatture da rimuovere: idDocumentoDecreto = " + idDocumentoDecreto,
							ErrorCode.DD_NON_CONTENENTE_TUTTE_LE_FATTURE_DA_RIMUOVERE);
				}
				LOGGER.info("dati gli idFascicoliFEPA, si recuperano gli idDocumento");
				for (final String fattura : fattureDaRimuovere) {
					final String idFattura = fceh.getIdDocumentoByMetadatoFepa(pp.getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA), fattura);
					queryFilter = pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY) + " = '" + idFattura + "'";
					workflows = (List<PEDocumentoDTO>) fpeh.getWorkflowByFilter(DocumentQueueEnum.FATTURE_DA_LAVORARE.getName(), null, queryFilter);
					if (workflows.isEmpty()) {
						throw new RedWSException(LABEL_ERROR_FATTURA + fattura + "] non trovata.", ErrorCode.FATTURA_NON_TROVATA);
					} else {
						docId2Remove.add(idFattura);
					}
				}
			}

			if (fattureDaAggiungere != null && !fattureDaAggiungere.isEmpty()) {
				LOGGER.info("Verifica che tutte le fatture da aggiungere non siano associate a decreto e che siano dell'utente corrente.");
				for (final String fattura : fattureDaAggiungere) {
					final String idFattura = fceh.getIdDocumentoByMetadatoFepa(pp.getParameterByKey(PropertiesNameEnum.METADATO_FASCICOLO_IDFASCICOLOFEPA), fattura);
					queryFilter = pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY) + " = '" + idFattura + "'";
					workflows = (List<PEDocumentoDTO>) fpeh.getWorkflowByFilter(DocumentQueueEnum.FATTURE_DA_LAVORARE.getName(), null, queryFilter);
					VWWorkObject wobFattura = null;
					if (!workflows.isEmpty()) {
						wobFattura = fpeh.getWorkFlowByWob(workflows.get(0).getWobNumber(), true);
						if (!idUtente.equals(wobFattura.getFieldValue(pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY)).toString())) {
							throw new RedWSException(LABEL_ERROR_FATTURA + fattura + "] già associata all'utente [" + wobFattura.getFieldValue("idUtenteDestinatario") + "].",
									ErrorCode.FATTURA_GIA_ASSOCIATA_ALL_UTENTE);
						}
					} else {
						throw new RedWSException(LABEL_ERROR_FATTURA + fattura + "] non trovata.", ErrorCode.FATTURA_NON_TROVATA);
					}
				}
			}
			Document docDecreto = null;
			try {
				docDecreto = fceh.getDocumentByDTandAOO(idDocumentoDecreto, utente.getIdAoo());
				// Si recupera il folder contente il decreto.
				Folder folderDecreto = null;
				final String folderDocumenti = pp.getParameterByKey(PropertiesNameEnum.FOLDER_DOCUMENTI_CE_KEY);

				@SuppressWarnings("unchecked")
				final Iterator<Folder> folders = docDecreto.get_FoldersFiledIn().iterator();
				while (folders.hasNext()) {
					final Folder folder = folders.next();
					LOGGER.info("verifica folder  [" + folder.get_Name() + "]");
					if (!folder.get_Name().equals(folderDocumenti)) {
						folderDecreto = folder;
						break;
					}
				 }
				 
				 if (folderDecreto == null) {
						throw new RedWSException("Impossibile trovare il folder del decreto.");
				 }
				 
				 LOGGER.info("il folder del decreto è [" + folderDecreto.get_Name() + "]");
				 if (content != null) {
					 
					 final Id tempId = com.filenet.api.util.Id.createId();
					 docDecreto.checkout(ReservationType.EXCLUSIVE, tempId, null, null);
					 docDecreto.save(RefreshMode.REFRESH);
					  
					 final String fileName = docDecreto.getProperties().getStringValue("nomeFile");
				 
					 final Document reservation = (Document) docDecreto.get_Reservation();
					 fceh.checkin(reservation, new ByteArrayInputStream(content), null, null, null, fileName, contentType, utente.getIdAoo());
					 
				 }
				 
				 for (final String doc : docId2Remove) {
					 LOGGER.info("Aggiunta del documento [" + doc + "] ad decreto");
					 final com.filenet.api.core.Document docFattura = fceh.getDocumentByDTandAOO(doc, utente.getIdAoo());
					 final ReferentialContainmentRelationship rf = folderDecreto.unfile(docFattura);
					 rf.save(RefreshMode.REFRESH);
				 }
				 
				 for (final String doc : docId2Add) {
					 LOGGER.info("Rimozione del documento [" + doc + "] dal decreto");
					 final com.filenet.api.core.Document docFattura = fceh.getDocumentByDTandAOO(doc, utente.getIdAoo());
					 final ReferentialContainmentRelationship rf = folderDecreto.file(docFattura, AutoUniqueName.NOT_AUTO_UNIQUE, docFattura.get_Name(), null);
					 rf.save(RefreshMode.REFRESH);
				 }
				 
			} catch (final Exception ex) {
				LOGGER.warn(ex);
				if (docDecreto != null && docDecreto.get_IsReserved()) {
					docDecreto.cancelCheckout();
				}
				throw new RedWSException("ERROR: si è verificato un errore generico con il content engine.", ErrorCode.GENERIC_FILENET_CE_ERROR);
			}

			LOGGER.info("Fascicolazione delle fatture avvenuta con successo, si procede con il dispatch del decreto");

			final Map<String, Object> metadati = new HashMap<>();
			metadati.put("FattureDaAggiungere", fattureDaAggiungere.toArray());
			metadati.put("FattureDaEliminare", fattureDaRimuovere.toArray());

			if (!fpeh.nextStep(wob, metadati, "SYS_ModificaDecreto")) {
				throw new RedWSException("Errore nell'avanzamento del WF");
			}
			LOGGER.info("Operazione eseguita con successo");
		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}
	}
}
