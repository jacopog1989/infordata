package it.ibm.red.business.dto;

import java.util.Date;

/**
 * @author SimoneLungarella
 * DTO per la gestione dei risultati di ricerca per l'elenco divisionale.
 */
public class RisultatoRicercaElencoDivisionaleDTO extends AbstractDTO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 4442366554731499787L;

	/**
	 * Numero identificativo.
	 */
	private int numeroElenco;
	
	/**
	 * Anno elenco divisionale.
	 */
	private int annoElenco;
	
	/**
	 * Data protocollo.
	 */
	private Date dataProtocollo;
	
	/**
	 * Descrizione del titolario.
	 */
	private String descrizioneTitolario;
	
	/**
	 * Oggetto.
	 */
	private String oggetto;
	
	/**
	 * Descrizione tipologia documento.
	 */
	private String tipologiaDocumento;

	/**
	 * Descrizione ufficio o ufficio - utente del contatto mittente.
	 */
	private String descrizioneMittente;
	
	/**
	 * Descrizione utente assegnatario.
	 */
	private String utenteAssegnatario;
	
	/**
	 * Numero documento.
	 */
	private int numeroDocumento;
	
	/**
	 * Costruttore vuoto, inizializza i parametri di tipo String.
	 */
	public RisultatoRicercaElencoDivisionaleDTO() {
		
		initParams();
	}

	/**
	 * Restituisce il numero.
	 * @return numero
	 */
	public int getNumeroElenco() {
		return numeroElenco;
	}

	/**
	 * Imposta il numero identificativo.
	 * @param numeroElenco
	 */
	public void setNumeroElenco(int numeroElenco) {
		this.numeroElenco = numeroElenco;
	}

	/**
	 * Restituisce l'anno.
	 * @return anno
	 */
	public int getAnnoElenco() {
		return annoElenco;
	}

	/**
	 * Imposta l'anno.
	 * @param annoElenco
	 */
	public void setAnnoElenco(int annoElenco) {
		this.annoElenco = annoElenco;
	}

	/**
	 * Restituisce la data protocollo.
	 * @return data protocollo
	 */
	public Date getDataProtocollo() {
		return dataProtocollo;
	}

	/**
	 * Imposta la data protocollo.
	 * @param dataProtocollo
	 */
	public void setDataProtocollo(Date dataProtocollo) {
		this.dataProtocollo = dataProtocollo;
	}

	/**
	 * Restituisce la descrizione del titolario.
	 * @return descrizione titolario
	 */
	public String getDescrizioneTitolario() {
		return descrizioneTitolario;
	}

	/**
	 * Imposta la descrizione del titolario.
	 * @param descrizioneTitolario
	 */
	public void setDescrizioneTitolario(String descrizioneTitolario) {
		this.descrizioneTitolario = descrizioneTitolario;
	}

	/**
	 * Restituisce l'oggetto.
	 * @return oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * Imposta l'oggetto.
	 * @param oggetto
	 */
	public void setOggetto(String oggetto) {
		this.oggetto = oggetto;
	}

	/**
	 * Restituisce la tipologia documento.
	 * @return tipologia documento
	 */
	public String getTipologiaDocumento() {
		return tipologiaDocumento;
	}

	/**
	 * Imposta la tipologia documento.
	 * @param tipologiaDocumento
	 */
	public void setTipologiaDocumento(String tipologiaDocumento) {
		this.tipologiaDocumento = tipologiaDocumento;
	}

	/**
	 * Restituisce la descrizione del mittente.
	 * @return descrizione mittente
	 */
	public String getDescrizioneMittente() {
		return descrizioneMittente;
	}

	/**
	 * Imposta la descrizione del mittente.
	 * @param descrizioneMittente
	 */
	public void setDescrizioneMittente(String descrizioneMittente) {
		this.descrizioneMittente = descrizioneMittente;
	}

	/**
	 * Restituisce l'utente assegnatario.
	 * @return utente assegnatario
	 */
	public String getUtenteAssegnatario() {
		return utenteAssegnatario;
	}

	/**
	 * Imposta l'utente assegnatario.
	 * @param utenteAssegnatario
	 */
	public void setUtenteAssegnatario(String utenteAssegnatario) {
		this.utenteAssegnatario = utenteAssegnatario;
	}
	
	/**
	 * Restituisce il numero del documento.
	 * 
	 * @return numero documento
	 */
	public int getNumeroDocumento() {
		return numeroDocumento;
	}

	/**
	 * Imposta il numero del documento.
	 * 
	 * @param numeroDocumento
	 */
	public void setNumeroDocumento(int numeroDocumento) {
		this.numeroDocumento = numeroDocumento;
	}

	/**
	 * Inizializzazione parametri String.
	 */
	private void initParams() {
		setOggetto("");
		setDescrizioneMittente("");
		setUtenteAssegnatario("");
		setTipologiaDocumento("");
		setDescrizioneTitolario("");
	}
	
}
