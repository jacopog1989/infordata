package it.ibm.red.business.enums;

/**
 * Enum che definisce il tipo di esecuzione.
 */
public enum ExecutionTypeEnum {
	
	/**
	 * Valore.
	 */
	SINGLE,

	/**
	 * Valore.
	 */
	MULTI;
	
}
