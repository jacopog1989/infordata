package it.ibm.red.business.enums;

/**
 * The Enum LogDownloadContentEnum.
 *
 * @author CPIERASC
 * 
 *         Tipologie di codifica.
 */
public enum LogDownloadContentEnum {

	/**
	 * Documento originale.
	 */
	DOCUMENTO_ORIGINALE("Documento originale"), 

	/**
	 * Allegato.
	 */
	ALLEGATO("Allegato"), 

	/**
	 * Contributo.
	 */
	CONTRIBUTO("Contributo");

	/**
	 * Descrizione.
	 */
	private String label;
	
	/**
	 * Costruttore.
	 * 
	 * @param inLabel	descrizione
	 */
	LogDownloadContentEnum(final String inLabel) {
		label = inLabel;
	}

	/**
	 * Getter label.
	 * 
	 * @return	label
	 */
	public String getLabel() {
		return label;
	}

}