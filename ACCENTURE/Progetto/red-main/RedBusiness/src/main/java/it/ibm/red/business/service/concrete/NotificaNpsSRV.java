package it.ibm.red.business.service.concrete;

import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.core.Document;

import filenet.vw.api.VWRosterQuery;
import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.IEventoLogDAO;
import it.ibm.red.business.dao.IGestioneNotificheEmailDAO;
import it.ibm.red.business.dao.INotificaNpsDAO;
import it.ibm.red.business.dao.INpsConfigurationDAO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnatarioDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.EventoLogDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.MessaggioEmailDTO;
import it.ibm.red.business.dto.NotificaAzioneNpsDTO;
import it.ibm.red.business.dto.NotificaNpsDTO;
import it.ibm.red.business.dto.NotificaOperazioneNpsDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.TipologiaDTO;
import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.FilenetStatoFascicoloEnum;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.MezzoSpedizioneEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.PostaEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ProvenienzaSalvaDocumentoEnum;
import it.ibm.red.business.enums.SalvaDocumentoErroreEnum;
import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;
import it.ibm.red.business.enums.StatoCodaEmailEnum;
import it.ibm.red.business.enums.StatoElabMessaggioPostaNpsEnum;
import it.ibm.red.business.enums.StatoElabNotificaNpsEnum;
import it.ibm.red.business.enums.TipoAllaccioEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.enums.TipoNotificaAzioneNPSEnum;
import it.ibm.red.business.enums.TipoProtocolloEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.nps.dto.AllegatoNpsDTO;
import it.ibm.red.business.nps.dto.DocumentoNpsDTO;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.persistence.model.NpsConfiguration;
import it.ibm.red.business.persistence.model.TipoProcedimento;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.ICodaMailSRV;
import it.ibm.red.business.service.ICodeDocumentiSRV;
import it.ibm.red.business.service.IFascicoloSRV;
import it.ibm.red.business.service.INotificaNpsSRV;
import it.ibm.red.business.service.INpsSRV;
import it.ibm.red.business.service.ISecuritySRV;
import it.ibm.red.business.service.ISpeditoSRV;
import it.ibm.red.business.service.ITipoProcedimentoSRV;
import it.ibm.red.business.service.ITipologiaDocumentoSRV;
import it.ibm.red.business.service.ITitolarioSRV;
import it.ibm.red.business.service.IWorkflowSRV;
import it.ibm.red.business.service.facade.IEventoLogFacadeSRV;
import it.ibm.red.business.service.facade.IRubricaFacadeSRV;
import it.ibm.red.webservice.model.interfaccianps.dto.ParametersAzioneRispostaAutomatica;
import it.ibm.red.webservice.model.interfaccianps.dto.ParametersAzioneRispostaAutomatica.InfoProtocollo;
import it.ibm.red.webservice.model.interfaccianps.dto.ParametersElaboraNotificaAzione;
import it.ibm.red.webservice.model.interfaccianps.dto.ParametersElaboraNotificaOperazione;

/**
 * Service notifica NPS.
 */
@Service
@Qualifier("NotificaNpsSRV")
public class NotificaNpsSRV extends CreaDocumentoRedUscitaAbstractSRV implements INotificaNpsSRV {

	private static final String AND_D = " AND d.";

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 9139106437661064833L;

	/**
	 * Label elaboraNotificaAzioneRispondiA. Occorre appendere il protocollo al
	 * messaggio.
	 */
	private static final String PROTOCOLLO_ELABORA_NOTIFICA_LABEL = "elaboraNotificaAzioneRispondiA -> [Protocollo: ";

	/**
	 * Label protocollo ingresso. Occorre appendere questo messaggio al protocollo
	 * ingresso.
	 */
	private static final String PROTOCOLLO_INGRESSO_LABEL = "] - Protocollo ingresso ";

	/**
	 * Messaggio errore recupero protocollo ingresso.
	 */
	private static final String ERROR_RECUPERO_PROTOCOLLO_MSG = "Impossibile recuperare il protocollo in ingresso a cui agganciare la risposta";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NotificaNpsSRV.class.getName());

	/**
	 * Servizio.
	 */
	@Autowired
	private IAooSRV aooSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private ICodaMailSRV codaMailSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private INotificaNpsDAO notificaNpsDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private INpsConfigurationDAO npsConfigurationDAO;

	/**
	 * Servizio.
	 */
	@Autowired
	private INpsSRV npsSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IFascicoloSRV fascicoloSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IEventoLogFacadeSRV eventLogSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private ITitolarioSRV titolarioSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private ITipologiaDocumentoSRV tipoDocSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private ITipoProcedimentoSRV tipoProcedimentoSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IRubricaFacadeSRV rubricaSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private ISpeditoSRV speditoSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private IEventoLogDAO eventDAO;

	/**
	 * Servizio.
	 */
	@Autowired
	private ICodeDocumentiSRV codeDocumentiSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private ISecuritySRV securitySRV;

	/**
	 * DAO.
	 */
	@Autowired
	private IGestioneNotificheEmailDAO gestioneNotificheEmailDAO;
	
	/**
	 * Servizio.
	 */
	@Autowired
	private IWorkflowSRV workflowSRV;

	/**
	 * @see it.ibm.red.business.service.facade.INotificaNpsFacadeSRV#
	 *      accodaNotificaOperazione(
	 *      it.ibm.red.webservice.model.interfaccianps.dto.
	 *      ParametersElaboraNotificaOperazione).
	 */
	@Override
	public Long accodaNotificaOperazione(final ParametersElaboraNotificaOperazione richiesta) {
		final Integer tipoProtocollo = richiesta.isEntrata() ? TipoProtocolloEnum.ENTRATA.getId() : TipoProtocolloEnum.USCITA.getId();

		final NotificaOperazioneNpsDTO notificaOperazione = new NotificaOperazioneNpsDTO(richiesta.getMessageIdNPS(), richiesta.getIdMessaggioNPS(),
				richiesta.getMessageIdMail(), richiesta.getCodiceAooNPS(), richiesta.getDataProtocollo(), richiesta.getNumeroProtocollo(), richiesta.getAnnoProtocollo(),
				tipoProtocollo, richiesta.getIdProtocollo(), richiesta.getDataNotifica(), richiesta.getTipoNotifica(), richiesta.getEsitoNotifica(),
				richiesta.getEmailDestinatario());

		return accodaNotifica(notificaOperazione);
	}

	/**
	 * @see it.ibm.red.business.service.facade.INotificaNpsFacadeSRV#
	 *      accodaNotificaAzione( it.ibm.red.webservice.model.interfaccianps.dto.
	 *      ParametersElaboraNotificaAzione, java.lang.Long).
	 */
	@Override
	public Long accodaNotificaAzione(final ParametersElaboraNotificaAzione richiesta, final Long idTrack) {
		Object datiAzione = null;
		Integer tipoProtocollo = null;

		if (TipoNotificaAzioneNPSEnum.isRispostaAutomatica(richiesta.getTipoAzione())) {
			datiAzione = richiesta.getDatiRispondiA();
			tipoProtocollo = TipoProtocolloEnum.USCITA.getId();
		} else {
			tipoProtocollo = TipoProtocolloEnum.NESSUNO.getId();
		}

		final NotificaAzioneNpsDTO notificaAzione = new NotificaAzioneNpsDTO(richiesta.getMessageIdNPS(), richiesta.getIdMessaggioNPS(), richiesta.getCodiceAooNPS(),
				richiesta.getDataProtocollo(), richiesta.getNumeroProtocollo(), richiesta.getAnnoProtocollo(), richiesta.getIdProtocollo(), tipoProtocollo,
				richiesta.getDataAzione(), richiesta.getTipoAzione(), datiAzione);

		return accodaNotifica(notificaAzione);
	}

	/**
	 * @param notifica
	 * @return
	 */
	private Long accodaNotifica(final NotificaNpsDTO notifica) {
		Connection con = null;
		Long idCoda = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			final NpsConfiguration npsConfig = npsConfigurationDAO.getByCodiceAoo(notifica.getCodiceAooNPS(), con);

			final Aoo aoo = aooSRV.recuperaAoo(npsConfig.getIdAoo(), con);

			// Se l'AOO è abilitato alla gestione della posta in modalità esterna
			// interoperabile (semi-automatica o automatica)
			if (aoo.getPostaInteropEsterna() == PostaEnum.POSTA_INTERNA.getValue().intValue()) {
				throw new RedException("L'AOO non è abilitato alla gestione dei messaggi in modalità esterna interoperabile. Codice AOO: " + aoo.getCodiceAoo());
			}

			notifica.setIdAoo(aoo.getIdAoo());

			idCoda = notificaNpsDAO.inserisciNotificaInCoda(notifica, con);

		} catch (final Exception e) {
			LOGGER.error(
					"Errore durante l'inserimento in coda di una richiesta di elaborazione della notifica di operazione " + notifica.getMessageIdNPS() + " eseguita da NPS.",
					e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

		return idCoda;
	}

	/**
	 * @see it.ibm.red.business.service.facade.INotificaNpsFacadeSRV#
	 *      recuperaRichiestaElabDaLavorare(long).
	 */
	@Override
	public NotificaNpsDTO recuperaProssimaRichiestaElabDaLavorare(final long idAoo) {
		Connection con = null;
		NotificaNpsDTO notifica = null;
		try {

			con = setupConnection(getDataSource().getConnection(), true);

			// Recupera la richiesta
			notifica = notificaNpsDAO.getAndLockNextNotifica(idAoo, con);

			// Si aggiorna lo stato della richiesta in coda impostando la presa in carico
			if (notifica != null) {
				notificaNpsDAO.aggiornaStatoNotifica(notifica.getIdCoda(), StatoElabNotificaNpsEnum.IN_ELABORAZIONE.getId(), null, con);
			}

			commitConnection(con);

		} catch (final Exception e) {
			rollbackConnection(con);
			LOGGER.error("Errore durante il recupero della prima richiesta di elaborazione della notifica di operazione eseguita da NPS.", e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

		return notifica;
	}

	/**
	 * @see it.ibm.red.business.service.facade.INotificaNpsFacadeSRV#
	 *      elaboraNotificaSpedizione(it.ibm.red.business.dto.NotificaOperazioneNpsDTO).
	 */
	@Override
	public void elaboraNotificaSpedizione(final NotificaOperazioneNpsDTO item) {
		Connection con = null;
		IFilenetCEHelper fceh = null;
		Long idCoda = null;
		String documentTitle = null;

		try {
			con = setupConnection(getDataSource().getConnection(), true);

			idCoda = item.getIdCoda();

			final int idAoo = item.getIdAoo().intValue();
			// Recupera l'AOO
			final Aoo aoo = aooSRV.recuperaAoo(idAoo, con);

			// Inizializza accesso a CE filenet
			final AooFilenet aooFilenet = aoo.getAooFilenet();
			fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getObjectStore(), aoo.getIdAoo());

			// Recupera documento tramite il protocollo
			final Document docFnItem = getDocFileNetByProtocollo(item.getIdProtocollo(), item.getNumeroProtocollo(), item.getAnnoProtocollo(), item.getTipoProtocollo(), fceh,
					idAoo);

			// Recupera item in coda mail tramite documentTitle e indirizzo di spedizione
			documentTitle = (String) TrasformerCE.getMetadato(docFnItem, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
			final String emailDestinatario = item.getEmailDestinatario();
			final MessaggioEmailDTO codaEmailItem = codaMailSRV.getItemByDocumentTitleAndEmailDestAndStato(documentTitle, emailDestinatario,
					StatoCodaEmailEnum.ATTESA_SPEDIZIONE_NPS.getStatus(), con);
			if (codaEmailItem == null) {
				throw new RedException("Attesa spedizione in coda mail non trovata per il documento " + documentTitle + " e destinatario " + emailDestinatario);
			}

			LOGGER.info("Aggiornamento spedizione [" + idCoda + "," + item.getMessageIdMail() + "] per il documento " + codaEmailItem.getDocumentTitle() + " ed invio "
					+ codaEmailItem.getIdNotifica());

			// Gestisci invio mail
			codaEmailItem.setMsgID(item.getMessageIdMail());
			codaMailSRV.gestisciInvioNPS(codaEmailItem, idAoo, con, fceh);

			if (documentTitle != null && !documentTitle.isEmpty()) {
				// Si aggiorna lo stato della richiesta in coda impostando l'avvenuta
				// elaborazione
				notificaNpsDAO.aggiornaStatoNotifica(idCoda, StatoElabMessaggioPostaNpsEnum.ELABORATA.getId(), null, documentTitle, con);
			} else {
				// Si aggiorna lo stato della richiesta in coda impostando l'avvenuta
				// elaborazione
				notificaNpsDAO.aggiornaStatoNotifica(idCoda, StatoElabMessaggioPostaNpsEnum.ELABORATA.getId(), null, con);
			}

			commitConnection(con);
		} catch (final Exception e) {
			gestisciErroreElaborazioneNotifica(idCoda, documentTitle, e, con);
		} finally {
			closeConnection(con);
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.INotificaNpsFacadeSRV#
	 *      elaboraNotificaAzione(it.ibm.red.business.dto.NotificaAzioneNpsDTO).
	 */
	@Override
	public String elaboraNotificaAzione(final NotificaAzioneNpsDTO item) {
		Connection con = null;
		Long idCoda = null;
		String documentTitle = null;

		try {
			con = setupConnection(getDataSource().getConnection(), true);

			idCoda = item.getIdCoda();

			final int idAoo = item.getIdAoo().intValue();
			// Recupera l'AOO
			final Aoo aoo = aooSRV.recuperaAoo(idAoo, con);

			if (TipoNotificaAzioneNPSEnum.isRispostaAutomatica(item.getTipoNotifica())) {
				documentTitle = elaboraNotificaAzioneRispostaAutomatica(item, aoo, aoo.getIdAoo(), con);
			} else {
				throw new RedException("Tipo di azione non gestita.");
			}

			// Si aggiorna lo stato della richiesta in coda impostando l'avvenuta
			// elaborazione
			notificaNpsDAO.aggiornaStatoNotifica(idCoda, StatoElabMessaggioPostaNpsEnum.ELABORATA.getId(), null, con);

			commitConnection(con);
		} catch (final Exception e) {
			gestisciErroreElaborazioneNotifica(idCoda, documentTitle, e, con);
		} finally {
			closeConnection(con);
		}

		return documentTitle;
	}

	/**
	 * Gestisce un'azione di "RISPONDI A" effettuata su un protocollo presente su
	 * RED su un sistema esterno a RED. La notifica di questa azione è inviata da
	 * NPS.
	 * 
	 * @param item
	 * @param aoo
	 * @param idAoo
	 * @param con
	 */
	private String elaboraNotificaAzioneRispostaAutomatica(final NotificaAzioneNpsDTO item, final Aoo aoo, final Long idAoo, final Connection con) {
		final ParametersAzioneRispostaAutomatica datiAzione = ((ParametersAzioneRispostaAutomatica) item.getDatiAzione());

		final String sistemaAusiliario = datiAzione.getSistemaAusiliario();
		final String protocollatore = datiAzione.getProtocollatore();
		Integer numeroProtocolloUscita = item.getNumeroProtocollo();
		Integer annoProtocolloUscita = item.getAnnoProtocollo();
		Date dataProtocolloUscita = item.getDataProtocollo();
		String idProtocolloUscita = item.getIdProtocollo();
		
		String identificatore = idProtocolloUscita;
		if (StringUtils.isBlank(identificatore)) {
			identificatore = numeroProtocolloUscita + "/" + annoProtocolloUscita;
		}

		LOGGER.info("elaboraNotificaAzioneRispostaAutomatica -> START. [Protocollo in uscita: " + identificatore + "]");
		final PropertiesProvider pp = PropertiesProvider.getIstance();
		IFilenetCEHelper fceh = null;
		FilenetPEHelper fpeh = null;
		final List<InfoProtocollo> protocolliIngresso = new ArrayList<>();
		UtenteDTO utenteProtocollatore = null;
		String titolarioNPS = null;
		String indiceClassificazione = null;
		String documentTitleUscita = null;
		String wobNumberUscita = null;
		final Map<String, List<AssegnatarioDTO>> elencoAssegnazioneAllacci = new HashMap<>();

		try {

			final AooFilenet aooFilenet = aoo.getAooFilenet();

			// Si inizializza l'accesso al CE FileNet
			fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getObjectStore(), aoo.getIdAoo());

			// Si inizializza l'accesso al PE FileNet
			fpeh = new FilenetPEHelper(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(), aooFilenet.getConnectionPoint());

			// recupera dettaglio del protocollo in uscita

			ProtocolloNpsDTO protocolloUscita = new ProtocolloNpsDTO();
			protocolloUscita.setIdProtocollo(idProtocolloUscita);
			protocolloUscita.setNumeroProtocollo(numeroProtocolloUscita);
			protocolloUscita.setAnnoProtocollo(annoProtocolloUscita != null ? "" + annoProtocolloUscita : null);
			protocolloUscita.setDataProtocollo(dataProtocolloUscita);
			protocolloUscita.setTipoProtocollo(TipoProtocolloEnum.getById(item.getTipoProtocollo()).getCodiceEstesoNPS());
			protocolloUscita = npsSRV.getDettagliProtocollo(protocolloUscita, true, aoo.getIdAoo().intValue(), null, false, con);

			LOGGER.info(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatore + "] - recuperato su NPS " + protocolloUscita.getNumeroProtocollo() + "/" + protocolloUscita.getAnnoProtocollo());

			utenteProtocollatore = npsSRV.getUtenteProtocollatore(protocolloUscita, 
					TipoNotificaAzioneNPSEnum.RISPONDI_A.equals(TipoNotificaAzioneNPSEnum.fromValue(item.getTipoNotifica())));

			LOGGER.info(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatore + "] - Protocollatore " + utenteProtocollatore);

			protocolliIngresso.add(datiAzione.getAllaccioPrincipale());
			protocolliIngresso.addAll(datiAzione.getUlterioriAllacci());

			LOGGER.info(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatore + "] - " + protocolliIngresso.size() + " allacci");

			// per ogni documenti in ingresso
			String identificatoreProtocolloIngresso = null;
			Document docFnItem = null;
			int numeroAllacci = 0;
			String documentTitleIngresso = null;
			Integer numeroProtocolloIngresso = null;
			Integer annoProtocolloIngresso = null;
			final List<RispostaAllaccioDTO> allacci = new ArrayList<>();
			final List<String> documentTitleAllacciati = new ArrayList<>();
			DetailDocumentRedDTO detailAllaccioDTO = null;
			RispostaAllaccioDTO allaccio = null;
			FascicoloDTO fascicoloAllaccio = null;
			FascicoloDTO fascicoloProcedimentaleUscita = null;
			boolean riservato = false;
			for (final InfoProtocollo protocolloIngresso : protocolliIngresso) {

				identificatoreProtocolloIngresso = protocolloIngresso.getIdProtocollo();
				if (StringUtils.isBlank(identificatoreProtocolloIngresso)) {
					identificatoreProtocolloIngresso = protocolloIngresso.getNumeroProtocollo() + "/" + protocolloIngresso.getAnnoProtocollo();
				}

				LOGGER.info(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatore + PROTOCOLLO_INGRESSO_LABEL + identificatoreProtocolloIngresso);

				if (!TipoProtocolloEnum.ENTRATA.getId().equals(protocolloIngresso.getTipoProtocollo())) {
					LOGGER.warn(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatore + PROTOCOLLO_INGRESSO_LABEL + identificatoreProtocolloIngresso + " non trovato su Filenet");
					if (numeroAllacci == 0) {
						throw new RedException(ERROR_RECUPERO_PROTOCOLLO_MSG);
					} else {
						continue;
					}
				}

				// recupera il documento del CE e metti nella lista dei documenti da allacciare
				try {
					docFnItem = getDocFileNetByProtocollo(protocolloIngresso.getIdProtocollo(), protocolloIngresso.getNumeroProtocollo(),
							protocolloIngresso.getAnnoProtocollo(), protocolloIngresso.getTipoProtocollo(), fceh, idAoo.intValue());
				} catch (final RedException e) {
					LOGGER.warn(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatore + PROTOCOLLO_INGRESSO_LABEL + identificatoreProtocolloIngresso + ": " + e.getMessage());
					if (numeroAllacci == 0) {
						throw e;
					} else {
						continue;
					}
				} catch (final Exception e) {
					LOGGER.warn(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatore + PROTOCOLLO_INGRESSO_LABEL + identificatoreProtocolloIngresso
							+ ": protocollo non presente sui sistemi o errore in fase di recupero su Filenet");
					if (numeroAllacci == 0) {
						throw new RedException(ERROR_RECUPERO_PROTOCOLLO_MSG, e);
					} else {
						continue;
					}
				}

				if (docFnItem != null) {
					
					documentTitleIngresso = (String) TrasformerCE.getMetadato(docFnItem, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY);
					numeroProtocolloIngresso = (Integer) TrasformerCE.getMetadato(docFnItem, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
					annoProtocolloIngresso = (Integer) TrasformerCE.getMetadato(docFnItem, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
					
					if(documentTitleAllacciati.contains(documentTitleIngresso)) {
						LOGGER.warn(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatore + PROTOCOLLO_INGRESSO_LABEL + identificatoreProtocolloIngresso
								+ ": protocollo scartato perchè duplicato nell'elento degli allacci inviati da NPS");
						continue;
					} 
					
					numeroAllacci++;
					documentTitleAllacciati.add(documentTitleIngresso);
					detailAllaccioDTO = TrasformCE.transform(docFnItem, TrasformerCEEnum.FROM_DOCUMENTO_TO_LIGHT_DETAIL_RED);
					
					fascicoloAllaccio = fascicoloSRV.getFascicoloProcedimentale(documentTitleIngresso, idAoo.intValue(), fceh, con);
					LOGGER.info(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatore + PROTOCOLLO_INGRESSO_LABEL + identificatoreProtocolloIngresso + " - fascicolo "
							+ fascicoloAllaccio.getIdFascicolo());
					if (numeroAllacci == 1) {
						fascicoloProcedimentaleUscita = fascicoloAllaccio;
						LOGGER.info(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatore + "] - fascicolo procedimentale " + fascicoloAllaccio.getIdFascicolo());
					}
					allaccio = new RispostaAllaccioDTO(null, documentTitleIngresso, TipoAllaccioEnum.RISPOSTA.getTipoAllaccioId(), 1, numeroAllacci == 1);
					allaccio.setFascicolo(fascicoloAllaccio);
					allaccio.setDocumentoDaChiudere(true);
					allaccio.setDocument(detailAllaccioDTO);
					allacci.add(allaccio);

					// recupera il wf principale
					final VWWorkObject workflowPrincipale = fpeh.getWorkflowPrincipale(documentTitleIngresso, aooFilenet.getIdClientAoo());
					if (workflowPrincipale != null) {

						final String motivazioneChiudiLavorazione = "Il documento è stato annullato automaticamente essendo allacciato al protocollo "
								+ numeroProtocolloIngresso + "/" + annoProtocolloIngresso + " chiuso con risposta" + " a fronte del protocollo "
								+ protocolloUscita.getNumeroProtocollo() + "/" + protocolloUscita.getAnnoProtocollo() + " eseguita dall'utente " + protocollatore
								+ " sul sistema " + sistemaAusiliario;

						workflowSRV.gestioneCodaInLavorazioneInSospeso(workflowPrincipale, Integer.parseInt(documentTitleIngresso), con, motivazioneChiudiLavorazione,
								utenteProtocollatore, fceh, pp);

						// kill wf principale
						fpeh.chiudiWF(workflowPrincipale);
						LOGGER.info(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatore + PROTOCOLLO_INGRESSO_LABEL + identificatoreProtocolloIngresso
								+ ": chiuso wf principale {" + workflowPrincipale.getWorkflowNumber() + "}");

						// insert nello storico item con PREDISPOSIZIONE ALLA RISPOSTA e motivazione
						// RISPONDI A
						final String motivazioneRispondiA = "Il documento è stato chiuso automaticamente con risposta a fronte del protocollo "
								+ protocolloUscita.getNumeroProtocollo() + "/" + protocolloUscita.getAnnoProtocollo() + " eseguita dall'utente " + protocollatore
								+ " sul sistema " + sistemaAusiliario;

						eventLogSRV.writePredisposizioneRispostaAutomaticaEventLog(utenteProtocollatore.getIdUfficio(), utenteProtocollatore.getId(),
								Integer.parseInt(documentTitleIngresso), motivazioneRispondiA, idAoo);

						LOGGER.info(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatore + PROTOCOLLO_INGRESSO_LABEL + identificatoreProtocolloIngresso
								+ ": inserita predisposizione risposta nello storico");

						// recupera e killa altri eventuali wf
						final VWRosterQuery workflows = fpeh.getRosterQueryWorkByIdDocumentoAndCodiceAOO(documentTitleIngresso, aooFilenet.getIdClientAoo());
						while (workflows.hasNext()) {
							final VWWorkObject workflow = (VWWorkObject) workflows.next();
							fpeh.chiudiWF(workflow);
							LOGGER.info(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatore + PROTOCOLLO_INGRESSO_LABEL + identificatoreProtocolloIngresso
									+ ": chiuso wf secondario {" + workflow.getWorkflowNumber() + "}");
						}

					} else {
						LOGGER.info(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatore + PROTOCOLLO_INGRESSO_LABEL + identificatoreProtocolloIngresso
								+ " risulta già precedentemente chiuso");
					}

					// verifica se è un allaccio "forzato" rispetto alle logiche standard
					final AssegnatarioDTO assegnatarioAllaccioForzato = recuperaDifferenteAssegnatario(utenteProtocollatore, identificatore, identificatoreProtocolloIngresso,
							workflowPrincipale, documentTitleIngresso, pp);

					final List<AssegnatarioDTO> assegnatari = new ArrayList<>();
					if (assegnatarioAllaccioForzato != null) {
						assegnatari.add(assegnatarioAllaccioForzato);
						assegnatari.add(new AssegnatarioDTO(utenteProtocollatore.getIdUfficio(), utenteProtocollatore.getId()));
						elencoAssegnazioneAllacci.put(documentTitleIngresso, assegnatari);

						// aggiorna security allaccio forzato
						riservato = false;
						final Integer metadatoRiservato = (Integer) TrasformerCE.getMetadato(docFnItem, PropertiesNameEnum.RISERVATO_METAKEY);
						if (metadatoRiservato != null && metadatoRiservato == 1) {
							riservato = true;
						}

						aggiornaSecurityAllaccioForzato(idAoo, documentTitleIngresso, utenteProtocollatore.getIdUfficio(), utenteProtocollatore.getId(), riservato, con, fceh);
						LOGGER.info(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatore + PROTOCOLLO_INGRESSO_LABEL + identificatoreProtocolloIngresso
								+ ": security aggiornate per allaccio forzato");
					} else {
						assegnatari.add(new AssegnatarioDTO(utenteProtocollatore.getIdUfficio(), utenteProtocollatore.getId()));
						elencoAssegnazioneAllacci.put(documentTitleIngresso, assegnatari);
					}

				} else {
					LOGGER.warn(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatore + PROTOCOLLO_INGRESSO_LABEL + identificatoreProtocolloIngresso + " non trovato su Filenet");
					if (numeroAllacci == 0) {
						throw new RedException(ERROR_RECUPERO_PROTOCOLLO_MSG);
					}
				}

			}

			// uscita: protocollo nps
			idProtocolloUscita = protocolloUscita.getIdProtocollo();
			numeroProtocolloUscita = protocolloUscita.getNumeroProtocollo();
			annoProtocolloUscita = Integer.parseInt(protocolloUscita.getAnnoProtocollo());
			dataProtocolloUscita = protocolloUscita.getDataProtocollo();
			final Integer tipoProtocollo = TipoProtocolloEnum.getByName(protocolloUscita.getTipoProtocollo()).getId();

			// uscita: destinatari cartacei
			final MezzoSpedizioneEnum mezzoSpedizione = MezzoSpedizioneEnum.CARTACEO;
			final ModalitaDestinatarioEnum toCC = ModalitaDestinatarioEnum.TO;
			final List<DestinatarioRedDTO> contattiDestinatari = new ArrayList<>();
			DestinatarioRedDTO destinatarioRedDTO = null;
			Contatto contatto = null;
			for (final Contatto destinatarioProtocollo : protocolloUscita.getDestinatari()) {
				contatto = getDestinatario(destinatarioProtocollo, utenteProtocollatore);
				destinatarioRedDTO = new DestinatarioRedDTO(contatto.getContattoID(), mezzoSpedizione, toCC);
				contattiDestinatari.add(destinatarioRedDTO);
			}

			// uscita: voce di titolario del protocollo, se presente, altrimenti
			// dell'allaccio principale, se presente, altrimenti di default
			indiceClassificazione = protocolloUscita.getCodiceTitolario();
			if (StringUtils.isBlank(indiceClassificazione) && fascicoloProcedimentaleUscita != null) {
				
				indiceClassificazione = fascicoloProcedimentaleUscita.getIndiceClassificazione();
				if (StringUtils.isBlank(indiceClassificazione) || FilenetStatoFascicoloEnum.NON_CATALOGATO.getNome().equals(indiceClassificazione)) {
					indiceClassificazione = PropertiesProvider.getIstance()
							.getParameterByString(aoo.getCodiceAoo() + "." + PropertiesNameEnum.RISPONDIA_CODICE_TITOLARIO_DEFAULT.getKey());
					final TitolarioDTO t = titolarioSRV.getNodoByIndice(idAoo, indiceClassificazione, con);
					if (t != null) {
						titolarioNPS = indiceClassificazione + " - " + t.getDescrizione();
					}
				} else {
					titolarioNPS = indiceClassificazione + " - " + fascicoloProcedimentaleUscita.getDescrizioneTitolario();
				}
			}

			// uscita: recupera tipo documento / tipo procedimento da tipo documento NPS
			Integer idTipologiaDocumento = null;
			Integer idTipoProcedimento = null;
			final TipologiaDTO tipologia = tipoDocSRV.getTipologiaFromTipologiaNPS(false, idAoo.intValue(), protocolloUscita.getDescrizioneTipologiaDocumento());
			if (tipologia != null) {
				idTipologiaDocumento = tipologia.getIdTipoDocumento();
				idTipoProcedimento = tipologia.getIdTipoProcedimento();
			} else {
				idTipologiaDocumento = tipoDocSRV.getTipologiaDocumentalePredefinita(idAoo, aoo.getCodiceAoo(), TipoCategoriaEnum.USCITA, con);
				final List<TipoProcedimento> tipiProcedimentoByTipologiaDocumento = tipoProcedimentoSRV.getTipiProcedimentoByTipologiaDocumento(idTipologiaDocumento, con);
				idTipoProcedimento = (int) tipiProcedimentoByTipologiaDocumento.get(0).getTipoProcedimentoId();
			}

			// uscita: iter spedizione con assegnatario utente protocollatore
			final Integer idIterApprovativo = IterApprovativoDTO.ITER_FIRMA_MANUALE;
			final TipoAssegnazioneEnum tipoAssegnazione = TipoAssegnazioneEnum.SPEDIZIONE;
			final Long idUtenteAssegnatario = utenteProtocollatore.getId();
			final Long idNodoAssegnatario = utenteProtocollatore.getIdUfficio();
			
			//recupera il documento principale su NPS			
			byte[] content = null;
			String contentType = null;
			String nomeFile = null;
			String guidLastVersion = null;
			final DocumentoNpsDTO documentoPrincipaleNPS = protocolloUscita.getDocumentoPrincipale();
			if (documentoPrincipaleNPS != null && documentoPrincipaleNPS.getInputStream() != null) {
				try {
					content = IOUtils.toByteArray(documentoPrincipaleNPS.getInputStream());
					contentType = documentoPrincipaleNPS.getContentType();
					nomeFile = documentoPrincipaleNPS.getNomeFile();
					guidLastVersion = documentoPrincipaleNPS.getGuidLastVersion();
				} catch (IOException e) {
					LOGGER.error(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatore + "] - Errore in fase recupero del content principale su NPS", e);
				}
				
			}
			
			//recupera gli allegati su NPS
			List<AllegatoDTO> allegati = new ArrayList<>();
			final Integer idTipologiaDocumentoAllegato = Integer.parseInt(PropertiesProvider.getIstance().getParameterByString(aoo.getCodiceAoo() + ".tipologia.generica.default"));
			
			if(!CollectionUtils.isEmpty(protocolloUscita.getAllegatiList())) {
				for(AllegatoNpsDTO allegatoNPS: protocolloUscita.getAllegatiList()) {
					
					if(allegatoNPS != null && allegatoNPS.getInputStream() != null) {
						
						byte[] contentAllegatoByte;
						try {
							contentAllegatoByte = IOUtils.toByteArray(allegatoNPS.getInputStream());
							// Se l'allegato è firmato, il suo formato deve essere 'Firmato Digitalmente'
							final FormatoAllegatoEnum formatoAllegato = PdfHelper.getSignatureNumber(contentAllegatoByte) > 0 ? FormatoAllegatoEnum.FIRMATO_DIGITALMENTE : FormatoAllegatoEnum.ELETTRONICO;
							
							allegati.add(new AllegatoDTO(allegatoNPS.getNomeFile(), contentAllegatoByte, allegatoNPS.getContentType(), 
									formatoAllegato, allegatoNPS.getOggetto(), Boolean.FALSE, Boolean.TRUE, idTipologiaDocumentoAllegato));
						} catch (IOException e) {
							LOGGER.error(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatore + "] - Errore in fase recupero dell'allegato su NPS", e);
						}
						
					}
					
				}
			}

			// crea documento in uscita senza comunicare nulla a NPS
			final EsitoSalvaDocumentoDTO esitoCreazioneUscita = creaDocumentoRedUscitaDaNPS(TipoNotificaAzioneNPSEnum.fromValue(datiAzione.getTipoAzione()), 
					content, contentType, nomeFile, guidLastVersion, utenteProtocollatore, fascicoloProcedimentaleUscita, indiceClassificazione, idTipologiaDocumento, idTipoProcedimento, idIterApprovativo, tipoAssegnazione,
					MomentoProtocollazioneEnum.PROTOCOLLAZIONE_NON_STANDARD, allegati, allacci, contattiDestinatari, protocolloUscita.getOggetto(), null, null, null,
					idUtenteAssegnatario, idNodoAssegnatario, null, idProtocolloUscita, numeroProtocolloUscita, annoProtocolloUscita, dataProtocolloUscita,
					tipoProtocollo, protocolloUscita.getMetadatiEstesiTipologiaDoc(), SottoCategoriaDocumentoUscitaEnum.RISPOSTA, con);

			if (!esitoCreazioneUscita.isEsitoOk()) {
				final StringBuilder errori = new StringBuilder("");
				for (final SalvaDocumentoErroreEnum err : esitoCreazioneUscita.getErrori()) {
					errori.append("[").append(err.getCodice()).append("]").append(err.getMessaggio()).append(";");
				}
				throw new RedException("Errore in fase di creazione del documento in uscita: " + errori);
			}

			documentTitleUscita = esitoCreazioneUscita.getDocumentTitle();
			wobNumberUscita = esitoCreazioneUscita.getWobNumber();
			LOGGER.info(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatore + "] - Creato documento in uscita " + documentTitleUscita + " associato al wf " + wobNumberUscita);

			// aggiorna l'item con i dati di protocollo
			notificaNpsDAO.aggiornaDatiProtocollo(item.getIdCoda(), idProtocolloUscita, numeroProtocolloUscita, annoProtocolloUscita, dataProtocolloUscita,
					documentTitleUscita, con);
			LOGGER.info(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatore + "] - Aggiornati dati di protocollo in coda per l'item " + item.getIdCoda());

			// chiudi wf documento in uscita con SPEDITO (data = data protocollo)
			final EsitoOperazioneDTO esitoSpedito = speditoSRV.spedito(utenteProtocollatore, protocolloUscita.getDataProtocollo(), wobNumberUscita, false, con);
			if (!esitoSpedito.isEsito()) {
				throw new RedException("Errore in fase di spedizione del documento in uscita");
			} else {
				LOGGER.info(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatore + "] - Documento " + documentTitleUscita + " associato al wf " + wobNumberUscita + " spedito");
			}

		} finally {
			logoff(fpeh);
			popSubject(fceh);
		}

		try {
			// comunica variazioni al protocollo in uscita a NPS
			if (StringUtils.isNotBlank(titolarioNPS)) {
				final String infoProtocollo = new StringBuilder().append(numeroProtocolloUscita).append("/").append(annoProtocolloUscita).append("_")
						.append(aoo.getCodiceAoo()).toString();
				npsSRV.aggiornaTitolarioProtocollo(utenteProtocollatore, infoProtocollo, idProtocolloUscita, indiceClassificazione, documentTitleUscita, con);
				LOGGER.info(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatore + "] - Creato documento in uscita " + documentTitleUscita);
			}

			// aggiorna documentoutentestato allacci "forzati"
			for (final Entry<String, List<AssegnatarioDTO>> entry : elencoAssegnazioneAllacci.entrySet()) {
				final String documentTitle = entry.getKey();
				final List<AssegnatarioDTO> assegnatari = entry.getValue();
				codeDocumentiSRV.checkChiusuraAllacci(Integer.parseInt(documentTitle), assegnatari, con);
				LOGGER.info(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatore + "] - Aggiornate le code applicative dell'allaccio forzato " + documentTitle);
			}

		} catch (final Exception e) {
			LOGGER.error(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatore + "] - Errore in fase comunicazione verso NPS del nuovo titolario per il documento " + documentTitleUscita, e);
		}

		LOGGER.info("elaboraNotificaAzioneRispondiA -> END. [Protocollo: " + identificatore + "]");

		return documentTitleUscita;

	}

	private void aggiornaSecurityAllaccioForzato(final Long idAoo, final String documentTitleIngresso, final Long idUfficio, final Long idUtente, final boolean isRiservato,
			final Connection con, final IFilenetCEHelper fceh) {
		// aggiorna security
		final ListSecurityDTO securities = securitySRV.getSecurityAssegnazione(idUfficio + "," + idUtente, true, true, isRiservato, con);
		securitySRV.verificaSecurities(securities, con);
		securitySRV.modificaSecurityFascicoli(idAoo, securities, documentTitleIngresso, fceh);
		securitySRV.aggiornaSecurityDocumento(documentTitleIngresso, idAoo, securities, true, false, fceh);

	}

	/**
	 * Restituisce l'assegnatario differente dall'utenteProtocollatore, se esiste.
	 * 
	 * @param utenteProtocollatore
	 * @param identificatoreProtocolloUscita
	 * @param identificatoreProtocolloIngresso
	 * @param workflowPrincipale
	 * @param documentTitle
	 * @param pp
	 * @return
	 */
	private AssegnatarioDTO recuperaDifferenteAssegnatario(final UtenteDTO utenteProtocollatore, final String identificatoreProtocolloUscita,
			final String identificatoreProtocolloIngresso, final VWWorkObject workflowPrincipale, final String documentTitle, final PropertiesProvider pp) {

		if (workflowPrincipale != null) {
			final Long idUtenteDestinatario = Long
					.valueOf((Integer) TrasformerPE.getMetadato(workflowPrincipale, pp.getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY)));
			final Long idNodoDestinatario = Long
					.valueOf((Integer) TrasformerPE.getMetadato(workflowPrincipale, pp.getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY)));

			if (idUtenteDestinatario == null || idNodoDestinatario == null || !idUtenteDestinatario.equals(utenteProtocollatore.getId())
					|| !idNodoDestinatario.equals(utenteProtocollatore.getIdUfficio())) {
				LOGGER.info(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatoreProtocolloUscita + PROTOCOLLO_INGRESSO_LABEL + identificatoreProtocolloIngresso
						+ " assegnato a [" + idNodoDestinatario + "," + idUtenteDestinatario + "]: allaccio forzato");
				return new AssegnatarioDTO(idNodoDestinatario, idUtenteDestinatario);
			}
		} else {
			Connection connectionDWH = null;
			try {
				connectionDWH = setupConnection(getFilenetDataSource().getConnection(), false);
				final EventoLogDTO event = eventDAO.getLastChiusuraIngressoEvent(connectionDWH, Integer.valueOf(documentTitle), utenteProtocollatore.getIdAoo().intValue());
				if (event == null) {
					return null;
				}
				if (event.getIdUfficioMittente().intValue() != utenteProtocollatore.getIdUfficio().intValue()) {
					LOGGER.info(PROTOCOLLO_ELABORA_NOTIFICA_LABEL + identificatoreProtocolloUscita + PROTOCOLLO_INGRESSO_LABEL + identificatoreProtocolloIngresso
							+ " chiuso da [" + event.getIdUfficioMittente() + "]: allaccio forzato");
					return new AssegnatarioDTO(event.getIdUfficioMittente(), 0L);
				}
			} catch (final Exception e) {
				LOGGER.error("Errore in fase di ricerca dell'ultima chiusura del documento " + documentTitle, e);
				throw new RedException("Errore in fase di ricerca dell'ultima chiusura del documento " + documentTitle, e);
			} finally {
				closeConnection(connectionDWH);
			}
		}

		return null;
	}
	
	

	/**
	 * Restituisce il documento filenet associato ai parametri in ingresso.
	 * 
	 * @param idProtocollo
	 * @param numeroProtocollo
	 * @param annoProtocollo
	 * @param tipoProtocollo
	 * @param fceh
	 * @param idAoo
	 * @return Document recuperato da Filenet se esistente, null altrimenti.
	 */
	@Override
	public Document getDocFileNetByProtocollo(final String idProtocollo, final Integer numeroProtocollo, final Integer annoProtocollo, final int tipoProtocollo,
			final IFilenetCEHelper fceh, final int idAoo) {
		Document documento = null;

		if (StringUtils.isBlank(idProtocollo) && (numeroProtocollo == null || annoProtocollo == null)) {
			throw new RedException("Dati in input non sufficienti per estrarre il protocollo.");
		}

		String logProt = Constants.EMPTY_STRING;
		String searchCondition = "d." + PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY) + " = " + tipoProtocollo;

		if (StringUtils.isNotBlank(idProtocollo)) {
			searchCondition += AND_D + PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY) + " = '" + idProtocollo + "'";
			logProt = "ID: " + idProtocollo;
		}
		if (numeroProtocollo != null && annoProtocollo != null) {
			searchCondition += AND_D + PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY) + " = " + numeroProtocollo
					+ AND_D + PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY) + " = " + annoProtocollo;
			if (StringUtils.isNotBlank(idProtocollo)) {
				logProt += " e ";
			}
			logProt += "NUMERO/ANNO: " + numeroProtocollo + "/" + annoProtocollo;
		}

		final DocumentSet documents = fceh.getDocuments(idAoo, searchCondition,
				PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), true, false);

		if (documents != null && !documents.isEmpty()) {

			final Iterator<?> it = documents.iterator();
			int i = 0;
			while (it.hasNext()) {
				if (i != 0) {
					throw new RedException("Esiste più di un documento associato al protocollo " + TipoProtocolloEnum.getById(tipoProtocollo) + " con " + logProt);
				}
				documento = (Document) it.next();
				i++;
			}

		} else {
			throw new RedException("Non esistono documenti associati al protocollo: " + TipoProtocolloEnum.getById(tipoProtocollo) + " con " + logProt);
		}

		return documento;
	}

	/**
	 * @param idCoda
	 * @param documentTitle
	 * @param e
	 * @param con
	 */
	private void gestisciErroreElaborazioneNotifica(final Long idCoda, final String documentTitle, final Exception e, final Connection con) {
		rollbackConnection(con);
		if (idCoda != null) {
			LOGGER.error("Errore durante l'elaborazione della notifica con id: " + idCoda, e);
			Connection conErrore = null;
			try {
				conErrore = setupConnection(getDataSource().getConnection(), false);

				if (documentTitle != null && !documentTitle.isEmpty()) {
					// Si aggiorna lo stato della richiesta in coda impostando l'errore
					notificaNpsDAO.aggiornaStatoNotifica(idCoda, StatoElabMessaggioPostaNpsEnum.IN_ERRORE.getId(),
							"Errore nell'elaborazione della richiesta: " + e.getMessage(), documentTitle, conErrore);
				} else {
					// Si aggiorna lo stato della richiesta in coda impostando l'errore
					notificaNpsDAO.aggiornaStatoNotifica(idCoda, StatoElabMessaggioPostaNpsEnum.IN_ERRORE.getId(),
							"Errore nell'elaborazione della richiesta: " + e.getMessage(), conErrore);
				}
			} catch (final Exception ex) {
				LOGGER.error("Errore durante l'aggiornamento dello stato di una notifica.", ex);
			} finally {
				closeConnection(conErrore);
			}
		} else {
			LOGGER.error("Errore durante il recupero di una richiesta di elaborazione dalla coda delle notifiche.", e);
		}
	}

	/**
	 * @see it.ibm.red.business.service.concrete.CreaDocumentoRedAbstractSRV#getProvenienza().
	 */
	@Override
	protected ProvenienzaSalvaDocumentoEnum getProvenienza() {
		return ProvenienzaSalvaDocumentoEnum.POSTA_NPS;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.INotificaNpsFacadeSRV#getNotifica(java.lang.Long,
	 *      java.lang.Integer, java.lang.Integer).
	 */
	@Override
	public NotificaNpsDTO getNotificaAzione(final Long idAoo, final Integer numeroProtocollo, final Integer annoProtocollo) {
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			if (idAoo == null || numeroProtocollo == null || annoProtocollo == null) {
				return null;
			} else {
				return notificaNpsDAO.getNotificaAzione(idAoo, numeroProtocollo, annoProtocollo, connection);
			}
		} catch (final Exception e) {
			LOGGER.error("Errore riscontrato durante il recupero della notifica associato al protocollo: [" + numeroProtocollo + "/" + annoProtocollo + "]", e);
			throw new RedException("Errore riscontrato durante il recupero della notifica associato al protocollo: [" + numeroProtocollo + "/" + annoProtocollo + "]", e);
		} finally {
			closeConnection(connection);
		}
	}

	private Contatto getDestinatario(final Contatto destinatarioProtocollo, final UtenteDTO utenteProtocollatore) {
		if (destinatarioProtocollo != null) {
			return rubricaSRV.insertContattoFlussoAutomatico(destinatarioProtocollo, utenteProtocollatore);
		} else {
			throw new RedException("getMittente -> Errore: mittente NON presente nel protocollo NPS in input");
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.INotificaNpsFacadeSRV#recuperaRichiestaElab(int).
	 */
	@Override
	public NotificaNpsDTO recuperaRichiestaElab(final int idCoda) {
		Connection con = null;
		NotificaNpsDTO notifica = null;
		try {

			con = setupConnection(getDataSource().getConnection(), true);

			// Recupera la richiesta
			notifica = notificaNpsDAO.getAndLockNotifica(idCoda, con);

			// Si aggiorna lo stato della richiesta in coda impostando la presa in carico
			if (notifica != null) {
				notificaNpsDAO.aggiornaStatoNotifica(notifica.getIdCoda(), StatoElabNotificaNpsEnum.IN_ELABORAZIONE.getId(), null, con);
			}

			commitConnection(con);

		} catch (final Exception e) {
			rollbackConnection(con);
			LOGGER.error("Errore durante il recupero della richiesta di elaborazione della notifica di operazione eseguita da NPS con id = " + idCoda, e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

		return notifica;
	}

	/**
	 * @see it.ibm.red.business.service.facade.INotificaNpsFacadeSRV#updateStatoAndManageEmailByCodiMessage(java.lang.String,
	 *      java.lang.String).
	 */
	@Override
	public void updateStatoAndManageEmailByCodiMessage(final String codiMessaggio, final String msgId) {
		Connection conn = null;
		try {
			conn = setupConnection(getDataSource().getConnection(), false);
			final MessaggioEmailDTO msgNext = gestioneNotificheEmailDAO.getStatoEmailByCodiMessage(codiMessaggio, StatoCodaEmailEnum.CHIUSURA.getStatus(), conn);
			codaMailSRV.updateStatoAndManageEmailByCodiMessage(msgNext, msgId, conn);
		} catch (final Exception ex) {
			LOGGER.error("Errore nell'aggiornamento dello stato della notifica di spedizione ", ex);
			throw new RedException("Errore nell'aggiornamento dello stato della notifica", ex);
		} finally {
			closeConnection(conn);
		}
	}

}