package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * Facade del servizio di integrazione dati.
 */
public interface IIntegrazioneDatiFacadeSRV extends Serializable {

	/**
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	EsitoOperazioneDTO validaIntegrazioneDati(UtenteDTO utente, String wobNumber);

	/**
	 * @param utente
	 * @param wobNumber
	 * @return
	 */
	Collection<EsitoOperazioneDTO> validaIntegrazioneDati(UtenteDTO utente, Collection<String> wobNumber);
	
	/**
	 * @param utente
	 * @param wobNumber
	 * @param spostaInLavorazione
	 * @return
	 */
	EsitoOperazioneDTO validaIntegrazioneDati(UtenteDTO utente, String wobNumber, boolean spostaInLavorazione, boolean attiAsync, String statoNPS);
	
	/**
	 * Associa il documento di Integrazione Dati al documento che insiste sul wf identificato dal wobNumber in input
	 * 
	 * @param utente
	 * @param wobNumber
	 * @param idDocumentoIntegrazioneDati
	 * @return
	 */
	EsitoOperazioneDTO associaIntegrazioneDati(UtenteDTO utente, String wobNumber, String idDocumentoIntegrazioneDati);
	
	/**
	 * Recupera i documenti in ingresso di tipo Integrazione Dati che insistono sulla coda Corriere o Da Lavorare dell'utente in input.
	 * 
	 * @param utente
	 * @return
	 */
	Collection<MasterDocumentRedDTO> getIntegrazioneDati4Associa(UtenteDTO utente);

}