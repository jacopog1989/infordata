package it.ibm.red.business.dto;

import java.util.Date;

/**
 * DTO per la gestione degli allegati OP.
 */
public class AllegatoOPDTO extends AbstractDTO {
	
	private static final long serialVersionUID = 5028840327881723736L;
	
	/**
	 * Identificativo ocumento allegato op.
	 */
	private String idDocumentoAllegatoOp;
	
	/**
	 * Guid documento.
	 */
	private String guid;
	
	/**
	 * Oggetto.
	 */
	private String oggetto;
	
	/**
	 * Tipologia documento.
	 */
	private String tipologiaDocumento;
	
	/**
	 * Data creazione.
	 */
	private Date dataCreazione;
	
	/**
	 * Flag possiede content.
	 */
	private boolean isWithContent;
	
	/**
	 * Nome documento.
	 */
	private String nomeDocumento;
	
	/**
	 * Tipo content.
	 */
	private String contentType = null;
	
	/**
	 * Data handler.
	 */
	private byte[] dataHandler;
	
	/**
	 * Tipo mime.
	 */
	private String mimeType;
	
	/**
	 * Descrizione tipologia documento.
	 */
	private String tipologiaDocumentoDescrizione;

	/**
	 * @return the idDocumentoAllegatoOp
	 */
	public String getIdDocumentoAllegatoOp() {
		return idDocumentoAllegatoOp;
	}

	/**
	 * @param idDocumentoAllegatoOp the idDocumentoAllegatoOp to set
	 */
	public void setIdDocumentoAllegatoOp(final String idDocumentoAllegatoOp) {
		this.idDocumentoAllegatoOp = idDocumentoAllegatoOp;
	}

	/**
	 * @return the guid
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * @param guid the guid to set
	 */
	public void setGuid(final String guid) {
		this.guid = guid;
	}

	/**
	 * @return the oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * @param oggetto the oggetto to set
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}

	/**
	 * @return the tipologiaDocumento
	 */
	public String getTipologiaDocumento() {
		return tipologiaDocumento;
	}

	/**
	 * @param tipologiaDocumento the tipologiaDocumento to set
	 */
	public void setTipologiaDocumento(final String tipologiaDocumento) {
		this.tipologiaDocumento = tipologiaDocumento;
	}

	/**
	 * @return the dataCreazione
	 */
	public Date getDataCreazione() {
		return dataCreazione;
	}

	/**
	 * @param dataCreazione the dataCreazione to set
	 */
	public void setDataCreazione(final Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	/**
	 * @return the isWithContent
	 */
	public boolean isWithContent() {
		return isWithContent;
	}

	/**
	 * @param isWithContent the isWithContent to set
	 */
	public void setWithContent(final boolean isWithContent) {
		this.isWithContent = isWithContent;
	}

	/**
	 * @return the nomeDocumento
	 */
	public String getNomeDocumento() {
		return nomeDocumento;
	}

	/**
	 * @param nomeDocumento the nomeDocumento to set
	 */
	public void setNomeDocumento(final String nomeDocumento) {
		this.nomeDocumento = nomeDocumento;
	}

	/**
	 * @return the contentType
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * @param contentType the contentType to set
	 */
	public void setContentType(final String contentType) {
		this.contentType = contentType;
	}

	/**
	 * @return the dataHandler
	 */
	public byte[] getDataHandler() {
		return dataHandler;
	}

	/**
	 * @param dataHandler the dataHandler to set
	 */
	public void setDataHandler(final byte[] dataHandler) {
		this.dataHandler = dataHandler;
	}

	/**
	 * @return the mimeType
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * @param mimeType the mimeType to set
	 */
	public void setMimeType(final String mimeType) {
		this.mimeType = mimeType;
	}

	/**
	 * @return the tipologiaDocumentoDescrizione
	 */
	public String getTipologiaDocumentoDescrizione() {
		return tipologiaDocumentoDescrizione;
	}

	/**
	 * @param tipologiaDocumentoDescrizione the tipologiaDocumentoDescrizione to set
	 */
	public void setTipologiaDocumentoDescrizione(final String tipologiaDocumentoDescrizione) {
		this.tipologiaDocumentoDescrizione = tipologiaDocumentoDescrizione;
	}
	
}
