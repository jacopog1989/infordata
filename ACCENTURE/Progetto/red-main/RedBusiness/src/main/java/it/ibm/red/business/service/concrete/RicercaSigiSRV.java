package it.ibm.red.business.service.concrete;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.DocumentSet;

import it.ibm.red.business.constants.Constants.Ricerca;
import it.ibm.red.business.dto.MasterSigiDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IRicercaSigiSRV;
import it.ibm.red.business.utils.DateUtils;

/**
 * The Class RicercaSigiSRV.
 *
 * @author m.crescentini
 * 
 *         Servizio per la gestione della ricerca SIGI.
 */
@Service
public class RicercaSigiSRV extends AbstractService implements IRicercaSigiSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -6147767378111021038L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaSigiSRV.class.getName());

	/**
	 * Properties.
	 */
	private PropertiesProvider pp;

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaSigiFacadeSRV#eseguiRicercaSigi(it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO,
	 *      it.ibm.red.business.dto.UtenteDTO, boolean).
	 */
	@Override
	public Collection<MasterSigiDTO> eseguiRicercaSigi(final ParamsRicercaAvanzataDocDTO paramsRicercaSigi, final UtenteDTO utente, final boolean orderbyInQuery) {
		LOGGER.info("eseguiRicercaSigi -> START");
		Collection<MasterSigiDTO> documentiRicerca = new ArrayList<>();
		final HashMap<String, Object> mappaValoriRicercaFilenet = new HashMap<>();
		IFilenetCEHelper fceh = null;

		try {
			// Si convertono i parametri di ricerca in una mappa chiave -> valore
			convertiInParametriRicercaFilenet(mappaValoriRicercaFilenet, paramsRicercaSigi, utente.getIdAoo());

			// ### RICERCA NEL CE -> START
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final DocumentSet documentiFilenet = fceh.ricercaSigi(mappaValoriRicercaFilenet, utente, orderbyInQuery);

			if (documentiFilenet != null && !documentiFilenet.isEmpty()) {
				documentiRicerca = TrasformCE.transform(documentiFilenet, TrasformerCEEnum.FROM_DOCUMENTO_TO_DOC_SIGI);

				if (!orderbyInQuery) {
					final List<MasterSigiDTO> docList = new ArrayList<>(documentiRicerca);
					// implementa Comparable
					Collections.sort(docList, Collections.reverseOrder());
					documentiRicerca = docList;
				}
			}
			// ### RICERCA NEL CE -> END
		} catch (final Exception e) {
			LOGGER.error("eseguiRicercaSigi -> Si è verificato un errore durante l'esecuzione della ricerca.", e);
			throw new RedException("eseguiRicercaSigi -> Si è verificato un errore durante l'esecuzione della ricerca.", e);
		} finally {
			popSubject(fceh);
		}

		LOGGER.info("eseguiRicercaSigi -> END. Numero di documenti trovati: " + documentiRicerca.size());
		return documentiRicerca;
	}

	private void convertiInParametriRicercaFilenet(final HashMap<String, Object> paramsRicercaFilenet, final ParamsRicercaAvanzataDocDTO paramsRicerca, final Long idAoo) {
		LOGGER.info("convertiInParametriRicercaFilenet -> START");

		paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY), idAoo);

		// Oggetto
		if (StringUtils.isNotBlank(paramsRicerca.getOggetto())) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY), paramsRicerca.getOggetto());
		}

		// ID Documento (Numero Documento)
		if (paramsRicerca.getNumeroDocumento() != null) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY), paramsRicerca.getNumeroDocumento());
		}

		// Data Creazione Da
		if (paramsRicerca.getDataCreazioneDa() != null) {
			paramsRicercaFilenet.put(Ricerca.DATA_CREAZIONE_DA, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataCreazioneDa(), true));
		}

		// Data Creazione A
		if (paramsRicerca.getDataCreazioneA() != null) {
			paramsRicercaFilenet.put(Ricerca.DATA_CREAZIONE_A, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataCreazioneA(), false));
		}

		// Numero Protocollo Da
		if (paramsRicerca.getNumeroProtocolloDa() != null) {
			paramsRicercaFilenet.put(Ricerca.NUMERO_PROTOCOLLO_DA, paramsRicerca.getNumeroProtocolloDa());
		}

		// Numero Protocollo A
		if (paramsRicerca.getNumeroProtocolloA() != null) {
			paramsRicercaFilenet.put(Ricerca.NUMERO_PROTOCOLLO_A, paramsRicerca.getNumeroProtocolloA());
		}

		if (paramsRicerca.getAnnoProtocolloDa() != null || paramsRicerca.getAnnoProtocolloA() != null) {
			// Data Protocollo Da
			if (paramsRicerca.getAnnoProtocolloDa() != null) {
				paramsRicercaFilenet.put(Ricerca.DATA_PROTOCOLLO_DA, paramsRicerca.getAnnoProtocolloDa());
			}
			// Data Protocollo A
			if (paramsRicerca.getAnnoProtocolloA() != null) {
				paramsRicercaFilenet.put(Ricerca.DATA_PROTOCOLLO_A, paramsRicerca.getAnnoProtocolloA());
			}
		}

		// Annullati
		if (paramsRicerca.isAnnullati()) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.METADATO_DOCUMENTO_DATA_ANNULLAMENTO), "NOT NULL");
		}

		LOGGER.info("convertiInParametriRicercaFilenet -> END");
	}
}