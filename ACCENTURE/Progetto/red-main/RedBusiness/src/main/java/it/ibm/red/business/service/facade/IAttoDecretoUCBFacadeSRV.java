package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import it.ibm.red.business.dto.AttributiEstesiAttoDecretoDTO;
import it.ibm.red.business.dto.DestinatarioCodaMailDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.service.concrete.CodaMailSRV.ResponseSendMail;

/**
 * Facade del servizio che gestisce l'atto decreto UCB.
 */
public interface IAttoDecretoUCBFacadeSRV extends Serializable {
	
	/**
	 * @param idAOO
	 * @return
	 */
	AttributiEstesiAttoDecretoDTO getAttributiEstesi(Integer idAOO);
	
	/**
	 * @param filenames
	 * @return
	 */
	Collection<String> validateFilenames(Collection<String> filenames);
	
	/**
	 * @param utente
	 * @param contentMessaggio
	 * @param destinatario
	 * @param casellaPostale
	 * @param mittenteMessaggio
	 * @param numeroProtocollo
	 * @param dataProtocollo
	 * @param filesNonConformi
	 * @return
	 */
	ResponseSendMail comunicaErroreProtocollazione(UtenteDTO utente, byte[] contentMessaggio, DestinatarioCodaMailDTO destinatario,
			String casellaPostale, String mittenteMessaggio, Integer numeroProtocollo, Date dataProtocollo,
			Collection<String> filesNonConformi);
	
}