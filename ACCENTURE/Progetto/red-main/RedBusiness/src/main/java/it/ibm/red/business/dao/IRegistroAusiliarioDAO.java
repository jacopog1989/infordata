/**
 * 
 */
package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;

import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.RegistroDTO;
import it.ibm.red.business.enums.TipoRegistroAusiliarioEnum;
import it.ibm.red.business.pcollector.ParametersCollector;

/**
 * @author APerquoti
 *
 */
public interface IRegistroAusiliarioDAO extends Serializable {

	/**
	 * Restituisce tutti i registri configurati.
	 * 
	 * @param connection
	 *            connessione al database
	 * @return lista dei registri recuperati
	 */
	Collection<RegistroDTO> getRegistri(Connection connection);

	/**
	 * Restituisce i registri del tipo specificato dal parametro <code> tipo </code>
	 * e che sia valido per la coppia tipologia documento / tipologia procedimento.
	 * 
	 * @see it.ibm.red.business.dao.IRegistroAusiliarioDAO#getRegistri(java.sql.Connection,
	 *      it.ibm.red.business.enums.TipoRegistroAusiliarioEnum,
	 *      java.lang.Integer).
	 * @param connection
	 *            connessione al database
	 * @param tipo
	 *            tipo registro da recuperare
	 * @param idTipologiaDocumento
	 *            identificativo tipologia documento
	 * @param idTipoProcedimento
	 *            identificativo tipologia procedimento
	 * @return lista dei registri del tipo specificato validi per il documento
	 */
	Collection<RegistroDTO> getRegistri(Connection connection, TipoRegistroAusiliarioEnum tipo, Integer idTipologiaDocumento, Integer idTipoProcedimento);

	/**
	 * Restituisce il template xslt associato al registro identificato dall'id.
	 * 
	 * @param connection
	 *            connession al database
	 * @param idRegistro
	 *            identificativo registro ausiliario
	 * @return byte array del template xslt
	 */
	byte[] getXSLT(Connection connection, Integer id);

	/**
	 * Restituisce i registri ausiliari validi sui documento identificati dalla
	 * tipologia documento.
	 * 
	 * @see it.ibm.red.business.dao.IRegistroAusiliarioDAO#getRegistriByTipologieDocumento(java.sql.Connection,
	 *      java.util.List).
	 * @param connection
	 *            connession al database
	 * @param idsTipologieDocumento
	 *            id delle tipologia documento
	 * @return lista dei registri ausiliari
	 */
	Collection<RegistroDTO> getRegistriByTipologieDocumento(Connection connection, Collection<Integer> idsTipologieDocumento);

	/**
	 * Restituisce i metadati specifici del registro ausiliario identificato dall:
	 * <code> idRegistro </code>.
	 * 
	 * @param connection
	 *            connession al database
	 * @param idRegistro
	 *            identificativo registro ausiliario
	 * @return lista dei metadati estesi propri del template associato al registro
	 *         ausiliario
	 */
	Collection<MetadatoDTO> getMetadatiRegistro(Connection connection, Integer idRegistro);
	
	/**
	 * Restituisce la classe @see ParametersCollector che definisce il collector per
	 * la creazione del template associato al registro ausiliario identificato dall:
	 * <code> idRegistro </code>.
	 * 
	 * @param connection
	 *            connession al database
	 * @param idRegistro
	 *            identificativo registro
	 * @return collector dei parametri per il popolamento del template
	 */
	ParametersCollector getCollector(Connection connection, Integer idRegistro);
	
	/**
	 * Esegue il salvataggio della relazion tra procedimento identificato dal
	 * <code> idProcedimento </code>, dal documento identificato dal
	 * <code> idDocumento </code> e dal registro ausiliario identificato dal
	 * <code> idRegistro </code>.
	 * 
	 * @param connection
	 *            connession al database
	 * @param idProcedimento
	 *            identificativo tipo procedimento
	 * @param idDocumento
	 *            identificativo tipologia documento
	 * @param idRegistro
	 *            identificativo registro ausiliario
	 */
	void saveCrossProcedimentoRegistro(Connection connection, Long idProcedimento, int idDocumento, int idRegistro);

	/**
	 * Restituisce il registro identificato dal codice:
	 * <code> codiceRegistro </code>.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param codiceRegistro
	 *            codie registro ausiliario
	 * @return registro ausiliario
	 */
	RegistroDTO getRegistro(Connection con, String codiceRegistro);
	
	/**
	 * Restituisce il registro identificato dall' identificativo univoco:
	 * <code> idRegistro </code>.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idRegistro
	 *            identificativo registro ausiliario
	 * @return registro ausiliario
	 */
	RegistroDTO getRegistro(Connection con, Integer idRegistro);
}
