package it.ibm.red.business.service.concrete;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.collection.DocumentSet;
import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.Document;
import com.filenet.api.property.Properties;
import com.google.common.net.MediaType;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.ContentType;
import it.ibm.red.business.constants.Constants.Firma;
import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.ICodaTrasformazioneDAO;
import it.ibm.red.business.dao.IIterApprovativoDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IRuoloDAO;
import it.ibm.red.business.dao.IStampigliaturaSegnoGraficoDAO;
import it.ibm.red.business.dao.ITipoFileDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.CodaTrasformazioneDTO;
import it.ibm.red.business.dto.CodaTrasformazioneParametriDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.StampigliaturaSegnoGraficoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.BooleanFlagEnum;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.FormatoAllegatoEnum;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.SottoCategoriaDocumentoUscitaEnum;
import it.ibm.red.business.enums.StatoCodaTrasformazioneEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoProtocolloEnum;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.exception.FilenetException;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.adobe.AdobeLCHelper;
import it.ibm.red.business.helper.adobe.IAdobeLCHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.pdf.PdfHelper;
import it.ibm.red.business.helper.signing.SignHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.nps.dto.AllegatoNpsDTO;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.AooFilenet;
import it.ibm.red.business.persistence.model.CodaTrasformazione;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.Utente;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IASignSRV;
import it.ibm.red.business.service.IAooSRV;
import it.ibm.red.business.service.ICodaMailSRV;
import it.ibm.red.business.service.INpsSRV;
import it.ibm.red.business.service.IRegistroRepertorioSRV;
import it.ibm.red.business.service.ISalvaDocumentoSRV;
import it.ibm.red.business.service.ITrasformazionePDFSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.DocumentoUtils;
import it.ibm.red.business.utils.FileUtils;
import npstypes.v1.it.gov.mef.DocTipoOperazioneType;

/**
 * The Class TrasformazionePDFSRV.
 *
 * @author a.dilegge
 * 
 *         Servizio gestione trasformazioni PDF.
 */
@Service
@Component
public class TrasformazionePDFSRV extends AbstractService implements ITrasformazionePDFSRV {

	private static final String CONVERSIONE_PDF = "CONVERSIONE PDF (";

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -1720173749210830823L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TrasformazionePDFSRV.class.getName());

	/**
	 * Label FILE.
	 */
	private static final String FILE_LABEL = "File: ";

	/**
	 * Messaggio errore inserimento documento nella coda trasformazione PDF.
	 */
	private static final String ERROR_INSERIMENTO_DOCUMENTO_MSG = "Errore nell'inserimento nel DB di un documento nella coda trasformazione PDF.";

	/**
	 * Mime type non supportati.
	 */
	private static final List<String> UNSUPPORTED_MIME_TYPES = Arrays.asList(ContentType.P7M_MIME, ContentType.P7M, ContentType.MSG, ContentType.EML, "message",
			ContentType.P7M_X_MIME, ContentType.P7M_PKNET_MANAGER, ContentType.MIMETYPE_ENC, ContentType.XML, ContentType.TEXT_XML);

	/**
	 * Estensione.
	 */
	private static final String PDF_EXTENSION = ".pdf";

	/**
	 * Factory.
	 */
	private transient DiskFileItemFactory diskFileItemFactory = new DiskFileItemFactory();

	/**
	 * Fornitore della properties dell'applicazione.
	 */
	private PropertiesProvider pp;
	
	/**
	 * DAO coda trasformazione.
	 */
	@Autowired
	private ICodaTrasformazioneDAO codaTrasformazioneDAO;

	/**
	 * DAO Nodo.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * DAO Utente.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;

	/**
	 * DAO Ruolo.
	 */
	@Autowired
	private IRuoloDAO ruoloDAO;

	/**
	 * DAO IterApprovativoDAO.
	 */
	@Autowired
	private IIterApprovativoDAO iterApprovativoDAO;

	/**
	 * NpsSRV.
	 */
	@Autowired
	private INpsSRV npsSRV;

	/**
	 * UtenteSRV.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;

	/**
	 * UtenteSRV.
	 */
	@Autowired
	private ISalvaDocumentoSRV salvaDocSRV;
	
	/**
	 * AooSRV.
	 */
	@Autowired
	private IAooSRV aooSRV;

	/**
	 * Dao gestione aoo.
	 */
	@Autowired
	private IAooDAO aooDAO;

	/**
	 * Dao gestione tipoFile.
	 */
	@Autowired
	private ITipoFileDAO tipoFileDAO;

	/**
	 * Servizio per la gestione dei Registri di Repertorio.
	 */
	@Autowired
	private IRegistroRepertorioSRV registroRepertorioSRV;

	/**
	 * DAO per la gestione dei segni grafici.
	 */
	@Autowired
	private IStampigliaturaSegnoGraficoDAO stampigliaturaSegnoGraficoDAO;

	/**
	 * Servizio per l'invio delle email
	 * */
	@Autowired
	private ICodaMailSRV codaMailSRV;
	
	/**
	 * Servizio firma asincrona.
	 */
	@Autowired
	private IASignSRV aSignSRV;

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}
	
	/**
	 * Recupero di tutti i job configurati su db.
	 * 
	 * @return
	 */
	@Override
	public final CodaTrasformazioneDTO getItem(final int idAoo) {
		CodaTrasformazioneDTO output = null;
		Connection connection = null;
		CodaTrasformazione item = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			item = codaTrasformazioneDAO.getAndLockFirstItem(idAoo, connection);
			if (item != null) {

				output = new CodaTrasformazioneDTO(item.getIdCoda(), item.getIdDocumento(), item.getProcessoChiamante(), item.getTipoTrasformazione(), item.getStato(),
						item.getPrioritaria(), item.getDataInserimento(), item.getMessaggioErrore(),
						(CodaTrasformazioneParametriDTO) FileUtils.getObjectFromByteArray(item.getParametri()));

				codaTrasformazioneDAO.changeStatusItem(output.getIdCoda(), StatoCodaTrasformazioneEnum.PRESA_IN_CARICO.getStatus(), "", connection);
			}
			commitConnection(connection);

		} catch (final Exception e) {

			if (item != null) {

				LOGGER.error("Errore nel recupero dell'item " + item.getIdCoda() + " relativo al documento " + item.getIdDocumento(), e);

				// se sono riuscito a recuperare l'item, marcalo come errore
				try {
					changeStatusItem(item.getIdCoda(), StatoCodaTrasformazioneEnum.ERRORE, "Errore in fase di parsing dell'item", connection);
					commitConnection(connection);
				} catch (final Exception e2) {
					LOGGER.error("Errore in fase di aggiornamento delloo stato dell'item " + item.getIdCoda(), e2);
					rollbackConnection(connection);
				}

			} else {

				LOGGER.error("Errore nel recupero dell'item dal DB", e);
				rollbackConnection(connection);

			}

		} finally {
			closeConnection(connection);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITrasformazionePDFFacadeSRV#changeStatusItem(int,
	 *      it.ibm.red.business.enums.StatoCodaTrasformazioneEnum,
	 *      java.lang.String).
	 */
	@Override
	public void changeStatusItem(final int idCoda, final StatoCodaTrasformazioneEnum stato, final String messaggioErrore) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			changeStatusItem(idCoda, stato, messaggioErrore, connection);
			commitConnection(connection);
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di aggiornamento dell'item " + idCoda + ": " + e.getMessage(), e);
			rollbackConnection(connection);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

	}

	private void changeStatusItem(final int idCoda, final StatoCodaTrasformazioneEnum stato, final String inMessaggioErrore, final Connection connection) {
		String messaggioErrore = inMessaggioErrore;
		if (messaggioErrore != null) {
			messaggioErrore = (messaggioErrore.length() > 4000) ? messaggioErrore.substring(0, 4000) : messaggioErrore;
		}

		codaTrasformazioneDAO.changeStatusItem(idCoda, stato.getStatus(), messaggioErrore, connection);
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITrasformazionePDFFacadeSRV#setItemDone(int,
	 *      long).
	 */
	@Override
	public void setItemDone(final int idCoda, final long secondElapsed) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			codaTrasformazioneDAO.closeItem(idCoda, secondElapsed, connection);
			commitConnection(connection);
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di aggiornamento dell'item " + idCoda + ": " + e.getMessage(), e);
			rollbackConnection(connection);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITrasformazionePDFFacadeSRV#isContentTrasformabile(java.lang.String,
	 *      java.lang.String).
	 */
	@Override
	public boolean isContentTrasformabile(final String mimeType, final String estensione) {
		boolean isTrasformabile = false;
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), true);
			if (mimeType == null || "application/octet-stream".equalsIgnoreCase(mimeType)) {
				isTrasformabile = tipoFileDAO.isEstensioneConvertibile(estensione, connection);
			} else {
				isTrasformabile = tipoFileDAO.isMimeTypeConvertibile(mimeType, connection);
			}
			commitConnection(connection);
		} catch (final Exception e) {
			LOGGER.error("Errore in fase di retrieve info sulla tabella TipoFile " + e);
			rollbackConnection(connection);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
		return isTrasformabile;
	}

	/**
	 * Metodo che esegue la trasformazione in PDF del documento registrato
	 * all'interno dei parametri in input.
	 * 
	 * @param parametri
	 */
	@Override
	public void doTransformation(final CodaTrasformazioneParametriDTO parametri) {
		IAdobeLCHelper adobeh = null;
		String idDocumento = null;
		String guidNewDocPrincipale4NPS = null;
		Connection connection = null;
		IFilenetCEHelper fceh = null;

		try {
			adobeh = new AdobeLCHelper();
			idDocumento = parametri.getIdDocumento();

			LOGGER.info(CONVERSIONE_PDF + idDocumento + "): START");
			LOGGER.info(CONVERSIONE_PDF + idDocumento + "): " + parametri);

			// preparazione dati per la trasformazione
			connection = setupConnection(getDataSource().getConnection(), false);
			final Nodo nodo = nodoDAO.getNodo(parametri.getIdNodo(), connection);
			final Aoo aoo = nodo.getAoo();
			final AooFilenet aooFilenet = aoo.getAooFilenet();
			final Utente utente = utenteDAO.getUtente(parametri.getIdUtente(), connection);
			final UtenteDTO utenteDTO = utenteSRV.getByUsername(utente.getUsername());

			fceh = FilenetCEHelperProxy.newInstance(aooFilenet.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
					aooFilenet.getObjectStore(), aoo.getIdAoo());

			final Document documento = fceh.getDocumentByIdGestionale(idDocumento, null, null, aoo.getIdAoo().intValue(), null, null);
			if (documento == null) {
				throw new FilenetException("Documento con id = " + idDocumento + " non trovato");
			}

			final boolean isAdminConnection = isAdminConnection(documento, connection);

			if (!isAdminConnection) {
				popSubject(fceh);
				fceh = FilenetCEHelperProxy.newInstance(utente.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
						aooFilenet.getObjectStore(), aoo.getIdAoo());
			}

			final Properties metadati = documento.getProperties();

			final Integer annoProtocollo = (Integer) TrasformerCE.getMetadato(documento, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY);
			final Integer numeroProtocollo = (Integer) TrasformerCE.getMetadato(documento, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY);
			final String dataProtocollo = DateUtils.dateToString(metadati.getDateTimeValue((pp.getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY))), null);
			final Integer idTipoDocumento = (Integer) TrasformerCE.getMetadato(documento, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY);
			final Integer idTipoProcedimento = (Integer) TrasformerCE.getMetadato(documento, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY);
			final Integer uffCreatore = (Integer) TrasformerCE.getMetadato(documento, PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY);
			final Integer tipoProtocollo = (Integer) TrasformerCE.getMetadato(documento, PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY);
			
			String idProtocollo = null;
			String numeroAnnoProtocollo = null;
			String protocollo4Conversion = null;
			ProtocolloNpsDTO protocolloNps = null;

			Boolean isRegistroRepertorio = Boolean.FALSE;
			if (idTipoDocumento != null && idTipoProcedimento != null) {
				isRegistroRepertorio = registroRepertorioSRV.checkIsRegistroRepertorio(aoo.getIdAoo(), idTipoDocumento.longValue(), idTipoProcedimento.longValue(), null,
						connection);
			}

			if (metadati.isPropertyPresent(pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY))
					&& metadati.getStringValue(pp.getParameterByKey(PropertiesNameEnum.ID_PROTOCOLLO_METAKEY)) != null) {
				idProtocollo = (String) TrasformerCE.getMetadato(documento, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY);
				numeroAnnoProtocollo = numeroProtocollo + "/" + annoProtocollo + "_" + aoo.getCodiceAoo();
				
				if(parametri.isFirstVersionOnNPS()) {
					protocolloNps = new ProtocolloNpsDTO();
					protocolloNps.setIdProtocollo(idProtocollo);
					protocolloNps.setTipoProtocollo(TipoProtocolloEnum.getById(tipoProtocollo).getCodiceEstesoNPS());
					//recupera il guid della prima versione du NPS
					protocolloNps = npsSRV.getDettagliProtocollo(protocolloNps , false, aoo.getIdAoo().intValue(), utenteDTO, false, connection);
				}
				
			}

			final Integer categoria = (Integer) TrasformerCE.getMetadato(documento, PropertiesNameEnum.IDCATEGORIADOCUMENTO_METAKEY);
			final Integer sottoCategoriaUscita = (Integer) TrasformerCE.getMetadato(documento, PropertiesNameEnum.SOTTOCATEGORIA_DOCUMENTO_USCITA);
			SottoCategoriaDocumentoUscitaEnum sottoCategoriaDocumento = null;
			if (sottoCategoriaUscita != null) {
				sottoCategoriaDocumento = SottoCategoriaDocumentoUscitaEnum.get(sottoCategoriaUscita);
			}
			Integer momentoProtocollazione = -1;
			boolean isDocEntrata = DocumentoUtils.isCategoriaDocumentoEntrata(categoria);
			
			if (!isDocEntrata && metadati.isPropertyPresent(pp.getParameterByKey(PropertiesNameEnum.MOMENTO_PROTOCOLLAZIONE_ID_METAKEY))
					&& metadati.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.MOMENTO_PROTOCOLLAZIONE_ID_METAKEY)) != null) {
				momentoProtocollazione = metadati.getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.MOMENTO_PROTOCOLLAZIONE_ID_METAKEY));
			}
			
			//invia content convertiti ad nps se è una protocollazione standard o se è iter per spedizione
			final boolean inviaContentConvertitiNPS = momentoProtocollazione != MomentoProtocollazioneEnum.PROTOCOLLAZIONE_NON_STANDARD.getId() ||
					(parametri.getIdTipoAssegnazione() != null && TipoAssegnazioneEnum.SPEDIZIONE.getId() == parametri.getIdTipoAssegnazione());

			final Integer idCategoriaDocumento = isDocEntrata ? CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.getIds()[0] : CategoriaDocumentoEnum.DOCUMENTO_USCITA.getIds()[0];

			boolean apponiCampoFirma = false;
			boolean firmato = false;
			Integer numeroDocumento = null;
			String commaSeparatedIdUteSimboloGrafico = null;

			Long idTipoProtocollo = aoo.getTipoProtocollo().getIdTipoProtocollo();
			final Integer numeroProtocolloEmergenza = (Integer) TrasformerCE.getMetadato(documento, PropertiesNameEnum.NUMERO_PROTOCOLLO_EMERGENZA_METAKEY);
			if (numeroProtocolloEmergenza != null && numeroProtocolloEmergenza > 0) {
				idTipoProtocollo = TipologiaProtocollazioneEnum.EMERGENZANPS.getId();
			}

			// ### ELABORAZIONE DOCUMENTO PRINCIPALE
			if (parametri.isPrincipale()) {
				// Doc principale
				LOGGER.info(CONVERSIONE_PDF + idDocumento + "): DOCUMENTO PRINCIPALE");

				String nomeClasseDocumentale = documento.getClassName();
				boolean isDSR = nomeClasseDocumentale.equals(pp.getParameterByKey(PropertiesNameEnum.DSR_CLASSNAME_FN_METAKEY));
				boolean isDD = nomeClasseDocumentale.equals(pp.getParameterByKey(PropertiesNameEnum.DECRETO_DIRIGENZIALE_FEPA_CLASSNAME_FN_METAKEY));
				String nomeFile = documento.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY));
				String estensione ="";
				if(nomeFile!=null) {
					String[] arrEstensione = nomeFile.split("\\.");
					if(arrEstensione.length >0) {
						estensione = arrEstensione[arrEstensione.length-1];
					}
				}
			
				boolean isTrasformabile = isContentTrasformabile(documento.get_MimeType(), estensione);

				// Esclusione DSR e P7M, si possono evitare
				boolean elaboraFile = false;
				if (!isDSR && (isTrasformabile || parametri.isProtocollazioneP7M())) {
					FileItem daElaborare = getFileItem(documento, parametri.getFileRevisionNumber(), fceh, parametri.isProtocollazioneP7M(), utenteDTO);
					
					if (daElaborare.getInputStream().available() != 0) {
						elaboraFile = true;
					
						// Stampigliatura protocollo o numero documento
						if (!isDD) {
							protocollo4Conversion = PdfHelper.getStampigliaturaProtocollo(true, idCategoriaDocumento, annoProtocollo, numeroProtocollo, 
									dataProtocollo, parametri.isFlagFirmaCopiaConforme(), aoo.getCodiceAoo(), aoo.getDescrizione(), idTipoProtocollo, 
									isRegistroRepertorio, utenteDTO.getTimbroUscitaAoo(), null);
						}

						if (!isDocEntrata) {
							numeroDocumento = (Integer) TrasformerCE.getMetadato(documento, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY);
						
							if (parametri.getTipoFirma() != null) {
								// Si inserisce il campo firma solo se non ne è già presente uno
								apponiCampoFirma = !checkEmptySignFields(daElaborare, adobeh);
							}
						}
					
						byte[] contentPrincipale = FileUtils.getByteFromInputStream(daElaborare.getInputStream());
						String contentTypePrincipale = daElaborare.getContentType();
						String nomeFilePrincipale = daElaborare.getName();
						firmato = ContentType.PDF.equalsIgnoreCase(contentTypePrincipale) && PdfHelper.getSignatureNumber(contentPrincipale) > 0;
						// Trasforma in pdf se:
						// a) non è un pdf
						// b) non è firmato
						// c) non ha sottocategoria
						// b) ha una sottocategoria non associata a registrazioni ausiliarie
						if ((!ContentType.PDF.equalsIgnoreCase(contentTypePrincipale) || !firmato) &&
								(sottoCategoriaDocumento == null || !sottoCategoriaDocumento.isRegistrazioneAusiliaria())) {
							contentPrincipale = adobeh.trasformazionePDF(nomeFilePrincipale, contentPrincipale, false, idDocumento);
							if (contentPrincipale != null) {
								contentTypePrincipale = ContentType.PDF;
								nomeFilePrincipale = FilenameUtils.getBaseName(nomeFilePrincipale) + ".pdf";
								daElaborare.delete();
								daElaborare = diskFileItemFactory.createItem(nomeFilePrincipale, ContentType.PDF, false, nomeFilePrincipale);
								IOUtils.copy(new ByteArrayInputStream(contentPrincipale), daElaborare.getOutputStream());
							} else {
								throw new RedException("Errore nel processo Adobe di conversione in formato PDF del documento principale: " + idDocumento);
							}
						}
						
						// PDF inserito dall'utente oppure ottenuto dal processo di trasformazione
						if (ContentType.PDF.equalsIgnoreCase(contentTypePrincipale)) {
							// ### Principale
							FileDTO fdDocumento = gestisciContentPDFPerStampigliaturePrincipale(daElaborare, parametri, protocollo4Conversion, numeroDocumento, 
									parametri.getIdTipoAssegnazione(), aoo, firmato, isDocEntrata, apponiCampoFirma, connection);
						
							byte[] contentPreview = FileUtils.getByteFromInputStream(PdfHelper.getPDFPreview(fdDocumento.getContent()));
							FileDTO fdPreview = new FileDTO(fdDocumento.getFileName(), contentPreview, fdDocumento.getMimeType());
							
							// ### Aggiornamento del CE con l'inserimento di una nuova versione del documento principale
							updateDocument(documento, fdDocumento, fdPreview, fceh, null, aoo.getIdAoo());
						}
					}
					
					daElaborare.delete();
					
					if (inviaContentConvertitiNPS && elaboraFile && TipologiaProtocollazioneEnum.isProtocolloNPS(utenteDTO.getIdTipoProtocollo()) && idProtocollo != null) {
						String oggetto = metadati.getStringValue(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY));

						Document documentoPrincipale = fceh.getDocumentByIdGestionale(idDocumento, null, null, aoo.getIdAoo().intValue(), null, null);
						guidNewDocPrincipale4NPS = documentoPrincipale.get_Id().toString();
						
						String guidFirstVersionDocPrincipale = fceh.getFirstVersionById(idDocumento, aoo.getIdAoo().intValue()).get_Id().toString();
						
						if (guidFirstVersionDocPrincipale != null && parametri.isFirstVersionOnNPS()
								&& protocolloNps != null && protocolloNps.getDocumentoPrincipale() != null) {
							guidFirstVersionDocPrincipale = protocolloNps.getDocumentoPrincipale().getGuid();
						}
						
						// ### Upload asincrono documento principale su DB
						FileItem fiDoc = getFileItem(documentoPrincipale, null, fceh, parametri.isProtocollazioneP7M(), utenteDTO);
						if (fiDoc.getInputStream().available() != 0) {
							int idNar = npsSRV.uploadDocToAsyncNps(fiDoc.getInputStream(), guidNewDocPrincipale4NPS, idProtocollo,
									fiDoc.getContentType(), fiDoc.getName(), oggetto, numeroAnnoProtocollo, utenteDTO, idDocumento, connection);

							npsSRV.associateDocumentoProtocolloToAsyncNps(idNar, guidNewDocPrincipale4NPS, idProtocollo,
									fiDoc.getName(), oggetto, utenteDTO, true, true, guidFirstVersionDocPrincipale, DocTipoOperazioneType.COLLEGA_DOCUMENTO, 
									numeroAnnoProtocollo, idDocumento, connection);
						}
						
						if (fiDoc != null) {
							fiDoc.delete();
						}
					}
				} else {
					LOGGER.info(CONVERSIONE_PDF + idDocumento + ") - DOCUMENTO PRINCIPALE: TRASFORMAZIONE NON ESEGUITA IN QUANTO DSR O P7M");
				}
			}
			
			// ### ELABORAZIONE ALLEGATI
			if (parametri.isAllegati()) {
				LOGGER.info(CONVERSIONE_PDF + idDocumento + "): ALLEGATI");

				final DocumentSet allegati = fceh.getAllegati(documento.get_Id().toString(), false, true, UNSUPPORTED_MIME_TYPES, parametri.getIdAllegati());

				if (!allegati.isEmpty()) {
					final Iterator<?> iter = allegati.iterator();
					FileItem allDaElaborare = null;
					String documentTitleAll = null;
					Document dAllConvertito = null;
					String nomeAll = null;
					boolean mantieniFormatoOriginale;
					boolean stampigliaSegniGrafici;
					Map<String, byte[]> placeholderUtente;
					while (iter.hasNext()) {
						final Document dAll = (Document) iter.next();
						documentTitleAll = dAll.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
						nomeAll = dAll.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY));
						allDaElaborare = getFileItem(dAll, parametri.getFileRevisionNumber(), fceh,	parametri.isProtocollazioneP7M(), utenteDTO);
						
						// Gestione timbro Uscita Allegati
						final Boolean timbroUscitaMetadatoBool = (Boolean) TrasformerCE.getMetadato(dAll, PropertiesNameEnum.IS_TIMBRO_USCITA_METAKEY);
						boolean timbroUscitaMetadato = false;
						if (timbroUscitaMetadatoBool != null) {
							timbroUscitaMetadato = timbroUscitaMetadatoBool;
						}
						final boolean timbroUscita = utenteDTO.getTimbroUscitaAoo() && timbroUscitaMetadato;

						if (allDaElaborare.getInputStream().available() != 0) {
							apponiCampoFirma = false;
							// Gestione apposizione campo firma allegati
							if (isSignatureFieldCompliance(allDaElaborare, dAll, adobeh, isDocEntrata, parametri.getIdAllegatiDaFirmare())) {
								apponiCampoFirma = true;
							}
							
							protocollo4Conversion = PdfHelper.getStampigliaturaProtocollo(false, idCategoriaDocumento, annoProtocollo, numeroProtocollo, dataProtocollo, 
									parametri.isFlagFirmaCopiaConforme(), aoo.getCodiceAoo(), aoo.getDescrizione(), idTipoProtocollo, isRegistroRepertorio, 
									timbroUscita, null);
							mantieniFormatoOriginale = Boolean.TRUE.equals(TrasformerCE.getMetadato(dAll, PropertiesNameEnum.IS_FORMATO_ORIGINALE_METAKEY));
							firmato = FormatoAllegatoEnum.FIRMATO_DIGITALMENTE.getId().equals(TrasformerCE.getMetadato(dAll, PropertiesNameEnum.FORMATO_ALLEGATO_ID_METAKEY));
							stampigliaSegniGrafici = Boolean.TRUE.equals(TrasformerCE.getMetadato(dAll, PropertiesNameEnum.STAMPIGLIATURA_SIGLA_ALLEGATO));
							placeholderUtente = null;
							
							if (thereIsSomeThingToDoForThisAttachment(allDaElaborare, dAll, apponiCampoFirma, protocollo4Conversion, mantieniFormatoOriginale, firmato, 
									stampigliaSegniGrafici, timbroUscita)) {
								List<String> placeholderDaSostituire = null;  
							
								byte[] allContent = FileUtils.getByteFromInputStream(allDaElaborare.getInputStream());
								String allContentType = allDaElaborare.getContentType();
								String allNomeFile = allDaElaborare.getName();
								
								if (!firmato) {
									allContent = adobeh.trasformazionePDF(allNomeFile, allContent, false, documentTitleAll);
									if (allContent != null) {
										allContentType = ContentType.PDF;
										allNomeFile = FilenameUtils.getBaseName(allNomeFile) + ".pdf";
										allDaElaborare.delete();
										allDaElaborare = diskFileItemFactory.createItem(allNomeFile, ContentType.PDF, false, allNomeFile);
										IOUtils.copy(new ByteArrayInputStream(allContent), allDaElaborare.getOutputStream());
									} else {
										throw new RedException("Errore nel processo Adobe di conversione in formato PDF dell'allegato: " + documentTitleAll);
									}
								}
													
								// PDF inserito dall'utente oppure ottenuto dal processo di trasformazione
								if (ContentType.PDF.equalsIgnoreCase(allContentType)) {
									// ### Allegato
									if (stampigliaSegniGrafici && !Integer.valueOf(TipoAssegnazioneEnum.FIRMA_MULTIPLA.getId()).equals(parametri.getIdTipoAssegnazione())) {
										commaSeparatedIdUteSimboloGrafico = getFirmatariStringList(parametri.getFirmatariString(), 
												parametri.getIdNodo(), parametri.getTipoFirma(), connection);
										
										if (commaSeparatedIdUteSimboloGrafico != null) {
											placeholderUtente = stampigliaturaSegnoGraficoDAO.getPlaceholderSegnoGrafico(
													Long.valueOf(commaSeparatedIdUteSimboloGrafico), uffCreatore.longValue(), idTipoDocumento, connection);
											
											placeholderDaSostituire = PdfHelper.getPlaceholderFromAllegati(allContent, placeholderUtente);
											
											stampigliaturaSegnoGraficoDAO.insertOrUpdatePlaceholderJob(idDocumento,uffCreatore.longValue(), 
													Long.valueOf(commaSeparatedIdUteSimboloGrafico), idTipoDocumento, placeholderDaSostituire, connection);
										}
									}
									
									FileDTO fdDocumento = gestisciContentPDFPerStampigliatureAllegato(allDaElaborare, parametri, protocollo4Conversion, 
											parametri.getIdTipoAssegnazione(), aoo, firmato, isDocEntrata, apponiCampoFirma, mantieniFormatoOriginale, stampigliaSegniGrafici, 
											placeholderUtente, connection);
									
									// ### Aggiornamento del CE con l'inserimento di una nuova versione dell'allegato
									dAllConvertito = updateDocument(dAll, fdDocumento, null, fceh, placeholderDaSostituire, aoo.getIdAoo());
								}
								
								// ### Gestione NPS
								if (inviaContentConvertitiNPS && TipologiaProtocollazioneEnum.isProtocolloNPS(utenteDTO.getIdTipoProtocollo()) && idProtocollo != null) {

									// Upload asincrono allegato su DB
									final FileItem fiAll = getFileItem(dAllConvertito, null, fceh, parametri.isProtocollazioneP7M(), utenteDTO);

									if (fiAll.getInputStream().available() != 0) {
										String oggetto = null;
										if (dAllConvertito.getProperties().isPropertyPresent(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY))) {
											oggetto = dAllConvertito.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY));
										}

										final int idNar = npsSRV.uploadDocToAsyncNps(fiAll.getInputStream(), dAllConvertito.get_Id().toString(), idProtocollo,
												fiAll.getContentType(), fiAll.getName(), oggetto, numeroAnnoProtocollo, utenteDTO, documentTitleAll, connection);

										String guidFirstVersionAllegato = dAll.get_Id().toString();
										
										if (guidFirstVersionAllegato != null && parametri.isFirstVersionOnNPS() && protocolloNps != null
												&& protocolloNps.getDocumentoPrincipale() != null && protocolloNps.getAllegatiList() != null && nomeAll != null) {
											for (AllegatoNpsDTO allNps : protocolloNps.getAllegatiList()) {
												if (nomeAll.equals(allNps.getNomeFile())) {
													guidFirstVersionAllegato = allNps.getGuid();
													break;
												}
											}
										}
										
										// Associa nuova versione del documento a NPS
										npsSRV.associateDocumentoProtocolloToAsyncNps(idNar, dAllConvertito.get_Id().toString(), idProtocollo, fiAll.getName(), 
												oggetto, utenteDTO, false, false, guidFirstVersionAllegato, DocTipoOperazioneType.COLLEGA_DOCUMENTO,
												numeroAnnoProtocollo, documentTitleAll, connection);
									}

									if (fiAll != null) {
										fiAll.delete();
									}

								}
							} else {
								LOGGER.warn("Allegato " + documentTitleAll + " - "	+ allDaElaborare.getName() + " scartato");
							}

							allDaElaborare.delete();
						}

					}
				} else {
					LOGGER.warn(CONVERSIONE_PDF + idDocumento + "): nessun allegato da elaborare ");
				}
			}

			// Si aggiorna il flag renderizzato solo se:
			// 1) NON si tratta di un documento in entrata
			// 2) l'assegnazione NON è per competenza o spedizione
			if (!isDocEntrata 
					&& (parametri.getIdTipoAssegnazione() == null ||
						(TipoAssegnazioneEnum.COMPETENZA.getId() != parametri.getIdTipoAssegnazione()
							&& TipoAssegnazioneEnum.SPEDIZIONE.getId() != parametri.getIdTipoAssegnazione()))) {
				updateFlagRenderizzato(utente, aooFilenet, Integer.parseInt(idDocumento));
			}

			if (parametri.isAggiornaFirmaPDF()) {
				final String guidNew = fceh.getDocumentByIdGestionale(idDocumento, null, null, aoo.getIdAoo().intValue(), null, null).get_Id().toString();
				fceh.updateFirmaPDF(guidNew);
			}
		} catch (final Exception e) {
			LOGGER.error(CONVERSIONE_PDF + idDocumento + "): " + e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
			popSubject(fceh);
			LOGGER.info(CONVERSIONE_PDF + idDocumento + "): END");
		}
	}

	/**
	 * @see it.ibm.red.business.service.ITrasformazionePDFSRV#insertSchedule(
	 *      it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.CodaTrasformazioneParametriDTO,
	 *      java.lang.String, java.sql.Connection).
	 */
	@Override
	public void insertSchedule(final UtenteDTO utente, final CodaTrasformazioneParametriDTO parametri, final String chiamante, final Connection connection) {
		try {
			final CodaTrasformazione codaTrasformazione = new CodaTrasformazione();
			codaTrasformazione.setIdAoo(utente.getIdAoo().intValue());
			codaTrasformazione.setIdDocumento(parametri.getIdDocumento());
			codaTrasformazione.setProcessoChiamante(chiamante);
			codaTrasformazione.setStato(StatoCodaTrasformazioneEnum.DA_ELABORARE.getStatus());
			codaTrasformazione.setTipoTrasformazione(Constants.Varie.TRASFORMATAIMPL);
			codaTrasformazione.setMessaggioErrore("");
			codaTrasformazione.setParametri(FileUtils.getByteObject(parametri));

			final int categoriaRuolo = getCategoriaRuolo(utente, connection);
			if (categoriaRuolo == Constants.Ruolo.ISPETTORE) {
				// Priorità impostata a 1 per l'ispettore
				codaTrasformazione.setPrioritaria(1);
			}

			final int result = codaTrasformazioneDAO.insert(codaTrasformazione, connection);

			if (result > 0) {
				LOGGER.info("Inserimento nel DB del documento: " + parametri.getIdDocumento() + " nella coda trasformazione PDF.");
			} else {
				LOGGER.error(ERROR_INSERIMENTO_DOCUMENTO_MSG);
				throw new RedException(ERROR_INSERIMENTO_DOCUMENTO_MSG);
			}
		} catch (final Exception e) {
			LOGGER.error(ERROR_INSERIMENTO_DOCUMENTO_MSG, e);
			throw new RedException(ERROR_INSERIMENTO_DOCUMENTO_MSG, e);
		}
	}

	/**
	 * @param utente
	 * @param connection
	 * @return id categoria ruolo
	 */
	private int getCategoriaRuolo(final UtenteDTO utente, final Connection connection) {
		int categoriaRuolo = -1;
		try {
			if (utente != null && utente.getIdRuolo() != null && utente.getIdRuolo() != 0) {
				categoriaRuolo = ruoloDAO.getCategoriaByRuolo(utente.getIdRuolo(), connection);
			}
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore nel recupero del ruolo", e);
		}
		return categoriaRuolo;
	}

	/**
	 * @see it.ibm.red.business.service.ITrasformazionePDFSRV#
	 *      avviaTrasformazionePDFPerNuovaAssegnazioneFirmaSiglaVisto(
	 *      java.lang.String, java.lang.String, java.lang.Integer,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.String[],
	 *      java.lang.Integer, java.lang.String,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetHelper,
	 *      java.sql.Connection).
	 */
	@Override
	public void avviaTrasformazionePDFPerNuovaAssegnazioneFirmaSiglaVisto(final String idDocumento, final String guid, final Integer idTipoAssegnazione,
			final UtenteDTO utente, final String[] firmatari, final Integer tipoFirma, final String chiamante, final IFilenetCEHelper fceh, final Connection con,
			final boolean isModificaIter) {
		try {
			final Document doc = fceh.getDocumentByGuid(guid);
			final String mimeType = doc.get_MimeType();
			Integer fileRevisionNumber = null;
			if (MediaType.PDF.toString().equalsIgnoreCase(mimeType)) {
				if (isModificaIter) {
					fileRevisionNumber = fceh.getPreviousVersionWordRevisionNumber(guid);
				} else {
					final Aoo aoo = aooDAO.getAoo(utente.getIdAoo(), con);
					final byte[] content = FilenetCEHelper.getDocumentContentAsByte(doc);
					final boolean isValido = salvaDocSRV.verificaFirmaIterSpedizione(null, content, utente, false, aoo.getSignerTimbro(), MediaType.PDF);
					if (!isValido) {
						fileRevisionNumber = fceh.getPreviousVersionWordRevisionNumber(guid);
					}
				}
			}

			List<String> idAllegatiDaFirmare = null;
			List<String> idAllegatiDaTrasformare = null;

			final DocumentSet allegatiFilenet = fceh.getAllegatiConContent(guid);
			if (allegatiFilenet != null && !allegatiFilenet.isEmpty()) {
				final IAdobeLCHelper adobeHelper = new AdobeLCHelper();
				idAllegatiDaFirmare = new ArrayList<>();
				idAllegatiDaTrasformare = new ArrayList<>();

				final Iterator<?> itAllegatiFilenet = allegatiFilenet.iterator();
				Document allegatoFilenet = null;
				while (itAllegatiFilenet.hasNext()) {
					allegatoFilenet = (Document) itAllegatiFilenet.next();

					final Integer daFirmare = (Integer) TrasformerCE.getMetadato(allegatoFilenet, PropertiesNameEnum.DA_FIRMARE_METAKEY);
					if (BooleanFlagEnum.SI.getIntValue().equals(daFirmare)) {
						idAllegatiDaTrasformare.add((String) TrasformerCE.getMetadato(allegatoFilenet, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
						idAllegatiDaFirmare.add((String) TrasformerCE.getMetadato(allegatoFilenet, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
					}

					// Se l'allegato è un PDF con almeno un campo firma vuoto, si elimina l'ultima
					// versione in quanto
					// una nuova versione in PDF e con campo firma vuoto dell'allegato sarà aggiunta
					// nuovamente dal processo di trasformazione PDF
					if (MediaType.PDF.toString().equals(allegatoFilenet.get_MimeType())
							&& FileUtils.hasEmptySignFields(FilenetCEHelper.getDocumentContentAsInputStream(allegatoFilenet), adobeHelper)) {
						fceh.eliminaUltimaVersione(allegatoFilenet);
					}
				}
			}

			final CodaTrasformazioneParametriDTO cdp = new CodaTrasformazioneParametriDTO();
			cdp.setIdDocumento(idDocumento);
			cdp.setIdNodo(utente.getIdUfficio());
			cdp.setIdUtente(utente.getId());
			cdp.setFirmatariString(firmatari);
			cdp.setTipoFirma(tipoFirma);
			cdp.setIdTipoAssegnazione(idTipoAssegnazione);
			cdp.setPrincipale(true);
			cdp.setIdCoda(null);
			cdp.setFileRevisionNumber(fileRevisionNumber);
			cdp.setProtocollazioneP7M(false);
			cdp.setFlagFirmaCopiaConforme(false);
			cdp.setAggiornaFirmaPDF(true);

			// Si verifica che l'insieme degli allegati e del sottoinsieme allegati da
			// firmare sia consistente
			if (!CollectionUtils.isEmpty(idAllegatiDaTrasformare)) {
				cdp.setAllegati(true);
				cdp.setIdAllegati(idAllegatiDaTrasformare);
			} else {
				cdp.setAllegati(false);
				cdp.setIdAllegati(null);
			}

			if (!CollectionUtils.isEmpty(idAllegatiDaFirmare)) {
				cdp.setIdAllegatiDaFirmare(idAllegatiDaFirmare);
			}

			if (StringUtils.isNotBlank(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED)) && "true".equals(pp.getParameterByKey(PropertiesNameEnum.MOCK_ENABLED))) {
				LOGGER.warn("[avviaTrasformazionePDFPerNuovaAssegnazioneFirmaSiglaVisto] solo trasformazioni sincrone in regime mock");
				doTransformation(cdp);
			} else {
				insertSchedule(utente, cdp, this.getClass().getSimpleName(), con);
			}

		} catch (final Exception e) {
			LOGGER.error("Errore nell'inserimento di un documento nella coda trasformazione PDF.", e);
			throw new RedException("Errore nell'inserimento di un documento nella coda trasformazione PDF.");
		}
	}
	
	/**
	 * @param documento
	 * @param fdDoc
	 * @param fceh
	 * @param placeholderDaSostituire
	 * @return
	 */
	private Document updateDocument(final Document documento, final FileDTO fdDoc, final FileDTO fdPreview, final IFilenetCEHelper fceh, final List<String> placeholderDaSostituire, final Long idAoo) {
		String fileName = null;
		String mimeType = null;
		Map<String, Object> metadati = null;
		if (fdDoc != null) {
			fileName = fdDoc.getFileName();
			metadati = new HashMap<>();
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), fileName);
			mimeType = fdDoc.getMimeType();
		}
 
		if (metadati != null && !CollectionUtils.isEmpty(placeholderDaSostituire)) {
			String placeholderFilenet = StringUtils.join(placeholderDaSostituire.toArray(), "||");
			metadati.put(pp.getParameterByKey(PropertiesNameEnum.PLACEHOLDER_SIGLA_ALLEGATO), placeholderFilenet);
		}
		documento.refresh();

		InputStream content = fdDoc != null ? new ByteArrayInputStream(fdDoc.getContent()) : null;
		InputStream contentLibroFirma = (fdDoc != null && fdDoc.getAnnotation() != null) ? new ByteArrayInputStream(fdDoc.getAnnotation()) : null;
		
		return fceh.addVersion(documento, content, fdPreview != null ? new ByteArrayInputStream(fdPreview.getContent()) : null, contentLibroFirma, metadati, fileName, mimeType, idAoo);
	}

	private boolean isAdminConnection(final Document documento, final Connection connection) {
		boolean adminConnection = false;

		try {
			final Properties metadati = documento.getProperties();

			if (metadati != null) {
				final Integer idNodoCreatore = (Integer) metadati.get(pp.getParameterByKey(PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY)).getObjectValue();

				final Integer riservato = (Integer) metadati.get(pp.getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY)).getObjectValue();

				if (riservato != null && riservato.equals(BooleanFlagEnum.SI.getIntValue()) && idNodoCreatore != null
						&& Boolean.TRUE.equals(nodoDAO.isNodoUcp(idNodoCreatore.longValue(), connection))) {
					adminConnection = true;
				}
			}
		} catch (final Exception ex) {
			LOGGER.error("Errore nel recupero del nodo creatore", ex);
		}

		return adminConnection;
	}

	private String getFirmatariStringList(final String[] firmatariString, final Long idNodo, final Integer tipoFirma, final Connection connection) {
		final String[] arrayFirmatari = getFirmatariStringArray(firmatariString, idNodo, tipoFirma, connection);

		return (arrayFirmatari != null && arrayFirmatari.length > 0) ? StringUtils.join(arrayFirmatari, ",") : Constants.EMPTY_STRING;
	}

	private List<Integer> getFirmatariIntegerList(final String[] firmatariString, final Long idNodo, final Integer tipoFirma, final Connection connection) {
		final List<Integer> firmatari = new ArrayList<>();

		final String[] arrayFirmatari = getFirmatariStringArray(firmatariString, idNodo, tipoFirma, connection);
		if (arrayFirmatari != null && arrayFirmatari.length > 0) {
			for (final String firmatario : arrayFirmatari) {
				firmatari.add(Integer.valueOf(firmatario));
			}
		}

		return firmatari;
	}

	private String[] getFirmatariStringArray(final String[] firmatariString, final Long idNodo, final Integer tipoFirma, final Connection connection) {
		String[] arrayFirmatari = null;
		int lunghezza = 0;

		if (firmatariString == null || firmatariString.length == 0) {
			final String firmatarioAutomatico = iterApprovativoDAO.getFirmatario(idNodo, tipoFirma, connection);
			if (!"0".equals(firmatarioAutomatico.split("\\,")[1])) {
				arrayFirmatari = new String[1];
				arrayFirmatari[0] = firmatarioAutomatico.split("\\,")[1];
			}
		} else {
			for (final String firmatario : firmatariString) {
				if (firmatario.split("\\,") != null && firmatario.split("\\,").length == 3) {
					if ("4".equals(firmatario.split("\\,")[2])) {
						lunghezza = lunghezza + 1;
					}
				} else {
					lunghezza = lunghezza + 1;
				}
			}

			arrayFirmatari = new String[lunghezza];
			int i = 0;
			for (final String firmatario : firmatariString) {
				if (firmatario.split("\\,") != null && firmatario.split("\\,").length == 3) {
					if ("4".equals(firmatario.split("\\,")[2])) {
						arrayFirmatari[i] = firmatario.split("\\,")[1];
						i++;
					}
				} else {
					final String[] arrF = firmatario.split("\\,");
					if (arrF == null || arrF.length < 2) {
						continue;
					}
					arrayFirmatari[i] = arrF[1];
					i++;
				}
			}
		}

		return arrayFirmatari;
	}

	private FileItem getFileItem(final Document inD, final Integer fileRevisionNumber, final IFilenetCEHelper fceh, final boolean protocollazioneP7M, final UtenteDTO utente)
			throws IOException {
		Document d = inD;
		if (d.get_ContentElements().isEmpty()) {
			throw new RedException("Documento " + d.get_Id() + " senza content");
		}

		if (fileRevisionNumber != null) {
			d = fceh.getDocumentVersion(d, fileRevisionNumber);
		}

		final String nomeFile = d.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY));
		final String mimetype = d.get_MimeType();

		if (isUnsupportedMimeType(mimetype, nomeFile) && !("p7m".equalsIgnoreCase(FilenameUtils.getExtension(nomeFile)) && protocollazioneP7M)) {
			throw new RedException("Documento " + d.get_Id() + " - " + nomeFile + " non valido per la trasformazione");
		}

		final FileItem daelaborare = diskFileItemFactory.createItem(nomeFile, mimetype, false, nomeFile);
		IOUtils.copy(((ContentTransfer) d.get_ContentElements().get(0)).accessContentStream(), daelaborare.getOutputStream());

		if ("p7m".equalsIgnoreCase(FilenameUtils.getExtension(nomeFile)) && protocollazioneP7M) {
			return FileUtils.p7mExtract(daelaborare, diskFileItemFactory, utente.getSignerInfo().getPkHandlerVerifica().getHandler(),
					utente.getSignerInfo().getPkHandlerVerifica().getSecurePin(), utente.getDisableUseHostOnly());
		}

		return daelaborare;
	}

	private static boolean isUnsupportedMimeType(final String contentType, final String fileName) {
		boolean isUnsupported = false;

		if (UNSUPPORTED_MIME_TYPES.contains(contentType)) {
			LOGGER.info(FILE_LABEL + fileName + " - unsupported ContentType: " + contentType);
			isUnsupported = true;
		}
		if (UNSUPPORTED_MIME_TYPES.contains(URLConnection.getFileNameMap().getContentTypeFor(fileName))) {
			LOGGER.info(FILE_LABEL + fileName + " - unsupported ContentType " + URLConnection.getFileNameMap().getContentTypeFor(fileName));
			isUnsupported = true;
		}
		if (UNSUPPORTED_MIME_TYPES.contains(FileUtils.getMimeType(FilenameUtils.getExtension(fileName)))) {
			LOGGER.info(FILE_LABEL + fileName + " - unsupported ContentType " + FileUtils.getMimeType(FilenameUtils.getExtension(fileName)));
			isUnsupported = true;
		}

		return isUnsupported;
	}

	/**
	 * Controlla se è possibile aggiungere il campo firma all'allegato.
	 * 
	 * @param alldaelaborare
	 * @param dAll
	 * @return
	 * @throws IOException
	 */
	private boolean isSignatureFieldCompliance(final FileItem alldaelaborare, final Document dAll, final IAdobeLCHelper adobeh, final boolean isDocEntrata,
			final List<String> idAllegatiDaFirmare) throws IOException {
		// è possibile apporre il campo firma
		boolean isCompliance = false;
		final boolean firmaVisible = idAllegatiDaFirmare != null
				&& idAllegatiDaFirmare.contains(dAll.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY)))
				&& dAll.getProperties().getBooleanValue(pp.getParameterByKey(PropertiesNameEnum.FIRMA_VISIBILE_METAKEY)) != null
				&& dAll.getProperties().getBooleanValue(pp.getParameterByKey(PropertiesNameEnum.FIRMA_VISIBILE_METAKEY));

		// se non è documento in entrata
		isCompliance = !isDocEntrata
				// e se l'allegato è da firmare
				&& idAllegatiDaFirmare != null
				&& idAllegatiDaFirmare.contains(dAll.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.DOCUMENT_TITLE_METAKEY)))
				// e non ha il campo firma
				&& !checkEmptySignFields(alldaelaborare, adobeh)
				// e che l'allegato sia contrassegnato come "da firmare"
				&& dAll.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.DA_FIRMARE_METAKEY)) != null
				&& BooleanFlagEnum.SI.getIntValue() == (dAll.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.DA_FIRMARE_METAKEY))).intValue()
				// e abbia il tag "firmatario"
				&& (adobeh.hasTagFirmatario(alldaelaborare) > 0 || firmaVisible);
		return isCompliance;
	}

	private boolean checkEmptySignFields(final FileItem fi, final IAdobeLCHelper adobeh) throws IOException {
		return fi.getName().endsWith(PDF_EXTENSION) && FileUtils.hasEmptySignFields(fi.getInputStream(), adobeh);
	}

	/**
	 * Se è PDF e non c'è campo firma da inserire né protocollo, l'allegato non è da
	 * elaborare.
	 * 
	 * @param daElaborare
	 * @param allegato
	 * @param apponiCampoFirma
	 * @param protocollo
	 * @param mantieniFormatoOriginale
	 * @param firmato
	 * @param stampigliaturaSegniGrafici
	 * @return
	 */
	private boolean thereIsSomeThingToDoForThisAttachment(final FileItem daElaborare, final Document allegato, final boolean apponiCampoFirma, final String protocollo,
			final boolean mantieniFormatoOriginale, final boolean firmato, final Boolean stampigliaturaSegniGrafici, final boolean timbroUscita) {
		boolean thereIsSomeThingToDoForThisAttachment = true;
		
		boolean copiaConforme = Boolean.TRUE.equals(TrasformerCE.getMetadato(allegato, PropertiesNameEnum.IS_COPIA_CONFORME_METAKEY));
		
		if(daElaborare.getContentType().equals(ContentType.PDF) && ((stampigliaturaSegniGrafici!=null && stampigliaturaSegniGrafici)|| (timbroUscita && allegato.getProperties().getInteger32Value(pp.getParameterByKey(
				PropertiesNameEnum.FORMATO_ALLEGATO_ID_METAKEY)).equals(FormatoAllegatoEnum.FIRMATO_DIGITALMENTE.getId())))) {
			LOGGER.info("thereIsSomeThingToDoForThisAttachment");
		} else if (((ContentType.PDF.equals(daElaborare.getContentType()) || daElaborare.getName().toLowerCase().endsWith(PDF_EXTENSION))
						&& ((!apponiCampoFirma && StringUtils.isBlank(protocollo)) || copiaConforme))
						|| mantieniFormatoOriginale
						|| firmato) {
			// Se è PDF e non c'è campo firma da inserire né protocollo, oppure se è copia conforme o formato originale o firmato, allora l'allegato non è da elaborare
			thereIsSomeThingToDoForThisAttachment = false;
		} 

		return thereIsSomeThingToDoForThisAttachment;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.ITrasformazionePDFFacadeSRV#convertiInPDFConStampigliature(java.lang.Long,
	 *      it.ibm.red.business.dto.FileDTO, java.lang.Integer, java.lang.String,
	 *      java.util.List, boolean, boolean, boolean, boolean, boolean, boolean,
	 *      boolean, boolean, boolean, java.lang.Long, java.lang.Long,
	 *      java.lang.Integer).
	 */
	@Override
	public FileDTO convertiInPDFConStampigliature(Long idAoo, FileDTO inputFile, Integer inNumeroDocumento, String inProtocollo, List<Integer> idFirmatari, 
			boolean entrata, boolean principale, boolean apponiProtocollo, boolean apponiNumeroDocumento, boolean apponiCampoFirma, boolean firmaMultipla,
			boolean applicaAncheSenzaTag, boolean mantieniFormatoOriginale, boolean stampigliaturaSigla, Long idSiglatario, Long idUffCreatore, Integer idTipoDoc) {
		FileDTO pdf = null;
		Connection connection = null;

		try {
			Integer numeroDocumento = null;
			String protocollo = null;

			final Aoo aoo = aooSRV.recuperaAoo(idAoo.intValue());

			if (apponiProtocollo) {
				protocollo = inProtocollo;
			}

			if (apponiNumeroDocumento) {
				numeroDocumento = inNumeroDocumento;
			}

			byte[] content = inputFile.getContent();
			String contentType = inputFile.getMimeType();
			String nomeFile = inputFile.getFileName();
			
			// ### Se NON si tratta di un PDF, si richiama il processo di TRASFORMAZIONE "semplice" (senza stampigliature, campo firma, etc.)
			if (!ContentType.PDF.equalsIgnoreCase(contentType)) {
				IAdobeLCHelper alch = new AdobeLCHelper();
				content = alch.trasformazionePDF(nomeFile, content, false, inputFile.getUuid());
									
				if (content != null) {
					contentType = ContentType.PDF;
					nomeFile = FilenameUtils.getBaseName(nomeFile) + ".pdf";
				}
			}

			// PDF inserito dall'utente oppure ottenuto dal processo di trasformazione
			if (ContentType.PDF.equalsIgnoreCase(contentType)) {
				connection = setupConnection(getDataSource().getConnection(), false);
				boolean firmato = PdfHelper.getSignatureNumber(content) > 0; // Vale sia per documento principale sia per allegato in quanto sempre PDF
				
				// Stampigliatura segni grafici per allegato
				Map<String, byte[]> placeholderSegnoGraficoUtente = null;
				List<StampigliaturaSegnoGraficoDTO> listStampigliaturaSegnoGrafico = new ArrayList<>();
				if (!principale && stampigliaturaSigla && idSiglatario != null && idUffCreatore != null && idTipoDoc != null) {
					placeholderSegnoGraficoUtente = stampigliaturaSegnoGraficoDAO.getPlaceholderSegnoGrafico(idSiglatario, idUffCreatore, idTipoDoc, listStampigliaturaSegnoGrafico, connection);
				}
				
				pdf = gestisciContentPDFPerStampigliature(aoo, content, nomeFile, principale, idFirmatari, protocollo, numeroDocumento, firmato, firmaMultipla, 
						entrata, apponiCampoFirma, applicaAncheSenzaTag, mantieniFormatoOriginale, stampigliaturaSigla, placeholderSegnoGraficoUtente, listStampigliaturaSegnoGrafico);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException("Si è verificato un errore durante la conversione in PDF del content", e);
		} finally {
			closeConnection(connection);
		}

		return pdf;
	}
	
	/**
	 * @param item
	 * @param parametri
	 * @param protocollo
	 * @param numeroDocumento
	 * @param idTipoAssegnazione
	 * @param aoo
	 * @param firmato
	 * @param docEntrata
	 * @param apponiCampoFirma
	 * @param connection
	 * @return
	 */
	private FileDTO gestisciContentPDFPerStampigliaturePrincipale(FileItem item, CodaTrasformazioneParametriDTO parametri, String protocollo, 
			Integer numeroDocumento, Integer idTipoAssegnazione, Aoo aoo, boolean firmato, boolean docEntrata, boolean apponiCampoFirma, Connection connection) {
		return gestisciContentPDFPerStampigliature(true, item, parametri, protocollo, numeroDocumento, idTipoAssegnazione, aoo, firmato, docEntrata, 
				apponiCampoFirma, null, null, null, connection);
	}
	
	
	/**
	 * @param item
	 * @param parametri
	 * @param protocollo
	 * @param idTipoAssegnazione
	 * @param aoo
	 * @param firmato
	 * @param docEntrata
	 * @param apponiCampoFirma
	 * @param mantieniFormatoOriginale
	 * @param stampigliaSegniGrafici
	 * @param placeholderSegnoGraficoUtente
	 * @param connection
	 * @return
	 */
	private FileDTO gestisciContentPDFPerStampigliatureAllegato(FileItem item, CodaTrasformazioneParametriDTO parametri, String protocollo, 
			Integer idTipoAssegnazione, Aoo aoo, boolean firmato, boolean docEntrata, boolean apponiCampoFirma, 
			boolean mantieniFormatoOriginale, boolean stampigliaSegniGrafici, Map<String, byte[]> placeholderSegnoGraficoUtente, Connection connection) {
		return gestisciContentPDFPerStampigliature(false, item, parametri, protocollo, null, idTipoAssegnazione, aoo, firmato, docEntrata, 
				apponiCampoFirma, mantieniFormatoOriginale, stampigliaSegniGrafici, placeholderSegnoGraficoUtente, connection);
	}
	
	
	/**
	 * @param principale
	 * @param item
	 * @param parametri
	 * @param protocollo
	 * @param numeroDocumento
	 * @param idTipoAssegnazione
	 * @param aoo
	 * @param firmato
	 * @param docEntrata
	 * @param apponiCampoFirma
	 * @param mantieniFormatoOriginale
	 * @param stampigliaSegniGrafici
	 * @param placeholderSegnoGraficoUtente
	 * @param connection
	 * @return
	 */
	private FileDTO gestisciContentPDFPerStampigliature(boolean principale, FileItem item, CodaTrasformazioneParametriDTO parametri, String protocollo, 
			Integer numeroDocumento, Integer idTipoAssegnazione, Aoo aoo, boolean firmato, boolean docEntrata, boolean apponiCampoFirma, 
			Boolean mantieniFormatoOriginale, Boolean stampigliaSegniGrafici, Map<String, byte[]> placeholderSegnoGraficoUtente, Connection connection) {
		List<Integer> firmatari = null;
		if (apponiCampoFirma) {
			firmatari = getFirmatariIntegerList(parametri.getFirmatariString(), parametri.getIdNodo(), parametri.getTipoFirma(), connection);
		}
		boolean flagFirmaMultipla = (idTipoAssegnazione != null && TipoAssegnazioneEnum.FIRMA_MULTIPLA.getId() == idTipoAssegnazione);

		// Se il documento è stato firmato almeno una volta: 
		// Il protocollo è già stato stampigliato quindi viene azzerato per evitare di apporlo una seconda volta
		// Il numero documento è stato già stampigliato quindi viene azzerato per evitare di apporlo una seconda volta
		if (aSignSRV.isAlreadySigned(parametri.getIdDocumento())) {
			protocollo = "";
			numeroDocumento = 0;
		}
		return gestisciContentPDFPerStampigliature(aoo, item, principale, firmatari, protocollo, numeroDocumento, firmato, flagFirmaMultipla, 
				docEntrata, apponiCampoFirma, true, mantieniFormatoOriginale, stampigliaSegniGrafici, placeholderSegnoGraficoUtente);
	}
	
	
	/**
	 * @param aoo
	 * @param fileItem
	 * @param principale
	 * @param firmatari
	 * @param protocollo
	 * @param numeroDocumento
	 * @param firmato
	 * @param firmaMultipla
	 * @param docEntrata
	 * @param apponiCampoFirma
	 * @param applicaAncheSenzaTag
	 * @param mantieniFormatoOriginale
	 * @param stampigliaSegniGrafici
	 * @param placeholderSegnoGraficoUtente
	 * @return
	 */
	private FileDTO gestisciContentPDFPerStampigliature(Aoo aoo, FileItem fileItem, boolean principale, List<Integer> firmatari, String protocollo, 
			Integer numeroDocumento, boolean firmato, boolean firmaMultipla, boolean docEntrata, boolean apponiCampoFirma, boolean applicaAncheSenzaTag, 
			Boolean mantieniFormatoOriginale, Boolean stampigliaSegniGrafici, Map<String, byte[]> placeholderSegnoGraficoUtente) {
		return gestisciContentPDFPerStampigliature(aoo, fileItem.get(), fileItem.getName(), principale, firmatari, protocollo, numeroDocumento, 
				firmato, firmaMultipla, docEntrata, apponiCampoFirma, applicaAncheSenzaTag, mantieniFormatoOriginale, stampigliaSegniGrafici, placeholderSegnoGraficoUtente, null);
	}
	

	/**
	 * @param aoo
	 * @param content
	 * @param fileName
	 * @param principale
	 * @param firmatari
	 * @param protocollo
	 * @param numeroDocumento
	 * @param firmato
	 * @param firmaMultipla
	 * @param docEntrata
	 * @param apponiCampoFirma
	 * @param applicaAncheSenzaTag
	 * @param mantieniFormatoOriginale
	 * @param stampigliaSegniGrafici
	 * @param placeholderSegnoGraficoUtente
	 * @return
	 */
	private FileDTO gestisciContentPDFPerStampigliature(Aoo aoo, byte[] content, String fileName, boolean principale, List<Integer> firmatari, 
			String protocollo, Integer numeroDocumento, boolean firmato, boolean firmaMultipla, boolean docEntrata, boolean apponiCampoFirma, boolean applicaAncheSenzaTag, 
			Boolean mantieniFormatoOriginale, Boolean stampigliaSegniGrafici, Map<String, byte[]> placeholderSegnoGraficoUtente, List<StampigliaturaSegnoGraficoDTO> listSegnoGrafico) {
		FileDTO out = null;
		
		try {
			byte[] docStampigliato = content;
			IAdobeLCHelper alch = null;
			boolean apponiNumeroDocumento = numeroDocumento != null && numeroDocumento > 0;
			boolean apponiProtocollo = StringUtils.isNotBlank(protocollo);

			// ### Apposizione del CAMPO FIRMA
			if (!docEntrata && apponiCampoFirma)  {
				docStampigliato = PdfHelper.inserisciCampiFirma(docStampigliato, firmatari, aoo.getAltezzaFooter(), aoo.getSpaziaturaFirma(), firmaMultipla, 
						applicaAncheSenzaTag);
				LOGGER.info("Inserimento del campo firma sul documento: " + fileName + " eseguita");
			}
			
			if (apponiProtocollo || apponiNumeroDocumento) {
				SignHelper sh = new SignHelper(aoo.getPkHandlerTimbro().getHandler(), aoo.getPkHandlerTimbro().getSecurePin(), aoo.getDisableUseHostOnly());
				
				// ### Apposizione del NUMERO DOCUMENTO
				if (principale && apponiNumeroDocumento) {
					
					// Si appone come timbro se il content è firmato, altrimenti tramite processo Adobe
					// N.B. Anche i timbri sono considerate firme!
					if (!firmato) {
						alch = new AdobeLCHelper();
						docStampigliato = alch.predisposizioneFirmaDigitale(null, null, numeroDocumento + Constants.EMPTY_STRING, " ", null, docStampigliato);
						LOGGER.info("Eseguita apposizione del numero documento tramite processo Adobe sul documento principale: " + fileName);
					} else {
						docStampigliato = sh.applicaTimbroNumeroDocumento(aoo.getSignerTimbro(), aoo.getPinTimbro(), docStampigliato, String.valueOf(numeroDocumento), 
								aoo.isConfPDFAPerHandler(), aoo.getDescrizione());
						LOGGER.info("Eseguita apposizione del numero documento tramite timbro PK sul documento principale: " + fileName);
					}
					
				}
				
				// ### Apposizione del PROTOCOLLO
				// N.B. Viene stampigliato sempre come timbro, quindi è considerato come una firma
				if (apponiProtocollo) {
					
					// Documento principale
					if (principale) {
						if (docEntrata) {
							docStampigliato = sh.applicaTimbroProtocolloPrincipaleEntrata(aoo.getSignerTimbro(), aoo.getPinTimbro(), docStampigliato, protocollo, aoo.isConfPDFAPerHandler());
						} else {
							docStampigliato = sh.applicaTimbroProtocolloPrincipaleUscita(aoo.getSignerTimbro(), aoo.getPinTimbro(), docStampigliato, protocollo, aoo.isConfPDFAPerHandler());
						}
					// Allegati
					} else {
						if (docEntrata) {
							docStampigliato = sh.applicaTimbroProtocolloAllegatoEntrata(aoo.getSignerTimbro(), aoo.getPinTimbro(), docStampigliato, protocollo, aoo.isConfPDFAPerHandler());
						} else {
							docStampigliato = sh.applicaTimbroProtocolloAllegatoUscita(aoo.getSignerTimbro(), aoo.getPinTimbro(), docStampigliato, protocollo, aoo.isConfPDFAPerHandler());
						}
					}
					
					LOGGER.info("Eseguita apposizione del timbro di protocollo in " + (docEntrata ? "entrata" : "uscita") + " tramite timbro PK sul" 
							+ (principale ? " documento principale:" : "l'allegato:") + fileName);
				}
			}

			out = new FileDTO(fileName, docStampigliato, ContentType.PDF);
			
			// Eventuale creazione di una seconda versione del documento con apposizione aggiuntive, da memorizzare su Filenet come annotation insieme alla versione appena creata
			if (!firmato && !firmaMultipla && !apponiProtocollo) {

				// ### Principale
				if (principale) {
					
					// Se il principale NON è firmato (timbro di protocollo compreso) e l'iter NON è di firma multipla, si crea un secondo documento uguale a quello appena creato 
					// ma con la postilla stampigliata, che andrà inserito come ANNOTATION del documento che sarà caricato su FileNet
					// NB: Il protocollo deve essere uno spazio vuoto per fare in modo che venga accettato da Adobe come protocollo ma allo stesso tempo non modifichi il documento
					byte[] principaleStampigliatoConPostilla = docStampigliato;
					if(aoo.getPostillaAttiva()!=null && aoo.getPostillaAttiva()==1) {
						String testoPostilla = getTestoPostilla(aoo.getIdAoo());
						if(testoPostilla == null || StringUtils.isEmpty(testoPostilla)) {
							testoPostilla = Firma.TESTO_POSTILLA_STANDARD;
						}
						boolean postillaPag1 = aoo.getPostillaSoloPag1()!=null && aoo.getPostillaSoloPag1()==1;
						
					principaleStampigliatoConPostilla = PdfHelper.inserisciPostilla(docStampigliato, testoPostilla, aoo.isConfPDFAPerHandler(),postillaPag1);
						LOGGER.info("Eseguita apposizione della postilla tramite Itext sul documento principale: " + fileName);
					}
					out.setAnnotation(principaleStampigliatoConPostilla);
				
				// ### Allegato
				} else {
					
					// Se l'iter NON è di firma multipla e un allegato (1) NON è firmato (timbro di protocollo compreso), (2) NON è flaggato per "mantieni formato originale" 
					// e (3) è flaggato per la stampigliatura dei segni grafici, si crea un secondo documento uguale a quello appena creato ma con i segni grafici,
					// che andrà inserito come ANNOTATION del documento che sarà caricato su FileNet
					if (Boolean.TRUE.equals(stampigliaSegniGrafici) && !Boolean.TRUE.equals(mantieniFormatoOriginale) && !MapUtils.isEmpty(placeholderSegnoGraficoUtente)) {
						byte[] allegatoStampigliatoConSegniGrafici = PdfHelper.inserisciCampoSigla(docStampigliato, placeholderSegnoGraficoUtente, true, listSegnoGrafico);
						out.setAnnotation(allegatoStampigliatoConSegniGrafici);
						LOGGER.info("Eseguita apposizione dei segni grafici sull'allegato: " + fileName);
					}
					
				}

			}
		} catch (Exception e) {
			LOGGER.error("Errore durante il processo di stampigliatura del content " + fileName + ": " + e.getMessage(), e);
			throw new RedException("Si è verificato un errore durante il processo di stampigliatura del content " + fileName, e);
		}

		return out;
	}

	/**
	 * Valorizza a 1 il flagRenderizzato che consente la visualizzazione del
	 * documento sul libro firma.
	 * 
	 * @param utente
	 * @param aooFilenet
	 * @param idDocumento
	 */
	private static void updateFlagRenderizzato(final Utente utente, final AooFilenet aooFilenet, final Integer idDocumento) {
		final FilenetPEHelper peh = new FilenetPEHelper(utente.getUsername(), aooFilenet.getPassword(), aooFilenet.getUri(), aooFilenet.getStanzaJaas(),
				aooFilenet.getConnectionPoint());

		final HashMap<String, Object> metadati = new HashMap<>();
		metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.FLAG_RENDERIZZATO_METAKEY), BooleanFlagEnum.SI.getIntValue());

		peh.updateWorkflow(idDocumento, metadati);
	}
	
	/**
	 * Effettua la trasformazione di un documento da trasformare associata all'AOO definita dall'id
	 * gestendo eventuali errori o comunicazioni a valle della trasformazione.
	 * @param idAoo identificativo dell'AOO
	 */
	public void cleanTransformation(int idAoo) {
		CodaTrasformazioneDTO codaTrasformazione = getItem(idAoo);

		if (codaTrasformazione != null) {
			try {
				//trasforma
				long now = System.currentTimeMillis();
				doTransformation(codaTrasformazione.getParametri());
				long secondElapsed = System.currentTimeMillis() - now;

				// nessun errore, item done
				setItemDone(codaTrasformazione.getIdCoda(), secondElapsed);

				LOGGER.info("Trasformazione del documento " + codaTrasformazione.getIdDocumento() + " eseguita con successo");
				
				if(codaTrasformazione.getParametri().getIdNotificaSped()!=null) {
					codaMailSRV = ApplicationContextProvider.getApplicationContext().getBean(ICodaMailSRV.class);
					codaMailSRV.updateNotifica(codaTrasformazione.getParametri().getIdNotificaSped());
					LOGGER.info("Invio email spedizione iter manuale");
				}
			} catch (Exception e) {
				LOGGER.error("Si è verificato un errore nella trasformazione PDF del documento " + codaTrasformazione.getIdDocumento(), e);
				changeStatusItem(codaTrasformazione.getIdCoda(), StatoCodaTrasformazioneEnum.ERRORE, e.getMessage());
			}
		} else {
			LOGGER.info("Nessuna Trasformazione da eseguire per l'aoo " + idAoo);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITrasformazionePDFFacadeSRV#getTestoPostilla(java.lang.Long).
	 */
	@Override
	public String getTestoPostilla(Long idAOO) {

		Connection connection = null;
		String returnString = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			returnString = utenteDAO.getTestoPostilla(idAOO, connection);

		} catch (Exception e) {
			LOGGER.error("Errore generico in fase di gestione di retrieve della postilla",e );
		} finally {
			closeConnection(connection);
		}
		return returnString;
	}

}