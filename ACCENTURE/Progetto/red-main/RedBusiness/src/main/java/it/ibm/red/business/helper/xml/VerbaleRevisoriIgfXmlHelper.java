package it.ibm.red.business.helper.xml;

import java.util.List;

import it.ibm.red.business.exception.RedException;

/**
 * Helper xml Verbale Revisori Igf.
 */
public class VerbaleRevisoriIgfXmlHelper extends AbstractXmlHelper {

	/**
	 * La costante serial Version UID.
	 */
	private static final long serialVersionUID = 5729001260655036604L;
	
	/**
	 * Messaggio di errore legato all'invocazione di <code>setXmlDocument</code>.
	 */
	private static final String SET_XML_DOCUMENT_NOT_INVOKED = "Please invoke setXmlDocument before!";
	
	/**
	 * Tag oggetto mail.
	 */
	private static final String OGGETTO_MAIL = "oggettoMail";
	
	/**
	 * Tag casella pec ente.
	 */
	private static final String CASELLA_PEC_ENTE = "CasellaPECEnte";
	
	/**
	 * Tag fascicolo FEPA.
	 */
	private static final String ID_FASCICOLO_FEPA = "IdFascicoloFepa";
	
	/**
	 * Tag ufficio assegnatario.
	 */
	private static final String IDENTIFICATIVO_UFFICIO_ASSEGNATARIO = "CodiceUfficioAssegnatario";
	
	/**
	 * Tag lista fepa.
	 */
	private static final String ID_DOCUMENTO_FEPA_LIST = "IdDocumentoFepa";

	/**
	 * Costruttore vuoto.
	 */
	public VerbaleRevisoriIgfXmlHelper() {
		super();
	}

	/**
	 * Restituisce l'id del fascicolo FEPA.
	 * @return idFascicoloFepa
	 */
	public String getIdFascicoloFepa()  {
		if (getXmlDocument() == null) {
			throw new RedException(SET_XML_DOCUMENT_NOT_INVOKED);
		}
		return getStringFromElements(ID_FASCICOLO_FEPA); 
	}

	/**
	 * Restituisce l'oggetto della mail.
	 * @return oggettoMail
	 */
	public String getOggettoMail() {
		if (getXmlDocument() == null) {
			throw new RedException(SET_XML_DOCUMENT_NOT_INVOKED);
		}
		return getStringFromElements(OGGETTO_MAIL); 
	}

	/**
	 * Restituisce la cassella PEC ente.
	 * @return casellaPecEnte
	 */
	public String getCasellaPecEnte() {
		if (getXmlDocument() == null) {
			throw new RedException(SET_XML_DOCUMENT_NOT_INVOKED);
		}
		return getStringFromElements(CASELLA_PEC_ENTE); 
	}

	/**
	 * Restituisce il codice ufficio dell'assegnatario.
	 * @return codiceUfficioAssgnatario
	 */
	public Integer getCodiceUfficioAssegnatario() {
		if (getXmlDocument() == null) {
			throw new RedException(SET_XML_DOCUMENT_NOT_INVOKED);
		}
		return Integer.parseInt(getStringFromElements(IDENTIFICATIVO_UFFICIO_ASSEGNATARIO)); 
	}

	/**
	 * Restituisce una lista di id documento.
	 * @return lista di idDocumento
	 */
	public List<String> getIdDocumenti() {
		if (getXmlDocument() == null) {
			throw new RedException(SET_XML_DOCUMENT_NOT_INVOKED);
		}
		return getStringListFromElementsList(ID_DOCUMENTO_FEPA_LIST);
	}
}
