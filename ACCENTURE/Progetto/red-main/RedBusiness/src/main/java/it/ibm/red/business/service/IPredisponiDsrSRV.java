package it.ibm.red.business.service;

import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.service.facade.IPredisponiDsrFacadeSRV;

/**
 * Interfaccia del servizio di predisposizione DSR.
 */
public interface IPredisponiDsrSRV extends IPredisponiDsrFacadeSRV {

	/**
	 * Per l'inizializzazione del detail vedere predisponiDichiarazione
	 * @param utente
	 * @param detail
	 * @return
	 */
	EsitoSalvaDocumentoDTO predisponiDichiarazione(UtenteDTO utente, DetailDocumentRedDTO detail);
	
	
	/**
	 * Costruisce il detail del documento protocollato unendo le informazioni provenienti dal documento protocollato con le security costanti per fepa. 
	 * 
	 * Vale solo per il dettaglio che è stato appena protocollato.
	 * 
	 * La predisposizione ha un dettalgio differente.
	 * 
	 * @param documentTitle
	 * @param wobNumber
	 * @param utente
	 * @return
	 */
	DetailDocumentRedDTO retrieveDetailProtocollato(String documentTitle, String wobNumber, UtenteDTO utente);

}
