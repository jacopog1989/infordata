package it.ibm.red.business.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * The Class ProtocolliPregressiDTO.
 *
 * @author VINGENITO
 */
public class ProtocolliPregressiDTO extends AbstractDTO implements Serializable {

	private static final long serialVersionUID = 6715083489880204442L;

	/**
	 * Codice aoo
	 */
	private String codiceAoo;

	/**
	 * Codice registro
	 */
	private String codRegistro;

	/**
	 * Nome protocollatore
	 */
	private String nomeProtocollatore;

	/**
	 * Cognome protocollatore
	 */
	private String cognomeProtocollatore;

	/**
	 * Descrizione protocollatore
	 */
	private String descProtocollatore;

	/**
	 * Campo titolario
	 */
	private String campoTitolario;

	/**
	 * identificativo protocollo
	 */
	private Integer idProtocollo;

	/**
	 * Anno protocollo
	 */
	private String annoProtocollo;

	/**
	 * Descrizione oggetto
	 */
	private String descOggetto;

	/**
	 * Descrizione ufficio
	 */
	private String descUfficio;

	/**
	 * Data protocollo
	 */
	private Date dataProtocollo;

	/**
	 * registro
	 */
	private Long registro;

	/**
	 * Tipo documento
	 */
	private String tipoDocumento;

	// USATI PER I DOWNLOAD DEI DOC
	/**
	 * Identificativo documento per download
	 */
	private String originalDocId;

	/**
	 * Lista allegati
	 */
	private List<AllegatoNsdDTO> listaAllegati;

	/**
	 * Num allegati
	 */
	private String numAllegati;

	/**
	 * Descrizione nome file
	 */
	private String descFileName;

	/**
	 * Mittente
	 */
	private String mittente;

	/**
	 * Destinatario
	 */
	private String destinatario;

	/**
	 * Is annullato
	 */
	private boolean flagAnnullato;

	/**
	 * Flag chiuso
	 */
	private boolean flagChiuso;

	/**
	 * Modalita
	 */
	private String modalita;

	/**
	 * @return the codice aoo
	 */
	public String getCodiceAoo() {
		return codiceAoo;
	}

	/**
	 * @param codiceAoo the new codice aoo
	 */
	public void setCodiceAoo(final String codiceAoo) {
		this.codiceAoo = codiceAoo;
	}

	/**
	 * @return the cod registro
	 */
	public String getCodRegistro() {
		return codRegistro;
	}

	/**
	 * @param codRegistro the new cod registro
	 */
	public void setCodRegistro(final String codRegistro) {
		this.codRegistro = codRegistro;
	}

	/**
	 * @return the nome protocollatore
	 */
	public String getNomeProtocollatore() {
		return nomeProtocollatore;
	}

	/**
	 * @param nomeProtocollatore the new nome protocollatore
	 */
	public void setNomeProtocollatore(final String nomeProtocollatore) {
		this.nomeProtocollatore = nomeProtocollatore;
	}

	/**
	 * @return the cognome protocollatore
	 */
	public String getCognomeProtocollatore() {
		return cognomeProtocollatore;
	}

	/**
	 * @param cognomeProtocollatore the new cognome protocollatore
	 */
	public void setCognomeProtocollatore(final String cognomeProtocollatore) {
		this.cognomeProtocollatore = cognomeProtocollatore;
	}

	/**
	 * @return the desc protocollatore
	 */
	public String getDescProtocollatore() {
		return descProtocollatore;
	}

	/**
	 * @param descProtocollatore the new desc protocollatore
	 */
	public void setDescProtocollatore(final String descProtocollatore) {
		this.descProtocollatore = descProtocollatore;
	}

	/**
	 * @return the campo titolario
	 */
	public String getCampoTitolario() {
		return campoTitolario;
	}

	/**
	 * @param campoTitolario the new campo titolario
	 */
	public void setCampoTitolario(final String campoTitolario) {
		this.campoTitolario = campoTitolario;
	}

	/**
	 * @return the id protocollo
	 */
	public Integer getIdProtocollo() {
		return idProtocollo;
	}

	/**
	 * @param idProtocollo the new id protocollo
	 */
	public void setIdProtocollo(final Integer idProtocollo) {
		this.idProtocollo = idProtocollo;
	}

	/**
	 * @return the anno protocollo
	 */
	public String getAnnoProtocollo() {
		return annoProtocollo;
	}

	/**
	 * @param annoProtocollo the new anno protocollo
	 */
	public void setAnnoProtocollo(final String annoProtocollo) {
		this.annoProtocollo = annoProtocollo;
	}

	/**
	 * @return the desc oggetto
	 */
	public String getDescOggetto() {
		return descOggetto;
	}

	/**
	 * @param descOggetto the new desc oggetto
	 */
	public void setDescOggetto(final String descOggetto) {
		this.descOggetto = descOggetto;
	}

	/**
	 * @return the desc ufficio
	 */
	public String getDescUfficio() {
		return descUfficio;
	}

	/**
	 * @param descUfficio the new desc ufficio
	 */
	public void setDescUfficio(final String descUfficio) {
		this.descUfficio = descUfficio;
	}

	/**
	 * @return the data protocollo
	 */
	public Date getDataProtocollo() {
		return dataProtocollo;
	}

	/**
	 * @param dataProtocollo the new data protocollo
	 */
	public void setDataProtocollo(final Date dataProtocollo) {
		this.dataProtocollo = dataProtocollo;
	}

	/**
	 * @return the registro
	 */
	public Long getRegistro() {
		return registro;
	}

	/**
	 * @param registro the new registro
	 */
	public void setRegistro(final Long registro) {
		this.registro = registro;
	}

	/**
	 * @return the tipo documento
	 */
	public String getTipoDocumento() {
		return tipoDocumento;
	}

	/**
	 * @param tipoDocumento the new tipo documento
	 */
	public void setTipoDocumento(final String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	/**
	 * @return the original doc id
	 */
	public String getOriginalDocId() {
		return originalDocId;
	}

	/**
	 * @param originalDocId the new original doc id
	 */
	public void setOriginalDocId(final String originalDocId) {
		this.originalDocId = originalDocId;
	}

	/**
	 * @return the lista allegati
	 */
	public List<AllegatoNsdDTO> getListaAllegati() {
		return listaAllegati;
	}

	/**
	 * @param listaAllegati the new lista allegati
	 */
	public void setListaAllegati(final List<AllegatoNsdDTO> listaAllegati) {
		this.listaAllegati = listaAllegati;
	}

	/**
	 * @return the num allegati
	 */
	public String getNumAllegati() {
		return numAllegati;
	}

	/**
	 * @param numAllegati the new num allegati
	 */
	public void setNumAllegati(final String numAllegati) {
		this.numAllegati = numAllegati;
	}

	/**
	 * @return the desc file name
	 */
	public String getDescFileName() {
		return descFileName;
	}

	/**
	 * @param descFileName the new desc file name
	 */
	public void setDescFileName(final String descFileName) {
		this.descFileName = descFileName;
	}

	/**
	 * @return the mittente
	 */
	public String getMittente() {
		return mittente;
	}

	/**
	 * @param mittente the new mittente
	 */
	public void setMittente(final String mittente) {
		this.mittente = mittente;
	}

	/**
	 * @return the destinatario
	 */
	public String getDestinatario() {
		return destinatario;
	}

	/**
	 * @param destinatario the new destinatario
	 */
	public void setDestinatario(final String destinatario) {
		this.destinatario = destinatario;
	}

	/**
	 * @return true, if is flag annullato
	 */
	public boolean isFlagAnnullato() {
		return flagAnnullato;
	}

	/**
	 * @param flagAnnullato the new flag annullato
	 */
	public void setFlagAnnullato(final boolean flagAnnullato) {
		this.flagAnnullato = flagAnnullato;
	}

	/**
	 * @return true, if is flag chiuso
	 */
	public boolean isFlagChiuso() {
		return flagChiuso;
	}

	/**
	 * @param flagChiuso the new flag chiuso
	 */
	public void setFlagChiuso(final boolean flagChiuso) {
		this.flagChiuso = flagChiuso;
	}

	/**
	 * @return the modalita
	 */
	public String getModalita() {
		return modalita;
	}

	/**
	 * @param modalita the new modalita
	 */
	public void setModalita(final String modalita) {
		this.modalita = modalita;
	}

	/**
	 * @return the data table key
	 */
	public String getDataTableKey() {
		return idProtocollo + " - " + annoProtocollo;
	}

}
