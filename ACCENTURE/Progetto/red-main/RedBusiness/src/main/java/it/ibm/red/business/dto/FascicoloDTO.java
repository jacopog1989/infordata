package it.ibm.red.business.dto;

import java.util.Collection;
import java.util.Date;

/**
 * The Class FascicoloDTO.
 *
 * @author CPIERASC
 * 
 *         DTO per modellare fascicolo.
 */
public class FascicoloDTO extends AbstractDTO implements Comparable<FascicoloDTO> {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 3009099894672496242L;

	/**
	 * Data creazione.
	 */
	private Date dataCreazione;

	/**
	 * Descrizione.
	 */
	private String descrizione;

	/**
	 * Indice classificazione.
	 */
	private String indiceClassificazione;

	/**
	 * Descrizione titolario.
	 */

	private String descrizioneTitolario;

	/**
	 * Stato.
	 */
	private String stato;

	/**
	 * Identificativo fascicolo.
	 */
	private String idFascicolo;

	/**
	 * Guid.
	 */
	private String guid;

	/**
	 * Su FileNet -> IdFascicoloFEPA.
	 */
	private String idFascicoloFEPA;


	/**
	 * Faldoni.
	 */
	private Collection<FaldoneDTO> faldoni;

	/**
	 * Classe docuentale.
	 */
	private String classeDocumentale;

	/**
	 * Nome del documento.
	 */
	private String nomeDocumento;

	/**
	 * Oggetto.
	 */
	private String oggetto;

	/**
	 * Identificativo aoo.
	 */
	private Integer idAOO;

	/**
	 * Flag faldonato.
	 */
	private Boolean faldonato;

	/**
	 * Data terminazione.
	 */
	private Date dataTerminazione;

	/**
	 * Document title.
	 */
	private String documentTitle;

	/**
	 * Anno.
	 */
	private String anno;
	
	/**
	 * Descrizione documento provenienza.
	 */
	private String descrizioneDocumentoProvenienza;
	
	/**
	 * Tipologia fascicolo FEPA.
	 */
	private String tipoFascicoloFEPA;
	
	/**
	 * Prefisso utilizzato nelle maschere del dettaglio.
	 * Formato "guid_anno_"
	 */
	private String prefix;
	

	/**
	 * Costruttore.
	 */
	public FascicoloDTO() {
		super();
	}

	/**
	 * Costruttore.
	 * 
	 * @param inNomeFascicolo
	 * @param inDescrizione
	 * @param inIndiceClassificazione
	 * @param inDataCreazione
	 * @param inStato
	 * @param guid
	 * @param idFascicoloFEPA
	 * @param security
	 */
	public FascicoloDTO(final String inIdFascicolo, final String inDescrizione, final String inIndiceClassificazione,
			final Date inDataCreazione, final String inStato, final String guid, final String idFascicoloFEPA) {
		super();
		this.descrizione = inDescrizione;
		this.indiceClassificazione = inIndiceClassificazione;
		this.dataCreazione = inDataCreazione;
		this.stato = inStato;
		this.idFascicolo = inIdFascicolo;
		this.guid = guid;
		this.idFascicoloFEPA = idFascicoloFEPA;
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param inNomeFascicolo
	 * @param inDescrizione
	 * @param inIndiceClassificazione
	 * @param inDescrizioneTitolario
	 * @param inPrefix
	 */
	public FascicoloDTO(final String inIdFascicolo, final String inDescrizione, final String inIndiceClassificazione,
			final String inDescrizioneTitolario, final String inPrefix) {
		this.idFascicolo = inIdFascicolo;
		this.descrizione = inDescrizione;
		this.indiceClassificazione = inIndiceClassificazione;
		this.descrizioneTitolario = inDescrizioneTitolario;
		this.prefix = inPrefix;
	}

	/**
	 * @param inIdFascicolo
	 * @param inDescrizione
	 * @param inIndiceClassificazione
	 * @param inDataCreazione
	 * @param inStato
	 * @param guid
	 * @param idFascicoloFEPA
	 * @param classeDocumentale
	 * @param nomeDocumento
	 * @param oggetto
	 * @param inIdAOO
	 * @param inFaldone
	 * @param inDataTerminazione
	 * @param inDocumentTitle
	 * @param descrizioneTitolario
	 * @param tipoFascicoloFEPA
	 */
	public FascicoloDTO(final String inIdFascicolo, final String inDescrizione, final String inIndiceClassificazione,
			final Date inDataCreazione, final String inStato, final String guid, final String idFascicoloFEPA,
			final String classeDocumentale, final String nomeDocumento, final String oggetto, final Integer inIdAOO,
			final Boolean inFaldone, final Date inDataTerminazione, final String inDocumentTitle, 
			final String descrizioneTitolario, final String tipoFascicoloFEPA) {
		
		this(inIdFascicolo, inDescrizione, inIndiceClassificazione, inDataCreazione, inStato, guid, idFascicoloFEPA);

		this.classeDocumentale = classeDocumentale;
		this.nomeDocumento = nomeDocumento;
		this.oggetto = oggetto;

		this.idAOO = inIdAOO;
		this.faldonato = inFaldone;
		this.dataTerminazione = inDataTerminazione;
		this.documentTitle = inDocumentTitle;

		final String[] oggettoSplit = oggetto.split("_");
		this.anno = oggettoSplit[1];
		this.descrizione = oggetto.replace((oggettoSplit[0] + "_" + oggettoSplit[1] + "_"), "");
		
		this.descrizioneTitolario = descrizioneTitolario;
		
		this.tipoFascicoloFEPA = tipoFascicoloFEPA;
	}

	/**
	 * Getter.
	 * 
	 * @return data creazione
	 */
	public final Date getDataCreazione() {
		return dataCreazione;
	}

	/**
	 * Getter.
	 * 
	 * @return descrizione
	 */
	public final String getDescrizione() {
		return descrizione;
	}

	/**
	 * Getter.
	 * 
	 * @return indice classificazione
	 */
	public final String getIndiceClassificazione() {
		return indiceClassificazione;
	}

	/**
	 * Getter.
	 * 
	 * @return stato
	 */
	public final String getStato() {
		return stato;
	}

	/**
	 * Getter.
	 * 
	 * @return identificativo fascicolo
	 */
	public final String getIdFascicolo() {
		return idFascicolo;
	}

	/**
	 * Getter.
	 * 
	 * @return guid
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * Getter.
	 * 
	 * @return idFascicoloFEPA
	 */
	public String getIdFascicoloFEPA() {
		return idFascicoloFEPA;
	}

	/**
	 * Getter.
	 * 
	 * @return faldoni
	 */
	public Collection<FaldoneDTO> getFaldoni() {
		return faldoni;
	}

	/**
	 * Setter.
	 * 
	 * @param inDataCreazione
	 *            data creazione
	 */
	public final void setDataCreazione(final Date inDataCreazione) {
		this.dataCreazione = inDataCreazione;
	}

	/**
	 * Setter.
	 * 
	 * @param inDescrizione
	 *            descrizione
	 */
	public final void setDescrizione(final String inDescrizione) {
		this.descrizione = inDescrizione;
	}

	/**
	 * Setter.
	 * 
	 * @param inIndiceClassificazione
	 *            indice classificazione
	 */
	public final void setIndiceClassificazione(final String inIndiceClassificazione) {
		this.indiceClassificazione = inIndiceClassificazione;
	}

	/**
	 * @return
	 */
	public String getDescrizioneTitolario() {
		return descrizioneTitolario;
	}

	/**
	 * @param descrizioneTitolario
	 */
	public void setDescrizioneTitolario(final String descrizioneTitolario) {
		this.descrizioneTitolario = descrizioneTitolario;
	}

	/**
	 * Setter.
	 * 
	 * @param inIdFascicolo
	 *            identificativo fascicolo
	 */
	public final void setIdFascicolo(final String inIdFascicolo) {
		this.idFascicolo = inIdFascicolo;
	}

	/**
	 * Setter.
	 * 
	 * @param faldoni
	 */
	public void setFaldoni(final Collection<FaldoneDTO> faldoni) {
		this.faldoni = faldoni;
	}

	/**
	 * Restituisce la classe documentale.
	 * @return classe documentale
	 */
	public String getClasseDocumentale() {
		return classeDocumentale;
	}

	/**
	 * Imposta la classe documentale.
	 * @param classeDocumentale
	 */
	public void setClasseDocumentale(final String classeDocumentale) {
		this.classeDocumentale = classeDocumentale;
	}

	/**
	 * Restituisce il nome documento.
	 * @return nome documento
	 */
	public String getNomeDocumento() {
		return nomeDocumento;
	}

	/**
	 * Imposta il nome documento.
	 * @param nomeDocumento
	 */
	public void setNomeDocumento(final String nomeDocumento) {
		this.nomeDocumento = nomeDocumento;
	}

	/**
	 * Restituisce l'oggetto.
	 * @return oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}

	/**
	 * Imposta l'oggetto.
	 * @param oggetto
	 */
	public void setOggetto(final String oggetto) {
		this.oggetto = oggetto;
	}

	/**
	 * Restituisce l'id dell'Area Organizzativa Omogenea.
	 * @return id AOO
	 */
	public Integer getIdAOO() {
		return idAOO;
	}

	/**
	 * Restituisce true se faldonato, false altrimenti.
	 * @return true se faldonato, false altrimenti
	 */
	public Boolean getFaldonato() {
		return faldonato;
	}

	/**
	 * @param faldonato
	 *            the faldonato to set
	 */
	public void setFaldonato(final Boolean faldonato) {
		this.faldonato = faldonato;
	}

	/**
	 * Restituisce la data di terminazione.
	 * @return data trasmissione
	 */
	public Date getDataTerminazione() {
		return dataTerminazione;
	}

	/**
	 * Restituisce il documentTitle.
	 * @return documentTitle
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Restituisce l'anno.
	 * @return anno
	 */
	public String getAnno() {
		return anno;
	}

	/**
	 * Imposta l'anno.
	 * @param stato
	 */
	public void setStato(final String stato) {
		this.stato = stato;
	}

	/**
	 * Imposta il guid.
	 * @param guid
	 */
	public void setGuid(final String guid) {
		this.guid = guid;
	}

	/**
	 * Imposta l'id del fascicolo FEPA.
	 * @param idFascicoloFEPA
	 */
	public void setIdFascicoloFEPA(final String idFascicoloFEPA) {
		this.idFascicoloFEPA = idFascicoloFEPA;
	}

	/**
	 * Imposta l'id dell'Area organizzativa omogenea.
	 * @param idAOO
	 */
	public void setIdAOO(final Integer idAOO) {
		this.idAOO = idAOO;
	}

	/**
	 * Imposta la data terminazione.
	 * @param dataTerminazione
	 */
	public void setDataTerminazione(final Date dataTerminazione) {
		this.dataTerminazione = dataTerminazione;
	}

	/**
	 * Imposta il documentTitle.
	 * @param documentTitle
	 */
	public void setDocumentTitle(final String documentTitle) {
		this.documentTitle = documentTitle;
	}

	/**
	 * Imposta l'anno.
	 * @param anno
	 */
	public void setAnno(final String anno) {
		this.anno = anno;
	}

	/**
	 * Restituisce la descrizione del documento di provenienza.
	 * @return descrizione documento provenienza.
	 */
	public String getDescrizioneDocumentoProvenienza() {
		return descrizioneDocumentoProvenienza;
	}

	/**
	 * Imposta la descrizione del documento di provenienza.
	 * @param descrizioneDocumentoProvenienza
	 */
	public void setDescrizioneDocumentoProvenienza(final String descrizioneDocumentoProvenienza) {
		this.descrizioneDocumentoProvenienza = descrizioneDocumentoProvenienza;
	}

	/**
	 * Restituisce il tipo fascicolo FEPA.
	 * @return tipo fascicolo
	 */
	public String getTipoFascicoloFEPA() {
		return tipoFascicoloFEPA;
	}

	/**
	 * Restituisce il prefisso.
	 * @return prefix
	 */
	public String getPrefix() {
		return prefix;
	}

	/**
	 * Imposta il prefisso.
	 * @param prefix
	 */
	public void setPrefix(final String prefix) {
		this.prefix = prefix;
	}

	/**
	 * @see java.lang.Object#hashCode().
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idFascicolo == null) ? 0 : idFascicolo.hashCode());
		return result;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object).
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final FascicoloDTO other = (FascicoloDTO) obj;
		if (idFascicolo == null) {
			if (other.idFascicolo != null)
				return false;
		} else if (!idFascicolo.equals(other.idFascicolo))
			return false;
		return true;
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object).
	 */
	@Override
	public int compareTo(final FascicoloDTO o) {
		return this.dataCreazione.compareTo(o.getDataCreazione());
	}
}