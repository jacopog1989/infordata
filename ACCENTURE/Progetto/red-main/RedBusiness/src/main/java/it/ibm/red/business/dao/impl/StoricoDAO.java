package it.ibm.red.business.dao.impl;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.IStoricoDAO;
import it.ibm.red.business.dao.ITipoEventoDAO;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.filenet.EventLogDTO;
import it.ibm.red.business.dto.filenet.StoricoDTO;
import it.ibm.red.business.enums.StatoDocumentoFuoriLibroFirmaEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.DEventiCustom;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.TipoEvento;
import it.ibm.red.business.persistence.model.Utente;

/**
 * The Class StoricoDAO.
 *
 * @author CPIERASC
 * 
 * 	Dao per la gestione dello storico.
 */
@Repository
public class StoricoDAO extends AbstractDAO implements IStoricoDAO {
	
	private static final String ATTIVITA = "attivita";

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(StoricoDAO.class.getName());

	/**
	 * Dao per la gestione del nodo.
	 */
	@Autowired
	private INodoDAO nodoDAO;
	
	/**
	 * Dao per la gestione dell'utente.
	 */
	@Autowired
	private IUtenteDAO utenteDAO;
	
	/**
	 * Dao per la gestione del tipo utente.
	 */
	@Autowired
	private ITipoEventoDAO tipoEventoDAO;
	

	/**
	 * Gets the storico.
	 *
	 * @param idDocumento the id documento
	 * @param idAoo the id aoo
	 * @param connection the connection
	 * @param filenetConnection the filenet connection
	 * @return the storico
	 */
	@Override
	public final Collection<StoricoDTO> getStorico(final int idDocumento, final Integer idAoo, final Connection connection, final Connection filenetConnection) {
		PreparedStatement ps = null;
		final PreparedStatement psFN = null;
		ResultSet rs = null;
		final ResultSet rsFN = null;
		final Collection<StoricoDTO> storicoCollection = new ArrayList<>();
		try {
			String tmp = "";
			if (idAoo != null && idAoo > 0) {
				tmp = " and dc.IDAOO = " + sanitize(idAoo);
			}
			final String filenetQuerySQL = 
					"SELECT dc.IDDOCUMENTO, dc.IDNODOMITTENTE, dc.IDNODODESTINATARIO, dc.IDUTENTEMITTENTE, "
					+ "dc.IDUTENTEDESTINATARIO, dc.TIPOEVENTO, dc.TIMESTAMPOPERAZIONE, dc.MOTIVAZIONEASSEGNAZIONE as motivoassegnazione, dc.ATTIVITA, "
					+ "dc.IDWORKFLOW, dc.IDWORKFLOW_PADRE "
					+ " FROM D_EVENTI_CUSTOM dc "
					+ " WHERE dc.IDNODOMITTENTE IS NOT NULL AND dc.IDNODODESTINATARIO IS NOT NULL AND dc.IDUTENTEMITTENTE IS NOT NULL "
					+ " AND dc.IDUTENTEDESTINATARIO IS NOT NULL AND dc.TIPOEVENTO IS NOT NULL "
					+ " AND dc.IDDOCUMENTO = " + sanitize(idDocumento) + tmp + " ORDER BY dc.TIMESTAMPOPERAZIONE ASC, dc.ID ASC";
			
			ps = filenetConnection.prepareStatement(filenetQuerySQL);
			
			rs = ps.executeQuery();
			while (rs.next()) {
				
				final Long idNodoMittente = rs.getLong("IDNODOMITTENTE");
				final Long idNodoDestinatario = rs.getLong("IDNODODESTINATARIO");
				final Long idUtenteMittente = rs.getLong("IDUTENTEMITTENTE");
				final Long idUtenteDestinatario = rs.getLong("IDUTENTEDESTINATARIO");
				final Integer tipoEventoId = rs.getInt("TIPOEVENTO");
				
				final boolean mittenteNull = idNodoMittente == null || idUtenteMittente == null;
				final boolean destinatarioNull = idNodoDestinatario == null || idUtenteDestinatario == null;
				
				if (mittenteNull || destinatarioNull || tipoEventoId == null) {
					continue;
				}
				
				final Timestamp dataTimeTS = rs.getTimestamp("TIMESTAMPOPERAZIONE");
				Date dataTime = null;
				if (dataTimeTS != null) {
					dataTime = 	new Date(dataTimeTS.getTime());
				}

				String ufficioAssegnante = "";
				final Nodo nodoAssegnante = nodoDAO.getNodo(idNodoMittente, connection);
				if (nodoAssegnante != null) {
					ufficioAssegnante = nodoAssegnante.getDescrizione();
				}
				
				String ufficioAssegnatario = "";
				final Nodo nodoAssegnatario = nodoDAO.getNodo(idNodoDestinatario, connection);
				if (nodoAssegnatario != null) {
					ufficioAssegnatario = nodoAssegnatario.getDescrizione();
				}
				
				String utenteAssegnanteS = "";
				final Utente utenteAssegnante = utenteDAO.getUtente(idUtenteMittente, connection);
				if (utenteAssegnante != null) {
					final String nomeAssegnante = utenteAssegnante.getNome();
					final String cognomeAssegnante = utenteAssegnante.getCognome();
					utenteAssegnanteS = getDescrizione(nomeAssegnante, cognomeAssegnante);
				}

				String utenteAssegnatarioS = "";
				final Utente utenteAssegnatario = utenteDAO.getUtente(idUtenteDestinatario, connection);
				if (utenteAssegnatario != null) {
					final String nomeAssegnatario = utenteAssegnatario.getNome();
					final String cognomeAssegnatario = utenteAssegnatario.getCognome();
					utenteAssegnatarioS = getDescrizione(nomeAssegnatario, cognomeAssegnatario);
				}
				
				String tipoEventoS = "";
				final TipoEvento tipoEvento = tipoEventoDAO.getById(tipoEventoId, connection);
				if (tipoEvento != null) {
					tipoEventoS = tipoEvento.getDescrizione();
				}
				
				InputStream is = null;
				if (rs.getBlob(ATTIVITA) != null && rs.getBlob(ATTIVITA).length() != 0) {
					is = rs.getBlob(ATTIVITA).getBinaryStream();
				}
					
				final StoricoDTO storico = new StoricoDTO(dataTime, 
													tipoEventoS, 
													tipoEventoId,
													ufficioAssegnante, 
													utenteAssegnanteS, 
													ufficioAssegnatario, 
													utenteAssegnatarioS,
													rs.getString("motivoassegnazione"),
													idNodoMittente,
													idUtenteMittente,
													idNodoDestinatario,
													idUtenteDestinatario,
													rs.getString("idworkflow"),
													rs.getString("idworkflow_padre"));
				
				storico.setAttivita(is);
				storicoCollection.add(storico);
			}
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero dello storico del documento: ", e);
		} finally {
			closeStatement(ps, rs);
			closeStatement(psFN, rsFN);
		}
		return storicoCollection;
	}
	

	/**
	 * @see it.ibm.red.business.dao.IStoricoDAO#getStoricoDocumento(java.lang.Integer,
	 *      java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public List<DEventiCustom> getStoricoDocumento(final Integer inIdDocumento, final Integer inIdAoo, final Connection filenetConnection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final List<DEventiCustom> storicoCollection = new ArrayList<>();
		try {
			String tmp = "";
			if (inIdAoo != null && inIdAoo > 0) {
				tmp = " and dc.IDAOO = " + sanitize(inIdAoo);
			}
			final String filenetQuerySQL = 
					"SELECT dc.ID, dc.IDAOO, dc.IDDOCUMENTO, dc.IDNODOMITTENTE, dc.IDNODODESTINATARIO, "
					+ "dc.IDUTENTEMITTENTE, dc.DATAASSEGNAZIONE, dc.IDTIPOASSEGNAZIONE, dc.ANNO, "
					+ "dc.IDUTENTEDESTINATARIO, dc.TIPOEVENTO, dc.TIMESTAMPOPERAZIONE, dc.MOTIVAZIONEASSEGNAZIONE "
					+ " FROM D_EVENTI_CUSTOM dc "
					+ " WHERE dc.IDNODOMITTENTE IS NOT NULL AND dc.IDNODODESTINATARIO IS NOT NULL AND dc.IDUTENTEMITTENTE IS NOT NULL "
					+ " AND dc.IDUTENTEDESTINATARIO IS NOT NULL AND dc.TIPOEVENTO IS NOT NULL "
					+ " AND dc.IDDOCUMENTO = " + sanitize(inIdDocumento) + tmp + " ORDER BY dc.TIMESTAMPOPERAZIONE ASC, dc.ID ASC";
			
			ps = filenetConnection.prepareStatement(filenetQuerySQL);
			rs = ps.executeQuery();
			while (rs.next()) {
				
				final Integer id = rs.getInt("ID");
				final Integer idAoo = rs.getInt("IDAOO");
				final Integer idDocumento =  rs.getInt("IDDOCUMENTO");
				final Long idNodoMittente = rs.getLong("IDNODOMITTENTE");
				final Long idNodoDestinatario = rs.getLong("IDNODODESTINATARIO");
				final Long idUtenteMittente = rs.getLong("IDUTENTEMITTENTE");
				final Long idUtenteDestinatario = rs.getLong("IDUTENTEDESTINATARIO");
				final Date dataAssegnazione = rs.getDate("DATAASSEGNAZIONE");
				final Integer tipoEventoId = rs.getInt("TIPOEVENTO");
				final String motivazioneAssegnazione = rs.getString("MOTIVAZIONEASSEGNAZIONE");
				final Integer idTipoAssegnazione = rs.getInt("IDTIPOASSEGNAZIONE");
				final Date timestampOperazione = rs.getDate("TIMESTAMPOPERAZIONE");
				final Integer anno = rs.getInt("ANNO");
				
				final boolean mittenteNull = idNodoMittente == null || idUtenteMittente == null;
				final boolean destinatarioNull = idNodoDestinatario == null || idUtenteDestinatario == null;
				
				if (mittenteNull || destinatarioNull || tipoEventoId == null) {
					continue;
				}

				final DEventiCustom evento = new DEventiCustom(id, idAoo, idDocumento,
						idNodoMittente, idUtenteMittente,
						idNodoDestinatario, idUtenteDestinatario,
						dataAssegnazione, tipoEventoId, motivazioneAssegnazione, idTipoAssegnazione, timestampOperazione, anno);
				
			
				storicoCollection.add(evento);
			}
		} catch (final SQLException e) {
			throw new RedException("Errore durante il recupero dello storico del documento: ", e);
		} finally {
			closeStatement(ps, rs);
		}
		return storicoCollection;
	}
	
	/**
	 * Metodo per il recupero del nome dell'assegnatario.
	 * 
	 * @param nomeAssegnante	nome assegnatario
	 * @param cognomeAssegnante	cognome assegnatario
	 * @return					descrizione assegnatario
	 */
	private static String getDescrizione(final String nomeAssegnante, final String cognomeAssegnante) {
		String utenteAssegnanteS;
		utenteAssegnanteS = "";
		if (cognomeAssegnante != null) {
			utenteAssegnanteS += cognomeAssegnante + " ";
		}
		if (nomeAssegnante != null) {
			utenteAssegnanteS += nomeAssegnante;
		}
		return utenteAssegnanteS;
	}

	/**
	 * Gets the stato documento chiuso.
	 *
	 * @param documentTitle the document title
	 * @param idUtente the id utente
	 * @param idUfficio the id ufficio
	 * @param connection the connection
	 * @return the stato documento chiuso
	 */
	@Override
	public final StatoDocumentoFuoriLibroFirmaEnum getStatoDocumentoChiuso(final String documentTitle, final Long idUtente, 
			final Long idUfficio, final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			StatoDocumentoFuoriLibroFirmaEnum output = null;
			final String querySQL = "select IDSTATOLAVORAZIONE from DOCUMENTOUTENTESTATO where IDDOCUMENTO=" + sanitize(Integer.valueOf(documentTitle))
				+ " order by DATA_STATO desc";
			ps = connection.prepareStatement(querySQL);
			
			rs = ps.executeQuery();
			
			if (rs.next()) {
				final Long idStatoLavorazione = rs.getLong("IDSTATOLAVORAZIONE");
				if (StatoDocumentoFuoriLibroFirmaEnum.CHIUSO_ATTI.getLongValue().equals(idStatoLavorazione)) {
					output = StatoDocumentoFuoriLibroFirmaEnum.CHIUSO_ATTI;
				} else if (StatoDocumentoFuoriLibroFirmaEnum.CHIUSO_MOZIONE.getLongValue().equals(idStatoLavorazione)) {
					output = StatoDocumentoFuoriLibroFirmaEnum.CHIUSO_MOZIONE;
				} else if (StatoDocumentoFuoriLibroFirmaEnum.CHIUSO_RISPOSTA.getLongValue().equals(idStatoLavorazione)) {
					output = StatoDocumentoFuoriLibroFirmaEnum.CHIUSO_RISPOSTA;
				}
			}
			
			return output;
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/*
	 * Inizio storico visuale.
	 */
	
	/**
	 * Metodo per la mappatura di un ResultSet nel DTO che rappresenta gli EventLog.
	 * 
	 * @param rs	ResultSet da traformare
	 * @return		DTO
	 */
	private static Collection<EventLogDTO> trasformToEventLogDTO(final ResultSet rs) {
		final Collection<EventLogDTO> output = new ArrayList<>();
		try {
			while (rs.next()) {
				final EventLogDTO eventLog = new EventLogDTO(
					rs.getString("tipoEvento"),
					rs.getTimestamp("dataAssegnazione"),
					rs.getString("idworkflow"),
					rs.getString("tipo"),
					rs.getString("descrizione"),
					rs.getTimestamp("datachiusura"),
					rs.getString("nodoMittente"),
					rs.getString("nodoDestinatario"),
					rs.getString("nomeMitt"),
					rs.getString("cognomeMitt"),
					rs.getString("nomeDest"),
					rs.getString("cognomeDest"),
					rs.getLong("idUtenteMittente"),
					rs.getLong("idUtenteDestinatario"),
					rs.getLong("idNodoMittente"),
					rs.getLong("idNodoDestinatario"),
					rs.getLong("idTipoAssegnazione")
					);
				output.add(eventLog);
			}
			return output;
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeStatement(null, rs);
		}
	}
	
	
	/**
	 * Gets the storico event acc.
	 *
	 * @param filenetConnection the filenet connection
	 * @param documentTitle the document title
	 * @param idAoo the id aoo
	 * @return the storico event acc
	 */
	@Override
	public final Collection<EventLogDTO> getStoricoEventAcc(final Connection filenetConnection, final String documentTitle, final Long idAoo) {
		
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			
			final String filenetQuerySQL = 
					"select * from d_eventi_custom d, ( "
					+ "select d.id,t.tipo,t.descrizione,"
					+ " ( select dataassegnazione from table(f_1(d.iddocumento,d.idaoo,d.idworkflow)) )"
			        + " as dataChiusura, n1.descrizione as nodoMittente, n2.descrizione as nodoDestinatario, u1.nome as nomeMitt, u1.cognome as cognomeMitt, "
			        + " u2.nome as nomeDest, u2.cognome as cognomeDest"
			        + " from d_eventi_custom d "
			        + " join tipoevento t on d.tipoevento = t.idtipoevento "
			        + " left join nodo n1 on n1.idnodo = d.idnodomittente "
			        + " left join nodo n2 on n2.idnodo = d.idnododestinatario "
			        + " left join utente u1 on u1.idutente = d.idutentemittente "
			        + " left join utente u2 on u2.idutente = d.idutentedestinatario "
			        + " where "
			        + " d.iddocumento = " + sanitize(documentTitle) + " and d.idaoo = " + sanitize(idAoo) + " and d.idworkflow_padre is not null and"
			        + " (t.idtipoevento_padre is null or (t.idtipoevento_padre is not null and t.idtipoevento_padre = d.tipoevento)) and"
			        + " d.idworkflow_padre not in ("
			        + " select d2.idworkflow "
			        + " from d_eventi_custom d2 "
			        + " join tipoevento t2 on d2.tipoevento = t2.idtipoevento "
			        + " where d2.iddocumento = d.iddocumento and t2.tipo = 'I') and"
			        + " ((d.id = "
			        + " ( select min(id) "
			        + " from d_eventi_custom d3 "
			        + " join tipoevento t3 on d3.tipoevento = t3.idtipoevento "
			        + " where "
			        + " d3.idworkflow = d.idworkflow and d3.idworkflow_padre = d.idworkflow_padre and t3.tipo = t.tipo and t3.tipo = 'I') and t.tipo = 'I') or"
			        + " (t.tipo is null) or (t.tipo = 'R' and t.idtipoevento_padre is null))"
			        + " union "
			        + " select d.id,t.tipo,t.descrizione,null "
			        + " as dataChiusura,n1.descrizione as nodoMittente,n2.descrizione as nodoDestinatario,u1.nome as nomeMitt,u1.cognome as cognomeMitt,"
			        + " u2.nome as nomeDest,u2.cognome as cognomeDest"
			        + " from d_eventi_custom d "
			        + " join tipoevento t on d.tipoevento = t.idtipoevento "
			        + " left join nodo n1 on n1.idnodo = d.idnodomittente "
			        + " left join nodo n2 on n2.idnodo = d.idnododestinatario "
			        + " left join utente u1 on u1.idutente = d.idutentemittente "
			        + " left join utente u2 on u2.idutente = d.idutentedestinatario "
			        + " where d.iddocumento = " + sanitize(documentTitle) + " and d.idaoo = " + sanitize(idAoo) + " and d.idworkflow is null "
			        + " ) a"
			        + " where d.id = a.id"
			        + " order by 1 asc";
				
			
			ps = filenetConnection.prepareStatement(filenetQuerySQL);
			rs = ps.executeQuery();		
			return trasformToEventLogDTO(rs);
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}		
	}
	
	/**
	 * Gets the storico event acc dett.
	 *
	 * @param filenetConnection the filenet connection
	 * @param documentTitle the document title
	 * @param idAoo the id aoo
	 * @param idworkflow the idworkflow
	 * @return the storico event acc dett
	 */
	@Override
	public final Collection<EventLogDTO> getStoricoEventAccDett(final Connection filenetConnection, final String documentTitle, final Long idAoo, final String idworkflow) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			final String filenetQuerySQL =
				"WITH recursiveBOM"
			   	+ " (id, idaoo, iddocumento, idnodomittente, idutentemittente, idnododestinatario, idutentedestinatario, dataassegnazione, tipoevento, "
			   	+ " motivazioneassegnazione, idtipoassegnazione, timestampoperazione, anno, idworkflow, idworkflow_padre, tipo, descrizione, dataChiusura, "
			   	+ " nodoMittente, nodoDestinatario, nomeMitt, cognomeMitt, nomeDest, cognomeDest) AS"
			   	+ " (SELECT parent.id,"
			   	+ " parent.idaoo,"
			   	+ " parent.iddocumento,"
			   	+ " parent.idnodomittente,"
			   	+ " parent.idutentemittente,"
			   	+ " parent.idnododestinatario,"
			   	+ " parent.idutentedestinatario,"
			   	+ " parent.dataassegnazione,"
			   	+ " parent.tipoevento,"
			   	+ " parent.motivazioneassegnazione,"
			   	+ " parent.idtipoassegnazione,"
			   	+ " parent.timestampoperazione,"
			   	+ " parent.anno,"
			   	+ " parent.idworkflow,"
			   	+ " parent.idworkflow_padre,"
			   	+ " t.tipo,"
			   	+ " t.descrizione,"
			   	+ " null,"
			   	+ " n1.descrizione,"
			   	+ " n2.descrizione,"
			   	+ " u1.nome,"
			   	+ " u1.cognome,"
			   	+ " u2.nome,"
			   	+ " u2.cognome"
			   	+ " FROM d_eventi_custom parent"
			   	+ " join tipoevento t on parent.tipoevento = t.idtipoevento"
			   	+ " left join nodo n1 on n1.idnodo = parent.idnodomittente"
			   	+ " left join nodo n2 on n2.idnodo = parent.idnododestinatario"
			   	+ " left join utente u1 on u1.idutente = parent.idutentemittente"
			   	+ " left join utente u2 on u2.idutente = parent.idutentedestinatario"
			   	+ " WHERE parent.iddocumento = " + sanitize(documentTitle) + " and parent.idaoo = " + sanitize(idAoo) + " and parent.idworkflow = " + sanitize(idworkflow)
			   	+ " UNION ALL"
			   	+ " SELECT child.id,"
			   	+ " child.idaoo,"
			   	+ " child.iddocumento,"
			   	+ " child.idnodomittente,"
			   	+ " child.idutentemittente,"
			   	+ " child.idnododestinatario,"
			   	+ " child.idutentedestinatario,"
			   	+ " child.dataassegnazione,"
			   	+ " child.tipoevento,"
			   	+ " child.motivazioneassegnazione,"
			   	+ " child.idtipoassegnazione,"
			   	+ " child.timestampoperazione,"
			   	+ " child.anno,"
			   	+ " child.idworkflow,"
			   	+ " child.idworkflow_padre,"
			   	+ " t.tipo,"
			   	+ " t.descrizione,"
			   	+ " null,"
			   	+ " n1.descrizione,"
			   	+ " n2.descrizione,"
			   	+ " u1.nome,"
			   	+ " u1.cognome,"
			   	+ " u2.nome,"
			   	+ " u2.cognome"
			   	+ " FROM recursiveBOM parent, d_eventi_custom child"
			   	+ " join tipoevento t on child.tipoevento = t.idtipoevento left join nodo n1 on n1.idnodo = child.idnodomittente"
			   	+ " left join nodo n2 on n2.idnodo = child.idnododestinatario"
			   	+ " left join utente u1 on u1.idutente = child.idutentemittente"
			   	+ " left join utente u2 on u2.idutente = child.idutentedestinatario"
			   	+ " WHERE child.idworkflow_padre = parent.idworkflow"
			   	+ " )"
			   	+ " SELECT distinct id, idaoo, iddocumento, idnodomittente, idutentemittente, idnododestinatario, idutentedestinatario, dataassegnazione, "
			   	+ " tipoevento, motivazioneassegnazione, idtipoassegnazione, timestampoperazione, anno, idworkflow, idworkflow_padre, tipo, descrizione, "
			   	+ " dataChiusura, nodoMittente, nodoDestinatario, nomeMitt, cognomeMitt, nomeDest, cognomeDest"
			   	+ " FROM recursiveBOM order by id";
			
			ps = filenetConnection.prepareStatement(filenetQuerySQL);
			rs = ps.executeQuery();
			
			return trasformToEventLogDTO(rs);
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.IStoricoDAO#isDocLavoratoDaUfficio(java.lang.Long,
	 *      java.lang.String, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public boolean isDocLavoratoDaUfficio(final Long idAoo, final String documentTitle, final Long idUfficio, final Connection filenetConnection) {
		int risultatoEsecuzione = 0;
		PreparedStatement psFN = null;
		ResultSet rsFN = null;
		try {
			final StringBuilder filenetQuerySQL = new StringBuilder();
			filenetQuerySQL.append("SELECT COUNT(*) NUM_NOTIFICHE ");
			filenetQuerySQL.append(" FROM D_EVENTI_CUSTOM DC ");
			filenetQuerySQL.append(" WHERE DC.IDAOO = " + idAoo);
			filenetQuerySQL.append(" AND DC.IDDOCUMENTO = " + documentTitle);
			filenetQuerySQL.append(" AND (DC.IDNODOMITTENTE = " + idUfficio + " OR DC.IDNODODESTINATARIO = " + idUfficio + ")");

			psFN = filenetConnection.prepareStatement(filenetQuerySQL.toString());
			rsFN = psFN.executeQuery();
			if (rsFN.next()) {
				risultatoEsecuzione = rsFN.getInt("NUM_NOTIFICHE");
			}
		} catch (final SQLException ex) { 
			throw new RedException("Errore durante il recupero del documento lavorato da ufficio: ", ex);
		} finally {
			closeStatement(psFN, rsFN);
		}
		return risultatoEsecuzione > 0;
	}
}