package it.ibm.red.business.dto;

import java.util.Date;

import it.ibm.red.business.service.asign.step.StepEnum;

/**
 * DTO per la gestione degli step degli item di firma asincrona.
 */
public class ASignStepDTO extends AbstractDTO {

	/**
	 * La costante serial Version UID.
	 */
	private static final long serialVersionUID = -6855582232661385513L;
	
	/**
	 * Data inserimento step.
	 */
	private Date dataDa;
	
	/**
	 * Data chiusura step.
	 */
	private Date dataA;
	
	/**
	 * Log associato allo step.
	 */
	private String log;
	
	/**
	 * Definizione dello step.
	 */
	private StepEnum step;
	
	/**
	 * Restituisce la data di inserimento dello step.
	 * @return data inserimento step
	 */
	public Date getDataDa() {
		return dataDa;
	}
	
	/**
	 * Imposta la data di inserimento dello step.
	 * @param dataDa
	 */
	public void setDataDa(Date dataDa) {
		this.dataDa = dataDa;
	}
	
	/**
	 * Restituisce la data di chiusura dello step.
	 * @return data chiusura step
	 */
	public Date getDataA() {
		return dataA;
	}
	
	/**
	 * Impsota la data di chiusura dello step.
	 * @param dataA
	 */
	public void setDataA(Date dataA) {
		this.dataA = dataA;
	}
	
	/**
	 * Restituisce il log dello stato se presente.
	 * @return log associato allo stato
	 */
	public String getLog() {
		return log;
	}
	
	/**
	 * Imposta il log associato allo stato.
	 * @param log
	 */
	public void setLog(String log) {
		this.log = log;
	}
	
	/**
	 * Restituisce lo step.
	 * @return step
	 */
	public StepEnum getStep() {
		return step;
	}
	
	/**
	 * Imposta lo step.
	 * @param step
	 */
	public void setStep(StepEnum step) {
		this.step = step;
	}
}
