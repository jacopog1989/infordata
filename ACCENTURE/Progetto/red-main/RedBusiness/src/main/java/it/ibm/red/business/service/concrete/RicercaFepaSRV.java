package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.DocumentSet;

import it.ibm.red.business.constants.Constants.Ricerca;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IRicercaFepaSRV;
import it.ibm.red.business.service.IRicercaSRV;
import it.ibm.red.business.utils.DateUtils;

/**
 * The Class RicercaFepaSRV.
 *
 * @author m.crescentini
 * 
 *         Servizio per la gestione della ricerca FEPA.
 */
@Service
@Component
public class RicercaFepaSRV extends AbstractService implements IRicercaFepaSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 935560908227139030L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaFepaSRV.class.getName());

	/**
	 * Service.
	 */
	@Autowired
	private IRicercaSRV ricercaSRV;

	/**
	 * Properties.
	 */
	private PropertiesProvider pp;

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.service.facade.IRicercaFepaFacadeSRV#eseguiRicercaFepa(it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO,
	 *      it.ibm.red.business.dto.UtenteDTO, boolean).
	 */
	@Override
	public Collection<MasterDocumentRedDTO> eseguiRicercaFepa(final ParamsRicercaAvanzataDocDTO paramsRicercaFepa, final UtenteDTO utente, final boolean orderbyInQuery) {
		LOGGER.info("eseguiRicercaFepa -> START");
		Collection<MasterDocumentRedDTO> documentiRicerca = new ArrayList<>();
		final HashMap<String, Object> mappaValoriRicercaFilenet = new HashMap<>();
		IFilenetCEHelper fceh = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			final String tipologiaDocumento = recuperaTipologiaDocValues(paramsRicercaFepa.getDescrizioneTipologiaDocumento());

			paramsRicercaFepa.setDescrizioneTipologiaDocumento(tipologiaDocumento);

			// Si convertono i parametri di ricerca in una mappa chiave -> valore
			convertiInParametriRicercaFilenet(mappaValoriRicercaFilenet, paramsRicercaFepa, utente.getIdAoo());

			// ### RICERCA NEL CE -> START
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			final DocumentSet documentiFilenet = fceh.ricercaAvanzataDocumenti(mappaValoriRicercaFilenet, null, utente, orderbyInQuery, null, true);

			if (documentiFilenet != null && !documentiFilenet.isEmpty()) {
				documentiRicerca = ricercaSRV.trasformToGenericDocContext(documentiFilenet, utente, connection);

				if (!orderbyInQuery) {
					final List<MasterDocumentRedDTO> docList = new ArrayList<>(documentiRicerca);
					// implementa Comparable
					Collections.sort(docList, Collections.reverseOrder());
					documentiRicerca = docList;
				}
			}

			// ### RICERCA NEL CE -> END
		} catch (final Exception e) {
			LOGGER.error("eseguiRicercaFepa -> Si è verificato un errore durante l'esecuzione della ricerca.", e);
			throw new RedException("eseguiRicercaFepa -> Si è verificato un errore durante l'esecuzione della ricerca.", e);
		} finally {
			closeConnection(connection);
			popSubject(fceh);
		}

		LOGGER.info("eseguiRicercaFepa -> END. Numero di documenti trovati: " + documentiRicerca.size());
		return documentiRicerca;
	}

	/**
	 * Recupera la descrizione della tipologia del documento.
	 */
	@Override
	public String recuperaTipologiaDocValues(final String i) {
		// prendi DSR
		String tipologia = null;
		switch (i) {
		case "1":
			tipologia = pp.getParameterByKey(PropertiesNameEnum.DSR_CLASSNAME_FN_METAKEY);
			break;
		case "2":
			tipologia = pp.getParameterByKey(PropertiesNameEnum.DECRETO_DIRIGENZIALE_FEPA_CLASSNAME_FN_METAKEY);
			break;
		default:
			break;
		}

		return tipologia;
	}

	private void convertiInParametriRicercaFilenet(final HashMap<String, Object> paramsRicercaFilenet, final ParamsRicercaAvanzataDocDTO paramsRicerca, final Long idAoo) {
		LOGGER.info("convertiInParametriRicercaFilenet -> START");

		paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.ID_AOO_METAKEY), idAoo);

		// Classe documentale
		paramsRicercaFilenet.put(Ricerca.CLASSE_DOCUMENTALE_FWS, paramsRicerca.getDescrizioneTipologiaDocumento());

		// Anno documento
		paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY), paramsRicerca.getAnnoDocumento());

		// ID Documento (Numero Documento)
		if (paramsRicerca.getNumeroDocumento() != null) {
			paramsRicercaFilenet.put(pp.getParameterByKey(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY), paramsRicerca.getNumeroDocumento());
		}

		// Data Creazione Da
		if (paramsRicerca.getDataCreazioneDa() != null) {
			paramsRicercaFilenet.put(Ricerca.DATA_CREAZIONE_DA, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataCreazioneDa(), true));
		}

		// Data Creazione A
		if (paramsRicerca.getDataCreazioneA() != null) {
			paramsRicercaFilenet.put(Ricerca.DATA_CREAZIONE_A, DateUtils.buildFilenetDataForSearch(paramsRicerca.getDataCreazioneA(), false));
		}

		LOGGER.info("convertiInParametriRicercaFilenet -> END");
	}
}