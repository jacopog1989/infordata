package it.ibm.red.business.service.concrete;

import java.io.InputStream;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.core.Document;

import it.ibm.red.business.constants.Constants.Modalita;
import it.ibm.red.business.dao.IUtenteDAO;
import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.AssegnazioneDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedParametriDTO;
import it.ibm.red.business.dto.UfficioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ProvenienzaSalvaDocumentoEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipologiaDestinatarioEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Contatto;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IDocumentoRedSRV;
import it.ibm.red.business.service.IPredisponiDsrSRV;
import it.ibm.red.business.service.IProtocollaDsrSRV;
import it.ibm.red.business.service.ISalvaDocumentoSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * Service che gestisce la predisposizione di un DSR.
 */
@Service
public class PredisponiDsrSRV extends AbstractService implements IPredisponiDsrSRV {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = -1150899348250758780L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(PredisponiDsrSRV.class.getName());
	
	/**
	 * Properties.
	 */	
	private PropertiesProvider pp;

	/**
	 * Service.
	 */	
	@Autowired
	private IDocumentoRedSRV documentoRedSRV;
	
	/**
	 * Service.
	 */	
	@Autowired
	private IProtocollaDsrSRV protocollaSRV;
		
	/**
	 * DAO.
	 */	
	@Autowired
	private IUtenteDAO utenteDAO;
	
	/**
	 * Service.
	 */	
	@Autowired
	private ISalvaDocumentoSRV salvaDocumentoSRV;
	
	/**
	 * Post construct del service.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.service.facade.IPredisponiDsrFacadeSRV#getDocumentDetail(java.lang.String,
	 *      java.lang.String, java.lang.Integer, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public DetailDocumentRedDTO getDocumentDetail(final String documentTitle, final String wobNumber, final Integer numeroDocumento, final UtenteDTO utente) {
		final DetailDocumentRedDTO detail = retrieveDetailProtocollato(documentTitle, wobNumber, utente);
		detail.setNumeroDocumento(numeroDocumento);
		if (CollectionUtils.isNotEmpty(detail.getAllegati())) {					
			detail.getAllegati().clear();
		}
		return detail;
	}

	/**
	 * @see it.ibm.red.business.service.IPredisponiDsrSRV#retrieveDetailProtocollato(java.lang.String,
	 *      java.lang.String, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public DetailDocumentRedDTO retrieveDetailProtocollato(final String documentTitle, final String wobNumber, final UtenteDTO utente) {
		DetailDocumentRedDTO detail = null;
		Connection con = null;
		try {
			detail = documentoRedSRV.getDocumentDetail(documentTitle, utente, wobNumber, pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));
			
			con = setupConnection(getDataSource().getConnection(), false);
			
			/**** Probabilmente queste informazioni sono non necessarie non vengono usate da nessuno e sono duplicate in rispostaAllaccioDTO ****/
			if (detail.getAnnoProtocollo() != null) {
				detail.setAnnoProtocolloRisposta(detail.getAnnoProtocollo());
			}
			if (detail.getNumeroProtocollo() != null) {
				detail.setNumeroProtocolloRisposta(detail.getNumeroProtocollo().toString());
			}
			detail.setNumeroProtocollo(0);
			detail.setAnnoProtocollo(0);
			/****** fine informazioni non necessarie ****/
			
			detail.setMittente(null);
			protocollaSRV.inizializzaTipoProcedimentoAndDocumento(detail, CategoriaDocumentoEnum.DOCUMENTO_USCITA.getIds()[0], utente, con);
			
			final RispostaAllaccioDTO rispostaAllaccio = new RispostaAllaccioDTO();
			if (detail.getAnnoProtocolloRisposta() != null) {
				rispostaAllaccio.setAnnoProtocollo(detail.getAnnoProtocolloRisposta());
			}
			
			if (detail.getNumeroProtocolloRisposta() != null) {
				impostaProtcolloAllaccio(detail, rispostaAllaccio);
			} else {
				rispostaAllaccio.setNumeroProtocollo(0);
			}
			
			rispostaAllaccio.setIdDocumentoAllacciato(documentTitle);
			final List<RispostaAllaccioDTO> rispostaAllaccioList = detail.getAllacci() == null ? new ArrayList<>() : detail.getAllacci();
			rispostaAllaccioList.add(rispostaAllaccio);
			detail.setAllacci(rispostaAllaccioList);

			/**
			 * Alla dichiarazione appena creata vanno aggiunti i destinatari presi dalle configurazioni e anche il responsabile per la firma.
			 */
			final List<UtenteDTO> utentiDestinatariList = utenteDAO.getUtentiByDescNodoByDescPermesso(pp.getParameterByKey(PropertiesNameEnum.FEPA_UFFICIO_DESTINATARI), pp.getParameterByKey(PropertiesNameEnum.FEPA_PERMESSI_DESTINATARI), con);
			final List<DestinatarioRedDTO> destinatarioList = new ArrayList<>();
			for (final UtenteDTO utenteDest : utentiDestinatariList) {
				final DestinatarioRedDTO destinatario = new DestinatarioRedDTO();
				destinatario.setIdNodo(utenteDest.getIdUfficio());
				destinatario.setIdUtente(utenteDest.getId());
				
				//COSTANTI
				final Contatto contatto = new Contatto();
				contatto.setAliasContatto(formatDsrUtenteDescrizione(utenteDest));
				destinatario.setContatto(contatto);
				destinatario.setModalitaDestinatarioEnum(ModalitaDestinatarioEnum.TO);
				destinatario.setTipologiaDestinatarioEnum(TipologiaDestinatarioEnum.INTERNO);
				//FINE COSTANTI
				
				destinatarioList.add(destinatario);
			}
			detail.setDestinatari(destinatarioList);
			
			final AssegnazioneDTO utenteFirma = createAssegnatarioFirmaDefault(utente, con);
			
			final AssegnazioneDTO ufficioAssegnatario = protocollaSRV.getAssegnazioneDefaultUfficioFepa(con);

			if (ufficioAssegnatario == null) {
				throw new RedException("Predisponi dsr, impossibile recuperare l'ufficio di competenza");
			}
		
			detail.setAssegnazioni(new ArrayList<>());
			
			detail.getAssegnazioni().add(ufficioAssegnatario);
			detail.getAssegnazioni().add(utenteFirma);
			
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
		return detail;
	}

	/**
	 * @param detail
	 * @param rispostaAllaccio
	 */
	private static void impostaProtcolloAllaccio(final DetailDocumentRedDTO detail, final RispostaAllaccioDTO rispostaAllaccio) {
		try {
			final int npr = Integer.parseInt(detail.getNumeroProtocolloRisposta());
			rispostaAllaccio.setNumeroProtocollo(npr);
		} catch (final Exception e) {
			LOGGER.warn(e);
			rispostaAllaccio.setNumeroProtocollo(0);
		}
	}

	private AssegnazioneDTO createAssegnatarioFirmaDefault(final UtenteDTO utente, final Connection con) {
		/**
		 * si aggiunge il responsabile della firma
		 */
		final String descrizioneNodo = pp.getParameterByKey(PropertiesNameEnum.FEPA_NODO_FIRMA);
		final UtenteDTO utenteFirmatario = utenteDAO.getUtenteByUsernameByDescNodo(pp.getParameterByKey(
				PropertiesNameEnum.FEPA_UTENTE_FIRMA),
				descrizioneNodo,
				con);
		final String descrizione = formatDsrUtenteDescrizione(utenteFirmatario);
		
		final UfficioDTO ufficioUtenteFirma = new UfficioDTO(utente.getIdUfficio(), ""); 
		return new AssegnazioneDTO("Assegnazione di default per predisponi dsr", TipoAssegnazioneEnum.FIRMA, "", null, descrizione, utenteFirmatario, ufficioUtenteFirma);
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IPredisponiDsrFacadeSRV#getDetailDSRPredisposta(java.lang.String,
	 *      java.lang.String, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public DetailDocumentRedDTO getDetailDSRPredisposta(final String documentTitle, final String wobNumber, final UtenteDTO utente) {
		DetailDocumentRedDTO detail = null;
		Connection con = null;
		try {
			detail = documentoRedSRV.getDocumentDetail(documentTitle, utente, wobNumber, pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY));
			
			con = setupConnection(getDataSource().getConnection(), false);
		
			if (detail.getFascicoli() != null) {
				
				//Inserisco l'ultimo come fascicolo procedimentale (per ottenere lo stesso risultato di NSD) START
				final FascicoloDTO fasc = detail.getFascicoli().get(detail.getFascicoli().size() - 1);		
				
				detail.setDescrizioneFascicoloProcedimentale(fasc.getDescrizione());
				detail.setIdFascicoloProcedimentale(fasc.getIdFascicolo());
				if (!StringUtils.isNullOrEmpty(fasc.getOggetto())) {
					final String[] arr = fasc.getOggetto().split("_");
					if (arr.length >= 3) {
						detail.setPrefixFascicoloProcedimentale(arr[0] + "_" + arr[1] + "_");
					}
				}
				detail.setIndiceClassificazioneFascicoloProcedimentale(fasc.getIndiceClassificazione());
			}
			
			// END
			final AssegnazioneDTO utenteFirma = createAssegnatarioFirmaDefault(utente, con);
			
			final AssegnazioneDTO ufficioAssegnatario = protocollaSRV.getAssegnazioneDefaultUfficioFepa(con);

			if (ufficioAssegnatario == null) {
				throw new RedException("Predisponi dsr, impossibile recuperare l'ufficio di competenza");
			}
		
			detail.setAssegnazioni(new ArrayList<>());
			
			detail.getAssegnazioni().add(ufficioAssegnatario);
			detail.getAssegnazioni().add(utenteFirma);
			
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
		return detail;
	}

	private static String formatDsrUtenteDescrizione(final UtenteDTO utente) {
		return utente.getNodoDesc() + " - " + utente.getNome() + " - " + utente.getCognome();
	}
	
	/**
	 * @see it.ibm.red.business.service.IPredisponiDsrSRV#predisponiDichiarazione(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.DetailDocumentRedDTO).
	 */
	@Override
	public EsitoSalvaDocumentoDTO predisponiDichiarazione(final UtenteDTO utente, final DetailDocumentRedDTO detail) {
		EsitoSalvaDocumentoDTO esito = new EsitoSalvaDocumentoDTO();
		esito.setDocumentTitle(detail.getDocumentTitle());
		esito.setEsitoOk(false);
		IFilenetCEHelper fceh = null;
		
		try {
			if (detail.getContent() != null && detail.getContent().length != 0) { // caso contenuto modificato
				if (!protocollaSRV.validateAdobeProtocollazioneDsr(detail.getContent())) {
					throw new RedException("Il documento inserito non è protocollabile");
				}
			} else { // se il contenuto non è stato modificato lo recupero				
				fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
				final Document document = fceh.getDocumentByDTandAOO(detail.getDocumentTitle(), utente.getIdAoo());
				final byte[] content = FilenetCEHelper.getDocumentContentAsByte(document);
				detail.setContent(content);
			}
			
			// Questo perché sto ribaltando in un nuovo dettaglio che non deve essere lo stesso
			detail.setIdFascicoloProcedimentale(null);
			final SalvaDocumentoRedParametriDTO parametriPredisposizione = new SalvaDocumentoRedParametriDTO();
			parametriPredisposizione.setModalita(Modalita.MODALITA_INS);
			parametriPredisposizione.setContentVariato(true);
			esito = salvaDocumentoSRV.salvaDocumento(detail, parametriPredisposizione, utente, ProvenienzaSalvaDocumentoEnum.FEPA_DSR);
		} catch (final RedException e) {
			throw e;
		} catch (final Exception e) {
			LOGGER.error("Errore generico durante la predisposizione documento.", e);
			throw new RedException("Impossibile predisporre la dichiarazione");
		} finally {
			popSubject(fceh);
		}
		
		return esito;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IPredisponiDsrFacadeSRV#predisponiDichiarazione(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 *      java.util.List, byte[]).
	 */
	@Override
	public EsitoSalvaDocumentoDTO predisponiDichiarazione(final UtenteDTO utente, final String documentTitle, final String wobNumber, final String nomeFile, final String oggetto, final List<AllegatoDTO> allegati, final byte[] content) {
		try {
			DetailDocumentRedDTO detail = null;
			detail = retrieveDetailProtocollato(documentTitle, wobNumber, utente);
			// il mittente deve essere null per settare i destinatari
			detail.setMittente(null);
			detail.setMittenteContattoFromMailOnTheFly(null);
			detail.setNomeFile(nomeFile);
			detail.setOggetto(oggetto);
			detail.setAllegati(allegati);
			detail.setContent(content);
			return predisponiDichiarazione(utente, detail);
		} catch (final RedException e) {
			LOGGER.warn(e);
			final EsitoSalvaDocumentoDTO esito = new EsitoSalvaDocumentoDTO();
			esito.setDocumentTitle(documentTitle);
			esito.setEsitoOk(false);
			esito.setNote(e.getMessage());
			return esito;
		}
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IPredisponiDsrFacadeSRV#retrieveDestinatariDsr().
	 */
	@Override
	public List<UtenteDTO> retrieveDestinatariDsr() {
		List<UtenteDTO> list = null;
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			list = utenteDAO.getUtentiByDescNodoByDescPermesso(pp.getParameterByKey(
					PropertiesNameEnum.FEPA_UFFICIO_DESTINATARI),
					pp.getParameterByKey(PropertiesNameEnum.FEPA_PERMESSI_DESTINATARI),
					con);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero dei destinatari per predispone dichiarazione", e);
			rollbackConnection(con);
		} finally {
			closeConnection(con);
		}
		return list;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IPredisponiDsrFacadeSRV#retrieveContentDsr(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public InputStream retrieveContentDsr(final String documentTitle, final UtenteDTO utente) {
		InputStream io = null;
		final IFilenetCEHelper fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
		
		try {
			final Document document = fceh.getDocumentByDTandAOO(documentTitle, utente.getIdAoo());
			io = FilenetCEHelper.getDocumentContentAsInputStream(document);
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero del contenuto della DSR.", e);
		} finally {
			popSubject(fceh);
		}
		
		return io;
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IPredisponiDsrFacadeSRV#aggiornaDichiarazione(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String, java.lang.String, java.lang.String,
	 *      java.util.List, byte[], boolean).
	 */
	@Override
	public EsitoSalvaDocumentoDTO aggiornaDichiarazione(final UtenteDTO utente, final String documentTitle, final String wobNumber, final String nomeFile, final String oggetto, final List<AllegatoDTO> allegati, final byte[] content, final boolean contentVariato) {
		DetailDocumentRedDTO detail = null;
		try {
			detail = getDetailDSRPredisposta(documentTitle, wobNumber, utente);
		} catch (final RedException e) {
			LOGGER.warn(e);
			final EsitoSalvaDocumentoDTO esito = new EsitoSalvaDocumentoDTO();
			esito.setDocumentTitle(documentTitle);
			esito.setEsitoOk(false);
			esito.setNote(e.getMessage());
			return esito;
		}
		detail.setNomeFile(nomeFile);
		detail.setOggetto(oggetto);
		detail.setAllegati(allegati);
		if (contentVariato) {
			detail.setContent(content);
		}
		return aggiornaDichiarazione(utente, detail, contentVariato);
	}
	
	/**
	 * @see it.ibm.red.business.service.facade.IPredisponiDsrFacadeSRV#aggiornaDichiarazione(it.ibm.red.business.dto.UtenteDTO,
	 *      it.ibm.red.business.dto.DetailDocumentRedDTO, boolean).
	 */
	@Override
	public EsitoSalvaDocumentoDTO aggiornaDichiarazione(final UtenteDTO utente, final DetailDocumentRedDTO detail, final boolean contentVariato) {
		EsitoSalvaDocumentoDTO esito = new EsitoSalvaDocumentoDTO();
		esito.setDocumentTitle(detail.getDocumentTitle());
		esito.setEsitoOk(false);
		try {
			if (contentVariato && detail.getContent() != null && detail.getContent().length != 0) { // caso contenuto modificato
				if (!protocollaSRV.validateAdobeProtocollazioneDsr(detail.getContent())) {
					throw new RedException("Il documento inserito non è valido");
				}
			} else if (contentVariato) {
				throw new RedException("E'necessario inserire un documento principale");
			}
			
			final SalvaDocumentoRedParametriDTO parametriPredisposizione = new SalvaDocumentoRedParametriDTO();
			parametriPredisposizione.setModalita(Modalita.MODALITA_UPD);
			parametriPredisposizione.setContentVariato(contentVariato); // in realtà il servizio sottostante lo modifica sempre
			esito = salvaDocumentoSRV.salvaDocumento(detail, parametriPredisposizione, utente, ProvenienzaSalvaDocumentoEnum.FEPA_DSR);
		} catch (final RedException e) {
			throw e;
		} catch (final Exception e) {
			LOGGER.error("Errore generico durante la predisposizione documento", e);
			throw new RedException("Impossibile predisporre la dichiarazione");
		} 
		return esito;
	}
	
	/**
	 * 
	 * Questo metodo fa delle verifiche a basso costo prestazionale per dapere se il
	 * documento è modificabile.
	 * 
	 * Il senso è che un utente che non ha i privilegi per visualizzare i fascicoli
	 * non può mdoificare il docuemnto.
	 * 
	 * Teoricamente per fare una verifica accurrata dovremmo chiamare il pe per
	 * saperlo, ma per come abbiamo recuperato il documento questa verifica è già
	 * stata eseguita in maniera implicita.
	 * 
	 * @param detail
	 *            il detail deve essere visualizzato dal metodo contenuto in questo
	 *            servizio.
	 * @param utente
	 *            utente che vuole verificare il docuemnto.
	 * @return Una verifica del fatto che l'utente possa registrare il dettaglio.
	 */
	@Override
	public boolean isModificabile(final DetailDocumentRedDTO detail, final UtenteDTO utente) {
		return CollectionUtils.isNotEmpty(detail.getFascicoli());
	}
}
