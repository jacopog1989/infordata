package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.INotaDocumentoDAO;
import it.ibm.red.business.dto.AssegnatarioInteropDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.NotaDTO;
import it.ibm.red.business.dto.NotaDocumentoDTO;
import it.ibm.red.business.dto.NotaDocumentoDTO.OrigineNota;
import it.ibm.red.business.dto.NotaDocumentoDescrizioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ColoreNotaEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IDocumentoRedSRV;
import it.ibm.red.business.service.INotaSRV;
import it.ibm.red.business.service.IUtenteSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * The Class NotaSRV.
 *
 * @author FMANTUANO
 * 
 *        Servizi per la gestione della nota sul workflow.
 */
@Service
public class NotaSRV extends AbstractService implements INotaSRV {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3694101401238057042L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NotaSRV.class.getName());
	
	/**
	 * DAO.
	 */
	@Autowired
	private INotaDocumentoDAO notaDAO;
	
	/**
	 * Servizio.
	 */
	@Autowired
	private IDocumentoRedSRV documentoRedSRV;
	
	/**
	 * Servizio.
	 */
	@Autowired
	private IUtenteSRV utenteSRV;

	/**
	 * Recupero della nota.
	 * 
	 * @param fcDTO
	 *            credenziali filenet
	 * @param wobNumber
	 *            Wob Number
	 * @return content
	 */
	@Override
	public NotaDTO getNota(final FilenetCredentialsDTO fcDTO, final String wobNumber) {
		final NotaDTO result = new NotaDTO();
		final FilenetPEHelper fpeh = new FilenetPEHelper(fcDTO);
		final String notaFormat = (String) fpeh.getMetadato(wobNumber, "F_Comment");
		final String[] assArr = notaFormat.split("_");
		result.setColore(ColoreNotaEnum.get(Integer.parseInt(assArr[0].substring(1))));
		if (assArr.length > 1) {
			result.setContentNota(assArr[1]);
		}
		return result;
	}
 
	/**
	 * Salvataggio della nota. Qualora la nota e sopratutto l'indice di categoria
	 * della nota è nullo viene cancellata.
	 * 
	 * @param fcDTO
	 *            credenziali filenet
	 * @param wobNumbers
	 *            WobNumbers
	 * @param nota
	 *            content da salvare
	 * @param writeOnDb
	 *            indica se salvare la nota anche nello storico
	 */
	@Override
	public final Collection<EsitoOperazioneDTO> registraNotaFromWorkflow(final UtenteDTO utente, final List<String> wobNumbers, final NotaDTO nota, final Integer numDoc, final boolean writeOnDb) {
		final Collection<EsitoOperazioneDTO> esiti = new ArrayList<>();
		
		for (final String wobNumber : wobNumbers) {
			esiti.add(registraNotaFromWorkflow(utente, wobNumber, nota, numDoc, writeOnDb));
		}
		
		return esiti;
	} 
	
	/**
	 * Salvataggio della nota. Qualora la nota e sopratutto l'indice di categoria
	 * della nota è nullo viene cancellata.
	 * 
	 * @param fcDTO
	 *            credenziali filenet
	 * @param wobNumber
	 *            Wob Number
	 * @param nota
	 *            content da salvare
	 * @param writeOnDb
	 *            indica se salvare la nota anche nello storico
	 */
	@Override
	public EsitoOperazioneDTO registraNotaFromWorkflow(final UtenteDTO utente, final String wobNumber, final NotaDTO nota, final Integer numDoc, final boolean writeOnDb) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		FilenetPEHelper fpeh = null;
		try {
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			
			String notaFormat = null;
			if (nota.getContentNota() != null || nota.getColore() != null) {
				notaFormat = "#" + (nota.getColore().getId()) + "_" + nota.getContentNota();
			}
			
			final Map<String, Object> metadati = new HashMap<>();
			metadati.put("F_Comment", notaFormat);
			
			fpeh.updateWorkflow(fpeh.getWorkFlowByWob(wobNumber, false), metadati);
			
			if (writeOnDb) {
				final Integer idDocumento = (Integer) fpeh.getMetadato(wobNumber, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
				registraNotaOnDb(utente, idDocumento, nota.getColore(), nota.getContentNota(), OrigineNota.CODA);
			}
			
			esito.setEsito(true);
			esito.setIdDocumento(numDoc);
			esito.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inserimento di una nota per il workflow con wobNumber: " + wobNumber, e);
			esito.setIdDocumento(numDoc);
			esito.setNote("Errore durante l'inserimento della nota.");
		} finally {
			logoff(fpeh);
		}
		
		return esito;
	}
	
	private void registraNotaOnDb(final UtenteDTO utente, final Integer idDocumento, final ColoreNotaEnum colore, final String testoNota, final OrigineNota origine) {
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			registraNotaOnDb(utente, idDocumento, colore, testoNota, null, origine, con);
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il tentativo di apertura della connessione " + idDocumento, e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
		
	}
	
	/**
	 * Permette di inserire la nota, sfruttando la connessione comune al service.
	 * 
	 * @param utente
	 *            utente che esegue l'operazione.
	 * @param idDocumento
	 *            documentitle del documento a cui è associata la nota.
	 * @param colore
	 *            {@link ColoreNotaEnum}
	 * @param testoNota
	 *            viene chiamato nel documento contentNote.
	 * @param origine
	 *            La nota può essere prodotta durante la protocollazione mail, da
	 *            una response via coda, mediante il modifica documento nel tab nota
	 *            storico.
	 * @param con
	 *            connessione proveniente da altre fonti.
	 */
	private void registraNotaOnDb(final UtenteDTO utente, final Integer idDocumento, final ColoreNotaEnum colore,
			final String testoNota, /*final Integer ufficioAutoreNota, final Integer idAutoreNota*/ final String descrPreassegnatario, final OrigineNota origine, final Connection con) {
		try {
			String testoNotaDb = testoNota;
			// Se il testo della nota è vuoto, si inserisce il testo "N.D."
			if (StringUtils.isNullOrEmpty(testoNotaDb)) {
				testoNotaDb = "N.D.";
			}
			
			int idUfficio = utente.getIdUfficio().intValue();
			int idUtente = utente.getId().intValue();
			if (OrigineNota.MAIL.equals(origine)) {
				final UtenteDTO utenteAmm = utenteSRV.getAmministratoreAoo(utente.getCodiceAoo());
				idUfficio = utenteAmm.getIdUfficio().intValue();
				idUtente = utenteAmm.getId().intValue();
			}
			
			final NotaDocumentoDTO notaDocumento = new NotaDocumentoDTO(idDocumento, null, colore, testoNotaDb, 
					idUfficio, idUtente, origine.getIdOrigine());
			
			notaDAO.insertNote(notaDocumento, descrPreassegnatario, con);
		} catch (final Exception e) {
			LOGGER.error("Errore durante l'inserimento della nota per il documento " + idDocumento, e);
			throw new RedException(e);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.INotaFacadeSRV#deleteNotaDocumento(it.ibm.red.business.dto.NotaDocumentoDTO,
	 *      java.lang.Integer).
	 */
	@Override
	public void deleteNotaDocumento(final NotaDocumentoDTO nota, final Integer idAoo) {
		Connection con = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			notaDAO.deleteNote(nota.getIdDocumento(), idAoo, nota.getNumeroNota(), con);
		} catch (final Exception e) {
			LOGGER.error("Errore in fase cancellazione logica della nota " + nota, e);
			throw new RedException("Errore in fase cancellazione logica della nota " + nota, e);
		} finally {
			closeConnection(con);
		}
		
	}

	/**
	 * @see it.ibm.red.business.service.facade.INotaFacadeSRV#getAllNotes(java.lang.String,
	 *      boolean, it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public List<NotaDocumentoDescrizioneDTO> getAllNotes(final String idDocumento, final boolean alsoClosed, final UtenteDTO utente) {
		Connection con = null;
		
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			
			final Long idUtente = utente.getId();
			final boolean isCompetente = documentoRedSRV.isCompetenteDocumento(Integer.parseInt(idDocumento), utente);
			
			final List<NotaDocumentoDescrizioneDTO> note = notaDAO.getAllNotes(idDocumento, utente.getIdAoo().intValue(), alsoClosed, con);
			NotaDocumentoDescrizioneDTO lastNotaDocumentoFromCoda = null;
			for (final NotaDocumentoDescrizioneDTO nota : note) {
				
				boolean removable = false;
				if (isCompetente 
						&& idUtente.intValue() == nota.getIdUtenteOwner().intValue() 
						&& !nota.getDeleted()) {
					removable = true;
				}
				
				nota.setRemovable(removable);
				
				if (!nota.getDeleted() && OrigineNota.CODA.getIdOrigine().equals(nota.getIdOrigine())) {
					lastNotaDocumentoFromCoda = nota;
				}
				
				final String descrizioneUtente = StringUtils.joinWithwhiteSpace(nota.getCognomeUtenteOwner(), nota.getNomeUtenteOwner());
				nota.setDescrizioneUtenteOwner(descrizioneUtente);
				 
				final UtenteDTO utenteAmm = utenteSRV.getAmministratoreAoo(utente.getCodiceAoo());
				nota.setPregressoStoricoNote(true);
				if (nota.getIdUtenteOwner().equals(utenteAmm.getId().intValue())) {
					nota.setPregressoStoricoNote(false);
				} 
			}
			
			if (lastNotaDocumentoFromCoda != null) {
				lastNotaDocumentoFromCoda.setRemovable(false);
			}
			
			return note;
		} catch (final Exception e) {
			LOGGER.error("Errore in fase recupero delle note del documento " + idDocumento, e);
			throw new RedException("Errore in fase recupero delle note del documento " + idDocumento, e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.INotaFacadeSRV#registraNotaFromDocumento(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.Integer, it.ibm.red.business.enums.ColoreNotaEnum,
	 *      java.lang.String).
	 */
	@Override
	public void registraNotaFromDocumento(final UtenteDTO utente, final Integer idDocumento, final ColoreNotaEnum colore, final String testoNota) {
		registraNotaOnDb(utente, idDocumento, colore, testoNota, OrigineNota.STORICO);
	}
	
	/**
	 * @see it.ibm.red.business.service.INotaSRV#registraNotaFromCodaMail(it.ibm.red.business.dto.UtenteDTO,
	 *      int, java.lang.String, java.sql.Connection).
	 */
	@Override
	public void registraNotaFromCodaMail(final UtenteDTO utente, final int idDocumento, final String contentNoteDaMailProtocolla, final Connection con) {
		registraNotaOnDb(utente, idDocumento, ColoreNotaEnum.NERO, contentNoteDaMailProtocolla, null, OrigineNota.MAIL, con);
	}

	/**
	 * @see it.ibm.red.business.service.INotaSRV#registraNotaFromCodaMailDTO(it.ibm.red.business.dto.UtenteDTO,
	 *      int, it.ibm.red.business.dto.NotaDTO,
	 *      it.ibm.red.business.dto.AssegnatarioInteropDTO, java.sql.Connection).
	 */
	@Override
	public void registraNotaFromCodaMailDTO(final UtenteDTO utente, final int idDocumento, final NotaDTO contentNoteDaMailProtocolla, final AssegnatarioInteropDTO preassegnatario, final Connection con) {
		String descrizionePreassegnatario = Constants.EMPTY_STRING;
		if (preassegnatario != null && (preassegnatario.getIdUfficio() != null || preassegnatario.getDescrizioneUfficio() != null)) {
			descrizionePreassegnatario = preassegnatario.getDescrizioneUfficio();
			if (preassegnatario.getIdUtente() != null || preassegnatario.getDescrizioneUtente() != null) {
				descrizionePreassegnatario += "-" + preassegnatario.getDescrizioneUtente();
			}
		} 
		registraNotaOnDb(utente, idDocumento, contentNoteDaMailProtocolla.getColore(),
				contentNoteDaMailProtocolla.getContentNota(),
				descrizionePreassegnatario, OrigineNota.MAIL, con);
	}

}
