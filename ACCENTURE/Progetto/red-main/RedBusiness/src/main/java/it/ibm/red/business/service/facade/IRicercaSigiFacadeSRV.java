package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.dto.MasterSigiDTO;
import it.ibm.red.business.dto.ParamsRicercaAvanzataDocDTO;
import it.ibm.red.business.dto.UtenteDTO;

/**
 * The Interface IRicercaSigiFacadeSRV.
 *
 * @author m.crescentini
 * 
 *         Facade del servizio per la gestione della ricerca SIGI.
 */
public interface IRicercaSigiFacadeSRV extends Serializable {

	/**
	 * Esegue la ricerca SIGI.
	 * @param paramsRicercaSigi
	 * @param utente
	 * @param orderbyInQuery
	 * @return masters SIGI
	 */
	Collection<MasterSigiDTO> eseguiRicercaSigi(ParamsRicercaAvanzataDocDTO paramsRicercaSigi, UtenteDTO utente,
			boolean orderbyInQuery);
}