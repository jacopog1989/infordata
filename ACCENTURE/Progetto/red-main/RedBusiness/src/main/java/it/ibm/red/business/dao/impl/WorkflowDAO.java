package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IWorkflowDAO;
import it.ibm.red.business.dto.TipoProcedimentoDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Workflow;

/**
 * The Class WorkflowDAO.
 *
 * @author CPIERASC
 * 
 * 	Dao per la gestione dei workflow.
 */
@Repository
public class WorkflowDAO extends AbstractDAO implements IWorkflowDAO {

	/**
	 * Serializzation.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(WorkflowDAO.class.getName());
	
	/**
	 * Messaggio errore recupero workflow.
	 */
	private static final String ERROR_RECUPERO_WORKFLOW_MSG = "Errore durante il recupero del workflow dal DB.";

	/**
	 * Restituisce tutti i workflow.
	 * 
	 * @see it.ibm.red.business.dao.IWorkflowDAO#getAll(java.sql.Connection)
	 */
	@Override
	public Collection<Workflow> getAll(final Connection connection) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		final Collection<Workflow> workflowList = new ArrayList<>();
		
		try {
			final String querySQL = "SELECT * FROM workflow";
			ps = connection.prepareStatement(querySQL);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				final Workflow workflow = new Workflow(
						rs.getInt("IDWORKFLOW"), 
						rs.getString("WORKFLOWNAME"), 
						rs.getString("WORKFLOWDESCRIPTION"), 
						rs.getInt("FLAGGENERICOENTRATA"), 
						rs.getInt("FLAGGENERICOUSCITA"), 
						rs.getLong("IDAOO"));
				
				workflowList.add(workflow);
			}
		} catch (final SQLException e) {
			LOGGER.error("Errore durante il recupero dell'elenco dei workflow dal DB.", e);
			throw new RedException("Errore durante il recupero dell'elenco dei workflow dal DB.", e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return workflowList;
	}

	/**
	 * Restituisce il workflow identificato dall' {@code idWorkflow}.
	 * 
	 * @see it.ibm.red.business.dao.IWorkflowDAO#getById(java.sql.Connection, int)
	 */
	@Override
	public Workflow getById(final Connection connection, final int idWorkflow) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Workflow workflow = null;
		
		try {
			final String querySQL = "SELECT * FROM workflow WHERE IDWORKFLOW = ?";
			ps = connection.prepareStatement(querySQL);
			ps.setInt(1, idWorkflow);
			rs = ps.executeQuery();
			
			if (rs.next()) {
				workflow = new Workflow(
						rs.getInt("IDWORKFLOW"), 
						rs.getString("WORKFLOWNAME"), 
						rs.getString("WORKFLOWDESCRIPTION"), 
						rs.getInt("FLAGGENERICOENTRATA"), 
						rs.getInt("FLAGGENERICOUSCITA"), 
						rs.getLong("IDAOO"));
			}
		} catch (final SQLException e) {
			LOGGER.error(ERROR_RECUPERO_WORKFLOW_MSG, e);
			throw new RedException(ERROR_RECUPERO_WORKFLOW_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return workflow;
	}

	/**
	 * Recupera il workflow dal {@code procedimento}.
	 * 
	 * @see it.ibm.red.business.dao.IWorkflowDAO#getWorkflowId(it.ibm.red.business.dto.TipoProcedimentoDTO,
	 *      java.sql.Connection)
	 */
	@Override
	public int getWorkflowId(final TipoProcedimentoDTO procedimento, final Connection connection) {
		int idWorkflow = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			final String querySQL = "SELECT idworkflow FROM workflow WHERE idaoo = ? AND flaggenericoentrata = ? AND flaggenericouscita = ?";
			ps = connection.prepareStatement(querySQL);
			int index = 1;
			ps.setInt(index++, procedimento.getIdAoo());
			ps.setInt(index++, procedimento.getFlagGenericoEntrata());
			ps.setInt(index++, procedimento.getFlagGenericoUscita());
			rs = ps.executeQuery();
			
			if (rs.next()) {
				idWorkflow = rs.getInt("idworkflow");
			}
		} catch (final SQLException e) {
			LOGGER.error(ERROR_RECUPERO_WORKFLOW_MSG, e);
			throw new RedException(ERROR_RECUPERO_WORKFLOW_MSG, e);
		} finally {
			closeStatement(ps, rs);
		}
		return idWorkflow;
	}
}