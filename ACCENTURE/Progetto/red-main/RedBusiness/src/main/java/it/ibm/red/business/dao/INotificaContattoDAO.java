/**
 * 
 */
package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.persistence.model.NotificaContatto;

/**
 * Interfaccia per le transazioni a DB relativa alle Notichie dei contatti
 * @author raffaele.cassandra
 * @version 1.0
 */
public interface INotificaContattoDAO extends Serializable {
	
	/**
	 * Metodo che ottiene la lista delle notifiche di un contatto.
	 * @param con
	 * @return
	 * @throws DAOException
	 */
	List<NotificaContatto> getListaNotificaByIdContatto(int idContatto, String tipoNotifica, Connection con);
	
	
	/**
	 * Metodo che ottiene la notifica specifica.
	 * @param con
	 * @return
	 * @throws DAOException
	 */
	NotificaContatto getNotificaContatto(int idNotifica, Connection con);
	
	/**
	 * Metodo che effettua l'inserimento di una notifica contatto.
	 * @param con
	 * @return
	 * @throws DAOException
	 */
	void insertNotificaContatto(NotificaContatto notificaContatto, Connection con);

	
	/**
	 * COUNT Lista notifiche da evadere
	 * @param con
	 * @return
	 * @throws DAOException
	 */
	int getCountListaNotificheRichieste(Long idAoo, int numGiorni, Connection con);
	

	
	/**
	 * Metodo che aggiorna lostato di una singola notifica.
	 * @param idNotifica
	 * @param statoNotifica
	 * @param con
	 * @return
	 * @throws DAOException
	 */
	int updateStatoNotifica(int idNotifica, String statoNotifica, String motivoRifiuto, Connection con);
	 
	/**
	 * Ottiene la lista di notifiche tramite lo stato della notifica.
	 * @param idContatto
	 * @param statoNotifica
	 * @param con
	 * @return lista delle notifiche contatto
	 */
	List<NotificaContatto> getListaNotificheByStato(Long idContatto, String statoNotifica, Connection con);
	
	/**
	 * Elimina la notifica contatto sulla base dati.
	 * @param idNotifica
	 * @param con
	 * @return
	 */
	int removeNotifica(int idNotifica, Connection con);

	/**
	 * Lista notifiche da evadere.
	 * @param con
	 * @return
	 * @throws DAOException
	 */
	List<NotificaContatto> getListaNotificheRichieste(int numGiorni, Integer inNumRow, Long idAoo, Long idUtente, Connection con);


	/**
	 * Lista notifiche evase
	 * @param con
	 * @return
	 * @throws DAOException
	 */
	List<NotificaContatto> getListaNotificheEvase(Long idNodo, int numGiorni, Integer numRow, Connection con);


	/**
	 * Ottiene la lista di notifiche.
	 * @param idNodo
	 * @param numGiorni
	 * @param inNumRow
	 * @param con
	 * @param utente
	 * @return lista delle notifiche contatto
	 */
	List<NotificaContatto> getListaNotifiche(Long idNodo, int numGiorni, int inNumRow, Connection con, String utente);

 
	/**
	 * Aggiorna lo stato della notifiche sulla rubrica.
	 * @param statoNotifica
	 * @param idUtente
	 * @param idAoo
	 * @param idNotificaRubrica
	 * @param conn
	 * @return
	 */
	int updateStatoNotificaRubrica(Integer statoNotifica, Long idUtente, Long idAoo, Integer idNotificaRubrica, Connection conn);


	/**
	 * Ottiene la lista delle notifiche sull'homepage.
	 * @param idNodo
	 * @param numGiorni
	 * @param maxNotificheNonLette
	 * @param inNumRow
	 * @param con
	 * @param utente
	 * @param idUtente
	 * @return lista delle notifiche contatto
	 */
	List<NotificaContatto> getListaNotificheHomePage(Long idNodo, int numGiorni, int maxNotificheNonLette, int inNumRow,
			Connection con, String utente, Long idUtente);

	/**
	 * Ottiene la lista delle notifiche richieste sull'homepage.
	 * @param numGiorni
	 * @param maxNotificheNonLette
	 * @param inNumRow
	 * @param idAoo
	 * @param idUtente
	 * @param con
	 * @return lista delle notifiche contatto
	 */
	List<NotificaContatto> getListaNotificheRichiesteHome(int numGiorni, int maxNotificheNonLette, int inNumRow,
			Long idAoo, Long idUtente, Connection con);
 
	/**
	 * Ottiene il numero delle notifiche richieste sull'homepage.
	 * @param maxNotificheNonLette
	 * @param inNumRow
	 * @param idAoo
	 * @param idUtente
	 * @param con
	 * @return numero delle notifiche contatto
	 */
	Integer getCountListaNotificheRichiesteHome(int maxNotificheNonLette, int inNumRow, Long idAoo, Long idUtente, Connection con);
 
	/**
	 * Ottiene il numero delle notifiche sull'homepage.
	 * @param maxNotificheNonLette
	 * @param inNumRow
	 * @param con
	 * @param utente
	 * @param idUtente
	 * @return numero delle notifiche contatto
	 */
	Integer getCountListaNotificheHomePage(int maxNotificheNonLette, int inNumRow, Connection con, String utente, Long idUtente);

  
}
