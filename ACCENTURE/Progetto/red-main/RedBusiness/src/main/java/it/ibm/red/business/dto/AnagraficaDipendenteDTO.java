package it.ibm.red.business.dto;

import java.util.Date;

/**
 * DTO per la gestione della anagrafiche dipendenti.
 */
public class AnagraficaDipendenteDTO extends RicercaAnagDipendentiDTO {

	/**
	 * La costante serial Version UID.
	 */
	private static final long serialVersionUID = -7247644054266563175L;
	
	/**
	 * Codice.
	 */
	private String codice;
	
	/**
	 * Data di nascita.
	 */
	private Date dataNascita;  
	
	/**
	 * Comune di nascita.
	 */
	private String comuneNascita;
	
	/**
	 * Codice provincia di nascita.
	 */
	private String codiceProvinciaNascita;
	
	/**
	 * Flag maschio.
	 */
	private Boolean flagMaschio;  
	
	/**
	 * Flag celibe.
	 */
	private Boolean flagCelibe;  
	
	/**
	 * Flag pensione.
	 */
	private Boolean flagPensione;  
	
	/**
	 * Flag ufficio mittente.
	 */
	private String ufficioMittente;
	
	/**
	 * Flag identificativo.
	 */
	private Integer id;

	/**
	 * Costruttore di classe.
	 */
	public AnagraficaDipendenteDTO() {
		super();
		flagMaschio = true;  
		flagCelibe = true;  
		flagPensione = false;  
	}
	
	/**
	 * Costruttore di classe.
	 * @param nome
	 * @param cognome
	 * @param codiceFiscale
	 * @param dataNascita
	 * @param comuneNascita
	 * @param flagMaschio
	 */
	public AnagraficaDipendenteDTO(String nome, String cognome, String codiceFiscale, Date dataNascita,
			String comuneNascita, Boolean flagMaschio) {
		super(nome, cognome, codiceFiscale);
		this.dataNascita = dataNascita;
		this.comuneNascita = comuneNascita;
		this.flagMaschio = flagMaschio;
	}

	/**
	 * Restituisce il codice della provincia di nascita.
	 * @return codiceProvinciaNascita
	 */
	public String getCodiceProvinciaNascita() {
		return codiceProvinciaNascita;
	}

	/**
	 * Imposta il codice di provincia di nascita.
	 * @param codiceProvinciaNascita
	 */
	public void setCodiceProvinciaNascita(final String codiceProvinciaNascita) {
		this.codiceProvinciaNascita = codiceProvinciaNascita;
	}

	/**
	 * @return codice
	 */
	public String getCodice() {
		return codice;
	}

	/**
	 * @param codice
	 */
	public void setCodice(final String codice) {
		this.codice = codice;
	}

	/**
	 * @return data Nascita utente
	 */
	public Date getDataNascita() {
		return dataNascita;
	}

	/**
	 * @param dataNascita
	 */
	public void setDataNascita(final Date dataNascita) {
		this.dataNascita = dataNascita;
	}

	/**
	 * @return comune Nascita utente
	 */
	public String getComuneNascita() {
		return comuneNascita;
	}

	/**
	 * @param comuneNascita
	 */
	public void setComuneNascita(final String comuneNascita) {
		this.comuneNascita = comuneNascita;
	}

	/**
	 * @return true se maschio, false se femmina
	 */
	public boolean getFlagMaschio() {
		return flagMaschio;
	}

	/**
	 * @param flagMaschio
	 */
	public void setFlagMaschio(final boolean flagMaschio) {
		this.flagMaschio = flagMaschio;
	}

	/**
	 * @return true se celibe, false altrimenti
	 */
	public Boolean getFlagCelibe() {
		return flagCelibe;
	}

	/**
	 * @param flagCelibe
	 */
	public void setFlagCelibe(final Boolean flagCelibe) {
		this.flagCelibe = flagCelibe;
	}

	/**
	 * @return true se in pensione, false altrimenti
	 */
	public Boolean getFlagPensione() {
		return flagPensione;
	}

	/**
	 * @param flagPensione
	 */
	public void setFlagPensione(final Boolean flagPensione) {
		this.flagPensione = flagPensione;
	}

	/**
	 * @return ufficioMittente
	 */
	public String getUfficioMittente() {
		return ufficioMittente;
	}

	/**
	 * @param ufficioMittente
	 */
	public void setUfficioMittente(final String ufficioMittente) {
		this.ufficioMittente = ufficioMittente;
	}

	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param inId
	 */
	public void setId(final Integer inId) {
		id = inId;
	}
}
