
package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.filenet.api.collection.DocumentSet;

import filenet.vw.api.VWQueueQuery;
import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.ICalendarioDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.INotificaUtenteDAO;
import it.ibm.red.business.dao.impl.TipoCalendario;
import it.ibm.red.business.dto.EventoDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.MasterDocumentRedDTO;
import it.ibm.red.business.dto.RuoloDTO;
import it.ibm.red.business.dto.SearchEventiDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.DocumentQueueEnum;
import it.ibm.red.business.enums.EventoCalendarioEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.StatoNotificaUtenteEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.TrasformPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.Ente;
import it.ibm.red.business.persistence.model.Evento;
import it.ibm.red.business.persistence.model.Nodo;
import it.ibm.red.business.persistence.model.TestiDefault;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.ICalendarioSRV;
import it.ibm.red.business.utils.CollectionUtils;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * Service gestione calendar.
 */
@Service
@Component
public class CalendarioSRV extends AbstractService implements ICalendarioSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 3095033521224983882L;

	/**
	 * Label scadenza, occorre appendere la data di scadenza al messaggio.
	 */
	private static final String SCADENZA_LABEL = " in scadenza il ";

	/**
	 * Label Documento.
	 */
	private static final String DOCUMENTO_LABEL = "Documento ";
	
	/**
	 * Messaggio errore.
	 */
	private static final String ERRORE_NEL_RAGGIUNGERE_LA_CONNESSIONE = "Errore nel raggiungere la connessione";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(CalendarioSRV.class.getName());

	/**
	 * DAO calendario.
	 */
	@Autowired
	private ICalendarioDAO calendarioDAO;

	/**
	 * DAO aoo.
	 */
	@Autowired
	private IAooDAO aooDAO;

	/**
	 * DAO nodo.
	 */
	@Autowired
	private INodoDAO nodoDAO;

	/**
	 * Notifica utente dao.
	 */
	@Autowired
	private INotificaUtenteDAO notificaUtenteDAO;

	/**
	 * Fornitore della properties dell'applicazione.
	 */
	private PropertiesProvider pp;

	/**
	 * Metodo richiamato a seguito dell'instance della classe.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICalendarioFacadeSRV#creaEvento(it.ibm.red.business.dto.EventoDTO).
	 */
	@Override
	public boolean creaEvento(final EventoDTO evento) {
		Connection connection = null;
		boolean isCreated = false;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			final Aoo aoo = aooDAO.getAoo(evento.getIdAoo(), connection);
			if (aoo != null && aoo.getEnte() != null) {
				evento.setIdEnte(aoo.getEnte().getIdEnte());
			}

			final Evento evt = settingEvento(evento);
			evt.setDataInizio(DateUtils.setDateTo0000(evt.getDataInizio()));
			evt.setDataScadenza(DateUtils.setDateTo2359(evt.getDataScadenza()));

			final int idEvento = calendarioDAO.insertEvento(evt, connection);
			if (evt.getTipoCalendario().getTipoEnum().equals(EventoCalendarioEnum.COMUNICAZIONE) || evt.getTipoCalendario().getTipoEnum().equals(EventoCalendarioEnum.NEWS)) {

				if (evt.getIdRuoliCategoriaTarget() != null) {
					calendarioDAO.insertRuoliTarget(idEvento, evt.getIdRuoliCategoriaTarget(), connection);
				}

				if (evt.getIdNodiTarget() != null) {
					calendarioDAO.insertNodiTarget(idEvento, evt.getIdNodiTarget(), connection);
				}
			}
			isCreated = true;
		} catch (final Exception e) {
			LOGGER.error(ERRORE_NEL_RAGGIUNGERE_LA_CONNESSIONE + e.getMessage(), e);
			throw new RedException("Si è verificato un'errore nella creazione dell'evento", e);
		} finally {
			closeConnection(connection);
		}

		return isCreated;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICalendarioFacadeSRV#cancellaEvento(int).
	 */
	@Override
	public boolean cancellaEvento(final int idEvento) {
		Connection connection = null;
		boolean actionOk = false;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			actionOk = calendarioDAO.cancellaEvento(idEvento, connection);
		} catch (final Exception e) {
			LOGGER.error(ERRORE_NEL_RAGGIUNGERE_LA_CONNESSIONE + e.getMessage(), e);
			throw new RedException("Si è verificato un'errore durante l'eliminazione dell'evento", e);
		} finally {
			closeConnection(connection);
		}

		return actionOk;
	}

	private static Evento settingEvento(final EventoDTO evt) {
		final Evento evento = new Evento();

		evento.setDataInizio(evt.getDataInizio());
		evento.setDataScadenza(evt.getDataScadenza());
		evento.setTitolo(evt.getTitolo());
		evento.setContenuto(evt.getContenuto());
		evento.setAllegato(evt.getAllegato());
		evento.setDataTime(evt.getDataTime());
		evento.setEstensioneAllegato(evt.getEstensioneAllegato());
		evento.setIdAoo(evt.getIdAoo());
		evento.setIdEnte(evt.getIdEnte());
		evento.setIdEvento(evt.getIdEvento());
		evento.setIdNodo(evt.getIdNodo());
		evento.setIdRuolo(evt.getIdRuolo());
		evento.setIdUtente(evt.getIdUtente());
		evento.setLinkEsterno(evt.getLinkEsterno());
		final TipoCalendario tipo = new TipoCalendario();
		evento.setTipoCalendario(tipo);
		evento.getTipoCalendario().setIdTipo(evt.getTipologia().getIdTipoCal());
		evento.getTipoCalendario().setCodice(evt.getTipologia().getCodice());
		evento.getTipoCalendario().setTipoEnum(evt.getTipologia());
		evento.setNomeAllegato(evt.getNomeAllegato());
		evento.setAllegato(evt.getAllegato());
		evento.setEstensioneAllegato(evt.getEstensioneAllegato());
		evento.setNumeroDocumento(evt.getNumeroDocumento());
		evento.setIdRuoliCategoriaTarget(evt.getIdRuoliCategoriaTarget());
		evento.setIdNodiTarget(evt.getIdNodiTarget());
		evento.setDescrizioneNodi(evt.getDescrizioneNodi());
		evento.setIdCategoriaDocumento(evt.getIdCategoriaDocumento());

		return evento;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICalendarioFacadeSRV#changeEventoToEventoDTO(it.ibm.red.business.persistence.model.Evento).
	 */
	@Override
	public EventoDTO changeEventoToEventoDTO(final Evento evt) {
		final EventoDTO evento = new EventoDTO();

		evento.setDataInizio(evt.getDataInizio());
		evento.setDataScadenza(evt.getDataScadenza());
		evento.setTitolo(evt.getTitolo());
		evento.setContenuto(evt.getContenuto());
		evento.setAllegato(evt.getAllegato());
		evento.setDataTime(evt.getDataTime());
		evento.setEstensioneAllegato(evt.getEstensioneAllegato());
		evento.setIdAoo(evt.getIdAoo());
		evento.setIdEnte(evt.getIdEnte());
		evento.setIdEvento(evt.getIdEvento());
		evento.setIdNodo(evt.getIdNodo());
		if (evt.getIdRuolo() != null) {
			evento.setIdRuolo(evt.getIdRuolo());
		}
		evento.setIdUtente(evt.getIdUtente());
		evento.setLinkEsterno(evt.getLinkEsterno());
		evento.setTipologia(evt.getTipoCalendario().getTipoEnum());
		evento.setNomeAllegato(evt.getNomeAllegato());
		evento.setAllegato(evt.getAllegato());
		evento.setEstensioneAllegato(evt.getEstensioneAllegato());
		evento.setMimeTypeAllegato(evt.getMimeTypeAllegato());
		evento.setDocumentTitle(evt.getDocumentTitle());
		evento.setNumeroDocumento(evt.getNumeroDocumento());
		evento.setIdRuoliCategoriaTarget(evt.getIdRuoliCategoriaTarget());
		evento.setIdNodiTarget(evt.getIdNodiTarget());
		evento.setDescrizioneNodi(evt.getDescrizioneNodi());
		evento.setIdCategoriaDocumento(evt.getIdCategoriaDocumento());

		return evento;
	}

	@Override
	public final Ente getEnte(final Long idAoo) {
		Ente ente = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			final Aoo aoo = aooDAO.getAoo(idAoo, connection);
			ente = aoo.getEnte();
		} catch (final Exception e) {
			LOGGER.error(ERRORE_NEL_RAGGIUNGERE_LA_CONNESSIONE + e.getMessage(), e);
			throw new RedException(e.getMessage());
		} finally {
			closeConnection(connection);
		}

		return ente;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICalendarioFacadeSRV#modificaEvento(it.ibm.red.business.dto.EventoDTO).
	 */
	@Override
	public boolean modificaEvento(final EventoDTO evento) {
		Connection connection = null;
		boolean actionOk = false;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			final Evento evt = settingEvento(evento);
			evt.setDataInizio(DateUtils.setDateTo0000(evt.getDataInizio()));
			evt.setDataScadenza(DateUtils.setDateTo2359(evt.getDataScadenza()));

			actionOk = calendarioDAO.aggiornaEvento(evt, connection);

			if (evt.getTipoCalendario().getTipoEnum().equals(EventoCalendarioEnum.COMUNICAZIONE) || evt.getTipoCalendario().getTipoEnum().equals(EventoCalendarioEnum.NEWS)) {

				final int idEvento = evt.getIdEvento();

				calendarioDAO.deleteRuoliTarget(idEvento, connection);

				if (evt.getIdRuoliCategoriaTarget() != null) {
					calendarioDAO.insertRuoliTarget(idEvento, evt.getIdRuoliCategoriaTarget(), connection);
				}

				calendarioDAO.deleteNodiTarget(idEvento, connection);

				if (evt.getIdNodiTarget() != null) {
					calendarioDAO.insertNodiTarget(idEvento, evt.getIdNodiTarget(), connection);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(ERRORE_NEL_RAGGIUNGERE_LA_CONNESSIONE + e.getMessage(), e);
			throw new RedException(e.getMessage());
		} finally {
			closeConnection(connection);
		}
		return actionOk;
	}

	/**
	 * @return Restituisce la lista dei testi di default per gli eventi di tipo
	 *         comunicazione censiti su db.
	 */
	@Override
	public List<TestiDefault> getAllTestiDefault() {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return calendarioDAO.getAllTestiDefault(connection);
		} catch (final SQLException e) {
			LOGGER.error(ERRORE_NEL_RAGGIUNGERE_LA_CONNESSIONE + e.getMessage(), e);
			throw new RedException(e.getMessage());
		} finally {
			closeConnection(connection);
		}

	}

	/**
	 * @see it.ibm.red.business.service.facade.ICalendarioFacadeSRV#getCategorieRuoli(java.lang.Long).
	 */
	@Override
	public List<RuoloDTO> getCategorieRuoli(final Long idAoo) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return calendarioDAO.getCategorieRuoli(connection, idAoo);
		} catch (final SQLException e) {
			LOGGER.error(ERRORE_NEL_RAGGIUNGERE_LA_CONNESSIONE + e.getMessage(), e);
			throw new RedException(e.getMessage());
		} finally {
			closeConnection(connection);
		}

	}

	/**
	 * Recupera gli eventi da mostrare nel calendario di qualsiasi tipologia che
	 * rispetti i seguenti vincoli (se impostati):<br/>
	 * <ul>
	 * <li>siano visibili a searchEventiDTO.account</li>
	 * <li>non scadano prima di searchEventiDTO.dataInizio</li>
	 * <li>non iniziano dopo searchEventiDTO.dataFine</li>
	 * <li>contengano la stringa searchEventiDTO.descrLike nel titolo o nel
	 * contenuto</li>
	 * <li>siano caratterizzati da una delle tipologie in
	 * searchEventiDTO.tipiEvento</li>
	 * </ul>
	 * searchEventiDTO.maxResults viene ignorato<br/>
	 * .
	 * 
	 * @param searchEventiDTO
	 * @return
	 * @throws SQLException
	 */
	@Override
	public Map<EventoCalendarioEnum, List<Evento>> getEventiForCalendario(final SearchEventiDTO searchEventiDTO, final UtenteDTO utente, final Integer idEnte,
			final Connection con) {
		final EnumMap<EventoCalendarioEnum, List<Evento>> m = new EnumMap<>(EventoCalendarioEnum.class);

		try {
			List<EventoCalendarioEnum> listaTipiDaRicercare = null;

			if (searchEventiDTO.getTipiEvento() != null && !searchEventiDTO.getTipiEvento().isEmpty()) {
				listaTipiDaRicercare = new ArrayList<>(searchEventiDTO.getTipiEvento());
			} else {
				// cerca tutti i tipi di evento
				listaTipiDaRicercare = Arrays.asList(EventoCalendarioEnum.values());
				searchEventiDTO.setTipiEvento(new ArrayList<>());
			}

			// cerca i tipi evento presenti in lista
			List<Evento> listaEventi = null;
			for (final EventoCalendarioEnum tipoEnum : listaTipiDaRicercare) {
				if (EventoCalendarioEnum.SCADENZA.equals(tipoEnum)) {
					listaEventi = getEventiDocumentiInScadenza(searchEventiDTO, utente.getId(), utente.getIdUfficio(), utente.getFcDTO(), utente.getIdAoo());
					if (!CollectionUtils.isEmptyOrNull(listaEventi)) {
						final List<String> docTitleList = new ArrayList<>();
						for (final Evento evento : listaEventi) {
							docTitleList.add(evento.getDocumentTitle());
						}
						if (!CollectionUtils.isEmptyOrNull(docTitleList)) {
							final HashMap<String, Integer> statoNotificheInScadenza = notificaUtenteDAO.checkNotificaCalendarioScadenza(utente.getId(), docTitleList, con);
							if (statoNotificheInScadenza != null) {
								for (final Evento evento : listaEventi) {
									if (statoNotificheInScadenza.containsKey(evento.getDocumentTitle())) {
										if (statoNotificheInScadenza.get(evento.getDocumentTitle()).equals(2)) {
											evento.setStatoNotificaCalendario(2);
										}
										evento.setDaLeggere(false);
									} else {
										evento.setDaLeggere(true);
									}
								}
							}
						}
					}
				} else {
					searchEventiDTO.getTipiEvento().clear();
					searchEventiDTO.getTipiEvento().add(tipoEnum);
					listaEventi = calendarioDAO.getEventi(searchEventiDTO, utente.getId(), utente.getIdUfficio(), utente.getIdRuolo(), utente.getIdAoo(), idEnte, true, con);
					if (!CollectionUtils.isEmptyOrNull(listaEventi)) {
						for (final Evento evento : listaEventi) {
							evento.setDaLeggere(StatoNotificaUtenteEnum.NOTIFICA_DA_LEGGERE.getIdStato().equals(evento.getStatoNotificaCalendario()));
						}
					}
				}
				m.put(tipoEnum, listaEventi);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException("Si è verificato un errore durante il recupero degli eventi del calendario.", e);
		}

		return m;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICalendarioFacadeSRV#getEventiForCalendarioHomepage(it.ibm.red.business.dto.SearchEventiDTO,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public Map<EventoCalendarioEnum, List<Evento>> getEventiForCalendarioHomepage(final SearchEventiDTO searchEventiDTO, final UtenteDTO utente, final Integer idEnte,
			final Connection con) {
		final EnumMap<EventoCalendarioEnum, List<Evento>> m = new EnumMap<>(EventoCalendarioEnum.class);

		try {
			List<EventoCalendarioEnum> listaTipiDaRicercare = null;

			if (searchEventiDTO.getTipiEvento() != null && !searchEventiDTO.getTipiEvento().isEmpty()) {
				listaTipiDaRicercare = new ArrayList<>(searchEventiDTO.getTipiEvento());
			} else {
				// cerca tutti i tipi di evento
				listaTipiDaRicercare = Arrays.asList(EventoCalendarioEnum.values());
				searchEventiDTO.setTipiEvento(new ArrayList<>());
			}

			// cerca i tipi evento presenti in lista
			List<Evento> listaEventi = null;
			for (final EventoCalendarioEnum tipoEnum : listaTipiDaRicercare) {
				if (EventoCalendarioEnum.SCADENZA.equals(tipoEnum)) {
					listaEventi = getEventiDocumentiInScadenza(searchEventiDTO, utente.getId(), utente.getIdUfficio(), utente.getFcDTO(), utente.getIdAoo());
					if (!CollectionUtils.isEmptyOrNull(listaEventi)) {
						final List<String> docTitleList = new ArrayList<>();
						for (final Evento evento : listaEventi) {
							docTitleList.add(evento.getDocumentTitle());
						}
						if (!CollectionUtils.isEmptyOrNull(docTitleList)) {
							final HashMap<String, Integer> statoNotificheInScadenza = notificaUtenteDAO.checkNotificaCalendarioScadenza(utente.getId(), docTitleList, con);
							if (statoNotificheInScadenza != null) {
								for (final Evento evento : listaEventi) {
									if (statoNotificheInScadenza.containsKey(evento.getDocumentTitle())) {
										if (statoNotificheInScadenza.get(evento.getDocumentTitle()).equals(2)) {
											evento.setStatoNotificaCalendario(2);
										}
										evento.setDaLeggere(false);
									} else {
										evento.setDaLeggere(true);
									}
								}
							}
						}
					}
				} else {
					searchEventiDTO.getTipiEvento().clear();
					searchEventiDTO.getTipiEvento().add(tipoEnum);
					listaEventi = calendarioDAO.getEventiHomepage(searchEventiDTO, utente.getId(), utente.getIdUfficio(), utente.getIdRuolo(), utente.getIdAoo(), idEnte, true,
							con);
					if (!CollectionUtils.isEmptyOrNull(listaEventi)) {
						for (final Evento evento : listaEventi) {
							evento.setDaLeggere(StatoNotificaUtenteEnum.NOTIFICA_DA_LEGGERE.getIdStato().equals(evento.getStatoNotificaCalendario()));
						}
					}
				}
				m.put(tipoEnum, listaEventi);
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException("Si è verificato un errore durante il recupero degli eventi del calendario.", e);
		}

		return m;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICalendarioFacadeSRV#getEventiForCalendario(it.ibm.red.business.dto.SearchEventiDTO,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.Integer).
	 */
	@Override
	public Map<EventoCalendarioEnum, List<Evento>> getEventiForCalendario(final SearchEventiDTO searchEventiDTO, final UtenteDTO utente, final Integer idEnte) {
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			return getEventiForCalendario(searchEventiDTO, utente, idEnte, connection);
		} catch (final SQLException e) {
			LOGGER.error(e);
			throw new RedException("Si è verificato un errore di connessione verso il database durante il recupero degli eventi del calendario.", e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICalendarioFacadeSRV#getEventiDocumentiInScadenza(it.ibm.red.business.dto.SearchEventiDTO,
	 *      java.lang.Long, java.lang.Long,
	 *      it.ibm.red.business.dto.FilenetCredentialsDTO, java.lang.Long).
	 */
	@Override
	public List<Evento> getEventiDocumentiInScadenza(final SearchEventiDTO searchEventiDTO, final Long idUtente, final Long idUfficio, final FilenetCredentialsDTO fcUtente,
			final Long idAoo) {
		List<Evento> eventiDocScadenza = new ArrayList<>();
		final List<String> documentTitleList = new ArrayList<>();
		FilenetPEHelper fpeh = null;

		try {
			// inizializzo pe helper
			fpeh = new FilenetPEHelper(fcUtente);

			// ricerco documenti su coda DA LAVORARE => non serve differenziare per UCB in
			// quanto la count prende in considerazione tutti i documenti nella coda Filenet
			// Da Lavorare, anche quelli nella coda logica In Lavorazione
			final VWQueueQuery vwqqDaLavorare = fpeh.getWorkFlowsByWobQueueFilterRed(null, DocumentQueueEnum.DA_LAVORARE, null, idUfficio, Arrays.asList(idUtente),
					fcUtente.getIdClientAoo(), null, null, null, null, null, searchEventiDTO.getDataInizio(), searchEventiDTO.getDataFine(), false);

			documentTitleList.addAll(fpeh.getIdDocumenti(vwqqDaLavorare));

			// ricerco documenti su coda IN SOSPESO
			final VWQueueQuery vwqqSospeso = fpeh.getWorkFlowsByWobQueueFilterRed(null, DocumentQueueEnum.SOSPESO, null, idUfficio, Arrays.asList(idUtente),
					fcUtente.getIdClientAoo(), null, null, null, null, null, searchEventiDTO.getDataInizio(), searchEventiDTO.getDataFine(), false);

			documentTitleList.addAll(fpeh.getIdDocumenti(vwqqSospeso));

			// ricerco documenti su coda LIBRO FIRMA
			final VWQueueQuery vwqqLibroFirma = fpeh.getWorkFlowsByWobQueueFilterRed(null, DocumentQueueEnum.NSD, null, idUfficio, Arrays.asList(idUtente),
					fcUtente.getIdClientAoo(), null, null, null, null, null, searchEventiDTO.getDataInizio(), searchEventiDTO.getDataFine(), false);

			documentTitleList.addAll(fpeh.getIdDocumenti(vwqqLibroFirma));
		} catch (final Exception e) {
			LOGGER.error(e);
		} finally {
			if (fpeh != null) {
				fpeh.logoff();
			}
		}

		if (!documentTitleList.isEmpty()) {
			final IFilenetCEHelper fceh = FilenetCEHelperProxy.newInstance(fcUtente, idAoo);
			final DocumentSet documentiInScadenzaFN = fceh.getDocumentsForMasters(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documentTitleList);
			final Collection<MasterDocumentRedDTO> cMasters = TrasformCE.transform(documentiInScadenzaFN, TrasformerCEEnum.FROM_DOCUMENTO_TO_GENERIC_DOC);

			final Iterator<MasterDocumentRedDTO> it = cMasters.iterator();
			while (it.hasNext()) {
				final MasterDocumentRedDTO doc = it.next();

				String titolo = "";
				if (doc.getAnnoProtocollo() != null && doc.getNumeroProtocollo() != null) {
					titolo = DOCUMENTO_LABEL + "Prot. " + doc.getNumeroProtocollo() + "/" + doc.getAnnoProtocollo() + SCADENZA_LABEL
							+ DateUtils.dateToString(doc.getDataScadenza(), true);
				} else {
					titolo = DOCUMENTO_LABEL + "Doc. " + doc.getNumeroDocumento() + " - " + doc.getAnnoDocumento() + SCADENZA_LABEL
							+ DateUtils.dateToString(doc.getDataScadenza(), true);
				}
				// costruisco l'evento di tipo documento in scadenza
				if (doc.getDataScadenza() != null) {
					final Evento evento = new Evento();
					evento.setDataScadenza(doc.getDataScadenza());
					evento.setDataInizio(doc.getDataScadenza());
					final TipoCalendario tipo = new TipoCalendario();
					evento.setTipoCalendario(tipo);
					evento.getTipoCalendario().setTipoEnum(EventoCalendarioEnum.SCADENZA);
					evento.setTitolo(titolo);
					evento.setDocumentTitle(doc.getDocumentTitle());
					evento.setNumeroDocumento(doc.getNumeroDocumento());
					evento.setIdCategoriaDocumento(doc.getIdCategoriaDocumento());
					eventiDocScadenza.add(evento);
				}
			}
		}

		if (searchEventiDTO.getDescrizioneLike() != null) {
			eventiDocScadenza = eventiDocScadenza.stream().filter(evt -> evt.getTitolo().toUpperCase().contains(searchEventiDTO.getDescrizioneLike().toUpperCase()))
					.collect(Collectors.toList());
		}

		return eventiDocScadenza;
	}

	/**
	 * Recupera i nodi per essere visualizzati nella maschera di modifica
	 * dell'evento.
	 * 
	 * @param ids
	 * @param idAoo
	 * @return
	 * @throws ConnectionException
	 * @throws DAOException
	 * @throws ConfigurationException
	 */
	@Override
	public List<Nodo> getNodiByIds(final List<Integer> ids, final int idAoo) {
		Connection connection = null;

		final List<Nodo> list = new ArrayList<>();
		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			for (final Integer id : ids) {
				final Nodo nodo = nodoDAO.getNodo(id.longValue(), connection);
				list.add(nodo);
			}

			return list;

		} catch (final SQLException e) {
			LOGGER.error(ERRORE_NEL_RAGGIUNGERE_LA_CONNESSIONE + e.getMessage(), e);
			throw new RedException(e.getMessage());
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICalendarioFacadeSRV#getNews(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.Integer, java.lang.Integer).
	 */
	@Override
	public Collection<EventoDTO> getNews(final UtenteDTO utente, final Integer nDaysBefore, final Integer nDaysAfter) {
		final Date today = new Date();
		final Date dataInizio = DateUtils.addDay(today, -nDaysBefore);
		final Date dataFine = DateUtils.addDay(today, nDaysAfter);
		return getEventiFromDb(dataInizio, dataFine, null, EventoCalendarioEnum.NEWS, utente.getId(), utente.getIdUfficio(), utente.getIdRuolo(), utente.getIdAoo(), false);
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICalendarioFacadeSRV#ricercaEventi(java.util.Date,
	 *      java.util.Date, java.lang.String,
	 *      it.ibm.red.business.enums.EventoCalendarioEnum, java.lang.Long,
	 *      java.lang.Long, java.lang.Long, java.lang.Long,
	 *      it.ibm.red.business.dto.FilenetCredentialsDTO).
	 */
	@Override
	public Map<Integer, Collection<EventoDTO>> ricercaEventi(final Date dataInizio, final Date dataFine, final String titolo, final EventoCalendarioEnum tipoevento,
			final Long idUtente, final Long idUfficio, final Long idRuolo, final Long idAoo, final FilenetCredentialsDTO fcUtente) {
		final Collection<EventoDTO> newestDocsInScadenza = new ArrayList<>();
		Collection<EventoDTO> resultEventiFromRicerca = new ArrayList<>();
		final Collection<String> documenTitles = new ArrayList<>();
		final HashMap<String, EventoDTO> idEventoMap = new HashMap<>();
		final HashMap<Integer, Collection<EventoDTO>> output = new HashMap<>();
		try {

//			<-- Refresh di TUTTI i documenti in Scadenza -->
			final Collection<EventoDTO> allDocScadenzaFromPe = getDocInScadenzaFromPe(idUtente, idUfficio, fcUtente);
			for (final EventoDTO e : allDocScadenzaFromPe) {
				documenTitles.add(e.getDocumentTitle());
				idEventoMap.put(e.getDocumentTitle(), new EventoDTO(e));
			}

			Collection<MasterDocumentRedDTO> docScadenza = new ArrayList<>();
			if (!documenTitles.isEmpty()) {
				final IFilenetCEHelper fceh = FilenetCEHelperProxy.newInstance(fcUtente, idAoo);
				final DocumentSet allDocScadenzaFromCe = fceh.getDocumentsForMasters(pp.getParameterByKey(PropertiesNameEnum.DOCUMENTO_CLASSNAME_FN_METAKEY), documenTitles);
				docScadenza = TrasformCE.transform(allDocScadenzaFromCe, TrasformerCEEnum.FROM_DOCUMENTO_TO_GENERIC_DOC);
			}

			for (final MasterDocumentRedDTO doc : docScadenza) {

				final EventoDTO event = idEventoMap.get(doc.getDocumentTitle());

				if (doc.getDataScadenza() == null) {
					doc.setDataScadenza(event.getDataScadenza());
				}

				String titoloDoc = "";
				if (doc.getAnnoProtocollo() != null && doc.getNumeroProtocollo() != null) {
					titoloDoc = DOCUMENTO_LABEL + "Prot. " + doc.getNumeroProtocollo() + "/" + doc.getAnnoProtocollo() + SCADENZA_LABEL
							+ DateUtils.dateToString(doc.getDataScadenza(), true);
				} else {
					titoloDoc = DOCUMENTO_LABEL + "Doc. " + doc.getNumeroDocumento() + " - " + doc.getAnnoDocumento() + SCADENZA_LABEL
							+ DateUtils.dateToString(doc.getDataScadenza(), true);
				}

				event.setTitolo(titoloDoc);

				newestDocsInScadenza.add(event);
			}
//			<-- Fine refresh doc in Scadenza -->

//			<-- Ricerca degli eventi sul Db-->
			if (!EventoCalendarioEnum.SCADENZA.equals(tipoevento)) {
				resultEventiFromRicerca = getEventiFromDb(dataInizio, dataFine, titolo, tipoevento, idUtente, idUfficio, idRuolo, idAoo, false);
			}
//			<-- Fine Ricerca-->

//			<-- Filtro dei doc provenineti da Pe/Ce-->
			if (EventoCalendarioEnum.SCADENZA.equals(tipoevento) || tipoevento == null) {
				for (final EventoDTO doc : newestDocsInScadenza) {

					if (DateUtils.beetwenDate(dataInizio, dataFine, doc.getDataScadenza()) && StringUtils.likeInsensitive(doc.getTitolo(), titolo)) {
						resultEventiFromRicerca.add(doc);
					}
				}
			}
//			<-- Fine Filtro-->			

			// Preparo due liste separate da resitutire al chiamante
			output.put(1, allDocScadenzaFromPe);
			output.put(2, resultEventiFromRicerca);

		} catch (final Exception e) {
			LOGGER.error("Errore durante la ricerca degli Eventi", e);
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICalendarioFacadeSRV#getDocInScadenzaFromPe(java.lang.Long,
	 *      java.lang.Long, it.ibm.red.business.dto.FilenetCredentialsDTO).
	 */
	@Override
	public Collection<EventoDTO> getDocInScadenzaFromPe(final Long idUtente, final Long idUfficio, final FilenetCredentialsDTO fcUtente) {
		final Collection<EventoDTO> output = new ArrayList<>();
		FilenetPEHelper fpeh = null;

		Date dataScadenzaDa = null;
		// N.B. RED, cercando i documenti in scadenza nelle diverse code per il tab
		// "Attività ufficiali" del Calendario, in realtà cerca documenti
		// con la data scadenza valorizzata (quindi anche già scaduti), in quanto nella
		// query imposta una data scadenza maggiore di una data fake passata.
		// Qui viene replicato questo comportamento.
		dataScadenzaDa = Date.from(DateUtils.NULL_DATE.atZone(ZoneId.systemDefault()).toInstant());

		try {
			// inizializzo pe helper
			fpeh = new FilenetPEHelper(fcUtente);

			// ricerco documenti su coda DA LAVORARE => non serve differenziare per UCB in
			// quanto la count prende in considerazione tutti i documenti nella coda Filenet
			// Da Lavorare, anche quelli nella coda logica In Lavorazione
			final VWQueueQuery vwqqDaLavorare = fpeh.getWorkFlowsByWobQueueFilterRed(null, DocumentQueueEnum.DA_LAVORARE, null, idUfficio, Arrays.asList(idUtente),
					fcUtente.getIdClientAoo(), null, null, null, null, null, dataScadenzaDa, null, false);

			output.addAll(TrasformPE.transform(vwqqDaLavorare, TrasformerPEEnum.FROM_WF_TO_EVENTO, null));

			// ricerco documenti su coda IN SOSPESO
			final VWQueueQuery vwqqSospeso = fpeh.getWorkFlowsByWobQueueFilterRed(null, DocumentQueueEnum.SOSPESO, null, idUfficio, Arrays.asList(idUtente),
					fcUtente.getIdClientAoo(), null, null, null, null, null, dataScadenzaDa, null, false);

			output.addAll(TrasformPE.transform(vwqqSospeso, TrasformerPEEnum.FROM_WF_TO_EVENTO, null));

			// ricerco documenti su coda LIBRO FIRMA
			final VWQueueQuery vwqqLibroFirma = fpeh.getWorkFlowsByWobQueueFilterRed(null, DocumentQueueEnum.NSD, null, idUfficio, Arrays.asList(idUtente),
					fcUtente.getIdClientAoo(), null, null, null, null, null, dataScadenzaDa, null, false);

			output.addAll(TrasformPE.transform(vwqqLibroFirma, TrasformerPEEnum.FROM_WF_TO_EVENTO, null));

		} catch (final Exception e) {
			LOGGER.error(e);
		} finally {
			if (fpeh != null) {
				fpeh.logoff();
			}
		}

		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICalendarioFacadeSRV#getEventiFromDb(java.util.Date,
	 *      java.util.Date, java.lang.String,
	 *      it.ibm.red.business.enums.EventoCalendarioEnum, java.lang.Long,
	 *      java.lang.Long, java.lang.Long, java.lang.Long, java.lang.Boolean).
	 */
	@Override
	public Collection<EventoDTO> getEventiFromDb(final Date dataInizio, final Date dataFine, final String titolo, final EventoCalendarioEnum tipoevento, final Long idUtente,
			final Long idUfficio, final Long idRuolo, final Long idAoo, final Boolean bForCalendar) {
		final Collection<EventoDTO> output = new ArrayList<>();
		List<Evento> toTrasform = new ArrayList<>();
		int idEnte = 0;
		Connection connection = null;

		final SearchEventiDTO paramsRicercaEventi = new SearchEventiDTO();
		paramsRicercaEventi.setDataInizio(dataInizio);
		paramsRicercaEventi.setDataFine(dataFine);
		paramsRicercaEventi.setTipiEvento(new ArrayList<>());
		paramsRicercaEventi.getTipiEvento().add(EventoCalendarioEnum.COMUNICAZIONE);
		paramsRicercaEventi.getTipiEvento().add(EventoCalendarioEnum.NEWS);
		paramsRicercaEventi.getTipiEvento().add(EventoCalendarioEnum.UTENTE);

		if (tipoevento != null && !EventoCalendarioEnum.SCADENZA.equals(tipoevento)) {
			paramsRicercaEventi.getTipiEvento().clear();
			paramsRicercaEventi.getTipiEvento().add(tipoevento);
		}

		if (!StringUtils.isNullOrEmpty(titolo)) {
			paramsRicercaEventi.setDescrizioneLike(titolo);
		}

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			final Aoo aoo = aooDAO.getAoo(idAoo, connection);
			if (aoo != null && aoo.getEnte() != null) {
				idEnte = aoo.getEnte().getIdEnte();
			}

			toTrasform = calendarioDAO.getEventi(paramsRicercaEventi, idUtente, idUfficio, idRuolo, idAoo, idEnte, false, connection);

			for (final Evento e : toTrasform) {
				Date dScadenza = e.getDataScadenza();
				if (bForCalendar != null && bForCalendar) {
					dScadenza = DateUtils.setDateTo2359(dScadenza);
				}

				output.add(new EventoDTO(e.getIdEvento(), e.getDataInizio(), dScadenza, e.getTitolo(), e.getTipoCalendario().getTipoEnum(), e.getContenuto(),
						e.getLinkEsterno(), null, e.getNomeAllegato(), e.getEstensioneAllegato(), e.getMimeTypeAllegato(), e.getIdUtente(), e.getIdRuoliCategoriaTarget(),
						e.getIdNodiTarget(), e.getDescrizioneNodi()));
			}

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero degli eventi dal DB", e);
		} finally {
			closeConnection(connection);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ICalendarioFacadeSRV#getAllegatoEvento(java.lang.Integer).
	 */
	@Override
	public byte[] getAllegatoEvento(final Integer idEvento) {
		byte[] allegato = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			allegato = calendarioDAO.getAllegatoById(idEvento, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dell'allegato ", e);
		} finally {
			closeConnection(connection);
		}

		return allegato;
	}

	/**
	 * @see it.ibm.red.business.service.ICalendarioSRV#getCountEventiForCalendarioHomepage(it.ibm.red.business.dto.SearchEventiDTO,
	 *      it.ibm.red.business.dto.UtenteDTO, java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public Integer getCountEventiForCalendarioHomepage(final SearchEventiDTO searchEventiDTO, final UtenteDTO utente, final Integer idEnte, final Connection con) {
		Integer contatoreOutput = 0;

		try {
			List<EventoCalendarioEnum> listaTipiDaRicercare = null;

			if (searchEventiDTO.getTipiEvento() != null && !searchEventiDTO.getTipiEvento().isEmpty()) {
				listaTipiDaRicercare = new ArrayList<>(searchEventiDTO.getTipiEvento());
			} else {
				// cerca tutti i tipi di evento
				listaTipiDaRicercare = Arrays.asList(EventoCalendarioEnum.values());
				searchEventiDTO.setTipiEvento(new ArrayList<>());
			}

			// cerca i tipi evento presenti in lista
			List<Evento> listaEventi = null;
			for (final EventoCalendarioEnum tipoEnum : listaTipiDaRicercare) {
				if (EventoCalendarioEnum.SCADENZA.equals(tipoEnum)) {
					listaEventi = getEventiDocumentiInScadenza(searchEventiDTO, utente.getId(), utente.getIdUfficio(), utente.getFcDTO(), utente.getIdAoo());
					if (!CollectionUtils.isEmptyOrNull(listaEventi)) {
						final List<String> docTitleList = new ArrayList<>();
						for (final Evento evento : listaEventi) {
							docTitleList.add(evento.getDocumentTitle());
						}
						if (!CollectionUtils.isEmptyOrNull(docTitleList)) {
							final HashMap<String, Integer> statoNotificheInScadenza = notificaUtenteDAO.checkNotificaCalendarioScadenza(utente.getId(), docTitleList, con);
							if (statoNotificheInScadenza != null) {
								for (final Evento evento : listaEventi) {
									if (statoNotificheInScadenza.containsKey(evento.getDocumentTitle())) {
										if (statoNotificheInScadenza.get(evento.getDocumentTitle()).equals(2)) {
											evento.setStatoNotificaCalendario(2);
										}
										evento.setDaLeggere(false);
									} else {
										evento.setDaLeggere(true);
										contatoreOutput = contatoreOutput + 1;
									}
								}
							}
						}
					}
				} else {
					searchEventiDTO.getTipiEvento().clear();
					searchEventiDTO.getTipiEvento().add(tipoEnum);
					contatoreOutput += calendarioDAO.getCountEventiHomepage(searchEventiDTO, utente.getId(), utente.getIdUfficio(), utente.getIdRuolo(), utente.getIdAoo(),
							idEnte, con);
				}
			}
		} catch (final Exception e) {
			LOGGER.error(e);
			throw new RedException("Si è verificato un errore durante il recupero della count degli eventi del calendario.", e);
		}

		return contatoreOutput;
	}
}
