package it.ibm.red.business.dto;

import java.util.Date;

import it.ibm.red.business.enums.IconaExtensionEnum;
import it.ibm.red.business.enums.ValueMetadatoVerificaFirmaEnum;

/**
 * Una lista di questi oggetti va associata ad ogni documento, 
 * in particolare agli oggetti DetailDocumentRedDTO e AllegatoDTO.
 *
 * @author SLac
 */
public class VersionDTO extends VerificaFirmaDTO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Sul CE e' l'ID.
	 */
	private final String guid;
	
	/**
	 * Data del checkin del documento .
	 */
	private final Date dateCheckedIn;
	
	/**
	 * 
	 */
	private final String mimeType;
	
	/**
	 * 
	 */
	private boolean currentVersion;
	
	/**
	 * 
	 */
	private final boolean frozenVersion;
	
	/**
	 * 
	 */
	private final String className;
	
	/**
	 * 
	 */
	private final String fileName;
	
	/**
	 * 
	 */
	private String documentTitle;
	
	/**
	 * Numero della versione.
	 */
	private Integer versionNumber;
	
	/**
	 * Icona.
	 */
	private IconaExtensionEnum iconaExt;
	
	/**
	 * Creatore del documento .
	 */
	private String creatore; 

	/**
	 * @param guid
	 * @param dateCheckedIn
	 * @param mimeType
	 * @param currentVersion
	 * @param frozenVersion
	 * @param className
	 * @param fileName
	 * @param valoreVerificaFirma
	 * @param visualizzaInfoVerificaFirma
	 */
	public VersionDTO(final String guid, final Date dateCheckedIn, final String mimeType, 
			final boolean currentVersion, final boolean frozenVersion, final String className, final String fileName, 
			final ValueMetadatoVerificaFirmaEnum valoreVerificaFirma, final boolean visualizzaInfoVerificaFirma) {
		this.guid = guid;
		this.dateCheckedIn = dateCheckedIn;
		this.mimeType = mimeType;
		this.currentVersion = currentVersion;
		this.frozenVersion = frozenVersion;
		this.className = className;
		this.fileName = fileName;
		this.valoreVerificaFirma = valoreVerificaFirma;
		this.visualizzaInfoVerificaFirma = visualizzaInfoVerificaFirma;
		this.iconaExt = IconaExtensionEnum.get(fileName);
	}

	/**
	 * @param guid
	 * @param dateCheckedIn
	 * @param mimeType
	 * @param currentVersion
	 * @param frozenVersion
	 * @param className
	 * @param fileName
	 * @param valoreVerificaFirma
	 * @param visualizzaInfoVerificaFirma
	 */
	public VersionDTO(final String guid, final Date dateCheckedIn, final String mimeType, final boolean currentVersion, 
			final boolean frozenVersion, final String className, final String fileName, 
			final ValueMetadatoVerificaFirmaEnum valoreVerificaFirma, final boolean visualizzaInfoVerificaFirma, final String creatore) {
		this.guid = guid;
		this.dateCheckedIn = dateCheckedIn;
		this.mimeType = mimeType;
		this.currentVersion = currentVersion;
		this.frozenVersion = frozenVersion;
		this.className = className;
		this.fileName = fileName;
		this.valoreVerificaFirma = valoreVerificaFirma;
		this.visualizzaInfoVerificaFirma = visualizzaInfoVerificaFirma;
		this.iconaExt = IconaExtensionEnum.get(fileName);
		this.creatore = creatore;
	}

	/* GET & SET **********************************************************/

	/**
	 * Restituisce il numero della versione.
	 * @return versionNumber
	 */
	public Integer getVersionNumber() {
		return versionNumber;
	}

	/**
	 * Imposta il numero della versione.
	 * @param inVersionNumber
	 */
	public void setVersionNumber(final Integer inVersionNumber) {
		this.versionNumber = inVersionNumber;
	}

	/**
	 * @return guid
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * @return dateCheckedIn
	 */
	public Date getDateCheckedIn() {
		return dateCheckedIn;
	}

	/**
	 * @return mimeType
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * Restituisce true se questa è la versione corrente.
	 * @return currentVersion
	 */
	public boolean isCurrentVersion() {
		return currentVersion;
	}

	/**
	 * Restituisce true se questa è una versione "congelata".
	 * @return frozenVersion
	 */
	public boolean isFrozenVersion() {
		return frozenVersion;
	}

	/**
	 * Restituisce il FN Class Name.
	 * @return className
	 */
	public String getClassName() {
		return className;
	}

	/**
	 * @return documentTitle
	 */
	public String getDocumentTitle() {
		return documentTitle;
	}

	/**
	 * Imposta il document title.
	 * @param inDocumentTitle
	 */
	public void setDocumentTitle(final String inDocumentTitle) {
		this.documentTitle = inDocumentTitle;
	}

	/**
	 * Restituisce il nome del file.
	 * @return fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 *  Restituisce un oggetto {@link IconaExtensionEnum} che
	 *  definisce l'icona per l'estensione del documento.
	 * @return iconaExt
	 */
	public IconaExtensionEnum getIconaExt() {
		return iconaExt;
	}

	/**
	 * Imposta un oggetto {@link IconaExtensionEnum} che
	 * definisce l'icona per l'estensione del documento.
	 * @param inIconaExt
	 */
	public void setIconaExt(final IconaExtensionEnum inIconaExt) {
		this.iconaExt = inIconaExt;
	}

	/**
	 * Restituisce l'utente creatore.
	 * @return creatore
	 */
	public String getCreatore() {
		return creatore;
	}

	/**
	 * Imposta l'utente creatore.
	 */
	public void setCreatore(final String inCreatore) {
		this.creatore = inCreatore;
	}

	/**
	 * Imposta questa versione come versione corrente.
	 * @param currentVersion
	 */
	public void setCurrentVersion(final boolean currentVersion) {
		this.currentVersion = currentVersion;
	}
}