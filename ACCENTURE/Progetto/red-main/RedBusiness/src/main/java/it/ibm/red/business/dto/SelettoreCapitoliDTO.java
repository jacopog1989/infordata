/**
 * 
 */
package it.ibm.red.business.dto;

import java.util.List;

import it.ibm.red.business.enums.LookupTablePresentationModeEnum;
import it.ibm.red.business.enums.TipoDocumentoModeEnum;
import it.ibm.red.business.enums.TipoMetadatoEnum;

/**
 * @author APerquoti
 *
 */
public class SelettoreCapitoliDTO extends MetadatoDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -6717350337820843906L;

	/**
	 * Prefisso WDG.
	 */
	private static final String WDG_PREFIX = "WDG_";

	/**
	 * Valori disponibili nella lookup table.
	 */
	private List<SelectItemDTO> lookupValues; 
	
	/**
	 * Valore selezionato tra quelli disponibili nella lookup table.
	 */
	private SelectItemDTO lookupValueSelected;
	
	/**
	 * Valore selezionato tra quelli disponibili nella lookup table.
	 */
	private String lookupWidgetVar;
	
	
	/**
	 * Costruttore del selettore.
	 * 
	 * @param name
	 *            nome del selettore
	 * @param displayName
	 *            display name del selettore
	 * @param inLookupTableModeEnum
	 *            modalità di visualizzazione del selettore
	 * @param visibility
	 *            visibilità del selettore capitoli
	 * @param editability
	 *            editabilità del selettore capitoli
	 * @param obligatoriness
	 *            obbligatorietà del selettore capitoli
	 * @param flagOut
	 *            flag out del metadato
	 * @param inLookupValues
	 *            valori del selettore
	 */
	public SelettoreCapitoliDTO(final String name, final String displayName, final LookupTablePresentationModeEnum inLookupTableModeEnum, final TipoDocumentoModeEnum visibility,
			final TipoDocumentoModeEnum editability, final TipoDocumentoModeEnum obligatoriness, final Boolean flagOut, final List<SelectItemDTO> inLookupValues) {
		super(null, name, TipoMetadatoEnum.LOOKUP_TABLE, displayName, null, inLookupTableModeEnum, null, visibility, editability, obligatoriness, false, flagOut, null, false);
		this.lookupValues = inLookupValues;
		this.lookupWidgetVar = new StringBuilder().append(WDG_PREFIX).append(name).toString();
	}

	/**
	 * Restituisce la lista dei valori disponibili per la lookup Table.
	 * 
	 * @return lista valori validi
	 */
	public List<SelectItemDTO> getLookupValues() {
		return lookupValues;
	}

	/**
	 * Imposta la lista dei valori disponibili per la lookup Table.
	 * 
	 * @param lookupValues
	 *            valori da impostare
	 */
	public void setLookupValues(final List<SelectItemDTO> lookupValues) {
		this.lookupValues = lookupValues;
	}

	/**
	 * Restituisce il valore selezionato da quelli disponibili nella lista dei
	 * valori disponibili: {@link #lookupValues}.
	 * 
	 * @return valore selezionato
	 */
	public SelectItemDTO getLookupValueSelected() {
		return lookupValueSelected;
	}

	/**
	 * Imposta il valore selezionato da quelli disponibili nella lista dei valori
	 * disponibili: {@link #lookupValues}.
	 * 
	 * @param lookupValueSelected
	 *            valore selezionato da impostare
	 */
	public void setLookupValueSelected(final SelectItemDTO lookupValueSelected) {
		this.lookupValueSelected = lookupValueSelected;
	}

	/**
	 * Restituisce l'Id per la gestione del widget in Front-End.
	 * 
	 * @return id widget
	 */
	public String getLookupWidgetVar() {
		return lookupWidgetVar;
	}

	/**
	 * Imposta l'Id per la gestione del widget in Front-End.
	 * 
	 * @param lookupWidgetVar
	 *            id widget da impostare
	 */
	public void setLookupWidgetVar(final String lookupWidgetVar) {
		this.lookupWidgetVar = lookupWidgetVar;
	}

	/**
	 * Restituisce la descrizione del valore selezionato se non <code> null </code>,
	 * restitusce <code> null </code> altrimenti.
	 * 
	 * @see it.ibm.red.business.dto.MetadatoDTO#getValue4AttrExt()
	 */
	@Override
	public String getValue4AttrExt() {
		return (getLookupValueSelected() != null) ? getLookupValueSelected().getDescription() : null;
	}
	
}
