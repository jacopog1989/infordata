package it.ibm.red.business.helper.notifiche;

import java.util.Map;

import it.ibm.red.business.dto.EmailDTO;
import it.ibm.red.business.persistence.model.CodaEmail;

/**
 * Helper per la gestione delle notifiche
 */
public class NotificaHelper {
	

	/**
	 * Gestione stati mail.
	 */
	private IGestioneMailState gestioneMailState;
	

	/**
	 * Mittente.
	 */
	private String mittente;
	
	
	/**
	 * Costruttore notifica helper.
	 *
	 * @param mittente the mittente
	 */
	public  NotificaHelper(final String mittente) {
		this.mittente = mittente;
	}
    
    /**
     * Costruttore notifica helper.
     */
    public  NotificaHelper() {
    }
    
    /**
     * Getter gestione mail state.
     *
     * @return the gestione mail state
     */
    public IGestioneMailState getGestioneMailState() {
		return gestioneMailState;
	}

	/**
	 * Setter gestione mail state.
	 *
	 * @param gestioneMailState the new gestione mail state
	 */
	public void setGestioneMailState(final IGestioneMailState gestioneMailState) {
		this.gestioneMailState = gestioneMailState;
	}
	
	/**
	 * Getter next notifica email to update.
	 *
	 * @param notificaEmail the notifica email
	 * @param notifiche the notifiche
	 * @param reInvio the re invio
	 * @return the next notifica email to update
	 */
	public CodaEmail getNextNotificaEmailToUpdate(final CodaEmail notificaEmail, final Map<String, EmailDTO> notifiche, final boolean reInvio) {
		CodaEmail result = null;
		
		if (gestioneMailState != null) {
			result = gestioneMailState.getNextNotificaEmailToUpdate(this, notificaEmail, notifiche, reInvio);
		}
		
		return result;
	}

	/**
	 * Getter del mittente.
	 *
	 * @return the mittente
	 */
	public String getMittente() {
		return mittente;
	}

	/**
	 * Setter del mittente.
	 *
	 * @param mittente the new mittente
	 */
	public void setMittente(final String mittente) {
		this.mittente = mittente;
	}
	
}
