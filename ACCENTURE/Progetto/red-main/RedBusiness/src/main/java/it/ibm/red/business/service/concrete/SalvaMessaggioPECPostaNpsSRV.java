package it.ibm.red.business.service.concrete;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import it.ibm.red.business.dto.MessaggioPostaNpsDTO;
import it.ibm.red.business.dto.SalvaMessaggioPostaNpsContestoDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoMessaggioPostaNpsIngressoEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.utils.EmailUtils;

/**
 * Service che gestisce il salvataggio di un messaggio Pec.
 */
@Service
@Component
public class SalvaMessaggioPECPostaNpsSRV extends SalvaMessaggioPostaNpsAbstractSRV<MessaggioPostaNpsDTO> {

	/**
	 * Costante serial Version UID.
	 */
	private static final long serialVersionUID = -5145239662556047935L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(SalvaMessaggioPECPostaNpsSRV.class.getName());

	
	/**
	 * @see it.ibm.red.business.service.concrete.SalvaMessaggioInteropSRV#getMessageId(it.ibm.red.business.dto.SalvaMessaggioInteropContestoDTO).
	 */
	@Override
	protected String getMessageId(final SalvaMessaggioPostaNpsContestoDTO<MessaggioPostaNpsDTO> contesto) {
		String outMessageId = contesto.getRichiesta().getMessaggio().getMessageId();
		
		if (StringUtils.isBlank(outMessageId)) {
			try {
				final String[] mimeMessageId = contesto.getMessaggioMime().getHeader("Message-ID"); // Message-ID della busta di trasporto
				
				if (mimeMessageId.length > 0 && StringUtils.isNotBlank(mimeMessageId[0])) {
					outMessageId = mimeMessageId[0];
					
					if (!EmailUtils.validaMessageId(outMessageId)) {
						LOGGER.warn("Message-ID non valido: " + outMessageId);
						outMessageId = LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli() + "." + this.getClass().getModifiers() + "@gestioneemail.it";
						LOGGER.warn("Generato il Message-ID: - " + outMessageId);
					}
				}
			} catch (final MessagingException me) {
				LOGGER.error("Si è verificato un errore durante il recupero del Message-ID per la richiesta di elaborazione: " 
						+ contesto.getRichiesta().getIdCoda(), me);
			}
		}
		
		return outMessageId;
	}


	/**
	 * @see it.ibm.red.business.service.concrete.SalvaMessaggioInteropSRV#getMittente(it.ibm.red.business.dto.SalvaMessaggioInteropContestoDTO).
	 */
	@Override
	protected String getMittente(final SalvaMessaggioPostaNpsContestoDTO<MessaggioPostaNpsDTO> contesto) {
		String mittente = null;
		
		try {
			List<String> indirizziMittente = null;
			
			// Attributo "from" del messaggio MIME
			if (contesto.getMessaggioMime().getFrom() != null) {
				indirizziMittente = EmailUtils.getIndirizziMailDaAddresses(contesto.getMessaggioMime().getFrom());
				
				if (!CollectionUtils.isEmpty(indirizziMittente)) {
					mittente = indirizziMittente.get(0);
				}
				
			// Altrimenti, attributo "sender" del messaggio MIME
			} else if (contesto.getMessaggioMime().getSender() != null) {
				
				mittente = EmailUtils.getIndirizzoMailDaAddress(contesto.getMessaggioMime().getSender());
			
			} else {
				mittente = "NoSender@NoSender.com";
			}
		} catch (final MessagingException me) {
			LOGGER.error("Si è verificato un errore durante il recupero del mittente", me);
		}
		
		return mittente;
	}

	/**
	 * @see it.ibm.red.business.service.concrete.SalvaMessaggioPostaNpsAbstractSRV#getFolderDestinazioneMessaggio(
	 *      it.ibm.red.business.enums.TipoMessaggioPostaNpsIngressoEnum).
	 */
	@Override
	protected String getFolderDestinazioneMessaggio(final TipoMessaggioPostaNpsIngressoEnum tipoIngresso) {
		String folder = getPp().getParameterByKey(PropertiesNameEnum.FOLDER_MAIL_INBOX_FN_METAKEY);
		
		// In caso di protocollazione automatica, la mail viene direttamente inserita nel folder 'Archiviate'
		if (tipoIngresso != null && tipoIngresso.isProtocollazioneAutomatica()) {
			folder = getPp().getParameterByKey(PropertiesNameEnum.FOLDER_MAIL_ARCHIVIATE_FN_METAKEY);
		}
		
		return folder;
	}

	/**
	 * @see it.ibm.red.business.service.concrete.SalvaMessaggioPostaNpsAbstractSRV#isPec().
	 */
	@Override
	protected boolean isPec() {
		return true;
	}

}