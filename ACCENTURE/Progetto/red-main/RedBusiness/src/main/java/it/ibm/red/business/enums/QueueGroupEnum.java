package it.ibm.red.business.enums;

/**
 * Enum che definisce il tipo di coda.
 */
public enum QueueGroupEnum {

	/**
	 * Valore.
	 */
	UTENTE, 
	
	/**
	 * Valore.
	 */
	UFFICIO, 
	
	/**
	 * Valore.
	 */
	ARCHIVIO, 
	
	/**
	 * Valore.
	 */
	SERVIZIO,
	
	/**
	 * Valore.
	 */
	RECOGNIFORM;
}