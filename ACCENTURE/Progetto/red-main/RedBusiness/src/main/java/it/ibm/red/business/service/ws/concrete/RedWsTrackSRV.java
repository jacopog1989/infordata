package it.ibm.red.business.service.ws.concrete;

import java.sql.Connection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.ibm.red.business.dao.IRedWsTrackDAO;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.concrete.AbstractService;
import it.ibm.red.business.service.ws.IRedWsTrackSRV;

/**
 * 
 * @author a.dilegge
 *
 */
@Service
public class RedWsTrackSRV extends AbstractService implements IRedWsTrackSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 6088787481287153939L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RedWsTrackSRV.class.getName());
	
	/**
	 * DAO.
	 */
	@Autowired
	private IRedWsTrackDAO redWsTrackDAO;

	/**
	 * @see it.ibm.red.business.service.ws.facade.IRedWsTrackFacadeSRV#logRequest(java.lang.String,
	 *      java.lang.String, java.lang.String).
	 */
	@Override
	public Long logRequest(final String request, final String serviceName, final String remoteAddress) {
		
		Connection con = null;
		long idTrack = 0;
		
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			idTrack = redWsTrackDAO.insert(request, serviceName, remoteAddress, con);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
		} finally {
			closeConnection(con);
		}
		
		return idTrack;
	}

	/**
	 * @see it.ibm.red.business.service.ws.facade.IRedWsTrackFacadeSRV#logResponse(java.lang.Long,
	 *      java.lang.Integer, java.lang.String).
	 */
	@Override
	public void logResponse(final Long idTrack, final Integer esitoOperazione, final String response) {
		
		Connection con = null;
				
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			redWsTrackDAO.update(idTrack, esitoOperazione, response, con);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
		} finally {
			closeConnection(con);
		}
		
	}
}