package it.ibm.red.business.enums;
 
/**
 * @author VINGENITO
 * Enum che definisce la tipologia degli indirizzi mail.
 */
public enum TipologiaIndirizzoEmailEnum {

	/**
	 * Valore.
	 */
	PEC("PEC"),

	/**
	 * Valore.
	 */
	PEO("PEO");

	/**
	 * Tipologia mail.
	 */
	private String tipologiaEmail;
	
	/**
	 * Costruttore.
	 * @param inTipologiaEmail
	 */
	TipologiaIndirizzoEmailEnum(final String inTipologiaEmail) {
		tipologiaEmail = inTipologiaEmail;
	}
	
	/**
	 * Restituisce la tipologia mail.
	 * @return tipologia mail
	 */
	public String getTipologiaEmail() {
		return tipologiaEmail;
	}
	
	/**
	 * Imposta la tipologia mail.
	 * @param tipologiaEmail
	 */
	protected void setTipologiaEmail(final String tipologiaEmail) {
		this.tipologiaEmail = tipologiaEmail;
	}

	/**
	 * Restituisce l'enum associata alla pk.
	 * @param pk
	 * @return enum associata
	 */
	public static TipologiaIndirizzoEmailEnum get(final String pk) {
		TipologiaIndirizzoEmailEnum output = null;
		for (final TipologiaIndirizzoEmailEnum t:TipologiaIndirizzoEmailEnum.values()) {
			if (pk.equals(t.getTipologiaEmail())) {
				output = t;
				break;
			}
		}
		return output;
	}
}
