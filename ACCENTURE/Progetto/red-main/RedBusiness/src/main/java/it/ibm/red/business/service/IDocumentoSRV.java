package it.ibm.red.business.service;

import java.io.InputStream;

import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.service.facade.IDocumentoFacadeSRV;

/**
 * The Interface IDocumentoSRV.
 *
 * @author CPIERASC
 * 
 *         Servizio gestione documenti.
 */
public interface IDocumentoSRV extends IDocumentoFacadeSRV {
	
	/**
	 * Metodo per testare se un documento è in entrata o meno.
	 * 
	 * @param tipoProtocollo			identificativo tipologia protocollo
	 * @param momentoProtocollazione	identificativo momento protocollazione
	 * @return							esito test
	 */
	Boolean isEntrata(Integer tipoProtocollo, Integer momentoProtocollazione);
	
	/**
	 * Metodo per testare se un documento è in uscita o meno.
	 * 
	 * @param tipoProtocollo			identificativo tipologia protocollo
	 * @param momentoProtocollazione	identificativo momento protocollazione
	 * @return							esito test
	 */
	Boolean isUscita(Integer tipoProtocollo, Integer momentoProtocollazione);
	
	/**
	 * Richiede protocollo NPS in modo sincrono.
	 * @param wobNumber
	 * @param documentTitle
	 * @param idAOO
	 * @param idUtente
	 * @param idUfficio
	 * @param idRuolo
	 * @param isRiservato
	 * @return protocollo NPS
	 */
	ProtocolloNpsDTO richiediProtocolloNPSync(String wobNumber, String documentTitle, Integer idAOO, Long idUtente,Long idUfficio, Long idRuolo, 
			boolean isRiservato);

	/**
	 * Metodo per l'associazione del documento.
	 * @param documentTitle
	 * @param protocolloGuid
	 * @param nomeFile
	 * @param oggetto
	 * @param idAOO
	 * @param idUtente
	 * @param idUfficio
	 * @param idRuolo
	 * @param isPrincipale
	 * @param daInviare
	 * @param numeroAnnoProtocollo
	 * @param narUpload
	 * @param isProtocollazioneP7M
	 * @return true o false
	 */
	boolean associateDocumentoProtocolloToAsyncNps(String documentTitle, String protocolloGuid, String nomeFile,
			String oggetto, Integer idAOO, Long idUtente, Long idUfficio, Long idRuolo, boolean isPrincipale,
			boolean daInviare, String numeroAnnoProtocollo, int narUpload, boolean isProtocollazioneP7M);

	/**
	 * Metodo per l'upload del documento.
	 * @param contentIS
	 * @param documentTitle
	 * @param protocolloGuid
	 * @param contentType
	 * @param nomefile
	 * @param oggetto
	 * @param numeroAnnoProtocollo
	 * @param idAOO
	 * @param idUtente
	 * @param idUfficio
	 * @param idRuolo
	 * @return
	 */
	int uploadDocToAsyncNps(InputStream contentIS, String documentTitle, String protocolloGuid, String contentType, String nomefile, String oggetto, 
			String numeroAnnoProtocollo, Integer idAOO, Long idUtente, Long idUfficio, Long idRuolo);

	/**
	 * Spedisce il protocollo in uscita in modo asincrono.
	 * @param protocolloGuid
	 * @param infoProtocollo
	 * @param idAOO
	 * @param idUtente
	 * @param idUfficio
	 * @param idRuolo
	 * @param documentTitle
	 * @param numeroAnnoProtocollo
	 * @param dataSpedizione
	 * @return true o false
	 */
	boolean spedisciProtocolloUscitaAsync(String protocolloGuid, String infoProtocollo, Integer idAOO, Long idUtente,
			Long idUfficio, Long idRuolo, String documentTitle, String numeroAnnoProtocollo, String dataSpedizione);

	/**
	 * Aggiunge allacci in modo asincrono.
	 * @param idAOO
	 * @param idUtente
	 * @param idUfficio
	 * @param idRuolo
	 * @param documentTitle
	 * @param idProtocollo
	 * @param numeroAnnoProtocollo
	 * @param isRisposta
	 * @return true o false
	 */
	boolean aggiungiAllacciAsync(Integer idAOO, Long idUtente, Long idUfficio, Long idRuolo, String documentTitle,
			String idProtocollo, String numeroAnnoProtocollo, boolean isRisposta);
	
}