package it.ibm.red.business.enums;

import it.ibm.red.business.constants.Constants;

/**
 * The Enum CampoRicercaEnum.
 *
 * @author m.crescentini
 * 
 *         Enum per i possibili campi in cui cercare la stringa di ricerca della ricerca rapida.
 */
public enum CampoRicercaEnum {

	/**
	 * Tutti.
	 */
	ALL(null, null),
	
	/**
	 * Protocollo.
	 */
	PROTOCOLLO(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY, Constants.Ricerca.TIPO_CAMPO_RICERCA_NUMERICO),
	
	/**
	 * Numero.
	 */
	NUMERO(PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY, Constants.Ricerca.TIPO_CAMPO_RICERCA_NUMERICO),
	
	/**
	 * Oggetto.
	 */
	OGGETTO(PropertiesNameEnum.OGGETTO_METAKEY, Constants.Ricerca.TIPO_CAMPO_RICERCA_TESTO_LIKE),

	/**
	 * Barcode.
	 */
	BARCODE(PropertiesNameEnum.BARCODE_METAKEY, Constants.Ricerca.TIPO_CAMPO_RICERCA_TESTO_ESATTO);
	
	
	/**
	 * Chiave metadato.
	 */
	private PropertiesNameEnum chiaveMetadato;
	
	/**
	 * Tipologia ricerca.
	 */
	private String tipoRicerca;


	/**
	 * Costruttore.
	 * 
	 * @param inKey		chiave
	 * @param inLabel	label
	 */
	CampoRicercaEnum(final PropertiesNameEnum chiaveMetadato, final String tipoRicerca) {
		this.chiaveMetadato = chiaveMetadato;
		this.tipoRicerca = tipoRicerca;
	}


	/**
	 * Getter chiave.
	 * 
	 * @return chiave
	 */
	public PropertiesNameEnum getChiaveMetadato() {
		return chiaveMetadato;
	}


	/**
	 * @return the tipoRicerca
	 */
	public String getTipoRicerca() {
		return tipoRicerca;
	}

	/**
	 * Restituisce l'enum associata al value.
	 * @param typeValue
	 * @return CampoRicercaEnum, enum associata al value
	 */
	public static CampoRicercaEnum get(final String typeValue) {
		CampoRicercaEnum output = null;
		
		for (final CampoRicercaEnum e : CampoRicercaEnum.values()) {
			if (e.name().equalsIgnoreCase(typeValue)) {
				 output = e;
				 break;
			}
		}
		
		return output;
	}
	
}
