package it.ibm.red.business.service.ws.facade;

import java.io.Serializable;

import it.ibm.red.business.persistence.model.RedWsClient;
import it.ibm.red.webservice.model.documentservice.types.messages.RedAssociaFascicoloATitolarioType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedCambiaStatoFascicoloType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedCreaFascicoloType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedEliminaFascicoloType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedFascicolazioneDocumentoType;
import it.ibm.red.webservice.model.documentservice.types.messages.RedModificaFascicoloType;

/**
 * The Interface IFascicoloFacadeSRV.
 *
 * @author a.dilegge
 * 
 *         Facade fascicolo service da web service.
 */
public interface IFascicoloWsFacadeSRV extends Serializable {

	/**
	 * Fascicola il documento.
	 * @param client
	 * @param request
	 */
	void redFascicolazioneDocumento(RedWsClient client, RedFascicolazioneDocumentoType request);

	/**
	 * Elimina il fascicolo.
	 * @param client
	 * @param request
	 */
	void redEliminaFascicolo(RedWsClient client, RedEliminaFascicoloType request);

	/**
	 * Cambia lo stato del fascicolo.
	 * @param client
	 * @param request
	 */
	void redCambiaStatoFascicolo(RedWsClient client, RedCambiaStatoFascicoloType request);

	/**
	 * Modifica il fascicolo.
	 * @param client
	 * @param request
	 */
	void redModificaFascicolo(RedWsClient client, RedModificaFascicoloType request);

	/**
	 * Associa il fascicolo al titolario.
	 * @param client
	 * @param request
	 */
	void redAssociaFascicoloATitolario(RedWsClient client, RedAssociaFascicoloATitolarioType request);

	/**
	 * Crea il fascicolo.
	 * @param client
	 * @param request
	 * @return
	 */
	String redCreaFascicolo(RedWsClient client, RedCreaFascicoloType request);
	
}