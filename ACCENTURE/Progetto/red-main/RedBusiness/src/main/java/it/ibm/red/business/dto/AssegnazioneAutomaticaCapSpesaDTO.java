package it.ibm.red.business.dto;

import java.util.UUID;

/**
 * DTO che definisce un'assegnazione automatica per capitolo di spesa.
 */
public class AssegnazioneAutomaticaCapSpesaDTO extends AbstractDTO {

	/**
	 * La costante serial Version UID.
	 */
	private static final long serialVersionUID = 7481421380937929936L;
	
	/**
	 * Guid.
	 */
	private final String guid;
	
	/**
	 * Capitolo di spesa.
	 */
	private CapitoloSpesaDTO capitoloSpesa;
	
	/**
	 * Assegnatario.
	 */
	private AssegnatarioDTO assegnatario;
	
	/**
	 * Flag valida.
	 */
	private boolean valida;
	
	
	/**
	 * Costruttore di default.
	 */
	public AssegnazioneAutomaticaCapSpesaDTO() {
		super();
		this.guid = UUID.randomUUID().toString();
	}
	
	/**
	 * @param capitoloSpesa
	 * @param assegnatario
	 */
	public AssegnazioneAutomaticaCapSpesaDTO(final CapitoloSpesaDTO capitoloSpesa, final AssegnatarioDTO assegnatario) {
		super();
		this.guid = UUID.randomUUID().toString();
		this.capitoloSpesa = capitoloSpesa;
		this.assegnatario = assegnatario;
	}


	/**
	 * Ridefinizione di <code> java.lang.Object#hashCode() </code.
	 * 
	 * @see java.lang.Object#hashCode().
	 * @return hashcode generato
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((assegnatario == null) ? 0 : assegnatario.hashCode());
		result = prime * result + ((capitoloSpesa == null) ? 0 : capitoloSpesa.hashCode());
		result = prime * result + ((guid == null) ? 0 : guid.hashCode());
		return result;
	}


	/**
	 * Ridefinizione di <code> java.lang.Object#equals(java.lang.Object) </code>.
	 * 
	 * @see java.lang.Object#equals(java.lang.Object).
	 * @param obj
	 *            oggetto da comparare
	 * @return true se gli oggetti sono uguali, false altrimenti
	 */
	@Override
	public boolean equals(final Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AssegnazioneAutomaticaCapSpesaDTO)) {
			return false;
		}
		final AssegnazioneAutomaticaCapSpesaDTO other = (AssegnazioneAutomaticaCapSpesaDTO) obj;
		if (guid == null) {
			if (other.guid != null) {
				return false;
			}
		} else if (!guid.equals(other.guid)) {
			return false;
		}
		if (assegnatario == null) {
			if (other.assegnatario != null) {
				return false;
			}
		} else if (!assegnatario.equals(other.assegnatario)) {
			return false;
		}
		if (capitoloSpesa == null) {
			if (other.capitoloSpesa != null) {
				return false;
			}
		} else if (!capitoloSpesa.equals(other.capitoloSpesa)) {
			return false;
		}
		return true;
	}


	/**
	 * @return the guid
	 */
	public String getGuid() {
		return guid;
	}

	/**
	 * @return the capitoloSpesa
	 */
	public CapitoloSpesaDTO getCapitoloSpesa() {
		return capitoloSpesa;
	}

	/**
	 * @param capitoloSpesa the capitoloSpesa to set
	 */
	public void setCapitoloSpesa(final CapitoloSpesaDTO capitoloSpesa) {
		this.capitoloSpesa = capitoloSpesa;
	}

	/**
	 * @return the assegnatario
	 */
	public AssegnatarioDTO getAssegnatario() {
		return assegnatario;
	}

	/**
	 * @param assegnatario the assegnatario to set
	 */
	public void setAssegnatario(final AssegnatarioDTO assegnatario) {
		this.assegnatario = assegnatario;
	}

	/**
	 * @return the valida
	 */
	public boolean isValida() {
		return valida;
	}

	/**
	 * @param valida the valida to set
	 */
	public void setValida(final boolean valida) {
		this.valida = valida;
	}
	
}
