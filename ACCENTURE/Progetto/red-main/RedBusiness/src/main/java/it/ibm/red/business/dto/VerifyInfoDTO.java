package it.ibm.red.business.dto;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import it.ibm.red.business.enums.EsitoVerificaFirmaEnum;
import it.ibm.red.business.enums.ValueMetadatoVerificaFirmaEnum;
import it.pkbox.client.InvalidEnvelopeException;
import it.pkbox.client.PKBoxException;
import it.pkbox.client.VerifyInfoEx;
import it.pkbox.client.XMLVerifyInfoEx;

/**
 * The Class VerifyInfoDTO.
 *
 * @author CPIERASC
 * 
 * 	Oggetto per la verifica firma dei contenuti.
 */
public class VerifyInfoDTO extends AbstractDTO {

	private static final long serialVersionUID = 2492881298736417506L;

	/**
	 * Lista dei firmatari.
	 */
	private Collection<SignerDTO> firmatari;

	/**
	 * Numero di firme non valide.
	 */
	private Integer firmeNonValide;

	/**
	 * Esito della verifica della firma.
	 */
	private EsitoVerificaFirmaEnum esito;

	/**
	 * E' l'error code restituito dalla verifyEx di PK.
	 */
	private Integer errorCodePK;

	/**
	 * Eccezione sollevata dal metodo di verifica di PK diverso da PKBoxException | IOException | InvalidEnvelopeException.
	 */
	private Exception genericException;

	/**
	 * Costruttore.
	 */
	public VerifyInfoDTO() {
		super();
		firmeNonValide = 0;
		firmatari = new ArrayList<>();
	}

	/**
	 * Costruttore.
	 * @param e - eccezione verifica.
	 */
	public VerifyInfoDTO(final Exception e) {
		this();
		if (e instanceof PKBoxException) {
			PKBoxException pbe = (PKBoxException) e;
			
			this.errorCodePK = pbe.GetErrorCode();

			if (pbe.GetErrorCode() == PKBoxException.PKBOX_CLIENT_HTTP_ERROR 
					|| pbe.GetErrorCode() == PKBoxException.PKBOX_CLIENT_INIT_ERROR) {
				
				this.esito = EsitoVerificaFirmaEnum.ERRORE_CONNESSIONE_PK;
			
			} else if ((pbe.GetErrorCode() == PKBoxException.PKBOX_PARAM_NO_SIGNATURE) 
					&& ("No valid signatures found.".equalsIgnoreCase(pbe.getMessage()))) {
				
				this.esito = EsitoVerificaFirmaEnum.ERRORE_NO_SIGNATURE;
			
			} else if ((pbe.GetErrorCode() == PKBoxException.PKBOX_PARAM_NO_SIGNATURE) 
					&& ("No signatures found in PDF document.".equalsIgnoreCase(e.getMessage()))) {
				
				this.esito = EsitoVerificaFirmaEnum.ERRORE_NO_SIGNATURE;
			
			} else {
				this.esito = EsitoVerificaFirmaEnum.ERRORE_GENERICO_PK;
				
			}
			
		} else if (e instanceof IOException) {
			
			this.esito = EsitoVerificaFirmaEnum.ERRORE_CONVERSIONE_CONTENT;
		
		} else if (e instanceof InvalidEnvelopeException) {
			
			this.esito = EsitoVerificaFirmaEnum.ERRORE_ENVELOPE;
		
		} else {
			
			this.genericException = e;
			this.esito = EsitoVerificaFirmaEnum.ERRORE_GENERIC;
			
		}
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param vi	informazioni verifica contenuti firmati PK
	 */
	public VerifyInfoDTO(final VerifyInfoEx vinfo) {
		this();
		
		if (vinfo == null) {
			
			this.esito = EsitoVerificaFirmaEnum.ERRORE_NO_SIGNER;
		
		} else if (vinfo.getEnvelopeComplianceCode() != VerifyInfoEx.COMPLIANCE_CODE_GOOD) {
			
			this.esito = EsitoVerificaFirmaEnum.ERRORE_GLOBAL_COMPLIANCE;
		
		} else if (vinfo.getSignerCount() == 0) {
			
			this.esito = EsitoVerificaFirmaEnum.ERRORE_NO_SIGNER;
			
		} else {
			
			this.esito = EsitoVerificaFirmaEnum.OK;
			
			SignerDTO signer = null;
			for (int i = 0; i < vinfo.getSignerCount(); i++) {
				
				signer = new SignerDTO(vinfo.getSigner(i));
			
				if (!signer.isValid()) {
					firmeNonValide++;
				}
				firmatari.add(signer);
			}
		}
	}
	
	/**
	 * Costruttore che gestisce le informazioni di una firma xades.
	 * @param xmlInfo
	 */
	public VerifyInfoDTO (final XMLVerifyInfoEx xmlInfo) {
		this();
		
		if (xmlInfo == null) {
			
			this.esito = EsitoVerificaFirmaEnum.ERRORE_NO_SIGNER;
		
		} else if (xmlInfo.getEnvelopeComplianceCode() != VerifyInfoEx.COMPLIANCE_CODE_GOOD) {
			
			this.esito = EsitoVerificaFirmaEnum.ERRORE_GLOBAL_COMPLIANCE;
		
		} else if (xmlInfo.getSignerCount() == 0) {
			
			this.esito = EsitoVerificaFirmaEnum.ERRORE_NO_SIGNER;
			
		} else {
			
			this.esito = EsitoVerificaFirmaEnum.OK;
			
			SignerDTO signer = null;
			for (int i = 0; i < xmlInfo.getSignerCount(); i++) {
				
				signer = new SignerDTO(xmlInfo.getSigner(i));
			
				if (!signer.isValid()) {
					firmeNonValide++;
				}
				firmatari.add(signer);
			}
		}
	}

	/**
	 * Getter firmatari.
	 * @return	firmatari
	 */
	public final Collection<SignerDTO> getFirmatari() {
		return firmatari;
	}

	/**
	 * Restituisce il numero di firme non valide.
	 * @return firmeNonValide
	 */
	public Integer getFirmeNonValide() {
		return firmeNonValide;
	}
	
	/**
	 * @return esito verifica firma.
	 */
	public EsitoVerificaFirmaEnum getEsito() {
		return esito;
	}

	/**
	 * Imposta l'esito della verifica della firma.
	 * @param esito
	 */
	public void setEsito(final EsitoVerificaFirmaEnum esito) {
		this.esito = esito;
	}
	
	/**
	 * @return errorCodePK
	 */
	public Integer getErrorCodePK() {
		return errorCodePK;
	}
	
	/**
	 * @return genericException
	 */
	public Exception getGenericException() {
		return genericException;
	}

	/**
	 * Restituisce il valore del metadato "validitaFirma" a fonte delle info di verifica firma contenute nell'oggetto.
	 * 
	 * @return
	 */
	public Integer getMetadatoValiditaFirmaValue() {
		Integer output = null;

		ValueMetadatoVerificaFirmaEnum value = getMetadatoValiditaFirma();
		if (value != null) {
			output = value.getValue();
		}
		return output;
	}
	/**
	 * Restituisce il valore del metadato "validitaFirma" a fonte delle info di verifica firma contenute nell'oggetto.
	 * @return
	 */
	public ValueMetadatoVerificaFirmaEnum getMetadatoValiditaFirma() {
		
		ValueMetadatoVerificaFirmaEnum value = null;
		switch(esito) {
		
			case OK:
				value = ValueMetadatoVerificaFirmaEnum.FIRMA_OK;
				if (firmeNonValide > 0) {
					value = ValueMetadatoVerificaFirmaEnum.FIRMA_KO;
				}
				break;
			
			case ERRORE_CONVERSIONE_CONTENT:
			case ERRORE_ENVELOPE:
			case ERRORE_GLOBAL_COMPLIANCE:
			case ERRORE_GENERICO_PK:
				value = ValueMetadatoVerificaFirmaEnum.FIRMA_KO;
				break;
			
			case ERRORE_GENERIC:
			case ERRORE_CONNESSIONE_PK:
				value = null;
				break;
		
			case ERRORE_NO_SIGNER:
			case ERRORE_NO_SIGNATURE:
				value = ValueMetadatoVerificaFirmaEnum.FIRMA_NON_PRESENTE;
				break;
			
			default:
				value = null;
				break;
		}
		return value;
	}
}