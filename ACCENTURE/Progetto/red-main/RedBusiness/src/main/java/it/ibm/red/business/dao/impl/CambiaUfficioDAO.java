package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.ICambiaUfficioDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;

/**
 * DAO cambio ufficio.
 */
@Repository
public class CambiaUfficioDAO extends AbstractDAO implements ICambiaUfficioDAO {

	/**
	 * La costante serial Version UID.
	 */
	private static final long serialVersionUID = -2566588660693178062L;

	/**
	 * Inner join.
	 */
	private static final String INNER_JOIN = "INNER JOIN ";

	/**
	 * Errore.
	 */
	private static final String ERRORE_DURANTE_LA_CONNESSIONE = "Errore durante la connessione";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(CambiaUfficioDAO.class);

	/**
	 * Cambio ufficio.
	 */
	private static final String SQL_TABLE_PER_CAMBIO_AOO_UFFICIO_RUOLO = "NODOUTENTERUOLO " + INNER_JOIN
			+ "NODO ON NODO.IDNODO = NODOUTENTERUOLO.IDNODO " 
			+ INNER_JOIN
			+ "RUOLO ON RUOLO.IDRUOLO = NODOUTENTERUOLO.IDRUOLO " 
			+ INNER_JOIN
			+ "UTENTE ON UTENTE.IDUTENTE = NODOUTENTERUOLO.IDUTENTE " 
			+ INNER_JOIN + "AOO ON AOO.IDAOO = NODO.IDAOO "
			+ "AND (NODOUTENTERUOLO.dataDisattivazione IS NULL OR NODOUTENTERUOLO.dataDisattivazione > sysdate) "   
			+ "AND NODOUTENTERUOLO.DATAATTIVAZIONE <= sysdate "   
			+ "AND NODO.DATADISATTIVAZIONE IS NULL "
			+ "AND UTENTE.DATADISATTIVAZIONE IS NULL " 
			+ "AND RUOLO.DATADISATTIVAZIONE IS NULL "
			+ "AND AOO.DATADISATTIVAZIONE IS NULL ";

	/**
	 * @see it.ibm.red.business.dao.ICambiaUfficioDAO#getIdNodiFromIdUtenteandIdAOO(java.lang.Long,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<Long> getIdNodiFromIdUtenteandIdAOO(final Long idUtente, final Long idAOO,
			final Connection connection) {
		final List<Long> idNodi = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			final String querySQL = "SELECT DISTINCT NODO.IDNODO FROM " + SQL_TABLE_PER_CAMBIO_AOO_UFFICIO_RUOLO
					+ " AND UTENTE.IDUTENTE = ? AND AOO.IDAOO = ?";
			ps = connection.prepareStatement(querySQL);
			ps.setLong(1, idUtente);
			ps.setLong(2, idAOO);
			rs = ps.executeQuery();
			while (rs.next()) {
				idNodi.add(rs.getLong("IDNODO"));
			}
			return idNodi;
		} catch (final SQLException e) {
			LOGGER.error(ERRORE_DURANTE_LA_CONNESSIONE + e.getMessage(), e);
			throw new RedException(ERRORE_DURANTE_LA_CONNESSIONE);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ICambiaUfficioDAO#getIdRuoloFromIdUtenteAndIdNodoAndIdAOO(java.lang.Long,
	 *      java.lang.Integer, java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<Long> getIdRuoloFromIdUtenteAndIdNodoAndIdAOO(final Long idUtente, final Integer idNodo,
			final Long idAOO, final Connection connection) {
		final List<Long> idRuoli = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			final String querySQL = "SELECT DISTINCT RUOLO.IDRUOLO FROM " + SQL_TABLE_PER_CAMBIO_AOO_UFFICIO_RUOLO
					+ " AND UTENTE.IDUTENTE = ? AND NODO.IDNODO = ? AND AOO.IDAOO = ?";
			ps = connection.prepareStatement(querySQL);
			ps.setLong(1, idUtente);
			ps.setLong(2, idNodo);
			ps.setLong(3, idAOO);
			rs = ps.executeQuery();
			while (rs.next()) {
				idRuoli.add(rs.getLong("IDRUOLO"));
			}
			return idRuoli;

		} catch (final SQLException e) {
			LOGGER.error(ERRORE_DURANTE_LA_CONNESSIONE + e.getMessage(), e);
			throw new RedException(ERRORE_DURANTE_LA_CONNESSIONE);
		} finally {
			closeStatement(ps, rs);
		}
	}

	/**
	 * @see it.ibm.red.business.dao.ICambiaUfficioDAO#getAOOfromIdUtente(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public List<Aoo> getAOOfromIdUtente(final Long idUtente, final Connection connection) {
		final List<Aoo> listaAOO = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			final String querySQL = "SELECT DISTINCT AOO.* FROM " + SQL_TABLE_PER_CAMBIO_AOO_UFFICIO_RUOLO
					+ " AND UTENTE.IDUTENTE = ?";
			ps = connection.prepareStatement(querySQL);
			ps.setLong(1, idUtente);
			rs = ps.executeQuery();
			while (rs.next()) {
				final Aoo aoo = new Aoo(rs.getLong("IDAOO"), rs.getBigDecimal("IDENTE"), rs.getString("CODICEAOO"),
						rs.getString("DESCRIZIONE"), rs.getBoolean("SALVA_PIN"), rs.getInt("ALTEZZAFOOTER"),
						rs.getInt("SPAZIATURAFIRMA"), rs.getLong("PARAMETRIAOO"), rs.getLong("IDNODORADICE"),
						rs.getInt("ID_LOGO"), rs.getInt("MAX_SIZE_ENTRATA"), rs.getInt("MAX_SIZE_USCITA"),
						rs.getInt("CONF_FIRMA"), rs.getInt("MANTIENI_ALLEGATI_ORIGINALI"), rs.getInt("INOLTRA_MAIL"), 
						rs.getString("VIS_FALDONI"), rs.getInt("FORZA_REFRESH_CODE"), rs.getInt("POSTA_INTEROP_ESTERNA"),
						rs.getLong("IDRUOLO_DELEGATO_LIBRO_FIRMA"), rs.getBoolean("SHOW_DIALOG"), rs.getBoolean("TIMBRO_USCITA_AOO"),
						rs.getBoolean("IS_ESTENDI_VISIBILITA"), rs.getBoolean("DISABLEUSEHOSTONLY"), rs.getBoolean("SHOW_RIFERIMENTO_STORICO"),
						rs.getBoolean("SHOW_RICERCA_NSD"), rs.getBoolean("SELEZIONA_TUTTI_VISTO"), rs.getLong("ASSEGNAZIONEINDIRETTA"),
						rs.getInt("UCB"), rs.getInt("FLAG_STRATEGIA_FIRMA"), rs.getInt("POSTILLA_ATTIVA"), rs.getInt("POSTILLA_SOLO_PAG1"));
				listaAOO.add(aoo);
			}
			return listaAOO;

		} catch (final SQLException e) {
			LOGGER.error(ERRORE_DURANTE_LA_CONNESSIONE + e.getMessage(), e);
			throw new RedException(ERRORE_DURANTE_LA_CONNESSIONE);
		} finally {
			closeStatement(ps, rs);
		}
	}

}
