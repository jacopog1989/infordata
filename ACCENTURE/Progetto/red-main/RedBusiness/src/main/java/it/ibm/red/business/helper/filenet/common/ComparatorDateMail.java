package it.ibm.red.business.helper.filenet.common;

import java.util.Comparator;

import it.ibm.red.business.dto.EmailDTO;

/**
 * Comparatore date mail.
 */
public class ComparatorDateMail implements Comparator<EmailDTO> {

	/**
	 * Esegue la comparazione tra le date delle mail passate come parametri.
	 * @param mail0
	 * @param mail1
	 * @return  0 se le date sono uguali, un valore negativo se mail1 è antecedente a mail0,
	 * 			un valore maggiore di 1 se la mail1 è successiva a mail0.
	 */
	@Override
	public int compare(final EmailDTO mail0, final EmailDTO mail1) {
		return mail1.getData().compareTo(mail0.getData());
	}

}
