package it.ibm.red.business.helper.html;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.util.List;

import it.ibm.red.business.dto.DetailEmailDTO;
import it.ibm.red.business.dto.SalvaDocumentoRedDTO;
import it.ibm.red.business.dto.TemplateMetadatoValueDTO;
import it.ibm.red.business.helper.xml.DatiProtocollo;
import it.ibm.red.business.template.TemplateDocumento;

/**
 * Interfaccia Helper file HTML.
 */
public interface IHtmlFileHelper extends Serializable {
	
	/**
	 * Crea il file HTML per il content della mail.
	 * @param contentMail
	 * @return file HTML
	 */
	ByteArrayInputStream creaDocumentContentMailHTML(String contentMail);
	
	/**
	 * Crea il file HTML per il documento.
	 * @param templateDocumento
	 * @param documento
	 * @return file HTML
	 */
	ByteArrayInputStream creaDocumentoHtml(TemplateDocumento templateDocumento, SalvaDocumentoRedDTO documento);
	
	/**
	 * Genera un file HTML che elenca i protocolli del flusso FAD in maschera.
	 * @param protocolli
	 * @return file HTML
	 */
	ByteArrayInputStream createReportProtocolliFadHTML(List<DatiProtocollo> protocolli);
	
	/**
	 * Crea il file HTML per il documento in uscita da template.
	 * @param fileHtml
	 * @param metadati
	 * @return file HTML
	 */
	ByteArrayInputStream createTemplateDocUscita(StringBuilder fileHtml, List<TemplateMetadatoValueDTO> metadati);

	//START VI
	/**
	 * Crea il file HTML per la mail.
	 * @param templateDocumento
	 * @param email
	 * @return file HTML
	 */
	ByteArrayInputStream creaEmailHtml(TemplateDocumento templateDocumento, DetailEmailDTO email);

	/**
	 * Crea il file HTML per la mail.
	 * @param templateDocumento
	 * @param email
	 * @return file HTML
	 */
	ByteArrayInputStream creaEmailHtmlPlainText(TemplateDocumento templateDocumento, DetailEmailDTO email);
	
}
