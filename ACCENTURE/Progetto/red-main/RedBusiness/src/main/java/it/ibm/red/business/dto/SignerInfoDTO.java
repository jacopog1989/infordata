package it.ibm.red.business.dto;

import it.ibm.red.business.enums.ConfigurazioneFirmaAooEnum;
import it.ibm.red.business.helper.signing.SignatureLayout;
import it.ibm.red.business.persistence.model.PkHandler;

/**
 * The Class SignerInfoDTO.
 *
 * @author CPIERASC
 * 
 * 	Classe per la gestione delle credenziali di firma.
 */
public class SignerInfoDTO extends AbstractDTO {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -6685454901939147148L;

	/**
	 * Info firmatario.
	 */
	private String customerinfo;

	/**
	 * Glifo firmatario.
	 */
	private byte[] imageFirma;

	/**
	 * Pin.
	 */
	private String pin;

	/**
	 * Motivazione firma.
	 */
	private String reason;

	/**
	 * Layout firma.
	 */
	private SignatureLayout sigLayout;

	/**
	 * Firmatario.
	 */
	private String signer;

	/**
	 * Signer pin (otp).
	 */
	private String signerPin;
	
	/**
	 * PIN verificato.
	 */
	private Boolean pinVerificato;
	
	/**
	 * Configurazione dell'abilitazione alle differenti tipologie di firma (locale o remota).
	 */
	private ConfigurazioneFirmaAooEnum configurazioneFirma;
	
	/**
	 * Handler PK per la firma.
	 */
	private PkHandler pkHandlerFirma;
	
	/**
	 * Handler PK per la verifica.
	 */
	private PkHandler pkHandlerVerifica;

	/**
	 * Costruttore.
	 * 
	 * @param inPkHandlerFirma		handler PK per la firma
	 * @param inPkHandlerVerifica	handler PK per la verifica
	 * @param inSigner				firmatario
	 * @param inPin					pin firmatario
	 * @param inSignerPin			signer pin (otp per firma remota)
	 * @param inReason				motivo firma
	 * @param inCustomerinfo		informazioni sul firmatario
	 * @param inImageFirma			glifo
	 * @param inSigLayout			layout firma
	 * @param inPinVerificato		pin verificato
	 */
	public SignerInfoDTO(final PkHandler inPkHandlerFirma, final PkHandler inPkHandlerVerifica, final String inSigner, final String inPin, final String inSignerPin, 
			final String inReason, final String inCustomerinfo, final byte[] inImageFirma, final SignatureLayout inSigLayout, final boolean inPinVerificato) {
		this(inPkHandlerFirma, inPkHandlerVerifica);
		this.signer = inSigner;
		this.pin = inPin;
		this.signerPin = inSignerPin;
		this.reason = inReason;
		this.customerinfo = inCustomerinfo;
		this.imageFirma = inImageFirma;
		this.sigLayout = inSigLayout;
		this.pinVerificato = inPinVerificato;
	}

	/**
	 * Costruttore.
	 * @param inPkHandlerFirma
	 * @param inPkHandlerVerifica
	 */
	public SignerInfoDTO(final PkHandler inPkHandlerFirma, final PkHandler inPkHandlerVerifica) {
		super();
		this.pkHandlerFirma = inPkHandlerFirma;
		this.pkHandlerVerifica = inPkHandlerVerifica;
	}
	
	
	/**
	 * Costruttore copia.
	 * 
	 * @param dtoToCopy
	 *            Dto da copiare.
	 */
	public SignerInfoDTO(SignerInfoDTO dtoToCopy) {
		super();
		this.customerinfo = dtoToCopy.getCustomerinfo();
		this.imageFirma = dtoToCopy.getImageFirma();
		this.pin = dtoToCopy.getPin();
		this.reason = dtoToCopy.getReason();
		this.sigLayout = dtoToCopy.getSigLayout();
		this.signer = dtoToCopy.getSigner();
		this.signerPin = dtoToCopy.getSignerPin();
		this.pinVerificato = dtoToCopy.getPinVerificato();
		this.configurazioneFirma = dtoToCopy.getConfigurazioneFirma();
		this.pkHandlerFirma = dtoToCopy.getPkHandlerFirma();
		this.pkHandlerVerifica = dtoToCopy.getPkHandlerVerifica();
	}

	/**
	 * Getter info firmatario.
	 * 
	 * @return	info firmatario
	 */
	public final String getCustomerinfo() {
		return customerinfo;
	}

	/**
	 * Getter glifo.
	 * 
	 * @return	glifo
	 */
	public final byte[] getImageFirma() {
		return imageFirma;
	}

	/**
	 * Imposta il byte array che rappresentano l'immagine della firma.
	 * @param imageFirma
	 */
	public void setImageFirma(byte[] imageFirma) {
		this.imageFirma = imageFirma;
	}

	/**
	 * Getter pin.
	 * 
	 * @return	pin
	 */
	public final String getPin() {
		return pin;
	}

	/**
	 * Getter reason.
	 * 
	 * @return	reason
	 */
	public final String getReason() {
		return reason;
	}

	/**
	 * Imposta la reason.
	 * @param reason
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * Getter layout firma.
	 * 
	 * @return	layout firma
	 */
	public final SignatureLayout getSigLayout() {
		return sigLayout;
	}

	/**
	 * Getter Alias Signer.
	 * 
	 * @return	alias signer
	 */
	public final String getSigner() {
		return signer;
	}

	/**
	 * Getter signer pin.
	 * 
	 * @return	signer pin
	 */
	public final String getSignerPin() {
		return signerPin;
	}

	/**
	 * Setter signer pin.
	 * 
	 * @param inSignerPin	signer pin
	 */
	public final void setSignerPin(final String inSignerPin) {
		this.signerPin = inSignerPin;
	}

	/**
	 * Setter PIN.
	 * @param inPin - PIN
	 */
	public final void setPin(final String inPin) {
		this.pin = inPin;
	}

	/**
	 * Getter Pin verificato.
	 * @return pin verificato
	 */
	public Boolean getPinVerificato() {
		return pinVerificato;
	}
	
	/**
	 * Setter PIN VERIFICATO.
	 * @param inPinVerificato - pinVerificato
	 */
	public final void setPinVerificato(final Boolean inPinVerificato) {
		pinVerificato = inPinVerificato;
	}

	/**
	 * @return the configurazioneFirma
	 */
	public ConfigurazioneFirmaAooEnum getConfigurazioneFirma() {
		return configurazioneFirma;
	}
	
	/**
	 * @param configurazioneFirma the configurazioneFirma to set
	 */
	public void setConfigurazioneFirma(final ConfigurazioneFirmaAooEnum configurazioneFirma) {
		this.configurazioneFirma = configurazioneFirma;
	}

	/**
	 * @return the pkHandlerFirma
	 */
	public PkHandler getPkHandlerFirma() {
		return pkHandlerFirma;
	}
	
	/**
	 * @param pkHandlerFirma the pkHandlerFirma to set
	 */
	public void setPkHandlerFirma(final PkHandler pkHandlerFirma) {
		this.pkHandlerFirma = pkHandlerFirma;
	}

	/**
	 * @return the pkHandlerVerifica
	 */
	public PkHandler getPkHandlerVerifica() {
		return pkHandlerVerifica;
	}

	/**
	 * @param pkHandlerVerifica
	 */
	public void setPkHandlerVerifica(final PkHandler pkHandlerVerifica) {
		this.pkHandlerVerifica = pkHandlerVerifica;
	}
	
}