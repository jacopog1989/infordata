package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.INotificaContattoDAO;
import it.ibm.red.business.dao.INotificaUtenteDAO;
import it.ibm.red.business.dto.NotificaDTO;
import it.ibm.red.business.dto.NotificheUtenteDTO;
import it.ibm.red.business.dto.SearchEventiDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.EventoCalendarioEnum;
import it.ibm.red.business.enums.PermessiEnum;
import it.ibm.red.business.enums.StatoNotificaUtenteEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.Evento;
import it.ibm.red.business.persistence.model.NotificaContatto;
import it.ibm.red.business.service.ICalendarioSRV;
import it.ibm.red.business.service.INotificaSRV;
import it.ibm.red.business.service.INotificaUtenteSRV;
import it.ibm.red.business.utils.DateUtils;
import it.ibm.red.business.utils.PermessiUtils;

/**
 * Servizi per la gestione delle notifiche utente (pannello con notifiche
 * sottoscrizioni, calendario e rubrica).
 *
 * @author m.crescentini
 */
@Service
public class NotificaUtenteSRV extends AbstractService implements INotificaUtenteSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -5372607576405994330L;

	/**
	 * Messaggio errore recupero notifiche utente.
	 */
	private static final String ERROR_RECUPERO_NOTIFICHE_MSG = "Errore in fase di recupero delle notifiche utente";

	/**
	 * Messaggio erore aggiornamento notifica.
	 */
	private static final String ERROR_AGGIORNAMENTO_NOTIFICA_MSG = "Eccezione in fase di aggiornamento della notifica ";

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(NotificaUtenteSRV.class.getName());

	/**
	 * Numero giorni notifiche.
	 */
	private static final Integer NUM_GIORNI_NOTIFICHE = 30;

	/**
	 * Service.
	 */
	@Autowired
	private ICalendarioSRV calendarioSRV;

	/**
	 * Service.
	 */
	@Autowired
	private INotificaSRV notificaSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private INotificaContattoDAO notificaContattoDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IAooDAO aooDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private INotificaUtenteDAO notificaUtenteDAO;

	/**
	 * Contatore notifiche non lette.
	 */
	private Integer contatoreNotificheNonLette;

	/**
	 * @see it.ibm.red.business.service.facade.INotificaUtenteFacadeSRV#getAll(it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public final List<NotificheUtenteDTO> getAll(final UtenteDTO utente) {
		final List<NotificheUtenteDTO> notificaList = new ArrayList<>();
		Connection con = null;
		Connection dwhCon = null;

		int rowKey = -1; // Variabile usate per il datatable latofronend
		contatoreNotificheNonLette = 0;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			dwhCon = setupConnection(getFilenetDataSource().getConnection(), false);

			// ### RECUPERO NOTIFICHE RUBRICA -> START
			List<NotificaContatto> notificheRubrica = null;

			// Si controllano i permessi dell'utente per capire se prendere la lista delle
			// richieste effettuate o delle richieste evase (amministratore rubrica)
			if (PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.GESTIONE_RUBRICA)) {
				notificheRubrica = notificaContattoDAO.getListaNotificheRichiesteHome(NUM_GIORNI_NOTIFICHE, utente.getMaxNotificheNonLette(), utente.getMaxNotificheRubrica(),
						utente.getIdAoo(), utente.getId(), con);
			} else {
				notificheRubrica = notificaContattoDAO.getListaNotificheHomePage(utente.getIdUfficio(), NUM_GIORNI_NOTIFICHE, utente.getMaxNotificheNonLette(),
						utente.getMaxNotificheRubrica(), con, utente.getUsername(), utente.getId());
			}

			if (!it.ibm.red.business.utils.CollectionUtils.isEmptyOrNull(notificheRubrica)) {
				for (final NotificaContatto notificaRubrica : notificheRubrica) {
					rowKey++;
					if (notificaRubrica.getStatoNotificaRubrica() != null
							&& !StatoNotificaUtenteEnum.NOTIFICA_DA_LEGGERE.getIdStato().equals(notificaRubrica.getStatoNotificaRubrica())) {
						notificaRubrica.setDaLeggere(false);
					} else {
						notificaRubrica.setDaLeggere(true);
						contatoreNotificheNonLette = contatoreNotificheNonLette + 1;
					}
					final NotificheUtenteDTO notificheUtente = new NotificheUtenteDTO(null, null, notificaRubrica, rowKey);
					notificaList.add(notificheUtente);
				}
			}
			// ### RECUPERO NOTIFICHE RUBRICA -> END

			// ### RECUPERO NOTIFICHE SOTTOSCRIZIONI -> START
			final List<NotificaDTO> notificheSottoscrizioni = notificaSRV.recuperaNotifiche(utente.getId(), utente.getIdAoo(), utente.getFcDTO(), NUM_GIORNI_NOTIFICHE,
					utente.getMaxNotificheSottoscrizioni(), true, utente.getMaxNotificheNonLette(), dwhCon);

			if (!it.ibm.red.business.utils.CollectionUtils.isEmptyOrNull(notificheSottoscrizioni)) {
				for (final NotificaDTO notificaSottoscrizioni : notificheSottoscrizioni) {
					rowKey++;
					if (notificaSottoscrizioni.getVisualizzato() == 0) {
						notificaSottoscrizioni.setDaLeggere(true);
						contatoreNotificheNonLette = contatoreNotificheNonLette + 1;
					} else {
						notificaSottoscrizioni.setDaLeggere(false);
					}
					final NotificheUtenteDTO notificheUtente = new NotificheUtenteDTO(notificaSottoscrizioni, null, null, rowKey);
					notificaList.add(notificheUtente);
				}
			}
			// ### RECUPERO NOTIFICHE SOTTOSCRIZIONI -> END

			// ### RECUPERO NOTIFICHE CALENDARIO -> START
			List<Evento> notificheCalendario = null;
			final Aoo aoo = aooDAO.getAoo(utente.getIdAoo(), con);

			final SearchEventiDTO paramsRicercaEventiCalendario = new SearchEventiDTO();
			// Data di inizio per la ricerca degli eventi di calendario: oggi -
			// OFFSET_GIORNI_EVENTI_CALENDARIO
			final Date dataInizioEventiCalendario = DateUtils.setDateTo0000(new Date());
			LocalDate dataInizioEventiCalendarioCal = Instant.ofEpochMilli(dataInizioEventiCalendario.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
			dataInizioEventiCalendarioCal = dataInizioEventiCalendarioCal.minusDays(NUM_GIORNI_NOTIFICHE);
			paramsRicercaEventiCalendario.setDataInizio(Date.from(dataInizioEventiCalendarioCal.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));

			// Data di fine per la ricerca degli eventi di calendario: oggi +
			// OFFSET_GIORNI_EVENTI_CALENDARIO
			final Date dataFineEventiCalendario = DateUtils.setDateTo2359(new Date());
			LocalDate dataFineEventiCalendarioCal = Instant.ofEpochMilli(dataFineEventiCalendario.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
			dataFineEventiCalendarioCal = dataFineEventiCalendarioCal.plusDays(NUM_GIORNI_NOTIFICHE);
			paramsRicercaEventiCalendario.setDataFine(Date.from(dataFineEventiCalendarioCal.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));

			paramsRicercaEventiCalendario.setMaxResults(utente.getMaxNotificheCalendario());

			if (utente.getMaxNotificheNonLette() > 0) {
				// Data di inizio per la ricerca degli eventi di calendario notifiche non lette
				final Date dataInizioNotificheNonLette = DateUtils.setDateTo0000(new Date());
				LocalDate dataInizioNotNonLette = Instant.ofEpochMilli(dataInizioNotificheNonLette.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
				dataInizioNotNonLette = dataInizioNotNonLette.minusDays(utente.getMaxNotificheNonLette());
				paramsRicercaEventiCalendario.setDataInizioNotificheNonLette(Date.from(dataInizioNotNonLette.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));

				// Data di fine per la ricerca degli eventi di calendario notifiche non lette
				final Date dataFineNotificheNonLette = DateUtils.setDateTo2359(new Date());
				LocalDate dataFineNotNonLette = Instant.ofEpochMilli(dataFineNotificheNonLette.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
				dataFineNotNonLette = dataFineNotNonLette.plusDays(utente.getMaxNotificheNonLette());
				paramsRicercaEventiCalendario.setDataFineNotificheNonLette(Date.from(dataFineNotNonLette.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
			}

			final Map<EventoCalendarioEnum, List<Evento>> mappaEventiCalendario = calendarioSRV.getEventiForCalendarioHomepage(paramsRicercaEventiCalendario, utente,
					aoo.getEnte().getIdEnte(), con);

			if (!CollectionUtils.isEmpty(mappaEventiCalendario)) {
				notificheCalendario = mappaEventiCalendario.values().stream().sequential().flatMap(List::stream).collect(Collectors.toList());
			}

			if (!it.ibm.red.business.utils.CollectionUtils.isEmptyOrNull(notificheCalendario)) {
				for (final Evento notificaCalendario : notificheCalendario) {
					rowKey++;
					final NotificheUtenteDTO notificheUtente = new NotificheUtenteDTO(null, notificaCalendario, null, rowKey);
					if (notificaCalendario.isDaLeggere()) {
						contatoreNotificheNonLette = contatoreNotificheNonLette + 1;
					}
					notificaList.add(notificheUtente);
				}
			}
			// ### RECUPERO NOTIFICHE CALENDARIO -> END

		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante il recupero delle notifiche utente.", e);
		} finally {
			closeConnection(con);
			closeConnection(dwhCon);
		}

		return notificaList;
	}

	/**
	 * @see it.ibm.red.business.service.facade.INotificaUtenteFacadeSRV#contrassegnaNotificaCalendario(java.lang.Long,
	 *      java.lang.Long, int, int).
	 */
	@Override
	public boolean contrassegnaNotificaCalendario(final Long idUtente, final Long idAoo, final int idEvento, final int statoNotifica) {
		Connection conn = null;

		try {
			conn = setupConnection(getDataSource().getConnection(), true);
			boolean isOk = true;
			isOk = notificaUtenteDAO.contrassegnaOEliminaNotificaCalendario(idUtente, idAoo, idEvento, statoNotifica, conn);
			if (!isOk) {
				throw new RedException(ERROR_AGGIORNAMENTO_NOTIFICA_MSG + idEvento);
			}

			commitConnection(conn);
			return true;
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_NOTIFICHE_MSG, e);
			rollbackConnection(conn);
		} finally {
			closeConnection(conn);
		}
		return false;
	}

	/**
	 * @see it.ibm.red.business.service.facade.INotificaUtenteFacadeSRV#eliminaNotificaCalendario(java.lang.Long,
	 *      java.lang.Long, int, int).
	 */
	@Override
	public boolean eliminaNotificaCalendario(final Long idUtente, final Long idAoo, final int idEvento, final int statoNotifica) {
		
		return contrassegnaNotificaCalendario(idUtente, idAoo, idEvento, statoNotifica);
	}

	/**
	 * @see it.ibm.red.business.service.facade.INotificaUtenteFacadeSRV#contrassegnaNotificaCalendarioScadenza(java.lang.Long,
	 *      java.lang.Long, java.lang.String, int).
	 */
	@Override
	public boolean contrassegnaNotificaCalendarioScadenza(final Long idUtente, final Long idAoo, final String docTitle, final int statoNotifica) {
		Connection conn = null;

		try {
			conn = setupConnection(getDataSource().getConnection(), true);
			boolean isOk = true;
			isOk = notificaUtenteDAO.contrassegnaOEliminaNotificaCalendarioScadenza(idUtente, idAoo, docTitle, statoNotifica, conn);
			if (!isOk) {
				throw new RedException(ERROR_AGGIORNAMENTO_NOTIFICA_MSG + docTitle);
			}

			commitConnection(conn);
			return true;
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_NOTIFICHE_MSG, e);
			rollbackConnection(conn);
		} finally {
			closeConnection(conn);
		}
		return false;
	}

	/**
	 * @see it.ibm.red.business.service.facade.INotificaUtenteFacadeSRV#eliminaNotificaCalendarioScadenza(java.lang.Long,
	 *      java.lang.Long, java.lang.String, int).
	 */
	@Override
	public boolean eliminaNotificaCalendarioScadenza(final Long idUtente, final Long idAoo, final String docTitle, final int statoNotifica) {
		
		return contrassegnaNotificaCalendarioScadenza(idUtente, idAoo, docTitle, statoNotifica);
	}

	/**
	 * @see it.ibm.red.business.service.facade.INotificaUtenteFacadeSRV#contrassegnaNotificaRubrica(java.lang.Long,
	 *      java.lang.Long, java.lang.Integer, int).
	 */
	@Override
	public boolean contrassegnaNotificaRubrica(final Long idUtente, final Long idAoo, final Integer idNotificaRubrica, final int statoNotifica) {
		Connection conn = null;

		try {
			conn = setupConnection(getDataSource().getConnection(), true);
			boolean isOk = true;
			isOk = notificaUtenteDAO.contrassegnaOEliminaNotificaRubrica(idUtente, idAoo, idNotificaRubrica, statoNotifica, conn);
			if (!isOk) {
				throw new RedException(ERROR_AGGIORNAMENTO_NOTIFICA_MSG + idNotificaRubrica);
			}

			commitConnection(conn);
			return true;
		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_NOTIFICHE_MSG, e);
			rollbackConnection(conn);
		} finally {
			closeConnection(conn);
		}
		return false;
	}

	/**
	 * @see it.ibm.red.business.service.facade.INotificaUtenteFacadeSRV#eliminaNotificaRubrica(java.lang.Long,
	 *      java.lang.Long, java.lang.Integer, int).
	 */
	@Override
	public boolean eliminaNotificaRubrica(final Long idUtente, final Long idAoo, final Integer idNotificaRubrica, final int statoNotifica) {

		return contrassegnaNotificaRubrica(idUtente, idAoo, idNotificaRubrica, statoNotifica);
	}

	/**
	 * @see it.ibm.red.business.service.facade.INotificaUtenteFacadeSRV#countNotificheNonLette().
	 */
	@Override
	public Integer countNotificheNonLette() {
		return contatoreNotificheNonLette;
	}

	/**
	 * @see it.ibm.red.business.service.facade.INotificaUtenteFacadeSRV#contaNotificheNonLette(it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public final Integer contaNotificheNonLette(final UtenteDTO utente) {
		Integer contNotificheNonLette = 0;
		Connection con = null;

		contatoreNotificheNonLette = 0;
		try {
			con = setupConnection(getDataSource().getConnection(), false);

			if (PermessiUtils.hasPermesso(utente.getPermessi(), PermessiEnum.GESTIONE_RUBRICA)) {
				contNotificheNonLette += notificaContattoDAO.getCountListaNotificheRichiesteHome(utente.getMaxNotificheNonLette(), utente.getMaxNotificheRubrica(),
						utente.getIdAoo(), utente.getId(), con);
			} else {
				contNotificheNonLette += notificaContattoDAO.getCountListaNotificheHomePage(utente.getMaxNotificheNonLette(), utente.getMaxNotificheRubrica(), con,
						utente.getUsername(), utente.getId());
			}
			// ### RECUPERO NOTIFICHE RUBRICA -> END

			// ### RECUPERO NOTIFICHE SOTTOSCRIZIONI -> START
			contNotificheNonLette += notificaSRV.countNotificheSottoscrizione(utente.getId(), utente.getIdAoo(), utente.getMaxNotificheSottoscrizioni(),
					utente.getMaxNotificheNonLette());
			// ### RECUPERO NOTIFICHE SOTTOSCRIZIONI -> END

			// ### RECUPERO NOTIFICHE CALENDARIO -> START
			final Aoo aoo = aooDAO.getAoo(utente.getIdAoo(), con);

			final SearchEventiDTO paramsRicercaEventiCalendario = new SearchEventiDTO();
			// Data di inizio per la ricerca degli eventi di calendario: oggi -
			// OFFSET_GIORNI_EVENTI_CALENDARIO
			final Date dataInizioEventiCalendario = DateUtils.setDateTo0000(new Date());
			LocalDate dataInizioEventiCalendarioCal = Instant.ofEpochMilli(dataInizioEventiCalendario.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
			dataInizioEventiCalendarioCal = dataInizioEventiCalendarioCal.minusDays(NUM_GIORNI_NOTIFICHE);
			paramsRicercaEventiCalendario.setDataInizio(Date.from(dataInizioEventiCalendarioCal.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));

			// Data di fine per la ricerca degli eventi di calendario: oggi +
			// OFFSET_GIORNI_EVENTI_CALENDARIO
			final Date dataFineEventiCalendario = DateUtils.setDateTo2359(new Date());
			LocalDate dataFineEventiCalendarioCal = Instant.ofEpochMilli(dataFineEventiCalendario.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
			dataFineEventiCalendarioCal = dataFineEventiCalendarioCal.plusDays(NUM_GIORNI_NOTIFICHE);
			paramsRicercaEventiCalendario.setDataFine(Date.from(dataFineEventiCalendarioCal.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));

			paramsRicercaEventiCalendario.setMaxResults(utente.getMaxNotificheCalendario());

			if (utente.getMaxNotificheNonLette() > 0) {
				// Data di inizio per la ricerca degli eventi di calendario notifiche non lette
				final Date dataInizioNotificheNonLette = DateUtils.setDateTo0000(new Date());
				LocalDate dataInizioNotNonLette = Instant.ofEpochMilli(dataInizioNotificheNonLette.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
				dataInizioNotNonLette = dataInizioNotNonLette.minusDays(utente.getMaxNotificheNonLette());
				paramsRicercaEventiCalendario.setDataInizioNotificheNonLette(Date.from(dataInizioNotNonLette.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));

				// Data di fine per la ricerca degli eventi di calendario notifiche non lette
				final Date dataFineNotificheNonLette = DateUtils.setDateTo2359(new Date());
				LocalDate dataFineNotNonLette = Instant.ofEpochMilli(dataFineNotificheNonLette.getTime()).atZone(ZoneId.systemDefault()).toLocalDate();
				dataFineNotNonLette = dataFineNotNonLette.plusDays(utente.getMaxNotificheNonLette());
				paramsRicercaEventiCalendario.setDataFineNotificheNonLette(Date.from(dataFineNotNonLette.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant()));
			}
			contNotificheNonLette += calendarioSRV.getCountEventiForCalendarioHomepage(paramsRicercaEventiCalendario, utente, aoo.getEnte().getIdEnte(), con);
			// ### RECUPERO NOTIFICHE CALENDARIO -> END

			contatoreNotificheNonLette = contNotificheNonLette;
		} catch (final Exception e) {
			LOGGER.error("Si è verificato un errore durante la count delle notifiche utente.", e);
		} finally {
			closeConnection(con);
		}

		return contatoreNotificheNonLette;
	}

}