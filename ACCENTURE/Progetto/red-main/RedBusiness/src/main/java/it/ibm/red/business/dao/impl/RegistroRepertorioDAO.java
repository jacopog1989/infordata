/**
 * 
 */
package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IRegistroRepertorioDAO;
import it.ibm.red.business.dto.RegistroRepertorioDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * @author APerquoti
 *
 */
@Repository
public class RegistroRepertorioDAO extends AbstractDAO implements IRegistroRepertorioDAO {

	private static final String DESCRIZIONE = "DESCRIZIONE";

	private static final String IDAOO = "IDAOO";

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -1301458953793678369L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RegistroRepertorioDAO.class);

	/**
	 * @see it.ibm.red.business.dao.IRegistroRepertorioDAO#getRegistriRepertorioByAoo(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public List<RegistroRepertorioDTO> getRegistriRepertorioByAoo(final Long idAoo, final Connection conn) {
		final List<RegistroRepertorioDTO> output = new ArrayList<>();
		final StringBuilder sb = new StringBuilder();
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try {
			
			sb.append("SELECT rr.*, trr.DESCRIZIONE ")
			  .append("FROM REGISTRO_REPERTORIO rr, TIPOLOGIA_REGISTRO_REPERTORIO trr ")
			  .append("WHERE rr.idaoo = ? ")
			  .append("AND rr.REGISTRO_REPERTORIO = trr.ID_TIPOLOGIA ");
			
			ps = conn.prepareStatement(sb.toString());
			
			final int index = 1;
			ps.setLong(index, idAoo);
			
			rs = ps.executeQuery();
			
			while (rs.next()) {
				output.add(new RegistroRepertorioDTO(rs.getLong(IDAOO), rs.getLong("IDTIPOPROCEDIMENTO"), 
													 rs.getLong("IDTIPOLOGIADOCUMENTO"), rs.getString(DESCRIZIONE), rs.getString("LABEL_STAMPIGLIATURA")));
				
			}
			
		} catch (final Exception e) {
			LOGGER.error("", e);
			throw new RedException("", e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return output;
	}

	/**
	 * @see it.ibm.red.business.dao.IRegistroRepertorioDAO#getDenominazioneRegistroUfficiale(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public RegistroRepertorioDTO getDenominazioneRegistroUfficiale(final Long idAoo, final Connection conn) {
		RegistroRepertorioDTO denominazioneRegistroNPS = new RegistroRepertorioDTO();
		PreparedStatement ps = null;
		ResultSet rs = null;
		final StringBuilder sb = new StringBuilder();
		
		try { 
			
			sb.append("SELECT * ")
			  .append("FROM ( ")
			  
				  .append("SELECT npsc.* ")
				  .append("FROM NPS_CONFIGURATION npsc JOIN AOO a ")
				  .append("ON npsc.IDAOO = a.IDAOO ")
			  
			  .append(") ")
			  .append("WHERE IDAOO = ?");
			
			ps = conn.prepareStatement(sb.toString());
			final int index = 1;
			ps.setLong(index, idAoo);
			rs = ps.executeQuery();
			
			while (rs.next()) {
				denominazioneRegistroNPS = new RegistroRepertorioDTO(rs.getLong(IDAOO), rs.getString("DENOMINAZIONE_REGISTRO"));
			}
		} catch (final Exception e) {	
			LOGGER.error("Errore nel recupero del registro repertorio", e);
			throw new RedException("Errore nel recupero del registro repertorio", e);
		} finally {
			closeStatement(ps, rs);
		}
		return denominazioneRegistroNPS;
	}
	
	/**
	 * @see it.ibm.red.business.dao.IRegistroRepertorioDAO#getAllRegistroRepertorio(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public List<RegistroRepertorioDTO> getAllRegistroRepertorio(final Long idAoo, final Connection conn) {
		final List<RegistroRepertorioDTO> registriRepertorio = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		final StringBuilder sb = new StringBuilder();
		try {
			sb.append("SELECT DISTINCT rr.idaoo, trr.descrizione ")
				.append("FROM TIPOLOGIA_REGISTRO_REPERTORIO trr JOIN REGISTRO_REPERTORIO rr ")
				.append("ON trr.id_tipologia = rr.registro_repertorio ")
				.append("WHERE rr.idaoo = ?");
			
			ps = conn.prepareStatement(sb.toString());
			final int index = 1;
			ps.setLong(index, idAoo);
			rs = ps.executeQuery();
			 
			while (rs.next()) {
				registriRepertorio.add(new RegistroRepertorioDTO(rs.getLong(IDAOO), rs.getString(DESCRIZIONE)));
			}
			
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dei tipi di registri repertorio", e);
			throw new RedException("Errore nel recupero dei tipi di registri repertorio", e);
		} finally {
			closeStatement(ps, rs);
		}
		return registriRepertorio;
	}

	/**
	 * @see it.ibm.red.business.dao.IRegistroRepertorioDAO#getDescrForShowEtichetteByAoo(java.lang.Long,
	 *      java.sql.Connection).
	 */
	@Override
	public List<String> getDescrForShowEtichetteByAoo(final Long idAoo, final Connection conn) {
		final List<String> descrRegistro = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		final StringBuilder sb = new StringBuilder();
		try {  
			sb.append("SELECT DISTINCT SER.AOO, TRR.DESCRIZIONE ")
			.append("FROM SHOW_ETICHETTE_REGISTRO SER JOIN TIPOLOGIA_REGISTRO_REPERTORIO TRR ")
			.append("ON SER.ID_REGISTRO = TRR.ID_TIPOLOGIA ")
			.append("WHERE SER.AOO = ? AND SER.SHOW_ETICHETTE = 1");

			ps = conn.prepareStatement(sb.toString());
			final int index = 1;
			ps.setLong(index, idAoo); 
			rs = ps.executeQuery();

			while (rs.next()) {
				descrRegistro.add(rs.getString(DESCRIZIONE));
			}

		} catch (final Exception ex) {
			LOGGER.error("Errore nel recupero della descrizione per la stampa etichette");
			throw new RedException("Errore nel recupero della descrizione per la stampa etichette", ex);
		} finally {
			closeStatement(ps, rs);
		}
		return descrRegistro;
	}
}
