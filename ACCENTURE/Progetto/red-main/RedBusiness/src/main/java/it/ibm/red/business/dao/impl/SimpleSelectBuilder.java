package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Classe builder per la costruzione di query tipo SELECT.
 */
public class SimpleSelectBuilder extends SimpleQueryBuilder {

	/**
	 * La costante serial Version UID.
	 */
	private static final long serialVersionUID = 1228267407878391943L;
	/**
	 * Mappa operazioni.
	 */
	private final Map<String, Operation> operations;

	/**
	 * Lista delle operazioni.
	 */
	enum Operation {
	    /**
		 * Valore.
		 */
		EQUAL, 
		
	    /**
		 * Valore.
		 */
		CONTAINS;
	}

	/**
	 * Costruttore.
	 * @param inTable
	 */
	public SimpleSelectBuilder(final String inTable) {
		super(inTable);
		operations = new HashMap<>();
	}

	/**
	 * Gestisce l'operazione SELECT restituendo il preparedStatement con il quale eseguire la select.
	 * @param con
	 * @param selectFields
	 * @param orderBy
	 * @return PreparedStatement con il quale eseguire la select
	 * @throws SQLException
	 */
	public PreparedStatement select(final Connection con, final String selectFields, final String orderBy) throws SQLException {
		StringBuilder query = new StringBuilder("SELECT ").append(selectFields).append(" FROM ").append(getTable()).append(" WHERE 1=1 AND ");
		
		for (final Entry<String, Object> param:getParams().entrySet()) {
			if (param.getValue() != null) {
				query.append(createWhereCondition(param));
			}
		}

		query = new StringBuilder(query.substring(0, query.length() - 4));
		
		query.append(" ").append(orderBy);
		return con.prepareStatement(query.toString());

	}

	private String createWhereCondition(final Entry<String, Object> param) {
		final Operation op = operations.get(param.getKey());
		String out = null;
		if (Operation.EQUAL.equals(op)) {
			out = param.getKey() + " = " + param.getValue();
		} else if (Operation.CONTAINS.equals(op)) {
			out = "upper(" + param.getKey() + ") like '%" + param.getValue().toString().toUpperCase() + "%'";
		}
		out += " AND ";
		return out;
	}

	/**
	 * Consente l'aggiunta di un parametro evitando di gestire null value o empty string.
	 * @param key
	 * @param op
	 * @param value
	 */
	public void addParams(final String key, final Operation op, final Object value) {
		addParams(false, false, key, value);
		operations.put(key, op);
	}

}
