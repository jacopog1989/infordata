package it.ibm.red.business.enums;

/**
 * The Enum TrasformerPEEnum.
 *
 * @author CPIERASC
 * 
 *         Enum per le possibili trasformazioni di oggetti del PE.
 */
public enum TrasformerPEEnum {
	
	/**
	 * Da workflow a documento fepa.
	 */
	FROM_WF_TO_DOCUMENTO_FEPA,
	
	/**
	 * Gestione workflow documenti.
	 */
	FROM_WF_TO_DOCUMENTO,

	/**
	 * Gestione workflow documenti.
	 */
	FROM_WF_TO_EVENTO,
	
	/**
	 * Gestione modifica iter.
	 */
	MODIFICA_ITER, 
	
	/**
	 * Scadenziario Strutture.
	 */
	FROM_WF_TO_DOCUMENTO_SCADENZATO,
	
	/**
	 * Gestione workflow documenti con contesto.
	 */
	FROM_WF_TO_DOCUMENTO_CONTEXT;
}
