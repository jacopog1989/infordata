package it.ibm.red.business.dto;

import it.ibm.red.business.enums.ModalitaDestinatarioEnum;
import it.ibm.red.business.enums.TipologiaDestinatariEnum;

/**
 * The Class DestinatarioEsternoDTO.
 * 
 * Rappresenta un destinatario presente in rubrica.
 *
 * @author adilegge
 * 
 */
public class DestinatarioEsternoDTO extends AbstractDTO {

	private static final long serialVersionUID = 7587457073538520671L;

	/**
	 * ID destinatario.
	 */
	private Long idDestinatario;

	/**
	 * Flag tipo spedizione.
	 */
	private TipologiaDestinatariEnum tipoSpedizioneDestinatario; // PEO/PEC/CARTACEO

	/**
	 * Flag destinatario.
	 */
	private ModalitaDestinatarioEnum modalitaDestinatario; // TO/CC

	
	/**
	 * @param idDestinatario
	 * @param tipoSpedizioneDestinatario
	 * @param modalitaDestinatario
	 */
	public DestinatarioEsternoDTO(final Long idDestinatario, final TipologiaDestinatariEnum tipoSpedizioneDestinatario, 
			final ModalitaDestinatarioEnum modalitaDestinatario) {
		super();
		this.idDestinatario = idDestinatario;
		this.tipoSpedizioneDestinatario = tipoSpedizioneDestinatario;
		this.modalitaDestinatario = modalitaDestinatario;
	}

	/**
	 * Restituisce l'id del destinatario.
	 * @return idDestinatario
	 */
	public Long getIdDestinatario() {
		return idDestinatario;
	}

	/**
	 * Imposta l'id del destinatario.
	 * @param idDestinatario
	 */
	public void setIdDestinatario(final Long idDestinatario) {
		this.idDestinatario = idDestinatario;
	}

	/**
	 * Restituisce la tiplogia di destinatario dello specifico destinatario.
	 * @see TipologiaDestinatariEnum
	 * @return tipoSpedizioneDestinatario
	 */
	public TipologiaDestinatariEnum getTipoSpedizioneDestinatario() {
		return tipoSpedizioneDestinatario;
	}

	/**
	 * Imposta la tipologia di destinatario.
	 * @param tipoSpedizioneDestinatario
	 */
	public void setTipoSpedizioneDestinatario(final TipologiaDestinatariEnum tipoSpedizioneDestinatario) {
		this.tipoSpedizioneDestinatario = tipoSpedizioneDestinatario;
	}

	/**
	 * Restituisce la modalità di destinatario indicando se si tratta di un destinatario principale o in copia conoscenza.
	 * @see ModalitaDestinatarioEnum
	 * @return modalitaDestinatario
	 */
	public ModalitaDestinatarioEnum getModalitaDestinatario() {
		return modalitaDestinatario;
	}

	/**
	 * Imposta la modalità destinatario.
	 * @param modalitaDestinatario
	 */
	public void setModalitaDestinatario(final ModalitaDestinatarioEnum modalitaDestinatario) {
		this.modalitaDestinatario = modalitaDestinatario;
	}

}