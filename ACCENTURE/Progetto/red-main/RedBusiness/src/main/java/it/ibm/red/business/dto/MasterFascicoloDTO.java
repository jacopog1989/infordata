package it.ibm.red.business.dto;

import java.util.Collection;
import java.util.Date;

/**
 * @author 
 *
 */
public class MasterFascicoloDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -5303354003816776014L;
	
	/**
	 * Id del master.
	 */
	private final Integer id;
	
	/**
	 * Oggetto del master.
	 */
	private final String oggetto;
	
	/**
	 * Data di creazione del master.
	 */
	private Date dataCreazione;
	
	/**
	 * Documenti.
	 */
	private Collection<DocumentoFascicoloDTO> documenti;
	

	/**
	 * Id aoo.
	 */
	private Long idAOO;
	 

	/**
	 * Stato fascicolo.
	 */
	private String statoFascicolo;
	
	/**
	 * @param id
	 * @param oggetto
	 */
	public MasterFascicoloDTO(final Integer id, final String oggetto) {
		this.id = id;
		this.oggetto = oggetto;
	}
	
	/**
	 * @param id
	 * @param oggetto
	 * @param dataCreazione
	 */
	public MasterFascicoloDTO(final Integer id, final String oggetto, final Date dataCreazione, final Long idAOO) {
		this(id, oggetto);
		this.dataCreazione = dataCreazione;
		this.idAOO = idAOO;
	}
	
	/**
	 * Costruttore del DTO.
	 * @param id
	 * @param oggetto
	 * @param dataCreazione
	 * @param idAOO
	 * @param statoFascicolo
	 */
	public MasterFascicoloDTO(final Integer id, final String oggetto, final Date dataCreazione, final Long idAOO, final String statoFascicolo) {
		this(id, oggetto);
		this.dataCreazione = dataCreazione;
		this.idAOO = idAOO;
		this.statoFascicolo = statoFascicolo;
	}
	
	/**
	 * @return id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return oggetto
	 */
	public String getOggetto() {
		return oggetto;
	}
	
	/**
	 * @return Collection di DocumentoFascicoloDTO
	 */
	public Collection<DocumentoFascicoloDTO> getDocumenti() {
		return documenti;
	}
	
	/**
	 * @param documenti
	 */
	public void setDocumenti(final Collection<DocumentoFascicoloDTO> documenti) {
		this.documenti = documenti;
	}
	/**
	 * @return dataCreazione
	 */
	public Date getDataCreazione() {
		return dataCreazione;
	}

	/**
	 * Restituisce l'id dell'Area Organizzativa Omogenea.
	 * @return id AOO
	 */
	public Long getIdAOO() {
		return idAOO;
	}
	
	/**
	 * Restituisce lo stato del fascicolo.
	 * @return stato fascicolo.
	 */
	public String getStatoFascicolo() {
		return statoFascicolo;
	}
	
	/**
	 * Imposta lo stato del fascicolo.
	 * @param statoFascicolo
	 */
	public void setStatoFascicolo(final String statoFascicolo) {
		this.statoFascicolo = statoFascicolo;
	}

}