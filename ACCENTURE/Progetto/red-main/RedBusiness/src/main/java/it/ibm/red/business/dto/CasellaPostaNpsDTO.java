/**
 * 
 */
package it.ibm.red.business.dto;

/**
 * @author m.crescentini
 *
 */
public class CasellaPostaNpsDTO extends ContattoPostaNpsDTO {

	private static final long serialVersionUID = -1347625000649909106L;
	
	/**
	 * Codice aoo.
	 */
	private final String codiceAoo;
	
	
	/**
	 * @param displayName
	 * @param mail
	 * @param codiceAoo
	 */
	public CasellaPostaNpsDTO(final String displayName, final String mail, final String codiceAoo) {
		super(displayName, mail);
		this.codiceAoo = codiceAoo;
	}


	/**
	 * @return the codiceAoo
	 */
	public String getCodiceAoo() {
		return codiceAoo;
	}
	
}
