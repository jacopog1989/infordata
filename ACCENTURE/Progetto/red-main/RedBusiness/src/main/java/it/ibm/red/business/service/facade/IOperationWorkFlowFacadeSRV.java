package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.sql.Connection;
import java.util.Collection;

import it.ibm.red.business.dto.DocumentAppletContentDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;

/**
 * Interfaccia dei metodi delle operazioni di sigla/vista/rifiuta.
 * 
 * @author CPAOLUZI
 *
 */
public interface IOperationWorkFlowFacadeSRV extends Serializable {
	
	/**
	 * Response "Procedi da Corriere".
	 * @param fcDTO
	 * @param wobNumber
	 * @param utente
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO procediDaCorriere(FilenetCredentialsDTO fcDTO, String wobNumber, UtenteDTO utente);
	
	/**
	 * Metodo per implementare la transizione generica (HomeGenericNoWindowController).
	 * @param wobNumber
	 * @param utente
	 * @param response
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO genericNextStep(String wobNumber, UtenteDTO utente, String response);
	
	/**
	 * Metodo per implementare la transizione generica (HomeGenericNoWindowController).
	 * @param wobNumber
	 * @param utente
	 * @param response
	 * @param motivazioneAssegnazione
	 * @param fceh
	 * @param fpeh
	 * @param connection
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO genericNextStep(String wobNumber, UtenteDTO utente, String response, String motivazioneAssegnazione, IFilenetCEHelper fceh, FilenetPEHelper fpeh, Connection connection);
	
	/**
	 * Metodo per implementare la transizione generica (HomeGenericNoWindowController).
	 * @param wobNumbers
	 * @param utente
	 * @param response
	 * @return esiti dell'operazione
	 */
	Collection<EsitoOperazioneDTO> genericNextStep(Collection<String> wobNumbers, UtenteDTO utente, String response);
	
	/**
	 * Response "Procedi da Corriere" massiva.
	 * @param wobNumbers
	 * @param utente
	 * @return esiti dell'operazione
	 */
	Collection<EsitoOperazioneDTO> procediDaCorriere(Collection<String> wobNumbers, UtenteDTO utente);
	
	/**
	 * Response "Spedisci Mail".
	 * @param utente
	 * @param wobNumber
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO spedisciMail(UtenteDTO utente, String wobNumber);
	
	/**
	 * Response "Spedisci Mail" massiva.
	 * @param utente
	 * @param wobNumbers
	 * @return esiti dell'operazione
	 */
	Collection<EsitoOperazioneDTO> spedisciMail(UtenteDTO utente, Collection<String> wobNumbers);
	
	/**
	 * Response "Sposta in Lavorazione".
	 * @param utente
	 * @param wobNumber
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO spostaInLavorazione(UtenteDTO utente, String wobNumber);
	
	/**
	 * Response "Sposta in Lavorazione" massiva.
	 * @param utente
	 * @param wobNumbers
	 * @return esiti dell'operazione
	 */
	Collection<EsitoOperazioneDTO> spostaInLavorazione(UtenteDTO utente, Collection<String> wobNumbers);
	
	/**
	 * Ottiene le info del documento per l'applet.
	 * @param workflowNumber
	 * @param utente
	 * @return DocumentAppletContentDTO
	 */
	DocumentAppletContentDTO getDocumentInfoForApplet(String workflowNumber, UtenteDTO utente);

	/**
	 * Avanza il workflow e storna il workflow contributi.
	 * @param utente
	 * @param wobNumber
	 * @param motivoAssegnazione
	 * @param tipoResponse
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO avanzaWorkflowEStornaWfContributi(UtenteDTO utente, String wobNumber, String motivoAssegnazione, 
			ResponsesRedEnum tipoResponse);

	/**
	 * Effettua le richieste di firma Copia Conforme.
	 * @param utente
	 * @param wobNumbers
	 * @return esiti dell'operazione
	 */
	Collection<EsitoOperazioneDTO> richiediFirmaCopiaConforme(UtenteDTO utente, Collection<String> wobNumbers);

	/**
	 * Effettua la richiesta di firma Copia Conforme.
	 * @param utente
	 * @param wobNumber
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO richiediFirmaCopiaConforme(UtenteDTO utente, String wobNumber);


}