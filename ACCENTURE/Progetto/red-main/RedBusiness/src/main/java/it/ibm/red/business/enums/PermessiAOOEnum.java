package it.ibm.red.business.enums;

/**
 * The Enum PermessiAOOEnum. Enum dei permessi associati ai vari ruoli.
 */
public enum PermessiAOOEnum {

	
	/**
	 * Valore.
	 */
	PRESA_IN_CARICO_AUTOMATICA(1L),
	
	/**
	 * Valore.
	 */
	PROTOCOLLAZIONE_MULTIPLA_IN(2L),
	
	/**
	 * Valore.
	 */
	ALERT_MODIFICA_DOCUMENTO(4L),
	
	/**
	 * Valore.
	 */
	DOC_IN_ENTRATA_NO_PROTOCOLLATI(8L),
	
	/**
	 * Valore.
	 */
	DOC_ORIGINALE_USCITA(16L),
	
	/**
	 * Valore.
	 */
	LIBRO_FIRMA_UNICO(32L),
	
	/**
	 * Valore.
	 */
	SCANSIONE_PRECEDENTE_ACQUISIZIONE(64L),
	
	/**
	 * Valore.
	 */
	GESTIONE_REGISTRI_RISERVATI(128L),
	
	/**
	 * Valore.
	 */
	GESTIONE_CONTRIBUTO_ESTERNO(256L),
	
	/**
	 * Valore.
	 */
	GESTIONE_REPORT_SPECIFICI(512L),
	
	/**
	 * Valore.
	 */
	GESTIONE_VISTO(1024L),
	
	/**
	 * Valore.
	 */
	GESTIONE_ELENCO_TRASMISSIONE(2048L),
	
	/**
	 * Valore.
	 */
	FIRMA_COPIA_CONFORME(4096L),
	
	/**
	 * Valore.
	 */
	PREASSEGNAZIONE_DOCUMENTI(8192L),
	
	/**
	 * Valore.
	 */
	VISIBILITA_OSSERVAZIONE(16384L),
	
	/**
	 * Valore.
	 */
	AGGIUNGI_DOCUMENTO_FASCICOLO(32768L),
	
	/**
	 * Valore.
	 */
	CORTE_DEI_CONTI(65536L),
	
	/**
	 * Valore.
	 */
	FALDONATURA_AUTOMATICA(131072L),
	
	/**
	 * Valore.
	 */
	AVVIO_PROCEDIMENTI_PARZIALI(262144L),
	
	/**
	 * Valore.
	 */
	AL_GIRO_VISTI(524288L),
	
	/**
	 * Valore.
	 */
	CONTRIBUTO_ESTERNO(1048576L),
	
	/**
	 * Valore.
	 */
	MULTI_NOTE(2097152L),
	
	/**
	 * Valore.
	 */
	TEMPLATE_DOC_USCITA(4194304L);
	
	/**
	 * Identificativo.
	 */
	private Long id;

	/**
	 * Costruttore.
	 * 
	 * @param inId
	 *            identificativo
	 */
	PermessiAOOEnum(final Long inId) {
		this.id = inId;
	}

	/**
	 * Getter identificativo.
	 * 
	 * @return identificativo
	 */
	public Long getId() {
		return id;
	}

}
