package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IRicercaRedDAO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.RicercaAvanzataSalvata;


/**
 * The class RicercaRedDAO.
 *
 * @author mcrescentini
 * 
 * 	DAO per la gestione della ricerca RED.
 */
@Repository
public class RicercaRedDAO extends AbstractDAO implements IRicercaRedDAO {
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RicercaRedDAO.class);

	/**
	 * @see it.ibm.red.business.dao.IRicercaRedDAO#getAllRicercheSalvateByUtente(long,
	 *      long, long, java.sql.Connection).
	 */
	@Override
	public List<RicercaAvanzataSalvata> getAllRicercheSalvateByUtente(final long idUtente, final long idUfficio, final long idRuolo, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<RicercaAvanzataSalvata> ricercheSalvate = null;
		
		try {
			int index = 1;
			
			ps = con.prepareStatement("SELECT * FROM RICERCA_AVANZATA_SALVATA r WHERE r.id_utente = ? AND r.id_nodo = ? AND r.id_ruolo = ? AND r.applicazione = '" + APPLICAZIONE_DB + "'");
			ps.setLong(index++, idUtente);
			ps.setLong(index++, idUfficio);
			ps.setLong(index++, idRuolo);
			
			rs = ps.executeQuery();

			ricercheSalvate = new ArrayList<>();
			RicercaAvanzataSalvata r = null;
			while (rs.next()) {
				r = new RicercaAvanzataSalvata();
				r.setId(rs.getInt("id_ricerca"));
				r.setIdUtente(rs.getLong("id_utente"));
				r.setIdUfficio(rs.getLong("id_nodo"));
				r.setIdRuolo(rs.getLong("id_ruolo"));
				r.setNome(rs.getString("nome"));
				r.setDataCreazione(rs.getDate("data_creazione"));
				
				ricercheSalvate.add(r);
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return ricercheSalvate;
	}
	
	/**
	 * @see it.ibm.red.business.dao.IRicercaRedDAO#insertRicercaAvanzataSalvata(it.ibm.red.business.persistence.model.RicercaAvanzataSalvata,
	 *      java.sql.Connection).
	 */
	@Override
	public Integer insertRicercaAvanzataSalvata(final RicercaAvanzataSalvata ricercaAvanzataSalvata, final Connection con)  {
		LOGGER.info("insertRicercaAvanzataSalvata -> START. ID utente: " + ricercaAvanzataSalvata.getIdUtente());
		PreparedStatement ps = null;
		ResultSet rs = null;
		Integer idRicerca = null;
		
		try {
			int index = 1;
			
			ps = con.prepareStatement("INSERT INTO RICERCA_AVANZATA_SALVATA (ID_RICERCA, ID_UTENTE, ID_NODO, ID_RUOLO, NOME, DATA_CREAZIONE, APPLICAZIONE)"
					+ " VALUES (SEQ_RIC_AV_SALVATA.NEXTVAL, ?, ?, ?, ?, SYSDATE, '" + APPLICAZIONE_DB + "')", new String[]{ "ID_RICERCA" });
			ps.setLong(index++, ricercaAvanzataSalvata.getIdUtente());
			ps.setLong(index++, ricercaAvanzataSalvata.getIdUfficio());
			ps.setLong(index++, ricercaAvanzataSalvata.getIdRuolo());
			ps.setString(index++, ricercaAvanzataSalvata.getNome());
			
			ps.executeUpdate();
	        
	        rs = ps.getGeneratedKeys();
	        if (rs.next()) {
	        	idRicerca = rs.getInt(1);
	        }
		} catch (Exception e) {
			LOGGER.error("insertRicercaAvanzataSalvata -> Si è verificato un errore nell'inserimento della ricerca salvata per l'utente: "
					+ ricercaAvanzataSalvata.getIdUtente(), e);
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		LOGGER.info("insertRicercaAvanzataSalvata -> END. ID utente: " + ricercaAvanzataSalvata.getIdUtente());
		return idRicerca;
	}
	
	/**
	 * @see it.ibm.red.business.dao.IRicercaRedDAO#insertRicercaCampoValore(int,
	 *      java.lang.String, java.lang.String, java.sql.Connection).
	 */
	@Override
	public void insertRicercaCampoValore(final int idRicerca, final String campo, final String valore, final Connection con) {
		PreparedStatement ps = null;
		
		try {
			int index = 1;
			
			ps = con.prepareStatement("INSERT INTO RICERCA_CAMPO_VALORE (ID_RICERCA, CAMPO, VALORE) VALUES (?, ?, ?)");
			ps.setInt(index++, idRicerca);
			ps.setString(index++, campo);
			ps.setString(index++, valore);
			
			ps.executeQuery();
		} catch (Exception e) {
			LOGGER.error("insertRicercaCampoValore -> Si è verificato un errore durante l'operazione. ID ricerca: " + idRicerca, e);
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}
	
	/**
	 * @see it.ibm.red.business.dao.IRicercaRedDAO#getCampiValoriByIdRicerca(int,
	 *      java.sql.Connection).
	 */
	@Override
	public Map<String, String> getCampiValoriByIdRicerca(final int idRicerca, final Connection con) {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Map<String, String> mappaCampiValori = new HashMap<>();
		
		try {
			int index = 1;
			
			ps = con.prepareStatement("SELECT * FROM RICERCA_CAMPO_VALORE r WHERE r.id_ricerca = ?");
			ps.setInt(index++, idRicerca);
			rs = ps.executeQuery();

			while (rs.next()) {
				mappaCampiValori.put(rs.getString("CAMPO"), rs.getString("VALORE"));
			}
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new RedException(e);
		} finally {
			closeStatement(ps, rs);
		}
		
		return mappaCampiValori;
	}
	
	/**
	 * @see it.ibm.red.business.dao.IRicercaRedDAO#eliminaRicercaAvanzataSalvata(int, java.sql.Connection).
	 */
	@Override
	public void eliminaRicercaAvanzataSalvata(final int idRicerca, final Connection con) {
		PreparedStatement ps = null;
		
		try {
			int index = 1;
			
			ps = con.prepareStatement("DELETE RICERCA_AVANZATA_SALVATA R WHERE R.ID_RICERCA = ?");
			ps.setInt(index++, idRicerca);
			
			ps.executeQuery();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			throw new RedException(e);
		} finally {
			closeStatement(ps);
		}
	}

}