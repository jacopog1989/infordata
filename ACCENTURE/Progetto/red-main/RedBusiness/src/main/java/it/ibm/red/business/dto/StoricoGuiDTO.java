package it.ibm.red.business.dto;

import it.ibm.red.business.dto.filenet.StoricoDTO;

/**
 * DTO che definisce un elemento dello storico grafico.
 */
public class StoricoGuiDTO extends AbstractDTO {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -8352726868473625347L;

	/**
	 * Storico.
	 */
	private StoricoDTO storico;
	
	/**
	 * Flag attiva download.
	 */
	private boolean attivitaDownloadable;
	
	/**
	 * Costruttore.
	 */
	public StoricoGuiDTO() {
		this(null, false);
	}

	/**
	 * Costruttore.
	 * @param storico
	 */
	public StoricoGuiDTO(final StoricoDTO storico) {
		this(storico, false);
	}
	
	/**
	 * Costruttore.
	 * @param storico
	 * @param attivitaDownloadble
	 */
	public StoricoGuiDTO(final StoricoDTO storico, final boolean attivitaDownloadble) {
		this.storico = storico;
		this.attivitaDownloadable = attivitaDownloadble;
	}
	
	/**
	 * Restituisce lo storico.
	 * @return storico
	 */
	public StoricoDTO getStorico() {
		return storico;
	}

	/**
	 * Imposta lo storico.
	 * @param storico
	 */
	public void setStorico(final StoricoDTO storico) {
		this.storico = storico;
	}

	/**
	 * Restituisce true se è possibile effettuare il download dell'attivita.
	 * @return true se è possibile effettuare il download dell'attivita, false altrimenti
	 */
	public boolean isAttivitaDownloadable() {
		return attivitaDownloadable;
	}

	/**
	 * Imposta il flag associato alla possibilita di download dell'attivita.
	 * @param attivitaDownloadable
	 */
	public void setAttivitaDownloadable(final boolean attivitaDownloadable) {
		this.attivitaDownloadable = attivitaDownloadable;
	}
}
