package it.ibm.red.business.helper.filenet.ce.trasform.impl;

import java.sql.Connection;
import java.util.Date;

import com.filenet.api.core.Document;

import it.ibm.red.business.dao.IAooDAO;
import it.ibm.red.business.dao.INodoDAO;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dao.ITipologiaDocumentoDAO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.persistence.model.TipoProcedimento;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.utils.StringUtils;

/**
 * Trasformer dal Documento al LightDetailRed.
 */
public class FromDocumentoToLightDetailRedTrasformer extends TrasformerCE<DetailDocumentRedDTO> {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 3336074476679033461L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FromDocumentoToLightDetailRedTrasformer.class);

	/**
	 * Dao per recuperare la tipologia del documento.
	 */
	private ITipologiaDocumentoDAO tipologiaDocumentoDAO;

	/**
	 * Dao nodo.
	 */
	private INodoDAO nodoDAO;
	
	/**
	 * DAO per recuperare l'aoo e l'ente
	 */
	private IAooDAO aooDAO;

	/**
	 * Dao per recuperare il tipo procedimento
	 */
	private ITipoProcedimentoDAO tipoProcedimentoDAO;
	
	/**
	 * Costruttore.
	 */
	public FromDocumentoToLightDetailRedTrasformer() {
		super(TrasformerCEEnum.FROM_DOCUMENTO_TO_LIGHT_DETAIL_RED);
		nodoDAO = ApplicationContextProvider.getApplicationContext().getBean(INodoDAO.class);
		tipologiaDocumentoDAO = ApplicationContextProvider.getApplicationContext().getBean(ITipologiaDocumentoDAO.class);
		aooDAO = ApplicationContextProvider.getApplicationContext().getBean(IAooDAO.class);
		tipoProcedimentoDAO = ApplicationContextProvider.getApplicationContext().getBean(ITipoProcedimentoDAO.class);
	}

	/**
	 * @see it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE#trasform(com.filenet.api.core.Document,
	 *      java.sql.Connection).
	 */
	@Override
	public DetailDocumentRedDTO trasform(final Document document, final Connection connection) {
		if (document == null) {
			return null;
		}
		
		try {
			DetailDocumentRedDTO d = new DetailDocumentRedDTO();
			d.setGuid(StringUtils.cleanGuidToString(document.get_Id()));
			d.setDocumentTitle((String) getMetadato(document, PropertiesNameEnum.DOCUMENT_TITLE_METAKEY));
	
			d.setAnnoDocumento((Integer) getMetadato(document, PropertiesNameEnum.ANNO_DOCUMENTO_METAKEY));
			d.setNumeroDocumento((Integer) getMetadato(document, PropertiesNameEnum.NUMERO_DOCUMENTO_METAKEY));
			d.setIdUtenteCreatore((Integer) getMetadato(document, PropertiesNameEnum.UTENTE_CREATORE_ID_METAKEY));
			d.setIdUfficioCreatore((Integer) getMetadato(document, PropertiesNameEnum.UFFICIO_CREATORE_ID_METAKEY));
			d.setDateCreated((Date) getMetadato(document, PropertiesNameEnum.DATA_CREAZIONE_METAKEY));
			
			if (d.getIdUfficioCreatore() != null && d.getIdUfficioCreatore() > 0) {
				d.setUfficioMittente(nodoDAO.getNodo(Long.valueOf(d.getIdUfficioCreatore()), connection));
			}
			
			// tipologia documento start
			d.setIdTipologiaDocumento((Integer) getMetadato(document, PropertiesNameEnum.TIPOLOGIA_DOCUMENTO_ID_METAKEY));
			if (d.getIdTipologiaDocumento() != null) {
				TipologiaDocumentoDTO tipologiaDocumento = tipologiaDocumentoDAO.getById(d.getIdTipologiaDocumento(), connection);
				d.setDescTipologiaDocumento(tipologiaDocumento.getDescrizione());
			}
			// tipologia documento end
			
			d.setOggetto((String) getMetadato(document, PropertiesNameEnum.OGGETTO_METAKEY));
			d.setAnnoProtocollo((Integer) getMetadato(document, PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY));
			d.setNumeroProtocollo((Integer) getMetadato(document, PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY));
			d.setTipoProtocollo((Integer) getMetadato(document, PropertiesNameEnum.TIPO_PROTOCOLLO_METAKEY));
			d.setIdProtocollo((String) getMetadato(document, PropertiesNameEnum.ID_PROTOCOLLO_METAKEY));
			d.setDataProtocollo((Date) getMetadato(document, PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY));
			d.setCodiceFlusso((String) getMetadato(document, PropertiesNameEnum.CODICE_FLUSSO_FN_METAKEY));
			
			d.setNomeFile((String) getMetadato(document, PropertiesNameEnum.NOME_FILE_METAKEY));
			
			d.setIdAOO((Integer) getMetadato(document, PropertiesNameEnum.ID_AOO_METAKEY));
			Aoo aoo = aooDAO.getAoo(d.getIdAOO().longValue(), connection);
			d.setAoo(aoo);
			
			
			//tipologia procedimento start
			d.setIdTipologiaProcedimento((Integer) getMetadato(document, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY));
			if (d.getIdTipologiaProcedimento() != null) {
				TipoProcedimento t = tipoProcedimentoDAO.getTPbyId(connection, d.getIdTipologiaProcedimento()); 
				d.setDescTipoProcedimento(t.getDescrizione());
			}
			//tipologia procedimento end
			
			return d;
			
		} catch (Exception e) {
			LOGGER.error("Errore nel recupero di un metadato del dettaglio LIGHT del documento: ", e);
			return null;
		}
	}

}
