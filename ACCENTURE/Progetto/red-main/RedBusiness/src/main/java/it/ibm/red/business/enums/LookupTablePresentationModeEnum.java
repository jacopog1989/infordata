package it.ibm.red.business.enums;

/**
 * Enum che definisce i tipi di presentazione di un metadato di tipo @see LookupTableDTO.
 */
public enum LookupTablePresentationModeEnum {

	/**
	 * combobox
	 */
	COMBOBOX("C", "COMBOBOX"),

	/**
	 * datatable
	 */
	DATATABLE("D", "DATATABLE");

	
	/**
	 * Codice.
	 */
	private String code;
	
	/**
	 * Descrizione.
	 */
	private String description;

	LookupTablePresentationModeEnum(final String code, final String description) {
		this.code = code;
		this.description = description;
	}

	/**
	 * @return code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @return description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Restituisce la modalità di visualizzazione
	 * della lookup table in base al codice in input.
	 * @param inCode
	 * @return lookupTablePresentationModeEnum
	 */
	public static LookupTablePresentationModeEnum get(final String inCode) {
		LookupTablePresentationModeEnum output = null;

		for (final LookupTablePresentationModeEnum e: LookupTablePresentationModeEnum.values()) {
			if (e.getCode().equalsIgnoreCase(inCode)) {
				output = e;
				break;
			}
		}
		return output;
	}
}
