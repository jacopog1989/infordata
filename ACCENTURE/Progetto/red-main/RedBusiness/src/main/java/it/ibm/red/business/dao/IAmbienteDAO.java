package it.ibm.red.business.dao;

import java.sql.Connection;

/**
 * DAO Ambiente.
 */
public interface IAmbienteDAO {

	/**
	 * Ottiene il tipo di ambiente.
	 * @param con
	 * @return ambiente
	 */
	 String getAmbiente(Connection con);
	
}
