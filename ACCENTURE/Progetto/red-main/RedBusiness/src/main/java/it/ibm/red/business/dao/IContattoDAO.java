package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.RicercaRubricaDTO;
import it.ibm.red.business.enums.TipoRubricaEnum;
import it.ibm.red.business.persistence.model.Contatto;

/**
 * 
 * @author CPIERASC
 *
 *         Dao contatto.
 */
public interface IContattoDAO extends Serializable {

	/**
	 * Recupera contatti per id.
	 * 
	 * @param contattiIDs
	 *            id
	 * @param connection
	 *            connessione
	 * @return contatti
	 */
	Collection<Contatto> getContattiByIDs(List<Long> contattiIDs, Connection connection);

	/**
	 * Recupera contatto per id.
	 * 
	 * @param contattoID
	 *            id
	 * @param connection
	 *            connessione
	 * @return contatto
	 */
	Contatto getContattoByID(Long contattoID, Connection connection);

	/**
	 * Rimuove il contatto/gruppo dai preferiti.
	 * @param idUfficio - id dell'ufficio
	 * @param contattoID - id del contatto
	 * @param tipoRubrica
	 * @param connection
	 */
	void rimuoviPreferito(Long idUfficio, Long contattoID, TipoRubricaEnum tipoRubrica, Connection connection);

	/**
	 * Aggiunge il contatto/gruppo ai preferiti.
	 * @param idUfficio - id dell'ufficio
	 * @param contattoID - id del contatto
	 * @param tipoRubrica
	 * @param idUtente - id dell'utente
	 * @param connection
	 */
	void aggiungiAiPreferiti(Long idUfficio, Long contattoID, TipoRubricaEnum tipoRubrica, Long idUtente, Connection connection);

	// IdUfficio è necessario per effettuare una left join e capire se quel contatto
	// è un preferito per quell'ufficio
	/**
	 * Ricerca contatti.
	 * @param idUfficio - id dell'ufficio
	 * @param ricercaContattoObj
	 * @param connection
	 * @return lista di contatti
	 */
	List<Contatto> ricerca(Long idUfficio, RicercaRubricaDTO ricercaContattoObj, Connection connection);

	/**
	 * Inserisce il contatto in input nella rubrica red.
	 * 
	 * @param contatto
	 * @return l'id del contatto appena inserito
	 */


	Long inserisci(Long idUfficio, Contatto inserisciContattoItem, Connection connection);

	/**
	 * Esegue un update sul contatto.
	 * @param contatto
	 * @param con
	 * @return
	 */
	int updateContatto(Contatto contatto, Connection con);

	/**
	 * Ottiene i contatti preferiti per alias.
	 * @param aliasDescrizione
	 * @param idUfficio - id dell'ufficio
	 * @param idAOO - id dell'Aoo
	 * @param connection
	 * @return lista di contatti
	 */
	List<Contatto> getContattiPreferitiPerAlias(String aliasDescrizione, Long idUfficio, Long idAOO,
			Connection connection);

	/**
	 * Ottiene la struttura di I livello contatti MEF dal DB.
	 * @param con
	 * @return lista del I livello
	 */
	List<String> caricaIlivello(Connection con);

	/**
	 * Ottiene la struttura di II livello contatti MEF dal DB.
	 * @param idComboILivello
	 * @param con
	 * @return lista del II livello
	 */
	List<String> caricaIIlivello(String idComboILivello, Connection con);

	/**
	 * Ottiene la struttura di III livello contatti MEF dal DB.
	 * @param idcomboIILivello
	 * @param con
	 * @return lista del III livello
	 */
	List<String> caricaIIIlivello(String idcomboIILivello, Connection con);

	/**
	 * Modifica il contatto sulla base dati.
	 * @param contatto
	 * @param connection
	 */
	void modifica(Contatto contatto, Connection connection);

	/**
	 * Elimina il contatto sulla base dati.
	 * @param idContatto - id del contatto
	 * @param con
	 * @return
	 */
	int eliminaContatto(Long idContatto, Connection con);

	/**
	 * Elimina il gruppo sulla base dati.
	 * @param gruppo
	 * @param connection
	 */
	void eliminaGruppo(Contatto gruppo, Connection connection);

	/**
	 * Crea un gruppo sulla base dati.
	 * @param gruppo
	 * @param contattiSelezionati
	 * @param idUfficio - id dell'ufficio
	 * @param idUtente - id dell'utente
	 * @param connection
	 */
	void creaGruppo(Contatto gruppo, Collection<Contatto> contattiSelezionati, Long idUfficio, Long idUtente, Connection connection);

	/**
	 * Ricerca contatto per modifica tramite id.
	 * @param idContatto - id del contatto
	 * @param connection
	 * @return contatto
	 */
	Contatto ricercaContattoPerModificaByID(long idContatto, Connection connection);

	/**
	 * Ottiene tutti i gruppi.
	 * 
	 * @param idUfficio
	 *            Identificativo dell'ufficio, viene usato per capire la preferenza
	 *            da impostare al gruppo.
	 * @param connection
	 *            Connessione al database.
	 * @return Lista dei contatti dei gruppi.
	 */
	List<Contatto> getAllGruppi(Long idUfficio, Connection connection);

	/**
	 * Modifica il gruppo.
	 * @param gruppo
	 * @param contattiSelezionati
	 * @param contattidaRimuovere
	 * @param idUfficio - id dell'ufficio
	 * @param connection
	 */
	void modificaGruppo(Contatto gruppo, List<Contatto> contattiSelezionati, List<Contatto> contattidaRimuovere,
			Long idUfficio, Connection connection);

	/**
	 * Ricerca i figli del contatto IPA.
	 * @param contattoIpa
	 * @param con
	 * @return lista di contatti figli
	 */
	List<Contatto> ricercaFigliContattoIPA(Contatto contattoIpa, Connection con);

	/**
	 * Ottiene i gruppi email per alias.
	 * @param casellaPostale - casella postale
	 * @param nomeGruppo - nome del gruppo
	 * @param con
	 * @return lista di contatti
	 */
	List<Contatto> getListaGruppoEmailByAlias(String casellaPostale, String nomeGruppo, Connection con);

	/**
	 * Ottiene i contatti mail per alias.
	 * @param casellaPostale - casella postale
	 * @param nomeContatto - nome del contatto
	 * @param con
	 * @return lista di contatti
	 */
	List<Contatto> getContattiMailFromAlias(String casellaPostale, String nomeContatto, Connection con);

	/**
	 * Inserisce il contatto casella postale sulla base dati.
	 * @param c - contatto
	 * @param connection
	 * @return id del contatto
	 */
	long inserisciContattoCasellaPostale(Contatto c, Connection connection);

	/**
	 * Associa il contatto alla casella postale.
	 * @param contattoID - id del contatto
	 * @param casellaPostale - casella postale
	 * @param connection
	 * @return 
	 */
	int associaContattoACasellaPostale(long contattoID, String casellaPostale, Connection connection);

	/**
	 * Ricerca un duplicato in rubrica.
	 * @param contattoToCheck - contatto da verificare
	 * @param connection
	 * @return true se esiste un duplicato, false in caso contrario
	 */
	boolean checkEsistenzaContattoRED(Contatto contattoToCheck, Connection connection);

	/**
	 * Ottiene i contatti tramite l'email.
	 * @param casellaPostale - casella postale
	 * @param emailContatto - email del contatto
	 * @param con
	 * @return lista di contatti
	 */
	List<Contatto> getContattiByEmail(String casellaPostale, String emailContatto, Connection con);

	/**
	 * Ottiene il contatto tramite la denominazione.
	 * @param denominazione
	 * @param con
	 * @return contatto
	 */
	Contatto getContattoByDenominazione(String denominazione, Connection con);

	/**
	 * Aggiorna il contatto casella postale sulla base dati.
	 * @param contatto
	 * @param con
	 * @return
	 */
	long updateContattoCasellaPostale(Contatto contatto, Connection con);

	/**
	 * Inserisce il contatto casella postale sulla base dati.
	 * @param gruppoEmail
	 * @param con
	 * @return
	 */
	int inserisciGruppoEmail(Contatto gruppoEmail, Connection con);

	/**
	 * Aggiorna il gruppo sulla base dati.
	 * @param gruppoEmail
	 * @param con
	 * @return
	 */
	int updateGruppoEmail(Contatto gruppoEmail, Connection con);

	/**
	 * Aggiorna il contatto casella postale sulla base dati.
	 * @param idGruppoEmail
	 * @param idContatto - id del contatto
	 * @param con
	 * @return
	 */
	int associaContattoGruppoMail(long idGruppoEmail, long idContatto, Connection con);

	/**
	 * Associa la casella postale al gruppo.
	 * @param idGruppoEmail
	 * @param indirizzoEmail
	 * @param con
	 * @return
	 */
	int associaCasellaPostaleGruppo(long idGruppoEmail, String indirizzoEmail, Connection con);

	/**
	 * Elimina l'associazione del contatto al gruppo.
	 * @param idGruppoEmail
	 * @param idContatto
	 * @param con
	 * @return
	 */
	int eliminaAssociazioneContattoGruppo(long idGruppoEmail, long idContatto, Connection con);

	/**
	 * Elimina l'associazione della casella postale al gruppo.
	 * @param idGruppoEmail
	 * @param indirizzoEmail
	 * @param con
	 * @return
	 */
	int eliminaAssociazioneCasellaPostaleGruppo(long idGruppoEmail, String indirizzoEmail, Connection con);

	/**
	 * Ottiene i gruppi email tramite l'indirizzo email.
	 * @param indirizzoEmail - indirizzo email
	 * @param nomeGruppo - nome del gruppo
	 * @param con
	 * @return lista di contatti
	 */
	List<Contatto> getListaGruppoEmailByNomeEmail(String indirizzoEmail, String nomeGruppo, Connection con);

	/**
	 * Ottiene i contatti per gruppo email.
	 * @param idGruppo - id del gruppo
	 * @param con
	 * @return contatti
	 */
	Collection<Contatto> getListaContattiGruppoEmail(long idGruppo, Connection con);

	/**
	 * Ottiene le caselle postali per gruppo email.
	 * @param idGruppo - id del gruppo
	 * @param con
	 * @return caselle postali
	 */
	Collection<String> getListaCasellaPostaleGruppoEmail(long idGruppo, Connection con);

	/**
	 * Ottiene il gruppo email tramite l'id.
	 * @param idGruppo
	 * @param con
	 * @return contatto
	 */
	Contatto getGruppoEmailById(long idGruppo, Connection con);

	/**
	 * Elimina il gruppo email.
	 * @param idGruppoEmail
	 * @param con
	 * @return
	 */
	int eliminaGruppoEmail(long idGruppoEmail, Connection con);

	/**
	 * Elimina l'associazione del contatto alle caselle postali.
	 * @param idContatto - id del contatto
	 * @param con
	 * @return
	 */
	int eliminaAssociazioneContattoCasellaPostale(long idContatto, Connection con);

	/**
	 * Elimina l'associazione dei contatti al gruppo email.
	 * @param idGruppo - id del gruppo
	 * @param con
	 * @return
	 */
	int eliminaAssociazioneContattiGruppoEmail(long idGruppo, Connection con);

	/**
	 * Elimina il contatto email.
	 * @param idContatto - id del contatto
	 * @param con
	 * @return
	 */
	int eliminaContattoEmail(long idContatto, Connection con);

	/**
	 * Elimina l'associazine delle caselle al gruppo email.
	 * @param idGruppo - id del gruppo
	 * @param con
	 * @return
	 */
	int eliminaAssociazioneCaselleGruppoEmail(long idGruppo, Connection con);

	/**
	 * Ottiene il contatto tramite alias.
	 * @param alias
	 * @param con
	 * @return contatto
	 */
	Contatto getContattoByAlias(String alias, Connection con);

	/**
	 * Ottiene i contatti preferiti.
	 * @param idUfficio - id dell'ufficio
	 * @param idAOO - id dell'Aoo
	 * @param consideraGruppi
	 * @param connection
	 * @return lista di contatti
	 */
	List<Contatto> getContattiPreferiti(Long idUfficio, Long idAOO, boolean consideraGruppi, Connection connection);
	
	/**
	 * Ricerca il contatto IPA.
	 * @param mailMittente
	 * @param connection
	 * @return contatto
	 */
	Contatto ricercaIPAInteroperabilita(String mailMittente, Connection connection);
	
	/**
	 * Ricerca il contatto Red interoperabilità.
	 * @param mailMittente
	 * @param connection
	 * @return contatto
	 */
	Contatto ricercaRedInteroperabilita(String mailMittente, Connection connection);
	
	/**
	 * Ricerca un contatto a partire dalla mail del mittente.
	 * @param mailMittente - mail del mittente
	 * @param connection
	 * @return contatto trovato
	 */
	Contatto ricercaMEFInteroperabilita(String mailMittente, Connection connection);
	
	/**
	 * Ricerca contatti sulla rubrica.
	 * @param descrizione
	 * @param mail
	 * @param tipoPersona
	 * @param idAoo
	 * @param idContatto
	 * @param tipoRubrica
	 * @param limit
	 * @param connection
	 * @return
	 */
	List<Contatto> getContattiByTipoRubrica(String descrizione, String mail, String tipoPersona, Integer idAoo, Integer idContatto, 
			TipoRubricaEnum tipoRubrica, int limit, Connection connection);
	
	/**
	 * Ottiene un contatto dall'id.
	 * @param isConsideraGruppi
	 * @param contattoID - id del contatto
	 * @param connection
	 * @return contatto
	 */
	Contatto getContattoByID(boolean isConsideraGruppi, Long contattoID, Connection connection);

	/**
	 * Ricerca contatti.
	 * @param idUfficio - id dell'ufficio
	 * @param ricercaContattoObj
	 * @param acceptNullValues
	 * @param connection
	 * @return lista di contatti
	 */
	List<Contatto> ricerca(Long idUfficio, RicercaRubricaDTO ricercaContattoObj, boolean acceptNullValues,
			Connection connection);

	/**
	 * Ottiene il padre del contatto IPA.
	 * @param contattoIpa
	 * @param con
	 * @return contatto padre
	 */
	Contatto getPadreContattoIPA(Contatto contattoIpa, Connection con);

	/**
	 * Inserisce il contatto sulla base dati.
	 * @param contatto
	 * @param con
	 * @return id del contatto
	 */
	Long insertContatto(Contatto contatto, Connection con);

	/**
	 * Rimuove il record dalla tabella contatto.
	 * @param idContatto - id del contatto
	 * @param conn
	 */
	void removeContattoRichiestaCreazione(Long idContatto, Connection conn);

	/**
	 * Modifica il contatto sulla base dati.
	 * @param contatto
	 * @param connection
	 * @param isApprovaRichiestaCreazione
	 */
	void modifica(Contatto contatto, Connection connection, boolean isApprovaRichiestaCreazione);

	/**
	 * Controlla se la mail è già presente per il contatto.
	 * @param contattoToCheck - contatto da verificare
	 * @param connection
	 * @param isInModifica
	 * @return true se la mail è già presente, false in caso contrario
	 */
	boolean checkMailPerContattoGiaPresentePerAoo(Contatto contattoToCheck, Connection connection, boolean isInModifica);

	/**
	 * Rimuove contatti dal gruppo.
	 * @param gruppo
	 * @param contattiDaRimuovere
	 * @param connection
	 */
	void rimuoviDAGruppo(Contatto gruppo, List<Contatto> contattiDaRimuovere, Connection connection);

	/**
	 * Controlla se il contatto è già all'interno del gruppo.
	 * @param idgruppoToCheck - id del gruppo da verificare
	 * @param idContattoToCheck - id del contatto da verificare
	 * @param con
	 * @return true se il contatto è già all'interno del gruppo, false in caso contrario
	 */
	Boolean contattoGiaInGruppi(Long idgruppoToCheck, Long idContattoToCheck, Connection con);
	 
	/**
	 * Elimina contatti on the fly in un range temporale.
	 * @param numGiorniDelete
	 * @param idAoo - id dell'Aoo
	 * @param conn
	 */
	void eliminaContattiOnTheFlyRangeTemporale(int numGiorniDelete, Long idAoo, Connection conn);
	
	/**
	 * Ricerca contatti.
	 * 
	 * @param idUfficio
	 *            IdUfficio, è necessario per capire se quel contatto è un preferito
	 *            per quell'ufficio.
	 * @param ricercaContattoObj
	 * @param acceptNullValues
	 * @param pubblico
	 * @param connection
	 * @return lista di contatti
	 */
	List<Contatto> ricerca(Long idUfficio, RicercaRubricaDTO ricercaContattoObj, boolean acceptNullValues, boolean pubblico, Connection connection);

	/**
	 * Ricerca contatti per singolo campo.
	 * 
	 * @param value
	 *            - valore
	 * @param idUfficio
	 *            IdUfficio, è necessario per effettuare una left join e capire se
	 *            quel contatto è un preferito per quell'ufficio
	 * @param idAOO
	 *            - id dell'Aoo
	 * @param ricercaRED
	 * @param ricercaIPA
	 * @param ricercaMEF
	 * @param ricercaGruppo
	 * @param topN
	 * @param contattiDaEscludere
	 *            - contatti da escludere
	 * @param idUfficioGruppo
	 *            - id dell'ufficio del gruppo
	 * @param mostraPrimaPreferitiUfficio
	 * @param connection
	 * @return lista di contatti
	 */
	List<Contatto> ricercaCampoSingolo(String value, Long idUfficio, Integer idAOO, boolean ricercaRED,
			boolean ricercaIPA, boolean ricercaMEF, boolean ricercaGruppo, String topN,
			ArrayList<Long> contattiDaEscludere, Long idUfficioGruppo, boolean mostraPrimaPreferitiUfficio,
			Connection connection);

	/**
	 * Restituisce il contatto identificato da <code> contattoID </code>.
	 * @param contattoID
	 * @param connection
	 * @return Contatto recuperato se esistente
	 */
	Contatto getContattoSoloREDByID(Long contattoID, Connection connection);

}