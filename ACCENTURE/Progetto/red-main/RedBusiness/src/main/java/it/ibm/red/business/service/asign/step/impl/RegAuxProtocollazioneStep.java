package it.ibm.red.business.service.asign.step.impl;

import java.sql.Connection;
import java.util.Map;

import org.springframework.stereotype.Service;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.dto.ASignItemDTO;
import it.ibm.red.business.dto.ASignStepResultDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.service.asign.step.ASignStepAbstract;
import it.ibm.red.business.service.asign.step.ContextASignEnum;
import it.ibm.red.business.service.asign.step.StepEnum;

/**
 * Step di staccamento numero registrazione ausiliaria - firma asincrona.
 * @author SimoneLungarella
 */
@Service
public class RegAuxProtocollazioneStep extends ASignStepAbstract {

	/**
	 * Serial version.
	 */
	private static final long serialVersionUID = -7553486970039206304L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(RegAuxProtocollazioneStep.class.getName());

	/**
	 * Esegue parte della registrazione ausiliaria per un item di firma asincrona.
	 * La registrazione ausiliaria si divide in due fasi, recupero numero registrazione e rigenerazione content gestita da @see RegAuxRigenerazioneContentStep.
	 * @param in
	 * @param item
	 * @param context
	 * @return risultato dell'esecuzione dello step
	 */
	@Override
	protected ASignStepResultDTO run(Connection con, ASignItemDTO item, Map<ContextASignEnum, Object> context) {
		Long idItem = item.getId();
		LOGGER.info("run -> START [ID item: " + idItem + "]");
		ASignStepResultDTO out = new ASignStepResultDTO(idItem, getStep());
		
		// Si recupera l'utente firmatario
		UtenteDTO utenteFirmatario = getUtenteFirmatario(item, con);
		
		// Si recupera il workflow
		VWWorkObject wob = getWorkflow(item, out, utenteFirmatario);
		
		if (wob != null) {
			// ### REGISTRAZIONE AUSILIARIA - FASE PROTOCOLLAZIONE ###
			EsitoOperazioneDTO esitoRegAus = signSRV.protocollaRegAux(aSignSRV.getSignTransactionId(utenteFirmatario, "Batch"), wob, utenteFirmatario, con);
			
			if (esitoRegAus.isEsito()) {
				out.setStatus(true);
				out.appendMessage("[OK] Protocollazione registrazione ausiliaria eseguita");
			} else {
				throw new RedException("[KO] Errore nello step di gestione della protocollazione della registrazione ausiliaria: " + esitoRegAus.getNote() 
					+ ". [Codice errore: " + esitoRegAus.getCodiceErrore() + "]");
			}
		}
		
		LOGGER.info("run -> END [ID item: " + idItem + "]");
		return out;
	}

	/**
	 * Restituisce il nome dello step a cui è associata questa classe.
	 * 
	 * @return StepEnum
	 */
	@Override
	protected StepEnum getStep() {
		return StepEnum.REG_AUX_PROTOCOLLAZIONE;
	}

	/**
	 * @see it.ibm.red.business.service.asign.step.ASignStepAbstract#getErrorMessage().
	 */
	@Override
	protected String getErrorMessage() {
		return "[KO] Errore imprevisto nello step di gestione della protocollazione della registrazione ausiliaria";
	}

}
