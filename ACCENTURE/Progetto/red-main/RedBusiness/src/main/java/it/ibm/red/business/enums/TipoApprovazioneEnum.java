package it.ibm.red.business.enums;

/**
 * The Enum TipoApprovazioneEnum.
 *
 * @author CPIERASC
 * 
 *         Enumerato utilizzato per definire le tipologie di approvazione.
 */
public enum TipoApprovazioneEnum {
	
	/**
	 * Firma autografa.
	 */
	FIRMA_AUTOGRAFA(1L, "Firma Autografa"),

	/**
	 * Firma digitale.
	 */
	FIRMA_DIGITALE(2L, "Firma Digitale"),

	/**
	 * Sigla.
	 */
	SIGLA(3L, "Sigla"),

	/**
	 * Visto.
	 */
	VISTO(4L, "Visto"),

	/**
	 * Visto positivo.
	 */
	VISTO_POSITIVO(5L, "Visto POSITIVO"),

	/**
	 * Visto condizionato.
	 */
	VISTO_CONDIZIONATO(6L, "Visto Condizionato"),


	/**
	 * Visto negativo.
	 */
	VISTO_NEGATIVO(7L, "Visto NEGATIVO");
	
	/**
	 * Identificativo azione.
	 */
	private Long id;
	/**
	 * Descrizione azione.
	 */
	private String descrizione;
	
	/**
	 * Costruttore.
	 * 
	 * @param inId			identificativo
	 * @param inDescrizione	descrizione
	 */
	TipoApprovazioneEnum(final Long inId, final String inDescrizione) {
		this.id = inId;
		this.descrizione = inDescrizione;
	}

	/**
	 * Getter descrizione.
	 * 
	 * @return	descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Getter identificativo.
	 * 
	 * @return	identificativo
	 */
	public Long getId() {
		return id;
	}
	
	/**
	 * Metodo per il recupero dell'enum a partire dall'identificativo associato.
	 * 
	 * @param idTipoApprovazione	identificativo associato
	 * @return						enum associato all'identificativo
	 */
	public static TipoApprovazioneEnum get(final Integer idTipoApprovazione) {
		TipoApprovazioneEnum output = null;
		if (idTipoApprovazione != null) {
			for (TipoApprovazioneEnum t: TipoApprovazioneEnum.values()) {
				if (t.getId().intValue() == idTipoApprovazione) {
					output = t;
				}
			}
		}
		return output;
	}

}
