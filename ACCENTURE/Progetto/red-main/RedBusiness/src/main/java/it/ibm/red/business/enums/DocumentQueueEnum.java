package it.ibm.red.business.enums;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.constants.Constants.QueueConstant;

/**
 * The Enum QueueEnum.
 *
 * @author CPIERASC
 * 
 *         Enum per elencare le tipologia di coda gestite.
 */
public enum DocumentQueueEnum {

	/**
	 * Coda RED.
	 */
	NSD(1, DocumentQueueEnum.NSD_LIBROFIRMA, "LIBRO FIRMA", SourceTypeEnum.FILENET, QueueGroupEnum.UTENTE, null, AccessoFunzionalitaEnum.LIBROFIRMA, true, false,
			PropertiesNameEnum.CODA_LIBRO_MAX_RESULTS, ModalitaRicercaCodeEnum.USCITA),

	/**
	 * Coda FEPA.
	 */
	DD_DA_FIRMARE(2, "FEPA_LibroFirma", "DD DA FIRMARE", SourceTypeEnum.FILENET, QueueGroupEnum.UTENTE, QueueConstant.INDEX_NAME_DESTINATARIO_STEP,
			AccessoFunzionalitaEnum.DECRETI_DIRIGENTE, true, true, PropertiesNameEnum.CODA_DD_DA_FIRMARE_MAX_RESULTS, ModalitaRicercaCodeEnum.ENTRATA_USCITA),
	/**
	 * Coda Corriere.
	 */
	CORRIERE(3, DocumentQueueEnum.NSD_CORRIERE, "CORRIERE", SourceTypeEnum.FILENET, QueueGroupEnum.UFFICIO, QueueConstant.INDEX_NAME_DESTINATARIO, AccessoFunzionalitaEnum.CORRIERE, true,
			false, PropertiesNameEnum.CODA_CORRIERE_MAX_RESULTS, ModalitaRicercaCodeEnum.ENTRATA_USCITA),
	/**
	 * Coda Assegnate
	 */
	ASSEGNATE(10, "NSD_Assegnate", "ASSEGNATE", SourceTypeEnum.APP, QueueGroupEnum.UFFICIO, null, AccessoFunzionalitaEnum.ASSEGNATE, false, false,
			PropertiesNameEnum.CODA_MAX_RESULTS, ModalitaRicercaCodeEnum.ENTRATA_USCITA, StatoLavorazioneEnum.LAVORATE),
	/**
	 * Coda Chiuse
	 */
	CHIUSE(11, "NSD_Chiuse", "CHIUSE", SourceTypeEnum.APP, QueueGroupEnum.UFFICIO, null, null, false, false, PropertiesNameEnum.CODA_MAX_RESULTS,
			ModalitaRicercaCodeEnum.ENTRATA_USCITA, StatoLavorazioneEnum.ATTI, StatoLavorazioneEnum.RISPOSTA, StatoLavorazioneEnum.OSSERVAZIONE, StatoLavorazioneEnum.MOZIONE),
	/**
	 * Coda Giro Visti
	 */
	GIRO_VISTI(7, "NSD_CORRIERE_AlGiroVisti", "GIRO VISTI", SourceTypeEnum.FILENET, QueueGroupEnum.UFFICIO, null, AccessoFunzionalitaEnum.GIROVISTI, true, false,
			PropertiesNameEnum.CODA_GIRO_VISTI_MAX_RESULTS, ModalitaRicercaCodeEnum.USCITA),
	/**
	 * Coda Spedizione
	 */
	SPEDIZIONE(6, "NSD_CORRIERE_IN_SPEDIZIONE", "SPEDIZIONE", SourceTypeEnum.FILENET, QueueGroupEnum.UFFICIO, QueueConstant.INDEX_NAME_DESTINATARIO,
			AccessoFunzionalitaEnum.SPEDIZIONE, true, false, PropertiesNameEnum.CODA_SPEDIZIONE_MAX_RESULTS, ModalitaRicercaCodeEnum.USCITA),
	/**
	 * Coda Da Lavorare può essere anche uscita per iter manuale
	 */
	DA_LAVORARE(4, DocumentQueueEnum.NSD_DALAVORARE, "DA LAVORARE", SourceTypeEnum.FILENET, QueueGroupEnum.UTENTE, QueueConstant.INDEX_NAME_DESTINATARIO, AccessoFunzionalitaEnum.DALAVORARE,
			true, false, PropertiesNameEnum.CODA_DA_LAVORARE_MAX_RESULTS, ModalitaRicercaCodeEnum.ENTRATA_USCITA),
	/**
	 * Cosa sospesi.
	 */
	SOSPESO(5, "NSD_InSospeso", "IN SOSPESO", SourceTypeEnum.FILENET, QueueGroupEnum.UTENTE, null, AccessoFunzionalitaEnum.INSOSPESO, true, false,
			PropertiesNameEnum.CODA_SOSPESO_MAX_RESULTS, ModalitaRicercaCodeEnum.ENTRATA_USCITA),
	/**
	 * Coda Stornati
	 */
	STORNATI(8, "NSD_Stornati", "STORNATI", SourceTypeEnum.APP, QueueGroupEnum.UTENTE, null, null, false, false, PropertiesNameEnum.CODA_MAX_RESULTS,
			ModalitaRicercaCodeEnum.ENTRATA_USCITA, StatoLavorazioneEnum.ASSEGNATE),
	/**
	 * Coda Procedimenti
	 */
	PROCEDIMENTI(9, "NSD_Procedimenti", "PROCEDIMENTI ATTIVI", SourceTypeEnum.APP, QueueGroupEnum.UTENTE, null, null, false, false, PropertiesNameEnum.CODA_MAX_RESULTS,
			ModalitaRicercaCodeEnum.ENTRATA_USCITA, StatoLavorazioneEnum.LAVORATE, StatoLavorazioneEnum.RICHIAMA_DOCUMENTI_STATO),
	/**
	 * Coda Atti
	 */
	ATTI(12, "NSD_Atti", "ATTI", SourceTypeEnum.APP, QueueGroupEnum.ARCHIVIO, null, null, true, false, PropertiesNameEnum.CODA_MAX_RESULTS,
			ModalitaRicercaCodeEnum.ENTRATA_USCITA, StatoLavorazioneEnum.ATTI),
	/**
	 * Coda Risposta
	 */
	RISPOSTA(13, "NSD_Risposta", "RISPOSTA", SourceTypeEnum.APP, QueueGroupEnum.ARCHIVIO, null, null, false, false, PropertiesNameEnum.CODA_MAX_RESULTS,
			ModalitaRicercaCodeEnum.ENTRATA_USCITA, StatoLavorazioneEnum.RISPOSTA),
	/**
	 * Coda Mozione
	 */
	MOZIONE(14, "NSD_Mozione", "MOZIONE", SourceTypeEnum.APP, QueueGroupEnum.ARCHIVIO, null, AccessoFunzionalitaEnum.MOZIONE, false, false,
			PropertiesNameEnum.CODA_MAX_RESULTS, ModalitaRicercaCodeEnum.USCITA, StatoLavorazioneEnum.MOZIONE),
	/**
	 * Coda Fepa da Lavorare
	 */
	FATTURE_DA_LAVORARE(15, "FEPA_DaLavorare", "FATTURE DA LAVORARE", SourceTypeEnum.FILENET, QueueGroupEnum.UTENTE, QueueConstant.INDEX_NAME_DESTINATARIO_STEP,
			AccessoFunzionalitaEnum.FATTURE, true, true, PropertiesNameEnum.CODA_FATTURE_DA_LAVORARE_MAX_RESULTS, ModalitaRicercaCodeEnum.ENTRATA),
	/**
	 * Coda di servizio se il documento non viene trovato in nessuna coda
	 */
	NESSUNA_CODA(16, "In_Nessuna_Coda_Servizio", "In Nessuna Coda", SourceTypeEnum.APP, QueueGroupEnum.SERVIZIO, null, null, false, false, null, null),
	/**
	 * Coda di servizio se non vengono trovate ne code filenet ne applicative
	 */
	NON_CENSITO(17, "Non_Censito_Servizio", "NON CENSITO", SourceTypeEnum.APP, QueueGroupEnum.SERVIZIO, null, null, false, false, null, null),
	/**
	 * Coda Fepa DSR
	 */
	DSR(18, "NSD_Roster", "DSR", SourceTypeEnum.FILENET, QueueGroupEnum.UTENTE, QueueConstant.INDEX_NAME_DESTINATARIO_STEP, AccessoFunzionalitaEnum.DSR, true, true,
			PropertiesNameEnum.CODA_DSR_MAX_RESULTS, ModalitaRicercaCodeEnum.USCITA),
	/**
	 * Coda Fepa Fatture Da Assegnare
	 */
//	FATTURE_DA_ASSEGNARE(19,"FEPA_DaLavorare","Fatturazione Elettronica - Da Assegnare", SourceTypeEnum.FILENET, QueueGroupEnum.UTENTE, QueueConstant.INDEX_NAME_DESTINATARIO_STEP,AccessoFunzionalitaEnum.FATTURE, true, true),
	/**
	 * Coda Fepa Fatture In Lavorazione
	 */
	FATTURE_IN_LAVORAZIONE(20, DocumentQueueEnum.FEPA_ATTESA, "FATTURE IN LAVORAZIONE", SourceTypeEnum.FILENET, QueueGroupEnum.UTENTE, QueueConstant.INDEX_NAME_DESTINATARIO_STEP,
			AccessoFunzionalitaEnum.FATTURE, true, true, PropertiesNameEnum.CODA_FATTURE_IN_LAVORAZIONE_MAX_RESULTS, ModalitaRicercaCodeEnum.ENTRATA),
	/**
	 * Coda Decreti Dirigenziali Da Lavorare
	 */
	DD_DA_LAVORARE(21, "FEPA_DaLavorare", "DD DA LAVORARE", SourceTypeEnum.FILENET, QueueGroupEnum.UTENTE, QueueConstant.INDEX_NAME_DESTINATARIO_STEP,
			AccessoFunzionalitaEnum.DECRETI, true, true, PropertiesNameEnum.CODA_DD_DA_LAVORARE_MAX_RESULTS, ModalitaRicercaCodeEnum.ENTRATA_USCITA),
	/**
	 * Coda Decreti Dirigenziali In Firma
	 */
	DD_IN_FIRMA(22, DocumentQueueEnum.FEPA_ATTESA, "DD IN FIRMA", SourceTypeEnum.FILENET, QueueGroupEnum.UTENTE, QueueConstant.INDEX_NAME_DESTINATARIO_STEP, AccessoFunzionalitaEnum.DECRETI,
			true, true, PropertiesNameEnum.CODA_DD_IN_FIRMA_MAX_RESULTS, ModalitaRicercaCodeEnum.ENTRATA_USCITA),
	/**
	 * Coda Decreti Dirigenziali In Firma
	 */
	DD_FIRMATI(23, DocumentQueueEnum.FEPA_ATTESA, "DD FIRMATI", SourceTypeEnum.FILENET, QueueGroupEnum.UTENTE, QueueConstant.INDEX_NAME_DESTINATARIO_STEP,
			AccessoFunzionalitaEnum.DECRETI_DECRETI_DIRIGENTE, true, true, PropertiesNameEnum.CODA_DD_FIRMATI_MAX_RESULTS, ModalitaRicercaCodeEnum.ENTRATA_USCITA),
	/**
	 * Coda fittizia generata quando si clicca sull'organigramma scrivania dell menu
	 */
	ORG_SCRIVANIA(24, "Organigramma_Scrivania_Servizio", "ORGANIGRAMMA SCRIVANIA", SourceTypeEnum.APP, QueueGroupEnum.SERVIZIO, null, null, false, false, null, null),

	/**
	 * Coda Libro Firma Delegato.
	 */
	NSD_LIBRO_FIRMA_DELEGATO(25, DocumentQueueEnum.NSD_LIBROFIRMA, "LIBRO FIRMA DELEGATO", SourceTypeEnum.FILENET, QueueGroupEnum.UTENTE, null, AccessoFunzionalitaEnum.LIBROFIRMADELEGATO,
			true, false, PropertiesNameEnum.CODA_LIBRO_MAX_RESULTS, ModalitaRicercaCodeEnum.USCITA),
	/**
	 * Coda Cartacei In Acquisizione
	 */
	IN_ACQUISIZIONE(26, "NSD_CORRIERE_InAcquisizione", "IN ACQUISIZIONE", SourceTypeEnum.FILENET, QueueGroupEnum.UTENTE, null, AccessoFunzionalitaEnum.DOCUMENTI_CARTACEI,
			true, false, PropertiesNameEnum.CODA_DA_LAVORARE_MAX_RESULTS, ModalitaRicercaCodeEnum.ENTRATA),
	/**
	 * Coda Cartacei In Acquisizione
	 */
	ACQUISITI(27, "", "ACQUISITI", SourceTypeEnum.FILENET, QueueGroupEnum.RECOGNIFORM, null, AccessoFunzionalitaEnum.DOCUMENTI_CARTACEI_ACQUISITI_ELIMINATI, true, false, null,
			null),
	/**
	 * Coda Cartacei In Acquisizione
	 */
	ELIMINATI(28, "", "ELIMINATI", SourceTypeEnum.FILENET, QueueGroupEnum.RECOGNIFORM, null, AccessoFunzionalitaEnum.DOCUMENTI_CARTACEI_ACQUISITI_ELIMINATI, true, false, null,
			null),

	/**
	 * Coda spedizione errore.
	 */
	SPEDIZIONE_ERRORE(30, "SPEDIZIONE ERRORI", "SPEDIZIONE ERRORI", SourceTypeEnum.APP, QueueGroupEnum.SERVIZIO, null, null, false, false, null, null),

	/**
	 * Coda richiama documenti.
	 */
	RICHIAMA_DOCUMENTI(29, "RICHIAMA DOCUMENTI", "RICHIAMA DOCUMENTI", SourceTypeEnum.APP, QueueGroupEnum.UTENTE, null, null, true, false, PropertiesNameEnum.CODA_MAX_RESULTS,
			ModalitaRicercaCodeEnum.ENTRATA_USCITA, StatoLavorazioneEnum.RICHIAMA_DOCUMENTI_STATO),

	/**
	 * Coda Corriere diretto.
	 */
	CORRIERE_DIRETTO(31, DocumentQueueEnum.NSD_CORRIERE, "CORRIERE", SourceTypeEnum.FILENET, QueueGroupEnum.UFFICIO, QueueConstant.INDEX_NAME_DESTINATARIO, AccessoFunzionalitaEnum.CORRIERE,
			true, false, PropertiesNameEnum.CODA_CORRIERE_MAX_RESULTS, ModalitaRicercaCodeEnum.ENTRATA_USCITA),

	/**
	 * Coda Corriere indiretto.
	 */
	CORRIERE_INDIRETTO(32, DocumentQueueEnum.NSD_CORRIERE, "DA ASSEGNARE", SourceTypeEnum.FILENET, QueueGroupEnum.UFFICIO, QueueConstant.INDEX_NAME_DESTINATARIO,
			AccessoFunzionalitaEnum.CORRIERE, true, false, PropertiesNameEnum.CODA_CORRIERE_MAX_RESULTS, ModalitaRicercaCodeEnum.ENTRATA_USCITA),

	/**
	 * Coda UCB - DA LAVORARE e IN LAVORAZIONE. Valida solo per AOO degli uffici centrali di bilancio.
	 */
	DA_LAVORARE_UCB(33, DocumentQueueEnum.NSD_DALAVORARE, "DA LAVORARE", SourceTypeEnum.FILENET, QueueGroupEnum.UTENTE, QueueConstant.INDEX_NAME_DESTINATARIO,
			AccessoFunzionalitaEnum.DALAVORARE, true, false, PropertiesNameEnum.CODA_DA_LAVORARE_MAX_RESULTS, ModalitaRicercaCodeEnum.ENTRATA_USCITA),
	/**
	 * Coda UCB IN LAVORAZIONE. Valida solo per AOO degli uffici centrali di
	 * bilancio.
	 */
	IN_LAVORAZIONE_UCB(34, DocumentQueueEnum.NSD_DALAVORARE, "IN LAVORAZIONE", SourceTypeEnum.FILENET, QueueGroupEnum.UTENTE, QueueConstant.INDEX_NAME_DESTINATARIO,
			AccessoFunzionalitaEnum.DALAVORARE, true, false, PropertiesNameEnum.CODA_DA_LAVORARE_MAX_RESULTS, ModalitaRicercaCodeEnum.ENTRATA_USCITA),
	
	/**
	 * Coda GA che consente la visualizzazione dei documenti durante il processo di firma.
	 */
	PREPARAZIONE_ALLA_SPEDIZIONE(35, DocumentQueueEnum.NSD_LIBROFIRMA, "PREPARAZIONE ALLA SPEDIZIONE", SourceTypeEnum.APP, QueueGroupEnum.UTENTE, null, 
			null, true, false, PropertiesNameEnum.CODA_MAX_RESULTS, ModalitaRicercaCodeEnum.USCITA),
	
	/**
	 * Coda GA che consente la visualizzazione dei documenti durante il processo di firma FEPA.
	 */
	PREPARAZIONE_ALLA_SPEDIZIONE_FEPA(36, "FEPA_LibroFirma", "PREPARAZIONE ALLA SPEDIZIONE", SourceTypeEnum.APP, QueueGroupEnum.UTENTE, null, 
			null, true, false, PropertiesNameEnum.CODA_MAX_RESULTS, ModalitaRicercaCodeEnum.USCITA);
	
	private static final String NSD_LIBROFIRMA = "NSD_LibroFirma";
	private static final String NSD_CORRIERE = "NSD_CORRIERE";
	private static final String NSD_DALAVORARE = "NSD_DaLavorare";
	private static final String FEPA_ATTESA = "FEPA_Attesa";
	
	/**
	 * Identificativo.
	 */
	private Integer id;

	/**
	 * Descrizione.
	 */
	private String name;

	/**
	 * Nome visualizzato front end.
	 */
	private String displayName;

	/**
	 * Tipo coda (Filenet/Applicativa).
	 */
	private SourceTypeEnum type;

	/**
	 * Gruppo di riferimento.
	 */
	private QueueGroupEnum group;

	/**
	 * Index Name utile per la query al PE.
	 */
	private String indexName;

	/**
	 * enum utilizzato per gestire i permessi delle code.
	 */
	private AccessoFunzionalitaEnum accessFun;

	/**
	 * flag che identifica se la coda è operativa.
	 */
	private boolean codaOperativa;

	/**
	 * Lista di ID che rappresentato lo stato lavorazione di una coda.
	 */
	private StatoLavorazioneEnum[] idsStatoLavorazione;

	/**
	 * Se è true viene chiamato il metodo per creare il VWQueueQuery o VWRosterQuery
	 * (gestione da ListaDocumentiSRV).
	 */
	private boolean textFilter;

	/**
	 * Max risultato.
	 */
	private PropertiesNameEnum maxResult;

	/**
	 * Modalità ricerca code.
	 */
	private ModalitaRicercaCodeEnum modalitaRicercaCodeEnum;

	/**
	 * Costruttore.
	 * 
	 * @param inId          identificativo
	 * @param inDescrizione descrizione
	 */
	DocumentQueueEnum(final Integer inId, final String inName, final String inDisplayName, final SourceTypeEnum inType, final QueueGroupEnum inGroup, final String inIndexName,
			final AccessoFunzionalitaEnum inAccessFun, final boolean inCodaOperativa, final boolean textFilter, final PropertiesNameEnum inMaxResult,
			final ModalitaRicercaCodeEnum inmodalitaRicercaCodeEnum, final StatoLavorazioneEnum... inIdsStatoLavorazione) {
		id = inId;
		name = inName;
		displayName = inDisplayName;
		type = inType;
		group = inGroup;
		indexName = inIndexName;
		accessFun = inAccessFun;
		codaOperativa = inCodaOperativa;
		this.textFilter = textFilter;
		idsStatoLavorazione = inIdsStatoLavorazione;
		maxResult = inMaxResult;
		modalitaRicercaCodeEnum = inmodalitaRicercaCodeEnum;
	}

	/**
	 * Getter identifcativo.
	 * 
	 * @return identificativo
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * Getter descrizione.
	 * 
	 * @return descrizione
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the displayName
	 */
	public final String getDisplayName() {
		return displayName;
	}

	/**
	 * @return the group
	 */
	public final QueueGroupEnum getGroup() {
		return group;
	}

	/**
	 * @return the indexName
	 */
	public final String getIndexName() {
		return indexName;
	}

	/**
	 * @return the accessFun
	 */
	public final AccessoFunzionalitaEnum getAccessFun() {
		return accessFun;
	}

	/**
	 * @return the codaOperativa
	 */
	public final boolean isCodaOperativa() {
		return codaOperativa;
	}

	/**
	 * @return the idsStatoLavorazione
	 */
	public final StatoLavorazioneEnum[] getIdsStatoLavorazione() {
		return idsStatoLavorazione;
	}

	/**
	 * @return the textQuery
	 */
	public boolean getTextFilter() {
		return textFilter;
	}

	/**
	 * @param textQuery the textQuery to set
	 */
	public void getTextFilter(final boolean textFilter) {
		this.textFilter = textFilter;
	}

	/**
	 * @return the maxResult
	 */
	public final PropertiesNameEnum getMaxResult() {
		return maxResult;
	}

	/**
	 * Metodo per il recupero di un'enum dato il valore caratteristico.
	 * 
	 * @param value valore
	 * @return enum
	 */
	public static DocumentQueueEnum get(final String value, final Boolean inLavorazione) {
		DocumentQueueEnum output = DocumentQueueEnum.NON_CENSITO;
		
		for (final DocumentQueueEnum q : DocumentQueueEnum.values()) {

			// Metadato "in Lavorazione" non esistente per l'Aoo
			final boolean metadatoNonEsistente = inLavorazione == null && (q.equals(DA_LAVORARE_UCB) || q.equals(IN_LAVORAZIONE_UCB)); 
			// In Lavorazione
			final boolean codeInLavorazione = Boolean.TRUE.equals(inLavorazione) && (q.equals(DA_LAVORARE_UCB) || q.equals(DA_LAVORARE));
			// Non in Lavorazione
			final boolean codeNonInLavorazione = Boolean.FALSE.equals(inLavorazione) && (q.equals(IN_LAVORAZIONE_UCB) || q.equals(DA_LAVORARE));
			
			if (metadatoNonEsistente || codeInLavorazione || codeNonInLavorazione) {
				continue;
			}
			
			if (q.getName().equals(value)) {
				return q;
			}

		}
		return output;
	}

	/**
	 * Metodo per il recupero di un'enum dato il valore caratteristico.
	 * 
	 * @param value valore
	 * @return enum
	 */
	public static DocumentQueueEnum getByDisplayName(final String name) {
		DocumentQueueEnum output = DocumentQueueEnum.NON_CENSITO;
		for (final DocumentQueueEnum q : DocumentQueueEnum.values()) {
			if (q.getName().equals(name)) {
				output = q;
				break;
			}
		}
		return output;
	}

	/**
	 * Metodo per verificare se la descrizione fornita in input appartiene ad una
	 * della code fornite.
	 * 
	 * @param queue descrizione coda
	 * @return true se la coda è gestita, false altrimenti
	 */
	public static boolean isInSignQueue(final String queue) {
		boolean output = false;
		for (final DocumentQueueEnum q : DocumentQueueEnum.values()) {
			if (q.getId() <= DocumentQueueEnum.DD_DA_FIRMARE.getId() && queue.equals(q.getName())) {
				output = true;
				break;
			}
		}
		return output;
	}

	/**
	 * 
	 * @param idStatoLav
	 * @param idUtente
	 * @return
	 */
	public static List<DocumentQueueEnum> getQueue(final StatoLavorazioneEnum statoLav, final QueueGroupEnum group) {
		final List<DocumentQueueEnum> output = new ArrayList<>();
		final Collection<DocumentQueueEnum> queues = getQueueByGroup(group);
		for (final DocumentQueueEnum q : queues) {
			if (canHandle(q, statoLav)) {
				output.add(q);
			}
		}
		return output;
	}

	/**
	 * Restituisce la lista delle code contenute nel gruppo.
	 * 
	 * @param group
	 * @return List di DocumentQueueEnum
	 */
	public static Collection<DocumentQueueEnum> getQueueByGroup(final QueueGroupEnum group) {
		final Collection<DocumentQueueEnum> output = new ArrayList<>();
		for (final DocumentQueueEnum q : DocumentQueueEnum.values()) {
			if (group.equals(q.getGroup())) {
				output.add(q);
			}
		}
		return output;
	}

	private static boolean canHandle(final DocumentQueueEnum queue, final StatoLavorazioneEnum statoLav) {
		boolean output = false;
		for (final StatoLavorazioneEnum sl : queue.getIdsStatoLavorazione()) {
			if (sl.equals(statoLav)) {
				output = true;
				break;
			}
		}
		return output;
	}

	/**
	 * Restituisce il tipo della coda.
	 * 
	 * @return Fielenet o applicativa
	 */
	public SourceTypeEnum getType() {
		return type;
	}

	/**
	 * Restituisce restituisce gli id degli stati lavorazione.
	 * 
	 * @see StatoLavorazioneEnum.
	 * @return List<Long>
	 */
	public List<Long> getIdStatiLavorazione() {
		final List<Long> output = new ArrayList<>();
		for (final StatoLavorazioneEnum sle : idsStatoLavorazione) {
			output.add(sle.getId());
		}
		return output;
	}

	/**
	 * Restituisce la modalita di ricerca della coda.
	 * 
	 * @see ModalitaRicercaCodeEnum
	 * @return modalita ricerca
	 */
	public ModalitaRicercaCodeEnum getModalitaRicercaCodeEnum() {
		return modalitaRicercaCodeEnum;
	}

	/**
	 * Restituisce l'id della coda associata al display name.
	 * 
	 * @param displayName
	 * @return id coda
	 */
	public static int getQueueByDisplayName(final String displayName) {
		int id = 0;

		for (final DocumentQueueEnum q : DocumentQueueEnum.values()) {
			if (q.getDisplayName().equalsIgnoreCase(displayName)) {
				id = q.getId();
				break;
			}
		}
		return id;
	}

	/**
	 * Restituisce true se la coda è associata ad uno dei corrieri, false
	 * altrimenti.
	 * 
	 * @param queue
	 * @return true se la coda è associata ad uno dei corrieri, false altrimenti
	 */
	public static boolean isOneKindOfCorriere(final DocumentQueueEnum queue) {
		return queue != null
				&& (queue.equals(DocumentQueueEnum.CORRIERE) || queue.equals(DocumentQueueEnum.CORRIERE_DIRETTO) || queue.equals(DocumentQueueEnum.CORRIERE_INDIRETTO));
	}

	/**
	 * Restituisce true se la coda è associata ad una delle possibili "DA LAVORARE",
	 * false altrimenti.
	 * 
	 * @param queue
	 * @return true se la coda è associata ad una delle possibili "DA LAVORARE",
	 *         false altrimenti
	 */
	public static boolean isOneKindOfDaLavorare(final DocumentQueueEnum queue) {
		return queue != null && (queue.equals(DocumentQueueEnum.DA_LAVORARE) || queue.equals(DocumentQueueEnum.DA_LAVORARE_UCB));
	}

	/**
	 * Restituisce true se la coda è associata ad una delle possibili "Cartacei",
	 * false altrimenti.
	 * 
	 * @param queue
	 * @return true se la coda è associata ad una delle possibili "Cartacei", false
	 *         altrimenti
	 */
	public static boolean isOneKindOfCartacei(final DocumentQueueEnum queue) {
		return queue != null && (queue.equals(DocumentQueueEnum.ACQUISITI) || queue.equals(DocumentQueueEnum.ELIMINATI) || queue.equals(DocumentQueueEnum.IN_ACQUISIZIONE));
	}

}
