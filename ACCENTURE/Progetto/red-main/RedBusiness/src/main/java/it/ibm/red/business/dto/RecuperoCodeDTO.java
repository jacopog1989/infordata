/**
 * 
 */
package it.ibm.red.business.dto;

import java.util.Collection;

import it.ibm.red.business.enums.DocumentQueueEnum;

/**
 * DTO utilizzato per gestire i risultati di una ricerca code 
 * 
 * @author APerquoti
 *
 */
public class RecuperoCodeDTO extends AbstractDTO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2584146986339813198L;
	
	/**
	 * 
	 */
	private Collection<DocumentQueueEnum> code;
	
	/**
	 * 
	 */
	private Collection<String> codeNonCensite;
	
	
	/**
	 * 
	 * @param inCode
	 * @param inCodeNonCensite
	 */
	public RecuperoCodeDTO(final Collection<DocumentQueueEnum> inCode, final Collection<String> inCodeNonCensite) {
		super();
		
		code = inCode;
		codeNonCensite = inCodeNonCensite;
	}

	/**
	 * @return the code
	 */
	public final Collection<DocumentQueueEnum> getCode() {
		return code;
	}

	/**
	 * @return the codeNonCensite
	 */
	public final Collection<String> getCodeNonCensite() {
		return codeNonCensite;
	}
	
	
	
	
}
