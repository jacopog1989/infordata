package it.ibm.red.business.service.concrete;

import java.io.InputStream;
import java.sql.Connection;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.filenet.api.core.ContentTransfer;
import com.filenet.api.core.Document;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.constants.Constants.Protocollo;
import it.ibm.red.business.dao.IParametriProtocolloDAO;
import it.ibm.red.business.dao.impl.AooDAO;
import it.ibm.red.business.dto.DetailDocumentoDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.ListSecurityDTO;
import it.ibm.red.business.dto.PEDocumentoDTO;
import it.ibm.red.business.dto.ProtocolloDTO;
import it.ibm.red.business.dto.ProtocolloEmergenzaDocDTO;
import it.ibm.red.business.dto.ProtocolloNpsDTO;
import it.ibm.red.business.dto.SequKDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipologiaProtocollazioneEnum;
import it.ibm.red.business.enums.TrasformerCEEnum;
import it.ibm.red.business.enums.TrasformerPEEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.TrasformCE;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.TrasformPE;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.Aoo;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IFirmatoSpeditoSRV;
import it.ibm.red.business.service.INpsSRV;
import it.ibm.red.business.service.IPMefSRV;
import it.ibm.red.business.service.IProtocolliEmergenzaSRV;
import it.ibm.red.business.utils.DestinatariRedUtils;
import it.ibm.red.business.utils.StringUtils;

/**
 * Service gestione firmati spediti.
 */
@Service
public class FirmatoSpeditoSRV extends AbstractService implements IFirmatoSpeditoSRV {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = 6511862671433177505L;
	
	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(FirmatoSpeditoSRV.class.getName());
	
	/**
	 * Properties.
	 */
	private PropertiesProvider pp;

	/**
	 * Servizio.
	 */
	@Autowired
	private SecuritySRV securitySRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private SignSRV signSRV;
	
	/**
	 * Servizio.
	 */
	@Autowired
	private INpsSRV npsSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private AooDAO aooDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IParametriProtocolloDAO parametriProtocolloDAO;

	/**
	 * Servizio.
	 */
	@Autowired
	private IPMefSRV pMefSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private VistiSRV vistiSRV;
	
	/**
	 * Servizio.
	 */
	@Autowired
	private IProtocolliEmergenzaSRV protocolliEmergenzaSRV;
	
	/**
	 * Post construct del Service.
	 */
	@PostConstruct
	public final void postCostruct() {
		pp = PropertiesProvider.getIstance();
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFirmatoSpeditoFacadeSRV#firmatoESpedito(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, it.ibm.red.business.dto.ProtocolloEmergenzaDocDTO).
	 */
	@Override
//	@ServiceTask(name = "firmatoESpedito")
	public EsitoOperazioneDTO firmatoESpedito(final UtenteDTO utente, final String wobNumber, final ProtocolloEmergenzaDocDTO protocolloEmergenza) {
		return firmatoSpedito(wobNumber, utente, true, protocolloEmergenza);
	}

	/**
	 * @see it.ibm.red.business.service.facade.IFirmatoSpeditoFacadeSRV#firmato(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, it.ibm.red.business.dto.ProtocolloEmergenzaDocDTO).
	 */
	@Override
//	@ServiceTask(name = "firmato")
	public EsitoOperazioneDTO firmato(final UtenteDTO utente, final String wobNumber, final ProtocolloEmergenzaDocDTO protocolloEmergenza) {
		return firmatoSpedito(wobNumber, utente, false, protocolloEmergenza);
	}

	private EsitoOperazioneDTO firmatoSpedito(final String wobNumber, final UtenteDTO utente, final boolean spedito,
			final ProtocolloEmergenzaDocDTO protocolloEmergenza) {
		final EsitoOperazioneDTO esito = new EsitoOperazioneDTO(wobNumber);
		Connection connection = null;
		Integer numDoc = null;
		FilenetPEHelper fpeh = null;
		IFilenetCEHelper fceh = null;
		
		try {
			final HashMap<String, Object> map = new HashMap<>();
			connection = setupConnection(getDataSource().getConnection(), true);
			fpeh = new FilenetPEHelper(utente.getFcDTO());
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			
			final String response = spedito ? ResponsesRedEnum.FIRMATO_E_SPEDITO.getResponse() : ResponsesRedEnum.FIRMATO.getResponse();
			
			final VWWorkObject wo = fpeh.getWorkFlowByWob(wobNumber, true);		
			final String assegnazioneCoord = signSRV.getSecurityCoordinatore(wo);
			final Object idDocumento = TrasformerPE.getMetadato(wo, pp.getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
			final String idDoc = idDocumento == null ? null : idDocumento.toString();
			
			numDoc = fceh.getNumDocByDocumentTitle(idDoc);
			
			final String idAoo = utente.getIdAoo().toString();
			final Document document = fceh.getDocumentByIdGestionale(idDoc, null, null, Integer.parseInt(idAoo), null, null);
			final boolean isRiservato = document.getProperties().getInteger32Value(pp.getParameterByKey(PropertiesNameEnum.RISERVATO_METAKEY)).intValue() == 1;
			final List<String> destinatariInterni = Arrays.asList(
					(String[]) TrasformerPE.getMetadato(wo,	pp.getParameterByKey(PropertiesNameEnum.ELENCO_DESTINATARI_INTERNI_METAKEY)));
			ListSecurityDTO securities;
			
			
			final Object annoProt = TrasformerCE.getMetadato(document, pp.getParameterByKey(PropertiesNameEnum.ANNO_PROTOCOLLO_METAKEY)); 
			final String annoProtocollo =  annoProt == null ? null : annoProt.toString();
			final Object numProt = TrasformerCE.getMetadato(document, pp.getParameterByKey(PropertiesNameEnum.NUMERO_PROTOCOLLO_METAKEY));
			
			final String numeroProtocollo = numProt == null ? null : numProt.toString();
			final Date dataProtocollo = (Date) TrasformerCE.getMetadato(document, pp.getParameterByKey(PropertiesNameEnum.DATA_PROTOCOLLO_METAKEY));
			final Collection<?> inDestinatari = (Collection<?>) TrasformerCE.getMetadato(document, PropertiesNameEnum.DESTINATARI_DOCUMENTO_METAKEY);
			// Si normalizza il metadato per evitare errori comuni durante lo split delle singole stringhe dei destinatari
			final List<String[]> destinatariDocumento = DestinatariRedUtils.normalizzaMetadatoDestinatariDocumento(inDestinatari);
			int countEsterni = 0;
			
			for (int z = 0; z < destinatariDocumento.size(); z++) {
				final String[] destinatario = destinatariDocumento.get(z);
				if (Constants.TipoDestinatario.ESTERNO.equalsIgnoreCase(destinatario[Constants.Mail.IE_MAIL_POSITION])) {
					countEsterni++;
				}
			}
			
			boolean update = false;
			final DetailDocumentoDTO  documentDetailDTO = TrasformCE.transform(document, TrasformerCEEnum.FROM_DOCUMENTO_TO_DETAIL);
			ProtocolloDTO protocollo = null;
			if (StringUtils.isNullOrEmpty(annoProtocollo) && StringUtils.isNullOrEmpty(numeroProtocollo) && dataProtocollo == null && countEsterni > 0 && utente.getIdAoo() != null) {
					
				protocollo = documentDetailDTO.getProtocollo();

				if (TipologiaProtocollazioneEnum.isProtocolloNPS(utente.getIdTipoProtocollo())) {
					
					ProtocolloNpsDTO protocolloNps = null;
					if (TipologiaProtocollazioneEnum.NPS.getId().equals(utente.getIdTipoProtocollo())) {
						
//							<-- Gestione e Controllo della configurazione dei Registri Repertorio -->
						// Verifica se è un registro di repertorio e nel caso esegue il set della descrizione trovata.
						signSRV.checkIsRegistroRepertorio(utente, documentDetailDTO, connection);
						
						protocolloNps = signSRV.getNewProtocolloNps(wo, documentDetailDTO, utente, fceh, connection, isRiservato);
					} else {
						protocolloNps = saveInfoProtocolloEmergenza(utente, protocolloEmergenza, documentDetailDTO,	protocolloNps);
					}
					
					protocollo = new ProtocolloDTO(protocolloNps.getIdProtocollo(), protocolloNps.getNumeroProtocollo(),
							protocolloNps.getAnnoProtocollo(), protocolloNps.getDataProtocollo(),
							protocolloNps.getDescrizioneTipologiaDocumento(), protocolloNps.getOggetto());
					update = true;
				} else if (utente.getIdTipoProtocollo().equals(TipologiaProtocollazioneEnum.MEF.getId())) {
					final SequKDTO sequK = parametriProtocolloDAO.getParametriProtocolloByIdAoo(utente.getIdAoo(), connection);
					protocollo = pMefSRV.creaProtocolloMEFUscita(documentDetailDTO.getOggetto(), sequK, utente.getNome() + " " + utente.getCognome(), utente);
					update = true;
				} else {
					throw new RedException("Il servizio di protocollazione da utilizzare per la protocollazione del documento NON è stato riconosciuto."
							+ " Contattare l'assistenza.");
				}

				if (protocollo == null || protocollo.getNumeroProtocollo() <= 0) {
					throw new RedException("Non è stato possibilile recuperare il protocollo. Contattare l'assistenza.");
				}

				if (update) {
					fceh.setPropertiesAndSaveDocument(document, signSRV.getMetadatiProtocolloToUpdate(protocollo, utente, true), true);
				}

				final Aoo aoo = aooDAO.getAoo(utente.getIdAoo(), connection);
				
				//invia la prima versione del documento a NPS
				final Document documentFirstVersion = fceh.getFirstVersionById(idDoc, utente.getIdAoo().intValue());
				final InputStream contentIS = ((ContentTransfer) documentFirstVersion.get_ContentElements().get(0)).accessContentStream();
				final String nomefile = documentFirstVersion.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY));
				final String oggetto = documentFirstVersion.getProperties().getStringValue(pp.getParameterByKey(PropertiesNameEnum.OGGETTO_METAKEY));
				final String numeroAnnoProtocollo = protocollo.getNumeroAnnoProtocolloNPS(utente.getCodiceAoo());
				npsSRV.uploadDocToAsyncNps(contentIS, documentFirstVersion.get_Id().toString(), protocollo.getIdProtocollo(), 
						documentFirstVersion.get_MimeType(), nomefile, oggetto, numeroAnnoProtocollo, utente, idDoc, connection);
				
				signSRV.gestisciAllacci(fceh, fpeh, aoo, document, utente, true, connection);
									
			}
			
			securities = securitySRV.getSecurityUtentePerUfficioFascicolo(utente, idDoc, utente.getIdUfficio() + "", destinatariInterni, assegnazioneCoord, 
					isRiservato);
			if (!securitySRV.modificaSecurityFascicoli(utente, securities, idDoc)) {
				throw new RedException("Errore nella modifica delle security");
			}
			securitySRV.updateSecurity(utente, idDoc, null, securities, true);
			
			final PEDocumentoDTO wobDTO = TrasformPE.transform(wo, TrasformerPEEnum.FROM_WF_TO_DOCUMENTO);
			final int tipoSpedizione = signSRV.getTipoSpedizione(wobDTO, documentDetailDTO);
			if (tipoSpedizione >= 0) {
				map.put(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_SPEDIZIONE_METAKEY), tipoSpedizione);
			}
			if (TrasformerPE.getMetadato(wo, pp.getParameterByKey(PropertiesNameEnum.FIRMA_DIG_RGS_WF_METAKEY)) != null) {
				map.put(pp.getParameterByKey(PropertiesNameEnum.FIRMA_DIG_RGS_WF_METAKEY), 0);
			}
			if (response.equals(ResponsesRedEnum.FIRMATO.getResponse())) {
				map.put(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY), TipoAssegnazioneEnum.FIRMATO.getId());
			} else {
				map.put(pp.getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY), TipoAssegnazioneEnum.FIRMATO_E_SPEDITO.getId());
			}

			if (fpeh.nextStepMittente(wobNumber, utente.getId(), response, map)) {
				vistiSRV.terminaWfVisti(idDoc, wobNumber, utente.getIdUfficio());
			} else {
				throw new RedException("Errore nell'avanzamento del WF.");
			}
			
			Integer numeroProtocolloInt = null;
			Integer annoProtocolloInt = null;
			if (update && protocollo != null) {
				if (protocollo.getNumeroProtocollo() > 0) {
					numeroProtocolloInt = protocollo.getNumeroProtocollo();
				}
				annoProtocolloInt = protocollo.getAnnoProtocolloInt();
			}
			
			esito.setEsito(true);
			esito.setAnnoProtocollo(annoProtocolloInt);
			esito.setNumeroProtocollo(numeroProtocolloInt);
			esito.setIdDocumento(numDoc);
			esito.setNote(EsitoOperazioneDTO.MESSAGGIO_ESITO_OK);
			
			commitConnection(connection);
		} catch (final Exception e) {
			LOGGER.error("Errore nell'avanzamento del workflow.", e);
			rollbackConnection(connection);
			esito.setIdDocumento(numDoc);
			esito.setNote("Si è verificato un errore nell'esecuzione dell'operazione");
		} finally {
			closeConnection(connection);
			logoff(fpeh);
			popSubject(fceh);
		}
		
		return esito;
	}

	/**
	 * Registra le informazioni di emergenza sulla base dati.
	 * @param utente
	 * @param protocolloEmergenza
	 * @param documentDetailDTO
	 * @param protocolloNps
	 * @return info protocollo NPS
	 */
	private ProtocolloNpsDTO saveInfoProtocolloEmergenza(final UtenteDTO utente, final ProtocolloEmergenzaDocDTO protocolloEmergenza, 
			final DetailDocumentoDTO documentDetailDTO, ProtocolloNpsDTO protocolloNps) {
		
		Connection conEmergenza = null;
		try {
			conEmergenza = setupConnection(getDataSource().getConnection(), true);
			
			protocolloNps = protocolliEmergenzaSRV.registraInfoEmergenza(documentDetailDTO.getDocumentTitle(), utente.getIdAoo(), utente.getCodiceAoo(), 
					protocolloEmergenza.getNumeroProtocolloEmergenza(), protocolloEmergenza.getAnnoProtocolloEmergenza(), protocolloEmergenza.getDataProtocolloEmergenza(), 
					Protocollo.TIPO_PROTOCOLLO_USCITA, documentDetailDTO.getOggetto(), documentDetailDTO.getTipologiaDocumento(), conEmergenza);
			
			commitConnection(conEmergenza);
		} catch (final Exception e) {
			LOGGER.error("Errore durante la registrazione nel DB delle informazioni del protocollo di emergenza", e);
			rollbackConnection(conEmergenza);
		} finally {
			closeConnection(conEmergenza);
		}
		return protocolloNps;
	}
}