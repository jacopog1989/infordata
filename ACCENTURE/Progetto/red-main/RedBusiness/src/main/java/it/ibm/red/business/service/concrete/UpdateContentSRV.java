package it.ibm.red.business.service.concrete;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.filenet.api.constants.PropertyNames;
import com.filenet.api.core.Document;
import com.google.common.net.MediaType;

import filenet.vw.api.VWWorkObject;
import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.dao.impl.IterApprovativoDAO;
import it.ibm.red.business.dto.CodaTrasformazioneParametriDTO;
import it.ibm.red.business.dto.DocumentAppletContentDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.IterApprovativoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.ResponsesRedEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.enums.TipoSpedizioneEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.filenet.pe.FilenetPEHelper;
import it.ibm.red.business.helper.filenet.pe.trasform.impl.TrasformerPE;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.IUpdateContentSRV;
import it.ibm.red.business.service.facade.IOperationWorkFlowFacadeSRV;
import it.ibm.red.business.service.facade.ITrasformazionePDFFacadeSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * Service di gestion aggiornamento content.
 */
@Service
@Component
public class UpdateContentSRV extends AbstractService implements IUpdateContentSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 2221174925592574544L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(UpdateContentSRV.class.getName());

	/**
	 * Messaggio errore avanzamento workflow.
	 */
	private static final String ERROR_AVANZAMENTO_WORKFLOW_MSG = "Errore durante l'avanzamento del workflow";

	/**
	 * DAO.
	 */
	@Autowired
	private IterApprovativoDAO iaDao;

	/**
	 * Servizio.
	 */
	@Autowired
	private ITrasformazionePDFFacadeSRV trasfSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IOperationWorkFlowFacadeSRV opSRV;

	/**
	 * @see it.ibm.red.business.service.facade.IUpdateContentFacadeSRV#updateContent(java.lang.String,
	 *      byte[], it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public void updateContent(final String workflowNumber, final byte[] newContent, final UtenteDTO utente) {
		final FilenetPEHelper fpeh = new FilenetPEHelper(utente.getFcDTO());

		final VWWorkObject wob = fpeh.getWorkFlowByWob(workflowNumber, true);

		if (wob == null) {
			LOGGER.error("Si è verificato un errore durante il recupero del workflow. WOB number: " + workflowNumber);
			throw new RedException("Errore durante il recupero del workflow. WOB number: " + workflowNumber);
		}

		final Integer idDocumento = (Integer) TrasformerPE.getMetadato(wob, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));

		if (idDocumento == null) {
			LOGGER.error("Metadato ID documento non trovato per il workflow: " + workflowNumber);
			throw new RedException("Metadato ID documento non trovato per il workflow: " + workflowNumber);
		}

		try {

			// Modifica la prima versione modificabile del documento
			updateDocument(utente, workflowNumber, idDocumento.toString(), newContent);

		} catch (final Exception e) {
			LOGGER.error("Si è verificto un errore durante l'inserimento della nuova versione del documento su FileNet", e);
			throw new RedException("Errore durante l'inserimento della nuova versione del documento su FileNet", e);
		}

		Connection connection = null;
		try {

			connection = setupConnection(getDataSource().getConnection(), false);
			addPDFVersion(utente, wob, connection);

		} catch (final Exception e) {
			LOGGER.error(ERROR_AVANZAMENTO_WORKFLOW_MSG, e);
			throw new RedException(ERROR_AVANZAMENTO_WORKFLOW_MSG, e);
		} finally {
			closeConnection(connection);
		}

		try {
			// Avanza il workflow
			final HashMap<String, Object> metadati = new HashMap<>();
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UTENTE_MITTENTE_WF_METAKEY), utente.getId());
			metadati.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_TIPO_SPEDIZIONE_METAKEY), TipoSpedizioneEnum.IGNOTO.getId());
			fpeh.nextStep(wob, metadati, ResponsesRedEnum.MODIFICA.getResponse());
		} catch (final Exception e) {
			LOGGER.error(ERROR_AVANZAMENTO_WORKFLOW_MSG, e);
			throw new RedException(ERROR_AVANZAMENTO_WORKFLOW_MSG, e);
		}

	}

	private void updateDocument(final UtenteDTO utente, final String workflowNumber, final String idDocumento, final byte[] newContent) {
		IFilenetCEHelper fcehAdmin = null;
		IFilenetCEHelper fceh = null;

		try (
			ByteArrayInputStream bis = new ByteArrayInputStream(newContent);
		) {
			fcehAdmin = FilenetCEHelperProxy.newInstance(new FilenetCredentialsDTO(utente.getFcDTO()), utente.getIdAoo());

			final List<String> selectList = Arrays.asList(PropertyNames.ID, PropertyNames.IS_RESERVED,
					PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY));

			Document doc = fcehAdmin.getDocumentByIdGestionale(idDocumento, selectList, null, utente.getIdAoo().intValue(), null, null);

			String fileName = (String) TrasformerCE.getMetadato(doc, PropertiesNameEnum.NOME_FILE_METAKEY);

			// Verifico se il documento è un pdf
			if (fileName.endsWith(".pdf")) {
				// Prelevo il nome del file dell'ultima versione editabile (la modifica del
				// content avverrà non sul pdf ma su tale elemento modificabile)
				final DocumentAppletContentDTO infoDoc = opSRV.getDocumentInfoForApplet(workflowNumber, utente);
				fileName = infoDoc.getNomeFile();
			}

			doc = fcehAdmin.getDocumentByIdGestionale(idDocumento, selectList, null, utente.getIdAoo().intValue(), null, null);

			final Map<String, Object> newProperty = new HashMap<>();
			newProperty.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.TRASFORMAZIONE_PDF_ERRORE_METAKEY), Constants.Varie.TRASFORMAZIONE_PDF_OK);
			newProperty.put(PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.NOME_FILE_METAKEY), fileName);
			fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());
			fceh.addVersion(doc, bis, newProperty, fileName, MediaType.MICROSOFT_WORD.toString(), utente.getIdAoo());
		} catch (IOException e) {
			LOGGER.error("Errore durante la chiusura dell'outputstream", e);
		} finally {
			popSubject(fcehAdmin);
			popSubject(fceh);
		}
	}

	/**
	 * @see it.ibm.red.business.service.IUpdateContentSRV#addPDFVersion(it.ibm.red.business.dto.UtenteDTO,
	 *      java.lang.String, java.lang.String[], java.lang.Boolean,
	 *      java.lang.Integer, java.lang.Integer).
	 */
	@Override
	public void addPDFVersion(final UtenteDTO utente, final String idDocumento, final String[] firmatari, final Boolean flagModificaMontanaro, final Integer tipoFirma,
			final Integer idTipoAssegnazione) {
		final CodaTrasformazioneParametriDTO parametri = new CodaTrasformazioneParametriDTO();

		parametri.setIdDocumento(idDocumento);
		parametri.setPrincipale(true);

		parametri.setFirmatariString(firmatari);

		parametri.setAllegati(flagModificaMontanaro);

		parametri.setIdAllegati(new ArrayList<>());
		parametri.setIdAllegatiDaFirmare(new ArrayList<>());

		parametri.setIdNodo(utente.getIdUfficio());
		parametri.setIdUtente(utente.getId());

		parametri.setTipoFirma(tipoFirma);

		parametri.setAggiornaFirmaPDF(true);
		parametri.setProtocollazioneP7M(false);
		parametri.setFlagFirmaCopiaConforme(false);

		parametri.setIdTipoAssegnazione(idTipoAssegnazione);

		/*
		 * Sul CE veniva impostato il metadato Constants.METADATO_STATO_THREAD_FIRMA ad
		 * 1 ma essendo la chiamata sincrona non è più necessario.
		 */

		trasfSRV.doTransformation(parametri);
	}

	/**
	 * @see it.ibm.red.business.service.IUpdateContentSRV#addPDFVersion(it.ibm.red.business.dto.UtenteDTO,
	 *      filenet.vw.api.VWWorkObject, java.sql.Connection).
	 */
	@Override
	public void addPDFVersion(final UtenteDTO utente, final VWWorkObject wob, final Connection connection) {
		
		final Integer idDocumento = (Integer) TrasformerPE.getMetadato(wob, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_DOCUMENTO_WF_METAKEY));
		String[] firmatari = (String[]) TrasformerPE.getMetadato(wob, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ELENCO_LIBROFIRMA_METAKEY));

		Boolean flagModificaMontanaro = false;
		final Object flagModificaMontanaroObj = TrasformerPE.getMetadato(wob, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.MODIFICA_MONTANARO_METAKEY));
		if (flagModificaMontanaroObj instanceof String) {
			flagModificaMontanaro = Boolean.parseBoolean((String) flagModificaMontanaroObj);
		} else if (flagModificaMontanaroObj != null) {
			flagModificaMontanaro = (Boolean) flagModificaMontanaroObj;
		}
		final Integer idTipoAssegnazione = (Integer) TrasformerPE.getMetadato(wob,
				PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_TIPO_ASSEGNAZIONE_METAKEY));

		if (firmatari == null || firmatari.length == 0 || StringUtils.isNullOrEmpty(firmatari[0])) {
			firmatari = null;
			if (TipoAssegnazioneEnum.isIn(TipoAssegnazioneEnum.get(idTipoAssegnazione), TipoAssegnazioneEnum.FIRMA, TipoAssegnazioneEnum.FIRMA_MULTIPLA,
					TipoAssegnazioneEnum.SIGLA)) {
				firmatari = new String[1];
				firmatari[0] = TrasformerPE.getMetadato(wob, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_NODO_DESTINATARIO_WF_METAKEY))
						+ ","
						+ TrasformerPE.getMetadato(wob, PropertiesProvider.getIstance().getParameterByKey(PropertiesNameEnum.ID_UTENTE_DESTINATARIO_WF_METAKEY));
			}
		}

		final boolean aggiornaPdf = TipoAssegnazioneEnum.isIn(TipoAssegnazioneEnum.get(idTipoAssegnazione), TipoAssegnazioneEnum.FIRMA, TipoAssegnazioneEnum.FIRMA_MULTIPLA,
				TipoAssegnazioneEnum.SIGLA, TipoAssegnazioneEnum.VISTO);

		Integer tipoFirma = iaDao.getTipoFirma(utente.getIdAoo().intValue(), wob.getWorkflowName(), connection);
		if (tipoFirma == null && TipoAssegnazioneEnum.isIn(TipoAssegnazioneEnum.get(idTipoAssegnazione), 
				TipoAssegnazioneEnum.FIRMA, TipoAssegnazioneEnum.FIRMA_MULTIPLA)) {
			tipoFirma = IterApprovativoDTO.ITER_FIRMA_MANUALE;
		}
		
		if (aggiornaPdf) {
			addPDFVersion(utente, idDocumento.toString(), firmatari, flagModificaMontanaro, tipoFirma, idTipoAssegnazione);
		}
		
	}

}