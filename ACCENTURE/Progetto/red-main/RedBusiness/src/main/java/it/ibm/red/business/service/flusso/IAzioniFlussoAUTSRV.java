package it.ibm.red.business.service.flusso;

import it.ibm.red.business.service.flusso.facade.IAzioniFlussoAUTFacadeSRV;

/**
 * Interfaccia del servizio azioni flusso AUT.
 */
public interface IAzioniFlussoAUTSRV extends IAzioniFlussoAUTFacadeSRV {

}
