package it.ibm.red.business.exception;

/**
 * The Class SignException.
 *
 * @author CPIERASC
 * 
 *         Eccezione in fase di firma.
 */
public class SignException extends RuntimeException {
	
	/**
	 * Codice errore.
	 */
	private final Enum<?> codiceErrore;
	
	/**
	 * Note.
	 */
	private final String note;
	
	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Costruttore.
	 * 
	 * @param e	eccezione
	 */
	public SignException(final Exception e) {
		super(e);
		this.codiceErrore = null;
		this.note = null;
	}

	/**
	 * Costruttore.
	 * 
	 * @param msg	messaggio
	 */
	public SignException(final String msg) {
		super(msg);
		this.codiceErrore = null;
		this.note = null;
	}
	
	/**
	 * Costruttore.
	 * 
	 * @param msg	messaggio
	 * @param e	eccezione
	 * 
	 */
	public SignException(final String msg, final Exception e) {
		super(msg, e);
		this.codiceErrore = null;
		this.note = null;
	}

	/**
	 * Costruttore.
	 * @param e
	 * @param codiceErrore
	 * @param note
	 */
	public SignException(final Exception e, final Enum<?> codiceErrore, final String note) {
		super(e);
		this.codiceErrore = codiceErrore;
		this.note = note;
	}
	
	/**
	 * Costruttore.
	 * @param codiceErrore
	 * @param note
	 */
	public SignException(final Enum<?> codiceErrore, final String note) {
		super();
		this.codiceErrore = codiceErrore;
		this.note = note;
	}
	
	/**
	 * Restituisce il codice errore dell'enum.
	 * @return codice errore
	 */
	public Enum<?> getCodiceErrore() {
		return codiceErrore;
	}

	/**
	 * Restituisce le note dell'enum.
	 * @return note
	 */
	public String getNote() {
		return note;
	}
}
