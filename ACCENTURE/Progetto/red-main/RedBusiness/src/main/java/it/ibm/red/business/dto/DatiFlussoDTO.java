/**
 * 
 */
package it.ibm.red.business.dto;

import it.ibm.red.webservice.model.interfaccianps.npstypesestesi.TIPOLOGIA;

/**
 * Dati flusso NPS.
 * 
 * @author m.crescentini
 *
 */
public class DatiFlussoDTO extends AbstractDTO {
	
	private static final long serialVersionUID = -6629604355661008394L;
	
	/**
	 * Identificativo.
	 */
	private final Integer identificativo;
	
	/**
	 * Identificativo processo.
	 */
	private final String identificatoreProcesso;

	/**
	 * Codice flusso.
	 */
	private final String codiceFlusso;
	
	/**
	 * Metadati estesi.
	 */
	private final TIPOLOGIA metadatiEstesi;
	
	/**
	 * Identificativo fascicolo FEPA.
	 */
	private final String idFascicoloFepa;
	
	/**
	 * Tipologia fascicolo FEPA.
	 */
	private final String tipoFascicoloFepa;
	
	
	/**
	 * @param identificativo
	 * @param identificatoreProcesso
	 * @param codiceFlusso
	 * @param metadatiEstesi
	 */
	public DatiFlussoDTO(final Integer identificativo, final String identificatoreProcesso, final String codiceFlusso, final TIPOLOGIA metadatiEstesi) {
		this(identificativo, identificatoreProcesso, codiceFlusso, metadatiEstesi, null, null);
	}
	
	
	/**
	 * @param identificativo
	 * @param identificatoreProcesso
	 * @param codiceFlusso
	 * @param metadatiEstesi
	 * @param idFascicoloFepa
	 * @param tipoFascicoloFepa
	 */
	public DatiFlussoDTO(final Integer identificativo, final String identificatoreProcesso, final String codiceFlusso, final TIPOLOGIA metadatiEstesi, 
			final String idFascicoloFepa, final String tipoFascicoloFepa) {
		super();
		this.identificativo = identificativo;
		this.identificatoreProcesso = identificatoreProcesso;
		this.codiceFlusso = codiceFlusso;
		this.metadatiEstesi = metadatiEstesi;
		this.idFascicoloFepa = idFascicoloFepa;
		this.tipoFascicoloFepa = tipoFascicoloFepa;
	}


	/**
	 * @return
	 */
	public Integer getIdentificativo() {
		return identificativo;
	}


	/**
	 * @return the identificatoreProcesso
	 */
	public String getIdentificatoreProcesso() {
		return identificatoreProcesso;
	}

	
	/**
	 * @return the codiceFlusso
	 */
	public String getCodiceFlusso() {
		return codiceFlusso;
	}


	/**
	 * @return the metadatiEstesi
	 */
	public TIPOLOGIA getMetadatiEstesi() {
		return metadatiEstesi;
	}


	/**
	 * @return the idFascicoloFepa
	 */
	public String getIdFascicoloFepa() {
		return idFascicoloFepa;
	}


	/**
	 * @return the tipoFascicoloFepa
	 */
	public String getTipoFascicoloFepa() {
		return tipoFascicoloFepa;
	}

}
