package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

import it.ibm.red.business.dto.CollectedParametersDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.RegistroDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.TipoRegistroAusiliarioEnum;

/**
 * Facade del servizio che gestisce le registrazioni ausiliarie.
 * 
 * @author APerquoti
 * @author SimoneLungarella
 */
public interface IRegistroAusiliarioFacadeSRV extends Serializable {

	/**
	 * Restituisce tutti i registri ausiliari configurati sulla base dati.
	 * 
	 * @return registri esistenti e configurati sul database
	 */
	Collection<RegistroDTO> getRegistri();

	/**
	 * Restituisce tutti i registri esistenti associati alla coppia: tipologia documento identificata dall' <code> idTipologiaDocumento </code> /
	 * tipologia procedimento identificata dall' <code> idTipoProcedimento </code>, recuperando solo i registri della tipologia definita da <code> tipo </code>.
	 * 
	 * @param tipo tipologia del registro ausiliario, @see TipoRegistroAusiliarioEnum
	 * @param idTipologiaDocumento identificativo della tipologia del documento
	 * @param idTipoProcedimento identificativo della tipologia del procedimento
	 * @return registri
	 */
	Collection<RegistroDTO> getRegistri(TipoRegistroAusiliarioEnum tipo, Integer idTipologiaDocumento, Integer idTipoProcedimento);

	/**
	 * Consente di creare un file pdf utilizzando i template <em> xslt </em> delle
	 * registrazioni ausiliarie e una mappa di valori con cui popolare i placeholder
	 * del template.
	 * 
	 * @param id
	 *            identificativo del registro ausiliario
	 * @param stringParams
	 *            parametri per il popolamento del template
	 * @return byte array del pdf generato
	 */
	byte[] createPdf(Integer id, Map<String, String> stringParams);

	/**
	 * Consente di creare un file pdf come {@link #createPdf(Integer, Map)} ma
	 * evidenziando i campi associati ai metadati tipici di una registrazione
	 * ausiliaria che sono: numero registrazione ausiliaria, data registrazione
	 * ausiliaria, id registrazione ausiliaria. Questo metodo consente di
	 * visualizzare quali sono i metadati che subiranno modifica nel processo di
	 * validazione della registrazione ausiliaria, questo è stato reso necessario
	 * perché l'utente prima della firma andava a visualizzare un template diverso
	 * da quello firmato data che i parametri tipici della registrazione vengono
	 * recuperati in maniera sincrona con la firma.
	 * 
	 * @param id
	 *            identificativo del registro ausiliario associato al template che
	 *            consente il recupero del CLOB <em> xslt </em>
	 * @param stringParams
	 *            parametri per il popolamento dei placeholder del template
	 * @return byte array del pdf generato
	 */
	byte[] createFakePdf(Integer id, Map<String, String> stringParams);

	/**
	 * Questo metodo consente di recuperare la collection dei metadati di uno
	 * specifico registro. Il metodo non si limita al recupero dei metadati dal DB,
	 * li recupera, crea il collector adatto alla gestione di quel tipo di registro
	 * e ne popola i campi per il quale non occorrono informazioni ulteriori dalla
	 * maschera fornita all'utente in fase di predisposizione della registrazione
	 * ausiliaria.
	 * 
	 * @param connection
	 *            connessione al database
	 * @param idRegistro
	 *            identificativo del registro per il quale occorre recuperare i
	 *            metadati
	 * @param utente
	 *            utente in sessione
	 * @param documentTitle
	 *            identificativo del documento in ingresso dal quale recuperare i
	 *            valori dei metadati già esistenti per il pre popolamento del
	 *            template
	 * @return collezione dei metadati popolati dove possibile
	 */
	Collection<MetadatoDTO> getMetadatiRegistro(Integer id, UtenteDTO utente, String documentTitle);

	/**
	 * Consente di configurare i metadati passati come parametro in modo da
	 * prepopolarli dove possibile e formattarli o modificarli in base alla
	 * tipologia documento del documento identificato dal
	 * <code> documentTitle </code>.
	 * 
	 * @param id
	 *            identificativo del registro associato alla registrazione
	 *            ausiliaria
	 * @param utente
	 *            utente in sessione
	 * @param documentTitle
	 *            identificativo del documento in ingresso per il quale si devono
	 *            inizializzare i metadati
	 * @param metadatiToConfigure
	 *            metadati da modificare e inizializzare in base alle altre
	 *            informazioni ottenute dal documento in ingresso
	 */
	void configureMetadatiRegistro(Integer id, UtenteDTO utente, String documentTitle, Collection<MetadatoDTO> metadatiToConfigure);

	/**
	 * Metodo che consente il recupero del collector per effettuare una
	 * registrazione ausiliaria e inizializza i parametri restituendo un
	 * <code> DTO </code> che detiene tutte le informazioni necessarie alla
	 * registrazione ausiliaria.
	 * 
	 * @param utente
	 *            utente in sessione
	 * @param idRegistro
	 *            identificativo del registro ausiliario associato alla
	 *            registrazione ausiliaria
	 * @param listaMetadatiRegistro
	 *            lista dei metadati della registrazione ausiliaria
	 * @param checkConfiguration
	 *            se <code> true </code> è necessario controllare le caratteristiche
	 *            dei metadati della registrazione per effettuarne una validazione
	 *            sull'obbligatorietà, sulla visibilità e sulla modificabilità degli
	 *            stessi se <code> false </code> non occorre effettuare la
	 *            validazione dei metadati
	 * @return collector completo dei parametri
	 */
	CollectedParametersDTO collectParameters(UtenteDTO utente, Integer idRegistro, Collection<MetadatoDTO> listaMetadatiRegistro, boolean checkConfiguration);

	/**
	 * Metodo che consente il recupero dei metadati per il documento in uscita,
	 * occorre recuperare i metadati in maniera differente in base al tipo di
	 * registro. NB: I metadati fanno riferimento alla tipologia documento, quindi
	 * non sono valorizzati.
	 * 
	 * @param idRegistro
	 *            identificativo del registro ausiliario associato alla
	 *            registrazione ausiliaria
	 * @param documentTitle
	 *            identificativo del documento sul quale viene generata la
	 *            registrazione ausiliaria
	 * @param utente
	 *            utente in sessione
	 * @param listaMetadatiRegistro
	 *            lista dei metadati del registro ausiliario non valorizzati
	 * @param isCreazione
	 *            se <code> true </code> indica che si è in fase di creazione, se
	 *            <code> false </code> indica che si è in fase di modifica
	 * @return collezione dei metadati per il ribaltamento dei dati sull'uscita
	 *         generata in fase di predisposizione
	 */
	Collection<MetadatoDTO> getMetadatiForDocUscita(Integer idRegistro, String documentTitle, UtenteDTO utente, Collection<MetadatoDTO> listaMetadatiRegistro,
			boolean isCreazione);

	/**
	 * Metodo che consente di recuperare i metadati del documento creato e i loro
	 * valori.
	 * 
	 * @param idRegistro
	 *            id del registro, il metodo di recupero differisce in base alla
	 *            tipologia del registro
	 * @param documentTitle
	 *            identificativo del documento da cui recuperare i metadati
	 * @param utente
	 *            utente in sessione
	 * @return collezione di metadati valorizzati
	 */
	Collection<MetadatoDTO> getValuesFromDocument(Integer idRegistro, String documentTitle, UtenteDTO utente);
}
