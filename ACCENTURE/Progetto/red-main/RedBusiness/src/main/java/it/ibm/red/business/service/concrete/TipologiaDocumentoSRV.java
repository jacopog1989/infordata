package it.ibm.red.business.service.concrete;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.filenet.api.core.Document;

import it.ibm.red.business.dao.IMetadatoDAO;
import it.ibm.red.business.dao.INpsConfigurationDAO;
import it.ibm.red.business.dao.IRegistroAusiliarioDAO;
import it.ibm.red.business.dao.ITipoProcedimentoDAO;
import it.ibm.red.business.dao.ITipologiaDocumentoDAO;
import it.ibm.red.business.dto.AnagraficaDipendentiComponentDTO;
import it.ibm.red.business.dto.CapitoloSpesaMetadatoDTO;
import it.ibm.red.business.dto.KeyValueDTO;
import it.ibm.red.business.dto.LookupTableDTO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.dto.RegistroDTO;
import it.ibm.red.business.dto.SelectItemDTO;
import it.ibm.red.business.dto.TipoProcedimentoDTO;
import it.ibm.red.business.dto.TipologiaDTO;
import it.ibm.red.business.dto.TipologiaDocumentoDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.CategoriaDocumentoEnum;
import it.ibm.red.business.enums.ContextTrasformerCEEnum;
import it.ibm.red.business.enums.PropertiesNameEnum;
import it.ibm.red.business.enums.TipoCategoriaEnum;
import it.ibm.red.business.enums.TipoMetadatoEnum;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.helper.filenet.ce.FilenetCEHelperProxy;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.helper.filenet.ce.trasform.impl.TrasformerCE;
import it.ibm.red.business.helper.metadatiestesi.MetadatiEstesiHelper;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.persistence.model.LookupTable;
import it.ibm.red.business.persistence.model.NpsConfiguration;
import it.ibm.red.business.provider.PropertiesProvider;
import it.ibm.red.business.service.ILookupTableSRV;
import it.ibm.red.business.service.ITipoProcedimentoSRV;
import it.ibm.red.business.service.ITipologiaDocumentoSRV;
import it.ibm.red.business.service.IWorkflowSRV;
import it.ibm.red.business.utils.StringUtils;

/**
 * Service che gestisce le tipologie documento.
 */
@Service
public class TipologiaDocumentoSRV extends AbstractService implements ITipologiaDocumentoSRV {

	/**
	 * La costante serialVersionUID.
	 */
	private static final long serialVersionUID = -3721868728523985495L;

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(TipologiaDocumentoSRV.class.getName());

	/**
	 * Messaggio errore recuperto tipologie documenti.
	 */
	private static final String ERROR_RECUPERO_TIPOLOGIA_DOCUMENTO_MSG = "Errore durante il recupero delle tipologie Documento";

	/**
	 * DAO.
	 */
	@Autowired
	private ITipologiaDocumentoDAO tipologiaDocumentoDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IMetadatoDAO metadatoDAO;

	/**
	 * Servizio.
	 */
	@Autowired
	private ILookupTableSRV lookupTableSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private IWorkflowSRV workflowSRV;

	/**
	 * Servizio.
	 */
	@Autowired
	private ITipoProcedimentoSRV tipoProcedimentoSRV;

	/**
	 * DAO.
	 */
	@Autowired
	private ITipoProcedimentoDAO tipoProcedimentoDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private IRegistroAusiliarioDAO registroDAO;

	/**
	 * DAO.
	 */
	@Autowired
	private INpsConfigurationDAO npsConfigurationDAO;

	/**
	 * @see it.ibm.red.business.service.ITipologiaDocumentoSRV#getComboTipologieDocumentoAttive(java.lang.Long,
	 *      java.lang.Long, java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public List<TipologiaDocumentoDTO> getComboTipologieDocumentoAttive(final Long idAoo, final Long idUfficio, final Integer idTipoCategoria, final Connection connection) {
		List<TipologiaDocumentoDTO> output = new ArrayList<>();
		try {

			output = tipologiaDocumentoDAO.getComboTipologieByUtenteAndTipoCategoria(true, false, false, idAoo, idUfficio, idTipoCategoria, connection);

		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_TIPOLOGIA_DOCUMENTO_MSG, e);
			throw new RedException(ERROR_RECUPERO_TIPOLOGIA_DOCUMENTO_MSG, e);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.ITipologiaDocumentoSRV#getComboTipologieDocumentoDisattive(java.lang.Long,
	 *      java.lang.Long, java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public List<TipologiaDocumentoDTO> getComboTipologieDocumentoDisattive(final Long idAoo, final Long idUfficio, final Integer idTipoCategoria,
			final Connection connection) {
		List<TipologiaDocumentoDTO> output = new ArrayList<>();
		try {

			output = tipologiaDocumentoDAO.getComboTipologieByUtenteAndTipoCategoria(false, true, false, idAoo, idUfficio, idTipoCategoria, connection);

		} catch (final Exception e) {
			LOGGER.error(ERROR_RECUPERO_TIPOLOGIA_DOCUMENTO_MSG, e);
			throw new RedException(ERROR_RECUPERO_TIPOLOGIA_DOCUMENTO_MSG, e);
		}
		return output;
	}

	/**
	 * @see it.ibm.red.business.service.ITipologiaDocumentoSRV#getComboTipologieByUtenteAndTipoCategoriaAndTipologiaSelezionataNoAutomatici(java.lang.Long,
	 *      java.lang.Long, java.lang.Integer, java.lang.Integer,
	 *      java.sql.Connection).
	 */
	@Override
	public List<TipologiaDocumentoDTO> getComboTipologieByUtenteAndTipoCategoriaAndTipologiaSelezionataNoAutomatici(final Long idAoo, final Long idUfficio,
			final Integer idTipoCategoria, final Integer idTipologiaDocumento, final Connection connection) {
		List<TipologiaDocumentoDTO> tipologiaList = null;

		try {
			tipologiaList = tipologiaDocumentoDAO.getComboTipologieByUtenteAndTipoCategoria(false, false, true, idAoo, idUfficio, idTipoCategoria, connection);

			tipologiaList = filterByDateAndTipologiaSelezionata(tipologiaList, idTipologiaDocumento);

		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero delle tipologie attive", e);
			throw e;
		}

		return tipologiaList;
	}

	/**
	 * Filtra la lista delle tipologie documento per data disattivazione.
	 * Attualmente durante il giorno di disattivazione si considera attiva la
	 * tipologia documento.
	 * 
	 * @param tipologiaList
	 * @return la lista filtrata
	 */
	private static List<TipologiaDocumentoDTO> filterByDate(List<TipologiaDocumentoDTO> tipologiaList) {
		final Date now = new Date();

		tipologiaList = tipologiaList.stream().filter(t -> t.getDataDisattivazione() == null || t.getDataDisattivazione().after(now)).collect(Collectors.toList());

		return tipologiaList;
	}

	/**
	 * Filtra la lista delle tipologie documento per data disattivazione, includendo
	 * sempre la tipologia in input. Attualmente durante il giorno di disattivazione
	 * si considera attiva la tipologia documento.
	 * 
	 * @param tipologiaList
	 * @param idTipologiaDocumento Viene passato solo in modifica, così che nella
	 *                             lista rientri in ogni caso la tipologia documento
	 *                             del documento, anche se disattivata.
	 * @return la lista filtrata
	 */
	private static List<TipologiaDocumentoDTO> filterByDateAndTipologiaSelezionata(List<TipologiaDocumentoDTO> tipologiaList, final Integer idTipologiaDocumento) {
		final Date now = new Date();

		tipologiaList = tipologiaList.stream()
				.filter(t -> t.getDataDisattivazione() == null || t.getIdTipologiaDocumento().equals(idTipologiaDocumento) || t.getDataDisattivazione().after(now))
				.collect(Collectors.toList());

		return tipologiaList;
	}

	/**
	 * Restituisce le tipologia documento attive della categoria identificata
	 * dall'Enum @see TipoCategoriaEnum: <code> tipoCategoria </code> valide per
	 * l'AOO identificata dall': <code> idAoo </code>.
	 * NB: Una tipologia documento risulta attiva quando non è stata superata la sua data di disabilitazione.
	 * 
	 * @see it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV#
	 *      getComboTipologieByAooAndTipoCategoriaAttivi(it.ibm.red.business.enums.
	 *      TipoCategoriaEnum, java.lang.Long).
	 * @param tipoCategoria
	 *            tipo categoria del documento
	 * @param idAoo
	 *            identificativo dell'Area organizzativa
	 * @return lista delle tipologia documento
	 */
	@Override
	public List<TipologiaDocumentoDTO> getComboTipologieByAooAndTipoCategoriaAttivi(final TipoCategoriaEnum tipoCategoria, final Long idAoo) {
		List<TipologiaDocumentoDTO> tipologiaList = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			tipologiaList = getComboTipologieByAooAndTipoCategoriaAttivi(tipoCategoria, idAoo, connection);
		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return tipologiaList;
	}

	/**
	 * @see it.ibm.red.business.service.ITipologiaDocumentoSRV#getComboTipologieByAooAndTipoCategoriaAttivi(it.ibm.red.business.enums.TipoCategoriaEnum,
	 *      java.lang.Long, java.sql.Connection).
	 */
	@Override
	public List<TipologiaDocumentoDTO> getComboTipologieByAooAndTipoCategoriaAttivi(final TipoCategoriaEnum tipoCategoria, final Long idAoo, final Connection connection) {
		List<TipologiaDocumentoDTO> tipologiaList = tipologiaDocumentoDAO.getComboTipologieByAooAndTipoCategoria(idAoo, tipoCategoria.getIdTipoCategoria(), connection);

		tipologiaList = filterByDate(tipologiaList);
		return tipologiaList;
	}

	/**
	 * Restituisce le tipologie documento del tipo categoria specificato dal
	 * parametro: <code> tce </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV#
	 *      getTipologieDocumentoByCategoriaDocumento
	 *      (it.ibm.red.business.enums.TipoCategoriaEnum,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 * 
	 * @param tce
	 *            tipo categoria tipologie da recuperare
	 * @param utente
	 *            utente in sessione
	 */
	@Override
	public List<TipologiaDocumentoDTO> getTipologieDocumentoByCategoriaDocumento(final TipoCategoriaEnum tce, final UtenteDTO utente) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			return tipologiaDocumentoDAO.getComboTipologieByAooAndTipoCategoria(utente.getIdAoo(), tce.getIdTipoCategoria(), connection);

		} catch (final Exception e) {
			LOGGER.error(e.getMessage(), e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV#getById(java.lang.Integer).
	 */
	@Override
	public TipologiaDocumentoDTO getById(final Integer idTipologiaDocumento) {
		TipologiaDocumentoDTO tipologia = null;
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			tipologia = getById(idTipologiaDocumento, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durate il recupero della tipologia documento per l'id: " + idTipologiaDocumento, e);
			throw new RedException("Errore durate il recupero della tipologia documento per l'id: " + idTipologiaDocumento, e);
		} finally {
			closeConnection(connection);
		}

		return tipologia;
	}

	/**
	 * @see it.ibm.red.business.service.ITipologiaDocumentoSRV#getById(java.lang.Integer, java.sql.Connection).
	 */
	@Override
	public TipologiaDocumentoDTO getById(final Integer idTipologiaDocumento, final Connection connection) {
		TipologiaDocumentoDTO tipologia = null;

		try {
			tipologia = tipologiaDocumentoDAO.getById(idTipologiaDocumento, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero della tipologia documento con ID: " + idTipologiaDocumento, e);
			throw e;
		}

		return tipologia;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV#getTipologiaDocumentalePredefinita(java.lang.Long,
	 *      java.lang.String, it.ibm.red.business.enums.TipoCategoriaEnum).
	 */
	@Override
	public Integer getTipologiaDocumentalePredefinita(final Long idAoo, final String codiceAoo, final TipoCategoriaEnum tipoCategoriaDocumento) {
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			return getTipologiaDocumentalePredefinita(idAoo, codiceAoo, tipoCategoriaDocumento, con);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero della tipologia documentale predefinita per l'AOO: " + idAoo + ", categoria: " + tipoCategoriaDocumento.toString(), e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}
	}

	/**
	 * @see it.ibm.red.business.service.ITipologiaDocumentoSRV#getTipologiaDocumentalePredefinita(java.lang.Long,
	 *      java.lang.String, it.ibm.red.business.enums.TipoCategoriaEnum,
	 *      java.sql.Connection).
	 */
	@Override
	public Integer getTipologiaDocumentalePredefinita(final Long idAoo, final String codiceAoo, final TipoCategoriaEnum tipoCategoriaDocumento, final Connection con) {
		int idTipologiaDocumento = 0;

		final String idTipologiaDocumentoString = PropertiesProvider.getIstance().getParameterByString(codiceAoo + ".tipologia.generica.default");
		if (tipoCategoriaDocumento.equals(TipoCategoriaEnum.ENTRATA) && !StringUtils.isNullOrEmpty(idTipologiaDocumentoString)) {
			idTipologiaDocumento = Integer.parseInt(idTipologiaDocumentoString);
		} else {
			final List<TipologiaDocumentoDTO> tipologieDocumento = tipologiaDocumentoDAO.getComboTipologieByAooAndTipoCategoria(idAoo,
					tipoCategoriaDocumento.getIdTipoCategoria(), con);
			if (!CollectionUtils.isEmpty(tipologieDocumento)) {
				idTipologiaDocumento = tipologieDocumento.get(0).getIdTipologiaDocumento();
			}
		}

		return idTipologiaDocumento;
	}

	/**
	 * @see it.ibm.red.business.service.ITipologiaDocumentoSRV#getContextTipologieDocumento(java.lang.Integer,
	 *      it.ibm.red.business.dto.UtenteDTO, java.sql.Connection).
	 */
	@Override
	public Map<ContextTrasformerCEEnum, Object> getContextTipologieDocumento(final Integer idCategoriaDocumento, final UtenteDTO utente, final Connection connection) {
		final Map<ContextTrasformerCEEnum, Object> context = new EnumMap<>(ContextTrasformerCEEnum.class);

		final CategoriaDocumentoEnum categoriaDOC = CategoriaDocumentoEnum.getExactly(idCategoriaDocumento);
		final TipoCategoriaEnum tce = (CategoriaDocumentoEnum.DOCUMENTO_ENTRATA.equals(categoriaDOC) || CategoriaDocumentoEnum.ASSEGNAZIONE_INTERNA.equals(categoriaDOC))
				? TipoCategoriaEnum.ENTRATA
				: TipoCategoriaEnum.USCITA;

		/*
		 * Recupero lista tipi documento attivi
		 */
		final List<TipologiaDocumentoDTO> tipoDocAttive = getComboTipologieDocumentoAttive(utente.getIdAoo(), utente.getIdUfficio(), tce.getIdTipoCategoria(), connection);
		context.put(ContextTrasformerCEEnum.TIPOLOGIE_DOCUMENTO_ATTIVE, tipoDocAttive);

		/*
		 * Recupero lista tipi documento non attivi
		 */
		final List<TipologiaDocumentoDTO> tipoDocDisattive = getComboTipologieDocumentoDisattive(utente.getIdAoo(), utente.getIdUfficio(), tce.getIdTipoCategoria(),
				connection);
		final Map<Integer, TipologiaDocumentoDTO> tipoDocDisattiveMap = new HashMap<>();
		for (final TipologiaDocumentoDTO td : tipoDocDisattive) {
			tipoDocDisattiveMap.put(td.getIdTipologiaDocumento(), td);
		}
		context.put(ContextTrasformerCEEnum.TIPOLOGIE_DOCUMENTO_NON_ATTIVE, tipoDocDisattiveMap);
		return context;
	}

	/**
	 * Questo metodo gestisce il salvataggio di una tipologia attraverso il Wizard.
	 */
	@Override
	public void save(final String nomeTipoDocumento, final String tipoGestione, final long idAoo, final TipoCategoriaEnum tipoCategoria,
			final Collection<TipoProcedimentoDTO> procedimenti) {

		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), true);

			if (TipoCategoriaEnum.ENTRATA_ED_USCITA.equals(tipoCategoria)) {
				// Salvo in ENTRATA
				for (final TipoProcedimentoDTO p : procedimenti) {
					p.setParametriCategoria(TipoCategoriaEnum.ENTRATA);
					p.setWorkflowId((long) workflowSRV.getWorkflowId(p));
				}

				saveTipologiaDocumento(nomeTipoDocumento, tipoGestione, idAoo, TipoCategoriaEnum.ENTRATA, procedimenti, connection);

				// Salvo in USCITA
				for (final TipoProcedimentoDTO p : procedimenti) {
					p.setParametriCategoria(TipoCategoriaEnum.USCITA);
					p.setWorkflowId((long) workflowSRV.getWorkflowId(p));
				}
				saveTipologiaDocumento(nomeTipoDocumento, tipoGestione, idAoo, TipoCategoriaEnum.USCITA, procedimenti, connection);
			} else {
				// Salvo solo ENTRATA o solo USCITA
				for (final TipoProcedimentoDTO p : procedimenti) {
					p.setParametriCategoria(tipoCategoria);
					p.setWorkflowId((long) workflowSRV.getWorkflowId(p));
				}
				saveTipologiaDocumento(nomeTipoDocumento, tipoGestione, idAoo, tipoCategoria, procedimenti, connection);
			}
			commitConnection(connection);

		} catch (final Exception e) {
			LOGGER.error("Errore durante il salvataggio del tipo documento", e);
			rollbackConnection(connection);
			throw new RedException("Errore durante il salvataggio del tipo documento", e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Restituisce la lista dei metadati estesi validi per la coppia tipologia
	 * procedimento / tipologia documento identificata dagli id:
	 * <code> idTipologiaDocumento </code> e <code> idTipoProcedimento </code>.
	 * 
	 * @see it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV#
	 *      caricaMetadatiEstesiPerGUI(int, int, boolean, long).
	 * 
	 * @param idTipologiaDocumento
	 *            identificativo tipologia documento
	 * @param idTipoProcedimento
	 *            identificativo tipologia procedimento
	 * @param forCreazione
	 *            flag che indica se occorre recuperare i metadati per la creazione
	 *            o per modifica o per ricerca
	 * @param idAoo
	 *            identificativo Area Organizzativa
	 * @param dateTarget
	 *            data di riferimento alla fase di utilizzo dei metadati
	 * @return lista dei metadati estesi formattati per l'interfaccia grafica
	 */
	@Override
	public List<MetadatoDTO> caricaMetadatiEstesiPerGUI(final int idTipologiaDocumento, final int idTipoProcedimento, final boolean forCreazione, final long idAoo,
			Date dateTarget) {
		final List<MetadatoDTO> metadatiEstesi = new ArrayList<>();
		if (forCreazione) {
			dateTarget = new Date();
		}

		final List<MetadatoDTO> tmp = caricaMetadati(idTipologiaDocumento, idTipoProcedimento, dateTarget, idAoo);

		for (final MetadatoDTO m : tmp) {
			if (Boolean.TRUE.equals(m.getFlagRange()) && !forCreazione && dateTarget == null) {

				final MetadatoDTO metadatoA = new MetadatoDTO(m);
				metadatoA.setFlagRangeStop(true);
				metadatiEstesi.add(0, metadatoA);

				final MetadatoDTO metadatoDa = new MetadatoDTO(m);
				metadatoDa.setFlagRangeStart(true);
				metadatiEstesi.add(0, metadatoDa);

			} else {
				metadatiEstesi.add(m);
			}
		}

		return metadatiEstesi;
	}

	private List<MetadatoDTO> caricaMetadati(final int idTipologiaDocumento, final int idTipoProcedimento, final Date dateTarget, final long idAoo,
			final Connection connection) {
		List<MetadatoDTO> metadati = new ArrayList<>();

		try {
			metadati = metadatoDAO.caricaMetadati(idTipologiaDocumento, idTipoProcedimento, connection);

			for (int i = 0; i < metadati.size(); i++) {
				final MetadatoDTO m = metadati.get(i);

//				<-- Gestione Lookup Table -->
				if (TipoMetadatoEnum.LOOKUP_TABLE.equals(m.getType())) {
					
					final List<SelectItemDTO> lookupValues = lookupTableSRV.getValues(
							m.getLookupTableSelector(), dateTarget, idAoo, connection);
					final LookupTableDTO lt = new LookupTableDTO(m.getId(), m.getName(), m.getDisplayName(), m.getLookupTableMode(), 
							m.getVisibility(), m.getEditability(), m.getObligatoriness(), m.getFlagOut(), lookupValues, m.isMetadatoValutaFlag());
					metadati.set(i, lt);

				}

//				<-- Gestione Selettore Persona Fisica -->
				if (TipoMetadatoEnum.PERSONE_SELECTOR.equals(m.getType())) {

					final AnagraficaDipendentiComponentDTO adc = new AnagraficaDipendentiComponentDTO(m.getName(), m.getDisplayName(), m.getVisibility(), m.getEditability(),
							m.getObligatoriness(), false, m.getFlagOut());
					metadati.set(i, adc);

				}

//				<-- Gestione Selettore Capitoli -->
				if (TipoMetadatoEnum.CAPITOLI_SELECTOR.equals(m.getType())) {

					final CapitoloSpesaMetadatoDTO csm = new CapitoloSpesaMetadatoDTO(m.getName(), m.getDisplayName(), m.getVisibility(), m.getEditability(),
							m.getObligatoriness(), m.getFlagOut());
					metadati.set(i, csm);

				}
			}
		} catch (final Exception e) {
			LOGGER.error("Errore nel carimento dei metadati estesi per la tipologia documento: " + idTipologiaDocumento + " e il tipo procedimento: " + idTipoProcedimento, e);
			throw new RedException("Errore nel carimento dei metadati estesi", e);
		}

		return metadati;
	}

	/**
	 * Restituisce i metadati validi per una coppia tipologia documento / tipologia
	 * procedimento. I metadati sono diversi in base alla date target.
	 * 
	 * @see it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV#
	 *      caricaMetadati(int, int, java.util.Date, boolean, long).
	 * @param idTipologiaDocumento
	 *            identificativo tipologia documento
	 * @param idTipoProcedimento
	 *            identificativo tipologia procedimento
	 * @param dateTarget
	 *            in creazione la date target dovrà essere concorrente alla
	 *            creazione, in fase di modifica occorre far riferimento alla data
	 *            di creazione del documento, mentre in fase di ricerca la date
	 *            target deve essere <code> null </code>.
	 * @param idAoo
	 *            identificativo dell'Area Organizzativa
	 * @return lista dei metadati recuperati
	 */
	@Override
	public final List<MetadatoDTO> caricaMetadati(final int idTipologiaDocumento, final int idTipoProcedimento, final Date dateTarget, final long idAoo) {
		Connection connection = null;

		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			return caricaMetadati(idTipologiaDocumento, idTipoProcedimento, dateTarget, idAoo, connection);

		} catch (final SQLException e) {
			LOGGER.error("Errore nella connessione al DB", e);
			throw new RedException("Errore nel caricamento dei metadati estesi", e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Restituisce l'id della tipologia documento associata al documento con
	 * descrizione: <code> descrizioneTipologiaDocumento </code> e il cui tipo
	 * categoria è uguale a <code> tce </code> @see TipoCategoriaEnum.
	 * 
	 * @see it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV#
	 *      getTipologiaDocumentoByNomeAndCategoriaDocumento( java.lang.String,
	 *      it.ibm.red.business.enums.TipoCategoriaEnum, java.lang.Long).
	 * @param descrizioneTipologiaDocumento
	 *            descrizione della tipologia documento
	 * @param tce
	 *            tipo categoria del documento
	 * @param idAoo
	 *            identificativo dell'Area Organizzativa
	 * @return identificativo tipologia documento
	 */
	@Override
	public final Integer getIdTipologiaDocumentoByDescrizioneAndCategoria(final String descrizioneTipologiaDocumento, final TipoCategoriaEnum tce, final Long idAoo) {
		Integer output = null;
		Connection con = null;

		try {
			con = setupConnection(getDataSource().getConnection(), false);

			output = tipologiaDocumentoDAO.getIdTipologiaDocumentoByDescrizioneAndTipoCategoria(descrizioneTipologiaDocumento, tce.getIdTipoCategoria(), idAoo, con);

		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero della tipologia documento: " + descrizioneTipologiaDocumento + " con categoria: " + tce.toString(), e);
			throw new RedException("Errore nel recupero della tipologia documento: " + descrizioneTipologiaDocumento + " con categoria: " + tce.toString(), e);
		} finally {
			closeConnection(con);
		}

		return output;
	}

	/**
	 * Carica tutti i valori di uno specifico selettore identificato dal nome:
	 * <code> selector </code>.
	 * 
	 * @param selector
	 *            Nome del selettore per il quale devono esserne recuperati i values
	 * @param dateTarget
	 *            in creazione la date target dovrà essere concorrente alla
	 *            creazione, in fase di modifica occorre far riferimento alla data
	 *            di creazione del documento, mentre in fase di ricerca la date
	 *            target deve essere <code> null </code>.
	 * @param forCreazione
	 *            flag che specifica fase di creazione
	 * @param idAoo
	 *            Se null vuol dire che il selettore non ha valenza per un solo AOO,
	 *            altrimenti indica l'identificativo dell'Area organizzativa
	 * @return lista dei values come @see SelectItemDTO
	 */
	@Override
	public List<SelectItemDTO> getValuesBySelector(final String selector, final Date dateTarget, final Long idAoo) {
		Connection connection = null;
		List<SelectItemDTO> values = new ArrayList<>();
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			values = lookupTableSRV.getValuesBySelector(selector, dateTarget, idAoo, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore nel recupero dei valore del selettore:" + selector, e);
		} finally {
			closeConnection(connection);
		}
		return values;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV#saveLookupTables(java.util.List).
	 */
	@Override
	public void saveLookupTables(final List<LookupTable> lookups) {
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			lookupTableSRV.saveLookupTables(lookups, connection);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il salvataggio delle lookup table", e);
		} finally {
			closeConnection(connection);
		}
	}

	/**
	 * Rende persistente la tipologia creata e l'associazione alle tipologie
	 * procedimenti.
	 * 
	 * @param nomeTipoDocumento
	 *            Nome della tipologia documento da memorizzare.
	 * @param tipoGestione
	 *            Tipo gestione della tipologia.
	 * @param idAoo
	 *            Identificativo dell'area organizzativa.
	 * @param tipoCategoria
	 *            Tipologia categoira della tipologia.
	 * @param procedimenti
	 *            Procedimenti associati alla tipologia documento.
	 * @param connection
	 *            Connessione al database.
	 */
	protected void saveTipologiaDocumento(final String nomeTipoDocumento, final String tipoGestione, final long idAoo, final TipoCategoriaEnum tipoCategoria,
			final Collection<TipoProcedimentoDTO> procedimenti, final Connection connection) {
		int flagGenericoEntrataProc = 1;
		int flagGenericoUscitaProc = 0;

		// Imposto i flag generici a partire dal tipo di categoria del documento
		if (tipoCategoria.equals(TipoCategoriaEnum.ENTRATA)) {
			flagGenericoEntrataProc = 1;
			flagGenericoUscitaProc = 0;
		} else if (tipoCategoria.equals(TipoCategoriaEnum.USCITA)) {
			flagGenericoEntrataProc = 0;
			flagGenericoUscitaProc = 1;
		}

		// Ottengo una lista dei procedimenti già esistenti per far in modo di non
		// ricreare gli stessi procedimenti
		final Collection<TipoProcedimentoDTO> existingProcedimenti = tipoProcedimentoSRV.getProcedimentoByFlags(idAoo, flagGenericoEntrataProc, flagGenericoUscitaProc);
		final Collection<String> descExistingProc = new ArrayList<>();

		for (final TipoProcedimentoDTO proc : existingProcedimenti) {
			descExistingProc.add(proc.getDescrizione());
		}

		Long idProcedimento = 0L;
		int idMetadato = 0;

		if (connection != null) {
			try {
				final int idDocumento = tipologiaDocumentoDAO.save(nomeTipoDocumento, tipoGestione, idAoo, tipoCategoria.getIdTipoCategoria(), connection);

				for (final TipoProcedimentoDTO procedimento : procedimenti) {
					// Se il procedimento non esiste già in DB, lo salvo ottenendo l'id e aggiornare
					// la cross
					if (!descExistingProc.contains(procedimento.getDescrizione())) {
						idProcedimento = tipoProcedimentoSRV.save(procedimento);

						if (idProcedimento != 0L) {
							tipoProcedimentoDAO.saveCrossProcedimentoAOO(idProcedimento, idAoo, connection);
						}
					} else {
						// Ottengo l'id
						for (final TipoProcedimentoDTO proc : existingProcedimenti) {
							if (proc.getDescrizione().equals(procedimento.getDescrizione())) {
								idProcedimento = proc.getTipoProcedimentoId();
								break;
							}
						}
					}

					// Se il procedimento è stato salvato o già esiste in db l'id non sarà 0 quindi
					// salvo la relazione con la tipologia documento
					if (idProcedimento != 0L) {
						tipoProcedimentoDAO.saveCrossProcedimentoDocumento(connection, idProcedimento, idDocumento, procedimento);
					}

					// Se il procedimento ha metadati associati
					if (procedimento.getMetadati() != null) {
						// Salvo il metadato ottenendo l'id
						for (final MetadatoDTO metadato : procedimento.getMetadati()) {
							idMetadato = metadatoDAO.save(metadato, connection);
							// Salvo relazione tra metadato tipologia documento e tipologia procedimento
							if (idMetadato != 0) {
								metadatoDAO.saveCrossDocumentoMetadatoProcedimento(connection, idDocumento, idProcedimento, idMetadato);
							}
						}

					}

					// Se il procedimento ha registri
					if (procedimento.getRegistri() != null) {
						// Salvo relazione tra procedimento e registri
						for (final RegistroDTO registro : procedimento.getRegistri()) {
							registroDAO.saveCrossProcedimentoRegistro(connection, idProcedimento, idDocumento, registro.getId());
						}
					}
				}
			} catch (final Exception e) {
				LOGGER.error(e.getMessage(), e);
				throw new RedException(e.getMessage(), e);
			}
		}
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV#getIdAOOByCodiceNPS(java.lang.String).
	 */
	@Override
	public Integer getIdAOOByCodiceNPS(final String codiceAOO) {
		Integer out = null;
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);
			final NpsConfiguration npsConfig = npsConfigurationDAO.getByCodiceAoo(codiceAOO, con);
			out = npsConfig.getIdAoo();
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dell'identificativo dell'aoo", e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

		return out;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV#getTipologiaFromTipologiaNPS(java.lang.Boolean,
	 *      java.lang.Integer, java.lang.String).
	 */
	@Override
	public TipologiaDTO getTipologiaFromTipologiaNPS(final Boolean isIngresso, final Integer idAOO, final String tipologiaNPS) {
		TipologiaDTO out = null;
		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);

			out = npsConfigurationDAO.getTipologia(isIngresso, idAOO, tipologiaNPS, con);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero della tipologia.", e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

		return out;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV#getDescTipologiaDocumentoById(java.lang.Integer).
	 */
	@Override
	public String getDescTipologiaDocumentoById(final Integer idTipologiaDocumento) {
		String descrizioneTipologiaDocumento = null;

		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);

			descrizioneTipologiaDocumento = tipologiaDocumentoDAO.getDescTipologiaDocumentoById(idTipologiaDocumento, con);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero della tipologia.", e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

		return descrizioneTipologiaDocumento;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV#recuperaMetadatiEstesi(java.lang.String,
	 *      it.ibm.red.business.dto.UtenteDTO).
	 */
	@Override
	public Collection<MetadatoDTO> recuperaMetadatiEstesi(final String documentTitle, final UtenteDTO utente) {
		Collection<MetadatoDTO> metadatiEstesi = new ArrayList<>();
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);
			final IFilenetCEHelper fceh = FilenetCEHelperProxy.newInstance(utente.getFcDTO(), utente.getIdAoo());

			// Gestisco i metadati in base alla tipologia di documento ottenuta in ingresso
			final Document documentIngresso = fceh.getDocumentByDTandAOO(documentTitle, utente.getIdAoo());

			final Integer idTipologiaDocumento = fceh.getTipoDocumentoByDocumentTitle(documentTitle);
			final Integer idTipoProcedimento = (Integer) TrasformerCE.getMetadato(documentIngresso, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY);

			final List<MetadatoDTO> metadatiTipologiaDocumento = metadatoDAO.caricaMetadati(idTipologiaDocumento, idTipoProcedimento, connection);
			final String metadatiXml = tipologiaDocumentoDAO.recuperaMetadatiEstesi(connection, Integer.parseInt(documentTitle), utente.getIdAoo(),
					Long.valueOf(idTipologiaDocumento), Long.valueOf(idTipoProcedimento));

			metadatiEstesi = MetadatiEstesiHelper.deserializeMetadatiEstesi(metadatiXml, metadatiTipologiaDocumento);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei metadati del documento con document title:" + documentTitle, e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return metadatiEstesi;
	}

	/**
	 * @see it.ibm.red.business.service.facade.ITipologiaDocumentoFacadeSRV#recuperaMetadatiEstesi(java.lang.String,
	 *      it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper, java.lang.Long).
	 */
	@Override
	public Collection<MetadatoDTO> recuperaMetadatiEstesi(final String documentTitle, final IFilenetCEHelper fceh, final Long idAoo) {
		Collection<MetadatoDTO> metadatiEstesi = new ArrayList<>();
		Connection connection = null;
		try {
			connection = setupConnection(getDataSource().getConnection(), false);

			// Gestisco i metadati in base alla tipologia di documento ottenuta in ingresso
			final Document documentIngresso = fceh.getDocumentByDTandAOO(documentTitle, idAoo);

			final Integer idTipologiaDocumento = fceh.getTipoDocumentoByDocumentTitle(documentTitle);
			final Integer idTipoProcedimento = (Integer) TrasformerCE.getMetadato(documentIngresso, PropertiesNameEnum.TIPO_PROCEDIMENTO_ID_FN_METAKEY);

			final List<MetadatoDTO> metadatiTipologiaDocumento = metadatoDAO.caricaMetadati(idTipologiaDocumento, idTipoProcedimento, connection);
			final String metadatiXml = tipologiaDocumentoDAO.recuperaMetadatiEstesi(connection, Integer.parseInt(documentTitle), idAoo, Long.valueOf(idTipologiaDocumento),
					Long.valueOf(idTipoProcedimento));

			metadatiEstesi = MetadatiEstesiHelper.deserializeMetadatiEstesi(metadatiXml, metadatiTipologiaDocumento);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero dei metadati del documento con document title:" + documentTitle, e);
			throw new RedException(e);
		} finally {
			closeConnection(connection);
		}

		return metadatiEstesi;
	}

	@Override
	public List<KeyValueDTO> getTipologieDocumentoNPS(UtenteDTO utente) {
		List<KeyValueDTO> tipiDocumentoNPS = null;

		Connection con = null;
		try {
			con = setupConnection(getDataSource().getConnection(), false);

			tipiDocumentoNPS = tipologiaDocumentoDAO.getTipologieDocumentoNPS(utente.getIdAoo(), con);
		} catch (final Exception e) {
			LOGGER.error("Errore durante il recupero delle tipologie documento NPS.", e);
			throw new RedException(e);
		} finally {
			closeConnection(con);
		}

		return tipiDocumentoNPS;
	}

}
