package it.ibm.red.business.utils;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;

import it.ibm.red.business.dto.AssegnatarioOrganigrammaDTO;
import it.ibm.red.business.dto.NodoOrganigrammaDTO;
import it.ibm.red.business.logger.REDLogger;

/**
 * Utilità per tutte le response che fanno uso di un organigramma.
 * In cui è presente il concetto di sicurezza o assegnatario
 */
public final class OrganigrammaRetrieverUtils {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(OrganigrammaRetrieverUtils.class);

	private OrganigrammaRetrieverUtils() {
	}

	/**
	 * Crea un oggetto più semplice, con le informazioni strettamente
	 * necessarie ad un'assegnazione, a partire dal nodo in input.
	 * @param organigramma
	 * @return assegnatario
	 */
	public static AssegnatarioOrganigrammaDTO create(final NodoOrganigrammaDTO organigramma) {
		final Long idUfficio = organigramma.getIdNodo();
		final Long idUtente = organigramma.getIdUtente() == null ? 0L : organigramma.getIdUtente();
		return new AssegnatarioOrganigrammaDTO(idUfficio, idUtente);
	}

	/**
	 * Restituisce idNodo,idUtente.
	 *
	 * Questo è il formato standard in cui vengono utilizati gli assegnatariOrganigramma per gestire la security
	 *
	 * @param assegnatario
	 * @return
	 *  Una rappresentazione testuale dell'assegnatarioOrganigramma
	 */
	public static String getArrayAssegnazione(final AssegnatarioOrganigrammaDTO assegnatario) {
		final StringBuilder str = new StringBuilder();
		str.append(assegnatario.getIdNodo().intValue());
		str.append(',');
		str.append(assegnatario.getIdUtente().intValue());
		return str.toString();
	}

	/**
	 * Ritorna un array di descrizioni Assegnazione utilizzate per la sicurezza.
	 *
	 * @param assegnazioneList
	 * @return
	 */
	public static String[] initArrayAssegnazioni(final Collection<AssegnatarioOrganigrammaDTO> assegnazioneList) {
		final List<String> stringList = assegnazioneList.stream()
				.map(c -> getArrayAssegnazione(c))
				.collect(Collectors.toList());
		final String[] array = new String[stringList.size()];
		return stringList.toArray(array);
	}

	/**
	 * Ritorna un array di id Nodo ovvero di idUfficio.
	 *
	 * @param assegnazioneList
	 * @return
	 */
	public static String[] initArrayUfficio(final Collection<AssegnatarioOrganigrammaDTO> assegnazioneList) {
		final List<String> stringList = assegnazioneList.stream()
				.map(c -> Integer.toString(c.getIdNodo().intValue()))
				.collect(Collectors.toList());

		final String[] array = new String[stringList.size()];
		return stringList.toArray(array);
	}

	/**
	 * Permette di convertire una assegnazione dal formato stringa ' idnodocorrente , idUtenteCorrente' al bean AssegnatarioOrganigrammaDTO
	 *
	 * @param assegnazione
	 *  stringa da convertire in assegnazione
	 *
	 * @return oggetto rappresentante la stringa.
	 *
	 * @throws NumberFormatException
	 *  Quando il formato della stringa passata non è una assegnazione.
	 */
	public static AssegnatarioOrganigrammaDTO create(final String assegnazione) {
		Long idNodoCorrente = null;
		Long idUtenteCorrente = 0L;
		if (StringUtils.isBlank(assegnazione)) {
			throw new NumberFormatException("Input is null or blank");
		}
		
		final String[] splittedValue = assegnazione.split(",");
		if (splittedValue != null && splittedValue.length > 1) {
			idNodoCorrente = Long.parseLong(splittedValue[0]);
			idUtenteCorrente = Long.parseLong(splittedValue[1]);
		} else {
			idNodoCorrente = Long.parseLong(assegnazione);
		}
		return new AssegnatarioOrganigrammaDTO(idNodoCorrente, idUtenteCorrente);
	}

	/**
	 * Crea l'assegnatario organigramma a partire dalle due stringhe rappresentanti il nodo.
	 *
	 * Verifica che l'idNodo non sia nullo o vuoto, se lo è ritorna un valore null.
	 * Non lancia eccezioni.
	 *
	 * @param idNodo
	 *  id dell'ufficio dell'utente.
	 * @param idUtente
	 *  id dell'utente
	 *
	 * @return
	 *  L'oggetto AssegnatarioOrganigrammaDTO con le informazioni dell'assegnatario.
	 */
	public static AssegnatarioOrganigrammaDTO createNullSafe(final String idNodo, final String idUtente) {
		AssegnatarioOrganigrammaDTO assegnatario = null;
		try {
			if (StringUtils.isNotBlank(idNodo)) {
				String assegnazione = idNodo;
				if (StringUtils.isNotBlank(idUtente)) {
					assegnazione = assegnazione + "," + idUtente;
				}
				assegnatario = create(assegnazione);
			}
		} catch (final NumberFormatException ne) {
			LOGGER.info("Tentativo di creare un assegnatarioOrganigramma con idNodo blank; idNodo: " + idNodo + " idUtente : " + idUtente);
			assegnatario = null;
		}
		return assegnatario;
	}
}
