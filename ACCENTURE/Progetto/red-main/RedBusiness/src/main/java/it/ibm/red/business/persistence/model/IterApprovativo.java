package it.ibm.red.business.persistence.model;

import java.io.Serializable;

/**
 * Model di un Iter Approvativo.
 */
public class IterApprovativo implements Serializable {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -1883428420565731996L;

	/**
	 * Iter approvativo.
	 */
	private int idIterApprovativo;
	
	/**
	 * Descrizione.
	 */
	private String descrizione;
	
	/**
	 * Aoo.
	 */
	private int idAoo;
	
	/**
	 * Tipo firma.
	 */
	private int tipoFirma;
	
	/**
	 * Parziale.
	 */
	private int parziale;
	
	/**
	 * Info.
	 */
	private String info;
	
	/**
	 * WF.
	 */
	private Integer workflow;
	
	/**
	 * Recupera l'id dell'iter approvativo.
	 * @return id dell'iter approvativo
	 */
	public int getIdIterApprovativo() {
		return idIterApprovativo;
	}

	/**
	 * Imposta l'id dell'iter approvativo.
	 * @param idIterApprovativo id dell'iter approvativo
	 */
	public void setIdIterApprovativo(final int idIterApprovativo) {
		this.idIterApprovativo = idIterApprovativo;
	}

	/**
	 * Recupera la descrizione.
	 * @return descrizione
	 */
	public String getDescrizione() {
		return descrizione;
	}

	/**
	 * Imposta la descrizione.
	 * @param descrizione
	 */
	public void setDescrizione(final String descrizione) {
		this.descrizione = descrizione;
	}

	/**
	 * Recupera l'id dell'Aoo.
	 * @return id dell'Aoo
	 */
	public int getIdAoo() {
		return idAoo;
	}
	
	/**
	 * Imposta l'id dell'Aoo.
	 * @param idAoo id dell'Aoo
	 */
	public void setIdAoo(final int idAoo) {
		this.idAoo = idAoo;
	}

	/**
	 * Recupera il tipo di firma.
	 * @return tipo di firma
	 */
	public int getTipoFirma() {
		return tipoFirma;
	}

	/**
	 * Imposta il tipo di firma.
	 * @param tipoFirma tipo di firma
	 */
	public void setTipoFirma(final int tipoFirma) {
		this.tipoFirma = tipoFirma;
	}

	/**
	 * Recupera il parziale.
	 * @return parziale.
	 */
	public int getParziale() {
		return parziale;
	}

	/**
	 * Imposta il parziale.
	 * @param parziale
	 */
	public void setParziale(final int parziale) {
		this.parziale = parziale;
	}

	/**
	 * Recupera le info.
	 * @return info
	 */
	public String getInfo() {
		return info;
	}

	/**
	 * Imposta le info.
	 * @param info
	 */
	public void setInfo(final String info) {
		this.info = info;
	}

	/**
	 * Recupera il workflow.
	 * @return workflow
	 */
	public Integer getWorkflow() {
		return workflow;
	}

	/**
	 * Imposta il workflow.
	 * @param workflow
	 */
	public void setWorkflow(final Integer workflow) {
		this.workflow = workflow;
	}
}