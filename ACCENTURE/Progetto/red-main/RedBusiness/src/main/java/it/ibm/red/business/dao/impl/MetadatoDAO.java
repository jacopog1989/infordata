package it.ibm.red.business.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import it.ibm.red.business.dao.AbstractDAO;
import it.ibm.red.business.dao.IMetadatoDAO;
import it.ibm.red.business.dto.MetadatoDTO;
import it.ibm.red.business.enums.LookupTablePresentationModeEnum;
import it.ibm.red.business.enums.StringMatchModeEnum;
import it.ibm.red.business.enums.TipoDocumentoModeEnum;
import it.ibm.red.business.enums.TipoMetadatoEnum;
import it.ibm.red.business.exception.RedException;

/**
 * @author SimoneLungarella
 * 
 *         DAO che gestisce la comunicazione col db, la tabella su cui si agisce
 *         con questo dao è METADATO. Ogni metadato viene salvato conservando un
 *         riferimento ad una tipologia di documento. Non viene mai effettuato
 *         il salvataggio di un metadato se non nella creazione di una tipologia
 *         documento
 */

@Repository
public class MetadatoDAO extends AbstractDAO implements IMetadatoDAO {

	private static final long serialVersionUID = -8521334901998463172L;

	/**
	 * Salva il metadato nella tabella METADATO
	 * 
	 * @param idTipologiaDocumento: identificativo univoco della tipologia di
	 *                              documento a cui fa riferimento il metadato
	 * @param metadato:             il dto del metadato da salvare in db
	 * @param connection
	 */
	@Override
	public int save(final MetadatoDTO metadato, final Connection connection) {
		final String queryGetId = "SELECT SEQ_METADATO.NEXTVAL FROM DUAL";
		int idMetadato = 0;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = connection.prepareStatement(queryGetId);
			rs = ps.executeQuery();
			if (rs.next()) {
				idMetadato = rs.getInt(1);
			}
		} catch (final Exception e) {
			throw new RedException("Errore durante la generazione dell'ID", e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}

		final String query = "INSERT INTO METADATO (IDMETADATO, NOMEMETADATO, TIPO, DIMENSIONE, DISPLAYNAME, FLAGRANGE, "
				+ "FLAGOUT, LOOKUPTABLESELECTOR, LOOKUPTABLEPRESENTATIONMODE,"
				+ "VISIBILITYMODE, EDITABILITYMODE, OBLIGATORINESSMODE, STRINGMATCHMODE) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			int index = 1;

			ps = connection.prepareStatement(query);
			ps.setInt(index++, idMetadato);
			ps.setString(index++, metadato.getName());
			ps.setString(index++, metadato.getType().getCode());

			if (metadato.getDimension() != null) {
				ps.setInt(index++, metadato.getDimension());
			} else {
				ps.setNull(index++, Types.INTEGER);
			}

			ps.setString(index++, metadato.getDisplayName());

			if (metadato.getFlagRange() != null) {
				ps.setInt(index++, Boolean.TRUE.equals(metadato.getFlagRange()) ? 1 : 0);
			} else {
				ps.setNull(index++, Types.INTEGER);
			}

			ps.setInt(index++, Boolean.TRUE.equals(metadato.getFlagOut()) ? 1 : 0);

			if (metadato.getLookupTableSelector() != null) {
				ps.setString(index++, metadato.getLookupTableSelector());
			} else {
				ps.setNull(index++, Types.VARCHAR);
			}

			if (metadato.getLookupTableMode() != null) {
				ps.setString(index++, metadato.getLookupTableMode().getCode());
			} else {
				ps.setNull(index++, Types.VARCHAR);
			}

			ps.setString(index++, metadato.getVisibility().getCode());
			ps.setString(index++, metadato.getEditability().getCode());
			ps.setString(index++, metadato.getObligatoriness().getCode());

			if (metadato.getStringMatchMode() != null) {
				ps.setString(index, metadato.getStringMatchMode().getCode());
			} else {
				ps.setNull(index++, Types.VARCHAR);
			}

			ps.executeQuery();
			return idMetadato;
		} catch (final Exception e) {
			throw new RedException("Errore durante l'inserimento del metadato", e);
		} finally {
			closeStatement(ps);
			closeResultset(rs);
		}

	}
	
	/**
	 * @see it.ibm.red.business.dao.IMetadatoDAO#caricaMetadati(int, int,
	 * java.sql.Connection)
	 */
	@Override
	public List<MetadatoDTO> caricaMetadati(final int idTipologiaDocumento, final int idTipoProcedimento, final Connection connection) {
		final List<MetadatoDTO> metadati = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			StringBuilder query = new StringBuilder()
			.append(" SELECT m.* ")
				.append(" , (SELECT COUNT(*) FROM VALUTA_METADATI vm ")
				.append(" WHERE vm.IDMETADATO_VALUTA = m.IDMETADATO ")
				.append(" OR vm.IDMETADATO_IMPORTO = m.IDMETADATO ")
				.append(" OR vm.IDMETADATO_IMPORTO_EURO = m.IDMETADATO) AS COUNTVALUTA ")
			.append(" FROM METADATO m, DOCUMENTOMETADATOPROCEDIMENTO mtdp ")
			.append(" WHERE m.IDMETADATO = mtdp.IDMETADATO AND mtdp.IDTIPOLOGIADOCUMENTO = ? AND mtdp.IDTIPOLOGIAPROCEDIMENTO = ? ")
			.append(" ORDER BY m.IDMETADATO ");
			
			ps = connection.prepareStatement(query.toString());
			ps.setInt(1, idTipologiaDocumento);
			ps.setInt(2, idTipoProcedimento);

			rs = ps.executeQuery();

			MetadatoDTO metadato = null;
			while (rs.next()) {
				metadato = populate(rs);

				metadati.add(metadato);
			}
		} catch (final Exception e) {
			throw new RedException("Errore durante il caricamento dei metadati", e);
		} finally {
			closeStatement(ps, rs);
		}

		return metadati;
	}

	/**
	 * Carica i metadati con flag OUT a <code> true </code> che fanno riferimento ad
	 * una coppia tipologia documento / tipologia procedimento.
	 * 
	 * @see it.ibm.red.business.dao.IMetadatoDAO#caricaMetadatiOut(int, int,
	 *      java.sql.Connection).
	 * @param idTipologiaDocumento
	 *            identificativo tipologia documento
	 * @param idTipoProcedimento
	 *            identificativo tipologia procedimento
	 * @param connection
	 *            connessione al database
	 * @return lista dei metadati OUT
	 */
	@Override
	public List<MetadatoDTO> caricaMetadatiOut(final int idTipologiaDocumento, final int idTipoProcedimento, final Connection connection) {
		final List<MetadatoDTO> metadati = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = connection.prepareStatement("SELECT m.* FROM METADATO m, DOCUMENTOMETADATOPROCEDIMENTO mtdp"
					+ " WHERE m.IDMETADATO = mtdp.IDMETADATO AND mtdp.IDTIPOLOGIADOCUMENTO = ? AND mtdp.IDTIPOLOGIAPROCEDIMENTO = ?" + " AND m.FLAGOUT = 1");
			ps.setInt(1, idTipologiaDocumento);
			ps.setInt(2, idTipoProcedimento);

			rs = ps.executeQuery();

			MetadatoDTO metadato = null;
			while (rs.next()) {
				metadato = populate(rs);

				metadati.add(metadato);
			}
		} catch (final Exception e) {
			throw new RedException("Errore durante il caricamento dei metadati con flag OUT", e);
		} finally {
			closeStatement(ps, rs);
		}

		return metadati;
	}

	/**
	 * @see it.ibm.red.business.dao.IMetadatoDAO#getMetadatoTipologiaDocRiferimento(int,
	 *      int, java.lang.String, java.sql.Connection).
	 */
	@Override
	public MetadatoDTO getMetadatoTipologiaDocRiferimento(final int idTipologiaDocumento, final int idTipoProcedimento, final String nomeMetadato,
			final Connection connection) {
		MetadatoDTO metadato = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = connection.prepareStatement("SELECT m.* FROM METADATO m, DOCUMENTOMETADATOPROCEDIMENTO mtdp"
					+ " WHERE m.IDMETADATO = mtdp.IDMETADATO AND mtdp.IDTIPOLOGIADOCUMENTO = ? AND mtdp.IDTIPOLOGIAPROCEDIMENTO = ?" + " AND m.NOMEMETADATO = ?");

			ps.setInt(1, idTipologiaDocumento);
			ps.setInt(2, idTipoProcedimento);
			ps.setString(3, nomeMetadato);

			rs = ps.executeQuery();

			if (rs.next()) {
				metadato = populate(rs);
			}
		} catch (final Exception e) {
			throw new RedException(
					"Errore durante il caricamento del metadato per la tipologia documento del documento in entrata di riferimento (allaccio principale) per l'uscita."
							+ " ID tipologia documento: " + idTipologiaDocumento + ", ID tipo procedimento: " + idTipoProcedimento + ", nome metadato: " + nomeMetadato,
					e);
		} finally {
			closeStatement(ps, rs);
		}

		return metadato;
	}

	
	private static MetadatoDTO populate(ResultSet rs) throws SQLException {
		final Integer id = rs.getInt("IDMETADATO");
		final String nome = rs.getString("NOMEMETADATO");
		final int dimensione = rs.getInt("DIMENSIONE");
		final String displayName = rs.getString("DISPLAYNAME");
		final String lookupTableSelector = rs.getString("LOOKUPTABLESELECTOR") == null ? "" : rs.getString("LOOKUPTABLESELECTOR");
		final TipoMetadatoEnum tipo = TipoMetadatoEnum.get(rs.getString("TIPO"));
		final LookupTablePresentationModeEnum lookupTableMode = LookupTablePresentationModeEnum.get(rs.getString("LOOKUPTABLEPRESENTATIONMODE"));
		final TipoDocumentoModeEnum visibility = TipoDocumentoModeEnum.get(rs.getString("VISIBILITYMODE"));
		final TipoDocumentoModeEnum editability = TipoDocumentoModeEnum.get(rs.getString("EDITABILITYMODE"));
		final TipoDocumentoModeEnum obligatoriness = TipoDocumentoModeEnum.get(rs.getString("OBLIGATORINESSMODE"));
		final StringMatchModeEnum stringMatchMode = StringMatchModeEnum.get(rs.getString("STRINGMATCHMODE"));
		final boolean flagRange = rs.getInt("FLAGRANGE") == 1;
		final boolean flagOut = rs.getInt("FLAGOUT") == 1;
		final boolean isMetadatoValuta = rs.getInt("COUNTVALUTA") > 0;
		
		return new MetadatoDTO(id, nome, tipo, displayName, lookupTableSelector, lookupTableMode, dimensione, visibility, editability, 
				obligatoriness, flagRange, flagOut, stringMatchMode, isMetadatoValuta);
	}

	/**
	 * @see it.ibm.red.business.dao.IMetadatoDAO#saveCrossDocumentoMetadatoProcedimento(java.sql.Connection,
	 *      int, java.lang.Long, int).
	 */
	@Override
	public void saveCrossDocumentoMetadatoProcedimento(final Connection connection, final int idDocumento, final Long idProcedimento, final int idMetadato) {
		// Salvataggio della relazione tra procedimento, documento e metadato in DB -
		// DOCUMENTOMETADATOPROCEDIMENTO
		PreparedStatement ps = null;
		int index = 1;

		final String queryCrossMetadato = "INSERT INTO DOCUMENTOMETADATOPROCEDIMENTO(IDTIPOLOGIADOCUMENTO, IDMETADATO, IDTIPOLOGIAPROCEDIMENTO) VALUES(?,?,?)";
		try {
			ps = connection.prepareStatement(queryCrossMetadato);
			ps.setInt(index++, idDocumento);
			ps.setInt(index++, idMetadato);
			ps.setLong(index++, idProcedimento);

			ps.executeUpdate();
		} catch (final Exception e) {
			throw new RedException("Errore durante il salvataggio della relazione tra tipologia documento, metadato e tipologia procedimento", e);
		} finally {
			closeStatement(ps);
		}
	}

}