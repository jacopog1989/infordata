package it.ibm.red.business.enums;

/**
 * The Enum PositionEnum.
 *
 * @author CPIERASC
 * 
 *         Enum posizione firma.
 */
public enum PositionEnum {
	
	/**
	 * Custom (definita da ascissa e ordinata).
	 */
	CUSTOM(0),
	
	/**
	 * Alto sinistra.
	 */
	TOP_LEFT(1),
	
	/**
	 * Alto centrale.
	 */
	TOP(2),
	
	/**
	 * Alto destra.
	 */
	TOP_RIGHT(3),
	
	/**
	 * Metà pagina sinistra.
	 */
	CENTRAL_LEFT(4),
	
	/**
	 * Metà pagina.
	 */
	CENTRAL(5),
	
	/**
	 * Metà pagina destra.
	 */
	CENTRAL_RIGHT(6),
	
	/**
	 * Basso sinistra.
	 */
	BOTTOM_LEFT(7),
	
	/**
	 * Basso centrale.
	 */
	BOTTOM(8),
	
	/**
	 * Basso destra.
	 */
	BOTTOM_RIGHT(9);
		
	/**
	 * Valore dell'enum.
	 */
	private Integer value;

	/**
	 * Costruttore.
	 * 
	 * @param inValue	valore
	 */
	PositionEnum(final Integer inValue) {
		this.value = inValue;
	}

	/**
	 * Getter valore.
	 * 
	 * @return	valore
	 */
	public Integer getValue() {
		return value;
	}

}
