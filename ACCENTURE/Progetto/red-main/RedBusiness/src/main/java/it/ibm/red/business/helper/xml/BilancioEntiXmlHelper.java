package it.ibm.red.business.helper.xml;

import java.util.List;

import it.ibm.red.business.exception.RedException;

/**
 * Helper xml Bilancio Enti.
 */
public class BilancioEntiXmlHelper extends AbstractXmlHelper {

	/**
	 * Costante serial version UID.
	 */
	private static final long serialVersionUID = -5760445382527705917L;

	/**
	 * Messaggio di errore.
	 */
	private static final String INVOKE_SETXML_MSG = "Please invoke setXmlDocument before!";

	/**
	 * Identificativo fascicolo FEPA.
	 */
	private static final String ID_FASCICOLO_FEPA = "IdFascicoloFepa";

	/**
	 * Casella PEC utente.
	 */
	private static final String CASELLA_PEC_ENTE = "CasellaPECEnte";
	
	/**
	 * Descriione ente.
	 */
	private static final String DESCRIZIONE_ENTE = "DescrizioneEnte";
	
	/**
	 * Identificativo documento.
	 */
	private static final String ID_DOCUMENTO_FEPA_LIST = "IdDocumentoFepa";

	/**
	 * Costruttore helper.
	 */
	public BilancioEntiXmlHelper() {
		super();
	}

	/**
	 * Restituisce l'id del fascicolo Fepa.
	 * @return id fascicolo fepa
	 */
	public String getIdFascicoloFepa() {
		if (getXmlDocument() == null) {
			throw new RedException(INVOKE_SETXML_MSG);
		}
		return getStringFromElements(ID_FASCICOLO_FEPA); 
	}

	/**
	 * Restituisce la casella PEC dell'ente.
	 * @return String che definisce la casella pec
	 */
	public String getCasellaPecEnte() {
		if (getXmlDocument() == null) {
			throw new RedException(INVOKE_SETXML_MSG);
		}
		return getStringFromElements(CASELLA_PEC_ENTE); 
	}

	/**
	 * Restituisce la descrizione dell'ente se l'xml document non è null.
	 * @return descrizioneEnte
	 */
	public String getDescrizioneEnte() {
		if (getXmlDocument() == null) {
			throw new RedException(INVOKE_SETXML_MSG);
		}
		return getStringFromElements(DESCRIZIONE_ENTE); 
	}

	/**
	 * Restituisce il codice ufficio dell'assegnatario.
	 * @return codice ufficio assegnatario
	 */
	public Integer getCodiceUfficioAssegnatario() {
		if (getXmlDocument() == null) {
			throw new RedException(INVOKE_SETXML_MSG);
		}
		return Integer.parseInt(getStringFromElements("CodiceUfficioAssegnatario")); 
	}

	/**
	 * Restituisce la lista di id documenti.
	 * 
	 * @return List di String che definisce tutti i documenti
	 */
	public List<String> getIdDocumenti() {
		if (getXmlDocument() == null) {
			throw new RedException(INVOKE_SETXML_MSG);
		}
		return getStringListFromElementsList(ID_DOCUMENTO_FEPA_LIST);
	}
	
	
}
