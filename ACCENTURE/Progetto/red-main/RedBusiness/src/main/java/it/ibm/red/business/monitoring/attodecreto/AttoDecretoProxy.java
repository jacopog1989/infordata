package it.ibm.red.business.monitoring.attodecreto;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Map;

import javax.xml.ws.BindingProvider;

import org.apache.commons.lang3.exception.ExceptionUtils;

import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembil.InterfacciaRaccoltaProvvisoriaDEMBIL;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.EsitoType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaAddDocumentoFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaChangeStatoFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaCreateFascicoloRaccoltaProvvisoriaType;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaGetFascicoloRaccoltaProvvisoria;
import internal.demdec.v1.it.gov.mef.servizi.interfacciadocumentaledembiltypes_1.RispostaModifyMetadataFascicoloRaccoltaProvvisoriaType;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;
import it.ibm.red.business.provider.ApplicationContextProvider;
import it.ibm.red.business.service.IAttoDecretoMonitoringService;

/**
 * Proxy Atto Decreto.
 */
public final class AttoDecretoProxy implements InvocationHandler {

	/**
	 * Logger.
	 */
	private static REDLogger logger = REDLogger.getLogger(AttoDecretoProxy.class);

	/**
	 * Label ATTO DECRETO - DOCUMENTO, occorre appendere l'id del documento al
	 * messaggio.
	 */
	private static final String ATTO_DECRETO_DOCUMENTO_LABEL = "[AttoDecreto][Documento ";

	/**
	 * Messaggio di errore modifica fascicolo.
	 */
	private static final String ERROR_MODIFICA_FASCICOLO = "Errore in fase di modifica stato fascicolo";

	/**
	 * Messaggio di successo modifica fascicolo.
	 */
	private static final String MODIFICA_FASICOLO_SUCCESS_MSG = "Stato fascicolo modificato con successo";

	/**
	 * Interfaccia DEMBIL.
	 */
	private final InterfacciaRaccoltaProvvisoriaDEMBIL obj;

	/**
	 * Info.
	 */
	private final AttoDecretoMonitoring beanInfo;

	/**
	 * Service.
	 */
	private final IAttoDecretoMonitoringService service;
	
	/**
	 * Costruttore.
	 * @param obj
	 * @param beanInfo
	 */
	private AttoDecretoProxy(final InterfacciaRaccoltaProvvisoriaDEMBIL obj, final AttoDecretoMonitoring beanInfo) {
		this.obj = obj;
		this.beanInfo = beanInfo;
		this.service = ApplicationContextProvider.getApplicationContext().getBean(IAttoDecretoMonitoringService.class);
	}

	/**
	 * Crea una nuova instanza di InterfacciaRaccoltaProvvisoriaDEMBIL e la
	 * restituisce al chiamante.
	 * 
	 * @param obj
	 * @param beanInfo
	 * @return un'istanza di InterfacciaRaccoltaProvvisoriaDEMBIL
	 */
	public static InterfacciaRaccoltaProvvisoriaDEMBIL newInstance(final InterfacciaRaccoltaProvvisoriaDEMBIL obj, final AttoDecretoMonitoring beanInfo) {
		return (InterfacciaRaccoltaProvvisoriaDEMBIL) java.lang.reflect.Proxy.newProxyInstance(obj.getClass().getClassLoader(), obj.getClass().getInterfaces(),
				new AttoDecretoProxy(obj, beanInfo));
	}

	@Override
	public Object invoke(final Object proxy, final Method m, final Object[] args) throws Throwable {
		final String logRadix = "[AttoDecretoMonitoring]" + beanInfo.getLogString();
		Object result = null;
		boolean esitoInserimento = false;
		Throwable targetException = null;
		try {

			// insert in tabella monitoring
			beanInfo.setOperazione(m.getName());
			esitoInserimento = service.insertOperation(beanInfo);

			result = m.invoke(obj, args);

		} catch (final InvocationTargetException e) {
			logger.warn(e);
			targetException = e.getTargetException();
			throw targetException;
		} catch (final Exception e) {
			targetException = new RedException("Errore in fase di invocazione del metodo", e);
			throw targetException;
		} finally {

			try {
				// et request e response
				final Map<String, Object> responseCtx = ((BindingProvider) obj).getResponseContext();

				final String request = (String) responseCtx.get(AttoDecretoWSClientHandler.SOAP_ENVELOPE_REQUEST);
				final String response = (String) responseCtx.get(AttoDecretoWSClientHandler.SOAP_ENVELOPE_RESPONSE);
				logger.info(logRadix + "[" + m.getName() + "][Request] " + request);
				logger.info(logRadix + "[" + m.getName() + "][Response] " + response);

				if (esitoInserimento) {
					populateEsito(result, request, response, targetException);
					// monitoring => update esito
					service.updateOperation(beanInfo);
				}
			} catch (final Exception e) {
				logger.warn(logRadix + "[" + m.getName() + "] errore in fase di aggiornamento delle info di monitoraggio", e);
			}
		}
		return result;
	}

	private void populateEsito(final Object result, final String request, final String response, final Throwable targetException) {
		// popola esito ed eventualmente request e response
		if (targetException != null) {

			beanInfo.setEsito(-1);
			beanInfo.setDescrizioneEsito("Sollevata eccezione");
			beanInfo.setStackTrace(ExceptionUtils.getStackTrace(targetException));

		} else {

			final AttoDecretoParsedResponse parsedResponse = parseResponse(result, beanInfo.getIdDocumento());

			if (parsedResponse == null) {
				throw new RedException("Oggetto AttoDecretoParsedResponse non inizializzato prima della chiamata ad un metodo della classe.");
			}

			beanInfo.setEsito(parsedResponse.getEsito());
			beanInfo.setDescrizioneEsito(parsedResponse.getDescrizioneEsito());
			if (beanInfo.getEsito() < 0) {
				beanInfo.setRequest(request);
				beanInfo.setResponse(response);
			} else {
				if (parsedResponse.getIdRaccoltaProvvisoria() != null) {
					beanInfo.setIdRaccoltaProvvisoria(parsedResponse.getIdRaccoltaProvvisoria());
				}
			}

		}

	}

	/**
	 * Esegue l'attività di parsing della response, comunica in console eventuali
	 * malformazioni.
	 * 
	 * @param result
	 * @param idDocumento
	 * @return AttoDecretoParsedResponse, definisce esito del parsing, descrizione
	 *         dell'esito e id della raccolta provvisoria.
	 */
	public static AttoDecretoParsedResponse parseResponse(final Object result, final Integer idDocumento) {
		Integer esito = 0;
		String descrEsito = null;
		String idRaccoltaProvvisoria = null;
		// parsing result
		if (result != null) {
			if (result instanceof RispostaCreateFascicoloRaccoltaProvvisoriaType) {

				final RispostaCreateFascicoloRaccoltaProvvisoriaType rispostaType = (RispostaCreateFascicoloRaccoltaProvvisoriaType) result;

				if (rispostaType.getDettaglioFascicolo() != null && rispostaType.getEsito().equals(EsitoType.OK)) {
					idRaccoltaProvvisoria = rispostaType.getDettaglioFascicolo().getIdFascicoloRaccoltaProvvisoria();
					esito = 1;
					descrEsito = "Raccolta creata correttamente";
					logger.info(ATTO_DECRETO_DOCUMENTO_LABEL + idDocumento + "] creata raccolta " + idRaccoltaProvvisoria);
				} else {
					if (rispostaType.getEsito() != null) {
						if (!rispostaType.getErrorList().isEmpty() && rispostaType.getErrorList().get(0).getErrorCode() == 1057) {
							descrEsito = rispostaType.getErrorList().get(0).getErrorMessageString();
							// Si estrae l'idRaccoltaFAD gia' presente, restituito alla fine della stringa
							// del messsaggio di errore
							// idRaccoltaFAD e' un UUID (32 caratteri + 4 separatori) -> 36 caratteri
							idRaccoltaProvvisoria = descrEsito.substring(descrEsito.length() - 36, descrEsito.length());
							esito = -1;

							logger.info(ATTO_DECRETO_DOCUMENTO_LABEL + idDocumento + "] creata raccolta " + idRaccoltaProvvisoria);
						} else {

							esito = -1;
							descrEsito = "Errore in fase di creazione della raccolta";
							if (rispostaType.getErrorList() != null && !rispostaType.getErrorList().isEmpty()) {
								descrEsito = descrEsito + ": " + rispostaType.getErrorList().get(0).getErrorMessageString();
							}

							logger.warn(ATTO_DECRETO_DOCUMENTO_LABEL + idDocumento + "]" + descrEsito);
						}
					}
				}

			} else if (result instanceof RispostaAddDocumentoFascicoloRaccoltaProvvisoriaType) {
				final RispostaAddDocumentoFascicoloRaccoltaProvvisoriaType rispostaType = (RispostaAddDocumentoFascicoloRaccoltaProvvisoriaType) result;

				if (rispostaType.getEsito().equals(EsitoType.OK)) {
					esito = 1;
					descrEsito = "Documento inviato con successo";
					logger.info(ATTO_DECRETO_DOCUMENTO_LABEL + idDocumento + "] documento inviato con successo ");
				} else {
					esito = -1;
					descrEsito = "Errore in fase di invio del documento";
					if (rispostaType.getErrorList() != null && !rispostaType.getErrorList().isEmpty()) {
						descrEsito = descrEsito + ": " + rispostaType.getErrorList().get(0).getErrorMessageString();
					}

					logger.warn(ATTO_DECRETO_DOCUMENTO_LABEL + idDocumento + "]" + descrEsito);
				}
			} else if (result instanceof RispostaChangeStatoFascicoloRaccoltaProvvisoriaType) {

				final RispostaChangeStatoFascicoloRaccoltaProvvisoriaType rispostaType = (RispostaChangeStatoFascicoloRaccoltaProvvisoriaType) result;
				if (rispostaType.getEsito().equals(EsitoType.OK)) {
					esito = 1;
					descrEsito = MODIFICA_FASICOLO_SUCCESS_MSG;
					logger.info(ATTO_DECRETO_DOCUMENTO_LABEL + idDocumento + "] " + MODIFICA_FASICOLO_SUCCESS_MSG);
				} else {
					esito = -1;
					descrEsito = ERROR_MODIFICA_FASCICOLO;
					if (rispostaType.getErrorList() != null && !rispostaType.getErrorList().isEmpty()) {
						descrEsito = descrEsito + ": " + rispostaType.getErrorList().get(0).getErrorMessageString();
					}

					logger.warn(ATTO_DECRETO_DOCUMENTO_LABEL + idDocumento + "]" + descrEsito);
				}

			} else if (result instanceof RispostaGetFascicoloRaccoltaProvvisoria) {

				final RispostaGetFascicoloRaccoltaProvvisoria rispostaType = (RispostaGetFascicoloRaccoltaProvvisoria) result;

				if (rispostaType.getEsito().equals(EsitoType.OK)) {
					esito = 1;
					descrEsito = MODIFICA_FASICOLO_SUCCESS_MSG;
					logger.info(ATTO_DECRETO_DOCUMENTO_LABEL + idDocumento + "] " + MODIFICA_FASICOLO_SUCCESS_MSG);
				} else {
					esito = -1;
					descrEsito = ERROR_MODIFICA_FASCICOLO;
					if (rispostaType.getErrorList() != null && !rispostaType.getErrorList().isEmpty()) {
						descrEsito = descrEsito + ": " + rispostaType.getErrorList().get(0).getErrorMessageString();
					}

					logger.warn(ATTO_DECRETO_DOCUMENTO_LABEL + idDocumento + "]" + descrEsito);
				}

			} else if (result instanceof RispostaModifyMetadataFascicoloRaccoltaProvvisoriaType) {

				final RispostaModifyMetadataFascicoloRaccoltaProvvisoriaType rispostaType = (RispostaModifyMetadataFascicoloRaccoltaProvvisoriaType) result;
				if (rispostaType.getEsito().equals(EsitoType.OK)) {
					esito = 1;
					descrEsito = MODIFICA_FASICOLO_SUCCESS_MSG;
					logger.info(ATTO_DECRETO_DOCUMENTO_LABEL + idDocumento + "] " + MODIFICA_FASICOLO_SUCCESS_MSG);
				} else {
					esito = -1;
					descrEsito = ERROR_MODIFICA_FASCICOLO;
					if (rispostaType.getErrorList() != null && !rispostaType.getErrorList().isEmpty()) {
						descrEsito = descrEsito + ": " + rispostaType.getErrorList().get(0).getErrorMessageString();
					}
					logger.warn(ATTO_DECRETO_DOCUMENTO_LABEL + idDocumento + "]" + descrEsito);
				}
			}
			return new AttoDecretoParsedResponse(esito, descrEsito, idRaccoltaProvvisoria);
		} else {
			return null;
		}
	}
}
