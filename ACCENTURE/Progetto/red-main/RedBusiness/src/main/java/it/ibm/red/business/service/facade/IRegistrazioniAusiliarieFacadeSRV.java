package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.dto.MetadatoDTO;

/**
 * Facade del servizio di gestioneregistrazioni ausiliarie.
 * 
 * @author SimoneLungarella
 */
public interface IRegistrazioniAusiliarieFacadeSRV extends Serializable {

	/**
	 * Aggiorna i metadati ed il flag DOCUMENTO_DEFINITIVO associato ad una
	 * registrazione ausiliaria.
	 * 
	 * @param documentTitle
	 *            identificativo del documento in uscita
	 * @param idAoo
	 *            identificativo dell'area organizzativa
	 * @param metadati
	 *            metadati del documento in uscita
	 * @param documentoDefinitivo
	 *            se {@code true} indica che il documento deve essere impostato come
	 *            non modificabile in quanto il content è stato aggiornato con la
	 *            sua versione finale
	 */
	void updateDatiRegistrazioneAusiliaria(String documentTitle, int idAoo, Collection<MetadatoDTO> metadati,
			boolean documentoDefinitivo);

}
