package it.ibm.red.business.service;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.AllegatoDTO;
import it.ibm.red.business.dto.DestinatarioRedDTO;
import it.ibm.red.business.dto.EsitoSalvaDocumentoDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.RispostaAllaccioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.enums.MomentoProtocollazioneEnum;
import it.ibm.red.business.enums.TipoAssegnazioneEnum;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;
import it.ibm.red.business.persistence.model.Aoo;

/**
 * Servizio per la creazione di documenti RED in uscita.
 * 
 * @author m.crescentini
 *
 */
public interface ICreaDocumentoRedUscitaSRV extends Serializable {

	/**
	 * Crea un nuovo documento in uscita in risposta ad un documento in entrata.
	 * L'ID del documento in entrata e i dati del template da cui generare il
	 * content sono passati in input.
	 * 
	 * @param idDocumentoEntrata
	 * @param utenteProtocollatore
	 *            Utente protocollatore del documento in entrata e creatore del
	 *            documento in uscita
	 * @param indiceClassificazione
	 * @param guidTemplate
	 * @param nomeTemplate
	 * @param oggettoTemplate
	 * @param testoTemplate
	 * @param destinatari
	 * @param casella
	 * @param mittenteMail
	 * @param oggetto
	 * @param idUtenteAssegnatario
	 *            ID utente dell'assegnatario
	 * @param idRuoloAssegnatario
	 *            ID ruolo dell'assegnatario
	 * @param idUfficioAssegnatario
	 *            ID ufficio dell'assegnatario
	 * @param aoo
	 * @param fceh
	 * @param con
	 * @return
	 */
	EsitoSalvaDocumentoDTO creaDocumentoRedUscitaRispostaDaTemplate(String idDocumentoEntrata, UtenteDTO utenteProtocollatore, String indiceClassificazione, 
			String guidTemplate, String nomeTemplate, String oggettoTemplate, String testoTemplate, List<DestinatarioRedDTO> destinatari, 
			String casella, String mittenteMail, String oggetto, Long idUtenteAssegnatario, Long idRuoloAssegnatario, Long idUfficioAssegnatario, Aoo aoo, 
			IFilenetCEHelper fceh, Connection con);
	
	
	/**
	 * Crea un nuovo documento in uscita inserendolo in un nuovo fascicolo. L'indice
	 * di classificazione del nuovo fascicolo e il content del documento sono
	 * passati in input.
	 * 
	 * @param content
	 * @param contentType
	 * @param nomeFile
	 * @param indiceClassificazione
	 * @param utenteCreatore
	 * @param idIterApprovativo
	 * @param idTipologiaDocumento
	 * @param idTipoProcedimento
	 * @param tipoAssegnazione
	 * @param momentoProtocollazione
	 * @param allegati
	 * @param allacci
	 * @param contattiDestinatari
	 * @param oggetto
	 * @param oggettoMail
	 * @param testoMail
	 * @param casellaMittenteMail
	 * @param mittenteMail
	 * @param idUtenteAssegnatario
	 *            ID utente dell'assegnatario
	 * @param idUfficioAssegnatario
	 *            ID ufficio dell'assegnatario
	 * @param codiceFlusso
	 *            Codice del flusso esterno per il quale si sta creando il documento
	 * @param aoo
	 * @param con
	 * @return
	 */
	EsitoSalvaDocumentoDTO creaDocumentoRedUscitaDaContent(byte[] content, String contentType, String nomeFile, UtenteDTO utenteCreatore, 
			FascicoloDTO fascicoloProcedimentale, String indiceClassificazione, Integer idTipologiaDocumento, Integer idTipoProcedimento, Integer idIterApprovativo, 
			TipoAssegnazioneEnum tipoAssegnazione, MomentoProtocollazioneEnum momentoProtocollazione, List<AllegatoDTO> allegati, List<RispostaAllaccioDTO> allacci, 
			List<DestinatarioRedDTO> contattiDestinatari, String oggetto, String oggettoMail, String testoMail, String casellaMittenteMail, String mittenteMail, 
			Long idUtenteAssegnatario, Long idUfficioAssegnatario, String codiceFlusso, Aoo aoo, Connection con);
	
}