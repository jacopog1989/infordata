package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import it.ibm.red.business.dto.ApprovazioneDTO;
import it.ibm.red.business.dto.FilenetCredentialsDTO;
import it.ibm.red.business.dto.filenet.StepDTO;
import it.ibm.red.business.dto.filenet.StoricoDTO;

/**
 * The Interface IStoricoFacadeSRV.
 *
 * @author CPIERASC
 * 
 *         Facade servizio gestione storico.
 */
public interface IStoricoFacadeSRV extends Serializable {

	/**
	 * Metodo per il recupero dello storico.
	 * 
	 * @param idDocumento identificativo documento
	 * @param idAoo       identificativo aoo
	 * @return storico
	 */
	Collection<StoricoDTO> getStorico(int idDocumento, Integer idAoo);

	/**
	 * Recupera il modello dati per la rappresentazione visuale dello storico.
	 * 
	 * @param dto           credenziali utente
	 * @param wobNumber     wob number workflow
	 * @param documentTitle document title documento
	 * @param idAoo         identificativo aoo
	 * @return modello dati storico visuale
	 */
	List<StepDTO> getModelloStoricoVisuale(FilenetCredentialsDTO dto, String wobNumber, String documentTitle, Long idAoo, Date date);

	/**
	 * Recupero modello approvazioni visuale.
	 * 
	 * @param idDocumento identificativo documento
	 * @param idAoo       identificativo aoo
	 * @return lista approvazioni
	 */
	List<ApprovazioneDTO> getModelloApprovazioniVisuale(Integer idDocumento, Integer idAoo);

	/**
	 * Ottiene il modello storico visuale.
	 * 
	 * @param dto
	 * @param wobNumber
	 * @param documentTitle
	 * @param idAoo
	 * @param dataCreazione
	 * @param isRicercaRiservato
	 * @return lista di step
	 */
	List<StepDTO> getModelloStoricoVisuale(FilenetCredentialsDTO dto, String wobNumber, String documentTitle, Long idAoo, Date dataCreazione, boolean isRicercaRiservato);

}
