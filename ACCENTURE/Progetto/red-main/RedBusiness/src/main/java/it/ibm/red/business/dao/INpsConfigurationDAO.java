package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.List;

import it.ibm.red.business.dto.TipologiaDTO;
import it.ibm.red.business.persistence.model.NpsAzioniConfig;
import it.ibm.red.business.persistence.model.NpsConfiguration;

/**
 * 
 * @author CPIERASC
 *
 *	Interfaccia dao gestione configurazione NPS.
 */
public interface INpsConfigurationDAO extends Serializable {
	
	/**
	 * Recupero configurazione per id aoo.
	 * 
	 * @param idAoo			id aoo
	 * @param connection	connessione al db
	 * @return				configurazione
	 */
	NpsConfiguration getByIdAoo(int idAoo, Connection connection);
	
	/**
	 * Recupero configurazione per codice aoo.
	 * 
	 * @param codiceAoo		codice aoo
	 * @param connection	connessione al db
	 * @return				configurazione
	 */
	NpsConfiguration getByCodiceAoo(String codiceAoo, Connection connection);
	
	/**
	 * Recupero configurazione per codice aoo.
	 * 
	 * @param idAoo
	 * @param methodName		
	 * @param className		
	 * @param connection	
	 * @return				
	 */
	NpsAzioniConfig getAzioneConfig(Integer idAoo, String methodName, String className, Connection connection);

	/**
	 * Recupera la coppia tipo documento/tipo procedimento a fronte di una tipologia documentale su sistemi esterni (NPS ad esempio)
	 * 
	 * @param isIngresso
	 * @param idAoo
	 * @param tipologia
	 * @param con
	 * @return
	 */
	TipologiaDTO getTipologia(Boolean isIngresso, Integer idAoo, String tipologiaNPS, Connection con);
	
	/**
	 * Recupera la tipologia documentale su sistemi esterni (NPS ad esempio) a fronte di una coppia tipo documento/tipo procedimento
	 * 
	 * @param idAoo
	 * @param tipologia
	 * @param con
	 * @return
	 */
	String getTipologia(Integer idAoo, TipologiaDTO tipologia, Connection con);

	/**
	 * Ottiene tutte le configurazioni Nps.
	 * @param con
	 * @return lista delle configurazioni Nps
	 */
	List<NpsConfiguration> getAll(Connection con);
}
