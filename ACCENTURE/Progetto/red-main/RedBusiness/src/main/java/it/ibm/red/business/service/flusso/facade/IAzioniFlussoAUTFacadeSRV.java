package it.ibm.red.business.service.flusso.facade;

import it.ibm.red.business.service.flusso.IAzioniFlussoBaseSRV;

/**
 * Interfaccia del servizio azioni flusso AUT.
 */
public interface IAzioniFlussoAUTFacadeSRV extends IAzioniFlussoBaseSRV {

}
