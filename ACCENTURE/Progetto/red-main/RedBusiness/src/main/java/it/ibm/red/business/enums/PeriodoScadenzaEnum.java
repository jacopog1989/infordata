package it.ibm.red.business.enums;

import java.util.Calendar;
import java.util.Date;

/**
 * Periodo in cui sono suddivise le scadenze dei documenti scadenzati
 * 
 * Le scadenze sono considerate sempre con scadenza uguale minore o ugual maggiore.
 *
 */
public enum PeriodoScadenzaEnum {
	
	/**
	 * Valore.
	 */
	MINORE_5,
	
	/**
	 * Valore.
	 */
	MINORE_10,
	
	/**
	 * Valore.
	 */
	MAGGIORE_11,
	
	/**
	 * Valore.
	 */
	SCADUTO;
	
	
	/**
	 * Costante 5 giorni.
	 */
	private static final Long FIVE_DAYS = 5L * 24L * 60L * 60L;  
	
	/**
	 * Restituisce l'enum associata alla data di scadenza.
	 * @param dataScadenza
	 * @return enum associata alla data di scadenza
	 */
	public static PeriodoScadenzaEnum get(final Date dataScadenza) {
		long now = Calendar.getInstance().getTimeInMillis();
		long expiring = dataScadenza.getTime();
		long elapsedTime = expiring - now;
		
		elapsedTime /= 1000L;
		
		if (elapsedTime < 0) {
			return SCADUTO;
		} else if (2 * FIVE_DAYS < elapsedTime) {
			return MAGGIORE_11;
		} else if (FIVE_DAYS < elapsedTime) {
			return MINORE_10;
		} else {
			return MINORE_5;
		}
		
	}
}
