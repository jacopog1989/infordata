package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import it.ibm.red.business.dto.AssegnatarioOrganigrammaDTO;
import it.ibm.red.business.dto.ContributoDTO;
import it.ibm.red.business.dto.DetailDocumentRedDTO;
import it.ibm.red.business.dto.EsitoOperazioneDTO;
import it.ibm.red.business.dto.FascicoloDTO;
import it.ibm.red.business.dto.FileDTO;
import it.ibm.red.business.dto.TitolarioDTO;
import it.ibm.red.business.dto.UtenteDTO;
import it.ibm.red.business.helper.filenet.ce.IFilenetCEHelper;

/**
 * The Interface IContributoFacadeSRV.
 *
 * @author CPIERASC
 * 
 *         Facade lookup service.
 */
public interface IContributoFacadeSRV extends Serializable {

	/**
	 * Recupera i contributi. Attenzione se non è presente il file (guid null e
	 * nomeFile null) il content viene visualizzato con il documento di default
	 * 
	 * @param fceh
	 * @param documentTitle
	 * @return
	 */
	List<ContributoDTO> getContributi(UtenteDTO utente, String documentTitle);

	/**
	 * Recupero contributi per id documento. Attenzione se non è presente il file
	 * (guid null e nomeFile null) il content viene visualizzato con il documento di
	 * default
	 * 
	 * @param fceh          helper filenet ce
	 * @param documentTitle identificativo documento
	 * @return aoo
	 */
	List<ContributoDTO> getContributi(IFilenetCEHelper fceh, String documentTitle, Long idAoo);

	/**
	 * Recupero contenuto contributo (nel caso in cui fosse presente solamente la
	 * nota verrebbe creato il pdf che la contiene on the fly).
	 * 
	 *
	 * @param utente      utente che esegue l'operazione
	 * @param flagEsterno flag che indica se il contributo è esterno o meno
	 * @param id          identificativo del contributo
	 * @return il contenuto del contributo
	 */
	FileDTO getContent(UtenteDTO utente, Boolean flagEsterno, Long id);

	/**
	 * Inserisce il contributo.
	 * 
	 * @param utente
	 * @param wobNumber
	 * @param contributo
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO inserisciContributo(UtenteDTO utente, String wobNumber, ContributoDTO contributo);

	/**
	 * Inserisce il contributo.
	 * 
	 * @param utente
	 * @param wobNumber
	 * @param contributo
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO inserisciContributo2(UtenteDTO utente, String wobNumber, ContributoDTO contributo);

	/**
	 * Valida i contributi.
	 * 
	 * @param utente
	 * @param wobNumber
	 * @param contributo
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO validaContributi(UtenteDTO utente, String wobNumber, ContributoDTO contributo);

	/**
	 * Richiede un contributo.
	 * 
	 * @param utente
	 * @param wobNumber
	 * @param inputAssegnazioniContributo
	 * @param motivazioneAssegnazione
	 * @param isRichiestaUrgente
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO richiediContributo(UtenteDTO utente, String wobNumber, List<AssegnatarioOrganigrammaDTO> inputAssegnazioniContributo, String motivazioneAssegnazione,
			Boolean isRichiestaUrgente);

	/**
	 * Richiede un contributo interno.
	 * 
	 * @param utente
	 * @param wobNumber
	 * @param inputAssegnazioniContributo
	 * @param motivazioneAssegnazione
	 * @param isRichiestaUrgente
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO richiediContributoInterno(UtenteDTO utente, String wobNumber, List<AssegnatarioOrganigrammaDTO> inputAssegnazioniContributo,
			String motivazioneAssegnazione, Boolean isRichiestaUrgente);

	/**
	 * Richiede un contributo utente.
	 * 
	 * @param utente
	 * @param wobNumber
	 * @param inputAssegnazioniContributo
	 * @param motivazioneAssegnazione
	 * @param isRichiestaUrgente
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO richiediContributoUtente(UtenteDTO utente, String wobNumber, List<AssegnatarioOrganigrammaDTO> inputAssegnazioniContributo,
			String motivazioneAssegnazione, Boolean isRichiestaUrgente);

	/**
	 * Richiede un contributo.
	 * 
	 * @param utente
	 * @param wobNumber
	 * @param inputAssegnazioniContributo
	 * @param motivazioneAssegnazione
	 * @param isRichiestaUrgente
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO richiestaContributo(UtenteDTO utente, String wobNumber, List<AssegnatarioOrganigrammaDTO> inputAssegnazioniContributo, String motivazioneAssegnazione,
			Boolean isRichiestaUrgente);

	/**
	 * Esegue la response "validazione Contributo".
	 * 
	 * Esegue anche un cambiamento di stato nel db per il contributo
	 * 
	 * @param utente                mittente dell'operazione
	 * @param wobNumbers            wobnumber del workflow da avanzare
	 * @param motivoAssegnazioneNew descrizione data dall'utente delle motivazioni
	 *                              di esecuzione dell'operazione
	 * @return Esito dell'operazione, qualora non sia stato possibile recuperare il
	 *         numeroDocumento ritorna il wobvNumber
	 */
	EsitoOperazioneDTO validazioneContributo(UtenteDTO utente, String wobNumbers, String motivoAssegnazioneNew);

	/**
	 * Elimina il contributo interno.
	 * 
	 * @param idContributo
	 * @param idUtenteContributo
	 * @param idDocumento
	 * @param utente
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO eliminaContributoInterno(Long idContributo, Long idUtenteContributo, Integer idDocumento, UtenteDTO utente);

	/**
	 * Elimina il contributo esterno
	 * 
	 * @param idContributo
	 * @param guidContributo
	 * @param idDocumento
	 * @param utente
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO eliminaContributoEsterno(Long idContributo, String guidContributo, Integer idDocumento, UtenteDTO utente);

	/**
	 * Aggiorna il contributo.
	 * 
	 * @param contributo
	 * @param utente
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO aggiornaContributo(ContributoDTO contributo, UtenteDTO utente);

	/**
	 * Ottiene i contributi attivi relativi al documento.
	 * 
	 * @param numeroDocumento
	 * @param annoDocumento
	 * @param utente
	 * @return dettaglio dei documenti
	 */
	Collection<DetailDocumentRedDTO> getContributiAttivi(Integer numeroDocumento, Integer annoDocumento, UtenteDTO utente);

	/**
	 * Collega il contributo
	 * 
	 * @param wobNumber
	 * @param documentTitle
	 * @param fascicoloSelezionato
	 * @param titolarioSelezionato
	 * @param utente
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO collegaContributo(String wobNumber, String documentTitle, FascicoloDTO fascicoloSelezionato, TitolarioDTO titolarioSelezionato, UtenteDTO utente);

	/**
	 * Elimina i contributi agganciati al documento.
	 * 
	 * @param documentTitle
	 * @param protocollo
	 * @param utente
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO eliminaContributiEsterniByProtocollo(String documentTitle, String protocollo, UtenteDTO utente);

	/**
	 * Inserisce il contributo esterno.
	 * 
	 * @param documentTitle
	 * @param contributo
	 * @param idFascicolo
	 * @param utente
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO inserisciContributoEsternoManuale(String documentTitle, ContributoDTO contributo, String idFascicolo, UtenteDTO utente);

	/**
	 * Generazione PDF nota on the fly.
	 * 
	 * @param utente      credenziali utente
	 * @param flagEsterno a true indica che il contributo è esterno
	 * @param id          identificativo contributo
	 * @return file
	 */
	FileDTO getPDFNotaOFT(UtenteDTO utente, Boolean flagEsterno, Long id);

	/**
	 * Richiede il contributo.
	 * 
	 * @param utente
	 * @param wobNumber
	 * @param inputAssegnazioniContributo
	 * @param motivazioneAssegnazione
	 * @param isRichiestaUrgente
	 * @param annoProtocollo
	 * @param numProtocollo
	 * @return esito dell'operazione
	 */
	EsitoOperazioneDTO richiediContributo(UtenteDTO utente, String wobNumber, List<AssegnatarioOrganigrammaDTO> inputAssegnazioniContributo, String motivazioneAssegnazione,
			Boolean isRichiestaUrgente, Integer annoProtocollo, Integer numProtocollo);
}