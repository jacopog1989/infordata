package it.ibm.red.business.dto.filenet;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import it.ibm.red.business.dto.AbstractDTO;

/**
 * The Class StoricoDTO.
 *
 * @author CPIERASC
 * 
 *         DTO per gestire lo storico operazioni di un documento.
 */
public class StoricoDTO extends AbstractDTO implements Comparable<StoricoDTO> {
	
	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -7531233098676889343L;

	/**
	 * Data operazione.
	 */
	private final Date dataOperazione;

	/**
	 * Motivo assegnazione.
	 */
	private final String motivoAssegnazione;

	/**
	 * Tipo operazione.
	 */
	private final String tipoOperazione;

	/**
	 * Ufficio assegnante.
	 */
	private final String ufficioAssegnante;

	/**
	 * Ufficio assegnatario.
	 */
	private final String ufficioAssegnatario;

	/**
	 * Utente assegnante.
	 */
	private final String utenteAssegnante;

	/**
	 * Utente assegnatario.
	 */
	private final String utenteAssegnatario;
	
	/**
	 * ID tipo operazione.
	 */
	private final Integer idTipoOperazione;
	
	/**
	 * L'identificativo può essere composto dal numero del documento 
	 * o anche da una composizione di informazioni tipo: numero protocollo/anno - numero doc
	 * (utilizzato nello storico dei fascicoli)
	 */
	private String idDocumento;
	
	/**
	 *  Nodo assegnatario.
	 */
	private final Long idNodoAssegnatario;

	/**
	 * Utente assegnatario.
	 */
	private final Long idUtenteAssegnatario;

	/**
	 * Nodo assegnante.
	 */
	private final Long idNodoAssegnante;

	/**
	 * Utente assegnante.
	 */
	private final Long idUtenteAssegnante;

	/**
	 * Attività.
	 */
	private transient InputStream attivita;

	/**
	 * Numero wf.
	 */
	private String workFlowNumber;

	/**
	 * Wf padre.
	 */
	private String workFlowNumberPadre;
	
	/**
	 * Costruttore.
	 * 
	 * @param inDataOperazione
	 *            data operazione
	 * @param inTipoOperazione
	 *            tipo operazione
	 * @param inUfficioAssegnante
	 *            ufficio assegnante
	 * @param inUtenteAssegnante
	 *            utente assegnante
	 * @param inUfficioAssegnatario
	 *            ufficio assegnatario
	 * @param inUtenteAssegnatario
	 *            utente assegnatario
	 * @param inMotivoAssegnazione
	 *            motivo assegnazione
	 */
	public StoricoDTO(final Date inDataOperazione, final String inTipoOperazione, final Integer inIdTipoOperazione, final String inUfficioAssegnante, 
			final String inUtenteAssegnante, final String inUfficioAssegnatario, final String inUtenteAssegnatario, final String inMotivoAssegnazione,
			final Long inIdNodoAssegnante, final Long inIdUtenteAssegnante, final Long inIdNodoAssegnatario, final Long inIdUtenteAssegnatario,
			final String inWorkFlowNumber, final String inWorkFlowNumberPadre) {
		super();
		this.dataOperazione = inDataOperazione;
		this.tipoOperazione = inTipoOperazione;
		this.idTipoOperazione = inIdTipoOperazione;
		this.ufficioAssegnante = inUfficioAssegnante;
		this.utenteAssegnante = inUtenteAssegnante;
		this.ufficioAssegnatario = inUfficioAssegnatario;
		this.utenteAssegnatario = inUtenteAssegnatario;
		this.motivoAssegnazione = inMotivoAssegnazione;
		this.idNodoAssegnante = inIdNodoAssegnante;
		this.idUtenteAssegnante = inIdUtenteAssegnante;
		this.idNodoAssegnatario = inIdNodoAssegnatario;
		this.idUtenteAssegnatario = inIdUtenteAssegnatario;
		this.workFlowNumber = inWorkFlowNumber;
		this.workFlowNumberPadre = inWorkFlowNumberPadre;
	}
	
	/**
	 * Getter.
	 * 
	 * @return	data operazione
	 */
	public final String getDataOperazione() {
		final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		return sdf.format(dataOperazione);
	}

	/**
	 * Getter.
	 * 
	 * @return	motivo assegnazione
	 */
	public final String getMotivoAssegnazione() {
		return motivoAssegnazione;
	}

	/**
	 * Getter.
	 * 
	 * @return	tipo operazione
	 */
	public final String getTipoOperazione() {
		return tipoOperazione;
	}

	/**
	 * Getter.
	 * 
	 * @return	ufficio assegnante
	 */
	public final String getUfficioAssegnante() {
		return ufficioAssegnante;
	}

	/**
	 * Getter.
	 * 
	 * @return	ufficio assegnatario
	 */
	public final String getUfficioAssegnatario() {
		return ufficioAssegnatario;
	}

	/**
	 * Getter.
	 * 
	 * @return	utente assegnante
	 */
	public final String getUtenteAssegnante() {
		return utenteAssegnante;
	}

	/**
	 * Getter.
	 * 
	 * @return	utente assegnatario
	 */
	public final String getUtenteAssegnatario() {
		return utenteAssegnatario;
	}

	/**
	 * Getter.
	 * 
	 * @return	ID tipo operazione
	 */
	public Integer getIdTipoOperazione() {
		return idTipoOperazione;
	}

	/**
	 * Restituisce l'id del documento.
	 * @return id documento
	 */
	public String getIdDocumento() {
		return idDocumento;
	}

	/**
	 * Imposta l'id del documento.
	 * @param inIdDocumento
	 */
	public void setIdDocumento(final String inIdDocumento) {
		this.idDocumento = inIdDocumento;
	}
	
	/**
	 * Restituisce l'id del nodo assegnatario.
	 * @return id nodo assegnatario
	 */
	public Long getIdNodoAssegnatario() {
		return idNodoAssegnatario;
	}

	/**
	 * Restituisce l'id dell'utente assegnatario.
	 * @return id utente assegnatario
	 */
	public Long getIdUtenteAssegnatario() {
		return idUtenteAssegnatario;
	}

	/**
	 * Restituisce l'id dell'ufficio assegnante.
	 * @return id ufficio assegnante
	 */
	public Long getIdNodoAssegnante() {
		return idNodoAssegnante;
	}

	/**
	 * Restituisce l'id dell'utente assegnante.
	 * @return id utente assegnante
	 */
	public Long getIdUtenteAssegnante() {
		return idUtenteAssegnante;
	}

	/**
	 * Restituisce l'attivita.
	 * @return attivita
	 */
	public InputStream getAttivita() {
		return attivita;
	}

	/**
	 * Imposta l'attivita.
	 * @param attivita
	 */
	public void setAttivita(final InputStream attivita) {
		this.attivita = attivita;
	}
	
	/**
	 * Restituisce il numero del workflow.
	 * @return numero workflow
	 */
	public String getWorkFlowNumber() {
		return workFlowNumber;
	}

	/**
	 * Imposta il numero del workflow.
	 * @param workFlowNumber
	 */
	public void setWorkFlowNumber(final String workFlowNumber) {
		this.workFlowNumber = workFlowNumber;
	}
	
	/**
	 * Restituisce il numero del workflow padre.
	 * @return numero workflow padre
	 */
	public String getWorkFlowNumberPadre() {
		return workFlowNumberPadre;
	}

	/**
	 * Imposta il numero del workflow padre.
	 * @param workFlowNumberPadre
	 */
	public void setWorkFlowNumberPadre(final String workFlowNumberPadre) {
		this.workFlowNumberPadre = workFlowNumberPadre;
	}

	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object).
	 */
	@Override
	public int compareTo(final StoricoDTO o) {
		return dataOperazione.compareTo(o.dataOperazione);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dataOperazione == null) ? 0 : dataOperazione.hashCode());
		result = prime * result + ((idDocumento == null) ? 0 : idDocumento.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StoricoDTO other = (StoricoDTO) obj;
		if (dataOperazione == null) {
			if (other.dataOperazione != null)
				return false;
		} else if (!dataOperazione.equals(other.dataOperazione))
			return false;
		if (idDocumento == null) {
			if (other.idDocumento != null)
				return false;
		} else if (!idDocumento.equals(other.idDocumento))
			return false;
		return true;
	}
	
	
}