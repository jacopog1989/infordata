package it.ibm.red.business.dao;

import java.io.Serializable;
import java.sql.Connection;
import java.util.HashMap;
import java.util.List;

/**
 * 
 * @author Vingenito
 *
 */
public interface INotificaUtenteDAO extends Serializable {
 
	/**
	 * Contrassegna o elimina la notifica dal calendario.
	 * @param idUtente
	 * @param idAoo
	 * @param idEvento
	 * @param statoNotifica
	 * @param conn
	 * @return true o false
	 */
	boolean contrassegnaOEliminaNotificaCalendario(Long idUtente, Long idAoo, Integer idEvento, Integer statoNotifica, Connection conn);
 
	/**
	 * Controlla la scadenza della notifica sul calendario.
	 * @param idUtente
	 * @param docTitleList
	 * @param conn
	 * @return mappa document title stato
	 */
	HashMap<String, Integer> checkNotificaCalendarioScadenza(Long idUtente, List<String> docTitleList, Connection conn);
	
	/**
	 * Contrassegna o elimina la notifica in scadenza dal calendario.
	 * @param idUtente
	 * @param idAoo
	 * @param docTitle
	 * @param statoNotifica
	 * @param conn
	 * @return true o false
	 */
	boolean contrassegnaOEliminaNotificaCalendarioScadenza(Long idUtente, Long idAoo, String docTitle, Integer statoNotifica, Connection conn);
 
	/**
	 * Contrassegna o elimina la notifica dalla rubrica.
	 * @param idUtente
	 * @param idAoo
	 * @param idNotificaRubrica
	 * @param statoNotifica
	 * @param conn
	 * @return true o false
	 */
	boolean contrassegnaOEliminaNotificaRubrica(Long idUtente, Long idAoo, Integer idNotificaRubrica, Integer statoNotifica, Connection conn);

	

	

}
