package it.ibm.red.business.service.facade;

import java.io.Serializable;
import java.util.Collection;

import it.ibm.red.business.dto.TimerRichiesteIntegrazioniDTO;

/**
 * Facade del servizio di gestione timer richieste integrazioni.
 */
public interface ITimerRichiesteIntegrazioniFacadeSRV extends Serializable {

	/**
	 * Restituisce tutti i documenti da lavorare verificando il tipo di allaccio principale e il flag relativo al suo stato
	 * @return		Documento da lavorare
	 * */
	Collection<TimerRichiesteIntegrazioniDTO> getNextDaLavorare();

	/**
	 * Elabora l'item.
	 * @param item
	 * @param idAoo
	 */
	void elaboraItem(TimerRichiesteIntegrazioniDTO item, long idAoo);

}
