package it.ibm.red.business.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import it.ibm.red.business.constants.Constants;
import it.ibm.red.business.exception.RedException;
import it.ibm.red.business.logger.REDLogger;

/**
 * The Class DateUtils.
 *
 * @author CPIERASC
 * 
 *         Utility per la gestione dei file.
 */
public final class DateUtils {

	/**
	 * Logger.
	 */
	private static final REDLogger LOGGER = REDLogger.getLogger(DateUtils.class.getName());

	/**
	 * Formato data semplice.
	 */
	public static final String DD_MM_YYYY = "dd/MM/yyyy";

	/**
	 * Formato data per export.
	 */
	public static final String DD_MM_YYYY_EXPORT = "dd_MM_yyyy";

	/**
	 * Formato data con ora, minuti e secondi.
	 */
	public static final String DD_MM_YYYY_HH_MM_SS = "dd/MM/yyyy HH:mm:ss";

	/**
	 * Data standard interpretata come null.
	 */
	public static final LocalDateTime NULL_DATE = LocalDateTime.parse("16/08/1906 21:26:41", DateTimeFormatter.ofPattern(DD_MM_YYYY_HH_MM_SS));

	/**
	 * Data standard interpretata come null.
	 */
	public static final LocalDateTime NULL_DATE_IT = LocalDateTime.parse("16/08/1906 21:26:40", DateTimeFormatter.ofPattern(DD_MM_YYYY_HH_MM_SS));

	/**
	 * Formato data per ricerca Filenet.
	 */
	public static final DateTimeFormatter FILENET_SEARCH_DATE_FORMAT = DateTimeFormatter.ofPattern("yyyyMMdd");

	/**
	 * Pattern data compatta - ISO.
	 */
	public static final String ISO_DATETIME_COMPACT_PATTERN = "yyyyMMdd'T'HHmmss'Z'";

	/**
	 * Formato data anno - mese - giorno.
	 */
	public static final String YYYY_MM_DD = "yyyy-MM-dd";

	/**
	 * Formato Data italiana con orario fino al secondo.
	 */
	public static final String DD_MM_YYYY_HH12_MM_SS = "dd/MM/yyyy hh:mm:ss";

	/**
	 * Pattern orario: ora - minuto - secondo.
	 */
	public static final String HH24_MM_SS = "HH:mm:ss";

	/**
	 * Costruttore.
	 */
	private DateUtils() {
	}

	/**
	 * Metodo per eliminare i dati relativi al tempo dalla data.
	 * 
	 * @param date data iniziale
	 * @return data epurata dalle informazioni temporali
	 */
	public static Date dropTimeInfo(final Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	/**
	 * Metodo per aggiungere ore ad una data.
	 * 
	 * @param date  data di partenza
	 * @param nHour ore da aggiungere
	 * @return nuova data
	 */
	public static Date addHour(final Date date, final Integer nHour) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.HOUR_OF_DAY, nHour);
		return cal.getTime();
	}

	/**
	 * Restituisce la data in ingresso aggiungendone n giorni.
	 * 
	 * @param date
	 * @param nDays giorni da aggiungere
	 * @return Date
	 */
	public static Date addDay(final Date date, final Integer nDays) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DAY_OF_MONTH, nDays);
		return cal.getTime();
	}

	/**
	 * Restituisce la data formattata che rispetta lo standard ISO.
	 * 
	 * @param d la data da trasformare
	 * @return la data che rispetta il pattern
	 */
	public static String getIsoDatetimeFormat(final Date d) {
		final SimpleDateFormat isodateformatter = new SimpleDateFormat(ISO_DATETIME_COMPACT_PATTERN);
		return isodateformatter.format(d);
	}

	/**
	 * Converti una data standard in USLocale.
	 * 
	 * @param data     data originaria
	 * @param inFormat formato
	 * @return data formattata
	 * @throws RedException restituita in caso di errore di formattazione
	 */
	public static String formatDataByUSLocale(final String data, final String inFormat) {
		String dateFormatted = Constants.EMPTY_STRING;

		if (!StringUtils.isNullOrEmpty(data)) {
			String format = inFormat;
			if (inFormat == null) {
				format = DD_MM_YYYY;
			}

			SimpleDateFormat dateStandard = null;
			Date d = null;
			try {
				dateStandard = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy", Locale.ENGLISH);
				d = dateStandard.parse(data);
			} catch (final ParseException e) {
				throw new RedException("Errore nella formattazione della data. " + e.getMessage());
			}

			final SimpleDateFormat dateFormatter = new SimpleDateFormat(format);
			dateFormatter.setTimeZone(TimeZone.getDefault());

			dateFormatted = dateFormatter.format(d);
		}

		return dateFormatted;
	}

	/**
	 * Metodo per la conversione di una data in stringa.
	 * 
	 * @param date     data da convertire
	 * @param inFormat formato
	 * @return stringa convertita
	 */
	public static String dateToString(final Date date, final String inFormat) {
		String output = null;
		if (date != null) {
			String format = inFormat;
			if (inFormat == null) {
				format = DD_MM_YYYY;
			}
			final SimpleDateFormat dateFormatter = new SimpleDateFormat(format);
			dateFormatter.setTimeZone(TimeZone.getDefault());
			output = dateFormatter.format(date);
		}
		return output;
	}

	/**
	 * Metodo per convertire una data in una stringa.
	 * 
	 * @param d      data da convertire
	 * @param noTime se false indica di recuperare anche l'orario
	 * @return la stringa risultante dalla conversione
	 */
	public static String dateToString(final Date d, final boolean noTime) {
		String s = Constants.EMPTY_STRING;
		String dateFormat;

		if (noTime) {
			dateFormat = DD_MM_YYYY;
		} else {
			dateFormat = "dd/MM/yyyy HH:mm";
		}

		final SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		if (d != null) {
			s = sdf.format(d);
		}

		return s;
	}

	/**
	 * Ottieni un oggetto data da una stringa.
	 * 
	 * @param s
	 * @return
	 */
	public static Date parseDate(final String s) {
		Date d = null;
		if (s == null) {
			return d;
		}

		String pattern = "";

		if (s.indexOf(':') >= 0) {
			if (s.length() > 20) {
				pattern = "yyyy-MM-dd HH:mm:ss.SSS";
			} else {
				if (s.lastIndexOf(':') != s.indexOf(':')) {
					pattern = DD_MM_YYYY_HH_MM_SS;
				} else {
					pattern = "dd/MM/yyyy HH:mm";
				}
			}
		} else {
			pattern = DD_MM_YYYY;
		}

		final SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		sdf.setTimeZone(TimeZone.getDefault());
		if (s.length() > 0) {
			try {
				d = sdf.parse(s);
			} catch (final ParseException e) {
				throw new RedException("Errore nella conversione della data: " + e.getMessage());
			}
		}

		return d;
	}

	/**
	 * Parser di una data contenuta in una stringa.
	 * 
	 * @param s
	 * @param myPattern
	 * @return
	 */
	public static String parseStringDate(final String s, final String myPattern) {

		String pattern = "";
		Locale locale = null;
		if (s.indexOf(':') >= 0) {
			if (s.length() > 23) {

				pattern = "EEE MMM dd HH:mm:ss Z yyyy";
				locale = Locale.ENGLISH;

			} else if (s.length() > 20) {

				pattern = "yyyy-MM-dd HH:mm:ss.SSS";

			} else {
				pattern = DD_MM_YYYY_HH_MM_SS;

			}
		} else {
			pattern = DD_MM_YYYY;
		}

		SimpleDateFormat sdfSource = null;
		if (pattern.length() > 23) {
			sdfSource = new SimpleDateFormat(pattern, locale);
		} else {
			sdfSource = new SimpleDateFormat(pattern);
		}

		Date date = null;
		try {
			date = sdfSource.parse(s);
		} catch (final ParseException e) {
			LOGGER.error(e);
		}

		final SimpleDateFormat sdfDestination = new SimpleDateFormat(myPattern);

		return sdfDestination.format(date);
	}

	/**
	 * Se dataInizio o dataFine sono null, il metodo è in grado di non prenderle in
	 * considerazione.
	 * 
	 * @param data
	 * @param dataInizio
	 * @param dataFine
	 * @param inclusive
	 * @return
	 */
	public static Boolean inRange(final Date data, final Date dataInizio, final Date dataFine, final Boolean inclusive) {
		boolean i = false;
		boolean f = false;
		if (Boolean.TRUE.equals(inclusive)) {
			i = (dataInizio == null || !dataInizio.after(data));
			f = (dataFine == null || !dataFine.before(data));
		} else {
			i = (dataInizio == null || dataInizio.before(data));
			f = (dataFine == null || dataFine.after(data));
		}
		return i && f;
	}

	/**
	 * metodo per verificare se il range di date preso in considerazione è valido.
	 * 
	 * @param dataDa
	 * @param dataA
	 * @return
	 */
	public static boolean isRangeOk(final Date dataDa, final Date dataA) {
		boolean isOk = false;

		if (dataDa == null || dataA == null || dataDa.compareTo(dataA) <= 0) {
			isOk = true;
		}

		return isOk;
	}

	/**
	 * metodo per verificare se il range di date preso in considerazione è valido e
	 * l'intervallo è massimo di un anno.
	 * 
	 * @param dataDa
	 * @param dataA
	 * @return
	 */
	public static boolean isRangeAnnualeOk(final Date dataDa, final Date dataA) {
		boolean isOk = isRangeOk(dataDa, dataA);

		if (isOk && dataDa != null && dataA != null) {

			// le date devono distare massimo un anno
			final long da = dataDa.getTime();
			final long a = dataA.getTime();

			final long diff = a - da;
			final long anno = 1000L * 60L * 60L * 24L * 365L;
			isOk = diff <= anno;
		}

		return isOk;
	}

	/**
	 * Restituisce true se le date in ingresso fanno riferimento allo stesso anno.
	 * 
	 * @param dataDa
	 * @param dataA
	 * @param checkRange se true si effettuerà una verifica sulla correttezza del
	 *                   range, altrimenti lo si considera valido.
	 * @return true se le date sono nello stesso anno, false altrimenti
	 */
	public static boolean isSameYearRange(final Date dataDa, final Date dataA, final boolean checkRange) {
		boolean isOk = true;

		if (dataDa != null && dataA != null) {
			if (checkRange) {
				isOk = isRangeOk(dataDa, dataA);
			}

			if (isOk) {
				final Calendar cal = Calendar.getInstance();
				cal.setTime(dataDa);
				final int annoDataDa = cal.get(Calendar.YEAR);
				cal.setTime(dataA);
				final int annoDataA = cal.get(Calendar.YEAR);

				isOk = (annoDataDa == annoDataA);
			}
		}

		return isOk;
	}

	/**
	 * Setta il time della data di riferimento alle 23:59:59:999.
	 * 
	 * @param date
	 * @return
	 */
	public static Date setDateTo2359(final Date date) {
		if (date == null) {
			return null;
		}
		final Date result = (Date) date.clone();
		final Calendar cal = Calendar.getInstance();
		cal.setTime(result);
		cal.set(Calendar.HOUR_OF_DAY, 23);
		cal.set(Calendar.MINUTE, 59);
		cal.set(Calendar.SECOND, 59);
		cal.set(Calendar.MILLISECOND, 999);
		result.setTime(cal.getTime().getTime());

		return result;
	}

	/**
	 * Setta il time della data di riferimento alle 00:00:00:000.
	 * 
	 * @param date
	 * @return
	 */
	public static Date setDateTo0000(final Date date) {
		if (date == null) {
			return null;
		}
		final Date result = (Date) date.clone();
		final Calendar cal = Calendar.getInstance();
		cal.setTime(result);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		result.setTime(cal.getTime().getTime());

		return result;
	}

	/**
	 * Trasforma l'intero associato ad un field di una data in due cifre decimali,
	 * gestendo il mese correttamente.
	 * 
	 * @param field
	 * @param inCalendar
	 * @return numero associato.
	 */
	public static String formatTwoDigitsFieldWithLeadingZero(final int field, final Calendar inCalendar) {
		Calendar calendar = inCalendar;
		if (calendar == null) {
			calendar = Calendar.getInstance();
		}

		int numberToFormat = calendar.get(field);
		if (field == Calendar.MONTH) { // I mesi del Calendar partono da 0, quindi si deve incrementare il numero
			numberToFormat++;
		}

		// Formato "%02d" per ottenere un intero decimale ("d") a due cifre,
		// eventualmente preceduto da uno zero.
		return String.format("%02d", numberToFormat);
	}

	/**
	 * Trasforma la data come XMLGregorianCalendar in java.util.Date.
	 * 
	 * @param date
	 * @return Date
	 */
	public static Date fromXmlGregCalToDate(final XMLGregorianCalendar date) {
		Date output = null;
		if (date != null) {
			output = date.toGregorianCalendar().getTime();
		}
		return output;
	}

	/**
	 * Trasforma la java.util.Date in XMLGregorianCalendar.
	 * 
	 * @param date
	 * @return XMLGregorianCalendar
	 * @throws DatatypeConfigurationException
	 */
	public static XMLGregorianCalendar buildXmlGregorianCalendarFromDate(final Date date) throws DatatypeConfigurationException {
		XMLGregorianCalendar output = null;

		if (date != null) {
			final GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(date);
			output = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
		}

		return output;
	}

	/**
	 * Restituisce una data formattata nel modo corretto per essere utilizzata nella
	 * ricerca Filenet.
	 * 
	 * @param inputData
	 * @param dataDa
	 * @return La data sotto forma di String
	 */
	public static String buildFilenetDataForSearch(final Date inputData, final boolean dataDa) {
		long millisecondsInputData = inputData.getTime();

		if (dataDa) {
			millisecondsInputData = millisecondsInputData - 7200000; // Si sottraggono 2 ore per ricavare il giorno corretto (es. 01/04 00:00:00 --->
																		// 31/03 22:00:00)
		}

		final LocalDate inputLocalDate = Instant.ofEpochMilli(millisecondsInputData).atZone(ZoneId.systemDefault()).toLocalDate();

		return buildFilenetData(inputLocalDate, dataDa);
	}

	/**
	 * Restituisce una data formattata nel modo corretto per essere utilizzata nella
	 * ricerca Filenet a partire dall'anno ed escludendo mese e giorno.
	 * 
	 * @param anno
	 * @param dataDa
	 * @return La data sotto forma di String
	 */
	public static String buildFilenetDataForSearchFromYear(final Integer anno, final boolean dataDa) {
		LocalDate data = null;

		if (dataDa) {
			// 31/12 dell'anno precedente anziché 01/01 dell'anno in input perché
			// altrimenti,
			// quando si aggiunge l'ora nel metodo buildFilenetData, si perderebbero i
			// documenti del primo gennaio.
			data = LocalDate.of(anno - 1, Month.DECEMBER, 31);
		} else {
			data = LocalDate.of(anno, Month.DECEMBER, 31);
		}

		return buildFilenetData(data, dataDa);
	}

	/**
	 * Restituisce una data compatibile con lo standard di Filenet.
	 * 
	 * @param inputLocalDate
	 * @param dataDa
	 * @return La data sotto forma di String
	 */
	private static String buildFilenetData(final LocalDate inputLocalDate, final boolean dataDa) {
		String dataRicercaFilenet = FILENET_SEARCH_DATE_FORMAT.format(inputLocalDate);

		if (dataDa) {
			dataRicercaFilenet += "T220000Z";
		} else {
			dataRicercaFilenet += "T215959Z";
		}

		return dataRicercaFilenet;
	}

	/**
	 * Restituisce true se la data "check" è compresa nel range che va da "start" e
	 * "stop".
	 * 
	 * @param start inizio range
	 * @param stop  fine range
	 * @param check data da controllare
	 * @return true se è nel range, false altrimenti
	 */
	public static Boolean beetwenDate(final Date start, final Date stop, final Date check) {
		Boolean output = true;

		if (check != null) {
			final Long nCheck = dropTimeInfo(check).getTime();
			if (start != null) {
				final Long nStart = dropTimeInfo(start).getTime();
				output &= (nStart <= nCheck);
			}
			if (stop != null) {
				final Long nStop = dropTimeInfo(stop).getTime();
				output &= (nStop >= nCheck);
			}
		} else {
			output = false;
		}

		return output;
	}

	/**
	 * Restituisce il 31/12/9999.
	 * 
	 * @return {@code 31/12/9999} come @see Date.
	 */
	public static Date getLastDate() {
		final SimpleDateFormat sdf = new SimpleDateFormat(DD_MM_YYYY);
		
		try {
			return sdf.parse("31/12/9999");
		} catch (ParseException e) {
			LOGGER.error("Errore nel parsing della data.", e);
		}
		
		return null;
	}

	/**
	 * Restituisce true se l'intervallo dei giorni supera la differenza del range in
	 * ingresso.
	 * 
	 * @param dataDa           inizio range
	 * @param dataA            fine range
	 * @param intervalloGiorni
	 * @return true se compreso nel delta di giorni, false altrimenti
	 */
	public static boolean maxDeltaGiorni(final Date dataDa, final Date dataA, final long intervalloGiorni) {
		boolean intervalloIsNotValid = false;
		final long diffInMilliSec = Math.abs(dataA.getTime() - dataDa.getTime());
		final long diffGiorni = TimeUnit.DAYS.convert(diffInMilliSec, TimeUnit.MILLISECONDS);
		if (diffGiorni > intervalloGiorni) {
			intervalloIsNotValid = true;
		}
		return intervalloIsNotValid;
	}

	/**
	 * Restituisce la data in ingresso aggiungendone n giorni.
	 * 
	 * @param dataDa
	 * @param numDiGiorni giorni da aggiungere
	 * @return Date
	 */
	public static String addDayToDate(final Date dataDa, final Integer numDiGiorni) {
		final SimpleDateFormat sdf = new SimpleDateFormat(DateUtils.DD_MM_YYYY);
		final Calendar cal = Calendar.getInstance();
		cal.setTime(dataDa);
		cal.add(Calendar.DAY_OF_MONTH, numDiGiorni);
		return sdf.format(cal.getTime());
	}
	
	/**
	 * Restituisce l'anno della data in input.
	 * 
	 * @param data
	 * 
	 * @return anno
	 */
	public static int getYearFromDate(final Date data) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		return cal.get(Calendar.YEAR);
		
	}

}